
drop table CRM_GC_ORD cascade constraints;
drop table CRM_GC_ORD_ITEM cascade constraints;

create table CRM_GC_ORD (
        id number(19,0) not null,
        CREATED_BY varchar2(255 char),
        CREATED_DATETIME timestamp,
        LAST_UPDATED_BY varchar2(255 char),
        LAST_UPDATED_DATETIME timestamp,
        MO_DATE timestamp,
        MO_NO varchar2(255 char),
        PO_DATE timestamp,
        PO_NO varchar2(255 char),
        STATUS varchar2(15 char) not null,
        VENDOR number(19,0) not null,
        primary key (id)
    );


    create table CRM_GC_ORD_ITEM (
        id number(19,0) not null,
        CREATED_BY varchar2(255 char),
        CREATED_DATETIME timestamp,
        LAST_UPDATED_BY varchar2(255 char),
        LAST_UPDATED_DATETIME timestamp,
        PRODUCT_CODE varchar2(255 char),
        QUANTITY number(19,0),
        GC_ORDER number(19,0) not null,
        PROD_PROFILE number(19,0) not null,
        primary key (id)
    );


  alter table CRM_GC_ORD 
        add constraint FK_mh73tpk4bulvvalerro5j4kko 
        foreign key (VENDOR) 
        references CRM_GC_VENDOR;


    alter table CRM_GC_ORD_ITEM 
        add constraint FK_cgb5lc62svtr1162twttg3bkm 
        foreign key (GC_ORDER) 
        references CRM_GC_ORD;

    alter table CRM_GC_ORD_ITEM 
        add constraint FK_guqh9go0nghf7996adh6k5b46 
        foreign key (PROD_PROFILE) 
        references CRM_GC_PROD_PROFILE;