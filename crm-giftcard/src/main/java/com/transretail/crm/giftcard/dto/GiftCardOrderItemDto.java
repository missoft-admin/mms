package com.transretail.crm.giftcard.dto;

import org.joda.time.LocalDate;
import org.springframework.format.annotation.DateTimeFormat;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.transretail.crm.common.web.converter.LocalDateSerializer;
import com.transretail.crm.core.entity.lookup.LookupDetail;
import com.transretail.crm.giftcard.entity.GiftCardOrderItem;
import com.transretail.crm.giftcard.entity.ProductProfile;
import com.transretail.crm.giftcard.entity.ProductProfile.Denomination;


/**
 *
 */
public class GiftCardOrderItemDto {

	private Long profileId;
	private String productCode;
	private String productName;
	private LookupDetail faceValue;
	private Long quantity;
	private Double unitCost;
	@DateTimeFormat(pattern = "dd-MM-yyyy")
	@JsonSerialize(using = LocalDateSerializer.class)
	private LocalDate expiredDate=new LocalDate();
	public GiftCardOrderItemDto() {}

	public GiftCardOrderItemDto(GiftCardOrderItem item) {
		if(item.getProfile() != null) {
			this.profileId = item.getProfile().getId();
			this.productCode = item.getProfile().getProductCode();
			this.productName = item.getProfile().getProductDesc();
			this.faceValue = item.getProfile().getFaceValue();
		}
		this.quantity = item.getQuantity();
		this.unitCost = item.getUnitCost();
		this.expiredDate=item.getExpiredDate();
	}

	public GiftCardOrderItem toModel(GiftCardOrderItem item) {
		if(item == null) {
			item = new GiftCardOrderItem();
		}

		item.setProfile(new ProductProfile(this.profileId));
		item.setQuantity(this.quantity);
		item.setUnitCost(unitCost);
		item.setExpiredDate(this.expiredDate);
		return item;
	}


	public String getProductCode() {
		return productCode;
	}

	public void setProductCode(String productCode) {
		this.productCode = productCode;
	}

	public String getProductName() {
		return productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}

	public Long getProfileId() {
		return profileId;
	}

	public void setProfileId(Long profileId) {
		this.profileId = profileId;
	}

	public Long getQuantity() {
		return quantity;
	}

	public void setQuantity(Long quantity) {
		this.quantity = quantity;
	}

	public LookupDetail getFaceValue() {
		return faceValue;
	}

	public void setFaceValue(LookupDetail faceValue) {
		this.faceValue = faceValue;
	}

	public Double getUnitCost() {
		return unitCost;
	}

	public void setUnitCost(Double unitCost) {
		this.unitCost = unitCost;
	}

	public LocalDate getExpiredDate() {
		return expiredDate;
	}

	public void setExpiredDate(LocalDate expiredDate) {
		this.expiredDate = expiredDate;
	}


}
