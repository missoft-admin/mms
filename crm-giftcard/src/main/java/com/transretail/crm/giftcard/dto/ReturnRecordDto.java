package com.transretail.crm.giftcard.dto;

import java.math.BigDecimal;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.joda.time.DateTime;
import org.joda.time.LocalDate;
import org.springframework.format.annotation.DateTimeFormat;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.google.common.collect.Lists;
import com.transretail.crm.common.web.converter.LocalDateSerializer;
import com.transretail.crm.core.entity.lookup.LookupDetail;
import com.transretail.crm.giftcard.entity.GiftCardCustomerProfile;
import com.transretail.crm.giftcard.entity.ReturnByCardsItem;
import com.transretail.crm.giftcard.entity.ReturnByCardsItem.ItemType;
import com.transretail.crm.giftcard.entity.ReturnRecord;
import com.transretail.crm.giftcard.entity.SalesOrder;
import com.transretail.crm.giftcard.entity.support.GiftCardInventoryStatus;
import com.transretail.crm.giftcard.entity.support.ReturnStatus;


/**
 *
 */
public class ReturnRecordDto {
	
	private Long id;
	private String createdBy;
	private String recordNo;
	@DateTimeFormat(pattern = "dd-MM-yyyy")
	@JsonSerialize(using = LocalDateSerializer.class)
	private LocalDate returnDate;
	private Long customerId;
	private String customerName;
	private Long orderId;
	private String orderNo;
	private LocalDate orderDate;
	private BigDecimal discountRate;
	private BigDecimal faceAmount;
	private BigDecimal returnAmount = BigDecimal.ZERO;
	private BigDecimal refundAmount = BigDecimal.ZERO;
	private BigDecimal replaceAmount = BigDecimal.ZERO;
	private ReturnStatus status;
	
	private LookupDetail reason;
	
	private String contactPerson;
	private String contactNumber;
	private String contactEmail;
	private String paymentStore;

	private DateTime lastUpdated;
	
	//validation
	private String startingSeries;
	private String endingSeries;
	private String productCode;
	private String productName;
	private Long prodId;
	private Double balance;
	private GiftCardInventoryStatus cardStatus;
	
	private List<ReturnByCardsItemDto> returns = Lists.newArrayList();
	private List<ReturnByCardsItemDto> replacements = Lists.newArrayList();
	
	
	public ReturnRecordDto() {}
	
	public ReturnRecordDto(ReturnRecord record, boolean all) {
		this.id = record.getId();
		this.createdBy = record.getCreateUser();
		this.lastUpdated = record.getLastUpdated();
		if(record.getOrderNo() != null) {
			SalesOrder order = record.getOrderNo();
			this.orderNo = order.getOrderNo();
			this.orderId = order.getId();
			this.discountRate = order.getDiscountVal();
			this.contactPerson = order.getContactPerson();
			this.contactNumber = order.getContactNumber();
			this.contactEmail = order.getContactEmail();
			this.paymentStore = order.getPaymentStore();
			this.orderDate = order.getOrderDate();
		}
		this.faceAmount = record.getFaceAmount();
		this.recordNo = record.getRecordNo();
		this.returnDate = record.getReturnDate();
		this.returnAmount = record.getReturnAmount();
		this.refundAmount = record.getRefundAmount();
		this.replaceAmount = record.getReplaceAmount();
		this.reason = record.getReason();
		if(record.getCustomer() != null) {
			this.customerId = record.getCustomer().getId();
			this.customerName = record.getCustomer().getName();
		}
		this.status = record.getStatus();
		
		if(all) {
			for(ReturnByCardsItem item: record.getItems()) {
				if(item.getType().compareTo(ItemType.RETURN) == 0) {
					returns.add(new ReturnByCardsItemDto(item));
				} else if(item.getType().compareTo(ItemType.REPLACEMENT) == 0) {
					replacements.add(new ReturnByCardsItemDto(item));
				}
			}
		}
	}
	
	public ReturnRecord toModel(ReturnRecord record) {
		if(record == null)
			record = new ReturnRecord();
		
		record.setOrderNo(new SalesOrder(this.orderId));
		record.setFaceAmount(this.faceAmount);
		record.setRecordNo(this.recordNo);
		record.setStatus(this.status);
		record.setReturnDate(this.returnDate);
		record.setReturnAmount(this.returnAmount);
		record.setCustomer(new GiftCardCustomerProfile(this.customerId));
		record.setReason(this.reason);
		
		if(record.getItems() != null) {
			record.getItems().clear();
			if(CollectionUtils.isNotEmpty(this.returns)) {
				for(ReturnByCardsItemDto dto : this.returns) {
	                if (dto != null) {
	                	ReturnByCardsItem item = dto.toModel(new ReturnByCardsItem());
	            		item.setRecord(record);
	            		item.setType(ItemType.RETURN);
	            		record.getItems().add(item);
	                }
	            }
			}
		} else {
			record.setItems(ReturnByCardsItemDto.toModel(this.returns, record));	
		}
		
		return record;
	}
	
	public static List<ReturnRecordDto> toDto(List<ReturnRecord> orders) {
		List<ReturnRecordDto> orderDtos = Lists.newArrayList();
		for(ReturnRecord order: orders) {
			orderDtos.add(new ReturnRecordDto(order, false));
		}
		return orderDtos;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getRecordNo() {
		return recordNo;
	}

	public void setRecordNo(String recordNo) {
		this.recordNo = recordNo;
	}

	public String getOrderNo() {
		return orderNo;
	}

	public void setOrderNo(String orderNo) {
		this.orderNo = orderNo;
	}

	public ReturnStatus getStatus() {
		return status;
	}

	public void setStatus(ReturnStatus status) {
		this.status = status;
	}

	public LocalDate getReturnDate() {
		return returnDate;
	}

	public void setReturnDate(LocalDate returnDate) {
		this.returnDate = returnDate;
	}

	public Long getCustomerId() {
		return customerId;
	}

	public void setCustomerId(Long customerId) {
		this.customerId = customerId;
	}

	public String getCustomerName() {
		return customerName;
	}

	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public BigDecimal getDiscountRate() {
		return discountRate;
	}

	public void setDiscountRate(BigDecimal discountRate) {
		this.discountRate = discountRate;
	}

	public BigDecimal getFaceAmount() {
		return faceAmount;
	}

	public void setFaceAmount(BigDecimal faceAmount) {
		this.faceAmount = faceAmount;
	}

	public Long getOrderId() {
		return orderId;
	}

	public void setOrderId(Long orderId) {
		this.orderId = orderId;
	}

	public BigDecimal getReturnAmount() {
		return returnAmount;
	}

	public void setReturnAmount(BigDecimal returnAmount) {
		this.returnAmount = returnAmount;
	}

	public BigDecimal getRefundAmount() {
		return refundAmount;
	}

	public void setRefundAmount(BigDecimal refundAmount) {
		this.refundAmount = refundAmount;
	}

	public BigDecimal getReplaceAmount() {
		return replaceAmount;
	}

	public void setReplaceAmount(BigDecimal replaceAmount) {
		this.replaceAmount = replaceAmount;
	}

	public BigDecimal getRemainingReturnAmount() {
		BigDecimal remRetAmt = getReturnAmount();
		if(getRefundAmount() != null)
			remRetAmt = remRetAmt.subtract(getRefundAmount());
		if(getReplaceAmount() != null)
			remRetAmt = remRetAmt.subtract(getReplaceAmount());
		return remRetAmt;
	}
	
	
	public boolean getIsReadyForApproval() {
		BigDecimal retAmt = getRemainingReturnAmount();
		if(retAmt != null && retAmt.compareTo(BigDecimal.ZERO) == 0) {
			return true;
		}
		return false;
	}

	public String getContactPerson() {
		return contactPerson;
	}

	public void setContactPerson(String contactPerson) {
		this.contactPerson = contactPerson;
	}

	public String getContactNumber() {
		return contactNumber;
	}

	public void setContactNumber(String contactNumber) {
		this.contactNumber = contactNumber;
	}

	public String getContactEmail() {
		return contactEmail;
	}

	public void setContactEmail(String contactEmail) {
		this.contactEmail = contactEmail;
	}

	public String getPaymentStore() {
		return paymentStore;
	}

	public void setPaymentStore(String paymentStore) {
		this.paymentStore = paymentStore;
	}

	public DateTime getLastUpdated() {
		return lastUpdated;
	}

	public void setLastUpdated(DateTime lastUpdated) {
		this.lastUpdated = lastUpdated;
	}

	public LocalDate getOrderDate() {
		return orderDate;
	}

	public void setOrderDate(LocalDate orderDate) {
		this.orderDate = orderDate;
	}

	public String getStartingSeries() {
		return startingSeries;
	}

	public void setStartingSeries(String startingSeries) {
		this.startingSeries = startingSeries;
	}

	public String getEndingSeries() {
		return endingSeries;
	}

	public void setEndingSeries(String endingSeries) {
		this.endingSeries = endingSeries;
	}

	public String getProductCode() {
		return productCode;
	}

	public void setProductCode(String productCode) {
		this.productCode = productCode;
	}

	public String getProductName() {
		return productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}

	public Double getBalance() {
		return balance;
	}

	public void setBalance(Double balance) {
		this.balance = balance;
	}

	public GiftCardInventoryStatus getCardStatus() {
		return cardStatus;
	}

	public void setCardStatus(GiftCardInventoryStatus cardStatus) {
		this.cardStatus = cardStatus;
	}

	public List<ReturnByCardsItemDto> getReturns() {
		return returns;
	}

	public void setReturns(List<ReturnByCardsItemDto> returns) {
		this.returns = returns;
	}

	public List<ReturnByCardsItemDto> getReplacements() {
		return replacements;
	}

	public void setReplacements(List<ReturnByCardsItemDto> replacements) {
		this.replacements = replacements;
	}

	public LookupDetail getReason() {
		return reason;
	}

	public void setReason(LookupDetail reason) {
		this.reason = reason;
	}

	public Long getProdId() {
		return prodId;
	}

	public void setProdId(Long prodId) {
		this.prodId = prodId;
	}
	
	
	
	
}
