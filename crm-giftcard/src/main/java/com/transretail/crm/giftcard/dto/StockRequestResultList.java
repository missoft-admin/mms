package com.transretail.crm.giftcard.dto;

import java.util.Collection;

import com.transretail.crm.common.service.dto.response.AbstractResultListDTO;

/**
 *
 */
public class StockRequestResultList extends AbstractResultListDTO<StockRequestDto> {
    public StockRequestResultList(Collection<StockRequestDto> results, long totalElements, boolean hasPreviousPage,
        boolean hasNextPage) {
        super(results, totalElements, hasPreviousPage, hasNextPage);
    }
}
