<%@ include file="../../common/taglibs.jsp" %>

<style type="text/css">
<!--

  #productTable tr td:last-child {
    border-left: 0px;
  }

  #addProductBtn.btn-small {
    margin-bottom: 5px;
  }
  table select, textarea, table input[type="text"], input[type="password"], input[type="datetime"], input[type="datetime-local"], input[type="date"], input[type="month"], input[type="time"], input[type="week"], input[type="number"], input[type="email"], input[type="url"], input[type="search"], input[type="tel"], input[type="color"], .uneditable-input {
      margin-bottom: 0px;
      margin-top: 0px;
  }

-->
</style>
<script src="<spring:url value="/js/common/typeaheadutil.js" />"></script>
<script src="<spring:url value="/js/moment.js" />"></script>
<c:set var="ENCTYPE" value="application/x-www-form-urlencoded"/>
<div class="modal-dialog">
	<div class="modal-content">
		<spring:message code="label_manufacture_order" var="typeName" />
		<div class="modal-header">
			<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
			<h4>
				<spring:message code="gc_order" />
			</h4>
		</div>
		<div class="modal-body">
			<div id="contentError" class="hide alert alert-error">
				<button type="button" class="close" onclick="$(this).parent().hide();">&times;</button>
				<div>
					<%-- ==========  Untuk error keseluruhan ========= --%>
					<form:errors path="*" />
					<%-- ==========  End Untuk error keseluruhan ========= --%>
				</div>
			</div>

			<%-- ==========  Form Untuk Gift Order =========== --%>
			<form:form id="orderForm" name="orderForm" modelAttribute="orderForm" action="${pageContext.request.contextPath}/giftcard/order/save/" method="POST" enctype="${ENCTYPE}" cssClass="form-reset form-horizontal">
				<%-- ===== path untuk field id ===== --%>
				<form:hidden path="id" />
				<div class="contentMain">
					<div class="row-fluid">
						<div class="span5">
							<div class="control-group">
								<spring:message code="order_mo_number" var="moNo" />
								<label for="moNumber" class="control-label">${moNo}</label>
								<div class="controls">
									<form:input path="moNumber" class="form-control intInput" readonly="true" placeholder="${moNo}" />
								</div>
							</div>
							<div class="control-group">
								<spring:message code="order_mo_date" var="moDt" />
								<label for="moDate" class="control-label">${moDt}</label>
								<div class="controls">
									<form:input path="moDate" class="form-control orderDate" placeholder="${moDt}" />
								</div>
							</div>
							<div class="control-group">
								<spring:message code="order_po_number" var="poNo" />
								<label for="poNumber" class="control-label"><b class="required">*</b>${poNo}</label>
								<div class="controls">
									<form:input path="poNumber" class="form-control intInput" placeholder="${poNo}" />
								</div>
							</div>
							<div class="control-group">
								<spring:message code="order_po_date" var="poDt" />
								<label for="poDate" class="control-label"><b class="required">*</b>${poDt}</label>
								<div class="controls">
									<form:input path="poDate" class="form-control orderDate" placeholder="${poDt}" />
								</div>
							</div>
							<div class="control-group">
								<spring:message code="order_supplier" var="supp" />
								<label for="vendorId" class="control-label">${supp}</label>
								<div class="controls">
									<form:select path="vendorId" items="${vendors.results}" itemLabel="name" itemValue="id" />
								</div>
							</div>
						</div>
						<div class="span7">
							<div class="pull-left" style="margin-right: 5px;">
								<button type="button" id="addProductBtn" class="btn btn-small">+</button>
								<br />
								<button type="button" id="removeProductBtn" class="btn btn-small">&ndash;</button>
							</div>
							<div class="pull-left" style="width: 90%;">
								<table id="productTable" class="table table-condensed table-compressed table-striped">
									<thead>
										<tr>
											<th></th>
											<th><spring:message code="order_item_card_type" /></th>
											<th><b class="required">*</b>
											<spring:message code="order_item_quantity" /></th>
											<th>Expired Date</th>
										</tr>
									</thead>
									<tbody>
										<c:forEach var="item" items="${orderForm.items}" varStatus="status">
											<tr data-index="${status.index}">
												<td><input type="checkbox" class="productRemoveFlag" /></td>
												<td><select class="input-medium profile" name="items[${status.index}].profileId" id="prod[${status.index}]">
													<option value="0" data-cost="0" data-exp="0"></option>
														<c:forEach items="${profiles}" var="profile">
															<option value="${profile.id}"
																	data-exp="${profile.effectiveMonths}"
																	data-cost="${profile.unitCost}"
																	<c:if test="${profile.id == item.profileId}">selected</c:if>>
																	${profile.productDesc}
															</option>
														</c:forEach>
												</select> <form:hidden path="items[${status.index}].unitCost" class="unit-cost" /></td>
												<td><input type="text" class="input-mini quantity" value="${item.quantity}" name="items[${status.index}].quantity" /></td>
												<td><form:input type="text" id="expdate[${status.index}]" class="input-medium" placeholder="${poDt}" path="items[${status.index}].expiredDate" autocomplete="off"  /></td>
												<td><input class="checkbox" type="checkbox" id="dateExpCheckBox[${status.index}]"><label for="dateExpCheckBox[${status.index}]" style="text-transform: lowercase">exp latter</label></td>
											</tr>
										</c:forEach>
									</tbody>
								</table>
							</div>
							<div class="clearFix"></div>
						</div>
					</div>
				</div>
			</form:form>
		</div>
		<div class="modal-footer" id="moOrderCtrlBtns">
			<button type="button" data-status="FOR_APPROVAL" class="orderSubmit btn btn-primary">
				<spring:message code="label_submit_for_approval" />
			</button>
			<button type="button" data-status="DRAFT" class="orderSubmit btn btn-primary">
				<spring:message code="label_draft" />
			</button>
			<button type="button" id="orderCancel" class="btn btn-default" data-dismiss="modal">
				<spring:message code="label_cancel" />
			</button>
		</div>
	</div>
</div>
<script>

	$(document).ready(function() {


		var CONTAINER_ERROR = "#contentError > div";
		var CONTENT_ERROR = "#contentError";

		var $form = $("#orderForm");
		var $orderDialog = $("#orderDialog").css({
			"width" : "1050px"
		});

		$(".orderSubmit").click(submitForm);
		$form.find(".productId").change(function() {
			$(this).siblings(".unit-cost").val(
			$(this).data('unit-cost'));
			console.log($(this).siblings(".unit-cost").val());
		}).change();

		function submitForm() {

			$(".orderSubmit").prop("disabled", true);
			$.post($form.attr("action")+ $(this).data("status"),$form.serialize(),function(data) {
				var errorInfo;

				if (data.success) {

					location.reload();

				} else {

					errorInfo = "";
					for (var i = 0; i < data.result.length; i++) {
						errorInfo += "<br>"
								+ (i + 1)
								+ ". "
								+ (data.result[i].code !== undefined ? data.result[i].code: data.result[i]);
					}

					$orderDialog.find(CONTAINER_ERROR).html("Please correct following errors: "+ errorInfo);
					$orderDialog.find(CONTENT_ERROR).show('slow');
					$(".orderSubmit").prop("disabled", false);
				}
			});
		}

		$("input.orderDate").datepicker({
			format : 'dd-mm-yyyy',
			autoclose : true,
			endDate : '+0d'
		});

		$("input[name='moDate']").datepicker("setDate",new Date()).datepicker("setStartDate",new Date());
		$("input[name='orderDate']").datepicker("setEndDate",new Date());

		var $productTable = $('#productTable');
		var $productTbody = $("tbody", $productTable);

		$('#removeProductBtn', $orderDialog).click(function() {
			$("input.productRemoveFlag:checked").each(function() {
				var $tr = $(this).closest("tr");
				var length = $productTbody.find("tr").length;
				if ($tr.is(":first-child") && length === 1) {

					$tr.find("input.productRemoveFlag").prop('checked',false);
					$tr.find("input").removeAttr('value').prop('disabled',true);
					$tr.find("select").removeAttr('value').prop('disabled',true);
					$tr.invisible();

				} else{
					$tr.remove();
				}
			});
		});

		$('#addProductBtn', $orderDialog).click(function() {

			var $tr = $productTbody.find("tr:first-child");
			if ($tr.css('visibility') === 'hidden' || $tr.css('display') === 'none') {

				$tr.find("input").removeAttr('value').prop('disabled', false);
				$tr.find("select").removeAttr('value').prop('disabled', false);
				$tr.visible();

			} else {

				var $trLast = $productTbody.find("tr:last-child");
				var $row = $tr.clone();
				$row.find("input").removeAttr('value').prop('disabled', false);
				$row.find("select").removeAttr('value').prop('disabled', false);
				var index = parseInt($trLast.attr("data-index")) + 1;
				var lastIndex = $tr.attr("data-index");
				$row.attr("data-index", index);
				var rg = new RegExp("\\["+ lastIndex + "\\]",'g');
				var html = $row.html();
				html = html.replace(rg, '['+ index + ']');
				$row.html(html);
				$row.find(".productId").change(function() {
					$(this).siblings(".unit-cost").val($(this).data('unit-cost'));
				});
				$productTbody.append($row);
				initFields($row);

				var m = $("#prod\\["+index+"\\]").find("option:selected").attr("data-exp");
				$("#expdate\\["+index+"\\]").datepicker({
					format : 'dd-mm-yyyy',
					autoclose : true,
					startDate: '+'+m+'m',
					immediateUpdates:true
				});
			}
		});

		initFields($orderDialog);

		jQuery.fn.visible = function() {
			return this.css('visibility', 'visible');
		};

		jQuery.fn.invisible = function() {
			return this.css('visibility', 'hidden');
		};

		function initFields(e) {

			var url = '<c:url value="/gc/productprofile/list/false/" />';
			var $hiddenId = $(e).find(".productId");

			TypeAhead.process($(e).find(".productDesc"),
					$hiddenId, url, {
						label : "productDesc",
						value : "id"
					}, [ "faceValueDesc", "unitCost"],
					function($item) {
						$hiddenId.data("face-value",$item["faceValueDesc"]);
						$hiddenId.data("unit-cost",$item["unitCost"]);
						$hiddenId.change();
					});

			$('.quantity', $(e)).numberInput({
				"type" : "int"
			});

			$('.intInput', $(e)).numberInput({
				"type" : "int"
			});

			$('.floatInput', $(e)).numberInput({
				"type" : "float"
			});

			$( ".profile").on( "change", function () {

                $( ".checkbox", $(e) ).click(function () {

                    var idx = $(this).parent().parent().attr("data-index");
                    var m = $(this).find("option:selected").attr("data-exp");
                    var expdate = moment().add(parseInt(m)+1,'months');
                    var d = parseInt(expdate.format("DD"));
                    expdate = expdate.subtract(d,'day').toDate();
                    $("#expdate\\["+idx+"\\]")
                        .datepicker({format:"dd-mm-yyyy",autoclose:true})
                        .datepicker( "setDate", expdate)
                        .datepicker( "setStartDate", expdate);

                    var $parent = $(this).closest("tr");
                    if ( $( ".checkbox").is(":checked")) {
                        $("input[type=text].input-medium", $parent).prop("readonly", "readonly").val("");
                    } else {
                        $("input[type=date].input-medium", $parent).prop("readonly", "");
                    }

                });
            });
		}
	});
</script>