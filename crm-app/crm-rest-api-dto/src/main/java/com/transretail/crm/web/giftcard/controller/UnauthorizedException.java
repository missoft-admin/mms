package com.transretail.crm.web.giftcard.controller;

/**
 *
 * @author Monte Cillo Co (mco@exist.com)
 */
public class UnauthorizedException extends RuntimeException {

    public UnauthorizedException() {
	super();
    }

    public UnauthorizedException(String message) {
	super(message);
    }

}
