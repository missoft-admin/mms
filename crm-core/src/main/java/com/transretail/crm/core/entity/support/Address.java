/*
 * Copyright (c) 2013.
 * All rights reserved.
 */
package com.transretail.crm.core.entity.support;

import javax.persistence.Column;
import javax.persistence.Embeddable;

/**
 * @author mhua
 */
@Embeddable
public class Address {

    @Column
    private String address1;

    @Column
    private String street;

    @Column
    private String city;

    @Column
    private String postal;

    @Column
    private String country;

    public String getAddress1() {
        return address1;
    }

    public void setAddress1(String address1) {
        this.address1 = address1;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getPostal() {
        return postal;
    }

    public void setPostal(String postal) {
        this.postal = postal;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

}
