$(document).ready(function() {
	var ITEM_DIALOG = "#itemDialog";
	var GROUP_FORM_ID = "groupForm";
	var CONTAINER_ERROR = "#contentError > div";
	var CONTENT_ERROR = "#contentError";
	var VIEW_ITEMS_DIALOG = "#viewItemsDialog";
	
	$list = $("#list_group");
	
	initDialogs();
	
	initDataTable();

	function initDataTable() {
		  
	  $list.ajaxDataTable({
	    'autoload'  : true,
	    'ajaxSource' : $list.data("url"),
	    'columnHeaders' : columnHeaders,
	    'modelFields' : modelFields,
	  })
	  .on("click", ".viewItemsLnk", function(e) {
		  e.preventDefault();
		  var itemId = $(this).data("itemId");
		  viewItems(itemId);
	  })
	  .on("click", ".editGroupLnk", function(e) {
		  e.preventDefault();
			var itemId = $(this).data("itemId");
			var queryStr = $(this).data("queryStr");
			editGroup(itemId, queryStr);
	  });
	}
	
	$("#groupsLnk").click(function(e) {
		e.preventDefault();
		var queryStr = $(this).data("queryStr");
		createGroup(queryStr);
	});


	$( "#selectStoreGroups" ).dblclick( function(e) {
		$( "#addBtn" ).trigger( "click" );
	});
	$( "#selectedStoreGroups" ).dblclick( function(e) {
		$( "#removeBtn" ).trigger( "click" );
	});

	$("#addBtn").click(function(e) {
		e.preventDefault();
		$("select[name='selectItems'] option:selected").each(function() {
			$("select.items").append($(this));
		});
	});
	
	$("#removeBtn").click(function(e) {
		e.preventDefault();
		$("select.items option:selected").each(function() {
			$("select[name='selectItems']").append($(this));
		});
	});
	
	$("#searchBtn").click(function(e) {
		e.preventDefault();
		searchItem(false);
	});
	
	
	function initDialogs() {
		$('.groupModal').modal({
			show : false
		});
	}
	function searchItem(ind) {
		var theParentForm = $("form[id='" + GROUP_FORM_ID + "']");
		var searchStr = $("input[name='searchStr']").val();
		$.ajax({
			type 	: "GET",
			url 	: $(theParentForm).attr("action") + "/searchitems",
			dataType: 'json',
			data 	: "searchStr=" + searchStr,
			        
			success : function(inResponse) {
				processSearchResults(inResponse);
				
				if(ind) {
					$(ITEM_DIALOG).modal("show");
				}
			},
			error : function(jqXHR, textStatus, errorThrown) {
				$( CONTENT_ERROR ).html("Error: " + errorThrown);
			}
		});	
	}
	
	function processSearchResults(inResponse) {
		var inReturnEntity = inResponse.result;
		$("select[name='selectItems']").empty();
		
		$.each(inReturnEntity, function(key, value) {
			var val = "";
			if(value.code != null && value.code != "")
				val = value.code;
			else if(value.id != null && value.id != "")
				val = value.id;

			var label = value.codeAndName;
			if($("select.items option[value='"+val+"']").length == 0)
				$("<option>").attr("value", val).text(label).appendTo($("select[name='selectItems']"));
		});	
		
	}
	
	function initDialogButtons(id, inAjaxController, inQueryString, methodType) {
		var theParentForm = $("form[id='" + GROUP_FORM_ID + "']");
		$(ITEM_DIALOG).find("#button_save").unbind("click").click( function(e) {
			e.preventDefault();
			$(ITEM_DIALOG).find("#button_save").prop( "disabled", true );
			$("select.items option").prop("selected", "selected");
			
			$.ajax({
				type 	: methodType,
				url 	: $(theParentForm).attr("action") + inAjaxController,
				dataType: 'json',
				data 	: $(theParentForm).serialize(),
				        
				success : function(inResponse) {
					processResults(inResponse, theParentForm, inQueryString);
				},
				error : function(jqXHR, textStatus, errorThrown) {
					$( CONTENT_ERROR ).html("Error: " + errorThrown);
				}
			});
		});
		
		$(ITEM_DIALOG).find("#button_cancel").unbind("click").click( function(e) {
			e.preventDefault();
			resetForm();
	    	$(ITEM_DIALOG).modal("hide");
	    });
	}

	function processResults(inResponse, theParentForm, inQueryString) {
		if (inResponse.success) {
			$(ITEM_DIALOG).find( CONTENT_ERROR ).hide('fast');
			$(theParentForm).unbind("submit").submit( function() {
				$(this).attr("action", $(this).attr("action"));// + "?" + inQueryString);
			});
			$(theParentForm).submit();
		} 
		else {
			$(ITEM_DIALOG).find("#button_save").prop( "disabled", false );
			errorInfo = "";
			for (i = 0; i < inResponse.result.length; i++) {
				errorInfo += "<br>" + (i + 1) + ". "
						+ ( inResponse.result[i].code != undefined ? 
								inResponse.result[i].code : inResponse.result[i]);
			}
			$(ITEM_DIALOG).find( CONTAINER_ERROR ).html("Please correct following errors: " + errorInfo);
			$(ITEM_DIALOG).find( CONTENT_ERROR ).show('slow');
		}
	}

	function initFormDialogButtons(id, queryString) {
		var controllerAction = "";
		var methodType = "POST";
		if(id == null || id == "") {
			controllerAction = "";
		} else {
			controllerAction += "/" + id;
		}
			
		initDialogButtons(id, controllerAction, queryString, methodType);
	}
	
	function showModal() {
		searchItem(true);
	}

	function createGroup(inQueryString) {
		resetForm();
		showModal();
		initFormDialogButtons('', inQueryString)
	}

	function resetForm() {
		var theParentForm = $("form[id='" + GROUP_FORM_ID + "']");
		$(theParentForm).trigger("reset");
		$(ITEM_DIALOG).find( CONTAINER_ERROR ).empty();
		$(ITEM_DIALOG).find( CONTENT_ERROR ).hide();
		$("select.items").empty();
	}

	function editGroup(id, inQueryString) {
		resetForm();
		showModal();
		populateForm(id);
		initFormDialogButtons(id, inQueryString);
	}

	function populateForm(id) {
		var CONTROLLER_POPULATE = "/populate/";
		if(id == null || id == "") {
			CONTROLLER_POPULATE += null;
		} else {
			CONTROLLER_POPULATE += id;
		}
		populate( CONTROLLER_POPULATE );
	}

	function populate( inPopulateUrl ) {
	    var theParentForm = $("form[id='" + GROUP_FORM_ID + "']");
		$.ajax({
			type 	: "POST",
			url 	: $(theParentForm).attr("action") + inPopulateUrl,
			dataType: 'json',
			        	
			success : function(inResponse) {
				var inReturnEntity = inResponse.result;
				$.each(inReturnEntity, function(key, value) {
					
					var theInput = $("input[name='" + key + "']");
					if ( theInput != null ) {
						if ( $(theInput).is(':radio') ) {
							$("input:radio[name='" + key + "']").each( function() {
								$(this).attr( 'checked', $(this).val() == value );
								if($(this).val() == value) {
									$(this).click();
								}
							});
						}
						
						else if ( $(theInput).is(':checkbox') ) {
							$.each(value.split(","), function(index, item) {
								$("input:checkbox[name='"+key+"'][value='"+item+"']").prop("checked", "checked");
							});
						}
						else {
							$(theInput).val( value );
						}
					}
					
					var theSelect = $("select[multiple][name='" + key + "']");
					if(theSelect != null && theSelect.length > 0) {
						if(value != null) {
							$.each(value.split(","), function(index, item) {
								$("select[name='selectItems'] option[value='"+item+"']").appendTo($("select.items"));
							});	
						}
						
					}
					
					var theTextArea = $("textarea[name='" + key + "']");
					if ( theTextArea != null ) {
						$(theTextArea).val( value );
					}
					
					fillField(key, value);
				});
			},
			error : function(jqXHR, textStatus, errorThrown) {
				$( CONTAINER_ERROR ).html("Error: " + errorThrown);
			}
		});
	}

	function viewItems(itemId) {
		$(VIEW_ITEMS_DIALOG).modal("show");
		reloadItemsTable(itemId);
	}


	function fillField(key, value) {
		var name = "[name='" + key + "']";
		var inputField = "";
		inputField = $("input[name='" + key + "']");

		if(({}).toString.call(value) == "[object Object]" && value.code !== undefined) {
			value = value.code;
		}
		
		if ( inputField != null ) {
			if ( $(inputField).is(':radio') ) {
				$("input:radio[name='" + key + "']").each( function() {
					$(this).attr( 'checked', $(this).val() == value );
					if($(this).val() == value) {
						$(this).click();
					}
				});
			} else if ( $(inputField).is(':checkbox') ) {
			} else {
				$(name).val( value );
			}
		}
	}

});


