<%@ include file="../../common/taglibs.jsp" %>

<spring:message var="groupName" code='label_group_groupname' />
<spring:message var="field" code='label_group_field' />
<spring:message var="value" code='label_group_value' />

<div id="itemDialog" class="modal  hide nofly">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4><spring:message code="label_member_grp_detail" /></h4>
            </div>
            <div class="modal-body">
                <c:set var="ENCTYPE"
                    value="application/x-www-form-urlencoded" />
                <c:if test="${multipart}">
                    <c:set var="ENCTYPE" value="multipart/form-data" />
                </c:if>

                <form:form id="memberGroup" name="memberGroup"
                    modelAttribute="memberGroup"
                    action="${pageContext.request.contextPath}/groups/member"
                    method="POST" enctype="${ENCTYPE}">
                        <div class="container-fluid">
                        
                        <div id="contentError" class="hide alert alert-error">
                          <button type="button" class="close" onclick="$(this).parent().hide();">&times;</button>
                          <div><form:errors path="*"/></div>
                        </div>
                        
                        <div class="row-fluid">
                            <div class="pull-left form-horizontal">
                                <div class="control-group">
                                    <label for="name" class="control-label"><b class="required">*</b><spring:message code="label_group_groupname" />&nbsp;&nbsp;</label>
                                    <div class="controls">
                                      <input type="text" name="name" class="name" id="memberGroupName" />
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label for="cardTypeGroupCode" class="control-label"><spring:message code="label_group_profitcode" />&nbsp;&nbsp;</label>
                                    <div class="controls">
                                      <input type="text" name="cardTypeGroupCode" class="cardTypeGroupCode" id="cardTypeGroupCode" />
                                    </div>
                                </div>
                                <div class="control-group checkbox-set1">
                                    <label class="control-label">&nbsp;&nbsp;</label>
                                    <div class="controls">
                                        <div class="checkbox"><input type="checkbox" name="rfs.enabled" id="rfsCheckbox" /><label><spring:message code="label_rfs" /></label></div>
                                        <div class="checkbox"><input type="checkbox" name="points.enabled" id="pointsCheckbox" /><label><spring:message code="label_points" /></label></div>
                                        <div class="checkbox"><input type="checkbox" name="prepaidUser" id="prepaidCheckbox" /><label><spring:message code="label_prepaid" /></label></div>
                                    </div>
                                </div>
                            </div>
                            <input type="button" value="<spring:message code="label_member_grp_add" />" class="btn btn-primary pull-right" id="addField" />
                        </div>
                        </div>
                        
                        <div id="fieldsform" class="container-fluid">
                            <label></label>
                        </div>
                        
                        <div class="clearfix"></div>
                        
                        <div id="rfsStoreForm" class="container-fluid hide" style="margin-top: 0px;"><jsp:include page="rfsstore.jsp"/></div>
                        <div id="pointsForm" class="container-fluid hide" style="margin-top: 20px;"><jsp:include page="points.jsp"/></div>
                        

                </form:form>
            </div>
            <div class="modal-footer container-btn">
                <!--<input type="button" class="btn btn-primary" id="button_save" value="<spring:message code="label_save" />" />
                <input type="button" class="btn btn-default" id="button_cancel" value="<spring:message code="label_cancel" />" /> -->
                <button type="button" id="button_save" class="btn btn-primary"><spring:message code="label_save" /></button>
                <button type="button" id="button_cancel" data-dismiss="modal" class="btn btn-default"><spring:message code="label_cancel"/></button>
            </div>
        </div>
    </div>
</div>

<style type="text/css">
    .checkbox-set1 {
      margin-top: -20px;
    }
    
    .fieldwrapper {
    	padding: 4px;
    }
    
    .values {
    	overflow-y: auto;
    	height: 100px;
    	margin-bottom: 0px;
    }
    .modal-body .btn-small {
        padding: 6px 18px;
    }
    .modal-body select {
        margin-right: 10px;
    }
    #memberListTable .modal-body {
        padding: 20px;
    }
    .rightBlock {
        clear: both;
    }
    #field1 {
        float: left;
    }
    #DataTables_Table_1.table tbody tr td:last-child {
        border-left: none;
    }
</style>

<script>
  var valueFieldMap = new Object();
  var fields = new Object();
  var operands = new Object();
  <c:forEach items="${groupFields}" var="field">
    <c:set var="fieldValues" value="${fieldValueMap[field.code]}"/>
    var values = new Object();
    <c:forEach items="${fieldValues}" var="value">
    	values["${value.code}"] = "${value.description}";
    </c:forEach>
    fields["${field.code}"] = "${field.description}";
    valueFieldMap["${field.code}"] = values;
  </c:forEach>

  <c:forEach items="${operands}" var="operand">
  operands["${operand.code}"] = "${operand.description}";
  </c:forEach>
</script>
<script src='<c:url value="/js/bootstrap/bootstrap-datepicker.js"/>'></script>
<link href="<c:url value="/css/bootstrap/datepicker.css" />" rel="stylesheet" />
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/viewspecific/groups/membergroup.js"><![CDATA[&nbsp;]]></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/viewspecific/groups/rfsstore.js"><![CDATA[&nbsp;]]></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/viewspecific/groups/points.js"><![CDATA[&nbsp;]]></script>

<script>setGlobalAttributes(fields, valueFieldMap, operands);</script>