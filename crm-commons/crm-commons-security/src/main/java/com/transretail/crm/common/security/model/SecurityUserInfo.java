/*
 * Copyright (c) 2012.
 * All rights reserved.
 */

package com.transretail.crm.common.security.model;

public interface SecurityUserInfo {

    String getFirstName();

    String getMiddleName();

    String getLastName();

}
