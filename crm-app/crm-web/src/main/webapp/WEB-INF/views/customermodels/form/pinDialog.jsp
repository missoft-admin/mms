<%@ include file="../../common/taglibs.jsp"%>

<div id="updatePinDialog" class="modal hide nofly">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
			  <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4><spring:message code="label_com_transretail_crm_dto_updatepindto" /></h4>
      </div>

            <c:url var="url_action" value="/customermodels" />
            <form:form id="updatePin" modelAttribute="updatePinForm" action="${url_action}">
                <div class="modal-body form-horizontal">
                    <div class="errors alert">
						<button type="button" class="close" onclick="hideErrors();">&times;</button>
						<div class="errorcontent"></div>
					</div>
                    
                    <div class="control-group">
                        <spring:message code="label_com_transretail_crm_dto_updatepindto_pin" var="pinLabel" />
                        <label for="pin" class="control-label"><b class="required">*</b><c:out value="${pinLabel}" /> </label>
                        <div class="controls">
                        	<input type="password" maxlength="6" placeholder="${pinLabel}" name="pin" id="pin" class="form-control intInput"/>
                        </div>
                    </div>
                    
                    <div class="control-group">
                        <spring:message code="label_com_transretail_crm_dto_updatepindto_confirmpin" var="confirmPinLabel" />
                        <label for="confirmPin" class="control-label"><b class="required">*</b><c:out value="${confirmPinLabel}" /> </label>
                        <div class="controls">
                        	<input type="password" maxlength="6" placeholder="${confirmPinLabel}" name="confirmPin" id="confirmPin" class="confirmPin intInput"/>
                    	</div>
                    </div>
                </div>
            </form:form>
            
            <div class="modal-footer container-btn">
                <!--<input type="button" class="btn btn-primary" id="save_pin" value="Save" />
                <input type="button" class="btn btn-default" data-dismiss="modal" value="Cancel" />-->
                <button type="button" id="save_pin" class="btn btn-primary"><spring:message code="label_save" /></button>
                <button type="button" id="formCancel" data-dismiss="modal" class="btn btn-default"><spring:message code="label_cancel"/></button>
            </div>
        </div>
    </div>
</div>

<style type="text/css">
<!--
    .modal-body {
        height: auto;
    }
  
-->
</style>