package com.transretail.crm.common.audit.annotation;

import static java.lang.annotation.ElementType.METHOD;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

import java.lang.annotation.Retention;
import java.lang.annotation.Target;

/**
 * @author Allan G. Ramirez (agramirez@exist.com)
 */
@Target(METHOD)
@Retention(RUNTIME)
public @interface Audited {
    String name() default "";
    String description();
}
