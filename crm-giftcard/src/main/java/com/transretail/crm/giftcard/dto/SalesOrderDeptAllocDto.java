package com.transretail.crm.giftcard.dto;

import java.math.BigDecimal;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.transretail.crm.common.web.converter.CurrencySerializer;
import org.apache.commons.lang3.StringUtils;

import com.google.common.collect.Lists;
import com.transretail.crm.core.entity.lookup.LookupDetail;
import com.transretail.crm.giftcard.entity.SalesOrder;
import com.transretail.crm.giftcard.entity.SalesOrderDeptAlloc;
import com.transretail.crm.giftcard.entity.SalesOrderDeptAlloc.DeptAllocStatus;
import org.springframework.format.annotation.NumberFormat;

public class SalesOrderDeptAllocDto {
	
	private Long id;
	private Long orderId;
	private LookupDetail department;
	private String deptId;
	private BigDecimal allocAmount;
	private DeptAllocStatus status;
	
	public SalesOrderDeptAllocDto(){}
	
	public SalesOrderDeptAllocDto(SalesOrderDeptAlloc alloc) {
		this.id = alloc.getId();
		if(alloc.getOrder() != null) {
			this.orderId = alloc.getOrder().getId();
		}
		this.department = alloc.getDepartment();
		this.allocAmount = alloc.getAllocAmount();
		this.status = alloc.getStatus();
	}
	
	public SalesOrderDeptAlloc toModel(SalesOrderDeptAlloc alloc) {
		if(alloc == null)
			alloc = new SalesOrderDeptAlloc();
		
		if(this.orderId != null)
			alloc.setOrder(new SalesOrder(this.orderId));
		if ( null != this.department
				&& StringUtils.isNotBlank( this.department.getCode() ) 
				&& StringUtils.isNotBlank( this.department.getDescription() ) ) {
			alloc.setDepartment(this.department);
		}
		alloc.setAllocAmount(this.allocAmount);
		alloc.setStatus(this.status);
		return alloc;
	}
	
	public static List<SalesOrderDeptAllocDto> toDtos(Iterable<SalesOrderDeptAlloc> allocs) {
		List<SalesOrderDeptAllocDto> allocDtos = Lists.newArrayList();
		for(SalesOrderDeptAlloc alloc: allocs) {
			allocDtos.add(new SalesOrderDeptAllocDto(alloc));
		}
		return allocDtos;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getOrderId() {
		return orderId;
	}

	public void setOrderId(Long orderId) {
		this.orderId = orderId;
	}

	public LookupDetail getDepartment() {
		return department;
	}

	public void setDepartment(LookupDetail department) {
		this.department = department;
	}
	
	public String getDeptId() {
		if(this.department != null) {
			if ( StringUtils.isNotBlank( this.department.getCode() ) ) {
				this.deptId = this.department.getCode().substring(3);
			}
			return this.deptId;
		}
		return "";
	}
	
	public String getDeptCodeFromId() {
		if(StringUtils.isNotBlank(this.deptId)) {
			return SalesOrderDto.DEPT_PREFIX + this.deptId;
		}
		return "";
	}
	
	public void setDeptId(String deptId) {
		this.deptId = deptId;
		if(StringUtils.isNotBlank(this.deptId))
			this.department = new LookupDetail(getDeptCodeFromId());
	}

	@NumberFormat(style = NumberFormat.Style.NUMBER, pattern = "#,###,###.##")
	public BigDecimal getAllocAmount() {
		return allocAmount;
	}

	public void setAllocAmount(BigDecimal allocAmount) {
		this.allocAmount = allocAmount;
	}

	public DeptAllocStatus getStatus() {
		return status;
	}

	public void setStatus(DeptAllocStatus status) {
		this.status = status;
	}
	
	

}
