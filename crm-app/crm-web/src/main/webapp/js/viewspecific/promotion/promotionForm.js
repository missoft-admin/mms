$(document).ready( function() {

	var theErrorContainer = $( "#content_promotionForm" ).find( "#content_error" );

	var $form = $( "#promotionForm" );
	var $program = $form.find( "#program" );
	var $campaign = $form.find( "#campaign" );
	var $theDuration = $form.find( "#duration" );
	var $theStartDate = $form.find( "#startDate" );
	var $theEndDate = $form.find( "#endDate" );
	var $theStartTime = $form.find( '#startTime' );
	var $theEndTime = $form.find( '#endTime' );
	var $theRewardType = $form.find( "#rewardType" );

	var $link_promotionReward = $form.find( "#link_promotionReward" );
	var $created = $form.find( "#content_created" );



	initLinks();
	initFormFields();



	function initLinks() {
		
		$('input[data-loading-text]').click(function () {
		    $(this).button("loading");
		    $(this).siblings().attr("disabled", "disabled").addClass('btn-disabled');
		});

		var $link_promotionProgram = $( "#link_promotionProgram" );
		$link_promotionProgram.click( function(e) {
			e.preventDefault();
			createProgram( $(this).data("url") );
		});

		var $link_promotionCampaign = $( "#link_promotionCampaign" );
		$link_promotionCampaign.click( function(e) {
			e.preventDefault();
			createCampaign( $(this).data("url") );
		});

		$link_promotionReward.click( function(e) {
			e.preventDefault();
			if ( undefined == $theRewardType.val() 
					|| null == $theRewardType.val() 
					|| "" == $theRewardType.val() ) {
				return;
			}
			createReward( $( this ).data("url") + $theRewardType.val() + "/" + $( ".promotionRewardsEntry" ).length, $(this) );
			/*var newName = $( "input[name$='" +  ($( "input[name$='].minQty']" ).length - 1)  + "].minQty']" ).attr("name");
			$( "input[name$='" +  ($( "input[name$='].minQty']" ).length - 1)  + "].minQty']" ).attr("name", newName.replace(/\[.*?\]/, "[1]"));*/			
		});
		
		$(".link_promotionReward").click( function(e) {
			e.preventDefault();
			var rewardType = $(this).data("type");
			var $parent = $(this).closest(".rewardContainer");
			var $promotionRewardsEntry = $parent.find(".promotionRewardsEntry");
			if ( $promotionRewardsEntry.length > 0 ) {
				createReward( $( this ).data("url") + $(this).data( "type" ) + "/" + $promotionRewardsEntry.length, $(this) );
			}
			else {
				listRewards( $( this ).data("list-url") + "" + $( this ).data( "type" ), $(this) );
			}
		});

		if ( $form.find( "#link_redemptionPromoReward" ).length > 0 ) {
			var $link_redemptionPromoReward = $form.find( "#link_redemptionPromoReward" );
			$link_redemptionPromoReward.click( function(e) {
				e.preventDefault();
				if ( $( ".promotionRewardsEntry" ).length > 0 ) {
					createReward( $( this ).data("url") + $link_redemptionPromoReward.data( "type" ) + "/" + $( ".promotionRewardsEntry" ).length, $(this) );
				}
				else {
					listRewards( $( this ).data("list-url") + "" + $( this ).data( "type" ), $(this) );
				}
			});
		}

		var $theCancelLink = $( "#promotionForm_cancelButton" );
		$theCancelLink.click( function(e) {
			window.location.href = $form.data( "url" );
		});
	}

	function initFormFields() {

		$form.submit( function(e) {
			e.preventDefault();
			if ( !$theStartTime.val() ) {
				$theStartTime.val( "00:00:00" );
			}
			if ( !$theEndTime.val() ) {
				$theEndTime.val( "00:00:00" );
			}
			/*$theAffectedDays.val( $( "#content_promotionDays > input:checked[type='checkbox']" ).map( function() { return $(this).val(); }).toArray().toString() );*/
			$( "#targetProductGroups" ).val( $( "#targetProductGroups" ).find( "option" ).map( function(){ return this.value; } ).get() );
			savePromotionPost( $(this).attr("action") );
		});

		setNumericFields();
		toggleSelectAllDays();

		$("#selectAllDays").click(function() {
			var selected = this.checked;
			$("input[name='promotionDays']").each(function() {this.checked = selected;});
		});
		$("input[name='promotionDays']").click(toggleSelectAllDays);

		$theStartTime.timepicker({ showMeridian : false, showSeconds : true});
		$theStartTime.val( "00:00:01" );
		$theStartTime.timepicker().on( 'changeTime.timepicker', function(e) {
			if ( !$theStartTime.val() ) {
				$theStartTime.val( "00:00:01" );
			}
		});
		$theEndTime.timepicker({ showMeridian : false, showSeconds	: true, defaultTime	: "23:59:59" });
		$theEndTime.timepicker().on( 'changeTime.timepicker', function(e) {
			if ( !$theEndTime.val() ) {
				$theEndTime.val( "00:00:01" );
			}
		});

		$theDuration.datepicker({ autoclose	: true, format : "dd M yyyy" });
		//$theStartDate.datepicker( "setDate", ( theMinDate )? new Date( theMinDate ) : null );
		//$theEndDate.datepicker( "setDate", ( theMaxDate )? new Date( theMaxDate ) : null );

		$campaign.change( function(e) {
			e.preventDefault();
			setDateDuration( $campaign.find(':selected').data("min-date"), $campaign.find(':selected').data("max-date") );
		}).change();

		$program.change( function(e) {
			e.preventDefault();
			if ( $( this ).val() != undefined && $( this ).val() != null ) {
				listCampaigns( $(this).data("url") + "" + $( this ).val() );
			}
		});

		$theRewardType.change( function(e) {
			e.preventDefault();
			if ( undefined == $theRewardType.val() 
					|| null == $theRewardType.val() 
					|| "" == $theRewardType.val() ) {
				$("#list_promotionRewards").html( "" );
				$(this).prop( "disabled", false );
				$link_promotionReward.hide();
				return;
			}
			$link_promotionReward.show();
			listRewards( $( this ).data("url") + "" + $( this ).val(), $link_promotionReward );
		});

		initItemPointReward();
		initPromotionRemarks();

		if ( $created.html() ) {
			$created.html( new Date( $created.html() -0  ).customize( 2 ) );
		}
	}

	function initItemPointReward() {
		var ITEMPOINT_CODE = null, ITEMPOINT_DESC = null;
		if ( $( "#targetProductGroups" ).data( "url" ) ) {
			$.get( $( "#targetProductGroups" ).data( "url" ), function(resp) {
				if ( resp.success ) {
					ITEMPOINT_CODE = resp.result.code;
					ITEMPOINT_DESC = resp.result.description;
				}
			}).done( function(e) { $( "#targetProductGroups" ).change(); });
		}

		$( "#targetProductGroups" ).change( function(e) {
			var rewardTypes = $theRewardType.find( "option" ).map( function(){ return this.value; } ).get();
			if ( rewardTypes.indexOf( "" + ITEMPOINT_CODE ) == -1 ) {
				if ( $(this).val() ) {
					$theRewardType.append( new Option( ITEMPOINT_DESC, ITEMPOINT_CODE ) );
				}
			}
			else {
				if ( !$(this).find( "option" ).length ) {
					$( "#rewardType option[value='" + ITEMPOINT_CODE + "']" ).remove();
				}
			}
			if ( undefined == $theRewardType.val() || null == $theRewardType.val() || "" == $theRewardType.val() ) {
				$theRewardType.change();
			}
		});
	}

	function initPromotionRemarks() {
		$( ".promotionStatus" ).each( function( idx, thePromotionStatus ) {
			$( this ).click( function(e) {
				$( "#remarksStatus" ).val( $(this).data( "value" ) );
			});
		});
	}



	function savePromotionPost( inUrl ) {
		$( "#promotionForm_saveButton" ).prop( "disabled", true );
		$.ajax({
			cache 		: false, 
			dataType 	: "json",
			type 		: "POST", 
			url 		: inUrl,
			data		: $form.serialize(),
			error 		: function(jqXHR, textStatus, errorThrown) {
				$( "#promotionForm_saveButton" ).prop( "disabled", false );
				theErrorContainer.find( "div" ).html( errorThrown );
				theErrorContainer.show( "slow" );				
				$(".form-actions input").button("reset").removeAttr("disabled").removeClass('btn-disabled');
			},
			success 	: function( inResponse ) {
				if ( inResponse.success ) {
					window.location.href = $form.data( "url" );
//					$.notification.send($.modelType.promotion);
				}
				else {
					$( "#promotionForm_saveButton" ).prop( "disabled", false );
					var theErrorInfo = "";
					$.each( inResponse.result, function( key, value ) {
						theErrorInfo += ( ( key + 1 ) + ". " + value.code + "<br>" );
					});
					theErrorContainer.find( "div" ).html( theErrorInfo );
					theErrorContainer.show( "slow" );
					
					$(".form-actions input").button("reset").removeAttr("disabled").removeClass('btn-disabled');
				}
			}
		});
	}

	function createProgram( inUrl ) {
		$( "#content_programForm" ).modal( "show" );
	}

	function createCampaign( inUrl ) {
		if ( $program.val() == undefined || $program.val() == null ) {
			return;
		}
		$.ajax({
			cache 		: false, 
			dataType 	: "html",
			type 		: "GET", 
			url 		: inUrl,
			error 		: function(jqXHR, textStatus, errorThrown) {}, 
			success 	: function( inResponse ) {
				$("#form_promotionCampaign").html( inResponse );
			}
		});
	}

	function createReward( inUrl, e ) {
		$.ajax({
			cache 		: false, 
			dataType 	: "html",
			type 		: "GET",
			url 		: inUrl, 
			error 		: function(jqXHR, textStatus, errorThrown) {}, 
			success 	: function( inResponse ) {
				
				$(e).closest(".rewardContainer").find("#promotionRewardsEntries").append( inResponse );
				setNumericFields();
			}
		});
	}

	function listRewards( inUrl, e ) {
		$.ajax({
			cache 		: false, 
			dataType 	: "html",
			type 		: "GET",
			url 		: inUrl, 
			error 		: function(jqXHR, textStatus, errorThrown) {}, 
			success 	: function( inResponse ) {
				$(e).closest(".rewardContainer").find("#list_promotionRewards").html( inResponse );
				setNumericFields();
			}
		});
	}
	
	function setNumericFields() {
		$(".floatInput").numberInput({ "type" : "float" });
		$(".intInput").numberInput({ "type" : "int" });
		$(".wholeInput").numberInput({ "type" : "whole" });
	}

	function listCampaigns( inUrl ) {
		$.ajax({
			cache 		: false,
			type 		: "GET", 
			url 		: inUrl,
			error 		: function(jqXHR, textStatus, errorThrown) {},
			success 	: function( inResponse ) {
				if ( inResponse.success ) {
					updateCampaignList( inResponse.result );
				}
			}
		});
	}

	function updateCampaignList( inData ) {
		$campaign.empty();
		$.each( inData, function( key, value ) {
			$campaign.append(
				"<option value='" + value.id + "' " +
					"data-min-date='" + value.startDate + "' " +
					"data-max-date='" + value.endDate + "' >" + value.name + "</option>");
		});
		$campaign.change();
	}

	function setDateDuration( inMinDate, inMaxDate ) {
		if ( $campaign.data( "setDate" ) ) {
			$theStartDate.datepicker( "setDate", (  null != inMinDate && undefined != inMinDate )? new Date( inMinDate - 0 ) : "" );
			$theEndDate.datepicker( "setDate", ( null != inMaxDate && undefined != inMaxDate )? new Date( inMaxDate - 0 ) : "" );
			$campaign.data( "setDate", "" );
		}
		$theStartDate.datepicker( "setStartDate", ( null != inMinDate && undefined != inMinDate )? new Date( inMinDate - 0 ) : "" );
		$theStartDate.datepicker( "setEndDate", ( null != inMaxDate && undefined != inMaxDate )? new Date( inMaxDate - 0 ) : "" );
		$theStartDate.on( "changeDate", function(e) {
			$theEndDate.datepicker( "setStartDate", $( this ).datepicker( "getDate") );
		});

		$theEndDate.datepicker( "setStartDate", ( null != inMinDate && undefined != inMinDate )? new Date( inMinDate - 0 ) : "" );
		$theEndDate.datepicker( "setEndDate", ( null != inMaxDate && undefined != inMaxDate )? new Date( inMaxDate - 0 ) : "" );
	}

	function toggleSelectAllDays() {
		if($("input[name='promotionDays']:checked").length == $("input[name='promotionDays']").length) {
			$("#selectAllDays").prop("checked", true);
		} else {
			$("#selectAllDays").prop("checked", false);
		}
	}
});




