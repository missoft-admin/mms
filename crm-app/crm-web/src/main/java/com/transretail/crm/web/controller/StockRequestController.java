package com.transretail.crm.web.controller;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.BooleanUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;
import org.apache.commons.lang3.tuple.Pair;
import org.joda.time.LocalDate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.transretail.crm.common.reporting.jasper.JRProcessor;
import com.transretail.crm.common.service.exception.MessageSourceResolvableException;
import com.transretail.crm.common.web.notification.NotificationType;
import com.transretail.crm.common.web.notification.annotation.Notifiable;
import com.transretail.crm.common.web.notification.annotation.NotificationUpdate;
import com.transretail.crm.core.dto.MessageDto;
import com.transretail.crm.core.entity.lookup.LookupDetail;
import com.transretail.crm.core.entity.lookup.Store;
import com.transretail.crm.core.security.util.UserUtil;
import com.transretail.crm.core.service.CodePropertiesService;
import com.transretail.crm.core.service.LookupService;
import com.transretail.crm.core.service.StoreService;
import com.transretail.crm.core.service.UserService;
import com.transretail.crm.giftcard.dto.GiftCardInventoryDto;
import com.transretail.crm.giftcard.dto.GiftCardInventorySeriesSearchDto;
import com.transretail.crm.giftcard.dto.MultipleStockRequestDto;
import com.transretail.crm.giftcard.dto.ReserveGcDto;
import com.transretail.crm.giftcard.dto.StockRequestDto;
import com.transretail.crm.giftcard.dto.StockRequestReceiveDto;
import com.transretail.crm.giftcard.dto.StockRequestResultList;
import com.transretail.crm.giftcard.dto.StockRequestSearchDto;
import com.transretail.crm.giftcard.entity.GiftCardInventory;
import com.transretail.crm.giftcard.entity.support.StockRequestStatus;
import com.transretail.crm.giftcard.service.GiftCardInventoryService;
import com.transretail.crm.giftcard.service.ProductProfileService;
import com.transretail.crm.giftcard.service.StockRequestService;
import com.transretail.crm.media.service.MailService;
import com.transretail.crm.report.template.impl.GiftCardTransferDocument;
import com.transretail.crm.report.template.impl.StockRequestListDocument;
import com.transretail.crm.web.controller.util.ControllerResponse;

@Controller
@RequestMapping(value="/stockrequest")
public class StockRequestController extends AbstractReportsController {
    
    @Autowired
    GiftCardTransferDocument giftCardTransferDocument;
    @Autowired
    StockRequestListDocument stockRequestListDocument;
	@Autowired
	private StockRequestService stockRequestService;
	@Autowired
	private GiftCardInventoryService giftCardInventoryService;
	@Autowired
	private StoreService storeService;
	@Autowired 
	private ProductProfileService productProfileService;
	@Autowired
	private CodePropertiesService codePropertiesService;
	@Autowired
	private LookupService lookupService;
	@Autowired
	private MessageSource messageSource;
	@Autowired
	private UserService userService;
	@Autowired
	private MailService mailService;
	
	private static final Logger LOGGER = LoggerFactory.getLogger(StockRequestController.class);
	
	private static final String MAIN_PAGE = "stockrequest/list";
	private static final String UPDATE_PAGE = "stockrequest/update";
	private static final String APPROVE_PAGE = "stockrequest/approve";
	private static final String VIEW_PAGE = "stockrequest/view";
	private static final String TRANSFER_IN_PAGE = "stockrequest/transferin";
	private static final String RESERVE_GC_PAGE = "stockrequest/reservegc";
	
	private static final String ATTR_MODEL = "stockrequest";
	private static final String ATTR_STORES = "stores";
	private static final String ATTR_CARDTYPES = "cardTypes";
	private static final String ATTR_SOURCELOCATION = "sourceLocations";
	private static final String ATTR_IS_HEAD_OFFICE = "isHeadOffice";
	private static final String ATTR_FORAPPROVAL = "forApproval";
	private final static String ATTR_STATUS = "statuses";
	private final static String ATTR_SEARCH = "searchcateg";
	private final static String ATTR_RESERVE_LIST="reservelist";
	private final static String LIST_SEPARATOR = ",";
	
	private final static Map<String, String> SEARCH_CATEGORIES;
	
	static {
		SEARCH_CATEGORIES = Maps.newHashMap();
		SEARCH_CATEGORIES.put("requestNo", "Request No");
		SEARCH_CATEGORIES.put("requestBy", "Requested By");
	}
	
	@Transactional(readOnly=true)
	private Map<String, String> getInventoryLocations() {
		Map<String, String> inv = Maps.newLinkedHashMap();

		LookupDetail headOffice = lookupService.getDetailByCode(codePropertiesService.getDetailInvLocationHeadOffice());
		inv.put(headOffice.getCode(), headOffice.getCode() + " - " + headOffice.getDescription());
		for(Store store: storeService.getAllStores()) {
			inv.put(store.getCode(), store.getCode() + " - " + store.getName());
		}

		List<Map.Entry<String, String>> entries =
		        new ArrayList<Map.Entry<String, String>>(inv.entrySet());
        Collections.sort(entries, new Comparator<Map.Entry<String, String>>() {
          public int compare(Map.Entry<String, String> a, Map.Entry<String, String> b){
            return a.getValue().compareTo(b.getValue());
          }
        });
        
        Map<String, String> sortedMap = new LinkedHashMap<String, String>();
        for (Map.Entry<String, String> entry : entries) {
          sortedMap.put(entry.getKey(), entry.getValue());
        }

		return sortedMap;
	}
	
	@RequestMapping(produces="text/html")
	public String list(Model uiModel) {
		uiModel.addAttribute(ATTR_SEARCH, SEARCH_CATEGORIES);
		String inventoryLocation = UserUtil.getCurrentUser().getInventoryLocation();
		List<String> statuses = new ArrayList<String>();
		for (StockRequestStatus status : StockRequestStatus.values()) {
		    statuses.add(status.name());
		}
		
        if (inventoryLocation == null || !inventoryLocation.equals(codePropertiesService.getDetailInvLocationHeadOffice())) {
            uiModel.addAttribute(ATTR_IS_HEAD_OFFICE, false);
            statuses.remove("TRANSFERRED_OUT");
        } else if(inventoryLocation.equals(codePropertiesService.getDetailInvLocationHeadOffice())) {
            uiModel.addAttribute(ATTR_IS_HEAD_OFFICE, true);
            statuses.remove("TRANSFERRED_IN");
        }
        uiModel.addAttribute(ATTR_STATUS, statuses);
		return MAIN_PAGE;
	}
	
	@RequestMapping(value="/{requestNo}")
	public String list(@PathVariable("requestNo") String requestNo, Model uiModel) {
		uiModel.addAttribute("id", requestNo);
		return list(uiModel);
	}
	
	@RequestMapping(value="/new", method=RequestMethod.POST, produces="application/json")
	@ResponseBody
	@Notifiable(type=NotificationType.STOCK_REQUEST)
	public ControllerResponse create(@ModelAttribute(ATTR_MODEL) MultipleStockRequestDto dto,
			BindingResult result, Model uiModel, HttpServletRequest request) {
		ControllerResponse response = new ControllerResponse();
		response.setSuccess(true);
		Map<String, List<String>> allocate = Maps.newHashMap();
		List<String> errors = Lists.newArrayList();
		
		for(StockRequestDto stockRequest : dto.getRequests()) {
			String allocateTo = stockRequest.getAllocateTo();
			String productCode = stockRequest.getProductCode();
			
			if(StringUtils.isBlank(allocateTo) || StringUtils.isBlank(productCode)) {
				response.setSuccess(false);
				errors.add(messageSource.getMessage("gc.stock.error.blank.fields", 
						null, LocaleContextHolder.getLocale()));
				break;
			}
			
			if(allocate.get(allocateTo) == null || !allocate.get(allocateTo).contains(productCode)) {
				if(allocate.get(stockRequest.getAllocateTo()) == null) {
					allocate.put(allocateTo, Lists.newArrayList(productCode));
				}
				else {
					allocate.get(allocateTo).add(productCode);
				}

				stockRequest.setStatus(StockRequestStatus.FORAPPROVAL);
				
				if(stockRequest.getQuantity() == null || stockRequest.getQuantity() <= 0) {
					response.setSuccess(false);
					errors.add(messageSource.getMessage("gc.stock.error.quanity", 
							null, LocaleContextHolder.getLocale()));
					break;
				}
			}
			else {
				response.setSuccess(false);
				errors.add(messageSource.getMessage("gc.stock.error.duplicate", 
						null, LocaleContextHolder.getLocale()));
				break;
			}
		}
		
		if(response.isSuccess()) {
			response.setModelId(dto.getRequestNo());
			response.setNotificationCreation(true);
			response.setResult(dto.getRequestNo());
			for(StockRequestDto stockRequest : dto.getRequests()) {
				stockRequestService.saveStockRequest(stockRequest);
				notifyHeadForApproval(dto.getRequestNo());
			}
		}
		else {
			response.setResult(errors);
		}
		
		return response;
	}
	
	private void notifyHeadForApproval(String requestNo) {
		List<String> rcpts = userService.listSupervisorEmails(codePropertiesService.getMerchantServiceSupervisorCode());
    	MessageDto msg = new MessageDto();
    	msg.setRecipients(rcpts.toArray(new String[rcpts.size()]));
    	msg.setMessage( messageSource.getMessage( "label_notification_stock_request", new String[]{requestNo}, LocaleContextHolder.getLocale() ) );
    	msg.setSubject( messageSource.getMessage( "label_notification_stock_request", new String[]{requestNo}, LocaleContextHolder.getLocale() ) );
    	mailService.send(msg);	
    }
	
	@RequestMapping(value="/status/{status}", method=RequestMethod.POST, produces="application/json")
	@ResponseBody
	@NotificationUpdate(type=NotificationType.STOCK_REQUEST)
	public ControllerResponse changeStatus(@PathVariable("status") String status, 
			@ModelAttribute(ATTR_MODEL) StockRequestDto dto) {
		ControllerResponse response = new ControllerResponse();
		
		StockRequestStatus newStatus = StockRequestStatus.valueOf(status);
		dto.setStatus(newStatus);
		Long id = stockRequestService.updateStockRequest(dto.getId(), dto);
		response.setSuccess(id != null);
		if(!stockRequestService.hasUnapprovedRequests(dto.getRequestNo())) {
			response.setModelId(dto.getRequestNo());
			response.setNotificationCreation(false);
		}
		
		return response;
	}
	
	@RequestMapping(value="/save", method=RequestMethod.POST, produces="application/json")
	@ResponseBody
	public ControllerResponse save(@ModelAttribute(ATTR_MODEL) StockRequestDto dto,
			BindingResult result, Model uiModel,
			HttpServletRequest request) {
		ControllerResponse response = new ControllerResponse();
		
		if(response.isSuccess()) {
			stockRequestService.updateStockRequest(dto.getId(), dto);
		}
		
		return response;
	}
	
	@RequestMapping(value="/search/{requestNo}", method=RequestMethod.POST)
	@ResponseBody
	public StockRequestResultList search(@PathVariable String requestNo, @RequestBody StockRequestSearchDto searchDto) {
		if(StringUtils.isNotBlank(requestNo)) {
			searchDto.setRequestNo(requestNo);
			searchDto.setStatus(Lists.newArrayList(StockRequestStatus.FORAPPROVAL));
		}
		
		return stockRequestService.getStockRequests(searchDto);
	}
	
	@RequestMapping(value="/search", method=RequestMethod.POST)
	@ResponseBody
	public StockRequestResultList search(@RequestBody StockRequestSearchDto searchDto) {
//		if(searchDto.getSingleStatus() == null) {
//			searchDto.setStatus(Lists.newArrayList(StockRequestStatus.FORAPPROVAL, StockRequestStatus.APPROVED, StockRequestStatus.IN_TRANSIT));
//		}
		return stockRequestService.getStockRequests(searchDto);
	}
	
	@RequestMapping(value="/edit/{requestNo}", produces="text/html")
	public String editDialog(@PathVariable String requestNo, Model uiModel) {
		List<StockRequestDto> requests = stockRequestService.findStockRequestByRequestNo(requestNo);
		uiModel.addAttribute(ATTR_MODEL, new MultipleStockRequestDto(requestNo, requests));
		return showUpdateDialog(uiModel);
	}
	
	@RequestMapping(value="/create", produces="text/html")
	public String createDialog(Model uiModel) {
		List<StockRequestDto> requests = Lists.newArrayList(new StockRequestDto());
		
		MultipleStockRequestDto dto = new MultipleStockRequestDto(
				stockRequestService.createStockRequestNumber(), requests);
		dto.setRequestDate(LocalDate.now());
		
		String inventoryLocation = UserUtil.getCurrentUser().getInventoryLocation();
		if(inventoryLocation.equals(codePropertiesService.getDetailInvLocationHeadOffice())) {
			uiModel.addAttribute(ATTR_IS_HEAD_OFFICE, true);
		}
		else {
			uiModel.addAttribute(ATTR_IS_HEAD_OFFICE, false);
			dto.setInventoryLocation(inventoryLocation);
		}
		
		uiModel.addAttribute(ATTR_MODEL, dto);
		return showUpdateDialog(uiModel);
	}
	
	@RequestMapping(value="/view/{id}", produces="text/html")
	public String viewDialog(@PathVariable("id") String id, Model uiModel) {
		StockRequestDto dto = stockRequestService.getStockRequest(Long.valueOf(id));
		uiModel.addAttribute(ATTR_MODEL, dto);
		LOGGER.debug("StockRequestController calls codePropertiesService.getDetailInvLocationHeadOffice");
		uiModel.addAttribute(ATTR_FORAPPROVAL, dto.getStatus() == StockRequestStatus.FORAPPROVAL
				&& UserUtil.getCurrentUser().getInventoryLocation().equals(
						codePropertiesService.getDetailInvLocationHeadOffice()));
		return VIEW_PAGE;
	}
	
	@RequestMapping(value="/reserve/gc/{id}", produces="text/html")
	public String reserveGcDialog(@PathVariable("id") Long id, Model uiModel) {
		StockRequestDto stockRequestDto = stockRequestService.getStockRequest(id);
		
		List<StockRequestReceiveDto> dtoList = Lists.newArrayList();
		ReserveGcDto dto = null;
		
		if(!stockRequestDto.getReservedSeriesList().isEmpty()) {
			LOGGER.error(stockRequestDto.getReservedSeriesList().toString());
			for(Pair<String, String> pair : stockRequestDto.getReservedSeriesList()) {
				StockRequestReceiveDto reserveDto = new StockRequestReceiveDto();
				reserveDto.setStartingSeries(Long.valueOf(pair.getLeft()));
				reserveDto.setEndingSeries(Long.valueOf(pair.getRight()));
				reserveDto.setQuantity((int)(reserveDto.getEndingSeries() - reserveDto.getStartingSeries() + 1));
				reserveDto.setProductCode(stockRequestDto.getProductCode());
				reserveDto.setProductDesc(stockRequestDto.getProductDescription() + " - " + stockRequestDto.getProductCode());
				dtoList.add(reserveDto);
			}
			dto = new ReserveGcDto(dtoList, id);
		}
		else {
			dto = new ReserveGcDto(dtoList, id);
			dtoList.add(new StockRequestReceiveDto());
		}
		
		uiModel.addAttribute(ATTR_RESERVE_LIST, dto);
		uiModel.addAttribute("id", id);
		return RESERVE_GC_PAGE;
	}
	
	@RequestMapping(value="/transferin/{id}", produces="text/html")
    public String transferInDialog(@PathVariable Long id,
            @ModelAttribute(value = "stockrequest") StockRequestDto stockrequest,
            Model uiModel) {
	    LOGGER.debug("StockRequestController calls giftCardInventoryService.getStockRequest");
	    StockRequestDto stock = stockRequestService.getStockRequest(id);
	    stock.setReceiveBy(UserUtil.getCurrentUser().getUsername());
	    List<StockRequestReceiveDto> list = new ArrayList<StockRequestReceiveDto>();
	    list.add(new StockRequestReceiveDto());
	    stock.setReceiveDtos(list);
        uiModel.addAttribute(ATTR_MODEL, stock);
        
        return TRANSFER_IN_PAGE;
    }
	
	@RequestMapping(value="/validate/{startingSeries}/{endingSeries}") 
	public @ResponseBody ControllerResponse getProduct(
	        @PathVariable Long startingSeries,
	        @PathVariable Long endingSeries,
	        @ModelAttribute("stockrequest") StockRequestDto stockrequest,
            BindingResult bindingResult) {
	    
	    ControllerResponse response = new ControllerResponse();
	    response.setSuccess(true);
	    GiftCardInventorySeriesSearchDto searchDto = new GiftCardInventorySeriesSearchDto();
	    searchDto.setSeriesFrom(startingSeries);
	    searchDto.setSeriesTo(endingSeries);
	    
	    StockRequestDto stockRequestDto = stockRequestService.getStockRequest(stockrequest.getId());
	    stockRequestDto.setReceiveDtos(stockrequest.getReceiveDtos());
	    
	    if (bindingResult.hasErrors()) {
	        response.setSuccess(false);
	        response.setResult(bindingResult.getAllErrors());
	    } else {
	        GiftCardInventoryDto gcDto = null;
            try {
                LOGGER.debug("StockRequestController calls giftCardInventoryService.getGiftCardSeriesProductForTransferIn");
                gcDto = giftCardInventoryService.getGiftCardSeriesProductForTransferIn(searchDto, stockRequestDto);
                if (gcDto != null) {
                    response.setResult(gcDto);
                }
            } catch (MessageSourceResolvableException e) {
                LOGGER.debug(e.getMessage());
                response.setSuccess(false);
                response.setResult(Lists.newArrayList(messageSource.getMessage(e, LocaleContextHolder.getLocale())));
            }
	    }
        return response;
	}
	
	@RequestMapping(value="/transferin/save", produces="application/json; charset=utf-8", method=RequestMethod.POST)
    public @ResponseBody ControllerResponse transferInSave(@ModelAttribute StockRequestDto stockrequest,
            BindingResult bindingResult) {
        
	    ControllerResponse response = new ControllerResponse();
	    response.setSuccess(true);
	    
	    if (bindingResult.hasErrors()) {
	        response.setSuccess(false);
	        response.setResult(bindingResult.getAllErrors());
	    } else {
	        try {
	            LOGGER.debug("StockRequestController calls stockRequestService.transferInStocks");
                stockRequestService.transferInStocks(stockrequest);
            } catch (MessageSourceResolvableException e) {
                LOGGER.debug(e.getMessage());
                response.setSuccess(false);
                response.setResult(Lists.newArrayList(messageSource.getMessage(e, LocaleContextHolder.getLocale())));
            }
	    }
        return response;
    }
	
	@RequestMapping(value="/approve", produces="text/html")
	public String approveDialog(Model uiModel) {
		uiModel.addAttribute(ATTR_FORAPPROVAL, true);
		return VIEW_PAGE;
	}
	
	private String showUpdateDialog(Model uiModel) {
		uiModel.addAttribute(ATTR_CARDTYPES, productProfileService.findAllDtos());
		
		LOGGER.debug("StockRequestController calls codePropertiesService.getDetailInvLocationHeadOffice");
		if(UserUtil.getCurrentUser().getInventoryLocation().equals(codePropertiesService.getDetailInvLocationHeadOffice())) {
			uiModel.addAttribute(ATTR_STORES, storeService.getAllStores());
			uiModel.addAttribute(ATTR_SOURCELOCATION, getInventoryLocations());
			uiModel.addAttribute(ATTR_IS_HEAD_OFFICE, true);
		}
		else {
			uiModel.addAttribute(ATTR_STORES, Lists.newArrayList(UserUtil.getCurrentUser().getStore()));
			Map<String, String> map = Maps.newHashMap();
			LOGGER.debug("StockRequestController calls codePropertiesService.getDetailInvLocationHeadOffice");
			LookupDetail ho = lookupService.getDetailByCode(codePropertiesService.getDetailInvLocationHeadOffice());
			map.put(ho.getCode(), ho.getDescription());
			uiModel.addAttribute(ATTR_SOURCELOCATION, map);
			uiModel.addAttribute(ATTR_IS_HEAD_OFFICE, false);
		}
		
		return UPDATE_PAGE;
	}
	
	@RequestMapping(value="/approve/{requestNo}", produces="text/html")
	public String approveDialog(@PathVariable String requestNo, Model uiModel) {
	    LOGGER.debug("StockRequestController calls stockRequestService.findStockRequestByRequestNo");
		List<StockRequestDto> requests = stockRequestService.findStockRequestByRequestNo(requestNo);
		uiModel.addAttribute(ATTR_MODEL, new MultipleStockRequestDto(requestNo, requests));
		return APPROVE_PAGE;
	}
	
	@RequestMapping(value = "/transfer/{id}", produces="application/json; charset=utf-8", method=RequestMethod.POST)
    public @ResponseBody ControllerResponse transferGiftCardStock(
            @PathVariable Long id,
            @ModelAttribute("stockrequest") StockRequestDto dto,
            BindingResult bindingResult) {
        
        ControllerResponse response = new ControllerResponse();
        response.setSuccess(true);
        
        if (bindingResult.hasErrors()) {
            response.setSuccess(false);
            response.setResult(bindingResult.getAllErrors());
        } else {
            LOGGER.debug("StockRequestController calls stockRequestService.transferStockRequest");
            stockRequestService.transferStockRequest(id);
        }
        
        return response;
    }
    
    @RequestMapping(value = "/printtransfer/{id}")
    public void printTransfer(
            @PathVariable Long id,
            @ModelAttribute("stockrequest") StockRequestDto stockrequest,
            HttpServletResponse response) throws Exception {
        
        LOGGER.debug("StockRequestController calls stockRequestService.getStockRequest");
        StockRequestDto stockRequestDto = stockRequestService.getStockRequest(id);
        if (stockrequest != null)
            stockRequestDto.setReceiveDtos(stockrequest.getReceiveDtos());

        LOGGER.debug("StockRequestController calls giftCardTransferDocument.createJrProcessor");
        JRProcessor jrProcessor = giftCardTransferDocument.createJrProcessor(stockRequestDto);
        renderReport(response, jrProcessor);
    }
    
    @RequestMapping( value="/print/validate", produces="application/json; charset=utf-8" )
    public @ResponseBody ControllerResponse printValidate(
            @RequestParam(value = "requestBy", required = false) String requestedBy,
            @RequestParam(value = "requestNo", required = false) String requestNo,
            @RequestParam(value = "singleStatus", required = false) String status,
            @RequestParam(value = "reqDateFrom", required = false) String dateFrom,
            @RequestParam(value = "reqDateTo", required = false) String dateTo,
            Model model) {

        ControllerResponse resp = new ControllerResponse();
        
        Map<String, String> filtersMap = new HashMap<String, String>();
        filtersMap.put("dateFrom", dateFrom);
        filtersMap.put("dateTo", dateTo);
        filtersMap.put("requestedBy", requestedBy);
        filtersMap.put("requestNo", requestNo);
        filtersMap.put("status", status);
        
         if (stockRequestListDocument.isEmpty(filtersMap)) {
             resp.setSuccess(false);
             resp.setResult(Lists.newArrayList(messageSource.getMessage("report.no.data.found", null, LocaleContextHolder.getLocale())));
         }
        
        return resp;
    }
    
    @RequestMapping(value = "/printlist")
    public void printList(@RequestParam(value = "requestBy", required = false) String requestedBy,
                          @RequestParam(value = "requestNo", required = false) String requestNo,
                          @RequestParam(value = "singleStatus", required = false) String status,
                          @RequestParam(value = "reqDateFrom", required = false) String dateFrom,
                          @RequestParam(value = "reqDateTo", required = false) String dateTo,
            HttpServletResponse response) throws Exception {
        
        LOGGER.debug("StockRequestController calls stockRequestListDocument.createJrProcessor");
        JRProcessor jrProcessor = stockRequestListDocument.createJrProcessor(requestedBy, requestNo, status, dateFrom, dateTo);
        renderExcelReport(response, jrProcessor, "Stock Request List Document (" + LocalDate.now() +").xls");
    }

    @RequestMapping("/reprint/{id}")
    public void reprint( @PathVariable Long id, 
	    HttpServletResponse response) throws Exception {
        this.printTransfer(id, null, response);
    }
    
    @RequestMapping(value="/reserve/gc/{id}", method=RequestMethod.POST, produces="application/json")
    @ResponseBody
    public ControllerResponse reserveGiftCard(@PathVariable("id") Long id,
    		@ModelAttribute(ATTR_RESERVE_LIST) ReserveGcDto gcList) {
    	ControllerResponse response = new ControllerResponse();
    	response.setSuccess(true);
    	StockRequestDto dto = stockRequestService.getStockRequest(id);
    	List<String> errors = Lists.newArrayList();
    	
     	List<StockRequestReceiveDto> list = gcList.getGcList();
     	
     	List<String> gcProductMismatch = Lists.newArrayList();
    	
    	for(int i = gcList.getStartIndex(); i < list.size(); i++) {
    		StockRequestReceiveDto reserveGc = list.get(i);
    		if(!reserveGc.getProductCode().equals(dto.getProductCode())) {
    			response.setSuccess(false);
    			errors.add(messageSource.getMessage("gc.reserve.error.product.mismatch", 
    					new Object[] {reserveGc.getStartingSeries() + " - " + reserveGc.getEndingSeries()}, 
    					LocaleContextHolder.getLocale()));
    		}
    	}
    	
    	if (response.isSuccess()){
    		List<String> result = stockRequestService.reserveGiftCardsForTranserOut(list.subList(gcList.getStartIndex(), list.size()), dto);
    		if(result != null) {
    			response.setSuccess(false);
    			errors = result;
    		}
    	}
    	
    	if(!response.isSuccess()) {
    		response.setResult(errors);
    	}
    	else {
    		response.setResult(gcList);
    	}
    	
//    	LOGGER.error(response.getResult() + " ");
//    	LOGGER.error("success: " + response.isSuccess());
    	
    	return response;
    }
    
    @RequestMapping(value="/{id}/gc/{giftCardNo}", method=RequestMethod.POST, produces="application/json")
	@ResponseBody
	public ControllerResponse getGiftCard(@PathVariable("id") Long id, @PathVariable("giftCardNo") String giftCardNo) {
    	
		ControllerResponse response = new ControllerResponse();
		Long series = null;
		
		try {
			series = Long.valueOf(giftCardNo);
		}
		catch(NumberFormatException e) {
			response.setSuccess(false);
			response.setResult(Lists.newArrayList(messageSource.getMessage("gc.reserve.error.invalid.series", 
					new Object[]{giftCardNo}, LocaleContextHolder.getLocale())));
			return response;
		}
		
		GiftCardInventory gcInventory = giftCardInventoryService.getGiftCardBySeries(series);
		response.setSuccess(gcInventory != null);
		if(response.isSuccess()) {
			StockRequestDto dto = stockRequestService.getStockRequest(id);
			List<String> errors = Lists.newArrayList();
			
			if(!gcInventory.getLocation().equals(dto.getSourceLocation())) {
				response.setSuccess(false);
				errors.add(messageSource.getMessage("gc.reserve.error.source.mismatch", 
						new Object[]{giftCardNo}, LocaleContextHolder.getLocale()));
			}
			
			if(gcInventory.getAllocateTo() != null) {
				response.setSuccess(false);
				errors.add(messageSource.getMessage("gc.reserve.error.allocated", 
						new Object[]{giftCardNo}, LocaleContextHolder.getLocale()));
			}

			if ( BooleanUtils.isTrue(gcInventory.getIsEgc()) ) {
				response.setSuccess( false );
				errors.add( messageSource.getMessage( "gc.stock.error.isegc", 
						new Object[]{ giftCardNo }, LocaleContextHolder.getLocale() ) );
			}

			if(!gcInventory.getProductCode().equals(dto.getProductCode())) {
				response.setSuccess(false);
				errors.add(messageSource.getMessage("gc.reserve.error.product.mismatch", 
						new Object[]{giftCardNo}, LocaleContextHolder.getLocale()));
			}
			
			if(response.isSuccess()) {
				response.setResult(new GiftCardInventoryDto(gcInventory));
			}
			else {
				response.setResult(errors);
			}
		}
		else {
			response.setResult(Lists.newArrayList(messageSource.getMessage("gc.reserve.error.not.exist", 
					new Object[]{giftCardNo}, LocaleContextHolder.getLocale())));
		}
		
		return response;
	}

    @RequestMapping( value="/approveall/{ids}", produces="application/json; charset=utf-8", method=RequestMethod.POST )
	@NotificationUpdate(type=NotificationType.STOCK_REQUEST)
	public @ResponseBody ControllerResponse approveAll( @PathVariable(value="ids") String strIds ) {
    	ControllerResponse resp = new ControllerResponse();
    	List<Long> ids = toList( strIds );
		if ( CollectionUtils.isNotEmpty( ids ) ) {
			stockRequestService.updateStatus( ids, StockRequestStatus.APPROVED );
			List<String> approvedReqs = stockRequestService.getApprovedStockNos( ids );
			if ( CollectionUtils.isNotEmpty( approvedReqs ) ) {
				resp.setModelIds( approvedReqs );
			}
			resp.setNotificationCreation(false);
			resp.setSuccess( true );
		}
		return resp;
	}



	private List<Long> toList( String delimIds ) {
		List<Long> ids = new ArrayList<Long>();
		if ( StringUtils.isNotBlank( delimIds ) ) {
			for ( String id : delimIds.indexOf( LIST_SEPARATOR ) != -1? delimIds.split( LIST_SEPARATOR ) : new String[]{ delimIds } ) {
				if ( StringUtils.isNotBlank( id ) && NumberUtils.isDigits( id.trim() ) ) {
					ids.add( Long.valueOf( id ) );
				}
			}
			return ids;
		}
		return ids;
	}
}
