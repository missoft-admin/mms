<%@ include file="../common/taglibs.jsp" %>


<style type="text/css">
<!--
    .control-group-02 {
        width: 277px !important;
    }
    #block_promotionDays.well {
        margin-left: 20px;
    }
    #content_target select {
        width: 202px;
    }
-->
</style>


<div id="content_promotionForm" class="">

  <div class="page-header page-header2"><h1><spring:message code="marketing_promotion" /></h1></div>


  <c:url var="url_listPromotions" value="/promotion"/>
  <c:url var="url_promotionForm" value="${promotionFormAction}"/>
  <form:form id="promotionForm" name="promotion" modelAttribute="promotion" action="${url_promotionForm}" data-url="${url_listPromotions}" class="form-horizontal">
  <fieldset>    

    <div class="content_promo">

      <div class="hide alert alert-error" id="content_error">
          <button type="button" class="close" onclick="$(this).parent().hide();">&times;</button>
          <div><form:errors path="*"/></div>    
      </div>

      <div class="">

        <div class="block-stack-01">
          <div class="pull-left span5">          
            <div id="block_promotionProgram" class="control-group">
              <label for="program" class="control-label"><spring:message code="promotion_prop_program"/></label>
              <div class="controls">
                <form:select path="program" id="program" disabled="true">
                  <c:forEach var="program" items="${promotionPrograms}">
                    <form:option value="${program.id}" label="${program.name}" data-min-date="${program.startDate}" data-max-date="${program.endDate}" />
                  </c:forEach>
                </form:select> 
              </div>
            </div>
    
            <div id="block_promotionCampaign" class="control-group">
              <label for="campaign" class="control-label"><spring:message code="promotion_prop_campaign"/></label>
              <div class="controls">
                <form:select path="campaign" id="campaign" disabled="true">
                  <c:forEach var="campaign" items="${promotionCampaigns}">
                    <form:option value="${campaign.id}" label="${campaign.name}" data-min-date="${campaign.startDate}" data-max-date="${campaign.endDate}" />
                  </c:forEach>
                </form:select>
                <div id="form_promotionCampaign"></div>
              </div>
            </div>
    
            <div id="block_promotionName" class="control-group">
              <label for="name" class="control-label"><spring:message code="promotion_prop_name"/></label>
              <div class="controls">
                <form:input path="name" id="name" disabled="true"/>
                <form:hidden path="id"/>
              </div>
            </div>
    
            <div id="block_promotionDesc" class="control-group">
              <label for="description" class="control-label"><spring:message code="promotion_prop_description"/></label>
              <div class="controls">
                <form:input path="description" id="description" disabled="true"/>
              </div>
            </div>

            <c:if test="${not empty promotion.createUser}">
            <div id="" class="control-group">
              <label for="" class="control-label"><spring:message code="promotion_label_author"/></label>
              <div class="controls"><c:out value="${promotion.createUser}" /> - <c:out value="${promotion.createUserFullName}" /></div>
            </div>
            </c:if>

            <c:if test="${not empty promotion.dispCreated}">
            <div id="" class="control-group">
              <label for="" class="control-label"><spring:message code="promotion_label_createddate"/></label>
              <div class="controls"><span id="content_created">${promotion.dispCreated}</span></div>
            </div>
            </c:if>
          </div>


          <div class="pull-left control-group-01">
	          <div class="pull-left control-group-small" id="block_promotionDuration">
	          <fieldset>
	            <legend class="block-stack-title"><spring:message code="promotion_prop_duration" /></legend>
	    
	            <div class="input-daterange" id="duration">
		            <div class="control-group">
		              <label for="startDate" class="control-label"><spring:message code="promotion_prop_startdate"/></label>
	                <div class="controls"><form:input path="startDate" id="startDate" cssClass="input-space-01 input-small" disabled="true" /></div>
	              </div>
	              <div class="control-group">
	                <label for="endDate" class="control-label"><spring:message code="promotion_prop_enddate"/></label>
	                <div class="controls"><form:input path="endDate" id="endDate" cssClass="input-small" disabled="true" /></div>
	              </div>
	            </div>
	          </fieldset>
	          </div>
	
	          <div class="pull-left control-data-01">
              <div class="pull-left control-group-small">
              <fieldset>
                <legend class="block-stack-title"><spring:message code="reward_label_affecteddaysandtime" /></legend>
        
                <div id="block_promotionTime" class="pull-left">
                  <div class="control-group">
                    <label for="startTime" class="control-label"><spring:message code="promotion_prop_starttime"/></label>
                    <div class="controls"><form:input path="startTime" id="startTime" cssClass="input-small input-space-01" disabled="true" /></div>
                  </div>
                  <div class="control-group">
                    <label for="endTime" class="control-label"><spring:message code="promotion_prop_endtime"/></label>
                    <div class="controls"><form:input path="endTime" id="endTime" cssClass="input-small" disabled="true" /></div>
                  </div>
                </div>
    
                <div class="clearfix"></div>
              </fieldset>
              </div>
      
              <div id="block_promotionDays" class="well pull-left">
                <div class="checkbox">
                    <div id="content_promotionDays" class="">
                      <c:forEach var="promotionDay" items="${promotionAffectedDays}">
                        <div class="capitalize" >
                          <c:if test="${null != promotion.affectedDays}">
                            <form:checkbox path="promotionDays" value="${promotionDay.code}" label="${promotionDay.description}" disabled="true" />
                          </c:if>
                          <c:if test="${null == promotion.affectedDays}">
                            <form:checkbox path="promotionDays" value="${promotionDay.code}" label="${promotionDay.description}" checked="checked" disabled="true" />
                          </c:if>
                        </div>
                      </c:forEach>
                    </div>
                </div>
              </div>

              <div class="clearfix"></div>
	          </div>

            <div class="clearfix"></div>
	        </div>

          <div class="clearfix"></div>
        </div>


        <div class="block-stack-01">          
          <div id="content_target" class="control-group">
            <div class="span4 pull-left control-group-02 control-group-03">
						<fieldset>
						  <legend class="block-stack-title"><spring:message code="target_member_group" /></legend>
						  <div class="well well-target">
	              <form:select path="targetMemberGroups" multiple="true" disabled="true">
	                <c:forEach var="item" items="${promotion.targetMemberGroups}" ><option value="${item}">${memberGroups[item]}</option></c:forEach>
	              </form:select>
						  </div>
						</fieldset>
						</div>
						<div class="span3 pull-left control-group-02">
						<fieldset>
						  <legend class="block-stack-title"><spring:message code="target_store_group" /></legend>
						  <div class="well well-target">
							  <form:select path="targetStoreGroups" multiple="true" disabled="true">
							    <c:forEach var="item" items="${promotion.targetStoreGroups}" ><option value="${item}">${storeGroups[item]}</option></c:forEach>
							  </form:select>
						  </div>
						</fieldset>
						</div>

						<c:if test="${!isRedemptionPromo}">
						<div class="span3 pull-left control-group-02">
						<fieldset>
						  <legend class="block-stack-title"><spring:message code="target_product_group" /></legend>
						  <div class="well well-target">
							  <form:select path="targetProductGroups" id="targetProductGroups" multiple="true" disabled="true">
							    <c:forEach var="item" items="${promotion.targetProductGroups}" ><option value="${item}">${productGroups[item]}</option></c:forEach>
							  </form:select>
						  </div>
						</fieldset>
						</div>
						<div class="span3 pull-left control-group-02">
						<fieldset>
						  <legend class="block-stack-title"><spring:message code="target_payment_type" /></legend>
						  <div class="well well-target">
							  <form:select path="targetPaymentTypes" multiple="true" disabled="true">
							    <c:forEach var="item" items="${promotion.targetPaymentTypes}" >
                                    <c:choose>
                                      <c:when test="${not empty paymentTypes[item]}">
                                        <option value="${item}">${paymentTypes[item]}</option>
                                      </c:when>
                                      <c:otherwise>
                                        <option value="${item}">${paymentTypesWilcard[item]}</option>
                                      </c:otherwise>
                                    </c:choose>
							      </c:forEach>
							  </form:select>
							  <c:forEach var="item" items="${paymentTypesWilcard}" >
                                <input type="hidden" data-code="${item.key}" class="targetWildcardsIn targetWildcardsIn${item.key}" name="targetWildcards['${item.key}']" value="${promotion.targetWildcards[item.key]}" />
                              </c:forEach>
						  </div>
						</fieldset>
						</div>
						</c:if>
          </div>
        </div>

        <c:if test="${!isStampPromo}">
        <div class="block-stack-01">
          <div class="control-group-01">
          <fieldset>
            <legend class="block-stack-title"><spring:message code="promotion_prop_rewards" /></legend>
            <c:if test="${isRedemptionPromo}">
              <div id="block_promotionRewards" class="">
                <div id="list_promotionRewards"><c:if test="${not empty promotion.promotionRewards}"><jsp:include page="reward/rewardView.jsp" /></c:if></div>
	            </div>
            </c:if>
            <c:if test="${!isRedemptionPromo}">
	            <div id="block_promotionRewards" class="control-group">
	              <label for="rewardType" class="control-label"><spring:message code="promotion_prop_rewardtype"/></label>
	              <div id="content_rewardTypes" class="controls">
	                <c:url var="url_createReward" value="/promotion/reward/list/"/>
	                <form:select path="rewardType" id="" data-url='${url_createReward}' class="" disabled="true" >
	                  <option/>
	                  <form:options items="${promotionRewardTypes}" itemValue="code" itemLabel="description" />
	                </form:select>
	              </div>
	
	              <div id="list_promotionRewards"><c:if test="${not empty promotion.promotionRewards}"><jsp:include page="reward/rewardView.jsp" /></c:if></div>
	            </div>
            </c:if>
          </fieldset>
          </div>
        </div>
        </c:if>
        
        <c:if test="${isStampPromo}">
        <jsp:include page="stamp/stampRewardsView.jsp" />
        </c:if>

        <c:if test="${isMarketingHead}">
        <div class="block-stack-01">
          <div class="control-group-01">
          <fieldset>
            <legend class="block-stack-title"><spring:message code="remark_prop_remark" /></legend>
            <div class="controls"><form:textarea path="remarks.remarks" id="remarksRemarks"/></div>
            <form:hidden path="remarks.status" id="remarksStatus"/>
            <form:hidden path="remarks.modelId"/>
            <form:hidden path="remarks.modelType"/>
          </fieldset>
          </div>
        </div>
        </c:if>

      </div>

    </div>
  
  </fieldset>


  <div class="form-actions container-btn">
    <c:if test="${isMarketingHead}">
      <c:if test="${promotion.status eq promotionStatuses[0].code}">
        <input id="promotionRemarksForm_disableButton" type="submit"  value="<spring:message code="label_${promotionStatuses[2].code}"/>"  class="promotionStatus btn btn-primary" data-value="${promotionStatuses[2].code}" />
      </c:if>
    </c:if>
    <input id="promotionForm_cancelButton" class="btn" type="button" value='<spring:message code="label_cancel"/>' />
  </div>

  </form:form>

	<div id="form_promotionProgram"><jsp:include page="program/programForm.jsp" /></div>
	<jsp:include page="targets/productgroup.jsp" />
	<jsp:include page="targets/storegroup.jsp" />
	<jsp:include page="targets/membergroup.jsp" />
	<jsp:include page="targets/paymenttype.jsp" />
  <div id="storeGroupContainer"></div>
  <div id="productGroupContainer"></div>
  <div id="memberGroupContainer"></div>

  <script src='<c:url value="/js/viewspecific/promotion/promotionForm.js"/>' type='text/javascript'><![CDATA[&nbsp; ]]></script>
  <script src='<c:url value="/js/bootstrap/bootstrap-datepicker.js"/>' type='text/javascript'><![CDATA[&nbsp; ]]></script>
  <script src='<c:url value="/js/bootstrap/bootstrap-timepicker.min.js"/>' type='text/javascript'><![CDATA[&nbsp; ]]></script>
  <link href="<c:url value="/css/bootstrap/datepicker.css" />" rel="stylesheet" />
  <link href="<c:url value="/css/bootstrap/bootstrap-timepicker.min.css" />" rel="stylesheet" />
  <link href="<c:url value="/styles/temp.css" />" rel="stylesheet" />
  <script src="<spring:url value="/js/jquery/jquery.numberInput.js" />" ></script>



</div>