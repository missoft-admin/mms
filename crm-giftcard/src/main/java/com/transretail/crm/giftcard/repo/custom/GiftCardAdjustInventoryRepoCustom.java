package com.transretail.crm.giftcard.repo.custom;

import com.transretail.crm.common.service.dto.request.PageSortDto;
import com.transretail.crm.common.service.dto.response.ResultList;
import com.transretail.crm.giftcard.dto.GiftCardPhysicalCountSummaryDto;
import com.transretail.crm.giftcard.entity.GiftCardPhysicalCount;
import java.util.Collection;
import java.util.Set;

/**
 *
 * @author Monte Cillo Co (mco@exist.com)
 */
public interface GiftCardAdjustInventoryRepoCustom {

    void updateReceivedInventories(Collection<GiftCardPhysicalCount> physicalCount);

    void updateMissingInventories(Collection<GiftCardPhysicalCount> physicalCount);

    ResultList<GiftCardPhysicalCountSummaryDto> getSummary(PageSortDto sortDto);
    
    Set<String> getStoreCodeIn(String startingSeries, String endingSeries);
}
