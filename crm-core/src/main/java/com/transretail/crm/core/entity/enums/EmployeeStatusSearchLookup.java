package com.transretail.crm.core.entity.enums;


public enum EmployeeStatusSearchLookup {
	
	ACTIVE ( "Active", MemberStatus.ACTIVE ),
	TERMINATED ( "Terminated", MemberStatus.TERMINATED);
	
	private EmployeeStatusSearchLookup(String inDesc, MemberStatus inValue) {
		this.desc = inDesc;
		this.value = inValue;
	}
	
	private final String desc;
    private final MemberStatus value;
	public String getDesc() {
		return desc;
	}
	public MemberStatus getValue() {
		return value;
	}
    
    
}
