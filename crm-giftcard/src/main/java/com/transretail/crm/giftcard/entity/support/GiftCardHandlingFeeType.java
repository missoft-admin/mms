package com.transretail.crm.giftcard.entity.support;

/**
 * @author ftopico
 */
public enum GiftCardHandlingFeeType {
    FIXED, PER_CARD
}
