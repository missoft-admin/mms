package com.transretail.crm.core.service;

import java.util.List;

import com.transretail.crm.common.service.dto.request.PageSortDto;
import com.transretail.crm.common.service.dto.response.ResultList;
import com.transretail.crm.core.dto.StoreGroupDto;
import com.transretail.crm.core.dto.StoreGrpSearchDto;
import com.transretail.crm.core.entity.lookup.Store;
import com.transretail.crm.core.entity.lookup.StoreGroup;

public interface StoreGroupService {

	void saveStoreGroup(StoreGroup group);

	List<StoreGroup> findAll();

	StoreGroup findOne(Long id);

	void delete(Long id);

	List<Store> getItems(Long id);

	void createStoreGroup(StoreGroup group);

	List<StoreGroup> getAllGroups();

	Iterable<Store> searchItems(String searchStr);

	ResultList<StoreGroup> listStoreGroups(PageSortDto dto);

	ResultList<Store> listStores(Long id, PageSortDto dto);

	ResultList<StoreGroupDto> search(StoreGrpSearchDto dto);

}
