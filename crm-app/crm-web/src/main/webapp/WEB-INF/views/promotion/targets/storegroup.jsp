<%@ include file="../../common/taglibs.jsp" %>

<style type="text/css">
<!--
  .cont-02 input { margin-bottom: 3px; } 
  label .link { font-weight: normal !important; }
-->
</style>


<spring:message code="operand_tooltip_label_info" var="info" />
<spring:message code="operand_tooltip_store_group" var="tooltip" />

<div class="modal hide  nofly" data-context-path="${pageContext.request.contextPath}" id="targetStoreGroupsDialog" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4>Add <spring:message code="target_store_group" /></h4>
      </div>
      <div class="modal-body">
      
   	  <div class="mb10">
        <a id="createStoreGrpLnk" class="btn" href="${pageContext.request.contextPath}/groups/store/dialog"><spring:message code="label_create" /> <spring:message code="label_group_storegroup"/></a><a id="link_viewStoreGroup" class="hide" data-url-view="<c:url value="/groups/store/view/" />">, <spring:message code="label_view" /></a>
      </div>
     	
     	<div>
	       	<div class="pull-left" style="width: 47%;">
	       		<div style="width: 100%" id=""><label for=""><spring:message code="label_group_storegroup"/></label></div>
	       		<div style="width: 100%">
	        		<select multiple="multiple" name="storeGroups" id="selectStoreGroups" style="width:100%;">
	        		</select>
	       		
	       		</div>
	       	</div>
	       	<div class="">
	       		<div class="cont-02" style="margin-top: 25px;">
  	       		<input type="button" id="addStoreGrp" class="btn" value=">>"/>
  	          <input type="button" id="removeStoreGrp" class="btn" value="<<"/>
	       		</div>
	       	</div>
	       	
	       	<div class="pull-left" style="width: 40%">
	       	
	       		<div style="width: 100%"><label for=""><spring:message code="label_group_selection"/></label></div>
	       		<div style="width: 100%">
	        		<select multiple="multiple" class="items" name="targetStoreGroups" id="selectedStoreGroups" style="width:100%;">
	        		</select>
	       		</div>
	       	</div>
      	</div>
        <div id="viewStoreGroup"></div>
      
      	
     
      </div>
      <div class="modal-footer container-btn">
        <button type="button" id="storeGroupSaveButton" class="btn btn-primary"><spring:message code="label_save"/></button>
        <button type="button" id="storeGroupCancelButton" class="btn " data-dismiss="modal"><spring:message code="label_cancel"/></button>
      </div>
    </div>
  </div>
</div>