package com.transretail.crm.giftcard.repo.custom.impl;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import com.mysema.query.jpa.impl.JPAQuery;
import com.transretail.crm.giftcard.entity.GiftCardInventory;
import com.transretail.crm.giftcard.entity.QSalesOrder;
import com.transretail.crm.giftcard.entity.QSalesOrderAlloc;
import com.transretail.crm.giftcard.entity.QSalesOrderItem;
import com.transretail.crm.giftcard.entity.SalesOrder;
import com.transretail.crm.giftcard.repo.custom.SalesOrderRepoCustom;

public class SalesOrderRepoImpl implements SalesOrderRepoCustom {

    @PersistenceContext
    private EntityManager em;

    @Override
    public SalesOrder findEnclosingSalesOrderOfGiftCardInventory(GiftCardInventory cardInventory) {
	QSalesOrderAlloc alloc = QSalesOrderAlloc.salesOrderAlloc;
	QSalesOrderItem orderItem = QSalesOrderItem.salesOrderItem;
	QSalesOrder order = QSalesOrder.salesOrder;

        List<SalesOrder> list = new JPAQuery(em).from(order)
                .join(order.items, orderItem)
                .join(orderItem.soAlloc, alloc)
                .where(alloc.seriesStart.loe( cardInventory.getSeries() ).and( alloc.seriesEnd.goe(cardInventory.getSeries())))//.where(alloc.gcInventory.eq(cardInventory))
                .orderBy(order.created.desc())
                .distinct().list(order);
        
        return list.iterator().hasNext() ? list.iterator().next() : null;
    }

}
