create table CRM_APPROVAL_MATRIX_AUD (
        id bigint not null,
        REV bigint not null,
        REVTYPE tinyint,
        AMOUNT double,
        MODEL_TYPE varchar(255),
        USERNAME bigint,
        primary key (id, REV)
    );

create table CRM_POINTS_AUD (
        id varchar(255) not null,
        REV bigint not null,
        REVTYPE tinyint,
        REASON varchar(1000),
        COMMENTS varchar(1000),
        EXPIRE_EXCESS double,
        EXPIRY_DATE date,
        INCLUSIVE_DATE_FROM timestamp,
        INCLUSIVE_DATE_TO timestamp,
        LAST_REDEEM timestamp,
        RETURN_TXN_NO varchar(255),
        STATUS varchar(255),
        STORE_CODE varchar(255),
        TXN_AMOUNT double,
        TXN_DATE timestamp,
        TXN_MEDIA varchar(255),
        TXN_NO varchar(255),
        TXN_POINTS double,
        TXN_TYPE varchar(255),
        VALID_PERIOD integer,
        VALIDATED_BY varchar(255),
        VALIDATED_DATE timestamp,
        MEMBER_ID bigint,
        TXN_OWNER bigint,
        primary key (id, REV)
    );

alter table CRM_APPROVAL_MATRIX_AUD 
        add constraint FK_djhyw5w2f7xf7vutud318355r 
        foreign key (REV) 
        references CRM_REVINFO;

alter table CRM_POINTS_AUD 
        add constraint FK_2jvrt58lew0bjwjhfpsw3o03c 
        foreign key (REV) 
        references CRM_REVINFO;