<%@ include file="../../common/taglibs.jsp" %>

<c:set var="ENCTYPE" value="application/x-www-form-urlencoded"/>

<div class="modal-dialog">
	<div class="modal-content">
		<div class="modal-header">
			<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
			<h4 class="modal-title"><spring:message code="label_mo_printmoreceipt"/></h4>
		</div>
		<div class="modal-body">
			<div class="control-group">
				<label><spring:message code="order_delivery_receipt" /></label>
				<div class="controls">
					<select id="deliveryReceipt">
						<c:forEach items="${gcOrderReceived}" var="gcOrder">
							<option value="${gcOrder.deliveryReceipt}/${gcOrder.receivedDate}/${gcOrder.receivedBy}">${gcOrder.deliveryReceipt} (${gcOrder.receivedBy} - ${gcOrder.receivedDate})</option>
						</c:forEach>
					</select>
				</div>
			</div>
		</div>
		<div class="modal-footer">
			<button type="button" id="reprint-button" data-print-url="${pageContext.request.contextPath}/giftcard/order/reprintreceipt/" class="btn btn-primary reprint"><spring:message code="label_print" /></button>
      		<button type="button" id="" class="btn btn-default" data-dismiss="modal"><spring:message code="label_cancel"/></button>
		</div>
	</div>
</div>

<script type="text/javascript">
$(document).ready(function() {
	$("#reprint-button").click(function() {
		var val = $("#deliveryReceipt").val().split("/");
		var receipt = val[0];
		var date = val[1].split("-");
		var receivedBy = val[2];
		var newDate = date[2] + "-" + date[1] + "-" + date[0];
		var url = $(this).data("printUrl") + "${id}/" + receipt + "/" + newDate + "/" + receivedBy;
		window.open(url, '_blank', 'toolbar=0,location=0,menubar=0');
		$("#orderDialog").modal('hide');
	});
});
</script>