<%@ include file="../../../common/taglibs.jsp" %>


  <!-- Interested Products -->
	<fieldset class="form-horizontal container-fluid">
	  <legend><spring:message code="label_member_indi_interestedproducts" /></legend>

	  <c:forEach items="${interestedProducts}" var="item">
	  <!--<label for="marketingDetails.interestedProducts" class="control-label"></label>-->
	  <div class="checkbox controls">
	    <input type="checkbox" name="marketingDetails.interestedProducts" value="${item.code}" />
      <label class="">${item.description}</label>
	  </div>
	  </c:forEach>
	</fieldset>


<style> input.form-control-2 { margin-top: 10px !important; } </style>