<%@ include file="../../common/taglibs.jsp" %>

<div id="txn_list">
</div>

  <link href="<spring:url value="/css/dataTables.css" />" rel="stylesheet">
  <script src="<spring:url value="/js/jquery/jquery.dataTables.js" />" ></script>
  <script src="<spring:url value="/js/jquery/jquery.dataTables.linkLengthMenu.js" />"></script>
  <script src="<spring:url value="/js/jquery/jquery.dataTables.customPaging.js" />"></script>
  <script src="<spring:url value="/js/jquery/ajax-datatable.js" />" ></script>
  <script src="<spring:url value="/js/common/form2js.js" />" ></script>
  <script src="<spring:url value="/js/common/json2.js" />" ></script>
  <script src="<spring:url value="/js/jquery/jquery.toObject.js" />" ></script>
  <link href="<c:url value="/styles/temp.css" />" rel="stylesheet" />
  
<script type="text/javascript">
$(document).ready(function() {
	
	$("#txn_list").ajaxDataTable({
		'autoload'  : true,
		'ajaxSource' : "<c:url value="/giftcard/b2b/exhibition/list" />",
		'columnHeaders' : [
			{text: "", className : "hide"}
		],
		'modelFields' : [
			{customCell : function ( innerData, sSpecific, json ) {
				if (sSpecific == 'display') {
					return '<a class="showTransaction" style="color: #0080FF !important;" href="#" data-id="'+json.id+'" onclick="return false;"><div>'
					+ json.created + '</div></a>';
				} else {
					return "";
				}
			}
			}
		]
	}).on("click", ".showTransaction", function() {
		$.post("<c:url value="/giftcard/b2b/exhibition/get/" />" + $(this).data("id"), function(data) {
			$("#exhibitionFormContainer").html(data);
		}, "html");
	});
  
  
});
</script>
<style>

.dataTables_length {
    overflow: hidden;
    text-indent: -77px;
    width: 50%;
}

.dataTables_info {
    width: 50%;
}
</style>