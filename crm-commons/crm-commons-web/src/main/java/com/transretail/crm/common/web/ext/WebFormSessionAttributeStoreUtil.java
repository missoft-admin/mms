package com.transretail.crm.common.web.ext;

import javax.servlet.http.HttpServletRequest;

import com.transretail.crm.common.web.dto.WebForm;


/**
 * @author aco
 */
public final class WebFormSessionAttributeStoreUtil {

    public static final String WEB_FORM_ID_PREFIX = "__";
    public static final String WEB_FORM_ID_SUFFIX = "$webFormId";

    private WebFormSessionAttributeStoreUtil() {
    }

    public static String createWebFormIdName(String attrName) {
        return WEB_FORM_ID_PREFIX + attrName + WEB_FORM_ID_SUFFIX;
    }

    public static String getWebFormId(String attrName, HttpServletRequest request) {
        return (String) request.getAttribute(createWebFormIdName(attrName));
    }

    public static WebForm getWebForm(String attrName, HttpServletRequest request) {
        String webFormId = getWebFormId(attrName, request);
        if (webFormId != null) {
            return (WebForm) request.getSession().getAttribute(webFormId);

        } else {
            return null;
        }
    }

}
