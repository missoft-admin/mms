package com.transretail.crm.giftcard.entity.support;

/**
 * @author ftopico
 */
public enum GiftCardBurnCardStatus {
    DRAFT, BURNED, FORAPPROVAL, REJECTED
}
