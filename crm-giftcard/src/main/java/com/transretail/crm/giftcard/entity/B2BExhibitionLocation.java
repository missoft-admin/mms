package com.transretail.crm.giftcard.entity;

import java.io.Serializable;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.annotations.Type;
import org.joda.time.LocalDate;

import com.transretail.crm.core.entity.support.CustomAuditableEntity;

/**
 *
 */
@Entity
@Table(name = "CRM_B2B_EXH_LOC")
public class B2BExhibitionLocation extends CustomAuditableEntity<Long> implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@OneToMany(mappedBy = "location", orphanRemoval = true, fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	private List<B2BExhibitionDiscount> discountMatrix;
	
	
	@Column(name = "NAME")
	private String name; //shortname
	
	@Column(name = "ADDRESS")
	private String address;
	
	@Column(name = "START_DATE")
	@Type(type = "org.jadira.usertype.dateandtime.joda.PersistentLocalDate")
	private LocalDate startDate;
	
	@Column(name = "END_DATE")
	@Type(type = "org.jadira.usertype.dateandtime.joda.PersistentLocalDate")
	private LocalDate endDate;
	
	public B2BExhibitionLocation() {}
	public B2BExhibitionLocation(Long id) {
		setId(id);
	}

	public List<B2BExhibitionDiscount> getDiscountMatrix() {
		return discountMatrix;
	}

	public void setDiscountMatrix(List<B2BExhibitionDiscount> discountMatrix) {
		this.discountMatrix = discountMatrix;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public LocalDate getStartDate() {
		return startDate;
	}

	public void setStartDate(LocalDate startDate) {
		this.startDate = startDate;
	}

	public LocalDate getEndDate() {
		return endDate;
	}

	public void setEndDate(LocalDate endDate) {
		this.endDate = endDate;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}
	
	
	
}
