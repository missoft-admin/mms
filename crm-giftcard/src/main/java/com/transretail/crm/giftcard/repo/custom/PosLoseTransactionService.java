package com.transretail.crm.giftcard.repo.custom;

import com.transretail.crm.giftcard.dto.*;

public interface PosLoseTransactionService {

    LoseTransactionResultList search(PosLoseTransactionSearchDto searchDto);

    void createNewLoseTransaction(PosLoseTxForm form);

    PosLoseTxForm findByTransactionNo(String transactionNo);
}
