package com.transretail.crm.loyalty.service.impl;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Map;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.hibernate.StaleObjectStateException;
import org.hibernate.StaleStateException;
import org.joda.time.DateTime;
import org.joda.time.LocalDate;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.mysema.query.jpa.JPQLQuery;
import com.mysema.query.jpa.impl.JPAQuery;
import com.mysema.query.types.expr.BooleanExpression;
import com.mysema.query.types.expr.NumberExpression;
import com.transretail.crm.common.reporting.jasper.ClasspathFileResolver;
import com.transretail.crm.common.reporting.jasper.DefaultJRProcessor;
import com.transretail.crm.common.reporting.jasper.JRProcessor;
import com.transretail.crm.common.service.dto.request.PageSortDto;
import com.transretail.crm.common.service.dto.response.NameIdPairDto;
import com.transretail.crm.common.service.dto.response.NameIdPairResultList;
import com.transretail.crm.common.service.dto.response.ResultList;
import com.transretail.crm.common.service.util.SpringDataPagingUtil;
import com.transretail.crm.core.entity.CounterModel;
import com.transretail.crm.core.entity.UserRoleModel;
import com.transretail.crm.core.entity.lookup.LookupDetail;
import com.transretail.crm.core.repo.CounterModelRepo;
import com.transretail.crm.core.repo.UserRoleRepo;
import com.transretail.crm.core.security.model.impl.CustomSecurityUserDetailsImpl;
import com.transretail.crm.core.security.util.UserUtil;
import com.transretail.crm.core.service.CodePropertiesService;
import com.transretail.crm.core.service.NameIdPairService;
import com.transretail.crm.giftcard.entity.CardVendor;
import com.transretail.crm.giftcard.entity.QCardVendor;
import com.transretail.crm.giftcard.repo.CardVendorRepo;
import com.transretail.crm.loyalty.dto.ManufactureOrderDto;
import com.transretail.crm.loyalty.dto.ManufactureOrderReceiveDto;
import com.transretail.crm.loyalty.dto.ManufactureOrderSearchDto;
import com.transretail.crm.loyalty.dto.ManufactureOrderTierDto;
import com.transretail.crm.loyalty.dto.ProductSeriesDto;
import com.transretail.crm.loyalty.dto.ReceiveOrderDto;
import com.transretail.crm.loyalty.entity.ManufactureOrder;
import com.transretail.crm.loyalty.entity.ManufactureOrderTier;
import com.transretail.crm.loyalty.entity.QLoyaltyCardInventory;
import com.transretail.crm.loyalty.entity.QManufactureOrder;
import com.transretail.crm.loyalty.entity.QManufactureOrderTier;
import com.transretail.crm.loyalty.repo.LoyaltyCardInventoryRepo;
import com.transretail.crm.loyalty.repo.ManufactureOrderRepo;
import com.transretail.crm.loyalty.repo.ManufactureOrderTierRepo;
import com.transretail.crm.loyalty.service.LoyaltyCardService;
import com.transretail.crm.loyalty.service.ManufactureOrderService;

@Service("manufactureOrderService")
@Transactional
public class ManufactureOrderServiceImpl implements ManufactureOrderService {

	@Autowired
	ManufactureOrderRepo manufactureOrderRepo;
	@Autowired
	ManufactureOrderTierRepo manufactureOrderTierRepo;
	@Autowired
	CodePropertiesService codePropertiesService;
	@Autowired
	UserRoleRepo userRoleRepo;
	@Autowired
	NameIdPairService nameIdPairService;
	@Autowired
	LoyaltyCardInventoryRepo inventoryRepo;
	@Autowired
	LoyaltyCardService loyaltyCardService;
    @PersistenceContext
    private EntityManager em;

    @Autowired
    private CounterModelRepo counterModelRepo;

	private static final DateTimeFormatter FULL_DATE_PATTERN = DateTimeFormat.forPattern("MMMM d, yyyy");

	@Override
	public ResultList<ManufactureOrderDto> listManufactureOrderDto(ManufactureOrderSearchDto dto) {
		Pageable pageable = SpringDataPagingUtil.INSTANCE.createPageable(dto.getPagination());
		Page<ManufactureOrder> page = manufactureOrderRepo.findAll(dto.createSearchExpression(), pageable);
		List<ManufactureOrderDto> list = toDto(page.getContent());
		return new ResultList<ManufactureOrderDto>(list, page.getTotalElements(), page.hasPreviousPage(),
				page.hasNextPage());
	}

	@Override
	public NameIdPairResultList getVendors() {
		QCardVendor qCardVendor = QCardVendor.cardVendor;
		return nameIdPairService.getNameIdPairs(null, qCardVendor, qCardVendor.id, qCardVendor.formalName, qCardVendor.defaultVendor.desc());
	}

	@Override
	public boolean isApprover() {
		CustomSecurityUserDetailsImpl userDetails = UserUtil.getCurrentUser();
		Collection<UserRoleModel> userRoles = userDetails.getRoles();
		if(CollectionUtils.isNotEmpty(userRoles) && userRoles.contains(userRoleRepo.findByCode(codePropertiesService.getMerchantServiceSupervisorCode()).get(0)))
			return true;
		return false;
	}

	@Override
	public List<ManufactureOrderDto> findAllDtosForApprover() {
		QManufactureOrder qManufactureOrder = QManufactureOrder.manufactureOrder;
		return convertToDto(manufactureOrderRepo.findAll(qManufactureOrder.status.code.eq(codePropertiesService.getDetailMoStatusForApproval())));
	}

	@Override
    public void saveMoDto(ManufactureOrderDto manufactureOrderDto) throws StaleStateException {
        if (StringUtils.isBlank(manufactureOrderDto.getMoNo())) {
            generateMoNo(manufactureOrderDto);
        }
        generateBatchNo(manufactureOrderDto);
        updateCompanyCardTypeCounter(manufactureOrderDto.getTiers());
        ManufactureOrder mo = manufactureOrderRepo.save(manufactureOrderDto.toModel());
        manufactureOrderDto.setId(mo.getId());
    }

    private void generateMoNo(ManufactureOrderDto manufactureOrderDto) {
		QManufactureOrder qManufactureOrder = QManufactureOrder.manufactureOrder;
		LocalDate monthBegin = manufactureOrderDto.getMoDate().withDayOfMonth(1);
		LocalDate monthEnd = manufactureOrderDto.getMoDate().plusMonths(1).withDayOfMonth(1).minusDays(1);
		long count = manufactureOrderRepo.count(qManufactureOrder.moDate.goe(monthBegin).and(qManufactureOrder.moDate.loe(monthEnd)));

		DateTimeFormatter formatter = DateTimeFormat.forPattern("yyMM");
		String prefix = formatter.print(manufactureOrderDto.getMoDate());
		String suffix = String.format("%06d", count + 1);
		manufactureOrderDto.setMoNo(prefix + suffix);
	}

	private void generateBatchNo(ManufactureOrderDto manufactureOrderDto) {
		if(CollectionUtils.isNotEmpty(manufactureOrderDto.getTiers())) {
			QManufactureOrderTier qProduct = QManufactureOrderTier.manufactureOrderTier;
			DateTime monthBegin = new DateTime().dayOfMonth().withMinimumValue().millisOfDay().withMinimumValue();
			DateTime monthEnd = new DateTime().dayOfMonth().withMaximumValue().millisOfDay().withMaximumValue();
			long count = manufactureOrderTierRepo.count(qProduct.manufactureOrder.created.goe(monthBegin).and(qProduct.manufactureOrder.created.loe(monthEnd)));
			DateTimeFormatter formatter = DateTimeFormat.forPattern("yyMM");
			String prefix = formatter.print(new DateTime());
			List<ManufactureOrderTierDto> products = manufactureOrderDto.getTiers();
			int i = 0;
			for(ManufactureOrderTierDto product: products) {
				if(StringUtils.isBlank(product.getBatchNo())) {
					String suffix = String.format("%03d", count + i + 1);
					product.setBatchNo(prefix + suffix);
					i++;
				}
			}
		}
	}
	@Override
	@Transactional
	public void updateMoStatus(Long moId, LookupDetail status) {
		ManufactureOrder order = manufactureOrderRepo.findOne(moId);
		order.setStatus(status);
		manufactureOrderRepo.save(order);
	}

	@Override
    public void updateMoDto(ManufactureOrderDto manufactureOrderDto) throws StaleObjectStateException {
        ManufactureOrder order = manufactureOrderRepo.findOne(manufactureOrderDto.getId());
		generateBatchNo(manufactureOrderDto);
        updateCompanyCardTypeCounter(manufactureOrderDto.getTiers());
		manufactureOrderRepo.save(manufactureOrderDto.toModel(order));
	}

    private void updateCompanyCardTypeCounter(List<ManufactureOrderTierDto> tiers) throws StaleObjectStateException{
        if (tiers != null && tiers.size() > 0) {
            Map<String, StartEndSeriesTuple> map = Maps.newHashMap();
            for (ManufactureOrderTierDto tier : tiers) {
                String startingSeries = tier.getStartingSeries();
                String comCardLast2Digits = startingSeries.substring(0, 4);
                StartEndSeriesTuple tuple = map.get(comCardLast2Digits);
                long start = Long.valueOf(startingSeries.substring(4));
                long end = start + tier.getQuantity() - 1;
                if (tuple == null) {
                    tuple = new StartEndSeriesTuple();
                    tuple.start = start;
                    tuple.end = end;
                    map.put(comCardLast2Digits, tuple);
                } else {
                    if (start < tuple.start) {
                        tuple.start = start;
                    }
                    if (end > tuple.end) {
                        tuple.end = end;
                    }
                }
            }
            for (String comCardLast2Digits : map.keySet()) {
                StartEndSeriesTuple tuple = map.get(comCardLast2Digits);
                String key = CounterModel.PRFX_LCNO_COUNTER + comCardLast2Digits;
                CounterModel counterModel = counterModelRepo.findOne(key);
                if (counterModel == null) {
                    counterModel = new CounterModel();
                    counterModel.setKey(key);
                }
                counterModel.setValue(tuple.end);
                counterModelRepo.save(counterModel);
            }
        }
    }

    private class StartEndSeriesTuple {
        long start;
        long end;
    }

    @Override
	public ManufactureOrderDto findDtoById(Long id) {
		return convertToDto(manufactureOrderRepo.findOne(id));
	}

	@Override
	public void populateReceiveCount(ManufactureOrderTierDto product) {
		QLoyaltyCardInventory qInventory = QLoyaltyCardInventory.loyaltyCardInventory;

		Long startSeries = Long.valueOf(product.getStartingSeries());
		Long endSeries = startSeries + product.getQuantity() - 1;
		NumberExpression<Long> seriesExp = qInventory.sequence.castToNum(Long.class);

		Long count = inventoryRepo.count(qInventory.product.name.eq(product.getName())
            .and(seriesExp.goe(startSeries))
            .and(seriesExp.loe(endSeries))
            .and(qInventory.status.isNotNull()));

		product.setReceived(count);
	}

	@Override
	public ManufactureOrderDto findDtoByMoNo(String moNo) {
		return convertToDto(manufactureOrderRepo.findByMoNo(moNo));
	}

	@Override
	public List<ManufactureOrderDto> findAllDtos() {
		return convertToDto(manufactureOrderRepo.findAll());
	}

	@Override
	public ManufactureOrderDto findDuplicateMoNo(ManufactureOrderDto orderDto) {
		ManufactureOrder order = manufactureOrderRepo.findByMoNo(orderDto.getMoNo());
		ManufactureOrderDto dto = convertToDto(order);
		return (order != null && !order.getId().equals(orderDto.getId())) ? dto : null;
	}

    @Transactional(readOnly = true)
    public String findDuplicateTier(ManufactureOrderTierDto tier) {
        QManufactureOrderTier qTier = QManufactureOrderTier.manufactureOrderTier;
        BooleanExpression condition = qTier.name.code.eq(tier.getName().getCode())
        		.and(qTier.company.code.eq(tier.getCompanyCode()))
        		.and(qTier.manufactureOrder.status.code.ne(codePropertiesService.getDetailMoStatusApproved()));
        if (tier.getManufactureOrderId() != null) {
            condition = condition.and(qTier.manufactureOrder.id.ne(tier.getManufactureOrderId()));
        }
        return new JPAQuery(em).from(qTier).where(condition).limit(1).singleResult(qTier.manufactureOrder.moNo);
    }

	@Override
	public void deleteMo(Long id) {
		manufactureOrderRepo.delete(id);
	}

	private ManufactureOrderDto convertToDto(ManufactureOrder order) {
		if(order != null)
			return new ManufactureOrderDto(order);
		else
			return null;
	}

	private ManufactureOrderTierDto convertToDto(ManufactureOrderTier orderTier) {
		if(orderTier != null)
			return new ManufactureOrderTierDto(orderTier);
		else
			return null;
	}

	private List<ManufactureOrderDto> convertToDto(Iterable<ManufactureOrder> orders) {
		if(orders != null) {
			List<ManufactureOrderDto> orderDtos = new ArrayList<ManufactureOrderDto>();
			for(ManufactureOrder order : orders) {
				orderDtos.add(new ManufactureOrderDto(order));
			}
			return orderDtos;
		} else
			return null;
	}

	
	private List<ManufactureOrderDto> toDto(Iterable<ManufactureOrder> orders) {
		if(orders != null) {
			List<ManufactureOrderDto> orderDtos = new ArrayList<ManufactureOrderDto>();
			for(ManufactureOrder order : orders) {
				ManufactureOrderDto dto = new ManufactureOrderDto(order);
				dto.setShowReceive(areInventoriesReceivable(dto.getId()));
				orderDtos.add(dto);
			}
			return orderDtos;
		} else
			return null;
	}
	
	private boolean areInventoriesReceivable(Long manufactureOrderId) {
		if(inventoryRepo.countReceivableInvetories(manufactureOrderId) > 0)
			return true;
		return false;
	}

	private List<ManufactureOrderTierDto> convertTiersToDto(Iterable<ManufactureOrderTier> orderTiers) {
		if(orderTiers != null) {
			List<ManufactureOrderTierDto> orderTierDtos = new ArrayList<ManufactureOrderTierDto>();
			for(ManufactureOrderTier order : orderTiers) {
				orderTierDtos.add(new ManufactureOrderTierDto(order));
			}
			return orderTierDtos;
		} else
			return null;
	}

	@Override
	public JRProcessor createJrProcessor(ManufactureOrderDto orderDto) {
    	Map<String, Object> parameters = Maps.newHashMap();
    	parameters.put("purchaseOrder", orderDto.getPoNo() + " / " + FULL_DATE_PATTERN.print(orderDto.getOrderDate()));
    	parameters.put("manufactureOrder", orderDto.getMoNo() + " / " + FULL_DATE_PATTERN.print(orderDto.getMoDate()));
    	parameters.put("supplier", orderDto.getSupplierName());
    	List<ManufactureOrderTierDto> products = orderDto.getTiers();
    	if (products.isEmpty()) {
            parameters.put("isEmpty", true);
            products.add(new ManufactureOrderTierDto());
        }
        DefaultJRProcessor jrProcessor = new DefaultJRProcessor("reports/manufacture_order.jasper", products);
        jrProcessor.addParameters(parameters);
        jrProcessor.setFileResolver(new ClasspathFileResolver("reports"));
        return jrProcessor;
    }

	@Override
	public JRProcessor createJrProcessorReceiveAll(ManufactureOrderDto orderDto) {
    	Map<String, Object> parameters = Maps.newHashMap();
    	parameters.put("purchaseOrder", orderDto.getPoNo() + " / " + FULL_DATE_PATTERN.print(orderDto.getOrderDate()));
    	parameters.put("manufactureOrder", orderDto.getMoNo() + " / " + FULL_DATE_PATTERN.print(orderDto.getMoDate()));
    	parameters.put("supplier", orderDto.getSupplierName());
    	parameters.put("createdBy", orderDto.getCreatedBy());
    	List<ManufactureOrderReceiveDto> inventories = loyaltyCardService.getReceivedInventories(orderDto.getId());
    	if (inventories.isEmpty()) {
            parameters.put("isEmpty", true);
            inventories.add(new ManufactureOrderReceiveDto());
        } 
        DefaultJRProcessor jrProcessor = new DefaultJRProcessor("reports/manufacture_order_receive_all.jasper", inventories);
        jrProcessor.addParameters(parameters);
        jrProcessor.setFileResolver(new ClasspathFileResolver("reports"));
        return jrProcessor;
    }
	
	@Override
	public JRProcessor createJrProcessor(ReceiveOrderDto receiveDto) {
    	Map<String, Object> parameters = Maps.newHashMap();
    	ManufactureOrderDto orderDto = findDtoById(receiveDto.getManufactureOrder());
    	parameters.put("purchaseOrder", orderDto.getPoNo() + " / " + FULL_DATE_PATTERN.print(orderDto.getOrderDate()));
    	parameters.put("manufactureOrder", orderDto.getMoNo() + " / " + FULL_DATE_PATTERN.print(orderDto.getMoDate()));
    	parameters.put("supplier", orderDto.getSupplierName());

    	parameters.put("receivedAt", receiveDto.getReceivedAt() + " - " + loyaltyCardService.getInventoryDesc(receiveDto.getReceivedAt()));
    	parameters.put("receiveDate", FULL_DATE_PATTERN.print(receiveDto.getReceiveDate()));
    	parameters.put("receipt", receiveDto.getReceiptNo());
    	
    	parameters.put("receivedBy", UserUtil.getCurrentUser().getUsername());
    	
    	List<ProductSeriesDto> products = receiveDto.getProducts();
    	if (products.isEmpty()) {
            parameters.put("isEmpty", true);
            products.add(new ProductSeriesDto());
        }
        DefaultJRProcessor jrProcessor = new DefaultJRProcessor("reports/manufacture_order_receive.jasper", products);
        jrProcessor.addParameters(parameters);
        jrProcessor.setFileResolver(new ClasspathFileResolver("reports"));
        return jrProcessor;
    }
}
