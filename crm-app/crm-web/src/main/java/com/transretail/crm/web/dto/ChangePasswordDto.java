package com.transretail.crm.web.dto;

import org.hibernate.validator.constraints.NotEmpty;

import com.transretail.crm.core.validator.Confirm;
import com.transretail.crm.core.validator.Password;

/**
 * @author Allan G. Ramirez (agramirez@exist.com)
 */
@Confirm(field = "password")
public class ChangePasswordDto {
	@NotEmpty
	private String currentPassword;
    @Password
    private String newPassword;
    @NotEmpty
    private String confirmPassword;
    private String password;
    
    public String getPassword() {
    	return password;
    }

    public String getNewPassword() {
        return newPassword;
    }

    public void setNewPassword(String newPassword) {
        this.newPassword = newPassword;
        this.password = newPassword;
    }

    public String getConfirmPassword() {
        return confirmPassword;
    }

    public void setConfirmPassword(String confirmPassword) {
        this.confirmPassword = confirmPassword;
    }

	public String getCurrentPassword() {
		return currentPassword;
	}

	public void setCurrentPassword(String currentPassword) {
		this.currentPassword = currentPassword;
	}
}
