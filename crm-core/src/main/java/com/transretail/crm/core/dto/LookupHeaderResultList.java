package com.transretail.crm.core.dto;

import com.transretail.crm.common.service.dto.response.AbstractResultListDTO;
import com.transretail.crm.core.entity.lookup.LookupHeader;
import org.springframework.data.domain.Page;

/**
 *
 */
public class LookupHeaderResultList extends AbstractResultListDTO<LookupHeader> {
    public LookupHeaderResultList(Page<LookupHeader> page) {
        super(page);
    }
}
