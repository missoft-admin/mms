package com.transretail.crm.core.entity.enums;

public enum DataAnalysisLMHBy {

	TXN_AMOUNT("txnAmount"),
	VISIT_FREQ("visitFreq");

    private DataAnalysisLMHBy(String code){
    	this.code = code;
    }

    private final String code;

	public String getCode() {
		return code;
	}
    
    
}
