package com.transretail.crm.core.dto;

import com.transretail.crm.core.entity.PosTxItem;
import com.transretail.crm.core.entity.lookup.Product;

/**
 * @author Allan G. Ramirez (agramirez@exist.com)
 */
public class PosTxItemDto {
    private String id;
    private String pluId;
    private String sku;
    private String productId;
    private String itemCode;
    private String itemDesc;
    private Double itemPrice;
    private Double totalPrice;
    private Double quantity;
    private String salesType;

    public PosTxItemDto() {

    }

    public PosTxItemDto(PosTxItem posTxItem) {
        this.id = posTxItem.getId();
        this.productId = posTxItem.getProductId();
        this.quantity = posTxItem.getQuantity();
        this.salesType = posTxItem.getSalesType();
    }

    public PosTxItemDto(PosTxItem posTxItem, Product prod) {
        this.id = posTxItem.getId();
        this.productId = posTxItem.getProductId();
        if( prod != null ) {
        	this.pluId = prod.getId();
        	this.sku = prod.getSku();
        	this.itemCode = prod.getItemCode();
        	this.itemDesc = prod.getDescription();
            this.itemPrice = posTxItem.getUnitPrice();//prod.getCurrentPrice();
            this.totalPrice = posTxItem.getTotalPrice();
            //posTxItem.getUnitPrice() * posTxItem.getQuantity();//prod.getCurrentPrice() * posTxItem.getQuantity();	
        }
        this.quantity = posTxItem.getQuantity();
        this.salesType = posTxItem.getSalesType();
    }

    public String getId() {
        return id;
    }

    public String getPluId() {
		return pluId;
	}

	public String getSku() {
		return sku;
	}

	public String getProductId() {
        return productId;
    }

    public String getItemDesc() {
		return itemDesc;
	}

	public Double getItemPrice() {
		return itemPrice;
	}

	public Double getTotalPrice() {
		return totalPrice;
	}

	public Double getQuantity() {
        return quantity;
    }

    public String getSalesType() {
        return salesType;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    public String getItemCode() {
		return itemCode;
	}

	public void setItemCode(String itemCode) {
		this.itemCode = itemCode;
	}

	public void setQuantity(Double quantity) {
        this.quantity = quantity;
    }
}
