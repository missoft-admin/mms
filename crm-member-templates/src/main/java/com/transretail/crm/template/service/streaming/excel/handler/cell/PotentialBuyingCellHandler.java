package com.transretail.crm.template.service.streaming.excel.handler.cell;

import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.springframework.context.i18n.LocaleContextHolder;

import com.google.common.collect.Maps;
import com.transretail.crm.core.entity.MemberModel;
import com.transretail.crm.core.entity.embeddable.ProfessionalProfile;
import com.transretail.crm.template.service.MemberTemplateParseException;
import com.transretail.crm.template.service.streaming.excel.handler.sheet.AbstractSheetHandler;

/**
 * @author Allan G. Ramirez (agramirez@exist.com)
 */
public class PotentialBuyingCellHandler implements CellHandler {
    private static final Map<String, String> ALLOWED_VALUES = Maps.newHashMap();
    private AbstractSheetHandler sheetHandler;
    private String headerName;

    static {
        ALLOWED_VALUES.put("<5 MILLION", "<5 m");
        ALLOWED_VALUES.put("5-9.9 MILLION", "5-9.9 m");
        ALLOWED_VALUES.put("10-50 MILLION", "10-50 m");
        ALLOWED_VALUES.put(">50 MILLION", ">50 m");
    }

    public PotentialBuyingCellHandler(AbstractSheetHandler sheetHandler, String headerName) {
        this.sheetHandler = sheetHandler;
        this.headerName = headerName;
    }

    @Override
    public void handle(MemberModel dto, String cellText) throws MemberTemplateParseException {
        ProfessionalProfile professionalProfile = dto.getProfessionalProfile();
        if (professionalProfile == null) {
            professionalProfile = new ProfessionalProfile();
            dto.setProfessionalProfile(professionalProfile);
        }
        if (StringUtils.isNotBlank(cellText)) {
            String value = ALLOWED_VALUES.get(cellText.toUpperCase());
            if (value == null) {
                throw new MemberTemplateParseException(sheetHandler.getMessageSource().getMessage(
                    "member.template.messages.excel.list.values.invalid", new String[]{cellText, headerName,
                        StringUtils.join(ALLOWED_VALUES.keySet(), ", ")},
                    LocaleContextHolder.getLocale()
                ));
            }
            professionalProfile.setPotentialBuyingPerMonth(value);
        }
    }
}
