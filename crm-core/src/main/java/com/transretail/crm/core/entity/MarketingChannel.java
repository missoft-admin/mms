package com.transretail.crm.core.entity;


import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Table;

import org.hibernate.annotations.Type;
import org.joda.time.LocalDate;
import org.joda.time.LocalTime;

import com.transretail.crm.core.entity.support.CustomAuditableEntity;


@Table(name = "CRM_MARKETING_CHANNEL")
@Entity
public class MarketingChannel extends CustomAuditableEntity<Long> {

	private static final long serialVersionUID = 1L;

	public static final int MESSAGE_COUNT = 1000;
	public static final int PER_COUNT = 160;
	public static final String TIME_FORMAT = "HH:mm:ss";
	public static final String TIME_FORMAT_1 = "h:mm a";
	public static enum ChannelType { email, sms };
	public static enum ChannelStatus { DRAFT, ACTIVE, FORAPPROVAL, REJECTED, CANCELED };



    @Column(unique=true, length=20)
    private String code;

    @Column(length=100)
    private String description;

    @Column(length=MESSAGE_COUNT)
    private String message;

    @Column(name = "MEMBER_GROUP", length=4000)
	private String memberGroup;

	@Column(name = "PROMOTION", length=4000)
	private String promotion;

	@Column(name = "SCHEDULE")
    @Type(type = "org.jadira.usertype.dateandtime.joda.PersistentLocalDate")
	private LocalDate schedule;

    @Column(name = "SCHEDULE_TIME")
    @Type(type = "org.jadira.usertype.dateandtime.joda.PersistentLocalTime")
    private LocalTime time;

    @Column(name = "ADVERTISEMENT_FILENAME")
    private String filename;

    @Column(name = "ADVERTISEMENT_FILEID", length=20)
    private String fileId;

    @Column(name="CHANNEL_TYPE", nullable=false, length=20)
    @Enumerated(EnumType.STRING)
    private ChannelType type;

    @Column(length=20)
    @Enumerated(EnumType.STRING)
    private ChannelStatus status;



	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
    public String getMemberGroup() {
		return memberGroup;
	}
	public void setMemberGroup(String memberGroup) {
		this.memberGroup = memberGroup;
	}
	public String getPromotion() {
		return promotion;
	}
	public void setPromotion(String promotion) {
		this.promotion = promotion;
	}
	public LocalDate getSchedule() {
		return schedule;
	}
	public void setSchedule(LocalDate schedule) {
		this.schedule = schedule;
	}
	public LocalTime getTime() {
		return time;
	}
	public void setTime(LocalTime time) {
		this.time = time;
	}
	public String getFilename() {
		return filename;
	}
	public void setFilename(String filename) {
		this.filename = filename;
	}
	public String getFileId() {
		return fileId;
	}
	public void setFileId(String fileId) {
		this.fileId = fileId;
	}
	public ChannelType getType() {
		return type;
	}
	public void setType(ChannelType type) {
		this.type = type;
	}
	public ChannelStatus getStatus() {
		return status;
	}
	public void setStatus(ChannelStatus status) {
		this.status = status;
	}

}
