package com.transretail.crm.common.service.exception;

/**
 * Exception thrown by transretail when there are any unexpected behaviors.
 *
 * @author Allan G. Ramirez (agramirez@exist.com)
 */
public class GenericServiceException extends RuntimeException {
    public GenericServiceException(String message) {
        super(message);
    }

    public GenericServiceException(Throwable e) {
        super(e);
    }

    public GenericServiceException(String message, Throwable e) {
        super(message, e);
    }
}
