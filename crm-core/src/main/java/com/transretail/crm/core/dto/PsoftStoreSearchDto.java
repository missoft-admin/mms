package com.transretail.crm.core.dto;


import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.mysema.query.types.expr.BooleanExpression;
import com.transretail.crm.common.service.dto.request.AbstractSearchFormDto;
import com.transretail.crm.core.entity.QPeopleSoftStore;


public class PsoftStoreSearchDto extends AbstractSearchFormDto {

	private String peoplesoftCode;
    private String storeCode;
    private String storeName;
    private String storeLike;
    private String storeNameLike;
	private String buCode;
	private String buName;



	@Override
	@JsonIgnore
	public BooleanExpression createSearchExpression() {
		List<BooleanExpression> expr = new ArrayList<BooleanExpression>();
		QPeopleSoftStore qModel = QPeopleSoftStore.peopleSoftStore;

		if ( StringUtils.isNotBlank( storeLike ) ) {
			expr.add( qModel.store.name.containsIgnoreCase( storeLike ).or( qModel.store.code.containsIgnoreCase( storeLike ) ) );
		}
		if ( StringUtils.isNotBlank( storeNameLike ) ) {
			expr.add( qModel.store.name.containsIgnoreCase( storeNameLike ) );
		}

		return BooleanExpression.allOf( expr.toArray( new BooleanExpression[expr.size()] ) );
	}



	public String getPeoplesoftCode() {
		return peoplesoftCode;
	}
	public void setPeoplesoftCode(String peoplesoftCode) {
		this.peoplesoftCode = peoplesoftCode;
	}
	public String getStoreCode() {
		return storeCode;
	}
	public void setStoreCode(String storeCode) {
		this.storeCode = storeCode;
	}
	public String getStoreName() {
		return storeName;
	}
	public void setStoreName(String storeName) {
		this.storeName = storeName;
	}
	public String getStoreLike() {
		return storeLike;
	}
	public void setStoreLike(String storeLike) {
		this.storeLike = storeLike;
	}
	public String getStoreNameLike() {
		return storeNameLike;
	}
	public void setStoreNameLike(String storeNameLike) {
		this.storeNameLike = storeNameLike;
	}
	public String getBuCode() {
		return buCode;
	}
	public void setBuCode(String buCode) {
		this.buCode = buCode;
	}
	public String getBuName() {
		return buName;
	}
	public void setBuName(String buName) {
		this.buName = buName;
	}

}
