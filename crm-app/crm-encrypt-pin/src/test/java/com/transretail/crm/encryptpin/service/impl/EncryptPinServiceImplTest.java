package com.transretail.crm.encryptpin.service.impl;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.TransactionCallbackWithoutResult;
import org.springframework.transaction.support.TransactionTemplate;

import com.google.common.collect.Lists;
import com.transretail.crm.common.util.SimpleEncrytor;
import com.transretail.crm.encryptpin.entity.MemberPinModel;
import com.transretail.crm.encryptpin.service.EncryptPinService;

/**
 * @author Allan G. Ramirez (agramirez@exist.com)
 */
@ContextConfiguration(locations = {"classpath:applicationContext-test.xml"})
@RunWith(SpringJUnit4ClassRunner.class)
public class EncryptPinServiceImplTest {
    @PersistenceContext
    private EntityManager em;
    @Autowired
    private PlatformTransactionManager transactionManager;

    @Autowired
    private EncryptPinService service;

    @Test
    public void encryptAllPinsTest() throws Exception {
        final String rawPin1 = "pin1";
        final String rawPin2 = "pin2";

        final List<Long> ids = Lists.newLinkedList();

        TransactionTemplate transactionTemplate = new TransactionTemplate(transactionManager);
        transactionTemplate.execute(new TransactionCallbackWithoutResult() {
            @Override
            protected void doInTransactionWithoutResult(TransactionStatus status) {
                MemberPinModel member = new MemberPinModel();
                member.setPin(rawPin1);
                em.persist(member);
                ids.add(member.getId());
                member = new MemberPinModel();
                member.setPin(rawPin2);
                em.persist(member);
                ids.add(member.getId());
                em.flush();
                em.clear();
            }
        });

        try {
            service.encryptAllPins();
        } catch (Exception e) {
            //
        }

        MemberPinModel member1 = em.find(MemberPinModel.class, ids.get(0));
        MemberPinModel member2 = em.find(MemberPinModel.class, ids.get(1));

        assertEquals(SimpleEncrytor.getInstance().encrypt(rawPin1), member1.getPin());
        assertNotNull(member1.getLastUpdated());
        assertEquals("SYSTEM", member1.getLastUpdateUser());
        assertEquals(SimpleEncrytor.getInstance().encrypt(rawPin2), member2.getPin());
        assertNotNull(member2.getLastUpdated());
        assertEquals("SYSTEM", member2.getLastUpdateUser());
    }
}
