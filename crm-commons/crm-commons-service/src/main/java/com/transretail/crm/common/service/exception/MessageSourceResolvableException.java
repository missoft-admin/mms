package com.transretail.crm.common.service.exception;

import org.springframework.context.MessageSourceResolvable;

import com.fasterxml.jackson.annotation.JsonIgnore;

/**
 * @author Allan G. Ramirez (agramirez@exist.com)
 */
public class MessageSourceResolvableException extends Exception implements MessageSourceResolvable {
    private String[] codes;

    private Object[] arguments;

    private String defaultMessage;

    public MessageSourceResolvableException() {

    }

    public MessageSourceResolvableException(String code, String[] arguments, String defaultMessage) {
        super(defaultMessage);
        this.codes = new String[]{code};
        this.arguments = arguments;
        this.defaultMessage = defaultMessage;
    }

    public MessageSourceResolvableException(String code, String[] arguments, String defaultMessage, Exception e) {
        super(defaultMessage, e);
        this.codes = new String[]{code};
        this.arguments = arguments;
        this.defaultMessage = defaultMessage;
    }

    @JsonIgnore
    public String getLastCode() {
        return codes[codes.length - 1];
    }

    @Override
    public String[] getCodes() {
        return codes;
    }

    @Override
    public Object[] getArguments() {
        return arguments;
    }

    @Override
    public String getDefaultMessage() {
        return defaultMessage;
    }
}
