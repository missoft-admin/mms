package com.transretail.crm.giftcard.dto;

import java.math.BigDecimal;
import java.util.List;

import com.google.common.collect.Lists;
import com.transretail.crm.giftcard.entity.B2BExhibitionDiscount;
import com.transretail.crm.giftcard.entity.B2BExhibitionLocation;


/**
 *
 */
public class B2BExhDiscDto {
	
	private Long locId;
	private BigDecimal minAmount;
	private BigDecimal maxAmount;
	private BigDecimal discount;
	private Long id;
	
	public B2BExhDiscDto() {}
	
	public B2BExhDiscDto(B2BExhibitionDiscount disc) {
		this.locId = disc.getLocation().getId();
		this.minAmount = disc.getMinAmount();
		this.maxAmount = disc.getMaxAmount();
		this.discount = disc.getDiscount();
		this.id = disc.getId();
	}
	
	public B2BExhibitionDiscount toModel(B2BExhibitionDiscount disc) {
		if(disc == null)
			disc = new B2BExhibitionDiscount();
		disc.setLocation(new B2BExhibitionLocation(this.locId));
		disc.setMinAmount(this.minAmount);
		disc.setMaxAmount(this.maxAmount);
		disc.setDiscount(this.discount);
		
		return disc;
	}
	
	public static List<B2BExhDiscDto> toDto(List<B2BExhibitionDiscount> discs) {
		List<B2BExhDiscDto> discDtos = Lists.newArrayList();
		for(B2BExhibitionDiscount disc: discs) {
			discDtos.add(new B2BExhDiscDto(disc));
		}
		return discDtos;
			
	}
	
	
	public BigDecimal getMinAmount() {
		return minAmount;
	}
	public void setMinAmount(BigDecimal minAmount) {
		this.minAmount = minAmount;
	}
	public BigDecimal getMaxAmount() {
		return maxAmount;
	}
	public void setMaxAmount(BigDecimal maxAmount) {
		this.maxAmount = maxAmount;
	}
	public BigDecimal getDiscount() {
		return discount;
	}
	public void setDiscount(BigDecimal discount) {
		this.discount = discount;
	}
	public Long getLocId() {
		return locId;
	}
	public void setLocId(Long locId) {
		this.locId = locId;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}
	
	
	
}

