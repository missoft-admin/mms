package com.transretail.crm.core.service.impl;

import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.google.common.collect.Sets;
import com.transretail.crm.core.dto.UserRolePermDto;
import com.transretail.crm.core.entity.QUserPermission;
import com.transretail.crm.core.entity.QUserRoleModel;
import com.transretail.crm.core.entity.QUserRolePermission;
import com.transretail.crm.core.entity.UserModel;
import com.transretail.crm.core.entity.UserPermission;
import com.transretail.crm.core.entity.UserRoleModel;
import com.transretail.crm.core.entity.UserRolePermission;
import com.transretail.crm.core.entity.enums.Status;
import com.transretail.crm.core.repo.UserPermissionRepo;
import com.transretail.crm.core.repo.UserRolePermissionRepo;
import com.transretail.crm.core.repo.UserRoleRepo;
import com.transretail.crm.core.service.CodePropertiesService;
import com.transretail.crm.core.service.UserRolePermissionService;

/**
 * @author Allan G. Ramirez (agramirez@exist.com)
 */
@Service
public class UserRolePermissionServiceImpl implements UserRolePermissionService {
    private static final String ROLE_PERM_SEPARATOR = "-";
    @Autowired
    private UserRoleRepo userRoleRepo;
    @Autowired
    private UserPermissionRepo userPermissionRepo;
    @Autowired
    private UserRolePermissionRepo userRolePermissionRepo;
    @Autowired
    private CodePropertiesService codePropertiesService;

    /**
     * {@inheritDoc}
     */
    @Override
    @Transactional(readOnly = true)
    public Map<UserRolePermDto, Set<UserRolePermDto>> listUserRolePermissions(long userId) {
        Map<UserRolePermDto, Set<UserRolePermDto>> result = Maps.newLinkedHashMap();

        QUserRoleModel qUserRoleModel = QUserRoleModel.userRoleModel;
        QUserPermission qUserPermission = QUserPermission.userPermission;
        QUserRolePermission qUserRolePermission = QUserRolePermission.userRolePermission;

        Map<String, UserRolePermDto> resultMap = Maps.newLinkedHashMap();

        for (UserRoleModel role : userRoleRepo.findAll(qUserRoleModel.enabled.isTrue().and(qUserRoleModel.order.isNotNull()) //null ORDER_NO will not show role on user role dialog 
            .and(qUserRoleModel.id.ne("ROLE_MEMBER")) // Task #85731,
            , qUserRoleModel.order.asc()
        )) {
            UserRolePermDto roleDto = new UserRolePermDto(role.getCode(), role.getDescription());
            
            if(StringUtils.isNotBlank(role.getSupervisorRoleCode())) {
            	UserRoleModel supervisor = userRoleRepo.findOne(role.getSupervisorRoleCode());
                if(supervisor.getEnabled()) {
                	UserRolePermDto supervisorDto = new UserRolePermDto(supervisor.getCode(), supervisor.getDescription());
                	supervisorDto.setStaff(roleDto.getCode());
                	roleDto.setSupervisor(supervisorDto);
                	resultMap.put(createRolePermTupleString(supervisor.getCode(), null), supervisorDto);
                	result.put(supervisorDto, new LinkedHashSet<UserRolePermDto>());
                }
                		
            }
            
            resultMap.put(createRolePermTupleString(role, null), roleDto);
            result.put(roleDto, new LinkedHashSet<UserRolePermDto>());
        }

        Iterable<UserPermission> test = userPermissionRepo.findAll(qUserPermission.status.eq(Status.ACTIVE)
                .and(qUserPermission.roleCategory.isNotNull())
                .and(qUserPermission.roleCategory.isNotEmpty()), qUserPermission.ordering.asc(), qUserPermission.description.asc());
            
        for (UserPermission perm : userPermissionRepo.findAll(qUserPermission.status.eq(Status.ACTIVE)
            .and(qUserPermission.roleCategory.isNotNull())
            .and(qUserPermission.roleCategory.isNotEmpty()), qUserPermission.ordering.asc(), qUserPermission.description.asc()
        )) {
        	List<String> permRoleCodes = Lists.newArrayList(perm.getRoleCategory().split(","));
            for (String roleCode : permRoleCodes) {
                UserRolePermDto roleDto = resultMap.get(roleCode);
                if(roleDto != null && !(roleDto.getStaff() != null && permRoleCodes.contains(roleDto.getStaff()))) { // In case role in CRM_USER_PERMISSION.ROLE_CATEGORY is not in db
                    UserRolePermDto permDto = new UserRolePermDto(perm.getCode(), perm.getDescription());
                    resultMap.put(createRolePermTupleString(roleCode, perm), permDto);
                    result.get(roleDto).add(permDto);
                }
            }
        }
        
        for (UserRolePermission rolePerm : userRolePermissionRepo.findAll(qUserRolePermission.userId.id.eq(userId))) {
            UserRolePermDto roleDto = resultMap.get(createRolePermTupleString(rolePerm.getRoleId(), rolePerm.getPermId()));
            if (roleDto != null) {
                roleDto.setCurrentRolePerm(true);
            }
        }
        return result;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    @Transactional
    public void saveUserRolePerm(long userId, List<String> currRolePermTuples) {
        QUserRolePermission qUserRolePermission = QUserRolePermission.userRolePermission;

        Map<String, UserRolePermission> prevRolePermMap = Maps.newHashMap();
        Set<String> prevRolePermTuples = Sets.newHashSet();
        for (UserRolePermission rolePerm : userRolePermissionRepo.findAll(qUserRolePermission.userId.id.eq(userId))) {
            String tupleString = createRolePermTupleString(rolePerm.getRoleId(), rolePerm.getPermId());
            prevRolePermMap.put(tupleString, rolePerm);
            prevRolePermTuples.add(tupleString);
        }

        for (String currRolePermTuple : currRolePermTuples) {
            if (prevRolePermTuples.contains(currRolePermTuple)) {
                prevRolePermMap.remove(currRolePermTuple);
            } else {
                UserRolePermission model = new UserRolePermission();
                model.setUserId(new UserModel(userId));
                String[] tuple = currRolePermTuple.split(ROLE_PERM_SEPARATOR);
                model.setRoleId(new UserRoleModel(tuple[0]));
                if (tuple.length > 1) {
                    model.setPermId(new UserPermission(tuple[1]));
                }
                userRolePermissionRepo.save(model);
            }
        }

        userRolePermissionRepo.delete(prevRolePermMap.values());
    }

    @Transactional(readOnly = true)
    @Override
    public Set<String> getMarchantServiceSupervisorEmails() {
        Set<String> result = Sets.newHashSet();
        QUserRolePermission qUserRolePermission = QUserRolePermission.userRolePermission;
        Iterable<UserRolePermission> iterable = userRolePermissionRepo.findAll(
            qUserRolePermission.roleId.id.eq(codePropertiesService.getMerchantServiceSupervisorCode())
                .and(qUserRolePermission.userId.email.isNotNull())
        );
        Iterator<UserRolePermission> it = iterable.iterator();
        while (it.hasNext()) {
            result.add(it.next().getUserId().getEmail());
        }
        return result;
    }

    private String createRolePermTupleString(UserRoleModel role, UserPermission perm) {
        StringBuilder builder = new StringBuilder();
        builder.append(role.getCode());
        if (perm != null) {
            builder.append(ROLE_PERM_SEPARATOR);
            builder.append(perm.getCode());
        }
        return builder.toString();
    }

    private String createRolePermTupleString(String roleCode, UserPermission perm) {
        StringBuilder builder = new StringBuilder();
        builder.append(roleCode);
        if (perm != null) {
            builder.append(ROLE_PERM_SEPARATOR);
            builder.append(perm.getCode());
        }
        return builder.toString();
    }
}
