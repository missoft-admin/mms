package com.transretail.crm.core.dto;

import org.springframework.web.multipart.MultipartFile;


public class MessageDto {

	public static enum MsgType { email, sms };

	private String subject;
	private String salutation;
	private String message;
	private String message1;
	private String emailAd;
	private String contactNo;
	private String[] recipients;
	private MultipartFile file;

	private MsgType msgType;



	public String getSubject() {
		return subject;
	}
	public void setSubject(String subject) {
		this.subject = subject;
	}
	public String getSalutation() {
		return salutation;
	}
	public void setSalutation(String salutation) {
		this.salutation = salutation;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public String getMessage1() {
		return message1;
	}
	public void setMessage1(String message1) {
		this.message1 = message1;
	}
	public String getEmailAd() {
		return emailAd;
	}
	public void setEmailAd(String emailAd) {
		this.emailAd = emailAd;
	}
	public String getContactNo() {
		return contactNo;
	}
	public void setContactNo(String contactNo) {
		this.contactNo = contactNo;
	}
	public String[] getRecipients() {
		return recipients;
	}
	public void setRecipients(String[] recipients) {
		this.recipients = recipients;
	}
	public MultipartFile getFile() {
		return file;
	}
	public void setFile(MultipartFile file) {
		this.file = file;
	}
	public MsgType getMsgType() {
		return msgType;
	}
	public void setMsgType(MsgType msgType) {
		this.msgType = msgType;
	}
}
