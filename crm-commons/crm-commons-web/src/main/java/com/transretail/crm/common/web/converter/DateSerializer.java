package com.transretail.crm.common.web.converter;

import java.io.IOException;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.apache.commons.lang3.StringUtils;
import org.joda.time.DateTime;
import org.joda.time.LocalDate;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;

/**
 *
 */
public class DateSerializer extends JsonSerializer<Date> {
    private static final SimpleDateFormat FORMATTER = new SimpleDateFormat("dd-MM-yyyy");

    public void serialize(Date date, JsonGenerator jsonGenerator, SerializerProvider serializerProvider) throws
        IOException {
        if (date != null) {
            jsonGenerator.writeString(FORMATTER.format(date));
        } else {
            jsonGenerator.writeString("");
        }
    }

}

