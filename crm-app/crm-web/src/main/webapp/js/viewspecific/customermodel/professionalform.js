var memberFormDialog = null;
var memberForm = null;
var updatePinDialog = null;
var updatePinForm = null;
var message_accountIdToBeGenerated = null;
var containerError = null;
var professional = null;
var member = null;
var employee = null;
var hasEmail = null;
var customerSegmentation = null;
var custSegSelect = null;
var childrenAge = null;
var childrenCount = null;
var acceptEmail = null;
var provinces = null;
var banks = null;

$(document).ready(function() {
	professionalFormDialog = $("#professionalFormDialog");
	professionalForm = $("#professionalForm");
	containerError = $(".errors");
	professional = $(".professionalDetails");
	member = $(".memberDetails");
	employee = $(".employeeDetails");
	hasEmail = $(".hasEmail");
	custSegSelect = $("#customerSegmentation");
	childrenAge = $("#childrenAge");
	childrenCount = $("#childrenCount");
	acceptEmail = $("#acceptEmail");
	banks = $(".banks");
	
	memberFormDialog.modal({
		show	:	false,
		keyboard:	false, 
		backdrop: "static" 
	});
	$( "#memberFormDialog" ).on( "hide", function(e) {
		if ( e.target === this ) {
			$('#birthdate').datepicker( "update", new Date() );
		}
	});
	$("#memberTypeSelect").hide();
	$("#employeeTypeSelect").hide();
	containerError.hide();
	professional.hide();
	$("#isSupplement").hide();
	
	updatePinDialog.modal({
		show	:	false,
		keyboard:	true
	});
	
	var nowTemp = new Date();
    var now = new Date(nowTemp.getFullYear(), nowTemp.getMonth(), nowTemp.getDate(), 0, 0, 0, 0);
    var birthdate = $('#birthdate');
    birthdate.datepicker({
		format		:	'dd-mm-yyyy',
		endDate		:	'-0d',
		autoclose	:	true
	});
	
    birthdate.change(function() {
		var pin = $('#pin');
		if(!$.trim(pin.val())) {
			var val = birthdate.val().split("-");
			pin.val(val[0] + val[1] + val[2].substr(2, 2));
//			console.log(pin.val());
		}
	});
	
	message_accountIdToBeGenerated = $(".accountId").html();
	
	$("#close_form").click(function(){
		memberFormDialog.modal("hide");
		memberForm[0].reset();
	});
	
	hasEmail.hide();
	$("#email").focusout(function() {
		var val = $(this).val();
		setUsernamePasswordRequired(val);
		toggleAcceptEmail(val);
	});
	$("#email").change(function() {
		var val = $(this).val();
		setUsernamePasswordRequired($(this).val());
		toggleAcceptEmail(val);
	});
	
	$("#customerGroup").change(function(){
		setSegmentationOptions($(this).val());
	});
	
	childrenCount.change(function() {
		childrenAgeFields($(this).val());
	});
	
	childrenCount.focusout(function() {
		childrenAgeFields($(this).val());
	});
	
	$(".floatInput").numberInput({
        "type" : "float"
    });
	$(".intInput").numberInput({
        "type" : "int"
    });
	
	$(".provinceSelect").change(function() {
		var province = $(this).val();
		$citySelect = $(this).closest(".provinceCityBlock").find(".citySelect");
		$citySelect.empty();
		$citySelect.append("<option></option>");
		if(province != "" && province != null) {
			var options = provinces[province];	
			if(options != null) {
				$.each(options, function(index, value) {
					$citySelect.append("<option value='" + value + "'>" + value + "</option>");
				});	
			}
		}
	});
	
	$("#hasCreditCard").change(function() {
		if($(this).is(':checked')) {
			banks.show();
		}
		else {
			banks.hide();
		}
	});
	banks.hide();
});

function toggleAcceptEmail(value) {
	console.log("email " + value);
	if(value) {
		acceptEmail.show();
	}
	else {
		acceptEmail.hide();
	}
}

function childrenAgeFields(count) {
	childrenAge.empty();
	for(var i = 0; i < count; i++) {
		childrenAge.append($("<input type='text' name='customerProfile.ageOfChildren' style='margin-bottom: 1px'/>"));
	}
	if(count > 4) {
		childrenAge.css('height', 'auto');
	}
}


function setCustomerSegmentation(map) {
	customerSegmentation = map;
}

function setProvinces(provinceMap) {
	provinces = provinceMap;
}



function setSegmentationOptions(key) {
	if(key == null || key == undefined) return;
	var options = customerSegmentation[key];
	custSegSelect.empty();
	$.each(options, function(k, value) {
		custSegSelect.append("<option value='" + k + "'>" + value + "</option>");
	});
}

function setUsernamePasswordRequired(val) {
	if($.trim(val)) {
		hasEmail.show();
	}
	else {
		hasEmail.hide();
	}
}

function hideErrors() {
	containerError.hide("slow");
}

function createProfessional(query) {
	populateMember("/professional/new");
	saveMember("/new", query);
}

function updateMember(memberId, query) {
	populateMember("/populate/" + memberId, false, memberId);
	saveMember("/update/" + memberId, query);
}

function updatePin(memberId, query) {
	containerError.hide();
	updatePinDialog.modal("show");
	updatePinForm.trigger( "reset" );
	
	$("#save_pin").unbind("click").click(function(){
		$.ajax({
			type 	: "POST",
			url 	: updatePinForm.attr("action") + "/pin/update/" + memberId,
			dataType: 'json',
			data 	: updatePinForm.serialize(),
			        
			success : function(inResponse) {
				if (inResponse.success) {
					containerError.hide('fast');
					updatePinForm.unbind("submit").submit( function() {
						if(!$.trim(query)) {
							query = "page=1&size=10";
						}
						$(this).attr("action", $(this).attr("action") + "?" + query);
					});
					updatePinForm.submit();
				} 
				else {
					errorInfo = "";
					for (var i = 0; i < inResponse.result.length; i++) {
						errorInfo += "<br>" + (i + 1) + ". "
								+ ( inResponse.result[i].code != undefined ? 
										inResponse.result[i].code : inResponse.result[i]);
					}
					$(".errorcontent").html("Please correct following errors: " + errorInfo);
					containerError.show('slow');
				}
			},
			error : function(jqXHR, textStatus, errorThrown) {
				containerError.html("Error: " + errorThrown);
			}
		});
	});
}

function saveMember(url, query) {
	$("#save_form").unbind("click").click(function() {
		$( "#memberFormDialog" ).find( "#save_form" ).prop( "disabled", true );
		//NPWP
		if($("#npwpId1").val() != "" || $("#npwpId2").val() != "" || 
				$("#npwpId3").val() != "" || $("#npwpId4").val() != "" || 
				$("#npwpId5").val() != "" || $("#npwpId6").val() != "") {
			var first = $("#npwpId1").val();
			if(first != "") {
				var padZero = 2 - first.length;
				if(padZero > 0) {
					for(var i = 0; i < padZero; i++)
						first = "0" + first;
				}	
			}
			var last = $("#npwpId6").val();
			if(last == "") {
				last = "000";	
			}
			var npwpId = first +
					$("#npwpId2").val() +
					$("#npwpId3").val() +
					$("#npwpId4").val() +
					$("#npwpId5").val() +
					last;
			$("#npwpId").val(npwpId);	
		}
//		console.log(memberForm.serialize());
		$.ajax({
			type 	: "POST",
			url 	: memberForm.attr("action") + url,
			dataType: 'json',
			data 	: memberForm.serialize(),
			        
			success : function(response) {
				$( "#memberFormDialog" ).find( "#save_form" ).prop( "disabled", false );
				if (response.success) {
					containerError.hide('fast');
                    memberFormDialog.modal("hide");
                    memberForm[0].reset();

                    // Ugly hack to reload the list
                    var $searchButton = $("#search_submit" );
                    if($searchButton.length > 0) {
                        $searchButton.click();
                    }
/*
    Allan: At this point, data is already saved. Consult me if there's a need to restore this block.
					memberForm.unbind("submit").submit( function() {
						$(this).attr("action", $(this).attr("action") + "?" + query);
					});
					memberForm.submit();
*/
				} 
				else {
					errorInfo = "";
					var count = 0;
					for (var i = 0; i < response.result.length; i++) {
						if(response.result[i].code != undefined && response.result[i].code != "typeMismatch") {
							count++;
							errorInfo += "<br>" + (count) + ". "
							+ ( response.result[i].code != undefined ? 
									response.result[i].code : response.result[i]);
							
						}
						
					}
					$(".errorcontent").html("<strong>Please correct following errors: </strong>" + errorInfo);
					containerError.show('slow');
				}
			},
			error : function(jqXHR, textStatus, errorThrown) {
				$( "#memberFormDialog" ).find( "#save_form" ).prop( "disabled", false );
				containerError.html("Error: " + errorThrown);
			}
		});
	});
}

function populateMember(url, isNew, memberId) {
	containerError.hide();
	$.ajax({
		type 	: "POST",
		url 	: memberForm.attr('action') + url,
		dataType: "json",
		success	: function(response) {
			var result = response.result;
			if(!response.success) {
				alert(result);
				memberFormDialog.modal("hide");
				return;
			}
			
			childrenAge.empty();
			$(".added-field").remove();

			$.each(result, function(key, value) {
				if ( key == "accountId" ) {
					$(".accountId").html(message_accountIdToBeGenerated);
					if ( value != null && value != "" ) {
						$(".accountId").html( value );
					}
					else {
						$(".accountId").html(message_accountIdToBeGenerated);
					}
				}

				if ( value != null && ( key == "address" || key == "channel" || key == "customerProfile" || key == "professionalProfile" ) ) {
                    $.each( value, function ( k, v ) {
                        if ( v != null && k == "businessAddress" ) {
                            $.each( v, function ( k1, v1 ) {
                                fillField( key + "." + k + "." + k1, v1 );
                            } );
                        }
                        fillField( key + "." + k, v );
                    } );
                }
				else if ( key == "registeredStore" ) {
					$.get( 
							$( "#content_registeredStore" ).data( "url-store" ) +(  memberId != undefined? memberId : "" ),
							function(data) {
								$( "#content_registeredStore" ).empty();
								$( "#content_registeredStoreStatic" ).hide();
								$.each( data.result, function( idx, store) {
									$( "#content_registeredStore" ).append( "<option value='" + store.code + "'>" + store.codeAndName + "</option>" );
								});
								$( "#content_registeredStore" ).val( value );
								if ( !memberId && result.memberType && result.memberType.description == "EMPLOYEE" ) {
									$( "#content_registeredStore" ).show();
								}
								else {
									if(value != null && value != "") {
										var registeredStoreDesc = $("#content_registeredStore option[value='"+value+"']").text();
										var storeValue = registeredStoreDesc;
										$( "#content_registeredStoreStatic" ).html(storeValue);
									}
									$( "#content_registeredStoreStatic" ).show();
									$( "#content_registeredStore" ).val(value);
//									$( "#content_registeredStore" ).html(value);
									$( "#content_registeredStore" ).hide();
								}
							},
							"json"
					);
				}
				else if ( key == "accountStatus" ) {
					$( "#accountStatus" ).prop( "checked", value == 'TERMINATED' );
				} else if ( key == "configurableFields" ) {
                    updateConfigurableFieldsDisplay($('.memberConfigurableFieldsContainer'), value);
                }
                else {
    				fillField(key, value);
                }
			});
			
			function populateCityProvince() {
				var name = $(this).attr("name");
				var val = result;
				$.each(name.split("."), function (index, value) {
					if(val[value] == null) {
						val = "";
						return false;
					} else 
						val = val[value];
				});
				if(val == null) {
					val = "";
				}
				var notFound = true;
				$(this).find("option").each(function() {
					var optionVal = $(this).attr("value");
					if(optionVal == null) {
						optionVal = "";
					}
					if(optionVal.toLowerCase() == val.toLowerCase()) {
						$(this).prop("selected", "selected");
						notFound = false;
						return false;
					}
				});
				if(notFound && val != "") {
					$(this).append($("<option value='"+val+"'>"+val+"</option>").prop("selected", "selected"));
				}
			}
			
			$(".provinceSelect").each(populateCityProvince);
			$(".provinceSelect").trigger("change");
			$(".citySelect").each(populateCityProvince);
			
			$("#mainPaneMemberType").find("select[name='memberType']").trigger("change");
			$("#firstTab").click();
			memberFormDialog.modal("show");
			containerError.hide();
			professional.hide();
			member.show();
			hasEmail.hide();
			if($("#email").val()) {
				hasEmail.show();
			}
			
			if(result.customerProfile.creditCardOwnership) {
				banks.show();
			}
			else {
				banks.hide();
			}
			
//			$("#memberTypeStatic").hide();
//			$("#memberTypeSelect").show();
			
			if(result.parent != null && result.parent != "") {
//				var parent = result.parent + " - " + result.parentName;
//				$("#parent").html(parent);
				$(".isPrimary").hide();
				$("#isSupplement").show();
			}
			else {
				$(".isPrimary").show();
				$("#isSupplement").hide();
				getSupplements(result.accountId);
			}

			if ( result.memberType == null || result.memberType != null && result.memberType.description != "EMPLOYEE" ) {
				$( "#accountEnabled" ).hide();
			}
			if(result.memberType != null) {
				if(result.memberType.description == "PROFESSIONAL") {
					professional.show();
					member.hide();
					employee.hide();
					
					setSegmentationOptions($("#customerGroup").val());
					
					if(result.professionalProfile.customerSegmentation) {
						custSegSelect.val(result.professionalProfile.customerSegmentation.code);
					}
				}
				$("#contentMemberType").html(result.memberType.description);
				$( "#employeeTypeSelect" ).show();
				if ( result.empType != null ) {
					$( "#employeeType" ).val( result.empType.code );
				}
			}
			else {
				$("#contentMemberType").html("INDIVIDUAL");
			}
		}
	});
}

function fillField(key, value) {
	
//	console.log(key + " " + value);
	if(key == "npwpId" && value != null && value != "") {
		var vals = value.replace('-', '.').split('.');
		if(vals.length > 1) { //for formatted db npwp data 
			for (var i = 0; i < vals.length; i++) {
				$("#npwpId" + (i+1)).val(vals[i]);
			}	
		} else {
			$("#npwpId1").val(value.substring(0,2));
			$("#npwpId2").val(value.substring(2,5));
			$("#npwpId3").val(value.substring(5,8));
			$("#npwpId4").val(value.substring(8,9));
			$("#npwpId5").val(value.substring(9,12));
			$("#npwpId6").val(value.substring(12,15));	
		}
	}
	
	if(key === 'customerProfile.ageOfChildren' && value !== null) {
		childrenAgeFields(value.length);
	}
	
	if((key === 'contact' || key === 'bestTimeToCall') && value !== null) {
//		value = value.split(',');

		for(var i = 1; i < value.length; i++) {
			addField(key);
		}
	}
	
	var name = $("[name='" + key + "']");

	if(({}).toString.call(value) == "[object Object]" && value.code !== undefined) {
		value = value.code;
	}
	
	if($.isArray(value)) {
		if(key === 'contact' || key == 'bestTimeToCall' || key === "customerProfile.ageOfChildren") {
			$.each(name, function(k, v) {
				$(v).val(value[k]);
			});
		}
		
		for(var i = 0; i < value.length; i++) {
			$("input[value='" + value[i].code + "']").prop("checked", true);
		}
	}
	else if(name.is("input[type='checkbox']")) {
//		console.log(key + " " + value);
		name.prop("checked", value);
	}
	else if(name != null) {
 		name.val( value );
 		if ( key.indexOf( "birthdate" ) != -1 ) {
 			try {
 				var dateValue = value ? 
 							( new Date( value ).getTime()? new Date( value ) 
 								: new Date( $.trim( value ).split( " " )[0] ).getTime()? 
 										new Date( $.trim( value ).split( " " )[0] ) : null ) 
 							: null;
 				$( "#birthdate" ).datepicker( "update", dateValue );
 			}catch(e) {}
 		}
	}
}

function addField(name) {
	$("#" + name).append($("<br class='added-field' /><input type='text' class='form-control added-field' name='" + name + "' style='margin-top: 1px;' />"));
}

function addFields() {
	addField('contact');
	addField('bestTimeToCall');
}

function getSupplements(accountId) {
	var memberList = $("#supplements");
	memberList.html("");
	
	var headers = {
		username: memberList.data("hdrUsername")  + "",
		accountno: memberList.data("hdrAccountno") + "",
		points:	memberList.data("hdrPoints") + "",
		name: memberList.data("hdrMember") + ""
	};
	
	table = memberList.ajaxDataTable({
		'autoload': true,
		'ajaxSource': memberList.data("url") + accountId,
		'columnHeaders': [
			headers['username'],
			headers['accountno'],
			headers['name'],
			headers['points'],
		],
		'modelFields': [
		    {name: 'username'},
		    {name: 'accountId'},
		    {name : 'firstName', sortable : true, fieldCellRenderer : function (data, type, row) {
		           return row.formattedMemberName;
	         }},
		    {name: 'totalPoints'}
		]
	});
}


/*LOYALTY CARD ASSIGNMENT*/
function assignLoyaltyCard( inUrl, inMemberId ) {
	if($("#list_member").data("userInventory") == null || $("#list_member").data("userInventory") == "") {
		$("#confirmbox #confirmFalse").hide();
		$("#confirmbox").on('hidden.bs.modal', function() {
			$("#confirmbox #confirmFalse").show();
		});
		getConfirm($("#list_member").data("noInventorylocationMsg"), function(ret) {});
	} else {
		$.ajax({
			cache 		: false, 
			dataType 	: "html",
			type 		: "GET", 
			url 		: inUrl,
			error 		: function(jqXHR, textStatus, errorThrown) {}, 
			success 	: function( inResponse ) {
				$("#form_loyaltyCardAssignment").html( inResponse );
			}
		});	
	}
	
}