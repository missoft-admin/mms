$(document).ready( function() {

	var theErrorContainer = $( "#content_approverForm" ).find("#content_error");

	var $theApproverDialog = $( "#content_approverForm" );
	var $theApproverForm = $( "#approverForm" );



	initFormFields();
	initDialog();
	


	function initFormFields() {

		$theApproverForm.submit( function(e) {
			e.preventDefault();
			saveApprover( $(this).attr("action") );
		});
	}

	function initDialog() {

		$theApproverDialog.modal({});
		$theApproverDialog.on( "hide", function() { theErrorContainer.hide(); });
		
		$(".floatInput").numberInput({
            "type" : "float"
        });
		$(".intInput").numberInput({
            "type" : "int"
        });
	}



	function saveApprover( inUrl ) {
	    $( "#saveButton" ).prop( "disabled", true );
		$.ajax({
			cache 		: false, 
			dataType 	: "json",
			type 		: "POST", 
			url 		: inUrl,
			data		: $theApproverForm.serialize(),
			error 		: function(jqXHR, textStatus, errorThrown) {},
			success 	: function( inResponse ) {	
				if ( inResponse.success ) {
					$theApproverDialog.modal( "hide" );
					$theApproverDialog.remove();
					 $( "#list_approver" ).ajaxDataTable( "search" ); //window.location.href = $theApproverForm.data( "url" );
				}
				else {
				    $( "#saveButton" ).prop( "disabled", false );
					var theErrorInfo = "";
					$.each( inResponse.result, function( key, value ) {
						theErrorInfo += ( ( key + 1 ) + ". " + value.code + "<br>" );
					});
					theErrorContainer.find( "div" ).html( theErrorInfo );
					theErrorContainer.show( "slow" );
				}
			}
		});
	}

});



