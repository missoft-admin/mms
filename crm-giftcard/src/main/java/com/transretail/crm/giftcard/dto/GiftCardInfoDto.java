package com.transretail.crm.giftcard.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.transretail.crm.common.web.converter.FormattedIntegerNumberSerializer;
import com.transretail.crm.common.web.converter.LocalDateSerializer;
import com.transretail.crm.common.web.converter.LocalDateTimeSerializer;
import org.joda.time.LocalDate;
import org.joda.time.LocalDateTime;

/**
 *
 * @author Monte Cillo Co (mco@exist.com)
 */
public class GiftCardInfoDto {

    private String cardNo;
    private String status;
    private String customerId;
    private String orderNo;
    private String product;
    @JsonSerialize(using = LocalDateTimeSerializer.class)
    private LocalDateTime activationTime;
    private String activatedUser;
    private String saleStore;
    private String terminalId;
    @JsonSerialize(using = FormattedIntegerNumberSerializer.class)
    private Double faceValue;
    private boolean reloadable;
    private String inventoryLocation;
    @JsonSerialize(using = LocalDateTimeSerializer.class)
    private LocalDateTime createdDateTime;
    private String createdBy;
    private String moNo;
    private String allocatedTo;
    @JsonInclude(Include.ALWAYS)
    @JsonSerialize(using = FormattedIntegerNumberSerializer.class)
    private Double balance;
    @JsonInclude(Include.ALWAYS)
    @JsonSerialize(using = FormattedIntegerNumberSerializer.class)
    private Double previousBalance;
    @JsonSerialize(using = LocalDateSerializer.class)
    private LocalDate expiryDate;
    private boolean allowPartialRedeem;
    @JsonInclude(Include.ALWAYS)
    @JsonSerialize(using = FormattedIntegerNumberSerializer.class)
    private Double unitCost;

    public GiftCardInfoDto cardNo(String cardNo) {
	this.cardNo = cardNo;
	return this;
    }

    public GiftCardInfoDto status(String status) {
	this.status = status;
	return this;
    }

    public GiftCardInfoDto createdDateTime(LocalDateTime createdDateTime) {
	this.createdDateTime = createdDateTime;
	return this;
    }

    public GiftCardInfoDto createdBy(String createdBy) {
	this.createdBy = createdBy;
	return this;
    }

    public GiftCardInfoDto moNo(String moNo) {
	this.moNo = moNo;
	return this;
    }

    public GiftCardInfoDto allocatedTo(String allocatedTo) {
	this.allocatedTo = allocatedTo;
	return this;
    }

    public GiftCardInfoDto balance(Double balance) {
	this.balance = balance;
	return this;
    }

    public GiftCardInfoDto previousBalance(Double previousBalance) {
	this.previousBalance = previousBalance;
	return this;
    }

    public GiftCardInfoDto expiryDate(LocalDate expiryDate) {
	this.expiryDate = expiryDate;
	return this;
    }

    public GiftCardInfoDto
	    allowPartialRedeem(Boolean allowPartialRedeem) {
	this.allowPartialRedeem = allowPartialRedeem;
	return this;
    }

    public GiftCardInfoDto customerId(String customerId) {
	this.customerId = customerId;
	return this;
    }

    public GiftCardInfoDto orderNo(String orderNo) {
	this.orderNo = orderNo;
	return this;
    }

    public GiftCardInfoDto product(String product) {
	this.product = product;
	return this;
    }

    public GiftCardInfoDto activationTime(LocalDateTime activationTime) {
	this.activationTime = activationTime;
	return this;
    }

    public GiftCardInfoDto activateUser(String activateUser) {
	this.activatedUser = activateUser;
	return this;
    }

    public GiftCardInfoDto saleStore(String saleStore) {
	this.saleStore = saleStore;
	return this;
    }

    public GiftCardInfoDto terminalId(String terminalId) {
	this.terminalId = terminalId;
	return this;
    }

    public GiftCardInfoDto faceValue(Double faceValue) {
	this.faceValue = faceValue;
	return this;
    }

    public GiftCardInfoDto reloadable(boolean reloadable) {
	this.reloadable = reloadable;
	return this;
    }

    public GiftCardInfoDto inventoryLocation(String inventoryLocation) {
	this.inventoryLocation = inventoryLocation;
	return this;
    }

    public GiftCardInfoDto unitCost(Double unitCost) {
	this.unitCost = unitCost;
	return this;
    }

    public String getCardNo() {
	return cardNo;
    }

    public String getStatus() {
	return status;
    }

    public String getCustomerId() {
	return customerId;
    }

    public String getOrderNo() {
	return orderNo;
    }

    public String getProduct() {
	return product;
    }

    public LocalDateTime getActivationTime() {
	return activationTime;
    }

    public String getActivatedUser() {
	return activatedUser;
    }

    public String getSaleStore() {
	return saleStore;
    }

    public String getTerminalId() {
	return terminalId;
    }

    public Double getFaceValue() {
	return faceValue;
    }

    public boolean isReloadable() {
	return reloadable;
    }

    public String getInventoryLocation() {
	return inventoryLocation;
    }

    public LocalDateTime getCreatedDateTime() {
	return createdDateTime;
    }

    public void setCreatedDateTime(LocalDateTime createdDateTime) {
	this.createdDateTime = createdDateTime;
    }

    public String getCreatedBy() {
	return createdBy;
    }

    public void setCreatedBy(String createdBy) {
	this.createdBy = createdBy;
    }

    public String getMoNo() {
	return moNo;
    }

    public void setMoNo(String moNo) {
	this.moNo = moNo;
    }

    public String getAllocatedTo() {
	return allocatedTo;
    }

    public void setAllocatedTo(String allocatedTo) {
	this.allocatedTo = allocatedTo;
    }

    public Double getBalance() {
	return balance;
    }

    public void setBalance(Double balance) {
	this.balance = balance;
    }

    public Double getPreviousBalance() {
	return previousBalance;
    }

    public void setPreviousBalance(Double previousBalance) {
	this.previousBalance = previousBalance;
    }

    public LocalDate getExpiryDate() {
	return expiryDate;
    }

    public void setExpiryDate(LocalDate expiryDate) {
	this.expiryDate = expiryDate;
    }

    public boolean isAllowPartialRedeem() {
	return allowPartialRedeem;
    }

    public void setAllowPartialRedeem(boolean allowPartialRedeem) {
	this.allowPartialRedeem = allowPartialRedeem;
    }

    public Double getUnitCost() {
	return unitCost;
    }

    public void setUnitCost(Double unitCost) {
	this.unitCost = unitCost;
    }

}
