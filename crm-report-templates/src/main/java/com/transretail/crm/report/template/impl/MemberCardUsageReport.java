package com.transretail.crm.report.template.impl;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import net.sf.jasperreports.engine.JRParameter;
import net.sf.jasperreports.engine.JRVirtualizer;
import net.sf.jasperreports.engine.fill.JRSwapFileVirtualizer;
import net.sf.jasperreports.engine.util.JRSwapFile;

import org.joda.time.DateTime;
import org.joda.time.LocalDate;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.MessageSourceAware;
import org.springframework.context.support.MessageSourceAccessor;
import org.springframework.stereotype.Service;

import com.mysema.query.BooleanBuilder;
import com.mysema.query.jpa.JPQLQuery;
import com.mysema.query.jpa.impl.JPAQuery;
import com.transretail.crm.common.reporting.jasper.ClasspathFileResolver;
import com.transretail.crm.common.reporting.jasper.DefaultJRProcessor;
import com.transretail.crm.common.reporting.jasper.JRProcessor;
import com.transretail.crm.common.util.DateUtil;
import com.transretail.crm.core.dto.MemberSearchDto;
import com.transretail.crm.core.entity.QMemberModel;
import com.transretail.crm.core.entity.QPointsTxnModel;
import com.transretail.crm.core.entity.enums.MemberStatus;
import com.transretail.crm.core.entity.enums.ReportType;
import com.transretail.crm.core.security.util.UserUtil;
import com.transretail.crm.core.service.MemberService;
import com.transretail.crm.core.service.StoreService;
import com.transretail.crm.report.template.CommonReportFilter;
/**
 * @author ftopico
 */

@Service("memberCardUsageReport")
public class MemberCardUsageReport implements MessageSourceAware {
    
    @Autowired
    MemberService memberService;
    @Autowired
    StoreService storeService;
    @PersistenceContext
    private EntityManager em;

    protected static final String REPORT_NAME_CARD_USAGE_WEEKLY = "reports/member_card_usage_weekly_report.jasper";
    protected static final String REPORT_NAME_CARD_USAGE_MONTHLY = "reports/member_card_usage_monthly_report.jasper";
    protected static final String FILTER_STANDARD_REPORT_TYPE = "standardReportType";
    protected static final String FILTER_START_WEEK = "startWeek";
    protected static final String CARD_USAGE_REPORT_TYPE_WEEKLY = "weekly";
    protected static final String CARD_USAGE_REPORT_TYPE_MONTHLY = "monthly";
    protected static final String PARAM_MONTH_FROM = "monthFrom";
    protected static final String PARAM_MONTH_TO = "monthTo";
    
    //Header Params
    private static final String PARAM_PRINT_DATE = "printedDate";
    private static final String PARAM_PRINT_BY = "printedBy";
    private static final DateTimeFormatter FULL_DATE_PATTERN = DateTimeFormat.forPattern("MMMM d, yyyy");
    private MessageSourceAccessor messageSource;
    
    public ReportType[] getReportTypes() {
        return ReportType.values();
    }
    
    @Override
    public void setMessageSource(MessageSource messageSource) {
        this.messageSource = new MessageSourceAccessor(messageSource);
    }
    
    public JRProcessor createJrProcessor(Map<String, String> map) {

        Integer year = Integer.parseInt(map.get(CommonReportFilter.YEAR));
        Integer week = (map.get(CommonReportFilter.WEEK) != null) ? Integer.parseInt(map.get(CommonReportFilter.WEEK)) : null;
        Integer month = (map.get(CommonReportFilter.MONTH) != null) ? Integer.parseInt(map.get(CommonReportFilter.MONTH)) : null;
        Integer startWeek = (map.get(FILTER_START_WEEK) != null) ? Integer.parseInt(map.get(FILTER_START_WEEK)) : null;
        String standardReportType = map.get(FILTER_STANDARD_REPORT_TYPE);
        String store = map.get(CommonReportFilter.STORE);
        SimpleDateFormat monthFormat = new SimpleDateFormat("MMMM");
        
        QPointsTxnModel qPointsTxn = QPointsTxnModel.pointsTxnModel;
        QMemberModel qMemberModel = QMemberModel.memberModel;
        Map<String, Object> parameters = new HashMap<String, Object>();
        List<MemberCardsUsageReportModel> list = new ArrayList<MemberCardUsageReport.MemberCardsUsageReportModel>();
        
        BooleanBuilder predicate = new BooleanBuilder(qPointsTxn.storeCode.isNotNull())
                                            .and(qMemberModel.isNotNull())
                                            .and(qMemberModel.loyaltyCardNo.isNotNull());
        
        if (store != null) {
            predicate.and(qPointsTxn.storeCode.eq(store));
        }
            
        
        if (standardReportType.equals(CARD_USAGE_REPORT_TYPE_WEEKLY)) {
            for (int ctr = week; ctr <= week+8; ctr++) {
                MemberCardsUsageReportModel memberCardsUsageReportModel = new MemberCardsUsageReportModel();
                BooleanBuilder innerPredicate = new BooleanBuilder(predicate);
                
                Date dateFrom = null;
                Date dateTo = null;
                if (ctr != 1)
                    dateFrom = DateUtil.getWeekDate(ctr, startWeek, year);
                else
                    dateFrom = DateUtil.getFirstDateOfYear(year);
                
                if (ctr != 52)
                    dateTo = DateUtil.getEndOfWeek(ctr, startWeek, year);
                else
                    dateTo = DateUtil.getLastDateOfYear(year);
                
                innerPredicate.and(qPointsTxn.transactionDateTime.between(dateFrom, dateTo));
                
                JPQLQuery query = new JPAQuery(em).from(qPointsTxn)
                        .rightJoin(qPointsTxn.memberModel, qMemberModel)
                        .where(innerPredicate);
                
                Long memberCardUsage = query.uniqueResult(qPointsTxn.memberModel.id.countDistinct());
                
                MemberSearchDto searchDto = new MemberSearchDto();
                searchDto.setLoyaltyCardNoNotNull(true);
                searchDto.setCreatedTo(new DateTime(dateTo));
                searchDto.setStatus(MemberStatus.ACTIVE);
                
                memberCardsUsageReportModel.setMaxMembers(memberService.countMembers(searchDto));
                memberCardsUsageReportModel.setWeek(ctr);
                memberCardsUsageReportModel.setCardUsage(memberCardUsage);
                if (memberCardsUsageReportModel.getMaxMembers() > 0)
                    memberCardsUsageReportModel.setPercentage(memberCardUsage.doubleValue()/memberCardsUsageReportModel.getMaxMembers().doubleValue());
                else 
                    memberCardsUsageReportModel.setPercentage(0.0);
                
                list.add(memberCardsUsageReportModel);
            }
        } else if (standardReportType.equals(CARD_USAGE_REPORT_TYPE_MONTHLY)) {
            for (int ctr = month; ctr <= month+8; ctr++) {
                MemberCardsUsageReportModel memberCardsUsageReportModel = new MemberCardsUsageReportModel();
                BooleanBuilder innerPredicate = new BooleanBuilder(predicate);
                
                Date dateFrom = DateUtil.getBeginningOfMonth(year, ctr);
                Date dateTo = DateUtil.getEndOfMonth(year, ctr);
                
                innerPredicate.and(qPointsTxn.transactionDateTime.between(dateFrom, dateTo));
                
                JPQLQuery query = new JPAQuery(em).from(qPointsTxn)
                        .rightJoin(qPointsTxn.memberModel, qMemberModel)
                        .where(innerPredicate);
                
                Long memberCardUsage = query.uniqueResult(qPointsTxn.memberModel.id.countDistinct());
                
                MemberSearchDto searchDto = new MemberSearchDto();
                searchDto.setLoyaltyCardNoNotNull(true);
                searchDto.setCreatedTo(new DateTime(dateTo));
                searchDto.setStatus(MemberStatus.ACTIVE);
                
                memberCardsUsageReportModel.setMaxMembers(memberService.countMembers(searchDto));
                memberCardsUsageReportModel.setMonth(monthFormat.format(DateUtil.getBeginningOfMonth(year, ctr)));
                memberCardsUsageReportModel.setCardUsage(memberCardUsage);
                if (memberCardsUsageReportModel.getMaxMembers() > 0)
                    memberCardsUsageReportModel.setPercentage(memberCardUsage.doubleValue()/memberCardsUsageReportModel.getMaxMembers().doubleValue());
                else 
                    memberCardsUsageReportModel.setPercentage(0.0);
                
                list.add(memberCardsUsageReportModel);
            }
        }
        
        parameters.put(CommonReportFilter.YEAR, year);
        parameters.put(CommonReportFilter.WEEK, week);
        
        if (month != null) {
            parameters.put(PARAM_MONTH_FROM, monthFormat.format(DateUtil.getBeginningOfMonth(year, month)));
            parameters.put(PARAM_MONTH_TO, monthFormat.format(DateUtil.getBeginningOfMonth(year, month + 8)));
        } else {
            SimpleDateFormat sdf = new SimpleDateFormat("MMM dd, yyyy");
            Date dateFrom = null;
            Date dateTo = null;
            
            if (week != 1)
                dateFrom = DateUtil.getWeekDate(week, startWeek, year);
            else
                dateFrom = DateUtil.getFirstDateOfYear(year);
            
            if (week+9 != 52)
                dateTo = DateUtil.getEndOfWeek(week+8, startWeek, year);
            else
                dateTo = DateUtil.getLastDateOfYear(year);
            
            parameters.put("dateFrom", sdf.format(dateFrom));
            parameters.put("dateTo", sdf.format(dateTo));
        }
        
        
        if (store != null) {
            parameters.put(CommonReportFilter.STORE, storeService.getStoreByCode(store).getCodeAndName());
        }
        parameters.put(PARAM_PRINT_DATE, FULL_DATE_PATTERN.print(new LocalDate()));
        parameters.put(PARAM_PRINT_BY, getCurrentUser());
        
    	DefaultJRProcessor jrProcessor = null;
    	if (standardReportType.equalsIgnoreCase(CARD_USAGE_REPORT_TYPE_WEEKLY))
    	    jrProcessor = new DefaultJRProcessor(REPORT_NAME_CARD_USAGE_WEEKLY, list);
    	else if (standardReportType.equalsIgnoreCase(CARD_USAGE_REPORT_TYPE_MONTHLY))
    	    jrProcessor = new DefaultJRProcessor(REPORT_NAME_CARD_USAGE_MONTHLY, list);
    	
    	JRSwapFile swapFile = new JRSwapFile("/tmp", 1024, 1024);
        JRVirtualizer virtualizer = new JRSwapFileVirtualizer(50, swapFile, true);
        jrProcessor.addParameter(JRParameter.REPORT_VIRTUALIZER, virtualizer);
    	jrProcessor.addParameters(parameters);
    	jrProcessor.setFileResolver(new ClasspathFileResolver("reports"));
    	
    	return jrProcessor;
	
    }
    
    private String getCurrentUser() {
        if (UserUtil.getCurrentUser() != null) {
            return UserUtil.getCurrentUser().getUsername();
        } else {
            return null;
        }
    }

    public static class MemberCardsUsageReportModel {
        Integer week;
        String month;
        Long maxMembers;
        Long cardUsage;
        Double percentage;
        
        public Integer getWeek() {
            return week;
        }
        public Long getMaxMembers() {
            return maxMembers;
        }
        public Long getCardUsage() {
            return cardUsage;
        }
        public void setWeek(Integer week) {
            this.week = week;
        }
        public void setMaxMembers(Long maxMembers) {
            this.maxMembers = maxMembers;
        }
        public void setCardUsage(Long cardUsage) {
            this.cardUsage = cardUsage;
        }
        public Double getPercentage() {
            return percentage;
        }
        public void setPercentage(Double percentage) {
            this.percentage = percentage;
        }
        public String getMonth() {
            return month;
        }
        public void setMonth(String month) {
            this.month = month;
        }
        
    }
    
}
