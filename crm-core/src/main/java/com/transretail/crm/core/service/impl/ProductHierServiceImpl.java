package com.transretail.crm.core.service.impl;


import java.util.ArrayList;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.IteratorUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import com.mysema.query.types.expr.BooleanExpression;
import com.transretail.crm.core.dto.ProductHierDto;
import com.transretail.crm.core.dto.ProductHierSearchDto;
import com.transretail.crm.core.repo.ProductHierRepo;
import com.transretail.crm.core.service.ProductHierService;
import com.transretail.crm.product.entity.CrmProductHier;
import com.transretail.crm.product.entity.QCrmProductHier;


@Service("productHierService")
public class ProductHierServiceImpl implements ProductHierService {

    @Autowired
    ProductHierRepo productHierRepo;



    @Override
	public List<ProductHierDto> get( String level ) {
    	if ( StringUtils.isBlank( level ) ) {
    		return null;
    	}
    	ProductHierSearchDto dto = new ProductHierSearchDto();
    	dto.setLevel( level );
    	return search( dto );
    }

    @Override
	public List<ProductHierDto> get( String level, Integer idx, Integer size ) {
    	if ( StringUtils.isBlank( level ) ) {
    		return null;
    	}
    	else if ( null == idx && null == size ) {
    		return get( level );
    	}
    	ProductHierSearchDto dto = new ProductHierSearchDto();
    	dto.setLevel( level );
    	return search( dto, idx, size );
    }

    @Override
	public List<ProductHierDto> get( String level, String code ) {
    	if ( StringUtils.isBlank( level ) && StringUtils.isBlank( code ) ) {
    		return null;
    	}
    	else if ( StringUtils.isNotBlank( level ) && StringUtils.isBlank( code ) ) {
    		return get( level );
    	}
    	ProductHierSearchDto dto = new ProductHierSearchDto();
    	dto.setLevel( level );
    	dto.setCode( code );
    	dto.setCodeStart( code );
    	return search( dto );
    }

    @Override
	public List<ProductHierDto> get( List<String> filterCodes, List<String> codes, List<String> levels ) {
    	if ( CollectionUtils.isEmpty( filterCodes ) && CollectionUtils.isEmpty( codes )
    			&& CollectionUtils.isEmpty( levels ) ) {
    		return null;
    	}
    	ProductHierSearchDto dto = new ProductHierSearchDto();
    	dto.setCodesStart( filterCodes );
    	dto.setDescriptions( filterCodes );
    	dto.setCodes( codes );
    	dto.setLevels( levels );
    	return search( dto );
    }



    @SuppressWarnings("unchecked")
	private List<ProductHierDto> search( ProductHierSearchDto dto ) {
    	BooleanExpression filter = dto.createSearchExpression();
    	QCrmProductHier qHier = QCrmProductHier.crmProductHier;
    	if ( null != filter ) {
        	return ProductHierDto.toDtoList( IteratorUtils.toList( productHierRepo.findAll( filter, qHier.code.asc() ).iterator() ) );
    	}
    	return ProductHierDto.toDtoList( productHierRepo.findAll( new Sort( new Sort.Order( Sort.Direction.ASC, "code" ) ) ) );
    }

	private List<ProductHierDto> search( ProductHierSearchDto dto, Integer idx, Integer size ) {
    	BooleanExpression filter = dto.createSearchExpression();
    	Page<CrmProductHier> pg = null;
    	if ( null != filter ) {
    		pg = productHierRepo.findAll( filter, new PageRequest( idx/size, size, Sort.Direction.ASC, "code" ) );
    	}
    	else {
    		pg = productHierRepo.findAll( new PageRequest( idx/size, size, Sort.Direction.ASC, "code" ) );
    	}
    	return ProductHierDto.toDtoList( null != pg? pg.getContent() : new ArrayList<CrmProductHier>() );
	}

}
