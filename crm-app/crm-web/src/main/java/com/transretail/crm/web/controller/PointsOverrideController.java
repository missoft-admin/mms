package com.transretail.crm.web.controller;


import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang3.EnumUtils;
import org.apache.commons.lang3.StringUtils;
import org.joda.time.LocalDate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.transretail.crm.common.web.notification.NotificationType;
import com.transretail.crm.common.web.notification.annotation.Notifiable;
import com.transretail.crm.common.web.notification.annotation.NotificationUpdate;
import com.transretail.crm.core.dto.ApprovalMatrixDto;
import com.transretail.crm.core.dto.ApprovalMatrixResultList;
import com.transretail.crm.core.dto.ApprovalMatrixSearchDto;
import com.transretail.crm.core.dto.PointsDto;
import com.transretail.crm.core.dto.PointsResultList;
import com.transretail.crm.core.dto.PointsSearchDto;
import com.transretail.crm.core.entity.MemberModel;
import com.transretail.crm.core.entity.UserModel;
import com.transretail.crm.core.entity.enums.EmployeeSearchCriteriaTwo;
import com.transretail.crm.core.entity.enums.ModelType;
import com.transretail.crm.core.entity.enums.PointTxnType;
import com.transretail.crm.core.entity.enums.TxnStatus;
import com.transretail.crm.core.entity.enums.TxnStatusSearch;
import com.transretail.crm.core.entity.lookup.LookupDetail;
import com.transretail.crm.core.entity.lookup.Store;
import com.transretail.crm.core.security.model.impl.CustomSecurityUserDetailsImpl;
import com.transretail.crm.core.security.util.UserUtil;
import com.transretail.crm.core.service.ApprovalMatrixService;
import com.transretail.crm.core.service.CodePropertiesService;
import com.transretail.crm.core.service.MemberService;
import com.transretail.crm.core.service.PointsTxnManagerService;
import com.transretail.crm.core.service.StoreService;
import com.transretail.crm.core.service.UserService;
import com.transretail.crm.web.controller.util.ControllerResponse;
import com.transretail.crm.web.controller.util.ControllerResponseExceptionHandler;
//import com.transretail.crm.web.notifications.Notifiable;
//import com.transretail.crm.web.notifications.NotificationUpdate;


@RequestMapping("/points/override")
@Controller
public class PointsOverrideController extends ControllerResponseExceptionHandler {

	@Autowired
	PointsTxnManagerService pointsManagerService;
	@Autowired
	ApprovalMatrixService approvalMatrixService;
	@Autowired
	UserService userService;
	@Autowired
	MemberService memberService;
	@Autowired
	StoreService storeService;
	@Autowired
	CodePropertiesService codePropertiesService;
    @Autowired
    private MessageSource messageSource;

    private static final Logger LOGGER = LoggerFactory.getLogger(PointsOverrideController.class);

	private static final String PATH_OVERRIDE 	 		= "/points/override";
	private static final String PATH_OVERRIDE_ONE 		= "/points/override/one";
	private static final String PATH_OVERRIDE_CREATE	= "/points/override/create";
	private static final String PATH_OVERRIDE_PROCESS	= "/points/override/process";
	private static final String PATH_OVERRIDE_SAVE		= "/points/override/save";

	private static final String PATH_APPROVERS 	 		= "/points/override/approver";
	private static final String PATH_CREATEAPPROVER		= "/points/override/approver/create";

	private static final String UIATTR_APPROVERS 		= "approvers";
	private static final String UIATTR_STORES	 		= "stores";

	private static final String UIATTR_FORMACTION		= "action";
	private static final String UIATTR_ADJUSTMENT		= "pointsAdjustment";
	private static final String UIATTR_STATUS_APPROVE	= "statusApprove";
	private static final String UIATTR_STATUS_REJECT	= "statusReject";
	private static final String UIATTR_STATUS_DELETE	= "statusDelete";
	private static final String UIATTR_ID				= "id";

	private static final String UIATTR_HIGHESTAPPROVAL	= "highestApprovalPt";
	private static final String UIATTR_OVERRIDETYPE		= "overrideType";
	private static final String UI_ATTR_SEARCHFIELDS           = "pointsAdjustmentSearchFields";
	private static final String UI_ATTR_FILTER_STATUS          = "pointsAdjustmentFilterStatus";
	private static final String UI_ATTR_FILTER_TXN_LOCATION    = "pointsAdjustmentFilterTxnLocation";
	private static enum ATTR_OVERRIDETYPE { member, employee };
	

	@InitBinder
	void initBinder(WebDataBinder binder) {
		DateFormat theFormat = new SimpleDateFormat( "dd-MM-yyyy" );
		theFormat.setLenient( false );
		binder.registerCustomEditor( Date.class, new CustomDateEditor( theFormat, true ) );
	}



	@RequestMapping( produces = "text/html" )
	public String showOverride( 
            @RequestParam(value = "page", required = false) Integer page,
            @RequestParam(value = "size", required = false) Integer size,
            Model inUiModel ) {

    	populateStatus( inUiModel );
		populateUiAtributes( inUiModel );
		return PATH_OVERRIDE;
	}

	@RequestMapping(value="/{type}", produces = "text/html" )
	public String showOverride( @PathVariable( value="type" ) String overrideType, Model inUiModel ) {
    	populateStatus( inUiModel );
		populateUiAtributes( inUiModel );
		populatePointsAdjustmentFilters(inUiModel);
        inUiModel.addAttribute( UIATTR_OVERRIDETYPE, overrideType );
		return PATH_OVERRIDE;
	}
	
	@RequestMapping(value="/one/{id}", produces = "text/html" )
	public String showOverrideOne( @PathVariable("id") String id, Model inUiModel ) {
    	populateStatus( inUiModel );
		populateUiAtributes( inUiModel );
		inUiModel.addAttribute(UIATTR_ID, id);

		PointsDto dto = new PointsDto();
        if ( StringUtils.isNotBlank( id ) ) {
        	dto = pointsManagerService.retrievePointsDtoById( id );
        }

    	LookupDetail memberType = dto.getMemberModel().getMemberType();
    	String overrideType = "";
    	if ( null != memberType ) {
        	boolean isEmployee = memberType.getCode().equalsIgnoreCase( codePropertiesService.getDetailMemberTypeEmployee() );
        	boolean isMember = memberType.getCode().equalsIgnoreCase( codePropertiesService.getDetailMemberTypeIndividual() )
    				|| memberType.getCode().equalsIgnoreCase( codePropertiesService.getDetailMemberTypeProfessional() )
    				|| memberType.getCode().equalsIgnoreCase( codePropertiesService.getDetailMemberTypeCompany() );
    		overrideType = ( isEmployee ? ATTR_OVERRIDETYPE.employee.toString() : isMember ? ATTR_OVERRIDETYPE.member.toString() : "" );
    	}
        inUiModel.addAttribute( UIATTR_OVERRIDETYPE, overrideType );

        return PATH_OVERRIDE_ONE;
	}

    @RequestMapping(value = "/list", method = RequestMethod.POST)
    public @ResponseBody PointsResultList listOverrides( @RequestBody PointsSearchDto inSearchForm ) {

		if ( continueSearch( inSearchForm ) ) {
	    	return pointsManagerService.searchPoints( inSearchForm );
		}
		return new PointsResultList( new ArrayList<PointsDto>(), 0, false, false );
    }
	
	@RequestMapping(value = "/list/one/{id}", method = RequestMethod.POST)
    public @ResponseBody PointsResultList listOverrides( @PathVariable String id ) {
		PointsSearchDto search = new PointsSearchDto();
		search.setId(id);
		return pointsManagerService.searchPoints(search);
	}

    @RequestMapping(value = "/list/{type}", method = RequestMethod.POST)
    public @ResponseBody PointsResultList listOverrides( 
    		@PathVariable( value="type" ) String overrideType, 
    		@RequestBody PointsSearchDto inSearchForm ) {

		if ( continueSearch( inSearchForm, EnumUtils.getEnum( ATTR_OVERRIDETYPE.class, overrideType ) ) ) {
	    	return pointsManagerService.searchPoints( inSearchForm );
		}
		return new PointsResultList( new ArrayList<PointsDto>(), 0, false, false );
    }

    @RequestMapping( value = "/create", produces = "text/html" )
    public String createOverride( 
    		@ModelAttribute(UIATTR_ADJUSTMENT) PointsDto inDto, 
    		Model inUiModel ) {

		populateUiAtributes( inUiModel );
        return PATH_OVERRIDE_CREATE;
    }

    @Notifiable(type=NotificationType.POINTS)
    @RequestMapping( value="/save/{type}", produces="application/json; charset=utf-8", method=RequestMethod.POST )
	public @ResponseBody ControllerResponse saveOverride( 
			@PathVariable( value = "type") String type,
			@ModelAttribute(UIATTR_ADJUSTMENT) PointsDto inDto,
    		BindingResult inBinding ) {

    	ControllerResponse theResp = new ControllerResponse();
		theResp.setSuccess( true );

    	if ( isValid( inDto, inBinding, EnumUtils.getEnum( ATTR_OVERRIDETYPE.class, type ) ) ) {
    		inDto.setStatus( TxnStatus.FORAPPROVAL.toString() );
    		inDto.setTransactionType( PointTxnType.ADJUST.toString() );
    		inDto.setCreateUser( UserUtil.getCurrentUser().getUsername() );
    		inDto.setMemberAccountId(inDto.getMemberModel().getAccountId());
    		pointsManagerService.savePointsDto( inDto );
    		theResp.setSuccess( true );
    		theResp.setResult(new Object[] {
    				new Boolean(ATTR_OVERRIDETYPE.employee.toString().equals(type)), 
    				inDto.getTransactionPoints()});
    		theResp.setModelId(inDto.getId());
    		theResp.setNotificationCreation(true);
    	}
    	else {
    		theResp.setSuccess( false );
    		theResp.setResult( inBinding.getAllErrors() );
    	}

		return theResp;
	}

    @NotificationUpdate(type=NotificationType.POINTS)
    @RequestMapping( value={ "/process" }, produces="application/json; charset=utf-8", method=RequestMethod.POST )
	public @ResponseBody ControllerResponse processOverride(
			@ModelAttribute(UIATTR_ADJUSTMENT) PointsDto inDto,
    		BindingResult inBinding ) {

    	ControllerResponse theResp = new ControllerResponse();
		theResp.setSuccess( true );

		PointsDto theDto = pointsManagerService.retrievePointsDtoById( inDto.getId() );
		theDto.setStatus( inDto.getStatus() );
		theDto.setValidatedBy( UserUtil.getCurrentUser().getUsername() );
		theDto.setValidatedDate( new Date() );
		theDto.setApprovalReason( inDto.getApprovalReason() );
		
		if(inDto.getStatus().equalsIgnoreCase(TxnStatus.ACTIVE.toString())) {
			// set expiry date
			if(theDto.getTransactionAmount() != null && theDto.getTransactionAmount() > 0) {
				// expiry based on transaction date, else date validated
				Date startDate = theDto.getTransactionDateTime() != null ? 
						theDto.getTransactionDateTime() : theDto.getValidatedDate();
				int monthsToAdd = pointsManagerService.getDefaultValidPeriod(); // default: 2 years, 1 month for indiv and prof
//				if(theDto.getMemberModel().getMemberType().getCode().equals(
//						codePropertiesService.getDetailMemberTypeEmployee())) {
//					// TODO: figure out settings for employee
//				}
				theDto.setExpiryDate(LocalDate.fromDateFields(startDate).plusMonths(monthsToAdd));
			}
			theResp.setModelId( theDto.getId());
		}
		
		pointsManagerService.savePointsDto( theDto );
		theResp.setResult( theDto );

		return theResp;
	}

    @NotificationUpdate(type=NotificationType.POINTS)
    @RequestMapping( value={ "/process/{id}/{status}" }, produces="application/json; charset=utf-8", method=RequestMethod.POST )
	public @ResponseBody ControllerResponse processOverride( 
    		@PathVariable(value = "id") String inId, 
    		@PathVariable(value = "status") String inStatus,
			@ModelAttribute(UIATTR_ADJUSTMENT) PointsDto inDto,
    		BindingResult inBinding ) {

    	ControllerResponse theResp = new ControllerResponse();
		theResp.setSuccess( true );

		if ( StringUtils.isNotBlank( inStatus ) && inStatus.equalsIgnoreCase( TxnStatus.DELETED.toString() ) ) {
			pointsManagerService.deleteAdjustPoints( inId );
			theResp.setModelId(inId);
		}
		else {

			PointsDto theDto = pointsManagerService.retrievePointsDtoById( inId );
			theDto.setStatus( inStatus );
			theDto.setValidatedBy( UserUtil.getCurrentUser().getUsername() );
			theDto.setValidatedDate( new Date() );
			
			if(inStatus.equals(TxnStatus.ACTIVE.toString())) {
				// set expiry date
				if(theDto.getTransactionAmount() != null && theDto.getTransactionAmount() > 0) {
					// expiry based on transaction date, else date validated
					Date startDate = theDto.getTransactionDateTime() != null ? 
							theDto.getTransactionDateTime() : theDto.getValidatedDate();
					int monthsToAdd = pointsManagerService.getDefaultValidPeriod(); // default: 2 years, 1 month for indiv and prof
//					if(theDto.getMemberModel().getMemberType().getCode().equals(
//							codePropertiesService.getDetailMemberTypeEmployee())) {
//						// TODO: figure out settings for employee
//					}
					theDto.setExpiryDate(LocalDate.fromDateFields(startDate).plusMonths(monthsToAdd));
				}
				theResp.setModelId(inId);
			}
			
			pointsManagerService.savePointsDto( theDto );
			theResp.setResult( theDto );
		}

		return theResp;
	}

    @RequestMapping( value = "/process/{id}", produces = "text/html" )
    public String processOverride( 
    		@PathVariable(value = "id") String inId,
    		@ModelAttribute(UIATTR_ADJUSTMENT) PointsDto inDto, 
    		Model inUiModel ) {

    	editOverride( inId, inDto, inUiModel );
        inUiModel.addAttribute( UIATTR_FORMACTION, PATH_OVERRIDE_PROCESS );
        return PATH_OVERRIDE_PROCESS;
    }

    @RequestMapping( value = "/edit/{id}", produces = "text/html" )
    public String editOverride( 
    		@PathVariable(value = "id") String inId,
    		@ModelAttribute(UIATTR_ADJUSTMENT) PointsDto inDto, 
    		Model inUiModel ) {

    	populateStatus( inUiModel );
		populateUiAtributes( inUiModel );

        PointsDto dto = new PointsDto();
        if ( StringUtils.isNotBlank( inId ) ) {
        	dto = pointsManagerService.retrievePointsDtoById( inId );
        }

    	LookupDetail memberType = dto.getMemberModel().getMemberType();
    	String overrideType = "";
    	if ( null != memberType ) {
        	boolean isEmployee = memberType.getCode().equalsIgnoreCase( codePropertiesService.getDetailMemberTypeEmployee() );
        	boolean isMember = memberType.getCode().equalsIgnoreCase( codePropertiesService.getDetailMemberTypeIndividual() )
    				|| memberType.getCode().equalsIgnoreCase( codePropertiesService.getDetailMemberTypeProfessional() )
    				|| memberType.getCode().equalsIgnoreCase( codePropertiesService.getDetailMemberTypeCompany() );
    		overrideType = ( isEmployee ? ATTR_OVERRIDETYPE.employee.toString() : isMember ? ATTR_OVERRIDETYPE.member.toString() : "" );
    	}
        inUiModel.addAttribute( UIATTR_FORMACTION, PATH_OVERRIDE_SAVE + ( "/" + overrideType ) );
    	inUiModel.addAttribute( UIATTR_ADJUSTMENT, dto );
        inUiModel.addAttribute( UIATTR_OVERRIDETYPE, overrideType );

        return PATH_OVERRIDE_CREATE;
    }



    @RequestMapping( value = "/approver", produces = "text/html" )
    public String showOverrideApprovers(
            @RequestParam(value = "page", required = false) Integer page,
            @RequestParam(value = "size", required = false) Integer size,
            Model inUiModel ) {
        return PATH_APPROVERS;
    }

    @RequestMapping( value = "/approver/list", method = RequestMethod.POST)
    public @ResponseBody ApprovalMatrixResultList listOverrideApprovers(@RequestBody ApprovalMatrixSearchDto inSearchForm) {
    	inSearchForm.setModelType( ModelType.POINTS.toString() );
    	return approvalMatrixService.searchApprover( inSearchForm );
    }

	@RequestMapping( value = "/approver/create", produces = "text/html" )
    public String createApprover( 
    		@ModelAttribute("approver") ApprovalMatrixDto inDto, 
    		Model inUiModel ) {

		inUiModel.addAttribute( UIATTR_APPROVERS, approvalMatrixService.findPointsOverrideApprovers() );

        inUiModel.addAttribute( UIATTR_FORMACTION, "/points/override/approver/save" );
        return PATH_CREATEAPPROVER;
    }

    @RequestMapping( value = "/approver/edit/{id}", produces = "text/html" )
    public String editApprover( 
    		@PathVariable(value="id") Long inId,
    		@ModelAttribute("approver") ApprovalMatrixDto inDto, 
    		Model inUiModel ) {

    	inUiModel.addAttribute( "approver", approvalMatrixService.findDto( inId ) );
    	return createApprover( inDto, inUiModel );
    }

    @RequestMapping( value="/approver/save", produces="application/json; charset=utf-8", method=RequestMethod.POST )
	public @ResponseBody ControllerResponse saveApprover( 
			@ModelAttribute("user") ApprovalMatrixDto inDto,
    		BindingResult inBinding ) {

    	ControllerResponse theResp = new ControllerResponse();
		theResp.setSuccess( true );

		UserModel theUser = userService.findUserModel( inDto.getUser().getUsername() );
		inDto.setUser( theUser );
		inDto.setModelType( ModelType.POINTS );
		approvalMatrixService.saveDto( inDto );

		return theResp;
	}

    @RequestMapping( value = "/approver/delete/{id}", produces="application/json; charset=utf-8", method=RequestMethod.POST )
    public @ResponseBody ControllerResponse deleteApprover( 
    		@PathVariable(value="id") Long inId, 
    		Model inUiModel ) {

    	ControllerResponse theResp = new ControllerResponse();
		theResp.setSuccess( true );
		if ( null != inId ) {
			approvalMatrixService.delete( inId );
		}
    	return theResp;
    }



    @SuppressWarnings("unused")
	private boolean isValid( PointsDto inDto, BindingResult inBinding ) {
    	return isValid( inDto, inBinding, null );
    }

    private boolean isValid( PointsDto inDto, BindingResult inBinding, ATTR_OVERRIDETYPE overrideType ) {

    	if ( null == overrideType ) {
    		CustomSecurityUserDetailsImpl theUser = UserUtil.getCurrentUser();
    		if ( theUser.hasAuthority( codePropertiesService.getCsCode() ) 
    				|| theUser.hasAuthority( codePropertiesService.getCssCode() ) ) {
    			overrideType = ATTR_OVERRIDETYPE.member;
    		}
    		else if ( theUser.hasAuthority( codePropertiesService.getHrCode() ) 
    				|| theUser.hasAuthority( codePropertiesService.getHrsCode() ) ) {
    			overrideType = ATTR_OVERRIDETYPE.employee;
    		}
    	}

		boolean isMember = overrideType == ATTR_OVERRIDETYPE.member;
		boolean isEmployee = overrideType == ATTR_OVERRIDETYPE.employee;

		if ( null == inDto.getMemberModel() || StringUtils.isBlank( inDto.getMemberModel().getAccountId() ) ) {
			inBinding.reject( messageSource.getMessage( isMember ? "points_msg_memberidnotempty" : "points_msg_empidnotempty", null, LocaleContextHolder.getLocale() ) );
		} 
		if ( StringUtils.isBlank( inDto.getTransactionNo() ) ) {
			inBinding.reject( messageSource.getMessage( "points_msg_txnnumbernotempty", null, LocaleContextHolder.getLocale() ) );
		}
		if ( null == inDto.getTransactionAmount() || StringUtils.isBlank( inDto.getTransactionAmount().toString() ) ) {
			inBinding.reject( messageSource.getMessage( "points_msg_txnamtnotempty", null, LocaleContextHolder.getLocale() ) );
		}
		if ( null == inDto.getTransactionPoints() || StringUtils.isBlank( inDto.getTransactionPoints().toString() ) ) {
			inBinding.reject( messageSource.getMessage( "points_msg_txnpointnotempty", null, LocaleContextHolder.getLocale() ) );
		}
		
		
		if(inDto.getTransactionDateTime() == null) {
			inBinding.reject( messageSource.getMessage( "points_msg_txndatenotempty", null, LocaleContextHolder.getLocale() ) );
		}


    	if (  null != inDto.getMemberModel() && StringUtils.isNotBlank( inDto.getMemberModel().getAccountId() ) ) {
    		MemberModel theMember = memberService.findByAccountId( inDto.getMemberModel().getAccountId() );
    		if ( null == theMember ) {
    			inBinding.reject( messageSource.getMessage( isMember ? "points_error_nullforgivenaccountid" : "points_error_nullforgivenaemployeeid", 
    					new Object[] { inDto.getMemberModel().getAccountId() }, LocaleContextHolder.getLocale() ) );
    		}
    		else {
        		if ( isEmployee && !theMember.getMemberType().getCode().equalsIgnoreCase( codePropertiesService.getDetailMemberTypeEmployee() ) ) {        			
        			inBinding.reject( messageSource.getMessage( "points_msg_invalidemployee", null, LocaleContextHolder.getLocale() ) );
        		}
        		if ( isMember && ( !theMember.getMemberType().getCode().equalsIgnoreCase( codePropertiesService.getDetailMemberTypeIndividual() ) && 
        						!theMember.getMemberType().getCode().equalsIgnoreCase( codePropertiesService.getDetailMemberTypeCompany() ) ) ) {        			
        			inBinding.reject( messageSource.getMessage( "points_msg_invalidcustomer", null, LocaleContextHolder.getLocale() ) );
        		}
    		}
    	}

    	return !inBinding.hasErrors();
    }

    private boolean continueSearch( PointsSearchDto inSearchForm ) {
		return continueSearch( inSearchForm, null );
    }

    private boolean continueSearch( PointsSearchDto inSearchForm, ATTR_OVERRIDETYPE overrideType ) {

		CustomSecurityUserDetailsImpl theUser = UserUtil.getCurrentUser();
		String theCsCode = codePropertiesService.getCsCode();
		String theCssCode = codePropertiesService.getCssCode();
		String theHrCode = codePropertiesService.getHrCode();
		String theHrsCode = codePropertiesService.getHrsCode();
    	if ( null == overrideType ) {
    		if ( theUser.hasAuthority( theCsCode ) || theUser.hasAuthority( theCssCode ) ) {
    			overrideType = ATTR_OVERRIDETYPE.member;
    		}
    		else if ( theUser.hasAuthority( theHrCode ) || theUser.hasAuthority( theHrsCode ) ) {
    			overrideType = ATTR_OVERRIDETYPE.employee;
    		}
    	}

		boolean isMemberMgmt = overrideType == ATTR_OVERRIDETYPE.member;
		boolean isEmployeeMgmt = overrideType == ATTR_OVERRIDETYPE.employee;
		boolean isStaff = theUser.hasAuthority( theCsCode ) || theUser.hasAuthority( theHrCode );
		boolean isHead = theUser.hasAuthority( theCssCode ) || theUser.hasAuthority( theHrsCode );

		inSearchForm.setTransactionType( PointTxnType.ADJUST.toString() );

		if ( isEmployeeMgmt ) {
			inSearchForm.setMemberType( codePropertiesService.getDetailMemberTypeEmployee() );
		}

		if ( isMemberMgmt ) {
			List<String> theCustomerTypes = new ArrayList<String>();
			theCustomerTypes.add( codePropertiesService.getDetailMemberTypeIndividual() );
			theCustomerTypes.add( codePropertiesService.getDetailMemberTypeCompany() );
			theCustomerTypes.add( codePropertiesService.getDetailMemberTypeProfessional() );
			inSearchForm.setCustomerTypes( theCustomerTypes );
		}

		if ( isStaff ) {
			inSearchForm.setForProcessing( true );
		}
		if ( isHead ) {
			inSearchForm.setStatus( TxnStatus.FORAPPROVAL.toString() );
			if ( !isStaff ) {
		        Long theHighestAmt = approvalMatrixService.findHighestApprovalPointsOfCurrentUser();
		        if ( null == theHighestAmt ) {
		        	return false;
		        }
		        inSearchForm.setHighestPoint( theHighestAmt );
			}
		}

		return true;
    }

    private void populateStatus( Model inUiModel ) {
    	inUiModel.addAttribute( UIATTR_STATUS_APPROVE, TxnStatus.ACTIVE );
    	inUiModel.addAttribute( UIATTR_STATUS_REJECT, TxnStatus.REJECTED );
    	inUiModel.addAttribute( UIATTR_STATUS_DELETE, TxnStatus.DELETED );
    }

    private void populateUiAtributes( Model inUiModel ) {
		inUiModel.addAttribute( UIATTR_ADJUSTMENT, new PointsDto() );
        inUiModel.addAttribute( UIATTR_STORES, storeService.getAllStores() );
        inUiModel.addAttribute( ATTR_OVERRIDETYPE.member.toString(), ATTR_OVERRIDETYPE.member.toString() );
        inUiModel.addAttribute( ATTR_OVERRIDETYPE.employee.toString(), ATTR_OVERRIDETYPE.employee.toString() );
        inUiModel.addAttribute( UIATTR_HIGHESTAPPROVAL, approvalMatrixService.findHighestApprovalPointsOfCurrentUser() );
    }

    private void populatePointsAdjustmentFilters( Model inUiModel ) {
        
        inUiModel.addAttribute(UI_ATTR_SEARCHFIELDS, EmployeeSearchCriteriaTwo.values());
        inUiModel.addAttribute(UI_ATTR_FILTER_TXN_LOCATION, storeService.getAllStores());
        inUiModel.addAttribute(UI_ATTR_FILTER_STATUS, TxnStatusSearch.values());
        
    }
}
