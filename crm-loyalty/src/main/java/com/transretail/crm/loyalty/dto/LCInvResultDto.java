package com.transretail.crm.loyalty.dto;

/**
 * @author Allan G. Ramirez (agramirez@exist.com)
 */
public class LCInvResultDto {
    private String cardNo;
    private String cardType;
    private String status;
    private String location;
    private String locationName;
    private String statusCode;
    private Double annualFee;

    public LCInvResultDto() {

    }

    public LCInvResultDto(String cardNo, String cardType, String status, String location) {
        this.cardNo = cardNo;
        this.cardType = cardType;
        this.status = status;
        this.location = location;
    }

    public LCInvResultDto(String cardNo, String cardType, String status, String location, String statusCode) {
        this.cardNo = cardNo;
        this.cardType = cardType;
        this.status = status;
        this.location = location;
        this.statusCode = statusCode;
    }

    public String getCardNo() {
        return cardNo;
    }

    public void setCardNo(String cardNo) {
        this.cardNo = cardNo;
    }

    public String getCardType() {
        return cardType;
    }

    public void setCardType(String cardType) {
        this.cardType = cardType;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getLocationName() {
        return locationName;
    }

    public void setLocationName(String locationName) {
        this.locationName = locationName;
    }

	public String getStatusCode() {
		return statusCode;
	}

	public void setStatusCode(String statusCode) {
		this.statusCode = statusCode;
	}

	public Double getAnnualFee() {
		return annualFee;
	}

	public void setAnnualFee(Double annualFee) {
		this.annualFee = annualFee;
	}
}
