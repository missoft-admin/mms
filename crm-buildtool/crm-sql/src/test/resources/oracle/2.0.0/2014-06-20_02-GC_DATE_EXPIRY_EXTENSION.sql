create table CRM_GC_DATE_EXPIRY_EXTENSION (
    id number(19,0) not null,
    CREATED_BY varchar2(255 char),
    CREATED_DATETIME timestamp,
    LAST_UPDATED_BY varchar2(255 char),
    LAST_UPDATED_DATETIME timestamp,
    CREATE_DATE date,
    DATE_EXTENSION_TYPE varchar2(255 char),
    EXTEND_NO varchar2(255 char),
    FILED_BY varchar2(255 char),
    SERIES number(19,0),
    STATUS varchar2(255 char),
    ORDER_ID number(19,0),
    primary key (id)
);

alter table CRM_GC_DATE_EXPIRY_EXTENSION 
    add constraint FK_44wws7p36i81l7u7g2fam1652 
    foreign key (ORDER_ID) 
    references CRM_GC_ORD;