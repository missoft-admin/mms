<%@ include file="../common/taglibs.jsp" %>

  <spring:message var="typeName" code='label_lookup_detail'/>
  <spring:url value="/resources/images/update.png" var="update_image_url" />
  <spring:message arguments="${typeName}" code="entity_update" var="update_label" htmlEscape="false" />

  <div class="page-header page-header2">
      <h1><spring:message code="menutab_programsetup_maintenance_lookup" /></h1>
  </div>

  <div class="well">
  <fieldset style="margin-bottom:-20px">
     <!--<legend class="block-stack-title"><spring:message code="label_lookup_header" /></legend>-->

  <div id="loookupHeaderForm" class="form-horizontal">
    <div class="messagesContainer">

    </div>
  	<div class="control-group">
        <label class="control-label" for="headerCode"><spring:message code="label_lookup_header" /></label>
        <div class="controls">
            <select data-header-code="${headerCode}" id="headerCode" name="code" style="width: 400px">
	                <option value=""></option>
	            <c:forEach var="item" items="${headers}">
	                <option value="${item.code}">${item.code} - ${item.description}</option>
	            </c:forEach>
	        </select>
        </div>
    </div>
    <div class="control-group" id="update_lookup_header_container" style="display: none;">
      <label class="control-label" for="headerDesc"><spring:message code="label_description" /></label>
      <div class="controls">
        <input id="headerDesc" name="desc" style="width: 392px" type="text">
        <input type="button" style="margin-left: 7px" id="update_lookup_header" value="<spring:message code="label_save" />" class="btn btn-primary">
      </div>
    </div>
  </div>

  </fieldset>
  </div>

  <fieldset id="detailFieldSet" class="hide" style="">
     <legend class="block-stack-title"><spring:message code="label_lookup_detail" /></legend>
		<c:set var="ENCTYPE" value="application/x-www-form-urlencoded"/>
		<c:if test="${multipart}"><c:set var="ENCTYPE" value="multipart/form-data"/></c:if>
		<div class="well clearfix">
		<form:form id="detailForm" name="detailForm" modelAttribute="detail" action="${pageContext.request.contextPath}/lookup/" method="POST" enctype="${ENCTYPE}" >
      <form:hidden path="oldCode" id="oldCode" />
		<div id="contentError" class="hide alert alert-error">
        	<button type="button" class="close" onclick="$(this).parent().hide();">&times;</button>
            <div><form:errors path="*"/></div>
        </div>
		<div>
			<div class="pull-left input-group" style="margin-left:0">
				<div>
					<spring:message code="label_lookup_detail_code" var="detailCode" />
					<div>
						<label for="code">${detailCode}</label>
					</div>
					<div>

						<form:input id="detail_code" path="code" placeholder="${detailCode}" />
					</div>
				</div>
			</div>

			<div class="pull-left input-group">
				<div>
					<spring:message code="label_lookup_detail_description" var="description" />
					<div>
						<label for="description">${description}</label>
					</div>
					<div>
						<form:input id="detail_description" path="description" placeholder="${description}" />
					</div>
				</div>
			</div>

			<div class="pull-left input-group statusContainer">
				<div>
					<div>
						<label for="description"><spring:message code="label_lookup_status"/></label>
					</div>
					<div>
						<select id="detail_status" name="status">
				            <c:forEach var="item" items="${status}">
				                <option value="${item}">${item}</option>
				            </c:forEach>
				        </select>
					</div>
				</div>
			</div>
			<div class="lookup-detail-well pull-left">
				<input type="button" id="button_add" value="<spring:message code="label_save" />" class="btn btn-primary"/>
				<input type="button" id="button_reset" value="<spring:message code="label_reset" />" class="btn"/>
			</div>
		</div>
		</form:form>
		</div>
   </fieldset>

  <div class="clearfix"></div>

<div id="content_detailList">

  <div id="list_detail"></div>

</div>

  <link href="<spring:url value="/css/dataTables.css" />" rel="stylesheet">
  <script src="<spring:url value="/js/jquery/jquery.dataTables.js" />" ></script>
  <script src="<spring:url value="/js/jquery/jquery.dataTables.linkLengthMenu.js" />"></script>
  <script src="<spring:url value="/js/jquery/jquery.dataTables.customPaging.js" />"></script>
  <script src="<spring:url value="/js/jquery/ajax-datatable.js" />" ></script>
  <script src="<spring:url value="/js/common/form2js.js" />" ></script>
  <script src="<spring:url value="/js/common/json2.js" />" ></script>
  <script src="<spring:url value="/js/jquery/jquery.toObject.js" />" ></script>
  <link href="<c:url value="/styles/temp.css" />" rel="stylesheet" />



  <script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/viewspecific/lookup/lookup.js"><![CDATA[&nbsp;]]></script>

<script>

var $headerDescInput = $('#headerDesc');
function reloadTable(headerCode) {
  $("#detailForm").data("headerCode", headerCode);
  resetForm();
  $("#list_detail").empty();
  if(headerCode != "") {
    $("#detailFieldSet").show();
    $("#list_detail").ajaxDataTable({
      'autoload'  : true,
      'ajaxSource' : "<c:url value="/lookup/list/" />" + headerCode,
      'aaSorting'   : [[ 1, "asc" ]],
      'columnHeaders' : [
        "<spring:message code="label_lookup_detail_code"/>",
        "<spring:message code="label_lookup_detail_description"/>",
        "<spring:message code="label_lookup_status"/>",
        ""
      ],
      'modelFields' : [
        {name : 'code', sortable : true},
        {name : 'description', sortable : true},
        {name : 'status', sortable : true},
        {customCell : function ( innerData, sSpecific, json ) {
          if ( sSpecific == 'display') {
            return "<a title=\"${fn:escapeXml(update_label)}\""+
                    "alt=\"${fn:escapeXml(update_label)}\""+
                    "onclick=\"javascript: editDetail( '"+json.code+"', '"+json.description+"', '"+json.status+"' );\""+
                    "class=\"icn-edit tiptip\""+
                    "href=\"#\"></a>";
          } else {
            return '';
          }
        }
        }
      ]
    });

    /* var $theFormDataObj = $( "#detailForm" ).toObject( { mode : 'first', skipEmpty : true } );
     $theFormDataObj.status = "ACTIVE";
     $("#list_detail").ajaxDataTable( 'search', $theFormDataObj, function () {}); */
  } else {
    $("#detailFieldSet").hide();
  }
}
var $headerCodeSelect = $('#headerCode' ).change(function() {
  var $selectedHeader = $('option:selected', this);
  var headerCode = $selectedHeader.val();

  if(headerCode.indexOf('CUSTMEMBERFLD') > -1) {
    $('#update_lookup_header_container' ).show();
    $headerDescInput.val($selectedHeader.text().split('-')[1].trim());
  } else {
    $('#update_lookup_header_container' ).hide();
    $headerDescInput.val('');
  }
  reloadTable(headerCode);
});

var $lookupHeaderForm = $( '#loookupHeaderForm' );
var $headerFormErrorContainer = $('.messagesContainer', $lookupHeaderForm);
var showErrorMessage = function(message) {
  $headerFormErrorContainer.html('<div class="alert alert-error">' +
          '<button type="button" class="close" data-dismiss="alert">&times;</button>' +
          message +
          '</div>');
};

var showSuccessMessage = function(message) {
  $headerFormErrorContainer.html('<div class="alert alert-success">' +
          '<button type="button" class="close" data-dismiss="alert">&times;</button>' +
          message +
          '</div>');
};

$( '#update_lookup_header' ).click( function () {
  var codeDescDto = $lookupHeaderForm.toObject( {mode : 'first', skipEmpty : false} );
  $.ajax( {
    "contentType" : 'application/json',
    "type" : "POST",
    "url" : "<spring:url value='/lookup/header/update'  />",
    "data" : JSON.stringify( codeDescDto ),
    "success" : function ( response ) {
      if ( !response.success ) {
        showErrorMessage( response.result );
      } else {
        $( 'option:selected', $headerCodeSelect ).text( codeDescDto.code + " - " + codeDescDto.desc );
        showSuccessMessage("<spring:message code="generic.update.successful" />");
      }
    },
    "error" : function ( xhr, textStatus, error ) {
      if ( textStatus === 'timeout' ) {
        showErrorMessage( 'The server took too long to send the data.' );
      } else if ( error ) {
        showErrorMessage( error );
      } else {
        showErrorMessage( 'An error occurred on the server. Please try again.' );
      }
    }
  } );
} );

</script>
<style>
    .well .input-group { margin: 5px; }
    .controls #headerCode { text-transform: uppercase !important; }
    .lookup-detail-well {
      padding-top: 31px;
      margin-left: 5px;
    }
    /*Chrome specific */
    @media screen and (-webkit-min-device-pixel-ratio:0) {    
    	.lookup-detail-well {
        padding-top: 30px;
      }
      #update_lookup_header {
        margin-top: 0px;
      }

    }
</style>