$(document).ready(function() {
	var CONTAINER_ERROR = "#contentError > div";
	var CONTENT_ERROR = "#contentError";
	
	$("#rfsCheckbox").click(toggleRfs);
	
	$.get($("#storeGroup").data("url"), function(data) {
		var inReturnEntity = data.result;
		$("#storeGroup").empty().append($("<option>"));
		$.each(inReturnEntity, function(key, value) {
			var val = value.id;
			var label = value.name;
			$("<option>").attr("value", val).text(label).appendTo($("#storeGroup"));
		});	
	}, "json");
	
	$.get($("#productGroup").data("url"), function(data) {
		var inReturnEntity = data.result;
		$("#productGroup").empty().append($("<option>"));
		$.each(inReturnEntity, function(key, value) {
			var val = value.id;
			var label = value.name;
			$("<option>").attr("value", val).text(label).appendTo($("#productGroup"));
		});	
	}, "json");
	
	$(".recencyRadio").change(toggleRecency);
	
	$(".rfsCheckbox").click(resetFields);
	
	$(".rfsCheckbox").each(resetFields);
	
	$("#zeroFrequency").click(function() {
		$(".frequenceField").val("");
		$(".frequenceField").prop("readonly", $(this).is(":checked"));
	});
	
	$("#dateRangeFrom").datepicker({
		autoclose : true,
		format : 'dd-mm-yyyy'
	}).on('changeDate', function(selected) {
		startDate = new Date(selected.date.valueOf());
		startDate.setDate(startDate.getDate(new Date(selected.date.valueOf())));
		$("#dateRangeTo").datepicker('setStartDate', startDate);
	});
	
	$("#dateRangeTo").datepicker({
		autoclose : true,
		format : 'dd-mm-yyyy'
	}).on('changeDate', function(selected) {
		endDate = new Date(selected.date.valueOf());
		endDate.setDate(endDate.getDate(new Date(selected.date.valueOf())));
		$("#dateRangeFrom").datepicker('setEndDate', endDate);
	});
	
	$(".float").numberInput({
        "type" : "float"
    });
	$(".int").numberInput({
        "type" : "int"
    });
	
});

function resetFields() {
	$parent = $(this).closest("fieldset");
	if($(this).is(":checked")) {
		$parent.find("input[type='text']").prop("readonly", false).prop("disabled", false);
		$parent.find(".blockCheckbox").prop("disabled", false);
	} else {
		$parent.find("input[type='text']").prop("readonly", true).prop("disabled", true).val("");
		$parent.find(".blockCheckbox").prop("checked", false).prop("disabled", true);
	}
}

function toggleRfs() {
	if($(this).is(":checked")) {
		$("#rfsStoreForm").show();	
	} else {
		$("#rfsStoreForm").hide();
		//TODO reset RFS form
	}
}

function toggleRecency() {
	var block = $(this).data("block");
	$(".recencyBlock").hide();
	$(".recencyBlock input, .recencyBlock select").not("#" + block + " input, #" + block + " select").val("");
	$("#" + block).show();
}

