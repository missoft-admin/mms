package com.transretail.crm.web.controller;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.joda.time.DateTime;
import org.joda.time.LocalDate;
import org.joda.time.format.DateTimeFormat;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Configurable;
import org.springframework.core.convert.converter.Converter;
import org.springframework.format.FormatterRegistry;
import org.springframework.format.support.FormattingConversionServiceFactoryBean;

import com.google.common.collect.Lists;
import com.transretail.crm.core.entity.MemberGroup;
import com.transretail.crm.core.entity.MemberModel;
import com.transretail.crm.core.entity.ProductGroup;
import com.transretail.crm.core.entity.UserModel;
import com.transretail.crm.core.entity.UserRoleModel;
import com.transretail.crm.core.entity.lookup.LookupDetail;
import com.transretail.crm.core.entity.lookup.Product;
import com.transretail.crm.core.entity.lookup.StoreGroup;
import com.transretail.crm.core.service.LookupService;
import com.transretail.crm.core.service.MemberGroupService;
import com.transretail.crm.core.service.MemberService;
import com.transretail.crm.core.service.ProductGroupService;
import com.transretail.crm.core.service.ProductService;
import com.transretail.crm.core.service.StoreGroupService;
import com.transretail.crm.core.service.UserService;
import com.transretail.crm.core.service.UserRoleModelService;
import org.joda.time.LocalTime;

/**
 * A central place to register application converters and formatters.
 */
@Configurable
public class ApplicationConversionServiceFactoryBean extends
	FormattingConversionServiceFactoryBean {

    @Autowired
    MemberService memberService;
    @Autowired
    UserService userService;
    @Autowired
    LookupService lookupService;

    @Autowired
    UserRoleModelService userRoleModelService;

    @Override
    protected void installFormatters(FormatterRegistry registry) {
	super.installFormatters(registry);
	// Register application converters and formatters
    }

    
    public Converter<MemberModel, String> getMemberModelToStringConverter() {
	return new Converter<MemberModel, String>() {
	    public String convert(MemberModel customerModel) {
		return new StringBuilder().append(customerModel.getFirstName())
			.append(' ').append(customerModel.getMiddleName())
			.append(' ').append(customerModel.getLastName())
			.toString();
	    }
	};
    }

    public Converter<Long, MemberModel> getIdToMemberModelConverter() {
	return new Converter<Long, MemberModel>() {
	    public MemberModel convert(Long id) {
		return memberService.findCustomerModel(id);
	    }
	};
    }

    public Converter<String, MemberModel> getStringToMemberModelConverter() {
	return new Converter<String, MemberModel>() {
	    public MemberModel convert(String id) {
		return getObject().convert(getObject().convert(id, Long.class),
			MemberModel.class);
	    }
	};
    }

    public Converter<UserModel, String> getUserModelToStringConverter() {
	return new Converter<UserModel, String>() {
	    public String convert(UserModel userModel) {
		return new StringBuilder().append(userModel.getUsername())
			.append(' ').append(userModel.getPassword()).toString();
	    }
	};
    }

    public Converter<Long, UserModel> getIdToUserModelConverter() {
	return new Converter<Long, UserModel>() {
	    public UserModel convert(Long id) {
		return userService.findUserModel(id);
	    }
	};
    }

    public Converter<String, UserModel> getStringToUserModelConverter() {
	return new Converter<String, UserModel>() {
	    public UserModel convert(String id) {
		return getObject().convert(getObject().convert(id, Long.class),
			UserModel.class);
	    }
	};
    }

    public Converter<UserRoleModel, String> getUserRoleModelToStringConverter() {
	return new Converter<UserRoleModel, String>() {
	    public String convert(UserRoleModel userRoleModel) {
		return new StringBuilder().append(userRoleModel.getCode())
			.append(' ').append(userRoleModel.getDescription())
			.toString();
	    }
	};
    }

    public Converter<String, UserRoleModel> getIdToUserRoleModelConverter() {
	return new Converter<String, UserRoleModel>() {
	    public UserRoleModel convert(String id) {
		return userRoleModelService.findUserRoleModel(id);
	    }
	};
    }

    public Converter<String, UserRoleModel> getStringToUserRoleModelConverter() {
	return new Converter<String, UserRoleModel>() {
	    public UserRoleModel convert(String id) {
		return getObject().convert(getObject().convert(id, Long.class),
			UserRoleModel.class);
	    }
	};
    }

    public Converter<String, LookupDetail> getStringToLookupDetailModelConverter() {
	return new Converter<String, LookupDetail>() {
	    public LookupDetail convert(String id) {
		return lookupService.getDetail(id);
	    }
	};
    }

    public Converter<String, Date> getStringToDateConverter() {
	return new Converter<String, Date>() {
	    public Date convert(String date) {
    			if(date == null && StringUtils.isBlank(date)) {
		    return null;
		}
		String args[] = date.split("-");
		Calendar calendar = Calendar.getInstance();
		calendar.clear();
		calendar.set(Integer.valueOf(args[2]), Integer.valueOf(args[1]), Integer.valueOf(args[0]));
		return calendar.getTime();
	    }
	};
    }

    public Converter<String, LocalDate> getStringToLocalDateConverter() {
	return new Converter<String, LocalDate>() {
	    public LocalDate convert(String date) {
    			if(date == null || StringUtils.isBlank(date) || date.equalsIgnoreCase("null")) {
		    return null;
		}
		String args[] = date.split("-");
    			if(args.length != 3) return null;
		Calendar calendar = Calendar.getInstance();
		calendar.clear();
		calendar.set(Integer.valueOf(args[2]), Integer.valueOf(args[1]) - 1, Integer.valueOf(args[0]));
		return LocalDate.fromCalendarFields(calendar);
	    }
	};
    }

    public Converter<LocalDate, String> getLocalDateToStringConverter() {
	return new Converter<LocalDate, String>() {
	    public String convert(LocalDate date) {
    			if(date == null) {
		    return "";
		}

		return date.toString("dd-MM-yyyy");
	    }
	};
    }

    public Converter<List<String>, String> getListToStringConverter() {
	return new Converter<List<String>, String>() {
	    static final String COMMA = ",";
	    @Override
	    public String convert(List<String> source) {
				if(source == null || source.size() < 1) return null;
		StringBuilder builder = new StringBuilder(source.get(0));

				for(int i = 1; i < source.size(); i++) {
		    builder.append(COMMA).append(source.get(i));
		}
		return builder.toString();
	    }
	};
    }

    public Converter<String, DateTime> getStringToDateTimeConverter() {
	return new Converter<String, DateTime>() {
	    @Override
	    public DateTime convert(String source) {
		source = source.trim();
    			if(StringUtils.isEmpty(source) || !source.matches("^\\d{2}-\\d{2}-\\d{4} \\d{1,2}:\\d{2}:\\d{2}$")) {
		    return null;
		}

		return DateTime.parse(source, DateTimeFormat.forPattern("dd-MM-yyyy H:mm:ss"));
	    }
	};
    }

    public Converter<DateTime, String> getDateTimeToStringConverter() {
	return new Converter<DateTime, String>() {
	    @Override
	    public String convert(DateTime dateTime) {
    		if (dateTime == null) {
		    return "";
		}
		return dateTime.toString("dd-MM-yyyy H:mm:ss");
	    }
	};
    }

    public Converter<String, LocalTime> getStringToLocalTimeConverter() {
	return new Converter<String, LocalTime>() {

	    @Override
	    public LocalTime convert(String source) {
		source = source.trim();
		if (StringUtils.isEmpty(source) || !source.matches("^\\d{1,2}:\\d{2}$")) {
		    return null;
		}

		return LocalTime.parse(source, DateTimeFormat.forPattern("HH:mm"));
	    }
	};
    }

    public Converter<LocalTime, String> getLocalTimeToStringConverter() {
	return new Converter<LocalTime, String>() {

	    @Override
	    public String convert(LocalTime source) {
		return source == null ? "" : source.toString("HH:mm");
	    }
	};
    }

    public void installLabelConverters(FormatterRegistry registry) {
	registry.addConverter(getMemberModelToStringConverter());
	registry.addConverter(getIdToMemberModelConverter());
	registry.addConverter(getStringToMemberModelConverter());
	registry.addConverter(getUserModelToStringConverter());
	registry.addConverter(getIdToUserModelConverter());
	registry.addConverter(getStringToUserModelConverter());
	registry.addConverter(getUserRoleModelToStringConverter());
	registry.addConverter(getIdToUserRoleModelConverter());
	registry.addConverter(getStringToUserRoleModelConverter());
	registry.addConverter(getStringToLookupDetailModelConverter());
	registry.addConverter(getStringToDateConverter());
	registry.addConverter(getStringToLocalDateConverter());
	registry.addConverter(getLocalDateToStringConverter());
	registry.addConverter(getStringToDateTimeConverter());
	registry.addConverter(getDateTimeToStringConverter());
//        registry.addConverter(getListToStringConverter());
	registry.addConverter(getLocalTimeToStringConverter());
	registry.addConverter(getStringToLocalTimeConverter());
    }

    public void afterPropertiesSet() {
	super.afterPropertiesSet();
	installLabelConverters(getObject());
    }
}
