<%@ include file="../../common/taglibs.jsp" %>

<style type="text/css">
<!--
  .well .input {
    background-color: #fff;
    border: 1px solid #E8E8E8;
    box-shadow: none;    
    padding: 6px;
    height: 18px;
    margin-left: 5px;
  }
  .well .btn-primary {
    margin-left: 5px;
  }
  .well select, .well .input {
    margin: 5px;
  }
  
-->
</style>

<div id="content_date_expiry">

  <div class="page-header page-header2"><h1><spring:message code="gc_date_expiry_lbl_header" /></h1></div>
    
  <jsp:include page="dateExpirySearchForm.jsp"/>

  <div class="">
      <div class="pull-right"><a href="#" class="btn btn-primary" data-url="<c:url value="/giftcard/dateexpiry/create"/>" id="link_create"><spring:message code="gc_date_expiry_lbl_create" /></a></div>
  </div>
  
  <div class="clearfix"></div>
  <div class="modal hide groupModal nofly" tabindex="-1" role="dialog" aria-hidden="true" id="form_date_expiry"></div>
  <div id="list_date_expiry_container" class="data-content-02">
    <div id="list_date_expiry"
      data-url="<c:url value="/giftcard/dateexpiry/list"/>"
      data-extend_no="<spring:message code="gc_date_expiry_lbl_extend_no"/>"
      data-create_date="<spring:message code="gc_date_expiry_lbl_create_date"/>" 
      data-so_series="<spring:message code="gc_date_expiry_lbl_sales_order_series"/>" 
      data-filed_by="<spring:message code="gc_date_expiry_lbl_filed_by"/>" 
      data-status="<spring:message code="gc_date_expiry_lbl_status"/>" >
    </div> 
  </div>
  
  <div id="date_expiry_list_actions" class="hide">
    <div id="view">
		<input type="button" data-url="<c:url value="/giftcard/dateexpiry/view"/>/%ID%" class="btn view icn-view tiptip pull-left" title="<spring:message code="label_view" />">
	</div>
    <div id="print">
      <input type="button" data-url="<c:url value="/giftcard/dateexpiry/print"/>/%ID%" class="btn print icn-print tiptip pull-left" title="<spring:message code="label_print" />">
    </div>
  </div>
  
  <link href="<c:url value="/styles/temp.css" />" rel="stylesheet" />
  <link href="<spring:url value="/css/dataTables.css" />" rel="stylesheet">
  <script src="<spring:url value="/js/jquery/jquery.dataTables.js" />" ></script>
  <script src="<spring:url value="/js/jquery/jquery.dataTables.linkLengthMenu.js" />"></script>
  <script src="<spring:url value="/js/jquery/jquery.dataTables.customPaging.js" />"></script>
  <script src="<spring:url value="/js/jquery/ajax-datatable.js" />" ></script>
  <script src="<spring:url value="/js/common/form2js.js" />" ></script>
  <script src="<spring:url value="/js/common/json2.js" />" ></script>
  <script src="<spring:url value="/js/jquery/jquery.toObject.js" />" ></script>
</div>

<script type="text/javascript">
$(document).ready(function() {
  
  var list = $("#list_date_expiry");
  var modalDateExpiry = $("#form_date_expiry");
  
  modalDateExpiry.modal({
		show: false
	}).on('hidden.bs.modal', function() {
		$(this).empty();
	});
  
  initButtons();
  initDataTable();
  
  function initDataTable() {
    var headers = [
        { text: "", className : "hide" },
        list.data("extend_no"),
        list.data("create_date"),
        list.data("so_series"), 
        list.data("filed_by"),
        list.data("status"),
        ""
    ];
  
    list.ajaxDataTable({
      'autoload'		: true,
      'aaSorting' :[[ 0, 'desc' ]],
      'ajaxSource'		: list.data("url"),
      'columnHeaders'	: headers,
    	'modelFields'	: [
    	             	    {name: 'lastUpdated', sortable: true, dataType: "DATE", aaClassname : "hide"},
    	             	  	{name: 'extendNo', sortable: true},
    	             	  	{name: 'createDate', sortable: true, fieldCellRenderer: function(data, type, row) {
    	             				return row.createDateString;
    	             			}},
    	             	  	{name: 'soSeries', sortable: false},
    	             	  	{name: 'filedBy', sortable: false},
    	             	  	{name: 'status', sortable: true},
    	             	  	{customCell: function(innerData, sSpecific, json) {
    	             	  	  if(sSpecific == 'display') {
  						    	var btnHtml = "";
  
  						    	if (json.status === 'CREATED') {
  								  btnHtml += $("#view").html();
  								} else if (json.status === 'APPROVED') {
  								  btnHtml += $("#print").html();
  								}
  						    	
                          		var rg = new RegExp("%ID%", 'g');
                          		btnHtml = btnHtml.replace(rg, json.extendNo);
    		
    								return btnHtml;
					  		  	} else {
  								  return '';
  						      	}
    	             	  	}}
    	             	  ]
    })
    .on("click", ".view", viewDateExpiry)
    .on("click", ".print", print);
  };
  
  function initButtons() {
    var link = $("#link_create");
    link.click(function(e) {
      e.preventDefault();
      createRequest(link.data("url"));
    });
  };
  
  function viewDateExpiry() {
    $.get($(this).data("url"), function(data) {
        modalDateExpiry.modal('show').html(data);
        modalDateExpiry.on('hidden.bs.modal', function() {
        modalDateExpiry.empty();
		});
	}, "html");
  };
  
  function createRequest( inUrl ) {
    $.ajax({
      cache     : false, 
      dataType  : "html",
      type    : "GET", 
      url     : inUrl,
      error     : function(jqXHR, textStatus, errorThrown) {}, 
      success   : function( inResponse ) {
        modalDateExpiry.modal('show').html(inResponse);
        modalDateExpiry.on('hidden.bs.modal', function() {
          modalDateExpiry.empty();
  			});
      }
    });
  };
  
  function print(){
  	window.open($(this).data("url"), '_blank', 'toolbar=0,location=0,menubar=0');
  }
  
});
</script>