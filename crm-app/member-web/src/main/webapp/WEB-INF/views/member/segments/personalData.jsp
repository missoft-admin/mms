<%@ include file="../../taglibs.jsp"%>


    <div class="tab-pane active" id="personal">
    <fieldset class="container-fluid">
      <div class="control-group">
        <form:label path="firstName" cssClass="control-label" cssErrorClass="control-label error">
          <spring:message code="firstname" />
        </form:label>
        <div class="controls">
          <form:input path="firstName" cssClass="input-xlarge" />
        </div>
      </div>

      <div class="control-group">
        <form:label path="lastName" cssClass="control-label" cssErrorClass="control-label error">
          <spring:message code="lastname" />
        </form:label>
        <div class="controls">
          <form:input path="lastName" cssClass="input-xlarge" />
        </div>
      </div>

      <div class="control-group">
        <spring:message code="id_card_number" var="idNumber" />
        <label for="idNumber" class="control-label"><b class="required">*</b><c:out value="${idNumber}" /></label>
        <div class="controls">
          <form:hidden path="idNumber" />
          <input type="text" name="idNumberTxt" class="form-control" placeholder="${idNumber}" />
            <div class="radio"><input type="radio" name="idNumberType" value="KTP"><label><spring:message code="ktp" /></label></div>
            <div class="radio"><input type="radio" name="idNumberType" value="SIM"><label><spring:message code="sim" /></label></div>
            <div class="radio"><input type="radio" name="idNumberType" value="PASSPORT"><label><spring:message code="passport" /></label></div>
        </div>
      </div>

      <%-- <div class="control-group">
        <form:label path="ktpId" cssClass="control-label" cssErrorClass="control-label error">
          <c:if test="${form.memberTypeCode == 'MTYP003'}"><span class="required">*</span></c:if>
          <spring:message code="ktp_id"/>
        </form:label>
        <div class="controls">
          <form:input path="ktpId" cssClass="input-xlarge"/>
        </div>
      </div> --%>

      <div class="control-group">
        <spring:message code="birthdate" var="birthdate" />
        <form:label path="birthDate" title="${birthdate}"
          cssClass="control-label" cssErrorClass="control-label error">${birthdate}</form:label>
        <div class="controls">
          <form:input path="birthDate" title="${birthdate} (dd-MM-yyyy)"
            cssClass="input-xlarge" placeholder="dd-MM-yyyy" id="birthDate" />
        </div>
      </div>

      <div class="control-group">
        <form:label path="gender" cssClass="control-label" cssErrorClass="control-label error">
          <spring:message code="gender" />
        </form:label>
        <div class="controls">
          <form:select path="gender" cssClass="input-xlarge">
            <form:option value="">&nbsp;</form:option>
            <form:options items="${genders}" itemValue="code" itemLabel="description" />
          </form:select>
        </div>
      </div>


      <hr />


      <!-- CONTACT DETAILS -->
      <div>
		    <div class="control-group">
		      <spring:message code="home_phone" var="homePhone" />
		      <label for="homePhone" class="control-label"><c:out value="${homePhone}" /></label>
		      <div id="homePhone" class="controls">
		        <form:input path="homePhone" cssClass="form-control" placeholder="${homePhone}" />
		      </div>
		    </div>

	      <div class="control-group">
	        <spring:message code="mobilephone" var="contact" />
	        <form:label path="contact" title="${contact}" cssClass="control-label" cssErrorClass="control-label error">
	          <span class="required">*</span>${contact}</form:label>
	        <div id="contact" class="controls">
	          <form:input path="contact" title="Contact" cssClass="input-xlarge" placeHolder="Contact Number"/><br/>
	          <input type="text" name="contact" class="input-xlarge form-control" placeholder="${contact} 2" />
	          <!-- <input type="button" onclick="addFields()" value="+" /> -->
	        </div>
	      </div>

	      <div class="control-group">
	        <label for="bestTimeToCall" class="control-label"> <spring:message code="best_time_to_call" /></label>
	        <div id="bestTimeToCall" class="controls checkbox">
	         <c:forEach items="${bestTimeToCall}" var="item">
	           <form:checkbox path="bestTimeToCall" value="${item.code}" label="${item.description}"  data-value="${item.code}" cssClass="checkboxFix" />
	         </c:forEach>
	        </div>
	      </div>
	
	      <div class="control-group">
	        <spring:message code="email" var="email" />
	        <form:label path="email" title="${email}" cssClass="control-label" cssErrorClass="control-label error">
	          <span class="required">*</span>${email}</form:label>
	        <div class="controls">
	          <form:input id="email" path="email" title="${email}" cssClass="input-xlarge" />
	        </div>
	      </div>
      </div>


      <!-- <hr /> -->


      <!-- ADDRESS -->
      <fieldset class="">
        <legend class="individualDetails"><spring:message code="address" /></legend>
        <legend class="professionalDetails"><spring:message code="business_address" /></legend>
        <div class="row-fluid address-line">
          <spring:message code="street" var="street" />
          <fieldset class="span9">
            <label for="street"><span class="required">*</span>${street }</label>
            <form:input path="street" placeholder="${street}" cssClass="span12" />
          </fieldset>
          <spring:message code="streetNo" var="streetNumber" />
          <fieldset class="span3">
            <label for="streetNumber">${streetNumber }</label>
            <form:input path="streetNumber" cssClass="span12" placeholder="${streetNumber}" />
          </fieldset>
        </div>
        <div class="row-fluid address-line">
          <spring:message code="block" var="block" />
          <fieldset class="span6">
            <label for="block">${block }</label>
            <form:input path="block" placeholder="${block}" cssClass="span12" />
          </fieldset>
          <spring:message code="km" var="km" />
          <fieldset class="span2 professionalDetails">
            <label for="km">${km }</label>
            <form:input path="km" placeholder="${km}" cssClass="span12" />
          </fieldset>
          <spring:message code="rt" var="rt" />
          <fieldset class="span2">
            <label for="rt">${rt }</label>
            <form:input path="rt" placeholder="${rt}" cssClass="span12" />
          </fieldset>
          <spring:message code="rw" var="rw" />
          <fieldset class="span2">
            <label for="rw">${rw }</label>
            <form:input path="rw" placeholder="${rw}" cssClass="span12" />
          </fieldset>
        </div>
        <div class="row-fluid address-line">
          <spring:message code="building" var="building" />
          <fieldset class="span8">
            <label for="building">${building }</label>
            <form:input path="building" placeholder="${building}" cssClass="span12" />
          </fieldset>
          <spring:message code="floor" var="floor" />
          <fieldset class="span2 professionalDetails">
            <label for="floor">${floor }</label>
            <form:input path="floor" placeholder="${floor}" cssClass="span12" />
          </fieldset>
          <spring:message code="room" var="room" />
          <fieldset class="span2 professionalDetails">
            <label for="room">${room }</label>
            <form:input path="room" placeholder="${room}" cssClass="span12" />
          </fieldset>
        </div>
        <div class="row-fluid address-line">
          <spring:message code="postcode" var="postCode" />
          <fieldset class="span2">
            <label for="postCode">${postCode }</label>
            <form:input path="postCode" placeholder="${postCode}" cssClass="span12" />
          </fieldset>
          <spring:message code="subdistrict" var="subdistrict" />
          <fieldset class="span4">
            <label for="subdistrict">${subdistrict }</label>
            <form:input path="subdistrict" placeholder="${subdistrict}" cssClass="span12" />
          </fieldset>
          <spring:message code="district" var="district" />
          <fieldset class="span4">
            <label for="district">${district }</label>
            <form:input path="district" placeholder="${district}" cssClass="span12" />
          </fieldset>
        </div>
        <div class="row-fluid address-line">
          <spring:message code="province" var="province" />
          <fieldset class="span6">
            <label for="province">${province }</label>
            <form:input path="province" placeholder="${province}" cssClass="span12" />
          </fieldset>
          <spring:message code="city" var="city" />
          <fieldset class="span6">
            <label for="city">${city }</label>
            <form:input path="city" placeholder="${city}" cssClass="span12" />
          </fieldset>
        </div>
      </fieldset>


      <!-- <hr /> -->


      <fieldset class="individualDetails" style="margin-top: 20px">
        <legend></legend>
        <div class="control-group">
          <form:label path="religionCode" cssClass="control-label"
            cssErrorClass="control-label error">
            <spring:message code="religion" />
          </form:label>
          <div class="controls">
            <form:select path="religionCode" cssClass="input-xlarge">
              <form:option value="">&nbsp;</form:option>
              <form:options items="${religions}" itemValue="code"
                itemLabel="description" />
            </form:select>
          </div>
        </div>

        <div class="control-group">
          <form:label path="maritalStatusCode" cssClass="control-label"
            cssErrorClass="control-label error">
            <spring:message code="marital_status" />
          </form:label>
          <div class="controls">
            <form:select path="maritalStatusCode" cssClass="input-xlarge">
              <form:option value="">&nbsp;</form:option>
              <form:options items="${maritalStatuses}" itemValue="code"
                itemLabel="description" />
            </form:select>
          </div>
        </div>

        <div class="control-group">
          <form:label path="familySizeCode" cssClass="control-label"
            cssErrorClass="control-label error">
            <spring:message code="family_size" />
          </form:label>
          <div class="controls">
            <form:select path="familySizeCode" cssClass="input-xlarge">
              <form:option value="">&nbsp;</form:option>
              <form:options items="${familySizes}" itemValue="code"
                itemLabel="description" />
            </form:select>
          </div>
        </div>


        <div class="control-group">
          <form:label path="childrenCountCode" cssClass="control-label"
            cssErrorClass="control-label error">
            <spring:message code="children" />
          </form:label>
          <div class="controls">
            <form:input id="childrenCount" path="childrenCountCode" cssClass="input-xlarge" />
          </div>
        </div>

        <div class="control-group">
          <form:label path="nationalityCode" cssClass="control-label"
            cssErrorClass="control-label error">
            <spring:message code="nationality" />
          </form:label>
          <div class="controls">
            <form:select path="nationalityCode" cssClass="input-xlarge">
              <form:option value="">&nbsp;</form:option>
              <form:options items="${nationalities}" itemValue="code"
                itemLabel="description" />
            </form:select>
          </div>
        </div>

        <div class="control-group">
          <form:label path="educationCode" cssClass="control-label"
            cssErrorClass="control-label error">
            <spring:message code="education" />
          </form:label>
          <div class="controls">
            <form:select path="educationCode" cssClass="input-xlarge">
              <form:option value="">&nbsp;</form:option>
              <form:options items="${educations}" itemValue="code"
                itemLabel="description" />
            </form:select>
          </div>
        </div>

        <div class="control-group">
          <form:label path="householdIncomeCode" cssClass="control-label"
            cssErrorClass="control-label error">
            <spring:message code="household_income" />
          </form:label>
          <div class="controls">
            <form:select path="householdIncomeCode" cssClass="input-xlarge">
              <form:option value="">&nbsp;</form:option>
              <form:options items="${householdIncomes}" itemValue="code"
                itemLabel="description" />
            </form:select>
          </div>
        </div>


        <%-- <hr />
        <div class="control-group">
          <form:label path="preferedLanguageCode" cssClass="control-label"
            cssErrorClass="control-label error">
            <spring:message code="prefered_language" />
          </form:label>
          <div class="controls">
            <form:select path="preferedLanguageCode" cssClass="input-xlarge">
              <form:option value="">&nbsp;</form:option>
              <form:options items="${preferedLanguages}" itemValue="code"
                itemLabel="description" />
            </form:select>
          </div>
        </div>
        <div class="control-group">
          <form:label path="childrenAges" cssClass="control-label"
            cssErrorClass="control-label error">
            <spring:message code="children_age" />
          </form:label>
          <div class="controls" id="childrenAges" style="overflow-y: auto;">

          </div>
        </div>
        <div class="control-group">
          <form:label path="personalIncomeCode" cssClass="control-label"
            cssErrorClass="control-label error">
            <spring:message code="personal_income" />
          </form:label>
          <div class="controls">
            <form:select path="personalIncomeCode" cssClass="input-xlarge">
              <form:option value="">&nbsp;</form:option>
              <form:options items="${personalIncomes}" itemValue="code"
                itemLabel="description" />
            </form:select>
          </div>
        </div> --%>
      </fieldset>


      <fieldset class="control-group form-horizontal" style="margin-top: 20px">
        <legend><spring:message code="vehicles_owned" /></legend>
        <div id="vehiclesOwned" class="controls checkbox">
          <form:checkboxes items="${vehiclesOwned}" path="vehiclesOwned" itemValue="code" itemLabel="description" />
        </div>
      </fieldset>


      <!-- <hr /> -->


      <div>
	      <fieldset class="control-group" style="margin-top: 20px">
	        <legend></legend>
	        <div class="controls">
	          <div class="checkbox">
	            <form:checkbox path="insuranceOwnership" />
	            <spring:message code="insurace" />
	          </div>
	          <div class="checkbox">
	            <form:checkbox id="hasCreditCard" path="creditCardOwnership" />
	            <spring:message code="credit_card" />
	          </div>
	        </div>
	      </fieldset>
      </div>

      <div class="span3 category-group" id="banks">
        <label><spring:message code="banks" />:</label>
        <c:forEach items="${banks}" var="bank">
          <label class="checkbox"> <input name="bankCodes"
            type="checkbox" value="${bank.code}"
            <c:if test="${not empty form.bankCodeMap[bank.code]}">checked</c:if>>
            ${bank.description}
          </label>
        </c:forEach>
      </div>

      <div class="clearfix"></div>


    <style><!-- fieldset.container-fluid { margin-top: 30px; } --></style>

    </fieldset>
    </div>