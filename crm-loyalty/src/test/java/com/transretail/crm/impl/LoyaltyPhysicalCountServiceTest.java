package com.transretail.crm.impl;

import java.util.List;

import junit.framework.Assert;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Configurable;
import org.springframework.security.authentication.TestingAuthenticationToken;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.context.SecurityContextImpl;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import com.google.common.collect.Lists;
import com.transretail.crm.common.service.dto.request.PageSortDto;
import com.transretail.crm.common.service.dto.response.ResultList;
import com.transretail.crm.core.entity.UserModel;
import com.transretail.crm.core.entity.lookup.LookupDetail;
import com.transretail.crm.core.entity.lookup.LookupHeader;
import com.transretail.crm.core.repo.LookupDetailRepo;
import com.transretail.crm.core.repo.LookupHeaderRepo;
import com.transretail.crm.core.repo.UserRepo;
import com.transretail.crm.core.security.model.impl.CustomSecurityUserDetailsImpl;
import com.transretail.crm.core.service.CodePropertiesService;
import com.transretail.crm.loyalty.dto.LoyaltyPhysicalCountDto;
import com.transretail.crm.loyalty.dto.LoyaltyPhysicalCountSummaryDto;
import com.transretail.crm.loyalty.entity.LoyaltyCardInventory;
import com.transretail.crm.loyalty.entity.LoyaltyPhysicalCount;
import com.transretail.crm.loyalty.entity.ManufactureOrderTier;
import com.transretail.crm.loyalty.entity.QLoyaltyCardInventory;
import com.transretail.crm.loyalty.repo.LoyaltyCardInventoryRepo;
import com.transretail.crm.loyalty.repo.LoyaltyPhysicalCountRepo;
import com.transretail.crm.loyalty.repo.ManufactureOrderTierRepo;
import com.transretail.crm.loyalty.service.LoyaltyPhysicalCountService;

@Configurable
@Transactional
@ContextConfiguration(locations = "classpath*:/META-INF/spring/applicationContext*.xml")
@RunWith(SpringJUnit4ClassRunner.class)
public class LoyaltyPhysicalCountServiceTest {
	@Autowired
	LoyaltyPhysicalCountService countService;
	@Autowired
	LoyaltyPhysicalCountRepo countRepo;
	@Autowired
	LookupDetailRepo detailRepo;
	@Autowired
	LookupHeaderRepo headerRepo;
	@Autowired
	UserRepo userRepo;
	@Autowired
	LoyaltyCardInventoryRepo inventoryRepo;
	@Autowired
	ManufactureOrderTierRepo productRepo;
	@Autowired
	CodePropertiesService codePropertiesService;
	
	@Before
	public void setup() {
		setupLookup();
		setupCurrentUser();
		setupPhysicalCount();
		setupInventory();
	}
	
	@After
	public void teardown() {
		countRepo.deleteAll();
		inventoryRepo.deleteAll();
		userRepo.deleteAll();
		detailRepo.deleteAll();
		headerRepo.deleteAll();
	}
	
	@Test
	public void testSavePhysicalCount() {
		LoyaltyPhysicalCountDto countDto = new LoyaltyPhysicalCountDto();
		String headOffice = codePropertiesService.getDetailInvLocationHeadOffice();
		countDto.setLocation(headOffice);
		LookupDetail regular = detailRepo.findByCode(codePropertiesService.getDetailMemberTierRegular());
		countDto.setCardType(regular);
		countDto.setStartingSeries("100000000000");
		countDto.setEndingSeries("100000000099");
		countDto.setQuantity(100L);
		countDto.setBoxNo("1");
		
		Long initialCount = countRepo.count();
		countService.savePhysicalCount(countDto);
		Assert.assertEquals(initialCount+1, countRepo.count());
	}
	
	@Test
	public void testListPhysicalCount() {
		ResultList<LoyaltyPhysicalCountDto> result = countService.listPhysicalCount(new PageSortDto());
		Assert.assertEquals(4, result.getTotalElements());
	}
	
	@Test
	public void testGetSummary() {
		ResultList<LoyaltyPhysicalCountSummaryDto> results = countService.getSummary(new PageSortDto());
		Assert.assertEquals(3, results.getTotalElements());
		for(LoyaltyPhysicalCountSummaryDto summaryDto: results.getResults()) {
			if(summaryDto.getCardType().getCode().equalsIgnoreCase(codePropertiesService.getDetailMemberTierRegular())) {
				Assert.assertTrue(summaryDto.getDifference() == -1);
			} else if(summaryDto.getCardType().getCode().equalsIgnoreCase(codePropertiesService.getDetailMemberTierSilver())) {
				Assert.assertTrue(summaryDto.getDifference() == -2);
			} else if(summaryDto.getCardType().getCode().equalsIgnoreCase(codePropertiesService.getDetailMemberTierGold())) {
				Assert.assertTrue(summaryDto.getDifference() == -3);
			}
		}
	}
	
	@Test
	public void testProcessInventory() {
		countService.processInventory();
		QLoyaltyCardInventory qInventory = QLoyaltyCardInventory.loyaltyCardInventory;
		Long missingCount = inventoryRepo.count(qInventory.status.code.eq(codePropertiesService.getDetailLoyaltyStatusMissing()));
		Assert.assertTrue(missingCount == 6);
	}
	
	@Test
	public void testValidateSeries() {
		Assert.assertFalse(countService.validateSeries("100000000000", "100000000000"));
		Assert.assertFalse(countService.validateSeries("100000000052", "100000000100"));
		Assert.assertTrue(countService.validateSeries("100000000050", "100000000050"));
	}
	
	private void setupInventory() {
		LookupDetail regular = detailRepo.findByCode(codePropertiesService.getDetailMemberTierRegular());
		LookupDetail silver = detailRepo.findByCode(codePropertiesService.getDetailMemberTierSilver());
		LookupDetail gold = detailRepo.findByCode(codePropertiesService.getDetailMemberTierGold());
		
		LookupDetail received = detailRepo.findByCode(codePropertiesService.getDetailLoyaltyStatusInactive());
		String headOffice = codePropertiesService.getDetailInvLocationHeadOffice();
		List<LoyaltyCardInventory> inventories = Lists.newArrayList();
		
		ManufactureOrderTier product = new ManufactureOrderTier();
		product.setName(regular);
		productRepo.save(product);
		
		for(long i = 100000000000L; i<100000000100L; i++) {
			LoyaltyCardInventory inventory = new LoyaltyCardInventory();
			inventory.setSequence(String.valueOf(i));
			inventory.setStatus(received);
			inventory.setLocation(headOffice);
			inventory.setProduct(product);
			inventories.add(inventory);
		}
		
		product = new ManufactureOrderTier();
		product.setName(silver);
		productRepo.save(product);
		
		for(long i = 200000000000L; i<200000000100L; i++) {
			LoyaltyCardInventory inventory = new LoyaltyCardInventory();
			inventory.setSequence(String.valueOf(i));
			inventory.setStatus(received);
			inventory.setLocation(headOffice);
			inventory.setProduct(product);
			inventories.add(inventory);
		}
		
		product = new ManufactureOrderTier();
		product.setName(gold);
		productRepo.save(product);
		
		for(long i = 300000000000L; i<300000000100L; i++) {
			LoyaltyCardInventory inventory = new LoyaltyCardInventory();
			inventory.setSequence(String.valueOf(i));
			inventory.setStatus(received);
			inventory.setLocation(headOffice);
			inventory.setProduct(product);
			inventories.add(inventory);
		}
		
		inventoryRepo.save(inventories);
	}
	
	private void setupPhysicalCount() {
		String headOffice = codePropertiesService.getDetailInvLocationHeadOffice();
		LookupDetail regular = detailRepo.findByCode(codePropertiesService.getDetailMemberTierRegular());
		LookupDetail silver = detailRepo.findByCode(codePropertiesService.getDetailMemberTierSilver());
		LookupDetail gold = detailRepo.findByCode(codePropertiesService.getDetailMemberTierGold());
		
		LoyaltyPhysicalCount count = new LoyaltyPhysicalCount();
		count.setLocation(headOffice);
		count.setCardType(regular);
		count.setStartingSeries("100000000000");
		count.setEndingSeries("100000000049");
		count.setBoxNo("1");
		count.setQuantity(50L);
		countRepo.save(count);
		
		count = new LoyaltyPhysicalCount();
		count.setLocation(headOffice);
		count.setCardType(regular);
		count.setStartingSeries("100000000051");
		count.setEndingSeries("100000000099");
		count.setBoxNo("1");
		count.setQuantity(49L);
		countRepo.save(count);
		
		count = new LoyaltyPhysicalCount();
		count.setLocation(headOffice);
		count.setCardType(silver);
		count.setStartingSeries("200000000000");
		count.setEndingSeries("200000000097");
		count.setBoxNo("2");
		count.setQuantity(98L);
		countRepo.save(count);
		
		count = new LoyaltyPhysicalCount();
		count.setLocation(headOffice);
		count.setCardType(gold);
		count.setStartingSeries("300000000003");
		count.setEndingSeries("300000000099");
		count.setBoxNo("3");
		count.setQuantity(97L);
		countRepo.save(count);
	}
	
	private void setupLookup() {

		LookupHeader header = new LookupHeader(codePropertiesService.getHeaderLoyaltyStatus());
		header.setDescription("MO STATUS");
		headerRepo.save(header);
		
		LookupDetail detail = new LookupDetail(codePropertiesService.getDetailLoyaltyStatusActive());
		detail.setDescription("ACTIVE");
		detail.setHeader(header);
		detailRepo.save(detail);
		
		detail = new LookupDetail(codePropertiesService.getDetailLoyaltyStatusInactive());
		detail.setDescription("INACTIVE");
		detail.setHeader(header);
		detailRepo.save(detail);
		
		detail = new LookupDetail(codePropertiesService.getDetailLoyaltyStatusAllocated());
		detail.setDescription("ALLOCATED");
		detail.setHeader(header);
		detailRepo.save(detail);
		
		detail = new LookupDetail(codePropertiesService.getDetailLoyaltyStatusForAllocation());
		detail.setDescription("FOR ALLOCATION");
		detail.setHeader(header);
		detailRepo.save(detail);
		
		detail = new LookupDetail(codePropertiesService.getDetailLoyaltyStatusInTransit());
		detail.setDescription("IN TRANSIT");
		detail.setHeader(header);
		detailRepo.save(detail);
		
		detail = new LookupDetail(codePropertiesService.getDetailLoyaltyStatusMissing());
		detail.setDescription("MISSING");
		detail.setHeader(header);
		detailRepo.save(detail);
		
		header = new LookupHeader(codePropertiesService.getHeaderMembershipTier());
		header.setDescription("MEMBERSHIP TIER");
		headerRepo.save(header);
		
		detail = new LookupDetail(codePropertiesService.getDetailMemberTierGold());
		detail.setDescription("GOLD");
		detail.setHeader(header);
		detailRepo.save(detail);
		
		detail = new LookupDetail(codePropertiesService.getDetailMemberTierSilver());
		detail.setDescription("SILVER");
		detail.setHeader(header);
		detailRepo.save(detail);
		
		detail = new LookupDetail(codePropertiesService.getDetailMemberTierRegular());
		detail.setDescription("REGULAR");
		detail.setHeader(header);
		detailRepo.save(detail);
		
		header = new LookupHeader(codePropertiesService.getHeaderInventoryLocation());
		header.setDescription("INV LOCATION");
		headerRepo.save(header);
		
		detail = new LookupDetail(codePropertiesService.getDetailInvLocationDenpasar());
		detail.setDescription("DENPASAR");
		detail.setHeader(header);
		detailRepo.save(detail);
		
		detail = new LookupDetail(codePropertiesService.getDetailInvLocationLebakBulus());
		detail.setDescription("LEBAK BULUS");
		detail.setHeader(header);
		detailRepo.save(detail);
		
		detail = new LookupDetail(codePropertiesService.getDetailInvLocationHeadOffice());
		detail.setDescription("HEAD OFFICE");
		detail.setHeader(header);
		detailRepo.save(detail);
	}
	
	private void setupCurrentUser() {
		UserModel currentUser = new UserModel();
    	currentUser.setUsername("loggedInUser");
    	currentUser.setPassword("");
    	currentUser.setEnabled(true);
    	currentUser.setInventoryLocation(codePropertiesService.getDetailInvLocationHeadOffice());
    	currentUser.setStoreCode( "store1" );
    	userRepo.save(currentUser);

        CustomSecurityUserDetailsImpl user = new CustomSecurityUserDetailsImpl();
        user.setUserId(currentUser.getId());
        user.setUsername(currentUser.getUsername());
        List<GrantedAuthority> authorities = Lists.newArrayList();
        authorities.add(new SimpleGrantedAuthority("ROLE_CSS"));
        user.setAuthorities(authorities);
        user.setInventoryLocation(codePropertiesService.getDetailInvLocationHeadOffice());

        SecurityContextImpl securityContext = new SecurityContextImpl();
        securityContext.setAuthentication(new TestingAuthenticationToken(user, ""));
        SecurityContextHolder.setContext(securityContext);
    }
}
