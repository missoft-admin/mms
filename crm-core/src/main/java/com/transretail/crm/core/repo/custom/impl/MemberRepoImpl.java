package com.transretail.crm.core.repo.custom.impl;


import com.google.common.collect.Lists;
import com.mysema.query.BooleanBuilder;
import com.mysema.query.jpa.impl.JPAQuery;
import com.mysema.query.types.ConstructorExpression;
import com.mysema.query.types.expr.BooleanExpression;
import com.transretail.crm.core.dto.MemberDto;
import com.transretail.crm.core.dto.MemberMonitorSearchDto;
import com.transretail.crm.core.entity.MemberModel;
import com.transretail.crm.core.entity.QEmployeePurchaseTxnModel;
import com.transretail.crm.core.entity.QMemberModel;
import com.transretail.crm.core.entity.QPointsTxnModel;
import com.transretail.crm.core.entity.enums.*;
import com.transretail.crm.core.entity.lookup.QLookupDetail;
import com.transretail.crm.core.repo.custom.MemberRepoCustom;
import com.transretail.crm.core.util.BooleanExprUtil;
import org.apache.commons.lang.mutable.MutableInt;
import org.joda.time.LocalDate;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;


public class MemberRepoImpl implements MemberRepoCustom {

	@PersistenceContext
	EntityManager em;


	@Override
	public List<MemberModel> findByCriteria(MemberModel inModel, String employeeType) {
		JPAQuery theQuery = new JPAQuery(em);
		QMemberModel theModel = QMemberModel.memberModel;

		theQuery.from(theModel).where(
				new BooleanBuilder().andAnyOf(
						BooleanExprUtil.INSTANCE.isStringPropertyLike(theModel.accountId, inModel.getAccountId()),
						BooleanExprUtil.INSTANCE.isStringPropertyLike(theModel.contact, inModel.getContact()),
						BooleanExprUtil.INSTANCE.isStringPropertyLike(theModel.username, inModel.getUsername()),
						BooleanExprUtil.INSTANCE.isStringPropertyLike(theModel.firstName, inModel.getFirstName()),
						BooleanExprUtil.INSTANCE.isStringPropertyLike(theModel.lastName, inModel.getLastName()),
						BooleanExprUtil.INSTANCE.isStringPropertyLike(theModel.lastName, inModel.getFirstName()),
						BooleanExprUtil.INSTANCE.isStringPropertyLike(theModel.email, inModel.getEmail())
						).and( theModel.memberType.code.notEqualsIgnoreCase( employeeType ) ) );
		return theQuery.list(theModel);
	}
	
	@Override
	public List<MemberModel> findEmployeeByCriteria(MemberModel inModel, String employeeType) {
		JPAQuery theQuery = new JPAQuery(em);
		QMemberModel theModel = QMemberModel.memberModel;

		theQuery.from(theModel).where(
				new BooleanBuilder().andAnyOf(
						BooleanExprUtil.INSTANCE.isStringPropertyLike(theModel.accountId, inModel.getAccountId()),
						BooleanExprUtil.INSTANCE.isStringPropertyLike(theModel.contact, inModel.getContact()),
						BooleanExprUtil.INSTANCE.isStringPropertyLike(theModel.username, inModel.getUsername()),
						BooleanExprUtil.INSTANCE.isStringPropertyLike(theModel.firstName, inModel.getFirstName()),
						BooleanExprUtil.INSTANCE.isStringPropertyLike(theModel.lastName, inModel.getLastName()),
						BooleanExprUtil.INSTANCE.isStringPropertyLike(theModel.lastName, inModel.getFirstName()),
						BooleanExprUtil.INSTANCE.isStringPropertyLike(theModel.email, inModel.getEmail())
						).and(theModel.memberType.code.eq(employeeType)));
		return theQuery.list(theModel);
	}

	@Override
    public List<MemberModel> findRewardQualifiedEmp( Double inAmtFrom, Double inAmtTo,
    		Date inIncDateFrom, Date inIncDateTo, String employeeType ) {
		JPAQuery theQuery = new JPAQuery(em);
        QEmployeePurchaseTxnModel thePurchaseTxn = QEmployeePurchaseTxnModel.employeePurchaseTxnModel;

         theQuery.from( thePurchaseTxn ).
				groupBy( thePurchaseTxn.memberModel.id, thePurchaseTxn.memberModel.registeredStore ).
				where( new BooleanBuilder().orAllOf(
						BooleanExprUtil.INSTANCE.isDateTimePropertyGoe( thePurchaseTxn.created, inIncDateFrom ),
						BooleanExprUtil.INSTANCE.isDateTimePropertyLoe( thePurchaseTxn.created, inIncDateTo ),
						isValidEmployee( thePurchaseTxn.memberModel, employeeType  ) ) ).
				having( new BooleanBuilder().orAllOf(
						thePurchaseTxn.transactionAmount.sum().goe( inAmtFrom ),
						inAmtTo == null ? null : thePurchaseTxn.transactionAmount.sum().loe( inAmtTo ) ) )
				.list( thePurchaseTxn.memberModel.id, thePurchaseTxn.memberModel.registeredStore );

//        if ( CollectionUtils.isNotEmpty( memberIds ) ) {
//        	JPAQuery jpaQuery = new JPAQuery(em);
//            QMemberModel member = QMemberModel.memberModel;
//        	JPQLQuery jplQuery = jpaQuery.from( member ).where( member.id.in( memberIds ) );
//            return jplQuery.list( member );
//        }
        return new ArrayList<MemberModel>();
    }


	@Override
	public List<String> findDeletedRewardQualifiedEmpIds( Date inFromDate, Date inToDate ) {

		JPAQuery theQuery = new JPAQuery(em);
        QPointsTxnModel thePoint = QPointsTxnModel.pointsTxnModel;
        
        return theQuery.from( thePoint ).
        		where(  new BooleanBuilder().orAllOf( 
	        				new BooleanBuilder().orAllOf( 
	        						BooleanExprUtil.INSTANCE.isEnumPropertyIn( thePoint.transactionType, PointTxnType.REWARD ),
	        						BooleanExprUtil.INSTANCE.isEnumPropertyIn( thePoint.status, TxnStatus.DELETED ),
	        						thePoint.incDateFrom.eq( inFromDate ),
	        						thePoint.incDateTo.eq( inToDate ) ) ) ).
        		orderBy( thePoint.transactionPoints.desc() ).
        		list( thePoint.memberModel.accountId );
	}

	@Override
	public List<MemberModel> findByNpwpId(String npwpId) {
		QMemberModel memberModel = QMemberModel.memberModel;
		JPAQuery query = new JPAQuery(em);
		
		return query.from(memberModel).where(memberModel.npwp.npwpId.eq(npwpId)).list(memberModel);
	}

    private BooleanBuilder isValidEmployee( QMemberModel theMember, String employeeType ) {
    	return new BooleanBuilder().orAllOf(
    			BooleanExprUtil.INSTANCE.isEnumPropertyIn( theMember.accountStatus, MemberStatus.ACTIVE ),
    	    	BooleanExprUtil.INSTANCE.isStringPropertyEqual( theMember.memberType.code,
                        employeeType) );
    }
    
    @Override
	public List<MemberDto> getMonitoredMembers(Integer transactionCount, LocalDate date) {
		QPointsTxnModel points = QPointsTxnModel.pointsTxnModel;
		QMemberModel qmm = QMemberModel.memberModel;
        QLookupDetail qLookupMemberType = QLookupDetail.lookupDetail;
		JPAQuery query = new JPAQuery(em);
		
		LocalDate today = date == null ? LocalDate.now() : date;
		Date todayDate = today.toDateTimeAtStartOfDay().toDate();
		Date tomorrowDate = today.plusDays(1).toDateTimeAtStartOfDay().toDate();
		
		return query.from(points)
		        .leftJoin(points.transactionContributer, qmm)
		        .rightJoin(qmm.memberType, qLookupMemberType)
				.where(points.transactionDateTime.between(todayDate, tomorrowDate)
					.and(points.transactionType.eq(PointTxnType.EARN))
					.and(qLookupMemberType.code.ne("MTYP002")))
				.groupBy(qmm.accountId, 
				        qmm.username,
				        qmm.firstName, 
				        qmm.lastName,
				        qLookupMemberType.code,
				        qLookupMemberType.createUser,
				        qLookupMemberType.created,
				        qLookupMemberType.lastUpdateUser,
				        qLookupMemberType.lastUpdated,
				        qLookupMemberType.description,
				        qLookupMemberType.header,
				        qLookupMemberType.status)
				.having(points.transactionNo.countDistinct().goe(transactionCount))
				.list(ConstructorExpression.create(MemberDto.class, 
				        qmm.accountId, 
				        qmm.username,
				        qmm.firstName, 
				        qmm.lastName,
				        qLookupMemberType,
						points.transactionNo.countDistinct()));
	}
    
    @Override
	public List<MemberDto> getMonitoredMembers(Integer transactionCount, LocalDate date, int limit, int offset, MutableInt totalElements) {
		QPointsTxnModel points = QPointsTxnModel.pointsTxnModel;
		JPAQuery query = new JPAQuery(em);
		
		LocalDate today = date == null ? LocalDate.now() : date;
		Date todayDate = today.toDateTimeAtStartOfDay().toDate();
		Date tomorrowDate = today.plusDays(1).toDateTimeAtStartOfDay().toDate();
		
		query = query.from(points)
                .where(points.transactionDateTime.between(todayDate, tomorrowDate)
                        .and(points.transactionType.eq(PointTxnType.EARN))
                        .and(points.transactionContributer.memberType.code.ne("MTYP002")))
                    .groupBy(points.transactionContributer.accountId, points.transactionContributer.username,
                            points.transactionContributer.firstName, points.transactionContributer.lastName)
                    .having(points.transactionNo.countDistinct().goe(transactionCount));
		
		totalElements.setValue(query.list(points.transactionContributer.accountId, 
            		        points.transactionContributer.username,
                            points.transactionContributer.firstName, 
                            points.transactionContributer.lastName).size());
		
		List<MemberDto> list = query.limit(limit).offset(offset)
                            .list(ConstructorExpression.create(MemberDto.class, 
                                points.transactionContributer.accountId, points.transactionContributer.username,
                                points.transactionContributer.firstName, points.transactionContributer.lastName, 
                                points.transactionNo.countDistinct()));
		
		return list;
	}
    
    @Override
	public List<MemberDto> getMonitoredMembers(MemberMonitorSearchDto dto) {
		QPointsTxnModel points = QPointsTxnModel.pointsTxnModel;
		JPAQuery query = new JPAQuery(em);
		
		return query.from(points)
				.where(dto.createSearchExpression()
					.and(points.transactionType.eq(PointTxnType.EARN))
					.and(points.transactionContributer.memberType.code.ne("MTYP002")))
				.groupBy(points.transactionContributer.accountId, points.transactionContributer.username,
						points.transactionContributer.firstName, points.transactionContributer.lastName)
				.having(points.transactionNo.countDistinct().goe(dto.getTransactionCount()))
				.list(ConstructorExpression.create(MemberDto.class, 
						points.transactionContributer.accountId, points.transactionContributer.username,
						points.transactionContributer.firstName, points.transactionContributer.lastName, 
						points.transactionNo.countDistinct()));
	}
    
    @Override
	public List<MemberDto> getMonitoredMembers(MemberMonitorSearchDto dto, int limit, int offset, MutableInt totalElements) {
		QPointsTxnModel points = QPointsTxnModel.pointsTxnModel;
		JPAQuery query = new JPAQuery(em);
		
		query = query.from(points)
                .where(dto.createSearchExpression()
                        .and(points.transactionType.eq(PointTxnType.EARN))
                        .and(points.transactionContributer.memberType.code.ne("MTYP002")))
                    .groupBy(points.transactionContributer.accountId, points.transactionContributer.username,
                            points.transactionContributer.firstName, points.transactionContributer.lastName)
                    .having(points.transactionNo.countDistinct().goe(dto.getTransactionCount()));
		
		totalElements.setValue(query.list(points.transactionContributer.accountId, 
            		        points.transactionContributer.username,
                            points.transactionContributer.firstName, 
                            points.transactionContributer.lastName).size());
		        
		List<MemberDto> list = query.offset(offset).limit(limit)
                .list(ConstructorExpression.create(MemberDto.class, 
                        points.transactionContributer.accountId, points.transactionContributer.username,
                        points.transactionContributer.firstName, points.transactionContributer.lastName,
                        points.transactionNo.countDistinct()));
		
		return list;
	}
	
	@Override
	public List<MemberDto> getMonitoredEmployees(Integer transactionCount, LocalDate date) {
		QEmployeePurchaseTxnModel purchases = QEmployeePurchaseTxnModel.employeePurchaseTxnModel;
		QMemberModel qmm = QMemberModel.memberModel;
		QLookupDetail qLookupMemberType = QLookupDetail.lookupDetail;
		QLookupDetail qLookupEmployeeType = new QLookupDetail("qLookupEmployeeType");
		
		JPAQuery query = new JPAQuery(em);
		
		LocalDate today = date == null ? LocalDate.now() : date;
		Date todayDate = today.toDateTimeAtStartOfDay().toDate();
		Date tomorrowDate = today.plusDays(1).toDateTimeAtStartOfDay().toDate();
		
		return query.from(purchases)
		        .rightJoin(purchases.memberModel, qmm)
		        .rightJoin(qmm.memberType, qLookupMemberType)
		        .rightJoin(qmm.empType, qLookupEmployeeType)
				.where(purchases.transactionDateTime.between(todayDate, tomorrowDate)
					.and(purchases.transactionType.eq(VoucherTransactionType.PURCHASE))
					.and(qLookupMemberType.code.eq("MTYP002")))
				.groupBy(qmm.accountId, 
				        qmm.username, 
				        qmm.firstName, 
				        qmm.lastName,
                        qLookupMemberType.code,
                        qLookupMemberType.createUser,
                        qLookupMemberType.created,
                        qLookupMemberType.lastUpdateUser,
                        qLookupMemberType.lastUpdated,
                        qLookupMemberType.description,
                        qLookupMemberType.header,
                        qLookupMemberType.status,
                        qLookupEmployeeType.code,
                        qLookupEmployeeType.createUser,
                        qLookupEmployeeType.created,
                        qLookupEmployeeType.lastUpdateUser,
                        qLookupEmployeeType.lastUpdated,
                        qLookupEmployeeType.description,
                        qLookupEmployeeType.header,
                        qLookupEmployeeType.status)
				.having(purchases.transactionNo.countDistinct().goe(transactionCount))
				.list(ConstructorExpression.create(MemberDto.class,
				        qmm.accountId, 
				        qmm.username, 
				        qmm.firstName, 
				        qmm.lastName,
				        qLookupMemberType,
				        qLookupEmployeeType,
						purchases.transactionNo.countDistinct()));
	}
	
	@Override
	public List<MemberDto> getMonitoredEmployees(Integer transactionCount, LocalDate date, int limit, int offset, MutableInt totalElements) {
		QEmployeePurchaseTxnModel purchases = QEmployeePurchaseTxnModel.employeePurchaseTxnModel;
		JPAQuery query = new JPAQuery(em);
		
		LocalDate today = date == null ? LocalDate.now() : date;
		Date todayDate = today.toDateTimeAtStartOfDay().toDate();
		Date tomorrowDate = today.plusDays(1).toDateTimeAtStartOfDay().toDate();
		
		query = query.from(purchases)
                .where(purchases.transactionDateTime.between(todayDate, tomorrowDate)
                        .and(purchases.transactionType.eq(VoucherTransactionType.PURCHASE))
                        .and(purchases.memberModel.memberType.code.eq("MTYP002")))
                    .groupBy(purchases.memberModel.accountId, purchases.memberModel.username, 
                            purchases.memberModel.firstName, purchases.memberModel.lastName)
                    .having(purchases.transactionNo.countDistinct().goe(transactionCount));
                            
		totalElements.setValue(query.list(purchases.memberModel.accountId, 
            		        purchases.memberModel.username, 
                            purchases.memberModel.firstName, 
                            purchases.memberModel.lastName).size());
		
		List<MemberDto> list = query.limit(limit).offset(offset)
                .list(ConstructorExpression.create(MemberDto.class, 
                purchases.memberModel.accountId, purchases.memberModel.username, 
                purchases.memberModel.firstName, purchases.memberModel.lastName,
                purchases.transactionNo.countDistinct()));
                    
		return list;
	}
	
	@Override
	public Long countTranscactionsToday(String accountId, String memberTypeCode) {
		JPAQuery query = new JPAQuery(em);
		
		LocalDate today = LocalDate.now();
		Date todayDate = today.toDateTimeAtStartOfDay().toDate();
		Date tomorrowDate = today.plusDays(1).toDateTimeAtStartOfDay().toDate();
		
		if(memberTypeCode.equals("MTYP002")) {
			QEmployeePurchaseTxnModel purchases = QEmployeePurchaseTxnModel.employeePurchaseTxnModel;
			
			return query.from(purchases)
					.where(purchases.transactionDateTime.between(todayDate, tomorrowDate)
						.and(purchases.transactionType.eq(VoucherTransactionType.PURCHASE))
						.and(purchases.memberModel.accountId.eq(accountId)))
					.groupBy(purchases.memberModel.accountId, purchases.memberModel.username, 
							purchases.memberModel.firstName, purchases.memberModel.lastName)
					.singleResult(purchases.transactionNo.countDistinct());
		}
		
		QPointsTxnModel points = QPointsTxnModel.pointsTxnModel;
		
		return query.from(points)
				.where(points.transactionDateTime.between(todayDate, tomorrowDate)
					.and(points.transactionType.eq(PointTxnType.EARN))
					.and(points.transactionContributer.accountId.eq(accountId)))
				.groupBy(points.transactionContributer.accountId, points.transactionContributer.username,
						points.transactionContributer.firstName, points.transactionContributer.lastName)
				.singleResult(points.transactionNo.countDistinct());
	}

    @Override
    public MemberModel findByAccountIdAndMemberTypeCode(String accountId, String memberTypeCode) {
        QMemberModel qMemberModel = QMemberModel.memberModel;
        return new JPAQuery(em).from(qMemberModel)
            .where(qMemberModel.accountId.eq(accountId).and(qMemberModel.memberType.code.eq(memberTypeCode)))
            .singleResult(qMemberModel);
    }
    
    @Override
    public List<MemberModel> findMembersWithExpiringTransactions(LocalDate expiryDate) {
        List<MemberModel> results = Lists.newArrayList();
        QPointsTxnModel points = QPointsTxnModel.pointsTxnModel;
    	JPAQuery query = new JPAQuery(em);

    	List<Long> memberIds = query.from(points)
             .innerJoin(points.memberModel)
             .where(BooleanExpression.allOf(
    			points.expiryDate.eq(expiryDate),
    			points.memberModel.accountStatus.eq(MemberStatus.ACTIVE),
                points.memberModel.enabled.isTrue()
    		))
             .distinct()
    		.list(points.memberModel.id);
        if(memberIds != null && memberIds.size() > 0) {
            QMemberModel qMemberModel = QMemberModel.memberModel;
            results = new JPAQuery(em).from(qMemberModel).where(qMemberModel.id.in(memberIds)).list(qMemberModel);
        }
        return results;
    }

}
