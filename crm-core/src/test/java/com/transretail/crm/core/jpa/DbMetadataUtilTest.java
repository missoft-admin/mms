package com.transretail.crm.core.jpa;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.mysema.query.sql.H2Templates;

/**
 * @author Allan G. Ramirez (agramirez@exist.com)
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = "classpath*:/META-INF/spring/applicationContext*.xml")
public class DbMetadataUtilTest {
    @Autowired
    private DbMetadataUtil dbMetadataUtil;

    @Test
    public void test() {
        assertEquals(DbMetadataUtil.DbType.H2, dbMetadataUtil.getDbType());
        assertTrue(dbMetadataUtil.getSqlTempates() instanceof H2Templates);
    }

}
