package com.transretail.crm.core.repo.custom.impl;


import com.mysema.query.BooleanBuilder;
import com.mysema.query.jpa.impl.JPAQuery;
import com.transretail.crm.core.entity.EmployeePurchaseTxnModel;
import com.transretail.crm.core.entity.QEmployeePurchaseTxnModel;
import com.transretail.crm.core.entity.enums.MemberStatus;
import com.transretail.crm.core.entity.enums.TxnStatus;
import com.transretail.crm.core.repo.custom.EmployeePurchaseTxnRepoCustom;
import com.transretail.crm.core.util.BooleanExprUtil;
import com.transretail.crm.core.util.CalendarUtil;
import org.joda.time.LocalDate;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.Calendar;
import java.util.Date;
import java.util.List;


public class EmployeePurchaseTxnRepoImpl implements EmployeePurchaseTxnRepoCustom {

    @PersistenceContext
    EntityManager em;


	
	@Override
	public List<EmployeePurchaseTxnModel> findAllActiveByIdAndPostingDates (String employeeId, Calendar inCalFrom, Calendar inCalTo ) {
	
		Calendar theDateStart = CalendarUtil.INSTANCE.getFromCal(inCalFrom);
		Calendar theDateEnd = CalendarUtil.INSTANCE.getToCal(inCalTo);

		JPAQuery theQuery = new JPAQuery(em);
        QEmployeePurchaseTxnModel thePurchaseTxn = QEmployeePurchaseTxnModel.employeePurchaseTxnModel;
        
        return theQuery.from(thePurchaseTxn).where(new BooleanBuilder().orAllOf( 
				/*thePurchaseTxn.created.goe( new DateTime( theDateStart.getTime() ) ),
				thePurchaseTxn.created.loe( new DateTime( theDateEnd.getTime() ) ),*/
				thePurchaseTxn.transactionDateTime.between( theDateStart.getTime(), theDateEnd.getTime() ),
				thePurchaseTxn.status.eq( TxnStatus.ACTIVE ),
				thePurchaseTxn.memberModel.accountId.eq( employeeId ) ) ).
        		orderBy(thePurchaseTxn.transactionDateTime.desc(), thePurchaseTxn.transactionNo.desc()).
        		list( thePurchaseTxn );
	}
	
	@Override
	public Double findTotalTxnAmountByAccountId( String inEmpId, Calendar inCalFrom, Calendar inCalTo ) {

		Calendar theDateStart = CalendarUtil.INSTANCE.getFromCal(inCalFrom);
		Calendar theDateEnd = CalendarUtil.INSTANCE.getToCal(inCalTo);
    	
    	JPAQuery theQuery = new JPAQuery(em);
        QEmployeePurchaseTxnModel thePurchaseTxn = QEmployeePurchaseTxnModel.employeePurchaseTxnModel;
        
        return theQuery.from(thePurchaseTxn).
				groupBy( thePurchaseTxn.memberModel ).
				where( new BooleanBuilder().orAllOf( 
						/*thePurchaseTxn.created.goe( new DateTime( theDateStart.getTime() ) ),
						thePurchaseTxn.created.loe( new DateTime( theDateEnd.getTime() ) ),*/
						thePurchaseTxn.transactionDateTime.between( theDateStart.getTime(), theDateEnd.getTime() ),
						thePurchaseTxn.status.eq( TxnStatus.ACTIVE ),
						thePurchaseTxn.memberModel.accountId.eq( inEmpId ) ) ).
				singleResult( thePurchaseTxn.transactionAmount.sum().doubleValue() );
	}
	
	@Override
	public List<EmployeePurchaseTxnModel> findAllPendingAdjustments() {
		JPAQuery theQuery = new JPAQuery(em);
        QEmployeePurchaseTxnModel thePurchaseTxn = QEmployeePurchaseTxnModel.employeePurchaseTxnModel;
        
        List<EmployeePurchaseTxnModel> theTxns;
        
        theTxns = theQuery.from(thePurchaseTxn)
        		.where( new BooleanBuilder( thePurchaseTxn.status.eq( TxnStatus.FORAPPROVAL ) ).or( 
        				new BooleanBuilder( thePurchaseTxn.status.eq( TxnStatus.REJECTED ) ) ) )
        		.list( thePurchaseTxn );
        
    	return theTxns;
	}

	@Override
	public Double findTotalAmount( Long inMemberId, Date inFromDate, Date inToDate ) {

    	JPAQuery theQuery = new JPAQuery(em);
        QEmployeePurchaseTxnModel thePurchaseTxn = QEmployeePurchaseTxnModel.employeePurchaseTxnModel;

        return theQuery.from(thePurchaseTxn).
				groupBy( thePurchaseTxn.memberModel ).
				where( new BooleanBuilder().orAllOf( 
						BooleanExprUtil.INSTANCE.isDateTimePropertyGoe( thePurchaseTxn.created, inFromDate ),
						BooleanExprUtil.INSTANCE.isDateTimePropertyLoe( thePurchaseTxn.created, inToDate ),
						BooleanExprUtil.INSTANCE.isEnumPropertyIn( thePurchaseTxn.memberModel.accountStatus, MemberStatus.ACTIVE ),
						BooleanExprUtil.INSTANCE.isNumberPropertyEqual( thePurchaseTxn.memberModel.id, inMemberId ) ) ).
				singleResult( thePurchaseTxn.transactionAmount.sum().doubleValue() );
	}
	
	public List<EmployeePurchaseTxnModel> findTransactionsToday(String accountId) {
		QEmployeePurchaseTxnModel purchases = QEmployeePurchaseTxnModel.employeePurchaseTxnModel;
		JPAQuery query = new JPAQuery(em);
		
		LocalDate today = LocalDate.now();
		Date todayDate = today.toDateTimeAtStartOfDay().toDate();
		Date tomorrowDate = today.plusDays(1).toDateTimeAtStartOfDay().toDate();
		
		return query.from(purchases)
			.where(purchases.memberModel.accountId.eq(accountId)
			.and(purchases.transactionDateTime.between(todayDate, tomorrowDate)))
			.list(purchases);
	}
	
	public List<EmployeePurchaseTxnModel> findTransactionsByDate(String accountId, LocalDate date) {
		QEmployeePurchaseTxnModel purchases = QEmployeePurchaseTxnModel.employeePurchaseTxnModel;
		JPAQuery query = new JPAQuery(em);
		
		Date todayDate = date.toDateTimeAtStartOfDay().toDate();
		Date tomorrowDate = date.plusDays(1).toDateTimeAtStartOfDay().toDate();
		
		return query.from(purchases)
			.where(purchases.memberModel.accountId.eq(accountId)
			.and(purchases.transactionDateTime.between(todayDate, tomorrowDate)))
			.list(purchases);
	}
}
