package com.transretail.crm.giftcard.service;

import com.transretail.crm.common.service.dto.response.ResultList;
import com.transretail.crm.common.service.exception.MessageSourceResolvableException;
import com.transretail.crm.giftcard.dto.GiftCardBurnCardDto;
import com.transretail.crm.giftcard.dto.GiftCardBurnCardSearchDto;
import com.transretail.crm.giftcard.dto.GiftCardExpiryExtensionDto;
import com.transretail.crm.giftcard.dto.GiftCardExpiryExtensionSearchDto;

/**
 * @author ftopico
 */
public interface GiftCardExpiryExtensionService {

    ResultList<GiftCardExpiryExtensionDto> searchGiftCardDateExpiryExtensionRequests(GiftCardExpiryExtensionSearchDto searchDto);

    String createExtendNo();
    
    void saveExpiryExtensionRequest(GiftCardExpiryExtensionDto dto) throws MessageSourceResolvableException;

    GiftCardExpiryExtensionDto getExpiryExtensionByExtendNo(String extendNo);

    void rejectRequest(String extendNo);
    
    void approveRequest(String extendNo);
    
    boolean isDateExtensionRequestUnique(GiftCardExpiryExtensionDto dto);
}
