package com.transretail.crm.core.entity;


import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Lob;
import javax.persistence.Table;

import org.apache.commons.lang3.StringUtils;

import com.transretail.crm.core.entity.support.CustomAuditableEntity;


@Entity
@Table(name="CRM_FILE")
public class CrmFile extends CustomAuditableEntity<Long> {

	private static final long serialVersionUID = 1L;

	public static enum FileType { IMAGE, HTML };



	@Column(name="FILE_OBJECT")
	@Lob
	private byte[] file;

    @Column(name = "FILENAME")
    private String filename;

    @Column(name = "FILETYPE", length = 50)
    private String fileType;

    @Column(name = "CONTENT_TYPE", length = 50)
    private String contentType;

    @Column(name = "MODEL_ID", length = 50)
    private String modelId;

    @Column(name = "MODEL_TYPE", length = 50, nullable=false)
    private String modelType;



	public byte[] getFile() {
		return file;
	}
	public void setFile(byte[] file) {
		this.file = file;
	}
	public String getFilename() {
		return filename;
	}
	public void setFilename(String filename) {
		this.filename = filename;
	}
	public String getFileType() {
		return fileType;
	}
	private void setFileType(String contentType) {
		if ( StringUtils.isNotBlank( contentType ) ) {
			if ( StringUtils.equalsIgnoreCase( contentType, "image/jpeg" ) ) {
				this.fileType = FileType.IMAGE.name();
			}
			else if ( StringUtils.equalsIgnoreCase( contentType, "text/html" ) ) {
				this.fileType = FileType.HTML.name();
			}
		}
	}
	public String getContentType() {
		return contentType;
	}
	public void setContentType(String contentType) {
		this.contentType = contentType;
		setFileType( contentType );
	}
	public String getModelId() {
		return modelId;
	}
	public void setModelId(String modelId) {
		this.modelId = modelId;
	}
	public String getModelType() {
		return modelType;
	}
	public void setModelType(String modelType) {
		this.modelType = modelType;
	}

}
