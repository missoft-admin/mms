package com.transretail.crm.common.util;

/**
 * @author Allan G. Ramirez (agramirez@exist.com)
 */
public class Timer {
    private long startTime;
    private long endTime;
    private static final int HOUR_DIV = 60 * 60 * 1000;
    private static final int MIN_DIV = 60 * 1000;
    private static final int SEC_DIV = 1000;

    public Timer start() {
        startTime = System.currentTimeMillis();
        return this;
    }

    public void stop() {
        if (startTime > 0) {
            endTime = System.currentTimeMillis();
        }
    }

    public String elapseTime() {
        if (startTime == 0) {
            return "Timer not started";
        }
        long elapseTime = getEndTime() - startTime;
        StringBuilder builder = new StringBuilder(8);
        int hr = (int) elapseTime / HOUR_DIV;
        long remaining = elapseTime - (hr * HOUR_DIV);
        int min = (int) (remaining / MIN_DIV);
        remaining = remaining - (min * MIN_DIV);
        int sec = (int) (remaining / SEC_DIV);

        if (hr < 10) {
            builder.append("0");
        }
        builder.append(hr);
        builder.append(":");
        if (min < 10) {
            builder.append("0");
        }
        builder.append(min);
        builder.append(":");
        if (sec < 10) {
            builder.append("0");
        }
        builder.append(sec);
        return builder.toString();
    }

    private long getEndTime() {
        long endTime = this.endTime;
        if (endTime == 0) {
            endTime = System.currentTimeMillis();
        }
        return endTime;
    }

    public Timer reset() {
        startTime = 0;
        endTime = 0;
        return this;
    }
}
