package com.transretail.crm.core.dto;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.joda.time.DateTime;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.transretail.crm.common.web.converter.CurrencySerializer;
import com.transretail.crm.core.entity.EmployeePurchaseTxnModel;
import com.transretail.crm.core.entity.enums.TxnStatus;
import com.transretail.crm.core.entity.enums.VoucherTransactionType;

public class EmployeePurchaseTxnDto {
//	private static final String[] IGNORE = {"store", "storeModel", "memberModel"};
	
	private String id;
	private MemberDto memberModel;
    private String transactionNo;
    private Date transactionDateTime;
    private Double transactionAmount;
    private String store;
    private StoreDto storeModel;
    private VoucherTransactionType transactionType;
    private TxnStatus status;
    private String accountId;
    private String reason;
    private String remarks;
    private String approvedBy;
    private DateTime approvalDate;
    private DateTime created;
    private String createUser;
    private Long longTxnDate;
	private String paymentType;
	private String memberAccountId;
    
    public EmployeePurchaseTxnDto() {
    }
    
    public static List<EmployeePurchaseTxnDto> toDtoList( List<EmployeePurchaseTxnModel> models ) {
		if ( CollectionUtils.isEmpty( models ) ) {
			return new ArrayList<EmployeePurchaseTxnDto>();
		}

		List<EmployeePurchaseTxnDto> dtos = new ArrayList<EmployeePurchaseTxnDto>();
		for ( EmployeePurchaseTxnModel model : models ) {
			dtos.add( new EmployeePurchaseTxnDto( model ) );
		}
		return dtos;
	}
    
	public EmployeePurchaseTxnDto(EmployeePurchaseTxnModel employeePurchaseTxnModel) {
//		BeanUtils.copyProperties(employeePurchaseTxnModel, this, IGNORE);
        this.id = employeePurchaseTxnModel.getId();
        this.transactionNo = employeePurchaseTxnModel.getTransactionNo();
        this.transactionDateTime = employeePurchaseTxnModel.getTransactionDateTime();
        this.transactionAmount = employeePurchaseTxnModel.getTransactionAmount();
        if (employeePurchaseTxnModel.getStore() != null) {
            this.storeModel = new StoreDto(employeePurchaseTxnModel.getStore());
        }
        this.store = (this.storeModel != null) ? this.storeModel.getCode() + " - " + this.storeModel.getName() : "";
        this.transactionType = employeePurchaseTxnModel.getTransactionType();
        this.status = employeePurchaseTxnModel.getStatus();
        this.reason = employeePurchaseTxnModel.getReason();
        this.remarks = employeePurchaseTxnModel.getRemarks();
        this.approvedBy = employeePurchaseTxnModel.getApprovedBy();
        this.approvalDate = employeePurchaseTxnModel.getApprovalDate();
        this.created = employeePurchaseTxnModel.getCreated();
        this.createUser = employeePurchaseTxnModel.getCreateUser();
        //this.memberAccountId = employeePurchaseTxnModel.getMemberAccountId();

        if(employeePurchaseTxnModel.getMemberModel() != null) {
            this.memberModel = new MemberDto(employeePurchaseTxnModel.getMemberModel());
        }



		if ( null != employeePurchaseTxnModel.getTransactionDateTime() ) {
            this.longTxnDate = employeePurchaseTxnModel.getTransactionDateTime().getTime();
		}
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getTransactionNo() {
		return transactionNo;
	}
	public void setTransactionNo(String transactionNo) {
		this.transactionNo = transactionNo;
	}
	public Date getTransactionDateTime() {
		return transactionDateTime;
	}
	public void setTransactionDateTime(Date transactionDateTime) {
		this.transactionDateTime = transactionDateTime;
	}
	public Double getTransactionAmount() {
		return transactionAmount;
	}
	public void setTransactionAmount(Double transactionAmount) {
		this.transactionAmount = transactionAmount;
	}
	@JsonInclude(Include.ALWAYS)
	@JsonSerialize(using=CurrencySerializer.class)
	public Double getTransactionAmountFormatted() {
		return transactionAmount;
	}
	public String getStore() {
		return store;
	}
	public void setStore(String store) {
		this.store = store;
		this.storeModel = new StoreDto(store);
	}

	public StoreDto getStoreModel() {
		return storeModel;
	}

	public void setStoreModel(StoreDto storeModel) {
		this.storeModel = storeModel;
	}

	public VoucherTransactionType getTransactionType() {
		return transactionType;
	}

	public void setTransactionType(VoucherTransactionType transactionType) {
		this.transactionType = transactionType;
	}

	public TxnStatus getStatus() {
		return status;
	}

	public void setStatus(TxnStatus status) {
		this.status = status;
	}

	public String getAccountId() {
		if(memberModel != null && StringUtils.isNotEmpty(memberModel.getAccountId()))
			return memberModel.getAccountId();
		return accountId;
	}

	public void setAccountId(String employeeId) {
		if(StringUtils.isNotEmpty(employeeId)) {
			if(memberModel == null)
				memberModel = new MemberDto();
			memberModel.setAccountId(employeeId);
		}
		this.accountId = employeeId;
	}

	public String getReason() {
		return reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	public String getRemarks() {
		return remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	public MemberDto getMemberModel() {
		return memberModel;
	}

	public void setMemberModel(MemberDto memberDto) {
		this.memberModel = memberDto;
	}

	public String getApprovedBy() {
		return approvedBy;
	}

	public void setApprovedBy(String approvedBy) {
		this.approvedBy = approvedBy;
	}

	public DateTime getApprovalDate() {
		return approvalDate;
	}

	public void setApprovalDate(DateTime approvalDate) {
		this.approvalDate = approvalDate;
	}

	public DateTime getCreated() {
		return created;
	}

	public void setCreated(DateTime created) {
		this.created = created;
	}

	public String getCreateUser() {
		return createUser;
	}

	public void setCreateUser(String createUser) {
		this.createUser = createUser;
	}
    
    public Long getLongTxnDate() {
		return longTxnDate;
	}

	public String getPaymentType() {
		return paymentType;
	}

	public void setPaymentType(String paymentType) {
		this.paymentType = paymentType;
	}

	public String getMemberAccountId() {
		return memberAccountId;
	}

	public void setMemberAccountId(String memberAccountId) {
		this.memberAccountId = memberAccountId;
	}
}
