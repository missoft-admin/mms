package com.transretail.crm.giftcard.service;

/**
 * Internal used only.
 * <p/>
 * TODO: Reference No Format is not yet final - waiting for feedback.
 *
 * @author Monte Cillo Co (mco@exist.com)
 */
public interface GiftCardRefNoProvider {

    String getReferenceNo(String merchantId, String terminalId, String cashierId);
}
