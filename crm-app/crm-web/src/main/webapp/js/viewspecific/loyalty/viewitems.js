$(document).ready(function() {
	$inventoryList = $("#list_inventoryItems");
	$inventoryParams = $("#inventoryParams");
	
	initUserDataTable();

	function initUserDataTable() {

		var headers = { 
			product: $inventoryList.data("hdrProduct"),
			barcode: $inventoryList.data("hdrBarcode"),
			series: $inventoryList.data("hdrSeries"),
		};
		
		$inventoryList.ajaxDataTable({
			'autoload' 	: false,
			'ajaxSource' : $inventoryList.data("url"),
			'columnHeaders' : [	
			    headers['series'],
		        headers['barcode']
			],
			'modelFields' : [
			 {name : 'sequence', sortable : true},
			 {name : 'barcode', sortable : false}
			 ]
		});
		
		var formDataObj = $inventoryParams.toObject( { mode : 'first', skipEmpty : true } );
		
		$inventoryList.ajaxDataTable( 
			'search', 
			formDataObj, 
			function() {}
		);
		
	}
});