    create table CRM_COUNTER (
        KEY varchar2(255 char) not null,
        VALUE number(19,0),
        VERSION number(10,0),
        primary key (KEY)
    );
