<%@include file="../../common/taglibs.jsp"%>
<script src="<spring:url value="/js/common/typeaheadutil.js" />"></script>
<div id="content_cxProfileForm" class="modal hide nofly modal-dialog">
  <div class="modal-content">
      <c:url var="url_action" value="${cxProfileAction}" />
      <%-- ================================== Form for create costomer profile =================================== --%>
      <form:form id="cxProfileForm" name="cxProfile" modelAttribute="cxProfile" method="POST" action="${url_action}" class="modal-form form-horizontal">

          <%-- ===== for close error message in header ===== --%>
          <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
              <h4><c:out value="${cxProfileHeader}" /></h4>
          </div>
          <%-- ===== end for close error message in header ===== --%>

          <div class="modal-body">
              <div class="hide alert alert-error" id="content_error">
                  <button type="button" class="close" onclick="$(this).parent().hide();">&times;</button>
                  <%-- ===== to render all errors ===== --%>
                  <div>
                      <form:errors path="*"/>
                  </div>
                      <%-- ===== end to render all errors ===== --%>
              </div>
              <div class="main-block">
                  <div class="control-group">
                       <spring:message code="sales_order_payment_store" var="paymentStore" />
                       <label for="paymentStore" class="control-label"><b class="required">*</b>${paymentStore}</label>
                       <div class="controls">
                         <div>
                             <form:input path="paymentStoreName" id="paymentStoreName" cssClass="form-control input-medium" />
                             <form:hidden path="paymentStore" id="paymentStore"/>
                         </div>
                       </div>
                   </div>
                  <div class="control-group">
                      <label for="name" class="control-label">
                          <b class="required">*</b>
                          <spring:message code="gc.cxprofile.cx.name" />
                      </label>
                      <div class="controls"><form:input id="name" path="name"/></div>
                  </div>
                  <div class="control-group">
                      <label for="invoiceTitle" class="control-label">
                          <b class="required">*</b>
                          <spring:message code="gc.cxprofile.invoice.title" />
                      </label>
                      <div class="controls"><form:input id="invoiceTitle" path="invoiceTitle"/></div>
                  </div>
                  <div class="control-group">
                      <label for="customerType" class="control-label">
                          <b class="required">*</b>
                          <spring:message code="gc.cxprofile.cx.type" /></label>
                      <div class="controls">
                              <%-- <form:input id="customerType" path="customerType"/> --%>
                          <form:select path="customerType" cssClass="">
                              <option/>
                              <form:options items="${customerTypes}"  />
                          </form:select>
                      </div>
                  </div>
                  <div>
                      <div class="control-group">
                        <label class="control-label">
                            <spring:message code="gc.cxprofile.contact.person" />
                        </label>
                        <div class="controls">
                          <fieldset class="row-fluid">
                              <label>
                                  <spring:message code="gc.cxprofile.contact.fname" />
                              </label>
                              <form:input id="contactFirstName" path="contactFirstName" />
                          </fieldset>
                          <fieldset class="row-fluid">
                              <label>
                                  <spring:message code="gc.cxprofile.contact.lname" />
                              </label>
                              <form:input id="contactLastName" path="contactLastName" />
                          </fieldset>
                        </div>
                      </div>
                      <div class="control-group">
                        <label for="gender" class="control-label"><spring:message code="gc.cxprofile.gender" /></label>
                        <div class="controls">
                          <%-- <form:input id="gender" path="gender"/> --%>
                            <form:select path="gender" cssClass="">
                              <option/>
                              <form:options items="${genders}" itemValue="code" itemLabel="description" />
                            </form:select>
                        </div>
                      </div>
                      <div class="control-group">
                        <label for="contactNo" class="control-label"><spring:message code="gc.cxprofile.mobile.no" /></label>
                        <div class="controls"><form:input id="contactNo" path="contactNo"/></div>
                      </div>
                      <div class="control-group">
                        <label for="faxNo" class="control-label"><spring:message code="gc.cxprofile.fax.no" /></label>
                        <div class="controls"><form:input id="faxNo" path="faxNo"/></div>
                      </div>
                      <div class="control-group">
                        <label for="email" class="control-label"><spring:message code="gc.cxprofile.email" /></label>
                        <div class="controls"><form:input id="email" path="email"/></div>
                      </div>
                      <div class="control-group">
                        <label for="companyPhoneNo" class="control-label"><spring:message code="gc.cxprofile.coy.phone.no" /></label>
                        <div class="controls"><form:input id="companyPhoneNo" path="companyPhoneNo"/></div>
                      </div>
                      <div class="control-group">
                        <label for="mailAddress" class="control-label"><spring:message code="gc.cxprofile.mail.address" /></label>
                        <div class="controls"><form:input id="mailAddress" path="mailAddress"/></div>
                      </div>
                      <div class="control-group">
                        <label for="shippingAddress" class="control-label"><spring:message code="gc.cxprofile.shipping.address" /></label>
                        <div class="controls"><form:input id="shippingAddress" path="shippingAddress"/></div>
                      </div>
                  </div>

                    <%-- <div class="control-group">
                      <label for="discountType" class="control-label"><spring:message code="gc.cxprofile.discount.type" /></label>
                      <div class="controls">
                        <form:select path="discountType" cssClass="">
                          <option/>
                          <form:options items="${discountTypes}" itemValue="code" itemLabel="description" />
                        </form:select>
                        <form:input id="discountType" path="discountType"/>
                      </div>
                    </div> --%>
                  <div class="control-group">
                      <%--<label for="lastOrderNo" class="control-label"><spring:message code="gc.cxprofile.last.order.no" /></label>--%>
                      <div class="controls"><form:label  id="lastOrderNo" path="lastOrderNo" /></div>
                    </div>
                  <div class="control-group">
                      <%--<label for="lastOrderDate" class="control-label"><spring:message code="gc.cxprofile.last.order.date" /></label>--%>
                      <div class="controls">
                        <form:label id="lastOrderDate" path="lastOrderDate" />
                        <form:hidden path="lastOrderDateTime" id="lastOrderDateTime" />
                      </div>
                  </div>
              </div>
          </div>


        <div class="modal-footer">
          <button id="saveBtn" class="btn btn-primary" type="submit"><spring:message code="label_save" /></button>
          <button  id="cancelBtn" class="btn" type="button" data-dismiss="modal"><spring:message code="label_cancel" /></button>
        </div>

      </form:form>
  </div>


<style type="text/css">
<!--
  .modal-footer { text-align: center; }
  .control-group .controls label { margin-top: 5px; }
-->
</style>
<link href="<c:url value="/css/bootstrap/datepicker.css" />" rel="stylesheet" />
<script src='<c:url value="/js/bootstrap/bootstrap-datepicker.js"/>' type='text/javascript'><![CDATA[&nbsp; ]]></script>
<script type='text/javascript'>
var CxProfileForm = null;

$(document).ready( function() {

    var errorContainer = $( "#content_cxProfileForm" ).find( "#content_error" );
    var $dialog = $( "#content_cxProfileForm" );
    var $form = $dialog.find( "#cxProfileForm" );
    var $save = $form.find( "#saveBtn" );
    var $lastOrderDate = $form.find( "#lastOrderDate" );
    var $lastOrderDateTime = $form.find( "#lastOrderDateTime" );

    initDialog();
    initFormFields();

    function initDialog() {
        $dialog.modal({ show : false });
        $dialog.on( "hide", function(e) {
        	  if ( e.target === this ) {
        		    errorContainer.hide();
            }
        });
    }

    function initFormFields() {
    	  $form.submit( function(e) {
    		    e.preventDefault();
						//if ( $lastOrderDate.val() ) {
						//    $lastOrderDateTime.val( $lastOrderDate.datepicker( "getDate" ).getTime() );
						//}
            $save.prop( "disabled", true );
    		    $.post( $(this).attr( "action" ), $form.serialize(), function( resp ) {
		            CxProfile.processResp( resp, $dialog, errorContainer );
		            CxProfileList.reloadTable();
		            $save.prop( "disabled", false );
    		    }, "json" );
    	  });
    	  //$lastOrderDate.datepicker({ autoclose : true, format : "dd M yyyy" });
        if ( $lastOrderDateTime.val() ) {
        	  //$lastOrderDate.datepicker( "setDate", new Date( $lastOrderDateTime.val() - 0 ) );
        	  $lastOrderDate.html( new Date( $lastOrderDateTime.val() - 0 ).customize(0) );
        }
    }

    CxProfileForm = {
    		show  : function() {
    			  $dialog.modal( "show" );
    		}
    };

    var url = '<c:url value="/store/peoplesoft/list/gcallowed/" />';
    var $paymentStore=$( "#paymentStore" );
    if("${edit}"){
    	if($paymentStore.val()==""){
    		TypeAhead.process( $( "#paymentStoreName" ), $paymentStore, url, { label: "codeAndName", value: "code" });
    	}else{
    		console.log("get");
	    	$.get(url+$paymentStore.val(),function(data){
	    		$( "#paymentStoreName" ).val(data[0].codeAndName);
	    		$( "#paymentStoreName" ).attr("readOnly",true);
	    	});
    	}
    }else{
	    TypeAhead.process( $( "#paymentStoreName" ), $paymentStore, url, { label: "codeAndName", value: "code" });
    }

});
</script>

</div>