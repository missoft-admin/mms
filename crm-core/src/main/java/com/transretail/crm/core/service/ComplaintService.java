package com.transretail.crm.core.service;


import java.util.List;

import com.transretail.crm.common.service.dto.response.CodeDescDto;
import com.transretail.crm.common.service.dto.response.ResultList;
import com.transretail.crm.core.dto.ApprovalRemarkDto;
import com.transretail.crm.core.dto.ComplaintDto;
import com.transretail.crm.core.dto.ComplaintSearchDto;
import com.transretail.crm.core.dto.MessageDto;
import com.transretail.crm.core.entity.Complaint.ComplaintMemberField;


public interface ComplaintService {

	ResultList<ComplaintDto> search(ComplaintSearchDto dto);

	ComplaintDto getDto(Long id);

	ComplaintDto saveDto(ComplaintDto dto, boolean isNew);

	ComplaintDto updateDto(ComplaintDto dto);

	ComplaintDto assignDto(ComplaintDto dto);

	ComplaintDto populateMember(String fieldVal, ComplaintMemberField field);

	ResultList<ApprovalRemarkDto> getHistoryDtos(Long id);

	ComplaintDto saveRemarks(ComplaintDto dto);

	ComplaintDto processReply(Long id, MessageDto msgDto);

	List<CodeDescDto> getStats(ComplaintDto dto);

}
