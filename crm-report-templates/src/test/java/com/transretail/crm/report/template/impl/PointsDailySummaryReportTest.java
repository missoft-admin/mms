package com.transretail.crm.report.template.impl;

import com.google.common.collect.ImmutableMap;
import com.transretail.crm.common.reporting.exception.ReportException;
import com.transretail.crm.common.reporting.jasper.JRProcessor;
import com.transretail.crm.report.template.ReportTemplate;
import java.io.File;
import java.util.*;
import javax.annotation.Resource;
import net.sf.jasperreports.engine.JRParameter;
import org.apache.commons.io.FileUtils;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.context.support.MessageSourceResourceBundle;

import static org.junit.Assert.assertFalse;

/**
 *
 * @author Monte Cillo Co (mco@exist.com)
 */
public class PointsDailySummaryReportTest extends AbstractPointSummaryReportTest {

    private static final Logger logger = LoggerFactory.getLogger(PointsDailySummaryReportTest.class);
    @Resource(name = "dailyPointsSummaryReport")
    private ReportTemplate dailyPointsSummaryReport;

    @Override
    @Test
    public void testCreateJrProcessor() throws ReportException {
	final Map<String, String> param = new ImmutableMap.Builder<String, String>()
		.put(PointsSummaryReport.Daily.FILTER_DATE_FROM, "05-03-2014")
		.put(PointsSummaryReport.Daily.FILTER_DATE_TO, "30-03-2014")
		.build();

	JRProcessor jrProcessor = dailyPointsSummaryReport.createJrProcessor(param);
	File testClassesDir = FileUtils.toFile(Thread.currentThread().getContextClassLoader().getResource(""));
	File output = new File(testClassesDir, "test-points-summary-daily-report-output.pdf");
	logger.debug("Output location {}", output.getAbsolutePath());
	output.delete();
	Locale locale = LocaleContextHolder.getLocale();
	jrProcessor.addParameter(JRParameter.REPORT_LOCALE, locale);
	jrProcessor.addParameter(JRParameter.REPORT_RESOURCE_BUNDLE, new MessageSourceResourceBundle(messageSourceRef, locale));
	reportService.exportPdfReport(jrProcessor, output);

	double bytes = output.length();
	double kilobytes = (bytes / 1024);
	logger.debug("Kilobytes: {}", kilobytes);
    }

    @Test
    public void getCollectionDatasourceTest() {
	PointsSummaryReport psr = (PointsSummaryReport) dailyPointsSummaryReport;

	Collection<?> ds = psr.getCollectionDatasource(new ImmutableMap.Builder<String, Object>()
		.put(PointsSummaryReport.Daily.REPORT_PARAM_DATE_FROM, "01-03-2014")
		.put(PointsSummaryReport.Daily.REPORT_PARAM_DATE_TO, "30-03-2014")
		.build());

	assertFalse(ds.isEmpty()); // TODO: testing og
    }

}
