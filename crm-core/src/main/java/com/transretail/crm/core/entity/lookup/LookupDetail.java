package com.transretail.crm.core.entity.lookup;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.validation.constraints.NotNull;

import org.hibernate.envers.Audited;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.transretail.crm.core.entity.enums.Status;
import com.transretail.crm.core.entity.support.AuditableEntity;

/**
 *
 */
@Audited
@Entity
@Table(name = "CRM_REF_LOOKUP_DTL")
public class LookupDetail extends AuditableEntity implements Serializable {
    private static final long serialVersionUID = 8113907966967399078L;

    @Id
    @NotNull
    @Column(name = "CODE", length = 15)
    private String code;
    @NotNull
    @Column(name = "DESCRIPTION", length = 100, nullable = false)
    private String description;
    @NotNull
    @JsonIgnore
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "HDR", nullable = false)
    private LookupHeader header;
    @NotNull
    @Column(name = "STATUS", length = 8, nullable = false)
    @Enumerated(EnumType.STRING)
    private Status status = Status.ACTIVE;

    @Transient
    private String oldCode;

    public LookupDetail() {}

    public LookupDetail(String code) {
        this.code = code;
    }

    public LookupDetail(String code, String description) {
        this(code, description, null, null);
    }

    public LookupDetail(String code, String description, LookupHeader header, Status status) {
        this.code = code;
        this.description = description;
        this.header = header;
        this.status = status;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public LookupHeader getHeader() {
        return header;
    }

    public void setHeader(LookupHeader header) {
        this.header = header;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public String getOldCode() {
        return oldCode;
    }

    public void setOldCode(String oldCode) {
        this.oldCode = oldCode;
    }
    
    public String getCodeAndDesc() {
        return code + " - " + description;
    }
}
