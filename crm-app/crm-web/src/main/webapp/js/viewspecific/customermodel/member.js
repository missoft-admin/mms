$(document).ready(function() {
	$list = $("#list_member");
	initDataTable();
	initButtons();

	prepaidCardModal = $("#prepaidCardModal");
	prepaidCardModal.modal({
    show: false
  }).on('hidden.bs.modal', function() {
    $(this).empty();
  });
  
	function initDataTable() {
		  
		  var headers = { 
		    username: $list.data("hdrUsername"),
		    member: $list.data("hdrMember"),
		    store: $list.data("hdrStore"),
		    accountNo: $list.data("hdrAccountno"),
		    status: $list.data("hdrStatus"),
		  };
		  
		  $list.ajaxDataTable({
		    'autoload'  : true,
		    'ajaxSource' : $list.data("url"),
		    'columnHeaders' : [ 
		         headers['username'],
		         headers['accountNo'],
		         headers['member'],
		         headers['store'],
		         headers['status'],
		         ""
		    ],
		    'modelFields' : [
		     {name : 'username', sortable : true},
		     {name : 'accountId', sortable : true},
	         {name : 'firstName', sortable : true, fieldCellRenderer : function (data, type, row) {
		           return row.formattedMemberName;
	         }},
	         {name : 'registeredStore', sortable : true, fieldCellRenderer : function (data, type, row) {
	        	 if(row.registeredStore != null && row.registeredStore != "")
	        		 return row.registeredStore + " - " + row.registeredStoreName;
	        	 else
	        		 return "";
	         }},
		     {name : 'accountStatus', sortable : true},
		     {customCell : function ( innerData, sSpecific, json ) {
		         if ( sSpecific == 'display' ) {
		        	 var showBtns = json.accountStatus == 'ACTIVE' || ( json.accountStatus == 'ACTIVE' && json.parentStatus && json.parentStatus == 'ACTIVE'  );
		        	 var btnHtml = "";
		        	 if ( showBtns ) {
			        	 btnHtml += $("#mainBtn").html();
		        	 }

		        	 if ( $( "#isNotHelpDesk" ).data( "grant" ) ) {
			        	 deleteBtn = $('#deleteBtn-list').html();
			        	 
			        	 $cloneHtml = $("#otherBtn").clone(true);
			        	 if(json.parent != null && json.parent != "") {
			        		 $cloneHtml.find("#supplementaryBtn").remove();
			        	 }

			        	 if ( showBtns ) {
			        		 btnHtml += $cloneHtml.html();
			        	 }

			        	 if ( !json.parentStatus || ( json.parentStatus && json.parentStatus == 'ACTIVE' ) ) {
							 btnHtml += ( json.accountStatus == 'ACTIVE'? 
									 $( "#btn_deactivateMember" ).html().replace( new RegExp("%ID%", 'g'), json.id ).replace( new RegExp("%ACTIVATE%", 'g'), false )
									 : $( "#btn_activateMember" ).html().replace( new RegExp("%ID%", 'g'), json.id ).replace( new RegExp("%ACTIVATE%", 'g'), true ) );
			        	 }

						 //btnHtml += deleteBtn;
		        	 }

					 btnHtml = btnHtml.replace( new RegExp("%ACCOUNTID%", 'g'), json.accountId );
					 btnHtml = btnHtml.replace( new RegExp("%ID%", 'g'), json.id );

					 return btnHtml;
		           } else {
		           	 return '';
		           }
		           }
		      }
		     ]
		  })
		  .on( "click", ".btn_activateMember", activateMember )
		  .on("click", ".prepaidCardBtn", showModal);
		  
		  initSearchForm($list);
		  
		}
	
	function showModal(e) {
    e.preventDefault();
    $.get($(this).data("url"), function(data) {
      prepaidCardModal.modal('show').html(data);
      initPrepaidCards();
      prepaidCardModal.on('hidden.bs.modal', function() {
        prepaidCardModal.empty();
      });
    }, "html");
    
  }
	
	function initPrepaidCards() {
	//for cards list
    var cardList = $("#list_cards");
    var headers = {
        cardNo: cardList.data("hdrCardno"),
        prodDesc: cardList.data("hdrProddesc"),
        prevBal: cardList.data("hdrPrevbal"),
        currBal: cardList.data("hdrCurrbal"),
        dateLinked: cardList.data("hdrDatelinked")
    };

    var columnHeaders=[
        headers['cardNo'],
        headers['prodDesc'],
        headers['prevBal'],
        headers['currBal'],
        headers['dateLinked']/*,
        ''*/
    ];

    var modelFields= [
        {name : 'cardNo', sortable: true},
        {name : 'description'},
        {name : 'previousBalance', fieldCellRenderer : function(data, type, row) {
            return commaSeparateNumber(row.previousBalance);
        }},
        {name : 'currentBalance', fieldCellRenderer : function(data, type, row) {
            return commaSeparateNumber(row.currentBalance);
        }},
        {name : 'registrationDate', fieldCellRenderer: function(data, type, row) {
          return row.regDateString;
        }}/*,
        {customCell: function(innerData, sSpecific, json) {return '';}}*/
    ];
    
    cardList.ajaxDataTable({
      'autoload' : true,
      'ajaxSource' : cardList.data("url"),
      'columnHeaders' : columnHeaders,
      'modelFields' : modelFields
   });
	}
	
	function commaSeparateNumber(val) {
    while (/(\d+)(\d{3})/.test(val.toString())){
        val = val.toString().replace(/(\d+)(\d{3})/, '$1'+','+'$2');
    }
    return val;
	}
	function activate(url) {
		
	}

	function activateMember(e) {
		e.preventDefault();
		var url = $(this).data( "url" );
		
		var activateFn = function(result) {
			if(result) {
				$.get( url, function(resp) { 
					$("#search_submit").click();
					//$list.ajaxDataTable( 'search', $("#searchModel").toObject({ mode : 'first', skipEmpty : true }) ); 
				});	
			}
				
		};
		if($(this).hasClass("icn-delete")) {
			getConfirm($("#list_member").data("deactivateConfirm"), activateFn);
		} else {
			activateFn(true);
		}
		
	}
	
	function initSearchForm(dataTable) {
		$("#search_submit").click(function(e) {
			e.preventDefault();
			
			$("#search_memberField").data("name", $("#search_memberCriteria").val());
			
			$(".memberSearchField").each(function() {
				$(this).attr("name", $(this).data("name"));
			});
			
			var formDataObj = $("#searchModel").toObject( { mode : 'first', skipEmpty : true } );
			$btnSearch = $(this);
			$btnSearch.prop('disabled', true);
			
			console.log(formDataObj);
			
			dataTable.ajaxDataTable( 
				'search', 
				formDataObj, 
				function() {
					$btnSearch.prop( 'disabled', false );
			});
			
			$(".memberSearchField").removeAttr("name");
		});
	}
	
	$(".pointsSearchDropdown").change(function() {
		$("#search_submit").click();
	});
	
	$("#pointsSearchClearButton").click(function(e) {
		e.preventDefault();
		$("#searchModel #search_memberCriteria option:first").attr('selected','selected');
		$("#searchModel #search_memberField").val("");
		$("#searchModel .pointsSearchDropdown").val("");
		$("#searchModel #search_memberField").removeAttr("name");
		$(".memberSearchField").removeAttr("name");
		$("#search_submit").click();
	});
	
	function initButtons(){
	  var CONTENT_ERROR = $("#contentError");
	  
    $("#printList").click(function(e){
      e.preventDefault();
      $("#search_memberField").data("name", $("#search_memberCriteria").val());
      
      $(".memberSearchField").each(function() {
        $(this).attr("name", $(this).data("name"));
      });
      var dataObject = $("#searchModel").serialize();
      $.get($("#printList").data("url"),dataObject , function(response) {
        console.log(response);
        if (response.success) {
          var url = ctx + "/customermodels/printList";
          $.get(url,dataObject , function(response) {
            $("#form_export").modal('show').html(response);
            $("#form_export").on('hidden.bs.modal', function() {
              $("#form_export").empty();
            });
          }, "html");
        } else {
          var theErrorInfo = response.result + "<br>";
          CONTENT_ERROR.find( "div" ).html( theErrorInfo );
          CONTENT_ERROR.show( "slow" );
        }
      }, "json");
    });
  }
	
});