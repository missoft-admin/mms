package com.transretail.crm.report.template.impl;

import com.transretail.crm.core.entity.*;
import com.transretail.crm.core.repo.*;

import com.mysema.query.jpa.impl.JPAQuery;
import com.transretail.crm.common.reporting.exception.ReportException;
import com.transretail.crm.common.reporting.service.ReportService;
import com.transretail.crm.core.entity.embeddable.Address;
import com.transretail.crm.core.entity.embeddable.CustomerProfile;
import com.transretail.crm.core.entity.embeddable.Duration;
import com.transretail.crm.core.entity.embeddable.ProfessionalProfile;
import com.transretail.crm.core.entity.enums.MemberCreatedFromType;
import com.transretail.crm.core.entity.enums.PointTxnType;
import com.transretail.crm.core.entity.enums.ProgramType;
import com.transretail.crm.core.entity.lookup.LookupDetail;
import com.transretail.crm.core.entity.lookup.QLookupDetail;
import com.transretail.crm.core.service.CodePropertiesService;
import java.io.InputStream;
import java.util.Date;
import java.util.Scanner;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.joda.time.LocalDate;
import org.joda.time.LocalDateTime;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.TransactionCallback;
import org.springframework.transaction.support.TransactionTemplate;

import static com.transretail.crm.core.entity.enums.AppKey.POINTS_VALUE;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.nullValue;
import static org.hamcrest.core.IsNot.not;
import static org.junit.Assert.assertThat;

/**
 * Base test class for PointSummaryReports. This class includes only the setup
 * and cleanup of the test data.
 *
 * @author Monte Cillo Co (mco@exist.com)
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath*:/META-INF/spring/applicationContext*.xml",
    "/META-INF/spring/applicationContext-test.xml"})
public abstract class AbstractPointSummaryReportTest {

    @PersistenceContext
    protected EntityManager em;
    @Autowired
    private PlatformTransactionManager transactionManager;
    @Autowired
    private LookupHeaderRepo lookupHeaderRepo;
    @Autowired
    private LookupDetailRepo lookupDetailRepo;
    @Autowired
    private PromotionRepo promotionRepo;
    @Autowired
    private MemberRepo memberRepo;
    @Autowired
    private TransactionAuditRepo taRepo;
    @Autowired
    private PointsTxnModelRepo pointsTxnModelRepo;
    @Autowired
    private ProgramRepo programRepo;
    @Autowired
    private CampaignRepo campaignRepo;
    @Autowired
    private CodePropertiesService codePropertiesService;
    @Autowired
    protected ReportService reportService;
    @Autowired
    protected MessageSource messageSourceRef;
    @Autowired
    private ApplicationConfigRepo appConfigRepo;

    @Before
    public void onSetup() {
	setupAppConfig();
	setUpLookupDetails();
	setupPromotion();
	setupMember();
	insertPointsTransaction();
    }

    @After
    public void tearDown() {
	appConfigRepo.deleteAll();
	pointsTxnModelRepo.deleteAll();
	taRepo.deleteAll();
	promotionRepo.deleteAll();
	campaignRepo.deleteAll();
	programRepo.deleteAll();

	memberRepo.deleteAll();
	lookupDetailRepo.deleteAll();
	lookupHeaderRepo.deleteAll();
    }

    @Test
    public abstract void testCreateJrProcessor() throws ReportException;

    //<editor-fold defaultstate="collapsed" desc="Helper Methods Region">
    private void insertPointsTransaction() {
	QMemberModel qm = QMemberModel.memberModel;
	MemberModel member = memberRepo.findOne(qm.accountId.eq("accountId"));

	QPromotion qpm = QPromotion.promotion;
	Promotion promo = promotionRepo.findOne(qpm.name.eq("test-promo-point"));
	assertThat(promo, is(not(nullValue())));
	assertThat(promo.getId(), is(not(nullValue())));

	PointsTxnModel p2013 = new PointsTxnModel();
	p2013.setId("pa-id-02013");
	p2013.setTransactionAmount(1980000.0);
	p2013.setTransactionDateTime(new LocalDateTime(2013, 01, 20, 14, 45).toDate());
	p2013.setTransactionNo("TXN_TEST_10010020013");
	p2013.setTransactionPoints(190.0);
	p2013.setTransactionType(PointTxnType.REWARD);
	pointsTxnModelRepo.save(p2013);

	TransactionAudit tap2013 = new TransactionAudit();
	tap2013.setId("ta-id-p2013");
	tap2013.setMemberModel(member);
	tap2013.setTransactionNo("TXN_TEST_10010020013");
	tap2013.setTransactionPoints(190l);
	tap2013.setPromotion(promo);
	taRepo.save(tap2013);

	// setup late transaction date that March 2014
	PointsTxnModel p1 = new PointsTxnModel();
	p1.setId("pa-id-0100");
	p1.setTransactionAmount(16000.0);
	p1.setTransactionDateTime(new LocalDateTime(2014, 01, 20, 14, 45).toDate());
	p1.setTransactionNo("TXN_TEST_100100");
	p1.setTransactionPoints(16.0);
	p1.setTransactionType(PointTxnType.EARN);
	pointsTxnModelRepo.save(p1);

	TransactionAudit tap1 = new TransactionAudit();
	tap1.setId("ta-id-p1");
	tap1.setMemberModel(member);
	tap1.setTransactionNo("TXN_TEST_100100");
	tap1.setTransactionPoints(6l);
	tap1.setPromotion(promo);
	taRepo.save(tap1);

	PointsTxnModel p2 = new PointsTxnModel();
	p2.setId("pa-id-0200");
	p2.setTransactionAmount(76897.0);
	p2.setTransactionDateTime(new LocalDateTime(2014, 03, 05, 14, 45).toDate());
	p2.setTransactionNo("TXN_TEST_100200");
	p2.setTransactionPoints(76.0);
	p2.setTransactionType(PointTxnType.EARN);
	pointsTxnModelRepo.save(p2);

	TransactionAudit tap2 = new TransactionAudit();
	tap2.setId("ta-id-p2");
	tap2.setMemberModel(member);
	tap2.setTransactionNo("TXN_TEST_100200");
	tap2.setTransactionPoints(76l);
	tap2.setPromotion(promo);
	taRepo.save(tap2);

	// setup POINTS
	PointsTxnModel pEarnPoint = new PointsTxnModel();
	pEarnPoint.setId("pa-id-01");
	pEarnPoint.setTransactionAmount(6000.0);
	pEarnPoint.setTransactionDateTime(new LocalDateTime(2014, 03, 06, 14, 45).toDate());
	pEarnPoint.setTransactionNo("TXN_TEST_1001");
	pEarnPoint.setTransactionPoints(6.0);
	pEarnPoint.setTransactionType(PointTxnType.EARN);
	pointsTxnModelRepo.save(pEarnPoint);

	TransactionAudit ta = new TransactionAudit();
	ta.setId("ta-id");
	ta.setMemberModel(member);
	ta.setTransactionNo("TXN_TEST_1001");
	ta.setTransactionPoints(6l);
	ta.setPromotion(promo);
	taRepo.save(ta);

	// setup ITEM_POINTS
	PointsTxnModel pEarnItemPoint = new PointsTxnModel();
	pEarnItemPoint.setId("pa-id-02");
	pEarnItemPoint.setTransactionAmount(6000.0);
	pEarnItemPoint.setTransactionDateTime(new LocalDateTime(2014, 03, 07, 14, 45).toDate());
	pEarnItemPoint.setTransactionNo("TXN_TEST_1002");
	pEarnItemPoint.setTransactionPoints(6.0);
	pEarnItemPoint.setTransactionType(PointTxnType.EARN);
	pointsTxnModelRepo.save(pEarnItemPoint);

	Promotion promoItem = promotionRepo.findOne(qpm.name.eq("test-promo-item_point"));
	assertThat(promoItem, is(not(nullValue())));
	assertThat(promoItem.getId(), is(not(nullValue())));

	TransactionAudit ta2 = new TransactionAudit();
	ta2.setId("ta-id2");
	ta2.setMemberModel(member);
	ta2.setTransactionNo("TXN_TEST_1002");
	ta2.setTransactionPoints(6l);
	ta2.setPromotion(promoItem);
	taRepo.save(ta2);

	PointsTxnModel redeem = new PointsTxnModel();
	redeem.setId("pa-id-01-redeem");
	redeem.setTransactionAmount(6000.0);
	redeem.setTransactionDateTime(new LocalDateTime(2014, 03, 06, 14, 45).toDate());
	redeem.setTransactionNo("TXN_TEST_1003");
	redeem.setTransactionPoints(-6.0);
	redeem.setTransactionType(PointTxnType.REDEEM);
	pointsTxnModelRepo.save(redeem);

	PointsTxnModel pRewards = new PointsTxnModel();
	pRewards.setId("pa-id-03");
	pRewards.setTransactionAmount(5000.0);
	pRewards.setTransactionDateTime(new LocalDateTime(2014, 03, 06, 14, 45).toDate());
	pRewards.setTransactionNo("TXN_TEST_1004");
	pRewards.setTransactionPoints(5.0);
	pRewards.setTransactionType(PointTxnType.REWARD);
	pointsTxnModelRepo.save(pRewards);

	PointsTxnModel voidPoints = new PointsTxnModel();
	voidPoints.setId("pa-id-04");
	voidPoints.setTransactionAmount(5000.0);
	voidPoints.setTransactionDateTime(new LocalDateTime(2014, 03, 06, 14, 45).toDate());
	voidPoints.setTransactionNo("TXN_TEST_1005");
	voidPoints.setTransactionPoints(-5.0);
	voidPoints.setTransactionType(PointTxnType.VOID);
	pointsTxnModelRepo.save(voidPoints);

	PointsTxnModel returnPoints = new PointsTxnModel();
	returnPoints.setId("pa-id-06");
	returnPoints.setTransactionAmount(5000.0);
	returnPoints.setTransactionDateTime(new LocalDateTime(2014, 03, 06, 14, 45).toDate());
	returnPoints.setTransactionNo("TXN_TEST_1006");
	returnPoints.setTransactionPoints(-5.0);
	returnPoints.setTransactionType(PointTxnType.RETURN);
	pointsTxnModelRepo.save(returnPoints);

	PointsTxnModel adjustPoints = new PointsTxnModel();
	adjustPoints.setId("pa-id-07");
	adjustPoints.setTransactionAmount(8700.0);
	adjustPoints.setTransactionDateTime(new LocalDateTime(2014, 03, 06, 14, 45).toDate());
	adjustPoints.setTransactionNo("TXN_TEST_1007");
	adjustPoints.setTransactionPoints(8.0);
	adjustPoints.setTransactionType(PointTxnType.ADJUST);
	pointsTxnModelRepo.save(adjustPoints);

    }

    private void setupMember() {
	MemberModel model = new MemberModel();
	model.setUsername("username");
	model.setAccountId("accountId");
	model.setFirstName("firstName");
	model.setLastName("lastName");
	model.setContact("contact");
	model.setPin("CRMMMS");
	model.setEnabled(true);
	model.setRegisteredStore("preferredStore1");
	model.setStoreName("STORE1");
	CustomerProfile customerProfile = new CustomerProfile();
	ProfessionalProfile professionalProfile = new ProfessionalProfile();
	Address address = new Address();
	address.setCity("aCity");
	address.setStreet("aStreet");
	address.setPostCode("12345");

	model.setMemberType(new LookupDetail(codePropertiesService.getDetailMemberTypeEmployee()));
	model.setMemberCreatedFromType(MemberCreatedFromType.FROM_UPLOAD);
	customerProfile.setGender(new LookupDetail(codePropertiesService.getDetailGenderFemale()));

	String departmentCode = getCode(codePropertiesService.getHeaderBusinessFields(), "Other");
	professionalProfile.setDepartment(new LookupDetail(departmentCode));
	customerProfile.setBirthdate(new Date());

	String empTypeCode = getCode(codePropertiesService.getHeaderEmpType(), "PROBATIONARY ASSOCIATE");
	model.setEmpType(new LookupDetail(empTypeCode));

	String genderCode = getCode(codePropertiesService.getHeaderGender(), "Female");
	customerProfile.setGender(new LookupDetail(genderCode));

	String religionCode = getCode(codePropertiesService.getHeaderReligion(), "Islam");
	customerProfile.setReligion(new LookupDetail(religionCode));

	String nationalityCode = getCode(codePropertiesService.getHeaderNationality(), "INDONESIAN");
	customerProfile.setNationality(new LookupDetail(nationalityCode));

	customerProfile.setPlaceOfBirth("birthplace");
	model.setContact("0856309991");

	model.setProfessionalProfile(professionalProfile);
	model.setCustomerProfile(customerProfile);
	model.setAddress(address);

	memberRepo.save(model);
    }

    protected String getCode(String headerCode, String description) {
	QLookupDetail qLookupDetail = QLookupDetail.lookupDetail;
	String code = new JPAQuery(em).from(qLookupDetail).where(
		qLookupDetail.header.code.eq(headerCode)
		.and(qLookupDetail.description.toUpperCase().eq(description.toUpperCase())))
		.uniqueResult(qLookupDetail.code);
	return code;
    }

    private void setUpLookupDetails() {
	InputStream is1 = Thread.currentThread().getContextClassLoader().getResourceAsStream("A_LOOKUPHEADERS.sql");
	InputStream is2 = Thread.currentThread().getContextClassLoader().getResourceAsStream("B_LOOKUPDETAILS.sql");
	try {
	    final Scanner in1 = new Scanner(is1);
	    final Scanner in2 = new Scanner(is2);
	    TransactionTemplate transactionTemplate = new TransactionTemplate(transactionManager);
	    transactionTemplate.execute(new TransactionCallback() {
		@Override
		public Object doInTransaction(TransactionStatus status) {
		    while (in1.hasNext()) {
			String script = in1.nextLine();
			if (StringUtils.isNotBlank(script)) {
			    em.createNativeQuery(script).executeUpdate();
			}
		    }
		    while (in2.hasNext()) {
			String script = in2.nextLine();
			if (StringUtils.isNotBlank(script)) {
			    em.createNativeQuery(script).executeUpdate();
			}
		    }
		    em.clear();
		    em.flush();
		    return null;
		}
	    });
	} finally {
	    IOUtils.closeQuietly(is1);
	    IOUtils.closeQuietly(is2);
	}
    }

    private void setupPromotion() {
	Duration d = new Duration();
	d.setStartDate(new LocalDate(2014, 01, 01).toDate());
	d.setEndDate(new LocalDate(2014, 12, 31).toDate());

	Program pr = new Program();
	pr.setName("test-program");
	pr.setDuration(d);
	pr.setStatus(lookupDetailRepo.findByCode("STAT001"));
	pr.setType(ProgramType.BASIC);
	pr.setDescription("test-program description");
	programRepo.save(pr);

	Campaign c = new Campaign();
	c.setName("test-campaign");
	c.setDescription("des");
	c.setDuration(d);
	c.setProgram(pr);
	campaignRepo.save(c);

	final Promotion promotionPoint = new Promotion(123l);
	promotionPoint.setAffectedDays("DAYS001,DAYS002,DAYS003,DAYS004,DAYS005,DAYS006,DAYS007");
	promotionPoint.setName("test-promo-point");
	promotionPoint.setStartDate(d.getStartDate());
	promotionPoint.setEndDate(d.getEndDate());
	promotionPoint.setRewardType(lookupDetailRepo.findByCode("RWRD001"));
	promotionPoint.setStatus(lookupDetailRepo.findByCode("STAT001"));
	promotionPoint.setCampaign(c);

	final Promotion promotionItemPoint = new Promotion(123l);
	promotionItemPoint.setAffectedDays("DAYS001,DAYS002,DAYS003,DAYS004,DAYS005,DAYS006,DAYS007");
	promotionItemPoint.setName("test-promo-item_point");
	promotionItemPoint.setStartDate(d.getStartDate());
	promotionItemPoint.setEndDate(d.getEndDate());
	promotionItemPoint.setRewardType(lookupDetailRepo.findByCode("RWRD004"));
	promotionItemPoint.setStatus(lookupDetailRepo.findByCode("STAT001"));
	promotionItemPoint.setCampaign(c);

	promotionRepo.save(promotionPoint);
	promotionRepo.save(promotionItemPoint);
    }

    private void setupAppConfig() {
	ApplicationConfig ac = new ApplicationConfig();
	ac.setKey(POINTS_VALUE);
	ac.setValue("2");
	appConfigRepo.save(ac);
    }
    //</editor-fold>
}
