package com.transretail.crm.schedule.service;

import com.transretail.crm.common.service.dto.request.PageSortDto;
import com.transretail.crm.common.service.dto.response.ResultList;
import com.transretail.crm.core.dto.ApplicationConfigDto;
import com.transretail.crm.core.entity.enums.AppKey;



public interface AppConfigService {

	ResultList<ApplicationConfigDto> list(PageSortDto sortDto);

	void update(ApplicationConfigDto configDto);

	void initAppConfigs();

	String getValue(AppKey key);
	
	
}
