var CONTAINER_ERROR = "#contentError > div";
var ID_TRANSACTION_FORM = "employeeTransaction";
var FORM_DIALOG = '#voucherTxnAdjustment';

$(document).ready(function() {	
	$( FORM_DIALOG ).dialog({
		draggable: 	false,
		autoOpen: false,
		width: 670,
		modal: true,
		autoOpen: false,
		create: function(event, ui) {
			$(".ui-widget-overlay").click( function(event) {
				$(FORM_DIALOG).dialog('close');
			});
        }
	});
});

function adjustVoucherTransaction(transactionId, action, type, queryString) {
	$( '#voucherTxnAdjustment' ).dialog('open');
	
	var path = "/" + action + "/";
	if(transactionId == null || transactionId == '') {
		transactionId = null;
	}
//	else {
//		path += transactionId;
//	}
	
	populateForm("/populate/" + transactionId);
	
	saveTransaction(transactionId, path, type,  queryString);
}

function populateForm( inPopulateUrl ) {
	$( CONTAINER_ERROR ).parent().hide();
	
    var theParentForm = $("form[id='" + ID_TRANSACTION_FORM + "']");
    
	$.ajax({
		type 	: "POST",
		url 	: $(theParentForm).attr("action") + inPopulateUrl,
		cache: false,
		dataType: 'json',
		        
		success : function(inResponse) {
			var inReturnEntity = inResponse.result;

			$.each(inReturnEntity, function(key, value) {
				var theInput = $("input[name='" + key + "']");
				if ( theInput != null ) {
					if ( $(theInput).is(':radio') ) {
						$("input:radio[name='" + key + "']").each( function() {
							$(this).attr( 'checked', $(this).val() == value );
							if($(this).val() == value) {
								$(this).click();
							}
						});
					}
					else {
						$(theInput).val( value );
					}
				}
				else {
					if(key != "employeeVoucher") {
						$(theParentForm).addHidden(key, value);
					}
				}
			});
		},
		error : function(jqXHR, textStatus, errorThrown) {
			$( CONTAINER_ERROR ).html("Error: " + errorThrown);
			$( CONTAINER_ERROR ).show();
		}
	});
}

function approveAdjustment(id, queryString) {
	if(id == null || id == '' ) id = null;
	overrideAdjustment(id, 'approve');
}

function rejectAdjustment(id, queryString) {
	if(id == null || id == '' ) id = null;
	overrideAdjustment(id, 'reject');
}

function showAdjustment(transactionId, queryString) {
	$( FORM_DIALOG ).dialog('open');
	if(transactionId == null || transactionId == '') {
		transactionId = null;
	}
	populateForm("/populate/" + transactionId);
	$("#cancelButton").click( function() {
    	$("form[id='" + ID_TRANSACTION_FORM + "']").trigger( "reset" );
    	$(FORM_DIALOG).dialog('close');
    });
	
	$('#approve').click(function() {
		approveAdjustment(transactionId, queryString);
	});
	$('#reject').click(function() {
		rejectAdjustment(transactionId, queryString);
	});
}

function overrideAdjustment(id, status, queryString) {
	var theParentForm = $("form[id='" + ID_TRANSACTION_FORM + "']");
	
	$.ajax({
		type	: 'PUT',
		url		: $(theParentForm).attr("action") + "/" + status + "/" + id,
		cache	: false,
		dataType: 'json',
		data 	: $(theParentForm).serialize(),
		success : function(inResponse) {
			if (inResponse.success) {
				$( CONTAINER_ERROR ).parent().hide('fast');
				$(theParentForm).unbind("submit").submit( function() {
					$(this).attr("action", $(this).attr("action"));
				});
				$(theParentForm).submit();
			} 
			else {
				errorInfo = "";
				for (i = 0; i < inResponse.result.length; i++) {
					errorInfo += "<br>" + (i + 1) + ". "
							+ ( inResponse.result[i].code != undefined ? 
									inResponse.result[i].code : inResponse.result[i]);
				}
				$( CONTAINER_ERROR ).html("Please correct following errors: " + errorInfo);
				$( CONTAINER_ERROR ).parent().show('slow');
			}
		},
		error : function(jqXHR, textStatus, errorThrown) {
			$( CONTENT_ERROR ).html("Error: " + errorThrown);
		}
	});
}

function saveTransaction(id, inAjaxController, type, inQueryString ) {
    var theParentForm = $("form[id='" + ID_TRANSACTION_FORM + "']");
    
	$("#proceed").click( function() {
	    $.ajax({
			type 	: type,
			url 	: $(theParentForm).attr("action") + inAjaxController,
			cache: false,
			dataType: 'json',
			data 	: $(theParentForm).serialize() + "&id=" + id,
			        
			success : function(inResponse) {
				if (inResponse.success) {
					$( CONTAINER_ERROR ).parent().hide('fast');
					$(theParentForm).unbind("submit").submit( function() {
						$(this).attr("action", $(this).attr("action") + "?" + inQueryString);
					});
					$(theParentForm).submit();
				} 
				else {
					errorInfo = "";
					for (i = 0; i < inResponse.result.length; i++) {
						errorInfo += "<br>" + (i + 1) + ". "
								+ ( inResponse.result[i].code != undefined ? 
										inResponse.result[i].code : inResponse.result[i]);
					}
					$( CONTAINER_ERROR ).html("Please correct following errors: " + errorInfo);
					$( CONTAINER_ERROR ).parent().show('slow');
				}
			},
			error : function(jqXHR, textStatus, errorThrown) {
				$( CONTENT_ERROR ).html("Error: " + errorThrown);
			}
		});
	});


    $("#cancelButton").click( function() {
    	$(theParentForm).trigger( "reset" );
    	$( '#voucherTxnAdjustment' ).dialog('close');
    });
}