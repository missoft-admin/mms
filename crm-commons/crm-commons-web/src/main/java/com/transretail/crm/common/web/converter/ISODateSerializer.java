package com.transretail.crm.common.web.converter;

import java.io.IOException;

import org.joda.time.LocalDate;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;
import org.joda.time.format.ISODateTimeFormat;

/**
 *
 * @author Monte Cillo Co (mco@exist.com)
 */
public class ISODateSerializer extends JsonSerializer<LocalDate> {

    private static final DateTimeFormatter FORMATTER = ISODateTimeFormat.date();

    @Override
    public void serialize(LocalDate localDate, JsonGenerator jsonGenerator, SerializerProvider serializerProvider) throws
	    IOException {
	jsonGenerator.writeString(localDate != null ? FORMATTER.print(localDate) : "");
    }
}
