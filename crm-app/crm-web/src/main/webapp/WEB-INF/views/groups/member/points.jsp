<%@ include file="../../common/taglibs.jsp" %>

<fieldset>
<legend class="block-stack-title rfsLegend"><spring:message code="label_points"/></legend>

<div class="form-inline clearfix">
  <div class="control-group" style="margin: 20px 0 0 180px">
     <input type="text" class="input-small float" name="points.pointsFrom" />
     <label><spring:message code="label_rfs_to" /></label>
     <input type="text" class="input-small float" name="points.pointsTo" />
  </div>
</div>
</fieldset>

