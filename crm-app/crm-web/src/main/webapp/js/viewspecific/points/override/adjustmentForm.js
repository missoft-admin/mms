$(document).ready( function() {

	var theErrorContainer = $( "#content_adjustmentForm" ).find("#content_error");

	var $theDialog = $( "#content_adjustmentForm" );
	var $theForm = $( "#adjustmentForm" );
	var $theStatus = $( "#status" );
	var $txnDate = $( "#transactionDateTime" );



	initFormFields();
	initDialog();



	function initFormFields() {
		
		$(".floatInput").numberInput({ "type" : "float" });
		$(".intInput").numberInput({ "type" : "int" });
		$(".signedInput").numberInput({ "type" : "signed" });
		
		$theForm.submit( function(e) {
			e.preventDefault();
			disableButtons( true );
			saveForm( $(this).attr("action") );
		});
		
		/*$("#approveButton, #rejectButton").click(function() {
			$theForm.submit();
		});*/

		var theStatuses = $( ".statusButton" );
		$.each( theStatuses, function( idx, theStatus ) {
			$( this ).click( function(e) {
				$theStatus.val( $( this ).data( "value" ) );
				disableButtons( true );
				$theForm.submit();
			});
		});

		$txnDate.datepicker({ format: 'dd-mm-yyyy', autoclose : true, endDate : '-0d', });
	}

	function initDialog() {
		$theDialog.modal({ show : false });
		$theDialog.on( "hide", function(e) { 
			if ( e.target === this ) {
				theErrorContainer.hide(); 
				resetForm( $theForm );
				disableButtons( false );
			}
		});
	}



	function saveForm( inUrl ) {
		$.ajax({
			cache 		: false, 
			dataType 	: "json",
			type 		: "POST", 
			url 		: inUrl,
			data		: $theForm.serialize(),
			error 		: function( jqXHR, textStatus, errorThrown ) {},
			success 	: function( inResponse ) {
				if ( inResponse.success ) {
					$theDialog.modal( "hide" );
					/*$( "#list_adjustment" ).ajaxDataTable( 'search' );*/
					window.location.reload();
				}
				else {
					var theErrorInfo = "";
					$.each( inResponse.result, function( key, value ) {
						theErrorInfo += ( ( key + 1 ) + ". " + value.code + "<br>" );
					});
					theErrorContainer.find( "div" ).html( theErrorInfo );
					theErrorContainer.show( "slow" );
				}
				disableButtons( false );
			}
		});
	}



	function resetForm( ele ) {

	    $( ele ).find(':input').each(function() {
	        switch(this.type) {
	            case 'password':
	            case 'select-multiple':
	            case 'select-one':
	            case 'text':
	            case 'textarea':
	            case 'hidden':
	                $(this).val('');
	                break;
	            case 'checkbox':
	            case 'radio':
	                this.checked = false;
	        }
	    });

	}

	function disableButtons( disable ) {
		if ( $theForm.find( "#saveButton" ).prop( "disabled" ) ) {
			$( "#saveButton" ).prop( "disabled", disable );
		}

		if ( $theForm.find( "#approveButton" ).prop( "disabled" ) ) {
			$( "#approveButton" ).prop( "disabled", disable );
		}

		if ( $theForm.find( "#rejectButton" ).prop( "disabled" ) ) {
			$( "#rejectButton" ).prop( "disabled", disable );
		}
	}

});