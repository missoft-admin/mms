package com.transretail.crm.web.giftcard.controller.dto;


import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotEmpty;
import org.joda.time.LocalDate;

import com.transretail.crm.common.web.converter.LocalDateDeserializer;
import com.transretail.crm.web.giftcard.controller.GiftCardRequest;


public class EGiftCardRequest extends GiftCardRequest {

	public final static String PIN_PATTERN = "yyyyMMdd";
	public final static String DATE_PATTERN = LocalDateDeserializer.DATETIME_FORMAT;



    @NotEmpty
    private String mobileNo;
    @NotEmpty
    private String email;
    @NotEmpty
    private String pin;
    @NotNull( message="Fill in with birthday in " + DATE_PATTERN + " format" )
    private LocalDate birthdate;



	public String getMobileNo() {
		return mobileNo;
	}
	public void setMobileNo(String mobileNo) {
		this.mobileNo = mobileNo;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getPin() {
		return pin;
	}
	public void setPin(String pin) {
		this.pin = pin;
	}
	public LocalDate getBirthdate() {
		return birthdate;
	}
	public void setBirthdate(LocalDate birthdate) {
		this.birthdate = birthdate;
	}

}
