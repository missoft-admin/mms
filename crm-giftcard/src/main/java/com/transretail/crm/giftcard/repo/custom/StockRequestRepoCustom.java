package com.transretail.crm.giftcard.repo.custom;

public interface StockRequestRepoCustom {
	String createStockRequestNumber();
}
