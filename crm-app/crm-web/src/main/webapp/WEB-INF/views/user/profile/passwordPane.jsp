<%@ include file="../../common/taglibs.jsp" %>

  <c:set var="errors"><form:errors path="*"/></c:set>
  <c:if test="${not empty errors}">
    <div class="alert alert-error">
      <button type="button" class="close" data-dismiss="alert">&times;</button>
      <div>${errors}</div>
    </div>
  </c:if>
  <spring:message code="user_label_newpassword" var="lbl_newpassword"/>
  <spring:message code="user_label_confirmpassword" var="lbl_confirmpassword"/>
  <spring:message code="user_label_currentpassword" var="lbl_currentpassword"/>
  <div id="currentPasswordBlock" class="control-group">
    <form:label path="currentPassword" title="${lbl_currentpassword}" cssClass="control-label" cssErrorClass="control-label error">
      <span class="required">*</span>${lbl_currentpassword}
    </form:label>
    <div class="controls">
      <form:password path="currentPassword" title="${lbl_newpassword}"/>
    </div>
  </div>
  <div class="control-group">
    <form:label path="newPassword" title="${lbl_newpassword}" cssClass="control-label" cssErrorClass="control-label error">
      <span class="required">*</span>${lbl_newpassword}
    </form:label>
    <div class="controls">
      <form:password path="newPassword" title="${lbl_newpassword}"/>
    </div>
  </div>
  <div class="control-group">
    <form:label path="confirmPassword" title="${lbl_confirmpassword}" cssClass="control-label" cssErrorClass="control-label error">
      <span class="required">*</span>${lbl_confirmpassword}
    </form:label>
    <div class="controls">
      <form:password path="confirmPassword" title="${lbl_confirmpassword}"/>
    </div>
  </div>
