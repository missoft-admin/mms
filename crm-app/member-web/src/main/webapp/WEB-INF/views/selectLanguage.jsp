<%@ include file="taglibs.jsp" %>
<jsp:useBean id="availableLanguages" class="java.util.HashMap" scope="session"/>
<%--See http://www.localeplanet.com/java/ for the list of Java Locale--%>
<c:set target="${availableLanguages}" property="en" value="Language: <b>English</b>"/>
<c:set target="${availableLanguages}" property="in" value="Bahasa: <b>Bahasa Indonesia</b>"/>

<c:set var="currentLocale">
  <c:choose>
    <c:when test="${!empty param.lang}">
      ${param.lang}
    </c:when>
    <%-- cookie name must match with CookieLocaleResolver#cookieName in webmvc-config.xml --%>
    <c:when test="${!empty cookie.locale.value}">
      ${cookie.locale.value}
    </c:when>
    <c:otherwise>
      <%-- defaultLocale must match with CookieLocaleResolver#defaultLocale in webmvc-config.xml --%>
      in
    </c:otherwise>
  </c:choose>
</c:set>



<%--<li class="">
  <span class="arrow-icon-bottom">
    <span>
      <i class="icon-<c:out value="${fn:toLowerCase(availableLanguages[currentLocale])}" />"></i>${availableLanguages[currentLocale]}
    </span>
  </span>

  <div class="dropdown-menu row minor-menu">
    <ul class="content main-menu-dropdown ">
      <c:forEach items="${availableLanguages}" var="lang">
        <c:if test="${currentLocale != lang.key}">
          <li>
            <a href="?lang=${lang.key}"><i class="icon-<c:out value="${fn:toLowerCase(lang.value)}" />"></i>${lang.value}</a>
          </li>
        </c:if>
      </c:forEach>
    </ul>
  </div>
</li>--%>
<c:url var="theUrl" value="">
  <c:forEach items="${param}" var="item">
    <c:if test="${item.key != 'lang'}">
      <c:param name="${item.key}" value="${item.value}" />
    </c:if>
  </c:forEach>
  <c:param name="lang" value="" />
</c:url>
<li class="dropdown language">
  <a href="${theUrl}${currentLocale}" class="dropdown-toggle arrow-icon-bottom block-language" data-toggle="dropdown"><!--<b class="icon-<c:out value="${fn:toLowerCase(availableLanguages[currentLocale])}" />"></b>-->${availableLanguages[currentLocale]} <b class="caret"></b></a>
  <ul class="dropdown-menu row minor-menu">
    <c:forEach items="${availableLanguages}" var="lang">
      <c:if test="${currentLocale != lang.key}">
        <li><a href="${theUrl}${lang.key}"><!--<b class="icon-<c:out value="${fn:toLowerCase(lang.value)}" />"></b>-->${lang.value}</a></li>
      </c:if>
    </c:forEach>
  </ul>
</li>