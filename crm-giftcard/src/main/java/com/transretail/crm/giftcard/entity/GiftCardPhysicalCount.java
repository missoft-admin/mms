package com.transretail.crm.giftcard.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import com.transretail.crm.core.entity.support.CustomAuditableEntity;

/**
 *
 * @author Monte Cillo Co (mco@exist.com)
 */
@Table(name = "CRM_GC_PHYSICAL_COUNT")
@Entity
public class GiftCardPhysicalCount extends CustomAuditableEntity<Long> implements Serializable {

    private static final long serialVersionUID = 1L;
    @Column(name="PRODUCT_PROFILE")
    private String productProfile;
    @Column(name = "LOCATION", nullable = false)
    private String location;
    @Column(name = "STARTING_SERIES", length = 16, nullable = false)
    private String startingSeries;
    @Column(name = "ENDING_SERIES", length = 16, nullable = false)
    private String endingSeries;
    @Column(name = "QUANTITY", nullable = false)
    private Long quantity;

    public String getProductProfile() {
	return productProfile;
    }

    public void setProductProfile(String productProfile) {
	this.productProfile = productProfile;
    }

    public String getLocation() {
	return location;
    }

    public void setLocation(String location) {
	this.location = location;
    }

    public String getStartingSeries() {
	return startingSeries;
    }

    public void setStartingSeries(String startingSeries) {
	this.startingSeries = startingSeries;
    }

    public String getEndingSeries() {
	return endingSeries;
    }

    public void setEndingSeries(String endingSeries) {
	this.endingSeries = endingSeries;
    }

    public Long getQuantity() {
	return quantity;
    }

    public void setQuantity(Long quantity) {
	this.quantity = quantity;
    }
}
