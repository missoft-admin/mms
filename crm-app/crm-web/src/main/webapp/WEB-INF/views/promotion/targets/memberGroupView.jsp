<%@ include file="../../common/taglibs.jsp" %>


<div class="clearfix"></div>
<div id="content_memberGroupView" class="alert alert-info">
  <button type="button" class="close" onclick="$(this).parent().hide();">&times;</button>

  <div id="form_view" class="form-horizontal control-group-01">
    <div class="control-group">
      <label for="" class="control-label"><spring:message code='label_group_groupname'/></label>
      <label for="" class="controls bold control-data el-width-06">
        <c:out value="${memberGroup.name}" />
      </label>
    </div>

    <c:if test="${not empty memberGroup.memberFields}">
	    <div class="control-group">
	      <label for="" class="control-label"><spring:message code="label_group_field"/></label>
	      <label class="control-label control-data el-width-06">
	        <c:forEach var="memberfield" items="${memberGroup.memberFields}">
	          <div class=""><c:out value="${memberfield.name.description}" /></div>
	          <div class="data-content-03 normal">          
	            <c:forEach var="fieldValue" varStatus="loop" items="${memberfield.fieldValues}">
	              <c:out value="${fieldValue.value}" />
                <c:if test="${!loop.last}">
                  <span class="lowercase">
                    <c:choose>
                      <c:when test="${memberfield.name.code != 'MEM014' or memberfield.name.code != 'MEM018'}">
		                    <c:if test="${fieldValue.operand.code == 'OPRD001'}">&#44;</c:if>
                      </c:when>
                      <c:otherwise><c:out value="${fieldValue.operand.description}" /></c:otherwise>
                    </c:choose>
                  </span>
                </c:if>
	            </c:forEach>
	          </div>
	        </c:forEach>
	      </label>
	    </div>
    </c:if>
    
    <c:if test="${memberGroup.rfs.enabled}">
      <div class="control-group">
        <label class="control-label"><spring:message code="label_rfs"/></label>
        <label class="control-label data-content-01 control-data el-width-06">
         
          <c:if test="${not empty memberGroup.rfs.storeGroup}">
          <div class=""><spring:message code="label_member_grp_rfs_store"/></div>
          <div class="data-content-03 normal">${memberGroup.rfs.storeGroupName}</div>
          </c:if>
          
          <c:if test="${not empty memberGroup.rfs.productGroup}">
          <div class=""><spring:message code="label_member_grp_rfs_product"/></div>
          <div class="data-content-03 normal">${memberGroup.rfs.productGroupName}</div>
          </c:if>
          
          <c:if test="${memberGroup.rfs.recency == 'dateRange'}">
          <div class=""><spring:message code="label_rfs_recency_daterange"/></div>
          <div class="data-content-03 normal">${memberGroup.rfs.dateRangeFrom} <spring:message code="label_rfs_to" /> ${memberGroup.rfs.dateRangeTo}</div>
          </c:if>
          
          <c:if test="${memberGroup.rfs.frequency}">
          <div class=""><spring:message code="label_rfs_frequency"/></div>
          <div class="data-content-03 normal">${memberGroup.rfs.frequencyFrom} <spring:message code="label_rfs_frequencytimes" /> <spring:message code="label_rfs_to" /> ${memberGroup.rfs.frequencyTo} <spring:message code="label_rfs_frequencytimes" /></div>
          </c:if>
          
          <c:if test="${memberGroup.rfs.spend}">
          <div class=""><spring:message code="label_rfs_spend"/></div>
          <div class="data-content-03 normal">${memberGroup.rfs.spendFrom} <spring:message code="label_rfs_to" /> ${memberGroup.rfs.spendTo} <c:if test="${memberGroup.rfs.spendPerTransaction}"><spring:message code="label_rfs_spendpertransaction"/></c:if></div>
          </c:if>
          
        </label>
      </div>
    </c:if>
    
    <c:if test="${memberGroup.points.enabled}">
      <div class="control-group">
        <label class="control-label"><spring:message code="label_points"/></label>
        <label class="control-label data-content-01 control-data el-width-06">${memberGroup.points.pointsFrom} <spring:message code="label_rfs_to" /> ${memberGroup.points.pointsTo}</label>
      </div>
    </c:if>
  </div>

<style><!-- .el-width-06 { margin-top: 5px !important; width: 300px !important; font-weight: bold; }  --></style>
</div>