package com.transretail.crm.template.service.streaming.excel.handler.cell;

import org.apache.commons.lang3.StringUtils;

import com.transretail.crm.core.entity.MemberModel;
import com.transretail.crm.core.entity.embeddable.ProfessionalProfile;
import com.transretail.crm.core.entity.lookup.LookupDetail;
import com.transretail.crm.template.service.MemberTemplateParseException;
import com.transretail.crm.template.service.streaming.excel.handler.sheet.AbstractSheetHandler;

/**
 * @author Allan G. Ramirez (agramirez@exist.com)
 */
public class RadiusHandler implements CellHandler {
    private AbstractSheetHandler sheetHandler;

    public RadiusHandler(AbstractSheetHandler sheetHandler) {
        this.sheetHandler = sheetHandler;
    }

    @Override
    public void handle(MemberModel dto, String cellText) throws MemberTemplateParseException {
        if (dto.getProfessionalProfile() == null) {
            dto.setProfessionalProfile(new ProfessionalProfile());
        }
        if (StringUtils.isNotBlank(cellText)) {
            LookupDetail detail = sheetHandler.getLoookupDetail(sheetHandler.getCodePropertiesService().getHeaderRadius(), cellText);
            dto.getProfessionalProfile().setRadius(detail.getCode());
        }
    }
}
