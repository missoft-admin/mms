package com.transretail.crm.core.entity.enums;

/**
 * @author Allan G. Ramirez (agramirez@exist.com)
 */
public enum Status {
    ACTIVE, INACTIVE
}
