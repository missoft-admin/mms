<%@ include file="../../common/taglibs.jsp" %>


<div id="rewardList" class="el-block-01 row-fluid pull-center">

  <table class="table table-striped">
    <thead>
      <tr>
        <c:if test="${rewardItemPoint}">
	        <th><spring:message code="reward_prop_minqty"/></th>
	        <th><spring:message code="reward_prop_maxqty"/></th>
          <th><spring:message code="reward_prop_baseqty"/></th>
          <th><spring:message code="reward_prop_point"/></th>
        </c:if>

        <c:if test="${rewardPoints}">
	        <th><spring:message code="reward_prop_minamount"/></th>
	        <th><spring:message code="reward_prop_maxamount"/></th>
          <th><spring:message code="reward_prop_baseamount"/></th>
          <th class="content_rewardPoints"><spring:message code="reward_prop_basefactor"/></th>
          <th></th>
        </c:if>

        <c:if test="${rewardDiscount}">
	        <th><spring:message code="reward_prop_minamount"/></th>
	        <th><spring:message code="reward_prop_maxamount"/></th>
          <th class="content_rewardDiscount"><spring:message code="reward_prop_discount"/></th>
        </c:if>

        <c:if test="${rewardFreebie}">
          <th><spring:message code="reward_prop_minqty"/></th>
          <th><spring:message code="reward_prop_maxqty"/></th>
	        <th><spring:message code="reward_prop_minamount"/></th>
	        <th><spring:message code="reward_prop_maxamount"/></th>
          <th class="content_rewardFreebie"><spring:message code="reward_prop_maxbonus"/></th>
        </c:if>

        <c:if test="${rewardRedeemItem}">
          <th><spring:message code="promo_redemption_prop_pluid"/></th>
          <th><spring:message code="promo_redemption_prop_prdname"/></th>
          <th><spring:message code="promo_redemption_prop_points"/></th>
        </c:if>
      </tr>
    </thead>

    <tbody id="promotionRewardsEntries">
      <c:forEach varStatus="varRewardIdx" begin="0" var="reward" items="${promotion.promotionRewards}" >
        <c:set var="rewardIdx" value="${varRewardIdx.index}" scope="request" />
	      <c:if test="${null != rewardIdx}">
				  <tr class="promotionRewardsEntry">
				    <form:hidden path="promotionRewards[${rewardIdx}].id" />				
				    <c:if test="${rewardItemPoint}">
				      <td><form:input path="promotionRewards[${rewardIdx}].minQty" cssClass="input-medium wholeInput" disabled="true" /></td>
				      <td><form:input path="promotionRewards[${rewardIdx}].maxQty" cssClass="input-medium wholeInput" disabled="true" /></td>
				      <td><form:input path="promotionRewards[${rewardIdx}].baseAmount" cssClass="input-medium wholeInput" disabled="true" /></td>
				      <td class=""><form:input path="promotionRewards[${rewardIdx}].baseFactor" cssClass="input-medium floatInput" disabled="true" /></td>
				    </c:if>
				
				    <c:if test="${rewardPoints}">
				      <td><form:input path="promotionRewards[${rewardIdx}].minAmount" cssClass="input-medium floatInput" disabled="true" /></td>
				      <td><form:input path="promotionRewards[${rewardIdx}].maxAmount" cssClass="input-medium floatInput" disabled="true" /></td>
				      <td><form:input path="promotionRewards[${rewardIdx}].baseAmount" cssClass="input-medium floatInput" disabled="true" /></td>
				      <td class=""><form:input path="promotionRewards[${rewardIdx}].baseFactor" cssClass="input-medium intInput" disabled="true" /></td>
                      <td class=""><div class="checkbox"><form:checkbox path="promotionRewards[${rewardIdx}].fixed" disabled="true" /> <spring:message code="reward_prop_fixed" /></div></td>
				    </c:if>
				
				    <c:if test="${rewardDiscount}">
				      <td><form:input path="promotionRewards[${rewardIdx}].minAmount" cssClass="input-medium floatInput" disabled="true" /></td>
				      <td><form:input path="promotionRewards[${rewardIdx}].maxAmount" cssClass="input-medium floatInput" disabled="true" /></td>
				      <td class="content_rewardDiscount"><form:input path="promotionRewards[${rewardIdx}].discount" cssClass="input-medium floatInput" disabled="true" /></td>
				    </c:if>
				
				    <c:if test="${rewardFreebie}">
				      <td><form:input path="promotionRewards[${rewardIdx}].minQty"  cssClass="input-medium floatInput" disabled="true" /></td>
				      <td><form:input path="promotionRewards[${rewardIdx}].maxQty" cssClass="input-medium floatInput" disabled="true" /></td>
				      <td><form:input path="promotionRewards[${rewardIdx}].minAmount" cssClass="input-medium floatInput" disabled="true" /></td>
				      <td><form:input path="promotionRewards[${rewardIdx}].maxAmount" cssClass="input-medium floatInput" disabled="true" /></td>
				      <td class="content_rewardFreebie"><form:input path="promotionRewards[${rewardIdx}].maxBonus" cssClass="input-medium floatInput" disabled="true" /></td>
				    </c:if>

		        <c:if test="${rewardRedeemItem}">
              <td><form:input path="promotionRewards[${rewardIdx}].productCode"  cssClass="input-medium floatInput" disabled="true" /></td>
              <td>${promotion.promotionRewards[rewardIdx].productName}</td>
              <td><form:input path="promotionRewards[${rewardIdx}].baseFactor"  cssClass="input-medium floatInput" disabled="true" /></td>
		        </c:if>
				  </tr>
	      </c:if>
      </c:forEach>
    </tbody>
  </table>


  <script src='<c:url value="/js/viewspecific/promotion/reward/rewardList.js"/>' type='text/javascript'><![CDATA[&nbsp; ]]></script>
  

</div>




