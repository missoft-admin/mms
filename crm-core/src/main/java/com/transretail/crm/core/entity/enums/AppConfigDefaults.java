package com.transretail.crm.core.entity.enums;

public enum AppConfigDefaults {
    INSTANCE;

    public static final String DEFAULT_REWARDS_JOB_CRON = "0 0 10am 1 * ?";
    public static final String DEFAULT_PRODUCT_HIER_UPDATER_CRON = "0 30 0 * * ?";
    public static final String DEFAULT_POINTS_VALUE = "1";
    public static final String DEFAULT_SUSPENDED_FROM_POINTS = "";
    public static final String DEFAULT_TXN_COUNT_EMP = "3";
    public static final String DEFAULT_TXN_COUNT_IND = "3";

    public static final String DEFAULT_MEMBER_MONITOR_NOTIF_CRON = "0 0 6am * * ?";
    public static final String DEFAULT_MARKETING_CHANNEL_CRON = "0 0/15 * * * ? *";//"0 0/1 * 1/1 * ? *";//
    public static final String DEFAULT_VALID_PERIOD = "25";
    public static final String DEFAULT_EXPIRE_JOB = "0 0 3am 1 * ?";
    public static final String DEFAULT_SHOPPING_LIST_INCLUSIVE_TIME = "6";
    public static final String DEFAULT_SHOPPING_LIST_EXCLUSIVE_TIME = "7";
    public static final String DEFAULT_SHOPPING_LIST_MIN_TXN = "2";

    public static final String DEFAULT_REWARDS_JOB_DONE_CRON = "0 0 10am 1 * ?";
    public static final String DEFAULT_LOYALTY_EXPIRY_JOB = "0 0 3am * * ?";
    public static final String DEFAULT_LOYALTY_EXPIRY_IN_MONTHS = "12";

    public static final String DEFAULT_ALLOW_NO_LOYALTYCARD_TO_EARN = "";
    public static final String DEFAULT_LOYALTY_EXPIRE_POINTS_IN_MONTHS = "6";


    public static final String DEFAULT_MD5_SECRET_KEY = "carrefourmmskey";

    public static final String DEFAULT_GIFT_CARD_EXPIRY_IN_MONTHS = "12";

    public static final String DEFAULT_COMPUTE_POINTS_CRON = "0 0/15 * * * ? *";
    public static final String DEFAULT_PRIMARY_WS_URL = "http://10.21.5.131:80/api";

    public static final String DEFAULT_PRIMARY_WS_SOCKET_TIMEOUT = "2000";  //default to 2 seconds
    
    public static final String DEFAULT_DISCOUNT_EOC_JOB = "0 0 3am * * ?";

    public static final String DEFAULT_SAFETY_STOCK_CRON = "0 0 0 L * ?";

    public static final String DEFAULT_GCSO_RECEIPT_COUNT = "0";

    public static final String DEFAULT_LOYALTY_CARD_RENEWAL_CODE = "71000002001001";

    public static final String DEFAULT_LOYALTY_CARD_REPLACEMENT_CODE = "71000001001001";

    public static final String DEFAULT_PRIMARY_WS_READ_TIMEOUT = "10000";  //default to 10 seconds

    public static final String DEFAULT_GC_TXN_SETTLEMENT_CRON = "0 0 3am * * ?";
    
    public static final String DEFAULT_GC_CLAIMS_TO_AFFILIATES_CRON="0 0 12am 1 * ?";

    public static final String DEFAULT_RECONCILIATION_CRON="0 30 0 * * ?"; // Fire at 00:30 AM

    public static final String DEFAULT_KWITANSI_TXN_PREFIX="KW";
    public static final String DEFAULT_KWITANSI_TXN_CONTROL_NO="0000000000";
    public static final String DEFAULT_RECONCILE_MAIL = "";

    public static final String DEFAULT_SMS_GATEWAY_URL = "http://%GATEWAYIP%/sms.php?tipe=XML&smsid=%SMSID%&msisdn=%MOBILENUMBER%&mess=%MESSAGE%&API=%API%";
    public static final String DEFAULT_RECONCILE_CARD = "'421408','627891','601900'";
    public static final String DEFAULT_RECONCILE_MEDIA = "'CASH','DEBIT','GC'";

    public static final String DEFAULT_PORTA_URL_STAMP_CREATE = "";
    public static final String DEFAULT_PORTA_URL_STAMP_REDEEM = "";
    public static final String DEFAULT_WS_SOCKET_TIMEOUT = "2000";  //default to 2 seconds
    public static final String DEFAULT_WS_READ_TIMEOUT = "10000";  //default to 10 seconds

    public static final String DEFAULT_PORTA_URL_STAMP_ACCOUNT = "";
}
