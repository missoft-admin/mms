package com.transretail.crm.core.service;

import java.util.List;

import com.mysema.query.types.OrderSpecifier;
import com.transretail.crm.common.service.dto.response.CodeDescDto;
import com.transretail.crm.common.service.exception.MessageSourceResolvableException;
import com.transretail.crm.core.dto.LookupDetailResultList;
import com.transretail.crm.core.dto.LookupDetailSearchDto;
import com.transretail.crm.core.dto.LookupHeaderResultList;
import com.transretail.crm.core.dto.LookupHeaderSearchDto;
import com.transretail.crm.core.entity.lookup.LookupDetail;
import com.transretail.crm.core.entity.lookup.LookupHeader;

/**
 *
 */
public interface LookupService {
    LookupHeaderResultList getLookupHeaders(LookupHeaderSearchDto dto);

    LookupDetailResultList getLookupDetails(LookupDetailSearchDto dto);

    List<LookupHeader> getAllHeaders();

    LookupDetail saveDetail(LookupDetail detail);

    List<LookupDetail> getDetails(LookupHeader header);

    List<LookupDetail> getDetailsByHeaderCode(String headerCode);

    boolean isDuplicateDetail(String code);

    List<LookupDetail> getDetails(String headerName);

    List<LookupHeader> getMemberHeaders();

    List<LookupHeader> getAllMemberHeaders();

    Iterable<LookupHeader> getMemberConfigurableFields();

    LookupDetail getDetail(String id);
    
    LookupDetail getDetailByTwoDigitCardTypeNumber(String code);
    
    LookupDetail getDetailByTwoDigitCompanyNumber(String code);

    LookupDetail getDetailByCode(String code);

    LookupHeader getHeader(String code);

    List<LookupDetail> getActiveDetails(LookupHeader header);

    List<LookupDetail> getActiveDetailsByHeaderCode(String headerCode);

    boolean hasDuplicateDetailDescription(String code, String description, LookupHeader header);

    List<LookupHeader> getAllHeaders(String[] ignore);

    boolean isValidCompanyDetailCode(String code);
    
    boolean isValidCardTypeDetailCode(String code);

    LookupDetail getLookupDetailByHdrCodeAndDesc(String headerCode, String desc);

    String getLookupDetailCodeByHdrCodeAndDesc(String headerCode, String desc);

	List<LookupHeader> getAllHeaders(String[] ignore, String field, boolean isAscending);

    void updateHeader(CodeDescDto codeDescDto) throws MessageSourceResolvableException;

    /**
     * Get lookup details by codes.
     *
     * @param codes comma separated {@link com.transretail.crm.core.entity.lookup.LookupDetail#code}
     * @return lookup details by codes.
     */
    List<LookupDetail> getLookupDetails(String codes);

	LookupDetail getActiveDetailByCode(String code);

	List<LookupDetail> getActiveDetailsByHeaderCode(String headerCode, OrderSpecifier<?>[] orders);
}
