<%@include file="/WEB-INF/views/common/taglibs.jsp" %>

<div class="page-header page-header2">
<h1><spring:message code="menutab_loyalty_card_inventory" /></h1>
</div>

<div id="searchForm">
  <div>
    <spring:message code="gc.inventory.seriesFrom" var="seriesFrom"/>
    <label for="seriesFrom" title="${seriesFrom}">${seriesFrom}</label>
    <input id="seriesFrom" name="seriesFrom" title="${seriesFrom}" type="text" value="">
    <spring:message code="to"/>
    <input id="seriesTo" name="seriesTo" type="text" value="">
  </div>
  <div>
    <label>&nbsp;</label>
      <span>
        <button id="receive" type="button"><spring:message code="receive"/></button>
        <button id="detailedView" type="button"><spring:message code="gc.inventory.view.detail"/></button>
        <button id="summarizedView" type="button"><spring:message code="gc.inventory.view.summary"/></button>
      </span>
  </div>
</div>
<div id="expiryDateDialog" class="contentHidden" style="text-align: center;">
  <div class="contentDialogBody">
    <spring:message code="gc.inventory.expiry" var="expiryDate"/>
    <h3>${expiryDate} (<spring:message code="optional"/>)</h3>
    <input id="expiryDate" name="expiryDateDate" class="datepicker-generic" title="${expiryDate}" type="text" value="">
  </div>
  <div class="contentDialogBody contentDialogButtons">
    <div class="contentButton">
      <button id="proceed" class="buttonRed" type="button"><spring:message code="proceed"/></button>
    </div>
    <div class="contentButton">
      <button id="cancel" class="buttonRed" type="button"><spring:message code="cancel"/></button>
    </div>
    <hr class="floatClear">
  </div>
</div>
<div id="errorMessages">&nbsp;</div>
<div id="successMessages">&nbsp;</div>
<div id="detailed_list_container" class="contentHidden">
</div>
<div id="summarized_list_container" class="contentHidden">
</div>
<script src="<spring:url value="/js/jquery/jquery.numberInput.js" />" ></script>
<link href="<spring:url value="/css/dataTables.css" />" rel="stylesheet">
<script src="<spring:url value="/js/jquery/jquery.dataTables.js" />"></script>
<script src="<spring:url value="/js/jquery/jquery.dataTables.linkLengthMenu.js" />"></script>
<script src="<spring:url value="/js/jquery/jquery.dataTables.customPaging.js" />"></script>
<script src="<spring:url value="/js/jquery/ajax-datatable.js" />"></script>
<script src="<spring:url value="/js/common/form2js.js" />"></script>
<script src="<spring:url value="/js/common/json2.js" />"></script>
<script src="<spring:url value="/js/jquery/jquery.toObject.js" />"></script>
<script>
  $( document ).ready( function () {
    var $seriesFrom = $( "#seriesFrom" ).numberInput( { "maxChar" : 16 } );
    var $seriesTo = $( "#seriesTo" ).numberInput( { "maxChar" : 16 } );
    var $expiryDate = $( '#expiryDate' );

    var $detailedList = $( "#detailed_list_container" ).ajaxDataTable( {
      "autoload" : false,
      "ajaxSource" : "<spring:url value="/giftcard/inventory/receive/detail/list" />",
      "columnHeaders" : [
        "<spring:message code="gc.inventory.series" />",
        "<spring:message code="gc.inventory.productName" />",
        "<spring:message code="gc.inventory.faceValue" />"
      ],
      "modelFields" : [
        {name : "series", sortable : true},
        {name : "productName", sortable : true},
        {name : "faceValue"}
      ]
    } );

    var $summarizedList = $( "#summarized_list_container" ).ajaxDataTable( {
      "autoload" : false,
      "ajaxSource" : "<spring:url value="/giftcard/inventory/receive/summary/list" />",
      "columnHeaders" : [
        "<spring:message code="gc.inventory.productName" />",
        "<spring:message code="gc.inventory.batchNo" />",
        "<spring:message code="gc.inventory.faceValue" />",
        "<spring:message code="gc.inventory.seriesFrom" />",
        "<spring:message code="gc.inventory.seriesTo" />",
        "<spring:message code="gc.inventory.quantity" />",
        "<spring:message code="gc.inventory.location" />",
        "<spring:message code="gc.inventory.allocatedTo" />",
        "<spring:message code="gc.inventory.status" />"
      ],
      "modelFields" : [
        {name : "productName", sortable : true},
        {name : "batchNo"},
        {name : "faceValue"},
        {name : "seriesFrom"},
        {name : "seriesTo"},
        {name : "quantity"},
        {name : "location"},
        {name : "allocateTo"},
        {name : "status"}
      ]
    } );

    $( "#detailedView" ).click( function () {
      var formDataObject = $( "#searchForm" ).toObject( {mode : "first", skipEmpty : false} );
      var $button = $( this );
      $button.prop( "disabled", true );
      $detailedList.ajaxDataTable( "search", formDataObject, function () {
        $summarizedList.hide();
        $detailedList.show();
        $button.prop( "disabled", false );
      } );
    } );

    $( "#summarizedView" ).click( function () {
      var formDataObject = $( "#searchForm" ).toObject( {mode : "first", skipEmpty : false} );
      var $button = $( this );
      $button.prop( "disabled", true );
      $summarizedList.ajaxDataTable( "search", formDataObject, function () {
        $detailedList.hide();
        $summarizedList.show();
        $button.prop( "disabled", false );
      } );
    } );

    var $expiryDateDialog = $( "#expiryDateDialog" ).dialog( {
      draggable : false,
      autoOpen : false,
      width : 300,
      modal : true,
      close : function ( event, ui ) {
        $expiryDate.val( '' );
      }
    } );
    $( "#receive" ).click( function () {
      if ( isEmpty( $seriesFrom.val() ) || isEmpty( $seriesFrom.val() ) ) {
        alert( "<spring:message code="gc.inventory.seriesRange.required" />" );
      } else {
        $expiryDateDialog.dialog( "open" );
      }
    } );

    $( "#proceed" ).click( function () {
      var expiryDate = $expiryDate.val();
      var formData = "{";
      formData += '"seriesFrom" : "' + $seriesFrom.val() + '"';
      formData += ', "seriesTo" : "' + $seriesTo.val() + '"';
      if ( !isEmpty( expiryDate ) ) {
        formData += ', "expiryDate" : "' + expiryDate + '"';
      }
      formData += "}";
      $.ajax( {
        headers : { "Content-Type" : "application/json" },
        "type" : "POST",
        "url" : "<spring:url value="/giftcard/inventory/receive/save" />",
        "data" : formData,
        "success" : function ( data ) {
          if ( data.hasOwnProperty( 'errorMessages' ) ) {
            var errorInfo = "";
            for ( var i = 0; i < data.errorMessages.length; i++ ) {
              errorInfo += "<br>" + (i + 1) + ". " + data.errorMessages[i];
            }
            $( '#errorMessages' ).html( "<spring:message code="haserror.message" />: " + errorInfo );
          } else {
            $( '#errorMessages' ).html( '' );
            $( '#successMessages' ).html( data.successMessages );
            $summarizedList.hide();
            $detailedList.hide();
            $expiryDateDialog.dialog( "close" );
          }
        },
        "error" : function ( xhr, textStatus, error ) {
          if ( textStatus === 'timeout' ) {
            $( '#errorMessages' ).html( 'The server took too long to send the data.' );
          } else if ( error ) {
            $( '#errorMessages' ).html( error );
          } else {
            $( '#errorMessages' ).html( 'An error occurred on the server. Please try again.' );
          }
          $expiryDateDialog.dialog( 'close' );
        }
      } );
    } );

    $( "#cancel" ).click( function () {
      $expiryDateDialog.dialog( 'close' );
    } );
  } );
</script>