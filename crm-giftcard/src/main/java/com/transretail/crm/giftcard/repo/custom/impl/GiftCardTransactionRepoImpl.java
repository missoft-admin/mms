package com.transretail.crm.giftcard.repo.custom.impl;

import static com.transretail.crm.giftcard.entity.support.GiftCardSaleTransaction.ACTIVATION;
import static com.transretail.crm.giftcard.entity.support.GiftCardSaleTransaction.REDEMPTION;
import static com.transretail.crm.giftcard.entity.support.GiftCardSaleTransaction.VOID_ACTIVATED;
import static com.transretail.crm.giftcard.entity.support.GiftCardSaleTransaction.VOID_REDEMPTION;

import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.annotation.Nonnull;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;
import org.joda.time.YearMonth;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import com.google.common.collect.ImmutableMap;
import com.mysema.query.BooleanBuilder;
import com.mysema.query.group.GroupBy;
import com.mysema.query.jpa.JPASubQuery;
import com.mysema.query.jpa.impl.JPAQuery;
import com.mysema.query.jpa.impl.JPAUpdateClause;
import com.mysema.query.support.Expressions;
import com.mysema.query.types.Projections;
import com.mysema.query.types.expr.BooleanExpression;
import com.mysema.query.types.query.ListSubQuery;
import com.transretail.crm.common.service.dto.request.PagingParam;
import com.transretail.crm.common.service.dto.request.PagingParam.DataType;
import com.transretail.crm.common.service.util.SpringDataPagingUtil;
import com.transretail.crm.core.entity.CrmForPeoplesoftConfig.TransactionType;
import com.transretail.crm.core.entity.QBusinessUnitPsoftConfig;
import com.transretail.crm.core.entity.QPeopleSoftStore;
import com.transretail.crm.core.entity.lookup.QLookupDetail;
import com.transretail.crm.core.entity.lookup.Store;
import com.transretail.crm.core.repo.StoreRepo;
import com.transretail.crm.giftcard.dto.AffiliateDto;
import com.transretail.crm.giftcard.dto.LoseTransactionResultList;
import com.transretail.crm.giftcard.dto.PosLoseTransactionSearchDto;
import com.transretail.crm.giftcard.dto.PosLoseTxForm;
import com.transretail.crm.giftcard.entity.GiftCardInventory;
import com.transretail.crm.giftcard.entity.GiftCardTransaction;
import com.transretail.crm.giftcard.entity.PosGcTransaction;
import com.transretail.crm.giftcard.entity.PosGcTransaction.ServerType;
import com.transretail.crm.giftcard.entity.QGiftCardInventory;
import com.transretail.crm.giftcard.entity.QGiftCardTransaction;
import com.transretail.crm.giftcard.entity.QGiftCardTransactionItem;
import com.transretail.crm.giftcard.entity.QPosGcTransaction;
import com.transretail.crm.giftcard.entity.QSalesOrder;
import com.transretail.crm.giftcard.entity.QSalesOrderAlloc;
import com.transretail.crm.giftcard.entity.QSalesOrderItem;
import com.transretail.crm.giftcard.entity.support.GiftCardSaleTransaction;
import com.transretail.crm.giftcard.entity.support.GiftCardSalesType;
import com.transretail.crm.giftcard.repo.custom.GiftCardTransactionRepoCustom;

public class GiftCardTransactionRepoImpl implements GiftCardTransactionRepoCustom {

    @PersistenceContext
    private EntityManager em;

    private static final String[] DATE_FIELDS = new String[]{"created", "transactionDate"};

    private static final GiftCardSaleTransaction[] TXN_SALES_TYPES = {ACTIVATION, VOID_ACTIVATED};
    private static final GiftCardSaleTransaction[] TXN_REDEMPTION_TYPES = {REDEMPTION, VOID_REDEMPTION};

    protected static final String YEAR_MONTH_FORMAT = "yyyyMM";

    private static final ImmutableMap<String, String> VIEW_MODEL_PROP_MAPPING = new ImmutableMap.Builder<String, String>()
            .put("createdBy", "createUser")
            .put("bookedDate", "created").build();

    @Autowired
    private StoreRepo storeRepo;

    @Override
    public GiftCardTransaction findEnclosingTransactionOfGiftCard(
            @Nonnull GiftCardInventory cardInventory,
            @Nonnull GiftCardSaleTransaction saleTransaction) {
        QGiftCardTransaction qtx = QGiftCardTransaction.giftCardTransaction;
        QGiftCardTransactionItem qtxi = QGiftCardTransactionItem.giftCardTransactionItem;

        GiftCardTransaction cardTransaction = new JPAQuery(em).from(qtx)
                .join(qtx.transactionItems, qtxi)
                .where(qtxi.giftCard.eq(cardInventory)
                        .and(qtx.transactionType.eq(saleTransaction)))
                .orderBy(qtx.transactionDate.desc())
                .distinct().limit(1).uniqueResult(qtx);
        return cardTransaction;
    }

    @Override
    public long countGiftCardGroupByTransaction(
            @Nonnull GiftCardInventory cardInventory,
            @Nonnull GiftCardSaleTransaction saleTransaction) {
        QGiftCardTransactionItem qgcti = QGiftCardTransactionItem.giftCardTransactionItem;
        QGiftCardTransaction qgct = QGiftCardTransaction.giftCardTransaction;
        long count = new JPAQuery(em).from(qgct).join(qgct.transactionItems, qgcti)
                .where(qgct.transactionType.eq(saleTransaction)
                        .and(qgcti.giftCard.eq(cardInventory)))
                .count();
        return count;
    }

    @Override
    public GiftCardTransaction findEnclosingTransactionViaSalesOrder(GiftCardInventory inventory) {
        QGiftCardTransaction qtx = QGiftCardTransaction.giftCardTransaction;
        QSalesOrderAlloc qsoa = QSalesOrderAlloc.salesOrderAlloc;
        QSalesOrderItem qsoi = QSalesOrderItem.salesOrderItem;
        QSalesOrder qso = QSalesOrder.salesOrder;

        List<GiftCardTransaction> list = new JPAQuery(em).from(qtx)
                .innerJoin(qtx.salesOrder, qso)
                .innerJoin(qso.items, qsoi)
                .innerJoin(qsoi.soAlloc, qsoa)
                .where(qsoa.seriesStart.loe( inventory.getSeries() ).and( qsoa.seriesEnd.goe(inventory.getSeries())))//.where(qsoa.gcInventory.eq(inventory))
                .orderBy(qtx.transactionDate.desc())
                .list(qtx);

        return list.iterator().hasNext() ? list.iterator().next() : null;
    }

    @Override
    public boolean transactionNoExists(String transactioNo, GiftCardSaleTransaction transaction) {
        QGiftCardTransaction qtx = QGiftCardTransaction.giftCardTransaction;
        return new JPAQuery(em).from(qtx).where(qtx.transactionNo.eq(transactioNo)
                .and(qtx.transactionType.eq(transaction))).exists();
    }

    @Override
    public LoseTransactionResultList findAllLoseTransactions(PosLoseTransactionSearchDto pageSort) {
        QGiftCardTransaction qtx = QGiftCardTransaction.giftCardTransaction;
        QGiftCardTransactionItem qtxi = QGiftCardTransactionItem.giftCardTransactionItem;
        QGiftCardTransaction qrtx = new QGiftCardTransaction("reftx");

        BooleanExpression predicates = BooleanExpression.allOf(qtx.loseTransaction.isTrue(),
                pageSort.createSearchExpression());
        final JPAQuery query = new JPAQuery(em).from(qtx)
                .leftJoin(qtx.refTransaction, qrtx);

        // if search has giftCardNo
        if (StringUtils.isNotBlank(pageSort.getGiftCardNo())) {
            query.join(qtx.transactionItems, qtxi);
        }

        // apply predicate - it safe due to we have here a default predicate for loseTxn column
        query.where(predicates);

        long totalCount = query.count();
        pageSort.createSearchExpression();
        final PagingParam pagination = pageSort.getPagination();
        Map<String, Boolean> order = pagination.getOrder();

        // sorting from ui to model convertion
        for (Entry<String, String> entry : VIEW_MODEL_PROP_MAPPING.entrySet()) {
            final String viewProperty = entry.getKey();
            if (order.containsKey(viewProperty)) {
                Boolean ordering = order.get(viewProperty);
                order.remove(viewProperty);
                order.put(entry.getValue(), ordering);
            }
        }

        // Work-around for proper date object sorting
        for (String key : pagination.getOrder().keySet()) {
            if (ArrayUtils.contains(DATE_FIELDS, key)) {
                pagination.getDataTypes().put(key, DataType.DATE);
            }
        }

        SpringDataPagingUtil.INSTANCE.applyPagination(query, pagination, GiftCardTransaction.class);
        final List<PosLoseTxForm> content = query.list(Projections.bean(PosLoseTxForm.class,
                qtx.createUser.as("createdBy"), qtx.created.as("bookedDate"),
                qtx.merchantId, qtx.terminalId, qtx.cashierId,
                qtx.transactionType, qtx.transactionNo, qtx.transactionDate,
                qrtx.transactionNo.as("refTransactionNo")));

        // fill Store as merchant for view presentation
        // TODO: remove this if Store mapping have change and update the query
        this.fillStore(content);

        return new LoseTransactionResultList(content, totalCount,
                pagination.getPageNo(), pagination.getPageSize());
    }

    private void fillStore(List<PosLoseTxForm> list) {
        for (PosLoseTxForm item : list) {
            Store merchant = storeRepo.findByCode(item.getMerchantId());
            item.setMerchant(merchant);
        }
    }

    @Override
    public GiftCardTransaction findLoseTransactionByTransactionNo(String transactioNo) {
        QGiftCardTransaction qtx = QGiftCardTransaction.giftCardTransaction;
        return new JPAQuery(em).from(qtx)
                .where(qtx.loseTransaction.isTrue()
                        .and(qtx.transactionNo.eq(transactioNo)))
                .singleResult(qtx);
    }

    @Override
    public Map<List<?>, List<PosGcTransaction>> findMissingPosGcTransactions() {
        QPosGcTransaction posGcTxn = QPosGcTransaction.posGcTransaction;
        QGiftCardTransactionItem gcTxnItem = QGiftCardTransactionItem.giftCardTransactionItem;
        QGiftCardInventory gcInventory = QGiftCardInventory.giftCardInventory;
        QGiftCardTransaction gcTxn = QGiftCardTransaction.giftCardTransaction;

        JPAQuery query = new JPAQuery(em);
        ListSubQuery<PosGcTransaction> subquery = new JPASubQuery().from(posGcTxn, gcTxnItem)
                .where(BooleanExpression.allOf(
                                posGcTxn.gcServerType.eq(PosGcTransaction.ServerType.MMS),
                                posGcTxn.cardNumber.eq(gcTxnItem.giftCard.barcode),
                                posGcTxn.txId.eq(gcTxnItem.transaction.transactionNo))).list(posGcTxn);
        return query.from(posGcTxn).where(posGcTxn.gcServerType.eq(ServerType.MMS).and(posGcTxn.notIn(subquery)))
                .transform(GroupBy.groupBy(posGcTxn.txId, posGcTxn.requestType, posGcTxn.transactionDate)
                        .as(GroupBy.list(posGcTxn)));
    }

    @Override
    public Character findPosGcTransactionType(String transactionId) {
        QPosGcTransaction posGcTxn = QPosGcTransaction.posGcTransaction;
        return new JPAQuery(em).from(posGcTxn).where(posGcTxn.txId.eq(transactionId))
                .list(posGcTxn.requestType).get(0);
    }

    @Override
    @Transactional
    public long tagAffiliatesTransactionAsPosted(YearMonth yearMonth) {
        QGiftCardTransaction qtx = QGiftCardTransaction.giftCardTransaction;
        QPeopleSoftStore qps = QPeopleSoftStore.peopleSoftStore;

        JPAUpdateClause updateClause = new JPAUpdateClause(em, qtx)
                .set(qtx.claimedByAffiliates, Boolean.TRUE)
                .where(qtx.transactionType.in(ArrayUtils.addAll(TXN_SALES_TYPES, TXN_REDEMPTION_TYPES))
                        .and(qtx.transactionDate.yearMonth().stringValue()
                                .eq(yearMonth.toString(YEAR_MONTH_FORMAT)))
                        .and(qtx.salesType.isNull().or(qtx.salesType.eq(GiftCardSalesType.B2C)))
                        .and(qtx.peoplesoftStoreMapping.in(
                                        new JPASubQuery().from(qps)
                                        .where(qps.businessUnit.code.ne("ID030")).list(qps)))
                        .and(qtx.claimedByAffiliates.isFalse()));
        return updateClause.execute();
    }

    @Override
    @Transactional(readOnly = true)
    public List<AffiliateDto> getBusinessUnitTransactionTypeAmount(YearMonth yearMonth) {
        QPeopleSoftStore qs = QPeopleSoftStore.peopleSoftStore;
        QBusinessUnitPsoftConfig qbu = QBusinessUnitPsoftConfig.businessUnitPsoftConfig;
        QGiftCardTransaction qtx = QGiftCardTransaction.giftCardTransaction;
        QGiftCardTransactionItem qtxi = QGiftCardTransactionItem.giftCardTransactionItem;
        QGiftCardInventory qgc = QGiftCardInventory.giftCardInventory;
        QLookupDetail qld = QLookupDetail.lookupDetail;

        BooleanBuilder booleanBuilder = new BooleanBuilder(qtx.transactionDate.yearMonth().stringValue()
                .eq(yearMonth.toString(YEAR_MONTH_FORMAT))
                .and(qtx.peoplesoftStoreMapping.businessUnit.code.ne("ID030"))
                .and(qtx.claimedByAffiliates.isFalse()
                        .and(qld.in(new JPASubQuery().from(qbu).list(qbu.businessUnit)))));

        JPAQuery claimQuery = new JPAQuery(em).from(qtx)
                .join(qtx.transactionItems, qtxi)
                .join(qtxi.giftCard, qgc)
                .join(qtx.peoplesoftStoreMapping, qs)
                .join(qs.businessUnit, qld)
                .groupBy(qld.code);

        List<AffiliateDto> result = claimQuery.where(
                qtx.transactionType.in(TXN_SALES_TYPES)
                .and(qgc.salesType.eq(GiftCardSalesType.B2C))// Include only all B2C sales
                .and(booleanBuilder.getValue()))
                .list(Projections.constructor(AffiliateDto.class, qld.code,
                                Expressions.stringTemplate(String.format("'%s'", TransactionType.AFFILIATE_CLAIM.name())),
                                qtxi.transactionAmount.sum()));

        qs = QPeopleSoftStore.peopleSoftStore;
        qbu = QBusinessUnitPsoftConfig.businessUnitPsoftConfig;
        qtx = QGiftCardTransaction.giftCardTransaction;
        qtxi = QGiftCardTransactionItem.giftCardTransactionItem;
        qgc = QGiftCardInventory.giftCardInventory;
        qld = QLookupDetail.lookupDetail;

        JPAQuery redemptionQuery = new JPAQuery(em).from(qtx)
                .join(qtx.transactionItems, qtxi)
                .join(qtxi.giftCard, qgc)
                .join(qtx.peoplesoftStoreMapping, qs)
                .join(qs.businessUnit, qld)
                .groupBy(qld.code);

        List<AffiliateDto> result2 = redemptionQuery.where(
                qtx.transactionType.in(TXN_REDEMPTION_TYPES)
                .and(qtx.transactionDate.yearMonth().stringValue()
                        .eq(yearMonth.toString(YEAR_MONTH_FORMAT))
                        .and(qtx.peoplesoftStoreMapping.businessUnit.code.ne("ID030"))
                        .and(qtx.claimedByAffiliates.isFalse()
                                .and(qld.in(new JPASubQuery().from(qbu).list(qbu.businessUnit))))))
                .list(Projections.constructor(AffiliateDto.class, qld.code,
                                Expressions.stringTemplate(String.format("'%s'", TransactionType.AFFILIATE_REDEMPTION.name())),
                                qtxi.transactionAmount.sum()));

        result.addAll(result2);
        return result;
    }

    @Override
    public Double getTransactionAmount(String transactionNo, GiftCardSaleTransaction txType, String barcode) {
        QGiftCardTransaction qtx =  QGiftCardTransaction.giftCardTransaction;
        QGiftCardTransactionItem qti = QGiftCardTransactionItem.giftCardTransactionItem;
        
        Double total = new JPAQuery(em)
                .from(qtx)
                .join(qtx.transactionItems, qti)
                .where(qtx.transactionNo.eq(transactionNo)
                        .and(qtx.transactionType.eq(txType)
                                .and(qti.giftCard.barcode.eq(barcode))))
                .uniqueResult(qti.transactionAmount.sum());
        return total;
    }
}
