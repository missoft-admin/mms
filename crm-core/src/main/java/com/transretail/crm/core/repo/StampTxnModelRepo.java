package com.transretail.crm.core.repo;

import com.transretail.crm.common.repo.CrmQueryDslPredicateExecutor;
import com.transretail.crm.core.entity.StampTxnModel;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @author Mike de Guzman
 */
@Repository
public interface StampTxnModelRepo extends CrmQueryDslPredicateExecutor<StampTxnModel, Long> {

    List<StampTxnModel> findByTransactionHeader_TransactionNo(String transactionNo);

}
