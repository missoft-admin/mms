package com.transretail.crm.report.template.impl;

import static org.junit.Assert.assertEquals;

import java.util.List;
import java.util.Map;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.joda.time.LocalDate;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.google.common.collect.Maps;
import com.transretail.crm.core.entity.lookup.LookupDetail;
import com.transretail.crm.core.entity.lookup.LookupHeader;
import com.transretail.crm.core.repo.LookupDetailRepo;
import com.transretail.crm.core.repo.LookupHeaderRepo;
import com.transretail.crm.core.service.CodePropertiesService;
import com.transretail.crm.giftcard.entity.CardVendor;
import com.transretail.crm.giftcard.entity.GiftCardInventory;
import com.transretail.crm.giftcard.entity.GiftCardOrder;
import com.transretail.crm.giftcard.entity.ProductProfile;
import com.transretail.crm.giftcard.entity.support.GiftCardOrderStatus;
import com.transretail.crm.giftcard.entity.support.ProductProfileStatus;
import com.transretail.crm.giftcard.repo.CardVendorRepo;
import com.transretail.crm.giftcard.repo.GiftCardInventoryRepo;
import com.transretail.crm.giftcard.repo.GiftCardOrderRepo;
import com.transretail.crm.giftcard.repo.ProductProfileRepo;
import com.transretail.crm.giftcard.service.GiftCardInventoryService;
import com.transretail.crm.giftcard.service.ProductProfileService;
import com.transretail.crm.report.template.impl.GiftCardManufactureCostReport.ManufactureOrderBean;
import com.transretail.crm.report.template.impl.GiftCardManufactureCostReport.ReportBean;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath*:/META-INF/spring/applicationContext*.xml",
    "/META-INF/spring/applicationContext-test.xml"})
public class GiftCardManufactureCostReportTest {
	@PersistenceContext
    private EntityManager em;
	@Autowired
	private ProductProfileService productProfileService;
	@Autowired
	private ProductProfileRepo productProfileRepo;
	@Autowired
	private GiftCardInventoryService giftCardInventoryService;
	@Autowired
	private CardVendorRepo cardVendorRepo;
	@Autowired
	private GiftCardOrderRepo gcOrderRepo;
	@Autowired
	private GiftCardInventoryRepo gcInventoryRepo;
	@Autowired
	private GiftCardManufactureCostReport instance;
	
	private ProductProfile profile1, profile2;
	private DateTimeFormatter dateFormat = DateTimeFormat.forPattern("dd-MM-yyyy");
	
	@Autowired
	private CodePropertiesService codePropertiesService;
	@Autowired
	private LookupHeaderRepo lookupHeaderRepo;
	@Autowired
	private LookupDetailRepo lookupDetailRepo;
	
	@Before
	public void setUp() {
		setUpProductProfile();
		setUpManufactureOrder();
	}
	
	private void setUpProductProfile() {
		
		LookupHeader hdr = new LookupHeader(codePropertiesService.getProductProfileFaceValue());
		hdr = lookupHeaderRepo.saveAndFlush(hdr);

		LookupDetail dtl = new LookupDetail("PPFV1");
		dtl.setDescription("100000");
		dtl.setHeader(hdr);
		dtl = lookupDetailRepo.saveAndFlush(dtl);
		
		profile1 = new ProductProfile();
		profile1.setProductCode("000000000001");
		profile1.setProductDesc("profile 1");
		profile1.setFaceValue(dtl);
		profile1.setCardFee(0.0);
		profile1.setMaxAmount(100L);
		profile1.setEffectiveMonths(10L);
		profile1.setUnitCost(100D);
		profile1.setStatus(ProductProfileStatus.APPROVED);
		profile1 = productProfileRepo.save(profile1);
		
		profile2 = new ProductProfile();
		profile2.setProductCode("000000000002");
		profile2.setProductDesc("profile 2");
		profile2.setFaceValue(dtl);
		profile2.setCardFee(0.0);
		profile2.setMaxAmount(100L);
		profile2.setEffectiveMonths(10L);
		profile2.setUnitCost(150D);
		profile2.setStatus(ProductProfileStatus.APPROVED);
		profile2 = productProfileRepo.save(profile2);
	}
	
	/**
	 * Quantity Ordered:
	 * 	profile1 - 50
	 * 	profile2 - 50
	 * Quantity Recieved:
	 * 	profile1 - 20 (as of 01-02-2014)
	 * 	profiel2 - 20 (10 on 01-02-2014, 10 on 01-03-2014)
	 */
	private void setUpManufactureOrder() {
		CardVendor vendor = new CardVendor();
		vendor.setFormalName("test");
		vendor.setMailAddress("test");
		vendor.setEmailAddress("test@test.com");
		vendor.setEnabled(true);
		vendor.setRegion("test");
		vendor.setDefaultVendor(true);
		vendor = cardVendorRepo.saveAndFlush(vendor);
		
		GiftCardOrder gcOrder = new GiftCardOrder();
		gcOrder.setMoNumber("1");
		gcOrder.setMoDate(LocalDate.parse("01-01-2014", dateFormat));
		gcOrder.setPoDate(LocalDate.parse("01-01-2014", dateFormat));
		gcOrder.setPoNumber("1");
		gcOrder.setVendor(vendor);
		gcOrder.setStatus(GiftCardOrderStatus.APPROVED);
		gcOrder = gcOrderRepo.saveAndFlush(gcOrder);
		
		for(int i = 1; i <= 50; i++) {
			GiftCardInventory gcInventory = new GiftCardInventory();
			gcInventory.setOrder(gcOrder);
			gcInventory.setOrderDate(gcOrder.getMoDate());
			gcInventory.setProfile(profile1);
			gcInventory.setProductCode(profile1.getProductCode());
			gcInventory.setProductName(profile1.getProductDesc());
			gcInventory.setSeqNo(i);
			gcInventory.setBatchNo(1);
			gcInventory.setSeries(10000L + i);
			gcInventory.setBarcode(String.format("%05d", i));
			if(i <= 10) {
				gcInventory.setDeliveryReceipt("1");
				gcInventory.setReceiveDate(LocalDate.parse("01-02-2014", dateFormat));
			}
			else if(i >= 41) {
				gcInventory.setDeliveryReceipt("2");
				gcInventory.setReceiveDate(LocalDate.parse("01-02-2014", dateFormat));
			}
			gcInventoryRepo.save(gcInventory);
		}
		
		for(int i = 51; i <= 100; i++) {
			GiftCardInventory gcInventory = new GiftCardInventory();
			gcInventory.setOrder(gcOrder);
			gcInventory.setOrderDate(gcOrder.getMoDate());
			gcInventory.setProfile(profile2);
			gcInventory.setProductCode(profile2.getProductCode());
			gcInventory.setProductName(profile2.getProductDesc());
			gcInventory.setSeqNo(i);
			gcInventory.setBatchNo(1);
			gcInventory.setSeries(10000L + i);
			gcInventory.setBarcode(String.format("%05d", i));
			if(i <= 60) {
				gcInventory.setDeliveryReceipt("3");
				gcInventory.setReceiveDate(LocalDate.parse("01-02-2014", dateFormat));
			}
			else if(i >= 91) {
				gcInventory.setDeliveryReceipt("4");
				gcInventory.setReceiveDate(LocalDate.parse("01-03-2014", dateFormat));
			}
			gcInventoryRepo.save(gcInventory);
		}
	}
	
	@Test
	public void test() {
		Map<String, Object> map = Maps.newHashMap();
		List<ReportBean> datasource = instance.createDatasource(LocalDate.parse("01-02-2014", dateFormat), 
				LocalDate.parse("01-02-2014", dateFormat), map);
		assertEquals(2, datasource.size());
		
		for(ReportBean bean : datasource) {
			if(bean.getCode().equals(profile1.getProductCode())) {
				assertEquals(2, bean.getManufactureOrders().size());
				assertEquals(2000D, bean.getSubTotal(), 0D);
				for(ManufactureOrderBean i : bean.getManufactureOrders()) {
					assertEquals(1000D, i.getCardCost(), 0D);
					assertEquals(10L, i.getQuantityCosting().longValue());
					assertEquals(50L, i.getQuantityOrdered().longValue());
					assertEquals(20L, i.getQuantityReceived().longValue());
				}
			}
			else {
				assertEquals(1, bean.getManufactureOrders().size());
				assertEquals(1500D, bean.getSubTotal(), 0D);
				for(ManufactureOrderBean i : bean.getManufactureOrders()) {
					assertEquals(1500D, i.getCardCost(), 0D);
					assertEquals(10L, i.getQuantityCosting().longValue());
					assertEquals(50L, i.getQuantityOrdered().longValue());
					assertEquals(20L, i.getQuantityReceived().longValue());
				}
			}
		}
		
		assertEquals(3500D, (Double) map.get("TOTAL_COST_VALUE"), 0D);
	}
}
