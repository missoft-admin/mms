package com.transretail.crm.report.template;

import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

/**
 *
 * @author Allan G. Ramirez (agramirez@exist.com)
 * @author Monte Cillo Co (mco@exist.com)
 */
public final class FilterField {

    private final FilterFieldType type;
    private final String label;
    private final String name;
    private final String placeHolder;
    private final boolean required;
    private final Map<String, String> selectValues;
    private final Set<Entry<String, String>> dropdownValues;
    private DateTypeOption dateOption;
    private final boolean allowFutureDates;

    FilterField(FilterFieldType type, String label, String name, String placeHolder, boolean required,
            Map<String, String> selectValues, Set<Entry<String, String>> dropdownValues, DateTypeOption dateOption,
            boolean allowFutureDates) {
        this.type = type;
        this.label = label;
        this.name = name;
        this.placeHolder = placeHolder;
        this.required = required;
        this.selectValues = selectValues;
        this.dropdownValues = dropdownValues;
        this.dateOption = dateOption;
        this.allowFutureDates = allowFutureDates;
    }

    public static enum DateTypeOption {

        PLAIN,
        YEAR_MONTH,
        YEAR
    }

    /**
     * Create Input Field with label as placeHolder and not required
     *
     * @param name
     * @param label
     * @return an instance of FilterField
     */
    public static final FilterField createInputField(String name, String label) {
        return new FilterFieldBuilder().type(FilterFieldType.INPUT).name(name).label(label)
                .placeHolder(label).required(false).createFilterField();
    }

    public static final FilterField createInputField(String name, String label, boolean required) {
        return new FilterFieldBuilder().type(FilterFieldType.INPUT).name(name).label(label)
                .placeHolder(label).required(required).createFilterField();
    }

    public static final FilterField createInputField(String name, String label, String placeHolder) {
        return new FilterFieldBuilder().type(FilterFieldType.INPUT).name(name)
                .label(label).placeHolder(placeHolder).required(false).createFilterField();
    }

    public static final FilterField createInputField(String name, String label, String placeHolder, boolean required) {
        return new FilterFieldBuilder().type(FilterFieldType.INPUT).name(name).label(label)
                .placeHolder(placeHolder).required(required).createFilterField();
    }

    public static final FilterField createDateField(String name, String label, String placeHolder) {
        return createDateField(name, label, placeHolder, DateTypeOption.PLAIN);
    }

    public static final FilterField createDateField(String name, String label, String placeHolder, DateTypeOption dateOption) {
        return createDateField(name, label, placeHolder, dateOption, false, false);
    }

    public static final FilterField createDateField(String name, String label, String placeHolder, boolean required) {
        return createDateField(name, label, placeHolder, DateTypeOption.PLAIN, required, false);
    }
    
    public static final FilterField createDateField(String name, String label, String placeHolder, DateTypeOption dateOption, boolean required) {
    	return createDateField(name, label, placeHolder, dateOption, required, false);
    }

    public static final FilterField createDateField(String name, String label, String placeHolder, DateTypeOption dateOption, boolean required, boolean allowFutureDates) {
        FilterField dateFilterField = new FilterFieldBuilder().type(FilterFieldType.DATE).name(name).label(label)
                .placeHolder(label).required(required).dateOption(dateOption).allowFutureDates(allowFutureDates).createFilterField();
        return dateFilterField;
    }
    
    public static final FilterField createDateTimeField(String name, String label, String placeHolder, boolean required, boolean allowFutureDates) {
        FilterField dateFilterField = new FilterFieldBuilder().type(FilterFieldType.DATE_TIME).name(name).label(label)
                .placeHolder(label).required(required).dateOption(DateTypeOption.PLAIN).allowFutureDates(allowFutureDates).createFilterField();
        return dateFilterField;
    }
    
    public static final FilterField createDateTimeField(String name, String label, String placeHolder) {
        return createDateTimeField(name, label, placeHolder, false, false);
    }

    public static final FilterField createDateToField(String name, String label, String placeHolder) {
        return createDateToField(name, label, placeHolder, DateTypeOption.PLAIN);
    }

    public static final FilterField createDateToField(String name, String label, String placeHolder, DateTypeOption dateOption) {
        return new FilterFieldBuilder().type(FilterFieldType.DATE_TO).name(name).label(label)
                .placeHolder(label).required(false).dateOption(dateOption).createFilterField();
    }

    public static final FilterField createDateFromField(String name, String label, String placeHolder) {
        return createDateFromField(name, label, placeHolder, DateTypeOption.PLAIN);
    }

    public static final FilterField createDateFromField(String name, String label, String placeHolder, DateTypeOption dateOption) {
        return new FilterFieldBuilder().type(FilterFieldType.DATE_FROM).name(name).label(label)
                .placeHolder(label).required(false).dateOption(dateOption).createFilterField();
    }

    public static final FilterField createDropdownField(String name, String label, Map<String, String> selectValues) {
        return new FilterFieldBuilder().type(FilterFieldType.DROPDOWN).name(name).label(label)
                .placeHolder(label).required(false).selectValues(selectValues).createFilterField();
    }
    
    public static final FilterField createDropdownField(String name, String label, Map<String, String> selectValues, boolean required) {
        return new FilterFieldBuilder().type(FilterFieldType.DROPDOWN).name(name).label(label)
                .placeHolder(label).required(required).selectValues(selectValues).createFilterField();
    }

    public static final FilterField createEntrySetDropdownField(String name, String label, Set<Entry<String, String>> dropdownValues) {
        return new FilterFieldBuilder().type(FilterFieldType.DROPDOWN).name(name).label(label).placeHolder(label)
                .dropdownValues(dropdownValues).createFilterField();
    }

    public static final FilterField createMultipleSelectField(String name, String label, Map<String, String> selectValues) {
        return new FilterFieldBuilder().type(FilterFieldType.SELECT_MULTIPLE).name(name).label(label)
                .placeHolder(label).required(false).selectValues(selectValues).createFilterField();
    }

    public static final FilterField createToggleField(String name, String label) {
        return new FilterFieldBuilder().type(FilterFieldType.TOGGLE).name(name).label(label)
                .placeHolder(label).required(false).createFilterField();

    }

    public FilterFieldType getType() {
        return type;
    }

    public String getLabel() {
        return label;
    }

    public boolean isRequired() {
        return required;
    }

    public String getName() {
        return name;
    }

    public String getplaceHolder() {
        return placeHolder;
    }

    public Map<String, String> getSelectValues() {
        if (selectValues != null) {
            Map<String, String> copy = new LinkedHashMap<String, String>();
            copy.putAll(selectValues);
            return copy;
        }
        return null;
    }

    public Set<Entry<String, String>> getDropdownValues() {
        if (dropdownValues != null) {
            Set<Entry<String, String>> copy = new LinkedHashSet<Map.Entry<String, String>>();
            copy.addAll(dropdownValues);
            return copy;
        }
        return null;
    }

    public DateTypeOption getDateOption() {
        return dateOption;
    }

    public boolean isAllowFutureDates() {
		return allowFutureDates;
	}

	@Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((name == null) ? 0 : name.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        FilterField other = (FilterField) obj;
        if (name == null) {
            if (other.getName() != null) {
                return false;
            }
        } else if (!name.equals(other.getName())) {
            return false;
        }
        return true;
    }

}
