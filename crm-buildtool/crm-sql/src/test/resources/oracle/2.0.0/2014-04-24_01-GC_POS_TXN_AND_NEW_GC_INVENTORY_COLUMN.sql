create table CRM_GC_POS_TXN (
        ID number(19,0) not null,
        CASHIER_ID varchar2(100 char) not null,
        DATE_CREATED timestamp,
        MERCHANT_ID varchar2(100 char) not null,
        REFERENCE_NO varchar2(100 char) not null,
        TERMINAL_ID varchar2(100 char) not null,
        POS_TXN_NO varchar2(100 char),
        POS_TXN_TIME timestamp,
        TX_TYPE varchar2(100 char) not null,
        primary key (ID)
    );

    create table CRM_GC_POS_TXN_DTL (
        id number(19,0) not null,
        CANCELLED number(1,0),
        FOLDED_AMOUNT double precision,
        CARD_NO varchar2(20 char) not null,
        TXN_ID number(19,0) not null,
        primary key (id)
    );

    alter table CRM_GC_POS_TXN_DTL 
        add constraint FK_djgfya16qdc2yx8rjvssn2ds5 
        foreign key (CARD_NO) 
        references CRM_GC_INVENTORY (BARCODE);

    alter table CRM_GC_POS_TXN_DTL 
        add constraint FK_2lsiv41oa8r8h6r6jybpu6pd4 
        foreign key (TXN_ID) 
        references CRM_GC_POS_TXN;

ALTER TABLE CRM_GC_INVENTORY ADD REDEMPTION_DATE TIMESTAMP;
ALTER TABLE CRM_GC_INVENTORY ADD REFUNDABLE number(1,0) NULL;