package com.transretail.crm.giftcard.entity;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.transretail.crm.core.entity.lookup.LookupDetail;
import com.transretail.crm.core.entity.support.CustomAuditableEntity;

/**
 *
 */
@Entity
@Table(name = "CRM_B2B_EXH_PAYMENTS")
public class B2BExhibitionPayments extends CustomAuditableEntity<Long> implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "TYPE")
	private LookupDetail paymentType;
	@Column(name = "AMT")
	private BigDecimal total;
	@Column(name = "DETAILS")
	private String details;
	@ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "EXH", nullable = false)
	private B2BExhibition exh;
	public LookupDetail getPaymentType() {
		return paymentType;
	}
	public void setPaymentType(LookupDetail paymentType) {
		this.paymentType = paymentType;
	}
	public BigDecimal getTotal() {
		return total;
	}
	public void setTotal(BigDecimal total) {
		this.total = total;
	}
	public String getDetails() {
		return details;
	}
	public void setDetails(String details) {
		this.details = details;
	}
	public B2BExhibition getExh() {
		return exh;
	}
	public void setExh(B2BExhibition exh) {
		this.exh = exh;
	}
	
	
}
