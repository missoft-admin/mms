<%@ include file="../../common/taglibs.jsp" %>


<div id="content_adjustmentForm" class="modal hide  nofly modal-dialog">

  <c:if test="${not empty overrideType and overrideType == member }">
    <spring:message var="lblAccountId" code="points_prop_member_id"/>
  </c:if>
  <c:if test="${not empty overrideType and overrideType == employee }">
    <spring:message var="lblAccountId" code="points_prop_member_empid"/>
  </c:if>

  <div class="modal-content">
  <c:url var="url_adjustment" value="/points/override" />
  <c:url var="url_action_process" value="/points/override/process" />
  <c:url var="url_action" value="${action}" />
  <form:form id="adjustmentForm" name="adjustment" modelAttribute="pointsAdjustment" action="${url_action}" class="modal-form form-horizontal" data-url="${url_adjustment}" data-action="${url_action_process}">

    <div class="modal-header">
      <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
      <h4><spring:message code="points_label_override"/></h4>
    </div>

    <div class="modal-body">
      <div class="hide alert alert-error" id="content_error"><button type="button" class="close" onclick="$(this).parent().hide();">&times;</button><div><form:errors path="*"/></div></div>


      <form:hidden path="id" />

      <sec:authorize ifAnyGranted="ROLE_HRS, ROLE_CSS">
      <div class="view-block">
        <div class="control-group">
          <label for="accountId" class="control-label" id="label_accountId"><c:out value="${lblAccountId}" /></label>
          <div class="controls"><span class="control-label data-content-01"><c:out value="${pointsAdjustment.memberModel.accountId}" /></span></div>
        </div>

        <div class="pull-left">
	        <div class="control-group">
	          <label for="storeCode" class="control-label"><spring:message code="points_label_storeloc" /></label>
	          <div class="controls"><span class="control-label data-content-01"><c:out value="${pointsAdjustment.storeCode}" /></span></div>
	        </div>
	        <div class="control-group">
	          <label for="transactionNo" class="control-label"><spring:message code="points_prop_txnno" /></label>
	          <div class="controls"><span class="control-label data-content-01"><c:out value="${pointsAdjustment.transactionNo}" /></span></div>
	        </div>
	        <div class="control-group">
	          <label for="transactionPoints" class="control-label"><spring:message code="points_label_adjustment" /></label>
	          <div class="controls">
              <span class="control-label data-content-01">
                <fmt:formatNumber value="${pointsAdjustment.transactionPoints}" maxFractionDigits="0" minFractionDigits="0"/>
              </span>
            </div>
	        </div>
        </div>

        <div class="pull-left">
          <div class="control-group">
            <label for="transactionDateTime" class="control-label"><spring:message code="points_prop_txndate" /></label>
            <div class="controls">
              <span class="control-label data-content-01">
                <fmt:formatDate value="${pointsAdjustment.transactionDateTime}" pattern="dd-MM-yyyy" />
              </span>
            </div>
          </div>
	        <div class="control-group">
	          <label for="transactionAmount" class="control-label"><spring:message code="points_prop_txnamount" /></label>
	          <div class="controls"><span class="control-label data-content-01"><fmt:formatNumber value="${pointsAdjustment.transactionAmount}" pattern="###,###.00" /></span></div>
	        </div>
	        <div class="control-group">
	          <label for="comment" class="control-label"><spring:message code="points_label_requestcomments" /></label>
	          <div class="controls"><span class="control-label data-content-01"><c:out value="${pointsAdjustment.comment}" /></span></div>
	        </div>
        </div>
        <div class="clearfix"></div>

      </div>

      <div class="control-group">
        <label for="approvalReason" class="control-label"><spring:message code="points_prop_approvalreason" /></label>
        <div class="controls"><form:textarea id="approvalReason" path="approvalReason" /></div>
      </div>
      </sec:authorize>

    </div>

    <div class="modal-footer">
      <sec:authorize ifAnyGranted="ROLE_HRS, ROLE_CSS">
	      <!--<input id="approveButton" data-value="${statusApprove}" class="btn btn-primary statusButton" type="submit" value="<spring:message code="label_approve" />" />
	      <input id="rejectButton" data-value="${statusReject}" class="btn btn-primary statusButton" type="submit" value="<spring:message code="label_reject" />" />-->
	      <button type="button" id="approveButton" data-value="${statusApprove}"  class="btn btn-primary statusButton"><spring:message code="label_approve" /></button>
        <button type="button" id="rejectButton" data-value="${statusReject}" data-dismiss="modal" class="btn btn-primary statusButton"><spring:message code="label_reject" /></button>
	      <form:hidden path="status" id="status" />
      </sec:authorize>

      <!--<input id="cancelButton" class="btn " type="button" value="<spring:message code="label_cancel" />" data-dismiss="modal" />-->
      <button type="button" id="formCancel" data-dismiss="modal" class="btn btn-default"><spring:message code="label_cancel"/></button>
      <div class="clearfix"></div>
    </div>

  </form:form>
  </div>


  <script src='<c:url value="/js/bootstrap/bootstrap-datepicker.js"/>' type='text/javascript'><![CDATA[&nbsp; ]]></script>
  <link href="<c:url value="/css/bootstrap/datepicker.css" />" rel="stylesheet" />
  <script src="<spring:url value="/js/jquery/jquery.numberInput.js" />" ></script>
  <script src='<c:url value="/js/viewspecific/points/override/adjustmentForm.js"/>' type='text/javascript'><![CDATA[&nbsp; ]]></script>


</div>