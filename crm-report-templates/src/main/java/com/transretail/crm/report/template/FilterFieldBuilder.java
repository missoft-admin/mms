package com.transretail.crm.report.template;

import com.transretail.crm.report.template.FilterField.DateTypeOption;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

public class FilterFieldBuilder {

    private FilterFieldType type;
    private String label;
    private String name;
    private String placeHolder;
    private boolean required;
    private Map<String, String> selectValues;
    private Set<Entry<String, String>> dropdownValues;
    private DateTypeOption dateOption;
    private boolean allowFutureDates;

    public FilterFieldBuilder() {
    }

    public FilterFieldBuilder type(FilterFieldType type) {
        this.type = type;
        return this;
    }

    public FilterFieldBuilder label(String label) {
        this.label = label;
        return this;
    }

    public FilterFieldBuilder name(String name) {
        this.name = name;
        return this;
    }

    public FilterFieldBuilder placeHolder(String placeHolder) {
        this.placeHolder = placeHolder;
        return this;
    }

    public FilterFieldBuilder required(boolean required) {
        this.required = required;
        return this;
    }

    public FilterFieldBuilder selectValues(Map<String, String> selectValues) {
        this.selectValues = selectValues;
        return this;
    }

    public FilterFieldBuilder dropdownValues(Set<Entry<String, String>> dropdownValues) {
        this.dropdownValues = dropdownValues;
        return this;
    }

    public FilterFieldBuilder dateOption(DateTypeOption dateOption) {
        this.dateOption = dateOption;
        return this;
    }
    
    public FilterFieldBuilder allowFutureDates(boolean allowFutureDates) {
        this.allowFutureDates = allowFutureDates;
        return this;
    }

    public FilterField createFilterField() {
        return new FilterField(type, label, name, placeHolder, required, selectValues, dropdownValues, dateOption, allowFutureDates);
    }

}
