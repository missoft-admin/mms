package com.transretail.crm.core.service.impl;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.collect.Lists;
import com.mysema.query.BooleanBuilder;
import com.mysema.query.types.expr.BooleanExpression;
import com.transretail.crm.common.enums.MessageType;
import com.transretail.crm.common.service.dto.response.ResultList;
import com.transretail.crm.common.service.dto.rest.ProductRestDto;
import com.transretail.crm.common.service.dto.rest.RestControllerResponse;
import com.transretail.crm.common.service.dto.rest.ReturnMessage;
import com.transretail.crm.common.service.dto.rest.ValidProductsDto;
import com.transretail.crm.common.service.util.SpringDataPagingUtil;
import com.transretail.crm.common.util.DateUtil;
import com.transretail.crm.core.dto.*;
import com.transretail.crm.core.entity.*;
import com.transretail.crm.core.entity.enums.*;
import com.transretail.crm.core.entity.lookup.LookupDetail;
import com.transretail.crm.core.entity.lookup.Product;
import com.transretail.crm.core.repo.*;
import com.transretail.crm.core.security.model.impl.CustomSecurityUserDetailsImpl;
import com.transretail.crm.core.security.service.MD5Service;
import com.transretail.crm.core.security.util.UserUtil;
import com.transretail.crm.core.service.*;
import com.transretail.crm.core.util.CalendarUtil;
import com.transretail.crm.core.util.generator.IdGeneratorService;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.IteratorUtils;
import org.apache.commons.collections.Predicate;
import org.apache.commons.lang3.BooleanUtils;
import org.apache.commons.lang3.StringUtils;
import org.joda.time.DateTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

@Service("promotionService")
@Transactional
public class PromotionServiceImpl implements PromotionService {

    private static final Logger LOGGER = LoggerFactory.getLogger(PromotionServiceImpl.class);
    private static final String URL_MEMGRP_WS = "/validatemembergroup/groupId/accountId/validationKey";
    @Autowired
    private CodePropertiesService codePropertiesService;
    @Autowired
    private LookupDetailRepo lookupDetailRepo;
    @Autowired
    private ProgramRepo programRepo;
    @Autowired
    private CampaignRepo campaignRepo;
    @Autowired
    private PromotionRewardRepo promotionRewardRepo;
    @Autowired
    private PromotionRedemptionRepo promotionRedemptionRepo;
    @Autowired
    private PromotionRepo promotionRepo;
    @Autowired
    private ApprovalRemarkRepo approvalRemarkRepo;
    @Autowired
    private MessageSource messageSource;
    @Autowired
    private PosTransactionService posTransactionService;
    @Autowired
    private PointsTxnManagerService pointsManagerService;
    @Autowired
    private TransactionAuditService transactionAuditService;
    @Autowired
    private MemberGroupService memberGroupService;
    @Autowired
    private MemberService memberService;
    @Autowired
    private ProductService productService;
    @Autowired
    private MemberRepo memberRepo;
    @Autowired
    private MemberGroupRepo memberGroupRepo;
    @Autowired
    private StoreGroupRepo storeGroupRepo;
    @Autowired
    private ProductGroupRepo productGroupRepo;
    @Autowired
    private UserRepo userRepo;
    @Autowired
    private PromotionMemberGroupRepo promotionMemberGroupRepo;
    @Autowired
    private PromotionStoreGroupRepo promotionStoreGroupRepo;
    @Autowired
    private PromotionProductGroupRepo promotionProductGroupRepo;
    @Autowired
    private PromotionPaymentTypeRepo promotionPaymentTypeRepo;
    @Autowired
    private StoreService storeService;
    @Autowired
    private IdGeneratorService customIdGeneratorService;
    @Autowired
    private ApplicationConfigRepo appConfigRepo;
    @Autowired
    private MD5Service md5Service;
    @Autowired
    private StampTxnModelRepo stampTxnModelRepo;
    @Autowired
    private StampTxnHeaderRepo stampTxnHeaderRepo;
    @Autowired
    private StampTxnDetailRepo stampTxnDetailRepo;
    @Autowired
    private StampPromoService stampPromoService;

    public Promotion getPromotion(Long id) {
        return promotionRepo.findOne(id);
    }

    public ValidProductsDto getValidItems(String id, String store) {

        List<ProductRestDto> validProducts = new ArrayList<ProductRestDto>();

        Date today = new Date();

        List<Program> programs = this.getValidPrograms(today);

        MemberModel memberModel = null;

        if (id != null && !id.trim().equals("")) {
            memberModel = memberService.findByAccountId(id);
        }

        for (Program program : programs) {
            List<Campaign> campaigns = this.getValidCampaigns(program, today);

            for (Campaign campaign : campaigns) {
                //retrieve only promotion with RewardType = RWRD005; Exchange Item
                List<Promotion> promotions = this.getValidPromotionsForCampaign(campaign, today, codePropertiesService.getDetailRewardTypeExchangeItem());

                for (Promotion promotion : promotions) {
                    //check if promotion is valid for this store and member
                    //if id is null check all valid items for store

                    if (validateTargetStores(promotion, store)
                            && validateTargetMemberGroups(promotion, id)) {

                        List<PromotionReward> rewards = promotion.getPromotionRewards();

                        if (rewards == null) {
                            break;
                        }

                        for (PromotionReward reward : rewards) {
                            //only return valid products for member
                            if (reward.getFixed() != null && reward.getFixed()) {

                                //if no member, no need to validate. if member exist, check if points is greater than product point
                                if (memberModel == null || (memberModel != null && memberModel.getTotalPoints() >= reward.getBaseFactor())) {
                                    ProductRestDto productRestDto = new ProductRestDto();
                                    productRestDto.setPluId(reward.getProductCode());
                                    productRestDto.setPoints(reward.getBaseFactor());
                                    productRestDto.setPromotionId(promotion.getId());
                                    productRestDto.setPromotionName(promotion.getName());
                                    productRestDto.setPromotionStartDate(promotion.getStartDate());
                                    productRestDto.setPromotionEndDate(promotion.getEndDate());
                                    validProducts.add(productRestDto);
                                }

                            }

                        }

                    }
                }

            }
        }

        ValidProductsDto validProductsDto = new ValidProductsDto();

        validProductsDto.setProductRestDtos(validProducts);
        Double memberPoints = 0d;

        if (memberModel != null) {
            memberPoints = memberModel.getTotalPoints();
        }


        validProductsDto.setMemberPoints(memberPoints);

        return validProductsDto;
    }

    public ProductRestDto getProductPromo(String pluId, String id, String store) {

        ProductRestDto productRestDto = null;

        //TODO if multiple promo exist for product get first?
        Date today = new Date();

        List<Program> programs = this.getValidPrograms(today);
        for (Program program : programs) {
            List<Campaign> campaigns = this.getValidCampaigns(program, today);
            for (Campaign campaign : campaigns) {
                //retrieve only promotion with RewardType = RWRD005; Exchange Item
                List<Promotion> promotions = this.getValidPromotionsForCampaign(campaign, today, codePropertiesService.getDetailRewardTypeExchangeItem());
                for (Promotion promotion : promotions) {
                    //check if promotion is valid for this store and member
                    //if id is null check all valid items for store
                    if (validateTargetStores(promotion, store)
                            && validateTargetMemberGroups(promotion, id)) {
                        List<PromotionReward> rewards = promotion.getPromotionRewards();
                        if (rewards == null) {
                            break;
                        }
                        for (PromotionReward reward : rewards) {
                            if (reward.getFixed() != null && reward.getFixed() && pluId.equalsIgnoreCase(reward.getProductCode())) {
                                productRestDto = new ProductRestDto();
                                productRestDto.setPluId(reward.getProductCode());
                                productRestDto.setPoints(reward.getBaseFactor());
                                productRestDto.setPromotionId(promotion.getId());
                                productRestDto.setPromotionName(promotion.getName());
                                productRestDto.setPromotionStartDate(promotion.getStartDate());
                                productRestDto.setPromotionEndDate(promotion.getEndDate());
                                return productRestDto;
                            }

                        }

                    }
                }

            }
        }


        return productRestDto;
    }

    @Override
    public long countTargetMembers(long id) {
        QMemberModel qMember = QMemberModel.memberModel;
        BooleanExpression loyaltyFilter = qMember.loyaltyCardNo.isNotEmpty().and(qMember.loyaltyCardNo.isNotNull());
        Promotion promo = promotionRepo.findOne(id);
        if (null != promo) {
            if (CollectionUtils.isNotEmpty(promo.getTargetMemberGroups())) {
                List<Long> members = new ArrayList<Long>();
                for (PromotionMemberGroup memGrp : promo.getTargetMemberGroups()) {
                    List<Long> membersPerGrp = memberGroupService.getQualifiedMembers(memGrp.getMemberGroup());
                    if (null != membersPerGrp) {
                        CollectionUtils.addAll(members, membersPerGrp.iterator());
                    }
                }
                if (CollectionUtils.isNotEmpty(members)) {
                    return memberRepo.count(qMember.id.in(members).and(loyaltyFilter));
                }
            } else {
                return memberRepo.count(loyaltyFilter);
            }
        }
        return 0;
    }

    @Override
    public ResultList<MemberDto> getTargetMembers( PromotionSearchDto searchDto ) {
    	Promotion promo = promotionRepo.findOne(searchDto.getId());
    	

    	if ( null != promo ) {
        	QMemberModel qMember = QMemberModel.memberModel;
        	BooleanBuilder exp = new BooleanBuilder();
        	
        	if(promo.getRewardType().getCode().equals(codePropertiesService.getDetailRewardTypeStamp())) {
        		exp.and(qMember.stampUser.isTrue());
        	} else {
        		exp.and(qMember.loyaltyCardNo.isNotEmpty().and( qMember.loyaltyCardNo.isNotNull() ));
        	}
        	
        	BooleanBuilder memFltr = new BooleanBuilder();

    		if ( CollectionUtils.isNotEmpty( promo.getTargetMemberGroups() ) ) {
    			Set<Long> memberSet = new HashSet<Long>();
    			
        		/*StringBuilder memberIds = new StringBuilder();*/
                for (PromotionMemberGroup memGrp : promo.getTargetMemberGroups()) {
                    if (null != memGrp.getMemberGroup() && StringUtils.isNotBlank(memGrp.getMemberGroup().getMembers())) {
                        for (String str : memGrp.getMemberGroup().getMemberList()) {
                            memberSet.add(Long.parseLong(str));
                        }
                    } else {
                        memberSet.addAll(memberGroupService.getQualifiedMembers(memGrp.getMemberGroup()));
                    }
                }


                QMemberModel qMemberModel = QMemberModel.memberModel;
                if (CollectionUtils.isNotEmpty(memberSet)) {
                    List<Long> ids = Lists.newArrayList(memberSet);
                    for (int i = 0; i < memberSet.size(); i += 1000) {
                        int toIndex = i + 1000;
                        if (toIndex > memberSet.size())
                            memFltr.or(qMemberModel.id.in(ids.subList(i, ids.size())));
                        else
                            memFltr.or(qMemberModel.id.in(ids.subList(i, toIndex)));
                    }
                }

        		/*if ( StringUtils.isNotBlank( memberIds.toString() ) ) {
                    MemberGroup memGrp = new MemberGroup();
            		memGrp.setMembers( memberIds.toString() );
            		PageSortDto pageSort = new PageSortDto();
            		pageSort.setPagination( searchDto.getPagination() );
        			return memberGroupService.getQualifiedMembers( memGrp, pageSort, loyaltyFilter );
        		}*/


            }

            Page<MemberModel> page = memberRepo.findAll(exp.and(memFltr), SpringDataPagingUtil.INSTANCE.createPageable(searchDto.getPagination()));
            return new ResultList<MemberDto>(MemberDto.toDtoList(page.getContent()), page.getTotalElements(),
                    searchDto.getPagination().getPageNo(), searchDto.getPagination().getPageSize());
        }

        return new ResultList<MemberDto>(new ArrayList<MemberDto>(), 0, searchDto.getPagination().getPageNo(), searchDto.getPagination().getPageSize());
    }

    @Override
    public PromotionResultList searchPromotion(PromotionSearchDto inDto) {
        Pageable pageable = SpringDataPagingUtil.INSTANCE.createPageable(inDto.getPagination());
        BooleanExpression filter = inDto.createSearchExpression();
        Page<Promotion> page = filter != null ? promotionRepo.findAll(filter, pageable) : promotionRepo.findAll(pageable);
        return new PromotionResultList(convertPromotionToDto((page.getContent())), page.getTotalElements(), page.hasPreviousPage(), page.hasNextPage());
    }

    @Override
    public List<ProgramDto> processSearch(PromotionSearchDto theSearchDto) {
        if (StringUtils.isBlank(theSearchDto.getModelType())) {
            return null;
        }

        List<ProgramDto> theList = null;
        if (theSearchDto.getModelType().equalsIgnoreCase(PromoModelType.PROGRAM.toString())) {
            theList = convertProgramToDto(searchPrograms(theSearchDto), true);
        } else if (theSearchDto.getModelType().equalsIgnoreCase(PromoModelType.CAMPAIGN.toString())) {
            theList = convertProgramToDto(searchProgramsByCampaigns(theSearchDto), true);
        } else if (theSearchDto.getModelType().equalsIgnoreCase(PromoModelType.PROMOTION.toString())) {
            theList = convertProgramToDto(searchProgramsByPromotions(theSearchDto), true);
            //theSearchDto.filterPromoInProgram( theList );
            //theList = filterProgramDtos( convertProgramToDto( searchProgramsByPromotions( theSearchDto ) ), theSearchDto );
            //theList = convertProgramToDto( searchProgramsByPromotions( theSearchDto ) );
        } else {
            theList = getProgramDtos();
        }

        for (ProgramDto programDto : theList) {
            if (CollectionUtils.isNotEmpty(programDto.getCampaigns())) {
                for (CampaignDto campaignDto : programDto.getCampaigns()) {
                    theSearchDto.setCampaign(campaignDto.getId());
                    List<Promotion> promotions = Lists.newArrayList(promotionRepo.findAll(theSearchDto.createSearchExpression()));
                    campaignDto.setPromotions(convertPromotionToDto(promotions));
                }
            }
        }
        return theList;
    }

    @SuppressWarnings("unchecked")
    private List<Program> searchPrograms(PromotionSearchDto theSearchDto) {
        ProgramSearchDto theDto = new ProgramSearchDto();
        theDto.setName(theSearchDto.getName());
        theDto.setDescription(theSearchDto.getDescription());
        return IteratorUtils.toList(programRepo.findAll(theDto.createSearchExpression()).iterator());
    }

    @SuppressWarnings("unchecked")
    public List<Program> searchProgramsByCampaigns(PromotionSearchDto theSearchDto) {
        CampaignSearchDto theDto = new CampaignSearchDto();
        theDto.setName(theSearchDto.getName());
        theDto.setDescription(theSearchDto.getDescription());
        return IteratorUtils.toList(programRepo.find(theDto).iterator());
    }

    @SuppressWarnings("unchecked")
    public List<Program> searchProgramsByPromotions(PromotionSearchDto theSearchDto) {
        List<Program> thePrograms = new ArrayList<Program>(IteratorUtils.toList(programRepo.find(theSearchDto).iterator()));
        return thePrograms;
    }

    @Override
    public List<ApprovalRemarkDto> getRemarkDtos(Long inPromoId) {
        if (null == inPromoId) {
            return null;
        }
        return convertRemarkToDto(approvalRemarkRepo.findByModelTypeAndModelId(ModelType.PROMOTION.toString(), inPromoId.toString()));
    }

    @Override
    public void saveRemarkDto(ApprovalRemarkDto inDto) {
        if (null == inDto || (StringUtils.isNotBlank(inDto.getModelType()) && !inDto.getModelType().equalsIgnoreCase(ModelType.PROMOTION.toString()))
                || StringUtils.isBlank(inDto.getModelId())) {
            return;
        }

        ApprovalRemark theModel = inDto.toModel();
        if (StringUtils.isNotBlank(inDto.getStatus())) {
            theModel.setStatus(lookupDetailRepo.findByCode(inDto.getStatus()));
        }
        theModel.setModelType(ModelType.PROMOTION.toString());
        approvalRemarkRepo.save(theModel);
        Promotion thePromotion = promotionRepo.findOne(Long.valueOf(inDto.getModelId()));
        thePromotion.setStatus(theModel.getStatus());
        promotionRepo.save(thePromotion);
        inDto.setId(theModel.getId());
        inDto.setDispStatus(theModel.getStatus().getDescription());
        if ( StringUtils.equalsIgnoreCase( thePromotion.getRewardType().getCode(), codePropertiesService.getDetailRewardTypeStamp() )
        		&& StringUtils.equalsIgnoreCase( thePromotion.getStatus().getCode(), codePropertiesService.getDetailStatusActive() ) ) {
        	stampPromoService.callPortaCreateStampPromo( thePromotion );
        }
    }

    @Override
    public List<Program> getPrograms() {
        return programRepo.findAll();
    }

    @Override
    public List<Program> getValidPrograms(Date inDate) {
        return programRepo.find(inDate, codePropertiesService.getDetailStatusActive());
    }

    @Override
    public List<Program> getValidPrograms() {
        return getValidPrograms(new Date());
    }

    @Override
    public void saveProgram(Program inProgram) {
        inProgram.setStatus(lookupDetailRepo.findByCode(codePropertiesService.getDetailStatusActive()));
        if (StringUtils.isBlank(inProgram.getName())) {
            inProgram.setName(new Date().toString());
        }
        if (StringUtils.isBlank(inProgram.getDescription())) {
            inProgram.setName(new Date().toString());
        }
        if (null != inProgram.getDuration().getEndDate()) {
            inProgram.getDuration().setEndDate(CalendarUtil.INSTANCE.getEndDate(inProgram.getDuration().getEndDate()));
        }
        programRepo.save(inProgram);
    }

    @Override
    public void saveProgramDto(ProgramDto inProgram) {
        Program theProgram = inProgram.toModel();
        saveProgram(theProgram);
        inProgram.setId(theProgram.getId());
    }

    @Override
    public List<ProgramDto> getValidProgramDtos() {
        return convertProgramToDto(getValidPrograms(), false);
    }

    @Override
    public List<ProgramDto> getProgramDtos() {
        return convertProgramToDto(programRepo.findAll(), false);
    }

    @Override
    public ProgramDto getProgramDto(Long inId) {
        return (null != inId) ? new ProgramDto(programRepo.findOne(inId), true) : null;
    }

    @Override
    public List<Campaign> getValidCampaigns(Program inProgram, Date inDate) {
        return campaignRepo.find(inDate, codePropertiesService.getDetailStatusActive(), inProgram);
    }

    @Override
    public List<Campaign> getValidCampaigns(Program program) {
        return getValidCampaigns(program, new Date());
    }

    @Override
    public List<Campaign> getValidCampaigns(Long programId) {
        return getValidCampaigns(programRepo.findOne(programId), new Date());
    }

    @Override
    public void saveCampaign(Campaign inCampaign) {
        inCampaign.setStatus(lookupDetailRepo.findByCode(codePropertiesService.getDetailStatusActive()));
        if (StringUtils.isBlank(inCampaign.getName())) {
            inCampaign.setName(new Date().toString());
        }
        if (StringUtils.isBlank(inCampaign.getDescription())) {
            inCampaign.setName(new Date().toString());
        }
        if (null != inCampaign.getDuration().getEndDate()) {
            inCampaign.getDuration().setEndDate(CalendarUtil.INSTANCE.getEndDate(inCampaign.getDuration().getEndDate()));
        }
        campaignRepo.save(inCampaign);
    }

    @Override
    public void saveCampaignDto(CampaignDto inCampaign) {
        Campaign theCampaign = inCampaign.toModel();
        if (null != inCampaign.getProgram())
            theCampaign.setProgram(programRepo.findOne(inCampaign.getProgram()));
        saveCampaign(theCampaign);
        inCampaign.setId(theCampaign.getId());
    }

    @Override
    public List<CampaignDto> getCampaignDtos(Long inProgramId) {
        return convertCampaignToDto(campaignRepo.findByProgram(programRepo.findOne(inProgramId)), false);
    }

    @Override
    public CampaignDto getCampaignDto(Long inId) {
        return (null != inId) ? new CampaignDto(campaignRepo.findOne(inId), true) : null;
    }

    @Override
    public PromotionDto getPromotionDto(Long inId) {
        Promotion thePromotion = promotionRepo.findOne(inId);
        if (null != thePromotion) {
            PromotionDto dto = new PromotionDto(thePromotion);
            if (StringUtils.isNotBlank(dto.getCreateUser())) {
                UserModel user = userRepo.findByUsername(dto.getCreateUser());
                if (null != user) {
                    dto.setCreateUserFirstName(user.getFirstName());
                    dto.setCreateUserLastName(user.getLastName());
                }
            }
            if (CollectionUtils.isNotEmpty(dto.getPromotionRewards())) {
                for (PromotionRewardDto rwd : dto.getPromotionRewards()) {
                    ProductSearchDto searchDto = new ProductSearchDto();
                    searchDto.setSku(rwd.getProductCode());
                    Product prd = productService.searchOne(searchDto);
                    if (null != prd) {
                        rwd.setProductName(prd.getDescription());
                    }
                }
            }

            if (CollectionUtils.isNotEmpty(dto.getPromotionRedemptions())) {
                for (PromotionRedemptionDto rwd : dto.getPromotionRedemptions()) {
                	if ( StringUtils.isNotBlank( rwd.getSku() ) ) {
                        ProductSearchDto searchDto = new ProductSearchDto();
                        searchDto.setSku(rwd.getSku());
                        Product prd = productService.searchOne(searchDto);
                        if (null != prd) {
                            rwd.setProductName(prd.getDescription());
                        }
                	}
                }
            }
            return dto;
        }
        return null;
    }

    @Override
    public List<Promotion> getValidPromotions(Campaign campaign) {
        return getValidPromotionsForCampaign(campaign, new Date());
    }

    @Override
    public List<Promotion> getValidPromotionsForCampaign(Campaign inCampaign, Date inDate) {


        return getValidPromotionsForCampaign(inCampaign, inDate, null);
    }

    @Override
    public List<Promotion> getValidPromotionsForCampaign(Campaign inCampaign, Date inDate, String rewardType) {

        List<Promotion> thePromotions = new ArrayList<Promotion>();

        PromotionSearchDto theDto = new PromotionSearchDto();
        theDto.setStatus(codePropertiesService.getDetailStatusActive());
        theDto.setGivenDate(inDate);
        theDto.setCampaign(inCampaign.getId());
        theDto.setRewardType(rewardType);
        theDto.setAffectedDays(getDayCodeFromDate(inDate));
        CollectionUtils.addAll(thePromotions,
                promotionRepo.findAll(theDto.createSearchExpression()).iterator());
        return thePromotions;
    }

    private String getDayCodeFromDate(Date date) {
        DateFormat fmt = new SimpleDateFormat("EEEE");
        String transactionDay = fmt.format(date);

        if (StringUtils.isNotBlank(transactionDay)) {
            if (lookupDetailRepo.findByCode(codePropertiesService.getDetailDayMonday()).getDescription().equalsIgnoreCase(transactionDay)) {
                return codePropertiesService.getDetailDayMonday();
            } else if (lookupDetailRepo.findByCode(codePropertiesService.getDetailDayTuesday()).getDescription().equalsIgnoreCase(transactionDay)) {
                return codePropertiesService.getDetailDayTuesday();
            } else if (lookupDetailRepo.findByCode(codePropertiesService.getDetailDayWednesday()).getDescription().equalsIgnoreCase(transactionDay)) {
                return codePropertiesService.getDetailDayWednesday();
            } else if (lookupDetailRepo.findByCode(codePropertiesService.getDetailDayThursday()).getDescription().equalsIgnoreCase(transactionDay)) {
                return codePropertiesService.getDetailDayThursday();
            } else if (lookupDetailRepo.findByCode(codePropertiesService.getDetailDayFriday()).getDescription().equalsIgnoreCase(transactionDay)) {
                return codePropertiesService.getDetailDayFriday();
            } else if (lookupDetailRepo.findByCode(codePropertiesService.getDetailDaySaturday()).getDescription().equalsIgnoreCase(transactionDay)) {
                return codePropertiesService.getDetailDaySaturday();
            } else if (lookupDetailRepo.findByCode(codePropertiesService.getDetailDaySunday()).getDescription().equalsIgnoreCase(transactionDay)) {
                return codePropertiesService.getDetailDaySunday();
            }
        }

        return null;

    }

    @Override
    public List<Promotion> getValidPromotionsForMember(String memberId, String storeCode, String transactionNo,
                                                       String paymentType,
                                                       String cardNumber, Date transactionDate, List<PosTxItemDto> itemList) {

        List<Promotion> validPromotions = new ArrayList<Promotion>();

        List<Program> programs = this.getValidPrograms(transactionDate);

        for (Program program : programs) {
            List<Campaign> campaigns = this.getValidCampaigns(program, transactionDate);

            for (Campaign campaign : campaigns) {
                List<Promotion> promotions = this.getValidPromotionsForCampaign(campaign, transactionDate);

                for (Promotion promotion : promotions) {

                    if (validatePaymentType(promotion, paymentType, cardNumber)
                            && validateTargetStores(promotion, storeCode)
                            && validateTargetMemberGroups(promotion, memberId)
                            && validateTargetProductGroups(promotion, transactionNo, itemList))
                        validPromotions.add(promotion);
                }

            }
        }


        return validPromotions;
    }

    private List<Promotion> getValidStampPromotionsForMember(String memberId, String storeCode, String transactionNo,
            String paymentType, String cardNumber, Date transactionDate, List<PosTxItemDto> itemList) {

        List<Promotion> stampPromotions = Lists.newArrayList();
        List<Promotion> validPromotions = getValidPromotionsForMember(memberId, storeCode, transactionNo, paymentType, cardNumber, transactionDate, itemList);
        for (Promotion promotion : validPromotions) {
            if (promotion.getRewardType() != null
                    && promotion.getRewardType().getCode().equals(codePropertiesService.getDetailRewardTypeStamp())) {
                stampPromotions.add(promotion);
            }
        }
        return stampPromotions;
    }

    @Override
    public Promotion savePromotion(Promotion inPromotion) {
        if (StringUtils.isBlank(inPromotion.getName())) {
            inPromotion.setName(new Date().toString());
        }
        if (StringUtils.isBlank(inPromotion.getDescription())) {
            inPromotion.setDescription(new Date().toString());
        }
        if (null != inPromotion.getEndDate()) {
            inPromotion.setEndDate(CalendarUtil.INSTANCE.getEndDate(inPromotion.getEndDate()));
        }
        return promotionRepo.save(inPromotion);
    }

    @Override
    public void savePromotionDto(PromotionDto inPromotion) {
        Promotion thePromotion = inPromotion.toModel();
        if (StringUtils.isNotBlank(inPromotion.getRewardType())) {
            thePromotion.setRewardType(lookupDetailRepo.findByCode(inPromotion.getRewardType()));
        }
        thePromotion.setStatus(
                lookupDetailRepo.findByCode((StringUtils.isNotBlank(inPromotion.getStatus())) ?
                        inPromotion.getStatus() :
                        codePropertiesService.getDetailStatusDraft()));

        thePromotion.setCampaign(campaignRepo.findOne(inPromotion.getCampaign()));
        thePromotion = savePromotion(thePromotion);
        inPromotion.setId(thePromotion.getId());
        saveTargets(inPromotion, thePromotion);
        saveRewards(inPromotion, thePromotion);
        saveRedemptions(inPromotion, thePromotion);
        promotionRepo.save(thePromotion);
    }

    @Override
    public void updatePromotionDto(PromotionDto inPromotion) {
        Promotion thePromotion = promotionRepo.findOne(inPromotion.getId());
        LookupDetail rewardType = thePromotion.getRewardType();
        thePromotion = inPromotion.toModel(thePromotion);
        if (StringUtils.isBlank(inPromotion.getRewardType()) && null != rewardType) {
            thePromotion.setRewardType(rewardType);
        } else {
            thePromotion.setRewardType(lookupDetailRepo.findByCode(inPromotion.getRewardType()));
        }
        if (null == thePromotion.getStatus()
                || (StringUtils.isNotBlank(inPromotion.getStatus())
                && null != thePromotion.getStatus()
                && !thePromotion.getStatus().getCode().equalsIgnoreCase(inPromotion.getStatus()))) {
            thePromotion.setStatus(lookupDetailRepo.findByCode(inPromotion.getStatus()));
        }
        if (thePromotion.getCampaign().getId() != inPromotion.getCampaign()) {
            thePromotion.setCampaign(campaignRepo.findOne(inPromotion.getCampaign()));
        }
        savePromotion(thePromotion);
        inPromotion.setId(thePromotion.getId());
        saveTargets(inPromotion, thePromotion);
        saveRewards(inPromotion, thePromotion);
        savePromotion(thePromotion);
    }

    @Override
    public void processPromotions(MemberModel member, String paymentType, String storeCode, String transactionNo, String
            transactionAmount, String cardNumber, String transactionDate, List<PosTxItemDto> itemList, ReturnMessage returnMessage) {


        if (returnMessage == null) {
            returnMessage = new ReturnMessage();
        }

        List<Map> payments = pointsManagerService.retrieveValidPaymentTypes(paymentType, transactionAmount,
                cardNumber, false, returnMessage);

        Double earnedPoints = 0D;

        for (Map payment : payments) {

            String pType = (String) payment.get(PointsTxnManagerService.PAYMENT_TYPE_KEY);
            Double amt = (Double) payment.get(PointsTxnManagerService.AMOUNT_KEY);
            String cardNo = (String) payment.get(PointsTxnManagerService.CARD_NUMBER_KEY);

            processMemberPoints(member, transactionDate, transactionNo, storeCode, pType, amt, cardNo, itemList, returnMessage);

            if (returnMessage != null && returnMessage.getEarnedPoints() != null)
                earnedPoints += returnMessage.getEarnedPoints();
        }

        // process stamp earning if stamp user
        Double earnedStamps = 0D;
        if (member != null && BooleanUtils.isTrue(member.getStampUser())) {

            Date transDate = null;
            try {
                transDate = DateUtil.convertDateString(DateUtil.REST_DATE_FORMAT, transactionDate);
            } catch (ParseException e) {
                LOGGER.error("Error Parsing date parameter {} for transaction no {} :: {}  ", transactionDate, transactionNo, e.getMessage());
            }

            List<Promotion> promotions = getValidStampPromotionsForMember(member.getAccountId(), storeCode, transactionNo, paymentType,
                    cardNumber, transDate, itemList);

            // workaround for create user/datetime not set
            DateTime currDateTime = DateTime.now();
            CustomSecurityUserDetailsImpl userDetails = UserUtil.getCurrentUser();
            String currUser = userDetails != null ? userDetails.getUsername() : "SYSTEM";

            for (Promotion promotion : promotions) {
                StampTxnModel stampTxnModel = new StampTxnModel();
                stampTxnModel.setStampPromotion(promotion);

                StampTxnHeader stampTxnHeader = new StampTxnHeader();
                stampTxnHeader.setTotalTxnStamps(0D);
                stampTxnHeader.setTransactionNo(handleNull(transactionNo));
                stampTxnHeader.setStoreCode(handleNull(storeCode));
                try {
                    stampTxnHeader.setTransactionDateTime(DateUtil.convertDateString(DateUtil.REST_DATE_FORMAT, transactionDate));
                } catch (ParseException e) {
                    LOGGER.error("Error Parsing date parameter {} for transaction no {} :: {}  ", transactionDate, transactionNo, e.getMessage());
                }

                for (Map payment : payments) {
                    String pType = (String) payment.get(PointsTxnManagerService.PAYMENT_TYPE_KEY);
                    Double amt = (Double) payment.get(PointsTxnManagerService.AMOUNT_KEY);
                    String cardNo = (String) payment.get(PointsTxnManagerService.CARD_NUMBER_KEY);

                    processMemberStamp(stampTxnModel, stampTxnHeader, member, transactionDate, transactionNo, storeCode, pType, amt,
                            cardNo, itemList, returnMessage, currDateTime, currUser);

                    if (returnMessage != null && returnMessage.getEarnedStamps() != null) {
                        earnedStamps += returnMessage.getEarnedStamps();
                    }
                }

                stampTxnHeaderRepo.save(stampTxnHeader);
                // TODO set stampTxnModel date fields
                stampTxnModel.setTransactionHeader(stampTxnHeader);

                stampTxnModel = stampTxnModelRepo.save(stampTxnModel);
                // only call porta ws if stamps were earned
                if (earnedStamps > 0) {
                    callPortaEarnStampWS(stampTxnModel);
                }
            }
        }

        //accumulate all earned points
        returnMessage.setEarnedPoints(earnedPoints);
        returnMessage.setEarnedStamps(earnedStamps);

        //return message for member as employee does not necessarily need to earn points unless there is a promotion for employees
        String memberTypeCode = member.getMemberType() != null ? member.getMemberType().getCode() : null;
        String individualTypeCode = codePropertiesService.getDetailMemberTypeIndividual();
        if (null != memberTypeCode && individualTypeCode.equalsIgnoreCase(memberTypeCode)) {
            if (returnMessage.getEarnedPoints() == 0D && BooleanUtils.isNotTrue(member.getStampUser())) {
                returnMessage.setType(MessageType.ERROR);
                returnMessage.setMessageCode(codePropertiesService.getDetailMessageCodeNoPointsEarned());
                returnMessage.setMessage(messageSource.getMessage("points_rs_no_points_earned", null, null));
            }
            if (returnMessage.getEarnedStamps() == 0D) {
                if (member != null && BooleanUtils.isNotTrue(member.getStampUser())) {
                    returnMessage.setType(MessageType.ERROR);
                    returnMessage.setMessageCode(codePropertiesService.getDetailMessageCodeNoStampsEarned());
                    returnMessage.setMessage(returnMessage.getMessage() + " " + messageSource.getMessage("points_rs_non_stamps_user", null, null));
                }
            }
        }
    }

    @Override
    public void savePromotionRewardDto(PromotionRewardDto inReward) {
        PromotionReward theReward = inReward.toModel();
        theReward.setRewardType(lookupDetailRepo.findByCode(inReward.getRewardType()));
        promotionRewardRepo.save(theReward);
        inReward.setId(theReward.getId());
    }

    @Override
    public PromoRewardDto retrievePromotionReward(String memberId, String paymentType, String storeCode, String transactionNo, Double
            transactionAmount, String cardNumber, Date transactionDate, List<TransactionAudit> audits, List<PosTxItemDto> itemList) {

        PromoRewardDto promoRewardDto = new PromoRewardDto();

        List<Promotion> promotions = getValidPromotionsForMember(memberId, storeCode, transactionNo, paymentType, cardNumber,
                transactionDate, itemList);

        for (Promotion promotion : promotions) {
            computePromotionRewards(memberId, promoRewardDto, promotion, transactionAmount, transactionNo, storeCode, itemList, audits);
        }

        return promoRewardDto;
    }

    public PromoRewardDto retrieveStampPromotion(StampTxnModel stampTxnModel, String memberId, String paymentType, String storeCode,
                                                 String transactionNo, Double transactionAmount, String cardNumber, Date transactionDate,
                                                 List<TransactionAudit> audits, List<PosTxItemDto> itemList) {

        PromoRewardDto promoRewardDto = new PromoRewardDto();

        Promotion promotion = stampTxnModel.getStampPromotion();
        PromotionReward promotionReward = getPromotionRewardForTransaction(promotion, transactionAmount, transactionNo);
        if (promotionReward != null) {
            Double stamps = computeStamps(promotionReward, transactionAmount);
            earnStamps(promoRewardDto, stamps);
        }
        return promoRewardDto;
    }

    public void computePromotionRewards(String memberId, PromoRewardDto reward, Promotion promotion, Double transactionAmount, String transactionNo, String storeCode, List<PosTxItemDto> itemList, List<TransactionAudit> audits) {

        Double points = null;

        PromotionReward promotionReward = null;
        if (null != promotion.getRewardType()
                && promotion.getRewardType().getCode().equals(codePropertiesService.getDetailRewardTypePoints())) {
            promotionReward = getPromotionRewardForTransaction(promotion, transactionAmount, transactionNo);
            if (promotionReward != null) {
                points = computePoints(promotionReward, transactionAmount);
                Double discount = computeDiscounts(promotionReward, transactionAmount);
                earnRewards(reward, points, discount);
            }
        } else if (null != promotion.getRewardType()
                && promotion.getRewardType().getCode().equals(codePropertiesService.getDetailRewardTypeItemPoint())) {
            double itemQty = countTargetProducts(promotion, transactionNo, itemList);
            promotionReward = getPromotionRewardForTransactionQty(promotion, itemQty);
            if (promotionReward != null) {
                points = computeItemPoints(promotionReward, itemQty);
                earnRewards(reward, points, null);
            }
        }

        //will now track all points transactions
        auditTransactionRewards(memberId, transactionNo, storeCode, audits, promotion, points);
    }

    private double countTargetProducts(Promotion promo, String txnNo, List<PosTxItemDto> txnItems) {
        //get target products
        List<String> productCodes = new ArrayList<String>();
        final List<String> cfnCodes = new ArrayList<String>();
        List<String> excProductCodes = new ArrayList<String>();
        for (PromotionProductGroup group : promo.getTargetProductGroups()) {
            if (CollectionUtils.isNotEmpty(group.getProductGroup().getProductList())) {
                productCodes.addAll(group.getProductGroup().getProductList());
            } else if (CollectionUtils.isNotEmpty(group.getProductGroup().getProductCfnList())) {
                cfnCodes.addAll(group.getProductGroup().getProductCfnList());
            }
            if (CollectionUtils.isNotEmpty(group.getProductGroup().getExcProductList())) {
                excProductCodes.addAll(group.getProductGroup().getExcProductList());
            }
        }

        //filter qualified products
        double itemCount = 0L;
        List<PosTxItemDto> items = new ArrayList<PosTxItemDto>();
        if (CollectionUtils.isEmpty(txnItems)) {
            PosTxItemSearchDto dto = new PosTxItemSearchDto();
            dto.setPosTransactionNo(txnNo);
            txnItems = new ArrayList<PosTxItemDto>(posTransactionService.getPosTxItems(dto).getResults());
        }
        for (final PosTxItemDto product : txnItems) {
            if (productCodes.contains(product.getProductId()) && !excProductCodes.contains(product.getProductId())) {
                items.add(product);
                itemCount += product.getQuantity();
            } else if (CollectionUtils.isNotEmpty(cfnCodes)) {
                for (String code : cfnCodes) {
                    boolean bool = CollectionUtils.countMatches(excProductCodes, new Predicate() {
                        public boolean evaluate(Object o) {
                            return (StringUtils.equalsIgnoreCase(((String) o), product.getProductId()));
                        }
                    }) > 0;
                    if (StringUtils.startsWithIgnoreCase(product.getProductId(), code) && !bool) {
                        items.add(product);
                        itemCount += product.getQuantity();
                        break;
                    }
                }
            }
        }

        return itemCount;
    }

    private void auditTransactionRewards(String memberId, String transactionNo, String storeCode, List<TransactionAudit> audits, Promotion promotion, Double points) {
//    	PosTransaction posTransaction = posTransactionService.getTransaction(transactionNo);

        if (audits == null) {
            return;
        }

        MemberModel memberModel = memberService.findByAccountId(memberId);

        TransactionAudit audit = new TransactionAudit();

        audit.setStore(storeService.getStoreByCode(storeCode));
        audit.setTransactionNo(transactionNo);
        audit.setTransactionPoints(points != null ? points.longValue() : null);
        audit.setMemberModel(memberModel);
        audit.setPromotion(promotion);
        audit.setId(customIdGeneratorService.generateId(storeCode));
        audit.setCreated(new DateTime());

        audits.add(audit);
    }

    private Double computePoints(PromotionReward promotionReward, Double transactionAmount) {
        Double points = null;
        if (BooleanUtils.isTrue(promotionReward.getFixed()) || promotionReward.getBaseAmount().compareTo(BigDecimal.ZERO) == 0) { // Fixed points when base amount is 0 or (Task #83466) when Fixed is true
            points = promotionReward.getBaseFactor();
        } else if (promotionReward.getBaseAmount() != null) {
            BigDecimal multiplier = new BigDecimal(transactionAmount.toString()).divide(promotionReward.getBaseAmount(), 0, RoundingMode.DOWN);

            if (promotionReward.getBaseFactor() != null)
                points = promotionReward.getBaseFactor() * multiplier.intValue();
        }

        return points;
    }

    private Double computeStamps(PromotionReward promotionReward, Double transactionAmount) {
        Double stamps = null;
        if (BooleanUtils.isTrue(promotionReward.getFixed()) || promotionReward.getBaseAmount().compareTo(BigDecimal.ZERO) == 0) {
            // Fixed points when base amount is 0 or (Task #83466) when Fixed is true
            stamps = promotionReward.getBaseFactor();
        } else if (promotionReward.getBaseAmount() != null) {
            BigDecimal multiplier = new BigDecimal(transactionAmount.toString()).divide(promotionReward.getBaseAmount(), 0, RoundingMode.DOWN);
            if (promotionReward.getBaseFactor() != null) {
                stamps = promotionReward.getBaseFactor() * multiplier.intValue();
            }
        }

        return stamps;
    }

    private Double computeDiscounts(PromotionReward promotionReward, Double transactionAmount) {
        if (promotionReward.getDiscount() != null)
            return promotionReward.getDiscount().doubleValue();
        else
            return null;

        //TODO compute discount here
    }

    private Double computeItemPoints(PromotionReward promotionReward, Double itemCount) {
        //same case with points-reward-type
        return computePoints(promotionReward, itemCount);
    }

    private void earnRewards(PromoRewardDto reward, Double points, Double discount) {
        if (points != null)
            reward.setPoints(reward.getPoints() + points);

        if (discount != null)
            reward.setDiscount(reward.getDiscount() + discount);
    }

    private void earnStamps(PromoRewardDto reward, Double stampsEarned) {
        if (stampsEarned != null) {
            reward.setStamps(reward.getStamps() + stampsEarned);
        }
    }

    private PromotionReward getPromotionRewardForTransaction(Promotion promotion, Double transactionAmount, String transactionNo) {
        List<PromotionReward> rewards = promotion.getPromotionRewards();
        BigDecimal txnAmount = new BigDecimal(transactionAmount.toString());

        if (promotion.getRewardType().getCode().equals(codePropertiesService.getDetailRewardTypePoints())) {
            for (PromotionReward reward : rewards) {
                BigDecimal minAmount = BigDecimal.ZERO;
                if (reward.getMinAmount() != null)
                    minAmount = reward.getMinAmount();

                if (txnAmount.compareTo(minAmount) > -1 && (reward.getMaxAmount() == null || reward.getMaxAmount().compareTo(BigDecimal.ZERO) == 0 || txnAmount.compareTo(reward.getMaxAmount()) < 1)) {
                    return reward;
                }
            }
        } else if (promotion.getRewardType().getCode().equals(codePropertiesService.getDetailRewardTypeStamp())) {
            for (PromotionReward reward : rewards) {
                BigDecimal minAmount = BigDecimal.ZERO;
                if (reward.getMinAmount() != null) {
                    minAmount = reward.getMinAmount();
                }
                if (txnAmount.compareTo(minAmount) > -1 && (reward.getMaxAmount() == null || reward.getMaxAmount().compareTo(BigDecimal.ZERO) == 0 || txnAmount.compareTo(reward.getMaxAmount()) < 1)) {
                    return reward;
                }
            }
        } else { //TODO other reward types

        }

        return null;
    }

    private PromotionReward getPromotionRewardForTransactionQty(Promotion promotion, double itemCount) {
        if (CollectionUtils.isEmpty(promotion.getPromotionRewards())) {
            return null;
        }

        for (PromotionReward reward : promotion.getPromotionRewards()) {
            double minQty = (reward.getMinQty() != null) ? reward.getMinQty() : 0;
            double maxQty = (reward.getMaxQty() != null) ? reward.getMaxQty() : 0;
            if (itemCount >= minQty && (maxQty != 0 && itemCount < maxQty) || itemCount >= minQty && maxQty == 0) {
                return reward;
            }
        }

        return null;
    }

    @Override
    public Double convertPointToCurrency(Long points) {
        return pointsManagerService.getCurrencyAmountPerPoint() * points;
    }

    @Override
    public Double convertCurrencyToPoints(Double amount) {
        if (amount == null || amount <= 0) return 0D;
        return (amount / pointsManagerService.getCurrencyAmountPerPoint());
    }

    private boolean validateTargetMemberGroups(Promotion promotion, String memberId) {
        boolean ret = false;

        Set<PromotionMemberGroup> memberGroups = promotion.getTargetMemberGroups();
        if (memberGroups == null || memberGroups.size() == 0) {
            //if no membergroups defined then pass all members
            return true;
        }


        for (PromotionMemberGroup group : memberGroups) {
            MemberGroup memberGroup = group.getMemberGroup();
            if (memberGroup.getRfs() != null && memberGroup.getRfs().getEnabled() && callPrimaryMemberGroupValidation(memberGroup, memberId)) {
                ret = true;
                break;
            } else if (memberGroupService.isQualifiedMember(memberGroup, memberId)) {
                ret = true;
                break;
            }
        }

        return ret;
    }

    private boolean callPrimaryMemberGroupValidation(MemberGroup group, String accountId) {
        try {

            String urlStr = getUrlStringForMemGrp() + URL_MEMGRP_WS
                    .replace("groupId", Long.toString(group.getId()))
                    .replace("accountId", accountId).replace("validationKey", md5Service.createMD5Hash(group.getId() + accountId + getMMSKey()));
            URL url = new URL(urlStr);
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setRequestMethod("GET");
            conn.setRequestProperty("Accept", "application/json");

            if (conn.getResponseCode() != 200) {
                throw new RuntimeException("Failed : HTTP error code : " + conn.getResponseCode());
            }

            BufferedReader br = new BufferedReader(new InputStreamReader((conn.getInputStream())));
            ObjectMapper mapper = new ObjectMapper();
            RestControllerResponse ret = mapper.readValue(br, RestControllerResponse.class);
			/*String output = ;
			System.out.println("Output from Server .... \n");
			while ((output = br.readLine()) != null) {
				System.out.println(output);
			}*/

            conn.disconnect();

            return BooleanUtils.toBoolean((Boolean) ret.getResult());
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return false;
    }

    public String getUrlStringForMemGrp() {
        ApplicationConfig config = appConfigRepo.findByKey(AppKey.PRIMARY_WS_URL);
        return config != null ? config.getValue() : AppConfigDefaults.DEFAULT_PRIMARY_WS_URL;
    }

    public String getMMSKey() {
        ApplicationConfig config = appConfigRepo.findByKey(AppKey.MD5_SECRET_KEY);
        return config != null ? config.getValue() : AppConfigDefaults.DEFAULT_MD5_SECRET_KEY;
    }

    private boolean validateTargetProductGroups(Promotion promotion, String transactionNo, List<PosTxItemDto> itemList) {
        Set<PromotionProductGroup> productGroups = promotion.getTargetProductGroups();
        if (CollectionUtils.isEmpty(productGroups)) {
            //if no productGroups defined then pass all members
            return true;
        }

        if (CollectionUtils.isNotEmpty(itemList)) {
            for (PromotionProductGroup group : productGroups) {
                for (PosTxItemDto product : itemList) {
                    if (CollectionUtils.isNotEmpty(group.getProductGroup().getProductList())
                            && group.getProductGroup().getProductList().contains(product.getProductId())) {
                        if (CollectionUtils.isNotEmpty(group.getProductGroup().getExcProductList()) &&
                                group.getProductGroup().getExcProductList().contains(product.getProductId())) {
                            continue;
                        }
                        return true;
                    } else if (CollectionUtils.isNotEmpty(group.getProductGroup().getProductCfnList())) {
                        List<String> excludedProducts = group.getProductGroup().getExcProductList() != null ?
                                group.getProductGroup().getExcProductList() : new ArrayList<String>();
                        for (String code : group.getProductGroup().getProductCfnList()) {
                            return product.getProductId().startsWith(code) && !excludedProducts.contains(product.getProductId());
                        }
                    }
                }
            }
        } else {
            for (PromotionProductGroup group : productGroups) {
                for (Product product : posTransactionService.getSoldProductsFromTransaction(transactionNo)) {
                    if (CollectionUtils.isNotEmpty(group.getProductGroup().getProductList())
                            && group.getProductGroup().getProductList().contains(product.getItemCode())) {
                        return true;
                    } else if (CollectionUtils.isNotEmpty(group.getProductGroup().getProductCfnList())) {
                        List<String> excludedProducts = group.getProductGroup().getExcProductList() != null ?
                                group.getProductGroup().getExcProductList() : new ArrayList<String>();
                        for (String code : group.getProductGroup().getProductCfnList()) {
                            return product.getItemCode().startsWith(code) && !excludedProducts.contains(product.getItemCode());
                        }
                    }
                }
            }
        }
        return false;
    }

    private boolean validateTargetStores(Promotion promotion, String storeCode) {
        boolean ret = false;

        Set<PromotionStoreGroup> storeGroups = promotion.getTargetStoreGroups();
        if (storeGroups == null || storeGroups.size() == 0) {
            //if no storeGroups defined then pass all members
            return true;
        }


        for (PromotionStoreGroup storeGroup : storeGroups) {
            List<String> stores = storeGroup.getStoreGroup().getStoreList();
            for (String store : stores) {
                if (storeCode.equalsIgnoreCase(store)) {
                    return true;
                }
            }
        }


        return ret;


    }

    private boolean validatePaymentType(Promotion promotion, String paymentType, String cardNumber) {
        boolean ret = false;

        Set<PromotionPaymentType> paymentTypes = promotion.getTargetPaymentTypes();
        if (paymentTypes == null || paymentTypes.size() == 0) {
            //if no paymentTypes defined then pass all members
            return true;
        }

        for (PromotionPaymentType group : paymentTypes) {
            if (group.getLookupDetail().getCode().equalsIgnoreCase(paymentType)) {
                if (StringUtils.isBlank(group.getWildcards()))
                    return true;
                else {
                    String[] wildcards = group.getWildcards().split(",");
                    if (wildcards.length > 0) {
                        if (StringUtils.isNotBlank(cardNumber)) {
                            for (String wildcard : wildcards) {
                                wildcard = wildcard.replaceAll("\\*", "[0-9*]*");
                                if (cardNumber.matches(wildcard))
                                    return true;
                            }
                        } else
                            return false;
                    } else
                        return true;

                }
            }
        }

        return ret;


    }

    private List<ProgramDto> filterProgramDtos(List<ProgramDto> inPrograms, PromotionSearchDto theSearchDto) {
        if (StringUtils.isNotBlank(theSearchDto.getName()) ||
                StringUtils.isNotBlank(theSearchDto.getDescription())) {
            for (ProgramDto theProgram : inPrograms) {
                for (CampaignDto theCampaign : theProgram.getCampaigns()) {
                    List<PromotionDto> thePromotions = new ArrayList<PromotionDto>();
                    for (PromotionDto thePromotion : theCampaign.getPromotions()) {
                        if (StringUtils.isNotBlank(theSearchDto.getName())) {
                            if (StringUtils.isNotBlank(thePromotion.getName()) && StringUtils.containsIgnoreCase(thePromotion.getName(), theSearchDto.getName())) {
                                thePromotions.add(thePromotion);
                                continue;
                            }
                        }
                        if (StringUtils.isNotBlank(theSearchDto.getDescription())) {
                            if (StringUtils.isNotBlank(thePromotion.getDescription()) && StringUtils.containsIgnoreCase(thePromotion.getDescription(), theSearchDto.getDescription())) {
                                thePromotions.add(thePromotion);
                            }
                        }
                    }
                    theCampaign.setPromotions(thePromotions);
                }
            }
        }
        return inPrograms;
    }

    private List<ProgramDto> convertProgramToDto(List<Program> inPrograms, boolean renderCampaign) {
        List<ProgramDto> theDtos = new ArrayList<ProgramDto>();
        for (Program theModel : inPrograms) {
            theDtos.add(new ProgramDto(theModel, renderCampaign));
        }
        return theDtos;
    }

    private List<CampaignDto> convertCampaignToDto(List<Campaign> inCampaigns, boolean renderPromo) {
        List<CampaignDto> theDtos = new ArrayList<CampaignDto>();
        for (Campaign theModel : inCampaigns) {
            theDtos.add(new CampaignDto(theModel, renderPromo));
        }
        return theDtos;
    }

    private List<PromotionDto> convertPromotionToDto(List<Promotion> inPromos) {
        List<PromotionDto> theDtos = new ArrayList<PromotionDto>();
        for (Promotion theModel : inPromos) {
            theDtos.add(new PromotionDto(theModel));
        }
        return theDtos;
    }

    private List<ApprovalRemarkDto> convertRemarkToDto(List<ApprovalRemark> inRemarks) {
        List<ApprovalRemarkDto> theDtos = new ArrayList<ApprovalRemarkDto>();
        for (ApprovalRemark theModel : inRemarks) {
            theDtos.add(new ApprovalRemarkDto(theModel));
        }
        return theDtos;
    }

    private void processMemberPoints(MemberModel member, String transactionDate, String transactionNo, String storeCode,
                                     String paymentType,
                                     Double amount, String cardNumber, List<PosTxItemDto> itemList, ReturnMessage returnMessage) {

        List<TransactionAudit> audits = new ArrayList<TransactionAudit>();

        String internalErrorCode = codePropertiesService.getDetailMessageCodeInternalError();

        PointsTxnModel pointsTxnModel = new PointsTxnModel();

        Date transDate = null;

        try {

            transDate = DateUtil.convertDateString(DateUtil.REST_DATE_FORMAT, transactionDate);
            pointsTxnModel.setTransactionDateTime(transDate);
        } catch (ParseException e) {

            LOGGER.error("Error Parsing date parameter {} for transaction no {} :: {}  ", transactionDate, transactionNo, e.getMessage());
        }

        PromoRewardDto reward = retrievePromotionReward(member.getAccountId(), paymentType, storeCode, transactionNo,
                amount, cardNumber, transDate, audits, itemList);


        //store earned points so it can be accumulated
        returnMessage.setEarnedPoints(reward.getPoints());

//        if (reward.getPoints() == 0.0) {
//
//            return;
//        }
//       commented out since we're now accepting all transactions regardless if points are earned

        pointsTxnModel.setTransactionPoints((double) reward.getPoints());

        pointsTxnModel.setTransactionNo(handleNull(transactionNo));
        pointsTxnModel.setStoreCode(handleNull(storeCode));
        pointsTxnModel.setTransactionAmount(amount);
        pointsTxnModel.setTransactionType(PointTxnType.EARN);
        pointsTxnModel.setTransactionMedia(paymentType);
        pointsTxnModel.setTransactionContributer(member);

        //check if member is supplementary  then use primary member for processing
        if (StringUtils.isNotBlank(member.getParentAccountId())) {
            member = memberRepo.findByAccountId(member.getParentAccountId());
        }

        pointsTxnModel.setMemberModel(member);

        //if (member != null) {
        //    pointsTxnModel.setMemberAccountId(member.getAccountId());
        //}


        if (!pointsManagerService.processPoints(pointsTxnModel, returnMessage)) {
            returnMessage.setType(MessageType.ERROR);
            returnMessage.setMessageCode(internalErrorCode);
            returnMessage.setMessage(messageSource.getMessage("points_rs_error_processing_points", null, null));
        } else {
            if (audits != null && audits.size() > 0)
                transactionAuditService.saveTransactionAudits(audits);
        }

    }

    private void processMemberStamp(StampTxnModel stampTxnModel, StampTxnHeader stampTxnHeader, MemberModel member, String transactionDate,
                                    String transactionNo, String storeCode, String paymentType, Double amount, String cardNumber,
                                    List<PosTxItemDto> itemList, ReturnMessage returnMessage, DateTime currDateTime, String currUser) {

        List<TransactionAudit> audits = new ArrayList<TransactionAudit>();
        //String internalErrorCode = codePropertiesService.getDetailMessageCodeInternalError();

        StampTxnDetail stampTxnDetail = new StampTxnDetail();
        stampTxnDetail.setTransactionMedia(lookupDetailRepo.findByCode(paymentType));
        stampTxnDetail.setTransactionAmount(amount);

        Date transDate = null;
        try {
            transDate = DateUtil.convertDateString(DateUtil.REST_DATE_FORMAT, transactionDate);
        } catch (ParseException e) {
            LOGGER.error("Error Parsing date parameter {} for transaction no {} :: {}  ", transactionDate, transactionNo, e.getMessage());
        }

        PromoRewardDto reward = retrieveStampPromotion(stampTxnModel, member.getAccountId(), paymentType, storeCode, transactionNo,
                amount, cardNumber, transDate, audits, itemList);

        //store earned points so it can be accumulated
        returnMessage.setEarnedStamps(reward.getStamps());

        stampTxnDetail.setStampsPerMedia(reward.getStamps());
        stampTxnHeader.setTotalTxnStamps(stampTxnHeader.getTotalTxnStamps() + stampTxnDetail.getStampsPerMedia());
        stampTxnDetail.setStampTxnHeader(stampTxnHeader);
        stampTxnDetail.setCreateUser(currUser);
        stampTxnDetail.setCreated(currDateTime);

        //check if member is supplementary then use primary member for processing
        if (StringUtils.isNotBlank(member.getParentAccountId())) {
            member = memberRepo.findByAccountId(member.getParentAccountId());
        }

        stampTxnHeader.getTransactionDetails().add(stampTxnDetail);
        stampTxnHeader.setMemberModel(member);
        stampTxnModel.setTransactionType(StampTxnType.EARN);
    }

    private String handleNull(String str) {

        if (null == str || str.equalsIgnoreCase("null")) {
            return null;
        }

        return str;
    }

    private void saveTargets(PromotionDto inDto, Promotion inModel) {

        deletePreviousTargetsStores(inModel);
        deletePreviousTargetsMembers(inModel);
        deletePreviousTargetsProducts(inModel);
        deletePreviousTargetsPaymentTypes(inModel);
        inModel.setTargetStoreGroups(null);
        inModel.setTargetMemberGroups(null);
        inModel.setTargetPaymentTypes(null);
        inModel.setTargetProductGroups(null);
        promotionRepo.save(inModel);

        if (inDto.getTargetMemberGroups() != null && inDto.getTargetMemberGroups().size() > 0) {
            inModel.setTargetMemberGroups(new HashSet<PromotionMemberGroup>());

            for (Long theId : inDto.getTargetMemberGroups()) {
                PromotionMemberGroup thePGrp = new PromotionMemberGroup();
                thePGrp.setPromotion(inModel);
                thePGrp.setMemberGroup(memberGroupRepo.findOne(theId));
                thePGrp.setId(inModel.getId() + "" + theId);
                inModel.getTargetMemberGroups().add(promotionMemberGroupRepo.save(thePGrp));
            }


        }

        if (inDto.getTargetPaymentTypes() != null && inDto.getTargetPaymentTypes().size() > 0) {
            inModel.setTargetPaymentTypes(new HashSet<PromotionPaymentType>());

            for (String theId : inDto.getTargetPaymentTypes()) {
                PromotionPaymentType thePGrp = new PromotionPaymentType();
                thePGrp.setPromotion(inModel);
                thePGrp.setLookupDetail(lookupDetailRepo.findByCode(theId));
                thePGrp.setId(inModel.getId() + "" + theId);
                thePGrp.setWildcards(inDto.getTargetWildcards().get(theId));

                inModel.getTargetPaymentTypes().add(promotionPaymentTypeRepo.save(thePGrp));
            }
        }

        if (inDto.getTargetStoreGroups() != null && inDto.getTargetStoreGroups().size() > 0) {
            inModel.setTargetStoreGroups(new HashSet<PromotionStoreGroup>());

            for (Long theId : inDto.getTargetStoreGroups()) {
                PromotionStoreGroup thePGrp = new PromotionStoreGroup();
                thePGrp.setPromotion(inModel);
                thePGrp.setStoreGroup(storeGroupRepo.findOne(theId));
                thePGrp.setId(inModel.getId() + "" + theId);
                inModel.getTargetStoreGroups().add(promotionStoreGroupRepo.save(thePGrp));
            }
        }

        if (inDto.getTargetProductGroups() != null && inDto.getTargetProductGroups().size() > 0) {
            inModel.setTargetProductGroups(new HashSet<PromotionProductGroup>());

            for (Long theId : inDto.getTargetProductGroups()) {
                PromotionProductGroup thePGrp = new PromotionProductGroup();
                thePGrp.setPromotion(inModel);
                thePGrp.setProductGroup(productGroupRepo.findOne(theId));
                thePGrp.setId(inModel.getId() + "" + theId);
                inModel.getTargetProductGroups().add(promotionProductGroupRepo.save(thePGrp));
            }
        }
    }

    private void saveRewards(PromotionDto inDto, Promotion model) {
        List<Long> ids = new ArrayList<Long>();
        if (null != inDto.getPromotionRewards()) {
            PromotionReward theReward = null;
            for (PromotionRewardDto theDto : inDto.getPromotionRewards()) {
                if (null == theDto.getBaseAmount() && null == theDto.getBaseFactor() && null == theDto.getDiscount()
                        && null == theDto.getMaxAmount() && null == theDto.getMaxBonus() && null == theDto.getMaxQty()
                        && null == theDto.getMinAmount() && null == theDto.getMinQty() && null == theDto.getProductCode()) {
                    continue;
                }
                theReward = (null != theDto.getId()) ?
                        promotionRewardRepo.findOne(theDto.getId()) :
                        null;
                theReward = theDto.toModel(theReward);
                theReward.setPromotion(model);
                theReward = promotionRewardRepo.save(theReward);
                ids.add(theReward.getId());
            }
            if (null == theReward) {
                model.setRewardType(null);
                promotionRepo.save(model);
            }
        }

        model = promotionRepo.findOne(model.getId());
        if (null != model && CollectionUtils.isNotEmpty(model.getPromotionRewards())) {
            List<Long> idsToDelete = new ArrayList<Long>();
            for (PromotionReward reward : model.getPromotionRewards()) {
                if (!ids.contains(reward.getId())) {
                    idsToDelete.add(reward.getId());
                }
            }
            if (CollectionUtils.isNotEmpty(idsToDelete)) {
                promotionRewardRepo.deleteInBatch(promotionRewardRepo.findAll(idsToDelete));
            }
        }
    }

    private void saveRedemptions(PromotionDto inDto, Promotion model) {
        List<Long> ids = new ArrayList<Long>();
        if (null != inDto.getPromotionRedemptions()) {
            PromotionRedemption redemption = null;
            for (PromotionRedemptionDto theDto : inDto.getPromotionRedemptions()) {
                if (null == theDto.getSku() && null == theDto.getQty() && null == theDto.getRedeemFrom()
                        && null == theDto.getRedeemTo()) {
                    continue;
                }
                redemption = (null != theDto.getId()) ?
                        promotionRedemptionRepo.findOne(theDto.getId()) :
                        null;
                redemption = theDto.toModel(redemption);
                redemption.setPromotion(model);
                redemption = promotionRedemptionRepo.save(redemption);
                ids.add(redemption.getId());
            }
            if (null == redemption) {
                model.setRewardType(null);
                promotionRepo.save(model);
            }
        }

        model = promotionRepo.findOne(model.getId());
        if (null != model && CollectionUtils.isNotEmpty(model.getPromotionRedemptions())) {
            List<Long> idsToDelete = new ArrayList<Long>();
            for (PromotionRedemption reward : model.getPromotionRedemptions()) {
                if (!ids.contains(reward.getId())) {
                    idsToDelete.add(reward.getId());
                }
            }
            if (CollectionUtils.isNotEmpty(idsToDelete)) {
                promotionRedemptionRepo.deleteInBatch(promotionRedemptionRepo.findAll(idsToDelete));
            }
        }
    }

    private void deletePreviousTargetsProducts(Promotion promotion) {
        QPromotionProductGroup qPromotionProductGroup = QPromotionProductGroup.promotionProductGroup;
        promotionProductGroupRepo.delete(promotionProductGroupRepo.findAll(qPromotionProductGroup.promotion.id.eq(promotion.getId())));
    }

    private void deletePreviousTargetsStores(Promotion promotion) {
        QPromotionStoreGroup qPromotionStoreGroup = QPromotionStoreGroup.promotionStoreGroup;
        promotionStoreGroupRepo.delete(promotionStoreGroupRepo.findAll(qPromotionStoreGroup.promotion.id.eq(promotion.getId())));
    }

    private void deletePreviousTargetsMembers(Promotion promotion) {
        QPromotionMemberGroup qPromotionMemberGroup = QPromotionMemberGroup.promotionMemberGroup;
        promotionMemberGroupRepo.delete(promotionMemberGroupRepo.findAll(qPromotionMemberGroup.promotion.id.eq(promotion.getId())));
    }

    private void deletePreviousTargetsPaymentTypes(Promotion promotion) {
        QPromotionPaymentType qPromotionPaymentType = QPromotionPaymentType.promotionPaymentType;
        promotionPaymentTypeRepo.delete(promotionPaymentTypeRepo.findAll(qPromotionPaymentType.promotion.id.eq(promotion.getId())));
    }

    private void callPortaEarnStampWS(StampTxnModel stampTxnModel) {
        StampPromoPortaEarnRestDto stampDto = new StampPromoPortaEarnRestDto(String.valueOf(stampTxnModel.getTransactionHeader().getMemberModel().getId()),
                stampTxnModel.getTransactionHeader().getStoreCode(), stampTxnModel.getTransactionHeader().getTransactionNo(),
                stampTxnModel.getCreated(), stampTxnModel.getTransactionHeader().getTotalTxnStamps(), null);
        for (StampTxnDetail txnDetail : stampTxnModel.getTransactionHeader().getTransactionDetails()) {
            StampPromoPortaEarnDetailsDto earnDetail = new StampPromoPortaEarnDetailsDto(stampTxnModel.getStampPromotion());
            earnDetail.setEarnedStamps(txnDetail.getStampsPerMedia());
            stampDto.getEarnDetails().add(earnDetail);
        }
        stampPromoService.callPortaEarnStampPromo(stampDto);
    }

}
