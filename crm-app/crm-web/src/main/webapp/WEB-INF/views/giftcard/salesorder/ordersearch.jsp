<%@ include file="../../common/taglibs.jsp"%>

<style type="text/css">
<!--
  .well .input {
    background-color: #fff;
    border: 1px solid #E8E8E8;
    box-shadow: none;
    padding: 6px;
    height: 18px;
    margin-left: 5px;
  }
  .well .btn-primary {
    margin-left: 10px;
  }
  .well select {
    margin: 5px;
  }
  #orderDateFrom {
    margin-right: 5px;
  }

-->
</style>

<form:form id="orderSearchDto" cssClass="">
<div class="well clearfix">
<div class="form-inline form-search" id="soSearchForm">
	<spring:message code="label_search" var="searchLabel"/>
	<label class="label-single">${searchLabel}:</label>
	<select id="criteria" class="searchField">
		<option value=""><spring:message code="label_criteria" arguments="${searchLabel}" /></option>
		<c:forEach items="${orderSearchCriteria}" var="criteria">
		<option value="${criteria.field}"><spring:message code="sales_order_search_${criteria.field}" /></option>
		</c:forEach>
	</select>
	<input type="text" id="criteriaValue" class="input searchField" placeholder="${searchLabel}"/>
	<input id="search" type="button" class="btn btn-primary" value="<spring:message code="button_search"/>"/>
	<input type="button" id="clear" value="<spring:message code="label_clear" />" class="btn custom-reset" data-default-id="search" />
</div>
<div class="form-inline form-search">
	<label class="label-single"><spring:message code="label_filter" />:</label>
    <input type="text" name="orderDateFrom" id="orderDateFrom" class="input searchField" placeholder="<spring:message code="sales_order_search_date_from" />"/>
    <input type="text" name="orderDateTo" id="orderDateTo" class="input searchField" placeholder="<spring:message code="sales_order_search_date_to" />"/>
    <select name="status" id="status" class="searchField">
      <option value=""><spring:message code="gc.so.status" /></option>
      <c:forEach var="item" items="${orderStatusCriteria}">
        <option value="${item}"><spring:message code="gc.so.status.${item}" /></option>
      </c:forEach>
    </select>
    <select name="orderType" id="orderType" class="searchField">
      <option value=""><spring:message code="sales_order_type" /></option>
      <c:forEach var="item" items="${orderTypeCriteria}">
        <option value="${item}"><spring:message code="sales_order_type_${item}" /></option>
      </c:forEach>
    </select>
</div>
</div>
</form:form>

<script>
$(document).ready(function() {
	$orderSearchForm = $("#orderSearchDto");

	$("#orderDateFrom").datepicker({
    	autoclose : true,
		format: 'dd-mm-yyyy'
	}).on('changeDate', function(selected){
		startDate = new Date(selected.date.valueOf());
        startDate.setDate(startDate.getDate(new Date(selected.date.valueOf())));
        $('#orderDateTo').datepicker('setStartDate', startDate);
	});
	$("#orderDateTo").datepicker({
	  	autoclose : true,
		format: 'dd-mm-yyyy'
	}).on('changeDate', function(selected){
		FromEndDate = new Date(selected.date.valueOf());
        FromEndDate.setDate(FromEndDate.getDate(new Date(selected.date.valueOf())));
        $('#orderDateFrom').datepicker('setEndDate', FromEndDate);
	});

	$("#clear", $orderSearchForm).click(function() {
		$(".searchField", $orderSearchForm).val("");
	});

	$("#search", $orderSearchForm).click(function() {
		$("#criteriaValue", $orderSearchForm).attr("name", $("#criteria", $orderSearchForm).val());
		var formDataObj = $orderSearchForm.toObject( { mode : 'first', skipEmpty : true } );
		console.log(formDataObj);
		$("#sales_order_list_container").ajaxDataTable('search', formDataObj);
	});
});


function reloadOrderTable() {
	$("#search", $("#orderSearchDto")).click();
	$("#orderDialog").modal("hide");
}
</script>
