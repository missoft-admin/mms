package com.transretail.crm.schedule.trigger;


import java.text.ParseException;

import javax.annotation.Resource;

import org.quartz.CronExpression;
import org.quartz.JobDetail;
import org.quartz.Scheduler;
import org.quartz.impl.triggers.CronTriggerImpl;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.quartz.JobDetailAwareTrigger;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.transretail.crm.common.service.exception.GenericServiceException;
import com.transretail.crm.core.entity.ApplicationConfig;
import com.transretail.crm.core.entity.enums.AppConfigDefaults;
import com.transretail.crm.core.entity.enums.AppKey;
import com.transretail.crm.core.repo.ApplicationConfigRepo;


@Service("marketingChannelSchedulerTrigger")
public class MarketingChannelSchedulerTrigger extends CronTriggerImpl implements InitializingBean {

	private static final long serialVersionUID = 1L;

	public static final String TRIGGER_NAME = "MARKETING_CHANNEL_JOB_TRIGGER";
    public static final String TRIGGER_GROUP = Scheduler.DEFAULT_GROUP;

    @Resource(name = "marketingChannelJob")
    private JobDetail jobDetail;

    @Autowired
    private ApplicationConfigRepo applicationConfigRepo;



    @Override
    public void afterPropertiesSet() throws Exception {
        getJobDataMap().put( JobDetailAwareTrigger.JOB_DETAIL_KEY, jobDetail );
        setName( TRIGGER_NAME );
        setGroup( TRIGGER_GROUP );
        super.setCronExpression( getCronExpression() );
        setJobKey( jobDetail.getKey() );
    }

    @Override
    @Transactional(readOnly = true)
    public String getCronExpression() {
        ApplicationConfig config = applicationConfigRepo.findByKey( AppKey.MARKETING_CHANNEL );
        if ( null == config ) {
        	config= new ApplicationConfig();
        	config.setKey( AppKey.MARKETING_CHANNEL );
        	config.setValue( AppConfigDefaults.DEFAULT_MARKETING_CHANNEL_CRON );
        	applicationConfigRepo.save( config );
        }
        return config != null ? config.getValue() : AppConfigDefaults.DEFAULT_MARKETING_CHANNEL_CRON;
    }

    @Override
    @Transactional
    public void setCronExpression(String cronExpression) throws ParseException {
        if ( !CronExpression.isValidExpression( cronExpression ) ) {
            throw new GenericServiceException( "Invalid cron expression [" + cronExpression + "]." );
        }
        super.setCronExpression( cronExpression );
        ApplicationConfig config = applicationConfigRepo.findByKey( AppKey.MARKETING_CHANNEL );
        if ( null == config ) {
        	config= new ApplicationConfig();
        	config.setKey( AppKey.MARKETING_CHANNEL );
        }
        config.setValue( cronExpression );
        applicationConfigRepo.save( config );
    }
}
