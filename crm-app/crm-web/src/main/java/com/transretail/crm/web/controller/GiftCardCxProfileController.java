package com.transretail.crm.web.controller;


import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Lists;
import com.transretail.crm.common.service.dto.request.PageSortDto;
import com.transretail.crm.common.service.dto.response.ResultList;
import com.transretail.crm.core.dto.ApprovalRemarkDto;
import com.transretail.crm.core.service.CodePropertiesService;
import com.transretail.crm.core.service.LookupService;
import com.transretail.crm.giftcard.dto.GiftCardCxProfileDto;
import com.transretail.crm.giftcard.dto.GiftCardCxProfileSearchDto;
import com.transretail.crm.giftcard.entity.support.CustomerProfileType;
import com.transretail.crm.giftcard.entity.support.SalesOrderType;
import com.transretail.crm.giftcard.service.GiftCardCxProfileService;
import com.transretail.crm.web.controller.util.ControllerResponse;


@Controller
@RequestMapping("/giftcard/customer/profile")
public class GiftCardCxProfileController {

	private static final String PATH_MAIN 		= "/giftcard/customer/profile";
	private static final String PATH_FORM 		= "/giftcard/customer/profile/form";
	private static final String PATH_SAVE 		= "/giftcard/customer/profile/save";
	private static final String PATH_UPDATE		= "/giftcard/customer/profile/update/";

	private static final String UIATTR_FORM		= "cxProfile";
	private static final String UIATTR_ACTION	= "cxProfileAction";
	private static final String UIATTR_HEADER	= "cxProfileHeader";

	GiftCardCxProfileService service;
	LookupService lookupService;
	CodePropertiesService codePropertiesService;
    private MessageSource messageSource;

    /* ------- IoC with setter ---------- */
    @Autowired
    public void setService(GiftCardCxProfileService service) {
        this.service = service;
    }
    @Autowired
    public void setLookupService(LookupService lookupService) {
        this.lookupService = lookupService;
    }
    @Autowired
    public void setCodePropertiesService(CodePropertiesService codePropertiesService) {
        this.codePropertiesService = codePropertiesService;
    }
    @Autowired
    public void setMessageSource(MessageSource messageSource) {
        this.messageSource = messageSource;
    }


    @InitBinder
	void initBinder( WebDataBinder binder ) {
		DateFormat theFormat = new SimpleDateFormat( "dd MMM yyyy" );
		theFormat.setLenient( false );
		binder.registerCustomEditor( Date.class, new CustomDateEditor( theFormat, true ) );
	}

    @RequestMapping( produces = "text/html" )
    public ModelAndView showMain() {
    	ModelAndView mv = new ModelAndView( PATH_MAIN );
    	populateSearchFields( mv );
    	populateLinks( mv );
        return mv;
    }

    @RequestMapping( value="/create", produces = "text/html" )
    public ModelAndView create() {
    	ModelAndView mv = new ModelAndView( PATH_FORM );
    	mv.addObject( UIATTR_FORM, new GiftCardCxProfileDto() );
    	mv.addObject( UIATTR_ACTION, PATH_SAVE );
    	mv.addObject( UIATTR_HEADER, messageSource.getMessage( "gc.cxprofile.add", null, LocaleContextHolder.getLocale() ) );
    	populateFormFields( mv );
    	return mv;
    }

    @RequestMapping( value="/edit/{id}", produces = "text/html" )
    public ModelAndView edit( @PathVariable(value="id") Long id ) {
    	ModelAndView mv = new ModelAndView( PATH_FORM );
    	mv.addObject("edit",true);
    	mv.addObject( UIATTR_FORM, service.getDto( id ) );
    	mv.addObject( UIATTR_ACTION, PATH_UPDATE + id );
    	mv.addObject( UIATTR_HEADER, messageSource.getMessage( "gc.cxprofile.edit", null, LocaleContextHolder.getLocale() ) );
    	populateFormFields( mv );
    	return mv;
    }

    @RequestMapping( value="/save", produces="application/json; charset=utf-8", method=RequestMethod.POST )
	public @ResponseBody ControllerResponse save(@ModelAttribute(UIATTR_FORM) GiftCardCxProfileDto dto, BindingResult br)
    {

    	ControllerResponse resp = validate(dto, br);
		if ( resp.isSuccess() ) {
			dto = service.saveDto( dto );
    		resp.setResult( dto );
		}
		return resp;
	}

    @RequestMapping( value="/update/{id}", produces="application/json; charset=utf-8", method=RequestMethod.POST )
	public @ResponseBody ControllerResponse update(
			@PathVariable(value="id") Long id,
			@ModelAttribute(UIATTR_FORM) GiftCardCxProfileDto dto,
			BindingResult br ) {

    	ControllerResponse resp = validate( dto, br );
		if ( resp.isSuccess() ) {
			dto.setId( id );
			dto = service.updateDto( dto );
    		resp.setResult( dto );
		}
		return resp;
	}

    @RequestMapping(value = "/list", method = RequestMethod.POST)
    public @ResponseBody ResultList<GiftCardCxProfileDto> list( @RequestBody GiftCardCxProfileSearchDto dto ) {
    	return service.search( dto );
    }

    @RequestMapping(value = "/list/{orderType}/{store}", method = RequestMethod.POST)
    public @ResponseBody ResultList<GiftCardCxProfileDto> list( @RequestBody GiftCardCxProfileSearchDto dto,
    		@PathVariable SalesOrderType orderType,
    		@PathVariable String store) {
    	if ( orderType == SalesOrderType.B2B_SALES || orderType == SalesOrderType.B2B_ADV_SALES ) {
    		dto.setCustomerTypes(new CustomerProfileType[]{ CustomerProfileType.B2B, CustomerProfileType.GENERAL } );
		}
		else if ( orderType == SalesOrderType.INTERNAL_ORDER ) {
			dto.setCustomerTypes(new CustomerProfileType[]{ CustomerProfileType.INTERNAL, CustomerProfileType.GENERAL } );
		}
    	if(!store.isEmpty() && !store.contains("HO") && !store.contains("HEAD"))
    		dto.setStoreName(store);
    	return service.search( dto );
    }

    @RequestMapping( value="/notes/save/{id}/{notes}", produces="application/json; charset=utf-8", method=RequestMethod.POST )
	public @ResponseBody ControllerResponse saveNotes(
			@PathVariable(value="id") Long id,
			@PathVariable(value="notes") String notes ) {

		ControllerResponse resp = new ControllerResponse();

		if ( null != id && StringUtils.isNotBlank( notes ) ) {
			service.saveCustomerNotes( id, notes );
			resp.setSuccess( true );
		}
		return resp;
    }

    @RequestMapping(value = "/notes/list/{id}", method = RequestMethod.POST)
    public @ResponseBody ResultList<ApprovalRemarkDto> listNotes(
    		@PathVariable("id") Long id,
    		@RequestBody PageSortDto searchDto ) {
    	return service.searchNotes( id, searchDto );
    }

    @SuppressWarnings({ "unchecked", "rawtypes" })
	@RequestMapping( value="/listbox/{orderType}/{store}/{filter}", produces="application/json; charset=utf-8", method=RequestMethod.GET )
	public @ResponseBody List<GiftCardCxProfileDto> listCustomers(
			@PathVariable(value="filter") String filter,
			@PathVariable(value="orderType") SalesOrderType orderType ,
			@PathVariable(value="store") String store) {
    	GiftCardCxProfileSearchDto searchDto = new GiftCardCxProfileSearchDto();
    	searchDto.setCxNameLike( filter );
		if ( orderType == SalesOrderType.B2B_SALES || orderType == SalesOrderType.B2B_ADV_SALES ) {
			searchDto.setCustomerTypes(
					new CustomerProfileType[]{ CustomerProfileType.B2B, CustomerProfileType.GENERAL } );
		}
		else if ( orderType == SalesOrderType.INTERNAL_ORDER ) {
			searchDto.setCustomerTypes(
					new CustomerProfileType[]{ CustomerProfileType.INTERNAL, CustomerProfileType.GENERAL } );
		}
		if(!store.isEmpty() && !store.contains("HO") && !store.contains("HEAD"))
			searchDto.setStoreName(store);
		searchDto.getPagination().setOrder( ImmutableMap.<String, Boolean>builder().put( "name", true ).build() );
    	ResultList<GiftCardCxProfileDto> dtos = list( searchDto );
		return Lists.newArrayList( CollectionUtils.isNotEmpty( dtos.getResults() )? dtos.getResults() : new ArrayList() );
    }



    private ControllerResponse validate( GiftCardCxProfileDto dto, BindingResult br ) {

        /*	if ( StringUtils.isBlank( dto.getCustomerId() ) ) {
    		br.reject( messageSource.getMessage( "gc.cxprofile.msg.reqd.customerid", null, LocaleContextHolder.getLocale() ) );
    	} else if(service.checkForDuplicateCustId(dto)) {
    		br.reject( messageSource.getMessage( "gc.cxprofile.msg.unique.customerid", null, LocaleContextHolder.getLocale() ) );

    	}*/

    	if ( StringUtils.isBlank( dto.getName() ) ) {
    		br.reject( messageSource.getMessage( "gc.cxprofile.msg.reqd.customername", null, LocaleContextHolder.getLocale() ) );
    	}
    	if ( StringUtils.isBlank( dto.getInvoiceTitle() ) ) {
    		br.reject( messageSource.getMessage( "gc.cxprofile.msg.reqd.invoicettl", null, LocaleContextHolder.getLocale() ) );
    	}
    	if (StringUtils.isBlank(dto.getCustomerType())) {
            br.reject(messageSource.getMessage("gc.cxprofile.msg.reqd.customer.type", null, LocaleContextHolder.getLocale()));
        }

    	ControllerResponse resp = new ControllerResponse();
    	if ( br.hasErrors() ) {
    		resp.setResult( br.getAllErrors() );
    	}
    	resp.setSuccess( !br.hasErrors() );
    	return resp;
    }

    private void populateFormFields( ModelAndView mv ) {
    	mv.addObject( "genders", lookupService.getActiveDetailsByHeaderCode( codePropertiesService.getHeaderGender() ) );
    	mv.addObject( "customerTypes", CustomerProfileType.values() );
    	mv.addObject( "discountTypes", lookupService.getActiveDetailsByHeaderCode( codePropertiesService.getGcDiscountType() ) );
    }

    private void populateSearchFields( ModelAndView mv ) {
    	mv.addObject( "searchFields", GiftCardCxProfileSearchDto.CxProfileSearch.values() );
    	mv.addObject( "customerTypes", CustomerProfileType.values() );
    	mv.addObject( "discountTypes", lookupService.getActiveDetailsByHeaderCode( codePropertiesService.getGcDiscountType() ) );
    }

    private void populateLinks( ModelAndView mv ) {
    	mv.addObject( "listUrl", "/giftcard/customer/profile/list" );
    }

}
