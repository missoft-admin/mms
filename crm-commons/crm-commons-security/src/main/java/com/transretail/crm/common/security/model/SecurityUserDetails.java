/*
 * Copyright (c) 2012.
 * All rights reserved.
 */

package com.transretail.crm.common.security.model;

import org.springframework.security.core.userdetails.UserDetails;

/**
 * @author aco
 */
public interface SecurityUserDetails<USER_INFO extends SecurityUserInfo> extends UserDetails {

    Long getUserId();

    String getEmail();

    USER_INFO getUserInfo();

    boolean hasAuthority(String targetAuthority);

}
