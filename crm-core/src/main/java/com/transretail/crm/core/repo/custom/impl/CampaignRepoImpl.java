package com.transretail.crm.core.repo.custom.impl;


import java.util.Date;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import com.mysema.query.BooleanBuilder;
import com.mysema.query.jpa.impl.JPAQuery;
import com.transretail.crm.core.entity.Campaign;
import com.transretail.crm.core.entity.Program;
import com.transretail.crm.core.entity.QCampaign;
import com.transretail.crm.core.repo.custom.CampaignRepoCustom;
import com.transretail.crm.core.util.BooleanExprUtil;


public class CampaignRepoImpl implements CampaignRepoCustom {

    @PersistenceContext
    EntityManager em;



	@Override
	public List<Campaign> find( Date inDate, String inStatus, Program inProgram ) {

		JPAQuery theQuery = new JPAQuery(em);
		QCampaign theCampaign = QCampaign.campaign;

		return theQuery.from( theCampaign ).
				where( new BooleanBuilder().orAllOf( 
						BooleanExprUtil.INSTANCE.isNumberPropertyEqual( theCampaign.program.id, inProgram.getId() ),
						BooleanExprUtil.INSTANCE.isDatePropertyLoe( theCampaign.duration.startDate, inDate ),
						BooleanExprUtil.INSTANCE.isDatePropertyGoe( theCampaign.duration.endDate, inDate ),
						BooleanExprUtil.INSTANCE.isStringPropertyEqual( theCampaign.status.code, inStatus ) ) ).
				list( theCampaign );
	}

}
