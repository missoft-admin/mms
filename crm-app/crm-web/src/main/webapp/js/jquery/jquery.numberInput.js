/**
 * JQuery plugin for text or input fields that accepts int and float.
 * Future plan:
 * - support for signed int and signed float
 * - add min and max number
 *
 * The plugin accepts the following options:
 * 1. type - int (default) for unsigned int, and float for unsigned float
 * 2. maxChar - number of characters (including +- signs) allowed.
 *
 * Sample usage:
 *
 * $('input').numberInput({
 *   "type" : "int" // Int input only
 * });
 *
 * $('input').numberInput({
 *   "type" : "float" // float input eg. 123.123
 * });
 *
 * Input field that accepts int input only and limits it's input upto 10 characters only
 * $('input').numberInput({
 *   "type" : "int",
 *   "maxChar" : 10
 * });
 */
if ( !jQuery().numberInput ) {
    (function ( $, window, document, undefined ) {
        $.fn.numberInput = function () {
            var args = arguments;
            return this.each( function () {
                var elem = this;
                var $input = $( elem );
                var options = $.fn.numberInput.options;
                if ( args[0] !== undefined ) {
                    options = $.extend( {}, options, args[0] );
                }
                var maxChar = options.maxChar;
                if ( typeof maxChar != 'number' ) {
                    throw 'maxChar is not a number';
                }
                var type = options.type;
                if ( type !== 'int' && type !== 'float' && type !== 'signed' && type !== 'whole' ) {
                    throw 'type is not either int or float';
                }

                var dataKey = 'numberInputInitialized';

                if ( $input.data( dataKey ) === undefined ) {
                    $input.data( dataKey, true );

                    // only opera doesn't trigger keydown multiple times while pressed, others don't work with keypress at all
                    $input.bind( ($.browser.opera ? "keypress" : "keydown") + ".numberInput", function ( event ) {
                        // Allow: backspace, delete, tab, escape, enter
                        if ( $.inArray( event.keyCode, [46, 8, 9, 27, 13] ) !== -1 ||
                            // Allow: Ctrl+A
                                (event.keyCode == 65 && event.ctrlKey === true) ||
                            // Allow: home, end, left, right
                                (event.keyCode >= 35 && event.keyCode <= 39) ) {
                            // let it happen, don't do anything
                            return true;
                        } else if ( event.keyCode == 190 ) { // If period
                        	if( type == 'whole' ) {
                                event.preventDefault();
                                return false;
                        	}
                            var inputVal = $input.val();
                            if ( type == 'int' || type == 'signed' || inputVal.indexOf( '.' ) != -1 || inputVal.length >= maxChar ) {
                                event.preventDefault();
                                return false;
                            }
                        } else if ( event.keyCode == 189 || event.keyCode == 173 ) {  // if negative for signed
                            var inputVal = $input.val();
                            if ( type != 'signed' || this.selectionEnd != 0 || inputVal.length >= maxChar ) {
                                event.preventDefault();
                                return false;
                            }
                        } else {
                            // Ensure that it is a number and stop the keypress
                            if ( event.shiftKey || ((event.keyCode < 48 || event.keyCode > 57) && (event.keyCode < 96 || event.keyCode > 105 ))
                                    || $input.val().length >= maxChar ) {
                                event.preventDefault();
                                return false;
                            }
                        }
                    } );
                }
            } );
        };

        $.fn.numberInput.options = {
            "type" : "int",
            "maxChar" : Number.MAX_VALUE
        };

    })( jQuery, window, document );
}