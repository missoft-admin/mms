package com.transretail.crm.web.controller;


import java.io.UnsupportedEncodingException;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.util.UriUtils;
import org.springframework.web.util.WebUtils;

import com.transretail.crm.common.reporting.jasper.JRProcessor;
import com.transretail.crm.core.dto.MessageDto;
import com.transretail.crm.core.security.util.UserUtil;
import com.transretail.crm.core.service.NameIdPairService;
import com.transretail.crm.giftcard.dto.EGiftCardInfoDto;
import com.transretail.crm.giftcard.dto.GiftCardOrderDto;
import com.transretail.crm.giftcard.dto.GiftCardOrderItemDto;
import com.transretail.crm.giftcard.dto.GiftCardOrderReceiveDto;
import com.transretail.crm.giftcard.dto.GiftCardOrderReceiveItemDto;
import com.transretail.crm.giftcard.dto.ProductProfileSearchDto;
import com.transretail.crm.giftcard.entity.GiftCardInventoryStock.GCIStockStatus;
import com.transretail.crm.giftcard.entity.QCardVendor;
import com.transretail.crm.giftcard.entity.support.GiftCardOrderStatus;
import com.transretail.crm.giftcard.entity.support.ProductProfileStatus;
import com.transretail.crm.giftcard.service.EGiftCardManager;
import com.transretail.crm.giftcard.service.GiftCardInventoryService;
import com.transretail.crm.giftcard.service.GiftCardInventoryStockService;
import com.transretail.crm.giftcard.service.GiftCardOrderService;
import com.transretail.crm.giftcard.service.ProductProfileService;
import com.transretail.crm.media.service.MailService;
import com.transretail.crm.web.controller.util.ControllerResponse;


@Controller
@RequestMapping( "/giftcard/egc" )
public class EGiftCardController extends AbstractReportsController {

	private static final String URL_VERIFY_MOBILEUPDATE = "/mobile/verify/update/{encryptedNewMobileNo}";

    @Autowired
    private MessageSource messageSource;
    @Autowired
    private NameIdPairService nameIdPairService;
    @Autowired
    private GiftCardOrderService giftCardOrderService;
    @Autowired
    private ProductProfileService productProfileService;
    @Autowired
    private GiftCardInventoryService inventoryService;
    @Autowired
    private GiftCardInventoryStockService inventoryStockService;
    @Autowired
    private EGiftCardManager eGiftCardManager;
    @Autowired
    private MailService mailService;



    @RequestMapping( value = "/order/form", method = RequestMethod.GET, produces = "text/html" )
    public String showForm( Model uiModel ) {
    	GiftCardOrderDto egc = new GiftCardOrderDto();
    	egc.setIsEgc( true );
    	populateOrder( egc, uiModel );
		return "view/giftcard/egc/order/form";
    }

	@RequestMapping(value = "/order/form/{id}", method = RequestMethod.GET, produces = "text/html")
    public String showForm( @PathVariable("id") String id, Model uiModel ) {
		GiftCardOrderDto dto = giftCardOrderService.getGiftCardOrder( Long.valueOf(id) );
		populateOrder( dto, uiModel );
		
		if( GiftCardOrderStatus.FOR_APPROVAL == dto.getStatus() || GiftCardOrderStatus.APPROVED == dto.getStatus() ) {
			return "view/giftcard/egc/order/form/approval";
		}
		return "view/giftcard/egc/order/form";
    }

	@RequestMapping( value = "/order/form/assign/{id}", method = RequestMethod.GET, produces = "text/html" )
	public String showAssignForm( @PathVariable("id") String id, Model uiModel ) {
		Long giftCardOrderId = Long.valueOf(id);
		GiftCardOrderDto orderDto = giftCardOrderService.getGiftCardOrder( giftCardOrderId );
		uiModel.addAttribute( "orderForm", orderDto );

		GiftCardOrderReceiveDto receiveDto = new GiftCardOrderReceiveDto();
		receiveDto.setSeries( giftCardOrderService.getGiftCardSeries( giftCardOrderId ) );
		receiveDto.setReceivedSeries( giftCardOrderService.getGiftCardReceivedSeries( giftCardOrderId ) );
		uiModel.addAttribute( "assignForm", receiveDto );
		uiModel.addAttribute( "stores", giftCardOrderService.getInventoryLocations() );

		return "view/giftcard/egc/order/form/assign";
	}

	@RequestMapping( value = "/order/save/{status}", method = RequestMethod.POST, produces = "application/json; charset=utf-8" )
	public @ResponseBody ControllerResponse saveOrder(
			@PathVariable("status") String status,
			@ModelAttribute(value = "orderForm") GiftCardOrderDto dto,
			BindingResult result, HttpServletRequest request ) {

		ControllerResponse resp = new ControllerResponse();
		validateOrder( dto, result );
		if ( result.hasErrors() ) {
			resp.setSuccess( false );
			resp.setResult( result.getAllErrors() );
			return resp;
		}
		else {
			resp.setSuccess( true );
			dto.setStatus( GiftCardOrderStatus.valueOf( status ) );
			giftCardOrderService.saveGiftCardOrder( dto );
		}
		return resp;
	}

	@RequestMapping( value = "/order/approve/{status}", method = RequestMethod.POST, produces = "application/json; charset=utf-8" )
	public @ResponseBody ControllerResponse approveOrder(
			@PathVariable("status") String status,
			@ModelAttribute(value = "orderForm") GiftCardOrderDto dto,
			BindingResult result, HttpServletRequest request ) {

		ControllerResponse resp = new ControllerResponse();
		if ( result.hasErrors() ) {
			resp.setSuccess(false);
			resp.setResult( result.getAllErrors() );
			return resp;
		}
		else {
			resp.setSuccess(true);
			giftCardOrderService.approveGiftCardOrder( dto.getId(), GiftCardOrderStatus.valueOf( status ) );
		}
		return resp;
	}

	@RequestMapping(value = "/order/assign", method = RequestMethod.POST, produces = "application/json; charset=utf-8")
	public @ResponseBody ControllerResponse assignOrder(
			@ModelAttribute(value = "orderForm") GiftCardOrderReceiveDto receiveDto,
			BindingResult result, HttpServletRequest request) {

		ControllerResponse theResponse = new ControllerResponse();
		theResponse.setSuccess(true);
		
		validateReceiveOrder(receiveDto, result);
		
		if (result.hasErrors()) {
			theResponse.setSuccess(false);
			theResponse.setResult(result.getAllErrors());
			return theResponse;
		}
		if (theResponse.isSuccess()) {
			giftCardOrderService.receiveGiftCardInventories(receiveDto);
			giftCardOrderService.updateGiftCardStatus(receiveDto.getId());
			inventoryStockService.saveStocksFromOrder( receiveDto.getItems(), GCIStockStatus.ASSIGNED, receiveDto.getReceivedAt() );
		}

		return theResponse;
	}

    @RequestMapping(value = "/order/assign/print")
    public void printReceive(
	    @ModelAttribute(value = "assignForm") GiftCardOrderReceiveDto receiveDto,
	    HttpServletResponse response) throws Exception {

		GiftCardOrderReceiveDto giftCardReceipt = giftCardOrderService.getGiftCardReceipt(
			receiveDto.getId(), receiveDto.getDeliveryReceipt(), receiveDto.getReceivedDate(), UserUtil.getCurrentUser().getUsername());
		JRProcessor jrProcessor = giftCardOrderService.createJrProcessor(giftCardReceipt);
		renderReport(response, jrProcessor);
    }

    @RequestMapping(value = "/info/{barcode}")
    public @ResponseBody EGiftCardInfoDto get( @PathVariable String barcode ) {
    	EGiftCardInfoDto res = null;
    	if ( StringUtils.isNotBlank( barcode ) ) {
    		res = eGiftCardManager.get( barcode );
    	}
		return res;
    }

    @RequestMapping(value = "/mobile/notify/update/{barcode}/{newMobileNo}")
    public @ResponseBody ControllerResponse notifyMobileUpdate( 
    		@PathVariable String barcode, 
    		@PathVariable String newMobileNo, 
    		HttpServletRequest req ) {

    	ControllerResponse res = new ControllerResponse();
		res.setResult( messageSource.getMessage( "gc.egc.msg.newmobileno.requestnotprocesses", null, LocaleContextHolder.getLocale() ) );
    	if ( StringUtils.isNotBlank( barcode ) && StringUtils.isNotBlank( newMobileNo ) ) {
    		EGiftCardInfoDto dto = eGiftCardManager.get( barcode );
    		
    		String infoUrl = req.getScheme() + "://" + req.getLocalAddr() + ":" + req.getLocalPort() + req.getContextPath();
    		String pathSegment = "/giftcard/egc" 
    				+ URL_VERIFY_MOBILEUPDATE.replace( "{encryptedNewMobileNo}", 
    						codeForwardSlash( encodeUrlPathSegment( eGiftCardManager.encryptMobileUpdate( barcode, newMobileNo ), req ), true ) );
    		MessageDto email = new MessageDto();
    		email.setMessage( messageSource.getMessage( "gc.egc.msg.link.newmobileno", new String[]{ dto.getMobileNo(), newMobileNo }, 
    				LocaleContextHolder.getLocale() )
    				+ "\n\n" + ( infoUrl + pathSegment ) );
    		email.setRecipients( new String[]{ dto.getEmail() } );
    		email.setSubject( messageSource.getMessage( "gc.egc.lbl.newmobileno", null, LocaleContextHolder.getLocale() ) );
    		mailService.send( email );
    		res.setResult( messageSource.getMessage( "gc.egc.msg.newmobileno.emailsent", null, LocaleContextHolder.getLocale() ) );
    		res.setSuccess( true );
    	}
		return res;
    }

    @RequestMapping(value = URL_VERIFY_MOBILEUPDATE, produces = "text/html")
    public String updateMobile( @PathVariable String encryptedNewMobileNo, Model uiModel ) {
    	if ( StringUtils.isNotBlank( encryptedNewMobileNo ) ) {
    		String newMobileNo = eGiftCardManager.proceedMobileUpdate( codeForwardSlash( encryptedNewMobileNo, false ) );
    		boolean isUpdateSuccessful = StringUtils.isNotBlank( newMobileNo );
    		uiModel.addAttribute( "isUpdateSuccessful", isUpdateSuccessful );
    		uiModel.addAttribute( "isUpdateMsg", messageSource.getMessage( 
    				isUpdateSuccessful? "gc.egc.msg.newmobileno.updated" : "gc.egc.msg.newmobileno.notupdated", 
    						isUpdateSuccessful? new String[]{ newMobileNo } : null, LocaleContextHolder.getLocale() ) );
    	}
		return "/redirect/mobile/verify/update/msg";
    }



    private void populateOrder( GiftCardOrderDto order, Model uiModel ) {
		uiModel.addAttribute( "orderForm", order );
		ProductProfileSearchDto dto = new ProductProfileSearchDto();
		dto.setStatusCode( ProductProfileStatus.APPROVED.name() );
		dto.setIsEgc( true );
		uiModel.addAttribute( "profiles", productProfileService.findDtos( dto ) );
		QCardVendor qCardVendor = QCardVendor.cardVendor;
		uiModel.addAttribute( "vendors", nameIdPairService.getNameIdPairs( null, qCardVendor, qCardVendor.id, qCardVendor.formalName, qCardVendor.defaultVendor.desc() ) );
	}

	private void validateOrder( GiftCardOrderDto dto, BindingResult result ) {
		if( StringUtils.isBlank( dto.getPoNumber() ) ) {
			result.reject(getMessage( "propkey_msg_notempty", new Object[]{"order_po_number"} ) );
		}
		if( dto.getPoDate() == null ) {
			result.reject( getMessage( "propkey_msg_notempty", new Object[]{"order_po_date"} ) );
		}

		Long totalQty = checkTotalQuantity( dto.getItems() );
		Long maxQty = new Long( 100000 );
		if( totalQty == null ) {
			result.reject(getMessage( "propkey_msg_notempty", new Object[]{"order_item_quantity"} ) );
		}
		if ( CollectionUtils.isNotEmpty( dto.getItems() ) ) {
			boolean isQtyValid = true;
			for ( GiftCardOrderItemDto item : dto.getItems() ) {
				if ( null != item.getQuantity() && item.getQuantity().compareTo( maxQty ) > 0 ) {
					isQtyValid = false;
				}
			}
			if ( !isQtyValid ) {
				result.reject( getMessage( "gc.so.err.qty.exceeds.hundredk", null ) );
			}
		}

		if( checkForDuplicateProfile( dto ) ) {
			result.reject( getMessage( "order_duplicate_profile", null ) );
		}
	}

	private void validateReceiveOrder(GiftCardOrderReceiveDto receiveDto, BindingResult result) {
		
		if ( StringUtils.isBlank( receiveDto.getReceivedAt() ) ) {
			result.reject( getMessage( "propkey_msg_notempty", new Object[]{"order_received_at"} ) );	
		}
		if ( receiveDto.getReceivedDate() == null ) {
			result.reject( getMessage( "propkey_msg_notempty", new Object[]{"order_received_date"} ) );
		}
		for( GiftCardOrderReceiveItemDto itemDto: receiveDto.getItems() ) {
			if( !giftCardOrderService.isValidReceiveOrder( itemDto, receiveDto.getId() ) ) {
				result.reject( getMessage( "order_invalid_series", null ) );
				break;
			}
		}
	}

	private Long checkTotalQuantity( List<GiftCardOrderItemDto> itemDtos ) {
		long total = 0;
		for( GiftCardOrderItemDto item: itemDtos ) {
			if( item.getQuantity() == null ) {
				return null;
			} 
			else {
				total += item.getQuantity(); 
			}
		}
		return total;
	}

	private boolean checkForDuplicateProfile( GiftCardOrderDto orderDto ) {
		List<GiftCardOrderItemDto> items = orderDto.getItems();
		if( items.size() > 1 ) {
			Collections.sort( items, new Comparator<GiftCardOrderItemDto>() {
	            @Override
	            public int compare( GiftCardOrderItemDto o1, GiftCardOrderItemDto o2 ) {
	                return o1.getProfileId().compareTo( o2.getProfileId() );
	            }
	        });
	        for( int i = 1; i < items.size(); i++ ) {
	        	if( items.get( i-1 ).getProfileId().compareTo( items.get(i).getProfileId() ) == 0 ) {
	        		return true;
	        	}
	        }	
		}
		return false;
	}

	private String getMessage(String inMsgCode, Object[] inArgMsgCodes) {
        if (inArgMsgCodes != null) {
            for (int i = 0; i < inArgMsgCodes.length; i++) {
                inArgMsgCodes[i] =
                    messageSource.getMessage(inArgMsgCodes[i].toString().toLowerCase(), null, LocaleContextHolder.getLocale());
            }
        }
        return messageSource.getMessage(inMsgCode, inArgMsgCodes, "Info given is invalid.", LocaleContextHolder.getLocale());
    }

	private String codeForwardSlash( String path, boolean isEncode ) {
		return isEncode? path.replace( "%2F", "%252F" ) : path.replace( "%2F", "/" );
	}

    String encodeUrlPathSegment( String pathSegment, HttpServletRequest httpServletRequest ) {
        String enc = httpServletRequest.getCharacterEncoding();
        if (enc == null) {
            enc = WebUtils.DEFAULT_CHARACTER_ENCODING;
        }
        try {
            pathSegment = UriUtils.encodePathSegment(pathSegment, enc);
        } catch (UnsupportedEncodingException uee) {
            uee.printStackTrace();
        }
        return pathSegment;
    }

}
