package com.transretail.crm.giftcard.dto;

import java.math.BigDecimal;
import java.util.Collection;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.Predicate;

import com.google.common.collect.Lists;
import com.mysema.query.annotations.QueryProjection;


/**
 *
 */
public class B2BExhibitionCountDto {
	
	private Long profileId;
	private String profileName;
	private Long count;
	private BigDecimal totalAmount;
	private BigDecimal faceValue;
	
	private Collection<B2BExhibitionGcDto> gcs = Lists.newArrayList();
	
	private String cardsStr;
	
	public B2BExhibitionCountDto() {}
	@QueryProjection
	public B2BExhibitionCountDto(Long profileId, String profileName, Long count,
			BigDecimal totalAmount) {
		super();
		this.profileId = profileId;
		this.profileName = profileName;
		this.count = count;
		this.totalAmount = totalAmount;
	}
	
	@QueryProjection
	public B2BExhibitionCountDto(Long profileId, String profileName, Long count,
			BigDecimal totalAmount, BigDecimal faceValue) {
		super();
		this.profileId = profileId;
		this.profileName = profileName;
		this.count = count;
		this.totalAmount = totalAmount;
		this.faceValue = faceValue;
	}
	public String getProfileName() {
		return profileName;
	}
	public void setProfileName(String profileName) {
		this.profileName = profileName;
	}
	public Long getCount() {
		return count;
	}
	public void setCount(Long count) {
		this.count = count;
	}
	public BigDecimal getTotalAmount() {
		return totalAmount;
	}
	public void setTotalAmount(BigDecimal totalAmount) {
		this.totalAmount = totalAmount;
	}
	public Long getProfileId() {
		return profileId;
	}
	public void setProfileId(Long profileId) {
		this.profileId = profileId;
	}
	public Collection<B2BExhibitionGcDto> getGcs() {
		return gcs;
	}
	
	@SuppressWarnings("unchecked")
	public void setFilteredGcs(List<B2BExhibitionGcDto> gcs) {
		this.gcs = CollectionUtils.select(gcs, new Predicate() {
			@Override
			public boolean evaluate(Object object) {
				B2BExhibitionGcDto gcDto = (B2BExhibitionGcDto) object;
				if(gcDto.getProduct().longValue() == profileId.longValue())
					return true;
				return false;
			}
		});
	}
	public BigDecimal getFaceValue() {
		return faceValue;
	}
	public void setFaceValue(BigDecimal faceValue) {
		this.faceValue = faceValue;
	}
	public String getCardsStr() {
		return cardsStr;
	}
	public void setCardsStr(String cardsStr) {
		this.cardsStr = cardsStr;
	}
	
	
}
