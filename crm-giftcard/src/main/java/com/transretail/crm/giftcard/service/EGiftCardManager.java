package com.transretail.crm.giftcard.service;


import java.util.List;
import java.util.Set;

import org.joda.time.LocalDate;

import com.transretail.crm.giftcard.dto.EGiftCardInfoDto;
import com.transretail.crm.giftcard.dto.EGiftCardProfileDto;
import com.transretail.crm.giftcard.dto.GiftCardInfo;
import com.transretail.crm.giftcard.dto.GiftCardTransactionInfo;


public interface EGiftCardManager {

	EGiftCardInfoDto update(EGiftCardInfoDto dto);
	Boolean updatePin(String barcode, String oldPin, String pin);
	Boolean validatePin(String barcode, String pin);
	Set<GiftCardInfo> inquire(String[] cardNos);
	EGiftCardInfoDto get(String barcode);
	String encryptMobileUpdate(String barcode, String newMobileNo);
	String proceedMobileUpdate(String encryptedNewMobileNo);
	Set<EGiftCardProfileDto> retrieveAvailableProfiles(String location);
	Set<GiftCardInfo> preactivate( GiftCardTransactionInfo<?> txInfo, String productCode, Long quantity );
	Set<GiftCardInfo> deallocate(GiftCardTransactionInfo<?> txInfo, Set<String> cards);
	List<GiftCardInfo> prepareMsgs(Set<GiftCardInfo> gcInfos,
			String emailAd, String mobileNo, String pin, LocalDate birthdate,
			String pinPattern, String url);

}
