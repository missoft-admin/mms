package com.transretail.crm.web.controller;


import java.math.BigDecimal;

import javax.servlet.http.HttpServletRequest;

import org.joda.time.LocalDate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.transretail.crm.common.service.dto.request.PageSortDto;
import com.transretail.crm.common.service.dto.response.ResultList;
import com.transretail.crm.giftcard.dto.GiftCardDiscountSchemeDto;
import com.transretail.crm.giftcard.entity.support.DiscountType;
import com.transretail.crm.giftcard.entity.support.SalesOrderDiscountType;
import com.transretail.crm.giftcard.service.DiscountSchemeService;
import com.transretail.crm.giftcard.service.DiscountSchemeService.DiscountDto;
import com.transretail.crm.giftcard.service.GiftCardCxProfileService;
import com.transretail.crm.web.controller.util.ControllerResponse;


@RequestMapping("/giftcard/discountscheme")
@Controller
public class DiscountSchemeController {
	@Autowired
	private DiscountSchemeService schemeService;
	@Autowired
	private GiftCardCxProfileService customerService;
	@Autowired
	private MessageSource messageSource;
	
	@RequestMapping(produces = "text/html")
    public String show(Model uiModel) {
        return "giftcard/discountscheme/list";
    }
	
	
	@RequestMapping(value = "/search", method = RequestMethod.POST)
    @ResponseBody
    public ResultList<GiftCardDiscountSchemeDto> list(@RequestBody PageSortDto sortDto) {
        return schemeService.list(sortDto);
    }
	
	@RequestMapping(value = "/form", method = RequestMethod.GET, produces = "text/html")
    public String showForm(Model uiModel) {
    	populateModel(new GiftCardDiscountSchemeDto(), uiModel);
		return "giftcard/discountscheme/form";
    }
	
	@RequestMapping(value = "/form/{id}", method = RequestMethod.GET, produces = "text/html")
    public String showForm(@PathVariable String id, Model uiModel) {
    	populateModel(schemeService.getSchemeDto(Long.valueOf(id)), uiModel);
		return "giftcard/discountscheme/form";
    }
	
	private void populateModel(GiftCardDiscountSchemeDto schemeDto, Model uiModel) {
		uiModel.addAttribute("scheme", schemeDto);
		uiModel.addAttribute("customers", customerService.getAllProfiles());
		uiModel.addAttribute("discountTypes", DiscountType.values());
	}
	
	@RequestMapping(value = "/form", method = RequestMethod.POST, produces = "application/json; charset=utf-8")
    public @ResponseBody ControllerResponse saveForm(
    		@ModelAttribute GiftCardDiscountSchemeDto schemeDto,
    		BindingResult result, HttpServletRequest request) {
		
		ControllerResponse response = new ControllerResponse();
		response.setSuccess(true);
		
		validateScheme(schemeDto, result);
		
		if (result.hasErrors()) {
			response.setSuccess(false);
			response.setResult(result.getAllErrors());
			return response;
		}

		if (response.isSuccess()) {
			schemeService.saveScheme(schemeDto);
		}

		return response;
    }
	
	private void validateScheme(GiftCardDiscountSchemeDto schemeDto, BindingResult result) {
		if(schemeDto.getCustomerId() == null) {
			result.reject(getMessage("propkey_msg_notempty", new Object[]{"gc_dcscheme_customer"}));
		}
		
		if(schemeDto.getMinPurchase() == null) {
			result.reject(getMessage("propkey_msg_notempty", new Object[]{"gc_dcscheme_minpurchase"}));
		}
		
		if(schemeDto.getPurchaseForMaxDiscount() == null) {
			result.reject(getMessage("propkey_msg_notempty", new Object[]{"gc_dcscheme_purchasetomaxdiscount"}));
		}
		
		if(schemeDto.getDiscountType().compareTo(DiscountType.REGULAR_YEARLY_DISCOUNT) != 0 && schemeDto.getStartingDiscount() == null) {
			result.reject(getMessage("propkey_msg_notempty", new Object[]{"gc_dcscheme_startingdiscount"}));
		}
		
		if(schemeDto.getEndingDiscount() == null) {
			result.reject(getMessage("propkey_msg_notempty", new Object[]{"gc_dcscheme_endingdiscount"}));
		}
		
		if(schemeDto.getStartDate() == null) {
			result.reject(getMessage("propkey_msg_notempty", new Object[]{"gc_dcscheme_startdate"}));
		}
		
		if(schemeDto.getEndDate() == null) {
			result.reject(getMessage("propkey_msg_notempty", new Object[]{"gc_dcscheme_enddate"}));
		}
		
		if(schemeDto.getStartDate() != null && schemeDto.getEndDate() != null && schemeDto.getStartDate().compareTo(schemeDto.getEndDate()) > 0) {
			result.reject(getMessage("gc_dcscheme_invalid_year_range", null));
		} else if(schemeDto.getStartDate() != null && schemeDto.getEndDate() != null 
				&& schemeService.validateForOverlappingDiscount(schemeDto.getStartDate(), schemeDto.getEndDate(), schemeDto.getCustomerId(), schemeDto.getId())) {
			result.reject(getMessage("gc_gcscheme_overlapping_scheme", null));
		}
	}
	
	private String getMessage(String inMsgCode, Object[] inArgMsgCodes) {
        if (inArgMsgCodes != null) {
            for (int i = 0; i < inArgMsgCodes.length; i++) {
                inArgMsgCodes[i] =
                    messageSource.getMessage(inArgMsgCodes[i].toString().toLowerCase(), null, LocaleContextHolder.getLocale());
            }
        }
        return messageSource.getMessage(inMsgCode, inArgMsgCodes, "Info given is invalid.", LocaleContextHolder.getLocale());
    }
	
	@RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    public @ResponseBody ControllerResponse delete(@PathVariable("id") Long id,
            Model uiModel) {
		ControllerResponse response = new ControllerResponse();
		response.setSuccess(true);
		schemeService.deleteScheme(id);
        return response;
    }
	
	
	@RequestMapping(value = "/get/{customer}/{orderDate}", method = RequestMethod.GET, produces = "application/json; charset=utf-8")
    public @ResponseBody ControllerResponse getScheme(
    		@PathVariable("customer") Long customer,
    		@PathVariable("orderDate") LocalDate orderDate,
            Model uiModel) {
		ControllerResponse response = new ControllerResponse();
		response.setSuccess(true);
		GiftCardDiscountSchemeDto scheme = schemeService.getDiscountScheme(customer, orderDate);
		if(scheme != null)
			response.setResult(scheme.getDiscountType());
		else
			response.setResult(null);
        return response;
    }
	
	@RequestMapping(value = "/get/beforeapproval/{customer}/{orderDate}/{purchase}/{discType}", method = RequestMethod.GET, produces = "application/json; charset=utf-8")
    public @ResponseBody ControllerResponse getScheme(
    		@PathVariable Long customer,
    		@PathVariable LocalDate orderDate,
    		@PathVariable BigDecimal purchase,
    		@PathVariable SalesOrderDiscountType discType) {
		ControllerResponse response = new ControllerResponse();
		response.setSuccess(true);
		DiscountDto dto = schemeService.getDiscountBeforeApproval(customer, purchase, orderDate, discType);
		if(dto != null)
			response.setResult(dto);
		else
			response.setResult(null);
        return response;
    }
	
}
