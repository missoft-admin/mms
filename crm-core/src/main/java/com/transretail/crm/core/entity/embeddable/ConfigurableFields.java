package com.transretail.crm.core.entity.embeddable;

import javax.persistence.Column;
import javax.persistence.Embeddable;

/**
 * @author Allan G. Ramirez (agramirez@exist.com)
 */
@Embeddable
public class ConfigurableFields {
    @Column(name = "CUST_FLD01", length = 1000)
    private String customField01;
    @Column(name = "CUST_FLD02", length = 1000)
    private String customField02;
    @Column(name = "CUST_FLD03", length = 1000)
    private String customField03;
    @Column(name = "CUST_FLD04", length = 1000)
    private String customField04;
    @Column(name = "CUST_FLD05", length = 1000)
    private String customField05;
    @Column(name = "CUST_FLD06", length = 1000)
    private String customField06;
    @Column(name = "CUST_FLD07", length = 1000)
    private String customField07;
    @Column(name = "CUST_FLD08", length = 1000)
    private String customField08;
    @Column(name = "CUST_FLD09", length = 1000)
    private String customField09;
    @Column(name = "CUST_FLD10", length = 1000)
    private String customField10;
    @Column(name = "CUST_FLD11", length = 1000)
    private String customField11;
    @Column(name = "CUST_FLD12", length = 1000)
    private String customField12;

    public ConfigurableFields() {
    }

    public ConfigurableFields(ConfigurableFields fields) {
        if (fields != null) {
            this.customField01 = fields.getCustomField01();
            this.customField02 = fields.getCustomField02();
            this.customField03 = fields.getCustomField03();
            this.customField04 = fields.getCustomField04();
            this.customField05 = fields.getCustomField05();
            this.customField06 = fields.getCustomField06();
            this.customField07 = fields.getCustomField07();
            this.customField08 = fields.getCustomField08();
            this.customField09 = fields.getCustomField09();
            this.customField10 = fields.getCustomField10();
            this.customField11 = fields.getCustomField11();
            this.customField12 = fields.getCustomField12();
        }
    }

    public String getCustomField01() {
        return customField01;
    }

    public void setCustomField01(String customField01) {
        this.customField01 = customField01;
    }

    public String getCustomField02() {
        return customField02;
    }

    public void setCustomField02(String customField02) {
        this.customField02 = customField02;
    }

    public String getCustomField03() {
        return customField03;
    }

    public void setCustomField03(String customField03) {
        this.customField03 = customField03;
    }

    public String getCustomField04() {
        return customField04;
    }

    public void setCustomField04(String customField04) {
        this.customField04 = customField04;
    }

    public String getCustomField05() {
        return customField05;
    }

    public void setCustomField05(String customField05) {
        this.customField05 = customField05;
    }

    public String getCustomField06() {
        return customField06;
    }

    public void setCustomField06(String customField06) {
        this.customField06 = customField06;
    }

    public String getCustomField07() {
        return customField07;
    }

    public void setCustomField07(String customField07) {
        this.customField07 = customField07;
    }

    public String getCustomField08() {
        return customField08;
    }

    public void setCustomField08(String customField08) {
        this.customField08 = customField08;
    }

    public String getCustomField09() {
        return customField09;
    }

    public void setCustomField09(String customField09) {
        this.customField09 = customField09;
    }

    public String getCustomField10() {
        return customField10;
    }

    public void setCustomField10(String customField10) {
        this.customField10 = customField10;
    }

    public String getCustomField11() {
        return customField11;
    }

    public void setCustomField11(String customField11) {
        this.customField11 = customField11;
    }

    public String getCustomField12() {
        return customField12;
    }

    public void setCustomField12(String customField12) {
        this.customField12 = customField12;
    }

}
