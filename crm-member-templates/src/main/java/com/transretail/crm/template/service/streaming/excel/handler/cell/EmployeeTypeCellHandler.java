package com.transretail.crm.template.service.streaming.excel.handler.cell;

import org.apache.commons.lang3.StringUtils;

import com.transretail.crm.core.entity.MemberModel;
import com.transretail.crm.core.entity.lookup.LookupDetail;
import com.transretail.crm.template.service.MemberTemplateParseException;
import com.transretail.crm.template.service.streaming.excel.handler.sheet.AbstractSheetHandler;

/**
 * @author Allan G. Ramirez (agramirez@exist.com)
 */
public class EmployeeTypeCellHandler implements CellHandler {
    private AbstractSheetHandler sheetHandler;

    public EmployeeTypeCellHandler(AbstractSheetHandler sheetHandler) {
        this.sheetHandler = sheetHandler;
    }

    @Override
    public void handle(MemberModel dto, String cellText) throws MemberTemplateParseException {
        LookupDetail detail = null;
        if (StringUtils.isNotBlank(cellText)) {
            detail = sheetHandler.getLoookupDetail(sheetHandler.getCodePropertiesService().getHeaderEmpType(), cellText);
        }
        dto.setEmpType(detail);
    }
}
