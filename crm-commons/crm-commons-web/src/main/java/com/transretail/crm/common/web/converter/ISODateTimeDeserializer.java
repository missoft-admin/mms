package com.transretail.crm.common.web.converter;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import java.io.IOException;
import org.joda.time.LocalDateTime;
import org.joda.time.format.ISODateTimeFormat;

/**
 *
 * @author Monte Cillo Co (mco@exist.com)
 */
public class ISODateTimeDeserializer extends JsonDeserializer<LocalDateTime> {

    @Override
    public LocalDateTime deserialize(JsonParser jp, DeserializationContext ctxt) throws IOException {
        return ISODateTimeFormat.dateTimeParser()
                .parseDateTime(jp.getText()).toLocalDateTime();
    }
}