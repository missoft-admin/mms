package com.transretail.crm.giftcard.service;

import com.transretail.crm.common.reporting.jasper.JRProcessor;
import com.transretail.crm.giftcard.dto.*;
import com.transretail.crm.giftcard.entity.GiftCardInventory;
import com.transretail.crm.giftcard.entity.support.GiftCardOrderStatus;
import org.joda.time.LocalDate;

import java.io.File;
import java.util.List;
import java.util.Map;

/**
 *
 */
public interface GiftCardOrderService {
   String STANDARD_ENCRYPTED_FILE_CONTENT_FORMAT = "%s.\t%s\t%d\t%s\t%s - %s\n";    // counter, 
    GiftCardOrderDto getGiftCardOrder(Long giftCardOrderId);

    GiftCardOrderResultList getGiftCardOrders(GiftCardOrderSearchDto searchForm);

    void saveGiftCardOrder(GiftCardOrderDto dto);

	void approveGiftCardOrder(Long id, GiftCardOrderStatus status);

	void deleteGiftCardOrder(Long giftCardOrderId);

	File printEncryptedFile(Long id);

	long receiveGiftCardInventories(GiftCardOrderReceiveDto receiveDto);

	List<GiftCardOrderSeriesDto> getGiftCardSeries(Long giftCardOrderId);

	List<GiftCardOrderReceivedSeriesDto> getGiftCardReceivedSeries(
			Long giftCardOrderId);

	boolean isValidReceiveOrder(GiftCardOrderReceiveItemDto receivedDto,
			Long giftCardOrderId);

	void updateGiftCardStatus(Long giftCardOrderId);

	List<GiftCardInventory> getInventories(List<GiftCardOrderReceiveItemDto> items);

	Map<String, String> getInventoryLocations();

	JRProcessor createJrProcessor(GiftCardOrderReceiveDto receiveDto);
	
	GiftCardOrderReceiveDto getGiftCardReceipt(Long id, String deliveryReceipt, LocalDate receiveDate, String receivedBy);
	
	JRProcessor createReprintTransactionJrProcessor(String orderId);

	boolean isExistBarcodingOrder();

	List<GiftCardOrderReceivedSeriesDto> getGiftCardReceipt(Long giftCardOrderId);

    JRProcessor createPendingMOReceiptReport();
}
