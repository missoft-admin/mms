package com.transretail.crm.core.service.impl;

import com.transretail.crm.common.service.dto.rest.ReturnMessage;
import com.transretail.crm.core.entity.MemberModel;
import com.transretail.crm.core.entity.PointsTxnModel;
import com.transretail.crm.core.entity.embeddable.CustomerProfile;
import com.transretail.crm.core.entity.enums.MemberStatus;
import com.transretail.crm.core.entity.enums.PointTxnType;
import com.transretail.crm.core.entity.enums.TxnStatus;
import com.transretail.crm.core.repo.MemberRepo;
import com.transretail.crm.core.repo.PointsTxnModelRepo;
import com.transretail.crm.core.service.PointsTxnManagerService;
import com.transretail.crm.core.util.generator.IdGeneratorService;
import org.joda.time.DateTime;
import org.joda.time.LocalDate;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Configurable;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;

import static com.transretail.crm.core.entity.enums.PointTxnType.*;
import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertTrue;

@Configurable
@ContextConfiguration(locations = "classpath*:/META-INF/spring/applicationContext*.xml")
@Transactional
@RunWith(SpringJUnit4ClassRunner.class)
public class PointsExpireTest {
	@Autowired
	private PointsTxnModelRepo pointsRepo;
	@Autowired
	private MemberRepo memberRepo;
	@Autowired
	private PointsTxnManagerService pointsMangerService;
	@Autowired
	private IdGeneratorService customIdGeneratorService;
	
	private MemberModel member;
	private DateTimeFormatter formatter;
	
	@Before
	public void setUp() {
		setUpMembers();
		formatter = DateTimeFormat.forPattern("MM-dd-yyyy");
	}
	
	private void setUpMembers() {
		member = new MemberModel();
		member.setAccountId("test");
		member.setAccountStatus(MemberStatus.ACTIVE);
		member.setEnabled(true);
		member.setFirstName("test");
		member.setUsername("test");
		member.setPin("1");
		member.setContact("1");
		member.setTotalPoints(0D);
		CustomerProfile profile = new CustomerProfile();
		profile.setBirthdate(new Date());
		member.setCustomerProfile(profile);
		
		member = memberRepo.save(member);
	}
	
	private void createPointsTxn(PointTxnType type, String transactionDate, Double transactionPoints) {
		member.setTotalPoints(member.getTotalPoints() + transactionPoints);
		LocalDate date = LocalDate.parse(transactionDate, formatter);
		
		PointsTxnModel points = new PointsTxnModel();
		points.setId(customIdGeneratorService.generateId());
		points.setMemberModel(member);
		points.setTransactionType(type);
		points.setTransactionPoints(transactionPoints);
		points.setTransactionDateTime(date.toDate());
		points.setTransactionNo(type.toString() + "_" + date.toString("ddMMyyyy"));
		points.setCreated(date.toDateTimeAtStartOfDay());
		points.setStatus(TxnStatus.ACTIVE);
		
		switch(type) {
			case ADJUST:
				if(transactionPoints < 0) break;
			case EARN:
			case RETURN:
			case VOID:
			case REWARD:
				points.setExpiryDate(date.plusMonths(2));
				break;
			default:
				points.setExpiryDate(null);
		}
		
		member = memberRepo.save(member);
		pointsRepo.save(points);
	}
	
	/**
	 * 	Type	Transaction Date	Points	Expiry Date
	1	EARN		01/01/14		 5000	03/01/14
	2	VOID		01/01/14		-5000	03/01/14
	3	EARN		01/05/14		10000	03/05/14
	4	EARN		01/05/14		 5000	03/05/14
	5	ADJUST		01/10/14		-1000	
	6	REDEEM		01/15/14		-1000	
	7	REDEEM		01/20/14		 -500	
	8	ADJUST		01/25/14		  500	03/25/14
	9	REDEEM		01/30/14		-1000	
	10	EARN		02/04/14		 3000	04/04/14
	11	REDEEM		02/09/14		-1500	
	12	REDEEM		02/14/14		 -500	
	13	EARN		02/19/14		  500	04/19/14
	14	ADJUST		02/24/14		  500	04/24/14
	Current TOTAL POINTS: 14000
	 */
	private void passThrough1() {
		createPointsTxn(EARN, 	"01-01-2014", 5000D);
		createPointsTxn(VOID, 	"01-01-2014", -5000D);
		createPointsTxn(EARN, 	"01-05-2014", 10000D);
		createPointsTxn(EARN, 	"01-05-2014", 5000D);
		createPointsTxn(ADJUST, "01-10-2014", -1000D);
		createPointsTxn(REDEEM, "01-15-2014", -1000D);
		createPointsTxn(REDEEM, "01-20-2014", -500D);
		createPointsTxn(ADJUST, "01-25-2014", 500D);
		createPointsTxn(REDEEM, "01-30-2014", -1000D);
		createPointsTxn(EARN, 	"02-04-2014", 3000D);
		createPointsTxn(REDEEM, "02-09-2014", -1500D);
		createPointsTxn(REDEEM, "02-14-2014", -500D);
		createPointsTxn(EARN, 	"02-19-2014", 500D);
		createPointsTxn(ADJUST, "02-24-2014", 500D);
	}
	
	/**
	 * 	REDEEM	03/10/14	-1000
		REDEEM	03/15/14	-2000
		EARN	03/20/14	 5000
		EARN	03/20/14	10000
		REDEEM	03/25/14	-1500
		Current Total points: 15000
	 */
	private void passThrough2() {
		createPointsTxn(REDEEM, "03-10-2014", -1000D);
		createPointsTxn(REDEEM, "03-15-2014", -2000D);
		createPointsTxn(EARN, 	"03-20-2014",  5000D);
		createPointsTxn(EARN, 	"03-20-2014", 10000D);
		createPointsTxn(REDEEM, "03-25-2014", -1500D);
	}
	
	/**
	 * 	REDEEM	03/30/14	-1000
		Current Total Points: 14000
	 */
	private void passThrough3() {
		createPointsTxn(REDEEM, "03-30-2014", -1000D);
	}
	
	/**
	 * 	REDEEM	04/30/14	-2500
		REDEEM	05/10/14	-1500
		REDEEM	05/15/14	-2500
		Current Total Points: 7500
	 */
	private void passThrough4() {
		createPointsTxn(REDEEM, "04-30-2014", -2500D);
		createPointsTxn(REDEEM, "05-10-2014", -1500D);
		createPointsTxn(REDEEM, "05-15-2014", -2500D);
	}
	
	/** Type	Date		Point	ID	Excess
	 	EXPIRE	03/01/14		0	17	-5500
		EXPIRE	03/05/14	-9500	17		0
		EXPIRE	03/25/14		0	26	-4000
		EXPIRE	04/04/14		0	28	-2000
		EXPIRE	04/19/14		0	28	-1500
		EXPIRE	04/24/14		0	28	-1000
		EXPIRE	05/20/14	-7500	34		0
		Target End Points: 0
	 */
	@Test
	public void testExpirePoints() {
//		System.out.println(member.getAccountId());
		
		passThrough1();
		assertEquals(14000D, member.getTotalPoints());
		assertEquals(14000D, pointsMangerService.synchronizePointsTxn(member));
		
		List<PointsTxnModel> list = pointsRepo.hasExistingExpirePoints(member.getAccountId(), LocalDate.parse("03-01-2014", formatter));
		assertEquals(0, list.size());
		
		// expire 1
		PointsTxnModel expirePoints = pointsMangerService.expirePoints(member, LocalDate.parse("03-01-2014", formatter));
		assertEquals(14000D, member.getTotalPoints());
		assertEquals(14000D, pointsMangerService.synchronizePointsTxn(member));
		assertEquals(-5500D, expirePoints.getExpireExcess());
//		assertEquals(0D, expirePoints.getTransactionPoints());
		
//		expirePoints = pointsMangerService.expirePoints(member, LocalDate.parse("03-01-2014", formatter));
//		assertNull(expirePoints);
//		pointsRepo.flush();
//		list = pointsRepo.hasExistingExpirePoints(member.getAccountId(), LocalDate.parse("03-01-2014", formatter));
//		assertEquals(3, list.size());
		
		// expire 2
		expirePoints = pointsMangerService.expirePoints(member, LocalDate.parse("03-05-2014", formatter));
		assertEquals(4500D, member.getTotalPoints());
		assertEquals(4500D, pointsMangerService.synchronizePointsTxn(member));
		assertEquals(0D, expirePoints.getExpireExcess());
//		assertEquals(-9500D, expirePoints.getTransactionPoints());
		
		passThrough2();
		assertEquals(15000D, member.getTotalPoints());
		assertEquals(15000D, pointsMangerService.synchronizePointsTxn(member));
		
		// expire 3
		expirePoints = pointsMangerService.expirePoints(member, LocalDate.parse("03-25-2014", formatter));
		assertEquals(15000D, member.getTotalPoints());
		assertEquals(15000D, pointsMangerService.synchronizePointsTxn(member));
		assertEquals(-4000D, expirePoints.getExpireExcess());
//		assertEquals(0D, expirePoints.getTransactionPoints());
		
		passThrough3();
		assertEquals(14000D, member.getTotalPoints());
		assertEquals(14000D, pointsMangerService.synchronizePointsTxn(member));
		
		// expire 4
		expirePoints = pointsMangerService.expirePoints(member, LocalDate.parse("04-04-2014", formatter));
		assertEquals(14000D, member.getTotalPoints());
		assertEquals(14000D, pointsMangerService.synchronizePointsTxn(member));
		assertEquals(-2000D, expirePoints.getExpireExcess());
//		assertEquals(0D, expirePoints.getTransactionPoints());
		
		// expire 5
		expirePoints = pointsMangerService.expirePoints(member, LocalDate.parse("04-19-2014", formatter));
		assertEquals(14000D, member.getTotalPoints());
		assertEquals(14000D, pointsMangerService.synchronizePointsTxn(member));
		assertEquals(-1500D, expirePoints.getExpireExcess());
//		assertEquals(0D, expirePoints.getTransactionPoints());
		
		// expire 6
		expirePoints = pointsMangerService.expirePoints(member, LocalDate.parse("04-24-2014", formatter));
		assertEquals(14000D, member.getTotalPoints());
		assertEquals(14000D, pointsMangerService.synchronizePointsTxn(member));
		assertEquals(-1000D, expirePoints.getExpireExcess());
//		assertEquals(0D, expirePoints.getTransactionPoints());
		
		passThrough4();
		assertEquals(7500D, member.getTotalPoints());
//		assertEquals(7500D, pointsMangerService.synchronizePointsTxn(member));
		// expire 7
		expirePoints = pointsMangerService.expirePoints(member, LocalDate.parse("05-20-2014", formatter));
		assertEquals(0D, member.getTotalPoints());
		assertEquals(0D, pointsMangerService.synchronizePointsTxn(member));
		assertEquals(0D, expirePoints.getExpireExcess());
		
//		for(PointsTxnModel txn : pointsRepo.findAll(QPointsTxnModel.pointsTxnModel.transactionType.eq(PointTxnType.EXPIRE))) {
//			System.out.printf("%s %s %f %f\n", txn.getTransactionDateTime().toString(), txn.getId(), txn.getTransactionPoints(), txn.getExpireExcess());
//		}
//		assertEquals(-7500D, expirePoints.getTransactionPoints());
	}
	
	@Test
	public void testSetExpiryDate() {
		DateTime date = DateTime.now();
		PointsTxnModel points = new PointsTxnModel();
		String id = customIdGeneratorService.generateId();
		points.setId(id);
		points.setMemberModel(member);
		points.setTransactionType(EARN);
		points.setTransactionPoints(1000D);
		points.setTransactionDateTime(date.toDate());
		points.setCreated(date);
		points.setStatus(TxnStatus.ACTIVE);
		ReturnMessage returnMessage = new ReturnMessage();
		assertTrue(pointsMangerService.processPoints(points, returnMessage));
		points = pointsMangerService.retrievePointsById(id);
		DateTime expected = date.plusMonths(25);
		if(expected.getDayOfMonth() > 1) {
			expected = expected.plusMonths(1).dayOfMonth().withMinimumValue();
		}
		assertEquals(expected.toLocalDate(), points.getExpiryDate());
		
		points = new PointsTxnModel();
		id = customIdGeneratorService.generateId();
		points.setId(id);
		points.setMemberModel(member);
		points.setTransactionType(ADJUST);
		points.setTransactionPoints(1000D);
		points.setTransactionDateTime(date.toDate());
		points.setCreated(date);
		points.setStatus(TxnStatus.ACTIVE);
		returnMessage = new ReturnMessage();
		assertTrue(pointsMangerService.processPoints(points, returnMessage));
		points = pointsMangerService.retrievePointsById(id);
		assertEquals(expected.toLocalDate(), points.getExpiryDate());
		
		points = new PointsTxnModel();
		id = customIdGeneratorService.generateId();
		points.setId(id);
		points.setMemberModel(member);
		points.setTransactionType(ADJUST);
		points.setTransactionPoints(-1000D);
		points.setTransactionDateTime(date.toDate());
		points.setCreated(date);
		points.setStatus(TxnStatus.ACTIVE);
		returnMessage = new ReturnMessage();
		assertTrue(pointsMangerService.processPoints(points, returnMessage));
		points = pointsMangerService.retrievePointsById(id);
		assertEquals(null, points.getExpiryDate());
	}
	
	@Test
	public void testSetDefaultValidPeriod() {
		DateTime date = DateTime.now();
		PointsTxnModel points = new PointsTxnModel();
		String id = customIdGeneratorService.generateId();
		points.setId(id);
		points.setMemberModel(member);
		points.setTransactionType(EARN);
		points.setTransactionPoints(1000D);
		points.setTransactionDateTime(date.toDate());
		points.setCreated(date);
		points.setStatus(TxnStatus.ACTIVE);
		ReturnMessage returnMessage = new ReturnMessage();
		assertTrue(pointsMangerService.processPoints(points, returnMessage));
		points = pointsMangerService.retrievePointsById(id);
		DateTime expected = date.plusMonths(25);
		if(expected.getDayOfMonth() > 1) {
			expected = expected.plusMonths(1).dayOfMonth().withMinimumValue();
		}
		assertEquals(expected.toLocalDate(), points.getExpiryDate());
		
		pointsMangerService.setDefaultValidPeriod(10);
		points = new PointsTxnModel();
		id = customIdGeneratorService.generateId();
		points.setId(id);
		points.setMemberModel(member);
		points.setTransactionType(EARN);
		points.setTransactionPoints(-1000D);
		points.setTransactionDateTime(date.toDate());
		points.setCreated(date);
		points.setStatus(TxnStatus.ACTIVE);
		returnMessage = new ReturnMessage();
		assertTrue(pointsMangerService.processPoints(points, returnMessage));
		points = pointsMangerService.retrievePointsById(id);
		expected = date.plusMonths(10);
		if(expected.getDayOfMonth() > 1) {
			expected = expected.plusMonths(1).dayOfMonth().withMinimumValue();
		}
		assertEquals(expected.toLocalDate(), points.getExpiryDate());
	}
}
