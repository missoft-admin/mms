package com.transretail.crm.report.template;

import java.util.Map;

/**
 *
 * @author Monte Cillo Co (mco@exist.com)
 */
public interface NoDataFoundSupport {

    boolean isEmpty(Map<String, String> filter);
}
