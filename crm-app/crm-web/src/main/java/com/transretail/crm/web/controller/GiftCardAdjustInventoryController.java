package com.transretail.crm.web.controller;

import com.google.common.base.Joiner;
import com.google.common.collect.Maps;
import com.transretail.crm.common.reporting.jasper.JRProcessor;
import com.transretail.crm.common.service.dto.request.PageSortDto;
import com.transretail.crm.common.service.dto.response.ResultList;
import com.transretail.crm.core.entity.enums.ReportType;
import com.transretail.crm.giftcard.dto.GiftCardPhysicalCountDto;
import com.transretail.crm.giftcard.dto.GiftCardPhysicalCountSummaryDto;
import com.transretail.crm.giftcard.entity.GiftCardInventory;
import com.transretail.crm.giftcard.entity.QProductProfile;
import com.transretail.crm.giftcard.entity.support.GiftCardInventoryStatus;
import com.transretail.crm.giftcard.service.GiftCardAdjustmentInventoryService;
import com.transretail.crm.giftcard.service.GiftCardAdjustmentInventoryService.History;
import com.transretail.crm.web.controller.util.ControllerResponse;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.joda.time.LocalDateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

/**
 *
 * @author Monte Cillo Co (mco@exist.com)
 */
@Controller
@RequestMapping("/giftcard/inventory/adjustment")
public class GiftCardAdjustInventoryController extends AbstractReportsController {

    @Autowired
    MessageSource messageSource;
    @Autowired
    private GiftCardAdjustmentInventoryService service;

    @RequestMapping(produces = MediaType.TEXT_HTML_VALUE)
    public String list(Model model) {
	model.addAttribute("giftCardPhysicalCountForm", new GiftCardPhysicalCountDto());
	model.addAttribute("reportTypes", ReportType.values());
	model.addAttribute("summaryDates", service.getSummaryDates(10l));

	return "giftcard/inventory/adjustment";
    }

    @RequestMapping(value = "/history/list", method = RequestMethod.POST)
    @ResponseBody
    public ResultList<History> listHistory(@RequestBody PageSortDto pageDto) {
        return service.getHistoryList(pageDto);
    }
    
    @RequestMapping(value = "/adjust", method = RequestMethod.POST, produces = "application/json; charset=utf-8")
    @ResponseBody
    public ControllerResponse savePhysicalCount(
	    @ModelAttribute(value = "giftCardPhysicalCountForm") @Valid GiftCardPhysicalCountDto adjustDto,
	    BindingResult result, HttpServletRequest request) {

	ControllerResponse response = new ControllerResponse();
	response.setSuccess(true);

	if (adjustDto.getBarcodes().isEmpty() && StringUtils.isNotBlank(adjustDto.getStartingSeries())) {
	    if (adjustDto.getStartingSeries().length() != 16 || adjustDto.getEndingSeries().length() != 16 || adjustDto.getQuantity() < 0) {
		result.reject(getMessage("gc.inventory.physical.count.invalid.series", null));
	    } /*else if (!service.validateCardByUserInventoryLocation(adjustDto.getStartingSeries(), adjustDto.getEndingSeries())) {
		result.reject(getMessage("gc.inventory.physical.count.invalid.gc.location", null));
	    }*/ else if (!service.validateSeries(adjustDto.getStartingSeries(), adjustDto.getEndingSeries())) {
		Map<String, GiftCardInventoryStatus> gcstatus = service.extractGiftCardStatus(adjustDto.getStartingSeries(), adjustDto.getEndingSeries());
		if (gcstatus.isEmpty()) {
		    result.reject(getMessage("gc.inventory.physical.count.invalid.series", null));
		} else {
		    for (Entry<String, GiftCardInventoryStatus> entry : gcstatus.entrySet()) {
			result.reject(messageSource.getMessage("gc.inventory.physical.count.invalid.status",
				new Object[]{entry.getKey(), entry.getValue().name()}, LocaleContextHolder.getLocale()));
		    }
		}

	    } else if (service.isCounted(adjustDto.getStartingSeries(), adjustDto.getEndingSeries())) {
		result.reject(getMessage("gc.inventory.physical.count.series.iscounted", null));
	    }
	}

	List<String> invalidBarcodes = new ArrayList<String>();
	for (String barcode : adjustDto.getBarcodes()) {
	    String series = barcode.substring(0, 16);
	    if (service.isCounted(series, series)) {
		invalidBarcodes.add(barcode);
	    }
	}

	if (!invalidBarcodes.isEmpty()) {
	    result.reject(messageSource.getMessage("gc.inventory.physical.count.barcode.series.iscounted",
		    new Object[]{Joiner.on(", ").join(invalidBarcodes)}, LocaleContextHolder.getLocale()));
	}

	if (result.hasErrors()) {
	    response.setSuccess(false);
	    response.setResult(result.getAllErrors());
	    return response;
	}
	QProductProfile qpp = QProductProfile.productProfile;

	for (String barcode : adjustDto.getBarcodes()) {
	    GiftCardPhysicalCountDto dto = new GiftCardPhysicalCountDto();
	    String series = barcode.substring(0, 16);
	    GiftCardInventory inventory = service.getGiftCardInventoryByBarcode(barcode);
	    dto.setProductProfileName(inventory.getProductName());
	    dto.setProductProfile(inventory.getProductCode());
	    dto.setStartingSeries(series);
	    dto.setEndingSeries(series);
	    dto.setQuantity(1l);
	    dto.setLocation(adjustDto.getLocation());
	    service.savePhysicalCount(dto);
	}

	// if barcodes is empty it means it use the series range
	if (adjustDto.getBarcodes().isEmpty() && adjustDto.getQuantity() != null) {
	    service.savePhysicalCount(adjustDto);
	}

	return response;
    }

    @RequestMapping(value = "/process", method = RequestMethod.POST, produces = "application/json; charset=utf-8")
    @ResponseBody
    public ControllerResponse process() {
	ControllerResponse response = new ControllerResponse();
	response.setSuccess(true);
	service.processPhysicalCount();
	return response;
    }

    @RequestMapping("/validate/{barcode}")
    @ResponseBody
    public ControllerResponse validateBarcode(@PathVariable String barcode) {
	ControllerResponse response = new ControllerResponse();
	response.setSuccess(true);

	if (barcode.length() != 20 || !service.isBarcodeExist(barcode)) {
	    response.setSuccess(false);
	    response.setResult(new String[]{getMessage("gc.inventory.physical.count.barcode.not.exist", null)});
	}

	return response;
    }

    @RequestMapping(value = "/list", method = RequestMethod.POST)
    @ResponseBody
    public ResultList<GiftCardPhysicalCountDto> listPhysicalCount(@RequestBody PageSortDto pageDto) {
	return service.listPhysicalCount(pageDto);
    }

    @RequestMapping(value = "/cardtype/{startingSeries}/{endingSeries}", produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public ControllerResponse getCardTypeBySeries(@PathVariable String startingSeries, @PathVariable String endingSeries) {
	ControllerResponse response = new ControllerResponse();
	response.setSuccess(true);
	List<GiftCardInventory> inventories = service.findInventoryBySeriesRange(startingSeries, endingSeries);
	int idx2 = CollectionUtils.isNotEmpty( inventories ) && inventories.size() > 1? 1 : 0;
	try {
	    if (inventories.isEmpty() || !inventories.get(0).getProductCode().equals(inventories.get( idx2 ).getProductCode())) {
		response.setSuccess(false);
		response.setResult(new String[]{"Series is not in same product profile"});
		return response;
	    }
	} catch (IndexOutOfBoundsException ex) {
	    response.setSuccess(false);
	    response.setResult(new String[]{"Series is not in same product profile"});
	    return response;
	}

	if (inventories.isEmpty() || !inventories.get(0).getProductCode().equals(inventories.get( idx2 ).getProductCode())) {
	    response.setSuccess(false);
	    response.setResult("Series is not in same product profile");
	    return response;
	}

	Map<String, String> result = Maps.newHashMapWithExpectedSize(2);
	result.put("productProfileCode", inventories.get(0).getProductCode());
	result.put("productProfileName", inventories.get(0).getProductName());
	Long inventoryCount = service.countInventory( startingSeries, endingSeries );

	if ( null != inventoryCount && inventoryCount.compareTo( 0L ) == 0 ) {
	    response.setSuccess(false);
	    response.setResult( new String[]{ messageSource.getMessage( "gc.adjust.inv.err.zerocount", null, LocaleContextHolder.getLocale() ) } );
	    return response;
	}

	result.put( "inventoryCount", Long.toString( inventoryCount ) );
	response.setResult(result);

	return response;
    }

    @RequestMapping(value = "/summary", method = RequestMethod.POST)
    @ResponseBody
    public ResultList<GiftCardPhysicalCountSummaryDto> listPhysicalCountSummary(@RequestBody PageSortDto pageDto) {
	return service.getSummary(pageDto);
    }

    @RequestMapping(value = "/print/{reportType}")
    public void printPhysicalCount(@PathVariable String reportType,
	    HttpServletResponse response) throws Exception {
	JRProcessor jrProcessor = service.createJRProcessor();
	if (ReportType.PDF.getCode().equals(reportType)) {
	    renderReport(response, jrProcessor);
	} else if (ReportType.EXCEL.getCode().equals(reportType)) {
	    renderExcelReport(response, jrProcessor, messageSource.getMessage("physical_count_label", null, LocaleContextHolder.getLocale()));
	}
    }

    @RequestMapping(value = "/print/{reportType}/{dateTime:.+}")
    public void printPhysicalCount(@PathVariable String reportType,
	    @PathVariable String dateTime,
	    HttpServletResponse response) throws Exception {
	LocalDateTime date = new LocalDateTime(dateTime);
        JRProcessor jrProcessor = service.createJRProcessor(date);
	if (ReportType.PDF.getCode().equals(reportType)) {
	    renderReport(response, jrProcessor);
	} else if (ReportType.EXCEL.getCode().equals(reportType)) {
	    renderExcelReport(response, jrProcessor, messageSource.getMessage("physical_count_label", null, LocaleContextHolder.getLocale()));
	}
    }

    private String getMessage(String inMsgCode, Object[] inArgMsgCodes) {
	if (inArgMsgCodes != null) {
	    for (int i = 0; i < inArgMsgCodes.length; i++) {
		inArgMsgCodes[i] = messageSource.getMessage(inArgMsgCodes[i].toString().toLowerCase(), null, LocaleContextHolder.getLocale());
	    }
	}
	return messageSource.getMessage(inMsgCode, inArgMsgCodes, "Info given is invalid.", LocaleContextHolder.getLocale());
    }
}
