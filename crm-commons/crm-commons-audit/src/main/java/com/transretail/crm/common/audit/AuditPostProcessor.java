package com.transretail.crm.common.audit;

import java.lang.reflect.Method;

import org.springframework.aop.framework.ProxyFactory;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.InstantiationAwareBeanPostProcessorAdapter;
import org.springframework.core.Ordered;
import org.springframework.core.PriorityOrdered;

import com.transretail.crm.common.audit.annotation.Audited;
import com.transretail.crm.common.audit.handler.SaveHandler;
import com.transretail.crm.common.audit.helper.ProxyHelper;

/**
 * @author Allan G. Ramirez (agramirez@exist.com)
 */
public class AuditPostProcessor extends InstantiationAwareBeanPostProcessorAdapter implements PriorityOrdered {
    private ServiceMethodLogger serviceMethodLogger = new ServiceMethodLogger();
    private ProxyHelper proxyHelper = new ProxyHelper();

    /**
     * Get the priority order of this post processor. Will always return Integer.MAX_VALUE, meaning last in order.
     *
     * @return Integer.MAX_VALU     E, this processor will be last in order
     */
    @Override
    public int getOrder() {
        return Ordered.LOWEST_PRECEDENCE;
    }

    public void setSaveHandler(SaveHandler saveHandler) {
        serviceMethodLogger.setSaveHandler(saveHandler);
    }

    @Override
    public Object postProcessAfterInitialization(Object bean, String beanName) throws BeansException {
        if (isAudited(proxyHelper.unwrapIfProxy(bean).getClass())) {
            ProxyFactory factory = new ProxyFactory(bean);
            factory.addAdvice(serviceMethodLogger);
            return factory.getProxy();
        } else {
            return bean;
        }
    }

    private boolean isAudited(Class<?> clazz) {
        for (Method method : clazz.getDeclaredMethods()) {
            if (method.isAnnotationPresent(Audited.class)) {
                return true;
            }
        }
        return false;
    }
}

