package com.transretail.crm.core.service.impl;

import static org.junit.Assert.*;

import java.util.List;

import org.joda.time.LocalDate;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Configurable;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import com.transretail.crm.common.service.dto.response.ResultList;
import com.transretail.crm.core.dto.PromoBannerDto;
import com.transretail.crm.core.dto.PromoBannerSearchDto;
import com.transretail.crm.core.entity.PromoBanner;
import com.transretail.crm.core.entity.enums.Status;
import com.transretail.crm.core.entity.lookup.LookupDetail;
import com.transretail.crm.core.entity.lookup.LookupHeader;
import com.transretail.crm.core.repo.LookupDetailRepo;
import com.transretail.crm.core.repo.LookupHeaderRepo;
import com.transretail.crm.core.repo.PromoBannerRepo;
import com.transretail.crm.core.service.PromoBannerService;

@Configurable
@Transactional
@ContextConfiguration(locations = "classpath*:/META-INF/spring/applicationContext*.xml")
@RunWith(SpringJUnit4ClassRunner.class)
public class PromoBannerServiceTest {
	@Autowired
	private PromoBannerService promoBannerService;
	@Autowired
	private PromoBannerRepo promoBannerRepo;
	@Autowired
	private LookupDetailRepo lookupDetailRepo;
	@Autowired
	private LookupHeaderRepo lookupHeaderRepo;
	
	LocalDate today, nextWeek, lastWeek, nextYear;
	
	@Before
	public void setUp() {
		today = LocalDate.now();
		nextWeek = today.plusDays(7);
		lastWeek = today.minusDays(7);
		nextYear = today.plusYears(1);
		
		LookupHeader header = new LookupHeader("GEN003", "STATUS");
		header = lookupHeaderRepo.save(header);
		
		LookupDetail status = new LookupDetail("STAT001");
		status.setDescription("ACTIVE");
		status.setStatus(Status.ACTIVE);
		status.setHeader(header);
		status = lookupDetailRepo.save(status);
		
		PromoBanner banner = new PromoBanner();
		banner.setId("banner1");
		banner.setName("current");
		banner.setStartDate(today);
		banner.setEndDate(nextYear);
		banner.setStatus(status);
		promoBannerRepo.save(banner);
		
		banner = new PromoBanner();
		banner.setId("banner2");
		banner.setName("ended");
		banner.setStartDate(lastWeek);
		banner.setEndDate(today.minusDays(1));
		banner.setStatus(status);
		promoBannerRepo.save(banner);
		
		banner = new PromoBanner();
		banner.setId("banner3");
		banner.setName("pending");
		banner.setStartDate(nextWeek);
		banner.setEndDate(nextYear);
		banner.setStatus(status);
		promoBannerRepo.save(banner);
	}
	
	@Test
	public void testSaveAndGet() {
		PromoBanner banner = new PromoBanner();
		banner.setId("test");
		PromoBannerDto dto = promoBannerService.save(banner);
		assertEquals(dto.getId(), promoBannerService.findById("test").getId());
		
		PromoBanner banner1 = new PromoBanner();
		banner1.setId("testDto");
		dto = promoBannerService.save(new PromoBannerDto(banner1));
		assertEquals(dto.getId(), promoBannerService.findById("testDto").getId());
	}
	
	@Test
	public void testFindCurrentPromoBanners() {
		List<PromoBannerDto> current = promoBannerService.findCurrentPromoBanners();
		
		assertEquals(1, current.size());
		assertEquals("banner1", current.get(0).getId());
	}
	
	@Test
	public void testSearchPromoBanner() {
		PromoBannerSearchDto searchDto = new PromoBannerSearchDto();
		searchDto.setName("current");
		
		ResultList<PromoBannerDto> results = promoBannerService.searchPromoBanner(searchDto);
		assertEquals(1, results.getTotalElements());
		for(PromoBannerDto dto : results.getResults()) {
			assertEquals("current", dto.getName());
		}
		
		searchDto = new PromoBannerSearchDto();
		searchDto.setStartDate(today);
		results = promoBannerService.searchPromoBanner(searchDto);
		assertEquals(1, results.getTotalElements());
		for(PromoBannerDto dto : results.getResults()) {
			assertEquals("current", dto.getName());
		}
		
		searchDto = new PromoBannerSearchDto();
		searchDto.setEndDate(nextYear);
		results = promoBannerService.searchPromoBanner(searchDto);
		assertEquals(2, results.getTotalElements());
		
		searchDto = new PromoBannerSearchDto();
		searchDto.setStartDate(today.minusYears(1));
		searchDto.setEndDate(nextYear);
		results = promoBannerService.searchPromoBanner(searchDto);
		assertEquals(3, results.getTotalElements());
	}
}
