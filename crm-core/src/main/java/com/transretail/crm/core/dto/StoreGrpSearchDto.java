package com.transretail.crm.core.dto;


import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.mysema.query.types.expr.BooleanExpression;
import com.transretail.crm.common.service.dto.request.AbstractSearchFormDto;
import com.transretail.crm.core.entity.lookup.QStoreGroup;


public class StoreGrpSearchDto extends AbstractSearchFormDto {

	private String name;



	@Override
	@JsonIgnore
	public BooleanExpression createSearchExpression() {
		List<BooleanExpression> expr = new ArrayList<BooleanExpression>();
		QStoreGroup qModel = QStoreGroup.storeGroup;

		if ( StringUtils.isNotBlank( name ) ) {
			expr.add( qModel.name.equalsIgnoreCase( name ) );
		}

		return BooleanExpression.allOf( expr.toArray( new BooleanExpression[expr.size()] ) );
	}



	public String getName() {
		return name;
	}
	public void setName( String name ) {
		this.name = name;
	}

}
