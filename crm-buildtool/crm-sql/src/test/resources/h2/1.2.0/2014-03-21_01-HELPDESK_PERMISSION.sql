insert into CRM_USER_ROLE ( CODE, DESCRIPTION, ENABLED, VERSION ) values ( 'ROLE_HELPDESK', 'Helpdesk', 'Y', 1 );
insert into CRM_USER_PERMISSION ( CODE, DESCRIPTION, STATUS, ROLE_CATEGORY ) 
  VALUES ( 'CUSTOMER_HELPDESK', 'Viewing of member related information', 'ACTIVE', 'ROLE_HELPDESK' );