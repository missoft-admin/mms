package com.transretail.crm.web.controller;

import com.google.common.base.Predicate;
import com.google.common.collect.Collections2;
import com.google.common.collect.Lists;
import com.transretail.crm.common.enums.MessageType;
import com.transretail.crm.core.dto.MemberDto;
import com.transretail.crm.core.dto.MemberResultList;
import com.transretail.crm.core.dto.MemberSearchDto;
import com.transretail.crm.core.dto.UpdatePinDto;
import com.transretail.crm.core.entity.MemberModel;
import com.transretail.crm.core.entity.PointsTxnModel;
import com.transretail.crm.core.entity.Province;
import com.transretail.crm.core.entity.UserModel;
import com.transretail.crm.core.entity.embeddable.*;
import com.transretail.crm.core.entity.enums.*;
import com.transretail.crm.core.entity.lookup.LookupDetail;
import com.transretail.crm.core.entity.lookup.LookupHeader;
import com.transretail.crm.core.entity.lookup.Store;
import com.transretail.crm.core.security.model.impl.CustomSecurityUserDetailsImpl;
import com.transretail.crm.core.security.util.UserUtil;
import com.transretail.crm.core.service.*;
import com.transretail.crm.giftcard.dto.PrepaidCardResultList;
import com.transretail.crm.giftcard.dto.PrepaidCardSearchDto;
import com.transretail.crm.giftcard.dto.StockRequestDto;
import com.transretail.crm.giftcard.entity.support.StockRequestStatus;
import com.transretail.crm.giftcard.service.MemberPrepaidService;
import com.transretail.crm.loyalty.entity.LoyaltyCardInventory;
import com.transretail.crm.loyalty.service.LoyaltyCardService;
import com.transretail.crm.web.controller.util.ControllerResponse;
import com.transretail.crm.web.controller.util.ControllerResponseExceptionHandler;
import com.transretail.crm.web.controller.util.MemberValidator;
import com.transretail.crm.web.controller.util.UpdatePinValidator;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.text.WordUtils;
import org.joda.time.format.DateTimeFormat;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ValidationUtils;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import org.springframework.web.util.UriUtils;
import org.springframework.web.util.WebUtils;

import javax.servlet.http.HttpServletRequest;

import java.io.UnsupportedEncodingException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;

@RequestMapping("/customermodels")
@Controller
public class MemberController extends ControllerResponseExceptionHandler {
    @Autowired
    MemberService memberService;
    
    @Autowired
    StoreService storeService;
    
    @Autowired
    MemberValidator memberValidator;
    
    @Autowired
    UpdatePinValidator updatePinValidator;
    
    @Autowired
    private MessageSource messageSource;

    @Autowired
    LookupService lookupService;

    @Autowired
    private CodePropertiesService codePropertiesService;
    
    @Autowired
    LocationService locationService;
    
    @Autowired
    LoyaltyCardService loyaltyCardService;
    
    @Autowired
    PointsTxnManagerService pointsTxnManagerService;
    
    @Autowired
    UserService userService;
    
    @Autowired
    private MemberPrepaidService memberPrepaidService;
    
    private static final Logger LOGGER = LoggerFactory.getLogger(MemberController.class);
    
    private static final String PATH_PRINT_FORM = "customermodels/printList";
    private static final String PATH_VIEW_PREPAID = "customermodels/viewPrepaid";
    private static final String UI_ATTR_MODEL_SEARCHMODEL  = "searchModel";
    private static final String UI_ATTR_MODEL_SEARCH       = "searchModelAttribute";
    private static final String UI_ATTR_MODEL              = "customerModel";
    private static final String UI_ATTR_UPDATE_PIN		   = "updatePinForm";
    private static final String UI_ATTR_CUST_GROUPS		   = "customerGroups";
    
    private static final String UI_ATTR_FIELD_SEARCHBY     = "memberCriteria";
    private static final String UI_ATTR_FIELD_STORES       = "preferredStore";
    private static final String UI_ATTR_IS_PRIMARY		   = "isPrimary";
    
    private static final String[] POSITIONS = {"OWNER", "PURCHASING", "CHEF", "STAFF"};

    
    String encodeUrlPathSegment(String pathSegment,
            HttpServletRequest httpServletRequest) {
        String enc = httpServletRequest.getCharacterEncoding();
        if (enc == null) {
            enc = WebUtils.DEFAULT_CHARACTER_ENCODING;
        }
        try {
            pathSegment = UriUtils.encodePathSegment(pathSegment, enc);
        } catch (UnsupportedEncodingException uee) {
            uee.printStackTrace();
        }
        return pathSegment;
    }

	@InitBinder
	void initBinder(WebDataBinder binder) {
		DateFormat theFormat = new SimpleDateFormat( "dd-MM-yyyy" );
		theFormat.setLenient( false );
		binder.registerCustomEditor( Date.class, new CustomDateEditor( theFormat, true ) );
	}
    
    private String formatWord(String word) {
    	word = word.toLowerCase();
    	return Character.toLowerCase(word.charAt(0)) + 
    			WordUtils.capitalize(word).replace(" ", "").substring(1);
    }

    void populateEditForm(Model uiModel, MemberDto customerModel) {
        uiModel.addAttribute(UI_ATTR_MODEL, customerModel);
        uiModel.addAttribute(UI_ATTR_IS_PRIMARY, false);
        
        uiModel.addAttribute("correspondenceAddress", lookupService.getActiveDetailsByHeaderCode(codePropertiesService.getHeaderCorrespondenceAddress()));
        uiModel.addAttribute("informedAbout", lookupService.getActiveDetailsByHeaderCode(codePropertiesService.getHeaderInformedAbout()));
        uiModel.addAttribute("interestedProducts", lookupService.getActiveDetailsByHeaderCode(codePropertiesService.getHeaderInterestedProds()));
        uiModel.addAttribute("configurableFieldHeaders", lookupService.getMemberConfigurableFields());

        CustomSecurityUserDetailsImpl userDetails = UserUtil.getCurrentUser();
        
        if(StringUtils.defaultString(userDetails.getInventoryLocation()).equals(codePropertiesService.getDetailInvLocationHeadOffice())) {
        	uiModel.addAttribute(UI_ATTR_FIELD_STORES, storeService.getAllStores());
        } else {
        	List<Store> stores = new ArrayList<Store>();
    		stores.add( storeService.getStoreByCode( userDetails.getStoreCode() ) );
            uiModel.addAttribute(UI_ATTR_FIELD_STORES, stores );
        }
        
        Map<String, List<LookupDetail>> lookups = new HashMap<String, List<LookupDetail>>();
        for(LookupHeader header : lookupService.getMemberHeaders()) {
        	String fieldName = formatWord(header.getDescription());
        	LOGGER.debug(fieldName);
        	lookups.put(fieldName, lookupService.getActiveDetails(header));
        }

        lookups.get("memberType").remove(codePropertiesService.getDetailMemberTypeCompany());
	final List<LookupDetail> departments = lookups.get("department"); // refactor if all options is must be sorted in natural order
	
	Collections.sort(departments == null? Collections.EMPTY_LIST : departments, 
		new Comparator<LookupDetail>(){

	    @Override
	    public int compare(LookupDetail o1, LookupDetail o2) {
		return StringUtils.defaultIfBlank(o1.getDescription(),"")
			.compareToIgnoreCase(StringUtils.defaultIfBlank(o2.getDescription(), ""));
	    }	    
	});
	lookups.put("department", departments);
        List<LookupDetail> customerGroups = lookupService.getActiveDetails(lookupService.getHeader((codePropertiesService.getHeaderCustomerGroup())));
        List<LookupDetail> customerSegmentation = lookupService.getActiveDetails(lookupService.getHeader((codePropertiesService.getHeaderCustomerSegmentation())));
        lookups.put(UI_ATTR_CUST_GROUPS, customerGroups);
        Map<String, Collection<LookupDetail>> segmentMap = new HashMap<String, Collection<LookupDetail>>();
        for(LookupDetail detail : customerGroups) {
        	final String detailCode = detail.getCode();
        	Collection<LookupDetail> segmentation = Collections2.filter(customerSegmentation, new Predicate<LookupDetail>() {
				@Override
				public boolean apply(LookupDetail input) {
					
					char code = detailCode.charAt(detailCode.length() - 1);
					return (input.getCode().charAt(4) == code);
				}
        	});
        	segmentMap.put(detailCode, segmentation);
        }
        uiModel.addAllAttributes(lookups);
        uiModel.addAttribute("customerSegmentation", segmentMap);
    	List<Province> provinces = locationService.getAllProvinces();
		uiModel.addAttribute("provinces", provinces);
		uiModel.addAttribute("positions", POSITIONS);
		uiModel.addAttribute("radii", lookupService.getActiveDetailsByHeaderCode(codePropertiesService.getHeaderRadius())); //radii = plural of radius
        addDateTimeFormatPatterns(uiModel);
    }
    
    void populateUpdatePinForm(Model uiModel, UpdatePinDto updatePinDto) {
    	uiModel.addAttribute(UI_ATTR_UPDATE_PIN, updatePinDto);
    }

    void addDateTimeFormatPatterns(Model uiModel) {
        uiModel.addAttribute(
                "customerModel_birthdate_date_format",
                DateTimeFormat.patternForStyle("M-",
                        LocaleContextHolder.getLocale()));
    }
    
    private void setRegisteredStore(MemberDto member) {
    	CustomSecurityUserDetailsImpl user = UserUtil.getCurrentUser();
		if(StringUtils.isNotBlank(user.getStoreCode())) {
			member.setRegisteredStore(storeService.getStoreByCode(user.getStoreCode()).getCode());
		}
    }

    @RequestMapping(value = "/populate/{id}", method = RequestMethod.POST, produces = "application/json; charset=utf-8")
    @ResponseBody
    public ControllerResponse populateMember(
    		@PathVariable("id") String id,
    		@ModelAttribute(value=UI_ATTR_MODEL) MemberDto member,
            BindingResult result,
            HttpServletRequest request, Model uiModel) {

    	ControllerResponse theResponse = new ControllerResponse();    	
    	theResponse.setSuccess(true);

		MemberDto theMember = null;
    	if (id.matches("[0-9]*")) {
    		theMember = new MemberDto(memberService.findCustomerModel(Long.valueOf(id)));
    		if(StringUtils.isNotBlank(theMember.getPassword())) {
    			String passwordCheck = UUID.randomUUID().toString().substring(23);
    			theMember.setPasswordCheck(passwordCheck);
    			theMember.setPassword(passwordCheck);
    		}
    		if(theMember.getCustomerProfile() == null) {
    			theMember.setCustomerProfile(new CustomerProfile());
    		}
    		if(theMember.getChannel() == null) {
    			theMember.setChannel(new Channel());
    		}
    		if(theMember.getAddress() == null) {
    			theMember.setAddress(new Address());
    		} else {
    			Address address = theMember.getAddress(); 
    			populateProvince(address);
    		}
    		if(theMember.getProfessionalProfile() == null) {
    			ProfessionalProfile profProfile = new ProfessionalProfile();
    			profProfile.setBusinessAddress(new Address());
    			theMember.setProfessionalProfile(profProfile);
    		} else {
    			Address profAddress = theMember.getProfessionalProfile().getBusinessAddress(); 
    			populateProvince(profAddress);
    		}
    		if(theMember.getMarketingDetails() == null) {
    			theMember.setMarketingDetails(new MarketingDetails());
    		}
    		if(theMember.getLoyaltyCardNo() != null) {
    			LoyaltyCardInventory inv = loyaltyCardService.findLoyaltyCard(theMember.getLoyaltyCardNo());
    			if(inv != null) {
                    if (inv.getActivationDate() != null) {
                        theMember.setLoyaltyCardDateIssued(inv.getActivationDate().toString("MM-dd-yyyy"));
                    }
                    if (inv.getExpiryDate() != null) {
                        theMember.setLoyaltyCardDateExpired(inv.getExpiryDate().toString("MM-dd-yyyy"));
                    }
                }
    		}
    		if(theMember.getParent() != null) {
    			MemberModel parent = memberService.findByAccountId(theMember.getParent());
    			if(parent != null) {
    				theMember.setParentName(parent.getFormattedMemberName());
    			}
    		}
    		
    		if(StringUtils.isNotBlank(theMember.getCreatedBy())) {
    			UserModel user = userService.findUserModel(theMember.getCreatedBy());
    			if(user != null) {
    				StringBuilder builder = new StringBuilder(user.getUsername());
    				builder.append(" - ");
    				if(StringUtils.isNotBlank(user.getFirstName())) {
    					builder.append(user.getFirstName()).append(" ");
    				}
    				if(StringUtils.isNotBlank(user.getLastName())) {
    					builder.append(user.getLastName());
    				}
    				theMember.setCreatedBy(builder.toString());
    			}
    		}
    	}
    	else {
    		theMember = new MemberDto(memberService.generateInitModel());
    		setRegisteredStore(theMember);
    	}
    	
    	uiModel.addAttribute(UI_ATTR_IS_PRIMARY, StringUtils.isBlank(theMember.getParent()));
    	
    	theResponse.setResult(theMember);
    	LOGGER.debug("Member Type: " + theMember.getMemberType());
    	
        return theResponse;
    }
    
    private void populateProvince(Address address) {
    	if(address != null) {
			if(StringUtils.isNotBlank(address.getCity())) {
				List<Province> provinces = locationService.getProvincesByCityName(address.getCity());
				if(CollectionUtils.isNotEmpty(provinces)) {
					address.setProvince(provinces.get(0).getName());
				}
			}
		}
    }
    
    @RequestMapping(value = "/professional/new", method = RequestMethod.POST, produces = "application/json; charset=utf-8")
    @ResponseBody
    public ControllerResponse createProfessional(
    		@ModelAttribute(value=UI_ATTR_MODEL) MemberDto member,
            BindingResult result,
            HttpServletRequest request) {
    	ControllerResponse response = new ControllerResponse();    	
    	response.setSuccess(true);

		MemberDto professional = new MemberDto(memberService.generateInitModel());
		professional.setMemberType(lookupService.getDetail(codePropertiesService.getDetailMemberTypeCompany()));
    	setRegisteredStore(professional);
		
    	response.setResult(professional);
    	
        return response;
    }
    
    @RequestMapping(value = "/supplement/new/{id}", method = RequestMethod.POST, produces = "application/json; charset=utf-8")
    @ResponseBody
    public ControllerResponse createSupplementaryMember(
    		@PathVariable("id") String id,
    		@ModelAttribute(value=UI_ATTR_MODEL) MemberDto member,
            BindingResult result,
            HttpServletRequest request) {
    	LOGGER.debug("Creating supplementary member for member " + id);
    	ControllerResponse theResponse = new ControllerResponse();    	
    	theResponse.setSuccess(true);

		MemberDto theMember = new MemberDto(memberService.generateInitModel());
    	if (id.matches("[0-9]*")) {
    		MemberDto parent = new MemberDto(memberService.findCustomerModel(Long.valueOf(id)));
    		LOGGER.debug("Parent: " + parent.getParent());
    		
    		if(parent.getParent() != null && StringUtils.isNotBlank(parent.getParent())) {
    			theResponse.setSuccess(false);
    			theResponse.setMessage(MessageType.ERROR);
    			theResponse.setResult("Supplements cannot have other supplements");
    			LOGGER.debug("Supplements cannot have other supplements");
    			return theResponse;
    		}
    		else {
    			populateSupplementary(parent, theMember);
    		}
    	}
    	else {
    		theResponse.setSuccess(false);
    	}
    	
    	theResponse.setResult(theMember);
    	
        return theResponse;
    }
    
    private void populateSupplementary(MemberDto parent, MemberDto theMember) {
    	if(parent.getMemberType().getCode().equals(codePropertiesService.getDetailMemberTypeProfessional())) {
        	theMember.setProfessionalProfile(parent.getProfessionalProfile());
    		
    		if(theMember.getProfessionalProfile() != null) {
        		ProfessionalProfile profProfile = theMember.getProfessionalProfile();
        		profProfile.setPosition("");
        		profProfile.setTransactionCode1(null);
        		profProfile.setTransactionCode2(null);
        		profProfile.setTransactionCode3(null);
        		profProfile.setTransactionCode4(null);
        		profProfile.setTransactionCodeLong1(null);
        		profProfile.setTransactionCodeLong2(null);
        		profProfile.setTransactionCodeShort(null);
        	}
    		else {
    			ProfessionalProfile profProfile = new ProfessionalProfile();
    			profProfile.setBusinessAddress(new Address());
    			theMember.setProfessionalProfile(profProfile);
    		}
        	
    		//TODO position, best time to call
//        	theMember.setUsername("");
//        	theMember.setPassword("");
//    		theMember.setFirstName("");
//    		theMember.setMiddleName("");
//    		theMember.setLastName("");
//    		theMember.setKtpId("");
//    		theMember.setContact(new ArrayList<String>());
//    		theMember.setEmail("");
//    		theMember.setBestTimeToCall(null);	
//    		theMember.setIdNumber("");
//    		theMember.setPin("");
//    		theMember.setAccountId("");
//    		theMember.setRegistrationDate(null);
    		
			theMember.setCustomerProfile(new CustomerProfile());
			theMember.setChannel(new Channel());
			theMember.setAddress(new Address());
			theMember.setMarketingDetails(new MarketingDetails());
    	}
		
    	theMember.setParentModel(parent);
		theMember.setParent(parent.getAccountId());
		theMember.setParentName(parent.getFormattedMemberName());
		theMember.setMemberType(parent.getMemberType());
		setRegisteredStore(theMember);
		if(theMember.getMemberType().getCode().equals(codePropertiesService.getDetailMemberTypeCompany())) {
			theMember.setCompanyName(parent.getCompanyName());
		}
		LOGGER.debug("Parent account: " + theMember.getParent());
		LOGGER.debug("Member Type: " + theMember.getMemberType().getDescription());
		
    }

    @RequestMapping(value = "/new", method = RequestMethod.POST, produces = "application/json; charset=utf-8")
    @ResponseBody
    public ControllerResponse createMember(
    		@ModelAttribute(value=UI_ATTR_MODEL) 
    		MemberDto member,
            BindingResult result,
            HttpServletRequest request) {
    	
        ControllerResponse theResponse = new ControllerResponse();
    	theResponse.setSuccess(true);
    	
    	member.setEnabled(true);
    	member.setMemberCreatedFromType(MemberCreatedFromType.FROM_CRM);
    	if(member.getMemberType() == null) {
    		member.setMemberType(lookupService.getDetail(codePropertiesService.getDetailMemberTypeIndividual()));
    	}
    	if(StringUtils.isBlank( member.getRegisteredStore() ) )
    		member.setRegisteredStore(null);
    	
    	LOGGER.debug("Birthdate: " + member.getCustomerProfile().getBirthdate());
    	LOGGER.debug("NPWP: " + member.getNpwpId());
    	LOGGER.debug("Business Address: " + member.getProfessionalProfile().getBusinessAddress());
//    	LOGGER.error("ACCOUNT ID SIZE: " + (member.getAccountId() != null ? member.getAccountId().length() : 0));
    	
    	ValidationUtils.invokeValidator( memberValidator, member, result );
    	
        if(result.hasErrors()) {
        	theResponse.setSuccess(false);
        	theResponse.setResult(result.getAllErrors());
        	return theResponse;
        }

        if (theResponse.isSuccess()) {
        	LOGGER.debug("Parent: " + member.getParent());
            memberService.saveCustomerModel(member);
        }

        return theResponse;
    }

    @RequestMapping(value = "/update/{id}", method = RequestMethod.POST, produces = "application/json; charset=utf-8")
    @ResponseBody
    public ControllerResponse updateMember(
    		@PathVariable("id") Long id,
    		@ModelAttribute(value=UI_ATTR_MODEL) MemberDto member,
            BindingResult result,
            HttpServletRequest request) {

        ControllerResponse theResponse = new ControllerResponse();
    	theResponse.setSuccess(true);
    	member.setId(id);
    	
    	MemberDto theEntity = memberService.findCustomer(id);
        BeanUtils.copyProperties(member, theEntity, new String[]{"password", "parent", "parentModel", "terminationDate"} );
        theEntity.setMemberCreatedFromType(MemberCreatedFromType.FROM_CRM);
    	theEntity.setId(id);
    	
    	LOGGER.debug("Registered Store: " + theEntity.getRegisteredStore());
    	LOGGER.debug("Professional Profile: " + theEntity.getKtpId());
    	LOGGER.debug("Professional Profile: " + theEntity.getProfessionalProfile().getZone());
    	LOGGER.debug("Password: " + (member.getPassword().equals(member.getPasswordCheck())));
//    	LOGGER.error("banks: " + theEntity.getCustomerProfile().getBanks());
//    	LOGGER.error("banks: " + theEntity.getCustomerProfile().getCreditCardOwnership());
    	
//    	LOGGER.debug("email: " + member.getChannel().getAcceptEmail());
//    	LOGGER.debug("sms: " + member.getChannel().getAcceptSMS());
//    	LOGGER.debug("mail " + member.getChannel().getAcceptMail());
    	
    	theEntity.setPassword(member.getPassword());
    	theEntity.setPasswordCheck(member.getPasswordCheck());

    	ValidationUtils.invokeValidator(memberValidator, theEntity, result);
    	
    	
        if(result.hasErrors()) {
        	theResponse.setSuccess(false);
        	theResponse.setResult(result.getAllErrors());
        	return theResponse;
        }

        if (theResponse.isSuccess()) {
            theEntity = memberService.updateCustomerModel(theEntity);
            
            if(theEntity.getMemberType().getCode().equals(codePropertiesService.getDetailMemberTypeEmployee())
            		&& theEntity.getAccountStatus() == MemberStatus.TERMINATED) {
//            	LOGGER.error("EXPIRING ALL POINTS for " + theEntity.getAccountId());
            	PointsTxnModel expire = pointsTxnManagerService.expireAllPoints(theEntity.getAccountId());
//            	LOGGER.error("" + (expire == null));
            }
        }

        return theResponse;
    }

	@RequestMapping(value = "/search", produces = "text/html")
	public String list(Model uiModel, @ModelAttribute(value = UI_ATTR_MODEL_SEARCHMODEL) MemberModel inModel) {
		populateEditForm(uiModel, new MemberDto());
		populatePointsSearchBox(uiModel, inModel);
		populateUpdatePinForm(uiModel, new UpdatePinDto());
		uiModel.addAttribute("customermodels", memberService.findByCriteria(inModel, codePropertiesService.getDetailMemberTypeEmployee()));
		return "customermodels/list";
	}
	
	@RequestMapping(value="/supplements/{id}", method=RequestMethod.POST)
	@ResponseBody
	public MemberResultList listSupplements(@PathVariable("id") String id) {
		if(id == null) {
			return new MemberResultList(new ArrayList<MemberDto>(), 0, false, false);
		}
		List<MemberDto> list = memberService.findSupplementaryMembersByParentId(id.toString());
		if(list == null || list.isEmpty()) {
			return new MemberResultList(new ArrayList<MemberDto>(), 0, false, false);
		}
		return new MemberResultList(list, list.size(), false, false);
	}

    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE, produces = "text/html")
    public String delete(@PathVariable("id") Long id,
            @RequestParam(value = "page", required = false) Integer page,
            @RequestParam(value = "size", required = false) Integer size,
            Model uiModel) {
        MemberModel customerModel = memberService
                .findCustomerModel(id);
        memberService.deleteCustomerModel(customerModel);
        uiModel.asMap().clear();
        uiModel.addAttribute("page", (page == null) ? "1" : page.toString());
        uiModel.addAttribute("size", (size == null) ? "10" : size.toString());
        return "redirect:/customermodels";
    }

    @RequestMapping(value = "/{id}", params = "form", produces = "text/html")
    public String updateForm(@PathVariable("id") Long id, Model uiModel) {
        populateEditForm(uiModel, new MemberDto(memberService.findCustomerModel(id)));
        return "customermodels/update";
    }

    @RequestMapping(method = RequestMethod.PUT, produces = "text/html")
    public String update(MemberDto customerModel,
            BindingResult bindingResult, Model uiModel,
            HttpServletRequest httpServletRequest) {
        if (bindingResult.hasErrors()) {
            populateEditForm(uiModel, customerModel);
            return "customermodels/update";
        }
        uiModel.asMap().clear();
        memberService.updateCustomerModel(customerModel);
        return "redirect:/customermodels/"
                + encodeUrlPathSegment(customerModel.getId().toString(),
                        httpServletRequest);
    }

    @RequestMapping(params={"page", "size"}, produces = "text/html")
    public String list(
            @RequestParam(value = "page", required = false) Integer page,
            @RequestParam(value = "size", required = false) Integer size,
            Model uiModel) {
    	populateEditForm(uiModel, new MemberDto());
    	populatePointsSearchBox(uiModel, new MemberModel());
    	populateUpdatePinForm(uiModel, new UpdatePinDto());

        return "customermodels/list";
    }
    
    @RequestMapping(value = "/list", method = RequestMethod.POST)
    public @ResponseBody MemberResultList list(@RequestBody MemberSearchDto searchDto) {
    	return memberService.listMemberDto(searchDto);
    }

    @RequestMapping(method = RequestMethod.POST, produces = "text/html")
    public String create(MemberDto customerModel,
            BindingResult bindingResult, Model uiModel,
            HttpServletRequest httpServletRequest) {
        if (bindingResult.hasErrors()) {
            populateEditForm(uiModel, customerModel);
            return "customermodels/create";
        }
        uiModel.asMap().clear();
        memberService.saveCustomerModel(customerModel);
        return "redirect:/customermodels/"
                + encodeUrlPathSegment(customerModel.getId().toString(),
                        httpServletRequest);
    }

    @RequestMapping(params = "form", produces = "text/html")
    public String createForm(Model uiModel) {
        populateEditForm(uiModel, new MemberDto());
        return "customermodels/create";
    }

    @RequestMapping(value = "/{id}", produces = "text/html")
    public String show(@PathVariable("id") Long id, Model uiModel) {
        addDateTimeFormatPatterns(uiModel);
        uiModel.addAttribute(UI_ATTR_MODEL,
                memberService.findCustomerModel(id));
        uiModel.addAttribute("itemId", id);
        return "customermodels/show";
    }
	
	private void populatePointsSearchBox(Model uiModel, MemberModel inModel) {
		uiModel.addAttribute(UI_ATTR_MODEL_SEARCHMODEL, inModel);
		uiModel.addAttribute(UI_ATTR_MODEL_SEARCH, UI_ATTR_MODEL_SEARCHMODEL);
		uiModel.addAttribute(UI_ATTR_FIELD_SEARCHBY, SearchCriteria.values());
		uiModel.addAttribute("status", MemberStatusSearchLookup.values());
		List<LookupDetail> memberTypes = new ArrayList<LookupDetail>();
		memberTypes.add(lookupService.getDetail(codePropertiesService.getDetailMemberTypeIndividual()));
		memberTypes.add(lookupService.getDetail(codePropertiesService.getDetailMemberTypeProfessional()));
		uiModel.addAttribute("memberType", memberTypes);
		
	
		
	}
	
    @RequestMapping(value = "/pin/update/{id}", method = RequestMethod.POST, produces = "application/json; charset=utf-8")
    @ResponseBody
    public ControllerResponse ajaxUpdatePin(@PathVariable("id") Long id, @ModelAttribute(UI_ATTR_UPDATE_PIN) UpdatePinDto dto,
        BindingResult result, RedirectAttributes redirectAttributes, HttpServletRequest request) {
    	
    	ControllerResponse theResponse = new ControllerResponse();
    	theResponse.setSuccess(true);
    	ValidationUtils.invokeValidator( updatePinValidator, dto, result );
    	
    	if(result.hasErrors()) {
        	theResponse.setSuccess(false);
        	theResponse.setResult(result.getAllErrors());
        	return theResponse;
        }
    	
    	if (theResponse.isSuccess()) {
            memberService.updatePin(id, dto.getPin());
        }
    	
    	return theResponse;
    }

    @RequestMapping( value="/member/store/", produces="application/json; charset=utf-8" )
	public @ResponseBody ControllerResponse getStores() {

		ControllerResponse theResp = new ControllerResponse();

    	List<Store> stores = new ArrayList<Store>();
		CustomSecurityUserDetailsImpl userDetails = UserUtil.getCurrentUser();
        if ( StringUtils.isNotBlank( userDetails.getStoreCode() ) ) {
        	if ( userDetails.getStoreCode().equalsIgnoreCase( codePropertiesService.getDetailStoreHeadOffice() ) ) {
        		CollectionUtils.addAll( stores, storeService.getAllStores().iterator() );
        	}
        	else {
        		stores.add( storeService.getStoreByCode( userDetails.getStoreCode() ) );
        	}
        }

		theResp.setResult( stores );
		theResp.setSuccess( true );

		return theResp;
	}

    @RequestMapping( value="/member/store/{id}", produces="application/json; charset=utf-8" )
	public @ResponseBody ControllerResponse getStores( @PathVariable(value = "id") Long id ) {

		ControllerResponse theResp = getStores();

		@SuppressWarnings("unchecked")
		List<Store> stores = (List<Store>) theResp.getResult();
		if ( null != id ) {
			MemberModel member = memberService.findCustomerModel( id );
			if ( StringUtils.isNotBlank( member.getRegisteredStore() ) ) {
				Store store = storeService.getStoreByCode( member.getRegisteredStore() );
				if ( !stores.contains( store ) ) {
					stores.add( store );
				}
			}
		}
		theResp.setResult( stores );
		theResp.setSuccess( true );

		return theResp;
	}

    @RequestMapping( value="/activate/{id}/{activate}", produces="application/json; charset=utf-8" )
	public @ResponseBody ControllerResponse activate( 
			@PathVariable("id") long id,
			@PathVariable("activate") boolean activate ) {

    	ControllerResponse resp = new ControllerResponse();
    	memberService.activateMember(id, activate);
    	resp.setSuccess(true);
    	return resp;
    }

    @RequestMapping( value="/printList/validate", produces="application/json; charset=utf-8" )
    public @ResponseBody ControllerResponse printListValidate(@ModelAttribute MemberSearchDto searchDto, Model model) {

        ControllerResponse resp = new ControllerResponse();
        if (searchDto.getMemberType() != null) {
            resp.setSuccess(true);
        } else {
            resp.setSuccess(false);
            resp.setResult(Lists.newArrayList(messageSource.getMessage("error_member_type_is_required", null, LocaleContextHolder.getLocale())));
        }
        return resp;
    }
    
    @RequestMapping(value = "/printList", produces="text/html")
    public String printListForm(@ModelAttribute MemberSearchDto searchDto, Model model) {
        Long count = memberService.countMembers(searchDto);
        List<Integer> segments = new ArrayList<Integer>();
        Integer segment = 0 ;
        for (segment = 1; count>(segment*memberService.getMemberReportLimit())+1; segment++)
            segments.add(segment);
        //last segment to be added
        segments.add(segment);
        
        model.addAttribute("memberCount", count);
        if (count > 0) {
            model.addAttribute("segments", segments);
        }
        return PATH_PRINT_FORM;
    }
    
    @RequestMapping(value="/viewPrepaid/{id}", produces="text/html")
    public String viewDialog(@PathVariable("id") MemberModel member, Model uiModel) {
        uiModel.addAttribute("member",member);
        return PATH_VIEW_PREPAID;
    }
    
    @RequestMapping(method = RequestMethod.POST, value="/prepaidCardList/{id}")
    public @ResponseBody
    PrepaidCardResultList retrieveCardList(@PathVariable("id") MemberModel member) {
        PrepaidCardResultList result = memberPrepaidService.retrieveCardsByMember(member);
        return result;
    }
    
}
