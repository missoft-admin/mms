package com.transretail.crm.web.member.validator;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import com.transretail.crm.web.member.bean.RegisterDto;
import com.transretail.crm.core.service.MemberService;

/**
 * @author Allan G. Ramirez (agramirez@exist.com)
 */
@Component("memberValidator")
public class MemberValidator implements Validator {
    @Autowired
    private MemberService memberService;

    @Override
    public boolean supports(Class<?> clazz) {
        return RegisterDto.class.equals(clazz);
    }

    @Override
    public void validate(Object target, Errors errors) {
        RegisterDto form = (RegisterDto) target;
        if (memberService.usernameExists(form.getUsername())) {
            errors.rejectValue("username", "member.username.exists");
        }
        if (memberService.contactExists(form.getContact())) {
            errors.rejectValue("contact", "member.contact.exists");
        }
        if (memberService.emailExists(form.getEmail())) {
            errors.rejectValue("email", "member.email.exists");
        }
        
        String contact = form.getContact().replaceAll("(\\s)|(-)", "");
        if(!contact.matches("\\d*")) {
        	errors.rejectValue("contact", "member.contact.incorrect");
        }
    }
}
