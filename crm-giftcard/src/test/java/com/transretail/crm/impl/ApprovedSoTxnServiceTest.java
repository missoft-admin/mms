package com.transretail.crm.impl;


import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import com.google.common.collect.Sets;
import junit.framework.Assert;

import org.apache.commons.collections.CollectionUtils;
import org.joda.time.LocalDate;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import com.google.common.collect.Lists;
import com.transretail.crm.core.dto.ApprovalRemarkDto;
import com.transretail.crm.core.entity.enums.Status;
import com.transretail.crm.core.entity.lookup.LookupDetail;
import com.transretail.crm.core.entity.lookup.LookupHeader;
import com.transretail.crm.core.repo.LookupDetailRepo;
import com.transretail.crm.core.repo.LookupHeaderRepo;
import com.transretail.crm.core.service.CodePropertiesService;
import com.transretail.crm.giftcard.dto.GcAllocDto;
import com.transretail.crm.giftcard.dto.SalesOrderDto;
import com.transretail.crm.giftcard.entity.CardVendor;
import com.transretail.crm.giftcard.entity.GiftCardInventory;
import com.transretail.crm.giftcard.entity.GiftCardOrder;
import com.transretail.crm.giftcard.entity.GiftCardOrderItem;
import com.transretail.crm.giftcard.entity.ProductProfile;
import com.transretail.crm.giftcard.entity.ProductProfile.Denomination;
import com.transretail.crm.giftcard.entity.QGiftCardInventory;
import com.transretail.crm.giftcard.entity.SalesOrder;
import com.transretail.crm.giftcard.entity.SalesOrderItem;
import com.transretail.crm.giftcard.entity.support.GiftCardInventoryStatus;
import com.transretail.crm.giftcard.entity.support.GiftCardOrderStatus;
import com.transretail.crm.giftcard.entity.support.SalesOrderStatus;
import com.transretail.crm.giftcard.repo.CardVendorRepo;
import com.transretail.crm.giftcard.repo.GiftCardInventoryRepo;
import com.transretail.crm.giftcard.repo.GiftCardOrderRepo;
import com.transretail.crm.giftcard.repo.ProductProfileRepo;
import com.transretail.crm.giftcard.repo.SalesOrderAllocRepo;
import com.transretail.crm.giftcard.repo.SalesOrderItemRepo;
import com.transretail.crm.giftcard.repo.SalesOrderRepo;
import com.transretail.crm.giftcard.service.SoAllocService;
import com.transretail.crm.giftcard.service.SoApprovedTxnsService;


@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath*:/META-INF/spring/applicationContext*.xml", "classpath:META-INF/spring/applicationContext-test.xml"})
public class ApprovedSoTxnServiceTest {

	@Autowired
	SoAllocService service;
	@Autowired
	SoApprovedTxnsService soApprovedTxnsService;

	@Autowired
	SalesOrderAllocRepo repo;
	@Autowired
	SalesOrderRepo salesOrderRepo;
	@Autowired
	SalesOrderItemRepo salesOrderItemRepo;
    @Autowired
    private GiftCardInventoryRepo giftCardInventoryRepo;
    @Autowired
    private GiftCardOrderRepo giftCardOrderRepo;
    @Autowired
    private ProductProfileRepo productProfileRepo;
    @Autowired
    private CardVendorRepo cardVendorRepo;
    @Autowired
    private LookupHeaderRepo lookupHeaderRepo;
    @Autowired
    private LookupDetailRepo lookupDetailRepo;
	@Autowired
	CodePropertiesService codePropertiesService;

	private SalesOrder so;
	private final static Long SERIES_START = 1405220010000001L;
	private final static Long SERIES_END = 1405220010000005L;
	private String barcodeStart = null;
	private String barcodeEnd = null;
	

	@Before
	public void setup() {
		CardVendor cv = new CardVendor();
        cv.setFormalName("name");
        cv.setMailAddress("add");
        cv.setEmailAddress("email");
        cv.setEnabled(true);
        cv.setRegion("region");
        cv.setDefaultVendor(true);
        cv = cardVendorRepo.saveAndFlush(cv);

		GiftCardOrder co = new GiftCardOrder();
        co.setMoNumber( "140403000001" );
        co.setPoNumber( "0001" );
        co.setMoDate( new LocalDate() );
        co.setPoDate( new LocalDate() );
        co.setVendor( cv );
        co.setStatus( GiftCardOrderStatus.APPROVED );
        co.setItems( new ArrayList<GiftCardOrderItem>() );
        co = giftCardOrderRepo.saveAndFlush( co );
        
        LookupHeader hdr = new LookupHeader(codePropertiesService.getProductProfileFaceValue());
        hdr = lookupHeaderRepo.saveAndFlush(hdr);

        LookupDetail dtl = new LookupDetail("PPFV1");
        dtl.setDescription("100000");
        dtl.setHeader(hdr);
        dtl = lookupDetailRepo.saveAndFlush(dtl);

        ProductProfile pp = new ProductProfile();
        pp.setFaceValue(dtl);
        pp.setProductCode( "000000001" );
        pp.setProductDesc( "PRODUCT PROFILE DESC" );
        pp = productProfileRepo.saveAndFlush( pp );

        int inc = 1111;
        for ( long series = SERIES_START; series <= SERIES_END; series++ ) {
            GiftCardInventory gci = new GiftCardInventory();
            gci.setProfile( pp );
            gci.setProductName( pp.getProductDesc() );
            gci.setProductCode( pp.getProductCode() );
            gci.setSeqNo( 1405 );
            gci.setBatchNo( 1405 );
            gci.setOrderDate( new LocalDate() );
            gci.setFaceValue( new BigDecimal( 10 ) );
            gci.setOrder( co );
            gci.setSeries( series );
            gci.setBarcode( series + "" + inc++ );
            gci.setStatus( GiftCardInventoryStatus.IN_STOCK );
            gci = giftCardInventoryRepo.saveAndFlush( gci );
            if ( series == SERIES_START ) {
            	barcodeStart = gci.getBarcode();
            }
            else if ( series == SERIES_END ) {
            	barcodeEnd = gci.getBarcode();
            }
        }

		so = new SalesOrder();
		so.setOrderNo( "BBMMS0501001" );
		so.setOrderDate( new LocalDate() );
		so.setStatus( SalesOrderStatus.DRAFT );
		so = salesOrderRepo.saveAndFlush( so );

		SalesOrderItem soi = new SalesOrderItem();
		soi.setProduct( pp );
		soi.setOrder( so );
		soi = salesOrderItemRepo.saveAndFlush( soi );

		List<SalesOrderItem> items = Lists.newArrayList();
		items.add( soi );
		so.setItems(Sets.newHashSet(items));
		salesOrderRepo.saveAndFlush( so );
	}
	
	@After
	public void teardown() {
		repo.deleteAll();
		salesOrderItemRepo.deleteAll();
		salesOrderRepo.deleteAll();
		giftCardInventoryRepo.deleteAll();
		giftCardOrderRepo.deleteAll();
		productProfileRepo.deleteAll();
		cardVendorRepo.deleteAll();
	}



	@Test
	public void testPreallocate() {
		Assert.assertEquals( true, CollectionUtils.isEmpty( repo.findAll() ) );
		GcAllocDto dto = service.preallocate( so.getId(), barcodeStart, barcodeEnd );
		Assert.assertEquals( 5, dto.getQuantity().longValue() );
		Assert.assertEquals( false, CollectionUtils.isEmpty( repo.findAll() ) );
	}

	@Test
	public void testIsValidAllocation() {
		service.preallocate( so.getId(), barcodeStart, barcodeEnd );
		List<String> errs = Lists.newArrayList();
		Assert.assertEquals( false, service.isValidAlloc( so.getId(), barcodeStart, barcodeEnd, errs, new GcAllocDto() ) );
	}

	@Test
	@Transactional
	public void testSaveAllocation() {
		GiftCardInventory gc = giftCardInventoryRepo.findOne( QGiftCardInventory.giftCardInventory.series.eq( SERIES_END ) );
		Assert.assertEquals( GiftCardInventoryStatus.IN_STOCK, gc.getStatus() );
		service.preallocate( so.getId(), barcodeStart, barcodeEnd );
		gc = giftCardInventoryRepo.findOne( QGiftCardInventory.giftCardInventory.series.eq( SERIES_END ) );
		Assert.assertEquals( GiftCardInventoryStatus.PREALLOCATED, gc.getStatus() );
		service.saveAllocation( so.getId() );
		gc = giftCardInventoryRepo.findOne( QGiftCardInventory.giftCardInventory.series.eq( SERIES_END ) );
		Assert.assertEquals( GiftCardInventoryStatus.ALLOCATED, gc.getStatus() );
	}

	@Test
	public void testRemoveAllocation() {
		service.preallocate( so.getId(), barcodeStart, barcodeEnd );
		service.removeAllocation( so.getId() );
		GiftCardInventory gc = giftCardInventoryRepo.findOne( QGiftCardInventory.giftCardInventory.series.eq( SERIES_END ) );
		Assert.assertEquals( GiftCardInventoryStatus.IN_STOCK, gc.getStatus() );
	}

	@Test
	@Transactional
	public void testGetAllocatedGcs() {
		service.preallocate( so.getId(), barcodeStart, barcodeEnd );
		service.saveAllocation( so.getId() );
		Assert.assertEquals( 5, service.getAllocatedGcs( so.getId() ).size() );
	}

	@Test
	@Transactional
	public void testUpdateStatus() {
		Assert.assertEquals( false, SalesOrderStatus.ALLOCATED == so.getStatus() );
		SalesOrderDto dto = soApprovedTxnsService.updateStatus( so.getId(), SalesOrderStatus.ALLOCATED );
		Assert.assertEquals( SalesOrderStatus.ALLOCATED, dto.getStatus() );
	}

	@Test
	@Transactional
	public void testActivateB2bGcs() {
		service.preallocate( so.getId(), barcodeStart, barcodeEnd );
		service.saveAllocation( so.getId() );
		SalesOrderDto dto = soApprovedTxnsService.activateB2bGcs( so.getId() );
		Assert.assertEquals( SalesOrderStatus.SOLD, dto.getStatus() );
	}

	@Test
	@Transactional
	public void testSaveRemarksAndCancelApproved() {
		LookupHeader hdr = lookupHeaderRepo.saveAndFlush( new LookupHeader( "STAT", "STATUS" ) );
		LookupDetail dtl = lookupDetailRepo.saveAndFlush( 
				new LookupDetail( codePropertiesService.getDetailStatusCancelled(), "CANCELLED", hdr, Status.ACTIVE ) );

		ApprovalRemarkDto remark = new ApprovalRemarkDto();
		remark.setModelId( so.getId().toString() );
		remark.setRemarks( "REMARKS" );
		remark.setStatus( dtl.getCode() );
		SalesOrderDto dto = soApprovedTxnsService.saveRemarks( remark );

		Assert.assertEquals( SalesOrderStatus.CANCELLED, dto.getStatus() );
	}

	@Test
	@Transactional
	public void testSaveRemarksAndCancelAllocactions() {
		LookupHeader hdr = lookupHeaderRepo.saveAndFlush( new LookupHeader( "STAT", "STATUS" ) );
		LookupDetail dtl = lookupDetailRepo.saveAndFlush( 
				new LookupDetail( codePropertiesService.getDetailStatusCancelled(), "CANCELLED", hdr, Status.ACTIVE ) );

		service.preallocate( so.getId(), barcodeStart, barcodeEnd );
		service.saveAllocation( so.getId() );
		SalesOrderDto dto = soApprovedTxnsService.activateB2bGcs( so.getId() );
		Assert.assertEquals( 5, service.getAllocatedGcs( so.getId() ).size() );

		ApprovalRemarkDto remark = new ApprovalRemarkDto();
		remark.setModelId( so.getId().toString() );
		remark.setRemarks( "REMARKS" );
		remark.setStatus( dtl.getCode() );
		dto = soApprovedTxnsService.saveRemarks( remark );

		Assert.assertEquals( SalesOrderStatus.CANCELLED, dto.getStatus() );
		Assert.assertEquals( 0, service.getAllocatedGcs( so.getId() ).size() );
	}

}