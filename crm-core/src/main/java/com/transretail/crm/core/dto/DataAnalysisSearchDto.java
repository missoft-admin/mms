package com.transretail.crm.core.dto;

import java.util.List;


public class DataAnalysisSearchDto {
	
	private String type;
	private String overview;
	private String lmhBy;
	private String analysisBy;
	
	private Boolean advancedSearch;
	private String storeType;
	private String productType;
	private List<String> products;
	private List<Long> productGroups;
	private List<String> stores;
	private List<Long> storeGroups;
	
	private Integer yearFrom;
	private Integer monthFrom;
	private Integer yearTo;
	private Integer monthTo;

    private String decileYearFrom;
    private String decileMonthFrom;
    private String decileYearTo;
    private String decileMonthTo;
	
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getOverview() {
		return overview;
	}
	public void setOverview(String overview) {
		this.overview = overview;
	}
	public String getLmhBy() {
		return lmhBy;
	}
	public void setLmhBy(String lmhBy) {
		this.lmhBy = lmhBy;
	}
	public String getAnalysisBy() {
		return analysisBy;
	}
	public void setAnalysisBy(String analysisBy) {
		this.analysisBy = analysisBy;
	}
	public List<String> getProducts() {
		return products;
	}
	public void setProducts(List<String> products) {
		this.products = products;
	}
	public List<Long> getProductGroups() {
		return productGroups;
	}
	public void setProductGroups(List<Long> productGroups) {
		this.productGroups = productGroups;
	}
	public List<String> getStores() {
		return stores;
	}
	public void setStores(List<String> stores) {
		this.stores = stores;
	}
	public List<Long> getStoreGroups() {
		return storeGroups;
	}
	public void setStoreGroups(List<Long> storeGroups) {
		this.storeGroups = storeGroups;
	}
	public Integer getYearFrom() {
		return yearFrom;
	}
	public void setYearFrom(Integer yearFrom) {
		this.yearFrom = yearFrom;
	}
	public Integer getMonthFrom() {
		return monthFrom;
	}
	public void setMonthFrom(Integer monthFrom) {
		this.monthFrom = monthFrom;
	}
	public Integer getYearTo() {
		return yearTo;
	}
	public void setYearTo(Integer yearTo) {
		this.yearTo = yearTo;
	}
	public Integer getMonthTo() {
		return monthTo;
	}
	public void setMonthTo(Integer monthTo) {
		this.monthTo = monthTo;
	}
	public Boolean getAdvancedSearch() {
		return advancedSearch;
	}
	public void setAdvancedSearch(Boolean advancedSearch) {
		this.advancedSearch = advancedSearch;
	}
	public String getStoreType() {
		return storeType;
	}
	public void setStoreType(String storeType) {
		this.storeType = storeType;
	}
	public String getProductType() {
		return productType;
	}
	public void setProductType(String productType) {
		this.productType = productType;
	}

    public String getDecileYearFrom() {
        return decileYearFrom;
    }

    public void setDecileYearFrom(String decileYearFrom) {
        this.decileYearFrom = decileYearFrom;
    }

    public String getDecileMonthFrom() {
        return decileMonthFrom;
    }

    public void setDecileMonthFrom(String decileMonthFrom) {
        this.decileMonthFrom = decileMonthFrom;
    }

    public String getDecileYearTo() {
        return decileYearTo;
    }

    public void setDecileYearTo(String decileYearTo) {
        this.decileYearTo = decileYearTo;
    }

    public String getDecileMonthTo() {
        return decileMonthTo;
    }

    public void setDecileMonthTo(String decileMonthTo) {
        this.decileMonthTo = decileMonthTo;
    }
}
