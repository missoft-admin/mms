<%@ include file="../../../common/taglibs.jsp" %>


  <!-- Interested Activities -->
	<fieldset class="form-horizontal container-fluid">
	  <legend><spring:message code="label_member_indi_interestedactivities" /></legend>

    <div id="interestedActivities" class="controls checkbox">
    <form:checkboxes path="marketingDetails.interestedActivities" items="${interestedActivities}" itemLabel="description" itemValue="code"/>
    </div>
	</fieldset>


<style> input.form-control-2 { margin-top: 10px !important; } </style>