package com.transretail.crm.core.dto;

import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.joda.time.LocalDate;

import com.google.common.collect.Lists;
import com.mysema.query.types.expr.BooleanExpression;
import com.transretail.crm.common.service.dto.request.AbstractSearchFormDto;
import com.transretail.crm.core.entity.QPromoBanner;
import com.transretail.crm.core.entity.enums.PromoImageType;
import com.transretail.crm.core.util.BooleanExprUtil;

public class PromoBannerSearchDto extends AbstractSearchFormDto{
	private LocalDate startDate;
	private LocalDate endDate;
	private String name;
	private String statusCode;
	private PromoImageType type;
	
	public BooleanExpression createSearchExpression() {
		QPromoBanner promoBanner = QPromoBanner.promoBanner;
		
		List<BooleanExpression> exprs = Lists.newArrayList();
		if(StringUtils.isNotBlank(name)) {
			exprs.add(BooleanExprUtil.INSTANCE.isStringPropertyLike(promoBanner.name, name));
		}
		
		if(startDate != null && endDate != null) {
			exprs.add(promoBanner.startDate.goe(startDate).and(promoBanner.endDate.loe(endDate)));
		}
		else {
			if(startDate != null) {
				exprs.add(promoBanner.startDate.eq(startDate));
			}
			if(endDate != null) {
				exprs.add(promoBanner.endDate.eq(endDate));
			}
		}
		
		if(StringUtils.isNotBlank(statusCode)) {
			exprs.add(promoBanner.status.code.eq(statusCode));
		}
		
		if(type != null) {
			exprs.add(promoBanner.type.eq(type));
		}
		
		return BooleanExpression.allOf(exprs.toArray(new BooleanExpression[exprs.size()]));
	}

	public LocalDate getStartDate() {
		return startDate;
	}

	public void setStartDate(LocalDate startDate) {
		this.startDate = startDate;
	}

	public LocalDate getEndDate() {
		return endDate;
	}

	public void setEndDate(LocalDate endDate) {
		this.endDate = endDate;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getStatusCode() {
		return statusCode;
	}

	public void setStatusCode(String statusCode) {
		this.statusCode = statusCode;
	}

	public PromoImageType getType() {
		return type;
	}

	public void setType(PromoImageType type) {
		this.type = type;
	}
}
