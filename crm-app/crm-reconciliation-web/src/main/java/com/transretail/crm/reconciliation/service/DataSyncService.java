package com.transretail.crm.reconciliation.service;

import java.sql.Timestamp;

import com.transretail.crm.common.service.exception.MessageSourceResolvableException;

/**
 * @author Allan G. Ramirez (agramirez@exist.com)
 */
public interface DataSyncService {
    /**
     * Sync all transaction records (eg. CRM_EMPLOYEE_PURCHASE_TXN, CRM_POINTS) in local db with tt server db
     *
     * @return the least created date time of un-synchronized record.
     * @throws com.transretail.crm.common.service.exception.MessageSourceResolvableException
     */
    Timestamp syncTxnRecords() throws MessageSourceResolvableException;

    /**
     * Sync all reference records (eg. CRM_MEMBER, CRM_REF_LOOKUP_DTL) in local db with tt server db
     *
     * @return the least created date time of un-synchronized record.
     * @throws com.transretail.crm.common.service.exception.MessageSourceResolvableException
     */
    Timestamp syncReferenceRecords() throws MessageSourceResolvableException;
}
