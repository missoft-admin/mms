INSERT INTO CRM_REF_LOOKUP_HDR(CODE, DESCRIPTION) VALUES ('MKT007', 'PAYMENT TYPE WITH WILDCARDS');
INSERT INTO CRM_REF_LOOKUP_DTL(CODE, DESCRIPTION, HDR, STATUS) VALUES ('PTYP009', 'ECS', 'MKT007', 'ACTIVE');
UPDATE CRM_REF_LOOKUP_DTL SET HDR = 'MKT007'  WHERE CODE = 'PTYP008';
