create table CRM_GC_RETURN (
        id number(19,0) not null,
        CREATED_BY varchar2(255 char),
        CREATED_DATETIME timestamp,
        LAST_UPDATED_BY varchar2(255 char),
        LAST_UPDATED_DATETIME timestamp,
        FACE_AMOUNT number(19,2),
        REC_NO varchar2(255 char),
        RETURN_DATE date,
        STATUS varchar2(255 char),
        CUSTOMER number(19,0),
        ORDER_NO number(19,0),
        primary key (id)
    );

    create table CRM_GC_RETURN_DTL (
        id number(19,0) not null,
        CREATED_BY varchar2(255 char),
        CREATED_DATETIME timestamp,
        LAST_UPDATED_BY varchar2(255 char),
        LAST_UPDATED_DATETIME timestamp,
        ENDING_SERIES varchar2(255 char),
        STARTING_SERIES varchar2(255 char),
        PROFILE number(19,0),
        RTN_REC number(19,0) not null,
        primary key (id)
    );
    
    alter table CRM_GC_RETURN 
        add constraint FK_5ev0phkrx1ufu266hrkodxsju 
        foreign key (CUSTOMER) 
        references CRM_GC_CX_PROFILE;

    alter table CRM_GC_RETURN 
        add constraint FK_3936wfephrdbwfs4y4ptxecv0 
        foreign key (ORDER_NO) 
        references CRM_SALES_ORD;

    alter table CRM_GC_RETURN_DTL 
        add constraint FK_lgyokytblesny20vhd9uqmq0a 
        foreign key (PROFILE) 
        references CRM_GC_PROD_PROFILE;

    alter table CRM_GC_RETURN_DTL 
        add constraint FK_mkhwwpmobevc22xqyv07627es 
        foreign key (RTN_REC) 
        references CRM_GC_RETURN;