ALTER TABLE STORE
ADD
   (
      CREATE_DATE_AUD          DATE,
      UPDATE_DATE_AUD          DATE
   );

ALTER TABLE PRODUCT
ADD
   (
      CREATE_DATE_AUD          DATE,
      UPDATE_DATE_AUD          DATE
   );

ALTER TABLE POS_TRANSACTION
ADD
   (
      CREATE_DATE_AUD          DATE,
      UPDATE_DATE_AUD          DATE
   );

ALTER TABLE POS_TX_ITEM
ADD
   (
      CREATE_DATE_AUD          DATE,
      UPDATE_DATE_AUD          DATE
   );

