package com.transretail.crm.web.controller;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.BooleanUtils;
import org.apache.commons.lang3.StringUtils;
import org.joda.time.LocalDate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.transretail.crm.common.reporting.jasper.JRProcessor;
import com.transretail.crm.common.service.dto.request.PageSortDto;
import com.transretail.crm.common.service.dto.response.ResultList;
import com.transretail.crm.core.dto.MemberDto;
import com.transretail.crm.core.dto.MemberGroupDto;
import com.transretail.crm.core.dto.MemberGroupFieldDto;
import com.transretail.crm.core.dto.MemberGrpSearchDto;
import com.transretail.crm.core.entity.MemberGroup;
import com.transretail.crm.core.entity.MemberGroupField;
import com.transretail.crm.core.entity.MemberGroupFieldValue;
import com.transretail.crm.core.entity.MemberGroupPoints;
import com.transretail.crm.core.entity.MemberGroupRFS;
import com.transretail.crm.core.entity.ProductGroup;
import com.transretail.crm.core.entity.lookup.LookupDetail;
import com.transretail.crm.core.entity.lookup.LookupHeader;
import com.transretail.crm.core.entity.lookup.StoreGroup;
import com.transretail.crm.core.service.CodePropertiesService;
import com.transretail.crm.core.service.LookupService;
import com.transretail.crm.core.service.MemberGroupService;
import com.transretail.crm.core.service.MemberService;
import com.transretail.crm.core.service.ProductGroupService;
import com.transretail.crm.core.service.StoreGroupService;
import com.transretail.crm.giftcard.service.GiftCardInventoryService;
import com.transretail.crm.report.template.impl.MemberGroupListDocument;
import com.transretail.crm.web.controller.AbstractReportsController;
import com.transretail.crm.web.controller.util.ControllerResponse;

@RequestMapping("/groups/member")
@Controller
public class MemberGroupController extends AbstractReportsController {
	@Autowired
	MemberGroupService memberGroupService;
	@Autowired
	MemberService memberService;
	@Autowired
	LookupService lookupService;
	@Autowired
	CodePropertiesService lookupCodeService;
	@Autowired
	MessageSource messageSource;
	@Autowired
	ProductGroupService productGroupService;
	@Autowired
	StoreGroupService storeGroupService;
	@Autowired
	MemberGroupListDocument memberGroupListDocument;
	@Autowired
	GiftCardInventoryService giftCardInventoryService;
	
	private final static Logger LOGGER = LoggerFactory.getLogger(MemberGroupController.class);
	
	private static final String PATH_PRINT_FORM = "group/member/print";
	private final static String UI_ATTR_GROUP 			= "memberGroup";
	//private final static String UI_ATTR_GROUP_LIST		= "memberGroups";
	private final static String UI_ATTR_GROUP_MEMBERS	= "memberList";
	private final static String UI_ATTR_GROUP_NAME		= "groupName";
	private final static String UI_ATTR_OPERANDS		= "operands";
	private final static String UI_ATTR_FIELDS			= "groupFields";
	private final static String UI_ATTR_FIELD_VAL_MAP	= "fieldValueMap";
	
	private void populateEditForm(Model uiModel, MemberGroupDto memberGroup) {
		List<LookupHeader> headers = lookupService.getAllMemberHeaders();
		headers.remove(lookupService.getHeader(lookupCodeService.getHeaderAccountStatus()));
		Collections.sort(headers, new Comparator<LookupHeader>() {
            @Override
            public int compare(LookupHeader h1, LookupHeader h2) {
                return h1.getDescription().compareTo(h2.getDescription());
            }
        });
		uiModel.addAttribute(UI_ATTR_GROUP, memberGroup);
		uiModel.addAttribute(UI_ATTR_FIELDS, headers);
		Map<String, List<LookupDetail>> fieldValueMap = new HashMap<String, List<LookupDetail>>();
		for(LookupHeader header : headers) {
			if(header.getDescription().equalsIgnoreCase("Account status")) {
				continue;
			}
			fieldValueMap.put(header.getCode(), lookupService.getActiveDetails(header));
		}
		uiModel.addAttribute(UI_ATTR_FIELD_VAL_MAP, fieldValueMap);
		uiModel.addAttribute(UI_ATTR_OPERANDS, lookupService.getActiveDetails(lookupService.getHeader(lookupCodeService.getHeaderOperands())));
	}
	
//	private String formatWord(String word) {
//    	word = word.toLowerCase();
//    	return Character.toLowerCase(word.charAt(0)) + 
//    			WordUtils.capitalize(word).replace(" ", "").substring(1);
//    }
	
	@RequestMapping(value="/populate/{id}", method=RequestMethod.POST, produces="application/json")
	@ResponseBody
	public ControllerResponse populateGroup(@PathVariable String id, 
			@ModelAttribute(value=UI_ATTR_GROUP) MemberGroup memberGroup,
			BindingResult result, HttpServletRequest request) {
		ControllerResponse response = new ControllerResponse();
		
		MemberGroupDto group = new MemberGroupDto();
		if(id != null) {
			group = memberGroupService.getMemberGroupDto(Long.valueOf(id));
		}
		
		response.setResult(group);
		response.setSuccess(true);
		
		return response;
	}
	
	@RequestMapping(produces="text/html")
	public String list(Model uiModel) {
		populateEditForm(uiModel, new MemberGroupDto());
		return "group/member/list";
	}
	
	@RequestMapping(value = "/search", method = RequestMethod.POST)
	public @ResponseBody ResultList<MemberGroup> list(@RequestBody PageSortDto dto) {
		return memberGroupService.listMemberGroups(dto);
	}

    @RequestMapping(value = "/search/list", method = RequestMethod.POST)
    public @ResponseBody ResultList<MemberGroupDto> searchList( @RequestBody MemberGrpSearchDto dto ) {
    	return memberGroupService.search( dto );
    }

	@RequestMapping(value="/new", method=RequestMethod.POST, produces="application/json")
	@ResponseBody
	public ControllerResponse createGroup( 
			@RequestBody MemberGroupDto memberGroup,
			BindingResult result, HttpServletRequest request) {
		ControllerResponse response = new ControllerResponse();

		if(StringUtils.isEmpty(memberGroup.getName()))
			result.reject(getMessage("propkey_msg_notempty", new Object[] {"label_group_groupname"}));

		if(CollectionUtils.isEmpty(memberGroup.getMemberFields()) && ((memberGroup.getRfs() != null && BooleanUtils.isNotTrue(memberGroup.getRfs().getEnabled()))
				&& (memberGroup.getPoints() != null && BooleanUtils.isNotTrue(memberGroup.getPoints().getEnabled()))
				&& !memberGroup.isPrepaidUser()))
			result.reject(getMessage("group_member_categoryempty", new Object[] {}));
		else if(CollectionUtils.isNotEmpty(memberGroup.getMemberFields())){
			for ( MemberGroupFieldDto dto : memberGroup.getMemberFields() ) {
				if ( CollectionUtils.isEmpty( dto.getFieldValues() ) ) {
					result.reject(getMessage("group_member_err_fieldempty_notselected", new Object[] {}));
					break;
				}
				else {
					for ( MemberGroupFieldValue value : dto.getFieldValues() ) {
						if ( StringUtils.isBlank( value.getValue() ) ) {
							result.reject(getMessage("group_member_err_fieldempty_notselected", new Object[] {}));
							break;
						}
					}
				}
			}
		}
		
		if(StringUtils.isNotBlank(memberGroup.getCardTypeGroupCode())) {
			if(CollectionUtils.isEmpty(memberGroup.getMemberFields())) {
				result.reject(getMessage("group_member_cardtypegroup_nocardtype", new Object[] {}));
			}
			else {
				boolean error = true;
				for(MemberGroupFieldDto field : memberGroup.getMemberFields()) {
					if(field.getName().getCode().equals(lookupCodeService.getHeaderMembershipTier())) {
						error = CollectionUtils.isEmpty(field.getFieldValues());
						break;
					}
				}
				
				if(error) {
					result.reject(getMessage("group_member_cardtypegroup_nocardtype", new Object[] {}));
				}
			}
		}

		if(validateRfs(result, memberGroup.getRfs()) && validatePoints(result, memberGroup.getPoints()))
			isValid( result, null, memberGroup.getMemberFields(), memberGroup.getRfs() );
		
		if(result.hasErrors()) {
			response.setResult(result.getAllErrors());
			response.setSuccess(false);
			return response;
		}
		memberGroup.setPromotion(null);

//		List<MemberGroupFieldDto> fields = memberGroup.getMemberFields();
//		MemberGroup group = memberGroupService.saveMemberGroup(memberGroup);
		saveMemberGroupFields(memberGroupService.saveMemberGroup(memberGroup), memberGroup.getMemberFields());
		
		response.setSuccess(true);
		response.setResult(memberGroup);
		
		return response;
	}
	
	private String getMessage(String inMsgCode, Object[] inArgMsgCodes) {
		if(inArgMsgCodes != null) {
			for ( int i=0; i < inArgMsgCodes.length; i++ ) {
				inArgMsgCodes[i] = messageSource.getMessage( inArgMsgCodes[i].toString().toLowerCase(), null, LocaleContextHolder.getLocale() );
			}
		}
		return messageSource.getMessage(inMsgCode, inArgMsgCodes, "Info given is invalid.", LocaleContextHolder.getLocale());
	}
	
	@RequestMapping(value="/update/{id}", method=RequestMethod.POST, produces="application/json")
	@ResponseBody
	public ControllerResponse updateGroup(@PathVariable String id, 
			@RequestBody MemberGroupDto memberGroup,
			BindingResult result, HttpServletRequest request) {
		ControllerResponse response = new ControllerResponse();

		if(CollectionUtils.isEmpty(memberGroup.getMemberFields()) && ((memberGroup.getRfs() != null && BooleanUtils.isNotTrue(memberGroup.getRfs().getEnabled()))
				&& (memberGroup.getPoints() != null && BooleanUtils.isNotTrue(memberGroup.getPoints().getEnabled()))))
			result.reject(getMessage("group_member_categoryempty", new Object[] {}));
		else if(CollectionUtils.isNotEmpty(memberGroup.getMemberFields())){
			for ( MemberGroupFieldDto dto : memberGroup.getMemberFields() ) {
				if ( CollectionUtils.isEmpty( dto.getFieldValues() ) ) {
					result.reject(getMessage("group_member_err_fieldempty_notselected", new Object[] {}));
					break;
				}
				else {
					for ( MemberGroupFieldValue value : dto.getFieldValues() ) {
						if ( StringUtils.isBlank( value.getValue() ) ) {
							result.reject(getMessage("group_member_err_fieldempty_notselected", new Object[] {}));
							break;
						}
					}
				}
			}
		}
		
		if(StringUtils.isNotBlank(memberGroup.getCardTypeGroupCode())) {
			if(CollectionUtils.isEmpty(memberGroup.getMemberFields())) {
				result.reject(getMessage("group_member_cardtypegroup_nocardtype", new Object[] {}));
			}
			else {
				boolean error = true;
				for(MemberGroupFieldDto field : memberGroup.getMemberFields()) {
					if(field.getName().getCode().equals(lookupCodeService.getHeaderMembershipTier())) {
						error = CollectionUtils.isEmpty(field.getFieldValues());
						break;
					}
				}
				
				if(error) {
					result.reject(getMessage("group_member_cardtypegroup_nocardtype", new Object[] {}));
				}
			}
		}

		if(validateRfs(result, memberGroup.getRfs()) && validatePoints(result, memberGroup.getPoints()))
			isValid( result, null, memberGroup.getMemberFields(), memberGroup.getRfs() );
		
		if(result.hasErrors()) {
			response.setResult(result.getAllErrors());
			response.setSuccess(false);
			return response;
		}

		MemberGroup origGroup = memberGroupService.getMemberGroup( Long.valueOf( id ) );
		for(MemberGroupField field : memberGroupService.getMemberGroupFields(origGroup)) {
			memberGroupService.delete(field);
		}

		BeanUtils.copyProperties(memberGroup, origGroup, new String[]{"id"});
//		origGroup = memberGroupService.saveMemberGroup(origGroup);
		saveMemberGroupFields(memberGroupService.saveMemberGroup(origGroup), memberGroup.getMemberFields());
		
		response.setSuccess(true);
		return response;
	}
	
	private boolean validateRfs(BindingResult result, MemberGroupRFS rfs) {
		boolean flag = true;
		if(rfs != null && BooleanUtils.isTrue(rfs.getEnabled())) {
			if(StringUtils.defaultString(rfs.getRecency()).equalsIgnoreCase("dateRange") &&
					(rfs.getDateRangeFrom() == null || rfs.getDateRangeTo() == null)) {
				result.reject(messageSource.getMessage("member_err_invalid_date", null, LocaleContextHolder.getLocale()));
				flag = false;
			}
			
			if(BooleanUtils.isTrue(rfs.getFrequency())&& BooleanUtils.isNotTrue(rfs.getZeroFrequency()) && (
					(rfs.getFrequencyFrom() == null && rfs.getFrequencyTo() == null) ||
					(rfs.getFrequencyTo() != null && rfs.getFrequencyFrom() != null && rfs.getFrequencyFrom() > rfs.getFrequencyTo()))) {
				result.reject(messageSource.getMessage("member_err_invalid_frequency", null, LocaleContextHolder.getLocale()));
				flag = false;
			} else if(BooleanUtils.isNotTrue(rfs.getZeroFrequency())
					&& ((rfs.getFrequencyFrom() != null && rfs.getFrequencyFrom().longValue() == 0) || 
							(rfs.getFrequencyTo() != null && rfs.getFrequencyTo().longValue() == 0))) {
				result.reject(messageSource.getMessage("member_err_invalid_zerofrequency", null, LocaleContextHolder.getLocale()));
				flag = false;
			}
			
			if(BooleanUtils.isTrue(rfs.getSpend()) && (
					(rfs.getSpendFrom() == null && rfs.getSpendTo() == null) ||
					(rfs.getSpendTo() != null && rfs.getSpendFrom() != null && rfs.getSpendFrom() > rfs.getSpendTo()))) {
				result.reject(messageSource.getMessage("member_err_invalid_spend", null, LocaleContextHolder.getLocale()));
				flag = false;
			}
		}
		return flag;
	}
	
	private boolean validatePoints(BindingResult result, MemberGroupPoints points) {
		boolean flag = true;
		if(points != null && BooleanUtils.isTrue(points.getEnabled())) {
			if((points.getPointsFrom() == null && points.getPointsTo() == null) ||
					(points.getPointsTo() != null && points.getPointsFrom() != null && points.getPointsFrom() > points.getPointsTo())) {
				result.reject(messageSource.getMessage("member_err_invalid_points", null, LocaleContextHolder.getLocale()));
				flag = false;
			}
		}
		return flag;
	}

	private void isValid(BindingResult result, MemberGroup group, List<MemberGroupFieldDto> fields, MemberGroupRFS rfs ) {
		/*if (  memberService.countAllActive() == memberGroupService.getTargetMembers( fields, rfs).size()) {
			result.reject( messageSource.getMessage( "member_err_cannottargetall", null, null ) );			
		}*/
	}
	
	private void saveMemberGroupFields(MemberGroup group, List<MemberGroupFieldDto> fields) {
		if(fields == null) return;
		for(int i = 0; i < fields.size(); i++) {
			MemberGroupFieldDto field = fields.get(i);
			field.setMemberGroup(group);
			if(field.getIsLookup()) {
				field.setType(lookupService.getDetail(lookupCodeService.getDetailFieldTypeString()));
			}
			else {
				field.setType(lookupService.getDetail(lookupCodeService.getDetailFieldTypeNumeric()));
			}
			
			LOGGER.debug("FIELD: " + field.getName());
			LOGGER.debug("FIELD: " + field.getType().getDescription());
			LOGGER.debug("FIELD: " + field.getIsLookup());
			
			List<MemberGroupFieldValue> values = field.getFieldValues();
			
			LOGGER.debug("FIELD: " + values);
			
			MemberGroupField memberField = memberGroupService.saveMemberGroupField(field);
			for(MemberGroupFieldValue value : values) {
				value.setMemberGroupField(memberField);
				memberGroupService.saveMemberGroupFieldValue(value);
			}
		}
	}
	
	@RequestMapping(value="/{id}", method=RequestMethod.DELETE, produces="text/html")
	public String delete(@PathVariable("id") Long id,
            @RequestParam(value = "page", required = false) Integer page,
            @RequestParam(value = "size", required = false) Integer size,
            Model uiModel){
		MemberGroup group = memberGroupService.getMemberGroup(id);
		if(group != null) memberGroupService.delete(group);
		return "redirect:/groups/member";
	}
	
	@RequestMapping(value = "/dialog", method = RequestMethod.GET, produces = "text/html")
    public String dialog(Model uiModel) {
		populateEditForm(uiModel, new MemberGroupDto());
		return "group/member/dialog";
	}
	
	@RequestMapping(value = "/list", method = RequestMethod.GET, produces = "application/json; charset=utf-8")
    @ResponseBody
	public ControllerResponse list() {
		ControllerResponse theResponse = new ControllerResponse();    	
    	theResponse.setSuccess(true);
    	
    	theResponse.setResult(memberGroupService.getAllMemberGroups());
    		
    	return theResponse;
	}

	@RequestMapping( value = "/view/{id}", produces = "text/html" )
	public String view(
    		@PathVariable("id") Long inId,
            Model inUiModel) {

		if ( null != inId ) {
			MemberGroupDto theGroup = memberGroupService.getMemberGroupDto( inId );
			for ( MemberGroupFieldDto theField : theGroup.getMemberFields() ) {
				for ( MemberGroupFieldValue theFieldValue : theField.getFieldValues() ) {
					if ( StringUtils.isNotBlank( theFieldValue.getValue() ) ) {
						LookupDetail theValLookup = lookupService.getDetail( theFieldValue.getValue() );
						if ( null != theValLookup ) {
							theFieldValue.setValue( theValLookup.getDescription() );
						}
					}
				}
			}
			if(theGroup.getRfs() != null && BooleanUtils.isTrue(theGroup.getRfs().getEnabled())) {
				if(theGroup.getRfs().getProductGroup() != null) {
					ProductGroup group = productGroupService.findOne(theGroup.getRfs().getProductGroup());
					if(group != null)
						theGroup.getRfs().setProductGroupName(group.getName());
				}
				if(theGroup.getRfs().getStoreGroup() != null) {
					StoreGroup group = storeGroupService.findOne(theGroup.getRfs().getStoreGroup());
					if(group != null)
						theGroup.getRfs().setStoreGroupName(group.getName());
				}
			}
			inUiModel.addAttribute( "memberGroup", theGroup );
		}
		return "/groups/member/view";
    }
	
	@RequestMapping(value = "/items/list/{id}", method = RequestMethod.POST)
	public @ResponseBody ResultList<MemberDto> list(
			@PathVariable Long id, 
			@RequestBody PageSortDto dto) {
		if(id != null) {
			MemberGroup group = memberGroupService.getMemberGroup( id );
			if(StringUtils.isNotBlank(group.getMembers())) {
				return memberGroupService.getQualifiedMembers(group, dto);
			} else {
			    if (group.getPrepaidUser()) {
			        ResultList<MemberDto> members = memberGroupService.getQualifiedMembers( 
	                        memberGroupService.getMemberGroupFieldsDto(group), group, dto);
			        return giftCardInventoryService.removeNonPrepaidUserMembers(members);
			    } else {
			        return memberGroupService.getQualifiedMembers( 
						memberGroupService.getMemberGroupFieldsDto(group), group, dto);
			    }
			}
		} else
			return null;
	}
	
	@RequestMapping(value = "/print/{id}", produces="text/html")
    public String printForm(@PathVariable Long id, 
            Model model) {
	    if(id != null) {
            Long count = 0L;
            MemberGroup group = memberGroupService.getMemberGroup(id);
            if(StringUtils.isNotBlank(group.getMembers())) {
                count = memberGroupService.getQualifiedMembersCount(group, null);
            } else {
                if (group.getPrepaidUser()) {
                    count = giftCardInventoryService.getQualifiedMembersCountPrepaidUsers( 
                            memberGroupService.getMemberGroupFieldsDto(group), group, new PageSortDto());
                } else {
                    count = memberGroupService.getQualifiedMembersCount( 
                            memberGroupService.getMemberGroupFieldsDto(group), group);
                }
            }
        
            List<Integer> segments = new ArrayList<Integer>();
            Integer segment = 0 ;
            for (segment = 1; count>(segment*1000)+1; segment++)
                segments.add(segment);
            //last segment to be added
            segments.add(segment);
            
            model.addAttribute("memberGroupCount", count);
            model.addAttribute("groupMember", id);
            if (count > 0) {
                model.addAttribute("segments", segments);
            }
	    }
        return PATH_PRINT_FORM;
    }
	
	@RequestMapping(value = "/print", method=RequestMethod.GET)
    public void exportMembersList(
            @RequestParam(value="segment") Integer segment,
            @RequestParam(value="groupMember") Long id,
            HttpServletResponse response) throws Exception {
        
        JRProcessor jrProcessor = memberGroupListDocument.createJrProcessor(segment, id);
        renderExcelReport(response, jrProcessor, "Member Group List Document (" + LocalDate.now() +")");
        
    }
}
