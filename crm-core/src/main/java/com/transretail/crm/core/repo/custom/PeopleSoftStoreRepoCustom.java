package com.transretail.crm.core.repo.custom;

import com.transretail.crm.core.entity.PeopleSoftStore;

/**
 *
 * @author Monte Cillo Co (mco@exist.com)
 */
public interface PeopleSoftStoreRepoCustom {

    PeopleSoftStore findByStoreCode(String storeCode);
}
