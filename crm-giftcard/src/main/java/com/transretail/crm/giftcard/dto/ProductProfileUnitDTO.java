package com.transretail.crm.giftcard.dto;

import com.transretail.crm.core.entity.lookup.LookupDetail;

public class ProductProfileUnitDTO {

	private Long id;
	private LookupDetail businessUnit;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public LookupDetail getBusinessUnit() {
		return businessUnit;
	}

	public void setBusinessUnit(LookupDetail businessUnit) {
		this.businessUnit = businessUnit;
	}

}
