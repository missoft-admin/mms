package com.transretail.crm.core.entity.enums;

/**
 * 
 * transaction type when processing points
 * 
 */
public enum PointTxnType {

    EARN, REDEEM, ADJUST, VOID, RETURN, RETRIEVE, EXPIRE, REWARD, RECON, REFUND

}
