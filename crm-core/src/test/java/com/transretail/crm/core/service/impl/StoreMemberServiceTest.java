package com.transretail.crm.core.service.impl;

import java.util.List;

import com.transretail.crm.core.dto.StoreMemberDto;
import com.transretail.crm.core.dto.StoreMemberSearchDto;
import com.transretail.crm.core.entity.StoreMember;
import com.transretail.crm.core.repo.StoreMemberRepo;
import com.transretail.crm.core.service.StoreMemberService;

import junit.framework.Assert;

import org.h2.util.StringUtils;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Configurable;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import com.google.common.collect.Lists;


@Configurable
@Transactional
@ContextConfiguration(locations = "classpath*:/META-INF/spring/applicationContext*.xml")
@RunWith(SpringJUnit4ClassRunner.class)
public class StoreMemberServiceTest {

    private static  String INVALID_ACCOUNT_ID = "111";

    private static  String STORE_ONE_COMPANY_ONE_CARDTYPE = "1111";

    private static  String STORE_ONE_COMPANY_ALL_CARDTYPE = "2222";

    private static  String STORE_ALL_COMPANY_ONE_CARDTYPE = "3333";

    private static  String STORE_ALL_COMPANY_ALL_CARDTYPE = "4444";

    private static  String STORE_ONE_COMPANY_MULTIPLE_CARDTYPE = "5555";

    private static  String STORE_MULTIPLE_COMPANY_ONE_CARDTYPE = "6666";

    private static  String STORE_MULTIPLE_COMPANY_MULTIPLE_CARDTYPE = "7777";


    private static  String COMPANY1 = "11";

    private static  String COMPANY2 = "22";

    private static  String CARDTYPE1 = "66";

    private static  String CARDTYPE2 = "77";

    private static  String ACCT1 = "116600000000000";

    private static  String ACCT2 = "226600000000000";

    private static  String ACCT3 = "118800000000000";



    @Autowired
    StoreMemberRepo storeMemberRepo;



    @Autowired
    StoreMemberService storeMemberService;



    @Before
    public void setup() {
        StoreMember storeMember = new StoreMember();
        storeMember.setStore(STORE_ONE_COMPANY_ONE_CARDTYPE);
        storeMember.setCompany(COMPANY1);
        storeMember.setCardType(CARDTYPE1);
        storeMemberRepo.saveAndFlush(storeMember);

        storeMember = new StoreMember();
        storeMember.setStore(STORE_ONE_COMPANY_ALL_CARDTYPE);
        storeMember.setCompany(COMPANY1);
        storeMemberRepo.saveAndFlush(storeMember);

        storeMember = new StoreMember();
        storeMember.setStore(STORE_ALL_COMPANY_ONE_CARDTYPE);
        storeMember.setCardType(CARDTYPE1);
        storeMemberRepo.saveAndFlush(storeMember);

        storeMember = new StoreMember();
        storeMember.setStore(STORE_ALL_COMPANY_ALL_CARDTYPE);
        storeMemberRepo.saveAndFlush(storeMember);

        storeMember = new StoreMember();
        storeMember.setStore(STORE_ONE_COMPANY_MULTIPLE_CARDTYPE);
        storeMember.setCompany(COMPANY1);
        storeMember.setCardType(CARDTYPE1);
        storeMemberRepo.saveAndFlush(storeMember);

        storeMember = new StoreMember();
        storeMember.setStore(STORE_ONE_COMPANY_MULTIPLE_CARDTYPE);
        storeMember.setCompany(COMPANY1);
        storeMember.setCardType(CARDTYPE2);
        storeMemberRepo.saveAndFlush(storeMember);

        storeMember = new StoreMember();
        storeMember.setStore(STORE_MULTIPLE_COMPANY_ONE_CARDTYPE);
        storeMember.setCompany(COMPANY1);
        storeMember.setCardType(CARDTYPE1);
        storeMemberRepo.saveAndFlush(storeMember);

        storeMember = new StoreMember();
        storeMember.setStore(STORE_MULTIPLE_COMPANY_ONE_CARDTYPE);
        storeMember.setCompany(COMPANY2);
        storeMember.setCardType(CARDTYPE1);
        storeMemberRepo.saveAndFlush(storeMember);


        storeMember = new StoreMember();
        storeMember.setStore(STORE_MULTIPLE_COMPANY_MULTIPLE_CARDTYPE);
        storeMember.setCompany(COMPANY1);
        storeMember.setCardType(CARDTYPE1);
        storeMemberRepo.saveAndFlush(storeMember);

        storeMember = new StoreMember();
        storeMember.setStore(STORE_MULTIPLE_COMPANY_MULTIPLE_CARDTYPE);
        storeMember.setCompany(COMPANY2);
        storeMember.setCardType(CARDTYPE2);
        storeMemberRepo.saveAndFlush(storeMember);


    }

    @After
    public void teardown() {
        storeMemberRepo.deleteAll();

    }


    @Test
    public void testFindByStore() {
        List<StoreMember> storeMember = storeMemberService.findByStore(STORE_ONE_COMPANY_ONE_CARDTYPE);

        Assert.assertNotNull(storeMember);
        Assert.assertEquals(1, storeMember.size());

        storeMember = storeMemberService.findByStore(STORE_ONE_COMPANY_MULTIPLE_CARDTYPE);

        Assert.assertNotNull(storeMember);
        Assert.assertEquals(2, storeMember.size());
    }

    @Test
    public void testValidMember() {

        StringBuffer messageCode = new StringBuffer();

        //invalid id
        Assert.assertFalse(storeMemberService.isValidMember(STORE_ONE_COMPANY_ONE_CARDTYPE, INVALID_ACCOUNT_ID, messageCode));
        Assert.assertEquals("MSG014", messageCode.toString());

        //test valid company
        Assert.assertTrue(storeMemberService.isValidMember(STORE_ONE_COMPANY_ONE_CARDTYPE, ACCT1, messageCode));
        Assert.assertTrue(storeMemberService.isValidMember(STORE_MULTIPLE_COMPANY_ONE_CARDTYPE, ACCT1, messageCode));
        Assert.assertTrue(storeMemberService.isValidMember(STORE_ONE_COMPANY_MULTIPLE_CARDTYPE, ACCT1, messageCode));
        Assert.assertTrue(storeMemberService.isValidMember(STORE_ALL_COMPANY_ALL_CARDTYPE, ACCT1, messageCode));
        Assert.assertTrue(storeMemberService.isValidMember(STORE_ALL_COMPANY_ONE_CARDTYPE, ACCT1, messageCode));
        Assert.assertTrue(storeMemberService.isValidMember(STORE_ONE_COMPANY_ALL_CARDTYPE, ACCT1, messageCode));

        Assert.assertTrue(storeMemberService.isValidMember(STORE_MULTIPLE_COMPANY_ONE_CARDTYPE, ACCT2, messageCode));
        Assert.assertTrue(storeMemberService.isValidMember(STORE_ALL_COMPANY_ALL_CARDTYPE, ACCT2, messageCode));
        Assert.assertTrue(storeMemberService.isValidMember(STORE_ALL_COMPANY_ONE_CARDTYPE, ACCT2, messageCode));

        //test valid company with invalid cardtype as input
        Assert.assertTrue(storeMemberService.isValidMember(STORE_ALL_COMPANY_ALL_CARDTYPE, ACCT3, messageCode));
        Assert.assertTrue(storeMemberService.isValidMember(STORE_ONE_COMPANY_ALL_CARDTYPE, ACCT3, messageCode));


        //test invalid company
        Assert.assertFalse(storeMemberService.isValidMember(STORE_ONE_COMPANY_ONE_CARDTYPE, ACCT2, messageCode));
        Assert.assertFalse(storeMemberService.isValidMember(STORE_ONE_COMPANY_MULTIPLE_CARDTYPE, ACCT2, messageCode));


        //test valid cardtype
        Assert.assertTrue(storeMemberService.isValidMember(STORE_ONE_COMPANY_ONE_CARDTYPE, ACCT1, messageCode));
        Assert.assertTrue(storeMemberService.isValidMember(STORE_MULTIPLE_COMPANY_ONE_CARDTYPE, ACCT1, messageCode));
        Assert.assertTrue(storeMemberService.isValidMember(STORE_ONE_COMPANY_MULTIPLE_CARDTYPE, ACCT1, messageCode));
        Assert.assertTrue(storeMemberService.isValidMember(STORE_ALL_COMPANY_ALL_CARDTYPE, ACCT1, messageCode));
        Assert.assertTrue(storeMemberService.isValidMember(STORE_ALL_COMPANY_ONE_CARDTYPE, ACCT1, messageCode));
        Assert.assertTrue(storeMemberService.isValidMember(STORE_ONE_COMPANY_ALL_CARDTYPE, ACCT1, messageCode));


    }

    @Test
    public void testSearchStoreMembers() {
        StoreMemberSearchDto searchDto =  new StoreMemberSearchDto();
        searchDto.setCompany(COMPANY1);
        searchDto.setStore(STORE_ONE_COMPANY_ONE_CARDTYPE);
        searchDto.setCardType(CARDTYPE1);
        Assert.assertTrue(storeMemberService.searchStoreMembers(searchDto).getNumberOfElements() > 0);
    }
    
    @Test
    public void testSaveUpdateDeleteStoreMemberDto() {
        StoreMemberDto dto = new StoreMemberDto();
        dto.setStore("1234");
        dto.setCompany("99");
        dto.setCardType("88");
        
        storeMemberService.saveStoreMemberDto(dto);
        
        List<StoreMember> storeMembers = storeMemberService.findByStore("1234");
        Assert.assertTrue(storeMembers.size() > 0);
        
        dto = new StoreMemberDto(storeMembers.get(0));
        dto.setCompany("88");
        storeMemberService.updateStoreMemberDto(dto);
        List<StoreMember> storeMembersUpdated = storeMemberService.findByStore("1234");
        
        Assert.assertTrue(storeMembersUpdated.size() > 0);
        Assert.assertEquals(storeMembersUpdated.get(0).getCompany(),"88");
        
        storeMemberService.deleteStoreMemberModel(storeMembersUpdated.get(0));
        storeMembers = storeMemberService.findByStore("1234");
        Assert.assertTrue(storeMembers.size() == 0);
    }
    
    @Test
    public void testStoreMemberSchemeExists() {
        StoreMemberDto dto =  new StoreMemberDto();
        dto.setCompany(COMPANY1);
        dto.setStore(STORE_ONE_COMPANY_ONE_CARDTYPE);
        dto.setCardType(CARDTYPE1);
        Assert.assertTrue(storeMemberService.isStoreMemberSchemeExists(dto));
    }
}
