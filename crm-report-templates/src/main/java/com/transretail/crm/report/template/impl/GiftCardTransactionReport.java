package com.transretail.crm.report.template.impl;

import com.google.common.collect.ImmutableMap;
import com.google.common.collect.ImmutableMap.Builder;
import com.google.common.collect.Lists;
import com.google.common.collect.Sets;
import com.mysema.query.BooleanBuilder;
import com.mysema.query.jpa.impl.JPAQuery;
import com.mysema.query.support.Expressions;
import com.mysema.query.types.ConstantImpl;
import com.mysema.query.types.Expression;
import com.mysema.query.types.Projections;
import com.mysema.query.types.QBean;
import com.mysema.query.types.expr.DateExpression;
import com.mysema.query.types.template.DateTemplate;
import com.transretail.crm.common.reporting.engine.data.JRQueryDSLDataSource;
import com.transretail.crm.common.reporting.engine.data.ReportDataSource;
import com.transretail.crm.common.reporting.jasper.ClasspathFileResolver;
import com.transretail.crm.common.reporting.jasper.DefaultJRProcessor;
import com.transretail.crm.common.reporting.jasper.JRProcessor;
import com.transretail.crm.common.util.DateUtil;
import com.transretail.crm.core.entity.QPeopleSoftStore;
import com.transretail.crm.core.entity.enums.ReportType;
import com.transretail.crm.core.entity.lookup.Store;
import com.transretail.crm.core.reporting.support.StoreFieldValueCustomizer;
import com.transretail.crm.core.service.StoreService;
import com.transretail.crm.giftcard.entity.*;
import com.transretail.crm.giftcard.entity.support.GiftCardSaleTransaction;
import com.transretail.crm.giftcard.entity.support.GiftCardSalesType;
import com.transretail.crm.report.template.FilterField;
import com.transretail.crm.report.template.NoDataFoundSupport;
import com.transretail.crm.report.template.ValidatableReportTemplate;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.apache.commons.lang3.StringUtils;
import org.joda.time.DateTime;
import org.joda.time.LocalDateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author Monte Cillo Co (mco@exist.com)
 */
@Service("giftCardTransactionReport")
public class GiftCardTransactionReport implements ValidatableReportTemplate, NoDataFoundSupport {

    static final String TEMPLATE_NAME = "Gift Card Transaction Report";
    static final String REPORT_NAME = "reports/gift_card_transaction.jasper";
    static final String PERMISSION_CODE = "GC_TRANSACTION_PERMISSION";
    @Autowired
    private MessageSource messageSource;
    @PersistenceContext
    private EntityManager em;
    @Autowired
    private StoreService storeService;
    private static final DateTimeFormatter DATE_FORMATTER = DateTimeFormat.forPattern(DateUtil.SEARCH_DATE_FORMAT);
    static final String HEAD_OFFICE = "Head Office";
    static final String HO_CODE = "INVT001";
    static final String REF_TRANSACTION_NO = "REF_TRANSACTION_NO";
    static final String TRANSACTION_NO = "TRANSACTION_NO";
    static final String POS_TERMINAL_NO = "POS_TERMINAL_NO";
    static final String CARD_NO = "CARD_NO";
    static final String SALES_TYPE = "SALES_TYPE";
    static final String TRANSACTION_TYPE = "TRANSACTION_TYPE";
    static final String CASHIER_ID = "CASHIER_ID";
    static final String STORE = "STORE";
    static final String DATE_TRUNC = "DD";
    static final String TRANSACTION_TIME_TO = "TRANSACTION_TIME_TO";
    static final String TRANSACTION_TIME_FROM = "TRANSACTION_TIME_FROM";
    static final String BOOKED_DATE_TO = "BOOKED_DATE_TO";
    static final String BOOKED_DATE_FROM = "BOOKED_DATE_FROM";

    @Override
    public String getName() {
        return TEMPLATE_NAME;
    }

    @Override
    public String getDescription() {
        return messageSource.getMessage("gc.transaction.report.title", null, LocaleContextHolder.getLocale());
    }

    @Override
    public Set<FilterField> getFilters() {
        Set<FilterField> filters = Sets.newLinkedHashSetWithExpectedSize(10);

        String terminalLabel = messageSource.getMessage("gc.transaction.report.filter.terminal", null, LocaleContextHolder.getLocale());
        filters.add(FilterField.createInputField(POS_TERMINAL_NO, terminalLabel));
        String txNoLabel = messageSource.getMessage("gc.transaction.report.filter.tx.no", null, LocaleContextHolder.getLocale());
        filters.add(FilterField.createInputField(TRANSACTION_NO, txNoLabel));
        String refTxNoLabel = messageSource.getMessage("gc.transaction.report.filter.ref.tx.no", null, LocaleContextHolder.getLocale());
        filters.add(FilterField.createInputField(REF_TRANSACTION_NO, refTxNoLabel));
        String dateTimeFromLabel = messageSource.getMessage("gc.transaction.report.filter.tx.time.from", null, LocaleContextHolder.getLocale());
        filters.add(FilterField.createDateField(TRANSACTION_TIME_FROM, dateTimeFromLabel, dateTimeFromLabel, true));
        String txTimeToNoLabel = messageSource.getMessage("gc.transaction.report.filter.tx.time.to", null, LocaleContextHolder.getLocale());
        filters.add(FilterField.createDateField(TRANSACTION_TIME_TO, txTimeToNoLabel, txTimeToNoLabel, true));
        String bookFromLabel = messageSource.getMessage("gc.transaction.report.filter.book.from", null, LocaleContextHolder.getLocale());
        filters.add(FilterField.createDateField(BOOKED_DATE_FROM, bookFromLabel, bookFromLabel));
        String bookToLabel = messageSource.getMessage("gc.transaction.report.filter.book.to", null, LocaleContextHolder.getLocale());
        filters.add(FilterField.createDateField(BOOKED_DATE_TO, bookToLabel, bookToLabel));
        String storeLabel = messageSource.getMessage("gc.transaction.report.filter.store", null, LocaleContextHolder.getLocale());
        filters.add(FilterField.createDropdownField(STORE, storeLabel, createStoreOptions()));
        String cashierLabel = messageSource.getMessage("gc.transaction.report.filter.cashier", null, LocaleContextHolder.getLocale());
        filters.add(FilterField.createInputField(CASHIER_ID, cashierLabel));
        String cardNoLabel = messageSource.getMessage("gc.transaction.report.filter.card.no", null, LocaleContextHolder.getLocale());
        filters.add(FilterField.createInputField(CARD_NO, cardNoLabel));
        String transactionTypeLabel = messageSource.getMessage("gc.transaction.report.filter.tx.type", null, LocaleContextHolder.getLocale());
        filters.add(FilterField.createDropdownField(TRANSACTION_TYPE, transactionTypeLabel, createTransactionTypeOptions()));
        String salesTypeLabel = messageSource.getMessage("gc.transaction.report.filter.sales.type", null, LocaleContextHolder.getLocale());
        filters.add(FilterField.createDropdownField(SALES_TYPE, salesTypeLabel, createSalesTypeOptions()));

        return filters;
    }

    @Override
    public boolean canView(Set<String> permissions) {
        return permissions.contains(PERMISSION_CODE);
    }

    @Override
    public JRProcessor createJrProcessor(Map<String, String> parameters) {

        DefaultJRProcessor jrProcessor = new DefaultJRProcessor(REPORT_NAME);
        jrProcessor.setFileResolver(new ClasspathFileResolver("reports"));
        jrProcessor.addParameter("SUB_DATA_SOURCE", createDataSource(parameters));
        // TODO: cleanup
        for (Entry<String, String> entry : parameters.entrySet()) {
            jrProcessor.addParameter(entry.getKey(), entry.getValue());
        }

        return jrProcessor;
    }

    @Override
    public List<String> validate(Map<String, String> parameters) {
        List<String> errors = Lists.newArrayList();

        String txFrom = parameters.get(TRANSACTION_TIME_FROM);
        String txTo = parameters.get(TRANSACTION_TIME_TO);

        if (StringUtils.isNotBlank(txFrom) && StringUtils.isNotBlank(txTo)) {
            DateTime dateFrom = DATE_FORMATTER.parseDateTime(txFrom);
            DateTime dateTo = DATE_FORMATTER.parseDateTime(txTo);
            if (dateFrom.isAfter(dateTo) || dateTo.isBefore(dateFrom)) {
                errors.add("Transaction Date is invalid range.");
            }
        } else {
            errors.add("Transaction Date From/To is required.");
        }

        return errors;
    }

    @Transactional(readOnly = true)
    public ReportDataSource createDataSource(Map<String, String> parameters) {
        //<editor-fold defaultstate="collapsed" desc="Native Query">
        //
        //select 
        //  tx.REF_TXN_NO as "Reference Transaction Number",
        //  tx.txn_no as "Transaction Number",
        //  tx.terminal_id as "POS Terminal Number",
        //  tx.txn_date as "Transaction Time",
        //  tx.created_datetime as "Book Date",
        //  tx.merchant_id as "Store",
        //  tx.cashier_id as "Cashier ID",
        //  gci.PRODUCT_CODE as "Product Code",
        //  case
        //    when tx.txn_type = 'VOID_ACTIVATED' then -(soi.PRINT_FEE/soi.quantity) 
        //    else (soi.PRINT_FEE/soi.quantity) end as "Card Fee",
        //  tx.sales_type as "Sales Type",
        //  tx.txn_type as "Transaction Type",
        //  tx.discount * gci.face_value / 100 as "Discount Amount",
        //  txi.curr_amount as "Balance Before",
        //  txi.txn_amount as "Transaction Amount",
        //  case when tx.txn_type = 'ACTIVATION' then txi.txn_amount 
        //   else (txi.curr_amount - abs(txi.txn_amount)) end as "Balance After",
        //  tx.settlement as "Settlement",
        //  ps.psoft_code as "PSOFT_CODE"
        //from crm_gc_transaction tx
        //  inner join crm_gc_transaction_item txi 
        //    on tx.id = txi.transaction_id
        //  left outer join crm_store_psoft ps
        //    on tx.psoft_store_mapping_id=ps.id
        //  inner join crm_gc_inventory gci
        //    on txi.card_no = gci.barcode
        //  left join crm_sales_ord so 
        //    on tx.SALES_ORDER_ID = so.id
        //  left join crm_sales_ord_item soi
        //    on soi.sales_order = so.id
        //    left join crm_gc_prod_profile pp on soi.product =  pp.id
        //where tx.txn_type not in ('PRE_ACTIVATION', 'PRE_REDEMPTION')
        //  and pp.id = gci.profile_id   and trunc(tx.txn_date,'DD') = trunc(now(),'DD');
        //</editor-fold> 
        QGiftCardTransaction qtx = QGiftCardTransaction.giftCardTransaction;
        QGiftCardTransaction qrtx = new QGiftCardTransaction("qtx");
        QGiftCardTransactionItem qtxi = QGiftCardTransactionItem.giftCardTransactionItem;
        QPeopleSoftStore qps = QPeopleSoftStore.peopleSoftStore;
        QGiftCardInventory qgc = QGiftCardInventory.giftCardInventory;
        QSalesOrder qso = QSalesOrder.salesOrder;
        QSalesOrderItem qsoi = QSalesOrderItem.salesOrderItem;
        QProductProfile qpp = QProductProfile.productProfile;
        
        JPAQuery query = createQuery(parameters).orderBy(qtx.transactionDate.desc());

        final QBean<ReportBean> projection = Projections.bean(ReportBean.class,
                qrtx.transactionNo.as("refTransactionNo"),
                qtx.transactionNo.as("transactionNo"),
                qtx.terminalId.as("posTerminalNo"),
                qtx.transactionDate.as("transactionTime"),
                qtx.created.as("bookDate"),
                qtx.merchantId.as("store"),
                qtx.cashierId.as("cashier"),
                qgc.barcode.as("cardNo"),
                qpp.productCode.as("productCode"),
                Expressions.cases()
                .when(qtx.transactionType.eq(GiftCardSaleTransaction.VOID_ACTIVATED))
                .then(qsoi.printFee.negate().doubleValue())
                .otherwise(qsoi.printFee.doubleValue())
                .divide(qsoi.quantity).doubleValue().as("cardFee"),
                qtx.salesType.stringValue().as("salesType"),
                qtx.transactionType.stringValue().as("transactionType"),
                qtx.discountPercentage.multiply(qgc.faceValue)
                .divide(100).doubleValue().as("discountAmount"),
                qtxi.currentAmount.as("balanceBefore"),
                qtxi.transactionAmount.as("transactionAmount"),
                qtxi.currentAmount.add(qtxi.transactionAmount)
                .doubleValue().as("balanceAfter"),
                qtx.settlement.as("settlement"),
                qps.peoplesoftCode.as("psStoreCode"));

        return new JRQueryDSLDataSource(query, projection,
                new StoreFieldValueCustomizer("store", storeService));
    }

    private JPAQuery createQuery(Map<String, String> parameters) {
        QGiftCardTransaction qtx = QGiftCardTransaction.giftCardTransaction;
        QGiftCardTransaction qrtx = new QGiftCardTransaction("qtx");
        QGiftCardTransactionItem qtxi = QGiftCardTransactionItem.giftCardTransactionItem;
        QPeopleSoftStore qps = QPeopleSoftStore.peopleSoftStore;
        QGiftCardInventory qgc = QGiftCardInventory.giftCardInventory;
        QSalesOrder qso = QSalesOrder.salesOrder;
        QSalesOrderItem qsoi = QSalesOrderItem.salesOrderItem;
        QProductProfile qpp = QProductProfile.productProfile;

        BooleanBuilder bb = new BooleanBuilder();

        String tmp = parameters.get(TRANSACTION_NO);
        if (StringUtils.isNotBlank(tmp)) {
            bb.and(qtx.transactionNo.eq(tmp));
        }

        tmp = parameters.get(REF_TRANSACTION_NO);
        if (StringUtils.isNotBlank(tmp)) {
            bb.and(qrtx.transactionNo.eq(tmp));
        }

        tmp = parameters.get(POS_TERMINAL_NO);
        if (StringUtils.isNotBlank(tmp)) {
            bb.and(qtx.terminalId.eq(tmp));
        }

        String bdf = parameters.get(BOOKED_DATE_FROM);
        String bdt = parameters.get(BOOKED_DATE_TO);
        if (StringUtils.isNotBlank(bdf) && StringUtils.isNotBlank(bdt)) {
            DateTime dateFrom = DATE_FORMATTER.parseDateTime(bdf);
            DateTime dateTo = DATE_FORMATTER.parseDateTime(bdt);
            bb.and(truncateDateTime(qtx.created, DATE_TRUNC).between(dateFrom, dateTo));
            parameters.put(BOOKED_DATE_FROM, DATE_FORMATTER.print(dateFrom));
            parameters.put(BOOKED_DATE_TO, DATE_FORMATTER.print(dateTo));
        }

        String txFrom = parameters.get(TRANSACTION_TIME_FROM);
        String txTo = parameters.get(TRANSACTION_TIME_TO);
        if (StringUtils.isNotBlank(txFrom) && StringUtils.isNotBlank(txTo)) {
            LocalDateTime dateFrom = DATE_FORMATTER.parseLocalDateTime(txFrom);
            LocalDateTime dateTo = DATE_FORMATTER.parseLocalDateTime(txTo);
            bb.and(truncate(qtx.transactionDate, DATE_TRUNC).between(dateFrom, dateTo));
            parameters.put(TRANSACTION_TIME_FROM, DATE_FORMATTER.print(dateFrom));
            parameters.put(TRANSACTION_TIME_TO, DATE_FORMATTER.print(dateTo));
        }

        tmp = parameters.get(STORE);
        if (StringUtils.isNotBlank(tmp)) {
            bb.and(qtx.merchantId.eq(tmp));
            String codeAndName = HEAD_OFFICE;
            if (!HO_CODE.equals(tmp)) {
                final Store store = storeService.getStoreByCode(tmp);
                codeAndName = null == store ? tmp : store.getCodeAndName();
            }
            parameters.put(STORE, codeAndName);
        }

        tmp = parameters.get(CASHIER_ID);
        if (StringUtils.isNotBlank(tmp)) {
            bb.and(qtx.cashierId.eq(tmp));
        }

        tmp = parameters.get(TRANSACTION_TYPE);
        if (StringUtils.isNotBlank(tmp)) {
            bb.and(qtx.transactionType.stringValue().eq(tmp));
        }

        tmp = parameters.get(SALES_TYPE);
        if (StringUtils.isNotBlank(tmp)) {
            bb.and(qtx.salesType.stringValue().eq(tmp));
        }

        tmp = parameters.get(CARD_NO);
        if (StringUtils.isNotBlank(tmp)) {
            bb.and(qgc.barcode.eq(tmp));
        }

        JPAQuery query = new JPAQuery(em).from(qtx)
                .innerJoin(qtx.transactionItems, qtxi)
                .leftJoin(qtx.refTransaction, qrtx)
                .leftJoin(qtx.peoplesoftStoreMapping, qps)
                .innerJoin(qtxi.giftCard, qgc)
                .leftJoin(qtx.salesOrder, qso)
                .leftJoin(qso.items, qsoi)
                .leftJoin(qsoi.product, qpp)
                .leftJoin(qgc.profile, qpp)
                .where(qtx.transactionType.notIn(
                                GiftCardSaleTransaction.PRE_ACTIVATION,
                                GiftCardSaleTransaction.PRE_REDEMPTION)
                        .and(bb.getValue()));

        return query;
    }

    @Override
    public ReportType[] getReportTypes() {
        return new ReportType[]{ReportType.EXCEL};
    }

    // Good to have this in a util class	
    protected DateExpression<LocalDateTime> truncate(Expression<?> expression, String pattern) {
        return DateTemplate.create(LocalDateTime.class, "trunc({0}, '{1s}')", expression, ConstantImpl.create(pattern));
    }

    protected DateExpression<DateTime> truncateDateTime(Expression<?> expression, String pattern) {
        return DateTemplate.create(DateTime.class, "trunc({0}, '{1s}')", expression, ConstantImpl.create(pattern));
    }

    private Map<String, String> createSalesTypeOptions() {
        Builder<String, String> builder = ImmutableMap.<String, String>builder().put("", "");
        for (GiftCardSalesType value : GiftCardSalesType.values()) {
            builder.put(value.name(), value.name());
        }

        return builder.build();
    }

    private Map<String, String> createTransactionTypeOptions() {
        Builder<String, String> builder = ImmutableMap.<String, String>builder().put("", "")
                .put(GiftCardSaleTransaction.ACTIVATION.name(), GiftCardSaleTransaction.ACTIVATION.name())
                .put(GiftCardSaleTransaction.VOID_ACTIVATED.name(), GiftCardSaleTransaction.VOID_ACTIVATED.name())
                .put(GiftCardSaleTransaction.REDEMPTION.name(), GiftCardSaleTransaction.REDEMPTION.name())
                .put(GiftCardSaleTransaction.VOID_REDEMPTION.name(), GiftCardSaleTransaction.VOID_REDEMPTION.name())
                .put(GiftCardSaleTransaction.RELOAD.name(), GiftCardSaleTransaction.RELOAD.name())
                .put(GiftCardSaleTransaction.VOID_RELOAD.name(), GiftCardSaleTransaction.VOID_RELOAD.name());

        return builder.build();
    }

    private Map<String, String> createStoreOptions() {
        return ImmutableMap.<String, String>builder()
                .put("", "")
                .put(HO_CODE, HEAD_OFFICE).
                putAll(storeService.getAllStoresHashMap()).build();
    }

    @Override
    public boolean isEmpty(Map<String, String> filter) {
        return createQuery(filter).notExists();
    }

    //<editor-fold defaultstate="collapsed" desc="Report Bean Definition">
    public static class ReportBean {

        private String refTransactionNo;
        private String transactionNo;
        private String posTerminalNo;
        private LocalDateTime transactionTime;
        private DateTime bookDate;
        private String store;
        private String cashier;
        private String cardNo;
        private String productCode;
        private Double cardFee;
        private String salesType;
        private String transactionType;
        private double discountAmount;
        private double balanceBefore;
        private double transactionAmount;
        private double balanceAfter;
        private boolean settlement;
        private String psStoreCode;

        public String getCardNo() {
            return cardNo;
        }

        public void setCardNo(String cardNo) {
            this.cardNo = cardNo;
        }

        public String getRefTransactionNo() {
            return refTransactionNo;
        }

        public void setRefTransactionNo(String refTransactionNo) {
            this.refTransactionNo = refTransactionNo;
        }

        public String getTransactionNo() {
            return transactionNo;
        }

        public void setTransactionNo(String transactionNo) {
            this.transactionNo = transactionNo;
        }

        public String getPosTerminalNo() {
            return posTerminalNo;
        }

        public void setPosTerminalNo(String posTerminalNo) {
            this.posTerminalNo = posTerminalNo;
        }

        public LocalDateTime getTransactionTime() {
            return transactionTime;
        }

        public void setTransactionTime(LocalDateTime transactionTime) {
            this.transactionTime = transactionTime;
        }

        public DateTime getBookDate() {
            return bookDate;
        }

        public void setBookDate(DateTime bookDate) {
            this.bookDate = bookDate;
        }

        public String getStore() {
            return store;
        }

        public void setStore(String store) {
            this.store = store;
        }

        public String getCashier() {
            return cashier;
        }

        public void setCashier(String cashier) {
            this.cashier = cashier;
        }

        public String getProductCode() {
            return productCode;
        }

        public void setProductCode(String productCode) {
            this.productCode = productCode;
        }

        public Double getCardFee() {
            return cardFee;
        }

        public void setCardFee(Double cardFee) {
            this.cardFee = cardFee;
        }

        public String getSalesType() {
            return salesType;
        }

        public void setSalesType(String salesType) {
            this.salesType = salesType;
        }

        public String getTransactionType() {
            return transactionType;
        }

        public void setTransactionType(String transactionType) {
            this.transactionType = transactionType;
        }

        public double getDiscountAmount() {
            return discountAmount;
        }

        public void setDiscountAmount(double discountAmount) {
            this.discountAmount = discountAmount;
        }

        public double getBalanceBefore() {
            return balanceBefore;
        }

        public void setBalanceBefore(double balanceBefore) {
            this.balanceBefore = balanceBefore;
        }

        public double getTransactionAmount() {
            return transactionAmount;
        }

        public void setTransactionAmount(double transactionAmount) {
            this.transactionAmount = transactionAmount;
        }

        public double getBalanceAfter() {
            return balanceAfter;
        }

        public void setBalanceAfter(double balanceAfter) {
            this.balanceAfter = balanceAfter;
        }

        public boolean isSettlement() {
            return settlement;
        }

        public void setSettlement(boolean settlement) {
            this.settlement = settlement;
        }

        public String getPsStoreCode() {
            return psStoreCode;
        }

        public void setPsStoreCode(String psStoreCode) {
            this.psStoreCode = psStoreCode;
        }
    }
    //</editor-fold>

}

    //<editor-fold defaultstate="collapsed" desc="FORMULA">
//cardCost or printFee
//salesAmount    = cardAddValue - discountAmount + cardCost
//               = 10,000,000,000 - 450,000,000 + 500,000 
//cardAddValue   = cardQty * faceValue
//               = 20,000 * 500,000
//               = 10,000,000,000
//discountAmount = cardAddValue * discountRate / 100
//               = 10,000,000,000 * 4.5 / 100
//               = 450,000,000
//</editor-fold>
