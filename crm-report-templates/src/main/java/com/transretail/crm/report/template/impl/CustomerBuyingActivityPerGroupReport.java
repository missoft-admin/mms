package com.transretail.crm.report.template.impl;

import com.google.common.base.Objects;
import com.google.common.collect.*;
import com.mysema.query.jpa.impl.JPAQuery;
import com.mysema.query.types.ConstantImpl;
import com.mysema.query.types.Expression;
import com.mysema.query.types.expr.DateExpression;
import com.mysema.query.types.template.DateTemplate;
import com.transretail.crm.common.reporting.jasper.ClasspathFileResolver;
import com.transretail.crm.common.reporting.jasper.DefaultJRProcessor;
import com.transretail.crm.common.reporting.jasper.JRProcessor;
import com.transretail.crm.core.entity.*;
import com.transretail.crm.core.entity.enums.ReportType;
import com.transretail.crm.core.entity.lookup.QLookupDetail;
import com.transretail.crm.core.entity.lookup.QProduct;
import com.transretail.crm.core.entity.lookup.Store;
import com.transretail.crm.core.security.util.UserUtil;
import com.transretail.crm.core.service.StoreService;
import com.transretail.crm.report.template.*;
import com.transretail.crm.report.template.impl.CustomerBuyingActivityPerGroupReport.ReportBean.Pair;
import com.transretail.crm.report.template.impl.CustomerBuyingActivityPerGroupReport.ReportBean.Stat;
import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.util.*;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import org.apache.commons.lang3.StringUtils;
import org.hibernate.Query;
import org.hibernate.Session;
import org.joda.time.LocalDate;
import org.joda.time.LocalDateTime;
import org.joda.time.LocalTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Service;

/**
 *
 * @author Monte Cillo Co (mco@exist.com)
 */
@Service("customerBuyingActivityPerGroupReport")
public class CustomerBuyingActivityPerGroupReport implements ReportTemplate, NoDataFoundSupport {

    private static final Logger logger = LoggerFactory.getLogger(CustomerBuyingActivityPerGroupReport.class);

    public static final String PERMISSION = "CUSTOMER_BUYING_ACTIVITY_PER_CUSTOMER_GROUP_PERMISSION";
    public static final String TARGET_DATE = "targetDate";
    public static final String STORE_PARAM = "store";
    static final String EXCEL_PERCENT_FORMAT = "#,##0.00##%;(#,##0.00##%)";
    static final DecimalFormat PERCENT_FORMATTER = new DecimalFormat(EXCEL_PERCENT_FORMAT);

    static {
        PERCENT_FORMATTER.setParseBigDecimal(true);
    }

    @Autowired
    private MessageSource messageSource;
    @Autowired
    private StoreService storeService;
    @PersistenceContext
    private EntityManager em;

    @Override
    public String getName() {
        return "customer-buying-activity-per-customer-group";
    }

    @Override
    public String getDescription() {
        return messageSource.getMessage("customer.buying.activity.per.group.report.title",
                null, LocaleContextHolder.getLocale());
    }

    @Override
    public Set<FilterField> getFilters() {
        Set<FilterField> filters = Sets.newLinkedHashSetWithExpectedSize(2);
        final Locale locale = LocaleContextHolder.getLocale();
        filters.add(FilterField.createDateField(TARGET_DATE,
                messageSource.getMessage("customer.buying.activity.per.group.param.from", null, locale),
                ReportDateFormat.DATE_FORMAT, true));
        filters.add(FilterField.createDropdownField(STORE_PARAM,
                messageSource.getMessage("customer.buying.activity.per.group.param.store", null, locale),
                storeService.getAllStoresHashMap()));

        return filters;
    }

    @Override
    public boolean canView(Set<String> permissions) {
        return permissions.contains(PERMISSION);
    }

    @Override
    public JRProcessor createJrProcessor(Map<String, String> parameters) {
        Map<String, Object> reportParams = Maps.<String, Object>newHashMap();
        DefaultJRProcessor jrProcessor = new DefaultJRProcessor("reports/customer-buying-activity-per-report-group.jrxml");
        jrProcessor.setFileResolver(new ClasspathFileResolver("reports"));
        String tmp = parameters.get(TARGET_DATE);
        final LocalDate targetDate = ReportDateFormat.parseDate(tmp);
        jrProcessor.addParameter("TARGET_DATE", targetDate);

        String storeCode = parameters.get(STORE_PARAM);
        if (StringUtils.isNotBlank(storeCode)) {
            Store store = storeService.getStoreByCode(storeCode);
            jrProcessor.addParameter("STORE_NAME", store.getName());
        }

        jrProcessor.addParameter("SUB_DATASOURCE", new JRBeanCollectionDataSource(
                getData(targetDate, storeCode, reportParams)));
        jrProcessor.addParameters(reportParams);

        jrProcessor.addParameter("PERCENT_FORMATTER", PERCENT_FORMATTER);
        jrProcessor.addParameter("EXCEL_PERCENT_FORMAT", EXCEL_PERCENT_FORMAT);
        jrProcessor.addParameter("PRINTED_BY", UserUtil.getCurrentUser().getUsername());

        return jrProcessor;
    }

    @Override
    public boolean isEmpty(Map<String, String> filter) {
        QDepartment qdep = QDepartment.department;
        QDivision qdiv = QDivision.division;
        QLookupDetail qld = QLookupDetail.lookupDetail;
        QMemberModel qm = QMemberModel.memberModel;
        QPosTransaction qptx = QPosTransaction.posTransaction;
        QPosTxItem qtxi = QPosTxItem.posTxItem;
        QProduct qp = QProduct.product;

        LocalDate targetDate = ReportDateFormat.parseDate(filter.get(TARGET_DATE));
        LocalDateTime targetTransactionDate = targetDate.toLocalDateTime(LocalTime.MIDNIGHT);
        boolean empty = new JPAQuery(em).from(qptx, qp)
                .join(qptx.orderItems, qtxi)
                .join(qp.department, qdep)
                .join(qdep.division, qdiv)
                .where(qtxi.productId.eq(qp.id)
                        .and(qptx.transactionDate.year().eq(targetDate.getYear())
                                .or(qptx.transactionDate.month().eq(targetDate.getMonthOfYear()))
                                .or(truncate(qptx.transactionDate, "DD").eq(targetTransactionDate))))
                .notExists();

        return empty;
    }

    // Good to have this in a util class	
    protected DateExpression<LocalDateTime> truncate(Expression<?> expression, String pattern) {
        return DateTemplate.create(LocalDateTime.class, "trunc({0}, '{1s}')", expression, ConstantImpl.create(pattern));
    }

    @Override
    public ReportType[] getReportTypes() {
        return ReportType.values();
    }

    private List<ReportBean> getData(LocalDate targetDate, String storeCode, Map<String, Object> reportParams) {
        QLookupDetail qld = QLookupDetail.lookupDetail;
        QMemberModel qm = QMemberModel.memberModel;
        QPosTransaction qptx = QPosTransaction.posTransaction;
        QPosTxItem qtxi = QPosTxItem.posTxItem;
        QProduct qp = QProduct.product;
        QDepartment qdep = QDepartment.department;
        QDivision qdiv = QDivision.division;
        // TODO: find a better way and translate to jpql if possible  @mco

        String sql = "select prev.customer_group_code, prev.customer_group, prev.division,\n"
                + "  prev.daily_customer_count as daily_customer_count_l,\n"
                + "  cur.daily_customer_count as daily_customer_count_r,\n"
                + "  prev.daily_sales_amount as daily_sales_amount_l,\n"
                + "  cur.daily_sales_amount as daily_sales_amount_r,\n"
                + "  prev.monthly_customer_count as monthly_customer_count_l, \n"
                + "  cur.monthly_customer_count as monthly_customer_count_r,\n"
                + "  prev.monthly_sales_amount as monthly_sales_amount_l,\n"
                + "  cur.monthly_sales_amount as monthly_sales_amount_r, \n"
                + "  prev.yearly_customer_count as yearly_customer_count_l,\n"
                + "  cur.yearly_customer_count as yearly_customer_count_r,\n"
                + "  prev.yearly_sales_amount as yearly_sales_amount_l,\n"
                + "  cur.yearly_sales_amount as yearly_sales_amount_r\n"
                + "from \n"
                + "  (select yearly.customer_group_code as customer_group_code, "
                + "   yearly.customer_group as customer_group, yearly.division as division,\n"
                + "    daily.customer_count as daily_customer_count, \n"
                + "    daily.sales_amount as daily_sales_amount,\n"
                + "    monthly.customer_count as monthly_customer_count, \n"
                + "    monthly.sales_amount as monthly_sales_amount, \n"
                + "    yearly.customer_count as yearly_customer_count, \n"
                + "    yearly.sales_amount as yearly_sales_amount\n"
                + "  from \n"
                + "    (select cust_div.customer_group_code, cust_div.customer_group, cust_div.division, l.customer_count, l.sales_amount from\n"
                + "      (select\n"
                + "        grp.code as customer_group_code, grp.description as customer_group, div.description as division \n"
                + "      from crm_ref_lookup_dtl grp, division div where grp.hdr='CMP001') cust_div\n"
                + "    left join\n"
                + "      (select grp.code as customer_group_code,grp.description as customer_group, count(m.account_id) as customer_count,\n"
                + "        dv.description as division, sum(tx.total_amount) - sum(tx.total_discount) - sum(tx.voided_discount) as sales_amount\n"
                + "      from crm_ref_lookup_dtl grp\n"
                + "        left join crm_member m        on grp.code = m.customer_group\n"
                + "        left join pos_transaction tx  on m.account_id = tx.customer_id\n"
                + "        left join pos_tx_item ti      on tx.id = ti.pos_txn_id\n"
                + "        left join product p           on ti.product_id = p.plu_id\n"
                + "        left join department dp       on p.department_code = dp.code\n"
                + "        left join division dv         on dp.division_id = dv.code\n"
                + "      where grp.hdr='CMP001' and tx.type in ('SALE', 'REFUND', 'RETURN', 'VOID')\n"
                + "        and to_char(tx.transaction_date, 'DD-MON-YYYY') = :prevDate \n"
                + (StringUtils.isNotBlank(storeCode) ? " and tx.store_id = :storeId \n" : "")
                + "      group by grp.code, grp.description, dv.description\n"
                + "      order by grp.code, grp.description, dv.description) l\n"
                + "        on l.customer_group = cust_div.customer_group and l.division = cust_div.division\n"
                + "    order by cust_div.division) daily,  \n"
                + "    (select cust_div.customer_group_code, cust_div.customer_group, cust_div.division, l.customer_count, l.sales_amount from\n"
                + "      (select\n"
                + "        grp.code as customer_group_code, grp.description as customer_group, div.description as division \n"
                + "      from crm_ref_lookup_dtl grp, division div where grp.hdr='CMP001') cust_div\n"
                + "    left join\n"
                + "      (select grp.code as customer_group_code, grp.description as customer_group, count(m.account_id) as customer_count,\n"
                + "        dv.description as division, sum(tx.total_amount) - sum(tx.total_discount) - sum(tx.voided_discount) as sales_amount\n"
                + "      from crm_ref_lookup_dtl grp\n"
                + "        left join crm_member m        on grp.code = m.customer_group\n"
                + "        left join pos_transaction tx  on m.account_id = tx.customer_id\n"
                + "        left join pos_tx_item ti      on tx.id = ti.pos_txn_id\n"
                + "        left join product p           on ti.product_id = p.plu_id\n"
                + "        left join department dp       on p.department_code = dp.code\n"
                + "        left join division dv         on dp.division_id = dv.code\n"
                + "      where grp.hdr='CMP001' and tx.type in ('SALE', 'REFUND', 'RETURN', 'VOID')\n"
                + "        and (tx.transaction_date >= :prevBasedMonthYear \n"
                + "        and tx.transaction_date <= :prevMonthYear) \n"
                + (StringUtils.isNotBlank(storeCode) ? " and tx.store_id = :storeId \n" : "")
                + "      group by grp.code, grp.description, dv.description\n"
                + "      order by grp.code, grp.description, dv.description) l\n"
                + "        on l.customer_group = cust_div.customer_group and l.division = cust_div.division\n"
                + "    order by cust_div.division) monthly, \n"
                + "      (select cust_div.customer_group_code, cust_div.customer_group, cust_div.division, l.customer_count, l.sales_amount from\n"
                + "      (select\n"
                + "        grp.code as customer_group_code, grp.description as customer_group, div.description as division \n"
                + "      from crm_ref_lookup_dtl grp, division div where grp.hdr='CMP001') cust_div\n"
                + "    left join\n"
                + "      (select grp.code as customer_group_code, grp.description as customer_group, count(m.account_id) as customer_count,\n"
                + "        dv.description as division, sum(tx.total_amount) - sum(tx.total_discount) - sum(tx.voided_discount) as sales_amount\n"
                + "      from crm_ref_lookup_dtl grp\n"
                + "        left join crm_member m        on grp.code = m.customer_group\n"
                + "        left join pos_transaction tx  on m.account_id = tx.customer_id\n"
                + "        left join pos_tx_item ti      on tx.id = ti.pos_txn_id\n"
                + "        left join product p           on ti.product_id = p.plu_id\n"
                + "        left join department dp       on p.department_code = dp.code\n"
                + "        left join division dv         on dp.division_id = dv.code\n"
                + "      where grp.hdr='CMP001' and tx.type in ('SALE', 'REFUND', 'RETURN', 'VOID')\n"
                + "        and tx.transaction_date >= :prevBasedYear \n"
                + "        and tx.transaction_date <= :prevYear \n"
                + (StringUtils.isNotBlank(storeCode) ? " and tx.store_id = :storeId \n" : "")
                + "      group by grp.code, grp.description, dv.description\n"
                + "      order by grp.code, grp.description, dv.description) l\n"
                + "        on l.customer_group = cust_div.customer_group and l.division = cust_div.division\n"
                + "    order by cust_div.division) yearly\n"
                + "    where daily.customer_group = monthly.customer_group \n"
                + "      and daily.division = monthly.division\n"
                + "      and yearly.customer_group = monthly.customer_group \n"
                + "      and yearly.division = monthly.division) prev\n"
                + "      \n"
                + "      join\n"
                + "      \n"
                + "      (select yearly.customer_group_code, yearly.customer_group, yearly.division,\n"
                + "    daily.customer_count as daily_customer_count, \n"
                + "    daily.sales_amount as daily_sales_amount,\n"
                + "    monthly.customer_count as monthly_customer_count, \n"
                + "    monthly.sales_amount as monthly_sales_amount, \n"
                + "    yearly.customer_count as yearly_customer_count, \n"
                + "    yearly.sales_amount as yearly_sales_amount\n"
                + "  from \n"
                + "    (select cust_div.customer_group_code, cust_div.customer_group, cust_div.division, l.customer_count, l.sales_amount from\n"
                + "      (select\n"
                + "        grp.code as customer_group_code, grp.description as customer_group, div.description as division \n"
                + "      from crm_ref_lookup_dtl grp, division div where grp.hdr='CMP001') cust_div\n"
                + "    left join\n"
                + "      (select grp.code as customer_group_code, grp.description as customer_group, count(m.account_id) as customer_count,\n"
                + "        dv.description as division, sum(tx.total_amount) - sum(tx.total_discount) - sum(tx.voided_discount) as sales_amount\n"
                + "      from crm_ref_lookup_dtl grp\n"
                + "        left join crm_member m        on grp.code = m.customer_group\n"
                + "        left join pos_transaction tx  on m.account_id = tx.customer_id\n"
                + "        left join pos_tx_item ti      on tx.id = ti.pos_txn_id\n"
                + "        left join product p           on ti.product_id = p.plu_id\n"
                + "        left join department dp       on p.department_code = dp.code\n"
                + "        left join division dv         on dp.division_id = dv.code\n"
                + "      where grp.hdr='CMP001' and tx.type in ('SALE', 'REFUND', 'RETURN', 'VOID')\n"
                + "        and to_char(tx.transaction_date, 'DD-MON-YYYY') = :curDate \n"
                + (StringUtils.isNotBlank(storeCode) ? " and tx.store_id = :storeId \n" : "")
                + "      group by grp.code, grp.description, dv.description\n"
                + "      order by grp.code, grp.description, dv.description) l\n"
                + "        on l.customer_group = cust_div.customer_group and l.division = cust_div.division\n"
                + "    order by cust_div.division) daily,  \n"
                + "    (select cust_div.customer_group_code, cust_div.customer_group, cust_div.division, l.customer_count, l.sales_amount from\n"
                + "      (select\n"
                + "        grp.code as customer_group_code, grp.description as customer_group, div.description as division \n"
                + "      from crm_ref_lookup_dtl grp, division div where grp.hdr='CMP001') cust_div\n"
                + "    left join\n"
                + "      (select grp.code as customer_group_code, grp.description as customer_group, count(m.account_id) as customer_count,\n"
                + "        dv.description as division, sum(tx.total_amount) - sum(tx.total_discount) - sum(tx.voided_discount) as sales_amount\n"
                + "      from crm_ref_lookup_dtl grp\n"
                + "        left join crm_member m        on grp.code = m.customer_group\n"
                + "        left join pos_transaction tx  on m.account_id = tx.customer_id\n"
                + "        left join pos_tx_item ti      on tx.id = ti.pos_txn_id\n"
                + "        left join product p           on ti.product_id = p.plu_id\n"
                + "        left join department dp       on p.department_code = dp.code\n"
                + "        left join division dv         on dp.division_id = dv.code\n"
                + "      where grp.hdr='CMP001' and tx.type in ('SALE', 'REFUND', 'RETURN', 'VOID')\n"
                + "        and tx.transaction_date  >= :targetBasedMonthYear \n"
                + "        and tx.transaction_date  <= :targetMonthYear \n"
                + (StringUtils.isNotBlank(storeCode) ? " and tx.store_id = :storeId \n" : "")
                + "      group by grp.code, grp.description, dv.description\n"
                + "      order by grp.code, grp.description, dv.description) l\n"
                + "        on l.customer_group = cust_div.customer_group and l.division = cust_div.division\n"
                + "    order by cust_div.division) monthly, \n"
                + "      (select cust_div.customer_group_code, cust_div.customer_group, cust_div.division, l.customer_count, l.sales_amount from\n"
                + "      (select\n"
                + "        grp.code as customer_group_code, grp.description as customer_group, div.description as division \n"
                + "      from crm_ref_lookup_dtl grp, division div where grp.hdr='CMP001') cust_div\n"
                + "    left join\n"
                + "      (select grp.code as customer_group_code, grp.description as customer_group, count(m.account_id) as customer_count,\n"
                + "        dv.description as division, sum(tx.total_amount) - sum(tx.total_discount) - sum(tx.voided_discount) as sales_amount\n"
                + "      from crm_ref_lookup_dtl grp\n"
                + "        left join crm_member m        on grp.code = m.customer_group\n"
                + "        left join pos_transaction tx  on m.account_id = tx.customer_id\n"
                + "        left join pos_tx_item ti      on tx.id = ti.pos_txn_id\n"
                + "        left join product p           on ti.product_id = p.plu_id\n"
                + "        left join department dp       on p.department_code = dp.code\n"
                + "        left join division dv         on dp.division_id = dv.code\n"
                + "      where grp.hdr='CMP001' and tx.type in ('SALE', 'REFUND', 'RETURN', 'VOID')\n"
                + "        and tx.transaction_date >= :targetBasedYear \n"
                + "        and tx.transaction_date <= :targetYear \n"
                + (StringUtils.isNotBlank(storeCode) ? " and tx.store_id = :storeId \n" : "")
                + "      group by grp.code, grp.description, dv.description\n"
                + "      order by grp.code, grp.description, dv.description) l\n"
                + "        on l.customer_group = cust_div.customer_group and l.division = cust_div.division\n"
                + "    order by cust_div.division) yearly\n"
                + "    where daily.customer_group = monthly.customer_group \n"
                + "      and daily.division = monthly.division\n"
                + "      and yearly.customer_group = monthly.customer_group \n"
                + "      and yearly.division = monthly.division) cur\n"
                + "on cur.customer_group = prev.customer_group and cur.division = prev.division\n"
                + " order by cur.customer_group_code, cur.customer_group, cur.division";

        Session session = em.unwrap(Session.class);
        Query query = session.createSQLQuery(sql)
                .setParameter("prevDate", targetDate.minusYears(1).toString("dd-MMM-yyyy").toUpperCase())
                .setParameter("prevBasedMonthYear", targetDate.minusYears(1).withDayOfMonth(1).withMonthOfYear(1).toDate())
                .setParameter("prevMonthYear", targetDate.minusYears(1).toDate())
                .setParameter("prevBasedYear", targetDate.minusYears(1).withDayOfMonth(1).withMonthOfYear(1).toDate())
                .setParameter("prevYear", targetDate.minusYears(1).toDate())
                .setParameter("curDate", targetDate.toString("dd-MMM-yyyy").toUpperCase())
                .setParameter("targetBasedMonthYear", targetDate.withDayOfMonth(1).withMonthOfYear(1).toDate())
                .setParameter("targetMonthYear", targetDate.toDate())
                .setParameter("targetBasedYear", targetDate.withDayOfMonth(1).withMonthOfYear(1).toDate())
                .setParameter("targetYear", targetDate.toDate());

        if (StringUtils.isNotBlank(storeCode)) {
            Store store = storeService.getStoreByCode(storeCode);
            query.setParameter("storeId", store.getId());
        }

        List result = query.list();
        logger.info(query.getQueryString());
        List<ReportBean> ds = Lists.newArrayList();

        Table<String, String, ReportBean.Stat> table = HashBasedTable.create();
        for (Object i : result) {
            Object[] row = (Object[]) i;
            ReportBean b = new ReportBean();
            b.customerGroupCode(String.valueOf(row[0]))
                    .customerGroup(String.valueOf(row[1]))
                    .division(String.valueOf(row[2]))
                    .daily(new ReportBean.Stat()
                            .customerCount(new ReportBean.Pair<Long>(
                                            ((BigDecimal) Objects.firstNonNull(row[3], BigDecimal.ZERO)).longValue(),
                                            ((BigDecimal) Objects.firstNonNull(row[4], BigDecimal.ZERO)).longValue()))
                            .sales(new ReportBean.Pair<Double>(
                                            ((BigDecimal) Objects.firstNonNull(row[5], BigDecimal.ZERO)).doubleValue(),
                                            ((BigDecimal) Objects.firstNonNull(row[6], BigDecimal.ZERO)).doubleValue())))
                    .monthly(new ReportBean.Stat()
                            .customerCount(new ReportBean.Pair<Long>(
                                            ((BigDecimal) Objects.firstNonNull(row[7], BigDecimal.ZERO)).longValue(),
                                            ((BigDecimal) Objects.firstNonNull(row[8], BigDecimal.ZERO)).longValue()))
                            .sales(new ReportBean.Pair<Double>(((BigDecimal) Objects.firstNonNull(row[9], BigDecimal.ZERO)).doubleValue(),
                                            ((BigDecimal) Objects.firstNonNull(row[10], BigDecimal.ZERO)).doubleValue())))
                    .yearly(new ReportBean.Stat()
                            .customerCount(new ReportBean.Pair<Long>(
                                            ((BigDecimal) Objects.firstNonNull(row[11], BigDecimal.ZERO)).longValue(),
                                            ((BigDecimal) Objects.firstNonNull(row[12], BigDecimal.ZERO)).longValue()))
                            .sales(new ReportBean.Pair<Double>(((BigDecimal) Objects.firstNonNull(row[13], BigDecimal.ZERO)).doubleValue(),
                                            ((BigDecimal) Objects.firstNonNull(row[14], BigDecimal.ZERO)).doubleValue())));

            final String division = b.getDivision();
            final Stat monthly = b.getMonthly();
            final Stat yearly = b.getYearly();
            final Stat daily = b.getDaily();
            if (table.containsRow(division)) {

                Stat daily_ = table.get(division, "daily");
                updateStat(daily_, daily);

                Stat monthly_ = table.get(division, "monthly");
                updateStat(monthly_, monthly);

                Stat yearly_ = table.get(division, "yearly");
                updateStat(yearly_, yearly);
            } else {
                // create new Stat for every entry to avoid reference = immutable Stat
                table.put(division, "daily", new Stat()
                        .customerCount(daily.getCustomerCount())
                        .sales(daily.getSales()));
                table.put(division, "monthly", new Stat()
                        .customerCount(monthly.getCustomerCount())
                        .sales(monthly.getSales()));
                table.put(division, "yearly", new Stat()
                        .customerCount(yearly.getCustomerCount())
                        .sales(yearly.getSales()));
            }

            ds.add(b);
        }

        // initialize stats
        Stat dailyGrandTotal = new Stat()
                .customerCount(new Pair<Long>(0l, 0l))
                .sales(new Pair<Double>(0.0, 0.0));
        Stat monthlyGrandTotal = new Stat()
                .customerCount(new Pair<Long>(0l, 0l))
                .sales(new Pair<Double>(0.0, 0.0));
        Stat yearlyGrandTotal = new Stat()
                .customerCount(new Pair<Long>(0l, 0l))
                .sales(new Pair<Double>(0.0, 0.0));

        List<ReportBean> totals = Lists.newArrayList();
        for (String division : table.rowKeySet()) {
            Stat daily = table.get(division, "daily");
            Stat monthly = table.get(division, "monthly");
            Stat yearly = table.get(division, "yearly");
            totals.add(new ReportBean()
                    .customerGroup("TOTAL") // the 'TOTAL' placeholder
                    .division(division)
                    .daily(daily)
                    .monthly(monthly)
                    .yearly(yearly));
            updateStat(dailyGrandTotal, daily);
            updateStat(monthlyGrandTotal, monthly);
            updateStat(yearlyGrandTotal, yearly);
        }

        // Sort Division Alphabetically
        Collections.sort(totals, new Comparator<ReportBean>() {

            @Override
            public int compare(ReportBean o1, ReportBean o2) {
                return o1.getDivision().compareTo(o2.getDivision());
            }
        });

        ds.addAll(totals);

        // pass as report parant the grand total
        // jasper's table component has no aggregation unlike crosstab
        reportParams.put("DAILY_STAT_GRAND_TOTAL", dailyGrandTotal);
        reportParams.put("MONTHLY_STAT_GRAND_TOTAL", monthlyGrandTotal);
        reportParams.put("YEARLY_STAT_GRAND_TOTAL", yearlyGrandTotal);

        return ds;
    }

    private void updateStat(Stat container, Stat temp) {
        final Pair<Long> customerCount_ = container.getCustomerCount();
        final Pair<Long> customerCount = temp.getCustomerCount();
        container.setCustomerCount(new Pair<Long>(
                customerCount_.getLeft() + customerCount.getLeft(),
                customerCount_.getRight() + customerCount.getRight()));

        final Pair<Double> sales_ = container.getSales();
        final Pair<Double> sales = temp.getSales();
        container.setSales(new Pair<Double>(
                sales_.getLeft() + sales.getLeft(),
                sales_.getRight() + sales.getRight()));
    }

    public static class ReportBean {

        private String customerGroupCode;
        private String customerGroup;
        private String division;
        private Stat daily;
        private Stat monthly;
        private Stat yearly;

        public ReportBean customerGroupCode(String customerGroupCode) {
            this.customerGroupCode = customerGroupCode;
            return this;
        }

        public ReportBean customerGroup(String customerGroup) {
            this.customerGroup = customerGroup;
            return this;
        }

        public ReportBean division(String division) {
            this.division = division;
            return this;
        }

        public ReportBean daily(Stat daily) {
            this.daily = daily;
            return this;
        }

        public ReportBean monthly(Stat monthly) {
            this.monthly = monthly;
            return this;
        }

        public ReportBean yearly(Stat yearly) {
            this.yearly = yearly;
            return this;
        }

        public String getCustomerGroup() {
            return customerGroup;
        }

        public String getDivision() {
            return division;
        }

        public void setDivision(String division) {
            this.division = division;
        }

        public Stat getDaily() {
            return daily;
        }

        public Stat getMonthly() {
            return monthly;
        }

        public Stat getYearly() {
            return yearly;
        }

        public String getCustomerGroupCode() {
            return customerGroupCode;
        }

        public void setCustomerGroupCode(String customerGroupCode) {
            this.customerGroupCode = customerGroupCode;
        }

        public static class Stat {

            private Pair<Long> customerCount;
            private Pair<Double> sales;

            public Stat customerCount(Pair<Long> customerCount) {
                this.customerCount = customerCount;
                return this;
            }

            public Stat sales(Pair<Double> sales) {
                this.sales = sales;
                return this;
            }

            public Pair<Long> getCustomerCount() {
                return customerCount;
            }

            public void setCustomerCount(Pair<Long> customerCount) {
                this.customerCount = customerCount;
            }

            public Pair<Double> getSales() {
                return sales;
            }

            public void setSales(Pair<Double> sales) {
                this.sales = sales;
            }
        }

        public static class Pair<N> {

            private N left;
            private N right;

            public Pair(N left, N right) {
                this.left = left;
                this.right = right;
            }

            public N getLeft() {
                return left;
            }

            public void setLeft(N left) {
                this.left = left;
            }

            public N getRight() {
                return right;
            }

            public void setRight(N right) {
                this.right = right;
            }

        }
    }
}
