package com.transretail.crm.core.util.generator.service.impl;


import com.transretail.crm.core.util.generator.IdGeneratorService;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Map;
import java.util.Random;


@Service
@Transactional
public class PromotionIdGeneratorService implements IdGeneratorService {

    @Override
    public String generateId() {

        //TODO need to get format of account id. For now let's have a 10 digit numeric number

        int defaultLength = 10;

        Random r = new Random();

        String number = "";

        int counter = 0;

        while (counter++ < defaultLength) {
            number += r.nextInt(9);

        }


        return number;
    }

    @Override
    public String generateId(int length, boolean alphanumeric) {
        return null;
    }

    @Override
    public String generateId(Map props) {
        return null;
    }


    @Override
	public String generateId(String prepend) {
		return null;
	}
}
