package com.transretail.crm.reconciliation.web;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseStatus;

import com.transretail.crm.Constants;
import com.transretail.crm.reconciliation.service.StoreInfoService;

/**
 * @author Allan G. Ramirez (agramirez@exist.com)
 */
@Controller
public class StoreInfoRestController {
    private static final Logger _LOG = LoggerFactory.getLogger(StoreInfoRestController.class);

    @Autowired
    private StoreInfoService storeInfoService;

    @ResponseStatus(HttpStatus.OK)
    @RequestMapping(value = "/update/posintegdb/used/to/{posIntegDbUsed}", method = RequestMethod.GET)
    public void updatePosIntegDbUsed(@PathVariable("posIntegDbUsed") String posIntegDbUsed) throws Exception {
        posIntegDbUsed = posIntegDbUsed.toUpperCase();
        if (!Constants.DBEE.toString().equals(posIntegDbUsed) && !Constants.TTSERVER.toString().equals(posIntegDbUsed)
            && !Constants.LOCALDB.toString().equals(posIntegDbUsed)) {
            throw new IllegalArgumentException("Valid values of posIntegDbUsed are DBEE, TTSERVER, LOCALDB");
        }

        storeInfoService.updatePosIntegDbUsed(posIntegDbUsed);
    }

    @ResponseStatus(HttpStatus.OK)
    @RequestMapping(value = "/update/mainddb/used/to/{mainDbUsed}", method = RequestMethod.GET)
    public void updateMainDbUsed(@PathVariable("mainDbUsed") String mainDbUsed) throws Exception {
        mainDbUsed = mainDbUsed.toUpperCase();
        if (!Constants.DBEE.toString().equals(mainDbUsed) && !Constants.TTSERVER.toString().equals(mainDbUsed)
            && !Constants.LOCALDB.toString().equals(mainDbUsed)) {
            throw new IllegalArgumentException("Valid values of main db used are DBEE, TTSERVER, LOCALDB");
        }

        storeInfoService.updateMainDbUsed(mainDbUsed);
    }
}
