package com.transretail.crm.core.entity;

import com.transretail.crm.core.entity.enums.StampTxnType;
import com.transretail.crm.core.entity.support.CustomAuditableEntity;
import org.hibernate.annotations.Type;
import org.hibernate.envers.Audited;
import org.hibernate.envers.NotAudited;
import org.joda.time.DateTime;
import org.joda.time.LocalDate;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

/**
 * @author Mike de Guzman
 */
@Table(name = "CRM_STAMP")
@Entity
@Audited
public class StampTxnModel extends CustomAuditableEntity<Long> implements Serializable {

    private static final long serialVersionUID = 1L;

    @ManyToOne
    @JoinColumn(name = "TXN_HDR_ID")
    @NotAudited
    private StampTxnHeader transactionHeader;

    @ManyToOne
    @JoinColumn(name = "PROMOTION_ID")
    private Promotion stampPromotion;

    @Column(name = "INCLUSIVE_DATE_FROM")
    @Temporal(TemporalType.TIMESTAMP)
    private Date incDateFrom;

    @Column(name = "INCLUSIVE_DATE_TO")
    @Temporal(TemporalType.TIMESTAMP)
    private Date incDateTo;

    @Column(name = "TXN_TYPE")
    @Enumerated(EnumType.STRING)
    private StampTxnType transactionType;

    @Column(name = "VALIDATED_BY")
    private String validatedBy;

    @Column(name = "VALIDATED_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date validatedDate;

    @Column(name = "EXPIRY_DATE")
    @Temporal(TemporalType.DATE)
    @Type(type = "org.jadira.usertype.dateandtime.joda.PersistentLocalDate")
    private LocalDate expiryDate;

    @Column(name = "LAST_REDEEM")
    @Temporal(TemporalType.TIMESTAMP)
    @Type(type = "org.jadira.usertype.dateandtime.joda.PersistentDateTime")
    private DateTime lastRedeemTimestamp;

    @Column(name = "VALID_PERIOD")
    private Integer validPeriod;

    public StampTxnHeader getTransactionHeader() {
        return transactionHeader;
    }

    public void setTransactionHeader(StampTxnHeader transactionHeader) {
        this.transactionHeader = transactionHeader;
    }

    public Date getIncDateFrom() {
        return incDateFrom;
    }

    public void setIncDateFrom(Date incDateFrom) {
        this.incDateFrom = incDateFrom;
    }

    public Date getIncDateTo() {
        return incDateTo;
    }

    public void setIncDateTo(Date incDateTo) {
        this.incDateTo = incDateTo;
    }

    public StampTxnType getTransactionType() {
        return transactionType;
    }

    public void setTransactionType(StampTxnType transactionType) {
        this.transactionType = transactionType;
    }

    public String getValidatedBy() {
        return validatedBy;
    }

    public void setValidatedBy(String validatedBy) {
        this.validatedBy = validatedBy;
    }

    public Date getValidatedDate() {
        return validatedDate;
    }

    public void setValidatedDate(Date validatedDate) {
        this.validatedDate = validatedDate;
    }

    public Integer getValidPeriod() {
        return validPeriod;
    }

    public void setValidPeriod(Integer validPeriod) {
        this.validPeriod = validPeriod;
    }

    public LocalDate getExpiryDate() {
        return expiryDate;
    }

    public void setExpiryDate(LocalDate expiryDate) {
        this.expiryDate = expiryDate;
    }

    public DateTime getLastRedeemTimestamp() {
        return lastRedeemTimestamp;
    }

    public void setLastRedeemTimestamp(DateTime lastRedeemTimestamp) {
        this.lastRedeemTimestamp = lastRedeemTimestamp;
    }

    public Promotion getStampPromotion() {
        return stampPromotion;
    }

    public void setStampPromotion(Promotion stampPromotion) {
        this.stampPromotion = stampPromotion;
    }
}
