package com.transretail.crm.core.service;


import com.transretail.crm.core.dto.MemberDto;
import com.transretail.crm.core.dto.MemberResultList;
import com.transretail.crm.core.dto.MemberSearchDto;
import com.transretail.crm.core.entity.enums.MemberStatus;

import java.util.Calendar;
import java.util.List;

public interface EmployeeService {

    List<MemberDto> findAllEmployees();

	List<MemberDto> findEmployeeEntries(int firstResult, int maxResults);

	long countAllEmployees();

	List<MemberDto> findByCriteria(MemberDto inModel, String employeeType);


	MemberDto findActiveEmployeeByAccountId(String accountId);


	MemberDto findEmployeeByAccountIdAndStatus(String accountId, MemberStatus status);


	MemberDto findEmployeeByAccountId(String accountId);

	MemberResultList listEmployeeDto(MemberSearchDto searchDto);

	MemberResultList listDto(MemberSearchDto searchDto, Calendar txnDateFrom, Calendar txnDateTo);

	long countAllEmployeesAsIndividuals();

    long countAllEmployeesAsProfessionals();

    long countCurrentUserStoreMembers();
}
