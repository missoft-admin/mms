package com.transretail.crm.giftcard.entity.support;

public enum ReturnStatus {
    NEW, FOR_REFUND, APPROVED, REFUND_APPROVED, REJECTED,
    FOR_APPROVAL,
    
    FOR_REPLACEMENT, REPLACED
}
