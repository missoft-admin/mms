<%@include file="../../common/taglibs.jsp" %>


<div id="content_complaintAssignForm" class="modal hide  nofly modal-dialog">

  <div class="modal-content">
  <c:url var="url_assign" value="/customer/complaint/assign" />
  <form:form id="complaintAssignForm" name="complaintAssign" modelAttribute="complaint" method="POST" action="${url_assign}" class="modal-form form-horizontal">
    <div class="modal-header">
      <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
      <h4><spring:message code="complaint_lbl_assign" /></h4>
    </div>


    <div class="modal-body">
      <div class="hide alert alert-error" id="content_error"><button type="button" class="close" onclick="$(this).parent().hide();">&times;</button><div><form:errors   path="*"/></div></div>

      <div class="main-block">
        <div id="content_dateTime" class="control-group">
          <label for="dateTime" class="control-label"><spring:message code="complaint_date"/></label>
          <div  class="controls"><div>${complaint.dateTime}</div></div>
        </div>
        <div class="control-group">
          <label for="ticketNo" class="control-label"><spring:message code="complaint_ticketno"/></label>
          <div  class="controls"><div>${complaint.ticketNo}</div></div>
        </div>
        <div id="content_status" class="control-group">
          <label for="status" class="control-label"><spring:message code="complaint_status"/></label>
          <div  class="controls"><div>${complaint.status}</div></div>
        </div>
        <div id="content_store" class="control-group">
          <label for="store" class="control-label"><spring:message code="complaint_store"/></label>
          <div  class="controls"><div>${store.code} - ${store.name}</div></div>
        </div>
        <div class="control-group">
          <label for="mobileNo" class="control-label"><spring:message code="complaint_mobile"/></label>
          <div  class="controls"><div>${complaint.mobileNo}</div></div>
        </div>
        <div class="control-group">
          <label for="email" class="control-label"><spring:message code="complaint_email"/></label>
          <div  class="controls"><div>${complaint.email}</div></div>
        </div>
        <div class="control-group">
          <label for="firstName" class="control-label"><spring:message code="complaint_fname"/></label>
          <div  class="controls"><div>${complaint.firstName}</div></div>
        </div>
        <div class="control-group">
          <label for="lastName" class="control-label"><spring:message code="complaint_lname"/></label>
          <div  class="controls"><div>${complaint.lastName}</div></div>
        </div>
        <div class="control-group">
          <label for="complaints" class="control-label"><spring:message code="complaint_complaints"/></label>
          <div  class="controls"><div>${complaint.complaints}</div></div>
        </div>
        <div class="control-group">
          <label for="genderDesc" class="control-label"><spring:message code="complaint_gender"/></label>
          <div  class="controls"><div>${complaint.genderDesc}</div></div>
        </div>
        <div class="control-group">
          <label for="priority" class="control-label"><spring:message code="complaint_priority"/></label>
          <div  class="controls"><div>${complaint.priority}</div></div>
        </div>

        <div class="control-group">
          <label for="priority" class="control-label"><spring:message code="complaint_assignedto"/></label>
          <div  class="controls">
            <form:hidden path="id"/>
            <form:select id="assignedTo" path="assignedTo" >
              <option/>
              <%-- <c:forEach var="user" items="${users}">
                <option value="${user.username}">${user.storeDesc} - ${user.username}</option>
              </c:forEach> --%>
              <form:options items="${users}" itemLabel="storeDescAndUser" itemValue="username" />
            </form:select>
          </div>
        </div>
      </div>
    </div>


    <div class="modal-footer">
      <!-- <input id="assignBtn" class="btn btn-primary" type="submit" value="<spring:message code="label_assign" />" />
      <input id="cancelBtn" class="btn" type="button" value="<spring:message code="label_cancel" />" data-dismiss="modal" /> -->
      <button type="button" id="assignBtn" class="btn btn-primary"><spring:message code="label_assign" /></button>
      <button type="button" id="cancelBtn" data-dismiss="modal" class="btn btn-default"><spring:message code="label_cancel"/></button>
    </div>
  </form:form>
  </div>


<style type="text/css"> 
<!-- 
  .modal-footer { text-align: center; } 
  .control-label { width: 100px !important; } 
  .controls { margin-left: 120px !important; } 
  .controls div { padding-top: 4px !important; }
--> 
</style>
<script type="text/javascript">
var ComplaintAssignForm = null;

$(document).ready( function() {

	  var errorContainer = $( "#content_complaintAssignForm" ).find( "#content_error" );

	  var $dialog = $( "#content_complaintAssignForm" );
	  var $form = $dialog.find( "#complaintAssignForm" );
	  var $save = $form.find( "#assignBtn" );
	  var $dispDate = $form.find( "#content_dateTime > .controls > div" );

	  initDialog();
	  initFormFields();

    function initDialog() {
		    $dialog.modal({ show : false });
		    $dialog.on( "hide", function(e) { 
		      if ( e.target === this ) { 
		        errorContainer.hide();
		        ComplaintCommon.reset( $form );
		      }
		    });
	  }

    function initFormFields() {
    	  $form.submit( function(e) {
    		    $save.prop( "disabled", true );
    		    e.preventDefault();
    		    $.post( $form.attr( "action" ), $form.serialize(), function( resp ) { 
    		    	ComplaintCommon.processResp( resp, $dialog, errorContainer, $save ); 
    		      ComplaintList.reloadTable();
    		    }, "json" );
    	  });

    	  $dispDate.html( ( $dispDate.html() )? new Date( $dispDate.html() - 0 ).customize( 0 ) : "" );
    }

	  ComplaintAssignForm = {
        show : function() { $dialog.modal( "show" ); }
	  };
});
</script>


</div>