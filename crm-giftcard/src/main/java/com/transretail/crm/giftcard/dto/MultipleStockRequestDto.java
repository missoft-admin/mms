package com.transretail.crm.giftcard.dto;

import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.joda.time.LocalDate;

import com.google.common.collect.Lists;
import com.transretail.crm.giftcard.entity.support.StockRequestStatus;

public class MultipleStockRequestDto {
	private String requestNo;
	private String sourceLocation;
	private String sourceLocationDescription;
	private LocalDate requestDate;
	private String createdBy;
	private String requestDateString;
	private Boolean forApproval;
	private String inventoryLocation;
	private List<StockRequestDto> requests = Lists.newArrayList(new StockRequestDto());
	
	public MultipleStockRequestDto() {}
	
	public MultipleStockRequestDto(String requestNo, List<StockRequestDto> requests) {
		this.requests = requests;
		this.requestNo = requestNo;
		
		if(CollectionUtils.isNotEmpty(requests)) {
			StockRequestDto request = requests.get(0);
			this.sourceLocation = request.getSourceLocation();
			this.requestDate = request.getRequestDate();
			this.createdBy = request.getCreateUser();
			
			this.forApproval = false;
			
			for(StockRequestDto req : requests) {
				if(req.getStatus() == StockRequestStatus.FORAPPROVAL) {
					forApproval = true;
					break;
				}
			}
		}
	}

	public String getRequestNo() {
		return requestNo;
	}

	public void setRequestNo(String requestNo) {
		this.requestNo = requestNo;
	}

	public String getSourceLocation() {
		return sourceLocation;
	}

	public void setSourceLocation(String sourceLocation) {
		this.sourceLocation = sourceLocation;
	}

	public String getSourceLocationDescription() {
		return sourceLocationDescription;
	}

	public void setSourceLocationDescription(String sourceLocationDescription) {
		this.sourceLocationDescription = sourceLocationDescription;
	}

	public LocalDate getRequestDate() {
		return requestDate;
	}

	public void setRequestDate(LocalDate requestDate) {
		this.requestDate = requestDate;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public List<StockRequestDto> getRequests() {
		return requests;
	}

	public void setRequests(List<StockRequestDto> requests) {
		this.requests = requests;
	}

	public String getRequestDateString() {
		return requestDate != null ? requestDate.toString("MM-dd-yyyy") : "";
	}

	public void setRequestDateString(String requestDateString) {
		this.requestDateString = requestDateString;
	}

	public Boolean getForApproval() {
		return forApproval;
	}

	public void setForApproval(Boolean forApproval) {
		this.forApproval = forApproval;
	}

	public String getInventoryLocation() {
		return inventoryLocation;
	}

	public void setInventoryLocation(String inventoryLocation) {
		this.inventoryLocation = inventoryLocation;
	}
}
