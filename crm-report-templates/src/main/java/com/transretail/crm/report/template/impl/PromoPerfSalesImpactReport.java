package com.transretail.crm.report.template.impl;

import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.joda.time.DateTime;
import org.joda.time.Days;
import org.joda.time.LocalDate;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Service;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.google.common.collect.Sets;
import com.mysema.query.Tuple;
import com.mysema.query.jpa.impl.JPAQuery;
import com.mysema.query.types.Predicate;
import com.transretail.crm.common.reporting.jasper.ClasspathFileResolver;
import com.transretail.crm.common.reporting.jasper.DefaultJRProcessor;
import com.transretail.crm.common.reporting.jasper.JRProcessor;
import com.transretail.crm.common.util.DateUtil;
import com.transretail.crm.core.entity.Promotion;
import com.transretail.crm.core.entity.QPointsTxnModel;
import com.transretail.crm.core.entity.QPosTxItem;
import com.transretail.crm.core.entity.QPromotion;
import com.transretail.crm.core.entity.QTransactionAudit;
import com.transretail.crm.core.entity.enums.ReportType;
import com.transretail.crm.core.security.util.UserUtil;
import com.transretail.crm.core.service.PromotionService;
import com.transretail.crm.report.template.FilterField;
import com.transretail.crm.report.template.ReportTemplate;

@Service("promoPerfSalesImpactReport")
public class PromoPerfSalesImpactReport implements ReportTemplate {
	protected static final String TEMPLATE_NAME = "Promotion Performance Total Sales Impact";
    protected static final String REPORT_NAME = "reports/promo_perf_sales_impact_report.jasper";
    
    protected static final String FILTER_PROMOTION = "promotionId";
    
    private static final DateTimeFormatter INPUT_DATE_PATTERN = DateTimeFormat.forPattern(DateUtil.SEARCH_DATE_FORMAT);
    
    private static final Logger LOGGER = LoggerFactory.getLogger(PromoPerfSalesImpactReport.class);
    
    @PersistenceContext
    private EntityManager em;
    @Autowired
    private MessageSource messageSource;
    
    @Autowired
    private PromotionService promotionService;
    
    
    @Override
    public ReportType[] getReportTypes() {
    	return ReportType.values();
    }
    
    @Override
    public String getName() {
    	return TEMPLATE_NAME;
    }

    @Override
    public String getDescription() {
    	return messageSource.getMessage("promo.perf.report.title", (Object[]) null, LocaleContextHolder.getLocale());
    }
    
    @Override
    public boolean canView(Set<String> permissions) {
    	return true;
    }
    
    @Override
    public Set<FilterField> getFilters() {
    	Set<FilterField> filters = Sets.newLinkedHashSet();
    	QPromotion qpromo = QPromotion.promotion;
    	Map<String, String> promotions = Maps.newLinkedHashMap();
    	promotions.putAll(new JPAQuery(em).from(qpromo)
    			.orderBy(qpromo.startDate.asc()).map(qpromo.id.stringValue(), qpromo.name));
    	filters.add(FilterField.createDropdownField(
                FILTER_PROMOTION, messageSource.getMessage("promo.perf.report.promo", (Object[]) null, LocaleContextHolder.getLocale()), promotions, true));
    	
        return filters;
    }
    
    @Override
    public JRProcessor createJrProcessor(Map<String, String> map) {
    	Long promoId = Long.valueOf(map.get(FILTER_PROMOTION));
		Promotion promotion = promotionService.getPromotion(promoId);
    	
		Date startDate = promotion.getStartDate();
		Date endDate = promotion.getEndDate();
		
		DateTime startDateTime = new DateTime(startDate);
		DateTime endDateTime = new DateTime(endDate);
		int daysPromoDuration = Days.daysBetween(startDateTime, endDateTime).getDays();
		
		QPointsTxnModel qpoints = QPointsTxnModel.pointsTxnModel;
		QPosTxItem qitem = QPosTxItem.posTxItem;
		QTransactionAudit qaudit = QTransactionAudit.transactionAudit;
		
		Predicate pointsF = qpoints.transactionDateTime.between(startDate, endDate);
		
		
		//DURING CAMPAIGGN
		Long totalSales = new JPAQuery(em).from(qpoints)
				.where(pointsF).singleResult(qpoints.transactionNo.countDistinct().coalesce(0l));
		Long plsSales = new JPAQuery(em).from(qaudit, qpoints)
				.where(qaudit.transactionNo.eq(qpoints.transactionNo).and(pointsF))
				.singleResult(qpoints.transactionNo.countDistinct().coalesce(0l));
		Long nonPlsSales = totalSales - plsSales;
		
		
		Double value = 0.0;
		Double valuePls = 0.0;
		Double valueNonPls = 0.0;
		
		Double qty = 0.0;
		Double qtyPls = 0.0;
		Double qtyNonPls = 0.0;
		
		Double shopper = 0.0;
		Double shopperPls = 0.0;
		Double shopperNonPls = 0.0;
		
		Tuple totalTuple = new JPAQuery(em).from(qpoints)
				.where(pointsF).singleResult(qpoints.transactionAmount.sum().coalesce(0.0), qpoints.memberModel.id.countDistinct().coalesce(0l));
		Tuple plsTuple = new JPAQuery(em).from(qaudit, qpoints)
				.where(qaudit.transactionNo.eq(qpoints.transactionNo).and(pointsF).and(qaudit.promotion.id.eq(promoId)))
				.singleResult(qpoints.transactionAmount.sum().coalesce(0.0), qpoints.memberModel.id.countDistinct().coalesce(0l));
		
		Double totalTxnAmount = totalTuple.get(qpoints.transactionAmount.sum().coalesce(0.0));
		Double totalTxnAmountPls = plsTuple.get(qpoints.transactionAmount.sum().coalesce(0.0));
		Double totalTxnAmountNonPls = totalTxnAmount - totalTxnAmountPls;
		
		Double totalQty = new JPAQuery(em).from(qitem)
    			.where(qitem.posTransaction.transactionDate.between(startDateTime.toLocalDateTime(), endDateTime.toLocalDateTime()))
    			.singleResult(qitem.quantity.sum().coalesce(0.0));
    	Double totalQtyPls = new JPAQuery(em).from(qitem, qaudit)
    			.where(qaudit.transactionNo.eq(qitem.posTransaction.id).and(qitem.posTransaction.transactionDate.between(startDateTime.toLocalDateTime(), endDateTime.toLocalDateTime())).and(qaudit.promotion.id.eq(promoId)))
    			.singleResult(qitem.quantity.sum().coalesce(0.0));
    	Double totalQtyNonPls = totalQty - totalQtyPls;

		if(totalSales > 0) {
	    	value = totalTxnAmount / totalSales;
	    	qty = totalQty / totalSales;
		}
		
		if(plsSales > 0) {
			valuePls = totalTxnAmountPls / plsSales;
			qtyPls = totalQtyPls / plsSales;
		}
		
		if(nonPlsSales > 0) {
			valueNonPls = totalTxnAmountNonPls / nonPlsSales;
			qtyNonPls = totalQtyNonPls / nonPlsSales;
		}
		
		Long totalMemCnt = totalTuple.get(qpoints.memberModel.id.countDistinct().coalesce(0l));
		Long plsMemCnt = plsTuple.get(qpoints.memberModel.id.countDistinct().coalesce(0l));
		Long nonPlsMemCnt = totalMemCnt - plsMemCnt;
		
		if(totalMemCnt > 0) {
			shopper = totalSales.doubleValue() / totalMemCnt.doubleValue();
		}
		if(plsMemCnt > 0) {
			shopperPls = plsSales.doubleValue() / plsMemCnt.doubleValue();
		}
		if(nonPlsMemCnt > 0) {
			shopperNonPls = nonPlsSales.doubleValue() / nonPlsMemCnt.doubleValue();
		}
		
    	
    	//PAST N DAYS
    	DateTime pastStartDateTime = startDateTime.minusDays(daysPromoDuration);
		DateTime pastEndDateTime = startDateTime;
		
		pointsF = qpoints.transactionDateTime.between(pastStartDateTime.toDate(), pastEndDateTime.toDate());
		
		Long pastTotalSales = new JPAQuery(em).from(qpoints)
				.where(pointsF).singleResult(qpoints.transactionNo.countDistinct().coalesce(0l));
		
		
		Double pastValue = 0.0;
		
		Double pastQty = 0.0;
		
		Double pastShopper = 0.0;
		
		Double pastTotalTxnAmount = new JPAQuery(em).from(qpoints)
				.where(pointsF).singleResult(qpoints.transactionAmount.sum().coalesce(0.0));
		
		Double pastTotalQty = new JPAQuery(em).from(qitem)
    			.where(qitem.posTransaction.transactionDate.between(pastStartDateTime.toLocalDateTime(), pastEndDateTime.toLocalDateTime()))
    			.singleResult(qitem.quantity.sum().coalesce(0.0));

		if(pastTotalSales > 0) {
	    	pastValue = pastTotalTxnAmount / pastTotalSales;
	    	pastQty = pastTotalQty / pastTotalSales;
		}
		
		Long pastTotalMemCnt = new JPAQuery(em).from(qpoints)
				.where(pointsF).singleResult(qpoints.memberModel.id.countDistinct().coalesce(0l));
		
		if(pastTotalMemCnt > 0) {
			pastShopper = pastTotalSales.doubleValue() / pastTotalMemCnt.doubleValue();
		}
		
		DefaultJRProcessor jrProcessor = new DefaultJRProcessor(REPORT_NAME);
        jrProcessor.setFileResolver(new ClasspathFileResolver("reports"));
        
        String duringStr = messageSource.getMessage("promo.perf.report.duringcampaign", null, LocaleContextHolder.getLocale());
        String pastStr = messageSource.getMessage("promo.perf.report.pastdays", new Object[]{daysPromoDuration}, LocaleContextHolder.getLocale());
        
        
        List<BarBean> valueBars = Lists.newArrayList();
        
        BarBean valueBar = new BarBean();
        valueBar.setDuration(pastStr);
        valueBar.setPastValue(pastValue);
        valueBars.add(valueBar);
        
        valueBar = new BarBean();
        valueBar.setDuration(duringStr);
        valueBar.setPlsValue(valuePls);
        valueBar.setNonPlsValue(valueNonPls);
        valueBars.add(valueBar);
        
        jrProcessor.addParameter("VALUE_BARS", valueBars);
        
        List<BarBean> qtyBars = Lists.newArrayList();
        
        BarBean qtyBar = new BarBean();
        qtyBar.setDuration(pastStr);
        qtyBar.setPastValue(pastQty);
        qtyBars.add(qtyBar);
        
        qtyBar = new BarBean();
        qtyBar.setDuration(duringStr);
        qtyBar.setPlsValue(qtyPls);
        qtyBar.setNonPlsValue(qtyNonPls);
        qtyBars.add(qtyBar);
        
        jrProcessor.addParameter("QTY_BARS", qtyBars);
        
        List<BarBean> shopperBars = Lists.newArrayList();
        
        BarBean shopperBar = new BarBean();
        shopperBar.setDuration(pastStr);
        shopperBar.setPastValue(pastShopper);
        shopperBars.add(shopperBar);
        
        shopperBar = new BarBean();
        shopperBar.setDuration(duringStr);
        shopperBar.setPlsValue(shopperPls);
        shopperBar.setNonPlsValue(shopperNonPls);
        shopperBars.add(shopperBar);
        
        jrProcessor.addParameter("SHOPPER_BARS", shopperBars);
        
        List<BarBean> tableBeans = Lists.newLinkedList();
        BarBean table = new BarBean();
        table.setDescription(messageSource.getMessage("promo.perf.report.txntotal", null, LocaleContextHolder.getLocale()));
        table.setPastValue(pastTotalSales.doubleValue());
        table.setDuringValue(totalSales.doubleValue());
        tableBeans.add(table);
        
        table = new BarBean();
        table.setDescription(messageSource.getMessage("promo.perf.report.avgvalue", null, LocaleContextHolder.getLocale()));
        table.setPastValue(pastValue);
        table.setDuringValue(value);
        tableBeans.add(table);
        
        table = new BarBean();
        table.setDescription(messageSource.getMessage("promo.perf.report.avgqty", null, LocaleContextHolder.getLocale()));
        table.setPastValue(pastQty);
        table.setDuringValue(qty);
        tableBeans.add(table);
        
        table = new BarBean();
        table.setDescription(messageSource.getMessage("promo.perf.report.avgshopper", null, LocaleContextHolder.getLocale()));
        table.setPastValue(pastShopper);
        table.setDuringValue(shopper);
        tableBeans.add(table);
        
        jrProcessor.addParameter("PROMO_NAME", promotion.getName());
        jrProcessor.addParameter("PAST_STR", pastStr);
        jrProcessor.addParameter("SUB_DATA_SOURCE", tableBeans);
        
        jrProcessor.addParameter("printedBy", UserUtil.getCurrentUser().getUsername());
        jrProcessor.addParameter("printedDate", new LocalDate().toString("dd-MM-yyyy"));
        
        return jrProcessor;
    }
    
    public static class BarBean {
    	private String duration;
    	private String description;
    	private Double pastValue;
    	private Double duringValue;
    	private Double plsValue;
    	private Double nonPlsValue;
    	
		public String getDescription() {
			return description;
		}
		public void setDescription(String description) {
			this.description = description;
		}
		public String getDuration() {
			return duration;
		}
		public void setDuration(String duration) {
			this.duration = duration;
		}
		public Double getPastValue() {
			return pastValue;
		}
		public void setPastValue(Double pastValue) {
			this.pastValue = pastValue;
		}
		public Double getPlsValue() {
			return plsValue;
		}
		public void setPlsValue(Double plsValue) {
			this.plsValue = plsValue;
		}
		public Double getNonPlsValue() {
			return nonPlsValue;
		}
		public void setNonPlsValue(Double nonPlsValue) {
			this.nonPlsValue = nonPlsValue;
		}
		public Double getDuringValue() {
			return duringValue;
		}
		public void setDuringValue(Double duringValue) {
			this.duringValue = duringValue;
		}
    	
		
    }

}