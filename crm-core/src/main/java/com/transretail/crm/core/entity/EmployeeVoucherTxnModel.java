package com.transretail.crm.core.entity;


import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.springframework.beans.BeanUtils;

import com.transretail.crm.core.dto.EmployeeVoucherTxnDto;
import com.transretail.crm.core.entity.enums.TxnStatus;
import com.transretail.crm.core.entity.enums.VoucherTransactionType;
import com.transretail.crm.core.entity.support.CustomAuditableEntity;

@Table(name = "CRM_EMPLOYEE_VOUCHER_TXN")
@Entity
public class EmployeeVoucherTxnModel extends CustomAuditableEntity<Long> implements Serializable {

    private static final long serialVersionUID = 1211221L;
    private static final String[] IGNORE = {"employeeVoucher"};


    @Column(name = "TXN_NO")
    private String transactionNo;

    @Column(name = "TXN_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date transactionDateTime;


    @Column(name = "TXN_AMOUNT")
    private Double transactionAmount;

//    @ManyToOne(fetch = FetchType.LAZY)
//    @JoinColumn(name = "STORE_CODE")
    @Column(name = "Store")
    private String store;


    @Column(name = "TXN_TYPE")
    @Enumerated(EnumType.STRING)
    private VoucherTransactionType voucherTransactionType;
    
    @Column(name = "STATUS")
    @Enumerated(EnumType.STRING)
    private TxnStatus status;
    
    @Column(name = "COMMENTS")
    private String comment;
    
    public EmployeeVoucherTxnModel() {
    	
    }
    
    public EmployeeVoucherTxnModel(EmployeeVoucherTxnDto voucherTxnDto) {
    	BeanUtils.copyProperties(voucherTxnDto, this, IGNORE);
    }

    public String getTransactionNo() {
        return transactionNo;
    }

    public void setTransactionNo(String transactionNo) {
        this.transactionNo = transactionNo;
    }

    public Date getTransactionDateTime() {
        return transactionDateTime;
    }

    public void setTransactionDateTime(Date transactionDateTime) {
        this.transactionDateTime = transactionDateTime;
    }

    public Double getTransactionAmount() {
        return transactionAmount;
    }

    public void setTransactionAmount(Double transactionAmount) {
        this.transactionAmount = transactionAmount;
    }

    public String getStore() {
        return store;
    }

    public void setStore(String store) {
        this.store = store;
    }

    public VoucherTransactionType getVoucherTransactionType() {
        return voucherTransactionType;
    }

    public void setVoucherTransactionType(VoucherTransactionType voucherTransactionType) {
        this.voucherTransactionType = voucherTransactionType;
    }

	public TxnStatus getStatus() {
		return status;
	}

	public void setStatus(TxnStatus status) {
		this.status = status;
	}

	public String getComment() {
		return comment;
	}

	public void setComment(String comment) {
		this.comment = comment;
	}
}
