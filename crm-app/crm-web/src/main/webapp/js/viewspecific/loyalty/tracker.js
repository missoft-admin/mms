$(document).ready(function() {
	
	initFields();
	initButtons();
	
	function initFields() {
		
		$("input[name='dateFrom']").datepicker({
			autoclose : true,
			format : 'dd-mm-yyyy'
		}).on('changeDate', function(selected) {
			startDate = new Date(selected.date.valueOf());
			startDate.setDate(startDate.getDate(new Date(selected.date.valueOf())));
			$("input[name='dateTo']").datepicker('setStartDate', startDate);
		});
		
		$("input[name='dateTo']").datepicker({
			autoclose : true,
			format : 'dd-mm-yyyy'
		}).on('changeDate', function(selected) {
			endDate = new Date(selected.date.valueOf());
			endDate.setDate(endDate.getDate(new Date(selected.date.valueOf())));
			$("input[name='dateFrom']").datepicker('setEndDate', endDate);
		});
	}
	
	function initButtons() {
		$("#inventoryTrackerPrintButton").click(function(e) {
			e.preventDefault();
			var url = $("#reportType option:selected").data("url") + "?" + $("#inventoryTrackerForm").serialize();
			var selectedReportType = $("select[name='reportType']").val();
			if(selectedReportType == "pdf")
				window.open(url, '_blank', 'toolbar=0,location=0,menubar=0' );
			else if(selectedReportType == "excel")
				window.location.href = url;
		});
	}
});

