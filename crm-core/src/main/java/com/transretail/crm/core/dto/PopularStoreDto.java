package com.transretail.crm.core.dto;


import java.math.BigDecimal;


public class PopularStoreDto {

	private String storeCode;
	private String storeName;
	private Long noOfCustomers;
	private Long noOfMembers;
	private Long noOfNonMembers;
	private Long noOfTxns;
	private BigDecimal totalTxnAmount;



    public PopularStoreDto( String storeCode, 
    		String storeName,
    		Long noOfMembers, 
    		Long noWithCxIds, 
    		Long noOfTxns, 
    		Double totalTxnAmount ) {

    	super();
    	this.storeCode = storeCode;
    	this.storeName = storeName;
    	this.noOfMembers = null != noOfMembers? noOfMembers : 0L;
    	this.noOfNonMembers = null != noOfTxns? noOfTxns - ( null != noWithCxIds? noWithCxIds : 0L ) : 0L;
    	this.noOfCustomers = this.noOfMembers.longValue() + this.noOfNonMembers.longValue();
    	this.noOfTxns = noOfTxns;
    	this.totalTxnAmount = new BigDecimal( totalTxnAmount );
    }



	public String getStoreCode() {
		return storeCode;
	}
	public void setStoreCode(String storeCode) {
		this.storeCode = storeCode;
	}
	public String getStoreName() {
		return storeName;
	}
	public void setStoreName(String storeName) {
		this.storeName = storeName;
	}
	public Long getNoOfCustomers() {
		return noOfCustomers;
	}
	public void setNoOfCustomers(Long noOfCustomers) {
		this.noOfCustomers = noOfCustomers;
	}
	public Long getNoOfMembers() {
		return noOfMembers;
	}
	public void setNoOfMembers(Long noOfMembers) {
		this.noOfMembers = noOfMembers;
	}
	public Long getNoOfNonMembers() {
		return noOfNonMembers;
	}
	public void setNoOfNonMembers(Long noOfNonMembers) {
		this.noOfNonMembers = noOfNonMembers;
	}
	public Long getNoOfTxns() {
		return noOfTxns;
	}
	public void setNoOfTxns(Long noOfTxns) {
		this.noOfTxns = noOfTxns;
	}
	public BigDecimal getTotalTxnAmount() {
		return totalTxnAmount;
	}
	public void setTotalTxnAmount(BigDecimal totalTxnAmount) {
		this.totalTxnAmount = totalTxnAmount;
	}

}
