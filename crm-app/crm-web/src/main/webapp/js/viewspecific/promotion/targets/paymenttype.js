$(document).ready( function() {
	$paymentTypesModal = $("#targetPaymentTypesDialog");
	var eftCode = $("input[name='eftCode']").val();
	
	initDialogs();
	
	initButtons();
	
	$("#promotionForm").submit(function() {
		$("form[name='promotion']").find("select[name='targetPaymentTypes'] > option").prop("selected", "selected");
	});
	
	$(".paymentTypeCheckbox").click(toggleWildcardBox);
	
	$(".targetWildcardsSel").change(toggleRemoveEftButton);
	
	$(".addWildcard").click(addWildcardStr);
	
	$(".removeWildcards").click(removeWildcard);
	
	function removeWildcard() {
		$parent = $(this).closest(".wildcardContainer");
		$parent.find(".targetWildcardsSel option:selected").remove();
		$parent.find(".targetWildcardsSel").each(toggleRemoveEftButton);
	}
	
	function addWildcardStr() {
		$parent = $(this).closest(".wildcardContainer");
		var val = $parent.find(".targetWildcardsStr").val();
		if(val != null && val != "") {
			$("<option>").attr("value", val).text(val).appendTo($parent.find(".targetWildcardsSel"));
			$parent.find(".targetWildcardsStr").val("");
		}
	}
	
	function toggleRemoveEftButton() {
		$parent = $(this).closest(".wildcardContainer");
		if($parent.find(".targetWildcardsSel option:selected").length > 0)
			$parent.find(".removeWildcardsContainer").show();
		else
			$parent.find(".removeWildcardsContainer").hide();
	}
	
	/*function toggleRemoveEftButton() {
		if($("#targetPaymentTypesDialog").find("select[name='targetEfts'] option:selected").length > 0)
			$("#removeEftContainer").show();
		else
			$("#removeEftContainer").hide();
	}*/
	
	function toggleWildcardBox() {
		var code = $(this).val();
		if($(this).is(":checked")) {
			$("." + code + "Container").show();
		} else {
			$("." + code + "Container").hide();
		}
	}
	
	/*function toggleEftBox() {
		$eftCheckbox = $("#targetPaymentTypesDialog").find("input[name='targetPaymentTypes'][value='"+eftCode+"']");
		if($eftCheckbox.is(":checked")) {
			$(".eftContainer").show();
			toggleRemoveEftButton();
		} else {
			$(".eftContainer").hide();
			$("select[name='targetEfts']").empty();
			$("input[name='eftStr']").val("");
		}
	}*/
	
	function initDialogs() {
		
		$paymentTypesModal.modal({
			show : false
		});
	
		$paymentTypesModal.on('show.bs.modal', openPaymentTypeDialogAction);
	}
	
	function showPaymentTypesDialog() {
		$paymentTypesModal.modal("show");
	}
	
	function openPaymentTypeDialogAction() {
		$selectedPaymentType = $("form[name='promotion']").find("select[name='targetPaymentTypes'] > option");
		$selectedPaymentType.each(function() {
			var value = $(this).val();
			$("#targetPaymentTypesDialog").find("input[name='targetPaymentTypes'][value='"+value+"']").prop("checked", "checked");
		});
		
		$(".paymentTypeCheckbox").each(toggleWildcardBox);
		$(".targetWildcardsIn").each(function() {
			var value = $(this).val();
			var code = $(this).data("code");
			$sel = $(".targetWildcardsSel" + code);
			if(value != null && value != "") {
				$.each(value.split(","), function(index, value) {
					$("<option>").attr("value", value).text(value).appendTo($sel);
				});	
			}
		});
		
		/*var eftCodes = $("input[name='targetEftWildcards']").val();
		if(eftCodes != null && eftCodes != "") {
			$.each(eftCodes.split(","), function(index, value) {
				$("<option>").attr("value", value).text(value).appendTo($("select[name='targetEfts']"));
			});	
		}*/
	}
	
	function initButtons() {
		$("#paymentTypeLnk").click(function(e) {
			e.preventDefault();
			showPaymentTypesDialog();
		});
		
		$("#paymentTypeCancelButton").click(function(e) {
			e.preventDefault();
			closePaymentTypeDialog();
		});
		
		$("#paymentTypeSaveButton").click(function(e) {
			e.preventDefault();
			
			$("form[name='promotion']").find("select[name='targetPaymentTypes']").empty();
			
			$("input[name='targetPaymentTypes']:checked").each(function() {
				$parentDiv = $(this).closest(".paymentTypeContainer");
				var value = $(this).val();
				var text = $parentDiv.find(".targetText").text();
				
				$option = $("<option>").attr({"value" : value}).text(text);
				
				$("form[name='promotion']").find("select[name='targetPaymentTypes']").append($option);
				
					
			});
			
			$(".paymentTypeCheckbox").each(function() {
				$parent = $("." + $(this).val() + "Container");
				$sel = $parent.find(".targetWildcardsSel");
				$input = $(".targetWildcardsIn" + $sel.data("code"));
				if($(this).is(":checked")) {
					$sel.find("option").prop("selected", "selected");
					var value = $sel.val();
					$input.val(value);
				} else {
					$input.val("");
				}
			});
			
			/*$eftCheckbox = $("#targetPaymentTypesDialog").find("input[name='targetPaymentTypes'][value='"+eftCode+"']");
			
			if($eftCheckbox.is(":checked")) {
				$("select[name='targetEfts'] option").prop("selected", "selected");
				$("input[name='targetEftWildcards']").val($("select[name='targetEfts']").val());
			} else {
				$("input[name='targetEftWildcards']").val("");
			}*/

			closePaymentTypeDialog();
		});
		
		/*$("#addEft").click(function(e) {
			e.preventDefault();
			var eft = $.trim($("input[name='eftStr']").val());
			if(eft != null && eft != "") {
				$("<option>").attr("value", eft).text(eft).appendTo($("select[name='targetEfts']"));
				$("input[name='eftStr']").val("");
			}
		});*/
		
		/*$("#removeEft").click(function(e) {
			$("#targetPaymentTypesDialog").find("select[name='targetEfts'] option:selected").remove();
			toggleRemoveEftButton();
		});*/
		
	}
	
	function closePaymentTypeDialog() {
		$("#targetPaymentTypesDialog").find("input").prop('checked', false);
		$("select[name='targetEfts']").empty();
		$("#targetPaymentTypesDialog").modal("hide");
		$("input[name='eftStr']").val("");
	}
	
	
});