package com.transretail.crm.common.audit.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.transretail.crm.common.audit.annotation.Audited;
import com.transretail.crm.common.audit.annotation.AuditParam;
import com.transretail.crm.common.audit.service.EnrollmentService;
import com.transretail.crm.common.audit.service.Student;
import com.transretail.crm.common.audit.service.StudentService;

/**
 * @author Allan G. Ramirez (agramirez@exist.com)
 */
@Service("enrollmentService")
public class EnrollmentServiceImpl implements EnrollmentService {
    @Autowired
    private StudentService studentService;

    @Audited(description = "Enrolling $student")
    public void enroll(@AuditParam("student") Student student) {
        // ...
        studentService.saveStudent(student);
        // ...
    }
}
