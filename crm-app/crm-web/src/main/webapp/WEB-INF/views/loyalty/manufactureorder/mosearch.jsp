<%@ include file="../../common/taglibs.jsp"%>

<style type="text/css">
<!--
  .well .input {
    background-color: #fff;
    border: 1px solid #E8E8E8;
    box-shadow: none;    
    padding: 6px;
    height: 18px;
    margin-left: 5px;
  }
  .well .btn-primary {
    margin-left: 10px;
  }
  .well select {
    margin: 5px;
  }
  
-->
</style>

<form:form id="searchDto" cssClass="">
<div class="well clearfix">
<div class="form-inline form-search">
	<spring:message code="label_search" var="searchLabel"/>
	<label class="label-single"><h5>${searchLabel}:</h5></label>
	<select id="criteria" class="searchField">
		<option value=""><spring:message code="label_criteria" arguments="${searchLabel}" /></option>
		<c:forEach items="${criterias}" var="criteria">
		<option value="${criteria.field}">${criteria.name}</option>
		</c:forEach>
	</select>
	<input type="text" id="criteriaValue" class="input searchField" placeholder="${searchLabel}"/>
	<input type="text" name="orderDate" id="orderDateSearch" class="input searchField" placeholder="<spring:message code="mo_prop_orderdate" />"/>
	<input id="search" type="button" class="btn btn-primary" value="<spring:message code="button_search"/>" text="Search"/>
	<input type="button" id="clear" value="<spring:message code="label_clear" />" class="btn "/>
</div>
<div class="form-inline form-search">
	<label class="label-single"><h5><spring:message code="label_filter" />:</h5></label>
	<select name="cardVendorId" class="searchField">
		<option value=""><spring:message code="mo_prop_supplier" /></option>
		<c:forEach items="${vendors}" var="supplier">
		<option value="${supplier.id}">${supplier.name}</option>
		</c:forEach>
	</select>
	<select name="statusCode" class="searchField">
		<option value=""><spring:message code="mo_prop_status" /></option>
		<c:forEach items="${status}" var="stat">
		<option value="${stat.code}">${stat.description}</option>
		</c:forEach>
	</select>
</div>
</div>
</form:form>
