package com.transretail.crm.core.entity;


import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.hibernate.annotations.Type;

import com.transretail.crm.core.entity.lookup.LookupDetail;
import com.transretail.crm.core.entity.lookup.Store;
import com.transretail.crm.core.entity.support.CustomAuditableEntity;


@Entity(name="CRM_STORE_PSOFT")
public class PeopleSoftStore extends CustomAuditableEntity<Long> {

	private static final long serialVersionUID = 1L;



	@Column(name = "PSOFT_CODE", length=50)
	private String peoplesoftCode;

	@OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "STORE_CODE", referencedColumnName="CODE", unique=true)
    private Store store;

	@ManyToOne
	@JoinColumn(name="BU_CODE")
	private LookupDetail businessUnit;

	@ManyToOne
	@JoinColumn(name="BU_REGION")
	private LookupDetail businessRegion;

	@ManyToOne
	@JoinColumn(name="BU_TERRITORY")
	private LookupDetail businessTerritory;

    @Column(name = "ALLOW_GC_TXN")
    @Type(type = "yes_no")
	private Boolean allowGcTxn;



    public String getPeoplesoftCode() {
		return peoplesoftCode;
	}
	public void setPeoplesoftCode(String peoplesoftCode) {
		this.peoplesoftCode = peoplesoftCode;
	}
	public Store getStore() {
		return store;
	}
	public void setStore(Store store) {
		this.store = store;
	}
	public LookupDetail getBusinessUnit() {
		return businessUnit;
	}
	public void setBusinessUnit(LookupDetail businessUnit) {
		this.businessUnit = businessUnit;
	}
	public LookupDetail getBusinessRegion() {
		return businessRegion;
	}
	public void setBusinessRegion(LookupDetail businessRegion) {
		this.businessRegion = businessRegion;
	}
	public LookupDetail getBusinessTerritory() {
		return businessTerritory;
	}
	public void setBusinessTerritory(LookupDetail businessTerritory) {
		this.businessTerritory = businessTerritory;
	}
	public Boolean getAllowGcTxn() {
		return allowGcTxn;
	}
	public void setAllowGcTxn(Boolean allowGcTxn) {
		this.allowGcTxn = allowGcTxn;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;

		if (o == null || getClass() != o.getClass()) return false;

		PeopleSoftStore that = (PeopleSoftStore) o;

		return new EqualsBuilder()
				.append(peoplesoftCode, that.peoplesoftCode)
				.isEquals();
	}

	@Override
	public int hashCode() {
		return new HashCodeBuilder(17, 37)
				.append(peoplesoftCode)
				.toHashCode();
	}
}
