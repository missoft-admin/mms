package com.transretail.crm.media.service.impl;


import com.transretail.crm.core.dto.MessageDto;
import com.transretail.crm.core.entity.ApplicationConfig;
import com.transretail.crm.core.entity.MarketingChannel;
import com.transretail.crm.core.entity.enums.AppKey;
import com.transretail.crm.core.repo.ApplicationConfigRepo;
import com.transretail.crm.media.service.SmsService;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Service;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;


@Service("smsService")
public class SmsServiceImpl implements SmsService {

    //private static final String URL_SMSWEBSERVICE = "http://%GATEWAYIP%/sms.php?tipe=XML&smsid=%SMSID%&msisdn=%MOBILENUMBER%&mess=%MESSAGE%&API=%API%";

	private static final Logger LOGGER = LoggerFactory.getLogger( SmsServiceImpl.class );

    @Autowired
    private MessageSource messageSource;

    @Autowired
    private ApplicationConfigRepo applicationConfigRepo;

	@Override
	public boolean send( MessageDto dto ) {
		String[] recipients = ArrayUtils.isNotEmpty( dto.getRecipients() )? dto.getRecipients() : 
			StringUtils.isNotBlank( dto.getContactNo() )? new String[]{ dto.getContactNo() } : null;
		if ( ArrayUtils.isEmpty( recipients ) ) {
			return false;
		}

		LOGGER.info( ":send:: SMS " );

		BufferedReader in = null;
		for ( String contactNo : recipients ) {
			for ( int i = 0; i < dto.getMessage().length() / MarketingChannel.PER_COUNT + 1; i++ ) {
				String message = dto.getMessage().substring( i*MarketingChannel.PER_COUNT, 
						( dto.getMessage().length() >= MarketingChannel.PER_COUNT*(i + 1) )? 
								MarketingChannel.PER_COUNT*(i + 1) : dto.getMessage().length() );
				try {
		            /*String query = String.format( "requestParam=%s", URLEncoder.encode(prop.toString(), "UTF-8") );
		            URL url = new URL(urlName + "?" + query);*/
                    ApplicationConfig urlConfig = applicationConfigRepo.findByKey(AppKey.SMS_GATEWAY_URL);

                    String urlConfigValue = urlConfig.getValue();
                    String strUrl = urlConfigValue.replace( "%MOBILENUMBER%", contactNo ).replace( "%MESSAGE%", URLEncoder.encode(message, "UTF-8"));
                    LOGGER.info("SMS URL={}", strUrl);
		            URLConnection conn = new URL( strUrl ).openConnection();            
		            conn.setDoOutput( true );
		            /*conn.setRequestProperty("Content-Type", "application/octet-stream");*/
		            
		            /*ObjectInputStream ois = new ObjectInputStream( conn.getInputStream() );*/                
		            in = new BufferedReader( new InputStreamReader( conn.getInputStream() ) );
		            while ( in.readLine() != null ) {
		            }
		            
		            String header = conn.getHeaderField(0);                      
		            char status = header.charAt(9);
		            if ( status == '4' ) { System.out.println("Client Errors: " + header.substring(9,12)); }            
		            else if ( status == '2' ) { System.out.println("Request failed Errors: " + header.substring(9,12)); }
		            else if ( status == '5' ) { System.out.println("Server Errors: " + header.substring(9,12)); }
		            System.out.println( "Successful: " + header.substring( 9, 12 ) );
		        }
		        catch( Exception e ) {
		    		LOGGER.error( ":send:: SMS Exception message: " + e.getMessage() );
		            return false;
		        }
				finally {
					try { in.close(); } catch( Exception e ){}
				}
			}
		}

		LOGGER.info( ":send:: SMS successfully sent sms to recipients:" + recipients );
		return true;
	}

}
