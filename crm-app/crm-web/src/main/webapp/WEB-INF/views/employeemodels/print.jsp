<%@ include file="../common/taglibs.jsp"%>
<div id="content_print" class="modal hide  nofly modal-dialog">
  <div class="modal-content">
    <form:form id="printForm" cssClass="modal-form" >
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
	      <h4>
	        <spring:message var="printArgs" code="employee.purchase.report.title" />
	        <span><spring:message code="label_print_args" arguments="${printArgs}" /></span>
	      </h4>
      </div>

      <div class="modal-body">
	<div class="hide alert alert-error" id="content_error"><button type="button" class="close" onclick="$(this).parent().hide();">&times;</button><div><form:errors path="*"/></div></div>

	<div class="row-fluid">
	  <label for="startDate" class="span4"><spring:message code="employee.purchase.report.filter.purchase.date.from" /></label>
	  <label for="endDate" class="span4"><spring:message code="employee.purchase.report.filter.purchase.date.to" /></label>
	  <label for="endDate" class="span4">&nbsp;</label>
	</div>
	<spring:message var="msg_invaliddaterange" code="points_msg_invaliddaterange" />
	<div class="row-fluid input-daterange controls" id="printDuration" data-msg-invaliddaterange="${msg_invaliddaterange}"
	  data-msg-reqddaterange="<spring:message code="points_msg_reqddaterange" />">
	  <input id="startDate" type="text" class="span4" />
	  <input id="endDate" type="text" class="span4" />
	  <select name="reportTypeMaster" id="reportTypeMaster">
	    <c:forEach items="${reportTypes}" var="reportType">
	      <option value="${reportType.code}"><spring:message code="report_type_${reportType.code}" /></option>
	    </c:forEach>
	  </select>
	</div>
      </div>

      <div class="modal-footer">
      	<!--<input id="saveButton" class="btn btn-print" type="submit" value="<spring:message code="label_print"/>" />
      	<input id="cancelButton" class="btn btn-not" type="button" value="<spring:message code="label_cancel"/>" data-dismiss="modal" /> -->
      	<button type="button" id="saveButton" class="btn btn-primary"><spring:message code="label_print"/></button>
        <button type="button" id="cancelButton" data-dismiss="modal" class="btn btn-default"><spring:message code="label_close"/></button>
      </div>
    </form:form>
  </div>

  <script src='<c:url value="/js/bootstrap/bootstrap-datepicker.js"/>' type='text/javascript'>< ![CDATA[ & nbsp; ]] ></script>
  <link href="<c:url value="/css/bootstrap/datepicker.css" />" rel="stylesheet" />
  <script type="text/javascript">
	    $(document).ready(function() {
      var $theDialog = $("#content_print");
      var $thePrintDuration = $("#printDuration");

      initFormFields();
      initDialog();
      initLinks();

      function initFormFields() {

	$thePrintDuration.datepicker({
	  autoclose: true,
	  format: "dd-mm-yyyy"
	});
	
	$("#saveButton").click(function() {
		$("#printForm").submit();
	});
      }

      function initDialog() {
	$theDialog.modal({show: false});
      }

      function initLinks() {
	$.each($(".link_print"), function(idx, aLink) {
	  $(aLink).click(function(e) {
	    print();
	    $("#content_print").modal("show");
	  });
	});
      }

      function print() {
	$("#printForm").submit(function(e) {
	  var startDateVal = $("#printForm").find("#startDate").val();
	  var endDateVal = $("#printForm").find("#endDate").val();

	  if( !startDateVal || !endDateVal ) {
	      e.preventDefault();
	      var theErrorContainer = $("#content_print").find("#content_error");
	      theErrorContainer.find("div").html($("#printDuration").data("msg-reqddaterange"));
	      theErrorContainer.show("slow");
	  }
	  else if (startDateVal && endDateVal
		  && startDateVal > endDateVal) {
	    e.preventDefault();
	    var theErrorContainer = $("#content_print").find("#content_error");
	    theErrorContainer.find("div").html($("#printDuration").data("msg-invaliddaterange"));
	    theErrorContainer.show("slow");
	  }
	  else {
	    $theDialog.modal("hide");
	    var selectedReportType = $("select[name='reportTypeMaster']").val();
	    var url = ctx + "/report/template/" + selectedReportType + "/Employee%20Purchase%20Report/view?" 
		    + "dateFrom=" + (startDateVal ? startDateVal : 0) + "&dateTo=" + (endDateVal ? endDateVal : null);
	    if (selectedReportType === "pdf"){
	      window.open(url, '_blank', 'toolbar=0,location=0,menubar=0');
	      $theDialog.dialog('destroy').remove();
	      return true;
	    } else if (selectedReportType === "excel"){
	      e.preventDefault();
	      window.location.href = url;
	      return true;
	    }
	    return false;
	  }
	});
      }
    });
  </script>


</div>