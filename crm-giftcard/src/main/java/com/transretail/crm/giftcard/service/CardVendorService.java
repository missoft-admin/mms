package com.transretail.crm.giftcard.service;

import java.util.List;

import com.transretail.crm.giftcard.dto.CardVendorDto;
import com.transretail.crm.giftcard.dto.CardVendorResultList;
import com.transretail.crm.giftcard.dto.CardVendorSearchDto;

/**
 *
 */
public interface CardVendorService {
    Long saveCardVendor(CardVendorDto cardVendor);

    void updateCardVendor(Long cardVendorId, CardVendorDto cardVendor);

    CardVendorDto getCardVendorById(Long vendorId);

    CardVendorResultList getCardVendors(CardVendorSearchDto searchForm);
    
    List<String> getAllCardVendorsByFormalName();
}
