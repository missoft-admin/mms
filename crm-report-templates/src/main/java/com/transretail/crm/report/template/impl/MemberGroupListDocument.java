package com.transretail.crm.report.template.impl;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.apache.commons.lang3.StringUtils;
import org.joda.time.LocalDate;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.MessageSourceAware;
import org.springframework.context.support.MessageSourceAccessor;
import org.springframework.stereotype.Service;

import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Lists;
import com.mysema.query.jpa.impl.JPAQuery;
import com.transretail.crm.common.reporting.jasper.ClasspathFileResolver;
import com.transretail.crm.common.reporting.jasper.DefaultJRProcessor;
import com.transretail.crm.common.reporting.jasper.JRProcessor;
import com.transretail.crm.common.service.dto.request.PageSortDto;
import com.transretail.crm.common.service.dto.response.ResultList;
import com.transretail.crm.core.dto.MemberDto;
import com.transretail.crm.core.dto.MemberResultList;
import com.transretail.crm.core.dto.MemberSearchDto;
import com.transretail.crm.core.entity.MemberGroup;
import com.transretail.crm.core.entity.enums.ReportType;
import com.transretail.crm.core.entity.enums.Status;
import com.transretail.crm.core.security.util.UserUtil;
import com.transretail.crm.core.service.LookupService;
import com.transretail.crm.core.service.MemberGroupService;
import com.transretail.crm.core.service.MemberService;
import com.transretail.crm.giftcard.entity.QGiftCardInventory;
import com.transretail.crm.report.template.FilterField;
/**
 * @author ftopico
 */

@Service("memberGroupListDocument")
public class MemberGroupListDocument implements MessageSourceAware {
    
    @PersistenceContext
    private EntityManager em;
    @Autowired
    MemberGroupService memberGroupService;

    protected static final String TEMPLATE_NAME = "Member Group List Document";
    protected static final String REPORT_NAME = "reports/member_group_list_document.jasper";
    //Header Params
    private static final String PARAM_PRINT_DATE = "printedDate";
    private static final String PARAM_PRINTED_BY = "printedBy";
    private static final String PARAM_MEMBER_GROUP_NAME = "memberGroupName";
    private static final DateTimeFormatter FULL_DATE_PATTERN = DateTimeFormat.forPattern("MMMM d, yyyy");
    private MessageSourceAccessor messageSource;
    
    public ReportType[] getReportTypes() {
        return ReportType.values();
    }
    
    @Override
    public void setMessageSource(MessageSource messageSource) {
        this.messageSource = new MessageSourceAccessor(messageSource);
    }
    
    public String getName() {
        return TEMPLATE_NAME;
    }
    
    public String getDescription() {
        return messageSource.getMessage("label_group_membergroup");
    }
    
    public Set<FilterField> getFilters() {
        return null;
    }
    
    public JRProcessor createJrProcessor(Integer segment, Long id) {
        MemberGroup group = memberGroupService.getMemberGroup(id);
        List<MemberDto> listDto = memberGroupService.getQualifiedMembersForPrint(group, segment);
        List<MemberDto> updatedMembers = new ArrayList<MemberDto>();
        
        if (group.getPrepaidUser()) {
            QGiftCardInventory qInventory = QGiftCardInventory.giftCardInventory;
            List<Long> prepaidCardUsers = 
                    new JPAQuery(em)
                        .from(qInventory)
                        .where(qInventory.owningMember.isNotNull())
                        .list(qInventory.owningMember.id);
            List<MemberDto> memberList = Lists.newArrayList(listDto);
            if (!prepaidCardUsers.isEmpty() && listDto != null) {
                for (MemberDto member : memberList) {
                    if (prepaidCardUsers.contains(member.getId())) {
                        updatedMembers.add(member);
                    }
                }
                    
            }  
        }

        List<ReportBean> beansDataSource = Lists.newArrayList();
    	List<MemberGroupReport> list = new ArrayList<MemberGroupReport>();
    	
    	if (group.getPrepaidUser()) {
    	    for (MemberDto memberDto : updatedMembers) {
                MemberGroupReport memberGroup = new MemberGroupReport();
                memberGroup.setAccountId(memberDto.getAccountId());
                memberGroup.setFirstName(memberDto.getFirstName());
                memberGroup.setLastName(memberDto.getLastName());
                memberGroup.setContactNumber(memberDto.getContact().toString());
                if (memberDto.getProfessionalProfile() != null)
                memberGroup.setBusinessPhone(memberDto.getProfessionalProfile().getBusinessPhone());
                memberGroup.setCompanyName(memberDto.getCompanyName());
                list.add(memberGroup);
            }
    	} else {
        	for (MemberDto memberDto : listDto) {
        	    MemberGroupReport memberGroup = new MemberGroupReport();
        	    memberGroup.setAccountId(memberDto.getAccountId());
        	    memberGroup.setFirstName(memberDto.getFirstName());
        	    memberGroup.setLastName(memberDto.getLastName());
        	    memberGroup.setContactNumber(memberDto.getContact().toString());
        	    if (memberDto.getProfessionalProfile() != null)
        	    memberGroup.setBusinessPhone(memberDto.getProfessionalProfile().getBusinessPhone());
        	    memberGroup.setCompanyName(memberDto.getCompanyName());
        	    list.add(memberGroup);
        	}
    	}
    	
    	Map<String, Object> parameters = ImmutableMap.<String, Object>builder()
    	        .put(PARAM_PRINT_DATE, FULL_DATE_PATTERN.print(new LocalDate()))
    	        .put(PARAM_PRINTED_BY, UserUtil.getCurrentUser().getUsername())
    	        .put(PARAM_MEMBER_GROUP_NAME, group.getName()).build();
    	
    	beansDataSource.add(new ReportBean(list));
    	DefaultJRProcessor jrProcessor = new DefaultJRProcessor(REPORT_NAME, beansDataSource);
    	jrProcessor.addParameters(parameters);
    	jrProcessor.setFileResolver(new ClasspathFileResolver("reports"));
    	
    	return jrProcessor;
    }
    
    public class MemberGroupReport {
        
        String accountId;
        String firstName;
        String lastName;
        String contactNumber;
        String businessPhone;
        String companyName;
        
        public MemberGroupReport() {}

        public MemberGroupReport(String accountId, String firstName,
                String lastName, String contactNumber, String businessPhone,
                String companyName) {
            super();
            this.accountId = accountId;
            this.firstName = firstName;
            this.lastName = lastName;
            this.contactNumber = contactNumber;
            this.businessPhone = businessPhone;
            this.companyName = companyName;
        }

        public String getAccountId() {
            return accountId;
        }

        public String getFirstName() {
            return firstName;
        }

        public String getLastName() {
            return lastName;
        }

        public String getContactNumber() {
            return contactNumber;
        }

        public String getBusinessPhone() {
            return businessPhone;
        }

        public String getCompanyName() {
            return companyName;
        }

        public void setAccountId(String accountId) {
            this.accountId = accountId;
        }

        public void setFirstName(String firstName) {
            this.firstName = firstName;
        }

        public void setLastName(String lastName) {
            this.lastName = lastName;
        }

        public void setContactNumber(String contactNumber) {
            this.contactNumber = contactNumber;
        }

        public void setBusinessPhone(String businessPhone) {
            this.businessPhone = businessPhone;
        }

        public void setCompanyName(String companyName) {
            this.companyName = companyName;
        }
        
    }
    
    public static class ReportBean {
        
        private List<MemberGroupReport> list;
        
        public ReportBean() {}
        
        public ReportBean(List<MemberGroupReport> list) {
            super();
            this.list = list;
        }

        public List<MemberGroupReport> getList() {
            return list;
        }

        public void setList(List<MemberGroupReport> list) {
            this.list = list;
        }

    }
}
