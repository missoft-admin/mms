package com.transretail.crm.web.controller;


import com.transretail.crm.common.service.dto.response.ResultList;
import com.transretail.crm.core.dto.PointsDto;
import com.transretail.crm.core.dto.PointsSearchDto;
import com.transretail.crm.core.dto.RewardBatchDto;
import com.transretail.crm.core.dto.ServiceDto;
import com.transretail.crm.core.entity.enums.AppKey;
import com.transretail.crm.core.entity.enums.EmployeeSearchCriteriaTwo;
import com.transretail.crm.core.entity.enums.ReportType;
import com.transretail.crm.core.entity.enums.TxnStatusSearch;
import com.transretail.crm.core.service.PointsRewardService;
import com.transretail.crm.core.service.StoreService;
import com.transretail.crm.core.util.StringUtility;
import com.transretail.crm.schedule.service.JobSchedulerService;
import com.transretail.crm.schedule.trigger.PointsRewardSchedulerTrigger;
import com.transretail.crm.web.controller.util.ControllerResponse;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.time.DateUtils;
import org.joda.time.DateTime;
import org.joda.time.LocalTime;
import org.quartz.CronExpression;
import org.quartz.SchedulerException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.util.UriUtils;
import org.springframework.web.util.WebUtils;

import javax.servlet.http.HttpServletRequest;
import java.io.UnsupportedEncodingException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
//import com.transretail.crm.web.notifications.Notifiable;
//import com.transretail.crm.web.notifications.NotificationUpdate;


@RequestMapping("/pointsreward")
@Controller
public class PointsRewardController {
	
    @Autowired
    PointsRewardService pointsRewardService;
    @Autowired
    JobSchedulerService jobSchedulerService;
    @Autowired
    private MessageSource messageSource;
    @Autowired
    StoreService storeService;


	private static final String PATH_REWARDFORPROCESSING 	= "/pointsreward/forprocessing";
	private static final String PATH_REWARDGENSCHED 		= "/pointsreward/generate/schedule";

	private static final String UIATTR_REWARDBATCHDTO      	= "rewardBatchDto";
	private static final String UIATTR_REWARDBATCH         	= "rewardBatch";

	private static final String UIATTR_VALUE_REWARDGENSCHED	= "rewardGenerationSched";
	private static final String UIATTR_NEXTGENSCHED			= "nextRewardGenSched";
	private static final String UIATTR_NEXTGENSCHED_COVERED	= "nextRewardGenSchedCoverage";
	private static final String UIATTR_DAYS				   	= "days";
	private static final String UIATTR_DAY				   	= "day";
	private static final String UIATTR_TIME				   	= "time";
	
	private static final String UI_ATTR_SEARCHFIELDS               = "rewardsSearchFields";
	private static final String UI_ATTR_FILTER_STATUS              = "rewardsFilterStatus";
//	private static final String UI_ATTR_FILTER_TXN_TYPES           = "rewardsFilterTxnTypes";
	private static final String UI_ATTR_FILTER_REGISTERED_STORES   = "rewardsFilterRegisteredStores";

    @RequestMapping( produces="text/html" )
	public String showPointsRewardForProcessing() {
		return PATH_REWARDFORPROCESSING;
    }


    @RequestMapping( value="/forprocessing", produces="text/html" )
	public String showPointsRewardForProcessing(
			@RequestParam(value="page", required=false) Integer inPage,
			@RequestParam(value="size", required=false) Integer inSize, 
			@ModelAttribute( UIATTR_REWARDBATCH ) RewardBatchDto inRewardBatch,
			BindingResult inBinding, Model inUiModel ) {

    	ServiceDto theSvcResp = pointsRewardService.getPointsRewardForProcessing();

    	if ( CollectionUtils.isEmpty( ( (RewardBatchDto)theSvcResp.getReturnObj() ).getRewards() ) ) {
    		inBinding.reject( "points_msg_emptyrewardsforprocessing" );
    	}
    	inUiModel.addAttribute( UIATTR_REWARDBATCHDTO, theSvcResp.getReturnObj() );
    	
    	inUiModel.addAttribute("reportTypes", ReportType.values());
    	
    	populateRewardstFilters(inUiModel);
    	
    	return PATH_REWARDFORPROCESSING;
    }
    
    @RequestMapping(value = "/forprocessing/list", method = RequestMethod.POST)
	public @ResponseBody ResultList<PointsDto> listForProcessing(@RequestBody PointsSearchDto dto) {
		return pointsRewardService.listRewardsForProcessing(dto);
	}

//    @Notifiable(type=ModelType.REWARDS)
	@RequestMapping( value="/forprocessing/generate", produces="text/html" )
	public String generatePointsRewardForProcessing(
			@RequestParam(value="page", required=false) Integer inPage,
			@RequestParam(value="size", required=false) Integer inSize,
			@ModelAttribute( UIATTR_REWARDBATCH ) RewardBatchDto inRewardBatch,
			BindingResult inBinding, Model inUiModel ) {

		pointsRewardService.generatePointsRewardForApproval();

    	return showPointsRewardForProcessing( inPage, inSize, inRewardBatch, inBinding, inUiModel );
    }

	@RequestMapping( value="/delete/{id}", produces="application/json; charset=utf-8", method=RequestMethod.GET )
	public @ResponseBody ControllerResponse deletePointsReward( @PathVariable("id") String id ) {

    	ControllerResponse resp = new ControllerResponse();
		if ( StringUtils.isNotBlank( id ) ) {
			pointsRewardService.deletePointsReward( id );
    		resp.setSuccess( true );
		}
		return resp;
	}

	@RequestMapping( value="/delete/{id}", produces="text/html" )
	public String deletePointsReward( 
			@PathVariable(value="id") String inId,
			@RequestParam(value="page", required=false) Integer inPage,
			@RequestParam(value="size", required=false) Integer inSize,
			@ModelAttribute( UIATTR_REWARDBATCH ) RewardBatchDto inRewardBatch,
			BindingResult inBinding, Model inUiModel ) {

		pointsRewardService.deletePointsReward( inId );

		return showPointsRewardForProcessing( inPage, inSize, inRewardBatch, inBinding, inUiModel );
	}


	@SuppressWarnings("unchecked")
	@RequestMapping( value="/reject/{rejectedids}", produces = "text/html" )
	public String rejectPointsRewards( 
			@PathVariable(value="rejectedids") String inRejectedIds,
			@RequestParam(value="page", required=false) Integer inPage,
			@RequestParam(value="size", required=false) Integer inSize,
			@ModelAttribute( UIATTR_REWARDBATCH ) RewardBatchDto inRewardBatch,
			BindingResult inBinding, Model inUiModel ) {

		pointsRewardService.rejectPointsRewards( StringUtility.generateList( String.class, inRejectedIds.split(",") ),
				inRewardBatch );

    	return showPointsRewardForProcessing( inPage, inSize, inRewardBatch, inBinding, inUiModel );
    }


	@SuppressWarnings("unchecked")
	@RequestMapping( value="/approve/{approvedids}", produces="text/html" )
	public String approvePointsRewards( 
			@PathVariable(value="approvedids") String inApprovedIds,
			@RequestParam(value="page", required=false) Integer inPage,
			@RequestParam(value="size", required=false) Integer inSize,
			@ModelAttribute( UIATTR_REWARDBATCH ) RewardBatchDto inRewardBatch,
			BindingResult inBinding, Model inUiModel ) {

		pointsRewardService.approvePointsRewards( StringUtility.generateList( String.class, inApprovedIds.split(",") ) );
//		updateNotification(inApprovedIds);
		
		return showPointsRewardForProcessing( inPage, inSize, inRewardBatch, inBinding, inUiModel );
	}

    @SuppressWarnings("unchecked")
    @RequestMapping(value="/approveAll", produces="text/html")
    public String approveAllPointsRewards(
            @RequestParam(value="page", required = false) Integer inPage,
            @RequestParam(value="size", required = false) Integer inSize,
            @ModelAttribute(UIATTR_REWARDBATCH) RewardBatchDto inRewardBatch,
            BindingResult inBinding, Model inUiModel) {
        pointsRewardService.approveAllPointsRewards();
        return showPointsRewardForProcessing( inPage, inSize, inRewardBatch, inBinding, inUiModel );
    }

	@SuppressWarnings("unchecked")
	@RequestMapping(value="/reset/{resetids}", produces="text/html")
	public String resetPointsRewards(
			@PathVariable(value="resetids") String resetIds,
			@RequestParam(value="page", required=false) Integer inPage,
			@RequestParam(value="size", required=false) Integer inSize,
			@ModelAttribute( UIATTR_REWARDBATCH ) RewardBatchDto inRewardBatch,
			BindingResult inBinding, Model inUiModel ) {
		pointsRewardService.resetPointsRewards( StringUtility.generateList( String.class, resetIds.split(",") ) );
//		updateNotification(inApprovedIds);
		
		return showPointsRewardForProcessing( inPage, inSize, inRewardBatch, inBinding, inUiModel );
	}

//	@NotificationUpdate
//	public ControllerResponse updateNotification(String approvedIds) {
//		ControllerResponse resp = new ControllerResponse();
//		resp.setSuccess(true);
//		resp.setModelId(approvedIds);
//		return resp;
//	}



	@RequestMapping( value={ "/generate/schedule" }, method=RequestMethod.GET )
    public String rescheduleVoucherGeneration( Model uiModel ) throws Exception {

		String strCronExp = jobSchedulerService.getCronExpression( 
				PointsRewardSchedulerTrigger.TRIGGER_NAME, PointsRewardSchedulerTrigger.TRIGGER_GROUP );
    	uiModel.addAttribute( UIATTR_VALUE_REWARDGENSCHED, strCronExp );

    	CronExpression cronExp = new CronExpression( strCronExp );
    	DateTime dt = new DateTime( cronExp.getNextValidTimeAfter( new Date() ).getTime() );
    	
    	uiModel.addAttribute( UIATTR_DAYS, 28 );
    	uiModel.addAttribute( UIATTR_DAY, dt.getDayOfMonth() );
    	uiModel.addAttribute( UIATTR_TIME, dt.getHourOfDay() + ":" + dt.getMinuteOfHour() + ":" + dt.getSecondOfMinute() );

    	Date nextGeneration = cronExp.getNextValidTimeAfter( new Date() );
    	uiModel.addAttribute( UIATTR_NEXTGENSCHED, new SimpleDateFormat( "dd MMMM yyyy HH:mm:ss" ).format( nextGeneration ) );
    	Date prevMonth = DateUtils.addMonths( nextGeneration, -1 );
    	uiModel.addAttribute( UIATTR_NEXTGENSCHED_COVERED, 
    			new SimpleDateFormat( "dd" ).format( DateUtils.truncate( prevMonth, Calendar.MONTH ) ) + " - " +
    			new SimpleDateFormat( "dd MMMM yyyy" ).format( DateUtils.addMilliseconds( DateUtils.ceiling( prevMonth, Calendar.MONTH ), -1 ) ) );

    	return PATH_REWARDGENSCHED;
	}

	@RequestMapping( value={ "/generate/reschedule" }, method=RequestMethod.GET )
    public  @ResponseBody ControllerResponse reschedule( 
    		@RequestParam(value="schedule", required=false) String sched,
    		@RequestParam(value="day", required=false) String day,
    		@RequestParam(value="time", required=false) String time,
			Model uiModel ) {

    	ControllerResponse resp = new ControllerResponse();
		resp.setSuccess( false );

		if ( StringUtils.isNotBlank( day ) && StringUtils.isNotBlank( time ) && StringUtils.isNotBlank( sched ) ) {
			LocalTime lt = null;
			try {
				lt = new LocalTime( time );
				String[] cronExpSegs = StringUtils.split( sched );
				cronExpSegs[0] = String.valueOf( lt.getSecondOfMinute() );
				cronExpSegs[1] = String.valueOf( lt.getMinuteOfHour() );
				cronExpSegs[2] = String.valueOf( lt.getHourOfDay() );
				cronExpSegs[3] = String.valueOf( day );
				sched = StringUtils.join( cronExpSegs, " " );
			}
			catch ( Exception e ) {
				resp.setResult( e.getMessage() );
			}
		}

    	if ( !StringUtils.isBlank( sched ) ) {
    		if ( !CronExpression.isValidExpression( sched ) ) {
    			resp.setResult( messageSource.getMessage( "points_msg_invalidsched", null, LocaleContextHolder.getLocale() ) );
    		}
    		else {
    			String ret = null;
				try {
					ret = jobSchedulerService.rescheduleJob( PointsRewardSchedulerTrigger.TRIGGER_NAME,
							PointsRewardSchedulerTrigger.TRIGGER_GROUP, sched, AppKey.REWARDS_JOB );
					sched = jobSchedulerService.getCronExpression( PointsRewardSchedulerTrigger.TRIGGER_NAME,
		    				PointsRewardSchedulerTrigger.TRIGGER_GROUP );
				} 
				catch ( SchedulerException e ) {
					resp.setResult( e.getMessage() );
				} 
				catch ( ParseException e ) {
					resp.setResult( e.getMessage() );
				}

				if ( StringUtils.isBlank( ret ) ) {
					resp.setResult( messageSource.getMessage( "points_msg_unsavedsched", null, LocaleContextHolder.getLocale() ) );
        		}
    		}
    	}

    	if ( null == resp.getResult() ) {
    		resp.setSuccess( true );
    		resp.setResult( sched );
    	}

    	return resp;
	}



    String encodeUrlPathSegment(String pathSegment,
            HttpServletRequest httpServletRequest) {
        String enc = httpServletRequest.getCharacterEncoding();
        if (enc == null) {
            enc = WebUtils.DEFAULT_CHARACTER_ENCODING;
        }
        try {
            pathSegment = UriUtils.encodePathSegment(pathSegment, enc);
        } catch (UnsupportedEncodingException uee) {
            uee.printStackTrace();
        }
        return pathSegment;
    }
    
    private void populateRewardstFilters( Model inUiModel ) {
        
        inUiModel.addAttribute(UI_ATTR_FILTER_REGISTERED_STORES, storeService.getAllStores());
//        inUiModel.addAttribute(UI_ATTR_FILTER_TXN_TYPES, PointTxnType.values());
        inUiModel.addAttribute(UI_ATTR_SEARCHFIELDS, EmployeeSearchCriteriaTwo.values());
        inUiModel.addAttribute(UI_ATTR_FILTER_STATUS, TxnStatusSearch.values());
        
    }
}
