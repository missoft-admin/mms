<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<sec:authorize access="isAuthenticated()">
  <c:redirect url="/"/>
</sec:authorize>
<!DOCTYPE html>
<!--[if lt IE 7]>
<html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>
<html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>
<html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!-->
<html class="no-js"> <!--<![endif]-->
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
  <spring:message code="application_name" var="app_name" htmlEscape="false"/>
  <title><spring:message code="welcome_h3" arguments="${app_name}"/></title>
  <meta name="description" content="Member Portal">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <!-- for non html5 compliant browsers -->
  <script src="<spring:url value="/js/modernizr-2.6.2.min.js" />"></script>

  <link href="<c:url value="/images/favicon.ico" />" rel="SHORTCUT ICON"/>
  <%--<link href='http://fonts.googleapis.com/css?family=Lato' rel='stylesheet' type='text/css'>--%>
  <link href="<c:url value="/css/bootstrap/bootstrap.css" />" rel="stylesheet">
  <link href="<c:url value="/css/bootstrap/bootstrap-responsive.css" />" rel="stylesheet">
  <link href="<c:url value="/css/custom.css" />" rel="stylesheet">
  <link href="<c:url value="/css/anonymous.css" />" rel="stylesheet">

  <script src="<c:url value="/js/jquery/jquery-1.10.2.min.js" />"></script>
  <script src="<c:url value="/js/jquery-cookie-master/jquery.cookie.js" />"></script>
  <script>
      function dontshow(){
          $.cookie('visited', 'yes', { expires: 30 }); // Set the cookie.
      }
  </script>
  <script>
      $(document).ready(function() {
          var visited = $.cookie('visited');
          if (visited == 'yes') {
              $("#notificationAlerts").hide();
          } else {
              $("#notificationAlerts").show(); 
              // cookie has no value, so show the text
          }
          
          $('#btnDontShow').click(function(e){
            $("#notificationAlerts").hide();
            dontshow();
          });
        
           
      });
  
  </script>
  <style>
  @font-face {
    font-family: 'Lato';
    font-style: normal;
    font-weight: 300;
    src: local('Lato Light'), local('Lato-Light'), url(/css/fonts/kcf5uOXucLcbFOydGU24WALUuEpTyoUstqEm5AMlJo4.woff) format('woff');
  }
  @font-face {
    font-family: 'Lato';
    font-style: normal;
    font-weight: 400;
    src: local('Lato Regular'), local('Lato-Regular'), url(/css/fonts/qIIYRU-oROkIk8vfvxw6QvesZW2xOQ-xsNqO47m55DA.woff) format('woff');
  }
  @font-face {
    font-family: 'Lato';
    font-style: normal;
    font-weight: 700;
    src: local('Lato Bold'), local('Lato-Bold'), url(/css/fonts/qdgUG4U09HnJwhYI-uK18wLUuEpTyoUstqEm5AMlJo4.woff) format('woff');
  }
  @font-face {
    font-family: 'Lato';
    font-style: normal;
    font-weight: 900;
    src: local('Lato Black'), local('Lato-Black'), url(/css/fonts/G2uphNnNqGFMHLRsO_72ngLUuEpTyoUstqEm5AMlJo4.woff) format('woff');
  }
    .body-container {
      max-width: 300px;
    }

    .form-signin .form-signin-heading,
    .form-signin .checkbox {
      margin-bottom: 10px;
    }

    h3.form-signin-heading {
      color: #16aeec;
    }

    .form-signin input[type="text"],
    .form-signin input[type="password"] {
      font-size: 16px;
      height: auto;
      margin-bottom: 15px;
      padding: 7px 9px;
    }
    .nav > li > a.registration-top {
      display: inline-table;
      padding-right: 0;
      padding-left: 0;
    }
    .login .navbar-inner {
      background-image: none;
      box-shadow: 0 0 20px #FFF;
      opacity: 0.1;
    }
    .login #notificationAlerts .btn {
      float: right;
      margin-top: 20px;
    }
    .alert-down-button {

    }
    .brower-news {
      padding: 20px 20px 70px;
      margin-top: 20px;
    }
    
  </style>
</head>
<body class="login">
  

<div class="navbar navbar-static-top">
  <div class="navbar-inner">
    <div class="container">
      <ul class="nav pull-right">
        <li>
          <%-- Not yet a member? <a href="<spring:url value="/member/registration" />" class="registration-top">
            <spring:message code="security.signup"/>.
          </a>  --%>
        </li>
      </ul>
    </div>
  </div>
</div>


  
<div class="container">
  <div id="notificationAlerts" class="hide alert alert-info brower-news">
  <div id="notificationAlertsContent" >
    We would like to remind you of the Carrefour Apps browser support policy. We support the latest version as well as the prior major release of Chrome, Firefox, Internet Explorer and Safari on a rolling basis. Each time a new version of one of these browsers is released, we begin supporting the update and stop supporting the third-oldest version.
  </div>
  <button id="btnDontShow" type="button" class="btn btn-small">I understand. Don't display again.</button>
  </div>

  
  
  <%@ include file="/WEB-INF/views/common/messages.jsp" %>
  <div class="logo-login text-center">
    <img width="350" src="<c:url value="/images/logo-login.png" />">
  </div>
  <form class="form-signin body-container" action="<spring:url value="/login-action"/>" method="post">
    <h3 class="form-signin-heading"><spring:message code="security.login.title"/></h3>
    <c:if test="${not empty param.login_error}">
      <div class="alert alert-error">
        <button type="button" class="close" data-dismiss="alert">&times;</button>
        <spring:message code="security.login.unsuccessful"/>
        <spring:message code="${SPRING_SECURITY_LAST_EXCEPTION.message}"/>
      </div>
    </c:if>
    <input type="text" class="input-block-level" placeholder="Username" name="username">
    <input type="password" class="input-block-level" placeholder="Password" name="password">
      <%--<label class="checkbox capitalize">
      <input type="checkbox" value="remember-me"> Remember me
      </label>--%>
    <button type="submit" class="btn btn-block btn-primary"><spring:message code="security.login"/></button>
    <p class="text-center">
      <%-- <a href="<spring:url value="/member/registration" />"><spring:message code="security.signup"/></a>
      &nbsp;<spring:message code="security.join.message"/>
      <br/> --%>
      <a href="<spring:url value="/member/password/reset"/>"><spring:message code="security.forgot.password"/></a>.
    </p>
  </form>
</div>
<div id="footer">
  <p>
    Version <spring:message code="project.version" />
    <spring:message var="commitId" code="git.commit.id.abbrev" />
    <c:if test="${!empty commitId}">
    &nbsp;rev. ${commitId}
  <div style="display: none;">
    Branch: <spring:message code="git.branch" />
    <br/>
    Last commit ID: <spring:message code="git.commit.id" />
    <br/>
    Last commit author: <spring:message code="git.commit.user.name" />
    <br/>
    Last commit time: <spring:message code="git.commit.time" />
    <br/>
    Last commit message: <spring:message code="git.commit.message.full" />
  </div>
  </c:if>
  </p>
</div>
<style type="text/css">
  #footer {
    text-align: center;
    clear: both;
    color: #444;
  }
</style>

<!-- /container -->
<!-- Bootstrap core JavaScript
================================================== -->
<!-- Placed at the end of the document so the pages load faster -->
<script src="<c:url value="/js/bootstrap/bootstrap.js" />"></script>
</body>
</html>