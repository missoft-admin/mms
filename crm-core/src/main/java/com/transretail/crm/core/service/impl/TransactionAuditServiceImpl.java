package com.transretail.crm.core.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.transretail.crm.core.entity.TransactionAudit;
import com.transretail.crm.core.repo.TransactionAuditRepo;
import com.transretail.crm.core.service.TransactionAuditService;

@Service("transactionAuditService")
@Transactional
public class TransactionAuditServiceImpl implements TransactionAuditService {

	
	@Autowired
	TransactionAuditRepo transactionAuditRepo;
	
	@Override
	public void saveTransactionAudits(List<TransactionAudit> audits) {
		
		transactionAuditRepo.save(audits);
		
	}

    @Override
    public List<TransactionAudit> retrieveAuditsByTransactionNo(String transactionNo) {

        return transactionAuditRepo.findByTransactionNo(transactionNo);
    }

    @Override
    public TransactionAudit saveTransactionAudit(TransactionAudit audit) {

        return transactionAuditRepo.saveAndFlush(audit);
    }


}
