$(document).ready(function() {
	$list = $("#list_voucherScheme");
	
	initDataTable();

	function initDataTable() {
		
		var isHelpDesk = $list.data("roleHelpdesk");
		  
	  var headers = { 
	    valueFrom: $list.data("hdrValueFrom"),
	    valueTo: $list.data("hdrValueTo"),
	    amount: $list.data("hdrAmount"),
	    validPeriod: $list.data("hdrValidPeriod"),
	    status: $list.data("hdrStatus")
	  };
	  
	  var columnHeaders = [ 
  	         headers['valueFrom'],
	         headers['valueTo'],
	         headers['amount'],
	         headers['validPeriod'],
	         headers['status']
	    ];
	  
	  var modelFields = [
    	     {name : 'valueFrom', sortable : true, fieldCellRenderer : function (data, type, row) {
  	           return row.valueFromFormatted;
           }, alignment : 'right'},
  	     {name : 'valueTo', sortable : true, fieldCellRenderer : function (data, type, row) {
  	           return row.valueToFormatted;
           }, alignment : 'right'},
  	     {name : 'amount', sortable : true, fieldCellRenderer : function (data, type, row) {
  	           return row.amountToFormatted;
           }, alignment : 'right'},
  	     {name : 'validPeriod', sortable : true, alignment : 'right'},
  	     {name : 'status', sortable : true}
  	     
  	     ];
	  
	  if(!isHelpDesk) {
		  columnHeaders.push("");
		  modelFields.push({customCell : function ( innerData, sSpecific, json ) {
	  	    	 if ( sSpecific == 'display' ) {
	   		        var rg = new RegExp("%ITEMID%", 'g');
	   	        	var btnHtml1 = $("#updateBtn").html();
	         		   	btnHtml1 = btnHtml1.replace(rg, json.id);

	   	        	var btnHtml2 = $("#deleteBtn").html();
	         		   	btnHtml2 = btnHtml2.replace(rg, json.id);

	         		   	return btnHtml1 + btnHtml2;
	   	          } else {
	   	            return '';
	   	          }
	   	     	}
	   	     });
	  }
		  
	  
    $list.ajaxDataTable({
	    'autoload'  : true,
	    'ajaxSource' : $list.data("url"),
	    'columnHeaders' : columnHeaders,
	    'modelFields' : modelFields
	  });
	}
});