package com.transretail.crm.impl;

import junit.framework.Assert;

import org.joda.time.LocalDate;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.google.common.collect.Lists;
import com.transretail.crm.common.service.dto.response.ResultList;
import com.transretail.crm.giftcard.dto.SalesOrderDto;
import com.transretail.crm.giftcard.dto.SalesOrderSearchDto;
import com.transretail.crm.giftcard.entity.QSalesOrder;
import com.transretail.crm.giftcard.entity.SalesOrder;
import com.transretail.crm.giftcard.entity.support.SalesOrderStatus;
import com.transretail.crm.giftcard.entity.support.SalesOrderType;
import com.transretail.crm.giftcard.repo.SalesOrderRepo;
import com.transretail.crm.giftcard.service.SalesOrderService;

/**
 *
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath*:/META-INF/spring/applicationContext*.xml",
    "classpath:META-INF/spring/applicationContext-test.xml"})
public class SalesOrderServiceTest {
	
	@Autowired
	private SalesOrderService service;
	@Autowired
	private SalesOrderRepo repo;
	
	private SalesOrder order;
	
	@Before
	public void setup() {
		order = new SalesOrder();
		order.setOrderNo("BBMMS0501001");
		order.setOrderDate(new LocalDate());
		order.setStatus(SalesOrderStatus.DRAFT);
		order.setOrderType(SalesOrderType.B2B_SALES);
		order = repo.save(order);
	}
	
	@After
	public void teardown() {
		repo.deleteAll();
	}
	
	@Test
	public void saveOrderTest() {
		SalesOrderDto orderDto = new SalesOrderDto();
		orderDto.setOrderDate(new LocalDate());
		orderDto.setStatus(SalesOrderStatus.DRAFT);
		service.saveOrder(orderDto);
		Assert.assertTrue(repo.count() == 2);
	}

	@Test
	public void getSalesOrdersTest() {
		ResultList<SalesOrderDto> result = service.getSalesOrders(new SalesOrderSearchDto());
		Assert.assertTrue(result.getNumberOfElements() == 1);
		SalesOrderDto orderDto = Lists.newArrayList(result.getResults()).get(0);
		Assert.assertTrue(orderDto.getOrderNo().equals("BBMMS0501001"));
	}
	@Test
	public void getOrderTest() {
		SalesOrderDto orderDto = service.getOrder(order.getId());
		Assert.assertNotNull(orderDto);
		Assert.assertEquals(orderDto.getOrderNo(), "BBMMS0501001");
	}
	@Test
	public void deleteOrderTest() {
		Assert.assertTrue(repo.count() == 1);
		service.deleteOrder(order.getId());
		Assert.assertTrue(repo.count() == 0);
	}
	@Test
	public void approveOrderTest() {
		Assert.assertTrue(repo.count(QSalesOrder.salesOrder.status.eq(SalesOrderStatus.APPROVED)) == 0);
		service.approveOrder(order.getId(), SalesOrderStatus.APPROVED);
		Assert.assertTrue(repo.count(QSalesOrder.salesOrder.status.eq(SalesOrderStatus.APPROVED)) == 1);
	}
	
}

/*
 * 
 * 	void saveOrder(SalesOrderDto orderDto);

	ResultList<SalesOrderDto> getSalesOrders(SalesOrderSearchDto searchForm);

	SalesOrderDto getOrder(Long id);

	void deleteOrder(Long id);

	void approveOrder(Long id, SalesOrderStatus status);
 * 
 * 
 */


