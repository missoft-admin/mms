package com.transretail.crm.giftcard.entity;

import java.io.Serializable;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.Type;
import org.joda.time.LocalDate;

import com.transretail.crm.core.entity.UserModel;
import com.transretail.crm.core.entity.lookup.Store;
import com.transretail.crm.core.entity.support.CustomAuditableEntity;
import com.transretail.crm.giftcard.entity.support.StockRequestStatus;

/**
 *
 */
@Entity
@Table(name = "CRM_STOCK_REQUEST")
public class StockRequest extends CustomAuditableEntity<Long> implements Serializable {
    private static final long serialVersionUID = 835994596874668336L;
    
    @Column(name="REQUEST_NO", length = 12)
    private String requestNo;
    
    @Column(name="REQUEST_DATE")
    @Temporal(TemporalType.DATE)
    @Type(type = "org.jadira.usertype.dateandtime.joda.PersistentLocalDate")
    private LocalDate requestDate;
    
    @Column(name="SOURCE_LOCATION")
    private String sourceLocation;
    
    @Column(name="ALLOCATE_TO")
	private String allocateTo;
	
	@Column(name="QUANTITY")
	private Integer quantity;
	
	@Column(name="PRODUCT_CODE")
	private String productCode;
	
	@Column(name="STATUS")
	@Enumerated(EnumType.STRING)
	private StockRequestStatus status;
	
	@Column(name="TRANSFER_REF_NO")
	private String transferRefNo;
	
	@Column(name="RECEIVE_BY")
	private String receiveBy;
    
	@Column(name="QUANTITY_TRANSFERRED")
	private Integer quantityTransferred;
	
	@Column(name="QUANTITY_RESERVED")
	private Integer quantityReserved;
	
	@Column(name="RESERVED_SERIES")
	private String reservedSeries;

    @Column(name = "IS_FOR_PROMO", length = 1)
    @Type(type = "yes_no")
    private Boolean isForPromo;
	
	public String getRequestNo() {
		return requestNo;
	}

	public void setRequestNo(String requestNo) {
		this.requestNo = requestNo;
	}

	public LocalDate getRequestDate() {
		return requestDate;
	}

	public void setRequestDate(LocalDate requestDate) {
		this.requestDate = requestDate;
	}

	public String getSourceLocation() {
		return sourceLocation;
	}

	public void setSourceLocation(String sourceLocation) {
		this.sourceLocation = sourceLocation;
	}

	public String getAllocateTo() {
		return allocateTo;
	}

	public void setAllocateTo(String allocateTo) {
		this.allocateTo = allocateTo;
	}

	public Integer getQuantity() {
		return quantity;
	}

	public void setQuantity(Integer quantity) {
		this.quantity = quantity;
	}

	public String getProductCode() {
		return productCode;
	}

	public void setProductCode(String productCode) {
		this.productCode = productCode;
	}

	public StockRequestStatus getStatus() {
		return status;
	}

	public void setStatus(StockRequestStatus status) {
		this.status = status;
	}

	public String getTransferRefNo() {
		return transferRefNo;
	}

	public void setTransferRefNo(String transferRefNo) {
		this.transferRefNo = transferRefNo;
	}

    public String getReceiveBy() {
        return receiveBy;
    }

    public void setReceiveBy(String receiveBy) {
        this.receiveBy = receiveBy;
    }

    public Integer getQuantityTransferred() {
        return quantityTransferred;
    }

    public void setQuantityTransferred(Integer quantityTransferred) {
        this.quantityTransferred = quantityTransferred;
    }

	public Integer getQuantityReserved() {
		return quantityReserved;
	}

	public void setQuantityReserved(Integer quantityReserved) {
		this.quantityReserved = quantityReserved;
	}

	public String getReservedSeries() {
		return reservedSeries;
	}

	public void setReservedSeries(String reservedSeries) {
		this.reservedSeries = reservedSeries;
	}

	public Boolean getIsForPromo() {
		return isForPromo;
	}

	public void setIsForPromo(Boolean isForPromo) {
		this.isForPromo = isForPromo;
	}
	
}
