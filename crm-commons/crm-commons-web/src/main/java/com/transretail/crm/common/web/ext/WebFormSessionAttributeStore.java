package com.transretail.crm.common.web.ext;

import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.joda.time.DateTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.support.DefaultSessionAttributeStore;
import org.springframework.web.bind.support.SessionAttributeStore;
import org.springframework.web.context.request.WebRequest;

import com.transretail.crm.common.web.dto.WebForm;

/**
 * @author aco
 */
public class WebFormSessionAttributeStore implements SessionAttributeStore {

    private static final transient Logger _LOG = LoggerFactory.getLogger(WebFormSessionAttributeStore.class);

    private SessionAttributeStore backingStore = new DefaultSessionAttributeStore();

    // -1 means there is no limit, default value set in webapp context is 20
    // we're only storing web forms
    private int webFormStoredAttributeLimit = -1;

    private final List<String> attributeNames = Collections.synchronizedList(new LinkedList<String>());

    @Override
    public void storeAttribute(WebRequest request, String attrName, Object attrValue) {

        if (attrValue instanceof WebForm) {
            WebForm webForm = (WebForm) attrValue;
            _LOG.debug("Storing web form attribute to backing store: request={}, attrName=[{}], webForm.info=[{}], webForm.class={}",
                request, attrName, webForm.getWebFormInfo(), webForm.getClass());

            // Update the last access of the web form to keep track of LRUs
            updateLastAccessed(webForm);

            // Store the web form id in the REQUEST scope only, so the page can correlate which web form
            // in session is attached to the current page request
            String webFormIdName = WebFormSessionAttributeStoreUtil.createWebFormIdName(attrName);

            request.setAttribute(webFormIdName, webForm.getWebFormInfo().getId(), WebRequest.SCOPE_REQUEST);

            // Store web form using the web form id instead of the attribute name
            backingStore.storeAttribute(request, webForm.getWebFormInfo().getId(), webForm);

            // clean up oldest attribute stored if limit is set and reached
            if (webFormStoredAttributeLimit != -1 && attributeNames.size() == webFormStoredAttributeLimit) {
                _LOG.debug("Stored attribute limit reached. Removing oldest stored attribute.");

                cleanupAttribute(request, attributeNames.get(0));
            }

            _LOG.debug("Adding web form attribute '{}' to the end of the stored attributes stack.", webFormIdName);

            attributeNames.add(webFormIdName);

        } else {
            _LOG.debug("Storing object attribute to backing store: request={}, attrName=[{}], attrValue.class={}", request, attrName,
                attrValue == null ? null : attrValue.getClass());
            backingStore.storeAttribute(request, attrName, attrValue);
        }
    }

    @Override
    public Object retrieveAttribute(WebRequest request, String attrName) {
        // Retrieve the web form id if available via request parameter
        String webFormId = findWebFormId(request, attrName);

        if (StringUtils.isNotEmpty(webFormId)) {
            WebForm result = (WebForm) backingStore.retrieveAttribute(request, webFormId);
            _LOG.debug("Retrieving web form attribute: request={}, attrName=[{}], webFormId=[{}], result={}", request, attrName,
                webFormId, result == null ? null : result.getClass());

            // Update the last access of the web form to keep track of LRUs
            updateLastAccessed(result);

            _LOG.debug("Moving web form attribute '{}' to the end of the stored attributes stack.", webFormId);

            // indicates that the web form is still being used?
            // reset (or push at the end of the list) in the tracked attribute names
            attributeNames.remove(webFormId);
            attributeNames.add(attrName);

            return result;

        } else {
            Object result = backingStore.retrieveAttribute(request, attrName);
            _LOG.debug("Retrieving object attribute: request={}, attrName=[{}], result={}", request, attrName,
                result == null ? null : result.getClass());

            return result;
        }
    }

    @Override
    public void cleanupAttribute(WebRequest request, String attrName) {
        // Retrieve the web form id if available via request parameter
        String webFormId = findWebFormId(request, attrName);

        if (StringUtils.isNotEmpty(webFormId)) {
            _LOG.debug("Cleaning web form attribute: request={}, attrName=[{}], webFormId=[{}]",
                new Object[]{request, attrName, webFormId});
            backingStore.cleanupAttribute(request, webFormId);

            _LOG.debug("Removing web form attribute '{}' from the stored attributes stack.", webFormId);

            // remove from the tracked attribute names
            attributeNames.remove(webFormId);
        } else {
            _LOG.debug("Cleaning object attribute: request={}, attrName=[{}",
                new Object[]{request, attrName});
            backingStore.cleanupAttribute(request, attrName);
        }
    }

    public void setBackingStore(SessionAttributeStore backingStore) {
        this.backingStore = backingStore;
    }

    public void setWebFormStoredAttributeLimit(int webFormStoredAttributeLimit) {
        this.webFormStoredAttributeLimit = webFormStoredAttributeLimit;
    }

    private String findWebFormId(WebRequest request, String attrName) {
        // Find the hidden field in the form that stores the conversation id of the web form
        return request.getParameter(WebFormSessionAttributeStoreUtil.createWebFormIdName(attrName));
    }

    private void updateLastAccessed(WebForm webForm) {
        if (webForm != null) {
            webForm.getWebFormInfo().setLastAccessed(new DateTime());
        }
    }
}
