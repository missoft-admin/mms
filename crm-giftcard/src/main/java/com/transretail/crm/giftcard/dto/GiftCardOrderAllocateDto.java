package com.transretail.crm.giftcard.dto;

import org.hibernate.validator.constraints.NotEmpty;
import org.joda.time.LocalDate;

import com.fasterxml.jackson.annotation.JsonIgnore;

/**
 * @author Allan G. Ramirez (agramirez@exist.com)
 */
public class GiftCardOrderAllocateDto {
    private LocalDate expiryDate;
    /**
     * Store to allocate to
     */
    @NotEmpty
    private String allocateToCode;
    @NotEmpty
    private String allocateToDesc;
    /**
     * Series number should conform to format: yyMMddbbfsssssss
     */
    @NotEmpty
    private String seriesFrom;
    /**
     * Series number should conform to format: yyMMddbbfsssssss
     */
    @NotEmpty
    private String seriesTo;

    public LocalDate getExpiryDate() {
        return expiryDate;
    }

    public void setExpiryDate(LocalDate expiryDate) {
        this.expiryDate = expiryDate;
    }

    public String getAllocateToCode() {
        return allocateToCode;
    }

    public void setAllocateToCode(String allocateToCode) {
        this.allocateToCode = allocateToCode;
    }

    public String getAllocateToDesc() {
        return allocateToDesc;
    }

    public void setAllocateToDesc(String allocateToDesc) {
        this.allocateToDesc = allocateToDesc;
    }

    public void setSeriesFrom(String seriesFrom) {
        this.seriesFrom = seriesFrom;
    }

    public void setSeriesTo(String seriesTo) {
        this.seriesTo = seriesTo;
    }

    public String getSeriesFrom() {
        return seriesFrom;
    }

    public String getSeriesTo() {
        return seriesTo;
    }

    @JsonIgnore
    public GiftCardSeriesDto getSeriesFromDto() {
        return new GiftCardSeriesDto(seriesFrom);
    }

    @JsonIgnore
    public GiftCardSeriesDto getSeriesToDto() {
        return new GiftCardSeriesDto(seriesTo);
    }
}
