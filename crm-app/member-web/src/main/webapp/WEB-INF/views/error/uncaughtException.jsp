<%@ include file="../taglibs.jsp" %>

<h2><spring:message code="error_uncaughtexception_title"/></h2>

<p>
  <spring:message code="error_uncaughtexception_problemdescription"/>
</p>
<c:if test="${not empty exception}">
  <p>
  <h4>
    <spring:message code="exception_details"/>
  </h4>

  <div>
    <c:out value="${exception.localizedMessage}"/>
  </div>
  <div>
    <c:forEach items="${exception.stackTrace}" var="trace">
      <c:out value="${trace}"/>
      <br/>
    </c:forEach>
  </div>
  </p>
</c:if>
</div>