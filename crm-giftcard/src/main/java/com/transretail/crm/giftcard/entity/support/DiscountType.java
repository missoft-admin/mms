package com.transretail.crm.giftcard.entity.support;

/**
 *
 */
public enum DiscountType {
	
	/*Regular Yearly Discount, SO Yearly Discount, Yearly Discount Combination*/
	REGULAR_YEARLY_DISCOUNT, SO_YEARLY_DISCOUNT, YEARLY_DISCOUNT_COMBO
}
