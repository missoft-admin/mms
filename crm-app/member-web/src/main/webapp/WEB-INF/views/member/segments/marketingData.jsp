<%@ include file="../../taglibs.jsp"%>


    <div class="tab-pane" id="marketing">

		  <!-- Informed About -->
		  <fieldset class="form-horizontal container-fluid">
		    <legend><spring:message code="member_indi_informedabout" /></legend>
        <c:forEach items="${informedAboutCodes}" var="item">
         <label class="checkbox"><form:checkbox path="informedAbout" data-text="${item.description}"  value="${item.code}" />${item.description}
         <c:if test="${item.description == 'OTHERS'}">&nbsp;&nbsp;<input type="text" name="informedAbout" class="informedAboutTxt" disabled="disabled" value="${form.informedAboutOthers}" /></c:if></label>
        </c:forEach>
		  </fieldset>


      <!-- Visited Supermarkets -->
      <fieldset class="form-horizontal container-fluid">
        <legend><spring:message code="member_indi_visitregularly" /></legend>
        <label class="checkbox"><form:checkboxes path="visitedSupermarkets" items="${visitedSupermarkets}" itemLabel="description" itemValue="code"/></label>
      </fieldset>


      <!-- Grocery Trips To Carrefour -->
      <fieldset class="form-horizontal container-fluid">
        <legend><spring:message code="member_indi_tripsto" /></legend>
        <form:input path="tripsToStore" cssClass="wholeInput" />
      </fieldset>


      <!-- Interested Products -->
      <fieldset class="form-horizontal container-fluid">
        <legend><spring:message code="member_indi_interestedproducts" /></legend>
        <c:forEach items="${interestedProductsCodes}" var="item">
          <label class="checkbox">
            <form:checkbox path="interestedProducts" value="${item.code}" />${item.description}
          </label>
        </c:forEach>
      </fieldset>


		  <!-- Interested Activities -->
		  <fieldset class="form-horizontal container-fluid">
		    <legend><spring:message code="member_indi_interestedactivities" /></legend>
		    <label class="checkbox"><form:checkboxes path="interestedActivities" items="${interestedActivities}" itemLabel="description" itemValue="code"/></label>
		  </fieldset>


    </div>