$(document).ready( function() {

	var $theForm = $( "#form_adjustment" );



	initFormFields();
	initLinks();



	function initFormFields() {

	  $( "#searchField" ).change( function(e) {
      e.preventDefault();
      $( "#searchValue" ).attr( "name", $( this ).val() );
    }).change();
    
    $("#transactionDate").datepicker({
      autoclose : true,
      format : "dd-mm-yyyy",
    }).on('changeDate', function(selected){
      transactionDate = new Date(selected.date.valueOf());
      transactionDate.setDate(transactionDate.getDate(new Date(selected.date.valueOf())));
    });
	} 

	function initLinks() {

		var theLinks_create = $( ".link_createAdjustment" );
		$.each( theLinks_create, function( idx ) {
			$( this ).click( function(e) {
				e.preventDefault();
				$( "#content_adjustmentForm" ).find( "#label_accountId" ).html( $( this ).data( "label-account-id" ) );
				$( "#content_adjustmentForm" ).find( "#adjustmentForm" ).attr( "action", $( this ).data( "action" ) );
				$( "#content_adjustmentForm" ).modal( "show" );
				/*create( $theLink_create.data( "url" ) );*/
			});
		});
	}



	function create( inUrl ) {
		$.ajax({
			cache 		: false, 
			dataType 	: "html",
			type 		: "GET", 
			url 		: inUrl,
			error 		: function(jqXHR, textStatus, errorThrown) {}, 
			success 	: function( inResponse ) {
				$theForm.html( inResponse );
			}
		});
	}

});



