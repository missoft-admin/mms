package com.transretail.crm.common.web.client;

import java.util.Iterator;

import org.springframework.beans.factory.InitializingBean;
import org.springframework.http.client.ClientHttpRequestFactory;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * @author Allan G. Ramirez (agramirez@exist.com)
 */
public class CrmRestTemplate extends RestTemplate implements InitializingBean {
    private ObjectMapper objectMapper;

    public CrmRestTemplate() {
        super();
    }

    public CrmRestTemplate(ClientHttpRequestFactory requestFactory) {
        this();
        setRequestFactory(requestFactory);
    }

    @Override
    public void afterPropertiesSet() throws Exception {
        if (objectMapper != null) {
            Iterator<HttpMessageConverter<?>> it = getMessageConverters().iterator();
            while (it.hasNext()) {
                HttpMessageConverter converter = it.next();
                if (converter instanceof MappingJackson2HttpMessageConverter) {
                    ((MappingJackson2HttpMessageConverter) converter).setObjectMapper(objectMapper);
                }
            }
        }
    }

    public void setObjectMapper(ObjectMapper objectMapper) {
        this.objectMapper = objectMapper;
    }
}
