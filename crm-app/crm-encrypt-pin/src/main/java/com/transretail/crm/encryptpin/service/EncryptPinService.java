package com.transretail.crm.encryptpin.service;

/**
 * @author Allan G. Ramirez (agramirez@exist.com)
 */
public interface EncryptPinService {
    public void encryptAllPins() throws Exception;
}
