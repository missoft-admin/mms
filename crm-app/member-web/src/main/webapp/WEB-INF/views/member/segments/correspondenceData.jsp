<%@ include file="../../taglibs.jsp"%>


    <div class="tab-pane" id="correspondence">
    <fieldset class="container-fluid">
      <div class="">
        <div class="row-fluid">
          <fieldset class="">
            <legend><spring:message code="correspondence_address" />:</legend>
            <c:forEach items="${correspondenceAddressCodes}" var="item">
              <label class="radio">
                <form:radiobutton path="correspondenceAddress" value="${item.code}"/>${item.description}
              </label>
            </c:forEach>
          </fieldset>

		      <fieldset class="" style="margin-top: 20px">
		        <legend class=""><spring:message code="alert_by" />:</legend>
	          <label class="checkbox"> <form:checkbox path="alertBySms" /><spring:message code="alertby.sms" /></label> 
	          <label class="checkbox" id="alertEmail"> <form:checkbox path="alertByEmail" /> <spring:message code="email" /></label>

            <%-- <sec:authorize var="isEmployee" ifAnyGranted="MTYP002" />
	          <c:if test="${!isEmployee}"><label class="checkbox"> <form:checkbox path="alertByMail" /><spring:message code="mail" /></label></c:if> --%>
	          <label class="checkbox"> <form:checkbox path="alertByPhone" /> <spring:message code="phone" /></label>
		      </fieldset>

          <fieldset class="" style="margin-top: 20px">
            <legend><spring:message code="social_media_accts" />:</legend>
				    <div id="socialMediaAccts" class="checkbox">
				      <c:forEach items="${socialMedia}" var="item" varStatus="stat">
	              <form:checkbox path="socialMediaAccts" value="${item.code}" label="${item.description}" data-value="${item.code}" cssClass="checkboxFix" />
	              <c:if test="${stat.last}"><c:set var="lastId" value="${stat.index}" /></c:if>
	            </c:forEach>
              <form:input path="socialMediaAccts" id="socialMediaAcctsOthersVal" cssClass="hide input-small"/>
				    </div>

						<script>
						$(document).ready( function() {
						    var $socialMedia = $( "#socialMediaAccts" );
						    var $others = $socialMedia.find( "#socialMediaAccts${lastId + 1}" );
						    var $othersVal = $socialMedia.find( "#socialMediaAcctsOthersVal" );
						  
						    initFields();
						  
						    function initFields() {
						        $others.click( function(e) {
						            if ( this.checked ) { $othersVal.show(); }
						            else {
						                $othersVal.val("");
						                $othersVal.hide();
						            }
						        });
						        if ( $othersVal.val() && $othersVal.val().indexOf( $others.data( "value" ) ) != -1 ) {
						        	$othersVal.val( $othersVal.val().substring( $othersVal.val().lastIndexOf( "," ) + 1 ) );
						        	$othersVal.show();
						        }
						        else {
						        	$othersVal.val( "" );
						        }
						    }
						});
						</script>
          </fieldset>

        </div>
      </div>
    </fieldset>
    </div>