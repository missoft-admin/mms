package com.transretail.crm.schedule.job;

import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.argThat;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.reset;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.Arrays;
import java.util.Locale;
import java.util.Set;

import org.joda.time.DateTimeZone;
import org.joda.time.LocalDate;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentMatcher;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;

import com.google.common.collect.Sets;
import com.transretail.crm.core.dto.MessageDto;
import com.transretail.crm.core.service.UserRolePermissionService;
import com.transretail.crm.giftcard.dto.GcInventorySafetyStockDto;
import com.transretail.crm.giftcard.service.SafetyStockService;
import com.transretail.crm.media.service.MailService;

/**
 * @author Allan G. Ramirez (agramirez@exist.com)
 */
@RunWith(MockitoJUnitRunner.class)
public class SafetyStockJobTest {
    @Mock
    private SafetyStockService safetyStockService;
    @Mock
    private MessageSource messageSource;
    @Mock
    private MailService mailService;
    @Mock
    private UserRolePermissionService userRolePermissionService;
    @InjectMocks
    private SafetyStockJob safetyStockJob = new SafetyStockJob();

    @Before
    public void doSetup() {
        reset(safetyStockService);
        reset(messageSource);
        reset(mailService);
        reset(userRolePermissionService);
    }

    @Test
    public void executeInternal() throws Exception {
        GcInventorySafetyStockDto gcDto = new GcInventorySafetyStockDto();
        gcDto.setProductCode("code");
        gcDto.setProductDesc("desc");
        gcDto.setInventoryCount(4);
        gcDto.setSafetyStockQty(5);
        gcDto.setCurrMonth("April");
        gcDto.setNextMonth("May");

        LocalDate april = LocalDate.now().withMonthOfYear(4);
        SafetyStockJob spy = spy(safetyStockJob);
        when(spy.getCurrentDate()).thenReturn(april);

        when(safetyStockService.checkSafetyStock(april)).thenReturn(Arrays.asList(gcDto));

        final Set<String> emailAddresses = Sets.newHashSet("x@cloud.com", "xcloudx@xcloudx.com");
        when(userRolePermissionService.getMarchantServiceSupervisorEmails()).thenReturn(emailAddresses);

        final String expectedSubject = "Product below safety stock";

        Locale locale = Locale.ENGLISH;
        LocaleContextHolder.setLocale(locale);
        when(messageSource.getMessage("safetystock.belowstock.email.subject", null, locale)).thenReturn(expectedSubject);
        when(messageSource.getMessage(eq("safetystock.belowstock.email.message.template"),
            argThat(new ArgumentMatcher<Object[]>() {
                @Override
                public boolean matches(Object argument) {
                    String[] argArr = (String[]) argument;
                    return argArr[0].equals("tempProductDesc") && argArr[1].equals("tempCurrMonth") && argArr[2].equals("tempCount")
                        && argArr[3].equals("tempNextMonth") && argArr[4].equals("tempSafetyStockQty");
                }
            }),
            eq(locale)
        )).thenReturn("Current stock of tempProductDesc product in head office of tempCurrMonth month is tempCount " +
            "but safety stock for tempNextMonth month is tempSafetyStockQty.");

        spy.executeInternal(null);

        verify(mailService).sendWithHtml(argThat(new ArgumentMatcher<MessageDto>() {
            @Override
            public boolean matches(Object argument) {
                MessageDto dto = (MessageDto) argument;
                boolean sameSubjectAndMessage = dto.getSubject().equals(expectedSubject) && dto.getMessage()
                    .equals("Current stock of desc product in head office of April month is 4 but safety stock for May month is 5.");
                boolean sameEmails = false;
                String[] emails = dto.getRecipients();
                String[] expectedEmails = emailAddresses.toArray(new String[emailAddresses.size()]);
                if (emails.length == emailAddresses.size()) {

                    for (int i = 0; i < emailAddresses.size(); i++) {
                        sameEmails = emails[i].equals(expectedEmails[i]);
                        if (!sameEmails) {
                            break;
                        }
                    }
                }
                return sameSubjectAndMessage && sameEmails;
            }
        }));
    }

    @Test
    public void getCurrentDate() {
        assertEquals(LocalDate.now(DateTimeZone.getDefault()), safetyStockJob.getCurrentDate());
    }
}
