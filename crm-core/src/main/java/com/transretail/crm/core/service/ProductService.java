package com.transretail.crm.core.service;


import java.util.List;

import com.transretail.crm.core.dto.ProductDto;
import com.transretail.crm.core.dto.ProductSearchDto;
import com.transretail.crm.core.entity.lookup.Product;


public interface ProductService {

	Iterable<Product> getAllProducts();

	Iterable<Product> getProducts(Integer idx, Integer size);

	boolean doExist(ProductSearchDto searchDto);

	Product getByPluId(String pluId);

	Product searchOne(ProductSearchDto searchDto);

	List<ProductDto> search(ProductSearchDto searchDto);

	List<ProductDto> search(ProductSearchDto searchDto, long limit);

}
