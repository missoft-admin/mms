alter table CRM_GC_SERVICE_REQUEST 
  add FILED_IN number(10,0) null;

update crm_gc_service_request set FILED_IN = 1;

alter table CRM_GC_SERVICE_REQUEST 
     modify FILED_IN number(10,0) not null;

alter table CRM_GC_SERVICE_REQUEST 
    add constraint FK_65wn0arfskrhnjr2xqga4g08j 
    foreign key (FILED_IN) 
    references STORE;