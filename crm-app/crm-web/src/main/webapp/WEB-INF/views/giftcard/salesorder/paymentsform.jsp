<%@ include file="../../common/taglibs.jsp" %>

<style type="text/css">
<!--

  #productTable tr td:last-child {
    border-left: 0px;
  }

  #addProductBtn.btn-small {
    margin-bottom: 5px;
  }
  table select, textarea, table input[type="text"], input[type="password"], input[type="datetime"], input[type="datetime-local"], input[type="date"], input[type="month"], input[type="time"], input[type="week"], input[type="number"], input[type="email"], input[type="url"], input[type="search"], input[type="tel"], input[type="color"], .uneditable-input {
      margin-bottom: 0px;
      margin-top: 0px;
  }
  
-->
</style>



<c:set var="ENCTYPE" value="application/x-www-form-urlencoded"/>

<div class="modal-dialog">
  <div class="modal-content">
    
    <div class="modal-header">
      <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
      <h4><spring:message code="sales_order_so_payment_info" /></h4>
    </div>
 
    <div class="modal-body">
    
      <div id="contentError" class="hide alert alert-error">
        <button type="button" class="close" onclick="$(this).parent().hide();">&times;</button>
        <div><form:errors path="*"/></div>
      </div>
    
      
      <form:form id="paymentForm" name="paymentForm" modelAttribute="paymentForm" action="${pageContext.request.contextPath}/giftcard/salesorder/payments/save/" method="POST" enctype="${ENCTYPE}" cssClass="form-reset form-horizontal" >
      <form:hidden path="orderId"/>
      <form:hidden path="status"/>
      
      <div class="contentMain">
      
      <div class="row-fluid">
      
        <div class="span6">
        <div class="control-group">
          <spring:message code="sales_order_no" var="orderNum" />
          <label for="orderNo" class="control-label">${orderNum}</label> 
          <div class="controls">
            <form:input path="orderNo" class="form-control" placeholder="${orderNum}" readonly="true" />
          </div>
        </div>
        
        
        <div class="control-group">
          <spring:message code="sales_order_date" var="orderDt" />
          <label for="orderDate" class="control-label">${orderDt}</label> 
          <div class="controls">
            <form:input path="orderDate" class="form-control" placeholder="${orderDt}" readonly="true" />
          </div>
        </div>
        </div>
        
        <div class="span6">
        
        <div class="control-group">
          <spring:message code="sales_order_customer_name" var="contactPerson" />
          <label for="customerId" class="control-label">${contactPerson}</label> 
          <div class="controls">
            <form:input path="customerName" class="form-control" placeholder="${contactPerson}" readonly="true" />
          </div>
        </div>
        
        
        </div>
        
        <div class="clearfix"></div>
        
        <div class="span6">
        
        <div class="control-group">
          <label for="" class="control-label"><spring:message code="order_total_face_amount" /></label> 
          <div class="controls">
            <input type="text" value="<fmt:formatNumber pattern="#,##0" value="${orderForm.totalFaceAmount}" />" readonly="readonly" />
          </div>
        </div>
        
        <div class="control-group">
          <label for="" class="control-label"><spring:message code="sales_order_discount_amount" /></label> 
          <div class="controls">
            <input type="text" value="<fmt:formatNumber pattern="#,##0" value="${orderForm.discountAmt}" />" readonly="readonly" >
          </div>
        </div>
        
        
        <div class="control-group">
          <label for="" class="control-label"><spring:message code="sales_order_total_card_fee" /></label> 
          <div class="controls">
            <input type="text" value="<fmt:formatNumber pattern="#,##0" value="${orderForm.totalCardFee}" />" readonly="readonly" />
          </div>
        </div>
        
        <div class="control-group">
          <label for="" class="control-label"><spring:message code="gc.so.delivery.shippingfee" /></label> 
          <div class="controls">
            <input type="text" value="<fmt:formatNumber pattern="#,##0" value="${orderForm.shippingFee}" />" readonly="readonly" >
          </div>
        </div>
        
        
        
        
        
        
        </div>
        
        <div class="span6">
        
        
        
        <div class="control-group">
          <label for="" class="control-label"><spring:message code="sales_order_receivable" /></label> 
          <div class="controls">
            <input type="text" id="netAmt" value="<fmt:formatNumber pattern="#,##0" value="${orderForm.netAmt}" />" readonly="readonly" >
          </div>
        </div>
        
        
        
        
        <div class="control-group">
          <label for="" class="control-label"><spring:message code="sales_order_receivedamount" /></label> 
          <div class="controls">
            <input type="text" id="totalPayment" value="<fmt:formatNumber pattern="#,##0" value="${orderForm.totalPayment}" />" readonly="readonly" >
          </div>
        </div>
        
        
        <c:set value="${orderForm.netAmt - orderForm.totalPayment}" var="diffSo" />
        <div class="control-group">
          <label for="" class="control-label" id="diffLabel">
          <span id="positiveDiff" <c:if test="${diffSo < 0}">class="hide"</c:if>><spring:message code="sales_order_notreceived" /></span>
          <span id="negativeDiff" <c:if test="${diffSo >= 0}">class="hide"</c:if>><spring:message code="sales_order_overpayment" /></span>
          </label> 
          <div class="controls">
            <input type="text" id="diffSo" value="<fmt:formatNumber pattern="#,##0" value="${diffSo >= 0? diffSo : -diffSo}" />" readonly="readonly" >
          </div>
        </div>
        
        
        
        
        
        
        </div>
        
        
        
        <div class="clearfix"></div>
        
        
        <div class="span12">
        
        <div class="pull-left" style="margin-right: 5px;">
        <button type="button" id="addProductBtn" class="btn btn-small">+</button><br/>
        <button type="button" id="removeProductBtn" class="btn btn-small">&ndash;</button>
      </div>
      
        <div class="pull-left" style="width:90%;">
          <table id="productTable" class="table table-condensed table-compressed table-striped">
            <thead>
              <tr>
                <th></th>
                <th><b class="required">*</b> <spring:message code="sales_order_payment_type" /></th>
                <th><b class="required">*</b> <spring:message code="sales_order_payment_date" /></th>
                <th><b class="required">*</b> <spring:message code="sales_order_payment_amount" /></th>
                <th><spring:message code="sales_order_payment_details" /></th>
              </tr>
            </thead>
                <c:forEach var="item" items="${paymentForm.payments}" varStatus="status" >
                  <tr data-index="${status.index}">
                    <td><input type="hidden" value="${item.id}" name="payments[${status.index}].id" /><input type="checkbox" class="productRemoveFlag" /></td>
                    <td>
                          <select class="input-medium productId" name="payments[${status.index}].paymentType">
                              <option></option>
                            <c:forEach items="${paymentTypes}" var="paymentType" >
                              <option value="${paymentType.code}" <c:if test="${not empty item.paymentType and paymentType.code == item.paymentType.code}">selected</c:if>>${paymentType.description}</option>
                            </c:forEach>
                          </select>
                    
                    </td>
                    <td><input type="text" class="input-medium paymentDate" value="${item.paymentDateStr}" name="payments[${status.index}].paymentDate" /></td>
                    <td><input class="salesOrderPaymentAmountInput" type="text" class="input-medium" value="" /></td><!-- floatInput class removed -->
                    <td><input type="text" value="${item.paymentDetails}" name="payments[${status.index}].paymentDetails" /></td>
                    <td class="hide"><input class="salesOrderPaymentAmount" type="hidden" value="${item.paymentAmount}" name="payments[${status.index}].paymentAmount"/></td>
                  </tr>
                </c:forEach>
            </tbody>
          </table>
        </div>
        
        
        </div>
        
      </div>
          
      </div>
      
      </form:form>
    
    </div>
    <div class="modal-footer">
      <button type="button" class="orderSubmit btn btn-primary"><spring:message code="label_submit" /></button>
      <button type="button" id="orderCancel" class="btn btn-default" data-dismiss="modal"><spring:message code="label_cancel"/></button>
    </div>
  </div>
</div>

<script>
$(document).ready(function() {
  
  var CONTAINER_ERROR = "#contentError > div";
  var CONTENT_ERROR = "#contentError";
  $form = $("#paymentForm");
  $orderDialog = $("#orderDialog").css({"width": "900px"});
  
  $(".orderSubmit").click(submitForm);
  
  function submitForm() {
    $(".orderSubmit").prop( "disabled", true );
    $.post($form.attr("action"), $form.serialize(), function(data) {
      $(".orderSubmit").prop( "disabled", false );
      if (data.success) {
    	  reloadOrderTable();
      } 
      else {
        errorInfo = "";
        for (i = 0; i < data.result.length; i++) {
          errorInfo += "<br>" + (i + 1) + ". "
              + ( data.result[i].code != undefined ? 
                  data.result[i].code : data.result[i]);
        }
        $orderDialog.find(CONTAINER_ERROR).html("Please correct following errors: " + errorInfo);
        $orderDialog.find(CONTENT_ERROR).show('slow');
      }
    }); 
  }	
  	
  
    var $productTable = $('#productTable');
	
	var  $productTbody = $("tbody", $productTable);
	
	
	function bindCommaFormatting() {

        var amountString = $(this).val();
        //strip commas from textfield
        var amountNoComma = amountString.replace(/\,/g,'');
        $('.salesOrderPaymentAmount', $(this).closest("tr")).val(amountNoComma);

        //format comma on text field
        $(this).val(commaSeparateNumber(amountNoComma));
	}
	
	function computeReceivedPayments() {
		var totalReceived = 0;
		$(".salesOrderPaymentAmountInput").each(function() {
			totalReceived += parseInt($(this).val().replace(/\,/g,''));
		});
		
		$("#totalPayment").val(commaSeparateNumber(totalReceived));
		var netAmt = $("#netAmt").val().replace(/\,/g,'');
		var diffSo = netAmt - totalReceived;
		if(diffSo < 0) {
			$("#positiveDiff").hide();
			$("#negativeDiff").show();
		} else {
			$("#positiveDiff").show();
			$("#negativeDiff").hide();
		}
		
		$("#diffSo").val(commaSeparateNumber(diffSo));
		
	}

    /* var paymentAmountTextField = $('.salesOrderPaymentAmountInput'); */
    $('.salesOrderPaymentAmountInput')
    	.each(function() {
        	$(this).val(commaSeparateNumber($('.salesOrderPaymentAmount', $(this).closest("tr")).val()));
        });

	$('#removeProductBtn', $orderDialog ).click(function() {
        $("input.productRemoveFlag:checked").each(function() {
            var $tr = $(this).closest("tr");
            var length = $productTbody.find("tr").length;
            if($tr.is(":first-child") && length == 1) {
                $tr.find("input.productRemoveFlag").prop('checked', false);
                $tr.find("input").removeAttr('value').prop('disabled', true);
                $tr.find("select").removeAttr('value').prop('disabled', true);
                $tr.invisible();
            } else
                $tr.remove();
        });
    });

    $('#addProductBtn', $orderDialog ).click(function() {
        var $tr = $productTbody.find("tr:first-child");
        if($tr.css('visibility') == 'hidden' || $tr.css('display') == 'none') {
            $tr.find("input").removeAttr('value').prop('disabled', false).prop('checked', false);
            $tr.find("select").removeAttr('value').prop('disabled', false);
            $tr.visible();
        } else {
            var $trLast = $productTbody.find("tr:last-child");
            var  $row = $tr.clone();
            $row.find("input").removeAttr('value').removeAttr("checked").prop('disabled', false);
            $row.find("select").removeAttr('value').prop('disabled', false);
            var index = +$trLast.data("index") + 1;
            var lastIndex = $tr.data("index");
            $row.data("index", index);
            var rg = new RegExp("\\["+lastIndex+"\\]", 'g');
            var html = $row.html();
            html = html.replace(rg, '['+index+']');
            $row.html(html);

            $productTbody.append($row);
            initFields($row);
        }
    });
    
    
    initFields($orderDialog);
	
	jQuery.fn.visible = function() {
	    return this.css('visibility', 'visible');
	};

	jQuery.fn.invisible = function() {
	    return this.css('visibility', 'hidden');
	};
	
	
	function initFields(e) {
        $('.intInput', $(e)).numberInput({"type" : "int"});
    	$('.floatInput', $(e)).numberInput({"type" : "float"});
    	$("input.paymentDate", $(e)).each(function() {
    		$(this).datepicker({
        		format: 'dd-mm-yyyy',
        		autoclose: true
        	});
    	});

    	
    	$('.salesOrderPaymentAmountInput', $(e))
    		.change(bindCommaFormatting)
    		.change(computeReceivedPayments);
       /*  paymentAmountTextField.val(commaSeparateNumber($('#salesOrderPaymentAmount').val())); */
    }

    function commaSeparateNumber(val){
        while (/(\d+)(\d{3})/.test(val.toString())){
            val = val.toString().replace(/(\d+)(\d{3})/, '$1'+','+'$2');
        }
        return val;
    }
});
</script>