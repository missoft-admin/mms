<%@include file="../common/taglibs.jsp" %>
<button id="decileGenerateGrpBtn" class="btn btn-primary" style="margin-bottom: 5px;">Generate Customer Group</button>
<table id="decileGroupList" class="table table-condensed table-compressed table-striped">
  <thead>
  <tr>
    <th><spring:message code="hdr_select"/></th>
    <th><spring:message code="hdr_decile"/></th>
    <th><spring:message code="hdr_number_of_customers"/></th>
    <th><spring:message code="hdr_spend_of_decile"/></th>
    <th><spring:message code="hdr_ave_txn_count"/></th>
    <th><spring:message code="hdr_ave_sped_per_month"/></th>
    <th><spring:message code="hdr_total_spend"/></th>
    <th><spring:message code="hdr_percentage"/></th>
  </tr>
  </thead>
  <tbody>
  <c:choose>
    <c:when test="${!empty decileReportDto}">
      <c:forEach items="${decileReportDto}" var="dto">
        <tr>
          <td><input type="checkbox" value="${dto.memberIds}" class="memberIds"/></td>
          <td>${dto.no}</td>
          <td>${dto.numberOfCustomers}</td>
          <td>
            <fmt:formatNumber type="number" pattern="${decimalFormat}" value="${dto.minSpend}"/>
            to
            <fmt:formatNumber type="number" pattern="${decimalFormat}" value="${dto.maxSpend}"/>
          </td>
          <td><fmt:formatNumber type="number" pattern="${decimalFormat}" value="${dto.aveTxnCount}"/></td>
          <td><fmt:formatNumber type="number" pattern="${decimalFormat}" value="${dto.aveSpendPerMonth}"/></td>
          <td><fmt:formatNumber type="number" pattern="${decimalFormat}" value="${dto.totalSpend}"/></td>
          <td><fmt:formatNumber type="number" pattern="${decimalFormat}" value="${dto.percentage}"/></td>
        </tr>
      </c:forEach>
    </c:when>
    <c:otherwise>
      <tr>
        <td colspan="7"><spring:message code="msg_empty_records"/></td>
      </tr>
    </c:otherwise>
  </c:choose>
  </tbody>
</table>
<div id="pieChart" style="width: 50%; margin-left: auto; margin-right: auto">

</div>

<div class="modal hide  groupDialog nofly" tabindex="-1" role="dialog" aria-hidden="true" id="decileGroup">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title"><spring:message code="label_lmh_savegroup"/></h4>
      </div>
      <div class="modal-body">
        <div class="hide alert alert-error">
          <button type="button" class="close" onclick="$(this).parent().hide();">&times;</button>
          <div><form:errors path="*"/></div>
        </div>
        <form action="<spring:url value="/dataanalysis/generategroup" />"
              class="form-reset form-horizontal" method="POST">
          <div class="control-group">
            <label for="" class="control-label"><spring:message code="label_lmh_groupname"/></label>

            <div class="controls">
              <input type="text" name="name"/>
              <input type="hidden" name="members"/>
            </div>
          </div>
        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="submit btn btn-primary"><spring:message code="label_submit"/></button>
        <button type="button" data-dismiss="modal" class="btn btn-default"><spring:message code="label_cancel"/></button>
      </div>
    </div>
  </div>
</div>
<script>
  $( document ).ready( function () {
    <c:choose>
    <c:when test="${!empty decileReportDto}">
    jQuery.jqplot( 'pieChart', [
      [
        ['${decileReportDto[0].no}', ${decileReportDto[0].percentage}]
        <c:forEach var="i" begin="1" end="${fn:length(decileReportDto)}">
        ,
        ['${decileReportDto[i].no}', ${decileReportDto[i].percentage}]
        </c:forEach>
      ]
    ],
            {
              seriesDefaults : {
                // Make this a pie chart.
                renderer : jQuery.jqplot.PieRenderer,
                rendererOptions : {
                  // Put data labels on the pie slices.
                  // By default, labels show the percentage of the slice.
                  showDataLabels : true
                }
              },
              legend : { show : true, location : 'e' }
            }
    );
    </c:when>
    <c:otherwise>
    </c:otherwise>
    </c:choose>

    var $decileGroup = $( '#decileGroup' );

    $( '#decileGenerateGrpBtn' ).click( function () {
      var memberIds = $( '#decileGroupList input.memberIds:checked' );
      if ( memberIds.length > 0 ) {
        $decileGroup.modal( "show" );
      }
    } );


    $( '.submit', $decileGroup ).click( function () {
      var memberIds = $( '#decileGroupList input.memberIds:checked' );
      var members = memberIds[0].value;
      for ( var i = 1; i < memberIds.length; i++ ) {
        members += "," + memberIds[i].value;
      }
      var $members = $( 'input[name="members"]', $decileGroup );
      $members.val( members );
      var $form = $( 'form', $decileGroup );

      $.post( $form.attr( "action" ), $form.serialize(), function ( inResponse ) {
        if ( inResponse.success ) {
          $decileGroup.modal( "hide" );
          $('input[name="name"]' ).val('');
          $members.val('');
        }
        else {
          var errorInfo = "";
          for ( i = 0; i < inResponse.result.length; i++ ) {
            errorInfo += "<br>" + (i + 1) + ". "
                    + ( inResponse.result[i].code != undefined ?
                    inResponse.result[i].code : inResponse.result[i]);
          }

          $( '.alert-error > div', $decileGroup ).html( "Please correct following errors: " + errorInfo );
          $( '.alert-error', $decileGroup ).show( 'slow' );
        }
      }, "json" );
    } );
  } );
</script>