package com.transretail.crm.common.audit;

import java.lang.reflect.Method;

import org.springframework.aop.framework.ProxyFactory;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.BeanFactory;
import org.springframework.beans.factory.BeanFactoryAware;
import org.springframework.beans.factory.config.InstantiationAwareBeanPostProcessorAdapter;
import org.springframework.core.Ordered;
import org.springframework.core.PriorityOrdered;
import org.springframework.stereotype.Component;

import com.transretail.crm.common.audit.annotation.Audited;
import com.transretail.crm.common.audit.helper.ProxyHelper;

/**
 * @author Allan G. Ramirez (agramirez@exist.com)
 */
@Component
public class PostProcessor extends InstantiationAwareBeanPostProcessorAdapter implements PriorityOrdered, BeanFactoryAware {

    private BeanFactory beanFactory;

    private ProxyHelper proxyHelper = new ProxyHelper();

    @Override
    public void setBeanFactory(BeanFactory beanFactory) throws BeansException {
        this.beanFactory = beanFactory;
    }

    /**
     * Get the priority order of this post processor. Will always return Integer.MAX_VALUE, meaning last in order.
     *
     * @return Integer.MAX_VALU     E, this processor will be last in order
     */
    @Override
    public int getOrder() {
        return Ordered.LOWEST_PRECEDENCE;
    }


    @Override
    public Object postProcessAfterInitialization(Object bean, String beanName) throws BeansException {
        if (isAudited(proxyHelper.unwrapIfProxy(bean).getClass())) {
            ServiceMethodLogger serviceMethodLogger = beanFactory.getBean(ServiceMethodLogger.class);
            ProxyFactory factory = new ProxyFactory(bean);
            factory.addAdvice(serviceMethodLogger);
            return factory.getProxy();
        } else {
            return bean;
        }
    }

    private boolean isAudited(Class<?> clazz) {
        for (Method method : clazz.getDeclaredMethods()) {
            if (method.isAnnotationPresent(Audited.class)) {
                return true;
            }
        }
        return false;
    }
}

