package com.transretail.crm.core.dto;


import java.util.Date;

import org.apache.commons.lang3.StringUtils;
import org.joda.time.LocalDateTime;

import com.transretail.crm.core.entity.Complaint;
import com.transretail.crm.core.entity.Complaint.ComplaintCategory;
import com.transretail.crm.core.entity.Complaint.ComplaintPriority;
import com.transretail.crm.core.entity.lookup.LookupDetail;
import com.transretail.crm.core.security.util.UserUtil;


public class ComplaintDto {

	private Long id;
	private String ticketNo;
	private String storeCode;
	private String storeDesc;
	private String firstName;
	private String lastName;
	private String genderCode;
	private String genderDesc;
	private String complaints;
	private String mobileNo;
	private String email;
	private String assignedTo;
    private String createUser;
    private String remarks;
    private String status;
    private ComplaintPriority priority;
    private ComplaintCategory category;
    private String memberBarcode;

    private UtilInfo info;

	private Date dateFiled;
	private Long dateTime;
	private String dispStatus;



    public ComplaintDto() {}
    public ComplaintDto( Complaint model ) {
    	this.id = model.getId();
    	this.ticketNo = model.getTicketNo();
    	this.dateFiled = ( null != model.getDateFiled() )? model.getDateFiled().toDate() : null;
    	this.dateTime = ( null != model.getDateFiled() )? model.getDateFiled().toDate().getTime() : null;
    	this.storeCode = model.getStoreCode();
    	this.firstName = model.getFirstName();
    	this.lastName = model.getLastName();
    	this.genderCode = ( null != model.getGender() )? model.getGender().getCode() : null;
    	this.complaints = model.getComplaints();
    	this.mobileNo = model.getMobileNo();
    	this.email = model.getEmail();
    	this.assignedTo = model.getAssignedTo();
    	this.createUser = model.getCreateUser();
    	this.status = model.getStatus();
    	this.priority = model.getPriority();
    	this.category = model.getCategory();
    	this.memberBarcode = model.getMemberBarcode();

    	this.info = new UtilInfo( this );
    }
    public ComplaintDto( Complaint model, String stat ) {
    	this( model );
    	if ( StringUtils.isNotBlank( model.getStatus() ) && StringUtils.isNotBlank( stat ) ) {
        	this.info.setClosed( model.getStatus().equalsIgnoreCase( stat ) );
    	}
    }

    public Complaint toModel() { return toModel( null ); }
    public Complaint toModel( Complaint model ) {
		if ( null == model ) {
			model = new Complaint();
		}

		model.setTicketNo( ticketNo );
		model.setDateFiled( ( null != dateTime )? new LocalDateTime( dateTime ) : null );
		model.setStoreCode( storeCode );
		model.setFirstName( firstName );
		model.setLastName( lastName );
		model.setGender( ( StringUtils.isNotBlank( genderCode ) )? new LookupDetail( genderCode ) : null );
		model.setComplaints( complaints );
		model.setMobileNo( mobileNo );
		model.setEmail( email );
		model.setAssignedTo( assignedTo );
		model.setStatus( status );
		model.setPriority( priority );
		model.setCategory(category);
		model.setMemberBarcode(memberBarcode);

		return model;
    }

    public Complaint toUpdatedModel() { return toUpdatedModel( null ); }
    public Complaint toUpdatedModel( Complaint model ) {
		if ( null == model ) {
			model = new Complaint();
		}

		model.setDateFiled( ( null != dateTime )? new LocalDateTime( dateTime ) : null );
		model.setFirstName( firstName );
		model.setLastName( lastName );
		model.setGender( ( StringUtils.isNotBlank( genderCode ) )? new LookupDetail( genderCode ) : null );
		model.setComplaints( complaints );
		model.setMobileNo( mobileNo );
		model.setEmail( email );
		model.setAssignedTo( assignedTo );
		model.setPriority( priority );
		model.setCategory(category);
		model.setMemberBarcode(memberBarcode);

		return model;
    }

	/*public static List<ComplaintDto> toDtoList( List<Complaint> models ) {
		if ( CollectionUtils.isEmpty( models ) ) {
			return new ArrayList<ComplaintDto>();
		}

		List<ComplaintDto> dtos = new ArrayList<ComplaintDto>();
		for ( Complaint model : models ) {
			dtos.add( new ComplaintDto( model ) );
		}
		return dtos;
	}*/



	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getTicketNo() {
		return ticketNo;
	}
	public void setTicketNo(String ticketNo) {
		this.ticketNo = ticketNo;
	}
	public String getStoreCode() {
		return storeCode;
	}
	public void setStoreCode(String storeCode) {
		this.storeCode = storeCode;
	}
	public String getGenderDesc() {
		return genderDesc;
	}
	public void setGenderDesc(String genderDesc) {
		this.genderDesc = genderDesc;
	}
	public String getStoreDesc() {
		return storeDesc;
	}
	public void setStoreDesc(String storeDesc) {
		this.storeDesc = storeDesc;
	}
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public String getGenderCode() {
		return genderCode;
	}
	public void setGenderCode(String genderCode) {
		this.genderCode = genderCode;
	}
	public String getComplaints() {
		return complaints;
	}
	public void setComplaints(String complaints) {
		this.complaints = complaints;
	}
	public String getMobileNo() {
		return mobileNo;
	}
	public void setMobileNo(String mobileNo) {
		this.mobileNo = mobileNo;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getAssignedTo() {
		return assignedTo;
	}
	public void setAssignedTo(String assignedTo) {
		this.assignedTo = assignedTo;
	}
	public String getCreateUser() {
		return createUser;
	}
	public void setCreateUser(String createUser) {
		this.createUser = createUser;
	}
	public String getRemarks() {
		return remarks;
	}
	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public ComplaintPriority getPriority() {
		return priority;
	}
	public void setPriority(ComplaintPriority priority) {
		this.priority = priority;
	}
	public Date getDateFiled() {
		return dateFiled;
	}
	public void setDateFiled(Date dateFiled) {
		this.dateFiled = dateFiled;
	}
	public Long getDateTime() {
		return dateTime;
	}
	public void setDateTime(Long dateTime) {
		this.dateTime = dateTime;
	}
	public String getDispStatus() {
		return dispStatus;
	}
	public void setDispStatus(String dispStatus) {
		this.dispStatus = dispStatus;
	}
    public UtilInfo getInfo() {
		return info;
	}



	protected class UtilInfo {
		private boolean isOwner;
		private boolean isCreator;
		private boolean isClosed;

		public UtilInfo( ComplaintDto dto ) {
	    	String username = UserUtil.getCurrentUser().getUsername();
			if ( StringUtils.isNotBlank( dto.getAssignedTo() ) ) {
				this.isOwner = dto.getAssignedTo().equalsIgnoreCase( username );
			}
			if ( StringUtils.isNotBlank( dto.getCreateUser() ) ) {
				this.isCreator = dto.getCreateUser().equalsIgnoreCase( username );
			}
		}

		public boolean isOwner() {
			return isOwner;
		}
		public boolean isCreator() {
			return isCreator;
		}
		public boolean isClosed() {
			return isClosed;
		}
		public void setClosed(boolean isClosed) {
			this.isClosed = isClosed;
		}
	}



	public String getMemberBarcode() {
		return memberBarcode;
	}
	public void setMemberBarcode(String memberBarcode) {
		this.memberBarcode = memberBarcode;
	}
	public ComplaintCategory getCategory() {
		return category;
	}
	public void setCategory(ComplaintCategory category) {
		this.category = category;
	}

}
