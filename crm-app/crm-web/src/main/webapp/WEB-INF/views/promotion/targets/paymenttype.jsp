<%@ include file="../../common/taglibs.jsp" %>

<style type="text/css">
<!--

  .table tbody tr td:first-child,   .table tbody tr th:first-child {
      width: 20px;
  }
  #targetPaymentTypesDialog .modal-body {
      overflow-y: visible;
  }
  
-->
</style>

<spring:message code="operand_tooltip_label_info" var="info" />
<spring:message code="operand_tooltip_payment_type" var="tooltip" />

<div class="modal hide  nofly" id="targetPaymentTypesDialog" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4>Add <spring:message code="target_payment_type" /></h4>
      </div>
      
      <div class="modal-body">
      
      	<div class="pull-left paymentTypes" style="width:100%; padding-right:2%">
			<input type="hidden" name="eftCode" value="${eftCode}" />
			<div class="">
				<table class="table table-condensed table-striped">
					<thead>
						<tr>
							<th class="targetSquare"><![CDATA[&nbsp;]]></th>
							<th><spring:message code="label_group_paymenttype"/></th>
						</tr>
					</thead>
					<tbody id="paymentTypes">
					<c:forEach var="item" items="${paymentTypes}" >
					<tr class="paymentTypeContainer">
						<td class="targetSquare"><input type="checkbox" class="targetCheckbox" name="targetPaymentTypes" value="${item.key}"></td>
						<td><span class="targetText"><c:out value="${item.value}" /></span></td>
					</tr>
					</c:forEach>
                    <c:forEach var="item" items="${paymentTypesWilcard}" >
                    <tr class="paymentTypeContainer">
                      <td class="targetSquare"><input type="checkbox" class="targetCheckbox paymentTypeCheckbox" name="targetPaymentTypes" value="${item.key}"></td>
                      <td><span class="targetText"><c:out value="${item.value}" /></span></td>
                    </tr>
                    </c:forEach>
					</tbody>
				</table>
			</div>
		</div>
        <c:forEach var="item" items="${paymentTypesWilcard}" >
        <div class="pull-left hide wildcardContainer ${item.key}Container" style="width:48%;padding-left:2%">
            <div style="width: 100%"><label for="">${item.value} <spring:message code="payment_type_number"/></label></div>
                <div style="width: 100%">
                  <select multiple="multiple" class="targetWildcardsSel targetWildcardsSel${item.key}" data-code="${item.key}" style="width:100%;">
                  </select>
                </div>
                <div>
                  <div class="form-horizontal pull-left">
                    <input type="text" class="targetWildcardsStr" style="width:80px;" /><input type="button" class="addWildcard btn btn-primary" value="<spring:message code="label_add" />"/>
                  </div>
                  
                        <div class="pull-right hide removeWildcardsContainer">
                          <input type="button" class="removeWildcards btn btn-primary" value="<spring:message code="label_remove" />"/>
                        </div>
                        
                        <div class="clearfix"></div>
                </div>
        </div>
        </c:forEach>
		<%--
		<div class="pull-left hide eftContainer" style="width:48%;padding-left:2%">
				<div style="width: 100%"><label for=""><spring:message code="label_group_eft"/></label></div>
	       		<div style="width: 100%">
	        		<select multiple="multiple" name="targetEfts" style="width:100%;">
	        		</select>
	       		</div>
	       		<div>
	        		<div class="form-horizontal pull-left">
	        			<input type="text" name="eftStr" style="width:80px;" /><input type="button" id="addEft" class="btn btn-primary" value="<spring:message code="label_add" />"/>
	        		</div>
              
                    <div class="pull-right hide" id="removeEftContainer">
                      <input type="button" id="removeEft" class="btn btn-primary" value="<spring:message code="label_remove" />"/>
                    </div>
                    
                    <div class="clearfix"></div>
	        	</div>
		</div>
		 --%>
		<div class="clearfix"></div>
		
      </div>
      <div class="modal-footer container-btn">
        <button type="button" id="paymentTypeSaveButton" class="btn btn-primary"><spring:message code="label_save"/></button>
        <button type="button" id="paymentTypeCancelButton" class="btn " data-dismiss="modal"><spring:message code="label_cancel"/></button>
      </div>
    </div>
  </div>
</div>
