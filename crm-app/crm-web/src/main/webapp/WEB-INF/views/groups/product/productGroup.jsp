<%@include file="../../common/messages.jsp" %>


<div id="content_productGroup">

  <div class="page-header page-header2"><h1><spring:message code="label_group_productgroup" /></h1></div>


  <div class="">
	  <div class="pull-right">
	    <a href="#" class="btn btn-primary" data-url="<c:url value="/groups/product/create"/>" id="link_createProductGroup">
        <spring:message code="group_product_create" />
      </a>
	  </div>
  </div>
  <div class="clearfix"></div>

  <div id="form_productGroup"><jsp:include page="productGroupForm.jsp" /></div>
  <div id="list_productGroups" class="data-content-02"><jsp:include page="productGroupList.jsp" /></div>
  <div><jsp:include page="productGroupItems.jsp" /></div>


  <script src='<c:url value="/js/viewspecific/groups/product/productGroup.js"/>' type='text/javascript'><![CDATA[&nbsp; ]]></script>
  <link href="<c:url value="/styles/temp.css" />" rel="stylesheet" />


</div>