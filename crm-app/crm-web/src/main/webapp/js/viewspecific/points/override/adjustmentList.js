$(document).ready( function() {

	var $theList = $( "#list_adjustment" );

	var $theSearchForm = $( "#form_SearchForm" );
  var $theSearchBtn = $( "#searchButton" );

	initDataTable();



	function initDataTable() {

		var highestApprovalPt = $theList.data( "highest-appr-point" );

		var theHeaders = { 
			accountId: $theList.data( "hdr-accountid" ),
			storeLocation: $theList.data( "hdr-transactionlocation" ),
			transactionDate: $theList.data( "hdr-transactiondate" ),
			transactionNo: $theList.data( "hdr-transactionno" ),
			transactionAmount: $theList.data( "hdr-transactionamount" ),
			existingPoints: $theList.data( "hdr-existingpoints" ),
			transactionPoints: $theList.data( "hdr-transactionpoints" ),

			requestedBy: $theList.data( "hdr-requestedby" ),
			requestComment: $theList.data( "hdr-requestcomment" ),
			requestDate: $theList.data( "hdr-requestdate" ),

			approvedBy: $theList.data( "hdr-approvedby" ),
			approvalComment: $theList.data( "hdr-approvalcomment" ),
			approvalDate: $theList.data( "hdr-approvaldate" ),

			status: $theList.data( "hdr-status" ),
			edit: $theList.data( "link-edit" )
		};

		$theList.ajaxDataTable({
			'autoload' 		: true,
			'ajaxSource' 	: $theList.data( "url" ),
			'aaSorting'		: [[ 1, "desc" ]],
			'columnHeaders' : [	
			   	
		    theHeaders['accountId'],
			 	theHeaders['storeLocation'],
			 	theHeaders['transactionDate'],
			 	theHeaders['transactionNo'],
			 	theHeaders['transactionAmount'],
			 	theHeaders['existingPoints'],
			 	theHeaders['transactionPoints'],

			 	theHeaders['requestedBy'],
			 	theHeaders['requestComment'],
			 	theHeaders['requestDate'],

			 	theHeaders['approvedBy'],
			 	{ sClass : "el-width-01", text : theHeaders['approvalComment'] }, 
			 	theHeaders['approvalDate'],

			 	theHeaders['status'],
			{ text: "", className : "el-width-x" }
			],
			'modelFields' 	: [

			 	{ name 	: 'memberModel.accountId', sortable : true, fieldCellRenderer : function( data, type, row ) { return row.memberModel.accountId + " - " + row.memberModel.formattedMemberName; } },
			 	{ name 	: 'storeCode', sortable : true, fieldCellRenderer : function( data, type, row ) {
				 		if ( row.storeDto != null && row.storeDto != "" ) {
				 			return row.storeDto.code + " - " + row.storeDto.name;
				 		}
				 		else {
				 			return  + row.storeCode;
				 		}
			 		} 
			 	},
			 	{ name 	: 'transactionDateTime', sortable : true, fieldCellRenderer : function (data, type, row) {
			           return ( (row.longTxnDate)? new Date( row.longTxnDate ).customize(0) : "" );
		        }},
			 	{ name 	: 'transactionNo', sortable : true },
			 	{ name 	: 'transactionAmount', sortable : true , fieldCellRenderer : function (data, type, row) {
              if(row.transactionAmount != null && row.transactionAmount != "")
                return row.transactionAmount.toLocaleString();
              else
                return "";
         }},
			 	{ name 	: 'memberModel.totalPoints', sortable : true },
			 	{ name 	: 'transactionPoints', sortable : true },

			 	{ name 	: 'createUser', sortable : true },
			 	{ name 	: 'comment', sortable : true },
			 	{ name 	: 'created', sortable : true },

			 	{ name 	: 'validatedBy', sortable : true },
			 	{ name 	: 'approvalReason', sortable : true },
			 	{ name 	: 'validatedDate', sortable : true },

			 	{ name 	: 'status', sortable : true, fieldCellRenderer : function (data, type, row) {
	                return (row.status == 'FORAPPROVAL')? "FOR APPROVAL" : row.status ;
	            }},
			  { customCell : function ( innerData, sSpecific, json ) {
		    		var ret = "";
		 			if ( sSpecific == 'display' ) {
		 				if ( $theList.data( "role-staff" ) ) {
		 					ret += ( $( "#btn_editAdjustment" ).html().replace( new RegExp("%ID%", 'g'), json.id ) 
			 						+ $( "#btn_deleteAdjustment" ).html().replace( new RegExp("%ID%", 'g'), json.id ) );
		 				}
		 				var boolProcess = ( highestApprovalPt && json.transactionPoints && 
		 						( (highestApprovalPt - 0) > ( json.transactionPoints - 0 ) ) ) ? true : false;
		 				if ( $theList.data( "role-head" ) && boolProcess ) {
		 					ret += ( $( "#btn_processAdjustment" ).html().replace( new RegExp("%ID%", 'g'), json.id ) );
		 				}
			        } 
		 			return ret;
		 		}
	        }
			 ]
		})
		.on( "click", ".btn_editAdjustment", edit )
		.on( "click", ".btn_deleteAdjustment", deleteProcess )
		.on( "click", ".btn_processAdjustment", process );
		
		initSearchForm($theList);
	}



	function edit( e ) {
		e.preventDefault();
		$.ajax({
			cache 		: false, 
			dataType 	: "html",
			type 		: "GET", 
			url 		: $( "#btn_editAdjustment" ).data( "url" ) + $( this ).data( "id" ),
			error 		: function(jqXHR, textStatus, errorThrown) {}, 
			success 	: function( inResponse ) {
				$( "#form_adjustment" ).html( inResponse );
				$( "#content_adjustmentForm" ).modal( "show" );
				$(".floatInput").numberInput({ "type" : "float" });
				$(".intInput").numberInput({  "type" : "int" });
			}
		});
	}

	function process( e ) {
		e.preventDefault();
		$.ajax({
			cache 		: false, 
			dataType 	: "html",
			type 		: "GET", 
			url 		: $( "#btn_processAdjustment" ).data( "url" ) + $( this ).data( "id" ),
			error 		: function(jqXHR, textStatus, errorThrown) {}, 
			success 	: function( inResponse ) {
				$( "#form_adjustment" ).html( inResponse );
				$( "#content_adjustmentForm" ).modal( "show" );
				$(".floatInput").numberInput({ "type" : "float" });
				$(".intInput").numberInput({  "type" : "int" });
			}
		});
	}

	function deleteProcess( e ) {
		e.preventDefault();
		var deleteUrl = $( "#btn_deleteAdjustment" ).data( "url" ) + $( this ).data( "id" ) + "/" + $( "#btn_deleteAdjustment" ).data( "status-process" );
		getConfirm( $( "#btn_deleteAdjustment" ).data( "msg-confirm" ), function( result ) {
			if( result ) {
				$.post( 
						deleteUrl,
						function( inResponse, textStatus, jqXHR ) { $( "#list_adjustment" ).ajaxDataTable( "search" ); },
						"json" );
			}
		});
	}

	function initSearchForm( inDataTable ) {
    $theSearchBtn.click( function() {
      var $theFormDataObj = $theSearchForm.toObject( { mode : 'first', skipEmpty : true } );
      var $btnSearch = $( this );
      $btnSearch.prop( 'disabled', true );
      inDataTable.ajaxDataTable( 
        'search', 
        $theFormDataObj, 
        function () {
          $btnSearch.prop( 'disabled', false );
      });
    });
    
    $("#clearButton").click(function(e) {
      e.preventDefault();
      $("#searchField option:first").attr('selected','selected');
      $("#searchValue").val("");
      $("#transactionDate").val("");
      $(".pointsSearchDropdown").val("");
      $("#txnNo").val("");
      $(".pointsSearchDropdown").val("");
      $("#searchButton").click();
    });
  }
});