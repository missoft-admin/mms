package com.transretail.crm.giftcard.dto;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.Lists;
import com.mysema.query.annotations.QueryProjection;
import com.transretail.crm.core.entity.lookup.LookupDetail;
import com.transretail.crm.giftcard.entity.GiftCardPhysicalCountSummary;
import java.util.List;
import org.joda.time.LocalDateTime;
import org.springframework.beans.BeanUtils;

/**
 *
 * @author Monte Cillo Co (mco@exist.com)
 */
public class GiftCardPhysicalCountSummaryDto {

    private String productProfile;
    private String productProfileName;
    private String location;
    private String locationName;
    private Long currentInventory;
    private Long physicalCount;
    private List<String> foundItems;
    private List<String> missingItems;

    public GiftCardPhysicalCountSummaryDto() {
    }

    @QueryProjection
    public GiftCardPhysicalCountSummaryDto(String location,
	    LookupDetail cardType, Long currentInventory, Long physicalCount) {
	super();
	this.location = location;
	this.currentInventory = currentInventory;
	this.physicalCount = physicalCount;
    }

    @QueryProjection
    public GiftCardPhysicalCountSummaryDto(String location,
	    LookupDetail cardType, Long currentInventory) {
	super();
	this.location = location;
	this.currentInventory = currentInventory;
    }

    public GiftCardPhysicalCountSummaryDto(GiftCardPhysicalCountSummary summary) {
	BeanUtils.copyProperties(summary, GiftCardPhysicalCountSummaryDto.this);
    }

    public GiftCardPhysicalCountSummary toModel(LocalDateTime created) {
	GiftCardPhysicalCountSummary summary = new GiftCardPhysicalCountSummary();
	BeanUtils.copyProperties(this, summary);
	summary.setCreated(created);
	return summary;
    }

    public String getProductProfile() {
	return productProfile;
    }

    public void setProductProfile(String productProfile) {
	this.productProfile = productProfile;
    }

    public String getProductProfileName() {
	return productProfileName;
    }

    public void setProductProfileName(String productProfileName) {
	this.productProfileName = productProfileName;
    }

    public String getLocation() {
	return location;
    }

    public void setLocation(String location) {
	this.location = location;
    }

    public String getLocationName() {
		return locationName;
	}

	public void setLocationName(String locationName) {
		this.locationName = locationName;
	}

	public Long getCurrentInventory() {
	return currentInventory;
    }

    public void setCurrentInventory(Long currentInventory) {
	this.currentInventory = currentInventory;
    }

    public Long getPhysicalCount() {
	return physicalCount;
    }

    public void setPhysicalCount(Long physicalCount) {
	this.physicalCount = physicalCount;
    }

    public Long getDifference() {
	return physicalCount - currentInventory;
    }

    public List<String> getFoundItems() {
	if (foundItems == null) {
	    this.foundItems = Lists.newArrayList();
	}
	return ImmutableList.copyOf(foundItems);
    }

    public void setFoundItems(List<String> foundItems) {
	this.foundItems = foundItems;
    }

    public List<String> getMissingItems() {
	if (missingItems == null) {
	    this.missingItems = Lists.newArrayList();
	}
	return ImmutableList.copyOf(missingItems);
    }

    public void setMissingItems(List<String> missingItems) {
	this.missingItems = missingItems;
    }

    public GiftCardPhysicalCountSummaryDto addMissingItem(String cardNo) {
	if (missingItems == null) {
	    this.missingItems = Lists.newArrayList();
	}

	this.missingItems.add(cardNo);
	return this;
    }

    public GiftCardPhysicalCountSummaryDto addFoundItem(String cardNo) {
	if (foundItems == null) {
	    this.foundItems = Lists.newArrayList();
	}

	this.foundItems.add(cardNo);
	return this;
    }
}
