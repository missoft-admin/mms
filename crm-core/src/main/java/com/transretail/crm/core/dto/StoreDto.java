package com.transretail.crm.core.dto;

import com.transretail.crm.core.entity.lookup.Store;

public class StoreDto {
	
	private Integer id;
	private String code;
    private String name;

    public StoreDto() {
    	
    }
    public StoreDto(Store store) {
//    	BeanUtils.copyProperties(store, this);
        this.id = store.getId();
        this.code = store.getCode();
        this.name = store.getName();
    }
    public StoreDto(String store) {
    	this.code = store;
    }
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
    public String getCodeAndName() {
        return code + " - " + name;
    }

}
