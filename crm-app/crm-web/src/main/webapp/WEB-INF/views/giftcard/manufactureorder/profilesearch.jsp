<%@ include file="../../common/taglibs.jsp"%>

<style type="text/css">
<!--
  .well .input {
    background-color: #fff;
    border: 1px solid #E8E8E8;
    box-shadow: none;    
    padding: 6px;
    height: 18px;
    margin-left: 5px;
  }
  .well .btn-primary {
    margin-left: 10px;
  }
  .well select {
    margin: 5px;
  }
  .well .statusContainer {
    margin-left: 100px;
    margin-top: 5px;
  }
  
-->
</style>

<form:form id="profileSearchDto" cssClass="">
<div class="well clearfix">
<div class="form-inline form-search" id="profileSearchForm">
	<spring:message code="label_search" var="searchLabel"/>
	<label class="label-single">${searchLabel}:</label>
	<select id="criteria" class="searchField">
		<option value=""><spring:message code="label_criteria" arguments="${searchLabel}" /></option>
		<c:forEach items="${profileSearchCriteria}" var="criteria">
		<option value="${criteria.field}"><spring:message code="profile_search_${criteria.field}" /></option>
		</c:forEach>
	</select>
	<input type="text" id="criteriaValue" class="input searchField" placeholder="${searchLabel}"/>
	<input id="search" type="button" class="btn btn-primary" value="<spring:message code="button_search"/>"/>
	<input type="button" id="clear" value="<spring:message code="label_clear" />" class="btn custom-reset" data-form-id="profileSearchForm" data-default-id="search" />
</div>
<div class="form-inline form-search">
    <div class="">
    	<label class="label-single"><spring:message code="label_filter" />:</label>
    	<select name="statusCode" class="searchField">
    		<option value=""><spring:message code="mo_prop_status" /></option>
    		<c:forEach items="${profileStatus}" var="stat">
    		<option value="${stat}">${stat}</option>
    		</c:forEach>
    	</select>
    </div>
    <div class="statusContainer">
      <div class="span checkbox" style="margin-left: 10px">
      	<input type="checkbox" name="allowReload" class="profileCheckbox" /><spring:message code="profile_allow_reload" />
  	  </div>
  
      <div class="span checkbox">
          <input type="checkbox" name="allowPartialRedeem" class="profileCheckbox" /><spring:message code="profile_allow_partial_redeem" />
      </div>
      
      <div class="span checkbox">
          <input type="checkbox" name="isEgc" id="isEgc" class="profileCheckbox" /><spring:message code="order_isegc" />
      </div>
    </div>
</div>
</div>
</form:form>

<script>
$(document).ready(function() {
  $profileSearchForm = $("#profileSearchDto");
  
  $("#orderDateSearch", $profileSearchForm).datepicker({
    format    : 'dd-mm-yyyy',
    endDate   : '-0d',
    autoclose : true
  });
  
  $("#clear", $profileSearchForm).click(function() {
    $(".searchField", $profileSearchForm).val("");
    $(".profileCheckbox", $profileSearchForm).prop("checked", false);
  });

  $("#search", $profileSearchForm).click(function() {
	  
	  if(!$("#isEgc").is(":checked")) {
		  $("#isEgc").prop("disabled", true);  
	  }
	  
    $("#criteriaValue", $profileSearchForm).attr("name", $("#criteria", $profileSearchForm).val());
    var formDataObj = $profileSearchForm.toObject( { mode : 'first', skipEmpty : true } );
    console.log(formDataObj);
    $("#product_profile_list_container").ajaxDataTable('search', formDataObj);
    
    $("#isEgc").prop("disabled", false);
  });
});
</script>
