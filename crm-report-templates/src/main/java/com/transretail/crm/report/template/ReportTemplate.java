package com.transretail.crm.report.template;

import java.util.Map;
import java.util.Set;

import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import com.transretail.crm.common.reporting.jasper.JRProcessor;
import com.transretail.crm.common.util.DateUtil;
import com.transretail.crm.core.entity.enums.ReportType;

/**
 * Base class for all reporting templates.
 * 
 * @author Allan G. Ramirez (agramirez@exist.com)
 */
public interface ReportTemplate {
    DateTimeFormatter INPUT_DATE_PATTERN = DateTimeFormat.forPattern(DateUtil.SEARCH_DATE_FORMAT);
    String PARAM_REPORT_TYPE = "reportType";
    /**
     * Get the unique template report name
     *
     * @return unique template report name
     */
    String getName();

    /**
     * Description of this template
     *
     * @return template description
     */
    String getDescription();

    Set<FilterField> getFilters();

    /**
     * Check if any permission in set is allowed to view this report
     *
     * @param permissions set of permissions to check
     * @return true if a permission in set is allowed to view this report
     */
    boolean canView(Set<String> permissions);

    JRProcessor createJrProcessor(Map<String, String> parameters);

    ReportType[] getReportTypes();
}
