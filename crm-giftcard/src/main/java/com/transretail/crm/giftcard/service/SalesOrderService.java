package com.transretail.crm.giftcard.service;

import java.math.BigDecimal;
import java.util.List;

import com.transretail.crm.common.reporting.jasper.JRProcessor;
import com.transretail.crm.common.service.dto.response.ResultList;
import com.transretail.crm.giftcard.dto.SalesOrderDto;
import com.transretail.crm.giftcard.dto.SalesOrderSearchDto;
import com.transretail.crm.giftcard.entity.SalesOrder;
import com.transretail.crm.giftcard.entity.SalesOrderHistory;
import com.transretail.crm.giftcard.entity.support.SalesOrderDiscountType;
import com.transretail.crm.giftcard.entity.support.SalesOrderStatus;
import com.transretail.crm.giftcard.service.DiscountSchemeService.DiscountDto;

public interface SalesOrderService {

	BigDecimal getTotalFaceAmount(SalesOrderDto orderDto);

	BigDecimal getTotalFaceAmount(Long orderId);

	SalesOrderDto getOrderByOrderNo(String orderNo);

	Long saveOrder(SalesOrderDto orderDto);

	Long save(SalesOrder order);

	ResultList<SalesOrderDto> getSalesOrders(SalesOrderSearchDto searchForm);

	List<SalesOrderDto> getSalesOrdersForPrint(SalesOrderSearchDto searchForm);

	SalesOrderDto getOrder(Long id);

	SalesOrder get(Long id);

	void deleteOrder(Long id);

	void processReplacement(Long id);

	void processDiscount(Long id, SalesOrderStatus status);

	JRProcessor createJrProcessor(SalesOrderSearchDto searchDto);

	DiscountDto getDiscount(SalesOrderDto order, SalesOrderDiscountType discType);

	void approveOrder(Long id, SalesOrderStatus status);

	boolean isSalesOrderExist(Long id);

	boolean isValidForExtension(Long id);
	
	List<SalesOrderHistory> getHistories(Long id);
	void processHistory(Long id,SalesOrderStatus stat);

}
