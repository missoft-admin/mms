package com.transretail.crm.loyalty.service.impl;


import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.BooleanUtils;
import org.apache.commons.lang3.StringUtils;
import org.hibernate.StatelessSession;
import org.joda.time.LocalDate;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.core.task.AsyncTaskExecutor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.google.common.collect.Sets;
import com.mysema.query.jpa.JPQLQuery;
import com.mysema.query.jpa.impl.JPAQuery;
import com.mysema.query.types.ConstructorExpression;
import com.mysema.query.types.expr.BooleanExpression;
import com.transretail.crm.common.reporting.jasper.ClasspathFileResolver;
import com.transretail.crm.common.reporting.jasper.DefaultJRProcessor;
import com.transretail.crm.common.reporting.jasper.JRProcessor;
import com.transretail.crm.common.service.dto.request.PagingParam;
import com.transretail.crm.common.service.util.SpringDataPagingUtil;
import com.transretail.crm.common.util.FileCryptor;
import com.transretail.crm.core.dto.MessageDto;
import com.transretail.crm.core.entity.ApplicationConfig;
import com.transretail.crm.core.entity.MemberModel;
import com.transretail.crm.core.entity.PointsTxnModel;
import com.transretail.crm.core.entity.QUserRolePermission;
import com.transretail.crm.core.entity.enums.AppConfigDefaults;
import com.transretail.crm.core.entity.enums.AppKey;
import com.transretail.crm.core.entity.lookup.LookupDetail;
import com.transretail.crm.core.entity.lookup.QLookupDetail;
import com.transretail.crm.core.entity.lookup.Store;
import com.transretail.crm.core.repo.ApplicationConfigRepo;
import com.transretail.crm.core.repo.PointsTxnModelRepo;
import com.transretail.crm.core.security.model.impl.CustomSecurityUserDetailsImpl;
import com.transretail.crm.core.security.util.UserUtil;
import com.transretail.crm.core.service.CodePropertiesService;
import com.transretail.crm.core.service.LookupService;
import com.transretail.crm.core.service.PointsTxnManagerService;
import com.transretail.crm.core.service.StoreService;
import com.transretail.crm.core.util.generator.IdGeneratorService;
import com.transretail.crm.giftcard.service.impl.GiftCardInventoryServiceImpl;
import com.transretail.crm.loyalty.dto.AllocationDto;
import com.transretail.crm.loyalty.dto.LCInvResultDto;
import com.transretail.crm.loyalty.dto.LCInvResultList;
import com.transretail.crm.loyalty.dto.LCInvSearchDto;
import com.transretail.crm.loyalty.dto.LoyaltyAnnualFeeSchemeDto;
import com.transretail.crm.loyalty.dto.LoyaltyCardInventoryDto;
import com.transretail.crm.loyalty.dto.LoyaltyCardInventoryResultList;
import com.transretail.crm.loyalty.dto.LoyaltyCardInventorySearchDto;
import com.transretail.crm.loyalty.dto.LoyaltyCardInventorySummaryResultList;
import com.transretail.crm.loyalty.dto.ManufactureOrderDto;
import com.transretail.crm.loyalty.dto.ManufactureOrderReceiveDto;
import com.transretail.crm.loyalty.dto.MultipleAllocationDto;
import com.transretail.crm.loyalty.dto.ProductSeriesDto;
import com.transretail.crm.loyalty.dto.ReceiveOrderDto;
import com.transretail.crm.loyalty.entity.LoyaltyCardInventory;
import com.transretail.crm.loyalty.entity.ManufactureOrder;
import com.transretail.crm.loyalty.entity.ManufactureOrderTier;
import com.transretail.crm.loyalty.entity.QLoyaltyCardInventory;
import com.transretail.crm.loyalty.repo.LoyaltyCardInventoryRepo;
import com.transretail.crm.loyalty.repo.ManufactureOrderRepo;
import com.transretail.crm.loyalty.service.LoyaltyAnnualFeeService;
import com.transretail.crm.loyalty.service.LoyaltyCardHistoryService;
import com.transretail.crm.loyalty.service.LoyaltyCardService;
import com.transretail.crm.loyalty.service.ManufactureOrderService;


@Service("loyaltyCardService")
public class LoyaltyCardServiceImpl implements LoyaltyCardService {

	@Autowired
	ManufactureOrderRepo manufactureOrderRepo;
	@Autowired
	ManufactureOrderService manufactureOrderService;
	@Autowired
	LoyaltyCardInventoryRepo loyaltyCardInventoryRepo;
	@Autowired
	CodePropertiesService codePropertiesService;
	@Autowired
	IdGeneratorService referenceNoGeneratorService;
	@Autowired
    StatelessSession statelessSession;
	@Autowired
	MessageSource messageSource;
	@Autowired
	LoyaltyCardHistoryService loyaltyCardHistoryService;
	@Autowired
	LoyaltyAnnualFeeService loyaltyAnnualFeeService;
	@Autowired
	StoreService storeService;
	@Autowired
	LookupService lookupService;
	@Autowired
    ApplicationConfigRepo applicationConfigRepo;
	@Autowired
	PointsTxnManagerService pointsService;
	@Autowired
    PointsTxnModelRepo pointsRepo;
	@PersistenceContext
	EntityManager em;
    @Autowired
    private AsyncTaskExecutor asyncTaskExecutor;
	@Value("#{'${hibernate.jdbc.batch_size}'}")
    private int batchSize;

	private static final Logger LOGGER = LoggerFactory.getLogger(LoyaltyCardServiceImpl.class);

	private static final DateTimeFormatter FULL_DATE_PATTERN = DateTimeFormat.forPattern("MMMM d, yyyy");


	@Override
    @Transactional
	public boolean validateInventory(String location, String transferTo, LookupDetail cardType, String startingSeries, String endingSeries, List<LookupDetail> status) {
		QLoyaltyCardInventory qInventory = QLoyaltyCardInventory.loyaltyCardInventory;
		List<BooleanExpression> expressions = Lists.newArrayList();
		if(location != null) {
			expressions.add(qInventory.location.eq(location));
		}
		if(transferTo != null) {
			expressions.add(qInventory.transferTo.eq(transferTo));
		}
		if(cardType != null) {
			expressions.add(qInventory.product.name.eq(cardType));
		}
		if(status != null) {
			expressions.add(qInventory.status.in(status));
		}
		BooleanExpression filter = BooleanExpression.allOf(expressions.toArray(new BooleanExpression[expressions.size()]));
		if(loyaltyCardInventoryRepo.count(filter.and(qInventory.sequence.eq(startingSeries))) == 0 ||
				loyaltyCardInventoryRepo.count(filter.and(qInventory.sequence.eq(endingSeries))) == 0) {
			return false;
		}
		Long starting = Long.valueOf(startingSeries);
		Long ending = Long.valueOf(endingSeries);
		Long qty = ending - starting + 1;
		long count = loyaltyCardInventoryRepo.count(filter.and(qInventory.sequence.castToNum(Long.class).between(starting, ending)));
		if(count != qty) {
			return false;
		}
		return true;
	}

	@Override
    @Transactional(readOnly = true)
	public Long[] countIncorrectTransferTo(MultipleAllocationDto dto) {
		QLoyaltyCardInventory qInventory = QLoyaltyCardInventory.loyaltyCardInventory;
		List<BooleanExpression> expressions = Lists.newArrayList();
		Long count = 0L;
		for(AllocationDto allocation: dto.getAllocations()) {
			expressions.add(qInventory.sequence.castToNum(Long.class).between(Long.valueOf(allocation.getStartingSeries()), Long.valueOf(allocation.getEndingSeries())));
			count += allocation.getQuantity();
		}
		BooleanExpression seriesfilter = BooleanExpression.anyOf(expressions.toArray(new BooleanExpression[expressions.size()]));
		return new Long[]{count, loyaltyCardInventoryRepo.count(seriesfilter
				.and(qInventory.transferTo.ne(dto.getTransferTo())))};
		/*return loyaltyCardInventoryRepo.count(seriesfilter
				.and(qInventory.transferTo.ne(dto.getTransferTo())));*/
	}


	@Override
	public String generateCardNumber(String cardNumber) {
		int oddSum = 0;
		int evenSum = 0;

		for(int i=0; i<cardNumber.length(); i++) {
			char digit = cardNumber.charAt(i);

			if((i&1) == 0)
				evenSum += Character.getNumericValue(digit);
			else
				oddSum += Character.getNumericValue(digit);
		}

		int checksum = (10 - ((3 * oddSum + evenSum) % 10)) % 10;

		return cardNumber+checksum;
	}

	@Override
    @Transactional
	public void generateLoyaltyInventory(ManufactureOrderDto orderDto) {
		ManufactureOrder order = manufactureOrderRepo.findOne(orderDto.getId());
		final List<ManufactureOrderTier> tiers = order.getTiers();
		final Long moId = orderDto.getId();
		Runnable task = new Runnable() {
			@Override
			public void run() {
				for(final ManufactureOrderTier tier : tiers) {
					final Long starting = Long.valueOf(tier.getStartingSeries());
					final Long ending = starting + tier.getQuantity() - 1;
					int insertCounter = 0;
					for(Long i = starting; i <= ending; i++) {
						LoyaltyCardInventory in = new LoyaltyCardInventory();
						String sequence = String.format("%012d", i);
						in.setSequence(sequence);
						in.setBarcode(generateCardNumber(sequence));
						in.setProduct(tier);
                        in.setExpiryDate(new LocalDate().plusMonths(getExpiryInMonths() + 1).withDayOfMonth(1));
						if(insertCounter < batchSize) {
							loyaltyCardInventoryRepo.save(in);
			                insertCounter++;
			            } else {
			            	loyaltyCardInventoryRepo.saveAndFlush(in);
			                em.clear();
			                insertCounter = 0;
			            }
					}		
				}
				manufactureOrderService.updateMoStatus(moId, new LookupDetail(codePropertiesService.getDetailMoStatusApproved()));
			}
		};
		asyncTaskExecutor.execute(task);
	}
	
	@Override
    @Transactional(readOnly = true)
	public File printLoyaltyInventory(String moNo) {
		QLoyaltyCardInventory qLoyaltyCardInventory = QLoyaltyCardInventory.loyaltyCardInventory;
		Map<String, Boolean> order = Maps.newHashMap();
		order.put("barcode", true);
		PagingParam pagination = new PagingParam();
		pagination.setOrder(order);
		
		Iterable<LoyaltyCardInventory> ins = loyaltyCardInventoryRepo.findAll(
		        qLoyaltyCardInventory.product.manufactureOrder.moNo.eq(moNo),
				SpringDataPagingUtil.INSTANCE.createOrderSpecifier(pagination, LoyaltyCardInventory.class));
		StringBuffer content = new StringBuffer();
		int i = 0;
		for(LoyaltyCardInventory in : ins) {
			content.append(++i)
				.append(".\t")
				.append(moNo)
				.append("\t")
				.append(in.getProduct().getBatchNo())
				.append("\t")
				.append(in.getBarcode())
				.append("\t")
				.append(in.getProduct().getName().getCode())
				.append(" - ")
				.append(in.getProduct().getName().getDescription())
				.append("\n");
		}

		FileOutputStream encFile = null;
		FileInputStream origFile = null;
		try {
			File original = File.createTempFile("temp", ".txt");
			File encrypted = File.createTempFile("tempenc", ".txt");

			PrintWriter out = new PrintWriter(original);
	        out.print(content.toString());
	        out.close();

			encFile = new FileOutputStream(encrypted);
			origFile = new FileInputStream(original);
			FileCryptor.encrypt(FileCryptor.DEFAULT_KEY, origFile, encFile);

			original.deleteOnExit();
			encrypted.deleteOnExit();

			return encrypted;
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {encFile.close();} catch (IOException e) {}
			try {origFile.close();} catch (IOException e) {}
		}

		return null;
	}

	@Override
    @Transactional
	public void receiveOrders(final ReceiveOrderDto orderDto) {
		LOGGER.info("[RECEIVE LC INVENTORIES][STARTED] orderId : " + orderDto.getManufactureOrder());
		loyaltyCardInventoryRepo.receiveOrders(orderDto);
		LookupDetail inactive = new LookupDetail( codePropertiesService.getDetailLoyaltyStatusInactive() );
		for( ProductSeriesDto productDto : orderDto.getProducts() ) {
			loyaltyCardHistoryService.saveHistoryForInventories(productDto.getStartingSeries(), productDto.getEndingSeries(), inactive, orderDto.getReceivedAt(), null, null, true);
		}
		LOGGER.info("[RECEIVE LC INVENTORIES][FINISHED] orderId : " + orderDto.getManufactureOrder());
	}

	@Override
    @Transactional
	public void allocateInventory(MultipleAllocationDto dto, String batchRefNo) {
		LookupDetail forAllocation = new LookupDetail(codePropertiesService.getDetailLoyaltyStatusForAllocation());
		for(AllocationDto seriesDto: dto.getAllocations()) {
			seriesDto.setEncodedBy(dto.getEncodedBy());
			seriesDto.setLocation(dto.getLocation());
			seriesDto.setAllocateDate(dto.getAllocateDate());
			seriesDto.setTransferTo(dto.getTransferTo());
			seriesDto.setReferenceNo(referenceNoGeneratorService.generateId());
			seriesDto.setBatchRefNo(batchRefNo);
			dto.setLocationName(getInventoryDesc(dto.getLocation()));
			loyaltyCardInventoryRepo.saveInventory(seriesDto, forAllocation);
			loyaltyCardHistoryService.saveHistoryForInventories(seriesDto.getStartingSeries(), seriesDto.getEndingSeries(), forAllocation, dto.getLocation(), dto.getTransferTo(), null, true);
		}
		dto.setTransferToName(getInventoryDesc(dto.getTransferTo()));
//		dto.setLocationName(getInventoryDesc(dto.getLocation()));
	}

	@Override
    @Transactional(readOnly = true)
	public JRProcessor createAllocateJrProcessor(MultipleAllocationDto allocateDto) {
    	Map<String, Object> parameters = Maps.newHashMap();
    	parameters.put("allocateDate", FULL_DATE_PATTERN.print(allocateDto.getAllocateDate()));
    	parameters.put("transferTo", allocateDto.getTransferTo() + " - " + getInventoryDesc(allocateDto.getTransferTo()));
    	parameters.put("location", allocateDto.getLocation() + " - " + getInventoryDesc(allocateDto.getLocation()));
    	parameters.put("encodedBy", allocateDto.getEncodedBy());
    	List<AllocationDto> beansDataSource = Lists.newArrayList();
    	if (allocateDto.getAllocations().isEmpty()) {
            parameters.put("isEmpty", true);
            beansDataSource.add(new AllocationDto());
        }
        DefaultJRProcessor jrProcessor = new DefaultJRProcessor("reports/loyalty_allocate_document.jasper", allocateDto.getAllocations());
        jrProcessor.addParameters(parameters);
        jrProcessor.setFileResolver(new ClasspathFileResolver("reports"));
        return jrProcessor;
    }

	@Override
    @Transactional(readOnly = true)
	public JRProcessor createReceiveJrProcessor(MultipleAllocationDto allocateDto) {
    	Map<String, Object> parameters = Maps.newHashMap();
    	parameters.put("receiveDate", FULL_DATE_PATTERN.print(new LocalDate()));
    	parameters.put("transferTo", allocateDto.getTransferTo() + " - " + getInventoryDesc(allocateDto.getTransferTo()));
    	parameters.put("location", allocateDto.getLocation() + " - " + getInventoryDesc(allocateDto.getLocation()));
    	parameters.put("cardType", allocateDto.getCardType().getDescription());
    	parameters.put("receivedBy", UserUtil.getCurrentUser().getUsername());
    	QLoyaltyCardInventory qInventory = QLoyaltyCardInventory.loyaltyCardInventory;
    	
    	List<AllocationDto> beansDataSource = Lists.newArrayList();
    	if (allocateDto.getAllocations().isEmpty()) {
            parameters.put("isEmpty", true);
            beansDataSource.add(new AllocationDto());
        } else {
        	for(AllocationDto allocation: allocateDto.getAllocations()) {
        		if(loyaltyCardInventoryRepo.count(qInventory.sequence.castToNum(Long.class).between(Long.valueOf(allocation.getStartingSeries()), Long.valueOf(allocation.getEndingSeries()))
        				.and(qInventory.status.code.eq(codePropertiesService.getDetailLoyaltyStatusTransferIn()))) > 0) {
        			beansDataSource.add(allocation);
        		}
        	}
        }
    	
        DefaultJRProcessor jrProcessor = new DefaultJRProcessor("reports/loyalty_receive_document.jasper", beansDataSource);
        jrProcessor.addParameters(parameters);
        jrProcessor.setFileResolver(new ClasspathFileResolver("reports"));
        return jrProcessor;
    }

	@Override
    @Transactional
	public void receiveInventory(final MultipleAllocationDto dto) {
		LOGGER.info("[RECEIVE LC INVENTORIES FROM ALLOCATION][STARTED]");
		final LookupDetail transferIn = new LookupDetail(codePropertiesService.getDetailLoyaltyStatusTransferIn());
		for(final AllocationDto seriesDto: dto.getAllocations()) {
			seriesDto.setEncodedBy(null);
			seriesDto.setLocation(dto.getTransferTo());
			seriesDto.setAllocateDate(null);
			seriesDto.setTransferTo(null);
			seriesDto.setReferenceNo(null);
			loyaltyCardInventoryRepo.saveInventory(seriesDto, transferIn);
			
			asyncTaskExecutor.execute(new Runnable() {
				@Override
				public void run() {
					QLoyaltyCardInventory qInventory = QLoyaltyCardInventory.loyaltyCardInventory;
					List<LoyaltyCardInventory> inventories = Lists.newArrayList(loyaltyCardInventoryRepo.findAll(qInventory.sequence.castToNum(Long.class).between(Long.valueOf(seriesDto.getStartingSeries()), Long.valueOf(seriesDto.getEndingSeries()))
							.and(qInventory.status.eq(transferIn))));
					loyaltyCardHistoryService.saveHistoryForInventories(transferIn, dto.getTransferTo(), null, dto.getLocation(), inventories, true);		
				}
			});

			
		}
		LOGGER.info("[RECEIVE LC INVENTORIES FROM ALLOCATION][FINISHED]");
	}

	@Override
    @Transactional
	public Set<String> saveInventory(MultipleAllocationDto dto, LookupDetail newStatus) {
		Set<String> unapproved = Sets.newHashSet();
		for(AllocationDto allocationDto: dto.getAllocations()) {
			if(StringUtils.isNotBlank(allocationDto.getReferenceNo())) {
				loyaltyCardInventoryRepo.saveInventory(allocationDto.getReferenceNo(), newStatus);
				loyaltyCardHistoryService.saveHistoryForInventories(allocationDto.getReferenceNo(), newStatus, allocationDto.getLocation(), allocationDto.getTransferTo(), null, true);
			}
			else {
				unapproved.add(allocationDto.getBatchRefNo());
			}
		}
		return unapproved;
	}


	@SuppressWarnings("unchecked")
	@Override
    @Transactional(readOnly = true)
	public MultipleAllocationDto getInventories(LookupDetail cardType, LookupDetail status, String location, String transferTo, String newTransferTo) {
		MultipleAllocationDto dto = new MultipleAllocationDto();
		dto.setCardType(cardType);
		dto.setStatus(status);
		dto.setLocation(location);
		dto.setLocationName(getInventoryDesc(location));
		dto.setTransferTo(transferTo);
		dto.setTransferToName(getInventoryDesc(transferTo));
		dto.setNewTransferTo(newTransferTo);
		dto.setNewTransferToName(getInventoryDesc(newTransferTo));
		Object[] result = loyaltyCardInventoryRepo.getInventories(cardType, status, location, transferTo, newTransferTo);
		dto.setAllocations((List<AllocationDto>) result[0]);
		dto.setBatchRefNo((Set<String>) result[1]);
		return dto;
	}

	@Override
    @Transactional(readOnly = true)
	public LoyaltyCardInventorySummaryResultList searchLoyaltyInventories(LoyaltyCardInventorySearchDto searchDto) {
		return loyaltyCardInventoryRepo.searchLoyaltyCardInventories(searchDto);
	}

	@Override
    @Transactional(readOnly = true)
    public LoyaltyCardInventoryResultList searchInventoryItems(LoyaltyCardInventorySearchDto searchDto) {
		Pageable pageable = SpringDataPagingUtil.INSTANCE.createPageable(searchDto.getPagination());
        BooleanExpression filter = searchDto.createItemsSearchExpression();
        Page<LoyaltyCardInventory> page = filter != null ? loyaltyCardInventoryRepo.findAll(filter, pageable) : loyaltyCardInventoryRepo.findAll(pageable);
        return new LoyaltyCardInventoryResultList(toDtos(page.getContent()), page.getTotalElements(), page.hasPreviousPage(),
            page.hasNextPage());
	}

	private List<LoyaltyCardInventoryDto> toDtos(Collection<LoyaltyCardInventory> list) {
        List<LoyaltyCardInventoryDto> results = Lists.newArrayList();
        for (LoyaltyCardInventory model : list) {
            results.add(new LoyaltyCardInventoryDto(model));
        }
        return results;
    }


	@Override
    @Transactional(readOnly = true)
	public LoyaltyCardInventory findLoyaltyCard( String inBarcodeNo ) {
		if ( StringUtils.isBlank( inBarcodeNo ) ) {
			return null;
		}
		return loyaltyCardInventoryRepo.findOne( QLoyaltyCardInventory.loyaltyCardInventory.barcode.equalsIgnoreCase( inBarcodeNo ) );
	}

	@Override
    @Transactional
	public LoyaltyCardInventory activateLoyaltyCard( String inBarcodeNo, LocalDate expiryDate, Long memberId ) {
		LoyaltyCardInventory theCard = loyaltyCardInventoryRepo.findOne( QLoyaltyCardInventory.loyaltyCardInventory.barcode.equalsIgnoreCase( inBarcodeNo ) );
		theCard.setStatus( new LookupDetail( codePropertiesService.getDetailLoyaltyStatusActive() ) );
		theCard.setActivationDate( new LocalDate() );
		if(expiryDate == null)
			expiryDate = new LocalDate().plusMonths(getExpiryInMonths() + 1).withDayOfMonth(1);
		theCard.setExpiryDate(expiryDate);
		if(memberId != null)
			theCard.setMemberId(memberId);
		return loyaltyCardInventoryRepo.save( theCard );
	}

	@Override
    @Transactional(readOnly = true)
	public int getExpiryInMonths() {
		ApplicationConfig config = applicationConfigRepo.findByKey(AppKey.LOYALTY_EXPIRY_IN_MONTHS);
        if(config == null || StringUtils.isBlank(config.getValue())) {
        	config = new ApplicationConfig();
        	config.setKey(AppKey.LOYALTY_EXPIRY_IN_MONTHS);
        	config.setValue(AppConfigDefaults.DEFAULT_LOYALTY_EXPIRY_IN_MONTHS);
        	applicationConfigRepo.save(config);
        }
        return Integer.parseInt(config.getValue());
	}

	@Override
    @Transactional
	public LoyaltyCardInventory replaceLoyaltyCard( String inBarcodeNo ) {
		LoyaltyCardInventory theCard = loyaltyCardInventoryRepo.findOne( QLoyaltyCardInventory.loyaltyCardInventory.barcode.equalsIgnoreCase( inBarcodeNo ) );
		theCard.setStatus( new LookupDetail( codePropertiesService.getDetailLoyaltyStatusDisabled() ) );
		return loyaltyCardInventoryRepo.save( theCard );
	}

	@Override
    @Transactional(readOnly = true)
	public List<LookupDetail> getCardTypes(String startingSeries, String endingSeries) {
		return loyaltyCardInventoryRepo.getCardTypes(startingSeries, endingSeries);
	}

	@Override
    @Transactional
	public void multipleAllocateInventory(MultipleAllocationDto dto, String batchRefNo) {
		LookupDetail forAllocation = new LookupDetail(codePropertiesService.getDetailLoyaltyStatusForAllocation());
		for(AllocationDto seriesDto: dto.getAllocations()) {
			seriesDto.setEncodedBy(dto.getEncodedBy());
			seriesDto.setLocation(dto.getLocation());
			seriesDto.setAllocateDate(dto.getAllocateDate());
			seriesDto.setReferenceNo(referenceNoGeneratorService.generateId());
			seriesDto.setBatchRefNo(batchRefNo);
			loyaltyCardInventoryRepo.saveInventory(seriesDto, forAllocation);
			loyaltyCardHistoryService.saveHistoryForInventories(seriesDto.getStartingSeries(), seriesDto.getEndingSeries(), forAllocation, dto.getLocation(), seriesDto.getTransferTo(), null, true);
		}
		dto.setTransferToName(getInventoryDesc(dto.getTransferTo()));
//		dto.setLocationName(getInventoryDesc(dto.getLocation()));
	}

	@SuppressWarnings("unchecked")
	@Override
    @Transactional(readOnly = true)
	public MultipleAllocationDto getInventoriesForMultipleApproval(String location) {
		MultipleAllocationDto dto = new MultipleAllocationDto();
		dto.setLocation(location);
		dto.setLocationName(getInventoryDesc(location));
		Object[] result = loyaltyCardInventoryRepo.getInventoriesForApproval(location);
		dto.setAllocations((List<AllocationDto>) result[0]);
		dto.setBatchRefNo((Set<String>) result[1]);
		return dto;
	}
	
	@SuppressWarnings("unchecked")
	@Override
    @Transactional(readOnly = true)
	public MultipleAllocationDto getInventoriesForBurning() {
		MultipleAllocationDto dto = new MultipleAllocationDto();
		Object[] result = loyaltyCardInventoryRepo.getInventoriesForBurning();
		dto.setAllocations((List<AllocationDto>) result[0]);
		dto.setBatchRefNo((Set<String>) result[1]);
		return dto;
	}

	@Override
    @Transactional(readOnly = true)
	public String getInventoryDesc(String inventoryCode) {
		if(StringUtils.isNotBlank(inventoryCode)) {
			Store store = storeService.getStoreByCode(inventoryCode);
			if(store != null)
				return store.getName();
			else {
				LookupDetail inv = lookupService.getDetailByCode(inventoryCode);
				if(inv != null)
					return inv.getDescription();
			}
		}
		return "";
	}

	@Override
    @Transactional(readOnly = true)
	public Map<String, String> getInventoryLocationsForTracking() {
		Map<String, String> inv = Maps.newLinkedHashMap();

		CustomSecurityUserDetailsImpl userDetails = UserUtil.getCurrentUser();
		String userLocation = userDetails.getInventoryLocation();

		LookupDetail headOffice = lookupService.getDetailByCode(codePropertiesService.getDetailInvLocationHeadOffice());
		if(StringUtils.isNotBlank(userLocation)) {
			if(headOffice.getCode().equals(userLocation)) {
				inv.put(headOffice.getCode(), headOffice.getDescription());
				for(Store store: storeService.getAllStores()) {
					inv.put(store.getCode(), store.getName());
				}
			} else {
				inv.put(userLocation, storeService.getStoreByCode(userLocation).getName());
			}
		}

		return inv;
	}

	@Override
    @Transactional(readOnly = true)
	public Map<String, String> getInventoryLocations() {
		Map<String, String> inv = Maps.newLinkedHashMap();

		LookupDetail headOffice = lookupService.getDetailByCode(codePropertiesService.getDetailInvLocationHeadOffice());
		inv.put(headOffice.getCode(), headOffice.getDescription());
		for(Store store: storeService.getAllStores()) {
			inv.put(store.getCode(), store.getCodeAndName());
		}

		return inv;
	}

	@Override
    @Transactional
	public MessageDto saveForBurning(MultipleAllocationDto dto, Boolean forBurning, String batchRefNo) {
		for(AllocationDto seriesDto: dto.getAllocations()) {
			seriesDto.setReferenceNo(referenceNoGeneratorService.generateId());
			seriesDto.setEncodedBy(dto.getEncodedBy());
			seriesDto.setAllocateDate(dto.getAllocateDate());
			seriesDto.setBatchRefNo(batchRefNo);
			loyaltyCardInventoryRepo.saveForBurning(seriesDto, forBurning);
			/*loyaltyCardHistoryService.saveHistoryForInventories(seriesDto.getStartingSeries(), seriesDto.getEndingSeries(), status, dto.getLocation(), null, null, true);*/
		}
		
		dto.setLocationName(getInventoryDesc(dto.getLocation()));
		String message = messageSource.getMessage("burn_card_approval_msg", new Object[]{UserUtil.getCurrentUser().getFullName(),
				dto.getLocation(),
				getInventoryDesc(dto.getLocation())
				}, LocaleContextHolder.getLocale());
		
		/*String message = "Dear Merchant Service Supervisor,\r\n"+
			UserUtil.getCurrentUser().getFullName() + " is requesting for approval to burn defective loyalty cards in " + dto.getLocation() + " - " + getInventoryDesc(dto.getLocation()) + ".\r\n"+
			"Please login to Carrefour MMS http://crm.trid-corp.net to approve the request.\r\n"+
			"Thank you.";*/
        List<String> recipients = getRecipients();
        MessageDto messageDto = null;
        if (CollectionUtils.isNotEmpty(recipients)) {
            messageDto = new MessageDto();
            messageDto.setRecipients(recipients.toArray(new String[recipients.size()]));
            messageDto.setMessage(message);
            messageDto.setSubject(messageSource.getMessage("burn_card_approval_subj", null, LocaleContextHolder.getLocale()));
        }
        return messageDto;
    }
	
	@Override
    @Transactional
	public Set<String> burnInventory(MultipleAllocationDto dto, Boolean approved) {
		LookupDetail status = new LookupDetail(codePropertiesService.getDetailLoyaltyStatusBurned());
		Set<String> unapproved = Sets.newHashSet();
		for(AllocationDto seriesDto: dto.getAllocations()) {
			if(StringUtils.isNotBlank(seriesDto.getReferenceNo())) {
				loyaltyCardInventoryRepo.approveBurnInventory(seriesDto.getReferenceNo(), approved);
				if(BooleanUtils.isTrue(approved))
					loyaltyCardHistoryService.saveHistoryForInventories(seriesDto.getReferenceNo(), status, seriesDto.getLocation(), null, null, true);	
			}
			else {
				unapproved.add(seriesDto.getBatchRefNo());
			}
		}
		return unapproved;
	}

	@Override
    @Transactional(readOnly = true)
	public List<ManufactureOrderReceiveDto> getReceivedInventories(Long manufactureOrder) {
		return loyaltyCardInventoryRepo.getReceivedInventories(manufactureOrder);
	}

    @Override
    @Transactional(readOnly = true)
    public Boolean isCardExpired(LoyaltyCardInventory loyaltyCardInventory) {

        if (loyaltyCardInventory != null && loyaltyCardInventory.getExpiryDate() != null) {

            if (loyaltyCardInventory.getExpiryDate().isAfter(new LocalDate())) {
                return false;
            }

        }

        return true;
    }

    @Override
    @Transactional(readOnly = true)
    public LCInvResultList searchAll(LCInvSearchDto dto) {
        QLoyaltyCardInventory qLoyaltyCardInventory = QLoyaltyCardInventory.loyaltyCardInventory;
        QLookupDetail qCardType = new QLookupDetail("cardType");
        QLookupDetail qStatus = new QLookupDetail("cardStatus");

        BooleanExpression expression = dto.createSearchExpression();
        JPQLQuery selectQuery = createQuery(expression, qLoyaltyCardInventory)
            .leftJoin(qLoyaltyCardInventory.product.name, qCardType)
            .leftJoin(qLoyaltyCardInventory.status, qStatus);

        PagingParam pagination = dto.getPagination();
        SpringDataPagingUtil.INSTANCE.applyPagination(selectQuery, pagination, LoyaltyCardInventory.class);
        List<LCInvResultDto> list = selectQuery.list(ConstructorExpression.create(LCInvResultDto.class, qLoyaltyCardInventory.barcode,
            qCardType.description, qStatus.description, qLoyaltyCardInventory.location, qStatus.code));
        if (list != null) {
            for (LCInvResultDto resultDto : list) {
                if ("INVT001".equals(resultDto.getLocation())) {
                    resultDto.setLocationName("HEAD OFFICE");
                } else {
                    Store store = storeService.getStoreByCode(resultDto.getLocation());
                    if (store != null) {
                        resultDto.setLocationName(store.getName());
                    }
                }
            }
        }
        JPQLQuery countQuery = createQuery(expression, qLoyaltyCardInventory);
        Long totalElements = countQuery.uniqueResult(qLoyaltyCardInventory.id.count());

        LCInvResultList resultList = new LCInvResultList(list, totalElements != null ? totalElements : 0, pagination.getPageNo(), pagination.getPageSize());
        setAnnualFee( resultList );
        return resultList;
    }

    private void setAnnualFee( LCInvResultList resultList ) {
    	if ( null != resultList && CollectionUtils.isNotEmpty( resultList.getResults() ) ) {
    		for ( LCInvResultDto dto : resultList.getResults() ) {
    			//if ( dto.getStatusCode().equalsIgnoreCase( codePropertiesService.getDetailLoyaltyStatusActive() ) ) {
        			LoyaltyAnnualFeeSchemeDto feeScheme = loyaltyAnnualFeeService.getScheme( dto.getCardNo() );
        			dto.setAnnualFee( null != feeScheme? feeScheme.getAnnualFee() : null );
    			//}
    		}
    	}
    }

    private JPQLQuery createQuery(BooleanExpression expression, QLoyaltyCardInventory qLoyaltyCardInventory) {
        JPQLQuery query = new JPAQuery(em).from(qLoyaltyCardInventory);
        if (expression != null) {
            query = query.where(expression);
        }
        return query;
    }

    @Override
    @Transactional(readOnly = true)
    public void expirePoints() {
    	expirePoints(new LocalDate());
    }

    @Override
    @Transactional
    public void expirePoints(LocalDate expireDate) {
    	for(MemberModel member: loyaltyCardInventoryRepo.getMembersWithExpiry(expireDate.minusMonths(getPointsExpiryInMonths()))) {
    		List<PointsTxnModel> pointsToExpire = pointsRepo.sumPointsToExpireForLoyalty(member.getAccountId(), expireDate);
    		pointsService.expirePoints(member, pointsToExpire, expireDate, false);
    	}
    }

    private int getPointsExpiryInMonths() {
		ApplicationConfig config = applicationConfigRepo.findByKey(AppKey.EXPIRE_POINTS_FOR_LOYALTY_IN_MONTHS);
        if(config == null || StringUtils.isBlank(config.getValue())) {
        	config = new ApplicationConfig();
        	config.setKey(AppKey.EXPIRE_POINTS_FOR_LOYALTY_IN_MONTHS);
        	config.setValue(AppConfigDefaults.DEFAULT_LOYALTY_EXPIRE_POINTS_IN_MONTHS);
        	applicationConfigRepo.save(config);
        }
        return Integer.parseInt(config.getValue());
	}

    @Override
    @Transactional(readOnly = true)
    public JRProcessor createReprintJrProcessor(String transactionType, String username, LocalDate dateFrom, LocalDate dateTo) {
    	List<MultipleAllocationDto> transactions = loyaltyCardHistoryService.getTransaction(transactionType, username, dateFrom, dateTo);

    	Map<String, Object> parameters = Maps.newHashMap();
    	parameters.put("transactionType", transactionType);
    	parameters.put("user", username);
    	parameters.put("dateFrom", FULL_DATE_PATTERN.print(dateFrom));
    	parameters.put("dateTo", FULL_DATE_PATTERN.print(dateTo));

    	if (transactions.isEmpty()) {
            parameters.put("isEmpty", true);
            parameters.put("noDataFound", messageSource.getMessage("label_no_data_found", null, LocaleContextHolder.getLocale()));
            transactions.add(new MultipleAllocationDto());
        }
        DefaultJRProcessor jrProcessor = new DefaultJRProcessor("reports/loyalty_reprint_transactions.jasper", transactions);
        jrProcessor.addParameters(parameters);
        jrProcessor.setFileResolver(new ClasspathFileResolver("reports"));
        return jrProcessor;
    }

    @Override
    @Transactional(readOnly = true)
    public List<MessageDto> sendAllocateToChangeForApproval(MultipleAllocationDto dto, String batchRefNo) {
        List<MessageDto> results = Lists.newArrayList();
        List<String> recipients = getRecipients();
        Set<String> origTransferTo = new HashSet<String>();
        dto.setLocationName(getInventoryDesc(dto.getLocation()));

        for (AllocationDto seriesDto : dto.getAllocations()) {
            seriesDto.setTransferTo(dto.getTransferTo());
            seriesDto.setReferenceNo(referenceNoGeneratorService.generateId());
            seriesDto.setBatchRefNo(batchRefNo);
            loyaltyCardInventoryRepo.sendAllocateToChangeForApproval(seriesDto);
            origTransferTo.addAll(loyaltyCardInventoryRepo.getOrginalLocation(seriesDto));
        }
        
        if (CollectionUtils.isNotEmpty(recipients)) {
            for (String originalTransferTo : origTransferTo) {
                String message = messageSource.getMessage("changealloc_approval_msg", new Object[]{
                    dto.getLocation(),
                    getInventoryDesc(dto.getLocation()),
                    originalTransferTo,
                    getInventoryDesc(originalTransferTo),
                    dto.getTransferTo(),
                    getInventoryDesc(dto.getTransferTo()),
                    UserUtil.getCurrentUser().getFullName()
                }, LocaleContextHolder.getLocale());


                MessageDto messageDto = new MessageDto();
                messageDto.setRecipients(recipients.toArray(new String[recipients.size()]));
                messageDto.setMessage(message);
                messageDto.setSubject(messageSource.getMessage("burn_card_approval_subj", null, LocaleContextHolder.getLocale()));
                results.add(messageDto);
            }
        }
        return results;
        /*String message = messageSource.getMessage("changealloc_approval_msg", null, LocaleContextHolder.getLocale());*/
		
		/*String message = "Dear Merchant Service Supervisor,\r\n"+
		"Loyalty cards from <STORE CODE - STORE NAME (originating store)> that are originally allocated to <STORE CODE - STORE NAME (allocate to)> have been delivered to <STORE CODE - STORE NAME (receiving store)>.\r\n" +
		"<FIRST NAME LAST NAME (requesting user)> is requesting for these loyalty cards to be allocated to <STORE CODE - STORE NAME (receiving store)>.\r\n" +
		"Please login to Carrefour MMS <http://crm.trid-corp.net> to approve the request.\r\n" +
		"Thank you.\r\n";
		sendEmailToMerchantSupervisor(messageSource.getMessage("changealloc_approval_subj", null, LocaleContextHolder.getLocale()), message);*/
	}

    @Override
    @Transactional
	public Set<String> approveAllocateToChange(MultipleAllocationDto dto) {
    	Set<String> remaining = Sets.newHashSet();
		for(AllocationDto seriesDto: dto.getAllocations()) {
			if(StringUtils.isNotBlank(seriesDto.getReferenceNo())) {
				loyaltyCardInventoryRepo.approveAllocateToChange(seriesDto.getReferenceNo());
				loyaltyCardHistoryService.saveHistoryForInventories(seriesDto.getReferenceNo(), new LookupDetail(codePropertiesService.getDetailLoyaltyStatusInTransit()), dto.getLocation(), dto.getNewTransferTo(), null, false);
			}
			else {
				remaining.add(seriesDto.getBatchRefNo());
			}
		}
		return remaining;
	}

    @Override
    @Transactional
	public Set<String> rejectAllocateToChange(MultipleAllocationDto dto) {
    	Set<String> remaining = Sets.newHashSet();
		for(AllocationDto seriesDto: dto.getAllocations()) {
			if(StringUtils.isNotBlank(seriesDto.getBatchRefNo())) {
				loyaltyCardInventoryRepo.rejectAllocateToChange(seriesDto.getReferenceNo());
			}
			else {
				remaining.add(seriesDto.getBatchRefNo());
			}
		}
		return remaining;
	}

    private List<String> getRecipients() {
        QUserRolePermission qRolePerm = QUserRolePermission.userRolePermission;
        return new JPAQuery(em).from(qRolePerm).where(
            qRolePerm.roleId.id.eq("ROLE_MERCHANT_SERVICE_SUPERVISOR")
                .and(qRolePerm.userId.inventoryLocation.eq(codePropertiesService.getDetailInvLocationHeadOffice()))
                .and(qRolePerm.userId.email.isNotNull())
                .and(qRolePerm.userId.email.isNotEmpty())
        )
            .distinct()
            .list(qRolePerm.userId.email);
    }
}
