$(document).ready( function() {

	var errorContainer = $( "#content_productGroupForm" ).find("#content_error");

	var $dialog = $( "#content_productGroupForm" );
	var $form = $( "#productGroupForm" );
	var $saveBtn = $( "#productGroupForm" ).find( "#saveButton" );

	var $itemCodeDesc =  $( "#itemCodeDesc" );
	var $excProductCodeDesc =  $( "#excProductCodeDesc" );

	var $classificationSelections = $( "#classificationSelections" );
	var $productSelections = $( "#productSelections" );

	var PREFIX_CODE = "dataCode";
	var CLASS_PRODUCT = $classificationSelections.data( "class-product" );//"productCode";
	var CLASS_CFN =  $classificationSelections.data( "class-cfn" );//"classificationCode";

	var $prdCfnFilterVal = $( "#filterPrdCfnForm" ).find( "#filterValue" );
	var $prdCfnFilterBtn = $( "#filterPrdCfnForm" ).find( "#filterButton" );
	var $prdFilterVal = $( "#filterPrdForm" ).find( "#filterValue" );
	var $prdFilterBtn = $( "#filterPrdForm" ).find( "#filterButton" );

	var $productCfnOptions = $form.find( ".productCfnOption" );

	var prdAjaxQ = null, cfnAjaxQ = new Array(), ajaxQ2 = null;




	initDialog();
	initFormFields();
	initCfnAjaxQ();



	function initCfnAjaxQ() {
		$productCfnOptions.each( function() {
			if ( !$(this).data( "is-productcode" ) ) {
				cfnAjaxQ[ $(this).data( "queue-num" ) - 0 ] = null;
			}
		});
	}

	var ProductCfnUtil = null, ProductExcUtil = null;
	var PRODUCTEXC_LIST_INDEX = 0, PRODUCTEXC_LIST_INC = 7;
	ProductExcUtil = {
			resetIndex			: function(reset) {
				PRODUCTEXC_LIST_INDEX = reset? 0 : PRODUCTEXC_LIST_INDEX + PRODUCTEXC_LIST_INC;
				enableUI();
			},
			loadProducts		: function($this, reset) {
				if ( reset ) {
					ProductExcUtil.resetIndex( true );
				}
				if ( null != ajaxQ2 ) {
					ajaxQ2.abort().done( function() { ajaxQ2 = null; });
				}
				var classificationCodes = $itemCodeDesc.find( $( "option." + CLASS_CFN ) ).map( function(){ return this.value; } ).get().join(",");
				var url = $this.data( "url" )
							.replace( new RegExp("%PRDCODE%", 'g'), /*productCodes*/"" )
							.replace( new RegExp("%CFNCODE%", 'g'), classificationCodes )
							.replace( new RegExp("%IDX%", 'g' ), PRODUCTEXC_LIST_INDEX )
							.replace( new RegExp("%SIZE%", 'g' ), PRODUCTEXC_LIST_INC );
				ajaxQ2 = $.ajaxQueue({
					url		: url,
					type	: "GET",
					success	: function( resp ) {
						if ( resp.success ) {
							var prdSelectedCodes = $excProductCodeDesc.find( "option" ).map( function(){ return this.value; } ).get().join(",").split(",");
							//$productSelections.empty();
							$.each( resp.result, function( idx ) {
								var elOption = new Option( this.code + " - " + this.description, this.code );
								if ( prdSelectedCodes.indexOf( this.code ) == -1 ) {
									$productSelections.append( elOption );
								}
							});
						}
						ProductExcUtil.resetIndex();
					}
				}).done( function() { ajaxQ2 = null; enableUI();});
			},
			oldLoadProductsMethod : function($this) {
				//var productCodes = $itemCodeDesc.find( $( "option." + CLASS_PRODUCT ) ).map( function(){ return this.value; } ).get().join(",");
				var classificationCodes = $itemCodeDesc.find( $( "option." + CLASS_CFN ) ).map( function(){ return this.value; } ).get().join(",");
				var url = $this.data( "url" ).replace( new RegExp("%PRDCODE%", 'g'), /*productCodes*/"" ).replace( new RegExp("%CFNCODE%", 'g'), classificationCodes );
				$.get( url, function( resp ) {
					if ( resp.success ) {
						var prdSelectedCodes = $excProductCodeDesc.find( "option" ).map( function(){ return this.value; } ).get().join(",").split(",");
						$productSelections.empty();
						//$excProductCodeDesc.empty();
						$.each( resp.result, function( idx ) {
							var elOption = new Option( this.code + " - " + this.description, this.code );
							if ( prdSelectedCodes.indexOf( this.code ) == -1 ) {
								$productSelections.append( elOption );
							}
						});
					}
					enableUI();
				});
			}
	};
	ProductCfnUtil = {
		resetPrdCfnIndex		: function( $this, reset ) {
			$this.data( "list-idx", reset? $this.data( "init-idx" ) : $this.data( "list-idx" ) - 0 + $this.data( "list-size" ) );
			$this.data( "fn-scroll", reset? false : true );
			enableUI();
		},
		processProductCfnResult	: function ( $this, resp ) {
			if ( resp.success ) {
				var selectionCodes = $classificationSelections.find( "option" ).map( function(){ return this.value; } ).get();
				var selectedCodes = $itemCodeDesc.find( "option" ).map( function(){ return this.value; } ).get();
				if ( resp.result ) {
					$.each( resp.result, function( idx ) {
						var isPrdCode =  $this.data( "is-productcode" );
						var code = "" + ( isPrdCode? this.itemCode : this.code );
						setProductCfnList( code, ( isPrdCode? this.sku : code ) + " - " + this.description, 
								$this.val(), isPrdCode, selectionCodes, selectedCodes, true );
					});
				}
			}
			enableUI();
		},
		loadProducts	: function($this) {
			if ( null != prdAjaxQ ) {
				prdAjaxQ.abort().done( function() { prdAjaxQ = null; });
				return;
			}
			if ( !$this.data( "fn-scroll" ) ) { return; }
			prdAjaxQ = $.ajaxQueue({
				url		: $this.data( "url" )
							.replace( new RegExp("%CODE%", 'g' ), $this.val() )
							.replace( new RegExp("%IDX%", 'g' ), $this.data( "list-idx" ) )
							.replace( new RegExp("%SIZE%", 'g' ), $this.data( "list-size" ) ),
				type	: "GET",
				success	: function( resp ) {
					ProductCfnUtil.processProductCfnResult( $this, resp );
					ProductCfnUtil.resetPrdCfnIndex( $this );
				}
			}).done( function() { prdAjaxQ = null; enableUI();});
		},
		loadCfns	: function($this) {
			var aQueue = cfnAjaxQ[ $this.data( "queue-num" ) - 0 ];
			if ( null != aQueue ) {
				aQueue.abort().done( function() { aQueue = null; });
				return;
			}
			if ( !$this.data( "fn-scroll" ) ) { return; }
			aQueue = $.ajaxQueue({
				url		: $this.data( "url" )
							.replace( new RegExp("%CODE%", 'g' ), $this.val() )
							.replace( new RegExp("%IDX%", 'g' ), $this.data( "list-idx" ) )
							.replace( new RegExp("%SIZE%", 'g' ), $this.data( "list-size" ) ),
				type	: "GET",
				success	: function( resp ) {
					ProductCfnUtil.processProductCfnResult( $this, resp );
					ProductCfnUtil.resetPrdCfnIndex( $this );
				}
			}).done( function() { aQueue = null; enableUI();});
		}
	};

	function initDialog() {
		$dialog.modal({ show : false });
		$dialog.on( "hide", function(e) { 
			if ( e.target === this ) { 
				errorContainer.hide();
				resetForm( $form, true );
				$form.attr( "action", $form.data( "save-action" ) );
				try {
					/*productGroupList.jsp => id:content_productGroupList*/
					$( "#content_productGroupList" ).find( "#list_productGroup" ).ajaxDataTable( "search" );
				} catch(e) {}
				$productCfnOptions.each( function(){ $(this).data( "list-idx", $(this).data( "init-idx" ) ); });
				if ( null != prdAjaxQ ) { prdAjaxQ.abort().done( function() { prdAjaxQ = null; }); }
				//if ( null != cfnAjaxQ ) { cfnAjaxQ.abort().done( function() { cfnAjaxQ = null; }); }
				if ( null != ajaxQ2 ) { ajaxQ2.abort().done( function() { ajaxQ2 = null; }); }
			} 
		});
		$dialog.on( "show", function(e) {
			if ( e.target === this ) {
				$itemCodeDesc.change();
			}
		});
	}

	function initFormFields() {
		$form.submit( saveForm );

		$itemCodeDesc.change( getProductList );
		$productCfnOptions.click( getProductCfnList );
		initFilterForms();

		$classificationSelections.dblclick( function(e) {
			$( "#addProductCfnBtn" ).trigger( "click" );
		});

		$itemCodeDesc.dblclick( function(e) {
			$( "#removeProductCfnBtn" ).trigger( "click" );
		});

		$productSelections.dblclick( function(e) {
			$( "#addExcProductBtn" ).trigger( "click" );
		});

		$excProductCodeDesc.dblclick( function(e) {
			$( "#removeExcProductBtn" ).trigger( "click" );
		});

		$( "#addProductCfnBtn" ).click( function(e) {
			e.preventDefault();
			$.each( $classificationSelections.find("option:selected"), function() { $itemCodeDesc.append( $(this) ); });
			$itemCodeDesc.change();
		});
		$( "#removeProductCfnBtn" ).click( function(e) {
			e.preventDefault();
			$.each( $itemCodeDesc.find("option:selected"), function() { $classificationSelections.append( $(this) ); });
			$itemCodeDesc.change();
		});

		$( "#addExcProductBtn" ).click( function(e) {
			e.preventDefault();
			$.each( $productSelections.find("option:selected"), function() { $excProductCodeDesc.append( $(this) ); });
		});
		$( "#removeExcProductBtn" ).click( function(e) {
			e.preventDefault();
			$.each( $excProductCodeDesc.find("option:selected"), function() { $productSelections.append( $(this) ); });
		});

	}



	function setProductCfnList( code, description, prefixCode, isProduct, selectionCodes, selectedCodes, doAppend ) {
		if ( selectedCodes.indexOf( code ) == -1 ) {
			var elOption = new Option( description, code );
			elOption.setAttribute( "class", ( PREFIX_CODE + prefixCode ) + " " + ( isProduct? CLASS_PRODUCT : CLASS_CFN ) );
			if ( doAppend && selectionCodes.indexOf( code ) != -1 ) {
				elOption = $classificationSelections.find( "option:eq(" + selectionCodes.indexOf( code ) + ")" );
				elOption.attr( "class", ( PREFIX_CODE + prefixCode ) + " " + ( isProduct? CLASS_PRODUCT : CLASS_CFN ) );
			}
			$classificationSelections.append( elOption );
		}
	}

	function getProductCfnList(e) {
	  disableUI();
		var $this = $(this);
		var $thisChecked = this.checked;
		//ProductCfnUtil.resetPrdCfnIndex( $this, false );
		if ( !$thisChecked ) {
			$classificationSelections.find( "." + PREFIX_CODE + $this.val() ).remove();
			ProductCfnUtil.resetPrdCfnIndex( $this, true );
			enableUI();
			return;
		}

		$prdCfnFilterVal.val( "" );
		$prdFilterVal.val( "" );
		$this.data( "fn-scroll", true );
		if ( $this.data( "is-productcode" ) ) {
			ProductCfnUtil.loadProducts( $this );
		}
		else {
			ProductCfnUtil.loadCfns( $this );
		}
		/*$.get( $this.data( "url" )
				.replace( new RegExp("%CODE%", 'g' ), $this.val() )
				.replace( new RegExp("%IDX%", 'g' ), "" )
				.replace( new RegExp("%SIZE%", 'g' ), "" ), 
			function( resp ) {
		ProductCfnUtil.processProductCfnResult( $this, resp );
		});*/
		$classificationSelections.scroll( function() {
			if( $classificationSelections.innerHeight() >= 
						( $classificationSelections.prop( "scrollHeight" ) - $classificationSelections.scrollTop() ) ) {
				$productCfnOptions.each( function() {
					if ( this.checked ) {
						if ( $(this).data( "is-productcode" ) ) { 
							ProductCfnUtil.loadProducts( $(this) );
						}
						else {
							ProductCfnUtil.loadCfns( $(this) );
						}
					}
				});
			}
		});
	}

	function getProductList(e) {
	  disableUI();
		var $this = $(this);
		if ( !$this.find( "option" ).length ) {
			$productSelections.empty();
			enableUI();
			return;
		}

		$productSelections.unbind( "scroll" ).scroll( function() {
			if( $productSelections.innerHeight() >= 
				( $productSelections.prop( "scrollHeight" ) - $productSelections.scrollTop() ) ) {
				ProductExcUtil.loadProducts( $this );
			}
		});
		$productSelections.empty();
		ProductExcUtil.loadProducts( $this, true );
	}

	function initFilterForms(e) {
		$prdCfnFilterBtn.click( searchProductCfnList );
		$prdCfnFilterVal.keypress( function(e) { if ( e.which == 13 ) searchProductCfnList(e); });

		$prdFilterBtn.click( searchProductList );
		$prdFilterVal.keypress( function(e) { if ( e.which == 13 ) searchProductList(e); });
	}

	function searchProductCfnList(e) {
	  disableUI();
		e.preventDefault();
		var $this = $(this);
		$this.prop( "disabled", true );
		if ( !$prdCfnFilterVal.val() ) {
			return;
		}

		var url = $prdCfnFilterBtn.data( "url-search" ).replace( new RegExp("%CODE%", 'g'), $prdCfnFilterVal.val() )
			.replace( new RegExp("%PRDCODE%", 'g'), 
				$( ".productCfnOption" + ":checkbox:checked" ).map( function(){ if( $(this).data( "is-productcode" ) ) { return this.checked; } } ).get() )
			.replace( new RegExp("%CFNCODE%", 'g'), 
				$( ".productCfnOption" + ":checkbox:checked" ).map( function(){ if( !$(this).data( "is-productcode" ) ) { return this.value; } } ).get().join(",") );
		$.get( url, function( resp ) { 
			if ( resp.success ) {
				var selectionCodes = $classificationSelections.find( "option" ).map( function(){ return this.value; } ).get().join(",").split(",");
				var selectedCodes = $itemCodeDesc.find( "option" ).map( function(){ return this.value; } ).get().join(",").split(",");
				$classificationSelections.empty();
				$.each( resp.result, function( idx ) {
					setProductCfnList( "" + this.code, ( this.product? this.sku : this.code ) + " - " + this.description, 
							this.type, this.product, selectionCodes, selectedCodes );
				});
			}
			$this.prop( "disabled", false );
			enableUI();
		});
	}

	function searchProductList(e) {
	  disableUI();
		e.preventDefault();
		if ( !$prdFilterVal.val() ) {
			return;
		}

		var url = $prdFilterBtn.data( "url-search" ).replace( new RegExp("%CODE%", 'g'), $prdFilterVal.val() )
			.replace( new RegExp( "%PRDCODE%", 'g' ), 
				$itemCodeDesc.find( $( "option." + CLASS_PRODUCT ) ).map( function(){ return this.value; } ).get().join(",") )
			.replace( new RegExp( "%CFNCODE%", 'g' ), 
				$itemCodeDesc.find( $( "option." + CLASS_CFN ) ).map( function(){ return this.value; } ).get().join(",") );
		$.get( url, function( resp ) { 
			if ( resp.success ) {
				var prdSelectedCodes = $excProductCodeDesc.find( "option" ).map( function(){ return this.value; } ).get().join(",").split(",");
				$productSelections.empty();
				$.each( resp.result, function( idx ) {
					var elOption = new Option( this.code + " - " + this.description, this.code );
					if ( prdSelectedCodes.indexOf( "" + this.code ) == -1 ) {
						$productSelections.append( elOption );
					}
				});
			}
			enableUI();
		});
	}

	function filterProductCfnList(e) {
	  disableUI();
		e.preventDefault();
		if ( !$prdCfnFilterVal.val() ) {
			return;
		}

		var productCodes = $classificationSelections.find( $( "option." + CLASS_PRODUCT ) ).map( function(){ return this.value; } ).get().join(",");
		var classificationCodes = $classificationSelections.find( $( "option." + CLASS_CFN ) ).map( function(){ return this.value; } ).get().join(",");
		var url = $prdCfnFilterBtn.data( "url" ).replace( new RegExp("%CODE%", 'g'), $prdCfnFilterVal.val() )
			.replace( new RegExp("%PRDCODE%", 'g'), productCodes ).replace( new RegExp("%CFNCODE%", 'g'), classificationCodes );
		$.get( url, function( resp ) { 
			if ( resp.success ) {
				var selectionCodes = $classificationSelections.find( "option" ).map( function(){ return this.value; } ).get().join(",").split(",");
				var selectedCodes = $itemCodeDesc.find( "option" ).map( function(){ return this.value; } ).get().join(",").split(",");
				$classificationSelections.empty();
				$.each( resp.result, function( idx ) {
					setProductCfnList( "" + this.code, ( this.product? this.sku : this.code ) + " " + this.description, 
							this.type, this.product, selectionCodes, selectedCodes );
				});
			}
			enableUI();
		});
	}

	function filterProductList(e) {
	  disableUI();
		e.preventDefault();
		if ( !$prdFilterVal.val() ) {
			return;
		}

		var productCodes = $productSelections.find( $( "option" ) ).map( function(){ return this.value; } ).get().join(",");
		var url = $prdCfnFilterBtn.data( "url" ).replace( new RegExp("%CODE%", 'g'), $prdFilterVal.val() )
			.replace( new RegExp("%PRDCODE%", 'g'), productCodes ).replace( new RegExp("%CFNCODE%", 'g'), "" );
		$.get( url, function( resp ) { 
			if ( resp.success ) {
				var prdSelectionCodes = $productSelections.find( "option" ).map( function(){ return this.value; } ).get().join(",").split(",");
				$productSelections.empty();
				$.each( resp.result, function( idx ) {
					var elOption = new Option( this.code + " - " + this.description, this.code );
					if ( prdSelectionCodes.indexOf( "" + this.code ) != -1 ) {
						$productSelections.append( elOption );
					}
				});
			}
			enableUI();
		});
	}



	function saveForm(e) {
		e.preventDefault();
		$saveBtn.prop( "disabled", true );
		$form.find( "#products" ).val( $productSelections.find( "option" ).map( function(){ return this.value; } ).get().join(",") );
		$form.find( "#productsCfns" ).val( $itemCodeDesc.find( "option" ).map( function(){ return this.value; } ).get().join(",") );
		$form.find( "#excludedProducts" ).val( $excProductCodeDesc.find( "option" ).map( function(){ return this.value; } ).get().join(",") );

		$.post( 
			$( this ).attr( "action" ), 
			$form.serialize(), 
			function( resp ) {
				if ( resp.success ) {
					$dialog.modal( "hide" );
				}
				else {
					var errorInfo = "";
					$.each( resp.result, function( key, value ) {
						errorInfo += ( ( key + 1 ) + ". " + value.code + "<br>" );
					});
					errorContainer.find( "div" ).html( errorInfo );
					errorContainer.show( "slow" );
				}
				$saveBtn.prop( "disabled", false );
			}
		);
	}

	function resetForm( ele, isEmpty ) {
	    $( ele ).find(':input').each(function() {
	        switch(this.type) {
	            case 'password':
	            case 'select-multiple': isEmpty? $(this).empty() : $(this).val("");
	            case 'select-one':
	            case 'text':
	            case 'textarea':
	            case 'hidden':
	                $(this).val("");
	                break;
	            case 'checkbox':
	            case 'radio':
	                this.checked = false;
	        }
	    });
	}

	function disableUI() {
	  $(".ui").prop("disabled", true);
	  $(".btnLoading").button("loading");
	}
	
	function enableUI() {
	  $(".ui").prop("disabled", false);
	  $(".btnLoading").button("reset");
	}
});