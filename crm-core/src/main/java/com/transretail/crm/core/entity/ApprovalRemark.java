package com.transretail.crm.core.entity;


import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.transretail.crm.core.entity.lookup.LookupDetail;
import com.transretail.crm.core.entity.support.CustomAuditableEntity;


@Table(name = "CRM_APPROVAL_REMARK")
@Entity
public class ApprovalRemark extends CustomAuditableEntity<Long> implements Serializable {

    private static final long serialVersionUID = 1L;


    @Column(name = "MODEL_ID", length = 50)
    private String modelId;

    @Column(name = "MODEL_TYPE", length = 50)
    private String modelType;

	@Column(name="REMARKS", length = 1000)
    private String remarks;

    @ManyToOne
    @JoinColumn(name="STATUS")
    private LookupDetail status;


    public String getModelId() {
		return modelId;
	}

	public void setModelId(String modelId) {
		this.modelId = modelId;
	}

	public String getModelType() {
		return modelType;
	}

	public void setModelType(String modelType) {
		this.modelType = modelType;
	}

	public String getRemarks() {
		return remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	public LookupDetail getStatus() {
		return status;
	}

	public void setStatus(LookupDetail status) {
		this.status = status;
	}
}
