package com.transretail.crm.giftcard.entity.support;

/**
 * @author ftopico
 */
public enum GiftCardDateExtendStatus {
    CREATED, APPROVED, REJECTED
}
