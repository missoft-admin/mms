package com.transretail.crm.giftcard.dto;

import java.util.List;

import org.joda.time.LocalDate;

import com.google.common.collect.Lists;

/**
 *
 */
public class GiftCardOrderReceiveDto {
	
	private Long id;
	private String receivedAt;
	private LocalDate receivedDate;
	private String deliveryReceipt;
	private String receivedBy;
	private List<GiftCardOrderReceiveItemDto> items = Lists.newArrayList(new GiftCardOrderReceiveItemDto());
	private List<GiftCardOrderSeriesDto> series = Lists.newArrayList();
	private List<GiftCardOrderReceivedSeriesDto> receivedSeries = Lists.newArrayList();
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	
	public String getReceivedAt() {
		return receivedAt;
	}
	public void setReceivedAt(String receivedAt) {
		this.receivedAt = receivedAt;
	}
	public LocalDate getReceivedDate() {
		return receivedDate;
	}
	public void setReceivedDate(LocalDate receivedDate) {
		this.receivedDate = receivedDate;
	}
	public String getDeliveryReceipt() {
		return deliveryReceipt;
	}
	public void setDeliveryReceipt(String deliveryReceipt) {
		this.deliveryReceipt = deliveryReceipt;
	}
	public List<GiftCardOrderReceiveItemDto> getItems() {
		return items;
	}
	public void setItems(List<GiftCardOrderReceiveItemDto> items) {
		this.items = items;
	}
	public List<GiftCardOrderSeriesDto> getSeries() {
		return series;
	}
	public void setSeries(List<GiftCardOrderSeriesDto> series) {
		this.series = series;
	}
	public List<GiftCardOrderReceivedSeriesDto> getReceivedSeries() {
		return receivedSeries;
	}
	public void setReceivedSeries(
			List<GiftCardOrderReceivedSeriesDto> receivedSeries) {
		this.receivedSeries = receivedSeries;
	}
	public String getReceivedBy() {
		return receivedBy;
	}
	public void setReceivedBy(String receivedBy) {
		this.receivedBy = receivedBy;
	}
	
	
	
    
}
