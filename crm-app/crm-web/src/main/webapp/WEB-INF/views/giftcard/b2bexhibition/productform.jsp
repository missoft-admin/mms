<%@ include file="../../common/taglibs.jsp" %>
<%@include file="../../common/messages.jsp" %>

<style type="text/css">
<!--

  #productTable tr td:last-child {
    border-left: 0px;
  }
  
-->
</style>



<div class="modal hide nofly" id="productDialog" tabindex="-1" role="dialog" aria-hidden="true">
<div class="modal-dialog">
  <div class="modal-content">
    
    <div class="modal-header">
      <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
      <h4><spring:message code="b2b.exhibition.scangc" /></h4>
    
    </div>
    
    <div class="modal-body">
    
      <div id="contentError" class="hide alert alert-error">
        <button type="button" class="close" onclick="$(this).parent().hide();">&times;</button>
        <div><form:errors path="*"/></div>
      </div>
    
      <form name="productForm" action="<c:url value="/giftcard/b2b/exhibition/validate?" />">
      
      <div class="contentMain">
        <div class="row-fluid">
          <div class="span4">
            <spring:message code="b2b.exhibition.startingseries" var="startingSeries" />
            <label for="">${startingSeries}</label>
            <input type="text" placeholder="${startingSeries}" name="startingSeries" id="startingSeries" class="intInput" maxlength="20" />
          </div>
          
          <div class="span4">
            <spring:message code="b2b.exhibition.endingseries" var="endingSeries" />
            <label for="">${endingSeries}</label>
            <input type="text" placeholder="${endingSeries}" name="endingSeries" id="endingSeries" class="intInput" maxlength="20" />
          </div>
          
          <div class="span4">
            <label for="">&nbsp;</label>
            <input type="button" id="addProductBtn" class="btn btn-primary" value="<spring:message code="label_add"/>" />
            <input type="button" class="btn" data-dismiss="modal" value="<spring:message code="label_close"/>" />
            <input type="button" id="clearProductBtn" class="btn" value="<spring:message code="label_clear"/>" />
          </div>
        </div>
      </div>
      
      
      </form>
      
      
      <div class="well">
      
      <label><spring:message code="b2b.exhibition.scannedcards" />:</label>
      
      <div data-is-empty="true" id="gcListContainer">
      <label><spring:message code="label_none" /></label>
      </div>
      
      </div>
    
    </div>
  </div>
</div>
</div>

<script type="text/javascript">
$(document).ready(function() {
	
	
});
</script>