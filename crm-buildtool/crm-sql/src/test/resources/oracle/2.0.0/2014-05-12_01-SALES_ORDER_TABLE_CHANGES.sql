    drop table CRM_SALES_ORD cascade constraints;

    drop table CRM_SALES_ORD_ITEM cascade constraints;
    
    create table CRM_SALES_ORD (
        id number(19,0) not null,
        CREATED_BY varchar2(255 char),
        CREATED_DATETIME timestamp,
        LAST_UPDATED_BY varchar2(255 char),
        LAST_UPDATED_DATETIME timestamp,
        CONTACT_EMAIL varchar2(255 char),
        CONTACT_NO varchar2(255 char),
        CONTACT_PERSON varchar2(255 char),
        NOTES varchar2(255 char),
        ORDER_DATE date,
        ORDER_NO varchar2(255 char),
        STATUS varchar2(255 char),
        CUSTOMER number(19,0),
        DISCOUNT number(19,0),
        primary key (id)
    );

    create table CRM_SALES_ORD_ITEM (
        id number(19,0) not null,
        CREATED_BY varchar2(255 char),
        CREATED_DATETIME timestamp,
        LAST_UPDATED_BY varchar2(255 char),
        LAST_UPDATED_DATETIME timestamp,
        FACE_AMOUNT number(19,2),
        PRINT_FEE number(19,2),
        QUANTITY number(19,0),
        SALES_ORDER number(19,0) not null,
        PRODUCT number(19,0),
        primary key (id)
    );
    
    alter table CRM_SALES_ORD 
        add constraint FK_gbebkmrl0uim0w5h7jesy3ukd 
        foreign key (CUSTOMER) 
        references CRM_GC_CX_PROFILE;

    alter table CRM_SALES_ORD 
        add constraint FK_5obbw5fmaj3dtuv27lwdwf11h 
        foreign key (DISCOUNT) 
        references CRM_GC_DISCOUNT_SCHEME;

    alter table CRM_SALES_ORD_ITEM 
        add constraint FK_etbuv7xswh1guxgfsylk354yg 
        foreign key (SALES_ORDER) 
        references CRM_SALES_ORD;

    alter table CRM_SALES_ORD_ITEM 
        add constraint FK_ju0i7pl4xmalvvpmfv51t4r3v 
        foreign key (PRODUCT) 
        references CRM_GC_PROD_PROFILE;