package com.transretail.crm.web.dto;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;

import com.mysema.query.types.expr.BooleanExpression;
import com.transretail.crm.common.service.dto.request.AbstractSearchFormDto;
import com.transretail.crm.core.entity.QPointsTxnModel;

/**
 * @author Allan G. Ramirez (agramirez@exist.com)
 */
public class PointSearchDto extends AbstractSearchFormDto {
    private Long userId;
    private List<String> contributerId;

    @Override
    public BooleanExpression createSearchExpression() {
    	List<BooleanExpression> exprs = new ArrayList<BooleanExpression>();
    	QPointsTxnModel points = QPointsTxnModel.pointsTxnModel;
    	
        if (userId != null) {
            exprs.add(points.memberModel.id.eq(userId).or(points.transactionContributer.id.eq(userId)));
        }
        
        if(contributerId != null && !contributerId.isEmpty()) {
        	BooleanExpression contributer = points.transactionContributer.accountId.eq(contributerId.get(0));
        	for(int i = 1; i < contributerId.size(); i++) {
        		contributer = contributer.or(points.transactionContributer.accountId.eq(contributerId.get(i)));
        	}
        	exprs.add(contributer);
        }
        return BooleanExpression.allOf(exprs.toArray(new BooleanExpression[exprs.size()]));
    }

    public PointSearchDto withUserId(Long userId) {
        this.userId = userId;
        return this;
    }

	public List<String> getContributerId() {
		return contributerId;
	}

	public void setContributerId(List<String> contributerId) {
		this.contributerId = contributerId;
	}
}
