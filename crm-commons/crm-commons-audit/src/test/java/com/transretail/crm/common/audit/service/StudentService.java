package com.transretail.crm.common.audit.service;

/**
 * @author Allan G. Ramirez (agramirez@exist.com)
 */
public interface StudentService {
    void saveStudent(Student student);

    void updateStudent(Long id, Name name);
}
