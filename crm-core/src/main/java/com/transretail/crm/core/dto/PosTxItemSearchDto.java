package com.transretail.crm.core.dto;

import org.apache.commons.lang3.StringUtils;

import com.mysema.query.types.expr.BooleanExpression;
import com.transretail.crm.common.service.dto.request.AbstractSearchFormDto;
import com.transretail.crm.core.entity.QPosTransaction;
import com.transretail.crm.core.entity.QPosTxItem;
import com.transretail.crm.core.entity.QTransactionAudit;

/**
 * @author Allan G. Ramirez (agramirez@exist.com)
 */
public class PosTxItemSearchDto extends AbstractSearchFormDto {
    private String posTransactionNo;

    public String getPosTransactionNo() {
        return posTransactionNo;
    }

    public void setPosTransactionNo(String posTransactionNo) {
        this.posTransactionNo = posTransactionNo;
    }

    @Override
    public BooleanExpression createSearchExpression() {
        if (StringUtils.isNotBlank(posTransactionNo)) {
            return QPosTxItem.posTxItem.posTransaction.id.eq(posTransactionNo);
        }
        return null;
    }
}
