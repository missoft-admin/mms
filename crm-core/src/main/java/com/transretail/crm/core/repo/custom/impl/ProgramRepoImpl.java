package com.transretail.crm.core.repo.custom.impl;


import java.util.Date;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import com.mysema.query.BooleanBuilder;
import com.mysema.query.jpa.impl.JPAQuery;
import com.transretail.crm.core.dto.CampaignSearchDto;
import com.transretail.crm.core.dto.PromotionSearchDto;
import com.transretail.crm.core.entity.Program;
import com.transretail.crm.core.entity.QCampaign;
import com.transretail.crm.core.entity.QProgram;
import com.transretail.crm.core.entity.QPromotion;
import com.transretail.crm.core.repo.custom.ProgramRepoCustom;
import com.transretail.crm.core.util.BooleanExprUtil;


public class ProgramRepoImpl implements ProgramRepoCustom {

    @PersistenceContext
    EntityManager em;



	@Override
	public List<Program> find( Date inDate, String inStatus ) {

		JPAQuery theQuery = new JPAQuery(em);
		QProgram theProgram = QProgram.program;

		return theQuery.from( theProgram ).
				where( new BooleanBuilder().orAllOf( 
						BooleanExprUtil.INSTANCE.isDatePropertyLoe( theProgram.duration.startDate, inDate ),
						BooleanExprUtil.INSTANCE.isDatePropertyGoe( theProgram.duration.endDate, inDate ),
						BooleanExprUtil.INSTANCE.isStringPropertyEqual( theProgram.status.code, inStatus ) ) ).
				list( theProgram );
	}

	@Override
	public List<Program> find( CampaignSearchDto theDto ) {

		JPAQuery theQuery = new JPAQuery(em);
		QProgram theProgram = QProgram.program;
		QCampaign theCampaign = QCampaign.campaign;

		return theQuery.from( theProgram ).
				innerJoin( theProgram.campaigns, theCampaign ).
				fetch().
				where( new BooleanBuilder().orAllOf( 
						theDto.createSearchExpression() ) ).
				distinct().
				list( theProgram );
	}

	@Override
	public List<Program> find( PromotionSearchDto theDto ) {

		JPAQuery theQuery = new JPAQuery(em);
		QProgram theProgram = QProgram.program;
		QCampaign theCampaign = QCampaign.campaign;
		QPromotion thePromotion = QPromotion.promotion;

		return theQuery.from( theProgram ).
				innerJoin( theProgram.campaigns, theCampaign ).
				fetch().
				innerJoin( theCampaign.promotions, thePromotion ).
				where( new BooleanBuilder().orAllOf( 
						theDto.createSearchExpression() ) ).
				distinct().
				list( theProgram );
	}

}
