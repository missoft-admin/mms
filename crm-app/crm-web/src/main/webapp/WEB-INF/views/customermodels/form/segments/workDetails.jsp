<%@ include file="../../../common/taglibs.jsp" %>


  <!-- Work Details -->
  <fieldset class="form-horizontal container-fluid span3">
    <div class="control-group">
      <label for="occupation" class="control-label"> 
        <spring:message code="label_com_transretail_crm_entity_customermodel_occupation" />:
      </label> 
      <div class="controls">
        <form:select path="customerProfile.occupation" items="${occupation}" itemLabel="description" itemValue="code" />
      </div>
    </div>

    <div class="control-group">
      <spring:message code="member_prop_companyname" var="companyName" />
      <label for="companyName" class="control-label"><c:out value="${companyName}" />:</label>
      <div class="controls"><input type="text" name="companyName" placeholder="${companyName}" /></div>
    </div>

    <div class="control-group">
      <label for="businessField" class="control-label"> 
        <spring:message code="label_com_transretail_crm_entity_customermodel_businessfield" />:
      </label> 
      <div class="controls">
        <form:select path="professionalProfile.businessField" items="${businessField}" itemLabel="description" itemValue="code" />
      </div>
    </div>

    <c:if test="${isEmployee}">
    <div class="control-group">
      <label for="empDepartment" class="control-label"><spring:message code="label_com_transretail_crm_entity_customermodel_department" />: </label>
      <div class="controls"><form:select id="empDepartment" path="professionalProfile.department" items="${department}" itemLabel="description" itemValue="code" /></div>
    </div>
    </c:if>
    
    <%-- <div class="control-group">
      <spring:message code="label_com_transretail_crm_entity_customermodel_businessemail" var="businessEmail" />
      <label for="businessEmail" class="control-label"><c:out value="${businessEmail}" /></label> 
      <div class="controls"><input type="text" name="professionalProfile.businessEmail" class="form-control" placeholder="${businessEmail}" /></div>
    </div> --%>
  </fieldset>


<style> input.form-control-2 { margin-top: 10px !important; } </style>