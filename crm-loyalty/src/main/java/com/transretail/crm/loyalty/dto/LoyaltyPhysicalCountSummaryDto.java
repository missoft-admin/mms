package com.transretail.crm.loyalty.dto;

import org.joda.time.LocalDateTime;
import org.springframework.beans.BeanUtils;

import com.mysema.query.annotations.QueryProjection;
import com.transretail.crm.core.entity.lookup.LookupDetail;
import com.transretail.crm.loyalty.entity.LoyaltyPhysicalCountSummary;


public class LoyaltyPhysicalCountSummaryDto {
	
	private String location;
	private LookupDetail cardType;
	private Long currentInventory;
	private Long physicalCount;
	public LoyaltyPhysicalCountSummaryDto() {}
	@QueryProjection
	public LoyaltyPhysicalCountSummaryDto(String location,
			LookupDetail cardType, Long currentInventory, Long physicalCount) {
		super();
		this.location = location;
		this.cardType = cardType;
		this.currentInventory = currentInventory;
		this.physicalCount = physicalCount;
	}
	@QueryProjection
	public LoyaltyPhysicalCountSummaryDto(String location,
			LookupDetail cardType, Long currentInventory) {
		super();
		this.location = location;
		this.cardType = cardType;
		this.currentInventory = currentInventory;
	}
	public LoyaltyPhysicalCountSummaryDto(LoyaltyPhysicalCountSummary summary) {
		BeanUtils.copyProperties(summary, this);
	}
	public LoyaltyPhysicalCountSummary toModel(LocalDateTime created) {
		LoyaltyPhysicalCountSummary summary = new LoyaltyPhysicalCountSummary();
		BeanUtils.copyProperties(this, summary);
		summary.setCreated(created);
		return summary;
	}
	
	public String getLocation() {
		return location;
	}
	public void setLocation(String location) {
		this.location = location;
	}
	public LookupDetail getCardType() {
		return cardType;
	}
	public void setCardType(LookupDetail cardType) {
		this.cardType = cardType;
	}
	public Long getCurrentInventory() {
		return currentInventory;
	}
	public void setCurrentInventory(Long currentInventory) {
		this.currentInventory = currentInventory;
	}
	public Long getPhysicalCount() {
		return physicalCount;
	}
	public void setPhysicalCount(Long physicalCount) {
		this.physicalCount = physicalCount;
	}
	public Long getDifference() {
		return physicalCount - currentInventory;
	}
}
