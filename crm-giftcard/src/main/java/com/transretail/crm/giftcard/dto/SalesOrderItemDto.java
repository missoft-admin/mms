package com.transretail.crm.giftcard.dto;

import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.util.List;
import java.util.Set;

import com.google.common.collect.Lists;
import com.google.common.collect.Sets;
import com.transretail.crm.giftcard.entity.ProductProfile;
import com.transretail.crm.giftcard.entity.SalesOrder;
import com.transretail.crm.giftcard.entity.SalesOrderItem;


/**
 *
 */
public class SalesOrderItemDto {
	
	private Long id;
	private Long order;
	private Long productId;
	private String productDesc;
	private Long quantity;
	private Integer faceValue;
	private BigDecimal faceAmount;
	private BigDecimal printFee;
	private GcAllocDto gcAlloc;
	private List<GcAllocDto> gcAllocs;
	private Double cardFee;
	private Double unitCost;
	private Boolean freePrintFee;
	
	public SalesOrderItemDto() {}
	
	public SalesOrderItemDto(SalesOrderItem item) {
		if(item.getOrder() != null) {
			this.order = item.getOrder().getId();	
		}
		if(item.getProduct() != null) {
			this.productId = item.getProduct().getId();
			this.faceValue = Integer.parseInt(item.getProduct().getFaceValue().getDescription());
			this.productDesc = item.getProduct().getProductDesc();
			this.cardFee = item.getProduct().getCardFee();
			this.unitCost = item.getProduct().getUnitCost();
		}
		this.quantity = item.getQuantity();
		this.faceAmount = item.getFaceAmount();
		this.printFee = item.getPrintFee();
		this.id = item.getId();
		this.freePrintFee = item.getFreePrintFee();
	}
	
	public SalesOrderItem toModel(SalesOrderItem item) {
		if(item == null)
			item = new SalesOrderItem();
		
		if(this.productId != null)
			item.setProduct(new ProductProfile(this.productId));
		item.setQuantity(this.quantity);
		item.setFaceAmount(this.faceAmount);
		item.setPrintFee(this.printFee);
		item.setFreePrintFee(this.freePrintFee);
		
		return item;
		
	}
	
	public static Set<SalesOrderItemDto> toDto(Set<SalesOrderItem> items) {
		Set<SalesOrderItemDto> itemDtos = Sets.newHashSet();
		for(SalesOrderItem item: items) {
			itemDtos.add(new SalesOrderItemDto(item));
		}
		return itemDtos;
	}
	
	public static Set<SalesOrderItem> toModel(Set<SalesOrderItemDto> dtos, SalesOrder order) {
    	Set<SalesOrderItem> items = Sets.newHashSet();
    	for(SalesOrderItemDto dto: dtos) {
    		SalesOrderItem item = dto.toModel(new SalesOrderItem());
    		item.setOrder(order);
    		items.add(item);
    	}
    	return items;
    }

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}
	
	public Long getOrder() {
		return order;
	}

	public void setOrder(Long order) {
		this.order = order;
	}
	public Long getProductId() {
		return productId;
	}
	public void setProductId(Long productId) {
		this.productId = productId;
	}
	public Long getQuantity() {
		return quantity;
	}
	public void setQuantity(Long quantity) {
		this.quantity = quantity;
	}
	public BigDecimal getFaceAmount() {
		return faceAmount;
	}
	public String getFaceAmountFmt() {
		if(faceAmount != null)
			return new DecimalFormat("#,##0.00").format(faceAmount);
		return "";
	}
	public void setFaceAmount(BigDecimal faceAmount) {
		this.faceAmount = faceAmount;
	}
	public BigDecimal getPrintFee() {
		return printFee;
	}
	public void setPrintFee(BigDecimal printFee) {
		this.printFee = printFee;
	}

	public Integer getFaceValue() {
		return faceValue;
	}
	public String getFaceValueFmt() {
		if(faceValue != null)
			return new DecimalFormat("#,##0.00").format(faceValue);
		return "";
	}
	public void setFaceValue(Integer faceValue) {
		this.faceValue = faceValue;
	}

	public String getProductDesc() {
		return productDesc;
	}

	public void setProductDesc(String productDesc) {
		this.productDesc = productDesc;
	}

	public GcAllocDto getGcAlloc() {
		return gcAlloc;
	}

	public void setGcAlloc(GcAllocDto gcAlloc) {
		this.gcAlloc = gcAlloc;
	}

	public List<GcAllocDto> getGcAllocs() {
		return gcAllocs;
	}

	public void setGcAllocs(List<GcAllocDto> gcAllocs) {
		this.gcAllocs = gcAllocs;
	}

	public Double getCardFee() {
		return cardFee;
	}

	public void setCardFee(Double cardFee) {
		this.cardFee = cardFee;
	}

	public Double getUnitCost() {
		return unitCost;
	}

	public void setUnitCost(Double unitCost) {
		this.unitCost = unitCost;
	}

	public Boolean getFreePrintFee() {
		return freePrintFee;
	}

	public void setFreePrintFee(Boolean freePrintFee) {
		this.freePrintFee = freePrintFee;
	}
	
	
	
}
