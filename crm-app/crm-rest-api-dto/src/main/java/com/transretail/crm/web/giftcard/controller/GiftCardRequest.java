package com.transretail.crm.web.giftcard.controller;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.*;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.node.TextNode;
import com.google.common.base.Objects;
import com.transretail.crm.common.web.converter.ISODateTimeDeserializer;
import com.transretail.crm.common.web.converter.ISODateTimeSerializer;
import com.transretail.crm.giftcard.dto.GiftCardTransactionInfo;
import java.io.IOException;
import java.util.Set;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import org.hibernate.validator.constraints.NotEmpty;
import org.joda.time.DateTime;
import org.joda.time.LocalDateTime;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.format.annotation.DateTimeFormat.ISO;

/**
 * @author Monte Cillo Co (mco@exist.com)
 */
public class GiftCardRequest extends AbstractGiftCardRequest {

    private String accountId;
    @NotEmpty
    @Size(max = 20)
    private String transactionNo;
    @NotNull
    @DateTimeFormat(iso = ISO.DATE_TIME)
    @JsonSerialize(using = ISODateTimeSerializer.class)
    @JsonDeserialize(using = ISODateTimeDeserializer.class)
    private LocalDateTime transactionDate;
    @NotEmpty
    @JsonSerialize(contentUsing = CardItemSerializer.class)
    @JsonDeserialize(contentUsing = CardItemDeserializer.class)
    private Set<? extends CardItem> items;

    public Set<? extends CardItem> getItems() {
        return items;
    }

    public void setItems(Set<? extends CardItem> items) {
        this.items = items;
    }

    public void setTransactionNo(String transactionNo) {
        this.transactionNo = transactionNo;
    }

    public void setTransactionDate(LocalDateTime transactionDate) {
        this.transactionDate = transactionDate;
    }

    public String getTransactionNo() {
        return transactionNo;
    }

    public LocalDateTime getTransactionDate() {
        return transactionDate;
    }

    public String getAccountId() {
        return accountId;
    }

    public void setAccountId(String accountId) {
        this.accountId = accountId;
    }

    @Override
    public String getSignature() {
        return String.format("%s%s%s%s", getMerchantId(), getTerminalId(), getCashierId(), getTransactionNo());
    }

    @Override
    public GiftCardTransactionInfo toGiftCardTransactionInfo() {
        return super.templateBaseTransactionInfo()
                .transactionNo(transactionNo)
                .transactionTime(transactionDate);
    }

    static class CardItemSerializer extends JsonSerializer<CardItem> {

        public CardItemSerializer() {
        }

        @Override
        public void serialize(CardItem value, JsonGenerator jgen, SerializerProvider provider) throws IOException,
                JsonProcessingException {
            if (value instanceof CardItemWithAmount) {
                new ObjectMapper().writeValue(jgen, (CardItemWithAmount) value);
                return;
            }
            jgen.writeString(value.getCardNo());
        }
    }

    static class CardItemDeserializer extends JsonDeserializer<CardItem> {

        public CardItemDeserializer() {
        }

        @Override
        public CardItem deserialize(JsonParser jp, DeserializationContext ctxt) throws IOException, JsonProcessingException {
            JsonNode node = jp.getCodec().readTree(jp);
            if (node instanceof TextNode) {
                String cardNo = ((TextNode) node).asText();
                return new CardItem(cardNo);
            }

            String cardNo = node.get("cardNo").asText();
            double amount = node.get("amount").doubleValue();
            return new CardItemWithAmount(cardNo, amount);
        }
    }

    /**
     * A wrapper class to provide a based object for transaction Item
     */
    static class CardItem {

        @Size(min = 20, max = 20)
        private String cardNo;

        public CardItem() {
        }

        public CardItem(String cardNo) {
            this.cardNo = cardNo;
        }

        public String getCardNo() {
            return cardNo;
        }

        @Override
        public int hashCode() {
            int hash = 5;
            hash = 71 * hash + (this.cardNo != null ? this.cardNo.hashCode() : 0);
            return hash;
        }

        @Override
        public boolean equals(Object obj) {
            if (obj == null) {
                return false;
            }
            if (getClass() != obj.getClass()) {
                return false;
            }
            final CardItem other = (CardItem) obj;
            if ((this.cardNo == null) ? (other.cardNo != null) : !this.cardNo.equals(other.cardNo)) {
                return false;
            }
            return true;
        }

    }

    /**
     * A wrapper class representing cardNo and amount value pair.
     */
    static class CardItemWithAmount extends CardItem {

        private double amount;

        public CardItemWithAmount() {
        }

        @JsonCreator
        public CardItemWithAmount(@JsonProperty String cardNo, @JsonProperty double amount) {
            super(cardNo);
            this.amount = amount;
        }

        @Override
        public String getCardNo() {
            return super.getCardNo();
        }

        public double getAmount() {
            return amount;
        }

        @Override
        public int hashCode() {
            return Objects.hashCode(super.getCardNo(), this.amount);
        }

        @Override
        public boolean equals(Object obj) {
            if (obj == null) {
                return false;
            }
            if (getClass() != obj.getClass()) {
                return false;
            }

            final CardItemWithAmount other = (CardItemWithAmount) obj;
            return Objects.equal(super.getCardNo(), other.getCardNo())
                    && Objects.equal(this.amount, other.getAmount());
        }
    }
}
