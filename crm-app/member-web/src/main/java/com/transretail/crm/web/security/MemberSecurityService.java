package com.transretail.crm.web.security;

import com.transretail.crm.core.entity.MemberModel;
import com.transretail.crm.core.entity.UserRoleModel;
import com.transretail.crm.core.entity.enums.MemberStatus;
import com.transretail.crm.core.entity.lookup.LookupDetail;
import com.transretail.crm.core.repo.MemberRepo;
import com.transretail.crm.core.security.model.impl.CustomSecurityUserDetailsImpl;
import org.apache.commons.lang3.BooleanUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

/**
 * @author Allan G. Ramirez (agramirez@exist.com)
 */
@Service("memberSecurityService")
public class MemberSecurityService implements UserDetailsService {

    private static final Logger _LOG = LoggerFactory.getLogger(MemberSecurityService.class);

    @Autowired
    private MemberRepo memberRepo;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        _LOG.debug("Attempting to load user by username: username=[{}]", username);
        CustomSecurityUserDetailsImpl userDetails = null;
        MemberModel member = memberRepo.findByUsername(username);
        if (member != null) {
            userDetails = new CustomSecurityUserDetailsImpl();
            userDetails.setUserId(member.getId());
            userDetails.setUsername(member.getUsername());
            userDetails.setPassword(member.getPassword());
            userDetails.setEnabled(BooleanUtils.toBoolean(member.getEnabled()) && member.getAccountStatus() == MemberStatus.ACTIVE);
            if ( null != member.getAccountStatus() ) {
                userDetails.setStatus( member.getAccountStatus().toString() );
            }
            LookupDetail memberType = member.getMemberType();
            if (memberType != null) {
                UserRoleModel role = new UserRoleModel();
                role.setCode(memberType.getCode());
                userDetails.getRoles().add(role);
                userDetails.getAuthorities().add(new SimpleGrantedAuthority(memberType.getCode()));
            }
            
            StringBuilder fullName = new StringBuilder();
            fullName.append(member.getFirstName() != null ? member.getFirstName() : "").append(" ");
            fullName.append(member.getLastName() != null ? member.getLastName() : "");
            userDetails.setFullName(fullName.toString());
        }
        return userDetails;
    }

}
