package com.transretail.crm.giftcard.dto;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.transretail.crm.common.web.converter.DateTimeSerializer;
import javax.annotation.Nonnull;
import javax.validation.constraints.Size;
import org.hibernate.validator.constraints.NotEmpty;
import org.joda.time.DateTime;

/**
 *
 * @author Monte Cillo Co (mco@exist.com)
 */
public class GiftCardServiceRequestDto {

    private Long id;
    @NotEmpty(message = "{gc.service.request.gc.no.required}")
    private String giftcardNo;
    private String filedBy;
    @JsonSerialize(using = DateTimeSerializer.class)
    private DateTime dateFiled;
    @NotEmpty(message="{gc.service.request.filed.in.required}")
    private String filedIn;
    private String filedInPresenter;
    @NotEmpty(message ="{gc.service.request.details.required}")
    @Size(max = 500, message = "{gc.service.request.detail.max.length}")
    private String details;

    public Long getId() {
	return id;
    }

    public void setId(Long id) {
	this.id = id;
    }

    public String getGiftcardNo() {
	return giftcardNo;
    }

    public void setGiftcardNo(String giftcardNo) {
	this.giftcardNo = giftcardNo;
    }

    public String getFiledBy() {
	return filedBy;
    }

    public void setFiledBy(String filedBy) {
	this.filedBy = filedBy;
    }

    public String getFiledIn() {
	return filedIn;
    }

    public void setFiledIn(String filedIn) {
	this.filedIn = filedIn;
    }

    public DateTime getDateFiled() {
	return dateFiled;
    }

    public void setDateFiled(DateTime dateFiled) {
	this.dateFiled = dateFiled;
    }

    public String getDetails() {
	return details;
    }

    public void setDetails(String details) {
	this.details = details;
    }

    public void setFiledInPresenter(String filedInPresenter) {
	this.filedInPresenter = filedInPresenter;
    }

    @Nonnull
    public String getFiledInPresenter() {
	return filedInPresenter;
    }
}
