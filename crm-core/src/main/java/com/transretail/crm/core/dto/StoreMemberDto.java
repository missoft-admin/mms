package com.transretail.crm.core.dto;

import com.transretail.crm.core.entity.StoreMember;
import com.transretail.crm.core.entity.lookup.LookupDetail;

/**
 * @author ftopico
 */
public class StoreMemberDto {

    private Long id;
    private String store;
    private String company;
    private String cardType;
    private String remarks;
    
    private StoreDto storeDto;
    private LookupDetail companyDto;
    private LookupDetail cardTypeDto;
    
    public StoreMemberDto() {
        
    }
    
    public StoreMemberDto(StoreMember storeMember) {
        this.id = storeMember.getId();
        this.store = storeMember.getStore();
        this.company = storeMember.getCompany();
        this.cardType = storeMember.getCardType();
        this.remarks = storeMember.getRemarks();
    }
    
    public StoreMember toModel() {
        return toModel(new StoreMember());
    }

    public StoreMember toModel(StoreMember storeMember) {
        storeMember.setStore(this.store);
        storeMember.setCompany(this.company);
        storeMember.setCardType(this.cardType);
        storeMember.setRemarks(this.remarks);
        
        return storeMember;
    }
    
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getStore() {
        return store;
    }

    public String getCompany() {
        return company;
    }

    public String getCardType() {
        return cardType;
    }

    public String getRemarks() {
        return remarks;
    }

    public void setStore(String store) {
        this.store = store;
    }

    public void setCompany(String company) {
        this.company = company;
    }

    public void setCardType(String cardType) {
        this.cardType = cardType;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public StoreDto getStoreDto() {
        return storeDto;
    }

    public void setStoreDto(StoreDto storeDto) {
        this.storeDto = storeDto;
    }

    public LookupDetail getCompanyDto() {
        return companyDto;
    }

    public LookupDetail getCardTypeDto() {
        return cardTypeDto;
    }

    public void setCompanyDto(LookupDetail companyDto) {
        this.companyDto = companyDto;
    }

    public void setCardTypeDto(LookupDetail cardTypeDto) {
        this.cardTypeDto = cardTypeDto;
    }
    
}
