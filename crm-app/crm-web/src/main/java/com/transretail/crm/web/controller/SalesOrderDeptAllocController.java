package com.transretail.crm.web.controller;

import java.beans.PropertyEditorSupport;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.joda.time.LocalDate;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.google.common.collect.Lists;
import com.transretail.crm.core.dto.LookupDetailResultList;
import com.transretail.crm.core.dto.LookupDetailSearchDto;
import com.transretail.crm.core.entity.enums.Status;
import com.transretail.crm.core.entity.lookup.LookupDetail;
import com.transretail.crm.core.service.CodePropertiesService;
import com.transretail.crm.core.service.LookupService;
import com.transretail.crm.giftcard.dto.SalesOrderDeptAllocDto;
import com.transretail.crm.giftcard.dto.SalesOrderMultDeptAllocsDto;
import com.transretail.crm.giftcard.entity.SalesOrderDeptAlloc.DeptAllocStatus;
import com.transretail.crm.giftcard.service.SalesOrderDeptService;
import com.transretail.crm.giftcard.service.SalesOrderService;
import com.transretail.crm.web.controller.util.ControllerResponse;

@Controller
@RequestMapping("/giftcard/salesorder/deptalloc")
public class SalesOrderDeptAllocController {

	private SalesOrderDeptService deptService;
	private LookupService lookupService;
	private SalesOrderService orderService;
	private CodePropertiesService codePropertiesService;
	private MessageSource messageSource;
	
	private static final DateTimeFormatter FORMATTER = DateTimeFormat.forPattern("dd-MM-yyyy");

	/* ================== IoC ==================== */
    @Autowired
    public void setDeptService(SalesOrderDeptService deptService) {
        this.deptService = deptService;
    }
    @Autowired
    public void setLookupService(LookupService lookupService) {
        this.lookupService = lookupService;
    }
    @Autowired
    public void setOrderService(SalesOrderService orderService) {
        this.orderService = orderService;
    }
    @Autowired
    public void setCodePropertiesService(CodePropertiesService codePropertiesService) {
        this.codePropertiesService = codePropertiesService;
    }
    @Autowired
    public void setMessageSource(MessageSource messageSource) {
        this.messageSource = messageSource;
    }



    @InitBinder
	void initBinder(WebDataBinder binder) {
		PropertyEditorSupport editor = new PropertyEditorSupport() {
			@Override
			public void setAsText(String text) throws IllegalArgumentException {
				if(StringUtils.isNotBlank(text))
					setValue(FORMATTER.parseLocalDate(text));
				else
					setValue(null);
			}

			@Override
			public String getAsText() throws IllegalArgumentException {
				if (getValue() != null)
					return FORMATTER.print((LocalDate) getValue());
				else
					return "";
			}
		};
		binder.registerCustomEditor(LocalDate.class, editor);
	}
	
	@RequestMapping(value = "/form/{id}", method = RequestMethod.GET, produces = "text/html")
    public String showPaymentForm(
    		@PathVariable("id") String id,
    		Model uiModel) {
		SalesOrderMultDeptAllocsDto allocs = deptService.getAllocations(Long.valueOf(id));
		populateForm(allocs, uiModel);
    	return "giftcard/so/deptalloc/form";
    }
	
	
	private void populateForm(SalesOrderMultDeptAllocsDto allocs, Model uiModel) {
		uiModel.addAttribute("allocForm", allocs);
	}
	
	@RequestMapping(value = "/save/{status}", method = RequestMethod.POST, produces = "application/json; charset=utf-8")
	@ResponseBody
	public ControllerResponse ajaxSavePayments(
			@PathVariable("status") String status,
			@ModelAttribute(value = "allocForm") SalesOrderMultDeptAllocsDto allocForm,
			BindingResult result, HttpServletRequest request) {

		ControllerResponse theResponse = new ControllerResponse();
		theResponse.setSuccess(true);
		
		allocForm.setStatus(DeptAllocStatus.valueOf(status));
		
		validate(allocForm, result);
		
		if (result.hasErrors()) {
			theResponse.setSuccess(false);
			theResponse.setResult(result.getAllErrors());
			return theResponse;
		}

		if (theResponse.isSuccess()) {
			deptService.saveAllocations(allocForm);
			deptService.createEmptyAllocForRemaingAmt(allocForm.getOrderId(), allocForm.getSalesOrderAmt());
		}

		return theResponse;
	}

    @SuppressWarnings({ "unchecked", "rawtypes" })
	@RequestMapping( value="/departments/list/{excCodes}/{filter}", produces="application/json; charset=utf-8", method=RequestMethod.GET )
	public @ResponseBody List<LookupDetail> listDepts( 
			@PathVariable(value="excCodes") String[] excCodes,
			@PathVariable(value="filter") String filter ) {
    	LookupDetailSearchDto searchDto = new LookupDetailSearchDto();
    	searchDto.setExcCodes( excCodes );
    	searchDto.setDescLike( filter );
    	searchDto.setStatus( Status.ACTIVE );
    	searchDto.setHeaderCode( codePropertiesService.getHeaderGcSoDepartment() );
    	LookupDetailResultList results = lookupService.getLookupDetails( searchDto );
		return Lists.newArrayList( CollectionUtils.isNotEmpty( results.getResults() )? results.getResults() : new ArrayList() );
    }


    /* ==================== Helper Method ================== */

	private void validate(SalesOrderMultDeptAllocsDto allocForm, BindingResult result) {
		BigDecimal totalAmt = BigDecimal.ZERO;
		for(SalesOrderDeptAllocDto allocDto: allocForm.getDeptAllocs()) {
			if(allocDto == null) {
				result.reject(getMessage("propkey_msg_notempty", new Object[]{"sales_order_department"}));	
			} else {
				if(StringUtils.isBlank(allocDto.getDeptId())) {
					/*result.reject(getMessage("propkey_msg_notempty", new Object[]{"sales_order_department"}));*/
					if ( null != allocDto.getDepartment() 
							&& StringUtils.isNotBlank( allocDto.getDepartment().getDescription() ) ) {
						String deptCode = lookupService.getLookupDetailCodeByHdrCodeAndDesc( codePropertiesService.getHeaderGcSoDepartment(), 
								allocDto.getDepartment().getDescription().trim() );
						if ( StringUtils.isNotBlank( deptCode ) ) {
							allocDto.getDepartment().setCode( deptCode );
						}
						else {
							result.reject( messageSource.getMessage( "sales_order_err_invaliddeptdesc", 
									new String[]{ allocDto.getDepartment().getDescription() }, LocaleContextHolder.getLocale() ) );
						}
					}
				} else {
					if(lookupService.getActiveDetailByCode(allocDto.getDeptCodeFromId()) == null) {
						result.reject(getMessage("sales_order_err_invaliddept", null));
					}
				}
				if(allocDto.getAllocAmount() == null || allocDto.getAllocAmount().compareTo(BigDecimal.ZERO) == 0) {
					result.reject(getMessage("propkey_msg_notempty", new Object[]{"sales_order_amount"}));
				} else {
					totalAmt = totalAmt.add(allocDto.getAllocAmount());
				}
			}
		}
		
		if(CollectionUtils.isNotEmpty(allocForm.getDeptAllocs()) && allocForm.getSalesOrderAmt().compareTo(totalAmt) < 0) {
			result.reject(getMessage("sales_order_err_invaliddepttotal", null));
		}
		
	}
	private String getMessage(String inMsgCode, Object[] inArgMsgCodes) {
        if (inArgMsgCodes != null) {
            for (int i = 0; i < inArgMsgCodes.length; i++) {
                inArgMsgCodes[i] =
                    messageSource.getMessage(inArgMsgCodes[i].toString().toLowerCase(), null, LocaleContextHolder.getLocale());
            }
        }
        return messageSource.getMessage(inMsgCode, inArgMsgCodes, "Info given is invalid.", LocaleContextHolder.getLocale());
    }
	
}
