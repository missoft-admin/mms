<%@ include file="../../common/taglibs.jsp" %>

<style type="text/css">
<!--

  #productTable tr td:last-child {
    border-left: 0px;
  }

  #addProductBtn.btn-small {
    margin-bottom: 5px;
  }
  table select, textarea, table input[type="text"], input[type="password"], input[type="datetime"], input[type="datetime-local"], input[type="date"], input[type="month"], input[type="time"], input[type="week"], input[type="number"], input[type="email"], input[type="url"], input[type="search"], input[type="tel"], input[type="color"], .uneditable-input {
      margin-bottom: 0px;
      margin-top: 0px;
  }
  
-->
</style>



<c:set var="ENCTYPE" value="application/x-www-form-urlencoded"/>

<div class="modal-dialog">
  <div class="modal-content">
    
    <div class="modal-header">
      <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
      <h4><spring:message code="gc_return"/></h4>
    </div>
 
    <div class="modal-body">
    
      <div id="contentError" class="hide alert alert-error">
        <button type="button" class="close" onclick="$(this).parent().hide();">&times;</button>
        <div><form:errors path="*"/></div>
      </div>
    
      
      <form:form id="returnForm" name="returnForm" modelAttribute="returnForm" action="${pageContext.request.contextPath}/giftcard/return/bycards/save/" method="POST" enctype="${ENCTYPE}" cssClass="form-reset form-horizontal" >
      <form:hidden path="id"/>
      
      <div class="contentMain">
      
      <div class="row-fluid">
      
        <div class="span6">
        <div class="control-group">
          <spring:message code="gc_return_no" var="returnNo" />
          <label for="recordNo" class="control-label">${returnNo}</label> 
          <div class="controls">
            <form:input path="recordNo" class="form-control" placeholder="${returnNo}" disabled="true" />
          </div>
        </div>
        
        <div class="control-group">
          <spring:message code="gc_return_date" var="dateReturned" />
          <label for="returnDate" class="control-label">${dateReturned}</label> 
          <div class="controls">
            <form:input path="returnDate" class="form-control" placeholder="${dateReturned}" disabled="true" />
          </div>
        </div>
        
        <div class="control-group">
          <spring:message code="gc_return_reason" var="reasons" />
          <label for="reason" class="control-label">${reasons}</label> 
          <div class="controls">
            <input type="text" value="${returnForm.reason.description}" class="form-control" disabled="disabled" />
          </div>
        </div>
        
        
        
        
        </div>
        
        <div class="span6">
        
        <div class="control-group">
          <spring:message code="gc_return_customer" var="customer" />
          <label for="customerId" class="control-label">${customer}</label> 
          <div class="controls">
            <form:input path="customerName" class="form-control" placeholder="${customer}" disabled="true" />
            <form:hidden path="customerId" />
          </div>
        </div>
        
        <div class="control-group">
          <spring:message code="gc_return_order_no" var="salesOrder" />
          <label for="orderNo" class="control-label">${salesOrder}</label> 
          <div class="controls">
            <form:input path="orderNo" class="form-control" placeholder="${salesOrder}" disabled="true" />
            <form:hidden path="orderId"  />
          </div>
        </div>
        
        
        
        </div>
        
        
      </div>
      
      
      <div class="row-fluid">
      
      <fieldset>
      <legend class="block-stack-title"><spring:message code="gc_return_cards"/></legend>
      
      
          
      
      <table id="productTable" class="table table-condensed table-compressed table-striped">
      <thead>
      <tr>
        <th><spring:message code="gc_return_series" /></th>
        <th><spring:message code="gc_return_prod_profile" /></th>
        <th><spring:message code="gc_return_face_amount" /></th>
        <th><spring:message code="gc_return_balance" /></th>
        <th><spring:message code="gc_return_status" /></th>
      </tr>
      </thead>
      <tbody>
      <c:forEach items="${returnForm.returns}" var="item">
      <tr>
      <td>${item.startingSeries} - ${item.endingSeries}</td>
      <td>${item.productCode} - ${item.productName}</td>
      <td><fmt:formatNumber pattern="###,##0" value="${item.faceAmount}" /></td>
      <td><fmt:formatNumber pattern="###,##0" value="${item.balance}" /></td>
      <td>${item.status}</td>
      </tr>
      
      </c:forEach>
      </tbody>
      </table>
      
      
      </fieldset>
      
      </div>
      
      
      
      
      
          
      </div>
      
      </form:form>
    
    </div>
    <div class="modal-footer">
      <button type="button" id="orderCancel" class="btn btn-default" data-dismiss="modal"><spring:message code="label_cancel"/></button>
    </div>
  </div>
</div>

<script>
$(document).ready(function() {
});
</script>