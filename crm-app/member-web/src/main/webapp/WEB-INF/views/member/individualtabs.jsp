<%@ include file="../taglibs.jsp"%>

  <li class="active individualDetails"><a href="#personal" data-toggle="tab"><spring:message
        code="personal_information" /></a></li>
  <li class="professionalDetails"><a href="#personal" data-toggle="tab"><spring:message
        code="company_details" /></a></li>
  <li class="workDetails"><a href="#work" data-toggle="tab"><spring:message
        code="work_information" /></a></li>
  <li class="correspondenceDetails"><a href="#correspondence" data-toggle="tab"><spring:message
        code="correspondence_info" /></a></li>
  <li class="marketingDetails"><a href="#marketing" data-toggle="tab"><spring:message
        code="marketing_information" /></a></li>

  <li class=""><a href="#membership" data-toggle="tab"><spring:message
        code="membership_information" /></a></li>
  <c:if test="${isPrimary && form.memberTypeCode != 'MTYP002'}">
    <li><a href="#cards" data-toggle="tab"><spring:message code="supplementary_accts" /></a>
  </c:if>