package com.transretail.crm.report.template.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;

import org.apache.commons.lang3.StringUtils;
import org.joda.time.DateTime;
import org.joda.time.LocalDate;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.MessageSourceAware;
import org.springframework.context.support.MessageSourceAccessor;
import org.springframework.stereotype.Service;

import com.google.common.collect.Lists;
import com.mysema.query.BooleanBuilder;
import com.mysema.query.jpa.JPQLQuery;
import com.mysema.query.jpa.impl.JPAQuery;
import com.mysema.query.types.Projections;
import com.transretail.crm.common.reporting.jasper.ClasspathFileResolver;
import com.transretail.crm.common.reporting.jasper.DefaultJRProcessor;
import com.transretail.crm.common.reporting.jasper.JRProcessor;
import com.transretail.crm.common.util.DateUtil;
import com.transretail.crm.core.dto.MemberDto;
import com.transretail.crm.core.entity.QMemberModel;
import com.transretail.crm.core.entity.enums.MemberCreatedFromType;
import com.transretail.crm.core.entity.enums.ReportType;
import com.transretail.crm.core.entity.lookup.LookupDetail;
import com.transretail.crm.core.entity.lookup.Store;
import com.transretail.crm.core.security.util.UserUtil;
import com.transretail.crm.core.service.CodePropertiesService;
import com.transretail.crm.core.service.LookupService;
import com.transretail.crm.core.service.StoreService;
import com.transretail.crm.report.template.CommonReportFilter;
import com.transretail.crm.report.template.FilterField;
/**
 * @author ftopico
 */

@Service("memberRegistrationByCustomerReport")
public class MemberRegistrationByCustomerReport implements MessageSourceAware {
    
    @PersistenceContext
    private EntityManager em;
    @Autowired
    private StoreService storeService;
    @Autowired
    private CodePropertiesService codePropertiesService;
    @Autowired
    private LookupService lookupService;

    protected static final String TEMPLATE_NAME = "Member Registration Report By Store";
    protected static final String REPORT_NAME_BY_STORE = "reports/member_registration_by_store_report.jasper";
    protected static final String REPORT_NAME_BY_TYPE = "reports/member_registration_by_register_type_report.jasper";
    protected static final String REPORT_NAME_SUMMARY = "reports/member_registration_summary_report.jasper";
    protected static final String FILTER_ACTIVATIONDATE_FROM = CommonReportFilter.DATE_FROM;
    protected static final String FILTER_ACTIVATIONDATE_TO = CommonReportFilter.DATE_TO;
    protected static final String FILTER_CUSTOMER_TYPE = "customerType";
    protected static final String FILTER_STORE = "store";
    protected static final String FILTER_MEMBER_REPORT_TYPE = "memberReportType";
    protected static final String MEMBER_REPORT_TYPE_SUMMARY = "summary";
    protected static final String MEMBER_REPORT_TYPE_REGISTER_TYPE = "type";
    protected static final String MEMBER_REPORT_TYPE_STORE = "store";
    
    
    //Header Params
    private static final String PARAM_PRINT_DATE = "printedDate";
    private static final String PARAM_PRINT_BY = "printedBy";
    private static final DateTimeFormatter FULL_DATE_PATTERN = DateTimeFormat.forPattern("MMMM d, yyyy");
    private static final String SEARCH_DATE_FORMAT = DateUtil.SEARCH_DATE_FORMAT;
    private static final DateTimeFormatter INPUT_DATE_PATTERN = DateTimeFormat.forPattern(SEARCH_DATE_FORMAT);
    private MessageSourceAccessor messageSource;
    
    public ReportType[] getReportTypes() {
        return ReportType.values();
    }
    
    @Override
    public void setMessageSource(MessageSource messageSource) {
        this.messageSource = new MessageSourceAccessor(messageSource);
    }
    
    public String getName() {
        return TEMPLATE_NAME;
    }
    
    public String getDescription() {
        return messageSource.getMessage("member.registration.by.store");
    }
    
    public Set<FilterField> getFilters() {
        return null;
    }
    
    public JRProcessor createJrProcessor(Map<String, String> map) {

        DateTime activationDateFrom = toDateTime(map.get(FILTER_ACTIVATIONDATE_FROM));
        DateTime activationDateTo = toDateTime(map.get(FILTER_ACTIVATIONDATE_TO));
        String customerType = map.get(FILTER_CUSTOMER_TYPE);
        String store = map.get(FILTER_STORE);
        String memberReportType = map.get(FILTER_MEMBER_REPORT_TYPE);
        QMemberModel qMemberModel = QMemberModel.memberModel;
        
        BooleanBuilder predicate = new BooleanBuilder(qMemberModel.created.between(activationDateFrom, DateUtil.getEndOfDay(activationDateTo))
                                                    .and(qMemberModel.registeredStore.isNotNull())
                                                    .and(qMemberModel.isNotNull())
                                                    .and(qMemberModel.memberType.code.ne("MTYP002")));
        
        if (StringUtils.isNotBlank(customerType)) {
            predicate.and(qMemberModel.memberType.code.eq(customerType));
        }
        
        if (StringUtils.isNotBlank(store)) {
            predicate.and(qMemberModel.registeredStore.eq(store));
        }
        
        JPQLQuery query = new JPAQuery(em);
        Map<String, Object> parameters = new HashMap<String, Object>();
        List<MemberRegistrationReportModel> list = new ArrayList<MemberRegistrationByCustomerReport.MemberRegistrationReportModel>();
        
        if (memberReportType.equalsIgnoreCase(MEMBER_REPORT_TYPE_STORE) || memberReportType.equalsIgnoreCase(MEMBER_REPORT_TYPE_SUMMARY)) {
            query = query.from(qMemberModel)
                .where(predicate.getValue())
                .groupBy(qMemberModel.registeredStore);
            
            List<MemberDto> tempList = query.list(Projections.fields(MemberDto.class,
                                    qMemberModel.registeredStore.as("registeredStore")));
            
            for (MemberDto member : tempList) {
                MemberRegistrationReportModel reportModel = new MemberRegistrationReportModel();
                String storeCode = member.getRegisteredStore();
                reportModel.setStore(storeCode);
                if (storeCode.equalsIgnoreCase(codePropertiesService.getDetailInvLocationHeadOffice())) {
                    reportModel.setStoreName(lookupService.getDetailByCode(codePropertiesService.getDetailInvLocationHeadOffice()).getDescription());
                } else {
                    Store tempStore = storeService.getStoreByCode(storeCode);
                    if (tempStore != null)
                    reportModel.setStoreName(tempStore.getName());
                }
                
                //CS CRM
                BooleanBuilder csRegPredicate = new BooleanBuilder(predicate.getValue());
                csRegPredicate.and(qMemberModel.registeredStore.eq(storeCode));
                csRegPredicate.and(qMemberModel.memberCreatedFromType.eq(MemberCreatedFromType.FROM_CRM));
                
                //CS UPLOAD
                BooleanBuilder uploadRegPredicate = new BooleanBuilder(predicate.getValue());
                uploadRegPredicate.and(qMemberModel.registeredStore.eq(storeCode));
                uploadRegPredicate.and(qMemberModel.memberCreatedFromType.eq(MemberCreatedFromType.FROM_UPLOAD));
                
                //Member Portal
                BooleanBuilder onlineRegPredicate = new BooleanBuilder(predicate.getValue());
                onlineRegPredicate.and(qMemberModel.registeredStore.eq(storeCode));
                onlineRegPredicate.and(qMemberModel.memberCreatedFromType.eq(MemberCreatedFromType.FROM_PORTAL));
                
                query = new JPAQuery(em).from(qMemberModel)
                        .where(csRegPredicate)
                        .groupBy(qMemberModel.registeredStore);
                
                try {
                    reportModel.setCsRegCount(query.count());
                } catch (NoResultException e) {
                    reportModel.setCsRegCount(0L);
                }
                
                query = new JPAQuery(em).from(qMemberModel)
                        .where(uploadRegPredicate)
                        .groupBy(qMemberModel.registeredStore);
                
                try {
                    reportModel.setCsUploadCount(query.count());
                } catch (NoResultException e) {
                    reportModel.setCsUploadCount(0L);
                }
                
                query = new JPAQuery(em).from(qMemberModel)
                        .where(onlineRegPredicate)
                        .groupBy(qMemberModel.registeredStore);
                
                try {
                    reportModel.setOnlineRegCount(query.count());
                } catch (NoResultException e) {
                    reportModel.setOnlineRegCount(0L);
                }
                
                list.add(reportModel);
            }
            
            parameters.put(FILTER_ACTIVATIONDATE_FROM, activationDateFrom);
            parameters.put(FILTER_ACTIVATIONDATE_TO, activationDateTo);
            if (customerType != null) {
                LookupDetail customer = lookupService.getActiveDetailByCode(customerType);
                parameters.put(FILTER_CUSTOMER_TYPE, customer.getDescription());
//                parameters.put(FILTER_CUSTOMER_TYPE, customer.getCode() + " - " + customer.getDescription());
            }
            if (store != null) {
                parameters.put(FILTER_STORE, storeService.getStoreByCode(store).getName());
//                parameters.put(FILTER_STORE, storeService.getStoreByCode(store).getCodeAndName());
            }
            parameters.put(PARAM_PRINT_DATE, FULL_DATE_PATTERN.print(new LocalDate()));
            parameters.put(PARAM_PRINT_BY, getCurrentUser());
        } else if (memberReportType.equalsIgnoreCase(MEMBER_REPORT_TYPE_REGISTER_TYPE)) {
            query = query.from(qMemberModel)
                    .where(predicate.getValue());
                
            MemberRegistrationReportModel reportModel = new MemberRegistrationReportModel();
            
            //CS CRM
            BooleanBuilder csRegPredicate = new BooleanBuilder(predicate.getValue());
            csRegPredicate.and(qMemberModel.memberCreatedFromType.eq(MemberCreatedFromType.FROM_CRM));
            
            //CS UPLOAD
            BooleanBuilder uploadRegPredicate = new BooleanBuilder(predicate.getValue());
            uploadRegPredicate.and(qMemberModel.memberCreatedFromType.eq(MemberCreatedFromType.FROM_UPLOAD));
            
            //Member Portal
            BooleanBuilder onlineRegPredicate = new BooleanBuilder(predicate.getValue());
            onlineRegPredicate.and(qMemberModel.memberCreatedFromType.eq(MemberCreatedFromType.FROM_PORTAL));
            
            query = new JPAQuery(em).from(qMemberModel)
                    .where(csRegPredicate);
            
            try {
                reportModel.setCsRegCount(query.count());
            } catch (NoResultException e) {
                reportModel.setCsRegCount(0L);
            }
            
            query = new JPAQuery(em).from(qMemberModel)
                    .where(uploadRegPredicate);
            
            try {
                reportModel.setCsUploadCount(query.count());
            } catch (NoResultException e) {
                reportModel.setCsUploadCount(0L);
            }
            
            query = new JPAQuery(em).from(qMemberModel)
                    .where(onlineRegPredicate);
            
            try {
                reportModel.setOnlineRegCount(query.count());
            } catch (NoResultException e) {
                reportModel.setOnlineRegCount(0L);
            }
            
            list.add(reportModel);
            
            parameters.put(FILTER_ACTIVATIONDATE_FROM, activationDateFrom);
            parameters.put(FILTER_ACTIVATIONDATE_TO, activationDateTo);
            if (customerType != null) {
                LookupDetail customer = lookupService.getActiveDetailByCode(customerType);
                parameters.put(FILTER_CUSTOMER_TYPE, customer.getDescription());
//                parameters.put(FILTER_CUSTOMER_TYPE, customer.getCode() + " - " + customer.getDescription());
            }
            parameters.put(PARAM_PRINT_DATE, FULL_DATE_PATTERN.print(new LocalDate()));
            parameters.put(PARAM_PRINT_BY, getCurrentUser());
        }
        
        List<ReportBean> beansDataSource = Lists.newArrayList();
        
        if (!list.isEmpty())
            beansDataSource.add(new ReportBean(list));
    	
    	DefaultJRProcessor jrProcessor = null;
    	if (memberReportType.equalsIgnoreCase(MEMBER_REPORT_TYPE_STORE))
    	    jrProcessor = new DefaultJRProcessor(REPORT_NAME_BY_STORE, beansDataSource);
    	else if (memberReportType.equalsIgnoreCase(MEMBER_REPORT_TYPE_REGISTER_TYPE))
    	    jrProcessor = new DefaultJRProcessor(REPORT_NAME_BY_TYPE, beansDataSource);
    	else if (memberReportType.equalsIgnoreCase(MEMBER_REPORT_TYPE_SUMMARY))
            jrProcessor = new DefaultJRProcessor(REPORT_NAME_SUMMARY, beansDataSource);
    	jrProcessor.addParameters(parameters);
    	jrProcessor.setFileResolver(new ClasspathFileResolver("reports"));
    	
    	return jrProcessor;
	
    }
    
    private String getCurrentUser() {
        if (UserUtil.getCurrentUser() != null) {
            return UserUtil.getCurrentUser().getUsername();
        } else {
            return null;
        }
    }

    private DateTime toDateTime(String dateString) {
        if (StringUtils.isNotBlank(dateString)) {
            return INPUT_DATE_PATTERN.parseDateTime(dateString);
        }
        return null;
    }
    
    public static class MemberRegistrationReportModel {
        private String store;
        private String storeName;
        private long csRegCount;
        private long csUploadCount;
        private long onlineRegCount;
        
        public String getStore() {
            return store;
        }
        public String getStoreName() {
            return storeName;
        }
        public long getCsRegCount() {
            return csRegCount;
        }
        public long getCsUploadCount() {
            return csUploadCount;
        }
        public long getOnlineRegCount() {
            return onlineRegCount;
        }
        public void setStore(String store) {
            this.store = store;
        }
        public void setStoreName(String storeName) {
            this.storeName = storeName;
        }
        public void setCsRegCount(long csRegCount) {
            this.csRegCount = csRegCount;
        }
        public void setCsUploadCount(long csUploadCount) {
            this.csUploadCount = csUploadCount;
        }
        public void setOnlineRegCount(long onlineRegCount) {
            this.onlineRegCount = onlineRegCount;
        }
    }
    
    public static class ReportBean {
        
        private List<MemberRegistrationReportModel> list;
        
        public ReportBean() {}

        
        public ReportBean(List<MemberRegistrationReportModel> list) {
            super();
            this.list = list;
        }

        public List<MemberRegistrationReportModel> getList() {
            return list;
        }

        public void setList(List<MemberRegistrationReportModel> list) {
            this.list = list;
        }
        
    }
}
