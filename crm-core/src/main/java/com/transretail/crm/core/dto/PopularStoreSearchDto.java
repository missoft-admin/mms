package com.transretail.crm.core.dto;


import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang3.time.DateUtils;
import org.joda.time.LocalDateTime;

import com.mysema.query.types.expr.BooleanExpression;
import com.transretail.crm.common.service.dto.request.AbstractSearchFormDto;
import com.transretail.crm.core.entity.QPosTransaction;


public class PopularStoreSearchDto extends AbstractSearchFormDto {

	private Boolean dateNe;
    private Date startDate;
	private Date endDate;

    @Override
    public BooleanExpression createSearchExpression() {
		List<BooleanExpression> expr = new ArrayList<BooleanExpression>();
		QPosTransaction qTxn = QPosTransaction.posTransaction;

		if ( dateNe.equals(true ) ) {
			if ( null == startDate && null != endDate ) {
				startDate = DateUtils.addDays( endDate, -30 );
	    	}
			else if ( null != startDate && null == endDate ) {
				endDate = DateUtils.addDays( startDate, 30 );
	    	}
			else if ( null == startDate && null == endDate ) {
				startDate = DateUtils.truncate( new Date(), Calendar.MONTH );
				endDate = DateUtils.ceiling( new Date(), Calendar.MONTH );
			}
			startDate = DateUtils.truncate( startDate, Calendar.DATE );
			endDate = DateUtils.addMilliseconds( DateUtils.ceiling( endDate, Calendar.DATE ), -1 );
		}
		if ( null != startDate ) {
			expr.add( qTxn.transactionDate.goe( new LocalDateTime( startDate.getTime() ) ) );
		}
		if ( null != endDate ) {
			expr.add( qTxn.transactionDate.loe( new LocalDateTime( endDate.getTime() ) ) );
		}

		return BooleanExpression.allOf( expr.toArray( new BooleanExpression[expr.size()] ) );
    }



	public void setDateNe(Boolean dateNe) {
		this.dateNe = dateNe;
	}
    public Date getStartDate() {
		return startDate;
	}
	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}
	public Date getEndDate() {
		return endDate;
	}
	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

}
