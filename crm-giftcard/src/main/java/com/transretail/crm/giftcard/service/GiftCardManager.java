package com.transretail.crm.giftcard.service;

import java.util.Set;

import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import com.transretail.crm.giftcard.dto.GiftCardTransactionInfo;
import com.transretail.crm.giftcard.dto.GiftCardInfo;
import com.transretail.crm.giftcard.entity.GiftCardInventory;
import com.transretail.crm.giftcard.entity.SalesOrder;
import com.transretail.crm.giftcard.entity.support.GiftCardSalesType;
import java.util.List;
import javax.annotation.Nullable;

/**
 * Responsible for giftcard transaction
 *
 * @author Monte Cillo Co (mco@exist.com)
 */
public interface GiftCardManager {

    public static final DateTimeFormatter TXNNO_DT_FORMATTER = DateTimeFormat.forPattern("YYMMddhhmmssSSS");

    /**
     * Gift Card inquiry
     *
     * @param transactionNo
     * @param cardNo - the card no to be retrieve
     * @param merchantId - store code to be used to check if the request card is
     * present in the stock of requesting merchant
     * @return the gift card info of the requested giftcard.
     */
    GiftCardInfo inquire(@Nullable String transactionNo, String cardNo, String merchantId);

    GiftCardInfo preActivate(GiftCardSalesType salesType, GiftCardTransactionInfo txInfo, String cardNo, String accountId);

    GiftCardInventory validateActivation(GiftCardTransactionInfo txInfo, String cardNo);
    
    GiftCardInventory validateActivation(GiftCardTransactionInfo txInfo, GiftCardSalesType salesType, String cardNo, String accountId);

    Set<GiftCardInfo> activate(GiftCardTransactionInfo cardTransactionInfo, Set<String> cards, GiftCardSalesType salesType, boolean ryns);
    
    Set<GiftCardInfo> activateFreeVoucher(GiftCardTransactionInfo cardTransactionInfo, Set<String> cards);
    
    Set<GiftCardInfo> activate(GiftCardTransactionInfo cardTransactionInfo, Set<String> cards, String accountId, GiftCardSalesType salesType, boolean ryns);

    void activateB2B(SalesOrder salesOrder, List<GiftCardInventory> allocatedCards);

    void returnB2B(SalesOrder salesOrder);

    Set<GiftCardInfo> voidActivation(GiftCardTransactionInfo cardTransactionInfo);

    GiftCardInfo preRedeem(GiftCardTransactionInfo txInfo, String cardNo, Double redeemAmount);

    GiftCardInventory validateRedemption(GiftCardTransactionInfo txInfo, String cardNo, Double redeemAmount);

    Set<GiftCardInfo> redeem(GiftCardTransactionInfo cardTransactionInfo, String cardNo, Double redeemAmount);

    Set<GiftCardInfo> voidRedemption(GiftCardTransactionInfo cardTransactionInfo);

    Set<GiftCardInfo> reload(GiftCardTransactionInfo txInfo, String cardNo, Long reloadAmount);

    GiftCardInfo preReload(String cardNo, String merchantId, Double amount);

    void returnB2BByCard(SalesOrder salesOrder, Iterable<GiftCardInventory> cards);

    Set<GiftCardInfo> reloadVoid(GiftCardTransactionInfo txInfo);
}
