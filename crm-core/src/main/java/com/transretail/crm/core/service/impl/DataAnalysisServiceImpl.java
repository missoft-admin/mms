package com.transretail.crm.core.service.impl;

import java.math.BigDecimal;
import java.math.MathContext;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.BooleanUtils;
import org.apache.commons.lang3.StringUtils;
import org.hibernate.ScrollMode;
import org.hibernate.ScrollableResults;
import org.hibernate.StatelessSession;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.mysema.query.BooleanBuilder;
import com.mysema.query.Tuple;
import com.mysema.query.jpa.hibernate.HibernateQuery;
import com.mysema.query.jpa.impl.JPAQuery;
import com.mysema.query.types.ConstantImpl;
import com.mysema.query.types.Expression;
import com.mysema.query.types.expr.BooleanExpression;
import com.mysema.query.types.expr.DateTimeExpression;
import com.mysema.query.types.expr.NumberExpression;
import com.mysema.query.types.template.DateTimeTemplate;
import com.transretail.crm.common.reporting.jasper.ClasspathFileResolver;
import com.transretail.crm.common.reporting.jasper.DefaultJRProcessor;
import com.transretail.crm.common.reporting.jasper.JRProcessor;
import com.transretail.crm.core.dto.DataAnalysisLMHDto;
import com.transretail.crm.core.dto.DataAnalysisLMHQueryResult;
import com.transretail.crm.core.dto.DataAnalysisOverviewDto;
import com.transretail.crm.core.dto.DataAnalysisSearchDto;
import com.transretail.crm.core.dto.QDataAnalysisLMHQueryResult;
import com.transretail.crm.core.entity.ProductGroup;
import com.transretail.crm.core.entity.QPointsTxnModel;
import com.transretail.crm.core.entity.QPosTxItem;
import com.transretail.crm.core.entity.QProductGroup;
import com.transretail.crm.core.entity.enums.DataAnalysisLMHAnalysisBy;
import com.transretail.crm.core.entity.enums.DataAnalysisLMHBy;
import com.transretail.crm.core.entity.enums.DataAnalysisOverview;
import com.transretail.crm.core.entity.lookup.QProduct;
import com.transretail.crm.core.entity.lookup.QStoreGroup;
import com.transretail.crm.core.entity.lookup.StoreGroup;
import com.transretail.crm.core.repo.ProductGroupRepo;
import com.transretail.crm.core.repo.StoreGroupRepo;
import com.transretail.crm.core.service.DataAnalysisService;

@Service("dataAnalysisService")
@Transactional
public class DataAnalysisServiceImpl implements DataAnalysisService {
	
	@PersistenceContext
	EntityManager em;
	@Autowired
	StoreGroupRepo storeGroupRepo;
	@Autowired
	ProductGroupRepo productGroupRepo;
	@Autowired
	MessageSource messageSource;
	@Autowired
    private StatelessSession statelessSession;
	private static final MathContext ROUNDING_CONTEXT = MathContext.DECIMAL32;
	private static final int MULTIPLIER = 1000;
	@Override
	public List<Integer> getYears() {
		QPointsTxnModel qPoints = QPointsTxnModel.pointsTxnModel;
        return new JPAQuery(em).from(qPoints).where(qPoints.transactionDateTime.isNotNull()).groupBy(
            qPoints.transactionDateTime.year()).list(qPoints.transactionDateTime.year());
    }
	
	@Override
	public List<DataAnalysisOverviewDto> getOverviewAnalysis(DataAnalysisSearchDto searchDto) {
		QPointsTxnModel qPoints = QPointsTxnModel.pointsTxnModel;
		String type = searchDto.getOverview();
		Integer currentYear = new DateTime().getYear();
		Integer prevYear = new DateTime().minusYears(1).getYear();
		Integer currentMo = new DateTime().getMonthOfYear();
		
		BooleanExpression advancedFilters = getAdvanceSearchFilters(searchDto, qPoints);
		
		List<DataAnalysisOverviewDto> list = Lists.newArrayList();
		
		for(int i=1; i<=12; i++) {
			Integer currentYearM = Integer.parseInt(currentYear + String.format("%02d", i));
			Integer prevYearM = Integer.parseInt(prevYear + String.format("%02d", i));
			
			DataAnalysisOverviewDto dto = new DataAnalysisOverviewDto();
			
			if(DataAnalysisOverview.SALES_VALUE.getCode().equalsIgnoreCase(type)) {
				Double current = new JPAQuery(em).from(qPoints)
						.where(qPoints.transactionDateTime.yearMonth().eq(currentYearM).and(advancedFilters))
						.singleResult(qPoints.transactionAmount.sum().divide(MULTIPLIER));
				
				Double previous = new JPAQuery(em).from(qPoints)
						.where(qPoints.transactionDateTime.yearMonth().eq(prevYearM).and(advancedFilters))
						.singleResult(qPoints.transactionAmount.sum().divide(MULTIPLIER));
				
				if(current == null && i <= currentMo)
					dto.setCurrent(0D);
				else
					dto.setCurrent(current);
				dto.setPrevious(previous);
			} else if(DataAnalysisOverview.TXN_COUNT.getCode().equalsIgnoreCase(type)) {
				Long current = new JPAQuery(em).from(qPoints)
						.where(qPoints.transactionDateTime.yearMonth().eq(currentYearM).and(advancedFilters))
						.singleResult(qPoints.transactionNo.countDistinct());
				
				Long previous = new JPAQuery(em).from(qPoints)
						.where(qPoints.transactionDateTime.yearMonth().eq(prevYearM).and(advancedFilters))
						.singleResult(qPoints.transactionNo.countDistinct());
				
				if(current != 0 || i <= currentMo)
					dto.setCurrent(current.doubleValue());
				if(previous != null)
					dto.setPrevious(previous.doubleValue());
			} else if(DataAnalysisOverview.SHOPPER_COUNT.getCode().equalsIgnoreCase(type)) {
				Long current = new JPAQuery(em).from(qPoints)
						.where(qPoints.transactionDateTime.yearMonth().eq(currentYearM).and(advancedFilters))
						.singleResult(qPoints.memberModel.id.countDistinct());
				
				Long previous = new JPAQuery(em).from(qPoints)
						.where(qPoints.transactionDateTime.yearMonth().eq(prevYearM).and(advancedFilters))
						.singleResult(qPoints.memberModel.id.countDistinct());
				
				if(current != 0 || i <= currentMo)
					dto.setCurrent(current.doubleValue());
				if(previous != null)
					dto.setPrevious(previous.doubleValue());
			} else if(DataAnalysisOverview.AVG_VISIT_FREQ.getCode().equalsIgnoreCase(type)) {
				Tuple current = new JPAQuery(em).from(qPoints)
						.where(qPoints.transactionDateTime.yearMonth().eq(currentYearM).and(advancedFilters))
						.singleResult(qPoints.transactionNo.countDistinct(), qPoints.memberModel.id.countDistinct());
				
				Tuple previous = new JPAQuery(em).from(qPoints)
						.where(qPoints.transactionDateTime.yearMonth().eq(prevYearM).and(advancedFilters))
						.singleResult(qPoints.transactionNo.countDistinct(), qPoints.memberModel.id.countDistinct());
				
				Long currentM = current.get(qPoints.memberModel.id.countDistinct());
				Long previousM = previous.get(qPoints.memberModel.id.countDistinct());
				if(currentM == 0 &&  i <= currentMo)
					dto.setCurrent(0D);
				else if(currentM != 0)
					dto.setCurrent(current.get(qPoints.transactionNo.countDistinct()).doubleValue() / currentM.doubleValue());
				if(previousM != 0)
					dto.setPrevious(previous.get(qPoints.transactionNo.countDistinct()).doubleValue() / previousM.doubleValue());
			}
			
			dto.setMonth(new DateTime().withMonthOfYear(i).monthOfYear().getAsText(LocaleContextHolder.getLocale()));
			list.add(dto);
		}
		
		return list;
	
	}
	
	@Override
	public List<DataAnalysisLMHDto> getLMHAnalysis(DataAnalysisSearchDto searchDto) {
		QPointsTxnModel qPoints = QPointsTxnModel.pointsTxnModel;
		BooleanExpression advancedFilters = getAdvanceSearchFilters(searchDto, qPoints);
		NumberExpression<Double> countExp = null;
		if(DataAnalysisLMHBy.TXN_AMOUNT.getCode().equalsIgnoreCase(searchDto.getLmhBy())) 
			countExp = qPoints.transactionAmount.sum();
		else if(DataAnalysisLMHBy.VISIT_FREQ.getCode().equalsIgnoreCase(searchDto.getLmhBy()))
			countExp = qPoints.transactionNo.countDistinct().castToNum(Double.class);
		
		NumberExpression<Double> analysisResult = null;
		if(DataAnalysisLMHAnalysisBy.SALES_VALUE.getCode().equalsIgnoreCase(searchDto.getAnalysisBy())) {
			analysisResult = qPoints.transactionAmount.sum();
		} 
		/*else if(DataAnalysisLMHAnalysisBy.BASKET_SIZE_PER_VISIT.getCode().equalsIgnoreCase(searchDto.getAnalysisBy())) {
			QPosTxItem qItem = QPosTxItem.posTxItem;
			QPointsTxnModel qPointsA = new QPointsTxnModel("pointsA"); //TODO
			ListSubQuery<String> txnNos = new HibernateSubQuery().from(qPointsA)
				.where(qPointsA.memberModel.id.eq(qPoints.memberModel.id).and(qPointsA.transactionDateTime.yearMonth().eq(qPoints.transactionDateTime.yearMonth())))
				.list(qPoints.transactionNo);
			analysisResult = new HibernateSubQuery().from(qItem).where(qItem.posTransaction.id.in(txnNos)).unique(qItem.quantity.sum());
		}*/ 
		else if(DataAnalysisLMHAnalysisBy.SHOPPER_COUNT.getCode().equalsIgnoreCase(searchDto.getAnalysisBy())) {
			analysisResult = qPoints.memberModel.id.countDistinct().castToNum(Double.class);
		} else if(DataAnalysisLMHAnalysisBy.AVG_VISIT_FREQ.getCode().equalsIgnoreCase(searchDto.getAnalysisBy())) {
			analysisResult = qPoints.transactionNo.countDistinct().castToNum(Double.class);
		}
		
		List<DataAnalysisLMHDto> list = new ArrayList<DataAnalysisLMHDto>();
		
		Integer startMonth = Integer.parseInt(searchDto.getYearFrom() + String.format("%02d", searchDto.getMonthFrom()));
		Integer endMonth = Integer.parseInt(searchDto.getYearTo() + String.format("%02d", searchDto.getMonthTo()));
		
		BigDecimal multiplier = BigDecimal.ONE;
		if(DataAnalysisLMHAnalysisBy.SALES_VALUE.getCode().equalsIgnoreCase(searchDto.getAnalysisBy())) {
			multiplier = new BigDecimal(MULTIPLIER);
		}
		
		for(int i=searchDto.getYearFrom(); i<=searchDto.getYearTo(); i++) {
			for(int j=1; j<=12; j++) {
				Integer month = Integer.parseInt(i + String.format("%02d", j));
				
				if(startMonth <= month && month <= endMonth) {
					Double monthAvgD = new JPAQuery(em).from(qPoints)
						.where(qPoints.transactionDateTime.yearMonth().eq(month).and(advancedFilters))
						.singleResult(countExp.divide(qPoints.memberModel.id.countDistinct().nullif(0L)));
					
					BigDecimal monthAvg = null;
					if(monthAvgD != null)
						monthAvg = new BigDecimal(monthAvgD);

					ScrollableResults scrollableResults = createQuery(qPoints, month, advancedFilters)
		        		.createQuery(countExp/*, analysisResult*/).scroll(ScrollMode.FORWARD_ONLY);
				
					BigDecimal varianceSum = BigDecimal.ZERO;
					int count = 0;
		            while (scrollableResults.next()) {
		                Double result = (Double) scrollableResults.get(0);
						varianceSum = varianceSum.add(new BigDecimal(result).subtract(monthAvg).pow(2));
		                ++count;
		            }
		            
		            Double variance = 0D;
		            if(varianceSum.signum() != 0)
		            	variance = Math.sqrt(varianceSum.divide(new BigDecimal(count - 1), ROUNDING_CONTEXT).doubleValue() /*varianceSum / (count - 1)*/);
		            
					BigDecimal belowVariance = new BigDecimal(variance).multiply(new BigDecimal(-.5))/* variance * -.5*/;
					BigDecimal aboveVariance = new BigDecimal(variance).multiply(new BigDecimal(.5))/*variance * .5*/;
					
					BigDecimal light = BigDecimal.ZERO, medium = BigDecimal.ZERO, heavy = BigDecimal.ZERO;
					String lightMembers = "", mediumMembers = "", heavyMembers = "";
					int lightCount = 0, mediumCount = 0, heavyCount = 0;
					
					scrollableResults = createQuery(qPoints, month, advancedFilters)
			        		.createQuery(new QDataAnalysisLMHQueryResult(qPoints.memberModel.id, countExp, analysisResult))
                        .scroll(ScrollMode.FORWARD_ONLY);
					
					while (scrollableResults.next()) {
						DataAnalysisLMHQueryResult result = (DataAnalysisLMHQueryResult) scrollableResults.get(0);
						BigDecimal compare = new BigDecimal(result.getCount()).subtract(monthAvg);
						if(compare.compareTo(belowVariance) <= 0/*compare <= belowVariance*/) {
							lightMembers += result.getMemberId() + ","; 
							light = light.add(new BigDecimal(result.getAnalysisCount()));
							lightCount++;
						} else if(compare.compareTo(aboveVariance) >= 0/*  compare >= aboveVariance*/) {
							heavyMembers += result.getMemberId() + ",";
							heavy = heavy.add(new BigDecimal(result.getAnalysisCount()));
							heavyCount++;
						} else {
							mediumMembers += result.getMemberId() + ",";
							medium = medium.add(new BigDecimal(result.getAnalysisCount()));
							mediumCount++;
						}
					}
					
					if(DataAnalysisLMHAnalysisBy.AVG_VISIT_FREQ.getCode().equalsIgnoreCase(searchDto.getAnalysisBy())) {
						if(lightCount != 0)
							light = light.divide(new BigDecimal(lightCount), ROUNDING_CONTEXT); 
						else
							light = null;
						if(mediumCount != 0)
							medium = medium.divide(new BigDecimal(mediumCount), ROUNDING_CONTEXT); 
						else
							medium = null;
						if(heavyCount != 0)
							heavy = heavy.divide(new BigDecimal(heavyCount), ROUNDING_CONTEXT); 
						else
							heavy = null;
					}
					
					DataAnalysisLMHDto lmhDto = new DataAnalysisLMHDto();
					DateTime date = DateTimeFormat.forPattern("yyyyMM").parseDateTime(Integer.toString(month));
					lmhDto.setMonthYear(DateTimeFormat.forPattern("MMM yyyy").print(date));
					lmhDto.setLightMembers(lightMembers);
					lmhDto.setMediumMembers(mediumMembers);
					lmhDto.setHeavyMembers(heavyMembers);
					lmhDto.setLight(light.divide(multiplier, ROUNDING_CONTEXT));
					lmhDto.setMedium(medium.divide(multiplier, ROUNDING_CONTEXT));
					lmhDto.setHeavy(heavy.divide(multiplier, ROUNDING_CONTEXT));
					list.add(lmhDto);
				}
			}
		}

		return list;
	}
	
	private HibernateQuery createQuery(QPointsTxnModel qPoints, Integer month, BooleanExpression advancedFilters) {
		return new HibernateQuery(statelessSession).from(qPoints)
	            .where(qPoints.transactionDateTime.yearMonth().eq(month).and(advancedFilters))
	            .groupBy(qPoints.memberModel.id);
	}
	
	@Override
	public JRProcessor createOverviewJrProcessor(DataAnalysisSearchDto searchDto) {
    	Map<String, Object> parameters = Maps.newHashMap();
    	List<DataAnalysisOverviewDto> list = getOverviewAnalysis(searchDto);
    	parameters.put("graphTitle", messageSource.getMessage("title_analysisby_" + searchDto.getOverview(), null, LocaleContextHolder.getLocale()));
    	Integer currentYear = new DateTime().getYear();
    	Integer previousYear = new DateTime().minusYears(1).getYear();
    	parameters.put("currentYear", currentYear);
    	parameters.put("previousYear", previousYear);
    	
    	if(DataAnalysisOverview.SALES_VALUE.getCode().equalsIgnoreCase(searchDto.getOverview())) {
    		parameters.put("legendStr", messageSource.getMessage("legend_one_is_to_thousand", null, LocaleContextHolder.getLocale()));
    	}
    	
    	DecimalFormat df = new DecimalFormat("#.##");
    	if(DataAnalysisOverview.SALES_VALUE.getCode().equalsIgnoreCase(searchDto.getOverview()) 
				|| DataAnalysisOverview.AVG_VISIT_FREQ.getCode().equalsIgnoreCase(searchDto.getOverview())) {
    		df = new DecimalFormat("#,##0.00;(#,##0.00)");
		} else {
			df = new DecimalFormat("#,##0;(#,##0)");
		}
    	
    	DecimalFormat dfPercent = new DecimalFormat("0.00;(0.00)");
    	List<OverviewReportBean> beans = new ArrayList<OverviewReportBean>();
    	OverviewReportBean bean = new OverviewReportBean();
    	bean.setCurrentStr(Integer.toString(currentYear));
    	bean.setPreviousStr(Integer.toString(previousYear));
    	bean.setDiffStr(messageSource.getMessage("label_overview_table_diff", null, LocaleContextHolder.getLocale()));
    	bean.setDiffPercentStr(messageSource.getMessage("label_overview_table_diffpercent", null, LocaleContextHolder.getLocale()));
    	beans.add(bean);
    	if(CollectionUtils.isEmpty(list)) {
    		parameters.put("isEmpty", true);
    		beans.add(new OverviewReportBean());
    		list.add(new DataAnalysisOverviewDto());
    	} else {
    		for(DataAnalysisOverviewDto dto: list) {
    			bean = new OverviewReportBean();
        		bean.setMonth(dto.getMonth());
        		if(dto.getCurrent() != null) {
        			bean.setCurrentStr(df.format(dto.getCurrent()));
        		}
        		if(dto.getPrevious() != null) {
        			bean.setPreviousStr(df.format(dto.getPrevious()));
        		}
        		if(dto.getDiff() != null) {
        			bean.setDiffStr(df.format(dto.getDiff()));
        		}
        		if(dto.getDiffPercent() != null) {
        			bean.setDiffPercentStr(dfPercent.format(dto.getDiffPercent()));
        		}
        		beans.add(bean);
    		}
    	}
    	parameters.put("tableData", beans);
    	
        DefaultJRProcessor jrProcessor = new DefaultJRProcessor("reports/data_analysis_overview.jasper", list);
        jrProcessor.addParameters(parameters);
        jrProcessor.setFileResolver(new ClasspathFileResolver("reports"));
        return jrProcessor;
    }
	
	@Override
	public JRProcessor createLMHJrProcessor(DataAnalysisSearchDto searchDto) {
    	Map<String, Object> parameters = Maps.newHashMap();
    	List<DataAnalysisLMHDto> list = getLMHAnalysis(searchDto);
    	parameters.put("graphTitle", messageSource.getMessage("label_lmh_analysis_" + searchDto.getAnalysisBy(), null, LocaleContextHolder.getLocale()));
    	List<LMHReportBean> beans = new ArrayList<LMHReportBean>();
    	
    	DecimalFormat df = new DecimalFormat("#.##");
    	if(DataAnalysisLMHAnalysisBy.SALES_VALUE.getCode().equalsIgnoreCase(searchDto.getAnalysisBy()) 
				|| DataAnalysisLMHAnalysisBy.AVG_VISIT_FREQ.getCode().equalsIgnoreCase(searchDto.getAnalysisBy())) {
    		df = new DecimalFormat("#,##0.00;(#,##0.00)");
		} else {
			df = new DecimalFormat("#,##0;(#,##0)");
		}
    	
    	if(DataAnalysisLMHAnalysisBy.SALES_VALUE.getCode().equalsIgnoreCase(searchDto.getAnalysisBy())) {
    		parameters.put("legendStr", messageSource.getMessage("legend_one_is_to_thousand", null, LocaleContextHolder.getLocale()));
    	}
    	
    	LMHReportBean bean = new LMHReportBean();
    	bean.setLightStr(messageSource.getMessage("label_lmh_light", null, LocaleContextHolder.getLocale()));
    	bean.setMediumStr(messageSource.getMessage("label_lmh_medium", null, LocaleContextHolder.getLocale()));
    	bean.setHeavyStr(messageSource.getMessage("label_lmh_heavy", null, LocaleContextHolder.getLocale()));
    	beans.add(bean);
    	
    	if(CollectionUtils.isEmpty(list)) {
    		parameters.put("isEmpty", true);
    		list.add(new DataAnalysisLMHDto());
    		beans.add(new LMHReportBean());
    	} else {
    		for(DataAnalysisLMHDto dto: list) {
    			bean = new LMHReportBean();
        		if(dto.getLight() != null) {
        			bean.setLightStr(df.format(dto.getLight()));
        		}
        		if(dto.getMedium() != null) {
        			bean.setMediumStr(df.format(dto.getMedium()));
        		}
        		if(dto.getHeavy() != null) {
        			bean.setHeavyStr(df.format(dto.getHeavy()));
        		}
        		bean.setMonthYear(dto.getMonthYear());
        		beans.add(bean);
    		}
    		
    	}
    	parameters.put("tableData", beans);
        DefaultJRProcessor jrProcessor = new DefaultJRProcessor("reports/data_analysis_lmh.jasper", list);
        jrProcessor.addParameters(parameters);
        jrProcessor.setFileResolver(new ClasspathFileResolver("reports"));
        return jrProcessor;
    }
	
	public class LMHReportBean {
		private String monthYear;
		private String lightStr;
		private String mediumStr;
		private String heavyStr;
		public String getMonthYear() {
			return monthYear;
		}
		public void setMonthYear(String monthYear) {
			this.monthYear = monthYear;
		}
		public String getLightStr() {
			return lightStr;
		}
		public void setLightStr(String lightStr) {
			this.lightStr = lightStr;
		}
		public String getMediumStr() {
			return mediumStr;
		}
		public void setMediumStr(String mediumStr) {
			this.mediumStr = mediumStr;
		}
		public String getHeavyStr() {
			return heavyStr;
		}
		public void setHeavyStr(String heavyStr) {
			this.heavyStr = heavyStr;
		}
	}
	
	public class OverviewReportBean {
		private String month;
		private String currentStr;
		private String previousStr;
		private String diffStr;
		private String diffPercentStr;
		public String getMonth() {
			return month;
		}
		public void setMonth(String month) {
			this.month = month;
		}
		public String getCurrentStr() {
			return currentStr;
		}
		public void setCurrentStr(String currentStr) {
			this.currentStr = currentStr;
		}
		public String getPreviousStr() {
			return previousStr;
		}
		public void setPreviousStr(String previousStr) {
			this.previousStr = previousStr;
		}
		public String getDiffStr() {
			return diffStr;
		}
		public void setDiffStr(String diffStr) {
			this.diffStr = diffStr;
		}
		public String getDiffPercentStr() {
			return diffPercentStr;
		}
		public void setDiffPercentStr(String diffPercentStr) {
			this.diffPercentStr = diffPercentStr;
		}
	}
	
	
	private DateTime endOfYear(DateTime dateTime) {
	    return dateTime.withMillisOfSecond(999).withSecondOfMinute(59).withMinuteOfHour(59).withHourOfDay(23).withMonthOfYear(12).withDayOfMonth(31);
	}
	
	private DateTime endOfMonth(DateTime dateTime) {
	    return beginningOfMonth(dateTime.plusMonths(1)).minusMillis(1);
	}
	
	private DateTime beginningOfMonth(DateTime dateTime) {
	    return dateTime.withMillisOfSecond(0).withSecondOfMinute(0).withMinuteOfHour(0).withHourOfDay(0).withDayOfMonth(1);
	}

	private DateTime beginningOfYear(DateTime dateTime) {
	    return dateTime.withMillisOfSecond(0).withSecondOfMinute(0).withMinuteOfHour(0).withHourOfDay(0).withDayOfMonth(1).withMonthOfYear(1);
	}
	
	private DateTimeExpression<DateTime> toDate(Expression<?> expr, String pattern){
		return DateTimeTemplate.create(DateTime.class, "to_date({0},  '{1s}')", 
    			expr, 
    			ConstantImpl.create(pattern));
    }
	
	private BooleanExpression getAdvanceSearchFilters(DataAnalysisSearchDto searchDto, QPointsTxnModel qPoints) {
		List<BooleanExpression> filters = new ArrayList<BooleanExpression>();
		
		if(BooleanUtils.isTrue(searchDto.getAdvancedSearch())) {
			
			if(StringUtils.defaultString(searchDto.getStoreType()).equals("stores") && CollectionUtils.isNotEmpty(searchDto.getStores())) {
				filters.add(qPoints.storeCode.in(searchDto.getStores()));
			}
			
			if(StringUtils.defaultString(searchDto.getStoreType()).equals("storeGroups") && CollectionUtils.isNotEmpty(searchDto.getStoreGroups())) {
				QStoreGroup qGroup = QStoreGroup.storeGroup;
				Iterable<StoreGroup> groups = storeGroupRepo.findAll(qGroup.id.in(searchDto.getStoreGroups()));
				Set<String> storeCodes = new HashSet<String>();
				for(StoreGroup group: groups) {
					storeCodes.addAll(group.getStoreList());
				}
				if(CollectionUtils.isEmpty(storeCodes))
					return qPoints.id.isNull();
				filters.add(qPoints.storeCode.in(storeCodes));
			}
			
			if(StringUtils.defaultString(searchDto.getProductType()).equals("products") && CollectionUtils.isNotEmpty(searchDto.getProducts())) {
				QPosTxItem qItem = QPosTxItem.posTxItem;
				List<String> validTxns = new JPAQuery(em).from(qItem).where(qItem.productId.in(searchDto.getProducts())).distinct().list(qItem.posTransaction.id); 
				if(CollectionUtils.isEmpty(validTxns))
					return qPoints.id.isNull();
				filters.add(qPoints.transactionNo.in(validTxns));
			}
			
			if(StringUtils.defaultString(searchDto.getProductType()).equals("productGroups") && CollectionUtils.isNotEmpty(searchDto.getProductGroups())) {
				QProduct qProd = QProduct.product;
				QPosTxItem qPosTx = QPosTxItem.posTxItem;
				QProductGroup qGroup = QProductGroup.productGroup;
				Iterable<ProductGroup> groups = productGroupRepo.findAll(qGroup.id.in(searchDto.getProductGroups()));
				Set<String> validTxns = new HashSet<String>();
				for(ProductGroup group: groups) {
					List<String> invalidProds = Lists.newArrayList();
					if(CollectionUtils.isNotEmpty(group.getExcProductList())) {
						invalidProds = new JPAQuery(em).from(qProd).where(qProd.itemCode.in(group.getExcProductList())).list(qProd.id);
					}
					List<String> validProds = Lists.newArrayList();
					if(CollectionUtils.isNotEmpty(group.getProductList())) {
						validProds = new JPAQuery(em).from(qProd).where(qProd.itemCode.in(group.getProductList())).list(qProd.id);
						
					} else if(CollectionUtils.isNotEmpty(group.getProductCfnList())) {
						BooleanBuilder bld = new BooleanBuilder();
						for(String cfnCode: group.getProductCfnList()) {
							bld.or(qProd.itemCode.startsWith(cfnCode));
						}
						validProds = new JPAQuery(em).from(qProd).where(bld).list(qProd.id);
					}	
					
					BooleanExpression invalidProdsExp = null;
					if(CollectionUtils.isNotEmpty(invalidProds))
						invalidProdsExp = qPosTx.productId.notIn(invalidProds);
					
					if(CollectionUtils.isNotEmpty(validProds))
						validTxns.addAll(new JPAQuery(em).from(qPosTx).where(qPosTx.productId.in(validProds).and(invalidProdsExp)).distinct().list(qPosTx.posTransaction.id));
				}
				if(CollectionUtils.isEmpty(validTxns))
					return qPoints.id.isNull();
				filters.add(qPoints.transactionNo.in(validTxns));
			}
		}
		
		return BooleanExpression.allOf(filters.toArray(new BooleanExpression[filters.size()]));
	}
}
