package com.transretail.crm.web.controller;

import com.google.common.base.Function;
import com.google.common.collect.FluentIterable;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.transretail.crm.common.service.exception.GenericServiceException;
import com.transretail.crm.core.service.StoreService;
import com.transretail.crm.giftcard.GiftCardServiceResolvableException;
import com.transretail.crm.giftcard.dto.*;
import com.transretail.crm.giftcard.entity.GiftCardInventory;
import com.transretail.crm.giftcard.entity.support.GiftCardSaleTransaction;
import com.transretail.crm.giftcard.repo.custom.PosLoseTransactionService;
import com.transretail.crm.giftcard.service.GiftCardManager;
import com.transretail.crm.web.controller.util.ControllerResponse;
import java.util.List;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.tuple.Pair;
import org.joda.time.LocalDateTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.NoSuchMessageException;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.annotation.*;

import static com.transretail.crm.giftcard.entity.support.GiftCardSalesType.B2C;
import static com.transretail.crm.giftcard.entity.support.GiftCardSalesType.REBATE;

@Controller
@RequestMapping(value = "/giftcard/pos-lose-tx")
public class PosLoseTransactionController {

    @Autowired
    private PosLoseTransactionService posLoseTransactionService;
    @Autowired
    private StoreService storeService;
    @Autowired
    private MessageSource messageSource;
    @Autowired
    private GiftCardManager cardManager;

    private final static Logger LOGGER = LoggerFactory.getLogger(PosLoseTransactionController.class);

    private final static String PAGE_MAIN = "poslose/list";
    private final static String PAGE_UPDATE = "poslose/update";
    private final static String PAGE_VIEW = "poslose/view";

    private final static String ATTR_MODEL = "posLoseTxForm";
    private final static String ATTR_STORES = "stores";
    private final static String ATTR_ID = "id";
    private final static String ATTR_SEARCH = "searchcateg";
    private final static String ATTR_TX_TYPES = "transactionTypes";

    private final static Map<String, String> SEARCH_CATEGORIES;
    private final static List<Pair<String, GiftCardSaleTransaction>> types;

    static {
        SEARCH_CATEGORIES = Maps.newHashMap();
        SEARCH_CATEGORIES.put("giftCardNo", "Gift Card Number");
        SEARCH_CATEGORIES.put("terminalId", "Terminal Id");
        SEARCH_CATEGORIES.put("cashierId", "Cashier Id");
        SEARCH_CATEGORIES.put("transactionNo", "Transaction No");

        types = Lists.newArrayList();
        GiftCardSaleTransaction[] excluded = {GiftCardSaleTransaction.INQUIRY, GiftCardSaleTransaction.PRE_ACTIVATION, GiftCardSaleTransaction.PRE_REDEMPTION};
        for (GiftCardSaleTransaction type : GiftCardSaleTransaction.values()) {
            if (!ArrayUtils.contains(excluded, type)) {
                types.add(Pair.of(type.toString(), type));
            }
        }
    }

    @RequestMapping(produces = "text/html")
    public String list(Model uiModel) {
        uiModel.addAttribute(ATTR_SEARCH, SEARCH_CATEGORIES);

        return PAGE_MAIN;
    }

    @RequestMapping(value = "/new", produces = "text/html")
    public String showDialogCreate(Model uiModel) {
        PosLoseTxForm form = new PosLoseTxForm();
        form.setTransactionDate(LocalDateTime.now());

        uiModel.addAttribute(ATTR_MODEL, form);
//		uiModel.addAttribute(ATTR_TX_TYPES, GiftCardSaleTransaction.values());
        uiModel.addAttribute(ATTR_TX_TYPES, types);
        uiModel.addAttribute(ATTR_STORES, storeService.getAllStores());

        return PAGE_UPDATE;
    }

    @RequestMapping(value = "/view/{transactionNo}", produces = "text/html")
    public String showDialogView(@PathVariable String transactionNo, Model uiModel) {
        uiModel.addAttribute(ATTR_MODEL, posLoseTransactionService.findByTransactionNo(transactionNo));
        return PAGE_VIEW;
    }

    @RequestMapping(value = "/save", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public ControllerResponse createNewPOSLoseTransaction(@ModelAttribute("posLoseTxForm") @Valid PosLoseTxForm form,
            BindingResult result, HttpServletRequest request) {

        ControllerResponse response = new ControllerResponse();
        response.setSuccess(true);

        if (result.hasErrors()) {
            response.setSuccess(false);
            response.setResult(FluentIterable.from(result.getAllErrors()).transform(new Function<ObjectError, String>() {

                @Override
                public String apply(ObjectError input) {
                    return input.getDefaultMessage(); // TODO: if null, please do manual resolving of message
                }
            }).toList());

            return response;
        }

        try {

            posLoseTransactionService.createNewLoseTransaction(form);

        } catch (GiftCardServiceResolvableException ex) {

            String errorMessage;
            try {

                errorMessage = messageSource.getMessage(ex.getCodes()[0], ex.getArguments(), LocaleContextHolder.getLocale());

            } catch (NoSuchMessageException e) {

                errorMessage = ex.getDefaultMessage();
            }

            response.setSuccess(false);
            response.setResult(new Object[]{errorMessage});

        } catch (GenericServiceException gse) {

            String errorMessage = gse.getMessage();
            response.setSuccess(false);
            response.setResult(new Object[]{errorMessage});

        }

        return response;
    }

    @RequestMapping(value = "/search", method = RequestMethod.POST)
    @ResponseBody
    public LoseTransactionResultList search(@RequestBody PosLoseTransactionSearchDto searchDto) {
        return posLoseTransactionService.search(searchDto);
    }

    @RequestMapping(value = "/validate/{cardNo}", produces = "application/json")
    @ResponseBody
    public ControllerResponse validateCard(@PathVariable String cardNo, @RequestParam("merchantId") String merchant,
            @RequestParam("transactionType") GiftCardSaleTransaction transactionType) {
        ControllerResponse response = new ControllerResponse();
        response.setSuccess(true);
        GiftCardTransactionInfo txInfo = new GiftCardTransactionInfo().merchantId(merchant);
        try {
            GiftCardInfo gc = null;
            if (transactionType == GiftCardSaleTransaction.ACTIVATION) {
                GiftCardInventory gci = cardManager.validateActivation(txInfo, B2C, cardNo, null);
                gc = GiftCardInfo.convert(gci);
            } else if (transactionType == GiftCardSaleTransaction.GIFT_TO_CUSTOMER) {
                GiftCardInventory gci = cardManager.validateActivation(txInfo, REBATE, cardNo, null);
                gc = GiftCardInfo.convert(gci);
            } else if (transactionType == GiftCardSaleTransaction.REDEMPTION) {
                GiftCardInventory gci = cardManager.validateRedemption(txInfo, cardNo, 0.0);
                gc = GiftCardInfo.convert(gci);
            } else if (transactionType == GiftCardSaleTransaction.RELOAD) {
                gc = cardManager.preReload(cardNo, merchant, 0.0);
            }

            response.setResult(gc);
        } catch (GiftCardServiceResolvableException ex) {
            String errorMessage;
            try {
                errorMessage = messageSource.getMessage(ex.getCodes()[0], ex.getArguments(), LocaleContextHolder.getLocale());
            } catch (NoSuchMessageException e) {
                errorMessage = ex.getDefaultMessage();
            }

            response.setSuccess(false);
            response.setResult(new Object[]{errorMessage});
        }

        return response;
    }
}
