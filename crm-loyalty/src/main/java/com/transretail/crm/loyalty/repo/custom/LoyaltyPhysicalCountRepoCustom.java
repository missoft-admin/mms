package com.transretail.crm.loyalty.repo.custom;



import java.util.List;

import org.joda.time.LocalDateTime;

import com.transretail.crm.common.service.dto.request.PageSortDto;
import com.transretail.crm.common.service.dto.response.ResultList;
import com.transretail.crm.loyalty.dto.LoyaltyPhysicalCountSummaryDto;
import com.transretail.crm.loyalty.entity.LoyaltyPhysicalCount;

public interface LoyaltyPhysicalCountRepoCustom {

	ResultList<LoyaltyPhysicalCountSummaryDto> getSummary(PageSortDto sortDto);

	long processMissingInventory(Iterable<LoyaltyPhysicalCount> counts);

	List<LocalDateTime> getSummaryDates();

	long processReceivedInventory(Iterable<LoyaltyPhysicalCount> counts);}
