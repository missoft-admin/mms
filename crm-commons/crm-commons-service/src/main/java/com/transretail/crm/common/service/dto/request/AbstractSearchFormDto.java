package com.transretail.crm.common.service.dto.request;

/**
 *
 */
public abstract class AbstractSearchFormDto implements SearchFormDto {
    private PagingParam pagination = new PagingParam();

    public PagingParam getPagination() {
        return pagination;
    }

    public void setPagination(PagingParam pagination) {
        this.pagination = pagination;
    }
}
