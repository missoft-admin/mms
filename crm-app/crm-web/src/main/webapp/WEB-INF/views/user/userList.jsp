<%@ include file="../common/taglibs.jsp" %>


<div id="content_userList">

  <div id="list_user"
    data-url="<c:url value="/user/list" />"
    data-hdr-username="<spring:message code="user_prop_username"/>"
    data-hdr-firstname="<spring:message code="user_prop_firstname"/>"
    data-hdr-lastname="<spring:message code="user_prop_lastname"/>"
    data-hdr-enabled="<spring:message code="user_prop_enabled"/>" ></div>

  <div id="user-list-actions" class="hide">
    <a href="#" class="btn pull-left tiptip icn-edit btn_editUser" data-url="<c:url value="/user/edit/" />" data-id="%ID%" title="<spring:message code="label_edit"/>"><spring:message code="label_edit"/></a>
    <a href="#" class="btn pull-left tiptip icn-edit btn_editUserRolePerms" data-user="%USER%" data-id="%ID%" title="<spring:message code="label_assign_role_perms"/>"><spring:message code="label_assign_role_perms"/></a>
  	<a href="#" class="btn pull-left tiptip icn-pin btn_changePassword" data-url="<c:url value="/user/profile/password/change/" />" data-id="%UNAME%"
  		title="<spring:message code="user_label_changepassword"/>"><spring:message code="user_label_changepassword"/></a>
    <a href="#" class="btn pull-left tiptip icn-edit btn_assignExh" data-url="<c:url value="/user/assignexhibition/" />" data-id="%ID%"
      title="<spring:message code="user_label_assignexhibition"/>"><spring:message code="user_label_assignexhibition"/></a>
  </div>

  <div id="btn_deleteUser" data-url="<c:url value="/user/delete/" />" data-msg-confirm="<spring:message code="entity_delete_confirm" />" class="hide">
    <a href="#" class="btn btn_deleteUser" data-id="%ID%" ><spring:message code="label_delete"/></a>
  </div>


  <div id="rolePermDialog" class="modal hide nofly modal-dialog">
    <input type="hidden" class="selectedUserId" />
    <div class="modal-header">
      <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
      <h4><spring:message code="label_assign_role_perms"/> - <span id="selectedUser" class="uppercase"></span></h4>
      <div class="clearfix"></div>
    </div>
    <div class="modal-body">
      <div class="hide alert alert-error" id="content_error"><button type="button" class="close" onclick="$(this).parent().hide();">&times;</button><div><form:errors path="*"/></div></div>
      <div id="content_email" class="control-group form-horizontal hide">
        <label for="email" class="control-label pull-left"><spring:message code="user_prop_email"/></label>
        <div class="controls"><input id="email" type="text"/></div>
      </div>
      <div id="rolePerm" data-supervisor-label="<spring:message code="user_supervisor" />"></div>
    </div>
    <div class="modal-footer">
      <div class="">
	      <button type="button" class="btn btn-primary" id="saveRolePerm">Save</button>
	      <button type="button" class="btn" data-dismiss="modal">Cancel</button>
      </div>
      <div class="clearfix"></div>
    </div>
  </div>

  <link href="<spring:url value="/css/dataTables.css" />" rel="stylesheet">
  <script src="<spring:url value="/js/jquery/jquery.dataTables.js" />" ></script>
  <script src="<spring:url value="/js/jquery/jquery.dataTables.linkLengthMenu.js" />"></script>
  <script src="<spring:url value="/js/jquery/jquery.dataTables.customPaging.js" />"></script>
  <script src="<spring:url value="/js/jquery/ajax-datatable.js" />" ></script>
  <script src="<spring:url value="/js/common/form2js.js" />" ></script>
  <script src="<spring:url value="/js/common/json2.js" />" ></script>
  <script src="<spring:url value="/js/jquery/jquery.toObject.js" />" ></script>

  <script src="<spring:url value="/js/viewspecific/user/userList.js"/>"></script>

</div>
<style>
  div#selectedUser { margin-right: 25px; margin-top: 3px; }
  #rolePermDialog li {
    list-style: none;
  }

  #rolePermDialog .accordion-heading label {
    font-size: 16px;
    margin-top: 5px;
    text-transform: uppercase;
    
  }
  #rolePermDialog label {
    text-transform: none;
  }
  
  #rolePermDialog .modal-body {
    overflow-y: scroll;
  }
  
  /*For user list */
  #content_userList .table tbody tr td:last-child {
      width: auto ;
  }
</style>