package com.transretail.crm.core.repo.custom.impl;

import static org.junit.Assert.assertEquals;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.transretail.crm.core.entity.UserModel;
import com.transretail.crm.core.entity.UserPermission;
import com.transretail.crm.core.entity.UserRoleModel;
import com.transretail.crm.core.entity.UserRolePermission;
import com.transretail.crm.core.repo.UserPermissionRepo;
import com.transretail.crm.core.repo.UserRepo;
import com.transretail.crm.core.repo.UserRolePermissionRepo;
import com.transretail.crm.core.repo.UserRoleRepo;

/**
 * @author Allan G. Ramirez (agramirez@exist.com)
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = "classpath*:/META-INF/spring/applicationContext*.xml")
public class UserRoleImplTest {
    @Autowired
    private UserRepo userRepo;
    @Autowired
    private UserRoleRepo roleRepo;
    @Autowired
    private UserPermissionRepo permissionRepo;
    @Autowired
    private UserRolePermissionRepo userRolePermissionRepo;

    @Before
    public void onSetup() {
        UserModel user = new UserModel();
        user.setUsername("username");
        user.setPassword("password");
        user.setEnabled(true);
        user.setStoreCode("HO");
        user = userRepo.save(user);

        UserRoleModel role = new UserRoleModel("ROLE_CSS");
        role = roleRepo.save(role);

        UserPermission permission = new UserPermission("UPLOAD_MEMBER","Upload Member");
        permission = permissionRepo.save(permission);

        UserRolePermission userRolePermission = new UserRolePermission(user, role, permission);
        userRolePermissionRepo.save(userRolePermission);
    }

    @After
    public void tearDown() {
        userRolePermissionRepo.deleteAll();
        permissionRepo.deleteAll();
        roleRepo.deleteAll();
        userRepo.deleteAll();
    }

    @Test
    public void findByRolesTest() {
        assertEquals(1, userRepo.findByRoleCode("ROLE_CSS").size());
    }
}
