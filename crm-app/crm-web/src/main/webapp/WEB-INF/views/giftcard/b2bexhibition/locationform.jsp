<%@ include file="../../common/taglibs.jsp"%>

  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title" id="modalTitle"><spring:message code="b2b.location" /></h4>
      </div>
      <div class="modal-body">
        <div id="contentError" class="hide alert alert-error">
          <button type="button" class="close" onclick="$(this).parent().hide();">&times;</button>
          <div><form:errors path="*"/></div>
        </div>
        <form:form id="locationForm" name="locationForm" modelAttribute="locationForm" action="${pageContext.request.contextPath}/giftcard/b2b/location/save" method="POST" enctype="${ENCTYPE}" cssClass="form-reset form-horizontal" >
          <form:hidden path="id"  />
          
          <div class="control-group">
            <spring:message code="b2b.location.name" var="name" />
            <label for="name" class="control-label"><b class="required">* </b>${name}</label> 
            <div class="controls">
              <form:input path="name" class="form-control" placeholder="${name}" />      
            </div>
          </div>
          
          <div class="control-group">
            <spring:message code="b2b.location.address" var="address" />
            <label for="address" class="control-label">${address}</label> 
            <div class="controls">
              <form:input path="address" class="form-control" placeholder="${address}" />      
            </div>
          </div>
          
          <div class="control-group">
            <spring:message code="b2b.location.startdate" var="startdate" />
            <label for="startdate" class="control-label"><b class="required">* </b>${startdate}</label> 
            <div class="controls">
              <form:input path="startDate" id="startDate" class="form-control" placeholder="${startdate}" />      
            </div>
          </div>
          
          <div class="control-group">
            <spring:message code="b2b.location.enddate" var="enddate" />
            <label for="enddate" class="control-label"><b class="required">* </b>${enddate}</label> 
            <div class="controls">
              <form:input path="endDate" id="endDate" class="form-control" placeholder="${enddate}" />      
            </div>
          </div>
        
        </form:form>
      </div>
      <div class="modal-footer">
        <button type="button" id="formSubmit" class="formBtn btn btn-primary"><spring:message code="label_submit" /></button>
        <button type="button" id="formCancel" data-dismiss="modal" class="btn btn-default"><spring:message code="label_cancel"/></button>
      </div>
    </div>
  </div>

<script type="text/javascript">

var CONTAINER_ERROR = "#contentError > div";
var CONTENT_ERROR = "#contentError";

$(document).ready(function() {
	
	$("#startDate").datepicker({
    	autoclose : true,
		format: 'dd-mm-yyyy'
	}).on('changeDate', function(selected){
		startDate = new Date(selected.date.valueOf());
        startDate.setDate(startDate.getDate(new Date(selected.date.valueOf())));
        $('#endDate').datepicker('setStartDate', startDate);
	});
	$("#endDate").datepicker({
	  	autoclose : true,
		format: 'dd-mm-yyyy'
	}).on('changeDate', function(selected){
		FromEndDate = new Date(selected.date.valueOf());
        FromEndDate.setDate(FromEndDate.getDate(new Date(selected.date.valueOf())));
        $('#startDate').datepicker('setEndDate', FromEndDate);
	});

	$form = $("#locationForm");
	$locationDialog = $("#locationDialog");
	
	$("#formSubmit").click(submitForm);
	  
	  function submitForm() {
	    $(".formBtn").prop( "disabled", true );
	    $.post($form.attr("action"), $form.serialize(), function(data) {
	      $(".formBtn").prop( "disabled", false );
	      if (data.success) {
	        location.reload();
	      } 
	      else {
	        errorInfo = "";
	        for (i = 0; i < data.result.length; i++) {
	          errorInfo += "<br>" + (i + 1) + ". "
	              + ( data.result[i].code != undefined ? 
	                  data.result[i].code : data.result[i]);
	        }
	        $locationDialog.find(CONTAINER_ERROR).html("Please correct following errors: " + errorInfo);
	        $locationDialog.find(CONTENT_ERROR).show('slow');
	      }
	    }); 
	  }
});

</script>