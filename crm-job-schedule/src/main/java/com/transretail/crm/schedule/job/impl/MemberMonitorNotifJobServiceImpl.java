package com.transretail.crm.schedule.job.impl;

import java.util.Date;
import java.util.List;

import org.joda.time.LocalDate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.transretail.crm.common.web.notification.NotificationType;
import com.transretail.crm.core.dto.MemberResultList;
import com.transretail.crm.core.entity.Notification;
import com.transretail.crm.core.entity.UserModel;
import com.transretail.crm.core.service.CodePropertiesService;
import com.transretail.crm.core.service.MemberMonitorService;
import com.transretail.crm.core.service.NotificationService;
import com.transretail.crm.core.service.UserService;
import com.transretail.crm.schedule.job.service.MemberMonitorNotifJobService;

@Service("memberMonitorNotifJobService")
public class MemberMonitorNotifJobServiceImpl implements
		MemberMonitorNotifJobService {
	private static final Logger LOGGER = LoggerFactory.getLogger(MemberMonitorNotifJobServiceImpl.class);
	
	@Autowired
	private MemberMonitorService memberMonitorService;
	@Autowired
	private NotificationService notificationService;
	@Autowired
	private UserService userService;
	@Autowired
	private CodePropertiesService codePropertiesService;
	@Autowired
	private MessageSource messageSource;

	@Override
	@Transactional
	public void sendNotification() {
		LOGGER.error( ":sendNotification:: job executing at " + new Date().toString()  );
		LocalDate yesterday = LocalDate.now().minusDays(1);
		String yesterdayStr = yesterday.toString("dd-MM-yyyy");
		
		// individuals and professionals
		MemberResultList monitoredMembers = memberMonitorService.getMonitoredMembers(yesterday);
		if(monitoredMembers.getTotalElements() > 0) {
			List<UserModel> users = userService.findUsersByRoleCode(codePropertiesService.getCsCode());
			for(UserModel user : userService.findUsersByRoleCode(codePropertiesService.getCssCode())) {
				if(!users.contains(user)) {
					users.add(user);
				}
			}
			
			Notification notification = new Notification();
			notification.setItemId("monitored_members_" + yesterdayStr);
			notification.setType(NotificationType.MONITOR_MEMBER);
			notification.setInfo(monitoredMembers.getTotalElements() + "");
			notification = notificationService.saveNotification(notification, users);
		}
		
//		Map<String, Object> output = createOutputMessage(notification, monitoredMembers, yesterdayStr);
//		if(output != null) {
//			notifierService.sendToChannel("/notification/notify/monitor/cs", output);
//		}
		
		// employees
		monitoredMembers = memberMonitorService.getMonitoredEmployees(yesterday);
		if(monitoredMembers.getTotalElements() > 0) {
			List<UserModel> users = userService.findUsersByRoleCode(codePropertiesService.getHrCode());
			for(UserModel user : userService.findUsersByRoleCode(codePropertiesService.getHrsCode())) {
				if(!users.contains(user)) {
					users.add(user);
				}
			}
			
			Notification notification = new Notification();
			notification.setItemId("monitored_employees_" + yesterdayStr);
			notification.setType(NotificationType.MONITOR_EMPLOYEE);
			notification.setInfo(monitoredMembers.getTotalElements() + "");
			notification = notificationService.saveNotification(notification, users);
		}
//		output = createOutputMessage(notification, monitoredMembers, yesterdayStr);
//		if(output != null) {
//			notifierService.sendToChannel("/notification/notify/monitor/hr", output);
//		}
		
		LOGGER.error( ":sendNotification:: job successfully executed at " + new Date().toString()  );
	}
	
//	private Map<String, Object> createOutputMessage(Notification notification, MemberResultList monitoredMembers, String dateString) {
//		if(notification != null) {
//			Map<String, Object> output = Maps.newHashMap();
//			output.put("url", "/member/monitor");
//			output.put("time", notification.getCreated().toString("MMM d, yyyy - HH:mm"));
//			output.put("id", notification.getItemId());
//			output.put("type", NotificationType.MEMBER.toString().toLowerCase());
//			try {
//				output.put("message", messageSource.getMessage("label_notification_member", 
//						new Object[]{monitoredMembers.getTotalElements(), dateString}, 
//						LocaleContextHolder.getLocale()));
//			}
//			catch(Exception e) {
//				LOGGER.error("", e);
//				output.put("message", monitoredMembers.getTotalElements() + " new monitored members");
//			}
//			return output;
//		}
//		return null;
//	}
}
