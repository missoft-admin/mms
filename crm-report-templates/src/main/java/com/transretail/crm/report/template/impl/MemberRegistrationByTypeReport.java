package com.transretail.crm.report.template.impl;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.google.common.collect.Sets;
import com.mysema.query.Tuple;
import com.mysema.query.jpa.impl.JPAQuery;
import com.mysema.query.types.expr.BooleanExpression;
import com.transretail.crm.common.reporting.jasper.ClasspathFileResolver;
import com.transretail.crm.common.reporting.jasper.DefaultJRProcessor;
import com.transretail.crm.common.reporting.jasper.JRProcessor;
import com.transretail.crm.common.util.DateUtil;
import com.transretail.crm.core.entity.QMemberModel;
import com.transretail.crm.core.entity.enums.MemberCreatedFromType;
import com.transretail.crm.core.entity.enums.MemberStatus;
import com.transretail.crm.core.entity.enums.ReportType;
import com.transretail.crm.core.entity.lookup.QLookupDetail;
import com.transretail.crm.core.service.StoreService;
import com.transretail.crm.report.template.*;
import org.apache.commons.collections.comparators.ComparatorChain;
import org.apache.commons.lang3.StringUtils;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.MessageSourceAware;
import org.springframework.context.support.MessageSourceAccessor;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.*;

/**
 * @author Allan G. Ramirez (agramirez@exist.com)
 */
@Service("memberRegistrationByTypeReport")
public class MemberRegistrationByTypeReport implements ReportTemplate, MessageSourceAware, ReportsCustomizer, ReportDateFormatProvider{
    protected static final String TEMPLATE_NAME = "Member Registration By Type";
    protected static final String REPORT_NAME = "reports/member_registration_by_type.jasper";
    private static final String PARAM_ACTIVATION_DATERANGE = "activationDateRange";
    private static final String PARAM_IS_EMPTY = "isEmpty";
    private static final String PARAM_NO_DATA_FOUND = "noDataFound";
    protected static final String FILTER_ACTIVATIONDATE_FROM = CommonReportFilter.DATE_FROM;
    protected static final String FILTER_ACTIVATIONDATE_TO = CommonReportFilter.DATE_TO;
    protected static final String FILTER_STATUS = "status";
    protected static final String FILTER_MEMBER_TYPE = "memberType";
    private static final String SEARCH_DATE_FORMAT = DateUtil.SEARCH_DATE_FORMAT;
    private static final DateTimeFormatter INPUT_DATE_PATTERN = DateTimeFormat.forPattern(SEARCH_DATE_FORMAT);
    private static final DateTimeFormatter FULL_DATE_PATTERN = DateTimeFormat.forPattern("MMMM d, yyyy");
    private static final String PARAM_ALL_MEMBER_TYPES = "All Member Types";
    private MessageSourceAccessor messageSource;
    @PersistenceContext
    private EntityManager em;
    @Autowired
    private StoreService storeService;
    
    @Override
    public ReportType[] getReportTypes() {
    	return ReportType.values();
    }

    @Override
    public void setMessageSource(MessageSource messageSource) {
        this.messageSource = new MessageSourceAccessor(messageSource);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getName() {
        return TEMPLATE_NAME;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getDescription() {
        return messageSource.getMessage("member.registration.bytype.report.title");
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Set<FilterField> getFilters() {
        Set<FilterField> filters = new LinkedHashSet<FilterField>(2);
        filters.add(FilterField.createDateField(FILTER_ACTIVATIONDATE_FROM,
                messageSource.getMessage("member.registration.bytype.filter.activationdatefrom"), DateUtil.SEARCH_DATE_FORMAT, true));
        filters.add(FilterField.createDateField(FILTER_ACTIVATIONDATE_TO,
                messageSource.getMessage("member.registration.bytype.filter.activationdateto"), DateUtil.SEARCH_DATE_FORMAT, true));
        String activeStatusCode = MemberStatus.ACTIVE.toString();
        String activeStatusDesc = MemberStatus.ACTIVE.toString();
        Map<String, String> selectValues = Maps.newHashMap();
        selectValues.put("", "");
        selectValues.put(activeStatusCode, activeStatusDesc);
        filters.add(FilterField.createDropdownField(FILTER_STATUS, messageSource.getMessage("member.registration.bytype.filter.status"),
                selectValues));

        QLookupDetail lookupDetail = QLookupDetail.lookupDetail;
        BooleanExpression memberTypePredicate = lookupDetail.header.code.eq("MEM007");
        List<Tuple> tuples = new JPAQuery(em).from(lookupDetail).where(memberTypePredicate)
                .list(lookupDetail.code, lookupDetail.description);

        Map<String, String> memberTypeSelectValues = Maps.newHashMap();
        memberTypeSelectValues.put("", "");
        for (Tuple tuple : tuples) {
            memberTypeSelectValues.put(tuple.get(lookupDetail.code), tuple.get(lookupDetail.description));
        }

        filters.add(FilterField.createDropdownField(
                FILTER_MEMBER_TYPE,
                messageSource.getMessage("member.registration.bytype.filter.membertype"),
                memberTypeSelectValues));
        return filters;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean canView(Set<String> permissions) {
        if (permissions != null) {
            for (String permission : permissions) {
                if (permission.equalsIgnoreCase("REPORT_MEMBER_REGISTRATION_SUMMARY_VIEW")) {
                    return true;
                }
            }
        }
        return false;
    }

    private DateTime toDateTime(String dateString) {
        if (StringUtils.isNotBlank(dateString)) {
            return INPUT_DATE_PATTERN.parseDateTime(dateString);
        }
        return null;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public JRProcessor createJrProcessor(Map<String, String> map) {
        DateTime activationDateFrom = toDateTime(map.get(FILTER_ACTIVATIONDATE_FROM));
        DateTime activationDateTo = toDateTime(map.get(FILTER_ACTIVATIONDATE_TO));
        String statusCode = map.get(FILTER_STATUS);
        String filterMemberType = map.get(FILTER_MEMBER_TYPE);
        QMemberModel qMemberModel = QMemberModel.memberModel;

        BooleanExpression predicate = qMemberModel.isNotNull();

        if (StringUtils.isNotBlank(statusCode)) {
            predicate = predicate.and(qMemberModel.accountStatus.stringValue().eq(statusCode));
        }

        if (StringUtils.isNotBlank(filterMemberType)) {
            predicate = predicate.and(qMemberModel.memberType.code.stringValue().eq(filterMemberType));
        }
        StringBuilder activationDateRange = new StringBuilder(50);
        if (activationDateFrom != null && activationDateTo != null) {
            activationDateFrom = activationDateFrom.toLocalDate().toDateTimeAtStartOfDay();
            activationDateTo = activationDateTo.toLocalDate().plusDays(1).toDateTimeAtStartOfDay().minusMillis(1);
            predicate = predicate.and(qMemberModel.created.goe(activationDateFrom));
            predicate = predicate.and(qMemberModel.created.loe(activationDateTo));
            activationDateRange.append(FULL_DATE_PATTERN.print(activationDateFrom));
            activationDateRange.append(" to ");
            activationDateRange.append(FULL_DATE_PATTERN.print(activationDateTo));
        } else if (activationDateFrom != null) {
            activationDateFrom = activationDateFrom.toLocalDate().toDateTimeAtStartOfDay();
            predicate = predicate.and(qMemberModel.created.goe(activationDateFrom));
            activationDateRange.append(messageSource.getMessage("member.registration.bytype.report.label.activationdatefrom"));
            activationDateRange.append(" ");
            activationDateRange.append(FULL_DATE_PATTERN.print(activationDateFrom));
        } else if (activationDateTo != null) {
            activationDateTo = activationDateTo.toLocalDate().plusDays(1).toDateTimeAtStartOfDay().minusMillis(1);
            predicate = predicate.and(qMemberModel.created.loe(activationDateTo));
            activationDateRange.append(messageSource.getMessage("member.registration.bytype.report.label.activationdateto"));
            activationDateRange.append(" ");
            activationDateRange.append(FULL_DATE_PATTERN.print(activationDateTo));
        }

        Map<String, Object> parameters = Maps.newHashMap();
        parameters.put(PARAM_ACTIVATION_DATERANGE, activationDateRange.toString());
        
        QLookupDetail qMemberType = new QLookupDetail("memberType");

        List<Tuple> tuples = new JPAQuery(em).from(qMemberModel)
        		.leftJoin(qMemberModel.memberType, qMemberType)
                .groupBy(qMemberType.description, qMemberModel.registeredStore, qMemberModel.storeName, qMemberModel.memberCreatedFromType)
                .orderBy(qMemberType.description.asc(), qMemberModel.registeredStore.asc())
                .where(predicate)
                .list(qMemberType.description, qMemberModel.registeredStore, qMemberModel.storeName,
                        qMemberModel.memberCreatedFromType, qMemberModel.memberCreatedFromType.count());

        List<ReportBean> beansDataSource = Lists.newArrayList();
        if (!tuples.isEmpty()) {
            Map<String, ReportBean> reportBeanMap = Maps.newHashMap();
            for (Tuple tuple : tuples) {
                String memberType = tuple.get(qMemberType.description);
                String store = tuple.get(qMemberModel.registeredStore);
                String storeName = tuple.get(qMemberModel.storeName);
                try {
                    storeName = storeService.getStoreByCode(store).getName();
                } catch(NullPointerException npe) {

                }

                long registrationCount = 0;
                registrationCount = tuple.get(qMemberModel.memberCreatedFromType.count());

                long highestAll = 0;
                long highestTypes = 0;
                for(ReportBean beanMap : reportBeanMap.values()) {
                    if (beanMap.getMemberType().equalsIgnoreCase(PARAM_ALL_MEMBER_TYPES)) {
                        beanMap.incrementGrandTotal(registrationCount);
                        if (beanMap.getGrandTotal() > highestAll)
                            highestAll = beanMap.getGrandTotal();
                        else
                            beanMap.setGrandTotal(highestAll);
                    } else if (beanMap.getMemberType().equalsIgnoreCase(memberType)) {
                        beanMap.incrementGrandTotal(registrationCount);
                        if (beanMap.getGrandTotal() > highestTypes)
                            highestTypes = beanMap.getGrandTotal();
                        else
                            beanMap.setGrandTotal(highestTypes);
                    }
                }

                StringBuilder allTypesBuilder = new StringBuilder(100);
                allTypesBuilder.append(PARAM_ALL_MEMBER_TYPES);
                allTypesBuilder.append("-");
                allTypesBuilder.append(store);
                String keyAll = allTypesBuilder.toString();
                ReportBean beanAll = reportBeanMap.get(keyAll);
                if (StringUtils.isBlank(filterMemberType)) {
                    if (beanAll == null) {
                        beanAll = new ReportBean(PARAM_ALL_MEMBER_TYPES, store, storeName);
                        reportBeanMap.put(keyAll, beanAll);
                    }
                    beanAll.incrementRegistrationCount(registrationCount);
                    if (highestAll == 0)
                        beanAll.incrementGrandTotal(registrationCount);
                    else
                        beanAll.setGrandTotal(highestAll);
                }

                StringBuilder keyBuilder = new StringBuilder(100);
                keyBuilder.append(memberType);
                keyBuilder.append("-");
                keyBuilder.append(store);
                String key = keyBuilder.toString();

                ReportBean bean = reportBeanMap.get(key);
                if (bean == null) {
                    bean = new ReportBean(memberType, store, storeName);
                    reportBeanMap.put(key, bean);
                }
                bean.incrementRegistrationCount(registrationCount);
                if (highestTypes == 0)
                    bean.incrementGrandTotal(registrationCount);
                else
                    bean.setGrandTotal(highestTypes);

                MemberCreatedFromType memberCreatedFromTypeCode = tuple.get(qMemberModel.memberCreatedFromType);
                if (memberCreatedFromTypeCode!=null) {
                    if (memberCreatedFromTypeCode.equals(MemberCreatedFromType.FROM_CRM)) {
                        if (beanAll != null) {
                            beanAll.incrementCsRegCount(registrationCount);
                        }

                        bean.incrementCsRegCount(registrationCount);
		    } else if (memberCreatedFromTypeCode.equals(MemberCreatedFromType.FROM_UPLOAD)) {
                        if (beanAll != null) {
                            beanAll.incrementCsUploadCount(registrationCount);
                        }
                        bean.incrementCsUploadCount(registrationCount);
                    } else if (memberCreatedFromTypeCode.equals(MemberCreatedFromType.FROM_PORTAL)) {
                        if (beanAll != null) {
                            beanAll.incrementOnlineRegCount(registrationCount);
                        }
                        bean.incrementOnlineRegCount(registrationCount);
                    }
                }
            }
            beansDataSource.addAll(reportBeanMap.values());
//            Collections.sort(beansDataSource, new Comparator<ReportBean>() {
//                @Override
//                public int compare(ReportBean o1, ReportBean o2) {
//                    String store1 = StringUtils.defaultString(o1.getMemberType());
//                    String store2 = StringUtils.defaultString(o2.getMemberType());
//                    return store1.compareTo(store2);
//                }
//            });
            List<ReportBean> toRemove = new ArrayList<ReportBean>();
            for (ReportBean bean : beansDataSource) {
                if (null==bean.getStore() || bean.getStore().equals("")) {
                    toRemove.add(bean);
                }
            }
            beansDataSource.removeAll(toRemove);
        }

        if (beansDataSource.isEmpty()) {
            parameters.put(PARAM_IS_EMPTY, true);
            parameters.put(PARAM_NO_DATA_FOUND, "No data found");
            beansDataSource.add(new ReportBean());
        }

	ComparatorChain comparatorChain = new ComparatorChain(new Comparator<ReportBean>() {

	    @Override
	    public int compare(ReportBean o1, ReportBean o2) {
		return o1.getMemberType().compareTo(o2.getMemberType());
	    }
	});
	comparatorChain.addComparator(new Comparator<ReportBean>() {

	    @Override
	    public int compare(ReportBean o1, ReportBean o2) {
		return o1.getStore().compareTo(o2.getStore());
	    }
	});
	    
	Collections.sort(beansDataSource, comparatorChain);
        DefaultJRProcessor jrProcessor = new DefaultJRProcessor(REPORT_NAME, beansDataSource);
        jrProcessor.addParameters(parameters);
        jrProcessor.setFileResolver(new ClasspathFileResolver("reports"));
        return jrProcessor;
    }

    @Override
    public DateTimeFormatter getDefaultDateFormat() {
	return INPUT_DATE_PATTERN;
    }
    
    @Override
    public Set<ReportsCustomizer.Option> customizerOption() {
	return Sets.newHashSet(ReportsCustomizer.Option.DATE_RANGE_FULL_VALIDATION);
    }

    public static class ReportBean {
        private String memberType;
        private String store;
        private String storeName;
        private long registrationCount;
        private long csRegCount;
        private long csUploadCount;
        private long onlineRegCount;
        private long grandTotal;

        public ReportBean() {

        }

        public ReportBean(String memberType, String store, String storeName) {
            this.memberType = memberType;
            this.store = store;
            this.storeName = storeName;
        }

        public String getStore() {
            return store;
        }

        public void setStore(String store) {
            this.store = store;
        }

        public String getMemberType() {
            return memberType;
        }

        public void setMemberType(String memberType) {
            this.memberType = memberType;
        }

        public String getStoreName() {
            return storeName;
        }

        public void setStoreName(String storeName) {
            this.storeName = storeName;
        }

        public long getRegistrationCount() {
            return registrationCount;
        }

        public void setRegistrationCount(long registrationCount) {
            this.registrationCount = registrationCount;
        }

        public long getCsRegCount() {
            return csRegCount;
        }

        public void setCsRegCount(long csRegCount) {
            this.csRegCount = csRegCount;
        }

        public long getOnlineRegCount() {
            return onlineRegCount;
        }

	public long getCsUploadCount() {
	    return csUploadCount;
	}

        public void setOnlineRegCount(long onlineRegCount) {
            this.onlineRegCount = onlineRegCount;
        }

        public long getGrandTotal() {
            return grandTotal;
        }

        public void setGrandTotal(long grandTotal) {
            this.grandTotal = grandTotal;
        }

        public void incrementRegistrationCount(long incrementBy) {
            registrationCount += incrementBy;
        }

        public void incrementCsRegCount(long incrementBy) {
            csRegCount += incrementBy;
        }
	
        public void incrementCsUploadCount(long incrementBy) {
            csUploadCount += incrementBy;
        }

        public void incrementOnlineRegCount(long incrementBy) {
            onlineRegCount += incrementBy;
        }

        public void incrementGrandTotal(long incrementBy) {
            grandTotal += incrementBy;
        }
    }

}
