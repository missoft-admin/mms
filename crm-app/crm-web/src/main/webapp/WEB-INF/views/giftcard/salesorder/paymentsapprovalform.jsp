<%@ include file="../../common/taglibs.jsp" %>

<style type="text/css">
<!--

  #productTable tr td:last-child {
    border-left: 0px;
  }

  #addProductBtn.btn-small {
    margin-bottom: 5px;
  }
  table select, textarea, table input[type="text"], input[type="password"], input[type="datetime"], input[type="datetime-local"], input[type="date"], input[type="month"], input[type="time"], input[type="week"], input[type="number"], input[type="email"], input[type="url"], input[type="search"], input[type="tel"], input[type="color"], .uneditable-input {
      margin-bottom: 0px;
      margin-top: 0px;
  }
  
-->
</style>



<c:set var="ENCTYPE" value="application/x-www-form-urlencoded"/>

<div class="modal-dialog">
  <div class="modal-content">
    
    <div class="modal-header">
      <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
      <h4><spring:message code="sales_order_so_payment_info" /></h4>
    </div>
 
    <div class="modal-body">
    
      <div id="contentError" class="hide alert alert-error">
        <button type="button" class="close" onclick="$(this).parent().hide();">&times;</button>
        <div><form:errors path="*"/></div>
      </div>
    
      
      <form:form id="paymentForm" name="paymentForm" modelAttribute="paymentForm" action="${pageContext.request.contextPath}/giftcard/salesorder/payments/save/" method="POST" enctype="${ENCTYPE}" cssClass="form-reset form-horizontal" >
      <form:hidden path="id"/>
      <form:hidden path="orderId"/>
      
      <div class="contentMain">
      
      <div class="row-fluid">
      
        <div class="span6">
        <div class="control-group">
          <spring:message code="sales_order_no" var="orderNum" />
          <label for="orderNo" class="control-label">${orderNum}</label> 
          <div class="controls">
            <form:input path="orderNo" class="form-control" placeholder="${orderNum}" readonly="true" />
          </div>
        </div>
        
        
        <div class="control-group">
          <spring:message code="sales_order_date" var="orderDt" />
          <label for="orderDate" class="control-label">${orderDt}</label> 
          <div class="controls">
            <form:input path="orderDate" class="form-control" placeholder="${orderDt}" readonly="true" />
          </div>
        </div>
        </div>
        
        <div class="span6">
        
        <div class="control-group">
          <spring:message code="sales_order_contact_person" var="contactPerson" />
          <label for="customerId" class="control-label">${contactPerson}</label> 
          <div class="controls">
            <form:input path="customerName" class="form-control" placeholder="${contactPerson}" readonly="true" />
          </div>
        </div>
        
        
        </div>
        
        <div class="clearfix"></div>
        
        
        <div class="span6">
        
        <div class="control-group">
          <spring:message code="sales_order_payment_date" var="paymentDate" />
          <label for="paymentDate" class="control-label">${paymentDate}</label> 
          <div class="controls">
            <form:input path="paymentDate" class="form-control" placeholder="${paymentDate}" readonly="true" />
          </div>
        </div>
        
        <div class="control-group">
          <spring:message code="sales_order_payment_type" var="paymentType" />
          <label for="paymentDate" class="control-label">${paymentType}</label> 
          <div class="controls">
            <input type="text" class="form-control" placeholder="${paymentType}" readonly="readonly" value="${paymentForm.paymentType.code} - ${paymentForm.paymentType.description}" />
          </div>
        </div>
        
        </div>
        
        <div class="span6">
        
        <div class="control-group">
          <spring:message code="sales_order_payment_amount" var="paymentAmount" />
          <label for="paymentAmount" class="control-label">${paymentAmount}</label> 
          <div class="controls">
            <form:input path="paymentAmount" class="form-control" placeholder="${paymentAmount}" readonly="true" />
          </div>
        </div>
        
        <div class="control-group">
          <spring:message code="sales_order_payment_details" var="paymentDetails" />
          <label for="paymentDetails" class="control-label">${paymentDetails}</label> 
          <div class="controls">
            <form:textarea path="paymentDetails" class="form-control" placeholder="${paymentDetails}" readonly="true" />
          </div>
        </div>
        
        </div>
        
        </div>
        
        
      </div>
          
      </form:form>
    
    </div>
    <div class="modal-footer">
      <button type="button" class="orderSubmit btn btn-primary" data-status='APPROVED'><spring:message code="label_approve" /></button>
      <button type="button" class="orderSubmit btn btn-primary" data-status='REJECTED'><spring:message code="label_reject" /></button>
      <button type="button" id="orderCancel" class="btn btn-default" data-dismiss="modal"><spring:message code="label_cancel"/></button>
    </div>
  </div>
</div>

<script>
$(document).ready(function() {
  
  var CONTAINER_ERROR = "#contentError > div";
  var CONTENT_ERROR = "#contentError";
  $form = $("#paymentForm");
  $orderDialog = $("#orderDialog").css({"width": "900px"});
  
  $(".orderSubmit").click(submitForm);
  
  function submitForm() {
    $(".orderSubmit").prop( "disabled", true );
    $.post($form.attr("action") + $(this).data("status"), $form.serialize(), function(data) {
      $(".orderSubmit").prop( "disabled", false );
      if (data.success) {
        location.reload();
      } 
      else {
        errorInfo = "";
        for (i = 0; i < data.result.length; i++) {
          errorInfo += "<br>" + (i + 1) + ". "
              + ( data.result[i].code != undefined ? 
                  data.result[i].code : data.result[i]);
        }
        $orderDialog.find(CONTAINER_ERROR).html("Please correct following errors: " + errorInfo);
        $orderDialog.find(CONTENT_ERROR).show('slow');
      }
    }); 
  }	
  	
  
});
</script>