package com.transretail.crm.core.entity.enums;

public enum GenderType {

    MALE, FEMALE

}
