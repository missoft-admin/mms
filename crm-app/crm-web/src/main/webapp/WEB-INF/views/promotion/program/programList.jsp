<%@ include file="../../common/taglibs.jsp" %>


<div id="programList" class="contentDialog">

  <div id="content_programList">
    <c:forEach var="program" items="${programs}">
      <a><c:out value="${program.name}" /></a>
    </c:forEach>

    <c:if test="${programs == null or empty programs}"><spring:message code="program_msg_empty"/></c:if>
  </div>

  <a href="javascript: createProgram( '<c:url value="/promotion/program/create"/>' )"><spring:message code="program_label_create"/></a>
  <div id="form_promotionProgram"></div>

</div>

<link href="<spring:url value="/css/dataTables.css" />" rel="stylesheet">
<script src="<spring:url value="/js/jquery/jquery.dataTables.js" />"></script>
<script src="<spring:url value="/js/jquery/jquery.dataTables.linkLengthMenu.js" />"></script>
<script src="<spring:url value="/js/jquery/jquery.dataTables.customPaging.js" />"></script>
<script src="<spring:url value="/js/jquery/ajax-datatable.js" />"></script>
<script src="<spring:url value="/js/common/form2js.js" />"></script>
<script src="<spring:url value="/js/common/json2.js" />"></script>
<script src="<spring:url value="/js/jquery/jquery.toObject.js" />"></script>

<script type="text/javascript">
  $(document).ready( function() {

    var theHeaders = { 
  		  name: "<spring:message code='program_prop_name'/>",
        description: "<spring:message code='program_prop_description'/>",
        duration: "<spring:message code='program_prop_duration'/>" };

    var theProgramTable = initializeProgramTable( '${contextPath}promotion/program/list', theHeaders );
    initializeProgramSearch( theProgramTable );
  });
</script>
<script src='<c:url value="/js/viewspecific/promotion/program/programList.js"/>' type='text/javascript'><![CDATA[&nbsp; ]]></script>