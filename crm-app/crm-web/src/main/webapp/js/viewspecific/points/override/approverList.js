$(document).ready( function() {

	var $theList = $( "#list_approver" );



	initApproverDataTable();



	function initApproverDataTable() {

		var theHeaders = {
			fname: $theList.data( "hdr-fname" ),
			lname: $theList.data( "hdr-lname" ),
			username: $theList.data( "hdr-username" ),
			amount	: $theList.data( "hdr-amount" ),
			edit	: $theList.data( "link-edit" ),
			_delete	: $theList.data( "link-delete" )
		};

		$theList.ajaxDataTable({
			'autoload' 		: true,
			'ajaxSource' 	: $theList.data( "url" ),
			'columnHeaders' : [
			   	theHeaders['username'],
			   	theHeaders['fname'],
			 	theHeaders['lname'],
			 	theHeaders['amount'],
			 	{ text: "", className : "el-width-x" }
			],
			'modelFields' 	: [
			   	{ name 	: 'user.username', sortable : true },
			   	{ name 	: 'user.firstName', sortable : true },
			   	{ name 	: 'user.lastName', sortable : true },
			 	{ name 	: 'amount', sortable : true },
		        { customCell : function ( innerData, sSpecific, json ) {
			 			if ( sSpecific == 'display' ) {
			 				return $( "#btn_editApprover" ).html().replace( new RegExp("%ID%", 'g'), json.id )
			 					+ $( "#btn_deleteApprover" ).html().replace( new RegExp("%ID%", 'g'), json.id );
				        } 
			 			else {
				              return '';
				        }
			 		}
		        }
			 ]
		})
		.on( "click", ".btn_editApprover", edit )
		.on( "click", ".btn_deleteApprover", process );
	}

	function initApproverSearchForm( inDataTable ) {
		var $theApproverSearchForm = $( "#approverSearchForm" );
		var $theApproverSearchBtn = $( "#approverSearchButton" );
		$theApproverSearchBtn.click( function() {
			var $theFormDataObj = $theApproverSearchForm.toObject( { mode : 'first', skipEmpty : true } );
			/*if ( $theFormDataObj.username == undefined ) { return true; }*/
			var $btnSearch = $( this );
			$btnSearch.prop( 'disabled', true );
			inDataTable.ajaxDataTable( 
				'search', 
				$theFormDataObj, 
				function () {
					$btnSearch.prop( 'disabled', false );
			});
		});
	}



	function edit() {
		$.ajax({
			cache 		: false, 
			dataType 	: "html",
			type 		: "GET", 
			url 		: $( "#btn_editApprover" ).data( "url" ) + $( this ).data( "id" ),
			error 		: function(jqXHR, textStatus, errorThrown) {}, 
			success 	: function( inResponse ) {
				$( "#form_approver" ).html( inResponse );
			}
		});
	}

	function process() {
		var deleteUrl = $( "#btn_deleteApprover" ).data( "url" ) + $( this ).data( "id" );
		getConfirm( $( "#btn_deleteApprover" ).data( "msg-confirm" ), function( result ) {
			if( result ) {
				$.post( 
						deleteUrl,
						function( inResponse, textStatus, jqXHR ) { $( "#list_approver" ).ajaxDataTable( "search" ); },
						"json" );
			}
		});
	}
	
});