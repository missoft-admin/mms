<%@ include file="../../common/taglibs.jsp"%>


<div class="clearfix"></div>

<div class="pull-left span11">
  <jsp:include page="segments/informedAbout.jsp" />
  <jsp:include page="segments/visitedSupermarkets.jsp" />


  <!-- Grocery Trips To Carrefour -->
  <fieldset class="form-horizontal container-fluid">
    <legend><spring:message code="label_member_indi_tripsto" /></legend>

    <div id="" class="controls">
      <form:input path="marketingDetails.tripsToStore" cssClass="wholeInput" />
    </div>
  </fieldset>
  <div class="clearfix"></div>
  <br />


  <jsp:include page="segments/interestedProducts.jsp" />
  <jsp:include page="segments/interestedActivities.jsp" />
</div>

<div class="clearfix"></div>