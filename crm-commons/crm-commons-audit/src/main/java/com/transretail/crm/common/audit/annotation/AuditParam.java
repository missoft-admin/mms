package com.transretail.crm.common.audit.annotation;

import static java.lang.annotation.ElementType.PARAMETER;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * @author Allan G. Ramirez (agramirez@exist.com)
 */
@Target(PARAMETER)
@Retention(RetentionPolicy.RUNTIME)
public @interface AuditParam {
    String value();
}
