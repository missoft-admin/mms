package com.transretail.crm.core.entity;

import javax.persistence.*;
import org.hibernate.annotations.Immutable;

@Table(name = "POS_PAYMENT")
@Entity
@Immutable
public class PosPayment {

    @Id
    private String id;

    @ManyToOne(targetEntity = PosTransaction.class, cascade = CascadeType.ALL)
    @JoinColumn(name = "POS_TXN_ID")
    private PosTransaction posTransaction;

    @Column(name = "MEDIA_TYPE")
    private String mediaType;

    @Column(name = "AMOUNT")
    private double amount;
    
    @OneToOne(mappedBy = "posPayment")
    private PosEftPayment eftPayment;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public PosTransaction getPosTransaction() {
        return posTransaction;
    }

    public void setPosTransaction(PosTransaction posTransaction) {
        this.posTransaction = posTransaction;
    }

    public String getMediaType() {
        return mediaType;
    }

    public void setMediaType(String mediaType) {
        this.mediaType = mediaType;
    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

    public PosEftPayment getEftPayment() {
        return eftPayment;
    }

    public void setEftPayment(PosEftPayment eftPayment) {
        this.eftPayment = eftPayment;
    }
}
