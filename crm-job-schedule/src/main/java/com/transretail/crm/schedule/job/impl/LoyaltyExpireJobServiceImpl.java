package com.transretail.crm.schedule.job.impl;

import java.util.Date;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections.CollectionUtils;
import org.joda.time.LocalDate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Service;

import com.transretail.crm.core.dto.MessageDto;
import com.transretail.crm.loyalty.repo.LoyaltyCardInventoryRepo;
import com.transretail.crm.media.service.MailService;
import com.transretail.crm.media.service.SmsService;
import com.transretail.crm.schedule.job.service.LoyaltyExpireJobService;

@Service("loyaltyExpireJobSvc")
public class LoyaltyExpireJobServiceImpl implements LoyaltyExpireJobService {
	
	@Autowired
	LoyaltyCardInventoryRepo inventoryRepo;
	@Autowired
	MailService mailService;
	@Autowired
	SmsService smsService;
	@Autowired
	MessageSource messageSource;
	
	private static final Logger LOGGER = LoggerFactory.getLogger(LoyaltyExpireJobServiceImpl.class);
	private final int MOS_BEFORE_EXPIRE = 2;  //TODO configurable? 2 months before expiration
	
	
	private String EXPIRY_NOTIFICATION_EMAIL;
	private String EXPIRED_NOTIFICATION_EMAIL;
	
	@Override
	public void notifyAndExpire() {
		LOGGER.error( ":notifyAndExpire:: job executing at " + new Date().toString()  );
		
		EXPIRY_NOTIFICATION_EMAIL = messageSource.getMessage("loyalty_exp_notification_toexpire", new Object[]{MOS_BEFORE_EXPIRE}, LocaleContextHolder.getLocale());
		EXPIRED_NOTIFICATION_EMAIL = messageSource.getMessage("loyalty_exp_notification_hasexpired", null, LocaleContextHolder.getLocale());
		
		sendNotificationForExpiration();
		expireInventories();

		LOGGER.error( ":notifyAndExpire:: job successfully executed at " + new Date().toString()  );
	}
	
	private void sendNotificationForExpiration() {
		LocalDate expiryDate = new LocalDate().plusMonths(MOS_BEFORE_EXPIRE);
		sendMessages(inventoryRepo.getEmailsWithExpiry(expiryDate), EXPIRY_NOTIFICATION_EMAIL);
	}
	
	private void expireInventories() {
		LocalDate expiryDate = new LocalDate();
		Map<String, List<String>> contacts = inventoryRepo.getEmailsWithExpiry(expiryDate);
		inventoryRepo.expireInventories(expiryDate);
		sendMessages(contacts, EXPIRED_NOTIFICATION_EMAIL);
	}
	
	private void sendMessages(Map<String, List<String>> contacts, String message) {
		sendExpiryEmail(contacts.get("email"), message);
		sendSms(contacts.get("sms"), message);
	}
	
	private boolean sendExpiryEmail(List<String> recipients, String message) {
		if(CollectionUtils.isNotEmpty(recipients)) {
			MessageDto dto = new MessageDto();
	    	dto.setMessage(message);
	    	dto.setSubject(messageSource.getMessage("loyalty_exp", null, LocaleContextHolder.getLocale()));
	    	dto.setRecipients(recipients.toArray(new String[recipients.size()]));
	    	
	    	return mailService.send(dto);
		}
		return false;
	}
	
	private boolean sendSms(List<String> recipients, String message) {
		if(CollectionUtils.isNotEmpty(recipients)) {
			MessageDto dto = new MessageDto();
	    	dto.setMessage(message);
	    	dto.setRecipients(recipients.toArray(new String[recipients.size()]));
	    	return smsService.send(dto);
		}
		return false;
	}
	
}
