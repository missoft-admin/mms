package com.transretail.crm.loyalty.service;

import java.util.List;

import org.hibernate.StaleObjectStateException;

import com.transretail.crm.common.reporting.jasper.JRProcessor;
import com.transretail.crm.common.service.dto.request.PageSortDto;
import com.transretail.crm.common.service.dto.response.NameIdPairResultList;
import com.transretail.crm.common.service.dto.response.ResultList;
import com.transretail.crm.core.entity.lookup.LookupDetail;
import com.transretail.crm.loyalty.dto.ManufactureOrderDto;
import com.transretail.crm.loyalty.dto.ManufactureOrderSearchDto;
import com.transretail.crm.loyalty.dto.ManufactureOrderTierDto;
import com.transretail.crm.loyalty.dto.ReceiveOrderDto;

public interface ManufactureOrderService {

    void saveMoDto(ManufactureOrderDto manufactureOrderDto) throws StaleObjectStateException;

	ManufactureOrderDto findDtoById(Long id);

	List<ManufactureOrderDto> findAllDtos();

    void updateMoDto(ManufactureOrderDto manufactureOrderDto) throws StaleObjectStateException;

	ManufactureOrderDto findDtoByMoNo(String moNo);

	ManufactureOrderDto findDuplicateMoNo(ManufactureOrderDto orderDto);

    String findDuplicateTier(ManufactureOrderTierDto tier);

	void deleteMo(Long id);

	List<ManufactureOrderDto> findAllDtosForApprover();

	boolean isApprover();

	NameIdPairResultList getVendors();

	ResultList<ManufactureOrderDto> listManufactureOrderDto(ManufactureOrderSearchDto dto);

	void populateReceiveCount(ManufactureOrderTierDto product);

	JRProcessor createJrProcessor(ManufactureOrderDto orderDto);

	JRProcessor createJrProcessor(ReceiveOrderDto receiveDto);

	JRProcessor createJrProcessorReceiveAll(ManufactureOrderDto orderDto);

	void updateMoStatus(Long moId, LookupDetail status);

}
