<%@ include file="../../common/taglibs.jsp" %>


<div class="clearfix"></div>
<div id="content_productGroupView" class="alert alert-info">
   <style type="text/css"><!--.cont-02 { width: 60% !important; } .cont-02 div { margin-top: 5px !important; }--></style>
  <button type="button" class="close" onclick="$(this).parent().hide();">&times;</button>

  <div id="form_view" class="form-horizontal control-group-01">
    <div class="control-group">
      <label for="store" class="bold control-label"><spring:message code='label_group_groupname'/></label>
      <label for="store" class="control-label data-content-01 control-data">
        <c:out value="${productGroup.name}" />
      </label>
    </div>

    <c:if test="${not empty productGroup.itemCodeDesc}">
    <div class="control-group">
      <label for="store" class="bold control-label"><spring:message code="group_product_andcfns"/></label>
      <label for="store" class="control-label data-content-01 control-data">
        <c:forEach var="item" items="${productGroup.itemCodeDesc}"><div><c:out value="${item.code} - ${item.description}" /></div></c:forEach>
      </label>
    </div>
    </c:if>

    <c:if test="${not empty productGroup.excProductCodeDesc}">
    <div class="control-group">
      <label for="store" class="bold control-label"><spring:message code="group_product_excluded"/></label>
      <label for="store" class="control-label data-content-01 control-data">
        <c:forEach var="prd" items="${productGroup.excProductCodeDesc}"><div><c:out value="${prd.code} - ${prd.description}" /></div></c:forEach>
      </label>
    </div>
    </c:if>

  </div>
</div>