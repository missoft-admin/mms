package com.transretail.crm.core.repo.custom;

import java.util.List;

import com.transretail.crm.core.entity.ApprovalMatrixModel;
import com.transretail.crm.core.entity.UserModel;
import com.transretail.crm.core.entity.enums.ModelType;


public interface ApprovalMatrixRepoCustom {

	Double findHighestAmountByUser( ModelType inModelType, UserModel inUser );

	Long findHighestPointsAmountByUser(UserModel inUser);
	
	List<ApprovalMatrixModel> findByUsername(String username);

	List<UserModel> findUsers(ModelType inModelType);

}
