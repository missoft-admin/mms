package com.transretail.crm.core.dto;

import java.util.Date;

import com.transretail.crm.core.entity.EmployeeVoucherTxnModel;
import com.transretail.crm.core.entity.enums.TxnStatus;
import com.transretail.crm.core.entity.enums.VoucherTransactionType;

public class EmployeeVoucherTxnDto {
//	private static final String[] IGNORE = {"employeeVoucher", "employeeId"};
	
	Long id;
	String transactionNo;
	Date transactionDateTime;
	Double transactionAmount;
	String store;
	String comment;
	String employeeId;
	VoucherTransactionType voucherTransactionType;
	TxnStatus status;
	
	public EmployeeVoucherTxnDto() {
		
	}
	
	public EmployeeVoucherTxnDto(EmployeeVoucherTxnModel model) {
        this.id = model.getId();
        this.transactionNo = model.getTransactionNo();
        this.transactionDateTime = model.getTransactionDateTime();
        this.transactionAmount = model.getTransactionAmount();
        this.store = model.getStore();
        this.comment = model.getComment();
        this.status = model.getStatus();
//		BeanUtils.copyProperties(model, this, IGNORE);
	}

	public String getTransactionNo() {
		return transactionNo;
	}
	public void setTransactionNo(String transactionNo) {
		this.transactionNo = transactionNo;
	}
	public Date getTransactionDateTime() {
		return transactionDateTime;
	}
	public void setTransactionDateTime(Date transactionDateTime) {
		this.transactionDateTime = transactionDateTime;
	}
	public Double getTransactionAmount() {
		return transactionAmount;
	}
	public void setTransactionAmount(Double transactionAmount) {
		this.transactionAmount = transactionAmount;
	}
	public String getStore() {
		return store;
	}
	public void setStore(String store) {
		this.store = store;
	}
	public VoucherTransactionType getVoucherTransactionType() {
		return voucherTransactionType;
	}
	public void setVoucherTransactionType(
			VoucherTransactionType voucherTransactionType) {
		this.voucherTransactionType = voucherTransactionType;
	}

	public TxnStatus getStatus() {
		return status;
	}

	public void setStatus(TxnStatus status) {
		this.status = status;
	}

	public String getComment() {
		return comment;
	}

	public void setComment(String comment) {
		this.comment = comment;
	}

	public String getEmployeeId() {
		return employeeId;
	}

	public void setEmployeeId(String employeeId) {
		this.employeeId = employeeId;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}
}
