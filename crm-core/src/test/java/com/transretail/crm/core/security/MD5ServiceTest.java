package com.transretail.crm.core.security;

import com.transretail.crm.core.entity.ApplicationConfig;
import com.transretail.crm.core.entity.enums.AppConfigDefaults;
import com.transretail.crm.core.entity.enums.AppKey;
import com.transretail.crm.core.repo.ApplicationConfigRepo;
import com.transretail.crm.core.security.service.MD5Service;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Configurable;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

@Configurable
@ContextConfiguration(locations = "classpath*:/META-INF/spring/applicationContext*.xml")
@Transactional
@RunWith(SpringJUnit4ClassRunner.class)
public class MD5ServiceTest {


    @Autowired
    MD5Service mD5Service;

    @Autowired
    ApplicationConfigRepo applicationConfigRepo;

    @Test
    public void createMD5Hash() {



        String hash = "098f6bcd4621d373cade4e832627b4f6";   //hash result for text "test"

        String signature = "test";

        String resultingHash = mD5Service.createMD5Hash(signature);

        Assert.assertNotNull(resultingHash);

        Assert.assertEquals(hash, resultingHash);



    }

    @Test
    public void validateMd5() {

        ApplicationConfig theConfig = new ApplicationConfig();
        theConfig.setKey(AppKey.MD5_SECRET_KEY);
        theConfig.setValue(AppConfigDefaults.DEFAULT_MD5_SECRET_KEY);
        applicationConfigRepo.save(theConfig);


        ApplicationConfig appConfig = applicationConfigRepo.findByKey(AppKey.MD5_SECRET_KEY);


        String secretKey = appConfig.getValue();

        Assert.assertNotNull(secretKey);

        String signature = "test";

        String resultingHash  = mD5Service.createMD5Hash(signature + secretKey);





        Assert.assertTrue(mD5Service.validateMd5(signature, resultingHash));

    }


}
