package com.transretail.crm.web.controller.util;

import java.util.Iterator;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import com.google.common.collect.Lists;
import com.mysema.query.types.expr.BooleanExpression;
import com.transretail.crm.core.service.CodePropertiesService;
import com.transretail.crm.loyalty.dto.ProductSeriesDto;
import com.transretail.crm.loyalty.dto.ReceiveOrderDto;
import com.transretail.crm.loyalty.entity.QLoyaltyCardInventory;
import com.transretail.crm.loyalty.repo.LoyaltyCardInventoryRepo;
import com.transretail.crm.loyalty.service.ManufactureOrderService;

@Component
public class ReceiveOrdersValidator implements Validator {
	
	@Autowired
	ManufactureOrderService manufactureOrderService;
	@Autowired
	CodePropertiesService codePropertiesService;
	@Autowired
	LoyaltyCardInventoryRepo loyaltyCardInventoryRepo;
	
	@Autowired
	MessageSource messageSource;

	@Override
	public boolean supports(Class<?> clazz) {
		return ReceiveOrderDto.class.isAssignableFrom(clazz);
	}

	@Override
	public void validate(Object target, Errors errors) {
		validateMo(errors, (ReceiveOrderDto) target);
	}
	
	private void validateMo(Errors errors, ReceiveOrderDto orderDto) {
		if(orderDto.getReceivedAt() == null)
			errors.reject(getMessage("propkey_msg_notempty", new Object[] {"loyalty_receive_receivedat"}));
		
		if(orderDto.getReceiveDate() == null)
			errors.reject(getMessage("propkey_msg_notempty", new Object[] {"loyalty_receive_receivedate"}));
		
		if(StringUtils.isBlank(orderDto.getReceiptNo()))
			errors.reject(getMessage("propkey_msg_notempty", new Object[] {"loyalty_receive_receipt"}));
		
		if(CollectionUtils.isNotEmpty(orderDto.getProducts())) {
			validateProducts(errors, orderDto.getProducts());
		}
	}
	
	private void validateProducts(Errors errors, List<ProductSeriesDto> products) {
		Iterator<ProductSeriesDto> iter = products.iterator();
		while (iter.hasNext()) {
			ProductSeriesDto dto = iter.next();
			if(dto.getName() == null && StringUtils.isBlank(dto.getStartingSeries()) && 
					StringUtils.isBlank(dto.getEndingSeries()) && dto.getQuantity() == null && StringUtils.isBlank(dto.getBoxNo()))
				iter.remove();
		}
		
		for(ProductSeriesDto product : products) {
			if(product != null && validateProduct(errors, product)) {
				compareProduct(errors, product);
			}
		}
	}
	
	private void compareProduct(Errors errors, ProductSeriesDto product) {
		if(!validateProduct(product))
			errors.reject(getMessage("loyalty_inv_invalid_series", null));
	}
	
	private boolean validateProduct(ProductSeriesDto product) {
		QLoyaltyCardInventory qInventory = QLoyaltyCardInventory.loyaltyCardInventory;
		List<BooleanExpression> expressions = Lists.newArrayList();
		
		expressions.add(qInventory.product.name.eq(product.getName()));
		expressions.add(qInventory.status.code.isNull());
		
		BooleanExpression filter = BooleanExpression.allOf(expressions.toArray(new BooleanExpression[expressions.size()]));
		
		if(loyaltyCardInventoryRepo.count(filter.and(qInventory.sequence.eq(product.getStartingSeries()))) == 0 ||
				loyaltyCardInventoryRepo.count(filter.and(qInventory.sequence.eq(product.getEndingSeries()))) == 0) {
			return false;
		}
		
		Long starting = Long.valueOf(product.getStartingSeries());
		Long ending = Long.valueOf(product.getEndingSeries());
		Long qty = ending - starting + 1;
		long count = loyaltyCardInventoryRepo.count(filter.and(qInventory.sequence.castToNum(Long.class).between(starting, ending)));
		if(count != qty) {
			return false;
		}
		return true;
	}
	 
	
	private boolean validateProduct(Errors errors, ProductSeriesDto product) {
		boolean isValid = true;

		if(product.getName() == null || StringUtils.isBlank(product.getStartingSeries()) || StringUtils.isBlank(product.getEndingSeries())) {
			errors.reject(getMessage("propkey_msg_notempty", new Object[] {"mo_tier_prop_name"}));
			isValid = false;
		} else {
			if (!StringUtils.isNumeric(product.getStartingSeries())) {
				errors.reject(getMessage("propkey_msg_numeric", new Object[] {"mo_tier_prop_startingseries"}));
				isValid = false;
			}
			if(!StringUtils.isNumeric(product.getEndingSeries())) {
				errors.reject(getMessage("propkey_msg_numeric", new Object[] {"mo_tier_prop_endingseries"}));
				isValid = false;
			}
			if(product.getStartingSeries().length() != 12 || product.getEndingSeries().length() != 12) {
				errors.reject(getMessage("loyalty_inv_invalid_series_length", null));
				isValid = false;
			}
		}
		
		return isValid;
	}

	
	
	private String getMessage(String inMsgCode, Object[] inArgMsgCodes) {
		if(inArgMsgCodes != null) {
			for ( int i=0; i < inArgMsgCodes.length; i++ ) {
				inArgMsgCodes[i] = messageSource.getMessage( inArgMsgCodes[i].toString().toLowerCase(), null, LocaleContextHolder.getLocale() );
			}
		}
		return messageSource.getMessage(inMsgCode, inArgMsgCodes, "Info given is invalid.", LocaleContextHolder.getLocale());
	}

}
