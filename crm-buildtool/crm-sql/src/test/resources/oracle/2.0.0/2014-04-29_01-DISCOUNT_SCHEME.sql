create table CRM_GC_DISCOUNT_SCHEME (
        id number(19,0) not null,
        CREATED_BY varchar2(255 char),
        CREATED_DATETIME timestamp,
        LAST_UPDATED_BY varchar2(255 char),
        LAST_UPDATED_DATETIME timestamp,
        END_YEAR number(10,0),
        ENDING_DISCOUNT number(19,2),
        MIN_PURCHASE number(19,2),
        PURCHASE_FOR_MAX_DISC number(19,2),
        START_YEAR number(10,0),
        STARTING_DISCOUNT number(19,2),
        CUSTOMER number(19,0),
        primary key (id)
    );

alter table CRM_GC_DISCOUNT_SCHEME 
        add constraint FK_ctje9607xil0khp0ia8ftj6o9 
        foreign key (CUSTOMER) 
        references CRM_GC_CX_PROFILE;
        
        ALTER TABLE CRM_GC_CX_PROFILE ADD TOTAL_DISC_PURCHASE number(19,2);