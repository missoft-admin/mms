package com.transretail.crm.report.template.impl.memberactivity;

import com.google.common.collect.Lists;
import com.transretail.crm.common.reporting.exception.ReportException;
import com.transretail.crm.core.entity.enums.ReportType;
import com.transretail.crm.report.template.dynamicreports.Templates;
import net.sf.dynamicreports.report.exception.DRException;
import net.sf.jasperreports.engine.JasperPrint;

import javax.sql.DataSource;
import java.util.List;

/**
 * @author Allan G. Ramirez (agramirez@exist.com)
 */
public class WeeklyJrProcessor extends AbstractMemberActivityReport {
    public WeeklyJrProcessor(String reportFileName, String title, String subTitle, DataSource dataSource, ReportType reportType) {
        super(reportFileName, title, subTitle, dataSource, reportType);
    }

    @Override
    public List<JasperPrint> getJasperPrints() throws ReportException {
        List<JasperPrint> jasperPrints = Lists.newArrayList();
        jasperPrints.add(createMemberListReport());
        return jasperPrints;
    }

    private JasperPrint createMemberListReport() throws ReportException {
        try {
            if (dateFrom.toString("MMMM").toUpperCase().equals(dateTo.toString("MMMM").toUpperCase())) {
                this.title = this.title + "\n" + dateFrom.toString("MMMM").toUpperCase();
            } else {
                this.title = this.title + "\n" + dateFrom.toString("MMMM").toUpperCase() + " to " + dateTo.toString("MMMM").toUpperCase();
            }
            return createJRBuilderWithTitleAndSubTitle()
                .setPageMargin(Templates.DEFAULT_MARGIN)
                .addColumn(createColumn("Account ID", "accountId", String.class, 100))
                .addColumn(createColumn("Member Name", "memberName", String.class, 200))
                .addColumn(createColumn("Business Name", "businessName", String.class, 200))
                .addColumn(createColumn("Business Segmentation", "segmentation", String.class, 150))
                .addColumn(createColumn("Total Sales", "totalSales", Long.class, 100))
                .addColumn(createColumn("Total Kena Pajak", "totalSalesAfterVat", Long.class, 100))
                .addColumn(createColumn("PPN", "ppn", Long.class, 100))
                .addColumn(createColumn("Total Mark up", "totalMarkup", Long.class, 100))
                .addColumn(createColumn("PPN Mark up", "ppnMarkup", Long.class, 100))
                .addColumn(createColumn("Freq Visit", "freqVisit", Long.class, 100))
                .addColumn(createColumn("Registration Date", "registrationDate", String.class, 150))
                .addColumn(createColumn("Last Visit Date", "lastVisitDate", String.class, 150))
                .setDataSource(createJrDataSource())
                .toJasperPrint();
        } catch (DRException e) {
            // Close the connection
            getAfterPageExportListener().afterPageExport();
            throw new ReportException(e);
        }
    }
}
