package com.transretail.crm.core.entity.lookup;

import java.io.Serializable;
import java.util.Arrays;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.apache.commons.lang3.StringUtils;
import org.hibernate.envers.Audited;
import org.hibernate.envers.NotAudited;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.transretail.crm.core.entity.Promotion;
import com.transretail.crm.core.entity.PromotionStoreGroup;
import com.transretail.crm.core.entity.support.CustomAuditableEntity;

@Audited
@Entity
@Table(name = "CRM_STORE_GROUP")
public class StoreGroup extends CustomAuditableEntity<Long> implements Serializable  {
    public static final String STORE_LIST_SEPARATOR = ",";
	private static final long serialVersionUID = 1L;

    @Column(name = "NAME")
	private String name;
	
	@Column(name = "STORES", length = 4000)
	private String stores;
	
	@NotAudited
	@JsonIgnore
	@OneToMany(mappedBy = "storeGroup", orphanRemoval = true, fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	private List<PromotionStoreGroup> promotionStoreGroups;
	
	@Transient
	private List<Store> storeModels;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getStores() {
		return stores;
	}

	public void setStores(String stores) {
		this.stores = stores;
	}

    public List<String> getStoreList() {
		if(StringUtils.isNotBlank(stores))
			return Arrays.asList(StringUtils.split(stores, STORE_LIST_SEPARATOR));
		else
			return null;
	}

	public List<Store> getStoreModels() {
		return storeModels;
	}

	public void setStoreModels(List<Store> storeModels) {
		this.storeModels = storeModels;
	}

	public List<PromotionStoreGroup> getPromotionStoreGroups() {
		return promotionStoreGroups;
	}

	public void setPromotionStoreGroups(
			List<PromotionStoreGroup> promotionStoreGroups) {
		this.promotionStoreGroups = promotionStoreGroups;
	}
    
    

}
