package com.transretail.crm.giftcard.service.impl;

import java.util.List;
import java.util.Map;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.mysema.query.BooleanBuilder;
import com.mysema.query.jpa.impl.JPAQuery;
import com.transretail.crm.common.service.dto.response.ResultList;
import com.transretail.crm.common.service.util.SpringDataPagingUtil;
import com.transretail.crm.giftcard.dto.GiftCardGeneralDiscountDto;
import com.transretail.crm.giftcard.dto.GiftCardGeneralDiscountSearchDto;
import com.transretail.crm.giftcard.entity.GiftCardGeneralDiscount;
import com.transretail.crm.giftcard.entity.QGiftCardGeneralDiscount;
import com.transretail.crm.giftcard.repo.GiftCardGeneralDiscountRepo;
import com.transretail.crm.giftcard.service.GiftCardGeneralDiscountService;

@Service("giftCardGeneralDiscountService")
public class GiftCardGeneralDiscountServiceImpl implements
		GiftCardGeneralDiscountService {
	@Autowired
	private GiftCardGeneralDiscountRepo repo;
	@PersistenceContext
	private EntityManager em;
	@Override
	public boolean checkForOverlap(GiftCardGeneralDiscountDto dto) {
		QGiftCardGeneralDiscount qdisc = QGiftCardGeneralDiscount.giftCardGeneralDiscount;
		BooleanBuilder fltr = new BooleanBuilder();
		if(dto.getMaxPurchase() != null && dto.getMinPurchase() != null) {
			fltr.and(qdisc.minPurchase.loe(dto.getMaxPurchase()))
			.and(qdisc.maxDiscount.goe(dto.getMinPurchase()));	
		} else if(dto.getMaxPurchase() != null) {
			fltr.and(qdisc.minPurchase.loe(dto.getMaxPurchase()))
			.and(qdisc.maxDiscount.goe(dto.getMaxPurchase()));
		} else if(dto.getMinPurchase() != null) {
			fltr.and(qdisc.minPurchase.loe(dto.getMinPurchase()))
			.and(qdisc.maxDiscount.goe(dto.getMinPurchase()));
		}
		
		if(dto.getId() != null)
			fltr.and(qdisc.id.ne(dto.getId()));
		return new JPAQuery(em).from(qdisc).where(fltr).exists();
		
	}
	
	private List<GiftCardGeneralDiscountDto> toDtoList(Iterable<GiftCardGeneralDiscount> models) {
		List<GiftCardGeneralDiscountDto> list = Lists.newArrayList();
		for(GiftCardGeneralDiscount model : models) {
			list.add(new GiftCardGeneralDiscountDto(model));
		}
		return list;
	}
	
	@Override
	@Transactional(readOnly=true)
	public GiftCardGeneralDiscountDto findById(Long id) {
		if(id == null) {
			return null;
		}
		return new GiftCardGeneralDiscountDto(repo.findOne(id));
	}

	@Override
	@Transactional
	public void save(GiftCardGeneralDiscountDto dto) {
		GiftCardGeneralDiscount model = null;
		
		if(dto.getId() != null) {
			model = repo.findOne(dto.getId());
		}
		
		repo.save(dto.toModel(model));
	}

	@Override
	@Transactional
	public void delete(Long id) {
		if(id == null) {
			return;
		}
		repo.delete(id);
	}
	
	@Override
	public ResultList<GiftCardGeneralDiscountDto> search(GiftCardGeneralDiscountSearchDto searchDto) {
		Map<String, String> sortMap = Maps.newHashMap();
		sortMap.put("maxPurchase", "maxDiscount");
		Pageable pageable = SpringDataPagingUtil.INSTANCE.createPageable(searchDto.getPagination(), sortMap);
		Page<GiftCardGeneralDiscount> page = repo.findAll(searchDto.createSearchExpression(), pageable);
		return new ResultList<GiftCardGeneralDiscountDto>(toDtoList(page.getContent()), page.getTotalElements(), 
				page.hasPreviousPage(), page.hasNextPage());
	}
}
