package com.transretail.crm.report.template.impl;

import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.joda.time.DateTime;
import org.joda.time.LocalDate;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.MessageSourceAware;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.context.support.MessageSourceAccessor;
import org.springframework.stereotype.Service;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.google.common.collect.Sets;
import com.mysema.query.Tuple;
import com.mysema.query.jpa.impl.JPAQuery;
import com.mysema.query.types.expr.BooleanExpression;
import com.transretail.crm.common.reporting.jasper.ClasspathFileResolver;
import com.transretail.crm.common.reporting.jasper.DefaultJRProcessor;
import com.transretail.crm.common.reporting.jasper.JRProcessor;
import com.transretail.crm.core.entity.QEmployeePurchaseTxnModel;
import com.transretail.crm.core.entity.QMemberModel;
import com.transretail.crm.core.entity.QPointsTxnModel;
import com.transretail.crm.core.entity.QRewardSchemeModel;
import com.transretail.crm.core.entity.embeddable.QProfessionalProfile;
import com.transretail.crm.core.entity.enums.PointTxnType;
import com.transretail.crm.core.entity.enums.ReportType;
import com.transretail.crm.core.entity.enums.VoucherTransactionType;
import com.transretail.crm.core.entity.lookup.QLookupDetail;
import com.transretail.crm.core.service.StoreService;
import com.transretail.crm.core.util.FormatterUtil;
import com.transretail.crm.report.template.FilterField;
import com.transretail.crm.report.template.ReportTemplate;
import com.transretail.crm.report.template.ReportsCustomizer;
import com.transretail.crm.report.template.ReportsCustomizer.Option;


@Service("rewardsSummaryByMonthReport")
public class RewardsSummaryByMonthReport implements ReportTemplate, MessageSourceAware, ReportsCustomizer{
	protected static final String TEMPLATE_NAME = "Rewards Summary By Month";
	protected static final String FILTER_MONTH_YEAR_FORMAT = "MMM-yyyy";
	protected static final String FILTER_MONTH_YEAR = "dateTo";
    protected static final String REPORT_NAME = "reports/monthly_rewards_summary_report.jasper";
	protected static final DateTimeFormatter MONTH_YR_FMT = DateTimeFormat.forPattern(FILTER_MONTH_YEAR_FORMAT).withLocale(LocaleContextHolder.getLocale());
	
	private static final String PARAM_IS_EMPTY = "isEmpty";
    private static final String PARAM_NO_DATA_FOUND = "noDataFound";
	
	private MessageSourceAccessor messageSource;
	@PersistenceContext
    private EntityManager em;
	@Autowired
	private StoreService storeService;

	@Override
	public void setMessageSource(MessageSource messageSource) {
		this.messageSource = new MessageSourceAccessor(messageSource);
	}

	@Override
	public String getName() {
		return TEMPLATE_NAME;
	}

	@Override
	public String getDescription() {
		return messageSource.getMessage("rewards.summary.by.month.report");
	}

	@Override
	public Set<FilterField> getFilters() {
		Set<FilterField> filters = Sets.newLinkedHashSetWithExpectedSize(2);
	    filters.add(FilterField.createDateField(FILTER_MONTH_YEAR,
		    messageSource.getMessage("rewards.summary.by.month.month"),
		    FILTER_MONTH_YEAR_FORMAT, FilterField.DateTypeOption.YEAR_MONTH, true));

	    return filters;
	}

	@Override
	public boolean canView(Set<String> permissions) {
		if (permissions != null) {
            for (String permission : permissions) {
                if (permission.equalsIgnoreCase("REPORT_REWARDS_SUMMARY_BY_MONTH_VIEW")) {
                    return true;
                }
            }
        }
        return false;
	}

	@Override
	public JRProcessor createJrProcessor(Map<String, String> map) {
		LocalDate date = MONTH_YR_FMT.parseLocalDate(map.get(FILTER_MONTH_YEAR));
		Date midMonth = date.withDayOfMonth(15).toDateTimeAtStartOfDay().toDate();
		/*int yearMonth = toYearMonth(date);*/
		int year = toYear(date);
		String yearMonthStr = toYearMonthStr(date);
		QPointsTxnModel qPoints = QPointsTxnModel.pointsTxnModel;
		QEmployeePurchaseTxnModel qPurchase = QEmployeePurchaseTxnModel.employeePurchaseTxnModel;
		QRewardSchemeModel qScheme = QRewardSchemeModel.rewardSchemeModel;
                QMemberModel qm = QMemberModel.memberModel;
		QLookupDetail qld = QLookupDetail.lookupDetail;
		
		Map<String, Object> parameters = Maps.newHashMap();
		parameters.put("YEAR_MONTH", yearMonthStr);
		
		BooleanExpression predicate = qPoints.transactionType.eq(PointTxnType.REWARD)
				/*.and(qPoints.transactionDateTime.yearMonth().eq(yearMonth))*/
				.and(qPoints.incDateFrom.loe(midMonth))
				.and(qPoints.incDateTo.goe(midMonth));
				/*.and(qm.registeredStore.eq(qStore.code))*/
				;
                List<Tuple> tuples = new JPAQuery(em).from(qPoints)
                    .leftJoin(qPoints.memberModel, qm)
                    .leftJoin(qm.professionalProfile.department, qld)
                    .where(predicate)
                    .orderBy(qm.lastName.asc(), qm.firstName.asc(), qm.registeredStore.asc())
                    .list(qm.accountId,
                            qm.firstName,
                            qm.lastName,
                            qm.registeredStore,
                            qPoints.transactionAmount, //purchase in current month
                            qPoints.transactionPoints,
                            qld.description);
		
		List<RewardBean> dataSource = Lists.newArrayList();
		
		if(CollectionUtils.isNotEmpty(tuples)) {
			Map<String, RewardBean> rewardBeanMap = new LinkedHashMap<String, RewardBean>();
			Map<Double, Double> scheme = new HashMap<Double, Double>();
			Map<String, String> storeMap = new HashMap<String, String>();
			int index = 1;
			for(Tuple tuple: tuples) {
				String accountId = tuple.get(qm.accountId);
				RewardBean rewardBean = rewardBeanMap.get(accountId);
				if(rewardBean == null) {
					rewardBean = new RewardBean();
					rewardBean.setNo(index);
					rewardBean.setAccountId(accountId);
					rewardBean.setFirstName(tuple.get(qm.firstName));
					rewardBean.setLastName(tuple.get(qm.lastName));
					String storeCode = tuple.get(qm.registeredStore);
					if(StringUtils.isNotBlank(storeCode)) {
						rewardBean.setRegisteredStore(storeCode);
						String storeName = storeMap.get(storeCode);
						if(StringUtils.isBlank(storeName)) {
							storeName = storeService.getStoreByCode(storeCode).getName();
							storeMap.put(storeCode, storeName);
						}
						rewardBean.setRegisteredStoreName(storeName);	
					}
                                        rewardBean.setDepartment(tuple.get(qld.description));
					rewardBean.setMonthsPurchase(tuple.get(qPoints.transactionAmount));
					Double ytdPurchase = new JPAQuery(em).from(qPurchase)
							.where(qPurchase.transactionType.eq(VoucherTransactionType.PURCHASE)
									.and(qPurchase.memberModel.accountId.eq(rewardBean.getAccountId()))
									.and(qPurchase.transactionDateTime.year().eq(year)))
							.singleResult(qPurchase.transactionAmount.sum());
					rewardBean.setYtdPurchase(ytdPurchase);
					rewardBeanMap.put(accountId, rewardBean);
					index++;
				}
				Double rewards = tuple.get(qPoints.transactionPoints);
				rewardBean.addRewards(rewards);
				Double validPurchase = scheme.get(rewards);
				if(validPurchase == null) {
					validPurchase = new JPAQuery(em).from(qScheme).where(qScheme.amount.eq(rewards)).singleResult(qScheme.valueFrom);
					scheme.put(rewards, validPurchase);
				}
				rewardBean.addValidPurchase(validPurchase);
			}
			dataSource.addAll(rewardBeanMap.values());
			
		} else {
			parameters.put(PARAM_IS_EMPTY, true);
            parameters.put(PARAM_NO_DATA_FOUND, "No data found");
            dataSource.add(new RewardBean());
		}
		dataSource.add(0, new RewardBean());
		
		DefaultJRProcessor jrProcessor = new DefaultJRProcessor(REPORT_NAME, dataSource);
        jrProcessor.addParameters(parameters);
        jrProcessor.setFileResolver(new ClasspathFileResolver("reports"));
        return jrProcessor;
		
	}

/*	private int toYearMonth(LocalDate date) {
		return Integer.parseInt(date.toString("yyyyMM"));
	}*/
	
	private int toYear(LocalDate date) {
		return Integer.parseInt(date.toString("yyyy"));
	}
	
	private String toYearMonthStr(LocalDate date) {
		return date.toString("MMMM yyyy");
	}
	
	@Override
	public ReportType[] getReportTypes() {
		return ReportType.values();
	}
	
	public static class RewardBean {
		private int no;
		private String accountId;
		private String firstName;
		private String lastName;
		private String registeredStore;
		private String registeredStoreName;
                private String department;
		private Double ytdPurchase;
		private Double monthsPurchase;
		private Double remainingPurchaseBal = 0d;
		private Integer rewards25000 = 0;
		private Integer rewards50000 = 0;
		private Integer rewards75000 = 0;
		private Integer rewards100000 = 0;
		private Double totalRewards = 0d;
		public int getNo() {
			return no;
		}
		public void setNo(int no) {
			this.no = no;
		}
		public String getAccountId() {
			return accountId;
		}
		public void setAccountId(String accountId) {
			this.accountId = accountId;
		}
		public String getFirstName() {
			return firstName;
		}
		public void setFirstName(String firstName) {
			this.firstName = firstName;
		}
		public String getLastName() {
			return lastName;
		}
		public void setLastName(String lastName) {
			this.lastName = lastName;
		}
		public String getFullName() {
			return FormatterUtil.INSTANCE.formatName(this.lastName, this.firstName);
		}
		public String getRegisteredStore() {
			return registeredStore;
		}
		public void setRegisteredStore(String registeredStore) {
			this.registeredStore = registeredStore;
		}
		public String getRegisteredStoreName() {
			return registeredStoreName;
		}
		public void setRegisteredStoreName(String registeredStoreName) {
			this.registeredStoreName = registeredStoreName;
		}

                public String getDepartment() {
                    return department;
                }

                public void setDepartment(String department) {
                    this.department = department;
                }

		public Double getYtdPurchase() {
			return ytdPurchase;
		}
		public void setYtdPurchase(Double ytdPurchase) {
			this.ytdPurchase = ytdPurchase;
		}
		public Double getMonthsPurchase() {
			return monthsPurchase;
		}
		public void setMonthsPurchase(Double monthsPurchase) {
			this.monthsPurchase = monthsPurchase;
		}
		public Double getRemainingPurchaseBal() {
			return remainingPurchaseBal;
		}
		public void setRemainingPurchaseBal(Double remainingPurchaseBal) {
			this.remainingPurchaseBal = remainingPurchaseBal;
		}
		public Integer getRewards25000() {
			return rewards25000;
		}
		public void setRewards25000(Integer rewards25000) {
			this.rewards25000 = rewards25000;
		}
		public Integer getRewards50000() {
			return rewards50000;
		}
		public void setRewards50000(Integer rewards50000) {
			this.rewards50000 = rewards50000;
		}
		public Integer getRewards75000() {
			return rewards75000;
		}
		public void setRewards75000(Integer rewards75000) {
			this.rewards75000 = rewards75000;
		}
		public Integer getRewards100000() {
			return rewards100000;
		}
		public void setRewards100000(Integer rewards100000) {
			this.rewards100000 = rewards100000;
		}
		public Double getTotalRewards() {
			return totalRewards;
		}
		public void setTotalRewards(Double totalRewards) {
			this.totalRewards = totalRewards;
		}
		public Double getUnredeemedVal() {
			if(this.monthsPurchase != null && this.remainingPurchaseBal != null)
				return this.monthsPurchase - this.remainingPurchaseBal;
			return null;
		}
		public void addRewards(Double reward) {
			this.totalRewards += reward;
			if(reward.doubleValue() == 25000) this.rewards25000++;
			else if(reward.doubleValue() == 50000) this.rewards50000++;
			else if(reward.doubleValue() == 75000) this.rewards75000++;
			else if(reward.doubleValue() == 100000) this.rewards100000++;
		}
		public void addValidPurchase(Double validPurchase) {
			this.remainingPurchaseBal += validPurchase;
		}
		
	}

	@Override
	public Set<Option> customizerOption() {
		return Sets.newHashSet(Option.DATE_FROM_OPTIONAL);
	}
	
	
}
