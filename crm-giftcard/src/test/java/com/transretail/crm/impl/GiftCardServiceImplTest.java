package com.transretail.crm.impl;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.math.BigDecimal;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.transretail.crm.giftcard.dto.GiftCardDto;
import com.transretail.crm.giftcard.dto.GiftCardResultList;
import com.transretail.crm.giftcard.dto.GiftCardSearchDTO;
import com.transretail.crm.giftcard.entity.CardVendor;
import com.transretail.crm.giftcard.entity.GiftCard;
import com.transretail.crm.giftcard.repo.CardVendorRepo;
import com.transretail.crm.giftcard.repo.GiftCardRepo;
import com.transretail.crm.giftcard.service.GiftCardService;

/**
 *
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath*:/META-INF/spring/applicationContext*.xml",
    "classpath:META-INF/spring/applicationContext-test.xml"})
public class GiftCardServiceImplTest {
    private static final String CARD_NAME = "tong-its";
    private static final String CARD_GENCODE = "gencode";
    @Autowired
    private GiftCardService service;
    @Autowired
    private GiftCardRepo giftCardRepo;
    @Autowired
    private CardVendorRepo vendorRepo;
    private CardVendor savedVendor;
    private GiftCard savedGiftCard;

    @Before
    public void onSetup() {
        savedVendor = new CardVendor();
        savedVendor.setFormalName("name");
        savedVendor.setMailAddress("add");
        savedVendor.setEmailAddress("email");
        savedVendor.setEnabled(true);
        savedVendor.setRegion("region");
        savedVendor.setDefaultVendor(true);
        savedVendor = vendorRepo.save(savedVendor);

        savedGiftCard = new GiftCard();
        savedGiftCard.setName(CARD_NAME);
        savedGiftCard.setGenCode(CARD_GENCODE);
        savedGiftCard.setVendor(savedVendor);
        savedGiftCard.setVendorName(savedVendor.getFormalName());
        savedGiftCard.setFaceValue(new BigDecimal(10000));
        savedGiftCard.setRandomNoDigits(111);

        savedGiftCard = giftCardRepo.save(savedGiftCard);
    }

    @After
    public void tearDown() {
        if (savedGiftCard != null && savedGiftCard.getId() != null) {
            giftCardRepo.delete(savedGiftCard.getId());
        }
        if (savedVendor != null && savedVendor.getId() != null) {
            vendorRepo.delete(savedVendor.getId());
        }
    }

    @Test
    public void getGiftCardTest() {
        GiftCardDto dto = service.getGiftCard(savedGiftCard.getId());
        assertEquals(CARD_NAME, dto.getName());
        assertEquals(CARD_GENCODE, dto.getGenCode());
        assertEquals(savedVendor.getId(), dto.getVendor());
        assertEquals(savedVendor.getFormalName(), dto.getVendorName());
    }

    @Test
    public void saveGiftCardTest() {
        Long savedGiftCard = null;
        String name = "xxxcloudxxx";
        try {
            GiftCardDto dto = new GiftCardDto();
            dto.setName(name);
            dto.setGenCode("gencode");
            dto.setVendor(savedVendor.getId());
            dto.setVendorName(savedVendor.getFormalName());
            dto.setFaceValue(new BigDecimal(10000));
            dto.setRandomNoDigits(111);
            savedGiftCard = service.saveGiftCard(dto);

            assertNotNull(savedGiftCard);
            assertTrue(giftCardRepo.exists(savedGiftCard));
        } finally {
            if (savedGiftCard != null) {
                giftCardRepo.delete(savedGiftCard);
            }
        }
    }

    @Test
    public void updateGiftCardTest() {
        String newName = "newname";
        GiftCardDto dto = new GiftCardDto();
        dto.setName(newName);
        dto.setGenCode("gencode");
        dto.setVendor(savedVendor.getId());
        dto.setVendorName(savedVendor.getFormalName());
        dto.setFaceValue(new BigDecimal(10000));
        dto.setRandomNoDigits(111);
        service.updateGiftCard(savedGiftCard.getId(), dto);
        GiftCard gc = giftCardRepo.findOne(savedGiftCard.getId());
        assertEquals(newName, gc.getName());
    }

    @Test
    public void getGiftCardsTest() {
        GiftCardSearchDTO searchForm = new GiftCardSearchDTO();
        GiftCardResultList resultList = service.getGiftCards(searchForm);
        assertEquals(1, resultList.getTotalElements());
        assertEquals(1, resultList.getNumberOfElements());
        assertEquals(CARD_GENCODE, resultList.getResults().iterator().next().getGenCode());

        searchForm.setName("unexistingname");
        resultList = service.getGiftCards(searchForm);
        assertEquals(0, resultList.getTotalElements());
        assertEquals(0, resultList.getNumberOfElements());
        assertEquals(0, resultList.getResults().size());
    }
}
