package com.transretail.crm.core.entity.enums;

public enum TxnStatus {
    FORAPPROVAL, REJECTED, ACTIVE, EXPIRED, DELETED
}
