package com.transretail.crm.giftcard.repo;

import com.transretail.crm.core.entity.CrmForPeoplesoftConfig.TransactionType;
import com.transretail.crm.giftcard.dto.AffiliateDto;
import java.io.InputStream;
import java.util.List;
import java.util.Scanner;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.sql.DataSource;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.fest.assertions.api.Assertions;
import org.fest.assertions.core.Condition;
import org.joda.time.*;
import org.junit.After;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.support.TransactionCallback;
import org.springframework.transaction.support.TransactionTemplate;

/**
 *
 * @author Monte Cillo Co (mco@exist.com)
 */
@RunWith(SpringJUnit4ClassRunner.class)
@Transactional
@ContextConfiguration(locations = {"classpath*:/META-INF/spring/applicationContext*.xml",
    "classpath:META-INF/spring/applicationContext-test.xml"})
public class GiftCardTransactionRepoTest {

    @Autowired
    private GiftCardTransactionRepo transactionRepo;
    @PersistenceContext
    private EntityManager em;
    @Autowired
    private PlatformTransactionManager transactionManager;

    @Before
    public void setup() {
        
        long fixedMillis = new DateTime(2014, 07, 25, 01, 50).getMillis();
        DateTimeUtils.setCurrentMillisFixed(fixedMillis);

        InputStream is = Thread.currentThread().getContextClassLoader().getResourceAsStream("scripts/gifcardtransactionrepo.sql");
        try {
            final Scanner in = new Scanner(is);
            TransactionTemplate transactionTemplate = new TransactionTemplate(transactionManager);
            transactionTemplate.execute(new TransactionCallback() {
                @Override
                public Object doInTransaction(TransactionStatus status) {
                    while (in.hasNext()) {
                        String script = in.nextLine();
                        if (StringUtils.isNotBlank(script)) {
                            em.createNativeQuery(script).executeUpdate();
                            
                        }
                    }
                    return null;
                }
            });
        } finally {
            IOUtils.closeQuietly(is);
        }

        em.flush();
    }

    @Test
    public void getBusinessUnitTransactionTypeAmountTest() {
        YearMonth yearMonth = new YearMonth(2014, 8);
        List<AffiliateDto> affiliateTxs = transactionRepo.getBusinessUnitTransactionTypeAmount(yearMonth);
        Assertions.assertThat(affiliateTxs).hasSize(2)
                .areExactly(1, new AffiliateCondition(TARGET_BU, 1000000.0, TransactionType.AFFILIATE_CLAIM, TARGET_BU))
                .areExactly(1, new AffiliateCondition(TARGET_BU, -500000.0, TransactionType.AFFILIATE_REDEMPTION, TARGET_BU));
    }
    
    @Test
    public void tagAffiliatesTransactionAsPosted() {
        YearMonth yearMonth = new YearMonth(2014, 8);
        long affectRecords = transactionRepo.tagAffiliatesTransactionAsPosted(yearMonth);
        Assertions.assertThat(affectRecords).isEqualTo(7);
    }
    

    static final String TARGET_BU = "ID050";

    private static class AffiliateCondition extends Condition<AffiliateDto> {

        private final String bu;
        private final Double amount;
        private final TransactionType target;

        public AffiliateCondition(String bu, Double amount, TransactionType target, String description) {
            super(description);
            this.bu = bu;
            this.amount = amount;
            this.target = target;
        }

        @Override
        public boolean matches(AffiliateDto value) {
            return bu.equals(value.getBu()) && value.getAmount() == amount
                    && value.getTransactionType() == target;
        }
    }
}
