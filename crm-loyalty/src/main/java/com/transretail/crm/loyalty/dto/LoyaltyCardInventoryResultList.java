package com.transretail.crm.loyalty.dto;

import java.util.Collection;

import org.springframework.data.domain.Page;

import com.transretail.crm.common.service.dto.response.AbstractResultListDTO;

public class LoyaltyCardInventoryResultList extends AbstractResultListDTO<LoyaltyCardInventoryDto> {
	public LoyaltyCardInventoryResultList(Collection<LoyaltyCardInventoryDto> results, long totalElements, boolean hasPreviousPage, boolean hasNextPage) {
        super(results, totalElements, hasPreviousPage, hasNextPage);
    }
}
