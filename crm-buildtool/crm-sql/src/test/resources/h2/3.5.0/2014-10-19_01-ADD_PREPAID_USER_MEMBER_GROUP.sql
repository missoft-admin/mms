alter table CRM_MEMBER_GRP add PREPAID_USER char(1);
alter table CRM_MEMBER_GRP alter column PREPAID_USER SET DEFAULT 'N';

alter table CRM_MEMBER_GRP_AUD add PREPAID_USER char(1);

update CRM_MEMBER_GRP set PREPAID_USER = 'N' where PREPAID_USER IS NULL;