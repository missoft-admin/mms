package com.transretail.crm.core.repo;

import org.springframework.stereotype.Repository;

import com.transretail.crm.common.repo.CrmQueryDslPredicateExecutor;
import com.transretail.crm.core.entity.MemberMonitorConfig;
import com.transretail.crm.core.entity.lookup.LookupDetail;
import com.transretail.crm.core.repo.custom.MemberMonitorConfigRepoCustom;

@Repository
public interface MemberMonitorConfigRepo extends CrmQueryDslPredicateExecutor<MemberMonitorConfig, Long>, MemberMonitorConfigRepoCustom {
	MemberMonitorConfig findByMemberType(LookupDetail memberType);
}
