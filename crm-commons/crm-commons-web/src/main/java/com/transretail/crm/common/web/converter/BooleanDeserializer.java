package com.transretail.crm.common.web.converter;

import java.io.IOException;

import org.apache.commons.lang3.StringUtils;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;

/**
 *
 */
public class BooleanDeserializer extends JsonDeserializer<Boolean> {
    @Override
    public Boolean deserialize(JsonParser jp, DeserializationContext ctxt) throws IOException, JsonProcessingException {
        if (StringUtils.isBlank(jp.getText())) {
            return null;
        }
        if (jp.getText().equalsIgnoreCase("on") || jp.getText().equalsIgnoreCase("true") || jp.getText().equalsIgnoreCase("yes")) {
            return Boolean.TRUE;
        }
        return Boolean.FALSE;
    }
}
