package com.transretail.crm.giftcard.repo.custom.impl;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.joda.time.LocalDate;

import com.mysema.query.jpa.impl.JPAQuery;
import com.transretail.crm.giftcard.entity.QGiftCardBurnCard;
import com.transretail.crm.giftcard.repo.custom.GiftCardBurnCardRepoCustom;
import com.transretail.crm.giftcard.repo.custom.StockRequestRepoCustom;

public class GiftCardBurnCardRepoImpl implements GiftCardBurnCardRepoCustom {
	@PersistenceContext
	EntityManager em;

	@Override
	public String createBurnCardNumber() {
		QGiftCardBurnCard burncard = QGiftCardBurnCard.giftCardBurnCard;
		JPAQuery query = new JPAQuery(em);
		
		LocalDate today = LocalDate.now();
		StringBuilder builder = new StringBuilder(today.toString("yyyyMMdd"));
		Long count = query.from(burncard).where(burncard.dateFiled.eq(today))
				.uniqueResult(burncard.burnNo.countDistinct()) + 1;
		builder.append(String.format("%04d", count != null ? count : 0));
    	return builder.toString();
	}

}
