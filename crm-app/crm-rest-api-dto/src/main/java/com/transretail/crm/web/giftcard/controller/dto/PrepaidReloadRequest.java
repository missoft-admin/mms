package com.transretail.crm.web.giftcard.controller.dto;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.transretail.crm.common.web.converter.ISODateTimeDeserializer;
import com.transretail.crm.common.web.converter.ISODateTimeSerializer;
import com.transretail.crm.giftcard.dto.GiftCardTransactionInfo;
import com.transretail.crm.web.giftcard.controller.GiftCardCommonRequest;
import javax.validation.constraints.NotNull;
import org.hibernate.validator.constraints.NotEmpty;
import org.joda.time.LocalDateTime;
import org.springframework.format.annotation.DateTimeFormat;

/**
 *
 * @author Allan G. Ramirez (agramirez@exist.com)
 * @author Monte Cillo Co (mco@exist.com)
 */
public class PrepaidReloadRequest extends GiftCardCommonRequest {
    @NotEmpty
    private String transactionNo;
    @NotNull
    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME)
    @JsonSerialize(using = ISODateTimeSerializer.class)
    @JsonDeserialize(using = ISODateTimeDeserializer.class)
    private LocalDateTime transactionDate;
    @NotNull
    private Long reloadAmount;

    public String getTransactionNo() {
        return transactionNo;
    }

    public void setTransactionNo(String transactionNo) {
        this.transactionNo = transactionNo;
    }

    public LocalDateTime getTransactionDate() {
        return transactionDate;
    }

    public void setTransactionDate(LocalDateTime transactionDate) {
        this.transactionDate = transactionDate;
    }

    public Long getReloadAmount() {
        return reloadAmount;
    }

    public void setReloadAmount(Long reloadAmount) {
        this.reloadAmount = reloadAmount;
    }
    
     @Override
    public String getSignature() {
        return String.format("%s%s%s%s%s", getMerchantId(), getTerminalId(), getCashierId(), getTransactionNo(), getCardNo());
    }

    @Override
    public GiftCardTransactionInfo toGiftCardTransactionInfo() {
        return super.toGiftCardTransactionInfo().transactionNo(transactionNo).transactionTime(transactionDate);
    }
}
