package com.transretail.crm.core.repo;

import org.springframework.stereotype.Repository;

import com.transretail.crm.common.repo.CrmQueryDslPredicateExecutor;
import com.transretail.crm.core.entity.UserModel;
import com.transretail.crm.core.repo.custom.UserRepoCustom;

@Repository
public interface UserRepo extends CrmQueryDslPredicateExecutor<UserModel, Long>, UserRepoCustom {

    UserModel findByUsername(String username);
    
    UserModel findByUsernameIgnoreCase(String username);

}
