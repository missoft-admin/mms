package com.transretail.crm.core.entity.enums;


public enum AuthenticationType {
    INTERNAL, LDAP
}
