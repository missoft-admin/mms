package com.transretail.crm.sql;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Scanner;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;

/**
 * @author Allan G. Ramirez (agramirez@exist.com)
 */
public class OrclInsertScriptToTTInsertCompatibleScript {
    public static void main(String[] args) throws Exception {
        Scanner in = new Scanner(System.in);
        System.out.print("Input schema (Oracle schema of the exported scripts)    : ");
        String inSchema = in.nextLine();
        if (StringUtils.isBlank(inSchema)) {
            throw new IllegalArgumentException("Input schema is required.");
        }
        System.out.print("Output schema (Schema destination in Timesten)    : ");
        String outSchema = in.nextLine();
        if (StringUtils.isBlank(outSchema)) {
            throw new IllegalArgumentException("Output schema is required.");
        }
        System.out.print("Exported scripts directory : ");
        String scriptsDirPath = in.nextLine();
        if (StringUtils.isBlank(scriptsDirPath)) {
            throw new IllegalArgumentException("Exported scripts directory is required.");
        }
        System.out.print("Output scripts directory : ");
        String outputDirPath = in.nextLine();
        if (StringUtils.isBlank(outputDirPath)) {
            throw new IllegalArgumentException("Output scripts directory is required.");
        }
        File outputDir = new File(outputDirPath);
        if (!outputDir.exists() && !outputDir.mkdirs()) {
            throw new IOException("Failed to create directory [" + outputDirPath + "]");
        }
        OrclInsertScriptToTTInsertCompatibleScript converter = new OrclInsertScriptToTTInsertCompatibleScript();
        convert(converter, new File(scriptsDirPath), outputDir, inSchema, outSchema);
    }

    static void convert(final OrclInsertScriptToTTInsertCompatibleScript converter, final File scriptsDir, final File outputDir,
        final String inSchema, final String outSchema) throws Exception {
        for (File file : scriptsDir.listFiles()) {
            if (file.isDirectory()) {
                convert(converter, file, outputDir, inSchema, outSchema);
            }
            String fileName = file.getName();
            if (!fileName.endsWith(".sql")) {
                continue;
            }
            File outputFile = new File(outputDir, fileName);
            final FileWriter writer = new FileWriter(outputFile);
            try {
                converter.convert(file, new LineWriterDelegate() {
                    @Override
                    public void write(String line) throws Exception {
                        writer.write(line + "\r\n");
                    }
                }, inSchema, outSchema);
            } finally {
                IOUtils.closeQuietly(writer);
            }
        }
    }

    public synchronized void convert(File file, LineWriterDelegate delegate, String inSchema, String outSchema) throws Exception {
        FileInputStream is = null;
        try {
            is = new FileInputStream(file);
            Scanner scanner = new Scanner(is);
            parse(scanner, delegate, inSchema, outSchema);
        } finally {
            if (is != null) {
                is.close();
            }
        }
    }

    private void parse(Scanner scanner, LineWriterDelegate delegate, String inSchema, String outSchema) throws Exception {
        while (scanner.hasNext()) {
            String line = scanner.nextLine();
            if (line.toUpperCase().startsWith("INSERT INTO")) {
                if (!inSchema.equals(outSchema)) {
                    line = line.replaceFirst(inSchema + ".", outSchema + ".");
                }
            }
            int toTimestampIdx = line.indexOf("to_timestamp");
            if (toTimestampIdx != -1) {
                String newLine = "";
                String remainingLine = line;
                while (toTimestampIdx != -1) {
                    newLine += remainingLine.substring(0, toTimestampIdx);
                    remainingLine = remainingLine.substring(toTimestampIdx);
                    int idx1 = remainingLine.indexOf('(');
                    int idx2 = remainingLine.indexOf(',', idx1 + 2);
                    newLine += "TIMESTAMP " + remainingLine.substring(idx1 + 1, idx2);
                    int idx3 = remainingLine.indexOf(')');
                    remainingLine = remainingLine.substring(idx3 + 1);
                    toTimestampIdx = remainingLine.indexOf("to_timestamp");
                }
                line = newLine + remainingLine;
            }
            delegate.write(line);
        }
    }
}
