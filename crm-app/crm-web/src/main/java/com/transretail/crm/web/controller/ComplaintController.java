package com.transretail.crm.web.controller;


import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.transretail.crm.common.service.dto.response.ResultList;
import com.transretail.crm.core.dto.ApprovalRemarkDto;
import com.transretail.crm.core.dto.ComplaintDto;
import com.transretail.crm.core.dto.ComplaintSearchDto;
import com.transretail.crm.core.dto.ComplaintSearchDto.ComplaintSearchField;
import com.transretail.crm.core.dto.MessageDto;
import com.transretail.crm.core.dto.MessageDto.MsgType;
import com.transretail.crm.core.entity.Complaint;
import com.transretail.crm.core.entity.Complaint.ComplaintCategory;
import com.transretail.crm.core.entity.Complaint.ComplaintMemberField;
import com.transretail.crm.core.entity.Complaint.ComplaintPriority;
import com.transretail.crm.core.entity.lookup.Store;
import com.transretail.crm.core.security.util.UserUtil;
import com.transretail.crm.core.service.CodePropertiesService;
import com.transretail.crm.core.service.ComplaintService;
import com.transretail.crm.core.service.LookupService;
import com.transretail.crm.core.service.StoreService;
import com.transretail.crm.core.service.UserService;
import com.transretail.crm.media.service.MailService;
import com.transretail.crm.media.service.SmsService;
import com.transretail.crm.web.controller.util.ControllerResponse;
import com.transretail.crm.web.controller.util.ControllerResponseExceptionHandler;


@RequestMapping("/customer/complaint")
@Controller
public class ComplaintController extends ControllerResponseExceptionHandler {

	private static final String PATH_MAIN 				= "/customer/complaint";
	private static final String PATH_SAVE 				= "/customer/complaint/save";
	private static final String PATH_UPDATE				= "/customer/complaint/update";
	private static final String PATH_LIST 				= "/customer/complaint/list";

	private static final String PATH_FORM				= "/customer/complaint/form";
	private static final String PATH_FORM_ASSIGN		= "/customer/complaint/form/assign";
	private static final String PATH_FORM_PROCESS		= "/customer/complaint/form/process";
	private static final String PATH_FORM_VIEW			= "/customer/complaint/form/view";
	private static final String PATH_FORM_REPLY			= "/customer/complaint/form/reply";
	private static final String PATH_POPULATEMEMBER 	= "/customer/complaint/member/populate?mobileNo=%MOBILENO%&email=%EMAIL%";

	private static final String UIATTR_FORM		 		= "complaint";
	private static final String UIATTR_ACTION			= "complaintAction";
	private static final String UIATTR_LBL_HDR			= "complaintHeader";

	private static final String UIATTR_CATEGORIES		= "categories";
	private static final String UIATTR_PRIORITIES		= "priorities";
	private static final String UIATTR_GENDERS			= "genders";
	private static final String UIATTR_USERS			= "users";
	private static final String UIATTR_STORES			= "stores";
	private static final String UIATTR_STATS			= "stats";
	private static final String UIATTR_SEARCHFIELDS		= "searchFields";
	private static final String UIATTR_REMARKS			= "remarks";

	private static final String UIATTR_STORE			= "store";
	private static final String UIATTR_ID				= "complaintId";
	private static final String UIATTR_REPLY	 		= "reply";
	private static final String UIATTR_REPLY_EMAIL 		= "emailReply";
	private static final String UIATTR_REPLY_SMS 		= "smsReply";
	private static final String UIATTR_ISSUPERVISOR		= "isCss";

	private static final String UIATTR_PERCOUNT			= "perCount";
	private static final String UIATTR_MAXCOUNT			= "maxCount";
	private static final String UIATTR_COMPLAINTSCOUNT	= "complaintsCount";



	@Autowired
	ComplaintService service;
	@Autowired
	LookupService lookupService;
	@Autowired
	CodePropertiesService codePropertiesService;
	@Autowired
	StoreService storeService;
	@Autowired
	UserService userService;
	@Autowired
	MailService mailService;
	@Autowired
	SmsService smsService;
    @Autowired
    private MessageSource messageSource;



	@InitBinder
	void initBinder( WebDataBinder binder ) {
		DateFormat theFormat = new SimpleDateFormat( "dd MMM yyyy" );
		theFormat.setLenient( false );
		binder.registerCustomEditor( Date.class, new CustomDateEditor( theFormat, true ) );
	}

    @RequestMapping( produces = "text/html" )
    public String showMain( Model uiModel ) {
    	create( uiModel );
    	populateSearchForm( uiModel );
    	return PATH_MAIN;
    }

    @RequestMapping(value = "/list", method = RequestMethod.POST)
    public @ResponseBody ResultList<ComplaintDto> list( @RequestBody ComplaintSearchDto dto ) {
    	return service.search( dto );
    }

    @RequestMapping( value="/form/new", produces = "text/html" )
    public String create( Model uiModel ) {
    	populateForm( uiModel, null );
    	uiModel.addAttribute( UIATTR_ACTION, PATH_SAVE );
    	uiModel.addAttribute( UIATTR_LBL_HDR, messageSource.getMessage( "complaint_lbl_create", null, LocaleContextHolder.getLocale() ) );
    	return PATH_FORM;
    }

    @RequestMapping( value="/form/edit/{id}", produces = "text/html" )
    public String edit( @PathVariable(value="id") Long id, Model uiModel ) {
    	populateForm( uiModel, service.getDto( id ) );
    	uiModel.addAttribute( UIATTR_ACTION, PATH_UPDATE );
    	uiModel.addAttribute( UIATTR_LBL_HDR, messageSource.getMessage( "complaint_lbl_edit", null, LocaleContextHolder.getLocale() ) );
    	return PATH_FORM;
    }

    @RequestMapping( value="/form/assign/{id}", produces = "text/html" )
    public String populateAssignForm( @PathVariable(value="id") Long id, Model uiModel ) {
    	populateForm( uiModel, service.getDto( id ), null );
    	return PATH_FORM_ASSIGN;
    }

    @RequestMapping( value="/form/process/{id}", produces = "text/html" )
    public String populateProcessForm( @PathVariable(value="id") Long id, Model uiModel ) {
    	ComplaintDto dto = service.getDto( id );
    	populateForm( uiModel, dto, null );
    	ResultList<ApprovalRemarkDto> remarks = listRemarks( id );
    	uiModel.addAttribute( UIATTR_REMARKS, null != remarks? remarks.getResults() : null );

    	uiModel.addAttribute( UIATTR_STATS, service.getStats( dto ) );
    	return PATH_FORM_PROCESS;
    }

    @RequestMapping( value="/form/view/{id}", produces = "text/html" )
    public String populateViewForm( @PathVariable(value="id") Long id, Model uiModel ) {
    	populateForm( uiModel, service.getDto( id ) );
    	ResultList<ApprovalRemarkDto> remarks = listRemarks( id );
    	uiModel.addAttribute( UIATTR_REMARKS, null != remarks? remarks.getResults() : null );
    	return PATH_FORM_VIEW;
    }

    @RequestMapping( value="/form/reply/{id}", produces = "text/html" )
    public String populateReplyForm( @PathVariable(value="id") Long id, Model uiModel ) {
    	ComplaintDto dto = service.getDto( id );
    	populateForm( uiModel, dto );

    	MessageDto msg = new MessageDto();
    	msg.setContactNo( dto.getMobileNo() );
    	msg.setEmailAd( dto.getEmail() );
    	uiModel.addAttribute( UIATTR_REPLY, msg );

    	uiModel.addAttribute( UIATTR_REPLY_EMAIL, MsgType.email );
    	uiModel.addAttribute( UIATTR_REPLY_SMS, MsgType.sms );
    	return PATH_FORM_REPLY;
    }

    @RequestMapping( value="/save", produces="application/json; charset=utf-8", method=RequestMethod.POST )
	public @ResponseBody ControllerResponse save(
			@ModelAttribute(UIATTR_FORM) ComplaintDto dto, 
			BindingResult result ) {

    	ControllerResponse resp = validate( dto, result );
		if ( resp.isSuccess() ) {
			dto = service.saveDto( dto, true );
    		resp.setResult( dto );
    		notifySupervisor( dto );
    		resp.setSuccess( true );
		}
		return resp;
	}

    @RequestMapping( value="/update", produces="application/json; charset=utf-8", method=RequestMethod.POST )
	public @ResponseBody ControllerResponse update(
			@ModelAttribute(UIATTR_FORM) ComplaintDto dto, 
			BindingResult result ) {

    	ControllerResponse resp = validate( dto, result );
		if ( resp.isSuccess() ) {
    		resp.setResult( service.updateDto( dto ) );
    		resp.setSuccess( true );
		}
		return resp;
	}

    @RequestMapping( value="/assign", produces="application/json; charset=utf-8", method=RequestMethod.POST )
	public @ResponseBody ControllerResponse assign(
			@ModelAttribute(UIATTR_FORM) ComplaintDto dto, 
			BindingResult result ) {

    	ControllerResponse resp = validateAssignee( dto, result );
		if ( resp.isSuccess() ) {
    		resp.setResult( service.assignDto( dto ) );
    		resp.setSuccess( true );
		}
		return resp;
	}

    @RequestMapping( value="/member/populate", produces="application/json; charset=utf-8", method=RequestMethod.GET )
	public @ResponseBody ControllerResponse populateMember( 
			@RequestParam(value="mobileNo", required = false) String mobileNo,
			@RequestParam(value="email", required = false) String email ) {

    	ControllerResponse resp = new ControllerResponse();
    	if ( StringUtils.isNotBlank( mobileNo ) && !mobileNo.equalsIgnoreCase( "%MOBILENO%" ) ) {
    		resp.setResult( service.populateMember( mobileNo, ComplaintMemberField.MOBILENO ) );
    	}
    	else if ( StringUtils.isNotBlank( email ) && !email.equalsIgnoreCase( "%EMAIL%" ) ) {
    		resp.setResult( service.populateMember( email, ComplaintMemberField.EMAIL ) );
    	}
		resp.setSuccess( true );
		return resp;
	}

    @RequestMapping(value = "/process/remarks/list/{id}", method = RequestMethod.POST)
    public @ResponseBody ResultList<ApprovalRemarkDto> listRemarks( @PathVariable(value="id") Long id ) {
    	return service.getHistoryDtos( id );
    }

    @RequestMapping( value="/process/remarks/save", produces="application/json; charset=utf-8", method=RequestMethod.POST )
	public @ResponseBody ControllerResponse saveRemarks(
			@ModelAttribute(UIATTR_FORM) ComplaintDto dto, 
			BindingResult result ) {

    	ControllerResponse resp = new ControllerResponse();
		resp.setResult( service.saveRemarks( dto ) );
		resp.setSuccess( true );
		return resp;
	}

    @RequestMapping( value="/process/reply/{id}", produces="application/json; charset=utf-8", method=RequestMethod.POST )
	public @ResponseBody ControllerResponse saveProcess(
			@PathVariable(value="id") Long id,
			@ModelAttribute(UIATTR_REPLY) MessageDto msgDto, 
			BindingResult result ) {

    	ControllerResponse resp = new ControllerResponse();
		ComplaintDto dto = service.getDto( id );
    	if ( StringUtils.isBlank( msgDto.getMessage() ) ) {
    		result.reject( messageSource.getMessage( "complaint_err_emptymessage", null, LocaleContextHolder.getLocale() ) );
    		resp.setResult( result.getAllErrors() );
    	}
    	else {
    		msgDto.setEmailAd( dto.getEmail() );
    		msgDto.setContactNo( dto.getMobileNo() );
    		if ( ( msgDto.getMsgType() == MsgType.email && mailService.send( msgDto ) ) 
    				|| ( msgDto.getMsgType() == MsgType.sms && smsService.send( msgDto ) ) ) {
        		resp.setResult( service.processReply( id, msgDto ) );
    		}
    	}
		resp.setSuccess( !result.hasErrors() );
		return resp;
	}



    private ControllerResponse validate( ComplaintDto dto, BindingResult result ) {
    	if ( StringUtils.isBlank( dto.getFirstName() ) ) {
    		result.reject( messageSource.getMessage( "complaint_err_emptyfname", null, LocaleContextHolder.getLocale() ) );
    	}
    	if ( StringUtils.isBlank( dto.getGenderCode() ) ) {
    		result.reject( messageSource.getMessage( "complaint_err_emptygender", null, LocaleContextHolder.getLocale() ) );
    	}
    	if ( StringUtils.isBlank( dto.getComplaints() ) ) {
    		result.reject( messageSource.getMessage( "complaint_err_emptycomplaints", null, LocaleContextHolder.getLocale() ) );
    	}
    	if ( StringUtils.isBlank( dto.getMobileNo() ) && StringUtils.isBlank( dto.getEmail() ) ) {
    		result.reject( messageSource.getMessage( "complaint_err_emptymobileemail", null, LocaleContextHolder.getLocale() ) );
    	}

    	ControllerResponse resp = new ControllerResponse();
    	if ( result.hasErrors() ) {
    		resp.setResult( result.getAllErrors() );
    	}
    	resp.setSuccess( !result.hasErrors() );
    	return resp;
    }

    private ControllerResponse validateAssignee( ComplaintDto dto, BindingResult result ) {
    	if ( StringUtils.isBlank( dto.getAssignedTo() ) ) {
    		result.reject( messageSource.getMessage( "complaint_err_emptyassignee", null, LocaleContextHolder.getLocale() ) );
    	}
    	ControllerResponse resp = new ControllerResponse();
    	if ( result.hasErrors() ) {
    		resp.setResult( result.getAllErrors() );
    	}
    	resp.setSuccess( !result.hasErrors() );
    	return resp;
    }

    private void populateForm( Model uiModel, ComplaintDto dto ) {
    	populateForm( uiModel, dto, ( null != dto )? dto.getAssignedTo() : null );
    }

    private void populateForm( Model uiModel, ComplaintDto dto, String exceptUser ) {
    	Store store = null;
    	if ( null != dto ) {
        	if ( StringUtils.isNotBlank( dto.getStoreCode() ) ) {
            	store = storeService.getStoreByCode( dto.getStoreCode() );
            	dto.setStoreDesc( store.getName() );
            	uiModel.addAttribute( UIATTR_STORE, store );
        	}
        	if ( StringUtils.isNotBlank( dto.getGenderCode() ) ) {
        		dto.setGenderDesc( lookupService.getDetailByCode( dto.getGenderCode() ).getDescription() );
        	}
        	uiModel.addAttribute( UIATTR_ID, dto.getId() );
    	}
    	else {
    		dto = new ComplaintDto();
        	store = UserUtil.getCurrentUser().getStore();
        	if ( null != store ) {
            	uiModel.addAttribute( UIATTR_STORE, store );
        	}
    	}
    	uiModel.addAttribute( UIATTR_FORM, dto );
    	populateFields( uiModel );
    	populateUsers( uiModel, null, exceptUser ); //null : populate all users irrelevant of store
    }

	private void populateFields( Model uiModel ) {
    	uiModel.addAttribute( UIATTR_PRIORITIES, ComplaintPriority.values() );
    	uiModel.addAttribute( UIATTR_GENDERS, lookupService.getActiveDetailsByHeaderCode( codePropertiesService.getHeaderGender() ) );

    	uiModel.addAttribute( "populateMemberUrl", PATH_POPULATEMEMBER );
    	uiModel.addAttribute( "listUrl", PATH_LIST );
    	uiModel.addAttribute( UIATTR_STATS, service.getStats( null ) );
    	uiModel.addAttribute( UIATTR_ISSUPERVISOR, UserUtil.getCurrentUser().hasAuthority( codePropertiesService.getCssCode() ) );

    	uiModel.addAttribute( UIATTR_PERCOUNT, Complaint.PER_COUNT );
    	uiModel.addAttribute( UIATTR_MAXCOUNT, Complaint.MESSAGE_COUNT );
    	uiModel.addAttribute( UIATTR_COMPLAINTSCOUNT, Complaint.COMPLAINTS_COUNT );
    	uiModel.addAttribute( UIATTR_CATEGORIES, ComplaintCategory.values() );
    }

    private void populateUsers( Model uiModel, Store store, String assignedTo ) {
		uiModel.addAttribute( UIATTR_USERS, userService.findCsByStoreCode( ( null == store || ( null != store && store.getCode()
    			.equalsIgnoreCase( codePropertiesService.getDetailStoreHeadOffice() ) ) ) ? null : store.getCode(), assignedTo ) );
	}

    private void populateSearchForm( Model uiModel ) {
    	uiModel.addAttribute( UIATTR_STORES, storeService.getAllStores() );
    	uiModel.addAttribute( UIATTR_STATS, service.getStats( null ) );
    	uiModel.addAttribute( UIATTR_SEARCHFIELDS, ComplaintSearchField.values() );
    }

    private void notifySupervisor( ComplaintDto dto ) {
    	MessageDto email = new MessageDto();
    	email.setMessage( messageSource.getMessage( "complaint_msg_newalert_msg", new String[]{ dto.getTicketNo() }, LocaleContextHolder.getLocale() ) );
    	List<String> emailAddresses = userService.listSupervisorEmails( dto.getStoreCode(), codePropertiesService.getCssCode() );
    	email.setRecipients( emailAddresses.toArray( new String[ emailAddresses.size() ] ) );
    	email.setSubject( messageSource.getMessage( "complaint_msg_newalert_subject", new String[]{ dto.getTicketNo() }, LocaleContextHolder.getLocale() ) );
    	mailService.send( email );
    }

}
