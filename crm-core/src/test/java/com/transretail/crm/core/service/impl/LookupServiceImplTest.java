package com.transretail.crm.core.service.impl;

import static org.junit.Assert.assertEquals;

import java.io.InputStream;
import java.util.Iterator;
import java.util.Scanner;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.support.TransactionCallback;
import org.springframework.transaction.support.TransactionTemplate;

import com.transretail.crm.core.entity.lookup.LookupHeader;
import com.transretail.crm.core.repo.LookupHeaderRepo;
import com.transretail.crm.core.service.LookupService;

/**
 * @author Allan G. Ramirez (agramirez@exist.com)
 */
@Transactional
@ContextConfiguration(locations = "classpath*:/META-INF/spring/applicationContext*.xml")
@RunWith(SpringJUnit4ClassRunner.class)
public class LookupServiceImplTest {
    @PersistenceContext
    private EntityManager em;
    @Autowired
    private PlatformTransactionManager transactionManager;
    @Autowired
    private LookupHeaderRepo lookupHeaderRepo;
    @Autowired
    private LookupService lookupService;

    @Before
    public void onSetup() {
        setUpLookupDetails();
    }

    @After
    public void tearDown() {
        lookupHeaderRepo.deleteAll();
    }

    @Transactional
    private void setUpLookupDetails() {
        InputStream is1 = Thread.currentThread().getContextClassLoader().getResourceAsStream("scripts/A_LOOKUPHEADERS.sql");
        try {
            final Scanner in1 = new Scanner(is1);
            TransactionTemplate transactionTemplate = new TransactionTemplate(transactionManager);
            transactionTemplate.execute(new TransactionCallback() {
                public Object doInTransaction(TransactionStatus status) {
                    while (in1.hasNext()) {
                        String script = in1.nextLine();
                        if (StringUtils.isNotBlank(script)) {
                            em.createNativeQuery(script).executeUpdate();
                        }
                    }
                    em.clear();
                    em.flush();
                    return null;
                }
            });
        } finally {
            IOUtils.closeQuietly(is1);
        }
    }

    @Test
    public void getMemberConfigurableFields() {
        Iterable<LookupHeader> lookupHeaders = lookupService.getMemberConfigurableFields();
        Iterator<LookupHeader> it = lookupHeaders.iterator();
        for (int i = 1; i <= 12; i++) {
            String code = "CUSTMEMBERFLD";
            String desc = "FIELD";
            if (i < 10) {
                code += "0" + i;
                desc += "0" + i;
            } else {
                code += i;
                desc += i;
            }
            LookupHeader header = it.next();
            assertEquals(code, header.getCode());
            assertEquals(desc, header.getDescription());
        }
    }
}
