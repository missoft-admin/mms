<%@ include file="../../common/taglibs.jsp" %>


<div class="clearfix"></div>
<div id="content_storeGroupView" class="alert alert-info">
  <button type="button" class="close" onclick="$(this).parent().hide();">&times;</button>

  <div id="form_view" class="form-horizontal control-group-01">
    <div class="control-group">
      <label for="store" class="control-label"><spring:message code='label_group_groupname'/></label>
      <label for="store" class="control-label data-content-01 control-data">
        <c:out value="${storeGroup.name}" />
      </label>
    </div>
    <div class="control-group">
      <label for="store" class="control-label"><spring:message code="promotion_prop_targetstoregroups"/></label>
      <label for="store" class="control-label data-content-01 control-data">
        <c:forEach var="store" items="${storeGroup.storeModels}"><div><c:out value="${store.code}" /> - <c:out value="${store.name}" /></div></c:forEach>
      </label>
    </div>
  </div>
</div>