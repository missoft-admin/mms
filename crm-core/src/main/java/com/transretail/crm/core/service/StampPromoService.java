package com.transretail.crm.core.service;


import java.util.List;

import com.transretail.crm.core.dto.StampAccountPortaRestDto;
import com.transretail.crm.core.dto.StampPromoPortaEarnRestDto;
import com.transretail.crm.core.dto.StampPromoPortaVoidRestDto;

import org.joda.time.LocalDate;

import com.transretail.crm.common.service.exception.MessageSourceResolvableException;
import com.transretail.crm.core.dto.StampPromoPortaRedeemRestDto;
import com.transretail.crm.core.dto.StampPromoTxnDto;
import com.transretail.crm.core.entity.MemberModel;
import com.transretail.crm.core.entity.Promotion;
import com.transretail.crm.core.entity.enums.AppKey;


public interface StampPromoService {

    static final String MD5_DATEFORMAT = "yyyyMMdd";

	boolean callPortaCreateStampPromo(Promotion stampPromo);

	boolean callPortaRedeemStampPromo(StampPromoPortaRedeemRestDto portaDto);

    boolean callPortaEarnStampPromo(StampPromoPortaEarnRestDto stampDto);

    boolean callPortaVoidStampPromo(StampPromoPortaVoidRestDto stampDto);

    List<StampPromoTxnDto> retrieveRedeemableItems(String memberId);

	StampPromoTxnDto redeemStamp(MemberModel member, String storeCode, String txnNo, LocalDate redeemDate, Long redeemItemQty, Long promoId, String sku) throws MessageSourceResolvableException;

	boolean callPortaStampPromoWs(Object postData, Object returnObj, AppKey appKey);

    boolean callPortaStampAccount(StampAccountPortaRestDto portaDto);

    boolean isPromoForApproval(AppKey appKey, Long promoId);

}
