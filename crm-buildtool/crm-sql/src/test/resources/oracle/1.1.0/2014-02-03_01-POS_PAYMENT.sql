create table POS_PAYMENT (
        id varchar2(255 char) not null,
        AMOUNT double precision,
        MEDIA_TYPE varchar2(255 char),
        POS_TXN_ID varchar2(255 char),
        primary key (id)
    );

alter table POS_PAYMENT 
        add constraint FK_k4cg4r56dx45db87hbq4tbmuq 
        foreign key (POS_TXN_ID) 
        references POS_TRANSACTION;
