    create table POS_TRANSACTION (
	   ID varchar(255) not null,
	   CUSTOMER_ID varchar(255),
	   START_DATE timestamp,
	   status varchar(255),
	   VAT double,
	   TOTAL_AMOUNT double,
	   TOTAL_DISCOUNT double,
	   TOTAL_NON_MEMBER_MARKUP double,
	   TOTAL_TAXABLE_AMOUNT double,
	   TRANSACTION_DATE timestamp,
	   SALES_DATE timestamp,
	   TYPE varchar(255),
	   VOIDED_DISCOUNT double,
	   STORE_ID integer,
	   primary key (ID)
       );

    create table POS_TX_ITEM (
        ID varchar(255) not null,
        PRODUCT_ID varchar(255),
        QUANTITY double,
        SALES_TYPE varchar(255),
        POS_TXN_ID varchar(255),
        primary key (ID)
    );

    create table PRODUCT (
        PLU_ID varchar(255) not null,
        CATEGORY_ID varchar(255),
        CURRENT_PRICE double,
        DESCRIPTION varchar(255),
        ITEM_CODE varchar(255),
        NAME varchar(255),
        SHORT_DESC varchar(255),
        STORE_CODE varchar(255),
        primary key (PLU_ID)
    );

    create table STORE (
        STORE_ID integer not null,
        CODE varchar(255),
        NAME varchar(255),
        primary key (STORE_ID)
    );

    alter table POS_TRANSACTION
        add constraint FK_3oi1kttjcuxhb7oy5cik3jqh3
        foreign key (STORE_ID)
        references STORE;

    alter table POS_TX_ITEM
        add constraint FK_8e4mb79qx1w3r6u4ll3elhiad
        foreign key (POS_TXN_ID)
        references POS_TRANSACTION;


    create sequence hibernate_sequence;
