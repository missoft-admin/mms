package com.transretail.crm.giftcard.service;

import com.transretail.crm.giftcard.dto.GiftCardDto;
import com.transretail.crm.giftcard.dto.GiftCardResultList;
import com.transretail.crm.giftcard.dto.GiftCardSearchDTO;

/**
 *
 */
public interface GiftCardService {
    Long saveGiftCard(GiftCardDto dto);

    void updateGiftCard(Long giftCardId, GiftCardDto dto);

    GiftCardDto getGiftCard(Long giftCardId);

    GiftCardResultList getGiftCards(GiftCardSearchDTO searchForm);

    void settleGiftCardTransactions();
}
