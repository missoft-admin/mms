package com.transretail.crm.giftcard.entity.support;

/**
 *
 */
public enum CustomerProfileType {
	/*General - all types of SO
	B2B - B2B SO
	Internal - Internal Order*/
	
	GENERAL, B2B, INTERNAL
}
