package com.transretail.crm.schedule.job.impl;


import java.util.Date;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.transretail.crm.core.entity.ApplicationConfig;
import com.transretail.crm.core.entity.enums.AppConfigDefaults;
import com.transretail.crm.core.entity.enums.AppKey;
import com.transretail.crm.core.repo.ApplicationConfigRepo;
import com.transretail.crm.core.service.PointsRewardService;
import com.transretail.crm.schedule.job.service.PointsRewardJobService;
import com.transretail.crm.schedule.trigger.PointsRewardSchedulerTrigger;


@Service("pointsRewardJobSvc")
public class PointsRewardJobServiceImpl implements PointsRewardJobService {

	@Autowired
	PointsRewardService pointsRewardService;
	@Autowired
	ApplicationConfigRepo applicationConfigRepo;

	private static final Logger LOGGER = LoggerFactory.getLogger( PointsRewardJobServiceImpl.class );



	/*
	 * pointsRewardJob-object method referenced 
	 * in applicationContext-scheduler.xml 
	 * */
	@Override
	public void generateRewards() {
		LOGGER.info( ":generateRewards:: job executing at " + new Date().toString()  );

		//ServiceDto dto = pointsRewardService.generatePointsRewardForApproval();
		pointsRewardService.generatePointsRewardForApproval();

		LOGGER.info( ":generateRewards:: job successfully executed at " + new Date().toString()  );

		ApplicationConfig appConfig = applicationConfigRepo.findByKey( AppKey.REWARDS_JOB );
		if ( null == appConfig ) {
			appConfig = new ApplicationConfig();
			appConfig.setValue( AppConfigDefaults.DEFAULT_REWARDS_JOB_CRON );
		}
		appConfig.setKey( AppKey.REWARDS_JOB_DONE );
	}

}
