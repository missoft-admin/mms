   create table DEPARTMENT (
        dept_id varchar2(16 char) not null,
        code varchar2(255 char),
        CREATE_DATE_AUD timestamp,
        description varchar2(255 char),
        UPDATE_DATE_AUD timestamp,
        division_id varchar2(16 char),
        primary key (dept_id)
    );

    create table DIVISION (
        division_id varchar2(16 char) not null,
        code varchar2(255 char),
        CREATE_DATE_AUD timestamp,
        description varchar2(255 char),
        UPDATE_DATE_AUD timestamp,
        primary key (division_id)
    );

     alter table DEPARTMENT 
        add constraint FK_6qk59tsgioehkihaee81ffxqe 
        foreign key (division_id) 
        references DIVISION;