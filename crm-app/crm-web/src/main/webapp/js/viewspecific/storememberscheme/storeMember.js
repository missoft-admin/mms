$(document).ready( function() {

  var $storeMemberForm = $("#form_store_member");
    
  initLinks();
  
  function initLinks() {

    var $linkCreateStoreMember = $( "#link_createStoreMember" );
    $linkCreateStoreMember.click( function(e) {
      e.preventDefault();
      createStoreMember( $linkCreateStoreMember.data( "url" ) );
    });
  }
  
  function createStoreMember( inUrl ) {
      $.ajax({
        cache     : false, 
        dataType  : "html",
        type    : "GET", 
        url     : inUrl,
        error     : function(jqXHR, textStatus, errorThrown) {}, 
        success   : function( inResponse ) {
          $storeMemberForm.html( inResponse );
        }
      });
    }
  
});