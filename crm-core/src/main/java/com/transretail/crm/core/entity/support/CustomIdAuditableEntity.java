package com.transretail.crm.core.entity.support;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;

import org.hibernate.annotations.Parameter;
import org.hibernate.annotations.Type;
import org.joda.time.DateTime;
import org.springframework.data.annotation.CreatedBy;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedBy;
import org.springframework.data.annotation.LastModifiedDate;


@MappedSuperclass
public abstract class CustomIdAuditableEntity<PK extends Serializable> {

	private static final long serialVersionUID = 1L;

	@Id
    private PK id;

	@Column(name = "LAST_UPDATED_DATETIME")
    @LastModifiedDate
    @Type(type = "org.jadira.usertype.dateandtime.joda.PersistentDateTime",
            parameters = { @Parameter(name = "databaseZone", value = "jvm") })
    private DateTime lastUpdated;

    @Column(name = "LAST_UPDATED_BY")
    @LastModifiedBy
    private String lastUpdateUser;

    @Column(name = "CREATED_DATETIME")
    @CreatedDate
    @Type(type = "org.jadira.usertype.dateandtime.joda.PersistentDateTime",
            parameters = { @Parameter(name = "databaseZone", value = "jvm") })
    private DateTime created;

    @Column(name = "CREATED_BY")
    @CreatedBy
    private String createUser;

    public DateTime getLastUpdated() {
        return lastUpdated;
    }

    public void setLastUpdated(DateTime lastUpdated) {
        this.lastUpdated = lastUpdated;
    }

    public String getLastUpdateUser() {
        return lastUpdateUser;
    }

    public void setLastUpdateUser(String lastUpdateUser) {
        this.lastUpdateUser = lastUpdateUser;
    }

    public DateTime getCreated() {
        return created;
    }

    public void setCreated(DateTime created) {
        this.created = created;
    }

    public String getCreateUser() {
        return createUser;
    }

    public void setCreateUser(String createUser) {
        this.createUser = createUser;
    }


    public PK getId() {
		return id;
	}


    public void setId(PK id) {
		this.id = id;
	}

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        CustomIdAuditableEntity that = (CustomIdAuditableEntity) o;

        if (!id.equals(that.id)) return false;

        return true;
    }

    @Override
    public int hashCode() {
        return id.hashCode();
    }
}
