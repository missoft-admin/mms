package com.transretail.crm.schedule.job.service;


import java.util.List;

import com.transretail.crm.core.entity.MarketingChannel;


public interface MarketingChannelJobService {

	List<MarketingChannel> send();

}
