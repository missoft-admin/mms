package com.transretail.crm.giftcard.dto;

import java.util.ArrayList;
import java.util.List;

import org.springframework.util.StringUtils;

import com.mysema.query.types.expr.BooleanExpression;
import com.transretail.crm.common.service.dto.request.AbstractSearchFormDto;
import com.transretail.crm.core.util.BooleanExprUtil;
import com.transretail.crm.giftcard.entity.QCardVendor;

/**
 *
 */
public class CardVendorSearchDto extends AbstractSearchFormDto {
    private String formalName;
    private String contactPersonName;
    private String bankName;
    private String paymentMethod;
    private Boolean enabled;
    private Boolean defaultVendor;
    private String region;

    public String getFormalName() {
        return formalName;
    }

    public void setFormalName(String formalName) {
        this.formalName = formalName;
    }

    public String getContactPersonName() {
        return contactPersonName;
    }

    public void setContactPersonName(String contactPersonName) {
        this.contactPersonName = contactPersonName;
    }

    public String getBankName() {
        return bankName;
    }

    public void setBankName(String bankName) {
        this.bankName = bankName;
    }

    public String getPaymentMethod() {
        return paymentMethod;
    }

    public void setPaymentMethod(String paymentMethod) {
        this.paymentMethod = paymentMethod;
    }

    public Boolean getEnabled() {
        return enabled;
    }

    public void setEnabled(Boolean enabled) {
        this.enabled = enabled;
    }

    public Boolean getDefaultVendor() {
        return defaultVendor;
    }

    public void setDefaultVendor(Boolean defaultVendor) {
        this.defaultVendor = defaultVendor;
    }

    public String getRegion() {
        return region;
    }

    public void setRegion(String region) {
        this.region = region;
    }

    @Override
    public BooleanExpression createSearchExpression() {
        List<BooleanExpression> expressions = new ArrayList<BooleanExpression>();
        QCardVendor qCardVendor = QCardVendor.cardVendor;
        if (StringUtils.hasText(formalName)) {
            expressions.add(BooleanExprUtil.INSTANCE.isStringPropertyLike(qCardVendor.formalName, formalName));
        }
        if (StringUtils.hasText(contactPersonName)) {
            expressions.add(BooleanExprUtil.INSTANCE.isStringPropertyLike(qCardVendor.contactPersonName, contactPersonName));
        }
        if (StringUtils.hasText(bankName)) {
            expressions.add(BooleanExprUtil.INSTANCE.isStringPropertyLike(qCardVendor.bankName, bankName));
        }
        if (StringUtils.hasText(paymentMethod)) {
            // In UI this is dropdown so this should be absolute
            expressions.add(BooleanExprUtil.INSTANCE.isStringPropertyLike(qCardVendor.paymentMethod, paymentMethod));
        }
        if (StringUtils.hasText(region)) {
            expressions.add(BooleanExprUtil.INSTANCE.isStringPropertyLike(qCardVendor.region, region));
        }
        if (enabled != null) {
            expressions.add(qCardVendor.enabled.eq(enabled));
        }
        if (defaultVendor != null) {
            expressions.add(qCardVendor.defaultVendor.eq(defaultVendor));
        }

        return BooleanExpression.allOf(expressions.toArray(new BooleanExpression[expressions.size()]));
    }

}
