<%@ include file="../common/taglibs.jsp" %>

	<c:set var="MODEL_ATTRIBUTE" value="voucherScheme" />
	

	
<div class="modal hide  groupModal nofly" id="wrapperDialog" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4>Create/Update Reward Scheme</h4>
      </div>  
   
     
   
   
      <div class="modal-body">
      
      <form:form id="${MODEL_ATTRIBUTE}" name="${MODEL_ATTRIBUTE}" modelAttribute="${MODEL_ATTRIBUTE}" action="${pageContext.request.contextPath}/voucherscheme" method="POST" enctype="${ENCTYPE}" >
      		<div id="contentError" class="hide alert alert-error">
            	<button type="button" class="close" onclick="$(this).parent().hide();">&times;</button>
                <div><form:errors path="*"/></div>
            </div>
            
        <div class="contentMain">
      	<div class="pull-left span-6">
      	<div>
      		<spring:message code="label_com_transretail_crm_entity_voucherschememodel_valuefrom" var="valueFrom" />
       		<div><label for="valueFrom"><b class="required">*</b> ${valueFrom}</label></div>
			<div><input type="text" name="valueFrom" placeholder="${valueFrom}" class="floatInput"/></div>
		</div>
     	</div>
     	
     	<div class="pull-left span-6">
      	<div>
      		<spring:message code="label_com_transretail_crm_entity_voucherschememodel_valueto" var="valueTo" />
       		<div><label for="valueTo"><b class="required">*</b> ${valueTo}</label></div>
			<div><input type="text" name="valueTo" placeholder="${valueTo}" class="floatInput"/></div>
		</div>
     	</div>
     	
     	<div class="clearfix"></div>
     	
     	<div class="pull-left span-6">
      	<div>
      		<spring:message code="label_com_transretail_crm_entity_voucherschememodel_amount" var="amount" />
       		<div><label for="amount"><b class="required">*</b> ${amount}</label></div>
			<div><input type="text" name="amount" placeholder="${amount}" class="floatInput"/></div>
		</div>
     	</div>
     	
     	<div class="pull-left span-6">
      	<div>
      		<spring:message code="label_com_transretail_crm_entity_voucherschememodel_validperiod" var="validPeriod" />
       		<div><label for="validPeriod"><b class="required">*</b> ${validPeriod}</label></div>
			<div><input type="text" name="validPeriod" placeholder="${validPeriod}" class="intInput"/></div>
		</div>
     	</div>
     	
     	<div class="clearfix"></div>
     	
     	<div class="pull-left span-6">
      	<div>
       		<div><label for="status"><spring:message code="label_com_transretail_crm_entity_voucherschememodel_status" /></label></div>
			<div>
			<select id="status" name="status">
				<c:forEach var="statusType" items="${statusTypes}">
					<option value="${statusType}">${statusType}</option>
				</c:forEach>
			</select>
			</div>
		</div>
     	</div>
     	
     	<div class="clearfix"></div>
     	</div>
     	
      </form:form>
      </div>
      <div class="modal-footer container-btn">
        <button type="button" id="proceed" class="btn btn-primary"><spring:message code="label_save" /></button>
        <button type="button" id="cancelButton" class="btn "><spring:message code="label_cancel"/></button>
      </div>
    </div>
  </div>
</div>

<script src="<spring:url value="/js/jquery/jquery.numberInput.js" />" ></script>
