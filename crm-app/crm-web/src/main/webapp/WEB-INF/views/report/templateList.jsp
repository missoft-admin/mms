<%@include file="/WEB-INF/views/common/taglibs.jsp" %>
<div class="form-horizontal">
  <div class="page-header page-header2"><h1>${reportTemplates[0].name}</h1></div>
  
  <div class="errorMessages error">&nbsp;</div>

  <c:choose>
    <c:when test="${fn:length(reportTemplates) > 1}">
      <div class="control-group">
        <label class="control-label"><spring:message code="member.card.activation.report.form.selecttemplate.label"/></label>

        <div class="controls">
          <select id="templateName">
            <c:forEach items="${reportTemplates}" var="reportTemplate">
              <option value="${reportTemplate.id}">${reportTemplate.name}</option>
            </c:forEach>
          </select>
        </div>
      </div>

    </c:when>
    <c:otherwise>
      <div class="">
          <select id="templateName" style="display: none;">
            <c:forEach items="${reportTemplates}" var="reportTemplate">
              <option value="${reportTemplate.id}">${reportTemplate.name}</option>
            </c:forEach>
          </select>
      </div>
    </c:otherwise>
  </c:choose>
  <div class="control-group">
    <label class="control-label"></label>
    <div class="controls">
      <select name="reportType" id="reportType">
        <c:forEach items="${reportTypes}" var="reportType">
          <option value="${reportType.code}"><spring:message code="report_type_${reportType.code}" /></option>
        </c:forEach>
      </select>
    </div>
  </div>
  

  <fieldset>
    <legend><spring:message code="report.filters.label"/></legend>
    <form id="filters"></form>
    <div class="control-group form-actions">
        <button id="view" type="button" class="btn btn-primary" data-loading-text="Loading..." data-url="${exportUrl}"><spring:message code="view"/></button>
    </div>
  </fieldset>


</div>
<style>
  #templateName {
    width: 30%;
  }
</style>
<jsp:include page="../common/noDataFound.jsp"/>
<script src="<c:url value="/js/bootstrap/bootstrap-datepicker.js"/>"></script>
<link href="<c:url value="/css/bootstrap/datepicker.css" />" rel="stylesheet"/>

<script>
$( document ).ready( function () {
    var templateFilterUrl = "<spring:url value="/report/template/reportTemplateName/filters"/>"
    var templateViewUrl = "<spring:url value="/report/template/reportType/reportTemplateName/view"/>"

    var CONTAINER_ERROR = "#contentError > div";
    var CONTENT_ERROR = "#contentError";
    var $errorMessagesContainer = $('div.errorMessages' );
    var $filters = $( '#filters' );

    var $templateNameSelect = $( '#templateName' ).change(function () {
        var selectedTemplateName = $( 'option:selected', this ).val();

        $.get( templateFilterUrl.replace( 'reportTemplateName', selectedTemplateName ), function ( filterFields ) {
            var hasDate = false;
            var hasMultipleSelect = false;
            var dateClass = [];
            var selects = [];
            $filters.children().remove();

            var htmlString = '';
            for ( var i = 0; i < filterFields.length; i++ ) {
                htmlString += '<div class="control-group">';
                htmlString += '<label class="control-label">' 
			+ (filterFields[i].required ? '<span class="required">*</span> ' : '')
			+ filterFields[i].label + '</label>';
                htmlString += '<div class="controls">';

                if ( filterFields[i].type === 'INPUT' ) {
                    htmlString += '<input name="' + filterFields[i].name + '" type="text" placeHolder="' + filterFields[i].placeHolder + '" />';
                } else if ( filterFields[i].type === 'DROPDOWN' ) {
                    htmlString += '<select name="' + filterFields[i].name + '" placeHolder="' + filterFields[i].placeHolder + '">';
		    htmlString += "<option value='' selected='selected'></option>";
                    var selectValues = filterFields[i].selectValues? filterFields[i].selectValues : filterFields[i].dropdownValues;
                    if ( filterFields[i].selectValues ) {
                        for ( var key in selectValues ) {
                            htmlString += '<option value="' + key + '">' + selectValues[key] + '</option>';
                        }
                    }
                    else if ( filterFields[i].dropdownValues ) {
                        for ( var j = 0; j < selectValues.length; j++ ) {
                            htmlString += '<option value="' + selectValues[j].key + '">' + selectValues[j].value + '</option>';
                        }
                    }
                    htmlString += '</select>';
                } else if ( filterFields[i].type === 'TOGGLE' ) {
                    htmlString += '<input name="' + filterFields[i].name + '" type="checkbox" placeHolder="' + filterFields[i].placeHolder + '" />';
                } else if ( filterFields[i].type === 'DATE' ) {
                    htmlString += addDate("", filterFields[i], dateClass);
                    hasDate = true;
                } else if (filterFields[i].type === 'DATE_FROM') {
                    htmlString += addDate("startDate", filterFields[i], dateClass);
                    hasDate = true;
                } else if (filterFields[i].type === 'DATE_TO') {
                    htmlString += addDate("endDate", filterFields[i], dateClass);
                    hasDate = true;
                } else if (filterFields[i].type === 'SELECT_MULTIPLE') {
                    htmlString += '<select id="values' + filterFields[i].name + '" multiple="multiple" class="pull-left"> ';
                    var selectValues = filterFields[i].selectValues;
                    for ( var key in selectValues ) {
                        htmlString += '<option value="' + key + '">' + selectValues[key] + '</option>';
                    }
                    htmlString += '</select>';
                    htmlString += '<div class="cont-02"> <input id="sendButton' + filterFields[i].name + '" data-name="' + filterFields[i].name + '" type="button" class="btn" value=">>"/>'
                    htmlString += '<input id="returnButton' + filterFields[i].name + '" data-name="' + filterFields[i].name + '" type="button" class="btn" value="<<"/> </div>'
                    htmlString += '<select id="selected' + filterFields[i].name + '" placeHolder="' + filterFields[i].placeHolder + '" multiple="multiple" class="pull-left" />';
                    hasMultipleSelect = true;
                    selects.push(filterFields[i].name);
                }

                htmlString += '</div>';
                htmlString += '</div>';
            }
            $filters.html( htmlString );

            if ( hasDate ) {
                for ( var i = 0; i < dateClass.length; i++ ) {
                	var $dateFilter = $( '.date' + dateClass[i], $filters );
                    if(dateClass[i] === 'YEAR_MONTH'){
                    	$dateFilter.datepicker( {
                            autoclose : true,
                            format : 'MM-yyyy',
                            startView : 2,
                            minViewMode : 1,
			    endDate: $dateFilter.data("allowFutureDates") ? null : new Date() 
                        } );
                    } else if(dateClass[i] === 'YEAR'){
                    	$dateFilter.datepicker( {
                            autoclose : true,
                            format : 'yyyy',
                            startView : 2,
                            minViewMode : 2,
			    endDate: $dateFilter.data("allowFutureDates") ? null : new Date()
                        } );
                    } else if(dateClass[i] === 'PLAIN'){
                    	$dateFilter.datepicker( {
                            autoclose : true,
                            format : 'dd-mm-yyyy',
			    endDate: $dateFilter.data("allowFutureDates") ? null : new Date()
//                          minViewMode : 1
                        } );
                    }
                }

                $(".startDate").change(function() {
                    $(".endDate").datepicker("setStartDate", $(this).datepicker("getDate"));
                });
            }
            
            // TODO: @mco
//             $("select").each(function() { //move empty option on first row and set as default
//             	$('option[value=""]',this)
//             	.prop("selected", "selected")
//             	.prependTo(this);
//             }); 

            // For Groserindo Member Report
          $filters.on( 'change', 'select[name="activityReportType"]', function () {
            var selected = $( 'option:selected', this ).val();
            if ( selected !== 'daily' ) {
              $( 'input[name="dateFrom"]', $filters ).datepicker( 'remove' ).val('');
              $( 'input[name="dateFrom"]', $filters ).datepicker( {
                autoclose : true,
                format : 'dd-mm-yyyy',
                minViewMode : 1
              } )
              $( 'input[name="dateTo"]', $filters ).datepicker( 'remove' ).val('');
              $( 'input[name="dateTo"]', $filters ).datepicker( {
                autoclose : true,
                format : 'dd-mm-yyyy',
                minViewMode : 1
              } )
            } else {
              $( 'input[name="dateFrom"]', $filters ).datepicker( 'remove' ).val('');
              $( 'input[name="dateFrom"]', $filters ).datepicker( {
                autoclose : true,
                format : 'dd-mm-yyyy',
                minViewMode : 0
              } )
              $( 'input[name="dateTo"]', $filters ).datepicker( 'remove' ).val('');
              $( 'input[name="dateTo"]', $filters ).datepicker( {
                autoclose : true,
                format : 'dd-mm-yyyy',
                minViewMode : 0
              } )
            }
          } );
          if ( $( 'select[name="activityReportType"]', $filters ).length > 0 ) {
            $( 'select[name="activityReportType"]', $filters ).change();
          }

            if(hasMultipleSelect) {
                for(var i = 0; i < selects.length; i++) {
                    $('#sendButton' + selects[i]).click(function() {
                        var values = $("select#values" + $(this).data('name') + "").val();
                        var texts = $("select#values" + $(this).data('name') + " option:selected").map(function() {
                            return $(this).text();
                        }).get();

                        var result = $("select#selected" + $(this).data('name'));
                        var resultSelected = result.find( "option" ).map( function(){ return this.value; } ).get();
                        var toSelect = "";
                        for(var j = 0; j < values.length; j++) {
                        	  if ( resultSelected.indexOf( values[j] ) == -1 ) {
                                toSelect += '<option value="' + values[j] + '" data-name="' + $(this).data('name') + '" class="multipleSelectVal">' + texts[j] + '</option>';
                        	  }
                        }
                        //result.empty();
                        result.append(toSelect);
                    });

                    $('#returnButton' + selects[i]).click(function() {
                        $("select#selected" + $(this).data('name') + " option:selected").remove();
                    });

                    var selectname = selects[i];
                    $( "select#values" + selectname ).dblclick( function(e) {
                    	  $.each( $(this).find( "option:selected" ), function() {
                    		    //$( "select#selected" + selectname ).append( $( this ).clone() ); 
                    		    $(this).data( "index", $(this).index() );
                            $( "select#selected" + selectname ).append( $( this ) ); 
                    		});
                    });
                    $( "select#selected" + selectname ).dblclick( function(e) {
                        $.each( $(this).find( "option:selected" ), function() {
                        	  //$( this ).remove(); 
                            $( "select#values" + selectname + " option" ).eq( $(this).data( "index" ) - 0 )
                              .before( $( "<option></option>" ).val( $( this ).val() ).html( $( this ).text() ).data( "index", $(this).index() ) );
                            $( this ).remove(); 
                        });
                    });
                }
            }
        } );
    } ).change();


    var showErrorMessage = function(message) {
        $errorMessagesContainer.html('<div class="alert alert-error">' +
            '<button type="button" class="close" data-dismiss="alert">&times;</button>' +
            message + '</div>');
    };

    $( '#view' ).click( function () {
        $("#view").button("loading");
        var parameters = $filters.serialize();
        var stores = "";
        $(".multipleSelectVal").each(function(key, element) {
            console.log(key + " " + $(element).val());
            stores += $(element).val() + "!";
        });
        if(stores) {
            parameters += "&stores=" + stores;
        }
        console.log(parameters);

        $.post("<c:url value="/report/template/validate" />" + "/" + $( 'option:selected', $templateNameSelect ).val() , parameters, function(errorMessages) {
	  if (errorMessages.length > 0) {
	      var errorInfo = "1. " + errorMessages[0];
	      for ( var i = 1; i < errorMessages.length; i++ ) {
		  errorInfo += "<br>" + (i + 1) + ". " + errorMessages[i];
	      }
	      showErrorMessage(errorInfo);
	      $("#view").button("reset");
	  } else {
	      $( ".alert", $errorMessagesContainer ).alert( 'close' );
	      var selectedTemplateName = $( 'option:selected', $templateNameSelect ).val();
	      var selectedReportType = $("select[name='reportType']").val();
	      var url = templateViewUrl.replace( 'reportTemplateName', selectedTemplateName ).replace('reportType', selectedReportType);
	      var params = parameters;
	      if ( params != null && '' != params.trim() ) {
		  url += "?" + params;
	      }

	      var dataCheck = ctx +"/report/template/" + selectedTemplateName + "/data/check";
	      $.post(dataCheck, parameters, function(noData) {
		if(noData) {
		  $("#noData").modal('show');
		} else {
		  if(selectedReportType == "pdf") {
		    window.open( url, '_blank', 'toolbar=0,location=0,menubar=0' );
		  } else if(selectedReportType == "excel") {
		    window.location.href = url;
		  }
		}
		
		$("#view").button("reset");
	      }, "json");
	  }
        }, "json");
    } );
} );

function addDate(className, filterFields, dateClass) {
    var htmlString = '';
    // for backward compatibilty
    if(!filterFields.dateOption){
        htmlString += '<input class="' + className +' date" name="' + filterFields.name + '" type="text" placeHolder="' + filterFields.placeHolder + '" data-allow-future-dates="'+filterFields.allowFutureDates+'" />';
    } else {
        dateClass.push(filterFields.dateOption);
        htmlString += '<input class="' + className +' date' + filterFields.dateOption +'"name="' + filterFields.name + '" type="text" data-format="yyyy-MM-dd" placeHolder="' + filterFields.placeHolder + '" data-allow-future-dates="'+filterFields.allowFutureDates+'"  />';
    }
    return htmlString;
}
</script>