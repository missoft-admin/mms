package com.transretail.crm.web.controller;


import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.transretail.crm.common.service.dto.response.ResultList;
import com.transretail.crm.core.dto.PointsRewardDto;
import com.transretail.crm.core.dto.PointsRewardSearchDto;
import com.transretail.crm.core.entity.PointsTxnModel;
import com.transretail.crm.core.entity.enums.AppConfigDefaults;
import com.transretail.crm.core.entity.enums.PointTxnType;
import com.transretail.crm.core.entity.enums.TxnStatus;
import com.transretail.crm.core.security.util.UserUtil;
import com.transretail.crm.core.service.CodePropertiesService;
import com.transretail.crm.core.service.PointsRewardService;
import com.transretail.crm.core.service.PointsTxnManagerService;
import com.transretail.crm.web.controller.util.ControllerResponse;


@RequestMapping("/points/recon")
@Controller
public class PointsReconController {

	private static final String PATH_MAIN 				= "/points/recon";

	private static final String UIATTR_STATS			= "statActive";
	private static final String UIATTR_ISSUPERVISOR		= "isCss";



    @Autowired
    private PointsRewardService service;
    @Autowired
    private PointsTxnManagerService pointsTxnManagerService;
    @Autowired
    private CodePropertiesService codePropertiesService;



	@InitBinder
	void initBinder( WebDataBinder binder ) {
		DateFormat theFormat = new SimpleDateFormat( "dd MMM yyyy" );
		theFormat.setLenient( false );
		binder.registerCustomEditor( Date.class, new CustomDateEditor( theFormat, true ) );
	}

    @RequestMapping( produces = "text/html" )
    public String showMain( Model uiModel ) {
    	populateConstants( uiModel );
    	return PATH_MAIN;
    }

    @RequestMapping( value="/generate", produces="application/json; charset=utf-8", method=RequestMethod.GET )
	public @ResponseBody ControllerResponse generate() {
    	ControllerResponse resp = new ControllerResponse();
    	resp.setResult( service.generateReconPoints( null, null, AppConfigDefaults.DEFAULT_REWARDS_JOB_CRON ) );
    	resp.setSuccess( true );
		return resp;
    }

    @RequestMapping(value = "/list", method = RequestMethod.POST)
    public @ResponseBody ResultList<PointsRewardDto> list( @RequestBody PointsRewardSearchDto dto ) {
    	if ( null == dto ) {
    		dto = new PointsRewardSearchDto();
    	}
    	dto.setStatus( TxnStatus.FORAPPROVAL );
    	dto.setTxnTypes( new PointTxnType[]{ PointTxnType.RECON } );
    	return service.searchRecon( dto );
    }

    @RequestMapping( value="/process/{id}", produces="application/json; charset=utf-8", method=RequestMethod.GET )
	public @ResponseBody ControllerResponse process( 
			@PathVariable(value="id") String id,
			@RequestParam(value="status", required=true) TxnStatus status ) {

    	ControllerResponse resp = new ControllerResponse();
    	PointsTxnModel model = pointsTxnManagerService.retrievePointsById( id );
    	if ( null != model ) {
    		model.setStatus( status );
    		resp.setResult( new PointsRewardDto( service.save( model ) ) );
    		resp.setSuccess( true );
    	}
		return resp;
	}



    private void populateConstants( Model uiModel ) {
    	uiModel.addAttribute( UIATTR_STATS, TxnStatus.ACTIVE );
    	uiModel.addAttribute( UIATTR_ISSUPERVISOR, UserUtil.getCurrentUser().hasAuthority( codePropertiesService.getCssCode() ) );
    }
}
