package com.transretail.crm.web.controller;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.sql.SQLException;
import java.util.List;
import java.util.Random;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.io.IOUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.transretail.crm.core.dto.PromoBannerDto;
import com.transretail.crm.core.entity.enums.MemberTypeConstants;
import com.transretail.crm.core.entity.enums.PromoImageType;
import com.transretail.crm.core.security.model.impl.CustomSecurityUserDetailsImpl;
import com.transretail.crm.core.security.util.UserUtil;
import com.transretail.crm.core.service.CodePropertiesService;
import com.transretail.crm.core.service.MemberService;
import com.transretail.crm.core.service.PromoBannerService;
import com.transretail.crm.loyalty.service.LoyaltyCardService;
import com.transretail.crm.web.member.bean.MemberDto;
import com.transretail.crm.web.member.service.MemberServiceExt;

import org.apache.commons.lang3.StringUtils;
import org.joda.time.LocalDate;
import org.joda.time.format.DateTimeFormat;

@Controller
public class IndexController {

    @Autowired
    private PromoBannerService promoBannerService;
    @Autowired
    private MemberServiceExt memberServiceExt;
    
    private static final Random rand = new Random();

    @RequestMapping(value = "/")
    public String index(Model uiModel) {
		final MemberDto memberDto = memberServiceExt.getMemberDto(UserUtil.getCurrentUser().getUsername());
		List<PromoBannerDto> promoBanners = promoBannerService.findCurrentPromoBanners();
		List<PromoBannerDto> promoNews = promoBannerService.findActivePromoImagesByType(PromoImageType.NEWS);
		List<PromoBannerDto> promoAd = promoBannerService.findActivePromoImagesByType(PromoImageType.ADVERTISMENT);
		List<PromoBannerDto> promoProduct = promoBannerService.findActivePromoImagesByType(PromoImageType.PRODUCT);
		
		// Promo Banner Attributes
		final boolean isEmployee = memberDto.getMemberTypeCode().equals(MemberTypeConstants.EMPLOYEE.getValue());
		uiModel.addAttribute("employee", isEmployee);
		uiModel.addAttribute("promoBanners", promoBanners);
		uiModel.addAttribute("hasPromoBanners", !promoBanners.isEmpty());
		uiModel.addAttribute("promoBannerCount", promoBanners.size());
		uiModel.addAttribute("promoNews", !promoNews.isEmpty() ? promoNews.get(0) : null);
		uiModel.addAttribute("hasPromoNews", !promoNews.isEmpty());
		uiModel.addAttribute("promoNewsCount", promoNews.size());
		uiModel.addAttribute("promoAd", promoAd.isEmpty() ? null : promoAd.get(rand.nextInt(promoAd.size())));
		uiModel.addAttribute("hasPromoAd", !promoAd.isEmpty());
		uiModel.addAttribute("promoAdCount", promoAd.size());
		uiModel.addAttribute("promoProducts", promoProduct);
		uiModel.addAttribute("hasPromoProducts", !promoProduct.isEmpty());
		uiModel.addAttribute("promoProductCount", promoProduct.size());
		// Loyalty Card attributes
		uiModel.addAttribute("noLoyaltyCard", !memberDto.hasLoyaltyCard());
		uiModel.addAttribute("isLoyaltyCardExpired", isLoyaltyCardExpired(memberDto));
	
		return "index";
    }

    private boolean isLoyaltyCardExpired(MemberDto memberDto) {
		String temp = memberDto.getLoyaltyCardExpiry();
		if (StringUtils.isBlank(temp)) {
		    return false;
		}
		LocalDate loyaltyCardExpiryDate = LocalDate.parse(temp, DateTimeFormat.forPattern("MM-dd-yyyy"));
		LocalDate now = LocalDate.now();
	
		return now.isEqual(loyaltyCardExpiryDate) || now.isAfter(loyaltyCardExpiryDate);
    }

    @RequestMapping(value = "/image/{id}")
    public void getPromoBannerImage(@PathVariable("id") String id, HttpServletResponse response) {
		PromoBannerDto promoBanner = promoBannerService.findById(id);
	
		if (promoBanner != null) {
		    try {
			writeFileContentToHttpResponse(promoBanner.getImage(), response);
		    } catch (Exception e) {
			e.printStackTrace();
		    }
		}
    }

    private void writeFileContentToHttpResponse(byte[] image, HttpServletResponse response) throws IOException, SQLException {
		if (image != null) {
		    try {
			response.setContentType("image/jpeg");
			ServletOutputStream out = response.getOutputStream();
			IOUtils.copy(new ByteArrayInputStream(image), out);
			out.flush();
		    } catch (Exception e) {
			e.printStackTrace();
		    }
		}
    }
}
