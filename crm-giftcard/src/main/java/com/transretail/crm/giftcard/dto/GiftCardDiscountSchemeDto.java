package com.transretail.crm.giftcard.dto;

import java.math.BigDecimal;

import org.joda.time.LocalDate;
import org.springframework.format.annotation.DateTimeFormat;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.transretail.crm.common.web.converter.LocalDateSerializer;
import com.transretail.crm.giftcard.entity.GiftCardCustomerProfile;
import com.transretail.crm.giftcard.entity.GiftCardDiscountScheme;
import com.transretail.crm.giftcard.entity.support.DiscountType;

/**
 *
 */
public class GiftCardDiscountSchemeDto {
	private Long id;
	private Long customerId;
	private String customerName;
	private BigDecimal minPurchase;
	private BigDecimal startingDiscount;
	private BigDecimal purchaseForMaxDiscount;
	private BigDecimal endingDiscount;
	private boolean readOnly = false;
	private DiscountType discountType;
	private Boolean includeBelowMinPurchase;
	
	@DateTimeFormat(pattern = "dd-MM-yyyy")
	@JsonSerialize(using = LocalDateSerializer.class)
	private LocalDate startDate;
	@DateTimeFormat(pattern = "dd-MM-yyyy")
	@JsonSerialize(using = LocalDateSerializer.class)
	private LocalDate endDate;
	
	public GiftCardDiscountSchemeDto() {}
	
	public GiftCardDiscountSchemeDto(GiftCardDiscountScheme model) {
		this.id = model.getId();
		if(model.getCustomer() != null) {
			this.customerId = model.getCustomer().getId();
			this.customerName = model.getCustomer().getName();
		}
		this.minPurchase = model.getMinPurchase();
		this.startingDiscount = model.getStartingDiscount();
		this.purchaseForMaxDiscount = model.getPurchaseForMaxDiscount();
		this.endingDiscount = model.getEndingDiscount();
		this.startDate = model.getStartDate();
		this.endDate = model.getEndDate();
		this.discountType = model.getDiscountType();
		this.includeBelowMinPurchase = model.getIncludeBelowMinPurchase();
	}
	
	public GiftCardDiscountScheme toModel(GiftCardDiscountScheme model) {
		if(model == null) 
			model = new GiftCardDiscountScheme();
		
		if(this.getCustomerId() != null)
			model.setCustomer(new GiftCardCustomerProfile(this.getCustomerId()));
		model.setMinPurchase(this.minPurchase);
		if(this.discountType.compareTo(DiscountType.REGULAR_YEARLY_DISCOUNT) != 0)
			model.setStartingDiscount(this.startingDiscount);
		else 
			model.setStartingDiscount(this.endingDiscount);
		model.setPurchaseForMaxDiscount(this.purchaseForMaxDiscount);
		model.setEndingDiscount(this.endingDiscount);
		model.setStartDate(this.startDate);
		model.setEndDate(this.endDate);
		model.setDiscountType(this.discountType);
		model.setIncludeBelowMinPurchase(this.includeBelowMinPurchase);
		
		return model;
	}
	
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getCustomerId() {
		return customerId;
	}

	public void setCustomerId(Long customerId) {
		this.customerId = customerId;
	}

	public String getCustomerName() {
		return customerName;
	}
	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}
	public BigDecimal getMinPurchase() {
		return minPurchase;
	}
	public BigDecimal getMinPurchaseFormatted() {
		return minPurchase;
	}
	public void setMinPurchase(BigDecimal minPurchase) {
		this.minPurchase = minPurchase;
	}
	public BigDecimal getStartingDiscount() {
		return startingDiscount;
	}
	public void setStartingDiscount(BigDecimal startingDiscount) {
		this.startingDiscount = startingDiscount;
	}
	public BigDecimal getPurchaseForMaxDiscount() {
		return purchaseForMaxDiscount;
	}
	public BigDecimal getPurchaseForMaxDiscountFormatted() {
		return purchaseForMaxDiscount;
	}
	public void setPurchaseForMaxDiscount(BigDecimal purchaseForMaxDiscount) {
		this.purchaseForMaxDiscount = purchaseForMaxDiscount;
	}
	public BigDecimal getEndingDiscount() {
		return endingDiscount;
	}
	public void setEndingDiscount(BigDecimal endingDiscount) {
		this.endingDiscount = endingDiscount;
	}

	public boolean getReadOnly() {
		return readOnly;
	}

	public void setReadOnly(boolean readOnly) {
		this.readOnly = readOnly;
	}

	public LocalDate getStartDate() {
		return startDate;
	}

	public void setStartDate(LocalDate startDate) {
		this.startDate = startDate;
	}

	public LocalDate getEndDate() {
		return endDate;
	}

	public void setEndDate(LocalDate endDate) {
		this.endDate = endDate;
	}

	public DiscountType getDiscountType() {
		return discountType;
	}

	public void setDiscountType(DiscountType discountType) {
		this.discountType = discountType;
	}

	public Boolean getIncludeBelowMinPurchase() {
		return includeBelowMinPurchase;
	}

	public void setIncludeBelowMinPurchase(Boolean includeBelowMinPurchase) {
		this.includeBelowMinPurchase = includeBelowMinPurchase;
	}
	
	
}
