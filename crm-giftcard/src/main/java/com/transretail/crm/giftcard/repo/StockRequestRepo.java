package com.transretail.crm.giftcard.repo;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.transretail.crm.common.repo.CrmQueryDslPredicateExecutor;
import com.transretail.crm.giftcard.entity.StockRequest;
import com.transretail.crm.giftcard.repo.custom.StockRequestRepoCustom;

/**
 *
 */
@Repository
public interface StockRequestRepo extends CrmQueryDslPredicateExecutor<StockRequest, Long>, StockRequestRepoCustom {
	List<StockRequest> findByRequestNo(String requestNo);
}
