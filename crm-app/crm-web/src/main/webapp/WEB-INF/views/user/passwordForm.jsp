<%@ include file="../common/taglibs.jsp" %>

<div id="content_userForm" class="modal  nofly modal-dialog" style="display: none;">
	<div class="modal-content">
	<form:form id="form" modelAttribute="form" method="POST" cssClass="form-horizontal" style="margin: 0px;">
		<div class="modal-header">
		  <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
			<h4>
			  <spring:message code="menu_user_changepassword"/>
			</h4>

		</div>
		<div class="modal-body">
			<div id="errorcontent" class="alert alert-error">
		      <button type="button" id="hideError" class="close">&times;</button>
		      <div id="errorlist"></div>
		    </div>
			<jsp:include page="profile/passwordPane.jsp" />
		</div>
		<div class="modal-footer">
		  <button id="passwordSubmit" type="button" class="btn btn-primary" data-url="<c:url value="/user/profile/password/change/${id}" />"><spring:message code="button_submit" /></button>
		  <a class="btn " href="#" data-dismiss="modal"><spring:message code="button_cancel" /></a>
		</div>
	</form:form>
	</div>
</div>

<script src="<spring:url value="/js/common/form2js.js" />" ></script>
<script src="<spring:url value="/js/common/json2.js" />" ></script>
<script src="<spring:url value="/js/jquery/jquery.toObject.js" />" ></script>

<script>
$(document).ready(function() {
	$("#content_userForm").modal({show: true});
	$("#errorcontent").hide();
	$("#currentPasswordBlock").hide();

	$("#hideError").click(function() {
		$("#errorlist").clear();
		$("#errorcontent").hide();
	});

	$("#passwordSubmit").click(function() {
		$.ajax({
			dataType 	: "json",
			type 		: "POST",
			data		: $("#form").serialize(),
			url 		: $(this).data("url"),
			error 		: function(jqXHR, textStatus, errorThrown) {},
			success 	: function( response ) {
				if(response.success) {
					$("#content_userForm").modal("hide");
					$("#content_userForm").remove();
				}
				else {
					errorInfo = "1. " + response.result[0];
					var count = 1;
					for (var i = 1; i < response.result.length; i++) {
						count++;
						errorInfo += "<br>" + (count) + ". " + response.result[i];
					}
					$("#errorlist").html(errorInfo);
					$("#errorcontent").show("slow");
				}
			}
		});
	});
});
</script>