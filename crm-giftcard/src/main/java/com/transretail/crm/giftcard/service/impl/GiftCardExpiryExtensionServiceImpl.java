package com.transretail.crm.giftcard.service.impl;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.joda.time.LocalDate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.mysema.query.Tuple;
import com.mysema.query.jpa.impl.JPAQuery;
import com.mysema.query.types.expr.BooleanExpression;
import com.transretail.crm.common.service.dto.response.ResultList;
import com.transretail.crm.common.service.exception.MessageSourceResolvableException;
import com.transretail.crm.common.service.util.SpringDataPagingUtil;
import com.transretail.crm.core.security.util.UserUtil;
import com.transretail.crm.giftcard.dto.GiftCardExpiryExtensionDto;
import com.transretail.crm.giftcard.dto.GiftCardExpiryExtensionSearchDto;
import com.transretail.crm.giftcard.entity.GiftCardExpiryExtension;
import com.transretail.crm.giftcard.entity.GiftCardInventory;
import com.transretail.crm.giftcard.entity.QGiftCardExpiryExtension;
import com.transretail.crm.giftcard.entity.QGiftCardInventory;
import com.transretail.crm.giftcard.entity.SalesOrder;
import com.transretail.crm.giftcard.entity.SalesOrderAlloc;
import com.transretail.crm.giftcard.entity.SalesOrderItem;
import com.transretail.crm.giftcard.entity.support.GiftCardDateExtendStatus;
import com.transretail.crm.giftcard.entity.support.GiftCardInventoryStatus;
import com.transretail.crm.giftcard.repo.GiftCardExpiryExtensionRepo;
import com.transretail.crm.giftcard.repo.GiftCardInventoryRepo;
import com.transretail.crm.giftcard.repo.SalesOrderAllocRepo;
import com.transretail.crm.giftcard.repo.SalesOrderRepo;
import com.transretail.crm.giftcard.service.GiftCardExpiryExtensionService;
import com.transretail.crm.giftcard.service.GiftCardInventoryService;
import com.transretail.crm.giftcard.service.SalesOrderService;

/**
 * @author ftopico
 */
@Service("giftCardExpiryExtensionService")
public class GiftCardExpiryExtensionServiceImpl implements GiftCardExpiryExtensionService {

    @Autowired
    private GiftCardExpiryExtensionRepo repo;
    @Autowired
    private SalesOrderRepo orderRepo;
    @Autowired
    private GiftCardInventoryRepo giftCardInventoryRepo;
    @Autowired
    private SalesOrderAllocRepo salesOrderAllocRepo;
    @Autowired
    private SalesOrderService salesOrderService;
    @Autowired
    private GiftCardInventoryService giftCardInventoryService;
	@PersistenceContext
	private EntityManager em;
    
    @Override
    public ResultList<GiftCardExpiryExtensionDto> searchGiftCardDateExpiryExtensionRequests(
            GiftCardExpiryExtensionSearchDto searchDto) {
        Pageable pageable = SpringDataPagingUtil.INSTANCE.createPageable(searchDto.getPagination());
        BooleanExpression filter = searchDto.createSearchExpression();
        Page<GiftCardExpiryExtension> page = filter != null ? repo.findAll(filter, pageable) : repo.findAll(pageable);
        return new ResultList<GiftCardExpiryExtensionDto>( toDto( page.getContent() ), page.getTotalElements(), page.getNumber(), page.getSize() );
    }

    private Collection<GiftCardExpiryExtensionDto> toDto(
            List<GiftCardExpiryExtension> models) {
        if (CollectionUtils.isEmpty(models)) {
            return new ArrayList<GiftCardExpiryExtensionDto>();
        }
        
        List<GiftCardExpiryExtensionDto> dtos = new ArrayList<GiftCardExpiryExtensionDto>();
        for (GiftCardExpiryExtension model : models) {
            GiftCardExpiryExtensionDto dto = new GiftCardExpiryExtensionDto(model);
            dto.setCreateDateString(dto.getCreateDate().toString("dd-MM-yyyy"));
            if (model.getOrder() != null) {
                dto.setSoSeries(model.getOrder().getOrderNo() + " - " + model.getOrder().getCustomer().getName());
            } else if (model.getSeriesFrom() != null && model.getSeriesTo() != null) {
                dto.setSoSeries(model.getSeriesFrom() + " - " + model.getSeriesTo());
            }
            dtos.add(dto);
        }
        return dtos;
    }

    @Override
    public String createExtendNo() {
        return repo.createExtendNo();
    }

    @Override
    public void saveExpiryExtensionRequest(GiftCardExpiryExtensionDto dto)
            throws MessageSourceResolvableException {
        
        if (dto.getRangeType() == null) {
            throw new MessageSourceResolvableException("gc_date_expiry_error_required_range", null, 
                    "Gift Card range is required");
        } else if (dto.getRangeType().equalsIgnoreCase("so") && StringUtils.isEmpty(dto.getSalesOrder())) {
            throw new MessageSourceResolvableException("gc_date_expiry_error_sales_order", null, 
                    "Sales Order is required");
        } else if (dto.getRangeType().equalsIgnoreCase("series") && dto.getSeriesFrom() == null) {
            throw new MessageSourceResolvableException("gc_date_expiry_error_series_from", null, 
                    "Series From is required");
        } else if (dto.getRangeType().equalsIgnoreCase("series") && dto.getSeriesTo() == null) {
            throw new MessageSourceResolvableException("gc_date_expiry_error_series_to", null, 
                    "Series To is required");
        }
        
        if (dto.getSalesOrder() != null)
            dto.setOrder(salesOrderService.getOrderByOrderNo(dto.getSalesOrder()));
        if (dto.getOrder() != null) {
            if (!salesOrderService.isSalesOrderExist(dto.getOrder().getId())) {
                throw new MessageSourceResolvableException("gc_date_expiry_error_sales_order_not_exist", null, 
                        "Sales order does not exist");
            }

        	QGiftCardInventory gc = QGiftCardInventory.giftCardInventory;
            SalesOrder salesOrder = orderRepo.findOne(dto.getOrder().getId());
            for (SalesOrderItem soItem : salesOrder.getItems()) {
                for (SalesOrderAlloc salesOrderAlloc : soItem.getSoAlloc()) {
                    //GiftCardInventory giftCardInventory = salesOrderAlloc.getGcInventory();
                	if ( null == salesOrderAlloc.getSeriesStart() || null == salesOrderAlloc.getSeriesEnd() ) { continue; }
                	for( Tuple tuple : new JPAQuery( em ).from( gc )
                			.where( gc.series.goe( salesOrderAlloc.getSeriesStart() ).and( gc.series.loe( salesOrderAlloc.getSeriesEnd() ) ) )
                			.list( gc.status, gc.expiryDate, gc.series ) ) {
                        if ( tuple.get( gc.status ) != GiftCardInventoryStatus.ACTIVATED) {
                            throw new MessageSourceResolvableException("gc_date_expiry_error_not_activated", null, 
                                    "Only ACTIVATED gift cards are allowed for expiry extension");
                        }
                        if (dto.getExpiryDate().toDate().before( tuple.get( gc.expiryDate ).toDate() ) ) {
                            SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
                            String dateExpiryRequest = sdf.format(dto.getExpiryDate().toDate());
                            String currentDateExpiry = sdf.format( tuple.get( gc.expiryDate ).toDate());
                            String[] args = {dateExpiryRequest, String.valueOf( tuple.get( gc.series ) ), currentDateExpiry};
                            throw new MessageSourceResolvableException("gc_date_expiry_error_date_expiry_before", args, 
                                    "The requested date expiration " + dateExpiryRequest + " must be after the current date expiration of series "
                                        + String.valueOf( tuple.get( gc.series ) ) + " with date expiration of " + currentDateExpiry);
                        }
                	}
                }
            }
            if (!salesOrderService.isValidForExtension(dto.getOrder().getId())) {
                throw new MessageSourceResolvableException("gc_date_expiry_error_order_invalid", null, 
                        "Cannot extend expiry date: There might be some gift cards under this sales order that has no date expiry indicated");
            }
        } else if (dto.getRangeType().equalsIgnoreCase("so") && (dto.getSalesOrder() == null || dto.getSalesOrder().isEmpty())) {
            throw new MessageSourceResolvableException("gc_date_expiry_error_sales_order_not_exist", null, 
                    "Sales order does not exist");
        }
        
        if (dto.getSeriesFrom() != null && dto.getSeriesTo() != null) {
            for (Long series = dto.getSeriesFrom(); series <= dto.getSeriesTo(); series++) {
                GiftCardInventory giftCardInventory = giftCardInventoryService.getGiftCardBySeries(series);
                if (giftCardInventory == null) {
                    String[] args = {String.valueOf(series)};
                    throw new MessageSourceResolvableException("gc_date_expiry_error_series_not_exist", args, 
                            "Series " + String.valueOf(series) + " does not exist or not yet IN_STOCK status");
                }
                if (giftCardInventory.getStatus() != GiftCardInventoryStatus.ACTIVATED) {
                    throw new MessageSourceResolvableException("gc_date_expiry_error_not_activated", null, 
                            "Only ACTIVATED gift cards are allowed for expiry extension");
                }
                if (dto.getExpiryDate().toDate().before(giftCardInventory.getExpiryDate().toDate())) {
                    SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
                    String dateExpiryRequest = sdf.format(dto.getExpiryDate().toDate());
                    String currentDateExpiry = sdf.format(giftCardInventory.getExpiryDate().toDate());
                    String[] args = {dateExpiryRequest, String.valueOf(giftCardInventory.getSeries()), currentDateExpiry};
                    throw new MessageSourceResolvableException("gc_date_expiry_error_date_expiry_before", args, 
                            "The requested date expiration " + dateExpiryRequest + " must be after the current date expiration of series "
                                + String.valueOf(series) + " with date expiration of " + currentDateExpiry);
                }
                
                if (!giftCardInventoryService.isSeriesValidForExtension(String.valueOf(series))) {
                    String[] args = {String.valueOf(series)};
                    throw new MessageSourceResolvableException("gc_date_expiry_error_series_invalid", args, 
                            "Cannot extend expiry date:Series " + String.valueOf(series) + " does not have an expiry date");
                }
            }
        }
        
        if (!isDateExtensionRequestUnique(dto)){
            throw new MessageSourceResolvableException("gc_date_expiry_error_date_extension_type_exist", null, 
                    "Some gift cards belonging in this request is already part of another request");
        }
        
        if (dto.getHandlingFee() != null && dto.getHandlingFeeType() == null) {
            throw new MessageSourceResolvableException("gc_date_expiry_error_handling_type_required", null, 
                    "Handling Fee type is required if handling fee has a value");
        }
        
        dto.setCreateDate(LocalDate.now());
        dto.setFiledBy(UserUtil.getCurrentUser().getUsername());
        dto.setExtendNo(createExtendNo());
        dto.setStatus(GiftCardDateExtendStatus.CREATED);
        GiftCardExpiryExtension model = toModel(dto);
        
        
        repo.save(model);
    }

    private GiftCardExpiryExtension toModel(GiftCardExpiryExtensionDto dto) {
        GiftCardExpiryExtension model = new GiftCardExpiryExtension();
        model.setCreateDate(dto.getCreateDate());
        model.setExtendNo(dto.getExtendNo());
        model.setFiledBy(dto.getFiledBy());
        if (dto.getOrder() != null) 
            model.setOrder(orderRepo.findOne(dto.getOrder().getId()));
        if (dto.getSeriesFrom() != null)
            model.setSeriesFrom(dto.getSeriesFrom());
        if (dto.getSeriesTo() != null)
            model.setSeriesTo(dto.getSeriesTo());    
        if (dto.getExpiryDate() != null)
            model.setExpiryDate(dto.getExpiryDate());
        model.setStatus(dto.getStatus());
        if (dto.getHandlingFee() != null)
            model.setHandlingFee(dto.getHandlingFee());
        if (dto.getHandlingFeeType() != null)
            model.setHandlingFeeType(dto.getHandlingFeeType());
        
        return model;
    }

    @Override
    public GiftCardExpiryExtensionDto getExpiryExtensionByExtendNo(String extendNo) {
        QGiftCardExpiryExtension qModel = QGiftCardExpiryExtension.giftCardExpiryExtension;
        GiftCardExpiryExtension model = repo.findOne(qModel.extendNo.eq(extendNo));
        return new GiftCardExpiryExtensionDto(model);
    }

    @Transactional
    @Override
    public void rejectRequest(String extendNo) {
        QGiftCardExpiryExtension qModel = QGiftCardExpiryExtension.giftCardExpiryExtension;
        GiftCardExpiryExtension model = repo.findOne(qModel.extendNo.eq(extendNo));
        model.setStatus(GiftCardDateExtendStatus.REJECTED);
        
        repo.save(model);
    }

    @Transactional
    @Override
    public void approveRequest(String extendNo) {
        QGiftCardExpiryExtension qModel = QGiftCardExpiryExtension.giftCardExpiryExtension;
        GiftCardExpiryExtension model = repo.findOne(qModel.extendNo.eq(extendNo));
        model.setStatus(GiftCardDateExtendStatus.APPROVED);
        
        repo.save(model);
        
        if (model.getOrder() != null) {
            SalesOrder salesOrder = orderRepo.findOne(model.getOrder().getId());
            for (SalesOrderItem soItem : salesOrder.getItems()) {
                for (SalesOrderAlloc salesOrderAlloc : soItem.getSoAlloc()) {
//                    salesOrderAlloc.getGcInventory().setExpiryDate(model.getExpiryDate());
//                    salesOrderAllocRepo.save(salesOrderAlloc);
                	QGiftCardInventory gc = QGiftCardInventory.giftCardInventory;
                	Iterable<GiftCardInventory> giftCardInventories = giftCardInventoryRepo.findAll( 
                			gc.series.goe( salesOrderAlloc.getSeriesStart() ).and( gc.series.loe( salesOrderAlloc.getSeriesEnd() ) ) );
                	for( GiftCardInventory giftCardInventory : giftCardInventories ) {
                		giftCardInventory.setExpiryDate(model.getExpiryDate() );
                    	giftCardInventoryRepo.saveAndFlush( giftCardInventory );
                	}
                }
            }
        } else {
            QGiftCardInventory qg = QGiftCardInventory.giftCardInventory;
            for (Long series = model.getSeriesFrom(); series <= model.getSeriesTo(); series++) {
                GiftCardInventory gc = giftCardInventoryRepo.findOne(qg.series.eq(series));
                if (gc != null) {
                    gc.setExpiryDate(model.getExpiryDate());
                    giftCardInventoryRepo.save(gc);
                }
            }
        }
    }

    @Override
    public boolean isDateExtensionRequestUnique(GiftCardExpiryExtensionDto dto) {
        boolean isUnique = true;
        QGiftCardExpiryExtension qModel = QGiftCardExpiryExtension.giftCardExpiryExtension;
        Iterable<GiftCardExpiryExtension> models = new ArrayList<GiftCardExpiryExtension>();
        List<GiftCardDateExtendStatus> set = new ArrayList<GiftCardDateExtendStatus>();
        set.add(GiftCardDateExtendStatus.REJECTED);
        set.add(GiftCardDateExtendStatus.APPROVED);
        if (dto.getOrder() != null) {
            models = repo.findAll(qModel.order.id.eq(dto.getOrder().getId()).and(qModel.status.notIn(set)));
            if (models.iterator().hasNext()) {
                isUnique = false;
            }
        }else if(dto.getSeriesFrom() != null && dto.getSeriesTo() != null) {
            long count1 = repo.count(qModel.seriesTo.between(Long.valueOf(dto.getSeriesFrom()), Long.valueOf(dto.getSeriesTo()))
                                    .and(qModel.status.notIn(set)));
            long count2 = repo.count(qModel.seriesFrom.between(Long.valueOf(dto.getSeriesFrom()), Long.valueOf(dto.getSeriesTo()))
                                    .and(qModel.status.notIn(set)));
            if (count1 > 0 || count2 > 0)
                isUnique = false;
        }
        
        return isUnique;
    }

}
