package com.transretail.crm.core.repo;

import org.springframework.stereotype.Repository;

import com.transretail.crm.common.repo.CrmQueryDslPredicateExecutor;
import com.transretail.crm.core.entity.lookup.Store;

/**
 *
 */
@Repository
public interface StoreRepo extends CrmQueryDslPredicateExecutor<Store, Integer> {
	
	Store findByCode(String code);
}
