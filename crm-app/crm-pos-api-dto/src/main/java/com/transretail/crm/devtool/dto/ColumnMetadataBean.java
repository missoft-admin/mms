package com.transretail.crm.devtool.dto;

import com.transretail.crm.AbstractRestResultBean;
import com.transretail.crm.common.service.exception.MessageSourceResolvableException;

/**
 * @author Allan G. Ramirez (agramirez@exist.com)
 */
public class ColumnMetadataBean extends AbstractRestResultBean {
    private String id;
    // yyyy-mm-dd hh:mm:ss.fff
    private String createdDateTime;

    public ColumnMetadataBean() {

    }

    public ColumnMetadataBean(MessageSourceResolvableException exception) {
        super(exception);
    }

    public ColumnMetadataBean(String id, String createdDateTime) {
        this.id = id;
        this.createdDateTime = createdDateTime;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCreatedDateTime() {
        return createdDateTime;
    }

    public void setCreatedDateTime(String createdDateTime) {
        this.createdDateTime = createdDateTime;
    }

}
