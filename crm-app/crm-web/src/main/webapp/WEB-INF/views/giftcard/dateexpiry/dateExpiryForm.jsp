<%@ include file="../../common/taglibs.jsp" %>

<style type="text/css">
<!--

  #addBtn.btn-small {
    margin-bottom: 5px;
  }
  table select, textarea, table input[type="text"], input[type="password"], input[type="datetime"], input[type="datetime-local"], input[type="date"], input[type="month"], input[type="time"], input[type="week"], input[type="number"], input[type="email"], input[type="url"], input[type="search"], input[type="tel"], input[type="color"], .uneditable-input {
      margin-bottom: 0px;
      margin-top: 0px;
  }
  
-->
</style>

<div id="content_date_expiry" class="modal-dialog">

  <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title"><c:out value="${dialogLabel}"/>:&nbsp<c:out value="${expiryExtensionForm.extendNo}"/></h4>
      </div>
    
      <div class="modal-body control-group-01">
        <form:form id="expiryExtensionForm" modelAttribute="expiryExtensionForm" name="expiryExtensionForm" method="POST" action="/giftcard/dateexpiry" class="form-horizontal" >
          <div id="contentError" class="hide alert alert-error">
            <button type="button" class="close" onclick="$(this).parent().hide();">&times;</button>
            <div><form:errors path="*"/></div>
          </div>
          
          <c:if test="${creation == true}">
            <div class="pull-left">

              <div class="control-group">
                <label class="control-label"><spring:message code="gc_date_expiry_lbl_giftcards"/></label>
                <spring:message code="gc_date_expiry_lbl_sales_order" var="soLabel"/>
                <spring:message code="gc_date_expiry_lbl_range" var="rangeLabel"/>
                <div class="controls">
                <div class="radio">
                  <form:radiobutton path="rangeType" id="radioSalesOrder" value="so" label="${soLabel}" title="${soLabel}"/>
                </div>
                </div>
                
                <div class="controls">
                  <form:input path="salesOrder" id="salesOrder" cssClass="" readonly="true"/>
                </div>
              </div>
              <div class="control-group">
                <div class="controls radio">
                  <form:radiobutton path="rangeType" id="radioSeries" value="series" label="${rangeLabel}" title="${rangeLabel}"/>
                </div>
                <div class="controls">
                  <form:input path="seriesFrom" id="seriesFrom" readonly="true" maxlength="16"/> - <form:input path="seriesTo" id="seriesTo" readonly="true" maxlength="16"/>
                </div>
              </div>
              
              <div class="control-group">
                <label class="control-label"><spring:message code="gc_date_expiry_lbl_expiry_date"/></label>
                <div class="controls">
                  <form:input path="expiryDate" id="expiryDate"/>
                </div>
              </div>
              
              <div class="control-group">
                <label class="control-label"><spring:message code="gc_date_expiry_lbl_handling_fee"/></label>
                <div class="controls">
                  <form:input cssClass="input-mini" path="handlingFee" id="handlingFee"/>
                </div>
              </div>
              
              <div class="control-group">
                <label class="control-label"><spring:message code="gc_date_expiry_lbl_handling_fee_type"/></label>
                <div class="controls">
                  <select id="handlingType" name="handlingFeeType" disabled="disabled" class="">
                    <option value=""></option>
                    <c:forEach var="item" items="${handlingFeeType}">
                      <option value="${item}"><spring:message code="gc_date_expiry_lbl_handling_fee_type_${item}" /></option>
                    </c:forEach>
                  </select>
                </div>  
              </div>
              
            </div>
          </c:if>
          
          <c:if test="${view == true}">
            <div class="pull-left">
              <label class="control-label"><spring:message code="gc_date_expiry_lbl_giftcards"/></label>
              <spring:message code="gc_date_expiry_lbl_sales_order" var="soLabel"/>
              <spring:message code="gc_date_expiry_lbl_range" var="rangeLabel"/>
              <div class="control-group">
                <div class="controls radio">
                  <form:radiobutton path="rangeType" id="radioSalesOrder" value="so" label="${soLabel}" title="${soLabel}" disabled="true"/>
                </div>
                <div class="controls">
                  <form:input path="salesOrder" id="salesOrder" cssClass="" readonly="true"/>
                </div>
              </div>
              <div class="control-group">
                <div class="controls radio">
                  <form:radiobutton path="rangeType" id="radioSeries" value="series" label="${rangeLabel}" title="${rangeLabel}" disabled="true"/>
                </div>
                <div class="controls">
                  <form:input path="seriesFrom" id="seriesFrom" readonly="true" maxlength="16"/> - <form:input path="seriesTo" id="seriesTo" readonly="true" maxlength="16"/>
                </div>
              </div>
              
              <div class="control-group">
                <label class="control-label"><spring:message code="gc_date_expiry_lbl_expiry_date"/></label>
                <div class="controls">
                  <form:input path="expiryDate" id="expiryDate" readonly="true"/>
                </div>
              </div>
              
              <div class="control-group">
                <label class="control-label"><spring:message code="gc_date_expiry_lbl_handling_fee"/></label>
                <div class="controls">
                  <form:input cssClass="input-mini"  path="handlingFee" id="handlingFee" readonly="true"/>
                </div>
                <label class="control-label"><spring:message code="gc_date_expiry_lbl_handling_fee_type"/></label>
                <form:select id="handlingType" path="handlingFeeType" disabled="true" class="controls">
                  <option value=""></option>
                  <c:forEach var="item" items="${handlingFeeType}">
                    <form:option value="${item}"><spring:message code="gc_date_expiry_lbl_handling_fee_type_${item}" /></form:option>
                  </c:forEach>
                </form:select>
              </div>
            </div>
          </c:if>
        </form:form>
      </div>

      <div class="modal-footer">
        <c:if test="${creation == true}">
          <button type="button" id="save" class="btn btn-primary"><spring:message code="label_save"/></button>
        </c:if>
        <c:if test="${approver == true}">
          <button type="button" id="approve" class="btn btn-primary"><spring:message code="label_approve"/></button>
          <button type="button" id="reject" class="btn btn-primary"><spring:message code="label_reject"/></button>
        </c:if>
        <button type="button" id="" class="btn btn-default" data-dismiss="modal"><spring:message code="label_cancel"/></button>
      </div>
  </div>
  
</div>

<script src="<spring:url value="/js/jquery/jquery.numberInput.js" />" ></script>

<script type="text/javascript">
$(document).ready(function() {
  var CONTENT_ERROR = $("#contentError");
  var form = $("#expiryExtensionForm");
  initButtons();
  initForms();
  
  function initForms() {
    
    $("#seriesTo").numberInput("type", "int");
    $("#seriesFrom").numberInput("type", "int");
    $("#handlingFee").numberInput("type", "int");
    
    $("#handlingFee").on("input", function(){
      if ($("#handlingFee").val() != "") {
		$("#handlingType").removeAttr("disabled");
      } else {
        $("#handlingType").attr("disabled", "disabled");
        $("#handlingType option:first").attr('selected','selected');
      }
    });
    
    $("#radioSalesOrder").click(function() {
      $("#salesOrder").attr("readOnly", false);
      $("#seriesFrom").attr("readOnly", true);
      $("#seriesTo").attr("readOnly", true);
      $("#seriesFrom").val("");
      $("#seriesTo").val("");
    });
    
    $("#radioSeries").click(function() {
      $("#salesOrder").attr("readOnly", true);
      $("#seriesFrom").attr("readOnly", false);
      $("#seriesTo").attr("readOnly", false);
      $("#salesOrder").val("");
  	});
    
    $("#expiryDate").datepicker({
      autoclose : true,
      format : "dd-mm-yyyy",
    }).on('changeDate', function(selected){
      expiryDate = new Date(selected.date.valueOf());
      expiryDate.setDate(expiryDate.getDate(new Date(selected.date.valueOf())));
    });
  }

  function initButtons() {
    $("#save").click(function() {
      var urlAction = ctx + form.attr("action") + "/save";
      var dataObject = form.serialize();
      $.post(
          urlAction,
          dataObject,
          function(response) {
            if (response.success) {
              location.reload();
            } else {
              var theErrorInfo = "";
              for (i=0; i < response.result.length; i++) {
                theErrorInfo += (i + 1) + ". " 
                					+ (response.result[i].code != undefined ? response.result[i].code : response.result[i])
        						    +"<br>";
        			}
              CONTENT_ERROR.find( "div" ).html( theErrorInfo );
              CONTENT_ERROR.show( "slow" );
            }
          }
      );
    });
    
    $("#approve").click(function(e) {
      var urlAction = ctx + form.attr("action") + "/approve/" + "${expiryExtensionForm.extendNo}";
      $.post(
          urlAction,
          function(response) {
            if (response.success) {
              location.reload();
            } else {
              var theErrorInfo = "";
              for (i=0; i < response.result.length; i++) {
                theErrorInfo += (i + 1) + ". " 
                					+ (response.result[i].code != undefined ? response.result[i].code : response.result[i])
        						    +"<br>";
        			}
              CONTENT_ERROR.find( "div" ).html( theErrorInfo );
              CONTENT_ERROR.show( "slow" );
            }
          }
      );
    });
    
    $("#reject").click(function() {
      var urlAction = ctx + form.attr("action") + "/reject/" + "${expiryExtensionForm.extendNo}";
      $.post(
          urlAction,
          function(response) {
            if (response.success) {
              location.reload();
            } else {
              var theErrorInfo = "";
              for (i=0; i < response.result.length; i++) {
                theErrorInfo += (i + 1) + ". " 
                					+ (response.result[i].code != undefined ? response.result[i].code : response.result[i])
        						    +"<br>";
        			}
              CONTENT_ERROR.find( "div" ).html( theErrorInfo );
              CONTENT_ERROR.show( "slow" );
            }
          }
      );
    });
  }

});
</script>