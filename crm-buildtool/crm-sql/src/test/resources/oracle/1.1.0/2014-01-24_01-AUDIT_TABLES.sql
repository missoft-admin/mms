create table CRM_CITY_AUD (
        id number(19,0) not null,
        REV number(19,0) not null,
        REVTYPE number(3,0),
        NAME varchar2(255 char),
        PROVINCE number(19,0),
        primary key (id, REV)
    );

create table CRM_PRODUCT_GRP_AUD (
        id number(19,0) not null,
        REV number(19,0) not null,
        REVTYPE number(3,0),
        BRAND varchar2(255 char),
        EXC_PRODUCTS varchar2(4000 char),
        NAME varchar2(255 char),
        PRODUCTS varchar2(4000 char),
        CLASSIFICATION varchar2(15 char),
        SUBCLASSIFICATION varchar2(15 char),
        primary key (id, REV)
    );

create table CRM_PROVINCE_AUD (
        id number(19,0) not null,
        REV number(19,0) not null,
        REVTYPE number(3,0),
        NAME varchar2(255 char),
        primary key (id, REV)
    );

alter table CRM_CITY_AUD 
        add constraint FK_fvelyjb0qdnpskfom6b0dpm8t 
        foreign key (REV) 
        references CRM_REVINFO;

alter table CRM_PRODUCT_GRP_AUD 
        add constraint FK_5smpic5f2ar1y9x415k04hf2v 
        foreign key (REV) 
        references CRM_REVINFO;

alter table CRM_PROVINCE_AUD 
        add constraint FK_qhh3ctxcw0dhrxvpc2k681j1w 
        foreign key (REV) 
        references CRM_REVINFO;