package com.transretail.crm.giftcard.entity;

import java.io.Serializable;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.annotations.Type;
import org.joda.time.DateTime;
import org.joda.time.LocalDate;

import com.transretail.crm.core.entity.support.CustomAuditableEntity;
import com.transretail.crm.giftcard.entity.support.GiftCardOrderStatus;

@Entity
@Table(name = "CRM_GC_ORD")
public class GiftCardOrder extends CustomAuditableEntity<Long> implements Serializable {

	private static final long serialVersionUID = 1L;

	@Column(name = "PO_NO")
	private String poNumber;

	@Column(name = "PO_DATE")
	@Type(type = "org.jadira.usertype.dateandtime.joda.PersistentLocalDate")
	private LocalDate poDate;

	@Column(name = "MO_NO")
	private String moNumber;

	@Column(name = "MO_DATE")
	@Type(type = "org.jadira.usertype.dateandtime.joda.PersistentLocalDate")
	private LocalDate moDate;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "VENDOR", nullable = false)
	private CardVendor vendor;

	@OneToMany(mappedBy = "order", orphanRemoval = true, fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	private List<GiftCardOrderItem> items;

	@Column(name = "STATUS", length = 15, nullable = false)
	@Enumerated(EnumType.STRING)
	private GiftCardOrderStatus status;

	@Column(name = "APPROVAL_DATE")
	@Type(type = "org.jadira.usertype.dateandtime.joda.PersistentDateTime")
	private DateTime approvalDate;

	@Column(name = "APPROVED_BY")
	private String approvedBy;

	@Column(name = "IS_EGC", length = 1)
	@Type(type = "yes_no")
	private Boolean isEgc;

	public GiftCardOrder() {

	}

	public GiftCardOrder(Long id) {
		setId(id);
	}

	public String getPoNumber() {
		return poNumber;
	}

	public void setPoNumber(String poNumber) {
		this.poNumber = poNumber;
	}

	public LocalDate getPoDate() {
		return poDate;
	}

	public void setPoDate(LocalDate poDate) {
		this.poDate = poDate;
	}

	public String getMoNumber() {
		return moNumber;
	}

	public void setMoNumber(String moNumber) {
		this.moNumber = moNumber;
	}

	public LocalDate getMoDate() {
		return moDate;
	}

	public void setMoDate(LocalDate moDate) {
		this.moDate = moDate;
	}

	public CardVendor getVendor() {
		return vendor;
	}

	public void setVendor(CardVendor vendor) {
		this.vendor = vendor;
	}

	public List<GiftCardOrderItem> getItems() {
		return items;
	}

	public void setItems(List<GiftCardOrderItem> items) {
		this.items = items;
	}

	public GiftCardOrderStatus getStatus() {
		return status;
	}

	public void setStatus(GiftCardOrderStatus status) {
		this.status = status;
	}

	public DateTime getApprovalDate() {
		return approvalDate;
	}

	public void setApprovalDate(DateTime approvalDate) {
		this.approvalDate = approvalDate;
	}

	public String getApprovedBy() {
		return approvedBy;
	}

	public void setApprovedBy(String approvedBy) {
		this.approvedBy = approvedBy;
	}

	public Boolean getIsEgc() {
		return isEgc;
	}

	public void setIsEgc(Boolean isEgc) {
		this.isEgc = isEgc;
	}

}
