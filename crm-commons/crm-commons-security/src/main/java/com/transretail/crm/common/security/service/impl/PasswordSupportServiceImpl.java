/*
 * Copyright (c) 2012.
 * All rights reserved.
 */

package com.transretail.crm.common.security.service.impl;

import org.apache.commons.lang3.RandomStringUtils;
import org.jasypt.util.password.PasswordEncryptor;
import org.jasypt.util.password.StrongPasswordEncryptor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.transretail.crm.common.security.service.PasswordSupportService;

public class PasswordSupportServiceImpl implements PasswordSupportService {

    private static final Logger _LOG = LoggerFactory.getLogger(PasswordSupportServiceImpl.class);

    private static final int DEFAULT_MIN_PASSWORD_LENGTH = 10;

    private int minPasswordLength = DEFAULT_MIN_PASSWORD_LENGTH;
    private boolean sanitizePassword = true;
    private PasswordEncryptor passwordEncryptor = new StrongPasswordEncryptor();

    @Override
    public String randomizePassword() {
        return RandomStringUtils.randomAlphanumeric(minPasswordLength);
    }

    @Override
    public String encodePassword(String rawPass) {
        return encodePassword(rawPass, null);
    }

    @Override
    public boolean isPasswordValid(String encPass, String rawPass) {
        return isPasswordValid(encPass, rawPass, null);
    }

    @Override
    public String encodePassword(String rawPass, Object salt) {
        // Ignore salt
        return passwordEncryptor.encryptPassword(sanitizePassword(rawPass));
    }

    @Override
    public boolean isPasswordValid(String encPass, String rawPass, Object salt) {
        // Ignore salt
        return passwordEncryptor.checkPassword(sanitizePassword(rawPass), encPass);
    }

    private String sanitizePassword(String rawPass) {
        if (sanitizePassword) {
            // Just trim beginning and trailing spaces
            return rawPass == null ? "" : rawPass.trim();
        } else {
            return rawPass;
        }
    }

    public void setMinPasswordLength(int minPasswordLength) {
        this.minPasswordLength = minPasswordLength;
    }

    public void setSanitizePassword(boolean sanitizePassword) {
        this.sanitizePassword = sanitizePassword;
    }

    public void setPasswordEncryptor(PasswordEncryptor passwordEncryptor) {
        this.passwordEncryptor = passwordEncryptor;
    }
}
