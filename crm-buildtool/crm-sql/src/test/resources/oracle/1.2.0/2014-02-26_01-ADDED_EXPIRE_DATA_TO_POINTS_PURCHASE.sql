alter table CRM_POINTS add EXPIRE_EXCESS double precision;
alter table CRM_POINTS add EXPIRY_DATE date;
alter table CRM_POINTS add LAST_REDEEM varchar2(255 char);

alter table CRM_EMPLOYEE_PURCHASE_TXN add EXPIRE_EXCESS double precision;
alter table CRM_EMPLOYEE_PURCHASE_TXN add EXPIRY_DATE date;
alter table CRM_EMPLOYEE_PURCHASE_TXN add LAST_REDEEM varchar2(255 char);