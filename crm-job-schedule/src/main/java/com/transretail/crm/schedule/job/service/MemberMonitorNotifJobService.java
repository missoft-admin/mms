package com.transretail.crm.schedule.job.service;

public interface MemberMonitorNotifJobService {
	void sendNotification();
}
