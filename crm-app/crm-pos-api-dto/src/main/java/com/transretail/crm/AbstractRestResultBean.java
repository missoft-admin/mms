package com.transretail.crm;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.transretail.crm.common.service.exception.MessageSourceResolvableException;

/**
 * @author Allan G. Ramirez (agramirez@exist.com)
 */
@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
public abstract class AbstractRestResultBean {
    private MessageSourceResolvableException exception;

    public AbstractRestResultBean() {

    }

    protected AbstractRestResultBean(MessageSourceResolvableException exception) {
        this.exception = exception;
    }

    public MessageSourceResolvableException getException() {
        return exception;
    }

    public void setException(MessageSourceResolvableException exception) {
        this.exception = exception;
    }
}
