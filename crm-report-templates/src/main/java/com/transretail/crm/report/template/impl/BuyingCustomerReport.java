package com.transretail.crm.report.template.impl;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.google.common.collect.Sets;
import com.mysema.query.jpa.impl.JPAQuery;
import com.transretail.crm.common.reporting.jasper.ClasspathFileResolver;
import com.transretail.crm.common.reporting.jasper.DefaultJRProcessor;
import com.transretail.crm.common.reporting.jasper.JRProcessor;
import com.transretail.crm.common.util.DateUtil;
import com.transretail.crm.common.util.IOUtils;
import com.transretail.crm.core.entity.enums.ReportType;
import com.transretail.crm.core.entity.lookup.QStore;
import com.transretail.crm.report.template.CommonReportFilter;
import com.transretail.crm.report.template.FilterField;
import com.transretail.crm.report.template.NoDataFoundSupport;
import com.transretail.crm.report.template.ReportTemplate;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import org.apache.commons.lang3.StringUtils;
import org.joda.time.DateTime;
import org.joda.time.LocalDate;
import org.joda.time.LocalDateTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.sql.DataSource;
import java.sql.*;
import java.sql.Date;
import java.util.*;

/**
 * @author Mike de Guzman
 */
@Service("buyingCustomerReport")
public class BuyingCustomerReport implements ReportTemplate, NoDataFoundSupport {

    private static Logger LOG = LoggerFactory.getLogger(MaritalStatusContributionReport.class);

    protected static final String TEMPLATE_NAME = "Buying Customer Weekly and Monthly Report";
    protected static final String REPORT_NAME_BUYING_CUSTOMER = "reports/buying_customer_report.jasper";

    protected static final String FILTER_STANDARD_REPORT_TYPE = "standardReportType";
    protected static final String FILTER_START_WEEK = "startWeek";
    protected static final String BUYING_CUSTOMER_REPORT_TYPE_WEEKLY = "weekly";
    protected static final String BUYING_CUSTOMER_REPORT_TYPE_MONTHLY = "monthly";
    protected static final String PARAM_MONTH_FROM = "monthFrom";
    protected static final String PARAM_MONTH_TO = "monthTo";
    protected static final String FILTER_STORE = "store";

    @PersistenceContext
    private EntityManager em;

    @Autowired
    private DataSource dataSource;

    @Autowired
    private MessageSource messageSource;

    @Override
    public String getName() {
        return TEMPLATE_NAME;
    }

    @Override
    public String getDescription() {
    	return messageSource.getMessage("buying.customer.report.label", (Object[]) null, LocaleContextHolder.getLocale());
    }

    @Override
    public Set<FilterField> getFilters() {
        Locale locale = LocaleContextHolder.getLocale();
        LinkedHashSet<FilterField> filters = Sets.newLinkedHashSet();
        Map<String, String> standardReportTypes = Maps.newLinkedHashMap();
        standardReportTypes.put(BUYING_CUSTOMER_REPORT_TYPE_WEEKLY,
                messageSource.getMessage("report.weekly", (Object[]) null, LocaleContextHolder.getLocale()));
        standardReportTypes.put(BUYING_CUSTOMER_REPORT_TYPE_MONTHLY,
                messageSource.getMessage("report.monthly", (Object[]) null, LocaleContextHolder.getLocale()));

        filters.add(FilterField.createDropdownField(FILTER_STANDARD_REPORT_TYPE,
                messageSource.getMessage("report.period", (Object[]) null, LocaleContextHolder.getLocale()), standardReportTypes));
        boolean required = true;

        filters.add(FilterField.createDateField(CommonReportFilter.DATE_FROM,
                messageSource.getMessage("report.misc.datefrom", null, LocaleContextHolder.getLocale()),
                DateUtil.SEARCH_DATE_FORMAT, required));
        filters.add(FilterField.createDateField(CommonReportFilter.DATE_TO,
                messageSource.getMessage("report.misc.dateto", null, LocaleContextHolder.getLocale()),
                DateUtil.SEARCH_DATE_FORMAT, required));

        QStore store = QStore.store;
        Map<String, String> stores = Maps.newLinkedHashMap();
        stores.put("", "");
        stores.putAll(new JPAQuery(em).from(store).distinct().orderBy(
                store.code.asc()).map(store.code, store.code.concat(" - ").concat(store.name)));

        filters.add(FilterField.createDropdownField(
                FILTER_STORE, messageSource.getMessage("label_store", (Object[]) null, LocaleContextHolder.getLocale()), stores));

        return filters;
    }

    @Override
    public boolean canView(Set<String> permissions) {
        return true;
    }

    @Override
    public JRProcessor createJrProcessor(Map<String, String> map) {
        LocalDate dateFrom = parseDate(map.get(CommonReportFilter.DATE_FROM));
        LocalDate dateTo = parseDate(map.get(CommonReportFilter.DATE_TO));

        // logic to put dataset into reportParams
        DefaultJRProcessor jrProcessor = new DefaultJRProcessor(REPORT_NAME_BUYING_CUSTOMER);
        jrProcessor.setFileResolver(new ClasspathFileResolver("reports"));
        Map<String, Object> reportParams = prepareReportParameters(dateFrom, dateTo);

        jrProcessor.addParameter("SUB_DATA_SOURCE", getDataSource(map));
        jrProcessor.addParameters(reportParams);
        String standardReportType = map.get(FILTER_STANDARD_REPORT_TYPE);
        if (standardReportType.equalsIgnoreCase(BUYING_CUSTOMER_REPORT_TYPE_WEEKLY)) {
            jrProcessor.addParameter("REPORT_TITLE", messageSource.getMessage("buying.customer.report.label.weekly", null, LocaleContextHolder.getLocale()));
        } else if (standardReportType.equalsIgnoreCase(BUYING_CUSTOMER_REPORT_TYPE_MONTHLY)) {
            jrProcessor.addParameter("REPORT_TITLE", messageSource.getMessage("buying.customer.report.label.monthly", null, LocaleContextHolder.getLocale()));
        } else {
            jrProcessor.addParameter("REPORT_TITLE", messageSource.getMessage("buying.customer.report.label.monthly", null, LocaleContextHolder.getLocale()));
        }
        return jrProcessor;
    }

    public Map <String, Object> prepareReportParameters(LocalDate dateFrom, LocalDate dateTo) {
        Map<String, Object> reportMap = Maps.newHashMap();
        Locale locale = LocaleContextHolder.getLocale();
        reportMap.put("DATE_FROM", dateFrom.toString(INPUT_DATE_PATTERN));
        reportMap.put("DATE_TO", dateTo.toString(INPUT_DATE_PATTERN));
        return reportMap;
    }

    private LocalDate parseDate(String dateInstance) {
        return INPUT_DATE_PATTERN.parseLocalDate(dateInstance);
    }

    private JRBeanCollectionDataSource getDataSource(Map<String, String> map) {

        String standardReportType = map.get(FILTER_STANDARD_REPORT_TYPE);

        String storeCode = map.get(CommonReportFilter.STORE);
        LocalDate dateFrom = parseDate(map.get(CommonReportFilter.DATE_FROM));
        LocalDate dateTo = parseDate(map.get(CommonReportFilter.DATE_TO));

        List<BuyingCustomerReportBean> result = Lists.newArrayList();
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;

        try {
            StringBuilder builder = new StringBuilder("SELECT b.customer_number,"); // Customer ID
            builder.append("b.customer_name,");// Customer Name
            builder.append("c.company_name,");// Business Name
            builder.append("d.description,");// Segmentation
            builder.append("c.contact,"); // Mobile Phone
            builder.append("sum(DECODE(TYPE,'RETURN',-1,'REFUND',-1,1)*a.total_amount - a.total_discount + a.voided_discount - abs(a.rounding_amount)),"); // Total Amount
            builder.append("sum(DECODE(TYPE,'RETURN',-1,'REFUND',-1,1)*a.tariff),");// Amount VAT
            builder.append("sum(DECODE(TYPE,'RETURN',-1,'REFUND',-1,1)*a.vat),"); // PPN/Total VAT
            builder.append("sum(DECODE(TYPE,'RETURN',-1,'REFUND',-1,1)*a.total_non_member_markup),");// Amount Markup
            builder.append("sum((DECODE(TYPE,'RETURN',-1,'REFUND',-1,1)*a.total_non_member_markup) - (DECODE(TYPE,'RETURN',-1,'REFUND',-1,1)*a.total_non_member_markup/1.1)),"); // PPN/Total Markup
            builder.append("count(b.customer_number),"); // Freq Visit
            builder.append("c.created_datetime,");// Registration Date
            builder.append("max(a.transaction_date) ");// Last Visit Date
            builder.append("FROM POS_TRANSACTION a ");
            builder.append("LEFT JOIN tax_invoice b ON b.pos_txn_id = a.id ");
            builder.append("left join crm_member c on b.customer_number=c.account_id ");
            builder.append("left join crm_ref_lookup_dtl d on d.code=c.customer_segregation ");
            if (StringUtils.isNotBlank(storeCode)) {
                builder.append("left join store e on a.store_id = e.store_id ");
            }
            builder.append("WHERE a.type in (?, ?, ?) AND a.status = ? ");
            builder.append("AND a.CUSTOMER_ID IS NOT NULL ");
            builder.append("AND b.customer_number IS NOT NULL ");
            builder.append("AND a.id LIKE ? ");
            builder.append("AND a.sales_date between ? and ? ");
            if (StringUtils.isNotBlank(storeCode)) {
                builder.append("and e.store_code = ? ");
            }
            builder.append("GROUP BY b.customer_number, b.customer_name, c.company_name, d.description, c.contact, c.business_phone, c.home_phone, c.created_datetime ");
            builder.append("ORDER BY b.customer_number");

            con = dataSource.getConnection();
            ps = con.prepareStatement(builder.toString(), ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_READ_ONLY);
            ps.setString(1, "SALE");
            ps.setString(2, "REFUND");
            ps.setString(3, "RETURN");
            ps.setString(4, "COMPLETED");
            ps.setString(5, "2000%");
            ps.setTimestamp(6, new Timestamp(dateFrom.toDate().getTime()));
            ps.setTimestamp(7, new Timestamp(dateTo.toDate().getTime()));
            rs = ps.executeQuery();
            if (StringUtils.isNotBlank(storeCode)) {
                ps.setString(8, storeCode);
            }
            rs = ps.executeQuery();

            while (rs.next()) {
                BuyingCustomerReportBean bean = new BuyingCustomerReportBean();
                bean.accountNumber = rs.getString(1);
                bean.customerName = rs.getString(2);
                bean.businessName = rs.getString(3);
                bean.customerGroup = rs.getString(4);
                bean.contact = rs.getString(5);
                bean.totalSales = rs.getDouble(6);
                bean.totalTaxableAmount = rs.getDouble(7);
                bean.ppn = rs.getDouble(8);
                bean.totalMarkup = rs.getDouble(9);
                bean.ppnMarkup = rs.getDouble(10);
                bean.visitCount = rs.getLong(11);
                Date regDate = rs.getDate(12);
                if (regDate != null) {
                    bean.regDate = new DateTime(regDate.getTime());
                }
                Date lastVisitDate = rs.getDate(13);
                if (lastVisitDate != null) {
                    bean.lastVisitDate = new LocalDateTime(lastVisitDate.getTime());
                }
                result.add(bean);
            }
        } catch (SQLException e) {
            LOG.error("SQL Error: {}", e);
        } finally {

            if (rs != null) {
                IOUtils.INSTANCE.close(rs);
            }
            if (ps != null) {
                IOUtils.INSTANCE.close(ps);
            }
            if (con != null) {
                IOUtils.INSTANCE.close(con, true);
            }
        }
        return new JRBeanCollectionDataSource(result);
    }

    @Override
    public ReportType[] getReportTypes() {
        return ReportType.values();
    }

    @Override
    public boolean isEmpty(Map<String, String> map) {
        String standardReportType = map.get(FILTER_STANDARD_REPORT_TYPE);
        String storeCode = map.get(CommonReportFilter.STORE);
        LocalDate dateFrom = parseDate(map.get(CommonReportFilter.DATE_FROM));
        LocalDate dateTo = parseDate(map.get(CommonReportFilter.DATE_TO));

        List<BuyingCustomerReportBean> result = Lists.newArrayList();
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;

        try {
            StringBuilder builder = new StringBuilder("SELECT b.customer_number,"); // Customer ID
            builder.append("b.customer_name,");// Customer Name
            builder.append("c.company_name,");// Business Name
            builder.append("d.description,");// Segmentation
            builder.append("c.contact,"); // Mobile Phone
            builder.append("sum(DECODE(TYPE,'RETURN',-1,'REFUND',-1,1)*a.total_amount - a.total_discount + a.voided_discount - abs(a.rounding_amount)),"); // Total Amount
            builder.append("sum(DECODE(TYPE,'RETURN',-1,'REFUND',-1,1)*a.tariff),");// Amount VAT
            builder.append("sum(DECODE(TYPE,'RETURN',-1,'REFUND',-1,1)*a.vat),"); // PPN/Total VAT
            builder.append("sum(DECODE(TYPE,'RETURN',-1,'REFUND',-1,1)*a.total_non_member_markup),");// Amount Markup
            builder.append("sum((DECODE(TYPE,'RETURN',-1,'REFUND',-1,1)*a.total_non_member_markup) - (DECODE(TYPE,'RETURN',-1,'REFUND',-1,1)*a.total_non_member_markup/1.1)),"); // PPN/Total Markup
            builder.append("count(b.customer_number),"); // Freq Visit
            builder.append("c.created_datetime,");// Registration Date
            builder.append("max(a.transaction_date) ");// Last Visit Date
            builder.append("FROM POS_TRANSACTION a ");
            builder.append("LEFT JOIN tax_invoice b ON b.pos_txn_id = a.id ");
            builder.append("left join crm_member c on b.customer_number=c.account_id ");
            builder.append("left join crm_ref_lookup_dtl d on d.code=c.customer_segregation ");
            if (StringUtils.isNotBlank(storeCode)) {
                builder.append("left join store e on a.store_id = e.store_id ");
            }
            builder.append("WHERE a.type in (?, ?, ?) AND a.status = ? ");
            builder.append("AND a.CUSTOMER_ID IS NOT NULL ");
            builder.append("AND b.customer_number IS NOT NULL ");
            builder.append("AND a.id LIKE ? ");
            builder.append("AND a.sales_date between ? and ? ");
            if (StringUtils.isNotBlank(storeCode)) {
                builder.append("and e.store_code = ? ");
            }
            builder.append("GROUP BY b.customer_number, b.customer_name, c.company_name, d.description, c.contact, c.business_phone, c.home_phone, c.created_datetime ");
            builder.append("ORDER BY b.customer_number");

            con = dataSource.getConnection();
            ps = con.prepareStatement(builder.toString(), ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_READ_ONLY);
            ps.setString(1, "SALE");
            ps.setString(2, "REFUND");
            ps.setString(3, "RETURN");
            ps.setString(4, "COMPLETED");
            ps.setString(5, "2000%");
            ps.setTimestamp(6, new Timestamp(dateFrom.toDate().getTime()));
            ps.setTimestamp(7, new Timestamp(dateTo.toDate().getTime()));
            rs = ps.executeQuery();
            if (StringUtils.isNotBlank(storeCode)) {
                ps.setString(8, storeCode);
            }
            rs = ps.executeQuery();

            if (!rs.isBeforeFirst() ) {
                return true;
            }
        } catch (SQLException e) {
            LOG.error("SQL Error: {}", e);
        } finally {

            if (rs != null) {
                IOUtils.INSTANCE.close(rs);
            }
            if (ps != null) {
                IOUtils.INSTANCE.close(ps);
            }
            if (con != null) {
                IOUtils.INSTANCE.close(con, true);
            }
        }
        return false;
    }

    public static class BuyingCustomerReportBean {
        String accountNumber;
        String customerName;
        String firstName;
        String middleName;
        String lastName;
        String businessName;
        String customerGroup;
        String contact;
        Double totalSales;
        Double totalTaxableAmount;
        Double ppn;
        Double totalMarkup;
        Double ppnMarkup;
        Long visitCount;
        DateTime regDate;
        LocalDateTime lastVisitDate;

        public BuyingCustomerReportBean() {
        }

        public String getAccountNumber() {
            return accountNumber;
        }

        public void setAccountNumber(String accountNumber) {
            this.accountNumber = accountNumber;
        }

        public String getCustomerName() {
            return customerName;
        }

        public void setCustomerName(String customerName) {
            this.customerName = customerName;
        }

        public String getFirstName() {
            return StringUtils.defaultIfEmpty(firstName, "");
        }

        public void setFirstName(String firstName) {
            this.firstName = firstName;
        }

        public String getLastName() {
            return StringUtils.defaultIfEmpty(lastName, "");
        }

        public void setLastName(String lastName) {
            this.lastName = lastName;
        }

        public String getMiddleName() {
            return StringUtils.defaultIfEmpty(middleName, "");
        }

        public void setMiddleName(String middleName) {
            this.middleName = middleName;
        }

        public String getBusinessName() {
            return businessName;
        }

        public void setBusinessName(String businessName) {
            this.businessName = businessName;
        }

        public String getCustomerGroup() {
            return customerGroup;
        }

        public void setCustomerGroup(String customerGroup) {
            this.customerGroup = customerGroup;
        }

        public String getContact() {
            return contact;
        }

        public void setContact(String contact) {
            this.contact = contact;
        }

        public Double getTotalSales() {
            return totalSales;
        }

        public void setTotalSales(Double totalSales) {
            this.totalSales = totalSales;
        }

        public Double getTotalTaxableAmount() {
            return totalTaxableAmount;
        }

        public void setTotalTaxableAmount(Double totalTaxableAmount) {
            this.totalTaxableAmount = totalTaxableAmount;
        }

        public Double getPpn() {
            return ppn;
        }

        public void setPpn(Double ppn) {
            this.ppn = ppn;
        }

        public Double getTotalMarkup() {
            return totalMarkup;
        }

        public void setTotalMarkup(Double totalMarkup) {
            this.totalMarkup = totalMarkup;
        }

        public Double getPpnMarkup() {
            return ppnMarkup;
        }

        public void setPpnMarkup(Double ppnMarkup) {
            this.ppnMarkup = ppnMarkup;
        }

        public Long getVisitCount() {
            return visitCount;
        }

        public void setVisitCount(Long visitCount) {
            this.visitCount = visitCount;
        }

        public DateTime getRegDate() {
            return regDate;
        }

        public void setRegDate(DateTime regDate) {
            this.regDate = regDate;
        }

        public LocalDateTime getLastVisitDate() {
            return lastVisitDate;
        }

        public void setLastVisitDate(LocalDateTime lastVisitDate) {
            this.lastVisitDate = lastVisitDate;
        }
    }

}
