<%@ include file="../common/taglibs.jsp" %>

<style type="text/css">
<!--
  .well .input {
    background-color: #fff;
    border: 1px solid #E8E8E8;
    box-shadow: none;    
    padding: 6px;
    height: 18px;
    margin-left: 6px;
  }
  .well .btn-primary {
    margin-left: 10px;
  }
  .label-single {
      width: 130px;
  }
  select.contributer, select#dateCriteria {
    width: 400px;
  }
  .btn-small {
    padding: 2px 10px;
  }
-->
</style>

    <div id="searchForm" class="well">
          <div class="form-inline form-search">
          	  <label class="label-single"><h5><spring:message code="entity_search" />:</h5></label>
              <select id="dateCriteria" style="margin: 5px;">
                  <c:forEach var="criteria" items="${dateCriteria}">
                      <option value="${criteria.value}">${criteria.description}</option>
                  </c:forEach>
              </select>
          </div>  
          <div class="form-inline form-search">
              <label class="label-single"><h5>From:</h5></label>
              <input type="text" id="searchFrom" class="input-small" style="margin: 5px;" />
              <label class=""><h5>To:</h5></label>
              <input type="text" id="searchTo" class="input-small" style="margin: 5px;" />
              <input id="searchBtn" type="submit" value="<spring:message code="label_search" />" class="btn btn-primary" />
              <input id="clearButton" type="button" value="<spring:message code="label_clear" />" class="btn"/>
          </div>
          <c:if test="${!isSupplement and !isEmployee}">
          <div class="form-inline form-search">
          	<label class="label-single"><h5><spring:message code="label_member_supplement_plural" />:</h5></label>
          	<select id="contributer" name="contributer" multiple="multiple" class="contributer" style="margin: 5px;">
          		<c:forEach var="supplement" items="${supplements}" varStatus="i">
          			<option value="${supplement.accountId}">${supplement.formattedMemberName}</option>
          		</c:forEach>
          	</select>
          </div>
          </c:if>
    </div>
    
    <script type="text/javascript">
    $("#searchFrom").datepicker({
    	 autoclose : true, format: 'dd-mm-yyyy'
	}).on('changeDate', function(selected){
		startDate = new Date(selected.date.valueOf());
        startDate.setDate(startDate.getDate(new Date(selected.date.valueOf())));
        $('#searchTo').datepicker('setStartDate', startDate);
	});
	$("#searchTo").datepicker({
		autoclose : true, format: 'dd-mm-yyyy'
	}).on('changeDate', function(selected){
		FromEndDate = new Date(selected.date.valueOf());
        FromEndDate.setDate(FromEndDate.getDate(new Date(selected.date.valueOf())));
        $('#searchFrom').datepicker('setEndDate', FromEndDate);
	});
    </script>