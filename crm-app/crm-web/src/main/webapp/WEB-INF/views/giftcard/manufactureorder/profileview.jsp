<%@ include file="../../common/taglibs.jsp" %>

<div class="modal-dialog">
  <div class="modal-content">
 
    <div class="modal-header">
      <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
      <h4><spring:message code="view_profile"/></h4>
    </div>
    
    <div class="modal-body">
    
    <div id="contentError" class="hide alert alert-error">
      <button type="button" class="close" onclick="$(this).parent().hide();">&times;</button>
      <div><form:errors path="*"/></div>
    </div>
    
    <div class="row-fluid">
    
    <form:form id="profileForm" name="profileForm" modelAttribute="profileForm" action="${pageContext.request.contextPath}/gc/productprofile/save/" method="POST" cssClass="form-reset form-horizontal" >
      <form:hidden path="id"/>
      <div class="span6">
      <div class="control-group">
        <spring:message code="profile_product_code" var="prodCode" />
        <label for="productCode" class="control-label"><b class="required">*</b>${prodCode}</label> 
        <div class="controls">${profileForm.productCode}</div>
      </div>
      
      <div class="control-group">
        <spring:message code="profile_product_desc" var="prodDesc" />
        <label for="productDesc" class="control-label"><b class="required">*</b>${prodDesc}</label> 
        <div class="controls">${profileForm.productDesc}</div>
      </div>
      
      <div class="control-group">
        <spring:message code="profile_face_value" var="faceVal" />
        <label for="faceValue" class="control-label">${faceVal}</label> 
        <fmt:formatNumber type="number" pattern="#,##0" value="${profileForm.faceValue}" var="faceValue"/>
        <fmt:formatNumber type="number" pattern="#,##0" value="${profileForm.faceValueDesc}" var="faceValueDesc"/>
        <div class="controls">${faceValue} - ${faceValueDesc}</div>
      </div>
      
      <div class="control-group">
        <spring:message code="profile_card_fee" var="crdFee" />
        <label for="cardFee" class="control-label">${crdFee}</label> 
        <fmt:formatNumber type="number" pattern="#,##0" value="${profileForm.cardFee}" var="cardFee"/>
        <div class="controls">${cardFee}</div>
      </div>
      
      <div class="control-group">
        <label for="cardFee" class="control-label">&nbsp;</label> 
        <div class="controls">
          <div class="span checkbox">
            <form:checkbox path="allowReload" disabled="true"/><label for="allowReload"><spring:message code="profile_allow_reload" /></label>
          </div>
          <div class="span checkbox">
            <form:checkbox path="allowPartialRedeem" disabled="true" /><label for="allowReload"><spring:message code="profile_allow_partial_redeem" /></label>
          </div>
        </div>
      </div>
      
      
      
      </div>
      
      <div class="span6">
      
      <div class="control-group">
        <spring:message code="profile_safety_stocks" var="safetyStocks" />
        <label for="safetyStocksEaMo" class="control-label">${safetyStocks}</label> 
        <div class="controls">${profileForm.safetyStocksEaMo}</div>
      </div>
      
      <div class="control-group">
        <spring:message code="profile_max_amount" var="maxAmnt" />
        <label for="maxAmount" class="control-label">${maxAmnt}</label> 
        <fmt:formatNumber type="number" pattern="#,##0" value="${profileForm.maxAmount}" var="maxAmount"/>
        <div class="controls">${maxAmount}</div>
      </div>
      
      <div class="control-group">
        <spring:message code="profile_effective_months" var="effectvMos" />
        <label for="effectiveMonths" class="control-label">${effectvMos}</label> 
        <div class="controls">${profileForm.effectiveMonths}</div>
      </div>
      
      <div class="control-group">
        <spring:message code="profile_unit_cost" var="unitCst" />
        <label for="unitCost" class="control-label">${unitCst}</label>
        <fmt:formatNumber type="number" pattern="#,##0" value="${profileForm.unitCost}" var="unitCost"/>
        <div class="controls">${unitCost}</div>
      </div>
      
      </div>
      
    </form:form>
    
    </div>
    
    </div>
    <div class="modal-footer">
      <button type="button" id="profileCancel" class="btn btn-default" data-dismiss="modal"><spring:message code="label_cancel"/></button>
    </div>
  </div>
</div>

<script>
</script>


