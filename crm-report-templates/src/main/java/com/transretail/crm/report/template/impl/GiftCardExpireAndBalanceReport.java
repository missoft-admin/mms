package com.transretail.crm.report.template.impl;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.apache.commons.collections.comparators.ComparatorChain;
import org.apache.commons.lang3.StringUtils;
import org.joda.time.LocalDate;
import org.joda.time.YearMonth;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.MessageSourceAware;
import org.springframework.context.NoSuchMessageException;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.context.support.MessageSourceAccessor;
import org.springframework.stereotype.Service;

import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.mysema.query.BooleanBuilder;
import com.mysema.query.Tuple;
import com.mysema.query.group.GroupBy;
import com.mysema.query.jpa.JPASubQuery;
import com.mysema.query.jpa.impl.JPAQuery;
import com.mysema.query.types.Projections;
import com.mysema.query.types.query.ListSubQuery;
import com.transretail.crm.common.reporting.jasper.ClasspathFileResolver;
import com.transretail.crm.common.reporting.jasper.DefaultJRProcessor;
import com.transretail.crm.common.reporting.jasper.JRProcessor;
import com.transretail.crm.core.entity.QPeopleSoftStore;
import com.transretail.crm.core.entity.enums.ReportType;
import com.transretail.crm.core.security.util.UserUtil;
import com.transretail.crm.core.service.LookupService;
import com.transretail.crm.giftcard.entity.QGiftCardCustomerProfile;
import com.transretail.crm.giftcard.entity.QGiftCardInventory;
import com.transretail.crm.giftcard.entity.QProductProfile;
import com.transretail.crm.giftcard.entity.QReturnRecord;
import com.transretail.crm.giftcard.entity.QSalesOrderAlloc;
import com.transretail.crm.giftcard.entity.QSalesOrderItem;
import com.transretail.crm.giftcard.entity.support.GiftCardInventoryStatus;
import com.transretail.crm.giftcard.entity.support.GiftCardSalesType;
import com.transretail.crm.giftcard.entity.support.SalesOrderStatus;
import com.transretail.crm.giftcard.repo.GiftCardInventoryRepo;
import com.transretail.crm.giftcard.service.GiftCardInventoryService;
import com.transretail.crm.giftcard.service.SoAllocService;
import com.transretail.crm.report.template.FilterField;
import com.transretail.crm.report.template.ReportTemplate;

/**
 * @author ftopico
 */

@Service("giftCardExpireAndBalanceReport")
public class GiftCardExpireAndBalanceReport implements ReportTemplate, MessageSourceAware {

	@Autowired
	private GiftCardInventoryService giftCardInventoryService;
	@Autowired
	private SoAllocService soAllocService;
	@Autowired
	private GiftCardInventoryRepo inventoryRepo;
	@PersistenceContext
	private EntityManager em;
	@Autowired
	private LookupService lookupService;

	private static final String PERMISSION_CODE = "GC_EXPIRE_AND_BALANCE_REPORT";
	protected static final String TEMPLATE_NAME = "Expire and Balance Detail Report";
	protected static final String REPORT_NAME = "reports/gift_card_expire_balance_detail_document.jasper";
	protected static final String REPORT_FILTER_DATE_FROM = "dateFrom";
	protected static final String REPORT_FILTER_DATE_TO = "dateTo";

	protected static final String REPORT_FILTER_SALES_ORDER = "orderNo";
	protected static final String REPORT_FILTER_CUST = "customer";
	protected static final String REPORT_FILTER_SALES_TYPE = "salesType";
	protected static final String REPORT_FILTER_ACT_DATE_FROM = "actFrom";
	protected static final String REPORT_FILTER_ACT_DATE_TO = "actTo";
	protected static final String REPORT_FILTER_CARD_FROM = "cardNoFrom";
	protected static final String REPORT_FILTER_CARD_TO = "cardNoTo";

	protected static final String REPORT_FILTER_TYPE = "analysisType";
	protected static final String REPORT_FILTER_BU = "bu";

	protected static final String REPORT_FILTER_DATE_FORMAT = "dd-MM-yyyy";
	//Header Params
	private static final String PARAM_PRINT_DATE = "printedDate";
	private static final String PARAM_DATE_RANGE = "dateRange";
	private static final String PARAM_PRINTED_BY = "printedBy";
	private static final DateTimeFormatter FULL_DATE_PATTERN = DateTimeFormat.forPattern("dd-MM-yyyy").withLocale(LocaleContextHolder.getLocale());
	private static final DateTimeFormatter RAW_YEAR_MONTH_FORMATTER = DateTimeFormat.forPattern("yyyyMM");
	private MessageSourceAccessor messageSource;

	public static enum AnalysisType {
		DETAIL, SUMMARY
	}

	public GiftCardExpireAndBalanceReport() {
	}

	public ReportType[] getReportTypes() {
		return ReportType.values();
	}

	public String getName() {
		return TEMPLATE_NAME;
	}

	public String getDescription() {
		return messageSource.getMessage("title_document_gift_card_expire_balance_detail");
	}

	@Override
	public boolean canView(Set<String> permissions) {
		return permissions.contains(PERMISSION_CODE);
	}

	@Override
	public void setMessageSource(MessageSource messageSource) {
		this.messageSource = new MessageSourceAccessor(messageSource);
	}
	public Set<FilterField> getFilters() {
		Set<FilterField> filters = new LinkedHashSet<FilterField>(2);

		Map<String, String> reportTypes = Maps.newLinkedHashMap();
		for(AnalysisType reportType: AnalysisType.values()) {
			reportTypes.put(reportType.toString(), reportType.toString());
		}
		filters.add(FilterField.createDropdownField(REPORT_FILTER_TYPE,
				messageSource.getMessage("gc_expirebalance_report_label_report_type"), reportTypes, true));

		filters.add(FilterField.createDateField(REPORT_FILTER_DATE_FROM,
				messageSource.getMessage("gc_expirebalance_report_label_expirydatefrom"),
				REPORT_FILTER_DATE_FORMAT, FilterField.DateTypeOption.PLAIN, true, true));
		filters.add(FilterField.createDateField(REPORT_FILTER_DATE_TO,
				messageSource.getMessage("gc_expirebalance_report_label_expirydateto"),
				REPORT_FILTER_DATE_FORMAT, FilterField.DateTypeOption.PLAIN, true, true));
		filters.add(FilterField.createInputField(REPORT_FILTER_SALES_ORDER,
				messageSource.getMessage("gc_expirebalance_report_label_sales_order")));
		QGiftCardCustomerProfile customerProfile = QGiftCardCustomerProfile.giftCardCustomerProfile;

		Map<String, String> customers = Maps.newLinkedHashMap();
		customers.putAll(new JPAQuery(em).from(customerProfile).orderBy(customerProfile.customerId.asc()).transform(GroupBy.groupBy(customerProfile.customerId).as(
				customerProfile.customerId.append(" - ").append(customerProfile.name))));
		filters.add(FilterField.createDropdownField(REPORT_FILTER_CUST,
				messageSource.getMessage("gc_expirebalance_report_label_customer"), customers));

		Map<String, String> salesTypes = Maps.newLinkedHashMap();
		for(GiftCardSalesType salesType: GiftCardSalesType.values()) {
			salesTypes.put(salesType.toString(), salesType.toString());
		}
		filters.add(FilterField.createDropdownField(REPORT_FILTER_SALES_TYPE,
				messageSource.getMessage("gc_expirebalance_report_label_sales_type"), salesTypes));


		filters.add(FilterField.createDateField(REPORT_FILTER_ACT_DATE_FROM,
				messageSource.getMessage("gc_expirebalance_report_label_actdatefrom"),
				REPORT_FILTER_DATE_FORMAT, FilterField.DateTypeOption.PLAIN));
		filters.add(FilterField.createDateField(REPORT_FILTER_ACT_DATE_TO,
				messageSource.getMessage("gc_expirebalance_report_label_actdateto"),
				REPORT_FILTER_DATE_FORMAT, FilterField.DateTypeOption.PLAIN));

		filters.add(FilterField.createInputField(REPORT_FILTER_CARD_FROM,
				messageSource.getMessage("gc_expirebalance_report_label_card_number_from")));
		filters.add(FilterField.createInputField(REPORT_FILTER_CARD_TO,
				messageSource.getMessage("gc_expirebalance_report_label_card_number_to")));

		Map<String, String> products = new LinkedHashMap<String, String>();
		products.putAll(getProducts());
		filters.add(FilterField.createDropdownField("product",
				messageSource.getMessage("gc_expirebalance_report_label_product", (Object[]) null, LocaleContextHolder.getLocale()), products));

		Map<String, String> buList = Maps.newLinkedHashMap();
		QPeopleSoftStore qpstore = QPeopleSoftStore.peopleSoftStore;
		buList.putAll(new JPAQuery(em).from(qpstore).groupBy(qpstore.businessUnit.code, qpstore.businessUnit.description)
				.map(qpstore.businessUnit.code, qpstore.businessUnit.code.append(" - ").append(qpstore.businessUnit.description)));
		filters.add(FilterField.createDropdownField("bu",
				messageSource.getMessage("gc_expirebalance_report_label_bu", (Object[]) null, LocaleContextHolder.getLocale()), buList));


		return filters;
	}

	private Map<String, String> getProducts() {
		QProductProfile qPrd = QProductProfile.productProfile;
		return new JPAQuery(em).from(qPrd).orderBy(qPrd.productDesc.asc()).map(qPrd.id.stringValue(), qPrd.productDesc);
	}

	@Override
	public JRProcessor createJrProcessor(Map<String, String> map) {

		String orderNo = map.get(REPORT_FILTER_SALES_ORDER);
		String custNo = map.get(REPORT_FILTER_CUST);
		GiftCardSalesType salesType = StringUtils.isNotBlank(map.get(REPORT_FILTER_SALES_TYPE))
				? GiftCardSalesType.valueOf(map.get(REPORT_FILTER_SALES_TYPE)) : null;
		LocalDate actDateFrom = StringUtils.isNotBlank(map.get(REPORT_FILTER_ACT_DATE_FROM))
				? FULL_DATE_PATTERN.parseLocalDate(map.get(REPORT_FILTER_ACT_DATE_FROM)) : null;
		LocalDate actDateTo = StringUtils.isNotBlank(map.get(REPORT_FILTER_ACT_DATE_TO))
				? FULL_DATE_PATTERN.parseLocalDate(map.get(REPORT_FILTER_ACT_DATE_TO)) : null;
		String cardNoFrom = map.get(REPORT_FILTER_CARD_FROM);
		String cardNoTo = map.get(REPORT_FILTER_CARD_TO);

		LocalDate dateFrom = FULL_DATE_PATTERN.parseLocalDate(map.get(REPORT_FILTER_DATE_FROM));
		LocalDate dateTo = FULL_DATE_PATTERN.parseLocalDate(map.get(REPORT_FILTER_DATE_TO));

		AnalysisType reportType = AnalysisType.valueOf(map.get(REPORT_FILTER_TYPE));

		Long prodId = StringUtils.isNotBlank(map.get("product")) ? Long.valueOf(map.get("product")) : null;

		String buCode = map.get("bu");
		List<String> storeCodes = null;
		if(StringUtils.isNotBlank(buCode)) {
			QPeopleSoftStore qpstore = QPeopleSoftStore.peopleSoftStore;
			storeCodes = new JPAQuery(em).from(qpstore).where(qpstore.businessUnit.code.eq(buCode)).list(qpstore.store.code);
		}

		String[] args = {map.get(REPORT_FILTER_DATE_FROM), map.get(REPORT_FILTER_DATE_TO)};

		String actStr = "";
		if(actDateFrom != null || actDateTo != null)
			actStr = new StringBuilder(actStr = actDateFrom == null ? "..." : actDateFrom.toString())
					.append(" - ")
					.append(actDateTo == null ? "..." : actDateTo.toString()).toString();

		String crdStr = "";
		if(StringUtils.isNotBlank(cardNoTo) || StringUtils.isNotBlank(cardNoFrom))
			crdStr = new StringBuilder(StringUtils.isBlank(cardNoFrom) ? "..." : cardNoFrom)
					.append(" - ")
					.append(StringUtils.isBlank(cardNoTo) ? "..." : cardNoTo).toString();

		QGiftCardCustomerProfile qProfile = QGiftCardCustomerProfile.giftCardCustomerProfile;
		String customerName = null;
		if(custNo != null)
			customerName = new JPAQuery(em).from(qProfile).where(qProfile.customerId.eq(custNo)).singleResult(qProfile.name);

		Map<String, Object> parameters = new HashMap<String, Object>();
		parameters.put("ORDER_NO", orderNo);
		parameters.put("CUST", StringUtils.isNotBlank(custNo) ? (custNo + " - " + customerName) : "");
		parameters.put("SALES_TYPE", salesType != null ? salesType.toString() : "");
		parameters.put("ACT_STR", actStr);
		parameters.put("CARD_STR", crdStr);
		parameters.put("REPORT_TYPE", reportType.toString());
		parameters.put("PRODUCT", prodId != null ? prodId + " - " + getProducts().get(Long.toString(prodId)) : "");
		parameters.put(PARAM_PRINT_DATE, FULL_DATE_PATTERN.print(new LocalDate()));
		parameters.put(PARAM_PRINTED_BY, getCurrentUser());
		parameters.put(PARAM_DATE_RANGE, getDateRangeMessage(args));
		if(StringUtils.isNotBlank(buCode))
			parameters.put("BU", lookupService.getActiveDetailByCode(buCode).getCodeAndDesc());


		List<ReportBean> beansDataSource = Lists.newArrayList();
		DecimalFormat df = new DecimalFormat("#,##0");

		QSalesOrderAlloc qSalesOrderAlloc = QSalesOrderAlloc.salesOrderAlloc;
		QGiftCardInventory qIn = QGiftCardInventory.giftCardInventory;
		QSalesOrderItem qSo = QSalesOrderItem.salesOrderItem;
		QGiftCardCustomerProfile qCx = QGiftCardCustomerProfile.giftCardCustomerProfile;
		QReturnRecord qRet = QReturnRecord.returnRecord;
		ListSubQuery<String> invalidSo = new JPASubQuery().from(qRet).distinct().list(qRet.orderNo.orderNo);
		BooleanBuilder gcFilter = new BooleanBuilder(qIn.expiryDate.between(dateFrom, dateTo));
		BooleanBuilder soFilter = new BooleanBuilder(
				qSo.order.status.eq(SalesOrderStatus.SOLD)
						.and(qSo.order.orderNo.notIn(invalidSo))
		);
		if(StringUtils.isNotBlank(orderNo))
			soFilter.and(qSo.order.orderNo.eq(orderNo));
		if(StringUtils.isNotBlank(custNo))
			soFilter.and(qCx.customerId.eq(custNo));
		if(storeCodes != null)
			soFilter.and(qSo.order.paymentStore.in(storeCodes));
		if(salesType != null)
			gcFilter.and(qIn.salesType.eq(salesType));
		if(actDateFrom != null)
			gcFilter.and(qIn.activationDate.goe(actDateFrom));
		if(actDateTo != null)
			gcFilter.and(qIn.activationDate.loe(actDateTo));
		if(StringUtils.isNotBlank(cardNoFrom))
			gcFilter.and(qIn.barcode.goe(cardNoFrom));
		if(StringUtils.isNotBlank(cardNoTo))
			gcFilter.and(qIn.barcode.loe(cardNoTo));
		if(prodId != null)
			gcFilter.and(qIn.profile.id.eq(prodId));

		BooleanBuilder gcBuFilter = new BooleanBuilder();
		if(storeCodes != null)
			gcBuFilter.and(qIn.location.in(storeCodes));

		List<ExpireAndBalance> list = new ArrayList<GiftCardExpireAndBalanceReport.ExpireAndBalance>();


		if(AnalysisType.DETAIL.compareTo(reportType) == 0) {
			List<Tuple> inventories = new JPAQuery(em).from(qSalesOrderAlloc, qIn)
					//.innerJoin(qSalesOrderAlloc.gcInventory, qIn)
					/*.innerJoin( qIn )*/
					/*.on(  )*/
					.leftJoin(qSalesOrderAlloc.soItem, qSo)
					.leftJoin(qSo.order.customer, qCx)
					.where(soFilter.and(gcFilter).and(qIn.series.goe( qSalesOrderAlloc.seriesStart ).and( qIn.series.loe( qSalesOrderAlloc.seriesEnd ) )))
					.orderBy(qSo.order.orderNo.asc(), qIn.series.asc())
					.list(
							qIn.id,
							qIn.barcode,
							qIn.expiryDate,
							qIn.balance,
							qIn.salesType,
							qCx.name,
							qSo.order.orderNo);


			List<Long> gcInventoryList = new ArrayList<Long>();

			int counter = 1;
			for(Tuple gcInventory: inventories) {
				if (gcInventory != null) {
					ExpireAndBalance expAndBalance =  new ExpireAndBalance();
					expAndBalance.setSequenceNo(String.valueOf(counter++));
					expAndBalance.setCardNumber(gcInventory.get(qIn.barcode));
					expAndBalance.setExpiryDate(FULL_DATE_PATTERN.print(gcInventory.get(qIn.expiryDate)));
					expAndBalance.setBalance(df.format(gcInventory.get(qIn.balance)));
					expAndBalance.setCustomer(gcInventory.get(qSo.order.customer.name));
					expAndBalance.setSalesOrder(gcInventory.get(qSo.order.orderNo));
					expAndBalance.setSalesType(gcInventory.get(qIn.salesType).name());
					list.add(expAndBalance);
					gcInventoryList.add(gcInventory.get(qIn.id));
				}
			}


			if(StringUtils.isBlank(orderNo) && StringUtils.isBlank(custNo)) {
				List<Tuple> gcs = new JPAQuery(em).from(qIn)
						.where(gcFilter.and(gcBuFilter))
						.orderBy(qIn.series.asc())
						.list(
								qIn.id,
								qIn.barcode,
								qIn.expiryDate,
								qIn.balance,
								qIn.salesType);

				for(Tuple tup : gcs) {
					Long gcId = tup.get(qIn.id);
					if (!gcInventoryList.contains(gcId)) {
						ExpireAndBalance expAndBalance =  new ExpireAndBalance();
						expAndBalance.setSequenceNo(String.valueOf(counter++));
						expAndBalance.setCardNumber(tup.get(qIn.barcode));

						final String theDate = tup.get(qIn.expiryDate) != null ? FULL_DATE_PATTERN.print(tup.get(qIn.expiryDate)) : "";
						expAndBalance.setExpiryDate(theDate);
						Double balance = tup.get(qIn.balance);
						if (balance == null) {

							expAndBalance.setBalance(df.format(0));
						} else {

							expAndBalance.setBalance(df.format(balance));
						}

						final String type = (tup.get(qIn.salesType) != null ? tup.get(qIn.salesType).name() : "");
						expAndBalance.setSalesType(type);
						list.add(expAndBalance);
					}
				}
			}
		}

		List<ExpireAndBalanceSummary> list2 = Lists.newArrayList();

		if(AnalysisType.SUMMARY.compareTo(reportType) == 0) {
			//Summary
			list2 = new JPAQuery(em).from(qSalesOrderAlloc, qIn)
					//.innerJoin(qSalesOrderAlloc.gcInventory, qIn)
					/*.innerJoin( qIn ).on(  )*/
					.innerJoin(qSalesOrderAlloc.soItem, qSo)
					.leftJoin(qSo.order.customer, qCx)
					.where(soFilter.and(gcFilter).and(qIn.series.goe( qSalesOrderAlloc.seriesStart ).and( qIn.series.loe( qSalesOrderAlloc.seriesEnd ) )))
					.orderBy(qIn.expiryDate.yearMonth().asc(),
							qCx.name.asc(),
							qIn.salesType.asc())
					.groupBy(qIn.expiryDate.yearMonth(),
							qCx.name,
							qIn.salesType)
					.list(Projections.fields(ExpireAndBalanceSummary.class,
							qIn.expiryDate.yearMonth().as("expiryMonth"),
							qIn.balance.sum().as("balance"),
							qCx.name.as("customer"),
							qIn.salesType.stringValue().as("salesType")));

			if(StringUtils.isBlank(orderNo) && StringUtils.isBlank(custNo)) {
				list2.addAll(new JPAQuery(em).from(qIn)
						.where(gcFilter.and(qIn.salesType.in(GiftCardSalesType.B2C, GiftCardSalesType.B2B, GiftCardSalesType.INTERNAL, GiftCardSalesType.REBATE)).and(gcBuFilter))
						.orderBy(qIn.expiryDate.yearMonth().asc(),
								qIn.salesType.asc())
						.groupBy(qIn.expiryDate.yearMonth(),
								qIn.salesType)
						.list(Projections.fields(ExpireAndBalanceSummary.class,
								qIn.expiryDate.yearMonth().as("expiryMonth"),
								qIn.balance.sum().as("balance"),
								qIn.salesType.stringValue().as("salesType"))));
			}

			Collections.sort(list2, getComparatorChain());
		}

		beansDataSource.add(new ReportBean(list, list2));

		DefaultJRProcessor jrProcessor = new DefaultJRProcessor(REPORT_NAME, beansDataSource);
		jrProcessor.addParameters(parameters);
		jrProcessor.setFileResolver(new ClasspathFileResolver("reports"));

		return jrProcessor;
	}

	private Comparator<ExpireAndBalanceSummary> getComparatorChain() {
		Comparator<ExpireAndBalanceSummary> yearMonthComparator = new Comparator<ExpireAndBalanceSummary>() {
			@Override
			public int compare(ExpireAndBalanceSummary o1, ExpireAndBalanceSummary o2) {
				return o1.getYearMonth().compareTo(o2.getYearMonth());
			}
		};

		Comparator<ExpireAndBalanceSummary> salesTypeComparator = new Comparator<ExpireAndBalanceSummary>() {
			@Override
			public int compare(ExpireAndBalanceSummary o1, ExpireAndBalanceSummary o2) {
				return o1.getSalesType().compareTo(o2.getSalesType());
			}
		};

		ComparatorChain chain = new ComparatorChain();
		chain.addComparator(yearMonthComparator);
		chain.addComparator(salesTypeComparator);

		return chain;
	}

	private String getCurrentUser() {
		if (UserUtil.getCurrentUser() != null) {
			return UserUtil.getCurrentUser().getUsername();
		} else {
			return null;
		}
	}

	private String getDateRangeMessage(String[] args) {
		String message = null;
		try {
			message = messageSource.getMessage("title_document_gift_card_expire_balance_detail_subheader", args, LocaleContextHolder.getLocale());
		} catch (NoSuchMessageException e) {
			return "";
		}

		return message;
	}

	public class ExpireAndBalance {

		private String sequenceNo;
		private String cardNumber;
		private String expiryDate;
		private String balance;
		private String customer;
		private String salesOrder;
		private String salesType;

		public ExpireAndBalance() {
		}

		public ExpireAndBalance(String sequenceNo, String cardNumber,
		                        String expiryDate, String balance, String customer,
		                        String salesOrder, String salesType) {
			super();
			this.sequenceNo = sequenceNo;
			this.cardNumber = cardNumber;
			this.expiryDate = expiryDate;
			this.balance = balance;
			this.customer = customer;
			this.salesOrder = salesOrder;
			this.salesType = salesType;
		}

		public String getSequenceNo() {
			return sequenceNo;
		}

		public String getCardNumber() {
			return cardNumber;
		}

		public String getExpiryDate() {
			return expiryDate;
		}

		public String getBalance() {
			return balance;
		}

		public String getCustomer() {
			return customer;
		}

		public String getSalesOrder() {
			return salesOrder;
		}

		public String getSalesType() {
			return salesType;
		}

		public void setSequenceNo(String sequence) {
			this.sequenceNo = sequence;
		}

		public void setCardNumber(String cardNumber) {
			this.cardNumber = cardNumber;
		}

		public void setExpiryDate(String expiryDate) {
			this.expiryDate = expiryDate;
		}

		public void setBalance(String balance) {
			this.balance = balance;
		}

		public void setCustomer(String customer) {
			this.customer = customer;
		}

		public void setSalesOrder(String salesOrder) {
			this.salesOrder = salesOrder;
		}

		public void setSalesType(String salesType) {
			this.salesType = salesType;
		}

	}

	public static class ExpireAndBalanceSummary {
		private Integer expiryMonth;
		private Double balance;
		private String customer;
		private String salesType;

		public String getCustomer() {
			return customer;
		}
		public String getSalesType() {
			return salesType;
		}
		public void setCustomer(String customer) {
			this.customer = customer;
		}
		public void setSalesType(String salesType) {
			this.salesType = salesType;
		}
		public Integer getExpiryMonth() {
			return expiryMonth;
		}
		public Double getBalance() {
			return balance;
		}
		public void setExpiryMonth(Integer expiryMonth) {
			this.expiryMonth = expiryMonth;
		}
		public void setBalance(Double balance) {
			this.balance = balance;
		}

		public YearMonth getYearMonth() {
			return YearMonth.parse(String.valueOf(expiryMonth), RAW_YEAR_MONTH_FORMATTER);
		}
	}

	public static class ReportBean {

		private List<ExpireAndBalance> list;
		private List<ExpireAndBalanceSummary> list2;

		public ReportBean(List<ExpireAndBalance> list,
		                  List<ExpireAndBalanceSummary> list2) {
			this.list = list;
			this.list2 = list2;
		}

		public ReportBean(List<ExpireAndBalance> list) {
			this.list = list;
		}

		public List<ExpireAndBalance> getList() {
			return list;
		}

		public void setList(List<ExpireAndBalance> list) {
			this.list = list;
		}

		public List<ExpireAndBalanceSummary> getList2() {
			return list2;
		}

		public void setList2(List<ExpireAndBalanceSummary> list2) {
			this.list2 = list2;
		}

	}
}
