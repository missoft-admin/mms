package com.transretail.crm.core.service.impl;

import com.transretail.crm.core.dto.MemberDto;
import com.transretail.crm.core.dto.MemberGroupDto;
import com.transretail.crm.core.dto.MemberResultList;
import com.transretail.crm.core.entity.*;
import com.transretail.crm.core.entity.embeddable.CustomerProfile;
import com.transretail.crm.core.entity.enums.MemberStatus;
import com.transretail.crm.core.entity.enums.Status;
import com.transretail.crm.core.entity.lookup.LookupDetail;
import com.transretail.crm.core.entity.lookup.LookupHeader;
import com.transretail.crm.core.entity.lookup.Product;
import com.transretail.crm.core.entity.lookup.StoreGroup;
import com.transretail.crm.core.repo.*;
import com.transretail.crm.core.service.MemberGroupService;

import org.joda.time.LocalDate;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Configurable;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

@Configurable
@ContextConfiguration(locations = "classpath*:/META-INF/spring/applicationContext*.xml")
@Transactional
@RunWith(SpringJUnit4ClassRunner.class)
public class MemberGroupServiceImplTest {
	@Autowired
	MemberGroupService memberGroupService;
	@Autowired
	LookupDetailRepo lookupDetailRepo;
	@Autowired
	LookupHeaderRepo lookupHeaderRepo;
	@Autowired
	MemberGroupRepo memberGroupRepo;
	@Autowired
	MemberGroupFieldRepo groupFieldRepo;
	@Autowired
	MemberGroupFieldValueRepo fieldValueRepo;
	@Autowired
	MemberRepo memberRepo;
	@Autowired
	StoreGroupRepo storeGroupRepo;
	@Autowired
	ProductGroupRepo productGroupRepo;
	@Autowired
	PointsTxnModelRepo pointsRepo;
	@Autowired
	PosTransactionRepo posTransactionRepo;
	@Autowired
	ProductRepo productRepo;
	@Autowired
	PosTxItemRepo itemRepo;
	
	LookupHeader ageHeader, genderHeader, typeHeader, nationHeader, children, cardType;
	LookupDetail operand1, operand2, numericType, stringType, male, female;
	LookupDetail filipino, american, chinese, regular, silver;
	MemberGroup memberGroup;
	ProductGroup productGroup;
	StoreGroup storeGroup;
	MemberModel member, regularMember, silverMember;
	
	@Before
	public void setUp() {
		setUpLookup();
		setUpMemberGroups();
		setUpMembers();
		setupCardTypeGroups();
		
		setupGroups();
		setupPoints();
	}
	
	private void setupPoints() {
		PointsTxnModel point = new PointsTxnModel();
		point.setTransactionDateTime(new Date());
		point.setTransactionNo("1");
		point.setStoreCode("ortigas");
		point.setTransactionAmount(new Double(1000));
		point.setTransactionPoints(new Double(1));
		point.setMemberModel(member);
		point.setId("99");
		pointsRepo.save(point);
		
		PosTransaction transaction = new PosTransaction();
		transaction.setId("1");
		posTransactionRepo.save(transaction);
		
		PosTxItem item = new PosTxItem();
		item.setPosTransaction(transaction);
		item.setProductId("2222220000");
		item.setId("0");
		itemRepo.save(item);
		
		Product product = new Product();
		product.setItemCode("2222220000");
		product.setId("2222220000");
		productRepo.save(product);
	}
	
	private void setupGroups() {
		StoreGroup storeGroup = new StoreGroup();
        storeGroup.setName("ncr");
        storeGroup.setStores("ortigas,makati");
        this.storeGroup = storeGroupRepo.save(storeGroup);
        
        ProductGroup productGroup = new ProductGroup();
    	productGroup.setName( "ProductGroupTest" );
    	productGroup.setProductsCfns( "111111111111,2" );
    	this.productGroup = productGroupRepo.save(productGroup);
	}
	
	private void setUpLookup() {
		ageHeader = new LookupHeader("MEM014");
		ageHeader.setDescription("AGE");
		ageHeader = lookupHeaderRepo.saveAndFlush(ageHeader);
		LookupHeader operands = new LookupHeader("GEN001");
		operands.setDescription("OPERANDS");
		operands = lookupHeaderRepo.saveAndFlush(operands);
		
		children = new LookupHeader("MEM014");
		children.setDescription("CHILDREN");
		children = lookupHeaderRepo.saveAndFlush(children);
		
		genderHeader = new LookupHeader("MEM002");
		genderHeader.setDescription("GENDER");
		genderHeader = lookupHeaderRepo.saveAndFlush(genderHeader);
		
		nationHeader = new LookupHeader("MEM003");
		nationHeader.setDescription("NATIONALITY");
		nationHeader = lookupHeaderRepo.saveAndFlush(nationHeader);
		
		typeHeader = new LookupHeader("GEN002");
		typeHeader.setDescription("FIELD TYPE");
		typeHeader = lookupHeaderRepo.saveAndFlush(typeHeader);
		
		cardType = new LookupHeader("MEM008");
		cardType.setDescription("CARD TYPE");
		cardType = lookupHeaderRepo.saveAndFlush(cardType);
		
		operand1 = new LookupDetail("OPRD004");
		operand1.setDescription("LESS THAN");
		operand1.setStatus(Status.ACTIVE);
		operand1.setHeader(operands);
		operand1 = lookupDetailRepo.saveAndFlush(operand1);
		
		operand2 = new LookupDetail("OPRD002");
		operand2.setDescription("GREATER THAN");
		operand2.setStatus(Status.ACTIVE);
		operand2.setHeader(operands);
		operand2 = lookupDetailRepo.saveAndFlush(operand2);
		
		numericType = new LookupDetail("FTYP002");
		numericType.setDescription("NUMERIC");
		numericType.setStatus(Status.ACTIVE);
		numericType.setHeader(typeHeader);
		numericType = lookupDetailRepo.saveAndFlush(numericType);
		
		stringType = new LookupDetail("FTYP001");
		stringType.setDescription("STRING");
		stringType.setStatus(Status.ACTIVE);
		stringType.setHeader(typeHeader);
		stringType = lookupDetailRepo.saveAndFlush(stringType);
		
		male = new LookupDetail("GEND001");
		male.setDescription("MALE");
		male.setHeader(genderHeader);
		male.setStatus(Status.ACTIVE);
		male = lookupDetailRepo.saveAndFlush(male);
		
		female = new LookupDetail("GEND002");
		female.setDescription("FEMALE");
		female.setHeader(genderHeader);
		female.setStatus(Status.ACTIVE);
		female = lookupDetailRepo.saveAndFlush(female);
		
		filipino = new LookupDetail("NATY002");
		filipino.setDescription("FILIPINO");
		filipino.setHeader(nationHeader);
		filipino.setStatus(Status.ACTIVE);
		filipino = lookupDetailRepo.saveAndFlush(filipino);
		
		american = new LookupDetail("NATY003");
		american.setDescription("AMERICAN");
		american.setHeader(nationHeader);
		american.setStatus(Status.ACTIVE);
		american = lookupDetailRepo.saveAndFlush(american);
		
		chinese = new LookupDetail("NATY004");
		chinese.setDescription("CHINESE");
		chinese.setHeader(nationHeader);
		chinese.setStatus(Status.ACTIVE);
		chinese = lookupDetailRepo.saveAndFlush(chinese);
		
		regular = new LookupDetail("TIER001");
		regular.setDescription("REGULAR");
		regular.setHeader(cardType);
		regular.setStatus(Status.ACTIVE);
		regular = lookupDetailRepo.saveAndFlush(regular);
		
		silver = new LookupDetail("TIER002");
		silver.setDescription("silver");
		silver.setHeader(cardType);
		silver.setStatus(Status.ACTIVE);
		silver = lookupDetailRepo.saveAndFlush(silver);
	}
	
	private void setUpMemberGroups() {
		memberGroup = new MemberGroup();
		memberGroup.setName("group1");
		memberGroup = memberGroupRepo.saveAndFlush(memberGroup);
		
		List<MemberGroupField> fields = new ArrayList<MemberGroupField>();
		List<MemberGroupFieldValue> values = new ArrayList<MemberGroupFieldValue>();
		MemberGroupField field;
		MemberGroupFieldValue value;
		
		field = new MemberGroupField();
		field.setMemberGroup(memberGroup);
		field.setName(ageHeader);
		field.setType(numericType);
		field = groupFieldRepo.saveAndFlush(field);
		fields.add(field);
		
		// age < 15
		value = new MemberGroupFieldValue();
		value.setMemberGroupField(field);
		value.setValue("15");
		value.setOperand(operand1);
		value = fieldValueRepo.saveAndFlush(value);
		values.add(value);
		
		// age > 5
		value = new MemberGroupFieldValue();
		value.setMemberGroupField(field);
		value.setValue("5");
		value.setOperand(operand1);
		value = fieldValueRepo.saveAndFlush(value);
		values.add(value);
		
		// gender
		field = new MemberGroupField();
		field.setMemberGroup(memberGroup);
		field.setName(genderHeader);
		field.setType(stringType);
		field = groupFieldRepo.saveAndFlush(field);
		fields.add(field);
		
		// male
		value = new MemberGroupFieldValue();
		value.setMemberGroupField(field);
		value.setValue("GEND001");
		value.setOperand(operand1);
		value = fieldValueRepo.saveAndFlush(value);
		values.add(value);
		
//		field.setFieldValues(values);
//		groupFieldRepo.saveAndFlush(field);
		values = new ArrayList<MemberGroupFieldValue>();
		
		// nationality
		field = new MemberGroupField();
		field.setMemberGroup(memberGroup);
		field.setName(nationHeader);
		field.setType(stringType);
		field = groupFieldRepo.saveAndFlush(field);
		fields.add(field);
		
		// filipino
		value = new MemberGroupFieldValue();
		value.setMemberGroupField(field);
		value.setValue("NATY002");
		value.setOperand(operand1);
		value = fieldValueRepo.saveAndFlush(value);
		values.add(value);
		
		// chinese
		value = new MemberGroupFieldValue();
		value.setMemberGroupField(field);
		value.setValue("NATY004");
		value.setOperand(operand1);
		value = fieldValueRepo.saveAndFlush(value);
		values.add(value);
		
		// children
		field = new MemberGroupField();
		field.setMemberGroup(memberGroup);
		field.setName(children);
		field.setType(numericType);
		field = groupFieldRepo.saveAndFlush(field);
		fields.add(field);
		
		value = new MemberGroupFieldValue();
		value.setMemberGroupField(field);
		value.setValue("3");
		value.setOperand(operand1);
		value = fieldValueRepo.saveAndFlush(value);
		values.add(value);
		
//		field.setFieldValues(values);
//		groupFieldRepo.saveAndFlush(field);
//		values = new ArrayList<MemberGroupFieldValue>();
//		
//		memberGroup.setMemberFields(fields);
//		memberGroup = memberGroupRepo.saveAndFlush(memberGroup);
	}
	
	private void setUpMembers() {
		Calendar invalid = Calendar.getInstance();
		invalid.set(Calendar.YEAR, -16);
		invalid.set(Calendar.DATE, -1);
		Calendar valid = Calendar.getInstance();
		valid.set(Calendar.YEAR, -10);
		valid.set(Calendar.DATE, -1);
		
		// male american 2 children
		MemberModel member = new MemberModel();
		member.setAccountId("1");
		member.setUsername("member1");
		member.setContact("1");
		member.setEnabled(true);
		member.setPin("1");
		member.setAccountStatus(MemberStatus.ACTIVE);
		member.setCardType(regular);
		CustomerProfile profile = new CustomerProfile();
		profile.setNationality(american);
		profile.setGender(male);
		profile.setBirthdate(invalid.getTime());
		profile.setChildren(2);
		member.setCustomerProfile(profile);
		regularMember = memberRepo.saveAndFlush(member);
		
		// male chinese 2 children
		member = new MemberModel();
		member.setAccountId("2");
		member.setUsername("member2");
		member.setContact("2");
		member.setEnabled(true);
		member.setPin("2");
		member.setAccountStatus(MemberStatus.ACTIVE);
		member.setCardType(silver);
		profile = new CustomerProfile();
		profile.setNationality(chinese);
		profile.setGender(male);
		profile.setBirthdate(valid.getTime());
		profile.setChildren(2);
		member.setCustomerProfile(profile);
		silverMember = memberRepo.saveAndFlush(member);
		
		// male filipino 1 child
		member = new MemberModel();
		member.setAccountId("3");
		member.setUsername("member3");
		member.setContact("3");
		member.setEnabled(true);
		member.setPin("3");
		member.setAccountStatus(MemberStatus.ACTIVE);
		profile = new CustomerProfile();
		profile.setNationality(filipino);
		profile.setGender(male);
		profile.setBirthdate(valid.getTime());
		profile.setChildren(1);
		member.setCustomerProfile(profile);
		this.member = memberRepo.saveAndFlush(member);
		
		// female filipino 1 child
		member = new MemberModel();
		member.setAccountId("4");
		member.setUsername("member4");
		member.setContact("4");
		member.setEnabled(true);
		member.setPin("4");
		member.setAccountStatus(MemberStatus.ACTIVE);
		profile = new CustomerProfile();
		profile.setNationality(filipino);
		profile.setGender(female);
		profile.setBirthdate(valid.getTime());
		profile.setChildren(1);
		member.setCustomerProfile(profile);
		memberRepo.saveAndFlush(member);
		
		// male filipino 3 children
		member = new MemberModel();
		member.setAccountId("5");
		member.setUsername("member5");
		member.setContact("5");
		member.setEnabled(true);
		member.setPin("5");
		member.setAccountStatus(MemberStatus.ACTIVE);
		profile = new CustomerProfile();
		profile.setNationality(filipino);
		profile.setGender(male);
		profile.setBirthdate(valid.getTime());
		profile.setChildren(3);
		member.setCustomerProfile(profile);
		memberRepo.saveAndFlush(member);
	}
	
	private void setupCardTypeGroups() {
		MemberGroup cardTypeGroup = new MemberGroup();
		cardTypeGroup.setName("REGULAR");
		cardTypeGroup.setCardTypeGroupCode("cardtype1");
		cardTypeGroup = memberGroupRepo.saveAndFlush(cardTypeGroup);
		
		MemberGroupField cardTypeField = new MemberGroupField();
		cardTypeField.setName(cardType);
		cardTypeField.setType(stringType);
		cardTypeField.setMemberGroup(cardTypeGroup);
		cardTypeField = groupFieldRepo.saveAndFlush(cardTypeField);
		
		MemberGroupFieldValue cardTypeVal = new MemberGroupFieldValue();
		cardTypeVal.setMemberGroupField(cardTypeField);
		cardTypeVal.setValue(regular.getCode());
		cardTypeVal = fieldValueRepo.saveAndFlush(cardTypeVal);
		
		cardTypeGroup = new MemberGroup();
		cardTypeGroup.setName("SILVER");
		cardTypeGroup.setCardTypeGroupCode("cardtype2");
		cardTypeGroup = memberGroupRepo.saveAndFlush(cardTypeGroup);
		
		cardTypeField = new MemberGroupField();
		cardTypeField.setName(cardType);
		cardTypeField.setType(stringType);
		cardTypeField.setMemberGroup(cardTypeGroup);
		cardTypeField = groupFieldRepo.saveAndFlush(cardTypeField);
		
		cardTypeVal = new MemberGroupFieldValue();
		cardTypeVal.setMemberGroupField(cardTypeField);
		cardTypeVal.setValue(silver.getCode());
		cardTypeVal = fieldValueRepo.saveAndFlush(cardTypeVal);
		
		cardTypeGroup = new MemberGroup();
		cardTypeGroup.setName("REGULAR & SILVER");
		cardTypeGroup.setCardTypeGroupCode("cardtype3");
		cardTypeGroup = memberGroupRepo.saveAndFlush(cardTypeGroup);
		
		cardTypeField = new MemberGroupField();
		cardTypeField.setName(cardType);
		cardTypeField.setType(stringType);
		cardTypeField.setMemberGroup(cardTypeGroup);
		cardTypeField = groupFieldRepo.saveAndFlush(cardTypeField);
		
		cardTypeVal = new MemberGroupFieldValue();
		cardTypeVal.setMemberGroupField(cardTypeField);
		cardTypeVal.setValue(regular.getCode());
		cardTypeVal = fieldValueRepo.saveAndFlush(cardTypeVal);
		
		cardTypeVal = new MemberGroupFieldValue();
		cardTypeVal.setMemberGroupField(cardTypeField);
		cardTypeVal.setValue(silver.getCode());
		cardTypeVal = fieldValueRepo.saveAndFlush(cardTypeVal);
	}
	
	@Test
	public void testGetMemberGroup() {
		MemberGroup group = memberGroupService.getMemberGroup("group1");
		assertNotNull(group);
		
		group = memberGroupService.getMemberGroup(group.getId());
		assertNotNull(group);
	}
	
	@Test
	public void testGetQualifiedMembers() {
		List<Long> list = memberGroupService.getQualifiedMembers(memberGroup);
		assertEquals(2, list.size());
		
		list = memberGroupService.getQualifiedMembers(memberGroupService.getMemberGroupFieldsDto(memberGroup), memberGroup);
		assertEquals(2, list.size());
		
		MemberGroupRFS rfs = new MemberGroupRFS();
		rfs.setEnabled(true);
		rfs.setRecency("dateRange");
		rfs.setDateRangeFrom(new LocalDate().minusDays(1));
		rfs.setDateRangeTo(new LocalDate().plusDays(1));
		rfs.setStoreGroup(storeGroup.getId());
		rfs.setProductGroup(productGroup.getId());
		rfs.setFrequency(true);
		rfs.setFrequencyFrom(new Long(0));
		rfs.setFrequencyTo(new Long(1));
		rfs.setSpend(true);
		rfs.setSpendFrom(new Long(0));
		rfs.setSpendTo(new Long(2000));
		
		memberGroup.setRfs(rfs);
		
		MemberGroupPoints points = new MemberGroupPoints();
		points.setEnabled(true);
		points.setPointsFrom(new Long(0));
		points.setPointsTo(new Long(10));
		
		memberGroup.setPoints(points);
		
		list = memberGroupService.getQualifiedMembers(memberGroup);
		assertEquals(1, list.size());
	}
	
	@Test
	public void testGetQualifiedCardTypeGroups() {
		List<MemberGroupDto> list = memberGroupService.getQualifiedCardTypeGroups(regularMember);
		assertEquals(2, list.size());
		
		list = memberGroupService.getQualifiedCardTypeGroups(silverMember);
		assertEquals(2, list.size());
	}
	
	@Test
	public void testGetQualifiedMembersForPring() {
        List<MemberDto> list = memberGroupService.getQualifiedMembersForPrint(memberGroup, 1);
        assertEquals(2, list.size());
	}
}
