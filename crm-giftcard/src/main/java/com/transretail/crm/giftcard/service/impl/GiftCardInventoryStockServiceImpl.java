package com.transretail.crm.giftcard.service.impl;


import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.IteratorUtils;
import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.tuple.Pair;
import org.joda.time.DateTime;
import org.joda.time.LocalDate;
import org.joda.time.LocalDateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.task.AsyncTaskExecutor;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.mysema.query.BooleanBuilder;
import com.mysema.query.Tuple;
import com.mysema.query.jpa.JPQLQuery;
import com.mysema.query.jpa.impl.JPAQuery;
import com.mysema.query.types.ConstantImpl;
import com.mysema.query.types.Expression;
import com.mysema.query.types.OrderSpecifier;
import com.mysema.query.types.Predicate;
import com.mysema.query.types.Projections;
import com.mysema.query.types.expr.BooleanExpression;
import com.mysema.query.types.expr.CaseBuilder;
import com.mysema.query.types.expr.DateTimeExpression;
import com.mysema.query.types.expr.NumberExpression;
import com.mysema.query.types.path.PathBuilder;
import com.mysema.query.types.path.PathBuilderFactory;
import com.mysema.query.types.path.StringPath;
import com.mysema.query.types.template.DateTimeTemplate;
import com.transretail.crm.common.reporting.jasper.ClasspathFileResolver;
import com.transretail.crm.common.reporting.jasper.DefaultJRProcessor;
import com.transretail.crm.common.reporting.jasper.JRProcessor;
import com.transretail.crm.common.service.dto.response.CodeDescDto;
import com.transretail.crm.common.service.dto.response.ResultList;
import com.transretail.crm.common.service.util.SpringDataPagingUtil;
import com.transretail.crm.core.entity.lookup.LookupDetail;
import com.transretail.crm.core.entity.lookup.Store;
import com.transretail.crm.core.jpa.DbMetadataUtil;
import com.transretail.crm.core.service.CodePropertiesService;
import com.transretail.crm.core.service.LookupService;
import com.transretail.crm.core.service.StoreService;
import com.transretail.crm.giftcard.dto.GiftCardInfo;
import com.transretail.crm.giftcard.dto.GiftCardInventoryDto;
import com.transretail.crm.giftcard.dto.GiftCardInventorySearchDto;
import com.transretail.crm.giftcard.dto.GiftCardInventorySeriesSearchDto;
import com.transretail.crm.giftcard.dto.GiftCardInventoryStockDto;
import com.transretail.crm.giftcard.dto.GiftCardInventoryStockSearchDto;
import com.transretail.crm.giftcard.dto.GiftCardOrderReceiveItemDto;
import com.transretail.crm.giftcard.dto.QGiftCardInventoryDto;
import com.transretail.crm.giftcard.dto.QGiftCardInventoryStockDto;
import com.transretail.crm.giftcard.dto.StockRequestDto;
import com.transretail.crm.giftcard.dto.StockRequestReceiveDto;
import com.transretail.crm.giftcard.entity.GiftCardInventory;
import com.transretail.crm.giftcard.entity.GiftCardInventoryStock;
import com.transretail.crm.giftcard.entity.GiftCardInventoryStock.GCIStockStatus;
import com.transretail.crm.giftcard.entity.QGiftCardInventory;
import com.transretail.crm.giftcard.entity.QGiftCardInventoryStock;
import com.transretail.crm.giftcard.entity.QProductProfile;
import com.transretail.crm.giftcard.entity.StockRequest;
import com.transretail.crm.giftcard.entity.support.GiftCardInventoryStatus;
import com.transretail.crm.giftcard.entity.support.ProductProfileStatus;
import com.transretail.crm.giftcard.repo.GiftCardInventoryRepo;
import com.transretail.crm.giftcard.repo.GiftCardInventoryStockRepo;
import com.transretail.crm.giftcard.service.GiftCardInventoryService;
import com.transretail.crm.giftcard.service.GiftCardInventoryStockService;


@Service("giftCardInventoryStockService")
@Transactional
public class GiftCardInventoryStockServiceImpl implements GiftCardInventoryStockService {

    @Autowired
    private GiftCardInventoryStockRepo repo;
    @Autowired
    private GiftCardInventoryRepo giftCardInventoryRepo;
    @Autowired
    private GiftCardInventoryService giftCardInventoryService;
    @Autowired
    private LookupService lookupService;
    @Autowired
    private CodePropertiesService codePropertiesService;
    @Autowired
    private StoreService storeService;
    @Autowired
    private DbMetadataUtil dbMetadataUtil;
    @Value("#{'${hibernate.jdbc.batch_size}'}")
    private int batchSize;
    @PersistenceContext
    private EntityManager entityManager;
    
    @PersistenceContext
    private EntityManager em;
    @Autowired
    private AsyncTaskExecutor asyncTaskExecutor;

    //use EXCEPT for GC status with distinct GC-Inventory Stock status
    @Override
    public void saveStocks( final List<GiftCardInventory> inventories ) {
    	Runnable task = new Runnable() {
            public void run() {
            	if ( CollectionUtils.isEmpty( inventories ) ) {
            		return;
            	}

        		String status = getTxnStatus( inventories.get( 0 ).getStatus() );
            	save( inventories, status );
            }
        };
        asyncTaskExecutor.execute(task);
    }

    //use for GC status with distinct GC-Inventory Stock status ( GCIStockStatus-enum in GiftCardInventoryStock-model )
    @Override
    public void saveStocks(final List<GiftCardInventory> inventories, final GCIStockStatus status) {
    	Runnable task = new Runnable() {
            public void run() {
            	if ( CollectionUtils.isEmpty( inventories ) ) {
            		return;
            	}

            	save( inventories, status.name() );
            }
        };
        asyncTaskExecutor.execute(task);
    }

    @SuppressWarnings("unchecked")
	@Override
    public void saveStocksFromStockRequest(StockRequest stockRequest, StockRequestReceiveDto receiveDto, GCIStockStatus status) {
        if (stockRequest == null) {
            return;
        }

        List<GiftCardInventory> inventories = Lists.newArrayList();
        if (stockRequest.getQuantity() != null) {
            if (receiveDto != null) {
                GiftCardInventorySeriesSearchDto searchDto = new GiftCardInventorySeriesSearchDto();
                searchDto.setSeriesFrom(receiveDto.getStartingSeries());
                searchDto.setSeriesTo(receiveDto.getEndingSeries());
                
                BooleanExpression filter = searchDto.createSearchExpression();
                Iterable<GiftCardInventory> list = giftCardInventoryRepo.findAll(filter);
                for (GiftCardInventory gcInventory : list) {
                    if (status == GCIStockStatus.TRANSFERRED_IN) {
                    	gcInventory.setLocation(stockRequest.getAllocateTo());
                    } else {
                    	gcInventory.setLocation(stockRequest.getSourceLocation());
                    }
                }
                inventories.addAll( IteratorUtils.toList( list.iterator() ) );
            } else {
                StockRequestDto dto = new StockRequestDto(stockRequest);
                for (Pair<String, String> pairs : dto.getReservedSeriesList()) {
                    GiftCardInventorySeriesSearchDto searchDto = new GiftCardInventorySeriesSearchDto();
                    searchDto.setSeriesFrom(Long.valueOf(pairs.getLeft()));
                    searchDto.setSeriesTo(Long.valueOf(pairs.getRight()));
                    
                    BooleanExpression filter = searchDto.createSearchExpression();
                    Iterable<GiftCardInventory> list = giftCardInventoryRepo.findAll(filter);
                    for (GiftCardInventory gcInventory : list) {
                        if (status == GCIStockStatus.TRANSFERRED_IN) {
                        	gcInventory.setLocation(stockRequest.getAllocateTo());
                        } else {
                        	gcInventory.setLocation(stockRequest.getSourceLocation());
                        }
                    }
                    inventories.addAll( IteratorUtils.toList( list.iterator() ) );
                }
            }
        }
        
        if ( CollectionUtils.isNotEmpty( inventories ) ) {
            save( inventories, status.name() );
        }
    }

    @Override
    @Transactional
    public void saveStocksFromPhysicalCount ( List<GiftCardInventory> missing, List<GiftCardInventory> found ) {

		if( CollectionUtils.isNotEmpty( missing ) ){
			save( missing, GCIStockStatus.PHYSCOUNT_MISSING.name() );
		}
		if( CollectionUtils.isNotEmpty( found ) ){
			save( found, GCIStockStatus.PHYSCOUNT_FOUND.name() );
		}
    }

    @Override
    @Transactional
    public void saveStocksFromB2Activation ( Set<GiftCardInfo> activated, GCIStockStatus status ) {
    	if ( CollectionUtils.isEmpty( activated ) ) {
    		return;
    	}

    	//String location = UserUtil.getCurrentUser().getInventoryLocation();
    	saveFromB2Activation( activated, status.name() );
    }

    @Override
    public void saveStocksFromOrder( List<GiftCardOrderReceiveItemDto> received, GCIStockStatus status, String location ) {
    	if ( CollectionUtils.isEmpty( received ) ) {
    		return;
    	}

    	List<GiftCardInventoryStock> stocks = Lists.newArrayList();
    	GiftCardInventoryStock gcStock = null;
    	Iterator<GiftCardOrderReceiveItemDto> itGcs = received.listIterator();
    	LocalDate dt = new LocalDate();
    	LocalDateTime ldt = getCompiledTimesStamp( status.name(), location );
    	while ( itGcs.hasNext() ) {
    		GiftCardOrderReceiveItemDto gc = itGcs.next();

    		gcStock = new GiftCardInventoryStock();
			gcStock.setSeriesStart( gc.getStartingSeries() );
    		gcStock.setStatus( status.name() );
    		gcStock.setLocation( location );
    		gcStock.setCreatedDate( dt );
    		gcStock.setCardType( gc.getProductCode() );
    		gcStock.setCompiledTimestamp( ldt );
			gcStock.setSeriesEnd( gc.getEndingSeries() );
			gcStock.setQuantity( gc.getQuantity() );

			stocks.add( gcStock );
    	}

		if ( CollectionUtils.isEmpty( stocks ) ) {
			return;
		}

    	int insertCounter = 0;
		for ( GiftCardInventoryStock stock : stocks ) {
    		if ( insertCounter < batchSize ) {
        		repo.save( stock );
                insertCounter++;
            } else {
            	repo.saveAndFlush( stock );
                // Prevent OOM
                entityManager.clear();
                insertCounter = 0;
            }
		}
    }



    @SuppressWarnings({ "unchecked", "rawtypes" })
	@Override
	public ResultList<GiftCardInventoryStockDto> searchStock(GiftCardInventoryStockSearchDto dto) {
		Pageable pageable = SpringDataPagingUtil.INSTANCE.createPageable( dto.getPagination() );
        QGiftCardInventoryStock qModel = QGiftCardInventoryStock.giftCardInventoryStock;
		QGiftCardInventory qInventory = QGiftCardInventory.giftCardInventory;

		StringPath statusPath = qModel.status;
		NumberExpression<Long> numExp = new CaseBuilder()
			.when( statusPath.equalsIgnoreCase( GCIStockStatus.RECEIVED.name() ) ).then( qModel.quantity.sum() )
			.when( statusPath.equalsIgnoreCase( GCIStockStatus.TRANSFERRED_OUT.name() ) ).then( qModel.quantity.sum().negate() )
			.when( statusPath.equalsIgnoreCase( GCIStockStatus.TRANSFERRED_IN.name() ) ).then( qModel.quantity.sum() )
			.when( statusPath.equalsIgnoreCase( GCIStockStatus.ACTIVATED.name() ) ).then( qModel.quantity.sum().negate() )
			.when( statusPath.equalsIgnoreCase( GCIStockStatus.VOID_ACTIVATION.name() ) ).then( qModel.quantity.sum() )
			.when( statusPath.equalsIgnoreCase( GCIStockStatus.RETURNED.name() ) ).then( qModel.quantity.sum() )
			.when( statusPath.equalsIgnoreCase( GCIStockStatus.BURNED.name() ) ).then( qModel.quantity.sum().negate() )
			.when( statusPath.equalsIgnoreCase( GCIStockStatus.PHYSCOUNT.name() ) ).then( 0L )
			.when( statusPath.equalsIgnoreCase( GCIStockStatus.PHYSCOUNT_MISSING.name() ) ).then( qModel.quantity.sum().negate() )
			.when( statusPath.equalsIgnoreCase( GCIStockStatus.PHYSCOUNT_FOUND.name() ) ).then( qModel.quantity.sum() )
			.otherwise( qModel.quantity.sum() );

		BooleanBuilder expression = new BooleanBuilder(dto.createSearchExpression()
            .and( StringUtils.isNotBlank( dto.getCardType() )? qInventory.productCode.equalsIgnoreCase( dto.getCardType() ) : null )
            .and( BooleanExpression.allOf( 
                    qInventory.profile.status.in( ProductProfileStatus.APPROVED ),
                    //qInventory.series.goe( qModel.seriesStart ),
                    qInventory.series.eq( qModel.seriesEnd ) ) ));
		
		if (dto.getVendorFormalName() != null) {
		    expression.and(qInventory.order.vendor.formalName.eq(dto.getVendorFormalName()));
		}
		
		Predicate filter = expression.getValue();

		JPQLQuery query = new JPAQuery( em )
			.from( qModel, qInventory )
			.where( filter )
			.groupBy( qModel.compiledTimestamp, 
				qInventory.profile.productCode, 
				qInventory.profile.productDesc, 
				qModel.status );

		QGiftCardInventoryStockDto result = new QGiftCardInventoryStockDto( 
				qModel.compiledTimestamp,
				qInventory.profile.productCode, 
				qInventory.profile.productDesc, 
				qModel.status,
				numExp );

		int count =  query.list( result ).size();

		if ( MapUtils.isNotEmpty( dto.getPagination().getOrder() ) ) {
            PathBuilder<?> builder = new PathBuilderFactory().create( GiftCardInventoryStock.class );
            for ( Map.Entry<String, Boolean> entry : dto.getPagination().getOrder().entrySet() ) {
                Expression<Object> property = builder.get(entry.getKey());
                query.orderBy( new OrderSpecifier(entry.getValue() ? 
						com.mysema.query.types.Order.ASC : com.mysema.query.types.Order.DESC, property ) );
            }
        }
		else {
			query.orderBy( new OrderSpecifier( com.mysema.query.types.Order.DESC, qModel.compiledTimestamp ) );
		}
		query.orderBy( new OrderSpecifier( com.mysema.query.types.Order.ASC, qModel.status ) );
		if ( null != dto.getPagination() && dto.getPagination().getPageSize() >= 0 && dto.getPagination().getPageNo() >= 0 ) {
			query.offset( pageable.getOffset() ).limit( pageable.getPageSize() );
		}

		List<GiftCardInventoryStockDto> dtos = query.list( result );
		for ( GiftCardInventoryStockDto dto1 : dtos ) {
			List<Long> bals = query
					.where( qModel.compiledTimestamp.lt( new LocalDateTime( dto1.getCompiledTimestampMilli() ) ) )
					.list( numExp );
			Long beginningBal = 0L;
			for ( Long bal : bals ) {
				beginningBal += ( null != bal? bal : 0L );
			}
			dto1.setBeginningBalance( beginningBal );
			dto1.setEndingBalance( beginningBal += dto1.getQuantity() );
		}

		return new ResultList<GiftCardInventoryStockDto>( dtos, count, pageable.getPageNumber(), pageable.getPageSize() );
	}
    
    @SuppressWarnings({ "unchecked", "rawtypes" })
	@Override
    @Transactional
    public ResultList<GiftCardInventoryDto> getGiftCardInventoriesForPrintSummary(GiftCardInventorySearchDto searchForm) {
        Pageable pageable = SpringDataPagingUtil.INSTANCE.createPageable(searchForm.getPagination());
        
        QGiftCardInventory qModel = QGiftCardInventory.giftCardInventory;
        BooleanExpression filter = searchForm.createSearchExpression();
        
        JPQLQuery query = new JPAQuery( em );
        
        QGiftCardInventoryDto result = new QGiftCardInventoryDto(
                qModel.productCode,
                qModel.productName,
                qModel.location,
                qModel.status);
        
        if (filter != null) {
            query.from(qModel)
            .where(filter.and(qModel.location.isNotNull().and(qModel.status.eq(GiftCardInventoryStatus.IN_STOCK))))
            .groupBy(
                    qModel.productCode,
                    qModel.productName,
                    qModel.location,
                    qModel.status);
        } else {
            query.from(qModel)
            .where(qModel.location.isNotNull().and(qModel.status.eq(GiftCardInventoryStatus.IN_STOCK)))
            .groupBy(
                    qModel.productCode,
                    qModel.productName,
                    qModel.location,
                    qModel.status);
        }
        query.orderBy( new OrderSpecifier( com.mysema.query.types.Order.ASC, qModel.location ) );
        long count =  query.list(result).size();
        
        List<GiftCardInventoryDto> dtoList = query.distinct().list(result);
        return new ResultList<GiftCardInventoryDto>(dtoList, count, pageable.getPageNumber(), pageable.getPageSize());
    }

    @SuppressWarnings({ "unchecked", "rawtypes" })
    @Override
    @Transactional
    public ResultList<GiftCardInventoryDto> getGiftCardInventoriesForPrintDetail(GiftCardInventorySearchDto searchForm) {
        Pageable pageable = SpringDataPagingUtil.INSTANCE.createPageable(searchForm.getPagination());
        
        QGiftCardInventory qModel = QGiftCardInventory.giftCardInventory;
        BooleanExpression filter = searchForm.createSearchExpression();
        JPQLQuery query = new JPAQuery( em );
        
        QGiftCardInventoryDto result = new QGiftCardInventoryDto(
                qModel.productCode,
                qModel.productName,
                qModel.salesType,
                qModel.location,
                qModel.barcode,
                qModel.status);
        
        if (filter != null) {
            query.from(qModel)
            .where(filter.and(qModel.location.isNotNull().and(qModel.status.eq(GiftCardInventoryStatus.IN_STOCK))))
            .groupBy(
                    qModel.productCode,
                    qModel.productName,
                    qModel.salesType,
                    qModel.location,
                    qModel.barcode,
                    qModel.status);
        } else {
            query.from(qModel)
            .where(qModel.location.isNotNull().and(qModel.status.eq(GiftCardInventoryStatus.IN_STOCK)))
            .groupBy(
                    qModel.productCode,
                    qModel.productName,
                    qModel.salesType,
                    qModel.location,
                    qModel.barcode,
                    qModel.status);
        }
        query.orderBy(new OrderSpecifier( com.mysema.query.types.Order.ASC, qModel.location),
                new OrderSpecifier( com.mysema.query.types.Order.ASC, qModel.productCode),
                new OrderSpecifier( com.mysema.query.types.Order.ASC, qModel.barcode));
        long count =  query.list(result).size();
        
        List<GiftCardInventoryDto> dtoList = query.distinct().list(result);
        return new ResultList<GiftCardInventoryDto>(dtoList, count, pageable.getPageNumber(), pageable.getPageSize());
    }
    
    @Override
    @Transactional(readOnly = true)
    public List<CodeDescDto> getCardTypes() {
    	QProductProfile qPrdProf = QProductProfile.productProfile;
    	JPQLQuery query = new JPAQuery( em )
    		.from( qPrdProf )
    		.where( BooleanExpression.allOf( qPrdProf.status.in( ProductProfileStatus.APPROVED ) ) );
    	List<Tuple> tuples = query.list( qPrdProf.productCode, qPrdProf.productDesc );
    	List<CodeDescDto> li = new ArrayList<CodeDescDto>();
    	for ( Tuple tuple : tuples ) {
    		li.add( new CodeDescDto( tuple.get( qPrdProf.productCode ), tuple.get( qPrdProf.productDesc ) ) );
    	}
    	return li;
    }

    @Override
    @Transactional(readOnly = true)
    public List<CodeDescDto> getCardTypes( String location ) {
    	List<CodeDescDto> li = new ArrayList<CodeDescDto>();
    	if ( StringUtils.isBlank( location ) ) {
    		return getCardTypes();
    	}
    	QProductProfile qproduct=QProductProfile.productProfile;
    	JPAQuery query=new JPAQuery(em).from(qproduct);
    	List<Tuple> tuples = query.list( qproduct.productCode, qproduct.productDesc );
    	for ( Tuple tuple : tuples ) {
    		li.add( new CodeDescDto( tuple.get( qproduct.productCode ), tuple.get( qproduct.productDesc ) ) );
    	}
    	Collections.sort(li, ALPHA_ORDER);
    	/*
    	//query based join inventory and product, then distinct the product
    	QGiftCardInventoryStock qGcStock = QGiftCardInventoryStock.giftCardInventoryStock;
    	QGiftCardInventory qInventory = QGiftCardInventory.giftCardInventory;
    	JPQLQuery query = new JPAQuery( em )
    		.from( qGcStock, qInventory )
    		<-- FROM    CRM_GC_INVENTORY_STOCK giftcardin0_,CRM_GC_INVENTORY giftcardin1_ LEFT OUTER JOIN CRM_MEMBER_GIFT_CARD giftcardin1_1_
    		<-- ON giftcardin1_.id=giftcardin1_1_.GC_ID,CRM_GC_PROD_PROFILE productpro2_
    		.where( BooleanExpression.allOf( <-- WHERE   giftcardin1_.PROFILE_ID         =productpro2_.id
    				qGcStock.location.equalsIgnoreCase( location ) ), <-- AND lower(giftcardin0_.LOCATION)=:1
    				qInventory.profile.status.in( ProductProfileStatus.APPROVED ), <--  AND productpro2_.STATUS         =:2
    				qInventory.series.goe( qGcStock.seriesStart ), <-- AND giftcardin1_.SERIES        >=giftcardin0_.SERIES_START
    				qInventory.series.loe( qGcStock.seriesEnd ) ) <-- AND giftcardin1_.SERIES        <=giftcardin0_.SERIES_END
    		.distinct();		
    		*/
    	//List<Tuple> tuples = query.list( qInventory.profile.productCode, qInventory.profile.productDesc );
    	// <-- SELECT DISTINCT productpro2_.PRODUCT_CODE AS col_0_0_,productpro2_.PRODUCT_DESC         AS col_1_0_
    	/*
    	for ( Tuple tuple : tuples ) {
    		li.add( new CodeDescDto( tuple.get( qInventory.profile.productCode ), tuple.get( qInventory.profile.productDesc ) ) );
    	}
    	*/
    	return li;
    }

	@Override
    @Transactional(readOnly = true)
	public List<CodeDescDto> getLocations( String storeCode ) {
    	List<CodeDescDto> li = new ArrayList<CodeDescDto>();

    	if ( StringUtils.isNotBlank( storeCode ) 
    			&& storeCode.equalsIgnoreCase( codePropertiesService.getDetailInvLocationHeadOffice() ) ) {
    		LookupDetail ho = lookupService.getDetailByCode( codePropertiesService.getDetailInvLocationHeadOffice() );
    		li.add( new CodeDescDto( ho.getCode(), ho.getDescription() ) );
    		for ( Store store: storeService.getAllStores() ) {
    			li.add( new CodeDescDto( store.getCode().toString(), store.getName() ) );
    		}
    	}
    	else if ( StringUtils.isNotBlank( storeCode ) ) {
    		Store store = storeService.getStoreByCode( storeCode );
    		if ( null != store ) {
        		li.add( new CodeDescDto( store.getCode().toString(), store.getName() ) );
    		}
    	}

		return li;
	}

	private static Comparator<CodeDescDto> ALPHA_ORDER = new Comparator<CodeDescDto>() {
	    public int compare(CodeDescDto dto1, CodeDescDto dto2) {
	        int x = String.CASE_INSENSITIVE_ORDER.compare(dto1.getDesc(), dto2.getDesc());
	        if (x== 0) {
	            x= dto1.getDesc().compareTo(dto2.getDesc());
	        }
	        return x;
	    }
	};

	private String getTxnStatus( GiftCardInventoryStatus gcStatus ) {
    	if ( gcStatus == GiftCardInventoryStatus.IN_STOCK ) {
    		return GCIStockStatus.RECEIVED.name();
    	}
    	else if ( gcStatus == GiftCardInventoryStatus.IN_TRANSIT ) {
    		return GCIStockStatus.TRANSFERRED_OUT.name();
    	}
    	else if ( gcStatus == GiftCardInventoryStatus.ACTIVATED ) {
    		return GCIStockStatus.ACTIVATED.name();
    	}
    	else if ( gcStatus == GiftCardInventoryStatus.BURNED ) {
    		return GCIStockStatus.BURNED.name();
    	}
    	else if ( gcStatus == GiftCardInventoryStatus.MISSING ) {
    		return GCIStockStatus.PHYSCOUNT_MISSING.name();
    	}
    	return null;
    }

    @Transactional
	public void save( List<GiftCardInventory> gcs, String status ) {
		if ( CollectionUtils.isEmpty( gcs ) ) {
			return;
		}

    	Long seriesStart = 0L;
    	int step = 0;
    	List<GiftCardInventoryStock> stocks = Lists.newArrayList();
    	GiftCardInventoryStock gcStock = null;
    	Iterator<GiftCardInventory> itGcs = gcs.listIterator();
    	LocalDate dt = new LocalDate();
    	LocalDateTime ldt = getCompiledTimesStamp( status, gcs.get(0).getLocation() );
    	while ( itGcs.hasNext() ) {
    		GiftCardInventory gc = itGcs.next();
    		if ( step > 0 ) {
    			if ( gc.getSeries().longValue() == ( seriesStart.longValue() + step ) 
    					&& gcStock.getCardType().equalsIgnoreCase( gc.getProductCode() ) 
    					&& gcStock.getLocation().equalsIgnoreCase( gc.getLocation() )
    					&& itGcs.hasNext()  ) {
    				step++;
    				gcStock.setSeriesEnd( gc.getSeries() );
    				gcStock.setQuantity( new Long( step ) );
    				continue;
    			}
    			else {
    				stocks.add( gcStock );
    				step = 0;
    			}
    		}

    		if ( step == 0 ) {
        		step++;
				seriesStart = gc.getSeries();

				gcStock = new GiftCardInventoryStock();
				gcStock.setSeriesStart( seriesStart );
        		gcStock.setStatus( status );
        		gcStock.setLocation( gc.getLocation() );
        		gcStock.setCreatedDate( dt );
        		gcStock.setCardType( gc.getProductCode() );
        		gcStock.setCompiledTimestamp( ldt );
				gcStock.setSeriesEnd( gc.getSeries() );
				gcStock.setQuantity( new Long( step ) );
        		if ( itGcs.hasNext() ) {
    				continue;
        		}
    			else {
    				stocks.add( gcStock );
    				step = 0;
    			}
			}
    	}

		if ( CollectionUtils.isEmpty( stocks ) ) {
			return;
		}

    	int insertCounter = 0;
		for ( GiftCardInventoryStock stock : stocks ) {
    		if ( insertCounter < batchSize ) {
        		repo.save( stock );
                insertCounter++;
            } else {
            	repo.saveAndFlush( stock );
                // Prevent OOM
                entityManager.clear();
                insertCounter = 0;
            }
		}
	}

	private void saveFromB2Activation( Set<GiftCardInfo> gcs, String status ) {
		if ( CollectionUtils.isEmpty( gcs ) ) {
			return;
		}

    	Long seriesStart = 0L;
    	int step = 0;
    	List<GiftCardInventoryStock> stocks = Lists.newArrayList();
    	GiftCardInventoryStock gcStock = null;
    	Iterator<GiftCardInfo> itGcs = gcs.iterator();
    	LocalDate dt = new LocalDate();
    	while ( itGcs.hasNext() ) {
    		GiftCardInfo gc = itGcs.next();
    		if ( step > 0 ) {
    			if ( gc.getSeries().longValue() == ( seriesStart.longValue() + step ) 
    					&& gcStock.getCardType().equalsIgnoreCase( gc.getProfile().getCode() ) 
    					&& gcStock.getLocation().equalsIgnoreCase( gc.getLocation() )
    					&& itGcs.hasNext()  ) {
    				step++;
    				gcStock.setSeriesEnd( gc.getSeries() );
    				gcStock.setQuantity( new Long( step ) );
    				continue;
    			}
    			else {
    				stocks.add( gcStock );
    				step = 0;
    			}
    		}

    		if ( step == 0 ) {
        		step++;
				seriesStart = gc.getSeries();

				gcStock = new GiftCardInventoryStock();
				gcStock.setSeriesStart( seriesStart );
        		gcStock.setStatus( status );
        		gcStock.setLocation( gc.getLocation() );
        		gcStock.setCreatedDate( dt );
        		gcStock.setCardType( gc.getProfile().getCode() );
            	LocalDateTime ldt = getCompiledTimesStamp( status, gc.getLocation() );
        		gcStock.setCompiledTimestamp( ldt );
				gcStock.setSeriesEnd( gc.getSeries() );
				gcStock.setQuantity( new Long( step ) );
        		if ( itGcs.hasNext() ) {
    				continue;
        		}
    			else {
    				stocks.add( gcStock );
    				step = 0;
    			}
			}
    	}

		if ( CollectionUtils.isEmpty( stocks ) ) {
			return;
		}

    	int insertCounter = 0;
		for ( GiftCardInventoryStock stock : stocks ) {
    		if ( insertCounter < batchSize ) {
        		repo.save( stock );
                insertCounter++;
            } else {
            	repo.saveAndFlush( stock );
                // Prevent OOM
                entityManager.clear();
                insertCounter = 0;
            }
		}
	}
	
	private LocalDateTime getCompiledTimesStamp( String status, String location ) {
		QGiftCardInventoryStock qStock = QGiftCardInventoryStock.giftCardInventoryStock;
		JPQLQuery query = new JPAQuery( em )
			.from( qStock )
			.where( 
				qStock.status.equalsIgnoreCase( status )
					.and( qStock.location.equalsIgnoreCase( location ) )
					.and( qStock.compiledTimestamp.goe( new LocalDateTime().hourOfDay().roundFloorCopy() ) )
					.and( qStock.compiledTimestamp.lt( new LocalDateTime().hourOfDay().roundCeilingCopy() ) ) );
		LocalDateTime dt = query.singleResult( qStock.compiledTimestamp.min() );
		return dt != null? dt : new LocalDateTime();
	}



    private static final String PARAM_DATE_RANGE = "dateRange";
	private static final String REPORT_NAME = "reports/loyalty_day-to-day_tracking.jasper";
	private static final String PARAM_IS_EMPTY = "isEmpty";
	private static final DateTimeFormatter FULL_DATETIME_PATTERN = DateTimeFormat.forPattern("MMMM d, yyyy HH:mm");
	private static final DateTimeFormatter FULL_DATE_PATTERN = DateTimeFormat.forPattern("MMMM d, yyyy");

//    private static final String DATE_FORMAT_H2 = "yyyy-MM-dd";
//    private static final String DATE_TRUNC_UNIT_ORACLE = "hh24:mi:ss";

	@Override
	public JRProcessor createJrProcessor( GiftCardInventoryStockSearchDto searchDto ) {
		searchDto.getPagination().setPageSize(-1);
		
		ResultList<GiftCardInventoryStockDto> results = searchStock( searchDto );
		
    	Map<String, Object> params = Maps.newHashMap();
    	StringBuffer dateStr = new StringBuffer();
    	if( searchDto.getCreatedFromMilli() != null ) {
    		dateStr.append( FULL_DATE_PATTERN.print( new LocalDateTime( searchDto.getCreatedFromMilli() ) ) );
    	}
    	dateStr.append( " - " );
    	if( searchDto.getCreatedToMilli() != null ) {
    		dateStr.append( FULL_DATE_PATTERN.print( new LocalDateTime( searchDto.getCreatedToMilli() ) ) );
    	}
    	params.put( PARAM_DATE_RANGE, dateStr.toString() );
//    	parameters.put(PARAM_CARD_TYPE, filterDto.getProduct().getCode() + " - " + filterDto.getProduct().getDescription());
//    	parameters.put(PARAM_LOCATION, filterDto.getLocation() + " - " + loyaltyCardService.getInventoryDesc(filterDto.getLocation()));
    	List<ReportBean> beansDataSource = Lists.newArrayList();
    	if ( results.getResults().isEmpty() ) {
            params.put( PARAM_IS_EMPTY, true );
            beansDataSource.add( new ReportBean() );
        } 
    	else {
        	for( GiftCardInventoryStockDto tracker : results.getResults() ) {
        		beansDataSource.add( 
        				new ReportBean( 
        						FULL_DATETIME_PATTERN.print( new LocalDateTime( tracker.getCreatedDate() ) ),
			    				tracker.getBeginningBalance(),
			    				tracker.getTransaction(),
			    				tracker.getQuantity(),
			    				tracker.getEndingBalance() ) );
        	}
        }
        DefaultJRProcessor jrProcessor = new DefaultJRProcessor(REPORT_NAME, beansDataSource);
        jrProcessor.addParameters(params);
        jrProcessor.setFileResolver(new ClasspathFileResolver("reports"));
        return jrProcessor;
    }

	public static class ReportBean {
		private String date;
		private Long begBal;
		private String transaction;
		private Long quantity;
		private Long endBal;

		public ReportBean() {};
		public ReportBean( 
				String date, 
				Long begBal, 
				String transaction,
				Long quantity, 
				Long endBal ) {
			super();
			this.date = date;
			this.begBal = begBal;
			this.transaction = transaction;
			this.quantity = quantity;
			this.endBal = endBal;
		}

		public String getDate() {
			return date;
		}
		public void setDate(String date) {
			this.date = date;
		}
		public Long getBegBal() {
			return begBal;
		}
		public void setBegBal(Long begBal) {
			this.begBal = begBal;
		}
		public String getTransaction() {
			return transaction;
		}
		public void setTransaction(String transaction) {
			this.transaction = transaction;
		}
		public Long getQuantity() {
			return quantity;
		}
		public void setQuantity(Long quantity) {
			this.quantity = quantity;
		}
		public Long getEndBal() {
			return endBal;
		}
		public void setEndBal(Long endBal) {
			this.endBal = endBal;
		}
	}

	@SuppressWarnings("unused")
	private List<GiftCardInventoryStockDto> toDtoList(List<GiftCardInventoryStock> stocks) {
        if (stocks.isEmpty()) {
            return new ArrayList<GiftCardInventoryStockDto>();
        }
        
        List<GiftCardInventoryStockDto> dtos = new ArrayList<GiftCardInventoryStockDto>();
        for (GiftCardInventoryStock stock : stocks) {
            GiftCardInventoryStockDto dto = new GiftCardInventoryStockDto();
            dto.setInventory(stock.getInventory());
            dto.setLocation(stock.getLocation());
            dto.setStatus(stock.getStatus());
            dto.setTransferFrom(stock.getTransferFrom());
            dto.setTransferTo(stock.getTransferTo());
            dtos.add(dto);
        }
        
        return dtos;
    }

    @SuppressWarnings("unused")
	private DateTimeExpression<DateTime> toDate(Expression<?> expr, String pattern){
        return DateTimeTemplate.create(DateTime.class, "to_date({0},  '{1s}')", 
                expr, 
                ConstantImpl.create(pattern));
    }
    
    @SuppressWarnings("unused")
	private DateTimeExpression<DateTime> trunc(Expression<?> expr, String unit){
        return DateTimeTemplate.create(DateTime.class, "trunc({0},  '{1s}')", 
                expr, 
                ConstantImpl.create(unit));
    }
    
}
