<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@include file="../../common/taglibs.jsp" %>
<div id="section-card-info" class="hide">
  <fieldset class="form-horizontal container-fluid mt20 pl0 pr0">
    <legend><spring:message code="gc.service.request.page.section.card.info"/></legend>
    <fieldset class="span5">
      <label class="block-stack-title" style="border-bottom: 1px solid #E8E8E8; margin-bottom: 20px; padding: 5px 0 4px;">
	<spring:message code="gc.service.request.card.info.section.basic" />
      </label>
      <div class="control-group">
	<spring:message code="gc.service.request.field.card.no" var="cardInfoCardNoLabel" />
	<label for="cardInfoCardNo" class="control-label">${cardInfoCardNoLabel}</label>
	<div class="controls">
	  <input type="text" name="cardInfoCardNo" disabled="disabled" placeholder="${cardInfoCardNoLabel}"/>
	</div>	   
      </div>	   
      <div class="control-group">
	<spring:message code="gc.service.request.field.status" var="cardInfoStatusLabel"/>
	<label for="cardInfoStatus" class="control-label">${cardInfoStatusLabel}</label>
	<div class="controls">
	  <input type="text" name="cardInfoStatus" disabled="disabled" placeholder="${cardInfoStatusLabel}"/>
	</div>
      </div>
      <div class="control-group">
	<spring:message code="gc.service.request.field.balance" var="balanceLabel" />
	<label for="balance" class="control-label">${balanceLabel}</label>
	<div class="controls">
	  <input type="text" name="balance" disabled="disabled" placeholder="${balanceLabel}" style="text-align: right"/>
	</div>
      </div>
      <div class="control-group">
	<spring:message code="gc.service.request.field.prev.balance" var="previousBalanceLabel" />
	<label for="previousBalance" class="control-label">${previousBalanceLabel}</label>
	<div class="controls">
	  <input type="text" name="previousBalance" disabled="disabled" placeholder="${previousBalanceLabel}" style="text-align: right"/>
	</div>
      </div>
      <div class="control-group">
	<spring:message code="gc.service.request.field.expiry.date" var="expiryDateLabel" />
	<label for="expiryDate" class="control-label">${expiryDateLabel}</label>
	<div class="controls">
	  <input type="text" name="expiryDate" disabled="disabled" placeholder="${expiryDateLabel}"/>
	</div>
      </div>
    </fieldset>
    <fieldset class="span5">
      <label class="block-stack-title" style="border-bottom: 1px solid #E8E8E8; margin-bottom: 20px; padding: 5px 0 4px;">
	<spring:message code="gc.service.request.card.info.section.product.profile" />
      </label>
      <div class="control-group">
	<spring:message code="gc.service.request.field.product" var="productLabel" />
	<label for="product" class="control-label">${productLabel}</label>
	<div class="controls">
	  <input type="text" name="product" disabled="disabled" placeholder="${productLabel}"/>
	</div>
      </div>
      <div class="control-group">
	<spring:message code="gc.service.request.field.face.amount" var="faceAmountLabel"/>
	<label for="faceValue" class="control-label">${faceAmountLabel}</label>
	<div class="controls">
	  <input type="text" name="faceValue" disabled="disabled" placeholder="${faceAmountLabel}" style="text-align: right"/>
	</div>
      </div>
      <div class="control-group">
	<spring:message code="gc.service.request.field.allow.partial.redeem" var="allowPartialRedeemLabel"/>
	<label for="allowPartialRedeem" class="control-label">${allowPartialRedeemLabel}</label>
	<div class="controls">
	  <input type="checkbox" name="allowPartialRedeem" disabled="disabled" class="checkbox checkbox-inline" />
	</div>
      </div>
      <div class="control-group">
	<spring:message code="gc.service.request.field.reloadable" var="reloadableLabel"/>
	<label for="reloadable" class="control-label">${reloadableLabel}</label>
	<div class="controls">
	  <input type="checkbox" name="reloadable" disabled="disabled" class="checkbox checkbox-inline"/>
	</div>
      </div>
      <div class="control-group">
	<spring:message code="gc.service.request.field.unit.cost" var="unitCostLabel" />
	<label for="unitCost" class="control-label">${unitCostLabel}</label>
	<div class="controls">
	  <input type="text" name="unitCost" disabled="disabled" placeholder="${unitCostLabel}" style="text-align: right"/>
	</div>
      </div>
    </fieldset>
    <fieldset class="span5">
      <label class="block-stack-title" style="border-bottom: 1px solid #E8E8E8; margin-bottom: 20px; padding: 5px 0 4px;">
	<spring:message code="gc.service.request.card.info.section.sales" />
      </label>
      <div class="control-group">
	<spring:message code="gc.service.request.field.order.no" var="orderNoLabel" />
	<label for="orderNo" class="control-label">${orderNoLabel}</label>
	<div class="controls">
	  <input type="text" name="orderNo" disabled="disabled" placeholder="${orderNoLabel}"/>
	</div>
      </div>
      <div class="control-group">
	<spring:message code="gc.service.request.field.customer.id" var="customerIdLabel" />
	<label for="customerId" class="control-label">${customerIdLabel}</label>
	<div class="controls">
	  <input type="text" name="customerId" disabled="disabled" placeholder="${customerIdLabel}"/>
	</div>
      </div>
      <div class="control-group">
	<spring:message code="gc.service.request.field.sale.store" var="saleStore"/>
	<label for="saleStore" class="control-label">${saleStore}</label>
	<div class="controls">
	  <input type="text" name="saleStore" disabled="disabled" placeholder="${saleStore}"/>
	</div>
      </div>
      <div class="control-group">
	<spring:message code="gc.service.request.field.sale.terminal.id" var="terminalIdLabel"/>
	<label for="saleTerminalId" class="control-label">${terminalIdLabel}</label>
	<div class="controls">
	  <input type="text" name="saleTerminalId" disabled="disabled" placeholder="${terminalIdLabel}"/>
	</div>
      </div>
      <!--
      <div class="control-group">
      <spring:message code="gc.service.request.field.activate.time" var="activateTimeLabel" />
      <label for="activateTime" class="control-label">${activateTimeLabel}</label>
      <div class="controls">
	<input type="text" name="activateTime" disabled="disabled" placeholder="${activateTimeLabel}"/>
      </div>
    </div>
    <div class="control-group">
      <spring:message code="gc.service.request.field.activate.user" var="activateUserLabel" />
      <label for="activateUser" class="control-label">${activateUserLabel}</label>
      <div class="controls">
	<input type="text" name="activateUser" disabled="disabled" placeholder="${activateUserLabel}"/>
      </div>
    </div>
      -->
    </fieldset>
    <fieldset class="span5">
      <label class="block-stack-title" style="border-bottom: 1px solid #E8E8E8; margin-bottom: 20px; padding: 5px 0 4px;">
	<spring:message code="gc.service.request.card.info.section.movement" />
      </label>
       <div class="control-group">
	<spring:message code="gc.service.request.field.mo.no" var="moNoLabel"/>
	<label for="moNo" class="control-label">${moNoLabel}</label>
	<div class="controls">
	  <input type="text" name="moNo" disabled="disabled" placeholder="${moNoLabel}"/>
	</div>
      </div>
      <div class="control-group">
	<spring:message code="gc.service.request.field.allocated.to" var="allocatedToLabel"/>
	<label for="allocatedTo" class="control-label">${allocatedToLabel}</label>
	<div class="controls">
	  <input type="text" name="allocatedTo" disabled="disabled" placeholder="${allocatedToLabel}"/>
	</div>
      </div>
      <div class="control-group">
	<spring:message code="gc.service.request.field.last.inv.location" var="lastInvLocLabel" />
	<label for="lastInvLoc" class="control-label">${lastInvLocLabel}</label>
	<div class="controls">
	  <input type="text" name="lastInvLoc" disabled="disabled" placeholder="${lastInvLocLabel}"/>
	</div>
      </div>
    </fieldset>
    <fieldset class="span5">
      <label class="block-stack-title" style="border-bottom: 1px solid #E8E8E8; margin-bottom: 20px; padding: 5px 0 4px;">
	<spring:message code="gc.service.request.card.info.section.auditing" />
      </label>
      <div class="control-group">
	<spring:message code="gc.service.request.field.created.date" var="createdDateTimeLabel" />
	<label for="createdDateTime" class="control-label">${createdDateTimeLabel}</label>
	<div class="controls">
	  <input type="text" name="createdDateTime" disabled="disabled" placeholder="${createdDateTimeLabel}"/>
	</div>
      </div>  
      <div class="control-group">
	<spring:message code="gc.service.request.field.created.by" var="createdByLabel"/>
	<label for="createdBy" class="control-label">${createdByLabel}</label>
	<div class="controls">
	  <input type="text" name="createdBy" disabled="disabled" placeholder="${createdByLabel}"/>
	</div>
      </div>
    </fieldset>
  </fieldset>
</div>
<div class="clearfix"></div>
<div id="section-card-info-enable-disable" class="hide" >
  <fieldset class="form-horizontal container-fluid">
    <legend>Enable/Disable Gift Card</legend>
    <div class="control-group">
      <div class="controls">
	<button type="button" id="enableDisableBtn" class="btn btn-primary" 
		data-label-enable="<spring:message code='gc.service.request.button.enable.gc' />"
		data-label-disable="<spring:message code='gc.service.request.button.disable.gc' />"
		data-loading-text="Processing..."></button>
      </div>
    </div>
  </fieldset>
</div>


<style type="text/css">

  #section-card-info .form-horizontal .controls {
      margin-left: 170px;
  }
  
</style>