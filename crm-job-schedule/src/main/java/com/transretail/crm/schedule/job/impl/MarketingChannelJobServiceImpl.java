package com.transretail.crm.schedule.job.impl;


import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.time.DateUtils;
import org.quartz.CronExpression;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import com.google.common.collect.Lists;
import com.transretail.crm.core.dto.MessageDto;
import com.transretail.crm.core.entity.ApplicationConfig;
import com.transretail.crm.core.entity.MarketingChannel;
import com.transretail.crm.core.entity.MarketingChannel.ChannelType;
import com.transretail.crm.core.entity.MemberModel;
import com.transretail.crm.core.entity.enums.AppConfigDefaults;
import com.transretail.crm.core.entity.enums.AppKey;
import com.transretail.crm.core.repo.ApplicationConfigRepo;
import com.transretail.crm.core.service.CrmFileService;
import com.transretail.crm.core.service.MarketingChannelService;
import com.transretail.crm.media.service.MailService;
import com.transretail.crm.media.service.SmsService;
import com.transretail.crm.schedule.job.service.MarketingChannelJobService;
import com.transretail.crm.schedule.trigger.MarketingChannelSchedulerTrigger;


@Service("marketingChannelJobSvc")
public class MarketingChannelJobServiceImpl implements MarketingChannelJobService {

	@Autowired
	MarketingChannelService marketingChannelService;
	@Autowired
	MailService mailService;
	@Autowired
	SmsService smsService;
	@Autowired
	CrmFileService crmFileService;
	@Autowired
	ApplicationConfigRepo applicationConfigRepo;
	@Autowired
	private MessageSource messageSource;

	private static final Logger LOGGER = LoggerFactory.getLogger( MarketingChannelJobServiceImpl.class );



	/*
	 * marketingChannelJob-object method referenced 
	 * in applicationContext-scheduler-media.xml 
	 * */
	@Override
	public List<MarketingChannel> send() {
		ApplicationConfig appConfig = applicationConfigRepo.findByKey( AppKey.MARKETING_CHANNEL );
		try {
			LOGGER.info( ":send:: job executing at " + new Date().toString() );
			CronExpression cronExp = new CronExpression( ( 
					null != appConfig )? appConfig.getValue() : AppConfigDefaults.DEFAULT_MARKETING_CHANNEL_CRON );
			Date dt = DateUtils.addMinutes( cronExp.getNextValidTimeAfter( new Date() ), -15 );
			List<MarketingChannel> channels = marketingChannelService.getScheduled( dt );

			LOGGER.info( ":send:: media scheduled for sending at " + dt + " :: " + ( CollectionUtils.isNotEmpty( channels )? channels.size() : 0 ) );

			if ( CollectionUtils.isNotEmpty( channels ) ) {
				for ( MarketingChannel channel : channels ) {
					if ( ChannelType.email == channel.getType() ) {
						sendEmail( channel );
					}
					else if ( ChannelType.sms == channel.getType() ) {
						sendSms( channel );
					}
				}
				LOGGER.info( ":send:: job successfully executed at " + new Date().toString() );
			}
			
		} 
		catch (ParseException e) {
			e.printStackTrace();
		}
		return null;
	}



	private boolean sendEmail( MarketingChannel model ) {
    	MessageDto dto = new MessageDto();
    	dto.setSalutation( messageSource.getMessage( "channel_msg_customer", new String[]{}, LocaleContextHolder.getLocale() ) );
    	dto.setMessage( model.getMessage() );
    	dto.setSubject( model.getDescription() );

    	List<MemberModel> recipients = marketingChannelService.getTargetRecipients( model.getMemberGroup(), model.getPromotion() );
    	if ( CollectionUtils.isNotEmpty( recipients ) ) {
    		List<String> emailAddresses = new ArrayList<String>();
    		for ( MemberModel member : recipients ) {
    			if ( StringUtils.isNotBlank( member.getEmail() ) ) {
        			emailAddresses.add( member.getEmail() );
    			}
    		}
    		dto.setRecipients( emailAddresses.toArray( new String[emailAddresses.size()] ) );

    		if ( StringUtils.isNotBlank( model.getFileId() ) ) {
    			MultipartFile file = crmFileService.getMultipartFile( Long.valueOf( model.getFileId() ) );
    			if ( null != file && file.getSize() > 0 ) {
					dto.setFile( file );
				}
    		}

    		if ( CollectionUtils.isNotEmpty( emailAddresses ) ) {
    			return mailService.sendWithHtml( dto );
    		}
    	}

		return false;
	}

	private boolean sendSms( MarketingChannel model ) {
    	MessageDto dto = new MessageDto();
    	dto.setSalutation( messageSource.getMessage( "channel_msg_customer", new String[]{}, LocaleContextHolder.getLocale() ) );
    	dto.setMessage( model.getMessage() );
    	dto.setSubject( model.getDescription() );

    	List<MemberModel> recipients = marketingChannelService.getTargetRecipients( model.getMemberGroup(), model.getPromotion() );
    	if ( CollectionUtils.isNotEmpty( recipients ) ) {
    		Set<String> contactNos = new HashSet<String>();
    		for ( MemberModel member : recipients ) {
    			if ( StringUtils.isNotBlank( member.getContact() ) ) {
        			contactNos.addAll( Lists.newArrayList(member.getContact().split(",")) );
    			}
    		}
    		dto.setRecipients( contactNos.toArray( new String[contactNos.size()] ) );

    		if ( CollectionUtils.isNotEmpty( contactNos ) ) {
    			return smsService.send( dto );
    		}
    	}

		return false;
	}

}
