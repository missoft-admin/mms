package com.transretail.crm.giftcard.repo;

import com.transretail.crm.common.repo.CrmQueryDslPredicateExecutor;
import com.transretail.crm.giftcard.entity.GiftCardTransaction;
import com.transretail.crm.giftcard.entity.SalesOrder;
import com.transretail.crm.giftcard.entity.support.GiftCardSaleTransaction;
import com.transretail.crm.giftcard.repo.custom.GiftCardTransactionRepoCustom;
import org.springframework.stereotype.Repository;

/**
 *
 * @author Monte Cillo Co (mco@exist.com)
 */
@Repository
public interface GiftCardTransactionRepo extends CrmQueryDslPredicateExecutor<GiftCardTransaction, Long>, GiftCardTransactionRepoCustom {

    GiftCardTransaction findByTransactionNoAndTransactionType(String transactioNo, GiftCardSaleTransaction transaction);
    
    GiftCardTransaction findBySalesOrder(SalesOrder salesOrder);
}
