package com.transretail.crm.giftcard.entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.transretail.crm.core.entity.support.CustomAuditableEntity;

/**
 *
 */
@Entity
@Table(name = "CRM_B2B_EXH")
public class B2BExhibition extends CustomAuditableEntity<Long> implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@OneToMany(mappedBy = "exh", orphanRemoval = true, fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	private List<B2BExhibitionGc> gcs;
	@OneToMany(mappedBy = "exh", orphanRemoval = true, fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	private List<B2BExhibitionPayments> payments;
	@Column(name = "DISCOUNT")
	private BigDecimal discount = BigDecimal.ZERO;
	
	@Column(name = "TAX")
	private Double tax = new Double(0);
	@Column(name = "TAXABLE_AMOUNT")
	private BigDecimal taxableAmount = BigDecimal.ZERO;
	@Column(name = "TOTAL_PAYMENT")
	private BigDecimal totalPayment = BigDecimal.ZERO;
	
	@Column(name = "TRANSACTION_NO")
	private String transactionNo;
	
	@Column(name = "KWITANSI_NO")
	private String kwitansiNo;
	@Column(name = "B2B_LOCATION")
	private String b2bLocation;
	
	public List<B2BExhibitionGc> getGcs() {
		return gcs;
	}
	public void setGcs(List<B2BExhibitionGc> gcs) {
		this.gcs = gcs;
	}
	public List<B2BExhibitionPayments> getPayments() {
		return payments;
	}
	public void setPayments(List<B2BExhibitionPayments> payments) {
		this.payments = payments;
	}
	public BigDecimal getDiscount() {
		return discount;
	}
	public void setDiscount(BigDecimal discount) {
		this.discount = discount;
	}
	public Double getTax() {
		return tax;
	}
	public void setTax(Double tax) {
		this.tax = tax;
	}
	public BigDecimal getTaxableAmount() {
		return taxableAmount;
	}
	public void setTaxableAmount(BigDecimal taxableAmount) {
		this.taxableAmount = taxableAmount;
	}
	public BigDecimal getTotalPayment() {
		return totalPayment;
	}
	public void setTotalPayment(BigDecimal totalPayment) {
		this.totalPayment = totalPayment;
	}
	public String getTransactionNo() {
		return transactionNo;
	}
	public void setTransactionNo(String transactionNo) {
		this.transactionNo = transactionNo;
	}
	public String getKwitansiNo() {
		return kwitansiNo;
	}
	public void setKwitansiNo(String kwitansiNo) {
		this.kwitansiNo = kwitansiNo;
	}
	public String getB2bLocation() {
		return b2bLocation;
	}
	public void setB2bLocation(String b2bLocation) {
		this.b2bLocation = b2bLocation;
	}
	
	
	
	
	
}
