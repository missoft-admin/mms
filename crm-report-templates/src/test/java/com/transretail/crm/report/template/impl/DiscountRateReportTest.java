package com.transretail.crm.report.template.impl;

import static org.junit.Assert.*;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import com.google.common.collect.Sets;
import org.joda.time.LocalDate;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.transretail.crm.giftcard.entity.SalesOrder;
import com.transretail.crm.giftcard.entity.SalesOrderItem;
import com.transretail.crm.giftcard.entity.support.SalesOrderStatus;
import com.transretail.crm.giftcard.entity.support.SalesOrderType;
import com.transretail.crm.giftcard.repo.SalesOrderItemRepo;
import com.transretail.crm.giftcard.repo.SalesOrderRepo;
import com.transretail.crm.report.template.impl.DiscountRateReport.DiscountRateBean;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath*:/META-INF/spring/applicationContext*.xml",
    "/META-INF/spring/applicationContext-test.xml"})
public class DiscountRateReportTest {
	@PersistenceContext
    private EntityManager em;
	@Autowired
	private SalesOrderRepo salesOrderRepo;
	@Autowired
	private SalesOrderItemRepo salesOrderItemRepo;
	@Autowired
	private DiscountRateReport instance;
	
	private DateTimeFormatter formatter = DateTimeFormat.forPattern("dd-MM-yyyy");

	/**
	 * January, 2014:
	 * 	Order 1 - 50%, 3000
	 * 	Order 2 - 20%, 1000
	 * 	Discount Value: 1700
	 * 	Sales Value: 4000
	 *  Discount Rate: 42.5%
	 * February, 2014:
	 * 	Order 3 - 20%, 1000
	 * January, 2015:
	 * 	Order 4 - 20%, 1000
	 * @throws Exception
	 */
	@Before
	public void setUp() throws Exception {
		SalesOrder order = new SalesOrder();
		order.setOrderNo("1");
		order.setOrderDate(LocalDate.parse("01-01-2014", formatter));
		order.setDiscountVal(BigDecimal.valueOf(50L));
		order.setStatus(SalesOrderStatus.SOLD);
		order.setOrderType(SalesOrderType.B2B_SALES);
		order = salesOrderRepo.save(order);
		
		List<SalesOrderItem> items = Lists.newArrayList();
		SalesOrderItem item = new SalesOrderItem();
		item.setFaceAmount(BigDecimal.valueOf(1000L));
		item.setOrder(order);
		items.add(salesOrderItemRepo.save(item));
		
		item = new SalesOrderItem();
		item.setFaceAmount(BigDecimal.valueOf(2000L));
		item.setOrder(order);
		items.add(salesOrderItemRepo.save(item));
		
		order.setItems(Sets.newHashSet(items));
//		salesOrderRepo.save(order);
		
		order = new SalesOrder();
		order.setOrderNo("2");
		order.setOrderDate(LocalDate.parse("05-01-2014", formatter));
		order.setDiscountVal(BigDecimal.valueOf(20L));
		order.setStatus(SalesOrderStatus.SOLD);
		order.setOrderType(SalesOrderType.B2B_SALES);
		order = salesOrderRepo.save(order);
		
		items = Lists.newArrayList();
		item = new SalesOrderItem();
		item.setFaceAmount(BigDecimal.valueOf(1000L));
		item.setOrder(order);
		items.add(salesOrderItemRepo.save(item));
		
		order.setItems(Sets.newHashSet(items));
//		salesOrderRepo.save(order);
		
		order = new SalesOrder();
		order.setOrderNo("3");
		order.setOrderDate(LocalDate.parse("02-02-2014", formatter));
		order.setDiscountVal(BigDecimal.valueOf(20L));
		order.setStatus(SalesOrderStatus.SOLD);
		order.setOrderType(SalesOrderType.B2B_SALES);
		order = salesOrderRepo.save(order);
		
		items = Lists.newArrayList();
		item = new SalesOrderItem();
		item.setFaceAmount(BigDecimal.valueOf(1000L));
		item.setOrder(order);
		items.add(salesOrderItemRepo.save(item));
		
		order.setItems(Sets.newHashSet(items));
//		salesOrderRepo.save(order);
		
		order = new SalesOrder();
		order.setOrderNo("4");
		order.setOrderDate(LocalDate.parse("02-01-2015", formatter));
		order.setDiscountVal(BigDecimal.valueOf(20L));
		order.setStatus(SalesOrderStatus.SOLD);
		order.setOrderType(SalesOrderType.B2B_SALES);
		order = salesOrderRepo.save(order);
		
		items = Lists.newArrayList();
		item = new SalesOrderItem();
		item.setFaceAmount(BigDecimal.valueOf(1000L));
		item.setOrder(order);
		items.add(salesOrderItemRepo.save(item));
		
		order.setItems(Sets.newHashSet(items));
//		salesOrderRepo.save(order);
	}

	@Test
	public void test() {
		Map<String, Object> map = Maps.newHashMap();
		DateTimeFormatter formatter = DateTimeFormat.forPattern("yyyy");
		LocalDate dateFrom = LocalDate.parse("2014", formatter);
		LocalDate dateTo = LocalDate.parse("2014", formatter);
		
		List<DiscountRateBean> list = instance.createDatasource(dateFrom, dateTo, null, null, null, map);
		assertEquals(1L, list.size());
		List<String> discountList = list.get(0).getMonthList();
		assertEquals("42.50", discountList.get(0));
		assertEquals("20.00", discountList.get(1));
		assertEquals("38.00", list.get(0).getAverage());
		assertEquals("38.00", map.get("AVERAGE_VALUE"));
		
		dateFrom = LocalDate.parse("2014", formatter);
		dateTo = LocalDate.parse("2015", formatter);
		list = instance.createDatasource(dateFrom, dateTo, null, null, null, map);
		assertEquals(2L, list.size());
		discountList = list.get(0).getMonthList();
		assertEquals("42.50", discountList.get(0));
		assertEquals("20.00", discountList.get(1));
		assertEquals("38.00", list.get(0).getAverage());
		discountList = list.get(1).getMonthList();
		assertEquals("20.00", discountList.get(1));
		assertEquals("20.00", list.get(1).getAverage());
		assertEquals("35.00", map.get("AVERAGE_VALUE"));
	}

}
