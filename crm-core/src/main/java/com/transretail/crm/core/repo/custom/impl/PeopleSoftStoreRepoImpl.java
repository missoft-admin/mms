package com.transretail.crm.core.repo.custom.impl;

import com.google.common.base.Objects;
import com.mysema.query.jpa.impl.JPAQuery;
import com.transretail.crm.core.entity.PeopleSoftStore;
import com.transretail.crm.core.entity.QPeopleSoftStore;
import com.transretail.crm.core.repo.custom.PeopleSoftStoreRepoCustom;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

public class PeopleSoftStoreRepoImpl implements PeopleSoftStoreRepoCustom {

    @PersistenceContext
    private EntityManager em;

    @Override
    public PeopleSoftStore findByStoreCode(String storeCode) {
        QPeopleSoftStore qp = QPeopleSoftStore.peopleSoftStore;
        return new JPAQuery(em).from(qp)
                .where(qp.store.code.eq(Objects.firstNonNull(storeCode, "")))
                .singleResult(qp);
    }
}
