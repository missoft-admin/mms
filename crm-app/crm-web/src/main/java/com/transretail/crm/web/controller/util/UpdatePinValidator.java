package com.transretail.crm.web.controller.util;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

import com.transretail.crm.core.dto.UpdatePinDto;

@Component
public class UpdatePinValidator implements Validator {
    @Autowired
    MessageSource messageSource;

    private final static String ITS_DEFAULTMSG_INVALID   = "Info given is invalid.";
    private final static String ITS_PROPKEY_EMPTYMSG     = "propkey_msg_notempty";
    private final static String ITS_PROPKEY_PIN   	     = "pin";
	private final static String ITS_PROPKEY_CONFIRMPIN   = "confirmPin";
	private final static String ITS_PROPKEY_PREARGUMENT  = "propkey_member_";
	private final static String ITS_PROPKEY_CONFIRM      = "constraints.Confirm.message";
	private final static String ITS_PROPKEY_PIN_LENGTH	 = "propkey_pin_invalid_length";
	private final static String ITS_PROPKEY_PIN_FORMAT	 = "propkey_pin_invalid_format";
	
	@Override
	public boolean supports(Class<?> clazz) {
		return UpdatePinDto.class.isAssignableFrom(clazz);
	}

	@Override
	public void validate(Object target, Errors errors) {
		validateEmptyFields(errors);
		validateConfirmConstraints((UpdatePinDto) target, errors);
	}
	
	private void validateEmptyFields( Errors inErrors ) {
		ValidationUtils.rejectIfEmptyOrWhitespace(inErrors, ITS_PROPKEY_PIN, 
				getMessage( ITS_PROPKEY_EMPTYMSG, new Object[] { (ITS_PROPKEY_PREARGUMENT + ITS_PROPKEY_PIN) } ) );
		ValidationUtils.rejectIfEmptyOrWhitespace(inErrors, ITS_PROPKEY_CONFIRMPIN, 
				getMessage( ITS_PROPKEY_EMPTYMSG, new Object[] { (ITS_PROPKEY_PREARGUMENT + ITS_PROPKEY_CONFIRMPIN) } ) );
	}
	
	private void validateConfirmConstraints(UpdatePinDto updatePinDto, Errors inErrors) {
		String pin = updatePinDto.getPin();
		if(!StringUtils.isBlank(pin)) {
			if(pin.length() != 6) {
				inErrors.reject( getMessage( ITS_PROPKEY_PIN_LENGTH, 
						new Object[] { (ITS_PROPKEY_PREARGUMENT + ITS_PROPKEY_PIN) } ) );
			}
			
			if(!pin.matches("\\d+")) {
				inErrors.reject( getMessage( ITS_PROPKEY_PIN_FORMAT, 
						new Object[] { (ITS_PROPKEY_PREARGUMENT + ITS_PROPKEY_PIN) } ) );
			}
		}
		
		if(StringUtils.isNotEmpty(updatePinDto.getPin()) && StringUtils.isNotEmpty(updatePinDto.getConfirmPin()) && !updatePinDto.getPin().equals(updatePinDto.getConfirmPin())) {
			inErrors.reject( getMessage( ITS_PROPKEY_CONFIRM, new Object[] { (ITS_PROPKEY_PREARGUMENT + ITS_PROPKEY_PIN) } ) );
		}
	}

	private String getMessage(String inMsgCode, Object[] inArgMsgCodes) {
		for ( int i=0; i < inArgMsgCodes.length; i++ ) {
			inArgMsgCodes[i] = messageSource.getMessage( inArgMsgCodes[i].toString().toLowerCase(), null, LocaleContextHolder.getLocale());
		}
		return messageSource.getMessage(inMsgCode, inArgMsgCodes, ITS_DEFAULTMSG_INVALID, LocaleContextHolder.getLocale());
	}
}
