<%@ include file="../../common/taglibs.jsp" %>


<style type="text/css">
<!--
  #addProductBtn.btn-small { margin-bottom: 5px; }
  #productTable tr td:last-child, #detailsTable tr td:last-child, #receivedTable tr td:last-child  { border-left: 0px; }
-->
</style>


<div class="modal-dialog" id="egcOrderContainer">
  <div class="modal-content">
    <div class="modal-header">
      <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
      <h4>cc<spring:message code="assign_order_egc"/></h4>
    </div>

    <div class="modal-body">
      <form:form id="assignForm" name="assignForm" modelAttribute="assignForm" data-print-url="${pageContext.request.contextPath}/giftcard/egc/order/assign/print/" action="${pageContext.request.contextPath}/giftcard/egc/order/assign" method="POST" enctype="application/x-www-form-urlencoded" >
        <div id="contentError" class="hide alert alert-error">
          <button type="button" class="close" onclick="$(this).parent().hide();">&times;</button>
          <div><form:errors path="*"/></div>
        </div>

        <div class="contentMain">
          <input type="hidden" name="id" value="${orderForm.id}" />

		      <div class="contentMain">
			      <div class="well">
			        <div class="row-fluid">
			          <div class="span4">
			            <label for=""><spring:message code="order_po" /></label>
			            <input type="text" value="${orderForm.poNumber} / ${orderForm.poDate}" disabled="disabled" />
			          </div>

			          <div class="span4">
			            <label for=""><spring:message code="order_mo" /></label>
			            <input type="text" value="${orderForm.moNumber} / ${orderForm.moDate}" disabled="disabled" />
			          </div>
			        </div>

			        <div style="margin-top: 10px; margin-bottom: 10px;">
			          <table id="detailsTable" class="table table-condensed table-compressed table-striped">
			            <thead>
			              <tr>
			                <th><spring:message code="profile_product_desc" /></th>
			                <th><spring:message code="order_item_starting_series" /></th>
			                <th><spring:message code="order_item_ending_series" /></th>
			                <th><spring:message code="order_ordered" /></th>
			                <th><spring:message code="order_served" /></th>
			              </tr>
			            </thead>
			            <tbody>
			              <c:forEach var="item" items="${assignForm.series}" varStatus="status" >
			              <tr>
			                <td>${item.profile}</td>
			                <td>${item.startingSeries}</td>
			                <td>${item.endingSeries}</td>
			                <td>${item.ordered}</td>
			                <td>${item.served}</td>
			              </tr>
			              </c:forEach>
			            </tbody>
			          </table>
			        </div>

			        <c:if test="${not empty assignForm.receivedSeries}">
			        <div style="margin-top: 10px; margin-bottom: 10px;">
			          <table id="receivedTable" class="table table-condensed table-compressed table-striped">
			            <thead>
			              <tr>
			                <th><spring:message code="profile_product_desc" /></th>
			                <th><spring:message code="order_item_starting_series" /></th>
			                <th><spring:message code="order_item_ending_series" /></th>
			                <th><spring:message code="order_received" /></th>
			                <th><spring:message code="order_delivery_receipt" /></th>
			              </tr>
			            </thead>
			            <tbody>
			              <c:forEach var="item" items="${assignForm.receivedSeries}" varStatus="status" >
			              <tr>
			                <td>${item.profile}</td>
			                <td>${item.startingSeries}</td>
			                <td>${item.endingSeries}</td>
			                <td>${item.received}</td>
			                <td>${item.deliveryReceipt} (${item.receivedDate})</td>
			              </tr>
			              </c:forEach>
			            </tbody>
			          </table>
			        </div>
			        </c:if>
			      </div>

		        <sec:authentication var="userLocationCode" property="principal.inventoryLocation" />
		        <div class="row-fluid">
		          <div class="span4">
		            <spring:message code="assign_order_egc_assignto" var="receivedAt" />
		            <label for=""><b class="required">*</b> ${receivedAt}</label>
		            <select name="receivedAt">
			            <c:if test="${not empty userLocationCode }">
			            <c:if test="${userLocationCode == 'INVT001' }">
			            <c:forEach items="${stores}" var="item" >
			              <option value="${item.key}">${item.value}</option>
			            </c:forEach>
			            </c:if>
			            <c:if test="${userLocationCode != 'INVT001' }">
			              <option value="${userLocationCode}">${stores[userLocationCode]}</option>
			            </c:if>
			            </c:if>
		            </select>
		          </div>

		          <div class="span4">
		            <spring:message code="assign_order_egc_assigndate" var="receiveDate" />
		            <label for=""><b class="required">*</b> ${receiveDate}</label>
		            <form:input path="receivedDate" data-mo-date="${orderForm.moDate}" placeholder="${receiveDate}" />
		          </div>
		        </div>

		        <div class="pull-left" style="margin-right: 5px;">        
			        <button type="button" id="addProductBtn" class="btn btn-small">+</button><br/>
			        <button type="button" id="removeProductBtn" class="btn btn-small">&ndash;</button>
		        </div>

		        <div class="pull-left" style="width:90%;">
		          <table id="productTable" class="table table-condensed table-compressed table-striped">
		            <thead>
		              <tr>
		                <th></th>
		                <th><spring:message code="order_item_quantity" /></th>
		                <th><spring:message code="order_item_starting_series" /></th>
		                <th><spring:message code="order_item_ending_series" /></th>
		              </tr>
		            </thead>
		            <tbody>
		              <c:forEach var="item" items="${assignForm.items}" varStatus="status" >
		              <tr data-index="${status.index}">
		                <td><input type="checkbox" class="productRemoveFlag" /></td>
		                <td><input type="text" class="input-mini modalQuantity intInput" value="${item.quantity}" name="items[${status.index}].quantity" /></td>
		                <td><input type="text" class="input-medium modalStartingSeries intInput" value="${item.startingSeries}" name="items[${status.index}].startingSeries" maxlength="16" /></td>
		                <td><input type="text" class="input-medium modalEndingSeries intInput" value="${item.endingSeries}" name="items[${status.index}].endingSeries" maxlength="16" /></td>
		              </tr>
		              </c:forEach>
		            </tbody>
		          </table>
		        </div>
          </div>
        </div>
      </form:form>
    </div>

    <div class="modal-footer">
      <button type="button" id="" data-loading-text="<spring:message code="loading_text" />" class="btn btn-primary receiveOrderSave"><spring:message code="label_save" /></button>
      <button type="button" id="" class="btn btn-default" data-dismiss="modal"><spring:message code="label_cancel"/></button>
    </div>
  </div>
</div>


<script>
$(document).ready( function() {

    $orderDialog = $( "#orderDialog" ).css( { "width": "900px" } );
    var $container = $( "#egcOrderContainer" );
    var CONTAINER_ERROR = $container.find( "#contentError > div" );
    var CONTENT_ERROR = $container.find( "#contentError" );
    var $form = $container.find( "#assignForm" );
    var $receiveOrderSave = $container.find( ".receiveOrderSave" );
    var $productTable = $container.find( "#productTable" );
    var $productTbody = $( "tbody", $productTable );
    var $removeProductBtn = $( "#removeProductBtn", $container );
    var $addProductBtn = $( "#addProductBtn", $container );

    initBtnsFields();
    function initBtnsFields() {
        $("input[name='receivedDate']").datepicker({ format: 'dd-mm-yyyy', autoclose: true })
		        .datepicker("setEndDate", new Date())
		        .datepicker("setStartDate", new Date($("input[name='receivedDate']").data("moDate")))
		        .datepicker( "setDate", new Date() );

        $removeProductBtn.click( function() {
            $( "input.productRemoveFlag:checked" ).each( function() {
                var $tr = $(this).closest("tr");
                var length = $productTbody.find("tr").length;
                if( $tr.is( ":first-child" ) && length == 1 ) {
                    $tr.find( "input.productRemoveFlag" ).prop( "checked", false );
                    $tr.find( "input" ).removeAttr( "value" ).prop( "disabled", true );
                    $tr.find( "select" ).removeAttr( "value" ).prop( "disabled", true );
                    $tr.invisible();
                } 
                else {
                    $tr.remove();
                }
            });
        });

        $addProductBtn.click( function() {
            var $tr = $productTbody.find("tr:first-child");
            if($tr.css('visibility') == 'hidden' || $tr.css('display') == 'none') {
                $tr.find("input").removeAttr('value').prop('disabled', false);
                $tr.find("select").removeAttr('value').prop('disabled', false);
                $tr.visible();
            } 
            else {
                var $trLast = $productTbody.find("tr:last-child");
                var  $row = $tr.clone();
                $row.find("input").removeAttr('value').prop('disabled', false);
                $row.find("select").removeAttr('value').prop('disabled', false);

                var index = +$trLast.data("index") + 1;
                var lastIndex = $tr.data("index");
                $row.data("index", index);

                var rg = new RegExp("\\["+lastIndex+"\\]", 'g');
                var html = $row.html();
                html = html.replace(rg, '['+index+']');
                $row.html(html);
                $row.find('select').change(function() {
                    $(this).siblings(".unit-cost").val($(this).find(":selected").data('cost'));
                    console.log($(this).siblings(".unit-cost").val());
                });
                $productTbody.append($row);
                initFields( $row );
            }
        });

        $receiveOrderSave.click( function(e) {
            $receiveOrderSave.prop( "disabled", true );
            $.post($form.attr("action"), $form.serialize(), function(data) {
	              $receiveOrderSave.prop( "disabled", false );
	              if (data.success) {
			              var url = $form.data("printUrl") + "?" + $form.serialize();
			              window.open(url, '_blank', 'toolbar=0,location=0,menubar=0');
			              location.reload();
	              } 
	              else {
		                errorInfo = "";
		                for (i = 0; i < data.result.length; i++) {
		                  errorInfo += "<br>" + (i + 1) + ". "
		                      + ( data.result[i].code != undefined ? 
		                          data.result[i].code : data.result[i]);
		                }
                    CONTAINER_ERROR.html( "Please correct following errors: " + errorInfo );
                    CONTENT_ERROR.show( 'slow' );
	              }
            }); 
        });
    }

    initFields( $container );
    function initFields( $con ) {
        $('.quantity', $con).numberInput({"type" : "int"});
        $('.intInput', $con).numberInput({"type" : "int"});
        $('.floatInput', $con).numberInput({"type" : "float"});
        $('.modalQuantity, .modalStartingSeries', $con).on( "input", populateEndingSeries );
    }

    function populateEndingSeries() {
        var startSeries = $(this).closest("tr").find(".modalStartingSeries").val();
        var quantity = $(this).closest("tr").find(".modalQuantity").val();
        if(startSeries != null && quantity != null && startSeries != "" && quantity != "") {
	          var temp = +startSeries + +quantity - 1;
	          var endingSeries = temp.toString();
	          var padZero = 12 - endingSeries.length;
	          if(padZero > 0) {
	            for(var i = 0; i < padZero; i++)
	              endingSeries = "0" + endingSeries;
	          }
	          $(this).closest("tr").find(".modalEndingSeries").val(endingSeries);
        }
    }

    function saveStocks() {
        $.post( $form.attr("action") + "/stocks", $form.serialize() );
    }

    jQuery.fn.visible = function() { return this.css('visibility', 'visible'); };
    jQuery.fn.invisible = function() { return this.css('visibility', 'hidden' ); };

});
</script>