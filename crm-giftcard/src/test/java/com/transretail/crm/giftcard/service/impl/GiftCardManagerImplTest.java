package com.transretail.crm.giftcard.service.impl;

import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Lists;
import com.googlecode.catchexception.CatchException;
import com.googlecode.catchexception.apis.CatchExceptionBdd;
import com.transretail.crm.core.entity.lookup.LookupDetail;
import com.transretail.crm.core.repo.PeopleSoftStoreRepo;
import com.transretail.crm.core.service.PsoftStoreService;
import com.transretail.crm.giftcard.GiftCardServiceResolvableException;
import com.transretail.crm.giftcard.dto.GiftCardInfo;
import com.transretail.crm.giftcard.dto.GiftCardTransactionInfo;
import com.transretail.crm.giftcard.entity.*;
import com.transretail.crm.giftcard.entity.support.GiftCardInventoryStatus;
import com.transretail.crm.giftcard.entity.support.GiftCardSaleTransaction;
import com.transretail.crm.giftcard.entity.support.GiftCardSalesType;
import com.transretail.crm.giftcard.repo.*;
import com.transretail.crm.giftcard.service.GiftCardAccountingService;
import com.transretail.crm.giftcard.service.GiftCardManager;
import java.math.BigDecimal;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import org.fest.assertions.api.Assertions;
import org.fest.assertions.core.Condition;
import org.joda.time.*;
import org.junit.Before;
import org.junit.Test;
import org.mockito.*;

import static org.fest.assertions.api.Assertions.assertThat;
import static org.mockito.Mockito.*;

/**
 *
 * @author Monte Cillo Co (mco@exist.com)
 */
public class GiftCardManagerImplTest {

    private static final LocalDateTime CURRENT_DATE_TIME = new LocalDateTime(2014, 04, 24, 03, 56);
    @Mock
    private GiftCardTransactionRepo transactionRepo;
    @Mock
    private GiftCardTransactionItemRepo transactionItemRepo;
    @Mock
    private GiftCardInventoryRepo inventoryRepo;
    @InjectMocks
    @Spy
    private final GiftCardManager mgr = new GiftCardManagerImpl();
    @Mock
    private ProductProfile productProfile;
    @Mock
    private GiftCardAccountingService accountingService;
    @Mock
    private PeopleSoftStoreRepo peopleSoftStoreRepo;
    @Mock
    private PsoftStoreService peopleSoftStoreService;
    @Mock
    private ProductProfileRepo profileRepo;
 /*   @Mock
    private GiftCardProductBuRepo productBuRepo;*/

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        long fixedMillis = DateTimeUtils.getInstantMillis(CURRENT_DATE_TIME.toDateTime());
        DateTimeUtils.setCurrentMillisFixed(fixedMillis);
    }

    @Test
    public void freeVoucherActivationTest_shouldFailWhenIsNotForPromo() {
        GiftCardInventory giftcard = new GiftCardInventory() {
            {
                setBarcode(cardNo);
                setFaceValue(new BigDecimal(100000.00));
                setActivationDate(new LocalDate(2014, 01, 15));
                setStatus(GiftCardInventoryStatus.ACTIVATED);
                setBalance(0.0);
                setPreviousBalance(100000.00);
                setLocation(merchantId);
                setProfile(productProfile);
                setExpiryDate(new LocalDate(2014, 01, 24));
                setIsForPromo(Boolean.FALSE);
            }
        };

        when(productProfile.isAllowPartialRedeem()).thenReturn(Boolean.FALSE);
        when(inventoryRepo.findByBarcode(cardNo)).thenReturn(giftcard);

        GiftCardTransactionInfo transactionInfo = new GiftCardTransactionInfo()
                .cashierId(cashierId)
                .merchantId("OTHER_STORE")
                .terminalId(terminalId)
                .transactionNo(transactionNo)
                .transactionTime(LocalDateTime.now());

        CatchExceptionBdd.when(mgr).activateFreeVoucher(transactionInfo, ImmutableSet.of(cardNo));
        CatchExceptionBdd.then(CatchException.caughtException())
                .isInstanceOf(GiftCardServiceResolvableException.class)
                .hasMessageContaining("Invalid card. Following card is/are not free or does not exist : " + cardNo);

        verify(inventoryRepo).findByBarcode(cardNo);
    }

    @Test
    public void freeVoucherActivationTest_shouldPassWhenIsForPromo() {
        GiftCardInventory giftcard = new GiftCardInventory() {
            {
                setBarcode(cardNo);
                setFaceValue(new BigDecimal(100000.00));
                setStatus(GiftCardInventoryStatus.IN_STOCK);
                setBalance(0.0);
                setPreviousBalance(100000.00);
                setLocation(merchantId);
                setProfile(productProfile);
                setIsForPromo(Boolean.TRUE);
            }
        };

        when(productProfile.isAllowPartialRedeem()).thenReturn(Boolean.FALSE);
        when(inventoryRepo.findByBarcode(cardNo)).thenReturn(giftcard);
        when(inventoryRepo.save(any(GiftCardInventory.class))).thenReturn(giftcard);
        when(peopleSoftStoreService.isStoreAllowedForGcTransaction(merchantId))
                .thenReturn(Boolean.TRUE);

        GiftCardTransactionInfo transactionInfo = new GiftCardTransactionInfo()
                .cashierId(cashierId)
                .merchantId(merchantId)
                .terminalId(terminalId)
                .transactionNo(transactionNo)
                .transactionTime(LocalDateTime.now());

        Set<GiftCardInfo> results = mgr.activateFreeVoucher(transactionInfo, ImmutableSet.of(cardNo));

        Assertions.assertThat(results).isNotEmpty();

        verify(inventoryRepo, times(2)).findByBarcode(cardNo);
    }

    @Test
    public void activateionShouldRevalidateLocation() {
        GiftCardInventory giftcard = new GiftCardInventory() {
            {
                setBarcode(cardNo);
                setFaceValue(new BigDecimal(100000.00));
                setActivationDate(new LocalDate(2014, 01, 15));
                setStatus(GiftCardInventoryStatus.ACTIVATED);
                setBalance(0.0);
                setPreviousBalance(100000.00);
                setLocation(merchantId);
                setProfile(productProfile);
                setExpiryDate(new LocalDate(2014, 01, 24));
            }
        };

        when(productProfile.isAllowPartialRedeem()).thenReturn(Boolean.FALSE);
        when(inventoryRepo.findByBarcode(cardNo)).thenReturn(giftcard);

        GiftCardTransactionInfo transactionInfo = new GiftCardTransactionInfo()
                .cashierId(cashierId)
                .merchantId("OTHER_STORE")
                .terminalId(terminalId)
                .transactionNo(transactionNo)
                .transactionTime(LocalDateTime.now());

        CatchExceptionBdd.when(mgr).activate(transactionInfo, ImmutableSet.of(cardNo), GiftCardSalesType.B2C, false);
        CatchExceptionBdd.then(CatchException.caughtException())
                .isInstanceOf(GiftCardServiceResolvableException.class)
                .hasMessageContaining("present in merchant");

        verify(inventoryRepo).findByBarcode(cardNo);
    }

    @Test
    public void preRedeem_mustThrowExceptionWhenRedemptionAmountIsGreaterThanGCBalance() {
        GiftCardInventory giftcard = new GiftCardInventory() {
            {
                setBarcode(cardNo);
                setFaceValue(new BigDecimal(100000.00));
                setActivationDate(new LocalDate(2014, 01, 15));
                setStatus(GiftCardInventoryStatus.ACTIVATED);
                setBalance(500000.0);
                setPreviousBalance(100000.00);
                setLocation(merchantId);
                setProfile(productProfile);
                setExpiryDate(new LocalDate(2015, 01, 24));
            }
        };

        when(peopleSoftStoreService.isStoreAllowedForGcTransaction(merchantId))
                .thenReturn(Boolean.TRUE);
        when(productProfile.isAllowPartialRedeem()).thenReturn(Boolean.TRUE);
        when(inventoryRepo.findByBarcode(cardNo)).thenReturn(giftcard);

        CatchExceptionBdd.when(mgr).preRedeem(new GiftCardTransactionInfo().merchantId(merchantId), cardNo, 600000.00);
        CatchExceptionBdd.then(CatchException.caughtException())
                .isInstanceOf(GiftCardServiceResolvableException.class)
                .hasMessage("Insufficient balance. GC 14052800100000036058 current balance is 500,000.");

        verify(inventoryRepo).findByBarcode(cardNo);
        verify(peopleSoftStoreService).isStoreAllowedForGcTransaction(merchantId);
    }

    @Test
    public void preRedeem_mustThrowExceptionWhenMerchantIsNotAllowedForGCTransaction() {
        GiftCardInventory giftcard = new GiftCardInventory() {
            {
                setBarcode(cardNo);
                setFaceValue(new BigDecimal(100000.00));
                setActivationDate(new LocalDate(2014, 01, 15));
                setStatus(GiftCardInventoryStatus.ACTIVATED);
                setBalance(500000.0);
                setPreviousBalance(100000.00);
                setLocation(merchantId);
                setProfile(productProfile);
                setExpiryDate(new LocalDate(2014, 01, 24));
            }
        };

        when(peopleSoftStoreService.isStoreAllowedForGcTransaction(merchantId))
                .thenReturn(Boolean.FALSE);
        when(productProfile.isAllowPartialRedeem()).thenReturn(Boolean.FALSE);
        when(inventoryRepo.findByBarcode(cardNo)).thenReturn(giftcard);

        CatchExceptionBdd.when(mgr).preRedeem(new GiftCardTransactionInfo().merchantId(merchantId), cardNo, 100000.00);
        CatchExceptionBdd.then(CatchException.caughtException())
                .isInstanceOf(GiftCardServiceResolvableException.class)
                .hasMessage("Current merchant " + merchantId + " does not allowed for Gift Card Transaction.");

        verify(inventoryRepo).findByBarcode(cardNo);
        verify(peopleSoftStoreService).isStoreAllowedForGcTransaction(merchantId);
    }

    @Test
    public void preRedeem_mustThrowExceptionWhenCurrentDateIsAfterToTheExpiryDate() {
        GiftCardInventory giftcard = new GiftCardInventory() {
            {
                setBarcode(cardNo);
                setFaceValue(new BigDecimal(100000.00));
                setActivationDate(new LocalDate(2014, 01, 15));
                setStatus(GiftCardInventoryStatus.ACTIVATED);
                setBalance(500000.00);
                setPreviousBalance(100000.00);
                setLocation(merchantId);
                setProfile(productProfile);
                setExpiryDate(new LocalDate(2014, 01, 24));
            }
        };

        when(productProfile.isAllowPartialRedeem()).thenReturn(Boolean.FALSE);
        when(inventoryRepo.findByBarcode(cardNo)).thenReturn(giftcard);
        when(peopleSoftStoreService.isStoreAllowedForGcTransaction(merchantId))
                .thenReturn(Boolean.TRUE);

        CatchExceptionBdd.when(mgr).preRedeem(new GiftCardTransactionInfo().merchantId(merchantId), cardNo, 100000.00);
        CatchExceptionBdd.then(CatchException.caughtException())
                .isInstanceOf(GiftCardServiceResolvableException.class)
                .hasMessage("GC " + cardNo + " is already expired.");

        verify(inventoryRepo).findByBarcode(cardNo);

    }

    @Test
    public void preRedeem_mustThrowExceptionWhenCurrentDateIsEqualToTheExpiryDate() {
        GiftCardInventory giftcard = new GiftCardInventory() {
            {
                setBarcode(cardNo);
                setFaceValue(new BigDecimal(100000.00));
                setActivationDate(new LocalDate(2014, 01, 15));
                setStatus(GiftCardInventoryStatus.ACTIVATED);
                setBalance(500000.0);
                setPreviousBalance(100000.00);
                setLocation(merchantId);
                setProfile(productProfile);
                setExpiryDate(LocalDate.now());
            }
        };

        when(productProfile.isAllowPartialRedeem()).thenReturn(Boolean.FALSE);
        when(inventoryRepo.findByBarcode(cardNo)).thenReturn(giftcard);
        when(peopleSoftStoreService.isStoreAllowedForGcTransaction(merchantId))
                .thenReturn(Boolean.TRUE);

        CatchExceptionBdd.when(mgr).preRedeem(new GiftCardTransactionInfo().merchantId(merchantId), cardNo, 100000.00);
        CatchExceptionBdd.then(CatchException.caughtException())
                .isInstanceOf(GiftCardServiceResolvableException.class)
                .hasMessage("GC " + cardNo + " is already expired.");

        verify(inventoryRepo).findByBarcode(cardNo);
    }

    @Test
    public void preRedeem_mustFailWhenRedeemingMultipleTimesInProfileWithDisallowMultipleRedemption() {
        GiftCardInventory giftcard = new GiftCardInventory() {
            {
                setBarcode(cardNo);
                setFaceValue(new BigDecimal(100000.00));
                setActivationDate(new LocalDate(2014, 01, 15));
                setStatus(GiftCardInventoryStatus.ACTIVATED);
                setBalance(90000.00); // balance is not equal to face value == not allowed for redemption
                setLocation(merchantId);
                setProfile(productProfile);
                setExpiryDate(LocalDate.now().plusYears(1));
            }
        };

        when(productProfile.isAllowPartialRedeem()).thenReturn(Boolean.FALSE);
        when(inventoryRepo.findByBarcode(cardNo)).thenReturn(giftcard);
        when(transactionRepo.countGiftCardGroupByTransaction(giftcard, GiftCardSaleTransaction.REDEMPTION))
                .thenReturn(0l);
        when(peopleSoftStoreService.isStoreAllowedForGcTransaction(merchantId))
                .thenReturn(Boolean.TRUE);

        CatchExceptionBdd.when(mgr).preRedeem(new GiftCardTransactionInfo().merchantId(merchantId), cardNo, 10000.00);
        CatchExceptionBdd.then(CatchException.caughtException())
                .isInstanceOf(GiftCardServiceResolvableException.class)
                .hasMessage("GC " + cardNo + " is not eligible for multiple redemption.");

        verify(productProfile).isAllowPartialRedeem();
        verify(inventoryRepo).findByBarcode(cardNo);
        verify(transactionRepo).countGiftCardGroupByTransaction(any(GiftCardInventory.class), any(GiftCardSaleTransaction.class));
    }

    @Test
    public void preRedeem_mustFailWhenRedeemingMultipleTimesInProfileWithDisallowMultipleRedemption2() {
        GiftCardInventory giftcard = new GiftCardInventory() {
            {
                setBarcode(cardNo);
                setFaceValue(new BigDecimal(100000.00));
                setActivationDate(new LocalDate(2014, 01, 15));
                setStatus(GiftCardInventoryStatus.ACTIVATED);
                setBalance(100000.00);
                setLocation(merchantId);
                setProfile(productProfile);
                setExpiryDate(LocalDate.now().plusYears(1));
            }
        };

        when(productProfile.isAllowPartialRedeem()).thenReturn(Boolean.FALSE);
        when(inventoryRepo.findByBarcode(cardNo)).thenReturn(giftcard);
        when(transactionRepo.countGiftCardGroupByTransaction(giftcard, GiftCardSaleTransaction.REDEMPTION))
                .thenReturn(1l); // must fail, because it has already a redemption record
        when(peopleSoftStoreService.isStoreAllowedForGcTransaction(merchantId))
                .thenReturn(Boolean.TRUE);
        CatchExceptionBdd.when(mgr).preRedeem(new GiftCardTransactionInfo().merchantId(merchantId), cardNo, 100000.00);
        CatchExceptionBdd.then(CatchException.caughtException())
                .isInstanceOf(GiftCardServiceResolvableException.class)
                .hasMessage("GC " + cardNo + " is not eligible for multiple redemption.");

        verify(productProfile).isAllowPartialRedeem();
        verify(inventoryRepo).findByBarcode(cardNo);
        verify(transactionRepo).countGiftCardGroupByTransaction(any(GiftCardInventory.class), any(GiftCardSaleTransaction.class));
    }

    @Test
    public void redeem_mustThrowExceptionWhenTransactionDateIsAfterToTheExpiryDate() {
        GiftCardInventory giftcard = new GiftCardInventory() {
            {
                setBarcode(cardNo);
                setFaceValue(new BigDecimal(100000.00));
                setActivationDate(new LocalDate(2014, 01, 15));
                setStatus(GiftCardInventoryStatus.ACTIVATED);
                setBalance(0.0);
                setPreviousBalance(100000.00);
                setLocation(merchantId);
                setProfile(productProfile);
                setExpiryDate(new LocalDate(2014, 01, 24));
            }
        };

        GiftCardTransactionInfo transactionInfo = new GiftCardTransactionInfo()
                .cashierId(cashierId)
                .merchantId(merchantId)
                .terminalId(terminalId)
                .transactionNo(transactionNo)
                .transactionTime(LocalDateTime.now());

        when(productProfile.isAllowPartialRedeem()).thenReturn(Boolean.FALSE);
        when(inventoryRepo.findByBarcode(cardNo)).thenReturn(giftcard);
        when(peopleSoftStoreService.isStoreAllowedForGcTransaction(merchantId))
                .thenReturn(Boolean.TRUE);
        CatchExceptionBdd.when(mgr).redeem(transactionInfo, cardNo, 1000.0);
        CatchExceptionBdd.then(CatchException.caughtException())
                .isInstanceOf(GiftCardServiceResolvableException.class)
                .hasMessageContaining("expired");

        verify(inventoryRepo).findByBarcode(cardNo);
    }

    @Test
    public void redeem_mustThrowExceptionWhenTransactionDateIsEqualToTheExpiryDate() {
        GiftCardInventory giftcard = new GiftCardInventory() {
            {
                setBarcode(cardNo);
                setFaceValue(new BigDecimal(100000.00));
                setActivationDate(new LocalDate(2014, 01, 15));
                setStatus(GiftCardInventoryStatus.ACTIVATED);
                setBalance(0.0);
                setPreviousBalance(100000.00);
                setLocation(merchantId);
                setProfile(productProfile);
                setExpiryDate(LocalDate.now());
            }
        };

        GiftCardTransactionInfo transactionInfo = new GiftCardTransactionInfo()
                .cashierId(cashierId)
                .merchantId(merchantId)
                .terminalId(terminalId)
                .transactionNo(transactionNo)
                .transactionTime(LocalDateTime.now());

        when(productProfile.isAllowPartialRedeem()).thenReturn(Boolean.FALSE);
        when(inventoryRepo.findByBarcode(cardNo)).thenReturn(giftcard);
        when(peopleSoftStoreService.isStoreAllowedForGcTransaction(merchantId))
                .thenReturn(Boolean.TRUE);
        CatchExceptionBdd.when(mgr).redeem(transactionInfo, cardNo, 1000.0);
        CatchExceptionBdd.then(CatchException.caughtException())
                .isInstanceOf(GiftCardServiceResolvableException.class)
                .hasMessageContaining("expired");

        verify(inventoryRepo).findByBarcode(cardNo);
    }

    @Test
    public void redeem_whenDisallowPartionRedemptionShouldRecordCorrectBalance() {
        GiftCardInventory giftcard = new GiftCardInventory() {
            {
                setBarcode(cardNo);
                setFaceValue(new BigDecimal(100000.00));
                setActivationDate(new LocalDate(2014, 01, 15));
                setStatus(GiftCardInventoryStatus.ACTIVATED);
                setBalance(100000.00);
                setPreviousBalance(0.00);
                setLocation(merchantId);
                setProfile(productProfile);
                setExpiryDate(LocalDate.now().plusYears(1));
            }
        };

        GiftCardTransactionInfo transactionInfo = new GiftCardTransactionInfo()
                .cashierId(cashierId)
                .merchantId(merchantId)
                .terminalId(terminalId)
                .transactionNo(transactionNo)
                .transactionTime(LocalDateTime.now());

        when(productProfile.isAllowPartialRedeem()).thenReturn(Boolean.FALSE);
        when(inventoryRepo.findByBarcode(cardNo)).thenReturn(giftcard);
        when(inventoryRepo.save(giftcard)).thenReturn(giftcard);
        when(peopleSoftStoreService.isStoreAllowedForGcTransaction(merchantId))
                .thenReturn(Boolean.TRUE);
        when(transactionItemRepo.save(any(GiftCardTransactionItem.class)))
                .thenReturn(null);
        mgr.redeem(transactionInfo, cardNo, 10000.0);

        verify(inventoryRepo).findByBarcode(cardNo);
        ArgumentCaptor<GiftCardInventory> acInventory = ArgumentCaptor.forClass(GiftCardInventory.class);
        verify(inventoryRepo).save(acInventory.capture());
        GiftCardInventoryAssert.assertThat(acInventory.getValue())
                .hasBalance(30000.00).hasPreviousBalance(100000.00);

        ArgumentCaptor<GiftCardTransaction> captor = ArgumentCaptor.forClass(GiftCardTransaction.class);
        verify(transactionRepo).save(captor.capture());
        assertThat(captor.getValue().getTransactionItems()).isNotEmpty().haveAtLeast(1, new Condition<GiftCardTransactionItem>() {

            @Override
            public boolean matches(GiftCardTransactionItem value) {
                return value.getCurrentAmount() == 100000.00 && value.getTransactionAmount() == -70000.00;
            }
        });
    }

    @Test
    public void voidRedemptionWhenProfileDisallowPartialRedemption() {
        GiftCardInventory giftcard = new GiftCardInventory() {
            {
                setBarcode(cardNo);
                setFaceValue(new BigDecimal(100000.00));
                setActivationDate(new LocalDate(2014, 01, 15));
                setStatus(GiftCardInventoryStatus.ACTIVATED);
                setBalance(10000.0);
                setPreviousBalance(100000.00);
                setLocation(merchantId);
                setProfile(productProfile);
            }
        };

        when(productProfile.isAllowPartialRedeem()).thenReturn(Boolean.FALSE);

        GiftCardTransaction refTransactionInfo = new GiftCardTransaction()
                .cashierId(cashierId)
                .merchantId(merchantId)
                .terminalId(terminalId)
                .transactionNo(refTransactionNo)
                .transactionDate(LocalDateTime.now())
                .transactionType(GiftCardSaleTransaction.REDEMPTION)
                .addItem(new GiftCardTransactionItem()
                        .currentAmount(100000.00)
                        .giftCard(giftcard)
                        .transactionAmount(-90000.00));

        when(transactionRepo.findByTransactionNoAndTransactionType(refTransactionNo, GiftCardSaleTransaction.REDEMPTION))
                .thenReturn(refTransactionInfo);
        when(inventoryRepo.save(giftcard)).thenReturn(giftcard);
        when(peopleSoftStoreService.isStoreAllowedForGcTransaction(merchantId))
                .thenReturn(Boolean.TRUE);
        GiftCardTransactionInfo transactionInfo = new GiftCardTransactionInfo()
                .cashierId(cashierId)
                .merchantId(merchantId)
                .terminalId(terminalId)
                .transactionNo(transactionNo)
                .refTransactionNo(refTransactionNo)
                .transactionTime(LocalDateTime.now());

        mgr.voidRedemption(transactionInfo);

        ArgumentCaptor<GiftCardInventory> gcAc = ArgumentCaptor.forClass(GiftCardInventory.class);
        verify(inventoryRepo).save(gcAc.capture());

        GiftCardInventory updatedGc = gcAc.getValue();
        GiftCardInventoryAssert.assertThat(updatedGc).hasBalance(100000.00)
                .hasPreviousBalance(10000.00);

        ArgumentCaptor<GiftCardTransaction> gtxCaptor = ArgumentCaptor.forClass(GiftCardTransaction.class);
        verify(transactionRepo).save(gtxCaptor.capture());

        final GiftCardTransaction captureGiftCardTransaction = gtxCaptor.getValue();
        assertThat(captureGiftCardTransaction.getRefTransaction()).isNotNull();
        assertThat(captureGiftCardTransaction.getTransactionItems()).isNotEmpty().haveAtLeast(1, new Condition<GiftCardTransactionItem>() {

            @Override
            public boolean matches(GiftCardTransactionItem value) {
                return value.getCurrentAmount() == 10000.00 && value.getTransactionAmount() == 90000.00;
            }
        });
    }

    @Test
    public void voidRedemptionWhenProfileAllowPartialRedemption() {
        GiftCardInventory giftcard = new GiftCardInventory() {
            {
                setBarcode(cardNo);
                setFaceValue(new BigDecimal(100000.00));
                setActivationDate(new LocalDate(2014, 01, 15));
                setStatus(GiftCardInventoryStatus.ACTIVATED);
                setBalance(70000.00);
                setPreviousBalance(100000.00);
                setLocation(merchantId);
                setProfile(productProfile);
            }
        };

        when(productProfile.isAllowPartialRedeem()).thenReturn(Boolean.TRUE);

        GiftCardTransaction refTransactionInfo = new GiftCardTransaction()
                .cashierId(cashierId)
                .merchantId(merchantId)
                .terminalId(terminalId)
                .transactionNo(refTransactionNo)
                .transactionType(GiftCardSaleTransaction.REDEMPTION)
                .transactionDate(LocalDateTime.now())
                .addItem(new GiftCardTransactionItem()
                        .currentAmount(100000.00)
                        .giftCard(giftcard)
                        .transactionAmount(-30000.00));

        when(transactionRepo.findByTransactionNoAndTransactionType(refTransactionNo, GiftCardSaleTransaction.REDEMPTION))
                .thenReturn(refTransactionInfo);
        when(inventoryRepo.save(giftcard)).thenReturn(giftcard);
        when(peopleSoftStoreService.isStoreAllowedForGcTransaction(merchantId))
                .thenReturn(Boolean.TRUE);

        GiftCardTransactionInfo transactionInfo = new GiftCardTransactionInfo()
                .cashierId(cashierId)
                .merchantId(merchantId)
                .terminalId(terminalId)
                .transactionNo(transactionNo)
                .refTransactionNo(refTransactionNo)
                .transactionTime(LocalDateTime.now());

        Set<GiftCardInfo> cardInfos = mgr.voidRedemption(transactionInfo);

        ArgumentCaptor<GiftCardInventory> gcAc = ArgumentCaptor.forClass(GiftCardInventory.class);
        verify(inventoryRepo).save(gcAc.capture());

        GiftCardInventory updatedGc = gcAc.getValue();
        assertThat(updatedGc).is(new Condition<GiftCardInventory>() {

            @Override
            public boolean matches(GiftCardInventory value) {
                return value.getPreviousBalance() == 70000.00 && value.getBalance() == 100000.00;
            }
        });

        ArgumentCaptor<GiftCardTransaction> gtxCaptor = ArgumentCaptor.forClass(GiftCardTransaction.class);
        verify(transactionRepo).save(gtxCaptor.capture());

        final GiftCardTransaction captureGiftCardTransaction = gtxCaptor.getValue();
        assertThat(captureGiftCardTransaction.getRefTransaction()).isNotNull();
        assertThat(captureGiftCardTransaction.getTransactionItems()).isNotEmpty().haveAtLeast(1, new Condition<GiftCardTransactionItem>() {

            @Override
            public boolean matches(GiftCardTransactionItem value) {
                return value.getCurrentAmount() == 70000.00 && value.getTransactionAmount() == 30000.00;
            }
        });
    }

    @Test
    public void voidRedemptionWithNotMatchReferenceTransaction() {
        GiftCardInventory giftcard = new GiftCardInventory() {
            {
                setBarcode(cardNo);
                setFaceValue(new BigDecimal(100000.00));
                setActivationDate(new LocalDate(2014, 01, 15));
                setStatus(GiftCardInventoryStatus.ACTIVATED);
                setBalance(70000.00);
                setPreviousBalance(100000.00);
                setLocation(merchantId);
                setProfile(productProfile);
            }
        };

        when(productProfile.isAllowPartialRedeem()).thenReturn(Boolean.TRUE);

        GiftCardTransaction refTransactionInfo = new GiftCardTransaction()
                .cashierId(cashierId)
                .merchantId(merchantId)
                .terminalId(terminalId)
                .transactionNo(refTransactionNo)
                .transactionDate(LocalDateTime.now())
                .transactionType(GiftCardSaleTransaction.ACTIVATION)
                .addItem(new GiftCardTransactionItem()
                        .currentAmount(100000.00)
                        .giftCard(giftcard)
                        .transactionAmount(-30000.00));

        when(transactionRepo.findByTransactionNoAndTransactionType(refTransactionNo, GiftCardSaleTransaction.ACTIVATION))
                .thenReturn(refTransactionInfo);
        when(peopleSoftStoreService.isStoreAllowedForGcTransaction(merchantId))
                .thenReturn(Boolean.TRUE);

        GiftCardTransactionInfo transactionInfo = new GiftCardTransactionInfo()
                .cashierId(cashierId)
                .merchantId(merchantId)
                .terminalId(terminalId)
                .transactionNo(transactionNo)
                .refTransactionNo(refTransactionNo)
                .transactionTime(LocalDateTime.now());

        CatchExceptionBdd.when(mgr).voidRedemption(transactionInfo);
        CatchExceptionBdd.then(CatchException.caughtException())
                .isInstanceOf(GiftCardServiceResolvableException.class)
                .hasMessageContaining("Reference Transaction's transaction type is not match!");

        verify(transactionRepo).findByTransactionNoAndTransactionType(refTransactionNo, GiftCardSaleTransaction.ACTIVATION);
        verifyZeroInteractions(inventoryRepo);
    }

    @Test
    public void testReloadInvalidReloadAmount() {
        PrepaidReloadInfo prepaidReloadInfo = new PrepaidReloadInfo();
        prepaidReloadInfo.setMonthEffective(12);
        prepaidReloadInfo.setReloadAmount(500000l);

        List<PrepaidReloadInfo> prepaidReloadInfos = Lists.newArrayList(prepaidReloadInfo);
        final ProductProfile reloadProfile = new ProductProfile();
        reloadProfile.setPrepaidReloadInfos(prepaidReloadInfos);
        reloadProfile.setAllowPartialRedeem(Boolean.TRUE);
        reloadProfile.setAllowReload(Boolean.TRUE);

        GiftCardInventory giftcard = new GiftCardInventory() {
            {
                setBarcode(cardNo);
                setFaceValue(new BigDecimal(100000.00));
                setActivationDate(new LocalDate(2014, 01, 15));
                setExpiryDate(new LocalDate(2015, 01, 15));
                setStatus(GiftCardInventoryStatus.ACTIVATED);
                setBalance(70000.00);
                setPreviousBalance(100000.00);
                setLocation(merchantId);
                setProfile(reloadProfile);
            }
        };

        when(inventoryRepo.findByBarcode(cardNo)).thenReturn(giftcard);
        when(peopleSoftStoreService.isStoreAllowedForGcTransaction(merchantId))
                .thenReturn(Boolean.TRUE);

        CatchExceptionBdd.when(mgr).preReload(cardNo, merchantId, 100000.00);
        CatchExceptionBdd.then(CatchException.caughtException())
                .hasMessage("Expected Result should be declined, only reload amount(s) [500,000] is allowed.");

        verify(inventoryRepo).findByBarcode(cardNo);
        verify(peopleSoftStoreService).isStoreAllowedForGcTransaction(merchantId);
    }

    @Test
    public void testReloadExceededMaxAmount() {
        PrepaidReloadInfo prepaidReloadInfo = new PrepaidReloadInfo();
        prepaidReloadInfo.setMonthEffective(12);
        prepaidReloadInfo.setReloadAmount(500000l);

        List<PrepaidReloadInfo> prepaidReloadInfos = Lists.newArrayList(prepaidReloadInfo);
        final ProductProfile reloadProfile = new ProductProfile();
        reloadProfile.setMaxAmount(1000000l);
        reloadProfile.setPrepaidReloadInfos(prepaidReloadInfos);
        reloadProfile.setAllowPartialRedeem(Boolean.TRUE);
        reloadProfile.setAllowReload(Boolean.TRUE);

        GiftCardInventory giftcard = new GiftCardInventory() {
            {
                setBarcode(cardNo);
                setFaceValue(new BigDecimal(100000.00));
                setActivationDate(new LocalDate(2014, 01, 15));
                setExpiryDate(new LocalDate(2015, 01, 15));
                setStatus(GiftCardInventoryStatus.ACTIVATED);
                setBalance(700000.00);
                setPreviousBalance(100000.00);
                setLocation(merchantId);
                setProfile(reloadProfile);
            }
        };

        when(inventoryRepo.findByBarcode(cardNo)).thenReturn(giftcard);
        when(peopleSoftStoreService.isStoreAllowedForGcTransaction(merchantId))
                .thenReturn(Boolean.TRUE);

        CatchExceptionBdd.when(mgr).preReload(cardNo, merchantId, 500000.00);
        CatchExceptionBdd.then(CatchException.caughtException())
                .hasMessage("Reload amount will exceed Gift Card max amount (1,000,000).");

        verify(inventoryRepo).findByBarcode(cardNo);
        verify(peopleSoftStoreService).isStoreAllowedForGcTransaction(merchantId);
    }

    @Test(expected = GiftCardServiceResolvableException.class)
    public void testProductAndBuCodeIsSame() {

        PrepaidReloadInfo prepaidReloadInfo = new PrepaidReloadInfo();
        prepaidReloadInfo.setMonthEffective(12);
        prepaidReloadInfo.setReloadAmount(500000l);

        List<PrepaidReloadInfo> prepaidReloadInfos = Lists.newArrayList(prepaidReloadInfo);
        final ProductProfile reloadProfile = new ProductProfile();
        reloadProfile.setMaxAmount(1000000l);
        reloadProfile.setPrepaidReloadInfos(prepaidReloadInfos);
        reloadProfile.setAllowPartialRedeem(Boolean.TRUE);
        reloadProfile.setAllowReload(Boolean.TRUE);
        reloadProfile.setProductDesc("VC Test");

        GiftCardInventory giftCardInventory = new GiftCardInventory() {
            {
                setBarcode(cardNo);
                setFaceValue(new BigDecimal(100000.00));
                setActivationDate(new LocalDate(2014, 01, 15));
                setExpiryDate(new LocalDate(2015, 01, 15));
                setStatus(GiftCardInventoryStatus.ACTIVATED);
                setBalance(700000.00);
                setPreviousBalance(100000.00);
                setLocation(merchantId);
                setProfile(reloadProfile);
            }
        };

        LookupDetail two = new LookupDetail("TRI", "TRI");
        LookupDetail three = new LookupDetail("INVT001", "HEAD OFFICE");

        Set<LookupDetail> lookupDetails = new HashSet<LookupDetail>();

        lookupDetails.add(two);
        lookupDetails.add(three);

      /*  GiftCardProductBu productBu = new GiftCardProductBu();
        productBu.setProductProfile(giftCardInventory.getProfile());
        productBu.setBusinessUnit(lookupDetails);

        GiftCardTransactionInfo transactionInfo = new GiftCardTransactionInfo()
                .cashierId(cashierId)
                .merchantId(merchantId)
                .terminalId(terminalId)
                .transactionNo(transactionNo)
                .transactionTime(LocalDateTime.now());

        when(productProfile.isAllowPartialRedeem()).thenReturn(Boolean.FALSE);
        when(inventoryRepo.findByBarcode(cardNo)).thenReturn(giftCardInventory);
        when(peopleSoftStoreService.isStoreAllowedForGcTransaction(merchantId))
                .thenReturn(Boolean.TRUE);
        when(peopleSoftStoreService.isStoreAllowedForGcTransaction(merchantId))
                .thenReturn(Boolean.TRUE);
        when(productBuRepo.findByProductProfile(giftCardInventory.getProfile())).thenReturn(productBu);*/

     //   when(mgr.redeem(transactionInfo, cardNo, 10000.0)).thenThrow(GiftCardServiceResolvableException.class);
        /*CatchExceptionBdd.then(CatchException.caughtException())
                .isInstanceOf(GiftCardServiceResolvableException.class)
                .hasMessageContaining("Current Merchant {0} does not allow GC {1} transaction");*/

    }
    private static final String merchantId = "10036";
    private static final String terminalId = "10036TR00008";
    private static final String cashierId = "10036US00008";
    private static final String cardNo = "14052800100000036058";
    private static final String transactionNo = "100360290000104";
    private static final String refTransactionNo = "100360290000103";
}
