var fieldsList = null;
var operandsList = null;
var fieldValueMap = null;
var memberGroupForm = null;
var modelAttribute = "memberGroup";
var fieldsForm = null;
var groupNameInput = null;
var mainForm = null;
var memberListTable = null;
var cardTypeGroupCode = null;

var CONTAINER_ERROR = "#contentError > div";
var CONTENT_ERROR = "#contentError";

$(document).ready(function(){
	// initialize 
	mainForm = $("form[id='" + modelAttribute + "']");
	groupNameInput = $("#memberGroupName");
	memberGroupForm = $('#itemDialog');
	memberListTable = $("#memberListTable");
	fieldsForm = $("#fieldsform");
	fieldsForm.empty();
	
	memberGroupForm.modal({
		show	: false
	});
	memberGroupForm.on( "hide", function(e) { 
		if ( e.target === this ) { 
			$( CONTAINER_ERROR ).empty();
			$( CONTENT_ERROR ).hide();
			resetForm( mainForm );
		} 
	});
	
	memberListTable.modal({
		show	: false
	});
	
	$('#addField').unbind("click").click(function() {
		addNewField();
	});
	
	$("#button_cancel").unbind("click").click(function(){
		memberGroupForm.modal("hide");
		groupNameInput.val('');
		fieldsForm.empty();
		resetRfs();
	});
	
	$("#button_ok").unbind("click").click(function(){
		memberListTable.modal("hide");
	});
	
	cardTypeGroupCode = $("#cardTypeGroupCode");
//	cardTypeGroupCode.change(addCardTypeField);
//	cardTypeGroupCode.focusout(addCardTypeField);


	function resetForm( ele, isEmpty ) {
		  $( ele ).find(':input').each( function() {
			    switch(this.type) {
              case 'password':
              case 'select-one':
              case 'text':
              case 'textarea':
              case 'file':
              case 'hidden': $(this).val(''); break;
              case 'select-multiple': isEmpty? $(this).empty() : $(this).val(""); break;
              case 'checkbox':
              case 'radio': this.checked = false;
	        }
	    });
	}
});

function addCardTypeField() {
	if($(this).val()) {
		var initField = new Object;
		initField.name = new Object;
		initField.name.code = "MEM008";
		initField.fieldValues = [];
		addNewField(initField);
	}
}

function resetRfs() {
	$("input[type='text'][name^='rfs.'], select[name^='rfs.']").val("");
	$("input[type='radio'][name^='rfs.'], input[type='checkbox'][name^='rfs.']").prop("checked", false);
	$("#rfsCheckbox").each(toggleRfs);
	$(".rfsCheckbox").each(resetFields);
	$(".recencyRadio:checked").each(toggleRecency);
}

var MemberFields = {
	getExisting 	: function() {
		return $( "select.memberFields" ).map( function(){ return this.value; } ).get().join(",").split(",");
	},
	changeExisting 	: function( elSelect ) {
		if ( $(elSelect).val() ) {
			$( "select.memberFields" ).each( function() {
				if ( $(elSelect).val() != $(this).val() ) {
					$(this).find( "option[value='" + $(elSelect).val() + "']" ).remove();
				}
			});
		}
	},
	updateOptions	: function( elSelect ) {
		if ( $(elSelect).val() ) {
			$( "select.memberFields" ).each( function() {
				if ( $(elSelect).val() != $(this).val() ) {
					$(this).find( "option[value='" + $(elSelect).val() + "']" ).remove();
					if ( $(elSelect).data( "prev-value" ) ) {
						$(this).find( "option" ).eq( $(elSelect).data( "prev-index" ) ).before( $("<option></option>" )
								.val( $(elSelect).data( "prev-value" ) ).text( $(elSelect).data( "prev-label" ) ) );
					}
				}
			});
		}
	}
};

function addNewField(initField) {
	var existingFields = MemberFields.getExisting();
	var intId = $("#fieldsform div").length + 1;
	var fieldWrapper = $("<div class='fieldwrapper row-fluid well' />");
	var leftBlock = $("<div class='leftBlock' />");
	var rightBlock = $("<div class='span6 rightBlock' />");
	var fieldSelect = $("<select class='input memberFields' data-prev-index='' data-prev-value='' data-prev-label='' name='memberFields' id='field" + intId + "'/>");
	fieldSelect.append("<option value=''>Select field...</option>");
	$.each(fieldsList, function(key, val){
		if ( existingFields.indexOf( key ) == -1) {
			var option = $("<option />").attr("value", key).text(val);
			fieldSelect.append(option);
		}
	});
	
	var valSelect = $("<div class='control-group values' />");
	fieldSelect
		.focus( function() { 
			$(this).data( "prev-index", $(this)[0].selectedIndex );
			$(this).data( "prev-value", $(this).val() );
			$(this).data( "prev-label", $(this).find( "option:selected" ).html() );
		})
		.change(function() {
			MemberFields.updateOptions(fieldSelect);
		var selected = fieldSelect.val();
		valSelect.empty();
		
		var fieldName = fieldsList[selected];
		if(fieldName == 'AGE' || fieldName == 'CHILDREN' ) {
			var inputFieldVal = "";
			if ( initField && initField.fieldValues[0].value != undefined ) {
				inputFieldVal = initField.fieldValues[0].value;
			}
			valSelect.append("<input class='" + fieldName + "' type='text'  value='" + inputFieldVal + "' />");
			$( oprSelect ).show();
		}
		else {
            // Fix for Bug #83452
			$( oprSelect ).hide();
            var url = ctx + '/reference/list/byheadercode/' + selected;
            $.get( url, function ( list ) {
                for ( var i = 0; i < list.length; i++ ) {
                    valSelect.append($("<label class='control-label' />"));
                    var checked = "";
                    if(initField !== 'undefined' && initField != null) {
                    	var initValues = initField.fieldValues;
                        $.each(initValues, function(key, val) {
                        	if ( val.value == list[i].code ) 
                        		checked = "checked";
            			});
                    }
                    valSelect.append($("<div class='checkbox'><input type='checkbox' name='value' value='" + list[i].code + "' " + checked + ">"
                            + list[i].desc + "</input></div>"));
                }
            });
		}
	});
	
	var oprSelect = $("<select class='hide input pull-left' name='operand' id='operand" + intId + "'/>");
	$.each(operandsList, function(key, val){
		oprSelect.append($("<option />").attr("value", key).text(val));
	});
	
	var removeButton = $("<input type='button' value='Remove' class='btn btn-small pull-right' />");
	removeButton.unbind("click").click(function() {
		$(this).parent().parent().remove();
	});
	
	leftBlock.append(fieldSelect);
	leftBlock.append(oprSelect);
	leftBlock.append(removeButton);
	rightBlock.append(valSelect);
	fieldWrapper.append(leftBlock);
	fieldWrapper.append(rightBlock);
	fieldsForm.append(fieldWrapper);
	
	if(initField !== 'undefined' && initField != null) {
		fieldSelect.val(initField.name.code);
		fieldSelect.trigger("change");
		var initValues = initField.fieldValues;
		if(initValues !== 'undefined' && initValues != null && !jQuery.isEmptyObject(initValues)) {
			oprSelect.val(initValues[0].operand.code);
			/*
			var selector = "";
			$.each(initValues, function(key, val) {
				if(selector != "") selector += ", ";
				selector += "input:checkbox[value='" + val.value + "']";
			});
			valSelect.find(selector).each(function(key, val){
				$(val).prop("checked", true);
			});*/
		}
		MemberFields.changeExisting( fieldSelect );
	}
}

function setGlobalAttributes(fields, map, operands) {
	fieldsList = fields;
	operandsList = operands;
	fieldValueMap = map;
}

function createGroup(query) {
	memberGroupForm.modal("show");
	saveGroup('/new');
}

function editGroup(groupId, query) {
	populateGroup(groupId);
	memberGroupForm.modal("show");
	saveGroup('/update/' + groupId);
}

function viewMembers(groupId, groupName) {
	memberListTable.modal('show');
	reloadItemsTable(groupId, groupName);
} 

function populateGroup(groupId) {
	$.ajax({
		type	: 'POST',
		url		: mainForm.attr("action") + "/populate/" + groupId,
		dataType: 'json',
		success	: function(response) {
			if(!response.success) {
				mainGroupForm.dialog("close");
				return;
			}
			
			var memberGroup = response.result;
			$("#cardTypeGroupCode").val(memberGroup.cardTypeGroupCode);
			groupNameInput.val(memberGroup.name);
			
			if (memberGroup.prepaidUser === true)
			  $("#prepaidCheckbox").attr("checked","checked");
			else
			  $("#prepaidCheckbox").removeAttr("checked");

			$.each(memberGroup.memberFields, function(key, field) {
				addNewField(field);
			});
			
			function getValue(e) {
				var name = $(e).attr("name");
				var val = memberGroup;
				$.each(name.split("."), function (index, value) {
					if(val[value] == null) {
						val = "";
						return false;
					} else 
						val = val[value];
				});
				if(val == null) {
					val = "";
				}
				return val;
			}
			
			if(memberGroup.rfs != null && memberGroup.rfs.enabled) {
				$("input[type='text'][name^='rfs.'], select[name^='rfs.']").each(function() {
					$(this).val(getValue($(this)));
				});
				$("input[type='radio'][name^='rfs.']").each(function() {
					if($(this).val() == getValue($(this))) {
						$(this).prop("checked", "checked");
					}
				});
				$("input[type='checkbox'][name^='rfs.']").each(function() {
					if(getValue($(this))) {
						$(this).prop("checked", "checked");
					}
				});
				$("#rfsCheckbox").each(toggleRfs);
				$(".rfsCheckbox").each(resetFields);
				$(".recencyRadio:checked").each(toggleRecency);
				
				if($('.frequenceField').filter(function() {
			        	return this.value == 0;
			    	}).length == 2) {
					$("#zeroFrequency").click();
				}
			}
			if(memberGroup.points != null && memberGroup.points.enabled) {
				$("input[type='text'][name^='points.'], select[name^='points.']").each(function() {
					$(this).val(getValue($(this)));
				});
				$("input[type='radio'][name^='points.']").each(function() {
					if($(this).val() == getValue($(this))) {
						$(this).prop("checked", "checked");
					}
				});
				$("input[type='checkbox'][name^='points.']").each(function() {
					if(getValue($(this))) {
						$(this).prop("checked", "checked");
					}
				});
				$("#pointsCheckbox").each(togglePoints);
			}
		}
	});
}

function saveGroup(controllerPath) {
	$("#button_save").unbind("click").click(function(){
		$( this ).prop( 'disabled', true );
		$.ajax({
			type	: 'POST',
			contentType: 'application/json',
			url		: mainForm.attr("action") + controllerPath,
			dataType: 'json',
			data	: parseData(),
			success	: function(inResponse) {
				if (inResponse.success) {
					groupNameInput.val('');
					fieldsForm.empty();
					mainForm.submit();
				} 
				else {
					$( "#button_save" ).prop( 'disabled', false );
					errorInfo = "";
					for (i = 0; i < inResponse.result.length; i++) {
						errorInfo += "<br>" + (i + 1) + ". "
								+ ( inResponse.result[i].code != undefined ? 
										inResponse.result[i].code : inResponse.result[i]);
					}
					$( CONTAINER_ERROR ).html("Please correct following errors: " + errorInfo);
					$( CONTENT_ERROR ).show('slow');
				}
				
			}
		});
	});
}

function parseData() {
	var data = new Object();
	
	data["name"] = groupNameInput.val();
	data["cardTypeGroupCode"] = cardTypeGroupCode.val();
	if ($("#prepaidCheckbox").attr("checked") === "checked")
	  data["prepaidUser"] = true;
	else
	  data["prepaidUser"] = false;
	fields = [];
	$.each(fieldsForm.children(".fieldwrapper"), function(key, fieldWrapper){
		var field = new Object();
		var values = [];
		var operand = null;
		$(fieldWrapper).find("select.input").each(function(key, obj){
			var name = $(obj).attr("name");
			
			if(name == "memberFields") {
				field["name"] = $(obj).val();
				var text = $(obj).find(':selected').text();
				field['isLookup'] = (text != 'AGE' && text != 'CHILDREN');
			}
			else {
				operand = $(obj).val();
			} 
		});
		
		$(fieldWrapper).find("input:checked, input[type='text']").each(function(key, obj) {
			var value = new Object();
			value["value"] = $(obj).val();
			value["operand"] = operand;
			values.push(value);
		});
		
		if(!jQuery.isEmptyObject(values)) field["fieldValues"] = values;
		fields.push(field);
	});
	if(!jQuery.isEmptyObject(fields)) data["memberFields"] = fields;
	
	//RFS
	rfs = new Object();
	$($("input[name^='rfs.']:checked, input[type='text'][name^='rfs.'], select[name^='rfs.']")).each(function() {
		var name = $(this).attr("name").split('.').pop();
		rfs[name] = $(this).val();
	});
	if(!jQuery.isEmptyObject(rfs)) data["rfs"] = rfs;
	
	//RFS
	points = new Object();
	$($("input[name^='points.']:checked, input[type='text'][name^='points.'], select[name^='points.']")).each(function() {
		var name = $(this).attr("name").split('.').pop();
		points[name] = $(this).val();
	});
	if(!jQuery.isEmptyObject(points)) data["points"] = points;
	
	return JSON.stringify(data);
} 
