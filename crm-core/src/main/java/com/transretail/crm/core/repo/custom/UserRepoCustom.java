package com.transretail.crm.core.repo.custom;

import java.util.List;
import java.util.Set;

import com.transretail.crm.core.entity.UserModel;
import com.transretail.crm.core.entity.UserRoleModel;


public interface UserRepoCustom {

	List<UserModel> searchUsers( UserModel inUser );

    List<UserModel> findByRoleCode( String roleCode );
}
