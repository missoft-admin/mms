<%@ include file="../../common/taglibs.jsp" %>
        <div class="block-stack-01">
          <div class="control-group-01">
          <fieldset>
            <legend class="block-stack-title"><spring:message code="stamp_promo_reward" /></legend>
              <div id="block_promotionRewards" class="">
                <div id="list_promotionRewards">
                <c:if test="${not empty promotion.promotionRewards}">
                <div id="rewardList" class="el-block-01 row-fluid pull-center">

                  <table class="table table-striped">
                    <thead>
                      <tr>
                          <th><spring:message code="reward_prop_minamount"/></th>
              	          <th><spring:message code="reward_prop_maxamount"/></th>
                          <th><spring:message code="reward_prop_baseamount"/></th>
                          <th><spring:message code="stamp_promo_earn_stamps" /></th>
                          <th></th>
                      </tr>
                    </thead>
                
                    <tbody id="promotionRewardsEntries">
                      <c:forEach varStatus="varRewardIdx" begin="0" var="reward" items="${promotion.promotionRewards}" >
                        <c:set var="rewardIdx" value="${varRewardIdx.index}" scope="request" />
                        <c:if test="${null != rewardIdx}">
                          <tr class="promotionRewardsEntry">
                            <form:hidden path="promotionRewards[${rewardIdx}].id" />        
                              <td><form:input path="promotionRewards[${rewardIdx}].minAmount" cssClass="input-medium wholeInput" disabled="true" /></td>
                              <td><form:input path="promotionRewards[${rewardIdx}].maxAmount" cssClass="input-medium wholeInput" disabled="true" /></td>
                              <td><form:input path="promotionRewards[${rewardIdx}].baseAmount" cssClass="input-medium wholeInput" disabled="true" /></td>
                              <td class=""><form:input path="promotionRewards[${rewardIdx}].baseFactor" cssClass="input-medium floatInput" disabled="true" /></td>
                              <td><div class="checkbox"><form:checkbox cssClass="fixedCheckbox" path="promotionRewards[${rewardIdx}].fixed" disabled="true" /> <spring:message code="reward_prop_fixed" /></div></td>
                          </tr>
                        </c:if>
                      </c:forEach>
                    </tbody>
                  </table>
                </div>
                </c:if>
                </div>
	            </div>
          </fieldset>
          </div>
        </div>
        
        
        
        
        <br/>
        <div class="block-stack-01">
          <div class="control-group-01">
          <fieldset>
            <legend class="block-stack-title"><spring:message code="stamp_promo_redemption" /></legend>
              <div id="block_promotionRewards" class="">
                <div id="list_promotionRewards">
                <c:if test="${not empty promotion.promotionRedemptions}">
                <div id="rewardList" class="el-block-01 row-fluid pull-center">

                  <table class="table table-striped">
                    <thead>
                      <tr>
                          <th><spring:message code="promo_redemption_prop_sku"/></th>
                          <th><spring:message code="promo_redemption_prop_prdname"/></th>
                          <th><spring:message code="stamp_promo_quantity"/></th>
                          <th><spring:message code="stamp_promo_redeem_from"/></th>
                          <th><spring:message code="stamp_promo_redeem_to"/></th>
                      </tr>
                    </thead>
                
                    <tbody id="promotionRedemptionsEntries">
                      <c:forEach varStatus="varRewardIdx" begin="0" var="reward" items="${promotion.promotionRedemptions}" >
                        <c:set var="rewardIdx" value="${varRewardIdx.index}" scope="request" />
                        <c:if test="${null != rewardIdx}">
                          <tr class="promotionRedemptionsEntry">
                            <form:hidden path="promotionRedemptions[${rewardIdx}].id" />        
                              <td><form:input path="promotionRedemptions[${rewardIdx}].sku" cssClass="input-medium" disabled="true" /></td>
                              <td><form:input path="promotionRedemptions[${rewardIdx}].productName" cssClass="input-medium" disabled="true" /></td>
                              <td><form:input path="promotionRedemptions[${rewardIdx}].qty" cssClass="input-medium" disabled="true" /></td>
                              <td><form:input path="promotionRedemptions[${rewardIdx}].redeemFrom" cssClass="input-medium" disabled="true" /></td>
                              <td><form:input path="promotionRedemptions[${rewardIdx}].redeemTo" cssClass="input-medium" disabled="true" /></td>
                          </tr>
                        </c:if>
                      </c:forEach>
                    </tbody>
                  </table>
                </div>
                </c:if>
                </div>
	            </div>
          </fieldset>
          </div>
        </div>
        
        
        <script src='<c:url value="/js/viewspecific/promotion/reward/rewardList.js"/>' type='text/javascript'><![CDATA[&nbsp; ]]></script>