<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<!--[if lt IE 7]>
<html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>
<html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>
<html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!-->
<html class="no-js"> <!--<![endif]-->
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
  <spring:message code="application_name" var="app_name" htmlEscape="false"/>
  <title><spring:message code="welcome_h3" arguments="${app_name}"/></title>
  <meta name="description" content="Member Portal">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <!-- for non html5 compliant browsers -->
  <script src="<spring:url value="/js/modernizr-2.6.2.min.js" />"></script>

  <link href="<c:url value="/images/favicon.ico" />" rel="SHORTCUT ICON"/>
  <%--<link href='http://fonts.googleapis.com/css?family=Lato' rel='stylesheet' type='text/css'>--%>
  <link href="<c:url value="/css/bootstrap/bootstrap.css" />" rel="stylesheet">
  <link href="<c:url value="/css/bootstrap/bootstrap-responsive.css" />" rel="stylesheet">
  <link href="<c:url value="/css/custom.css" />" rel="stylesheet">
  <link href="<c:url value="/css/anonymous.css" />" rel="stylesheet">

  <script src="<c:url value="/js/jquery/jquery-1.10.2.min.js" />"></script>
</head>
<body class="login">
<div class="container">
  <div class="logo-login text-center">
    <a href="<c:url value="/" />"><img width="350" src="<c:url value="/images/logo-login.png" />"></a>
  </div>
  <div class="body-container">
    <tiles:insertAttribute name="body"/>
  </div>
</div>
<!-- /container -->
<style>
  .body-container {
    max-width: 700px;
    background-color: #F1F1F1;
    overflow-x: auto;
  }
  h2 {
      font-weight: 200;
      color: #A1C042;
      font-size: 38.5px;
  }
</style>
</body>
</html>