<%@include file="../common/messages.jsp" %>

<style type="text/css">
<!--
  .well .input {
    background-color: #fff;
    border: 1px solid #E8E8E8;
    box-shadow: none;    
    padding: 6px;
    height: 18px;
    margin-left: 5px;
  }
  .well .btn-primary {
    margin-left: 5px;
  }
  .well select, .well .input {
    margin: 5px;
  }
  
-->
</style>

<div id="content_store_member">

  <div class="page-header page-header2"><h1><spring:message code="header_store_member_scheme" /></h1></div>
  
  <jsp:include page="storeMemberSearchForm.jsp"/>
  
  <div class="">
      <div class="pull-right"><a href="#" class="btn btn-primary" data-url="<c:url value="/storemember/create"/>" id="link_createStoreMember"><spring:message code="label_store_member_create" /></a></div>
  </div>
  <div class="clearfix"></div>
  <div id="form_store_member"></div>
  <div id="list_store_members" class="data-content-02"><jsp:include page="storeMemberList.jsp"></jsp:include></div>

  <script src='<c:url value="/js/viewspecific/storememberscheme/storeMember.js"/>' type='text/javascript'><![CDATA[&nbsp; ]]></script>
  <link href="<c:url value="/styles/temp.css" />" rel="stylesheet" />

</div>