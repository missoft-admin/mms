package com.transretail.crm.core.entity.enums;

public enum MemberTypeConstants {

    INDIVIDUAL("MTYP001"), COMPANY("MTYP003"), EMPLOYEE("MTYP002");
    

    private String value;

    private MemberTypeConstants(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }

    @Override
    public String toString() {
        return this.getValue();
    }

}
