create table CRM_B2B_EXH (
        id number(19,0) not null,
        CREATED_BY varchar2(255 char),
        CREATED_DATETIME timestamp,
        LAST_UPDATED_BY varchar2(255 char),
        LAST_UPDATED_DATETIME timestamp,
        DISCOUNT number(19,2),
        primary key (id)
    );

    create table CRM_B2B_EXH_GC (
        id number(19,0) not null,
        CREATED_BY varchar2(255 char),
        CREATED_DATETIME timestamp,
        LAST_UPDATED_BY varchar2(255 char),
        LAST_UPDATED_DATETIME timestamp,
        ENDING_SERIES varchar2(255 char),
        STARTING_SERIES varchar2(255 char),
        EXH number(19,0) not null,
        PRODUCT number(19,0),
        primary key (id)
    );

    create table CRM_B2B_EXH_PAYMENTS (
        id number(19,0) not null,
        CREATED_BY varchar2(255 char),
        CREATED_DATETIME timestamp,
        LAST_UPDATED_BY varchar2(255 char),
        LAST_UPDATED_DATETIME timestamp,
        DETAILS varchar2(255 char),
        AMT number(19,2),
        EXH number(19,0) not null,
        TYPE varchar2(15 char),
        primary key (id)
    );

alter table CRM_B2B_EXH_GC 
        add constraint FK_98e2753xve7ccccsd58o2rgr1 
        foreign key (EXH) 
        references CRM_B2B_EXH;

    alter table CRM_B2B_EXH_GC 
        add constraint FK_rkx945i5xw26vftq6d34x0175 
        foreign key (PRODUCT) 
        references CRM_GC_PROD_PROFILE;

    alter table CRM_B2B_EXH_PAYMENTS 
        add constraint FK_grpca5aog7g6xyc95ow37ri2v 
        foreign key (EXH) 
        references CRM_B2B_EXH;

    alter table CRM_B2B_EXH_PAYMENTS 
        add constraint FK_mwgfe595wjywmpaq5by3cccqo 
        foreign key (TYPE) 
        references CRM_REF_LOOKUP_DTL;
