package com.transretail.crm.core.service;

/**
 *
 * @author Monte Cillo Co (mco@exist.com)
 */
public interface InvalidTerminalService {

    /**
     * Check if terminal id is in the invalid list of terminals.
     *
     * @param storeCode
     * @param terminalId
     * @return true if terminal id is in the list otherwise return false.
     */
    boolean inList(String storeCode, String terminalId);
}
