/*
 * Copyright (c) 2012.
 * All rights reserved.
 */

package com.transretail.crm.common.security.service;

import java.util.List;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.UserDetailsService;

import com.transretail.crm.common.security.model.SecurityUserDetails;

public interface SecurityUserService<T extends SecurityUserDetails> extends UserDetailsService {

    T getCurrentUser();
    
    Authentication getCurrentAuth();

    List<String> getUserAuthorities();

}
