package com.transretail.crm.core.repo.custom;


import java.util.Date;
import java.util.List;

import org.joda.time.DateTime;
import org.joda.time.LocalDate;

import com.transretail.crm.common.service.dto.request.PageSortDto;
import com.transretail.crm.common.service.dto.response.ResultList;
import com.transretail.crm.core.dto.PointsDto;
import com.transretail.crm.core.dto.PointsExpireDto;
import com.transretail.crm.core.dto.PointsExpireReportDto;
import com.transretail.crm.core.dto.PointsSearchDto;
import com.transretail.crm.core.entity.PointsTxnModel;
import com.transretail.crm.core.entity.enums.PointTxnType;
import com.transretail.crm.core.entity.enums.TxnStatus;

public interface PointsTxnRepoCustom {


    List<PointsTxnModel> findPointsTxnByDateRange(String accountId, Date fromDate,
                                            Date toDate);

	List<PointsTxnModel> findByIds(List<String> inIds);
    
    List<PointsTxnModel> findAdjustPointsByTxnTypeAndHighestPoint( TxnStatus inType, Long inHighestPoint );

	List<PointsTxnModel> findByTxnTypeAndLowestPoint(TxnStatus inType, Long inLowestPoint);

	List<PointsTxnModel> findAdjustPointsTxnForProcessing();

	Double getTotalPoints(String inAccountId);

	Double getTotalPointsSupplement(String accountId);

	Date getLatestRewardGenerationToDate();

	Double getTotalPointsRewardForProcessing(Date inFromDate, Date inToDate);

	List<PointsTxnModel> findPointsRewardDeleted(Date inFromDate, Date inToDate);

	List<PointsTxnModel> findPointsRewardForProcessing(Date inFromDate, Date inToDate);

	List<PointsTxnModel> findPointsRewardForApproval(Date inFromDate, Date inToDate);

	List<PointsTxnModel> findPointsTxnByStatusAndDates(PointTxnType inTxnType,
			TxnStatus status, Date inFromDate, Date inToDate);

	List<PointsTxnModel> findRewardsByStatusAndDates(TxnStatus status,
			Date inFromDate, Date inToDate);

	ResultList<PointsDto> listPointsTxnForProcessing(PointTxnType inTxnType,
			Date inFromDate, Date inToDate, PointsSearchDto sortDto);

	List<PointsTxnModel> findTransactionsToday(String accountId);
	
	List<PointsTxnModel> findTransactionsByDate(String accountId, LocalDate date);
	
	List<PointsTxnModel> findExpiringTransactions(String accountId, LocalDate expiryDate);
	
	PointsExpireReportDto findPastExpiringTransactions(LocalDate month);
	
	PointsTxnModel findLastExpireTransaction(String accountId);
	
	List<PointsTxnModel> sumExpiringPoints(String accountId, LocalDate expiryDate);
	
	List<PointsTxnModel> findDeductablePoints(String accountId, DateTime startDate, LocalDate expiryDate);
	
	List<PointsExpireDto> expirePoints(Date startDate, LocalDate expiryDate);
	
	Object[] sumDeductablePoints(String accountId, DateTime startDate, LocalDate expiryDate);
	
	List<PointsTxnModel> hasExistingExpirePoints(String accountId, LocalDate expiryDate);

	List<PointsTxnModel> sumPointsToExpireForLoyalty(String accountId,
			LocalDate expiryDate);
	
	Date findDateOfLastExpire();
}
