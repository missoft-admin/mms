package com.transretail.crm.giftcard.dto;

import java.util.List;

import org.apache.commons.lang3.tuple.Pair;
import org.joda.time.DateTime;
import org.joda.time.LocalDate;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.util.Assert;

import com.google.common.collect.Lists;
import com.transretail.crm.giftcard.entity.StockRequest;
import com.transretail.crm.giftcard.entity.support.StockRequestStatus;

/**
 *
 */
public class StockRequestDto {
	private Long id;
	private String createUser;
	
	private String requestNo;
	@DateTimeFormat(pattern = "dd-MM-yyyy")
	private LocalDate requestDate = new LocalDate();
	private String sourceLocation;
	private String sourceLocationDescription;
	private String allocateTo;
	private String allocateToDescription;
	private Integer quantity;
	private String productCode;
	private String productDescription;
	private StockRequestStatus status;
	private StockRequestStatus statusForHO;
	private String transferRefNo;
	private Integer quantityTransferred;
	//QuantityReceived is per transaction
	private Integer quantityReceived;
	
	//For Transfer In
	@DateTimeFormat(pattern = "dd-MM-yyyy")
    private LocalDate receiveDate = new LocalDate();
	private String receiveBy;
	private List<StockRequestReceiveDto> receiveDtos;
	
	private Integer quantityReserved;
	private String reservedSeries;
	private List<Pair<String, String>> reservedSeriesList = Lists.newArrayList();
	private DateTime lastUpdated;
    private Boolean isForPromo;
	
    public StockRequestDto() {

    }

    public StockRequestDto(StockRequest request) {
    	if(request == null) {
    		return;
    	}
    	
    	this.id = request.getId();
    	this.createUser = request.getCreateUser();
    	this.requestDate = request.getRequestDate();
		this.lastUpdated = request.getLastUpdated();
    	this.requestNo = request.getRequestNo();
    	this.allocateTo = request.getAllocateTo();
    	this.quantity = request.getQuantity();
    	this.productCode = request.getProductCode();
    	this.status = request.getStatus();
    	if (request.getStatus() == StockRequestStatus.TRANSFERRED_IN) {
    	    this.statusForHO = StockRequestStatus.TRANSFERRED_OUT;
    	} else {
    	    this.statusForHO = request.getStatus();
    	}
    	this.sourceLocation = request.getSourceLocation();
    	this.transferRefNo = request.getTransferRefNo();
    	this.quantityTransferred = request.getQuantityTransferred();
    	this.quantityReserved = request.getQuantityReserved();
    	this.reservedSeries = request.getReservedSeries();
    	if(reservedSeries != null) {
    		String[] pairs = this.reservedSeries.split(",");
    		for(String pair : pairs) {
    			String[] series = pair.split(":");
    			reservedSeriesList.add(Pair.of(series[0], series[1]));
    		}
    	}
    	this.isForPromo = request.getIsForPromo();
    }

    /**
     * Convert this dto to StockRequest
     *
     * @return StockRequest model
     */
    public StockRequest toModel() {
        return copyValues(null);
    }

    /**
     * Update the StockRequest parameter with the values of the fields in this DTO
     *
     * @param request StockRequest to update
     * @return updated StockRequest
     */
    public StockRequest update(StockRequest request) {
        Assert.notNull(request);
        Assert.notNull(request.getId());
        return copyValues(request);
    }

    private StockRequest copyValues(StockRequest request) {
        if (request == null) {
            request = new StockRequest();
        }
        
        request.setRequestDate(requestDate);
        request.setRequestNo(requestNo);
        request.setSourceLocation(sourceLocation);
        request.setAllocateTo(allocateTo);
        request.setQuantity(quantity);
        request.setProductCode(productCode);
        request.setStatus(status);
        request.setTransferRefNo(transferRefNo);
        request.setQuantityTransferred(quantityTransferred);
        request.setQuantityReserved(quantityReserved);
        
        if(!reservedSeriesList.isEmpty()) {
        	StringBuilder builder = new StringBuilder();
        	for(Pair<String, String> pair : reservedSeriesList) {
        		builder.append(pair.toString("%1$s:%2$s")).append(',');
        	}
        	builder.deleteCharAt(builder.length() - 1);
        	this.reservedSeries = builder.toString();
        }
        
        request.setReservedSeries(reservedSeries);
        request.setIsForPromo( isForPromo );
        
        return request;
    }

    public Long getId() {
        return id;
    }

	public String getRequestNo() {
		return requestNo;
	}

	public void setRequestNo(String requestNo) {
		this.requestNo = requestNo;
	}

	public LocalDate getRequestDate() {
		return requestDate;
	}

	public void setRequestDate(LocalDate requestDate) {
		this.requestDate = requestDate;
	}
	
	public String getRequestDateString() {
		return requestDate.toString("dd-MM-yyyy");
	}

	public String getSourceLocation() {
		return sourceLocation;
	}

	public void setSourceLocation(String sourceLocation) {
		this.sourceLocation = sourceLocation;
	}

	public String getCreateUser() {
		return createUser;
	}

	public String getAllocateTo() {
		return allocateTo;
	}

	public void setAllocateTo(String allocateTo) {
		this.allocateTo = allocateTo;
	}

	public Integer getQuantity() {
		return quantity;
	}

	public void setQuantity(Integer quantity) {
		this.quantity = quantity;
	}

	public String getProductCode() {
		return productCode;
	}

	public void setProductCode(String productCode) {
		this.productCode = productCode;
	}

	public StockRequestStatus getStatus() {
		return status;
	}

	public void setStatus(StockRequestStatus status) {
		this.status = status;
	}

	public String getTransferRefNo() {
		return transferRefNo;
	}

	public void setTransferRefNo(String transferRefNo) {
		this.transferRefNo = transferRefNo;
	}

	public String getProductDescription() {
		return productDescription;
	}

	public void setProductDescription(String productDescription) {
		this.productDescription = productDescription;
	}

	public void setCreateUser(String createdBy) {
		this.createUser = createdBy;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getSourceLocationDescription() {
		return sourceLocationDescription;
	}

	public void setSourceLocationDescription(String sourceLocationDescription) {
		this.sourceLocationDescription = sourceLocationDescription;
	}

	public String getAllocateToDescription() {
		return allocateToDescription;
	}

	public void setAllocateToDescription(String allocateToDescription) {
		this.allocateToDescription = allocateToDescription;
	}

    public LocalDate getReceiveDate() {
        return receiveDate;
    }

    public void setReceiveDate(LocalDate receiveDate) {
        this.receiveDate = receiveDate;
    }

    public String getReceiveBy() {
        return receiveBy;
    }

    public void setReceiveBy(String receiveBy) {
        this.receiveBy = receiveBy;
    }

    public Integer getQuantityTransferred() {
        return quantityTransferred;
    }

    public void setQuantityTransferred(Integer quantityTransferred) {
        this.quantityTransferred = quantityTransferred;
    }

    public List<StockRequestReceiveDto> getReceiveDtos() {
        return receiveDtos;
    }

    public void setReceiveDtos(List<StockRequestReceiveDto> receiveDtos) {
        this.receiveDtos = receiveDtos;
    }

    public Integer getQuantityReceived() {
        return quantityReceived;
    }

    public void setQuantityReceived(Integer quantityReceived) {
        this.quantityReceived = quantityReceived;
    }

    public void addSeriesToList(Long startingSeries, Long endingSeries) {
    	this.reservedSeriesList.add(Pair.of(startingSeries.toString(), endingSeries.toString()));
    }

	public Integer getQuantityReserved() {
		return quantityReserved;
	}

	public void setQuantityReserved(Integer quantityReserved) {
		this.quantityReserved = quantityReserved;
	}

	public String getReservedSeries() {
		return reservedSeries;
	}

	public void setReservedSeries(String reservedSeries) {
		this.reservedSeries = reservedSeries;
	}

	public List<Pair<String, String>> getReservedSeriesList() {
		return reservedSeriesList;
	}

	public void setReservedSeriesList(List<Pair<String, String>> reservedSeriesList) {
		this.reservedSeriesList = reservedSeriesList;
	}
	
	public String getReservedSeriesListToString() {
		if(!reservedSeriesList.isEmpty()) {
        	StringBuilder builder = new StringBuilder();
        	for(Pair<String, String> pair : reservedSeriesList) {
        		builder.append(pair.toString("%1$s:%2$s")).append(',');
        	}
        	builder.deleteCharAt(builder.length() - 1);
        	this.reservedSeries = builder.toString();
        }
		return reservedSeries;
	}

    public StockRequestStatus getStatusForHO() {
        return statusForHO;
    }

    public void setStatusForHO(StockRequestStatus statusForHO) {
        this.statusForHO = statusForHO;
    }

	public DateTime getLastUpdated() {
		return lastUpdated;
	}

	public void setLastUpdated(DateTime lastUpdated) {
		this.lastUpdated = lastUpdated;
	}

	public Boolean getIsForPromo() {
		return isForPromo;
	}

	public void setIsForPromo(Boolean isForPromo) {
		this.isForPromo = isForPromo;
	}
}
