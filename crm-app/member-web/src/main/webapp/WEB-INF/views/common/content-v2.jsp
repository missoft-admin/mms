<%@ include file="../taglibs.jsp" %>

<div class="main-carousel-home">

 <c:if test="${hasPromoBanners}">
<div id="promoBanners" class="carousel slide" style="width: 960px; height: 350px" data-interval="5000">
    <ol class="carousel-indicators">
        <c:forEach items="${promoBanners}" varStatus="status">
        <c:choose>
            <c:when test="${status.index == 0}">
            <li data-target="#promoBanners" data-slide-to="${status.index}" class="active"></li>
            </c:when>
            <c:otherwise>
            <li data-target="#promoBanners" data-slide-to="${status.index}"></li>
            </c:otherwise>
        </c:choose>
        </c:forEach>
    </ol>
    <div class="carousel-inner">
        <c:forEach items="${promoBanners}" var="promoBanner" varStatus="status">
        <c:choose>
            <c:when test="${status.index == 0}">
                <c:choose>
                    <c:when test="${not empty promoBanner.link}">
                    <div class="item active">
                        <a href="${promoBanner.link}" target="_blank"><img src="<c:url value="/image/${promoBanner.id}" />" /></a>
                    </div>
                    </c:when>
                    <c:otherwise>
                    <div class="item active">
                        <span><img src="<c:url value="/image/${promoBanner.id}" />" /></span>
                    </div>
                    </c:otherwise>
                </c:choose>
            </c:when>
            <c:otherwise>
                <c:choose>
                    <c:when test="${not empty promoBanner.link}">
                    <div class="item">
                        <a href="${promoBanner.link}" target="_blank"><img src="<c:url value="/image/${promoBanner.id}" />" /></a>
                    </div>
                    </c:when>
                    <c:otherwise>
                    <div class="item">
                        <span><img src="<c:url value="/image/${promoBanner.id}" />" /></span>
                    </div>
                    </c:otherwise>
                </c:choose>
            </c:otherwise>
        </c:choose>
        </c:forEach>
    </div>
    <c:if test="${promoBannerCount > 1}">
        <a class="carousel-control left" href="#promoBanners" data-slide="prev">&lsaquo;</a>
        <a class="carousel-control right" href="#promoBanners" data-slide="next">&rsaquo;</a>
    </c:if>
</div>
</c:if>
</div>

<div class="product-slider row-fluid container">
<div class="span9">
    <div class="well mb20 clearfix"> 
        <form id="form" class="form-horizontal" autocomplete="off" method="POST" action="/member-web/member/password/change">
            <div class="control-group" style="margin-bottom:-20px">
                <label class="signup-label">Not yet a member? Sign up now!</label>
                <div class="controls">
                    <input id="" class="signup-input"type="text" value="" title="" name="" placeholder="Enter your email" style="width:50%">
                    <input id="" class="btn btn-primary btn-small" type="button" value="Join">
                </div>
            </div>    
        </form>  
    </div>
    
    <c:if test="${hasPromoProducts}">
    <div id="promoProducts" class="carousel slide" style="width: 590px; height: 280px" data-interval="5000">
        <ol class="carousel-indicators">
            <c:forEach var="i" begin="0" end="${promoProductCount / 3}">
            <c:choose>
                <c:when test="${i == 0}">
                <li data-target="#promoProducts" data-slide-to="${i}" class="active"></li>
                </c:when>
                <c:otherwise>
                <li data-target="#promoProducts" data-slide-to="${i}"></li>
                </c:otherwise>
            </c:choose>
            </c:forEach>
        </ol>
        <div class="carousel-inner">
            <c:forEach items="${promoProducts}" varStatus="status" begin="0" step="3">
            <c:choose>
                <c:when test="${status.index == 0}">
                    <div class="item active">
                        <c:forEach var="i" begin="${status.index}" end="${status.index + 2}">
                        <div class="span4">
                            <c:if test="${not empty promoProducts[i]}">
                                <img src="<c:url value="/image/${promoProducts[i].id}" />" />
                                <ul class="product-label">
                                    <li>${promoProducts[i].info}</li>
                                    <li>${promoProducts[i].name}</li>
                                    <li>${promoProducts[i].productQuantity}</li>
                                    <li class="product-price-orig">${promoProducts[i].oldPrice}</li>
                                    <li class="product-price-reduced">${promoProducts[i].newPrice}</li>
                                </ul>
                            </c:if>
                        </div>
                        </c:forEach>
                    </div>
                </c:when>
                <c:otherwise>
                    <div class="item">
                        <c:forEach var="i" begin="${status.index}" end="${status.index + 2}">
                        <div class="span4">
                            <c:if test="${not empty promoProducts[i]}">
                                <img src="<c:url value="/image/${promoProducts[i].id}" />" />
                                <ul class="product-label">
                                    <li>${promoProducts[i].info}</li>
                                    <li>${promoProducts[i].name}</li>
                                    <li>${promoProducts[i].productQuantity}</li>
                                    <li class="product-price-orig">${promoProducts[i].oldPrice}</li>
                                    <li class="product-price-reduced">${promoProducts[i].newPrice} </li>
                                </ul>
                            </c:if>
                        </div>
                        </c:forEach>
                    </div>
                </c:otherwise>
            </c:choose>
            </c:forEach>
        </div>
        <c:if test="${promoProductCount > 3}">
            <a class="carousel-control left" href="#promoProducts" data-slide="prev">&lsaquo;</a>
            <a class="carousel-control right" href="#promoProducts" data-slide="next">&rsaquo;</a>
        </c:if>
    </div>
    </c:if>
</div>

<div class="span3 m120">
    <c:if test="${hasPromoAd}">
        <div class="mb30"><img style="max-width:100%;" src="<c:url value="/image/${promoAd.id}" />"></div>
    </c:if>
    <div>
        <div class="box-s1">
        <div style="margin-top: -20px; padding-left: 10px;"><img width="" style="max-width:100%;" src="<c:url value="/images/ads-banner/bundaCarrefour.png" />"></div>
        <div class="box-s1-title-1">News From Bunda</div>
        <c:if test="${hasPromoNews}">
            <div class="box-s1-title-2">${promoNews.name}</div>
            <div class="box-s1-content">
                <p><img style="width: 248px; height: 140px" src="<c:url value="/image/${promoNews.id}" />" /></p>
                <p>${promoNews.info}</p>
            </div>
        </c:if>
    </div>
</div>
</div>

</div>

<script>
  var $internalLinks = $( '#internal-links-nav' );
  if ( $internalLinks.length > 0 ) {
    var navbarInner2Div = document.createElement( 'div' );
    navbarInner2Div.className = 'navbar-inner navbar-inner2';

    var $navbarInner2 = $( navbarInner2Div ).append( '<div class="container"><div class=""></div></div>' ).appendTo( $( '.navbar' ) );
    if ( !$internalLinks.hasClass( 'nav' ) ) {
      $internalLinks.addClass( 'nav' );
    }
    $( 'div div', $navbarInner2 ).append( $internalLinks );
    $internalLinks.show();
    $('body').css('padding-top', '7em');
  }
</script>



<script type="text/javascript">
// Generated by CoffeeScript 1.7.1
$(document).ready(function() {
$(".main-carousel-home .carousel").carousel({
  interval: 15000
});
$(".main-carousel-home .carousel").on("slid", function() {
  var to_slide;
  to_slide = $(".main-carousel-home .carousel-item.active").attr("data-slide-no");
  $(".main-carousel-home .myCarousel-target.active").removeClass("active");
  $(".main-carousel-home .carousel-indicators [data-slide-to=" + to_slide + "]").addClass("active");
});
$(".main-carousel-home .myCarousel-target").on("click", function() {
  $(this).preventDefault();
  $(".main-carousel-home .carousel").carousel(parseInt($(this).attr("data-slide-to")));
  $(".main-carousel-home .myCarousel-target.active").removeClass("active");
  $(this).addClass("active");
});
});
</script>




<script type="text/javascript">
	$(document).ready(function() {
    $('.product-slider #myCarousel').carousel({
	    interval: 10000
	})
});	</script> <!-- product slider -->

<link href="<c:url value="/css/index-v2.css" />" rel="stylesheet"/>