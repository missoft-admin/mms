package com.transretail.crm.common.service.dto.response;

import java.util.ArrayList;
import java.util.Collection;

import org.springframework.data.domain.Page;

public class ResultList<T> extends AbstractResultListDTO<T> {
    public ResultList() {
        super(new ArrayList<T>(), 0, false, false);
    }

    public ResultList(Collection<T> results, long totalElements, boolean hasPreviousPage, boolean hasNextPage) {
        super(results, totalElements, hasPreviousPage, hasNextPage);
    }

    public ResultList(Collection<T> results, long totalElements, int pageNo, int pageSize) {
        super(results, totalElements, pageNo, pageSize);
    }

    public ResultList(Page<T> page) {
        super(page);
    }

    public ResultList(Collection<T> results) {
        super(results);
    }
}
