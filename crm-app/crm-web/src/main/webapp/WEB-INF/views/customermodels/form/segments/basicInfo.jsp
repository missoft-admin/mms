<%@ include file="../../../common/taglibs.jsp" %>


  <fieldset class="form-horizontal container-fluid span3">
    <legend class="memberDetails">Basic Details</legend>
    <div class="pull-left personalData1">
      <div class="control-group">
        <spring:message code="label_com_transretail_crm_entity_customermodel_firstname" var="firstName" />
        <label for="firstName" class="control-label"> <b class="required">*</b><c:out value="${firstName}" /></label>
        <div class="controls">
          <input type="text" name="firstName" class="form-control" placeholder="${firstName}" />
        </div>
      </div>
      <div class="control-group">
        <spring:message code="label_com_transretail_crm_entity_customermodel_lastname" var="lastName" />
        <label for="lastName" class="control-label"><c:out value="${lastName}" /></label>
        <div class="controls">
          <input type="text" name="lastName" class="form-control" placeholder="${lastName}" />
        </div>
      </div>
      <div class="control-group">
        <spring:message code="label_com_transretail_crm_entity_customermodel_idnumber" var="idNumber" />
        <label for="idNumber" class="control-label"><c:out value="${idNumber}" /></label>
        <div class="controls">
          <form:hidden path="idNumber" />
          <input type="text" name="idNumberTxt" class="form-control" placeholder="${idNumber}" />
            <div class="radio mt10"><input type="radio" name="idNumberType" value="KTP"><label><spring:message code="member_label_ktp" /></label></div>
            <div class="radio"><input type="radio" name="idNumberType" value="SIM"><label><spring:message code="member_label_sim" /></label></div>
            <div class="radio"><input type="radio" name="idNumberType" value="PASSPORT"><label><spring:message code="member_label_passport" /></label></div>
        </div>
      </div>
      <div class="control-group professionalDetails">
        <spring:message code="label_com_transretail_crm_entity_customermodel_position" var="position" />
        <label for="professionalProfile.position" class="control-label"><c:out value="${position}:" /></label>
        <div class="controls">
          <form:input path="professionalProfile.position" cssClass="form-control" placeholder="${position}" />
        </div>
      </div>
      <%-- <div class="control-group"><spring:message code="label_com_transretail_crm_entity_customermodel_ktpid" var="ktpId" />
        <label for="ktpId" class="control-label"><b class="required professionalDetails">*</b><c:out value="${ktpId}" /></label>
        <div class="controls">
          <form:input path="ktpId" cssClass="form-control" placeholder="${ktpId}" />
        </div>
      </div> --%>
      <div class="control-group">
        <label for="birthdate" class="control-label"> <b class="required">*</b> <spring:message code="label_com_transretail_crm_entity_customermodel_birthdate" /></label>
        <div class="controls"><form:input path="customerProfile.birthdate" id="birthdate" cssClass="birthdate" /></div>
      </div>
      <div class="control-group">
        <label for="gender" class="control-label"> <b class="required">*</b> <spring:message code="label_com_transretail_crm_entity_customermodel_gender" /></label>
        <div class="controls"><form:select path="customerProfile.gender" items="${gender}" itemLabel="description" itemValue="code" /></div>
      </div>
    </div>
  </fieldset>