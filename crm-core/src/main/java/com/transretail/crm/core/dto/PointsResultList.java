package com.transretail.crm.core.dto;


import java.util.Collection;

import org.springframework.data.domain.Page;

import com.transretail.crm.common.service.dto.response.AbstractResultListDTO;


public class PointsResultList extends AbstractResultListDTO<PointsDto> {
    public PointsResultList(Collection<PointsDto> results, long totalElements, boolean hasPreviousPage, boolean hasNextPage) {
        super(results, totalElements, hasPreviousPage, hasNextPage);
    }
    
    public PointsResultList( Page<PointsDto> page ) {
    	super(page);
    }
}
