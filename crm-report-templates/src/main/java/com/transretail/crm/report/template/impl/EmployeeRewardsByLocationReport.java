package com.transretail.crm.report.template.impl;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.mysema.query.BooleanBuilder;
import com.mysema.query.Tuple;
import com.mysema.query.jpa.impl.JPAQuery;
import com.mysema.query.types.expr.BooleanExpression;
import com.transretail.crm.common.reporting.jasper.ClasspathFileResolver;
import com.transretail.crm.common.reporting.jasper.DefaultJRProcessor;
import com.transretail.crm.common.reporting.jasper.JRProcessor;
import com.transretail.crm.common.util.DateUtil;
import com.transretail.crm.core.entity.QMemberModel;
import com.transretail.crm.core.entity.QPointsTxnModel;
import com.transretail.crm.core.entity.RewardSchemeModel;
import com.transretail.crm.core.entity.enums.PointTxnType;
import com.transretail.crm.core.entity.enums.ReportType;
import com.transretail.crm.core.repo.RewardSchemeRepo;
import com.transretail.crm.core.service.CodePropertiesService;
import com.transretail.crm.core.service.StoreService;
import com.transretail.crm.report.template.FilterField;
import com.transretail.crm.report.template.LOVFactory;
import com.transretail.crm.report.template.NoDataFoundSupport;
import com.transretail.crm.report.template.ReportTemplate;
import java.util.*;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.apache.commons.lang3.StringUtils;
import org.joda.time.LocalDate;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.MessageSourceAware;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.context.support.MessageSourceAccessor;
import org.springframework.stereotype.Service;

/**
 * @author Ervy V. Bigornia (ebigornia@exist.com)
 */
@Service("employeeRewardsByLocationReport")
public class EmployeeRewardsByLocationReport implements ReportTemplate, MessageSourceAware, NoDataFoundSupport {

    protected static final String TEMPLATE_NAME = "Employee Rewards by Location Report";
    protected static final String REPORT_NAME = "reports/employee_rewards_by_location.jasper";
    protected static final String FILTER_DATE = "date";
    protected static final String FILTER_MONTH_YEAR_FORMAT = "MMM-yyyy";
    protected static final DateTimeFormatter MONTH_YR_FMT = DateTimeFormat.forPattern(FILTER_MONTH_YEAR_FORMAT).withLocale(LocaleContextHolder.getLocale());
    protected static final String FILTER_STORES = "stores";
    private static final String SEARCH_DATE_FORMAT = DateUtil.SEARCH_DATE_FORMAT;
    private static final DateTimeFormatter INPUT_DATE_PATTERN = DateTimeFormat.forPattern(SEARCH_DATE_FORMAT);
    private static final DateTimeFormatter FULL_DATE_PATTERN = DateTimeFormat.forPattern("MMMM d, yyyy");
    private static final String PARAM_DATE_UP_TO = "dateUpTo";
    private static final String PARAM_IS_EMPTY = "isEmpty";
    private static final String PARAM_NO_DATA_FOUND = "noDataFound";
    private MessageSourceAccessor messageSource;
    @PersistenceContext
    private EntityManager em;
    @Autowired
    private RewardSchemeRepo rewardSchemeRepo;
    @Autowired
    private StoreService storeService;
    @Autowired
    private CodePropertiesService codePropertiesService;
    @Autowired
    private LOVFactory lOVFactory;

    private static final ReportType[] REPORT_TYPES = {ReportType.EXCEL};

    @Override
    public ReportType[] getReportTypes() {
        return REPORT_TYPES;
    }

    @Override
    public void setMessageSource(MessageSource messageSource) {
        this.messageSource = new MessageSourceAccessor(messageSource);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getName() {
        return TEMPLATE_NAME;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getDescription() {
        return messageSource.getMessage("employee.rewards.by.location.report.title");
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Set<FilterField> getFilters() {
        Set<FilterField> filters = new LinkedHashSet<FilterField>(2);
        filters.add(FilterField.createDateField(FILTER_DATE,
                messageSource.getMessage("employee.rewards.by.location.filter.date"),
                FILTER_MONTH_YEAR_FORMAT, FilterField.DateTypeOption.YEAR_MONTH, true));
        filters.add(FilterField.createMultipleSelectField(FILTER_STORES,
                messageSource.getMessage("employee.rewards.by.location.filter.stores"), lOVFactory.getStoreOptions()));
        return filters;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean canView(Set<String> permissions) {
        return permissions.contains("REPORT_EMPLOYEE_REWARDS_BY_LOCATION_SUMMARY_VIEW");
    }

    private JPAQuery createQuery(Map<String, String> map) {
        LocalDate dateFrom = MONTH_YR_FMT.parseLocalDate(map.get(FILTER_DATE));
        String stores = map.get(FILTER_STORES);
        QPointsTxnModel qPointsTxnModel = QPointsTxnModel.pointsTxnModel;
        QMemberModel qMemberModel = QMemberModel.memberModel;

        int yearMonth = dateFrom.getYear() * 100 + dateFrom.getMonthOfYear(); // please see query dsl DateTimePathExpression.yearMonth()
        BooleanExpression predicate = qMemberModel.memberType.code.eq(codePropertiesService.getDetailMemberTypeEmployee())
                .and(qPointsTxnModel.transactionType.eq(PointTxnType.REWARD))
                .and(qPointsTxnModel.transactionDateTime.yearMonth().eq(yearMonth));

        if (StringUtils.isNotBlank(stores)) {
            String storeList[] = stores.split("!");
            BooleanBuilder builder = new BooleanBuilder();
            for (String storeCode : storeList) {
                builder.or(qMemberModel.registeredStore.eq(storeCode));
            }
            predicate = predicate.and(builder.getValue());
        }

        return new JPAQuery(em).from(qPointsTxnModel)
                .join(qPointsTxnModel.memberModel, qMemberModel)
             
                .where(predicate);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public JRProcessor createJrProcessor(Map<String, String> map) {
        LocalDate dateFrom = MONTH_YR_FMT.parseLocalDate(map.get(FILTER_DATE));
        String stores = map.get(FILTER_STORES);
        StringBuilder dateRange = new StringBuilder(50);
        dateRange.append(messageSource.getMessage("employee.rewards.by.location.report.label.datefrom"));
        dateRange.append(" ");
        dateRange.append(FULL_DATE_PATTERN.print(dateFrom));
        QPointsTxnModel qPointsTxnModel = QPointsTxnModel.pointsTxnModel;
        QMemberModel qMemberModel = QMemberModel.memberModel;
        Map<String, Object> parameters = Maps.newHashMap();
        parameters.put(PARAM_DATE_UP_TO, dateFrom.toString(MONTH_YR_FMT));
        List<Tuple> tuples = createQuery(map)
                .groupBy(qMemberModel.registeredStore, qPointsTxnModel.transactionPoints)
                .orderBy(qMemberModel.registeredStore.asc(), qPointsTxnModel.transactionPoints.asc())
                .list(qMemberModel.registeredStore, qPointsTxnModel.transactionPoints,
                        qPointsTxnModel.transactionPoints.count());
        List<ReportBean> beansDataSource = Lists.newArrayList();

        if (!tuples.isEmpty()) {
            Map<String, ReportBean> reportBeanMap = Maps.newHashMap();
            List<RewardSchemeModel> rewardSchemeModels = rewardSchemeRepo.findAllActivePointsSchemes();
            double highest = 0, lowest = 0;

            for (RewardSchemeModel reward : rewardSchemeModels) {
                if (highest < reward.getValueTo()) {
                    highest = reward.getValueTo();
                }
                if (lowest > reward.getValueFrom()) {
                    lowest = reward.getValueFrom();
                }
            }

            for (Tuple tuple : tuples) {

                String storeCode = tuple.get(qMemberModel.registeredStore);
                String storeName = storeCode;
                try {
                    storeName = storeService.getStoreByCode(storeCode).getName();
                } catch (NullPointerException npe) {

                }

                double points = 0;
                long count = 0;
                int scheme = 0;

                if (null != tuple.get(qPointsTxnModel.transactionPoints)) {
                    points = tuple.get(qPointsTxnModel.transactionPoints);
                    count = tuple.get(qPointsTxnModel.transactionPoints.count());
                }

                ReportBean bean = reportBeanMap.get(storeCode);
                if (bean == null) {
                    bean = new ReportBean(storeName);
                    reportBeanMap.put(storeCode, bean);
                }

                for (RewardSchemeModel reward : rewardSchemeModels) {
                    if (points == reward.getAmount()) {
                        if (points == 25000) {
                            scheme = 1;
                        } else if (points == 50000) {
                            scheme = 2;
                        } else if (points == 75000) {
                            scheme = 3;
                        } else if (points == 100000) {
                            scheme = 4;
                        }
                        break;
                    }
                }

                switch (scheme) {
                    case 1:
                        bean.incrementRewardsRp25000(count, count * points);
                        break;
                    case 2:
                        bean.incrementRewardsRp50000(count, count * points);
                        break;
                    case 3:
                        bean.incrementRewardsRp75000(count, count * points);
                        break;
                    case 4:
                        bean.incrementRewardsRp100000(count, count * points);
                        break;
                    default:
                        break;
                }
            }
            beansDataSource.addAll(reportBeanMap.values());

            Collections.sort(beansDataSource, new Comparator<ReportBean>() {
                @Override
                public int compare(ReportBean o1, ReportBean o2) {
                    String store1 = StringUtils.defaultString(o1.getStoreName());
                    String store2 = StringUtils.defaultString(o2.getStoreName());
                    return store1.compareTo(store2);
                }
            });

            List<ReportBean> toRemove = new ArrayList<ReportBean>();
            for (ReportBean bean : beansDataSource) {
                if (null == bean.getStoreName() || bean.getStoreName().equals("")) {
                    toRemove.add(bean);
                }
            }
            beansDataSource.removeAll(toRemove);
        }

        if (beansDataSource.isEmpty()) {
            parameters.put(PARAM_IS_EMPTY, true);
            parameters.put(PARAM_NO_DATA_FOUND, "No data found");
            beansDataSource.add(new ReportBean());
        }

        beansDataSource.add(0, new ReportBean());
        DefaultJRProcessor jrProcessor = new DefaultJRProcessor(REPORT_NAME, beansDataSource);
        jrProcessor.addParameters(parameters);
        jrProcessor.setFileResolver(new ClasspathFileResolver("reports"));
        return jrProcessor;
    }

    @Override
    public boolean isEmpty(Map<String, String> filter) {
        return createQuery(filter).notExists();
    }

    /**
     * Bean used by member card activation jasper report. Access modifier is
     * intentionallly set to public because jasper fill manager cannot read
     * protected or private classes
     */
    public static class ReportBean {

        private String storeName;
        private long totalNoOfRewardsIssuedAtRp25000;
        private double rewardsAmountInRp25000;
        private long totalNoOfRewardsIssuedAtRp50000;
        private double rewardsAmountInRp50000;
        private long totalNoOfRewardsIssuedAtRp75000;
        private double rewardsAmountInRp75000;
        private long totalNoOfRewardsIssuedAtRp100000;
        private double rewardsAmountInRp100000;
        private double totalRewardsAmountInRp;

        public ReportBean() {

        }

        public ReportBean(String storeName) {
            this.storeName = storeName;
        }

        public String getStoreName() {
            return storeName;
        }

        public void setStoreName(String storeName) {
            this.storeName = storeName;
        }

        public long getTotalNoOfRewardsIssuedAtRp25000() {
            return totalNoOfRewardsIssuedAtRp25000;
        }

        public void setTotalNoOfRewardsIssuedAtRp25000(long totalNoOfRewardsIssuedAtRp25000) {
            this.totalNoOfRewardsIssuedAtRp25000 = totalNoOfRewardsIssuedAtRp25000;
        }

        public double getRewardsAmountInRp25000() {
            return rewardsAmountInRp25000;
        }

        public void setRewardsAmountInRp25000(double rewardsAmountInRp25000) {
            this.rewardsAmountInRp25000 = rewardsAmountInRp25000;
        }

        public long getTotalNoOfRewardsIssuedAtRp50000() {
            return totalNoOfRewardsIssuedAtRp50000;
        }

        public void setTotalNoOfRewardsIssuedAtRp50000(long totalNoOfRewardsIssuedAtRp50000) {
            this.totalNoOfRewardsIssuedAtRp50000 = totalNoOfRewardsIssuedAtRp50000;
        }

        public double getRewardsAmountInRp50000() {
            return rewardsAmountInRp50000;
        }

        public void setRewardsAmountInRp50000(double rewardsAmountInRp50000) {
            this.rewardsAmountInRp50000 = rewardsAmountInRp50000;
        }

        public long getTotalNoOfRewardsIssuedAtRp75000() {
            return totalNoOfRewardsIssuedAtRp75000;
        }

        public void setTotalNoOfRewardsIssuedAtRp75000(long totalNoOfRewardsIssuedAtRp75000) {
            this.totalNoOfRewardsIssuedAtRp75000 = totalNoOfRewardsIssuedAtRp75000;
        }

        public double getRewardsAmountInRp75000() {
            return rewardsAmountInRp75000;
        }

        public void setRewardsAmountInRp75000(double rewardsAmountInRp75000) {
            this.rewardsAmountInRp75000 = rewardsAmountInRp75000;
        }

        public long getTotalNoOfRewardsIssuedAtRp100000() {
            return totalNoOfRewardsIssuedAtRp100000;
        }

        public void setTotalNoOfRewardsIssuedAtRp100000(long totalNoOfRewardsIssuedAtRp100000) {
            this.totalNoOfRewardsIssuedAtRp100000 = totalNoOfRewardsIssuedAtRp100000;
        }

        public double getRewardsAmountInRp100000() {
            return rewardsAmountInRp100000;
        }

        public void setRewardsAmountInRp100000(double rewardsAmountInRp100000) {
            this.rewardsAmountInRp100000 = rewardsAmountInRp100000;
        }

        public double getTotalRewardsAmountInRp() {
            return totalRewardsAmountInRp;
        }

        public void setTotalRewardsAmountInRp(double totalRewardsAmountInRp) {
            this.totalRewardsAmountInRp = totalRewardsAmountInRp;
        }

        public void incrementRewardsRp25000(long count, double reward) {
            totalNoOfRewardsIssuedAtRp25000 += count;
            rewardsAmountInRp25000 += reward;
            incrementTotalRewardsAmountInRp(reward);
        }

        public void incrementRewardsRp50000(long count, double reward) {
            totalNoOfRewardsIssuedAtRp50000 += count;
            rewardsAmountInRp50000 += reward;
            incrementTotalRewardsAmountInRp(reward);
        }

        public void incrementRewardsRp75000(long count, double reward) {
            totalNoOfRewardsIssuedAtRp75000 += count;
            rewardsAmountInRp75000 += reward;
            incrementTotalRewardsAmountInRp(reward);
        }

        public void incrementRewardsRp100000(long count, double reward) {
            totalNoOfRewardsIssuedAtRp100000 += count;
            rewardsAmountInRp100000 += reward;
            incrementTotalRewardsAmountInRp(reward);
        }

        public void incrementTotalRewardsAmountInRp(double reward) {
            totalRewardsAmountInRp += reward;
        }
    }
}
