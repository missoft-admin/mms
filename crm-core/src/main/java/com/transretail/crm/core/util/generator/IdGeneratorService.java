package com.transretail.crm.core.util.generator;


import java.util.Map;

/*
 *
 * interface for all id generation implementation
 * Implementation of this class would define specific implementation to generate id
 */
public interface IdGeneratorService {


    /**
     *
     * @return
     */
    public String  generateId();


    /**
     *
     * @param length  - length of id
     * @param alphanumeric
     * @return
     */
    public String generateId(int length, boolean alphanumeric);


    /**
     *
     * @param props
     * @return
     */
    public String generateId(Map props);


    /**
     *
     * @param prepend - String to prepend
     * @return
     */
    public String  generateId( String prepend );




}