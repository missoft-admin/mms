<%@ include file="../../common/taglibs.jsp" %>

 
    <c:choose>
    <c:when test="${isMarketingHead}">
      <c:if test="${promotion.status ne promotionStatuses[0].code}">
        <button class="btn btn-primary promotionStatus" id="promotionRemarksForm_approveButton" type="submit" data-loading-text="<spring:message code="loading_text" />" data-value="${promotionStatuses[0].code}"><spring:message code="label_${promotionStatuses[0].code}"/></button>
        <button class="btn btn-primary promotionStatus" id="promotionRemarksForm_rejectButton" type="submit" data-loading-text="<spring:message code="loading_text" />" data-value="${promotionStatuses[1].code}"><spring:message code="label_${promotionStatuses[1].code}"/></button>
      </c:if>
      <c:if test="${promotion.status eq promotionStatuses[0].code}">
        <button class="btn btn-primary promotionStatus" id="promotionRemarksForm_disableButton" type="submit" data-value="${promotionStatuses[2].code}" ><spring:message code="label_${promotionStatuses[2].code}"/></button>
      </c:if>
    </c:when>
    <c:otherwise>
      <button class="btn btn-primary" id="promotionForm_saveButton" type="submit" data-loading-text="<spring:message code="loading_text" />" ><spring:message code="label_submit_for_approval"/></button>
    </c:otherwise>
    </c:choose>
    <button class="btn" id="promotionForm_cancelButton" type="button"><spring:message code="label_cancel"/></button>
  
        
