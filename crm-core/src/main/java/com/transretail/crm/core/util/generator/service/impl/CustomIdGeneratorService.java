package com.transretail.crm.core.util.generator.service.impl;


import java.util.Date;
import java.util.Map;
import java.util.Random;

import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.transretail.crm.core.util.generator.IdGeneratorService;

/**
 * ID generator for member pin
 */
@Service
@Transactional
public class CustomIdGeneratorService implements IdGeneratorService {


    /**
     * pin length defaults to 6 and numeric only
     *
     * @return
     */
    @Override
    public String generateId() {

        int defaultLength = 6;

        Random r = new Random();

        String number = "";

        int counter = 0;

        while (counter++ < defaultLength) {
            number += r.nextInt(9);

        }


        return number;
    }

    @Override
    public String generateId(int length, boolean alphanumeric) {
        return null;
    }

    @SuppressWarnings("rawtypes")
	@Override
    public String generateId(Map props) {
        return null;
    }


    @Override
	public String generateId(String prepend) {

        return ( StringUtils.isBlank( prepend )? "" : prepend.toUpperCase() )  + 
        		"_" + new Date().getTime();
	}
}
