package com.transretail.crm.loyalty.repo;

import org.springframework.stereotype.Repository;

import com.transretail.crm.common.repo.CrmQueryDslPredicateExecutor;
import com.transretail.crm.loyalty.entity.LoyaltyPhysicalCount;
import com.transretail.crm.loyalty.repo.custom.LoyaltyPhysicalCountRepoCustom;

@Repository
public interface LoyaltyPhysicalCountRepo extends CrmQueryDslPredicateExecutor<LoyaltyPhysicalCount, Long>, LoyaltyPhysicalCountRepoCustom {

}
