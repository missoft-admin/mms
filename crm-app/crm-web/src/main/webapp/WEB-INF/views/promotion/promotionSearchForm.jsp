<%@ include file="../common/taglibs.jsp" %>

<style type="text/css">
<!--
  .well .input {
    background-color: #fff;
    border: 1px solid #E8E8E8;
    box-shadow: none;    
    padding: 6px;
    height: 18px;
    margin-left: 5px;
  }
  .well .btn-primary {
    margin-left: 5px;
  }
  .well select, .well .input {
    margin: 5px;
  }
  .label-single {
      width: 150px;
  }
  
-->
</style>

<div id="content_promotionSearchForm">


  <c:url var="url_listPromotions" value="/promotion"/>
  <c:url var="url_search" value="/promotion/search" />
  <form:form id="promotionSearchForm" name="promotionSearch" modelAttribute="promotionSearch" action="${url_search}" data-url="${url_listPromotions}" class="">
    <div class="form-horizontal well">	
	    <div class="form-inline">
        <label for="" class="label-single"><h5><spring:message code="search_by"/></h5></label>
          <select id="modelType" name="modelType">
            <c:forEach var="modelType" items="${promotionModelTypes}">
              <option value="${modelType}"><spring:message code="promo_${modelType}" /></option>
            </c:forEach>
          </select>
	        <select id="searchField">
	          <c:forEach var="searchField" items="${promotionFields}">
	            <option value="${searchField}"><spring:message code="promo_field_${searchField}" /></option>
	          </c:forEach>
	        </select>
	        <div class="inline">
	          <div class="">
	            <input id="searchValue" class="input" />
	            <input id="promotionSearchForm_searchButton" class="btn btn-primary" type="submit"  value="<spring:message code="label_search"/>" />
	          </div>
	        </div>
	    </div>
      
      <div class="form-inline">
        <label for="" class="label-single"><h5><spring:message code="search_label_promotion"/>:</h5></label>
          <spring:message code="promotion_prop_startdate" var="startDate" />
          <input type="text" id="startDate" name="startDate"  placeholder="${startDate}" class="input"/>
          <spring:message code="promotion_prop_enddate" var="endDate" />
          <input type="text" id="endDate" name="endDate"  placeholder="${endDate}" class="input" />
  
          <select name="status">
            <option value=""><spring:message code="promotion_prop_status" /></option>
          <c:forEach items="${statuses}" var="item" >
            <option value="${item.code}"><spring:message code="lookup_${item.code}" text="Code Not Set" /></option>
          </c:forEach>
          </select>

          <select name="rewardType">
            <option value=""><spring:message code="promotion_prop_rewardtype" /></option>
          <c:forEach items="${rewardTypes}" var="item" >
            <option value="${item.code}">${item.description}</option>
          </c:forEach>
          </select>
      </div>
        
    </div>
  </form:form>


  <script src="<c:url value="/js/viewspecific/promotion/promotionSearchForm.js"/>" type="text/javascript"><![CDATA[&nbsp; ]]></script>
  <script src='<c:url value="/js/bootstrap/bootstrap-datepicker.js"/>' type='text/javascript'><![CDATA[&nbsp; ]]></script>
  <link href="<c:url value="/css/bootstrap/datepicker.css" />" rel="stylesheet" />

</div>