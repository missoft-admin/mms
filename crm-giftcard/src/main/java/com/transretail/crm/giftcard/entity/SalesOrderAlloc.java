package com.transretail.crm.giftcard.entity;


import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.google.common.base.Objects;
import com.mysema.query.annotations.QueryInit;
import com.transretail.crm.core.entity.support.CustomAuditableEntity;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;


@Entity
@Table(name = "CRM_SALES_ORD_ALLOC")
public class SalesOrderAlloc extends CustomAuditableEntity<Long> implements Serializable {

	private static final long serialVersionUID = 1L;
	public static enum SoAllocRangeFlag { };

	@ManyToOne( fetch = FetchType.LAZY )
    @JoinColumn( name = "SO_ITEM" )
	@QueryInit({"order.*"})
	private SalesOrderItem soItem;

//	@OneToOne(fetch = FetchType.LAZY)
//    @JoinColumn(name = "GC_INVENTORY")
//	@QueryInit({"profile.*", "status.*"})
//    private GiftCardInventory gcInventory;
//
//    @Column(name = "IS_RANGE", length = 1)
//    @Type(type = "yes_no")
//    private Boolean isRange;

    @Column(name = "SERIES_START", nullable = false, length = 16, unique = true)
    private Long seriesStart;

    @Column(name = "SERIES_END", nullable = false, length = 16, unique = true)
    private Long seriesEnd;



	public SalesOrderItem getSoItem() {
		return soItem;
	}

	public void setSoItem(SalesOrderItem soItem) {
		this.soItem = soItem;
	}

//	public GiftCardInventory getGcInventory() {
//		return gcInventory;
//	}
//
//	public void setGcInventory(GiftCardInventory gcInventory) {
//		this.gcInventory = gcInventory;
//	}
//
//	public Boolean getIsRange() {
//		return isRange;
//	}
//
//	public void setIsRange(Boolean isRange) {
//		this.isRange = isRange;
//	}

	public Long getSeriesStart() {
		return seriesStart;
	}

	public void setSeriesStart(Long seriesStart) {
		this.seriesStart = seriesStart;
	}

	public Long getSeriesEnd() {
		return seriesEnd;
	}

	public void setSeriesEnd(Long seriesEnd) {
		this.seriesEnd = seriesEnd;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;

		if (o == null || getClass() != o.getClass()) return false;

		SalesOrderAlloc that = (SalesOrderAlloc) o;

		return new EqualsBuilder()
				.appendSuper(super.equals(o))
				.isEquals();
	}

	@Override
	public int hashCode() {
		return 31;
	}

	@Override
	public String toString() {
		return Objects.toStringHelper(this)
				.add("soItem", soItem)
				.add("seriesStart", seriesStart)
				.add("seriesEnd", seriesEnd)
				.toString();
	}
}
