/*
 * Copyright (c) 2012. St. Luke's Medical Center
 * All rights reserved.
 */
package com.transretail.crm.common.service.util;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.BooleanUtils;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.util.CollectionUtils;

import com.mysema.query.jpa.JPQLQuery;
import com.mysema.query.types.Expression;
import com.mysema.query.types.OrderSpecifier;
import com.mysema.query.types.expr.ComparableExpressionBase;
import com.mysema.query.types.path.PathBuilder;
import com.mysema.query.types.path.PathBuilderFactory;
import com.transretail.crm.common.repo.domain.CustomPageRequest;
import com.transretail.crm.common.service.dto.request.PagingParam;
import com.transretail.crm.common.service.dto.request.PagingParam.DataType;

/**
 *
 */
public enum SpringDataPagingUtil {
    INSTANCE;

    public Pageable createPageable(PagingParam param) {
        return createPageable(param, null);
    }
    
    @SuppressWarnings({"rawtypes", "unchecked"})
    public OrderSpecifier createOrderSpecifier(PagingParam param, Class<?> domainClass) {
    	if (param.getOrder() != null) {
	    	PathBuilder<?> builder = new PathBuilderFactory().create(domainClass);
	    	for (Map.Entry<String, Boolean> entry : param.getOrder().entrySet()) {
				Expression<String> property = builder.getString(entry.getKey()).lower();
				return new OrderSpecifier(entry.getValue() ? com.mysema.query.types.Order.ASC
	                    : com.mysema.query.types.Order.DESC, property);
	    	}
    	}
    	return null;
    }


    public Pageable createPageable(PagingParam param, Map<String, String> fieldSortNameMap) {
        Pageable request = null;
        if (param != null) {
            if (param.getOrder() == null || param.getOrder().isEmpty()) {
                request = new CustomPageRequest(param.getPageNo(), param.getPageSize());
            } else if (fieldSortNameMap == null || fieldSortNameMap.isEmpty()) {
                request = new CustomPageRequest(param.getPageNo(), param.getPageSize(),
                    new Sort(createSortOrder(param.getOrder(), param.getDataTypes())));
            } else {
                request = new CustomPageRequest(param.getPageNo(), param.getPageSize(),
                    new Sort(createSortOrder(param.getOrder(), param.getDataTypes(), fieldSortNameMap)));
            }
        }
        return request;
    }

    public List<Sort.Order> createSortOrder(Map<String, Boolean> order, Map<String, DataType> dataTypes) {
        List<Sort.Order> result = new ArrayList<Sort.Order>();
        for (String column : order.keySet()) {
        	if(dataTypes == null || dataTypes.get(column) == null || dataTypes.get(column).compareTo(DataType.STRING) == 0)
        		result.add(new Sort.Order(toDirection(order.get(column)), column).ignoreCase());
        	else
        		result.add(new Sort.Order(toDirection(order.get(column)), column));
        }
        return result;
    }

    private List<Sort.Order> createSortOrder(Map<String, Boolean> order, Map<String, DataType> dataTypes, Map<String, String> fieldSortNameMap) {
        List<Sort.Order> result = new ArrayList<Sort.Order>();
        for (String column : order.keySet()) {
            String field = fieldSortNameMap.get(column);
            if (field == null) {
                field = column;
            }
            if(dataTypes == null || dataTypes.get(column) == null || dataTypes.get(column).compareTo(DataType.STRING) == 0)
        		result.add(new Sort.Order(toDirection(order.get(column)), field).ignoreCase());
        	else
        		result.add(new Sort.Order(toDirection(order.get(column)), field));
        }
        return result;
    }

    private Sort.Direction toDirection(Boolean asc) {
        if (BooleanUtils.toBoolean(asc)) {
            return Sort.Direction.ASC;
        } else {
            return Sort.Direction.DESC;
        }
    }

    @SuppressWarnings({"rawtypes", "unchecked"})
    public JPQLQuery applyPagination(JPQLQuery query, PagingParam pagination, Class<?> domainClass) {
        if (pagination != null && pagination.getPageSize() >= 0 && pagination.getPageNo() >= 0) {
            query.offset(pagination.getPageNo() * pagination.getPageSize());
            query.limit(pagination.getPageSize());
            if (pagination.getOrder() != null) {
                PathBuilder<?> builder = new PathBuilderFactory().create(domainClass);
                for (Map.Entry<String, Boolean> entry : pagination.getOrder().entrySet()) {
                    Expression<Object> property = builder.get(entry.getKey());
                    query.orderBy(new OrderSpecifier(entry.getValue() ? com.mysema.query.types.Order.ASC
                        : com.mysema.query.types.Order.DESC, property));
                }
            }
        }
        return query;
    }

}

