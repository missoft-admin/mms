<%@ include file="../../common/taglibs.jsp" %>

<style type="text/css">
<!--

  #productTable tr td:last-child {
    border-left: 0px;
  }

  #addProductBtn.btn-small {
    margin-bottom: 5px;
  }
  table select, textarea, table input[type="text"], input[type="password"], input[type="datetime"], input[type="datetime-local"], input[type="date"], input[type="month"], input[type="time"], input[type="week"], input[type="number"], input[type="email"], input[type="url"], input[type="search"], input[type="tel"], input[type="color"], .uneditable-input {
      margin-bottom: 0px;
      margin-top: 0px;
  }
  
-->
</style>



<c:set var="ENCTYPE" value="application/x-www-form-urlencoded"/>

<div class="modal-dialog">
  <div class="modal-content">
    <spring:message code="label_manufacture_order" var="typeName" />
    
    <div class="modal-header">
      <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
      <h4>
      <c:if test="${orderForm.orderType == 'B2B_SALES' || orderForm.orderType == 'B2B_ADV_SALES' || orderForm.orderType == 'VOUCHER'}"><spring:message code="sales_order"/></c:if>
      <c:if test="${orderForm.orderType == 'YEARLY_DISCOUNT'}"><spring:message code="yearly_discount"/></c:if>
      <c:if test="${orderForm.orderType == 'INTERNAL_ORDER'}"><spring:message code="internal_order"/></c:if>
      </h4>
    </div>
 
    <div class="modal-body">
    
      <div id="contentError" class="hide alert alert-error">
        <button type="button" class="close" onclick="$(this).parent().hide();">&times;</button>
        <div><form:errors path="*"/></div>
      </div>
    
      
      <form:form id="orderForm" name="orderForm" modelAttribute="orderForm" action="${pageContext.request.contextPath}/giftcard/salesorder/approve/" method="POST" enctype="${ENCTYPE}" cssClass="form-reset form-horizontal" >
      <form:hidden path="id"/>
      
      <div class="contentMain">
      
      <div class="row-fluid">
      
        
      
        <div class="span6">
        <div class="control-group">
          <spring:message code="sales_order_no" var="orderNum" />
          <label for="orderNo" class="control-label">${orderNum}</label> 
          <div class="controls">
            <form:input path="orderNo" class="form-control" placeholder="${orderNum}" disabled="true"/>
          </div>
        </div>
        
        <div class="control-group">
          <spring:message code="sales_order_type" var="orderTyp" />
          <label for="orderNo" class="control-label">${orderTyp}</label> 
          <div class="controls">
            <input type="text" value="${orderForm.orderType}" class="form-control" disabled="disabled" />
          </div>
        </div>
        
        <div class="control-group">
          <spring:message code="sales_order_date" var="orderDt" />
          <label for="orderDate" class="control-label">${orderDt}</label> 
          <div class="controls">
            <form:input path="orderDate" class="form-control orderDate" placeholder="${orderDt}" disabled="true" id="orderDateField" />
          </div>
        </div>
        
        <div class="control-group">
          <spring:message code="sales_order_notes" var="notes" />
          <label for="customerId" class="control-label">${notes}</label> 
          <div class="controls">
            <form:textarea path="notes" class="form-control"  disabled="true" />
          </div>
        </div>
        
        <c:if test="${orderForm.orderType == 'INTERNAL_ORDER'}">
        
        <div class="control-group">
          <spring:message code="sales_order_category" var="category" />
          <label for="category" class="control-label">${category}</label> 
          <div class="controls">
            <form:select path="category" items="${categories}" disabled="true" itemLabel="description" itemValue="code" class="form-control"  />
          </div>
        </div>
        
        </c:if>
        
        <c:if test="${orderForm.orderType == 'B2B_SALES' || orderForm.orderType == 'B2B_ADV_SALES'}">
        
        <div class="control-group" id="discountTypeContainer">
          <div class="controls">
            <c:forEach items="${discTypes}" var="item">
            <div class="radio">
              <form:radiobutton disabled="true" path="discType" value="${item}"/><label style="text-align:left"><spring:message code="sales_order_${item}" /></label>
            </div>
            </c:forEach>
          </div>
        </div>
        
        </c:if>
        
        <c:if test="${orderForm.orderType == 'B2B_SALES' || orderForm.orderType == 'B2B_ADV_SALES' || orderForm.orderType == 'REPLACEMENT'}">
        <div class="control-group">
          <spring:message code="gc.so.delivery.shippingfee" var="shippingFee" />
          <label for="shippingFee" class="control-label">${shippingFee}</label> 
          <div class="controls">
            <%-- <form:input path="shippingFee" class="form-control" placeholder="${shippingFee}" disabled="true" /> --%>
            <input type="text" class="form-control" placeholder="${shippingFee}" value="<fmt:formatNumber value="${orderForm.shippingFee}" pattern="#,##0" />" disabled="disabled" />
          </div>
        </div>
        </c:if>
        
        
        </div>
        
        <div class="span6">
        
        <div class="control-group">
          <spring:message code="sales_order_customer" var="customer" />
          <label for="customerId" class="control-label">${customer}</label> 
          <div class="controls">
            <%-- <select name="customerId" class="form-control" id="customerIdField" disabled="disabled">
              <c:forEach items="${customers}" var="item">
                <option value="${item.id}" ${item.id == orderForm.customerId ? "selected" : ""} label="${item.name}" data-contact-fullname="${item.contactFullName}" data-contact-no="${item.contactNo}" data-email="${item.email}">${item.name}</option>
                <form:option value="${item.id}" label="${item.name}" data-contact-fullname="${item.contactFullName}" data-contact-no="${item.contactNo}" data-email="${item.email}"/>
              </c:forEach>
            </select> --%>
            <form:input path="customerDesc" disabled="true" />
            <form:hidden path="customerId" id="customerIdField" disabled="true" 
              data-contact-fullname="${cxProfile.contactFullName}" 
              data-contact-no="${cxProfile.contactNo}" 
              data-email="${cxProfile.email}" />
          </div>
        </div>
        
        
        <div class="control-group">
          <spring:message code="sales_order_contact_person" var="contactPerson" />
          <label for="customerId" class="control-label">${contactPerson}</label> 
          <div class="controls">
            <form:input path="contactPerson" id="contactPerson" disabled="true" class="form-control" placeholder="${contactPerson}" />
          </div>
        </div>
        
        <div class="control-group">
          <spring:message code="sales_order_contact_number" var="contactNumber" />
          <label for="customerId" class="control-label">${contactNumber}</label> 
          <div class="controls">
            <form:input path="contactNumber" id="contactNumber" disabled="true" class="form-control" placeholder="${contactNumber}" />
          </div>
        </div>
        
        <div class="control-group">
          <spring:message code="sales_order_email" var="email" />
          <label for="customerId" class="control-label">${email}</label> 
          <div class="controls">
            <form:input path="contactEmail" id="contactEmail" disabled="true" class="form-control" placeholder="${email}" />
          </div>
        </div>
        
        <div class="control-group">
          <spring:message code="order_total_face_amount" var="totalFaceAmount" />
          <label for="totalFaceAmount" class="control-label">${totalFaceAmount}</label> 
          <div class="controls">
            <input type="text" class="form-control" value="<fmt:formatNumber type="number" pattern="#,##0" value="${orderForm.totalFaceAmount}"/>" disabled="disabled" name="totalFaceAmount" />
            <input type="hidden" value="${orderForm.totalFaceAmount}" id="totalFaceAmount" />
          </div>
        </div>
        
        <c:if test="${orderForm.orderType == 'B2B_SALES' || orderForm.orderType == 'B2B_ADV_SALES'}">
        
        <div class="control-group">
          <spring:message code="order_discount" var="discount" />
          <label for="discount" class="control-label">${discount}</label> 
          <div class="controls">
            <input type="text" id="discountFmt" value="0" disabled="disabled" />
          </div>
        </div>
        
        </c:if>
        
        <c:if test="${orderForm.orderType == 'B2B_SALES' || orderForm.orderType == 'B2B_ADV_SALES' || orderForm.orderType == 'VOUCHER'}">
        
        
        <div class="control-group">
          <spring:message code="sales_order_payment_store" var="paymentStore" />
          <label for="paymentStore" class="control-label">${paymentStore}</label> 
          <div class="controls">
            <form:select path="paymentStore" items="${stores}" itemLabel="name" itemValue="code" class="form-control" disabled="true" />
          </div>
        </div>
        
        </c:if>
        
        </div>
        
        <div class="clearfix"></div>
        
        
        <div class="span12">
        
        <div>
          <table id="productTable" class="table table-condensed table-compressed table-striped">
            <thead>
              <tr>
                <th><spring:message code="sales_order_item_product_name" /></th>
                <th><spring:message code="sales_order_item_face_value" /></th>
                <th><spring:message code="sales_order_item_quantity" /></th>
                <th><spring:message code="sales_order_item_face_amount" /></th>
                <th><spring:message code="sales_order_item_print_fee" /></th>
                <th></th>
              </tr>
            </thead>
            <tbody>
                <c:forEach var="item" items="${orderForm.items}" varStatus="status" >
                  <tr data-index="${status.index}">
                    <td>
                    <input type="text" class="input-medium" value="${item.productDesc}" disabled="disabled" />
                    </td>
		    <fmt:formatNumber type="number" pattern="#,##0" value="${item.faceValue}" var="formattedFaceValue"/>
		    <fmt:formatNumber type="number" pattern="#,##0" value="${item.quantity}" var="formattedQuantity"/>
		    <fmt:formatNumber type="number" pattern="#,##0" value="${item.faceAmount}" var="formattedFaceAmount"/>
		    <fmt:formatNumber type="number" pattern="#,##0" value="${item.printFee}" var="formattedPrintFee"/>
                    <td><input type="text" class="input-mini faceValue" value="${formattedFaceValue}" disabled="disabled" name="items[${status.index}].faceValue" /></td>
                    <td><input type="text" class="input-mini quantity" value="${formattedQuantity}" disabled="disabled" name="items[${status.index}].quantity" /></td>
                    <td><input type="text" class="input-medium faceAmount" value="${formattedFaceAmount}" disabled="disabled" name="items[${status.index}].faceAmount" /></td>
                    <td><input type="text" class="input-mini printFee" value="${formattedPrintFee}" disabled="disabled" name="items[${status.index}].printFee" /></td>
                    <td>
                    <div class="checkbox">
                      <input type="checkbox" ${item.freePrintFee == true ? "checked" : ""} disabled="disabled"  name="items[${status.index}].freePrintFee" />
                      <label class="" style="text-align:left"><spring:message code="order_item_free_print_fee" /></label>
                    </div>  
                    </td>
                  </tr>
                </c:forEach>
            </tbody>
          </table>
        </div>
        
        </div>
        
      </div>
          
      </div>
      
      </form:form>
    
    </div>
    <div class="modal-footer">
      <button type="button" data-status="APPROVED" class="orderSubmit btn btn-primary"><spring:message code="label_approve" /></button>
      <button type="button" data-status="DRAFT" class="orderSubmit btn btn-primary"><spring:message code="label_reject" /></button>
      <button type="button" id="orderCancel" class="btn btn-default" data-dismiss="modal"><spring:message code="label_cancel"/></button>
    </div>
  </div>
</div>

<script>
$(document).ready(function() {
  
  var CONTAINER_ERROR = "#contentError > div";
  var CONTENT_ERROR = "#contentError";
  $form = $("#orderForm");
  $orderDialog = $("#orderDialog").css({"width": "900px"});
  
  $(".orderSubmit").click(submitForm);
  
  function submitForm() {
    $(".orderSubmit").prop( "disabled", true );
    var status = $(this).data("status"); 
    if(status == "VALIDATED") {
    	getConfirm('<spring:message code="sales_verified_payment_confirm" />', function(result) { 
			if(result) {
				ajaxSubmit(status);
			} else {
				$(".orderSubmit").prop( "disabled", false );
			}
		});
    } else {
    	ajaxSubmit(status);
    }
  }
  
  $("#orderDateField, #customerIdField").change(toggleDiscountType);
  
  toggleDiscountType();
  
  function toggleDiscountType() {
  	var orderDate = $("#orderDateField").val();
  	var custId = $("#customerIdField").val();
  	if(orderDate != "" && custId != "") {
  		$.get("<c:url value="/giftcard/discountscheme/get/" />" + custId + "/" + orderDate, function(data) {
  	  		var result = data.result;
  	  		$container = $("#discountTypeContainer");
  	  		if(result == null || result == 'SO_YEARLY_DISCOUNT') {
  	  			$container.show();
  	  		} else {
  	  			$container.hide();
  	  			$("input[type='radio']", $container).prop("checked", false);
  	  		}
  	  	}, "json");
  	}
  }
  
  function ajaxSubmit(status) {
	  $.post($form.attr("action") + status, $form.serialize(), function(data) {
	      $(".orderSubmit").prop( "disabled", false );
	      if (data.success) {
	    	  reloadOrderTable();
	      } 
	      else {
	        errorInfo = "";
	        for (i = 0; i < data.result.length; i++) {
	          errorInfo += "<br>" + (i + 1) + ". "
	              + ( data.result[i].code != undefined ? 
	                  data.result[i].code : data.result[i]);
	        }
	        $orderDialog.find(CONTAINER_ERROR).html("Please correct following errors: " + errorInfo);
	        $orderDialog.find(CONTENT_ERROR).show('slow');
	      }
	    });  
  }
  
  updateDiscount();
	
	function updateDiscount() {
		var discType = $("input[name='discType']:checked").val();
		var custId = $("input[name='customerId']").val();
		var orderDate = $("input[name='orderDate']").val();
		var totalFceAmt = $("#totalFaceAmount").val();
		if(discType != null && custId != null && orderDate != null && totalFceAmt != null && totalFceAmt != 0) {
			$.get("<c:url value="/giftcard/discountscheme/get/beforeapproval/" />" + custId + "/" + orderDate + "/" + totalFceAmt + "/" + discType, function(data) {
		  		var result = data.result;
		  		if(result != null && result.discount != null) {
		  			var discount = result.discount;
		  			$("#discountFmt").val(toCurrency(totalFceAmt * discount / 100));
		  		}
		  	}, "json");	
		}
	}
    
	function toCurrency(total) {
		var currency = parseFloat(total, 10).toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, "$1,").toString();
		return currency.slice( 0,-3 );
	}
});
</script>