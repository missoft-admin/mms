<%@include file="../../common/taglibs.jsp" %>

<div class="page-header page-header2">
    <h1><spring:message code="gc_order" /></h1>
</div>

  <div>
    <jsp:include page="ordersearch.jsp" />
  </div>
  
<div class="pull-left mb20">
  <button type="button" class="btn btn-print" id="pendingMOReceiptReport"><spring:message code="pending_receipt_report" /></button>
</div>  
  
<div class="pull-right mb20">
  <button type="button" class="btn btn-primary" id="createEgcOrder"><spring:message code="create_order_egc" /></button>
  <button type="button" class="btn btn-primary" id="createOrder"><spring:message code="create_order" /></button>
</div>

<div id="order_list_container">
</div>


<div class="modal hide  nofly" id="orderDialog" tabindex="-1" role="dialog" aria-hidden="true">
</div>

<jsp:include page="../../common/confirm.jsp" />

  <link href="<spring:url value="/css/dataTables.css" />" rel="stylesheet">
  <script src="<spring:url value="/js/jquery/jquery.dataTables.js" />" ></script>
  <script src="<spring:url value="/js/jquery/jquery.dataTables.linkLengthMenu.js" />"></script>
  <script src="<spring:url value="/js/jquery/jquery.dataTables.customPaging.js" />"></script>
  <script src="<spring:url value="/js/jquery/ajax-datatable.js" />" ></script>
  <script src="<spring:url value="/js/common/form2js.js" />" ></script>
  <script src="<spring:url value="/js/common/json2.js" />" ></script>
  <script src="<spring:url value="/js/jquery/jquery.toObject.js" />" ></script>
  <link href="<c:url value="/styles/temp.css" />" rel="stylesheet" />
  
  
  <script src="<spring:url value="/js/jquery/jquery.numberInput.js" />" ></script>
  <script src="<c:url value="/js/bootstrap/bootstrap-datepicker.js"/>" type='text/javascript'><![CDATA[&nbsp; ]]></script>
  <link href="<c:url value="/css/bootstrap/datepicker.css" />" rel="stylesheet" />
  <link href="<c:url value="/styles/temp.css" />" rel="stylesheet" />
  <script src="<spring:url value="/js/jquery/jquery.bindWithDelay.js" />"></script>
  <script src="<spring:url value="/js/jquery/jquery.ajaxQueue.js" />"></script>

<script>
$(document).ready(function() {
	
	$orderDialog = $("#orderDialog").modal({
		show: false
	}).on('hidden.bs.modal', function() {
		$(this).empty();
	});
	
	
	$("#createOrder").click(function() {
		$.get("<c:url value="/giftcard/order/form" />", function(data) {
			$orderDialog.modal("show").html(data);
		}, "html");
	});

    $("#pendingMOReceiptReport").click(function() {
        window.location.href = "<c:url value="/giftcard/order/pendingMOReceiptReport/" />";
    });

	$( "#createEgcOrder" ).click(function() { $.get( "<c:url value="/giftcard/egc/order/form" />", function( data ) { $orderDialog.modal("show").html(data); }, "html" ); });
	
	$("#order_list_container").ajaxDataTable({
		'autoload'  : true,
		'aaSorting' :[[ 1, 'desc' ]],
		'ajaxSource' : "<c:url value="/giftcard/order/search" />",
		'columnHeaders' : [
			"<spring:message code="order_mo_number"/>",
			{text: "", className : "hide"},
			"<spring:message code="order_mo_date"/>",
			"<spring:message code="order_po_number"/>",
			"<spring:message code="order_po_date"/>",
			"<spring:message code="order_supplier"/>",
			"<spring:message code="order_status"/>",
			""
		],
		'modelFields' : [
			{name : 'moNumber', sortable : true},
			{name : 'lastUpdated', sortable: true, aaClassname : "hide", dataType: "DATE"},
			{name : 'moDate', sortable : false, dataType: "DATE"},
			{name : 'poNumber', sortable : false},
			{name : 'poDate', sortable : false},
			{name : 'vendorName', sortable : false},
			{name : 'status', sortable : false, fieldCellRenderer : function (data, type, row) {
                return (row.status)? row.status.replace( new RegExp( "_", "g" ), " " ) : "";
            }},
			{customCell : function ( innerData, sSpecific, json ) {
				if (sSpecific == 'display') {
					var html = "";
					if(json.status == 'FOR_APPROVAL' || json.status == 'DRAFT' || json.status == 'REJECTED') {
						if(json.status != 'FOR_APPROVAL')
							html += "<button class=\"updateOrder btn btn-primary pull-left tiptip icn-edit\" data-id=\""+ json.id + "\" data-isegc=\"" + json.isEgc + "\" title=\"<spring:message code="label_update"/>\"></button>";
						<sec:authorize ifAnyGranted="ROLE_MERCHANT_SERVICE_SUPERVISOR">
						else
							html += "<button class=\"updateOrder btn btn-primary pull-left tiptip icn-edit\" data-id=\""+ json.id + "\" data-isegc=\"" + json.isEgc + "\" title=\"<spring:message code="label_process"/>\"></button>";
						</sec:authorize>
							
						html += "<button type=\"button\" class=\"tiptip btn pull-left icn-delete deleteOrder\" data-id=\""+json.id + "\" data-isegc=\"" + json.isEgc +"\" title=\"<spring:message code="label_delete"/>\"></button>";
					} else if(json.status != 'APPROVED' && json.status != 'BARCODING'){
						if(json.status != "FULL")
							html += "<button type=\"button\" data-id=\""+json.id + "\" data-isegc=\"" + json.isEgc +"\" class=\"tiptip btn pull-left icn-recieve receiveInventories\" title=\"" 
							  + ( json.isEgc? "<spring:message code='label_assign'/>" : "<spring:message code='label_receive'/>" ) + "\">" 
							  + ( json.isEgc? "<spring:message code='label_assign'/>" : "<spring:message code='label_receive'/>" ) + "</button>";
						if(json.status != "FULL" && json.status != "PARTIAL")
							html += "<button type=\"button\" data-id=\""+json.id + "\" data-isegc=\"" + json.isEgc +"\" class=\"tiptip btn pull-left icn-print printOrderEnc\" title=\"<spring:message code="label_mo_print"/>\"></button>";
						if(json.status == 'FULL' || json.status == 'PARTIAL')
							if ( !json.isEgc )
							  html += "<button type=\"button\" data-id=\""+json.id + "\" data-isegc=\"" + json.isEgc +"\" class=\"tiptip btn pull-left icn-print reprintReceipt\" title=\"<spring:message code="label_mo_printmoreceipt"/>\"></button>"
					} else if(json.status == 'APPROVED') {
						html += "<button class=\"generateOrder btn btn-primary pull-left tiptip icn-edit\" data-id=\""+json.id + "\" data-isegc=\"" + json.isEgc +"\" title=\"<spring:message code="generate_inventories"/>\"></button>";
					}
					// prepare button for rep-print transactions
					if(json.status !== 'DRAFT' && json.status !== 'FOR_APPROVAL' && json.status !== 'APPROVED' && json.status !== 'BARCODING' && json.status !== 'GENERATED'){ // hide reprint for status 'FOR_APPROVAL'
					  html += "<button type=\"button\" data-id=\""+json.id + "\" data-isegc=\"" + json.isEgc +"\" class=\"tiptip btn pull-left icn-print reprintTransaction\" title=\"<spring:message code="gc.mo.reprint.tx.label"/>\"></button>";
					}
					return html;
				} else {
					return "";
				}
			}
			}
		]
	}).on("click", ".updateOrder", function() {
		    $.get( ( $(this).data( "isegc" )? "<c:url value="/giftcard/egc/order/form/" />" : "<c:url value="/giftcard/order/form/" />" ) + $(this).data("id"), 
		    		function(data) { $orderDialog.modal("show").html(data); }, "html");
	}).on("click", ".receiveInventories", function() {
		    $.get( ( $(this).data( "isegc" )? "<c:url value="/giftcard/egc/order/form/assign/" />" : "<c:url value="/giftcard/order/receiveform/" />" ) + $(this).data("id"), 
		    		function(data) { $orderDialog.modal("show").html(data); }, "html");
	}).on("click", ".deleteOrder", function() {
		var itemId = $(this).data("id");
		getConfirm('<spring:message code="entity_delete_confirm" />', function(result) { 
			if(result) {
				$.post("<c:url value="/giftcard/order/" />" + itemId, {_method: 'DELETE'}, function(data) {
					location.reload();
				});
			}
		});
	}).on("click", ".printOrderEnc", function() {
		window.location.href = "<c:url value="/giftcard/order/printenc/" />" + $(this).data("id");
	}).on("click", ".reprintReceipt", function() {
		$.get("<c:url value="/giftcard/order/reprintreceipt/" />" + $(this).data("id"), function(data) {
			$orderDialog.modal("show").html(data);
		}, "html");
	}).on('click', '.reprintTransaction', function(){
            var url = "<c:url value="/giftcard/order/reprint/transaction/" />" + $(this).data("id");
		  window.open(url, '_blank', 'toolbar=0,location=0,menubar=0');
	}).on('click', '.generateOrder', function(){
		    $.get( ( $(this).data( "isegc" )? "<c:url value="/giftcard/egc/order/form/" />" : "<c:url value="/giftcard/order/form/" />" ) + $(this).data("id"), 
		    		function(data) { $orderDialog.modal("show").html(data); }, "html");
	});
	
	
	
	function initCommonFields(dialog) {
		$(".floatInput", $(dialog)).numberInput({ "type" : "float" });
		$(".intInput", $(dialog)).numberInput({ "type" : "int" });
	    $(".wholeInput", $(dialog)).numberInput({ "type" : "whole" });
	}
});
</script>