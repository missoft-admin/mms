package com.transretail.crm.rest.pos;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.springframework.http.MediaType.APPLICATION_JSON;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.io.File;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.Properties;

import org.joda.time.LocalDate;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TemporaryFolder;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.config.PropertyPlaceholderConfigurer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.AnnotationConfigWebContextLoader;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.transretail.crm.common.web.converter.CustomJsonMapper;
import com.transretail.crm.common.web.dto.LogFilterDto;
import com.transretail.crm.rest.store.LogRestController;
import com.transretail.crm.web.controller.AbstractMockMvcTest;

/**
 * @author Allan G. Ramirez (agramirez@exist.com)
 */
@RunWith(SpringJUnit4ClassRunner.class)
@WebAppConfiguration
@ContextConfiguration(loader = AnnotationConfigWebContextLoader.class)
public class LogRestControllerTest extends AbstractMockMvcTest {
    @Configuration
    @EnableWebMvc
    static class Config {
        @Bean
        public LogRestController logRestController() {
            return new LogRestController();
        }

        @Bean
        public PropertyPlaceholderConfigurer propertyPlaceholderConfigurer() {
            URL url = Thread.currentThread().getContextClassLoader().getResource("test-logs");
            File f = null;
            try {
                f = new File(url.toURI());
            } catch (URISyntaxException e) {
                f = new File(url.getPath());
            }
            PropertyPlaceholderConfigurer propertyPlaceholderConfigurer = new PropertyPlaceholderConfigurer();
            Properties properties = new Properties();
            properties.setProperty("crm.proxy.log.directory", f.getAbsolutePath());
            properties.setProperty("project.version", "1.2.0");
            propertyPlaceholderConfigurer.setProperties(properties);
            return propertyPlaceholderConfigurer;
        }

        @Bean
        public ObjectMapper objectMapper() {
            return new CustomJsonMapper();
        }
    }

    @Rule
    public TemporaryFolder tmpFolder = new TemporaryFolder();

    @Test
    public void searchCurrentDayLogsTest() throws Exception {
        LogFilterDto dto = new LogFilterDto();
        dto.setDate(LocalDate.now());
        dto.setFilter("[EARN][INIT]");
        ObjectMapper mapper = new CustomJsonMapper();
        String json = mapper.writeValueAsString(dto);
        String content = mockMvc.perform(post("/log/search").content(json).contentType(APPLICATION_JSON))
            .andExpect(status().isOk()).andReturn().getResponse().getContentAsString();
        String[] results = content.split("\r\n");
        assertEquals(6, results.length);
    }

    @Test
    public void searchPreviousDayLogsTest() throws Exception {
        LogFilterDto dto = new LogFilterDto();
        DateTimeFormatter df = DateTimeFormat.forPattern("yyyy-MM-dd");
        dto.setDate(df.parseLocalDate("2014-05-06"));
        dto.setFilter("[EARN][INIT]");
        ObjectMapper mapper = new CustomJsonMapper();
        String json = mapper.writeValueAsString(dto);
        String content = mockMvc.perform(post("/log/search").content(json).contentType(APPLICATION_JSON))
            .andExpect(status().isOk()).andReturn().getResponse().getContentAsString();
        String[] results = content.split("\r\n");
        assertEquals(13, results.length);
    }

    @Test
    public void logContainsTest() throws Exception {
        LogFilterDto dto = new LogFilterDto();
        dto.setDate(LocalDate.now());
        dto.setRegexFilter("^.*[EARN][INIT].*TXNNO=100360410002690.*$");
        ObjectMapper mapper = new CustomJsonMapper();
        String json = mapper.writeValueAsString(dto);
        String content = mockMvc.perform(post("/log/filters/found").content(json).contentType(APPLICATION_JSON))
            .andExpect(status().isOk()).andReturn().getResponse().getContentAsString();
        assertFalse(Boolean.valueOf(content));

        dto.setRegexFilter("^.*[EARN][INIT].*TXNNO=100360360010676.*$");
        json = mapper.writeValueAsString(dto);
        content = mockMvc.perform(post("/log/filters/found").content(json).contentType(APPLICATION_JSON))
            .andExpect(status().isOk()).andReturn().getResponse().getContentAsString();
        assertTrue(Boolean.valueOf(content));
    }
}
