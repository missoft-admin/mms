
drop table CRM_GC_INVENTORY cascade constraints;

create table CRM_GC_INVENTORY (
        id number(19,0) not null,
        CREATED_BY varchar2(255 char),
        CREATED_DATETIME timestamp,
        LAST_UPDATED_BY varchar2(255 char),
        LAST_UPDATED_DATETIME timestamp,
        ACTIVATION_DATE date,
        BALANCE double precision,
        BARCODE varchar2(20 char) not null,
        BATCH_NO number(10,0) not null,
        BATCH_REF_NO varchar2(255 char),
        CURR_VALUE number(12,4),
        EXPIRY_DATE date,
        FACE_VALUE number(12,4),
        ORDER_DATE date not null,
        PRODUCT_CODE varchar2(100 char) not null,
        PRODUCT_NAME varchar2(100 char) not null,
        SEQ_NO number(10,0) not null,
        SERIES number(19,0) not null,
        STATUS varchar2(8 char) not null,
        ALLOCATE_TO number(10,0),
        LOCATION number(10,0),
        PROFILE_ID number(19,0) not null,
        primary key (id)
    );

alter table CRM_GC_INVENTORY 
        add constraint UK_7xxguncy50cj4hqdcgb2d2sod unique (BARCODE);

    alter table CRM_GC_INVENTORY 
        add constraint UK_3dfjvi916kc3x84lkadua7j13 unique (SERIES);

alter table CRM_GC_INVENTORY 
        add constraint FK_evjesfydlcky8dsy55wwkiib6 
        foreign key (ALLOCATE_TO) 
        references STORE;

    alter table CRM_GC_INVENTORY 
        add constraint FK_7odx9nru2rdrle8k2mh8v61du 
        foreign key (LOCATION) 
        references STORE;

    alter table CRM_GC_INVENTORY 
        add constraint FK_ade12xow89h6uc5f1bvxm6o35 
        foreign key (PROFILE_ID) 
        references CRM_GC_PROD_PROFILE;