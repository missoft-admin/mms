package com.transretail.crm.core.dto;

public class ServiceDto {
	 
	private Enum returnCode;
	
	private Object returnObj;

	public Enum getReturnCode() {
		return returnCode;
	}

	public void setReturnCode(Enum returnCode) {
		this.returnCode = returnCode;
	}
	
	public Object getReturnObj() {
		return returnObj;
	}
	
	public void setReturnObj(Object returnObj) {
		this.returnObj = returnObj;
	}
}
