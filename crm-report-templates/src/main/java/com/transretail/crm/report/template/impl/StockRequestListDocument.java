package com.transretail.crm.report.template.impl;

import java.text.DecimalFormat;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.lang.StringUtils;
import org.joda.time.LocalDate;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.MessageSourceAware;
import org.springframework.context.support.MessageSourceAccessor;
import org.springframework.stereotype.Service;

import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Lists;
import com.transretail.crm.common.reporting.jasper.ClasspathFileResolver;
import com.transretail.crm.common.reporting.jasper.DefaultJRProcessor;
import com.transretail.crm.common.reporting.jasper.JRProcessor;
import com.transretail.crm.core.entity.enums.ReportType;
import com.transretail.crm.core.security.util.UserUtil;
import com.transretail.crm.core.service.CodePropertiesService;
import com.transretail.crm.giftcard.dto.StockRequestDto;
import com.transretail.crm.giftcard.dto.StockRequestSearchDto;
import com.transretail.crm.giftcard.entity.support.StockRequestStatus;
import com.transretail.crm.giftcard.service.StockRequestService;
import com.transretail.crm.report.template.FilterField;
import com.transretail.crm.report.template.NoDataFoundSupport;

/**
 * @author ftopico
 */

@Service("stockRequestListDocument")
public class StockRequestListDocument implements MessageSourceAware, NoDataFoundSupport {
    
    @Autowired
    private CodePropertiesService codePropertiesService;
    @Autowired
    private StockRequestService stockRequestService;

    protected static final String TEMPLATE_NAME = "Stock Request List Document";
    protected static final String REPORT_NAME = "reports/stock_request_list_document.jasper";
    //Header Params
    private static final String PARAM_PRINT_DATE = "printDate";
    private static final DateTimeFormatter FULL_DATE_PATTERN = DateTimeFormat.forPattern("MMMM d, yyyy");
    private MessageSourceAccessor messageSource;
    
    public ReportType[] getReportTypes() {
        return ReportType.values();
    }
    
    @Override
    public void setMessageSource(MessageSource messageSource) {
        this.messageSource = new MessageSourceAccessor(messageSource);
    }
    
    public String getName() {
        return TEMPLATE_NAME;
    }
    
    public String getDescription() {
        return messageSource.getMessage("title_document_stock_request_list");
    }
    
    public Set<FilterField> getFilters() {
        return null;
    }
    
    public JRProcessor createJrProcessor(String requestedBy, String requestNo, String status, String dateFrom, String dateTo) {

        StockRequestSearchDto stockRequestSearchDto = new StockRequestSearchDto();
        
        if (StringUtils.isNotBlank(requestedBy)) {
            stockRequestSearchDto.setRequestBy(requestedBy);
        }

        if (StringUtils.isNotBlank(requestNo)) {
            stockRequestSearchDto.setRequestNo(requestNo);
        }

        if (StringUtils.isNotBlank(status)) {
            stockRequestSearchDto.setSingleStatus(StockRequestStatus.valueOf(status));
        }
        DateTimeFormatter formatter = DateTimeFormat.forPattern("dd-MM-yyyy");
        if (StringUtils.isNotBlank(dateFrom)) {
            stockRequestSearchDto.setReqDateFrom(formatter.parseLocalDate(dateFrom));
        }
        if (StringUtils.isNotBlank(dateTo)) {
            stockRequestSearchDto.setReqDateTo(formatter.parseLocalDate(dateTo));
        }
    
    	List<StockRequestDto> resultList = stockRequestService.getStockRequestsForPrint(stockRequestSearchDto);
    	
    	Map<String, Object> parameters = ImmutableMap.<String, Object>builder()
    		.put(PARAM_PRINT_DATE, FULL_DATE_PATTERN.print(new LocalDate())).build();
    	
    	List<ReportBean> beansDataSource = Lists.newArrayList();
    	DecimalFormat df = new DecimalFormat("#,##0");
    	
    	for (StockRequestDto stockRequestDto : resultList) {
    	    String statusReport = null;
    	            
            if (codePropertiesService.getDetailInvLocationHeadOffice()
                    .equals(UserUtil.getCurrentUser().getInventoryLocation())) {
                statusReport = stockRequestDto.getStatusForHO().name();
            }
            else { 
                statusReport = stockRequestDto.getStatus().name();
            }
                
        	beansDataSource.add(new ReportBean(stockRequestDto.getRequestNo(),
        	                                    stockRequestDto.getTransferRefNo(),
        	                                    FULL_DATE_PATTERN.print(stockRequestDto.getRequestDate()),
        	                                    stockRequestDto.getSourceLocationDescription(),
        	                                    stockRequestDto.getAllocateToDescription(),
        	                                    statusReport,
        	                                    stockRequestDto.getProductCode() + " - " + stockRequestDto.getProductDescription(),
        	                                    df.format(stockRequestDto.getQuantity()),
        	                                    stockRequestDto.getCreateUser(),
        	                                    FULL_DATE_PATTERN.print(stockRequestDto.getLastUpdated())));
        }
    	
    	DefaultJRProcessor jrProcessor = new DefaultJRProcessor(REPORT_NAME, beansDataSource);
    	jrProcessor.addParameters(parameters);
    	jrProcessor.setFileResolver(new ClasspathFileResolver("reports"));
    	
    	return jrProcessor;
	
    }
    
    public static class ReportBean {
        
        private String requestNo;
        private String transferRefNo;
        private String requestDate;
        private String sourceLocation;
        private String allocateTo;
        private String status;
        private String product;
        private String quantity;
        private String createdBy;
        private String lastUpdated;
        
        public ReportBean() {}
        
        public ReportBean(String requestNo, 
                            String transferRefNo,
                            String requestDate,
                            String sourceLocation,
                            String allocateTo,
                            String status,
                            String product,
                            String quantity,
                            String createdBy,
                            String lastUpdated) {
            super();
            this.requestNo = requestNo;
            this.transferRefNo = transferRefNo;
            this.requestDate = requestDate;
            this.sourceLocation = sourceLocation;
            this.allocateTo = allocateTo;
            this.status = status;
            this.product = product;
            this.quantity = quantity;
            this.createdBy = createdBy;
            this.lastUpdated = lastUpdated;
        }

        public String getRequestNo() {
            return requestNo;
        }

        public void setRequestNo(String requestNo) {
            this.requestNo = requestNo;
        }

        public String getTransferRefNo() {
            return transferRefNo;
        }

        public void setTransferRefNo(String transferRefNo) {
            this.transferRefNo = transferRefNo;
        }

        public String getRequestDate() {
            return requestDate;
        }

        public void setRequestDate(String requestDate) {
            this.requestDate = requestDate;
        }

        public String getSourceLocation() {
            return sourceLocation;
        }

        public void setSourceLocation(String sourceLocation) {
            this.sourceLocation = sourceLocation;
        }

        public String getAllocateTo() {
            return allocateTo;
        }

        public void setAllocateTo(String allocateTo) {
            this.allocateTo = allocateTo;
        }

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }

        public String getProduct() {
            return product;
        }

        public void setProduct(String product) {
            this.product = product;
        }

        public String getQuantity() {
            return quantity;
        }

        public void setQuantity(String quantity) {
            this.quantity = quantity;
        }

        public String getCreatedBy() {
            return createdBy;
        }

        public void setCreatedBy(String createdBy) {
            this.createdBy = createdBy;
        }

        public String getLastUpdated() {
            return lastUpdated;
        }

        public void setLastUpdated(String lastUpdated) {
            this.lastUpdated = lastUpdated;
        }

    }

    @Override
    public boolean isEmpty(Map<String, String> filter) {
        String requestedBy = filter.get("requestedBy");
        String requestNo = filter.get("requestNo");
        String status = filter.get("status");
        String dateFrom = filter.get("dateFrom");
        String dateTo = filter.get("dateTo");
        
        StockRequestSearchDto stockRequestSearchDto = new StockRequestSearchDto();
        
        if (StringUtils.isNotBlank(requestedBy)) {
            stockRequestSearchDto.setRequestBy(requestedBy);
        }

        if (StringUtils.isNotBlank(requestNo)) {
            stockRequestSearchDto.setRequestNo(requestNo);
        }

        if (StringUtils.isNotBlank(status)) {
            stockRequestSearchDto.setSingleStatus(StockRequestStatus.valueOf(status));
        }
        DateTimeFormatter formatter = DateTimeFormat.forPattern("dd-MM-yyyy");
        if (StringUtils.isNotBlank(dateFrom)) {
            stockRequestSearchDto.setReqDateFrom(formatter.parseLocalDate(dateFrom));
        }
        if (StringUtils.isNotBlank(dateTo)) {
            stockRequestSearchDto.setReqDateTo(formatter.parseLocalDate(dateTo));
        }
    
        List<StockRequestDto> resultList = stockRequestService.getStockRequestsForPrint(stockRequestSearchDto);
        
        return resultList.isEmpty();
    }
}
