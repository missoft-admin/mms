package com.transretail.crm.web.controller;

import static org.mockito.Matchers.any;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.reset;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.model;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.view;

import java.util.Locale;

import org.hamcrest.core.IsInstanceOf;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.TestingAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.context.SecurityContextImpl;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.AnnotationConfigWebContextLoader;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

import com.transretail.crm.core.security.model.impl.CustomSecurityUserDetailsImpl;
import com.transretail.crm.core.service.CodePropertiesService;
import com.transretail.crm.core.service.LookupService;
import com.transretail.crm.core.service.StoreService;
import com.transretail.crm.core.service.UserRoleModelService;
import com.transretail.crm.core.service.UserRolePermissionService;
import com.transretail.crm.core.service.UserService;
import com.transretail.crm.web.dto.ChangePasswordDto;

/**
 * @author Allan G. Ramirez (agramirez@exist.com)
 */
@RunWith(SpringJUnit4ClassRunner.class)
@WebAppConfiguration
@ContextConfiguration(loader = AnnotationConfigWebContextLoader.class)
public class UserControllerTest extends AbstractMockMvcTest {
    private static final String CURRENT_USERNAME = "cloudx";
    private static final Long USER_ID = 1L;

    @Configuration
    @EnableWebMvc
    static class Config {
        @Bean
        public UserController userController() {
            return new UserController();
        }

        @Bean
        public UserService userService() {
            return mock(UserService.class);
        }

        @Bean
        public UserRoleModelService userRoleModelService() {
            return mock(UserRoleModelService.class);
        }

        @Bean
        public StoreService storeService() {
            return mock(StoreService.class);
        }

        @Bean
        public MessageSource messageSource() {
            return mock(MessageSource.class);
        }

        @Bean
        public LookupService lookupService() {
            return mock(LookupService.class);
        }

        @Bean
        public CodePropertiesService codePropertiesService() {
            return mock(CodePropertiesService.class);
        }

        @Bean
        public UserRolePermissionService userRolePermissionService() {
            return mock(UserRolePermissionService.class);
        }
    }

    @Autowired
    private UserService userService;
    @Autowired
    private MessageSource messageSource;

    @BeforeClass
    public static void setupCurrentUser() {
        CustomSecurityUserDetailsImpl user = new CustomSecurityUserDetailsImpl();
        user.setUserId(USER_ID);
        user.setUsername(CURRENT_USERNAME);
        user.setPassword("user");

        SecurityContextImpl securityContext = new SecurityContextImpl();
        securityContext.setAuthentication(new TestingAuthenticationToken(user, ""));
        SecurityContextHolder.setContext(securityContext);
    }

    @AfterClass
    public static void removeCurrentUser() {
        SecurityContextHolder.clearContext();
    }

    @Override
    protected void doSetup() throws Exception {
        reset(userService);
        reset(messageSource);
    }

    @Test
    public void showPasswordChangePageTest() throws Exception {
        mockMvc.perform(get("/user/profile/password/change"))
            .andExpect(model().attribute(UserController.FORM_ATTR_NAME, new IsInstanceOf(ChangePasswordDto.class)))
            .andExpect(view().name("/user/profile/changePassword"))
            .andExpect(status().isOk());
    }

    @Test
    public void savePasswordChangeWithErrorTest() throws Exception {
        ChangePasswordDto dto = new ChangePasswordDto();
        mockMvc.perform(
            setParams(post("/user/profile/password/change"), dto))
            .andExpect(status().isOk())
            .andExpect(model().hasErrors())
            .andExpect(model().errorCount(3))
            .andExpect(model().attributeHasFieldErrors(UserController.FORM_ATTR_NAME, "currentPassword", "newPassword", "confirmPassword"))
            .andExpect(view().name("/user/profile/changePassword"));

        dto.setNewPassword("password");
        mockMvc.perform(
            setParams(post("/user/profile/password/change"), dto))
            .andExpect(status().isOk())
            .andExpect(model().hasErrors())
            .andExpect(model().errorCount(2))
            .andExpect(model().attributeHasFieldErrors(UserController.FORM_ATTR_NAME, "currentPassword", "confirmPassword"))
            .andExpect(view().name("/user/profile/changePassword"));

        dto.setNewPassword("password");
        dto.setConfirmPassword("test");
        mockMvc.perform(
            setParams(post("/user/profile/password/change"), dto))
            .andExpect(status().isOk())
            .andExpect(model().hasErrors())
            .andExpect(model().errorCount(2))
            .andExpect(model().attributeHasFieldErrors(UserController.FORM_ATTR_NAME, "password", "currentPassword"))
            .andExpect(view().name("/user/profile/changePassword"));
        
        dto.setConfirmPassword("password");
        mockMvc.perform(
                setParams(post("/user/profile/password/change"), dto))
                .andExpect(status().isOk())
                .andExpect(model().hasErrors())
                .andExpect(model().errorCount(1))
                .andExpect(model().attributeHasFieldErrors(UserController.FORM_ATTR_NAME, "currentPassword"))
                .andExpect(view().name("/user/profile/changePassword"));
        
        dto.setCurrentPassword("password");
        mockMvc.perform(
                setParams(post("/user/profile/password/change"), dto))
                .andExpect(status().isOk())
                .andExpect(model().hasErrors())
                .andExpect(model().errorCount(1))
                .andExpect(view().name("/user/profile/changePassword"));
    }

    @Test
    public void savePasswordChangeSuccessTest() throws Exception {
        ChangePasswordDto dto = new ChangePasswordDto();
        dto.setNewPassword("password");
        dto.setConfirmPassword("password");
        dto.setCurrentPassword("user");

        when(userService.currentPasswordMatch(dto.getCurrentPassword())).thenReturn(true);
        mockMvc.perform(
            setParams(post("/user/profile/password/change"), dto))
            .andExpect(model().hasNoErrors())
            .andExpect(view().name("redirect:/user/profile/password/change"));

        verify(userService).changePassword(CURRENT_USERNAME, dto.getPassword());
        verify(messageSource).getMessage(eq("user_msg_passwordchange_success"), any(String[].class), any(Locale.class));
    }
}
