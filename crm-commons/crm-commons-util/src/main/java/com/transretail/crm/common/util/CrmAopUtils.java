package com.transretail.crm.common.util;

import org.springframework.aop.framework.Advised;
import org.springframework.aop.support.AopUtils;

/**
 * @author Allan G. Ramirez (agramirez@exist.com)
 */
public enum CrmAopUtils {
    INSTANCE;

    public <T> T getRealObject(Object object, Class<T> targetClass) throws Exception {
        T realObject = null;
        if (AopUtils.isAopProxy(object) && object instanceof Advised) {
            realObject = (T) ((Advised) object).getTargetSource().getTarget();

        } else {
            realObject = (T) object;
        }
        return realObject;
    }
}
