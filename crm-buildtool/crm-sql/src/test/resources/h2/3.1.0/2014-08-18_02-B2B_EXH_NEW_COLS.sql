alter table CRM_B2B_EXH add (
TAX double,
TAXABLE_AMOUNT decimal(19,2),
TOTAL_PAYMENT decimal(19,2)
);