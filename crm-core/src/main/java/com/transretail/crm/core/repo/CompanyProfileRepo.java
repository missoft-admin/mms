package com.transretail.crm.core.repo;

import org.springframework.stereotype.Repository;

import com.transretail.crm.common.repo.CrmQueryDslPredicateExecutor;
import com.transretail.crm.core.entity.CompanyProfile;

@Repository
public interface CompanyProfileRepo extends CrmQueryDslPredicateExecutor<CompanyProfile, String> {

}
