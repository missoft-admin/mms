Date.prototype.customize= function( pattern ) {
	customizeMonth = function ( mth ) {
		switch(mth) {
		case(0): return "January";
		case(1): return "February";
		case(2): return "March";
		case(3): return "April";
		case(4): return "May";
		case(5): return "June";
		case(6): return "July";
		case(7): return "August";
		case(8): return "September";
		case(9): return "October";
		case(10): return "November";
		case(11): return "December";
		}
	};
	customizeDigit = function( dgt ) {
		if( dgt != null && dgt != undefined && ( dgt + "" ).length < 2 ) {
			return "0" + dgt;
		}
		return dgt;
	};
	customizeDay = function( day ) {
		return ( day + ( (day > 20 || day < 10) ? ([false, "st", "nd", "rd"])[(day%10)] || "th" : "th" ) );
	};

	if ( this ) {
		switch( pattern ) {
			case(0)	: return customizeDay( this.getDate() ) 
				+ " " + customizeMonth( this.getMonth() ) 
				+ " " + this.getFullYear() 
				+ " " + customizeDigit( this.getHours() ) 
				+ ":" + customizeDigit( this.getMinutes() ) 
				+ ":" + customizeDigit( this.getSeconds() );
			case(1)	: return customizeDay( this.getDate() ) 
				+ " " + customizeMonth( this.getMonth() ) 
				+ " " + this.getFullYear();
			default : return this;
		}
		
	}
};