package com.transretail.crm.core.service.impl;

import java.util.ArrayList;
import java.util.List;

import junit.framework.Assert;

import org.junit.After;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Configurable;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import com.transretail.crm.core.entity.Campaign;
import com.transretail.crm.core.entity.Program;
import com.transretail.crm.core.entity.Promotion;
import com.transretail.crm.core.entity.TransactionAudit;
import com.transretail.crm.core.repo.CampaignRepo;
import com.transretail.crm.core.repo.MemberRepo;
import com.transretail.crm.core.repo.ProgramRepo;
import com.transretail.crm.core.repo.PromotionRepo;
import com.transretail.crm.core.repo.TransactionAuditRepo;
import com.transretail.crm.core.service.TransactionAuditService;
import com.transretail.crm.core.util.generator.IdGeneratorService;

@Configurable
@Transactional
@ContextConfiguration(locations = "classpath*:/META-INF/spring/applicationContext*.xml")
@RunWith(SpringJUnit4ClassRunner.class)
public class TransactionAuditServiceTest {
	
	@Autowired
	ProgramRepo programRepo;
	
	@Autowired
	TransactionAuditService transactionAuditService;
	
	@Autowired
	TransactionAuditRepo transactionAuditRepo;
	
	private Promotion promotion;
	
	@Before
	public void setup() {
		promotion = new Promotion();
		promotion.setName("promotion1");
		List<Promotion> promotions = new ArrayList<Promotion>();
		promotions.add(promotion);
		
		Campaign campaign = new Campaign();
		campaign.setName("campaign 1");
		campaign.setPromotions(promotions);
		List<Campaign> campaigns = new ArrayList<Campaign>();
		campaigns.add(campaign);
		
		Program program = new Program();
		program.setCampaigns(campaigns);
		program.setName("program1");
		
		programRepo.save(program);
		
	}
	
	@After
	public void teardown() {
		programRepo.deleteAll();
	}
	
	@Test
	public void testSaveTransactionAudits() {
		Assert.assertEquals(0, transactionAuditRepo.count());
		
		List<TransactionAudit> audits = new ArrayList<TransactionAudit>();
		
		TransactionAudit audit = new TransactionAudit();
		audit.setPromotion(promotion);
		audit.setTransactionNo("001");
		audit.setTransactionPoints(1000L);
		audit.setId("ID1");
		audits.add(audit);
		
		audit = new TransactionAudit();
		audit.setPromotion(promotion);
		audit.setTransactionNo("002");
		audit.setTransactionPoints(2000L);
		audit.setId("ID2");
		audits.add(audit);
		
		transactionAuditService.saveTransactionAudits(audits);
		
		Assert.assertEquals(2, transactionAuditRepo.count());
		Assert.assertNotNull(transactionAuditRepo.findAll());
	}

}
