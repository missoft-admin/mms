delete from CRM_FOR_PEOPLESOFT_CONFIG  where txn_type in ('REDEMPTION_OF_TGI', 'CLAIM_TO_TGI');

update CRM_FOR_PEOPLESOFT_CONFIG set txn_type='AFFILIATE_REDEMPTION' where txn_type in ('REDEMPTION_OF_ARI', 'REDEMPTION_OF_TGI');
update CRM_FOR_PEOPLESOFT_CONFIG set txn_type='AFFILIATE_CLAIM' where txn_type in ('CLAIM_TO_ARI', 'CLAIM_TO_TGI');

create table CRM_BU_PSOFT_CONFIG (
    id number(19,0) not null,
    CREATED_BY varchar2(255 char),
    CREATED_DATETIME timestamp,
    LAST_UPDATED_BY varchar2(255 char),
    LAST_UPDATED_DATETIME timestamp,
    BU varchar2(15 char),
    PSOFT_CONFIG_ID number(19,0),
    primary key (id)
);

alter table CRM_BU_PSOFT_CONFIG 
    add constraint UK_bjtefqvr5m97aif3lndar4mr3 unique (BU, PSOFT_CONFIG_ID);

alter table CRM_BU_PSOFT_CONFIG 
    add constraint FK_39tqgs7d4e9175a32ccuaphrb 
    foreign key (BU) 
    references CRM_REF_LOOKUP_DTL;

alter table CRM_BU_PSOFT_CONFIG 
    add constraint FK_gib8npbpjun2ogh9xb7r3mpye 
    foreign key (PSOFT_CONFIG_ID) 
    references CRM_FOR_PEOPLESOFT_CONFIG;