create table CRM_GC_PCS_CARD_STATUS (
    GC_INV_ID number(19,0),
    PCS_ID number(19,0),
    STATUS varchar2(7 char) not null,
    primary key (GC_INV_ID, PCS_ID)
);

alter table CRM_GC_PCS_CARD_STATUS 
    add constraint FK_dh6jmu91p6xnn7slgav5qoeli 
    foreign key (GC_INV_ID) 
    references CRM_GC_INVENTORY;

alter table CRM_GC_PCS_CARD_STATUS 
    add constraint FK_jervwe7mx7x2ymtrcf09x7uqb 
    foreign key (PCS_ID) 
    references CRM_GC_PHYSICAL_COUNT_SUMMARY;

-- DROP 
-- drop table CRM_GC_PCS_CARD_STATUS cascade constraints;