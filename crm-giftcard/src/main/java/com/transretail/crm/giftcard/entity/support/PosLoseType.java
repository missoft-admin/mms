package com.transretail.crm.giftcard.entity.support;

public enum PosLoseType {

    ACTIVATION,
    VOID_ACTIVATION,
    REDEMPTION,
    VOID_REDEMPTION
}
