package com.transretail.crm.giftcard.repo.custom;

/**
 * @author ftopico
 */
public interface GiftCardExpiryExtensionRepoCustom {
    String createExtendNo();
}
