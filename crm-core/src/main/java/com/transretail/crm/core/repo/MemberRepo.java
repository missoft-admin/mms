package com.transretail.crm.core.repo;

import java.util.List;

import com.transretail.crm.core.entity.lookup.LookupDetail;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.transretail.crm.common.repo.CrmQueryDslPredicateExecutor;
import com.transretail.crm.core.entity.MemberModel;
import com.transretail.crm.core.repo.custom.MemberRepoCustom;

@Repository
public interface MemberRepo extends CrmQueryDslPredicateExecutor<MemberModel, Long>, MemberRepoCustom {

    MemberModel findByContact(String inContact);

    List<MemberModel> findByEmail(String inEmail);

    MemberModel findByUsername(String inUsername);

    List<MemberModel> findByCompanyName(String inCompanyName);

    List<MemberModel> findByMemberType(LookupDetail type);

    MemberModel findByLastNameAndFirstName(String inLastName, String inFirstName);

    MemberModel findByAccountId(String id);



    /**
     * Updates the memberType and cardType of MemberModel that matches the corresponding accountId.
     * <br/>
     * Note that we used accountId as the descriminator because it's unique and it has more importance than username.
     * Also note that entitymanager might contain outdated entities after execution of this method. It's up to you if you'll flush and then clear the entitymanager.
     *
     * @param cardTypeCode cardtypecode to set
     * @return total affected rows
     * @see <a href="http://docs.spring.io/spring-data/data-jpa/docs/current/reference/html/jpa.repositories.html#jpa.modifying-queries">Modifying queries</a>
     */
    @Modifying
    @Query("UPDATE MemberModel m SET m.cardType.code = ?1 WHERE m.accountId = ?2")
    int updateCardType(String cardTypeCode, String accountId);
}
