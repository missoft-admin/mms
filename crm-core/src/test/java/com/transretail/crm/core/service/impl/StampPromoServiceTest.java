package com.transretail.crm.core.service.impl;


import java.math.BigDecimal;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Set;

import org.apache.commons.lang3.time.DateUtils;
import org.hamcrest.CoreMatchers;
import org.joda.time.LocalDate;
import org.joda.time.LocalTime;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Configurable;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import ch.qos.logback.classic.Level;
import ch.qos.logback.classic.Logger;
import ch.qos.logback.classic.spi.ILoggingEvent;
import ch.qos.logback.classic.spi.LoggingEvent;
import ch.qos.logback.core.Appender;

import com.google.common.collect.Lists;
import com.google.common.collect.Sets;
import com.transretail.crm.core.dto.StampPromoTxnDto;
import com.transretail.crm.core.entity.ApplicationConfig;
import com.transretail.crm.core.entity.Campaign;
import com.transretail.crm.core.entity.MemberModel;
import com.transretail.crm.core.entity.Program;
import com.transretail.crm.core.entity.Promotion;
import com.transretail.crm.core.entity.PromotionRedemption;
import com.transretail.crm.core.entity.PromotionReward;
import com.transretail.crm.core.entity.StampTxnDetail;
import com.transretail.crm.core.entity.StampTxnHeader;
import com.transretail.crm.core.entity.StampTxnModel;
import com.transretail.crm.core.entity.embeddable.CustomerProfile;
import com.transretail.crm.core.entity.embeddable.Duration;
import com.transretail.crm.core.entity.enums.AppKey;
import com.transretail.crm.core.entity.enums.StampTxnType;
import com.transretail.crm.core.entity.enums.Status;
import com.transretail.crm.core.entity.lookup.LookupDetail;
import com.transretail.crm.core.entity.lookup.LookupHeader;
import com.transretail.crm.core.entity.lookup.Product;
import com.transretail.crm.core.entity.lookup.Store;
import com.transretail.crm.core.repo.ApplicationConfigRepo;
import com.transretail.crm.core.repo.CampaignRepo;
import com.transretail.crm.core.repo.LookupDetailRepo;
import com.transretail.crm.core.repo.LookupHeaderRepo;
import com.transretail.crm.core.repo.MemberRepo;
import com.transretail.crm.core.repo.ProductRepo;
import com.transretail.crm.core.repo.ProgramRepo;
import com.transretail.crm.core.repo.PromotionRepo;
import com.transretail.crm.core.repo.StampTxnDetailRepo;
import com.transretail.crm.core.repo.StampTxnHeaderRepo;
import com.transretail.crm.core.repo.StampTxnModelRepo;
import com.transretail.crm.core.repo.StoreRepo;
import com.transretail.crm.core.service.CodePropertiesService;
import com.transretail.crm.core.service.StampPromoService;


@Configurable
@Transactional
@ContextConfiguration(locations = "classpath*:/META-INF/spring/applicationContext*.xml")
@RunWith(SpringJUnit4ClassRunner.class)
public class StampPromoServiceTest {

    @Autowired
    private StampPromoService service;
    @Autowired
    private CodePropertiesService codePropertiesService;

    @Autowired
    private ApplicationConfigRepo applicationConfigRepo;
    @Autowired
    private LookupHeaderRepo lookupHeaderRepo;
    @Autowired
    private LookupDetailRepo lookupDetailRepo;
    @Autowired
    private StoreRepo storeRepo;
    @Autowired
    private ProductRepo productRepo;
    @Autowired
    private MemberRepo memberRepo;

    @Autowired
    private ProgramRepo programRepo;
    @Autowired
    private CampaignRepo campaignRepo;
    @Autowired
    private PromotionRepo promotionRepo;

    @Autowired
    private StampTxnHeaderRepo stampTxnHeaderRepo;
    @Autowired
    private StampTxnDetailRepo stampTxnDetailRepo;
    @Autowired
    private StampTxnModelRepo repo;

//	@Mock
//    private Appender<ILoggingEvent> mockAppender;
//    @Captor
//    private ArgumentCaptor<LoggingEvent> captorLoggingEvent;

    private static final String STORE_CODE = "20001";
    private static final String ACCOUNT_ID = "1";
    private Promotion stampPromo = null;



	@Before
    public void setup() {
    	setupStore();
    	setupMember();
    	setupPromotion();

    	double stampsPerMedia = 2D;
		StampTxnHeader stampTxnHeader = new StampTxnHeader();
		stampTxnHeader.setTransactionNo( STORE_CODE );
		stampTxnHeader.setStoreCode( STORE_CODE );
		stampTxnHeader.setTransactionDateTime( new Date() );
		stampTxnHeader.setTotalTxnStamps( stampsPerMedia );
		stampTxnHeader.setMemberModel( memberRepo.findByAccountId( ACCOUNT_ID ) );
		stampTxnHeaderRepo.saveAndFlush( stampTxnHeader );

		StampTxnDetail stampTxnDetail = new StampTxnDetail();
		stampTxnDetail.setTransactionAmount( 1000D );
		stampTxnDetail.setStampsPerMedia( stampsPerMedia );
		stampTxnDetail.setStampTxnHeader( stampTxnHeader );
		stampTxnDetailRepo.saveAndFlush( stampTxnDetail );

        stampTxnHeader.getTransactionDetails().add( stampTxnDetail );
		stampTxnHeaderRepo.saveAndFlush( stampTxnHeader );

        StampTxnModel stamp = new StampTxnModel();
        stamp.setTransactionHeader( stampTxnHeader );
        stamp.setTransactionType( StampTxnType.EARN );
        stamp.setStampPromotion( stampPromo );
        repo.saveAndFlush( stamp );

//        MockitoAnnotations.initMocks(this);
//        Mockito.mock( StampPromoServiceImpl.class );
//        final Logger logger = (Logger) LoggerFactory.getLogger( Logger.ROOT_LOGGER_NAME );
//        logger.addAppender( mockAppender );
    }

	@After
    public void tearDown() {
        repo.deleteAll();
        stampTxnDetailRepo.deleteAll();
        stampTxnHeaderRepo.deleteAll();
        promotionRepo.deleteAll();
        campaignRepo.deleteAll();
        programRepo.deleteAll();
        memberRepo.deleteAll();
        productRepo.deleteAll();
        storeRepo.deleteAll();
        lookupDetailRepo.deleteAll();
        lookupHeaderRepo.deleteAll();
    	applicationConfigRepo.deleteAll();

//        final Logger logger = (Logger) LoggerFactory.getLogger( Logger.ROOT_LOGGER_NAME );
//        logger.detachAppender(mockAppender);
    }



    @Test
    public void testCallPortaCreateStampPromo() {
    	ApplicationConfig config = new ApplicationConfig( AppKey.PORTA_URL_STAMP_CREATE, null );
    	applicationConfigRepo.saveAndFlush( config );
    	Assert.assertFalse( service.callPortaCreateStampPromo( stampPromo ) );

//        Mockito.verify(mockAppender, Mockito.times(3)).doAppend(captorLoggingEvent.capture());//Now verify our logging interactions
//        final LoggingEvent loggingEvent = captorLoggingEvent.getValue();//Having a genricised captor means we don't need to cast
//        //Assert.assertThat(loggingEvent.getLevel(), CoreMatchers.is(Level.INFO));
//        Assert.assertThat(loggingEvent.getFormattedMessage(), CoreMatchers.is("String a:foo, String b:bar"));
    }

    @Test
    public void testRetrieveRedeemableItems() {
    	List<StampPromoTxnDto> stamps = service.retrieveRedeemableItems( ACCOUNT_ID );
    	Assert.assertEquals( 0, stamps.size() );
    }



    private void setupStore() {
    	Store store = new Store( Integer.parseInt( STORE_CODE ) );
    	store.setCode( STORE_CODE );
    	storeRepo.saveAndFlush( store );
    }

    private void setupMember() {
    	MemberModel member = new MemberModel();
    	member.setAccountId( "test" );
    	member.setEnabled( true );
    	member.setAccountId( ACCOUNT_ID );
    	member.setContact( "0000000000000" );
    	member.setPin( "1234" );
        member.setTotalPoints( 100d );
    	member.setUsername( "username" );
    	CustomerProfile customerProfile = new CustomerProfile();
        customerProfile.setBirthdate( new Date() );
    	member.setCustomerProfile( customerProfile );
    	memberRepo.save( member );
    }

    private void setupPromotion() {
        LookupHeader luHdr = new LookupHeader( codePropertiesService.getHeaderStatus() );
        lookupHeaderRepo.saveAndFlush( luHdr );
        luHdr = new LookupHeader( codePropertiesService.getHeaderRewardType() );
        lookupHeaderRepo.saveAndFlush( luHdr );
        luHdr = new LookupHeader( codePropertiesService.getHeaderPaymentType() );
        lookupHeaderRepo.saveAndFlush( luHdr );

        //status
        LookupDetail luDtl = new LookupDetail( codePropertiesService.getDetailStatusActive(), "STATUS ACTIVE", 
        		lookupHeaderRepo.findOne( codePropertiesService.getHeaderStatus() ), Status.ACTIVE );
        lookupDetailRepo.saveAndFlush( luDtl );
        
        //reward type
        luDtl = new LookupDetail( codePropertiesService.getDetailRewardTypeStamp(), "STAMP", 
        		lookupHeaderRepo.findOne( codePropertiesService.getHeaderRewardType() ), Status.ACTIVE );
        lookupDetailRepo.saveAndFlush( luDtl );

        //payment type
        luDtl = new LookupDetail( codePropertiesService.getDetailPaymentTypeCash(), "PAYMENTTYPE CASH", 
        		lookupHeaderRepo.findOne( codePropertiesService.getHeaderPaymentType() ), Status.ACTIVE );
        lookupDetailRepo.saveAndFlush( luDtl) ;

    	Duration currentMonth = new Duration();
        currentMonth.setStartDate( DateUtils.truncate( new Date(), Calendar.MONTH ) );
        currentMonth.setEndDate( DateUtils.ceiling( new Date(), Calendar.MONTH ) );

        Program program = new Program();
        program.setName( "Program" );
        program.setDescription( "Program1 Description" );
        program.setDuration( currentMonth );
        program.setStatus( lookupDetailRepo.findByCode( codePropertiesService.getDetailStatusActive() ) );
        programRepo.save( program );

        Campaign campaign = new Campaign();
        campaign.setProgram( program );
        campaign.setDuration( currentMonth );
        campaign.setName( "Program-Campaign" );
        program.setStatus( lookupDetailRepo.findByCode( codePropertiesService.getDetailStatusActive() ) );
        campaignRepo.save( campaign );

        Promotion promotion = new Promotion();
        promotion.setName( "Promotion" );
        promotion.setStatus(lookupDetailRepo.findByCode(codePropertiesService.getDetailStatusActive()));
        promotion.setCampaign(campaign);
        StringBuffer affectedDays = new StringBuffer(codePropertiesService.getDetailDayMonday())
        	.append(",").append(codePropertiesService.getDetailDayTuesday())
        	.append(",").append(codePropertiesService.getDetailDayWednesday())
        	.append(",").append(codePropertiesService.getDetailDayThursday())
        	.append(",").append(codePropertiesService.getDetailDayFriday())
        	.append(",").append(codePropertiesService.getDetailDaySaturday())
        	.append(",").append(codePropertiesService.getDetailDaySunday());
        promotion.setAffectedDays(affectedDays.toString());
        promotion.setStartDate(currentMonth.getStartDate());
        promotion.setEndDate(currentMonth.getEndDate());
        promotion.setRewardType(lookupDetailRepo.findByCode(codePropertiesService.getDetailRewardTypeStamp()));
        promotion.setStartTime(LocalTime.MIDNIGHT);
        promotion.setEndTime(LocalTime.MIDNIGHT.minusMillis(1));

        PromotionReward reward = new PromotionReward();
        reward.setFixed( true );
        reward.setBaseFactor( 1D );
        reward.setMinAmount( new BigDecimal(200) );
        reward.setMaxAmount( new BigDecimal(1000) );
        List<PromotionReward> rewards = Lists.newArrayList();
        rewards.add( reward );
        promotion.setPromotionRewards( rewards );

        Product prod = new Product();
        prod.setId( "PRODUCT" );
        prod.setSku( "PRODUCT" );
        prod.setDescription( "PRODUCT" );
        productRepo.saveAndFlush( prod );
        PromotionRedemption redemption = new PromotionRedemption();
        redemption.setQty( 1L );
        redemption.setRedeemFrom( LocalDate.fromDateFields( currentMonth.getStartDate() ) );
        redemption.setRedeemTo( LocalDate.fromDateFields( currentMonth.getEndDate() ) );
        redemption.setSku( "PRODUCT" );
        Set<PromotionRedemption> redemptions = Sets.newHashSet();
        redemptions.add( redemption );
        promotion.setPromotionRedemptions( redemptions );

        stampPromo = promotionRepo.save(promotion);
    }

}
