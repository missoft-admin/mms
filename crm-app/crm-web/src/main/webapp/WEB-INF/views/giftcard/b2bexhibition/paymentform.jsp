<%@ include file="../../common/taglibs.jsp" %>
<%@include file="../../common/messages.jsp" %>


<div class="modal hide nofly" id="paymentDialog" tabindex="-1" role="dialog" aria-hidden="true">
<div class="modal-dialog">
  <div class="modal-content">
    
    <div class="modal-header">
      <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
      <h4><spring:message code="b2b.exhibition.addpayments" /></h4>
    
    </div>
    
    <div class="modal-body">
    
      <div id="contentError" class="hide alert alert-error">
        <button type="button" class="close" onclick="$(this).parent().hide();">&times;</button>
        <div><form:errors path="*"/></div>
      </div>
    
      <form name="paymentForm" action="<c:url value="/giftcard/b2b/exhibition/validate/payments?" />">
      
      <div class="contentMain">
        <div class="row-fluid">
          <div class="span6">
            <spring:message code="b2b.exhibition.type" var="type" />
            <label for="">${type}</label>
            <select name="type" id="type" >
              <c:forEach items="${paymentTypes}" var="item">
              <option value="${item.code}">${item.description}</option>
              </c:forEach>
            </select>
          </div>
          
          <div class="span6">
            <spring:message code="b2b.exhibition.amount" var="amount" />
            <label for="">${amount}</label>
            <input type="text" placeholder="${amount}" name="amount" id="amount" class="floatInput" />
          </div>
          
          <div class="span6">
            <spring:message code="b2b.exhibition.details" var="details" />
            <label for="">${details}</label>
            <input type="text" placeholder="${details}" name="details" id="details" />
          </div>
          
          <div class="span6">
            <label for="">&nbsp;</label>
            <input type="button" id="addPaymentBtn" class="btn btn-primary" value="<spring:message code="label_add"/>" />
            <input type="button" class="btn" data-dismiss="modal" value="<spring:message code="label_close"/>" />
            <input type="button" id="clearPaymentBtn" class="btn" value="<spring:message code="label_clear"/>" />
          </div>
        </div>
      </div>
      
      
      </form>
    
    </div>
  </div>
</div>
</div>

<script type="text/javascript">
$(document).ready(function() {
	
	
});
</script>