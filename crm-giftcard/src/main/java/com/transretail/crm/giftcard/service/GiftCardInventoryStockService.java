package com.transretail.crm.giftcard.service;


import java.util.List;
import java.util.Set;

import com.transretail.crm.common.reporting.jasper.JRProcessor;
import com.transretail.crm.common.service.dto.response.CodeDescDto;
import com.transretail.crm.common.service.dto.response.ResultList;
import com.transretail.crm.giftcard.dto.GiftCardInfo;
import com.transretail.crm.giftcard.dto.GiftCardInventoryDto;
import com.transretail.crm.giftcard.dto.GiftCardInventorySearchDto;
import com.transretail.crm.giftcard.dto.GiftCardInventoryStockDto;
import com.transretail.crm.giftcard.dto.GiftCardInventoryStockSearchDto;
import com.transretail.crm.giftcard.dto.GiftCardOrderReceiveItemDto;
import com.transretail.crm.giftcard.dto.StockRequestReceiveDto;
import com.transretail.crm.giftcard.entity.GiftCardInventory;
import com.transretail.crm.giftcard.entity.GiftCardInventoryStock.GCIStockStatus;
import com.transretail.crm.giftcard.entity.StockRequest;


public interface GiftCardInventoryStockService {

	void saveStocks(List<GiftCardInventory> inventories);

	void saveStocks(List<GiftCardInventory> inventories, GCIStockStatus status);

	void saveStocksFromStockRequest(StockRequest stockRequest, StockRequestReceiveDto receiveDto, GCIStockStatus status);

	void saveStocksFromPhysicalCount(List<GiftCardInventory> missing, List<GiftCardInventory> found);

	void saveStocksFromB2Activation(Set<GiftCardInfo> activated, GCIStockStatus status);

	void saveStocksFromOrder(List<GiftCardOrderReceiveItemDto> received, GCIStockStatus status, String location);

	ResultList<GiftCardInventoryStockDto> searchStock(GiftCardInventoryStockSearchDto dto);
	
	List<CodeDescDto> getCardTypes();

	List<CodeDescDto> getCardTypes(String location);

	List<CodeDescDto> getLocations(String storeCode);

	JRProcessor createJrProcessor(GiftCardInventoryStockSearchDto dto);

    ResultList<GiftCardInventoryDto> getGiftCardInventoriesForPrintSummary(
            GiftCardInventorySearchDto searchForm);
    
    ResultList<GiftCardInventoryDto> getGiftCardInventoriesForPrintDetail(
            GiftCardInventorySearchDto searchForm);

}
