package com.transretail.crm.loyalty.dto;

import org.joda.time.LocalDate;
import org.springframework.beans.BeanUtils;

import com.transretail.crm.core.entity.lookup.LookupDetail;
import com.transretail.crm.loyalty.entity.LoyaltyCardInventory;

public class LoyaltyCardInventoryDto {
	
	public static final String[] IGNORE = {"product"};
	
	private String sequence;
	
	private String barcode;
	
	private String location;
	
	private String transferTo;
	
	private LookupDetail status;
	
	private ManufactureOrderTierDto product;
	
	private LocalDate receiveDate;
	
	private String receivedAt;
	
	private String receiptNo;
	
	private String batchRefNo;
	
	public LoyaltyCardInventoryDto() {}
	
	public LoyaltyCardInventoryDto(LoyaltyCardInventory in) {
//		BeanUtils.copyProperties(in, this, IGNORE);
        this.sequence = in.getSequence();
        this.barcode = in.getBarcode();
        this.location = in.getLocation();
        this.transferTo = in.getTransferTo();
        if (in.getStatus() != null) {
            this.status = new LookupDetail(in.getStatus().getCode(), in.getStatus().getDescription());
        }
        if (in.getProduct() != null) {
            product = new ManufactureOrderTierDto(in.getProduct());
        }
        this.receiveDate = in.getReceiveDate();
        this.receivedAt = in.getReceivedAt();
        this.receiptNo = in.getReceiptNo();
        this.batchRefNo = in.getBatchRefNo();
    }
	
	public LoyaltyCardInventory toModel() {
		LoyaltyCardInventory in = new LoyaltyCardInventory();
		BeanUtils.copyProperties(this, in, IGNORE);
		if(product != null)
			in.setProduct(product.toModel());
		return in;
	}

	public String getSequence() {
		return sequence;
	}

	public void setSequence(String sequence) {
		this.sequence = sequence;
	}
	
	public String getBarcode() {
		return barcode;
	}

	public void setBarcode(String barcode) {
		this.barcode = barcode;
	}

	public LookupDetail getStatus() {
		return status;
	}

	public void setStatus(LookupDetail status) {
		this.status = status;
	}

	public ManufactureOrderTierDto getProduct() {
		return product;
	}

	public void setProduct(ManufactureOrderTierDto product) {
		this.product = product;
	}

	public LocalDate getReceiveDate() {
		return receiveDate;
	}

	public void setReceiveDate(LocalDate receiveDate) {
		this.receiveDate = receiveDate;
	}

	public String getReceiptNo() {
		return receiptNo;
	}

	public void setReceiptNo(String receiptNo) {
		this.receiptNo = receiptNo;
	}

	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		this.location = location;
	}

	public String getTransferTo() {
		return transferTo;
	}

	public void setTransferTo(String transferTo) {
		this.transferTo = transferTo;
	}

	public String getReceivedAt() {
		return receivedAt;
	}

	public void setReceivedAt(String receivedAt) {
		this.receivedAt = receivedAt;
	}

	public String getBatchRefNo() {
		return batchRefNo;
	}

	public void setBatchRefNo(String batchRefNo) {
		this.batchRefNo = batchRefNo;
	}

	
	

}
