if ( !jQuery().formUtil ) {
    (function ( $, window, document, undefined ) {
        $.fn.formUtil = function ( method ) {
            if ( method === 'clearInputs' ) {
                return this.each( function () {
                    $( 'input[type="text"], input[type="checkbox"], select' ).each( function ( index, Element ) {
                        if ( Element.tagName === 'INPUT' ) {
                            if ( Element.type === 'checkbox' ) {
                                Element.checked = false;
                            } else {
                                Element.value = '';
                            }
                        } else if ( Element.tagName === 'SELECT' ) {
                            Element.selectedIndex = 0;
                        }
                    } );
                } );
            }
            // Add additional methods here
        };
    })( jQuery, window, document );
}