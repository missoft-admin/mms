<%@ include file="../../../common/taglibs.jsp" %>


  <!-- Visited Supermarkets -->
	<fieldset class="form-horizontal container-fluid">
	  <legend><spring:message code="label_member_indi_visitregularly" /></legend>

    <div id="visitedSupermarkets" class="controls checkbox">
    <form:checkboxes path="marketingDetails.visitedSupermarkets" items="${visitedSupermarkets}" itemLabel="description" itemValue="code"/>
    </div>
	</fieldset>


<style> input.form-control-2 { margin-top: 10px !important; } </style>