package com.transretail.crm.core.service;

import com.mysema.query.types.Expression;
import com.mysema.query.types.OrderSpecifier;
import com.mysema.query.types.path.EntityPathBase;
import com.transretail.crm.common.service.dto.request.SearchFormDto;
import com.transretail.crm.common.service.dto.response.NameIdPairResultList;

/**
 *
 */
public interface NameIdPairService {
    NameIdPairResultList getNameIdPairs(SearchFormDto searchForm, EntityPathBase<?> qModel, Expression<?> idExp,
        Expression<?> nameExp, OrderSpecifier<?> orderByExp);
}
