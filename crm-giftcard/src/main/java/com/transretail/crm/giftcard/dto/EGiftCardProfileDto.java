package com.transretail.crm.giftcard.dto;


import java.math.BigDecimal;

import com.fasterxml.jackson.annotation.JsonInclude;


@JsonInclude(JsonInclude.Include.NON_NULL)
public class EGiftCardProfileDto {

    private String productCode;
	private String productName;
    private BigDecimal faceValue;
    private Long quantity;



    public EGiftCardProfileDto(){}
    public EGiftCardProfileDto( String productCode, String productName, BigDecimal faceValue, Long quantity ){
    	this.productCode = productCode;
    	this.productName = productName;
    	this.faceValue = faceValue;
    	this.quantity = quantity;
    }



    public String getProductCode() {
		return productCode;
	}
	public void setProductCode(String productCode) {
		this.productCode = productCode;
	}
	public String getProductName() {
		return productName;
	}
	public void setProductName(String productName) {
		this.productName = productName;
	}
	public BigDecimal getFaceValue() {
		return faceValue;
	}
	public void setFaceValue(BigDecimal faceValue) {
		this.faceValue = faceValue;
	}
	public Long getQuantity() {
		return quantity;
	}
	public void setQuantity(Long quantity) {
		this.quantity = quantity;
	}
    public EGiftCardProfileDto withProductCode(String productCode) {
        this.productCode = productCode;
        return this;
    }
    public EGiftCardProfileDto withProductName(String productName) {
        this.productName = productName;
        return this;
    }
    public EGiftCardProfileDto withFaceValue(BigDecimal faceValue) {
        this.faceValue = faceValue;
        return this;
    }
    public EGiftCardProfileDto withQuantity(Long quantity) {
        this.quantity = quantity;
        return this;
    }

}
