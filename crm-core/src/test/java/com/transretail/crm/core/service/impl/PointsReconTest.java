package com.transretail.crm.core.service.impl;


import com.transretail.crm.common.service.dto.response.ResultList;
import com.transretail.crm.core.dto.PointsDto;
import com.transretail.crm.core.dto.PointsRewardDto;
import com.transretail.crm.core.dto.PointsRewardSearchDto;
import com.transretail.crm.core.dto.RewardBatchDto;
import com.transretail.crm.core.entity.EmployeePurchaseTxnModel;
import com.transretail.crm.core.entity.MemberModel;
import com.transretail.crm.core.entity.RewardSchemeModel;
import com.transretail.crm.core.entity.embeddable.CustomerProfile;
import com.transretail.crm.core.entity.enums.*;
import com.transretail.crm.core.entity.lookup.LookupDetail;
import com.transretail.crm.core.entity.lookup.LookupHeader;
import com.transretail.crm.core.entity.lookup.Store;
import com.transretail.crm.core.repo.*;
import com.transretail.crm.core.service.PointsRewardService;
import junit.framework.Assert;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.time.DateUtils;
import org.joda.time.DateTime;
import org.junit.After;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Configurable;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;


@Configurable
@Transactional
@ContextConfiguration(locations = "classpath*:/META-INF/spring/applicationContext*.xml")
@RunWith(SpringJUnit4ClassRunner.class)
public class PointsReconTest {

	@Autowired
	PointsRewardService service;

	@Autowired
	PointsTxnModelRepo repo;
	@Autowired
	StoreRepo storeRepo;
	@Autowired
	LookupHeaderRepo lookupHeaderRepo;
	@Autowired
	LookupDetailRepo lookupDetailRepo;
	@Autowired
	MemberRepo memberRepo;
	@Autowired
	RewardSchemeRepo rewardSchemeRepo;
	@Autowired
	EmployeePurchaseTxnRepo employeePurchaseTxnRepo;



    private static final String storeName	  	= "TEST";

    private static final String employeeType  	= "MTYP002";
    private static final String individualType  = "MTYP001";

	private static final String EMP1_ACCTID 	= "000001";
	private static final String EMP2_ACCTID 	= "000002";
	private static final String EMP3_INACID 	= "000003";
	private static final String IND1_ACCTID 	= "000004";

	private static final double SCHEME1_LOW 	= 1000D;
	private static final double SCHEME1_HIGH 	= 2000D;
	private static final double SCHEME2_LOW 	= 2001D;
	private static final double SCHEME2_HIGH 	= 3000D;



    @After
    public void tearDown() {
        repo.deleteAll();
    	employeePurchaseTxnRepo.deleteAll();
    	rewardSchemeRepo.deleteAll();
    	memberRepo.deleteAll();
        storeRepo.deleteAll();
        lookupDetailRepo.deleteAll();
        lookupHeaderRepo.deleteAll();
        storeRepo.deleteAll();
    }

    @Test
    public void testGenerateAndSearchReconPoints() {
    	setupPointsRewardServiceTest();

    	RewardBatchDto rewardBatch = (RewardBatchDto) service.generatePointsRewardForApproval().getReturnObj();
    	Assert.assertEquals( 1, rewardBatch.getRewards().size() );
    	Assert.assertEquals( true, CollectionUtils.isEmpty( 
    			service.generateReconPoints( null, null, AppConfigDefaults.DEFAULT_REWARDS_JOB_CRON ) ) );

    	PointsRewardSearchDto dto = new PointsRewardSearchDto();
    	dto.setStatus( TxnStatus.FORAPPROVAL );
    	ResultList<PointsRewardDto> reconsResult = service.searchRecon( dto );
    	Assert.assertEquals( 0, reconsResult.getNumberOfElements() );

    	setupReconTxns();
    	List<PointsDto> recons = service.generateReconPoints( DateUtils.addDays( new Date(), -1 ), null, AppConfigDefaults.DEFAULT_REWARDS_JOB_CRON );
    	Assert.assertEquals( 1, recons.size() );

    	reconsResult = service.searchRecon( dto );
    	Assert.assertEquals( 1, reconsResult.getNumberOfElements() );
    }



    private void setupPointsRewardServiceTest() {
    	setupStores();
    	setupMembers();
    	setupRewardSchemes();
    	setupPurchaseTxns();
	}

	private void setupStores() {
		Store theStore = new Store( storeName );
		theStore.setId( new Integer(1) );
		storeRepo.save( theStore );
	}

	private void setupMembers() {
	    LookupHeader hdr = new LookupHeader( "MEMBER_TYPE" );
	    lookupHeaderRepo.saveAndFlush( hdr );

	    LookupDetail dt = new LookupDetail( employeeType );
	    dt.setStatus( Status.ACTIVE );
	    dt.setHeader( hdr );
	    dt.setDescription( employeeType );
	    lookupDetailRepo.saveAndFlush( dt );

	    dt = new LookupDetail( individualType );
	    dt.setStatus( Status.ACTIVE );
	    dt.setHeader( hdr );
	    dt.setDescription( individualType );
	    lookupDetailRepo.saveAndFlush( dt );

	    CustomerProfile profile = new CustomerProfile();
	    profile.setBirthdate( DateUtils.addYears( new Date(), -21 ) );

	    //ACTIVE EMPS
	    MemberModel mem = new MemberModel();
	    mem.setEnabled( true );
	    mem.setAccountStatus( MemberStatus.ACTIVE );
	    mem.setAccountId( EMP1_ACCTID );
	    mem.setUsername( EMP1_ACCTID );
	    mem.setPassword( EMP1_ACCTID );
	    mem.setPin( EMP1_ACCTID );
	    mem.setContact( EMP1_ACCTID );
	    mem.setRegisteredStore( storeName );
	    mem.setMemberType( new LookupDetail( employeeType ) );
	    mem.setCustomerProfile( profile );
	    memberRepo.saveAndFlush( mem );

	    mem = new MemberModel();
	    mem.setEnabled( true );
	    mem.setAccountStatus( MemberStatus.ACTIVE );
	    mem.setAccountId( EMP2_ACCTID );
	    mem.setUsername( EMP2_ACCTID );
	    mem.setPassword( EMP2_ACCTID );
	    mem.setPin( EMP2_ACCTID );
	    mem.setContact( EMP2_ACCTID );
	    mem.setRegisteredStore( storeName );
	    mem.setMemberType( new LookupDetail( employeeType ) );
	    mem.setCustomerProfile( profile );
	    memberRepo.saveAndFlush( mem );

	    //INACTIVE EMP
	    mem = new MemberModel();
	    mem.setEnabled( true );
	    mem.setAccountStatus( MemberStatus.TERMINATED );
	    mem.setAccountId( EMP3_INACID );
	    mem.setUsername( EMP3_INACID );
	    mem.setPassword( EMP3_INACID );
	    mem.setPin( EMP3_INACID );
	    mem.setContact( EMP3_INACID );
	    mem.setRegisteredStore( storeName );
	    mem.setMemberType( new LookupDetail( employeeType ) );
	    mem.setCustomerProfile( profile );
	    memberRepo.saveAndFlush( mem );

	    //INDIVIDUAL TYPE MEMBER
	    mem = new MemberModel();
	    mem.setEnabled( true );
	    mem.setAccountStatus( MemberStatus.ACTIVE );
	    mem.setAccountId( IND1_ACCTID );
	    mem.setUsername( IND1_ACCTID );
	    mem.setPassword( IND1_ACCTID );
	    mem.setPin( IND1_ACCTID );
	    mem.setContact( IND1_ACCTID );
	    mem.setRegisteredStore( storeName );
	    mem.setMemberType( new LookupDetail( individualType ) );
	    mem.setCustomerProfile( profile );
	    memberRepo.saveAndFlush( mem );
	}

	private void setupRewardSchemes() {
		RewardSchemeModel scheme = new RewardSchemeModel();
		scheme.setStatus( Status.ACTIVE );
		scheme.setAmount( SCHEME1_LOW );
		scheme.setValueFrom( SCHEME1_LOW );
		scheme.setValueTo( SCHEME1_HIGH );
		rewardSchemeRepo.saveAndFlush( scheme );

		scheme = new RewardSchemeModel();
		scheme.setStatus( Status.ACTIVE );
		scheme.setAmount( SCHEME2_LOW );
		scheme.setValueFrom( SCHEME2_LOW );
		scheme.setValueTo( SCHEME2_HIGH );
		rewardSchemeRepo.saveAndFlush( scheme );
	}

	private void setupPurchaseTxns() {
		EmployeePurchaseTxnModel txn = new EmployeePurchaseTxnModel();
		txn.setTransactionType( VoucherTransactionType.PURCHASE );
		txn.setStatus( TxnStatus.ACTIVE );
		txn.setId( "PURCHASE_000001" );
		txn.setCreated( new DateTime( DateUtils.addMonths( new Date(), -1 ).getTime() ) ); //TODO temp solution to created datetime not being populated
		txn.setTransactionDateTime( DateUtils.addMonths( new Date(), -1 ) );
		txn.setTransactionAmount( SCHEME1_LOW );
		txn.setTransactionNo( "LASTMONTH_1000D" );
		txn.setMemberModel( memberRepo.findByAccountId( EMP1_ACCTID ) );
		employeePurchaseTxnRepo.saveAndFlush( txn );

		txn = new EmployeePurchaseTxnModel();
		txn.setTransactionType( VoucherTransactionType.PURCHASE );
		txn.setStatus( TxnStatus.ACTIVE );
		txn.setId( "PURCHASE_000002" );
		txn.setCreated( new DateTime( DateUtils.addMonths( new Date(), -1 ).getTime() ) );
		txn.setTransactionDateTime( DateUtils.addMonths( new Date(), -1 ) );
		txn.setTransactionAmount( SCHEME1_HIGH );
		txn.setTransactionNo( "LASTMONTH_2000D" );
		txn.setMemberModel( memberRepo.findByAccountId( EMP1_ACCTID ) );
		employeePurchaseTxnRepo.saveAndFlush( txn );

		txn = new EmployeePurchaseTxnModel();
		txn.setTransactionType( VoucherTransactionType.PURCHASE );
		txn.setStatus( TxnStatus.ACTIVE );
		txn.setId( "PURCHASE_000003" );
		txn.setCreated( new DateTime( DateUtils.addMonths( new Date(), -1 ).getTime() ) );
		txn.setTransactionDateTime( DateUtils.addMonths( new Date(), -1 ) );
		txn.setTransactionAmount( SCHEME1_LOW - 1D );
		txn.setTransactionNo( "LASTMONTH_1000DLOWER" );
		txn.setMemberModel( memberRepo.findByAccountId( EMP2_ACCTID ) );
		employeePurchaseTxnRepo.saveAndFlush( txn );

		txn = new EmployeePurchaseTxnModel();
		txn.setTransactionType( VoucherTransactionType.PURCHASE );
		txn.setStatus( TxnStatus.ACTIVE );
		txn.setId( "PURCHASE_000004" );
		txn.setCreated( new DateTime( DateUtils.addMonths( new Date(), -1 ).getTime() ) );
		txn.setTransactionDateTime( DateUtils.addMonths( new Date(), -1 ) );
		txn.setTransactionAmount( SCHEME1_HIGH );
		txn.setTransactionNo( "LASTMONTH_2000D" );
		txn.setMemberModel( memberRepo.findByAccountId( EMP3_INACID ) );
		employeePurchaseTxnRepo.saveAndFlush( txn );

		txn = new EmployeePurchaseTxnModel();
		txn.setTransactionType( VoucherTransactionType.PURCHASE );
		txn.setStatus( TxnStatus.ACTIVE );
		txn.setId( "PURCHASE_000005" );
		txn.setCreated( new DateTime( DateUtils.addMonths( new Date(), -1 ).getTime() ) );
		txn.setTransactionDateTime( DateUtils.addMonths( new Date(), -1 ) );
		txn.setTransactionAmount( SCHEME1_HIGH );
		txn.setTransactionNo( "LASTMONTH_2000D" );
		txn.setMemberModel( memberRepo.findByAccountId( IND1_ACCTID ) );
		employeePurchaseTxnRepo.saveAndFlush( txn );
	}

	private void setupReconTxns() {
		EmployeePurchaseTxnModel txn = new EmployeePurchaseTxnModel();
		txn.setTransactionType( VoucherTransactionType.PURCHASE );
		txn.setStatus( TxnStatus.ACTIVE );
		txn.setId( "PURCHASE_000006" );
		txn.setCreated( new DateTime() );
		txn.setTransactionDateTime( DateUtils.addMonths( new Date(), -1 ) );
		txn.setTransactionAmount( SCHEME1_LOW );
		txn.setTransactionNo( "LASTMONTH_1000D" );
		txn.setMemberModel( memberRepo.findByAccountId( EMP2_ACCTID ) );
		employeePurchaseTxnRepo.saveAndFlush( txn );
	}

}
