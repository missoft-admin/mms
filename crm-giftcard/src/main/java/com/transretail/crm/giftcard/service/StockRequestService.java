package com.transretail.crm.giftcard.service;

import java.util.List;

import com.transretail.crm.common.service.exception.MessageSourceResolvableException;
import com.transretail.crm.giftcard.dto.StockRequestDto;
import com.transretail.crm.giftcard.dto.StockRequestReceiveDto;
import com.transretail.crm.giftcard.dto.StockRequestResultList;
import com.transretail.crm.giftcard.dto.StockRequestSearchDto;
import com.transretail.crm.giftcard.entity.support.StockRequestStatus;

/**
 *
 */
public interface StockRequestService {
    Long saveStockRequest(StockRequestDto dto);

    Long updateStockRequest(Long id, StockRequestDto dto);

    StockRequestDto getStockRequest(Long id);
    
    StockRequestResultList getStockRequests(StockRequestSearchDto searchForm);
    
    List<StockRequestDto> getStockRequestsForPrint(
            StockRequestSearchDto searchForm);

    void submitStockRequest(Long id, Integer quantity);

    void validateStockRequest(Long id);

    void rejectStockRequest(Long id, String reason);

    void approveStockRequest(Long id);

    void disapproveStockRequest(Long id, String reason);

    List<StockRequestDto> findStockRequestByRequestNo(String requestNo);
    
    String createStockRequestNumber();

    void transferStockRequest(Long id);
    
    boolean hasUnapprovedRequests(String requestNo);

    void transferInStocks(StockRequestDto stockrequest) throws MessageSourceResolvableException;
    
    List<String> reserveGiftCardsForTranserOut(List<StockRequestReceiveDto> gcList, StockRequestDto stockRequestDto);

	void updateStatus(List<Long> ids, StockRequestStatus stat);

	List<String> getApprovedStockNos(List<Long> ids);

}
