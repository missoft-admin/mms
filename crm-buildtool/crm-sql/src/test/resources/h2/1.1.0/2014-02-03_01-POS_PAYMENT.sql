create table POS_PAYMENT (
        id varchar(255) not null,
        AMOUNT double,
        MEDIA_TYPE varchar(255),
        POS_TXN_ID varchar(255),
        primary key (id)
    );

alter table POS_PAYMENT 
        add constraint FK_k4cg4r56dx45db87hbq4tbmuq 
        foreign key (POS_TXN_ID) 
        references POS_TRANSACTION;
