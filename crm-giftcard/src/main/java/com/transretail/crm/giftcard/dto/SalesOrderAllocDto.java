package com.transretail.crm.giftcard.dto;


import java.util.List;


public class SalesOrderAllocDto {

	private Long soItemId;
	private List<GcAllocDto> gcBundles;



	public Long getSoItemId() {
		return soItemId;
	}
	public void setSoItemId(Long soItemId) {
		this.soItemId = soItemId;
	}
	public List<GcAllocDto> getGcBundles() {
		return gcBundles;
	}
	public void setGcBundles(List<GcAllocDto> gcBundles) {
		this.gcBundles = gcBundles;
	}

}
