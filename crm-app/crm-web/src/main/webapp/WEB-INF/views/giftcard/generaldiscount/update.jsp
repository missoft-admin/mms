<%@include file="/WEB-INF/views/common/taglibs.jsp" %>

<div class="modal-dialog">
	<div class="modal-content">
		<div class="modal-header">
		  <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
			<spring:message code="gc.general.discount.label" var="typeName" />
			<h4 class="modal-title">${typeName}</h4>
		</div>
		<div class="modal-body">
			<div id="contentError" class="hide alert alert-error">
		        <button type="button" class="close" onclick="$(this).parent().hide();">&times;</button>
		        <!-- <div><form:errors path="*"/></div>  -->
		        <div id="containerError">
		        </div>
	      	</div>
	      	
	      	<form:form id="discount" modelAttribute="discount" name="discount"
				action="${pageContext.request.contextPath}/giftcard/generaldiscount/save" cssClass="form-horizontal">
				<form:hidden path="id"/>
				
				<div class="control-group">
					<label class="control-label" for="discount"><spring:message code="gc.general.discount.label.discount" /></label>
					<div class="controls">
						<form:input path="discount"/>
					</div>
				</div>
				<div class="control-group">
					<label class="control-label" for="minPurchase"><spring:message code="gc.general.discount.label.min.purchase" /></label>
					<div class="controls">
						<form:input path="minPurchase"/>
					</div>
				</div>
				<div class="control-group">
					<label class="control-label" for="maxPurchase"><spring:message code="gc.general.discount.label.max.purchase" /></label>
					<div class="controls">
						<form:input path="maxPurchase"/>
					</div>
				</div>
			</form:form>
		</div>
		<div class="modal-footer">			
    		<button type="button" id="saveButton" class="btn btn-primary"><spring:message code="label_save" /></button>
    		<button type="button" id="cancelButton" class="btn btn-default" data-dismiss="modal"><spring:message code="label_cancel"/></button>
		</div>
	</div>
</div>

<script type="text/javascript">
$(document).ready(function() {
	$("#saveButton").unbind("click").click(function(e) {
		var form = $("#discount");
		ajaxSubmit(form.attr("action"), form.serialize(), function(response) {
			location.reload();
		});
	});
});
</script>