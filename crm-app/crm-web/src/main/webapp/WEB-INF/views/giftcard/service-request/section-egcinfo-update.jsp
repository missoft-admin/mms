<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@include file="../../common/taglibs.jsp" %>


<div id="section-egcinfo-update" class="hide" data-url="<c:url value="/giftcard/egc/info/{barcode}" />" >
  <fieldset class="form-horizontal container-fluid">
    <legend><spring:message code="gc.egc.lbl.newmobileno" /> </legend>

    <div class="hide alert alert-error" id="content_error"><button type="button" class="close" onclick="$(this).parent().hide();">&times;</button><div></div></div>
    <div class="hide alert alert-info" id="content_info"><button type="button" class="close" onclick="$(this).parent().hide();">&times;</button><div></div></div>
    <div class="control-group">
      <label class="control-label"><spring:message code="gc.egc.lbl.mobileno" /></label>
      <label class="controls"><span id="mobileNo"></span></label>
    </div>
    <div class="control-group">
      <label class="control-label"><spring:message code="gc.egc.lbl.newmobileno" /></label>
      <div class="controls"><input type="text" id="newMobileNo" placeholder="<spring:message code="gc.egc.lbl.newmobileno" />" /></div>
    </div>
    <div class="control-group">
      <div class="controls">
        <button type="button" id="updateMobileNoBtn" class="btn btn-primary" data-loading-text="Processing..."
            data-url="<c:url value="/giftcard/egc/mobile/notify/update/{barcode}/{newMobileNo}" />">
          <spring:message code="gc.egc.lbl.mobileno.update" />
        </button>
      </div>
    </div>
  </fieldset>


<style>
<!-- 
  #section-egcinfo-update label.controls { padding-top: 5px !important; }
--> 
</style>
<script type='text/javascript'>
var EgcInfo = null;
$(document).ready( function() {
    var errorContainer = $( "#section-egcinfo-update" ).find( "#content_error" );
    var infoContainer = $( "#section-egcinfo-update" ).find( "#content_info" );
	  var $egcInfo = $( "#section-egcinfo-update" );
	  var $mobileNo = $egcInfo.find( "#mobileNo" );
	  var $newMobileNo = $egcInfo.find( "#newMobileNo" );
	  var $updateBtn = $egcInfo.find( "#updateMobileNoBtn" );

	  EgcInfo = {
			  showEgcInfoSection : function( barcode ) {
				    $updateBtn.data( "barcode", barcode );
				    $.get( $egcInfo.data( "url" ).replace( new RegExp( "{barcode}", "g" ), barcode ), function( result ) {
						    if ( result.mobileNo ) {
					          $mobileNo.html( result.mobileNo );
					          $egcInfo.show( "slow" );
						    }
						    else {
			              $egcInfo.hide();
						    }
						});
			  }
	  }

	  $updateBtn.click( function(e) {
		    e.preventDefault();
		    if ( $newMobileNo.val() ) {
		    	  $.get( $updateBtn.data( "url" )
				    			  .replace( new RegExp( "{barcode}", "g" ), $updateBtn.data( "barcode" ) )
				    			  .replace( new RegExp( "{newMobileNo}", "g" ), $newMobileNo.val() ), function( result ) {
		    		    var $msgCon = $( result.success? infoContainer : errorContainer );
		    		    $msgCon.find( "div" ).html( result.result );
		    		    $msgCon.show( "slow" );
		    	  });
		    }
	  });
});
</script>
</div>
<div class="clearfix"></div>