package com.transretail.crm.core.dto;

import org.apache.commons.lang3.StringUtils;

import com.mysema.query.types.expr.BooleanExpression;
import com.transretail.crm.common.service.dto.request.AbstractSearchFormDto;
import com.transretail.crm.core.entity.QCrmInStore;

/**
 * @author Allan G. Ramirez (agramirez@exist.com)
 */
public class CrmInStoreSearchForm extends AbstractSearchFormDto {
    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public BooleanExpression createSearchExpression() {
        if (StringUtils.isNotBlank(name)) {
            return QCrmInStore.crmInStore.name.containsIgnoreCase(name);
        }
        return null;
    }
}
