package com.transretail.crm.core.service;

import java.io.InputStream;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import org.joda.time.LocalDate;

import com.transretail.crm.common.reporting.jasper.JRProcessor;
import com.transretail.crm.common.service.dto.request.PageSortDto;
import com.transretail.crm.common.service.dto.response.ResultList;
import com.transretail.crm.core.dto.EmployeePurchaseTxnDto;
import com.transretail.crm.core.dto.PurchaseTxnSearchDto;
import com.transretail.crm.core.entity.EmployeePurchaseTxnModel;
import com.transretail.crm.core.entity.enums.VoucherTransactionType;

public interface EmployeePurchaseService {

	List<EmployeePurchaseTxnDto> findAllPendingPurchasesAdjustments();

	EmployeePurchaseTxnDto findPurchaseTransactionById(String id);

	EmployeePurchaseTxnDto findOneByTransactionNo(String txnNo);

    List<EmployeePurchaseTxnModel> findByTransactionNo(String txnNo);
	
	long countAllPendingPurchaseAdjustments();

	List<EmployeePurchaseTxnDto> findPendingPurchasesAdjustmentsEntries(
			int firstResult, int maxResults);

	void deletePurchaseTransaction(String id);

	EmployeePurchaseTxnDto savePurchaseTxn(
			EmployeePurchaseTxnDto employeePurchaseTxnDto);

	JRProcessor createPurchaseTxnJrProcessor(String empId, Date dateFrom,
			Date dateTo, InputStream logo);

	List<EmployeePurchaseTxnDto> findPurchaseTxnByIdAndPostingDates(
			String employeeId, Date postingDateFrom, Date postingDateTo);

	ResultList<EmployeePurchaseTxnDto> listTransactionDto(String employeeId,
			PageSortDto searchDto);
	
	ResultList<EmployeePurchaseTxnDto> listTransactionDtoForApproval(PageSortDto searchDto);
	
	List<EmployeePurchaseTxnDto> getTransactionsToday(String accountId);
	
	List<EmployeePurchaseTxnDto> getTransactionsByDate(String accountId, LocalDate date);
	
	EmployeePurchaseTxnDto findById(String id);

	List<EmployeePurchaseTxnDto> getTransactionDetails(String transactionNo);

	ResultList<EmployeePurchaseTxnDto> searchPurchaseTxn(PurchaseTxnSearchDto theSearchDto);

	BigDecimal getTotal( VoucherTransactionType[] types, String acctId );
}
