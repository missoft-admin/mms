<%@include file="/WEB-INF/views/common/taglibs.jsp" %>

<div class="page-header page-header2">
<h1><spring:message code="menutab_giftcard_orders" /></h1>
</div>

<sec:authorize ifAllGranted="ROLE_MERCHANT_SERVICE">
  <c:set var="isMerchantService" value="true"/>
</sec:authorize>
<sec:authorize ifAllGranted="ROLE_MERCHANT_SERVICE_SUPERVISOR">
  <c:set var="isMerchantServiceSupervisor" value="true"/>
</sec:authorize>
<style>
  .long_list_container {
    overflow-y: hidden;
    overflow-x: scroll;
    height: auto;
  }

  .contentDialogBody label {
    width: 130px;
    padding: 0;
  }
  .table {
      float: left;
  }
</style>
<link href="<spring:url value="/css/dataTables.css" />" rel="stylesheet">
<link href="<spring:url value="/css/bootstrap/datepicker.css" />" rel="stylesheet" />
<script src='<spring:url value="/js/bootstrap/bootstrap-datepicker.js"/>' type='text/javascript'></script>
<script src="<spring:url value="/js/jquery/jquery.dataTables.js" />"></script>
<script src="<spring:url value="/js/jquery/jquery.dataTables.linkLengthMenu.js" />"></script>
<script src="<spring:url value="/js/jquery/jquery.dataTables.customPaging.js" />"></script>
<script src="<spring:url value="/js/jquery/ajax-datatable.js" />"></script>
<script src="<spring:url value="/js/common/form2js.js" />"></script>
<script src="<spring:url value="/js/common/json2.js" />"></script>
<script src="<spring:url value="/js/jquery/jquery.toObject.js" />"></script>
<script src="<spring:url value="/js/jquery/jquery.numberInput.js" />"></script>
<div id="order_panel">
  <%@include file="searchForm.jsp" %>
  <div id="errorMessages error">&nbsp;</div>
  <div id="successMessages">&nbsp;</div>
  <div id="order_list_container">
  </div>
</div>
<%@include file="orderForm.jsp" %>
<%@include file="itemForm.jsp" %>
<script>
$( document ).ready( function () {
  if ( jQuery().ajaxDataTable ) {
    $('.date' ).datepicker({
      format    : 'dd-mm-yyyy',
      autoclose : true
    });

    var $orderList = $( '#order_list_container' );
    var searchForm = $( '#searchForm' );
    $( '#search' ).click( function () {
      var formDataObject = searchForm.toObject( {mode : 'first', skipEmpty : false} );
      var $btnSearch = $( this );
      $btnSearch.prop( 'disabled', true );
      $orderList.ajaxDataTable( 'search', formDataObject, function () {
        $btnSearch.prop( 'disabled', false );
      } );
    } );

    var rowIdPrefix = 'order-';
    var clickableCell = function ( data, type, row ) {
      <c:choose>
              <c:when test="${isMerchantServiceSupervisor}">var clickable = true;
      </c:when>
              <c:otherwise>var clickable = false;
      </c:otherwise>
      </c:choose>
      if ( clickable || row.status == 'REJECTED' || row.status == 'DRAFT' ) {
        return '<a class="clickable" href="#" onclick="return false;"><div>' + ( data == null || data === '' ? '&nbsp;' : data) + '</div></a>';
      } else {
        return data;
      }
    };
    $orderList.ajaxDataTable( {
      'autoload' : true,
      'ajaxSource' : '<spring:url value="/giftcard/order/list/${id}" />',
      'fnRowCallback' : function ( nRow, aData, iDisplayIndex ) {
        nRow.id = rowIdPrefix + aData.id;
        return nRow;
      },
      'columnHeaders' : [
        '<spring:message code="gc.order.label.id" />',
        '<spring:message code="gc.order.label.cardOrderNo" />',
        '<spring:message code="gc.order.label.orderDate" />',
        '<spring:message code="gc.order.label.vendor" />',
        '<spring:message code="gc.order.label.status" />',
        '<spring:message code="gc.order.label.statusLastUpdated" />'
      ],
      'modelFields' : [
        {name : 'id', sortable : true, fieldCellRenderer : clickableCell},
        {name : 'cardOrderNo', sortable : true, fieldCellRenderer : clickableCell},
        {name : 'orderDate', sortable : true, fieldCellRenderer : clickableCell},
        {name : 'vendorName', sortable : true, fieldCellRenderer : clickableCell},
        {name : 'status', sortable : true, fieldCellRenderer : clickableCell},
        {name : 'statusLastUpdated', sortable : true, fieldCellRenderer : clickableCell}
      ]
    } );

    var itemList = $( '#itemList' );
    var resetItemList = function () {
      itemList.addClass( 'empty' );
      itemList.children( 'tr' ).remove();
      itemList.append( '<tr><td colspan="9">Nothing to display</td></tr>' );
    };

    var $statusDisplay = $( '#status_display' );
    var $orderForm = $( '#orderForm' );
    var $addEditOrderDialog = $( '#addEditOrderDialog' ).modal( { show : false } )
            .on( 'hide.bs.modal', function () {
              $orderForm.formUtil( 'clearInputs' );
              $( ".alert", $orderForm ).alert( 'close' )
            } )
            .on( 'show.bs.modal', function () {
              var htmlString = '';
              if ( $statusDisplay.val() == '' || $statusDisplay.val() == 'DRAFT' || $statusDisplay.val() == 'SUBMITTED' ) {
                $( '.modal-footer button:not(#sendToFtp)', $addEditOrderDialog ).show();
                $( '#sendToFtp' ).hide();
                $( '#addNewItem' ).show();
                htmlString += '<a class="edit" href="#" onclick="return false;"><spring:message code="edit"/></a>&nbsp;';
                htmlString += '<a class="delete" href="#" onclick="return false;"><spring:message code="delete"/></a>';
              } else {
                $( '.modal-footer button:not(#closeAddEditOrderDialog)', $addEditOrderDialog ).hide();
                $( '#addNewItem' ).hide();
                if ( $statusDisplay.val() == 'APPROVED' ) {
                  $( '#sendToFtp' ).show();
                }
              }
              $( 'tr > td:eq( 5 )', itemList ).html( htmlString );
            } );

    var $itemForm = $( '#itemForm' );
    var $addEditItemDialog = $( '#addEditItemDialog' ).modal( { show : false } )
            .on( 'hide.bs.modal', function () {
              $itemForm.formUtil( 'clearInputs' );
              $( ".alert", $itemForm ).alert( 'close' );
              $( '#itemId' ).val( '' );
              $( '#itemRowId' ).val( '' );
            } )

    var setRowContents = function ( rowObject, item, index ) {
      var htmlString = '<td>';
      htmlString += ' <span>' + item.giftCardName + '</span>';
      htmlString += ' <input type="hidden" name="items[' + index + '].id" value="' + item.id + '"/>';
      htmlString += ' <input type="hidden" name="items[' + index + '].giftCardId" value="' + item.giftCardId + '"/>';
      htmlString += ' <input type="hidden" name="items[' + index + '].giftCardName" value="' + item.giftCardName + '"/>';
      htmlString += '</td>';
      htmlString += '<td>';
      htmlString += ' <span>' + item.lpo + '</span>';
      htmlString += ' <input type="hidden" name="items[' + index + '].lpo" value="' + item.lpo + '"/>';
      htmlString += '</td>';
      htmlString += '<td>';
      htmlString += ' <span>' + item.generationType + '</span>';
      htmlString += ' <input type="hidden" name="items[' + index + '].generationType" value="' + item.generationType + '"/>';
      htmlString += '</td>';
      htmlString += '<td>';
      htmlString += ' <span>' + item.quantity + '</span>';
      htmlString += ' <input type="hidden" name="items[' + index + '].quantity" value="' + item.quantity + '"/>';
      htmlString += '</td>';
      htmlString += '<td>';
      htmlString += ' <span>' + item.storeName + '</span>';
      htmlString += ' <input type="hidden" name="items[' + index + '].storeId" value="' + item.storeId + '"/>';
      htmlString += ' <input type="hidden" name="items[' + index + '].storeName" value="' + item.storeName + '"/>';
      htmlString += '</td>';
      htmlString += '<td>';
      htmlString += '<a class="edit" href="#" onclick="return false;"><spring:message code="edit"/></a>&nbsp;';
      htmlString += '<a class="delete" href="#" onclick="return false;"><spring:message code="delete"/></a>';
      htmlString += '</td>';
      rowObject.html( htmlString );
    };

    var showErrorMessage = function ( $errorElement, message ) {
      $errorElement.html( '<div class="alert alert-error">' +
              '<button type="button" class="close" data-dismiss="alert">&times;</button>' +
              message +
              '</div>' );
    };

    var showSuccessMessage = function ( message ) {
      $( '#successMessages' ).html( '<div class="alert alert-success">' +
              '<button type="button" class="close" data-dismiss="alert">&times;</button>' +
              message +
              '</div>' );
    };

    var itemRowPrefix = 'itemrow-';
    // Update Gift Order Card / Manufacture Order
    $( $orderList ).on( 'click', 'a.clickable', function () {
      var trId = $( this ).parents( 'tr' ).attr( 'id' );
      var orderId = trId.substring( rowIdPrefix.length );
      $.ajax( {
        "type" : "GET",
        "url" : '<spring:url value="/giftcard/order/"/>' + orderId,
        "success" : function ( data ) {
          $( '#orderId' ).val( orderId );
          $( '#cardOrderNo' ).val( data.cardOrderNo );
          $( '#vendor' ).val( data.vendor ).change();
          $( '#memo' ).val( data.memo );
          $( '#orderDate' ).val( data.orderDate );
          $statusDisplay.val( data.status );
          $( '#statusLastUpdated' ).val( data.statusLastUpdated );
          if ( data.items.length > 0 ) {
            itemList.children( 'tr' ).remove();
            itemList.removeClass( 'empty' );
            for ( var i = 0; i < data.items.length; i++ ) {
              var rowObject = $( '<tr id=' + itemRowPrefix + i + '></tr>' );
              setRowContents( rowObject, data.items[i], i );
              itemList.append( rowObject );
            }
          } else if ( !itemList.hasClass( 'empty' ) ) {
            // Reset if not in reset form
            resetItemList();
          }
          $addEditOrderDialog.modal( 'toggle' );
        },
        "error" : function ( xhr, textStatus, error ) {
          if ( textStatus === 'timeout' ) {
            showErrorMessage( $( '#errorMessages' ), 'The server took too long to send the data.' );
          }
          else {
            if ( error ) {
              showErrorMessage( $( '#errorMessages' ), error );
            } else {
              showErrorMessage( $( '#errorMessages' ), 'An error occurred on the server. Please try again.' );
            }
          }
        }
      } );
    } );

    // Add Gift Order Card / Manufacture Order
    $( '#addOrder' ).click( function () {
      $( '#orderId' ).val( '' );
      $addEditOrderDialog.modal( 'toggle' );
    } );

    $( '#addNewItem' ).click( function () {
      $addEditItemDialog.modal( 'toggle' );
    } );

    var $orderForm = $( '#orderForm' );

    var submitForm = function ( status ) {
      var orderButtons = $( 'button', $addEditOrderDialog );
      orderButtons.each( function () {
        this.className = '';
        this.disabled = true;
      } );
      $( '#status' ).val( status );

      var ajaxUrl = '';
      var selectedOrderId = $( '#orderId' ).val();
      if ( selectedOrderId ) {
        ajaxUrl = '<spring:url value="/giftcard/order/update/"/>' + selectedOrderId;
      } else {
        // Add
        ajaxUrl = '<spring:url value="/giftcard/order/add"/>';
      }

      $.ajax( {
        "contentType" : 'application/json',
        headers : { 'Content-Type' : 'application/json' },
        "type" : "POST",
        "url" : ajaxUrl,
        "data" : JSON.stringify( $orderForm.toObject( {mode : 'first', skipEmpty : false} ) ),
        "success" : function ( data ) {
          $( '#status' ).val( '' );
          orderButtons.each( function () {
            this.className = 'btn btn-primary';
            this.disabled = false;
          } );

          if ( data.hasOwnProperty( 'errorMessages' ) ) {
            var errorInfo = "";
            for ( var i = 0; i < data.errorMessages.length; i++ ) {
              errorInfo += "<br>" + (i + 1) + ". " + data.errorMessages[i];
            }
            showErrorMessage( $( 'div.errorMessages', $orderForm ), "Please correct following errors: " + errorInfo );
          } else {
            $( ".alert", $orderForm ).alert( 'close' )
            showSuccessMessage( data.successMessages );
            $addEditOrderDialog.modal( 'toggle' );
            // Reload the list
            $( '#search' ).click();
          }
        },
        "error" : function ( xhr, textStatus, error ) {
          $( '#status' ).val( '' );
          orderButtons.each( function () {
            this.className = 'btn btn-primary';
            this.disabled = false;
          } );
          if ( textStatus === 'timeout' ) {
            showErrorMessage( $( 'div.errorMessages', $orderForm ), 'The server took too long to send the data.' );
          } else if ( error ) {
            showErrorMessage( $( 'div.errorMessages', $orderForm ), error );
          } else {
            showErrorMessage( $( 'div.errorMessages', $orderForm ), 'An error occurred on the server. Please try again.' );
          }
        }
      } );
    };
    <%-- ROLE_MERCHANT_SERVICE && ROLE_MERCHANT_SERVICE_SUPERVISOR should not be assigned to single user --%>
    <c:choose>
    <c:when test="${isMerchantService}">
    $( '#saveOrder' ).click( function () {
      submitForm( 'DRAFT' );
    } );
    $( '#submitOrder' ).click( function () {
      submitForm( 'SUBMITTED' );
    } );
    </c:when>
    <c:when test="${isMerchantServiceSupervisor}">
    $( '#approveOrder' ).click( function () {
      submitForm( 'APPROVED' );
    } );
    $( '#rejectOrder' ).click( function () {
      submitForm( 'REJECTED' );
    } );
    $( '#voidOrder' ).click( function () {
      submitForm( 'VOID' );
    } );
    $( '#sendToFtp' ).click( function () {
      submitForm( 'SENT_TO_FTP' );
    } );
    </c:when>
    </c:choose>
    $( '#closeAddEditOrderDialog' ).click( function () {
      $addEditOrderDialog.modal( 'toggle' );
    } );

    $( 'select#vendor' ).change(function () {
      $( this ).siblings( '#vendorName' ).val( $( this ).find( "option:selected" ).text() );
    } ).change();

    // Gift Order Item
    $( 'select#giftCardId' ).change(function () {
      $( this ).siblings( '#giftCardName' ).val( $( this ).find( "option:selected" ).text() );
    } ).change();

    $( 'select#storeId' ).change(function () {
      $( this ).siblings( '#storeName' ).val( $( this ).find( "option:selected" ).text() );
    } ).change();

    $( "#saveItem" ).click( function () {
      var giftCardId = $( '#giftCardId' ).val();
      var giftCardName = $( '#giftCardName' ).val();
      var storeId = $( '#storeId' ).val();
      var storeName = $( '#storeName' ).val();
      var lpo = $( '#lpo' ).val();
      var generationType = $( '#generationType' ).val();
      var quantity = $( '#quantity' ).val();
      var itemId = $( '#itemId' ).val();
      var rowNo = $( '#rowNo' ).val();

      var totalErrors = 0;
      var errorMessages = '';
      if ( isEmpty( giftCardName ) ) {
        totalErrors++;
        errorMessages += totalErrors + '. <spring:message code="gc.order.label.giftCardName.required"/><br/>';
      }
      if ( isEmpty( lpo ) ) {
        totalErrors++;
        errorMessages += totalErrors + '. <spring:message code="gc.order.label.lpo.required"/><br/>';
      }
      if ( isEmpty( generationType ) ) {
        totalErrors++;
        errorMessages += totalErrors + '. <spring:message code="gc.order.label.generationType.required"/><br/>';
      }
      if ( isEmpty( storeName ) ) {
        totalErrors++;
        errorMessages += totalErrors + '. <spring:message code="gc.order.label.storeName.required"/><br/>';
      }
      if ( totalErrors > 0 ) {
        showErrorMessage( $( '.errorMessages', $addEditItemDialog ), errorMessages );
        return false;
      } else {
        var item = {
          'id' : itemId,
          'generationType' : generationType,
          'giftCardId' : giftCardId,
          'giftCardName' : giftCardName,
          'lpo' : lpo,
          'quantity' : quantity,
          'storeId' : storeId,
          'storeName' : storeName
        };

        var index = 0;
        var rowObject = null;
        if ( itemList.hasClass( 'empty' ) ) {
          itemList.children( 'tr' ).remove();
          itemList.removeClass( 'empty' );
        } else {
          var itemRowId = $( '#itemRowId' ).val();
          if ( itemRowId ) {
            rowObject = $( 'tr#' + itemRowId );
            index = itemRowId.substring( itemRowPrefix.length );
          } else {
            // Should be fine. Else somethigns wrong with setting itemRowId during edit of item
            index = parseInt( $( 'tr:last', itemList ).attr( 'id' ).substring( itemRowPrefix.length ) ) + 1;
          }
        }

        if ( rowObject == null ) {
          rowObject = $( '<tr id=' + itemRowPrefix + index + '></tr>' );
          itemList.append( rowObject );
        }
        setRowContents( rowObject, item, index );

        $addEditItemDialog.modal( 'toggle' );
        return true;
      }
    } );
    $( "#closeItemDialog" ).click( function () {
      $addEditItemDialog.modal( 'toggle' );
    } );

    $( itemList ).on( 'click', 'a.edit', function () {
      var trObject = $( this ).parents( 'tr' );
      trObject.find( 'input:hidden' ).each( function () {
        var fieldName = this.name.substring( this.name.indexOf( '.' ) + 1 );
        if ( fieldName == 'id' ) {
          $( '#itemId' ).val( this.value );
        } else {
          $( '#' + fieldName ).val( this.value );
        }
      } );
      $( '#itemRowId' ).val( trObject.attr( 'id' ) );
      $addEditItemDialog.modal( 'toggle' );
    } );

    $( itemList ).on( 'click', 'a.delete', function () {
      $( this ).parents( 'tr' ).remove();
    } );
  } else {
    $( '#order_panel' ).html( '<spring:message code="gc.product.load.error" />' );
  }
} );
</script>