<%@ include file="../taglibs.jsp"%>

    <div class="tab-pane active" id="businessinfo">
    
      <fieldset class="container-fluid">
      <div class="control-group">
        <form:label path="preferedStoreCode" title="Registered Store"
          cssClass="control-label" cssErrorClass="control-label error">
          <spring:message code="registered_store" />
        </form:label>
        <div class="controls">
          <c:forEach var="registeredStore" items="${stores}">
            <c:if test="${registeredStore.code == form.preferedStoreCode}">
              <span class="bootstrap-input input-xlarge">${registeredStore.code} - ${registeredStore.name}</span>
            </c:if>
          </c:forEach>
          <form:hidden path="preferedStoreCode" />
          <!--
          <form:select path="preferedStoreCode" items="${stores}"
            title="Preferred Store" itemValue="code" itemLabel="name"
            cssClass="input-xlarge" /> -->
        </div>
      </div>
    
        <div class="control-group">
          <form:label path="companyName" cssClass="control-label"
            cssErrorClass="control-label error">
            <spring:message code="business_name" />
          </form:label>
          <div class="controls">
            <form:input path="companyName" cssClass="input-xlarge" />
          </div>
        </div>
        </fieldset>
        
        <hr />
        
      <fieldset class="container-fluid">
        <label class=""><spring:message code="business_address" /></label>
        <div class="row-fluid address-line">
          <spring:message code="street" var="street" />
          <fieldset class="span9">
            <label for="street">${street }</label>
            <form:input path="businessStreet" placeholder="${street}"
              cssClass="span12" />
          </fieldset>
          <spring:message code="streetNo" var="streetNumber" />
          <fieldset class="span3">
            <label for="streetNumber">${streetNumber }</label>
            <form:input path="businessStreetNumber" cssClass="span12"
              placeholder="${streetNumber}" />
          </fieldset>
        </div>
        <div class="row-fluid address-line">
          <spring:message code="block" var="block" />
          <fieldset class="span6">
            <label for="block">${block }</label>
            <form:input path="businessBlock" placeholder="${block}" cssClass="span12" />
          </fieldset>
          <spring:message code="km" var="km" />
          <fieldset class="span2">
            <label for="km">${km }</label>
            <form:input path="businessKm" placeholder="${km}" cssClass="span12" />
          </fieldset>
          <spring:message code="rt" var="rt" />
          <fieldset class="span2">
            <label for="rt">${rt }</label>
            <form:input path="businessRt" placeholder="${rt}" cssClass="span12" />
          </fieldset>
          <spring:message code="rw" var="rw" />
          <fieldset class="span2">
            <label for="rw">${rw }</label>
            <form:input path="businessRw" placeholder="${rw}" cssClass="span12" />
          </fieldset>
        </div>
        <div class="row-fluid address-line">
          <spring:message code="building" var="building" />
          <fieldset class="span8">
            <label for="building">${building }</label>
            <form:input path="businessBuilding" placeholder="${building}"
              cssClass="span12" />
          </fieldset>
          <spring:message code="floor" var="floor" />
          <fieldset class="span2">
            <label for="floor">${floor }</label>
            <form:input path="businessFloor" placeholder="${floor}" cssClass="span12" />
          </fieldset>
          <spring:message code="room" var="room" />
          <fieldset class="span2">
            <label for="room">${room }</label>
            <form:input path="businessRoom" placeholder="${room}" cssClass="span12" />
          </fieldset>
        </div>
        <div class="row-fluid address-line">
          <spring:message code="postcode" var="postCode" />
          <fieldset class="span2">
            <label for="postCode">${postCode }</label>
            <form:input path="businessPostCode" placeholder="${postCode}"
              cssClass="span12" />
          </fieldset>
          <spring:message code="subdistrict" var="subdistrict" />
          <fieldset class="span4">
            <label for="subdistrict">${subdistrict }</label>
            <form:input path="businessSubdistrict" placeholder="${subdistrict}"
              cssClass="span12" />
          </fieldset>
          <spring:message code="district" var="district" />
          <fieldset class="span6">
            <label for="district">${district }</label>
            <form:input path="businessDistrict" placeholder="${district}"
              cssClass="span12" />
          </fieldset>
        </div>
        <div class="row-fluid address-line">
          <spring:message code="province" var="province" />
          <fieldset class="span6">
            <label for="city">${province }</label>
            <form:input path="businessProvince" placeholder="${province}" cssClass="span12" />
          </fieldset>
          <spring:message code="city" var="city" />
          <fieldset class="span6">
            <label for="city">${city }</label>
            <form:input path="businessCity" placeholder="${city}" cssClass="span12" />
          </fieldset>
        </div>
      </fieldset>
        
        <hr />
        
        <fieldset class="container-fluid">
        <div class="control-group">
          <form:label path="customerSegmentationCode" cssClass="control-label" cssErrorClass="control-label error">
            <spring:message code="business_field" />
          </form:label>
          <div class="controls">
            <form:select id="customerGroup" path="customerGroupCode" items="${customerGroups}"
              title="Preferred Store" itemValue="code" itemLabel="description"
              cssClass="input-xlarge" />
            <select name="customerSegmentationCode" id="customerSegmentation"></select>
          </div>
        </div>
        
        
        <div class="control-group">
          <form:label path="businessLicense" cssClass="control-label"
            cssErrorClass="control-label error">
            <spring:message code="business_license" />
          </form:label>
          <div class="controls">
            <form:input path="businessLicense" cssClass="input-xlarge" />
          </div>
        </div>
        
        <div class="control-group">
          <form:label path="registrationId" cssClass="control-label"
            cssErrorClass="control-label error">
            <spring:message code="company_reg_id" />
          </form:label>
          <div class="controls">
            <form:input path="registrationId" cssClass="input-xlarge" />
          </div>
        </div>
        </fieldset>
        
        <hr />
        
        <fieldset class="container-fluid">
        <label><spring:message code="header.npwp" /></label>
        
        <div class="control-group">
          <form:label path="npwp.npwpId" cssClass="control-label"
            cssErrorClass="control-label error">
            <spring:message code="id" />
          </form:label>
          <div class="controls">
            <form:input path="npwp.npwpId" cssClass="input-xlarge" id="npwpId"
              placeholder="#.###.###.#-###" />
          </div>
        </div>
        <div class="control-group">
          <form:label path="npwp.npwpName" cssClass="control-label"
            cssErrorClass="control-label error">
            <spring:message code="name" />
          </form:label>
          <div class="controls">
            <form:input path="npwp.npwpName" cssClass="input-xlarge"
              id="npwpName" />
          </div>
        </div>
        <div class="control-group">
          <form:label path="npwp.npwpAddress" cssClass="control-label"
            cssErrorClass="control-label error">
            <spring:message code="address" />
          </form:label>
          <div class="controls">
            <form:input path="npwp.npwpAddress" cssClass="input-xlarge"
              id="npwpAddress" />
          </div>
        </div>
        
        <div class="control-group">
          <form:label path="businessPhone" cssClass="control-label"
            cssErrorClass="control-label error">
            <spring:message code="business_phone" />
          </form:label>
          <div class="controls">
            <form:input path="businessPhone" cssClass="input-xlarge" />
          </div>
        </div>
        
        
        <div class="control-group">
          <form:label path="businessEmail" cssClass="control-label"
            cssErrorClass="control-label error">
            <spring:message code="business_email" />
          </form:label>
          <div class="controls">
            <form:input path="businessEmail" cssClass="input-xlarge" />
          </div>
        </div>
        
        <div class="control-group">
          <form:label path="radius" cssClass="control-label"
            cssErrorClass="control-label error">
            <spring:message code="radius" />
          </form:label>
          <div class="controls">
            <form:input path="radius" cssClass="input-xlarge" />
          </div>
        </div>
        
        <div class="control-group">
          <form:label path="zone" cssClass="control-label"
            cssErrorClass="control-label error">
            <spring:message code="zone" />
          </form:label>
          <div class="controls">
            <form:input path="zone" cssClass="input-xlarge" />
          </div>
        </div>
        
        <div class="control-group">
          <form:label path="noOfEmp" cssClass="control-label"
            cssErrorClass="control-label error">
            <spring:message code="no_of_employees" />
          </form:label>
          <div class="controls">
            <form:select path="noOfEmp" items="${noOfEmps}"
              itemLabel="description" itemValue="code" />
            <%-- <form:input path="noOfEmp" cssClass="input-xlarge" /> --%>
          </div>
        </div>
        
        <div class="control-group">
          <form:label path="potentialBuyingPerMonth" cssClass="control-label"
            cssErrorClass="control-label error">
            <spring:message code="potential_buying_per_month" />
          </form:label>
          <div class="controls">
            <div class="radio"><form:radiobutton path="potentialBuyingPerMonth" value="<5 m" /><label><spring:message code="member_prof_potential_buying_lt5" /></label></div>
            <div class="radio"><form:radiobutton path="potentialBuyingPerMonth" value="5-9.9 m" /><label><spring:message code="member_prof_potential_buying_5to9.9" /></label></div>
            <div class="radio"><form:radiobutton path="potentialBuyingPerMonth" value="10-50 m" /><label><spring:message code="member_prof_potential_buying_10to50" /></label></div>
            <div class="radio"><form:radiobutton path="potentialBuyingPerMonth" value=">50 m" /><label><spring:message code="member_prof_potential_buying_gt50" /></label></div>
            <%-- <form:input path="potentialBuyingPerMonth" cssClass="input-xlarge" /> --%>
          </div>
        </div>
        
        </fieldset>
        
    
    </div>
    
    
    <div class="tab-pane" id="personalinfo">
      <fieldset class="container-fluid">
      <div class="control-group">
        <form:label path="idNumber" cssClass="control-label"
          cssErrorClass="control-label error">
          <span class="required">*</span><spring:message code="id_card_number" />
        </form:label>
        <div class="controls">
          <form:hidden path="idNumber" />
          <input type="text" name="idNumberTxt" class="form-control input-xlarge" placeholder="${idNumber}" />
          <div class="radio"><input type="radio" name="idNumberType" value="KTP"><label><spring:message code="ktp" /></label></div>
          <div class="radio"><input type="radio" name="idNumberType" value="SIM"><label><spring:message code="sim" /></label></div>
          <div class="radio"><input type="radio" name="idNumberType" value="PASSPORT"><label><spring:message code="passport" /></label></div>
        </div>
      </div>
      
      <div class="control-group">
        <form:label path="firstName" cssClass="control-label"
          cssErrorClass="control-label error">
          <spring:message code="firstname" />
        </form:label>
        <div class="controls">
          <form:input path="firstName" cssClass="input-xlarge" />
        </div>
      </div>
      <div class="control-group">
        <form:label path="lastName" cssClass="control-label"
          cssErrorClass="control-label error">
          <spring:message code="lastname" />
        </form:label>
        <div class="controls">
          <form:input path="lastName" cssClass="input-xlarge" />
        </div>
      </div>
      
      <div class="control-group">
        <spring:message code="birthdate" var="birthdate" />
        <form:label path="birthDate" title="${birthdate}"
          cssClass="control-label" cssErrorClass="control-label error">${birthdate}</form:label>
        <div class="controls">
          <form:input path="birthDate" title="${birthdate} (dd-MM-yyyy)"
            cssClass="input-xlarge" placeholder="dd-MM-yyyy" id="birthDate" />
        </div>
      </div>
      
      <div class="control-group">
        <form:label path="gender" cssClass="control-label"
          cssErrorClass="control-label error">
          <spring:message code="gender" />
        </form:label>
        <div class="controls">
          <form:select path="gender" cssClass="input-xlarge">
            <form:option value="">&nbsp;</form:option>
            <form:options items="${genders}" itemValue="code"
              itemLabel="description" />
          </form:select>
        </div>
      </div>
      </fieldset>
      
      <hr/>
      
      <fieldset class="container-fluid">
        <label><spring:message code="home_address" /></label>
        <div class="row-fluid address-line">
          <spring:message code="street" var="street" />
          <fieldset class="span9">
            <label for="street"><span class="required">*</span>${street }</label>
            <form:input path="street" placeholder="${street}"
              cssClass="span12" />
          </fieldset>
          <spring:message code="streetNo" var="streetNumber" />
          <fieldset class="span3">
            <label for="streetNumber">${streetNumber }</label>
            <form:input path="streetNumber" cssClass="span12"
              placeholder="${streetNumber}" />
          </fieldset>
        </div>
        <div class="row-fluid address-line">
          <spring:message code="block" var="block" />
          <fieldset class="span6">
            <label for="block">${block }</label>
            <form:input path="block" placeholder="${block}" cssClass="span12" />
          </fieldset>
          <spring:message code="km" var="km" />
          <fieldset class="span2 professionalDetails">
            <label for="km">${km }</label>
            <form:input path="km" placeholder="${km}" cssClass="span12" />
          </fieldset>
          <spring:message code="rt" var="rt" />
          <fieldset class="span2">
            <label for="rt">${rt }</label>
            <form:input path="rt" placeholder="${rt}" cssClass="span12" />
          </fieldset>
          <spring:message code="rw" var="rw" />
          <fieldset class="span2">
            <label for="rw">${rw }</label>
            <form:input path="rw" placeholder="${rw}" cssClass="span12" />
          </fieldset>
        </div>
        <div class="row-fluid address-line">
          <spring:message code="building" var="building" />
          <fieldset class="span8">
            <label for="building">${building }</label>
            <form:input path="building" placeholder="${building}"
              cssClass="span12" />
          </fieldset>
          <spring:message code="floor" var="floor" />
          <fieldset class="span2 professionalDetails">
            <label for="floor">${floor }</label>
            <form:input path="floor" placeholder="${floor}" cssClass="span12" />
          </fieldset>
          <spring:message code="room" var="room" />
          <fieldset class="span2 professionalDetails">
            <label for="room">${room }</label>
            <form:input path="room" placeholder="${room}" cssClass="span12" />
          </fieldset>
        </div>
        <div class="row-fluid address-line">
          <spring:message code="postcode" var="postCode" />
          <fieldset class="span2">
            <label for="postCode">${postCode }</label>
            <form:input path="postCode" placeholder="${postCode}"
              cssClass="span12" />
          </fieldset>
          <spring:message code="subdistrict" var="subdistrict" />
          <fieldset class="span4">
            <label for="subdistrict">${subdistrict }</label>
            <form:input path="subdistrict" placeholder="${subdistrict}"
              cssClass="span12" />
          </fieldset>
          <spring:message code="district" var="district" />
          <fieldset class="span4">
            <label for="district">${district }</label>
            <form:input path="district" placeholder="${district}"
              cssClass="span12" />
          </fieldset>
        </div>
        <div class="row-fluid address-line">
          <spring:message code="province" var="province" />
          <fieldset class="span6">
            <label for="province">${province }</label>
            <form:input path="province" placeholder="${province}" cssClass="span12" />
          </fieldset>
          <spring:message code="city" var="city" />
          <fieldset class="span6">
            <label for="city">${city }</label>
            <form:input path="city" placeholder="${city}" cssClass="span12" />
          </fieldset>
        </div>
      </fieldset>
      
      <hr/>
      
      <fieldset class="container-fluid">
      <div class="control-group">
          <form:label path="religionCode" cssClass="control-label"
            cssErrorClass="control-label error">
            <spring:message code="religion" />
          </form:label>
          <div class="controls">
            <form:select path="religionCode" cssClass="input-xlarge">
              <form:option value="">&nbsp;</form:option>
              <form:options items="${religions}" itemValue="code"
                itemLabel="description" />
            </form:select>
          </div>
        </div>
        
        <div class="control-group">
          <form:label path="maritalStatusCode" cssClass="control-label"
            cssErrorClass="control-label error">
            <spring:message code="marital_status" />
          </form:label>
          <div class="controls">
            <form:select path="maritalStatusCode" cssClass="input-xlarge">
              <form:option value="">&nbsp;</form:option>
              <form:options items="${maritalStatuses}" itemValue="code"
                itemLabel="description" />
            </form:select>
          </div>
        </div>
        
        
        <div class="control-group">
          <form:label path="childrenCountCode" cssClass="control-label"
            cssErrorClass="control-label error">
            <spring:message code="children" />
          </form:label>
          <div class="controls">
            <form:input id="childrenCount" path="childrenCountCode" cssClass="input-xlarge" />
          </div>
        </div>
        
        <div class="control-group">
          <form:label path="familySizeCode" cssClass="control-label"
            cssErrorClass="control-label error">
            <spring:message code="family_size" />
          </form:label>
          <div class="controls">
            <form:select path="familySizeCode" cssClass="input-xlarge">
              <form:option value="">&nbsp;</form:option>
              <form:options items="${familySizes}" itemValue="code"
                itemLabel="description" />
            </form:select>
          </div>
        </div>
        
        
        <div class="control-group">
          <form:label path="nationalityCode" cssClass="control-label"
            cssErrorClass="control-label error">
            <spring:message code="nationality" />
          </form:label>
          <div class="controls">
            <form:select path="nationalityCode" cssClass="input-xlarge">
              <form:option value="">&nbsp;</form:option>
              <form:options items="${nationalities}" itemValue="code"
                itemLabel="description" />
            </form:select>
          </div>
        </div>
        
        <div class="control-group">
          <form:label path="educationCode" cssClass="control-label"
            cssErrorClass="control-label error">
            <spring:message code="education" />
          </form:label>
          <div class="controls">
            <form:select path="educationCode" cssClass="input-xlarge">
              <form:option value="">&nbsp;</form:option>
              <form:options items="${educations}" itemValue="code"
                itemLabel="description" />
            </form:select>
          </div>
        </div>
        <div class="control-group">
          <spring:message code="mobilephone" var="contact" />
          <form:label path="contact" title="${contact}"
            cssClass="control-label" cssErrorClass="control-label error">
            <span class="required">*</span>${contact}</form:label>
          <div id="contact" class="controls">
            <form:input path="contact" title="Contact" cssClass="input-xlarge" placeHolder="Contact Number"/>
            <input type="button" onclick="addFields()" value="+" />
          </div>
        </div>
        
        <div class="control-group">
          <form:label path="homePhone" cssClass="control-label"
            cssErrorClass="control-label error">
            <spring:message code="home_phone" />
          </form:label>
          <div class="controls">
            <form:input path="homePhone" cssClass="input-xlarge" />
          </div>
        </div>
        
        <%-- <div class="control-group">
          <spring:message code="best_time_to_call" var="bestTimeToCall" />
          <form:label path="bestTimeToCall" title="${bestTimeToCall }"
            cssClass="control-label" cssErrorClass="control-label error">
      ${bestTimeToCall }</form:label>
          <div id="bestTimeToCall" class="controls">
            <form:input path="bestTimeToCall" title="${bestTimeToCall }"
              cssClass="input-xlarge" />
          </div>
        </div> --%>

        <div class="control-group">
          <label for="bestTimeToCall" class="control-label"> <b class="required">*</b><spring:message code="best_time_to_call" /></label>
          <div id="bestTimeToCall" class="controls checkbox">
           <c:forEach items="${bestTimeToCall}" var="item">
             <form:checkbox path="bestTimeToCall" value="${item.code}" label="${item.description}"  data-value="${item.code}" cssClass="checkboxFix" />
           </c:forEach>
          </div>
        </div>
        
        <div class="control-group">
          <spring:message code="personal_email" var="email" />
          <form:label path="email" title="${email}" cssClass="control-label"
            cssErrorClass="control-label error">
            <span class="required">*</span>${email}</form:label>
          <div class="controls">
            <form:input id="email" path="email" title="${email}" cssClass="input-xlarge" />
          </div>
        </div>
        
        <div class="professionalDetails">
          <div class="control-group">
            <form:label path="position" cssClass="control-label"
              cssErrorClass="control-label error">
              <spring:message code="position" />
            </form:label>
            <div class="controls">
              <form:input path="position" cssClass="input-xlarge" />
            </div>
          </div>
        </div>
        <div class="container-fluid category-group">
        <div class="span3">
            <label><spring:message code="interests" />:</label>
            <c:forEach items="${interests}" var="interest">
              <label class="checkbox"> <input name="interestCodes"
                type="checkbox" value="${interest.code}"
                <c:if test="${not empty form.interestCodeMap[interest.code]}">checked</c:if>>
                ${interest.description}
              </label>
            </c:forEach>
          </div>
          </div>
        
        </fieldset>
    </div>


    <div class="tab-pane" id="marketinginfo">
    
      <div class="control-group">
        <label class="control-label"><spring:message code="correspondence_address" /></label>

        <div class="controls">
          <c:forEach items="${correspondenceAddressCodes}" var="item">
              <label class="radio"><form:radiobutton path="correspondenceAddress" value="${item.code}" />${item.description}</label>
          </c:forEach>
          
        </div>
      </div>
    
      <div class="control-group">
        <label class="control-label"><spring:message code="alert_by" /></label>

        <div class="controls">
          <label class="checkbox"> <form:checkbox path="alertBySms" />
            <spring:message code="alertby.sms" />
          </label> <label class="checkbox" id="alertEmail"> <form:checkbox
              path="alertByEmail" /> <spring:message code="email" />
          </label> <%-- <label class="checkbox"> <form:checkbox path="alertByMail" />
            <spring:message code="mail" />
          </label> --%> <label class="checkbox"> <form:checkbox
              path="alertByPhone" /> <spring:message code="phone" />
          </label>
        </div>
      </div>
      
      <div class="control-group">
        <label class="control-label"><spring:message code="informed_about" /></label>

        <div class="controls">
          <c:forEach items="${informedAboutCodes}" var="item">
              <label class="checkbox"><form:checkbox path="informedAbout" data-text="${item.description}" value="${item.code}" />${item.description}
              <c:if test="${item.description == 'OTHERS'}">&nbsp;&nbsp;<input type="text" name="informedAbout" class="informedAboutTxt" disabled="disabled" value="${form.informedAboutOthers}" /></c:if></label>
          </c:forEach>
          
        </div>
      </div>
      
      <div class="control-group">
        <label class="control-label"><spring:message code="interested_products" /></label>

        <div class="controls">
          <c:forEach items="${interestedProductsCodes}" var="item">
              <label class="checkbox"><form:checkbox path="interestedProducts" value="${item.code}" />${item.description}</label>
          </c:forEach>
          
        </div>
      </div>
      
    
    </div>

    <div class="tab-pane" id="accountdata">
    
      <div class="control-group">
        <label class="control-label"><spring:message
            code="member.update.accountno" /></label>

        <div class="controls">
          <span class="bootstrap-input input-xlarge">${form.accountId}</span>
        </div>
      </div>
      <div class="control-group">
        <label class="control-label"><spring:message code="card_no" /></label>

        <div class="controls">
          <span class="bootstrap-input input-xlarge">${form.accountId}</span>
        </div>
      </div>
      
      <div class="control-group">
        <spring:message code="member_type" var="memberType" />
        <form:label path="memberTypeCode" title="${memberType}"
          cssClass="control-label" cssErrorClass="control-label error">${memberType}</form:label>
        <div class="controls">
          <c:forEach var="memType" items="${memberTypes}">
            <c:if test="${memType.code == form.memberTypeCode}">
              <span class="bootstrap-input input-xlarge">${memType.description}</span>
            </c:if>
          </c:forEach>
          <form:hidden path="memberTypeCode" />
          <%-- <form:select path="memberTypeCode" items="${memberTypes}" title="${memberType}" itemValue="code" itemLabel="description"
                   cssClass="input-xlarge" /> --%>
        </div>
      </div>
      
      <div class="control-group">
        <label class="control-label"><spring:message
            code="loyalty_card_issued" /></label>

        <div class="controls">
          <span class="bootstrap-input input-xlarge">${form.loyaltyCardIssued}</span>
        </div>
      </div>
      <div class="control-group">
        <label class="control-label"><spring:message
            code="loyalty_card_expiry" /></label>

        <div class="controls">
          <span class="bootstrap-input input-xlarge">${form.loyaltyCardExpiry}</span>
        </div>
      </div>
    
    </div>
    
    
    
    
    <div class="tab-pane" id="additional">
      <div class="control-group">
        <form:label path="transactionCodeShort" cssClass="control-label"
          cssErrorClass="control-label error">
          <spring:message code="transactionShort" />
        </form:label>
        <div class="controls">
          <form:input path="transactionCodeShort" cssClass="input-xlarge" />
        </div>
      </div>
      <div class="control-group">
        <form:label path="transactionCodeLong1" cssClass="control-label"
          cssErrorClass="control-label error">
          <spring:message code="transactionLong1" />
        </form:label>
        <div class="controls">
          <form:input path="transactionCodeLong1" cssClass="input-xlarge" />
        </div>
      </div>
      <div class="control-group">
        <form:label path="transactionCodeLong2" cssClass="control-label"
          cssErrorClass="control-label error">
          <spring:message code="transactionLong2" />
        </form:label>
        <div class="controls">
          <form:input path="transactionCodeLong2" cssClass="input-xlarge" />
        </div>
      </div>
      <div class="control-group">
        <form:label path="transactionCode1" cssClass="control-label"
          cssErrorClass="control-label error">
          <spring:message code="transaction1" />
        </form:label>
        <div class="controls">
          <form:input path="transactionCode1" cssClass="input-xlarge" />
        </div>
      </div>
      <div class="control-group">
        <form:label path="transactionCode2" cssClass="control-label"
          cssErrorClass="control-label error">
          <spring:message code="transaction2" />
        </form:label>
        <div class="controls">
          <form:input path="transactionCode2" cssClass="input-xlarge" />
        </div>
      </div>
      <div class="control-group">
        <form:label path="transactionCode3" cssClass="control-label"
          cssErrorClass="control-label error">
          <spring:message code="transaction3" />
        </form:label>
        <div class="controls">
          <form:input path="transactionCode3" cssClass="input-xlarge" />
        </div>
      </div>
      <div class="control-group">
        <form:label path="transactionCode4" cssClass="control-label"
          cssErrorClass="control-label error">
          <spring:message code="transaction4" />
        </form:label>
        <div class="controls">
          <form:input path="transactionCode4" cssClass="input-xlarge" />
        </div>
      </div>
    </div>

    <c:if test="${isPrimary}">
    <div class="tab-pane" id="cards">
    </div>
    </c:if>

    <div class="form-actions">
      <button type="submit" class="btn btn-primary">
        <spring:message code="button_submit" />
      </button>
      <a class="btn" href="<spring:url value="/"/>"><spring:message
          code="button_cancel" /></a>
    </div>
