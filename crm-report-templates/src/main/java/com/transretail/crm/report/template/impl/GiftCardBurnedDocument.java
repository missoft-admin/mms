package com.transretail.crm.report.template.impl;

import java.math.BigDecimal;
import java.text.NumberFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;

import org.joda.time.DateTime;
import org.joda.time.LocalDate;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.MessageSourceAware;
import org.springframework.context.support.MessageSourceAccessor;
import org.springframework.stereotype.Service;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.transretail.crm.common.reporting.jasper.ClasspathFileResolver;
import com.transretail.crm.common.reporting.jasper.DefaultJRProcessor;
import com.transretail.crm.common.reporting.jasper.JRProcessor;
import com.transretail.crm.core.entity.enums.ReportType;
import com.transretail.crm.core.entity.lookup.LookupDetail;
import com.transretail.crm.core.service.CodePropertiesService;
import com.transretail.crm.core.service.LookupService;
import com.transretail.crm.core.service.StoreService;
import com.transretail.crm.giftcard.dto.GiftCardBurnCardDto;
import com.transretail.crm.giftcard.dto.GiftCardInventoryDto;
import com.transretail.crm.giftcard.dto.GiftCardInventoryStockDto;
import com.transretail.crm.giftcard.entity.GiftCardBurnCard;
import com.transretail.crm.giftcard.repo.GiftCardBurnCardRepo;
import com.transretail.crm.giftcard.repo.GiftCardInventoryStockRepo;
import com.transretail.crm.giftcard.service.GiftCardBurnCardService;
import com.transretail.crm.giftcard.service.GiftCardInventoryStockService;
import com.transretail.crm.giftcard.entity.GiftCardInventoryStock;
import com.transretail.crm.giftcard.entity.QGiftCardBurnCard;
import com.transretail.crm.giftcard.entity.QGiftCardInventoryStock;
import com.transretail.crm.giftcard.entity.support.GiftCardInventoryStatus;
import com.transretail.crm.report.template.FilterField;

/**
 * @author ftopico
 */
@Service("giftCardBurnedDocument")
public class GiftCardBurnedDocument implements MessageSourceAware {
    
    @Autowired
    private GiftCardBurnCardRepo giftCardBurnCardRepo;
    @Autowired
    private GiftCardInventoryStockRepo giftCardInventoryStockRepo;
    @Autowired
    private GiftCardBurnCardService giftCardBurnCardService;
    @Autowired
    private GiftCardInventoryStockService giftCardInventoryStockService;
    @Autowired
    private LookupService lookupService;
    @Autowired
    private CodePropertiesService codePropertiesService;
    @Autowired
    private StoreService storeService;

    protected static final String TEMPLATE_NAME = "Burned Gift Card Document";
    protected static final String REPORT_NAME = "reports/gift_card_burned_document.jasper";
    //Header Params
    private static final String PARAM_BURN_NO = "burnNo";
    private static final String PARAM_STORE = "store";
    private static final String PARAM_DATE_FILED = "dateFiled";
    private static final String PARAM_FILED_BY = "filedBy";
    private static final String PARAM_DATE_BURNED = "burnDate";
    private static final String PARAM_APPROVED_BY = "approvedBy";
    private static final String PARAM_QUANTITY = "quantity";
    private static final String PARAM_REASON = "reason";
    private static final String PARAM_IS_EMPTY = "isEmpty";
    
    private static final DateTimeFormatter FULL_DATE_PATTERN = DateTimeFormat.forPattern("MMMM d, yyyy");
    private MessageSourceAccessor messageSource;

    public ReportType[] getReportTypes() {
        return ReportType.values();
    }
    
    @Override
    public void setMessageSource(MessageSource messageSource) {
        this.messageSource = new MessageSourceAccessor(messageSource);
    }
    
    public String getName() {
        return TEMPLATE_NAME;
    }
    
    public String getDescription() {
        return messageSource.getMessage("title_document_burncard");
    }
    
    public Set<FilterField> getFilters() {
        return null;
    }
    
    public JRProcessor createJrProcessor(String burnNo) {
        
        GiftCardBurnCardDto gcBurnCardRequest = giftCardBurnCardService.getBurnCardRequest(burnNo);
        
        QGiftCardBurnCard qGiftCardBurnCard = QGiftCardBurnCard.giftCardBurnCard;
        Iterable<GiftCardBurnCard> listOfGiftCardBurnCard = giftCardBurnCardRepo.findAll(qGiftCardBurnCard.burnNo.eq(burnNo));
        Long quantity = giftCardBurnCardRepo.count(qGiftCardBurnCard.burnNo.eq(burnNo));
        
        QGiftCardInventoryStock qGiftCardInventoryStock = QGiftCardInventoryStock.giftCardInventoryStock;
        
        Map<String, Object> parameters = Maps.newHashMap();
        DateTimeFormatter dateStringFormat = DateTimeFormat.forPattern("dd-MMMM-yyyy");
        parameters.put(PARAM_BURN_NO, gcBurnCardRequest.getBurnNo());
        parameters.put(PARAM_STORE, storeService.findById(gcBurnCardRequest.getStoreId()).getCodeAndName());
        parameters.put(PARAM_DATE_FILED, gcBurnCardRequest.getDateFiled());
        parameters.put(PARAM_FILED_BY, gcBurnCardRequest.getFiledBy());
        
        parameters.put(PARAM_DATE_BURNED, FULL_DATE_PATTERN.print(listOfGiftCardBurnCard.iterator().next().getInventory().getLastUpdated()));
        parameters.put(PARAM_APPROVED_BY, gcBurnCardRequest.getApprovedBy());
        parameters.put(PARAM_QUANTITY, quantity);
        LookupDetail detail = lookupService.getDetailByCode(gcBurnCardRequest.getBurnReason());
        if (detail != null)
            parameters.put(PARAM_REASON, detail.getDescription());
        
        List<ReportBean> beansDataSource = Lists.newArrayList();
        if (!listOfGiftCardBurnCard.iterator().hasNext()) {
            parameters.put(PARAM_IS_EMPTY, true);
            beansDataSource.add(new ReportBean());
        } else {
            for(GiftCardBurnCard dto : listOfGiftCardBurnCard) {
                beansDataSource.add(new ReportBean(dto.getInventory().getSeries(), dto.getInventory().getBarcode(), dto.getInventory().getProductCode(), dto.getInventory().getProductName(), 
                        NumberFormat.getNumberInstance(Locale.US).format(dto.getInventory().getFaceValue().longValueExact()))); 
            }
        }
        DefaultJRProcessor jrProcessor = new DefaultJRProcessor(REPORT_NAME, beansDataSource);
        jrProcessor.addParameters(parameters);
        jrProcessor.setFileResolver(new ClasspathFileResolver("reports"));
        return jrProcessor;
    }
    
    public static class ReportBean {
        private Long series;
        private String barcode;
        private String productCode;
        private String productDescription;
        private String faceValue;
        
        public ReportBean() {}
        
        public ReportBean(Long series, String barcode, String productCode, String productDescription, String faceValue) {
            super();
            this.series = series;
            this.barcode = barcode;
            this.productCode = productCode;
            this.productDescription = productDescription;
            this.faceValue = faceValue;
        }

        public Long getSeries() {
            return series;
        }

        public void setSeries(Long series) {
            this.series = series;
        }

        public String getBarcode() {
            return barcode;
        }

        public void setBarcode(String barcode) {
            this.barcode = barcode;
        }

        public String getProductDescription() {
            return productDescription;
        }

        public void setProductDescription(String productDescription) {
            this.productDescription = productDescription;
        }

        public String getFaceValue() {
            return faceValue;
        }

        public void setFaceValue(String faceValue) {
            this.faceValue = faceValue;
        }

        public String getProductCode() {
            return productCode;
        }

        public void setProductCode(String productCode) {
            this.productCode = productCode;
        }

    }
}
