<%@include file="../../../common/taglibs.jsp"%>

          <div style="width: 100%; overflow-x: auto;">
          <table class="table table-condensed table-compressed table-striped">
            <thead>
              <tr>
                <th><spring:message code="gc.so.item.productname" /></th>
                <th><spring:message code="gc.so.item.facevalue" /></th>
                <th><spring:message code="gc.so.item.qty" /></th>
                <th><spring:message code="gc.so.item.faceamt" /></th>
                <th><spring:message code="gc.so.item.printfee" /></th>
                <th><spring:message code="gc.so.lbl.alloc" /></th>
              </tr>
            </thead>
            <tbody>            
              <c:forEach var="item" items="${salesOrder.items}">
              <tr>
                <td class="table-data-01">${item.productId} - ${item.productDesc}</td>
                <td><fmt:formatNumber type="number" pattern="#,##0" value="${item.faceValue}" /></td>
                <td>${item.quantity}</td>
                <td><fmt:formatNumber type="number" pattern="#,##0" value="${item.faceAmount}" /></td>
                <td><fmt:formatNumber type="number" pattern="#,##0" value="${item.printFee}" /></td>
                <td>
                  <div id="${item.id}" data-product-id="${item.productId}" class="allocations table-col-xlarge">
                    <%-- <c:if test="${not empty item.gcAlloc}">
                      ${item.gcAlloc.barcodeStart} - ${item.gcAlloc.barcodeEnd} (${item.gcAlloc.quantity})
                    </c:if> --%>
                    <c:if test="${not empty item.gcAllocs}">
                    <c:forEach var="gcAlloc" items="${item.gcAllocs}">
                      <div>${gcAlloc.barcodeStart} - ${gcAlloc.barcodeEnd} (${gcAlloc.quantity})</div>
                    </c:forEach>
                    </c:if>
                  </div>
                </td>
              </tr>
              </c:forEach>
            </tbody>
          </table>
          </div>
