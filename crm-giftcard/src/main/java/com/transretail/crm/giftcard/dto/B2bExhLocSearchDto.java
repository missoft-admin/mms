package com.transretail.crm.giftcard.dto;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.joda.time.LocalDate;

import com.mysema.query.types.expr.BooleanExpression;
import com.transretail.crm.common.service.dto.request.AbstractSearchFormDto;
import com.transretail.crm.giftcard.entity.QB2BExhibitionDiscount;
import com.transretail.crm.giftcard.entity.QB2BExhibitionLocation;

/**
 *
 */
public class B2bExhLocSearchDto extends AbstractSearchFormDto {
	
	private String locationName;
	private LocalDate startDate;
	private LocalDate endDate;
	
	private Long locId;

	@Override
	public BooleanExpression createSearchExpression() {
		List<BooleanExpression> expressions = new ArrayList<BooleanExpression>();
		
		if(locId != null) {
			QB2BExhibitionDiscount qDisc = QB2BExhibitionDiscount.b2BExhibitionDiscount;
			expressions.add(qDisc.location.id.eq(locId));
		} else {
			QB2BExhibitionLocation qLoc = QB2BExhibitionLocation.b2BExhibitionLocation;
			if(StringUtils.isNotBlank(locationName)) {
				expressions.add(qLoc.name.containsIgnoreCase(locationName));
			}
			if(startDate != null) {
				expressions.add(qLoc.startDate.loe(startDate));
			}
			if(endDate != null) {
				expressions.add(qLoc.endDate.goe(endDate));
			}
		}
		return BooleanExpression.allOf(expressions.toArray(new BooleanExpression[expressions.size()]));
		
	}

	public String getLocationName() {
		return locationName;
	}

	public void setLocationName(String locationName) {
		this.locationName = locationName;
	}

	public LocalDate getStartDate() {
		return startDate;
	}

	public void setStartDate(LocalDate startDate) {
		this.startDate = startDate;
	}

	public LocalDate getEndDate() {
		return endDate;
	}

	public void setEndDate(LocalDate endDate) {
		this.endDate = endDate;
	}

	public Long getLocId() {
		return locId;
	}

	public void setLocId(Long locId) {
		this.locId = locId;
	}
	
	
	
	
}
