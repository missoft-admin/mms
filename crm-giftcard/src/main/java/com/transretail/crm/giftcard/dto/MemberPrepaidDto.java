package com.transretail.crm.giftcard.dto;

import java.util.List;

/**
 * description here.
 *
 * @author Vincent Gerard Tan
 */
public class MemberPrepaidDto {
    private Long totalBalance;

    private List<GiftCardInventoryDto> giftCards;

    public Long getTotalBalance() {
        return totalBalance;
    }

    public void setTotalBalance(Long totalBalance) {
        this.totalBalance = totalBalance;
    }

    public List<GiftCardInventoryDto> getGiftCards() {
        return giftCards;
    }

    public void setGiftCards(List<GiftCardInventoryDto> giftCards) {
        this.giftCards = giftCards;
    }
}
