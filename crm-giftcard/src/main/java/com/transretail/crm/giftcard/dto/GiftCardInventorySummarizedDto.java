package com.transretail.crm.giftcard.dto;

import java.math.BigDecimal;

import com.transretail.crm.giftcard.entity.support.GiftCardInventoryStatus;

/**
 * @author Allan G. Ramirez (agramirez@exist.com)
 */
public class GiftCardInventorySummarizedDto {
    private Long productCode;
    private String productName;
    private BigDecimal faceValue;
    private String location;
    private String allocateTo;
    private GiftCardInventoryStatus status;
    private int batchNo;
    private long quantity;
    private long seriesFrom;
    private long seriesTo;
    private String locationCode;
    private String allocateToCode;

    public GiftCardInventorySummarizedDto() {

    }

    public GiftCardInventorySummarizedDto(Long productCode, String productName, BigDecimal faceValue, String location,
        String allocateTo, GiftCardInventoryStatus status, Long quantity, Integer batchNo, long seriesFrom, long seriesTo,
        String locationCode, String allocateToCode) {
        this.productCode = productCode;
        this.productName = productName;
        this.faceValue = faceValue;
        this.location = location;
        this.allocateTo = allocateTo;
        this.status = status;
        this.quantity = quantity != null ? quantity : 0L;
        this.batchNo = batchNo;
        this.seriesFrom = seriesFrom;
        this.seriesTo = seriesTo;
        this.locationCode = locationCode;
        this.allocateToCode = allocateToCode;
    }
    
    public GiftCardInventorySummarizedDto(Long productCode, String productName, BigDecimal faceValue,
            GiftCardInventoryStatus status, Long quantity, Integer batchNo, long seriesFrom, long seriesTo,
            String locationCode, String allocateToCode) {
            this.productCode = productCode;
            this.productName = productName;
            this.faceValue = faceValue;
            this.status = status;
            this.quantity = quantity != null ? quantity : 0L;
            this.batchNo = batchNo;
            this.seriesFrom = seriesFrom;
            this.seriesTo = seriesTo;
            this.locationCode = locationCode;
            this.allocateToCode = allocateToCode;
        }

    public Long getProductCode() {
        return productCode;
    }

    public void setProductCode(Long productCode) {
        this.productCode = productCode;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public BigDecimal getFaceValue() {
        return faceValue;
    }

    public void setFaceValue(BigDecimal faceValue) {
        this.faceValue = faceValue;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getAllocateTo() {
        return allocateTo;
    }

    public void setAllocateTo(String allocateTo) {
        this.allocateTo = allocateTo;
    }

    public GiftCardInventoryStatus getStatus() {
        return status;
    }

    public void setStatus(GiftCardInventoryStatus status) {
        this.status = status;
    }

    public int getBatchNo() {
        return batchNo;
    }

    public void setBatchNo(int batchNo) {
        this.batchNo = batchNo;
    }

    public long getQuantity() {
        return quantity;
    }

    public void setQuantity(long quantity) {
        this.quantity = quantity;
    }

    public long getSeriesFrom() {
        return seriesFrom;
    }

    public void setSeriesFrom(long seriesFrom) {
        this.seriesFrom = seriesFrom;
    }

    public long getSeriesTo() {
        return seriesTo;
    }

    public void setSeriesTo(long seriesTo) {
        this.seriesTo = seriesTo;
    }

    public String getLocationCode() {
        return locationCode;
    }

    public void setLocationCode(String locationCode) {
        this.locationCode = locationCode;
    }

    public String getAllocateToCode() {
        return allocateToCode;
    }

    public void setAllocateToCode(String allocateToCode) {
        this.allocateToCode = allocateToCode;
    }
}
