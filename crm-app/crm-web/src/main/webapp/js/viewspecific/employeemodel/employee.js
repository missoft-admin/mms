var CONTAINER_ERROR = "#contentError > div";
var ID_EMPLOYEE_FORM = "employeeModel";
var ID_SEARCHPURCHASETXN_FORM = "searchPurchaseTxnForm";
var ID_MEMBER_UPDATEFORM = "customerModel";
var MSG_ACCOUNTIDTOBEGENERATED = "";

$(document).ready(function() {
	$list = $("#list_employee");
	
	initForm();
	initButtons();
	initDialogs();
	
	initDataTable();

	function initDataTable() {
	  
	  var headers = { 
		username: $list.data("hdrUsername"),
	    member: $list.data("hdrMember"),
	    store: $list.data("hdrStore"),
	    status: $list.data("hdrStatus"),
	    accountNo: $list.data("hdrAccountno"),
	    txnAmount: $list.data("hdrTxnAmount")
	  };
	  
	  $list.ajaxDataTable({
	    'autoload'  : true,
	    'ajaxSource' : $list.data("url"),
	    'columnHeaders' : [ 
             headers['username'],
             headers['accountNo'],
	         headers['member'],
	         headers['store'],
	         headers['status'],
	         headers['txnAmount'],
	         ""
	    ],
	    'modelFields' : [
	     {name : 'username', sortable : true},
	     {name : 'accountId', sortable : true},
         {name : 'firstName', sortable : true, fieldCellRenderer : function (data, type, row) {
	           return row.formattedMemberName;
         }},
         {name : 'registeredStore', sortable : true, fieldCellRenderer : function (data, type, row) {
        	 if(row.registeredStore != null &&
        			 
        			 row.registeredStore != "")
        		 return row.registeredStore + " - " + row.registeredStoreName;
        	 else
        		 return "";
         }},
	     {name : 'accountStatus', sortable : true},
	     {name : 'totalTransactionAmount', sortable : false, fieldCellRenderer : function (data, type, row) {
	           return row.txnAmount;
         }},
         {customCell : function ( innerData, sSpecific, json ) {
	           if ( sSpecific == 'display' ) {
	        	   var btnHtml = $("#viewPntsBtn").html();
	        	   btnHtml += $("#viewTxnBtn").html();
	        	   btnHtml += $("#updateBtn").html();
	        	   //btnHtml += $("#deleteBtn").html();
	               var rg = new RegExp("%ACCOUNTID%", 'g');
	      		   btnHtml = btnHtml.replace(rg, json.accountId);
	      		   rg = new RegExp("%ITEMID%", 'g');
	      		   btnHtml = btnHtml.replace(rg, json.id);
	      		   rg = new RegExp("%CURRENTTOTAL%", 'g');
	      		   btnHtml = btnHtml.replace(rg, json.currentTotalTransactionAmount);
	               return btnHtml;
	             } else {
	               return '';
	             }
	             }
	        },
	     ]
	  }).on("click", ".updateEmpLnk", updateEmployee)
	  .on("click", ".viewTxnLnk", viewTransaction);
	  
	  initSearchForm($list);
	}
	
	function initSearchForm(dataTable) {
		$("#search_submit").click(function(e) {
			e.preventDefault();
			if($("#search_employeeField").val() != "")
				$("#search_employeeField").attr("name", $("#search_employeeCriteria").val());
			else
				$("#search_employeeField").removeAttr("name");
			
			var formDataObj = $("#searchEmployeeModel").toObject( { mode : 'first', skipEmpty : true } );
			$btnSearch = $(this);
			$btnSearch.prop('disabled', true);
//			console.log(formDataObj);

			dataTable.ajaxDataTable( 
				'search', 
				formDataObj, 
				function() {
					$btnSearch.prop( 'disabled', false );
			});
		});
		
		$("#searchEmployeeModel #status").change(function() {
			$("#search_submit").click();
		});
		
		$("#searchEmployeeModel #searchClearButton").click(function(e) {
			e.preventDefault();
			$("#searchEmployeeModel #search_employeeCriteria option:first").attr('selected','selected');
			$("#searchEmployeeModel #search_employeeField").val("");
			$("#searchEmployeeModel .pointsSearchDropdown").val("");
			$("#search_employeeField").removeAttr("name");
			$("#search_submit").click();
		});
	}
	
	function initForm() {
		$("form#memberForm").attr("action", $("form#memberForm").data("action") );
	}
	
	function initButtons() {
		$("#createEmpLnk").click(createEmployee); 
		$(".updateEmpLnk").click(updateEmployee);
		$(".viewTxnLnk").click(viewTransaction);
		
    $("#printList").click(function(e){
      e.preventDefault();
      if($("#search_employeeField").val() != "")
        $("#search_employeeField").attr("name", $("#search_employeeCriteria").val());
      else
        $("#search_employeeField").removeAttr("name");
      var dataObject = $("#searchEmployeeModel").serialize();
      console.log($("#printList").data("url"));
      $.get($("#printList").data("url"),dataObject , function(response) {
        $("#form_export").modal('show').html(response);
        $("#form_export").on('hidden.bs.modal', function() {
          $("#form_export").empty();
        });
      }, "html");
    });
	}
	
	function initDialogs() {
		$("#viewTxnDialog").modal({
			show: false,
			keyboard:	false, 
			backdrop: "static" 
		});
	}
	
	function createEmployee(e) {
		e.preventDefault();
		dialog = memberFormDialog;
		populateMember(function(){saveMember("/new", $(this).data("query"))}, "/populate/" + null, null);
	}
	
	function updateEmployee(e) {
		e.preventDefault();
		var memberId = $(this).data("memberId");
		var query = $(this).data("query");
		populateMember(function(){saveMember("/update/" + memberId, query)}, "/populate/" + memberId, false);
	}
	
	function viewTransaction(e) {
		e.preventDefault();
		$("#viewTxnDialog").modal("show");
		$("#contentPrint").data("accountId", $(this).data("accountId"));
		reloadTxnTable($(this).data("accountId"));
		var currentTotal = 0;
		if($(this).data("currentTxnTotal") != null) 
			currentTotal = $(this).data("currentTxnTotal");
		$(".currentTxnTotal").text(currentTotal);
//		ajaxViewTransaction($(this).data("accountId") + "/" + null + "/" + null, $(this).data("contextPath"), $(this).data("controllerPath"));
	}
	
	function ajaxViewTransaction(employeeId, contextPath, controllerPath) {
		$.ajax({
			type 	: "GET",
			url 	: contextPath + "/employeemodels/show/transaction/" + controllerPath + employeeId,
			cache: false,
			dataType: "html",
			success : function(inResponse) {
				$("#viewTxnDialog").html(inResponse);
				$("#viewTxnDialog").modal("show");
			},
			error : function(jqXHR, textStatus, errorThrown) {
				$( CONTAINER_ERROR ).html("Error: " + errorThrown);
			}
		});
	}

	initListPurchaseItems();
	function initListPurchaseItems() {
		$( "#content_itemList" ).on( "hide", function(e) {
			$("#viewTxnDialog").modal( "show" );
		});
		$("#viewTxnDialog").on( "show", function(e) {
			listItems();
		});
	}
	function listItems() {
	  var $theItemListLinks = $( ".link_itemList" );
	  $.each( $theItemListLinks, function( idx, theItemListLink ) {
	    $( this ).click( function(e) {
	    	$( "#list_item" ).data( "txn-no", $( this ).data( "txn-no" ) );
	    	$( "#content_transactionNo" ).html( $( this ).data( "txn-no" ) );
	    	$("#viewTxnDialog").modal( "hide" );
	    	$( "#content_itemList" ).modal( "show" );
	    });
	  });
	}
});


