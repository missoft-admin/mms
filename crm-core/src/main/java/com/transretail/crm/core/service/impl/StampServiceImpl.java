package com.transretail.crm.core.service.impl;

import com.transretail.crm.core.entity.StampTxnModel;
import com.transretail.crm.core.repo.StampTxnHeaderRepo;
import com.transretail.crm.core.repo.StampTxnModelRepo;
import com.transretail.crm.core.service.StampService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * @author Mike de Guzman
 */
@Service("stampService")
@Transactional
public class StampServiceImpl implements StampService {

    @Autowired
    private StampTxnHeaderRepo stampTxnHeaderRepo;

    @Autowired
    private StampTxnModelRepo stampTxnModelRepo;

    @Override
    public List<StampTxnModel> retrieveStampsByTransactionNo(String transactionNo) {
        return stampTxnModelRepo.findByTransactionHeader_TransactionNo(transactionNo);
    }

    @Override
    public StampTxnModel saveStamp(StampTxnModel stampTxnModel) {
        stampTxnHeaderRepo.save(stampTxnModel.getTransactionHeader());
        return stampTxnModelRepo.save(stampTxnModel);
    }

}
