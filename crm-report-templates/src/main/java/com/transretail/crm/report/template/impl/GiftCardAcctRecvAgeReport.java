package com.transretail.crm.report.template.impl;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.mysema.query.jpa.JPASubQuery;
import com.mysema.query.jpa.impl.JPAQuery;
import com.mysema.query.types.ConstructorExpression;
import com.mysema.query.types.OrderSpecifier;
import com.mysema.query.types.expr.BooleanExpression;
import com.mysema.query.types.expr.CaseBuilder;
import com.mysema.query.types.expr.NumberExpression;
import com.transretail.crm.common.reporting.jasper.ClasspathFileResolver;
import com.transretail.crm.common.reporting.jasper.DefaultJRProcessor;
import com.transretail.crm.common.reporting.jasper.JRProcessor;
import com.transretail.crm.core.entity.QPeopleSoftStore;
import com.transretail.crm.core.entity.enums.ReportType;
import com.transretail.crm.core.entity.lookup.LookupDetail;
import com.transretail.crm.core.entity.lookup.QLookupDetail;
import com.transretail.crm.core.security.util.UserUtil;
import com.transretail.crm.core.service.CodePropertiesService;
import com.transretail.crm.core.service.LookupService;
import com.transretail.crm.core.service.StoreService;
import com.transretail.crm.giftcard.entity.QGiftCardCustomerProfile;
import com.transretail.crm.giftcard.entity.QReturnRecord;
import com.transretail.crm.giftcard.entity.QSalesOrder;
import com.transretail.crm.giftcard.entity.QSalesOrderPaymentInfo;
import com.transretail.crm.giftcard.entity.support.PaymentInfoStatus;
import com.transretail.crm.giftcard.entity.support.SalesOrderStatus;
import com.transretail.crm.report.template.CommonReportFilter;
import com.transretail.crm.report.template.FilterField;
import com.transretail.crm.report.template.ReportTemplate;
import java.math.BigDecimal;
import java.util.*;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.EnumUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.time.DateUtils;
import org.joda.time.Interval;
import org.joda.time.LocalDate;
import org.joda.time.LocalDateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.MessageSourceAware;
import org.springframework.context.support.MessageSourceAccessor;
import org.springframework.stereotype.Service;

@Service("giftCardAcctRecvAgeReport")
public final class GiftCardAcctRecvAgeReport implements ReportTemplate, MessageSourceAware {

    private MessageSourceAccessor messageSource;
    @Autowired
    StoreService storeService;
    @Autowired
    CodePropertiesService codePropertiesService;
    @Autowired
    LookupService lookupService;
    @PersistenceContext
    protected EntityManager em;

    private static final String DATE_FORMAT_FILTER = "dd-MM-yyyy";
    private static final String DATE_FORMAT_2 = "d MMM yyyy";
    private static final String REPORTFILTER_ALL = "";

    private static enum ReportFilter {

        REGION, ACTIVATION_STAT, ALL
    };

    private static enum SoActivationStat {

        ACTIVATED, PARTIALLY_ACTIVATED, NON_ACTIVATED
    };

    @Override
    public void setMessageSource(MessageSource messageSource) {
        this.messageSource = new MessageSourceAccessor(messageSource);
    }

    @Override
    public ReportType[] getReportTypes() {
        return new ReportType[]{ReportType.EXCEL};
    }

    @Override
    public String getName() {
        return "Accounts Receivable Age";
    }

    @Override
    public String getDescription() {
        return getName();
    }

    @Override
    public Set<FilterField> getFilters() {
        Set<FilterField> filters = new LinkedHashSet<FilterField>(2);
        filters.add(FilterField.createDateField(CommonReportFilter.DATE_FROM,
                messageSource.getMessage("report.misc.datefrom"), DATE_FORMAT_FILTER, FilterField.DateTypeOption.PLAIN, true));
        filters.add(FilterField.createDateField(CommonReportFilter.DATE_TO,
                messageSource.getMessage("report.misc.dateto"), DATE_FORMAT_FILTER, FilterField.DateTypeOption.PLAIN, true));

        Map<String, String> filter = Maps.newLinkedHashMap();
        filter.put(ReportFilter.ALL.name(), REPORTFILTER_ALL);
        List<LookupDetail> brs = lookupService.getActiveDetailsByHeaderCode(
                codePropertiesService.getHeaderBusinessRegion(), new OrderSpecifier<?>[]{QLookupDetail.lookupDetail.description.asc()});
        for (LookupDetail br : brs) {
            filter.put(br.getCode(), br.getDescription());
        }
        filters.add(FilterField.createDropdownField(ReportFilter.REGION.name(), messageSource.getMessage("report.gc.ordersummary.region"), filter));

        filter = Maps.newLinkedHashMap();
        filter.put(ReportFilter.ALL.name(), REPORTFILTER_ALL);
        for (SoActivationStat stat : SoActivationStat.values()) {
            filter.put(stat.name(), messageSource.getMessage("report.gc.actvnstat." + stat.name().toLowerCase()));
        }
        filters.add(FilterField.createDropdownField(ReportFilter.ACTIVATION_STAT.name(), messageSource.getMessage("report.gc.activationstat"), filter));

        return filters;
    }

    @Override
    public boolean canView(Set<String> perms) {
        /*if ( CollectionUtils.isNotEmpty( perms ) ) { for ( String perm : perms ) { if ( perm.equalsIgnoreCase( PERMISSION ) ) { return true; } } }*/
        return true;
    }

    @Override
    public JRProcessor createJrProcessor(Map<String, String> map) {
        Map<String, Date> dateRange = getDateRange(map);
        LocalDate dateFrom = new LocalDate(dateRange.get(CommonReportFilter.DATE_FROM).getTime());
        LocalDate dateTo = new LocalDate(dateRange.get(CommonReportFilter.DATE_TO).getTime());
        String region = map.get(ReportFilter.REGION.name());
        List<String> regionStoreCodes = null;
        if (StringUtils.isNotBlank(region)) {
            QPeopleSoftStore psoftStore = QPeopleSoftStore.peopleSoftStore;
            regionStoreCodes = new JPAQuery(em).from(psoftStore).where(psoftStore.businessRegion.code.equalsIgnoreCase(region)).list(psoftStore.store.code.toLowerCase());
        }
        String activationStatus = map.get(ReportFilter.ACTIVATION_STAT.name());

        QSalesOrderPaymentInfo qPi = QSalesOrderPaymentInfo.salesOrderPaymentInfo;
        QGiftCardCustomerProfile qCp = QGiftCardCustomerProfile.giftCardCustomerProfile;
        QSalesOrder qSo = QSalesOrder.salesOrder;
        JPAQuery query = new JPAQuery(em);
        query.from(qPi)
                .leftJoin(qPi.order, qSo)
                .leftJoin(qSo.customer, qCp)
                .where(BooleanExpression.allOf(qPi.status.eq(PaymentInfoStatus.VERIFIED),
                                StringUtils.isBlank(region) || region.equalsIgnoreCase(ReportFilter.ALL.name())
                                        ? null : qSo.paymentStore.toLowerCase().in(regionStoreCodes)),
                        StringUtils.isBlank(activationStatus) || activationStatus.equalsIgnoreCase(ReportFilter.ALL.name())
                                ? null : createActvnStatExpr(EnumUtils.getEnum(SoActivationStat.class, activationStatus)))
                .groupBy(qCp.name, qCp.customerId, qSo.orderNo, qSo.orderDate)
                .orderBy(qCp.name.asc(), qSo.orderDate.desc());

        List<NumberExpression<BigDecimal>> nex = Lists.newArrayList();
        LocalDate currentDate = LocalDate.now();
        //TODO: temp listing of intervals
        List<Interval> intervals = Lists.newArrayList();
        int[] ints = new int[]{0, 1, 3, 6, 9, 12, 24, 36, 48, 56};
        for (int i = 0; i < ints.length; i++) {
            Interval interval = new Interval(
                    (i + 1 != ints.length) ? DateUtils.addMilliseconds(currentDate.minusMonths(ints[i + 1]).toDate(), 1).getTime()
                            : 0, currentDate.minusMonths(ints[i]).toDate().getTime());
            intervals.add(interval);

            nex.add(new CaseBuilder()
                    .when(qPi.paymentDate.between(interval.getStart().toLocalDate(), interval.getEnd().toLocalDate())
                            .and(qPi.paymentDate.between(dateFrom, dateTo)))
                    .then(qPi.paymentAmount).otherwise(new BigDecimal(0)).sum());
        }

        List<AccountsReceivableAge.AccountsReceivable> acctRecvs = query.list(ConstructorExpression.create(AccountsReceivableAge.AccountsReceivable.class,
                qCp.customerId, qCp.name,  qSo.orderNo, qSo.orderDate,
                //qPi.paymentAmount.sum(),
                new CaseBuilder()
                .when(qPi.paymentDate.between(dateFrom, dateTo))
                .then(qPi.paymentAmount).otherwise(new BigDecimal(0)).sum(),
                nex.get(0), nex.get(1), nex.get(2), nex.get(3), nex.get(4), nex.get(5), nex.get(6), nex.get(7), nex.get(8), nex.get(9)));

//        List<AccountsReceivableAge> acctRecvAges = Lists.newArrayList();
//        AccountsReceivableAge acctRecvAge = null;
//        for (AccountsReceivableAge.AccountsReceivable acctRecv : acctRecvs) {
//            if (null == acctRecvAge || !StringUtils.equalsIgnoreCase(acctRecvAge.getCxNumber(), acctRecv.getCxNumber())) {
//                acctRecvAge = new AccountsReceivableAge(acctRecv.getCxName(), acctRecv.getCxNumber(), null);
//                acctRecvAges.add(acctRecvAge);
//            }
//            if (CollectionUtils.isEmpty(acctRecvAge.getPaymentList())) {
//                acctRecvAge.setPaymentList(new ArrayList<GiftCardAcctRecvAgeReport.AccountsReceivableAge.AccountsReceivable>());
//            }
//            acctRecvAge.getPaymentList().add(acctRecv);
//
//            if (null == acctRecvAge.getPaymentListTotals()) {
//                acctRecvAge.setPaymentListTotals(new AccountsReceivableAge.AccountsReceivable(acctRecv));
//            } else {
//                AccountsReceivableAge.AccountsReceivable paymentTotal = acctRecvAge.getPaymentListTotals();
//                paymentTotal.setPaymentTotal(paymentTotal.getPaymentTotal().add(acctRecv.getPaymentTotal()));
//                paymentTotal.setPayment00(paymentTotal.getPayment00().add(acctRecv.getPayment00()));
//                paymentTotal.setPayment01(paymentTotal.getPayment01().add(acctRecv.getPayment01()));
//                paymentTotal.setPayment02(paymentTotal.getPayment02().add(acctRecv.getPayment02()));
//                paymentTotal.setPayment03(paymentTotal.getPayment03().add(acctRecv.getPayment03()));
//                paymentTotal.setPayment04(paymentTotal.getPayment04().add(acctRecv.getPayment04()));
//                paymentTotal.setPayment05(paymentTotal.getPayment05().add(acctRecv.getPayment05()));
//                paymentTotal.setPayment06(paymentTotal.getPayment06().add(acctRecv.getPayment06()));
//                paymentTotal.setPayment07(paymentTotal.getPayment07().add(acctRecv.getPayment07()));
//                paymentTotal.setPayment08(paymentTotal.getPayment08().add(acctRecv.getPayment08()));
//                paymentTotal.setPayment09(paymentTotal.getPayment09().add(acctRecv.getPayment09()));
//        }

        /*Map<String, Group> acctRecvAgeMap = Maps.newHashMap();
         for ( Interval interval: intervals ) {
         query.where( qPi.paymentDate.between( interval.getStart().toLocalDate(), interval.getEnd().toLocalDate() ) );
         acctRecvAgeMap = query.transform( GroupBy.groupBy( qPi.order.customer.name ).as( 
         GroupBy.set( qPi.paymentAmount, qPi.order.orderNo, qPi.paymentAmount.sum(),
         nex.get(0), nex.get(1), nex.get(2), nex.get(3), nex.get(4), nex.get(5), nex.get(6), nex.get(7), nex.get(8), nex.get(9) ) ) );
         }*/
        Map<String, Object> params = new HashMap<String, Object>();
        DateTimeFormatter df = DateTimeFormat.forPattern(DATE_FORMAT_2);
        params.put("dateFrom", df.print(dateFrom));
        params.put("dateTo", df.print(dateTo));
        params.put("printedBy", UserUtil.getCurrentUser().getUsername());
        params.put("printDate", df.print(new LocalDateTime(currentDate.toDate().getTime())));
        params.put("isEmpty", CollectionUtils.isEmpty(acctRecvs));
        params.put("headerList", Lists.newArrayList());//tempfix
        params.put("TABLE_DATA_SOURCE", new JRBeanCollectionDataSource(acctRecvs));
        if (StringUtils.isNotBlank(region)) {
            params.put("region", region.equalsIgnoreCase(ReportFilter.ALL.name()) ? region
                    : lookupService.getActiveDetailByCode(region).getDescription());
        }
        if (StringUtils.isNotBlank(activationStatus)) {
            params.put("activationStatus", activationStatus.equalsIgnoreCase(ReportFilter.ALL.name()) ? activationStatus
                    : messageSource.getMessage("report.gc.actvnstat." + activationStatus.toLowerCase()));
        }

        DefaultJRProcessor jrProcessor = new DefaultJRProcessor("reports/gift_card_accountsreceivable_age.jrxml"); //default-param=REPORT_DATA_SOURCE 
        jrProcessor.setParameters(params);
        jrProcessor.setFileResolver(new ClasspathFileResolver("reports"));
        return jrProcessor;
    }

    private Map<String, Date> getDateRange(Map<String, String> map) {
        Map<String, Date> dateRange = Maps.newHashMap();

        LocalDate dateFrom = LocalDate.parse(map.get(CommonReportFilter.DATE_FROM), DateTimeFormat.forPattern(DATE_FORMAT_FILTER));
        LocalDate dateTo = LocalDate.parse(map.get(CommonReportFilter.DATE_TO), DateTimeFormat.forPattern(DATE_FORMAT_FILTER));
        Date startDate = null != dateFrom ? dateFrom.toDateTimeAtStartOfDay().toDate() : null;
        Date endDate = null != dateTo ? dateTo.toDateMidnight().toDate() : null;;
        if (null == startDate && null != endDate) {
            startDate = DateUtils.addDays(endDate, -30);
        } else if (null != startDate && null == endDate) {
            endDate = DateUtils.addDays(startDate, 30);
        } else if (null == startDate && null == endDate) {
            startDate = DateUtils.truncate(new Date(), Calendar.MONTH);
            endDate = DateUtils.ceiling(new Date(), Calendar.MONTH);
        }

        dateRange.put(CommonReportFilter.DATE_FROM, DateUtils.truncate(startDate, Calendar.DATE));
        dateRange.put(CommonReportFilter.DATE_TO, DateUtils.addMilliseconds(DateUtils.ceiling(endDate, Calendar.MONTH), -1));
        return dateRange;
    }

    private BooleanExpression createActvnStatExpr(SoActivationStat actvnStat) {
        QSalesOrder qSo = QSalesOrder.salesOrder;
        QReturnRecord qRet = QReturnRecord.returnRecord;
        if (SoActivationStat.ACTIVATED == actvnStat) {
            return BooleanExpression.allOf(qSo.status.eq(SalesOrderStatus.SOLD));
        } else if (SoActivationStat.PARTIALLY_ACTIVATED == actvnStat) {
            return BooleanExpression.allOf(
                    BooleanExpression.allOf(qSo.status.eq(SalesOrderStatus.SOLD)),
                    qSo.id.notIn(new JPASubQuery()
                            .from(qRet)
                            .list(qRet.orderNo.id)));
        } else if (SoActivationStat.NON_ACTIVATED == actvnStat) {
            return BooleanExpression.allOf(qSo.status.ne(SalesOrderStatus.SOLD));
        }
        return null;
    }

    public static class AccountsReceivableAge {

        private String cxName;
        private String cxNumber;
        private BigDecimal paymentTotal;
        private List<AccountsReceivable> paymentList;
        private AccountsReceivable paymentListTotals;

        public AccountsReceivableAge() {
        }

        public AccountsReceivableAge(String cxName, String cxNumber,
                List<AccountsReceivable> paymentList) {
            this.cxName = cxName;
            this.cxNumber = cxNumber;
            this.paymentList = paymentList;
        }

        public String getCxName() {
            return cxName;
        }

        public String getCxNumber() {
            return cxNumber;
        }

        public BigDecimal getPaymentTotal() {
            return paymentTotal;
        }

        public List<AccountsReceivable> getPaymentList() {
            return paymentList;
        }

        public void setPaymentList(List<AccountsReceivable> paymentList) {
            this.paymentList = paymentList;
        }

        public AccountsReceivable getPaymentListTotals() {
            return paymentListTotals;
        }

        public void setPaymentListTotals(AccountsReceivable paymentListTotals) {
            this.paymentListTotals = paymentListTotals;
        }

        public static class AccountsReceivable {

            private String cxNumber;
            private String cxName;
            private String orderNumber;
            private LocalDate orderDate;
            private BigDecimal payment00;
            private BigDecimal payment01;
            private BigDecimal payment02;
            private BigDecimal payment03;
            private BigDecimal payment04;
            private BigDecimal payment05;
            private BigDecimal payment06;
            private BigDecimal payment07;
            private BigDecimal payment08;
            private BigDecimal payment09;
            private BigDecimal paymentTotal;

            public AccountsReceivable() {
            }

            public AccountsReceivable(String cxNumber, String cxName, String orderNumber, LocalDate orderDate, BigDecimal paymentTotal,
                    BigDecimal payment00,
                    BigDecimal payment01,
                    BigDecimal payment02,
                    BigDecimal payment03,
                    BigDecimal payment04,
                    BigDecimal payment05,
                    BigDecimal payment06,
                    BigDecimal payment07,
                    BigDecimal payment08,
                    BigDecimal payment09) {
                this.cxNumber = cxNumber;
                this.cxName = cxName;
                this.orderNumber = orderNumber;
                this.orderDate = orderDate;
                this.paymentTotal = paymentTotal;
                this.payment00 = payment00;
                this.payment01 = payment01;
                this.payment02 = payment02;
                this.payment03 = payment03;
                this.payment04 = payment04;
                this.payment05 = payment05;
                this.payment06 = payment06;
                this.payment07 = payment07;
                this.payment08 = payment08;
                this.payment09 = payment09;
            }

            public AccountsReceivable(AccountsReceivable ar) {
                this(ar.getCxNumber(), ar.getCxName(), ar.getOrderNumber(), ar.getOrderDate(), ar.getPaymentTotal(),
                        ar.getPayment00(), ar.getPayment01(), ar.getPayment02(), ar.getPayment03(), ar.getPayment04(),
                        ar.getPayment05(), ar.getPayment06(), ar.getPayment07(), ar.getPayment08(), ar.getPayment09());
            }

            public String getCxNumber() {
                return cxNumber;
            }

            public String getCxName() {
                return cxName;
            }

            public String getOrderNumber() {
                return orderNumber;
            }

            public LocalDate getOrderDate() {
                return orderDate;
            }

            public BigDecimal getPaymentTotal() {
                return paymentTotal;
            }

            public BigDecimal getPayment00() {
                return null != payment00 ? payment00 : new BigDecimal(0);
            }

            public BigDecimal getPayment01() {
                return null != payment01 ? payment01 : new BigDecimal(0);
            }

            public BigDecimal getPayment02() {
                return null != payment02 ? payment02 : new BigDecimal(0);
            }

            public BigDecimal getPayment03() {
                return null != payment03 ? payment03 : new BigDecimal(0);
            }

            public BigDecimal getPayment04() {
                return null != payment04 ? payment04 : new BigDecimal(0);
            }

            public BigDecimal getPayment05() {
                return null != payment05 ? payment05 : new BigDecimal(0);
            }

            public BigDecimal getPayment06() {
                return null != payment06 ? payment06 : new BigDecimal(0);
            }

            public BigDecimal getPayment07() {
                return null != payment07 ? payment07 : new BigDecimal(0);
            }

            public BigDecimal getPayment08() {
                return null != payment08 ? payment08 : new BigDecimal(0);
            }

            public BigDecimal getPayment09() {
                return null != payment09 ? payment09 : new BigDecimal(0);
            }

            public void setPaymentTotal(BigDecimal paymentTotal) {
                this.paymentTotal = paymentTotal;
            }

            public void setPayment00(BigDecimal payment00) {
                this.payment00 = payment00;
            }

            public void setPayment01(BigDecimal payment01) {
                this.payment01 = payment01;
            }

            public void setPayment02(BigDecimal payment02) {
                this.payment02 = payment02;
            }

            public void setPayment03(BigDecimal payment03) {
                this.payment03 = payment03;
            }

            public void setPayment04(BigDecimal payment04) {
                this.payment04 = payment04;
            }

            public void setPayment05(BigDecimal payment05) {
                this.payment05 = payment05;
            }

            public void setPayment06(BigDecimal payment06) {
                this.payment06 = payment06;
            }

            public void setPayment07(BigDecimal payment07) {
                this.payment07 = payment07;
            }

            public void setPayment08(BigDecimal payment08) {
                this.payment08 = payment08;
            }

            public void setPayment09(BigDecimal payment09) {
                this.payment09 = payment09;
            }
        }
    }

}
