package com.transretail.crm.core.service;

import java.io.InputStream;

import org.joda.time.LocalDate;

import com.transretail.crm.common.reporting.jasper.JRProcessor;
import com.transretail.crm.common.service.dto.request.PagingParam;
import com.transretail.crm.core.dto.MemberMonitorSearchDto;
import com.transretail.crm.core.dto.MemberResultList;
import com.transretail.crm.core.entity.lookup.LookupDetail;

public interface MemberMonitorService {
	MemberResultList getMonitoredMembers(LocalDate date);
	
	MemberResultList getAllMonitoredMembers(LocalDate date);
	
	MemberResultList getMonitoredEmployees(LocalDate date);
	
	MemberResultList getMonitoredMembers(LocalDate date, PagingParam paging);
	
	MemberResultList getMonitoredEmployees(LocalDate date, PagingParam paging);
	
	MemberResultList getMonitoredMembers(MemberMonitorSearchDto dto);
	
	MemberResultList getMonitoredEmployees(MemberMonitorSearchDto dto);
	
	void setMonitoredTransactionCountEmployee(Integer transactionCount);
	
	void setMonitoredTransactionCountIndividual(Integer transactionCount);
	
	void setMonitoredTransactionCount(Integer transcationCount, LookupDetail memberType);
	
	Integer getTransactionCountIndividual();
	
	Integer getTransactionCountEmployee();
	
	boolean isMonitored(String accountId, String memberTypeCode);
	
	Long countTransactionsToday(String accountId, String memberTypeCode);
	
	JRProcessor createJRProcessor(LocalDate date, InputStream logo, boolean isEmployee);
}
