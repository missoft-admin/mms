<%@ include file="common/taglibs.jsp"%>

<h1 class="left-zero"><spring:message code="welcome"/>&nbsp;<sec:authentication property="principal.fullName"/></h1>
<div class="span8 left-zero">

  <div class="well well-blue" style="">

    <div class="form-inline ">
      <label class="label-1">
        <spring:message code="label_right_panel_no_employees">:</spring:message>
      </label>
      <label class="pull-right numbers-1">
        ${employeeStats.numberOfEmployees}
      </label>
    </div>

  </div>
  
  <div class="well well-blue" style="">

    <div class="form-inline label-2">
      <label class="label-2">
        <spring:message code="label_right_panel_no_individuals">:</spring:message>
      </label>
      <label class="pull-right">
        ${employeeStats.numberOfIndividuals}
      </label>
    </div>

    <div class="form-inline mb10 label-2">
      <label class="">
        <spring:message code="label_right_panel_no_professionals">:</spring:message>
      </label>
      <label class="pull-right">
        ${employeeStats.numberOfProfessionals}
      </label>
    </div>



    <div class="form-inline">
      <label class="label-1">
        <spring:message code="label_right_panel_total_no_customers">:</spring:message>
      </label>
      <label class="pull-right numbers-1">
        ${employeeStats.numberOfTotalCustomers}
      </label>
    </div>

  </div>
  
  <div class="well well-blue" style="">
    
    <div class="form-inline mb20">
      <label class="label-1">
        <spring:message code="label_right_panel_store_members">:</spring:message>
      </label>
      <label class="pull-right numbers-1">${employeeStats.numberOfStoreMembers}</label>
    </div>
    
  </div>
  <div class="well" style="height: 200px">
    <h3 class="top-zero hide">Just a Note</h3>
    <p class="hide">
      There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don't look even slightly believable. If you are going to use a passage of Lorem Ipsum, you need to be sure there isn't anything embarrassing hidden in the middle of text. All the Lorem Ipsum generators on the Internet tend to repeat predefined chunks as necessary.</p>
  </div>
</div>
<div class="span4">
  <div class="well well-blue">
    <jsp:include page="rightPanel.jsp"></jsp:include>
  </div>
</div>

<%--<img width="100%" src="images/01-dashboard-r1.jpg"> place holder dashboard image--%>

<%--Sample on how to add internal or inner links --%>
<%--<ul id="internal-links-nav">--%>
<%--<li class="active"><a href="#">Link</a></li>--%>
<%--<li><a href="#">Another One</a></li>--%>
<%--<li><a href="#">And Lastly</a></li>--%>
<%--</ul>--%>