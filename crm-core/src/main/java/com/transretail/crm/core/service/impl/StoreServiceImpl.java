package com.transretail.crm.core.service.impl;


import java.math.BigDecimal;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import com.mysema.query.BooleanBuilder;
import com.transretail.crm.core.security.util.UserUtil;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.time.DateUtils;
import org.joda.time.LocalDate;
import org.joda.time.LocalDateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.google.common.collect.Lists;
import com.mysema.query.jpa.impl.JPAQuery;
import com.mysema.query.types.ConstructorExpression;
import com.mysema.query.types.expr.BooleanExpression;
import com.transretail.crm.common.reporting.jasper.ClasspathFileResolver;
import com.transretail.crm.common.reporting.jasper.DefaultJRProcessor;
import com.transretail.crm.common.reporting.jasper.JRProcessor;
import com.transretail.crm.core.dto.PopularStoreDto;
import com.transretail.crm.core.dto.PopularStoreSearchDto;
import com.transretail.crm.core.dto.StoreDto;
import com.transretail.crm.core.entity.QPosTransaction;
import com.transretail.crm.core.entity.lookup.LookupDetail;
import com.transretail.crm.core.entity.lookup.QStore;
import com.transretail.crm.core.entity.lookup.Store;
import com.transretail.crm.core.repo.StoreRepo;
import com.transretail.crm.core.service.CodePropertiesService;
import com.transretail.crm.core.service.LookupService;
import com.transretail.crm.core.service.StoreService;


@Service("storeService")
public class StoreServiceImpl implements StoreService {

    @Autowired
    private StoreRepo storeRepo;
    @Autowired
    private CodePropertiesService codePropertiesService;
    @Autowired
    private LookupService lookupService;
    @PersistenceContext
    private EntityManager em;



    @Override
    @Transactional(readOnly = true)
    public Iterable<Store> getAllStores() {
        return storeRepo.findAll( new Sort( new Sort.Order( Sort.Direction.ASC, "code" ) ) );
    }
    
    @Override
    @Transactional(readOnly = true)
    public Map<String, String> getAllStoresHashMap() {
        QStore qs = QStore.store;
        return new JPAQuery(em).from(qs).orderBy(qs.code.asc()).map(qs.code, qs.code.concat(" - ").concat(qs.name));
    }

	@Override
	@Transactional(readOnly = true)
	public Map<String, String> getStoreByUserStoreLoc() {

    	QStore store = QStore.store;
		BooleanBuilder booleanBuilder = new BooleanBuilder();
		final String userStore = UserUtil.getCurrentUser().getStore().getCode();
		if (!userStore.equals("10007")) {

			booleanBuilder.and(store.code.eq(userStore));
		}

		return new JPAQuery(em).from(store)
				.where(booleanBuilder)
				.orderBy(store.code.asc())
				.map(store.code, store.code.concat(" - ").concat(store.name));
	}

	@Override
    @Transactional(readOnly = true)
    public Map<String, Store> getAllStoresMap() {
        QStore qs = QStore.store;
        return new JPAQuery(em).from(qs).orderBy(qs.code.asc()).map(qs.code, qs);
    }
    
    @Override
    public Store getStoreByCode(String code) {
    	return storeRepo.findByCode(code);
    }

    @Override
    public Store findById(Integer id) {
        return storeRepo.findOne(id);
    }

    @Override
    public List<StoreDto> find( String storeLike ) {
    	List<StoreDto> stores = Lists.newArrayList();

		LookupDetail ho = lookupService.getDetailByCode( codePropertiesService.getDetailInvLocationHeadOffice() );
    	if ( null != ho && ( StringUtils.containsIgnoreCase( ho.getCode(), storeLike ) || StringUtils.containsIgnoreCase( ho.getDescription(), storeLike ) ) ){
    		StoreDto hoDto = new StoreDto();
    		hoDto.setCode( ho.getCode() );
    		hoDto.setName( ho.getDescription() );
    		stores.add( hoDto );
    	}

		QStore qStore = QStore.store;
		JPAQuery query = new JPAQuery(em);
		List<StoreDto> allStores = query.from( qStore )
			.where(
					BooleanExpression.allOf( 
						qStore.name.containsIgnoreCase( storeLike ).or( qStore.code.containsIgnoreCase( storeLike ) ) ) )
			.distinct()
			.list( ConstructorExpression.create( StoreDto.class, qStore ) );

		if ( CollectionUtils.isNotEmpty( allStores ) ) {
			stores.addAll( allStores );
		}

		return stores;
    }

	@Override
	public List<PopularStoreDto> getPopularStores( Date startDate, Date endDate ) {
		PopularStoreSearchDto searchDto = new PopularStoreSearchDto();
		searchDto.setStartDate( startDate );
		searchDto.setEndDate( endDate );
		searchDto.setDateNe( true );
		BooleanExpression filter = searchDto.createSearchExpression();

		QStore qStore = QStore.store;
		QPosTransaction qTxn = QPosTransaction.posTransaction;
		
		JPAQuery query = new JPAQuery( em ).from( qTxn )
			.leftJoin( qTxn.store, qStore )
			.groupBy( qTxn.store.code, qTxn.store.name )
			.where( filter );
		List<PopularStoreDto> popStores = query
			.list( ConstructorExpression.create( PopularStoreDto.class, 
				qTxn.store.code,
				qTxn.store.name,
				qTxn.customerId.countDistinct(),
				qTxn.customerId.count(),
				qTxn.id.countDistinct(),
				qTxn.totalAmount.sum() ) );

		return popStores;
	}

	@Override
    public JRProcessor createPopularStoresJrProcessor( LocalDate dateFrom, LocalDate dateTo ) {
		Date startDate = null != dateFrom? dateFrom.toDateTimeAtStartOfDay().toDate() : null;
		Date endDate = null != dateTo? dateTo.toDateMidnight().toDate() : null;;
		if ( null == startDate && null != endDate ) {
			startDate = DateUtils.addDays( endDate, -30 );
    	}
		else if ( null != startDate && null == endDate ) {
			endDate = DateUtils.addDays( startDate, 30 );
    	}
		else if ( null == startDate && null == endDate ) {
			startDate = DateUtils.truncate( new Date(), Calendar.MONTH );
			endDate = DateUtils.ceiling( new Date(), Calendar.MONTH );
		}
		startDate = DateUtils.truncate( startDate, Calendar.DATE );
		endDate = DateUtils.addMilliseconds( DateUtils.ceiling( endDate, Calendar.DATE ), -1 );
		dateFrom = LocalDate.fromDateFields( startDate );
		dateTo = LocalDate.fromDateFields( endDate );

		Map<String, Object> params = new HashMap<String, Object>();
		DateTimeFormatter df = DateTimeFormat.forPattern( "MMMM d, yyyy" );
		params.put( "dateFrom", df.print( dateFrom ) );
		params.put( "dateTo", df.print( dateTo ) );
		params.put( "printDate", df.print( new LocalDateTime() ) );

		List<PopularStoreDto> popularStores = getPopularStores( startDate, endDate );
		params.put( "isEmpty", CollectionUtils.isEmpty( popularStores ) );
		params.put( "popularStoresList", popularStores );

		DefaultJRProcessor jrProcessor = new DefaultJRProcessor( "reports/popular_stores_report.jasper" );
		jrProcessor.setParameters( params );
		jrProcessor.setFileResolver( new ClasspathFileResolver( "reports" ) );

		return jrProcessor;
    }



    public static class PopularStoreReportBean {
    	private String storeCode;
		private String storeName;
    	private Long noOfCustomers;
    	private Long noOfMembers;
    	private Long noOfNonMembers;
    	private Long noOfTxns;
    	private BigDecimal totalTxnAmount;

		public PopularStoreReportBean() {}
		public PopularStoreReportBean( PopularStoreDto dto ) {
			this.storeCode = dto.getStoreCode();
			this.storeName = dto.getStoreName();
			this.noOfCustomers = dto.getNoOfCustomers();
			this.noOfMembers = dto.getNoOfMembers();
			this.noOfNonMembers = dto.getNoOfNonMembers();
			this.noOfTxns = dto.getNoOfTxns();
			this.totalTxnAmount = dto.getTotalTxnAmount();
		}

    	public String getStoreCode() {
			return storeCode;
		}
		public String getStoreName() {
			return storeName;
		}
		public Long getNoOfCustomers() {
			return noOfCustomers;
		}
		public Long getNoOfMembers() {
			return noOfMembers;
		}
		public Long getNoOfNonMembers() {
			return noOfNonMembers;
		}
		public Long getNoOfTxns() {
			return noOfTxns;
		}
		public BigDecimal getTotalTxnAmount() {
			return totalTxnAmount;
		}
    }

}
