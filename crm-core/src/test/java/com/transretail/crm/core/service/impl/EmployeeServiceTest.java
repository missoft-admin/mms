package com.transretail.crm.core.service.impl;


import com.transretail.crm.core.dto.MemberDto;
import com.transretail.crm.core.entity.MemberModel;
import com.transretail.crm.core.entity.embeddable.CustomerProfile;
import com.transretail.crm.core.entity.enums.MemberStatus;
import com.transretail.crm.core.entity.enums.Status;
import com.transretail.crm.core.entity.lookup.LookupDetail;
import com.transretail.crm.core.entity.lookup.LookupHeader;
import com.transretail.crm.core.entity.lookup.MemberType;
import com.transretail.crm.core.repo.LookupDetailRepo;
import com.transretail.crm.core.repo.LookupHeaderRepo;
import com.transretail.crm.core.repo.MemberRepo;
import com.transretail.crm.core.service.EmployeePurchaseService;
import com.transretail.crm.core.service.EmployeeService;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.omg.PortableInterceptor.INACTIVE;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Configurable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;

@Configurable
@Transactional
@ContextConfiguration(locations = "classpath*:/META-INF/spring/applicationContext*.xml")
@RunWith(SpringJUnit4ClassRunner.class)
public class EmployeeServiceTest {

	@Autowired
	MemberRepo memberRepo;
	
    @Autowired
    EmployeeService employeeService;
    
    @Autowired
    EmployeePurchaseService employeePurchaseService;
    
    @Autowired
    JpaRepository<MemberType, String> memberTypeRepo;
    
    @Autowired
	LookupHeaderRepo lookupHeaderRepo;
	
	@Autowired
	LookupDetailRepo lookupDetailRepo;

    static String employeeType  = "MTYP002";
    static String employeeTypeDesc = "Employee";
    
    @Before
    public void setup() {


    	LookupHeader header = new LookupHeader();
    	header.setCode("membertype");
    	header.setDescription("membertype");
    	header = lookupHeaderRepo.saveAndFlush(header);
    	
    	LookupDetail theMemberType = new LookupDetail();
    	theMemberType.setHeader(header);
    	theMemberType.setCode(employeeType);
    	theMemberType.setDescription(employeeTypeDesc);
    	theMemberType.setStatus(Status.ACTIVE);
    	theMemberType = lookupDetailRepo.saveAndFlush(theMemberType);
    	
        MemberModel emp = new MemberModel();
        emp.setAccountId("1");
        emp.setFirstName("first1");
        emp.setLastName("last1");
        emp.setPin("pin1");
        emp.setUsername("username1");
        emp.setEnabled(true);
        emp.setContact("000001");
        emp.setAccountStatus(MemberStatus.ACTIVE);
        emp.setMemberType(theMemberType);
        CustomerProfile customerProfile = new CustomerProfile();
        customerProfile.setBirthdate(new Date());
        emp.setCustomerProfile(customerProfile);
        memberRepo.save(emp);
        
        emp = new MemberModel();
        emp.setAccountId("2");
        emp.setFirstName("first2");
        emp.setLastName("last2");
        emp.setPin("pin2");
        emp.setUsername("username2");
        emp.setEnabled(true);
        emp.setContact("000002");
        emp.setAccountStatus(MemberStatus.ACTIVE);
        emp.setMemberType(theMemberType);
        customerProfile = new CustomerProfile();
        customerProfile.setBirthdate(new Date());
        emp.setCustomerProfile(customerProfile);
        memberRepo.save(emp);
        
        emp = new MemberModel();
        emp.setAccountId("3");
        emp.setFirstName("first3");
        emp.setLastName("last3");
        emp.setPin("pin3");
        emp.setUsername("username3");
        emp.setEnabled(true);
        emp.setContact("000003");
        emp.setAccountStatus(MemberStatus.ACTIVE);
        emp.setMemberType(theMemberType);
        customerProfile = new CustomerProfile();
        customerProfile.setBirthdate(new Date());
        emp.setCustomerProfile(customerProfile);
        memberRepo.save(emp);
        
        emp = new MemberModel();
        emp.setAccountId("4");
        emp.setFirstName("first4");
        emp.setLastName("last4");
        emp.setPin("pin4");
        emp.setUsername("username4");
        emp.setEnabled(true);
        emp.setContact("000004");
        emp.setAccountStatus(MemberStatus.ACTIVE);
        emp.setMemberType(theMemberType);
        customerProfile = new CustomerProfile();
        customerProfile.setBirthdate(new Date());
        emp.setCustomerProfile(customerProfile);
        memberRepo.save(emp);
        
        emp = new MemberModel();
        emp.setAccountId("5");
        emp.setFirstName("first5");
        emp.setLastName("last5");
        emp.setPin("pin5");
        emp.setUsername("username5");
        emp.setEnabled(true);
        emp.setContact("000005");
        emp.setAccountStatus(MemberStatus.TERMINATED);
        emp.setMemberType(theMemberType);
        customerProfile = new CustomerProfile();
        customerProfile.setBirthdate(new Date());
        emp.setCustomerProfile(customerProfile);
        memberRepo.save(emp);
        
    }


    @Test
    public void testFindAllEmployees() throws Exception {
        List<MemberDto> emps = employeeService.findAllEmployees();

        Assert.assertNotNull(emps);
        Assert.assertEquals(5, emps.size());
    }
    
    @Test
    public void testFindByCriteria() throws Exception {
    	MemberDto theDto = new MemberDto();
    	theDto.setAccountId("2");
        List<MemberDto> emps = employeeService.findByCriteria(theDto, employeeType);
        Assert.assertNotNull(emps);
        Assert.assertEquals(1, emps.size());
    }
    
    @Test
    public void testFindEmployeeDtoEntries() {
    	int firstResult = 1;
    	int maxResults = 10;
    	
    	List<MemberDto> emps = employeeService.findEmployeeEntries(firstResult, maxResults);
    	Assert.assertNotNull(emps);
    	Assert.assertEquals(5, emps.size());
    }
    
    @Test
    public void testCountAllEmployeeDtos() {
    	Assert.assertEquals(5, employeeService.countAllEmployees());
    }
    
    @Test
    public void testFindActiveEmployeeByAccountId() {
    	MemberDto theMember = employeeService.findActiveEmployeeByAccountId("1");
    	Assert.assertNotNull(theMember);
    	
    	theMember = employeeService.findActiveEmployeeByAccountId("5");
    	Assert.assertNull(theMember);
    }
    
    @Test
    public void testFindEmployeeByAccountIdAndStatus() {
    	MemberDto theMember = employeeService.findEmployeeByAccountIdAndStatus("1", MemberStatus.ACTIVE);
    	Assert.assertNotNull(theMember);
    	
    	theMember = employeeService.findEmployeeByAccountIdAndStatus("2", MemberStatus.TERMINATED);
    	Assert.assertNull(theMember);
    }
   
}
