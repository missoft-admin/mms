package com.transretail.crm.giftcard.dto;

import java.math.BigDecimal;

import javax.validation.constraints.Max;
import javax.validation.constraints.NotNull;

import org.apache.commons.lang3.BooleanUtils;
import org.hibernate.validator.constraints.NotEmpty;

import com.transretail.crm.giftcard.entity.CardVendor;
import com.transretail.crm.giftcard.entity.GiftCard;

/**
 *
 */
public class GiftCardDto {
    private Long id;
    private String seqNo;
    //    @NotEmpty
    private String type;
    @NotEmpty
    private String name;
    //    @NotEmpty
    private String genCode;
    //    @NotEmpty
    private String country;
    private String currency;
    private BigDecimal cardBarePrice;
    private Boolean fixValue = Boolean.FALSE;
    @NotNull
    private BigDecimal faceValue;
    private Boolean includeLiner = Boolean.FALSE;
    @NotNull
    private Long vendor;
    private String vendorName;
    //    @NotNull
    private BigDecimal cardFee;
    //    @NotNull
    private Integer cardNoLength;
    //    @NotNull
    private Integer lpoDigits;
    @NotNull
    @Max(999)
    private Integer randomNoDigits;
    private Boolean luhnCheck = Boolean.FALSE;
    //    @NotNull
    private Integer graceDays;
    private Boolean allowToSale = Boolean.FALSE;
    private Boolean redemption = Boolean.FALSE;
    private Boolean partialRedeem = Boolean.FALSE;
    private Boolean reload = Boolean.FALSE;
    //    @NotNull
    private BigDecimal maxAmount;
    //    @NotNull
    private Integer maxReloadCount;
    private Boolean refund = Boolean.FALSE;
    //    @NotNull
    private Integer effectivityMonths;
    //    @NotNull
    private Integer bankTrustMonths;
    private Double yellowAlertSafetyPercent;
    private Double redAlertSafetyPercent;
    private String imageLocation;

    public GiftCardDto() {

    }

    public GiftCardDto(GiftCard gc) {
        id = gc.getId();
        seqNo = gc.getSeqNo();
        type = gc.getType();
        name = gc.getName();
        genCode = gc.getGenCode();
        country = gc.getCountry();
        currency = gc.getCurrency();
        cardBarePrice = gc.getCardBarePrice();
        fixValue = gc.getFixValue();
        faceValue = gc.getFaceValue();
        includeLiner = gc.getIncludeLiner();
        vendor = gc.getVendor().getId();
        vendorName = gc.getVendorName();
        cardFee = gc.getCardFee();
        cardNoLength = gc.getCardNoLength();
        lpoDigits = gc.getLpoDigits();
        randomNoDigits = gc.getRandomNoDigits();
        luhnCheck = gc.getLuhnCheck();
        graceDays = gc.getGraceDays();
        allowToSale = gc.getAllowToSale();
        redemption = gc.getRedemption();
        partialRedeem = gc.getPartialRedeem();
        reload = gc.getReload();
        maxAmount = gc.getMaxAmount();
        maxReloadCount = gc.getMaxReloadCount();
        refund = gc.getRefund();
        effectivityMonths = gc.getEffectivityMonths();
        bankTrustMonths = gc.getBankTrustMonths();
        yellowAlertSafetyPercent = gc.getYellowAlertSafetyPercent();
        redAlertSafetyPercent = gc.getRedAlertSafetyPercent();
        imageLocation = gc.getImageLocation();
    }

    /**
     * Convert this dto to GiftCard
     *
     * @return GiftCard model
     */
    public GiftCard toModel() {
        return copyValues(null);
    }


    /**
     * Update the giftcard parameter with the values of the fields in this DTO
     *
     * @param gc GiftCard to update
     * @return updated GiftCard
     */
    public GiftCard update(GiftCard gc) {
        return copyValues(gc);
    }

    private GiftCard copyValues(GiftCard gc) {
        if (gc == null) {
            gc = new GiftCard();
        }
        gc.setSeqNo(getSeqNo());
        gc.setType(getType());
        gc.setName(getName());
        gc.setGenCode(getGenCode());
        gc.setCountry(getCountry());
        gc.setCurrency(getCurrency());
        gc.setCardBarePrice(getCardBarePrice());
        gc.setFixValue(getFixValue());
        gc.setFaceValue(getFaceValue());
        gc.setIncludeLiner(getIncludeLiner());
        gc.setVendor(new CardVendor(getVendor()));
        gc.setVendorName(getVendorName());
        gc.setCardFee(getCardFee());
        gc.setCardNoLength(getCardNoLength());
        gc.setLpoDigits(getLpoDigits());
        gc.setRandomNoDigits(getRandomNoDigits());
        gc.setLuhnCheck(getLuhnCheck());
        gc.setGraceDays(getGraceDays());
        gc.setAllowToSale(getAllowToSale());
        gc.setRedemption(getRedemption());
        gc.setPartialRedeem(getPartialRedeem());
        gc.setReload(getReload());
        gc.setMaxAmount(getMaxAmount());
        gc.setMaxReloadCount(getMaxReloadCount());
        gc.setRefund(getRefund());
        gc.setEffectivityMonths(getEffectivityMonths());
        gc.setBankTrustMonths(getBankTrustMonths());
        gc.setYellowAlertSafetyPercent(getYellowAlertSafetyPercent());
        gc.setRedAlertSafetyPercent(getRedAlertSafetyPercent());
        gc.setImageLocation(getImageLocation());
        return gc;
    }

    public Long getId() {
        return id;
    }

    public String getSeqNo() {
        return seqNo;
    }

    public void setSeqNo(String seqNo) {
        this.seqNo = seqNo;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getGenCode() {
        return genCode;
    }

    public void setGenCode(String genCode) {
        this.genCode = genCode;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public BigDecimal getCardBarePrice() {
        return cardBarePrice;
    }

    public void setCardBarePrice(BigDecimal cardBarePrice) {
        this.cardBarePrice = cardBarePrice;
    }

    public Boolean getFixValue() {
        return BooleanUtils.toBoolean(fixValue);
    }

    public void setFixValue(Boolean fixValue) {
        this.fixValue = fixValue;
    }

    public BigDecimal getFaceValue() {
        return faceValue;
    }

    public void setFaceValue(BigDecimal faceValue) {
        this.faceValue = faceValue;
    }

    public Boolean getIncludeLiner() {
        return BooleanUtils.toBoolean(includeLiner);
    }

    public void setIncludeLiner(Boolean includeLiner) {
        this.includeLiner = includeLiner;
    }

    public Long getVendor() {
        return vendor;
    }

    public void setVendor(Long vendor) {
        this.vendor = vendor;
    }

    public String getVendorName() {
        return vendorName;
    }

    public void setVendorName(String vendorName) {
        this.vendorName = vendorName;
    }

    public BigDecimal getCardFee() {
        return cardFee;
    }

    public void setCardFee(BigDecimal cardFee) {
        this.cardFee = cardFee;
    }

    public Integer getCardNoLength() {
        return cardNoLength;
    }

    public void setCardNoLength(Integer cardNoLength) {
        this.cardNoLength = cardNoLength;
    }

    public Integer getLpoDigits() {
        return lpoDigits;
    }

    public void setLpoDigits(Integer lpoDigits) {
        this.lpoDigits = lpoDigits;
    }

    public Integer getRandomNoDigits() {
        return randomNoDigits;
    }

    public void setRandomNoDigits(Integer randomNoDigits) {
        this.randomNoDigits = randomNoDigits;
    }

    public Boolean getLuhnCheck() {
        return BooleanUtils.toBoolean(luhnCheck);
    }

    public void setLuhnCheck(Boolean luhnCheck) {
        this.luhnCheck = luhnCheck;
    }

    public Integer getGraceDays() {
        return graceDays;
    }

    public void setGraceDays(Integer graceDays) {
        this.graceDays = graceDays;
    }

    public Boolean getAllowToSale() {
        return BooleanUtils.toBoolean(allowToSale);
    }

    public void setAllowToSale(Boolean allowToSale) {
        this.allowToSale = allowToSale;
    }

    public Boolean getRedemption() {
        return BooleanUtils.toBoolean(redemption);
    }

    public void setRedemption(Boolean redemption) {
        this.redemption = redemption;
    }

    public Boolean getPartialRedeem() {
        return partialRedeem;
    }

    public void setPartialRedeem(Boolean partialRedeem) {
        this.partialRedeem = partialRedeem;
    }

    public Boolean getReload() {
        return reload;
    }

    public void setReload(Boolean reload) {
        this.reload = reload;
    }

    public BigDecimal getMaxAmount() {
        return maxAmount;
    }

    public void setMaxAmount(BigDecimal maxAmount) {
        this.maxAmount = maxAmount;
    }

    public Integer getMaxReloadCount() {
        return maxReloadCount;
    }

    public void setMaxReloadCount(Integer maxReloadCount) {
        this.maxReloadCount = maxReloadCount;
    }

    public Boolean getRefund() {
        return refund;
    }

    public void setRefund(Boolean refund) {
        this.refund = BooleanUtils.toBoolean(refund);
    }

    public Integer getEffectivityMonths() {
        return effectivityMonths;
    }

    public void setEffectivityMonths(Integer effectivityMonths) {
        this.effectivityMonths = effectivityMonths;
    }

    public Integer getBankTrustMonths() {
        return bankTrustMonths;
    }

    public void setBankTrustMonths(Integer bankTrustMonths) {
        this.bankTrustMonths = bankTrustMonths;
    }

    public Double getYellowAlertSafetyPercent() {
        return yellowAlertSafetyPercent;
    }

    public void setYellowAlertSafetyPercent(Double yellowAlertSafetyPercent) {
        this.yellowAlertSafetyPercent = yellowAlertSafetyPercent;
    }

    public Double getRedAlertSafetyPercent() {
        return redAlertSafetyPercent;
    }

    public void setRedAlertSafetyPercent(Double redAlertSafetyPercent) {
        this.redAlertSafetyPercent = redAlertSafetyPercent;
    }

    public String getImageLocation() {
        return imageLocation;
    }

    public void setImageLocation(String imageLocation) {
        this.imageLocation = imageLocation;
    }
}
