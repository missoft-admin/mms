package com.transretail.crm.giftcard.entity;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Table;

import org.hibernate.annotations.Type;
import org.joda.time.DateTime;

import com.transretail.crm.core.entity.support.CustomAuditableEntity;
import com.transretail.crm.core.entity.CrmForPeoplesoftConfig.TransactionType;

/**
 *
 */
@Entity
@Table(name = "CRM_FOR_PEOPLESOFT")
public class CrmForPeoplesoft extends CustomAuditableEntity<Long> implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Column(name = "MMS_ACCT")
	private String mmsAcct;
	@Column(name = "PS_ACCT")
	private String psAcct;
	@Column(name = "TYPE")
	private String type;
	@Column(name = "TXN_DATE")
	@Type(type = "org.jadira.usertype.dateandtime.joda.PersistentDateTime")
	private DateTime transactionDate;
	@Column(name = "TXN_AMT")
	private BigDecimal transactionAmt;
	@Column(name = "STORE_CODE")
	private String storeCode;
	@Column(name = "BU")
	private String businessUnit;
	@Column(name = "PS_STORE_CODE")
	private String psStoreCode;
	@Column(name = "DEPT_ID")
	private String deptId;
	@Column(name = "NOTES")
	private String notes;
	@Column(name = "TXN_TYPE")
	@Enumerated(EnumType.STRING)
	private TransactionType transactionType;
	
	public String getMmsAcct() {
		return mmsAcct;
	}

	public void setMmsAcct(String mmsAcct) {
		this.mmsAcct = mmsAcct;
	}

	public DateTime getTransactionDate() {
		return transactionDate;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public void setTransactionDate(DateTime transactionDate) {
		this.transactionDate = transactionDate;
	}

	public BigDecimal getTransactionAmt() {
		return transactionAmt;
	}

	public void setTransactionAmt(BigDecimal transactionAmt) {
		this.transactionAmt = transactionAmt;
	}

	public String getStoreCode() {
		return storeCode;
	}

	public void setStoreCode(String storeCode) {
		this.storeCode = storeCode;
	}

	public String getBusinessUnit() {
		return businessUnit;
	}

	public void setBusinessUnit(String businessUnit) {
		this.businessUnit = businessUnit;
	}

	public String getPsStoreCode() {
		return psStoreCode;
	}

	public void setPsStoreCode(String psStoreCode) {
		this.psStoreCode = psStoreCode;
	}

	public String getDeptId() {
		return deptId;
	}

	public void setDeptId(String deptId) {
		this.deptId = deptId;
	}

	public String getNotes() {
		return notes;
	}

	public void setNotes(String notes) {
		this.notes = notes;
	}

	public String getPsAcct() {
		return psAcct;
	}

	public void setPsAcct(String psAcct) {
		this.psAcct = psAcct;
	}

	public TransactionType getTransactionType() {
		return transactionType;
	}

	public void setTransactionType(TransactionType transactionType) {
		this.transactionType = transactionType;
	}
	
	

	
}
