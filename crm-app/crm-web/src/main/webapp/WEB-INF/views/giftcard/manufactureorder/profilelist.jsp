<%@include file="../../common/taglibs.jsp" %>
<%@ taglib prefix="permission" tagdir="/WEB-INF/tags/permission" %>

<div class="page-header page-header2">
    <h1><spring:message code="gc_profile" /></h1>
</div>

  <div>
    <jsp:include page="profilesearch.jsp" />
  </div>

<permission:render inventoryLoc="INVT001">
<div class="pull-right mb20">
  <button type="button" class="btn btn-primary" id="createProfile"><spring:message code="create_profile" /></button>
</div>
</permission:render>

<div id="product_profile_list_container">
</div>

<div class="modal hide  nofly" id="orderDialog" tabindex="-1" role="dialog" aria-hidden="true">
</div>


<jsp:include page="../../common/confirm.jsp" />

  <link href="<spring:url value="/css/dataTables.css" />" rel="stylesheet">
  <script src="<spring:url value="/js/jquery/jquery.dataTables.js" />" ></script>
  <script src="<spring:url value="/js/jquery/jquery.dataTables.linkLengthMenu.js" />"></script>
  <script src="<spring:url value="/js/jquery/jquery.dataTables.customPaging.js" />"></script>
  <script src="<spring:url value="/js/jquery/ajax-datatable.js" />" ></script>
  <script src="<spring:url value="/js/common/form2js.js" />" ></script>
  <script src="<spring:url value="/js/common/json2.js" />" ></script>
  <script src="<spring:url value="/js/jquery/jquery.toObject.js" />" ></script>
  <link href="<c:url value="/styles/temp.css" />" rel="stylesheet" />


  <script src="<spring:url value="/js/jquery/jquery.numberInput.js" />" ></script>
  <script src="<c:url value="/js/bootstrap/bootstrap-datepicker.js"/>" type='text/javascript'><![CDATA[&nbsp; ]]></script>
  <link href="<c:url value="/css/bootstrap/datepicker.css" />" rel="stylesheet" />
  <link href="<c:url value="/styles/temp.css" />" rel="stylesheet" />
  <script src="<spring:url value="/js/jquery/jquery.bindWithDelay.js" />"></script>
  <script src="<spring:url value="/js/jquery/jquery.ajaxQueue.js" />"></script>

<%@include file="fragment/safetystock.jsp" %>
<script>
$(document).ready(function() {

  $orderDialog = $("#orderDialog").modal({
    show: false
  }).on('hidden.bs.modal', function() {
    $(this).empty();
  }).css({
    "width": "900px",
  });

  $("#createProfile").click(function() {
    $.get("<c:url value="/gc/productprofile/form" />", function(data) {
      $orderDialog.modal("show").html(data);
    }, "html");
  });

  $("#product_profile_list_container").ajaxDataTable({
    'autoload'  : true,
    'ajaxSource' : "<c:url value="/gc/productprofile/search" />",
    'aaSorting'   : [[ 0, "desc" ]],
    'columnHeaders' : [
		{text: "", className : "hide"},
      "<spring:message code="profile_product_code"/>",
      "<spring:message code="profile_product_desc"/>",
      "<spring:message code="profile_face_value"/>",
      "<spring:message code="profile_card_fee"/>",
      "<spring:message code="profile_allow_reload"/>",
      "<spring:message code="profile_allow_partial_redeem"/>",
      "<spring:message code="profile_status"/>",
      ""
    ],
    'modelFields' : [
	  {name: 'created', sortable: true, aaClassname : "hide", dataType: "DATE"},
      {name : 'productCode', sortable : true},
      {name : 'productDesc', sortable : true},
      {name : 'faceValue', sortable : true, fieldCellRenderer : function (data, type, row) {
          return row.faceValueDesc.toLocaleString();
      }},
      {name : 'cardFee', sortable : true, fieldCellRenderer : function (data, type, row) {
          return row.cardFee.toLocaleString();
      }},
      {name : 'allowReload', sortable : true},
      {name : 'allowPartialRedeem', sortable : true},
      {name : 'status', sortable : true},
      {customCell : function ( innerData, sSpecific, json ) {
        if (sSpecific == 'display') {
          var html = "";
          var isActive = json.status != 'DEACTIVATED';
          if ( isActive ) {
              if(json.status != 'APPROVED') {
                  <permission:render inventoryLoc="INVT001">
                  if(json.status != 'SUBMITTED'){
                    html += "<button class=\"updateProfile btn btn-primary pull-left tiptip icn-edit\" data-id=\""+json.id+"\" title=\"<spring:message code="label_update"/>\"></button>";
                  <sec:authorize ifAnyGranted="ROLE_MERCHANT_SERVICE_SUPERVISOR">
                  }else{
                    html += "<button class=\"updateProfile btn btn-primary pull-left tiptip icn-edit\" data-id=\""+json.id+"\" title=\"<spring:message code="label_process"/>\"></button>";
                  </sec:authorize>
                  }
                  html += "<button type=\"button\" class=\"tiptip btn pull-left icn-delete deleteProfile\" data-id=\""+json.id+"\" title=\"<spring:message code="label_delete"/>\"></button>";
                  </permission:render>
                }
                <sec:authorize ifAnyGranted="ROLE_MERCHANT_SERVICE_SUPERVISOR">
                else {
                  html += "<button type=\"button\" class=\"tiptip btn pull-left icn-edit viewSafetyStock\" data-id=\""+json.id+"\" title=\"<spring:message code="safetystock.button.view"/>\"></button>";
                }
                </sec:authorize>

                if(json.status == 'APPROVED')
              	  html += "<button type=\"button\" class=\"tiptip btn pull-left icn-view viewProfile\" data-id=\""+json.id+"\" title=\"<spring:message code="label_view"/>\"></button>";
          }

          var title = isActive? "<spring:message code="label_deactivate"/>" : "<spring:message code="label_activate"/>";
          var classname = isActive? "icn-delete" : "icn-approve";
		  html += "<button class=\"activateProfile btn btn-primary pull-left tiptip " + classname + "\" data-id=\""+json.id+"\" data-isactive=\""+ isActive + "\" title=\""
		  				+ title + "\" data-url=\"<c:url value="/gc/productprofile/activate/%ID%/%ACTIVATE%" />\" ></button>";

          return html;
        } else {
          return "";
        }
      }
      }
    ]
  }).on("click", ".updateProfile", function() {
    $.get("<c:url value="/gc/productprofile/form/" />" + $(this).data("id"), function(data) {
      $orderDialog.modal("show").html(data);
    }, "html");
  }).on("click", ".deleteProfile", function() {
    var itemId = $(this).data("id");
    getConfirm('<spring:message code="entity_delete_confirm" />', function(result) {
      if(result) {
        $.post("<c:url value="/gc/productprofile/" />" + itemId, {_method: 'DELETE'}, function(data) {
          location.reload();
        });
      }
    });
  })
  .on( "click", ".viewProfile", function(e) {
      $.get("<c:url value="/gc/productprofile/view/" />" + $(this).data( "id" ), function( data ) { $orderDialog.modal( "show" ).html( data ); }, "html" );
  })
  .on( "click", ".activateProfile", function(e) {
		e.preventDefault();
		var url = $(this).data( "url" ).replace( new RegExp("%ID%", 'g'), $(this).data("id") ).replace( new RegExp("%ACTIVATE%", 'g'), !$(this).hasClass( "icn-delete" ) );
		var activateFn = function(result) {
			if(result) { $.get( url, function(resp) {  $("#product_profile_list_container").ajaxDataTable( "search" ); });	}
		};
		if( $(this).hasClass( "icn-delete" ) ) {
			getConfirm( "<spring:message code="gc.profile.msg.deactivate" />", activateFn );
		} else {
			activateFn( true );
		}
	});

  function initCommonFields(dialog) {
    $(".floatInput", $(dialog)).numberInput({ "type" : "float" });
    $(".intInput", $(dialog)).numberInput({ "type" : "int" });
      $(".wholeInput", $(dialog)).numberInput({ "type" : "whole" });
  }
});
</script>