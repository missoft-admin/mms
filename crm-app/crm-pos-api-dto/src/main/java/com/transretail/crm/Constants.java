package com.transretail.crm;

/**
 * @author Allan G. Ramirez (agramirez@exist.com)
 */
public enum Constants {
    COLUMN_CREATED_DATETIME("CREATED_DATETIME"), JOBCONFIG_PUSH("CRMPUSHTXN"), JOBCONFIG_PULL("CRMPULLTXN"), JDBC_ERROR_CODE(
        "jdbc.error"), JDBC_ERROR_DEFAULT_MESSAGE("Failed to execute query."), DBEE("DBEE"), TTSERVER("TTSERVER"), LOCALDB("LOCALDB");

    private String value;

    private Constants(String value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return value;
    }
}
