package com.transretail.crm.marketing.dto;

/**
 * @author Allan G. Ramirez (agramirez@exist.com)
 */
public class DecileQueryResultDto {
    private Long memberId;
    private Double txnAmountTotal;
    private Long txnNoCount;
    private Long monthWithTxnCount;

    public DecileQueryResultDto() {

    }

    public DecileQueryResultDto(Long memberId, Double txnAmountTotal, Long txnNoCount, Long monthWithTxnCount) {
        this.memberId = memberId;
        this.txnAmountTotal = txnAmountTotal;
        this.txnNoCount = txnNoCount;
        this.monthWithTxnCount = monthWithTxnCount;
    }

    public Double getTxnAmountTotal() {
        return txnAmountTotal;
    }

    public void setTxnAmountTotal(Double txnAmountTotal) {
        this.txnAmountTotal = txnAmountTotal;
    }

    public Long getTxnNoCount() {
        return txnNoCount;
    }

    public void setTxnNoCount(Long txnNoCount) {
        this.txnNoCount = txnNoCount;
    }

    public Long getMonthWithTxnCount() {
        return monthWithTxnCount;
    }

    public void setMonthWithTxnCount(Long monthWithTxnCount) {
        this.monthWithTxnCount = monthWithTxnCount;
    }

    public Long getMemberId() {
        return memberId;
    }

    public void setMemberId(Long memberId) {
        this.memberId = memberId;
    }
}
