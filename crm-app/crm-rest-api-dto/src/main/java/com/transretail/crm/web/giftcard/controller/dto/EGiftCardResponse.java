package com.transretail.crm.web.giftcard.controller.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.transretail.crm.giftcard.dto.EGiftCardProfileDto;
import com.transretail.crm.web.giftcard.controller.GiftCardResponse;
import java.util.Set;

public class EGiftCardResponse extends GiftCardResponse {

    @JsonInclude(Include.NON_EMPTY)
    private String mobileNo;
    @JsonInclude(Include.NON_EMPTY)
    private String email;
    @JsonInclude(Include.NON_NULL)
    private Boolean isEmailSent;
    @JsonInclude(Include.NON_NULL)
    private Set<EGiftCardProfileDto> availableProducts;

    public String getMobileNo() {
        return mobileNo;
    }

    public void setMobileNo(String mobileNo) {
        this.mobileNo = mobileNo;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Boolean getIsEmailSent() {
        return isEmailSent;
    }

    public void setIsEmailSent(Boolean isEmailSent) {
        this.isEmailSent = isEmailSent;
    }

    public GiftCardResponse withAvailableProducts(Set<EGiftCardProfileDto> availableProducts) {
    	this.availableProducts = availableProducts;
    	return this;
    }

    public Set<EGiftCardProfileDto> getAvailableProducts() {
        return availableProducts;
    }

    public void setAvailableProducts(Set<EGiftCardProfileDto> availableProducts) {
        this.availableProducts = availableProducts;
    }
}
