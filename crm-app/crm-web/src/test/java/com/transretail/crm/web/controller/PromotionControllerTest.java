package com.transretail.crm.web.controller;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.AnnotationConfigWebContextLoader;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

import com.transretail.crm.core.repo.PromotionRepo;
import com.transretail.crm.core.service.CodePropertiesService;
import com.transretail.crm.core.service.LookupService;
import com.transretail.crm.core.service.MemberGroupService;
import com.transretail.crm.core.service.MemberService;
import com.transretail.crm.core.service.OverlappingPromoService;
import com.transretail.crm.core.service.ProductGroupService;
import com.transretail.crm.core.service.ProductService;
import com.transretail.crm.core.service.PromotionService;
import com.transretail.crm.core.service.StoreGroupService;

/**
 * @author Allan G. Ramirez (agramirez@exist.com)
 */
@RunWith(SpringJUnit4ClassRunner.class)
@WebAppConfiguration
@ContextConfiguration(loader = AnnotationConfigWebContextLoader.class)
public class PromotionControllerTest extends AbstractMockMvcTest {
    @Configuration
    @EnableWebMvc
    static class Config {
        @Bean
        public PromotionController promotionController() {
            return new PromotionController();
        }

        @Bean
        public PromotionService promotionService() {
            return mock(PromotionService.class);
        }

        @Bean
        public LookupService lookupService() {
            return mock(LookupService.class);
        }

        @Bean
        public CodePropertiesService codePropertiesService() {
            return mock(CodePropertiesService.class);
        }

        @Bean
        public MemberGroupService memberGroupService() {
            return mock(MemberGroupService.class);
        }

        @Bean
        public StoreGroupService storeGroupService() {
            return mock(StoreGroupService.class);
        }

        @Bean
        public ProductGroupService productGroupService() {
            return mock(ProductGroupService.class);
        }

        @Bean
        public ProductService productService() {
            return mock(ProductService.class);
        }

        @Bean
        public OverlappingPromoService overlappingPromoService() {
            return mock(OverlappingPromoService.class);
        }

        @Bean
        public PromotionRepo promotionRepo() {
            return mock(PromotionRepo.class);
        }

        @Bean
        public MemberService memberService() {
            return mock(MemberService.class);
        }
    }

    @Autowired
    private PromotionService promotionService;

    @Autowired
    private LookupService lookupService;

    @Autowired
    private CodePropertiesService codePropertiesService;

    @Autowired
    private MemberGroupService memberGroupService;

    @Autowired
    private StoreGroupService storeGroupService;

    @Autowired
    private ProductGroupService productGroupService;

    @Autowired
    private ProductService productService;
    @Autowired
    private OverlappingPromoService overlappingPromoService;
    @Autowired
    private PromotionRepo promotionRepo;

    @Test
    public void getOverlappingPromotionsTest() throws Exception {
        Long promotionId = 1L;
        mockMvc.perform(get("/promotion/" + promotionId + "/overlappingpromo"))
            .andExpect(status().isOk());
        verify(overlappingPromoService).getOverlappingPromo(promotionId);
    }
}
