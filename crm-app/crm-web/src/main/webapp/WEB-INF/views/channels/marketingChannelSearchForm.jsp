<%@include file="../common/taglibs.jsp" %>

<style type="text/css">
  <!-- 
  /* .control-label { width: 50px !important; } 
  .controls { margin-left: 80px !important; } 
  .controls div { margin-bottom: 5px; } */
  .row-fluid { width: 100px !important; display: inline-block; display: inline; }
  .input-daterange { display: inline-block; display: inline; }
  .well input, .well select {margin-right:10px}
  .well input.btn-primary {margin-right:0px}
  -->
</style>

<div id="content_marketingChannelSearchForm" class="form-horizontal block-search well">

  <form id="marketingChannelSearchForm" method="POST" class="mb0">
  <div class="control-group mb0">
    <%-- <label for="" class="control-label"><spring:message code="label_filter" /></label> --%>
    <div class="">
      <div>
        <div class="mb10">
	      <input type="text" name="code" placeholder="<spring:message code="channel_code" />" class="input-small" />
	      <input type="text" name="description" placeholder="<spring:message code="channel_description" />" class="input-medium" />
	      </div>
	      <div class="mb0">
        <div class="input-daterange" id="duration">
          <fieldset class="row-fluid">
            <label><spring:message code="label_from" /></label>
            <input type="text" name="scheduleFrom" id="scheduleFrom" class="input-small" />
          </fieldset>
          <fieldset class="row-fluid">
            <label><spring:message code="label_to" /></label>
            <input type="text" name="scheduleTo" id="scheduleTo" class="input-small" />
          </fieldset>
        </div>
        <fieldset class="row-fluid mb0">
          <label><spring:message code="channel_createuser" /></label>
          <select name="createUser" class="input-medium mb0">
            <option/>
            <c:forEach var="item" items="${createUsers}"><option value="${item}">${item}</option></c:forEach>
          </select>
        </fieldset>
        <fieldset class="row-fluid mb0">
          <label><spring:message code="channel_status" /></label>
          <select name="status" class="input-medium mb0">
            <option/>
            <c:forEach var="item" items="${allStatus}"><option value="${item}"><spring:message code="channel_status_${item}" /></option></c:forEach>
          </select>
        </fieldset>
        <input id="btn_search" class="btn btn-primary" type="submit" value="<spring:message code="label_search" />" />
        <input id="resetBtn" type="button" class="btn" value="<spring:message code="label_reset" />" />
        </div>
      </div>
      
    </div>
  </div>
  </form>

<link href="<c:url value="/css/bootstrap/datepicker.css" />" rel="stylesheet" />
<script src='<c:url value="/js/bootstrap/bootstrap-datepicker.js"/>' type='text/javascript'><![CDATA[&nbsp; ]]></script>
<script src="<spring:url value="/js/jquery/jquery.toObject.js" />" ></script>
<script type="text/javascript">
var MarketingChannelSearchForm = null;

$(document).ready( function() {

	  var $form = $( "#marketingChannelSearchForm" );
	  var $duration = $form.find( "#duration" );
	  var $scheduleFrom = $form.find( "#scheduleFrom" );
	  var $scheduleTo = $form.find( "#scheduleTo" );
	  var $search = $form.find( "#btn_search" );
	  var $reset = $form.find( "#resetBtn" );

	  initFormFields();

	  function initFormFields() {
		    $duration.datepicker({ autoclose : true, format : "dd M yyyy" });
		    $form.submit( submit );

        $reset.click( function(e) {
        	  MarketingChannelCommon.reset( $form );
            $form.submit();
        });
	  }

	  function submit(e) {
		    e.preventDefault();
		    var filterForm = $(this).toObject( { mode : 'first', skipEmpty : true } );
		    filterForm.scheduleFrom = ( $scheduleFrom.val() )? $scheduleFrom.datepicker( "getDate" ).getTime() : null;
	      filterForm.scheduleTo = ( $scheduleTo.val() )? $scheduleTo.datepicker( "getDate" ).getTime() : null;
		    $search.prop( "disabled", true );
		    MarketingChannelList.filterTable( filterForm, function() { $search.prop( "disabled", false ); } );
	  }

});
</script>


</div>
