package com.transretail.crm.core.util;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.util.regex.Pattern;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.BlockJUnit4ClassRunner;

/**
 * @author Allan G. Ramirez (agramirez@exist.com)
 */
@RunWith(BlockJUnit4ClassRunner.class)
public class AppConstantsTest {

    @Test
    public void npwpIdPatternTest() {
        Pattern pattern = Pattern.compile(AppConstants.NPWP_ID_PATTERN);
        assertTrue(pattern.matcher("").matches());
        assertTrue(pattern.matcher("1.111.111.1-111.111").matches());
        assertTrue(pattern.matcher("01.111.111.1-111.111").matches());
        assertFalse(pattern.matcher("01.111.111.1-111").matches());
        assertFalse(pattern.matcher("01.111.111.1-111.11a").matches());
        assertFalse(pattern.matcher("0a.111.111.1-111.111").matches());
        assertFalse(pattern.matcher("0a.1111111.1-111.111").matches());

        assertTrue(pattern.matcher("11111111111111").matches());
        assertTrue(pattern.matcher("111111111111111").matches());
        assertFalse(pattern.matcher("1111111111111").matches());
        assertFalse(pattern.matcher("1111111111111111").matches());
    }
}
