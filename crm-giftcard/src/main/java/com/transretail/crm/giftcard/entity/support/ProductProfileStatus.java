package com.transretail.crm.giftcard.entity.support;


public enum ProductProfileStatus {
    APPROVED, REJECTED, SUBMITTED, DRAFT, DEACTIVATED
}
