package com.transretail.crm.giftcard.dto;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.transretail.crm.common.web.converter.ISODateSerializer;
import com.transretail.crm.core.entity.MemberModel;
import com.transretail.crm.giftcard.entity.GiftCardInventory;
import com.transretail.crm.giftcard.entity.ProductProfile;
import com.transretail.crm.giftcard.entity.support.GiftCardInventoryStatus;
import java.math.BigDecimal;
import org.joda.time.LocalDate;

/**
 * A Gift Card Data transfer object with a basic info, used in transferring data
 * between POS and webservice.
 *
 * @author Monte Cillo Co (mco@exist.com)
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
public class GiftCardInfo {

    private String currency;
    private Double faceValue = 0.0;
    private BigDecimal previousAvailableBalance = BigDecimal.ZERO;
    private BigDecimal availableBalance = BigDecimal.ZERO;
    private BigDecimal currentBalance = BigDecimal.ZERO;
    @JsonSerialize(using = ISODateSerializer.class)
    private LocalDate expireDate;
    private Profile profile;
    private String cardNumber;
    @JsonIgnore
    private Long series;
    private GiftCardInventoryStatus status;
    private String note = "";
    private String location;
    @JsonIgnore
    private boolean electronicGiftCard;
    private Member member;

    private String customer;
    private String discountType;

    public static GiftCardInfo convert(GiftCardInventory giftCardInventory) {
        final MemberModel owner = giftCardInventory.getOwningMember();
        Member member = null;
        if (owner != null) {
            member = new Member(owner);
        }

        return new GiftCardInfo().withAvailableBalance(giftCardInventory.getBalance())
                .withPreviousAvailableBalance(giftCardInventory.getPreviousBalance())
                .withCurrentBalance(giftCardInventory.getBalance())
                .withFaceValue((giftCardInventory.getFaceValue()) == null ? 0.0 : giftCardInventory.getFaceValue().doubleValue())
                .withCardNo(giftCardInventory.getBarcode())
                .withStatus(giftCardInventory.getStatus())
                .withExpireDate(giftCardInventory.getExpiryDate())
                .withProfile(Profile.convert(giftCardInventory.getProfile()))
                .withMember(member)
                .withSeries(giftCardInventory.getSeries())
                .withLocation(giftCardInventory.getLocation())
                .isElectronicGiftCard((giftCardInventory.getIsEgc() != null));
    }

    public GiftCardInfo withCurrency(String currency) {
        this.currency = currency;
        return this;
    }

    public GiftCardInfo withFaceValue(Double faceValue) {
        this.faceValue = faceValue;
        return this;
    }

    public GiftCardInfo withPreviousAvailableBalance(Double previousBalance) {
        return this.withPreviousAvailableBalance(new BigDecimal(previousBalance == null ? 0 : previousBalance));
    }

    public GiftCardInfo withPreviousAvailableBalance(BigDecimal previousBalance) {
        this.previousAvailableBalance = previousBalance;
        return this;
    }

    public GiftCardInfo withAvailableBalance(Double balance) {
        this.availableBalance = new BigDecimal(balance == null ? 0 : balance);
        return this;
    }

    public GiftCardInfo withAvailableBalance(BigDecimal balance) {
        this.availableBalance = balance;
        return this;
    }

    public GiftCardInfo withCurrentBalance(Double currentBalance) {
        this.currentBalance = new BigDecimal(currentBalance == null ? 0 : currentBalance);
        return this;
    }

    public GiftCardInfo withExpireDate(LocalDate expireDate) {
        this.expireDate = expireDate;
        return this;
    }

    public GiftCardInfo withStatus(GiftCardInventoryStatus status) {
        this.status = status;
        return this;
    }

    public GiftCardInfo withCardNo(String cardNo) {
        this.cardNumber = cardNo;
        return this;
    }

    public GiftCardInfo withSeries(Long series) {
        this.series = series;
        return this;
    }

    public GiftCardInfo withLocation(String location) {
        this.location = location;
        return this;
    }

    public GiftCardInfo isElectronicGiftCard(Boolean isEgc) {
        this.electronicGiftCard = isEgc;
        return this;
    }

    public GiftCardInfo withProfile(String code, String description) {
        this.profile = new Profile(code, description);
        return this;
    }

    public GiftCardInfo withMember(Long id, String accountId, String lastName, String firstName) {
        this.member = new Member(id, accountId, lastName, firstName);
        return this;
    }

    public GiftCardInfo withMember(Member owner) {
        this.member = owner;
        return this;
    }

    public Member getMember() {
        return member;
    }

    public GiftCardInfo withProfile(Profile profile) {
        this.profile = profile;
        return this;
    }

    public String getCurrency() {
        return currency;
    }

    public Double getFaceValue() {
        return faceValue;
    }

    public BigDecimal getPreviousAvailableBalance() {
        return previousAvailableBalance;
    }

    public BigDecimal getAvailableBalance() {
        return availableBalance;
    }

    public LocalDate getExpireDate() {
        return expireDate;
    }

    public String getCardNumber() {
        return cardNumber;
    }

    public Long getSeries() {
        return series;
    }

    public GiftCardInventoryStatus getStatus() {
        return status;
    }

    public String getNote() {
        return note;
    }

    public String getLocation() {
        return location;
    }

    public Boolean isElectronicGiftCard() {
        return electronicGiftCard;
    }

    public Profile getProfile() {
        return profile;
    }

    public BigDecimal getCurrentBalance() {
        return currentBalance;
    }

    public void setCurrentBalance(BigDecimal currentBalance) {
        this.currentBalance = currentBalance;
    }

    public void setElectronicGiftCard(boolean electronicGiftCard) {
        this.electronicGiftCard = electronicGiftCard;
    }

    public String getCustomer() {
        return customer;
    }

    public void setCustomer(String customer) {
        this.customer = customer;
    }

    public String getDiscountType() {
        return discountType;
    }

    public void setDiscountType(String discountType) {
        this.discountType = discountType;
    }

    public void setNote(String note) {
        this.note = note;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 29 * hash + (this.cardNumber != null ? this.cardNumber.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final GiftCardInfo other = (GiftCardInfo) obj;
        return !((this.cardNumber == null) ? (other.cardNumber != null) : !this.cardNumber.equals(other.cardNumber));
    }

    public static class Profile {

        private String code = ""; // refer to giftcardInventory.product_code
        private String description = ""; // refer to giftcardInventory.product_name	
        private boolean allowPartialRedeem;
        private boolean allowReload;
        private Long maxAmount;

        public Profile(String code, String description) {
            this.code = code;
            this.description = description;
        }

        public String getCode() {
            return code;
        }

        public String getDescription() {
            return description;
        }

        public boolean isAllowPartialRedeem() {
            return allowPartialRedeem;
        }

        public boolean isAllowReload() {
            return allowReload;
        }

        public Long getMaxAmount() {
            return maxAmount;
        }

        public Profile withCode(String code) {
            this.code = code;
            return this;
        }

        public Profile withDescription(String desc) {
            this.description = desc;
            return this;
        }

        public Profile withAllowPartialRedeem(boolean flag) {
            this.allowPartialRedeem = flag;
            return this;
        }

        public Profile withAllowReload(boolean flag) {
            this.allowReload = flag;
            return this;
        }

        public Profile withMaxAmount(Long maxAmount) {
            this.maxAmount = maxAmount;
            return this;
        }

        static Profile convert(ProductProfile productProfile) {
            return new Profile(productProfile.getProductCode(), productProfile.getProductDesc())
                    .withAllowPartialRedeem(productProfile.isAllowPartialRedeem())
                    .withAllowReload(productProfile.isAllowReload())
                    .withMaxAmount(productProfile.getMaxAmount());
        }
    }

    public static class Member {

        public Member(MemberModel member) {
            this(member.getId(), member.getAccountId(), member.getLastName(), member.getFirstName());
        }

        public Member(Long id, String accountId, String lastName, String firstName) {
            this.id = id;
            this.accountId = accountId;
            this.lastName = lastName;
            this.firstName = firstName;
        }

        private Long id = 0L;
        private final String accountId;
        private final String lastName;
        private final String firstName;

        public Long getId() {
            return id;
        }

        public String getAccountId() {
            return accountId;
        }

        public String getLastName() {
            return lastName;
        }

        public String getFirstName() {
            return firstName;
        }

    }
}
