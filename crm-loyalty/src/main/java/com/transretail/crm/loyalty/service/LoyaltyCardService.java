package com.transretail.crm.loyalty.service;


import java.io.File;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.joda.time.LocalDate;
import org.springframework.web.bind.annotation.RequestBody;

import com.transretail.crm.common.reporting.jasper.JRProcessor;
import com.transretail.crm.core.dto.MessageDto;
import com.transretail.crm.core.entity.lookup.LookupDetail;
import com.transretail.crm.loyalty.dto.LCInvResultList;
import com.transretail.crm.loyalty.dto.LCInvSearchDto;
import com.transretail.crm.loyalty.dto.LoyaltyCardInventoryResultList;
import com.transretail.crm.loyalty.dto.LoyaltyCardInventorySearchDto;
import com.transretail.crm.loyalty.dto.LoyaltyCardInventorySummaryResultList;
import com.transretail.crm.loyalty.dto.ManufactureOrderDto;
import com.transretail.crm.loyalty.dto.ManufactureOrderReceiveDto;
import com.transretail.crm.loyalty.dto.MultipleAllocationDto;
import com.transretail.crm.loyalty.dto.ReceiveOrderDto;
import com.transretail.crm.loyalty.entity.LoyaltyCardInventory;


public interface LoyaltyCardService {

	String generateCardNumber(String cardNumber);

	void generateLoyaltyInventory(ManufactureOrderDto orderDto);

	File printLoyaltyInventory(String moNo);

	void receiveOrders(ReceiveOrderDto orderDto);

	LoyaltyCardInventorySummaryResultList searchLoyaltyInventories(
		LoyaltyCardInventorySearchDto searchDto);

	LoyaltyCardInventory findLoyaltyCard(String inBarcodeNo);

	LoyaltyCardInventory activateLoyaltyCard(String inBarcodeNo, LocalDate expiryDate, Long memberId);

	LoyaltyCardInventory replaceLoyaltyCard(String inBarcodeNo);

	LoyaltyCardInventoryResultList searchInventoryItems(
			LoyaltyCardInventorySearchDto searchDto);

	List<LookupDetail> getCardTypes(String startingSeries, String endingSeries);

	void multipleAllocateInventory(MultipleAllocationDto dto, String batchRefNo);

	MultipleAllocationDto getInventoriesForMultipleApproval(
			String location);

	void allocateInventory(MultipleAllocationDto dto, String batchRefNo);

	MultipleAllocationDto getInventories(LookupDetail cardType,
			LookupDetail status, String location, String transferTo, String newTransferTo);

	Set<String> saveInventory(MultipleAllocationDto dto, LookupDetail newStatus);

	void receiveInventory(MultipleAllocationDto dto);

	boolean validateInventory(String location, String transferTo,
			LookupDetail cardType, String startingSeries, String endingSeries,
			List<LookupDetail> status);

	String getInventoryDesc(String inventoryCode);

	Map<String, String> getInventoryLocations();

	JRProcessor createAllocateJrProcessor(MultipleAllocationDto allocateDto);

	JRProcessor createReceiveJrProcessor(MultipleAllocationDto allocateDto);

	MessageDto saveForBurning(MultipleAllocationDto dto, Boolean forBurning, String batchRefNo);

	List<ManufactureOrderReceiveDto> getReceivedInventories(
			Long manufactureOrder);

    Boolean isCardExpired(LoyaltyCardInventory loyaltyCardInventory);

    LCInvResultList searchAll(@RequestBody LCInvSearchDto searchDto);

	void expirePoints(LocalDate expireDate);

	void expirePoints();

	JRProcessor createReprintJrProcessor(String transactionType,
			String username, LocalDate dateFrom, LocalDate dateTo);

	Map<String, String> getInventoryLocationsForTracking();

	List<MessageDto> sendAllocateToChangeForApproval(MultipleAllocationDto dto, String batchRefNo);

	Set<String> approveAllocateToChange(MultipleAllocationDto dto);

	Long[] countIncorrectTransferTo(MultipleAllocationDto dto);

	Set<String> rejectAllocateToChange(MultipleAllocationDto dto);

	Set<String> burnInventory(MultipleAllocationDto dto, Boolean approved);

	MultipleAllocationDto getInventoriesForBurning();

	int getExpiryInMonths();

}
