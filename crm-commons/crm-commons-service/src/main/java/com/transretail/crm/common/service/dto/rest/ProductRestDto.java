package com.transretail.crm.common.service.dto.rest;


import java.io.Serializable;
import java.util.Date;


public class ProductRestDto implements Serializable {



    private String pluId;


    private String name;

    private Long promotionId;

    private String promotionName;

    private Date promotionStartDate;

    private Date promotionEndDate;

    private Double points;

    public String getPluId() {
        return pluId;
    }

    public void setPluId(String pluId) {
        this.pluId = pluId;
    }



    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Double getPoints() {
        return points;
    }

    public void setPoints(Double points) {
        this.points = points;
    }

    public Long getPromotionId() {
        return promotionId;
    }

    public void setPromotionId(Long promotionId) {
        this.promotionId = promotionId;
    }

    public String getPromotionName() {
        return promotionName;
    }

    public void setPromotionName(String promotionName) {
        this.promotionName = promotionName;
    }

    public Date getPromotionStartDate() {
        return promotionStartDate;
    }

    public void setPromotionStartDate(Date promotionStartDate) {
        this.promotionStartDate = promotionStartDate;
    }

    public Date getPromotionEndDate() {
        return promotionEndDate;
    }

    public void setPromotionEndDate(Date promotionEndDate) {
        this.promotionEndDate = promotionEndDate;
    }
}
