package com.transretail.crm.web.controller;


import java.io.IOException;
import java.io.OutputStream;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import com.transretail.crm.common.service.dto.response.ResultList;
import com.transretail.crm.core.dto.ApprovalRemarkDto;
import com.transretail.crm.core.dto.FileDto;
import com.transretail.crm.core.dto.MarketingChannelDto;
import com.transretail.crm.core.dto.MarketingChannelSearchDto;
import com.transretail.crm.core.dto.PromotionSearchDto;
import com.transretail.crm.core.entity.MarketingChannel;
import com.transretail.crm.core.entity.MarketingChannel.ChannelStatus;
import com.transretail.crm.core.entity.MarketingChannel.ChannelType;
import com.transretail.crm.core.entity.MemberModel;
import com.transretail.crm.core.security.util.UserUtil;
import com.transretail.crm.core.service.CodePropertiesService;
import com.transretail.crm.core.service.CrmFileService;
import com.transretail.crm.core.service.MarketingChannelService;
import com.transretail.crm.core.service.MemberGroupService;
import com.transretail.crm.core.service.MemberService;
import com.transretail.crm.core.service.PromotionService;
import com.transretail.crm.web.controller.util.ControllerResponse;
import com.transretail.crm.web.controller.util.ControllerResponseExceptionHandler;


@RequestMapping("/marketing/channel")
@Controller
public class MarketingChannelController extends ControllerResponseExceptionHandler {

	private static final String PATH_CHANNEL 				= "/marketing/channel";
	private static final String PATH_CHANNEL_EMAIL			= "/marketing/channel/email";
	private static final String PATH_CHANNEL_SMS			= "/marketing/channel/sms";
	private static final String PATH_CHANNEL_VIEW			= "/marketing/channel/view";
	private static final String PATH_CHANNEL_PROCESS		= "/marketing/channel/process";
	private static final String UIATTR_FORM_FILETYPE 		= "text/html";

	private static final String UIATTR_FORM		 			= "marketingChannel";
	private static final String UIATTR_FORMREMARK 			= "marketingChannelRemark";
	private static final String UIATTR_ACTION_PROCESS		= "processAction";

	private static final String UIATTR_FORM_EMAIL 			= "emailDraft";
	private static final String UIATTR_ACTION_EMAILSAVE		= PATH_CHANNEL + "/email/save";
	private static final String UIATTR_ACTION_EMAILUPDATE	= PATH_CHANNEL + "/email/update";
	private static final String UIATTR_ACTION_EMAIL			= "emailAction";
	private static final String UIATTR_LBL_EMAILHEADER		= "emailDialogHeader";

	private static final String UIATTR_FORM_SMS 			= "smsDraft";
	private static final String UIATTR_ACTION_SMSSAVE		= PATH_CHANNEL + "/sms/save";
	private static final String UIATTR_ACTION_SMSUPDATE		= PATH_CHANNEL + "/sms/update";
	private static final String UIATTR_ACTION_SMS			= "smsAction";
	private static final String UIATTR_LBL_SMSHEADER		= "smsDialogHeader";

	private static final String UIATTR_MEMBRERGROUPS		= "memberGroups";
	private static final String UIATTR_PROMOTIONS			= "promotions";
	private static final String UIATTR_CREATEUSERS			= "createUsers";
	private static final String UIATTR_ALLSTATUS			= "allStatus";

	private static final String UIATTR_MAXCOUNT				= "maxCount";
	private static final String UIATTR_PERCOUNT				= "perCount";



	@Autowired
	MarketingChannelService service;
	@Autowired
	CrmFileService fileService;
	@Autowired
	PromotionService promotionService;
	@Autowired
	MemberGroupService memberGroupService;
	@Autowired
	MemberService memberService;
	@Autowired
	CodePropertiesService codePropertiesService;
    @Autowired
    private MessageSource messageSource;



    @InitBinder
	void initBinder( WebDataBinder binder ) {
		DateFormat theFormat = new SimpleDateFormat( "dd MMM yyyy" );
		theFormat.setLenient( false );
		binder.registerCustomEditor( Date.class, new CustomDateEditor( theFormat, true ) );
	}

    @RequestMapping( produces = "text/html" )
    public String showMain( Model uiModel ) {
    	populateEmailDraftFields( uiModel, null, true );
    	populateSmsDraftFields( uiModel, null, true );
    	populateSearchFields( uiModel );
    	populateListStatFields( uiModel );

    	return PATH_CHANNEL;
    }

    @RequestMapping(value = "/list", method = RequestMethod.POST)
    public @ResponseBody ResultList<MarketingChannelDto> list( @RequestBody MarketingChannelSearchDto dto ) {
    	return service.search( dto );
    }

    @RequestMapping( value="/view/{id}", produces = "text/html", method = RequestMethod.GET )
    public String view( @RequestParam(value="process", required = false) boolean process, @PathVariable(value="id") String id, Model uiModel ) {
    	MarketingChannelDto dto = service.getDto( Long.valueOf(id) );
    	uiModel.addAttribute( UIATTR_FORM, dto );

    	if ( process && UserUtil.getCurrentUser().hasAuthority( codePropertiesService.getMktHeadCode() ) ) {
    		populateListStatFields( uiModel );
    		uiModel.addAttribute( UIATTR_FORMREMARK, new ApprovalRemarkDto() );
    		uiModel.addAttribute( UIATTR_ACTION_PROCESS, PATH_CHANNEL_PROCESS + "/" + id );
    		return PATH_CHANNEL_PROCESS;
    	}

    	return PATH_CHANNEL_VIEW;
    }

    @RequestMapping( value="/edit/{id}", produces = "text/html", method = RequestMethod.GET )
    public String edit( @PathVariable(value="id") String id, Model uiModel ) {
    	MarketingChannelDto dto = service.getDto( Long.valueOf(id) );
    	if ( null != dto && null != dto.getType() && dto.getType().equals( ChannelType.email ) ) {
        	populateEmailDraftFields( uiModel, dto, false );
        	return PATH_CHANNEL_EMAIL;
    	}
    	else if ( null != dto && null != dto.getType() && dto.getType().equals( ChannelType.sms ) ) {
        	populateSmsDraftFields( uiModel, dto, false );
        	return PATH_CHANNEL_SMS;
    	}

    	return PATH_CHANNEL;
    }

    @RequestMapping( value="/update/{id}", produces="application/json; charset=utf-8", method=RequestMethod.POST )
    public @ResponseBody ControllerResponse updateStatus(
    		@RequestParam(value="status", required = false) String status,
    		@PathVariable(value="id") String id, 
    		@ModelAttribute(UIATTR_FORM_EMAIL) MarketingChannelDto dto, 
			BindingResult result ) {

    	ControllerResponse resp = new ControllerResponse();

    	dto = service.getDto( Long.valueOf(id) );
    	dto.setStatus( ChannelStatus.valueOf(status) );
    	service.updateDto( dto );
    	resp.setSuccess( true );

    	return resp;
    }

    @RequestMapping( value="/email/save", produces="application/json; charset=utf-8", headers = "content-type=multipart/*", method=RequestMethod.POST )
	public @ResponseBody ControllerResponse saveEmail(
			@ModelAttribute(UIATTR_FORM_EMAIL) MarketingChannelDto dto, 
			BindingResult result,
            MultipartHttpServletRequest fileReq  ) {

    	ControllerResponse resp = new ControllerResponse();
		if ( isValid( result, dto ) ) {
			dto.setType(ChannelType.email);
    		resp.setResult( service.saveFileAndDto( dto, true ) );
    		resp.setSuccess( true );
		}
		return populateErrors( resp, result );
	}

    @RequestMapping( value="/email/update", produces="application/json; charset=utf-8", headers = "content-type=multipart/*", method=RequestMethod.POST )
	public @ResponseBody ControllerResponse updateEmail(
			@ModelAttribute(UIATTR_FORM_EMAIL) MarketingChannelDto dto, 
			BindingResult result,
            MultipartHttpServletRequest fileReq  ) {

    	ControllerResponse resp = new ControllerResponse();
		if ( isValid( result, dto ) ) {
    		resp.setResult( service.updateFileAndDto( dto ) );
    		resp.setSuccess( true );
		}
		return populateErrors( resp, result );
	}

    @RequestMapping( value="/sms/save", produces="application/json; charset=utf-8", method=RequestMethod.POST )
	public @ResponseBody ControllerResponse saveSms(
			@ModelAttribute(UIATTR_FORM_SMS) MarketingChannelDto dto, 
			BindingResult result ) {

    	ControllerResponse resp = new ControllerResponse();
		if ( isValid( result, dto ) ) {
			dto.setType(ChannelType.sms);
    		resp.setResult( service.saveDto( dto, true ) );
    		resp.setSuccess( true );
		}
		return populateErrors( resp, result );
	}

    @RequestMapping( value="/sms/update", produces="application/json; charset=utf-8", method=RequestMethod.POST )
	public @ResponseBody ControllerResponse updateSms(
			@ModelAttribute(UIATTR_FORM_SMS) MarketingChannelDto dto, 
			BindingResult result ) {

    	ControllerResponse resp = new ControllerResponse();
		if ( isValid( result, dto ) ) {
    		resp.setResult( service.updateDto( dto ) );
    		resp.setSuccess( true );
		}
		return populateErrors( resp, result );
	}

    @RequestMapping( value="/process/{id}", produces="application/json; charset=utf-8", method=RequestMethod.POST )
    public @ResponseBody ControllerResponse process( 
    		@PathVariable(value="id") String id, 
			@ModelAttribute(UIATTR_FORMREMARK) ApprovalRemarkDto dto, 
			BindingResult result ) {

    	ControllerResponse resp = new ControllerResponse();
    	resp.setResult( service.saveRemarksDto( dto ) );    	
		resp.setSuccess( true );
		return populateErrors( resp, result );
    }

    @RequestMapping( value="/target/member/count", produces="application/json; charset=utf-8", method=RequestMethod.GET )
	public @ResponseBody ControllerResponse getMemberCount( 
    		@RequestParam(value="memberGrp", required = false) String memberGroups,
    		@RequestParam(value="promo", required = false) String promotions ) {

		ControllerResponse resp = new ControllerResponse();
		if ( StringUtils.isNotBlank( memberGroups ) || StringUtils.isNotBlank( promotions ) ) {
			resp.setResult( service.countTargetMembers( memberGroups, promotions )) ;
		}
		else {
			resp.setResult( 0 );
		}
		resp.setSuccess(true);
		return resp;
	}

    @RequestMapping( value="/file/preview/uploaded/{id}")
    public void preview(
    		@PathVariable(value="id") long fileId, 
    		final HttpServletResponse res, 
    		HttpServletRequest req) throws Exception {

    	MultipartFile file = fileService.getMultipartFile( fileId );
    	OutputStream out = null;
        try {
        	res.setHeader( "Content-Disposition", "inline;filename=\"" + file.getOriginalFilename() + "\"" );
        	out = res.getOutputStream();
            res.setContentType( file.getContentType() );
            IOUtils.copy( file.getInputStream(), out);
        } 
        catch ( IOException e ) {
            e.printStackTrace();//TODO
        }
        finally {
        	try { out.flush(); } catch( Exception e ){}
        	try { out.close(); } catch( Exception e ){}
        }
    }

    @RequestMapping( value="/file/preview/forupload", headers = "content-type=multipart/*", method=RequestMethod.POST )
    public void preview(
			@ModelAttribute("fileForUpload") FileDto dto, 
			BindingResult result,
    		final HttpServletResponse res,
            MultipartHttpServletRequest req ) throws Exception {

    	if ( null == dto || null == dto.getFile() ) {
    		return;
    	}

    	MultipartFile file = dto.getFile();
    	OutputStream out = null;
        try {
        	res.setHeader( "Content-Disposition", "inline;filename=\"" + file.getOriginalFilename() + "\"" );
        	out = res.getOutputStream();
            res.setContentType( file.getContentType() );
            IOUtils.copy( file.getInputStream(), out);
        } 
        catch ( IOException e ) {
            e.printStackTrace();//TODO
        }
        finally {
        	try { out.flush(); } catch( Exception e ){}
        	try { out.close(); } catch( Exception e ){}
        }
    }



    private boolean isValid( BindingResult result, MarketingChannelDto dto ) {
    	if ( StringUtils.isBlank( dto.getMemberGroup() ) && StringUtils.isBlank( dto.getPromotion() ) ) {
    		result.reject( messageSource.getMessage( "channel_err_targetreqd", null, LocaleContextHolder.getLocale() ) );
    	}
    	if ( null != dto.getFile() && dto.getFile().getSize() != 0 && !StringUtils.isBlank( dto.getFile().getContentType() ) 
    			&& !dto.getFile().getContentType().equalsIgnoreCase( UIATTR_FORM_FILETYPE ) ) {
    		result.reject( messageSource.getMessage( "channel_err_invalidfile", null, LocaleContextHolder.getLocale() ) );
    	}
    	if ( StringUtils.isBlank( dto.getDescription() ) ) {
    		result.reject( messageSource.getMessage( "channel_err_descforsubject", null, LocaleContextHolder.getLocale() ) );
    	}

    	return !result.hasErrors();
    }

    private void populateEmailDraftFields( Model uiModel, MarketingChannelDto dto, boolean isNew ) {
    	uiModel.addAttribute( UIATTR_FORM_EMAIL, isNew? new MarketingChannelDto() : ( dto != null )? dto : new MarketingChannelDto() );
    	uiModel.addAttribute( UIATTR_ACTION_EMAIL, isNew? UIATTR_ACTION_EMAILSAVE : UIATTR_ACTION_EMAILUPDATE );
    	uiModel.addAttribute( UIATTR_LBL_EMAILHEADER, messageSource.getMessage( isNew? "channel_email_draft" : "channel_email_edit", null, null ) );
    	uiModel.addAttribute( UIATTR_MAXCOUNT, MarketingChannel.MESSAGE_COUNT );
    	uiModel.addAttribute( "fileForUpload", new FileDto() );
    	populateDraftFields( uiModel );
    }

    private void populateSmsDraftFields( Model uiModel, MarketingChannelDto dto, boolean isNew ) {
    	uiModel.addAttribute( UIATTR_FORM_SMS, isNew? new MarketingChannelDto() : ( dto != null )? dto : new MarketingChannelDto() );
    	uiModel.addAttribute( UIATTR_ACTION_SMS, isNew? UIATTR_ACTION_SMSSAVE : UIATTR_ACTION_SMSUPDATE );
    	uiModel.addAttribute( UIATTR_LBL_SMSHEADER, messageSource.getMessage( isNew? "channel_sms_draft" : "channel_sms_edit", null, null ) );
    	uiModel.addAttribute( UIATTR_MAXCOUNT, MarketingChannel.MESSAGE_COUNT );
    	uiModel.addAttribute( UIATTR_PERCOUNT, MarketingChannel.PER_COUNT );
    	populateDraftFields( uiModel );
    }

    private void populateDraftFields( Model uiModel ) {
    	uiModel.addAttribute( UIATTR_MEMBRERGROUPS, memberGroupService.getAllMemberGroups() );
    	PromotionSearchDto dto = new PromotionSearchDto();
    	dto.setStatus( codePropertiesService.getDetailStatusActive() );
    	uiModel.addAttribute( UIATTR_PROMOTIONS, promotionService.searchPromotion( dto ).getResults() );
    }

    private void populateSearchFields( Model uiModel ) {
    	uiModel.addAttribute( UIATTR_CREATEUSERS, service.getCreateUsers() );
    	uiModel.addAttribute( UIATTR_ALLSTATUS, MarketingChannel.ChannelStatus.values() );
    }

    private void populateListStatFields( Model uiModel ) {
    	uiModel.addAttribute( ChannelStatus.DRAFT.toString().toLowerCase(), ChannelStatus.DRAFT );
    	uiModel.addAttribute( ChannelStatus.FORAPPROVAL.toString().toLowerCase(), ChannelStatus.FORAPPROVAL );
    	uiModel.addAttribute( ChannelStatus.ACTIVE.toString().toLowerCase(), ChannelStatus.ACTIVE );
    	uiModel.addAttribute( ChannelStatus.CANCELED.toString().toLowerCase(), ChannelStatus.CANCELED );
    	uiModel.addAttribute( ChannelStatus.REJECTED.toString().toLowerCase(), ChannelStatus.REJECTED );
    }

    private ControllerResponse populateErrors( ControllerResponse resp, BindingResult result ) {
    	if ( result.hasErrors() ) {
    		resp.setResult( result.getAllErrors() );
    		resp.setSuccess( false );
    	}
    	return resp;
    }

}
