package com.transretail.crm.giftcard.entity;

import com.transretail.crm.core.entity.MemberModel;
import com.transretail.crm.core.entity.support.CustomAuditableEntity;
import com.transretail.crm.giftcard.entity.support.GiftCardInventoryStatus;
import com.transretail.crm.giftcard.entity.support.GiftCardSalesType;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.*;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.hibernate.annotations.Type;
import org.joda.time.DateTime;
import org.joda.time.LocalDate;

/**
 *
 */
@Entity
@Table(name = "CRM_GC_INVENTORY")
@SecondaryTable(name = GiftCardInventory.CRM_MEMBER_GIFT_CARD,
                pkJoinColumns = @PrimaryKeyJoinColumn(name = "GC_ID"))
public class GiftCardInventory extends CustomAuditableEntity<Long> implements Serializable {

    private static final long serialVersionUID = 1L;
    protected static final String CRM_MEMBER_GIFT_CARD = "CRM_MEMBER_GIFT_CARD";

    @Transient
    public static final String SERIES_DATETIME_FORMAT = "yyMMdd";
    @Transient
    public static final int FACE_VALUE_MULTIPLIER = 100000;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "PROFILE_ID", nullable = false) // SKU - gc id for now
    private ProductProfile profile;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "ORDER_ID", nullable = false)
    private GiftCardOrder order;
    /**
     * Must be equal to ProductProfile#name
     */
    @Column(name = "PRODUCT_NAME", length = 100, nullable = false)
    private String productName;
    @Column(name = "PRODUCT_CODE", length = 100, nullable = false)
    private String productCode;
    @Column(name = "SEQ_NO", nullable = false)
    private Integer seqNo;
    @Column(name = "BATCH_NO", nullable = false)
    private Integer batchNo;
    @Column(name = "ORDER_DATE", nullable = false)
    @Type(type = "org.jadira.usertype.dateandtime.joda.PersistentLocalDate")
    private LocalDate orderDate;
    @Column(name = "STATUS")
    @Enumerated(EnumType.STRING)
    private GiftCardInventoryStatus status;
    /**
     * Format: yyMMddbbfsssssss<br>
     * where:
     * <ul>
     * <li>yy - last 2 digits of year in orderDate</li>
     * <li>MM - month in orderDate</li>
     * <li>dd - day in orderDate</li>
     * <li>bb - batch number</li>
     * <li>f - face value per 100k</li>
     * <li>sssssss - series digits</li>
     * </ul>
     */
    @Column(name = "SERIES", nullable = false, length = 16, unique = true)
    private Long series;
    @Column(name = "BARCODE", nullable = false, length = 20, unique = true)
    private String barcode; // series + 3 random digits + 1 luhn check
    @Column(name = "LOCATION")
    private String location;
    @Column(name = "FACE_VALUE", precision = 12, scale = 4)
    private BigDecimal faceValue;
    @Column(name = "EXPIRY_DATE")
    @Type(type = "org.jadira.usertype.dateandtime.joda.PersistentLocalDate")
    private LocalDate expiryDate;
    @Column(name = "ALLOCATE_TO")
    private String allocateTo;
    @Column(name = "BATCH_REF_NO")
    private String batchRefNo;
    @Column(name = "BALANCE")
    private Double balance;
    @Column(name = "PREV_BALANCE")
    private Double previousBalance;
    @Column(name = "REFUNDABLE")
    private Boolean refundable;
    @Column(name = "ACTIVATION_DATE")
    @Type(type = "org.jadira.usertype.dateandtime.joda.PersistentLocalDate")
    private LocalDate activationDate;
    @Column(name = "RECEIVE_DATE")
    @Type(type = "org.jadira.usertype.dateandtime.joda.PersistentLocalDate")
    private LocalDate receiveDate;
    @Column(name = "RECEIPT_NO")
    private String deliveryReceipt;
    @Column(name = "BOX_NO")
    private String boxNo;
    @Column(name = "REDEMPTION_DATE")
    @Type(type = "org.jadira.usertype.dateandtime.joda.PersistentLocalDate")
    private LocalDate redemptionDate;

    @Column(name = "RECEIVED_BY")
    private String receivedBy;
    @Column(name = "SALES_TYPE")
    @Enumerated(EnumType.STRING)
    private GiftCardSalesType salesType;

    @Column(name = "RYNS")
    @Type(type = "yes_no")
    private Boolean ryns;

    @Column(name = "IS_EGC", length = 1)
    @Type(type = "yes_no")
    private Boolean isEgc;

    @Column(name = "IS_FOR_PROMO", length = 1)
    @Type(type = "yes_no")
    private Boolean isForPromo;
    @Embedded
    private EGiftCardInfo egcInfo;
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "MEMBER_ID", table = CRM_MEMBER_GIFT_CARD, nullable = false)
    private MemberModel owningMember;
    @Column(name = "ACCOUNT_ID", table = CRM_MEMBER_GIFT_CARD)
    private String accountId;
    @Column(name = "REG_DATE", table = CRM_MEMBER_GIFT_CARD)
    @Type(type = "org.jadira.usertype.dateandtime.joda.PersistentDateTime")
    private DateTime registrationDate;

    public MemberModel getOwningMember() {
        return owningMember;
    }

    public void setOwningMember(MemberModel owningMember) {
        this.owningMember = owningMember;
    }

    public String getAccountId() {
        return accountId;
    }

    public void setAccountId(String accountId) {
        this.accountId = accountId;
    }

    public DateTime getRegistrationDate() {
        return registrationDate;
    }

    public void setRegistrationDate(DateTime registrationDate) {
        this.registrationDate = registrationDate;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public Integer getSeqNo() {
        return seqNo;
    }

    public void setSeqNo(Integer seqNo) {
        this.seqNo = seqNo;
    }

    public Integer getBatchNo() {
        return batchNo;
    }

    public void setBatchNo(Integer batchNo) {
        this.batchNo = batchNo;
    }

    public LocalDate getOrderDate() {
        return orderDate;
    }

    public void setOrderDate(LocalDate orderDate) {
        this.orderDate = orderDate;
    }

    public GiftCardInventoryStatus getStatus() {
        return status;
    }

    public void setStatus(GiftCardInventoryStatus status) {
        this.status = status;
    }

    public Long getSeries() {
        return series;
    }

    public void setSeries(Long series) {
        this.series = series;
    }

    public String getBarcode() {
        return barcode;
    }

    public void setBarcode(String barcode) {
        this.barcode = barcode;
    }

    public BigDecimal getFaceValue() {
        return faceValue;
    }

    public void setFaceValue(BigDecimal faceValue) {
        this.faceValue = faceValue;
    }

    public LocalDate getExpiryDate() {
        return expiryDate;
    }

    public void setExpiryDate(LocalDate expiryDate) {
        this.expiryDate = expiryDate;
    }

    public Double getBalance() {
        return balance;
    }

    public void setBalance(Double balance) {
        this.balance = balance;
    }

    public LocalDate getActivationDate() {
        return activationDate;
    }

    public void setActivationDate(LocalDate activationDate) {
        this.activationDate = activationDate;
    }

    public ProductProfile getProfile() {
        return profile;
    }

    public void setProfile(ProductProfile profile) {
        this.profile = profile;
    }

    public String getBatchRefNo() {
        return batchRefNo;
    }

    public void setBatchRefNo(String batchRefNo) {
        this.batchRefNo = batchRefNo;
    }

    public String getProductCode() {
        return productCode;
    }

    public void setProductCode(String productCode) {
        this.productCode = productCode;
    }

    public Double getPreviousBalance() {
        return previousBalance;
    }

    public void setPreviousBalance(Double previousBalance) {
        this.previousBalance = previousBalance;
    }

    public GiftCardOrder getOrder() {
        return order;
    }

    public void setOrder(GiftCardOrder order) {
        this.order = order;
    }

    public LocalDate getReceiveDate() {
        return receiveDate;
    }

    public void setReceiveDate(LocalDate receiveDate) {
        this.receiveDate = receiveDate;
    }

    public String getDeliveryReceipt() {
        return deliveryReceipt;
    }

    public void setDeliveryReceipt(String deliveryReceipt) {
        this.deliveryReceipt = deliveryReceipt;
    }

    public Boolean isRefundable() {
        return refundable;
    }

    public void setRefundable(Boolean refundable) {
        this.refundable = refundable;
    }

    public LocalDate getRedemptionDate() {
        return redemptionDate;
    }

    public void setRedemptionDate(LocalDate redemptionDate) {
        this.redemptionDate = redemptionDate;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getAllocateTo() {
        return allocateTo;
    }

    public void setAllocateTo(String allocateTo) {
        this.allocateTo = allocateTo;
    }

    public String getBoxNo() {
        return boxNo;
    }

    public void setBoxNo(String boxNo) {
        this.boxNo = boxNo;
    }

    public String getReceivedBy() {
        return receivedBy;
    }

    public void setReceivedBy(String receivedBy) {
        this.receivedBy = receivedBy;
    }

    public GiftCardSalesType getSalesType() {
        return salesType;
    }

    public void setSalesType(GiftCardSalesType salesType) {
        this.salesType = salesType;
    }

    public Boolean getRyns() {
        return ryns;
    }

    public void setRyns(Boolean ryns) {
        this.ryns = ryns;
    }

    public Boolean getIsEgc() {
        return isEgc;
    }

    public void setIsEgc(Boolean isEgc) {
        this.isEgc = isEgc;
    }

	public Boolean getIsForPromo() {
		return isForPromo;
	}

	public void setIsForPromo(Boolean isForPromo) {
		this.isForPromo = isForPromo;
	}

    public EGiftCardInfo getEgcInfo() {
        return egcInfo;
    }

    public void setEgcInfo(EGiftCardInfo egcInfo) {
        this.egcInfo = egcInfo;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;

        if (o == null || getClass() != o.getClass()) return false;

        GiftCardInventory that = (GiftCardInventory) o;

        return new EqualsBuilder()
                .append(barcode, that.barcode)
                .isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 37)
                .append(barcode)
                .toHashCode();
    }
}
