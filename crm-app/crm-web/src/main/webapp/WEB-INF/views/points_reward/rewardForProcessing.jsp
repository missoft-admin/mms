<%@ include file="../common/taglibs.jsp" %>

    <div class="page-header page-header2">
      <h1><spring:message code="menutab_employeemgmt_manager_rewardforapproval" /></h1>
  </div>

  <jsp:include page="rewardSearchForm.jsp"/>

  <div id="rewardForProcessing">

    <jsp:include page="rewardForProcessingButtons.jsp" />

	  <jsp:include page="rewardForProcessingList.jsp" />

  </div>

  <script type="text/javascript" src="<c:url value="/js/viewspecific/points_reward/reward.js" />"><![CDATA[&nbsp;]]></script>

