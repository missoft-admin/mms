$(document).ready( function() {

	var errorContainer = $( "#content_productGroup" ).find("#content_error");

	var $dialog = $( "#content_productGroup" );

	var $searchForm = $( "#searchProductGroupForm" );
	var $division = $searchForm.find( "#division" );
	var $department = $searchForm.find( "#department" );
	var $group = $searchForm.find( "#group" );
	var $family = $searchForm.find( "#family" );
	var $subfamily = $searchForm.find( "#subfamily" );



	initDialog();
	initLinks();
	initExternalRef();
	initFormFields();
	initViewGroup();
	//initSearchFormFields();	



	function initDialog() {
		$dialog.modal({ show : false });
		$dialog.on( "hide", function(e) { 
			if ( e.target === this ) { 
				errorContainer.hide();
				resetForm( $searchForm );
				$( "#viewProductGroup" ).empty();
			} 
		});
		$dialog.on( "show", function(e) { 
			if ( e.target === this ) {
				setGroupSelections();
			} 
		});
	}

	function initLinks() {
		$( "#link_createProductGroup" ).click( function(e) {
			e.preventDefault();
			$.get( $(this).attr( "href" ), function(resp) {
				$( "#form_productGroup" ).html( resp );
				$dialog.modal( "hide" );

				/*productGroupForm.jsp => id:content_productGroupForm*/
				$( "#content_productGroupForm" ).modal( "show" );
				$( "#content_productGroupForm" ).on( "hide", function(e) {
					$dialog.modal( "show" );
				});
			}, "html" );
		});
	}

	function initExternalRef() {
		/*target.jsp => id:productGroupLnk*/
		$( "#productGroupLnk" ).click( function(e) {
			e.preventDefault();
			$dialog.modal( "show" );
		});
	}



	function initFormFields() {
		$( "#productGroups" ).dblclick( function(e) {
			$( "#addGroupBtn" ).trigger( "click" );
		});
		$( "#selectedProductGroups" ).dblclick( function(e) {
			$( "#removeGroupBtn" ).trigger( "click" );
		});

		$( "#addGroupBtn" ).click( function(e) {
			e.preventDefault();
			$.each( $( "#productGroups" ).find("option:selected"), function() { $( "#selectedProductGroups" ).append( $(this) ); });
		});
		$( "#removeGroupBtn" ).click( function(e) {
			e.preventDefault();
			$.each( $( "#selectedProductGroups" ).find("option:selected"), function() { $( "#productGroups" ).append( $(this) ); });
		});

		$dialog.find( "#saveButton" ).click( function(e) {
			e.preventDefault();
			$( "#targetProductGroups" ).empty();
			$.each( $( "#selectedProductGroups" ).find("option"), function() { $( "#targetProductGroups" ).append( $(this) ); });
			$( "#targetProductGroups" ).val( $( "#targetProductGroups" ).find( "option" ).map( function(){ return this.value; } ).get() );
			$("#targetProductGroups").change();
			$dialog.modal( "hide" );
		});

		$( "#targetProductGroups" ).val( $( "#targetProductGroups" ).find( "option" ).map( function(){ return this.value; } ).get() );
	}

	function initViewGroup() {
		$( "#link_viewProductGroup" ).click( function(e) {
			e.preventDefault();
			$.get( $(this).attr( "href" ) + $(this).data("id"), function(resp) {
				$( "#viewProductGroup" ).html( resp );
			}, "html" );
			$( "#link_viewProductGroup" ).hide();
		});

		$( ".select_viewProductGroup" ).change( function(e) {
			$( "#link_viewProductGroup" ).data( "id", this.value );
			$( "#link_viewProductGroup" ).show();
		});
	}



	function initSearchFormFields() {
		$( ".linkShowAll" ).each( function() {
			var $this = $(this);
			$this.click( function(e) {
				e.preventDefault();
				$.get( $this.attr("href").replace( new RegExp( "%CODE%", "g" ), "" ), function(resp) {
					$( "#" + $this.data("id") ).empty();
					$.each( resp.result, function() { $( "#" + $this.data("id") ).append( new Option( this.code + " - " + this.description, this.code ) ); });
					$( "#" + $this.data("id") ).val("");
					$( ".linkShowAll" ).each( function() {
						if ( $(this).data("id") != $this.data("id") ) {
							$( "#" + $(this).data("id") ).empty();
						}
					});
				});
			});
		});

		$division.change( function(e) {
			e.preventDefault();
			$department.empty();
			$.get( $department.data("url").replace( new RegExp( "%CODE%", "g" ), $(this).val() ), function(resp) {
				$.each( resp.result, function() { $department.append( new Option( this.code + " - " + this.description, this.code ) ); });
				$department.val("");
			}); 
		});

		$department.change( function(e) {
			e.preventDefault();
			$group.empty();
			$.get( $group.data("url").replace( new RegExp( "%CODE%", "g" ), $(this).val() ), function(resp) {
				$.each( resp.result, function() { $group.append( new Option( this.code + " - " + this.description, this.code ) ); });
				$group.val("");
			}); 
		});

		$group.change( function(e) {
			e.preventDefault();
			$family.empty();
			$.get( $family.data("url").replace( new RegExp( "%CODE%", "g" ), $(this).val() ), function(resp) {
				$.each( resp.result, function() { $family.append( new Option( this.code + " - " + this.description, this.code ) ); });
				$family.val("");
			}); 
		});

		$family.change( function(e) {
			e.preventDefault();
			$subfamily.empty();
			$.get( $subfamily.data("url").replace( new RegExp( "%CODE%", "g" ), $(this).val() ), function(resp) {
				$.each( resp.result, function() { $subfamily.append( new Option( this.code + " - " + this.description, this.code ) ); });
				$subfamily.val("");
			}); 
		});

		$searchForm.submit( function(e) {
			e.preventDefault();
			$( "#filterButton" ).prop( "disabled", true );

			$.post( $searchForm.attr( "action" ), 
				$searchForm.serialize(), 
				function( resp ) {
					if ( resp.success ) {}
							
			});
		});
	}



	function setGroupSelections() {
		/*promotionForm.jsp => id:targetProductGroups*/
		var promoTargetPrdGrp = $( "#targetProductGroups" ).find( "option" ).map( function(){ return this.value; } ).get();

		$.get( $( "#productGroups" ).data( "url" ), function(resp) {
			$( "#selectedProductGroups" ).empty();
			$( "#productGroups" ).empty();
			$.each( resp.result, function() {
				if ( promoTargetPrdGrp.indexOf( "" + this.id ) != -1 ) {
					$( "#selectedProductGroups" ).append( new Option( this.name, this.id ) );
				}
				else {
					$( "#productGroups" ).append( new Option( this.name, this.id ) );
				}
			});
		});
	}
	
	function resetForm( ele, isEmpty ) {
	    $( ele ).find(':input').each(function() {
	        switch(this.type) {
	            case 'password':
	            case 'select-multiple': isEmpty? $(this).empty() : $(this).val("");
	            case 'select-one':
	            case 'text':
	            case 'textarea':
	            case 'hidden':
	                $(this).val("");
	                break;
	            case 'checkbox':
	            case 'radio':
	                this.checked = false;
	        }
	    });
	}

});

