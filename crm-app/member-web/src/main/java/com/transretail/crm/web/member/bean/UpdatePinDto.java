package com.transretail.crm.web.member.bean;

import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

import org.hibernate.validator.constraints.NotEmpty;

import com.transretail.crm.core.validator.Confirm;

/**
 * @author Allan G. Ramirez (agramirez@exist.com)
 */
@Confirm(field = "pin")
public class UpdatePinDto {
	@NotEmpty
    @Pattern(regexp = "^\\d{6}$")
    private String pin;
	@NotEmpty
    @Pattern(regexp = "^\\d{6}$")
    private String confirmPin;

    public String getPin() {
        return pin;
    }

    public void setPin(String pin) {
        this.pin = pin;
    }

    public String getConfirmPin() {
        return confirmPin;
    }

    public void setConfirmPin(String confirmPin) {
        this.confirmPin = confirmPin;
    }
}
