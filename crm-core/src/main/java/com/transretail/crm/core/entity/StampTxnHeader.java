package com.transretail.crm.core.entity;

import com.google.common.collect.Lists;
import com.mysema.query.annotations.QueryInit;
import com.transretail.crm.core.entity.support.CustomAuditableEntity;
import org.hibernate.envers.NotAudited;

import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * @author Mike de Guzman
 */
@Entity(name = "CRM_STAMP_TXN_HDR")
public class StampTxnHeader extends CustomAuditableEntity<Long> implements Serializable {

    private static final long serialVersionUID = 1L;

    @Column(name = "TXN_NO")
    private String transactionNo;

    @Column(name = "RETURN_TXN_NO")
    private String returnTransactionNo;

    @Column(name = "TXN_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date transactionDateTime;

    @Column(name = "REASON", length = 1000)
    private String approvalReason;

    @Column(name = "STORE_CODE")
    private String storeCode;

    @Column(name = "TOTAL_STAMP")
    private Double totalTxnStamps;

    @OneToMany(mappedBy = "stampTxnHeader", cascade = CascadeType.ALL)
    private List<StampTxnDetail> transactionDetails = Lists.newArrayList();


//    @ManyToOne(fetch = FetchType.LAZY)
//    @JoinColumn(name = "STORE_CODE")
//    private Store store;

    /*
        TODO: Mark this as lazy because listing points might not necessarily
        need membermodel info. All *ToOne are eager by default
    */
    @ManyToOne
    @JoinColumn(name = "MEMBER_ID")
    @NotAudited
    private MemberModel memberModel;


    public String getTransactionNo() {
        return transactionNo;
    }

    public void setTransactionNo(String transactionNo) {
        this.transactionNo = transactionNo;
    }

    public String getReturnTransactionNo() {
        return returnTransactionNo;
    }

    public void setReturnTransactionNo(String returnTransactionNo) {
        this.returnTransactionNo = returnTransactionNo;
    }

    public Date getTransactionDateTime() {
        return transactionDateTime;
    }

    public void setTransactionDateTime(Date transactionDateTime) {
        this.transactionDateTime = transactionDateTime;
    }

    public String getApprovalReason() {
        return approvalReason;
    }

    public void setApprovalReason(String approvalReason) {
        this.approvalReason = approvalReason;
    }

    public String getStoreCode() {
        return storeCode;
    }

    public void setStoreCode(String storeCode) {
        this.storeCode = storeCode;
    }

    public MemberModel getMemberModel() {
        return memberModel;
    }

    public void setMemberModel(MemberModel memberModel) {
        this.memberModel = memberModel;
    }

    public Double getTotalTxnStamps() {
        return totalTxnStamps;
    }

    public void setTotalTxnStamps(Double totalTxnStamps) {
        this.totalTxnStamps = totalTxnStamps;
    }

    public List<StampTxnDetail> getTransactionDetails() {
        return transactionDetails;
    }

    public void setTransactionDetails(List<StampTxnDetail> transactionDetails) {
        this.transactionDetails = transactionDetails;
    }
}
