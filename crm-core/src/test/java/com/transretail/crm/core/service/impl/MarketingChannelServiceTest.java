package com.transretail.crm.core.service.impl;


import java.util.Date;
import java.util.List;

import junit.framework.Assert;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.time.DateUtils;
import org.junit.After;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.TestingAuthenticationToken;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.context.SecurityContextImpl;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.google.common.collect.Lists;
import com.transretail.crm.core.dto.MarketingChannelDto;
import com.transretail.crm.core.entity.MarketingChannel;
import com.transretail.crm.core.entity.MarketingChannel.ChannelType;
import com.transretail.crm.core.entity.MemberGroup;
import com.transretail.crm.core.entity.UserModel;
import com.transretail.crm.core.repo.MarketingChannelRepo;
import com.transretail.crm.core.repo.MemberGroupRepo;
import com.transretail.crm.core.repo.UserRepo;
import com.transretail.crm.core.security.model.impl.CustomSecurityUserDetailsImpl;
import com.transretail.crm.core.service.MarketingChannelService;


@ContextConfiguration(locations = "classpath*:/META-INF/spring/applicationContext*.xml")
@RunWith(SpringJUnit4ClassRunner.class)
public class MarketingChannelServiceTest {

	@Autowired
	MarketingChannelService service;
	@Autowired
	MarketingChannelRepo repo;
	@Autowired
	UserRepo userRepo;
	@Autowired
	MemberGroupRepo memberGroupRepo;

	private Date jobDate = null;
	private Long memberGroupID = null;



    @After
    public void tearDown() {
        SecurityContextHolder.clearContext();
        repo.deleteAll();
    }

	@Test
	public void testSaveGetUpdateDto() {
		setupMemberGroup();
		MarketingChannelDto dto = new MarketingChannelDto();
		dto.setDescription( "SAMPLE DESCRIPTION" );
		dto.setMemberGroup( memberGroupID + "" );
		dto.setType( ChannelType.email );
		Assert.assertEquals( false, StringUtils.isNotBlank( dto.getCode() ) );
		Assert.assertEquals( false, null != service.getDto( null == dto.getId()? null : Long.valueOf( dto.getId() ) ) );

		dto = service.saveDto( dto, true );
		Assert.assertEquals( true, StringUtils.isNotBlank( dto.getCode() ) );
		Assert.assertEquals( true, null != service.getDto( Long.valueOf( dto.getId() ) ) );

		Assert.assertEquals( "SAMPLE DESCRIPTION", dto.getDescription() );
		dto.setDescription( "NEW DESCRIPTION" );
		service.updateDto( dto );
		MarketingChannelDto updated = service.getDto( Long.valueOf( dto.getId() ) );
		Assert.assertEquals( "NEW DESCRIPTION", updated.getDescription() );
	}

	@Test
	public void testGetScheduled() {
		setupMarketingChannels();
		List<MarketingChannel> models = service.getScheduled( jobDate );
		Assert.assertEquals( 1, models.size() );

		jobDate = DateUtils.addHours( jobDate, 1 );
		models = service.getScheduled( jobDate );
		Assert.assertEquals( true, CollectionUtils.isEmpty( models ) );
	}

	@Test
	public void testGetCreateUsers() {
		setupCurrentUser();
		List<String> users = service.getCreateUsers();
		Assert.assertEquals( true, CollectionUtils.isEmpty( users ) );
		setupMarketingChannels();
		users = service.getCreateUsers();
		Assert.assertEquals( true, users.indexOf( userRepo.findByUsername( "loggedInMktUser" ).getUsername() ) != -1 );
	}



	private void setupMarketingChannels() {
		Date date = new Date();
		jobDate = new Date();
		jobDate = DateUtils.setHours( jobDate, 8 );
		jobDate = DateUtils.setMinutes( jobDate, 0 );
		jobDate = DateUtils.setSeconds( jobDate, 0 );

		MarketingChannelDto dto = new MarketingChannelDto();
		dto.setType( ChannelType.email );
		dto.setSchedule( date );
		dto.setDispTime( "08:00:00" );
		service.saveDto( dto, true );
	}

	private void setupCurrentUser() {
		UserModel loggedInMktUser = new UserModel();
    	loggedInMktUser.setUsername( "loggedInMktUser" );
    	loggedInMktUser.setPassword( "user" );
    	loggedInMktUser.setEnabled( true );
    	loggedInMktUser.setStoreCode( "store1" );
    	userRepo.save( loggedInMktUser );
   
        CustomSecurityUserDetailsImpl user = new CustomSecurityUserDetailsImpl();
        user.setUserId( loggedInMktUser.getId() );
        user.setUsername( loggedInMktUser.getUsername() );
        List<GrantedAuthority> authorities = Lists.newArrayList();
        authorities.add(new SimpleGrantedAuthority("ROLE_MARKETING"));
        user.setAuthorities(authorities);

        SecurityContextImpl securityContext = new SecurityContextImpl();
        securityContext.setAuthentication(new TestingAuthenticationToken(user, ""));
        SecurityContextHolder.setContext(securityContext);
    }

	private void setupMemberGroup() {
		MemberGroup memberGroup = new MemberGroup();
		memberGroup.setName( "Sample MemberGroup" );
		memberGroupRepo.save( memberGroup );
		memberGroupID = memberGroup.getId();
	}

}
