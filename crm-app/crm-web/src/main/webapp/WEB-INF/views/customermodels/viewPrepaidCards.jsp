<%@include file="/WEB-INF/views/common/taglibs.jsp" %>

  <div class="modal-dialog">
  	<div class="modal-content">
  		<div class="modal-header">
  			<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
  			<h4 class="modal-title"><spring:message code="label_prepaid_cards"/></h4>
  		</div>
  		<div class="modal-body form-horizontal">
            <div id="cardList">
                <div id="list_cards" class="data-table-01"
                        data-url="<c:url value="/customermodels/prepaidCardList/${member.id}" /> "
                        data-hdr-cardno="<spring:message code="gc.prepaid.transaction.history.cardno"/>"
                        data-hdr-proddesc="<spring:message code="profile_product_desc"/>"
                        data-hdr-prevbal="<spring:message code="gc.prepaid.transaction.history.prevbal"/>"
                        data-hdr-currbal="<spring:message code="gc.prepaid.transaction.history.curramt"/>"
                        data-hdr-datelinked="<spring:message code="gc.prepaid.transaction.history.datelinked"/>">
                </div>
            </div>
  		</div>
  		<div class="modal-footer">
  			<button type="button" id="close" class="btn btn-default" data-dismiss="modal"><spring:message code="label_close"/></button>
  		</div>
  	</div>
  </div>
<link href="<spring:url value="/styles/temp.css" />" rel="stylesheet"/>