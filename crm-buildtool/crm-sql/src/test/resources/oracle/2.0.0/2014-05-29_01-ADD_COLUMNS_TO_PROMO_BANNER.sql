alter table CRM_PROMO_BANNER add INFO varchar2(255 char);
alter table CRM_PROMO_BANNER add NEW_PRICE double precision;
alter table CRM_PROMO_BANNER add OLD_PRICE double precision;
alter table CRM_PROMO_BANNER add PROD_QUANTITY varchar2(255 char);