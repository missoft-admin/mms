package com.transretail.crm.core.dto;

import java.util.List;
import java.util.Set;

import com.transretail.crm.core.entity.MemberGroup;
import com.transretail.crm.core.entity.MemberGroupPoints;
import com.transretail.crm.core.entity.MemberGroupRFS;
import com.transretail.crm.core.entity.Promotion;

public class MemberGroupDto {
	Long id;
	Promotion promotion;
	String name;
	Set<String> members;
	List<MemberGroupFieldDto> memberFields;
	MemberGroupRFS rfs;
	MemberGroupPoints points;
	String cardTypeGroupCode;
	boolean prepaidUser = false;

	private Boolean isUsed;
	
	public MemberGroupDto() {
		
	}
	
	public MemberGroupDto(MemberGroup memberGroup) {
//		BeanUtils.copyProperties(memberGroup, this);
        this.id = memberGroup.getId();
        this.name = memberGroup.getName();
        this.points = memberGroup.getPoints();
        this.cardTypeGroupCode = memberGroup.getCardTypeGroupCode();
        this.rfs = memberGroup.getRfs();
        this.points = memberGroup.getPoints();
        this.prepaidUser = memberGroup.getPrepaidUser();
	}
	
	public Set<String> getMembers() {
		return members;
	}

	public void setMembers(Set<String> members) {
		this.members = members;
	}

	public Promotion getPromotion() {
		return promotion;
	}

	public void setPromotion(Promotion promotion) {
		this.promotion = promotion;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<MemberGroupFieldDto> getMemberFields() {
		return memberFields;
	}

	public void setMemberFields(List<MemberGroupFieldDto> memberFields) {
		this.memberFields = memberFields;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public MemberGroupRFS getRfs() {
		return rfs;
	}

	public void setRfs(MemberGroupRFS rfs) {
		this.rfs = rfs;
	}

	public MemberGroupPoints getPoints() {
		return points;
	}

	public void setPoints(MemberGroupPoints points) {
		this.points = points;
	}

	public String getCardTypeGroupCode() {
		return cardTypeGroupCode;
	}

	public void setCardTypeGroupCode(String cardTypeGroupCode) {
		this.cardTypeGroupCode = cardTypeGroupCode;
	}

	public Boolean getIsUsed() {
		return isUsed;
	}

	public void setIsUsed(Boolean isUsed) {
		this.isUsed = isUsed;
	}

    public boolean isPrepaidUser() {
        return prepaidUser;
    }

    public void setPrepaidUser(boolean prepaidUser) {
        this.prepaidUser = prepaidUser;
    }

}
