package com.transretail.crm.giftcard.repo;

import com.transretail.crm.common.repo.CrmQueryDslPredicateExecutor;
import com.transretail.crm.giftcard.entity.GiftCardPhysicalCountSummary;

/**
 *
 * @author Monte Cillo Co (mco@exist.com)
 */
public interface GiftCardPhysicalCountSummaryRepo extends CrmQueryDslPredicateExecutor<GiftCardPhysicalCountSummary, Long> {

}
