package com.transretail.crm.loyalty.dto;

import org.apache.commons.lang3.StringUtils;

import com.mysema.query.types.expr.BooleanExpression;
import com.transretail.crm.common.service.dto.request.AbstractSearchFormDto;
import com.transretail.crm.loyalty.entity.QLoyaltyCardInventory;

/**
 * @author Allan G. Ramirez (agramirez@exist.com)
 */
public class LCInvSearchDto extends AbstractSearchFormDto {
    private String cardNo;

    public String getCardNo() {
        return cardNo;
    }

    public void setCardNo(String cardNo) {
        this.cardNo = cardNo;
    }

    @Override
    public BooleanExpression createSearchExpression() {
        if (StringUtils.isNotBlank(cardNo)) {
            return QLoyaltyCardInventory.loyaltyCardInventory.barcode.containsIgnoreCase(cardNo);
        }
        return null;
    }
}
