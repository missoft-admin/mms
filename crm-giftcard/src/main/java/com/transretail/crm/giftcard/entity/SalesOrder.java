package com.transretail.crm.giftcard.entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.hibernate.annotations.Type;
import org.joda.time.LocalDate;

import com.transretail.crm.core.entity.lookup.LookupDetail;
import com.transretail.crm.core.entity.support.CustomAuditableEntity;
import com.transretail.crm.giftcard.entity.support.SalesOrderDiscountType;
import com.transretail.crm.giftcard.entity.support.SalesOrderStatus;
import com.transretail.crm.giftcard.entity.support.SalesOrderType;

@Entity
@Table(name = "CRM_SALES_ORD")
public class SalesOrder extends CustomAuditableEntity<Long> implements Serializable {

	private static final long serialVersionUID = 1L;

	public static enum DeliveryType {
		PICK_UP, SHIP_TO
	}

	@Column(name = "ORDER_NO", unique = true)
	private String orderNo;

	@Column(name = "ORDER_DATE")
	@Type(type = "org.jadira.usertype.dateandtime.joda.PersistentLocalDate")
	private LocalDate orderDate;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "CUSTOMER")
	private GiftCardCustomerProfile customer;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "DISCOUNT")
	private GiftCardDiscountScheme discount;

	@Column(name = "CONTACT_PERSON")
	private String contactPerson;
	@Column(name = "CONTACT_NO")
	private String contactNumber;
	@Column(name = "CONTACT_EMAIL")
	private String contactEmail;

	@Column(name = "NOTES")
	private String notes;

	@OneToMany(mappedBy = "order", orphanRemoval = true, fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	private Set<SalesOrderItem> items;

	@OneToMany(mappedBy = "order", orphanRemoval = true, fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	private Set<SalesOrderPaymentInfo> payments;

	@OneToMany(mappedBy = "order", orphanRemoval = true, fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	private Set<SalesOrderDeptAlloc> deptAllocs;

	@OneToMany(mappedBy = "order", orphanRemoval = true, fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	private Set<SalesOrderHistory> statusHistory;

	@Column(name = "STATUS")
	@Enumerated(EnumType.STRING)
	private SalesOrderStatus status;

	@Column(name = "DISCOUNT_VAL") /*in percent*/
	private BigDecimal discountVal;

	@Column(name = "RECEIPT_NO")
	private String receiptNo;

	@Column(name = "ORDER_TYPE")
	@Enumerated(EnumType.STRING)
	private SalesOrderType orderType;

	@Column(name = "RETURN_NO")
	private String returnNo;
	@Column(name = "PAID_HANDLING_FEE")
	@Type(type = "yes_no")
	private Boolean paidHandlingFee;

	@Column(name = "PAID_REPLACEMENT")
	@Type(type = "yes_no")
	private Boolean paidReplacement;

	@Column(name = "HANDLING_FEE")
	private BigDecimal handlingFee;

	@Column(name = "DISC_TYPE")
	@Enumerated(EnumType.STRING)
	private SalesOrderDiscountType discType;

	@Column(name = "PAYMENT_STORE")
	private String paymentStore;

	@ManyToOne
	@JoinColumn(name = "CATEGORY")
	private LookupDetail category;

	@Column(name = "DELIVERY_TYPE")
	private String deliveryType;

	@Column(name = "SHIPPING_INFO")
	private String shippingInfo;

	@Column(name = "SHIPPING_FEE")
	private BigDecimal shippingFee;

	@Column(name = "ACTIVATION_DATE")
	@Type(type = "org.jadira.usertype.dateandtime.joda.PersistentLocalDate")
	private LocalDate activationDate;

	@Column(name = "APPROVAL_DATE")
	@Type(type = "org.jadira.usertype.dateandtime.joda.PersistentLocalDate")
	private LocalDate approvalDate;

	@ManyToOne
	@JoinColumn(name = "\"RETURN\"")
	private ReturnRecord returnRecord;

	@Column(name = "VOUCHER_VAL")
	private BigDecimal voucherVal;

	@ManyToOne
	@JoinColumn(name = "PARENT_SALES_ORD")
	private SalesOrder parent;

	public SalesOrder() {
	}

	public SalesOrder(Long id) {
		setId(id);
	}

	public LocalDate getOrderDate() {
		return orderDate;
	}

	public void setOrderDate(LocalDate orderDate) {
		this.orderDate = orderDate;
	}

	public GiftCardCustomerProfile getCustomer() {
		return customer;
	}

	public void setCustomer(GiftCardCustomerProfile customer) {
		this.customer = customer;
	}

	public GiftCardDiscountScheme getDiscount() {
		return discount;
	}

	public void setDiscount(GiftCardDiscountScheme discount) {
		this.discount = discount;
	}

	public Set<SalesOrderItem> getItems() {
		return items;
	}

	public void setItems(Set<SalesOrderItem> items) {
		this.items = items;
	}

	public String getNotes() {
		return notes;
	}

	public void setNotes(String notes) {
		this.notes = notes;
	}

	public SalesOrderStatus getStatus() {
		return status;
	}

	public void setStatus(SalesOrderStatus status) {
		this.status = status;
	}

	public String getOrderNo() {
		return orderNo;
	}

	public void setOrderNo(String orderNo) {
		this.orderNo = orderNo;
	}

	public String getContactPerson() {
		return contactPerson;
	}

	public void setContactPerson(String contactPerson) {
		this.contactPerson = contactPerson;
	}

	public String getContactNumber() {
		return contactNumber;
	}

	public void setContactNumber(String contactNumber) {
		this.contactNumber = contactNumber;
	}

	public String getContactEmail() {
		return contactEmail;
	}

	public void setContactEmail(String contactEmail) {
		this.contactEmail = contactEmail;
	}

	public SalesOrderType getOrderType() {
		return orderType;
	}

	public void setOrderType(SalesOrderType orderType) {
		this.orderType = orderType;
	}

	public BigDecimal getDiscountVal() {

		if (this.discountVal != null) {
			return discountVal;
		}

		return BigDecimal.ZERO;
	}

	public void setDiscountVal(BigDecimal discountVal) {
		this.discountVal = discountVal;
	}

	public String getReceiptNo() {
		return receiptNo;
	}

	public void setReceiptNo(String receiptNo) {
		this.receiptNo = receiptNo;
	}

	public String getReturnNo() {
		return returnNo;
	}

	public void setReturnNo(String returnNo) {
		this.returnNo = returnNo;
	}

	public Boolean getPaidHandlingFee() {
		return paidHandlingFee;
	}

	public void setPaidHandlingFee(Boolean paidHandlingFee) {
		this.paidHandlingFee = paidHandlingFee;
	}

	public SalesOrderDiscountType getDiscType() {
		return discType;
	}

	public void setDiscType(SalesOrderDiscountType discType) {
		this.discType = discType;
	}

	public String getPaymentStore() {
		return paymentStore;
	}

	public void setPaymentStore(String paymentStore) {
		this.paymentStore = paymentStore;
	}

	public Set<SalesOrderPaymentInfo> getPayments() {
		return payments;
	}

	public void setPayments(Set<SalesOrderPaymentInfo> payments) {
		this.payments = payments;
	}

	public Set<SalesOrderDeptAlloc> getDeptAllocs() {
		return deptAllocs;
	}

	public void setDeptAllocs(Set<SalesOrderDeptAlloc> deptAllocs) {
		this.deptAllocs = deptAllocs;
	}

	public LookupDetail getCategory() {
		return category;
	}

	public void setCategory(LookupDetail category) {
		this.category = category;
	}

	public String getDeliveryType() {
		return deliveryType;
	}

	public void setDeliveryType(String deliveryType) {
		this.deliveryType = deliveryType;
	}

	public String getShippingInfo() {
		return shippingInfo;
	}

	public void setShippingInfo(String shippingInfo) {
		this.shippingInfo = shippingInfo;
	}

	public BigDecimal getShippingFee() {
		return shippingFee;
	}

	public void setShippingFee(BigDecimal shippingFee) {
		this.shippingFee = shippingFee;
	}

	public Boolean getPaidReplacement() {
		return paidReplacement;
	}

	public void setPaidReplacement(Boolean paidReplacement) {
		this.paidReplacement = paidReplacement;
	}

	public BigDecimal getHandlingFee() {
		return handlingFee;
	}

	public void setHandlingFee(BigDecimal handlingFee) {
		this.handlingFee = handlingFee;
	}

	public LocalDate getActivationDate() {
		return activationDate;
	}

	public void setActivationDate(LocalDate activationDate) {
		this.activationDate = activationDate;
	}

	public ReturnRecord getReturnRecord() {
		return returnRecord;
	}

	public void setReturnRecord(ReturnRecord returnRecord) {
		this.returnRecord = returnRecord;
	}

	public BigDecimal getVoucherVal() {
		return voucherVal;
	}

	public void setVoucherVal(BigDecimal voucherVal) {
		this.voucherVal = voucherVal;
	}

	public SalesOrder getParent() {
		return parent;
	}

	public void setParent(SalesOrder parent) {
		this.parent = parent;
	}

	public LocalDate getApprovalDate() {
		return approvalDate;
	}

	public void setApprovalDate(LocalDate approvalDate) {
		this.approvalDate = approvalDate;
	}

	public Set<SalesOrderHistory> getStatusHistory() {
		return statusHistory;
	}

	public void setStatusHistory(Set<SalesOrderHistory> statusHistory) {
		this.statusHistory = statusHistory;
	}

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;

        if (o == null || getClass() != o.getClass()) return false;

        SalesOrder that = (SalesOrder) o;

        return new EqualsBuilder()
                .append(orderNo, that.orderNo)
                .isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 37)
                .append(orderNo)
                .toHashCode();
    }
}
