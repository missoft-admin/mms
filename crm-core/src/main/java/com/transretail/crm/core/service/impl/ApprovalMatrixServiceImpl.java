package com.transretail.crm.core.service.impl;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.google.common.collect.Lists;
import com.mysema.query.jpa.impl.JPAQuery;
import com.mysema.query.types.expr.BooleanExpression;
import com.transretail.crm.common.service.util.SpringDataPagingUtil;
import com.transretail.crm.core.dto.ApprovalMatrixDto;
import com.transretail.crm.core.dto.ApprovalMatrixResultList;
import com.transretail.crm.core.dto.ApprovalMatrixSearchDto;
import com.transretail.crm.core.dto.ServiceDto;
import com.transretail.crm.core.dto.UserResultList;
import com.transretail.crm.core.dto.UserSearchDto;
import com.transretail.crm.core.entity.ApprovalMatrixModel;
import com.transretail.crm.core.entity.QUserModel;
import com.transretail.crm.core.entity.QUserRolePermission;
import com.transretail.crm.core.entity.UserModel;
import com.transretail.crm.core.entity.UserRoleModel;
import com.transretail.crm.core.entity.enums.ModelType;
import com.transretail.crm.core.entity.enums.SvcReturnType;
import com.transretail.crm.core.repo.ApprovalMatrixRepo;
import com.transretail.crm.core.repo.UserRepo;
import com.transretail.crm.core.repo.UserRoleRepo;
import com.transretail.crm.core.security.model.impl.CustomSecurityUserDetailsImpl;
import com.transretail.crm.core.security.service.CustomSecurityUserService;
import com.transretail.crm.core.service.ApprovalMatrixService;
import com.transretail.crm.core.service.CodePropertiesService;
import com.transretail.crm.core.service.UserService;


@Service("approvalMatrixService")
@Transactional
public class ApprovalMatrixServiceImpl implements ApprovalMatrixService {
    @PersistenceContext
    private EntityManager em;
	@Autowired
    private ApprovalMatrixRepo approvalMatrixRepo;
	@Autowired
    private UserRepo userRepo;
    @Autowired
    private UserRoleRepo userRoleRepo;
    @Autowired
    private CodePropertiesService codePropertiesService;

    @Autowired
    UserService userService;
    @Autowired
    CustomSecurityUserService customSecurityUserService;



    @Override
    public ApprovalMatrixDto findDto( Long inId ) {
    	return new ApprovalMatrixDto( approvalMatrixRepo.findOne( inId ) );
    }

	@Override
	public ApprovalMatrixModel save(ApprovalMatrixModel inMatrix) {
		return approvalMatrixRepo.save( inMatrix );
	}

    @Override
    public ServiceDto retrieveAdjustPointsApprovalMatrix() {

    	ServiceDto theSvcResp = new ServiceDto();

    	theSvcResp.setReturnObj( approvalMatrixRepo.findByModelType( ModelType.POINTS ) );
    	theSvcResp.setReturnCode( SvcReturnType.SUCCESS );

    	return theSvcResp;
    }

	@Override
	public Long findHighestApprovalPointsOfCurrentUser() {

		UserModel theUser = getAuthenticatedUser();
		if ( null == theUser ) {
			return null;			
		}
		return approvalMatrixRepo.findHighestPointsAmountByUser( theUser );
	}

    @Override
    public ApprovalMatrixResultList searchApprover( ApprovalMatrixSearchDto theSearchDto ) {
        Pageable pageable = SpringDataPagingUtil.INSTANCE.createPageable(theSearchDto.getPagination());
        BooleanExpression filter = theSearchDto.createSearchExpression();
        Page<ApprovalMatrixModel> page = filter != null ? approvalMatrixRepo.findAll(filter, pageable) : approvalMatrixRepo.findAll(pageable);
        return new ApprovalMatrixResultList(convertToDtoList(page.getContent()), page.getTotalElements(), page.hasPreviousPage(), page.hasNextPage());
    }
    
    private List<ApprovalMatrixDto> convertToDtoList(List<ApprovalMatrixModel> models) {
    	List<ApprovalMatrixDto> dtos = Lists.newArrayList();
    	for(ApprovalMatrixModel model : models) {
    		dtos.add(new ApprovalMatrixDto(model));
    	}
    	return dtos;
    }

	@Override
	public void saveDto( ApprovalMatrixDto inMatrix ) {
		ApprovalMatrixModel theModel = ( null != inMatrix.getId() )? 
				inMatrix.toModel( approvalMatrixRepo.findOne( inMatrix.getId() ) ) : inMatrix.toModel();
		approvalMatrixRepo.save( theModel );
	}

	@Override
	public void delete( Long inId ) {
		approvalMatrixRepo.delete( inId );
	}

    @Override
    public List<UserModel> findPointsOverrideApprovers( ) {
        /*
    	UserSearchDto theSearchDto = new UserSearchDto();
    	Set<UserRoleModel> theRoles = new HashSet<UserRoleModel>();
    	theRoles.addAll( userRoleRepo.findByCode( codePropertiesService.getCssCode() ) );
    	theRoles.addAll( userRoleRepo.findByCode( codePropertiesService.getHrsCode() ) );
    	theSearchDto.setRoles( theRoles );
    	theSearchDto.setNotUsers( approvalMatrixRepo.findUsers( ModelType.POINTS ) );
    	UserResultList theResult = userService.searchUser( theSearchDto );
    	if ( null != theResult && null != theResult.getResults() ) {
    		return new ArrayList<UserModel>( theResult.getResults() );
    	}
    	return null;
    	*/
        QUserModel qUserModel = QUserModel.userModel;
        QUserRolePermission qUserRolePermission = QUserRolePermission.userRolePermission;

        BooleanExpression predicate =
            qUserRolePermission.roleId.id.in(codePropertiesService.getCssCode(), codePropertiesService.getHrsCode());
        List<UserModel> pointsUsers = approvalMatrixRepo.findUsers(ModelType.POINTS);
        if (pointsUsers != null && pointsUsers.size() > 0) {
            predicate.and(qUserModel.notIn(pointsUsers));
        }
        return new JPAQuery(em).from(qUserModel)
            .innerJoin(qUserModel.rolePermissions, qUserRolePermission)
            .where(predicate).distinct().list(qUserModel);
    }



    private UserModel getAuthenticatedUser() {
    	
    	CustomSecurityUserDetailsImpl thePrincipal = 
    			(CustomSecurityUserDetailsImpl) customSecurityUserService.getCurrentAuth().getPrincipal();
    	return userRepo.findOne( thePrincipal.getUserId() );
    }
}
