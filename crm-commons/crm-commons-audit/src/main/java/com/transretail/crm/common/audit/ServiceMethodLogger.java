package com.transretail.crm.common.audit;

import java.lang.annotation.Annotation;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.aopalliance.intercept.MethodInterceptor;
import org.aopalliance.intercept.MethodInvocation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.aop.AfterReturningAdvice;
import org.springframework.aop.ThrowsAdvice;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import com.transretail.crm.common.audit.annotation.AuditModule;
import com.transretail.crm.common.audit.annotation.AuditParam;
import com.transretail.crm.common.audit.annotation.Audited;
import com.transretail.crm.common.audit.handler.DefaultSaveHandler;
import com.transretail.crm.common.audit.handler.SaveHandler;
import com.transretail.crm.common.audit.helper.ProxyHelper;

/**
 * @author Allan G. Ramirez (agramirez@exist.com)
 */
@Component
public class ServiceMethodLogger implements MethodInterceptor, AfterReturningAdvice, ThrowsAdvice {
    private static final Logger _LOG = LoggerFactory.getLogger(ServiceMethodLogger.class);
    private static final Pattern NAMED_PARAM_PATTERN = Pattern.compile("(?:^|\\s)(\\$\\w+)");
    private static final ThreadLocal<Integer> DEPTH = new ThreadLocal<Integer>() {
        @Override
        protected Integer initialValue() {
            return 0;
        }
    };

    private SaveHandler saveHandler = new DefaultSaveHandler();

    private ProxyHelper proxyHelper = new ProxyHelper();

    @Autowired
    public void setSaveHandler(SaveHandler saveHandler) {
        this.saveHandler = saveHandler;
    }

    @Override
    public Object invoke(MethodInvocation invocation) throws Throwable {
        DEPTH.set(DEPTH.get() + 1);
        return invocation.proceed();
    }

    @Override
    public void afterReturning(Object returnValue, Method method, Object[] args, Object target) throws Throwable {
        DEPTH.set(DEPTH.get() - 1);
        if (DEPTH.get() == 0) {
            DEPTH.remove();

            Object targetInstance = proxyHelper.unwrapIfProxy(target);
            Method instanceMethod = targetInstance.getClass().getDeclaredMethod(method.getName(), method.getParameterTypes());
            if (instanceMethod.isAnnotationPresent(Audited.class)) {
                saveHandler.saveMessage(AuditStatus.SUCCESS, getModuleName(targetInstance.getClass()),
                    getAuditMessage(instanceMethod, args));
            }
        }
    }

    public void afterThrowing(Method method, Object[] args, Object target, Exception ex) {
        DEPTH.set(DEPTH.get() - 1);
        if (DEPTH.get() == 0) {
            DEPTH.remove();
            try {
                Object targetInstance = proxyHelper.unwrapIfProxy(target);
                Method instanceMethod = targetInstance.getClass().getDeclaredMethod(method.getName(), method.getParameterTypes());
                if (instanceMethod.isAnnotationPresent(Audited.class)) {
                    saveHandler.saveMessage(AuditStatus.ERROR, getModuleName(targetInstance.getClass()),
                        getAuditMessage(instanceMethod, args));
                }
            } catch (Exception e) {
                _LOG.error("Error in invoking afterThrowing method", e);
                throw new RuntimeException(e);
            }
        }
    }

    private AuditMessage getAuditMessage(Method instanceMethod, Object[] args) {
        try {
            Audited auditMessage = instanceMethod.getAnnotation(Audited.class);
            String name = StringUtils.hasText(auditMessage.name()) ? auditMessage.name() : instanceMethod.getName();
            String rawDescription = auditMessage.description();

            Map<String, String> auditParamMap = new HashMap<String, String>();
            Matcher matcher = NAMED_PARAM_PATTERN.matcher(rawDescription);
            while (matcher.find()) {
                auditParamMap.put(matcher.group().trim(), null);
            }

            /*
            Method argument names cannot be determined unless the class is compiled with -g or debug option.
            Therefore, the named parameter in AuditMessage#description must be in order with the method arguments.
            Eg. Given This is a $custom and $sample description, the method arguments must be
            method(@AuditParam("custom") Object param1, ..., @AuditParam("sample") Object param2).
            @AuditParam("sample") cannot be interchanged with @AuditParam("custom")
             */
            Annotation[][] paramAnnotations = instanceMethod.getParameterAnnotations();
            instanceMethod.setAccessible(true);
            for (int i = 0; i < paramAnnotations.length; i++) {
                for (Annotation annotation : paramAnnotations[i]) {
                    if (annotation instanceof AuditParam) {
                        String paramNameKey = "$" + ((AuditParam) annotation).value().trim();
                        if (!auditParamMap.containsKey(paramNameKey)) {
                            throw new IllegalArgumentException(
                                String.format("No %s found in AuditMessage description", paramNameKey));
                        }
                        auditParamMap.put(paramNameKey, args[i].toString());
                        break;
                    }
                }
            }

            // cloudx: Feel free to modify this if there's a better way to improve this
            String description = rawDescription;
            for (Map.Entry<String, String> entry : auditParamMap.entrySet()) {
                // Escape the $
                description = rawDescription.replace(entry.getKey(), entry.getValue());
            }

            return new AuditMessage(name, description, rawDescription, auditParamMap);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    private String getModuleName(Class<?> clazz) {
        String moduleName = null;
        if (clazz.isAnnotationPresent(AuditModule.class)) {
            moduleName = clazz.getAnnotation(AuditModule.class).moduleName();
        }
        if (!StringUtils.hasText(moduleName)) {
            moduleName = clazz.getName();
        }
        return moduleName;
    }

}
