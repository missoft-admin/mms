<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ taglib prefix="permission" tagdir="/WEB-INF/tags/permission" %>

<!-- Get the user local from the page context (it was set by Spring MVC's locale resolver) -->
<c:set var="userLocale">
  <c:set var="plocale">${pageContext.response.locale}</c:set>
  <c:out value="${fn:replace(plocale, '_', '-')}" default="in"/>
</c:set>
<!DOCTYPE HTML>
<%--<!--[if lt IE 7]>
<html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>
<html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>
<html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!-->
<html class="no-js"> <!--<![endif]-->--%>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
  <spring:message code="application_name" var="app_name" htmlEscape="false"/>
  <title><spring:message code="welcome_h3" arguments="${app_name}"/></title>
  <meta name="description" content="Member Portal">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <!-- for non html5 compliant browsers -->
  <meta http-equiv="X-UA-Compatible" content="IE=9" />
  <script src="<spring:url value="/js/modernizr-2.6.2.min.js" />"></script>

  <link href="<c:url value="/images/favicon.ico" />" rel="SHORTCUT ICON"/>
  <link href="<c:url value="/css/jqueryui/no-theme/jquery-ui-1.10.3.custom.min.css" />" rel="stylesheet"/>
  <%--<link href="<c:url value="/css/mega-menu3d.css"/>" rel="stylesheet" media="screen, projection">--%>
  <%--<link href="<c:url value="/css/mega-skin.css"/>" rel="stylesheet" type="text/css">--%>
  <link href="<c:url value="/css/bootstrap/bootstrap.css" />" rel="stylesheet">
  <style type="text/css">
      body {
      padding-top: 0px;
      padding-bottom: 40px;
      }
  </style>
  <link href="<c:url value="/css/bootstrap/bootstrap-responsive.css" />" rel="stylesheet">
  <link href="<c:url value="/css/bootstrap/bootstrap.override.css" />" rel="stylesheet">
  <link href="<c:url value="/css/custom.css" />" rel="stylesheet">
  <style>
  @font-face {
    font-family: 'Lato';
    font-style: normal;
    font-weight: 300;
    src: local('Lato Light'), local('Lato-Light'), url(/css/fonts/kcf5uOXucLcbFOydGU24WALUuEpTyoUstqEm5AMlJo4.woff) format('woff');
  }
  @font-face {
    font-family: 'Lato';
    font-style: normal;
    font-weight: 400;
    src: local('Lato Regular'), local('Lato-Regular'), url(/css/fonts/qIIYRU-oROkIk8vfvxw6QvesZW2xOQ-xsNqO47m55DA.woff) format('woff');
  }
  @font-face {
    font-family: 'Lato';
    font-style: normal;
    font-weight: 700;
    src: local('Lato Bold'), local('Lato-Bold'), url(/css/fonts/qdgUG4U09HnJwhYI-uK18wLUuEpTyoUstqEm5AMlJo4.woff) format('woff');
  }
  @font-face {
    font-family: 'Lato';
    font-style: normal;
    font-weight: 900;
    src: local('Lato Black'), local('Lato-Black'), url(/css/fonts/G2uphNnNqGFMHLRsO_72ngLUuEpTyoUstqEm5AMlJo4.woff) format('woff');
  }
  
  .username-topbar-info, .username-topbar {
    color: #777777;
    text-shadow: 0 1px 0 #FFFFFF;
  }
  .username-topbar-info {
    font-size: 12px;
    line-height: 16px;
    padding: 3px 15px 10px 0;
  }
  .username-topbar {

    border-left: 1px solid #CCCCCC;
    margin-left: 20px;
    padding: 10px 20px;
    text-transform: uppercase;
    
  }
  .language {
    margin-left: 22px;
    margin-top: 3px;
  }
  
  ul.nav li.dropdown.language {
      background-image: linear-gradient(to bottom, #ffffff, #f1f1f1);
  }
  
  ul.nav li.dropdown a.block-language {
      margin-bottom: 0;
      margin-top: 0;
      padding-bottom: 5px;
      padding-top: 3px;
  } 
  
  .navbar .nav li.dropdown.open > .dropdown-toggle,
  .navbar .nav li.dropdown.active > .dropdown-toggle,
  .navbar .nav li.dropdown.open.active > .dropdown-toggle {
    background-color: transparent !important;
  }
  .navbar .nav.pull-right li.link-icon:nth-child(3) {
      margin-right: 10px;
  }
  .navbar .nav.pull-right li.link-icon:nth-child(2) {
      margin-left: 20px;
  }
  
  .dropdown-menu > li > a:hover,
  .dropdown-menu > li > a:focus,
  .dropdown-submenu:hover > a,
  .dropdown-submenu:focus > a {
    color: #16aeec;
    text-decoration: none;
    background-color: transparent !important;
    background-image: none !important;
  }
  .navbar .nav .dropdown-toggle.icon-menu-settings .caret, .navbar .nav .dropdown-toggle.icon-menu-notifications .caret {
    margin-right: 13px;
    margin-top: -5px;
    position: absolute;
    right: -16px;
  }
  </style>

  <script src="<c:url value="/js/jquery/jquery-1.10.2.min.js" />"></script>
  <script src="<c:url value="/js/jquery/jqueryui/jquery-ui-1.10.3.custom.min.js" />"></script>

  <script src="<c:url value="/js/jquery/jquery-migrate-1.2.1.js" />"></script>
  <script src="<c:url value="/js/jquery/jquery.formUtil.js" />"></script>
  <script src="<c:url value="/js/common/formcheck.js" />"></script>
  
  <script src="<c:url value="/js/comet/cometd-namespace.js" />"></script>
  <script src="<c:url value="/js/comet/cometd-json.js" />"></script>
  <script src="<c:url value="/js/comet/TransportRegistry.js" />"></script>
  <script src="<c:url value="/js/comet/Transport.js" />"></script>
  <script src="<c:url value="/js/comet/CallbackPollingTransport.js" />"></script>
  <script src="<c:url value="/js/comet/LongPollingTransport.js" />"></script>
  <script src="<c:url value="/js/comet/WebSocketTransport.js" />"></script>
  <script src="<c:url value="/js/comet/RequestTransport.js" />"></script>
  <script src="<c:url value="/js/comet/Utils.js" />"></script>
  <script src="<c:url value="/js/comet/Cometd.js" />"></script>
  <script src="<c:url value="/js/comet/jquery.cometd.js" />"></script>
  <script src="<c:url value="/js/comet/ReloadExtension.js" />"></script>
  <script src="<c:url value="/js/comet/jquery.cometd-reload.js" />"></script>
  
  <script src="<c:url value="/js/notification.js" />"></script>
  <script src="<spring:url value="/js/dateutil.js" />"></script>
  <script src="<spring:url value="/js/common/commonutil.js" />"></script>
  <script src="<c:url value="/js/bootstrap-select.js" />"></script>
  <script>
    var ctx = "${pageContext.request.contextPath}";
    var DEFAULT_DATE_FORMAT="dd-mm-yyyy";
  </script>
  

  
  
  <%--<!--[if lte IE 9]>
    <script src="//cdnjs.cloudflare.com/ajax/libs/html5shiv/3.6.2/html5shiv.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/respond.js/1.3.0/respond.js"></script>
  <![endif]-->--%>
  
</head>

<body>
  
  



<div>
	<div class="navbar navbar-static-top">
	  <div class="navbar-inner">
	    <div class="container">

	      <div class="navbar-brand pull-left">
          <a class="" href="<spring:url value="/" />">
            <img width="" src="<c:url value="/images/Logo-1.png" />">
          </a>
        </div>
        <ul class="nav pull-left">
          <li class="username-topbar">
              <sec:authentication property="principal.username"/>
          </li>
          <li class="username-topbar-info">
              <permission:info userStore="true" /></br>

              <permission:info inventoryLoc="true" />
              
          </li>
        </ul>
	      <div class="">
          

	        <ul class="nav pull-right">
	          <li class="link-icon"><a href="<spring:url value="/" />" class="icon-menu-home">Home</a></li>
	          <tiles:insertAttribute name="notifications"/>
	          <%--<li><a href="#" title="<permission:info userStore="true" inventoryLoc="true" />"><sec:authentication property="principal.username"/></a></li>--%>
            <li class="dropdown link-icon">
              <a href="#" class="dropdown-toggle icon-menu-settings" data-hover="dropdown" data-delay="200" data-close-others="true">
                Settings <b class="caret"></b>
              </a>
              <ul class="dropdown-menu">
                <li><a href="<spring:url value="/user/profile/password/change"/>"><spring:message code="menu_user_changepassword"/></a></li>
                <li class="divider"></li>
                <li><a href="<spring:url value="/logout"/>">Logout</a></li>
              </ul>
            </li>
	          <%--<li><a href="<spring:url value="/" />" class="icon-menu-home tiptip" title="Home">Home</a></li>--%>
            <tiles:insertAttribute name="selectLanguage"/>
	        </ul>
	      </div>
	    </div>
	  </div>
	</div>
</div>
<%-- <div style="display: none;">
	<header role="banner" class="">
	  <div id="menuMega" class="menu3dmega navbar navbar-fixed-top bs-docs-nav skin-black">
	    <div class="menuToggle">Menu <span class="megaMenuToggle-icon"></span></div>
	    <div class="menuToggle">Menus <span class="megaMenuToggle-icon"></span></div>

      <ul class="container">
        <tiles:insertAttribute name="menu"/>
        <tiles:insertAttribute name="selectLanguage"/>
        <li class="nav pull-right"><a href="<spring:url value="/logout"/>">Logout</a></li>
        <li class="nav pull-right"><a href="<spring:url value="/" />">Home</a></li>
        <li class="right"><a href="#"><sec:authentication property="principal.username"/></a></li>
      </ul>
	  </div>
	</header>
</div> --%>



<div class="container">
  <div class="content_menu"><tiles:insertAttribute name="menu"/></div>

  <%@ include file="../views/common/messages.jsp" %>
  <!--<div id="notificationAlerts" class="alert">
	<button type="button" class="close" onclick="hideNotifAlerts();">&times;</button>
	<ul id="notificationAlertsContent" ></ul>
  </div>	-->
  <div id="mainBody">
  	<tiles:insertAttribute name="body"/>
  </div>
</div>
<!-- Bootstrap core JavaScript
================================================== -->
<!-- Placed at the end of the document so the pages load faster -->
<script src="<c:url value="/js/bootstrap/bootstrap.js" />"></script>
<script type="text/javascript">
$(document).ready(function(e){
 $('.tiptip').tooltip();    
});

</script>
<script src="<c:url value="/js/jquery/jquery.cookie.js" />" type="text/javascript"></script>
<%--<script src="<c:url value="/js/jquery/jquery.ddslick.custom.js" />" type="text/javascript"></script>
<style id="css-ddslick" type="text/css">.dd-select{ border-radius:4px; border:solid 1px #ccc; position:relative; cursor:pointer; background:none !important; }.dd-desc { color:#aaa; display:block; overflow: hidden; font-weight:normal; line-height: 1.4em; }.dd-selected{ overflow:hidden; display:block; font-weight:normal !important;}.dd-selected label{font-weight:normal !important;}.dd-pointer{ width:0; height:0; position:absolute; right:10px; top:50%; margin-top:-3px;}.dd-pointer-down{ border:solid 5px transparent; border-top:solid 5px #000; }.dd-option-color{ height:20px; width:50px;display:block;float:right}.dd-selected-color{ height:20px; width:50px;display:block;float:right; margin-right:20px}.dd-pointer-up{border:solid 5px transparent !important; border-bottom:solid 5px #000 !important; margin-top:-8px;}.dd-options{ border:solid 1px #ccc; border-top:none; list-style:none; box-shadow:0px 1px 5px #ddd; display:none; position:absolute; z-index:2000; margin:0; padding:0;background:#fff; overflow:auto;}.dd-option{ padding:10px 32px 10px 12px; display:block; border-bottom:solid 1px #ddd; overflow:hidden; text-decoration:none; color:#333; cursor:pointer;-webkit-transition: all 0.25s ease-in-out; -moz-transition: all 0.25s ease-in-out;-o-transition: all 0.25s ease-in-out;-ms-transition: all 0.25s ease-in-out; }.dd-options > li:last-child > .dd-option{ border-bottom:none;}.dd-option:hover{ background:#f3f3f3; color:#000;}.dd-selected-description-truncated { text-overflow: ellipsis; white-space:nowrap; }.dd-option-selected { background:#f6f6f6; }.dd-option-image, .dd-selected-image { vertical-align:middle; float:left; margin-right:5px; max-width:64px;}.dd-image-right { float:right; margin-right:15px; margin-left:5px;}.dd-container{ position:relative;}​ .dd-selected-text { font-weight:bold}​</style>--%>
<%--<script src="<c:url value="/js/mega/mega-demo.js" />" type="text/javascript"></script>--%>
  
<script>
	$(document).ready($.notification.init());
  var $internalLinks = $( '#internal-links-nav' );
  if ( $internalLinks.length > 0 ) {
    var navbarInner2Div = document.createElement( 'div' );
    navbarInner2Div.className = 'navbar-inner navbar-inner2';

    var $navbarInner2 = $( navbarInner2Div ).append( '<div class="container"><div class=""></div></div>' ).appendTo( $( '.navbar' ) );
    if ( !$internalLinks.hasClass( 'nav' ) ) {
      $internalLinks.addClass( 'nav' );
    }
    $( 'div div', $navbarInner2 ).append( $internalLinks );
    $internalLinks.show();
    $('body').css('padding-top', '7em');
  }

  $("#notificationAlerts").hide();

  function hideNotifAlerts() {
	$("#notificationAlerts").hide();
  }
</script>




</body>
</html>
