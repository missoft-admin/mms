$( document ).ready( function () {
    var htmlString = '';
    htmlString += '<div class="modal hide" style="width: 500px; max-height: 600px;">';
    htmlString += '  <div class="modal-dialog">';
    htmlString += '    <div class="modal-content">';
    htmlString += '      <div class="modal-header"><span>&nbsp;</span>';
    htmlString += '        <button type="button" class="close btn-close" aria-hidden="true">&times;</button>';
    htmlString += '      </div>';
    htmlString += '      <div class="modal-body" style="padding: 0">';
    htmlString += '      <select multiple style="width: 100%; margin: 0"></select>';
    htmlString += '      </div>';
    htmlString += '      <div class="modal-footer container-btn">';
    htmlString += '        <input type="button" class="btn btn-primary btn-ok" value="Ok" />';
    htmlString += '        <input type="button" class="btn btn-close" value="Cancel" />';
    htmlString += '      </div>';
    htmlString += '    </div>';
    htmlString += '  </div>';
    htmlString += '</div>';

    var $customFieldDialog = $( htmlString );
    $customFieldDialog.modal( {
        show : false,
        backdrop : "static"
    } ).on( 'show.bs.modal', function () {
        $( this ).css( 'position', 'absolute' );
        $( this ).css(
                {
                    'margin-top' : function () {
                        return window.pageYOffset - ($( this ).height() / 2 );
                    }
                } );
    } );

    var $customFieldSelect = null;
    var $customFieldInput = null;

    var $customFieldDetails = $( '.modal-body select', $customFieldDialog );
    $( '.memberConfigurableFieldsContainer button.addCustomField' ).click( function () {
        var $fieldGroup = $( this ).parents( '.field-group' );
        $customFieldSelect = $( 'select[class*="customField"]', $fieldGroup );
        $customFieldInput = $( 'input[class*="customField"]', $fieldGroup );

        if ( !$customFieldDialog.hasClass( 'in' ) ) {
            $customFieldDialog.modal( 'show' );
        }

        var headerCode = $( this ).siblings( '.headerCode' ).val();
        var headerDesc = $( this ).siblings( '.headerDesc' ).val();

        $( '.modal-header > span', $customFieldDialog ).text( headerCode + " - " + headerDesc );
        $.get( ctx + "/reference/list/byheadercode/" + headerCode, function ( details ) {
            $customFieldDetails.empty();
            for ( var i = 0; i < details.length; i++ ) {
                $customFieldDetails.append( '<option value="' + details[i].code + '">' + details[i].desc + '</option>' );
            }
        }, "json" );
    } );
    var updateInputValue = function ( $input, $select ) {
        var $options = $( 'option', $select );
        if ( $options.length > 0 ) {
            var values = $options[0].value;
            for ( var i = 1; i < $options.length; i++ ) {
                values += ',' + $options[i].value;
            }
            $input.val( values );
        } else {
            $input.val( '' );
        }
    }

    $( '.memberConfigurableFieldsContainer button.removeCustomField' ).click( function () {
        var $fieldGroup = $( this ).parents( '.field-group' );
        $customFieldSelect = $( 'select[class*="customField"]', $fieldGroup );
        $customFieldInput = $( 'input[class*="customField"]', $fieldGroup );

        $( 'option:selected', $customFieldSelect ).remove();
        updateInputValue( $customFieldInput, $customFieldSelect );
    } );
    $( '.btn-close', $customFieldDialog ).click( function () {
        $customFieldDialog.modal( 'hide' );
    } );
    $( '.btn-ok', $customFieldDialog ).click( function () {
        $customFieldDialog.modal( 'hide' );
        var $selectedFields = $( 'option:selected', $customFieldDetails );

        if ( $selectedFields.length > 0 ) {
            for ( var i = 0; i < $selectedFields.length; i++ ) {
                var code = $selectedFields[i].value;
                var desc = $selectedFields[i].innerHTML;
                if ( $( 'option[value="' + code + '"]', $customFieldSelect ).length == 0 ) {
                    $customFieldSelect.append( '<option value="' + code + '">' + desc + '</option>' );
                }
            }
        }
        updateInputValue( $customFieldInput, $customFieldSelect );
    } );
} );

function updateConfigurableFieldsDisplay( $memberConfigurableFieldsContainer, configurableFields ) {
    $( 'input[class*="customField"]', $memberConfigurableFieldsContainer ).val( '' );
    $( 'select[class*="customField"]', $memberConfigurableFieldsContainer ).empty();
    if ( configurableFields != null ) {
        $.each( configurableFields, function ( key, value ) {
            $( 'input.' + key, $memberConfigurableFieldsContainer ).val( value );
            if ( value != null && value.trim() != '' ) {
                $.get( ctx + "/lookup/detail/" + value, function ( details ) {
                    var selectOptionHtmlString = '';
                    for ( var i = 0; i < details.length; i++ ) {
                        selectOptionHtmlString += '<option value="' + details[i].code + '">' + details[i].description + '</option>';
                    }
                    $( 'select.' + key, $memberConfigurableFieldsContainer ).html( selectOptionHtmlString );
                }, "json" );
            }
        } );
    }
}