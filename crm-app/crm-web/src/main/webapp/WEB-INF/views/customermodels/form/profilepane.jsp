<%@ include file="../../common/taglibs.jsp" %>


<div class="clearfix"></div>

<div class="pull-left span11">
  <jsp:include page="segments/basicInfo.jsp" />
  <jsp:include page="segments/address.jsp" />
</div>

<div class="clearfix"></div>

<div class="pull-left span11">
  <jsp:include page="segments/contactDetails.jsp" />
  <jsp:include page="segments/memberDetails.jsp" />
</div>

<div class="clearfix"></div>

<div class="pull-left span11">
  <jsp:include page="segments/otherData.jsp" />
  <jsp:include page="segments/companyProfile.jsp" />
</div>

<div class="clearfix"></div>
