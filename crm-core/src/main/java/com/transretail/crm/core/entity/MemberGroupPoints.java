package com.transretail.crm.core.entity;


import javax.persistence.Column;
import javax.persistence.Embeddable;

import org.hibernate.annotations.Type;


@Embeddable
public class MemberGroupPoints {
	
	@Column(name = "POINTS_ENABLED")
    @Type(type = "yes_no")
	private Boolean enabled;

	@Column(name = "POINTS_FROM")
	private Long pointsFrom;
	
	@Column(name = "POINTS_TO")
	private Long pointsTo;

	public Boolean getEnabled() {
		return enabled;
	}

	public void setEnabled(Boolean enabled) {
		this.enabled = enabled;
	}

	public Long getPointsFrom() {
		return pointsFrom;
	}

	public void setPointsFrom(Long pointsFrom) {
		this.pointsFrom = pointsFrom;
	}

	public Long getPointsTo() {
		return pointsTo;
	}

	public void setPointsTo(Long pointsTo) {
		this.pointsTo = pointsTo;
	}
	
	
    
}



