package com.transretail.crm.core.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.envers.Audited;

import com.transretail.crm.core.entity.lookup.LookupDetail;

/**
 * @author Allan G. Ramirez (agramirez@exist.com)
 */
@Audited
@Entity
@Table(name = "CRM_PROMOTION_PAYMENTTYPE")
public class PromotionPaymentType extends PromotionTargetGroup<String> implements Serializable {
    private static final long serialVersionUID = 2949144849536853114L;
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "PAYMENTTYPE_ID")
    private LookupDetail lookupDetail;
    @Column(name = "WILDCARDS", length = 4000)
    private String wildcards;

    public LookupDetail getLookupDetail() {
        return lookupDetail;
    }

    public void setLookupDetail(LookupDetail lookupDetail) {
        this.lookupDetail = lookupDetail;
    }

	public String getWildcards() {
		return wildcards;
	}

	public void setWildcards(String wildcards) {
		this.wildcards = wildcards;
	}

	@Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        PromotionPaymentType that = (PromotionPaymentType) o;

        if (!lookupDetail.equals(that.lookupDetail)) return false;
        if (!getPromotion().equals(that.getPromotion())) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = getPromotion().hashCode();
        result = 31 * result + lookupDetail.hashCode();
        return result;
    }
}
