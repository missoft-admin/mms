package com.transretail.crm.giftcard.service.impl;

import java.util.List;
import java.util.Locale;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.apache.commons.lang3.StringUtils;
import org.joda.time.DateTimeFieldType;
import org.joda.time.LocalDate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.google.common.collect.Lists;
import com.mysema.commons.lang.CloseableIterator;
import com.mysema.query.Tuple;
import com.mysema.query.jpa.impl.JPAQuery;
import com.transretail.crm.core.service.CodePropertiesService;
import com.transretail.crm.giftcard.dto.GcInventorySafetyStockDto;
import com.transretail.crm.giftcard.dto.SafetyStockDto;
import com.transretail.crm.giftcard.entity.ProductProfile;
import com.transretail.crm.giftcard.entity.QGiftCardInventory;
import com.transretail.crm.giftcard.entity.QProductProfile;
import com.transretail.crm.giftcard.entity.support.GiftCardInventoryStatus;
import com.transretail.crm.giftcard.entity.support.ProductProfileStatus;
import com.transretail.crm.giftcard.repo.ProductProfileRepo;
import com.transretail.crm.giftcard.service.SafetyStockService;

/**
 * @author Allan G. Ramirez (agramirez@exist.com)
 */
@Service("safetyStockService")
public class SafetyStockServiceImpl implements SafetyStockService {
    @PersistenceContext
    private EntityManager em;

    @Autowired
    private ProductProfileRepo productProfileRepo;
    @Autowired
    private CodePropertiesService codePropertiesService;
    @Autowired
    private MessageSource messageSource;

    @Transactional(readOnly = true)
    @Override
    public SafetyStockDto getSafetyStock(Long productProfileId) {
        ProductProfile stock = productProfileRepo.findOne(productProfileId);
        return stock != null ? new SafetyStockDto(stock) : null;
    }

    @Transactional
    @Override
    public void update(SafetyStockDto dto) {
        ProductProfile stock = productProfileRepo.findOne(dto.getProductProfileId());
        stock.setSafetyStocksEaMo(dto.getQuantityPerMonth());
        productProfileRepo.save(stock);
    }

    @Transactional(readOnly = true)
    public List<GcInventorySafetyStockDto> checkSafetyStock(LocalDate date) {
        List<GcInventorySafetyStockDto> result = Lists.newArrayList();

        QProductProfile productProfile = QProductProfile.productProfile;
        CloseableIterator<Tuple> it = new JPAQuery(em).from(productProfile)
            .where(productProfile.status.eq(ProductProfileStatus.APPROVED))
            .iterate(productProfile.id, productProfile.safetyStocksEaMo, productProfile.productCode, productProfile.productDesc);
        try {
            if (it.hasNext()) {
                Locale currLocale = LocaleContextHolder.getLocale();
                String currMonth = messageSource
                    .getMessage(date.property(DateTimeFieldType.monthOfYear()).getAsText(Locale.ENGLISH).toLowerCase(), null,
                        currLocale);
                String nextMonth = messageSource
                    .getMessage(date.plusMonths(1).property(DateTimeFieldType.monthOfYear()).getAsText(Locale.ENGLISH).toLowerCase(),
                        null,
                        currLocale);

                int monthIdx = date.getMonthOfYear();
                if (monthIdx == 12) {
                    monthIdx = 0;
                }

                QGiftCardInventory qInventory = QGiftCardInventory.giftCardInventory;
                while (it.hasNext()) {
                    Tuple tuple = it.next();
                    Long count = new JPAQuery(em).from(qInventory)
                        .where(
                            qInventory.profile.id.eq(tuple.get(productProfile.id))
                                .and(qInventory.location.eq(codePropertiesService.getDetailInvLocationHeadOffice()))
                                .and(qInventory.allocateTo.isNull())
                                .and(qInventory.status.eq(GiftCardInventoryStatus.IN_STOCK))
                        ).count();

                    String qtyPerMonth = tuple.get(productProfile.safetyStocksEaMo);
                    if (StringUtils.isNotBlank(qtyPerMonth)) {
                        String[] arr = qtyPerMonth.split(",");
                        int safetyStockQty =
                            monthIdx < arr.length && StringUtils.isNotBlank(arr[monthIdx]) ? Integer.valueOf(arr[monthIdx]) : 0;
                        if (safetyStockQty > 0 && count <= safetyStockQty) {
                            GcInventorySafetyStockDto dto = new GcInventorySafetyStockDto();
                            dto.setInventoryCount(count);
                            dto.setSafetyStockQty(safetyStockQty);
                            dto.setProductCode(tuple.get(productProfile.productCode));
                            dto.setProductDesc(tuple.get(productProfile.productDesc));
                            dto.setCurrMonth(currMonth);
                            dto.setNextMonth(nextMonth);
                            result.add(dto);
                        }
                    }
                }
            }
        } finally {
            it.close();
        }
        return result;
    }

}