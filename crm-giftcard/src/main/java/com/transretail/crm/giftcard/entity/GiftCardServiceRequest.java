package com.transretail.crm.giftcard.entity;

import com.transretail.crm.core.entity.lookup.Store;
import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import org.hibernate.annotations.Type;
import org.joda.time.DateTime;

/**
 *
 * @author Monte Cillo Co (mco@exist.com)
 * @since 2.0
 */
@Entity
@Table(name = "CRM_GC_SERVICE_REQUEST")
public class GiftCardServiceRequest {

    @Id
    @Column(name = "ID")
    @GeneratedValue
    private Long id;
    @NotNull
    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "CARD_NO", referencedColumnName = "BARCODE", nullable = false)
    private GiftCardInventory giftcard;
    @Column(name = "FILED_BY", nullable = false)
    private String filedBy;
    @JoinColumn(name = "FILED_IN")
    @ManyToOne(optional = false)
    private Store filedIn;
    @Column(name = "DATE_FILED")
    @Type(type = "org.jadira.usertype.dateandtime.joda.PersistentDateTime")
    private DateTime dateFiled;
    @Column(name = "DETAILS", length = 500, nullable = false)
    @Size(max = 500)
    private String details;

    public Long getId() {
	return id;
    }

    public void setId(Long id) {
	this.id = id;
    }

    public GiftCardInventory getGiftcard() {
	return giftcard;
    }

    public void setGiftcard(GiftCardInventory giftcard) {
	this.giftcard = giftcard;
    }

    public Store getFiledIn() {
	return filedIn;
    }

    public void setFiledIn(Store filedIn) {
	this.filedIn = filedIn;
    }

    public String getFiledBy() {
	return filedBy;
    }

    public void setFiledBy(String filedBy) {
	this.filedBy = filedBy;
    }

    public DateTime getDateFiled() {
	return dateFiled;
    }

    public void setDateFiled(DateTime dateFiled) {
	this.dateFiled = dateFiled;
    }

    public String getDetails() {
	return details;
    }

    public void setDetails(String details) {
	this.details = details;
    }

    public GiftCardServiceRequest giftCard(GiftCardInventory giftCard) {
	this.giftcard = giftCard;
	return this;
    }

    public GiftCardServiceRequest details(String details) {
	this.details = details;
	return this;
    }

    public GiftCardServiceRequest filedIn(Store store) {
	this.filedIn = store;
	return this;
    }

    public GiftCardServiceRequest filedBy(String currentUser) {
	this.filedBy = currentUser;
	return this;
    }

    public GiftCardServiceRequest dateFiled(DateTime dateFiled) {
	this.dateFiled = dateFiled;
	return this;
    }
}
