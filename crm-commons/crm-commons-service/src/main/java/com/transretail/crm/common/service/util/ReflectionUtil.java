package com.transretail.crm.common.service.util;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;

import com.transretail.crm.common.service.exception.GenericServiceException;

/**
 * @author Allan G. Ramirez (agramirez@exist.com)
 */
public enum ReflectionUtil {
    INSTANCE;

    public void invokePrivateMethod(Class<?> clazz, Object target, String methodName, Class<?>[] parameterTypes, Object... args) {
        try {
            Method method = clazz.getDeclaredMethod(methodName, parameterTypes);
            method.setAccessible(true);
            method.invoke(target, args);
        } catch (NoSuchMethodException e) {
            throw new GenericServiceException(e);
        } catch (IllegalAccessException e) {
            throw new GenericServiceException(e);
        } catch (InvocationTargetException e) {
            throw new GenericServiceException(e);
        }
    }

    public void setPrivateFieldValue(Class<?> clazz, Object target, String fieldName, Object value) throws GenericServiceException {
        try {
            Field field = clazz.getDeclaredField(fieldName);
            field.setAccessible(true);
            field.set(target, value);
        } catch (NoSuchFieldException e) {
            throw new GenericServiceException(e);
        } catch (IllegalAccessException e) {
            throw new GenericServiceException(e);
        }
    }

    public <T> T getPrivateFieldValue(Class<?> clazz, Object target, String fieldName) {
        try {
            Field field = clazz.getDeclaredField(fieldName);
            field.setAccessible(true);
            return (T) field.get(target);
        } catch (NoSuchFieldException e) {
            throw new GenericServiceException(e);
        } catch (IllegalAccessException e) {
            throw new GenericServiceException(e);
        }
    }

    public void copyDeclaredFieldValues(Object source, Object target, Class<?> referencedClass) {
        for (Field field : referencedClass.getDeclaredFields()) {
            if (!Modifier.isFinal(field.getModifiers())) {
                try {
                    Object sourceValue = getPrivateFieldValue(referencedClass, source, field.getName());
                    field.setAccessible(true);
                    field.set(target, sourceValue);
                } catch (IllegalAccessException e) {
                    throw new GenericServiceException(e);
                }
            }
        }
    }
}
