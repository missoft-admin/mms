package com.transretail.crm.report.template.impl;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.MessageSourceAware;
import org.springframework.context.support.MessageSourceAccessor;
import org.springframework.stereotype.Service;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.transretail.crm.common.reporting.jasper.ClasspathFileResolver;
import com.transretail.crm.common.reporting.jasper.DefaultJRProcessor;
import com.transretail.crm.common.reporting.jasper.JRProcessor;
import com.transretail.crm.core.entity.enums.ReportType;
import com.transretail.crm.giftcard.entity.GiftCardExpiryExtension;
import com.transretail.crm.giftcard.entity.GiftCardInventory;
import com.transretail.crm.giftcard.entity.QGiftCardExpiryExtension;
import com.transretail.crm.giftcard.entity.SalesOrder;
import com.transretail.crm.giftcard.entity.SalesOrderAlloc;
import com.transretail.crm.giftcard.entity.SalesOrderItem;
import com.transretail.crm.giftcard.entity.support.GiftCardHandlingFeeType;
import com.transretail.crm.giftcard.repo.GiftCardExpiryExtensionRepo;
import com.transretail.crm.giftcard.repo.SalesOrderRepo;
import com.transretail.crm.giftcard.service.GiftCardInventoryService;
import com.transretail.crm.giftcard.service.ProductProfileService;
import com.transretail.crm.report.template.FilterField;

/**
 * @author ftopico
 */
@Service("giftCardDateExpiryDocument")
public class GiftCardDateExpiryDocument implements MessageSourceAware {
    
    @Autowired
    private GiftCardExpiryExtensionRepo repo;
    @Autowired
    private SalesOrderRepo orderRepo;
    @Autowired
    private ProductProfileService productProfileService;
    @Autowired
    private GiftCardInventoryService giftCardInventoryService;

    protected static final String TEMPLATE_NAME = "Gift Card Date Expiry Extension Document";
    protected static final String REPORT_NAME = "reports/gift_card_date_expiry_document.jasper";
    //Header Params
    private static final String PARAM_CREATE_DATE = "createDate";
    private static final String PARAM_NEW_EXPIRY_DATE = "newExpiryDate";
    private static final String PARAM_HANDLING_FEE = "handlingFee";
    private static final String PARAM_CUSTOMER_NAME = "customerName";
    private static final String PARAM_AMOUNT = "amount";
    private static final String PARAM_CREATED_BY = "createdBy";
    private static final String PARAM_IS_EMPTY = "isEmpty";
    
    private static final DateTimeFormatter FULL_DATE_PATTERN = DateTimeFormat.forPattern("MMMM d, yyyy");
    
    private MessageSourceAccessor messageSource;

    public ReportType[] getReportTypes() {
        return ReportType.values();
    }
    
    @Override
    public void setMessageSource(MessageSource messageSource) {
        this.messageSource = new MessageSourceAccessor(messageSource);
    }
    
    public String getName() {
        return TEMPLATE_NAME;
    }
    
    public String getDescription() {
        return messageSource.getMessage("title_document_date_expiry");
    }
    
    public Set<FilterField> getFilters() {
        return null;
    }
    
    public JRProcessor createJrProcessor(String extendNo) {
        
        Map<String, Object> parameters = Maps.newHashMap();
        
        QGiftCardExpiryExtension qModel = QGiftCardExpiryExtension.giftCardExpiryExtension;
        GiftCardExpiryExtension model = repo.findOne(qModel.extendNo.eq(extendNo));
        
        int totalCount = 0;
        String series = new String();
        String product = new String();
        String customerName = new String();
        List<Long> giftCardSeriesList = new ArrayList<Long>();
        GiftCardInventory giftCardInventory = new GiftCardInventory();
        if (model.getOrder() != null) {
            SalesOrder salesOrder = orderRepo.findOne(model.getOrder().getId());
            StringBuilder sb = new StringBuilder();
            Long seriesNo = null;
            for (SalesOrderItem soItem : salesOrder.getItems()) {
                totalCount = totalCount + soItem.getSoAlloc().size();
                for (SalesOrderAlloc soAlloc : soItem.getSoAlloc()) {
//                    if (soAlloc.getGcInventory() != null && soAlloc.getGcInventory().getSeries() != null)
//                        giftCardSeriesList.add(soAlloc.getGcInventory().getSeries());
                	sb.append( soAlloc.getSeriesStart() + " - " + soAlloc.getSeriesEnd() + ", " );
                	if ( null == seriesNo ) {
                		seriesNo = soAlloc.getSeriesStart();
                	}
                }
            }
            Collections.sort(giftCardSeriesList);
//            series = String.valueOf(giftCardSeriesList.get(0)) + " - " + String.valueOf(giftCardSeriesList.get(giftCardSeriesList.size() - 1));
//            giftCardInventory = giftCardInventoryService.getGiftCardBySeries(giftCardSeriesList.get(0));
            series = sb.deleteCharAt( sb.lastIndexOf( "," ) ).toString();
            giftCardInventory = giftCardInventoryService.getGiftCardBySeries( seriesNo );
            product = giftCardInventory.getProductCode() + " - " + giftCardInventory.getProductName();
            customerName = salesOrder.getCustomer().getName();
        } else {
            totalCount = (int) (model.getSeriesTo() - model.getSeriesFrom()) + 1;
            series = String.valueOf(model.getSeriesFrom()) + " - " + String.valueOf(model.getSeriesTo());
            giftCardInventory = giftCardInventoryService.getGiftCardBySeries(model.getSeriesFrom());
            product = giftCardInventory.getProductCode() + " - " + giftCardInventory.getProductName();
            customerName = "N/A";
        }
        
        
        parameters.put(PARAM_CREATE_DATE, FULL_DATE_PATTERN.print(model.getCreateDate()));
        parameters.put(PARAM_NEW_EXPIRY_DATE, FULL_DATE_PATTERN.print(model.getExpiryDate()));
        parameters.put(PARAM_CUSTOMER_NAME, customerName);
        
        DecimalFormat df = new DecimalFormat("#,###,###,##0");
        Double handlingFee = new Double(0);
        Double faceAmount = giftCardInventory.getFaceValue().doubleValue()*totalCount;
        if (model.getHandlingFeeType() == GiftCardHandlingFeeType.PER_CARD && model.getHandlingFee() != null) {
            handlingFee = totalCount*model.getHandlingFee();
            parameters.put(PARAM_AMOUNT, df.format(faceAmount+(handlingFee*totalCount)));
            parameters.put(PARAM_HANDLING_FEE, df.format(handlingFee*totalCount) 
                    + " ("+ df.format(handlingFee) + " " + messageSource.getMessage("gc_date_expiry_lbl_handling_fee_type_PER_CARD")+ ")");
        } else if (model.getHandlingFeeType() == GiftCardHandlingFeeType.FIXED && model.getHandlingFee() != null) {
            handlingFee = model.getHandlingFee();
            parameters.put(PARAM_AMOUNT, df.format(faceAmount+handlingFee));
            parameters.put(PARAM_HANDLING_FEE, df.format(handlingFee) 
                    + " " + messageSource.getMessage("gc_date_expiry_lbl_handling_fee_type_FIXED"));
        } else {
            parameters.put(PARAM_AMOUNT, df.format(0));
        }
        
        parameters.put(PARAM_CREATED_BY, model.getCreateUser());
        
        List<ReportBean> beansDataSource = Lists.newArrayList();
        beansDataSource.add(new ReportBean(product, series, df.format(totalCount),
                df.format(giftCardInventory.getFaceValue().longValueExact()),
                df.format(faceAmount))); 
        
        DefaultJRProcessor jrProcessor = new DefaultJRProcessor(REPORT_NAME, beansDataSource);
        jrProcessor.addParameters(parameters);
        jrProcessor.setFileResolver(new ClasspathFileResolver("reports"));
        return jrProcessor;
    }
    
    public static class ReportBean {
        private String product;
        private String series;
        private String quantity;
        private String faceValue;
        private String faceAmount;
        
        public ReportBean() {}
        
        public ReportBean(String product,String series, String quantity, String faceValue, String faceAmount) {
            super();
            this.product = product;
            this.series = series;
            this.quantity = quantity;
            this.faceValue = faceValue;
            this.faceAmount = faceAmount;
        }

        public String getProduct() {
            return product;
        }

        public void setProduct(String product) {
            this.product = product;
        }

        public String getSeries() {
            return series;
        }

        public void setSeries(String series) {
            this.series = series;
        }

        public String getQuantity() {
            return quantity;
        }

        public void setQuantity(String quantity) {
            this.quantity = quantity;
        }

        public String getFaceValue() {
            return faceValue;
        }

        public void setFaceValue(String faceValue) {
            this.faceValue = faceValue;
        }

        public String getFaceAmount() {
            return faceAmount;
        }

        public void setFaceAmount(String faceAmount) {
            this.faceAmount = faceAmount;
        }

    }
}
