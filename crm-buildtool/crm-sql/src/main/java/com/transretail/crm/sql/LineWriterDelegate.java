package com.transretail.crm.sql;

/**
 * @author Allan G. Ramirez (agramirez@exist.com)
 */
public interface LineWriterDelegate {
    void write(String line) throws Exception;
}
