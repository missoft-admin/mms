package com.transretail.crm.report.template.impl;

import com.mysema.query.BooleanBuilder;
import com.mysema.query.jpa.impl.JPAQuery;
import com.mysema.query.types.Projections;
import com.mysema.query.types.QBean;
import com.transretail.crm.common.reporting.engine.data.JRQueryDSLDataSource;
import com.transretail.crm.common.reporting.jasper.ClasspathFileResolver;
import com.transretail.crm.common.reporting.jasper.DefaultJRProcessor;
import com.transretail.crm.common.reporting.jasper.JRProcessor;
import com.transretail.crm.common.util.DateUtil;
import com.transretail.crm.core.entity.QPeopleSoftStore;
import com.transretail.crm.core.entity.enums.ReportType;
import com.transretail.crm.core.entity.lookup.LookupDetail;
import com.transretail.crm.core.entity.lookup.QStore;
import com.transretail.crm.core.entity.lookup.Store;
import com.transretail.crm.core.reporting.support.StoreFieldValueCustomizer;
import com.transretail.crm.core.security.util.UserUtil;
import com.transretail.crm.core.service.CodePropertiesService;
import com.transretail.crm.core.service.LookupService;
import com.transretail.crm.core.service.StoreService;
import com.transretail.crm.giftcard.entity.QGiftCardInventory;
import com.transretail.crm.giftcard.entity.QGiftCardTransaction;
import com.transretail.crm.giftcard.entity.QGiftCardTransactionItem;
import com.transretail.crm.giftcard.entity.QSalesOrder;
import com.transretail.crm.giftcard.entity.support.GiftCardSaleTransaction;
import com.transretail.crm.report.template.FilterField;
import com.transretail.crm.report.template.NoDataFoundSupport;
import com.transretail.crm.report.template.ReportTemplate;
import java.util.*;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.JREmptyDataSource;
import org.joda.time.LocalDate;
import org.joda.time.YearMonth;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.MessageSourceAware;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.context.support.MessageSourceAccessor;
import org.springframework.stereotype.Service;

/**
 * @author ftopico
 */
@Service("affiliateTransactionRedemptionReport")
public class AffiliateTransactionRedemptionReport implements ReportTemplate, MessageSourceAware, NoDataFoundSupport {
    
    @PersistenceContext
    private EntityManager em;
    @Autowired
    private LookupService lookupService;
    @Autowired
    private CodePropertiesService codePropertiesService;
    @Autowired
    private StoreService storeService;

    private static final String PERMISSION_CODE = "GC_AFFILIATE_TRANSACTION_REDEMPTION";
    protected static final String TEMPLATE_NAME = "Affiliate Transaction Redemption Report";
    protected static final String REPORT_NAME = "reports/affiliate_transaction_redemption_report.jasper";
    protected static final String REPORT_FILTER_DATE_FROM = "DATE_FROM";
    protected static final String REPORT_FILTER_DATE_TO = "DATE_TO";
    protected static final String REPORT_FILTER_BU = "BU";
    protected static final String REPORT_FILTER_REDEMPTION_STORE = "REDEEM_STORE";
    protected static final String REPORT_FILTER_DATE_FORMAT = "dd-MM-yyyy";
    //Header Params
    private static final String PARAM_PRINT_DATE = "printedDate";
    private static final String PARAM_PRINTED_BY = "printedBy";
    private static final String PARAM_TRANSACTION_DT_FROM = "transactionDateFrom";
    private static final String PARAM_TRANSACTION_DT_TO = "transactionDateTo";
    private static final String PARAM_REDEMPTION_STORE = "redeemStore";
    private static final String PARAM_BU = "bu";
    private static final DateTimeFormatter FULL_DATE_PATTERN = DateTimeFormat.forPattern("dd-MM-yyyy").withLocale(LocaleContextHolder.getLocale());
    private static final DateTimeFormatter RAW_YEAR_MONTH_FORMATTER = DateTimeFormat.forPattern("yyyyMM");
    private MessageSourceAccessor messageSource;
    
    public AffiliateTransactionRedemptionReport() {
    }
    
    @Override
    public void setMessageSource(MessageSource messageSource) {
        this.messageSource = new MessageSourceAccessor(messageSource);
    }

    @Override
    public String getName() {
        return TEMPLATE_NAME;
    }

    @Override
    public String getDescription() {
        return messageSource.getMessage("gc_affiliate_transaction_lbl_redemption_title");
    }

    @Override
    public Set<FilterField> getFilters() {
        Set<FilterField> filters = new LinkedHashSet<FilterField>();
        filters.add(FilterField.createDateField(REPORT_FILTER_DATE_FROM,
                messageSource.getMessage("gc_expirebalance_report_label_datefrom"),
                DateUtil.SEARCH_DATE_FORMAT, FilterField.DateTypeOption.PLAIN, true));
        filters.add(FilterField.createDateField(REPORT_FILTER_DATE_TO,
                messageSource.getMessage("gc_expirebalance_report_label_dateto"),
                DateUtil.SEARCH_DATE_FORMAT, FilterField.DateTypeOption.PLAIN, true));
        
        //BU
        Map<String, String> bu = new LinkedHashMap<String, String>();
        bu.put("", "");
        List<LookupDetail> buList = lookupService.getActiveDetailsByHeaderCode(codePropertiesService.getHeaderBusinessUnit());
        Collections.sort(buList, ALPHA_ORDER);
        for (LookupDetail buDetail : buList) {
            //ID030 is Carrefour
            if (!buDetail.getCode().equalsIgnoreCase("ID030"))
                bu.put(buDetail.getCode(),buDetail.getDescription());
        }
        filters.add(FilterField.createDropdownField(REPORT_FILTER_BU, messageSource.getMessage("gc_transaction_lbl_bu"), bu));
        
        //Stores
        Map<String, String> stores = new LinkedHashMap<String, String>();
        stores.put("", "");
        stores.put(codePropertiesService.getDetailInvLocationHeadOffice(), lookupService.getDetailByCode(codePropertiesService.getDetailInvLocationHeadOffice()).getDescription());
        for (Store store : storeService.getAllStores()) {
            stores.put(store.getCode(), store.getCode() + " - " + store.getName());
        }
        filters.add(FilterField.createDropdownField(REPORT_FILTER_REDEMPTION_STORE, messageSource.getMessage("gc_affiliate_transaction_lbl_redeem_store"), stores));
        
        return filters;
    }

    private static Comparator<LookupDetail> ALPHA_ORDER = new Comparator<LookupDetail>() {
        public int compare(LookupDetail ld1, LookupDetail ld2) {
            int x = String.CASE_INSENSITIVE_ORDER.compare(ld1.getDescription(), ld2.getDescription());
            if (x== 0) {
                x= ld1.getDescription().compareTo(ld2.getDescription());
            }
            return x;
        }
    };
    
    @Override
    public boolean canView(Set<String> permissions) {
        return permissions.contains(PERMISSION_CODE);
    }

    @Override
    public ReportType[] getReportTypes() {
        return ReportType.values();
    }

    @Override
    public JRProcessor createJrProcessor(Map<String, String> map) {
        Map<String, Object> parameters = new HashMap<String, Object>();
        parameters.put(PARAM_PRINT_DATE, FULL_DATE_PATTERN.print(new LocalDate()));
        parameters.put(PARAM_PRINTED_BY, getCurrentUser());
        parameters.put(PARAM_TRANSACTION_DT_FROM, map.get(REPORT_FILTER_DATE_FROM));
        parameters.put(PARAM_TRANSACTION_DT_TO, map.get(REPORT_FILTER_DATE_TO));
        parameters.put(PARAM_BU, map.get(REPORT_FILTER_BU));
        parameters.put(PARAM_REDEMPTION_STORE, map.get(REPORT_FILTER_REDEMPTION_STORE));
        
        //Store
        if (!map.get(REPORT_FILTER_REDEMPTION_STORE).equalsIgnoreCase("")) {
            String store = null;
            if (map.get(REPORT_FILTER_REDEMPTION_STORE).equalsIgnoreCase(codePropertiesService.getDetailInvLocationHeadOffice())) {
                LookupDetail detail = lookupService.getDetailByCode(codePropertiesService.getDetailInvLocationHeadOffice());
                store = detail.getCode() + " - " + detail.getDescription();
            } else {
                Store modelStore = storeService.getStoreByCode(map.get(REPORT_FILTER_REDEMPTION_STORE));
                if (modelStore != null)
                    store = modelStore.getCodeAndName();
            }
            parameters.put(REPORT_FILTER_REDEMPTION_STORE, store);
        }
        
        DefaultJRProcessor jrProcessor = new DefaultJRProcessor(REPORT_NAME, new JREmptyDataSource(1));
        jrProcessor.addParameter("SUB_DATASOURCE", createDataSource(map));
        jrProcessor.addParameters(parameters);
        jrProcessor.setFileResolver(new ClasspathFileResolver("reports"));
        
        return jrProcessor;
    }
    
    private JPAQuery createQuery(Map<String, String> map) {
        QGiftCardTransaction qtx = QGiftCardTransaction.giftCardTransaction;
        QPeopleSoftStore qp = QPeopleSoftStore.peopleSoftStore;
        QGiftCardTransactionItem qtxi = QGiftCardTransactionItem.giftCardTransactionItem;
        QGiftCardInventory qgc = QGiftCardInventory.giftCardInventory;
        QSalesOrder qso = QSalesOrder.salesOrder;
        QStore qs = QStore.store;

        BooleanBuilder exp = new BooleanBuilder(qtx.transactionDate.between(FULL_DATE_PATTERN.parseLocalDateTime(map.get(REPORT_FILTER_DATE_FROM)),
                DateUtil.getEndOfDay(FULL_DATE_PATTERN.parseLocalDateTime(map.get(REPORT_FILTER_DATE_TO)))));

        exp.and(qp.businessUnit.code.ne(codePropertiesService.getDtlGcBuC4Default()));
        exp.and(qtx.transactionType.in(GiftCardSaleTransaction.REDEMPTION, GiftCardSaleTransaction.VOID_REDEMPTION));

        if (!map.get(REPORT_FILTER_BU).equalsIgnoreCase("")) {
            exp.and(qp.businessUnit.code.eq(map.get(REPORT_FILTER_BU)));
        }
        if (!map.get(REPORT_FILTER_REDEMPTION_STORE).equalsIgnoreCase("")) {
            exp.and(qs.code.eq(map.get(REPORT_FILTER_REDEMPTION_STORE)));
        }

        JPAQuery query = new JPAQuery(em).from(qtx)
                .leftJoin(qtx.peoplesoftStoreMapping, qp)
                .leftJoin(qp.store, qs)
                .leftJoin(qtx.transactionItems, qtxi)
                .leftJoin(qtxi.giftCard, qgc)
                .leftJoin(qtx.salesOrder, qso)
                .where(exp.getValue());

        return query;
    }
    private JRDataSource createDataSource(Map<String, String> map) {
        QGiftCardTransaction qtx = QGiftCardTransaction.giftCardTransaction;
        QPeopleSoftStore qp = QPeopleSoftStore.peopleSoftStore;
        QGiftCardTransactionItem qtxi = QGiftCardTransactionItem.giftCardTransactionItem;
        QGiftCardInventory qgc = QGiftCardInventory.giftCardInventory;
        QSalesOrder qso = QSalesOrder.salesOrder;
        QStore qs = QStore.store;
        JPAQuery query = createQuery(map) .groupBy(qtx.transactionDate.yearMonth(),
                qtx.peoplesoftStoreMapping.businessUnit.code,
                qtx.merchantId,
                qp.peoplesoftCode)
                .orderBy(qtx.transactionDate.yearMonth().desc());
        
        QBean<AffiliateRedemptionReport> projections = Projections.fields(AffiliateRedemptionReport.class,
                qtx.transactionDate.yearMonth().as("transactionDate"),
                qtx.peoplesoftStoreMapping.businessUnit.code.as("bu"),
                qtx.merchantId.as("redeemStore"),
                qp.peoplesoftCode.as("psStore"),
                qtxi.transactionAmount.sum().doubleValue().coalesce(0.0).as("faceAmount"),
                qtx.discountPercentage.multiply(qtxi.transactionAmount).sum().doubleValue().coalesce(0.0).as("discount"));
        
        return new JRQueryDSLDataSource(query, projections, 
                new StoreFieldValueCustomizer("redeemStore", storeService));
    }
    
    private String getCurrentUser() {
        if (UserUtil.getCurrentUser() != null) {
            return UserUtil.getCurrentUser().getUsername();
        } else {
            return null;
        }
    }

    @Override
    public boolean isEmpty(Map<String, String> filter) {
        return !createQuery(filter).exists();
    }

    public static class AffiliateRedemptionReport {
        private Integer transactionDate;
        private String bu;
        private String redeemStore;
        private Double faceAmount;
        private Double discount;
        private String psStore;
        
        public AffiliateRedemptionReport() {
        }

        public Integer getTransactionDate() {
            return transactionDate;
        }

        public YearMonth getYearMonth() {
            return YearMonth.parse(String.valueOf(transactionDate), RAW_YEAR_MONTH_FORMATTER);
        }
        
        public String getBu() {
            return bu;
        }

        public String getRedeemStore() {
            return redeemStore;
        }

        public void setRedeemStore(String redeemStore) {
            this.redeemStore = redeemStore;
        }

        public Double getFaceAmount() {
            return faceAmount;
        }

        public Double getDiscount() {
            return discount;
        }

        public void setTransactionDate(Integer transactionDate) {
            this.transactionDate = transactionDate;
        }

        public void setBu(String bu) {
            this.bu = bu;
        }

        public void setFaceAmount(Double faceAmount) {
            this.faceAmount = faceAmount;
        }

        public void setDiscount(Double discount) {
            this.discount = discount;
        }

        public String getPsStore() {
            return psStore;
        }

        public void setPsStore(String psStore) {
            this.psStore = psStore;
        }
    }
}
