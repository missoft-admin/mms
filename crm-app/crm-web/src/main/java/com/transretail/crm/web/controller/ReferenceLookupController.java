package com.transretail.crm.web.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.google.common.collect.Lists;
import com.transretail.crm.common.service.dto.response.CodeDescDto;
import com.transretail.crm.core.entity.lookup.LookupDetail;
import com.transretail.crm.core.entity.lookup.LookupHeader;
import com.transretail.crm.core.service.LookupService;

/**
 * @author Allan G. Ramirez (agramirez@exist.com)
 */

@Controller
@RequestMapping("/reference")
public class ReferenceLookupController {
    @Autowired
    private LookupService lookupService;

    /**
     * List active LookupDetails given header code
     *
     * @param headerCode
     * @return list of active lookup details given header code
     */
    @RequestMapping(value = "/list/byheadercode/{headerCode}", method = RequestMethod.GET)
    @ResponseBody
    public List<CodeDescDto> listActiveDetailsByHeaderCode(@PathVariable("headerCode") String headerCode) {
        List<CodeDescDto> result = Lists.newArrayList();
        for (LookupDetail detail : lookupService.getActiveDetails(new LookupHeader(headerCode))) {
            result.add(new CodeDescDto(detail.getCode(), detail.getDescription()));
        }
        return result;
    }
}
