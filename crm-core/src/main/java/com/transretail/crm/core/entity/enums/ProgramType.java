package com.transretail.crm.core.entity.enums;


public enum ProgramType {

    BASIC, REGULAR
}
