package com.transretail.crm.giftcard.dto;

import com.google.common.collect.Lists;
import com.mysema.query.types.expr.BooleanExpression;
import com.transretail.crm.common.service.dto.request.AbstractSearchFormDto;
import com.transretail.crm.giftcard.entity.QGiftCardTransaction;
import com.transretail.crm.giftcard.entity.QGiftCardTransactionItem;
import com.transretail.crm.giftcard.entity.support.GiftCardSaleTransaction;
import java.util.List;
import org.apache.commons.lang3.StringUtils;
import org.joda.time.LocalDate;

public class PosLoseTransactionSearchDto extends AbstractSearchFormDto {

    private Long id;
    private String cashierId;
    private String giftCardNo;
    private String terminalId;
    private String transactionNo;
    private String transactionType;
    private LocalDate dateFrom;
    private LocalDate dateTo;

    @Override
    public BooleanExpression createSearchExpression() {
	List<BooleanExpression> expr = Lists.newArrayList();
	QGiftCardTransaction qtx = QGiftCardTransaction.giftCardTransaction;
	QGiftCardTransactionItem qtxi = QGiftCardTransactionItem.giftCardTransactionItem;

	if (id != null) {
	    expr.add(qtx.id.eq(id));
	}

	if (StringUtils.isNotBlank(cashierId)) {
	    expr.add(qtx.cashierId.eq(cashierId));
	}

	if (StringUtils.isNotBlank(terminalId)) {
	    expr.add(qtx.terminalId.eq(terminalId));
	}
	
	if(StringUtils.isNotBlank(transactionType)){
	    expr.add(qtx.transactionType.eq(GiftCardSaleTransaction.valueOf(transactionType)));
	}
	
	// decoupled to executor
	if (StringUtils.isNotBlank(giftCardNo)) {
	    expr.add(qtxi.giftCard.barcode.eq(giftCardNo));
	}

	if (StringUtils.isNotBlank(transactionNo)) {
	    expr.add(qtx.transactionNo.eq(transactionNo));
	}

	if (dateFrom != null) {
	    expr.add(qtx.created.goe(dateFrom.toDateTimeAtStartOfDay()));
	}

	if (dateTo != null) {
	    expr.add(qtx.created.loe(dateTo.plusDays(1).toDateTimeAtStartOfDay()));
	}

	return BooleanExpression.allOf(expr.toArray(new BooleanExpression[expr.size()]));
    }

    public String getCashierId() {
	return cashierId;
    }

    public void setCashierId(String cashierId) {
	this.cashierId = cashierId;
    }

    public String getGiftCardNo() {
	return giftCardNo;
    }

    public void setGiftCardNo(String giftCardNo) {
	this.giftCardNo = giftCardNo;
    }

    public String getTerminalId() {
	return terminalId;
    }

    public void setTerminalId(String terminalId) {
	this.terminalId = terminalId;
    }

    public LocalDate getDateFrom() {
	return dateFrom;
    }

    public void setDateFrom(LocalDate dateFrom) {
	this.dateFrom = dateFrom;
    }

    public LocalDate getDateTo() {
	return dateTo;
    }

    public void setDateTo(LocalDate dateTo) {
	this.dateTo = dateTo;
    }

    public Long getId() {
	return id;
    }

    public void setId(Long id) {
	this.id = id;
    }

    public String getTransactionNo() {
	return transactionNo;
    }

    public void setTransactionNo(String transactionNo) {
	this.transactionNo = transactionNo;
    }

    public String getTransactionType() {
	return transactionType;
    }

    public void setTransactionType(String transactionType) {
	this.transactionType = transactionType;
    }
}