    create table CRM_SAFETYSTOCK (
        CREATED_BY varchar(255),
        CREATED_DATETIME timestamp,
        LAST_UPDATED_BY varchar(255),
        LAST_UPDATED_DATETIME timestamp,
        QTY_PER_MONTH varchar(255),
        PRODUCT_PROFILE_ID bigint not null,
        primary key (PRODUCT_PROFILE_ID)
    );

    alter table CRM_SAFETYSTOCK
        add constraint FK_ny2nbfldool2pfw5lo0klm1mg
        foreign key (PRODUCT_PROFILE_ID)
        references CRM_GC_PROD_PROFILE;