package com.transretail.crm.core.entity.enums;

public enum MaritalStatus {
	SINGLE, MARRIED, SEPARATED, DIVORCED, WIDOWED;
}
