package com.transretail.crm.giftcard;

import com.google.common.base.Joiner;
import com.google.common.collect.Lists;
import com.transretail.crm.giftcard.entity.PrepaidReloadInfo;
import com.transretail.crm.giftcard.entity.support.GiftCardInventoryStatus;
import com.transretail.crm.giftcard.entity.support.GiftCardSaleTransaction;
import java.text.MessageFormat;
import java.util.Collection;
import java.util.List;
import org.springframework.context.MessageSourceResolvable;

/**
 *
 * @author Monte Cillo Co (mco@exist.com)
 */
public class GiftCardServiceResolvableException extends RuntimeException implements MessageSourceResolvable {

    private final GiftCardErrorCode errorCode;
    private final String[] codes;
    private final Object[] arguments;
    private final String defaultMessage;

    public GiftCardServiceResolvableException(GiftCardErrorCode errorCode, String defaultMessage, String code, Object... arguments) {
        super(defaultMessage);
        this.errorCode = errorCode;
        this.codes = new String[]{code};
        this.arguments = arguments;
        this.defaultMessage = defaultMessage;
    }
    
    public GiftCardServiceResolvableException(GiftCardErrorCode errorCode, String defaultMessage, String code) {
        this(errorCode, defaultMessage, code, new Object[]{});
    }

    public GiftCardErrorCode getErrorCode() {
        return errorCode;
    }

    @Override
    public String[] getCodes() {
        return this.codes;
    }

    @Override
    public Object[] getArguments() {
        return arguments;
    }

    @Override
    public String getDefaultMessage() {
        return defaultMessage;
    }

    public static GiftCardServiceResolvableException giftCardNotPresentInMerchantException(String cardNo, String merchant) {
        Object[] args = {cardNo, merchant};
        return new GiftCardServiceResolvableException(GiftCardErrorCode.GC_NOT_PRESENT_IN_MERCHANT,
                MessageFormat.format("GC {0} is not present in merchant {1}.", args),
                "gc.not.present.in.merchant", args);
    }

    public static GiftCardServiceResolvableException merchantNotAllowedForGCTransactionsException(String merchant) {
        Object[] args = {merchant};
        return new GiftCardServiceResolvableException(GiftCardErrorCode.GC_TX_NOT_ALLOWED_IN_MERCHANT,
                MessageFormat.format("Current merchant {0} does not allowed for Gift Card Transaction.", args),
                "gc.merchant.not.allowed.gc_transaction", args);
    }

    public static GiftCardServiceResolvableException giftCardIsAlreadyBurnedException(String cardNo) {
        Object[] args = {cardNo};
        return new GiftCardServiceResolvableException(GiftCardErrorCode.GC_BURNED,
                MessageFormat.format("GC {0} is already burned.", args),
                "gc.already.burned", args);
    }

    public static GiftCardServiceResolvableException giftCardIsDeactivatedException(String cardNo) {
        return new GiftCardServiceResolvableException(GiftCardErrorCode.GC_DEACTIVATED,
                MessageFormat.format("GC {0} is deactivated.", new Object[]{cardNo}),
                "gc.deactivated", new Object[]{cardNo});
    }

    public static GiftCardServiceResolvableException giftCardStatusNotInStockStatusException(String cardNo, GiftCardInventoryStatus status) {
        return new GiftCardServiceResolvableException(GiftCardErrorCode.GC_NOT_IN_STOCK_STATUS,
                MessageFormat.format("GC {0} is not currently in stock, current status {1}.",
                        new Object[]{cardNo, status}),
                "gc.status.not.in_stock", new Object[]{cardNo, status});
    }

    public static GiftCardServiceResolvableException giftCardActivatedException(String cardNo) {
        return new GiftCardServiceResolvableException(GiftCardErrorCode.GC_ACTIVATED,
                MessageFormat.format("GC {0} is already activated.",
                        new Object[]{cardNo}),
                "gc.status.activated", new Object[]{cardNo});
    }

    public static GiftCardServiceResolvableException giftCardNotYetActivatedException(String cardNo) {
        return new GiftCardServiceResolvableException(GiftCardErrorCode.GC_NOT_YET_ACTIVATED,
                MessageFormat.format("GC {0} is not yet activated.",
                        new Object[]{cardNo}),
                "gc.status.not.yet.activated", new Object[]{cardNo});
    }

    public static GiftCardServiceResolvableException giftCardExpiredException(String cardNo) {
        return new GiftCardServiceResolvableException(GiftCardErrorCode.GC_EXPIRED,
                MessageFormat.format("GC {0} is already expired.",
                        new Object[]{cardNo}),
                "gc.status.expired", new Object[]{cardNo});
    }

    public static GiftCardServiceResolvableException refTransactionNoNotFoundException(String refTransactionNo) {
        return new GiftCardServiceResolvableException(GiftCardErrorCode.VOID_REF_TX_NOT_FOUND,
                MessageFormat.format("Ref. Transaction Number {0} is not found.",
                        new Object[]{refTransactionNo}),
                "gc.ref.tx.not.found", new Object[]{refTransactionNo});
    }

    public static GiftCardServiceResolvableException refTransactionTypeNotMatchException(GiftCardSaleTransaction transactionType, GiftCardSaleTransaction expected) {
        return new GiftCardServiceResolvableException(GiftCardErrorCode.VOID_REF_TX_TYPE_NOT_MATCH,
                "Invalid Ref. Transaction Number!", "gc.ref.tx.transaction.type.not.match", new Object[]{});
    }

    public static GiftCardServiceResolvableException giftCardNotAllowedForMultipleRedemptionException(String cardNo) {
        return new GiftCardServiceResolvableException(GiftCardErrorCode.GC_NOT_ALLOWED_FOR_MULTIPLE_REDEMPTION,
                MessageFormat.format("GC {0} is not eligible for multiple redemption.",
                        new Object[]{cardNo}),
                "gc.not.allowed.for.multiple.redemption", new Object[]{cardNo});
    }

    public static GiftCardServiceResolvableException giftCardInsufficientBalanceException(String cardNo, double balance) {
        return new GiftCardServiceResolvableException(GiftCardErrorCode.INSUFFICIENT_BALANCE,
                String.format("Insufficient balance. GC %s current balance is %,.0f.", cardNo, balance),
                "gc.insufficient.balance", new Object[]{cardNo, balance});
    }

    public static GiftCardServiceResolvableException prepaidCardNotAllowedToReloadException(String cardNo) {
        return new GiftCardServiceResolvableException(GiftCardErrorCode.GC_NOT_ALLOWED_TO_RELOAD,
                String.format("GC %s is not allowed to reload.", cardNo),
                "prepaid.card.reload.notallowed", new Object[]{cardNo});
    }

    public static GiftCardServiceResolvableException reloadInfoIsEmptyException(String product) {
        return new GiftCardServiceResolvableException(GiftCardErrorCode.GC_NOT_ALLOWED_FOR_MULTIPLE_REDEMPTION,
                String.format("Reload table of product %s not set.", product),
                "prepaid.card.reload.infos.empty", new Object[]{product});
    }

    public static GiftCardServiceResolvableException invalidReloadAmount(Collection<PrepaidReloadInfo> prepaidReloadInfos) {
        List<String> formattedAmounts = Lists.newArrayListWithExpectedSize(prepaidReloadInfos.size());
        for (PrepaidReloadInfo prepaidReload : prepaidReloadInfos) {
            formattedAmounts.add(String.format("%,d", prepaidReload.getReloadAmount()));
        }

        String presentation = Joiner.on(" | ").join(formattedAmounts);

        return new GiftCardServiceResolvableException(GiftCardErrorCode.GC_RELOAD_INVALID_DENOMINATION,
                String.format("Only reload amount(s) [%s] is allowed.", presentation),
                "prepaid.card.reload.amount.invalid", new Object[]{presentation});
    }

    public static GiftCardServiceResolvableException reloadExceededMaxAmount(Long maxAmount) {
        final String presentation = String.format("%,d", maxAmount);
        return new GiftCardServiceResolvableException(GiftCardErrorCode.GC_RELOAD_MAX_AMOUNT_EXCEEDED,
                String.format("Reload amount will exceed GC max amount (%s).", presentation),
                "prepaid.card.profile.maxamount.exceeded", new Object[]{presentation});
    }

    public static GiftCardServiceResolvableException giftCardNotFoundException(String cardNo) {
        return new GiftCardServiceResolvableException(GiftCardErrorCode.GC_NOT_FOUND,
                String.format("GC %s is not found.", cardNo),
                "gc.not.found", new Object[]{cardNo});
    }

    public static GiftCardServiceResolvableException accountIdNotFoundException(String accountId) {
        return new GiftCardServiceResolvableException(GiftCardErrorCode.GC_ACCOUNT_ID_NOT_FOUND,
                String.format("Account Id '%s' is not found.", accountId),
                "gc.account.id.not.found", new Object[]{accountId});
    }

    public static GiftCardServiceResolvableException transactionNoAlreadyExistException(String transactionNo) {
        return new GiftCardServiceResolvableException(GiftCardErrorCode.GC_TX_TX_NO_EXIST,
                String.format("Transaction No '%s' is already exists.", transactionNo),
                "gc.poslose.transactionNo.exists", new Object[]{transactionNo});
    }

    public static GiftCardServiceResolvableException noAvailableGiftCardException() {
        return new GiftCardServiceResolvableException(GiftCardErrorCode.GC_NOT_AVAILABLE,
                String.format("No available gift card", new Object[]{}),
                "gc.egc.err.noavailable", new Object[]{});
    }

    public static GiftCardServiceResolvableException insufficientGiftCardException(long quantity) {
        return new GiftCardServiceResolvableException(GiftCardErrorCode.GC_INSUFFICIENT,
                String.format("Only {0} available gift cards", new Object[]{quantity}),
                "gc.egc.err.insufficientgc", new Object[]{quantity});
    }

    public static GiftCardServiceResolvableException invalidGiftCardOldPinException() {
        return new GiftCardServiceResolvableException(GiftCardErrorCode.GC_PIN_INVALID,
                String.format("Invalid Old PIN", new Object[]{}),
                "gc.egc.err.invalidoldpin", new Object[]{});
    }

    public static GiftCardServiceResolvableException invalidGiftCardPinException() {
        return new GiftCardServiceResolvableException(GiftCardErrorCode.GC_PIN_INVALID,
                String.format("Invalid PIN", new Object[]{}),
                "gc.egc.err.invalidpin", new Object[]{});
    }

    public static GiftCardServiceResolvableException voidNotAllowedException() {
        return new GiftCardServiceResolvableException(GiftCardErrorCode.GC_VOID_NOT_ALLOWED,
                String.format("GC not allowed to void.", new Object[]{}), // TODO: error message
                "gc.not.allowed.to.void", new Object[]{});
    }

    public static GiftCardServiceResolvableException egcActivationNotAllowedException() {
        return new GiftCardServiceResolvableException(GiftCardErrorCode.GC_EGC_ACTIVATION_NOT_ALLOWED,
                String.format("Not allowed to activated E-GC.", new Object[]{}),
                "gc.not.allowed.to.activate.egc", new Object[]{});
    }

    public static GiftCardServiceResolvableException invalidBuGiftCardException(String gc) {

        return new GiftCardServiceResolvableException(GiftCardErrorCode.GC_INVALID_BU,
                String.format("%s can not be used here",  gc),
                "gc.invalid.bu.code", gc);
    }
    /**
     * Used when the transaction is already redeem and try to use it again in
     * pre-redeem
     *
     * @return
     */
    public static GiftCardServiceResolvableException transactionIsAlreadyClosedException(String transactionNo) {
        return new GiftCardServiceResolvableException(GiftCardErrorCode.GC_TX_ALREADY_CLOSED,
                String.format("Transaction No '%s' is already closed.", new Object[]{transactionNo}),
                "gc.tx.already.closed", new Object[]{transactionNo});
    }
    
    
    public static GiftCardServiceResolvableException noPreRedeemTransaction(String transactionNo){
        return new GiftCardServiceResolvableException(GiftCardErrorCode.GC_NO_PRE_REDEEM_TX,
                "Invalid transactionNo! Transaction {0} has no corresponding PRE_REDEEM transaction.",
                "gc.tx.no.pre_redeem", new Object[]{transactionNo});
    }
    
    public static GiftCardServiceResolvableException invalidCardForFreeVoucher(String cardNo){
        return new GiftCardServiceResolvableException(GiftCardErrorCode.GC_INVALID_CARD_NOT_FREE,
                MessageFormat.format("Invalid card. Following card is/are not free or does not exist : {0}", cardNo), 
                "giftcard_invalidcard_card_not_free", new Object[]{cardNo});
    }
    
    public static GiftCardServiceResolvableException invalidCardForB2C(String cardNo){
        return new GiftCardServiceResolvableException(GiftCardErrorCode.GC_INVALID_CARD_NO_FOR_SALE,
                null, "giftcard_invalidcard_card_is_free", new Object[]{cardNo});
    }
    
    public static GiftCardServiceResolvableException maximumQuantityReached(long maxQuantity){
        return new GiftCardServiceResolvableException(GiftCardErrorCode.EGC_MAX_REQUESTED_QUANTITY,
                MessageFormat.format("Invalid requested quantity. Quantity must be less than or equal to {0}.", maxQuantity),
                "egc.max.requested.quantity", maxQuantity);
    }
    
    public static GiftCardServiceResolvableException notAnElectronicGiftCard(){
        return new GiftCardServiceResolvableException(GiftCardErrorCode.GC_NOT_EGC, 
                "Invalid GC. GC is not electronic.", "gc.egc.err.nullgc");
    }
}
