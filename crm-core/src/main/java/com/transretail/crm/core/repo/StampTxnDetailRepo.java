package com.transretail.crm.core.repo;

import com.transretail.crm.common.repo.CrmQueryDslPredicateExecutor;
import com.transretail.crm.core.entity.StampTxnDetail;
import org.springframework.stereotype.Repository;

/**
 * @author Mike de Guzman
 */
@Repository
public interface StampTxnDetailRepo extends CrmQueryDslPredicateExecutor<StampTxnDetail, Long> {
}
