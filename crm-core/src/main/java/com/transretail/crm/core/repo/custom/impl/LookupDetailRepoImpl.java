package com.transretail.crm.core.repo.custom.impl;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import com.mysema.query.jpa.impl.JPAQuery;
import com.transretail.crm.core.entity.lookup.LookupHeader;
import com.transretail.crm.core.entity.lookup.QLookupDetail;
import com.transretail.crm.core.repo.custom.LookupDetailRepoCustom;

/**
 * @author Allan G. Ramirez (agramirez@exist.com)
 */
public class LookupDetailRepoImpl implements LookupDetailRepoCustom {
    @PersistenceContext
    EntityManager em;

    @Override
    public String getDescriptionByCode(String code) {
        QLookupDetail qLookupDetail = QLookupDetail.lookupDetail;
        return new JPAQuery(em).from(qLookupDetail).where(qLookupDetail.code.eq(code)).uniqueResult(qLookupDetail.description);
    }

    @Override
    public String getCodeByDescription(String headerCode, String description) {
        QLookupDetail qLookupDetail = QLookupDetail.lookupDetail;
        return new JPAQuery(em).from(qLookupDetail).where(
            qLookupDetail.header.code.eq(headerCode).and(qLookupDetail.description.equalsIgnoreCase(description))).uniqueResult(
            qLookupDetail.code);
    }
    
    @Override
    public Boolean hasDuplicateDescription(String code, String description, String header) {
    	QLookupDetail detail = QLookupDetail.lookupDetail;
    	JPAQuery query = new JPAQuery(em);
    	
    	return query.from(detail).where(detail.header.code.eq(header)
    			.and(detail.description.equalsIgnoreCase(description))
    			.and(detail.code.ne(code))).count() > 0;
    }
}
