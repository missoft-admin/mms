package com.transretail.crm.loyalty.entity.enums;

public enum LoyaltyInventoryTransaction {
	
	ALLOCATE, APPROVE_ALLOCATION, TRANSFER, RECEIVE

}
