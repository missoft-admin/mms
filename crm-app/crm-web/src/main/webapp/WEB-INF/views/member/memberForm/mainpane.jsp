<%@ include file="../../common/taglibs.jsp" %>


<div id="content_mainpane">

  <div id="accountDetails">
  <fieldset class="container-fluid">
    <legend><spring:message code="label_field_accountdetails" /></legend>

    <div class="pull-left">
      <div id="mainDetails">
        <div class="control-group">
          <label for="accountId" class="control-label col-sm-2"><spring:message code="member_prop_accountno" /></label>
          <div class="controls"><span class="accountId label col-sm-10"><spring:message code="msg_value_to_be_generated" /></span></div>
        </div>
        <div class="control-group">
          <label for="accountId" class="control-label col-sm-2"><spring:message code="member_prop_barcode" /></label>
          <div class="controls"><span class="accountId label col-sm-10"> <spring:message code="msg_value_to_be_generated" /></span></div>
        </div>
        <div class="control-group">
          <label for="memberType" class="control-label col-sm-2"><spring:message code="member_prop_type" /></label>
          <div class="controls">
            <span class="accountId label col-sm-10"><spring:message code="msg_value_to_be_generated" /></span>
            <%-- <form:select items="${memberType}" itemValue="code" itemLabel="description" path="memberType" multiple="false" cssClass="form-control" /> --%>
          </div>
        </div>

        <c:if test="${isEmployee}">
        <div class="control-group">
          <label for="employeeType" class="control-label col-sm-2"><spring:message code="member_emp_prop_type" /></label>
          <div class="controls"><form:select items="${employeeType}" itemValue="code" itemLabel="description" path="empType" multiple="false" cssClass="form-control" id="employeeType" /></div>
        </div>
        <div class="form-group">
          <label for="status" class="control-label"></label>
          <div class="checkbox">
            <input type="checkbox" name="accountStatus" value="TERMINATED">
            <label class="control-label"><spring:message code="member_label_terminated" /></label>
          </div>
        </div>
        </c:if>

        <div class="control-group">
          <label for="registeredStore" class="control-label"><spring:message code="member_prop_prefstore" /></label>
          <div class="controls">
            <c:if test="${not empty stores}"><form:select path="registeredStore" items="${stores}" itemLabel="codeAndName" itemValue="code" cssClass="form-control" /></c:if>
          </div>
        </div>
      </div>

      <div class="professionalDetails space-top-01">
        <hr />
        <div class="control-group">
          <label for="businessType" class="control-label"><spring:message code="label_com_transretail_crm_entity_customermodel_businesstype" /></label>
          <div class="controls"><form:select id="customerGroup" path="professionalProfile.customerGroup" items="${customerGroups}" itemLabel="description" itemValue="code" /></div>
        </div>
        <div class="control-group">
          <label for="businessType" class="control-label"></label>
          <div class="controls"><select name="professionalProfile.customerSegmentation" id="customerSegmentation"></select></div>
        </div>
        <div class="control-group">
          <label for="professionalProfile.businessName" class="control-label"><b class="required">*</b><spring:message code="member_prof_prop_businessname" /></label>
          <spring:message code="member_prof_prop_businessname" var="label_businessName" />
          <div class="controls"><input type="text" name="companyName" placeholder="${label_businessName}" /></div>
        </div>
        <div class="control-group">
          <label for="businessLicense" class="control-label"><spring:message code="member_prof_prop_businesslic" /></label>
          <spring:message code="member_prof_prop_businesslic" var="label_businessLicense" />
          <div class="controls"><form:input path="professionalProfile.businessLicense" placeholder="${label_businessLicense}" /></div>
        </div>
        <%-- <jsp:include page="npwppane.jsp" /> --%>
      </div>
    </div>


    <div class="pull-left">
      <div class="control-group">
        <label for="firstName" class="control-label"><b class="required">*</b><spring:message code="member_prop_fname" /></label>
        <spring:message code="member_prop_fname" var="label_firstName" />
        <div class="controls"><input type="text" name="firstName" class="form-control" placeholder="${label_firstName}" /></div>
      </div>
      <div class="control-group">
        <label for="lastName" class="control-label"><b class="required">*</b><spring:message code="member_prop_lname" /></label>
        <spring:message code="member_prop_lname" var="label_lastName" />
        <div class="controls"><input type="text" name="lastName" class="form-control" placeholder="${label_lastName}" /></div>
      </div>

      <div class="professionalDetails">
        <div class="control-group">
          <label for="professionalProfile.position" class="control-label"><spring:message code="member_prof_prop_position" /></label>
          <spring:message code="member_prof_prop_position" var="label_position" />
          <div class="controls"><form:input path="professionalProfile.position" cssClass="form-control" placeholder="${label_position}" /></div>
        </div>
        <div class="control-group">
          <label for="ktpId" class="control-label"><spring:message code="member_prof_prop_ktpid" /></label>
          <spring:message code="member_prof_prop_ktpid" var="label_ktpId" />
          <div class="controls"><form:input path="ktpId" cssClass="form-control" placeholder="${label_ktpId}" /></div>
        </div>
      </div>

      <div class="control-group">
        <label for="gender" class="control-label"><b class="required">*</b><spring:message code="member_prop_gender" /></label>
        <div class="controls"><form:select path="customerProfile.gender" items="${gender}" itemLabel="description" itemValue="code" /></div>
      </div>
      <div class="control-group">
        <label for="birthdate" class="control-label"> <b class="required">*</b> <spring:message code="member_prop_bdate" /></label>
        <div class="controls"><form:input path="customerProfile.birthdate" id="birthdate" /></div>
      </div>
    </div>


    <div class="clearfix"></div>

  </fieldset>
  </div>


  <div id="">
    <div id="contactDetails" class="pull-left">
    <fieldset class="container-fluid">
      <legend><spring:message code="label_field_contactdetails" /></legend>

      <div class="control-group">
        <label for="contact" class="control-label"><b class="required">*</b><spring:message code="member_prop_contact" /></label>
        <div id="contact" class="controls">
          <spring:message code="member_prop_contact" var="label_contact" />
          <input type="text" name="contact" class="form-control" placeholder="${label_contact}" />
          <input type="button" onclick="addField('contact')" value="+"/>
        </div>
      </div>
      <div class="control-group">
        <label for="bestTimeToCall" class="control-label"><spring:message code="member_prop_timetocall" /></label>
        <div id="bestTimeToCall" class="controls bootstrap-timepicker">
          <spring:message code="member_prop_timetocall" var="label_timeToCall" />
          <form:input id="bestTimeToCall" path="bestTimeToCall" cssClass="form-control" placeholder="${label_timeToCall}" />
          <input type="button" onclick="addField('bestTimeToCall')" value="+"/>
        </div>
      </div>
      <div class="control-group">
        <label for="email" class="control-label"><spring:message code="member_prop_email" /></label>
        <spring:message code="member_prop_email" var="label_email" />
        <div class="controls"><input id="email" type="text" name="email" class="form-control" placeholder="${label_email}" /></div>
      </div>
    </fieldset>
    </div>

    <div id="loginDetails" class="pull-left">
    <fieldset class="form-horizontal container-fluid">
      <legend><spring:message code="label_field_logindetails" /></legend>

      <div class="control-group">
        <label for="username" class="control-label"><b class="required hasEmail">*</b><spring:message code="member_prop_username" /></label>
        <spring:message code="member_prop_username" var="label_username" />
        <div class="controls"><input type="text" name="username" class="form-control" placeholder="${label_username}" /></div>
      </div>
      <div class="control-group">
        <label for="password" class="control-label"><b class="required hasEmail">*</b><spring:message code="member_prop_password" /></label>
        <spring:message code="member_prop_password" var="label_password" />
        <div class="controls"><input type="password" name="password" class="form-control" placeholder="${label_password}" /></div>
      </div>
      <div class="control-group">
        <label for="pin" class="control-label"><b class="required">*</b><spring:message code="member_prop_pin" /></label>
        <spring:message code="member_prop_pin" var="label_pin" />
        <div class="controls"><form:password id="pin" path="pin" maxlength="6" cssClass="form-control intInput" placeholder="${label_pin}" /></div>
      </div>
    </fieldset>
    </div>

    <div id="deliveryChannels" class="pull-left">
    <fieldset class="form-horizontal container-fluid">
      <legend><spring:message code="label_field_deliverychannels" /></legend>

      <label for="channel.acceptEmail" class="control-label"></label>
      <div class="checkbox">
        <input type="checkbox" name="channel.acceptEmail">
        <label class="control-label"><spring:message code="member_prop_acceptemail" /></label>
      </div>

      <label for="channel.acceptSms" class="control-label"></label>
      <div class="checkbox">
        <input type="checkbox" name="channel.acceptSMS">
        <label class="control-label"><spring:message code="member_prop_acceptsms" /></label>
      </div>

      <label for="channel.acceptMail" class="control-label"></label>
      <div class="checkbox">
        <input type="checkbox" name="channel.acceptMail">
        <label class="control-label"><spring:message code="member_prop_acceptmail" /></label>
      </div>

      <label for="channel.acceptPhone" class="control-label"></label>
      <div class="checkbox">
        <input type="checkbox" name="channel.acceptPhone">
        <label class="control-label"><spring:message code="member_prop_acceptphone" /></label>
      </div>
    </fieldset>
    </div>

    <div class="clearfix"></div>
  </div>

</div>
