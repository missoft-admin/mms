/*
 * Copyright (c) 2012.
 * All rights reserved.
 */
package com.transretail.crm.core.security.model.impl;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.joda.time.LocalDate;

import com.transretail.crm.core.security.model.CustomSecurityUserInfo;

/**
 * @author mhua
 */
public class CustomSecurityUserInfoImpl implements CustomSecurityUserInfo {

    private String gender;

    private LocalDate birthDate;

    private String firstName;

    private String middleName;

    private String lastName;

    public CustomSecurityUserInfoImpl() {
    }

    public CustomSecurityUserInfoImpl(String gender, LocalDate birthDate, String firstName,
                                      String middleName, String lastName) {
        this.gender = gender;
        this.birthDate = birthDate;
        this.firstName = firstName;
        this.middleName = middleName;
        this.lastName = lastName;
    }

    @Override
    public String getGender() {
        return gender;
    }

    @Override
    public LocalDate getBirthDate() {
        return birthDate;
    }

    @Override
    public String getFirstName() {
        return firstName;
    }

    @Override
    public String getMiddleName() {
        return middleName;
    }

    @Override
    public String getLastName() {
        return lastName;
    }

    public String printInfo() {
        return new ToStringBuilder(this)
                .append("lastName", lastName)
                .append("firstName", firstName)
                .append("middleName", middleName)
                .append("gender", gender)
                .append("birthDate", birthDate)
                .toString();
    }

}
