package com.transretail.crm.giftcard.repo;

import com.transretail.crm.common.repo.CrmQueryDslPredicateExecutor;
import com.transretail.crm.giftcard.entity.GiftCardOrder;
import com.transretail.crm.giftcard.entity.support.GiftCardOrderStatus;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 *
 */
@Repository
public interface GiftCardOrderRepo extends CrmQueryDslPredicateExecutor<GiftCardOrder, Long> {

    List<GiftCardOrder> findByStatusIn(List<GiftCardOrderStatus> status);
}
