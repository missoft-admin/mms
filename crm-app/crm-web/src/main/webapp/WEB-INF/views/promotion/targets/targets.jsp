<%@ include file="../../common/taglibs.jsp" %>

<style type="text/css">
<!--
    .well-target .btn-small {
        margin-bottom: 20px;
        padding: 2px 10px;
        margin-left: 10px;
    }
     #link_promotionReward.btn-small, .content_promo .btn-small {
        padding: 2px 10px;
    }
    .control-group-02 .well-target {
        padding: 10px 0 0 0px !important;
    }
    #content_target .control-group-02 {
        margin-left: 20px;
        width: 265px !important;
    }
    #content_target .control-group-03 {
        margin-left: 0px;
    }
    .btn-small {
        margin-left: 10px;
    }
-->
</style>

<div class="span4 pull-left control-group-02 control-group-03">
<fieldset>
  <label class="block-stack-title"><spring:message code="target_member_group" /></label>

	<div class="">
	  <div class="pull-left">
	    <form:select path="targetMemberGroups" multiple="true">
	    	<c:forEach var="item" items="${promotion.targetMemberGroups}" >
	      	  <option value="${item}">${memberGroups[item]}</option>
	      	</c:forEach>
	    </form:select>
	  </div>
	  <a href="#" class="memberGroupLnk btn btn-small pull-left"><spring:message code="label_add" /></a>
	</div>
</fieldset>
</div>


<div class="span3 pull-left control-group-02">
<fieldset>
  <label class="block-stack-title"><spring:message code="target_store_group" /></label>

  <div class="">
    
    <div class=" pull-left">
      <form:select path="targetStoreGroups" multiple="true">
      	<c:forEach var="item" items="${promotion.targetStoreGroups}" >
      	  <option value="${item}">${storeGroups[item]}</option>
      	</c:forEach>
      </form:select>
    </div>
    <a href="#" class="storeGroupLnk btn btn-small pull-left"><spring:message code="label_add" /></a>
  </div>
</fieldset>
</div>

<c:if test="${!isRedemptionPromo}">
<div class="span3 pull-left control-group-02">
<fieldset>
  <label class="block-stack-title"><spring:message code="target_product_group" /></label>

  <div class="">
    
    <div class=" pull-left">
      <c:url var="url_getItemPoint" value="/promotion/reward/itempoint" />
      <form:select path="targetProductGroups" id="targetProductGroups" multiple="true" data-url="${url_getItemPoint}">
      	<c:forEach var="item" items="${promotion.targetProductGroups}" >
      	  <option value="${item}">${productGroups[item]}</option>
      	</c:forEach>
      </form:select>
    </div>
    <a href="#" id="productGroupLnk" class="btn btn-small pull-left"><spring:message code="label_add" /></a>
  </div>
</fieldset>
</div>

<div class="span3 pull-left control-group-02">
<fieldset>
  <label class="block-stack-title"><spring:message code="target_payment_type" /></label>

	<div class="">
	  
	  <div class=" pull-left">
	    <form:select path="targetPaymentTypes" multiple="true">
	    	<c:forEach var="item" items="${promotion.targetPaymentTypes}" >
              <c:choose>
                <c:when test="${not empty paymentTypes[item]}">
                  <option value="${item}">${paymentTypes[item]}</option>
                </c:when>
                <c:otherwise>
                  <option value="${item}">${paymentTypesWilcard[item]}</option>
                </c:otherwise>
              </c:choose>
	      	</c:forEach>
	    </form:select>
	    <c:forEach var="item" items="${paymentTypesWilcard}" >
          <input type="hidden" data-code="${item.key}" class="targetWildcardsIn targetWildcardsIn${item.key}" name="targetWildcards['${item.key}']" value="${promotion.targetWildcards[item.key]}" />
        </c:forEach>
	  </div>
	  <a href="#" id="paymentTypeLnk" class="btn btn-small pull-left"><spring:message code="label_add" /></a>
	</div>
</fieldset>
</div>
</c:if>

<div class="clearfix"></div>


<%-- <script src='<c:url value="/js/viewspecific/promotion/targets/productgroup.js"/>' type='text/javascript'><![CDATA[&nbsp; ]]></script> --%>
<script src='<c:url value="/js/viewspecific/promotion/targets/membergroup.js"/>' type='text/javascript'><![CDATA[&nbsp; ]]></script>
<script src='<c:url value="/js/viewspecific/promotion/targets/storegroup.js"/>' type='text/javascript'><![CDATA[&nbsp; ]]></script>
<script src='<c:url value="/js/viewspecific/promotion/targets/paymenttype.js"/>' type='text/javascript'><![CDATA[&nbsp; ]]></script>
<script src='<c:url value="/js/viewspecific/groups/membergroup.js"/>' type='text/javascript'><![CDATA[&nbsp; ]]></script>
