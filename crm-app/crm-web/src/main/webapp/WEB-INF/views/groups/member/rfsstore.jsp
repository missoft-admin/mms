<%@ include file="../../common/taglibs.jsp" %>

<style type="text/css">
<!--
  legend.rfsLegend {
    margin-bottom: 0px !important;
  }
  
  fieldset.rfsBlock {
    margin-top: 10px !important;
  }
  
  .form-search label, .form-inline label, .form-search .btn-group, .form-inline .btn-group {
    text-transform: lowercase;
  }
  
  .checkbox input[type="checkbox"] {
      margin-top: 3px;
  }
-->
</style>
 
<fieldset>
<legend class="block-stack-title rfsLegend"><spring:message code="label_rfs"/></legend>

<div class="form-horizontal">
  <div class="control-group">
    <label class="control-label"><spring:message code="label_member_grp_rfs_store_group" /></label>
    <div class="controls"><select name="rfs.storeGroup" id="storeGroup" data-url="<spring:url value="/groups/store/list" />"></select></div>
  </div>
  <div class="control-group">
    <label class="control-label"><spring:message code="label_member_grp_rfs_product_group" /></label>
    <div class="controls"><select name="rfs.productGroup" id="productGroup" data-url="<spring:url value="/groups/product/list" />"></select></div>
  </div>
  <%-- 
  <div class="control-group" style="margin-bottom: 10px;">      
    <label class="control-label"><spring:message code="label_rfs_recency"/></label>
    <div class="clearfix" style="padding-top: 5px;">
      <div class="span radio" style="margin-left:0"><input type="radio" class="recencyRadio" name="rfs.recency" value="" /><label><spring:message code="label_rfs_recency_none" /></label></div>
      <div class="span hide radio"><input type="radio" class="recencyRadio" name="rfs.recency" value="early" /><label><spring:message code="label_rfs_recency_early" /></label></div>
      <div class="span hide radio"><input type="radio" class="recencyRadio" name="rfs.recency" data-block="recentBlock" value="recent" /><label><spring:message code="label_rfs_recency_recent" /></label></div>
      <div class="span radio"><input type="radio" class="recencyRadio" name="rfs.recency" data-block="dateRangeBlock" value="dateRange" /><label><spring:message code="label_rfs_recency_daterange" /></label></div>
    </div>
  </div>
  
  <div class="">    
  <div class="form-inline clearfix" style="margin-bottom: -10px;">
    <div class="control-group hide recencyBlock" id="recentBlock">
      <label><spring:message code="label_rfs_recency_last" /></label>
      <input type="hidden" value="recentValue" /> <!-- populate with recentCnt+recentType -->
      <input type="text" class="input-mini" id="recentCnt" />
      <select id="recentType" class="">
        <option value=""><spring:message code="label_rfs_recency_lastlabel" /></option>
        <option value="w"><spring:message code="label_rfs_recency_lastweek" /></option>
        <option value="m"><spring:message code="label_rfs_recency_lastmonth" /></option>
      </select>
    </div>    
    <div class="control-group hide recencyBlock controls" id="dateRangeBlock">
       <input type="text" class="input-medium" id="dateRangeFrom" name="rfs.dateRangeFrom" />
       <label><spring:message code="label_rfs_to" /></label>
       <input type="text" class="input-medium" id="dateRangeTo" name="rfs.dateRangeTo" />
    </div>
  </div>
  </div> --%>
  
  <fieldset class="rfsBlock">    
  <div class="control-group" style="margin-left: -20px; margin-bottom: -10px;">
    <div class="checkbox control-label">
      <div class="pull-right" style="width:auto">
      <input type="checkbox" style="" value="dateRange" class="rfsCheckbox" name="rfs.recency" />
      <label class="" style="text-align:left"><spring:message code="label_rfs_recency" /></label>
      </div>
    </div>  
  
    <div class="form-inline clearfix">
      <div class="control-group">   
         <input type="text" class="input-medium" id="dateRangeFrom" name="rfs.dateRangeFrom" />
         <label><spring:message code="label_rfs_to" /></label>
         <input type="text" class="input-medium" id="dateRangeTo" name="rfs.dateRangeTo" />
      </div>
    </div>
  </div>  
  </fieldset>
      
  <fieldset class="rfsBlock">    
  <div class="control-group" style="margin-left: -20px; margin-bottom: -10px;">
    <div class="checkbox control-label">
      <div class="pull-right" style="width:auto">
      <input type="checkbox" style="" class="rfsCheckbox" name="rfs.frequency" />
      <label class="" style="text-align:left"><spring:message code="label_rfs_frequency" /></label>
      </div>
    </div>  
  
    <div class="form-inline clearfix">
      <div class="control-group">   
        <input type="text" class="input-small int frequenceField" name="rfs.frequencyFrom" />
        <label><spring:message code="label_rfs_frequencytimes" /></label>
        <label><spring:message code="label_rfs_to" /></label>
        <input type="text" class="input-small int frequenceField" name="rfs.frequencyTo" />
        <label><spring:message code="label_rfs_frequencytimes" /></label>
           <div class="checkbox" style="margin: 4px 0 0 200px">
             <input type="checkbox" class="blockCheckbox" id="zeroFrequency" name="rfs.zeroFrequency" />
             <label><spring:message code="label_rfs_zerofrequency" /></label>
           </div>
      </div>
    </div>
  </div>  
  </fieldset>
    
  <fieldset class="rfsBlock">      
  <div class="control-group" style="margin-left: -20px">  
      <div class="checkbox control-label">  
        <div class="pull-right" style="width:auto">   
          <input type="checkbox" style="" class="rfsCheckbox" name="rfs.spend" />
          <label class=""><spring:message code="label_rfs_spend" /></label>
        </div>
      </div>

      <div class="form-inline clearfix">
        <div class="control-group">
           <input type="text" class="input-small float" name="rfs.spendFrom" />
           <label><spring:message code="label_rfs_to" /></label>
           <input type="text" class="input-small float" name="rfs.spendTo" />
           <div class="checkbox" style="margin: 4px 0 0 200px">
             <input type="checkbox" class="blockCheckbox" name="rfs.spendPerTransaction" />
             <label><spring:message code="label_rfs_spendpertransaction" /></label>
           </div>
        </div>
      </div>
  </div>
  </fieldset>
    
    

  </div>
</fieldset>

