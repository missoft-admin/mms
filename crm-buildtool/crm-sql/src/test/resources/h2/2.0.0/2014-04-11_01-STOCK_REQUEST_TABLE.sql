drop table CRM_STOCK_REQUEST;

create table CRM_STOCK_REQUEST (
    id bigint generated by default as identity,
    CREATED_BY varchar(255),
    CREATED_DATETIME timestamp,
    LAST_UPDATED_BY varchar(255),
    LAST_UPDATED_DATETIME timestamp,
    PRODUCT_CODE varchar(255),
    QUANTITY integer,
    REQUEST_DATE date,
    REQUEST_NO varchar(12),
    STATUS varchar(255),
    ALLOCATE_TO integer,
    SOURCE_LOCATION integer,
    primary key (id)
);

alter table CRM_STOCK_REQUEST 
    add constraint FK_bdydqssie75jlt60e4qocap05 
    foreign key (ALLOCATE_TO) 
    references STORE;

alter table CRM_STOCK_REQUEST 
    add constraint FK_so4mj8g405igxlcsri2y6722n 
    foreign key (SOURCE_LOCATION) 
    references STORE;