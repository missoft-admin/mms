package com.transretail.crm.template.service.streaming.excel.handler.sheet.optional;

import static org.apache.commons.lang3.StringUtils.isBlank;

import java.util.Map;

import org.apache.commons.lang3.BooleanUtils;
import org.springframework.context.i18n.LocaleContextHolder;

import com.google.common.collect.Maps;
import com.mysema.query.jpa.impl.JPAUpdateClause;
import com.transretail.crm.core.entity.MemberModel;
import com.transretail.crm.core.entity.QMemberModel;
import com.transretail.crm.template.service.streaming.excel.handler.HandlerException;
import com.transretail.crm.template.service.streaming.excel.handler.cell.AccountIdCellHandler;
import com.transretail.crm.template.service.streaming.excel.handler.cell.CellHandler;
import com.transretail.crm.template.service.streaming.excel.handler.cell.CommunicationCellHandler;
import com.transretail.crm.template.service.streaming.excel.handler.cell.CorrespondenceCellHandler;
import com.transretail.crm.template.service.streaming.excel.handler.cell.GroserindoInfoCellHandler;
import com.transretail.crm.template.service.streaming.excel.handler.sheet.AbstractSheetHandler;

/**
 * @author Allan G. Ramirez (agramirez@exist.com)
 */
public class ProfMarketingHandler extends AbstractSheetHandler {
    private static final String HEADER_ACCOUNTID = "ACCOUNTNUMBER";
    private static final String HEADER_CORRESPONDENCE = "CORRESPONDENCE";
    private static final String HEADER_COMMUNICATION = "COMMUNICATION";
    private static final String HEADER_GROSERINDOINFO = "GROSERINDOINFO";

    private Map<String, CellHandler> cellHandlerClassMap = Maps.newHashMap();
    private CellHandler[] cellHandlers = null;

    public ProfMarketingHandler() {
        cellHandlerClassMap.put(HEADER_ACCOUNTID, new AccountIdCellHandler());
        cellHandlerClassMap.put(HEADER_CORRESPONDENCE, new CorrespondenceCellHandler(this));
        cellHandlerClassMap.put(HEADER_COMMUNICATION, new CommunicationCellHandler(this, HEADER_COMMUNICATION));
        cellHandlerClassMap.put(HEADER_GROSERINDOINFO, new GroserindoInfoCellHandler(this));
        cellHandlers = new CellHandler[cellHandlerClassMap.size()];
    }

    @Override
    protected void processHeaderCell(int i, String headerName) {
        CellHandler cellHandler = cellHandlerClassMap.get(headerName);
        if (cellHandler == null) {
            throw new HandlerException(getMessageSource()
                .getMessage("member.template.messages.excel.nohandler", new String[]{sheetName, headerName},
                    LocaleContextHolder.getLocale()));
        }
        cellHandlers[i] = cellHandler;
    }

    @Override
    protected CellHandler getCellHandler(int index) {
        return cellHandlers[index];
    }

    @Override
    protected void saveOrUpdateDto(MemberModel dto) throws HandlerException {
        String accountId = dto.getAccountId();
        if (isBlank(accountId)) {
            throw new HandlerException(getMessageSource().getMessage("member.template.messages.excel.columns.required",
                new String[]{sheetName, String.valueOf(currentRowNum), HEADER_ACCOUNTID},
                LocaleContextHolder.getLocale()));
        } else if (accountId.length() != 13 || accountId.charAt(2) != '0' || accountId.charAt(3) != '2') {
            throw new HandlerException(getMessageSource().getMessage("member.template.messages.excel.accountid.invalid",
                new String[]{sheetName, accountId, "02"},
                LocaleContextHolder.getLocale()));
        }
        if (!accountIdExists(dto.getAccountId())) {
            throw new HandlerException(getMessageSource().getMessage("member.template.messages.excel.accountid.notfound",
                new String[]{sheetName, String.valueOf(currentRowNum), dto.getAccountId()},
                LocaleContextHolder.getLocale()));
        }

        QMemberModel qMemberModel = QMemberModel.memberModel;
        new JPAUpdateClause(em, qMemberModel).where(qMemberModel.accountId.eq(dto.getAccountId()))
            .set(qMemberModel.marketingDetails.correspondenceAddress, dto.getMarketingDetails().getCorrespondenceAddress())
            .set(qMemberModel.marketingDetails.informedAbout, dto.getMarketingDetails().getInformedAbout())
            .set(qMemberModel.channel.acceptSMS, BooleanUtils.toBoolean(dto.getChannel().getAcceptSMS()))
            .set(qMemberModel.channel.acceptPhone, BooleanUtils.toBoolean(dto.getChannel().getAcceptPhone()))
            .set(qMemberModel.channel.acceptEmail, BooleanUtils.toBoolean(dto.getChannel().getAcceptEmail()))
            .execute();

        if (currentRowNum % batchSize == 0) {
            cleanup();
        }
    }

    @Override
    public int getExpectedNumberOfColumns() {
        return cellHandlers.length;
    }
}
