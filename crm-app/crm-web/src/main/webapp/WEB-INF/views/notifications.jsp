<%@ include file="common/taglibs.jsp" %>


<li class="dropdown link-icon">
  <span class="badge badge-important" id="notificationCountBadge"></span>
  
  <a href="#" class="dropdown-toggle icon-menu-notifications" data-toggle="dropdown" data-hover="dropdown" data-delay="200" data-close-others="true" >
   
   <spring:message code="label_notification" /> <b class="caret"></b>
  </a>
  <input type="hidden" id="notificationCount"/>
  <ul id="notifications" class="dropdown-menu row " style="height: auto; max-height: 550px; overflow-y: scroll;">
    <!--<li class="nav-header">Notifications</li>-->
  	<li id="noNotifications"><a href="#"><spring:message code="label_no_notification" /></a></li>
  </ul>
</li>

<style type="text/css">
    dd {
        margin-left: 0px;
    }
    dt {
        font-weight: lighter;
    }
    ul#notifications.nav li.dropdown a {
        margin-bottom: 0px;
    }
</style>