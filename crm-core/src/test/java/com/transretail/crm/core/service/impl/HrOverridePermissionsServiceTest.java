package com.transretail.crm.core.service.impl;

import java.util.List;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Configurable;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import com.transretail.crm.core.entity.ApprovalMatrixModel;
import com.transretail.crm.core.entity.UserModel;
import com.transretail.crm.core.entity.enums.ModelType;
import com.transretail.crm.core.repo.ApprovalMatrixRepo;
import com.transretail.crm.core.repo.UserRepo;
import com.transretail.crm.core.security.model.impl.CustomSecurityUserDetailsImpl;
import com.transretail.crm.core.service.HrOverridePermissionsService;

@Configurable
@Transactional
@ContextConfiguration(locations = "classpath*:/META-INF/spring/applicationContext*.xml")
@RunWith(SpringJUnit4ClassRunner.class)
public class HrOverridePermissionsServiceTest {
	

	@Autowired
	ApprovalMatrixRepo approvalMatrixRepo;
	
	@Autowired
	UserRepo userRepo;
	
	@Autowired
	HrOverridePermissionsService overridePermissionsService;
	
	private Long approverId;
	
	@Before
	public void setUp() {
		setupApprovers();
	}
	
	private void setupApprovers() {
		ApprovalMatrixModel model = new ApprovalMatrixModel();
		UserModel user = new UserModel();
		user.setUsername("username1");
		user.setPassword("password1");
		user.setEnabled(true);
		user.setStoreCode( "store1" );
		userRepo.save(user);
		model.setModelType(ModelType.PURCHASETXN);
		model.setUser(user);
		approverId = approvalMatrixRepo.save(model).getId();
		
		user = new UserModel();
		user.setUsername("username2");
		user.setPassword("password2");
		user.setEnabled(true);
		user.setStoreCode( "store1" );
		userRepo.save(user);
		model = new ApprovalMatrixModel();
		model.setModelType(ModelType.PURCHASETXN);
		model.setUser(user);
		approvalMatrixRepo.save(model);
	}
	
	@Test
	public void testFindApprovers() {
		List<ApprovalMatrixModel> theApprovers = overridePermissionsService.findApprovers();
		Assert.assertNotNull(theApprovers);
		Assert.assertEquals(2, theApprovers.size());
	}
	
	
	@Test
	public void testDeleteApprover() {
		Assert.assertEquals(2, approvalMatrixRepo.count());
		overridePermissionsService.deleteApprover(approverId);
		Assert.assertEquals(1, approvalMatrixRepo.count());
	}
	
	@Test
	public void testIsApprover() {
		CustomSecurityUserDetailsImpl currentUser = new CustomSecurityUserDetailsImpl();
		currentUser.setUsername("test");
		
		Assert.assertFalse(overridePermissionsService.isApprover(currentUser));
		
		UserModel user = new UserModel();
		user.setUsername("test");
		user.setPassword("test");
		user.setEnabled(true);
		user.setStoreCode( "store1" );
		userRepo.save(user);
		ApprovalMatrixModel model = new ApprovalMatrixModel();
		model.setModelType(ModelType.PURCHASETXN);
		model.setUser(user);
		approvalMatrixRepo.save(model);
		
		Assert.assertTrue(overridePermissionsService.isApprover(currentUser));
		
	}
	
}
