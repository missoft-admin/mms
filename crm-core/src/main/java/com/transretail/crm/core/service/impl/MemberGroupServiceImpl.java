package com.transretail.crm.core.service.impl;


import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.BooleanUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.google.common.collect.Lists;
import com.mysema.query.types.expr.BooleanExpression;
import com.transretail.crm.common.service.dto.request.PageSortDto;
import com.transretail.crm.common.service.dto.request.PagingParam;
import com.transretail.crm.common.service.dto.response.ResultList;
import com.transretail.crm.common.service.util.SpringDataPagingUtil;
import com.transretail.crm.core.dto.MemberDto;
import com.transretail.crm.core.dto.MemberGroupDto;
import com.transretail.crm.core.dto.MemberGroupFieldDto;
import com.transretail.crm.core.dto.MemberGrpSearchDto;
import com.transretail.crm.core.entity.MemberGroup;
import com.transretail.crm.core.entity.MemberGroupField;
import com.transretail.crm.core.entity.MemberGroupFieldValue;
import com.transretail.crm.core.entity.MemberModel;
import com.transretail.crm.core.entity.PromotionMemberGroup;
import com.transretail.crm.core.entity.QMemberModel;
import com.transretail.crm.core.entity.QPromotionMemberGroup;
import com.transretail.crm.core.repo.MemberGroupFieldRepo;
import com.transretail.crm.core.repo.MemberGroupFieldValueRepo;
import com.transretail.crm.core.repo.MemberGroupRepo;
import com.transretail.crm.core.repo.MemberRepo;
import com.transretail.crm.core.repo.PromotionMemberGroupRepo;
import com.transretail.crm.core.service.CodePropertiesService;
import com.transretail.crm.core.service.LookupService;
import com.transretail.crm.core.service.MemberGroupService;


@Service
@Transactional
public class MemberGroupServiceImpl implements MemberGroupService {

	@Autowired
	MemberGroupRepo memberGroupRepo;
	@Autowired
	MemberGroupFieldRepo groupFieldRepo;
	@Autowired
	MemberGroupFieldValueRepo fieldValueRepo;
	@Autowired
	MemberRepo memberRepo;
	@Autowired
	PromotionMemberGroupRepo promoMemberGrpRepo;
	@Autowired
	CodePropertiesService codePropertiesService;
	@Autowired
	LookupService lookupService;

    @PersistenceContext
    private EntityManager em;



    @Override
	public ResultList<MemberGroup> listMemberGroups(PageSortDto dto) {
    	Pageable pageable = SpringDataPagingUtil.INSTANCE.createPageable(dto.getPagination());
    	if ( pageable == null && dto.getPagination() != null &&  dto.getPagination().getOrder() != null ) {
    		return new ResultList<MemberGroup>(memberGroupRepo.findAll( 
    				new Sort(SpringDataPagingUtil.INSTANCE.createSortOrder(dto.getPagination().getOrder(), null)) ) );
    	}
    	else {
            return new ResultList<MemberGroup>(memberGroupRepo.findAll(pageable));
    	}
	}

	@Override
	public ResultList<MemberGroupDto> search( MemberGrpSearchDto searchDto ) {
		Pageable pageable = SpringDataPagingUtil.INSTANCE.createPageable( searchDto.getPagination() );
        BooleanExpression filter = searchDto.createSearchExpression();
        Page<MemberGroup> page = ( filter != null )? memberGroupRepo.findAll( filter, pageable ) : memberGroupRepo.findAll( pageable );
        return new ResultList<MemberGroupDto>( toDtoList( page.getContent() ), page.getTotalElements(), page.getNumber(), page.getSize() );
	}

    private List<MemberGroupDto> toDtoList( List<MemberGroup> models ) {
        List<MemberGroupDto> dtos = Lists.newArrayList();

        Iterator<PromotionMemberGroup> it = promoMemberGrpRepo.findAll( 
        		QPromotionMemberGroup.promotionMemberGroup.promotion.status.code.equalsIgnoreCase( 
        				codePropertiesService.getDetailStatusActive() ) ).iterator();
        List<Long> activeIds = Lists.newArrayList();
        while( it.hasNext() ) {
        	PromotionMemberGroup pmg = it.next();
        	activeIds.add( pmg.getMemberGroup().getId() );
        }

        for ( MemberGroup model : models ) {
        	MemberGroupDto dto = new MemberGroupDto( model );
        	if ( CollectionUtils.isNotEmpty( activeIds ) ) {
        		if ( activeIds.contains( dto.getId().longValue() ) ) {
        			dto.setIsUsed( true );
        		}
        	}
        	dtos.add( dto );
        }
        return dtos;
    }

	@Override
	public MemberGroup getMemberGroup(String name) {
		return memberGroupRepo.findByName(name);
	}

	@Override
	public MemberGroup getMemberGroup(Long id) {
		return memberGroupRepo.findOne(id);
	}
	
	@Override
	public MemberGroupDto getMemberGroupDto(Long id) {
		MemberGroup memberGroup = getMemberGroup(id);
		if(memberGroup != null) {
			MemberGroupDto group = new MemberGroupDto(memberGroup);
			List<MemberGroupFieldDto> fields = new ArrayList<MemberGroupFieldDto>();
			for(MemberGroupField field : groupFieldRepo.findByMemberGroup(memberGroup)) {
				MemberGroupFieldDto fieldDto = new MemberGroupFieldDto(field);
				fieldDto.setFieldValues(getMemberGroupFieldValues(field));
				fields.add(fieldDto);
			}
			group.setMemberFields(fields);
			return group;
		}
		return null;
	}

	@Override
	public MemberGroup saveMemberGroup(MemberGroup group) {
		if(BooleanUtils.isTrue(group.getRfs().getZeroFrequency())) {
			group.getRfs().setFrequencyFrom(0L);
			group.getRfs().setFrequencyTo(0L);
		}
		return memberGroupRepo.save(group);
	}
	
	@Override
	public MemberGroup saveMemberGroup(MemberGroupDto group) {
		MemberGroup memberGroup = new MemberGroup();
		BeanUtils.copyProperties(group, memberGroup);
		return saveMemberGroup(memberGroup);
	}
	
	@Override
	public List<MemberGroupFieldDto> getMemberGroupFieldsDto(MemberGroup group) {
		List<MemberGroupFieldDto> fields = new ArrayList<MemberGroupFieldDto>();
		for(MemberGroupField field : groupFieldRepo.findByMemberGroup(group)) {
			fields.add(new MemberGroupFieldDto(field));
		}
		return fields;
	}
	
	@Override
	public List<MemberGroupField> getMemberGroupFields(MemberGroup group) {
		return groupFieldRepo.findByMemberGroup(group);
	}

	@Override
	public List<MemberGroupField> saveMemberGroupFields(List<MemberGroupField> groupFields) {
		return groupFieldRepo.save(groupFields);
	}

	@Override
	public MemberGroupField saveMemberGroupField(MemberGroupField groupField) {
		return groupFieldRepo.save(groupField);
	}
	
	@Override
	public MemberGroupField saveMemberGroupField(MemberGroupFieldDto groupField) {
		MemberGroupField field = new MemberGroupField();
		BeanUtils.copyProperties(groupField, field);
		return saveMemberGroupField(field);
	}
	
	@Override
	public List<MemberGroupFieldValue> getMemberGroupFieldValues(MemberGroupField groupField) {
		return fieldValueRepo.findByMemberGroupField(groupField);
	}

	@Override
	public List<MemberGroupFieldValue> saveMemberGroupFieldValues(List<MemberGroupFieldValue> fieldValues) {
		return fieldValueRepo.save(fieldValues);
	}

	@Override
	public MemberGroupFieldValue saveMemberGroupFieldValue(MemberGroupFieldValue fieldValue) {
		return fieldValueRepo.save(fieldValue);
	}

	@Override
	public List<MemberGroup> getAllMemberGroups() {
		return memberGroupRepo.findAll(getSort());
	}
	
	private Sort getSort() {
		List<Sort.Order> orders = new ArrayList<Sort.Order>();
		orders.add(new Sort.Order(Sort.Direction.ASC,"name").ignoreCase());
		return new Sort(orders);
	}

	@Override
	public void delete(MemberGroup group) {
		for(MemberGroupField field : getMemberGroupFields(group)) {
			delete(field);
		}
		memberGroupRepo.delete(group);
	}

	@Override	
	public void delete(MemberGroupField field) {
		delete(getMemberGroupFieldValues(field));
		groupFieldRepo.delete(field);
	}

	@Override
	public void delete(List<MemberGroupFieldValue> values) {
		fieldValueRepo.delete(values);
	}

	@Override
	public ResultList<MemberDto> getQualifiedMembers( MemberGroup group, PageSortDto sortDto ) {
        return getQualifiedMembers( group, sortDto, null );
	}

	@Override
	public ResultList<MemberDto> getQualifiedMembers( MemberGroup group, PageSortDto sortDto, BooleanExpression filter ) {
		QMemberModel qMember = QMemberModel.memberModel;
		Pageable pageable = SpringDataPagingUtil.INSTANCE.createPageable(sortDto.getPagination());
		List<Long> members = new ArrayList<Long>();
		for(String str: group.getMemberList()) {
			members.add(Long.parseLong(str));
		}

        filter = BooleanExpression.allOf( qMember.id.in( members ), null != filter ? filter : null );
        Page<MemberModel> page = filter != null ? memberRepo.findAll(filter, pageable) : memberRepo.findAll(pageable);
        return new ResultList<MemberDto>(convertMemberListToDtoList(page.getContent()), page.getTotalElements(), page.hasPreviousPage(),
                page.hasNextPage());
	}
	
	@Override
    public Long getQualifiedMembersCount( MemberGroup group, BooleanExpression filter ) {
        QMemberModel qMember = QMemberModel.memberModel;
        List<Long> members = new ArrayList<Long>();
        for(String str: group.getMemberList()) {
            members.add(Long.parseLong(str));
        }

        filter = BooleanExpression.allOf( qMember.id.in( members ), null != filter ? filter : null );
        List<MemberModel> membersModelList = Lists.newArrayList(memberRepo.findAll(filter));
        return Long.valueOf(membersModelList.size());
    }
	
	@Override
    public List<MemberDto> getQualifiedMembersForPrint(MemberGroup group, Integer segment) {
	    PageSortDto sortDto = new PageSortDto();
	    PagingParam pagingParam = new PagingParam();
	    Map<String, Boolean> order = new HashMap<String, Boolean>();
	    order.put("accountId", true);
	    pagingParam.setOrder(order);
	    pagingParam.setPageSize(1000);
	    pagingParam.setPageNo(segment - 1);
	    sortDto.setPagination(pagingParam);
	    
	    List<MemberDto> members = getQualifiedMembersForPrint(getMemberGroupFieldsDto(group), group, sortDto);
	    
        return members;
    }

	@Override
	public ResultList<MemberDto> getQualifiedMembers( List<MemberGroupFieldDto> fields, MemberGroup group, PageSortDto sortDto ) {
		return memberGroupRepo.getQualifiedMembers(fields, group, sortDto);
	}
	
	@Override
    public List<MemberDto> getQualifiedMembersForPrint( List<MemberGroupFieldDto> fields, MemberGroup group, PageSortDto sortDto) {
        return memberGroupRepo.getQualifiedMembersForPrint(fields, group, sortDto);
    }
	
	@Override
    public Long getQualifiedMembersCount( List<MemberGroupFieldDto> fields, MemberGroup group) {
	    return memberGroupRepo.getQualifiedMembersCount(fields, group);
    }
	
	@Override
	public List<Long> getQualifiedMembers( List<MemberGroupFieldDto> fields, MemberGroup group) {
		if(StringUtils.isNotBlank(group.getMembers())) {
			return getQualifiedMembers(group);
		} else
			return memberGroupRepo.getQualifiedMembers(fields, group);
	}

    public boolean isQualifiedMember(MemberGroup group, String accountId) {
        return memberGroupRepo.isQualifiedMember(getMemberGroupFieldsDto(group), group, accountId);
    }

	@Override
	public List<Long> getQualifiedMembers(MemberGroup group) {
		if(StringUtils.isNotBlank(group.getMembers())) {
			Set<Long> members = new HashSet<Long>();
			for(String str: group.getMemberList()) {
				members.add(Long.parseLong(str));
			}
			return Lists.newArrayList(members);
		} else 
			return getQualifiedMembers(getMemberGroupFieldsDto(group), group);
	}
	
	@Override
	public List<MemberGroupDto> getQualifiedCardTypeGroups(MemberModel member) {
		List<MemberGroupDto> qualifiedGroups = Lists.newArrayList();
		
		for(MemberGroup cardTypeGroup : memberGroupRepo.getCardTypeMemberGroups()) {
			if(getQualifiedMembers(cardTypeGroup).contains(member.getId())) {
				qualifiedGroups.add(new MemberGroupDto(cardTypeGroup));
			}
		}
		return qualifiedGroups;
	}
	
	private List<MemberDto> convertMemberListToDtoList(Iterable<MemberModel> iterable) {
    	List<MemberDto> dtoList = new ArrayList<MemberDto>();
    	for(MemberModel member : iterable) {
    		MemberDto dto = new MemberDto(member);
    		dtoList.add(dto);
    	}
    	return dtoList;
    }
}
