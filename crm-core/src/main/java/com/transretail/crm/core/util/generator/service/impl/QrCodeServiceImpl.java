package com.transretail.crm.core.util.generator.service.impl;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

import javax.imageio.ImageIO;

import net.glxn.qrgen.QRCode;
import net.glxn.qrgen.image.ImageType;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.google.zxing.BinaryBitmap;
import com.google.zxing.MultiFormatReader;
import com.google.zxing.NotFoundException;
import com.google.zxing.Result;
import com.google.zxing.client.j2se.BufferedImageLuminanceSource;
import com.google.zxing.common.HybridBinarizer;
import com.transretail.crm.core.util.generator.QrCodeService;

@Service("qrCodeService")
@Transactional
public class QrCodeServiceImpl implements QrCodeService {

	@Override
	public File generateQrCode( String code ) {
		return QRCode.from( code ).to( ImageType.PNG ).withSize( 300, 300 ).file();

		// Map<EncodeHintType, ErrorCorrectionLevel> hint = Maps.newHashMap();
		// hint.put( EncodeHintType.ERROR_CORRECTION, ErrorCorrectionLevel.L );
		// BitMatrix matrix = new MultiFormatWriter().encode(
		// new String( code.getBytes( "UTF-8" /*"ISO-8859-1"*/ ), "UTF-8" ),
		// BarcodeFormat.QR_CODE, 200, 200 );
		// MatrixToImageWriter.writeToStream( matrix, "png", baos );
		// return MatrixToImageWriter.toBufferedImage( matrix );
	}

	@Override
	public String decryptQrCode( File file ) throws FileNotFoundException, IOException, NotFoundException {
		Result result = null;
		BinaryBitmap binaryBitmap = null;
		binaryBitmap = new BinaryBitmap(
				new HybridBinarizer(
						new BufferedImageLuminanceSource(
								ImageIO.read( new FileInputStream( file ) ) ) ) );
		result = new MultiFormatReader().decode( binaryBitmap );
		return result.getText();
	}

}
