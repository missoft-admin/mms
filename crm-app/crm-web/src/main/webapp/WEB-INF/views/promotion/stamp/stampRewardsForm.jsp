<%@ include file="../../common/taglibs.jsp" %>
<c:set var="rewardSelType" value="stampReward" scope="request" />
          <div class="control-group-01 rewardContainer">
          <fieldset>
            <legend class="block-stack-title"><spring:message code="stamp_promo_reward" /></legend>

            <div id="" class="">
              <div id="content_rewardTypes" class="">
                <a href="#" class="btn btn-small link_promotionReward" id=""
                  data-list-url="<c:url value="/promotion/reward/list/" />" data-type="stampReward" data-url="<c:url value="/promotion/reward/create/"/>">
                  <spring:message code="stamp_promo_add_reward"/>
                </a>
              </div>
              <div id="list_promotionRewards"><c:if test="${not empty promotion.promotionRewards and null != promotion.rewardType}"><jsp:include page="../reward/rewardList.jsp" /></c:if></div>

            </div>
          </fieldset>
          </div>
