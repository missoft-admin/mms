package com.transretail.crm.loyalty.repo;

import org.springframework.stereotype.Repository;

import com.transretail.crm.common.repo.CrmQueryDslPredicateExecutor;
import com.transretail.crm.loyalty.entity.ManufactureOrder;

@Repository
public interface ManufactureOrderRepo extends CrmQueryDslPredicateExecutor<ManufactureOrder, Long> {

	ManufactureOrder findByMoNo(String moNo);
	
}
