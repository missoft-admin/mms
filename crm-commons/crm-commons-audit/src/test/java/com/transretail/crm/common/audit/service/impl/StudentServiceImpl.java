package com.transretail.crm.common.audit.service.impl;

import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import com.transretail.crm.common.audit.annotation.AuditModule;
import com.transretail.crm.common.audit.annotation.AuditParam;
import com.transretail.crm.common.audit.annotation.Audited;
import com.transretail.crm.common.audit.service.Name;
import com.transretail.crm.common.audit.service.Student;
import com.transretail.crm.common.audit.service.StudentService;

/**
 * @author Allan G. Ramirez (agramirez@exist.com)
 */
@Service("studentService")
@AuditModule(moduleName = "Student")
public class StudentServiceImpl implements StudentService {
    @Audited(name = "Save", description = "Saving $student")
    public void saveStudent(@AuditParam("student") Student student) {
        if (!validName(student.getName())) {
            throw new IllegalArgumentException("Firstname, MiddleName and Lastname are required.");
        }
    }

    public void updateStudent(Long id, Name name) {
        //...
    }

    private boolean validName(Name name) {
        return name != null && StringUtils.hasText(name.getFirstName()) && StringUtils.hasText(name.getMiddleName()) && StringUtils
            .hasText(name.getLastName());
    }
}
