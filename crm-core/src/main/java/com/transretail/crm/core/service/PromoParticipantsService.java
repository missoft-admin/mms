package com.transretail.crm.core.service;

import com.transretail.crm.common.reporting.jasper.JRProcessor;




public interface  PromoParticipantsService {

	JRProcessor createJrProcessor(Long promotionId);

	boolean isExistParticipants(Long promoId);
	
	
}
