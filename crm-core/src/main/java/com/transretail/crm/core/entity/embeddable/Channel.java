package com.transretail.crm.core.entity.embeddable;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.validation.constraints.Pattern;

import org.apache.commons.lang3.BooleanUtils;
import org.hibernate.annotations.Type;

@Embeddable
public class Channel {
//	@Column(name="EMAIL")
//	@Pattern(regexp = "^$|^([0-9a-zA-Z]([-.\\w]*[0-9a-zA-Z])*@([0-9a-zA-Z][-\\w]*[0-9a-zA-Z]\\.)+[a-zA-Z]{2,9})")
//	private String email;
	
	@Column(name="SMS", length = 1)
    @Type(type = "yes_no")
	private String sms;
	
	@Column(name="ACCEPT_EMAIL", length = 1)
    @Type(type = "yes_no")
	private Boolean acceptEmail = Boolean.FALSE;
	
	@Column(name="ACCEPT_MAIL", length = 1)
    @Type(type = "yes_no")
	private Boolean acceptMail = Boolean.FALSE;
	
	@Column(name="ACCEPT_SMS", length = 1)
    @Type(type = "yes_no")
	private Boolean acceptSMS = Boolean.FALSE;
	
	@Column(name="ACCEPT_PHONE", length = 1)
    @Type(type = "yes_no")
	private Boolean acceptPhone = Boolean.FALSE;

//	public String getEmail() {
//		return email;
//	}
//
//	public void setEmail(String email) {
//		this.email = email;
//	}

	public String getSms() {
		return sms;
	}

	public void setSms(String sms) {
		this.sms = sms;
	}

	public Boolean getAcceptEmail() {
		return acceptEmail;
	}

	public void setAcceptEmail(Boolean acceptEmail) {
		this.acceptEmail = BooleanUtils.toBoolean(acceptEmail);
	}

	public Boolean getAcceptMail() {
		return acceptMail;
	}

	public void setAcceptMail(Boolean acceptMail) {
		this.acceptMail = BooleanUtils.toBoolean(acceptMail);
	}

	public Boolean getAcceptSMS() {
		return acceptSMS;
	}

	public void setAcceptSMS(Boolean acceptSMS) {
		this.acceptSMS = BooleanUtils.toBoolean(acceptSMS);
	}

	public Boolean getAcceptPhone() {
		return acceptPhone;
	}

	public void setAcceptPhone(Boolean acceptPhone) {
		this.acceptPhone = BooleanUtils.toBoolean(acceptPhone);
	}
	
	
}
