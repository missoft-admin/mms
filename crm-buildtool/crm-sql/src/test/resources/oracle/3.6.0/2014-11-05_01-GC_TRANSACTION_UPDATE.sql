
alter table CRM_GC_TRANSACTION 
    drop constraint UK_PNVSRLBNU551LIAWP7L0629VV;

alter table CRM_GC_TRANSACTION 
    drop constraint FK_q1a3ub5qqw4h97fg1itqxeuap;

alter table CRM_GC_TRANSACTION
  add REF_TXN_ID number(19,0);

update CRM_GC_TRANSACTION t1 set REF_TXN_ID = (select id from CRM_GC_TRANSACTION t2 where t2.txn_no = t1.ref_txn_no);

alter table CRM_GC_TRANSACTION drop column REF_TXN_NO;

alter table CRM_GC_TRANSACTION 
        add constraint UK_ery2wsrnogetfnkn5kuvu1jf8 unique (TXN_NO, TXN_TYPE);

alter table CRM_GC_TRANSACTION 
    add constraint FK_ph97p142uyaul5qe8unhajg50 
    foreign key (REF_TXN_ID) 
    references CRM_GC_TRANSACTION;