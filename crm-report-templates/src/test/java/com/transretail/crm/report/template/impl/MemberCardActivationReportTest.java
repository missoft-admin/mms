package com.transretail.crm.report.template.impl;

import static org.junit.Assert.assertEquals;

import java.io.File;
import java.io.InputStream;
import java.util.Date;
import java.util.Locale;
import java.util.Map;
import java.util.Scanner;

import javax.annotation.Resource;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.joda.time.LocalDate;
import org.joda.time.format.DateTimeFormat;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.context.support.MessageSourceResourceBundle;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.support.TransactionCallback;
import org.springframework.transaction.support.TransactionTemplate;

import com.google.common.collect.Maps;
import com.transretail.crm.common.reporting.jasper.JRProcessor;
import com.transretail.crm.common.reporting.service.ReportService;
import com.transretail.crm.core.entity.MemberModel;
import com.transretail.crm.core.entity.embeddable.CustomerProfile;
import com.transretail.crm.core.entity.lookup.LookupDetail;
import com.transretail.crm.core.repo.LookupDetailRepo;
import com.transretail.crm.core.repo.LookupHeaderRepo;
import com.transretail.crm.core.repo.MemberRepo;
import com.transretail.crm.core.service.CodePropertiesService;
import com.transretail.crm.loyalty.entity.LoyaltyCardInventory;
import com.transretail.crm.loyalty.entity.ManufactureOrderTier;
import com.transretail.crm.loyalty.repo.LoyaltyCardInventoryRepo;
import com.transretail.crm.loyalty.repo.ManufactureOrderTierRepo;
import com.transretail.crm.report.template.ReportTemplate;

import net.sf.jasperreports.engine.JRParameter;

/**
 * @author Allan G. Ramirez (agramirez@exist.com)
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath*:/META-INF/spring/applicationContext*.xml",
    "/META-INF/spring/applicationContext-test.xml"})
public class MemberCardActivationReportTest {
    private static final Logger _LOG = LoggerFactory.getLogger(MemberCardActivationReportTest.class);
    @PersistenceContext
    private EntityManager em;
    @Autowired
    private PlatformTransactionManager transactionManager;
    @Resource(name = "cardActivationReport")
    private ReportTemplate cardActivationReport;
    @Autowired
    private ReportService reportService;
    @Autowired
    private MessageSource messageSource;
    @Autowired
    private LookupHeaderRepo lookupHeaderRepo;
    @Autowired
    private LookupDetailRepo lookupDetailRepo;
    @Autowired
    private MemberRepo memberRepo;
    @Autowired
    private ManufactureOrderTierRepo manufactureOrderTierRepo;
    @Autowired
    private LoyaltyCardInventoryRepo loyaltyCardInventoryRepo;
    @Autowired
    private CodePropertiesService codePropertiesService;

    @Before
    public void onSetup() {
        setUpLookupDetails();
    }

    @After
    public void tearDown() {
        loyaltyCardInventoryRepo.deleteAll();
        manufactureOrderTierRepo.deleteAll();
        memberRepo.deleteAll();
        lookupDetailRepo.deleteAll();
        lookupHeaderRepo.deleteAll();
    }

    @Test
    public void getNameAndGetFiltersTest() {
        assertEquals(MemberCardActivationReport.TEMPLATE_NAME, cardActivationReport.getName());
        assertEquals(3, cardActivationReport.getFilters().size());
    }

    @Test
    public void createJrProcessorTest() throws Exception {
        LocalDate activationDate = DateTimeFormat.forPattern("dd-MM-yyyy").parseLocalDate("08-04-2013");
        setupData(activationDate);
        Map<String, String> map = Maps.newHashMap();
        map.put(MemberCardActivationReport.FILTER_ACTIVATIONDATE_FROM, "13-03-2013");
        map.put(MemberCardActivationReport.FILTER_ACTIVATIONDATE_TO, "21-11-2013");
        map.put(MemberCardActivationReport.FILTER_STATUS, codePropertiesService.getDetailLoyaltyStatusActive());
        JRProcessor jrProcessor = cardActivationReport.createJrProcessor(map);

        File testClassesDir = FileUtils.toFile(Thread.currentThread().getContextClassLoader().getResource(""));
        File output = new File(testClassesDir, "test-card-activation-report-output.pdf");
        _LOG.debug("Output location {}", output.getAbsolutePath());
        output.delete();
        Locale locale = LocaleContextHolder.getLocale();
        jrProcessor.addParameter(JRParameter.REPORT_LOCALE, locale);
        jrProcessor.addParameter(JRParameter.REPORT_RESOURCE_BUNDLE, new MessageSourceResourceBundle(messageSource, locale));
        reportService.exportPdfReport(jrProcessor, output);

        double bytes = output.length();
        double kilobytes = (bytes / 1024);
        _LOG.debug("Kilobytes: {}", kilobytes);
    }

    @Transactional
    private void setupData(LocalDate activationDate) {
        String preferredStore1 = "TITA GWAPA";
        String preferredStore2 = "TITO POGI";

        ManufactureOrderTier silverTier = new ManufactureOrderTier();
        silverTier.setName(new LookupDetail(codePropertiesService.getDetailMemberTierSilver()));
        manufactureOrderTierRepo.save(silverTier);
        ManufactureOrderTier goldTier = new ManufactureOrderTier();
        goldTier.setName(new LookupDetail(codePropertiesService.getDetailMemberTierGold()));
        manufactureOrderTierRepo.save(goldTier);

        for (int i = 0; i < 10; i++) {
            MemberModel model = new MemberModel();
            model.setUsername("username" + i);
            String accountId = "accountId" + i;
            model.setAccountId(accountId);
            model.setContact("contact" + i);
            model.setPin("pin" + i);
            model.setEnabled(true);
            model.setRegisteredStore(preferredStore1);
            model.setStoreName("TITA GWAPA Store");
            if (i % 2 == 0) {
                model.setMemberType(new LookupDetail(codePropertiesService.getDetailMemberTypeIndividual()));
            } else {
                model.setMemberType(new LookupDetail(codePropertiesService.getDetailMemberTypeProfessional()));
            }
            CustomerProfile customerProfile = new CustomerProfile();
            customerProfile.setBirthdate(new Date());
            model.setCustomerProfile(customerProfile);
            memberRepo.save(model);

            LoyaltyCardInventory cardInventory = new LoyaltyCardInventory();
            cardInventory.setStatus(new LookupDetail(codePropertiesService.getDetailLoyaltyStatusActive()));
            cardInventory.setBarcode(accountId);
            cardInventory.setActivationDate(activationDate);
            if (i < 5) {
                cardInventory.setProduct(silverTier);
            } else {
                cardInventory.setProduct(goldTier);
            }
            loyaltyCardInventoryRepo.save(cardInventory);
        }

        for (int i = 10; i < 20; i++) {
            MemberModel model = new MemberModel();
            model.setUsername("username" + i);
            String accountId = "accountId" + i;
            model.setAccountId(accountId);
            model.setContact("contact" + i);
            model.setPin("pin" + i);
            model.setEnabled(true);
            model.setRegisteredStore(preferredStore2);
            model.setStoreName("TITO POGI Store");
            if (i % 2 == 0) {
                model.setMemberType(new LookupDetail(codePropertiesService.getDetailMemberTypeIndividual()));
            } else {
                model.setMemberType(new LookupDetail(codePropertiesService.getDetailMemberTypeProfessional()));
            }
            CustomerProfile customerProfile = new CustomerProfile();
            customerProfile.setBirthdate(new Date());
            model.setCustomerProfile(customerProfile);
            memberRepo.save(model);

            LoyaltyCardInventory cardInventory = new LoyaltyCardInventory();
            cardInventory.setStatus(new LookupDetail(codePropertiesService.getDetailLoyaltyStatusActive()));
            cardInventory.setBarcode(accountId);
            cardInventory.setActivationDate(activationDate);
            if (i < 5) {
                cardInventory.setProduct(silverTier);
            } else {
                cardInventory.setProduct(goldTier);
            }
            loyaltyCardInventoryRepo.save(cardInventory);
        }
    }

    @Transactional
    private void setUpLookupDetails() {
        InputStream is1 = Thread.currentThread().getContextClassLoader().getResourceAsStream("A_LOOKUPHEADERS.sql");
        InputStream is2 = Thread.currentThread().getContextClassLoader().getResourceAsStream("B_LOOKUPDETAILS.sql");
        try {
            final Scanner in1 = new Scanner(is1);
            final Scanner in2 = new Scanner(is2);
            TransactionTemplate transactionTemplate = new TransactionTemplate(transactionManager);
            transactionTemplate.execute(new TransactionCallback() {
                public Object doInTransaction(TransactionStatus status) {
                    while (in1.hasNext()) {
                        String script = in1.nextLine();
                        if (StringUtils.isNotBlank(script)) {
                            em.createNativeQuery(script).executeUpdate();
                        }
                    }
                    while (in2.hasNext()) {
                        String script = in2.nextLine();
                        if (StringUtils.isNotBlank(script)) {
                            em.createNativeQuery(script).executeUpdate();
                        }
                    }
                    em.clear();
                    em.flush();
                    return null;
                }
            });
        } finally {
            IOUtils.closeQuietly(is1);
            IOUtils.closeQuietly(is2);
        }
    }
}
