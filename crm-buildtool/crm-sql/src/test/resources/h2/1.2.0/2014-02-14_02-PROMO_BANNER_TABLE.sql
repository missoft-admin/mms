create table CRM_PROMO_BANNER (
    ID varchar(255) not null,
    END_DATE date,
    IMAGE blob,
    LINK varchar(255),
    NAME varchar(255),
    START_DATE date,
    primary key (ID)
);