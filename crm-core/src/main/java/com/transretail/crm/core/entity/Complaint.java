package com.transretail.crm.core.entity;


import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.Type;
import org.joda.time.LocalDateTime;

import com.transretail.crm.core.entity.lookup.LookupDetail;
import com.transretail.crm.core.entity.support.CustomAuditableEntity;


@Entity(name="CRM_COMPLAINT")
public class Complaint extends CustomAuditableEntity<Long> {

	private static final long serialVersionUID = 1L;

	public static enum ComplaintStatus { NEW, ASSIGNED, REASSIGNED, INPROGRESS, CLOSED };
	public static enum ComplaintPriority { NORMAL, HIGH };
	public static enum ComplaintMemberField { MOBILENO, EMAIL };
	public static enum ComplaintCategory {PRODUCT, SERVICE, PRICE, QUALITY, OTHER};

	public static final int MESSAGE_COUNT = 900;
	public static final int PER_COUNT = 160;
	public static final int COMPLAINTS_COUNT = 255;



	@Column(name = "TICKET_NO", length=20, unique=true)
	private String ticketNo;

    @Temporal(TemporalType.TIMESTAMP)
	@Column(name = "FILED_DATE")
    @Type(type = "org.jadira.usertype.dateandtime.joda.PersistentLocalDateTime")
	private LocalDateTime dateFiled;

	@Column(name = "STORE_CODE", length=20)
	private String storeCode;

	@Column(name = "FIRST_NAME", length=100, nullable=false)
	private String firstName;

	@Column(name = "LAST_NAME", length=100)
	private String lastName;

	@JoinColumn(name="GENDER", nullable=false)
	@OneToOne
	private LookupDetail gender;

	@Column(name = "COMPLAINTS", nullable=false)
	private String complaints;

	@Column(name = "MOBILE_NO", length=20)
	private String mobileNo;

	@Column(name = "EMAIL", length=50)
	private String email;

	@Column(name = "ASSIGNED_TO", length=20)
	private String assignedTo;

    @Column(length=20)
    //@Enumerated(EnumType.STRING)
    private String status;
    
    @Column(length=20)
    @Enumerated(EnumType.STRING)
    private ComplaintPriority priority;

    @Column(name="CATEGORY", length=20)
    @Enumerated(EnumType.STRING)
    private ComplaintCategory category;

    @Column(name="MEMBER_BARCODE", length=100)
    private String memberBarcode;

    public String getTicketNo() {
		return ticketNo;
	}
	public void setTicketNo(String ticketNo) {
		this.ticketNo = ticketNo;
	}
	public LocalDateTime getDateFiled() {
		return dateFiled;
	}
	public void setDateFiled(LocalDateTime dateFiled) {
		this.dateFiled = dateFiled;
	}
	public String getStoreCode() {
		return storeCode;
	}
	public void setStoreCode(String storeCode) {
		this.storeCode = storeCode;
	}
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public LookupDetail getGender() {
		return gender;
	}
	public void setGender(LookupDetail gender) {
		this.gender = gender;
	}
	public String getComplaints() {
		return complaints;
	}
	public void setComplaints(String complaints) {
		this.complaints = complaints;
	}
	public String getMobileNo() {
		return mobileNo;
	}
	public void setMobileNo(String mobileNo) {
		this.mobileNo = mobileNo;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getAssignedTo() {
		return assignedTo;
	}
	public void setAssignedTo(String assignedTo) {
		this.assignedTo = assignedTo;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public ComplaintPriority getPriority() {
		return priority;
	}
	public void setPriority(ComplaintPriority priority) {
		this.priority = priority;
	}
	public ComplaintCategory getCategory() {
		return category;
	}
	public void setCategory(ComplaintCategory category) {
		this.category = category;
	}
	public String getMemberBarcode() {
		return memberBarcode;
	}
	public void setMemberBarcode(String memberBarcode) {
		this.memberBarcode = memberBarcode;
	}

}
