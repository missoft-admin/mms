package com.transretail.crm.web.controller;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ValidationUtils;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.google.common.collect.Lists;
import com.transretail.crm.common.service.dto.response.ResultList;
import com.transretail.crm.core.dto.StoreMemberDto;
import com.transretail.crm.core.dto.StoreMemberSearchDto;
import com.transretail.crm.core.entity.StoreMember;
import com.transretail.crm.core.entity.lookup.Store;
import com.transretail.crm.core.service.CodePropertiesService;
import com.transretail.crm.core.service.LookupService;
import com.transretail.crm.core.service.StoreMemberService;
import com.transretail.crm.core.service.StoreService;
import com.transretail.crm.web.controller.util.ControllerResponse;
import com.transretail.crm.web.controller.util.ControllerResponseExceptionHandler;
import com.transretail.crm.web.controller.util.StoreMemberSchemeValidator;

/**
 * @author ftopico
 */
@RequestMapping("/storemember")
@Controller
public class StoreMemberSchemeController extends ControllerResponseExceptionHandler {

    private static final String PATH_STORE_MEMBER_SCHEME    = "/storemember";
    private static final String PATH_CREATE_STORE_MEMBER_SCHEME     = "/storemember/create";
    
    private static final String UI_ATTR_FORM_ACTION   = "action";
    private static final String UI_ATTR_STORE_MEMBER   = "storeMember";
    private static final String UI_ATTR_DIALOG_LABEL   = "dialogLabel";
    private static final String UI_ATTR_STORES   = "stores";
    private static final String UI_ATTR_COMPANIES   = "companies";
    private static final String UI_ATTR_CARD_TYPES   = "cardTypes";
    protected Logger logger = LoggerFactory.getLogger(getClass());
    
    @Autowired
    private StoreMemberService storeMemberService;
    @Autowired
    private StoreService storeService;
    @Autowired
    private LookupService lookupService;
    @Autowired
    private CodePropertiesService codePropertiesService;
    @Autowired
    private StoreMemberSchemeValidator storeMemberSchemeValidator;
    @Autowired
    private MessageSource messageSource;
    
    @RequestMapping( produces="text/html" )
    public String showStoreMemberScheme(
            @RequestParam(value = "page", required = false) Integer page,
            @RequestParam(value = "size", required = false) Integer size,
            Model inUiModel ) {
        
        populateFields(inUiModel);
        return PATH_STORE_MEMBER_SCHEME;
    }
    
    @RequestMapping(value = "/list", method = RequestMethod.POST)
    public @ResponseBody ResultList<StoreMemberDto> listStoreMemberScheme(@RequestBody StoreMemberSearchDto searchForm) {
        logger.debug("[Method Call from StoreMemberSchemeController] storeMemberService.searchStoreMembers()");
        return storeMemberService.searchStoreMembers(searchForm);
    }
    
    @RequestMapping(value = "/create", produces = "text/html")
    public String createStoreMemberScheme(
            @ModelAttribute("storeMember") StoreMember inStoreMember,
            Model inUiModel) {
        
        populateFields(inUiModel);
        inUiModel.addAttribute(UI_ATTR_FORM_ACTION, "/storemember/save");
        inUiModel.addAttribute(UI_ATTR_DIALOG_LABEL, messageSource.getMessage("label_store_member_form_create", 
                null, LocaleContextHolder.getLocale()) );
        return PATH_CREATE_STORE_MEMBER_SCHEME;
    }
    
    @RequestMapping(value = "/edit/{id}", produces = "text/html")
    public String editStoreMember(
            @PathVariable(value = "id") Long id,
            @ModelAttribute("storeMember") StoreMemberDto storeMember,
            Model inUiModel) {
        
        inUiModel.addAttribute(UI_ATTR_STORE_MEMBER, storeMemberService.findByIdAndPreparedForView(id));
        populateFields(inUiModel);
        inUiModel.addAttribute(UI_ATTR_FORM_ACTION, "/storemember/update");
        inUiModel.addAttribute(UI_ATTR_DIALOG_LABEL,  messageSource.getMessage("label_store_member_form_update", 
                null, LocaleContextHolder.getLocale()));
        return PATH_CREATE_STORE_MEMBER_SCHEME;
    }
    
    @RequestMapping(value = "/save", produces="application/json; charset=utf-8", method=RequestMethod.POST)
    public @ResponseBody ControllerResponse saveStoreMemberScheme(
            @ModelAttribute("storeMember") StoreMemberDto storeMemberDto,
            BindingResult inBinding) {
        
        ControllerResponse response = new ControllerResponse();
        response.setSuccess(true);
        
        if (isValid(storeMemberDto, inBinding)) {
            logger.debug("[Method Call from StoreMemberSchemeController] storeMemberService.saveStoreMemberDto()");
            storeMemberService.saveStoreMemberDto(storeMemberDto);
            response.setSuccess(true);
            response.setResult(storeMemberDto);
        } else {
            response.setSuccess(false);
            response.setResult(inBinding.getAllErrors());
        }
        
        return response;
    }
    
    @RequestMapping(value = "/update", produces="application/json; charset=utf-8", method=RequestMethod.POST)
    public @ResponseBody ControllerResponse updateStoreMemberScheme(
            @ModelAttribute("storeMember") StoreMemberDto storeMemberDto,
            BindingResult inBinding) {
        
        ControllerResponse response = new ControllerResponse();
        response.setSuccess(true);
        
        if (isValid(storeMemberDto, inBinding)) {
            logger.debug("[Method Call from StoreMemberSchemeController] storeMemberService.updateStoreMemberDto()");
            storeMemberService.updateStoreMemberDto(storeMemberDto);
            response.setSuccess(true);
            response.setResult(storeMemberDto);
        } else {
            response.setSuccess(false);
            response.setResult(inBinding.getAllErrors());
        }
        
        return response;
    }
    
    @RequestMapping(value = "/delete/{id}", produces="application/json; charset=utf-8", method=RequestMethod.POST)
    public @ResponseBody ControllerResponse deleteStoreMemberScheme(
            @PathVariable(value = "id") Long id,
            @ModelAttribute("storeMember") StoreMemberDto storeMemberDto,
            BindingResult binding) {
        ControllerResponse response = new ControllerResponse();
        response.setSuccess(true);
        if (id != null) {
            logger.debug("[Method Call from StoreMemberSchemeController] storeMemberService.deleteStoreMemberModel()");
            storeMemberService.deleteStoreMemberModel(storeMemberService.findById(id));
        }
        
        return response;
    }
    
    private boolean isValid(StoreMemberDto storeMember,
            BindingResult binding) {
        if (StringUtils.isBlank(storeMember.getStore())) {
            binding.reject(messageSource.getMessage("error_store_member_empty_store", null, LocaleContextHolder.getLocale()));
        }
        
        ValidationUtils.invokeValidator(storeMemberSchemeValidator, storeMember, binding);
        return !binding.hasErrors();
    }

    private void populateFields(Model inUiModel) {
        List<Store> stores = Lists.newArrayList(storeService.getAllStores());
        /*Collections.sort(stores, new Comparator<Store>() {
            @Override
            public int compare(Store o1, Store o2) {
                return o1.getName().compareTo(o2.getName());
            }
        });*/
        
        inUiModel.addAttribute(UI_ATTR_STORES, stores);
        inUiModel.addAttribute(UI_ATTR_COMPANIES, lookupService.getDetailsByHeaderCode(codePropertiesService.getHeaderCompany()));
        inUiModel.addAttribute(UI_ATTR_CARD_TYPES, lookupService.getDetailsByHeaderCode(codePropertiesService.getHeaderCardTypes()));
    }

}
