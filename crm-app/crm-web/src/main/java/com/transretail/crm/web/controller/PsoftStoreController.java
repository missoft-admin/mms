package com.transretail.crm.web.controller;


import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.google.common.collect.Lists;
import com.mysema.query.types.OrderSpecifier;
import com.transretail.crm.common.service.dto.response.ResultList;
import com.transretail.crm.core.dto.PsoftStoreDto;
import com.transretail.crm.core.dto.PsoftStoreSearchDto;
import com.transretail.crm.core.dto.StoreDto;
import com.transretail.crm.core.entity.lookup.LookupDetail;
import com.transretail.crm.core.entity.lookup.QLookupDetail;
import com.transretail.crm.core.service.CodePropertiesService;
import com.transretail.crm.core.service.LookupService;
import com.transretail.crm.core.service.PsoftStoreService;
import com.transretail.crm.core.service.StoreService;
import com.transretail.crm.web.controller.util.ControllerResponse;


@Controller
@RequestMapping( "/store/peoplesoft" )
public class PsoftStoreController extends AbstractReportsController {

	private static final String VIEW_MAIN 				= "view/store/peoplesoft";

	private static final String UIATTR_FORM		  		= "psoftStore";
	private static final String UIATTR_BUNITS	  		= "businessUnits";
	private static final String UIATTR_BREGIONS	  		= "businessRegions";
	private static final String UIATTR_STORES	  		= "stores";



	@Autowired
	PsoftStoreService service;
	@Autowired
	StoreService storeService;
	@Autowired
	CodePropertiesService codePropertiesService;
	@Autowired
	LookupService lookupService;
    @Autowired
    private MessageSource messageSource;



    @InitBinder
	void initBinder( WebDataBinder binder ) {
		DateFormat df = new SimpleDateFormat( "dd MMM yyyy" );
		df.setLenient( false );
		binder.registerCustomEditor( Date.class, new CustomDateEditor( df, true ) );
	}

    @RequestMapping( produces = "text/html" )
    public ModelAndView showMain() {
    	ModelAndView mv = new ModelAndView( VIEW_MAIN );
    	mv.addObject( UIATTR_FORM, new PsoftStoreDto() );
    	mv.addObject( UIATTR_BUNITS, lookupService.getActiveDetailsByHeaderCode( 
    		codePropertiesService.getHeaderBusinessUnit(), new OrderSpecifier<?>[]{ QLookupDetail.lookupDetail.description.asc() } ) );
    	mv.addObject( UIATTR_BREGIONS, lookupService.getActiveDetailsByHeaderCode( 
    		codePropertiesService.getHeaderBusinessRegion(), new OrderSpecifier<?>[]{ QLookupDetail.lookupDetail.description.asc() } ) );
    	mv.addObject( UIATTR_STORES, service.getUnmappedStores() );
        return mv;
    }

    @RequestMapping( value="/save", produces="application/json; charset=utf-8", method=RequestMethod.POST )
	public @ResponseBody ControllerResponse save(
			@ModelAttribute(UIATTR_FORM) PsoftStoreDto dto, 
			BindingResult br ) {

    	ControllerResponse resp = new ControllerResponse();
    	if ( null != dto ) {
    		if ( StringUtils.isBlank( dto.getPeoplesoftCode() ) ) {
    			br.reject( messageSource.getMessage( "store.psoft.err.code", null, LocaleContextHolder.getLocale() ) );
    		}
    		if ( null == dto.getStoreId() ) {
    			br.reject( messageSource.getMessage( "store.psoft.err.store", null, LocaleContextHolder.getLocale() ) );
    		}
    		if ( StringUtils.isBlank( dto.getBuCode() ) ) {
    			br.reject( messageSource.getMessage( "store.psoft.err.bu", null, LocaleContextHolder.getLocale() ) );
    		}
    		resp.setSuccess( !br.hasErrors() );
    		resp.setResult( resp.isSuccess()? service.save( dto ) : br.getAllErrors() );
    	}
    	return resp;
    }

    @RequestMapping( value="/update/{id}", produces="application/json; charset=utf-8", method=RequestMethod.POST )
	public @ResponseBody ControllerResponse update(
			@PathVariable("id") Long id,
			@ModelAttribute(UIATTR_FORM) PsoftStoreDto dto, 
			BindingResult br ) {
    	dto.setId( id );
    	return save( dto, br );
    }

    @RequestMapping( value="/delete/{id}", produces="application/json; charset=utf-8", method=RequestMethod.POST )
	public @ResponseBody ControllerResponse delete( @PathVariable("id") Long id ) {
    	ControllerResponse resp = new ControllerResponse();
    	if ( null != id ) {
    		service.delete( id );
    		resp.setSuccess( true );
    	}
    	return resp;
    }

    @RequestMapping( value="/edit/{id}", produces="application/json; charset=utf-8", method=RequestMethod.POST )
	public @ResponseBody ControllerResponse edit( @PathVariable("id") Long id ) {
    	ControllerResponse resp = new ControllerResponse();
    	if ( null != id ) {
    		resp.setResult( service.getDto( id ) );
    		resp.setSuccess( true );
    	}
    	return resp;
    }

    @RequestMapping(value = "/list", method = RequestMethod.POST)
    public @ResponseBody ResultList<PsoftStoreDto> list( @RequestBody PsoftStoreSearchDto dto ) {
    	return service.search( dto );
    }

    @RequestMapping(value = "/list/gcallowed/{filter}", produces="application/json; charset=utf-8", method=RequestMethod.GET)
    public @ResponseBody List<StoreDto> listStores( @PathVariable(value="filter") String filter ) {
    	PsoftStoreSearchDto dto = new PsoftStoreSearchDto();
    	dto.setStoreLike( filter );
    	return service.getGcAllowedStores( dto );
    }

    @RequestMapping(value = "/list/all/{filter}", produces="application/json; charset=utf-8", method=RequestMethod.GET)
    public @ResponseBody List<StoreDto> listAllStores( @PathVariable(value="filter") String filter ) {
    	return storeService.find( filter );
    }

    @RequestMapping(value = "/store/list", produces="application/json; charset=utf-8", method = RequestMethod.GET)
    public @ResponseBody List<StoreDto> listStores() {
    	return service.getUnmappedStores();
    }

}
