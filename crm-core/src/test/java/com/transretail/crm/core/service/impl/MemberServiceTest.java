package com.transretail.crm.core.service.impl;

import com.transretail.crm.core.dto.MemberDto;
import com.transretail.crm.core.entity.MemberModel;
import com.transretail.crm.core.entity.embeddable.CustomerProfile;
import com.transretail.crm.core.entity.embeddable.ProfessionalProfile;
import com.transretail.crm.core.entity.lookup.LookupDetail;
import com.transretail.crm.core.repo.MemberRepo;
import com.transretail.crm.core.service.CodePropertiesService;
import com.transretail.crm.core.service.MemberService;
import com.transretail.crm.core.util.generator.IdGeneratorService;
import java.util.ArrayList;
import java.util.List;
import org.joda.time.LocalDate;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.*;
import org.springframework.beans.factory.annotation.Configurable;
import org.springframework.data.domain.Page;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import static org.mockito.Mockito.*;

@Configurable
@ContextConfiguration(locations = "classpath*:/META-INF/spring/applicationContext*.xml")
@Transactional
@RunWith(SpringJUnit4ClassRunner.class)
public class MemberServiceTest {

	MemberModel memberModel;
	MemberModel memberModel2;
	List<MemberModel> memberModels;
	final Long ID1 = 11110000L, ID2 = 11110011L;
	final String USERNAME1 = "Model1";
	final String CONTACT1 = "09101111111";

    static String employeeType;


    @Mock
	MemberModel theModel1;
	
	@Mock
	MemberModel theModel2;
	
	@Mock
	private MemberRepo memberRepo;
	
	@Mock
	private IdGeneratorService pinGeneratorService;
	
	@Mock
	private IdGeneratorService accountIdGeneratorService;
	
	@Mock
	private PasswordEncoder passwordEncoder;
	
	@Mock
	private CodePropertiesService codePropertiesService;
	
	@InjectMocks
	MemberService memberService = new MemberServiceImpl();
	
	@Before
	public void setup() {

        employeeType = "MTYP002";
		memberModel = new MemberModel();
        memberModel2 = new MemberModel();
        memberModels = new ArrayList<MemberModel>();
        memberModels.add(memberModel);
        memberModels.add(memberModel2);
		
        MockitoAnnotations.initMocks(this);
		when(theModel1.getId()).thenReturn(ID1);
		when(theModel2.getId()).thenReturn(ID2);
	}
	
	@Test
	public void testCountAllCustomerModels() {
		long expected = 1L;
		when(memberRepo.count()).thenReturn(expected);
		long returned = memberService.countAllCustomerModels();
		Assert.assertEquals(expected, returned);
	}
	
	@Test
	public void testFindByAccountId() {
		String id = "100";
		when(memberRepo.findByAccountId(id)).thenReturn(memberModel);
		MemberModel returned = memberService.findByAccountId(id);
		Assert.assertEquals(memberModel, returned);
	}
	
	@Test
	public void testFindByContact() {
		String contact = "111111";
		when(memberRepo.findByContact(contact)).thenReturn(memberModel);
		MemberModel returned = memberService.findByContact(contact);
		Assert.assertEquals(memberModel, returned);
	}
	
	@Test
	public void testFindByEmail() {
		String email = "test@email.com";
		when(memberRepo.findByEmail(email)).thenReturn(memberModels);
		List<MemberModel> returned = memberService.findByEmail(email);
		Assert.assertEquals(memberModels, returned);
		Assert.assertEquals(2, returned.size());
	}
	
	@Test
	public void testFindCustomerModel() {
		long id = 100L;
		when(memberRepo.findOne(id)).thenReturn(memberModel);
		MemberModel returned = memberService.findCustomerModel(id);
		Assert.assertEquals(memberModel, returned);
	}
	
	@Test
	public void testFindAllCustomerModels() {
		when(memberRepo.findAll()).thenReturn(memberModels);
		List<MemberModel> returned = memberService.findAllCustomerModels();
		Assert.assertEquals(memberModels, returned);
	}
	
	@Test
	public void testFindCustomerModelEntries() {
		int firstResult = 0;
		int maxResult = 5;
		Page<MemberModel> memberModelPage = (Page<MemberModel>) mock(Page.class);
		
		when(memberRepo.findAll(new org.springframework.data.domain.PageRequest(firstResult / maxResult, maxResult))).thenReturn(memberModelPage);
		when(memberModelPage.getContent()).thenReturn(memberModels);
	}
	
	@Test
	public void testSaveCustomerModel() {
		String password = "password";
		memberModel.setPassword(password);
		MemberService spy = spy(memberService);
		when(pinGeneratorService.generateId()).thenReturn("12345");
		when(passwordEncoder.encode(password)).thenReturn("encoded");
		
		spy.saveCustomerModel(memberModel);
		
		verify(passwordEncoder, times(1)).encode(password);
		verify(memberRepo, times(1)).saveAndFlush(memberModel);
		verify(spy).saveCustomerModel(argThat(new ArgumentMatcher<MemberModel>() {
            @Override
            public boolean matches(Object argument) {
                MemberModel model = (MemberModel) argument;
                return model.getPassword().equals("encoded");
            }
        }));
		
		verifyNoMoreInteractions(memberRepo);
	}
	
    @Test
    public void testUsernameAndPasswordDefaulting() {
	// ensure username and password is not yet set
	memberModel.setUsername("");
	memberModel.setPassword("");
	final LocalDate testBirthDate = new LocalDate(1990, 1, 30);
	final String EXPECTED_USERNAME_FROM_MOBILE = "4112000";
	final String EXPECTED_PASSWORD = testBirthDate.toString(MemberServiceImpl.DEFAULT_PWD_FROM_BIRTH_DATE_FORMAT);
	// setup mobile no and birthday (in this test scope only)
	memberModel.setCustomerProfile(new CustomerProfile());
	memberModel.getCustomerProfile().setBirthdate(testBirthDate.toDate());
	memberModel.setContact(EXPECTED_USERNAME_FROM_MOBILE);
	memberModel.setProfessionalProfile(new ProfessionalProfile());
	memberModel.setMemberType(new LookupDetail("MTYP001"));
	// set passwordEncoder to return same string.
	when(passwordEncoder.encode(EXPECTED_PASSWORD)).thenReturn(EXPECTED_PASSWORD);
	when(codePropertiesService.getDetailMemberTypeEmployee()).thenReturn("MTYP002");
	MemberService memberServiceSpy = spy(memberService);

	final MemberDto memberDto = new MemberDto(memberModel);
	memberServiceSpy.saveCustomerModel(memberDto);

	verify(memberServiceSpy).saveCustomerModel(argThat(new ArgumentMatcher<MemberDto>() {
	    @Override
	    public boolean matches(Object argument) {
		MemberDto dto = (MemberDto) argument;
		return EXPECTED_USERNAME_FROM_MOBILE.equals(dto.getUsername())
			&& dto.getPassword().equals(EXPECTED_PASSWORD);
	    }
	}));

	ArgumentCaptor<MemberModel> mmac = ArgumentCaptor.forClass(MemberModel.class);
	verify(memberRepo).save(mmac.capture());
	verify(memberRepo, times(1)).save(mmac.getValue());
	
	verifyNoMoreInteractions(memberRepo);
    }

	@Test
	public void testUpdateCustomerModel() {
		memberModel.setPassword("newPassword");
		memberModel2.setPassword("oldPassword");
		when(memberRepo.findByUsername(memberModel.getUsername())).thenReturn(memberModel2);
		MemberService spy = spy(memberService);
		
		spy.updateCustomerModel(memberModel);
		
		verify(memberRepo, times(1)).findByUsername(memberModel.getUsername());
		verify(memberRepo, times(1)).save(memberModel);
		verify(spy).updateCustomerModel(argThat(new ArgumentMatcher<MemberModel>() {
            @Override
            public boolean matches(Object argument) {
                MemberModel model = (MemberModel) argument;
                return model.getPassword().equals("oldPassword");
            }
        }));
		verifyNoMoreInteractions(memberRepo);
	}
	
	@Test
	public void testFindDuplicate() {
		when(memberRepo.findByContact(memberModel.getContact())).thenReturn(memberModel2);
		MemberModel returned = memberService.findDuplicate(memberModel);
		Assert.assertEquals(memberModel2, returned);
	}
	
	@Test
	public void testFindDuplicateContact() {		
		when(memberRepo.findByContact(theModel1.getContact())).thenReturn(theModel1);
		MemberModel returned = memberService.findDuplicateContact(theModel1);
		Assert.assertNull(returned);
		
		when(memberRepo.findByContact(theModel1.getContact())).thenReturn(theModel2);
		returned = memberService.findDuplicateContact(theModel1);
		Assert.assertNotNull(returned);
	}
	
	@Test
	public void testFindDuplicateUsername() {
		when(memberRepo.findByUsername(theModel1.getUsername())).thenReturn(theModel1);
		MemberModel returned = memberService.findDuplicateUsername(theModel1);
		Assert.assertNull(returned);
		
		when(memberRepo.findByUsername(theModel1.getUsername())).thenReturn(theModel2);
		returned = memberService.findDuplicateUsername(theModel1);
		Assert.assertNotNull(returned);
	}
	
	@Test
    @Ignore("Ignored due to removal parent field")
	public void testFindDuplicateCompany() {
		List<MemberModel> list = new ArrayList<MemberModel>();
		list.add(theModel1);
		when(memberRepo.findByCompanyName(theModel1.getUsername())).thenReturn(list);
		MemberModel returned = memberService.findDuplicateCompany(theModel1);
		Assert.assertNull(returned);
		
		list.remove(0);
		list.add(theModel2);
		when(memberRepo.findByCompanyName(theModel1.getUsername())).thenReturn(list);
		returned = memberService.findDuplicateCompany(theModel1);
		Assert.assertNotNull(returned);
		
		String company = "test";
		MemberModel member3 = Mockito.mock(MemberModel.class);
		when(member3.getParentAccountId()).thenReturn(theModel2.getAccountId());
		when(member3.getCompanyName()).thenReturn(company);
		when(member3.getId()).thenReturn(100L);
		MemberModel member4 = Mockito.mock(MemberModel.class);
		when(member4.getParentAccountId()).thenReturn(theModel2.getAccountId());
		when(member4.getCompanyName()).thenReturn(company);
		when(member4.getId()).thenReturn(101L);
		
		list.add(member3);
		list.add(member4);
		when(memberRepo.findByCompanyName(company)).thenReturn(list);
		when(theModel2.getAccountId()).thenReturn("1");
		Assert.assertNull(memberService.findDuplicateCompany(member3));
		Assert.assertNull(memberService.findDuplicateCompany(member4));
		Assert.assertNotNull(memberService.findDuplicateCompany(theModel1));
	}
	
	@Test
	public void testFindDuplicateEmail() {
		memberModels.clear();
        memberModels.add(theModel1);
		when(memberRepo.findByEmail(theModel1.getEmail())).thenReturn(memberModels);
		MemberModel returned = memberService.findDuplicateEmail(theModel1);
		Assert.assertNull(returned);

        memberModels.add(theModel2);
		when(memberRepo.findByEmail(theModel1.getEmail())).thenReturn(memberModels);
		returned = memberService.findDuplicateEmail(theModel1);
		Assert.assertNotNull(returned);
	}
	
	@Test
	public void testFindDuplicateName() {
		when(memberRepo.findByLastNameAndFirstName(theModel1.getLastName(),
						theModel1.getFirstName())).thenReturn(theModel1);
		MemberModel returned = memberService.findDuplicateName(theModel1);
		Assert.assertNull(returned);
		
		when(memberRepo.findByLastNameAndFirstName(theModel1.getLastName(),
				theModel1.getFirstName())).thenReturn(theModel2);
		returned = memberService.findDuplicateName(theModel1);
		Assert.assertNotNull(returned);
	}
	
	@Test
	public void testGenerateInitModel() {
		/*String accountId = "100";
		String pinId = "11111111111";
		
		when(accountIdGeneratorService.generateId()).thenReturn(accountId);
		when(pinGeneratorService.generateId()).thenReturn(pinId);*/
		
		MemberModel returned = memberService.generateInitModel();
		
		/*Assert.assertEquals(accountId, returned.getAccountId());
		Assert.assertEquals(pinId, returned.getPin());
		Assert.assertEquals(accountId, returned.getUsername());*/
		Assert.assertTrue(returned.getEnabled());
	}
	
	@Test
	public void testFindByCriteria() {
		when(memberRepo.findByCriteria(memberModel, employeeType)).thenReturn(memberModels);
		List<MemberModel> returned = memberService.findByCriteria(memberModel, employeeType);
		Assert.assertEquals(memberModels, returned);
		Assert.assertEquals(2, returned.size());
	}


    @Test
    public void testValidatePin() {
        String id = "100";
        when(memberRepo.findByAccountId(id)).thenReturn(memberModel);
        MemberModel returned = memberService.findByAccountId(id);
        Assert.assertEquals(memberModel, returned);

    }
    
    @Test
    public void testUpdatePin() {
    	String newPin = "1000";
    	String oldPin = "999";
    	MemberService spy = spy(memberService);
    	
    	memberModel.setPin(oldPin);
 
    	when(memberRepo.findOne(001L)).thenReturn(memberModel);
    	
    	spy.updatePin(001L, newPin);
    	
    	verify(memberRepo, times(1)).findOne(001L);
		verify(memberRepo, times(1)).save(memberModel);
		verifyNoMoreInteractions(memberRepo);
    }

    @Test
    @Ignore("\n"
        + "java.lang.IllegalArgumentException: eq(null) is not allowed. Use isNull() instead\n"
        + "\tat com.mysema.query.types.expr.SimpleExpression.eq(SimpleExpression.java:133)\n"
        + "\tat com.transretail.crm.core.service.impl.MemberServiceImpl.findSupplementaryMembersByParentId(MemberServiceImpl.java:559)")
    public void testActivateMember() {
        MemberService spy = spy(memberService);
        when(memberRepo.findOne(1L)).thenReturn(memberModel);
        spy.activateMember(1L, false);
        verify(memberRepo, times(1)).findOne(1L);
        verify(memberRepo, times(1)).save(memberModel);
        verifyNoMoreInteractions(memberRepo);
    }

}
