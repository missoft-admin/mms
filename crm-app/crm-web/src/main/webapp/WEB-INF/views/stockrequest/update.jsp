<%@include file="/WEB-INF/views/common/taglibs.jsp" %>

<div class="modal-dialog">
<script src="<spring:url value="/js/common/typeaheadutil.js" />"></script>
	<div class="modal-content">
		<div class="modal-header">
		  <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
			<spring:message code="gc.stock.label.stockrequest" var="typeName" />
			<h4 class="modal-title"><spring:message code="global_menu_new" arguments="${typeName}"/></h4>
		</div>
		<div class="modal-body">
			<div id="contentError" class="hide alert alert-error">
		        <button type="button" class="close" onclick="$(this).parent().hide();">&times;</button>
		        <div><form:errors path="*"/></div>
	      	</div>
			
			<form:form id="stockrequest" modelAttribute="stockrequest" name="stockrequest" 
					action="${pageContext.request.contextPath}/stockrequest/">
			<form:hidden path="inventoryLocation" id="inventoryLocation" />		
			
			<div class="row-fluid">
				<div class="span4">
					<label><spring:message code="gc.stock.label.requestno" /></label>
					<form:input id="requestNoMain" path="requestNo" readonly="true"/>
				</div>
				<div class="span4">
					<label><spring:message code="gc.stock.label.requestdate" /></label>
					<form:input id="requestDateMain" path="requestDate" readonly="true"/>
				</div>
				<!-- <div class="span4">
					<label><spring:message code="gc.stock.label.requestedby" /></label>
					<form:input path="createdBy" readonly="true"/>
				</div> -->
				<div class="span4">
					<label><spring:message code="gc.stock.label.sourcelocation" /></label>
					<c:choose>
						<c:when test="${isHeadOffice}">
							<%-- <form:select id="sourceLocationMain" path="sourceLocation" items="${sourceLocations}"/> --%>
							<div>
				              <form:input path="sourceLocationDescription" id="sourceLocationDescription" cssClass="form-control input-medium" />
				              <form:hidden path="sourceLocation" id="sourceLocation"/>
				              <script type='text/javascript'>
				              $(document).ready( function() {
				                  var url = '<c:url value="/store/peoplesoft/list/all/" />';$(".sourceLocation").val(sourceLocation);
				                  var $paymentStore = $( "#sourceLocation" );
				                  TypeAhead.process( $( "#sourceLocationDescription" ), $paymentStore, url, { label: "codeAndName", value: "code" },
				                		[ "code" ],
				                		function( $item ) { $(".sourceLocation").val( $item[ "code" ] ); } );
				              });
				              </script>
				            </div>
						</c:when>
						<c:otherwise>
							<form:select id="sourceLocationMain" path="sourceLocation" items="${sourceLocations}" readonly="true"/>
						</c:otherwise>
					</c:choose>
				</div>
			</div>
			
			<div id="requestContainer">
				<table id="requestTable" class="table table-condensed table-compressed table-striped" style="width: 94%;">
					<thead><tr>
						<th><spring:message code="gc.stock.label.cardtype" /></th>
						<th><spring:message code="gc.stock.label.quantity" /></th>
						<th><spring:message code="gc.stock.label.allocateto" /></th>
						<th><spring:message code="gc.stock.label.isforpromo" /></th>
						<th></th>
					</tr></thead>
					<tbody>
					<c:forEach items="${stockrequest.requests}" var="request" varStatus="status">
						<tr>
							<td hidden="true" class="hidden">
								<input type="hidden" name="requests[${status.index}].requestNo" class="hidden requestNo" />
								<input type="hidden" name="requests[${status.index}].requestDate" class="hidden requestDate" />
								<input type="hidden" name="requests[${status.index}].sourceLocation" class="hidden sourceLocation" />
								<input type="hidden" name="requests[${status.index}].transferRefNo" class="hidden transferRefNo" />
							</td>
							<td><select name="requests[${status.index}].productCode" class="productCode">
								<option value=""></option>
								<c:forEach items="${cardTypes}" var="type">
									<option value="${type.productCode}">${type.productDesc}</option>
								</c:forEach>
							</select></td>
							<td><input name="requests[${status.index}].quantity" class="input-mini numeric"/></td>
							<td>
								<select class="allocateTo" name="requests[${status.index}].allocateTo">
									<option value=""></option>
									<c:forEach items="${stores}" var="store">
										<option value="${store.code}">${store.name}</option>
									</c:forEach>
								</select>
							</td>
							<td><input type="checkbox" name="requests[${status.index}].isForPromo" class="isForPromo" /></td>
							<td><button type="button" id="" class="tiptip btn rowDelete icn-delete hidden" title="<spring:message code="label_remove"/>">-</button></td>
						</tr>
					</c:forEach>
					</tbody>
				</table>
				<button type="button" id="addRowButton" class="tiptip btn btn-small pull-right" style="" title="<spring:message code="label_add"/>">+</button>
			</div>
			</form:form>
		</div>
		<div class="modal-footer">			
    		<button type="button" id="stockRequestSave" class="btn btn-primary"><spring:message code="label_save" /></button>
    		<button type="button" id="" class="btn btn-default" data-dismiss="modal"><spring:message code="label_cancel"/></button>
		</div>
	</div>
</div>

<script>
var allocateList = new Object();
var duplicateError = "<spring:message code='gc.stock.error.duplicate' />";
</script>

<c:forEach items="${stores}" var="store">
	<script>
		allocateList["${store.code}"] = [];
	</script>
</c:forEach>

<script src="<spring:url value="/js/viewspecific/giftcard/stockrequest.js"/>"></script>
<link href="<spring:url value="/styles/temp.css" />" rel="stylesheet">

<style>
    .table {
        float: left;
    }
    #requestTable.table tbody tr td:last-child {
        min-width: 40px;
    }
    #requestTable.table tbody tr td:last-child button {
        margin-left: 10px;
    }
    #requestTable.table-compressed td {
      padding-left: 0px !important;
    }
</style>