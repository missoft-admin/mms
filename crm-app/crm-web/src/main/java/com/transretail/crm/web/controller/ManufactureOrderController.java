package com.transretail.crm.web.controller;

import java.beans.PropertyEditorSupport;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.joda.time.LocalDate;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ValidationUtils;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.google.common.collect.Lists;
import com.transretail.crm.common.reporting.jasper.JRProcessor;
import com.transretail.crm.common.service.dto.response.CodeDescDto;
import com.transretail.crm.common.service.dto.response.ResultList;
import com.transretail.crm.common.web.notification.NotificationType;
import com.transretail.crm.common.web.notification.annotation.Notifiable;
import com.transretail.crm.common.web.notification.annotation.NotificationUpdate;
import com.transretail.crm.core.entity.CounterModel;
import com.transretail.crm.core.entity.lookup.LookupDetail;
import com.transretail.crm.core.repo.CounterModelRepo;
import com.transretail.crm.core.service.CodePropertiesService;
import com.transretail.crm.core.service.LookupService;
import com.transretail.crm.loyalty.dto.ManufactureOrderDto;
import com.transretail.crm.loyalty.dto.ManufactureOrderSearchDto;
import com.transretail.crm.loyalty.dto.ManufactureOrderTierDto;
import com.transretail.crm.loyalty.dto.ReceiveOrderDto;
import com.transretail.crm.loyalty.service.LoyaltyCardService;
import com.transretail.crm.loyalty.service.ManufactureOrderService;
import com.transretail.crm.web.controller.util.ControllerResponse;
import com.transretail.crm.web.controller.util.ManufactureOrderValidator;
import com.transretail.crm.web.controller.util.ReceiveOrdersValidator;
//import com.transretail.crm.web.notifications.Notifiable;
//import com.transretail.crm.web.notifications.NotificationUpdate;

@RequestMapping("/manufactureorder")
@Controller
public class ManufactureOrderController extends AbstractReportsController {
	
	private static final String UI_ATTR_MO = "manufactureOrder";
	private static final String UI_ATTR_RO = "receiveOrderDto";
	private static final String UI_ATTR_MOS = "manufactureOrders";
	private static final String UI_ATTR_DRAFTED = "draftStatus";
	private static final String UI_ATTR_FOR_APPROVAL = "forApprovalStatus";
	private static final String UI_ATTR_APPROVED = "approvedStatus";
	private static final String UI_ATTR_VENDORS = "vendors";
	private static final String UI_ATTR_PRODUCTS = "products";
	private static final String UI_ATTR_INV_LOCATIONS = "inventoryLocations";
	private static final String UI_ATTR_IS_APPROVER = "isApprover";
	private static final String UI_ATTR_ID = "id";
	
	@Autowired
	ManufactureOrderValidator manufactureOrderValidator;
	
	@Autowired
	ReceiveOrdersValidator receiveOrdersValidator;

	@Autowired
	ManufactureOrderService manufactureOrderService;
	
	@Autowired
	LoyaltyCardService loyaltyCardService;
	
	@Autowired
	CodePropertiesService codePropertiesService;
	
	@Autowired
	MessageSource messageSource;
	
	@Autowired
	LookupService lookupService;

    @Autowired
    private CounterModelRepo counterModelRepo;
	
	private static final Logger _LOG = LoggerFactory.getLogger(ManufactureOrderController.class);
	
	private static final DateTimeFormatter FORMATTER = DateTimeFormat.forPattern("dd-MM-yyyy");
	
	@InitBinder
	void initBinder(WebDataBinder binder) {
		PropertyEditorSupport editor = new PropertyEditorSupport() {
			@Override
			public void setAsText(String text) throws IllegalArgumentException {
				if(StringUtils.isNotBlank(text))
					setValue(FORMATTER.parseLocalDate(text));
				else
					setValue(null);
			}

			@Override
			public String getAsText() throws IllegalArgumentException {
				if (getValue() != null)
					return FORMATTER.print((LocalDate) getValue());
				else
					return "";
			}
		};
		binder.registerCustomEditor(LocalDate.class, editor);
	}
	
	@RequestMapping(produces = "text/html")
    public String list(Model uiModel) {
		
		populateStatus(uiModel);
		populateSearch(uiModel);
		
		uiModel.addAttribute(UI_ATTR_IS_APPROVER, manufactureOrderService.isApprover());
		
		return "loyalty/mo/list";
    }
	
	@RequestMapping(value="/{id}", produces = "text/html")
    public String list(@PathVariable String id, Model uiModel) {
		
		populateStatus(uiModel);
		populateSearch(uiModel);
		
		uiModel.addAttribute(UI_ATTR_IS_APPROVER, manufactureOrderService.isApprover());
		uiModel.addAttribute(UI_ATTR_ID, id);
		
		return "loyalty/mo/listOne";
    }
	
	@RequestMapping(value = "/view/{id}", method = RequestMethod.POST)
	public @ResponseBody ResultList<ManufactureOrderDto> list(@PathVariable String id) {
		ManufactureOrderDto mo = manufactureOrderService.findDtoById(Long.valueOf(id));
		if(mo != null) {
			return new ResultList<ManufactureOrderDto>(Lists.newArrayList(mo));
		}
		return new ResultList<ManufactureOrderDto>(new ArrayList<ManufactureOrderDto>(), 0L, false, false);
	}
	
	@RequestMapping(value = "/list", method = RequestMethod.POST)
	public @ResponseBody ResultList<ManufactureOrderDto> list(@RequestBody ManufactureOrderSearchDto dto) {
		return manufactureOrderService.listManufactureOrderDto(dto);
	}
	
	@RequestMapping(value = "/modal", method = RequestMethod.GET, produces = "text/html")
    public String dialog(
    		Model uiModel) {
		populateForm(uiModel, initMoDto(new ManufactureOrderDto()));
		return "loyalty/mo/modal";
    }
	
	@RequestMapping(value = "/modal/{id}", method = RequestMethod.GET, produces = "text/html")
    public String dialog(
    		@PathVariable("id") Long id,
    		Model uiModel) {
		populateForm(uiModel, initMoDto(manufactureOrderService.findDtoById(id)));
		return "loyalty/mo/modal";
    }
	
	@RequestMapping(value = "/receivemodal/{id}", method = RequestMethod.GET, produces = "text/html")
    public String receiveDialog(
    		@PathVariable("id") Long id,
    		Model uiModel) {
		ManufactureOrderDto dto = manufactureOrderService.findDtoById(id);
		for(ManufactureOrderTierDto product:dto.getTiers()) {
			manufactureOrderService.populateReceiveCount(product);
		}
		
		uiModel.addAttribute("receivedOrders", loyaltyCardService.getReceivedInventories(id));
		uiModel.addAttribute(UI_ATTR_MO, dto);
		uiModel.addAttribute(UI_ATTR_RO, new ReceiveOrderDto());
		uiModel.addAttribute(UI_ATTR_INV_LOCATIONS, loyaltyCardService.getInventoryLocations());
		return "loyalty/mo/receivemodal";
    }
	
	private ManufactureOrderDto initMoDto(ManufactureOrderDto orderDto) {
		if(CollectionUtils.isEmpty(orderDto.getTiers())) {
			List<ManufactureOrderTierDto> tierDtos = new ArrayList<ManufactureOrderTierDto>();
			tierDtos.add(new ManufactureOrderTierDto());
			orderDto.setTiers(tierDtos);	
		}
        return orderDto;
	}
	
	private void populateForm(Model uiModel, ManufactureOrderDto orderDto) {
		uiModel.addAttribute(UI_ATTR_MO, orderDto);
		uiModel.addAttribute(UI_ATTR_VENDORS, manufactureOrderService.getVendors());
		uiModel.addAttribute(UI_ATTR_PRODUCTS, lookupService.getActiveDetails(lookupService.getHeader(codePropertiesService.getHeaderMembershipTier())));
		uiModel.addAttribute(UI_ATTR_IS_APPROVER, manufactureOrderService.isApprover());
        List<CodeDescDto> companies = Lists.newArrayList();
        List<LookupDetail> comDtls = lookupService.getActiveDetailsByHeaderCode(codePropertiesService.getHeaderCompany());
        if (comDtls != null && comDtls.size() > 0) {
            for (LookupDetail dtl : comDtls) {
                companies.add(new CodeDescDto(dtl.getCode(), dtl.getDescription()));
            }
        }
        uiModel.addAttribute("companies", companies);
		populateStatus(uiModel);
	}
	
	private void populateStatus(Model uiModel) {
		uiModel.addAttribute(UI_ATTR_DRAFTED, codePropertiesService.getDetailMoStatusDrafted());
		uiModel.addAttribute(UI_ATTR_FOR_APPROVAL, codePropertiesService.getDetailMoStatusForApproval());
		uiModel.addAttribute(UI_ATTR_APPROVED, codePropertiesService.getDetailMoStatusApproved());
		uiModel.addAttribute("barcodingStatus", codePropertiesService.getDetailMoStatusBarcoding());
		uiModel.addAttribute("receivingStatus", codePropertiesService.getDetailMoStatusReceiving());
	}
	
	private void populateSearch(Model uiModel) {
		uiModel.addAttribute("status", lookupService.getDetailsByHeaderCode(codePropertiesService.getHeaderMoStatus()));
		uiModel.addAttribute("criterias", MoSearchCriteria.values());
		uiModel.addAttribute(UI_ATTR_VENDORS, manufactureOrderService.getVendors().getResults());
	}
	
	public static enum MoSearchCriteria {
		MO("moNo", "MO Number"),
		PO("poNo", "PO Number"),
		MPO("mpoNo", "MO/PO Number");
		private final String field;
		private final String name;
		
		private MoSearchCriteria(String field, String name) {
			this.field = field;
			this.name = name;
		}

		public String getField() {
			return field;
		}

		public String getName() {
			return name;
		}
	}
 	
//	@NotificationUpdate
//	@RequestMapping(value="/save/MOST003", method=RequestMethod.POST, produces="application/json; charset=utf-8")
//	@ResponseBody
//	public ControllerResponse ajaxApprove(
//			@ModelAttribute(value = UI_ATTR_MO) ManufactureOrderDto orderDto,
//			BindingResult result, HttpServletRequest request) {
//		LookupDetail status = lookupService.getDetailByCode(codePropertiesService.getDetailMoStatusApproved());
//		return ajaxSave(status, orderDto, result, request);
//	}
	
//	@NotificationUpdate
//	@RequestMapping(value="/save/MOST003/{id}", method=RequestMethod.POST, produces="application/json; charset=utf-8")
//	@ResponseBody
//	public ControllerResponse ajaxApprove(
//			@PathVariable Long id,
//			@ModelAttribute(value = UI_ATTR_MO) ManufactureOrderDto orderDto,
//			BindingResult result, HttpServletRequest request) {
//		LookupDetail status = lookupService.getDetailByCode(codePropertiesService.getDetailMoStatusApproved());
//		return ajaxSave(id, status, orderDto, result, request);
//	}
	
	@Notifiable(type=NotificationType.MANUFACTURE_ORDER)
	@RequestMapping(value = "/save/{status}", method = RequestMethod.POST, produces = "application/json; charset=utf-8")
	@ResponseBody
	public ControllerResponse ajaxSave(
			@PathVariable("status") LookupDetail status,
			@ModelAttribute(value = UI_ATTR_MO) ManufactureOrderDto orderDto,
			BindingResult result, HttpServletRequest request) {

		ControllerResponse theResponse = new ControllerResponse();
		theResponse.setSuccess(true);
		
		ValidationUtils.invokeValidator(manufactureOrderValidator, orderDto, result);

		if (result.hasErrors()) {
			theResponse.setSuccess(false);
			theResponse.setResult(result.getAllErrors());
			return theResponse;
		}

		if (theResponse.isSuccess()) {
			orderDto.setStatus(status);
			manufactureOrderService.saveMoDto(orderDto);
			theResponse.setModelId(orderDto.getId().toString());
			theResponse.setResult(orderDto.getMoNo());
			
			if(status.getCode().equals(codePropertiesService.getDetailMoStatusForApproval())) {
				theResponse.setNotificationCreation(true);
			}
			
			if(status.getCode().equals(codePropertiesService.getDetailMoStatusBarcoding())) {
				loyaltyCardService.generateLoyaltyInventory(orderDto);
			}
		}

		return theResponse;
	}
	
	@Notifiable(type=NotificationType.MANUFACTURE_ORDER)
	@NotificationUpdate(type=NotificationType.MANUFACTURE_ORDER)
	@RequestMapping(value = "/save/{status}/{id}", method = RequestMethod.POST, produces = "application/json; charset=utf-8")
	@ResponseBody
	public ControllerResponse ajaxSave(
			@PathVariable("id") Long id,
			@PathVariable("status") LookupDetail status,
			@ModelAttribute(value = UI_ATTR_MO) ManufactureOrderDto orderDto,
			BindingResult result, HttpServletRequest request) {

		ControllerResponse theResponse = new ControllerResponse();
		theResponse.setSuccess(true);
		
		ValidationUtils.invokeValidator(manufactureOrderValidator, orderDto, result);

		if (result.hasErrors()) {
			theResponse.setSuccess(false);
			theResponse.setResult(result.getAllErrors());
			return theResponse;
		}

		if (theResponse.isSuccess()) {
			orderDto.setStatus(status);
			manufactureOrderService.updateMoDto(orderDto);
			theResponse.setModelId(id.toString());
			theResponse.setResult(orderDto.getMoNo());
			theResponse.setNotificationCreation(
					status.getCode().equals(codePropertiesService.getDetailMoStatusForApproval()));
			
			if(status.getCode().equals(codePropertiesService.getDetailMoStatusBarcoding())) {
				loyaltyCardService.generateLoyaltyInventory(orderDto);
			}
		}

		return theResponse;
	}
	
	@RequestMapping(value = "/receive/{id}", method = RequestMethod.POST, produces = "application/json; charset=utf-8")
	@ResponseBody
	public ControllerResponse ajaxReceive(
			@PathVariable("id") Long id,
			@ModelAttribute(value = UI_ATTR_RO) ReceiveOrderDto orderDto,
			BindingResult result, HttpServletRequest request) {

		ControllerResponse theResponse = new ControllerResponse();
		theResponse.setSuccess(true);
		orderDto.setManufactureOrder(id);
		
		ValidationUtils.invokeValidator(receiveOrdersValidator, orderDto, result);
		
		if (result.hasErrors()) {
			theResponse.setSuccess(false);
			theResponse.setResult(result.getAllErrors());
			return theResponse;
		}

		if (theResponse.isSuccess()) {
			/*manufactureOrderService.updateMoStatus( orderDto.getManufactureOrder(), new LookupDetail( codePropertiesService.getDetailMoStatusReceiving() ) );*/
			loyaltyCardService.receiveOrders(orderDto);
		}

		return theResponse;
	}
	
	@RequestMapping(value = "/printreceive/{id}")
	public void printReceive(
			@PathVariable("id") Long id,
			@ModelAttribute(value = UI_ATTR_RO) ReceiveOrderDto orderDto,
			HttpServletResponse response) throws Exception {
		orderDto.setManufactureOrder(id);
		JRProcessor jrProcessor = manufactureOrderService.createJrProcessor(orderDto);
		renderReport(response, jrProcessor);
	}
	
	@NotificationUpdate(type=NotificationType.MANUFACTURE_ORDER)
	@RequestMapping(value = "/{id}", method = RequestMethod.DELETE, produces = "text/html")
    public ModelAndView delete(@PathVariable("id") Long id,
            @RequestParam(value = "page", required = false) Integer page,
            @RequestParam(value = "size", required = false) Integer size,
            Model uiModel) {
        manufactureOrderService.deleteMo(id);
        uiModel.asMap().clear();
        uiModel.addAttribute("page", (page == null) ? "1" : page.toString());
        uiModel.addAttribute("size", (size == null) ? "10" : size.toString());
        return new ModelAndView("redirect:/manufactureorder").addObject("itemId", id.toString());
    }
	
	@RequestMapping(value = "/printmanufactureorder/{moNo}")
	public void printMo(@PathVariable String moNo,
			HttpServletResponse response) throws Exception {
		JRProcessor jrProcessor = manufactureOrderService.createJrProcessor(manufactureOrderService.findDtoByMoNo(moNo));
		renderReport(response, jrProcessor);
	}

	@RequestMapping(value = "/printreceiveall/{moNo}")
	public void printReceiveAll(
			@PathVariable String moNo,
			HttpServletResponse response) throws Exception {
		JRProcessor jrProcessor = manufactureOrderService.createJrProcessorReceiveAll(manufactureOrderService.findDtoByMoNo(moNo));
		renderReport(response, jrProcessor);
	}

    @RequestMapping(value = "/lastseriesno/{comCode}/{cardTypeCode}", method = RequestMethod.GET)
    @ResponseBody
    public int getLastSeriesNo(@PathVariable("comCode") String companyCode, @PathVariable("cardTypeCode") String cardTypeCode) {
        CounterModel counterModel = counterModelRepo.findOne(CounterModel.PRFX_LCNO_COUNTER + companyCode + cardTypeCode);
        return counterModel == null ? 0 : counterModel.getValue().intValue();
    }
}
