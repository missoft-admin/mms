alter table CRM_MEMBER alter column ZONE varchar(255);
alter table CRM_MEMBER alter column RADIUS varchar(255);
alter table CRM_ADDRESS alter column RT varchar(255);
alter table CRM_ADDRESS alter column RW varchar(255);