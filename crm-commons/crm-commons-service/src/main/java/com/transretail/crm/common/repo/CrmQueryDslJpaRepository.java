package com.transretail.crm.common.repo;

import java.io.Serializable;

import javax.persistence.EntityManager;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.support.JpaEntityInformation;
import org.springframework.data.jpa.repository.support.QueryDslJpaRepository;
import org.springframework.data.jpa.repository.support.Querydsl;
import org.springframework.data.querydsl.EntityPathResolver;
import org.springframework.data.querydsl.SimpleEntityPathResolver;

import com.mysema.query.JoinType;
import com.mysema.query.jpa.JPQLQuery;
import com.mysema.query.types.EntityPath;
import com.mysema.query.types.Predicate;
import com.mysema.query.types.path.PathBuilder;

/**
 * @author Allan G. Ramirez (agramirez@exist.com)
 */
public class CrmQueryDslJpaRepository<T, ID extends Serializable> extends QueryDslJpaRepository<T, ID>
    implements CrmQueryDslPredicateExecutor<T, ID> {
    private static final EntityPathResolver DEFAULT_ENTITY_PATH_RESOLVER = SimpleEntityPathResolver.INSTANCE;

    private final EntityPath<T> path;
    private final PathBuilder<T> builder;
    private final Querydsl querydsl;

    /**
     * {@inheritDoc}
     */
    public CrmQueryDslJpaRepository(JpaEntityInformation<T, ID> entityInformation, EntityManager entityManager) {
        this(entityInformation, entityManager, DEFAULT_ENTITY_PATH_RESOLVER);
    }

    /**
     * {@inheritDoc}
     */
    public CrmQueryDslJpaRepository(JpaEntityInformation<T, ID> entityInformation, EntityManager entityManager,
        EntityPathResolver resolver) {
        super(entityInformation, entityManager, resolver);

        this.path = resolver.createPath(entityInformation.getJavaType());
        this.builder = new PathBuilder<T>(path.getType(), path.getMetadata());
        this.querydsl = new Querydsl(entityManager, builder);
    }

    /**
     * This method allows null {@link Pageable} unlike {@link QueryDslJpaRepository#findAll(com.mysema.query.types.Predicate, org.springframework.data.domain.Pageable)}
     *
     * @param predicate
     * @param pageable
     * @return
     * @see org.springframework.data.querydsl.QueryDslPredicateExecutor#findAll(com.mysema.query.types.Predicate, org.springframework.data.domain.Pageable)
     */
    @Override
    public Page<T> findAll(Predicate predicate, Pageable pageable) {
        Page<T> result = null;
        JPQLQuery countQuery = createQuery(predicate);
        JPQLQuery selectQuery = createQuery(predicate);

        if (pageable != null) {
            Long total = countQuery.count();
            if (pageable.getPageSize() >= 0 && pageable.getPageNumber() >= 0) {
                selectQuery = querydsl.applyPagination(pageable, selectQuery);
            } else {
                selectQuery = querydsl.applySorting(pageable.getSort(), selectQuery);
            }
            result = new PageImpl<T>(selectQuery.list(path), pageable, total);
        } else {
            result = new PageImpl<T>(selectQuery.list(path));
        }
        return result;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Page<T> findAll(Predicate predicate, Pageable pageable, JoinDef... joinDefs) {
        Page<T> result = null;
        JPQLQuery countQuery = createQuery(predicate);
        JPQLQuery selectQuery = createQuery(predicate);

        if (pageable != null) {
            Long total = addJoins(countQuery, joinDefs).count();
            if (pageable.getPageSize() >= 0 && pageable.getPageNumber() >= 0) {
                selectQuery = querydsl.applyPagination(pageable, selectQuery);
            } else {
                selectQuery = querydsl.applySorting(pageable.getSort(), selectQuery);
            }
            selectQuery = addJoins(selectQuery, joinDefs);
            result = new PageImpl<T>(selectQuery.list(path), pageable, total);
        } else {
            selectQuery = addJoins(selectQuery, joinDefs);
            result = new PageImpl<T>(selectQuery.list(path));
        }
        return result;
    }

    @Override
    public Page<T> findAll(Pageable pageable) {
        Page<T> result = null;
        if (pageable != null) {
            JPQLQuery selectQuery = createQuery();
            JPQLQuery countQuery = createQuery();

            Long total = countQuery.count();
            if (pageable.getPageSize() >= 0 && pageable.getPageNumber() >= 0) {
                selectQuery = querydsl.applyPagination(pageable, selectQuery);
            } else {
                selectQuery = querydsl.applySorting(pageable.getSort(), selectQuery);
            }
            result = new PageImpl<T>(selectQuery.list(path), pageable, total);
        } else {
            JPQLQuery selectQuery = createQuery();
            result = new PageImpl<T>(selectQuery.list(path));
        }
        return result;
    }

    private JPQLQuery addJoins(JPQLQuery query, JoinDef... joinDefs) {
        if (joinDefs != null) {
            for (JoinDef joinDef : joinDefs) {
                if (joinDef.getJoinType() == JoinType.LEFTJOIN) {
                    if (joinDef.getAlias() != null) {
                        query.leftJoin(joinDef.getTarget(), joinDef.getAlias());
                    } else {
                        query.leftJoin(joinDef.getTarget());
                    }
                } else if (joinDef.getJoinType() == JoinType.LEFTJOIN) {
                    if (joinDef.getAlias() != null) {
                        query.rightJoin(joinDef.getTarget(), joinDef.getAlias());
                    } else {
                        query.rightJoin(joinDef.getTarget());
                    }
                } else if (joinDef.getJoinType() == JoinType.INNERJOIN) {
                    if (joinDef.getAlias() != null) {
                        query.innerJoin(joinDef.getTarget(), joinDef.getAlias());
                    } else {
                        query.innerJoin(joinDef.getTarget());
                    }
                } else if (joinDef.getJoinType() == JoinType.FULLJOIN) {
                    if (joinDef.getAlias() != null) {
                        query.fullJoin(joinDef.getTarget(), joinDef.getAlias());
                    } else {
                        query.fullJoin(joinDef.getTarget());
                    }
                } else if (joinDef.getJoinType() == JoinType.JOIN) {
                    if (joinDef.getAlias() != null) {
                        query.join(joinDef.getTarget(), joinDef.getAlias());
                    } else {
                        query.join(joinDef.getTarget());
                    }
                }
            }
        }
        return query;
    }

    /**
     * Creates a new {@link JPQLQuery} for the given {@link Predicate}.
     *
     * @param predicate nullable predicate
     * @return the Querydsl {@link JPQLQuery}.
     */
    protected JPQLQuery createQuery(Predicate... predicate) {
        if (predicate != null) {
            return querydsl.createQuery(path).where(predicate);
        } else {
            return querydsl.createQuery(path);
        }
    }
}
