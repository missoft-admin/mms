package com.transretail.crm.giftcard.service.impl;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.hibernate.StatelessSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.google.common.collect.Lists;
import com.mysema.query.jpa.hibernate.HibernateUpdateClause;
import com.mysema.query.types.expr.BooleanExpression;
import com.transretail.crm.common.service.util.SpringDataPagingUtil;
import com.transretail.crm.giftcard.dto.CardVendorDto;
import com.transretail.crm.giftcard.dto.CardVendorResultList;
import com.transretail.crm.giftcard.dto.CardVendorSearchDto;
import com.transretail.crm.giftcard.entity.CardVendor;
import com.transretail.crm.giftcard.entity.QCardVendor;
import com.transretail.crm.giftcard.repo.CardVendorRepo;
import com.transretail.crm.giftcard.service.CardVendorService;

/**
 *
 */
@Service("cardVendorService")
public class CardVendorServiceImpl implements CardVendorService {
    @Autowired
    private CardVendorRepo repo;
    @Autowired
    private StatelessSession statelessSession;
    @PersistenceContext
    private EntityManager em;

    @Override
    @Transactional
    public Long saveCardVendor(CardVendorDto cardVendor) {
    	Long cardVendorId = repo.save(cardVendor.toModel()).getId();
    	if(cardVendor.getDefaultVendor()) {
    		uncheckDefaultOtherVendors(cardVendorId);
    	}
        return cardVendorId;
    }

    @Override
    @Transactional
    public void updateCardVendor(Long cardVendorId, CardVendorDto cardVendor) {
        CardVendor vendor = repo.findOne(cardVendorId);
        cardVendor.update(vendor);
        repo.save(vendor);
        
        if(cardVendor.getDefaultVendor()) {
    		uncheckDefaultOtherVendors(cardVendorId);
    	}
    }
    
    @Transactional
    private long uncheckDefaultOtherVendors(Long cardVendorId) {
    	QCardVendor qVendor = QCardVendor.cardVendor;
    	HibernateUpdateClause clause = new HibernateUpdateClause(statelessSession, qVendor)
        .set(qVendor.defaultVendor, Boolean.FALSE);
    	return clause.where(qVendor.defaultVendor.isTrue().and(qVendor.id.ne(cardVendorId))).execute();
    }

    @Override
    @Transactional(readOnly = true)
    public CardVendorDto getCardVendorById(Long vendorId) {
        return new CardVendorDto(repo.findOne(vendorId));
    }

    @Override
    @Transactional(readOnly = true)
    public com.transretail.crm.giftcard.dto.CardVendorResultList getCardVendors(CardVendorSearchDto searchForm) {
        Pageable pageable = SpringDataPagingUtil.INSTANCE.createPageable(searchForm.getPagination());
        BooleanExpression filter = searchForm.createSearchExpression();
        Page<CardVendor> page = filter != null ? repo.findAll(filter, pageable) : repo.findAll(pageable);
        return new CardVendorResultList(toCardVendorDto(page.getContent()), page.getTotalElements(), page.hasPreviousPage(),
            page.hasNextPage());
    }

    private List<CardVendorDto> toCardVendorDto(Collection<CardVendor> vendors) {
        List<CardVendorDto> result = Lists.newArrayList();
        for (CardVendor vendor : vendors) {
            result.add(new CardVendorDto(vendor));
        }
        return result;
    }

    @Override
    public List<String> getAllCardVendorsByFormalName() {
        QCardVendor qModel = QCardVendor.cardVendor;
        Iterable<CardVendor> cardVendors = repo.findAll(qModel.enabled.eq(true));
        List<String> cardVendorFormalNames = new ArrayList<String>();
        
        if (!cardVendors.iterator().hasNext())
            return new ArrayList<String>();
        
        for (CardVendor cardVendor : cardVendors) {
            cardVendorFormalNames.add(cardVendor.getFormalName());
        }
        
        return cardVendorFormalNames;
    }
}
