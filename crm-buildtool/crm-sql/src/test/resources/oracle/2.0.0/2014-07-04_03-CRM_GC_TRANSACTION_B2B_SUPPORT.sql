alter table crm_gc_transaction
  modify CASHIER_ID varchar2(12 char) null;
alter table crm_gc_transaction
  modify TERMINAL_ID varchar2(12 char) null;
alter table crm_gc_transaction
  modify MERCHANT_ID varchar2(255 char) null;