package com.transretail.crm.giftcard.service.impl;


import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.google.common.collect.Maps;
import com.mysema.query.BooleanBuilder;
import com.mysema.query.jpa.impl.JPAQuery;
import com.mysema.query.types.Predicate;
import com.mysema.query.types.expr.BooleanExpression;
import com.transretail.crm.common.service.dto.request.PageSortDto;
import com.transretail.crm.common.service.dto.response.ResultList;
import com.transretail.crm.common.service.util.SpringDataPagingUtil;
import com.transretail.crm.core.dto.ApprovalRemarkDto;
import com.transretail.crm.core.entity.ApprovalRemark;
import com.transretail.crm.core.entity.QApprovalRemark;
import com.transretail.crm.core.entity.enums.ModelType;
import com.transretail.crm.core.repo.ApprovalRemarkRepo;
import com.transretail.crm.core.service.CodePropertiesService;
import com.transretail.crm.core.service.LookupService;
import com.transretail.crm.giftcard.dto.GiftCardCxProfileDto;
import com.transretail.crm.giftcard.dto.GiftCardCxProfileSearchDto;
import com.transretail.crm.giftcard.entity.GiftCardCustomerProfile;
import com.transretail.crm.giftcard.entity.QGiftCardCustomerProfile;
import com.transretail.crm.giftcard.repo.GiftCardCustomerProfileRepo;
import com.transretail.crm.giftcard.service.GiftCardCxProfileService;


@Service("giftCardCxProfileService")
public class GiftCardCxProfileServiceImpl implements GiftCardCxProfileService {

	private GiftCardCustomerProfileRepo repo;
	private ApprovalRemarkRepo approvalRemarkRepo;
	private LookupService lookupService;
	private CodePropertiesService codePropertiesService;

	@PersistenceContext
	private EntityManager em;

	@Autowired
    public void setRepo(GiftCardCustomerProfileRepo repo) {
        this.repo = repo;
    }
    @Autowired
    public void setApprovalRemarkRepo(ApprovalRemarkRepo approvalRemarkRepo) {
        this.approvalRemarkRepo = approvalRemarkRepo;
    }
    @Autowired
    public void setLookupService(LookupService lookupService) {
        this.lookupService = lookupService;
    }
    @Autowired
    public void setCodePropertiesService(CodePropertiesService codePropertiesService) {
        this.codePropertiesService = codePropertiesService;
    }

	@Override
	public GiftCardCxProfileDto getDto(Long id) {
        return new GiftCardCxProfileDto(repo.findOne(id));
	}

	@Transactional
	@Override
	public GiftCardCxProfileDto saveDto(GiftCardCxProfileDto dto) {

	    GiftCardCustomerProfile profile = dto.toModel();

	    Long no = generateCustId();
        profile.setCustomerId(String.valueOf(no + 1));
	    repo.save(profile);
		return new GiftCardCxProfileDto( profile );
	}

	@Override
	public GiftCardCxProfileDto updateDto(GiftCardCxProfileDto dto) {
		if ( null == dto.getId() ) {
			return dto;
		}

		GiftCardCustomerProfile model = repo.findOne( dto.getId() );
		return new GiftCardCxProfileDto( repo.save( dto.toUpdatedModel( model ) ) );
	}

	@Override
	public ResultList<GiftCardCxProfileDto> search( GiftCardCxProfileSearchDto dto ) {
		Map<String, String> sortMap = Maps.newHashMap();
		sortMap.put("contactFullName", "contactFirstName");
		Pageable pageable = SpringDataPagingUtil.INSTANCE.createPageable( dto.getPagination(), sortMap );
        BooleanExpression filter = dto.createSearchExpression();
        Page<GiftCardCustomerProfile> page = ( filter != null )? repo.findAll( filter, pageable ) : repo.findAll( pageable );
        return new ResultList<GiftCardCxProfileDto>( GiftCardCxProfileDto.toDtoList( page.getContent() ), 
        		page.getTotalElements(), page.getNumber(), page.getSize() );
	}

	@Override
	@Transactional(readOnly = true)
	public List<GiftCardCxProfileDto> getAllProfiles() {
		return GiftCardCxProfileDto.toDtoList(repo.findAll());
	}

	@Override
	@Transactional(readOnly = true)
	public List<GiftCardCxProfileDto> getProfiles( GiftCardCxProfileSearchDto dto ) {
		ResultList<GiftCardCxProfileDto> dtos = search( dto );
		return (List<GiftCardCxProfileDto>) dtos.getResults();
	}

	@Override
	public void saveCustomerNotes( Long cxProfileId, String notes ) {
		if ( null == cxProfileId || StringUtils.isBlank( notes ) ) {
			return;
		}
		ApprovalRemark model = new ApprovalRemark();
		model.setModelId( cxProfileId.toString() );
		model.setModelType( ModelType.CXPROFILE_NOTES.name() );
		model.setRemarks( notes );
		approvalRemarkRepo.save( model );
	}

	@Override
	public ResultList<ApprovalRemarkDto> searchNotes( Long id, PageSortDto searchDto ) {
    	if ( null == id ) {
    		new ResultList<ApprovalRemarkDto>( new ArrayList<ApprovalRemarkDto>() );
    	}

    	QApprovalRemark qModel = QApprovalRemark.approvalRemark;
		Pageable pageable = SpringDataPagingUtil.INSTANCE.createPageable( searchDto.getPagination() );
        Page<ApprovalRemark> page = approvalRemarkRepo.findAll( 
        		qModel.modelType.equalsIgnoreCase( ModelType.CXPROFILE_NOTES.toString() )
        			.and( qModel.modelId.equalsIgnoreCase( id.toString() ) ), 
        		pageable );
        return new ResultList<ApprovalRemarkDto>( ApprovalRemarkDto.toDtoList( page.getContent() ), 
        		page.getTotalElements(), page.getNumber(), page.getSize() );
	}

	@Transactional
    @Override
    public Long generateCustId() {

	    Long id = 0L;
        List<GiftCardCustomerProfile> resultList = em.createQuery("SELECT p FROM com.transretail.crm.giftcard.entity.GiftCardCustomerProfile p order by p.created desc",
                        GiftCardCustomerProfile.class)
                .setMaxResults(1)
                .getResultList();

        if (resultList.isEmpty())
            return id;
        else {
            return Long.parseLong(resultList.get(0).getCustomerId());
        }
    }
}