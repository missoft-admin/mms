package com.transretail.crm.core.service.impl;

import com.transretail.crm.core.util.generator.IdGeneratorService;

import junit.framework.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Configurable;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

@Configurable
@ContextConfiguration(locations = "classpath*:/META-INF/spring/applicationContext*.xml")
@Transactional
@RunWith(SpringJUnit4ClassRunner.class)
public class PinGeneratorServiceTest {

    @Autowired
    IdGeneratorService pinGeneratorService;


    @Test
    public void testGenerateId() {

        String pin = pinGeneratorService.generateId();
        Assert.assertEquals(6, pin.length());

    }



}
