package com.transretail.crm.common.audit.service;

/**
 * @author Allan G. Ramirez (agramirez@exist.com)
 */
public interface EnrollmentService {
    void enroll(Student student);
}
