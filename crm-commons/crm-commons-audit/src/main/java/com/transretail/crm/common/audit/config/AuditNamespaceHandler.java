package com.transretail.crm.common.audit.config;

import org.springframework.beans.factory.xml.NamespaceHandlerSupport;

/**
 * @author Allan G. Ramirez (agramirez@exist.com)
 */
public class AuditNamespaceHandler extends NamespaceHandlerSupport {
    @Override
    public void init() {
        registerBeanDefinitionParser("annotation-driven", new AuditBeanDefinitionParser());
    }
}
