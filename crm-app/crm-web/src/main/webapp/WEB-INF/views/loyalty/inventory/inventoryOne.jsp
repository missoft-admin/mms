<%@ include file="../../common/taglibs.jsp" %>
<%@ taglib prefix="crm" uri="http://www.carrefour.com/crm/permission/tags" %>

<sec:authorize var="isMerchantSupervisor" ifAnyGranted="ROLE_MERCHANT_SERVICE_SUPERVISOR" />
<sec:authentication var="userLocationCode" property="principal.inventoryLocation" />

<style type="text/css">
<!--
  #content_inventoryList .dataTable tr td:last-child {
    min-width: 180px;
  }
  .well .checkbox {
    margin-left:0;
    width: 134px;
  }
  #inventoryModal .table-compressed tr td:last-child {
    border-left: 0px;
  }

  #inventoryBigModal .table-compressed tr td:last-child {
    border-left: 0px;
  }
  .well .input {
    background-color: #fff;
    border: 1px solid #E8E8E8;
    box-shadow: none;
    padding: 6px;
    height: 18px;
    margin-left: 5px;
  }
  .well .btn-primary {
    margin-left: 5px;
  }
  .well select {
    margin: 0;
  }
  .well .input-group {
    margin: 5px;
  }
  .checkbox {
    margin: 0 0 0 20px;
  }

-->
</style>

<div class="page-header page-header2">
    <h1><spring:message code="label_loyalty_card_inventory" /></h1>
</div>

<div id="inventorySearchForm" class="hide well clearfix">

    <div class="pull-left" style="margin-left:0; width: 100%">
        <div class= "pull-left input-group" style="margin-left:0">
          <spring:message code="label_manufacture_order" var="mo" />
          <label for="">${mo}</label>
          <input type="text" class="input-medium" name="manufactureOrder"  placeholder="${mo}" />
        </div>

        <div class="pull-left input-group">
          <spring:message code="mo_tier_prop_name" var="moProduct" />
          <label for="">${moProduct}</label>
          <select name="manufactureOrderTier" class="span2">
            <option value=""></option>
          <c:forEach items="${products}" var="item" >
            <option value="${item.code}" >${item.description}</option>
          </c:forEach>
          </select>
        </div>

        <div class="pull-left input-group">
          <spring:message code="inventory_prop_location" var="location" />
          <label for="">${location}</label>
          <select name="location" class="span2">
            <option value=""></option>
          <c:forEach items="${inventoryLocations}" var="item" >
            <option value="${item.key}" >${item.key} - ${item.value}</option>
          </c:forEach>
          </select>
        </div>

        <div class="pull-left input-group">
          <spring:message code="inventory_prop_transferto" var="transferTo" />
          <label for="">${transferTo}</label>
          <select name="transferTo" class="span2">
            <option value=""></option>
          <c:forEach items="${inventoryLocations}" var="item" >
            <option value="${item.key}" >${item.key} - ${item.value}</option>
          </c:forEach>
          </select>
        </div>

        <div class="input-group">
          <div class="pull-left">
          <label for="">&nbsp;</label>
          <input type="button" id="inventorySearchButton" value="<spring:message code="label_search" />" class="btn btn-primary"/>
          </div>

          <div class=" pull-left" style="margin-left: 3px;">
          <label for="">&nbsp;</label>
          <input type="button" id="inventoryClearButton" value="<spring:message code="label_clear" />" class="btn "/>
          </div>
        </div>
    



        <div class="clearfix"></div>

        <div style="margin-left:-0px; max-width: 850px" class="clearfix">
        <c:forEach var="item" items="${loyaltyCardStatus}">
          <div class="span checkbox">
          <input type="checkbox" class="statusChk" value="${item.code}" />${item.description}
          </div>
        </c:forEach>
        
          <div class="span checkbox">
          <input type="checkbox" class="statusChk" value="changeAllocation" /><spring:message code="loyalty_status_changeallocation" />
          </div>
          
          <div class="span checkbox">
          <input type="checkbox" class="statusChk" value="forBurning" /><spring:message code="loyalty_status_for_burning" />
          </div>
        </div>
    </div>



    <input type="hidden" name="status" />

    <br/>

</div>

<div class="pull-left hide">
<input type="button" id="reprintTxnBtn" value="<spring:message code="label_reprint_transactions" />" class="btn btn-primary"/>
<button type="button" class="btn btn-print" id="reprintTxnBtn"><spring:message code="label_reprint_transactions" /></button>
</div>

<div class="pull-right hide">
<input type="button" id="showCardFinder" value="<spring:message code="label_find_card" />" class="btn btn-primary"/>
<input type="button" id="multipleAllocationBtn" data-url="<spring:url value="/loyaltycardinventory/multipleallocation" />" value="<spring:message code="label_multiple_allocation" />" class="btn btn-primary"/>
<c:if test="${headOfficeLocation == userLocationCode}">
<input type="button" id="multipleApprovalBtn" data-url="<spring:url value="/loyaltycardinventory/multipleapproval" />" value="<spring:message code="label_multiple_approval" />" class="btn btn-primary"/>
<crm:permission hasPermissions="LC_APPROVE_BURN_CARDS">
<input type="button" id="burnApprovalBtn" data-url="<spring:url value="/loyaltycardinventory/burnapproval" />" value="<spring:message code="label_burn_cards_approval" />" class="btn btn-primary"/>
</crm:permission>
<crm:permission hasPermissions="LC_BURN_CARDS">
<input type="button" id="burnCardBtn" data-url="<spring:url value="/loyaltycardinventory/burn" />" value="<spring:message code="label_burn_cards" />" class="btn"/>
</crm:permission>
</c:if>
</div>

<div class="clearfix"></div><br/>

<jsp:include page="cardFinder.jsp" />
<div id="content_inventoryList">

  
  <div id="list_inventory"
    data-url="<c:url value="/loyaltycardinventory/search/${id}" />"
    data-hdr-product="<spring:message code="mo_tier_prop_name" />"
    data-hdr-status="<spring:message code="mo_prop_status" />"
    data-hdr-location="<spring:message code="inventory_prop_location" />"
    data-hdr-transfer-to="<spring:message code="inventory_prop_transferto" />"
    data-hdr-count="<spring:message code="inventory_prop_count" />"
    data-hdr-lastupdate="<spring:message code="inventory_prop_lastupdatedate" />"
    data-status-inactive="${inactive}"
    data-status-for-allocation="${forAllocation}"
    data-status-allocated="${allocated}"
    data-status-in-transit="${inTransit}"
    data-status-transfer-in="${transferIn}"
    data-status-found="${found}"
    data-location-head-office="${headOfficeLocation}"
    data-is-merchant-supervisor="${isMerchantSupervisor}"
    data-user-location="${userLocationCode}"
    data-changefrom-msg="<spring:message code="changealloc_msg_from" />"
    data-changeto-msg="<spring:message code="changealloc_msg_to" />"
    data-status-transfer-out="<spring:message code="loyalty_status_transferout" />"
    data-status-for-burning="<spring:message code="loyalty_status_for_burning" />"
    ></div>

</div>

<div id="buttonContainer" class="hide">
  <div id="viewItemsBtnContainer">
    <input type="button" data-url="<c:url value="/loyaltycardinventory/viewitems" />/%MOTIER%/%STATUS%/%LOCATION%/%TRANSFERTO%/%NEWTRANSFERTO%/%FORBURNING%/%MONO%" value="<spring:message code="label_view" />" class="btn modalBtn icn-view tiptip pull-left" title="<spring:message code="label_view" />"/>
  </div>
  <div id="allocateBtnContainer">
    <input type="button" data-url="<c:url value="/loyaltycardinventory/allocate" />/%LOCATION%" value="<spring:message code="label_allocate" />" class="btn modalBtn icn-allocate tiptip pull-left" title="<spring:message code="label_allocate" />" />
  </div>
  <div id="approveAllocBtnContainer">
    <input type="button" data-url="<c:url value="/loyaltycardinventory/approvealloc" />/%MOTIER%/%STATUS%/%LOCATION%/%TRANSFERTO%" value="<spring:message code="label_approve_allocate" />" class="btn modalBtn icn-approve tiptip pull-left" title="<spring:message code="label_approve_allocate" />" />
  </div>
  <div id="transferBtnContainer">
    <input type="button" data-url="<c:url value="/loyaltycardinventory/transfer" />/%MOTIER%/%STATUS%/%LOCATION%/%TRANSFERTO%" value="<spring:message code="label_transfer" />" class="btn modalBtn icn-transfer tiptip pull-left" title="<spring:message code="label_transfer" />" />
  </div>
  <div id="receiveBtnContainer">
    <input type="button" data-url="<c:url value="/loyaltycardinventory/receive" />/%MOTIER%/%STATUS%/%LOCATION%/%TRANSFERTO%" value="<spring:message code="label_receive" />" class="btn modalBtn icn-recieve tiptip pull-left" title="<spring:message code="label_receive" />" />
  </div>
  <div id="reprintBtnContainer">
    <input type="button" data-url="<c:url value="/loyaltycardinventory/reprinttransfer" />/%MOTIER%/%STATUS%/%LOCATION%/%TRANSFERTO%" value="<spring:message code="label_lc_transfer_reprint" />" class="btn reprintBtn icn-print tiptip pull-left" title="<spring:message code="label_lc_transfer_reprint" />" />
  </div>
  <div id="changeAllocationBtnContainer">
    <input type="button" data-url="<c:url value="/loyaltycardinventory/approvechangealloc" />/%MOTIER%/%STATUS%/%LOCATION%/%TRANSFERTO%/%NEWTRANSFERTO%" value="<spring:message code="label_approve_changeallocate" />" class="btn modalBtn icn-approve tiptip pull-left" title="<spring:message code="label_approve_changeallocate" />" />
  </div>
</div>

<div class="modal hide  groupModal nofly" tabindex="-1" role="dialog" aria-hidden="true" id="inventoryModal"></div>
<div class="modal hide  groupModal nofly" tabindex="-1" role="dialog" aria-hidden="true" id="inventoryBigModal"></div>
<jsp:include page="../../common/confirm.jsp" />
<jsp:include page="reprinttransactions.jsp" />

  <link href="<spring:url value="/css/dataTables.css" />" rel="stylesheet">
  <script src="<spring:url value="/js/jquery/jquery.dataTables.js" />" ></script>
  <script src="<spring:url value="/js/jquery/jquery.dataTables.linkLengthMenu.js" />"></script>
  <script src="<spring:url value="/js/jquery/jquery.dataTables.customPaging.js" />"></script>
  <script src="<spring:url value="/js/jquery/ajax-datatable.js" />" ></script>
  <script src="<spring:url value="/js/common/form2js.js" />" ></script>
  <script src="<spring:url value="/js/common/json2.js" />" ></script>
  <script src="<spring:url value="/js/jquery/jquery.toObject.js" />" ></script>
  <script src="<c:url value="/js/bootstrap/bootstrap-datepicker.js"/>" type='text/javascript'><![CDATA[&nbsp; ]]></script>
  <link href="<c:url value="/css/bootstrap/datepicker.css" />" rel="stylesheet" />

  <script src="<spring:url value="/js/jquery/jquery.numberInput.js" />" ></script>

  <script src="<spring:url value="/js/viewspecific/loyalty/inventory.js"/>"></script>
  <link href="<c:url value="/styles/temp.css" />" rel="stylesheet" />


<script>
function clickSearch() {
	$("#inventorySearchButton").click();
	$(".modal").modal("hide");
}

$(document).ready(function() {
	clickSearch();
});


</script>