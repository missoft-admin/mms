<%@ include file="../../common/taglibs.jsp" %>

<c:set var="ENCTYPE" value="application/x-www-form-urlencoded"/>

<div class="modal-dialog">
  <div class="modal-content">
    
    <div class="modal-header">
		  <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
			<h4 class="isPrimary"><spring:message code="label_multiple_approval" /></h4>
		</div>
 
    <div class="modal-body">
      <form:form id="multipleAllocateDto" name="multipleAllocateDto" modelAttribute="multipleAllocateDto" action="${pageContext.request.contextPath}/loyaltycardinventory/saveallocation/" method="POST" enctype="${ENCTYPE}" cssClass="form-reset" >

      <div id="contentError" class="hide alert alert-error">
        <button type="button" class="close" onclick="$(this).parent().hide();">&times;</button>
        <div><form:errors path="*"/></div>
      </div>
      
      <form:hidden path="batchRefNo"/>
      
      <div class="contentMain">
        <div class="row-fluid">
          <div class="span4">
            <spring:message code="mo_tier_prop_name" var="cardType" />
            <label for="">${cardType}</label>
            <input type="text" name="cardTypeDesc" value="${multipleAllocateDto.cardType.description}" disabled="disabled" /><input type="hidden" name="cardType" value="${multipleAllocateDto.cardType.code}" />
          </div>
          
          <div class="span4">
            <spring:message code="inventory_prop_location" var="locationLabel" />
            <label for="">${locationLabel}</label>
            <input type="text" name="locationDesc" value="${multipleAllocateDto.locationName}" disabled="disabled" /><input type="hidden" name="location" value="${multipleAllocateDto.location}" />
          </div>
          
        </div>
        
        <div class="row-fluid">
          <div class="span4">
            <spring:message code="inventory_prop_changeallocfrom" var="changeAllocFromLbl" />
            <label for="">${changeAllocFromLbl}</label>
            <input type="text" name="allocateToDesc" value="${multipleAllocateDto.transferToName}" disabled="disabled" /><input type="hidden" name="transferTo" value="${multipleAllocateDto.transferTo}" />
          </div>
          
          <div class="span4">
            <spring:message code="inventory_prop_changeallocto" var="changeAllocToLbl" />
            <label for="">${changeAllocToLbl}</label>
            <input type="text" name="newAllocateToDesc" value="${multipleAllocateDto.newTransferToName}" disabled="disabled" /><input type="hidden" name="newTransferTo" value="${multipleAllocateDto.newTransferTo}" />
          </div>
        </div>
        <%-- 
        <div class="row-fluid">
          <c:if test="${multipleAllocateDto.status.code == forAllocation}">
          <div class="span4">
            <spring:message code="mo_prop_reason" var="reason" />
            <label for="">${reason}</label>
            <form:textarea path="reason" placeholder="${reason}" />
          </div>
          </c:if>
        </div>
        --%>
        
        <div>
          <table id="productTable" class="table table-condensed table-compressed table-striped">
            <thead>
              <tr>
                <th><input type="checkbox" class="checkAll" /></th>
                <th><spring:message code="mo_tier_prop_startingseries" /></th>
                <th><spring:message code="mo_tier_prop_endingseries" /></th>
              </tr>
            </thead>
            <tbody>
              <c:forEach var="item" items="${multipleAllocateDto.allocations}" varStatus="status" >
              <tr>
                <td><input type="checkbox" class="referenceNo" name="allocations[${status.index}].referenceNo" value="${item.referenceNo}" /></td>
                <td>${item.startingSeries}</td>
                <td>${item.endingSeries}</td>
              </tr>
              </c:forEach>
            </tbody>
          </table>
        </div>
      </div>
      </form:form>
    
    </div>
    <div class="modal-footer">
      <button type="button" id="" class="btn btn-default" data-dismiss="modal"><spring:message code="label_cancel"/></button>
      <button type="button" id="" data-url="<spring:url value="/loyaltycardinventory/changealloc/approve/false"/>" class="btn btn-primary changeAllocationApprove"><spring:message code="label_reject" /></button>
      <button type="button" id="" data-url="<spring:url value="/loyaltycardinventory/changealloc/approve/true"/>" class="btn btn-primary changeAllocationApprove"><spring:message code="label_approve" /></button>
    </div>
  </div>
</div>
<%--
<div class="modal hide  nofly" tabindex="-1" role="dialog" aria-hidden="true" id="reasonModal">
<div class="modal-dialog">
  <div class="modal-content">
    <div class="modal-body">
        <div class="row-fluid">
          <div class="span4">
            <spring:message code="mo_prop_reason" var="reason" />
            <label for="">${reason}</label>
             <textarea name="reason" placeholder="${reason}"></textarea>
          </div>
        </div>
    </div>
    <div class="modal-footer">
    </div>
  </div>
</div>
</div>
 --%>

<script src="<spring:url value="/js/viewspecific/loyalty/allocate.js"/>"></script>