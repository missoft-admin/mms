package com.transretail.crm.core.entity;

import java.io.Serializable;

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Version;
import javax.validation.constraints.NotNull;

import org.apache.commons.lang3.BooleanUtils;
import org.apache.commons.lang3.builder.ReflectionToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import org.hibernate.annotations.Type;
import org.hibernate.envers.Audited;

import com.transretail.crm.core.entity.support.CustomIdAuditableEntity;

@Audited
@Entity
@Table(name = "CRM_USER_ROLE")
@AttributeOverrides({@AttributeOverride(name = "id", column = @Column(name = "CODE"))})
public class UserRoleModel extends CustomIdAuditableEntity<String> implements Serializable {
    private static final long serialVersionUID = 3866178980225233962L;
    @Column(name = "DESCRIPTION")
    private String description;

    @NotNull
    @Column(name = "ENABLED", length = 1)
    @Type(type = "yes_no")
    private Boolean enabled = Boolean.FALSE;

    @Version
    @Column(name = "version")
    private Integer version = 1;
    
    @Column(name = "ORDER_NO")
    private Integer order; //null ORDER_NO will not show role on user role dialog
    
    @Column(name = "SUPERVISOR_ROLE_CODE")
    private String supervisorRoleCode;

    public UserRoleModel() {

    }

    public UserRoleModel(String code) {
        setId(code);
    }

    public void setEnabled(Boolean enabled) {
        this.enabled = BooleanUtils.toBoolean(enabled);
    }

    public Boolean getEnabled() {
        return this.enabled;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getDescription() {
        return this.description;
    }

    public void setCode(String code) {
        setId(code);
    }

    public String getCode() {
        return getId();
    }

    public String toString() {
        return ReflectionToStringBuilder.toString(this, ToStringStyle.SHORT_PREFIX_STYLE);
    }

    public void setVersion(Integer version) {
        this.version = version;
    }

    public Integer getVersion() {
        return this.version;
    }

    public boolean equals(UserRoleModel other) {
        return getId().equals(other.getId());
    }

	public Integer getOrder() {
		return order;
	}

	public void setOrder(Integer order) {
		this.order = order;
	}

	public String getSupervisorRoleCode() {
		return supervisorRoleCode;
	}

	public void setSupervisorRoleCode(String supervisorRoleCode) {
		this.supervisorRoleCode = supervisorRoleCode;
	}
	
    
    
}
