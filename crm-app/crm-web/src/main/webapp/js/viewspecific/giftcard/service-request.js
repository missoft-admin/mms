$(document).ready(function () {
    var CONTAINER_ERROR = "#content-error > div";
    var CONTENT_ERROR = "#content-error";
    var inputCardNo = $("input[name='cardNo']");
    var $sectionNewRequest = $('#section-file-new-request');

    inputCardNo.numberInput("type", "int");

    $('#enableDisableBtn').click(function () {
        var btn = $(this);
        btn.button('loading');
        var url = ctx + '/giftcard/service-request/activate/' + inputCardNo.val();

        $.get(url, function (data) {
            if (data.success) {
                btn.button('reset');
                var status = data.result;
                if (status !== 'DISABLED')
                    btn.text(btn.data('label-disable'));
                else
                    btn.text(btn.data('label-enable'));

                $("input[name='cardInfoStatus']").val(status);
            }
        });
    });

    $listServiceRequest = $("#list_service_request");

    $listTxInfo = $("#list_gc_tx_info");

    $listCardStatus = $("#list_giftcard_status");
//  $listCardStatus.ajaxDataTable({
//    'autoload': true,
//    'ajaxSource': ctx + "/giftcard/inventory/adjustment/list",
//    //'aaSorting': [[0, "desc"]],
//    'columnHeaders': [
//      "<spring:message code='gc.service.request.table.column.status' />",
//      "<spring:message code='gc.service.request.table.column.status.time' />",
//      "<spring:message code='gc.service.request.table.column.status.user' />"
//    ],
//    'modelFields': [
//      {name: 'productProfile', sortable: false, fieldCellRenderer: function(data, type, row) {
//	  return row.productProfile + " - " + row.productProfileName;
//	}
//      },
//      {name: 'startingSeries', sortable: false},
//      {name: 'endingSeries', sortable: false}
//    ]
//  });

    $('#show-table').on('show', function () {
        reloadServiceRequestTable();
    });

    $('#searchGiftCard').click(function (e) {
        e.preventDefault();

        var $cardNo = inputCardNo.val();
        var $url = ctx + "/giftcard/service-request/find/" + $cardNo;
        $.get($url, function (data) {
            var sectionViewGiftCard = $('#section-view-giftcard');
            if (data.success) {
                // clear previous errors
                sectionViewGiftCard.find(CONTENT_ERROR).hide();
                sectionViewGiftCard.find(CONTAINER_ERROR).empty();
                $sectionNewRequest.find('#content-success').hide();
                $sectionNewRequest.find('#content-success > div').empty();

                var cardInfo = data.result;
                fillCardInfoSection(cardInfo);
                loadServiceRequestTable(cardInfo);
                loadTransactions(cardInfo);
                // Section Loading
                $sectionNewRequest.show('slow');
                $('#section-file-new-request-details').show('slow');
                $('#section-card-info').show('slow');
                if (cardInfo.status === 'ACTIVATED' || cardInfo.status === 'DISABLED')
                    $('#section-card-info-enable-disable').show('slow');
                else
                    $('#section-card-info-enable-disable').hide();
                $('#section-tx-info').show('slow');
            } else {
                sectionViewGiftCard.find(CONTAINER_ERROR).html(data.result);
                sectionViewGiftCard.find(CONTENT_ERROR).show('slow');
                // hide all sections in case it was loaded at previous giftcard
                $sectionNewRequest.hide();
                $('#section-file-new-request-details').hide();
                $('#section-card-info').hide();
                if (cardInfo.status === 'ACTIVATED' || cardInfo.status === 'DISABLED')
                    $('#section-card-info-enable-disable').hide();
                $('#section-tx-info').hide();
            }
        });
        EgcInfo.showEgcInfoSection($cardNo);
    });

    function fillCardInfoSection(cardInfo) {
        $("input[name='cardInfoCardNo']").val(cardInfo.cardNo);
        $("input[name='giftcardNo']").val(cardInfo.cardNo);
        $("input[name='createdDateTime']").val(cardInfo.createdDateTime);
        $("input[name='orderNo']").val(cardInfo.orderNo);
        $("input[name='activateTime']").val(cardInfo.activateTime);
        $("input[name='saleStore']").val(cardInfo.saleStore);
        $("input[name='createdBy']").val(cardInfo.createdBy);
        $("input[name='faceValue']").val(cardInfo.faceValue);
        $("input[name='allocatedTo']").val(cardInfo.allocatedTo);
        $("input[name='previousBalance']").val(cardInfo.previousBalance);
        $("input[name='cardInfoStatus']").val(cardInfo.status);
        $("input[name='customerId']").val(cardInfo.customerId);
        $("input[name='product']").val(cardInfo.product);
        $("input[name='activateUser']").val(cardInfo.activateUser);
        $("input[name='saleTerminalId']").val(cardInfo.terminalId);
        $("input[name='moNo']").val(cardInfo.moNo);
        $("input[name='reloadable']").attr("checked", cardInfo.reloadable);
        $("input[name='allowPartialRedeem']").attr("checked", cardInfo.allowPartialRedeem);
        $("input[name='balance']").val(cardInfo.balance);
        $("input[name='expiryDate']").val(cardInfo.expiryDate);
        $("input[name='unitCost']").val(cardInfo.unitCost);
        $("input[name='lastInvLoc']").val(cardInfo.inventoryLocation);

        if (cardInfo.status === 'ACTIVATED') {
            $('#enableDisableBtn').text($('#enableDisableBtn').data('label-disable'));
        } else if (cardInfo.status === 'DISABLED') {
            $('#enableDisableBtn').text($('#enableDisableBtn').data('label-enable'));
        }
    }

    function loadServiceRequestTable(cardInfo) {
        var tableElement = $listServiceRequest.has('table');
        var hasTableElement = tableElement.length === 1;
        var searchCardNo = inputCardNo.val();
        var owningGiftcard = $listServiceRequest.data("owning-giftcard");
        var currCardNo = $("input[name='cardInfoCardNo']").val();

        if (hasTableElement && searchCardNo === currCardNo && owningGiftcard === currCardNo) {
            reloadServiceRequestTable();
            return;
        }

        $listServiceRequest.empty();
        $listServiceRequest.ajaxDataTable({
            'autoload': false,
            'ajaxSource': ctx + "/giftcard/service-request/list/" + cardInfo.cardNo,
            'aaSorting': [[0, "desc"]],
            'columnHeaders': [
                $listServiceRequest.data("column-date-filed"),
                $listServiceRequest.data("column-filed-by"),
                $listServiceRequest.data("column-filed-in"),
                $listServiceRequest.data("column-details")],
            'modelFields': [
                {name: 'dateFiled', sortable: true},
                {name: 'filedBy', sortable: true},
                {
                    name: 'filedIn', sortable: true, fieldCellRenderer: function (data, type, row) {
                    return row.filedInPresenter;
                }
                },
                {name: 'details', sortable: false}
            ]
        });
        $listServiceRequest.data("owning-giftcard", currCardNo);

        var curr_class = $('#accor-service-req-details').attr('class');
        if (curr_class.indexOf('in') > -1) {
            $('#accor-service-req-details').collapse('toggle');
        }
    }

    function loadTransactions(cardInfo) {
        var tableElement = $listTxInfo.has('table');
        var hasTableElement = tableElement.length === 1;
        var searchCardNo = inputCardNo.val();
        var owningGiftcard = $listTxInfo.data("owning-giftcard");
        var currCardNo = $("input[name='cardInfoCardNo']").val();

        if (hasTableElement && searchCardNo === currCardNo && owningGiftcard === currCardNo) {
            reloadTransactionInfoTable();
            return;
        }

        $listTxInfo.empty();
        $listTxInfo.ajaxDataTable({
            'autoload': true,
            'ajaxSource': ctx + "/giftcard/service-request/list/transactions/" + cardInfo.cardNo,
            'aaSorting': [[0, "desc"]],
            'columnHeaders': [
                $listTxInfo.data('column-created-time'),
                $listTxInfo.data('column-tx-time'),
                $listTxInfo.data('column-book-date'),
                $listTxInfo.data('column-store-id'),
                $listTxInfo.data('column-terminal-id'),
                $listTxInfo.data('column-cashier-id'),
                $listTxInfo.data('column-tx-no'),
                $listTxInfo.data('column-tx-type'),
                $listTxInfo.data('column-tx-prev-bal'),
                $listTxInfo.data('column-tx-amount'),
                $listTxInfo.data('column-tx-curr-bal')
            ],
            'modelFields': [
                {name: 'createdDateTime', sortable: true, alignment: 'center', dataType: "DATE"},
                {name: 'transactionTime', sortable: true, alignment: 'center', dataType: "DATE"},
                {name: 'createdBy', sortable: true},
                {name: 'store', sortable: true},
                {name: 'terminalId', sortable: true},
                {name: 'cashierId', sortable: true},
                {name: 'transactionNo', sortable: true},
                {name: 'transactionType', sortable: true},
                {name: 'previousBalance', sortable: false, alignment: 'right'},
                {name: 'transactionAmount', sortable: false, alignment: 'right'},
                {name: 'balance', sortable: false, alignment: 'right'}
            ]
        });
        $listTxInfo.data('owning-giftcard', currCardNo);
    }

    $('#fileServiceRequestBtn').click(function (e) {
        e.preventDefault();
        var serviceRequestForm = $('#giftCardServiceRequestForm');
        $.post(ctx + serviceRequestForm.attr("action"), serviceRequestForm.serialize(),
            function (data) {
                if (data.success) {
                    serviceRequestForm.trigger("reset");
                    $sectionNewRequest.find('#content-success > div')
                        .html("New Service Request is created.");
                    $sectionNewRequest.find('#content-success').show('slow');
                    reloadServiceRequestTable();
                } else {
                    errorInfo = "";
                    for (i = 0; i < data.result.length; i++) {
                        errorInfo += "<br>" + (i + 1) + ". "
                            + (data.result[i].code !== undefined ? data.result[i].code : data.result[i]);
                    }
                    $sectionNewRequest.find(CONTAINER_ERROR).html("Please correct following errors: " + errorInfo);
                    $sectionNewRequest.find(CONTENT_ERROR).show('slow');
                }
            });
    });

    function reloadServiceRequestTable() {
        $listServiceRequest.ajaxDataTable('search', new Object());
    }

    function reloadTransactionInfoTable() {
        $listTxInfo.ajaxDataTable('search', new Object());
    }
});