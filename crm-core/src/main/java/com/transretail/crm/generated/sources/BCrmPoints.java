package com.transretail.crm.generated.sources;

import javax.annotation.Generated;

/**
 * BCrmPoints is a Querydsl bean type
 */
@Generated("com.mysema.query.codegen.BeanSerializer")
public class BCrmPoints {

    private String comments;

    private String createdBy;

    private java.sql.Timestamp createdDatetime;

    private Double expireExcess;

    private java.sql.Date expiryDate;

    private String id;

    private java.sql.Timestamp inclusiveDateFrom;

    private java.sql.Timestamp inclusiveDateTo;

    private java.sql.Timestamp lastRedeem;

    private String lastUpdatedBy;

    private java.sql.Timestamp lastUpdatedDatetime;

    private Long memberId;

    private String reason;

    private String status;

    private String storeCode;

    private Double txnAmount;

    private java.sql.Timestamp txnDate;

    private String txnMedia;

    private String txnNo;

    private Long txnOwner;

    private Double txnPoints;

    private String txnType;

    private String validatedBy;

    private java.sql.Timestamp validatedDate;

    private Integer validPeriod;

    public String getComments() {
        return comments;
    }

    public void setComments(String comments) {
        this.comments = comments;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public java.sql.Timestamp getCreatedDatetime() {
        return createdDatetime;
    }

    public void setCreatedDatetime(java.sql.Timestamp createdDatetime) {
        this.createdDatetime = createdDatetime;
    }

    public Double getExpireExcess() {
        return expireExcess;
    }

    public void setExpireExcess(Double expireExcess) {
        this.expireExcess = expireExcess;
    }

    public java.sql.Date getExpiryDate() {
        return expiryDate;
    }

    public void setExpiryDate(java.sql.Date expiryDate) {
        this.expiryDate = expiryDate;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public java.sql.Timestamp getInclusiveDateFrom() {
        return inclusiveDateFrom;
    }

    public void setInclusiveDateFrom(java.sql.Timestamp inclusiveDateFrom) {
        this.inclusiveDateFrom = inclusiveDateFrom;
    }

    public java.sql.Timestamp getInclusiveDateTo() {
        return inclusiveDateTo;
    }

    public void setInclusiveDateTo(java.sql.Timestamp inclusiveDateTo) {
        this.inclusiveDateTo = inclusiveDateTo;
    }

    public java.sql.Timestamp getLastRedeem() {
        return lastRedeem;
    }

    public void setLastRedeem(java.sql.Timestamp lastRedeem) {
        this.lastRedeem = lastRedeem;
    }

    public String getLastUpdatedBy() {
        return lastUpdatedBy;
    }

    public void setLastUpdatedBy(String lastUpdatedBy) {
        this.lastUpdatedBy = lastUpdatedBy;
    }

    public java.sql.Timestamp getLastUpdatedDatetime() {
        return lastUpdatedDatetime;
    }

    public void setLastUpdatedDatetime(java.sql.Timestamp lastUpdatedDatetime) {
        this.lastUpdatedDatetime = lastUpdatedDatetime;
    }

    public Long getMemberId() {
        return memberId;
    }

    public void setMemberId(Long memberId) {
        this.memberId = memberId;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getStoreCode() {
        return storeCode;
    }

    public void setStoreCode(String storeCode) {
        this.storeCode = storeCode;
    }

    public Double getTxnAmount() {
        return txnAmount;
    }

    public void setTxnAmount(Double txnAmount) {
        this.txnAmount = txnAmount;
    }

    public java.sql.Timestamp getTxnDate() {
        return txnDate;
    }

    public void setTxnDate(java.sql.Timestamp txnDate) {
        this.txnDate = txnDate;
    }

    public String getTxnMedia() {
        return txnMedia;
    }

    public void setTxnMedia(String txnMedia) {
        this.txnMedia = txnMedia;
    }

    public String getTxnNo() {
        return txnNo;
    }

    public void setTxnNo(String txnNo) {
        this.txnNo = txnNo;
    }

    public Long getTxnOwner() {
        return txnOwner;
    }

    public void setTxnOwner(Long txnOwner) {
        this.txnOwner = txnOwner;
    }

    public Double getTxnPoints() {
        return txnPoints;
    }

    public void setTxnPoints(Double txnPoints) {
        this.txnPoints = txnPoints;
    }

    public String getTxnType() {
        return txnType;
    }

    public void setTxnType(String txnType) {
        this.txnType = txnType;
    }

    public String getValidatedBy() {
        return validatedBy;
    }

    public void setValidatedBy(String validatedBy) {
        this.validatedBy = validatedBy;
    }

    public java.sql.Timestamp getValidatedDate() {
        return validatedDate;
    }

    public void setValidatedDate(java.sql.Timestamp validatedDate) {
        this.validatedDate = validatedDate;
    }

    public Integer getValidPeriod() {
        return validPeriod;
    }

    public void setValidPeriod(Integer validPeriod) {
        this.validPeriod = validPeriod;
    }

}

