package com.transretail.crm.common.reporting.jasper;

import net.sf.jasperreports.engine.util.FileResolver;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;

/**
 * @author Allan G. Ramirez (agramirez@exist.com)
 */
public class ClasspathFileResolver implements FileResolver {
	private Logger logger = LoggerFactory.getLogger(getClass());
	private String parentFolder;

	public ClasspathFileResolver(String parentFolder) {
		this.parentFolder = parentFolder;
	}

	@Override
	public File resolveFile(String fileName) {

		String fullPath = parentFolder + File.separator + fileName;
		if (fileName.endsWith(".png")) {
			fileName = fileName.substring(2);
		}
		String tmpFile = System.getProperty("java.io.tmpdir") + File.separator + fileName;
		File file = new File(tmpFile);

		if (!file.exists()) {

			InputStream inputStream = null;
			OutputStream outputStream = null;
			try {

				inputStream = new BufferedInputStream(getClass().getClassLoader().getResourceAsStream(fullPath));
				outputStream = new BufferedOutputStream(new FileOutputStream(new File(tmpFile)));

				byte[] buffer = new byte[1024];
				int lengthRead;
				while ((lengthRead = inputStream.read(buffer)) > 0) {
					outputStream.write(buffer, 0, lengthRead);
					outputStream.flush();
				}


			} catch (IOException e) {
				e.getCause();
				e.printStackTrace();
			} finally {

				if (inputStream != null) {
					try {
						inputStream.close();
					} catch (IOException e) {
						e.printStackTrace();
					}
				}

				if (outputStream != null) {
					try {
						outputStream.close();
					} catch (IOException e) {
						e.printStackTrace();
					}
				}

			}

			return file;
		}

		logger.debug("Jasper file path: " + file.getAbsolutePath());
		return file;
	}
}
