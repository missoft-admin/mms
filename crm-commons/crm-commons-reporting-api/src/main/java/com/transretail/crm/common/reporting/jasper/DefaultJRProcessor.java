package com.transretail.crm.common.reporting.jasper;

import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;

import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.transretail.crm.common.reporting.exception.ReportException;

import net.sf.jasperreports.engine.DefaultJasperReportsContext;
import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.JREmptyDataSource;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.JasperReportsContext;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import net.sf.jasperreports.engine.design.JasperDesign;
import net.sf.jasperreports.engine.util.FileResolver;
import net.sf.jasperreports.engine.util.JRLoader;
import net.sf.jasperreports.engine.util.LocalJasperReportsContext;
import net.sf.jasperreports.engine.xml.JRXmlLoader;

/**
 * Default implementation of JRProcessor
 *
 * @author Allan G. Ramirez (agramirez@exist.com)
 */
public class DefaultJRProcessor extends AbstractJRProcessor {
    private JRDataSource dataSource;
    private Logger logger = LoggerFactory.getLogger(getClass());
    private FileResolver fileResolver;

    public DefaultJRProcessor(String reportFileName) {
        super(reportFileName);
    }

    public DefaultJRProcessor(String reportFileName, Collection<?> beansDataSource) {
        this(reportFileName, new JRBeanCollectionDataSource(beansDataSource));
    }

    public DefaultJRProcessor(String reportFileName, JRDataSource dataSource) {
        super(reportFileName);
        this.dataSource = dataSource;
    }

    /**
     * Set the file resolver that would resolve string paths in jasper reports elements.
     * This is required if .jasper or .jrxml and the needed resources are placed in a sub-folder.
     * <p>
     * Eg.:
     * Scenario:
     * <br />
     * <ul>
     * <li>test-reports.jrxml, logo.jpg and mysubreport.jasper are placed in <em>reports</em> folder</li>
     * <li>In test-reports.jrxml you have these expressions:
     * <br />
     * <pre>
     *      <imageExpression class="java.lang.String"><![CDATA["logo.jpg"]]></imageExpression>
     *      <subreportExpression class="java.lang.String"><![CDATA["mysubreport.jasper"]]></subreportExpression>
     * </pre>
     * </li>
     * </ul>
     * Usage:
     * <pre>
     *      DefaultJRProcessor jrProcessor = new DefaultJRProcessor("test-reports.jrxml");
     *      jrProcessor.setFileResolver(new ClasspathFileResolver("reports"));
     *      reportService.exportPdfReport(jrProcessor, file);
     * </pre>
     * </p>
     *
     * @param fileResolver file resolver
     */
    public void setFileResolver(FileResolver fileResolver) {
        this.fileResolver = fileResolver;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<JasperPrint> getJasperPrints() throws ReportException {
        try {
            if (dataSource == null) {
                dataSource = new JREmptyDataSource();
            }

            JasperReportsContext reportsContext = DefaultJasperReportsContext.getInstance();
            if (fileResolver != null) {
                // JRParameter.REPORT_FILE_RESOLVER is replaced with JasperReportsContext
                reportsContext = new LocalJasperReportsContext(reportsContext);
                ((LocalJasperReportsContext) reportsContext).setFileResolver(fileResolver);
            }
            JasperReport jasperReport = loadJasper(getReportFileName());
            return Arrays.asList(JasperFillManager.getInstance(reportsContext).fill(jasperReport, parameters, dataSource));
        } catch (FileNotFoundException ex) {
            throw new ReportException("File [" + getReportFileName() + "] does not exist.", ex);
        } catch (JRException ex) {
            throw new ReportException("Could not parse or fill JasperReports report from " + getReportFileName(), ex);
        }
    }

    private JasperReport loadJasper(String filename) throws FileNotFoundException, JRException {
        InputStream is = null;
        try {
            if (filename.endsWith(".jasper")) {
                // Load pre-compiled report.
                if (logger.isInfoEnabled()) {
                    logger.info("Loading pre-compiled Jasper Report from " + filename);
                }
                is = Thread.currentThread().getContextClassLoader().getResourceAsStream(filename);
                return (JasperReport) JRLoader.loadObject(is);
            } else if (filename.endsWith(".jrxml")) {
                // Compile report on-the-fly.
                if (logger.isInfoEnabled()) {
                    logger.info("Compiling Jasper Report loaded from " + filename);
                }
                is = Thread.currentThread().getContextClassLoader().getResourceAsStream(filename);
                JasperDesign design = JRXmlLoader.load(is);
                return JasperCompileManager.compileReport(design);
            }
            throw new IllegalArgumentException(
                "Report filename [" + filename + "] must end in either .jasper or .jrxml");
        } finally {
            IOUtils.closeQuietly(is);
        }
    }
}
