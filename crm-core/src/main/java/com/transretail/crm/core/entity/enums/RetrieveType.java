package com.transretail.crm.core.entity.enums;


public enum RetrieveType {

    RETRIEVE_ALL, RETRIEVE_BY_ID, RETRIEVE_BY_EMAIL, RETRIEVE_BY_CONTACT, VALIDATE_PIN
}
