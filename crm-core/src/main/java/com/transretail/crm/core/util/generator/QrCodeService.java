package com.transretail.crm.core.util.generator;


import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;

import com.google.zxing.NotFoundException;


public interface QrCodeService {

	File generateQrCode(String code);
	String decryptQrCode(File file) throws FileNotFoundException, IOException, NotFoundException;

}
