package com.transretail.crm.report.template.impl;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.apache.commons.lang3.StringUtils;
import org.joda.time.LocalDate;
import org.joda.time.LocalDateTime;
import org.joda.time.YearMonth;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Service;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.google.common.collect.Sets;
import com.mysema.query.BooleanBuilder;
import com.mysema.query.jpa.impl.JPAQuery;
import com.transretail.crm.common.reporting.jasper.ClasspathFileResolver;
import com.transretail.crm.common.reporting.jasper.DefaultJRProcessor;
import com.transretail.crm.common.reporting.jasper.JRProcessor;
import com.transretail.crm.common.util.DateUtil;
import com.transretail.crm.core.entity.enums.ReportType;
import com.transretail.crm.core.security.util.UserUtil;
import com.transretail.crm.giftcard.entity.QGiftCardInventory;
import com.transretail.crm.giftcard.entity.QGiftCardTransaction;
import com.transretail.crm.giftcard.entity.QGiftCardTransactionItem;
import com.transretail.crm.giftcard.entity.QProductProfile;
import com.transretail.crm.giftcard.entity.support.GiftCardSaleTransaction;
import com.transretail.crm.giftcard.entity.support.GiftCardSalesType;
import com.transretail.crm.report.template.CommonReportFilter;
import com.transretail.crm.report.template.FilterField;
import com.transretail.crm.report.template.NoDataFoundSupport;
import com.transretail.crm.report.template.ReportTemplate;

@Service("giftCardActivationReport")
public class GiftCardActivationReport implements ReportTemplate, NoDataFoundSupport {
	protected static final String TEMPLATE_NAME = "Gift Card Add Value Summary By Month and Year";
    protected static final String REPORT_NAME = "reports/gift_card_activation_report.jasper";
    protected static final String FILTER_DATE_TO = CommonReportFilter.DATE_TO;
    protected static final String FILTER_DATE_FROM = CommonReportFilter.DATE_FROM;
    
    private static final DateTimeFormatter INPUT_DATE_PATTERN = DateTimeFormat.forPattern(DateUtil.SEARCH_DATE_FORMAT);
    
    private static final Logger LOGGER = LoggerFactory.getLogger(GiftCardActivationReport.class);
    private static final DateTimeFormatter RAW_YEAR_MONTH_FORMATTER = DateTimeFormat.forPattern("yyyyMM")
    	    .withLocale(LocaleContextHolder.getLocale());
    
    @PersistenceContext
    private EntityManager em;
    @Autowired
    private MessageSource messageSource;
    
    @Override
    public ReportType[] getReportTypes() {
    	return new ReportType[] {ReportType.EXCEL, ReportType.PDF};
    }
    
    @Override
    public String getName() {
    	return TEMPLATE_NAME;
    }

    @Override
    public String getDescription() {
    	return messageSource.getMessage("gift.card.activation.report.menu", (Object[]) null, LocaleContextHolder.getLocale());
    }
    
    @Override
    public boolean canView(Set<String> permissions) {
    	return true;
    }
    
    @Override
    public Set<FilterField> getFilters() {
    	Set<FilterField> filters = Sets.newLinkedHashSet();
		filters.add(FilterField.createDateField(FILTER_DATE_FROM,
				messageSource.getMessage("label_date_from", (Object[]) null, LocaleContextHolder.getLocale()),
				DateUtil.SEARCH_DATE_FORMAT, true));
			filters.add(FilterField.createDateField(FILTER_DATE_TO,
				messageSource.getMessage("label_date_to", (Object[]) null, LocaleContextHolder.getLocale()),
				DateUtil.SEARCH_DATE_FORMAT, true));
			
			
			Map<String, String> products = Maps.newLinkedHashMap();
			products.put("", "");
			products.putAll(getProducts());
			filters.add(FilterField.createDropdownField("product",
	                messageSource.getMessage("gift.card.activation.report.product", (Object[]) null, LocaleContextHolder.getLocale()), products));
			
			Map<String, String> salesTypes = new LinkedHashMap<String, String>();
			salesTypes.put("", "");
			for (GiftCardSalesType key : GiftCardSalesType.values()) {
				salesTypes.put(key.toString(), key.toString());
			}
			filters.add(FilterField.createDropdownField("salesType",
	                messageSource.getMessage("gift.card.activation.report.salestype", (Object[]) null, LocaleContextHolder.getLocale()), salesTypes));
			
		return filters;
    }
    
    private Map<String, String> getProducts() {
    	QProductProfile qPrd = QProductProfile.productProfile;
    	return new JPAQuery(em).from(qPrd).orderBy(qPrd.productDesc.asc()).map(qPrd.id.stringValue(), qPrd.productDesc);
    }
    
    @Override
    public JRProcessor createJrProcessor(Map<String, String> map) {
    	LocalDateTime startDate = INPUT_DATE_PATTERN.parseLocalDate(map.get(FILTER_DATE_FROM)).toDateTimeAtStartOfDay().toLocalDateTime();
		LocalDateTime endDate = INPUT_DATE_PATTERN.parseLocalDate(map.get(FILTER_DATE_TO)).plusDays(1).toDateTimeAtStartOfDay().minusMillis(1).toLocalDateTime();
		Long prodId = StringUtils.isNotBlank(map.get("product")) ? Long.valueOf(map.get("product")) : null;
		GiftCardSalesType salesType = StringUtils.isNotBlank(map.get("salesType")) ? GiftCardSalesType.valueOf(map.get("salesType")) : null;
		DefaultJRProcessor jrProcessor = new DefaultJRProcessor(REPORT_NAME, ImmutableList.copyOf(createDataSource(startDate, endDate, prodId, salesType)));
		Map<String, Object> paramMap = prepareReportParameters(startDate, endDate, prodId, salesType);
		jrProcessor.addParameters(paramMap);
		jrProcessor.setFileResolver(new ClasspathFileResolver("reports"));
		return jrProcessor;
    }

    public List<TransactionBean> createDataSource(LocalDateTime start, LocalDateTime end, Long prodId, GiftCardSalesType salesType) {
    	List<TransactionBean> transactionBeans = Lists.newArrayList();
    	YearMonth startM = YearMonth.fromDateFields(start.toDate());
    	YearMonth endM = YearMonth.fromDateFields(end.toDate());
    	
    	YearMonth startYm = startM.withMonthOfYear(1);
    	YearMonth endYm = endM.withMonthOfYear(12);
    	
    	for(YearMonth yms = startYm; yms.isBefore(endYm) || yms.isEqual(endYm); yms = yms.plusMonths(1)) {
    		int year = yms.getYear();
    		int i = yms.getMonthOfYear();
    		
    		Double activationSum = 0d;
    		Double voidSum = 0d;
    		if(!(yms.isAfter(endM) || yms.isBefore(startM))) {
    			activationSum = getSum(year, i, GiftCardSaleTransaction.ACTIVATION, start, end, prodId, salesType);
        		voidSum = getSum(year, i, GiftCardSaleTransaction.VOID_ACTIVATED, start, end, prodId, salesType);
    		}
    		transactionBeans.add(new TransactionBean(year, i, activationSum, voidSum));
    	}
    	return transactionBeans;
    }
    
    private double getSum(Integer year, Integer month, LocalDate start, LocalDate end, Long prodId, GiftCardSalesType salesType) { // applies to ACTIVATION only
    	QGiftCardInventory qInventory = QGiftCardInventory.giftCardInventory;
    	BooleanBuilder dateFilter = new BooleanBuilder(qInventory.activationDate.year().eq(year)
    	.and(qInventory.activationDate.month().eq(month))
    	.and(qInventory.activationDate.between(start, end)));
    	
    	if(prodId != null) {
    		dateFilter.and(qInventory.profile.id.eq(prodId));
    	}
    	
    	if(salesType != null)
    		dateFilter.and(qInventory.salesType.eq(salesType));
    	
    	BigDecimal ret = new JPAQuery(em).from(qInventory)
    			.where(dateFilter).singleResult(qInventory.faceValue.sum());
    	return ret != null ? ret.doubleValue() : 0; 
    }
    
    private double getSum(Integer year, Integer month, GiftCardSaleTransaction txnType, LocalDateTime start, LocalDateTime end, Long prodId, GiftCardSalesType salesType) {
    	QGiftCardTransactionItem transactionItem = QGiftCardTransactionItem.giftCardTransactionItem;
    	BooleanBuilder dateFilter = new BooleanBuilder(transactionItem.transaction.transactionDate.year().eq(year)
				.and(transactionItem.transaction.transactionDate.month().eq(month))
				.and(transactionItem.transaction.transactionDate.between(start, end)));
    	if(prodId != null) {
    		dateFilter.and(transactionItem.giftCard.profile.id.eq(prodId));
    	}
    	if(salesType != null)
    		dateFilter.and(transactionItem.giftCard.salesType.eq(salesType));
    	Double ret = new JPAQuery(em).from(transactionItem)
				.where(dateFilter
						.and(transactionItem.transaction.transactionType.eq(txnType)))
						.singleResult(transactionItem.transactionAmount.sum());
    	return ret != null ? ret.doubleValue() : 0; 
    }
    
    
    private Map<String, Object> prepareReportParameters(LocalDateTime start, LocalDateTime end, Long prodId, GiftCardSalesType salesType) {
    	HashMap<String, Object> reportMap = Maps.newHashMap();
    	reportMap.put("START_DATE", start);
    	reportMap.put("END_DATE", end);
    	if(prodId != null) {
    		reportMap.put("PRODUCT_ID", prodId);
        	reportMap.put("PRODUCT_DESC", getProducts().get(Long.toString(prodId)));	
    	}
    	reportMap.put("SALES_TYPE", salesType != null ? salesType.toString() : "");
    	Locale locale = LocaleContextHolder.getLocale();
    	reportMap.put("NO_DATA", messageSource.getMessage("report.no.data", (Object[]) null, 
				LocaleContextHolder.getLocale()));
    	reportMap.put("printedBy", UserUtil.getCurrentUser().getUsername());
    	reportMap.put("printedDate", new LocalDate().toString("dd-MM-yyyy"));
    	return reportMap;
    }
    
    public static class TransactionBean {
    	private Integer year;
    	private Integer month;
    	private Double transactionAmt = new Double(0);
    	public TransactionBean() {}
    	public TransactionBean(Integer year, Integer month, Double activatedSum, Double voidSum) {
    		this.year = year;
    		this.month = month;
    		if(activatedSum != null)
    			this.transactionAmt += activatedSum;
    		if(voidSum != null)
    			this.transactionAmt -= voidSum;
    	}
		
		public Integer getYear() {
			return year;
		}
		public void setYear(Integer year) {
			this.year = year;
		}
		
		public Integer getMonth() {
			return month;
		}
		public void setMonth(Integer month) {
			this.month = month;
		}
		public Double getTransactionAmt() {
			return transactionAmt;
		}
		public void setTransactionAmt(Double transactionAmt) {
			this.transactionAmt = transactionAmt;
		}
    }
    
    @Override
	public boolean isEmpty(Map<String, String> filter) {
		LocalDateTime startDate = INPUT_DATE_PATTERN.parseLocalDate(filter.get(FILTER_DATE_FROM)).toDateTimeAtStartOfDay().toLocalDateTime();
		LocalDateTime endDate = INPUT_DATE_PATTERN.parseLocalDate(filter.get(FILTER_DATE_TO)).plusDays(1).toDateTimeAtStartOfDay().minusMillis(1).toLocalDateTime();
		
		QGiftCardTransaction transaction = QGiftCardTransaction.giftCardTransaction;
		
		return !new JPAQuery(em).from(transaction).where(transaction.transactionDate.between(startDate, endDate).and(transaction.transactionType.in(GiftCardSaleTransaction.ACTIVATION, GiftCardSaleTransaction.VOID_ACTIVATED))).exists();
	}
}
