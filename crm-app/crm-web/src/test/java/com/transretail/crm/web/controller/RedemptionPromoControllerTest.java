package com.transretail.crm.web.controller;


import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.reset;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.model;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.view;

import java.util.ArrayList;
import java.util.List;

import org.hamcrest.core.IsInstanceOf;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.TestingAuthenticationToken;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.context.SecurityContextImpl;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.AnnotationConfigWebContextLoader;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

import com.transretail.crm.core.dto.PromotionDto;
import com.transretail.crm.core.security.model.impl.CustomSecurityUserDetailsImpl;
import com.transretail.crm.core.service.CodePropertiesService;
import com.transretail.crm.core.service.LookupService;
import com.transretail.crm.core.service.MemberGroupService;
import com.transretail.crm.core.service.ProductService;
import com.transretail.crm.core.service.PromotionService;
import com.transretail.crm.core.service.StoreGroupService;


@RunWith(SpringJUnit4ClassRunner.class)
@WebAppConfiguration
@ContextConfiguration(loader = AnnotationConfigWebContextLoader.class)
public class RedemptionPromoControllerTest extends AbstractMockMvcTest {



	@Configuration
    @EnableWebMvc
    static class Config {
        @Bean
        public RedemptionPromoController redemptionPromoController() { return new RedemptionPromoController(); }
        @Bean
        public PromotionService promotionService() { return mock( PromotionService.class ); }
        @Bean
        public LookupService lookupService() { return mock( LookupService.class ); }
        @Bean
        public CodePropertiesService codePropertiesService() { return mock( CodePropertiesService.class ); }
        @Bean
        public MemberGroupService memberGroupService() { return mock( MemberGroupService.class ); }
        @Bean
        public StoreGroupService storeGroupService() { return mock( StoreGroupService.class ); }
        @Bean
        public ProductService productService() { return mock( ProductService.class ); }
        @Bean
        public MessageSource messageSource() { return mock( MessageSource.class ); }
    }



    @Autowired
    private PromotionService promotionService;
    @Autowired
    private LookupService lookupService;
    @Autowired
    private CodePropertiesService codePropertiesService;
    @Autowired
    private MemberGroupService memberGroupService;
    @Autowired
    private StoreGroupService storeGroupService;
    @Autowired
    private ProductService productService;
    @Autowired
    private MessageSource messageSource;



    private static final String MKTHEAD = "MKTHEAD";



    @SuppressWarnings("serial")
	@BeforeClass
    public static void setupCurrentUser() {
        CustomSecurityUserDetailsImpl user = new CustomSecurityUserDetailsImpl();
        user.setUserId( 0000L );
        user.setUsername( "user" );
        user.setPassword( "user" );
        List<GrantedAuthority> auths = new ArrayList<GrantedAuthority>();
        auths.add( new GrantedAuthority() { @Override public String getAuthority() { return MKTHEAD; } } );
        user.setAuthorities( auths );

        SecurityContextImpl securityContext = new SecurityContextImpl();
        securityContext.setAuthentication( new TestingAuthenticationToken( user, "" ) );
        SecurityContextHolder.setContext( securityContext );
    }

    @AfterClass
    public static void removeCurrentUser() {
        SecurityContextHolder.clearContext();
    }

    @Before
    public void setup() {
    	/*InternalResourceViewResolver viewResolver = new InternalResourceViewResolver();
        viewResolver.setPrefix( "/WEB-INF/jsp/view/" );
        viewResolver.setSuffix( ".jsp" );

        mockMvc = MockMvcBuilders.standaloneSetup( new RedemptionPromoController() )
                                 .setViewResolvers( viewResolver )
                                 .build();*/
    }

    @Override
    protected void doSetup() throws Exception {
        reset( promotionService );
        reset( lookupService );
        reset( codePropertiesService );
        reset( memberGroupService );
        reset( storeGroupService );
        reset( productService );
        reset( messageSource );
    }

    @Test
    public void showCreatePageTest() throws Exception {
        mockMvc.perform( get( "/promotion/redemption/create" ) )
            .andExpect( model().attribute( "promotion", new IsInstanceOf( PromotionDto.class ) ) )
            .andExpect( view().name( RedemptionPromoController.PATH_CREATE ) )
            .andExpect( status().isOk() );
    }

    @Test
    public void showEditPageTest() throws Exception {
    	Mockito.when( codePropertiesService.getMktHeadCode() ).thenReturn( MKTHEAD );
    	Mockito.when( codePropertiesService.getDetailStatusActive() ).thenReturn( "ACTIVE" );
    	PromotionDto dto = new PromotionDto();
    	dto.setCreateUser( "user" );
    	dto.setStatus( "ACTIVE" );
    	dto.setProgram( 1L );
    	Mockito.when( promotionService.getPromotionDto( 1L ) ).thenReturn( dto );
    	
        mockMvc.perform( get( "/promotion/redemption/edit/1" ) )
            .andExpect( model().attribute( "promotion", new IsInstanceOf( PromotionDto.class ) ) )
            .andExpect( view().name( RedemptionPromoController.PATH_VIEW ) )
            .andExpect( status().isOk() );
    }

}
