package com.transretail.crm.core.service.impl;


import com.google.common.collect.Lists;
import com.mysema.query.BooleanBuilder;
import com.mysema.query.Tuple;
import com.mysema.query.jpa.hibernate.HibernateSubQuery;
import com.mysema.query.jpa.impl.JPAQuery;
import com.mysema.query.types.Order;
import com.mysema.query.types.expr.BooleanExpression;
import com.transretail.crm.common.reporting.jasper.ClasspathFileResolver;
import com.transretail.crm.common.reporting.jasper.DefaultJRProcessor;
import com.transretail.crm.common.reporting.jasper.JRProcessor;
import com.transretail.crm.common.service.dto.response.ResultList;
import com.transretail.crm.common.service.util.SpringDataPagingUtil;
import com.transretail.crm.common.util.DateUtil;
import com.transretail.crm.core.dto.*;
import com.transretail.crm.core.dto.PurchaseTxnSearchDto.PurchaseTxnField;
import com.transretail.crm.core.entity.*;
import com.transretail.crm.core.entity.enums.*;
import com.transretail.crm.core.entity.lookup.Store;
import com.transretail.crm.core.repo.*;
import com.transretail.crm.core.security.util.UserUtil;
import com.transretail.crm.core.service.CodePropertiesService;
import com.transretail.crm.core.service.PointsRewardService;
import com.transretail.crm.core.service.StoreService;
import com.transretail.crm.core.util.CalendarUtil;
import com.transretail.crm.core.util.generator.IdGeneratorService;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.time.DateUtils;
import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import org.joda.time.LocalDate;
import org.quartz.CronExpression;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.io.InputStream;
import java.text.ParseException;
import java.util.*;


@Service("pointsRewardServiceImpl")
@Transactional
public class PointsRewardServiceImpl implements PointsRewardService {

	@Autowired
	RewardSchemeRepo rewardSchemeRepo;
	@Autowired
	MemberRepo memberRepo;
	@Autowired
	PointsTxnModelRepo pointsTxnModelRepo;
	@Autowired
	EmployeePurchaseTxnRepo employeePurchaseTxnRepo;
	@Autowired
	IdGeneratorService customIdGeneratorService;
	@Autowired
	StoreRepo storeRepo;
    @Autowired
    CodePropertiesService codePropertiesService;
    @Autowired
    StoreService storeService;

    @Autowired
    ApplicationConfigRepo applicationConfigRepo;

    @PersistenceContext
    private EntityManager em;
	@Autowired
    private MessageSource messageSource;

	private static final Logger LOGGER = LoggerFactory.getLogger( PointsRewardServiceImpl.class );



	@Override
	public PointsTxnModel save( PointsTxnModel model ) {
		if ( null == model ) {
			return null;
		}
		return pointsTxnModelRepo.save( model );
	}

	@Override
	public ResultList<PointsRewardDto> searchRecon( PointsRewardSearchDto dto ){
    	dto.setTxnTypes( new PointTxnType[]{ PointTxnType.RECON } );
		Pageable pageable = SpringDataPagingUtil.INSTANCE.createPageable( dto.getPagination() );
        BooleanExpression filter = dto.createSearchExpression();
        Page<PointsTxnModel> page = ( filter != null )? pointsTxnModelRepo.findAll( filter, pageable ) 
        		: pointsTxnModelRepo.findAll( pageable );
        return new ResultList<PointsRewardDto>( PointsRewardDto.toDtoList( page.getContent() ) );
	}

	@Override
	public List<PointsDto> generateReconPoints( Date postedAfter, Date highestTxnDate, String defaultCron ) {
		if ( null == postedAfter ) {
			ApplicationConfig appConfig = applicationConfigRepo.findByKey( AppKey.REWARDS_JOB_DONE );
			if ( CronExpression.isValidExpression( ( null != appConfig )? appConfig.getValue() : defaultCron ) ) {
				CronExpression cronExp = null;
				try {
					cronExp = new CronExpression( ( null != appConfig )? appConfig.getValue() : defaultCron );
				} 
				catch ( ParseException pe ) {
					LOGGER.error( ":generateLatePostedTxns:: ParseException " + pe.getMessage() );
				}
				postedAfter = DateUtils.addMonths( cronExp.getNextValidTimeAfter( new Date() ), -1 );
			}
			else {
				postedAfter = DateUtils.addMilliseconds( 
						null != highestTxnDate? highestTxnDate : 
							DateUtils.truncate( new DateTime().toDateTime( DateTimeZone.UTC ).toDate(), Calendar.MONTH ), -1 );
			}
		}
		if ( null == highestTxnDate ) {
			highestTxnDate = DateUtils.addMilliseconds( 
					DateUtils.truncate( new DateTime().toDateTime( DateTimeZone.UTC ).toDate(), Calendar.MONTH ), -1 );
		}

		PurchaseTxnSearchDto searchDto = new PurchaseTxnSearchDto();
		searchDto.setPostedAfter( postedAfter );
		searchDto.setTxnDateTo( highestTxnDate );
		searchDto.setValidRecord( true );
		searchDto.setMemberType( codePropertiesService.getDetailMemberTypeEmployee() );
		Iterable<EmployeePurchaseTxnModel> purchases = employeePurchaseTxnRepo.findAll( 
				searchDto.createSearchExpression(), 
				PurchaseTxnSearchDto.createOrderSpecifier( Order.DESC, PurchaseTxnField.txnDate ), 
				PurchaseTxnSearchDto.createOrderSpecifier( Order.ASC, PurchaseTxnField.member ) );

		List<PointsDto> dtos = new ArrayList<PointsDto>();
		if ( null != purchases && purchases.iterator().hasNext() ) {
			Iterator<EmployeePurchaseTxnModel> it = purchases.iterator();
			EmployeePurchaseTxnModel purchase1 = it.next();

			List<Long> ids = new ArrayList<Long>();
			ids.add( purchase1.getMemberModel().getId() );
			Date txnDateFrom = DateUtils.truncate( new DateTime( purchase1.getTransactionDateTime().getTime() ).toDate(), Calendar.MONTH );
			Date txnDateTo = DateUtils.addMilliseconds( 
					DateUtils.ceiling( new DateTime( purchase1.getTransactionDateTime().getTime() ).toDate(), Calendar.MONTH ), -1 );

			searchDto = new PurchaseTxnSearchDto();
			searchDto.setValidRecord( true );
			searchDto.setMemberType( codePropertiesService.getDetailMemberTypeEmployee() );
			List<PointsTxnModel> newRewards = new ArrayList<PointsTxnModel>();

			while ( it.hasNext() ) {
				EmployeePurchaseTxnModel purchase = it.next();
				if ( purchase.getTransactionDateTime().getTime() >= txnDateFrom.getTime() 
						&& purchase.getTransactionDateTime().getTime() <= txnDateTo.getTime() ) {
					ids.add( purchase.getMemberModel().getId() );
					continue;
				}
				searchDto.setMemberIds( ids );
				searchDto.setTxnDateFrom( txnDateFrom );
				searchDto.setTxnDateTo( txnDateTo );
				List<PointsTxnModel> rewards = findMemberForRewardPoints( searchDto );
				if ( CollectionUtils.isNotEmpty( rewards ) ) {
					newRewards.addAll( rewards );
				}

				ids = new ArrayList<Long>();
				ids.add( purchase.getMemberModel().getId() );
				txnDateFrom = DateUtils.truncate( new DateTime( purchase.getTransactionDateTime().getTime() ).toDate(), Calendar.MONTH );
				txnDateTo = DateUtils.addMilliseconds( 
						DateUtils.ceiling( new DateTime( purchase.getTransactionDateTime().getTime() ).toDate(), Calendar.MONTH ), -1 );
			}
			searchDto.setMemberIds( ids );
			searchDto.setTxnDateFrom( txnDateFrom );
			searchDto.setTxnDateTo( txnDateTo );
			List<PointsTxnModel> rewards = findMemberForRewardPoints( searchDto );
			if ( CollectionUtils.isNotEmpty( rewards ) ) {
				newRewards.addAll( rewards );
			}

			if ( CollectionUtils.isNotEmpty( newRewards ) ) {
				QPointsTxnModel qPoint = QPointsTxnModel.pointsTxnModel;
				for ( PointsTxnModel newReward : newRewards ) {
					Tuple oldReward = findRewardedAmount( newReward.getMemberModel().getId(), newReward.getIncDateFrom(), newReward.getIncDateTo() );
					if ( null != oldReward ) {
						newReward.setTransactionAmount( newReward.getTransactionAmount() - 
								( null != oldReward.get( qPoint.transactionAmount.sum() )? oldReward.get( qPoint.transactionAmount.sum() ) : 0 ) );
						newReward.setTransactionPoints( newReward.getTransactionPoints() - 
								( null != oldReward.get( qPoint.transactionPoints.sum() )? oldReward.get( qPoint.transactionPoints.sum() ) : 0 ) );
					}
					newReward.setTransactionType( PointTxnType.RECON );
					if ( null != newReward.getTransactionPoints() && newReward.getTransactionPoints().doubleValue() > 0 ) {
						//newReward.setMemberAccountId(newReward.getMemberModel() != null ? newReward.getMemberModel().getAccountId() : null);
						dtos.add( new PointsDto( pointsTxnModelRepo.save( newReward ) ) );
					}
				}
			}
		}
		return dtos;
	}

	private Tuple findRewardedAmount( Long memberId, Date incDateFrom, Date incDateTo ) {
        JPAQuery query = new JPAQuery( em );
		QPointsTxnModel qPoint = QPointsTxnModel.pointsTxnModel;

		return query.from( qPoint )
			.where( qPoint.memberModel.id.eq( memberId )
					.and( qPoint.transactionType.in( PointTxnType.REWARD, PointTxnType.RECON ) )
					.and( qPoint.status.in( TxnStatus.ACTIVE, TxnStatus.FORAPPROVAL, TxnStatus.EXPIRED, TxnStatus.REJECTED ) )
					.and( qPoint.incDateFrom.eq( incDateFrom ) )
					.and( qPoint.incDateTo.eq( incDateTo ) ) )
			.singleResult( qPoint.transactionAmount.sum(), qPoint.transactionPoints.sum() );
	}

	private List<PointsTxnModel> findMemberForRewardPoints( PurchaseTxnSearchDto searchDto ) {
    	List<PointsTxnModel> points = new ArrayList<PointsTxnModel>();

    	QRewardSchemeModel qScheme = QRewardSchemeModel.rewardSchemeModel;
        Iterable<RewardSchemeModel> schemes = rewardSchemeRepo.findAll( qScheme.status.eq( Status.ACTIVE ), qScheme.valueFrom.desc() );
        if ( null != schemes && schemes.iterator().hasNext() ) {
            Iterator<RewardSchemeModel> it = schemes.iterator();
            RewardSchemeModel scheme = it.next();
            List<MemberForRewardBean> beans = findMemberForReward( searchDto.createSearchExpression(), scheme.getValueFrom(), null );
            points.addAll( setPointsRewards( searchDto.getTxnDateFrom(), searchDto.getTxnDateTo(), scheme, beans ) );
            while ( it.hasNext() ) {
            	scheme = it.next();
            	beans = findMemberForReward( searchDto.createSearchExpression(), scheme.getValueFrom(), scheme.getValueTo() );
                points.addAll( setPointsRewards( searchDto.getTxnDateFrom(), searchDto.getTxnDateTo(), scheme, beans ) );
            }
        }

		return points;
	}

	private List<MemberForRewardBean> findMemberForReward( 
			BooleanExpression filter, 
			Double amtFrom, Double amtTo ) {

        JPAQuery query = new JPAQuery( em );
        QEmployeePurchaseTxnModel qPurchase = QEmployeePurchaseTxnModel.employeePurchaseTxnModel;

        List<Tuple> tuples = query.from( qPurchase )
        		.groupBy( qPurchase.memberModel.id, qPurchase.memberModel.registeredStore )
        		.where( filter )
        		.having( new BooleanBuilder().orAllOf(
        				qPurchase.transactionAmount.sum().goe( amtFrom ),
        				amtTo == null ? null : qPurchase.transactionAmount.sum().loe( amtTo ) ) )
        		.list( qPurchase.memberModel.id, qPurchase.memberModel.registeredStore, qPurchase.transactionAmount.sum() );

        List<MemberForRewardBean> results = Lists.newArrayList();
        for ( Tuple tuple : tuples ) {
        	results.add( new MemberForRewardBean( tuple.get( qPurchase.memberModel.id ),
                    tuple.get( qPurchase.memberModel.registeredStore ), 
                    tuple.get(qPurchase.transactionAmount.sum() ) ) );
        }
        return results;
    }



	@Override
	public ServiceDto generatePointsRewardForApproval() {
        Date inFromDate = CalendarUtil.INSTANCE.getPrevMonthStartDate();
        Date inToDate = CalendarUtil.INSTANCE.getPrevMonthEndDate();

		List<PointsTxnModel> reward = generatePointsReward( inFromDate, inToDate );

		RewardBatchDto rewardBatch = new RewardBatchDto();
		rewardBatch.setRewards( reward );
		rewardBatch.setIncDateFrom( inFromDate );
		rewardBatch.setIncDateTo( inToDate );
		Double totalRewards = Double.MIN_VALUE;
		for ( PointsTxnModel model : reward ) {
			totalRewards += ( null != model.getTransactionPoints() ? model.getTransactionPoints() : 0 );
		}
		//totalRewards = pointsTxnModelRepo.getTotalPointsRewardForProcessing( inFromDate, inToDate );
		rewardBatch.setTotalRewards( totalRewards != null ? totalRewards.longValue() : 0L );

        ServiceDto theSvcResp = new ServiceDto();
    	theSvcResp.setReturnObj( rewardBatch );
    	theSvcResp.setReturnCode( SvcReturnType.SUCCESS );

		LOGGER.info( ":generatePointsRewardForApproval:: totalRewards:" + totalRewards );
    	return theSvcResp;
	}

	@Override
	public ServiceDto getPointsRewardForProcessing() {
		return getPointsRewardForProcessing( null, null );
	}

	@Override
	public ServiceDto getPointsRewardForProcessing( Date inFromDate, Date inToDate ) {

		ServiceDto theSvcResp = new ServiceDto();

		if ( inFromDate != null && inToDate != null ) {
	    	inFromDate = CalendarUtil.INSTANCE.getStartDate( inFromDate );
	    	inToDate = CalendarUtil.INSTANCE.getEndDate( inToDate );
		}

		RewardBatchDto theBatch = new RewardBatchDto();
		//theBatch.setRewards( pointsTxnModelRepo.findPointsRewardForProcessing( inFromDate, inToDate ) );
		theBatch.setIncDateFrom( inFromDate );
		theBatch.setIncDateTo( inToDate );
		Double totalRewards = pointsTxnModelRepo.getTotalPointsRewardForProcessing( inFromDate, inToDate );
		theBatch.setTotalRewards( totalRewards != null ? totalRewards.longValue() : 0L );
		
		theSvcResp.setReturnObj( theBatch );
    	theSvcResp.setReturnCode( SvcReturnType.SUCCESS );

		return theSvcResp;
	}
	
	@Override
	public ResultList<PointsDto> listRewardsForProcessing( PointsSearchDto dto ) {
		ResultList<PointsDto> resultList = pointsTxnModelRepo.listPointsTxnForProcessing(PointTxnType.REWARD, null, null, dto);
		for ( PointsDto pointsDto : resultList.getResults() ) {
			if(StringUtils.isNotBlank(pointsDto.getMemberModel().getRegisteredStore())) {
    			Store store = storeRepo.findByCode(pointsDto.getMemberModel().getRegisteredStore());
    			if(store != null) {
    				pointsDto.getMemberModel().setRegisteredStoreName(store.getName());	
    			}
    		}
		}
		return resultList;
	}

	@Override
	public ServiceDto getPointsRewardForApproval() {
		return getPointsRewardForApproval( null, null );
	}

	@Override
	public ServiceDto getPointsRewardForApproval( Date inFromDate, Date inToDate ) {

		ServiceDto theSvcResp = new ServiceDto();

		if ( inFromDate != null && inToDate != null ) {
	    	inFromDate = CalendarUtil.INSTANCE.getStartDate( inFromDate );
	    	inToDate = CalendarUtil.INSTANCE.getEndDate( inToDate );
		}

		RewardBatchDto theBatch = new RewardBatchDto();
		theBatch.setRewards( pointsTxnModelRepo.findPointsRewardForApproval( inFromDate, inToDate ) );
		theBatch.setIncDateFrom( inFromDate );
		theBatch.setIncDateTo( inToDate );

		theSvcResp.setReturnObj( theBatch );
    	theSvcResp.setReturnCode( SvcReturnType.SUCCESS );

		return theSvcResp;
	}

	@Override
	public ServiceDto rejectPointsRewards( List<String> inRewardIds, RewardBatchDto inReward ) {

		ServiceDto theSvcResp = new ServiceDto();

		List<PointsTxnModel> theRewards = new ArrayList<PointsTxnModel>();

		for ( String theRewardId: inRewardIds ) {
			PointsTxnModel theReward = pointsTxnModelRepo.findOne( theRewardId );
			theReward.setStatus( TxnStatus.REJECTED );
			theReward.setApprovalReason( inReward.getMessage() );
			theRewards.add( pointsTxnModelRepo.save( theReward ) );
		}

		theSvcResp.setReturnObj( theRewards );
		theSvcResp.setReturnCode( SvcReturnType.SUCCESS );

		return theSvcResp;		
	}

    @Override
    public ServiceDto approveAllPointsRewards() {
        LOGGER.info("approve all points rewards");
        //get all points rewards for processing
        PointsSearchDto searchDto = new PointsSearchDto();
        searchDto.setPagination(null);
        ResultList<PointsDto> result = this.listRewardsForProcessing(searchDto);
        ServiceDto returnResult = null;
        if (!CollectionUtils.isEmpty(result.getResults())) {
            //get ids and approve each
            List<String> rewardIds = Lists.newArrayList();
            for (PointsDto dto : result.getResults()) {
                String id = dto.getId();
                rewardIds.add(id);
            }

            if (!CollectionUtils.isEmpty(rewardIds)) {
                returnResult = this.approvePointsRewards(rewardIds);
            }
        }
        return returnResult;
    }

	@Override
	public ServiceDto approvePointsRewards( List<String> inRewardIds ) {

		ServiceDto theSvcResp = new ServiceDto();

		List<PointsTxnModel> theRewards = new ArrayList<PointsTxnModel>();

		for ( String theRewardId: inRewardIds ) {
			PointsTxnModel theReward = pointsTxnModelRepo.findOne( theRewardId );
			theReward.setStatus( TxnStatus.ACTIVE );
			theReward.setCreated( new DateTime() ); //TODO temp fix
			theReward.setTransactionDateTime( new DateTime().toDate() ); //TODO temp fix
			LocalDate expiryDate = LocalDate.fromDateFields(theReward.getTransactionDateTime())
					.plusMonths(theReward.getValidPeriod());
			if(expiryDate.getDayOfMonth() > 1) {
				expiryDate = expiryDate.dayOfMonth().withMinimumValue().plusMonths(1);
			}
			
			theReward.setExpiryDate(expiryDate);
			theRewards.add( pointsTxnModelRepo.save( theReward ) );
		}

		theSvcResp.setReturnObj( theRewards );
		theSvcResp.setReturnCode( SvcReturnType.SUCCESS );

		return theSvcResp;		
	}

	@Override
	public ServiceDto deletePointsReward( String inRewardId ) {

		ServiceDto theSvcResp = new ServiceDto();

		PointsTxnModel theReward = pointsTxnModelRepo.findOne( inRewardId );
		if ( theReward != null ) {
			theReward.setStatus( TxnStatus.DELETED );
			theReward = pointsTxnModelRepo.save( theReward );
		}

		theSvcResp.setReturnObj( theReward );
		theSvcResp.setReturnCode( SvcReturnType.SUCCESS );

		return theSvcResp;		
	}

	@Override
	@Transactional
	public JRProcessor createRewardsJrProcessor( InputStream logo, Date dateFrom, Date dateTo ) {
		Calendar calFrom = null;
		Calendar calTo = null;
		if ( null == dateFrom ) {
			calFrom = CalendarUtil.INSTANCE.getPrevMonthFromCal(null);
		}
		else {
			calFrom = Calendar.getInstance();
			calFrom.setTime( dateFrom );
			calFrom.getTime();
		}
		if ( null == dateTo ) {
			calTo= CalendarUtil.INSTANCE.getPrevMonthToCal(null);
		}
		else {
			calTo = Calendar.getInstance();
			calTo.setTime( dateTo );
			calTo.getTime();
		}

		calTo = CalendarUtil.INSTANCE.getToCal(calTo);
		calFrom = CalendarUtil.INSTANCE.getFromCal(calFrom);

		List<PointsTxnModel> active = pointsTxnModelRepo
				.findRewardsByStatusAndDates(TxnStatus.ACTIVE, calFrom.getTime(), calTo.getTime());
		List<PointsTxnModel> rejected = pointsTxnModelRepo
				.findRewardsByStatusAndDates(TxnStatus.REJECTED, calFrom.getTime(), calTo.getTime());
		List<PointsTxnModel> forApproval = pointsTxnModelRepo
				.findRewardsByStatusAndDates(TxnStatus.FORAPPROVAL, calFrom.getTime(), calTo.getTime());
		Map<String, Object> parameters = new HashMap<String, Object>();
		parameters.put("ACTIVE_VOUCHERS", populateMemberStoreName(active));
		parameters.put("REJECTED_VOUCHERS", populateMemberStoreName(rejected));
		parameters.put("FORAPPROVAL_VOUCHERS", populateMemberStoreName(forApproval));
		parameters.put("logo", logo);
		parameters.put("FROM_DATE", DateUtil.convertDateToString(
				DateUtil.SEARCH_DATE_FORMAT, calFrom.getTime()));
		parameters.put(
				"TO_DATE",
				DateUtil.convertDateToString(DateUtil.SEARCH_DATE_FORMAT,
						calTo.getTime()));
		parameters.put("REPORT_TYPE", messageSource.getMessage(
				"rewards_report_label_report_type", null,
				LocaleContextHolder.getLocale()));
		parameters.put("EMP_ID", messageSource.getMessage(
				"rewards_report_label_employee_id", null,
				LocaleContextHolder.getLocale()));
		parameters.put("EMP_NAME", messageSource.getMessage(
				"rewards_report_label_employee_name", null,
				LocaleContextHolder.getLocale()));
		parameters.put("TOTAL_PURCHASES", messageSource.getMessage(
				"rewards_report_label_purchases_total", null,
				LocaleContextHolder.getLocale()));
		parameters.put("VOUCHER_NO", messageSource.getMessage(
				"rewards_report_label_voucher_no", null,
				LocaleContextHolder.getLocale()));
		parameters.put("EXP_DATE", messageSource.getMessage(
				"rewards_report_label_expiration_date", null,
				LocaleContextHolder.getLocale()));
		parameters.put("VOUCHER_AMOUNT", messageSource.getMessage(
				"rewards_report_label_voucher_amount", null,
				LocaleContextHolder.getLocale()));
		parameters.put("STATUS", messageSource.getMessage(
				"rewards_report_label_status", null,
				LocaleContextHolder.getLocale()));
		parameters.put("STORE", messageSource.getMessage(
				"rewards_report_label_store", null,
				LocaleContextHolder.getLocale()));

		parameters.put("LABEL_ACTIVE", messageSource.getMessage(
				"rewards_report_label_active", null,
				LocaleContextHolder.getLocale()));
		parameters.put("LABEL_REJECTED", messageSource.getMessage(
				"rewards_report_label_rejected", null,
				LocaleContextHolder.getLocale()));
		parameters.put("LABEL_FOR_APPROVAL", messageSource.getMessage(
				"rewards_report_label_forapproval", null,
				LocaleContextHolder.getLocale()));

		Collection activeVouchers = (Collection) parameters.get("ACTIVE_VOUCHERS");
		Collection rejectedVouchers = (Collection) parameters.get("REJECTED_VOUCHERS");
		Collection forApprovalVouchers = (Collection) parameters.get("FORAPPROVAL_VOUCHERS");
		   List<Object> ds = new ArrayList<Object>();
		if(!( activeVouchers.isEmpty() && rejectedVouchers.isEmpty() && forApprovalVouchers.isEmpty())){
		    ds.add(new Object());
		}
		DefaultJRProcessor jrProcessor = new DefaultJRProcessor(
				"reports/employee_reward_report.jasper", ds);
		jrProcessor.setParameters(parameters);
		jrProcessor.setFileResolver(new ClasspathFileResolver("reports"));
		return jrProcessor;
	}
	
	public static class ReportBean {
		private String accountId;
		private String memberName;
		private String registeredStore;
		private Double transactionPoints;
		private Double transactionAmount;
		private String status;
		public String getAccountId() {
			return accountId;
		}
		public void setAccountId(String accountId) {
			this.accountId = accountId;
		}
		public String getMemberName() {
			return memberName;
		}
		public void setMemberName(String memberName) {
			this.memberName = memberName;
		}
		public String getRegisteredStore() {
			return registeredStore;
		}
		public void setRegisteredStore(String registeredStore) {
			this.registeredStore = registeredStore;
		}
		public Double getTransactionPoints() {
			return transactionPoints;
		}
		public void setTransactionPoints(Double transactionPoints) {
			this.transactionPoints = transactionPoints;
		}
		public Double getTransactionAmount() {
			return transactionAmount;
		}
		public void setTransactionAmount(Double transactionAmount) {
			this.transactionAmount = transactionAmount;
		}
		public String getStatus() {
			return status;
		}
		public void setStatus(String status) {
			this.status = status;
		}
	}



	private List<PointsTxnModel> generatePointsReward( Date fromDate, Date toDate ) {

    	List<PointsTxnModel> thePointsTxns = new ArrayList<PointsTxnModel>();
    	/*ea20131016: highest scheme first if order-specified is Order.DESC*/
        List<RewardSchemeModel> schemes = rewardSchemeRepo.findAllActivePointsSchemesSortValueFrom( Order.DESC );
        if (schemes != null && schemes.size() > 0) {
            RewardSchemeModel scheme = schemes.get(0);
            List<MemberForRewardBean> beans = findQualifiedForRewardEmployees(fromDate, toDate, scheme.getValueFrom(), null);
            thePointsTxns.addAll(setPointsRewards( fromDate, toDate, scheme, beans ));
            for (int i = 1; i < schemes.size(); i++) {
                scheme = schemes.get(i);
                beans = findQualifiedForRewardEmployees(fromDate, toDate, scheme.getValueFrom(), scheme.getValueTo());
                thePointsTxns.addAll(setPointsRewards( fromDate, toDate, scheme, beans ));
            }
        }
        List<PointsTxnModel> result = new ArrayList<PointsTxnModel>();
        for ( PointsTxnModel model : thePointsTxns ) {
        	result.add( pointsTxnModelRepo.save( model ) );
        }
		LOGGER.info( ":generatePointsReward:: from: " + fromDate + ", to: " + toDate 
				+ ", points-rewards-generated: " + result.size() );
		return result;
	}

    private List<MemberForRewardBean> findQualifiedForRewardEmployees(Date inIncDateFrom, Date inIncDateTo, Double inAmtFrom,
        Double inAmtTo) {
        JPAQuery theQuery = new JPAQuery(em);
        QEmployeePurchaseTxnModel qPurchaseTxn = QEmployeePurchaseTxnModel.employeePurchaseTxnModel;
        QPointsTxnModel qPointsTxnModel = QPointsTxnModel.pointsTxnModel;

        List<Tuple> tuples = theQuery.from(qPurchaseTxn).
            groupBy(qPurchaseTxn.memberModel.id, qPurchaseTxn.memberModel.registeredStore).
            where(
                qPurchaseTxn.transactionDateTime.goe(inIncDateFrom)
                    .and(qPurchaseTxn.transactionDateTime.loe(inIncDateTo))
                    .and(qPurchaseTxn.memberModel.accountStatus.eq(MemberStatus.ACTIVE))
                    .and(qPurchaseTxn.memberModel.memberType.code.eq(codePropertiesService.getDetailMemberTypeEmployee()))
                    .and(
                        qPurchaseTxn.transactionType.eq(VoucherTransactionType.PURCHASE)
                            .or(qPurchaseTxn.transactionType.eq(VoucherTransactionType.VOID))
                            .or(qPurchaseTxn.transactionType.eq(VoucherTransactionType.ADJUSTED)
                                .and(qPurchaseTxn.status.eq(TxnStatus.ACTIVE))
                            )
                    ).and(
                    new HibernateSubQuery().from(qPointsTxnModel)
                        .where(
                            qPointsTxnModel.memberModel.id.eq(qPurchaseTxn.memberModel.id)
                            	.and( qPointsTxnModel.transactionType.eq( PointTxnType.REWARD ) )                            	
                                .and(
                                    qPointsTxnModel.status.eq(TxnStatus.FORAPPROVAL)
                                        .or(qPointsTxnModel.status.eq(TxnStatus.REJECTED))
                                        .or(qPointsTxnModel.status.eq(TxnStatus.ACTIVE))
                                )
                            .and(qPointsTxnModel.incDateFrom.eq(inIncDateFrom))
                            .and(qPointsTxnModel.incDateTo.eq(inIncDateTo))
                        )
                        .count().lt(1)
                )
            )
            .having(
                new BooleanBuilder().orAllOf(
                    qPurchaseTxn.transactionAmount.sum().goe(inAmtFrom),
                    inAmtTo == null ? null : qPurchaseTxn.transactionAmount.sum().loe(inAmtTo))
            ).list(qPurchaseTxn.memberModel.id, qPurchaseTxn.memberModel.registeredStore, qPurchaseTxn.transactionAmount.sum());

        List<MemberForRewardBean> results = Lists.newArrayList();
        for (Tuple tuple : tuples) {
        	results.add(new MemberForRewardBean( tuple.get(qPurchaseTxn.memberModel.id),
                    tuple.get(qPurchaseTxn.memberModel.registeredStore), tuple.get(qPurchaseTxn.transactionAmount.sum()) ));
        }

		LOGGER.info( ":findQualifiedForRewardEmployees:: txnAmtFrom:" + inAmtFrom + ", txnAmtTo:" + ( null != inAmtTo? inAmtTo : "MAX" )
				+ ", number-of-rewarded-employees: " + ( null != results? results.size() : 0 ) );
        return results;
    }

	private List<ReportBean> populateMemberStoreName(List<PointsTxnModel> list) {
		List<ReportBean> beanList = new ArrayList<ReportBean>();
		for(PointsTxnModel point: list) {
			ReportBean bean = new ReportBean();
			bean.setAccountId(point.getMemberModel().getAccountId());
			bean.setMemberName(point.getMemberModel().getFormattedMemberName());
			if(StringUtils.isNotBlank(point.getMemberModel().getRegisteredStore())) {
				Store store = storeService.getStoreByCode(point.getMemberModel().getRegisteredStore());
				if(store != null)
					bean.setRegisteredStore(store.getCode() + " - " + store.getName());
			}
			bean.setTransactionAmount(point.getTransactionAmount());
			bean.setTransactionPoints(point.getTransactionPoints());
			bean.setStatus(point.getStatus().toString());
			beanList.add(bean);
		}
		return beanList;
	}

    private class MemberForRewardBean {
        private Long memberId;
        private String registeredStore;
        private Double txnAmount;

        private MemberForRewardBean(Long memberId, String registeredStore, Double txnAmount) {
            this.memberId = memberId;
            this.registeredStore = registeredStore;
            this.txnAmount = txnAmount;
        }
    }

	private List<PointsTxnModel> setPointsRewards( 
			Date inFromDate, Date inToDate,
			RewardSchemeModel inScheme, 
			List<MemberForRewardBean> theQualifiedEmps ) {

		List<PointsTxnModel> points = new ArrayList<PointsTxnModel>();

		for ( MemberForRewardBean theMember : theQualifiedEmps ) {
			PointsTxnModel point = new PointsTxnModel();
			String prepend = StringUtils.isNotEmpty(theMember.registeredStore) ? theMember.registeredStore : ""; 
			String id = customIdGeneratorService.generateId(prepend);
			while ( null != pointsTxnModelRepo.findOne( id ) ) {
				id = customIdGeneratorService.generateId(prepend);
			}
			point.setId( id );
			point.setTransactionPoints( inScheme.getAmount() );
			point.setTransactionType( PointTxnType.REWARD );
			point.setStatus( TxnStatus.FORAPPROVAL );
			point.setMemberModel( new MemberModel(theMember.memberId) );
			point.setIncDateFrom( inFromDate );
			point.setIncDateTo( inToDate );
			//thePointTxn.setTransactionAmount( employeePurchaseTxnRepo.
			//findTotalAmount( theMember.memberId, inFromDate, inToDate ) );
			point.setTransactionAmount( theMember.txnAmount );
			point.setValidPeriod(inScheme.getValidPeriod());
			//thePointTxn.setTransactionAmount();
			//thePointTxn.setPurchaseTxnList();
			DateTime dt = new DateTime();
			point.setCreated( dt );
			point.setTransactionNo( DateUtil.convertDateToString( "yyyyMMdd", dt.toDate() ) + theMember.memberId );
			if ( null != UserUtil.getCurrentUser() ) {
				point.setCreateUser( ( null != UserUtil.getCurrentUser() ) ? 
						UserUtil.getCurrentUser().getUsername() : "SYSTEM" );
			}
			
			points.add( point );
		}

		return points;
	}

	@Override
	public ServiceDto resetPointsRewards( List<String> inRewardIds ) {

		ServiceDto theSvcResp = new ServiceDto();

		List<PointsTxnModel> theRewards = new ArrayList<PointsTxnModel>();

		for ( String theRewardId: inRewardIds ) {
			PointsTxnModel theReward = pointsTxnModelRepo.findOne( theRewardId );
			
			// only reset rejected values
			if(theReward.getStatus() != TxnStatus.REJECTED) {
				continue;
			}
			
			theReward.setStatus( TxnStatus.FORAPPROVAL );
			theRewards.add( pointsTxnModelRepo.save( theReward ) );
		}

		theSvcResp.setReturnObj( theRewards );
		theSvcResp.setReturnCode( SvcReturnType.SUCCESS );

		return theSvcResp;		
	}
}
