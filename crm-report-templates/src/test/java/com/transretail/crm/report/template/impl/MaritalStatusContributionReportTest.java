package com.transretail.crm.report.template.impl;

import com.google.common.collect.Maps;
import com.transretail.crm.common.reporting.jasper.JRProcessor;
import com.transretail.crm.common.reporting.service.ReportService;
import com.transretail.crm.core.entity.MemberModel;
import com.transretail.crm.core.entity.PointsTxnModel;
import com.transretail.crm.core.entity.embeddable.CustomerProfile;
import com.transretail.crm.core.entity.enums.Status;
import com.transretail.crm.core.entity.lookup.LookupDetail;
import com.transretail.crm.core.entity.lookup.LookupHeader;
import com.transretail.crm.core.entity.lookup.Store;
import com.transretail.crm.core.repo.*;
import com.transretail.crm.report.template.CommonReportFilter;
import net.sf.jasperreports.engine.JRParameter;
import org.apache.commons.io.FileUtils;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.context.support.MessageSourceResourceBundle;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.io.File;
import java.util.Date;
import java.util.Locale;
import java.util.Map;

/**
 * @author Mike de Guzman
 */
@Transactional
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath*:/META-INF/spring/applicationContext*.xml",
        "/META-INF/spring/applicationContext-test.xml"})
public class MaritalStatusContributionReportTest {

    @Autowired
    private MemberRepo memberRepo;

    @Autowired
    private StoreRepo storeRepo;

    @Autowired
    private PointsTxnModelRepo pointsTxnModelRepo;

    @Autowired
    private LookupDetailRepo lookupDetailRepo;

    @Autowired
    private LookupHeaderRepo lookupHeaderRepo;

    @Autowired
    private MessageSource messageSource;

    @Autowired
    private ReportService reportService;

    @Resource(name = "MaritalStatusContributionReport")
    private MaritalStatusContributionReport maritalStatusContributionReport;

    protected static final String FILTER_TXN_DATE_FROM = CommonReportFilter.DATE_FROM;
    protected static final String FILTER_TXN_DATE_TO = CommonReportFilter.DATE_TO;
    protected static final String FILTER_MARITAL_STATUS = "maritalStatus";
    protected static final String FILTER_STORE = "store";

    private static final Logger LOG = LoggerFactory.getLogger(MaritalStatusContributionReport.class);

    @Before
    public void setup() {
        LookupHeader header = lookupHeaderRepo.saveAndFlush(new LookupHeader("MEM001", "Member Type"));
        lookupDetailRepo.saveAndFlush(new LookupDetail("MTYP001", "Individual", header, Status.ACTIVE));
        lookupDetailRepo.saveAndFlush(new LookupDetail("MTYP003", "Professional", header, Status.ACTIVE));

        MemberModel memberModel = new MemberModel();
        memberModel.setFirstName("First name");
        memberModel.setLastName("Last name");
        memberModel.setMiddleName("Middle Name");
        memberModel.setPin("1234");
        memberModel.setPinEncryped(false);
        memberModel.setEmail("email@email.com");
        memberModel.setUsername("username");
        memberModel.setPassword("password");
        memberModel.setAccountId("12341234");
        memberModel.setContact("0123456789");
        memberModel.setMemberType(lookupDetailRepo.findByCode("MTYP001"));

        CustomerProfile customerProfile = new CustomerProfile();
        customerProfile.setBirthdate(new Date());
        customerProfile.setGender(lookupDetailRepo.findByCode("GEND001"));
        customerProfile.setMaritalStatus(lookupDetailRepo.findByCode("MARI001"));
        customerProfile.setOccupation(lookupDetailRepo.findByCode("OCCU001"));
        memberModel.setCustomerProfile(customerProfile);

        memberRepo.saveAndFlush(memberModel);

        MemberModel memberModel2 = new MemberModel();
        memberModel2.setFirstName("First name2");
        memberModel2.setLastName("Last name2");
        memberModel2.setMiddleName("Middle Name2");
        memberModel2.setPin("12345");
        memberModel2.setPinEncryped(false);
        memberModel2.setEmail("email2@email.com");
        memberModel2.setUsername("username2");
        memberModel2.setPassword("admin2");
        memberModel2.setAccountId("123412342");
        memberModel2.setContact("012356789");
        memberModel2.setMemberType(lookupDetailRepo.findByCode("MTYP003"));

        customerProfile = new CustomerProfile();
        customerProfile.setBirthdate(new Date());
        customerProfile.setGender(lookupDetailRepo.findByCode("GEND002"));
        customerProfile.setMaritalStatus(lookupDetailRepo.findByCode("MARI002"));
        customerProfile.setOccupation(lookupDetailRepo.findByCode("OCCU002"));
        memberModel2.setCustomerProfile(customerProfile);

        memberRepo.saveAndFlush(memberModel2);

        Store store = new Store();
        store.setId(1);
        store.setCode("100");
        store.setName("Store Test");
        storeRepo.saveAndFlush(store);

        PointsTxnModel pointsTxnModel = new PointsTxnModel();
        pointsTxnModel.setId("1");
        pointsTxnModel.setTransactionNo("0001");
        pointsTxnModel.setTransactionDateTime(new Date());
        pointsTxnModel.setTransactionAmount(100000.0);
        pointsTxnModel.setStoreCode("100");
        pointsTxnModel.setMemberModel(memberModel);

        pointsTxnModelRepo.saveAndFlush(pointsTxnModel);

        PointsTxnModel pointsTxnModel2 = new PointsTxnModel();
        pointsTxnModel2.setId("2");
        pointsTxnModel2.setTransactionNo("0002");
        pointsTxnModel2.setTransactionDateTime(new Date());
        pointsTxnModel2.setTransactionAmount(200000.0);
        pointsTxnModel2.setStoreCode("100");
        pointsTxnModel2.setMemberModel(memberModel2);

        pointsTxnModelRepo.saveAndFlush(pointsTxnModel);

    }

    @After
    public void cleanup() {
        memberRepo.deleteAll();
        storeRepo.deleteAll();
        pointsTxnModelRepo.deleteAll();
        lookupDetailRepo.deleteAll();
        lookupHeaderRepo.deleteAll();
    }

    @Test
    public void createJrProcessorTest() throws Exception {
        Map<String, String> map = Maps.newHashMap();
        map.put(FILTER_TXN_DATE_FROM, "01-01-2000");
        map.put(FILTER_TXN_DATE_TO, "01-01-2099");
        map.put(FILTER_MARITAL_STATUS, null);
        map.put(FILTER_STORE, null);

        JRProcessor jrProcessor = maritalStatusContributionReport.createJrProcessor(map);

        File testClassesDir = FileUtils.toFile(Thread.currentThread().getContextClassLoader().getResource(""));
        File output = new File(testClassesDir, "test-marital-status-contribution-report-output.pdf");

        LOG.debug("Output location {}", output.getAbsolutePath());
        output.delete();
        Locale locale = LocaleContextHolder.getLocale();
        jrProcessor.addParameter(JRParameter.REPORT_LOCALE, locale);
        jrProcessor.addParameter(JRParameter.REPORT_RESOURCE_BUNDLE, new MessageSourceResourceBundle(messageSource, locale));
        reportService.exportPdfReport(jrProcessor, output);

        double bytes = output.length();
        double kilobytes = (bytes / 1024);
        LOG.debug("Kilobytes: {}", kilobytes);

    }
}