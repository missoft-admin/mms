package com.transretail.crm.loyalty.dto;

import java.util.List;
import java.util.Set;

import org.joda.time.DateTime;
import org.joda.time.LocalDate;
import org.springframework.format.annotation.DateTimeFormat;

import com.google.common.collect.Lists;
import com.transretail.crm.core.entity.lookup.LookupDetail;

public class MultipleAllocationDto {
	private Set<String> batchRefNo; 
	private String location;
	private String locationName;
	private String transferTo;
	private String transferToName;
	private String newTransferTo;
	private String newTransferToName;
	private LookupDetail status;
	private LookupDetail cardType;
	private String reason;
	private String encodedBy;
	@DateTimeFormat(pattern = "dd-MM-yyyy")
	private LocalDate allocateDate = new LocalDate();
	private DateTime transactionDateTime = new DateTime();
	private List<AllocationDto> allocations = Lists.newArrayList(new AllocationDto());
	
	public MultipleAllocationDto() {}
	
	public MultipleAllocationDto(String location, String locationName,
			String transferTo, String transferToName,
			DateTime transactionDateTime, List<AllocationDto> allocations) {
		super();
		this.location = location;
		this.locationName = locationName;
		this.transferTo = transferTo;
		this.transferToName = transferToName;
		this.transactionDateTime = transactionDateTime;
		this.allocations = allocations;
	}

	public Set<String> getBatchRefNo() {
		return batchRefNo;
	}

	public void setBatchRefNo(Set<String> batchRefNo) {
		this.batchRefNo = batchRefNo;
	}

	public List<AllocationDto> getAllocations() {
		return allocations;
	}

	public void setAllocations(List<AllocationDto> allocations) {
		this.allocations = allocations;
	}

	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		this.location = location;
	}

	public String getTransferTo() {
		return transferTo;
	}

	public void setTransferTo(String transferTo) {
		this.transferTo = transferTo;
	}

	public String getEncodedBy() {
		return encodedBy;
	}

	public void setEncodedBy(String encodedBy) {
		this.encodedBy = encodedBy;
	}

	public LocalDate getAllocateDate() {
		return allocateDate;
	}

	public void setAllocateDate(LocalDate allocateDate) {
		this.allocateDate = allocateDate;
	}

	public LookupDetail getStatus() {
		return status;
	}

	public void setStatus(LookupDetail status) {
		this.status = status;
	}

	public LookupDetail getCardType() {
		return cardType;
	}

	public void setCardType(LookupDetail cardType) {
		this.cardType = cardType;
	}

	public String getReason() {
		return reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	public String getLocationName() {
		return locationName;
	}

	public void setLocationName(String locationName) {
		this.locationName = locationName;
	}

	public String getTransferToName() {
		return transferToName;
	}

	public void setTransferToName(String transferToName) {
		this.transferToName = transferToName;
	}

	public DateTime getTransactionDateTime() {
		return transactionDateTime;
	}
	
	public String getTransactionDateTimeStr() {
		return transactionDateTime.toString("MMMM d, yyyy hh:mm a");
	}

	public void setTransactionDateTime(DateTime transactionDateTime) {
		this.transactionDateTime = transactionDateTime;
	}

	public String getNewTransferTo() {
		return newTransferTo;
	}

	public void setNewTransferTo(String newTransferTo) {
		this.newTransferTo = newTransferTo;
	}

	public String getNewTransferToName() {
		return newTransferToName;
	}

	public void setNewTransferToName(String newTransferToName) {
		this.newTransferToName = newTransferToName;
	}
	
	
	
}
