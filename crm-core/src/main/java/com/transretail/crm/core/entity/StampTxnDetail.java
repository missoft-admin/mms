package com.transretail.crm.core.entity;

import com.mysema.query.annotations.QueryInit;
import com.transretail.crm.core.entity.enums.TxnStatus;
import com.transretail.crm.core.entity.lookup.LookupDetail;
import com.transretail.crm.core.entity.support.CustomAuditableEntity;

import javax.persistence.*;
import java.io.Serializable;

/**
 * @author Mike de Guzman
 */
@Entity(name = "CRM_STAMP_TXN_DTL")
public class StampTxnDetail extends CustomAuditableEntity<Long> implements Serializable {

    private static final long serialVersionUID = 1L;

    @QueryInit("*")
    @ManyToOne
    @JoinColumn(name = "TXN_HDR_ID", nullable = false)
    private StampTxnHeader stampTxnHeader;

    @Column(name = "STATUS")
    @Enumerated(EnumType.STRING)
    private TxnStatus status;

    @ManyToOne
    @JoinColumn(name = "REF_MEDIA_ID")
    private LookupDetail transactionMedia;

    @Column(name = "TXN_AMOUNT")
    private Double transactionAmount;

    @Column(name = "STAMPS_PER_MEDIA")
    private Double stampsPerMedia;

    public StampTxnHeader getStampTxnHeader() {
        return stampTxnHeader;
    }

    public void setStampTxnHeader(StampTxnHeader stampTxnHeader) {
        this.stampTxnHeader = stampTxnHeader;
    }

    public TxnStatus getStatus() {
        return status;
    }

    public void setStatus(TxnStatus status) {
        this.status = status;
    }

    public LookupDetail getTransactionMedia() {
        return transactionMedia;
    }

    public void setTransactionMedia(LookupDetail transactionMedia) {
        this.transactionMedia = transactionMedia;
    }

    public Double getTransactionAmount() {
        return transactionAmount;
    }

    public void setTransactionAmount(Double transactionAmount) {
        this.transactionAmount = transactionAmount;
    }

    public Double getStampsPerMedia() {
        return stampsPerMedia;
    }

    public void setStampsPerMedia(Double stampsPerMedia) {
        this.stampsPerMedia = stampsPerMedia;
    }
}

