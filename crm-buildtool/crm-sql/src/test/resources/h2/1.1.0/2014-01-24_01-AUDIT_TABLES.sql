create table CRM_PRODUCT_GRP_AUD (
        id bigint not null,
        REV bigint not null,
        REVTYPE tinyint,
        BRAND varchar(255),
        EXC_PRODUCTS varchar(4000),
        NAME varchar(255),
        PRODUCTS varchar(4000),
        CLASSIFICATION varchar(15),
        SUBCLASSIFICATION varchar(15),
        primary key (id, REV)
    );

create table CRM_CITY_AUD (
        id bigint not null,
        REV bigint not null,
        REVTYPE tinyint,
        NAME varchar(255),
        PROVINCE bigint,
        primary key (id, REV)
    );

create table CRM_PROVINCE_AUD (
        id bigint not null,
        REV bigint not null,
        REVTYPE tinyint,
        NAME varchar(255),
        primary key (id, REV)
    );

alter table CRM_CITY_AUD 
        add constraint FK_fvelyjb0qdnpskfom6b0dpm8t 
        foreign key (REV) 
        references CRM_REVINFO;

alter table CRM_PRODUCT_GRP_AUD 
        add constraint FK_5smpic5f2ar1y9x415k04hf2v 
        foreign key (REV) 
        references CRM_REVINFO;

alter table CRM_PROVINCE_AUD 
        add constraint FK_qhh3ctxcw0dhrxvpc2k681j1w 
        foreign key (REV) 
        references CRM_REVINFO;