package com.transretail.crm.giftcard.repo;

import com.transretail.crm.common.repo.CrmQueryDslPredicateExecutor;
import com.transretail.crm.core.entity.MemberModel;
import com.transretail.crm.giftcard.entity.GiftCardInventory;
import com.transretail.crm.giftcard.repo.custom.GiftCardInventoryRepoCustom;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @author Allan G. Ramirez (agramirez@exist.com)
 */
@Repository
public interface GiftCardInventoryRepo extends CrmQueryDslPredicateExecutor<GiftCardInventory, Long>, GiftCardInventoryRepoCustom {
    public GiftCardInventory findByBarcode(String barcode); // use same as cardNo

    public List<GiftCardInventory> findByOwningMember_Username(String userName);

    public Page<GiftCardInventory> findByOwningMember_Username(String userName, Pageable pageable);

    public List<GiftCardInventory> findByOwningMember(MemberModel member);
}
