<%@ include file="../common/taglibs.jsp" %>

<style type="text/css">
<!--
  .select-all-header {
      margin-bottom: 10px;
      border-bottom: 1px solid #eee;
      padding-bottom: 10px;

  }
-->
</style>

<div id="content_store_member_form" class="modal hide nofly modal-dialog">

  <div class="modal-content">
    <c:url var="url_storemember" value="/storemember" />
    <c:url var="url_storemember_action" value="${action}" />
    
    <form:form id="storeMemberForm" name="storeMember" modelAttribute="storeMember" action="${url_storemember_action}" class="modal-form form-horizontal" data-url="${url_storemember}">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4><c:out value="${dialogLabel}"/></h4>
      </div>
      
      <div class="modal-body control-group-01">
        <div class="hide alert alert-error" id="content_error"><button type="button" class="close" onclick="$(this).parent().hide();">&times;</button><div><form:errors path="*"/></div></div>
        
        <div class="pull-left">
          <div class="control-group">
            <form:hidden path="id" />
            <label for="store" class="control-label"><spring:message code="label_store_member_store"/><span class="required">*</span></label>
            <div class="controls">
              <form:select path="store" id="store">
                <option />
                <c:forEach var="store" items="${stores}">
                  <c:set var="storeCodeAndDesc" value="${store.code} - ${store.name}"/>
                  <form:option value="${store.code}" label="${storeCodeAndDesc}" />
                </c:forEach>
              </form:select>
            </div>
          </div>
          
          <div class="control-group">
            <label for="company" class="control-label"><spring:message code="label_store_member_company"/></label>
            <div class="controls">
              <form:select path="company" id="company">
                <option />
                <c:forEach var="company" items="${companies}">
                  <c:set var="companyCodeAndDesc" value="${company.code} - ${company.description}"/>
                  <form:option value="${company.code}" label="${companyCodeAndDesc}" />
                </c:forEach>
              </form:select>
            </div>
          </div>
          
          <div class="control-group">
            <label for="cardType" class="control-label"><spring:message code="label_store_member_card_type"/></label>
            <div class="controls">
              <form:select path="cardType" id="cardType">
                <option />
                <c:forEach var="cardType" items="${cardTypes}">
                  <c:set var="cardTypeCodeAndDesc" value="${cardType.code} - ${cardType.description}"/>
                  <form:option value="${cardType.code}" label="${cardTypeCodeAndDesc}" />
                </c:forEach>
              </form:select>
            </div>
          </div>
        </div>
      </div>
      
      <div class="modal-footer">
        <button type="button" class="btn btn-primary" id="saveButton"><spring:message code="label_save" /></button>
	      <button type="button" class="btn" data-dismiss="modal" id="cancelButton"><spring:message code="label_cancel" /></button>
        <!--<input id="saveButton" class="btn btn-primary" type="submit" value="<spring:message code="label_save" />" />
        <input id="cancelButton" class="btn " type="button" value="<spring:message code="label_cancel" />" data-dismiss="modal" />-->
      </div>
    </form:form>
  </div>
  
  <script src='<c:url value="/js/viewspecific/storememberscheme/storeMemberForm.js"/>' type='text/javascript'><![CDATA[&nbsp; ]]></script>
</div>