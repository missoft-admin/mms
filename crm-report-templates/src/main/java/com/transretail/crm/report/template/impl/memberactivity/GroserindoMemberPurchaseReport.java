package com.transretail.crm.report.template.impl.memberactivity;

import com.google.common.collect.Maps;
import com.google.common.collect.Sets;
import com.transretail.crm.common.reporting.jasper.JRProcessor;
import com.transretail.crm.common.util.DateUtil;
import com.transretail.crm.core.entity.enums.ReportType;
import com.transretail.crm.core.entity.lookup.Store;
import com.transretail.crm.core.service.StoreService;
import com.transretail.crm.report.template.*;
import com.transretail.crm.report.template.impl.AbstractMemberPurchaseReport;
import org.apache.commons.lang3.StringUtils;
import org.joda.time.LocalDate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.sql.DataSource;
import java.util.LinkedHashSet;
import java.util.Locale;
import java.util.Map;
import java.util.Set;

/**
 * @author Mike de Guzman
 */
@Service
public class GroserindoMemberPurchaseReport implements ReportTemplate, NoDataFoundSupport {

    @PersistenceContext
    private EntityManager em;
    @Autowired
    private LOVFactory lOVFactory;
    @Autowired
    private StoreService storeService;
    @Autowired
    private MessageSource messageSource;
    @Autowired
    private DataSource dataSource;

    @Override
    public String getName() {
        return "Member Purchase Amount Report";
    }

    @Override
    public String getDescription() {
        return messageSource.getMessage("member.purchase.report.print.label", null, LocaleContextHolder.getLocale());
    }

    @Override
    public Set<FilterField> getFilters() {
        Locale locale = LocaleContextHolder.getLocale();
        LinkedHashSet<FilterField> filters = Sets.newLinkedHashSet();
        Map<String, String> reportTypes = Maps.newHashMap();
        reportTypes.put("weekly", messageSource.getMessage("member.purchase.report.filter.reporttype.weekly", null, locale));
        reportTypes.put("monthly", messageSource.getMessage("member.purchase.report.filter.reporttype.monthly", null, locale));
        boolean required = true;

        filters.add(FilterField.createDropdownField("activityReportType",
                messageSource.getMessage("member.purchase.report.filter.reporttype", null, locale), reportTypes));
        filters.add(FilterField.createDateField(CommonReportFilter.DATE_FROM,
                messageSource.getMessage("member.purchase.report.filter.date.from", null, LocaleContextHolder.getLocale()),
                DateUtil.SEARCH_DATE_FORMAT, required));
        filters.add(FilterField.createDateField(CommonReportFilter.DATE_TO,
                messageSource.getMessage("member.purchase.report.filter.date.to", null, LocaleContextHolder.getLocale()),
                DateUtil.SEARCH_DATE_FORMAT, required));
        filters.add(FilterField.createDropdownField(CommonReportFilter.STORE,
                messageSource.getMessage("member.activity.report.filter.store", null, locale),
                lOVFactory.getStoreOptions()));
        return filters;
    }

    @Override
    public boolean canView(Set<String> permissions) {
        return true;
    }

    @Override
    public JRProcessor createJrProcessor(Map<String, String> parameters) {
        final LocalDate dateFrom = parseDate(parameters.get(CommonReportFilter.DATE_FROM));
        final LocalDate dateTo = parseDate(parameters.get(CommonReportFilter.DATE_TO));
        final String storeCode = parameters.get(CommonReportFilter.STORE);
        final String activityReportType = parameters.get("activityReportType");
        final ReportType reportType = ReportType.valueOf(parameters.get(ReportTemplate.PARAM_REPORT_TYPE).toUpperCase());

        Locale locale = LocaleContextHolder.getLocale();
        String storeName = "";
        if (StringUtils.isNotBlank(storeCode)) {
            Store store = storeService.getStoreByCode(storeCode);
            storeName = store.getName();
        }
        AbstractMemberPurchaseReport jrProcessor = null;
        // TODO : validate diff of weekly and monthly reports
        if ("weekly".equalsIgnoreCase(activityReportType)) {
            jrProcessor = new WeeklyMemberPurchaseJrProcessor("weekly-member-purchase-report",
                    messageSource.getMessage("member.purchase.report.weekly.title", new Object[]{storeName}, locale),
                    messageSource.getMessage("member.purchase.report.list.weekly.title", null, locale),
                    dataSource, reportType);
        } else {
            jrProcessor = new MonthlyMemberPurchaseJrProcessor("mothly-member-purchase-report",
                    messageSource.getMessage("member.purchase.report.monthly.title", new Object[]{storeName}, locale),
                    messageSource.getMessage("member.purchase.report.list.monthly.title", null, locale),
                    dataSource, reportType);
        }
        jrProcessor.setDateFrom(dateFrom);
        jrProcessor.setDateTo(dateTo);
        jrProcessor.setStoreCode(storeCode);
        return jrProcessor;
    }

    @Override
    public ReportType[] getReportTypes() {
        return ReportType.values();
    }

    private LocalDate parseDate(String dateInstance) {
        return StringUtils.isNotBlank(dateInstance) ? INPUT_DATE_PATTERN.parseLocalDate(dateInstance) : null;
    }

    @Override
    public boolean isEmpty(Map<String, String> map) {
        String activityReportType = map.get("activityReportType");
        Locale locale = LocaleContextHolder.getLocale();
        AbstractMemberPurchaseReport jrProcessor = null;

        if ("weekly".equalsIgnoreCase(activityReportType)) {
            jrProcessor = new WeeklyMemberPurchaseJrProcessor("weekly-member-purchase-report",
                    messageSource.getMessage("member.purchase.report.weekly.title", new Object[]{}, locale),
                    messageSource.getMessage("member.purchase.report.list.weekly.title", null, locale),
                    dataSource, null);
        } else {
            jrProcessor = new MonthlyMemberPurchaseJrProcessor("mothly-member-purchase-report",
                    messageSource.getMessage("member.purchase.report.monthly.title", new Object[]{}, locale),
                    messageSource.getMessage("member.purchase.report.list.monthly.title", null, locale),
                    dataSource, null);
        }
        return jrProcessor.isEmpty(map);
    }
}