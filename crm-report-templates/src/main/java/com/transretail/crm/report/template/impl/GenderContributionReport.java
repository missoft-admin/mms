package com.transretail.crm.report.template.impl;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.google.common.collect.Sets;
import com.mysema.query.jpa.impl.JPAQuery;
import com.mysema.query.types.Projections;
import com.mysema.query.types.QBean;
import com.mysema.query.types.expr.BooleanExpression;
import com.mysema.query.types.expr.CaseBuilder;
import com.mysema.query.types.expr.NumberExpression;
import com.mysema.query.types.path.StringPath;
import com.transretail.crm.common.reporting.engine.data.JRQueryDSLDataSource;
import com.transretail.crm.common.reporting.jasper.ClasspathFileResolver;
import com.transretail.crm.common.reporting.jasper.DefaultJRProcessor;
import com.transretail.crm.common.reporting.jasper.JRProcessor;
import com.transretail.crm.common.util.DateUtil;
import com.transretail.crm.core.entity.QMemberModel;
import com.transretail.crm.core.entity.QPointsTxnModel;
import com.transretail.crm.core.entity.enums.ReportType;
import com.transretail.crm.core.entity.lookup.QLookupDetail;
import com.transretail.crm.core.entity.lookup.QStore;
import com.transretail.crm.report.template.CommonReportFilter;
import com.transretail.crm.report.template.FilterField;
import com.transretail.crm.report.template.NoDataFoundSupport;
import com.transretail.crm.report.template.ReportTemplate;
import org.apache.commons.lang.StringUtils;
import org.joda.time.LocalDate;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;

/**
 *
 * @author Vincent Gerard Tan, Mike de Guzman
 */
@Service("GenderContributionReport")
public class GenderContributionReport implements ReportTemplate, NoDataFoundSupport {

    private static Logger LOG = LoggerFactory.getLogger(GenderContributionReport.class);

    protected static final String TEMPLATE_NAME = "Member Contribution By Gender Report";
    protected static final String REPORT_NAME = "reports/member_contribution_by_gender_report.jasper";

    protected static final String FILTER_TXN_DATE_FROM = CommonReportFilter.DATE_FROM;
    protected static final String FILTER_TXN_DATE_TO = CommonReportFilter.DATE_TO;
    protected static final String FILTER_CUSTOMER_TYPE = "customerType";
    protected static final String FILTER_STORE = "store";

    private static final DateTimeFormatter INPUT_DATE_PATTERN = DateTimeFormat.forPattern("dd-MM-yyyy");

    @PersistenceContext
    private EntityManager em;

    @Autowired
    private MessageSource messageSource;

    /**
     * Get the unique template report name
     *
     * @return unique template report name
     */
    @Override
    public String getName() {
        return TEMPLATE_NAME;
    }

    /**
     * Description of this template
     *
     * @return template description
     */
    @Override
    public String getDescription() {
        return messageSource.getMessage("gender.contribution.report.label", (Object[])null, LocaleContextHolder.getLocale());
    }

    @Override
    public Set<FilterField> getFilters() {
        Set<FilterField> filters = Sets.newLinkedHashSet();
        filters.add(FilterField.createDateField(FILTER_TXN_DATE_FROM,
                messageSource.getMessage("label_from", (Object[]) null, LocaleContextHolder.getLocale()),
                DateUtil.SEARCH_DATE_FORMAT, true));
        filters.add(FilterField.createDateField(FILTER_TXN_DATE_TO,
                messageSource.getMessage("label_to", (Object[]) null, LocaleContextHolder.getLocale()),
                DateUtil.SEARCH_DATE_FORMAT, true));

        QStore store = QStore.store;
        Map<String, String> stores = Maps.newLinkedHashMap();
        stores.put("", "");
        stores.putAll(new JPAQuery(em).from(store).distinct().orderBy(
                store.code.asc()).map(store.code, store.code.concat(" - ").concat(store.name)));

        filters.add(FilterField.createDropdownField(
                FILTER_STORE, messageSource.getMessage("label_store", (Object[]) null, LocaleContextHolder.getLocale()), stores));

        QLookupDetail lookupDetail = QLookupDetail.lookupDetail;
        Map<String, String> customerTypes = Maps.newLinkedHashMap();
        customerTypes.put("", "");
        customerTypes.putAll(new JPAQuery(em).from(lookupDetail).where(lookupDetail.header.code.eq("MEM007")).map(lookupDetail.code, lookupDetail.description));

        filters.add(FilterField.createDropdownField(
                FILTER_CUSTOMER_TYPE, messageSource.getMessage("label_customertype", null, LocaleContextHolder.getLocale()), customerTypes));
        return filters;
    }

    /**
     * Check if any permission in set is allowed to view this report
     *
     * @param permissions set of permissions to check
     * @return true if a permission in set is allowed to view this report
     */
    @Override
    public boolean canView(Set<String> permissions) {
        return true;
    }

    @Override
    public JRProcessor createJrProcessor(Map<String, String> map) {
        LocalDate dateFrom = parseDate(map.get(FILTER_TXN_DATE_FROM));
        LocalDate dateTo = parseDate(map.get(FILTER_TXN_DATE_TO));
        DefaultJRProcessor jrProcessor = new DefaultJRProcessor(REPORT_NAME);
        Map<String, Object> reportParams = prepareReportParameters(dateFrom, dateTo);

        // logic to put dataset into reportParams
        QPointsTxnModel points = QPointsTxnModel.pointsTxnModel;
        QMemberModel member = QMemberModel.memberModel;
        QStore store = QStore.store;

        Long nullValue = null;

        StringPath gender = member.customerProfile.gender.code;
        NumberExpression<Long> maleCount = new CaseBuilder()
                .when(gender.eq("GEND001")).then(points.memberModel.id)
                .otherwise(nullValue);
        NumberExpression<Long> femaleCount = new CaseBuilder()
                .when(gender.eq("GEND002")).then(points.memberModel.id)
                .otherwise(nullValue);
        JPAQuery query = createQuery(map)
                .groupBy(points.storeCode, store.name)
                .orderBy(points.storeCode.asc());

        QBean<GenderContributionReportBean> projection = Projections.bean(GenderContributionReportBean.class,
                points.storeCode.as("storeCode"),
                store.name.as("storeName"),
                femaleCount.countDistinct().as("femaleNo"),
                maleCount.countDistinct().as("maleNo")
        );

        reportParams.put("SUB_DATA_SOURCE", new JRQueryDSLDataSource(query, projection));
        jrProcessor.addParameters(reportParams);
        jrProcessor.setFileResolver(new ClasspathFileResolver("reports"));
        return jrProcessor;
    }

    public Map <String, Object> prepareReportParameters(LocalDate dateFrom, LocalDate dateTo) {
        Map<String, Object> reportMap = Maps.newHashMap();
        Locale locale = LocaleContextHolder.getLocale();
        reportMap.put("REPORT_TYPE", messageSource.getMessage("gender.contribution.report.label", null, locale));
        reportMap.put("ROW_NUMBER", messageSource.getMessage("gender.contribution.report.rownum", null, locale));
        reportMap.put("STORE_CODE", messageSource.getMessage("gender.contribution.report.storecode", null, locale));
        reportMap.put("STORE_NAME", messageSource.getMessage("gender.contribution.report.storename", null, locale));
        reportMap.put("FEMALE_NO", messageSource.getMessage("gender.contribution.report.femalenum", null, locale));
        reportMap.put("MALE_NO", messageSource.getMessage("gender.contribution.report.malenum", null, locale));
        reportMap.put("TOTAL_NO", messageSource.getMessage("gender.contribution.report.totalnum", null, locale));
        reportMap.put("FEMALE_RATE", messageSource.getMessage("gender.contribution.report.femalerate", null, locale));
        reportMap.put("MALE_RATE", messageSource.getMessage("gender.contribution.report.malerate", null, locale));
        reportMap.put("DATE_FROM", dateFrom.toString(INPUT_DATE_PATTERN));
        reportMap.put("DATE_TO", dateTo.toString(INPUT_DATE_PATTERN));
        return reportMap;
    }
    @Override
    public ReportType[] getReportTypes() {
        return ReportType.values();
    }

    private LocalDate parseDate(String dateInstance) {
        return INPUT_DATE_PATTERN.parseLocalDate(dateInstance);
    }

    private JPAQuery createQuery(Map<String, String> map) {
        LocalDate dateFrom = parseDate(map.get(FILTER_TXN_DATE_FROM));
        LocalDate dateTo = parseDate(map.get(FILTER_TXN_DATE_TO));
        String storeCode = map.get(FILTER_STORE);
        String customerType = map.get(FILTER_CUSTOMER_TYPE);

        QPointsTxnModel points = QPointsTxnModel.pointsTxnModel;
        QMemberModel member = QMemberModel.memberModel;
        QStore store = QStore.store;

        List<BooleanExpression> exprs = Lists.newArrayList();
        exprs.add(points.transactionDateTime.between(dateFrom.toDate(), dateTo.toDate()));

        if (StringUtils.isNotBlank(storeCode)) {
            exprs.add(points.storeCode.eq(storeCode));
        }

        if (StringUtils.isNotBlank(customerType)) {
            exprs.add(member.memberType.code.eq(customerType));
        }
/*
        select a.store_code as store_code, c.name as store_name,
        count(distinct (case when b.gender = 'GEND001' then a.member_id end)) as male_count,
        count(distinct (case when b.gender = 'GEND002' then a.member_id end)) as female_count
        from crm_points a
        left join crm_member b on a.member_id = b.id
        left outer join store c on c.code = a.store_code
        where
        c.code is not null
        group by a.store_code, c.name
        order by a.store_code;
*/
        JPAQuery query = new JPAQuery(em).from(store, points)
                .leftJoin(points.memberModel, member)
                .where(store.code.isNotNull()
                        .and(store.code.eq(points.storeCode))
                        .and(BooleanExpression.allOf(exprs.toArray(new BooleanExpression[exprs.size()]))));
//                .groupBy(points.storeCode, store.name)
//                .orderBy(points.storeCode.asc());

        return query;
    }

    @Override
    public boolean isEmpty(Map<String, String> map) {
        return !createQuery(map).exists();
    }

    public static class GenderContributionReportBean {

        private String storeCode;
        private String storeName;
        private long femaleNo;
        private long maleNo;
        private long totalNo;
        private String femaleRate;
        private String maleRate;

        public String getStoreCode() {
            return storeCode;
        }

        public void setStoreCode(String storeCode) {
            this.storeCode = storeCode;
        }

        public String getStoreName() {
            return storeName;
        }

        public void setStoreName(String storeName) {
            this.storeName = storeName;
        }

        public long getFemaleNo() {
            return femaleNo;
        }

        public void setFemaleNo(long femaleNo) {
            this.femaleNo = femaleNo;
        }

        public long getMaleNo() {
            return maleNo;
        }

        public void setMaleNo(long maleNo) {
            this.maleNo = maleNo;
        }

        public long getTotalNo() {
            return this.getMaleNo() + this.getFemaleNo();
        }

        public void setTotalNo(long totalNo) {
            this.totalNo = totalNo;
        }

        public String getFemaleRate() {
            if (this.getTotalNo() != 0) {
                BigDecimal female = new BigDecimal(this.femaleNo).divide(new BigDecimal(this.getTotalNo()), 2, RoundingMode.HALF_EVEN).multiply(new BigDecimal(100)).setScale(2, RoundingMode.HALF_EVEN);
                return female.floatValue() + "%";
            } else {
                return BigDecimal.ZERO.setScale(2).floatValue() + "%";
            }
        }

        public void setFemaleRate(String femaleRate) {
            this.femaleRate = femaleRate;
        }

        public String getMaleRate() {
            if (this.getTotalNo() != 0) {
                BigDecimal male = new BigDecimal(this.maleNo).divide(new BigDecimal(this.getTotalNo()), 2, RoundingMode.HALF_EVEN).multiply(new BigDecimal(100)).setScale(2, RoundingMode.HALF_EVEN);
                return male.floatValue() + "%";
            } else {
                return BigDecimal.ZERO.setScale(2).floatValue() + "%";
            }
        }

        public void setMaleRate(String maleRate) {
            this.maleRate = maleRate;
        }
    }
}
