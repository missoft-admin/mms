<%@ include file="../common/taglibs.jsp" %>

<style type="text/css">
<!--
  .select-all-header {
      margin-bottom: 10px;
      border-bottom: 1px solid #eee;
      padding-bottom: 10px;

  }

  

-->
</style>

<div id="content_userForm" class="modal hide  nofly modal-dialog">

  <div class="modal-content">
  <c:url var="url_user" value="/user" />
  <c:url var="url_useraction" value="${action}" />
  <form:form id="userForm" name="user" modelAttribute="user" action="${url_useraction}" class="modal-form form-horizontal" data-url="${url_user}">

    <div class="modal-header">
      <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
      <h4><c:out value="${dialogLabel}" /></h4>     
    </div>

    <div class="modal-body control-group-01">
      <div class="hide alert alert-error" id="content_error"><button type="button" class="close" onclick="$(this).parent().hide();">&times;</button><div><form:errors path="*"/></div></div>

      <div class="pull-left">
        <div class="control-group">
          <label for="username" class="control-label"><spring:message code="user_prop_username" /><span class="required">*</span></label>
          <div class="controls"><form:input path="username" id="username" cssClass="" /></div>
          <form:hidden path="id" />
        </div>
        <c:if test="${action eq '/user/save'}">
        <div class="control-group" id="passowrd_block">
          <label for="password" class="control-label"><spring:message code="user_prop_password" /><span class="required">*</span></label>
          <div class="controls"><form:password path="password" id="password" cssClass="" /></div>
        </div>
        </c:if>
        <div class="control-group">
          
          <div class="controls checkbox"><form:checkbox path="enabled" id="enabled" /><label for="enabled" class="case-cap"><spring:message code="user_prop_enabled"/></label></div>
        </div>


        <div class="control-group">

              <label for="password" class="control-label"><spring:message code="user_prop_authenticationtype" /><span class="required">*</span></label>
              <div class="controls">
              <form:select path="authenticationType" id="authenticationType">
                  <option />
                  <c:forEach var="item" items="${authenticationTypes}">
                      <form:option value="${item.key}" label="${item.value}" />
                  </c:forEach>
              </form:select>
              </div>
        </div>


      </div>

      <div class="pull-left">
        <div class="control-group">
          <label for="firstName" class="control-label"><spring:message code="user_prop_firstname"/></label>
          <div class="controls"><form:input path="firstName" id="firstName"/></div>
        </div>
        <div class="control-group">
          <label for="lastName" class="control-label"><spring:message code="user_prop_lastname"/></label>
          <div class="controls"><form:input path="lastName" id="lastName"/></div>
        </div>
        <div class="control-group">
          <label for="email" class="control-label"><spring:message code="user_prop_email"/></label>
          <div class="controls"><form:input path="email" id="email"/></div>
        </div>
        <div class="control-group">
          <label for="store" class="control-label"><spring:message code="user_prop_store"/><b
					class="required">*</b></label>
          <div class="controls">
            <form:select path="storeCode" id="storeCode">
              <option />
              <c:forEach var="store" items="${stores}">
                <form:option value="${store.code}" label="${store.codeAndName}" />
              </c:forEach>
            </form:select>
          </div>
        </div>
        <div class="control-group">
          <label for="store" class="control-label"><spring:message code="user_prop_inventory"/></label>
          <div class="controls">
            <form:select path="inventoryLocation" id="inventoryLocation">
              <option />
              <c:forEach var="item" items="${inventoryLocations}">
                <form:option value="${item.key}" label="${item.value}" />
              </c:forEach>
            </form:select>
          </div>
        </div>
        
      </div>
      <div class="clearfix"></div>
    </div>

    <div class="modal-footer">
      <button type="saveButton" id="saveButton" class="btn btn-primary"><spring:message code="label_save" /></button>
      <button type="button" class="btn" data-dismiss="modal" id="cancelButton"><spring:message code="label_cancel" /></button>
    </div>

  </form:form>
  </div>


  <script src='<c:url value="/js/viewspecific/user/userForm.js"/>' type='text/javascript'><![CDATA[&nbsp; ]]></script>

<c:if test="${user.inventoryLocation == user.storeCode}">
<script type="text/javascript">
$("#inventoryLocation option[value='selectedStore']").prop("selected", "selected");
</script>
</c:if>

</div>