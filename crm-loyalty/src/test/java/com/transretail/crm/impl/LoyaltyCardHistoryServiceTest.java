package com.transretail.crm.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import junit.framework.Assert;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Configurable;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.security.authentication.TestingAuthenticationToken;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.context.SecurityContextImpl;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import com.google.common.collect.Lists;
import com.mysema.query.types.expr.NumberExpression;
import com.transretail.crm.common.reporting.jasper.JRProcessor;
import com.transretail.crm.common.service.dto.response.ResultList;
import com.transretail.crm.core.entity.UserModel;
import com.transretail.crm.core.entity.lookup.LookupDetail;
import com.transretail.crm.core.entity.lookup.LookupHeader;
import com.transretail.crm.core.repo.LookupDetailRepo;
import com.transretail.crm.core.repo.LookupHeaderRepo;
import com.transretail.crm.core.repo.UserRepo;
import com.transretail.crm.core.security.model.impl.CustomSecurityUserDetailsImpl;
import com.transretail.crm.core.service.CodePropertiesService;
import com.transretail.crm.loyalty.dto.LoyaltyCardTrackerDto;
import com.transretail.crm.loyalty.dto.LoyaltyCardTrackerFilterDto;
import com.transretail.crm.loyalty.dto.LoyaltyCardTrackingDto;
import com.transretail.crm.loyalty.dto.ProductSeriesDto;
import com.transretail.crm.loyalty.entity.LoyaltyCardInventory;
import com.transretail.crm.loyalty.entity.ManufactureOrderTier;
import com.transretail.crm.loyalty.entity.QLoyaltyCardInventory;
import com.transretail.crm.loyalty.entity.QLoyaltyCardInventoryHistory;
import com.transretail.crm.loyalty.repo.LoyaltyCardInventoryHistoryRepo;
import com.transretail.crm.loyalty.repo.LoyaltyCardInventoryRepo;
import com.transretail.crm.loyalty.repo.ManufactureOrderTierRepo;
import com.transretail.crm.loyalty.service.LoyaltyCardHistoryService;

@Configurable
@Transactional
@ContextConfiguration(locations = "classpath*:/META-INF/spring/applicationContext*.xml")
@RunWith(SpringJUnit4ClassRunner.class)
public class LoyaltyCardHistoryServiceTest {
	
	@Autowired
	LoyaltyCardInventoryRepo loyaltyCardInventoryRepo;
	@Autowired
	LoyaltyCardHistoryService loyaltyCardHistoryService;
	@Autowired
	LoyaltyCardInventoryHistoryRepo historyRepo;
	@Autowired
	LookupDetailRepo lookupDetailRepo;
	@Autowired
	LookupHeaderRepo lookupHeaderRepo;
	@Autowired
	ManufactureOrderTierRepo productRepo;
	@Autowired
	CodePropertiesService codePropertiesService;
	@Autowired
	UserRepo userRepo;
	@Autowired
	MessageSource messageSource;
	static Locale defaultLocale = Locale.getDefault();
	@BeforeClass
    public static void setDefaultLocale() {
        Locale.setDefault(Locale.ENGLISH);
    }
    @AfterClass
    public static void restoreLocale() {
        Locale.setDefault(defaultLocale);
    }
	
	@Before
	public void setup() {
		UserModel currentUser = new UserModel();
    	currentUser.setUsername("loggedInUser");
    	currentUser.setPassword("");
    	currentUser.setEnabled(true);
    	currentUser.setInventoryLocation(codePropertiesService.getDetailInvLocationDenpasar());
    	currentUser.setStoreCode( "store1" );
    	userRepo.save(currentUser);

        CustomSecurityUserDetailsImpl user = new CustomSecurityUserDetailsImpl();
        user.setUserId(currentUser.getId());
        user.setUsername(currentUser.getUsername());
        List<GrantedAuthority> authorities = Lists.newArrayList();
        authorities.add(new SimpleGrantedAuthority("ROLE_CSS"));
        user.setAuthorities(authorities);
        user.setInventoryLocation(codePropertiesService.getDetailInvLocationDenpasar());

        SecurityContextImpl securityContext = new SecurityContextImpl();
        securityContext.setAuthentication(new TestingAuthenticationToken(user, ""));
        SecurityContextHolder.setContext(securityContext);
		
		setupLookup();
		
		LookupDetail regular = lookupDetailRepo.findByCode(codePropertiesService.getDetailMemberTierRegular());
		LookupDetail silver = lookupDetailRepo.findByCode(codePropertiesService.getDetailMemberTierSilver());
		
		ManufactureOrderTier product = new ManufactureOrderTier();
		product.setName(regular);
		productRepo.save(product);
		
		long starting = 100000000000L;
		List<LoyaltyCardInventory> inventories = new ArrayList<LoyaltyCardInventory>();
		for(int i = 0; i < 100; i++) {
			LoyaltyCardInventory inventory = new LoyaltyCardInventory();
			inventory.setSequence(String.valueOf(starting + i));
			inventory.setProduct(product);
			inventories.add(inventory);
		}
		
		product = new ManufactureOrderTier();
		product.setName(silver);
		productRepo.save(product);
		
		starting = 200000000000L;
		for(int i = 0; i < 100; i++) {
			LoyaltyCardInventory inventory = new LoyaltyCardInventory();
			inventory.setSequence(String.valueOf(starting + i));
			inventory.setProduct(product);
			inventories.add(inventory);
		}
		
		product = new ManufactureOrderTier();
		product.setName(regular);
		productRepo.save(product);
		
		starting = 300000000000L;
		for(int i = 0; i < 100; i++) {
			LoyaltyCardInventory inventory = new LoyaltyCardInventory();
			inventory.setSequence(String.valueOf(starting + i));
			inventory.setProduct(product);
			inventories.add(inventory);
		}
		
		starting = 400000000000L;
		for(int i = 0; i < 100; i++) {
			LoyaltyCardInventory inventory = new LoyaltyCardInventory();
			inventory.setSequence(String.valueOf(starting + i));
			inventory.setProduct(product);
			inventory.setReferenceNo("111111111");
			inventories.add(inventory);
		}
		
		loyaltyCardInventoryRepo.save(inventories);
		
	}
	
	@After
	public void teardown() {
		SecurityContextHolder.clearContext();
		historyRepo.deleteAll();
		loyaltyCardInventoryRepo.deleteAll();
		lookupDetailRepo.deleteAll();
		lookupHeaderRepo.deleteAll();
		
	}
	
	@Test
	public void testSaveHistoryForInventories() {
		QLoyaltyCardInventory qInventory = QLoyaltyCardInventory.loyaltyCardInventory;
		String headOffice = codePropertiesService.getDetailInvLocationHeadOffice();
		LookupDetail received = new LookupDetail(codePropertiesService.getDetailLoyaltyStatusInactive());
		loyaltyCardHistoryService.saveHistoryForInventories("200000000000", "200000000099", received, headOffice, null, null, true);
		loyaltyCardHistoryService.saveHistoryForInventories("100000000000", "100000000098", received, headOffice, null, null, true);
		countHistory(received, headOffice, 199); //test saveHistoryForInventories
		
		LookupDetail inTransit = new LookupDetail(codePropertiesService.getDetailLoyaltyStatusInTransit());
		NumberExpression<Long> seriesExp = qInventory.sequence.castToNum(Long.class);
		List<LoyaltyCardInventory> inventories = Lists.newArrayList(loyaltyCardInventoryRepo.findAll(seriesExp.goe(300000000000L).and(seriesExp.loe(300000000099L))));
		loyaltyCardHistoryService.saveHistoryForInventories(inTransit, headOffice, null, null, inventories, true);
		countHistory(inTransit, headOffice, 100); //test saveHistoryForInventories
		
		
		LoyaltyCardInventory inventory = loyaltyCardInventoryRepo.findOne(qInventory.sequence.eq("100000000099"));
		LookupDetail active = lookupDetailRepo.findByCode(codePropertiesService.getDetailLoyaltyStatusActive());
		loyaltyCardHistoryService.saveHistoryForInventory(inventory, active, headOffice);
		countHistory(active, headOffice, 1); //test saveHistoryForInventory
		
		loyaltyCardHistoryService.saveHistoryForInventories("111111111", active, headOffice, null, null, true);
		countHistory(active, headOffice, 101); //test saveHistoryForInventories
		
		inventory = loyaltyCardInventoryRepo.findOne(qInventory.sequence.eq("100000000098"));
		loyaltyCardHistoryService.saveHistoryForInventory(inventory, active, headOffice);
		
		LoyaltyCardTrackerFilterDto filterDto = new LoyaltyCardTrackerFilterDto();
		filterDto.setLocation(headOffice);
		filterDto.setProduct(lookupDetailRepo.findByCode(codePropertiesService.getDetailMemberTierRegular()));
		ResultList<LoyaltyCardTrackingDto> result = loyaltyCardHistoryService.trackInventory(filterDto); //test trackInventory
		Assert.assertTrue(result.getTotalElements() == 3);
		
		String activeStr = getTransactionType("tracker_status_active");
		String receiveStr = getTransactionType("tracker_status_receive");
		String transferOutStr = getTransactionType("tracker_status_transferout");
		
		for(LoyaltyCardTrackingDto dto: result.getResults()) {
			if(dto.getTransaction().equalsIgnoreCase(transferOutStr)) {
				Assert.assertTrue(dto.getQuantity() == -100);
			}
			if(dto.getTransaction().equalsIgnoreCase(receiveStr)) {
				Assert.assertTrue(dto.getQuantity() == 99);
			}
			if(dto.getTransaction().equalsIgnoreCase(activeStr)) {
				Assert.assertTrue(dto.getQuantity() == -102);
			}
		}
	}
	
	private String getTransactionType(String code) {
		return messageSource.getMessage(code, null, LocaleContextHolder.getLocale());
	}

	
	private void countHistory(LookupDetail status, String location, long expected) {
		QLoyaltyCardInventoryHistory qHistory = QLoyaltyCardInventoryHistory.loyaltyCardInventoryHistory;
		long actual = historyRepo.count(qHistory.status.eq(status).and(qHistory.location.eq(location)));
		Assert.assertEquals(expected, actual);
	}
	
	private void setupLookup() {

		LookupHeader header = new LookupHeader(codePropertiesService.getHeaderLoyaltyStatus());
		header.setDescription("MO STATUS");
		lookupHeaderRepo.save(header);
		
		LookupDetail detail = new LookupDetail(codePropertiesService.getDetailLoyaltyStatusActive());
		detail.setDescription("ACTIVE");
		detail.setHeader(header);
		lookupDetailRepo.save(detail);
		
		detail = new LookupDetail(codePropertiesService.getDetailLoyaltyStatusInactive());
		detail.setDescription("INACTIVE");
		detail.setHeader(header);
		lookupDetailRepo.save(detail);
		
		detail = new LookupDetail(codePropertiesService.getDetailLoyaltyStatusAllocated());
		detail.setDescription("ALLOCATED");
		detail.setHeader(header);
		lookupDetailRepo.save(detail);
		
		detail = new LookupDetail(codePropertiesService.getDetailLoyaltyStatusForAllocation());
		detail.setDescription("FOR ALLOCATION");
		detail.setHeader(header);
		lookupDetailRepo.save(detail);
		
		detail = new LookupDetail(codePropertiesService.getDetailLoyaltyStatusInTransit());
		detail.setDescription("IN TRANSIT");
		detail.setHeader(header);
		lookupDetailRepo.save(detail);
		
		detail = new LookupDetail(codePropertiesService.getDetailLoyaltyStatusMissing());
		detail.setDescription("MISSING");
		detail.setHeader(header);
		lookupDetailRepo.save(detail);
		
		header = new LookupHeader(codePropertiesService.getHeaderMembershipTier());
		header.setDescription("MEMBERSHIP TIER");
		lookupHeaderRepo.save(header);
		
		detail = new LookupDetail(codePropertiesService.getDetailMemberTierGold());
		detail.setDescription("GOLD");
		detail.setHeader(header);
		lookupDetailRepo.save(detail);
		
		detail = new LookupDetail(codePropertiesService.getDetailMemberTierSilver());
		detail.setDescription("SILVER");
		detail.setHeader(header);
		lookupDetailRepo.save(detail);
		
		detail = new LookupDetail(codePropertiesService.getDetailMemberTierRegular());
		detail.setDescription("REGULAR");
		detail.setHeader(header);
		lookupDetailRepo.save(detail);
		
		header = new LookupHeader(codePropertiesService.getHeaderInventoryLocation());
		header.setDescription("INV LOCATION");
		lookupHeaderRepo.save(header);
		
		detail = new LookupDetail(codePropertiesService.getDetailInvLocationDenpasar());
		detail.setDescription("DENPASAR");
		detail.setHeader(header);
		lookupDetailRepo.save(detail);
		
		detail = new LookupDetail(codePropertiesService.getDetailInvLocationLebakBulus());
		detail.setDescription("LEBAK BULUS");
		detail.setHeader(header);
		lookupDetailRepo.save(detail);
		
		detail = new LookupDetail(codePropertiesService.getDetailInvLocationHeadOffice());
		detail.setDescription("HEAD OFFICE");
		detail.setHeader(header);
		lookupDetailRepo.save(detail);
	}
	
	
}
