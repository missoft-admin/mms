package com.transretail.crm.core.entity;

import com.transretail.crm.core.dto.MemberDto;
import com.transretail.crm.core.entity.embeddable.*;
import com.transretail.crm.core.entity.enums.MemberCreatedFromType;
import com.transretail.crm.core.entity.enums.MemberStatus;
import com.transretail.crm.core.entity.lookup.LookupDetail;
import com.transretail.crm.core.entity.support.CustomAuditableEntity;
import com.transretail.crm.core.util.AppConstants;
import com.transretail.crm.core.util.FormatterUtil;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.BooleanUtils;
import org.apache.commons.lang3.StringUtils;
import org.hibernate.annotations.Type;
import org.hibernate.envers.Audited;
import org.hibernate.envers.NotAudited;
import org.hibernate.validator.constraints.NotEmpty;
import org.joda.time.LocalDate;
import org.springframework.beans.BeanUtils;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import java.io.Serializable;
import java.util.List;


// TODO: *ToOne are eager by default. Need to mark this to lazy to improve performance in fetching members
@Entity
@Table(name = "CRM_MEMBER")
@Audited
public class MemberModel extends CustomAuditableEntity<Long> implements Serializable {

    private static final long serialVersionUID = 1L;


    @Column(name = "FIRST_NAME")
    private String firstName;


    @Column(name = "MIDDLE_NAME")
    private String middleName;


    @Column(name = "LAST_NAME")
    private String lastName;

    /**
     * Encrypted PIN
     */
    @NotEmpty
    @Column(name = "PIN", nullable = false)
    private String pin;

    @Column(name = "PIN_ENCRYPTED", length = 1)
    @Type(type = "yes_no")
    private Boolean pinEncryped;

    @Column(name = "EMAIL")
    @Pattern(regexp = AppConstants.EMAIL_PATTERN)
    private String email;

    @NotEmpty
    @Column(nullable = false, unique = true)
    private String username;

    @Column(name = "PASSWORD")
    private String password;

    @NotNull
    @Column(name = "ACCOUNT_ENABLED", nullable = false, length = 1)
    @Type(type = "yes_no")
    private Boolean enabled = Boolean.TRUE;

    @Column(name = "STAMP_USER", length = 1)
    @Type(type = "yes_no")
    private Boolean stampUser = Boolean.FALSE;

    @Version
    @Column(name = "VERSION")
    private Integer version = 1;

    @NotEmpty
    @Column(name = "ACCOUNT_ID", nullable = false, unique = true)
    private String accountId;

    @Column(name = "COMPANY_NAME")
    private String companyName;

    @Column(name = "STORE_NAME")
    private String storeName;

    @Column(name = "REGISTERED_STORE")
    private String registeredStore;

    @NotEmpty
    @Column(name = "CONTACT", unique = true, nullable = false)
    private String contact;

    @Column(name = "HOME_PHONE")
    private String homePhone;

    @NotAudited
    @Column(name = "TOTAL_POINTS")
    private Double totalPoints;

    @NotAudited
    @OneToMany(mappedBy = "memberModel", orphanRemoval = true, fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private List<PointsTxnModel> pointsDetails;

    @JoinColumn(name="MEMBER_TYPE")
    @ManyToOne
    private LookupDetail memberType;

    @JoinColumn(name="EMP_TYPE")
    @ManyToOne
    private LookupDetail empType;


    @JoinColumn(name="CARD_TYPE")
    @ManyToOne
    private LookupDetail cardType;

    @Column(name="KTP")
    private String ktpId;

    @Column(name="ID_NUMBER")
    private String idNumber;

    @Column(name = "CREATED_FROM")
    @Enumerated(EnumType.STRING)
    private MemberCreatedFromType memberCreatedFromType;

    @Column(name = "VALIDATION_CODE", length = 120)
    private String validationCode;

    @Column(name = "PARENT_ACCOUNT_ID")
    private String parentAccountId;

    @NotAudited
    @OneToMany(mappedBy = "memberModel", orphanRemoval = true, fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private List<EmployeePurchaseTxnModel> employeePurchaseTxnModels;

    @NotNull
    @Column(name = "STATUS", length = 20, nullable = false)
    @Enumerated(EnumType.STRING)
    private MemberStatus accountStatus = MemberStatus.ACTIVE;

    @Embedded
    private Channel channel;

    @Embedded
    private CustomerProfile customerProfile;

    @Embedded
    private MarketingDetails marketingDetails;

    @Embedded
    private Npwp npwp;

    @Embedded
    private ProfessionalProfile professionalProfile;

    @NotAudited
    @JoinColumn(name="ADDRESS")
    @OneToOne(cascade = CascadeType.ALL)
    private Address address;

    @Column(name = "CARD_NO", length = 50)
    private String cardNo;

    @Column(name = "LOYALTY_CARD_NO", unique = true)
    private String loyaltyCardNo;

    @Column(name = "TIME_TO_CALL")
    private String bestTimeToCall;

    //    @Transient
    @Column(name = "TERMINATION_DATE")
    @Type(type = "org.jadira.usertype.dateandtime.joda.PersistentLocalDate")
    private LocalDate terminationDate;

    @Column(name = "SOCIAL_MEDIA_ACCTS")
    private String socialMediaAccts;



    @Embedded
    @NotAudited
    private ConfigurableFields configurableFields;

    private static final String[] IGNORE = {"address", "parent", "pointsDetails",
            "prefStore", "employeePurchaseTxnModels", "contact", "bestTimeToCall"};
    private static final String COMMA = ",";

    public MemberModel() {

    }

    public MemberModel(Long id) {
        setId(id);
    }

    public MemberModel(MemberDto memberDto) {
        BeanUtils.copyProperties(memberDto, this, IGNORE);
        this.pin = memberDto.getEncryptedPin();
        this.pinEncryped = true;
        this.stampUser = memberDto.getStampUser();
        this.parentAccountId = memberDto.getParent();
        if(memberDto.getAddress() != null) {
            this.address = memberDto.getAddress();
        }
        Npwp npwp = new Npwp();
        npwp.setNpwpAddress(memberDto.getNpwpAddress());
        if(StringUtils.isNotBlank(memberDto.getNpwpId()))
            npwp.setNpwpId(memberDto.getNpwpId());
        else
            npwp.setNpwpId(Npwp.DEFAULT_NPWP_ID);
        npwp.setNpwpName(memberDto.getNpwpName());
        this.npwp = npwp;

        List<String> contacts = memberDto.getContact();
        if(contacts != null && contacts.size() > 0) {
            StringBuilder builder = new StringBuilder(contacts.get(0));
            for(int i = 1; i < contacts.size(); i++) {
                builder.append(COMMA).append(contacts.get(i));
            }
            this.contact = builder.toString();
        }

        List<String> times = memberDto.getBestTimeToCall();
        if(times != null && times.size() > 0) {
            StringBuilder builder = new StringBuilder(times.get(0));
            for(int i = 1; i < times.size(); i++) {
                builder.append(COMMA).append(times.get(i));
            }
            this.bestTimeToCall = builder.toString();
        }
        List<String> socialMediaAccts = memberDto.getSocialMediaAccts();
        if( CollectionUtils.isNotEmpty( socialMediaAccts ) ) {
            StringBuilder builder = new StringBuilder( socialMediaAccts.get(0) );
            for( int i = 1; i < socialMediaAccts.size(); i++ ) {
                builder.append( COMMA ).append( socialMediaAccts.get(i) );
            }
            this.socialMediaAccts = builder.toString();
        }
        this.configurableFields = memberDto.getConfigurableFields();
    }


    public MemberCreatedFromType getMemberCreatedFromType() {
        return memberCreatedFromType;
    }

    public void setMemberCreatedFromType(MemberCreatedFromType memberCreatedFromType) {
        this.memberCreatedFromType = memberCreatedFromType;
    }

    public String getParentAccountId() {
        return parentAccountId;
    }

    public void setParentAccountId(String parentAccountId) {
        this.parentAccountId = parentAccountId;
    }

    public void setVersion(Integer version) {
        this.version = version;
    }

    public Integer getVersion() {
        return this.version;
    }

    public void setEnabled(Boolean enabled) {
        this.enabled = BooleanUtils.toBoolean(enabled);
    }

    public Boolean getEnabled() {
        return this.enabled;
    }

    public Boolean getStampUser() {
        return this.stampUser;
    }

    public void setStampUser(Boolean stampUser) {
        this.stampUser = stampUser;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getPassword() {
        return this.password;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getUsername() {
        return this.username;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getEmail() {
        return this.email;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getLastName() {
        return this.lastName;
    }

    public void setMiddleName(String middleName) {
        this.middleName = middleName;
    }

    public String getMiddleName() {
        return this.middleName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getFirstName() {
        return this.firstName;
    }

    @Override
    public String toString() {
        return "MemberModel{" +
                "firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", accountId='" + accountId + '\'' +
                '}';
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public String getStoreName() {
        return storeName;
    }

    public void setStoreName(String storeName) {
        this.storeName = storeName;
    }

    public String getRegisteredStore() {
        return registeredStore;
    }

    public void setRegisteredStore(String registeredStore) {
        this.registeredStore = registeredStore;
    }

    public String getContact() {
        return contact;
    }

    public void setContact(String contact) {
        this.contact = contact;
    }


    public Double getTotalPoints() {


        return totalPoints == null ? 0L : totalPoints;
    }

    public void setTotalPoints(Double totalPoints) {
        this.totalPoints = totalPoints;
    }


    public List<PointsTxnModel> getPointsDetails() {
        return pointsDetails;
    }

    public void setPointsDetails(List<PointsTxnModel> pointsDetails) {
        this.pointsDetails = pointsDetails;
    }

    public String getPin() {
        return pin;
    }

    public void setPin(String pin) {
        this.pin = pin;
    }

    public Boolean getPinEncryped() {
        return pinEncryped;
    }

    public void setPinEncryped(Boolean pinEncryped) {
        this.pinEncryped = pinEncryped;
    }

    public String getAccountId() {
        return accountId;
    }

    public void setAccountId(String accountId) {
        this.accountId = accountId;
    }

    public String getValidationCode() {
        return validationCode;
    }

    public void setValidationCode(String validationCode) {
        this.validationCode = validationCode;
    }

    @Transient
    public String getFormattedMemberName() {
        return FormatterUtil.INSTANCE.formatName(lastName, firstName);

    }

    public List<EmployeePurchaseTxnModel> getEmployeePurchaseTxnModels() {
        return employeePurchaseTxnModels;
    }

    public void setEmployeePurchaseTxnModels(
            List<EmployeePurchaseTxnModel> employeePurchaseTxnModels) {
        this.employeePurchaseTxnModels = employeePurchaseTxnModels;
    }

    public Channel getChannel() {
        return channel;
    }

    public void setChannel(Channel channel) {
        this.channel = channel;
    }

    public CustomerProfile getCustomerProfile() {
        return customerProfile;
    }

    public void setCustomerProfile(CustomerProfile customerProfile) {
        this.customerProfile = customerProfile;
    }

    public LookupDetail getMemberType() {
        return memberType;
    }

    public void setMemberType(LookupDetail memberType) {
        this.memberType = memberType;
    }

    public LookupDetail getEmpType() {
        return empType;
    }

    public void setEmpType(LookupDetail empType) {
        this.empType = empType;
    }

    public LookupDetail getCardType() {
        return cardType;
    }

    public void setCardType(LookupDetail cardType) {
        this.cardType = cardType;
    }

    public Npwp getNpwp() {
        return npwp;
    }

    public void setNpwp(Npwp npwp) {
        this.npwp = npwp;
    }

    public String getCardNo() {
        return cardNo;
    }

    public void setCardNo(String cardNo) {
        this.cardNo = cardNo;
    }

    public String getLoyaltyCardNo() {
        return loyaltyCardNo;
    }

    public void setLoyaltyCardNo(String loyaltyCardNo) {
        this.loyaltyCardNo = loyaltyCardNo;
    }

    public ProfessionalProfile getProfessionalProfile() {
        return professionalProfile;
    }

    public void setProfessionalProfile(ProfessionalProfile professionalProfile) {
        this.professionalProfile = professionalProfile;
    }

    public Address getAddress() {
        return address;
    }

    public void setAddress(Address address) {
        this.address = address;
    }

    public String getKtpId() {
        return ktpId;
    }

    public void setKtpId(String ktpId) {
        this.ktpId = ktpId;
    }

    public MemberStatus getAccountStatus() {
        return accountStatus;
    }

    public void setAccountStatus(MemberStatus accountStatus) {
        this.accountStatus = accountStatus;
    }

    public String getBestTimeToCall() {
        return bestTimeToCall;
    }

    public void setBestTimeToCall(String bestTimeToCall) {
        this.bestTimeToCall = bestTimeToCall;
    }

    public LocalDate getTerminationDate() {
        return terminationDate;
    }

    public void setTerminationDate(LocalDate terminationDate) {
        this.terminationDate = terminationDate;
    }

    public String getHomePhone() {
        return homePhone;
    }

    public void setHomePhone(String homePhone) {
        this.homePhone = homePhone;
    }

    public MarketingDetails getMarketingDetails() {
        return marketingDetails;
    }

    public void setMarketingDetails(MarketingDetails marketingDetails) {
        this.marketingDetails = marketingDetails;
    }

    public String getIdNumber() {
        return idNumber;
    }

    public void setIdNumber(String idNumber) {
        this.idNumber = idNumber;
    }

    public String getSocialMediaAccts() {
        return socialMediaAccts;
    }

    public void setSocialMediaAccts(String socialMediaAccts) {
        this.socialMediaAccts = socialMediaAccts;
    }

    public ConfigurableFields getConfigurableFields() {
        return configurableFields;
    }

    public void setConfigurableFields(ConfigurableFields configurableFields) {
        this.configurableFields = configurableFields;
    }
}

