package com.transretail.crm.core.repo;

import org.springframework.stereotype.Repository;

import com.transretail.crm.common.repo.CrmQueryDslPredicateExecutor;
import com.transretail.crm.core.entity.CrmInStore;

/**
 * @author Allan G. Ramirez (agramirez@exist.com)
 */
@Repository
public interface CrmInStoreRepo extends CrmQueryDslPredicateExecutor<CrmInStore, String> {
}
