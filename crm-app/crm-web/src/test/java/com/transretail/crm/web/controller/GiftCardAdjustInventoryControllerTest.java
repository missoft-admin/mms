package com.transretail.crm.web.controller;

import com.google.common.collect.Lists;
import com.transretail.crm.common.reporting.service.ReportService;
import com.transretail.crm.core.entity.enums.ReportType;
import com.transretail.crm.giftcard.dto.GiftCardPhysicalCountDto;
import com.transretail.crm.giftcard.repo.ProductProfileRepo;
import com.transretail.crm.giftcard.service.GiftCardAdjustmentInventoryService;

import static org.hamcrest.Matchers.*;

import org.joda.time.DateTimeUtils;
import org.joda.time.LocalDate;
import org.joda.time.LocalDateTime;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.MediaType;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.AnnotationConfigWebContextLoader;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

import static org.hamcrest.core.IsInstanceOf.instanceOf;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 *
 * @author Monte Cillo Co (mco@exist.com)
 */
@RunWith(SpringJUnit4ClassRunner.class)
@WebAppConfiguration
@ContextConfiguration(loader = AnnotationConfigWebContextLoader.class)
public class GiftCardAdjustInventoryControllerTest extends AbstractMockMvcTest {

    private static final LocalDate CURRENT_DATE = new LocalDate(2014, 04, 24);
    @Autowired
    private GiftCardAdjustmentInventoryService service;

    @Before
    public void setup() {
	reset(service);
	
	long fixedMillis = DateTimeUtils.getInstantMillis(CURRENT_DATE.toDateTimeAtStartOfDay());
	DateTimeUtils.setCurrentMillisFixed(fixedMillis);
	when(service.getSummaryDates(10l))
		.thenReturn(Lists.newArrayList(new LocalDateTime()));
    }

    @Test
    public void showMainPage() throws Exception {
	mockMvc.perform(get("/giftcard/inventory/adjustment"))
		.andExpect(status().isOk())
		.andExpect(model().size(3))
		.andExpect(model().attribute("giftCardPhysicalCountForm",
				is(instanceOf(GiftCardPhysicalCountDto.class))))
		.andExpect(model().attribute("reportTypes", ReportType.values()))
		.andExpect(model().attribute("summaryDates", Lists.newArrayList(new LocalDateTime())))
		.andExpect(view().name("giftcard/inventory/adjustment"));
    }

    @Test
    public void savePhysicalCount() throws Exception {
	when(service.validateSeries(anyString(), anyString())).thenReturn(Boolean.TRUE);
	when(service.isCounted(anyString(), anyString())).thenReturn(Boolean.FALSE);

	mockMvc.perform(post("/giftcard/inventory/adjustment/adjust")
		.contentType(MediaType.APPLICATION_FORM_URLENCODED)
		.param("location", "HO")
		.param("startingSeries", "1404241010000002")
		.param("endingSeries", "1404241010000010")
		.param("quantity", "8")
		.param("boxNo", "SERIES"))
		.andExpect(status().isOk())
		.andExpect(jsonPath("$.success", is(true)))
		.andDo(print());

	verify(service).savePhysicalCount(Mockito.any(GiftCardPhysicalCountDto.class));
    }

    @Test
    public void validateBarcodeExpectSuccessAndResponseSuccessIsTrue() throws Exception {
	String barcode = "14042410100000021234";
	when(service.isBarcodeExist(barcode)).thenReturn(Boolean.TRUE);

	mockMvc.perform(get("/giftcard/inventory/adjustment/validate/{barcode}", barcode))
		.andExpect(status().isOk())
		.andExpect(jsonPath("$.success", is(true)));

	verify(service).isBarcodeExist(barcode);
    }

    @Test
    public void validateBarcodeExpectSuccessAndResponseSuccessIsFalse() throws Exception {
	String nonExistingBarcode = "14042410100000021234";
	when(service.isBarcodeExist(nonExistingBarcode)).thenReturn(Boolean.FALSE);

	mockMvc.perform(get("/giftcard/inventory/adjustment/validate/{barcode}", nonExistingBarcode))
		.andExpect(status().isOk())
		.andExpect(jsonPath("$.success", is(false)))
		.andExpect(jsonPath("$.result[*]", hasSize(1)));

	verify(service).isBarcodeExist(nonExistingBarcode);
    }

    @Configuration
    @EnableWebMvc
    static class Config {

	//<editor-fold defaultstate="collapsed" desc="Bean Definition">
	@Bean
	public ProductProfileRepo productProfileRepo() {
	    return mock(ProductProfileRepo.class);
	}
	
	@Bean
	public ReportService reportService() {
	    return mock(ReportService.class);
	}

	@Bean
	public MessageSource messageSource() {
	    return mock(MessageSource.class);
	}

	@Bean
	public GiftCardAdjustInventoryController giftCardAdjustInventoryController() {
	    return new GiftCardAdjustInventoryController();
	}

	@Bean
	public GiftCardAdjustmentInventoryService giftCardAdjustmentInventoryService() {
	    return mock(GiftCardAdjustmentInventoryService.class);
	}
	//</editor-fold>
    }
}
