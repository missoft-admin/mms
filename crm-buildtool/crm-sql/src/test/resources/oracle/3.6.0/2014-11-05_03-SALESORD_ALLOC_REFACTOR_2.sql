begin
for soitems in ( select distinct( so_item ) as soItem from crm_sales_ord_alloc ) loop
	begin
	for seri in ( 
			SELECT t1.series AS range_start, MIN(t2.series) AS range_end
			FROM ( SELECT series FROM CRM_GC_INVENTORY tbl1 
			  	WHERE tbl1.id IN ( SELECT gc_inventory FROM CRM_SALES_ORD_ALLOC WHERE so_item=soitems.soItem )
					AND NOT EXISTS( SELECT * FROM CRM_GC_INVENTORY tbl2 WHERE tbl1.series - tbl2.series = 1 AND tbl2.id IN ( SELECT gc_inventory FROM CRM_SALES_ORD_ALLOC WHERE so_item=soitems.soItem ) ) ) t1 
			INNER JOIN ( SELECT series FROM CRM_GC_INVENTORY tbl1 
			  	WHERE tbl1.id IN ( SELECT gc_inventory FROM CRM_SALES_ORD_ALLOC WHERE so_item=soitems.soItem ) 
			  		AND NOT EXISTS( SELECT * FROM crm_gc_inventory tbl2 WHERE tbl2.series - tbl1.series = 1 AND tbl2.id IN ( SELECT gc_inventory FROM CRM_SALES_ORD_ALLOC WHERE so_item=soitems.soItem ) ) ) t2
			ON t1.series <= t2.series GROUP BY t1.series ) loop
	   			INSERT INTO CRM_SALES_ORD_ALLOC ( ID, CREATED_BY, CREATED_DATETIME, SERIES_START, SERIES_END, SO_ITEM ) 
	   				VALUES( hibernate_sequence.nextval, 'SYSTEM-ELMO', CURRENT_TIMESTAMP, seri.range_start, seri.range_end, soitems.soItem );
	end loop;
	end;
end loop;
end;
