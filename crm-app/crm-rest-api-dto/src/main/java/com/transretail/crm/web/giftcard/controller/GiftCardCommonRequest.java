package com.transretail.crm.web.giftcard.controller;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.transretail.crm.common.web.converter.ISODateTimeDeserializer;
import com.transretail.crm.common.web.converter.ISODateTimeSerializer;
import com.transretail.crm.giftcard.dto.GiftCardTransactionInfo;
import com.transretail.crm.web.giftcard.controller.validation.RedemptionCheck;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import org.hibernate.validator.constraints.NotEmpty;
import org.joda.time.LocalDateTime;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.format.annotation.DateTimeFormat.ISO;

/**
 * Used in pre-activation, pre-redeem and inquiry
 *
 * @author Monte Cillo Co (mco@exist.com)
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
@SuppressWarnings({"unchecked"})
public class GiftCardCommonRequest extends AbstractGiftCardRequest {

    @Size(min = 20, max = 20, message = "{giftcard.card.no.length}") // TODO: message
    private String cardNo;
    // TODO: restructure dtos
    @NotNull(groups = RedemptionCheck.class)
    private Double redemptionAmount;
    @NotEmpty(groups = RedemptionCheck.class)
    @Size(max = 20, groups = RedemptionCheck.class)
    private String transactionNo;
    @NotNull(groups = RedemptionCheck.class)
    @DateTimeFormat(iso = ISO.DATE_TIME)
    @JsonSerialize(using = ISODateTimeSerializer.class)
    @JsonDeserialize(using = ISODateTimeDeserializer.class)
    private LocalDateTime transactionDate;
    // TODO: end of restructure code
    private String accountId;

    public String getTransactionNo() {
        return transactionNo;
    }

    public void setTransactionNo(String transactionNo) {
        this.transactionNo = transactionNo;
    }

    public LocalDateTime getTransactionDate() {
        return transactionDate;
    }

    public void setTransactionDate(LocalDateTime transactionDate) {
        this.transactionDate = transactionDate;
    }

    public String getAccountId() {
        return accountId;
    }

    public void setAccountId(String accountId) {
        this.accountId = accountId;
    }

    public String getCardNo() {
        return cardNo;
    }

    public void setCardNo(String cardNo) {
        this.cardNo = cardNo;
    }

    public Double getRedemptionAmount() {
        return redemptionAmount;
    }

    public void setRedemptionAmount(Double redemptionAmount) {
        this.redemptionAmount = redemptionAmount;
    }

    @Override
    public String getSignature() {
        return String.format("%s%s%s%s", getMerchantId(), getTerminalId(), getCashierId(), cardNo);
    }

    @Override
    public GiftCardTransactionInfo toGiftCardTransactionInfo() {
        return super.templateBaseTransactionInfo()
                .transactionNo(transactionNo)
                .transactionTime(transactionDate);
    }
}
