package com.transretail.crm.loyalty.repo;

import org.springframework.stereotype.Repository;

import com.transretail.crm.common.repo.CrmQueryDslPredicateExecutor;
import com.transretail.crm.loyalty.entity.LoyaltyCardInventory;
import com.transretail.crm.loyalty.repo.custom.LoyaltyCardInventoryRepoCustom;

@Repository
public interface LoyaltyCardInventoryRepo extends CrmQueryDslPredicateExecutor<LoyaltyCardInventory, Long>, LoyaltyCardInventoryRepoCustom {

}
