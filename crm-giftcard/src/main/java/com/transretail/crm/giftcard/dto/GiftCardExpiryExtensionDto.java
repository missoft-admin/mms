package com.transretail.crm.giftcard.dto;

import org.joda.time.DateTime;
import org.joda.time.LocalDate;
import org.springframework.format.annotation.DateTimeFormat;

import com.transretail.crm.giftcard.entity.GiftCardExpiryExtension;
import com.transretail.crm.giftcard.entity.support.GiftCardDateExtendStatus;
import com.transretail.crm.giftcard.entity.support.GiftCardHandlingFeeType;

/**
 * @author ftopico
 */
public class GiftCardExpiryExtensionDto {
    
    private String extendNo;
    @DateTimeFormat(pattern = "dd-MM-yyyy")
    private LocalDate createDate = new LocalDate();
    private SalesOrderDto order;
    private Long seriesTo;
    private Long seriesFrom;
    private String filedBy;
    private GiftCardDateExtendStatus status;
    private Double handlingFee;
    private GiftCardHandlingFeeType handlingFeeType;
    
    //For List View
    private String createDateString;
    private String soSeries;
    
    //For Form View
    private String rangeType;
    private String salesOrder;
    private LocalDate expiryDate;
    private Integer extendDays;

	private DateTime lastUpdated;
    
    public GiftCardExpiryExtensionDto() {
        
    }
    
    public GiftCardExpiryExtensionDto(GiftCardExpiryExtension model) {
        this.extendNo = model.getExtendNo();
        this.createDate = model.getCreateDate();
        this.lastUpdated = model.getLastUpdated();
        if (model.getOrder() != null) {
            this.order = new SalesOrderDto(model.getOrder(), true);
            this.rangeType = "so";
            this.salesOrder = model.getOrder().getOrderNo();
        }
        if (model.getSeriesTo() != null && model.getSeriesFrom() != null) {
            this.seriesTo = model.getSeriesTo();
            this.seriesFrom = model.getSeriesFrom();
            this.rangeType = "series";
        }
        this.expiryDate = model.getExpiryDate();
        this.filedBy = model.getFiledBy();
        this.status = model.getStatus();
        if (model.getHandlingFee() != null)
            this.handlingFee = model.getHandlingFee();
        if (model.getHandlingFeeType() != null)
            this.handlingFeeType = model.getHandlingFeeType();
    }
    
    public String getExtendNo() {
        return extendNo;
    }
    public void setExtendNo(String extendNo) {
        this.extendNo = extendNo;
    }
    public LocalDate getCreateDate() {
        return createDate;
    }
    public void setCreateDate(LocalDate createDate) {
        this.createDate = createDate;
    }
    public SalesOrderDto getOrder() {
        return order;
    }
    public void setOrder(SalesOrderDto order) {
        this.order = order;
    }
    public Long getSeriesTo() {
        return seriesTo;
    }
    public void setSeriesTo(Long seriesTo) {
        this.seriesTo = seriesTo;
    }
    public Long getSeriesFrom() {
        return seriesFrom;
    }
    public void setSeriesFrom(Long seriesFrom) {
        this.seriesFrom = seriesFrom;
    }
    public String getFiledBy() {
        return filedBy;
    }
    public void setFiledBy(String filedBy) {
        this.filedBy = filedBy;
    }
    public GiftCardDateExtendStatus getStatus() {
        return status;
    }
    public void setStatus(GiftCardDateExtendStatus status) {
        this.status = status;
    }

    public String getCreateDateString() {
        return createDateString;
    }

    public String getSoSeries() {
        return soSeries;
    }

    public void setCreateDateString(String createDateString) {
        this.createDateString = createDateString;
    }

    public void setSoSeries(String soSeries) {
        this.soSeries = soSeries;
    }

    public String getRangeType() {
        return rangeType;
    }

    public void setRangeType(String rangeType) {
        this.rangeType = rangeType;
    }

    public String getSalesOrder() {
        return salesOrder;
    }

    public void setSalesOrder(String salesOrder) {
        this.salesOrder = salesOrder;
    }

    public LocalDate getExpiryDate() {
        return expiryDate;
    }

    public Integer getExtendDays() {
        return extendDays;
    }

    public void setExpiryDate(LocalDate expiryDate) {
        this.expiryDate = expiryDate;
    }

    public void setExtendDays(Integer extendDays) {
        this.extendDays = extendDays;
    }

    public Double getHandlingFee() {
        return handlingFee;
    }

    public void setHandlingFee(Double handlingFee) {
        this.handlingFee = handlingFee;
    }

    public GiftCardHandlingFeeType getHandlingFeeType() {
        return handlingFeeType;
    }

    public void setHandlingFeeType(GiftCardHandlingFeeType handlingFeeType) {
        this.handlingFeeType = handlingFeeType;
    }

	public DateTime getLastUpdated() {
		return lastUpdated;
	}

	public void setLastUpdated(DateTime lastUpdated) {
		this.lastUpdated = lastUpdated;
	}
    
}
