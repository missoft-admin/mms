package com.transretail.crm.loyalty.repo.custom.impl;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.hibernate.StatelessSession;
import org.joda.time.LocalDateTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import com.google.common.collect.Lists;
import com.mysema.query.BooleanBuilder;
import com.mysema.query.Tuple;
import com.mysema.query.jpa.JPQLQuery;
import com.mysema.query.jpa.hibernate.HibernateSubQuery;
import com.mysema.query.jpa.hibernate.HibernateUpdateClause;
import com.mysema.query.jpa.impl.JPAQuery;
import com.mysema.query.types.expr.BooleanExpression;
import com.mysema.query.types.expr.NumberExpression;
import com.transretail.crm.common.service.dto.request.PageSortDto;
import com.transretail.crm.common.service.dto.request.PagingParam;
import com.transretail.crm.common.service.dto.response.ResultList;
import com.transretail.crm.common.service.util.SpringDataPagingUtil;
import com.transretail.crm.core.entity.lookup.LookupDetail;
import com.transretail.crm.core.security.model.impl.CustomSecurityUserDetailsImpl;
import com.transretail.crm.core.security.util.UserUtil;
import com.transretail.crm.core.service.CodePropertiesService;
import com.transretail.crm.core.service.LookupService;
import com.transretail.crm.loyalty.dto.LoyaltyPhysicalCountSummaryDto;
import com.transretail.crm.loyalty.dto.QLoyaltyPhysicalCountSummaryDto;
import com.transretail.crm.loyalty.entity.LoyaltyCardInventory;
import com.transretail.crm.loyalty.entity.LoyaltyPhysicalCount;
import com.transretail.crm.loyalty.entity.QLoyaltyCardInventory;
import com.transretail.crm.loyalty.entity.QLoyaltyPhysicalCount;
import com.transretail.crm.loyalty.entity.QLoyaltyPhysicalCountSummary;
import com.transretail.crm.loyalty.repo.LoyaltyCardInventoryRepo;
import com.transretail.crm.loyalty.repo.custom.LoyaltyPhysicalCountRepoCustom;
import com.transretail.crm.loyalty.service.LoyaltyCardHistoryService;

public class LoyaltyPhysicalCountRepoImpl implements LoyaltyPhysicalCountRepoCustom {
	@PersistenceContext
	EntityManager em;
	@Autowired
	CodePropertiesService codePropertiesService;
	@Autowired
    StatelessSession statelessSession;
	@Autowired
	LoyaltyCardHistoryService loyaltyCardHistoryService;
	@Autowired
	LoyaltyCardInventoryRepo inventoryRepo;
	@Autowired
	LookupService lookupService;
	
	static Logger logger = LoggerFactory.getLogger(LoyaltyPhysicalCountRepoImpl.class);
	
	private String getCurrentUserLocation() {
		CustomSecurityUserDetailsImpl userDetails = UserUtil.getCurrentUser();
		return userDetails.getInventoryLocation();
	}
	
	@Override
	public ResultList<LoyaltyPhysicalCountSummaryDto> getSummary(PageSortDto sortDto) {
		QLoyaltyPhysicalCount qCount = QLoyaltyPhysicalCount.loyaltyPhysicalCount;
		QLoyaltyCardInventory qInventory = QLoyaltyCardInventory.loyaltyCardInventory;
		
		JPAQuery jpaQuery = new JPAQuery(em);
		PagingParam pagination = sortDto.getPagination();
		
		String[] status = new String[] {codePropertiesService.getDetailLoyaltyStatusInactive(),
				codePropertiesService.getDetailLoyaltyStatusTransferIn(),
				codePropertiesService.getDetailLoyaltyStatusFound()};
		
		BooleanExpression cardTypeFilter = qInventory.location.eq(getCurrentUserLocation())
				.and(qInventory.status.code.in(status));
		
		JPQLQuery query = jpaQuery
				.from(qInventory)
				.where(cardTypeFilter)
				.groupBy(qInventory.product.name.code, qInventory.location);
		
		Long totalCount = new Long(query.list(qInventory.product.name.code, qInventory.location).size());
		
		SpringDataPagingUtil.INSTANCE.applyPagination(query, pagination, LoyaltyPhysicalCount.class);
		
		
		List<Tuple> tuples = query
				.list(qInventory.location,
					qInventory.product.name.code,
					qInventory.count());
		
		List<LoyaltyPhysicalCountSummaryDto> summaries = Lists.newArrayList();
		
		for(Tuple tuple: tuples) {
			LoyaltyPhysicalCountSummaryDto summary = new LoyaltyPhysicalCountSummaryDto();
			summary.setLocation(tuple.get(qInventory.location));
			summary.setCardType(lookupService.getDetailByCode(tuple.get(qInventory.product.name.code)));
			summary.setCurrentInventory(tuple.get(qInventory.count()));
			Long physicalCount = new JPAQuery(em)
				.from(qCount)
				.where(qCount.location.eq(summary.getLocation()))
				.groupBy(qCount.cardType)
				.having(qCount.cardType.eq(summary.getCardType()))
				.singleResult(qCount.quantity.sum());
			if(physicalCount == null)
				physicalCount = 0L;
			summary.setPhysicalCount(physicalCount);
			summaries.add(summary);
		}
		
		return new ResultList<LoyaltyPhysicalCountSummaryDto>(summaries,
				totalCount != null ? totalCount : 0,
				pagination.getPageNo(), pagination.getPageSize());
	}
	
	@Override
	@Transactional
	public long processMissingInventory(Iterable<LoyaltyPhysicalCount> counts) {
		QLoyaltyCardInventory qInventory = QLoyaltyCardInventory.loyaltyCardInventory;
		
		HibernateUpdateClause missingClause = new HibernateUpdateClause(statelessSession, qInventory)
		.set(qInventory.status, new LookupDetail(codePropertiesService.getDetailLoyaltyStatusMissing()));
		
		NumberExpression<Long> seriesExp = qInventory.sequence.castToNum(Long.class);
		
		List<BooleanExpression> filterList = new ArrayList<BooleanExpression>();
		
		for(LoyaltyPhysicalCount count : counts) {
			Long starting = Long.valueOf(count.getStartingSeries());
			Long ending = Long.valueOf(count.getEndingSeries());
			filterList.add(seriesExp.goe(starting).and(seriesExp.loe(ending)));
		}
		
		String[] status = new String[] {codePropertiesService.getDetailLoyaltyStatusInactive(),
				codePropertiesService.getDetailLoyaltyStatusTransferIn(),
				codePropertiesService.getDetailLoyaltyStatusFound()};
		
		BooleanExpression filter = qInventory.location.eq(getCurrentUserLocation())
				.and(qInventory.status.code.in(status))
				.and(qInventory.notIn(new HibernateSubQuery()
					.from(qInventory)
					.where(BooleanExpression.anyOf(filterList.toArray(new BooleanExpression[filterList.size()]))).list(qInventory)));
		
		List<LoyaltyCardInventory> inventories = Lists.newArrayList(inventoryRepo.findAll(filter));
		loyaltyCardHistoryService.saveHistoryForInventories(new LookupDetail(codePropertiesService.getDetailLoyaltyStatusMissing()), getCurrentUserLocation(), null, null, inventories, true);
		
		return missingClause.where(filter).execute();
	}
	
	@Override
	@Transactional
	public long processReceivedInventory(Iterable<LoyaltyPhysicalCount> counts) {
		QLoyaltyCardInventory qInventory = QLoyaltyCardInventory.loyaltyCardInventory;
		
		HibernateUpdateClause receivedClause = new HibernateUpdateClause(statelessSession, qInventory)
		.set(qInventory.status, new LookupDetail(codePropertiesService.getDetailLoyaltyStatusFound()))
		.set(qInventory.location, getCurrentUserLocation())
		.setNull(qInventory.transferTo)
		.setNull(qInventory.referenceNo)
		.setNull(qInventory.batchRefNo);
		
		NumberExpression<Long> seriesExp = qInventory.sequence.castToNum(Long.class);
		
		List<BooleanExpression> filterList = new ArrayList<BooleanExpression>();
		
		for(LoyaltyPhysicalCount count : counts) {
			Long starting = Long.valueOf(count.getStartingSeries());
			Long ending = Long.valueOf(count.getEndingSeries());
			filterList.add(seriesExp.goe(starting).and(seriesExp.loe(ending)));
		}
		
		BooleanExpression filter = qInventory.in(new HibernateSubQuery()
					.from(qInventory)
					.where(BooleanExpression.anyOf(filterList.toArray(new BooleanExpression[filterList.size()]))).list(qInventory))
					.andAnyOf(qInventory.status.code.eq(codePropertiesService.getDetailLoyaltyStatusMissing()).and(qInventory.location.eq(getCurrentUserLocation())), 
							qInventory.location.ne(getCurrentUserLocation()));
		
		List<LoyaltyCardInventory> inventories = Lists.newArrayList(inventoryRepo.findAll(filter));
		loyaltyCardHistoryService.saveHistoryForInventories(new LookupDetail(codePropertiesService.getDetailLoyaltyStatusFound()), getCurrentUserLocation(), null, null, inventories, true);
		
		return receivedClause.where(filter).execute();
	}
	
	@Override
	public List<LocalDateTime> getSummaryDates() {
		QLoyaltyPhysicalCountSummary qSummary = QLoyaltyPhysicalCountSummary.loyaltyPhysicalCountSummary;
		JPQLQuery query = new JPAQuery(em)
				.from(qSummary)
				.where(qSummary.location.eq(getCurrentUserLocation()))
				.groupBy(qSummary.created);
		return query.list(qSummary.created);
	}
	
	
}
