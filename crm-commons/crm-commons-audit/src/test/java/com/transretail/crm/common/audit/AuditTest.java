package com.transretail.crm.common.audit;

import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyString;
import static org.mockito.Matchers.argThat;
import static org.mockito.Mockito.reset;
import static org.mockito.Mockito.verify;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentMatcher;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.transretail.crm.common.audit.handler.SaveHandler;
import com.transretail.crm.common.audit.service.EnrollmentService;
import com.transretail.crm.common.audit.service.Name;
import com.transretail.crm.common.audit.service.Student;
import com.transretail.crm.common.audit.service.StudentService;

/**
 * @author Allan G. Ramirez (agramirez@exist.com)
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("classpath:audit-context.xml")
public class AuditTest {
    @Autowired
    private SaveHandler handler;
    @Autowired
    private EnrollmentService enrollmentService;
    @Autowired
    private StudentService studentService;

    @Before
    public void setUp() {
        reset(handler);
    }

    @Test
    public void nestedAuditedMethodCall() {
        enrollmentService.enroll(createStudent("Cloud Xavier", "Davin", "Ramirez"));
        verify(handler).saveMessage(argThat(new ArgumentMatcher<AuditStatus>() {
            @Override
            public boolean matches(Object argument) {
                return argument == AuditStatus.SUCCESS;
            }
        }), argThat(new ArgumentMatcher<String>() {
            @Override
            public boolean matches(Object argument) {
                return argument.equals("com.transretail.crm.common.audit.service.impl.EnrollmentServiceImpl");
            }
        }),
            argThat(new ArgumentMatcher<AuditMessage>() {
                @Override
                public boolean matches(Object argument) {
                    AuditMessage message = (AuditMessage) argument;
                    boolean nameMatches = message.getName().equals("enroll");
                    boolean descMatches = message.getDescription().equals("Enrolling Ramirez, Cloud Xavier Davin");
                    boolean rawDescMatches = message.getRawDesription().equals("Enrolling $student");
                    boolean auditParamMapMatches = message.getAuditParamMap().size() == 1 && message.getAuditParamMap().get("$student")
                        .equals("Ramirez, Cloud Xavier Davin");
                    return nameMatches && descMatches && rawDescMatches && auditParamMapMatches;
                }
            }));
        verify(handler).saveMessage(any(AuditStatus.class), anyString(), any(AuditMessage.class));
    }


    public Student createStudent(String fname, String mname, String lname) {
        Student student = new Student();
        student.setId(1L);
        student.setName(new Name(fname, mname, lname));
        return student;
    }
}
