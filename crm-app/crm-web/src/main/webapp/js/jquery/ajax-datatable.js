if ( jQuery().dataTable && !jQuery().ajaxDataTable ) {
    // Utility
    if ( typeof Object.create !== 'function' ) {
        Object.create = function ( obj ) {
            function F() {
            }

            F.prototype = obj;
            return new F();
        };
    }

    (function ( $, window, document, undefined ) {
        var AjaxDataTable = {
            init : function ( elem, options, formDataObject ) {
                var self = this;
                if ( self.table !== undefined ) {
                    // Do nothing if ui is already drawn
                    return;
                }
                self._validate( options );

                self.formDataObject = formDataObject;
                self.elem = elem;
                self.$elem = $( elem ); // jQuery elem
                self.options = $.extend( {}, $.fn.ajaxDataTable.options, options );
                self.pageNo = 0;
                self.table = document.createElement( 'table' );

                self.table.className = 'display table' + (typeof options.tableClass === 'string' ? options.tableClass : '');
                var tableHtml = '';
                tableHtml += '';
                tableHtml += '<thead>';
                tableHtml += '    <tr>';
                for ( var i = 0; i < options.columnHeaders.length; i++ ) {
                    if ( typeof options.columnHeaders[i] === 'string' ) {
                        tableHtml += '<th>' + options.columnHeaders[i] + '</th>';
                    } else {
                        var headObject = options.columnHeaders[i];
                        tableHtml += '<th class="' + headObject.className + '">' + headObject.text + '</th>';
                    }

                }
                tableHtml += '    </tr>';
                tableHtml += '</thead>';
                tableHtml += '<tbody></tbody>';
                $( self.table ).html( tableHtml );
                self.$elem.append( self.table );

                if ( options != undefined ) {
                    if ( options.autoload === true ) {
                        self._buildAjaxDataTable();
                    }
                }
            },
            search : function ( formDataObject, fnCallback ) {
                var self = this;
                self.pageNo = 0;
                if ( formDataObject !== undefined && formDataObject != null ) {
                    // Reuse what's already set in init function or what's already set in previous search
                    // if no formDataObject parameter passed. This function will act as reload if search
                    // with formDataObject is already done.
                    self.formDataObject = formDataObject;
                }
                if ( self.dataTableObject === undefined ) {
                    self._buildAjaxDataTable();
                } else {
//                    self.dataTableObject.fnReloadAjax();
                    self.dataTableObject.dataTable().fnDraw();
                }
                if ( typeof fnCallback == 'function' && fnCallback != null ) {
                    fnCallback();
                }
            },
            _buildAjaxDataTable : function () {
                var self = this;
                if ( self.formDataObject === undefined ) {
                    self.formDataObject = new Object();
                }

                var column = [];
                var aoColumns = [];
                var aoColumnDefs = [];
                for ( var i = 0; i < self.options.modelFields.length; i++ ) {
                    var field = self.options.modelFields[i];

                    if ( typeof field === 'string' ) {
                        column[i] = field;
                        aoColumns[i] = { mData : field, bSortable : false };
                    } else {
                        // Assumed to be object. Add checking here if necessary
                        if ( field.customCell != undefined && $.isFunction( field.customCell ) ) {
                            aoColumns[i] = { mData : null, bSortable : false, mRender : field.customCell };
                        } else {
                            if ( field.name == undefined || field.name.trim() === '' ) {
                                $.error( "Field name is required" );
                            }
			    column[i] = field.name;
			    var al = "dataTables_align_" + (field.alignment == undefined ? "left" : field.alignment) 
			    	+ " " + (field.aaClassname == undefined ? "" : field.aaClassname); // TODO: please refactoring for better api
                            aoColumns[i] = { mData : field.name, bSortable : field.sortable !== undefined && field.sortable === true, sClass: al };
                            if ( field.fieldCellRenderer != undefined && $.isFunction( field.fieldCellRenderer ) ) {
                                aoColumnDefs[aoColumnDefs.length] = { aTargets : [i], mRender : field.fieldCellRenderer};
                            }
                        }
                    }
                }

                if ( self.options.aaSorting ) {
                    self.options.aaSorting = self.options.aaSorting;
                }

                var isSortable = function ( columnName ) {
                    for ( var j = 0; j < self.options.modelFields.length; j++ ) {
                        var field = self.options.modelFields[j];
                        if ( columnName == field.name && field.sortable !== undefined && field.sortable === true ) {
                            return true;
                        }
                    }
                    return false;
                };
                
                var getDataType = function ( columnName ) {
                	for ( var j = 0; j < self.options.modelFields.length; j++ ) {
                        var field = self.options.modelFields[j];
                        if ( columnName == field.name ) {
                            return field.dataType;
                        }
                    }
                	return null;
                };

                self.options.sAjaxSource = self.options.ajaxSource;
                self.options.sAjaxDataProp = self.options.resultListName;
                self.options.iDisplayLength = self.options.pageSize;
                self.options.sPaginationType = self.options.paginationType;
                if ( self.options.bDestroy === undefined ) {
                    self.options.bDestroy = true;
                }
                if ( self.options.bFilter === undefined ) {
                    self.options.bFilter = false;
                }
                self.options.aLengthMenu = [
                    [10, 25, 50, 100],
                    [10, 25, 50, 100]
                ];

                self.options.fnInitComplete = function ( oSettings, json ) {
                    if ( self.options.initCompleteCallback != undefined ) {
                        self.options.initCompleteCallback( self );
                    }
                };
                self.options.aoColumns = aoColumns;
                self.options.aoColumnDefs = aoColumnDefs;
                self.options.bServerSide = true;
                self.options.bProcessing = true;
                // Make sure jquery.dataTables.linkLengthMenu.js is loaded so that L feature would take effect
                self.options.sDom = "ifrtLp";
                self.options.oLanguage = {
                    "sInfoEmpty" : "No entries to show",
                    "sInfo" : "showing: _START_ to _END_ (_TOTAL_)",
                    "sInfoFiltered" : "",
                    "sLengthMenu" : "# per page: _MENU_",
                    "oPaginate" : {
                        "sPrevious" : "Prev",
                        "sNext" : "Next"
                    }
                }
                var dataTableSortColIndex = 'iSortCol_';
                self.options.fnServerData = function ( sSource, aoData, fnCallback, oSettings ) {
                    self.formDataObject.pagination = new Object();

                    var pagingSort = new Object();
                    var dataTypeMap = new Object();
                    for ( var i = 0; i < aoData.length; i++ ) {
                        if ( aoData[i].name.substring( 0, dataTableSortColIndex.length ) == dataTableSortColIndex ) {
                            var sortColIndex = aoData[i].value;
                            var columnName = column[parseInt( sortColIndex )];
                            i++; // Next object is sSortDir_. Verify if datatable version is upgraded
                            if ( isSortable( columnName ) ) {
                                var sortDir = aoData[i].value;
                                if ( self.options.fieldModelPropertyMap[columnName] !== undefined ) {
                                    columnName = self.options.fieldModelPropertyMap[columnName];
                                }
                                pagingSort[columnName] = sortDir == 'asc' ? true : false;
                                
                                var dataType = getDataType( columnName );
                                if(dataType != null)
                                	dataTypeMap[columnName] = dataType;
                            }
                        }
                    }
                    self.formDataObject.pagination.pagingSort = pagingSort;
                    self.formDataObject.pagination.dataTypes = dataTypeMap;

                    if ( !oSettings.bSorted ) {
                        self.pageNo = Math.ceil( oSettings._iDisplayStart / oSettings._iDisplayLength );
                    } else {
                        oSettings._iDisplayStart = self.pageNo * oSettings._iDisplayLength;
                    }
                    self.formDataObject.pagination.pageNo = self.pageNo;
                    self.formDataObject.pagination.pageSize = oSettings._iDisplayLength;

                    var formData = JSON.stringify( self.formDataObject );
                    $.ajax( {
                        headers : { 'Content-Type' : 'application/json' },
                        "type" : "POST",
                        "url" : sSource,
                        "data" : formData,
                        "success" : function ( data ) {
                            data.iTotalRecords = data.numberOfElements;
                            data.iTotalDisplayRecords = data.totalElements;
                            fnCallback( data );
                        },
                        "error" : function ( xhr, textStatus, error ) {
                            if ( textStatus === 'timeout' ) {
                                alert( 'The server took too long to send the data.', 'Timed-out' );
                            }
                            else {
                            	if ( xhr.readyState == 0 ) {
                            		xhr.abort();
                            	}
                            	else {
                            		alert( 'An error occurred on the server. Please try again. ' + error, 'Server Error' );
                                }
                            }
                            dataTableObject.fnProcessingIndicator( false );
                        }
                    } );
                };

                self.options = $.extend( {}, $.fn.dataTable.defaults, self.options );

                var dataTableObject = $( self.table ).dataTable( self.options );
                self.dataTableObject = dataTableObject;
            },
            _validate : function ( options ) {
                if ( options.ajaxSource == undefined || '' == options.ajaxSource.trim() ) {
                    $.error( 'Property "ajaxSource" option is required. Eg. { ajaxSource : "/users/list.html"}' );
                }
                if ( options.modelFields == undefined || !$.isArray( options.modelFields ) ) {
                    var message = 'Property "modelFields" option array is required';
                    message += 'Eg. { ' +
                            'modelFields : [' +
                            '"firstname", "lastname", ' +
                            '{ "name" : "middlename", "sortable" : true}, ' +
                            '{ "name" : "fullname", "sortable" : true, fieldCellRenderer : function(data, type, row) { return composeName(data, row.firstName, row.middleName).toFullNameFormat(); } }, ' +
                            '{ customCell : function (innerData, sSpecific, json) {  if (sSpecific == "display") return "cloudx";  else  return ""; } } ' +
                            ']}';
                    $.error( message );
                }
                if ( options.columnHeaders == undefined || !$.isArray( options.columnHeaders ) ) {
                    $.error( 'Property "columnHeaders" option array is required.' );
                }
                // XXX: This is experimental. Remove it's logical to remove this.
                if ( options.columnHeaders.length != options.modelFields.length ) {
                    $.error( 'Property "columnHeaders" length must match with "modelFields" lenght.' );
                }
            }
        };

        $.fn.ajaxDataTable = function ( method ) {
            var args = arguments;
            if ( 'datatableLoaded' === method ) {
                var ajaxDataTableInstance = $.data( this.get( 0 ), 'ajaxDataTableInstance' );
                return ajaxDataTableInstance != null && ajaxDataTableInstance.dataTableObject !== undefined;
            } else if ( 'getDataTableObject' === method ) {
                var ajaxDataTableInstance = $.data( this.get( 0 ), 'ajaxDataTableInstance' );
                return ajaxDataTableInstance.dataTableObject;
            } else if ( AjaxDataTable[method] ) {
                return this.each( function () {
                    return AjaxDataTable[ method ].apply( $.data( this, 'ajaxDataTableInstance' ), Array.prototype.slice.call( args, 1 ) );
                } );
            } else if ( typeof method === 'object' || !method ) {
                return this.each( function () {
                    var elem = this;
                    var options = args[0];
                    if ( args.length == 2 ) {
                        var formDataObject = args[1];
                    }
                    var ajaxDataTableInstance = Object.create( AjaxDataTable );
                    AjaxDataTable.init.call( ajaxDataTableInstance, elem, options, formDataObject );
                    $( elem ).data( 'ajaxDataTableInstance', ajaxDataTableInstance );
                } );
            } else {
                $.error( 'AjaxDataTable ' + method + ' does not exist on jQuery.ajaxDataTable' );
            }
        };

        $.fn.ajaxDataTable.options = {
            "autoload" : false,
            "resultListName" : 'results',
            "pageSize" : 10,
            "fieldModelPropertyMap" : [],
            // Make sure jquery.dataTables.customPaging.js is loaded
            "paginationType" : "custom_paging",
            "initCompleteCallback" : function ( dataTable ) {
                    $('.tiptip').tooltip(); // apply tooltips to all datatables;
            },
               "fnDrawCallback" : function(dataTable) {
                    $('.tiptip').tooltip(); // apply tooltips to all datatables;
										//this.fnAdjustColumnSizing(false);
               }
	
        };
    })( jQuery, window, document );
}