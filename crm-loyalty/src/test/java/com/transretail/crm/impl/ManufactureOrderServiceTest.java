package com.transretail.crm.impl;

import java.util.ArrayList;
import java.util.List;

import org.joda.time.LocalDate;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Configurable;
import org.springframework.security.authentication.TestingAuthenticationToken;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.context.SecurityContextImpl;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import com.google.common.collect.Lists;
import com.google.common.collect.Sets;
import com.transretail.crm.common.service.dto.response.NameIdPairResultList;
import com.transretail.crm.core.entity.UserModel;
import com.transretail.crm.core.entity.UserPermission;
import com.transretail.crm.core.entity.UserRoleModel;
import com.transretail.crm.core.entity.UserRolePermission;
import com.transretail.crm.core.entity.enums.Status;
import com.transretail.crm.core.entity.lookup.LookupDetail;
import com.transretail.crm.core.entity.lookup.LookupHeader;
import com.transretail.crm.core.repo.LookupDetailRepo;
import com.transretail.crm.core.repo.LookupHeaderRepo;
import com.transretail.crm.core.repo.UserPermissionRepo;
import com.transretail.crm.core.repo.UserRepo;
import com.transretail.crm.core.repo.UserRolePermissionRepo;
import com.transretail.crm.core.repo.UserRoleRepo;
import com.transretail.crm.core.security.model.impl.CustomSecurityUserDetailsImpl;
import com.transretail.crm.core.service.CodePropertiesService;
import com.transretail.crm.giftcard.entity.CardVendor;
import com.transretail.crm.giftcard.repo.CardVendorRepo;
import com.transretail.crm.loyalty.dto.ManufactureOrderDto;
import com.transretail.crm.loyalty.dto.ManufactureOrderTierDto;
import com.transretail.crm.loyalty.entity.ManufactureOrder;
import com.transretail.crm.loyalty.entity.ManufactureOrderTier;
import com.transretail.crm.loyalty.repo.ManufactureOrderRepo;
import com.transretail.crm.loyalty.service.ManufactureOrderService;

import junit.framework.Assert;

@Configurable
@Transactional
@ContextConfiguration(locations = "classpath*:/META-INF/spring/applicationContext*.xml")
@RunWith(SpringJUnit4ClassRunner.class)
public class ManufactureOrderServiceTest {
	
	@Autowired
	ManufactureOrderRepo manufactureOrderRepo;
	
	@Autowired
	ManufactureOrderService manufactureOrderService;
	
	@Autowired
	LookupHeaderRepo lookupHeaderRepo;
	
	@Autowired
	LookupDetailRepo lookupDetailRepo;
	
	@Autowired
	CodePropertiesService codePropertiesService;
	
	@Autowired
	CardVendorRepo cardVendorRepo;
	
	@Autowired
	UserRepo userRepo;
	
	@Autowired
	UserRoleRepo userRoleRepo;

    @Autowired
    private UserPermissionRepo userPermissionRepo;

    @Autowired
    private UserRolePermissionRepo userRolePermissionRepo;
	
	private Long moId;
	
	private Long vendorId;

    private String companyCode = "COKE01";
	
	@Before
	public void setup() {
		setupCurrentUser();

		CardVendor vendor = new CardVendor();
		vendor.setFormalName("Vendor 1");
		vendor.setMailAddress("Mail Address");
		vendor.setEmailAddress("email@test.com");
		vendor.setRegion("Region");
		vendor.setEnabled(true);
		vendor.setDefaultVendor(true);
		vendorId = cardVendorRepo.save(vendor).getId();
		
		vendor = new CardVendor();
		vendor.setFormalName("Vendor 2");
		vendor.setMailAddress("Mail Address 2");
		vendor.setEmailAddress("email2@test.com");
		vendor.setRegion("Region 2");
		vendor.setEnabled(true);
		vendor.setDefaultVendor(true);
		cardVendorRepo.save(vendor);

        LookupHeader companyHeader = new LookupHeader(codePropertiesService.getHeaderCompany());
        lookupHeaderRepo.saveAndFlush(companyHeader);
        lookupDetailRepo.saveAndFlush(new LookupDetail(companyCode, "COCA-COLA", companyHeader, Status.ACTIVE));

        LookupHeader header = new LookupHeader(codePropertiesService.getHeaderVendors());
		header.setDescription("Vendors");
		lookupHeaderRepo.save(header);
		
		LookupDetail detail = new LookupDetail("VEND001");
		detail.setHeader(header);
		detail.setDescription("Vendor 1");
		lookupDetailRepo.save(detail);
		
		header = new LookupHeader(codePropertiesService.getHeaderMembershipTier());
		header.setDescription("Card Type");
		lookupHeaderRepo.save(header);
		
		detail = new LookupDetail(codePropertiesService.getDetailMemberTierGold());
		detail.setHeader(header);
		detail.setDescription("Gold");
		lookupDetailRepo.save(detail);
		
		header = new LookupHeader(codePropertiesService.getHeaderMoStatus());
		header.setDescription("Mo Status");
		lookupHeaderRepo.save(header);
		
		detail = new LookupDetail(codePropertiesService.getDetailMoStatusForApproval());
		detail.setHeader(header);
		detail.setDescription("For Approval");
		lookupDetailRepo.save(detail);
		
		ManufactureOrder order = new ManufactureOrder();
		order.setMoNo("0000000");
		order.setOrderDate(new LocalDate());
		order.setPoNo("0000000");
		order.setSupplier(new CardVendor(vendorId));
		
		List<ManufactureOrderTier> orderTiers = new ArrayList<ManufactureOrderTier>();
		ManufactureOrderTier orderTier = new ManufactureOrderTier();
		orderTier.setName(lookupDetailRepo.findByCode(codePropertiesService.getDetailMemberTierGold()));
        orderTier.setCompany(lookupDetailRepo.findByCode(companyCode));
		orderTier.setStartingSeries("00010000");
		orderTier.setQuantity(100L);
		orderTier.setManufactureOrder(order);
		orderTiers.add(orderTier);

		orderTier = new ManufactureOrderTier();
		orderTier.setName(lookupDetailRepo.findByCode(codePropertiesService.getDetailMemberTierGold()));
        orderTier.setCompany(lookupDetailRepo.findByCode(companyCode));
		orderTier.setStartingSeries("00020000");
		orderTier.setQuantity(100L);
		orderTier.setManufactureOrder(order);
		orderTiers.add(orderTier);
		
		order.setTiers(orderTiers);
		
		moId = manufactureOrderRepo.save(order).getId();
	}
	
	private void setupCurrentUser() {
		UserRoleModel roleModel = new UserRoleModel();
		roleModel.setCode("ROLE_MERCHANT_SERVICE_SUPERVISOR");
		roleModel.setDescription("ROLE_MERCHANT_SERVICE_SUPERVISOR");
		roleModel.setEnabled(true);
		userRoleRepo.save(roleModel);
		
		UserModel currentUser = new UserModel();
    	currentUser.setUsername("loggedInUser");
    	currentUser.setPassword("");
    	currentUser.setEnabled(true);
    	currentUser.setStoreCode( "store1" );
    	userRepo.save(currentUser);

        saveUserRolePermission(currentUser, "ROLE_MERCHANT_SERVICE_SUPERVISOR", "TEST_PERM");

        CustomSecurityUserDetailsImpl user = new CustomSecurityUserDetailsImpl();
        user.setUserId(currentUser.getId());
        user.setUsername(currentUser.getUsername());
        List<GrantedAuthority> authorities = Lists.newArrayList();
        authorities.add(new SimpleGrantedAuthority("ROLE_MERCHANT_SERVICE_SUPERVISOR"));
        user.setAuthorities(authorities);
        user.setRoles(Sets.newHashSet(roleModel));

        SecurityContextImpl securityContext = new SecurityContextImpl();
        securityContext.setAuthentication(new TestingAuthenticationToken(user, ""));
        SecurityContextHolder.setContext(securityContext);
    }
	
	@After
	public void teardown() {
		SecurityContextHolder.clearContext();
		manufactureOrderRepo.deleteAll();
		cardVendorRepo.deleteAll();
        lookupDetailRepo.deleteAll();
        lookupHeaderRepo.deleteAll();
	}
	
	@Test
	public void testGetVendors() {
		NameIdPairResultList list = manufactureOrderService.getVendors();
		Assert.assertEquals(2, list.getResults().size());
	}
	
	@Test
	public void testIsApprover() {
		Assert.assertTrue(manufactureOrderService.isApprover());
	}
	
	@Test
	public void testFindAllDtosForApprover() {
		ManufactureOrderDto orderDto = new ManufactureOrderDto();
		orderDto.setMoNo("0000002");
		orderDto.setOrderDate(new LocalDate());
		orderDto.setPoNo("2000000");
		orderDto.setSupplier(vendorId);
		orderDto.setStatus(lookupDetailRepo.findByCode(codePropertiesService.getDetailMoStatusForApproval()));
		orderDto.setId(moId);
		
		List<ManufactureOrderTierDto> orderTierDtos = new ArrayList<ManufactureOrderTierDto>();
		ManufactureOrderTierDto orderTierDto = new ManufactureOrderTierDto();
		orderTierDto.setName(lookupDetailRepo.findByCode(codePropertiesService.getDetailMemberTierGold()));
        orderTierDto.setCompanyCode(companyCode);
		orderTierDto.setStartingSeries("30000000");
		orderTierDto.setQuantity(100L);
		orderTierDtos.add(orderTierDto);
		orderDto.setTiers(orderTierDtos);
		
		manufactureOrderService.updateMoDto(orderDto);
		
		List<ManufactureOrderDto> list = manufactureOrderService.findAllDtosForApprover();
		Assert.assertEquals(1, list.size());
	}
	
	@Test
	public void testSaveMoDto() {
		long initialCount = manufactureOrderRepo.count();
		
		ManufactureOrderDto orderDto = new ManufactureOrderDto();
		orderDto.setMoNo("0000001");
		orderDto.setOrderDate(new LocalDate());
		orderDto.setPoNo("1000000");
		orderDto.setSupplier(vendorId);
		
		List<ManufactureOrderTierDto> orderTierDtos = new ArrayList<ManufactureOrderTierDto>();
		ManufactureOrderTierDto orderTierDto = new ManufactureOrderTierDto();
		orderTierDto.setName(lookupDetailRepo.findByCode(codePropertiesService.getDetailMemberTierGold()));
		orderTierDto.setStartingSeries("10000000");
		orderTierDto.setQuantity(100L);
		orderTierDtos.add(orderTierDto);
		
		orderTierDto = new ManufactureOrderTierDto();
		orderTierDto.setName(lookupDetailRepo.findByCode(codePropertiesService.getDetailMemberTierGold()));
		orderTierDto.setStartingSeries("20000000");
		orderTierDto.setQuantity(100L);
		orderTierDtos.add(orderTierDto);
		
		orderDto.setTiers(orderTierDtos);
		
		manufactureOrderService.saveMoDto(orderDto);
		
		Assert.assertEquals(initialCount + 1, manufactureOrderRepo.count());
	}
	
	@Test
	public void testUpdateMoDto() {
		Assert.assertNotNull(manufactureOrderRepo.findOne(moId));
		Assert.assertEquals(2, manufactureOrderRepo.findOne(moId).getTiers().size());
		
		long initialCount = manufactureOrderRepo.count();
		
		ManufactureOrderDto orderDto = new ManufactureOrderDto();
		orderDto.setMoNo("0000002");
		orderDto.setOrderDate(new LocalDate());
		orderDto.setPoNo("2000000");
		orderDto.setSupplier(vendorId);
		orderDto.setId(moId);
		
		List<ManufactureOrderTierDto> orderTierDtos = new ArrayList<ManufactureOrderTierDto>();
		ManufactureOrderTierDto orderTierDto = new ManufactureOrderTierDto();
		orderTierDto.setName(lookupDetailRepo.findByCode(codePropertiesService.getDetailMemberTierGold()));
        orderTierDto.setCompanyCode(companyCode);
		orderTierDto.setStartingSeries("30000000");
		orderTierDto.setQuantity(100L);
		orderTierDtos.add(orderTierDto);
		orderDto.setTiers(orderTierDtos);
		
		manufactureOrderService.updateMoDto(orderDto);
		
		Assert.assertEquals(initialCount, manufactureOrderRepo.count());
		Assert.assertNotNull(manufactureOrderRepo.findOne(moId));
		Assert.assertEquals(1, manufactureOrderRepo.findOne(moId).getTiers().size());
	}
	
	@Test
	public void testFindDtoById() {
		ManufactureOrderDto orderDto = manufactureOrderService.findDtoById(0L);
		Assert.assertNull(orderDto);
		
		orderDto = manufactureOrderService.findDtoById(moId);
		Assert.assertNotNull(orderDto);
	
	}
	
	@Test
	public void testValidateSeries() {
        /*
		ManufactureOrderTierDto orderTierDto = new ManufactureOrderTierDto();
		orderTierDto.setName(lookupDetailRepo.findByCode(codePropertiesService.getDetailMemberTierGold()));
		orderTierDto.setQuantity(10L);
		orderTierDto.setStartingSeries("00010001");
		
		Assert.assertFalse(manufactureOrderService.validateSeries(orderTierDto));
		
		orderTierDto = new ManufactureOrderTierDto();
		orderTierDto.setName(lookupDetailRepo.findByCode(codePropertiesService.getDetailMemberTierGold()));
		orderTierDto.setQuantity(10L);
		orderTierDto.setStartingSeries("00011000");
		
		Assert.assertTrue(manufactureOrderService.validateSeries(orderTierDto));
		
		orderTierDto = new ManufactureOrderTierDto();
		orderTierDto.setName(lookupDetailRepo.findByCode(codePropertiesService.getDetailMemberTierGold()));
		orderTierDto.setQuantity(100L);
		orderTierDto.setStartingSeries("00010000");
		
		Assert.assertFalse(manufactureOrderService.validateSeries(orderTierDto));
		
		orderTierDto = new ManufactureOrderTierDto();
		orderTierDto.setName(lookupDetailRepo.findByCode(codePropertiesService.getDetailMemberTierGold()));
		orderTierDto.setQuantity(1L);
		orderTierDto.setStartingSeries("00010099");
		
		Assert.assertFalse(manufactureOrderService.validateSeries(orderTierDto));
		*/
	}
	
	@Test
	public void testFindAllDtos() {
		Assert.assertEquals(1, manufactureOrderService.findAllDtos().size());
	}

	@Test
	public void testFindDtoByMoNo() {
		ManufactureOrderDto orderDto = manufactureOrderService.findDtoByMoNo("01010101");
		Assert.assertNull(orderDto);
		
		orderDto = manufactureOrderService.findDtoByMoNo("0000000");
		Assert.assertNotNull(orderDto);
	}
	
	@Test
	public void testFindDuplicateMoNo() {
		ManufactureOrderDto orderDto = new ManufactureOrderDto();
		orderDto.setMoNo("0000000");
		orderDto.setPoNo("0000000");
		orderDto.setOrderDate(new LocalDate());
		orderDto.setSupplier(vendorId);
		
		ManufactureOrderDto duplicate = manufactureOrderService.findDuplicateMoNo(orderDto);
		Assert.assertNotNull(duplicate);
		
		orderDto = new ManufactureOrderDto();
		orderDto.setMoNo("0000001");
		orderDto.setPoNo("0000000");
		orderDto.setOrderDate(new LocalDate());
		orderDto.setSupplier(vendorId);
		
		duplicate = manufactureOrderService.findDuplicateMoNo(orderDto);
		Assert.assertNull(duplicate);
	}
	
	@Test
	public void testDeleteMo() {
		long initialCount = manufactureOrderRepo.count();
		manufactureOrderService.deleteMo(moId);
		Assert.assertEquals(initialCount - 1, manufactureOrderRepo.count());
	}

    private void saveUserRolePermission(UserModel member, String roleCode, String permissionCode) {
        UserRoleModel role = userRoleRepo.findOne(roleCode);
        if (role == null) {
            role = userRoleRepo.save(new UserRoleModel(roleCode));
        }
        UserPermission permission = userPermissionRepo.findOne(permissionCode);
        if (permission == null) {
            permission = userPermissionRepo.save(new UserPermission(permissionCode, "TEST"));
        }
        userRolePermissionRepo.saveAndFlush(new UserRolePermission(member, role, permission));
    }
}
