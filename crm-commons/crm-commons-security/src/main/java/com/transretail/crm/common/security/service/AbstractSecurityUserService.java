/*
 * Copyright (c) 2012.
 * All rights reserved.
 */

package com.transretail.crm.common.security.service;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;

import com.transretail.crm.common.security.model.SecurityUserDetails;


public abstract class AbstractSecurityUserService<T extends SecurityUserDetails> implements SecurityUserService<T> {

    /**
     * Retrieves the current principal from the spring security context.
     *
     * @return current user principal
     */
    public Object getSecurityContextPrincipal() {
        Authentication authCtx = SecurityContextHolder.getContext().getAuthentication();

        if (authCtx != null && authCtx.isAuthenticated()) {
            return authCtx.getPrincipal();

        } else {
            return null;
        }
    }

}
