<%@ include file="../../common/taglibs.jsp" %>


	<div class="contentTable">
		<table class="tableData targetTable">
			<thead>
				<tr>
					<th class="targetSquare"><![CDATA[&nbsp;]]></th>
					<th><spring:message code="label_group_storegroup"/></th>
				</tr>
			</thead>
			<tbody>
				<c:forEach var="item" items="${storeGroups}" >
				<tr class="storeGroupContainer">
					<td class="targetSquare"><input type="checkbox" class="targetCheckbox" name="targetStoreGroups" value="${item.id}"></td>
					<td><span class="targetText"><c:out value="${item.name}" /></span></td>
				</tr>
				</c:forEach>
			</tbody>
		</table>
	</div>
