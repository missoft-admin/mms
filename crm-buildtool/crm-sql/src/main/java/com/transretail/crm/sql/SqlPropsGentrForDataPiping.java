package com.transretail.crm.sql;

import java.io.FileInputStream;
import java.io.FileWriter;
import java.util.List;
import java.util.Scanner;

import org.apache.commons.io.IOUtils;

import com.google.common.collect.Lists;

/**
 * @author Allan G. Ramirez (agramirez@exist.com)
 */
public class SqlPropsGentrForDataPiping {
    private String sqlAbsoluteFilePath;

    public SqlPropsGentrForDataPiping(String sqlAbsoluteFilePath) {
        this.sqlAbsoluteFilePath = sqlAbsoluteFilePath;
    }

    public static void main(String[] args) throws Exception {
        final FileWriter writer = new FileWriter(
            "/Users/aramirez/workspace/carrefour/transretail-crm/crm-buildtool/crm-sql/src/test/resources/zmisc/sql-for-crm-1.2.properties");
        try {
            SqlPropsGentrForDataPiping generator = new SqlPropsGentrForDataPiping(
                "/Users/aramirez/workspace/carrefour/transretail-crm/"
                    + "crm-buildtool/crm-sql/src/test/resources/timesten/1.2.0/01_CRM-PROXY-SCHEMA.sql"
            );
            generator.generate(new LineWriterDelegate() {
                @Override
                public void write(String line) throws Exception {
                    writer.write(line + "\r\n");
                }
            });
        } finally {
            IOUtils.closeQuietly(writer);
        }
    }

    public synchronized void generate(LineWriterDelegate delegate) throws Exception {
        FileInputStream is = null;
        try {
            is = new FileInputStream(sqlAbsoluteFilePath);
            Scanner scanner = new Scanner(is);
            parse(scanner, delegate);
        } finally {
            if (is != null) {
                is.close();
            }
        }
    }

    private void parse(Scanner scanner, LineWriterDelegate delegate) throws Exception {
        while (scanner.hasNext()) {
            String str = scanner.nextLine().trim().toUpperCase();
            if (str != null && str.contains("CREATE TABLE")) {
                int start = str.indexOf("CRM_");
                String tableName = str.substring(start);
                tableName = tableName.substring(0, tableName.indexOf(" "));
                String primaryKey = null;

                List<String> columnNames = Lists.newArrayList();

                boolean hasColumns = true;
                while (hasColumns) {
                    str = scanner.nextLine().trim().toUpperCase();
                    if (str.contains(");")) {
                        hasColumns = false;
                    } else if (str.toUpperCase().contains("PRIMARY KEY")) {
                        primaryKey = str.substring("PRIMARY KEY".length()).trim();
                        primaryKey = primaryKey.substring(1, primaryKey.indexOf(")"));
                    } else {
                        String[] columnDef = str.trim().split(" ");
                        columnNames.add(columnDef[0]);
                    }
                }

                delegate.write(generateLine(tableName, columnNames, primaryKey, true));
                delegate.write(generateLine(tableName, columnNames, primaryKey, false));
            }
        }
    }

    private String generateLine(String tableName, List<String> columnNames, String primaryKey, boolean insert) {
        StringBuilder builder = null;
        if (insert) {
            builder = new StringBuilder("insert.");
            builder.append(tableName);
            builder.append("=INSERT INTO ");
            builder.append(tableName);
            builder.append(" (");
            builder.append(columnNames.get(0));
            for (int i = 1; i < columnNames.size(); i++) {
                builder.append(",");
                builder.append(columnNames.get(i));
            }
            builder.append(")");
            builder.append(" VALUES (");
            builder.append(":#");
            builder.append(columnNames.get(0));
            for (int i = 1; i < columnNames.size(); i++) {
                builder.append(",:#");
                builder.append(columnNames.get(i));
            }
            builder.append(")");
        } else {
            builder = new StringBuilder("update.");
            builder.append(tableName);
            builder.append("=UPDATE ");
            builder.append(tableName);
            builder.append(" SET ");
            String columnName = columnNames.get(0);
            builder.append(columnName);
            builder.append("=:#");
            builder.append(columnName);
            for (int i = 1; i < columnNames.size(); i++) {
                columnName = columnNames.get(i);
                builder.append(",");
                builder.append(columnName);
                builder.append("=:#");
                builder.append(columnName);
            }
            builder.append(" WHERE ");
            builder.append(primaryKey);
            builder.append("=:#");
            builder.append(primaryKey);
        }
        return builder.toString();

    }

}
