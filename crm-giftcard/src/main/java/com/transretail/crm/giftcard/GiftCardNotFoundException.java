package com.transretail.crm.giftcard;

import com.google.common.base.Joiner;
import java.util.Set;

/**
 *
 * @author Monte Cillo Co (mco@exist.com)
 */
public class GiftCardNotFoundException extends RuntimeException {

    private Set<String> invalidCardNos;

    /**
     * Creates a new instance of <code>GiftCardNotFoundException</code> without
     * detail message.
     */
    public GiftCardNotFoundException() {
	super();
    }

    /**
     * Constructs an instance of <code>GiftCardNotFoundException</code> with the
     * specified detail message.
     *
     * @param cardNo
     */
    public GiftCardNotFoundException(String cardNo) {
	super(String.format("Gift Card %s is not found.", cardNo));
    }

    public GiftCardNotFoundException(Set<String> invalidCardNos) {
	super(String.format("Gift Cards [%s] is not found.", Joiner.on(",").join(invalidCardNos)));
	this.invalidCardNos = invalidCardNos;
    }

    public Set<String> getInvalidGiftCardNos() {
	return invalidCardNos;
    }
}
