package com.transretail.crm.core.entity.enums;

public enum EmployeeSearchCriteria {
	
	EMPLOYEEID ( "Employee ID", "employeeId" ),
    FIRSTNAME( "First Name", "firstName" ),
    LASTNAME( "Last Name", "lastName" );
	
	private EmployeeSearchCriteria(String inDesc, String inValue){
        this.desc = inDesc;
        this.value = inValue;
    }
	
	private final String desc;
    private final String value;
    
    public String getDescription() {
    	return desc;
    }
    public String getValue() {
    	return value;
    }

}
