package com.transretail.crm.core.repo;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.transretail.crm.common.repo.CrmQueryDslPredicateExecutor;
import com.transretail.crm.core.entity.ApprovalMatrixModel;
import com.transretail.crm.core.entity.enums.ModelType;
import com.transretail.crm.core.repo.custom.ApprovalMatrixRepoCustom;


@Repository
public interface ApprovalMatrixRepo extends CrmQueryDslPredicateExecutor<ApprovalMatrixModel, Long>, ApprovalMatrixRepoCustom {

	List<ApprovalMatrixModel> findByModelType( ModelType inType );

}
