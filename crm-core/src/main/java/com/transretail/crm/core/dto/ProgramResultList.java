package com.transretail.crm.core.dto;


import java.util.Collection;

import org.springframework.data.domain.Page;

import com.transretail.crm.common.service.dto.response.AbstractResultListDTO;


public class ProgramResultList extends AbstractResultListDTO<ProgramDto> {

    public ProgramResultList( 
    		Collection<ProgramDto> results, 
    		long totalElements, 
    		boolean hasPreviousPage, 
    		boolean hasNextPage) {

    	super(results, totalElements, hasPreviousPage, hasNextPage);
    }

    public ProgramResultList( Page<ProgramDto> page ) {
    	super(page);
    }
}
