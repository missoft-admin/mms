package com.transretail.crm.giftcard.entity;

import java.io.Serializable;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.annotations.Type;

import com.google.common.collect.Lists;
import com.transretail.crm.core.entity.lookup.LookupDetail;
import com.transretail.crm.core.entity.support.CustomAuditableEntity;
import com.transretail.crm.giftcard.entity.support.ProductProfileStatus;

@Entity
@Table(name = "CRM_GC_PROD_PROFILE")
public class ProductProfile extends CustomAuditableEntity<Long> implements Serializable {

	/**
	 *
	 */
	private static final long serialVersionUID = 1L;

	@Column(name = "PRODUCT_CODE")
	private String productCode;

	@Column(name = "PRODUCT_DESC")
	private String productDesc;

	@ManyToOne
    @JoinColumn(name = "FACE_VALUE")
	private LookupDetail faceValue;

	@Column(name = "CARD_FEE")
	private double cardFee = 0;

	@Column(name = "ALLOW_RELOAD")
	@Type(type = "yes_no")
	private Boolean allowReload = Boolean.FALSE;

	@Column(name = "ALLOW_PARTIAL_REDEEM")
	@Type(type = "yes_no")
	private Boolean allowPartialRedeem = Boolean.FALSE;

	@Column(name = "SAFETY_STOCKS_EA_MO")
	private String safetyStocksEaMo;

	@Column(name = "MAX_AMOUNT")
	private Long maxAmount;

	@Column(name = "EFFECTIVE_MONTHS")
	private Long effectiveMonths;

	@Column(name = "UNIT_COST")
	private Double unitCost;

	@Column(name = "STATUS")
	private ProductProfileStatus status;

	@OneToMany(mappedBy = "productProfile", orphanRemoval = true, cascade = CascadeType.ALL)
    private List<PrepaidReloadInfo> prepaidReloadInfos;

    @Column(name = "IS_EGC", length = 1)
    @Type(type = "yes_no")
    private Boolean isEgc;

    @OneToMany(mappedBy = "productProfile", orphanRemoval = true, cascade = CascadeType.ALL)
    private List<ProductProfileUnit> productProfileUnits=Lists.newArrayList();

	public ProductProfile() {}

	public ProductProfile(Long id) {
		setId(id);
	}

	public String getProductCode() {
		return productCode;
	}
	public void setProductCode(String productCode) {
		this.productCode = productCode;
	}
	public String getProductDesc() {
		return productDesc;
	}
	public void setProductDesc(String productDesc) {
		this.productDesc = productDesc;
	}

	public LookupDetail getFaceValue() {
		return faceValue;
	}

	public void setFaceValue(LookupDetail faceValue) {
		this.faceValue = faceValue;
	}

	public double getCardFee() {
		return cardFee;
	}
	public void setCardFee(double cardFee) {
		this.cardFee = cardFee;
	}
	public Boolean isAllowReload() {
		return allowReload;
	}
	public void setAllowReload(Boolean allowReload) {
		this.allowReload = allowReload;
	}
	public Boolean isAllowPartialRedeem() {
		return allowPartialRedeem;
	}
	public void setAllowPartialRedeem(Boolean allowPartialRedeem) {
		this.allowPartialRedeem = allowPartialRedeem;
	}

    public String getSafetyStocksEaMo() {
        return safetyStocksEaMo;
    }

    public void setSafetyStocksEaMo(String safetyStocksEaMo) {
        this.safetyStocksEaMo = safetyStocksEaMo;
    }

    public Long getMaxAmount() {
		return maxAmount;
	}
	public void setMaxAmount(Long maxAmount) {
		this.maxAmount = maxAmount;
	}
	public Long getEffectiveMonths() {
		return effectiveMonths;
	}
	public void setEffectiveMonths(Long effectiveMonths) {
		this.effectiveMonths = effectiveMonths;
	}
	public ProductProfileStatus getStatus() {
		return status;
	}
	public void setStatus(ProductProfileStatus status) {
		this.status = status;
	}
	public Double getUnitCost() {
		return unitCost;
	}
	public void setUnitCost(Double unitCost) {
		this.unitCost = unitCost;
	}

    public String getCodeAndName() {
	return String.format("%s - %s", this.productCode, this.productDesc);
    }

    public List<PrepaidReloadInfo> getPrepaidReloadInfos() {
        return prepaidReloadInfos;
    }

    public void setPrepaidReloadInfos(List<PrepaidReloadInfo> prepaidReloadInfos) {
        this.prepaidReloadInfos = prepaidReloadInfos;
    }

	public Boolean getIsEgc() {
		return isEgc;
	}

	public void setIsEgc(Boolean isEgc) {
		this.isEgc = isEgc;
	}

    public enum Denomination {

		D500K(5, 500),
		D200K(2, 200),
		D100K(1, 100),
		D50K(9, 50),
		D25K(7, 25),
		D10K(6, 10);

		private final int code;
		private final int base;
		private static final int MULTIPLIER = 1000;

		private Denomination(int code, int base) {
			this.code = code;
			this.base = base;
		}

		public int getAmount() {
			return MULTIPLIER * base;
		}

		public int getCode() {
			return code;
		}

		public static Denomination getByCode(int code) {
			for (Denomination denomination : Denomination.values()) {
				if (denomination.code == code) {
					return denomination;
				}
			}

			return null;
		}

		public static Denomination getByAmount(int amount) {
			for (Denomination denomination : Denomination.values()) {
				if (denomination.getAmount() == amount) {
					return denomination;
				}
			}

			return null;
		}

		@Override
		public String toString() {
			return this.name().substring(1);
		}

	}

	public List<ProductProfileUnit> getProductProfileUnits() {
		return productProfileUnits;
	}

	public void setProductProfileUnits(List<ProductProfileUnit> productProfileUnits) {
		this.productProfileUnits = productProfileUnits;
	}


}
