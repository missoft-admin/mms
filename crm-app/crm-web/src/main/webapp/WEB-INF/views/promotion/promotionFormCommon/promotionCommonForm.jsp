<%@ include file="../../common/taglibs.jsp" %>


          <div class="pull-left">
            <div id="block_promotionProgram" class="control-group">
              <label for="program" class="control-label"><spring:message code="promotion_prop_program"/></label>
              <div class="controls">
                <c:url var="url_listPrograms" value="/promotion/campaign/list/"/>
                <form:select path="program" id="program" data-url="${url_listPrograms}">
                  <c:forEach var="program" items="${promotionPrograms}">
                    <form:option value="${program.id}" label="${program.name}" data-min-date="${program.startDate}" data-max-date="${program.endDate}" />
                  </c:forEach>
                </form:select>
    
                <a href="#" class="btn btn-small" id="link_promotionProgram" data-url="<c:url value="/promotion/program/create"/>">
                  <spring:message code="label_add"/>
                </a>    
              </div>
            </div>
    
            <div id="block_promotionCampaign" class="control-group">
              <label for="campaign" class="control-label"><spring:message code="promotion_prop_campaign"/></label>
              <div class="controls">
                <form:select path="campaign" id="campaign">
                  <c:forEach var="campaign" items="${promotionCampaigns}">
                    <form:option value="${campaign.id}" label="${campaign.name}" data-min-date="${campaign.startDate}" data-max-date="${campaign.endDate}" />
                  </c:forEach>
                </form:select>
    
                <a href="#" class="btn btn-small" id="link_promotionCampaign" data-url='<c:url value="/promotion/campaign/create"/>'>
                  <spring:message code="label_add"/>
                </a>
                <div id="form_promotionCampaign"></div>
              </div>
            </div>
    
            <div id="block_promotionName" class="control-group">
              <label for="name" class="control-label"><b class="required">*</b> <spring:message code="promotion_prop_name"/></label>
              <div class="controls">
                <form:input path="name" id="name"/>
                <form:hidden path="id"/>
              </div>
            </div>
    
            <div id="block_promotionDesc" class="control-group">
              <label for="description" class="control-label"><spring:message code="promotion_prop_description"/></label>
              <div class="controls">
                <form:input path="description" id="description"/>
              </div>
            </div>

            <c:if test="${not empty promotion.createUser}">
            <div id="" class="control-group">
              <label for="" class="control-label"><spring:message code="promotion_label_author"/></label>
              <div class="controls"><c:out value="${promotion.createUser}" /> - <c:out value="${promotion.createUserFullName}" /></div>
            </div>
            </c:if>

            <c:if test="${not empty promotion.dispCreated}">
            <div id="" class="control-group">
              <label for="" class="control-label"><spring:message code="promotion_label_createddate"/></label>
              <div class="controls"><span id="content_created">${promotion.dispCreated}</span></div>
            </div>
            </c:if>
          </div>


          <div class="pull-left control-group-01">
            <div class="pull-left control-group-small control-group" id="block_promotionDuration">
            <fieldset>
              <label class="block-stack-title" style="border-bottom: 1px solid #E8E8E8;
              margin-bottom: 20px;
              padding: 5px 0 4px;"><spring:message code="promotion_prop_duration" /></label>
      
              <div class="input-daterange" id="duration">
                <div class="control-group" style="margin-bottom: 10px;">
                  <label for="startDate" class="control-label case-cap"><spring:message code="promotion_prop_startdate"/></label>
                  <div class="controls"><form:input path="startDate" id="startDate" cssClass="input-space-01 input-small" /></div>
                </div>
                <div class="control-group">
                  <label for="endDate" class="control-label case-cap"><spring:message code="promotion_prop_enddate"/></label>
                  <div class="controls"><form:input path="endDate" id="endDate" cssClass="input-small"/></div>
                </div>
              </div>
            </fieldset>
            </div>
  
            <div class="pull-left control-data-01">
              <div class="pull-left control-group-small">
              <fieldset>
                <label class="block-stack-title" style="border-bottom: 1px solid #E8E8E8;
                margin-bottom: 20px;
                padding: 5px 0 4px;"><spring:message code="reward_label_affecteddaysandtime" /></label>
        
                <div id="block_promotionTime" class="pull-left">
                  <div class="control-group" style="margin-bottom: 10px;">
                    <label for="startTime" class="control-label case-cap"><spring:message code="promotion_prop_starttime"/></label>
                    <div class="controls">
                      <form:input path="startTime" id="startTime" cssClass="input-small input-space-01" />
                    </div>
                  </div>
                  <div class="control-group">
                    <label for="endTime" class="control-label case-cap"><spring:message code="promotion_prop_endtime"/></label>
                    <div class="controls">
                      <form:input path="endTime" id="endTime" cssClass="input-small" />
                    </div>
                  </div>
                </div>
    
                <div class="clearfix"></div>
              </fieldset>
              </div>
      
              <div id="block_promotionDays" class="well pull-left">
                <div class="checkbox">
                    <div class="select-all-header">
                        <input type="checkbox" id="selectAllDays" /><label for="affectedDays" class=""><spring:message code="promotion_prop_selectall"/></label>
                    </div>
                    <div id="content_promotionDays" class="">
                      <c:forEach var="promotionDay" items="${promotionAffectedDays}">
                        <div class="capitalize" >
                          <c:if test="${null != promotion.affectedDays}">
                            <form:checkbox path="promotionDays" value="${promotionDay.code}" label="${promotionDay.description}" />
                          </c:if>
                          <c:if test="${null == promotion.affectedDays}">
                            <form:checkbox path="promotionDays" value="${promotionDay.code}" label="${promotionDay.description}" checked="checked" />
                          </c:if>
                        </div>
                      </c:forEach>
                    </div>
                </div>
              </div>

              <div class="clearfix"></div>
            </div>

            <div class="clearfix"></div>
          </div>


          <div class="clearfix"></div>
