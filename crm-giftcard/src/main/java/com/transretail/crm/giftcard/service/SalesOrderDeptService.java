package com.transretail.crm.giftcard.service;

import java.math.BigDecimal;

import com.transretail.crm.giftcard.dto.SalesOrderMultDeptAllocsDto;






public interface SalesOrderDeptService {

	SalesOrderMultDeptAllocsDto getAllocations(Long orderId);

	void saveAllocations(SalesOrderMultDeptAllocsDto allocsDto);

	void deleteAllocBySo(Long orderId);

	void createEmptyAllocForRemaingAmt(Long orderId, BigDecimal totalSoAmt);
	
	
}
