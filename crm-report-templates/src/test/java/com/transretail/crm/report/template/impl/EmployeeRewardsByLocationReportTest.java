package com.transretail.crm.report.template.impl;

import com.google.common.collect.Maps;
import com.mysema.query.jpa.impl.JPAQuery;
import com.transretail.crm.common.reporting.jasper.JRProcessor;
import com.transretail.crm.common.reporting.service.ReportService;
import com.transretail.crm.core.entity.MemberModel;
import com.transretail.crm.core.entity.PointsTxnModel;
import com.transretail.crm.core.entity.RewardSchemeModel;
import com.transretail.crm.core.entity.embeddable.Address;
import com.transretail.crm.core.entity.embeddable.CustomerProfile;
import com.transretail.crm.core.entity.embeddable.ProfessionalProfile;
import com.transretail.crm.core.entity.enums.MemberCreatedFromType;
import com.transretail.crm.core.entity.enums.PointTxnType;
import com.transretail.crm.core.entity.enums.Status;
import com.transretail.crm.core.entity.lookup.LookupDetail;
import com.transretail.crm.core.entity.lookup.QLookupDetail;
import com.transretail.crm.core.repo.*;
import com.transretail.crm.core.service.CodePropertiesService;
import com.transretail.crm.report.template.ReportTemplate;
import net.sf.jasperreports.engine.JRParameter;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.joda.time.DateTime;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.context.support.MessageSourceResourceBundle;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.support.TransactionCallback;
import org.springframework.transaction.support.TransactionTemplate;

import javax.annotation.Resource;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.io.File;
import java.io.InputStream;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.Map;
import java.util.Scanner;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

/**
 * @author Ervy V. Bigornia (ebigornia@exist.com)
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath*:/META-INF/spring/applicationContext*.xml",
    "/META-INF/spring/applicationContext-test.xml"})
public class EmployeeRewardsByLocationReportTest {
    private static final Logger _LOG = LoggerFactory.getLogger(EmployeeRewardsByLocationReportTest.class);
    @PersistenceContext
    private EntityManager em;
    @Autowired
    private PlatformTransactionManager transactionManager;
    @Resource(name = "employeeRewardsByLocationReport")
    private ReportTemplate employeeRewardsByLocationReport;
    @Autowired
    private ReportService reportService;
    @Autowired
    private MessageSource messageSource;
    @Autowired
    private LookupHeaderRepo lookupHeaderRepo;
    @Autowired
    private LookupDetailRepo lookupDetailRepo;
    @Autowired
    private MemberRepo memberRepo;
    @Autowired
    private PointsTxnModelRepo pointsTxnModelRepo;
    @Autowired
    private RewardSchemeRepo rewardSchemeRepo;
    @Autowired
    private CodePropertiesService codePropertiesService;
    @Autowired
    private PasswordEncoder passwordEncoder;

    private static final DateFormat BIRTHDATE_PASSWORD_FORMAT = new SimpleDateFormat("ddMMyyyy");
    private static final DateFormat BIRTHDATE_PIN_FORMAT = new SimpleDateFormat("ddMMyy");

    @Before
    public void onSetup() {
        setUpLookupDetails();
    }

    @After
    public void tearDown() {
        memberRepo.deleteAll();
        lookupDetailRepo.deleteAll();
        lookupHeaderRepo.deleteAll();
    }

    @Test
    public void getNameAndGetFiltersTest() {
        assertEquals(EmployeeRewardsByLocationReport.TEMPLATE_NAME, employeeRewardsByLocationReport.getName());
        assertNotNull(employeeRewardsByLocationReport.getFilters());
    }

    @Test
    public void createJrProcessorTest() throws Exception {
        setupData();
        Map<String, String> map = Maps.newHashMap();
        map.put(EmployeeRewardsByLocationReport.FILTER_DATE, "10-02-2014");
        JRProcessor jrProcessor = employeeRewardsByLocationReport.createJrProcessor(map);

        File testClassesDir = FileUtils.toFile(Thread.currentThread().getContextClassLoader().getResource(""));
        File output = new File(testClassesDir, "test-employee-rewards-by-location-report-output.pdf");
        _LOG.debug("Output location {}", output.getAbsolutePath());
        output.delete();
        Locale locale = LocaleContextHolder.getLocale();
        jrProcessor.addParameter(JRParameter.REPORT_LOCALE, locale);
        jrProcessor.addParameter(JRParameter.REPORT_RESOURCE_BUNDLE, new MessageSourceResourceBundle(messageSource, locale));
        reportService.exportPdfReport(jrProcessor, output);

        double bytes = output.length();
        double kilobytes = (bytes / 1024);
        _LOG.debug("Kilobytes: {}", kilobytes);
    }

    @Transactional
    private void setupData() {
        String preferredStore1 = "TITA GWAPA";
        String preferredStore2 = "TITO POGI";

        for (int i = 0; i < 10; i++) {
            MemberModel model = new MemberModel();
            model.setUsername("username" + i);
            String accountId = "accountId" + i;
            model.setAccountId(accountId);
            model.setFirstName("FNUser" + i);
            model.setLastName("LNUser" + i);
            model.setContact("contact" + i);
            model.setPin("pin" + i);
            model.setEnabled(true);
            model.setRegisteredStore(preferredStore1);
            model.setStoreName("TITA GWAPA Store");
            CustomerProfile customerProfile = new CustomerProfile();
            ProfessionalProfile professionalProfile = new ProfessionalProfile();
            Address address = new Address();

            if (i % 2 == 0) {
                model.setMemberType(new LookupDetail(codePropertiesService.getDetailMemberTypeEmployee()));
                model.setMemberCreatedFromType(MemberCreatedFromType.FROM_UPLOAD);
                customerProfile.setGender(new LookupDetail(codePropertiesService.getDetailGenderFemale()));
            } else {
                model.setMemberType(new LookupDetail(codePropertiesService.getDetailMemberTypeEmployee()));
                model.setMemberCreatedFromType(MemberCreatedFromType.FROM_CRM);
                customerProfile.setGender(new LookupDetail(codePropertiesService.getDetailGenderMale()));
            }
            String departmentCode = getCode(codePropertiesService.getHeaderBusinessFields(), "Other");
            professionalProfile.setDepartment(new LookupDetail(departmentCode));
            customerProfile.setBirthdate(new Date());

            String empTypeCode = getCode(codePropertiesService.getHeaderEmpType(), "PROBATIONARY ASSOCIATE");
            model.setEmpType(new LookupDetail(empTypeCode));

            String genderCode = getCode(codePropertiesService.getHeaderGender(), "Female");
            customerProfile.setGender(new LookupDetail(genderCode));

            String religionCode = getCode(codePropertiesService.getHeaderReligion(), "Islam");
            customerProfile.setReligion(new LookupDetail(religionCode));

            String nationalityCode = getCode(codePropertiesService.getHeaderNationality(), "INDONESIAN");
            customerProfile.setNationality(new LookupDetail(nationalityCode));


            model.setContact("0856309991" + i);

            model.setProfessionalProfile(professionalProfile);
            model.setCustomerProfile(customerProfile);
            model.setAddress(address);
            memberRepo.save(model);

            PointsTxnModel pointsTxnModel = new PointsTxnModel();
            pointsTxnModel.setId("epoints" + i);
            pointsTxnModel.setMemberModel(model);
            pointsTxnModel.setTransactionType(PointTxnType.REWARD);
            pointsTxnModel.setTransactionContributer(model);
            pointsTxnModel.setStoreCode("DENPASAR");
            pointsTxnModel.setTransactionPoints(1000.0);
            pointsTxnModel.setTransactionDateTime(new Date());
            pointsTxnModelRepo.save(pointsTxnModel);

        }

        RewardSchemeModel rewardSchemeModel = new RewardSchemeModel();
        rewardSchemeModel.setAmount(1000.0);
        rewardSchemeModel.setStatus(Status.ACTIVE);
        rewardSchemeModel.setValueFrom(1000.0);
        rewardSchemeModel.setValueTo(1999.0);
        rewardSchemeModel.setValidPeriod(4);
        rewardSchemeRepo.save(rewardSchemeModel);

        RewardSchemeModel rewardSchemeModel2 = new RewardSchemeModel();
        rewardSchemeModel2.setAmount(2000.0);
        rewardSchemeModel2.setStatus(Status.ACTIVE);
        rewardSchemeModel2.setValueFrom(2000.0);
        rewardSchemeModel2.setValueTo(2999.0);
        rewardSchemeModel2.setValidPeriod(4);
        rewardSchemeRepo.save(rewardSchemeModel2);

        RewardSchemeModel rewardSchemeModel3 = new RewardSchemeModel();
        rewardSchemeModel3.setAmount(3000.0);
        rewardSchemeModel3.setStatus(Status.ACTIVE);
        rewardSchemeModel3.setValueFrom(3000.0);
        rewardSchemeModel3.setValueTo(3999.0);
        rewardSchemeModel3.setValidPeriod(4);
        rewardSchemeRepo.save(rewardSchemeModel3);

        RewardSchemeModel rewardSchemeModel4 = new RewardSchemeModel();
        rewardSchemeModel4.setAmount(4000.0);
        rewardSchemeModel4.setStatus(Status.ACTIVE);
        rewardSchemeModel4.setValueFrom(4000.0);
        rewardSchemeModel4.setValueTo(4999.0);
        rewardSchemeModel4.setValidPeriod(4);
        rewardSchemeRepo.save(rewardSchemeModel4);

        for (int i = 10; i < 20; i++) {
            MemberModel model = new MemberModel();
            model.setUsername("username" + i);
            String accountId = "accountId" + i;
            model.setAccountId(accountId);
            model.setFirstName("FNUser" + i);
            model.setLastName("LNUser" + i);
            model.setContact("contact" + i);
            model.setPin("pin" + i);
            model.setEnabled(true);
            model.setRegisteredStore(preferredStore2);
            model.setStoreName("TITO POGI Store");
            CustomerProfile customerProfile = new CustomerProfile();
            ProfessionalProfile professionalProfile = new ProfessionalProfile();
            Address address = new Address();

            if (i % 2 == 0) {
                model.setMemberType(new LookupDetail(codePropertiesService.getDetailMemberTypeEmployee()));
                model.setMemberCreatedFromType(MemberCreatedFromType.FROM_CRM);
                customerProfile.setGender(new LookupDetail(codePropertiesService.getDetailGenderMale()));
            } else {
                model.setMemberType(new LookupDetail(codePropertiesService.getDetailMemberTypeEmployee()));
                model.setMemberCreatedFromType(MemberCreatedFromType.FROM_UPLOAD);
                customerProfile.setGender(new LookupDetail(codePropertiesService.getDetailGenderFemale()));
            }
            String departmentCode = getCode(codePropertiesService.getHeaderBusinessFields(), "Other");
            professionalProfile.setDepartment(new LookupDetail(departmentCode));
            Date birthDate = new Date();
            customerProfile.setBirthdate(birthDate);
            String formattedBirthDate = BIRTHDATE_PASSWORD_FORMAT.format(birthDate);
            model.setPassword(passwordEncoder.encode(formattedBirthDate));
            model.setPin(BIRTHDATE_PIN_FORMAT.format(birthDate));

            String empTypeCode = getCode(codePropertiesService.getHeaderEmpType(), "CONTRACTED ASSOCIATED FULL-TIME");
            model.setEmpType(new LookupDetail(empTypeCode));

            String genderCode = getCode(codePropertiesService.getHeaderGender(), "Male");
            customerProfile.setGender(new LookupDetail(genderCode));

            String religionCode = getCode(codePropertiesService.getHeaderReligion(), "Islam");
            customerProfile.setReligion(new LookupDetail(religionCode));

            String nationalityCode = getCode(codePropertiesService.getHeaderNationality(), "INDONESIAN");
            customerProfile.setNationality(new LookupDetail(nationalityCode));

            model.setContact("0856309991" + i);

            model.setProfessionalProfile(professionalProfile);
            model.setCustomerProfile(customerProfile);
            model.setAddress(address);
            memberRepo.save(model);

            PointsTxnModel pointsTxnModel = new PointsTxnModel();
            pointsTxnModel.setId("epoints" + i);
            pointsTxnModel.setMemberModel(model);
            pointsTxnModel.setTransactionType(PointTxnType.REWARD);
            pointsTxnModel.setTransactionContributer(model);
            pointsTxnModel.setStoreCode("LEBAK_BULUS");
            pointsTxnModel.setTransactionPoints(2000.0);
            pointsTxnModel.setTransactionDateTime(new Date());
            pointsTxnModelRepo.save(pointsTxnModel);
        }
    }

    @Transactional
    private void setUpLookupDetails() {
        InputStream is1 = Thread.currentThread().getContextClassLoader().getResourceAsStream("A_LOOKUPHEADERS.sql");
        InputStream is2 = Thread.currentThread().getContextClassLoader().getResourceAsStream("B_LOOKUPDETAILS.sql");
        try {
            final Scanner in1 = new Scanner(is1);
            final Scanner in2 = new Scanner(is2);
            TransactionTemplate transactionTemplate = new TransactionTemplate(transactionManager);
            transactionTemplate.execute(new TransactionCallback() {
                public Object doInTransaction(TransactionStatus status) {
                    while (in1.hasNext()) {
                        String script = in1.nextLine();
                        if (StringUtils.isNotBlank(script)) {
                            em.createNativeQuery(script).executeUpdate();
                        }
                    }
                    while (in2.hasNext()) {
                        String script = in2.nextLine();
                        if (StringUtils.isNotBlank(script)) {
                            em.createNativeQuery(script).executeUpdate();
                        }
                    }
                    em.clear();
                    em.flush();
                    return null;
                }
            });
        } finally {
            IOUtils.closeQuietly(is1);
            IOUtils.closeQuietly(is2);
        }
    }


    protected String getCode(String headerCode, String description) {
        QLookupDetail qLookupDetail = QLookupDetail.lookupDetail;
        String code = new JPAQuery(em).from(qLookupDetail).where(
                qLookupDetail.header.code.eq(headerCode)
                        .and(qLookupDetail.description.toUpperCase().eq(description.toUpperCase()))
        )
                .uniqueResult(
                        qLookupDetail.code);
        return code;
    }
}


