package com.transretail.crm.giftcard.entity;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import com.transretail.crm.core.entity.support.CustomAuditableEntity;

/**
 *
 */
@Entity
@Table(name = "CRM_GC_GEN_DISCOUNT")
public class GiftCardGeneralDiscount extends CustomAuditableEntity<Long> implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Column(name = "MIN_PURCHASE", precision = 19, scale = 5)
	private BigDecimal minPurchase;
	@Column(name = "MAX_PURCHASE", precision = 19, scale = 5)
	private BigDecimal maxDiscount;
	@Column(name = "DISCOUNT")
	private BigDecimal discount;
	
	public BigDecimal getMinPurchase() {
		return minPurchase;
	}
	public void setMinPurchase(BigDecimal minPurchase) {
		this.minPurchase = minPurchase;
	}

	public BigDecimal getMaxDiscount() {
		return maxDiscount;
	}
	public void setMaxDiscount(BigDecimal maxDiscount) {
		this.maxDiscount = maxDiscount;
	}
	public BigDecimal getDiscount() {
		return discount;
	}
	public void setDiscount(BigDecimal discount) {
		this.discount = discount;
	}
	
	
	
}
