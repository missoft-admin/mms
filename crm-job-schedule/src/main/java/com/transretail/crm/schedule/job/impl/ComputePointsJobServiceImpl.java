package com.transretail.crm.schedule.job.impl;


import java.util.Date;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.transretail.crm.core.service.CrmIdRefService;
import com.transretail.crm.core.service.MemberService;
import com.transretail.crm.core.service.PointsTxnManagerService;
import com.transretail.crm.schedule.job.service.ComputePointsJobService;


@Service("computePointsJobSvc")
public class ComputePointsJobServiceImpl implements ComputePointsJobService {

	@Autowired
	PointsTxnManagerService pointsTxnManagerService;
	@Autowired
	CrmIdRefService crmIdRefService;
	@Autowired
	MemberService memberService;

	private static final Logger LOGGER = LoggerFactory.getLogger( ComputePointsJobServiceImpl.class );



	/* * computePointsJob-object method referenced in applicationContext-scheduler.xml * */
	@Override
    @Transactional
	public void computeTotalPoints() {
		LOGGER.info( ":computeTotalPoints:: job executing at " + new Date().toString()  );
		List<Long> ids = pointsTxnManagerService.updateMemberPoints();
		if ( CollectionUtils.isNotEmpty( ids ) ) {
			crmIdRefService.deleteEarningMember( ids );
		}
		LOGGER.info( ":computeTotalPoints:: job successfully executed at " + new Date().toString()  );
	}

}
