<%@ include file="common/taglibs.jsp" %>
<%@ taglib prefix="permission" tagdir="/WEB-INF/tags/permission" %>

<spring:url var="contextPath" value="/"/>
<spring:message code="role_admin" var="role_admin"/>
<spring:message code="role_customerservice" var="role_customerservice"/>
<spring:message code="role_css" var="role_css" />
<spring:message code="role_marketing" var="role_marketing" />
<spring:message code="role_marketing_head" var="role_marketing_head" />
<spring:message code="role_hr" var="role_hr" />
<spring:message code="role_hrs" var="role_hrs" />
<spring:message code="role_merchant_service" var="role_merchant_service" />
<spring:message code="role_merchant_service_supervisor" var="role_merchant_service_supervisor" />
<spring:message code="role_store_cashier" var="role_store_cashier" />
<spring:message code="role_store_cashier_head" var="role_store_cashier_head" />
<spring:message code="role_store_cashier_manager" var="role_store_cashier_manager" />
<spring:message code="role_helpdesk" var="role_helpdesk" />
<spring:message code="role_member" var="role_member" />
<sec:authorize var="hasaccess_menu_programsetup" ifAnyGranted="${role_admin}"/>
<sec:authorize var="hasaccess_menu_marketingmgmt" ifAnyGranted="${role_marketing}, ${role_marketing_head}" />
<sec:authorize var="hasaccess_menu_giftcard" ifAnyGranted="${role_merchant_service}, ${role_merchant_service_supervisor}, ${role_store_cashier}, ${role_store_cashier_manager}" />
<sec:authorize var="hasaccess_menu_giftcard_inventory" ifAnyGranted="${role_merchant_service_supervisor}, ${role_store_cashier_head}" />
<sec:authorize var="hasaccess_menu_membermgmt" ifAnyGranted="${role_customerservice}, ${role_css}" />
<sec:authorize var="hasaccess_menu_membermgmt_mgr" ifAnyGranted="${role_css}" />
<sec:authorize var="hasaccess_menu_employeemgmt" ifAnyGranted="${role_hr}, ${role_hrs}" />
<sec:authorize var="hasaccess_menu_employeemgmt_mgr" ifAnyGranted="${role_hrs}" />
<sec:authorize var="hasaccess_menu_programsetup" ifAnyGranted="${role_admin}" />
<sec:authorize var="hasaccess_menu_mediamgr" ifAnyGranted="${role_admin}" />


<ul class="nav">
    
    <c:if test="${hasaccess_menu_programsetup}">      
    <li class="dropdown">
      <a href="#" class="dropdown-toggle nav2" data-toggle="dropdown"><spring:message code="menutab_programsetup" /> <b class="caret"></b></a>
      <ul class="dropdown-menu">
        <li class="nav-header"><spring:message code="menutab_programsetup_maintenance" /></li>
        <li><a href="<spring:url value="/report/template/list" />"><spring:message code="menutab_programsetup_maintenance_reports" /></a></li>
        <%--<li><a href="<spring:url value="/companyprofile" />"><spring:message code="menutab_programsetup_maintenance_company" /></a></li> --%>
        <li><a href="<spring:url value="/user" />"><spring:message code="menutab_programsetup_maintenance_user" /></a></li>
        <%-- <li><a href="<spring:url value="/groups/store?page=1&size=10" />"><spring:message code="menutab_programsetup_maintenance_store" /></a></li> --%>
        <li><a href="<spring:url value="/lookup?page=1&size=10" />"><spring:message code="menutab_programsetup_maintenance_lookup" /></a></li>
        <%-- <li><a href="<spring:url value="/" />"><spring:message code="menutab_programsetup_maintenance_channels" /></a></li> --%>
        <li><a href="<spring:url value="/member/points/config" />"><spring:message code="menutab_programsetup_maintenance_points" /></a></li>
        <li class="divider"></li>
        <li class="nav-header"><spring:message code="menutab_programsetup_permissions" /></li>
        <li><a href="<spring:url value="/override/permissions?page=1&size=10" />"><spring:message code="menutab_programsetup_permissions_hroverridescheme" /></a></li>
        <li><a href="<spring:url value="/points/override/approver" />"><spring:message code="menutab_programsetup_permissions_pointsoverridescheme" /></a></li>
      </ul>
    </li>
    </c:if>

    <c:if test="${hasaccess_menu_employeemgmt}">
    <li class="dropdown">
      <a href="#" class="dropdown-toggle nav2" data-toggle="dropdown"><spring:message code="menutab_employeemgmt" /> <b class="caret"></b></a>
      <ul class="dropdown-menu">
        <li class="nav-header"><spring:message code="menutab_employeemgmt_helpdesk" /></li>
        <li><a href="<spring:url value="/employeemodels?page=1&size=10" />"><spring:message code="menutab_employeemgmt_helpdesk_emplist" /></a></li>
        <li><a href="<spring:url value="/employee/adjustments/purchases?page=1&size=10" />"><spring:message code="menutab_employeemgmt_helpdesk_purchaseadjustment"/></a></li>
        <li><a href="<spring:url value="/points/override" />"><spring:message code="menutab_employeemgmt_helpdesk_pointsadjustment"/></a></li>

        <c:if test="${hasaccess_menu_employeemgmt_mgr}">
        <li class="divider"></li>
        <li class="nav-header"><spring:message code="menutab_employeemgmt_manager" /></li>
        <li><a href="<spring:url value="/pointsreward/generate/schedule" />"><spring:message code="menutab_employeemgmt_manager_rewardsched" text=""/></a></li>
        <li><a href="<spring:url value="/pointsreward/forprocessing" />"><spring:message code="menutab_employeemgmt_manager_rewardforapproval" text=""/></a></li>
        <li><a href="<spring:url value="/voucherscheme?page=1&size=5" />"><spring:message code="menutab_employeemgmt_manager_rewardscheme" text=""/></a></li>
        </c:if>
      </ul>
    </li>
    </c:if>

    <c:if test="${hasaccess_menu_marketingmgmt}">
    <li class="dropdown">
      <a href="#" class="dropdown-toggle nav2" data-toggle="dropdown"><spring:message code="menutab_marketingmgmt" /> <b class="caret"></b></a>
      <ul class="dropdown-menu">
        <li class="nav-header"><spring:message code="menutab_marketingmgmt" /></li>
        <li><a href="<spring:url value="/promotion" />"><spring:message code="menutab_marketingmgmt_config" /></a></li>
        <li><a href="<spring:url value="/groups/store?page=1&size=10" />"><spring:message code="menutab_marketingmgmt_config_storegroup" /></a></li>
        <li><a href="<spring:url value="/groups/product?page=1&size=10" />"><spring:message code="menutab_marketingmgmt_config_productgroup" /></a></li>
        <li><a href="<spring:url value="/groups/member" />"><spring:message code="menutab_marketingmgmt_campaign_customergroup" /></a></li>
      </ul>
    </li>
    </c:if>
    
    <c:if test="${hasaccess_menu_membermgmt}">
    <li class="dropdown">
      <a href="#" class="dropdown-toggle nav2" data-toggle="dropdown">
        <spring:message code="menutab_membermgmt" /> <b class="caret"></b></a>
      <ul class="dropdown-menu">
        <li class="nav-header"><spring:message code="menutab_membermgmt_loyalty" /></li>
        <li><a href="<spring:url value="/customermodels?page=1&size=10" />"><spring:message code="menutab_membermgmt_loyalty_members" /></a></li>
        <li><a href="<spring:url value="/points/override" />"><spring:message code="menutab_membermgmt_loyalty_adjustment" /></a></li>
      </ul>
    </li>
    </c:if>
    
    <c:if test="${hasaccess_menu_giftcard}">
    <li class="dropdown">
      <a href="#" class="dropdown-toggle nav2" data-toggle="dropdown">
        <spring:message code="menu_tab_role_merchant_service"/> <b class="caret"></b></a>
      <ul class="dropdown-menu">
        <li class="nav-header"><spring:message code="menutab_loyalty_menu_group_title" /></li>
        <li><a href="<spring:url value="/manufactureorder" />"><spring:message code="menutab_manufacture_order"/></a></li>
        <li><a href="<spring:url value="/loyaltycardinventory" />"><spring:message code="menutab_loyalty_card_inventory"/></a></li>
        <li class="divider"></li>
        <li class="nav-header"><spring:message code="gc.menu.group.title" /></li>
        <li><a href="<spring:url value="/giftcard/vendor/list" />"><spring:message code="menutab_giftcard_vendors"/></a></li>
        <li><a href="<spring:url value="/giftcard/list" />"><spring:message code="menutab_giftcard_list"/></a></li>
         <li><a href="<spring:url value="/giftcard/order/list" />"><spring:message code="menutab_giftcard_orders"/></a></li>
        <c:if test="${hasaccess_menu_giftcard_inventory}">
        <li class="divider"></li>
        <li class="nav-header"><spring:message code="gc.inventory.menu.group.title" /></li>
        <li><a href="<spring:url value="/giftcard/stock/list" />"><spring:message code="menutab_giftcard_stockrequest"/></a></li>
        <li><a href="<spring:url value="/giftcard/inventory/list" />"><spring:message code="menutab_giftcard_inventory_list"/></a></li>
        <sec:authorize ifAnyGranted="${role_merchant_service_supervisor}">
          <li><a href="<spring:url value="/giftcard/inventory/receive"/>"><spring:message code="menutab_giftcard_inventory_receive" text=""/></a></li>
        </sec:authorize>
        </c:if>
      </ul>
    </li>
    </c:if>

    <li class="dropdown">
    	<a href="#" class="dropdown-toggle nav2" data-toggle="dropdown">
    	<spring:message code="label_notification" /><b class="caret"></b>
    	</a>
    	<ul id="notifications" class="dropdown-menu" style="overflow-y: auto;"></ul>
    </li>

</ul>