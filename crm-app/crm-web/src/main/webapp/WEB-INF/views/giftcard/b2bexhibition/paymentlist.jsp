<%@ include file="../../common/taglibs.jsp" %>
<c:set var="totalPayment" value="0" />
<c:forEach items="${paymentSummary}" var="item" varStatus="stat">

<c:if test="${not empty item.paymentType}">

<c:set var="totalPayment">${item.total + totalPayment}</c:set>


<tr>
  <td>${item.paymentType.description}<c:if test="${not empty item.details}"> (${item.details})</c:if></td>
  <td style="text-align: right;"><fmt:formatNumber type="number" pattern="#,##0" value="${item.total}" /></td>
  <td>
  <c:if test="${empty readOnly or !readOnly}">
  <button type="button" class="tiptip btn pull-left icn-delete deletePayment" data-index="${item.paymentType.code}<c:out value="${item.details}" />${item.total}" title="<spring:message code="label_delete"/>"></button>
  </c:if>
  </td>
</tr>
</c:if>
</c:forEach>
<c:if test="${not empty paymentSummary}">
<tr>
  <td style="text-align: right;"><label><spring:message code="b2b.exhibition.totalpayment" /></label></td>
  <td style="border-top: solid;text-align: right;"><fmt:formatNumber type="number" pattern="#,##0" value="${totalPayment}" />
  <input type="hidden" name="totalPayment" value="${totalPayment}" /></td>
  <td></td>
</tr>
<tr>
  <td style="text-align: right;"><label><spring:message code="b2b.exhibition.amountdue" /></label></td>
  <td style="text-align: right;"><fmt:formatNumber type="number" pattern="#,##0" value="${totalAmountDue}" /></td>
  <td></td>
</tr>
<c:set var="change" value="${totalPayment - totalAmountDue}" />
<tr>
  <td style="text-align: right;"><label><spring:message code="b2b.exhibition.change" /></label></td>
  <td style="border-top: solid;text-align: right; border-bottom-style: double;"><fmt:formatNumber type="number" pattern="#,##0" value="${change}" />
  <input type="hidden" name="change" value="${change}" />
  </td>
  <td></td>
</tr>
</c:if>

<script type="text/javascript">
$(document).ready(function() {
  
  $(".deletePayment").click(function() {
    var index = $(this).data("index");
    $(".payment" + index).val("");
    refreshPaymentsTable();
  });
  
  
});
</script>