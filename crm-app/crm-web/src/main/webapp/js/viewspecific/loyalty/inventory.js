$(document).ready(function() {
	$inventoryList = $("#list_inventory");
	$inventorySearchForm = $("#inventorySearchForm");
	$inventorySearchBtn = $("#inventorySearchButton");
	$inventoryModal = $("#inventoryModal");
	$inventoryBigModal = $("#inventoryBigModal");
	$reprintModal = $("#reprintModal");
	var editUrl = $inventoryList.data( 'url-edit' );
	
	$reprintModal.modal({
		show: false,
		keyboard:	false, 
		backdrop: "static"
	}).on('hidden.bs.modal', function() {
		$(this).find("input, select").val("");
		$(this).find("div.errorMessages").empty();
	})
	.css({
		"width": "500px",
	});
	
	$inventoryBigModal.modal({
		show: false,
		keyboard:	false, 
		backdrop: "static"
	}).on('hidden.bs.modal', function() {
		$(this).empty();
	})
	.css({
		"width": "900px",
	});
	
	$inventoryModal.modal({
		show: false,
		keyboard:	false, 
		backdrop: "static"
	}).on('hidden.bs.modal', function() {
		$(this).empty();
	})
	.css({
		"width": "750px",
	});
	
	initUserDataTable();

	function initUserDataTable() {
		
		var headers = { 
			product: $inventoryList.data("hdrProduct"),
			status: $inventoryList.data("hdrStatus"),
			location: $inventoryList.data("hdrLocation"),
			transferTo: $inventoryList.data("hdrTransferTo"),
			count: $inventoryList.data("hdrCount"),
			lastUpdate: $inventoryList.data("hdrLastupdate")
		};
		
		var status = {
			inactive: $inventoryList.data("statusInactive"),
			forAllocation: $inventoryList.data("statusForAllocation"),
			allocated: $inventoryList.data("statusAllocated"),
			inTransit: $inventoryList.data("statusInTransit"),
			transferIn: $inventoryList.data("statusTransferIn"),
			transferOut: $inventoryList.data("statusTransferOut"),
			found: $inventoryList.data("statusFound"),
			forBurning: $inventoryList.data("statusForBurning")
		}
		
		var changeallocfrom = $inventoryList.data("changefromMsg");
		var changeallocto = $inventoryList.data("changetoMsg");
		var changeallocMsg = $inventoryList.data("changeallocMsg");

		var headOffice = $inventoryList.data("locationHeadOffice");
		var userLocation = $inventoryList.data("userLocation");
		
		$inventoryList.ajaxDataTable({
			'autoload' 	: false,
			'ajaxSource' : $inventoryList.data("url"),
			'columnHeaders' : [	
		        headers['product'],
			 headers['status'],
			 headers['location'],
			 headers['transferTo'],
			 headers['count'],
			 headers['lastUpdate'],
			 ""
			],
			'modelFields' : [
			 {name : 'product', sortable : false},
			 {name : 'status', sortable : false, fieldCellRenderer : function (data, type, row) {
				 var stat = "";
				 if(headOffice == userLocation) {
					 if(status["transferIn"] == row.statusCode && row.locationCode != headOffice)
						 stat = status["transferOut"];
					 else 
						 stat = row.status;
				 } else {
					 stat = row.status;
				 }
				 if(row.forBurning) {
					 stat += " (" + status["forBurning"] + ")";
				 }
				 return stat;
             }},
             {name : 'location', sortable : false, fieldCellRenderer : function (data, type, row) {
                 if(row.location != null && row.location != "")
                     return row.locationCode + " - " + row.location;
                 else
                    return "";
             }},
             {name : 'transferTo', sortable : false, fieldCellRenderer : function (data, type, row) {
            	 if(row.newTransferTo != null && row.newTransferTo != "") {
            		 return changeallocMsg + " "
            		 	+ row.newTransferToCode + " - " 
            		 	+ row.newTransferTo;
            	 }
                 if(row.transferTo != null && row.transferTo != "")
                     return row.transferToCode + " - " + row.transferTo;
                 else
                     return "";
             }},
			 {name : 'quantity', sortable : false},
			 {name : 'lastUpdateDate', sortable : false},
		        {customCell : function ( innerData, sSpecific, json ) {
			 		if ( sSpecific == 'display' ) {

			 			var btnHtml = $("#viewItemsBtnContainer").html();
			 			if(!json.forBurning && (json.statusCode == status["inactive"] || json.statusCode == status["transferIn"] || json.statusCode == status["found"]) && json.locationCode == $inventoryList.data("userLocation")) {
			 				btnHtml += $("#allocateBtnContainer").html(); 
			 			} else if(json.statusCode == status["forAllocation"] && $inventoryList.data( "is-merchant-supervisor" ) && userLocation == headOffice) {
			 				btnHtml += $("#approveAllocBtnContainer").html();
			 			} else if(json.statusCode == status["allocated"] && json.transferToCode != $inventoryList.data("userLocation") && json.locationCode == $inventoryList.data("userLocation")) {
			 				btnHtml += $("#transferBtnContainer").html();
			 			} else if(json.statusCode == status["inTransit"] && json.transferToCode == $inventoryList.data("userLocation")) {
			 				btnHtml += $("#receiveBtnContainer").html();
			 			} else if(json.statusCode == status["inTransit"] && json.locationCode == $inventoryList.data("userLocation") && json.newTransferToCode == null) {
			 				btnHtml += $("#reprintBtnContainer").html();
			 			} else if(json.statusCode == status["inTransit"] && json.newTransferToCode != null && json.newTransferToCode != "" && userLocation == headOffice) {
			 				btnHtml += $("#changeAllocationBtnContainer").html();
			 			}

			 			var rg = new RegExp("%MOID%", 'g');
						btnHtml = btnHtml.replace(rg, json.orderId);
						rg = new RegExp("%MOTIER%", 'g');
						btnHtml = btnHtml.replace(rg, json.productId);
						rg = new RegExp("%STATUS%", 'g');
						btnHtml = btnHtml.replace(rg, json.statusCode);
						rg = new RegExp("%LOCATION%", 'g');
						btnHtml = btnHtml.replace(rg, json.locationCode);
						rg = new RegExp("%TRANSFERTO%", 'g');
						btnHtml = btnHtml.replace(rg, json.transferToCode);
						rg = new RegExp("%NEWTRANSFERTO%", 'g');
						btnHtml = btnHtml.replace(rg, json.newTransferToCode);
						rg = new RegExp("%REFNO%", 'g');
						btnHtml = btnHtml.replace(rg, json.referenceNo);
						rg = new RegExp("%FORBURNING%", 'g');
						btnHtml = btnHtml.replace(rg, json.forBurning);
						rg = new RegExp("%MONO%", 'g');
						btnHtml = btnHtml.replace(rg, $("#search_manufactureOrder").val() != "" && $("#search_manufactureOrder").val() != null ? $("#search_manufactureOrder").val(): null);
			 			return btnHtml;
				    } else {
				              return '';
				    }
		        }
		        }  
			 ]
		}).on("click", ".modalBtn", showModal)
		.on("click", ".reprintBtn", function(e) {
			e.preventDefault();
			window.open($(this).data("url"), '_blank', 'toolbar=0,location=0,menubar=0');
		});

		initSearchForm($inventoryList);
	}
	
	function initSearchForm(dataTable) {
		$inventorySearchBtn.click(function() {
			
			var output = $.map($(":checkbox.statusChk:checked"), function(n, i){
			      return n.value;
			}).join(',');
			$("input[name='status']").val(output);
			
			var formDataObj = $inventorySearchForm.toObject( { mode : 'first', skipEmpty : true } );
			$btnSearch = $(this);
			$btnSearch.prop('disabled', true);

			dataTable.ajaxDataTable( 
				'search', 
				formDataObj, 
				function() {
					$btnSearch.prop( 'disabled', false );
			});
		});
	}
	
	function showModal(e) {
		e.preventDefault();
		$.get($(this).data("url"), function(data) {
			$inventoryModal.modal("show").html(data);
			$inventoryModal.on('hidden.bs.modal', function () {
				$inventoryModal.empty();
			});
			$(".numeric").numberInput("type", "int");
		}, "html");
	}

	
	function showBigModal(e) {
		e.preventDefault();
		$.get($(this).data("url"), function(data) {
			$inventoryBigModal.modal("show").html(data);
			$inventoryBigModal.on('hidden.bs.modal', function () {
				$inventoryBigModal.empty();
			});
			$(".numeric").numberInput("type", "int");
		}, "html");
	}
	
	$("#multipleAllocationBtn, #multipleApprovalBtn, #burnApprovalBtn").click(showBigModal);
	
	$("#burnCardBtn").click(showModal);
	
	$("#reprintTxnBtn").click(function() {
		$reprintModal.modal("show");
	});
	
	$("#inventoryClearButton").click(function(e) {
		e.preventDefault();
		$inventorySearchForm.find("input[type=checkbox]").prop('checked', false);;
		$inventorySearchForm.find("input[type=text]").val("");
		$inventorySearchForm.find("select").val("");
		$inventorySearchBtn.click();
	});
});

