$(document).ready(function() {
	var CONTAINER_ERROR = "#contentError > div";
	var CONTENT_ERROR = "#contentError";
	$inventoryModal = $("#inventoryModal");
	$inventoryList = $("#list_inventory");
	
	var locationCode = $inventoryModal.find("input[name='location']").val();
	$inventoryModal.find("select[name='allocateTo'] > option[value='"+locationCode+"']").remove();
	
	$("#productContainer").on("click", "#addProductBtn", function(e) {
		$productTbody = $("#productTable > tbody");
		$tr = $productTbody.find("tr:first-child"); 
		if($tr.css('visibility') == 'hidden' || $tr.css('display') == 'none') {
			$tr.find("input").val("").prop('disabled', false);
			$tr.find("select").val("").prop('disabled', false);
			$tr.visible();
		} else {
			var index = $productTbody.find("tr").length;
			$row = $tr.clone();
			$row.find("input").val("").prop('disabled', false);
			$row.find("select").val("").prop('disabled', false);
			$row.find(".rowDelete").css('visibility', 'visible');
			
			var rg = new RegExp("\\[0\\]", 'g');
			var html = $row.html();
			html = html.replace(rg, '['+index+']');
			$row.html(html);
			
			$productTbody.append($row);	
		}
	}).on("click", ".rowDelete", function(e) {
		e.preventDefault();
		$tr = $(this).closest("tr"); 
		if($tr.is(":first-child")) {
			$tr.find("input.productRemoveFlag");
			$tr.find("input").val("");
			$tr.find("select").val("");
		} else {
			$tr.remove();
		}
			
	}).on("keyup", ".modalQuantity, .modalStartingSeries", populateEndingSeries)
	.on("keyup", ".modalEndingSeries", populateQuantity);;
	
	function populateEndingSeries() {
		var startSeries = $(this).closest("tr").find(".modalStartingSeries").val();
		var quantity = $(this).closest("tr").find(".modalQuantity").val();
		if(startSeries != null && quantity != null && startSeries != "" && quantity != "") {
			var temp = +startSeries + +quantity - 1;
			var endingSeries = temp.toString(); 
			var padZero = 12 - endingSeries.length;
			if(padZero > 0) {
				for(var i = 0; i < padZero; i++)
					endingSeries = "0" + endingSeries;
			}
			$(this).closest("tr").find(".modalEndingSeries").val(endingSeries);
		} else if(quantity == "" || startSeries == "") {
			$(this).closest("tr").find(".modalEndingSeries").val("");
		}
		
	}
	
	function populateQuantity() {
		var startSeries = $(this).closest("tr").find(".modalStartingSeries").val();
		var endingSeries = $(this).closest("tr").find(".modalEndingSeries").val();
		if(startSeries != null && endingSeries != null && startSeries != "" && endingSeries != "") {
			var quantity = +endingSeries - +startSeries + 1;
			$(this).closest("tr").find(".modalQuantity").val(quantity);
		}
		
	}
	
	$(".allocateSave").unbind("click").click(function(e) {
		e.preventDefault();
		$(this).prop( "diabled", true );
		$form = $("form[name='multipleAllocateDto']");
		ajaxSubmit($form.attr("action") + $(this).data("status"), $form.serialize(), function(data) {
			$form = $("form[name='multipleAllocateDto']");
			var url = $form.data("printUrl") + $form.serialize();
			window.open(url, '_blank', 'toolbar=0,location=0,menubar=0');
			$(this).prop( "diabled", false );
			clickSearch();
		});
	});
	
	$(".allocateApprove").unbind("click").click(function(e) {
		e.preventDefault();
		$form = $("form[name='multipleAllocateDto']");
		ajaxSubmit($form.attr("action") + "approve/" + $(this).data("status"), $form.serialize(), function(data){clickSearch();});
	});
	
	$(".changeAllocationApprove").unbind("click").click(function(e) {
		e.preventDefault();
		$form = $("form[name='multipleAllocateDto']");
		ajaxSubmit($(this).data("url"), $form.serialize(), function(data){clickSearch();});
	});
	
	$(".burnApprove").unbind("click").click(function(e) {
		e.preventDefault();
		$form = $("form[name='multipleAllocateDto']");
		ajaxSubmit($(this).data("url"), $form.serialize(), function(data){clickSearch();});
	});
	
	$(".receiveSave").unbind("click").click(function(e) {
		e.preventDefault();
		$form = $("form[name='multipleAllocateDto']");
		var msg = $(this).data("msg");
		ajaxSubmit($form.attr("action") + "receive", $form.serialize(), function(data){
			
			function print(count) {
				if(count > 0) {
					$form = $("form[name='multipleAllocateDto']");
					var url = $form.data("printUrl") + $form.serialize();
					window.open(url, '_blank', 'toolbar=0,location=0,menubar=0');
				}
				clickSearch();
			}
			var incorrect = data.result.incorrect; 
			var correct = data.result.total - incorrect;
			if(incorrect > 0) {
				getConfirm(msg, function(result) {
					if(result) {
						$form = $("form[name='multipleAllocateDto']");
						ajaxSubmit($form.data("changelocationUrl"), $form.serialize(), function(data){print(correct)});
					} else {
						print(correct);
					}
				});
			} else {
				print(correct);
			}
			
			/*$form = $("form[name='multipleAllocateDto']");
			var url = $form.data("printUrl") + $form.serialize();
			window.open(url, '_blank', 'toolbar=0,location=0,menubar=0');
			clickSearch();*/
		});
	});
	
	$("#multipleAllocateSave").unbind("click").click(function(e) {
		e.preventDefault();
		$(this).prop( "diabled", true );
		$form = $("form[name='multipleAllocateDto']");
		ajaxSubmit($form.attr("action"), $form.serialize(), function(data){ $(this).prop( "diabled", false ); clickSearch();});
	});
	
	$("#burnSave").unbind("click").click(function(e) {
		e.preventDefault();
		$form = $("form[name='multipleAllocateDto']");
		var msg = $(this).data("msg") + ": " + getBurnCardStr() + "?";
		ajaxSubmit($(this).data("urlValidate"), $form.serialize(), function(data){
			getConfirm(msg, function(result) {
				if(result) {
					$form = $("form[name='multipleAllocateDto']");
					ajaxSubmit($form.attr("action"), $form.serialize(), function(data){clickSearch(); });
				}
			});	
		});
	});
	
	
	function getBurnCardStr() {
		var str = "";
		$("#productContainer tbody tr").each(function() {
			if(str != "")
				str += ", "
			str += "<b>" + $(this).find(".modalStartingSeries").val() + " - " + $(this).find(".modalEndingSeries").val() + "</b>";
		});
		return str;
	}
	
	$(".checkAll").click(function(e) {
		var selected = this.checked;
		$("input.referenceNo").each(function() {this.checked = selected;});
	});
	
	function ajaxSubmit(action, data, fn) {
		$("input[type='button'], button", $inventoryModal).prop("disabled", true);
		$.ajax({
			type 	: "POST",
			url 	: action,
			dataType: 'json',
			data 	: data,
			        
			success : function(inResponse) {
				if (inResponse.success) {
					fn(inResponse);
				} 
				else {
					errorInfo = "";
					for (i = 0; i < inResponse.result.length; i++) {
						errorInfo += "<br>" + (i + 1) + ". "
								+ ( inResponse.result[i].code != undefined ? 
										inResponse.result[i].code : inResponse.result[i]);
					}
					$( CONTAINER_ERROR ).html("Please correct following errors: " + errorInfo);
					$( CONTENT_ERROR ).show('slow');
				}
				$("input[type='button'], button", $inventoryModal).prop("disabled", false);
			},
			error : function(jqXHR, textStatus, errorThrown) {
				$( CONTENT_ERROR ).html("Error: " + errorThrown);
				$("input[type='button'], button", $inventoryModal).prop("disabled", false);
			}
		});
	}
	
	jQuery.fn.visible = function() {
	    return this.css('visibility', 'visible');
	};

	jQuery.fn.invisible = function() {
	    return this.css('visibility', 'hidden');
	};
	
});
