$(document).ready( function() {

	var theErrorContainer = $( "#content_userForm" ).find("#content_error");

	var $theUserDialog = $( "#content_userForm" );
	var $theUserForm = $( "#userForm" );



	initFormFields();
	initDialog();
	


	function initFormFields() {

		$theUserForm.submit( function(e) {
			e.preventDefault();
			saveUser( $(this).attr("action") );
		});

		$( "#selectAllRoles" ).change( function(e) {
			var $theCheckedStat = $( this ).is( ":checked" );
			var $theCheckBoxes = $("input:checkbox[name='" + $( this ).data( "form-path" ) + "']");
			$( $theCheckBoxes ).each( function() {
				this.checked = $theCheckedStat;
			});
		});


        $("#content_userForm #authenticationType").change(function() {
          if ($('#content_userForm #authenticationType option:selected').val() === 'LDAP') {
            $("#content_userForm #passowrd_block").hide();
          } else {
            $("#content_userForm #passowrd_block").show();
          }


        });

    }

	function initDialog() {

		$theUserDialog.modal({});
		$theUserDialog.on( "hide", function(e) { if ( e.target === this ) { theErrorContainer.hide(); } });
	}



	function saveUser( inUrl ) {
		$( "#content_userForm" ).find( "#saveButton" ).prop( "disabled", true );
		$.ajax({
			cache 		: false, 
			dataType 	: "json",
			type 		: "POST", 
			url 		: inUrl,
			data		: $theUserForm.serialize(),
			error 		: function(jqXHR, textStatus, errorThrown) {},
			success 	: function( inResponse ) {		
				if ( inResponse.success ) {
					$theUserDialog.modal( "hide" );
					$theUserDialog.remove();
					$( "#list_user" ).ajaxDataTable( "search" );//window.location.href = $theUserForm.data( "url" );
				}
				else {
					$( "#content_userForm" ).find( "#saveButton" ).prop( "disabled", false );
					var theErrorInfo = "";
					$.each( inResponse.result, function( key, value ) {
						theErrorInfo += ( ( key + 1 ) + ". " + value.code + "<br>" );
					});
					theErrorContainer.find( "div" ).html( theErrorInfo );
					theErrorContainer.show( "slow" );
				}
			}
		});
	}

});



