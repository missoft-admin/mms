create table CRM_COMPLAINT (
        id number(19,0) not null,
        CREATED_BY varchar2(255 char),
        CREATED_DATETIME timestamp,
        LAST_UPDATED_BY varchar2(255 char),
        LAST_UPDATED_DATETIME timestamp,
        ASSIGNED_TO varchar2(20 char),
        COMPLAINTS varchar2(255 char) not null,
        FILED_DATE timestamp,
        EMAIL varchar2(50 char),
        FIRST_NAME varchar2(100 char) not null,
        LAST_NAME varchar2(100 char),
        MOBILE_NO varchar2(20 char),
        priority varchar2(20 char),
        status varchar2(20 char),
        STORE_CODE varchar2(20 char),
        TICKET_NO varchar2(20 char),
        GENDER varchar2(15 char) not null,
        primary key (id)
    );

alter table CRM_COMPLAINT 
        add constraint UK_bef8pis62u3ci04nifflw89j5 unique (TICKET_NO);

alter table CRM_COMPLAINT 
        add constraint FK_l6atb9iev9h13y57xjr0c8jnw 
        foreign key (GENDER) 
        references CRM_REF_LOOKUP_DTL;