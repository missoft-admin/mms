var memberFormDialog = null;
var memberForm = null;
var updatePinDialog = null;
var updatePinForm = null;
var message_accountIdToBeGenerated = null;
var containerError = null;
var professional = null;
var member = null;
var employee = null;
var hasEmail = null;
var customerSegmentation = null;
var custSegSelect = null;
var childrenAge = null;
var childrenCount = null;
var acceptEmail = null;
var provinces = null;
var banks = null;
var dialog = null;
var isProfessional = false;

$(document).ready(function() {
	memberFormDialog = $("#memberFormDialog");
	professionalFormDialog = $("#professionalFormDialog");
	memberForm = "#memberForm";
	updatePinDialog = $("#updatePinDialog");
	updatePinForm = $("#updatePin");
	containerError = ".errors";
	professional = ".professionalDetails";
	member = ".memberDetails";
	employee = ".employeeDetails";
	hasEmail = ".hasEmail";
	custSegSelect = "#customerSegmentation";
	childrenAge = "#childrenAge";
	childrenCount = "#childrenCount";
	acceptEmail = "#acceptEmail";
	banks = ".banks";
	dialog = null;
	isProfessional = false;
	
	memberFormDialog.modal({
		show	:	false,
		keyboard:	false, 
		backdrop: "static" 
	});
	
	professionalFormDialog.modal({
		show	:	false,
		keyboard:	false, 
		backdrop: "static" 
	});
	
	$( "#memberFormDialog, #professionalFormDialog" ).on( "hide", function(e) {
		if ( e.target === this ) {
			$(this).find('.birthdate').datepicker( "update", new Date() );
		}
	});
	$(".memberTypeSelect").hide();
	$("#employeeTypeSelect").hide();
	$(containerError).hide();
	$(professional).hide();
	$("#isSupplement").hide();
	
	updatePinDialog.modal({
		show	:	false,
		keyboard:	true
	});
	
	var nowTemp = new Date();
    var now = new Date(nowTemp.getFullYear(), nowTemp.getMonth(), nowTemp.getDate(), 0, 0, 0, 0);
    var birthdate = $('.birthdate');
    birthdate.datepicker({

    	
		format		:	'dd-mm-yyyy',
		endDate		:	'-0d',
		autoclose	:	true
	});
    
    birthdate.each(function() {
    	$(this).change(function() {
    		var pin = $(this).closest("#memberForm").find('#pin');
    		if(!$.trim(pin.val())) {
    			var val = $(this).val().split("-");
    			pin.val(val[0] + val[1] + val[2].substr(2, 2));
//    			console.log(pin.val());
    		}
    	});
    });
    
    $("input[type='checkbox'][name='marketingDetails.informedAbout']").click(toggleOthersInput);
    $("input[type='checkbox'][name='marketingDetails.informedAbout']").each(toggleOthersInput);
    
   /* birthdate.change(function() {
		var pin = $('#pin');
		if(!$.trim(pin.val())) {
			var val = birthdate.val().split("-");
			pin.val(val[0] + val[1] + val[2].substr(2, 2));
//			console.log(pin.val());
		}
	});*/
    
    
	
	message_accountIdToBeGenerated = $(".accountId").html();
	
	$(".close_form").click(function(){
		dialog.modal("hide");
		dialog.find(memberForm)[0].reset();
	});
	
	$(hasEmail).hide();
	$(".email").focusout(function() {
		var val = $(this).val();
		setUsernamePasswordRequired(val);
		toggleAcceptEmail(val);
	});
	$(".email").change(function() {
		var val = $(this).val();
		setUsernamePasswordRequired(val);
		toggleAcceptEmail(val);
	});
	
	$(".customerGroup").change(function(){
		setSegmentationOptions($(this).val());
	});
	
	$(childrenCount).change(function() {
		childrenAgeFields($(this).val());
	});
	
	$(childrenCount).focusout(function() {
		childrenAgeFields($(this).val());
	});
	
	$(".floatInput").numberInput({ "type" : "float" });
	$(".intInput").numberInput({ "type" : "int" });
    $(".wholeInput").numberInput({ "type" : "whole" });
	
	$(".provinceSelect").change(function() {
		var province = $(this).val();
		$citySelect = $(this).closest(".provinceCityBlock").find(".citySelect");
		$citySelect.empty();
		$citySelect.append("<option></option>");
		if(province != "" && province != null) {
			var options = provinces[province];	
			if(options != null) {
				$.each(options, function(index, value) {
					$citySelect.append("<option value='" + value + "'>" + value + "</option>");
				});	
			}
		}
	});
	
	$("#hasCreditCard").change(function() {
		if($(this).is(':checked')) {
			$(banks).show();
		}
		else {
			$("#banks").val("");
			$(banks).hide();
		}
	});
	$(banks).hide();
});

function toggleOthersInput() {
	if($(this).data("text") == "OTHERS") {
		$others = $(this).parent().find("input[type='text']");
		if($(this).is(":checked")) {
			$others.removeAttr("disabled");
		} else {
			$others.attr("disabled", "disabled");
			$others.val("");
		}	
	}
	
}

function toggleAcceptEmail(value) {
	console.log("email " + value);
	if(value) {
		dialog.find(acceptEmail).show();
	}
	else {
		dialog.find(acceptEmail).hide();
	}
}

function childrenAgeFields(count) {
	dialog.find(childrenAge).empty();
	for(var i = 0; i < count; i++) {
		dialog.find(childrenAge).append($("<input type='text' name='customerProfile.ageOfChildren' style='margin-bottom: 1px'/>"));
	}
	if(count > 4) {
		dialog.find(childrenAge).css('height', 'auto');
	}
}


function setCustomerSegmentation(map) {
	customerSegmentation = map;
}

function setProvinces(provinceMap) {
	provinces = provinceMap;
}



function setSegmentationOptions(key) {
	if(key == null || key == undefined) return;
	var options = customerSegmentation[key];
	dialog.find(custSegSelect).empty();
	$.each(options, function(k, value) {
		dialog.find(custSegSelect).append("<option value='" + k + "'>" + value + "</option>");
	});
}

function setUsernamePasswordRequired(val) {
	if($.trim(val)) {
		dialog.find(hasEmail).show();
	}
	else {
		dialog.find(hasEmail).hide();
	}
}

function hideErrors() {
	dialog.find(containerError).hide("slow");
}

function createCustomer(query) {
	isProfessional = false;
	populateMember(function() {saveMember("/new", query)}, "/populate/" + null, null);
}

function createProfessional(query) {
	isProfessional = true;
	populateMember(function() {saveMember("/new", query)}, "/professional/new");
}

function createSupplement(parentId, query) {
	isProfessional = false;
	populateMember(function() {saveMember("/new", query)}, "/supplement/new/" + parentId);
}

function updateMember(memberId, query) {
	isProfessional = false;

	populateMember(function() {saveMember("/update/" + memberId, query)}, "/populate/" + memberId, false, memberId);
}

function updatePin(memberId, query) {
	$(containerError).hide();
	updatePinDialog.modal("show");
	updatePinForm.trigger( "reset" );
	
	$("#save_pin").unbind("click").click(function(){
		$.ajax({
			type 	: "POST",
			url 	: updatePinForm.attr("action") + "/pin/update/" + memberId,
			dataType: 'json',
			data 	: updatePinForm.serialize(),
			        
			success : function(inResponse) {
				if (inResponse.success) {
					$(containerError).hide('fast');
					updatePinForm.unbind("submit").submit( function() {
						if(!$.trim(query)) {
							query = "page=1&size=10";
						}
						$(this).attr("action", $(this).attr("action") + "?" + query);
					});
					updatePinForm.submit();
				} 
				else {
					errorInfo = "";
					for (var i = 0; i < inResponse.result.length; i++) {
						errorInfo += "<br>" + (i + 1) + ". "
								+ ( inResponse.result[i].code != undefined ? 
										inResponse.result[i].code : inResponse.result[i]);
					}
					$(".errorcontent").html("Please correct following errors: " + errorInfo);
					$(containerError).show('slow');
				}
			},
			error : function(jqXHR, textStatus, errorThrown) {
				$(containerError).html("Error: " + errorThrown);
			}
		});
	});
}

function saveMember(url, query) {
	dialog.find("#save_form").unbind("click").click(function() {
		dialog.find( "#save_form" ).prop( "disabled", true );
		if(dialog.find("[name='idNumberTxt']").val() != "" && dialog.find("[name='idNumberType']:checked").val() != "") {
			dialog.find("[name='idNumber']").val(dialog.find("[name='idNumberType']:checked").val() +"-"+ dialog.find("[name='idNumberTxt']").val());
		}
		
		//NPWP
		if(dialog.find("#npwpId1").val() != "" || dialog.find("#npwpId2").val() != "" || 
				dialog.find("#npwpId3").val() != "" || dialog.find("#npwpId4").val() != "" || 
				dialog.find("#npwpId5").val() != "" || dialog.find("#npwpId6").val() != "") {
			var first = dialog.find("#npwpId1").val();
			if(first != "") {
				var padZero = 2 - first.length;
				if(padZero > 0) {
					for(var i = 0; i < padZero; i++)
						first = "0" + first;
				}	
			}
			var last = dialog.find("#npwpId6").val();
			if(last == "") {
				last = "000";	
			}
			var npwpId = first +
					dialog.find("#npwpId2").val() +
					dialog.find("#npwpId3").val() +
					dialog.find("#npwpId4").val() +
					dialog.find("#npwpId5").val() +
					last;
			dialog.find("#npwpId").val(npwpId);	
		}
		console.log(dialog.find(memberForm).serialize());
		$.ajax({
			type 	: "POST",
			url 	: dialog.find(memberForm).attr("action") + url,
			dataType: 'json',
			data 	: dialog.find(memberForm).serialize(),
			        
			success : function(response) {
				dialog.find( "#save_form" ).prop( "disabled", false );
				if (response.success) {
					dialog.find(containerError).hide('fast');
                    dialog.modal("hide");
                    dialog.find(memberForm)[0].reset();

                    // Ugly hack to reload the list
                    var $searchButton = $("#search_submit" );
                    if($searchButton.length > 0) {
                        $searchButton.click();
                    }
/*
    Allan: At this point, data is already saved. Consult me if there's a need to restore this block.
					dialog.find(memberForm).unbind("submit").submit( function() {
						$(this).attr("action", $(this).attr("action") + "?" + query);
					});
					dialog.find(memberForm).submit();
*/
				} 
				else {
					errorInfo = "";
					var count = 0;
					for (var i = 0; i < response.result.length; i++) {
						if(response.result[i].code != undefined && response.result[i].code != "typeMismatch") {
							count++;
							errorInfo += "<br>" + (count) + ". "
							+ ( response.result[i].code != undefined ? 
									response.result[i].code : response.result[i]);
							
						}
						
					}
					$(".errorcontent").html("<strong>Please correct following errors: </strong>" + errorInfo);
					dialog.find(containerError).show('slow');
				}
			},
			error : function(jqXHR, textStatus, errorThrown) {
				dialog.find( "#save_form" ).prop( "disabled", false );
				dialog.find(containerError).html("Error: " + errorThrown);
			}
		});
	});
}

function populateMember(fn, url, isNew, memberId) {
	$(containerError).hide();
	
	$.ajax({
		type 	: "POST",
		url 	: $(memberForm).attr('action') + url,
		dataType: "json",
		success	: function(response) {
			var result = response.result;
			if((result.memberType != null && (result.memberType.description == "PROFESSIONAL") || isProfessional)) {
				dialog = professionalFormDialog;
			} else {
				dialog = memberFormDialog;
			}
			
			if(!response.success) {
				alert(result);
				dialog.modal("hide");
				return;
			}
			
			
			
			dialog.find(childrenAge).empty();
			dialog.find(".added-field").remove();
			
			
			
			$.each(result, function(key, value) {
				
				if ( key == "accountId" ) {
					dialog.find(".accountId").html(message_accountIdToBeGenerated);
					if ( value != null && value != "" ) {
						dialog.find(".accountId").html( value );
					}
					else {
						dialog.find(".accountId").html(message_accountIdToBeGenerated);
					}
				}

				/*ea20140704: for mandatory info*/
				if ( value != null 
						&& ( key == "pin" || key == "firstName" || key == "customerProfile" || key == "address" || key == "contact" || key == "accountId"
							|| key == "professionalProfile" || key == "companyName" || key == "idNumber" || key == "bestTimeToCall" ) ) {
					if( ( result.memberType != null && (result.memberType.description == "PROFESSIONAL") || isProfessional ) ) {
						MandatoryProfInfo.fill( key, value );
					}
					else {
						MandatoryInfo.fill( key, value );
					}
				}

				if ( value != null && ( key == "address" || key == "channel" || key == "customerProfile" || key == "professionalProfile" || key == "marketingDetails" ) ) {
                    $.each( value, function ( k, v ) {
                        if ( v != null && k == "businessAddress" ) {
                            $.each( v, function ( k1, v1 ) {
                                fillField( key + "." + k + "." + k1, v1 );
                            } );
                        }
                        fillField( key + "." + k, v );
                    } );
                }
				else if ( key == "registeredStore" ) {
					$.get( 
							dialog.find( "#content_registeredStore" ).data( "url-store" ) +(  memberId != undefined? memberId : "" ),
							function(data) {
								dialog.find( "#content_registeredStore" ).empty();
								dialog.find( "#content_registeredStoreStatic" ).hide();
								$.each( data.result, function( idx, store) {
									dialog.find( "#content_registeredStore" ).append( "<option value='" + store.code + "'>" + store.codeAndName + "</option>" );
								});
								$( "#content_registeredStore" ).val( value );
								if ( !memberId && result.memberType && result.memberType.description == "EMPLOYEE" ) {
									dialog.find( "#content_registeredStore" ).show();
								}
								else {
									if(value == null)
										value = "";
									var registeredStoreDesc = dialog.find("#content_registeredStore option[value='"+value+"']").text();
									var storeValue = registeredStoreDesc;
									dialog.find( "#content_registeredStoreStatic" ).html(storeValue);
									dialog.find( "#content_registeredStoreStatic" ).show();
									dialog.find( "#content_registeredStore" ).val(value);
//									dialog.find( "#content_registeredStore" ).html(value);
									dialog.find( "#content_registeredStore" ).hide();
								}
							},
							"json"
					);
				}
				else if ( key == "accountStatus" ) {
					dialog.find( "#accountStatus" ).prop( "checked", value == 'TERMINATED' );
                } else if ( key == "configurableFields" ) {
                    updateConfigurableFieldsDisplay($('.memberConfigurableFieldsContainer'), value);
                }
                else {
    				fillField(key, value);
                }
			});
			
			function populateCityProvince() {
				var name = $(this).attr("name");
				var val = result;
				$.each(name.split("."), function (index, value) {
					if(val[value] == null) {
						val = "";
						return false;
					} else 
						val = val[value];
				});
				if(val == null) {
					val = "";
				}
				var notFound = true;
				$(this).find("option").each(function() {
					var optionVal = $(this).attr("value");
					if(optionVal == null) {
						optionVal = "";
					}
					if(optionVal.toLowerCase() == val.toLowerCase()) {
						$(this).prop("selected", "selected");
						notFound = false;
						return false;
					}
				});
				if(notFound && val != "") {
					$(this).append($("<option value='"+val+"'>"+val+"</option>").prop("selected", "selected"));
				}
			}
			
			dialog.find(".provinceSelect").each(populateCityProvince);
			dialog.find(".provinceSelect").trigger("change");
			dialog.find(".citySelect").each(populateCityProvince);
			
			$("[name='idNumber']").each(function() {
		    	if($(this).val() != null && $(this).val() != "") {
		    		var vals = $(this).val().split("-");
		    		$parent = $(this).parent();
		    		$parent.find("[name='idNumberType'][value='"+vals[0]+"']").prop("checked", true);
		    		$parent.find("[name='idNumberTxt']").val(vals[1]);
		    	}
		    	
		    });
			
			dialog.find("#mainPaneMemberType").find("select[name='memberType']").trigger("change");
			dialog.find("#firstTab > a").click();
			dialog.modal("show");
			dialog.find(containerError).hide();
			dialog.find(professional).hide();
			dialog.find(member).show();
			dialog.find(hasEmail).hide();
			if(dialog.find("#email").val()) {
				dialog.find(hasEmail).show();
			}
			
			if(result.customerProfile.creditCardOwnership) {
				dialog.find(banks).show();
			}
			else {
				dialog.find(banks).hide();
			}
			
//			$("#memberTypeStatic").hide();
//			$("#memberTypeSelect").show();
			dialog.find("#registeredDate").html(result.registrationDate);
			dialog.find("#loyaltyCardIssued").html(result.loyaltyCardDateIssued);
			dialog.find("#loyaltyCardExpire").html(result.loyaltyCardDateExpired);
			dialog.find("#memberBarcode").html(result.loyaltyCardNo);
			dialog.find("#createdBy").html(result.createdBy);
			
			console.log(result.customerProfile.creditCardOwnership);
			
			if(!result.customerProfile.banks) {
				dialog.find("#noBank").prop("selected", true);
			}
			
			if(result.parent != null && result.parent != "") {
				dialog.find("#parent").html(result.parentName + " - " + result.parent);
				dialog.find(".isPrimary").hide();
				dialog.find("#isSupplement").show();
			}
			else {
				dialog.find("#parent").html("");
				dialog.find(".isPrimary").show();
				dialog.find("#isSupplement").hide();
				getSupplements(result.accountId);
			}

			if ( result.memberType == null || result.memberType != null && result.memberType.description != "EMPLOYEE" ) {
				dialog.find( "#accountEnabled" ).hide();
			}
			if(result.memberType != null) {
				if(result.memberType.description == "PROFESSIONAL") {
					dialog.find(professional).show();
					dialog.find(member).hide();
					dialog.find(employee).hide();
					
					setSegmentationOptions(dialog.find(".customerGroup").val());
					
					if(result.professionalProfile.customerSegmentation) {
						dialog.find(custSegSelect).val(result.professionalProfile.customerSegmentation.code);
					}
				}
				dialog.find("#contentMemberType").html(result.memberType.description);
				dialog.find( "#employeeTypeSelect" ).show();
				if ( result.empType != null ) {
					dialog.find( "#employeeType" ).val( result.empType.code );
				}
			}
			else {
				dialog.find("#contentMemberType").html("INDIVIDUAL");
			}
			
			fn();
		}
	});
}

function fillField(key, value) {
//	console.log(key + " " + value);
	if(key == "npwpId" && value != null && value != "") {
		var vals = value.replace('-', '.').split('.');
		if(vals.length > 1) { //for formatted db npwp data 
			for (var i = 0; i < vals.length; i++) {
				dialog.find("#npwpId" + (i+1)).val(vals[i]);
			}	
		} else {
			dialog.find("#npwpId1").val(value.substring(0,2));
			dialog.find("#npwpId2").val(value.substring(2,5));
			dialog.find("#npwpId3").val(value.substring(5,8));
			dialog.find("#npwpId4").val(value.substring(8,9));
			dialog.find("#npwpId5").val(value.substring(9,12));
			dialog.find("#npwpId6").val(value.substring(12,15));	
		}
	}
	
	if(key === 'customerProfile.ageOfChildren' && value !== null) {
		childrenAgeFields(value.length);
	}

//	if((key === 'contact' || key === 'bestTimeToCall') && value !== null) {
////		value = value.split(',');
//
//		for(var i = 1; i < value.length; i++) {
//			addField(key);
//		}
//	}
	
	var name = dialog.find("[name='" + key + "']");

	if(({}).toString.call(value) == "[object Object]" && value.code !== undefined) {
		value = value.code;
	}

	if ( key == 'marketingDetails.informedAbout' || 'marketingDetails.interestedProducts' ) {
		$.each( name, function( k, obj ) {
			if( $(obj).is("input[type='checkbox']") ) {
				if( name.length == 1 )
					$(obj).prop( "checked", value ); 
				else if(value != null && value != ""){
					var values = null;
					try { values = value.split(","); }
					catch (e) { values = value; }
					for(var i = 0; i < values.length; i++) {
						$field = $("input[name='"+key+"'][value='" + values[i] + "']");
						if($field.length > 0){
							$("input[type='checkbox'][name='"+key+"'][value='" + values[i] + "']").prop("checked", true);
						}
						else {
							$("input[type='text'][name='"+key+"']").val(values[i]);
						}
					}
				}
					
			}
		});
	}
	
	if( (key === "customerProfile.banks" || key === "customerProfile.interests") && $.type(value) == "string") {
		var values = value.split(',');
		$(name).val(values);
		return;
	}
	
	if($.isArray(value)) {
		if(key === 'contact' || key === "customerProfile.ageOfChildren") {
			$.each(name, function(k, v) {
				$(v).val(value[k]);
			});
		}
		else if ( key == 'bestTimeToCall' || key == 'socialMediaAccts' || key == 'marketingDetails.informedAbout' ) {
			$.each( name, function( k, obj ) {
				if( $(obj).is("input[type='checkbox']") ) {
//					console.log(key + " " + value);
					if( name.length == 1 )
						obj.prop( "checked", value ); 
					else if(value != null && value != ""){
						var values = null;
						try { values = value.split(","); }
						catch (e) { values = value; }
						for(var i = 0; i < values.length; i++) {
							$field = $("input[name='"+key+"'][value='" + values[i] + "']"); 
							if($field.length == 1)
								$("input[type='checkbox'][name='"+key+"'][value='" + values[i] + "']").prop("checked", true);
							else {
								$("input[type='text'][name='"+key+"']").val(values[i]);
								$("input[type='text'][name='"+key+"']").show();
							}
						}
					}
						
				}
			});
		}
		
		for(var i = 0; i < value.length; i++) {
			$("input[value='" + value[i].code + "']").prop("checked", true);
		}
	}
	else if(name.is("input[type='checkbox']")) {
//		console.log(key + " " + value);
		if(name.length == 1)
			name.prop("checked", value); 
		else if(value != null && value != ""){
			var values = null;
			try { values = value.split(","); }
			catch (e) { values = value; }
			for(var i = 0; i < values.length; i++) {
				$field = $("input[name='"+key+"'][value='" + values[i] + "']"); 
				if($field.length == 1)
					$("input[type='checkbox'][name='"+key+"'][value='" + values[i] + "']").prop("checked", true);
				else {
					$("input[type='text'][name='"+key+"']").val(values[i]);
				}
			}
		}
	}
	else if(name.is("input[type='radio']")) {
		$("input[value='" + value + "']").prop("checked", true);
	}
	else if(name != null) {
 		name.val( value );
 		if ( key.indexOf( "birthdate" ) != -1 ) {
 			//name.val(  );
 			try {
 				var dateValue = value ? 
 							( new Date( value ).getTime()? new Date( value ) 
 								: new Date( $.trim( value ).split( " " )[0] ).getTime()? 
 										new Date( $.trim( value ).split( " " )[0] ) : null ) 
 							: null;
 	 			dialog.find( "#birthdate" ).datepicker( "update", dateValue );
 			}catch(e) {}
 		}
	}
}

function addField(name) {
	dialog.find("#" + name).append($("<br class='added-field' /><input type='text' class='form-control added-field' name='" + name + "' style='margin-top: 1px;' />"));
}

function addFields() {
	//addField('contact');
	addField('bestTimeToCall');
}

function getSupplements(accountId) {
	var memberList = dialog.find("#supplements");
	memberList.html("");
	
	var headers = {
		username: memberList.data("hdrUsername")  + "",
		accountno: memberList.data("hdrAccountno") + "",
		points:	memberList.data("hdrPoints") + "",
		name: memberList.data("hdrMember") + ""
	};
	
	table = memberList.ajaxDataTable({
		'autoload': true,
		'ajaxSource': memberList.data("url") + accountId,
		'columnHeaders': [
			headers['username'],
			headers['accountno'],
			headers['name'],
			headers['points'],
		],
		'modelFields': [
		    {name: 'username'},
		    {name: 'accountId'},
		    {name : 'firstName', sortable : true, fieldCellRenderer : function (data, type, row) {
		           return row.formattedMemberName;
	         }},
		    {name: 'totalPoints'}
		]
	});
}


/*LOYALTY CARD ASSIGNMENT*/
function assignLoyaltyCard( inUrl, inMemberId ) {
	if($("#list_member").data("userInventory") == null || $("#list_member").data("userInventory") == "") {
		$("#confirmbox #confirmFalse").hide();
		$("#confirmbox").on('hidden.bs.modal', function() {
			$("#confirmbox #confirmFalse").show();
		});
		getConfirm($("#list_member").data("noInventorylocationMsg"), function(ret) {});
	} else {
		$.ajax({
			cache 		: false, 
			dataType 	: "html",
			type 		: "GET", 
			url 		: inUrl,
			error 		: function(jqXHR, textStatus, errorThrown) {}, 
			success 	: function( inResponse ) {
				$("#form_loyaltyCardAssignment").html( inResponse );
			}
		});	
	}
	
}