<%@ include file="../../common/taglibs.jsp" %>

<c:set var="ENCTYPE" value="application/x-www-form-urlencoded"/>

<div class="modal-dialog">
  <div class="modal-content">
    
    <div class="modal-header">
      <button aria-hidden="true" data-dismiss="modal" class="close" type="button">&times;</button>
      <h4><spring:message code="label_multiple_approval" /></h4>      
    </div>
 
    <div class="modal-body">
      <form:form id="multipleAllocateDto" name="multipleAllocateDto" modelAttribute="multipleAllocateDto" action="${pageContext.request.contextPath}/loyaltycardinventory/saveallocation/" method="POST" enctype="${ENCTYPE}" cssClass="form-reset" >

      <div id="contentError" class="hide alert alert-error">
        <button type="button" class="close" onclick="$(this).parent().hide();">&times;</button>
        <div><form:errors path="*"/></div>
      </div>
      
      <form:hidden path="batchRefNo"/>
      
      <div class="contentMain">
        <div>
          <table id="productTable" class="table table-condensed table-compressed table-striped">
            <thead>
              <tr>
                <th><input type="checkbox" class="checkAll" /></th>
                <th><spring:message code="inventory_prop_referenceno" /></th>
                <th><spring:message code="mo_tier_prop_startingseries" /></th>
                <th><spring:message code="mo_tier_prop_endingseries" /></th>
                <th><spring:message code="inventory_prop_location" /></th>
                <th><spring:message code="inventory_prop_allocateto" /></th>
                <th><spring:message code="inventory_prop_encodedby" /></th>
              </tr>
            </thead>
            <tbody>
              <c:forEach var="item" items="${multipleAllocateDto.allocations}" varStatus="status" >
              <tr>
                <td><input type="checkbox" class="referenceNo" name="allocations[${status.index}].referenceNo" value="${item.referenceNo}" />
                <input type="hidden" name="allocations[${status.index}].location" value="${item.location}" />
                <input type="hidden" name="allocations[${status.index}].transferTo" value="${item.transferTo}" />
                </td>
                <td>${item.referenceNo}</td>
                <td>${item.startingSeries}</td>
                <td>${item.endingSeries}</td>
                <td>${item.locationName}</td>
                <td>${item.transferToName}</td>
                <td>${item.encodedBy} (${item.allocateDate})</td>
              </tr>
              </c:forEach>
            </tbody>
          </table>
        </div>
      </div>
      </form:form>
    
    </div>
    <div class="modal-footer">
      <button type="button" id="" class="btn btn-default" data-dismiss="modal"><spring:message code="label_cancel"/></button>
      <button type="button" id="" data-action="reject" data-status="${inactive}" class="btn btn-primary allocateApprove"><spring:message code="label_reject" /></button>
      <button type="button" id="" data-action="approve" data-status="${allocated}" class="btn btn-primary allocateApprove"><spring:message code="label_approve" /></button>
    </div>
  </div>
</div>

<script src="<spring:url value="/js/viewspecific/loyalty/allocate.js"/>"></script>