package com.transretail.crm.common.web;

/**
 * @author Allan G. Ramirez (agramirez@exist.com)
 */
public enum RestConstants {
    KEY_REST_ERROR("ERROR"), KEY_REST_RESULT("RESULT");

    private String value;

    private RestConstants(String value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return value;
    }
}
