<%@include file="../common/messages.jsp"%>

<style type="text/css">
<!--
  #vendor_list_container .dataTable tr td:first-child a {
    color: #0080ff !important;
  }
  
  .modal {
    width: 84%;
  }
  
  #vendor_list_container .table td {
    vertical-align: top;
  }
  
  #vendor_list_container .table tbody td, .table tfoot th, .table tfoot td {
    padding: 9px 20px;
  }

  .modal-fullscreen { left: 1px; bottom: 0px; top: 0% !important; width: 100%; margin-left: 0% !important; height: 95%; }
  .modal-fullscreen .modal-dialog { position: relative !important; }
  .modal-fullscreen .modal-body { position: absolute; width: 94%; top: 50px; bottom: 60px; }
  .modal-fullscreen .modal-footer { position: absolute !important; bottom: 0px; margin-bottom: 0% !important; width: 100%; }

  .span12 { width: 91%; top: 8% !important; }
  .span12 .modal-body {}
  .control-group { margin-top: 10px; margin-bottom: 10px; }
  .control-group .controls { margin-right: 5px !important; margin-left: 200px !important; }
  .checkbox .control-label { text-align: left !important; margin-left: 5px; text-transform: capitalize !important; }
  .required { font-size: inherit !important; }
  .checkbox { clear: both; }
  .space-top-01 { padding-top: 20px; }
  .input-mini { width: 40px; }

  #addEditDialog label {
    float: left;
    padding: 5px 0 0;
    text-align: right;
    width: 190px;
  }

  table {
  	clear: both;
  	float: left
  }
  .control-group.top-zero {
      margin-bottom: 29px;
      margin-top: 0;
  }
  .table tbody tr td:last-child {
      border-left: none;
      min-width: auto;
  }
  .row-fluid {
      display: inline;
      width: 100px !important;
  }
  #vendor_form.well label {
      margin: 0 5px;
      text-align: left;
  }
  
  /*Chrome specific */
  @media screen and (-webkit-min-device-pixel-ratio:0) {    
  	.well .row-fluid {
  	  float: left;
  	}
  	
  	.well .row-fluid .blank {
  	  height: 25px;
  	}
  }
-->
</style>


<div class="page-header page-header2">
<h1><spring:message code="menutab_giftcard_vendors" /></h1>
</div>

<div id="vendor_panel">
	<form id="vendor_form" class="form-horizontal well">
		<div class="control-group mb0 mt0">
			<fieldset class="row-fluid">
				<label for="q_formalName" class="control-label">
				  <spring:message code="gc.vendor.label.formalname" />
				</label> 
				<input type="text" id="q_formalName" name="formalName" />
			</fieldset>
			<fieldset class="row-fluid ml10">
				<label for="q_contactPersonName" class="control-label">
				  <spring:message code="gc.vendor.label.contactname" />
				</label> 
				<input type="text" id="q_contactPersonName" name="contactPersonName" />
			</fieldset>
			<fieldset class="row-fluid ml10">
				<label for="q_bankName" class="control-label">
				  <spring:message code="gc.vendor.label.bankname" />
				</label> 
				<input type="text" id="q_bankName" name="bankName" />
			</fieldset>
			<fieldset class="row-fluid ml10" style="width: auto !important;">
			  <label class="blank" style="float: none;">&nbsp;</label>
  			<input id="search" class="btn btn-primary" type="button" value="<spring:message code="search" />" />
        <input id="resetBtn" type="button" value="<spring:message code="label_clear" />" class="btn custom-reset" data-default-id="search" />
			</fieldset>
			
		</div>
		<div class="control-group mb0 mt0">
		  <fieldset class="row-fluid">
				<label for="q_paymentMethod" class="control-label"><spring:message
						code="gc.vendor.label.paymentmethod" /></label> <input type="text"
					id="q_paymentMethod" name="paymentMethod" />
			</fieldset>
			<fieldset class="row-fluid ml10">
				<label for="q_enabled" class="control-label"><spring:message
						code="gc.vendor.label.enabled" /></label> 
                <select id="q_enabled" name="enabled" class="pointsSearchDropdown">
                  <option value=""></option>
                  <option value="true">true</option>
                  <option value="false">false</option>
                </select>
			</fieldset>
			<fieldset class="row-fluid ml10">
				<label for="q_defaultVendor" class="control-label"><spring:message
						code="gc.vendor.label.defaultvendor" /></label> <select
					id="q_defaultVendor" name="defaultVendor">
					<option value=""></option>
					<option value="yes"><spring:message code="yes" /></option>
					<option value="no"><spring:message code="no" /></option>
				</select>
			</fieldset>
		</div>
		
		<div class="control-group mb0 mt0">
			<fieldset class="row-fluid">
				<label for="q_region" class="control-label"><spring:message
						code="gc.vendor.label.region" /></label> <input type="text" id="q_region"
					name="region" />
			</fieldset>
		</div>
		
		<div class="clearfix"></div>

	</form>
	<div id="successMessages">&nbsp;</div>
	<button id="add" type="button" class="btn btn-primary pull-right">
		<spring:message code="add" /> Vendor
	</button>
	<div id="vendor_list_container"></div>
</div>
<div id="addEditDialog" class="modal hide" style="display: none;">
  
  <div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
    <h4>Add Vendor</h4>
  </div>
  
	<div class="modal-body container-fluid">
		<form id="addUpdateVendorForm" class="row-fluid">
			<div class="errorMessages error"></div>
			<div class="pull-left">
				<div class="control-group">
					<spring:message code="gc.vendor.label.contactemail"
						var="contactemail" />
					<label class="control-label" for="contactPersonEmail"
						title="${contactemail}">${contactemail}</label>

					<div class="controls">
						<input id="contactPersonEmail" name="contactPersonEmail"
							title="${contactemail}" type="text" value="">
					</div>
				</div>
				<div class="control-group">
					<spring:message code="gc.vendor.label.paymentmethod"
						var="paymentmethod" />
					<label class="control-label" for="paymentMethod"
						title="${paymentmethod}">${paymentmethod}</label>

					<div class="controls">
						<input id="paymentMethod" name="paymentMethod"
							title="${paymentmethod}" type="text" value="">
					</div>
				</div>
				<div class="control-group">
					<spring:message code="gc.vendor.label.bankname" var="bankname" />
					<label class="control-label" for="bankName" title="${bankname}">${bankname}</label>

					<div class="controls">
						<input id="bankName" name="bankName" title="${bankname}"
							type="text" value="">
					</div>
				</div>
				<div class="control-group">
					<spring:message code="gc.vendor.label.bankaccountno"
						var="bankaccountno" />
					<label class="control-label" for="bankAccountNo"
						title="${bankaccountno}">${bankaccountno}</label>

					<div class="controls">
						<input id="bankAccountNo" name="bankAccountNo"
							title="${bankaccountno}" type="text" value="">
					</div>
				</div>
				<div class="control-group">
					<spring:message code="gc.vendor.label.bankaccountname"
						var="bankaccountname" />
					<label class="control-label" for="bankAccountName"
						title="${bankaccountname}">${bankaccountname}</label>

					<div class="controls">
						<input id="bankAccountName" name="bankAccountName"
							title="${bankaccountname}" type="text" value="">
					</div>
				</div>
				<div class="control-group">
					<spring:message code="gc.vendor.label.daysafteraccountspayment"
						var="daysafteraccountspayment" />
					<label class="control-label" for="daysAfterAccountsPayment"
						title="${daysafteraccountspayment}">${daysafteraccountspayment}</label>

					<div class="controls">
						<input id="daysAfterAccountsPayment" class="intInput"
							name="daysAfterAccountsPayment"
							title="${daysafteraccountspayment}" type="text" value="">
					</div>
				</div>
				<div class="control-group">
					<spring:message code="gc.vendor.label.invoicetitle"
						var="invoicetitle" />
					<label class="control-label" for="invoiceTitle"
						title="${invoicetitle}">${invoicetitle}</label>

					<div class="controls">
						<input id="invoiceTitle" name="invoiceTitle"
							title="${invoicetitle}" type="text" value="">
					</div>
				</div>
				<div class="control-group top-zero">
					<spring:message code="gc.vendor.label.enabled" var="enabled" />
					<label class="control-label" for="enabled" title="${enabled}">${enabled}</label>

					<div class="controls">
						<input id="enabled" name="enabled" title="${enabled}"
							type="checkbox" class="manual">
					</div>
				</div>
				<div class="control-group">
					<spring:message code="gc.vendor.label.maxcardsperdaymanufacture"
						var="maxcardsperdaymanufacture" />
					<label class="control-label" for="cardsPerDayManufactureMax"
						title="${maxcardsperdaymanufacture}">${maxcardsperdaymanufacture}</label>

					<div class="controls">
						<input id="cardsPerDayManufactureMax" class="intInput"
							name="cardsPerDayManufactureMax"
							title="${maxcardsperdaymanufacture}" type="text" value="">
					</div>
				</div>
				<div class="control-group">
					<spring:message code="gc.vendor.label.maxcardsperdayship"
						var="maxcardsperdayship" />
					<label class="control-label" for="cardsPerDayToShipMax"
						title="${maxcardsperdayship}">${maxcardsperdayship}</label>

					<div class="controls">
						<input id="cardsPerDayToShipMax" name="cardsPerDayToShipMax" class="intInput"
							title="${maxcardsperdayship}" type="text" value="">
					</div>
				</div>
				<div class="control-group">
					<spring:message code="gc.vendor.label.region" var="region" />
					<label class="control-label" for="region" title="${region}">${region}<span
						class="required">*</span></label>

					<div class="controls">
						<input id="region" name="region" title="${region}" type="text"
							value="">
					</div>
				</div>
				<div class="control-group top-zero">
					<spring:message code="gc.vendor.label.defaultvendor"
						var="defaultvendor" />
					<label class="control-label" for="defaultVendor"
						title="${defaultvendor}">${defaultvendor}</label>

					<div class="controls">
						<input id="defaultVendor" name="defaultVendor"
							title="${defaultvendor}" type="checkbox" class="manual">
					</div>
				</div>
			</div>
			<div class="pull-left">
				<div class="control-group">
					<spring:message code="gc.vendor.label.formalname" var="formalname" />
					<label class="control-label" for="formalName" title="${formalname}">${formalname}<span
						class="required">*</span></label>

					<div class="controls">
						<input id="formalName" name="formalName" title="${formalname}"
							type="text" value="">
					</div>
				</div>
				<div class="control-group">
					<spring:message code="gc.vendor.label.shortname" var="shortname" />
					<label class="control-label" for="shortName" title="${shortname}">${shortname}</label>

					<div class="controls">
						<input id="shortName" name="shortName" title="${shortname}"
							type="text" value="">
					</div>
				</div>
				<div class="control-group">
					<spring:message code="gc.vendor.label.registeredaddress1"
						var="registeredaddress1" />
					<label class="control-label" for="registeredAddress1"
						title="${registeredaddress1}">${registeredaddress1}</label>

					<div class="controls">
						<input id="registeredAddress1" name="registeredAddress1"
							title="${registeredaddress1}" type="text" value="">
					</div>
				</div>
				<div class="control-group">
					<spring:message code="gc.vendor.label.mailaddress"
						var="mailaddress" />
					<label class="control-label" for="mailAddress"
						title="${mailaddress}">${mailaddress}<span
						class="required">*</span></label>

					<div class="controls">
						<input id="mailAddress" name="mailAddress" title="${mailaddress}"
							type="text" value="">
					</div>
				</div>
				<div class="control-group">
					<spring:message code="gc.vendor.label.phoneno" var="phoneno" />
					<label class="control-label" for="registeredPhoneNo"
						title="${phoneno}">${phoneno}</label>

					<div class="controls">
						<input id="registeredPhoneNo" name="registeredPhoneNo"
							title="${phoneno}" type="text" value="">
					</div>
				</div>
				<div class="control-group">
					<spring:message code="gc.vendor.label.faxno" var="faxno" />
					<label class="control-label" for="faxNo" title="${faxno}">${faxno}</label>

					<div class="controls">
						<input id="faxNo" name="faxNo" title="${faxno}" type="text"
							value="">
					</div>
				</div>
				<div class="control-group">
					<spring:message code="gc.vendor.label.email" var="email" />
					<label class="control-label" for="emailAddress" title="${email}">${email}<span
						class="required">*</span></label>

					<div class="controls">
						<input id="emailAddress" name="emailAddress" title="${email}"
							type="text" value="">
					</div>
				</div>
				<div class="control-group">
					<spring:message code="gc.vendor.label.contactname"
						var="contactname" />
					<label class="control-label" for="contactPersonName"
						title="${contactname}">${contactname}</label>

					<div class="controls">
						<input id="contactPersonName" name="contactPersonName"
							title="${contactname}" type="text" value="">
					</div>
				</div>
				<div class="control-group">
					<spring:message code="gc.vendor.label.contacttitle"
						var="contacttitle" />
					<label class="control-label" for="contactPersonTitle"
						title="${contacttitle}">${contacttitle}</label>

					<div class="controls">
						<input id="contactPersonTitle" name="contactPersonTitle"
							title="${contacttitle}" type="text" value="">
					</div>
				</div>
				<div class="control-group">
					<spring:message code="gc.vendor.label.contactid" var="contactid" />
					<label class="control-label" for="contactPersonIdentityNo"
						title="${contactid}">${contactid}</label>

					<div class="controls">
						<input id="contactPersonIdentityNo" name="contactPersonIdentityNo"
							title="${contactid}" type="text" value="">
					</div>
				</div>
				<div class="control-group">
					<spring:message code="gc.vendor.label.contactphone"
						var="contactphone" />
					<label class="control-label" for="contactPersonPhoneNo"
						title="${contactphone}">${contactphone}</label>

					<div class="controls">
						<input id="contactPersonPhoneNo" name="contactPersonPhoneNo"
							title="${contactphone}" type="text" value="">
					</div>
				</div>
				<div class="control-group">
					<spring:message code="gc.vendor.label.contactcell"
						var="contactcell" />
					<label class="control-label" for="contactPersonCellNo"
						title="${contactcell}">${contactcell}</label>

					<div class="controls">
						<input id="contactPersonCellNo" name="contactPersonCellNo"
							title="${contactcell}" type="text" value="">
					</div>
				</div>
				<div class="control-group">
					<spring:message code="gc.vendor.label.contactaddress"
						var="contactaddress" />
					<label class="control-label" for="contactPersonAddress"
						title="${contactaddress}">${contactaddress}</label>

					<div class="controls">
						<input id="contactPersonAddress" name="contactPersonAddress"
							title="${contactaddress}" type="text" value="">
					</div>
				</div>
			</div>
		</form>
		<input id="vendorId" type="hidden" value="" />
	</div>
	<div class="modal-footer">
		<button id="save" class="btn btn-primary" aria-hidden="true">
			<spring:message code="save" />
		</button>
		<button id="cancel" class="btn " aria-hidden="true">
			<spring:message code="cancel" />
		</button>
	</div>
</div>
<link href="<spring:url value="/css/dataTables.css" />" rel="stylesheet">
<link href="<spring:url value="/styles/temp.css" />" rel="stylesheet" />
<script src="<spring:url value="/js/jquery/jquery.dataTables.js" />"></script>
<script src="<spring:url value="/js/jquery/jquery.dataTables.linkLengthMenu.js" />"></script>
<script src="<spring:url value="/js/jquery/jquery.dataTables.customPaging.js" />"></script>
<script src="<spring:url value="/js/jquery/ajax-datatable.js" />"></script>
<script src="<spring:url value="/js/common/form2js.js" />"></script>
<script src="<spring:url value="/js/common/json2.js" />"></script>
<script src="<spring:url value="/js/jquery/jquery.toObject.js" />"></script>
<script src="<spring:url value="/js/jquery/jquery.numberInput.js" />" ></script>
<script>
	$(document)
			.ready(
					function() {
						$(".floatInput").numberInput({
				            "type" : "float"
				        });
						$(".intInput").numberInput({
				            "type" : "int"
				        });
						var vendorList = $('#vendor_list_container');
						$('#search').click(
								function() {
									var formDataObject = $('#vendor_form')
											.toObject({
												mode : 'first',
												skipEmpty : false
											});
									var $btnSearch = $(this);
									$btnSearch.prop('disabled', true);
									vendorList.ajaxDataTable('search',
											formDataObject, function() {
												$btnSearch.prop('disabled',
														false);
											});
								});

						var rowIdPrefix = 'vendor-';

						var clickableCell = function(data, type, row) {
							return '<a class="clickable" href="#" onclick="return false;"><div>'
									+ (data == null || data === '' ? '&nbsp;'
											: data) + '</div></a>';
						};
						vendorList
								.ajaxDataTable({
									'autoload' : true,
									'ajaxSource' : '<spring:url value="/giftcard/vendor/list" />',
									'fnRowCallback' : function(nRow, aData,
											iDisplayIndex) {
										nRow.id = rowIdPrefix + aData.id;
										return nRow;
									},
									'columnHeaders' : [
											'<spring:message code="gc.vendor.label.formalname" />',
											'<spring:message code="gc.vendor.label.shortname" />',
											'<spring:message code="gc.vendor.label.registeredaddress1" />',
											'<spring:message code="gc.vendor.label.mailaddress" />',
											'<spring:message code="gc.vendor.label.phoneno" />',
											'<spring:message code="gc.vendor.label.faxno" />',
											'<spring:message code="gc.vendor.label.email" />',
											'<spring:message code="gc.vendor.label.contactname" />',
											'<spring:message code="gc.vendor.label.contacttitle" />',
											'<spring:message code="gc.vendor.label.contactid" />',
											'<spring:message code="gc.vendor.label.contactphone" />',
											'<spring:message code="gc.vendor.label.contactcell" />',
											'<spring:message code="gc.vendor.label.contactaddress" />',
											'<spring:message code="gc.vendor.label.contactemail" />',
											'<spring:message code="gc.vendor.label.paymentmethod" />',
											'<spring:message code="gc.vendor.label.bankaccountno" />',
											'<spring:message code="gc.vendor.label.bankaccountname" />',
											'<spring:message code="gc.vendor.label.daysafteraccountspayment" />',
											'<spring:message code="gc.vendor.label.invoicetitle" />',
											'<spring:message code="gc.vendor.label.enabled" />',
											'<spring:message code="gc.vendor.label.maxcardsperdaymanufacture" />',
											'<spring:message code="gc.vendor.label.maxcardsperdayship" />',
											'<spring:message code="gc.vendor.label.region" />',
											'<spring:message code="gc.vendor.label.defaultvendor" />' ],
									'modelFields' : [ {
										name : 'formalName',
										sortable : true,
										fieldCellRenderer : clickableCell
									}, {
										name : 'shortName',
										sortable : false,
										fieldCellRenderer : clickableCell
									}, {
										name : 'registeredAddress1',
										sortable : false,
										fieldCellRenderer : clickableCell
									}, {
										name : 'mailAddress',
										sortable : false,
										fieldCellRenderer : clickableCell
									}, {
										name : 'registeredPhoneNo',
										sortable : false,
										fieldCellRenderer : clickableCell
									}, {
										name : 'faxNo',
										sortable : false,
										fieldCellRenderer : clickableCell
									}, {
										name : 'emailAddress',
										sortable : false,
										fieldCellRenderer : clickableCell
									}, {
										name : 'contactPersonName',
										sortable : false,
										fieldCellRenderer : clickableCell
									}, {
										name : 'contactPersonTitle',
										sortable : false,
										fieldCellRenderer : clickableCell
									}, {
										name : 'contactPersonIdentityNo',
										sortable : false,
										fieldCellRenderer : clickableCell
									}, {
										name : 'contactPersonPhoneNo',
										sortable : false,
										fieldCellRenderer : clickableCell
									}, {
										name : 'contactPersonCellNo',
										sortable : false,
										fieldCellRenderer : clickableCell
									}, {
										name : 'contactPersonAddress',
										sortable : false,
										fieldCellRenderer : clickableCell
									}, {
										name : 'contactPersonEmail',
										sortable : false,
										fieldCellRenderer : clickableCell
									}, {
										name : 'paymentMethod',
										sortable : false,
										fieldCellRenderer : clickableCell
									}, {
										name : 'bankAccountNo',
										sortable : false,
										fieldCellRenderer : clickableCell
									}, {
										name : 'bankAccountName',
										sortable : false,
										fieldCellRenderer : clickableCell
									}, {
										name : 'daysAfterAccountsPayment',
										sortable : false,
										fieldCellRenderer : clickableCell
									}, {
										name : 'invoiceTitle',
										sortable : false,
										fieldCellRenderer : clickableCell
									}, {
										name : 'enabled',
										sortable : false,
										fieldCellRenderer : clickableCell
									}, {
										name : 'cardsPerDayManufactureMax',
										sortable : false,
										fieldCellRenderer : clickableCell
									}, {
										name : 'cardsPerDayToShipMax',
										sortable : false,
										fieldCellRenderer : clickableCell
									}, {
										name : 'region',
										sortable : false,
										fieldCellRenderer : clickableCell
									}, {
										name : 'defaultVendor',
										sortable : false,
										fieldCellRenderer : clickableCell
									} ]
								});

						var addUpdateVendorForm = $('#addUpdateVendorForm');
						var $dialog = $('#addEditDialog').modal({
							show : false
						}).on('hidden', function() {
							addUpdateVendorForm.formUtil('clearInputs');
							$(".alert", addUpdateVendorForm).alert('close')
						});

						// Update
						$(vendorList)
								.on(
										'click',
										'a.clickable',
										function() {
											var trId = $(this).parents('tr')
													.attr('id');
											var vendorId = trId
													.substring(rowIdPrefix.length);
											$
													.ajax({
														"type" : "GET",
														"url" : '<spring:url value="/giftcard/vendor/"/>'
																+ vendorId,
														"success" : function(
																data) {
															for ( var propertyName in data) {
																var input = $('#'
																		+ propertyName);
																// Read controls whose id is equal to property name of this data object
																// Ignore if no control found
																if (input.length > 0) {
																	var value = data[propertyName];
																	if (input
																			.attr('type') == 'checkbox') {
																		input
																				.prop(
																						'checked',
																						value);
																	} else {
																		input
																				.val(value);
																	}
																}
															}
															$('#vendorId').val(
																	vendorId);
															$dialog
																	.modal('toggle');
														},
														"error" : function(xhr,
																textStatus,
																error) {
															if (textStatus === 'timeout') {
																alert('The server took too long to send the data.');
															} else {
																alert('An error occurred on the server. Please try again.');
															}
														}
													});
										});

						// Add
						$('#add').click(function() {
							$('#vendorId').val('');
							$dialog.modal('toggle');
						});

						$('#cancel').click(function() {
							$dialog.modal('toggle');
						});

						var showErrorMessage = function(message) {
							$('div.errorMessages')
									.html(
											'<div class="alert alert-error">'
													+ '<button type="button" class="close" data-dismiss="alert">&times;</button>'
													+ message + '</div>');
						};

						var showSuccessMessage = function(message) {
							$('#successMessages')
									.html(
											'<div class="alert alert-success">'
													+ '<button type="button" class="close" data-dismiss="alert">&times;</button>'
													+ message + '</div>');
						};

						$('#save')
								.click(
										function() {
											var ajaxUrl = '';
											var selectedVendorId = $(
													'#vendorId').val();
											if (selectedVendorId) {
												ajaxUrl = '<spring:url value="/giftcard/vendor/update/"/>'
														+ selectedVendorId;
											} else {
												// Add
												ajaxUrl = '<spring:url value="/giftcard/vendor/add"/>';
											}
											$
													.ajax({
														"contentType" : 'application/json',
														headers : {
															'Content-Type' : 'application/json'
														},
														"type" : "POST",
														"url" : ajaxUrl,
														"data" : JSON
																.stringify(addUpdateVendorForm
																		.toObject({
																			mode : 'first',
																			skipEmpty : false
																		})),
														"success" : function(
																data) {
															if (data
																	.hasOwnProperty('errorMessages')) {
																var errorInfo = "";
																for (var i = 0; i < data.errorMessages.length; i++) {
																	errorInfo += "<br>"
																			+ (i + 1)
																			+ ". "
																			+ data.errorMessages[i];
																}
																showErrorMessage("Please correct following errors: "
																		+ errorInfo);
															} else {
																showSuccessMessage(data.successMessages);
																$dialog
																		.modal('toggle');
																// Reload the list
																$('#search')
																		.click();
															}
														},
														"error" : function(xhr,
																textStatus,
																error) {
															if (textStatus === 'timeout') {
																showErrorMessage('The server took too long to send the data.');
															} else if (error) {
																showErrorMessage(error);
															} else {
																showErrorMessage('An error occurred on the server. Please try again.');
															}
														}
													});
										});
						$( "#resetBtn" ).click( function(e) {
							  e.preventDefault();
                $(this).closest( "form" ).find(':input').each( function() {
                    switch(this.type) {
                        case 'password':
                        case 'select-one':
                        case 'text':
                        case 'textarea':
                        case 'file':
                        case 'hidden': $(this).val(''); break;
                        case 'select-multiple': true? $(this).empty() : $(this).val(""); break;
                        case 'checkbox':
                        case 'radio': this.checked = false;
                    }
                });
						})
					});
</script>

<style type="text/css">

  #vendor_list_container .table tbody tr td:nth-child(1) a div, #vendor_list_container .table tbody tr td:nth-child(2) a div{
    width: 200px !important;
  }
  
  #vendor_list_container .table tbody tr td:nth-child(3) a div{
    width: 240px !important;
  }
  
  #vendor_list_container .table tbody tr td:nth-child(4) a div{
    width: 200px !important;
  }

</style>