package com.transretail.crm.common.web.dto;

import java.util.UUID;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

/**
 * @author Allan G. Ramirez (agramirez@exist.com)
 * @author aco
 */
public abstract class AbstractMultiInstanceWebForm<T> implements WebForm<T> {

    private final WebFormInfo webFormInfo;

    protected AbstractMultiInstanceWebForm() {
        this.webFormInfo = new WebFormInfo(UUID.randomUUID().toString());
    }

    protected AbstractMultiInstanceWebForm(String webFormIdPrefix) {
        this.webFormInfo = new WebFormInfo(webFormIdPrefix + UUID.randomUUID().toString());
    }

    @Override
    public WebFormInfo getWebFormInfo() {
        return webFormInfo;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this, ToStringStyle.DEFAULT_STYLE)
            .append("webFormInfo", webFormInfo)
            .append("this", this)
            .toString();
    }
}
