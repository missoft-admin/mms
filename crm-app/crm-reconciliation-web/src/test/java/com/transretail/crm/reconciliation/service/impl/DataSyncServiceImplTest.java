package com.transretail.crm.reconciliation.service.impl;

import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.anyList;
import static org.mockito.Mockito.doAnswer;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.reset;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.util.Map;
import java.util.Properties;

import javax.sql.DataSource;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.support.PropertySourcesPlaceholderConfigurer;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.AnnotationConfigContextLoader;

import com.google.common.collect.Maps;
import com.transretail.crm.common.util.IOUtils;
import com.transretail.crm.devtool.dto.TableMetadataBean;
import com.transretail.crm.Constants;
import com.transretail.crm.reconciliation.service.DataSyncService;
import com.transretail.crm.reconciliation.service.DbConnectionProvider;
import com.transretail.crm.reconciliation.service.DiscrepancyToolService;
import com.transretail.crm.reconciliation.service.StoreInfoService;

/**
 * @author Allan G. Ramirez (agramirez@exist.com)
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(loader = AnnotationConfigContextLoader.class)
public class DataSyncServiceImplTest {
    @Configuration
    static class ContextConfiguration {
        @Bean
        public DataSyncService dataSyncService() {
            return new DataSyncServiceImpl();
        }

        @Bean
        public PropertySourcesPlaceholderConfigurer propertySourcesPlaceholderConfigurer() throws Exception {
            PropertySourcesPlaceholderConfigurer configurer = new PropertySourcesPlaceholderConfigurer();
            Properties properties = new Properties();
            properties.put("dbee.jdbc.driverClassName", "org.h2.Driver");
            properties.put("dbee.jdbc.url", "jdbc:h2:mem:dbee");
            properties.put("dbee.jdbc.username", "sa");
            properties.put("dbee.jdbc.password", "");
            properties.put("jdbc.driverClassName", "org.h2.Driver");
            properties.put("ttserver.jdbc.url", "jdbc:h2:mem:ttserver");
            properties.put("ttserver.jdbc.username", "sa");
            properties.put("ttserver.jdbc.password", "");
            configurer.setProperties(properties);
            return configurer;
        }

        @Bean(name = "dataSource")
        public DataSource dataSource() {
            DriverManagerDataSource ds = new DriverManagerDataSource();
            ds.setDriverClassName("org.h2.Driver");
            ds.setUrl("jdbc:h2:mem:db2");
            ds.setUsername("sa");
            return ds;
        }

        @Bean
        public DiscrepancyToolService discrepancyToolService() {
            return mock(DiscrepancyToolService.class);
        }

        @Bean
        public StoreInfoService storeInfoService() {
            return mock(StoreInfoService.class);
        }

        @Bean
        public DbConnectionProvider connectionProvider() {
            return mock(DbConnectionProvider.class);
        }
    }

    @Autowired
    private StoreInfoService storeInfoService;
    @Autowired
    private DiscrepancyToolService discrepancyToolService;
    @Autowired
    private DataSyncServiceImpl dataSyncService;
    @Autowired
    private DbConnectionProvider connectionProvider;

    private Connection dbeeCon;
    private String posIntgRouteConfigTable = "POSINTG_ROUTECONFIG";
    private String storeCode = "20001";

    @Before
    public void onSetup() throws Exception {
        reset(storeInfoService);
        reset(discrepancyToolService);
        reset(connectionProvider);

        dbeeCon = createDbeeCon();
        String createTableScript = "CREATE TABLE " + posIntgRouteConfigTable
            + " (ID INTEGER AUTO_INCREMENT PRIMARY KEY NOT NULL, LAST_EXECUTION_DATE timestamp, JOB_CONFIGURATION VARCHAR(50), STORE_CODE VARCHAR(20));";
        PreparedStatement ps = null;
        try {
            ps = dbeeCon.prepareStatement(createTableScript);
            ps.execute();

            IOUtils.INSTANCE.close(ps);

            ps = dbeeCon.prepareStatement(
                "INSERT INTO " + posIntgRouteConfigTable + "(ID,LAST_EXECUTION_DATE,JOB_CONFIGURATION,STORE_CODE) VALUES (?,?,?,?)");
            ps.setInt(1, 1);
            ps.setTimestamp(2, new Timestamp(System.currentTimeMillis()));
            ps.setString(3, Constants.JOBCONFIG_PULL.toString());
            ps.setString(4, storeCode);
            ps.executeUpdate();

            ps.setInt(1, 2);
            ps.setTimestamp(2, new Timestamp(System.currentTimeMillis()));
            ps.setString(3, Constants.JOBCONFIG_PUSH.toString());
            ps.setString(4, storeCode);
            ps.executeUpdate();
        } finally {
            IOUtils.INSTANCE.close(ps);
        }
    }

    @After
    public void tearDown() throws Exception {
        String dropScript = "DROP TABLE " + posIntgRouteConfigTable + ";";
        Statement ttSt = null;
        try {
            ttSt = dbeeCon.createStatement();
            ttSt.execute(dropScript);
        } finally {
            IOUtils.INSTANCE.close(ttSt);
            IOUtils.INSTANCE.close(dbeeCon, false);
        }
    }

    @Test
    public void syncReferenceRecords() throws Exception {
        when(storeInfoService.getLastRefSyncDate()).thenReturn(null);
        TableMetadataBean bean = new TableMetadataBean();
        bean.setTable("CRM_ADDRESS");
        bean.setId("ID");
        Map<TableMetadataBean, Timestamp> map = Maps.newHashMap();
        long currentTimeInMillis = System.currentTimeMillis();
        map.put(bean, new Timestamp(currentTimeInMillis - (24 * 60 * 60 * 1000))); // yesterday
        when(discrepancyToolService.getMinCreatedDateOfMissingRecordsInLocalDb(anyList())).thenReturn(map);
        when(storeInfoService.getStoreCode()).thenReturn(storeCode);
        when(connectionProvider.createPosIntegDbConnection()).thenReturn(createDbeeCon());
        final Timestamp expectedTs = new Timestamp(currentTimeInMillis);
        final DataSyncServiceImpl spy = spy(dataSyncService);
        doAnswer(new Answer() {
            @Override
            public Object answer(InvocationOnMock invocation) throws Throwable {
                spy.updatePosIntegLastExecutionDate(dbeeCon, expectedTs, storeCode, Constants.JOBCONFIG_PULL.toString());
                return null;
            }
        }).when(spy).waitForPosIntegUpdate();
        Timestamp actualTs = spy.syncReferenceRecords();
        assertEquals(expectedTs, actualTs);
        verify(storeInfoService).updateLastRefSyncDate(expectedTs);
    }

    @Test
    public void syncTxnRecords() throws Exception {
        when(storeInfoService.getLastTxnSyncDate()).thenReturn(null);
        TableMetadataBean empTxnBean = new TableMetadataBean();
        empTxnBean.setTable("CRM_EMPLOYEE_PURCHASE_TXN");
        empTxnBean.setId("ID");

        TableMetadataBean pointsBean = new TableMetadataBean();
        pointsBean.setTable("CRM_POINTS");
        pointsBean.setId("ID");
        Map<TableMetadataBean, Timestamp> map = Maps.newHashMap();


        long currentTimeInMillis = System.currentTimeMillis();
        map.put(empTxnBean, new Timestamp(currentTimeInMillis - (24 * 60 * 60 * 1000))); // yesterday
        map.put(pointsBean, new Timestamp(currentTimeInMillis - (24 * 60 * 60 * 1000) - 100)); // yesterday - 100ms

        when(discrepancyToolService.getMinCreatedDateOfMissingRecordsInLocalDb(anyList())).thenReturn(map);
        when(storeInfoService.getStoreCode()).thenReturn(storeCode);
        when(connectionProvider.createPosIntegDbConnection()).thenReturn(createDbeeCon());

        final Timestamp expectedTs = new Timestamp(currentTimeInMillis);
        final DataSyncServiceImpl spy = spy(dataSyncService);
        doAnswer(new Answer() {
            @Override
            public Object answer(InvocationOnMock invocation) throws Throwable {
                spy.updatePosIntegLastExecutionDate(dbeeCon, expectedTs, storeCode, Constants.JOBCONFIG_PUSH.toString());
                return null;
            }
        }).when(spy).waitForPosIntegUpdate();
        Timestamp actualTs = spy.syncTxnRecords();
        assertEquals(expectedTs, actualTs);
        verify(storeInfoService).updateLastTxnSyncDate(expectedTs);
    }

    private Connection createDbeeCon() throws SQLException {
        return DriverManager.getConnection("jdbc:h2:mem:dbee", "sa", "");
    }
}
