package com.transretail.crm.web.member.bean;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

import java.util.Arrays;

import org.joda.time.DateTime;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.MutablePropertyValues;
import org.springframework.beans.PropertyValue;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.validation.BeanPropertyBindingResult;
import org.springframework.validation.DataBinder;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

/**
 * @author Allan G. Ramirez (agramirez@exist.com)
 */
@WebAppConfiguration
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(
    locations = {"classpath:META-INF/spring/applicationContext-test.xml", "classpath:META-INF/spring/test-app-context.xml"})
public class RegisterDtoTest {
    @Autowired
    private Validator validator;

    @Test
    public void getFullNameTest() {
        String firstName = "Cloud";
        String lastName = "Ramirez";
        String username = "cloudx";
        RegisterDto form = new RegisterDto();
        form.setFirstName(firstName);
        form.setLastName(lastName);
        form.setUsername(username);

        assertEquals(firstName + " " + lastName, form.getFullName());

        form.setFirstName("");
        form.setLastName("");
        assertEquals(username, form.getFullName());
    }

    @Test
    public void validateEmptyFields() {
        Errors result = validate(new RegisterDto());
        assertEquals(8, result.getErrorCount());
        assertNotNull(result.getFieldError("username"));
        assertNotNull(result.getFieldError("password"));
        assertNotNull(result.getFieldError("confirmPassword"));
        assertNotNull(result.getFieldError("contact"));
    }

    @Test
    public void validateEmail() {
        RegisterDto form = new RegisterDto();
        form.setEmail("invalidemail");
        validate(form, "email",
            "must match \"^$|^([0-9a-zA-Z]([-.\\w]*[0-9a-zA-Z])*@([0-9a-zA-Z][-\\w]*[0-9a-zA-Z]\\.)+[a-zA-Z]{2,9})\"");
    }

    @Test
    public void validateUsername() {
        RegisterDto form = new RegisterDto();
        validate(form, "username", "may not be empty");

        form.setUsername("anyname");
        Errors errors = validate(form);
        assertNull(errors.getFieldError("username"));
    }

    @Test
    public void validatePassword() {
        String expectedMessage = "minimum of 8 characters<br/>" +
            "should contain at least 1 uppercase character<br/>" +
            "should contain at least 1 digit";

        RegisterDto form = new RegisterDto();
        validate(form, "password", expectedMessage);

        form.setEmail("invalidemail");
        validate(form, "password", expectedMessage);
    }

    @Test
    @Ignore
    // TODO: Fix this
    public void validateBirthDateFormat() {
        RegisterDto form = new RegisterDto();
        // Invalid format
        DataBinder binder = new DataBinder(form);
        binder.bind(new MutablePropertyValues(Arrays.asList(new PropertyValue("birthDate", "2013-03-13"))));
        Errors result = binder.getBindingResult();
        assertEquals(1, result.getErrorCount());
        // TODO: validate the display message
        assertNotNull(result.getFieldError("birthDate"));

        // Invalid year
        binder = new DataBinder(form);
        binder.bind(new MutablePropertyValues(Arrays.asList(new PropertyValue("birthDate", "2013/13/13"))));
        result = binder.getBindingResult();
        assertEquals(1, result.getErrorCount());
        // TODO: validate the display message
        assertNotNull(result.getFieldError("birthDate"));

        // Valid date format
        binder = new DataBinder(form);
        binder.bind(new MutablePropertyValues(Arrays.asList(new PropertyValue("birthDate", "2013/03/13"))));
        result = binder.getBindingResult();
        assertEquals(0, result.getErrorCount());
        // TODO: validate the display message
        assertNotNull(result.getFieldError("birthDate"));
    }

    @Test
    public void validateTomorrowBirthDate() {
        RegisterDto form = new RegisterDto();
        Errors result = validate(form);
        assertNotNull(result.getFieldError("birthDate"));

        DateTime birthDate = DateTime.now();
        birthDate = birthDate.plusDays(1);
        form.setBirthDate(birthDate);
        result = validate(form);
        assertEquals("must be in the past", result.getFieldError("birthDate").getDefaultMessage());
    }

    @Test
    public void validateContact() {
        RegisterDto form = new RegisterDto();
        validate(form, "contact", "may not be empty");

        form.setContact("anycontact");
        Errors errors = validate(form);
        assertNull(errors.getFieldError("contact"));
    }

    private Errors validate(RegisterDto form) {
        Errors result = new BeanPropertyBindingResult(form, "registrationForm");
        validator.validate(form, result);
        return result;
    }

    private void validate(RegisterDto form, String fieldName, String expectedMessage) {
        Errors result = new BeanPropertyBindingResult(form, "registrationForm");
        validator.validate(form, result);
        assertEquals(expectedMessage, result.getFieldError(fieldName).getDefaultMessage());
    }

}
