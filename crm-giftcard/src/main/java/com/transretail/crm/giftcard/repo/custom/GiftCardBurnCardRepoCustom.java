package com.transretail.crm.giftcard.repo.custom;

public interface GiftCardBurnCardRepoCustom {
	String createBurnCardNumber();
}
