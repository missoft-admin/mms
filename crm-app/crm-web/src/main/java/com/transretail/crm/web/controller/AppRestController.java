package com.transretail.crm.web.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.transretail.crm.common.service.dto.rest.RestControllerResponse;
import com.transretail.crm.core.entity.MemberGroup;
import com.transretail.crm.core.security.service.MD5Service;
import com.transretail.crm.core.service.CodePropertiesService;
import com.transretail.crm.core.service.MemberGroupService;
import com.transretail.crm.web.controller.util.ControllerResponseExceptionHandler;

@RequestMapping("/api")
@Controller
public class AppRestController extends ControllerResponseExceptionHandler {
	private static final Logger LOGGER = LoggerFactory.getLogger(AppRestController.class);

    @Autowired
    private MD5Service mD5Service;
    @Autowired
    private CodePropertiesService codePropertiesService;
    @Autowired
    private MessageSource messageSource;
    @Autowired
    private MemberGroupService memberGroupService;
	
	
	@RequestMapping("/validatemembergroup/{groupId}/{accountNo}/{validationKey}")
    @ResponseBody
    @Transactional
    public RestControllerResponse returnItems(@PathVariable Long groupId, @PathVariable String accountNo, @PathVariable String validationKey) {
		LOGGER.info("[VALIDATE RFS][INIT] groupId : " + groupId + " accountNo: " + accountNo  +  " validationKey : " + validationKey);

		RestControllerResponse response = new RestControllerResponse();
		response.setSuccess(true);
		response.setCallingAction("VALIDATE MEMBER GROUP");

        StringBuffer keySignature = new StringBuffer();
        keySignature.append(groupId);
        keySignature.append(accountNo);

        if (!mD5Service.validateMd5(keySignature.toString(), validationKey)) {
            LOGGER.info("[VALIDATE RFS][INVALID ACCESS] groupId : " + groupId + " accountNo: " + accountNo  +  " validationKey : " + validationKey);
            response.setSuccess(false);
            response.setMessage(messageSource.getMessage("points_rs_error_invalid_client_signature", null, null));
            return response;
        }

        MemberGroup group = memberGroupService.getMemberGroup(groupId);
        response.setResult(memberGroupService.isQualifiedMember(group, accountNo));
        
        LOGGER.info("[VALIDATE RFS][END] groupId : " + groupId + " accountNo: " + accountNo  +  " validationKey : " + validationKey + " response: " + response.getResult());
        
        return response;
    }

	
}
