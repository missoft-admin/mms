package com.transretail.crm.giftcard.dto;

import org.apache.commons.lang3.BooleanUtils;
import org.hibernate.validator.constraints.NotEmpty;

import com.transretail.crm.giftcard.entity.CardVendor;

/**
 *
 */
public class CardVendorDto {
    private Long id;
    @NotEmpty
    private String formalName;
    private String shortName;
    private String registeredAddress1;
    @NotEmpty
    private String mailAddress;
    private String registeredPhoneNo;
    private String faxNo;
    @NotEmpty
    private String emailAddress;
    private String contactPersonName;
    private String contactPersonTitle;
    private String contactPersonIdentityNo;
    private String contactPersonPhoneNo;
    private String contactPersonCellNo;
    private String contactPersonAddress;
    private String contactPersonEmail;
    private String paymentMethod;
    private String bankName;
    private String bankAccountNo;
    private String bankAccountName;
    private Integer daysAfterAccountsPayment;
    private String invoiceTitle;
    private Boolean enabled = Boolean.FALSE;
    private Integer cardsPerDayManufactureMax;
    private Integer cardsPerDayToShipMax;
    @NotEmpty
    private String region;
    private Boolean defaultVendor  = Boolean.FALSE;

    public CardVendorDto() {
    }

    public CardVendorDto(CardVendor vendor) {
        id = vendor.getId();
        formalName = vendor.getFormalName();
        shortName = vendor.getShortName();
        registeredAddress1 = vendor.getRegisteredAddress1();
        mailAddress = vendor.getMailAddress();
        registeredPhoneNo = vendor.getRegisteredPhoneNo();
        faxNo = vendor.getFaxNo();
        emailAddress = vendor.getEmailAddress();
        contactPersonName = vendor.getContactPersonName();
        contactPersonTitle = vendor.getContactPersonTitle();
        contactPersonIdentityNo = vendor.getContactPersonIdentityNo();
        contactPersonPhoneNo = vendor.getContactPersonPhoneNo();
        contactPersonCellNo = vendor.getContactPersonCellNo();
        contactPersonAddress = vendor.getContactPersonAddress();
        contactPersonEmail = vendor.getContactPersonEmail();
        paymentMethod = vendor.getPaymentMethod();
        bankName = vendor.getBankName();
        bankAccountNo = vendor.getBankAccountNo();
        bankAccountName = vendor.getBankAccountName();
        daysAfterAccountsPayment = vendor.getDaysAfterAccountsPayment();
        invoiceTitle = vendor.getInvoiceTitle();
        enabled = vendor.getEnabled();
        cardsPerDayManufactureMax = vendor.getCardsPerDayManufactureMax();
        cardsPerDayToShipMax = vendor.getCardsPerDayToShipMax();
        region = vendor.getRegion();
        defaultVendor = vendor.getDefaultVendor();
    }

    /**
     * Convert this dto to CardVendor
     *
     * @return CardVendor model
     */
    public CardVendor toModel() {
        return copyValues(null);
    }

    /**
     * Update the vendor parameter with the values of the fields in this DTO
     *
     * @param vendor CardVendor to update
     * @return updated CardVendor
     */
    public CardVendor update(CardVendor vendor) {
        return copyValues(vendor);
    }

    private CardVendor copyValues(CardVendor vendor) {
        if (vendor == null) {
            vendor = new CardVendor();
        }
        vendor.setFormalName(formalName);
        vendor.setShortName(shortName);
        vendor.setRegisteredAddress1(registeredAddress1);
        vendor.setMailAddress(mailAddress);
        vendor.setRegisteredPhoneNo(registeredPhoneNo);
        vendor.setFaxNo(faxNo);
        vendor.setEmailAddress(emailAddress);
        vendor.setContactPersonName(contactPersonName);
        vendor.setContactPersonTitle(contactPersonTitle);
        vendor.setContactPersonIdentityNo(contactPersonIdentityNo);
        vendor.setContactPersonPhoneNo(contactPersonPhoneNo);
        vendor.setContactPersonCellNo(contactPersonCellNo);
        vendor.setContactPersonAddress(contactPersonAddress);
        vendor.setContactPersonEmail(contactPersonEmail);
        vendor.setPaymentMethod(paymentMethod);
        vendor.setBankName(bankName);
        vendor.setBankAccountNo(bankAccountNo);
        vendor.setBankAccountName(bankAccountName);
        vendor.setDaysAfterAccountsPayment(daysAfterAccountsPayment);
        vendor.setInvoiceTitle(invoiceTitle);
        vendor.setEnabled(getEnabled());
        vendor.setCardsPerDayManufactureMax(cardsPerDayManufactureMax);
        vendor.setCardsPerDayToShipMax(cardsPerDayToShipMax);
        vendor.setRegion(region);
        vendor.setDefaultVendor(getDefaultVendor());
        return vendor;
    }

    public Long getId() {
        return id;
    }

    public String getFormalName() {
        return formalName;
    }

    public void setFormalName(String formalName) {
        this.formalName = formalName;
    }

    public String getShortName() {
        return shortName;
    }

    public void setShortName(String shortName) {
        this.shortName = shortName;
    }

    public String getRegisteredAddress1() {
        return registeredAddress1;
    }

    public void setRegisteredAddress1(String registeredAddress1) {
        this.registeredAddress1 = registeredAddress1;
    }

    public String getMailAddress() {
        return mailAddress;
    }

    public void setMailAddress(String mailAddress) {
        this.mailAddress = mailAddress;
    }

    public String getRegisteredPhoneNo() {
        return registeredPhoneNo;
    }

    public void setRegisteredPhoneNo(String registeredPhoneNo) {
        this.registeredPhoneNo = registeredPhoneNo;
    }

    public String getFaxNo() {
        return faxNo;
    }

    public void setFaxNo(String faxNo) {
        this.faxNo = faxNo;
    }

    public String getEmailAddress() {
        return emailAddress;
    }

    public void setEmailAddress(String emailAddress) {
        this.emailAddress = emailAddress;
    }

    public String getContactPersonName() {
        return contactPersonName;
    }

    public void setContactPersonName(String contactPersonName) {
        this.contactPersonName = contactPersonName;
    }

    public String getContactPersonTitle() {
        return contactPersonTitle;
    }

    public void setContactPersonTitle(String contactPersonTitle) {
        this.contactPersonTitle = contactPersonTitle;
    }

    public String getContactPersonIdentityNo() {
        return contactPersonIdentityNo;
    }

    public void setContactPersonIdentityNo(String contactPersonIdentityNo) {
        this.contactPersonIdentityNo = contactPersonIdentityNo;
    }

    public String getContactPersonPhoneNo() {
        return contactPersonPhoneNo;
    }

    public void setContactPersonPhoneNo(String contactPersonPhoneNo) {
        this.contactPersonPhoneNo = contactPersonPhoneNo;
    }

    public String getContactPersonCellNo() {
        return contactPersonCellNo;
    }

    public void setContactPersonCellNo(String contactPersonCellNo) {
        this.contactPersonCellNo = contactPersonCellNo;
    }

    public String getContactPersonAddress() {
        return contactPersonAddress;
    }

    public void setContactPersonAddress(String contactPersonAddress) {
        this.contactPersonAddress = contactPersonAddress;
    }

    public String getContactPersonEmail() {
        return contactPersonEmail;
    }

    public void setContactPersonEmail(String contactPersonEmail) {
        this.contactPersonEmail = contactPersonEmail;
    }

    public String getPaymentMethod() {
        return paymentMethod;
    }

    public void setPaymentMethod(String paymentMethod) {
        this.paymentMethod = paymentMethod;
    }

    public String getBankName() {
        return bankName;
    }

    public void setBankName(String bankName) {
        this.bankName = bankName;
    }

    public String getBankAccountNo() {
        return bankAccountNo;
    }

    public void setBankAccountNo(String bankAccountNo) {
        this.bankAccountNo = bankAccountNo;
    }

    public String getBankAccountName() {
        return bankAccountName;
    }

    public void setBankAccountName(String bankAccountName) {
        this.bankAccountName = bankAccountName;
    }

    public Integer getDaysAfterAccountsPayment() {
        return daysAfterAccountsPayment;
    }

    public void setDaysAfterAccountsPayment(Integer daysAfterAccountsPayment) {
        this.daysAfterAccountsPayment = daysAfterAccountsPayment;
    }

    public String getInvoiceTitle() {
        return invoiceTitle;
    }

    public void setInvoiceTitle(String invoiceTitle) {
        this.invoiceTitle = invoiceTitle;
    }

    public Boolean getEnabled() {
        return enabled;
    }

    public void setEnabled(Boolean enabled) {
        this.enabled = BooleanUtils.toBoolean(enabled);
    }

    public Integer getCardsPerDayManufactureMax() {
        return cardsPerDayManufactureMax;
    }

    public void setCardsPerDayManufactureMax(Integer cardsPerDayManufactureMax) {
        this.cardsPerDayManufactureMax = cardsPerDayManufactureMax;
    }

    public Integer getCardsPerDayToShipMax() {
        return cardsPerDayToShipMax;
    }

    public void setCardsPerDayToShipMax(Integer cardsPerDayToShipMax) {
        this.cardsPerDayToShipMax = cardsPerDayToShipMax;
    }

    public String getRegion() {
        return region;
    }

    public void setRegion(String region) {
        this.region = region;
    }

    public Boolean getDefaultVendor() {
        return defaultVendor;
    }

    public void setDefaultVendor(Boolean defaultVendor) {
        this.defaultVendor = BooleanUtils.toBoolean(defaultVendor);
    }

}
