package com.transretail.crm.core.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.mysema.query.types.expr.BooleanExpression;
import com.transretail.crm.common.service.dto.request.PageSortDto;
import com.transretail.crm.common.service.dto.response.ResultList;
import com.transretail.crm.common.service.util.SpringDataPagingUtil;
import com.transretail.crm.core.dto.MemberDto;
import com.transretail.crm.core.dto.MemberResultList;
import com.transretail.crm.core.entity.MemberModel;
import com.transretail.crm.core.entity.RewardSchemeModel;
import com.transretail.crm.core.repo.RewardSchemeRepo;
import com.transretail.crm.core.service.PointsSchemeService;
import com.transretail.crm.logging.annotation.Loggable;

@Service("pointsSchemeService")
@Transactional
public class PointsSchemeServiceImpl implements PointsSchemeService {


    @Autowired
    RewardSchemeRepo rewardSchemeRepo;
    
    @Override
    public ResultList<RewardSchemeModel> listSchemeDto(PageSortDto dto) {
    	Pageable pageable = SpringDataPagingUtil.INSTANCE.createPageable(dto.getPagination());
        Page<RewardSchemeModel> page=  rewardSchemeRepo.findAll(pageable);
        return new ResultList<RewardSchemeModel>(page);
    }

    @Override
    public RewardSchemeModel saveAndUpdate(RewardSchemeModel pointsSchemeModel) {

        return rewardSchemeRepo.save(pointsSchemeModel);
    }

    @Override
    public List<RewardSchemeModel> findAllPointsSchemes() {
        return rewardSchemeRepo.findAll();


    }

    @Override
    public List<RewardSchemeModel> findAllActivePointsSchemes() {
        return rewardSchemeRepo.findAllActivePointsSchemes();
    }

    @Override
    public void deleteAllPointsSchemes() {
        rewardSchemeRepo.deleteAll();
    }

	@Override
	public List<RewardSchemeModel> findPointsSchemeEntries(int firstResult,
			int maxResult) {
		return rewardSchemeRepo.findAll(new PageRequest(firstResult / maxResult, maxResult)).getContent();
	}

	@Override
	public long countAllPointsSchemes() {
		return rewardSchemeRepo.count();
	}

	@Loggable
	@Override
	public void savePointsScheme(RewardSchemeModel pointsScheme) {
		rewardSchemeRepo.save(pointsScheme);
	}

	@Override
	public Double findLowestActivePointsSchemeValue(){
		return rewardSchemeRepo.findLowestActivePointsSchemeValue();
	}
	
	@Override
	public void deletePointsScheme(Long id) {
		rewardSchemeRepo.delete(id);
	}
	
	@Override
	public RewardSchemeModel findPointsScheme(Long id) {
		if(id == null) return null;
		return rewardSchemeRepo.findOne(id);
	}
	
	@Override
	public boolean hasOverlappingRange(Long id, Double from, Double to) {
		return rewardSchemeRepo.checkRangeOverlap(id, from, to);
	}
	
	@Override
	public boolean hasOverlappingValue(Long id, Double value) {
		return rewardSchemeRepo.checkValueOverlap(id, value);
	}
}
