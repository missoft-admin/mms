package com.transretail.crm.core.dto;

import com.transretail.crm.core.entity.CrmInStore;
import com.transretail.crm.core.entity.enums.Status;

/**
 * @author Allan G. Ramirez (agramirez@exist.com)
 */
public class CrmInStoreDto extends CrmInStore {
    public Status status;
    public String version;
    public String shaId;
    public String lastUpdate;

    public CrmInStoreDto() {

    }

    public CrmInStoreDto(CrmInStore info) {
        setId(info.getId());
        setName(info.getName());
        setIp(info.getIp());
        setTomcatPort(info.getTomcatPort());
        setCrmProxyRestPrefix(info.getCrmProxyRestPrefix());
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public String getShaId() {
        return shaId;
    }

    public void setShaId(String shaId) {
        this.shaId = shaId;
    }

    public String getLastUpdate() {
        return lastUpdate;
    }

    public void setLastUpdate(String lastUpdate) {
        this.lastUpdate = lastUpdate;
    }
}
