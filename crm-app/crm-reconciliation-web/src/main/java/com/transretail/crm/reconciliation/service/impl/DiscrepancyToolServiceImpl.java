package com.transretail.crm.reconciliation.service.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.sql.DataSource;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.google.common.collect.Maps;
import com.transretail.crm.Constants;
import com.transretail.crm.common.service.exception.MessageSourceResolvableException;
import com.transretail.crm.common.util.IOUtils;
import com.transretail.crm.devtool.dto.ColumnMetadataBean;
import com.transretail.crm.devtool.dto.TableMetadataBean;
import com.transretail.crm.reconciliation.service.DbConnectionProvider;
import com.transretail.crm.reconciliation.service.DiscrepancyToolService;

/**
 * @author Allan G. Ramirez (agramirez@exist.com)
 */
@Service
public class DiscrepancyToolServiceImpl implements DiscrepancyToolService {
    private static final Logger _LOG = LoggerFactory.getLogger(DiscrepancyToolServiceImpl.class);
    @Autowired
    private DataSource localDataSource;
    @Autowired
    private DbConnectionProvider connectionProvider;

    /**
     * {@inheritDoc}
     */
    @Override
    public Map<String, List<ColumnMetadataBean>> getLocalDbMissingRecords(List<TableMetadataBean> beans) throws
        MessageSourceResolvableException {
        Connection ttCon = null;
        Connection localCon = null;
        try {
            ttCon = connectionProvider.createMainDbDbConnection();
            localCon = localDataSource.getConnection();
            return checkMissingRecords(ttCon, localCon, beans);
        } catch (SQLException e) {
            _LOG.error(Constants.JDBC_ERROR_DEFAULT_MESSAGE.toString(), e);
            throw new MessageSourceResolvableException(Constants.JDBC_ERROR_CODE.toString(), null,
                Constants.JDBC_ERROR_DEFAULT_MESSAGE.toString());
        } finally {
            IOUtils.INSTANCE.close(ttCon, true);
            IOUtils.INSTANCE.close(localCon, true);
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Map<String, List<ColumnMetadataBean>> getMainDbMissingRecords(List<TableMetadataBean> beans) throws
        MessageSourceResolvableException {
        Connection ttCon = null;
        Connection localCon = null;
        try {
            ttCon = connectionProvider.createMainDbDbConnection();
            localCon = localDataSource.getConnection();
            return checkMissingRecords(localCon, ttCon, beans);
        } catch (SQLException e) {
            _LOG.error(Constants.JDBC_ERROR_DEFAULT_MESSAGE.toString(), e);
            throw new MessageSourceResolvableException(Constants.JDBC_ERROR_CODE.toString(), null,
                Constants.JDBC_ERROR_DEFAULT_MESSAGE.toString());
        } finally {
            IOUtils.INSTANCE.close(ttCon, true);
            IOUtils.INSTANCE.close(localCon, true);
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Map<TableMetadataBean, Timestamp> getMinCreatedDateOfMissingRecordsInLocalDb(List<TableMetadataBean> beans) throws
        MessageSourceResolvableException {
        Connection ttCon = null;
        Connection localCon = null;
        try {
            ttCon = connectionProvider.createMainDbDbConnection();
            localCon = localDataSource.getConnection();
            return checkMinCreatedDateOfMissingRecords(ttCon, localCon, beans);
        } catch (SQLException e) {
            _LOG.error(Constants.JDBC_ERROR_DEFAULT_MESSAGE.toString(), e);
            throw new MessageSourceResolvableException(Constants.JDBC_ERROR_CODE.toString(), null,
                Constants.JDBC_ERROR_DEFAULT_MESSAGE.toString());
        } finally {
            IOUtils.INSTANCE.close(ttCon, true);
            IOUtils.INSTANCE.close(localCon, true);
        }
    }

    private Map<String, List<ColumnMetadataBean>> checkMissingRecords(Connection sourceCon, Connection targetConToCheck,
        List<TableMetadataBean> beans) throws SQLException {
        Map<String, List<ColumnMetadataBean>> result = Maps.newHashMap();
        sourceCon.setAutoCommit(false);
        // com.ibm.ws.rsadapter.jdbc.WSJdbcConnection does not support readonly
        if (!sourceCon.getClass().getName().contains("WSJdbcConnection")) {
            sourceCon.setReadOnly(true);
        }
        targetConToCheck.setAutoCommit(false);
        // com.ibm.ws.rsadapter.jdbc.WSJdbcConnection does not support readonly
        if (!targetConToCheck.getClass().getName().contains("WSJdbcConnection")) {
            targetConToCheck.setReadOnly(true);
        }

        for (TableMetadataBean bean : beans) {
            result.put(bean.getTable(), new ArrayList<ColumnMetadataBean>());
            Statement st = null;
            ResultSet rs = null;
            try {
                StringBuilder builder = createSelectQuery(bean);
                builder.append(" ORDER BY ");
                builder.append(Constants.COLUMN_CREATED_DATETIME);
                builder.append(" ASC");

                st = sourceCon.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_READ_ONLY);
                rs = st.executeQuery(builder.toString());

                String selectWhereIdQuery = createSelectWhereIdQuery(bean);
                while (rs.next()) {
                    Object id = rs.getObject(1);
                    if (!targetDbHasId(targetConToCheck, selectWhereIdQuery, id)) {
                        List<ColumnMetadataBean> list = result.get(bean.getTable());
                        Timestamp createdDateTime = (Timestamp) rs.getObject(2);
                        list.add(new ColumnMetadataBean(id.toString(), createdDateTime.toString()));
                    }
                }
            } finally {
                IOUtils.INSTANCE.close(rs);
                IOUtils.INSTANCE.close(st);
            }
        }
        return result;
    }

    private Map<TableMetadataBean, Timestamp> checkMinCreatedDateOfMissingRecords(Connection sourceCon,
        Connection targetConToCheck,
        List<TableMetadataBean> beans) throws SQLException {
        Map<TableMetadataBean, Timestamp> result = Maps.newHashMap();
        sourceCon.setAutoCommit(false);
        // com.ibm.ws.rsadapter.jdbc.WSJdbcConnection does not support readonly
        if (!sourceCon.getClass().getName().contains("WSJdbcConnection")) {
            sourceCon.setReadOnly(true);
        }
        targetConToCheck.setAutoCommit(false);
        // com.ibm.ws.rsadapter.jdbc.WSJdbcConnection does not support readonly
        if (!targetConToCheck.getClass().getName().contains("WSJdbcConnection")) {
            targetConToCheck.setReadOnly(true);
        }

        for (TableMetadataBean bean : beans) {
            PreparedStatement ps = null;
            ResultSet rs = null;
            try {
                StringBuilder builder = createSelectQuery(bean);
                if (StringUtils.isNotBlank(bean.getCreatedDateTime())) {
                    builder.append(" WHERE ");
                    builder.append(Constants.COLUMN_CREATED_DATETIME);
                    builder.append(" > ? ");
                }
                builder.append(" ORDER BY ");
                builder.append(Constants.COLUMN_CREATED_DATETIME);
                builder.append(" ASC");

                ps = sourceCon.prepareStatement(builder.toString(), ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_READ_ONLY);
                if (StringUtils.isNotBlank(bean.getCreatedDateTime())) {
                    ps.setTimestamp(1, Timestamp.valueOf(bean.getCreatedDateTime()));
                }

                rs = ps.executeQuery();

                String selectWhereIdQuery = createSelectWhereIdQuery(bean);
                while (rs.next()) {
                    Object id = rs.getObject(1);
                    if (!targetDbHasId(targetConToCheck, selectWhereIdQuery, id)) {
                        Timestamp createdDateTime = (Timestamp) rs.getObject(2);
                        result.put(bean, createdDateTime);
                        break;
                    }
                }
            } finally {
                IOUtils.INSTANCE.close(rs);
                IOUtils.INSTANCE.close(ps);
            }
        }
        return result;
    }

    private boolean targetDbHasId(Connection localCon, String selectWhereIdQuery, Object id) throws SQLException {
        PreparedStatement locPs = null;
        ResultSet localRs = null;
        try {
            locPs = localCon.prepareStatement(selectWhereIdQuery);
            locPs.setObject(1, id);
            localRs = locPs.executeQuery();
            return localRs.next();
        } finally {
            IOUtils.INSTANCE.close(locPs);
            IOUtils.INSTANCE.close(localRs);
        }
    }

    private StringBuilder createSelectQuery(TableMetadataBean bean) {
        StringBuilder builder = new StringBuilder("SELECT ");
        builder.append(bean.getId());
        builder.append(", ");
        builder.append(Constants.COLUMN_CREATED_DATETIME);
        builder.append(" FROM ");
        builder.append(bean.getTable());
        return builder;
    }

    private String createSelectWhereIdQuery(TableMetadataBean bean) {
        StringBuilder builder = createSelectQuery(bean);
        builder.append(" WHERE ");
        builder.append(bean.getId());
        builder.append(" = ?");
        return builder.toString();
    }

}
