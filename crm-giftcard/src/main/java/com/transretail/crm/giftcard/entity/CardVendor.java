package com.transretail.crm.giftcard.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import org.apache.commons.lang3.BooleanUtils;
import org.hibernate.annotations.Type;
import org.hibernate.validator.constraints.NotEmpty;

import com.transretail.crm.core.entity.support.CustomAuditableEntity;

/**
 *
 */
@Entity
@Table(name = "CRM_GC_VENDOR")
public class CardVendor extends CustomAuditableEntity<Long> implements Serializable {
    private static final long serialVersionUID = 1499360281863050056L;

    @NotEmpty
    @Column(name = "FORMAL_NAME", length = 50, nullable = false)
    private String formalName;
    @Column(name = "SHORT_NAME", length = 25)
    private String shortName;
    @Column(name = "REG_ADD1", length = 120)
    private String registeredAddress1;
    @NotEmpty
    @Column(name = "MAIL_ADD", length = 120, nullable = false)
    private String mailAddress;
    @Column(name = "REG_PHONENO", length = 15)
    private String registeredPhoneNo;
    @Column(name = "FAX", length = 15)
    private String faxNo;
    @NotEmpty
    @Column(name = "EMAIL_ADD", length = 100, nullable = false)
    private String emailAddress;
    @Column(name = "CNTACT_NAME", length = 20)
    private String contactPersonName;
    @Column(name = "CNTACT_TITLE", length = 15)
    private String contactPersonTitle;
    @Column(name = "CNTACT_ID", length = 15)
    private String contactPersonIdentityNo;
    @Column(name = "CNTACT_PHNENO", length = 15)
    private String contactPersonPhoneNo;
    @Column(name = "CNTACT_CELLNO", length = 15)
    private String contactPersonCellNo;
    @Column(name = "CNTACT_ADD", length = 100)
    private String contactPersonAddress;
    @Column(name = "CNTACT_EMAIL", length = 100)
    private String contactPersonEmail;
    @Column(name = "PAYMNT_METHOD", length = 15)
    private String paymentMethod;
    @Column(name = "BANK_NAME", length = 30)
    private String bankName;
    @Column(name = "BANK_ACCT_NO", length = 20)
    private String bankAccountNo;
    @Column(name = "BANK_ACCT_NAME", length = 50)
    private String bankAccountName;
    @Column(name = "DAYS_ACCT_PAYMNT")
    private Integer daysAfterAccountsPayment;
    @Column(name = "INVOICE_TITLE", length = 50)
    private String invoiceTitle;
    @NotNull
    @Column(name = "ENABLED", nullable = false, length = 1)
    @Type(type = "yes_no")
    private Boolean enabled = Boolean.FALSE;
    @Column(name = "DAYL_MANFTR_MAX")
    private Integer cardsPerDayManufactureMax;
    @Column(name = "DAYL_SHIP_MAX")
    private Integer cardsPerDayToShipMax;
    @NotEmpty
    @Column(name = "REGION", length = 50, nullable = false)
    private String region;
    @NotNull
    @Column(name = "DEFAULT_VENDOR", nullable = false, length = 1)
    @Type(type = "yes_no")
    private Boolean defaultVendor = Boolean.FALSE;

    public CardVendor() {

    }

    public CardVendor(Long id) {
        setId(id);
    }

    public String getFormalName() {
        return formalName;
    }

    public void setFormalName(String formalName) {
        this.formalName = formalName;
    }

    public String getShortName() {
        return shortName;
    }

    public void setShortName(String shortName) {
        this.shortName = shortName;
    }

    public String getRegisteredAddress1() {
        return registeredAddress1;
    }

    public void setRegisteredAddress1(String registeredAddress1) {
        this.registeredAddress1 = registeredAddress1;
    }

    public String getMailAddress() {
        return mailAddress;
    }

    public void setMailAddress(String mailAddress) {
        this.mailAddress = mailAddress;
    }

    public String getRegisteredPhoneNo() {
        return registeredPhoneNo;
    }

    public void setRegisteredPhoneNo(String registeredPhoneNo) {
        this.registeredPhoneNo = registeredPhoneNo;
    }

    public String getFaxNo() {
        return faxNo;
    }

    public void setFaxNo(String faxNo) {
        this.faxNo = faxNo;
    }

    public String getEmailAddress() {
        return emailAddress;
    }

    public void setEmailAddress(String emailAddress) {
        this.emailAddress = emailAddress;
    }

    public String getContactPersonName() {
        return contactPersonName;
    }

    public void setContactPersonName(String contactPersonName) {
        this.contactPersonName = contactPersonName;
    }

    public String getContactPersonTitle() {
        return contactPersonTitle;
    }

    public void setContactPersonTitle(String contactPersonTitle) {
        this.contactPersonTitle = contactPersonTitle;
    }

    public String getContactPersonIdentityNo() {
        return contactPersonIdentityNo;
    }

    public void setContactPersonIdentityNo(String contactPersonIdentityNo) {
        this.contactPersonIdentityNo = contactPersonIdentityNo;
    }

    public String getContactPersonPhoneNo() {
        return contactPersonPhoneNo;
    }

    public void setContactPersonPhoneNo(String contactPersonPhoneNo) {
        this.contactPersonPhoneNo = contactPersonPhoneNo;
    }

    public String getContactPersonCellNo() {
        return contactPersonCellNo;
    }

    public void setContactPersonCellNo(String contactPersonCellNo) {
        this.contactPersonCellNo = contactPersonCellNo;
    }

    public String getContactPersonAddress() {
        return contactPersonAddress;
    }

    public void setContactPersonAddress(String contactPersonAddress) {
        this.contactPersonAddress = contactPersonAddress;
    }

    public String getContactPersonEmail() {
        return contactPersonEmail;
    }

    public void setContactPersonEmail(String contactPersonEmail) {
        this.contactPersonEmail = contactPersonEmail;
    }

    public String getPaymentMethod() {
        return paymentMethod;
    }

    public void setPaymentMethod(String paymentMethod) {
        this.paymentMethod = paymentMethod;
    }

    public String getBankName() {
        return bankName;
    }

    public void setBankName(String bankName) {
        this.bankName = bankName;
    }

    public String getBankAccountNo() {
        return bankAccountNo;
    }

    public void setBankAccountNo(String bankAccountNo) {
        this.bankAccountNo = bankAccountNo;
    }

    public String getBankAccountName() {
        return bankAccountName;
    }

    public void setBankAccountName(String bankAccountName) {
        this.bankAccountName = bankAccountName;
    }

    public Integer getDaysAfterAccountsPayment() {
        return daysAfterAccountsPayment;
    }

    public void setDaysAfterAccountsPayment(Integer daysAfterAccountsPayment) {
        this.daysAfterAccountsPayment = daysAfterAccountsPayment;
    }

    public String getInvoiceTitle() {
        return invoiceTitle;
    }

    public void setInvoiceTitle(String invoiceTitle) {
        this.invoiceTitle = invoiceTitle;
    }

    public Boolean getEnabled() {
        return enabled;
    }

    public void setEnabled(Boolean enabled) {
        this.enabled = BooleanUtils.toBoolean(enabled);
    }

    public Integer getCardsPerDayManufactureMax() {
        return cardsPerDayManufactureMax;
    }

    public void setCardsPerDayManufactureMax(Integer cardsPerDayManufactureMax) {
        this.cardsPerDayManufactureMax = cardsPerDayManufactureMax;
    }

    public Integer getCardsPerDayToShipMax() {
        return cardsPerDayToShipMax;
    }

    public void setCardsPerDayToShipMax(Integer cardsPerDayToShipMax) {
        this.cardsPerDayToShipMax = cardsPerDayToShipMax;
    }

    public String getRegion() {
        return region;
    }

    public void setRegion(String region) {
        this.region = region;
    }

    public Boolean getDefaultVendor() {
        return defaultVendor;
    }

    public void setDefaultVendor(Boolean defaultVendor) {
        this.defaultVendor = BooleanUtils.toBoolean(defaultVendor);
    }

    public void updateFields(CardVendor dto) {

    }
}
