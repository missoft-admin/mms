package com.transretail.crm.report.template.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;

import org.apache.commons.lang3.StringUtils;
import org.joda.time.DateTime;
import org.joda.time.LocalDate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Service;

import com.google.common.collect.Lists;
import com.mysema.query.BooleanBuilder;
import com.mysema.query.jpa.impl.JPAQuery;
import com.mysema.query.types.Projections;
import com.transretail.crm.common.reporting.jasper.ClasspathFileResolver;
import com.transretail.crm.common.reporting.jasper.DefaultJRProcessor;
import com.transretail.crm.common.reporting.jasper.JRProcessor;
import com.transretail.crm.common.util.DateUtil;
import com.transretail.crm.core.entity.QMemberModel;
import com.transretail.crm.core.entity.QPosTransaction;
import com.transretail.crm.core.entity.QPosTxItem;
import com.transretail.crm.core.entity.enums.ReportType;
import com.transretail.crm.core.entity.lookup.LookupDetail;
import com.transretail.crm.core.entity.lookup.QLookupDetail;
import com.transretail.crm.core.entity.lookup.QProduct;
import com.transretail.crm.core.entity.lookup.QStore;
import com.transretail.crm.core.entity.lookup.Store;
import com.transretail.crm.core.security.model.impl.CustomSecurityUserDetailsImpl;
import com.transretail.crm.core.security.util.UserUtil;
import com.transretail.crm.core.service.CodePropertiesService;
import com.transretail.crm.core.service.LookupService;
import com.transretail.crm.core.service.StoreService;
import com.transretail.crm.report.template.*;

/**
 * @author ftopico
 */
@Service("topProductPerCustomerReport")
public class TopProductPerCustomerReport implements ValidatableReportTemplate, NoDataFoundSupport {
    
    @PersistenceContext
    private EntityManager em;
    @Autowired
    private LookupService lookupService;
    @Autowired
    private CodePropertiesService codePropertiesService;
    @Autowired
    private StoreService storeService;
    @Autowired
    private MessageSource messageSource;

    private static final String PERMISSION_CODE = "REPORT_TOP_PRODUCT_PER_CUSTOMER";
    protected static final String TEMPLATE_NAME = "Top Product Per Customer Report";
    protected static final String REPORT_NAME = "reports/top_product_per_customer_report.jasper";
    //Header Params
    private static final String PARAM_PRINT_DATE = "printedDate";
    private static final String PARAM_PRINTED_BY = "printedBy";
    private static final String PARAM_DATE_FROM = "startDate";
    private static final String PARAM_DATE_TO = "endDate";
    
    public TopProductPerCustomerReport() {
    }
    
    @Override
    public String getName() {
        return TEMPLATE_NAME;
    }

    @Override
    public String getDescription() {
        return messageSource.getMessage("report.top.product.per.customer.title", null, LocaleContextHolder.getLocale());
    }

    @Override
    public Set<FilterField> getFilters() {
        Set<FilterField> filters = new LinkedHashSet<FilterField>();
        filters.add(FilterField.createDateField(CommonReportFilter.DATE_FROM,
                messageSource.getMessage("report.misc.datefrom", null, LocaleContextHolder.getLocale()),
                DateUtil.SEARCH_DATE_FORMAT, FilterField.DateTypeOption.PLAIN, true));
        filters.add(FilterField.createDateField(CommonReportFilter.DATE_TO,
                messageSource.getMessage("report.misc.dateto", null, LocaleContextHolder.getLocale()),
                DateUtil.SEARCH_DATE_FORMAT, FilterField.DateTypeOption.PLAIN, true));
        
        //Stores
        Map<String, String> stores = new LinkedHashMap<String, String>();
        stores.put("", "");
        stores.put(codePropertiesService.getDetailInvLocationHeadOffice(), lookupService.getDetailByCode(codePropertiesService.getDetailInvLocationHeadOffice()).getDescription());
        for (Store store : storeService.getAllStores()) {
            stores.put(store.getCode(), store.getCode() + " - " + store.getName());
        }
        filters.add(FilterField.createDropdownField(CommonReportFilter.STORE, 
                messageSource.getMessage("report.store", null, LocaleContextHolder.getLocale()), stores, true));
        
        //Rank
        Map<String, String> rankings = new LinkedHashMap<String, String>();
        for (int i=1; i<=10;i++)
            rankings.put(String.valueOf(i), String.valueOf(i));
        filters.add(FilterField.createDropdownField(CommonReportFilter.RANK, 
                messageSource.getMessage("report.number.of.rankings", null, LocaleContextHolder.getLocale()), rankings, true));
        
        //MemberType
        Map<String, String> memberTypes = new LinkedHashMap<String, String>();
        for (LookupDetail memberType : lookupService.getDetailsByHeaderCode(codePropertiesService.getHeaderMemberType()))
            memberTypes.put(memberType.getCode(), memberType.getDescription());
        filters.add(FilterField.createDropdownField(CommonReportFilter.MEMBER_TYPE, 
                messageSource.getMessage("report.member.type", null, LocaleContextHolder.getLocale()), memberTypes));
        
        return filters;
    }

    @Override
    public boolean canView(Set<String> permissions) {
        return permissions.contains(PERMISSION_CODE);
    }

    @Override
    public ReportType[] getReportTypes() {
        return ReportType.values();
    }

    @Override
    public JRProcessor createJrProcessor(Map<String, String> map) {
        Map<String, Object> parameters = new HashMap<String, Object>();
        parameters.put(PARAM_PRINT_DATE, ReportDateFormat.DATE_FORMATTER.print(new LocalDate()));
        parameters.put(PARAM_PRINTED_BY, getCurrentUser());
        parameters.put(PARAM_DATE_FROM, toDateTime(map.get(CommonReportFilter.DATE_FROM)));
        parameters.put(PARAM_DATE_TO, toDateTime(map.get(CommonReportFilter.DATE_TO)));
        parameters.put(CommonReportFilter.RANK, map.get(CommonReportFilter.RANK));
        
        //Store
        if (!map.get(CommonReportFilter.STORE).equalsIgnoreCase("")) {
            String store = null;
            if (map.get(CommonReportFilter.STORE).equalsIgnoreCase(codePropertiesService.getDetailInvLocationHeadOffice())) {
                LookupDetail detail = lookupService.getDetailByCode(codePropertiesService.getDetailInvLocationHeadOffice());
                store = detail.getDescription();
            } else {
                Store modelStore = storeService.getStoreByCode(map.get(CommonReportFilter.STORE));
                if (modelStore != null)
                    store = modelStore.getName();
            }
            parameters.put(CommonReportFilter.STORE, store);
        }
        
        //MemberType
        if (map.get(CommonReportFilter.MEMBER_TYPE) != null && !map.get(CommonReportFilter.MEMBER_TYPE).equalsIgnoreCase("")) {
            LookupDetail memberType = lookupService.getDetailByCode(map.get(CommonReportFilter.MEMBER_TYPE));
            parameters.put(CommonReportFilter.MEMBER_TYPE, memberType.getCode() + " " + memberType.getDescription());
        }
        
        DefaultJRProcessor jrProcessor = new DefaultJRProcessor(REPORT_NAME);
        jrProcessor.addParameter("SUB_DATASOURCE", new JRBeanCollectionDataSource(getTopProductPerCustomerList(map)));
        jrProcessor.addParameters(parameters);
        jrProcessor.setFileResolver(new ClasspathFileResolver("reports"));
        
        return jrProcessor;
    }
    
    private LocalDate toDateTime(String dateString) {
        if (StringUtils.isNotBlank(dateString)) {
            return ReportDateFormat.parseDate(dateString);
        }
        return null;
    }
    
    public List<TopProductReportModel> getTopProductPerCustomerList(Map<String, String> parameters) {
        QPosTransaction qpt = QPosTransaction.posTransaction;
        QPosTxItem qpti = QPosTxItem.posTxItem;
        QMemberModel qm = QMemberModel.memberModel;
        QProduct qp = QProduct.product;
        QStore qs = QStore.store;
        QLookupDetail ql = QLookupDetail.lookupDetail;
        Long rankings = Long.valueOf(parameters.get(CommonReportFilter.RANK));
        
        BooleanBuilder exp = new BooleanBuilder(qpt.transactionDate.between(ReportDateFormat.DATE_FORMATTER.parseLocalDateTime(parameters.get(CommonReportFilter.DATE_FROM)), 
                DateUtil.getEndOfDay(ReportDateFormat.DATE_FORMATTER.parseLocalDateTime(parameters.get(CommonReportFilter.DATE_TO)))));
        
        exp.and(qs.code.eq(parameters.get(CommonReportFilter.STORE)));
        exp.and(qpt.customerId.eq(qm.accountId));
        exp.and(qpti.productId.eq(qp.id));
        
        if (parameters.get(CommonReportFilter.MEMBER_TYPE) != null && !parameters.get(CommonReportFilter.MEMBER_TYPE).isEmpty())
            exp.and(qm.memberType.code.eq(parameters.get(CommonReportFilter.MEMBER_TYPE)));
        
        List<String> distinctCustomers = new JPAQuery(em).distinct().from(qpti, qm, qp)
                .innerJoin(qpti.posTransaction, qpt)
                .innerJoin(qpt.store, qs)
                .where(exp.getValue()).list(qpt.customerId);
        
        
        List<TopProductReportModel> topProductsPerCustomer = new ArrayList<TopProductPerCustomerReport.TopProductReportModel>();
        for (String customerId : distinctCustomers) {
            BooleanBuilder customerPredicate = new BooleanBuilder(exp.getValue());
            JPAQuery query = new JPAQuery(em);
            
            query = query.from(qpti, qm, qp)
                    .innerJoin(qpti.posTransaction, qpt)
                    .leftJoin(qpt.store, qs)
                    .leftJoin(qm.professionalProfile.customerGroup, ql)
                    .where(customerPredicate.and(qpt.customerId.eq(customerId)))
                    .groupBy(
                            qm.accountId,
                            qm.firstName,
                            qm.lastName,
                            qm.professionalProfile.businessName,
                            ql.description,
                            qm.contact,
                            qpti.productId,
                            qp.name
                            )
                    .orderBy(
                            qpti.totalPrice.sum().longValue().desc())
                    .limit(rankings);
            
            List<TopProductReportModel> tempList = query.list(Projections.fields(TopProductReportModel.class, 
                    qm.accountId.as("customerAccount"),
                    qm.firstName.as("firstName"),
                    qm.lastName.as("lastName"),
                    qm.professionalProfile.businessName.as("businessName"),
                    ql.description.as("customerGroup"),
                    qm.contact.as("handphone"),
                    qpti.productId.as("itemCode"),
                    qp.name.as("product"),
                    qpti.totalPrice.sum().longValue().as("value")));
            
            if (tempList!= null && !tempList.isEmpty())
                topProductsPerCustomer.addAll(tempList);
        }
        
        return topProductsPerCustomer;
    }
    

    @Override
    public boolean isEmpty(Map<String, String> parameters) {
        QPosTransaction qpt = QPosTransaction.posTransaction;
        QPosTxItem qpti = QPosTxItem.posTxItem;
        QMemberModel qm = QMemberModel.memberModel;
        QProduct qp = QProduct.product;
        QStore qs = QStore.store;

        BooleanBuilder exp = new BooleanBuilder(qpt.transactionDate.between(ReportDateFormat.DATE_FORMATTER.parseLocalDateTime(parameters.get(CommonReportFilter.DATE_FROM)),
                DateUtil.getEndOfDay(ReportDateFormat.DATE_FORMATTER.parseLocalDateTime(parameters.get(CommonReportFilter.DATE_TO)))));

        exp.and(qs.code.eq(parameters.get(CommonReportFilter.STORE)));
        exp.and(qpt.customerId.eq(qm.accountId));
        exp.and(qpti.productId.eq(qp.id));

        if (parameters.get(CommonReportFilter.MEMBER_TYPE) != null && !parameters.get(CommonReportFilter.MEMBER_TYPE).isEmpty()) {
            exp.and(qm.memberType.code.eq(parameters.get(CommonReportFilter.MEMBER_TYPE)));
        }

        JPAQuery query = new JPAQuery(em).distinct().from(qpti, qm, qp)
                .innerJoin(qpti.posTransaction, qpt)
                .leftJoin(qpt.store, qs)
                .where(exp.getValue());

        return query.notExists();
    }
    
    private String getCurrentUser() {
        CustomSecurityUserDetailsImpl currentUser = UserUtil.getCurrentUser();
        if (currentUser != null) {
            return currentUser.getUsername();
        }
        return null;
    }

    public static class TopProductReportModel {
        private String customerAccount;
        private String firstName;
        private String lastName;
        private String businessName;
        private String customerGroup;
        private String handphone;
        private String itemCode;
        private String product;
        private String memberType;
        private Long value;
        
        public String getCustomerAccount() {
            return customerAccount;
        }
        public String getFirstName() {
            return firstName;
        }
        public String getLastName() {
            return lastName;
        }
        public String getBusinessName() {
            return businessName;
        }
        public String getCustomerGroup() {
            return customerGroup;
        }
        public String getHandphone() {
            return handphone;
        }
        public String getItemCode() {
            return itemCode;
        }
        public String getProduct() {
            return product;
        }
        public String getMemberType() {
            return memberType;
        }
        public Long getValue() {
            return value;
        }
        public void setCustomerAccount(String customerAccount) {
            this.customerAccount = customerAccount;
        }
        public void setFirstName(String firstName) {
            this.firstName = firstName;
        }
        public void setLastName(String lastName) {
            this.lastName = lastName;
        }
        public void setBusinessName(String businessName) {
            this.businessName = businessName;
        }
        public void setCustomerGroup(String customerGroup) {
            this.customerGroup = customerGroup;
        }
        public void setHandphone(String handphone) {
            this.handphone = handphone;
        }
        public void setItemCode(String itemCode) {
            this.itemCode = itemCode;
        }
        public void setProduct(String product) {
            this.product = product;
        }
        public void setMemberType(String memberType) {
            this.memberType = memberType;
        }
        public void setValue(Long value) {
            this.value = value;
        }
        
    }

    @Override
    public List<String> validate(Map<String, String> parameters) {
        List<String> errors = Lists.newArrayList();

        String dtFrom = parameters.get(CommonReportFilter.DATE_FROM);
        String dtTo = parameters.get(CommonReportFilter.DATE_FROM);

        if (StringUtils.isNotBlank(dtFrom) && StringUtils.isNotBlank(dtTo)) {
            DateTime dateFrom = ReportDateFormat.DATE_FORMATTER.parseDateTime(dtFrom);
            DateTime dateTo = ReportDateFormat.DATE_FORMATTER.parseDateTime(dtTo);
            if (dateFrom.isAfter(dateTo) || dateTo.isBefore(dateFrom)) {
                errors.add(messageSource.getMessage("report.date.from.before.date.to", null, LocaleContextHolder.getLocale()));
            }
        } else {
            errors.add(messageSource.getMessage("report.date.from.date.to.required", null, LocaleContextHolder.getLocale()));
        }
        
        if (StringUtils.isBlank(parameters.get(CommonReportFilter.STORE))) {
            errors.add(messageSource.getMessage("report.store.required", null, LocaleContextHolder.getLocale()));
        }

        if (StringUtils.isBlank(parameters.get(CommonReportFilter.RANK))) {
            errors.add(messageSource.getMessage("report.rank.required", null, LocaleContextHolder.getLocale()));
        }
        
        return errors;
    }

}
