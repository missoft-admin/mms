package com.transretail.crm.loyalty.repo.custom.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.apache.commons.lang.StringUtils;
import org.hibernate.StatelessSession;
import org.joda.time.DateTime;
import org.joda.time.LocalDate;
import org.joda.time.format.DateTimeFormat;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;

import com.google.common.collect.Lists;
import com.mysema.query.BooleanBuilder;
import com.mysema.query.Tuple;
import com.mysema.query.jpa.JPASubQuery;
import com.mysema.query.jpa.JPQLQuery;
import com.mysema.query.jpa.hibernate.HibernateSubQuery;
import com.mysema.query.jpa.impl.JPAQuery;
import com.mysema.query.types.ConstantImpl;
import com.mysema.query.types.Expression;
import com.mysema.query.types.OrderSpecifier;
import com.mysema.query.types.Predicate;
import com.mysema.query.types.expr.BooleanExpression;
import com.mysema.query.types.expr.DateTimeExpression;
import com.mysema.query.types.expr.NumberExpression;
import com.mysema.query.types.path.PathBuilder;
import com.mysema.query.types.path.PathBuilderFactory;
import com.mysema.query.types.template.DateTimeTemplate;
import com.transretail.crm.common.service.dto.request.PagingParam;
import com.transretail.crm.common.service.dto.response.ResultList;
import com.transretail.crm.core.entity.lookup.LookupDetail;
import com.transretail.crm.core.jpa.DbMetadataUtil;
import com.transretail.crm.core.jpa.DbMetadataUtil.DbType;
import com.transretail.crm.core.repo.UserRepo;
import com.transretail.crm.core.service.CodePropertiesService;
import com.transretail.crm.core.service.LookupService;
import com.transretail.crm.loyalty.dto.AllocationDto;
import com.transretail.crm.loyalty.dto.LoyaltyCardTrackerDto;
import com.transretail.crm.loyalty.dto.LoyaltyCardTrackerFilterDto;
import com.transretail.crm.loyalty.dto.LoyaltyCardTrackingDto;
import com.transretail.crm.loyalty.dto.MultipleAllocationDto;
import com.transretail.crm.loyalty.dto.QLoyaltyCardTrackerDto;
import com.transretail.crm.loyalty.entity.QLoyaltyCardInventory;
import com.transretail.crm.loyalty.entity.QLoyaltyCardInventoryHistory;
import com.transretail.crm.loyalty.entity.enums.LoyaltyInventoryTransaction;
import com.transretail.crm.loyalty.repo.custom.LoyaltyCardInventoryHistoryRepoCustom;
import com.transretail.crm.loyalty.service.LoyaltyCardService;

public class LoyaltyCardInventoryHistoryRepoImpl implements LoyaltyCardInventoryHistoryRepoCustom {
	
	@PersistenceContext
	EntityManager em;
	@Autowired
    StatelessSession statelessSession;
	@Autowired
	CodePropertiesService codePropertiesService;
	@Autowired
	MessageSource messageSource;
	@Autowired
    private DbMetadataUtil dbMetadataUtil;
	@Autowired
	LoyaltyCardService loyaltyCardService;
	@Autowired
	LookupService lookupService;
	@Autowired
	UserRepo userRepo;
	
	private static final String DATE_FORMAT_H2 = "yyyy-MM-dd HH:mm";
	private static final String DATE_TRUNC_UNIT_ORACLE = "mi";
	
	private static final String DATE_ONLY_FORMAT_H2 = "yyyy-MM-dd";
	private static final String DATE_ONLY_TRUNC_UNIT_ORACLE = "dd";
	
	private Predicate getCommonFilters(QLoyaltyCardInventoryHistory qHistory, String location, LookupDetail product) {
		BooleanBuilder filter = new BooleanBuilder(qHistory.track.isTrue().or(qHistory.track.isNull()));
		if(StringUtils.isNotBlank(location))
			filter.and(qHistory.location.eq(location));
		if(product != null)
			filter.and(qHistory.inventory.product.name.code.eq(product.getCode()));
		return filter;
	}
	
	@Override
	public ResultList<LoyaltyCardTrackerDto> trackInventories(LoyaltyCardTrackerFilterDto filterDto) {
		QLoyaltyCardInventoryHistory qHistory = QLoyaltyCardInventoryHistory.loyaltyCardInventoryHistory;
		
		PagingParam pagination = filterDto.getPagination();
		String location = filterDto.getLocation();
		LookupDetail product = filterDto.getProduct();
		String[] trackStats = new String[] {
				codePropertiesService.getDetailLoyaltyStatusActive(),
				codePropertiesService.getDetailLoyaltyStatusBurned(),
				codePropertiesService.getDetailLoyaltyStatusInactive(),
				codePropertiesService.getDetailLoyaltyStatusTransferIn(),
				codePropertiesService.getDetailLoyaltyStatusInTransit(),
				codePropertiesService.getDetailLoyaltyStatusFound(),
				codePropertiesService.getDetailLoyaltyStatusMissing()
		};
		
		
		boolean isH2 = (dbMetadataUtil.getDbType() == DbType.H2);
		
		QLoyaltyCardInventoryHistory qHistB = new QLoyaltyCardInventoryHistory("historyB");

		DateTimeExpression<DateTime> qHistDate = null;
		DateTimeExpression<DateTime> qHistDateB = null;
		
		Predicate commonFilters = getCommonFilters(qHistory, location, product);
		Predicate commonFiltersB = getCommonFilters(qHistB, location, product);
		
		if(isH2) {
			qHistDate = trunc(qHistory.created);
			qHistDateB = trunc(qHistB.created);
		} else {
			qHistDate = trunc(qHistory.created, DATE_ONLY_TRUNC_UNIT_ORACLE);
			qHistDateB = trunc(qHistB.created, DATE_ONLY_TRUNC_UNIT_ORACLE);
		}
		
		//negate values upon saving on dto
		NumberExpression<Long> activeExp = new JPASubQuery().from(qHistB)
			.where(qHistB.status.code.eq(codePropertiesService.getDetailLoyaltyStatusActive()).and(qHistDateB.eq(qHistDate)).and(commonFiltersB))
			.count();
		NumberExpression<Long> burnedExp = new JPASubQuery().from(qHistB)
				.where(qHistB.status.code.eq(codePropertiesService.getDetailLoyaltyStatusBurned()).and(qHistDateB.eq(qHistDate)).and(commonFiltersB))
				.count();
		NumberExpression<Long> inactiveExp = new JPASubQuery().from(qHistB)
				.where(qHistB.status.code.eq(codePropertiesService.getDetailLoyaltyStatusInactive()).and(qHistDateB.eq(qHistDate)).and(commonFiltersB))
				.count();
		NumberExpression<Long> transferInExp = new JPASubQuery().from(qHistB)
				.where(qHistB.status.code.eq(codePropertiesService.getDetailLoyaltyStatusTransferIn()).and(qHistDateB.eq(qHistDate)).and(commonFiltersB))
				.count();
		NumberExpression<Long> inTransitExp = new JPASubQuery().from(qHistB)
				.where(qHistB.status.code.eq(codePropertiesService.getDetailLoyaltyStatusInTransit()).and(qHistDateB.eq(qHistDate)).and(commonFiltersB))
				.count();
		NumberExpression<Long> foundExp = new JPASubQuery().from(qHistB)
				.where(qHistB.status.code.eq(codePropertiesService.getDetailLoyaltyStatusFound()).and(qHistDateB.eq(qHistDate)).and(commonFiltersB))
				.count();
		NumberExpression<Long> missingExp = new JPASubQuery().from(qHistB)
				.where(qHistB.status.code.eq(codePropertiesService.getDetailLoyaltyStatusMissing()).and(qHistDateB.eq(qHistDate)).and(commonFiltersB))
				.count();
		
		String[] status = new String[] {codePropertiesService.getDetailLoyaltyStatusInactive(),
				codePropertiesService.getDetailLoyaltyStatusForAllocation(),
				codePropertiesService.getDetailLoyaltyStatusAllocated(),
				codePropertiesService.getDetailLoyaltyStatusTransferIn(), 
				codePropertiesService.getDetailLoyaltyStatusFound()};

		Integer totalCount =  
				new JPAQuery(em)
				.from(qHistory)
				.where(qHistory.status.code.in(trackStats).and(filterDto.createSearchExpression()))
				.distinct()
				.list(qHistDate).size(); //TODO
		
		JPAQuery query = new JPAQuery(em)
			.from(qHistory)
			.where(qHistory.status.code.in(trackStats).and(filterDto.createSearchExpression()).and(commonFilters))
			.groupBy(qHistDate);
		
		applyPagination(query, pagination, qHistDate);
		
		List<LoyaltyCardTrackerDto> histories = 
				query
				.list(new QLoyaltyCardTrackerDto(qHistDate, transferInExp, inTransitExp, burnedExp, activeExp, inactiveExp, foundExp.subtract(missingExp)));
		
		for(LoyaltyCardTrackerDto history: histories) {
			Long begBal = new JPAQuery(em).from(qHistory).where(
					qHistory.status.code.in(status)
					.and(commonFilters)
					.and(qHistory.created.in(
					new JPASubQuery().from(qHistB).where(
							qHistory.inventory.eq(qHistB.inventory)
							.andAnyOf(qHistDateB.lt(history.getTransactionDatetime())
									)).list(qHistB.created.max()))))
					.count();
			history.setBegBal(begBal);
		}
		
		return new ResultList<LoyaltyCardTrackerDto>(histories,
				totalCount != null ? totalCount : 0,
				pagination.getPageNo(), pagination.getPageSize());
		
		/*if(pagination.getOrder() == null || pagination.getOrder().isEmpty()) {
			Map<String, Boolean> order = new LinkedHashMap<String, Boolean>();
			order.put("createdDate", Boolean.FALSE);
			pagination.setOrder(order);
		}
		
		JPQLQuery query = jpaQuery
				.from(qHistory)
				.where(filterDto.createSearchExpression())
				.groupBy(qHistory.createdDate);
		
		String location = filterDto.getLocation();
		LookupDetail product = filterDto.getProduct();
		
		QLoyaltyCardInventoryHistory qHistA = new QLoyaltyCardInventoryHistory("historyA");
		QLoyaltyCardInventoryHistory qHistB = new QLoyaltyCardInventoryHistory("historyB");
		
		BooleanExpression locationFilter = qHistA.createdDate.eq(qHistory.createdDate)
				.and(qHistA.location.eq(location))
				.and(qHistA.inventory.product.name.code.eq(product.getCode()));
		BooleanExpression transferredFilter = qHistA.createdDate.eq(qHistory.createdDate)
				.and(qHistA.location.eq(location))
				.and(qHistA.inventory.product.name.code.eq(product.getCode()))
				.and(qHistA.status.code.eq(codePropertiesService.getDetailLoyaltyStatusInTransit()));
		BooleanExpression totalStockFilter =
				qHistA.location.eq(location)
				.and(qHistA.inventory.product.name.code.eq(product.getCode()))
				.and(qHistA.status.code.eq(codePropertiesService.getDetailLoyaltyStatusInactive()))
				.and(qHistA.created.in(new HibernateSubQuery().from(qHistB).where(qHistB.inventory.eq(qHistA.inventory).and(qHistB.createdDate.lt(qHistory.createdDate))).list(qHistB.created.max()))); // TODO CREATED - 1
		BooleanExpression totalInactiveLCsFilter =
				qHistA.location.eq(location)
				.and(qHistA.inventory.product.name.code.eq(product.getCode()))
				.and(BooleanExpression.anyOf(qHistA.status.code.eq(codePropertiesService.getDetailLoyaltyStatusInactive()),
						qHistA.status.code.eq(codePropertiesService.getDetailLoyaltyStatusForAllocation()),
						qHistA.status.code.eq(codePropertiesService.getDetailLoyaltyStatusAllocated())))
				.and(qHistA.created.in(new HibernateSubQuery().from(qHistB).where(qHistB.inventory.eq(qHistA.inventory).and(qHistB.createdDate.loe(qHistory.createdDate))).list(qHistB.created.max())));
		
		Long totalCount = new Long(query.list(qHistory.createdDate).size());
		
		JPQLQuery selectQuery = applyPagination(query, pagination, LoyaltyCardInventoryHistory.class);
		
		List<LoyaltyCardTrackerDto> list = selectQuery
				.list(new QLoyaltyCardTrackerDto(qHistory.createdDate,
						new HibernateSubQuery().from(qHistA).where(totalStockFilter).count(), //total stock
						new HibernateSubQuery().from(qHistA).where(locationFilter.and(qHistA.status.code.eq(codePropertiesService.getDetailLoyaltyStatusInactive()))).count(), //received
						new HibernateSubQuery().from(qHistA).where(locationFilter.and(qHistA.status.code.eq(codePropertiesService.getDetailLoyaltyStatusActive()))).count(), //active
						new HibernateSubQuery().from(qHistA).where(locationFilter.and(qHistA.status.code.eq(codePropertiesService.getDetailLoyaltyStatusBurned()))).count(), //rejected TODO no status for that yet
						new HibernateSubQuery().from(qHistA).where(transferredFilter).count(), //transferred out
						new HibernateSubQuery().from(qHistA).where(totalInactiveLCsFilter).count())); //total inactive
		

		return new ResultList<LoyaltyCardTrackerDto>(list,
				totalCount != null ? totalCount : 0,
				pagination.getPageNo(), pagination.getPageSize());*/
	}
	
	@Override
	public ResultList<LoyaltyCardTrackingDto> trackInventories2(LoyaltyCardTrackerFilterDto filterDto) {
		boolean isH2 = (dbMetadataUtil.getDbType() == DbType.H2);
		
		String active = getTransactionType("tracker_status_active");
		String burn = getTransactionType("tracker_status_burn");
		String receive = getTransactionType("tracker_status_receive");
		String transferIn = getTransactionType("tracker_status_transferin");
		String transferOut = getTransactionType("tracker_status_transferout");
		String physicalCount = getTransactionType("tracker_status_physicalcount");
		
		QLoyaltyCardInventoryHistory qHistory = QLoyaltyCardInventoryHistory.loyaltyCardInventoryHistory;
		
		JPAQuery jpaQuery = new JPAQuery(em);
		
		String[] ignore = new String[] {
			codePropertiesService.getDetailLoyaltyStatusForAllocation(),
			codePropertiesService.getDetailLoyaltyStatusAllocated(),
			codePropertiesService.getDetailLoyaltyStatusDisabled()
		};
		
		BooleanExpression trackOnlyFilter = qHistory.track.isTrue().or(qHistory.track.isNull());
		
		String location = filterDto.getLocation();
		LookupDetail product = filterDto.getProduct();
		
		QLoyaltyCardInventoryHistory qHistB = new QLoyaltyCardInventoryHistory("historyB");
		
		DateTimeExpression<DateTime> qHistDate = null;
		DateTimeExpression<DateTime> qHistDateB = null;
		
		if(isH2) {
			qHistDate = toDate(qHistory.created.stringValue(), DATE_FORMAT_H2);
			qHistDateB = toDate(qHistB.created.stringValue(), DATE_FORMAT_H2);
		} else {
			/*qHistDate = SQLExpressions.datetrunc(DatePart.month, qHistory.created);
			qHistDateB = SQLExpressions.datetrunc(DatePart.month, qHistB.created);*/
			
			qHistDate = trunc(qHistory.created, DATE_TRUNC_UNIT_ORACLE);
			qHistDateB = trunc(qHistB.created, DATE_TRUNC_UNIT_ORACLE);
		}
		
		PagingParam pagination = filterDto.getPagination();
		if(pagination.getOrder() == null || pagination.getOrder().isEmpty()) {
			Map<String, Boolean> order = new LinkedHashMap<String, Boolean>();
			order.put("createdDate", Boolean.FALSE);
			pagination.setOrder(order);
		}
		
		JPQLQuery query = jpaQuery
				.from(qHistory)
				.where(filterDto.createSearchExpression().and(qHistory.status.code.notIn(ignore)).and(trackOnlyFilter))
				.groupBy(qHistDate, qHistory.status);
		
		Long totalCount = new Long(query.list(qHistDate, qHistory.status.code).size());
		
		applyPagination(query, pagination, qHistDate);
		
		List<Tuple> tuples = query.list(qHistDate, 
				qHistory.status.code, 
				qHistory.inventory.count());
		
		String[] status = new String[] {codePropertiesService.getDetailLoyaltyStatusInactive(),
				codePropertiesService.getDetailLoyaltyStatusForAllocation(),
				codePropertiesService.getDetailLoyaltyStatusAllocated(),
				codePropertiesService.getDetailLoyaltyStatusTransferIn(), 
				codePropertiesService.getDetailLoyaltyStatusFound()};
		
		List<LoyaltyCardTrackingDto> history = Lists.newArrayList();
		for(Tuple tuple : tuples) {
			DateTime datetime = null;
			
			if(isH2) {
				Object dateObj = tuple.get(qHistDate);
				datetime = DateTimeFormat.forPattern("yyyy-MM-dd HH:mm:ss.S").parseDateTime((String) dateObj);
			} else {
				datetime = tuple.get(qHistDate);
			}
			
			Long begBal = new JPAQuery(em).from(qHistory).where(
					qHistory.location.eq(location)
					.and(trackOnlyFilter)
					.and(qHistory.inventory.product.name.code.eq(product.getCode()))
					.and(qHistory.created.in(
					new JPASubQuery().from(qHistB).where(
							qHistory.inventory.eq(qHistB.inventory)
							.andAnyOf(qHistDateB.lt(tuple.get(qHistDate))
									)).list(qHistB.created.max())))
					.and(qHistory.status.code.in(status))).count();
			
			Long endBal = new JPAQuery(em).from(qHistory).where(
					qHistory.location.eq(location)
					.and(trackOnlyFilter)
					.and(qHistory.inventory.product.name.code.eq(product.getCode()))
					.and(qHistory.created.in(
					new JPASubQuery().from(qHistB).where(
							qHistory.inventory.eq(qHistB.inventory)
							.andAnyOf(qHistDateB.loe(tuple.get(qHistDate))
									)).list(qHistB.created.max())))
					.and(qHistory.status.code.in(status))).count();
			
			Long quantity = tuple.get(qHistory.inventory.count());
			String statusCode = tuple.get(qHistory.status.code);
			
			String transactionType = "";
			
			if(statusCode.equalsIgnoreCase(codePropertiesService.getDetailLoyaltyStatusActive())) {
				transactionType = active;
				quantity = -quantity;
			} else if(statusCode.equalsIgnoreCase(codePropertiesService.getDetailLoyaltyStatusBurned())) {
				transactionType = burn;
				quantity = -quantity;
			} else if(statusCode.equalsIgnoreCase(codePropertiesService.getDetailLoyaltyStatusInactive())) {
				transactionType = receive;
			} else if(statusCode.equalsIgnoreCase(codePropertiesService.getDetailLoyaltyStatusTransferIn())) {
				transactionType = transferIn;
			} else if(statusCode.equalsIgnoreCase(codePropertiesService.getDetailLoyaltyStatusInTransit())) {
				transactionType = transferOut;
				quantity = -quantity;
			} else if(statusCode.equalsIgnoreCase(codePropertiesService.getDetailLoyaltyStatusFound())) {
				transactionType = physicalCount;
			} else if(statusCode.equalsIgnoreCase(codePropertiesService.getDetailLoyaltyStatusMissing())) {
				transactionType = physicalCount;
				quantity = -quantity;
			}
			
			LoyaltyCardTrackingDto lastTxnDto = null;
			
			if(history.size() > 0) {
				lastTxnDto = history.get(history.size() - 1);	
			}
			
			if(transactionType.equals(physicalCount) 
					&& lastTxnDto != null 
					&& lastTxnDto.getTransaction().equals(physicalCount)
					&& lastTxnDto.getCreatedDate().equals(datetime)) {
				lastTxnDto.setQuantity(lastTxnDto.getQuantity() + quantity);
			} else {
				LoyaltyCardTrackingDto dto = new LoyaltyCardTrackingDto();
				dto.setBeginningBal(begBal);
				dto.setEndingBal(endBal);
				dto.setQuantity(quantity);
				dto.setTransaction(transactionType);
				dto.setCreatedDate(datetime);
				history.add(dto);	
			}
			
			
		}
		
		/*Iterator<LoyaltyCardTrackingDto> iter = history.iterator();
		int count = 0;
		while(iter.hasNext()) {
			LoyaltyCardTrackingDto dto = iter.next();
			if(count <= (history.size()) && dto.getTransaction().equalsIgnoreCase(physicalCount)) {
				for(int i = count + 1; i<history.size(); i++) {
					LoyaltyCardTrackingDto dtoNext = history.get(i);
					if(dto.getCreatedDate().equals(dtoNext.getCreatedDate())) {
						if(dtoNext.getTransaction().equals(physicalCount)) {
							dtoNext.setQuantity(dtoNext.getQuantity() + dto.getQuantity());
							iter.remove();
							totalCount--;
						}
					} else {
						break;
					}
				}
			}
			count ++;
		}*/
		
		return new ResultList<LoyaltyCardTrackingDto>(history,
				totalCount != null ? totalCount : 0,
				pagination.getPageNo(), pagination.getPageSize());
	}
	
	private String getTransactionType(String code) {
		return messageSource.getMessage(code, null, LocaleContextHolder.getLocale());
	}
	
	@SuppressWarnings({ "rawtypes", "unchecked" })
	private JPQLQuery applyPagination(JPQLQuery query, PagingParam pagination, DateTimeExpression<DateTime> qHistDate) {
		if (pagination != null && pagination.getPageSize() >= 0 && pagination.getPageNo() >= 0) {
            query.offset(pagination.getPageNo() * pagination.getPageSize());
            query.limit(pagination.getPageSize());
		}
		if (pagination.getOrder() != null) {
            for (Map.Entry<String, Boolean> entry : pagination.getOrder().entrySet()) {
                query.orderBy(new OrderSpecifier(entry.getValue() ? com.mysema.query.types.Order.ASC
                    : com.mysema.query.types.Order.DESC, qHistDate));
            }
        } else {
        	query.orderBy(qHistDate.desc());
        }
		return query;
	}
	
	@SuppressWarnings({ "rawtypes", "unchecked" })
	private JPQLQuery applyPagination(JPQLQuery query, PagingParam pagination, Class<?> domainClass) {
		if (pagination != null && pagination.getPageSize() >= 0 && pagination.getPageNo() >= 0) {
            query.offset(pagination.getPageNo() * pagination.getPageSize());
            query.limit(pagination.getPageSize());
		}
		if (pagination.getOrder() != null) {
            PathBuilder<?> builder = new PathBuilderFactory().create(domainClass);
            for (Map.Entry<String, Boolean> entry : pagination.getOrder().entrySet()) {
                Expression<Object> property = builder.get(entry.getKey());
                query.orderBy(new OrderSpecifier(entry.getValue() ? com.mysema.query.types.Order.ASC
                    : com.mysema.query.types.Order.DESC, property));
            }
        }
		return query;
	}
	
	/*
	Unit       Valid format parameters
	Year       SYYYY, YYYY, YEAR, SYEAR, YYY, YY, Y
	ISO Year   IYYY, IY, I
	Quarter    Q
	Month      MONTH, MON, MM, RM
	Week       WW
	IW         IW
	W          W
	Day         DDD, DD, J
	Start day of the week    DAY, DY, D
	Hour       HH, HH12, HH24
	Minute     MI
	*/
	
	private DateTimeExpression<DateTime> toDate(Expression<?> expr, String pattern){
		return DateTimeTemplate.create(DateTime.class, "to_date({0},  '{1s}')", 
    			expr, 
    			ConstantImpl.create(pattern));
    }
	
	private DateTimeExpression<DateTime> trunc(Expression<?> expr){
		/*SELECT TRUNC(CREATED_DATETIME, 'MI') FROM CRM_LC_INVENTORY_HIST;*/
		return DateTimeTemplate.create(DateTime.class, "trunc({0})", 
    			expr);
    }
	
	private DateTimeExpression<DateTime> trunc(Expression<?> expr, String unit){
		/*SELECT TRUNC(CREATED_DATETIME, 'MI') FROM CRM_LC_INVENTORY_HIST;*/
		return DateTimeTemplate.create(DateTime.class, "trunc({0},  '{1s}')", 
    			expr, 
    			ConstantImpl.create(unit));
    }
	
	private List<MultipleAllocationDto> getTransactionsForReceive(String username, LocalDate dateFrom, LocalDate dateTo) {
		boolean isH2 = (dbMetadataUtil.getDbType() == DbType.H2);
		QLoyaltyCardInventoryHistory qHistory = QLoyaltyCardInventoryHistory.loyaltyCardInventoryHistory;
		QLoyaltyCardInventoryHistory qHistB = new QLoyaltyCardInventoryHistory("historyB");
		
		String status = codePropertiesService.getDetailLoyaltyStatusTransferIn();
		String userLocation = userRepo.findByUsernameIgnoreCase(username).getInventoryLocation();
		
		DateTime dtFrom = dateFrom.toDateTimeAtStartOfDay();
		DateTime dtTo = dateTo.plusDays(1).toDateTimeAtStartOfDay().minusMillis(1);
		
		String dateFormat = "yyyy-MM-dd HH:mm:ss.S";
		
		DateTimeExpression<DateTime> qHistDate = null;
		
		if(isH2) {
			qHistDate = trunc(qHistory.created);
		} else {
			qHistDate = trunc(qHistory.created, DATE_TRUNC_UNIT_ORACLE);
		}
		
		List<Tuple> result = new JPAQuery(em).from(qHistory).where(
				qHistory.status.code.eq(status)
				.and(qHistory.createUser.equalsIgnoreCase(username))
				.and(qHistory.location.eq(userLocation))
				/*.and(qHistory.created.between(dtFrom, dtTo))*/.and(qHistory.created.goe(dtFrom)).and(qHistory.created.loe(dtTo))
				.and(qHistory.created.in(
						new JPASubQuery().from(qHistB).where(
						qHistory.inventory.eq(qHistB.inventory))
						.list(qHistB.created.max()))))
				.groupBy(qHistory.inventory.product.name.code, qHistory.origin, qHistory.location, qHistDate)
				.orderBy(qHistDate.asc())
				.list(qHistory.inventory.sequence.min(), qHistory.inventory.sequence.max(), 
						qHistory.inventory.product.name.code, qHistory.origin, qHistory.location, qHistDate);
		
		Map<String, List<AllocationDto>> map = new HashMap<String, List<AllocationDto>>();
		for(Tuple tuple: result) {
			DateTime datetime = null;
			if(isH2) {
				Object dateObj = tuple.get(qHistDate);
				datetime = DateTimeFormat.forPattern(dateFormat).parseDateTime((String) dateObj);
			} else {
				datetime = tuple.get(qHistDate);
			}
			
			String location = tuple.get(qHistory.origin);
			String transferTo = tuple.get(qHistory.location);
			
			String str = new StringBuffer(location)
				.append("^")
				.append(transferTo)
				.append("^")
				.append(datetime.toString(dateFormat)).toString();
			
			AllocationDto transaction = new AllocationDto(
					lookupService.getDetailByCode(tuple.get(qHistory.inventory.product.name.code)),
					tuple.get(qHistory.inventory.sequence.min()),
					tuple.get(qHistory.inventory.sequence.max()),
					null
					);
			
			if(map.get(str) == null) 
				map.put(str, new ArrayList<AllocationDto>());
			
			map.get(str).add(transaction);
		}
		
		List<MultipleAllocationDto> transactions = new ArrayList<MultipleAllocationDto>();
		
		for(String str: map.keySet()) {
			String[] strs = str.split("\\^");
			
			MultipleAllocationDto transaction = new MultipleAllocationDto(strs[0], 
					loyaltyCardService.getInventoryDesc(strs[0]), 
					strs[1], 
					loyaltyCardService.getInventoryDesc(strs[1]), 
					DateTimeFormat.forPattern(dateFormat).parseDateTime(strs[2]), 
					map.get(str));
			
			transactions.add(transaction);
		}
		
		return transactions;
	}
	
	private List<MultipleAllocationDto> getOtherTransactions(String status, String username, LocalDate dateFrom, LocalDate dateTo) {
		QLoyaltyCardInventory qInventory = QLoyaltyCardInventory.loyaltyCardInventory;
		String userLocation = userRepo.findByUsernameIgnoreCase(username).getInventoryLocation();
		
		boolean isH2 = (dbMetadataUtil.getDbType() == DbType.H2);
		DateTime dtFrom = dateFrom.toDateTimeAtStartOfDay();
		DateTime dtTo = dateTo.plusDays(1).toDateTimeAtStartOfDay().minusMillis(1);
		
		String dateFormat = "yyyy-MM-dd HH:mm:ss.S";
		
		DateTimeExpression<DateTime> qUpdateDate = null;
		if(isH2) {
			qUpdateDate = trunc(qInventory.lastUpdated);
		} else {
			qUpdateDate = trunc(qInventory.lastUpdated, DATE_TRUNC_UNIT_ORACLE);
		}
		
		BooleanExpression locationFilter = null;
		if(!status.equals(codePropertiesService.getDetailLoyaltyStatusAllocated()))
			locationFilter = qInventory.location.eq(userLocation);
		
		List<Tuple> result = new JPAQuery(em).from(qInventory)
				.where(/*qInventory.created.between(dtFrom, dtTo)*/qInventory.lastUpdated.goe(dtFrom).and(qInventory.lastUpdated.loe(dtTo))
						.and(qInventory.status.code.eq(status))
						.and(qInventory.lastUpdateUser.equalsIgnoreCase(username))
						.and(locationFilter))
				.groupBy(qInventory.product.name.code, qInventory.location, qInventory.transferTo, qUpdateDate, qInventory.referenceNo)
				.orderBy(qUpdateDate.asc())
				.list(qInventory.sequence.min(), qInventory.sequence.max(), 
						qInventory.product.name.code, qInventory.location, qInventory.transferTo, qUpdateDate, qInventory.referenceNo);
		
		Map<String, List<AllocationDto>> map = new HashMap<String, List<AllocationDto>>();
		for(Tuple tuple: result) {
			DateTime datetime = null;
			if(isH2) {
				Object dateObj = tuple.get(qUpdateDate);
				datetime = DateTimeFormat.forPattern(dateFormat).parseDateTime((String) dateObj);
			} else {
				datetime = tuple.get(qUpdateDate);
			}
			
			String location = tuple.get(qInventory.location);
			String transferTo = tuple.get(qInventory.transferTo);
			
			String str = new StringBuffer(location)
				.append("^")
				.append(transferTo)
				.append("^")
				.append(datetime.toString(dateFormat)).toString();
			
			AllocationDto transaction = new AllocationDto(
					lookupService.getDetailByCode(tuple.get(qInventory.product.name.code)),
					tuple.get(qInventory.sequence.min()),
					tuple.get(qInventory.sequence.max()),
					tuple.get(qInventory.referenceNo)
					);
			
			if(map.get(str) == null) 
				map.put(str, new ArrayList<AllocationDto>());
			
			map.get(str).add(transaction);
		}
		
		List<MultipleAllocationDto> transactions = new ArrayList<MultipleAllocationDto>();
		
		for(String str: map.keySet()) {
			String[] strs = str.split("\\^");
			
			MultipleAllocationDto transaction = new MultipleAllocationDto(strs[0], 
					loyaltyCardService.getInventoryDesc(strs[0]), 
					strs[1], 
					loyaltyCardService.getInventoryDesc(strs[1]), 
					DateTimeFormat.forPattern(dateFormat).parseDateTime(strs[2]), 
					map.get(str));
			
			transactions.add(transaction);
		}
		
		return transactions;
	}
	
	@Override
	public List<MultipleAllocationDto> getTransactions(String transactionType, String username, LocalDate dateFrom, LocalDate dateTo) {
		if(LoyaltyInventoryTransaction.valueOf(transactionType).compareTo(LoyaltyInventoryTransaction.ALLOCATE) == 0) {
			return getOtherTransactions(codePropertiesService.getDetailLoyaltyStatusForAllocation(), username, dateFrom, dateTo);
		} else if(LoyaltyInventoryTransaction.valueOf(transactionType).compareTo(LoyaltyInventoryTransaction.APPROVE_ALLOCATION) == 0){
			return getOtherTransactions(codePropertiesService.getDetailLoyaltyStatusAllocated(), username, dateFrom, dateTo);
		} else if(LoyaltyInventoryTransaction.valueOf(transactionType).compareTo(LoyaltyInventoryTransaction.TRANSFER) == 0){
			return getOtherTransactions(codePropertiesService.getDetailLoyaltyStatusInTransit(), username, dateFrom, dateTo);
		} else if(LoyaltyInventoryTransaction.valueOf(transactionType).compareTo(LoyaltyInventoryTransaction.RECEIVE) == 0){
			return getTransactionsForReceive(username, dateFrom, dateTo);
		}
		return Lists.newArrayList();
	}
	
}
