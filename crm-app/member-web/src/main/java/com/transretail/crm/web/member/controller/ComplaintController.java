package com.transretail.crm.web.member.controller;


import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.transretail.crm.common.service.dto.response.ResultList;
import com.transretail.crm.core.dto.ComplaintDto;
import com.transretail.crm.core.dto.ComplaintSearchDto;
import com.transretail.crm.core.dto.MessageDto;
import com.transretail.crm.core.entity.Complaint;
import com.transretail.crm.core.entity.MemberModel;
import com.transretail.crm.core.security.util.UserUtil;
import com.transretail.crm.core.service.CodePropertiesService;
import com.transretail.crm.core.service.ComplaintService;
import com.transretail.crm.core.service.MemberService;
import com.transretail.crm.core.service.UserService;
import com.transretail.crm.web.member.service.EmailService;


@RequestMapping("/member/complaint")
@Controller
public class ComplaintController {

	private static final String PATH_MAIN 				= "/member/complaint";
	private static final String PATH_SAVE 				= "/member/complaint/save";

	private static final String PATH_FORM				= "/member/complaint/form";
	private static final String UIATTR_FORM		 		= "complaint";
	private static final String UIATTR_ACTION			= "complaintAction";
	private static final String UIATTR_LBL_HDR			= "complaintHeader";

	private static final String UIATTR_MEMBERID			= "membertId";

	private static final String UIATTR_PERCOUNT			= "perCount";
	private static final String UIATTR_MAXCOUNT			= "maxCount";



	@Autowired
	ComplaintService service;
	@Autowired
	MemberService memberService;
	@Autowired
	UserService userService;
	@Autowired
	EmailService emailService;
	@Autowired
	CodePropertiesService codePropertiesService;
    @Autowired
    private MessageSource messageSource;



	@InitBinder
	void initBinder( WebDataBinder binder ) {
		DateFormat theFormat = new SimpleDateFormat( "dd MMM yyyy" );
		theFormat.setLenient( false );
		binder.registerCustomEditor( Date.class, new CustomDateEditor( theFormat, true ) );
	}

    @RequestMapping( produces = "text/html" )
    public String showMain( Model uiModel ) {
    	create( uiModel );
    	populateFields( uiModel );
    	return PATH_MAIN;
    }

    @RequestMapping(value = "/list", method = RequestMethod.POST)
    public @ResponseBody ResultList<ComplaintDto> list( @RequestBody ComplaintSearchDto dto ) {

    	MemberModel member = memberService.findCustomerModel( UserUtil.getCurrentUser().getUserId() );

    	dto.setFirstName( member.getFirstName() );
    	dto.setLastName( member.getLastName() );
    	dto.setMobileNo( member.getContact() );

    	return service.search( dto );
    }

    @RequestMapping( value="/form/new", produces = "text/html" )
    public String create( Model uiModel ) {
    	//populateForm( uiModel, null );
    	uiModel.addAttribute( UIATTR_FORM, new ComplaintDto() );
    	uiModel.addAttribute( UIATTR_ACTION, PATH_SAVE );
    	uiModel.addAttribute( UIATTR_LBL_HDR, messageSource.getMessage( "complaint_lbl_create", null, LocaleContextHolder.getLocale() ) );
    	return PATH_FORM;
    }

    @RequestMapping( value="/save", produces="application/json; charset=utf-8", method=RequestMethod.POST )
	public @ResponseBody ControllerResponse save(
			@ModelAttribute(UIATTR_FORM) ComplaintDto dto, 
			BindingResult result ) {

    	MemberModel member = memberService.findCustomerModel( UserUtil.getCurrentUser().getUserId() );
    	ControllerResponse resp = validate( member, dto, result );
		if ( resp.isSuccess() ) {

			dto.setDateTime( new Date().getTime() );
			dto.setEmail( member.getEmail() );
			dto.setMobileNo( member.getContact() );
			dto.setFirstName( member.getFirstName() );
			dto.setLastName( member.getLastName() );
			dto.setGenderCode( null != member.getCustomerProfile().getGender()? member.getCustomerProfile().getGender().getCode() : null );
			dto.setStoreCode( member.getRegisteredStore() );
			dto = service.saveDto( dto, true );
    		resp.setResult( dto );

    		notifySupervisor( dto );
    		resp.setSuccess( true );
		}
		return resp;
	}



    private ControllerResponse validate( MemberModel member, ComplaintDto dto, BindingResult result ) {
    	if ( StringUtils.isBlank( dto.getComplaints() ) ) {
    		result.reject( messageSource.getMessage( "complaint_err_emptycomplaints", null, LocaleContextHolder.getLocale() ) );
    	}
    	if ( null == member.getCustomerProfile().getGender() ) {
    		result.reject( messageSource.getMessage( "complaint_err_emptygender_update", null, LocaleContextHolder.getLocale() ) );
    	}

    	ControllerResponse resp = new ControllerResponse();
    	if ( result.hasErrors() ) {
    		resp.setResult( result.getAllErrors() );
    	}
    	resp.setSuccess( !result.hasErrors() );
    	return resp;
    }

    private void populateFields( Model uiModel ) {
    	uiModel.addAttribute( UIATTR_MEMBERID, UserUtil.getCurrentUser().getUserId() );

    	uiModel.addAttribute( UIATTR_PERCOUNT, Complaint.PER_COUNT );
    	uiModel.addAttribute( UIATTR_MAXCOUNT, Complaint.MESSAGE_COUNT );
    }

    private  class ControllerResponse {
        private boolean itsSuccess;
        private Object itsResult;

        public Boolean isSuccess() {
            return itsSuccess;
        }
        public void setSuccess(boolean inSuccess) {
            this.itsSuccess = inSuccess;
        }
        @SuppressWarnings("unused")
		public Object getResult() {
            return itsResult;
        }
        public void setResult(Object inResult) {
            this.itsResult = inResult;
        }
    }

    private void notifySupervisor( ComplaintDto dto ) {
    	MessageDto email = new MessageDto();
    	email.setMessage( messageSource.getMessage( "complaint_msg_newalert_msg", new String[]{ dto.getTicketNo() }, LocaleContextHolder.getLocale() ) );
    	List<String> emailAddresses = userService.listSupervisorEmails( dto.getStoreCode(), codePropertiesService.getCssCode() );
    	email.setRecipients( emailAddresses.toArray( new String[ emailAddresses.size() ] ) );
    	email.setSubject( messageSource.getMessage( "complaint_msg_newalert_subject", new String[]{ dto.getTicketNo() }, LocaleContextHolder.getLocale() ) );
    	emailService.send( email );
    }

}
