create table CRM_GC_INVENTORY_STOCK (
        id number(19,0) not null,
        CREATED_BY varchar2(255 char),
        CREATED_DATETIME timestamp,
        LAST_UPDATED_BY varchar2(255 char),
        LAST_UPDATED_DATETIME timestamp,
        CREATED_DATE date,
        COMPILED_TIMESTAMP timestamp,
        LOCATION varchar2(255 char),
        TXN_STATUS varchar2(255 char),
        TRANSFER_FROM varchar2(255 char),
        TRANSFER_TO varchar2(255 char),
        INVENTORY_NO number(19,0),
        primary key (id)
    );

alter table CRM_GC_INVENTORY_STOCK 
        add constraint FK_l0yqopc5ejj0f2srxppbqm6tg 
        foreign key (INVENTORY_NO) 
        references CRM_GC_INVENTORY;

INSERT INTO CRM_USER_PERMISSION(CODE, DESCRIPTION, STATUS,ROLE_CATEGORY) 
  VALUES ( 'GC_INV_STOCK','Inventory stock', 'ACTIVE','ROLE_GC' );