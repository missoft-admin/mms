ALTER TABLE CRM_POINTS ADD TXN_OWNER number(19,0);

alter table CRM_POINTS 
	add constraint FK_9yx5tvnepyq8t0ffradnhw8b1 
	foreign key (TXN_OWNER) 
	references CRM_MEMBER;