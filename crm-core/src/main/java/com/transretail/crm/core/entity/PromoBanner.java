package com.transretail.crm.core.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.Type;
import org.joda.time.LocalDate;

import com.transretail.crm.core.entity.enums.PromoImageType;
import com.transretail.crm.core.entity.lookup.LookupDetail;

@Entity
@Table(name="CRM_PROMO_BANNER")
public class PromoBanner {
	@Id
	@Column(name="ID")
	private String id;
	
	@Column(name="IMAGE")
	@Lob
	private byte[] image;
	
	@Column(name="LINK")
	private String link;
	
	@Column(name="START_DATE")
	@Temporal(TemporalType.DATE)
	@Type(type="org.jadira.usertype.dateandtime.joda.PersistentLocalDate")
	private LocalDate startDate;
	
	@Column(name="END_DATE")
	@Temporal(TemporalType.DATE)
	@Type(type="org.jadira.usertype.dateandtime.joda.PersistentLocalDate")
	private LocalDate endDate;
	
	@Column(name="NAME")
	private String name;
	
	@JoinColumn(name="STATUS")
	@ManyToOne
	private LookupDetail status;
	
	@Column(name="TYPE")
	private PromoImageType type;
	
	@Column(name="INFO")
	private String info;
	
	@Column(name="PROD_QUANTITY")
	private String productQuantity;
	
	@Column(name="OLD_PRICE")
	private Double oldPrice;
	
	@Column(name="NEW_PRICE")
	private Double newPrice;
	
	public byte[] getImage() {
		return image;
	}

	public void setImage(byte[] image) {
		this.image = image;
	}

	public String getLink() {
		return link;
	}

	public void setLink(String link) {
		this.link = link;
	}

	public LocalDate getStartDate() {
		return startDate;
	}

	public void setStartDate(LocalDate startDate) {
		this.startDate = startDate;
	}

	public LocalDate getEndDate() {
		return endDate;
	}

	public void setEndDate(LocalDate endDate) {
		this.endDate = endDate;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public LookupDetail getStatus() {
		return status;
	}

	public void setStatus(LookupDetail status) {
		this.status = status;
	}

	public PromoImageType getType() {
		return type;
	}

	public void setType(PromoImageType type) {
		this.type = type;
	}

	public String getProductQuantity() {
		return productQuantity;
	}

	public void setProductQuantity(String productQuantity) {
		this.productQuantity = productQuantity;
	}

	public Double getOldPrice() {
		return oldPrice;
	}

	public void setOldPrice(Double oldPrice) {
		this.oldPrice = oldPrice;
	}

	public Double getNewPrice() {
		return newPrice;
	}

	public void setNewPrice(Double newPrice) {
		this.newPrice = newPrice;
	}

	public String getInfo() {
		return info;
	}

	public void setInfo(String info) {
		this.info = info;
	}
}
