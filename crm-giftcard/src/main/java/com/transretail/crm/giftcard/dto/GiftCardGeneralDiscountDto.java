package com.transretail.crm.giftcard.dto;

import java.math.BigDecimal;

import com.transretail.crm.giftcard.entity.GiftCardGeneralDiscount;

public class GiftCardGeneralDiscountDto {
	private Long id;
	private BigDecimal minPurchase;
	private BigDecimal maxPurchase;
	private BigDecimal discount;
	
	public GiftCardGeneralDiscountDto() {
		
	}
	
	public GiftCardGeneralDiscountDto(GiftCardGeneralDiscount model) {
		if(model == null) {
			return;
		}
		
		this.id = model.getId();
		this.minPurchase = model.getMinPurchase();
		this.maxPurchase = model.getMaxDiscount();
		this.discount = model.getDiscount();
	}
	
	public GiftCardGeneralDiscount toModel(GiftCardGeneralDiscount model) {
		if(model == null) {
			model = new GiftCardGeneralDiscount();
		}
		
		model.setDiscount(discount);
		model.setMinPurchase(minPurchase);
		model.setMaxDiscount(maxPurchase);
		return model;
	}
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public BigDecimal getMinPurchase() {
		return minPurchase;
	}
	public void setMinPurchase(BigDecimal minPurchase) {
		this.minPurchase = minPurchase;
	}
	public BigDecimal getMaxPurchase() {
		return maxPurchase;
	}
	public void setMaxPurchase(BigDecimal maxPurchase) {
		this.maxPurchase = maxPurchase;
	}
	public BigDecimal getDiscount() {
		return discount;
	}
	public void setDiscount(BigDecimal discount) {
		this.discount = discount;
	}
}