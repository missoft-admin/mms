-- New inventory location column for user model
ALTER TABLE CRM_USER ADD COLUMN INVENTORY_LOCATION VARCHAR(15);
