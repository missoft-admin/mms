<%@include file="../common/taglibs.jsp" %>


<div id="content_promotionRemarks" class="modal hide  nofly modal-dialog">

  <div class="modal-content">
  <c:url var="url_listPromotions" value="/promotion"/>
  <c:url var="url_action" value="/promotion/process/save" />
  <form:form id="approvalRemarkForm" name="approvalRemark" modelAttribute="approvalRemark" action="${url_action}" data-url="${url_listPromotions}"  class="modal-form form-horizontal">

    <div class="modal-header">
      <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
      <h4><spring:message code="promotion_prop_approval"/></h4>
    </div>

    <div class="modal-body">      
      <div class="data-sub-content">
        <fieldset>
          <div class="page-header3">
            <div><h4><spring:message code="remarks_label"/></h4></div>
            <div><h6><spring:message code="marketing_promotion" />: <c:out value="${promotion.name}" /></h6></div>
          </div>
        </fieldset>        
      </div>

      <c:if test="${not empty promotionRemarks}">
      <div class="data-list-content">
        <table class="table table-bordered table-striped">
          <thead>
            <tr>
              <th><spring:message code="remarks_prop_created" /></th>
              <th><spring:message code="remarks_prop_createUser" /></th>
              <th><spring:message code="remarks_prop_remarks" /></th>
              <th><spring:message code="remarks_prop_status" /></th>
            </tr>
          </thead>
          <tbody>            
            <c:forEach var="remark" items="${promotionRemarks}">
            <tr>
              <td class="table-data-01"><c:out value="${remark.created}" /></td>
              <td><c:out value="${remark.createUser}" /></td>
              <td><c:out value="${remark.remarks}" /></td>
              <td><c:out value="${remark.dispStatus}" /></td>
            </tr>
            </c:forEach>
          </tbody>
        </table>
      </div>        
      </c:if>

      <%-- <div class="control-group">
        <label for="remarks" class="control-label"><spring:message code="remark_prop_remark" /></label>
        <div class="controls"><form:textarea path="remarks" id="remarks"/></div>
      </div> --%>
    </div>

    <%-- <div class="modal-footer">
      <c:if test="${promotion.status ne promotionStatuses[0].code}">
        <input id="promotionRemarksForm_approveButton" type="submit"  value="<spring:message code="label_${promotionStatuses[0].code}"/>" class="promotionStatus btn btn-primary" data-value="${promotionStatuses[0].code}" />
        <input id="promotionRemarksForm_rejectButton" type="submit"  value="<spring:message code="label_${promotionStatuses[1].code}"/>"  class="promotionStatus btn btn-primary" data-value="${promotionStatuses[1].code}" />
      </c:if>
      <c:if test="${promotion.status eq promotionStatuses[0].code}">
        <input id="promotionRemarksForm_disableButton" type="submit"  value="<spring:message code="label_${promotionStatuses[2].code}"/>"  class="promotionStatus btn btn-primary" data-value="${promotionStatuses[2].code}" />
      </c:if>
	    <input id="promotionRemarksForm_cancelButton" type="button"  value="<spring:message code="label_cancel"/>" class="btn" data-dismiss="modal" />
    </div>

    <form:hidden path="modelId" id="modelId"/>
    <form:hidden path="status" id="status"/> --%>

  </form:form>
  </div>


  <script src="<c:url value="/js/viewspecific/promotion/promotionRemarks.js"/>" type='text/javascript'><![CDATA[&nbsp; ]]></script>


</div>