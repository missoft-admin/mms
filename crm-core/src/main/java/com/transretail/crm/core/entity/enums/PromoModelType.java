package com.transretail.crm.core.entity.enums;


public enum PromoModelType {

	PROGRAM, CAMPAIGN, PROMOTION

}
