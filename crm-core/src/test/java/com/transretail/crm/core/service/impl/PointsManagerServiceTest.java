package com.transretail.crm.core.service.impl;

import com.google.common.collect.Lists;
import com.transretail.crm.core.dto.PointsDto;
import com.transretail.crm.core.dto.PointsSearchDto;
import com.transretail.crm.core.dto.PosTxItemDto;
import com.transretail.crm.core.dto.ServiceDto;
import com.transretail.crm.core.entity.*;
import com.transretail.crm.core.entity.embeddable.CustomerProfile;
import com.transretail.crm.core.entity.enums.*;
import com.transretail.crm.core.entity.lookup.LookupDetail;
import com.transretail.crm.core.entity.lookup.LookupHeader;
import com.transretail.crm.core.repo.*;
import com.transretail.crm.core.security.model.impl.CustomSecurityUserDetailsImpl;
import com.transretail.crm.core.service.MemberService;
import com.transretail.crm.core.service.PointsTxnManagerService;
import com.transretail.crm.core.service.TransactionAuditService;
import com.transretail.crm.core.util.generator.IdGeneratorService;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Configurable;
import org.springframework.security.authentication.TestingAuthenticationToken;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.context.SecurityContextImpl;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;

import static org.junit.Assert.assertEquals;

@Configurable
@Transactional
@ContextConfiguration(locations = "classpath*:/META-INF/spring/applicationContext*.xml")
@RunWith(SpringJUnit4ClassRunner.class)
public class PointsManagerServiceTest {

	@Autowired
	MemberService memberService;

	@Autowired
	PointsTxnManagerService pointsManagerService;

	@Autowired
	MemberRepo memberRepo;

	@Autowired
	UserRepo userRepo;

	@Autowired
	PointsTxnModelRepo pointsTxnModelRepo;

	@Autowired
	ApprovalMatrixRepo approvalMatrixRepo;

	@Autowired
	IdGeneratorService customIdGeneratorService;

    @Autowired
    private LookupHeaderRepo lookupHeaderRepo;
    @Autowired
    private LookupDetailRepo lookupDetailRepo;

    @Autowired
    TransactionAuditService transactionAuditService;

    @Autowired
    PosTransactionRepo posTransactionRepo;

    @Autowired
    PromotionRepo promotionRepo;
    @Autowired
    private CrmIdRefRepo crmIdRefRepo;

	// @Before

	/*
	 * @AfterClass public static void removeCurrentUser() {
	 * SecurityContextHolder.clearContext(); }
	 */

	@Before
	public void setUp() {
	}

	@After
	public void tearDown() throws Exception {

		pointsTxnModelRepo.deleteAll();
		memberRepo.deleteAll();
		userRepo.deleteAll();
		SecurityContextHolder.clearContext();
	}


    @Test
    public void testRemoveReturnItems() {


        List < PosTxItem > orderItems = new ArrayList<PosTxItem>();
        List < PosTxItemDto > returnedList = new ArrayList<PosTxItemDto>();

        PosTxItem item = new PosTxItem();
        item.setId("1");
        item.setProductId("111");
        orderItems.add(item);
        item = new PosTxItem();
        item.setId("2");
        item.setProductId("222");
        orderItems.add(item);

        PosTxItemDto itemDto = new PosTxItemDto();
        itemDto.setProductId("222");
        returnedList.add(itemDto);


        List<PosTxItemDto> result = pointsManagerService.removeReturnedItems(orderItems, returnedList);

        Assert.assertEquals(1, result.size());
        Assert.assertEquals(result.get(0).getProductId(),"111");

    }




	@Test
	public void testProcessPointsForInactiveMember() {

		MemberModel memberModel = new MemberModel();
		memberModel.setEnabled(true);
		memberModel.setUsername("test");
		memberModel.setPassword("1234");
		memberModel.setTotalPoints(100D); // starting points
		memberModel.setAccountId("1");
		memberModel.setContact("1");
		memberModel.setPin("1");
		memberModel.setAccountStatus(MemberStatus.TERMINATED);

        CustomerProfile customerProfile = new CustomerProfile();
        customerProfile.setBirthdate(new Date());
        memberModel.setCustomerProfile(customerProfile);

		MemberModel member = memberRepo.save(memberModel);

		PointsTxnModel points = new PointsTxnModel();
		points.setId(customIdGeneratorService.generateId("TEST"));
		points.setMemberModel(member);

		points.setTransactionNo("1");
		points.setTransactionType(PointTxnType.EARN);
		points.setTransactionPoints(10D);
		Assert.assertFalse(pointsManagerService.processPoints(points, null));
	}

	@Test
	public void testProcessPointsAreEarned() {

		MemberModel memberModel = new MemberModel();
		memberModel.setEnabled(true);
		memberModel.setUsername("test");
		memberModel.setPassword("1234");
		memberModel.setTotalPoints(100D); // starting points
		memberModel.setAccountId("1");
		memberModel.setContact("1");
		memberModel.setPin("1");

        CustomerProfile customerProfile = new CustomerProfile();
        customerProfile.setBirthdate(new Date());
        memberModel.setCustomerProfile(customerProfile);

		MemberModel member = memberRepo.save(memberModel);

		PointsTxnModel points = new PointsTxnModel();
		points.setId(customIdGeneratorService.generateId("TEST"));
		points.setMemberModel(member);

		// test if points are earned
		points.setTransactionNo("1");
		points.setTransactionType(PointTxnType.EARN);
		points.setTransactionPoints(10D);
		points.setTransactionDateTime(new Date());
		Assert.assertTrue(pointsManagerService.processPoints(points, null));
		Assert.assertEquals(new Double(110D),
				pointsManagerService.retrieveTotalPoints(member));

		List<PointsTxnModel> details = pointsManagerService
				.retrievePointsByTransactionNo("1");

		Assert.assertNotNull(details);
		Assert.assertEquals(PointTxnType.EARN, details.get(0)
				.getTransactionType());

		member.setAccountStatus(MemberStatus.TERMINATED);
		member = memberRepo.save(member);
		points.setMemberModel(member);
		Assert.assertFalse(pointsManagerService.processPoints(points, null));
	}

	@Test
	public void testProcessPointsAreRedeemed() {

		MemberModel memberModel = new MemberModel();
		memberModel.setEnabled(true);
		memberModel.setUsername("test");
		memberModel.setPassword("1234");
		memberModel.setTotalPoints(100D); // starting points
		memberModel.setAccountId("2");
		memberModel.setContact("2");
		memberModel.setPin("1");

        CustomerProfile customerProfile = new CustomerProfile();
        customerProfile.setBirthdate(new Date());
        memberModel.setCustomerProfile(customerProfile);

		MemberModel member = memberRepo.save(memberModel);

		PointsTxnModel points = new PointsTxnModel();
		points.setId(customIdGeneratorService.generateId("TEST"));
		points.setMemberModel(member);

		// test if points are deducted
		points.setTransactionNo("2");
		points.setTransactionType(PointTxnType.REDEEM);
		points.setTransactionPoints(5D);
		Assert.assertTrue(pointsManagerService.processPoints(points, null));
		Assert.assertEquals(new Double(95D),
				pointsManagerService.retrieveTotalPoints(member));
		Assert.assertNotNull(pointsManagerService
				.retrievePointsByTransactionNo("2"));

		member.setAccountStatus(MemberStatus.TERMINATED);
		member = memberRepo.save(member);
		points.setMemberModel(member);
		Assert.assertFalse(pointsManagerService.processPoints(points, null));
	}

	@Test
	public void testProcessPointsAreAdjustedAdd() {

		MemberModel memberModel = new MemberModel();
		memberModel.setEnabled(true);
		memberModel.setUsername("test");
		memberModel.setPassword("1234");
		memberModel.setTotalPoints(100D); // starting points
		memberModel.setAccountId("3");
		memberModel.setContact("3");
		memberModel.setPin("1");

        CustomerProfile customerProfile = new CustomerProfile();
        customerProfile.setBirthdate(new Date());
        memberModel.setCustomerProfile(customerProfile);

		MemberModel member = memberRepo.save(memberModel);

		PointsTxnModel points = new PointsTxnModel();
		points.setId(customIdGeneratorService.generateId("TEST"));
		points.setMemberModel(member);

		// test if points are adjusted (addition)
		points.setTransactionNo("3");
		points.setTransactionType(PointTxnType.ADJUST);
		points.setTransactionPoints(7D);
		points.setTransactionDateTime(new Date());
		Assert.assertTrue(pointsManagerService.processPoints(points, null));
		Assert.assertEquals(new Double(107D),
				pointsManagerService.retrieveTotalPoints(member));
		Assert.assertNotNull(pointsManagerService
				.retrievePointsByTransactionNo("3"));

		member.setAccountStatus(MemberStatus.TERMINATED);
		member = memberRepo.save(member);
		points.setMemberModel(member);
		Assert.assertFalse(pointsManagerService.processPoints(points, null));

	}

	@Test
	public void testProcessPointsAreAdjustedDeduct() {

		MemberModel memberModel = new MemberModel();
		memberModel.setEnabled(true);
		memberModel.setUsername("test");
		memberModel.setPassword("1234");
		memberModel.setTotalPoints(100D); // starting points
		memberModel.setAccountId("4");
		memberModel.setContact("4");
		memberModel.setPin("1");

        CustomerProfile customerProfile = new CustomerProfile();
        customerProfile.setBirthdate(new Date());
        memberModel.setCustomerProfile(customerProfile);

		MemberModel member = memberRepo.save(memberModel);

		PointsTxnModel points = new PointsTxnModel();
		points.setId(customIdGeneratorService.generateId("TEST"));
		points.setMemberModel(member);

		// test if points are adjusted (deduction)
		points.setTransactionNo("4");
		points.setTransactionType(PointTxnType.ADJUST);
		points.setTransactionPoints(-8D);
		Assert.assertTrue(pointsManagerService.processPoints(points, null));
		Assert.assertEquals(new Double(92D),
				pointsManagerService.retrieveTotalPoints(member));
		Assert.assertNotNull(pointsManagerService
				.retrievePointsByTransactionNo("4"));

		member.setAccountStatus(MemberStatus.TERMINATED);
		member = memberRepo.save(member);
		points.setMemberModel(member);
		Assert.assertFalse(pointsManagerService.processPoints(points, null));

	}

	@Test
	public void testProcessPointsWhenReturned() {

		MemberModel memberModel = new MemberModel();
		memberModel.setEnabled(true);
		memberModel.setUsername("test");
		memberModel.setPassword("1234");
		memberModel.setTotalPoints(100D); // starting points
		memberModel.setAccountId("5");
		memberModel.setContact("5");
		memberModel.setPin("1");

        CustomerProfile customerProfile = new CustomerProfile();
        customerProfile.setBirthdate(new Date());
        memberModel.setCustomerProfile(customerProfile);

		MemberModel member = memberRepo.save(memberModel);

		PointsTxnModel points = new PointsTxnModel();
		points.setId(customIdGeneratorService.generateId("TEST"));
		points.setMemberModel(member);

		// test if points are deducted after return
		points.setTransactionNo("5");
		points.setTransactionType(PointTxnType.RETURN);
		points.setTransactionPoints(20D);
		Assert.assertTrue(pointsManagerService.processPoints(points, null));
		Assert.assertEquals(new Double(80D),
				pointsManagerService.retrieveTotalPoints(member));
		Assert.assertNotNull(pointsManagerService
				.retrievePointsByTransactionNo("5"));

	}

	@Test
	public void testProcessPointsWhenReturnedNegativeValue() {

		MemberModel memberModel = new MemberModel();
		memberModel.setEnabled(true);
		memberModel.setUsername("test");
		memberModel.setPassword("1234");
		memberModel.setTotalPoints(100D); // starting points
		memberModel.setAccountId("6");
		memberModel.setContact("6");
		memberModel.setPin("1");

        CustomerProfile customerProfile = new CustomerProfile();
        customerProfile.setBirthdate(new Date());
        memberModel.setCustomerProfile(customerProfile);

		MemberModel member = memberRepo.save(memberModel);

		PointsTxnModel points = new PointsTxnModel();
		points.setId(customIdGeneratorService.generateId("TEST"));
		points.setMemberModel(member);

		// test if points are deducted after return using negative value
		points.setTransactionNo("6");
		points.setTransactionType(PointTxnType.RETURN);
		points.setTransactionPoints(-15D);
		Assert.assertTrue(pointsManagerService.processPoints(points, null));
		Assert.assertEquals(new Double(85L),
				pointsManagerService.retrieveTotalPoints(member));
		Assert.assertNotNull(pointsManagerService
				.retrievePointsByTransactionNo("6"));

	}

	@Test
	public void testProcessPointsWhenVoid() {

		MemberModel memberModel = new MemberModel();
		memberModel.setEnabled(true);
		memberModel.setUsername("test");
		memberModel.setPassword("1234");
		memberModel.setTotalPoints(100D); // starting points
		memberModel.setAccountId("7");
		memberModel.setContact("7");
		memberModel.setPin("1");

        CustomerProfile customerProfile = new CustomerProfile();
        customerProfile.setBirthdate(new Date());
        memberModel.setCustomerProfile(customerProfile);

		MemberModel member = memberRepo.save(memberModel);

		PointsTxnModel points = new PointsTxnModel();
		points.setId(customIdGeneratorService.generateId("TEST"));
		points.setMemberModel(member);

		// test if points are deducted after void
		points.setTransactionNo("7");
		points.setTransactionType(PointTxnType.VOID);
		points.setTransactionPoints(25D);
		points.setTransactionDateTime(new Date());
		Assert.assertTrue(pointsManagerService.processPoints(points, null));
		Assert.assertEquals(new Double(75D),
				pointsManagerService.retrieveTotalPoints(member));
		Assert.assertNotNull(pointsManagerService
				.retrievePointsByTransactionNo("7"));

        // test if points are deducted after void  using negative value
        points.setTransactionNo("7");
        points.setTransactionType(PointTxnType.VOID);
        points.setTransactionPoints(-25D);
        Assert.assertTrue(pointsManagerService.processPoints(points, null));
        Assert.assertEquals(new Double(50L),
                pointsManagerService.retrieveTotalPoints(member));
        Assert.assertNotNull(pointsManagerService
                .retrievePointsByTransactionNo("7"));

	}

	@Test
	public void testRetrieveTotalPoints() {

		MemberModel memberModel = new MemberModel();
		memberModel.setEnabled(true);
		memberModel.setUsername("test2");
		memberModel.setPassword("1234");
		memberModel.setTotalPoints(100D); // starting points
		memberModel.setAccountId("8");
		memberModel.setContact("8");
		memberModel.setPin("1");

        CustomerProfile customerProfile = new CustomerProfile();
        customerProfile.setBirthdate(new Date());
        memberModel.setCustomerProfile(customerProfile);

		MemberModel member = memberRepo.save(memberModel);

		Assert.assertEquals(new Double(100D),
				pointsManagerService.retrieveTotalPoints(member));

	}

	@Test
	public void testRetrievePointsByTransactionNo() {
		MemberModel memberModel = new MemberModel();
		memberModel.setEnabled(true);
		memberModel.setUsername("test3");
		memberModel.setPassword("1234");
		memberModel.setTotalPoints(100D); // starting points
		memberModel.setAccountId("9");
		memberModel.setContact("9");
		memberModel.setPin("1");

        CustomerProfile customerProfile = new CustomerProfile();
        customerProfile.setBirthdate(new Date());
        memberModel.setCustomerProfile(customerProfile);

		MemberModel member = memberRepo.save(memberModel);

		PointsTxnModel points = new PointsTxnModel();
		points.setId(customIdGeneratorService.generateId("TEST"));
		points.setTransactionNo("11");
		points.setTransactionDateTime(new Date());
		points.setTransactionType(PointTxnType.EARN);
		points.setMemberModel(member);

		Assert.assertTrue(pointsManagerService.processPoints(points, null));

		Assert.assertNotNull(pointsManagerService
				.retrievePointsByTransactionNo("11"));

	}

	@Test
	public void testRetrievePoints() {
		MemberModel memberModel = new MemberModel();
		memberModel.setEnabled(true);
		memberModel.setUsername("test6");
		memberModel.setPassword("1234");
		memberModel.setTotalPoints(100D);
		memberModel.setAccountId("10");
		memberModel.setContact("10");
		memberModel.setPin("1");

        CustomerProfile customerProfile = new CustomerProfile();
        customerProfile.setBirthdate(new Date());
        memberModel.setCustomerProfile(customerProfile);

		MemberModel member = memberRepo.save(memberModel);

		PointsTxnModel points = new PointsTxnModel();
		points.setId(customIdGeneratorService.generateId("TEST"));
		points.setMemberModel(member);

		// test if points are earned
		points.setTransactionNo("111");
		points.setTransactionType(PointTxnType.EARN);
		points.setTransactionPoints(10D);
		points.setTransactionDateTime(new Date());
		Assert.assertTrue(pointsManagerService.processPoints(points, null));
		Assert.assertEquals(
				pointsManagerService.retrieveTotalPoints(member.getAccountId()),
				new Double(110D));
		Assert.assertNotNull(pointsManagerService
				.retrievePointsByTransactionNo("111"));

		List<PointsTxnModel> details = pointsManagerService
				.retrievePointsByTransactionNo("111");
		Assert.assertEquals(PointTxnType.EARN, details.get(0)
				.getTransactionType());

		// test if points are deducted
		points.setTransactionNo("2222");
		points.setTransactionType(PointTxnType.REDEEM);
		points.setTransactionPoints(5D);
		Assert.assertTrue(pointsManagerService.processPoints(points, null));
		Assert.assertEquals(new Double(105D),
				pointsManagerService.retrieveTotalPoints(member.getAccountId()));
		Assert.assertNotNull(pointsManagerService
				.retrievePointsByTransactionNo("2"));

		List<PointsTxnModel> pointDetails = pointsManagerService
				.retrievePoints(member);

		Assert.assertNotNull(pointDetails);


        //test with transactionNo and type
        points.setId(customIdGeneratorService.generateId("TEST"));
        points.setTransactionNo("333");
        points.setTransactionType(PointTxnType.EARN);
        points.setTransactionPoints(5D);
        Assert.assertTrue(pointsManagerService.processPoints(points, null));
        pointDetails = pointsManagerService
                .retrievePoints("333", PointTxnType.EARN);


        Assert.assertNotNull(pointDetails);

        Assert.assertEquals(1, pointDetails.size());


        //test with transactionNo and wrong type
        pointDetails = pointsManagerService
                .retrievePoints("333", PointTxnType.REWARD);
        Assert.assertNotNull(pointDetails);
        Assert.assertEquals(0, pointDetails.size());


        // Assert.assertEquals(2, pointDetails.size());
	}

	//@Test
	public void testRetrievePointsByDate() {

		Calendar fromCal = Calendar.getInstance();
		fromCal.set(Calendar.YEAR, 2010);
		fromCal.set(Calendar.MONTH, 1);

		Calendar toCal = Calendar.getInstance();
		toCal.set(Calendar.YEAR, 2012);
		toCal.set(Calendar.MONTH, 11);

		Calendar inclusiveCal = Calendar.getInstance();
		inclusiveCal.set(Calendar.YEAR, 2011);
		inclusiveCal.set(Calendar.MONTH, 3);

		Calendar inclusiveCal2 = Calendar.getInstance();
		inclusiveCal2.set(Calendar.YEAR, 2010);
		inclusiveCal2.set(Calendar.MONTH, 4);

		Calendar exclusiveCal = Calendar.getInstance();
		exclusiveCal.set(Calendar.YEAR, 2009);
		exclusiveCal.set(Calendar.MONTH, 4);

		Date fromDate = fromCal.getTime();
		Date toDate = toCal.getTime();
		Date inclusiveDate = inclusiveCal.getTime();
		Date inclusiveDate2 = inclusiveCal2.getTime();
		Date exclusiveDate = exclusiveCal.getTime();

		MemberModel memberModel = new MemberModel();
		memberModel.setEnabled(true);
		memberModel.setUsername("test6");
		memberModel.setPassword("1234");
		memberModel.setTotalPoints(100D);
		memberModel.setAccountId("10");
		memberModel.setContact("10");
		memberModel.setPin("1");

        CustomerProfile customerProfile = new CustomerProfile();
        customerProfile.setBirthdate(new Date());
        memberModel.setCustomerProfile(customerProfile);

		MemberModel member = memberRepo.save(memberModel);

		PointsTxnModel points = new PointsTxnModel();
		points.setMemberModel(member);
		points.setTransactionDateTime(inclusiveDate);
		Assert.assertNotNull(pointsManagerService.savePointsTxnModel(points));

		points = new PointsTxnModel();
		points.setMemberModel(member);
		points.setTransactionDateTime(inclusiveDate2);
		Assert.assertNotNull(pointsManagerService.savePointsTxnModel(points));

		points = new PointsTxnModel();
		points.setMemberModel(member);
		points.setTransactionDateTime(exclusiveDate);
		Assert.assertNotNull(pointsManagerService.savePointsTxnModel(points));

		Assert.assertEquals(2,
				pointsManagerService.retrievePoints("10", fromDate, toDate)
						.size());

	}

	@Test
	public void textSynchronizePoints() {

		MemberModel theMember = new MemberModel();
		theMember.setAccountStatus(MemberStatus.ACTIVE);
		theMember.setEnabled(true);
		theMember.setAccountId("ACCT001");
		theMember.setUsername("ACCT001");
		theMember.setContact("10");
		theMember.setPin("1");
		theMember.setTotalPoints(100D);

        CustomerProfile customerProfile = new CustomerProfile();
        customerProfile.setBirthdate(new Date());
        theMember.setCustomerProfile(customerProfile);

		memberRepo.save(theMember);

		PointsTxnModel thePoint = new PointsTxnModel();
		thePoint.setId(customIdGeneratorService.generateId("TEST"));
		thePoint.setTransactionPoints(125D);
		thePoint.setMemberModel(theMember);
		pointsTxnModelRepo.save(thePoint);

		thePoint = new PointsTxnModel();
		thePoint.setId(customIdGeneratorService.generateId("TEST2"));
		thePoint.setTransactionPoints(-15D);
		thePoint.setMemberModel(theMember);
		pointsTxnModelRepo.save(thePoint);

		Assert.assertEquals(100L, theMember.getTotalPoints().longValue());
		double theUpdatedPoints = pointsManagerService
				.synchronizePointsTxn(theMember);
		Assert.assertEquals(110D, theUpdatedPoints, 0);
	}

	@SuppressWarnings("unchecked")
	@Test
	public void testRetrievePointsByTxnType() {

		setupPointsAdjustTest();

		ServiceDto theSvcResp = pointsManagerService
				.retrievePointsByTxnType(PointTxnType.ADJUST);
		Assert.assertTrue(4 == ((List<PointsDto>) theSvcResp.getReturnObj())
				.size());
	}

	@SuppressWarnings("unchecked")
	@Test
	public void testRetrieveAdjustPointsForApprovalWithAuth() {

		setupPointsAdjustTest();

		setupApproverAmount(100D);
		ServiceDto theSvcResp = pointsManagerService
				.retrieveAdjustPointsForApprovalWithAuth();
		Assert.assertTrue(1 == ((List<PointsDto>) theSvcResp.getReturnObj())
				.size());

		setupApproverAmount(50D);
		theSvcResp = pointsManagerService
				.retrieveAdjustPointsForApprovalWithAuth();
		Assert.assertTrue(0 == ((List<PointsDto>) theSvcResp.getReturnObj())
				.size());

	}


    @Test
    public void testRetrieveValidPaymentTypesWithoutFilter() {


        String paymentCash = "PTYP001";
        String amountCash = "100.00";

        String paymentVoucher = "PTYP002";
        String amountVoucher = "200.00";

        String paymentEft = "PTYP008";
        String amountEft = "300.00";

        String card = "12345";


        //single cash payment
        List<Map> results = pointsManagerService.retrieveValidPaymentTypes(paymentCash,  amountCash, null,
                false, null);

        Assert.assertNotNull(results);
        Assert.assertEquals(1, results.size());

        Map map = results.get(0);
        Assert.assertEquals(paymentCash, map.get(PointsTxnManagerService.PAYMENT_TYPE_KEY));
        Assert.assertEquals(new Double(amountCash), map.get(PointsTxnManagerService.AMOUNT_KEY));


        //multiple payments without card

        results = pointsManagerService.retrieveValidPaymentTypes(paymentCash + ":" + paymentVoucher,  amountCash + ":" + amountVoucher, null,
                false, null);

        Assert.assertNotNull(results);
        Assert.assertEquals(2, results.size());

        map = results.get(0);
        Assert.assertEquals(paymentCash, map.get(PointsTxnManagerService.PAYMENT_TYPE_KEY));
        Assert.assertEquals(new Double(amountCash), map.get(PointsTxnManagerService.AMOUNT_KEY));

        map = results.get(1);
        Assert.assertEquals(paymentVoucher, map.get(PointsTxnManagerService.PAYMENT_TYPE_KEY));
        Assert.assertEquals(new Double(amountVoucher), map.get(PointsTxnManagerService.AMOUNT_KEY));


        //multiple payments with card
        results = pointsManagerService.retrieveValidPaymentTypes(paymentCash + ":" + paymentEft,  amountCash + ":" + amountEft, card,
                false, null);

        Assert.assertNotNull(results);
        Assert.assertEquals(2, results.size());

        map = results.get(0);
        Assert.assertEquals(paymentCash, map.get(PointsTxnManagerService.PAYMENT_TYPE_KEY));
        Assert.assertEquals(new Double(amountCash), map.get(PointsTxnManagerService.AMOUNT_KEY));

        map = results.get(1);
        Assert.assertEquals(paymentEft, map.get(PointsTxnManagerService.PAYMENT_TYPE_KEY));
        Assert.assertEquals(new Double(amountEft), map.get(PointsTxnManagerService.AMOUNT_KEY));
        Assert.assertEquals(card, map.get(PointsTxnManagerService.CARD_NUMBER_KEY));

    }


    @Test
    public void testRetrieveValidPaymentTypesWithFilter() {


        String paymentCash = "PTYP001";
        String amountCash = "100.00";

        String paymentVoucher = "PTYP002";
        String amountVoucher = "200.00";

        String paymentEft = "PTYP008";
        String amountEft = "300.00";

        String paymentInstallmentt = "PTYP007";
        String amountInstallment = "300.00";

        String card = "123456********";

        String invalidCard = "223456********";

        String headerMediaCode = "APM001";
        String headerCardFilterCode = "APM002";



        LookupHeader mediaHeader = new LookupHeader(headerMediaCode);
        lookupHeaderRepo.saveAndFlush(mediaHeader);

        LookupHeader cardFilterHeader = new LookupHeader(headerCardFilterCode);
        lookupHeaderRepo.saveAndFlush(cardFilterHeader);

        LookupDetail mediaCashDetail = new LookupDetail("EAPM001");
        mediaCashDetail.setHeader(mediaHeader);
        mediaCashDetail.setStatus(Status.ACTIVE);
        mediaCashDetail.setDescription(paymentCash);
        lookupDetailRepo.saveAndFlush(mediaCashDetail);


        LookupDetail mediaGcDetail = new LookupDetail("EAPM002");
        mediaGcDetail.setHeader(mediaHeader);
        mediaGcDetail.setStatus(Status.ACTIVE);
        mediaGcDetail.setDescription(paymentVoucher);
        lookupDetailRepo.saveAndFlush(mediaGcDetail);


        LookupDetail mediaEftDetail = new LookupDetail("EAPM003");
        mediaEftDetail.setHeader(mediaHeader);
        mediaEftDetail.setStatus(Status.ACTIVE);
        mediaEftDetail.setDescription(paymentEft);
        lookupDetailRepo.saveAndFlush(mediaEftDetail);


        LookupDetail eftFilterDetail1 = new LookupDetail("ECF001");
        eftFilterDetail1.setHeader(cardFilterHeader);
        eftFilterDetail1.setStatus(Status.ACTIVE);
        eftFilterDetail1.setDescription("123456");

        LookupDetail eftFilterDetail2 = new LookupDetail("ECF002");
        eftFilterDetail2.setHeader(cardFilterHeader);
        eftFilterDetail2.setStatus(Status.ACTIVE);
        eftFilterDetail2.setDescription("1*");

        LookupDetail eftFilterDetail3 = new LookupDetail("ECF003");
        eftFilterDetail3.setHeader(cardFilterHeader);
        eftFilterDetail3.setStatus(Status.ACTIVE);
        eftFilterDetail3.setDescription("333*");




        //single cash payment
        List<Map> results = pointsManagerService.retrieveValidPaymentTypes(paymentCash,  amountCash, null,
                true, null);

        Assert.assertNotNull(results);
        Assert.assertEquals(1, results.size());

        Map map = results.get(0);
        Assert.assertEquals(paymentCash, map.get(PointsTxnManagerService.PAYMENT_TYPE_KEY));
        Assert.assertEquals(new Double(amountCash), map.get(PointsTxnManagerService.AMOUNT_KEY));


        //multiple payments without card

        results = pointsManagerService.retrieveValidPaymentTypes(paymentCash + ":" + paymentVoucher,  amountCash + ":" + amountVoucher, null,
                true, null);

        Assert.assertNotNull(results);
        Assert.assertEquals(2, results.size());

        map = results.get(0);
        Assert.assertEquals(paymentCash, map.get(PointsTxnManagerService.PAYMENT_TYPE_KEY));
        Assert.assertEquals(new Double(amountCash), map.get(PointsTxnManagerService.AMOUNT_KEY));

        map = results.get(1);
        Assert.assertEquals(paymentVoucher, map.get(PointsTxnManagerService.PAYMENT_TYPE_KEY));
        Assert.assertEquals(new Double(amountVoucher), map.get(PointsTxnManagerService.AMOUNT_KEY));


        //multiple payments without card but with non-approved paynment (Installment)

        results = pointsManagerService.retrieveValidPaymentTypes(paymentCash + ":" + paymentInstallmentt,  amountCash + ":" + amountInstallment, null,
                true, null);

        Assert.assertNotNull(results);
        Assert.assertEquals(1, results.size());

        map = results.get(0);
        Assert.assertEquals(paymentCash, map.get(PointsTxnManagerService.PAYMENT_TYPE_KEY));
        Assert.assertEquals(new Double(amountCash), map.get(PointsTxnManagerService.AMOUNT_KEY));


        //single card payment with all card valid (eft defined as valid payment media and card filter defined)
        results = pointsManagerService.retrieveValidPaymentTypes(paymentEft,  amountEft, card,
                true, null);

        Assert.assertNotNull(results);
        Assert.assertEquals(1, results.size());

        map = results.get(0);
        Assert.assertEquals(paymentEft, map.get(PointsTxnManagerService.PAYMENT_TYPE_KEY));
        Assert.assertEquals(new Double(amountEft), map.get(PointsTxnManagerService.AMOUNT_KEY));



        //multiple card payment with all card valid (eft defined as valid payment media and card filter defined)
        results = pointsManagerService.retrieveValidPaymentTypes(paymentCash + ":" + paymentEft,  amountCash + ":" + amountEft, card,
                true, null);

        Assert.assertNotNull(results);
        Assert.assertEquals(2, results.size());

        map = results.get(0);
        Assert.assertEquals(paymentCash, map.get(PointsTxnManagerService.PAYMENT_TYPE_KEY));
        Assert.assertEquals(new Double(amountCash), map.get(PointsTxnManagerService.AMOUNT_KEY));

        map = results.get(1);
        Assert.assertEquals(paymentEft, map.get(PointsTxnManagerService.PAYMENT_TYPE_KEY));
        Assert.assertEquals(new Double(amountEft), map.get(PointsTxnManagerService.AMOUNT_KEY));
        Assert.assertEquals(card, map.get(PointsTxnManagerService.CARD_NUMBER_KEY));


        //single card payment with filter
        lookupDetailRepo.saveAndFlush(eftFilterDetail1);
        lookupDetailRepo.saveAndFlush(eftFilterDetail2);
        lookupDetailRepo.saveAndFlush(eftFilterDetail3);

        results = pointsManagerService.retrieveValidPaymentTypes(paymentEft,  amountEft, card,
                true, null);

        Assert.assertNotNull(results);
        Assert.assertEquals(1, results.size());

        map = results.get(0);
        Assert.assertEquals(paymentEft, map.get(PointsTxnManagerService.PAYMENT_TYPE_KEY));
        Assert.assertEquals(new Double(amountEft), map.get(PointsTxnManagerService.AMOUNT_KEY));



        //multiple payments with valid card filter
        results = pointsManagerService.retrieveValidPaymentTypes(paymentCash + ":" + paymentEft,  amountCash + ":" + amountEft, card,
                true, null);

        Assert.assertNotNull(results);
        Assert.assertEquals(2, results.size());

        map = results.get(0);
        Assert.assertEquals(paymentCash, map.get(PointsTxnManagerService.PAYMENT_TYPE_KEY));
        Assert.assertEquals(new Double(amountCash), map.get(PointsTxnManagerService.AMOUNT_KEY));

        map = results.get(1);
        Assert.assertEquals(paymentEft, map.get(PointsTxnManagerService.PAYMENT_TYPE_KEY));
        Assert.assertEquals(new Double(amountEft), map.get(PointsTxnManagerService.AMOUNT_KEY));
        Assert.assertEquals(card, map.get(PointsTxnManagerService.CARD_NUMBER_KEY));


        //single invalid card payment with filter
        results = pointsManagerService.retrieveValidPaymentTypes(paymentEft,  amountEft, invalidCard,
                true, null);

        Assert.assertNotNull(results);
        Assert.assertEquals(0, results.size());


        //multiple payment with invalid card filter
        results = pointsManagerService.retrieveValidPaymentTypes(paymentCash + ":" + paymentEft,  amountCash + ":" + amountEft, invalidCard,
                true, null);

        Assert.assertNotNull(results);
        Assert.assertEquals(1, results.size());

        map = results.get(0);
        Assert.assertEquals(paymentCash, map.get(PointsTxnManagerService.PAYMENT_TYPE_KEY));
        Assert.assertEquals(new Double(amountCash), map.get(PointsTxnManagerService.AMOUNT_KEY));


    }






	private void setupPointsAdjustTest() {

		setupMember();
		setupPoints();
		setupApproverAsCurrentUser();
		setupApprovalMatrix();
	}

	private void setupMember() {

		MemberModel theMember = new MemberModel();
		theMember.setAccountId("ACCT1");
		theMember.setContact("090");
		theMember.setPin("000");
		theMember.setUsername("user");
		theMember.setEnabled(true);
		theMember.setAccountStatus(MemberStatus.ACTIVE);

        CustomerProfile customerProfile = new CustomerProfile();
        customerProfile.setBirthdate(new Date());
        theMember.setCustomerProfile(customerProfile);

		memberRepo.saveAndFlush(theMember);
	}

	private void setupPoints() {

		PointsTxnModel thePoint = new PointsTxnModel();
		thePoint.setId(customIdGeneratorService.generateId("TEST1"));
		thePoint.setTransactionType(PointTxnType.ADJUST);
		thePoint.setTransactionNo("TXNADJUST01");
		thePoint.setStatus(TxnStatus.FORAPPROVAL);
		thePoint.setMemberModel(memberRepo.findByAccountId("ACCT1"));
		thePoint.setTransactionPoints(99D);
		pointsTxnModelRepo.save(thePoint);

		thePoint = new PointsTxnModel();
		thePoint.setId(customIdGeneratorService.generateId("TEST2"));
		thePoint.setTransactionType(PointTxnType.ADJUST);
		thePoint.setTransactionNo("TXNADJUST02");
		thePoint.setStatus(TxnStatus.FORAPPROVAL);
		thePoint.setMemberModel(memberRepo.findByAccountId("ACCT1"));
		thePoint.setTransactionPoints(105D);
		pointsTxnModelRepo.save(thePoint);

		thePoint = new PointsTxnModel();
		thePoint.setId(customIdGeneratorService.generateId("TEST3"));
		thePoint.setTransactionType(PointTxnType.ADJUST);
		thePoint.setStatus(null);
		thePoint.setMemberModel(memberRepo.findByAccountId("ACCT1"));
		pointsTxnModelRepo.save(thePoint);

		thePoint = new PointsTxnModel();
		thePoint.setId(customIdGeneratorService.generateId("TEST4"));
		thePoint.setTransactionType(PointTxnType.ADJUST);
		thePoint.setStatus(TxnStatus.REJECTED);
		thePoint.setMemberModel(memberRepo.findByAccountId("ACCT1"));
		pointsTxnModelRepo.save(thePoint);
	}

	private void setupApproverAsCurrentUser() {

		UserModel theApprover = new UserModel();
		theApprover.setUsername("loggedInUser");
		theApprover.setPassword("");
		theApprover.setEnabled(true);
		theApprover.setStoreCode( "store1" );
		userRepo.save(theApprover);

		CustomSecurityUserDetailsImpl user = new CustomSecurityUserDetailsImpl();
		user.setUserId(theApprover.getId());
		user.setUsername("loggedInUser");
		List<GrantedAuthority> authorities = Lists.newArrayList();
		authorities.add(new SimpleGrantedAuthority("ROLE_CSS"));
		user.setAuthorities(authorities);

		SecurityContextImpl securityContext = new SecurityContextImpl();
		securityContext.setAuthentication(new TestingAuthenticationToken(user,
				""));
		SecurityContextHolder.setContext(securityContext);
	}

	private void setupApprovalMatrix() {

		ApprovalMatrixModel theMatrix = new ApprovalMatrixModel();
		theMatrix.setModelType(ModelType.POINTS);
		theMatrix.setUser(userRepo.findByUsername("loggedInUser"));
		approvalMatrixRepo.save(theMatrix);
	}

	private void setupApproverAmount(Double inAmount) {

		List<ApprovalMatrixModel> theMatrices = approvalMatrixRepo
				.findByUsername("loggedInUser");
		ApprovalMatrixModel theMatrix = theMatrices.get(0);
		theMatrix.setAmount(inAmount);
		approvalMatrixRepo.save(theMatrix);
	}



	@Test
	public void testSearchPoints() {
		MemberModel memberModel = new MemberModel();
		memberModel.setEnabled(true);
		memberModel.setUsername("test");
		memberModel.setPassword("1234");
		memberModel.setAccountId("TEST00001");
		memberModel.setContact("1");
		memberModel.setPin("1");
		memberModel.setAccountStatus(MemberStatus.ACTIVE);

        CustomerProfile customerProfile = new CustomerProfile();
        customerProfile.setBirthdate(new Date());
        memberModel.setCustomerProfile(customerProfile);

		memberRepo.save(memberModel);
		
		MemberModel memberModel2 = new MemberModel();
		memberModel2.setEnabled(true);
		memberModel2.setUsername("test1");
		memberModel2.setPassword("12345");
		memberModel2.setAccountId("TEST00002");
		memberModel2.setContact("11");
		memberModel2.setPin("11");
		memberModel2.setAccountStatus(MemberStatus.ACTIVE);
		memberModel2.setParentAccountId(memberModel.getAccountId());
		CustomerProfile customerProfile2 = new CustomerProfile();
		customerProfile2.setBirthdate(new Date());
		memberModel2.setCustomerProfile(customerProfile2);
		memberRepo.save(memberModel2);

		PointsTxnModel thePoint = new PointsTxnModel();
		thePoint.setId( customIdGeneratorService.generateId("TEST") );
		thePoint.setStatus( TxnStatus.ACTIVE );
		thePoint.setMemberModel( memberModel );
		pointsTxnModelRepo.save( thePoint );

		PointsTxnModel thePoint2 = new PointsTxnModel();
		thePoint2.setId( customIdGeneratorService.generateId("TEST2") );
		thePoint2.setStatus( TxnStatus.FORAPPROVAL );
		thePoint2.setMemberModel( memberModel );
		thePoint2.setTransactionContributer(memberModel);
		pointsTxnModelRepo.save( thePoint2 );
		
		PointsTxnModel thePoint1 = new PointsTxnModel();
		thePoint1.setId( customIdGeneratorService.generateId("TEST1"));
		thePoint1.setStatus(TxnStatus.ACTIVE);
		thePoint1.setMemberModel(memberModel);
		thePoint1.setTransactionContributer(memberModel2);
		pointsTxnModelRepo.save( thePoint1 );

		PointsSearchDto theSearchDto = new PointsSearchDto();
		theSearchDto.setStatus( TxnStatus.FORAPPROVAL.toString() );
		theSearchDto.setAccountId( "TEST00001" );
		Assert.assertEquals( 1, pointsManagerService.searchPoints( theSearchDto ).getNumberOfElements() );
		
		PointsSearchDto theSearchDto1 = new PointsSearchDto();
		theSearchDto1.setContributer( Lists.newArrayList( "TEST00001", "TEST00002" ) );
		Assert.assertEquals(2, pointsManagerService.searchPoints(theSearchDto1).getNumberOfElements());
	}

    @Test
    public void updateEarningMemberPointsTest() throws Exception {
        MemberModel parent = new MemberModel();
        parent.setEnabled(true);
        parent.setUsername("test");
        parent.setPassword("1234");
        parent.setAccountId("TEST00001");
        parent.setContact("1");
        parent.setPin("1");
        parent.setAccountStatus(MemberStatus.ACTIVE);
        CustomerProfile customerProfile = new CustomerProfile();
        customerProfile.setBirthdate(new Date());
        parent.setCustomerProfile(customerProfile);
        parent = memberRepo.saveAndFlush(parent);

        MemberModel suppMember = new MemberModel();
        suppMember.setEnabled(true);
        suppMember.setUsername("test1");
        suppMember.setPassword("12345");
        suppMember.setAccountId("TEST00002");
        suppMember.setContact("11");
        suppMember.setPin("11");
        suppMember.setAccountStatus(MemberStatus.ACTIVE);
        suppMember.setParentAccountId(parent.getAccountId());
        CustomerProfile customerProfile2 = new CustomerProfile();
        customerProfile2.setBirthdate(new Date());
        suppMember.setCustomerProfile(customerProfile2);
        suppMember = memberRepo.saveAndFlush(suppMember);

        MemberModel member1 = new MemberModel();
        member1.setEnabled(true);
        member1.setUsername("test2");
        member1.setPassword("123456");
        member1.setAccountId("TEST00003");
        member1.setContact("33");
        member1.setPin("33");
        member1.setAccountStatus(MemberStatus.ACTIVE);
        CustomerProfile customerProfile3 = new CustomerProfile();
        customerProfile3.setBirthdate(new Date());
        member1.setCustomerProfile(customerProfile3);
        member1 = memberRepo.saveAndFlush(member1);

        for (int i = 0; i < 3; i++) {
            PointsTxnModel parentPoint = new PointsTxnModel();
            parentPoint.setId(customIdGeneratorService.generateId("PARENT" + i));
            parentPoint.setTransactionPoints(10000d);
            parentPoint.setStatus(TxnStatus.ACTIVE);
            parentPoint.setMemberModel(parent);
            pointsTxnModelRepo.saveAndFlush(parentPoint);
        }

        for (int i = 0; i < 4; i++) {
            PointsTxnModel suppPoint = new PointsTxnModel();
            suppPoint.setId(customIdGeneratorService.generateId("SUPP" + i));
            suppPoint.setTransactionPoints(10000d);
            suppPoint.setStatus(TxnStatus.ACTIVE);
            suppPoint.setMemberModel(parent);
            suppPoint.setTransactionContributer(suppMember);
            pointsTxnModelRepo.saveAndFlush(suppPoint);
        }

        for (int i = 0; i < 5; i++) {
            PointsTxnModel memberPoint = new PointsTxnModel();
            memberPoint.setId(customIdGeneratorService.generateId("MEMBER1_" + i));
            memberPoint.setTransactionPoints(10000d);
            memberPoint.setStatus(TxnStatus.ACTIVE);
            memberPoint.setMemberModel(member1);
            pointsTxnModelRepo.saveAndFlush(memberPoint);
        }

        CrmIdRef crmIdRef = new CrmIdRef();
        crmIdRef.setId("crmIdRef1");
        crmIdRef.setIdType(CrmIdRefServiceImpl.IdRefType.POINT_EARNING_MEMBER.name());
        crmIdRef.setModelId(member1.getId().toString());
        crmIdRefRepo.saveAndFlush(crmIdRef);

        pointsManagerService.updateMemberPoints();

        assertEquals(new Double(0), memberRepo.findOne(parent.getId()).getTotalPoints());
        assertEquals(new Double(40000), memberRepo.findOne(suppMember.getId()).getTotalPoints());
        assertEquals(new Double(50000), memberRepo.findOne(member1.getId()).getTotalPoints());
    }
}
