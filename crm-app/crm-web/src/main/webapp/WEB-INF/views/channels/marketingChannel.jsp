<%@include file="../common/messages.jsp" %>


<div id="content_marketingChannel">

  <div class="page-header page-header2"><h1><spring:message code="channel_pageheader" /></h1></div>

  <div class="searchFilter">
    <div><jsp:include page="marketingChannelSearchForm.jsp" /></div>
  </div>

  <div class="">
    <div id="view_marketingChannel"></div>
	  <div class="pull-right mb20">
      <button id="link_draftEmail" data-href="<c:url value="${emailAction}"/>" class="btn btn-primary" data-modal-header="${emailDialogHeader}" ><spring:message code="channel_email_draft" /></button>
      <button id="link_draftSms" data-href="<c:url value="${smsAction}"/>" class="btn btn-primary" data-modal-header="${smsDialogHeader}" ><spring:message code="channel_sms_draft" /></button>    
    </div>
    <div id="form_emailDraft"><jsp:include page="email/emailDraftForm.jsp" /></div>
    <div id="form_smsDraft"><jsp:include page="sms/smsDraftForm.jsp" /></div>
	  <div class="clearfix"></div>
  </div>

  <div id="list_marketingChannels" class="data-content-02"><jsp:include page="marketingChannelList.jsp"/></div>


  <script src='<c:url value="/js/viewspecific/channels/marketingChannel.js"/>' type='text/javascript'><![CDATA[&nbsp; ]]></script>


</div>