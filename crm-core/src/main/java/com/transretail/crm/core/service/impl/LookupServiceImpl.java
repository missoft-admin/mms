package com.transretail.crm.core.service.impl;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.apache.commons.collections.IteratorUtils;
import org.apache.commons.lang3.math.NumberUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.google.common.collect.Lists;
import com.mysema.query.jpa.impl.JPAQuery;
import com.mysema.query.types.Order;
import com.mysema.query.types.OrderSpecifier;
import com.mysema.query.types.expr.BooleanExpression;
import com.transretail.crm.common.service.dto.response.CodeDescDto;
import com.transretail.crm.common.service.exception.MessageSourceResolvableException;
import com.transretail.crm.common.service.util.SpringDataPagingUtil;
import com.transretail.crm.core.dto.LookupDetailResultList;
import com.transretail.crm.core.dto.LookupDetailSearchDto;
import com.transretail.crm.core.dto.LookupHeaderResultList;
import com.transretail.crm.core.dto.LookupHeaderSearchDto;
import com.transretail.crm.core.entity.BusinessUnitPsoftConfig;
import com.transretail.crm.core.entity.CrmForPeoplesoftConfig.TransactionType;
import com.transretail.crm.core.entity.enums.Status;
import com.transretail.crm.core.entity.lookup.LookupDetail;
import com.transretail.crm.core.entity.lookup.LookupHeader;
import com.transretail.crm.core.entity.lookup.QLookupDetail;
import com.transretail.crm.core.entity.lookup.QLookupHeader;
import com.transretail.crm.core.repo.BusinessUnitPsoftConfigRepo;
import com.transretail.crm.core.repo.CrmForPeoplesoftConfigRepo;
import com.transretail.crm.core.repo.LookupDetailRepo;
import com.transretail.crm.core.repo.LookupHeaderRepo;
import com.transretail.crm.core.service.CodePropertiesService;
import com.transretail.crm.core.service.LookupService;
import com.transretail.crm.core.util.AppConstants;
import org.springframework.data.jpa.repository.support.JpaEntityInformation;
import org.springframework.data.jpa.repository.support.JpaEntityInformationSupport;
import org.springframework.data.jpa.repository.support.JpaRepositoryFactory;
import org.springframework.data.jpa.repository.utils.JpaClassUtils;

/**
 *
 */
@Service("lookupServiceImpl")
public class LookupServiceImpl implements LookupService {

	@PersistenceContext
	private EntityManager em;
	@Autowired
	private LookupHeaderRepo lookupHeaderRepo;
	@Autowired
	private LookupDetailRepo lookupDetailRepo;
	@Autowired
	private CodePropertiesService codePropertiesService;
	@Autowired
	private CrmForPeoplesoftConfigRepo psoftConfigRepo;
	@Autowired
	private BusinessUnitPsoftConfigRepo buPsoftConfigRepo;
	@Autowired
	private MessageSource messageSource;
	//    @Autowired
	//    private JpaRepositoryFactory jpaRepositoryFactory;

	@Override
	@Transactional(readOnly = true)
	public LookupHeaderResultList getLookupHeaders(LookupHeaderSearchDto dto) {
		Pageable pageable = SpringDataPagingUtil.INSTANCE.createPageable(dto.getPagination());
		BooleanExpression filter = dto.createSearchExpression();
		Page<LookupHeader> page = filter != null ? lookupHeaderRepo.findAll(filter, pageable) : lookupHeaderRepo.findAll(pageable);
		return new LookupHeaderResultList(page);
	}

	@Override
	@Transactional(readOnly = true)
	public LookupDetailResultList getLookupDetails(LookupDetailSearchDto dto) {
		if (dto.getPagination() != null) {
			Pageable pageable = SpringDataPagingUtil.INSTANCE.createPageable(dto.getPagination());
			BooleanExpression filter = dto.createSearchExpression();
			Page<LookupDetail> page = filter != null ? lookupDetailRepo.findAll(filter, pageable) : lookupDetailRepo.findAll(pageable);
			return new LookupDetailResultList(page);
		} else {
			BooleanExpression filter = dto.createSearchExpression();
			Iterable<LookupDetail> page = lookupDetailRepo.findAll(filter);
			return new LookupDetailResultList(IteratorUtils.toList(page.iterator()));
		}
	}

	@Override
	public List<LookupHeader> getAllHeaders() {
		return lookupHeaderRepo.findAll(new Sort("code"));
	}

	@Override
	public List<LookupHeader> getAllHeaders(String[] ignore) {
		QLookupHeader qHeader = QLookupHeader.lookupHeader;
		return Lists.newArrayList(lookupHeaderRepo.findAll(qHeader.code.notIn(ignore)));
	}

	@Override
	public List<LookupHeader> getAllHeaders(String[] ignore, String field, boolean isAscending) {
		QLookupHeader qHeader = QLookupHeader.lookupHeader;
		return Lists.newArrayList(lookupHeaderRepo.findAll(qHeader.code.notIn(ignore), new PageRequest(0, (int) lookupHeaderRepo.count(qHeader.code.notIn(ignore)), new Sort(new Sort.Order(isAscending ? Sort.Direction.ASC : Sort.Direction.DESC, field)))));
	}

	@Override
	public List<LookupDetail> getDetails(LookupHeader header) {
		return lookupDetailRepo.findByHeader(header);
	}

	@Override
	public List<LookupDetail> getActiveDetails(LookupHeader header) {
		QLookupDetail qDetail = QLookupDetail.lookupDetail;
		BooleanExpression filter = qDetail.header.eq(header).and(qDetail.status.eq(Status.ACTIVE));
		return Lists.newArrayList(lookupDetailRepo.findAll(filter));
	}

	@Override
	public List<LookupDetail> getDetailsByHeaderCode(String headerCode) {
		return getDetails(getHeader(headerCode));
	}

	@Override
	public List<LookupDetail> getActiveDetailsByHeaderCode(String headerCode) {
		QLookupDetail qDetail = QLookupDetail.lookupDetail;
		BooleanExpression filter = qDetail.header.code.eq(headerCode).and(qDetail.status.eq(Status.ACTIVE));
		return Lists.newArrayList(lookupDetailRepo.findAll(filter));
	}

	@Override
	@Transactional
	public LookupDetail saveDetail(LookupDetail detail) {
		boolean isNew = lookupDetailRepo.findByCode(detail.getCode()) == null;
		final LookupDetail savedDetail = lookupDetailRepo.save(detail);
		// BU need to be created only once
		if (isNew && isBusinessUnit(detail.getHeader())) {
			BusinessUnitPsoftConfig buConfig = new BusinessUnitPsoftConfig(savedDetail);
			buConfig.setConfiguration(psoftConfigRepo.findByTransactionType(TransactionType.AFFILIATE_CLAIM));
			buPsoftConfigRepo.save(buConfig);
			BusinessUnitPsoftConfig buConfig2 = new BusinessUnitPsoftConfig(savedDetail);
			buConfig2.setConfiguration(psoftConfigRepo.findByTransactionType(TransactionType.AFFILIATE_REDEMPTION));
			buPsoftConfigRepo.save(buConfig2);
		}

		return detail;
	}

	@Override
	public boolean isDuplicateDetail(String code) {
		return lookupDetailRepo.findByCode(code) != null ? true : false;
	}

	@Override
	public List<LookupDetail> getDetails(String headerName) {
		LookupHeader header = lookupHeaderRepo.findByDescription(headerName.toUpperCase());
		if (header != null) {
			return getDetails(header);
		}
		return null;
	}

	@Override
	public LookupDetail getDetail(String id) {
		return lookupDetailRepo.findOne(id);
	}

	@Override
	public LookupDetail getDetailByCode(String code) {
		return lookupDetailRepo.findByCode(code);
	}

	@Override
	public LookupDetail getActiveDetailByCode(String code) {
		QLookupDetail qDetail = QLookupDetail.lookupDetail;
		return lookupDetailRepo.findOne(qDetail.code.eq(code).and(qDetail.status.eq(Status.ACTIVE)));
	}

	@Override
	public List<LookupHeader> getMemberHeaders() {
		QLookupHeader qLookupHeader = QLookupHeader.lookupHeader;
		Iterable<LookupHeader> headers = lookupHeaderRepo.findAll(qLookupHeader.code.contains("MEM").and(qLookupHeader.code.startsWith(AppConstants.HEADER_CUSTMEMBERFLD_PFX).not()));
		if (headers == null) {
			return null;
		}

		List<LookupHeader> list = new ArrayList<LookupHeader>();
		for (LookupHeader header : headers) {
			list.add(header);
		}
		return list;
	}

	public List<LookupHeader> getAllMemberHeaders() {
		return Lists.newArrayList(lookupHeaderRepo.findAll(QLookupHeader.lookupHeader.code.contains("MEM")));
	}

	@Transactional(readOnly = true)
	public Iterable<LookupHeader> getMemberConfigurableFields() {
		QLookupHeader qLookupHeader = QLookupHeader.lookupHeader;
		return lookupHeaderRepo.findAll(qLookupHeader.code.startsWith(AppConstants.HEADER_CUSTMEMBERFLD_PFX), new OrderSpecifier<String>(Order.ASC, qLookupHeader.code));
	}

	@Override
	public LookupHeader getHeader(String code) {
		return lookupHeaderRepo.findByCode(code);
	}

	@Override
	public boolean hasDuplicateDetailDescription(String code, String description, LookupHeader header) {
		return lookupDetailRepo.hasDuplicateDescription(code, description, header.getCode());
	}

	@Override
	public boolean isValidCompanyDetailCode(String code) {
		boolean result = false;
		if (code.length() >= 2) {
			String last2Chars = code.substring(code.length() - 2);
			if (NumberUtils.isDigits(last2Chars)) {
				QLookupDetail qLookupDetail = QLookupDetail.lookupDetail;
				if (!new JPAQuery(em).from(qLookupDetail).where(qLookupDetail.code.endsWith(last2Chars).and(qLookupDetail.header.code.eq(codePropertiesService.getHeaderCompany()))).limit(1).exists()) {
					result = true;
				}
			}
		}
		return result;
	}

	@Override
	public boolean isValidCardTypeDetailCode(String code) {
		boolean result = false;
		if (code.length() >= 2) {
			String last2Chars = code.substring(code.length() - 2);
			if (NumberUtils.isDigits(last2Chars)) {
				QLookupDetail qLookupDetail = QLookupDetail.lookupDetail;
				if (!new JPAQuery(em).from(qLookupDetail).where(qLookupDetail.code.endsWith(last2Chars).and(qLookupDetail.header.code.eq(codePropertiesService.getHeaderCardTypes()))).limit(1).exists()) {
					result = true;
				}
			}
		}
		return result;
	}

	@Override
	public LookupDetail getDetailByTwoDigitCardTypeNumber(String code) {
		if (code.length() >= 2) {
			String last2Chars = code.substring(code.length() - 2);
			if (NumberUtils.isDigits(last2Chars)) {
				QLookupDetail qLookupDetail = QLookupDetail.lookupDetail;
				return lookupDetailRepo.findOne(qLookupDetail.code.endsWith(last2Chars).and(qLookupDetail.header.code.eq(codePropertiesService.getHeaderCardTypes())));
			}
		}
		return null;
	}

	@Override
	public LookupDetail getDetailByTwoDigitCompanyNumber(String code) {
		if (code.length() >= 2) {
			String last2Chars = code.substring(code.length() - 2);
			if (NumberUtils.isDigits(last2Chars)) {
				QLookupDetail qLookupDetail = QLookupDetail.lookupDetail;
				return lookupDetailRepo.findOne(qLookupDetail.code.endsWith(last2Chars).and(qLookupDetail.header.code.eq(codePropertiesService.getHeaderCompany())));
			}
		}
		return null;
	}

	@Transactional(readOnly = true)
	@Override
	public LookupDetail getLookupDetailByHdrCodeAndDesc(String headerCode, String desc) {
		QLookupDetail qLookupDetail = QLookupDetail.lookupDetail;
		return lookupDetailRepo.findOne(qLookupDetail.header.code.eq(headerCode).and(qLookupDetail.description.upper().eq(desc.toUpperCase())));
	}

	@Transactional(readOnly = true)
	@Override
	public String getLookupDetailCodeByHdrCodeAndDesc(String headerCode, String desc) {
		QLookupDetail qLookupDetail = QLookupDetail.lookupDetail;
		return new JPAQuery(em).from(qLookupDetail).where(qLookupDetail.header.code.eq(headerCode).and(qLookupDetail.description.upper().eq(desc.toUpperCase()))).singleResult(qLookupDetail.code);
	}

	@Transactional
	@Override
	public void updateHeader(CodeDescDto codeDescDto) throws MessageSourceResolvableException {
		LookupHeader lookupHeader = lookupHeaderRepo.findOne(codeDescDto.getCode());
		if (lookupHeader == null) {
			throw new MessageSourceResolvableException("generic.not.exists", new String[] { "Lookup Header code " + codeDescDto.getCode() }, "Lookup Header code " + codeDescDto.getCode() + " does not exists.");
		} else if (isHeaderDescExists(codeDescDto.getCode(), codeDescDto.getDesc())) {
			throw new MessageSourceResolvableException("generic.exists", new String[] { messageSource.getMessage("label_description", null, LocaleContextHolder.getLocale()) }, "Description already exists.");
		}
		lookupHeader.setDescription(codeDescDto.getDesc().toUpperCase());
		lookupHeaderRepo.save(lookupHeader);
	}

	private boolean isHeaderDescExists(String code, String desc) {
		QLookupHeader qLookupHeader = QLookupHeader.lookupHeader;
		return new JPAQuery(em).from(qLookupHeader).where(qLookupHeader.code.ne(code).and(qLookupHeader.description.upper().eq(desc.toUpperCase()))).exists();
	}

	@Transactional(readOnly = true)
	public List<LookupDetail> getLookupDetails(String codes) {
		String[] arr = codes.split(",");
		QLookupDetail qLookupDetail = QLookupDetail.lookupDetail;
		List<BooleanExpression> exprs = new ArrayList<BooleanExpression>();
		if (arr.length > 0) {
			for (String code : arr) {
				exprs.add(qLookupDetail.code.eq(code));
			}
		} else {
			exprs.add(qLookupDetail.code.eq(codes));
		}
		Iterable<LookupDetail> iterable = lookupDetailRepo.findAll(BooleanExpression.anyOf(exprs.toArray(new BooleanExpression[exprs.size()])));
		return iterable != null ? Lists.newArrayList(iterable) : null;
	}

	@Override
	public List<LookupDetail> getActiveDetailsByHeaderCode(String headerCode, OrderSpecifier<?>... orders) {
		QLookupDetail qDetail = QLookupDetail.lookupDetail;
		BooleanExpression filter = qDetail.header.code.eq(headerCode).and(qDetail.status.eq(Status.ACTIVE));
		return Lists.newArrayList(lookupDetailRepo.findAll(filter, orders));
	}

	private boolean isBusinessUnit(LookupHeader header) {
		return codePropertiesService.getHeaderBusinessUnit().equalsIgnoreCase(header.getCode());
	}

}
