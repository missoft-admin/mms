create table CRM_LC_PHYSICAL_COUNT (
        id number(19,0) not null,
        CREATED_BY varchar2(255 char),
        CREATED_DATETIME timestamp,
        LAST_UPDATED_BY varchar2(255 char),
        LAST_UPDATED_DATETIME timestamp,
        BOX_NO varchar2(255 char),
        ENDING_SERIES varchar2(255 char),
        QUANTITY number(19,0),
        STARTING_SERIES varchar2(255 char),
        CARD_TYPE varchar2(15 char),
        LOCATION varchar2(15 char),
        primary key (id)
    );

alter table CRM_LC_PHYSICAL_COUNT 
        add constraint FK_bor0kvslc1gahccguq5pmn0k8 
        foreign key (CARD_TYPE) 
        references CRM_REF_LOOKUP_DTL;

alter table CRM_LC_PHYSICAL_COUNT 
        add constraint FK_p5m3jhofwmddy5v8l3wuvi7hl 
        foreign key (LOCATION) 
        references CRM_REF_LOOKUP_DTL;