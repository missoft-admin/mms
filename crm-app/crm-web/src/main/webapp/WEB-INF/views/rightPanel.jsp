<%@ include file="common/taglibs.jsp"%>

<!--<div class="form-inline ">
  <label class="label-1">
    <spring:message code="label_right_panel_no_employees">:</spring:message>
  </label>
  <label class="pull-right numbers-1">
    ${employeeStats.numberOfEmployees}
  </label>
</div>

<hr size=1> 

<div class="form-inline label-2">
  <label class="label-2">
    <spring:message code="label_right_panel_no_individuals">:</spring:message>
  </label>
  <label class="pull-right">
    ${employeeStats.numberOfIndividuals}
  </label>
</div>

<div class="form-inline mb10 label-2">
  <label class="">
    <spring:message code="label_right_panel_no_professionals">:</spring:message>
  </label>
  <label class="pull-right">
    ${employeeStats.numberOfProfessionals}
  </label>
</div>



<div class="form-inline">
  <label class="label-1">
    <spring:message code="label_right_panel_total_no_customers">:</spring:message>
  </label>
  <label class="pull-right numbers-1">
    ${employeeStats.numberOfTotalCustomers}
  </label>
</div>

<hr size=1> 

<div class="form-inline mb20">
  <label class="label-1">
    <spring:message code="label_right_panel_store_members">:</spring:message>
  </label>
  <label class="pull-right numbers-1">${employeeStats.numberOfStoreMembers}</label>
</div>-->

<div class="form-inline" style="height: 300px;">
  <label class="label-1" style="width: 142px;">
    <spring:message code="label_right_panel_percentage_national">:</spring:message>
  </label>
  <label class="pull-right numbers-1">
    <fmt:formatNumber type="PERCENT" maxFractionDigits="2" value="${employeeStats.numberOfStoreMembers / employeeStats.numberOfTotalCustomers}"/>
  </label>
</div>

<style type="text/css">
    .numbers-1 {
      font-size: 30px;
      line-height: 24px;
    }
    .label-1 {
      font-size: 18px;
      color: #193c58 !important;
    }
    .label-2 label {
      font-size: 14px;
      text-transform: capitalize
    }
    [class*="span"].centered {
      margin-left: auto;
      margin-right: auto;
      float: none;
    }
</style>