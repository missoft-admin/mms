package com.transretail.crm.web.member.service.impl;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyMap;
import static org.mockito.Matchers.anyString;
import static org.mockito.Matchers.argThat;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.doAnswer;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.reset;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.Locale;
import java.util.Map;
import java.util.concurrent.ConcurrentMap;

import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.junit.After;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentMatcher;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.CacheManager;
import org.springframework.cache.concurrent.ConcurrentMapCache;
import org.springframework.context.MessageSource;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.ImportResource;
import org.springframework.mail.MailSendException;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.AnnotationConfigContextLoader;
import org.springframework.util.StringUtils;

import com.transretail.crm.common.service.exception.MessageSourceResolvableException;
import com.transretail.crm.common.util.SimpleEncrytor;
import com.transretail.crm.core.entity.MemberModel;
import com.transretail.crm.core.entity.PointsTxnModel;
import com.transretail.crm.core.entity.embeddable.CustomerProfile;
import com.transretail.crm.core.entity.enums.Status;
import com.transretail.crm.core.entity.lookup.LookupDetail;
import com.transretail.crm.core.entity.lookup.LookupHeader;
import com.transretail.crm.core.repo.LookupDetailRepo;
import com.transretail.crm.core.repo.LookupHeaderRepo;
import com.transretail.crm.core.repo.MemberRepo;
import com.transretail.crm.core.repo.PointsTxnModelRepo;
import com.transretail.crm.loyalty.service.LoyaltyCardService;
import com.transretail.crm.web.member.bean.ChangePasswordDto2;
import com.transretail.crm.web.member.bean.RegisterDto;
import com.transretail.crm.web.member.service.EmailService;
import com.transretail.crm.web.member.service.MemberServiceExt;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(loader = AnnotationConfigContextLoader.class)
@SuppressWarnings("unchecked")
public class MemberServiceExtImplTest {
    private static final String USERNAME = "xxxcloudxxx";

    @Configuration
    @ImportResource({"classpath:META-INF/spring/applicationContext-core.xml", "classpath:META-INF/spring/applicationContext-test.xml"})
    static class ContextConfiguration {
        @Bean
        public EmailService emailService() {
            return mock(EmailService.class);
        }

        @Bean
        public MessageSource messageSource() {
            return mock(MessageSource.class);
        }

        @Bean
        public MemberServiceExt memberServiceExt() {
            return new MemberServiceExtImpl();
        }

        @Bean
        public LoyaltyCardService loyaltyCardService() {
            return mock(LoyaltyCardService.class);
        }
    }

    private static final String EMAIL = "cloud@cloudx.com";
    private static final String PASSWORD = "password";
    private static final String GENDER = "M";
    private static final DateTime BIRTHDATE = DateTime.parse("2013/03/13", DateTimeFormat.forPattern("yyyy/MM/dd"));
    private static final String CONTACT = "contact";
    private String individualCode = "MTYP001";

    @Autowired
    private CacheManager cacheManager;
    @Autowired
    private MemberServiceExt service;
    @Autowired
    private MessageSource messageSource;
    @Autowired
    private EmailService emailService;
    @Autowired
    private MemberRepo memberRepo;
    @Autowired
    private PointsTxnModelRepo pointsRepo;

    @Autowired
    LookupHeaderRepo lookupHeaderRepo;

    @Autowired
    LookupDetailRepo lookupDetailRepo;

    private MemberModel savedMember;
    private PointsTxnModel savedPoints;

    @Before
    public void onSetup() {
        reset(messageSource);
        reset(emailService);

        LookupHeader header = new LookupHeader();
        header.setCode("gender");
        header.setDescription("gender");
        header = lookupHeaderRepo.saveAndFlush(header);

        LookupDetail detail = new LookupDetail();
        detail.setHeader(header);
        detail.setCode(GENDER);
        detail.setDescription(GENDER);
        detail.setStatus(Status.ACTIVE);
        detail = lookupDetailRepo.saveAndFlush(detail);

        header = new LookupHeader();
        header.setCode("membertype");
        header.setDescription("membertype");
        header = lookupHeaderRepo.saveAndFlush(header);

        detail = new LookupDetail();
        detail.setHeader(header);
        detail.setCode(individualCode);
        detail.setDescription(individualCode);
        detail.setStatus(Status.ACTIVE);
        lookupDetailRepo.saveAndFlush(detail);

        savedMember = new MemberModel();
        savedMember.setAccountId("accountId");
        savedMember.setPin("pin");
        savedMember.setUsername(USERNAME);
        savedMember.setPassword(PASSWORD);
        CustomerProfile profile = new CustomerProfile();
        profile.setGender(detail);
        profile.setBirthdate(BIRTHDATE.toDate());
        savedMember.setCustomerProfile(profile);
        savedMember.setContact(CONTACT);
        savedMember.setEmail(EMAIL);
        savedMember.setEnabled(true);
        memberRepo.save(savedMember);

        savedPoints = new PointsTxnModel();
        savedPoints.setId("TEST");
        savedPoints.setTransactionPoints(1D);
        savedPoints.setMemberModel(savedMember);
        pointsRepo.save(savedPoints);
    }

    @After
    public void tearDown() {
        cacheManager.getCache(MemberServiceExtImpl.CACHE_NAME).clear();
        if (savedPoints != null && savedPoints.getId() != null) {
            pointsRepo.delete(savedPoints.getId());
        }
        if (savedMember != null && savedMember.getId() != null) {
            memberRepo.delete(savedMember.getId());
        }
    }

    @Test
    @Ignore("Fix this")
    public void saveTest() throws MessageSourceResolvableException {
        try {
            // Remove existing savedMember in db
            // to avoid clash
            pointsRepo.deleteAll();
            memberRepo.deleteAll();
            savedPoints = null;
            savedMember = null;
            memberRepo.flush();

            // Case 1: Save with no email
            // Given
            final RegisterDto dto = new RegisterDto();
            dto.setUsername(USERNAME);
            dto.setPassword(PASSWORD);
            dto.setContact(CONTACT);
            try {
                // Save with no email add
                // When
                service.save(dto, new MockHttpServletRequest());
            } catch (MessageSourceResolvableException e) {
                // Then
                assertEquals("User [" + USERNAME + "] does not have an email.", e.getMessage());
            }

            // Case 2: Save with exception in sending email
            MailSendException expectedException = new MailSendException("test");
            try {
                // Given
                dto.setEmail(EMAIL);
                dto.setPassword(PASSWORD);
                dto.setGender(new LookupDetail(GENDER));
                dto.setBirthDate(BIRTHDATE);
                dto.setContact(CONTACT);
                doThrow(expectedException).when(emailService)
                    .sendValidationLink(anyString(), anyString(), anyString(), anyMap(), any(Locale.class));

                service.save(dto, new MockHttpServletRequest());
                fail("Should throw Exception");
            } catch (MessageSourceResolvableException e) {
                assertEquals(expectedException, e);
                // Nothing should be saved
                assertNull(memberRepo.findByUsername(USERNAME));
            }

            // Case 3: Successful save
            final String expectedSubject = "subject";
            when(messageSource.getMessage(eq("member.registration.email.subject"), any(Object[].class), any(Locale.class)))
                .thenReturn(expectedSubject);

            final StringBuilder validationCodeHolder = new StringBuilder();
            doAnswer(new Answer() {
                @Override
                public Object answer(InvocationOnMock invocation) throws Throwable {
                    Map<String, Object> data = (Map<String, Object>) invocation.getArguments()[3];
                    validationCodeHolder.append(data.get("queryString").toString());
                    return null;
                }
            }).when(emailService)
                .sendValidationLink(eq(expectedSubject), eq(dto.getEmail()), eq("registrationValidation.ftl"), anyMap(),
                    any(Locale.class));
            // When
            service.save(dto, new MockHttpServletRequest());

            // Then
            verify(emailService)
                .sendValidationLink(eq(expectedSubject), eq(dto.getEmail()), eq("registrationValidation.ftl"),
                    argThat(new ArgumentMatcher<Map<String, Object>>() {
                        @Override
                        public boolean matches(Object argument) {
                            Map<String, Object> data = (Map<String, Object>) argument;
                            boolean validMemberName = dto.getFullName().equals(data.get("membername"));
                            boolean validValidationLink = "http://localhost/member/validate".equals(data.get("link"));
                            boolean hasCode = StringUtils.hasText(data.get("queryString").toString());
                            return validMemberName && validValidationLink && hasCode;
                        }
                    }),
                    any(Locale.class));

            MemberModel savedMember = memberRepo.findByUsername(USERNAME);
            assertNotNull(savedMember);
            assertEquals(EMAIL, savedMember.getEmail());
            assertEquals(validationCodeHolder.toString(), savedMember.getValidationCode());
        } finally {
            MemberModel savedMember = memberRepo.findByUsername(USERNAME);
            if (savedMember != null) {
                memberRepo.delete(savedMember.getId());
            }
        }
    }

    @Test
    public void validateTest() {
        String validationCode = "xcloudcodx";
        // Case 1: No member found
        assertFalse(service.validate(validationCode));
        // Case 2: Member is already enabled
        MemberModel model = memberRepo.findOne(savedMember.getId());
        model.setEnabled(true);
        model.setValidationCode(validationCode);
        memberRepo.save(model);
        assertFalse(service.validate(validationCode));

        // Case 3: Successful validation
        model = memberRepo.findByUsername(USERNAME);
        model.setEnabled(false);
        memberRepo.save(model);
        assertTrue(service.validate(validationCode));
    }

    @Test
    public void sendChangePasswordValidationTest() throws MessageSourceResolvableException {
        String newPassword = "newpassword";
        ChangePasswordDto2 form = new ChangePasswordDto2();
        form.setPassword(newPassword);
        form.setOldPassword(PASSWORD);

        final String subject = "testemail";
        when(messageSource.getMessage(eq("member.password.change.email.subject"), any(Object[].class), any(Locale.class)))
            .thenReturn(subject);

        service.sendChangePasswordValidation(USERNAME, form, new MockHttpServletRequest());
        verify(emailService).sendValidationLink(eq(subject), eq(EMAIL), eq("genericValidationTemplate.ftl"),
            argThat(new ArgumentMatcher<Map<String, Object>>() {
                @Override
                public boolean matches(Object argument) {
                    Map<String, Object> data = (Map<String, Object>) argument;
                    boolean matchMemberName = data.get("membername").equals(USERNAME);
                    boolean matchTitle = data.get("title").equals(subject);
                    boolean matchLink = data.get("link").equals("http://localhost/member/" + USERNAME + "/password/change");
                    boolean hasQueryString = StringUtils.hasText(data.get("queryString").toString());
                    return matchMemberName && matchTitle && matchLink && hasQueryString;
                }
            }), any(Locale.class));
        assertEquals(1, getCacheStore().size());
        assertEquals(newPassword, getCacheStore().values().iterator().next());
        getCacheStore().clear();
    }

    @Test
    public void sendChangePasswordValidationWithErrorTest() throws MessageSourceResolvableException {
        // Case 1: User not found
        String unexistingUser = "unexisting";
        try {
            service.sendChangePasswordValidation(unexistingUser, null, null);
            fail("Should throw exception");
        } catch (MessageSourceResolvableException e) {
            assertEquals("User with username [" + unexistingUser + "] does not exist.", e.getMessage());
            verify(emailService, never()).sendValidationLink(anyString(), anyString(), anyString(), anyMap(), any(Locale.class));
        }

        // Case 2: Old password not matched
        ChangePasswordDto2 form = new ChangePasswordDto2();
        form.setOldPassword("wrongpassword");
        try {
            service.sendChangePasswordValidation(USERNAME, form, null);
            fail("Should throw exception");
        } catch (MessageSourceResolvableException e) {
            assertEquals("Invalid password.", e.getMessage());
            verify(emailService, never()).sendValidationLink(anyString(), anyString(), anyString(), anyMap(), any(Locale.class));
        }

        // Case 3: User has no email
        MemberModel model = memberRepo.findByUsername(USERNAME);
        model.setEmail(null);
        memberRepo.save(model);

        form.setOldPassword(PASSWORD);
        try {
            service.sendChangePasswordValidation(USERNAME, form, null);
            fail("Should throw exception");
        } catch (MessageSourceResolvableException e) {
            assertEquals("User [" + USERNAME + "] does not have an email.", e.getMessage());
            verify(emailService, never()).sendValidationLink(anyString(), anyString(), anyString(), anyMap(), any(Locale.class));
        }

        // Case 4: Failed to send validation link
        getCacheStore().clear();
        model = memberRepo.findByUsername(USERNAME);
        model.setEmail(EMAIL);
        memberRepo.save(model);
        final String subject = "testemail";
        when(messageSource.getMessage(eq("member.password.change.email.subject"), any(Object[].class), any(Locale.class)))
            .thenReturn(subject);

        MessageSourceResolvableException expectedException =
            new MessageSourceResolvableException("member.password.change.mailsending.failed", new String[]{EMAIL},
                "Failed to send email to [" + EMAIL + "].");
        doThrow(expectedException).when(emailService)
            .sendValidationLink(eq(subject), eq(EMAIL), eq("genericValidationTemplate.ftl"), anyMap(), any(Locale.class));
        try {
            service.sendChangePasswordValidation(USERNAME, form, new MockHttpServletRequest());
            fail("Should throw exception");
        } catch (MessageSourceResolvableException e) {
            assertEquals(expectedException, e);
            assertTrue(getCacheStore().isEmpty());
            verify(emailService).sendValidationLink(eq(subject), eq(EMAIL), eq("genericValidationTemplate.ftl"),
                argThat(new ArgumentMatcher<Map<String, Object>>() {
                    @Override
                    public boolean matches(Object argument) {
                        Map<String, Object> data = (Map<String, Object>) argument;
                        boolean matchMemberName = data.get("membername").equals(USERNAME);
                        boolean matchTitle = data.get("title").equals(subject);
                        boolean matchLink = data.get("link").equals("http://localhost/member/" + USERNAME + "/password/change");
                        boolean hasQueryString = StringUtils.hasText(data.get("queryString").toString());
                        return matchMemberName && matchTitle && matchLink && hasQueryString;
                    }
                }), any(Locale.class));
        }
    }

    @Test
    public void validateChangePasswordTest() {
        String validationCode = "somecode";
        // Case 1: No validation code in cache
        assertFalse(service.validateChangePassword(USERNAME, validationCode));
        getCacheStore().put(validationCode, "");
        // Case 2: Validation code key has empty value or password
        assertFalse(service.validateChangePassword(USERNAME, validationCode));
        // Case 3: Successful validation
        String newPassword = "newpassword";
        getCacheStore().put(validationCode, newPassword);
        assertTrue(service.validateChangePassword(USERNAME, validationCode));
        MemberModel model = memberRepo.findByUsername(USERNAME);
        assertEquals(newPassword, model.getPassword());
        assertEquals(validationCode, model.getValidationCode());
    }

    @Test
    public void updateAndGetPinTest() throws Exception {
        String expectedNewPin = "iamnewpin";
        service.updatePin(USERNAME, expectedNewPin);
        String encrypedPin = SimpleEncrytor.getInstance().encrypt(expectedNewPin);
        assertEquals(encrypedPin, memberRepo.findOne(savedMember.getId()).getPin());
        assertEquals(encrypedPin, service.getPin(USERNAME));
        assertNull(service.getPin("notexistinguser"));
    }

//    @Test
//    public void getUserPointsTest() {
//        Iterable<PointsTxnModel> points = service.getUserPoints(savedMember.getId());
//        Iterator<PointsTxnModel> it = points.iterator();
//        PointsTxnModel point = it.next();
//        assertFalse(it.hasNext());
//        assertEquals(savedPoints.getTransactionPoints(), point.getTransactionPoints());
//    }

    @Test
    public void getTotalPointsTest() {
        // Case 1: User not exists
        assertNull(service.getTotalPoints(99999L));
        // Case 2: No total Points
        assertNull(service.getTotalPoints(savedMember.getId()));
        // Case 3: Total points = 10
        Double expectedTotal = 10D;
        MemberModel memberModel = memberRepo.findOne(savedMember.getId());
        memberModel.setTotalPoints(expectedTotal);
        memberRepo.save(memberModel);
        assertEquals(expectedTotal.longValue(), service.getTotalPoints(savedMember.getId()).longValue());
    }

    @Test
    public void validateResetPasswordTest() {
        String validationCode = "somevalidationcode";
        assertFalse(service.validateResetPassword(USERNAME, validationCode));

        getCacheStore().put(validationCode, "          ");
        assertFalse(service.validateResetPassword(USERNAME, validationCode));

        getCacheStore().put(validationCode, "wrongemail");
        assertFalse(service.validateResetPassword(USERNAME, validationCode));
        assertEquals(1, getCacheStore().size());

        getCacheStore().put(validationCode, EMAIL);
        assertTrue(service.validateResetPassword(USERNAME, validationCode));
        assertEquals(0, getCacheStore().size());
    }

    @Test
    public void updatePasswordTest() {
        String newPassword = "newpassword";
        service.updatePassword(USERNAME, newPassword);
        MemberModel model = memberRepo.findByUsername(USERNAME);
        assertEquals(newPassword, model.getPassword());
    }

    private ConcurrentMap getCacheStore() {
        ConcurrentMapCache cache = (ConcurrentMapCache) cacheManager.getCache(MemberServiceExtImpl.CACHE_NAME);
        return cache.getNativeCache();
    }
}