package com.transretail.crm.core.entity;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import org.hibernate.annotations.Immutable;
import org.springframework.format.annotation.DateTimeFormat;

/**
 * POS table
 * 
 * @author Monte Cillo Co (mco@exist.com)
 */
@Entity
@Table(name = "DIVISION")
@Immutable
public class Division {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "division_id", length = 16) //default to 16 as most are using this size -- override in implementing class as needed.
    private String id;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    // @NotNull
    @Temporal(TemporalType.TIMESTAMP)
    @DateTimeFormat(style = "M-")
    @Column(name = "UPDATE_DATE_AUD")
    private Date updateDateAud;

    @NotNull
    @Temporal(TemporalType.TIMESTAMP)
    @DateTimeFormat(style = "M-")
    @Column(name = "CREATE_DATE_AUD")
    private Date createDateAud;
    @NotNull
    @Size(max = 40)
    private String description;

    @NotNull
    @Size(max = 10)
    private String code;

    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, targetEntity = Department.class, mappedBy = "division")
    private Set<Department> departments = new HashSet<Department>();

    public Date getUpdateDateAud() {
        return updateDateAud;
    }

    public void setUpdateDateAud(Date updateDateAud) {
        this.updateDateAud = updateDateAud;
    }

    public Date getCreateDateAud() {
        return createDateAud;
    }

    public void setCreateDateAud(Date createDateAud) {
        this.createDateAud = createDateAud;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public Set<Department> getDepartments() {
        return departments;
    }

    public void setDepartments(Set<Department> departments) {
        this.departments = departments;
    }

}
