package com.transretail.crm.report.template.impl;

import java.io.File;
import java.io.InputStream;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Scanner;

import javax.annotation.Resource;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import net.sf.jasperreports.engine.JRParameter;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.joda.time.LocalDate;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.context.support.MessageSourceResourceBundle;
import org.springframework.security.authentication.TestingAuthenticationToken;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.context.SecurityContextImpl;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.TransactionCallback;
import org.springframework.transaction.support.TransactionTemplate;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.transretail.crm.common.reporting.exception.ReportException;
import com.transretail.crm.common.reporting.jasper.JRProcessor;
import com.transretail.crm.common.reporting.service.ReportService;
import com.transretail.crm.core.entity.UserModel;
import com.transretail.crm.core.repo.UserRepo;
import com.transretail.crm.core.security.model.impl.CustomSecurityUserDetailsImpl;
import com.transretail.crm.core.service.CodePropertiesService;
import com.transretail.crm.report.template.CommonReportFilter;
import com.transretail.crm.report.template.ReportTemplate;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath*:/META-INF/spring/applicationContext*.xml",
    "/META-INF/spring/applicationContext-test.xml"})
public class NoOfChildrenContributionReportTest {
	@Autowired
    private PlatformTransactionManager transactionManager;
	@PersistenceContext
    private EntityManager em;
	@Autowired
    private ReportService reportService;
	@Autowired
    private MessageSource messageSource;
	@Resource(name = "noOfChildrenContributionReport")
    private ReportTemplate noOfChildrenContributionReport;
	
	private static final Logger logger = LoggerFactory.getLogger(NoOfChildrenContributionReportTest.class);
	
	@Autowired
	private CodePropertiesService codePropertiesService;
	@Autowired
	private UserRepo userRepo;
	@Before
	public void setUp() {
		UserModel currentUser = new UserModel();
    	currentUser.setUsername("loggedInUser");
    	currentUser.setPassword("");
    	currentUser.setEnabled(true);
    	currentUser.setInventoryLocation(codePropertiesService.getDetailInvLocationDenpasar());
    	currentUser.setStoreCode( "store1" );
    	userRepo.save(currentUser);

        CustomSecurityUserDetailsImpl user = new CustomSecurityUserDetailsImpl();
        user.setUserId(currentUser.getId());
        user.setUsername(currentUser.getUsername());
        List<GrantedAuthority> authorities = Lists.newArrayList();
        authorities.add(new SimpleGrantedAuthority("ROLE_CSS"));
        user.setAuthorities(authorities);
        user.setInventoryLocation(codePropertiesService.getDetailInvLocationDenpasar());

        SecurityContextImpl securityContext = new SecurityContextImpl();
        securityContext.setAuthentication(new TestingAuthenticationToken(user, ""));
        SecurityContextHolder.setContext(securityContext);
		
		InputStream is = Thread.currentThread().getContextClassLoader().getResourceAsStream("scripts/transactions_for_member_contribution_report_test.sql");
        try {
            final Scanner in = new Scanner(is);
            TransactionTemplate transactionTemplate = new TransactionTemplate(transactionManager);
            transactionTemplate.execute(new TransactionCallback() {
                public Object doInTransaction(TransactionStatus status) {
                    while (in.hasNext()) {
                        String script = in.nextLine();
                        if (StringUtils.isNotBlank(script)) {
                            em.createNativeQuery(script).executeUpdate();
                        }
                    }
                    return null;
                }
            });
        } finally {
            IOUtils.closeQuietly(is);
        }
	}
	
	@Test
	public void test() throws ReportException {
		Map<String, String> map = Maps.newHashMap();
		map.put(CommonReportFilter.DATE_FROM, new LocalDate(2013, 1, 1).toString("dd-MM-yyyy"));
		map.put(CommonReportFilter.DATE_TO, new LocalDate(2013, 2, 1).toString("dd-MM-yyyy"));
		JRProcessor jrProcessor = noOfChildrenContributionReport.createJrProcessor(map);

		File testClassesDir = FileUtils.toFile(Thread.currentThread().getContextClassLoader().getResource(""));
		File output = new File(testClassesDir, "test-store-member-transaction-report-output.pdf");
		logger.debug("Output location {}", output.getAbsolutePath());
		output.delete();
		Locale locale = LocaleContextHolder.getLocale();
		jrProcessor.addParameter(JRParameter.REPORT_LOCALE, locale);
		jrProcessor.addParameter(JRParameter.REPORT_RESOURCE_BUNDLE, new MessageSourceResourceBundle(messageSource, locale));
		reportService.exportPdfReport(jrProcessor, output);

		double bytes = output.length();
		double kilobytes = (bytes / 1024);
		logger.debug("Kilobytes: {}", kilobytes);
	}
}
