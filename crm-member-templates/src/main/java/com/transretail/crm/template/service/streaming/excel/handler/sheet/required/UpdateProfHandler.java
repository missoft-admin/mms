package com.transretail.crm.template.service.streaming.excel.handler.sheet.required;

import static org.apache.commons.lang3.StringUtils.isBlank;
import static org.apache.commons.lang3.StringUtils.isNotBlank;

import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.regex.Pattern;

import javax.annotation.Resource;

import org.apache.commons.lang3.StringUtils;
import org.joda.time.DateTime;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.mysema.query.jpa.impl.JPAQuery;
import com.transretail.crm.common.util.SimpleEncrytor;
import com.transretail.crm.core.entity.MemberModel;
import com.transretail.crm.core.entity.QMemberModel;
import com.transretail.crm.core.entity.embeddable.CustomerProfile;
import com.transretail.crm.core.entity.embeddable.Npwp;
import com.transretail.crm.core.entity.embeddable.ProfessionalProfile;
import com.transretail.crm.core.security.model.impl.CustomSecurityUserDetailsImpl;
import com.transretail.crm.template.service.streaming.excel.handler.HandlerException;
import com.transretail.crm.template.service.streaming.excel.handler.cell.AccountIdCellHandler;
import com.transretail.crm.template.service.streaming.excel.handler.cell.AccountStatusHandler;
import com.transretail.crm.template.service.streaming.excel.handler.cell.BestTimeToCellHandler;
import com.transretail.crm.template.service.streaming.excel.handler.cell.BirthDateCellHandler;
import com.transretail.crm.template.service.streaming.excel.handler.cell.BusinessLicenseCellHandler;
import com.transretail.crm.template.service.streaming.excel.handler.cell.BusinessNameHandler;
import com.transretail.crm.template.service.streaming.excel.handler.cell.CellHandler;
import com.transretail.crm.template.service.streaming.excel.handler.cell.EducationCellHandler;
import com.transretail.crm.template.service.streaming.excel.handler.cell.EmailCellHandler;
import com.transretail.crm.template.service.streaming.excel.handler.cell.FirstNameCellHandler;
import com.transretail.crm.template.service.streaming.excel.handler.cell.GenderCellHandler;
import com.transretail.crm.template.service.streaming.excel.handler.cell.KtpIdCellHandler;
import com.transretail.crm.template.service.streaming.excel.handler.cell.LastNameCellHandler;
import com.transretail.crm.template.service.streaming.excel.handler.cell.MaritalStatusCellHandler;
import com.transretail.crm.template.service.streaming.excel.handler.cell.MobilePhoneCellHandler;
import com.transretail.crm.template.service.streaming.excel.handler.cell.NationalityCellHandler;
import com.transretail.crm.template.service.streaming.excel.handler.cell.NoOfChildrenCellHandler;
import com.transretail.crm.template.service.streaming.excel.handler.cell.NpwpAddressHandler;
import com.transretail.crm.template.service.streaming.excel.handler.cell.NpwpIdHandler;
import com.transretail.crm.template.service.streaming.excel.handler.cell.NpwpNameHandler;
import com.transretail.crm.template.service.streaming.excel.handler.cell.PositionHandler;
import com.transretail.crm.template.service.streaming.excel.handler.cell.ReligionCellHandler;
import com.transretail.crm.template.service.streaming.excel.handler.sheet.AbstractSheetHandler;

/**
 * @author Allan G. Ramirez (agramirez@exist.com)
 */
public class UpdateProfHandler extends AbstractSheetHandler {
    private static final Pattern NPWPID_PATTERN = Pattern.compile("^\\d{14,15}$");
    private static final String HEADER_ACCOUNTID = "ACCOUNTNUMBER";
    private static final String HEADER_BUSINESSNAME = "BUSINESSNAME";
    private static final String HEADER_BUSINESSLICENSE = "BUSINESSLICENSE";
    private static final String HEADER_NPWPID = "NPWPID";
    private static final String HEADER_NPWPNAME = "NPWPNAME";
    private static final String HEADER_NPWPADDRESS = "NPWPADDRESS";
    private static final String HEADER_FIRSTNAME = "FIRSTNAME";
    private static final String HEADER_LASTNAME = "LASTNAME";
    private static final String HEADER_POSITION = "POSITION";
    private static final String HEADER_GENDER = "GENDER";
    private static final String HEADER_DOB = "DATEOFBIRTH";
    private static final String HEADER_MOBILEPHONE = "MOBILEPHONE";
    private static final String HEADER_BESTTIMETOCALL = "BESTTIMETOCALL";
    private static final String HEADER_EMAIL = "EMAIL";
    private static final String HEADER_STATUS = "STATUS";
    private static final String HEADER_RELIGION = "RELIGION";
    private static final String HEADER_MARITALSTATUS = "MARITALSTATUS";
    private static final String HEADER_NOOFCHILDREN = "NOOFCHILDREN";
    private static final String HEADER_NATIONALITY = "NATIONALITY";
    private static final String HEADER_EDUCATION = "EDUCATION";
    private static final String HEADER_KTPID = "KTPID";

    @Resource(name = "passwordEncoder")
    private PasswordEncoder passwordEncoder;

    private Map<String, CellHandler> cellHandlerClassMap = Maps.newHashMap();
    private CellHandler[] cellHandlers = null;
    private DateTime currentDateTime;
    private CustomSecurityUserDetailsImpl currentUser;

    public UpdateProfHandler(DateTime currentDateTime, CustomSecurityUserDetailsImpl currentUser) {
        this.currentDateTime = currentDateTime;
        this.currentUser = currentUser;
        cellHandlerClassMap.put(HEADER_ACCOUNTID, new AccountIdCellHandler());
        cellHandlerClassMap.put(HEADER_BUSINESSNAME, new BusinessNameHandler());
        cellHandlerClassMap.put(HEADER_BUSINESSLICENSE, new BusinessLicenseCellHandler());
        cellHandlerClassMap.put(HEADER_NPWPID, new NpwpIdHandler());
        cellHandlerClassMap.put(HEADER_NPWPNAME, new NpwpNameHandler());
        cellHandlerClassMap.put(HEADER_NPWPADDRESS, new NpwpAddressHandler());
        cellHandlerClassMap.put(HEADER_FIRSTNAME, new FirstNameCellHandler());
        cellHandlerClassMap.put(HEADER_LASTNAME, new LastNameCellHandler());
        cellHandlerClassMap.put(HEADER_POSITION, new PositionHandler(this, HEADER_POSITION));
        cellHandlerClassMap.put(HEADER_GENDER, new GenderCellHandler(this));
        cellHandlerClassMap.put(HEADER_DOB, new BirthDateCellHandler(this));
        cellHandlerClassMap.put(HEADER_MOBILEPHONE, new MobilePhoneCellHandler());
        cellHandlerClassMap.put(HEADER_BESTTIMETOCALL, new BestTimeToCellHandler(this));
        cellHandlerClassMap.put(HEADER_EMAIL, new EmailCellHandler());
        cellHandlerClassMap.put(HEADER_STATUS, new AccountStatusHandler(this));
        cellHandlerClassMap.put(HEADER_RELIGION, new ReligionCellHandler(this));
        cellHandlerClassMap.put(HEADER_MARITALSTATUS, new MaritalStatusCellHandler(this));
        cellHandlerClassMap.put(HEADER_NOOFCHILDREN, new NoOfChildrenCellHandler());
        cellHandlerClassMap.put(HEADER_NATIONALITY, new NationalityCellHandler(this));
        cellHandlerClassMap.put(HEADER_EDUCATION, new EducationCellHandler(this));
        cellHandlerClassMap.put(HEADER_KTPID, new KtpIdCellHandler());
        cellHandlers = new CellHandler[cellHandlerClassMap.size()];
    }

    @Override
    protected void processHeaderCell(int i, String headerName) {
        CellHandler cellHandler = cellHandlerClassMap.get(headerName);
        if (cellHandler == null) {
            throw new HandlerException(getMessageSource()
                .getMessage("member.template.messages.excel.nohandler", new String[]{sheetName, headerName},
                    LocaleContextHolder.getLocale()));
        }
        cellHandlers[i] = cellHandler;
    }

    @Override
    protected CellHandler getCellHandler(int index) {
        return cellHandlers[index];
    }

    @Override
    protected void saveOrUpdateDto(MemberModel dto) throws HandlerException {
        if (StringUtils.isBlank(dto.getContact())) {
            dto.setContact(dto.getAccountId());
        }

        Locale locale = LocaleContextHolder.getLocale();
        List<String> errors = Lists.newArrayList();

        String npwpId = dto.getNpwp().getNpwpId();
        if (isNotBlank(npwpId)) {
            npwpId = npwpId.trim();
            if (!NPWPID_PATTERN.matcher(npwpId).matches()) {
                errors.add(
                    getMessageSource().getMessage("member.template.messages.excel.npwpid.invalid", new String[]{npwpId}, locale));
            }
            if (npwpId.length() < 15) {
                npwpId = "0" + npwpId;
            }
            dto.getNpwp().setNpwpId(npwpId);
        }

        Date birthDate = dto.getCustomerProfile().getBirthdate();
        String accountId = dto.getAccountId();
        if (isBlank(accountId)) {
            errors.add(getMessageSource().getMessage("member.template.messages.excel.columns.required",
                new String[]{sheetName, String.valueOf(currentRowNum), StringUtils
                    .join(new String[]{HEADER_ACCOUNTID},
                        ", ")},
                locale
            ));
        } else if (accountId.length() != 13 || accountId.charAt(2) != '0' || accountId.charAt(3) != '2') {
            throw new HandlerException(getMessageSource().getMessage("member.template.messages.excel.accountid.invalid",
                new String[]{sheetName, accountId, "02"},
                LocaleContextHolder.getLocale()));
        }
        if (isBlank(dto.getContact())) {
            errors.add(getMessageSource().getMessage("member.template.messages.excel.columns.required",
                new String[]{sheetName, String.valueOf(currentRowNum), StringUtils
                    .join(new String[]{HEADER_MOBILEPHONE},
                        ", ")},
                locale
            ));
        }
        if (birthDate == null) {
            errors.add(getMessageSource().getMessage("member.template.messages.excel.columns.required",
                new String[]{sheetName, String.valueOf(currentRowNum), StringUtils
                    .join(new String[]{HEADER_DOB},
                        ", ")},
                locale
            ));
        }
        if (isBlank(dto.getIdNumber())) {
            errors.add(getMessageSource().getMessage("member.template.messages.excel.columns.required",
                new String[]{sheetName, String.valueOf(currentRowNum), StringUtils
                    .join(new String[]{HEADER_KTPID},
                        ", ")},
                locale
            ));
        }
        QMemberModel qMemberModel = QMemberModel.memberModel;
        MemberModel model =
            new JPAQuery(em).from(qMemberModel).where(qMemberModel.accountId.eq(dto.getAccountId())).singleResult(qMemberModel);
        if (model == null) {
            throw new HandlerException(getMessageSource().getMessage("member.template.messages.excel.accountid.notfound",
                new String[]{sheetName, String.valueOf(currentRowNum), dto.getAccountId()},
                LocaleContextHolder.getLocale()));
        }
// Task #88915
//        if (StringUtils.isNotBlank(dto.getUsername()) && !dto.getUsername().equals(model.getUsername())) {
//            if (usernameExists(dto.getAccountId(), dto.getUsername())) {
//                errors.add(getMessageSource().getMessage("member.template.messages.excel.username.exists",
//                    new String[]{sheetName, dto.getUsername()}, locale));
//            }
//        }
        if (isNotBlank(dto.getContact()) && !dto.getContact().equals(model.getContact())) {
            if (contactExists(dto.getAccountId(), dto.getContact())) {
                errors.add(getMessageSource().getMessage("member.template.messages.excel.mobile.exists",
                    new String[]{sheetName, dto.getContact()}, locale));
            }
            // If username = contact, meaning it's unchanged so we might as well update username if contact changed
            if (model.getUsername().equals(model.getContact())) {
                model.setUsername(dto.getContact());
            }
        }
        if (isNotBlank(dto.getKtpId()) && !dto.getKtpId().equals(model.getKtpId()) && idNumberExists(dto.getAccountId(),
            dto.getKtpId())) {
            errors.add(getMessageSource().getMessage("member.template.messages.excel.ktid.exists",
                new String[]{sheetName, dto.getIdNumber().substring("KTP-".length())}, locale));
        }
        if (isNotBlank(dto.getEmail()) && !dto.getEmail().equals(model.getEmail())) {
            if (!validEmail(dto.getEmail())) {
                errors.add(getMessageSource()
                    .getMessage("member.template.messages.excel.email.invalid", new String[]{sheetName, dto.getEmail()}, locale));
            } else if (emailExists(dto.getAccountId(), dto.getEmail())) {
                errors.add(getMessageSource().getMessage("member.template.messages.excel.email.exists",
                    new String[]{sheetName, dto.getEmail()}, locale));
            }
        }

        if (errors.size() > 0) {
            throw new HandlerException(StringUtils.join(errors, "\r\n"));
        }

        if (model.getProfessionalProfile() == null) {
            model.setProfessionalProfile(new ProfessionalProfile());
        }
        model.getProfessionalProfile().setBusinessName(dto.getProfessionalProfile().getBusinessName());
        model.getProfessionalProfile().setBusinessLicense(dto.getProfessionalProfile().getBusinessLicense());
        if (model.getNpwp() == null) {
            model.setNpwp(new Npwp());
        }
        model.getNpwp().setNpwpId(dto.getNpwp().getNpwpId());
        model.getNpwp().setNpwpName(dto.getNpwp().getNpwpName());
        model.getNpwp().setNpwpAddress(dto.getNpwp().getNpwpAddress());
        model.setFirstName(dto.getFirstName());
        model.setLastName(dto.getLastName());
        model.getProfessionalProfile().setPosition(dto.getProfessionalProfile().getPosition());
        model.setIdNumber(dto.getIdNumber());
        if (model.getCustomerProfile() == null) {
            model.setCustomerProfile(new CustomerProfile());
        }
        model.getCustomerProfile().setGender(dto.getCustomerProfile().getGender());
        model.getCustomerProfile().setBirthdate(dto.getCustomerProfile().getBirthdate());
        model.setContact(dto.getContact());
        model.setBestTimeToCall(dto.getBestTimeToCall());
        model.setEmail(dto.getEmail());
        // Bug #88757
        if (dto.getAccountStatus() != null) {
            model.setAccountStatus(dto.getAccountStatus());
        }
        model.getCustomerProfile().setReligion(dto.getCustomerProfile().getReligion());
        model.getCustomerProfile().setMaritalStatus(dto.getCustomerProfile().getMaritalStatus());
        model.getCustomerProfile().setChildren(dto.getCustomerProfile().getChildren());
        model.getCustomerProfile().setNationality(dto.getCustomerProfile().getNationality());
        model.getCustomerProfile().setEducation(dto.getCustomerProfile().getEducation());
        // KTP ID
        model.setIdNumber(dto.getIdNumber());

        if (StringUtils.isBlank(model.getPassword())) {
            // Task #84568 / Task #88915
            String formattedBirthDate = BIRTHDATE_PASSWORD_FORMAT.format(birthDate);
            model.setPassword(passwordEncoder.encode(formattedBirthDate));
        }
        if (StringUtils.isBlank(model.getPin())) {
            // Bug #85179
            try {
                model.setPin(SimpleEncrytor.getInstance().encrypt(BIRTHDATE_PIN_FORMAT.format(birthDate)));
                model.setPinEncryped(true);
            } catch (SimpleEncrytor.EncryptDecryptException e) {
                throw new HandlerException(e);
            }
        }

        model.setLastUpdated(currentDateTime);
        model.setLastUpdateUser(currentUser.getUsername());

        em.merge(model);

        if (currentRowNum % batchSize == 0) {
            cleanup();
        }
    }

    @Override
    public int getExpectedNumberOfColumns() {
        return cellHandlers.length;
    }
}
