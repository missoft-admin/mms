package com.transretail.crm.report.template.impl;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.apache.commons.lang3.StringUtils;
import org.joda.time.LocalDate;
import org.joda.time.LocalDateTime;
import org.joda.time.YearMonth;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Service;

import com.google.common.collect.Maps;
import com.google.common.collect.Sets;
import com.mysema.query.BooleanBuilder;
import com.mysema.query.jpa.JPASubQuery;
import com.mysema.query.jpa.impl.JPAQuery;
import com.mysema.query.types.Projections;
import com.mysema.query.types.QBean;
import com.mysema.query.types.expr.CaseBuilder;
import com.mysema.query.types.expr.NumberExpression;
import com.mysema.query.types.path.EnumPath;
import com.mysema.query.types.path.NumberPath;
import com.mysema.query.types.query.NumberSubQuery;
import com.transretail.crm.common.reporting.engine.data.JRQueryDSLDataSource;
import com.transretail.crm.common.reporting.engine.data.ReportDataSource;
import com.transretail.crm.common.reporting.jasper.ClasspathFileResolver;
import com.transretail.crm.common.reporting.jasper.DefaultJRProcessor;
import com.transretail.crm.common.reporting.jasper.JRProcessor;
import com.transretail.crm.common.util.DateUtil;
import com.transretail.crm.core.entity.enums.ReportType;
import com.transretail.crm.giftcard.entity.QGiftCardInventory;
import com.transretail.crm.giftcard.entity.QGiftCardTransaction;
import com.transretail.crm.giftcard.entity.QGiftCardTransactionItem;
import com.transretail.crm.giftcard.entity.QProductProfile;
import com.transretail.crm.giftcard.entity.support.GiftCardSaleTransaction;
import com.transretail.crm.giftcard.entity.support.GiftCardSalesType;
import com.transretail.crm.report.template.CommonReportFilter;
import com.transretail.crm.report.template.FilterField;
import com.transretail.crm.report.template.NoDataFoundSupport;
import com.transretail.crm.report.template.ReportTemplate;

@Service("giftCardAgedAnalysisReport")
public class GiftCardAgedAnalysisReport implements ReportTemplate, NoDataFoundSupport {
	protected static final String TEMPLATE_NAME = "Analysis Report";//"Aged Analysis By Month and Year";
    protected static final String REPORT_NAME = "reports/gift_card_aged_analysis_report.jasper";
    protected static final String FILTER_DATE_TO = CommonReportFilter.DATE_TO;
    protected static final String FILTER_DATE_FROM = CommonReportFilter.DATE_FROM;
    
    private static final Logger LOGGER = LoggerFactory.getLogger(GiftCardAgedAnalysisReport.class);
    private static final DateTimeFormatter INPUT_DATE_PATTERN = DateTimeFormat.forPattern(DateUtil.SEARCH_DATE_FORMAT);
    
    @PersistenceContext
    private EntityManager em;
    @Autowired
    private MessageSource messageSource;
    
    @Override
    public ReportType[] getReportTypes() {
    	return ReportType.values();
    }
    
    @Override
    public String getName() {
    	return TEMPLATE_NAME;
    }

    @Override
    public String getDescription() {
    	return messageSource.getMessage("gift.card.agedanalysis.report.menu", (Object[]) null, LocaleContextHolder.getLocale());
    }
    
    @Override
    public boolean canView(Set<String> permissions) {
    	return true;
    }

    public enum AnalysisType {
    	DAILY, MONTHLY
    }
    
    @Override
    public Set<FilterField> getFilters() {
    	
    	
		Set<FilterField> filters = Sets.newLinkedHashSet();
		filters.add(FilterField.createDateField(FILTER_DATE_FROM,
				messageSource.getMessage("label_date_from", (Object[]) null, LocaleContextHolder.getLocale()),
				DateUtil.SEARCH_DATE_FORMAT, true));
		filters.add(FilterField.createDateField(FILTER_DATE_TO,
				messageSource.getMessage("label_date_to", (Object[]) null, LocaleContextHolder.getLocale()),
				DateUtil.SEARCH_DATE_FORMAT, true));
		
		Map<String, String> products = new LinkedHashMap<String, String>();
		products.put("", "");
		products.putAll(getProducts());
		filters.add(FilterField.createDropdownField("product",
                messageSource.getMessage("gift.card.agedanalysis.report.product", (Object[]) null, LocaleContextHolder.getLocale()), products));
		
		Map<String, String> salesTypes = new LinkedHashMap<String, String>();
		salesTypes.put("", "");
		for (GiftCardSalesType key : GiftCardSalesType.values()) {
			salesTypes.put(key.toString(), key.toString());
		}
		filters.add(FilterField.createDropdownField("salesType",
                messageSource.getMessage("gift.card.agedanalysis.report.salestype", (Object[]) null, LocaleContextHolder.getLocale()), salesTypes));
		
		Map<String, String> analysisType = new LinkedHashMap<String, String>();
		for (AnalysisType key : AnalysisType.values()) {
			analysisType.put(key.toString(), key.toString());
		}
		filters.add(FilterField.createDropdownField("analysisType",
                messageSource.getMessage("gift.card.agedanalysis.analysistype", (Object[]) null, LocaleContextHolder.getLocale()), analysisType, true));
		
		return filters;
    }
    
    private Map<String, String> getProducts() {
    	QProductProfile qPrd = QProductProfile.productProfile;
    	return new JPAQuery(em).from(qPrd).orderBy(qPrd.productDesc.asc()).map(qPrd.id.stringValue(), qPrd.productDesc);
    }
    
    public JRProcessor createJrProcessor(Map<String, String> map) {
    	DefaultJRProcessor jrProcessor = new DefaultJRProcessor(REPORT_NAME);
        jrProcessor.setFileResolver(new ClasspathFileResolver("reports"));
        jrProcessor.addParameter("SUB_DATA_SOURCE", createDataSource(map));
        
        /*PARAMS*/
        LocalDateTime startDate = INPUT_DATE_PATTERN.parseLocalDate(map.get(FILTER_DATE_FROM)).toDateTimeAtStartOfDay().toLocalDateTime();
		LocalDateTime endDate = INPUT_DATE_PATTERN.parseLocalDate(map.get(FILTER_DATE_TO)).plusDays(1).toDateTimeAtStartOfDay().minusMillis(1).toLocalDateTime();

		Long prodId = StringUtils.isNotBlank(map.get("product")) ? Long.valueOf(map.get("product")) : null;
		GiftCardSalesType salesType = StringUtils.isNotBlank(map.get("salesType")) ? GiftCardSalesType.valueOf(map.get("salesType")) : null;
		AnalysisType analysisType = AnalysisType.valueOf(map.get("analysisType"));
        Map<String, Object> paramMap = prepareReportParameters(startDate, endDate, prodId, salesType, analysisType);
        /*END OF PARAMS*/
        jrProcessor.addParameters(paramMap);
    	
		return jrProcessor;
    }
    
    
    public static class TransactionDateBean {
    	private Integer year;
    	private Integer month;
    	private Integer dayOfMonth;
    	public TransactionDateBean() {}
		public TransactionDateBean(Integer year, Integer month) {
			super();
			this.year = year;
			this.month = month;
		}
		public TransactionDateBean(Integer year, Integer month,
				Integer dayOfMonth) {
			super();
			this.year = year;
			this.month = month;
			this.dayOfMonth = dayOfMonth;
		}
		public Integer getYear() {
			return year;
		}
		public void setYear(Integer year) {
			this.year = year;
		}
		public Integer getMonth() {
			return month;
		}
		public void setMonth(Integer month) {
			this.month = month;
		}
		public Integer getDayOfMonth() {
			return dayOfMonth;
		}
		public void setDayOfMonth(Integer dayOfMonth) {
			this.dayOfMonth = dayOfMonth;
		}
    	
    }
    
    
	public ReportDataSource createDataSource(Map<String, String> parameters) {
    	LocalDateTime startDate = INPUT_DATE_PATTERN.parseLocalDate(parameters.get(FILTER_DATE_FROM)).toDateTimeAtStartOfDay().toLocalDateTime();
		LocalDateTime endDate = INPUT_DATE_PATTERN.parseLocalDate(parameters.get(FILTER_DATE_TO)).plusDays(1).toDateTimeAtStartOfDay().minusMillis(1).toLocalDateTime();

		Long prodId = StringUtils.isNotBlank(parameters.get("product")) ? Long.valueOf(parameters.get("product")) : null;
		GiftCardSalesType salesType = StringUtils.isNotBlank(parameters.get("salesType")) ? GiftCardSalesType.valueOf(parameters.get("salesType")) : null;
		AnalysisType analysisType = AnalysisType.valueOf(parameters.get("analysisType"));
		
    	GiftCardSaleTransaction[] validTxns = {
    			GiftCardSaleTransaction.ACTIVATION,
    			GiftCardSaleTransaction.VOID_ACTIVATED,
    			GiftCardSaleTransaction.REDEMPTION,
    			GiftCardSaleTransaction.VOID_REDEMPTION,
    	};
		
		QGiftCardTransactionItem qtxitem = QGiftCardTransactionItem.giftCardTransactionItem;
		QGiftCardTransaction qtx = QGiftCardTransaction.giftCardTransaction;
		
		BooleanBuilder filter = new BooleanBuilder(qtx.transactionType.in(validTxns)
    			.and(qtx.transactionDate.goe(startDate))
    			.and(qtx.transactionDate.loe(endDate))
    			.and(qtxitem.giftCard.salesType.isNotNull()));
    	if(prodId != null)
    		filter.and(qtxitem.giftCard.profile.id.eq(prodId));
    	if(salesType != null)
    		filter.and(qtxitem.giftCard.salesType.eq(salesType));
    	
    	EnumPath<GiftCardSaleTransaction> transactionType = qtx.transactionType;
    	NumberPath<Double> transactionAmt = qtxitem.transactionAmount;
    	
    	NumberExpression<Double> activation = new CaseBuilder()
		.when(transactionType.eq(GiftCardSaleTransaction.ACTIVATION)).then(transactionAmt)
		.when(transactionType.eq(GiftCardSaleTransaction.VOID_ACTIVATED)).then(transactionAmt.negate())
		.otherwise(0.0);
    	NumberExpression<Double> redemption = new CaseBuilder()
		.when(transactionType.eq(GiftCardSaleTransaction.REDEMPTION)).then(transactionAmt.negate())
		.when(transactionType.eq(GiftCardSaleTransaction.VOID_REDEMPTION)).then(transactionAmt)
		.otherwise(0.0);
    	
    	QGiftCardInventory qinventory = QGiftCardInventory.giftCardInventory;
    	BooleanBuilder subqFilter = new BooleanBuilder(qinventory.expiryDate.year().eq(qtx.transactionDate.year())
    			.and(qinventory.expiryDate.month().eq(qtx.transactionDate.month()))
    			.and(qinventory.salesType.isNotNull())) ;
    	if(analysisType.compareTo(AnalysisType.DAILY) == 0)
    		subqFilter.and(qinventory.expiryDate.dayOfMonth().eq(qtx.transactionDate.dayOfMonth()));
    	if(prodId != null)
    		subqFilter.and(qinventory.profile.id.eq(prodId));
    	if(salesType != null)
    		subqFilter.and(qinventory.salesType.eq(salesType));
    	NumberSubQuery<Double> expired = new JPASubQuery().from(qinventory).where(subqFilter).unique(qinventory.balance.sum());
    	
    	JPAQuery query = new JPAQuery(em).from(qtxitem)
    			.leftJoin(qtxitem.transaction, qtx)
				.where(filter);
    	
    	QBean<TransactionBean> projection = null;
    	
    	if(analysisType.compareTo(AnalysisType.DAILY) == 0) {
    		query.groupBy(qtx.transactionDate.year(), qtx.transactionDate.month(), qtx.transactionDate.dayOfMonth())
    		.orderBy(qtx.transactionDate.year().asc(), qtx.transactionDate.month().asc(), qtx.transactionDate.dayOfMonth().asc());
    		projection = Projections.bean(TransactionBean.class, 
    				qtx.transactionDate.year().as("year"), 
    				qtx.transactionDate.month().as("month"), 
    				qtx.transactionDate.dayOfMonth().as("dayOfMonth"),
    				activation.sum().as("activation"),
        			redemption.sum().as("redeem"),
        			expired.as("expired"));
    	} else if(analysisType.compareTo(AnalysisType.MONTHLY) == 0) {
    		query.groupBy(qtx.transactionDate.year(), qtx.transactionDate.month())
    		.orderBy(qtx.transactionDate.year().asc(), qtx.transactionDate.month().asc());
    		projection = Projections.bean(TransactionBean.class, 
    				qtx.transactionDate.year().as("year"), 
    				qtx.transactionDate.month().as("month"),
    				activation.sum().as("activation"),
        			redemption.sum().as("redeem"),
        			expired.as("expired"));
    	}
    	
		return new JRQueryDSLDataSource(query, projection, null);
	}
    
    
    private Map<String, Object> prepareReportParameters(LocalDateTime start, LocalDateTime end, Long prodId, GiftCardSalesType salesType, AnalysisType analysisType) {
    	HashMap<String, Object> reportMap = Maps.newHashMap();
    	reportMap.put("START_DATE", start);
    	reportMap.put("END_DATE", end);
    	if(prodId != null) {
	    	reportMap.put("PRODUCT_ID", prodId);
	    	reportMap.put("PRODUCT_DESC", getProducts().get(Long.toString(prodId)));
    	}
    	reportMap.put("SALES_TYPE", salesType != null ? salesType.toString() : "");
    	reportMap.put("ANALYSIS_TYPE", analysisType.toString());
    	return reportMap;
    }
    
    public static class TransactionBean {
    	private Integer year;
    	private Integer month;
    	private Integer dayOfMonth;
    	private Double activation = 0d;
    	private Double redeem = 0d;
    	private Double expired = 0d;
    	public TransactionBean() {}
    	
    	public TransactionBean(Integer year, Integer month, Integer dayOfMonth, Double activation, Double redeem, Double expired) {
    		this.year = year;
    		this.month = month;
    		this.dayOfMonth = dayOfMonth;
    		
    		this.activation = activation == null ? 0 : activation;
    		this.redeem = redeem == null ? 0 : 0 - redeem;
    		this.expired = expired == null ? 0 : expired;
    	}
    	
    	public TransactionBean(Integer year, Integer month, Double activation, Double redeem, Double expired) {
    		this.year = year;
    		this.month = month;

    		this.activation = activation == null ? 0 : activation;
    		this.redeem = redeem == null ? 0 : 0 - redeem;
    		this.expired = expired == null ? 0 : expired;
    	}
    	
    	public String getDateStr() {
    		if(year == null || month == null)
    			return "";
    		if(dayOfMonth == null)
    			return new YearMonth(year, month).toString("MM-yyyy");
    		else
    			return new LocalDate(year, month, dayOfMonth).toString("dd-MM-yyyy");
    	}
		
		public Integer getDayOfMonth() {
			return dayOfMonth;
		}

		public void setDayOfMonth(Integer dayOfMonth) {
			this.dayOfMonth = dayOfMonth;
		}

		public Double getExpired() {
			return expired;
		}

		public void setExpired(Double expired) {
			this.expired = expired;
		}

		public Integer getYear() {
			return year;
		}
		public void setYear(Integer year) {
			this.year = year;
		}
		
		public Integer getMonth() {
			return month;
		}
		public void setMonth(Integer month) {
			this.month = month;
		}
		public Double getActivation() {
			return activation;
		}
		public void setActivation(Double activation) {
			this.activation = activation;
		}
		public Double getRedeem() {
			return redeem;
		}
		public void setRedeem(Double redeem) {
			this.redeem = redeem;
		}
		public Double getRxP() {
			if(activation != 0)
				return (redeem / activation) * 100;
			return new Double(0);
		}
		public Double getOutstanding() {
			return activation - redeem;
		}
		public Double getOutP() {
			if(activation != 0)
				return (getOutstanding() / activation) * 100;
			return new Double(0);
		}
		public Double getExpiry() {
			return expired;
		}
		public Double getExpP() {
			if(activation != 0)
				return (expired / activation) * 100;
			return new Double(0);
		}
	
    }

	@Override
	public boolean isEmpty(Map<String, String> parameters) {
		LocalDateTime startDate = INPUT_DATE_PATTERN.parseLocalDate(parameters.get(FILTER_DATE_FROM)).toDateTimeAtStartOfDay().toLocalDateTime();
		LocalDateTime endDate = INPUT_DATE_PATTERN.parseLocalDate(parameters.get(FILTER_DATE_TO)).plusDays(1).toDateTimeAtStartOfDay().minusMillis(1).toLocalDateTime();

		Long prodId = StringUtils.isNotBlank(parameters.get("product")) ? Long.valueOf(parameters.get("product")) : null;
		GiftCardSalesType salesType = StringUtils.isNotBlank(parameters.get("salesType")) ? GiftCardSalesType.valueOf(parameters.get("salesType")) : null;
		
    	GiftCardSaleTransaction[] validTxns = {
    			GiftCardSaleTransaction.ACTIVATION,
    			GiftCardSaleTransaction.VOID_ACTIVATED,
    			GiftCardSaleTransaction.REDEMPTION,
    			GiftCardSaleTransaction.VOID_REDEMPTION,
    	};
		
		QGiftCardTransactionItem qtxitem = QGiftCardTransactionItem.giftCardTransactionItem;
		QGiftCardTransaction qtx = QGiftCardTransaction.giftCardTransaction;
		
		BooleanBuilder filter = new BooleanBuilder(qtx.transactionType.in(validTxns)
    			.and(qtx.transactionDate.goe(startDate))
    			.and(qtx.transactionDate.loe(endDate))
    			.and(qtxitem.giftCard.salesType.isNotNull()));
    	if(prodId != null)
    		filter.and(qtxitem.giftCard.profile.id.eq(prodId));
    	if(salesType != null)
    		filter.and(qtxitem.giftCard.salesType.eq(salesType));
		
		JPAQuery query = new JPAQuery(em).from(qtxitem)
    			.leftJoin(qtxitem.transaction, qtx)
				.where(filter);
		
		return !query.exists();
	}

	
}
