package com.transretail.crm.web.controller;


import java.util.ArrayList;
import java.util.Map;

import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.StringUtils;
import org.joda.time.LocalDate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.transretail.crm.common.reporting.jasper.JRProcessor;
import com.transretail.crm.common.service.dto.response.ResultList;
import com.transretail.crm.core.entity.enums.ReportType;
import com.transretail.crm.core.security.model.impl.CustomSecurityUserDetailsImpl;
import com.transretail.crm.core.security.util.UserUtil;
import com.transretail.crm.core.service.CodePropertiesService;
import com.transretail.crm.core.service.LookupService;
import com.transretail.crm.giftcard.dto.GiftCardInventorySearchDto;
import com.transretail.crm.giftcard.dto.GiftCardInventoryStockDto;
import com.transretail.crm.giftcard.dto.GiftCardInventoryStockSearchDto;
import com.transretail.crm.giftcard.entity.GiftCardInventoryStock.GCIStockStatus;
import com.transretail.crm.giftcard.service.CardVendorService;
import com.transretail.crm.giftcard.service.GiftCardInventoryStockService;
import com.transretail.crm.report.template.impl.GiftCardInventoryStockDetailedListDocument;
import com.transretail.crm.report.template.impl.GiftCardInventoryStockSummaryListDocument;
import com.transretail.crm.web.controller.util.ControllerResponse;


@Controller
@RequestMapping("/giftcard/inventory")
public class GiftCardInventoryStockController extends AbstractReportsController {

	private static final String PATH_MAIN 			= "/giftcard/inventory/stock";
	private static final String UIATTR_SEARCHFORM	= "inventorySearch";



	@Autowired
	GiftCardInventoryStockService service;
	@Autowired
	LookupService lookupService;
	@Autowired
	CodePropertiesService codePropertiesService;
    @Autowired
    private MessageSource messageSource;
    @Autowired
    private GiftCardInventoryStockSummaryListDocument inventoryStockListDocument;
    @Autowired
    private GiftCardInventoryStockDetailedListDocument inventoryStockDetailedListDocument;
    @Autowired
    private CardVendorService cardVendorService;



    @RequestMapping( value="/stock", produces = "text/html" )
    public ModelAndView showMain() {
    	ModelAndView mv = new ModelAndView( PATH_MAIN );
    	CustomSecurityUserDetailsImpl user = UserUtil.getCurrentUser();
    	GiftCardInventoryStockSearchDto searchDto = new GiftCardInventoryStockSearchDto();
    	searchDto.setLocation( user.getInventoryLocation() );
    	mv.addObject( UIATTR_SEARCHFORM, searchDto );
    	mv.addObject( "listUrl", "/giftcard/inventory/stock/list" );
    	mv.addObject( "locations", service.getLocations( user.getInventoryLocation() ) );
    	mv.addObject( "cardTypes", service.getCardTypes( user.getInventoryLocation() ) );
    	mv.addObject("vendors", cardVendorService.getAllCardVendorsByFormalName());
    	mv.addObject( "reportTypes", ReportType.values() );
    	mv.addObject( "txnStatus", GCIStockStatus.values() );
    	
        return mv;
    }

    @RequestMapping(value = "/stock/list", method = RequestMethod.POST)
    public @ResponseBody ResultList<GiftCardInventoryStockDto> list( @RequestBody GiftCardInventoryStockSearchDto dto ) {
    	CustomSecurityUserDetailsImpl user = UserUtil.getCurrentUser();
    	if ( StringUtils.isNotBlank( user.getInventoryLocation() ) ) {
    		if ( StringUtils.isBlank( dto.getLocation() ) ) {
            	dto.setLocation( user.getInventoryLocation() );
    		}
        	return service.searchStock( dto );
    	}
    	return new ResultList<GiftCardInventoryStockDto>( new ArrayList<GiftCardInventoryStockDto>() );
    }

    @RequestMapping(value = "/stock/print/{reportType}")
	public void reprintTransfer(
			@RequestParam Map<String, String> reqParams,
			@PathVariable String reportType,
			HttpServletResponse response ) throws Exception {

    	GiftCardInventoryStockSearchDto dto = new GiftCardInventoryStockSearchDto();
		if( StringUtils.isNotBlank( reqParams.get( "createdFromMilli" ) ) ) {
			dto.setCreatedFromMilli( Long.valueOf( reqParams.get( "createdFromMilli" ) ) );
		}
		if( StringUtils.isNotBlank( reqParams.get( "createdToMilli" ) ) ) {
			dto.setCreatedFromMilli( Long.valueOf( reqParams.get( "createdToMilli" ) ) );
		}
		dto.setLocation( reqParams.get( "location" ) );
		dto.setCardType( reqParams.get( "cardType" ) );

		JRProcessor jrProcessor = service.createJrProcessor( dto );
		if ( ReportType.PDF.getCode().equals( reportType ) ) {
        	renderReport( response, jrProcessor );
		}
        else if ( ReportType.EXCEL.getCode().equals( reportType ) ) {
        	renderExcelReport( response, jrProcessor, 
        			messageSource.getMessage( "tracker_report_title", null, LocaleContextHolder.getLocale() ) );
        }
	}

    @RequestMapping(value = "/stock/cards/list/{location}", method = RequestMethod.POST)
    public @ResponseBody ControllerResponse listLocation( @PathVariable( "location" ) String location ) {
    	ControllerResponse resp = new ControllerResponse();
    	if ( StringUtils.isNotBlank( location ) ) {
    		resp.setResult( service.getCardTypes( location ) );
    		resp.setSuccess( true );
    	}
    	return resp;
    }

    @RequestMapping(value = "/printlist/{type}/{cardType}/{location}/{vendor}")
    public void printList(
            @PathVariable String type,
            @PathVariable String cardType,
            @PathVariable String location,
            @PathVariable String vendor,
            HttpServletResponse response) throws Exception {
        GiftCardInventorySearchDto searchDto = new GiftCardInventorySearchDto();
        
        String inventoryLocation = UserUtil.getCurrentUser().getInventoryLocation();
        if (inventoryLocation == null)
            inventoryLocation = "";
        if(!inventoryLocation.equals(codePropertiesService.getDetailInvLocationHeadOffice())) {
            searchDto.setLocationCode(UserUtil.getCurrentUser().getInventoryLocation());
        } else {
            if (!location.equalsIgnoreCase("null"))
                searchDto.setLocationCode(location);
        }
        
        if (!cardType.equalsIgnoreCase("null"))
            searchDto.setProductCode(cardType);
        if (!vendor.equalsIgnoreCase("null"))
            searchDto.setVendorFormalName(vendor);
        if (type.equalsIgnoreCase("SUMMARY")) {
            JRProcessor jrProcessor = inventoryStockListDocument.createJrProcessor(searchDto);
            renderExcelReport(response, jrProcessor, "Gift Card Inventory Stock Summary List Document (" + LocalDate.now() +")");
        } else if (type.equalsIgnoreCase("DETAILED")) {
            JRProcessor jrProcessor = inventoryStockDetailedListDocument.createJrProcessor(searchDto);
            renderExcelReport(response, jrProcessor, "Gift Card Inventory Stock Detailed List Document (" + LocalDate.now() +")");
        }
    }

}
