package com.transretail.crm.core.service;

import java.util.List;
import java.util.Set;

import com.transretail.crm.core.entity.UserRoleModel;

public interface UserRoleModelService {

    long countAllUserRoleModels();

    void deleteUserRoleModel(UserRoleModel userRoleModel);

    UserRoleModel findUserRoleModel(String id);

    List<UserRoleModel> findAllUserRoleModels();

    List<UserRoleModel> findUserRoleModelEntries(int firstResult, int maxResults);

    void saveUserRoleModel(UserRoleModel userRoleModel);

    UserRoleModel updateUserRoleModel(UserRoleModel userRoleModel);

    Set<UserRoleModel> findByIdSet(String[] inIds);
}
