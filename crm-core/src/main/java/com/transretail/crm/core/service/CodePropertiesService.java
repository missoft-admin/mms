package com.transretail.crm.core.service;

public interface CodePropertiesService {

    String getHeaderTitle();

    String getHeaderGender();

    String getHeaderNationality();

    String getHeaderMaritalStatus();

    String getHeaderPreferredLanguage();

    String getHeaderAccountStatus();

    String getHeaderMemberType();

    String getHeaderMembershipTier();

    String getHeaderEducation();

    String getHeaderOccupation();

    String getHeaderPersonalIncome();

    String getHeaderHouseHoldIncome();

    String getHeaderFamilySize();

    String getHeaderChildren();

    String getHeaderPetOwnership();

    String getHeaderFoodPreference();

    String getHeaderInterest();

    String getHeaderPaymentType();

    String getHeaderPointsAdjustment();

    String getHeaderAgeFilter();

    String getHeaderDays();

    String getHeaderRewardType();

    String getHeaderInventoryLocation();

    String getHeaderMessageCode();

    String getHeaderEmployeeApprovedPaymentMedia();

    String getHeaderEmployeeApprovedCardFilter();

    String getHeaderCustomerGroup();

    String getHeaderCustomerSegmentation();

    String getHeaderVendors();

    String getHeaderEmpType();

    String getHeaderReligion();

    String getHeaderDepartmentFields();

    String getHeaderBusinessFields();

    String getHeaderBanks();

    String getHeaderNoOfEmps();

    String getHeaderCompany();
    
    String getHeaderCardTypes();

    String getDetailTitleMr();

    String getDetailTitleMrs();

    String getDetailTitleMiss();

    String getDetailTitleMadam();

    String getDetailGenderMale();

    String getDetailGenderFemale();

    String getDetailGenderNotSelected();

    String getDetailNationalityIndonesian();

    String getDetailNationalityFilipino();

    String getDetailNationalityAmerican();

    String getDetailNationalityChinese();

    String getDetailNationalityMalaysian();

    String getDetailMaritalStatusSingle();

    String getDetailMaritalStatusMarried();

    String getDetailMaritalStatusDivorce();

    String getDetailLanguageBahasa();

    String getDetailLanguageEnglish();

    String getDetailLanguageChinese();

    String getDetailAccountStatusActive();

    String getDetailAccountStatusDisable();

    String getDetailAccountStatusOnHold();

    String getDetailMemberTypeIndividual();

    String getDetailMemberTypeEmployee();

    String getDetailMemberTypeCompany();

    String getDetailMemberTypeProfessional();

    String getDetailMemberTierRegular();

    String getDetailMemberTierSilver();

    String getDetailMemberTierGold();

    String getDetailEducationNoCertificate();

    String getDetailEducationPrimarySchool();

    String getDetailEducationSecondarySchool();

    String getDetailEducationTertiarySchool();

    String getDetailEducationGraduateSchool();

    String getDetailEducationPhd();

    String getDetailOccupationJobless();

    String getDetailOccupationHousewife();

    String getDetailOccupationProfessional();

    String getDetailOccupationStudent();

    String getDetailPersonalIncomeBelow100();

    String getDetailPersonalIncome1001To3000();

    String getDetailPersonalIncome3001To4000();

    String getDetailPersonalIncome4001Above();

    String getDetailHouseholdIncomeBelow100();

    String getDetailHouseholdIncome1001To3000();

    String getDetailHouseholdIncome3001To4000();

    String getDetailHouseholdIncome4001Above();

    String getDetailFamilySize1();

    String getDetailFamilySize2To3();

    String getDetailFamilySize3To5();

    String getDetailFamilySize6orMore();

    String getDetailChildrenNone();

    String getDetailChildrenOne();

    String getDetailChildren2orMore();

    String getDetailPetDog();

    String getDetailPetCat();

    String getDetailPetFish();

    String getDetailFoodPrefLocal();

    String getDetailFoodPrefWestern();

    String getDetailFoodPrefChinese();

    String getDetailFoodPrefFast();

    String getDetailInterestReading();

    String getDetailInterestSports();

    String getDetailInterestOutdoors();

    String getDetailPaymentTypeCash();

    String getDetailPaymentTypeGiftCard();

    String getDetailPaymentTypeVoucher();

    String getDetailPaymentTypeLoyaltyPoints();

    String getDetailPaymentTypeFlazz();

    String getDetailPaymentTypeCoupon();

    String getDetailPaymentTypeInstallment();

    String getDetailPaymentTypeEft();

    String getMerchantServiceSupervisor();

    String getHrCode();

    String getHrsCode();

    String getCsCode();

    String getCssCode();

    String getMktHeadCode();

    String getDetailPointsAdjNotIssued();

    String getDetailPointsAdjDoublePointIssued();

    String getDetailPointsAdjDuplicatePoint();

    String getDetailPointsAdjWrongValue();

    String getDetailAgeBelow13();

    String getDetailAge14To18();

    String getDetailAge19To30();

    String getDetailAge31To45();

    String getDetailAge46Older();

    String getDetailDayMonday();

    String getDetailDayTuesday();

    String getDetailDayWednesday();

    String getDetailDayThursday();

    String getDetailDayFriday();

    String getDetailDaySaturday();

    String getDetailDaySunday();

    String getDetailRewardTypePoints();

    String getDetailRewardTypeDiscount();

    String getDetailRewardTypeItemPoint();

    String getDetailRewardTypeExchangeItem();

    String getDetailMessageCodeMemberNotFound();

    String getDetailMessageCodeNotEnoughPoints();

    String getDetailMessageCodeNoPointsEarned();

    String getDetailMessageCodeInternalError();

    String getDetailMessageCodeNoPaymentType();

    String getDetailMessageCodePaymentTypeNotAllowed();

    String getDetailMessageCodeInvalidPin();

    String getDetailMessageCodeInvalidParameter();

    String getDetailMessageCodeSupplementaryNotAllowed();

    String getDetailMessageCodeNoLoyaltyCard();

    String getDetailMessageCodeNoRecordFound();

    String getDetailMessageCodeLoyaltyCardExpired();

    String getDetailMessageCodeInvalidClientSignature();

    String getDetailMessageCodeInvalidStoreMemberAccount();

    String getDetailMessageCodeInvalidStoreMemberCompany();

    String getDetailMessageCodeInvalidStoreMemberCardType();

    String getDetailMessageCodeInvalidStoreMemberStore();

    String getDetailMessageCodeInvalidCard();


    String getDetailMessageCodeContactNumberAlreadyExist();

    String getDetailStoreHeadOffice();

    String getDetailStoreExhibit();

    String getMerchantServiceSupervisorCode();

    String getDetailOperandEq();

    String getDetailOperandGt();

    String getDetailOperandGe();

    String getDetailOperandLe();

    String getDetailOperandLt();

    String getHeaderOperands();

    String getHeaderStatus();

    String getDetailStatusActive();

    String getDetailStatusApproval();

    String getDetailStatusRejected();

    String getDetailStatusExpired();

    String getDetailStatusDeleted();

    String getDetailStatusDraft();

    String getDetailStatusDisabled();

	String getDetailStatusCancelled();

    String getHeaderFieldTypes();

    String getDetailFieldTypeString();

    String getDetailFieldTypeNumeric();

    String getHeaderProductGrpClassification();

    String getHeaderProductGrpSubclassification();

    String getHeaderProductClassification();

    String getHeaderMoStatus();

    String getDetailMoStatusDrafted();

    String getDetailMoStatusForApproval();

    String getDetailMoStatusApproved();

    String getDetailInvLocationHeadOffice();

    String getDetailInvLocationLebakBulus();

    String getDetailInvLocationDenpasar();

    String getDetailInvLocationExhibit();

    String getHeaderLoyaltyStatus();

    String getDetailLoyaltyStatusInactive();

    String getDetailLoyaltyStatusForAllocation();

    String getDetailLoyaltyStatusAllocated();

    String getDetailLoyaltyStatusInTransit();

    String getDetailLoyaltyStatusActive();

    String getDetailLoyaltyStatusTransferIn();

    String getDetailLoyaltyStatusDisabled();

    String getDetailLoyaltyStatusMissing();

    String getDetailLoyaltyStatusBurned();

    String getDetailProductCfnDivision();

    String getDetailProductCfnDepartment();

    String getDetailProductCfnGroup();

    String getDetailProductCfnFamily();

    String getDetailProductCfnSubfamily();

    String getDetailLoyaltyStatusFound();

    String getHeaderPaymentTypeWildcard();

    String getPermissionPromoBannerManagement();

    String getPermissionPromotionsManagement();

    String getPermissionLoyaltyCardManufactureOrder();

    String getPermissionLoyaltyCardInventory();

    String getPermissionEmployeeRewardsApproval();

    String getPermissionMemberPointsAdjustment();

    String getPermissionEmployeePointsAdjustment();

    String getPermissionEmployeePurchaseAdjustment();

    String getPermissionMediaMgmt();

    String getPermissionComplaintsMgmt();

    String getComplaintStatNew();

    String getComplaintStatAssigned();

    String getComplaintStatReassigned();

    String getComplaintStatInprogress();

    String getComplaintStatResolved();

    String getComplaintStatHdr();

	String getBestTimeToCall();

	String getSocialMedia();

	String getVisitedSupermarkets();

	String getInterestedActivities();

	String getVehiclesOwned();

	String getHeaderRadius();

	String getHeaderCorrespondenceAddress();

	String getHeaderInformedAbout();

	String getHeaderInterestedProds();


	String getGcDiscountType();


	public String getHeaderPosLoseStatus();
    
    public String getDetailPosLoseStatusNew();
    
    public String getDetailPosLoseStatusAccounted();
    
    public String getDetailPosLoseStatusApproved();
    
    public String getDetailPosLoseStatusRejected();

    String getBurnReasons();

    String getBurnType();

	String getHeaderGcSoPaymentMethod();

	String getDetailGcSoPaymentMethodCash();

	String getDetailGcSoPaymentMethodTransfer();

	String getDetailMoStatusBarcoding();

	String getDetailMoStatusReceiving();

	String getHeaderGcSoCategory();

	String getHeaderGcSoDepartment();

	String getHeaderBusinessUnit();

	String getHeaderBusinessRegion();

	String getHeaderBusinessTerritory();

	String getDtlGcBuC4Default();

	String getHdrExhibitionPaymentTypes();

	String getAccountingCode();

	String getReturnReasons();

	String getProductProfileFaceValue();

	String getDetailRewardTypeStamp();

    String getDetailMessageCodeNoStampsEarned();
}
