package com.transretail.crm.web.member.bean;

import org.hibernate.validator.constraints.NotEmpty;

import com.transretail.crm.core.validator.Confirm;

/**
 * @author Allan G. Ramirez (agramirez@exist.com)
 */
public class ChangePasswordDto2 extends ChangePasswordDto {
    @NotEmpty
    private String oldPassword;

    public String getOldPassword() {
        return oldPassword;
    }

    public void setOldPassword(String oldPassword) {
        this.oldPassword = oldPassword;
    }
}
