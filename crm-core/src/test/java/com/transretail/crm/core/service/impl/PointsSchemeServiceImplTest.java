package com.transretail.crm.core.service.impl;

import com.transretail.crm.core.entity.RewardSchemeModel;
import com.transretail.crm.core.entity.enums.Status;
import com.transretail.crm.core.service.PointsSchemeService;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Configurable;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Configurable
@Transactional
@ContextConfiguration(locations = "classpath*:/META-INF/spring/applicationContext*.xml")
@RunWith(SpringJUnit4ClassRunner.class)
public class PointsSchemeServiceImplTest {

	@Autowired
	PointsSchemeService voucherService;

	@Before
	public void setUp() throws Exception {

	}

	@After
	public void tearDown() throws Exception {

		voucherService.deleteAllPointsSchemes();
	}

	@Test
	public void testSaveAndUpdate() throws Exception {

		RewardSchemeModel model = new RewardSchemeModel();
		model.setStatus(Status.ACTIVE);

		RewardSchemeModel voucher = voucherService.saveAndUpdate(model);

		Assert.assertNotNull(voucher);

		voucher.setStatus(Status.INACTIVE);

		RewardSchemeModel voucher2 = voucherService.saveAndUpdate(voucher);

		Assert.assertNotNull(voucher2);

		Assert.assertTrue(voucher2.getStatus().equals(Status.INACTIVE));

	}

	@Test
	public void testFindAllVoucherSchemes() throws Exception {
		RewardSchemeModel model = new RewardSchemeModel();
		model.setStatus(Status.ACTIVE);

		voucherService.saveAndUpdate(model);

		RewardSchemeModel model2 = new RewardSchemeModel();
		model.setStatus(Status.ACTIVE);

		voucherService.saveAndUpdate(model2);

		List<RewardSchemeModel> list = voucherService.findAllPointsSchemes();

		Assert.assertNotNull(list);

		Assert.assertTrue(list.size() > 1);

	}

	@Test
	public void testFindAllActiveVouchers() throws Exception {

		RewardSchemeModel model = new RewardSchemeModel();
		model.setStatus(Status.ACTIVE);

		voucherService.saveAndUpdate(model);

		RewardSchemeModel model2 = new RewardSchemeModel();
		model.setStatus(Status.ACTIVE);

		voucherService.saveAndUpdate(model2);

		RewardSchemeModel model3 = new RewardSchemeModel();
		model.setStatus(Status.INACTIVE);

		voucherService.saveAndUpdate(model3);

		List<RewardSchemeModel> list = voucherService
				.findAllActivePointsSchemes();

		Assert.assertNotNull(list);

		Assert.assertTrue(list.size() == 2);

	}

	@Test
	public void testHasOverlappingRange() {
		RewardSchemeModel model1 = new RewardSchemeModel();
		model1.setAmount(1.0);
		model1.setStatus(Status.ACTIVE);
		model1.setValidPeriod(1);
		model1.setValueFrom(10.0);
		model1.setValueTo(100.0);
		
		RewardSchemeModel model2 = new RewardSchemeModel();
		model2.setAmount(1.0);
		model2.setStatus(Status.INACTIVE);
		model2.setValidPeriod(1);
		model2.setValueFrom(200.0);
		model2.setValueTo(210.0);
		
		model1 = voucherService.saveAndUpdate(model1);
		model2 = voucherService.saveAndUpdate(model2);
		
		// saved value is 10 to 100

		Assert.assertTrue(voucherService.hasOverlappingRange(0L, 10.0, 100.0));
		Assert.assertTrue(voucherService.hasOverlappingRange(0L, 9.0, 101.0));
		Assert.assertFalse(voucherService.hasOverlappingRange(0L, 1.0, 9.0));
		Assert.assertFalse(voucherService.hasOverlappingRange(0L, 101.0, 102.0));
		Assert.assertFalse(voucherService.hasOverlappingRange(model1.getId(), 10.0, 100.0));
		Assert.assertFalse(voucherService.hasOverlappingRange(0L, 200.0, 210.0));
		Assert.assertFalse(voucherService.hasOverlappingRange(0L, 199.0, 211.0));
		
		
		voucherService.deletePointsScheme(model1.getId());
		voucherService.deletePointsScheme(model2.getId());
	}
	
	@Test
	public void testHasOverlappingValue() {
		RewardSchemeModel model1 = new RewardSchemeModel();
		model1.setAmount(1.0);
		model1.setStatus(Status.ACTIVE);
		model1.setValidPeriod(1);
		model1.setValueFrom(10.0);
		model1.setValueTo(100.0);
		
		RewardSchemeModel model2 = new RewardSchemeModel();
		model2.setAmount(1.0);
		model2.setStatus(Status.INACTIVE);
		model2.setValidPeriod(1);
		model2.setValueFrom(200.0);
		model2.setValueTo(210.0);
		
		model1 = voucherService.saveAndUpdate(model1);
		model2 = voucherService.saveAndUpdate(model2);
		
		// saved value is 10 to 100

		Assert.assertTrue(voucherService.hasOverlappingValue(0L, 100.0));
		Assert.assertTrue(voucherService.hasOverlappingValue(0L, 99.0));
		Assert.assertTrue(voucherService.hasOverlappingValue(0L, 10.0));
		Assert.assertTrue(voucherService.hasOverlappingValue(0L, 11.0));
		Assert.assertFalse(voucherService.hasOverlappingValue(0L, 9.0));
		Assert.assertFalse(voucherService.hasOverlappingValue(0L, 101.0));
		Assert.assertFalse(voucherService.hasOverlappingValue(model1.getId(), 100.0));
		Assert.assertFalse(voucherService.hasOverlappingValue(model1.getId(), 10.0));
		Assert.assertFalse(voucherService.hasOverlappingValue(0L, 201.0));
		Assert.assertFalse(voucherService.hasOverlappingValue(0L, 200.0));
		
		voucherService.deletePointsScheme(model1.getId());
		voucherService.deletePointsScheme(model2.getId());
	}
}
