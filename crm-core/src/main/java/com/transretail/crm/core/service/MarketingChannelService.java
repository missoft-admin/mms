package com.transretail.crm.core.service;


import java.util.Date;
import java.util.List;

import org.springframework.web.multipart.MultipartFile;

import com.transretail.crm.common.service.dto.response.ResultList;
import com.transretail.crm.core.dto.ApprovalRemarkDto;
import com.transretail.crm.core.dto.MarketingChannelDto;
import com.transretail.crm.core.dto.MarketingChannelSearchDto;
import com.transretail.crm.core.entity.CrmFile;
import com.transretail.crm.core.entity.MarketingChannel;
import com.transretail.crm.core.entity.MemberModel;


public interface MarketingChannelService {

	MarketingChannelDto getDto(Long id);

	MarketingChannel save(MarketingChannel model);

	MarketingChannelDto saveDto(MarketingChannelDto dto, boolean isNew);

	MarketingChannelDto saveFileAndDto(MarketingChannelDto dto, boolean isNew);

	MarketingChannelDto updateDto(MarketingChannelDto dto);

	MarketingChannelDto updateFileAndDto(MarketingChannelDto dto);

	CrmFile saveFile(MultipartFile file);

	ResultList<MarketingChannelDto> search(MarketingChannelSearchDto dto);

	List<String> getCreateUsers();

	MarketingChannelDto saveRemarksDto(ApprovalRemarkDto dto);

	List<MemberModel> getTargetRecipients(String commaDelimMemberGrpIds, String commaDelimPromoIds);

	String getAppConfig(String configKey);

	List<MarketingChannel> getScheduled(Date jobDate);

	Long countTargetMembers(String commaDelimMemberGrpIds,
			String commaDelimPromoIds);

}
