package com.transretail.crm.core.security.service.impl;

import com.transretail.crm.core.entity.InvalidTerminalModel;
import com.transretail.crm.core.repo.InvalidTerminalRepo;
import com.transretail.crm.core.service.InvalidTerminalService;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.*;

/**
 *
 * @author Monte Cillo Co (mco@exist.com)
 */
@Transactional
@ContextConfiguration(locations = "classpath*:/META-INF/spring/applicationContext*.xml")
@RunWith(SpringJUnit4ClassRunner.class)
public class InvalidTerminalServiceImplTest {

    @Autowired
    private InvalidTerminalService service;
    @Autowired
    private InvalidTerminalRepo repo;

    @Before
    public void setup() {
	InvalidTerminalModel invalidTerminalModel = new InvalidTerminalModel();
	invalidTerminalModel.setStoreCode("store_code");
	invalidTerminalModel.setTerminalId("890-123-678990");
	repo.save(invalidTerminalModel);
    }

    @Test
    public void terminalIdExists() {
	assertThat(service.inList("store_code", "890-123-678990"), is(true));
    }

    @After
    public void cleanup() {
	repo.deleteAll();
    }
}
