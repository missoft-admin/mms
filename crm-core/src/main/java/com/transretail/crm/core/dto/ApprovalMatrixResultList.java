package com.transretail.crm.core.dto;


import java.util.Collection;

import org.springframework.data.domain.Page;

import com.transretail.crm.common.service.dto.response.AbstractResultListDTO;
import com.transretail.crm.core.entity.ApprovalMatrixModel;


public class ApprovalMatrixResultList extends AbstractResultListDTO<ApprovalMatrixDto> {
    public ApprovalMatrixResultList(Collection<ApprovalMatrixDto> results, long totalElements, boolean hasPreviousPage, boolean hasNextPage) {
        super(results, totalElements, hasPreviousPage, hasNextPage);
    }
    
    public ApprovalMatrixResultList( Page<ApprovalMatrixDto> page ) {
    	super(page);
    }
}
