package com.transretail.crm.sql;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.util.List;
import java.util.Scanner;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;

import com.google.common.collect.Lists;

/**
 * Generates sql file given exported timesten dump using the command ttBulkCp -o.
 * <p>
 * Sample output of ttBulkCp -o DSN=POSSBACK TRANSRETAIL.CRM_EMPLOYEE_PURCHASE_TXN CRM_EMPLOYEE_PURCHASE_TXN.dmp
 * <pre>
 * ##ttBulkCp
 * #
 * # TRANSRETAIL.CRM_EMPLOYEE_PURCHASE_TXN, 16 columns, dumped Fri Apr  4 09:09:04 2014
 * # columns:
 * #      1. ID           VARCHAR2(255 BYTE)
 * #      2. CREATED_BY   VARCHAR2(255 BYTE)
 * #      3. CREATED_DATETIME TIMESTAMP(6)
 * #      4. LAST_UPDATED_BY VARCHAR2(255 BYTE)
 * #      5. LAST_UPDATED_DATETIME TIMESTAMP(6)
 * #      6. APPROVAL_DATE TIMESTAMP(6)
 * #      7. APPROVED_BY  VARCHAR2(255 BYTE)
 * #      8. REASON       VARCHAR2(10000 BYTE)
 * #      9. REMARKS      VARCHAR2(10000 BYTE)
 * #     10. STATUS       VARCHAR2(20 BYTE)
 * #     11. TXN_AMOUNT   BINARY_DOUBLE
 * #     12. TXN_DATE     TIMESTAMP(6)
 * #     13. TXN_NO       VARCHAR2(255 BYTE)
 * #     14. TXN_TYPE     VARCHAR2(255 BYTE)
 * #     15. ACCOUNT_ID   TT_BIGINT
 * #     16. STORE_CODE   TT_INTEGER
 * # end
 * #
 * "10036_1392465104413","SYSTEM",2014-02-15 18:51:44.413000,NULL,NULL,NULL,NULL,NULL,NULL,"ACTIVE",11000,2014-02-15 11:51:43.000000,"100360540000831",NULL,79001000,309
 * "10036_1393078548006","SYSTEM",2014-02-22 21:15:48.006000,NULL,NULL,NULL,NULL,NULL,NULL,"ACTIVE",13600,2014-02-22 21:14:24.000000,"100360470001483",NULL,78438000,309
 * </pre>
 * </p>
 * Improve this class as well as the comments if output of ttBulkCp has changed.
 *
 * @author Allan G. Ramirez (agramirez@exist.com)
 */
public class TtBulkCpDumpToSql {
    private static final String SPLIT_REGEX = ",(?=([^\"]*\"[^\"]*\")*[^\"]*$)";

    public static void main(String[] args) throws Exception {
        Scanner in = new Scanner(System.in);
        System.out.print("Schema name    : ");
        String schemaName = in.nextLine();
        System.out.print("Dump directory : ");
        String dumpDir = in.nextLine();
        TtBulkCpDumpToSql helper = new TtBulkCpDumpToSql();
        File baseDir = new File(dumpDir);
        toSql(helper, baseDir, schemaName);
    }

    static void toSql(final TtBulkCpDumpToSql helper, final File baseDir, final String schemaName) throws Exception {
        for (File file : baseDir.listFiles()) {
            if (file.isDirectory()) {
                toSql(helper, file, schemaName);
            }

            String filePath = file.getAbsolutePath();
            if (!filePath.endsWith(".dump") && !filePath.endsWith(".dmp")) {
                continue;
            }
            String outputFileName = filePath.substring(0, filePath.lastIndexOf('.'));
            final FileWriter writer = new FileWriter(outputFileName + ".sql");
            try {
                helper.toSql(file, new LineWriterDelegate() {
                    @Override
                    public void write(String line) throws Exception {
                        writer.write(line + "\r\n");
                    }
                }, schemaName);
            } finally {
                IOUtils.closeQuietly(writer);
            }
        }
    }

    public synchronized void toSql(File file, LineWriterDelegate delegate, String schema) throws Exception {
        FileInputStream is = null;
        try {
            is = new FileInputStream(file);
            Scanner scanner = new Scanner(is);
            parse(scanner, delegate, schema);
        } finally {
            if (is != null) {
                is.close();
            }
        }
    }

    private void parse(Scanner scanner, LineWriterDelegate delegate, String schema) throws Exception {
        // Parse Headers
        int nextColumnNumber = 1;
        String tablename = "";
        List<ColumnMetadata> metadatas = Lists.newArrayList();
        String line = "#";
        while (scanner.hasNext() && line.startsWith("#")) {
            line = scanner.nextLine();
            // Refer to # TRANSRETAIL.CRM_EMPLOYEE_PURCHASE_TXN, 16 columns, dumped Fri Apr  4 09:09:04 2014 on sample output above
            if (line.contains("dumped")) {
                String[] arr = line.substring(1).trim().split(",");
                if (StringUtils.isNotBlank(schema)) {
                    tablename = arr[0].replace(schema + ".", "");
                } else {
                    tablename = arr[0];
                }
            } else if (line.contains(nextColumnNumber + ". ")) {
                String substr = line.substring(1).trim();
                String[] arr = substr.split("\\s+");
                metadatas.add(new ColumnMetadata(arr[1].toUpperCase(), arr[2].toUpperCase()));
                nextColumnNumber++;
            }
        }
        StringBuilder initialStatement = new StringBuilder("INSERT INTO ");
        initialStatement.append(tablename);
        initialStatement.append(" (");
        initialStatement.append(metadatas.get(0).columnName);
        for (int i = 1; i < metadatas.size(); i++) {
            initialStatement.append(",");
            initialStatement.append(metadatas.get(i).columnName);
        }
        initialStatement.append(") VALUES (");
        while (scanner.hasNext()) {
            line = scanner.nextLine();
            if (StringUtils.isBlank(line) || line.startsWith("#")) {
                continue;
            }
            String[] values = line.split(SPLIT_REGEX);
            if (values.length != metadatas.size()) {
                throw new RuntimeException(
                    "Dirty values found.\r\n" + line + "\r\n. Total values does not matched with total columns");
            }
            StringBuilder insertStatement = new StringBuilder(initialStatement.toString());
            insertStatement.append(toSqlValue(values[0], metadatas.get(0)));
            for (int i = 1; i < values.length; i++) {
                insertStatement.append(",");
                insertStatement.append(toSqlValue(values[i], metadatas.get(i)));
            }
            insertStatement.append(");");
            delegate.write(insertStatement.toString());
        }
    }

    private String toSqlValue(final String rawValue, final ColumnMetadata metadata) {
        String result = null;
        if (!rawValue.toUpperCase().equals("NULL")) {
            if (metadata.dataType.contains("TIMESTAMP")) {
                result = "TO_TIMESTAMP('" + rawValue + "', 'YYYY-MM-DD HH24:MI:SS.FF')";
            } else if (metadata.dataType.contains("DATE")) {
                String[] dateString = rawValue.split(" ");
                if (dateString.length > 0) {
                    result = "TO_DATE('" + dateString[0] + "', 'YYYY-MM-DD')";
                } else {
                    result = "TO_DATE('" + rawValue + "', 'YYYY-MM-DD')";
                }
            } else if (metadata.dataType.contains("CHAR")) {
                String string = rawValue.substring(1, rawValue.length() - 1);
                result = "'" + string + "'";
            } else {
                result = rawValue;
            }
        } else {
            result = "NULL";
        }
        return result;
    }

    private class ColumnMetadata {
        String columnName;
        String dataType;

        private ColumnMetadata(String columnName, String dataType) {
            this.columnName = columnName;
            this.dataType = dataType;
        }
    }
}
