$(document).ready(function() {
	var CONTAINER_ERROR = "#contentError > div";
	var CONTENT_ERROR = "#contentError";

	$manufactureOrderModal = $("#manufactureOrderModal");
	$list = $("#list_mo");

	$manufactureOrderModal.modal({
		show: false,
		keyboard:	false, 
		backdrop: "static"
	}).on('hidden.bs.modal', function() {
		$(this).empty();
	})
	.css({
		"width": '60%',
		"margin-left": "-375px"
	});

	initDataTable();

	$("#orderDateSearch").datepicker({
		format		:	'dd-mm-yyyy',
		endDate		:	'-0d',
 		autoclose	:	true
	});

	$("#clear").click(function() {
		$(".searchField").val("");
	});

	$("#search").click(function() {
		$("#criteriaValue").attr("name", $("#criteria").val());
		var formDataObj = $("#searchDto").toObject( { mode : 'first', skipEmpty : true } );
		console.log(formDataObj);
		$list.ajaxDataTable('search', formDataObj);
	});

	function initDataTable() {

		var isApprover = $list.data("isApprover");

	  var headers = {
	    moNo: $list.data("hdrMoNo"),
	    moDate: $list.data("hdrMoDate"),
	    poNo: $list.data("hdrPoNo"),
	    supplier: $list.data("hdrSupplier"),
	    orderDate: $list.data("hdrOrderDate"),
	    status: $list.data("hdrStatus")
	  };

	  var status = {
		draft: $list.data("statusDraft"),
		approved: $list.data("statusApproved"),
		forApproval: $list.data("statusForApproval"),
		barcoding: $list.data("statusBarcoding"),
		receiving: $list.data("statusReceiving")
	  }

	  $list.ajaxDataTable({
	    'autoload'  : true,
	    'ajaxSource' : $list.data("url"),
	    'aaSorting'     : [[ 0, 'desc' ]],
	    'columnHeaders' : [
	           { className : 'hide', text : '' },
	           headers['moNo'],
	           headers['moDate'],
	           headers['poNo'],
	           headers['supplier'],
	           headers['orderDate'],
	           headers['status'],
	           ""
	    ],
	    'modelFields' : [
	     {name : 'lastUpdated', sortable : true, aaClassname : 'hide', dataType: "DATE" },
	     {name : 'moNo', sortable : true},
	     {name : 'moDate', sortable : false},
	     {name : 'poNo', sortable : false},
	     {name : 'supplierName', sortable : false},
	     {name : 'orderDate', sortable : false},
	     {name : 'status.description', sortable : false},
	     {customCell : function ( innerData, sSpecific, json ) {
	        if ( sSpecific == 'display' ) {
	        	var statusCode = json.status.code;
	        	var html = "";
	        	if((isApprover && statusCode == status["draft"]) || (!isApprover && statusCode != status["approved"]))
	        		html += $("#editBtn").html();
	        	if(statusCode == status["approved"] && json.showReceive)
	        		html += $("#receiveBtn").html();
	        	if(statusCode == status["approved"] || statusCode == status["barcoding"])
	        		html += $("#printMoBtn").html();
	        	if(statusCode == status["approved"])
	        		html += $("#otherBtn").html();
	        	if(isApprover && statusCode == status["forApproval"])
	        		html += $("#viewBtn").html();
	        	if(statusCode != status["approved"] && statusCode != status["barcoding"] && statusCode != status["receiving"])
	        		html += $("#deleteBtn").html();

	        	var rg = new RegExp("%ITEMID%", 'g');
	        	html = html.replace(rg, json.id);
      		    rg = new RegExp("%MONO%", 'g');
      		    html = html.replace(rg, json.moNo);

	        	return html;
	          } else {
	            return '';
	          }
	          }
	     }
	     ]
	  })
	  .on("click", ".modalBtn", function(e) {
		  e.preventDefault();
		  var modalBtn = $(this);
			$.get($(this).data("url"), function(data) {
				if ((modalBtn).hasClass('icn-view')) $manufactureOrderModal.addClass('view-mo');
				$manufactureOrderModal.html(data).modal("show");
				initDialog();
			}, "html");
		})
		.on("click", ".printBtn", function(e) {
			e.preventDefault();
			window.location.href = $(this).data("url");
		})
		.on("click", ".printMoBtn", function(e) {
			e.preventDefault();
			window.open($(this).data("url"), '_blank', 'toolbar=0,location=0,menubar=0');
		});;
	}

	$(".modalBtn").click(function(e) {
		e.preventDefault();
		$.get($(this).data("url"), function(data) {
			$manufactureOrderModal.html(data).modal("show");
			initDialog();
		}, "html");
	});

	var quantityClassPrefix = "company_cardtype-";

	function initDialog() {
		$("input.moDate").datepicker({
			format: 'dd-mm-yyyy',
			autoclose: true
		}).datepicker( "setDate", new Date() );

		$("input[name='moDate']")
			.datepicker("setDate", new Date())
			.datepicker("setStartDate", new Date());

		$("input[name='orderDate']")
			.datepicker("setEndDate", new Date());

		$("input[name='receiveDate']")
			.datepicker("setEndDate", new Date())
			.datepicker("setStartDate", new Date($("input[name='receiveDate']").data("moDate")));

        var $companyCardTypeStartSeriesContainer = $('#companyCardTypeStartSeriesContainer');
        var $productTable = $('#productTable');
        var bindProductRowEvents = function ( tr ) {
            function ReOrderSeries( _quantityClass ) {
                this.quantityClass = _quantityClass;
                var $quantityInputs = $( '.' + this.quantityClass, $productTable );
                if ( $quantityInputs.length > 0 ) {
                    var arr = this.quantityClass.split( '-' );
                    var companyLast2Digits = arr[1].substr( arr[1].length - 2 );
                    var cardTypeLast2Digits = arr[2].substr( arr[2].length - 2 );
                    var $startSeries = $( 'input#' + companyLast2Digits + cardTypeLast2Digits, $companyCardTypeStartSeriesContainer );

                    var createSeries = function ( strSeriesNo ) {
                        var padZero = 8 - strSeriesNo.length;
                        if ( padZero > 0 ) {
                            for ( var i = 0; i < padZero; i++ )
                                strSeriesNo = "0" + strSeriesNo;
                        }
                        return companyLast2Digits + cardTypeLast2Digits + strSeriesNo;
                    }

                    var updateMatchingInputs = function(lastSeriesNo) {
                        $quantityInputs.each( function ( index, Element ) {
                            var $tr = $( Element ).parents( 'tr' );
                            var $startingSeriesInput = $( '.startingSeries', $tr );
                            var $endingSeriesInput = $( '.endingSeries', $tr );

                            var quantity = parseInt( Element.value );
                            if ( quantity && quantity > 0 ) {
                                $startingSeriesInput.val( createSeries( lastSeriesNo.toString() ) );
                                lastSeriesNo += quantity;
                                $endingSeriesInput.val( createSeries( (lastSeriesNo - 1).toString() ) );
                            } else {
                                $startingSeriesInput.val( '' );
                                $endingSeriesInput.val( '' );
                            }
                        } );
                    }

                    if($startSeries.length > 0) {
                        updateMatchingInputs(parseInt($startSeries.val()));
                    } else {
                        var ajaxQueue = jQuery.ajaxQueue( {
                            url : ctx + "/manufactureorder/lastseriesno/" + companyLast2Digits + "/" + cardTypeLast2Digits,
                            dataType : "json"
                        },function () {
                            $( '#moOrderCtrlBtns button' ).prop( 'disabled', true );
                        } ).done( function ( lastSeriesNo ) {
                            // Should never conflict because we queue ajax calls
                            $companyCardTypeStartSeriesContainer.append('<input id="' + companyLast2Digits + cardTypeLast2Digits + '" type="hidden" value="' + (lastSeriesNo + 1) + '"/>');
                            updateMatchingInputs(lastSeriesNo + 1);
                            if ( !ajaxQueue.hasQueue() ) {
                                $( '#moOrderCtrlBtns button' ).prop( 'disabled', true );
                            }
                        } );
                    }
                }
            }

            $( '.quantity', tr )
                    .numberInput( "type", "int" )
                    .each( function ( index, Element ) {
                        var $quantity = $( Element );
                        var $tr = $quantity.parents( 'tr' );
                        var $company = $( '.companyCode', $tr );
                        var $cardType = $( '.cardType', $tr );
                        $quantity.addClass( quantityClassPrefix + $company.val() + '-' + $cardType.val() );
                    } )
                    .bindWithDelay( "keyup", function(e) {
                        var classesArr = e.target.className.split( ' ' );
                        var comCardTypeClassName = '';
                        for ( var i = 0; i < classesArr.length; i++ ) {
                            if ( classesArr[i].indexOf( quantityClassPrefix ) != -1 ) {
                                comCardTypeClassName += classesArr[i];
                                break;
                            }
                        }
                        new ReOrderSeries(comCardTypeClassName);
                    }, 200 );

            var updateQuantityClassName = function ( companyCode, cardTypeCode, $quantityEl ) {
                var quantityEl = $quantityEl.get( 0 );
                var classesArr = quantityEl.className.split( ' ' );
                var newComCardTypeClassName = quantityClassPrefix + companyCode + "-" + cardTypeCode;
                var oldComCardTypeClassName = '';
                var newClassName = '';
                for ( var i = 0; i < classesArr.length; i++ ) {
                    if ( classesArr[i].indexOf( quantityClassPrefix ) == -1 ) {
                        newClassName += classesArr[i] + ' ';
                    } else {
                        oldComCardTypeClassName = classesArr[i];
                    }
                }

                newClassName += newComCardTypeClassName;
                quantityEl.className = newClassName;

                new ReOrderSeries(oldComCardTypeClassName);
                new ReOrderSeries(newComCardTypeClassName);
            };

            $( '.companyCode', tr ).change( function () {
                var $company = $( this );
                var $tr = $company.parents( 'tr' );
                var $cardType = $( '.cardType', $tr );
                var $quantity = $( '.quantity', $tr );
                updateQuantityClassName( $company.val(), $cardType.val(), $quantity );
            } );
            $( '.cardType', tr ).change( function () {
                var $cardType = $( this );
                var $tr = $cardType.parents( 'tr' );
                var $company = $( '.companyCode', $tr );
                var $quantity = $( '.quantity', $tr );
                updateQuantityClassName( $company.val(), $cardType.val(), $quantity );
            } );
        }

        $('tbody tr', $productTable ).each(function(index, Element) {
            bindProductRowEvents(Element);
        });

        var  $productTbody = $("tbody", $productTable);
        var $manufactureOrder = $('#manufactureOrder');
        $('#removeProductBtn', $manufactureOrder ).click(function() {
            $("input.productRemoveFlag:checked").each(function() {
                var $tr = $(this).closest("tr");
                var length = $productTbody.find("tr").length;
                if($tr.is(":first-child") && length == 1) {
                    $tr.find("input.productRemoveFlag").prop('checked', false);
                    $tr.find("input").removeAttr('value').prop('disabled', true);
                    $tr.find("select").removeAttr('value').prop('disabled', true);
                    $tr.invisible();
                } else
                    $tr.remove();
            });
        });

        $('#addProductBtn', $manufactureOrder ).click(function() {
            var $tr = $productTbody.find("tr:first-child");
            if($tr.css('visibility') == 'hidden' || $tr.css('display') == 'none') {
                $tr.find("input").removeAttr('value').prop('disabled', false);
                $tr.find("select").removeAttr('value').prop('disabled', false);
                $tr.visible();
            } else {
                var $trLast = $productTbody.find("tr:last-child");
                var  $row = $tr.clone();
                $row.find("input").removeAttr('value').prop('disabled', false);
                $row.find("select").removeAttr('value').prop('disabled', false);
                var index = +$trLast.data("index") + 1;
                var lastIndex = $tr.data("index");
                $row.data("index", index);
                var rg = new RegExp("\\["+lastIndex+"\\]", 'g');
                var html = $row.html();
                html = html.replace(rg, '['+index+']');
                $row.html(html);

                $productTbody.append($row);
                bindProductRowEvents($row.get(0));
            }
        });
	}

	$manufactureOrderModal.on("keyup", ".modalQuantity, .modalStartingSeries", populateEndingSeries);
	$manufactureOrderModal.on("click", 'button[data-loading-text]', function () {
	    $(this).button("loading");
	    $(this).siblings().attr("disabled", "disabled").addClass('btn-disabled');
	});

	$manufactureOrderModal.on("click", ".receiveOrderSave", function(e) {
		e.preventDefault();
		$form = $("form[name='receiveOrderDto']");
		var moId = $(this).data("moId");
		$.ajax({
			type 	: "POST",
			url 	: $form.attr("action") + moId,
			dataType: 'json',
			data 	: $form.serialize(),

			success : function(inResponse) {
				if (inResponse.success) {
					var url = $form.data("printUrl") + moId + "?" + $form.serialize();
					window.open(url, '_blank', 'toolbar=0,location=0,menubar=0');
					location.reload();
					$manufactureOrderModal.modal( "hide" );
					$list.ajaxDataTable( "search" );
				}
				else {
					errorInfo = "";
					for (i = 0; i < inResponse.result.length; i++) {
						errorInfo += "<br>" + (i + 1) + ". "
								+ ( inResponse.result[i].code != undefined ?
										inResponse.result[i].code : inResponse.result[i]);
					}
					$manufactureOrderModal.find( CONTAINER_ERROR ).html("Please correct following errors: " + errorInfo);
					$manufactureOrderModal.find( CONTENT_ERROR ).show('slow');
					resetButtons();
				}
			},
			error : function(jqXHR, textStatus, errorThrown) {
				$( CONTENT_ERROR ).html("Error: " + errorThrown);
			}
		});
	});

	$manufactureOrderModal.on("click", ".manufactureOrderSave", function(e) {
		e.preventDefault();
		var submitButton = $( this );
		$( submitButton ).prop( "disabled", true );
		$form = $("form[name='manufactureOrder']");
		$.ajax({
			type 	: "POST",
			url 	: $form.attr("action") + $(this).data("status") + "/" + $(this).data("moId"),
			dataType: 'json',
			data 	: $form.serialize(),

			success : function(inResponse) {
				if (inResponse.success) {
					location.reload();
				}
				else {
					errorInfo = "";
					for (i = 0; i < inResponse.result.length; i++) {
						errorInfo += "<br>" + (i + 1) + ". "
								+ ( inResponse.result[i].code != undefined ?
										inResponse.result[i].code : inResponse.result[i]);
					}
					$manufactureOrderModal.find( CONTAINER_ERROR ).html("Please correct following errors: " + errorInfo);
					$manufactureOrderModal.find( CONTENT_ERROR ).show('slow');
					resetButtons();
				}
				$( submitButton ).prop( "disabled", false );
			},
			error : function(jqXHR, textStatus, errorThrown) {
				$( CONTENT_ERROR ).html("Error: " + errorThrown);
				$( submitButton ).prop( "disabled", false );
			}
		});
	});

	function populateEndingSeries() {
		var startSeries = $(this).closest("tr").find(".modalStartingSeries").val();
		var quantity = $(this).closest("tr").find(".modalQuantity").val();
		if(startSeries != null && quantity != null && startSeries != "" && quantity != "") {
			var temp = +startSeries + +quantity - 1;
			var endingSeries = temp.toString();
			var padZero = 12 - endingSeries.length;
			if(padZero > 0) {
				for(var i = 0; i < padZero; i++)
					endingSeries = "0" + endingSeries;
			}
			$(this).closest("tr").find(".modalEndingSeries").val(endingSeries);
		}
	}

	function populateQuantity() {
		var startSeries = $(this).closest("tr").find(".modalStartingSeries").val();
		var endingSeries = $(this).closest("tr").find(".modalEndingSeries").val();
		if(startSeries != null && endingSeries != null && startSeries != "" && endingSeries != "") {
			var quantity = +endingSeries - +startSeries + 1;
			$(this).closest("tr").find(".modalQuantity").val(quantity);
		}

	}

	function resetButtons() {
		$('button').button("reset").removeAttr("disabled").removeClass('btn-disabled');
	}

	jQuery.fn.visible = function() {
	    return this.css('visibility', 'visible');
	};

	jQuery.fn.invisible = function() {
	    return this.css('visibility', 'hidden');
	};

});