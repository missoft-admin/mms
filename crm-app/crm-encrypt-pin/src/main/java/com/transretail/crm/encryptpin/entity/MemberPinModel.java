package com.transretail.crm.encryptpin.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import org.hibernate.annotations.Parameter;
import org.hibernate.annotations.Type;
import org.joda.time.DateTime;
import org.springframework.data.annotation.LastModifiedBy;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.AbstractPersistable;


/**
 * @author Allan G. Ramirez (agramirez@exist.com)
 */
@Entity
@Table(name = "CRM_MEMBER")
public class MemberPinModel extends AbstractPersistable<Long> {
    private static final long serialVersionUID = -1715557827644404413L;

    @Column(name = "PIN", nullable = false)
    private String pin;

    @Column(name = "PIN_ENCRYPTED", length = 1)
    @Type(type = "yes_no")
    private Boolean pinEncryped;

    @Column(name = "LAST_UPDATED_DATETIME")
    @LastModifiedDate
    @Type(type = "org.jadira.usertype.dateandtime.joda.PersistentDateTime",
        parameters = {@Parameter(name = "databaseZone", value = "jvm")})
    private DateTime lastUpdated;

    @Column(name = "LAST_UPDATED_BY")
    @LastModifiedBy
    private String lastUpdateUser;

    public String getPin() {
        return pin;
    }

    public void setPin(String pin) {
        this.pin = pin;
    }

    public Boolean getPinEncryped() {
        return pinEncryped;
    }

    public void setPinEncryped(Boolean pinEncryped) {
        this.pinEncryped = pinEncryped;
    }

    public DateTime getLastUpdated() {
        return lastUpdated;
    }

    public void setLastUpdated(DateTime lastUpdated) {
        this.lastUpdated = lastUpdated;
    }

    public String getLastUpdateUser() {
        return lastUpdateUser;
    }

    public void setLastUpdateUser(String lastUpdateUser) {
        this.lastUpdateUser = lastUpdateUser;
    }

}
