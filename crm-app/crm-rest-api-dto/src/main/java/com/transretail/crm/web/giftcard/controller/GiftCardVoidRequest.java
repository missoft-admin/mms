package com.transretail.crm.web.giftcard.controller;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.transretail.crm.common.web.converter.ISODateTimeDeserializer;
import com.transretail.crm.common.web.converter.ISODateTimeSerializer;
import com.transretail.crm.giftcard.dto.GiftCardTransactionInfo;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import org.hibernate.validator.constraints.NotEmpty;
import org.joda.time.LocalDateTime;

/**
 * @author Monte Cillo Co (mco@exist.com)
 */
public class GiftCardVoidRequest extends AbstractGiftCardRequest {

    @NotEmpty
    @Size( max = 20)
    private String transactionNo;
    @NotNull
    @JsonSerialize(using = ISODateTimeSerializer.class)
    @JsonDeserialize(using = ISODateTimeDeserializer.class)
    private LocalDateTime transactionDate;
    @NotEmpty
    @Size( max = 15)
    private String refTransactionNo;

    public GiftCardVoidRequest withTransactionNo(String transactionNo) {
        this.transactionNo = transactionNo;
        return this;
    }

    public GiftCardVoidRequest withTransactionDate(LocalDateTime transactionDate) {
        this.transactionDate = transactionDate;
        return this;
    }

    public GiftCardVoidRequest withRefTransactionNo(String refTransactionNo) {
        this.refTransactionNo = refTransactionNo;
        return this;
    }

    public String getTransactionNo() {
        return transactionNo;
    }

    public String getRefTransactionNo() {
        return refTransactionNo;
    }

    public void setRefTransactionNo(String refTransactionNo) {
        this.refTransactionNo = refTransactionNo;
    }

    public LocalDateTime getTransactionDate() {
        return transactionDate;
    }

    @Override
    public String getSignature() {
        return String.format("%s%s%s%s%s", getMerchantId(), getTerminalId(), getCashierId(), getTransactionNo(), refTransactionNo);
    }

    @Override
    public GiftCardTransactionInfo toGiftCardTransactionInfo() {
        return super.templateBaseTransactionInfo()
                .transactionNo(transactionNo)
                .transactionTime(transactionDate)
                .refTransactionNo(refTransactionNo);
    }
}
