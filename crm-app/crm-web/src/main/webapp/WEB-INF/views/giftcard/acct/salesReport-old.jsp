<%@include file="/WEB-INF/views/common/taglibs.jsp" %>
<div id="sales-report" class="form-horizontal">
  <div class="page-header page-header2">
    <h1><spring:message code="gc.acct.sales.report.page.title" /></h1>
  </div>
  <div id="contentError" class="hide alert alert-error">
    <button type="button" class="close" onclick="$(this).parent().hide();">&times;</button>
    <div><form:errors path="*"/></div>
  </div>
  <spring:url value="/giftcard/acct/sales/report/export" var="form_url" />
  <form:form name="reportFiltersForm" commandName="reportFiltersForm" action="${form_url}" method="GET" modelAttribute="reportFiltersForm" >
    <div class="control-group">
      <label class="control-label"></label>
      <div class="controls">
	<select name="reportType" id="reportType">
	  <c:forEach items="${reportTypes}" var="reportType">
	    <option value="${reportType}"><spring:message code="report_type_${reportType.code}" /></option>
	  </c:forEach>
	</select>
      </div>
    </div>
    <fieldset>
      <legend><spring:message code="report.filters.label"/></legend>
      <div class="control-group row-fluid">
	<label class="control-label"><spring:message code="gc.acct.sales.filter.report.type" /></label>
	<div class="controls-row">
	  <c:forEach items="${salesReportCategory}" var="type">
	    <form:radiobutton cssClass="sales-report-type" id="sales-report-type" path="salesReportType" value="${type}"/><spring:message code="sales.report.category.${type}" />
	  </c:forEach>
	</div>
      </div>
      <div id="filters" class="">
	<div class="input-daterange" id="datepicker">
	  <div class="control-group">
	    <label class="control-label"><spring:message code="gc.acct.sales.filter.book.date.range.from"/></label>
	    <div class="controls"><form:input path="bookedDateFrom" cssClass="input-medium"/></div>
	  </div>
	  <div class="control-group">
	    <label class="control-label"><spring:message code="gc.acct.sales.filter.book.date.range.to"/></label>
	    <div class="controls"><form:input path="bookedDateTo" cssClass="input-medium"/></div>
	  </div>
	</div>
      </div>
    </fieldset>
    <div class="control-group form-actions">
      <button id="view" type="button" class="btn btn-primary"><spring:message code="view"/></button>
    </div>
  </form:form>
</div>
<link href="<c:url value="/css/bootstrap/datepicker.css" />" rel="stylesheet" />
<script src='<c:url value="/js/bootstrap/bootstrap-datepicker.js"/>' type='text/javascript'>< ![CDATA[ & nbsp; ]] ></script>
<script src="<spring:url value="/js/jquery/jquery.toObject.js" />" ></script>
<script type="text/javascript">
      $(document).ready(function () {
	var CONTAINER_ERROR = "#contentError > div";
	var CONTENT_ERROR = "#contentError";
	var $filters = $('#filters');

	$('#sales-report .input-daterange').datepicker({
	  format: DEFAULT_DATE_FORMAT,
	  startDate: '-3m',
	  endDate: new Date(),
	  todayBtn: "linked",
	  autoclose: true
	});

//$("input:radio[name=sales-report-type]").click(function() {
//    var value = $(this).val();
//    alert("value"  + value);
//});

	$("input[name=salesReportType]:radio").change(function () {
	  var value = $("input[name=salesReportType]:checked").val();
//	  alert("value: " + value);
	  if (value === 'SUMMARY') {

	  } else if (value === 'REDEEM') {

	  } else if (value === 'OUTSTANDING_BALANCE') {
	    $filters.append( "<h1>Header</h1>");
//	    $filters.html("<h1>Header</h1>");
	    $.get(ctx + "/giftcard/acct/sales/report/filters/OUTSTANDING_BALANCE", function(data, status){
	      alert(data);
	      var idx;
	      for( idx in data ){
		alert(data[idx].label);
	      }
	      for (var i = 0, max = 10; i < max; i++) {
    
}
	              $filters.append( "<h1>Header</h1>");
	    });
	  }
	});
	$('#view').click(function (e) {
	  e.preventDefault();
	  $.get($("#reportFiltersForm").attr("action") + "/validate", $("#reportFiltersForm").serialize(), function (data) {
	    if (data.success) {
	      $(CONTENT_ERROR).hide();
	      $(CONTAINER_ERROR).empty();
	      var url = $("#reportFiltersForm").attr("action") + "?" + $("#reportFiltersForm").serialize();
	      var selectedReportType = $("select[name='reportType']").val();
	      if (selectedReportType === "PDF") {
		window.open(url, '_blank', 'toolbar=0,location=0,menubar=0');
	      }
	      else if (selectedReportType === "EXCEL") {
		window.location.href = url;
	      }
	    }
	    else {
	      renderErrors(data);
	    }
	  });
	});

	function renderErrors(data) {
	  var errorInfo = "";
	  for (i = 0; i < data.result.length; i++) {
	    errorInfo += "<br>" + (i + 1) + ". " + (data.result[i]);
	  }

	  $(CONTAINER_ERROR).html("Please correct following errors: " + errorInfo);
	  $(CONTENT_ERROR).show('slow');
	}
      });
</script>