package com.transretail.crm.giftcard.service;

import com.transretail.crm.core.dto.MemberDto;
import com.transretail.crm.core.entity.MemberModel;
import com.transretail.crm.giftcard.dto.*;

import org.springframework.validation.Errors;

/**
 * description here.
 *
 * @author Vincent Gerard Tan
 */
public interface MemberPrepaidService {

    MemberPrepaidDto getMemberPrepaidDetails();

    MemberDto getMemberDetails();

    void registerCardToMember(MemberGiftCardDto giftCardDto, Errors errors);

    PrepaidTransactionResultList retrieveTransactionHistory(PrepaidTransactionSearchDto searchDto);

    PrepaidCardResultList retrieveCards(PrepaidCardSearchDto prepaidCardSearchDto);

    PrepaidCardResultList retrieveCardsByMember(MemberModel member);
}
