package com.transretail.crm.core.repo.custom;


import java.util.Date;
import java.util.List;

import com.transretail.crm.core.dto.CampaignSearchDto;
import com.transretail.crm.core.dto.PromotionSearchDto;
import com.transretail.crm.core.entity.Program;


public interface ProgramRepoCustom {

	List<Program> find( Date inDate, String inStatus );

	List<Program> find( CampaignSearchDto theDto );

	List<Program> find( PromotionSearchDto theDto );

}
