package com.transretail.crm.common.web.handler;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

import java.nio.charset.Charset;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.BlockJUnit4ClassRunner;
import org.springframework.http.HttpStatus;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.HttpStatusCodeException;
import org.springframework.web.servlet.ModelAndView;

/**
 *
 */
@RunWith(BlockJUnit4ClassRunner.class)
public class AjaxHandlerExceptionResolverTest {
    @Test
    public void doResolveExceptionTest() throws Exception {
        MockHttpServletRequest request = new MockHttpServletRequest();

        AjaxHandlerExceptionResolver resolver = new AjaxHandlerExceptionResolver();
        assertNull(resolver.resolveException(request, null, null, null));


        request.addHeader("X-Requested-With", "XMLHttpRequest");
        MockHttpServletResponse response = new MockHttpServletResponse();
        HttpStatusCodeException rce =
            new HttpClientErrorException(HttpStatus.FORBIDDEN, "FORBIDDEN", "rest exception".getBytes(), Charset.defaultCharset());
        ModelAndView mav = resolver.resolveException(request, response, null, rce);
        assertNotNull(mav);
        assertNull(mav.getViewName());
        assertEquals("rest exception", response.getErrorMessage());

        response = new MockHttpServletResponse();
        NullPointerException npe =
            new NullPointerException("npe");
        mav = resolver.resolveException(request, response, null, npe);
        assertNotNull(mav);
        assertNull(mav.getViewName());
        assertEquals("npe", response.getErrorMessage());
    }
}
