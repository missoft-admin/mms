package com.transretail.crm.web.security;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.util.Date;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.ImportResource;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.AnnotationConfigContextLoader;

import com.transretail.crm.core.entity.MemberModel;
import com.transretail.crm.core.entity.embeddable.CustomerProfile;
import com.transretail.crm.core.entity.enums.Status;
import com.transretail.crm.core.entity.lookup.LookupDetail;
import com.transretail.crm.core.entity.lookup.LookupHeader;
import com.transretail.crm.core.entity.lookup.MemberType;
import com.transretail.crm.core.repo.LookupDetailRepo;
import com.transretail.crm.core.repo.LookupHeaderRepo;
import com.transretail.crm.core.repo.MemberRepo;
import com.transretail.crm.core.security.model.impl.CustomSecurityUserDetailsImpl;

/**
 * @author Allan G. Ramirez (agramirez@exist.com)
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(loader = AnnotationConfigContextLoader.class)
public class MemberSecurityServiceTest {
    @Configuration
    @ImportResource({"classpath:META-INF/spring/applicationContext-test.xml",
            "classpath:META-INF/spring/applicationContext-core.xml"})
    static class ContextConfiguration {
        @Bean
        public MemberSecurityService memberSecurityService() {
            return new MemberSecurityService();
        }
    }

    private static final String USERNAME = "ramirezag";

    @Autowired
    private MemberRepo memberRepo;
    @Autowired
    private MemberSecurityService service;
    private Long savedMemberId;
    
    @Autowired
    LookupHeaderRepo lookupHeaderRepo;
    
    @Autowired
    LookupDetailRepo lookupDetailRepo;

    @Before
    public void onSetup() {
    	LookupHeader header = new LookupHeader();
    	header.setCode("gender");
    	header.setDescription("gender");
    	header = lookupHeaderRepo.saveAndFlush(header);
    	
    	LookupDetail detail = new LookupDetail();
    	detail.setHeader(header);
    	detail.setCode("m");
    	detail.setDescription("m");
    	detail.setStatus(Status.ACTIVE);
    	detail = lookupDetailRepo.saveAndFlush(detail);
    	
    	MemberType memberType = new MemberType("INDIVIDUAL");
    	
        MemberModel model = new MemberModel();
        model.setPin("pinpinpin");
        model.setUsername(USERNAME);
        model.setLastName("ramirez");
        model.setFirstName("cloudx");
        model.setEmail("agramirez@exist.com");
        model.setPassword("Y3h3Y!");
        CustomerProfile profile = new CustomerProfile();
        profile.setGender(new LookupDetail("m"));
        profile.setBirthdate(new Date());
        model.setCustomerProfile(profile);
        model.setEnabled(true);
        model.setContact("ramirezag");
        model.setAccountId("accountId");
//        model.setMemberType(memberType);
        savedMemberId = memberRepo.saveAndFlush(model).getId();
    }

    @After
    public void tearDown() {
        memberRepo.delete(savedMemberId);
    }

    @Test
    public void loadUserByUsernameTest() {
        assertNull(service.loadUserByUsername("notexisting"));

        UserDetails result = service.loadUserByUsername(USERNAME);
        assertTrue(result.getClass() + " is not an instance of CustomSecurityUserDetailsImpl",
            result instanceof CustomSecurityUserDetailsImpl);
        CustomSecurityUserDetailsImpl user = (CustomSecurityUserDetailsImpl) result;
        assertEquals(USERNAME, user.getUsername());
        assertEquals("Y3h3Y!", user.getPassword());
        assertEquals(true, user.isEnabled());
//        assertEquals("INDIVIDUAL", user.getRoles().iterator().next().getCode());
//        assertEquals("INDIVIDUAL", user.getAuthorities().iterator().next().getAuthority());
    }
}
