package com.transretail.crm.giftcard.dto;

/**
 * description here.
 *
 * @author Vincent Gerard Tan
 */
public class MemberGiftCardDto {
    private String cardNo;

    public String getCardNo() {
        return cardNo;
    }

    public void setCardNo(String cardNo) {
        this.cardNo = cardNo;
    }
}
