package com.transretail.crm.common.web.controller.support;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;
import org.springframework.web.servlet.FlashMap;
import org.springframework.web.servlet.support.RequestContextUtils;

/**
 * @author aco
 */
public final class MavBuilderUtils {

    private MavBuilderUtils() {
    }

    public static HttpServletRequest getCurrentServletRequest() {
        return ((ServletRequestAttributes) RequestContextHolder.currentRequestAttributes()).getRequest();
    }

    public static Map<String, ?> getCurrentInputFlashMap() {
        return RequestContextUtils.getInputFlashMap(getCurrentServletRequest());
    }

    public static FlashMap getCurrentOutputFlashMap() {
        return RequestContextUtils.getOutputFlashMap(getCurrentServletRequest());
    }

}
