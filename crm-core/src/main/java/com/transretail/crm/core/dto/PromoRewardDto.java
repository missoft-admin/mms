package com.transretail.crm.core.dto;


import java.util.List;

public class PromoRewardDto {

    private Double points = 0D;

    private Double discount = 0.0;

    private List<String> items;

    private String messageCode;

    private String messageDesc;

    private Double stamps = 0D;
    
    public Double getPoints() {
        return points;
    }

    public void setPoints(Double points) {
        this.points = points;
    }

    public Double getDiscount() {
        return discount;
    }

    public void setDiscount(Double discount) {
        this.discount = discount;
    }

    public List<String> getItems() {
        return items;
    }

    public void setItems(List<String> items) {
        this.items = items;
    }

    public String getMessageCode() {
        return messageCode;
    }

    public void setMessageCode(String messageCode) {
        this.messageCode = messageCode;
    }

    public String getMessageDesc() {
        return messageDesc;
    }

    public void setMessageDesc(String messageDesc) {
        this.messageDesc = messageDesc;
    }

    public Double getStamps() {
        return stamps;
    }

    public void setStamps(Double stamps) {
        this.stamps = stamps;
    }
}
