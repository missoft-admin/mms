package com.transretail.crm.core.entity.enums;

public enum MarketingCorrespondenceAddress {

	BUSINESS_ADD("businessAddress"),
	HOME_ADD("homeAddress");

    private MarketingCorrespondenceAddress(String code){
    	this.code = code;
    }

    private final String code;

	public String getCode() {
		return code;
	}
    
    
}
