$(document).ready(function() {
	var CONTAINER_ERROR = "#contentError > div";
	var CONTENT_ERROR = "#contentError";
	$countDialog = $("#physicalCountDialog");
	$countDialogByBarcode = $("#physicalCountDialogByBarcode");
	$countForm = $("#physicalCountForm");
	$countFormByBarcode = $("#physicalCountFormByBarcode");
	$summaryDialog = $("#physicalCountSummaryDialog");
	
	$countDialog.modal({
		show: false,
		keyboard: false, 
		backdrop: "static" 
	}).on('hidden.bs.modal', function() {
		$countForm.trigger("reset");
		$countDialog.find(CONTENT_ERROR).hide();
		$countDialog.find(CONTAINER_ERROR).empty();
	}).css({
		"width": "500px"
	});
	
	$countDialogByBarcode.modal({
		show: false,
		keyboard: false, 
		backdrop: "static" 
	}).on('hidden.bs.modal', function() {
		$countForm.trigger("reset");
		$countDialogByBarcode.find(CONTENT_ERROR).hide();
		$countDialogByBarcode.find(CONTAINER_ERROR).empty();
	}).css({
		"width": "500px"
	});
	
	$summaryDialog.modal({
		show: false,
		keyboard: false, 
		backdrop: "static" 
	}).on('hidden.bs.modal', function() {
//		$countForm.trigger("reset");
	});
	
	$("input[name='startingSeries'], input[name='endingSeries'], input[name='barcode']").numberInput("type", "int");
	
	$("#countStart").click(function(e) {
		e.preventDefault();
		$countDialog.modal("show");
	});
	
	$("#bySeries").click(function(e){
	  e.preventDefault();
	  $countDialog.modal("show");	  
	});
	
	$('#byBarcode').click(function(e){
	  e.preventDefault();
	  $countDialogByBarcode.modal("show");	 
	});
	
	$('#barcode-interface').keypress(function(event) {
	  var keycode = (event.keyCode ? event.keyCode : event.which);
	  if (keycode == '13') {
	    getCardTypeBySeriesOnly();
	  }
	  event.stopPropagation();
	});
	
	$("#countCancel").click(hideProcessBtn);
	$("#countCancel2").click(hideProcessBtn);
	
	$("#countSubmit").click(function(e) {
		e.preventDefault();
		$.post($countForm.attr("action"), $countForm.serialize())
		.done(function(data) {
			if (data.success) {
				reloadTable();
				$countForm.trigger("reset");
				$countDialog.find(CONTENT_ERROR).hide();
				$countDialog.find(CONTAINER_ERROR).empty();
			} 
			else {
			  renderErrors($countDialog, data);
			}
		})
		.fail(function(data) {
			$(CONTENT_ERROR).html("Error: " + data);
		});
	});
	
	$("#countSubmit2").click(function(e) {
	  e.preventDefault();
	  $('#barcodes_select option').prop('selected', true);
	  $.post($countFormByBarcode.attr("action"), $countFormByBarcode.serialize())
		  .done(function(data) {
		    if (data.success) {
		      reloadTable();
		      $countFormByBarcode.trigger("reset");
		      //manual reseting select box(actually remove all options)
		      $('#barcodes_select')[0].options.length = 0;
		      $countDialogByBarcode.find(CONTENT_ERROR).hide();
		      $countDialogByBarcode.find(CONTAINER_ERROR).empty();
		    } else {
		      renderErrors($countDialogByBarcode, data);
		    }
		  })
		  .fail(function(data) {
		    $(CONTENT_ERROR).html("Error: " + data);
		  });
	});

	$("#countEnd").click(function(e) {
		e.preventDefault();
		$summaryDialog.modal("show");
		reloadSummaryTable();
	});
	
	$("#summaryProcess").click(function(e) {
		$.get($(this).data("url"), function(data) {
			if(data.success) {
				$summaryDialog.modal("hide");
				location.reload();
			} else {
				alert("An error occurred on the server. Please try again.");
			}
		});	
	});
	
	$("#summaryPrint").click(function(e) {
		e.preventDefault();
		var url = $("#reportType option:selected").data("url");
		var selectedReportType = $("select[name='reportType']").val();
		if(selectedReportType == "pdf")
			window.open(url, '_blank', 'toolbar=0,location=0,menubar=0' );
		else if(selectedReportType == "excel")
			window.location.href = url;
	});
	
	$("#summaryHistoryPrint").click(function(e) {
		e.preventDefault();
		var url = $("#historyReportType option:selected").data("url") + $("select[name='summaryDate']").val();
		var selectedReportType = $("select[name='historyReportType']").val();
		if(selectedReportType == "pdf")
			window.open(url, '_blank', 'toolbar=0,location=0,menubar=0' );
		else if(selectedReportType == "excel")
			window.location.href = url;
	});
	
	$("input[name='startingSeries'], input[name='endingSeries']").change(computeQuantity).change(getCardType);
	
	function computeQuantity() {
		var starting = $("input[name='startingSeries']").val();
		var ending = $("input[name='endingSeries']").val();
		var quantity = "";
		if(starting.length == 12 && ending.length == 12)
			quantity = +ending - +starting + 1;
		$("input[name='quantity']").val(quantity);
	}
	
	function getCardType() {
		var starting = $("input[name='startingSeries']").val();
		var ending = $("input[name='endingSeries']").val();
		
		$cardTypeCode = $("input[name='cardType']");
		$cardTypeDesc = $("input[name='cardTypeDesc']");
		
		if(starting.length == 12 && ending.length == 12) {
			$.get($($cardTypeCode).data("url") + "/" + starting + "/" + ending, function(data) {
				if(data.success) {
					$cardTypeCode.val(data.result.code);
					$cardTypeDesc.val(data.result.description);
				} else {
					$cardTypeCode.val("");
					$cardTypeDesc.val("");
				}
			});	
		} else {
			$cardTypeCode.val("");
			$cardTypeDesc.val("");
		}
	}
	
	function getCardTypeBySeriesOnly() {
	  $barcodeDom = $("input[name='barcode']");
	  var barcode = $barcodeDom.val();
	  $.get(ctx + "/loyaltycardinventory/validate/" + barcode, function(data) {
	    if (data.success) {
	      $('#barcodes_select').append($('<option>', {
		text: barcode
	      }));
	      $('#barcode-interface').val("");
	      $countDialogByBarcode.find(CONTENT_ERROR).hide();
	      $countDialogByBarcode.find(CONTAINER_ERROR).empty();
	    } else {
	      renderErrors($countDialogByBarcode, data);
	    }
	  });
	}
	
	function renderErrors(targetDialog, data) {
	  errorInfo = "";
	  for (i = 0; i < data.result.length; i++) {
	    errorInfo += "<br>" + (i + 1) + ". "
		    + (data.result[i].code !== undefined ? data.result[i].code : data.result[i]);
	  }
	  targetDialog.find(CONTAINER_ERROR).html("Please correct following errors: " + errorInfo);
	  targetDialog.find(CONTENT_ERROR).show('slow');
	}
});

