<%@ include file="../../common/taglibs.jsp" %>

<style type="text/css">
<!--

  #productTable tr td:last-child {
    border-left: 0px;
  }

  #addProductBtn.btn-small {
    margin-bottom: 5px;
  }
  table select, textarea, table input[type="text"], input[type="password"], input[type="datetime"], input[type="datetime-local"], input[type="date"], input[type="month"], input[type="time"], input[type="week"], input[type="number"], input[type="email"], input[type="url"], input[type="search"], input[type="tel"], input[type="color"], .uneditable-input {
      margin-bottom: 0px;
      margin-top: 0px;
  }
  
-->
</style>



<c:set var="ENCTYPE" value="application/x-www-form-urlencoded"/>

<div class="modal-dialog">
  <div class="modal-content">
    <spring:message code="label_manufacture_order" var="typeName" />
    
    <div class="modal-header">
      <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
      <h4><spring:message code="gc_replacement"/></h4>
    </div>
 
    <div class="modal-body">
    
      <div id="contentError" class="hide alert alert-error">
        <button type="button" class="close" onclick="$(this).parent().hide();">&times;</button>
        <div><form:errors path="*"/></div>
      </div>
    
      
      <form:form id="orderForm" name="orderForm" modelAttribute="orderForm" action="${pageContext.request.contextPath}/giftcard/salesorder/approve/" method="POST" enctype="${ENCTYPE}" cssClass="form-reset form-horizontal" >
      <form:hidden path="id"/>
      
      <div class="contentMain">
      
      <div class="row-fluid">
      
        
      
        <div class="span6">
        <div class="control-group">
          <spring:message code="sales_order_no" var="orderNum" />
          <label for="orderNo" class="control-label">${orderNum}</label> 
          <div class="controls">
            <form:input path="orderNo" class="form-control" placeholder="${orderNum}" disabled="true"/>
          </div>
        </div>
        
        <div class="control-group">
          <spring:message code="sales_order_type" var="orderTyp" />
          <label for="orderNo" class="control-label">${orderTyp}</label> 
          <div class="controls">
            <input type="text" value="${orderForm.orderType}" class="form-control" disabled="disabled" />
          </div>
        </div>
        
        <div class="control-group">
          <spring:message code="sales_order_date" var="orderDt" />
          <label for="orderDate" class="control-label">${orderDt}</label> 
          <div class="controls">
            <form:input path="orderDate" class="form-control orderDate" placeholder="${orderDt}" disabled="true" />
          </div>
        </div>
        
        <div class="control-group">
          <spring:message code="sales_order_notes" var="notes" />
          <label for="customerId" class="control-label">${notes}</label> 
          <div class="controls">
            <form:textarea path="notes" class="form-control"  disabled="true" />
          </div>
        </div>
        
        <div class="control-group">
          <spring:message code="order_total_face_amount" var="totalFaceAmount" />
          <label for="totalFaceAmount" class="control-label">${totalFaceAmount}</label> 
          <div class="controls">
            <%-- <form:input path="totalFaceAmount" id="totalFaceAmount" class="form-control" placeholder="${totalFaceAmount}" readonly="true" /> --%>
            <form:input path="totalFaceAmountFmt" id="totalFaceAmountFmt" class="form-control" placeholder="${totalFaceAmount}" disabled="true" />
            <form:hidden path="totalFaceAmount" id="totalFaceAmount" />
          </div>
        </div>
        
        <div class="control-group">
          <spring:message code="sales_order_replacementfee" var="replacementFee" />
          <label for="totalFaceAmount" class="control-label">${replacementFee}</label> 
          <div class="controls">
            <form:input path="handlingFee" class="form-control floatInput" placeholder="${replacementFee}" disabled="true" />
          </div>
        </div>
        
        </div>
        
        <div class="span6">
        
        <div class="control-group">
          <spring:message code="gc_return_no" var="returnNo" />
          <label for="returnNo" class="control-label">${returnNo}</label> 
          <div class="controls">
            <form:input path="returnNo" class="form-control" disabled="true" placeholder="${returnNo}" />
          </div>
        </div>
        
        <div class="control-group">
          <spring:message code="sales_original_order_no" var="origOrderNo" />
          <label class="control-label">${origOrderNo}</label> 
          <div class="controls">
            <form:input path="origOrderNo" id="soOrderNo" class="form-control" placeholder="${origOrderNo}" readonly="true" />
          </div>
        </div>
        
        <div class="control-group">
          <spring:message code="sales_original_order_date" var="origOrderDate" />
          <label class="control-label">${origOrderDate}</label> 
          <div class="controls">
            <form:input path="origOrderDate" id="soOrderDate" class="form-control" placeholder="${origOrderDate}" readonly="true" />
          </div>
        </div>
        
        <div class="control-group">
          <spring:message code="gc_return_amount" var="returnAmount" />
          <label class="control-label">${returnAmount}</label> 
          <div class="controls">
            <input type="text" value="<fmt:formatNumber pattern="###,###" value="${orderForm.returnAmount}" />" disabled="disabled" />
          </div>
        </div>
        
        <div class="control-group">
          <spring:message code="gc_return_customer_name" var="customerName" />
          <label class="control-label">${customerName}</label> 
          <div class="controls">
            <form:input path="customerDesc" id="customerDesc" class="form-control" placeholder="${customerName}" disabled="true" />
          </div>
        </div>
        
        
        <div class="control-group">
          <div class="controls checkbox">
            <form:checkbox path="paidReplacement" class="form-control" disabled="true" /><label><spring:message code="order_paid_replacement" /></label>
          </div>
        </div>
        
        </div>
        
        <div class="clearfix"></div>
        
        <div class="span12">
        
        <div>
          <table id="productTable" class="table table-condensed table-compressed table-striped">
            <thead>
              <tr>
                <th><spring:message code="sales_order_item_product_name" /></th>
                <th><spring:message code="sales_order_item_face_value" /></th>
                <th><spring:message code="sales_order_item_quantity" /></th>
                <th><spring:message code="sales_order_item_face_amount" /></th>
                <th><spring:message code="sales_order_item_print_fee" /></th>
                <th></th>
              </tr>
            </thead>
            <tbody>
                <c:forEach var="item" items="${orderForm.items}" varStatus="status" >
                  <tr data-index="${status.index}">
                    <td>
                    <input type="text" class="input-medium" value="${item.productDesc}" disabled="disabled" />
                    </td>
                    <td><input type="text" class="input-mini faceValue" value="${item.faceValue}" disabled="disabled" name="items[${status.index}].faceValue" /></td>
                    <td><input type="text" class="input-mini quantity" value="${item.quantity}" disabled="disabled" name="items[${status.index}].quantity" /></td>
                    <td><input type="text" class="input-medium faceAmount" value="${item.faceAmount}" disabled="disabled" name="items[${status.index}].faceAmount" /></td>
                    <td><input type="text" class="input-mini printFee" value="${item.printFee}" disabled="disabled" name="items[${status.index}].printFee" /></td>
                    <td>
                    <div class="checkbox">
                      <input type="checkbox" ${item.freePrintFee == true ? "checked" : ""} disabled="disabled"  name="items[${status.index}].freePrintFee" />
                      <label class="" style="text-align:left"><spring:message code="order_item_free_print_fee" /></label>
                    </div>  
                    </td>
                  </tr>
                </c:forEach>
            </tbody>
          </table>
        </div>
        
        </div>
        
      </div>
          
      </div>
      
      </form:form>
    
    </div>
    <div class="modal-footer">
      <button type="button" data-status="APPROVED" class="orderSubmit btn btn-primary"><spring:message code="label_approve" /></button>
      <button type="button" data-status="DRAFT" class="orderSubmit btn btn-primary"><spring:message code="label_reject" /></button>
      <button type="button" id="orderCancel" class="btn btn-default" data-dismiss="modal"><spring:message code="label_cancel"/></button>
    </div>
  </div>
</div>

<script>
$(document).ready(function() {
  
  var CONTAINER_ERROR = "#contentError > div";
  var CONTENT_ERROR = "#contentError";
  $form = $("#orderForm");
  $orderDialog = $("#orderDialog").css({"width": "900px"});
  
  $(".orderSubmit").click(submitForm);
  
  function submitForm() {
    $(".orderSubmit").prop( "disabled", true );
    var status = $(this).data("status"); 
    if(status == "VALIDATED") {
    	getConfirm('<spring:message code="sales_verified_payment_confirm" />', function(result) { 
			if(result) {
				ajaxSubmit(status);
			} else {
				$(".orderSubmit").prop( "disabled", false );
			}
		});
    } else {
    	ajaxSubmit(status);
    }
  }
  
  function ajaxSubmit(status) {
	  $.post($form.attr("action") + status, $form.serialize(), function(data) {
	      $(".orderSubmit").prop( "disabled", false );
	      if (data.success) {
	        reloadOrderTable();
	      } 
	      else {
	        errorInfo = "";
	        for (i = 0; i < data.result.length; i++) {
	          errorInfo += "<br>" + (i + 1) + ". "
	              + ( data.result[i].code != undefined ? 
	                  data.result[i].code : data.result[i]);
	        }
	        $orderDialog.find(CONTAINER_ERROR).html("Please correct following errors: " + errorInfo);
	        $orderDialog.find(CONTENT_ERROR).show('slow');
	      }
	    });  
  }
  
    
});
</script>