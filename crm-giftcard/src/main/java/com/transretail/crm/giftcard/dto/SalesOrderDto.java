package com.transretail.crm.giftcard.dto;

import java.math.BigDecimal;
import java.util.List;

import com.google.common.collect.Sets;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.joda.time.DateTime;
import org.joda.time.LocalDate;
import org.joda.time.Months;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.format.annotation.NumberFormat;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.google.common.collect.Lists;
import com.transretail.crm.common.web.converter.FormattedIntegerNumberSerializer;
import com.transretail.crm.common.web.converter.LocalDateSerializer;
import com.transretail.crm.core.dto.ApprovalRemarkDto;
import com.transretail.crm.core.entity.enums.ModelType;
import com.transretail.crm.core.entity.lookup.LookupDetail;
import com.transretail.crm.giftcard.entity.GiftCardCustomerProfile;
import com.transretail.crm.giftcard.entity.GiftCardDiscountScheme;
import com.transretail.crm.giftcard.entity.ReturnRecord;
import com.transretail.crm.giftcard.entity.SalesOrder;
import com.transretail.crm.giftcard.entity.SalesOrder.DeliveryType;
import com.transretail.crm.giftcard.entity.SalesOrderHistory;
import com.transretail.crm.giftcard.entity.SalesOrderItem;
import com.transretail.crm.giftcard.entity.SalesOrderPaymentInfo;
import com.transretail.crm.giftcard.entity.support.SalesOrderDiscountType;
import com.transretail.crm.giftcard.entity.support.SalesOrderStatus;
import com.transretail.crm.giftcard.entity.support.SalesOrderType;

/**
 *
 */
public class SalesOrderDto {

	public static final String DEPT_PREFIX = "GCD";

	private String orderNo;
	private Long id;
	@DateTimeFormat(pattern = "dd-MM-yyyy")
	@JsonSerialize(using = LocalDateSerializer.class)
	private LocalDate orderDate;
	private DateTime created;
	private DateTime lastUpdated;
	private Long customerId;
	private String customerDesc;
	private String invoiceTitle;
	private Long discountId;
	private String discountDesc;
	private String notes;
	private List<SalesOrderItemDto> items = Lists.newArrayList(new SalesOrderItemDto());
	private List<SalesOrderHistoryDto> statusHistories = Lists.newArrayList(new SalesOrderHistoryDto());
	private SalesOrderStatus status;
	private SalesOrderType orderType;

	private String contactPerson;
	private String contactNumber;
	private String contactEmail;
	private BigDecimal discountVal;
	private BigDecimal voucherVal;
	private String receiptNo;
	private String companyPhoneNo;
	private String mailAddress;
	private String faxNo;

	private String returnNo;
	private Long returnId;
	private LocalDate origOrderDate;
	private String origOrderNo;
	private BigDecimal returnAmount;

	private Boolean paidHandlingFee;

	private ApprovalRemarkDto remarks;

	private SalesOrderDiscountType discType;

	private String paymentStore;
	private String paymentStoreName;
	private BigDecimal totalPayment = BigDecimal.ZERO;
	private BigDecimal totalFaceAmount = BigDecimal.ZERO;
	private BigDecimal totalCardFee = BigDecimal.ZERO;
	private boolean readOnly;

	private String createdBy;

	private LookupDetail category;

	private LocalDate earliestPaymentDate = new LocalDate();

	private String deliveryType;
	private String shippingInfo;
	private BigDecimal shippingFee = BigDecimal.ZERO;

	private Boolean paidReplacement;
	private BigDecimal handlingFee;

	//parent
	private Long parentId;
	private BigDecimal parentFaceAmt;
	private BigDecimal parentSoAmt;
	private BigDecimal parentVoucherVal;

	public SalesOrderDto() {
	}

	public SalesOrderDto(SalesOrder order, boolean all) {
		this.orderNo = order.getOrderNo();
		this.id = order.getId();
		this.orderDate = order.getOrderDate();
		this.created = order.getCreated();
		this.lastUpdated = order.getLastUpdated();
		this.status = order.getStatus();
		this.orderType = order.getOrderType();
		this.createdBy = order.getCreateUser();
		this.voucherVal = order.getVoucherVal();
		for(SalesOrderHistory history:order.getStatusHistory()){
			this.statusHistories.add(new SalesOrderHistoryDto(history));
		}

		if (order.getCustomer() != null) {
			GiftCardCustomerProfile profile = order.getCustomer();
			this.customerId = profile.getId();
			this.customerDesc = profile.getName();
			this.contactPerson = order.getContactPerson();
			this.contactNumber = order.getContactNumber();
			this.contactEmail = order.getContactEmail();
			this.invoiceTitle = profile.getInvoiceTitle();
			this.companyPhoneNo = profile.getCompanyPhoneNo();
			this.mailAddress = profile.getMailAddress();
			this.faxNo = profile.getFaxNo();
			this.shippingInfo = profile.getShippingAddress();
		}
		if (StringUtils.isNotBlank(order.getShippingInfo())) {
			this.shippingInfo = order.getShippingInfo();
		}
		this.deliveryType = order.getDeliveryType();
		this.shippingFee = order.getShippingFee();

		if (CollectionUtils.isNotEmpty(order.getPayments())) {
			this.totalPayment = new BigDecimal(0);
			for (SalesOrderPaymentInfo payment : order.getPayments()) {
				if (null != payment && null != payment.getPaymentAmount()) {
					this.totalPayment = this.totalPayment.add(payment.getPaymentAmount());
					if (payment.getPaymentDate().isBefore(this.earliestPaymentDate)) {
						this.earliestPaymentDate = payment.getPaymentDate();
					}
				}
			}
		}

		if (CollectionUtils.isNotEmpty(this.items)) {
			BigDecimal totalFaceAmt = BigDecimal.ZERO;
			BigDecimal totalCrdFee = BigDecimal.ZERO;
			System.out.println(order.getOrderNo()+":"+order.getItems().size());
			for (SalesOrderItem itemDto : order.getItems()) {
				if (itemDto != null && itemDto.getProduct() != null) {
					if (itemDto.getFaceAmount() != null)
						totalFaceAmt = totalFaceAmt.add(itemDto.getFaceAmount());
					if (itemDto.getPrintFee() != null)
						totalCrdFee = totalCrdFee.add(itemDto.getPrintFee());
				}

			}
			this.totalFaceAmount = totalFaceAmt;
			this.totalCardFee = totalCrdFee;
		}

		if (all) {

			if (order.getDiscount() != null) {
				this.discountId = order.getDiscount().getId();
				this.discountDesc = order.getDiscount().getSchemeDesc();
			}
			this.notes = order.getNotes();
			if (CollectionUtils.isNotEmpty(order.getItems()))
				this.items = Lists.newArrayList(SalesOrderItemDto.toDto(order.getItems()));
			this.discountVal = order.getDiscountVal();
			this.receiptNo = order.getReceiptNo();
			this.returnNo = order.getReturnNo();
			this.paidHandlingFee = order.getPaidHandlingFee();
			this.discType = order.getDiscType();

			remarks = new ApprovalRemarkDto();
			remarks.setModelId(order.getId() != null ? order.getId().toString() : "");
			remarks.setModelType(ModelType.SALES_ORDER.toString());

			this.category = order.getCategory();
			this.paidReplacement = order.getPaidReplacement();
			this.handlingFee = order.getHandlingFee();

			if (order.getReturnRecord() != null) {
				ReturnRecord ret = order.getReturnRecord();
				this.origOrderDate = ret.getOrderNo().getOrderDate();
				this.origOrderNo = ret.getOrderNo().getOrderNo();
				this.returnAmount = ret.getReturnAmount();
				this.customerDesc = ret.getCustomer().getName();
			}

			if (order.getParent() != null) {
				SalesOrderDto parentOrder = new SalesOrderDto(order.getParent(), false);
				this.parentId = parentOrder.getId();
				this.parentSoAmt = parentOrder.getNetAmt();
				this.parentVoucherVal = parentOrder.getVoucherVal();
				this.parentFaceAmt = parentOrder.getTotalFaceAmount();
			}
		}
		this.paymentStore = order.getPaymentStore();

	}

	public SalesOrder toModel(SalesOrder order) {
		if (order == null)
			order = new SalesOrder();

		order.setOrderNo(this.orderNo);
		order.setOrderDate(this.orderDate);
		order.setOrderType(this.orderType);
		order.setDiscountVal(this.discountVal);
		order.setReturnNo(this.returnNo);
		if (this.returnId != null)
			order.setReturnRecord(new ReturnRecord(this.returnId));
		order.setPaidHandlingFee(this.paidHandlingFee);
		order.setCategory(this.category);

		order.setPaymentStore(this.paymentStore);

		if (order.getOrderType().compareTo(SalesOrderType.B2B_SALES) == 0 || order.getOrderType().compareTo(SalesOrderType.B2B_ADV_SALES) == 0)
			order.setDiscType(this.discType);

		if (customerId != null) {
			order.setCustomer(new GiftCardCustomerProfile(customerId));
			order.setContactPerson(contactPerson);
			order.setContactNumber(contactNumber);
			order.setContactEmail(contactEmail);
		}
		if (discountId != null)
			order.setDiscount(new GiftCardDiscountScheme(discountId));
		order.setNotes(this.notes);
		if (order.getItems() != null) {
			order.getItems().clear();
			if (CollectionUtils.isNotEmpty(this.items)) {
				for (SalesOrderItemDto dto : this.items) {
					if (dto != null) {
						SalesOrderItem item = dto.toModel(new SalesOrderItem());
						item.setOrder(order);
						order.getItems().add(item);
					}
				}
			}
		} else {
			order.setItems(SalesOrderItemDto.toModel(Sets.newHashSet(this.items), order));
		}
		order.setStatus(this.status);
		order.setDeliveryType(deliveryType);
		order.setShippingInfo(shippingInfo);
		order.setShippingFee(this.shippingFee);

		if (SalesOrderType.B2B_SALES.compareTo(this.orderType) == 0 ||
				SalesOrderType.B2B_ADV_SALES.compareTo(this.orderType) == 0 ||
				SalesOrderType.REPLACEMENT.compareTo(this.orderType) == 0) {
			BigDecimal shippingFee = this.shippingFee == null ? BigDecimal.ZERO : this.shippingFee;
			order.setShippingFee(shippingFee);
			if (BigDecimal.ZERO.compareTo(shippingFee) < 0)
				order.setDeliveryType(DeliveryType.SHIP_TO.toString());
			else
				order.setDeliveryType(DeliveryType.PICK_UP.toString());
		}

		order.setPaidReplacement(this.paidReplacement);
		order.setHandlingFee(this.handlingFee);
		return order;
	}

	public static List<SalesOrderDto> toDto(List<SalesOrder> orders) {
		return toDto(orders, false);
	}

	public static List<SalesOrderDto> toDto(List<SalesOrder> orders, boolean flag) {
		List<SalesOrderDto> orderDtos = Lists.newArrayList();
		for (SalesOrder order : orders) {
			orderDtos.add(new SalesOrderDto(order, flag));
		}
		return orderDtos;
	}

	public LocalDate getOrderDate() {
		return orderDate;
	}

	public String getOrderDateStr() {
		if (orderDate != null)
			return orderDate.toString("dd-MM-yyyy");
		return "";
	}

	public void setOrderDate(LocalDate orderDate) {
		this.orderDate = orderDate;
	}

	public DateTime getCreated() {
		return created;
	}

	public void setCreated(DateTime created) {
		this.created = created;
	}

	public DateTime getLastUpdated() {
		return lastUpdated;
	}

	public String getLastUpdatedStr() {
		if (lastUpdated != null)
			return lastUpdated.toString("dd-MM-yyyy hh:mm:ss aa");
		return "";
	}

	public void setLastUpdated(DateTime lastUpdated) {
		this.lastUpdated = lastUpdated;
	}

	public Long getCustomerId() {
		return customerId;
	}

	public void setCustomerId(Long customerId) {
		this.customerId = customerId;
	}

	public Long getDiscountId() {
		return discountId;
	}

	public void setDiscountId(Long discountId) {
		this.discountId = discountId;
	}

	public String getNotes() {
		return notes;
	}

	public void setNotes(String notes) {
		this.notes = notes;
	}

	public List<SalesOrderItemDto> getItems() {
		return items;
	}

	public void setItems(List<SalesOrderItemDto> items) {
		this.items = items;
	}

	public String getCustomerDesc() {
		return customerDesc;
	}

	public void setCustomerDesc(String customerDesc) {
		this.customerDesc = customerDesc;
	}

	public String getInvoiceTitle() {
		return invoiceTitle;
	}

	public void setInvoiceTitle(String invoiceTitle) {
		this.invoiceTitle = invoiceTitle;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public SalesOrderStatus getStatus() {
		return status;
	}

	public String getStatusStr() {
		if (status != null)
			return status.toString();
		return "";
	}

	public void setStatus(SalesOrderStatus status) {
		this.status = status;
	}

	public String getContactPerson() {
		return contactPerson;
	}

	public void setContactPerson(String contactPerson) {
		this.contactPerson = contactPerson;
	}

	public String getContactNumber() {
		return contactNumber;
	}

	public void setContactNumber(String contactNumber) {
		this.contactNumber = contactNumber;
	}

	public String getContactEmail() {
		return contactEmail;
	}

	public void setContactEmail(String email) {
		this.contactEmail = email;
	}

	public String getOrderNo() {
		return orderNo;
	}

	public void setOrderNo(String orderNo) {
		this.orderNo = orderNo;
	}

	public String getDiscountDesc() {
		return discountDesc;
	}

	public void setDiscountDesc(String discountDesc) {
		this.discountDesc = discountDesc;
	}

	public String getOrderTypeStr() {
		if (orderType != null)
			return orderType.toString();
		return "";
	}

	public SalesOrderType getOrderType() {
		return orderType;
	}

	public void setOrderType(SalesOrderType orderType) {
		this.orderType = orderType;
	}

	public BigDecimal getTotalSoAmt() {
		/*BigDecimal totalFceAmt = getTotalFaceAmount();
		BigDecimal totalCardFee = getTotalCardFee();
		return totalCardFee.add(totalFceAmt);*/
		return getTotalFaceAmount();
	}

	public BigDecimal getDiscountAmt() {
		BigDecimal discRate = getDiscountVal() != null ? getDiscountVal() : BigDecimal.ZERO;
		return getTotalSoAmt().multiply(discRate.divide(new BigDecimal(100)));
	}

	public BigDecimal getNetAmt() {
		return getTotalSoAmt().add(getTotalCardFee()).add(getShippingFee()).subtract(getDiscountAmt());
	}

	public BigDecimal getDiscountVal() {
		return discountVal;
	}

	public void setDiscountVal(BigDecimal discountVal) {
		this.discountVal = discountVal;
	}

	public String getReceiptNo() {
		return receiptNo;
	}

	public void setReceiptNo(String receiptNo) {
		this.receiptNo = receiptNo;
	}

	public String getCompanyPhoneNo() {
		return companyPhoneNo;
	}

	public void setCompanyPhoneNo(String companyPhoneNo) {
		this.companyPhoneNo = companyPhoneNo;
	}

	public String getMailAddress() {
		return mailAddress;
	}

	public void setMailAddress(String mailAddress) {
		this.mailAddress = mailAddress;
	}

	public String getFaxNo() {
		return faxNo;
	}

	public void setFaxNo(String faxNo) {
		this.faxNo = faxNo;
	}

	public ApprovalRemarkDto getRemarks() {
		return remarks;
	}

	public void setRemarks(ApprovalRemarkDto remarks) {
		this.remarks = remarks;
	}

	public String getReturnNo() {
		return returnNo;
	}

	public void setReturnNo(String returnNo) {
		this.returnNo = returnNo;
	}

	public Boolean getPaidHandlingFee() {
		return paidHandlingFee;
	}

	public void setPaidHandlingFee(Boolean paidHandlingFee) {
		this.paidHandlingFee = paidHandlingFee;
	}

	public SalesOrderDiscountType getDiscType() {
		return discType;
	}

	public void setDiscType(SalesOrderDiscountType discType) {
		this.discType = discType;
	}

	public BigDecimal getTotalFaceAmount() {
		return totalFaceAmount;
	}

	public void setTotalFaceAmount(BigDecimal totalFaceAmount) {
		this.totalFaceAmount = totalFaceAmount;
	}

	@JsonInclude(Include.ALWAYS)
	@JsonSerialize(using = FormattedIntegerNumberSerializer.class)
	@NumberFormat(pattern = "#,##0")
	public BigDecimal getTotalFaceAmountFmt() {
		return totalFaceAmount;
	}

	public String getPaymentStore() {
		return paymentStore;
	}

	public void setPaymentStore(String paymentStore) {
		this.paymentStore = paymentStore;
	}

	public String getPaymentStoreName() {
		return paymentStoreName;
	}

	public void setPaymentStoreName(String paymentStoreName) {
		this.paymentStoreName = paymentStoreName;
	}

	public BigDecimal getTotalPayment() {
		return totalPayment;
	}

	@JsonInclude(Include.ALWAYS)
	@JsonSerialize(using = FormattedIntegerNumberSerializer.class)
	@NumberFormat(pattern = "#,##0")
	public BigDecimal getTotalPaymentFmt() {
		return totalPayment;
	}

	public void setTotalPayment(BigDecimal totalPayment) {
		this.totalPayment = totalPayment;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public boolean isReadOnly() {
		return readOnly;
	}

	public void setReadOnly(boolean readOnly) {
		this.readOnly = readOnly;
	}

	public LocalDate getEarliestPaymentDate() {
		return earliestPaymentDate;
	}

	public void setEarliestPaymentDate(LocalDate earliestPaymentDate) {
		this.earliestPaymentDate = earliestPaymentDate;
	}

	public LookupDetail getCategory() {
		return category;
	}

	public void setCategory(LookupDetail category) {
		this.category = category;
	}

	public String getDeliveryType() {
		return deliveryType;
	}

	public void setDeliveryType(String deliveryType) {
		this.deliveryType = deliveryType;
	}

	public String getShippingInfo() {
		return shippingInfo;
	}

	public void setShippingInfo(String shippingInfo) {
		this.shippingInfo = shippingInfo;
	}

	public BigDecimal getShippingFee() {
		return shippingFee == null ? BigDecimal.ZERO : shippingFee;
	}

	public void setShippingFee(BigDecimal shippingFee) {
		this.shippingFee = shippingFee;
	}

	public Boolean getPaidReplacement() {
		return paidReplacement;
	}

	public void setPaidReplacement(Boolean paidReplacement) {
		this.paidReplacement = paidReplacement;
	}

	public BigDecimal getHandlingFee() {
		return handlingFee;
	}

	public void setHandlingFee(BigDecimal handlingFee) {
		this.handlingFee = handlingFee;
	}

	public BigDecimal getTotalCardFee() {
		return totalCardFee;
	}

	public void setTotalCardFee(BigDecimal totalCardFee) {
		this.totalCardFee = totalCardFee;
	}

	public Long getReturnId() {
		return returnId;
	}

	public void setReturnId(Long returnId) {
		this.returnId = returnId;
	}

	public LocalDate getOrigOrderDate() {
		return origOrderDate;
	}

	public void setOrigOrderDate(LocalDate origOrderDate) {
		this.origOrderDate = origOrderDate;
	}

	public String getOrigOrderNo() {
		return origOrderNo;
	}

	public void setOrigOrderNo(String origOrderNo) {
		this.origOrderNo = origOrderNo;
	}

	public BigDecimal getReturnAmount() {
		return returnAmount;
	}

	public void setReturnAmount(BigDecimal returnAmount) {
		this.returnAmount = returnAmount;
	}

	public boolean isAdvSo() {
		LocalDate earliestDate = getEarliestPaymentDate();
		if (earliestDate != null) {
			int diff = Months.monthsBetween(earliestDate.withDayOfMonth(1), this.orderDate.withDayOfMonth(1)).getMonths();
			if (diff > 0)
				return true;
		}
		/*		if(earliestDate != null &&
						Months.monthsBetween(this.orderDate.withDayOfMonth(1), earliestDate.withDayOfMonth(1)).getMonths() >= 1)
					return true;*/
		return false;
	}

	public Long getParentId() {
		return parentId;
	}

	public void setParentId(Long parentId) {
		this.parentId = parentId;
	}

	public BigDecimal getParentSoAmt() {
		return parentSoAmt;
	}

	public void setParentSoAmt(BigDecimal parentSoAmt) {
		this.parentSoAmt = parentSoAmt;
	}

	public BigDecimal getParentVoucherVal() {
		return parentVoucherVal;
	}

	public void setParentVoucherVal(BigDecimal parentVoucherVal) {
		this.parentVoucherVal = parentVoucherVal;
	}

	public BigDecimal getVoucherVal() {
		return voucherVal;
	}

	public void setVoucherVal(BigDecimal voucherVal) {
		this.voucherVal = voucherVal;
	}

	public BigDecimal getParentFaceAmt() {
		return parentFaceAmt;
	}

	public void setParentFaceAmt(BigDecimal parentFaceAmt) {
		this.parentFaceAmt = parentFaceAmt;
	}

	public List<SalesOrderHistoryDto> getStatusHistories() {
		return statusHistories;
	}

	public void setStatusHistories(List<SalesOrderHistoryDto> statusHistories) {
		this.statusHistories = statusHistories;
	}
}
