package com.transretail.crm.core.service;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


@Service( "redemptionPromoService" )
@Transactional
public class RedemptionPromoServiceImpl implements RedemptionPromoService {

    private static final Logger LOGGER = LoggerFactory.getLogger( RedemptionPromoServiceImpl.class );

}
