package com.transretail.crm.template.service.streaming.excel.handler.sheet;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.regex.Pattern;

import javax.annotation.Resource;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.apache.commons.lang3.StringUtils;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;

import com.google.common.collect.Lists;
import com.mysema.query.jpa.impl.JPAQuery;
import com.transretail.crm.core.entity.MemberModel;
import com.transretail.crm.core.entity.QMemberModel;
import com.transretail.crm.core.entity.lookup.LookupDetail;
import com.transretail.crm.core.service.CodePropertiesService;
import com.transretail.crm.core.service.LookupService;
import com.transretail.crm.core.util.AppConstants;
import com.transretail.crm.template.service.MemberTemplateParseException;
import com.transretail.crm.template.service.streaming.excel.handler.HandlerException;
import com.transretail.crm.template.service.streaming.excel.handler.cell.CellHandler;

/**
 * @author Allan G. Ramirez (agramirez@exist.com)
 */
public abstract class AbstractSheetHandler implements SheetHandler {
    protected static final DateFormat BIRTHDATE_PASSWORD_FORMAT = new SimpleDateFormat("ddMMyyyy");
    protected static final DateFormat BIRTHDATE_PIN_FORMAT = new SimpleDateFormat("ddMMyy");
    private static final Pattern EMAIL_PATTERN = Pattern.compile(AppConstants.EMAIL_PATTERN);

    protected int currentRowNum;
    protected String sheetName;
    @PersistenceContext
    protected EntityManager em;
    @Value("#{'${hibernate.jdbc.batch_size}'}")
    protected int batchSize;
    @Resource(name = "messageSource")
    private MessageSource messageSource;
    @Resource(name = "codePropertiesService")
    private CodePropertiesService codePropertiesService;
    @Resource(name = "lookupServiceImpl")
    private LookupService lookupService;

    private int totalRowData;
    private List<String> cellContents = Lists.newArrayList();
    private String datePattern;
    private String fallbackDatePattern;
    private DateTimeFormatter dateTimeFormatter;
    private DateTimeFormatter fallbackDateTimeFormatter;

    public MessageSource getMessageSource() {
        return messageSource;
    }

    public CodePropertiesService getCodePropertiesService() {
        return codePropertiesService;
    }

    @Value("#{'${format.date}'}")
    public void setDatePattern(String datePattern) {
        this.datePattern = datePattern;
        dateTimeFormatter = DateTimeFormat.forPattern(datePattern);
    }

    @Value("#{'${format.date.fallback}'}")
    public void setFallbackDatePattern(String fallbackDatePattern) {
        this.fallbackDatePattern = fallbackDatePattern;
        fallbackDateTimeFormatter = DateTimeFormat.forPattern(fallbackDatePattern);
    }

    public String getDatePattern() {
        return datePattern;
    }

    public String getFallbackDatePattern() {
        return fallbackDatePattern;
    }

    public DateTimeFormatter getDateTimeFormatter() {
        return dateTimeFormatter;
    }

    public DateTimeFormatter getFallbackDateTimeFormatter() {
        return fallbackDateTimeFormatter;
    }

    public void startRow(int rowNum) {
        if (_LOG.isDebugEnabled()) {
            _LOG.debug("Sheet Handler: Parsing sheet {} : row {}", sheetName, rowNum);
        }
        this.currentRowNum = rowNum;
    }

    @Override
    public void endRow() {
        if (currentRowNum < 1) { // Column headers
            for (int i = 0; i < cellContents.size() && i < getExpectedNumberOfColumns(); i++) {
                String headerName = cellContents.get(i);
                if (StringUtils.isBlank(headerName)) {
                    throw new HandlerException(messageSource.getMessage("member.template.messages.excel.header.length.notmatched",
                        new String[]{sheetName, String.valueOf(getExpectedNumberOfColumns())}, LocaleContextHolder.getLocale()));
                }
                processHeaderCell(i, headerName.toUpperCase());
            }
        } else { // Data
            // Check first of all cells are empty. Some excel editors put trailing empty rows in some sheets
            boolean hasContents = false;
            for (int i = 0; i < cellContents.size() && i < getExpectedNumberOfColumns(); i++) {
                if (StringUtils.isNotBlank(cellContents.get(i))) {
                    hasContents = true;
                    break;
                }
            }
            if (hasContents) {
                String errors = null;
                MemberModel dto = new MemberModel();
                for (int i = 0; i < cellContents.size() && i < getExpectedNumberOfColumns(); i++) {
                    CellHandler cellHandler = getCellHandler(i);
                    try {
                        cellHandler.handle(dto, cellContents.get(i));
                    } catch (MemberTemplateParseException e) {
                        if (errors == null) {
                            errors = e.getMessage();
                        } else {
                            errors += "\r\n" + e.getMessage();
                        }
                    }
                }
                if (errors != null) {
                    throw new HandlerException(errors);
                } else {
                    saveOrUpdateDto(dto);
                    totalRowData++;
                }
            }
        }
        cellContents.clear();
    }

    @Override
    public void cell(int index, String cellReference, String formattedValue, short formatIndex) {
        cellContents.add(formattedValue);
    }

    @Override
    public int getTotalRowData() {
        return totalRowData;
    }

    @Override
    public int getTotalRows() {
        return currentRowNum;
    }

    @Override
    public String setSheetName(String sheetName) {
        return this.sheetName = sheetName;
    }

    public LookupDetail getLoookupDetail(String headerCode, String description) throws MemberTemplateParseException {
        LookupDetail lookupDetail = lookupService.getLookupDetailByHdrCodeAndDesc(headerCode, description);
        if (lookupDetail == null) {
            throw new MemberTemplateParseException(
                messageSource.getMessage("member.template.messages.excel.code.notfound", new String[]{description, headerCode},
                    LocaleContextHolder.getLocale())
            );
        }
        return lookupDetail;
    }

    public boolean accountIdExists(String accountId) {
        QMemberModel qMemberModel = QMemberModel.memberModel;
        return new JPAQuery(em).from(qMemberModel).where(qMemberModel.accountId.eq(accountId)).exists();
    }

    public boolean contactExists(String contact) {
        QMemberModel qMemberModel = QMemberModel.memberModel;
        return new JPAQuery(em).from(qMemberModel).where(qMemberModel.contact.eq(contact)).exists();
    }

    public boolean contactExists(String excludedAccountId, String contact) {
        QMemberModel qMemberModel = QMemberModel.memberModel;
        return new JPAQuery(em).from(qMemberModel)
            .where(qMemberModel.accountId.ne(excludedAccountId).and(qMemberModel.contact.eq(contact))).exists();
    }

    public boolean emailExists(String email) {
        QMemberModel qMemberModel = QMemberModel.memberModel;
        return new JPAQuery(em).from(qMemberModel).where(qMemberModel.email.eq(email)).exists();
    }

    public boolean emailExists(String excludedAccountId, String email) {
        QMemberModel qMemberModel = QMemberModel.memberModel;
        return new JPAQuery(em).from(qMemberModel)
            .where(qMemberModel.accountId.ne(excludedAccountId).and(qMemberModel.email.eq(email))).exists();
    }

    public boolean idNumberExists(String idNumber) {
        QMemberModel qMemberModel = QMemberModel.memberModel;
        return new JPAQuery(em).from(qMemberModel).where(qMemberModel.idNumber.eq(idNumber)).exists();
    }

    public boolean idNumberExists(String excludedAccountId, String idNumber) {
        QMemberModel qMemberModel = QMemberModel.memberModel;
        return new JPAQuery(em).from(qMemberModel)
            .where(qMemberModel.accountId.ne(excludedAccountId).and(qMemberModel.idNumber.eq(idNumber))).exists();
    }

    public boolean validEmail(String email) {
        return EMAIL_PATTERN.matcher(email).matches();
    }

    @Override
    public void cleanup() {
        em.flush();
        em.clear();
    }

    protected abstract void processHeaderCell(int i, String headerName);

    protected abstract CellHandler getCellHandler(int index);

    protected abstract void saveOrUpdateDto(MemberModel dto) throws HandlerException;

}
