package com.transretail.crm.web.controller;

import java.math.BigDecimal;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.transretail.crm.common.service.dto.response.ResultList;
import com.transretail.crm.core.service.CodePropertiesService;
import com.transretail.crm.core.service.LookupService;
import com.transretail.crm.giftcard.dto.ReturnRecordDto;
import com.transretail.crm.giftcard.dto.ReturnSearchDto;
import com.transretail.crm.giftcard.dto.SalesOrderDto;
import com.transretail.crm.giftcard.entity.support.ReturnStatus;
import com.transretail.crm.giftcard.entity.support.SalesOrderStatus;
import com.transretail.crm.giftcard.service.GiftCardCxProfileService;
import com.transretail.crm.giftcard.service.ReturnByCardsService;
import com.transretail.crm.giftcard.service.ReturnGiftCardService;
import com.transretail.crm.giftcard.service.SalesOrderService;
import com.transretail.crm.web.controller.util.ControllerResponse;

@Controller
@RequestMapping("/giftcard/return")
public class GiftCardReturnController {

    @Autowired
    private ReturnGiftCardService returnService;
    @Autowired
    private ReturnByCardsService returnByCardsService;
    @Autowired
    private GiftCardCxProfileService customerService;
    @Autowired
    private MessageSource messageSource;
    @Autowired
    private SalesOrderService salesOrderService;
    @Autowired
    private CodePropertiesService codePropertiesService;
    @Autowired
    private LookupService lookupService;

    @RequestMapping(value = "/salesorder/{returnNo}", method = RequestMethod.POST, produces = "application/json; charset=utf-8")
    @ResponseBody
    public ControllerResponse ajaxGetSalesOrder(@PathVariable String returnNo) {
        ControllerResponse theResponse = new ControllerResponse();
        theResponse.setSuccess(true);
        theResponse.setResult(returnService.getSalesOrderByReturnNo(returnNo));
        return theResponse;
    }
    
    @RequestMapping(value = "/{returnNo}", method = RequestMethod.POST, produces = "application/json; charset=utf-8")
    @ResponseBody
    public ControllerResponse ajaxGetReturn(@PathVariable String returnNo) {
        ControllerResponse theResponse = new ControllerResponse();
        theResponse.setSuccess(true);
        Object ret = new Object();
        ret = returnService.getReturnByRecordNo(returnNo);
        if(ret != null)
        	theResponse.setResult(ret);
        else {
        	ret = returnByCardsService.getReturnByRecordNo(returnNo);
        	theResponse.setResult(ret);
        }
        return theResponse;
    }

    @RequestMapping(produces = "text/html")
    public String show(Model uiModel) {
        uiModel.addAttribute("searchCriteria", SearchCriteria.values());
        return "giftcard/return/list";
    }

    @RequestMapping(value = "/bycards/search", method = RequestMethod.POST)
    public @ResponseBody
    ResultList<ReturnRecordDto> searchByCards(@RequestBody ReturnSearchDto filterDto) {
        return returnByCardsService.getReturns(filterDto);
    }
    
    @RequestMapping(value = "/search", method = RequestMethod.POST)
    public @ResponseBody
    ResultList<ReturnRecordDto> search(@RequestBody ReturnSearchDto filterDto) {
        return returnService.getReturns(filterDto);
    }
    
    @RequestMapping(value = "/bycards/save/{status}", method = RequestMethod.POST, produces = "application/json; charset=utf-8")
    @ResponseBody
    public ControllerResponse ajaxSaveByCards(
            @PathVariable("status") String status,
            @ModelAttribute(value = "returnForm") ReturnRecordDto recordDto,
            BindingResult result, HttpServletRequest request) {

        ControllerResponse theResponse = new ControllerResponse();
        theResponse.setSuccess(true);
        
        if(CollectionUtils.isEmpty(recordDto.getReturns())) {
        	result.reject(getMessage("gc_return_empty_cards", null));
        } else if(recordDto.getOrderId() == null) {
        	result.reject(getMessage("gc_return_invalid_sales_order", null));
        }

        if (result.hasErrors()) {
            theResponse.setSuccess(false);
            theResponse.setResult(result.getAllErrors());
            return theResponse;
        }

        recordDto.setStatus(ReturnStatus.valueOf(status));
        returnByCardsService.saveAndReturn(recordDto);

        return theResponse;
    }

    @RequestMapping(value = "/save/{status}", method = RequestMethod.POST, produces = "application/json; charset=utf-8")
    @ResponseBody
    public ControllerResponse ajaxSave(
            @PathVariable("status") String status,
            @ModelAttribute(value = "returnForm") ReturnRecordDto recordDto,
            BindingResult result, HttpServletRequest request) {

        ControllerResponse theResponse = new ControllerResponse();
        theResponse.setSuccess(true);

        if (result.hasErrors()) {
            theResponse.setSuccess(false);
            theResponse.setResult(result.getAllErrors());
            return theResponse;
        }

        recordDto.setStatus(ReturnStatus.valueOf(status));
        returnService.saveReturn(recordDto);

        return theResponse;
    }

    @RequestMapping(value = "/refund/save", method = RequestMethod.POST, produces = "application/json; charset=utf-8")
    @ResponseBody
    public ControllerResponse ajaxRefundSave(
            @ModelAttribute(value = "refundForm") ReturnRecordDto recordDto,
            BindingResult result, HttpServletRequest request) {

        ControllerResponse theResponse = new ControllerResponse();
        theResponse.setSuccess(true);

        validateRefund(recordDto, result);

        if (result.hasErrors()) {
            theResponse.setSuccess(false);
            theResponse.setResult(result.getAllErrors());
            return theResponse;
        }

        if (theResponse.isSuccess()) {
            returnService.saveRefund(recordDto);
        }

        return theResponse;
    }

    private void validateRefund(ReturnRecordDto recordDto, BindingResult result) {
        if (recordDto.getRefundAmount() == null) {
            result.reject(getMessage("propkey_msg_notempty", new Object[]{"gc_refund_amount"}));
        } else {
            BigDecimal totalAmt = recordDto.getRefundAmount();
            if (recordDto.getReplaceAmount() != null) {
                totalAmt = totalAmt.add(recordDto.getReplaceAmount());
            }
            if (recordDto.getReturnAmount().compareTo(totalAmt) < 0) {
                result.reject(getMessage("gc_return_err_refund_gt_return", null));
            }
        }

    }

    @RequestMapping(value = "/validate", method = RequestMethod.POST, produces = "application/json; charset=utf-8")
    @ResponseBody
    public ControllerResponse ajaxValidate(
            @ModelAttribute(value = "returnForm") ReturnRecordDto recordDto,
            BindingResult result, HttpServletRequest request) {

        ControllerResponse theResponse = new ControllerResponse();
        theResponse.setSuccess(true);

        validateReturn(recordDto, result);

        if (result.hasErrors()) {
            theResponse.setSuccess(false);
            theResponse.setResult(result.getAllErrors());
            return theResponse;
        }

        return theResponse;
    }

    private void validateReturn(ReturnRecordDto returnDto, BindingResult result) {
        if (returnDto.getReturnDate() == null) {
            result.reject(getMessage("propkey_msg_notempty", new Object[]{"gc_return_date"}));
        }
        if (StringUtils.isBlank(returnDto.getOrderNo())) {
            result.reject(getMessage("propkey_msg_notempty", new Object[]{"gc_return_order_no"}));
        } else {
            SalesOrderDto order = salesOrderService.getOrderByOrderNo(returnDto.getOrderNo());
            if (order == null || order.getStatus().compareTo(SalesOrderStatus.SOLD) != 0) {
                result.reject(getMessage("gc_return_invalid_sales_order", null));
            } else if (!returnService.isValidSalesOrder(returnDto.getOrderNo())) {
                result.reject(getMessage("gc_return_invalid_sales_order_usedgc", null));
            }
        }

    }

    private String getMessage(String inMsgCode, Object[] inArgMsgCodes) {
        if (inArgMsgCodes != null) {
            for (int i = 0; i < inArgMsgCodes.length; i++) {
                inArgMsgCodes[i]
                        = messageSource.getMessage(inArgMsgCodes[i].toString().toLowerCase(), null, LocaleContextHolder.getLocale());
            }
        }
        return messageSource.getMessage(inMsgCode, inArgMsgCodes, "Info given is invalid.", LocaleContextHolder.getLocale());
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE, produces = "application/json; charset=utf-8")
    @ResponseBody
    public ControllerResponse delete(@PathVariable("id") Long id,
            Model uiModel) {
        ControllerResponse theResponse = new ControllerResponse();
        theResponse.setSuccess(true);
        returnService.deleteReturn(id);
        return theResponse;
    }

    @RequestMapping(value = "/approve/{status}", method = RequestMethod.POST, produces = "application/json; charset=utf-8")
    @ResponseBody
    public ControllerResponse ajaxApprove(
            @PathVariable("status") String status,
            @ModelAttribute(value = "returnForm") ReturnRecordDto orderDto,
            BindingResult result, HttpServletRequest request) {

        ControllerResponse theResponse = new ControllerResponse();
        theResponse.setSuccess(true);

        if (result.hasErrors()) {
            theResponse.setSuccess(false);
            theResponse.setResult(result.getAllErrors());
            return theResponse;
        }

        if (theResponse.isSuccess()) {
            ReturnStatus stat = ReturnStatus.valueOf(status);
            returnService.approveReturn(orderDto.getId(), stat);
        }

        return theResponse;
    }
    
    @RequestMapping(value = "/bycards/validate", method = RequestMethod.POST, produces = "application/json; charset=utf-8")
    @ResponseBody
    public ControllerResponse ajaxValidateByCards(
            @ModelAttribute(value = "returnForm") ReturnRecordDto orderDto,
            BindingResult result, HttpServletRequest request) {

        ControllerResponse theResponse = new ControllerResponse();
        theResponse.setSuccess(true);
        
        returnByCardsService.isValidReturn(orderDto, result);

        if (result.hasErrors()) {
            theResponse.setSuccess(false);
            theResponse.setResult(result.getAllErrors());
            return theResponse;
        }

        if (theResponse.isSuccess()) {
            theResponse.setResult(orderDto);
        }

        return theResponse;
    }


    @RequestMapping(value = "/form/{id}", method = RequestMethod.GET, produces = "text/html")
    public String showForm(
            @PathVariable("id") String id,
            Model uiModel) {
        ReturnRecordDto orderForm = new ReturnRecordDto();
        if (StringUtils.isNumeric(id)) {
            orderForm = returnService.getReturn(Long.valueOf(id));
        }
        populateForm(orderForm, uiModel);
        return "giftcard/return/form";
    }
    
    @RequestMapping(value = "/bycards/form/{id}", method = RequestMethod.GET, produces = "text/html")
    public String showFormByCards(
            @PathVariable("id") String id,
            Model uiModel) {
        ReturnRecordDto orderForm = new ReturnRecordDto();
        if (StringUtils.isNumeric(id)) {
            orderForm = returnByCardsService.getReturn(Long.valueOf(id));
        }
        populateForm(orderForm, uiModel);
        uiModel.addAttribute("returnReasons", lookupService.getActiveDetailsByHeaderCode(codePropertiesService.getReturnReasons()));
        return "giftcard/returnbycards/form";
    }
    
    @RequestMapping(value = "/bycards/view/{id}", method = RequestMethod.GET, produces = "text/html")
    public String viewByCards(
            @PathVariable("id") String id,
            Model uiModel) {
        ReturnRecordDto orderForm = new ReturnRecordDto();
        if (StringUtils.isNumeric(id)) {
            orderForm = returnByCardsService.getReturn(Long.valueOf(id));
        }
        populateForm(orderForm, uiModel);
        return "giftcard/returnbycards/readonly";
    }

    @RequestMapping(value = "/approvalform/{id}", method = RequestMethod.GET, produces = "text/html")
    public String showApprovalForm(
            @PathVariable("id") String id,
            Model uiModel) {
        ReturnRecordDto orderDto = returnService.getReturn(Long.valueOf(id));
        populateForm(orderDto, uiModel);
        return "giftcard/return/approvalform";
    }

    
    private void populateForm(ReturnRecordDto orderDto, Model uiModel) {
        uiModel.addAttribute("returnForm", orderDto);
        /*uiModel.addAttribute("customers", customerService.getAllProfiles());*/
    }

    public enum SearchCriteria {

        PRODUCT("product"),
        ORDER_NO("orderNo");

        private final String field;

        private SearchCriteria(String field) {
            this.field = field;
        }

        public String getField() {
            return field;
        }
    }
}
