package com.transretail.crm.core.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import org.hibernate.envers.Audited;

import com.transretail.crm.core.entity.enums.AppKey;
import com.transretail.crm.core.entity.support.CustomAuditableEntity;


@Entity
@Table(name = "CRM_APPLICATION_CONFIG")
@Audited
public class ApplicationConfig extends CustomAuditableEntity<Long> implements Serializable {

    private static final long serialVersionUID = 1L;

    @NotNull
    @Column(name = "CONFIG_KEY", nullable = false, unique=true)
    @Enumerated(EnumType.STRING)
    private AppKey key;

	@Column(name = "CONFIG_VALUE")
	private String value;
	
	public ApplicationConfig() {}
	
	public ApplicationConfig(AppKey key, String value) {
		super();
		this.key = key;
		this.value = value;
	}

	public AppKey getKey() {
		return key;
	}

	public void setKey(AppKey inJobKey) {
		this.key = inJobKey;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}
}
