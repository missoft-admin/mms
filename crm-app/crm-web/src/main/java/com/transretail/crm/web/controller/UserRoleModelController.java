package com.transretail.crm.web.controller;

import java.io.UnsupportedEncodingException;
import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.util.UriUtils;
import org.springframework.web.util.WebUtils;

import com.transretail.crm.core.entity.UserRoleModel;
import com.transretail.crm.core.service.UserRoleModelService;

@RequestMapping("/userrolemodels")
@Controller
public class UserRoleModelController {

    @Autowired
    UserRoleModelService userRoleModelService;

    String encodeUrlPathSegment(String pathSegment,
            HttpServletRequest httpServletRequest) {
        String enc = httpServletRequest.getCharacterEncoding();
        if (enc == null) {
            enc = WebUtils.DEFAULT_CHARACTER_ENCODING;
        }
        try {
            pathSegment = UriUtils.encodePathSegment(pathSegment, enc);
        } catch (UnsupportedEncodingException uee) {
            uee.printStackTrace();
        }
        return pathSegment;
    }

    void populateEditForm(Model uiModel, UserRoleModel userRoleModel) {
        uiModel.addAttribute("userRoleModel", userRoleModel);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE, produces = "text/html")
    public String delete(@PathVariable("id") String id,
            @RequestParam(value = "page", required = false) Integer page,
            @RequestParam(value = "size", required = false) Integer size,
            Model uiModel) {
        UserRoleModel userRoleModel = userRoleModelService
                .findUserRoleModel(id);
        userRoleModelService.deleteUserRoleModel(userRoleModel);
        uiModel.asMap().clear();
        uiModel.addAttribute("page", (page == null) ? "1" : page.toString());
        uiModel.addAttribute("size", (size == null) ? "10" : size.toString());
        return "redirect:/userrolemodels";
    }

    @RequestMapping(value = "/{id}", params = "form", produces = "text/html")
    public String updateForm(@PathVariable("id") String id, Model uiModel) {
        populateEditForm(uiModel, userRoleModelService.findUserRoleModel(id));
        return "userrolemodels/update";
    }

    @RequestMapping(method = RequestMethod.PUT, produces = "text/html")
    public String update(@Valid UserRoleModel userRoleModel,
            BindingResult bindingResult, Model uiModel,
            HttpServletRequest httpServletRequest) {
        if (bindingResult.hasErrors()) {
            populateEditForm(uiModel, userRoleModel);
            return "userrolemodels/update";
        }
        uiModel.asMap().clear();
        userRoleModelService.updateUserRoleModel(userRoleModel);
        return "redirect:/userrolemodels/"
                + encodeUrlPathSegment(userRoleModel.getId().toString(),
                        httpServletRequest);
    }

    @RequestMapping(produces = "text/html")
    public String list(
            @RequestParam(value = "page", required = false) Integer page,
            @RequestParam(value = "size", required = false) Integer size,
            Model uiModel) {
        if (page != null || size != null) {
            int sizeNo = size == null ? 10 : size.intValue();
            final int firstResult = page == null ? 0 : (page.intValue() - 1)
                    * sizeNo;
            uiModel.addAttribute("userrolemodels", userRoleModelService
                    .findUserRoleModelEntries(firstResult, sizeNo));
            float nrOfPages = (float) userRoleModelService
                    .countAllUserRoleModels() / sizeNo;
            uiModel.addAttribute(
                    "maxPages",
                    (int) ((nrOfPages > (int) nrOfPages || nrOfPages == 0.0) ? nrOfPages + 1
                            : nrOfPages));
        } else {
            uiModel.addAttribute("userrolemodels",
                    userRoleModelService.findAllUserRoleModels());
        }
        return "userrolemodels/list";
    }

    @RequestMapping(value = "/{id}", produces = "text/html")
    public String show(@PathVariable("id") String id, Model uiModel) {
        uiModel.addAttribute("userrolemodel",
                userRoleModelService.findUserRoleModel(id));
        uiModel.addAttribute("itemId", id);
        return "userrolemodels/show";
    }

    @RequestMapping(params = "form", produces = "text/html")
    public String createForm(Model uiModel) {
        populateEditForm(uiModel, new UserRoleModel());
        return "userrolemodels/create";
    }

    @RequestMapping(method = RequestMethod.POST, produces = "text/html")
    public String create(@Valid UserRoleModel userRoleModel,
            BindingResult bindingResult, Model uiModel,
            HttpServletRequest httpServletRequest) {
        if (bindingResult.hasErrors()) {
            populateEditForm(uiModel, userRoleModel);
            return "userrolemodels/create";
        }
        uiModel.asMap().clear();
        userRoleModelService.saveUserRoleModel(userRoleModel);
        return "redirect:/userrolemodels/"
                + encodeUrlPathSegment(userRoleModel.getId().toString(),
                        httpServletRequest);
    }
}
