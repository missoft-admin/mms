package com.transretail.crm.core.entity.enums;

/**
 * @author Mike de Guzman
 */
public enum StampTxnType {

    EARN, REDEEM, VOID
}
