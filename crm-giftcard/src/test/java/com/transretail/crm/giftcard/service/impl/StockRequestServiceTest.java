package com.transretail.crm.giftcard.service.impl;

import org.joda.time.LocalDate;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.transretail.crm.giftcard.dto.StockRequestDto;
import com.transretail.crm.giftcard.entity.ProductProfile;
import com.transretail.crm.giftcard.entity.StockRequest;
import com.transretail.crm.giftcard.entity.support.StockRequestStatus;
import com.transretail.crm.giftcard.repo.ProductProfileRepo;
import com.transretail.crm.giftcard.repo.StockRequestRepo;
import com.transretail.crm.giftcard.service.ProductProfileService;
import com.transretail.crm.giftcard.service.StockRequestService;

/**
 * @author ftopico
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath*:/META-INF/spring/applicationContext*.xml", "classpath:META-INF/spring/applicationContext-test.xml"})
public class StockRequestServiceTest {
    
    public static final String REQUEST_NO_TEST01 = "0001";
    public static final String REQUEST_NO_TEST02 = "0002";
    public static final String TRANSFER_REF_NO_TEST01 = "000111011";
    public static final String TRANSFER_REF_NO_TEST02 = "000122022";
    public static final String TRANSFER_REF_NO_TEST03 = "000133033";
    
    private Long id;
    
    @Autowired
    private StockRequestService stockRequestService;
    
    @Autowired
    private StockRequestRepo repo;
    
    @Autowired
    private ProductProfileRepo productProfileRepo;
    
    @Autowired
    private ProductProfileService productprofileService;
    
    @Before
    public void setup() {
        //Add more data if needed for testing
        StockRequest stockRequest = new StockRequest();
        stockRequest.setRequestNo(REQUEST_NO_TEST01);
        stockRequest.setTransferRefNo(TRANSFER_REF_NO_TEST01);
        stockRequest.setStatus(StockRequestStatus.APPROVED);
        stockRequest.setQuantity(10);
        stockRequest.setRequestDate(LocalDate.now());
        
        id = repo.saveAndFlush(stockRequest).getId();
        
        stockRequest = new StockRequest();
        stockRequest.setRequestNo(REQUEST_NO_TEST02);
        stockRequest.setTransferRefNo(TRANSFER_REF_NO_TEST02);
        stockRequest.setStatus(StockRequestStatus.IN_TRANSIT);
        stockRequest.setQuantity(10);
        stockRequest.setRequestDate(LocalDate.now());
        stockRequest.setProductCode("CODE01");
        repo.saveAndFlush(stockRequest);
        
        stockRequest = new StockRequest();
        stockRequest.setRequestNo(REQUEST_NO_TEST02);
        stockRequest.setTransferRefNo(TRANSFER_REF_NO_TEST02);
        stockRequest.setStatus(StockRequestStatus.IN_TRANSIT);
        stockRequest.setQuantity(10);
        stockRequest.setRequestDate(LocalDate.now());
        stockRequest.setProductCode("CODE02");
        repo.saveAndFlush(stockRequest);
        
        
        stockRequest = new StockRequest();
        stockRequest.setRequestNo(REQUEST_NO_TEST02);
        stockRequest.setTransferRefNo(TRANSFER_REF_NO_TEST03);
        stockRequest.setStatus(StockRequestStatus.IN_TRANSIT);
        stockRequest.setQuantity(10);
        stockRequest.setRequestDate(LocalDate.now());
        stockRequest.setProductCode("CODE01");
        repo.saveAndFlush(stockRequest);
        
        ProductProfile pf = new ProductProfile();
        pf.setProductCode("CODE01");
        pf.setProductDesc("Product desc test");
        productProfileRepo.save(pf);
        
        pf = new ProductProfile();
        pf.setProductCode("CODE02");
        pf.setProductDesc("Product desc test");
        productProfileRepo.save(pf);
    }
    
    @After
    public void cleanUp() {
        repo.deleteAll();
        productProfileRepo.deleteAll();
    }
    
    @Test
    public void transferStockRequestTest() {
        stockRequestService.transferStockRequest(id);
        StockRequestDto stockRequest = stockRequestService.getStockRequest(id);
        Assert.assertTrue(stockRequest.getStatus() == StockRequestStatus.IN_TRANSIT);
    }
    
    @Test
    public void updateStockRequestTest() {
    	StockRequestDto dto = new StockRequestDto();
    	dto.setRequestNo("1");
    	dto.setStatus(StockRequestStatus.APPROVED);
    	dto.setQuantity(10);
    	
    	Long id1 = stockRequestService.updateStockRequest(id, dto);
    	Assert.assertEquals(id, id1);
    	
    	dto = stockRequestService.getStockRequest(id);
    	Assert.assertEquals("1", dto.getRequestNo());
    	Assert.assertEquals(StockRequestStatus.APPROVED, dto.getStatus());
    	Assert.assertEquals(10, dto.getQuantity(), 0);
    }
    
    @Test
    public void hasUnapprovedRequestTest() {
    	Assert.assertFalse(stockRequestService.hasUnapprovedRequests(REQUEST_NO_TEST01));
    	
    	StockRequestDto dto = new StockRequestDto();
    	dto.setRequestNo(REQUEST_NO_TEST01);
    	dto.setStatus(StockRequestStatus.REJECTED);
    	dto.setQuantity(10);
    	stockRequestService.saveStockRequest(dto);
    	
    	Assert.assertFalse(stockRequestService.hasUnapprovedRequests(REQUEST_NO_TEST01));
    	
    	dto = new StockRequestDto();
    	dto.setRequestNo(REQUEST_NO_TEST01);
    	dto.setStatus(StockRequestStatus.APPROVED);
    	dto.setQuantity(10);
    	stockRequestService.saveStockRequest(dto);
    	
    	Assert.assertFalse(stockRequestService.hasUnapprovedRequests(REQUEST_NO_TEST01));
    	
    	dto = new StockRequestDto();
    	dto.setRequestNo(REQUEST_NO_TEST01);
    	dto.setStatus(StockRequestStatus.FORAPPROVAL);
    	dto.setQuantity(10);
    	stockRequestService.saveStockRequest(dto);
    	
    	Assert.assertTrue(stockRequestService.hasUnapprovedRequests(REQUEST_NO_TEST01));
    }
    
    @Test
    public void createStockRequestNumberTest() {
    	Assert.assertEquals(LocalDate.now().toString("yyyyMMdd") + "0003", 
    			stockRequestService.createStockRequestNumber());
    	StockRequestDto dto = new StockRequestDto();
    	dto.setRequestNo(stockRequestService.createStockRequestNumber());
    	dto.setStatus(StockRequestStatus.REJECTED);
    	dto.setQuantity(10);
    	stockRequestService.saveStockRequest(dto);
    	Assert.assertEquals(LocalDate.now().toString("yyyyMMdd") + "0004", 
    			stockRequestService.createStockRequestNumber());
    }
    
}
