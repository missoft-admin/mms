<%@include file="/WEB-INF/views/common/taglibs.jsp" %>
<div id="addEditItemDialog" class="modal hide" style="display: none;">
  
  
  
  <div class="modal-body">
    <form id="itemForm" class="row-fluid">
      <div class="span12 errorMessages error">&nbsp;</div>
      <div class="span6">
        <div>
          <spring:message code="gc.order.label.generationType" var="generationType"/>
          <label for="generationType" title="${generationType}">${generationType}</label>

          <div>
            <input id="generationType" name="generationType" title="${generationType}" type="text">
          </div>
        </div>
        <div>
          <spring:message code="gc.order.label.quantity" var="quantity"/>
          <label for="quantity" title="${quantity}">${quantity}</label>

          <div>
            <input id="quantity" name="quantity" title="${quantity}" type="text" value="0">
            <span class="error" style="display: none;">Invalid integer</span>
          </div>
        </div>
      </div>
      <div class="span">
        <div>
          <label for="giftCardId"><spring:message code="gc.order.label.giftCardName"/></label>

          <div>
            <%--TODO: Create and use vendor search instead of displaying everything in dropdown--%>
            <select id="giftCardId" name="giftCardId">
              <c:forEach items="${gcs.results}" var="gc">
                <option value="${gc.id}">${gc.name}</option>
              </c:forEach>
            </select>
            <input id="giftCardName" name="giftCardName" type="hidden"/>
          </div>
        </div>
        <div>
          <label for="storeId"><spring:message code="gc.order.label.storeName"/></label>

          <div>
            <%--TODO: Create and use vendor search instead of displaying everything in dropdown--%>
            <select id="storeId" name="storeId">
              <c:forEach items="${stores.results}" var="store">
                <option value="${store.id}">${store.name}</option>
              </c:forEach>
            </select>
            <input id="storeName" name="storeName" type="hidden"/>
          </div>
        </div>
        <div>
          <spring:message code="gc.order.label.lpo" var="lpo"/>
          <label for="lpo" title="${lpo}">${lpo}</label>

          <div>
            <input id="lpo" name="lpo" title="${lpo}" type="text">
          </div>
        </div>
      </div>
    </form>
    <input id="itemId" type="hidden" name="itemId" value=""/>
    <input id="itemRowId" type="hidden"/>
  </div>
  <div class="modal-footer">
    <button id="saveItem" class="btn btn-primary" aria-hidden="true"><spring:message code="save"/></button>
    <button id="closeItemDialog" class="btn" aria-hidden="true"><spring:message code="cancel"/></button>
  </div>
</div>
<style>
  #addEditItemDialog {
    width: 40%;
  }
</style>
<script>
  $( document ).ready( function () {
    $( '#quantity' ).numberInput( {'maxChar' : 7} );
  } )
</script>