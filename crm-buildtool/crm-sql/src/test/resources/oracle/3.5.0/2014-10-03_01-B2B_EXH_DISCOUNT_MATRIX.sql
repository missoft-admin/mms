create table CRM_B2B_EXH_DISC (
        id number(19,0) not null,
        CREATED_BY varchar2(255 char),
        CREATED_DATETIME timestamp,
        LAST_UPDATED_BY varchar2(255 char),
        LAST_UPDATED_DATETIME timestamp,
        DISCOUNT number(19,2),
        MAX_AMOUNT number(19,2),
        MIN_AMOUNT number(19,2),
        LOCATION number(19,0),
        primary key (id)
    );

create table CRM_B2B_EXH_LOC (
        id number(19,0) not null,
        CREATED_BY varchar2(255 char),
        CREATED_DATETIME timestamp,
        LAST_UPDATED_BY varchar2(255 char),
        LAST_UPDATED_DATETIME timestamp,
        END_DATE date,
        NAME varchar2(255 char),
        START_DATE date,
        ADDRESS varchar2(255 char),
        primary key (id)
    );


alter table CRM_B2B_EXH_DISC 
        add constraint FK_id01j3cl5uo1i0kfixs88f3au 
        foreign key (LOCATION) 
        references CRM_B2B_EXH_LOC;