<%@ include file="../taglibs.jsp" %>
<c:if test="${not empty successMessages}">
  <div class="alert alert-info">
    <button type="button" class="close" data-dismiss="alert">&times;</button>
    <c:out value="${successMessages}"/>
  </div>
</c:if>
<c:if test="${not empty errorMessages}">
  <div class="alert alert-error">
    <button type="button" class="close" data-dismiss="alert">&times;</button>
    <c:out value="${errorMessages}"/>
  </div>
</c:if>


<c:forEach items="${requestScope}" var="par">
  <c:if test="${fn:startsWith(par, 'org.springframework.validation.BindingResult' )}">
    <%--${par.value}--%>
    <c:if test="${!empty par.value.fieldErrors}">
      <div class="alert alert-error">
        <button type="button" class="close" data-dismiss="alert">&times;</button>
        <c:forEach items="${par.value.fieldErrors}" var="fieldError" varStatus="status">
          <c:forEach items="${fieldError.codes}" var="fieldErrorCode">
            <spring:message code="${fieldErrorCode}" arguments="${fieldError.arguments}" var="fieldErrorMessage" text=""/>
            <c:if test="${!empty fieldErrorMessage}">
              <!-- no break in jstl foreach -->
              <c:set var="errorMessageDisplay" value="${fieldErrorMessage}"/>
            </c:if>
          </c:forEach>
          <c:choose>
            <c:when test="${!empty errorMessageDisplay}">
              <c:out value="${status.index + 1}"/>.&nbsp;<c:out value="${errorMessageDisplay}"/>
              <c:set var="errorMessageDisplay" value=""/>
            </c:when>
            <c:otherwise>
              <c:out value="${status.index + 1}"/>.&nbsp;
              <c:if test="${not fn:containsIgnoreCase(fieldError.defaultMessage, fieldError.field)}">
                <c:out value="${fieldError.field  }"/> -
              </c:if>
              <c:out value="${fieldError.defaultMessage}" />
            </c:otherwise>
          </c:choose>
          <br/>
        </c:forEach>
      </div>
    </c:if>
  </c:if>
</c:forEach>