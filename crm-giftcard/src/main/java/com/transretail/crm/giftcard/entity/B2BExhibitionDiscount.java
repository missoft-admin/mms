package com.transretail.crm.giftcard.entity;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.transretail.crm.core.entity.support.CustomAuditableEntity;

/**
 *
 */
@Entity
@Table(name = "CRM_B2B_EXH_DISC")
public class B2BExhibitionDiscount extends CustomAuditableEntity<Long> implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "LOCATION")
	private B2BExhibitionLocation location;
	
	@Column(name = "MIN_AMOUNT")
	private BigDecimal minAmount;
	
	@Column(name = "MAX_AMOUNT")
	private BigDecimal maxAmount;
	
	@Column(name = "DISCOUNT")
	private BigDecimal discount;

	public B2BExhibitionLocation getLocation() {
		return location;
	}

	public void setLocation(B2BExhibitionLocation location) {
		this.location = location;
	}

	public BigDecimal getMinAmount() {
		return minAmount;
	}

	public void setMinAmount(BigDecimal minAmount) {
		this.minAmount = minAmount;
	}

	public BigDecimal getMaxAmount() {
		return maxAmount;
	}

	public void setMaxAmount(BigDecimal maxAmount) {
		this.maxAmount = maxAmount;
	}

	public BigDecimal getDiscount() {
		return discount;
	}

	public void setDiscount(BigDecimal discount) {
		this.discount = discount;
	}
	
	
	
	
}
