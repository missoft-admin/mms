package com.transretail.crm.report.template.impl;

import java.io.File;
import java.math.BigDecimal;
import java.util.*;

import javax.annotation.Resource;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import net.sf.jasperreports.engine.JRParameter;

import org.apache.commons.io.FileUtils;
import org.joda.time.LocalDate;
import org.joda.time.LocalDateTime;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.context.support.MessageSourceResourceBundle;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.transretail.crm.common.reporting.exception.ReportException;
import com.transretail.crm.common.reporting.jasper.JRProcessor;
import com.transretail.crm.common.reporting.service.ReportService;
import com.transretail.crm.core.entity.lookup.LookupDetail;
import com.transretail.crm.core.entity.lookup.LookupHeader;
import com.transretail.crm.core.repo.LookupDetailRepo;
import com.transretail.crm.core.repo.LookupHeaderRepo;
import com.transretail.crm.core.service.CodePropertiesService;
import com.transretail.crm.giftcard.entity.CardVendor;
import com.transretail.crm.giftcard.entity.GiftCardCustomerProfile;
import com.transretail.crm.giftcard.entity.GiftCardInventory;
import com.transretail.crm.giftcard.entity.GiftCardOrder;
import com.transretail.crm.giftcard.entity.GiftCardOrderItem;
import com.transretail.crm.giftcard.entity.GiftCardTransaction;
import com.transretail.crm.giftcard.entity.GiftCardTransactionItem;
import com.transretail.crm.giftcard.entity.ProductProfile;
import com.transretail.crm.giftcard.entity.ProductProfile.Denomination;
import com.transretail.crm.giftcard.entity.SalesOrder;
import com.transretail.crm.giftcard.entity.SalesOrderItem;
import com.transretail.crm.giftcard.entity.support.GiftCardOrderStatus;
import com.transretail.crm.giftcard.entity.support.GiftCardSaleTransaction;
import com.transretail.crm.giftcard.entity.support.GiftCardSalesType;
import com.transretail.crm.giftcard.entity.support.ProductProfileStatus;
import com.transretail.crm.giftcard.entity.support.SalesOrderStatus;
import com.transretail.crm.giftcard.entity.support.SalesOrderType;
import com.transretail.crm.giftcard.repo.CardVendorRepo;
import com.transretail.crm.giftcard.repo.GiftCardCustomerProfileRepo;
import com.transretail.crm.giftcard.repo.GiftCardInventoryRepo;
import com.transretail.crm.giftcard.repo.GiftCardOrderRepo;
import com.transretail.crm.giftcard.repo.GiftCardTransactionRepo;
import com.transretail.crm.giftcard.repo.ProductProfileRepo;
import com.transretail.crm.giftcard.repo.SalesOrderRepo;
import com.transretail.crm.giftcard.service.GiftCardInventoryService;
import com.transretail.crm.giftcard.service.GiftCardOrderService;
import com.transretail.crm.report.template.CommonReportFilter;
import com.transretail.crm.report.template.ReportTemplate;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath*:/META-INF/spring/applicationContext*.xml",
    "/META-INF/spring/applicationContext-test.xml"})
public class GiftCardAgedAnalysisReportTest {
	@PersistenceContext
    private EntityManager em;

	private CardVendor savedVendor;
    private GiftCardOrder savedGiftCardOrder;
    private ProductProfile savedProfile;
    private SalesOrder order;
    
    @Autowired
    private CardVendorRepo vendorRepo;
    @Autowired
    private ProductProfileRepo profileRepo;
    @Autowired
    private GiftCardOrderRepo giftCardOrderRepo;
    @Autowired
    private GiftCardOrderService orderService;
    @Autowired
    private GiftCardInventoryService giftCardInventoryService;
    @Autowired
    private GiftCardTransactionRepo transactionRepo;
    @Autowired
	private GiftCardCustomerProfileRepo customerRepo;
	@Autowired
	private GiftCardInventoryRepo inventoryRepo;
	@Autowired
	private SalesOrderRepo orderRepo;
	@Autowired
    private ReportService reportService;
	@Autowired
    private MessageSource messageSource;
	@Resource(name = "giftCardAgedAnalysisReport")
    private ReportTemplate giftCardAgedAnalysisReport;
	
	private static final Logger logger = LoggerFactory.getLogger(GiftCardAgedAnalysisReportTest.class);
	
	@Autowired
	private CodePropertiesService codePropertiesService;
	@Autowired
	private LookupHeaderRepo lookupHeaderRepo;
	@Autowired
	private LookupDetailRepo lookupDetailRepo;

	
	@Before
	public void setUp() {
		
		LookupHeader hdr = new LookupHeader(codePropertiesService.getProductProfileFaceValue());
        hdr = lookupHeaderRepo.saveAndFlush(hdr);

        LookupDetail dtl = new LookupDetail("PPFV1");
        dtl.setDescription("100000");
        dtl.setHeader(hdr);
        dtl = lookupDetailRepo.saveAndFlush(dtl);
        
		savedProfile = new ProductProfile();
		savedProfile.setCardFee(1.5);
		savedProfile.setEffectiveMonths(12l);
		savedProfile.setFaceValue(dtl);
		savedProfile.setProductCode("0000000000001");
		savedProfile.setStatus(ProductProfileStatus.APPROVED);
		savedProfile.setProductDesc("PROD");
		savedProfile.setUnitCost(.5);
		savedProfile = profileRepo.save(savedProfile);
		
		GiftCardCustomerProfile customer = new GiftCardCustomerProfile();
		customer.setContactFirstName("fname");
		customer.setContactLastName("lname");
		customer.setContactNo("contactno");
		customer.setName("name");
		customer.setInvoiceTitle("invoicetitle");
		customer.setCustomerId("customerid");
		customer = customerRepo.save(customer);
		
		order = new SalesOrder();
		order.setOrderNo("BBMMS0501001");
		order.setOrderDate(new LocalDate());
		order.setStatus(SalesOrderStatus.SOLD);
		order.setOrderType(SalesOrderType.B2B_SALES);
		order.setItems(new HashSet<SalesOrderItem>());
		order.setCustomer(customer);
		
		SalesOrderItem item = new SalesOrderItem();
		item.setProduct(savedProfile);
		item.setQuantity(10l);
		item.setFaceAmount(new BigDecimal(Denomination.D500K.getAmount() * 10));
		item.setOrder(order);
		order.getItems().add(item);
		order = orderRepo.save(order);
		
		savedVendor = new CardVendor();
        savedVendor.setFormalName("name");
        savedVendor.setMailAddress("add");
        savedVendor.setEmailAddress("email");
        savedVendor.setEnabled(true);
        savedVendor.setRegion("region");
        savedVendor.setDefaultVendor(true);
        savedVendor = vendorRepo.save(savedVendor);
        
        savedGiftCardOrder = new GiftCardOrder();
        savedGiftCardOrder.setMoNumber("140403000001");
        savedGiftCardOrder.setPoNumber("0001");
        savedGiftCardOrder.setMoDate(new LocalDate());
        savedGiftCardOrder.setPoDate(new LocalDate());
        savedGiftCardOrder.setVendor(savedVendor);
        savedGiftCardOrder.setStatus(GiftCardOrderStatus.APPROVED);
        savedGiftCardOrder.setItems(new ArrayList<GiftCardOrderItem>());
        
        GiftCardOrderItem gcItem = new GiftCardOrderItem();
        gcItem.setProfile(savedProfile);
        gcItem.setProductCode(savedProfile.getProductCode());
        gcItem.setQuantity(10L);
        gcItem.setOrder(savedGiftCardOrder);
        savedGiftCardOrder.getItems().add(gcItem);
        
        savedGiftCardOrder = giftCardOrderRepo.save(savedGiftCardOrder);
		
        List<GiftCardInventory> inventories = Lists.newArrayList();
		for(int i = 0; i < 10; i++) {
			GiftCardInventory inventory = new GiftCardInventory();
	        inventory.setProfile(savedProfile);
	        inventory.setProductName(savedProfile.getProductDesc());
	        inventory.setProductCode(savedProfile.getProductCode());
	        inventory.setSeqNo(1);
	        inventory.setBatchNo(1);
	        inventory.setOrderDate(savedGiftCardOrder.getMoDate());
	        inventory.setFaceValue(new BigDecimal(Denomination.D500K.getAmount()));
	        inventory.setOrder(savedGiftCardOrder);
	        inventory.setIsEgc(false);
	        inventory.setSeries(Long.valueOf(i));
	        inventory.setBarcode(Integer.toString(i));
	        inventory.setSalesType(GiftCardSalesType.B2B);
	        inventories.add(inventory);
		}
		inventoryRepo.save(inventories);
		
		GiftCardTransaction transaction = new GiftCardTransaction();
		transaction.setTransactionNo("1123123");
		transaction.setTransactionDate(new LocalDateTime());
		transaction.setTransactionType(GiftCardSaleTransaction.ACTIVATION);
		transaction.salesOrder(order);
		transaction.setTransactionItems(new HashSet<GiftCardTransactionItem>());
		
		for(int i=0; i <10;i++) {
			GiftCardInventory in = inventories.get(i);
			GiftCardTransactionItem txnItem = new GiftCardTransactionItem();
			txnItem.setGiftCard(in);
			txnItem.setTransaction(transaction);
			txnItem.setTransactionAmount(in.getFaceValue().doubleValue());
			transaction.getTransactionItems().add(txnItem);
		}
		
		transactionRepo.save(transaction);
		
		transaction = new GiftCardTransaction();
		transaction.setTransactionNo("1123124");
		transaction.setTransactionDate(new LocalDateTime());
		transaction.setTransactionType(GiftCardSaleTransaction.REDEMPTION);
		transaction.salesOrder(order);
		transaction.setTransactionItems(new HashSet<GiftCardTransactionItem>());
		
		for(int i=5; i <10;i++) {
			GiftCardInventory in = inventories.get(i);
			GiftCardTransactionItem txnItem = new GiftCardTransactionItem();
			txnItem.setGiftCard(in);
			txnItem.setTransaction(transaction);
			txnItem.setTransactionAmount(in.getFaceValue().doubleValue());
			transaction.getTransactionItems().add(txnItem);
		}
		
		transactionRepo.save(transaction);
	}
	
	@Test
	public void test() throws ReportException {
		
		Map<String, String> map = Maps.newHashMap();
		map.put(CommonReportFilter.DATE_FROM, new LocalDate().toString("dd-MM-yyyy"));
		map.put(CommonReportFilter.DATE_TO, new LocalDate().toString("dd-MM-yyyy"));
		map.put("product", Long.toString(savedProfile.getId()));
		map.put("salesType", "B2B");
		map.put("analysisType", "MONTHLY");
		JRProcessor jrProcessor = giftCardAgedAnalysisReport.createJrProcessor(map);

		File testClassesDir = FileUtils.toFile(Thread.currentThread().getContextClassLoader().getResource(""));
		File output = new File(testClassesDir, "test-store-member-transaction-report-output.pdf");
		logger.debug("Output location {}", output.getAbsolutePath());
		output.delete();
		Locale locale = LocaleContextHolder.getLocale();
		jrProcessor.addParameter(JRParameter.REPORT_LOCALE, locale);
		jrProcessor.addParameter(JRParameter.REPORT_RESOURCE_BUNDLE, new MessageSourceResourceBundle(messageSource, locale));
		reportService.exportPdfReport(jrProcessor, output);

		double bytes = output.length();
		double kilobytes = (bytes / 1024);
		logger.debug("Kilobytes: {}", kilobytes);
	}
}
