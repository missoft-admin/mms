package com.transretail.crm.template.service.streaming.excel.handler.sheet.required;

import static org.apache.commons.lang3.StringUtils.isBlank;
import static org.apache.commons.lang3.StringUtils.isNotBlank;

import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.commons.lang3.StringUtils;
import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.mysema.query.jpa.impl.JPAQuery;
import com.transretail.crm.common.util.SimpleEncrytor;
import com.transretail.crm.core.entity.MemberModel;
import com.transretail.crm.core.entity.QMemberModel;
import com.transretail.crm.core.entity.embeddable.Address;
import com.transretail.crm.core.entity.embeddable.CustomerProfile;
import com.transretail.crm.core.entity.embeddable.ProfessionalProfile;
import com.transretail.crm.core.entity.embeddable.QAddress;
import com.transretail.crm.core.repo.AddressRepo;
import com.transretail.crm.core.security.model.impl.CustomSecurityUserDetailsImpl;
import com.transretail.crm.template.service.streaming.excel.handler.HandlerException;
import com.transretail.crm.template.service.streaming.excel.handler.cell.AccountIdCellHandler;
import com.transretail.crm.template.service.streaming.excel.handler.cell.AddressCityCellHandler;
import com.transretail.crm.template.service.streaming.excel.handler.cell.AddressPostCodeCellHandler;
import com.transretail.crm.template.service.streaming.excel.handler.cell.AddressStreetCellHandler;
import com.transretail.crm.template.service.streaming.excel.handler.cell.BirthDateCellHandler;
import com.transretail.crm.template.service.streaming.excel.handler.cell.BirthPlaceCellHandler;
import com.transretail.crm.template.service.streaming.excel.handler.cell.CellHandler;
import com.transretail.crm.template.service.streaming.excel.handler.cell.DepartmentCellHandler;
import com.transretail.crm.template.service.streaming.excel.handler.cell.EmailCellHandler;
import com.transretail.crm.template.service.streaming.excel.handler.cell.EmployeeTypeCellHandler;
import com.transretail.crm.template.service.streaming.excel.handler.cell.FirstNameCellHandler;
import com.transretail.crm.template.service.streaming.excel.handler.cell.GenderCellHandler;
import com.transretail.crm.template.service.streaming.excel.handler.cell.KtpIdCellHandler;
import com.transretail.crm.template.service.streaming.excel.handler.cell.LastNameCellHandler;
import com.transretail.crm.template.service.streaming.excel.handler.cell.MaritalStatusCellHandler;
import com.transretail.crm.template.service.streaming.excel.handler.cell.MobilePhoneCellHandler;
import com.transretail.crm.template.service.streaming.excel.handler.cell.NationalityCellHandler;
import com.transretail.crm.template.service.streaming.excel.handler.cell.RegisteredStoreCellHandler;
import com.transretail.crm.template.service.streaming.excel.handler.cell.ReligionCellHandler;
import com.transretail.crm.template.service.streaming.excel.handler.cell.TerminationDateCellHandler;
import com.transretail.crm.template.service.streaming.excel.handler.sheet.AbstractSheetHandler;

/**
 * @author Allan G. Ramirez (agramirez@exist.com)
 */
public class UpdateEmpHandler extends AbstractSheetHandler {
    private static final String HEADER_ACCOUNTID = "ACCOUNTNUMBER";
    private static final String HEADER_FIRSTNAME = "FIRSTNAME";
    private static final String HEADER_LASTNAME = "LASTNAME";
    private static final String HEADER_TERMINATIONDATE = "TERMINATIONDATE";
    private static final String HEADER_EMPTYPE = "EMPLOYEETYPE";
    private static final String HEADER_DEPARTMENT = "DEPARTMENT";
    private static final String HEADER_REGISTEREDSTORE = "REGISTEREDSTORE";
    private static final String HEADER_BIRTHDATE = "DATEOFBIRTH";
    private static final String HEADER_BIRTHPLACE = "PLACEOFBIRTH";
    private static final String HEADER_GENDER = "GENDER";
    private static final String HEADER_RELIGION = "RELIGION";
    private static final String HEADER_NATIONALITY = "NATIONALITY";
    private static final String HEADER_MOBILEPHONE = "MOBILEPHONE";
    private static final String HEADER_STREET = "STREET";
    private static final String HEADER_CITY = "CITY";
    private static final String HEADER_ZIPCODE = "POSTCODE";
    private static final String HEADER_EMAIL = "EMAIL";
    private static final String HEADER_MARITALSTATUS = "MARITALSTATUS";
    // Task #85818
    private static final String HEADER_KTPID = "KTPID";

    @Resource(name = "passwordEncoder")
    private PasswordEncoder passwordEncoder;

    private Map<String, CellHandler> cellHandlerClassMap = Maps.newHashMap();
    private CellHandler[] cellHandlers = null;
    private DateTime currentDateTime;
    private CustomSecurityUserDetailsImpl currentUser;

    @Autowired
    private AddressRepo addressRepo;

    public UpdateEmpHandler(DateTime currentDateTime, CustomSecurityUserDetailsImpl currentUser) {
        this.currentDateTime = currentDateTime;
        this.currentUser = currentUser;
        cellHandlerClassMap.put(HEADER_ACCOUNTID, new AccountIdCellHandler());
        cellHandlerClassMap.put(HEADER_FIRSTNAME, new FirstNameCellHandler());
        cellHandlerClassMap.put(HEADER_LASTNAME, new LastNameCellHandler());
        cellHandlerClassMap.put(HEADER_TERMINATIONDATE, new TerminationDateCellHandler());
        cellHandlerClassMap.put(HEADER_EMPTYPE, new EmployeeTypeCellHandler(this));
        cellHandlerClassMap.put(HEADER_DEPARTMENT, new DepartmentCellHandler(this));
        cellHandlerClassMap.put(HEADER_REGISTEREDSTORE, new RegisteredStoreCellHandler());
        cellHandlerClassMap.put(HEADER_BIRTHDATE, new BirthDateCellHandler(this));
        cellHandlerClassMap.put(HEADER_BIRTHPLACE, new BirthPlaceCellHandler());
        cellHandlerClassMap.put(HEADER_GENDER, new GenderCellHandler(this));
        cellHandlerClassMap.put(HEADER_RELIGION, new ReligionCellHandler(this));
        cellHandlerClassMap.put(HEADER_NATIONALITY, new NationalityCellHandler(this));
        cellHandlerClassMap.put(HEADER_MOBILEPHONE, new MobilePhoneCellHandler());
        cellHandlerClassMap.put(HEADER_STREET, new AddressStreetCellHandler());
        cellHandlerClassMap.put(HEADER_CITY, new AddressCityCellHandler());
        cellHandlerClassMap.put(HEADER_ZIPCODE, new AddressPostCodeCellHandler());
        cellHandlerClassMap.put(HEADER_EMAIL, new EmailCellHandler());
        cellHandlerClassMap.put(HEADER_MARITALSTATUS, new MaritalStatusCellHandler(this));
        cellHandlerClassMap.put(HEADER_KTPID, new KtpIdCellHandler());
        cellHandlers = new CellHandler[cellHandlerClassMap.size()];
    }

    @Override
    protected void processHeaderCell(int i, String headerName) {
        CellHandler cellHandler = cellHandlerClassMap.get(headerName);
        if (cellHandler == null) {
            throw new HandlerException(getMessageSource()
                .getMessage("member.template.messages.excel.nohandler", new String[]{sheetName, headerName},
                    LocaleContextHolder.getLocale()));
        }
        cellHandlers[i] = cellHandler;
    }

    @Override
    protected CellHandler getCellHandler(int index) {
        return cellHandlers[index];
    }

    @Override
    protected void saveOrUpdateDto(MemberModel dto) throws HandlerException {
        Locale locale = LocaleContextHolder.getLocale();
        List<String> errors = Lists.newArrayList();

        Date birthDate = dto.getCustomerProfile().getBirthdate();
        if (isBlank(dto.getAccountId())) {
            errors.add(getMessageSource().getMessage("member.template.messages.excel.columns.required",
                new String[]{sheetName, String.valueOf(currentRowNum),
                    StringUtils.join(new String[]{HEADER_ACCOUNTID}, ",")}, locale
            ));
        }
        if (birthDate == null) {
            errors.add(getMessageSource().getMessage("member.template.messages.excel.columns.required",
                new String[]{sheetName, String.valueOf(currentRowNum),
                    StringUtils.join(new String[]{HEADER_BIRTHDATE}, ",")}, locale
            ));
        }
        if (isBlank(dto.getContact())) {
            errors.add(getMessageSource().getMessage("member.template.messages.excel.columns.required",
                new String[]{sheetName, String.valueOf(currentRowNum),
                    StringUtils.join(new String[]{HEADER_MOBILEPHONE}, ",")}, locale
            ));
        }
        if (isBlank(dto.getRegisteredStore())) {
            errors.add(getMessageSource().getMessage("member.template.messages.excel.columns.required",
                new String[]{sheetName, String.valueOf(currentRowNum),
                    StringUtils.join(new String[]{HEADER_REGISTEREDSTORE}, ",")}, locale
            ));
        }
        QMemberModel qMemberModel = QMemberModel.memberModel;
        MemberModel model =
            new JPAQuery(em).from(qMemberModel).where(qMemberModel.accountId.eq(dto.getAccountId())).singleResult(qMemberModel);
        if (model == null) {
            throw new HandlerException(getMessageSource().getMessage("member.template.messages.excel.accountid.notfound",
                new String[]{sheetName, String.valueOf(currentRowNum), dto.getAccountId()},
                LocaleContextHolder.getLocale()));
        } else if (em.contains(model)) {
            em.refresh(model);
        }

        if (isNotBlank(dto.getContact()) && !dto.getContact().equals(model.getContact())) {
            if (contactExists(dto.getAccountId(), dto.getContact())) {
                errors.add(getMessageSource().getMessage("member.template.messages.excel.mobile.exists",
                    new String[]{sheetName, dto.getContact()}, locale));
            }
        }

        if (isNotBlank(dto.getEmail()) && !dto.getEmail().equals(model.getEmail())) {
            if (!validEmail(dto.getEmail())) {
                errors.add(getMessageSource()
                    .getMessage("member.template.messages.excel.email.invalid", new String[]{sheetName, dto.getEmail()}, locale));
            } else if (emailExists(dto.getAccountId(), dto.getEmail())) {
                errors.add(getMessageSource().getMessage("member.template.messages.excel.email.exists",
                    new String[]{sheetName, dto.getEmail()}, locale));
            }
        }

        if (isNotBlank(dto.getKtpId()) && !dto.getKtpId().equals(model.getKtpId()) && idNumberExists(dto.getAccountId(),
            dto.getKtpId())) {
            errors.add(getMessageSource().getMessage("member.template.messages.excel.ktid.exists",
                new String[]{sheetName, dto.getIdNumber().substring("KTP-".length())}, locale));
        }

        if (errors.size() > 0) {
            throw new HandlerException(StringUtils.join(errors, "\r\n"));
        }

        model.setAddress(saveOrUpdateAddress(dto.getAddress(), dto.getAccountId(), qMemberModel));
        model.setFirstName(dto.getFirstName());
        model.setLastName(dto.getLastName());
        model.setAccountStatus(dto.getAccountStatus());
        model.setEmpType(dto.getEmpType());
        if (model.getProfessionalProfile() == null) {
            model.setProfessionalProfile(new ProfessionalProfile());
        }
        model.getProfessionalProfile().setDepartment(dto.getProfessionalProfile().getDepartment());
        model.setRegisteredStore(dto.getRegisteredStore());
        if (model.getCustomerProfile() == null) {
            model.setCustomerProfile(new CustomerProfile());
        }
        model.getCustomerProfile().setBirthdate(dto.getCustomerProfile().getBirthdate());
        model.getCustomerProfile().setPlaceOfBirth(dto.getCustomerProfile().getPlaceOfBirth());
        model.getCustomerProfile().setGender(dto.getCustomerProfile().getGender());
        model.getCustomerProfile().setNationality(dto.getCustomerProfile().getNationality());
        model.setContact(dto.getContact());
        model.setEmail(dto.getEmail());
        model.getCustomerProfile().setMaritalStatus(dto.getCustomerProfile().getMaritalStatus());

        if (StringUtils.isBlank(model.getPassword())) {
            // Task #84568 / Task #88915
            String formattedBirthDate = BIRTHDATE_PASSWORD_FORMAT.format(birthDate);
            model.setPassword(passwordEncoder.encode(formattedBirthDate));
        }
        if (StringUtils.isBlank(model.getPin())) {
            // Bug #85179
            try {
                model.setPin(SimpleEncrytor.getInstance().encrypt(BIRTHDATE_PIN_FORMAT.format(birthDate)));
                model.setPinEncryped(true);
            } catch (SimpleEncrytor.EncryptDecryptException e) {
                throw new HandlerException(e);
            }
        }

        model.setLastUpdated(currentDateTime);
        model.setLastUpdateUser(currentUser.getUsername());

        em.merge(model);

        if (currentRowNum % batchSize == 0) {
            cleanup();
        }
    }

    @Override
    public int getExpectedNumberOfColumns() {
        return cellHandlers.length;
    }

    private Address saveOrUpdateAddress(Address dto, String accountId, QMemberModel qMemberModel) {
        QAddress qAddress = new QAddress("businessAddress");
        Address address = new JPAQuery(em).from(qMemberModel)
            .rightJoin(qMemberModel.address, qAddress)
            .where(qMemberModel.accountId.eq(accountId)).singleResult(qAddress);
        if (address == null) {
            if (StringUtils.isNotBlank(dto.getStreet()) ||
                StringUtils.isNotBlank(dto.getPostCode()) ||
                StringUtils.isNotBlank(dto.getCity())
                ) {
                address = addressRepo.saveAndFlush(dto);
            }
        } else {
            address.setStreet(dto.getStreet());
            address.setCity(dto.getCity());
            address.setPostCode(dto.getPostCode());
            address = addressRepo.saveAndFlush(dto);
        }
        return address;
    }
}
