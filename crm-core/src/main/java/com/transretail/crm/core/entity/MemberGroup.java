package com.transretail.crm.core.entity;


import java.io.Serializable;
import java.util.Arrays;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.apache.commons.lang3.StringUtils;
import org.hibernate.annotations.Type;
import org.hibernate.envers.Audited;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.transretail.crm.core.entity.support.CustomAuditableEntity;


@Audited
@Table(name = "CRM_MEMBER_GRP")
@Entity
public class MemberGroup extends CustomAuditableEntity<Long> implements Serializable {

	private static final long serialVersionUID = 1L;

	@Column(name="NAME")
    private String name;
	
	@Column(name="MEMBERS")
	private String members;
	
	@Embedded
	private MemberGroupRFS rfs;
	
	@Embedded
	private MemberGroupPoints points;
	
	@Column(name="CARD_TYPE_GROUP_CODE")
	private String cardTypeGroupCode;
	
	@Column(name="PREPAID_USER", length = 1)
	@Type(type = "yes_no")
	private Boolean prepaidUser = Boolean.FALSE;
	
	@JsonIgnore
	@OneToMany(mappedBy = "memberGroup", orphanRemoval = true, fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	private List<PromotionMemberGroup> promotionMemberGroups;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

	public List<PromotionMemberGroup> getPromotionMemberGroups() {
		return promotionMemberGroups;
	}

	public void setPromotionMemberGroups(
			List<PromotionMemberGroup> promotionMemberGroups) {
		this.promotionMemberGroups = promotionMemberGroups;
	}

	public MemberGroupRFS getRfs() {
		return rfs;
	}

	public void setRfs(MemberGroupRFS rfs) {
		this.rfs = rfs;
	}

	public MemberGroupPoints getPoints() {
		return points;
	}

	public void setPoints(MemberGroupPoints points) {
		this.points = points;
	}

	public String getCardTypeGroupCode() {
		return cardTypeGroupCode;
	}

	public void setCardTypeGroupCode(String cardTypeGroupCode) {
		this.cardTypeGroupCode = cardTypeGroupCode;
	}

	public String getMembers() {
		return members;
	}

	public void setMembers(String members) {
		this.members = members;
	}
	
	public Boolean getPrepaidUser() {
        return prepaidUser;
    }

    public void setPrepaidUser(Boolean prepaidUser) {
        this.prepaidUser = prepaidUser;
    }

    public List<String> getMemberList() {
		if(StringUtils.isNotBlank(members))
            return Arrays.asList(StringUtils.split(members, ","));
        else
			return null;
	}
    
}



