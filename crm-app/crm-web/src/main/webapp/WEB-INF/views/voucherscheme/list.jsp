<%@ include file="../common/taglibs.jsp" %>

<sec:authorize var="isHelpDesk" ifAnyGranted="ROLE_HELPDESK"/>

<style type="text/css">
<!--
  #content_voucherSchemeList .dataTable tr td:last-child {
    min-width: 100px;
  }
-->
</style>

  <spring:message var="typeName" code='menu_item_voucherschememodel_new_label'/>
  <spring:url value="/resources/images/update.png" var="update_image_url" />
  <spring:url value="/resources/images/delete.png" var="delete_image_url" />

  <div class="page-header page-header2">
      <h1><spring:message code="menutab_employeemgmt_manager_rewardscheme" /></h1>
  </div>
  <c:if test="${!isHelpDesk}" >
  <div>
  <c:url var="url_create" value="/voucherscheme?form" />
  <button class="btn btn-primary mb20 pull-right" onclick="javascript: createVoucherScheme( '${url_create}', '${pageContext.request.queryString}' );">
        <spring:message code="label_com_transretail_crm_entity_voucherschememodel" var="argument" />
			<spring:message code="global_menu_new" arguments="${argument}" />
  </button>
  </div>
  </c:if>
  <div class="clearfix"></div>
  
  <div id="content_voucherSchemeList">

  <div id="list_voucherScheme" 
    data-url="<c:url value="/voucherscheme/list" />"
    data-hdr-value-from="<spring:message code="label_com_transretail_crm_entity_voucherschememodel_valuefrom" />"
    data-hdr-value-to="<spring:message code="label_com_transretail_crm_entity_voucherschememodel_valueto" />" 
    data-hdr-amount="<spring:message code="label_com_transretail_crm_entity_voucherschememodel_amount" />" 
    data-hdr-valid-period="<spring:message code="label_com_transretail_crm_entity_voucherschememodel_validperiod" />"
    data-hdr-status="<spring:message code="label_com_transretail_crm_entity_voucherschememodel_status" />"
    data-role-helpdesk="${isHelpDesk}" ></div>

	<style type="text/css">table { clear: both; }</style>
  </div>
  
  <c:if test="${!isHelpDesk}" >
  <div class="hide">
  <div id="updateBtn">
    <spring:message arguments="${typeName}" code="entity_update" var="update_label" htmlEscape="false" />
    <a class="btn pull-left icn-edit tiptip" title="${fn:escapeXml(update_label)}" alt="${fn:escapeXml(update_label)}" onclick="updateVoucherScheme( '%ITEMID%', '${pageContext.request.queryString}'  );" href="#" id="">
      
    </a>
  </div>
  
  <div id="deleteBtn">
    <spring:message arguments="${typeName}" code="entity_delete"
      var="delete_label" htmlEscape="false" />
    <c:set var="delete_confirm_msg"><spring:escapeBody 
      javaScriptEscape="true"><spring:message
      code="entity_delete_confirm" /></spring:escapeBody></c:set>
      <input class="btn btn-primary icn-delete tiptip" type="button" alt="${fn:escapeXml(delete_label)}" title="${fn:escapeXml(delete_label)}" value="${delete_label}" onclick="getConfirm('${delete_confirm_msg}',function(result) {if(result) $('form#deleteForm%ITEMID%').submit();});" />
      <div class="hide">
      <spring:url value="/voucherscheme/%ITEMID%"
      var="delete_form_url" /> 
    <form:form action="${delete_form_url}" id="deleteForm%ITEMID%" method="DELETE">
      <c:if test="${not empty param.page}">
        <input name="page" type="hidden" value="1" />
      </c:if>
      <c:if test="${not empty param.size}">
        <input name="size" type="hidden"
          value="${fn:escapeXml(param.size)}" />
      </c:if>
    </form:form></div>
  </div>
  </div>
  </c:if>
  <%--
	<c:choose>
	<c:when test="${not empty voucherSchemes}">
	<div class="">
		<table class="table table-condensed table-striped">
			<thead>
				<tr>
					<th><spring:message code="label_com_transretail_crm_entity_voucherschememodel_valuefrom"/></th>
					<th><spring:message code="label_com_transretail_crm_entity_voucherschememodel_valueto"/></th>
					<th><spring:message code="label_com_transretail_crm_entity_voucherschememodel_amount"/></th>
					<th><spring:message code="label_com_transretail_crm_entity_voucherschememodel_validperiod"/></th>
					<th><spring:message code="label_com_transretail_crm_entity_voucherschememodel_status"/></th>
					<th><![CDATA[&nbsp;]]></th>
					<th><![CDATA[&nbsp;]]></th>
				</tr>
			</thead>
			<tbody>
				<c:forEach var="item" items="${voucherSchemes}" >
				<tr>
					<td><c:out value="${item.valueFrom}" /></td>
					<td><c:out value="${item.valueTo}" /></td>
					<td><c:out value="${item.amount}" /></td>
					<td><c:out value="${item.validPeriod}" /></td>
					<td><c:out value="${item.status}" /></td>
					<td class="utilbox">
						<spring:message arguments="${typeName}" code="entity_update" var="update_label"
						htmlEscape="false" /><a
						title="${fn:escapeXml(update_label)}"
						alt="${fn:escapeXml(update_label)}"
						onclick="updateVoucherScheme( '${item.id}', '${pageContext.request.queryString}'  );"
						href="#" id=""><img
							title="${fn:escapeXml(update_label)}"
							src="${update_image_url}"
							class="image updateLink_id"
							alt="${fn:escapeXml(update_label)}"/></a></td>
					<c:if test="${!isApprover}">
					<td class="utilbox">
                          <spring:message arguments="${typeName}" code="entity_delete"
                          var="delete_label" htmlEscape="false" />
                        <c:set var="delete_confirm_msg"><spring:escapeBody 
                          javaScriptEscape="true"><spring:message
                          code="entity_delete_confirm" /></spring:escapeBody></c:set>
                          <input class="btn btn-primary icn-delete" type="button" alt="${fn:escapeXml(delete_label)}" title="${fn:escapeXml(delete_label)}" value="${delete_label}" onclick="getConfirm('${delete_confirm_msg}',function(result) {if(result) $('form#deleteForm${item.id}').submit();});" />
                          <div class="hide">
                          <spring:url value="/voucherscheme/${item.id}"
								var="delete_form_url" /> 
							<form:form action="${delete_form_url}" id="deleteForm${item.id}" method="DELETE">
								<c:if test="${not empty param.page}">
									<input name="page" type="hidden" value="1" />
								</c:if>
								<c:if test="${not empty param.size}">
									<input name="size" type="hidden"
										value="${fn:escapeXml(param.size)}" />
								</c:if>
							</form:form></div></td>
							</c:if>
				</tr>
				</c:forEach>
			</tbody>
		</table>
	</div>
	<jsp:include page="../common/pagination.jsp" />
	</c:when>
	<c:otherwise>
		<spring:message arguments="${typeName}" code="entity_not_found"/>
	</c:otherwise>
	</c:choose>
	 --%>
   
  
	
	<jsp:include page="voucherSchemeDialog.jsp" />
    <jsp:include page="../common/confirm.jsp" />
    
    <link href="<spring:url value="/css/dataTables.css" />" rel="stylesheet">
    <script src="<spring:url value="/js/jquery/jquery.dataTables.js" />" ></script>
    <script src="<spring:url value="/js/jquery/jquery.dataTables.linkLengthMenu.js" />"></script>
    <script src="<spring:url value="/js/jquery/jquery.dataTables.customPaging.js" />"></script>
    <script src="<spring:url value="/js/jquery/ajax-datatable.js" />" ></script>
    <script src="<spring:url value="/js/common/form2js.js" />" ></script>
    <script src="<spring:url value="/js/common/json2.js" />" ></script>
    <script src="<spring:url value="/js/jquery/jquery.toObject.js" />" ></script>

    <SCRIPT type="text/javascript" src="<c:url value="/js/viewspecific/voucherscheme/voucherScheme.js" />"><![CDATA[&nbsp;]]></SCRIPT>
    <SCRIPT type="text/javascript" src="<c:url value="/js/viewspecific/voucherscheme/voucherSchemeDialog.js" />"><![CDATA[&nbsp;]]></SCRIPT>
    <link href="<c:url value="/styles/temp.css" />" rel="stylesheet" />
