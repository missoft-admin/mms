package com.transretail.crm.core.dto;

import java.util.ArrayList;
import java.util.List;

import com.mysema.query.types.expr.BooleanExpression;
import com.transretail.crm.common.service.dto.request.AbstractSearchFormDto;
import com.transretail.crm.core.entity.lookup.QLookupDetail;
import com.transretail.crm.core.entity.lookup.QLookupHeader;
import org.springframework.util.StringUtils;

/**
 *
 */
public class LookupHeaderSearchDto extends AbstractSearchFormDto {
    private String code;
    private String description;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public BooleanExpression createSearchExpression() {
        List<BooleanExpression> expressions = new ArrayList<BooleanExpression>();
        QLookupHeader qLookupHeader = QLookupHeader.lookupHeader;
        if (StringUtils.hasText(code)) {
            expressions.add(qLookupHeader.code.startsWith(code));
        }
        if (StringUtils.hasText(description)) {
            expressions.add(qLookupHeader.description.startsWith(description));
        }
        return BooleanExpression.allOf(expressions.toArray(new BooleanExpression[expressions.size()]));
    }
}
