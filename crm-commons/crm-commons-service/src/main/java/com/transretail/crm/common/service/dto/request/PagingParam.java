/*
 * Copyright (c) 2012. St. Luke's Medical Center
 * All rights reserved.
 */
package com.transretail.crm.common.service.dto.request;

import java.util.Map;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 *
 */
// CSOFF: CyclomaticComplexity|NPathComplexity|MagicNumber
public class PagingParam {
    // Default page size
    private int pageSize = 10;
    /**
     * 0 based page number
     */
    private int pageNo;
    private Map<String, Boolean> order;
    private Map<String, DataType> dataTypes;

    @JsonProperty("pageSize")
    public int getPageSize() {
        return pageSize;
    }

    public void setPageSize(int pageSize) {
        this.pageSize = pageSize;
    }

    @JsonProperty("pageNo")
    public int getPageNo() {
        return pageNo;
    }

    public void setPageNo(int pageNo) {
        this.pageNo = pageNo;
    }

    @JsonProperty("pagingSort")
    public Map<String, Boolean> getOrder() {
        return order;
    }

    public void setOrder(Map<String, Boolean> order) {
        this.order = order;
    }
    
    @JsonProperty("dataTypes")
    public Map<String, DataType> getDataTypes() {
		return dataTypes;
	}

	public void setDataTypes(Map<String, DataType> dataTypes) {
		this.dataTypes = dataTypes;
	}

	@Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        PagingParam that = (PagingParam) o;

        if (pageNo != that.pageNo || pageSize != that.pageSize) {
            return false;
        }

        return true;
    }

    @Override
    public int hashCode() {
        int result = pageSize;
        result = 31 * result + pageNo;
        return result;
    }
    
    public enum DataType {
    	STRING, NUMBER, DATE
    }
}
