package com.transretail.crm.core.service.impl;

import java.io.InputStream;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;
import org.hibernate.Query;
import org.hibernate.ScrollMode;
import org.hibernate.ScrollableResults;
import org.hibernate.Session;
import org.joda.time.DateTime;
import org.joda.time.LocalDate;
import org.joda.time.Months;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.mysema.query.jpa.JPQLQuery;
import com.mysema.query.jpa.impl.JPAQuery;
import com.mysema.query.types.expr.BooleanExpression;
import com.transretail.crm.common.enums.MessageType;
import com.transretail.crm.common.reporting.jasper.ClasspathFileResolver;
import com.transretail.crm.common.reporting.jasper.DefaultJRProcessor;
import com.transretail.crm.common.reporting.jasper.JRProcessor;
import com.transretail.crm.common.service.dto.request.PagingParam;
import com.transretail.crm.common.service.dto.request.PagingParam.DataType;
import com.transretail.crm.common.service.dto.rest.ReturnMessage;
import com.transretail.crm.common.service.util.SpringDataPagingUtil;
import com.transretail.crm.common.util.StringUtil;
import com.transretail.crm.core.dto.EmployeePurchaseTxnDto;
import com.transretail.crm.core.dto.MemberDto;
import com.transretail.crm.core.dto.MemberPointsReportDto;
import com.transretail.crm.core.dto.PointsConfigDto;
import com.transretail.crm.core.dto.PointsDto;
import com.transretail.crm.core.dto.PointsExpireReportDto;
import com.transretail.crm.core.dto.PointsResultList;
import com.transretail.crm.core.dto.PointsSearchDto;
import com.transretail.crm.core.dto.PosTxItemDto;
import com.transretail.crm.core.dto.PromoRewardDto;
import com.transretail.crm.core.dto.ServiceDto;
import com.transretail.crm.core.dto.StoreDto;
import com.transretail.crm.core.entity.ApplicationConfig;
import com.transretail.crm.core.entity.EmployeePurchaseTxnModel;
import com.transretail.crm.core.entity.MemberModel;
import com.transretail.crm.core.entity.PointsTxnModel;
import com.transretail.crm.core.entity.PosEftPayment;
import com.transretail.crm.core.entity.PosPayment;
import com.transretail.crm.core.entity.PosTransaction;
import com.transretail.crm.core.entity.PosTxItem;
import com.transretail.crm.core.entity.Promotion;
import com.transretail.crm.core.entity.QPointsTxnModel;
import com.transretail.crm.core.entity.QPosEftPayment;
import com.transretail.crm.core.entity.QPosPayment;
import com.transretail.crm.core.entity.TransactionAudit;
import com.transretail.crm.core.entity.enums.AppConfigDefaults;
import com.transretail.crm.core.entity.enums.AppKey;
import com.transretail.crm.core.entity.enums.MemberStatus;
import com.transretail.crm.core.entity.enums.PointTxnType;
import com.transretail.crm.core.entity.enums.Status;
import com.transretail.crm.core.entity.enums.SvcReturnType;
import com.transretail.crm.core.entity.enums.TxnStatus;
import com.transretail.crm.core.entity.enums.VoucherTransactionType;
import com.transretail.crm.core.entity.lookup.LookupDetail;
import com.transretail.crm.core.entity.lookup.Store;
import com.transretail.crm.core.repo.ApplicationConfigRepo;
import com.transretail.crm.core.repo.MemberRepo;
import com.transretail.crm.core.repo.PointsTxnModelRepo;
import com.transretail.crm.core.repo.StoreRepo;
import com.transretail.crm.core.security.util.UserUtil;
import com.transretail.crm.core.service.ApprovalMatrixService;
import com.transretail.crm.core.service.CodePropertiesService;
import com.transretail.crm.core.service.EmployeePurchaseService;
import com.transretail.crm.core.service.LookupService;
import com.transretail.crm.core.service.PointsTxnManagerService;
import com.transretail.crm.core.service.PosTransactionService;
import com.transretail.crm.core.service.PromotionService;
import com.transretail.crm.core.service.StoreService;
import com.transretail.crm.core.service.TransactionAuditService;
import com.transretail.crm.core.util.generator.IdGeneratorService;

@Service("pointsManagerService")
@Transactional
public class PointsManagerServiceImpl implements PointsTxnManagerService {

    private static final Logger LOGGER = LoggerFactory.getLogger(PointsManagerServiceImpl.class);
    // fields/properties of class PointsTxnModel
    private static final String[] DATE_FIELDS = new String[]{"created", "transactionDateTime"};
    @PersistenceContext
    private EntityManager em;
    @Autowired
    private PointsTxnModelRepo pointsTxnModelrepo;
    @Autowired
    private MemberRepo memberRepo;
    @Autowired
    private StoreRepo storeRepo;
    @Autowired
    ApprovalMatrixService approvalMatrixService;

    @Autowired
    IdGeneratorService customIdGeneratorService;

    @Autowired
    private CodePropertiesService codePropertiesService;

    @Autowired
    private MessageSource messageSource;

    @Autowired
    private LookupService lookupService;

    @Autowired
    private ApplicationConfigRepo appConfigRepo;

    @Autowired
    private TransactionAuditService transactionAuditService;

    @Autowired
    private EmployeePurchaseService employeePurchaseService;

    @Autowired
    private PosTransactionService posTransactionService;

    @Autowired
    private StoreService storeService;

    @Autowired
    private PromotionService promotionService;
    @Value("#{'${hibernate.jdbc.batch_size}'}")
    private int batchSize;

    /**
     * @param transactionNo
     * @param newTransactionNo
     * @param amount
     * @param returnedList
     * @param store
     * @param ret
     * @param isRefund         - if refund, no need to check on item. ie. overpayment  but item will be the same
     * @return
     */

    @Override
    public boolean processReturnItems(String transactionNo, String newTransactionNo, Double amount,
                                      List<PosTxItemDto> returnedList, String store, ReturnMessage ret,
                                      boolean isRefund) {


        ret.setType(MessageType.SUCCESS);


        //check if there are returned items  and call is not for refund
        if (!isRefund && (returnedList == null || returnedList.size() == 0 || StringUtils.isBlank(transactionNo))) {

            ret.setMessage(messageSource.getMessage("points_rs_invalid_parameter",
                    new Object[]{"returned items or transaction num"}, null));
            ret.setMessageCode(codePropertiesService.getDetailMessageCodeInvalidParameter());
            ret.setType(MessageType.ERROR);

            return false;
        }


        List<TransactionAudit> audits = transactionAuditService.retrieveAuditsByTransactionNo(transactionNo);
        List<EmployeePurchaseTxnModel> purchaseTxns = employeePurchaseService.findByTransactionNo(transactionNo);

        //check if there are points earned for this transactionno  or has earn a purchase txn for employee
        if ((audits == null || audits.size() == 0) && (purchaseTxns == null || purchaseTxns.size() == 0)) {
            ret.setMessage(messageSource.getMessage("points_rs_no_record_found",
                    new Object[]{"points audit or purchase txn"}, null));
            ret.setMessageCode(codePropertiesService.getDetailMessageCodeNoRecordFound());
            ret.setType(MessageType.ERROR);

            return false;
        }

        PosTransaction posTransaction = posTransactionService.getTransaction(transactionNo);
        if (posTransaction == null) {
            ret.setMessage(messageSource.getMessage("points_rs_no_record_found", new Object[]{"POS Transaction"},
                    null));
            ret.setMessageCode(codePropertiesService.getDetailMessageCodeNoRecordFound());
            ret.setType(MessageType.ERROR);

            return false;
        }

        //retrieve all items from transaction no
        List<PosTxItem> orderItems = posTransaction.getOrderItems();
        List<PosPayment> payments = posTransaction.getPayments();


        // remove all returnedItems from list
        List<PosTxItemDto> itemList = removeReturnedItems(orderItems, returnedList);

        MemberModel memberModel = null;
        Double totalPreviousPoints = 0.0;
        Double totalNewPoints = 0.0;
        int paymentIndex = 0;

        double remainingReturnAmount = amount;

        double remainingTxnPointsDeductions = 0.0;

        //retrieve expiration date if any
        LocalDate expiryDate = null;

        List<PointsTxnModel> points = pointsTxnModelrepo.findByTransactionNo(transactionNo);

        if (points != null && points.size() > 0) {
            PointsTxnModel pointTxn = points.get(0);
            expiryDate = pointTxn.getExpiryDate();
        }


        Promotion promotion = null;

        // retrieve promotion codes  for this transactions
        for (TransactionAudit audit : audits) {


            if (memberModel == null && audit.getMemberModel() != null) {
                memberModel = audit.getMemberModel();
            }

            promotion = audit.getPromotion();
            //run all payments to all promotions - discard card number for now

            totalPreviousPoints += audit.getTransactionPoints().doubleValue();

            remainingTxnPointsDeductions += audit.getTransactionPoints();
            //for (PosPayment payment : payments) {
            if (paymentIndex < payments.size()) {

                PosPayment payment = payments.get(paymentIndex);
                paymentIndex++;
                // call all promotions with date exclude in computation
                PromoRewardDto newReward = new PromoRewardDto();
                newReward.setPoints(0.0);

                double newTransactionAmount = 0.0;
                if (payment.getAmount() >= remainingReturnAmount) {
                    newTransactionAmount = payment.getAmount() - remainingReturnAmount;
                    remainingReturnAmount = 0.0;
                } else {
                    //newTransactionAmount = 0.0 if return amount is greater than amount paid for this payment type
                    remainingReturnAmount = remainingReturnAmount - payment.getAmount();
                }

                //transaction audit not implemented?
                promotionService.computePromotionRewards(memberModel.getAccountId(), newReward, promotion,
                        newTransactionAmount, transactionNo, audit.getStore().getCode(), itemList, null);

                totalNewPoints += newReward.getPoints();

                Double pointDeduction = remainingTxnPointsDeductions - newReward.getPoints();


                //only add a record for adjustment if no points where given from the new parameters and there are
                // still points to be deducted.
                if (pointDeduction > 0 && remainingTxnPointsDeductions > 0) {
                    //add negated value of points deduction with type return and create a new pointtxn record
                    PointsTxnModel pointsTxnModel = new PointsTxnModel();

                    pointsTxnModel.setTransactionDateTime(new Date());

                    pointsTxnModel.setTransactionNo(transactionNo);
                    if (audit.getStore() != null) {
                        pointsTxnModel.setStoreCode(audit.getStore().getCode());

                    }
                    pointsTxnModel.setTransactionAmount(-amount);
                    if (isRefund) {
                        pointsTxnModel.setTransactionType(PointTxnType.REFUND);

                    } else {
                        pointsTxnModel.setTransactionType(PointTxnType.RETURN);

                    }
                    pointsTxnModel.setTransactionPoints(-pointDeduction);
                    pointsTxnModel.setMemberModel(audit.getMemberModel());
                    //if (audit.getMemberModel() != null) {
                    //    pointsTxnModel.setMemberAccountId(audit.getMemberModel().getAccountId());

                    // }

                    /*pointsTxnModel.setExpiryDate(expiryDate);*/
                    pointsTxnModel.setReturnTransactionNo(newTransactionNo);

                    this.processPoints(pointsTxnModel, ret);
                }

                remainingTxnPointsDeductions = remainingTxnPointsDeductions - pointDeduction;

                if (remainingReturnAmount <= 0) {
                    break;
                }


            }
        }


        Double totalPointDeduction = totalPreviousPoints - totalNewPoints;


        if (totalPointDeduction > 0) {
            ret.setEarnedPoints(-totalPointDeduction);

        } else {
            ret.setEarnedPoints(0.0);
        }






        //DEDUCT EMPLOYEE  RETURNED AMOUNT FROM EMPLOYEE PURCHASE

        Double totalPurchaseDeduction = 0.0;


        EmployeePurchaseTxnDto employeePurchaseTxnDto = employeePurchaseService.findOneByTransactionNo(transactionNo);
        if (employeePurchaseTxnDto != null) {
            totalPurchaseDeduction = amount;
            EmployeePurchaseTxnDto dto = new EmployeePurchaseTxnDto();
            dto.setId(null); //create new record
            dto.setAccountId(employeePurchaseTxnDto.getAccountId());
            MemberDto employeeDto = employeePurchaseTxnDto.getMemberModel();
            dto.setMemberModel(employeeDto);
            dto.setTransactionDateTime(new Date());
            dto.setTransactionNo(transactionNo);

            Store storeModel = storeService.getStoreByCode(store);
            if (storeModel != null) {
                dto.setStoreModel(new StoreDto(storeModel));
            } else {
                LOGGER.warn("Store code [" + store + "] does not exist.");
            }

            dto.setTransactionAmount(-totalPurchaseDeduction);
            if (isRefund) {
                dto.setTransactionType(VoucherTransactionType.REFUND);

            } else {
                dto.setTransactionType(VoucherTransactionType.RETURN);

            }
            dto.setStatus(TxnStatus.ACTIVE);
            employeePurchaseService.savePurchaseTxn(dto);

        }


        String memberId = null;
        if (memberModel != null) {
            memberId = memberModel.getAccountId();
        } else {

            List<PointsTxnModel>  pointsTxnModels =   pointsTxnModelrepo.findByTransactionNo(transactionNo);

            if (pointsTxnModels != null) {
                PointsTxnModel point =  pointsTxnModels.get(0);
                memberModel = point.getMemberModel();
            }
        }



        //create crm points of refund type for txn when no promotion is defined - no txn audit logs
        if (promotion == null) {
            //add negated points value in crm_points table
            PointsTxnModel pointsTxnModel = new PointsTxnModel();
            pointsTxnModel.setTransactionDateTime(new Date());

            pointsTxnModel.setTransactionNo(transactionNo);
            pointsTxnModel.setStoreCode(store);

            pointsTxnModel.setTransactionAmount(-amount);
            if (isRefund) {
                pointsTxnModel.setTransactionType(PointTxnType.REFUND);

            } else {
                pointsTxnModel.setTransactionType(PointTxnType.RETURN);

            }
            pointsTxnModel.setTransactionPoints(0.0);
            pointsTxnModel.setMemberModel(memberModel);

            pointsTxnModel.setReturnTransactionNo(newTransactionNo);

            this.processPoints(pointsTxnModel, ret);



        } else {

            //audit this transaction only if it has a promotion associated to it

            TransactionAudit audit = new TransactionAudit();

            audit.setStore(storeService.getStoreByCode(store));
            audit.setTransactionNo(newTransactionNo);

            audit.setTransactionPoints(-totalPointDeduction.longValue());
            audit.setMemberModel(memberModel);
            audit.setPromotion(promotion);
            audit.setId(customIdGeneratorService.generateId(store));
            audit.setCreated(new DateTime());


            transactionAuditService.saveTransactionAudit(audit);
        }
        ret.setMessage(messageSource.getMessage("points_rs_success_return_message", new Object[]{totalPointDeduction,
                totalPurchaseDeduction, memberId}, null));


        ret.setType(MessageType.SUCCESS);


        return true;
    }


    public List<PosTxItemDto> removeReturnedItems(List<PosTxItem> orderItems, List<PosTxItemDto> returnedList) {

        List<PosTxItemDto> itemList = new ArrayList<PosTxItemDto>();
        for (PosTxItem item : orderItems) {
            boolean remove = false;

            //this will be null if transaction is for refund
            if (returnedList != null) {
                for (PosTxItemDto retItem : returnedList) {
                    if (item.getProductId().equalsIgnoreCase(retItem.getProductId())) {
                        remove = true;
                        break;
                    }
                }
            }


            if (!remove) {
                PosTxItemDto itemDto = new PosTxItemDto();
                itemDto.setProductId(item.getProductId());
                itemDto.setQuantity(item.getQuantity());
                itemList.add(itemDto);
            }

        }

        return itemList;

    }


    @Override
    public boolean processPoints(PointsTxnModel pointsTxnModel, ReturnMessage returnMessage) {
        if (null == pointsTxnModel) {
            LOGGER.error("pointsTxnModel object is null");
            return false;
        }

        MemberModel member = pointsTxnModel.getMemberModel();
        MemberModel contributer = pointsTxnModel.getTransactionContributer();

        //check if member is supplementary
        if (StringUtils.isNotBlank(member.getParentAccountId())) {
            member = memberRepo.findByAccountId(member.getParentAccountId());
        }

        if (null == member) {
            LOGGER.error("No member defined in pointmodel");
            return false;
        }

        if (member.getAccountStatus() != MemberStatus.ACTIVE) {
            LOGGER.error("Member status is not active");
            return false;
        }

        double pointsContributed = contributer != null && contributer.getTotalPoints() != null ? contributer
                .getTotalPoints() : 0D;
        double points = member.getTotalPoints() != null ? member.getTotalPoints() : 0D;
        double transactionPoints = pointsTxnModel.getTransactionPoints() != null ? pointsTxnModel.getTransactionPoints
                () : 0D;

        if (PointTxnType.EARN.equals(pointsTxnModel.getTransactionType())) {

            points += transactionPoints;
            pointsContributed += transactionPoints;

        } else if (PointTxnType.REDEEM.equals(pointsTxnModel.getTransactionType())
                || PointTxnType.VOID.equals(pointsTxnModel.getTransactionType())
                || PointTxnType.RETURN.equals(pointsTxnModel.getTransactionType())) {
            if (isNegative(transactionPoints)) {
                points += transactionPoints;

            } else {
                points -= transactionPoints;
            }


        } else if (PointTxnType.ADJUST.equals(pointsTxnModel.getTransactionType())
                && (pointsTxnModel.getStatus() == null || pointsTxnModel.getStatus() == TxnStatus.ACTIVE)) {
            points += transactionPoints;
        }


        if (null != returnMessage) {
            returnMessage.setTotalPoints((long) points);
            returnMessage.setEarnedPoints(transactionPoints);
        }
        member.setTotalPoints(points);
        // memberRepo.save(member);
        //INFO remove saving of member in proxy call to avoid updating updated date time in store to allow pos-integ
        // to move record from dbee to store
        //     totalPoints should already be updated by  scheduler in the DBEE wherein the member record in the
        // primary gets downloaded to the store


        //TODO saving of member from a proxy call should be avoided since member model does not get uploaded back to
        // primary db
        //     use a web service call instead in primary to save  updated member record. refer to RFS approach for this
//        if(contributer != null && contributer != member) {
//        	contributer.setTotalPoints(pointsContributed);
//        	memberRepo.save(contributer);
//        }

        if (StringUtils.isBlank(pointsTxnModel.getId())) {
            pointsTxnModel.setId(customIdGeneratorService.generateId(pointsTxnModel.getStoreCode()));
        }

        pointsTxnModel.setCreated(new DateTime()); //TODO temp fix

        if (transactionPoints != 0) {
            switch (pointsTxnModel.getTransactionType()) {
                case ADJUST: // only consider positive adjustments
                    if (isNegative(transactionPoints)) {
                        break;
                    }
                case EARN:
                    // expiry date is 2 years, 1 month from transaction date
                    Integer validPeriod = getDefaultValidPeriod();
                    LocalDate expiryDate = null;
                    if (pointsTxnModel.getTransactionDateTime() != null) {
                        expiryDate = LocalDate.fromDateFields(pointsTxnModel.getTransactionDateTime())
                                .plusMonths(validPeriod);
                    } else {
                        expiryDate = LocalDate.fromDateFields(pointsTxnModel.getCreated().toDate())
                                .plusMonths(validPeriod);
                    }

                    if (expiryDate.getDayOfMonth() > 1) {
                        expiryDate = expiryDate.dayOfMonth().withMinimumValue().plusMonths(1);
                    }
                    pointsTxnModel.setExpiryDate(expiryDate);
                    break;
                default:
                    pointsTxnModel.setExpiryDate(null);
            }
        }

        pointsTxnModelrepo.save(pointsTxnModel);
        return true;
    }

    @Override
    public Double retrieveTotalPoints(MemberModel member) {
        return member.getTotalPoints();
    }

    @Override
    public Double retrieveTotalPoints(String id) {

        MemberModel member = memberRepo.findByAccountId(id);

        if (member != null) {
            return member.getTotalPoints();

        }

        return 0D;
    }

    @Override
    public List<PointsTxnModel> retrievePointsByTransactionNo(String transactionNo) {

        return pointsTxnModelrepo.findByTransactionNo(transactionNo);
    }

    @Override
    public List<PointsTxnModel> retrievePoints(MemberModel member) {
        List<PointsTxnModel> theList = new ArrayList<PointsTxnModel>();
        CollectionUtils.addIgnoreNull(theList,
                pointsTxnModelrepo.findAll(QPointsTxnModel.pointsTxnModel.memberModel.accountId.eq(member
                        .getAccountId()).and(
                        QPointsTxnModel.pointsTxnModel.status.in(TxnStatus.ACTIVE).or(
                                QPointsTxnModel.pointsTxnModel.status.isNull()))).iterator());
        return theList;
    }

    @Override
    public List<PointsTxnModel> retrievePointsByAcctId(String accountId) {
        MemberModel member = new MemberModel();
        member.setAccountId(accountId);
        return pointsTxnModelrepo.findByMemberModel(member);
    }

    @Override
    public List<PointsTxnModel> retrievePoints(String id, int firstResult, int maxResult) {
        return pointsTxnModelrepo.findAll(QPointsTxnModel.pointsTxnModel.memberModel.accountId.eq(id).and(
                QPointsTxnModel.pointsTxnModel.status.eq(TxnStatus.ACTIVE).or(
                        QPointsTxnModel.pointsTxnModel.status.isNull())),
                new PageRequest(firstResult / maxResult, maxResult,
                        new Sort(new Sort.Order(Sort.Direction.DESC, "created")))).getContent();
    }

    @Override
    public Double synchronizePointsTxn(MemberModel inMember) {
        if (StringUtils.isBlank(inMember.getParentAccountId())) {
            inMember.setTotalPoints(pointsTxnModelrepo.getTotalPoints(inMember.getAccountId()));
        } else {
            inMember.setTotalPoints(pointsTxnModelrepo.getTotalPointsSupplement(inMember.getAccountId()));
        }
        MemberModel theMember = memberRepo.saveAndFlush(inMember);

        return theMember.getTotalPoints();
    }

    @Override
    public List<Map> retrieveValidPaymentTypes(String paymentType, String amount, String cardNumber,
                                               boolean checkFilters, ReturnMessage ret) {


        List<Map> list = new ArrayList();
        List<String> paymentTypes = Arrays.asList(StringUtils.split(paymentType, ":"));
        List<String> amounts = Arrays.asList(StringUtils.split(amount, ":"));

        List<String> cardNumbers = new ArrayList();

        if (cardNumber != null) {
            cardNumbers = Arrays.asList(StringUtils.split(cardNumber, ":"));
        }

        if (paymentTypes.size() == 0) {
            if (ret != null) {
                ret.setType(MessageType.ERROR);
                ret.setMessage(messageSource.getMessage("points_rs_error_no_paymentType", null, null));
                ret.setMessageCode(codePropertiesService.getDetailMessageCodeNoPaymentType());
            }
        } else {


            int cardIndex = 0;
            List<LookupDetail> approvedCardTypes = lookupService.getActiveDetailsByHeaderCode(codePropertiesService.getHeaderPaymentTypeWildcard());


            for (int i = 0; i < paymentTypes.size(); ++i) {

                Map map = new HashMap();

                String pType = paymentTypes.get(i);

                //if checkFilters is false type then return all payments
                if (!checkFilters) {
                    map.put(PAYMENT_TYPE_KEY, pType);
                    map.put(AMOUNT_KEY, new Double(amounts.get(i)));

                    //TODO need to check this in case a card type is used that is not part of the list.
                    //if payment type is card, retrieve corresponding cardnumber which is not always equivalent to
                    // the no. of payment types; check against list of card types
                    for (LookupDetail cardType : approvedCardTypes) {
                        if (cardType.getCode().equalsIgnoreCase(pType)) {
                            if (cardIndex < cardNumbers.size()) {
                                map.put(CARD_NUMBER_KEY, cardNumbers.get(cardIndex));
                                cardIndex++;
                            }
                            break;

                        }
                    }


                    list.add(map);
                } else {
                    List<LookupDetail> approvedPaymentTypes = lookupService.getActiveDetailsByHeaderCode(codePropertiesService.getHeaderEmployeeApprovedPaymentMedia());


                    boolean validPaymentType = false;


                    for (LookupDetail lookupDetail : approvedPaymentTypes) {
                        //compare against the description which is mapped to the lookupdetail code for payment types
                        if (pType.equalsIgnoreCase(lookupDetail.getDescription())) {
                            validPaymentType = true;
                            break;
                        }
                    }

                    List<LookupDetail> approvedCardFilters = lookupService.getActiveDetailsByHeaderCode(codePropertiesService.getHeaderEmployeeApprovedCardFilter());
                    //for card type, if  no cardfilter is defined don't bother checking for card filters
                    if (approvedCardFilters.size() > 0) {
                        //compare against list of valid card types
                        for (LookupDetail cardType : approvedCardTypes) {
                            if (cardType.getCode().equalsIgnoreCase(pType)) {
                                for (LookupDetail lookupDetail : approvedCardFilters) {
                                    //compare if cardNumber matches lookupdetail.getDescription
                                    if (StringUtil.matches(cardNumber, lookupDetail.getDescription())) {
                                        validPaymentType = true;
                                        break;
                                    }

                                }
                            }
                        }
                    }




                    if (validPaymentType) {
                        map.put(PAYMENT_TYPE_KEY, pType);
                        map.put(AMOUNT_KEY, new Double(amounts.get(i)));

                        //if payment type is card, retrieve corresponding cardnumber which is not always equivalent to
                        // the no. of payment types
                        if (codePropertiesService.getDetailPaymentTypeEft().equalsIgnoreCase(pType)) {
                            if (cardIndex < cardNumbers.size()) {
                                map.put(CARD_NUMBER_KEY, cardNumbers.get(cardIndex));
                                cardIndex++;
                            }

                        }
                        list.add(map);
                    }


                }


            }


        }


        return list;
    }


    @Override
    public List<PointsTxnModel> retrievePoints(MemberModel member, Date fromDate,
                                               Date toDate) {
        return this.retrievePoints(member.getAccountId(), fromDate, toDate);
    }

    @Override
    public List<PointsTxnModel> retrievePoints(String accountId, Date fromDate,
                                               Date toDate) {
        return pointsTxnModelrepo.findPointsTxnByDateRange(accountId, fromDate, toDate);
    }

    @Override
    public PointsTxnModel savePointsTxnModel(PointsTxnModel model) {
        if (StringUtils.isBlank(model.getId())) {
            model.setId(customIdGeneratorService.generateId(model.getStoreCode()));
        }
        return pointsTxnModelrepo.save(model);
    }


    @Override
    public long countPointsTransactions(String accountId) {
        return pointsTxnModelrepo.count(QPointsTxnModel.pointsTxnModel.memberModel.accountId.eq(accountId));
    }

    @Transactional
    public JRProcessor createPointsJrProcessor(MemberModel member, Date dateFrom, Date dateTo, InputStream logo) {
        SimpleDateFormat formatter = new SimpleDateFormat("MMMM d, yyyy");
        List<PointsTxnModel> pointsList = retrievePoints(member, dateFrom, dateTo);
        Map<String, Object> parameters = new HashMap<String, Object>();
        List<PointsDto> pointsDtos = new ArrayList<PointsDto>();
        if (pointsList != null) {
            for (PointsTxnModel point : pointsList) {
                PointsDto dto = new PointsDto(point);
                if ( StringUtils.isNotBlank( dto.getStoreCode() ) ) {
                	Store store = storeRepo.findByCode(dto.getStoreCode());
                	if ( null != store ) {
                        dto.setStoreName(store.getName());
                	}
                }
                pointsDtos.add(dto);
            }
        }
        parameters.put("SUB_DATA_SOURCE", pointsDtos);
        parameters.put("DATE_FROM", formatter.format(dateFrom));
        parameters.put("DATE_TO", formatter.format(dateTo));
        parameters.put("logo", logo);
        MemberPointsReportDto pointsDto = new MemberPointsReportDto(member);
        if (StringUtils.isNotBlank(pointsDto.getStoreCode())) {
            Store memberStore = storeRepo.findByCode(pointsDto.getStoreCode());
            if (memberStore != null)
                pointsDto.setStoreName(memberStore.getName());
        }
        DefaultJRProcessor jrProcessor =
                new DefaultJRProcessor("reports/points_audit_report.jasper", Arrays.asList(pointsDto));
        jrProcessor.setParameters(parameters);
        jrProcessor.setFileResolver(new ClasspathFileResolver("reports"));
        return jrProcessor;
    }

    public JRProcessor createExpirePointsJrProcessor(LocalDate startMonth, LocalDate endMonth, InputStream logo) {
        Map<String, Object> parameters = new HashMap<String, Object>();
        String datePattern = "MMM yyyy";

        LocalDate start = startMonth.dayOfMonth().withMinimumValue();
        LocalDate end = endMonth.dayOfMonth().withMinimumValue();

        int months = Months.monthsBetween(start, end).getMonths();

        List<PointsExpireReportDto> pointsDtos = Lists.newArrayList();

        for (int i = 0; i <= months; i++) {
            LocalDate month = start.plusMonths(i);
            PointsExpireReportDto dto = pointsTxnModelrepo.findPastExpiringTransactions(month);
            dto.setMonth(month.toString(datePattern));
            if (dto.getTotalPointsExpired() == null) {
                dto.setTotalPointsExpired(0D);
                dto.setTotalAmount(0D);
            }
            pointsDtos.add(dto);
        }

        parameters.put("SUB_DATA_SOURCE", pointsDtos);
        parameters.put("logo", logo);
        parameters.put("REPORT_TYPE", messageSource.getMessage("txn_report_label_points_expire_report_type",
                null, LocaleContextHolder.getLocale()));
        parameters.put("DATE_FROM", start.toString(datePattern));
        parameters.put("DATE_TO", end.toString(datePattern));
        parameters.put("MONTH", messageSource.getMessage("txn_report_label_month", null,
                LocaleContextHolder.getLocale()));
        parameters.put("POINTS", messageSource.getMessage("txn_report_label_expired_points", null,
                LocaleContextHolder.getLocale()));
        parameters.put("AMOUNT", messageSource.getMessage("txn_report_label_converted_amount", null,
                LocaleContextHolder.getLocale()));


        DefaultJRProcessor jrProcessor =
                new DefaultJRProcessor("reports/expire_points_report.jasper");
        jrProcessor.setParameters(parameters);
        jrProcessor.setFileResolver(new ClasspathFileResolver("reports"));
        return jrProcessor;
    }

    public JRProcessor createFutureExpirePointsJrProcessor(LocalDate startMonth, LocalDate endMonth, InputStream logo) {
        Map<String, Object> parameters = new HashMap<String, Object>();
        String datePattern = "MMM yyyy";

        LocalDate start = startMonth.dayOfMonth().withMinimumValue();
        LocalDate end = endMonth.dayOfMonth().withMinimumValue();

        int months = Months.monthsBetween(start, end).getMonths();

        LocalDate lastExpireDate = null;
        Date expireDate = pointsTxnModelrepo.findDateOfLastExpire();
        if (expireDate != null) {
            lastExpireDate = LocalDate.fromDateFields(expireDate).dayOfMonth().withMinimumValue();
        } else {
            lastExpireDate = start;
        }

        int monthsFromLastExpire = Months.monthsBetween(lastExpireDate, start).getMonths();
        Map<String, PointsTxnModel> lastExpireMap = Maps.newHashMap();
        Double remainingPoints = 0D;

        for (int i = 1; i < monthsFromLastExpire; i++) {
            LocalDate month = lastExpireDate.plusMonths(i);
            Double[] result = futureExpirePoints(lastExpireMap, month);
            remainingPoints = result[2];
        }

        List<PointsExpireReportDto> pointsDtos = Lists.newArrayList();

        for (int i = 0; i <= months; i++) {
            LocalDate month = start.plusMonths(i);
            Double[] result = futureExpirePoints(lastExpireMap, month);

            Double monthlyExpire = result[0];
            Double monthlyPoints = result[1];
            Double monthlyExcess = result[2];
            Integer memberCount = result[3].intValue();

            PointsExpireReportDto dto = new PointsExpireReportDto();
            dto.setMonth(month.toString(datePattern));
            dto.setCardsWithExpiry(memberCount);
            dto.setOrigLoyaltyIssued(monthlyExpire);
            dto.setCurrLoyaltyUsed(remainingPoints + monthlyPoints);
            Double totalPointsExpired = monthlyExpire - dto.getCurrLoyaltyUsed();
            if (totalPointsExpired == null || totalPointsExpired < 0) {
                totalPointsExpired = 0D;
            }
            dto.setTotalPointsExpired(totalPointsExpired);
            remainingPoints = monthlyExcess;
            pointsDtos.add(dto);
        }

        parameters.put("SUB_DATA_SOURCE", pointsDtos);
        parameters.put("logo", logo);
        parameters.put("REPORT_TYPE", messageSource.getMessage("txn_report_label_points_expire_future_report_type",
                null, LocaleContextHolder.getLocale()));
        parameters.put("DATE_FROM", start.toString(datePattern));
        parameters.put("DATE_TO", end.toString(datePattern));
        parameters.put("MONTH", messageSource.getMessage("txn_report_label_month", null,
                LocaleContextHolder.getLocale()));
        parameters.put("ORIG_LOYALTY_ISSUED", messageSource.getMessage("txn_report_label_orig_loyalty_issued", null,
                LocaleContextHolder.getLocale()));
        parameters.put("CURR_LOYALTY_USED", messageSource.getMessage("txn_report_label_curr_loyalty_used", null,
                LocaleContextHolder.getLocale()));
        parameters.put("EXPIRY_VALUE", messageSource.getMessage("txn_report_label_potential_expiry_value", null,
                LocaleContextHolder.getLocale()));
        parameters.put("CARDS_WITH_EXPIRY", messageSource.getMessage("txn_report_label_cards_with_expiry", null,
                LocaleContextHolder.getLocale()));
        parameters.put("AVE_EXPIRY_VALUE", messageSource.getMessage("txn_report_label_ave_expiry_value", null,
                LocaleContextHolder.getLocale()));
        parameters.put("POTENTIAL_BREAKAGE", messageSource.getMessage("txn_report_label_potential_breakage", null,
                LocaleContextHolder.getLocale()));
        parameters.put("AVE_LOYALTY_BALANCE", messageSource.getMessage("txn_report_label_ave_loyalty_balance", null,
                LocaleContextHolder.getLocale()));

        DefaultJRProcessor jrProcessor =
                new DefaultJRProcessor("reports/future_expire_points_report.jasper");
        jrProcessor.setParameters(parameters);
        jrProcessor.setFileResolver(new ClasspathFileResolver("reports"));
        return jrProcessor;
    }

    private Double[] futureExpirePoints(Map<String, PointsTxnModel> lastExpireMap, LocalDate month) {
        Double monthlyExcess = 0D;
        Double monthlyExpire = 0D;
        Double monthlyPoints = 0D;
        Double memberCount = 0D;
        for (MemberModel member : memberRepo.findMembersWithExpiringTransactions(month)) {
            String accountId = member.getAccountId();
            DateTime lastTransactionDate = null;
            PointsTxnModel lastExpire = (lastExpireMap.containsKey(accountId) ? lastExpireMap.get(accountId) :
                    pointsTxnModelrepo.findLastExpireTransaction(accountId));
            Double pointsToExpire = 0D;
            Double pointsToDeduct = 0D;

            if (lastExpire != null) {
                pointsToDeduct += lastExpire.getExpireExcess() != null ? lastExpire.getExpireExcess() : 0D;
                lastTransactionDate = lastExpire.getLastRedeemTimestamp();
            }

            if (lastTransactionDate == null) {
                lastTransactionDate = new DateTime(0L);
            }

            PointsTxnModel expiryPoints = new PointsTxnModel();
            expiryPoints.setTransactionDateTime(month.toDate());
            expiryPoints.setTransactionType(PointTxnType.EXPIRE);
            expiryPoints.setMemberModel(member);
            //expiryPoints.setMemberAccountId(member.getAccountId());
            expiryPoints.setStatus(TxnStatus.ACTIVE);

            Object[] tuple = pointsTxnModelrepo.sumDeductablePoints(accountId, lastTransactionDate, month);
            pointsToDeduct += (Double) (tuple[0] != null ? tuple[0] : 0D);
            expiryPoints.setLastRedeemTimestamp(tuple[1] != null ? (DateTime) tuple[1] : lastTransactionDate);

            for (PointsTxnModel points : pointsTxnModelrepo.sumExpiringPoints(accountId, month)) {
                pointsToExpire += points.getTransactionPoints();
            }

            Double overflowPoints = pointsToExpire + pointsToDeduct;
            if (overflowPoints >= 0) {
                expiryPoints.setExpireExcess(0D);
                expiryPoints.setTransactionPoints(-overflowPoints);
                Double currencyAmount = overflowPoints * getCurrencyAmountPerPoint();
                expiryPoints.setTransactionAmount(currencyAmount);
            } else {
                expiryPoints.setExpireExcess(overflowPoints);
                expiryPoints.setTransactionPoints(0D);
                monthlyExcess += overflowPoints;
            }
            monthlyExpire += pointsToExpire;
            monthlyPoints += pointsToDeduct;
            memberCount++;

            lastExpireMap.put(accountId, expiryPoints);
        }

//    	System.out.println(month.toString("MMM yyyy"));
//    	System.out.println("Monthly Expire: " + monthlyExpire);
//    	System.out.println("Monthly Points: " + monthlyPoints);
//    	System.out.println("Monthly Excess: " + monthlyExcess);
//    	System.out.println("Monthly Count: " + memberCount);
        return new Double[]{monthlyExpire, monthlyPoints, monthlyExcess, memberCount};
    }

    @Override
    public List<PointsTxnModel> retrievePoints(String transactionNo, PointTxnType inTxnType) {
        return pointsTxnModelrepo.findByTransactionNoAndTransactionType(transactionNo, inTxnType);
    }


    @Override
    public ServiceDto retrievePointsByTxnType(PointTxnType inTxnType) {

        ServiceDto theSvcResp = new ServiceDto();

        theSvcResp.setReturnObj(
                generatePointDtos(pointsTxnModelrepo.findByTransactionType(inTxnType)));
        theSvcResp.setReturnCode(MessageType.SUCCESS);

        return theSvcResp;
    }

    @Override
    public ServiceDto retrieveAdjustPointsForApprovalWithAuth() {

        ServiceDto theSvcResp = new ServiceDto();

        Long theHighestAmt = approvalMatrixService.findHighestApprovalPointsOfCurrentUser();

        if (theHighestAmt == null) {
            theSvcResp.setReturnCode(SvcReturnType.NO_RIGHTS);
            return theSvcResp;
        }

        theSvcResp.setReturnObj(generatePointDtos(pointsTxnModelrepo.
                findAdjustPointsByTxnTypeAndHighestPoint(TxnStatus.FORAPPROVAL, theHighestAmt)));
        theSvcResp.setReturnCode(SvcReturnType.SUCCESS);

        return theSvcResp;
    }

    @Override
    public void deleteAdjustPoints(String inId) {
        pointsTxnModelrepo.delete(inId);
    }


    @Override
    public PointsTxnModel retrievePointsById(String inId) {
        return pointsTxnModelrepo.findOne(inId);
    }

    @Override
    public PointsDto retrievePointsDtoById(String inId) {
        return new PointsDto(retrievePointsById(inId));
    }

    @Override
    public void updatePointsConfig(Double pointValue, List<LookupDetail> memberTypes) {
        ApplicationConfig pointsConfig = appConfigRepo.findByKey(AppKey.POINTS_VALUE);
        if (pointsConfig == null) {
            pointsConfig = new ApplicationConfig();
            pointsConfig.setKey(AppKey.POINTS_VALUE);
        }
        pointsConfig.setValue(pointValue != null ? pointValue.toString() : "0");

        ApplicationConfig suspendedConfig = appConfigRepo.findByKey(AppKey.SUSPENDED_FROM_POINTS);
        if (suspendedConfig == null) {
            suspendedConfig = new ApplicationConfig();
            suspendedConfig.setKey(AppKey.SUSPENDED_FROM_POINTS);
        }
        StringBuilder builder = new StringBuilder();
        if (!memberTypes.isEmpty()) {
            builder.append(memberTypes.get(0).getCode());
            for (int i = 1; i < memberTypes.size(); i++) {
                builder.append(",").append(memberTypes.get(i).getCode());
            }
        }
        suspendedConfig.setValue(builder.toString());

//        QPointsConfig qPointsConfig = QPointsConfig.pointsConfig;
//    	PointsConfig config = new JPAQuery(em).from(qPointsConfig).limit(1).singleResult(qPointsConfig);
//        if(config == null) {
//            config = new PointsConfig();
//        }
//    	config.setPointValue(pointValue);
//        Set<SuspendedFromPoints> suspendedTypes = Sets.newHashSet();
//        for(LookupDetail memberType : memberTypes) {
//            suspendedTypes.add(new SuspendedFromPoints(config, memberType));
//        }
//    	config.setSuspendedTypes(suspendedTypes);
//    	return poinstConfigRepo.saveAndFlush(config);
    }

    @Override
    public PointsConfigDto getPointsConfig() {
        PointsConfigDto dto = new PointsConfigDto();
        dto.setSuspendedTypes(getSuspendedMemberTypes());
        dto.setPointValue(getCurrencyAmountPerPoint());
//        QPointsConfig qPointsConfig = QPointsConfig.pointsConfig;
//        PointsConfig pointsConfig = new JPAQuery(em).from(qPointsConfig).limit(1).singleResult(qPointsConfig);
//        if (pointsConfig != null) {
//            dto.setId(pointsConfig.getId());
//            dto.setPointValue(pointsConfig.getPointValue());
//            dto.setTransactionCount(pointsConfig.getTransactionCount());
//            List<LookupDetail> suspendedTypes = Lists.newArrayList();
//            QSuspendedFromPoints qSuspendedFromPoints = QSuspendedFromPoints.suspendedFromPoints;
//            for (SuspendedFromPoints suspendedFromPoints : suspendedFromPointsRepo
//                .findAll(qSuspendedFromPoints.pointsConfig.id.eq(dto.getId()))) {
//                suspendedTypes.add(suspendedFromPoints.getSuspendedType());
//            }
//            dto.setSuspendedTypes(suspendedTypes);
//        }
        return dto;
    }

    public Integer getTransactionCount() {
        return getPointsConfig().getTransactionCount();
    }


    private List<PointsDto> generatePointDtos(List<PointsTxnModel> inPoints) {

        List<PointsDto> thePoints = new ArrayList<PointsDto>();
        for (PointsTxnModel thePoint : inPoints) {
            thePoints.add(new PointsDto(thePoint));
        }

        return thePoints;
    }

    private boolean isNegative(Double num) {
        return num < 0L;

    }

    @Override
    public Double getCurrencyAmountPerPoint() {
        try {
            return Double.parseDouble(appConfigRepo.findByKey(AppKey.POINTS_VALUE).getValue());
        } catch (Exception e) {
            return 0D;
        }
//		return poinstConfigRepo.findAll().get(0).getPointValue();
    }

    @Override
    public List<LookupDetail> getSuspendedMemberTypes() {
        List<LookupDetail> memberTypes = Lists.newArrayList();
        ApplicationConfig suspendedFromPoints = appConfigRepo.findByKey(AppKey.SUSPENDED_FROM_POINTS);
        if (suspendedFromPoints != null && StringUtils.isNotBlank(suspendedFromPoints.getValue())) {
            String[] types = suspendedFromPoints.getValue().split(",");
            for (String typeCode : types) {
                if (StringUtils.isNotBlank(typeCode)) {
                    memberTypes.add(lookupService.getDetailByCode(typeCode));
                }
            }
        }
//        QPointsConfig qPointsConfig = QPointsConfig.pointsConfig;
//        Long pointsConfigId = new JPAQuery(em).from(qPointsConfig).limit(1).singleResult(qPointsConfig.id);
//        if (pointsConfigId != null) {
//            QSuspendedFromPoints qSuspendedFromPoints = QSuspendedFromPoints.suspendedFromPoints;
//            for (SuspendedFromPoints suspendedFromPoints : suspendedFromPointsRepo
//                .findAll(qSuspendedFromPoints.pointsConfig.id.eq(pointsConfigId))) {
//                memberTypes.add(suspendedFromPoints.getSuspendedType());
//            }
//        }
        return memberTypes;
    }

    @Override
    public PointsResultList searchPoints(PointsSearchDto theSearchDto) {
        if (null != theSearchDto && StringUtils.isNotBlank(theSearchDto.getAccountId())) {
            theSearchDto.setTxnContributer(memberRepo.findByAccountId(theSearchDto.getAccountId()));
        }

        final PagingParam pagination = theSearchDto.getPagination();
        // set manually the date field to used PagingParam.DataType
        // doing this, it will not treat as String so the querydsl executor
        // will not use the lower(created|transactionDateTime) in order by clause
        // that cause the db to sort the date fields as String.
        // related ticket: https://projects.exist.com/issues/show/94458
        // TODO: proper setup in pointList.jps ajaxDatatable config
        //      - need to pass from page the data type of per field that required for sorting
        //        otherwise it will sort as string.
        for (String key : pagination.getOrder().keySet()) {
            if (ArrayUtils.contains(DATE_FIELDS, key)) {
                pagination.getDataTypes().put(key, DataType.DATE);
            }
        }
        Pageable pageable = SpringDataPagingUtil.INSTANCE.createPageable(pagination);

        BooleanExpression filter = theSearchDto.createSearchExpression();
        Page<PointsTxnModel> page = filter != null ? pointsTxnModelrepo.findAll(filter,
                pageable) : pointsTxnModelrepo.findAll(pageable);
        return new PointsResultList(convertPointsToDto(page.getContent()), page.getTotalElements(),
                page.hasPreviousPage(), page.hasNextPage());
    }

    @Override
    public boolean savePointsDto(PointsDto inDto) {
        PointsTxnModel points = null;
        if (StringUtils.isNotBlank(inDto.getId())) {
            points = pointsTxnModelrepo.findOne(inDto.getId());
        }
        PointsTxnModel theModel = inDto.toModel(points);
        theModel.setMemberModel(memberRepo.findByAccountId(inDto.getMemberModel().getAccountId()));
        //theModel.setMemberAccountId(inDto.getMemberModel().getAccountId() );
        if (null == theModel.getCreated()) {
            theModel.setCreated(new DateTime()); //TODO temp fix
        }
        if (StringUtils.isBlank(theModel.getCreateUser())) {
            theModel.setCreateUser(UserUtil.getCurrentUser().getUsername()); //TODO temp fix
        }
        boolean result = processPoints(theModel, null);
        inDto.setId(theModel.getId());
        inDto.setMemberModel(new MemberDto(theModel.getMemberModel()));
        return result;
    }

    @Override
    public List<PointsDto> getTransactionsToday(String accountId) {
        return convertPointsToDto(pointsTxnModelrepo.findTransactionsToday(accountId));
    }

    @Override
    public List<PointsDto> getTransactionsByDate(String accountId, LocalDate date) {
        return convertPointsToDto(pointsTxnModelrepo.findTransactionsByDate(accountId, date));
    }

    @Override
    public PointsTxnModel expireAllPoints(String accountId) {
        return expireAllPoints(memberRepo.findByAccountId(accountId));
    }

    @Override
    public PointsTxnModel expireAllPoints(MemberModel member) {
        if (member != null) {
            PointsTxnModel expirePoints = new PointsTxnModel();
            expirePoints.setId(customIdGeneratorService.generateId("EXPIRE"));
            expirePoints.setTransactionPoints(member.getTotalPoints() != null ? -member.getTotalPoints() : 0D);
            expirePoints.setTransactionType(PointTxnType.EXPIRE);
            expirePoints.setTransactionContributer(member);
            expirePoints.setMemberModel(member);
            //expirePoints.setMemberAccountId(member.getAccountId());
            expirePoints.setCreated(DateTime.now());
            expirePoints.setTransactionDateTime(expirePoints.getCreated().toDate());
            expirePoints.setStatus(TxnStatus.ACTIVE);
            expirePoints.setExpireExcess(0D);
            expirePoints.setLastRedeemTimestamp(expirePoints.getCreated());
            return pointsTxnModelrepo.save(expirePoints);
        }
        return null;
    }

    public void expirePoints() {
        expirePoints(DateTime.now().withTimeAtStartOfDay().toLocalDate());
    }

    public void expirePoints(LocalDate expiryDate) {
        // for each active member
        for (MemberModel member : memberRepo.findMembersWithExpiringTransactions(expiryDate)) {
            expirePoints(member, expiryDate);
        }
    }

    @Override
    public PointsTxnModel expirePoints(MemberModel member, List<PointsTxnModel> pointsToExpireList,
                                       LocalDate expiryDate, boolean ind) {
        String accountId = member.getAccountId();

        PointsTxnModel lastExpire = pointsTxnModelrepo.findLastExpireTransaction(accountId);
        if (ind && lastExpire != null && lastExpire.getTransactionDateTime() != null
                && lastExpire.getTransactionDateTime().compareTo(expiryDate.toDate()) == 0) {
            return null;
        }

        Double pointsToExpire = 0D;
        Double pointsToDeduct = 0D;
        DateTime lastTransactionDate = null;
        if (lastExpire != null) {
            pointsToDeduct += lastExpire.getExpireExcess() != null ? lastExpire.getExpireExcess() : 0D;
            lastTransactionDate = lastExpire.getLastRedeemTimestamp();
        }

        if (lastTransactionDate == null) {
            lastTransactionDate = new DateTime(0L);
        }

        PointsTxnModel expiryPoints = new PointsTxnModel();
        expiryPoints.setId(customIdGeneratorService.generateId("EXPIRE") + System.currentTimeMillis());
        expiryPoints.setTransactionDateTime(expiryDate.toDate());
        expiryPoints.setTransactionType(PointTxnType.EXPIRE);
        expiryPoints.setMemberModel(member);
        //if (member != null) {
        //    expiryPoints.setMemberAccountId(member.getAccountId());

        //}
        expiryPoints.setStatus(TxnStatus.ACTIVE);

        Object[] tuple = pointsTxnModelrepo.sumDeductablePoints(accountId, lastTransactionDate, expiryDate);
        pointsToDeduct += (Double) (tuple[0] != null ? tuple[0] : 0D);
        expiryPoints.setLastRedeemTimestamp(tuple[1] != null ? (DateTime) tuple[1] : lastTransactionDate);

        DateTime now = DateTime.now();
        Double overflowPoints = pointsToDeduct;

//		System.out.println("Date: " + expiryDate.toString() + " \nExpire count: " + pointsToExpireList.size());

        for (PointsTxnModel point : pointsToExpireList) {
            PointsTxnModel expirePoint = new PointsTxnModel();
            expirePoint.setId(customIdGeneratorService.generateId("EXPIRE") + System.currentTimeMillis() + point
                    .getTransactionNo());
            expirePoint.setTransactionNo(point.getTransactionNo());
            expirePoint.setTransactionType(PointTxnType.EXPIRE);
            expirePoint.setTransactionDateTime(expiryDate.toDate());
            expirePoint.setMemberModel(member);
            //if (member != null) {
            //    expirePoint.setMemberAccountId(member.getAccountId());

            // }
            expirePoint.setCreated(now);
            expirePoint.setStatus(TxnStatus.ACTIVE);
            expirePoint.setExpireExcess(0D);
            expirePoint.setLastRedeemTimestamp(expirePoint.getLastRedeemTimestamp());
            Double transactionPoints = point.getTransactionPoints();
            pointsToExpire += transactionPoints;
            overflowPoints += transactionPoints;
            if (overflowPoints > 0) {
                Double points = overflowPoints > transactionPoints ? transactionPoints : overflowPoints;
                expirePoint.setTransactionPoints(-points);
            } else {
                expirePoint.setTransactionPoints(0D);
            }
//			System.out.println("Points: " + expirePoint.getTransactionPoints());
//			System.out.println("Points: " + expirePoint.getId());
            pointsTxnModelrepo.saveAndFlush(expirePoint);
        }


//		Double overflowPoints = pointsToExpire + pointsToDeduct;

//		System.out.println("Points to Expire: " + pointsToExpire);
//		System.out.println("Points to Deduct: " + pointsToDeduct);
//		System.out.println("Expire - Deduct: " + overflowPoints);

        expiryPoints.setTransactionPoints(0D);

        if (overflowPoints >= 0) {
            expiryPoints.setExpireExcess(0D);
            Double currencyAmount = overflowPoints * getCurrencyAmountPerPoint();
            expiryPoints.setTransactionAmount(currencyAmount);
            member.setTotalPoints(member.getTotalPoints() - overflowPoints);
            memberRepo.save(member);
        } else {
            expiryPoints.setExpireExcess(overflowPoints);
        }

        expiryPoints.setCreated(DateTime.now());
        if (overflowPoints != null && overflowPoints != 0 && expiryPoints.getLastRedeemTimestamp() != null) {
            return pointsTxnModelrepo.save(expiryPoints);
        }
        return null;
    }

    public PointsTxnModel expirePoints(MemberModel member, LocalDate expiryDate) {
        String accountId = member.getAccountId();
        List<PointsTxnModel> pointsToExpireList = pointsTxnModelrepo.sumExpiringPoints(accountId, expiryDate);
        return expirePoints(member, pointsToExpireList, expiryDate, true);
    }

    public void setDefaultValidPeriod(Integer validPeriodInMonths) {
        ApplicationConfig validPeriod = appConfigRepo.findByKey(AppKey.VALID_PERIOD);
        if (validPeriod == null) {
            validPeriod = new ApplicationConfig();
            validPeriod.setKey(AppKey.VALID_PERIOD);
        }

        if (validPeriodInMonths != null) {
            validPeriod.setValue(validPeriodInMonths.toString());
        }
        appConfigRepo.save(validPeriod);
    }

    public Integer getDefaultValidPeriod() {
        ApplicationConfig validPeriod = appConfigRepo.findByKey(AppKey.VALID_PERIOD);
        if (validPeriod == null) {
            validPeriod = new ApplicationConfig();
            validPeriod.setKey(AppKey.VALID_PERIOD);
            validPeriod.setValue(AppConfigDefaults.DEFAULT_VALID_PERIOD);
            appConfigRepo.save(validPeriod);
        }

        return Integer.parseInt(validPeriod.getValue());
    }

    private List<PointsDto> convertPointsToDto(Collection<PointsTxnModel> inItems) {
        List<PointsDto> results = Lists.newArrayList();
        for (PointsTxnModel theItem : inItems) {
            //System.out.println("" + theItem.getCreated().toString());
            PointsDto theDto = new PointsDto(theItem);
            if (StringUtils.isNotBlank(theDto.getStoreCode())) {
                Store store = storeRepo.findByCode(theDto.getStoreCode());
                if (store != null) {
                    theDto.setStoreName(store.getName());
                    theDto.setStoreDto(new StoreDto(store));
                }
            }
            if (StringUtils.isNotBlank(theDto.getTransactionMedia())) {
                LookupDetail payment = lookupService.getDetail(theDto.getTransactionMedia());
                if (payment != null) {
                    theDto.setTransactionMediaDesc(payment.getDescription());

                    QPosEftPayment qPosEftPayment = QPosEftPayment.posEftPayment;
                    QPosPayment qPayment = QPosPayment.posPayment;
                    JPQLQuery query = new JPAQuery( em )
                            .from( qPosEftPayment )
                            .leftJoin( qPosEftPayment.posPayment, qPayment )
                            .where( qPayment.posTransaction.id.equalsIgnoreCase( theItem.getTransactionNo() ) );

                    PosEftPayment a;
                    try {
                        a = query.singleResult(qPosEftPayment);
                        theDto.setTxnEftMediaDesc( a.getBankName() );
                        theDto.setCardNo(a.getCardNo());
                    } catch (Exception e) {
                        
                    }
                    
                }

                //if (theDto.getTransactionMedia().equalsIgnoreCase(codePropertiesService.getDetailPaymentTypeEft())) {
                    /*PosPayment posPayment = posPaymentRepo.findOne( QPosPayment.posPayment.posTransaction.id.equalsIgnoreCase( theItem.getTransactionNo() ) );
            		if ( null != posPayment ) {
                		PosEftPayment eftPayment = eftPaymentRepo.findOne( QPosEftPayment.posEftPayment.posPayment.id.equalsIgnoreCase( posPayment.getId() ) );
                		if ( null != eftPayment ) {
                    		theDto.setTxnEftMediaDesc( eftPayment.getApplName() );
                		}
            		}*/

            	//}
            }
            results.add(theDto);
        }

        return results;
    }

    @Override
    public Double retrieveTotal( PointTxnType type, String acctId ) {
    	JPAQuery query = new JPAQuery( em );
    	QPointsTxnModel qModel = QPointsTxnModel.pointsTxnModel;
    	return query
	    	.from( qModel )
	    	.where( qModel.memberModel.accountId.equalsIgnoreCase( acctId ).and( qModel.transactionType.eq( type ) ) )
	    	.singleResult( qModel.transactionPoints.sum() );
    }

    @Override
    public BigDecimal retrieveTotalPurchase( PointTxnType[] types, String acctId ) {
    	JPAQuery query = new JPAQuery( em );
    	QPointsTxnModel qModel = QPointsTxnModel.pointsTxnModel;
    	Double total = query
	    	.from( qModel )
	    	.where( qModel.memberModel.accountId.equalsIgnoreCase( acctId ).and( qModel.transactionType.in( types ) ) )
	    	.singleResult( qModel.transactionAmount.sum() );
    	return new BigDecimal( null != total? total : 0 );
    }

    @Override
    @Transactional
    public List<Long> updateMemberPoints() {
        final Session session = em.unwrap(Session.class);
        String queryString = "FROM MemberModel m WHERE " +
            "EXISTS (select 1 from CrmIdRef crmidref where crmidref.idType = :idType and crmidref.modelId = m.id) " +
            "OR (m.parentAccountId is not null and length(m.parentAccountId) <> 0 and m.accountStatus = :status)";
        Query query = session.createQuery(queryString);
        ScrollableResults results = query.setString("idType", CrmIdRefServiceImpl.IdRefType.POINT_EARNING_MEMBER.toString())
            .setString("status", Status.ACTIVE.toString())
            .setFetchSize(batchSize)
            .scroll(ScrollMode.FORWARD_ONLY);
        List<Long> updatedIds = Lists.newArrayList();
        try {
            while (results.next()) {
                MemberModel member = (MemberModel) results.get(0);
                synchronizePointsTxn(member);
                updatedIds.add( member.getId() );
            }
        } finally {
            results.close();
        }
        return updatedIds;
    }
}
