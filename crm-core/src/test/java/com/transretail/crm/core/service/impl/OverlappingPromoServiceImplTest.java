package com.transretail.crm.core.service.impl;

import static org.junit.Assert.assertEquals;

import java.io.InputStream;
import java.util.List;
import java.util.Scanner;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.TransactionCallback;
import org.springframework.transaction.support.TransactionTemplate;

import com.transretail.crm.core.dto.OverlappingPromoDto;
import com.transretail.crm.core.repo.MemberGroupRepo;
import com.transretail.crm.core.repo.ProductGroupRepo;
import com.transretail.crm.core.repo.PromotionMemberGroupRepo;
import com.transretail.crm.core.repo.PromotionPaymentTypeRepo;
import com.transretail.crm.core.repo.PromotionProductGroupRepo;
import com.transretail.crm.core.repo.PromotionRepo;
import com.transretail.crm.core.repo.PromotionStoreGroupRepo;
import com.transretail.crm.core.repo.StoreGroupRepo;
import com.transretail.crm.core.service.OverlappingPromoService;

/**
 * @author Allan G. Ramirez (agramirez@exist.com)
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = "classpath*:/META-INF/spring/applicationContext*.xml")
public class OverlappingPromoServiceImplTest {
    private static final Logger _LOG = LoggerFactory.getLogger(OverlappingPromoServiceImplTest.class);
    @PersistenceContext
    private EntityManager em;
    @Autowired
    private PlatformTransactionManager transactionManager;
    @Autowired
    private MemberGroupRepo memberGroupRepo;
    @Autowired
    private ProductGroupRepo productGroupRepo;
    @Autowired
    private StoreGroupRepo storeGroupRepo;
    @Autowired
    private PromotionRepo promotionRepo;
    @Autowired
    private PromotionMemberGroupRepo promotionMemberGroupRepo;
    @Autowired
    private PromotionProductGroupRepo promotionProductGroupRepo;
    @Autowired
    private PromotionStoreGroupRepo promotionStoreGroupRepo;
    @Autowired
    private PromotionPaymentTypeRepo promotionPaymentTypeRepo;
    @Autowired
    private OverlappingPromoService overlappingPromoService;

    @Before
    public void onSetup() throws Exception {
        InputStream is = Thread.currentThread().getContextClassLoader().getResourceAsStream("scripts/overlapping_test.sql");
        try {
            final Scanner in = new Scanner(is);
            TransactionTemplate transactionTemplate = new TransactionTemplate(transactionManager);
            transactionTemplate.execute(new TransactionCallback() {
                public Object doInTransaction(TransactionStatus status) {
                    while (in.hasNext()) {
                        String script = in.nextLine();
                        if (StringUtils.isNotBlank(script)) {
                            em.createNativeQuery(script).executeUpdate();
                        }
                    }
                    return null;
                }
            });
        } finally {
            IOUtils.closeQuietly(is);
        }
    }

    @After
    public void tearDown() {
        promotionPaymentTypeRepo.deleteAll();
        promotionStoreGroupRepo.deleteAll();
        promotionProductGroupRepo.deleteAll();
        promotionMemberGroupRepo.deleteAll();
        promotionRepo.deleteAll();
        storeGroupRepo.deleteAll();
        productGroupRepo.deleteAll();
        memberGroupRepo.deleteAll();
    }

    @Test
    public void getOverlappingPromoTest() {
        // TODO: Add comprehensive checking
        List<OverlappingPromoDto> result = overlappingPromoService.getOverlappingPromo(1);
        assertEquals(3, result.size());

    }
}
