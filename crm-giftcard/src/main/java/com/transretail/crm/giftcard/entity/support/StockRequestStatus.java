package com.transretail.crm.giftcard.entity.support;

/**
 *
 */
public enum StockRequestStatus {
    FORAPPROVAL, REJECTED, APPROVED, IN_TRANSIT, TRANSFERRED_IN, TRANSFERRED_OUT, FOR_TRANSFER_OUT, INCOMPLETE
}
