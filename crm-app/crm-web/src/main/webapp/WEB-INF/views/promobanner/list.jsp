<%@ include file="../common/taglibs.jsp"%>

<spring:message code="label_promo_banner" var="promoBannerLabel" />
<div class="page-header page-header2">
	<h1>${promoBannerLabel }</h1>
</div>

<a href="<c:url value="/promobanner/new/" />" class="btn btn-primary pull-right mb20" id="create" data-url="<c:url value="/promobanner/new/" />" data-id="">
	<spring:message code="global_menu_new" arguments="${promoBannerLabel}"/>
</a>

<div id="promoBannerList"
	data-url="<c:url value="/promobanner/search" />"
	data-hdr-name="<spring:message code="label_promo_banner_name" />"
	data-hdr-startdate="<spring:message code="label_promo_banner_startdate" />"
	data-hdr-enddate="<spring:message code="label_promo_banner_enddate" />"
	data-hdr-link="<spring:message code="label_promo_banner_link" />"
	data-hdr-status="<spring:message code="label_promo_banner_status" />"
	data-hdr-type="<spring:message code="label_promo_banner_type" />"></div>

<div id="action_buttons" class="hide">
	<a id="promoUpdate" href="<c:url value="/promobanner/update/" />%ID%" class="btn pull-left tiptip icn-edit" data-url="<c:url value="/promobanner/update/" />" data-id="%ID%"
  		title="<spring:message code="label_promo_banner"/>"><spring:message code="label_promo_banner"/></a>
  	<spring:url value="/promobanner/%ID%" var="delete_form_url" /> 
      <form:form action="${delete_form_url}" id="deleteForm%ID%" method="DELETE" cssClass="form-reset">
          <spring:message code="label_delete" var="label_delete"/>
          <c:set var="delete_confirm_msg">
              <spring:escapeBody javaScriptEscape="true">
                  <spring:message code="entity_delete_confirm" />
              </spring:escapeBody>
          </c:set>
          <input class="btn btn-primary icn-delete tiptip pull-left" type="button" alt="${fn:escapeXml(label_delete)}" data-toggle="tooltip" title="${fn:escapeXml(label_delete)}" value="${label_delete}" onclick="getConfirm('${delete_confirm_msg}',function(result) {if(result) $('#deleteForm%ID%').submit();});" />
      </form:form>
</div>

<div id="form"></div>

<jsp:include page="../common/confirm.jsp" />

<link href="<spring:url value="/css/dataTables.css" />" rel="stylesheet">
<script src="<spring:url value="/js/jquery/jquery.dataTables.js" />" ></script>
<script src="<spring:url value="/js/jquery/jquery.fnReloadAjax.js" />" ></script>
<script src="<spring:url value="/js/jquery/jquery.dataTables.linkLengthMenu.js" />"></script>
<script src="<spring:url value="/js/jquery/jquery.dataTables.customPaging.js" />"></script>
<script src="<spring:url value="/js/jquery/ajax-datatable.js" />" ></script>
<script src="<spring:url value="/js/common/form2js.js" />" ></script>
<script src="<spring:url value="/js/common/json2.js" />" ></script>
<script src="<spring:url value="/js/viewspecific/promobanner/promobanner.js" />" ></script>