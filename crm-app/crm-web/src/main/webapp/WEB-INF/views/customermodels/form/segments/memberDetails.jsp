<%@ include file="../../../common/taglibs.jsp" %>


	<fieldset class="form-horizontal container-fluid pull-left">
	  <legend><spring:message code="label_field_customerdetails" /> </legend>
	
    <div class="control-group">
      <label for="religion" class="control-label"> <spring:message code="label_com_transretail_crm_entity_customermodel_religion" />: </label>
      <div class="controls"><form:select path="customerProfile.religion" items="${religion}" itemLabel="description" itemValue="code" /></div>
    </div>
    <div class="control-group">
      <label for="maritalStatus" class="control-label"> <spring:message code="label_com_transretail_crm_entity_customermodel_maritalstatus" />: </label> 
      <div class="controls"><form:select path="customerProfile.maritalStatus" items="${maritalStatus}" itemLabel="description" itemValue="code" /></div>
    </div>
    <div class="control-group">
      <label for="familySize" class="control-label"> <spring:message code="label_com_transretail_crm_entity_customermodel_familysize" />: </label> 
      <div class="controls"><form:select path="customerProfile.familySize" items="${familySize}" itemLabel="description" itemValue="code" /></div>
    </div>
  
    <hr />

    <div class="control-group">
      <label for="children" class="control-label"> <spring:message code="label_com_transretail_crm_entity_customermodel_children" />: </label> 
      <div class="controls"><form:input path="customerProfile.children" id="childrenCount" cssClass="intInput input-small"/></div>
    </div>
	  <div class="control-group">
	    <label for="nationality" class="control-label"> <spring:message code="label_com_transretail_crm_entity_customermodel_nationality" />: </label>
	    <div class="controls"><form:select path="customerProfile.nationality" items="${nationality}" itemLabel="description" itemValue="code" /></div>
	  </div>
    <div class="control-group">
      <label for="education" class="control-label"> <spring:message code="label_com_transretail_crm_entity_customermodel_education" />: </label> 
      <div class="controls"><form:select path="customerProfile.education" items="${education}" itemLabel="description" itemValue="code" /></div>
    </div>

    <div class="control-group">
      <label for="householdIncome" class="control-label"> <spring:message code="label_com_transretail_crm_entity_customermodel_householdincome" />: </label> 
      <div class="controls"><form:select path="customerProfile.householdIncome" items="${householdIncome}" itemLabel="description" itemValue="code" /></div>
    </div>

    <hr />
    <%-- <div class="control-group">
      <label for="personalIncome" class="control-label"> <spring:message code="label_com_transretail_crm_entity_customermodel_personalincome" />: </label> 
      <div class="controls"><form:select path="customerProfile.personalIncome" items="${personalIncome}" itemLabel="description" itemValue="code" /></div>
    </div>
	  <div class="control-group">
	    <label for="preferredLanguage" class="control-label"><spring:message code="label_com_transretail_crm_entity_customermodel_preferredLanguage" />: </label>
	    <div class="controls"><form:select path="customerProfile.preferredLanguage" items="${preferredLanguage}" itemLabel="description" itemValue="code" /></div>
	  </div>
	  <div class="control-group">
	    <label for="childrenAge" class="control-label"> <spring:message code="label_com_transretail_crm_entity_customermodel_ageofchildren" />: </label> 
	    <div id="childrenAge" class="controls" style="height: auto;"></div>
	  </div> --%>
	</fieldset>


<style> #childrenAge input { display: list-item; width: 90px; margin-top: 10px !important; } </style>