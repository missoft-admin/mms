<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<!-- Get the user local from the page context (it was set by Spring MVC's locale resolver) -->
<c:set var="userLocale">
  <c:set var="plocale">${pageContext.response.locale}</c:set>
  <c:out value="${fn:replace(plocale, '_', '-')}" default="in"/>
</c:set>
<!DOCTYPE html>
<%--<!--[if lt IE 7]>
<html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>
<html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>
<html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!-->
<html class="no-js"> <!--<![endif]-->--%>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
  <spring:message code="application_name" var="app_name" htmlEscape="false"/>
  <title><spring:message code="welcome_h3" arguments="${app_name}"/></title>
  <meta name="description" content="Member Portal">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <!-- for non html5 compliant browsers -->
  <script src="<spring:url value="/js/modernizr-2.6.2.min.js" />"></script>

  <link href="<c:url value="/images/favicon.ico" />" rel="SHORTCUT ICON"/>
  <%--<link href='http://fonts.googleapis.com/css?family=Lato' rel='stylesheet' type='text/css'>--%>
  <link href="<c:url value="/css/jqueryui/no-theme/jquery-ui-1.10.3.custom.min.css" />" rel="stylesheet"/>
  <%--<link href="<c:url value="/css/mega-menu3d.css"/>" rel="stylesheet" media="screen, projection">--%>
  <%--<link href="<c:url value="/css/mega-skin.css"/>" rel="stylesheet" type="text/css">--%>
  <link href="<c:url value="/css/bootstrap/bootstrap.css" />" rel="stylesheet">
  <link href="<c:url value="/css/css/font-awesome.min.css" />" rel="stylesheet">

  <style type="text/css">
      body {
      padding-top: 0px;
      padding-bottom: 40px;
      }
  </style>
  <link href="<c:url value="/css/bootstrap/bootstrap-responsive.css" />" rel="stylesheet">
  <link href="<c:url value="/css/bootstrap/bootstrap.override.css" />" rel="stylesheet">
  <link href="<c:url value="/css/dataTables.css" />" rel="stylesheet">
  <link href="<c:url value="/css/custom.css" />" rel="stylesheet">
  <style>
  @font-face {
    font-family: 'Lato';
    font-style: normal;
    font-weight: 300;
    src: local('Lato Light'), local('Lato-Light'), url(/css/fonts/kcf5uOXucLcbFOydGU24WALUuEpTyoUstqEm5AMlJo4.woff) format('woff');
  }
  @font-face {
    font-family: 'Lato';
    font-style: normal;
    font-weight: 400;
    src: local('Lato Regular'), local('Lato-Regular'), url(/css/fonts/qIIYRU-oROkIk8vfvxw6QvesZW2xOQ-xsNqO47m55DA.woff) format('woff');
  }
  @font-face {
    font-family: 'Lato';
    font-style: normal;
    font-weight: 700;
    src: local('Lato Bold'), local('Lato-Bold'), url(/css/fonts/qdgUG4U09HnJwhYI-uK18wLUuEpTyoUstqEm5AMlJo4.woff) format('woff');
  }
  @font-face {
    font-family: 'Lato';
    font-style: normal;
    font-weight: 900;
    src: local('Lato Black'), local('Lato-Black'), url(/css/fonts/G2uphNnNqGFMHLRsO_72ngLUuEpTyoUstqEm5AMlJo4.woff) format('woff');
  }
  
  .username-topbar-info, .username-topbar {
    color: #777777;
    text-shadow: 0 1px 0 #FFFFFF;
  }
  .username-topbar-info {
    font-size: 12px;
    line-height: 16px;
    padding: 3px 15px 10px 0;
  }
  .username-topbar {

    border-left: 1px solid #CCCCCC;
    margin-left: 20px;
    padding: 10px 20px;
    text-transform: uppercase;
  }
  
  .username-topbar2 {
    text-transform: uppercase;
  }
  li.icon-topbar {
    margin-right: 0px;
    text-align: center;
    width: auto;
    margin-top: -4px;
  }
  ul.dropdown-menu li {
    text-align: left;
  }
  .icon-topbar .dropdown-menu {
      top: 226%;
  }
  .icon-topbar .caret {
      right: 4px;
      position: absolute;
  }
  .navbar .nav .dropdown-toggle .caret {
      margin-top: 4%;
  }
  .navbar .nav li.dropdown > .dropdown-toggle .caret {
      border-bottom-color: #ccc;
      border-top-color: #ccc;
  }
  .language .dropdown-menu {
      top: 100%;
  }
  .navbar .nav > li > a {
      color: #555;
  }
  </style>

  <script src="<c:url value="/js/jquery/jquery-1.10.2.min.js" />"></script>
  <script src="<c:url value="/js/jquery/jqueryui/jquery-ui-1.10.3.custom.min.js" />"></script>
  <script>
    var ctx = "${pageContext.request.contextPath}";
  </script>
</head>
<body>

<div class="navbar navbar-static-top hidden-desktop">
  <div class="navbar-inner">
    <div class="container">
      <div class="navbar-brand navbar-brand-mobile pull-left" style="display: block">
        <a class="" href="<spring:url value="/" />">
          <img width="" src="<c:url value="/images/Logo-3.png" />">
        </a>
      </div>
      <ul class="nav pull-left">
        <li class="username-topbar">
            <sec:authentication property="principal.username"/>
        </li>
      </ul>
      <button class="btn btn-navbar collapsed" data-target=".nav-collapse" data-toggle="collapse" type="button">
      <span class="icon-bar"></span>
      <span class="icon-bar"></span>
      <span class="icon-bar"></span>
      </button>
      
      <div class="nav-collapse collapse" style="">

        <ul class="nav pull-right">
            <!--<li class="">
              <a href="<spring:url value="/" />">
                <span class=""><spring:message code="home" /></span>
              </a>
            </li>-->
            <li class="dropdown">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                <span class="">My Points <b class="caret"></b></span>  
              </b></a>               
                <ul class="dropdown-menu">
                    <li><a href="<spring:url value="/member/points"/>"><spring:message code="menu.viewpoints"/></a></li>
                    <c:if test="${isEmployee}">
                    <li><a href="<spring:url value="/member/purchases"/>"><spring:message code="menu.viewpurchase"/></a></li>
                    </c:if>
                    <li><a href="<spring:url value="/member/shoppinglist"/>"><spring:message code="menu.viewshoppinglist"/></a></li>
                </ul>
            </li>
            <c:if test="${!isEmployee}">
              <li class="prepaidUser">
                <a href="<spring:url value="/member/prepaid/show"/>">
                    <span class="icon-menu-prepaid"></span>
                    <span class="glyphicon-class"><spring:message code="prepaid_pageheader"/></span>
                </a>
              </li>
            </c:if>
            <li class="">
              <a href="<spring:url value="/member/complaint"/>">            
              <span class=""><spring:message code="complaint_pageheader"/></span>
              </a>
            </li>
            <li class="dropdown">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                <span class="">Settings <b class="caret"></b></span>  
              </b></a>
                <ul class="dropdown-menu">
                    <li class="nav-header">Settings</li>
                    <li><a href="<spring:url value="/member/password/change"/>"><spring:message code="menu.passwordchange"/></a></li>
                    <li><a href="<spring:url value="/member/update"/>"><spring:message code="menu.memberupdate"/></a></li>
                    <li><a href="<spring:url value="/member/pin/update"/>"><spring:message code="menu.pinupdate"/></a></li>
                    <li class="divider"></li>
                    <li><a href="<spring:url value="/logout"/>"><spring:message code="security.logout"/></a></li>
                </ul>
            </li>
            <li style="border-left: 1px solid #CCCCCC;"></li>

            
            <span class="language-responsive"><tiles:insertAttribute name="selectLanguage"/></span>
        </ul>
      </div>
    </div>
  </div>
</div>


<!-- Topbar menu duplicated to address hover issue, for desktop -->
<div class="navbar navbar-static-top visible-desktop">
  <div class="navbar-inner">
    <div class="container">
      

      
      
      <div class="">
      
        <sec:authorize var="isEmployee" ifAnyGranted="MTYP002" />
        <button type="button" class="btn btn-navbar" data-toggle="collapse" data-target="nav-collapse">
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
        </button>
        <div class="navbar-brand pull-left" style="display: block">
          <a class="tiptip" href="<spring:url value="/" />" data-original-title="Back to Home" data-placement="right" title="">
            <img width="" src="<c:url value="/images/Logo-3.png" />">
          </a>
        </div>
        <ul class="nav pull-left ml10">
          <!--<li class="username-topbar">
              
          </li>-->
          <li class="dropdown icon-topbar">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
              <span class=""></span>
              <span class="glyphicon-class">My Points</span>  
              <b class="caret"></b>
            </b></a>               
              <ul class="dropdown-menu">
                  <li><a href="<spring:url value="/member/points"/>"><spring:message code="menu.viewpoints"/></a></li>
                  <c:if test="${isEmployee}">
                  <li><a href="<spring:url value="/member/purchases"/>"><spring:message code="menu.viewpurchase"/></a></li>
                  </c:if>
                  <li><a href="<spring:url value="/member/shoppinglist"/>"><spring:message code="menu.viewshoppinglist"/></a></li>
              </ul>
          </li>
          <c:if test="${!isEmployee}">
            <li class="icon-topbar prepaidUser">
              <a class="tiptip" href="<spring:url value="/member/prepaid/show"/>">
                  <span class=""></span>
                  <span class="glyphicon-class"><spring:message code="prepaid_pageheader"/></span>
              </a>
            </li>
          </c:if>
          <li class="icon-topbar">
            <a class="tiptip"href="<spring:url value="/member/complaint"/>">            
                <span class=""></span>
                <span class="glyphicon-class"><spring:message code="complaint_pageheader"/></span>
            </a>
          </li>
        </ul>
      

        <ul class="nav pull-right">
            <!--<li class="icon-topbar">
              <a href="<spring:url value="/" />">
                <span class="icon-menu-home"></span>
                <span class="glyphicon-class"><spring:message code="home" /></span>
              </a>
            </li>-->

            
            <li class="dropdown icon-topbar" style="margin-right: -4px; ">
              <a href="#" class="dropdown-toggle arrow-icon-bottom" data-toggle="dropdown">
                <span class="icon-menu-user2"></span>
                <span class="glyphicon-class username-topbar2"><sec:authentication property="principal.username"/></span> 
                <b class="caret"></b> 
              </b></a>
                <ul class="dropdown-menu row minor-menu">
                    <li class="nav-header">Settings</li>
                    <li><a href="<spring:url value="/member/password/change"/>"><spring:message code="menu.passwordchange"/></a></li>
                    <li><a href="<spring:url value="/member/update"/>"><spring:message code="menu.memberupdate"/></a></li>
                    <li><a href="<spring:url value="/member/pin/update"/>"><spring:message code="menu.pinupdate"/></a></li>
                    <li class="divider mb5"></li>
                    <li>                  
                      <a href="<spring:url value="/logout"/>">
                        <i class="icon-off"></i>
                        <spring:message code="security.logout"/>
                      </a>
                    </li>
                </ul>
            </li>

            <tiles:insertAttribute name="selectLanguage"/>
            

            
            
        </ul>
      </div>
    </div>
  </div>
</div>
    

    
<%--

<header role="banner" class="">
  <div id="menuMega" class="menu3dmega navbar navbar-fixed-top bs-docs-nav skin-black">
     <div class="menuToggle">Menu <span class="megaMenuToggle-icon"></span></div>
    <div class="menuToggle">Menasdasdus <span class="megaMenuToggle-icon"></span></div>

    <ul class="container">
      <tiles:insertAttribute name="menu"/>
      <tiles:insertAttribute name="selectLanguage"/>
      <li class="nav pull-right"><a href="<spring:url value="/logout"/>"><spring:message code="security.logout"/></a></li>
      <li class="nav pull-right"><a href="<spring:url value="/" />"><i class="icon-home"></i><spring:message code="home" /></a></li>
      <li class="right"><span class="arrow-icon-bottom"><span><sec:authentication property="principal.username"/></span></span>

        <div class="dropdown-menu row minor-menu">
          <ul class="content main-menu-dropdown">
            <li><a href="<spring:url value="/member/password/change"/>"><spring:message code="menu.passwordchange"/></a></li>
            <li><a href="<spring:url value="/member/update"/>"><spring:message code="menu.memberupdate"/></a></li>
            <li><a href="<spring:url value="/member/pin/update"/>"><spring:message code="menu.pinupdate"/></a></li>
            <li><a href="<spring:url value="/member/points"/>"><spring:message code="menu.viewpoints"/></a></li>
          </ul>
        </div>
      </li>
    </ul>
  </div>
</header>--%>



<div class="container">
  <%@ include file="/WEB-INF/views/common/messages.jsp" %>
  <tiles:insertAttribute name="body"/>
</div>


<!-- Bootstrap core JavaScript
================================================== -->
<!-- Placed at the end of the document so the pages load faster -->
<script src="<c:url value="/js/bootstrap/bootstrap.js" />"></script>
<%--<script src="<c:url value="/js/mega/mega-menu3d.js" />" type="text/javascript"></script>--%>
<!--Demo-->
<script src="<c:url value="/js/jquery/jquery.cookie.js"/>"></script>
<script src="<c:url value="/js/jquery/jquery.ddslick.custom.js"/>"></script>

<%--<script src="<c:url value="/js/mega/mega-demo.js" />" type="text/javascript"></script>--%>


</body>
</html>
