package com.transretail.crm.schedule.service.impl;


import java.text.ParseException;

import org.apache.commons.lang3.StringUtils;
import org.quartz.Scheduler;
import org.quartz.SchedulerException;
import org.quartz.TriggerKey;
import org.quartz.impl.triggers.CronTriggerImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.transretail.crm.core.entity.ApplicationConfig;
import com.transretail.crm.core.entity.enums.AppKey;
import com.transretail.crm.core.repo.ApplicationConfigRepo;
import com.transretail.crm.schedule.service.JobSchedulerService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


@Service("jobSchedulerService")
public class JobSchedulerServiceImpl implements JobSchedulerService {

    @Autowired
    private Scheduler scheduler;
    @Autowired
    private ApplicationConfigRepo applicationConfigRepo;
    private static final Logger logger = LoggerFactory.getLogger(JobSchedulerServiceImpl.class);

    @Override
	public String rescheduleJob( String triggerName, String triggerGrp, String newCronExp, AppKey appkey ) 
			throws SchedulerException, ParseException {
		if ( StringUtils.isBlank( triggerGrp ) ) {
			triggerGrp = Scheduler.DEFAULT_GROUP;
		}
		String cronExp = null;
        TriggerKey triggerKey = new TriggerKey( triggerName, triggerGrp );
		CronTriggerImpl trigger = (CronTriggerImpl) scheduler.getTrigger( triggerKey );
		trigger.setCronExpression( newCronExp );
                
		if ( null != trigger ) {
			scheduler.rescheduleJob( triggerKey, trigger );
			saveCronExpression( newCronExp, appkey );
			cronExp = newCronExp;
		}else{
                    logger.warn("No trigger key found for {}::{}. ", triggerName, triggerGrp);
                }
		return cronExp;
	}

	@Override
	public Scheduler getScheduler() {
		return scheduler;
	}

	@Override
	public String getCronExpression( String triggerName, String triggerGrp ) throws SchedulerException {
		if ( StringUtils.isBlank( triggerGrp ) ) {
			triggerGrp = Scheduler.DEFAULT_GROUP;
		}
		CronTriggerImpl trigger = (CronTriggerImpl) scheduler.getTrigger( new TriggerKey( triggerName, triggerGrp ) );
		if ( null != trigger ) {
			return trigger.getCronExpression();
		}
		return null;
	}



	private void saveCronExpression( String cronExp, AppKey appkey ) {
        ApplicationConfig config = applicationConfigRepo.findByKey( appkey );
        if ( null == config ) {
            config = new ApplicationConfig();
            config.setKey( appkey );
        }

        config.setValue( cronExp );
        applicationConfigRepo.save( config );
    }
}
