package com.transretail.crm.report.template;

/**
 *
 * @author Monte Cillo Co (mco@exist.com)
 */
public interface CommonReportFilter {

    String DATE_FROM = "dateFrom";
    String DATE_TO = "dateTo";
    String STORES = "stores";
    String STORE = "store";
    String YEAR = "year";
    String MONTH = "month";
    String WEEK = "week";
    String ITEM_CODE = "itemCode";
    String RANK = "rank";
    String MEMBER_TYPE = "memberType";
}
