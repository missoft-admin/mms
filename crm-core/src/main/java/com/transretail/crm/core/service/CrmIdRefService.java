package com.transretail.crm.core.service;


import java.util.List;


public interface CrmIdRefService {

	void saveEarningMember(Long id);

	void deleteEarningMember(List<Long> ids);

}
