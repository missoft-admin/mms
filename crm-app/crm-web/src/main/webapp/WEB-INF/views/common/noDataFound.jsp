<%@ include file="../common/taglibs.jsp" %>

<div id="noData" class="modal hide  groupModal nofly span4" id="noDataFound" tabindex="-1" role="dialog" aria-hidden="true" style="top: 40%; left: 50%;">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h4><span><spring:message code="report.no.data.found"/></span></h4>
      </div>
      <div class="modal-footer">
        <button type="button" id="close" class="btn btn-default" data-dismiss="modal"><spring:message code="label_close"/></button>
      </div>
    </div>
  </div>
</div>

<style type="text/css">
  <!-- 
  .modal-header { text-align: center !important; } 
  .modal-footer { text-align: center !important; } 
  -->
</style>
