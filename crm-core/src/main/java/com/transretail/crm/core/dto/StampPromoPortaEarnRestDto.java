package com.transretail.crm.core.dto;

import com.google.common.collect.Lists;
import com.transretail.crm.core.service.StampPromoService;
import org.joda.time.DateTime;

import java.io.Serializable;
import java.util.List;

import static org.apache.commons.lang3.StringUtils.isNotBlank;

/**
 * @author Mike de Guzman
 */
public class StampPromoPortaEarnRestDto implements Serializable {

    private String memberId;
    private String storeCode;
    private String transactionId;
    private DateTime earnDate;
    private List<StampPromoPortaEarnDetailsDto> earnDetails = Lists.newArrayList();
    private Double totalStamps;
    private String md5ValidationKey;

    public StampPromoPortaEarnRestDto() {}

    public StampPromoPortaEarnRestDto(String memberId, String storeCode, String transactionId, DateTime earnDate,
            Double totalStamps, String dateFormat) {

        this.memberId = memberId;
        this.storeCode = storeCode;
        this.transactionId = transactionId;
        this.earnDate = earnDate;
        this.totalStamps = totalStamps;
        this.md5ValidationKey = this.memberId + this.transactionId + earnDate.toString(isNotBlank(dateFormat) ? dateFormat : StampPromoService.MD5_DATEFORMAT);
            //<memberId><transactionId><earnDateYYYYMMDD>
    }

    public String getMemberId() {
        return memberId;
    }

    public void setMemberId(String memberId) {
        this.memberId = memberId;
    }

    public String getStoreCode() {
        return storeCode;
    }

    public void setStoreCode(String storeCode) {
        this.storeCode = storeCode;
    }

    public String getTransactionId() {
        return transactionId;
    }

    public void setTransactionId(String transactionId) {
        this.transactionId = transactionId;
    }

    public DateTime getEarnDate() {
        return earnDate;
    }

    public void setEarnDate(DateTime earnDate) {
        this.earnDate = earnDate;
    }

    public List<StampPromoPortaEarnDetailsDto> getEarnDetails() {
        return earnDetails;
    }

    public void setEarnDetails(List<StampPromoPortaEarnDetailsDto> earnDetails) {
        this.earnDetails = earnDetails;
    }

    public Double getTotalStamps() {
        return totalStamps;
    }

    public void setTotalStamps(Double totalStamps) {
        this.totalStamps = totalStamps;
    }

    public String getMd5ValidationKey() {
        return md5ValidationKey;
    }

    public void setMd5ValidationKey(String md5ValidationKey) {
        this.md5ValidationKey = md5ValidationKey;
    }
}
