<%@ include file="../../common/taglibs.jsp" %>


<div id="content_loyaltyAssignForm" class="modal hide  nofly modal-dialog span7">

  <div class="modal-content">
  <c:url var="url_theAction" value="${url_action}" />
  <form:form id="loyaltyAssignForm" name="loyaltyAssign" modelAttribute="loyaltyAssign" action="${url_theAction}" class="modal-form form-horizontal">

    <div class="modal-header">
      <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
      <h4>
        <spring:message var="lbl_header" code="loyalty_label_assign"/>
        <c:if test="${not empty loyaltyAssign.loyaltyCardNo}"><spring:message var="lbl_header" code="loyalty_label_replace"/></c:if>
        <span><c:out value="${lbl_header}" /></span>
      </h4>
    </div>

    <div class="modal-body">
      <div class="hide alert alert-error" id="content_error"><button type="button" class="close" onclick="$(this).parent().hide();">&times;</button><div><form:errors path="*"/></div></div>

      <div class="data-block">
        <form:hidden path="id" />

	      <div class="pull-left">
          <div class="control-group">
            <label for="createdBy" class="control-label control-label-01"><spring:message code="label_member_created_by" /></label>
            <span class="control-label control-label-x label-input"><c:out value="${loyaltyAssign.createdByDetails}" /></span>
          </div>
          <c:if test="${not empty loyaltyAssign.loyaltyCardNo}">
	        <div class="control-group">
	          <label for="accountId" class="control-label control-label-01 "><spring:message code="loyalty_label_memberbarcode" /></label>
	          <span class="control-label control-label-01 label-input"><c:out value="${loyaltyAssign.accountId}" /></span>
	        </div>
          </c:if>
          <div class="control-group">
            <label for="formattedMemberName" class="control-label control-label-01 "><spring:message code="loyalty_prop_name" /></label>
            <span class="control-label control-label-01 label-input"><c:out value="${loyaltyAssign.formattedMemberName}" /></span>
          </div>
          <div class="control-group">
            <label for="gender" class="control-label control-label-01 "><spring:message code="loyalty_prop_gender" /></label>
            <span class="control-label control-label-01 label-input"><c:out value="${loyaltyAssign.customerProfile.gender.description}" /></span>
          </div>
          <div class="control-group">
            <label for="birthday" class="control-label control-label-01 "><spring:message code="loyalty_prop_birthdate" /></label>
            <span class="control-label control-label-01 label-input"><fmt:formatDate value="${loyaltyAssign.customerProfile.birthdate}" pattern="MMM dd, yyyy"/></span>
          </div>
          <div class="control-group">
            <label for="registeredStore" class="control-label control-label-01 "><spring:message code="loyalty_prop_prefstore" /></label>
            <span class="control-label control-label-01 label-input"><c:if test="${not empty loyaltyAssign.registeredStore}"><c:out value="${loyaltyAssign.registeredStore}" /> - <c:out value="${loyaltyAssign.registeredStoreName}" /></c:if></span>
          </div>

          <c:if test="${not empty loyaltyAssign.loyaltyCardNo}">
          <div class="control-group">
            <label for="loyaltyCardNo" class="control-label control-label-01 "><spring:message code="loyalty_label_currentlc" /></label>
            <span class="control-label control-label-01 label-input"><c:out value="${loyaltyAssign.loyaltyCardNo}" /></span>
          </div>
          </c:if>
	      </div>
        
	      <div class="pull-left">
          <div class="control-group">
            <c:if test="${not empty loyaltyAssign.contact}"><c:set var="hasContact" value="checked" /></c:if>
            <input type="checkbox" class="control-label input-checkbox" disabled="disabled" ${hasContact} />
            <label for="" class="control-label control-label-02 "><spring:message code="loyalty_label_hascontactno" /></label>
          </div>
          <div class="control-group">
            <c:if test="${not empty loyaltyAssign.email}"><c:set var="hasEmail" value="checked" /></c:if>
            <input type="checkbox" class="control-label input-checkbox" disabled="disabled" ${hasEmail} />
            <label for="" class="control-label control-label-02 "><spring:message code="loyalty_label_hasemail" /></label>
          </div>
          <div class="control-group">
            <c:if test="${not empty loyaltyAssign.loyaltyCardNo}"><c:set var="hasLoyaltyCard" value="checked" /></c:if>
            <input type="checkbox" value="" class="control-label input-checkbox" disabled="disabled" ${hasLoyaltyCard} />
            <label for="" class="control-label control-label-02 "><spring:message code="loyalty_label_hasloyaltycard" /></label>
          </div>
	      </div>
	      <div class="clearfix"></div>

        <div>
          <div class="alert alert-info">
            <div>
              <spring:message var="lbl_scanCard" code="loyalty_msg_scancard" />
              <input type="text" placeholder="${lbl_scanCard}" id="lbl_scanCard" data-url="<c:url value="/loyaltycard/scan/" />" />
            </div>
          </div>
          <div class="control-group">
            <spring:message var="lbl_loyaltyCardNo" code="loyalty_label_loyaltycard"/>
            <c:if test="${not empty loyaltyAssign.loyaltyCardNo}"><spring:message var="lbl_loyaltyCardNo" code="loyalty_label_loyaltycard"/></c:if>
	          <label for="accountId" class="control-label control-label-01 "><c:out value="${lbl_loyaltyCardNo}" /></label>
	          <div class="controls">
              <form:input id="loyaltyCardNo" path="loyaltyCardNo" readonly="true" />
	          </div>
	        </div>
          
          
          
          <c:choose>
          <c:when test="${not empty loyaltyAssign.loyaltyCardNo}">
          <div class="control-group">
            <label for="currentCardFee" class="control-label control-label-01 "><spring:message code="loyalty_prop_currentcardfee" /></label>
            <span class="control-label control-label-01"><c:out value="${loyaltyAssign.currentFee}" /></span>
          </div>
          
          <div class="control-group">
            <label for="registeredStore" class="control-label control-label-01 "><spring:message code="loyalty_prop_cardfee" /></label>
            <span class="control-label control-label-01" id="replacementFeeTxt"></span>
            <span class="span checkbox checkbox-custom1">
              <input type="checkbox" name="isFree" id="freeChk" class="input-checkbox" />
              <label class="control-label control-label-02 "><spring:message code="loyalty_prop_free"/></label>
            </span>
            <form:hidden id="replacementFee" path="replacementFee" />
          </div>
          </c:when>
          <c:otherwise>
          <div class="control-group">
            <label for="registeredStore" class="control-label control-label-01 "><spring:message code="loyalty_prop_annualfee" /></label>
            <span class="control-label control-label-01" id="annualFeeTxt"></span>
            <span class="span checkbox checkbox-custom1">
              <input type="checkbox" name="isFree" id="freeChk" class="input-checkbox" />
              <label class="control-label control-label-02 "><spring:message code="loyalty_prop_free"/></label>
            </span>
            <form:hidden id="annualFee" path="annualFee" />
          </div>
          </c:otherwise>
          </c:choose>
          
          <div class="control-group">
            <label for="company" class="control-label control-label-01 "><spring:message code="loyalty_prop_company" /></label>
            <span class="control-label control-label-01" id="companyTxt">${cardDetails['company']}</span>
          </div>
          
          <div class="control-group">
            <label for="cardType" class="control-label control-label-01 "><spring:message code="loyalty_prop_cardtype" /></label>
            <span class="control-label control-label-01" id="cardTypeTxt">${cardDetails['cardType']}</span>
          </div>
          
          <div class="control-group">
            <label for="expiryDate" class="control-label control-label-01 "><spring:message code="loyalty_prop_expirydate" /></label>
            <span class="control-label control-label-01" id="expiryDateTxt">${cardDetails['expiryDate']}</span>
          </div>
          
          <div class="control-group">
            <spring:message var="lbl_transactionNo" code="loyalty_prop_transactionno"/>
            <label for="accountId" class="control-label control-label-01 "><b class="required hideFree">* </b><c:out value="${lbl_transactionNo}" /></label>
            <div class="controls">
              <form:input id="transactionNo" path="transactionNo" />
            </div>
          </div>
          
          
	        <div class="clearfix"></div>
        </div>
      </div>

    </div>

    <div class="modal-footer">
      <!--<input id="saveButton" class="btn btn-primary" type="submit" value="<spring:message code="label_save" />" />
      <input id="cancelButton" class="btn " type="button" value="<spring:message code="label_cancel" />" data-dismiss="modal" /> -->
      <button type="button" id="saveButton" class="btn btn-primary"><spring:message code="label_save" /></button>
      <button type="button" id="cancelButton" data-dismiss="modal" class="btn btn-default"><spring:message code="label_cancel"/></button>
    </div>

  </form:form>
  </div>


  <script src="<c:url value="/js/viewspecific/customermodel/loyaltyAssignForm.js"/>" type="text/javascript"></script>
  <style>
    .control-label-x { 
      text-align: left !important; width: 170px !important; 
      } 
      #content_loyaltyAssignForm label {
          float: left;
          padding: 5px 0 0;
          text-align: right;
          width: 190px;
      }  
      #loyaltyAssignForm.form-horizontal .control-label.label-input {
        color: #999;
        text-align: left;
      }
      #loyaltyAssignForm.form-horizontal .input-checkbox {
        width: 14px;
        margin-top: 8px;
        margin-right: 7px;
      }
      #loyaltyAssignForm.form-horizontal .checkbox-custom1 {
        margin-left: 198px;
      }
      #loyaltyAssignForm.form-horizontal .control-label.control-label-02 {
        margin-left: 0px;
      }

  </style>


</div>

