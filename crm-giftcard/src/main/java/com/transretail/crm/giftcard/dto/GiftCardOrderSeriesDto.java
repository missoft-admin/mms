package com.transretail.crm.giftcard.dto;


/**
 *
 */
public class GiftCardOrderSeriesDto {
	
	private String profile;
	private Long startingSeries;
	private Long endingSeries;
	private Long served;
	
	
	public GiftCardOrderSeriesDto(String profile, Long startingSeries,
			Long endingSeries) {
		super();
		this.profile = profile;
		this.startingSeries = startingSeries;
		this.endingSeries = endingSeries;
	}
	public String getProfile() {
		return profile;
	}
	public void setProfile(String profile) {
		this.profile = profile;
	}
	public Long getStartingSeries() {
		return startingSeries;
	}
	public void setStartingSeries(Long startingSeries) {
		this.startingSeries = startingSeries;
	}
	public Long getEndingSeries() {
		return endingSeries;
	}
	public void setEndingSeries(Long endingSeries) {
		this.endingSeries = endingSeries;
	}
	public Long getOrdered() {
		return endingSeries - startingSeries + 1;
	}
	public Long getServed() {
		return served;
	}
	public void setServed(Long served) {
		this.served = served;
	}
	
	
}
