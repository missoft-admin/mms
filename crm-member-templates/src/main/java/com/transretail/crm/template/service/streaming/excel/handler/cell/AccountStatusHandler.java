package com.transretail.crm.template.service.streaming.excel.handler.cell;

import com.transretail.crm.core.entity.MemberModel;
import com.transretail.crm.core.entity.enums.MemberStatus;
import com.transretail.crm.template.service.MemberTemplateParseException;
import com.transretail.crm.template.service.streaming.excel.handler.sheet.AbstractSheetHandler;
import org.apache.commons.lang3.StringUtils;
import org.springframework.context.i18n.LocaleContextHolder;

/**
 * @author Allan G. Ramirez (agramirez@exist.com)
 */
public class AccountStatusHandler implements CellHandler {
    private AbstractSheetHandler sheetHandler;

    public AccountStatusHandler(AbstractSheetHandler sheetHandler) {
        this.sheetHandler = sheetHandler;
    }

    @Override
    public void handle(MemberModel dto, String cellText) throws MemberTemplateParseException {
        if (StringUtils.isNotBlank(cellText)) {
            if (MemberStatus.ACTIVE.toString().equals(cellText)) {
                dto.setAccountStatus(MemberStatus.ACTIVE);
            } else if (MemberStatus.TERMINATED.toString().equals(cellText)) {
                dto.setAccountStatus(MemberStatus.TERMINATED);
            } else {
                StringBuilder allowedStatus = new StringBuilder();
                allowedStatus.append(MemberStatus.ACTIVE);
                allowedStatus.append(", ");
                allowedStatus.append(MemberStatus.TERMINATED);
                throw new MemberTemplateParseException(sheetHandler.getMessageSource()
                    .getMessage("member.template.messages.excel.accountstatus.invalid",
                        new String[]{cellText, allowedStatus.toString()}, LocaleContextHolder.getLocale()));
            }
        } else {
            dto.setAccountStatus(null);
        }
    }
}
