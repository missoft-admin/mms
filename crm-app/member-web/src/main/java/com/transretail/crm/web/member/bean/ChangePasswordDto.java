package com.transretail.crm.web.member.bean;

import org.hibernate.validator.constraints.NotEmpty;

import com.transretail.crm.core.validator.Confirm;
import com.transretail.crm.core.validator.Password;

/**
 * @author Allan G. Ramirez (agramirez@exist.com)
 */
@Confirm(field = "password")
public class ChangePasswordDto {
	@Password
	private String newPassword;
    private String password;
    @NotEmpty
    private String confirmPassword;

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getConfirmPassword() {
        return confirmPassword;
    }

    public void setConfirmPassword(String confirmPassword) {
        this.confirmPassword = confirmPassword;
    }

	public String getNewPassword() {
		return newPassword;
	}

	public void setNewPassword(String newPassword) {
		this.newPassword = newPassword;
		this.password = newPassword;
	}
}
