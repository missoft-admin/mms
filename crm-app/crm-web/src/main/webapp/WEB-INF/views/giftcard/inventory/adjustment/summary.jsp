<%@ include file="../../../common/taglibs.jsp" %>

<style type="text/css">
  <!--
  #content_summaryList .dataTable tr td:last-child {
    border-left: 0px;
  }
  -->
</style>

<div class="modal hide  nofly" id="summaryDialog" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog">
     <div class="modal-header">
      <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
      <h4><spring:message code="physical_count_summary_label"/></h4>
    </div>
    <div class="modal-content">

      <div class="modal-body">

        <div class="form-horizontal pull-right">
          <select name="reportType" class="input-small" id="reportType">
            <c:forEach items="${reportTypes}" var="reportType">
              <option data-url="<c:url value="/giftcard/inventory/adjustment/print/${reportType.code}" />" value="${reportType.code}"><spring:message code="report_type_${reportType.code}" /></option>
            </c:forEach>
          </select>
          <input type="button" id="summaryPrint" value="<spring:message code="label_print" />" class="btn btn-primary btn-print btn-print-small" />
        </div>

        <div id="content_summaryList">
          <div id="list_summary"></div>
        </div>

      </div>
      <div class="modal-footer">
        <button type="button" id="summaryCancel" class="btn btn-default" data-dismiss="modal"><spring:message code="label_cancel" /></button>
        <button type="button" id="summaryProcess" data-url="<c:url value="/giftcard/inventory/adjustment/process" />" class="btn btn-primary"><spring:message code="label_process" /></button>
      </div>
    </div>
  </div>
</div>

<script>
  $(document).ready(function() {
    $list = $("#list_summary");
    $list.ajaxDataTable({
      'autoload': false,
      'ajaxSource': "<c:url value="/giftcard/inventory/adjustment/summary" />",
      'aaSorting': [[0, "desc"]],
      'columnHeaders': [
	"<spring:message code="gc.inventory.physical.count.product.profile.label" />",
	"<spring:message code="physical_count_current_inventory" />",
	"<spring:message code="physical_count_label" />",
	"<spring:message code="physical_count_difference" />",
	"<spring:message code="gc.location" />"
      ],
      'modelFields': [
	{name: 'productProfile', sortable: false, fieldCellRenderer: function(data, type, row) {
	    return row.productProfile + " - " + row.productProfileName;
	  }
	},
	{name: 'currentInventory', sortable: false},
	{name: 'physicalCount', sortable: false},
	{name: 'difference', sortable: false},
	{name: 'location', sortable: false, fieldCellRenderer : function (data, type, row) {
      return row.location? row.location + " - " + row.locationName : "";
  }}
      ]
    });

  });

  function reloadSummaryTable() {
    $("#list_summary").ajaxDataTable('search', new Object());
  }

</script>


