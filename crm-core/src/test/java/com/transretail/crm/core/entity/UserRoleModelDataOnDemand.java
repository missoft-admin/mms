package com.transretail.crm.core.entity;

import java.security.SecureRandom;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Random;
import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Configurable;
import org.springframework.stereotype.Component;

import com.transretail.crm.core.entity.UserRoleModel;
import com.transretail.crm.core.repo.UserRoleRepo;
import com.transretail.crm.core.service.UserRoleModelService;

@Component
@Configurable
public class UserRoleModelDataOnDemand {
    @Autowired
    UserRoleModelService userRoleModelService;
    @Autowired
    UserRoleRepo userRoleRepo;
    private Random rnd = new SecureRandom();
    private List<UserRoleModel> data;

    public UserRoleModel getNewTransientUserRoleModel(int index) {
        UserRoleModel obj = new UserRoleModel();
        setCode(obj, index);
        setDescription(obj, index);
        setEnabled(obj, index);
        return obj;
    }

    public void setCode(UserRoleModel obj, int index) {
        String code = "code_" + index;
        obj.setCode(code);
    }

    public void setDescription(UserRoleModel obj, int index) {
        String description = "description_" + index;
        obj.setDescription(description);
    }

    public void setEnabled(UserRoleModel obj, int index) {
        Boolean enabled = Boolean.TRUE;
        obj.setEnabled(enabled);
    }

    public UserRoleModel getSpecificUserRoleModel(int index) {
        init();
        if (index < 0) {
            index = 0;
        }
        if (index > (data.size() - 1)) {
            index = data.size() - 1;
        }
        UserRoleModel obj = data.get(index);
        String id = obj.getId();
        return userRoleModelService.findUserRoleModel(id);
    }

    public UserRoleModel getRandomUserRoleModel() {
        init();
        UserRoleModel obj = data.get(rnd.nextInt(data.size()));
        String id = obj.getId();
        return userRoleModelService.findUserRoleModel(id);
    }

    public boolean modifyUserRoleModel(UserRoleModel obj) {
        return false;
    }

    public void init() {
        int from = 0;
        int to = 10;
        data = userRoleModelService.findUserRoleModelEntries(from, to);
        if (data == null) {
            throw new IllegalStateException("Find entries implementation for 'UserRoleModel' illegally returned null");
        }
        if (!data.isEmpty()) {
            return;
        }

        data = new ArrayList<UserRoleModel>();
        for (int i = 0; i < 10; i++) {
            UserRoleModel obj = getNewTransientUserRoleModel(i);
            try {
                userRoleModelService.saveUserRoleModel(obj);
            } catch (final ConstraintViolationException e) {
                final StringBuilder msg = new StringBuilder();
                for (Iterator<ConstraintViolation<?>> iter = e.getConstraintViolations().iterator(); iter.hasNext(); ) {
                    final ConstraintViolation<?> cv = iter.next();
                    msg.append("[").append(cv.getRootBean().getClass().getName()).append(".").append(cv.getPropertyPath()).append(": ").append(cv.getMessage()).append(" (invalid value = ").append(cv.getInvalidValue()).append(")").append("]");
                }
                throw new IllegalStateException(msg.toString(), e);
            }
            userRoleRepo.flush();
            data.add(obj);
        }
    }
}
