<%@include file="../../common/taglibs.jsp" %>

<c:set var="ENCTYPE" value="application/x-www-form-urlencoded"/>

<div class="modal hide  nofly" id="refundDialog" tabindex="-1" role="dialog" aria-hidden="true">

<div class="modal-dialog">
  <div class="modal-content">
    
    <div class="modal-header">
      <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
      <h4><spring:message code="gc_refund_min"/></h4>
    </div>
 
    <div class="modal-body">
    
    <div id="contentError" class="hide alert alert-error">
      <button type="button" class="close" onclick="$(this).parent().hide();">&times;</button>
      <div><form:errors path="*"/></div>
    </div>
    
    <form name="refundForm" id="refundForm" action="${pageContext.request.contextPath}/giftcard/return/refund/save/" method="POST" enctype="${ENCTYPE}" class="form-reset form-horizontal">
      <input type="hidden" name="id" id="id" value="" />
      <div class="control-group">
        <spring:message code="gc_return_amount" var="returnAmount" />
        <label for="returnAmount" class="control-label">${returnAmount}</label> 
        <div class="controls">
          <input type="text" name="returnAmount" id="returnAmount" value="" readonly="readonly" placeholder="${returnAmount}" />
        </div>
      </div>
      
      <div class="control-group">
        <spring:message code="gc_replace_amount" var="replaceAmount" />
        <label for="replaceAmount" class="control-label">${replaceAmount}</label> 
        <div class="controls">
          <input type="text" name="replaceAmount" id="replaceAmount" value="" readonly="readonly" placeholder="${replaceAmount}" />
        </div>
      </div>
      
      <div class="control-group">
        <spring:message code="gc_refund_amount" var="refundAmount" />
        <label for="refundAmount" class="control-label">${refundAmount}</label> 
        <div class="controls">
          <input type="text" name="refundAmount" id="refundAmount" value="" placeholder="${refundAmount}" />
        </div>
      </div>
      
    </form>
    
    </div>
    <div class="modal-footer">
      <button type="button" class="refundSubmit btn btn-primary"><spring:message code="label_submit"/></button>
      <button type="button" class="btn btn-default" data-dismiss="modal"><spring:message code="label_cancel"/></button>
    </div>
  </div>
</div>

</div>




<script>

$(document).ready(function() {
	
	var CONTAINER_ERROR = "#contentError > div";
	var CONTENT_ERROR = "#contentError";
	
	$refundForm = $("#refundForm");
	$refundDialog = $("#refundDialog").modal({
		show: false
	}).on('hidden.bs.modal', function() {
		$(this).find("input").val("");
	}).css({
		"width": "450px",
	});
	
	$(".refundSubmit").click(submitForm);
	  
	function submitForm() {
	    $(".refundSubmit").prop( "disabled", true );
	    $.post($refundForm.attr("action"), $refundForm.serialize(), function(data) {
	      $(".refundSubmit").prop( "disabled", false );
	      if (data.success) {
	        location.reload();
	      } 
	      else {
	        errorInfo = "";
	        for (i = 0; i < data.result.length; i++) {
	          errorInfo += "<br>" + (i + 1) + ". "
	              + ( data.result[i].code != undefined ? 
	                  data.result[i].code : data.result[i]);
	        }
	        $refundDialog.find(CONTAINER_ERROR).html("Please correct following errors: " + errorInfo);
	        $refundDialog.find(CONTENT_ERROR).show('slow');
	      }
	    }); 
	  }
	
});
</script>
