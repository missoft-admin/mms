package com.transretail.crm.core.service.impl;

import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.argThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentMatcher;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.internal.stubbing.answers.ReturnsArgumentAt;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.data.domain.Page;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.transretail.crm.core.dto.ServiceDto;
import com.transretail.crm.core.entity.UserModel;
import com.transretail.crm.core.entity.UserPermission;
import com.transretail.crm.core.entity.UserRoleModel;
import com.transretail.crm.core.entity.UserRolePermission;
import com.transretail.crm.core.repo.UserPermissionRepo;
import com.transretail.crm.core.repo.UserRepo;
import com.transretail.crm.core.repo.UserRolePermissionRepo;
import com.transretail.crm.core.repo.UserRoleRepo;
import com.transretail.crm.core.service.UserService;

@ContextConfiguration(locations = "classpath*:/META-INF/spring/applicationContext*.xml")
@RunWith(SpringJUnit4ClassRunner.class)
public class UserServiceTest {

    public final Long VALID_ID = 0L;
    public final String VALID_USER_NAME = "Test User Name";
    public final int FIRST_RESULT = 0;
    public final int MAX_RESULT = 5;

    @Autowired
    private UserService userService;

    @Mock
    private UserRepo userRepo;
 
    @Mock
    private UserRoleRepo userRoleRepo;

    @Mock
    private PasswordEncoder passwordEncoder;

    @InjectMocks
    private UserService userServiceImpl = new UserServiceImpl();

    @Autowired
    private UserRepo userRepo1;
    @Autowired
    private UserRoleRepo userRoleRepo1;
    @Autowired
    private UserPermissionRepo userPermissionRepo;
    @Autowired
    private UserRolePermissionRepo userRolePermissionRepo;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        userRolePermissionRepo.deleteAll();
        userRoleRepo1.deleteAll();
        userPermissionRepo.deleteAll();
        userRepo1.deleteAll();
    }

    @After
    public void tearDown() {
        userRolePermissionRepo.deleteAll();
        userRoleRepo1.deleteAll();
        userPermissionRepo.deleteAll();
        userRepo1.deleteAll();
    }

    //findUserModelEntries

    @Test
    public void testFindUserModelEntries() {
        List<UserModel> userModels = new ArrayList<UserModel>();
        Page<UserModel> userModelPage = (Page<UserModel>) mock(Page.class);

        when(userRepo.findAll(new org.springframework.data.domain.PageRequest(FIRST_RESULT / MAX_RESULT, MAX_RESULT))).thenReturn(
            userModelPage);
        when(userModelPage.getContent()).thenReturn(userModels);

        List<UserModel> returned = userServiceImpl.findUserModelEntries(FIRST_RESULT, MAX_RESULT);

        verify(userRepo, times(1)).findAll(new org.springframework.data.domain.PageRequest(FIRST_RESULT / MAX_RESULT, MAX_RESULT));
        verifyNoMoreInteractions(userRepo);

        assertEquals(userModels, returned);

    }

    @Test
    public void testDeleteUserModel() {
        UserModel userModel = new UserModel();

        userServiceImpl.deleteUserModel(userModel);

        verify(userRepo, times(1)).delete(userModel);
        verifyNoMoreInteractions(userRepo);
    }

    @Test
    public void testUpdateUserModel() {
        String username = "testusername";
        UserModel savedModel = new UserModel();
        savedModel.setUsername(username);
        savedModel.setPassword("oldpassword");

        UserModel userModel = new UserModel();
        userModel.setUsername(username);
        userModel.setPassword("newpassword");

        when(userRepo.findByUsername(username)).thenReturn(savedModel);
        when(userRepo.save(userModel)).thenAnswer(new ReturnsArgumentAt(0));

        UserModel returned = userServiceImpl.updateUserModel(userModel);

        assertEquals(userModel, returned);
        assertEquals("oldpassword", returned.getPassword());
        assertEquals(username, returned.getUsername());

        verify(userRepo).save(userModel);
        verify(userRepo).findByUsername(username);
        verifyNoMoreInteractions(userRepo);
    }

    @Test(expected = DuplicateKeyException.class)
    @Ignore("Not sure if mock or real data is tested here.")
    public void testSaveUserModelUserNameExists() {
        UserModel userModel = new UserModel();

        when(userRepo.findByUsername(userModel.getUsername())).thenReturn(new UserModel());

        userService.saveUserModel(userModel);
    }

    @Test
    public void testSaveUserModelUserNameDoesNotExists() {
        String password = "password";
        UserModel userModel = new UserModel();
        userModel.setPassword(password);
        when(userRepo.findByUsername(userModel.getUsername())).thenReturn(null);
        when(passwordEncoder.encode(password)).thenReturn("encoded");
        userServiceImpl.saveUserModel(userModel);

        verify(userRepo, times(1)).findByUsername(userModel.getUsername());
        verify(userRepo, times(1)).save(userModel);
        verify(userRepo).save(argThat(new ArgumentMatcher<UserModel>() {
            @Override
            public boolean matches(Object argument) {
                UserModel model = (UserModel) argument;
                return model.getPassword().equals("encoded");
            }
        }));
        verifyNoMoreInteractions(userRepo);
    }

    @Test
    public void testFindUserModelById() {
        UserModel userModel = new UserModel();
        when(userRepo.findOne(VALID_ID)).thenReturn(userModel);

        UserModel returned = userServiceImpl.findUserModel(VALID_ID);

        verify(userRepo, times(1)).findOne(VALID_ID);
        verifyNoMoreInteractions(userRepo);

        assertEquals(userModel, returned);
    }

    @Test
    public void testFindUserModelByUsername() {
        UserModel userModel = new UserModel();
        when(userRepo.findByUsername(VALID_USER_NAME)).thenReturn(userModel);

        UserModel returned = userServiceImpl.findUserModel(VALID_USER_NAME);

        verify(userRepo, times(1)).findByUsername(VALID_USER_NAME);
        verifyNoMoreInteractions(userRepo);

        assertEquals(userModel, returned);
    }

    @Test
    public void testFindAllUserModels() {
        List<UserModel> userModels = new ArrayList<UserModel>();
        when(userRepo.findAll()).thenReturn(userModels);

        List<UserModel> returned = userServiceImpl.findAllUserModels();

        verify(userRepo, times(1)).findAll();
        verifyNoMoreInteractions(userRepo);

        assertEquals(userModels, returned);
    }

    @Test
    public void testCountAllUserModels() {
        when(userRepo.count()).thenReturn(100L);

        long returned = userServiceImpl.countAllUserModels();

        verify(userRepo, times(1)).count();
        verifyNoMoreInteractions(userRepo);

        assertEquals(100L, returned);
    }


	@Test
    public void testRetrieveCssUsers() {
    	UserModel theCssUser = new UserModel();
    	theCssUser.setEnabled( true );
    	theCssUser.setPassword( "pword" );
    	theCssUser.setUsername( "css1" );
    	theCssUser.setStoreCode( "store1" );
    	userRepo1.save(theCssUser);

        saveUserRolePermission(theCssUser, "ROLE_CSS", null);

    	UserModel theCsUser = new UserModel();
    	theCsUser.setEnabled( true );
    	theCsUser.setPassword( "pword" );
    	theCsUser.setUsername( "cs" );
     	theCsUser.setStoreCode( "store1" );
    	userRepo1.save( theCsUser );

    	ServiceDto theSvcResp = userService.retrieveCssUsers();
    	Assert.assertTrue( 1 == ( (List<UserModel>)theSvcResp.getReturnObj() ).size() );
    }

	@Test
    public void testRetrieveHrsUsers() {
    	UserModel theUser = new UserModel();
    	theUser.setEnabled( true );
    	theUser.setPassword( "pword" );
    	theUser.setUsername( "css1" );
    	theUser.setStoreCode( "store1" );
    	userRepo1.save( theUser );

        saveUserRolePermission(theUser, "ROLE_HRS", "TEST_PERM");

    	List<UserModel> theUsers = userService.retrieveHrsUsers();
    	Assert.assertTrue( 1 == theUsers.size() );
    }


	@Test
	public void testSearchUser() {

    	UserModel theUser = new UserModel();
    	theUser.setEnabled( true );
    	theUser.setPassword( "pword" );
    	theUser.setUsername( "css1" );
    	theUser.setStoreCode( "store1" );
    	userRepo1.save( theUser );

    	theUser = new UserModel();
    	theUser.setEnabled( true );
    	theUser.setPassword( "pword" );
    	theUser.setUsername( "css2" );
    	theUser.setStoreCode( "store1" );
    	userRepo1.save( theUser );

    	theUser = new UserModel();
    	theUser.setEnabled( true );
    	theUser.setPassword( "pword" );
    	theUser.setUsername( "css3" );
    	theUser.setStoreCode( "store1" );
    	userRepo1.save( theUser );


    	UserModel theCriteria = new UserModel();
    	theCriteria.setUsername( "css" );
 
    	ServiceDto theSvcResp = userService.searchUser(theCriteria);
    	Assert.assertTrue( 3 == ( (List<UserModel>)theSvcResp.getReturnObj() ).size() );
    	

    	theSvcResp = userService.searchUser(theCriteria, 1, 3);
    	Assert.assertTrue( 0 == ( (List<UserModel>)( (Object[])theSvcResp.getReturnObj() )[1] ).size() );
    	

    	theSvcResp = userService.searchUser(theCriteria, 0, 3);
    	Assert.assertTrue( 3 == ( (List<UserModel>)( (Object[])theSvcResp.getReturnObj() )[1] ).size() );
    	
    	theCriteria = new UserModel();
    	theCriteria.setUsername( "admin" );

    	theSvcResp = userService.searchUser(theCriteria);
    	Assert.assertTrue( 0 == ( (List<UserModel>)theSvcResp.getReturnObj() ).size() );
	}

    private void saveUserRolePermission(UserModel member, String roleCode, String permissionCode) {
        UserRoleModel role = userRoleRepo1.findOne(roleCode);
        if (role == null) {
            role = userRoleRepo1.save(new UserRoleModel(roleCode));
        }
        UserPermission permission = null;
        if (permissionCode != null) {
            permission = userPermissionRepo.findOne(permissionCode);
            if (permission == null) {
                permission = userPermissionRepo.save(new UserPermission(permissionCode, "TEST"));
            }
        }
        userRolePermissionRepo.saveAndFlush(new UserRolePermission(member, role, permission));
    }
}
