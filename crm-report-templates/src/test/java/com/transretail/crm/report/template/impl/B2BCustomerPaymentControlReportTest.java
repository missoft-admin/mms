package com.transretail.crm.report.template.impl;

import static org.junit.Assert.*;

import java.math.BigDecimal;
import java.util.List;

import org.joda.time.LocalDate;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.transretail.crm.giftcard.entity.GiftCardCustomerProfile;
import com.transretail.crm.giftcard.entity.ReturnRecord;
import com.transretail.crm.giftcard.entity.SalesOrder;
import com.transretail.crm.giftcard.entity.SalesOrderItem;
import com.transretail.crm.giftcard.entity.SalesOrderPaymentInfo;
import com.transretail.crm.giftcard.entity.support.SalesOrderType;
import com.transretail.crm.giftcard.repo.GiftCardCustomerProfileRepo;
import com.transretail.crm.giftcard.repo.ReturnRecordRepo;
import com.transretail.crm.giftcard.repo.SalesOrderItemRepo;
import com.transretail.crm.giftcard.repo.SalesOrderPaymentInfoRepo;
import com.transretail.crm.giftcard.repo.SalesOrderRepo;
import com.transretail.crm.report.template.impl.B2BCustomerPaymentControlReport.ReportBean;
import com.transretail.crm.report.template.impl.B2BCustomerPaymentControlReport.SubreportBean;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath*:/META-INF/spring/applicationContext*.xml",
    "/META-INF/spring/applicationContext-test.xml"})
public class B2BCustomerPaymentControlReportTest {
	@Autowired
	private SalesOrderRepo salesOrderRepo;
	@Autowired
	private SalesOrderItemRepo soItemRepo;
	@Autowired
	private SalesOrderPaymentInfoRepo soPaymentRepo;
	@Autowired
	private GiftCardCustomerProfileRepo gcCustomerRepo;
	@Autowired
	private B2BCustomerPaymentControlReport instance;
	@Autowired
	private ReturnRecordRepo returnRepo;
	
	private static final DateTimeFormatter FULL_DATE_PATTERN = DateTimeFormat.forPattern("dd-MM-yyyy");

	/**
	 * Customer 1:
	 * Order 1 - 01-01-2014; advanced; sales - 3000; payment - 3000
	 * Order 2 - 01-02-2014; sales - 5000, payment - 5000
	 * Customer 2:
	 * Order 3 - 01-01-2014; sales - 3000; payment - 5000
	 * Order 4 - 01-02-2014; sales - 5000; payment - 6000
	 * @throws Exception
	 */
	@Before
	public void setUp() throws Exception {
		GiftCardCustomerProfile gcProfile1 = new GiftCardCustomerProfile();
		gcProfile1.setCustomerId("1");
		gcProfile1.setName("customer1");
		gcProfile1.setInvoiceTitle("customer1");
		gcProfile1 = gcCustomerRepo.save(gcProfile1);
		
		GiftCardCustomerProfile gcProfile2 = new GiftCardCustomerProfile();
		gcProfile2.setCustomerId("2");
		gcProfile2.setName("customer2");
		gcProfile2.setInvoiceTitle("customer2");
		gcProfile2 = gcCustomerRepo.save(gcProfile2);
		
		SalesOrder salesOrder = new SalesOrder();
		salesOrder.setOrderDate(LocalDate.parse("01-01-2014", FULL_DATE_PATTERN));
		salesOrder.setOrderNo("BBADVMMS1");
		salesOrder.setOrderType(SalesOrderType.B2B_SALES);
		salesOrder.setCustomer(gcProfile1);
		SalesOrder salesOrder1 = salesOrderRepo.save(salesOrder);
		
		SalesOrderItem soItem = new SalesOrderItem();
		soItem.setOrder(salesOrder);
		soItem.setFaceAmount(BigDecimal.valueOf(1000L));
		soItemRepo.save(soItem);
		
		soItem = new SalesOrderItem();
		soItem.setOrder(salesOrder);
		soItem.setFaceAmount(BigDecimal.valueOf(2000L));
		soItemRepo.save(soItem);
		
		SalesOrderPaymentInfo soPayment = new SalesOrderPaymentInfo();
		soPayment.setOrder(salesOrder);
		soPayment.setPaymentAmount(BigDecimal.valueOf(3000L));
		soPaymentRepo.save(soPayment);
		
		salesOrder = new SalesOrder();
		salesOrder.setOrderDate(LocalDate.parse("02-01-2014", FULL_DATE_PATTERN));
		salesOrder.setOrderNo("BBADVMMS2");
		salesOrder.setOrderType(SalesOrderType.REPLACEMENT);
		salesOrder.setCustomer(gcProfile1);
		salesOrder.setReturnNo("1");
		salesOrder = salesOrderRepo.save(salesOrder);
		
		ReturnRecord returnRecord = new ReturnRecord();
		returnRecord.setRecordNo("1");
		returnRecord.setOrderNo(salesOrder1);
		returnRecord.setReturnAmount(BigDecimal.valueOf(3000L));
		returnRecord.setReplaceAmount(BigDecimal.valueOf(1000L));
		returnRecord.setRefundAmount(BigDecimal.valueOf(2000L));
		returnRepo.save(returnRecord);
		
		salesOrder = new SalesOrder();
		salesOrder.setOrderDate(LocalDate.parse("01-01-2014", FULL_DATE_PATTERN));
		salesOrder.setOrderNo("0000003");
		salesOrder.setOrderType(SalesOrderType.B2B_SALES);
		salesOrder.setCustomer(gcProfile2);
		salesOrder = salesOrderRepo.save(salesOrder);
		
		soItem = new SalesOrderItem();
		soItem.setOrder(salesOrder);
		soItem.setFaceAmount(BigDecimal.valueOf(3000L));
		soItemRepo.save(soItem);
		
		soPayment = new SalesOrderPaymentInfo();
		soPayment.setOrder(salesOrder);
		soPayment.setPaymentAmount(BigDecimal.valueOf(5000L));
		soPaymentRepo.save(soPayment);
		
		salesOrder = new SalesOrder();
		salesOrder.setOrderDate(LocalDate.parse("02-02-2014", FULL_DATE_PATTERN));
		salesOrder.setOrderNo("BBADVMMS4");
		salesOrder.setOrderType(SalesOrderType.B2B_SALES);
		salesOrder.setCustomer(gcProfile2);
		salesOrder = salesOrderRepo.save(salesOrder);
		
		soItem = new SalesOrderItem();
		soItem.setOrder(salesOrder);
		soItem.setFaceAmount(BigDecimal.valueOf(5000L));
		soItemRepo.save(soItem);
		
		soPayment = new SalesOrderPaymentInfo();
		soPayment.setOrder(salesOrder);
		soPayment.setPaymentAmount(BigDecimal.valueOf(3000L));
		soPaymentRepo.save(soPayment);
		
		soPayment = new SalesOrderPaymentInfo();
		soPayment.setOrder(salesOrder);
		soPayment.setPaymentAmount(BigDecimal.valueOf(3000L));
		soPaymentRepo.save(soPayment);
	}

	@Test
	public void test() {
		List<ReportBean> list = instance.createDatasource(LocalDate.parse("01-01-2014", FULL_DATE_PATTERN), 
			LocalDate.parse("02-01-2014", FULL_DATE_PATTERN), "", "", "");
		
		assertEquals(2, list.size());
		
		for (ReportBean bean : list) {
			if(bean.getCustomerNo().equals("1")) {
				assertEquals(2, bean.getList().size());
				assertEquals(3000, bean.getReceivedAmount().intValue());
				assertEquals(3000, bean.getAmountReceivable().intValue());
				assertEquals(3000, bean.getAdvancedReceivedAmount().intValue());
				assertEquals(1000, bean.getAdvancedReceivedStrike().intValue());
				assertEquals(2000, bean.getAdvancedReceivedRefund().intValue());
				assertEquals(0, bean.getAdvancedReceivedBalance().intValue());
				assertEquals(0, bean.getBalance().intValue());
				
				SubreportBean sub = bean.getList().get(0);
				assertEquals(3000, sub.getSales().intValue());
				assertEquals(3000, sub.getReceivedAmount().intValue());
				assertEquals(0, sub.getAmountReceivable().intValue());
				assertEquals(3000, sub.getAdvancedReceivedAmount().intValue());
				assertEquals(3000, sub.getAdvancedReceivedBalance().intValue());
				assertEquals(3000, sub.getBalance().intValue());
				
				sub = bean.getList().get(1);
				assertEquals(3000, sub.getSales().intValue());
				assertEquals(0, sub.getReceivedAmount().intValue());
				assertEquals(3000, sub.getAmountReceivable().intValue());
				assertEquals(1000, sub.getAdvancedReceivedStrike().intValue());
				assertEquals(2000, sub.getAdvancedReceivedRefund().intValue());
				assertEquals(-3000, sub.getAdvancedReceivedBalance().intValue());
				assertEquals(-3000, sub.getBalance().intValue());
			}
			else {
				assertEquals(1, bean.getList().size());
				assertEquals(5000, bean.getReceivedAmount().intValue());
				assertEquals(2000, bean.getOverReceivedAmount().intValue());
				assertEquals(2000, bean.getOverReceivedBalance().intValue());
//				assertEquals(3000, bean.getBalance().intValue());
				
				SubreportBean sub = bean.getList().get(0);
				assertEquals(3000, sub.getSales().intValue());
				assertEquals(5000, sub.getReceivedAmount().intValue());
				assertEquals(2000, sub.getOverReceivedAmount().intValue());
				assertEquals(2000, sub.getOverReceivedBalance().intValue());
//				assertEquals(3000, sub.getBalance().intValue());
			}
		}
	}

}
