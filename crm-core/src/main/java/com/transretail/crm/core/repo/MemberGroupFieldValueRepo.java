package com.transretail.crm.core.repo;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.transretail.crm.common.repo.CrmQueryDslPredicateExecutor;
import com.transretail.crm.core.entity.MemberGroupField;
import com.transretail.crm.core.entity.MemberGroupFieldValue;

@Repository
public interface MemberGroupFieldValueRepo extends CrmQueryDslPredicateExecutor<MemberGroupFieldValue, Long> {
	List<MemberGroupFieldValue> findByMemberGroupField(MemberGroupField field);
}
