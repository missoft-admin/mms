package com.transretail.crm.web.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletResponse;

import org.joda.time.LocalDate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.google.common.collect.Lists;
import com.transretail.crm.common.reporting.jasper.JRProcessor;
import com.transretail.crm.core.entity.enums.ReportType;
import com.transretail.crm.core.service.StoreService;
import com.transretail.crm.report.template.impl.MemberCardUsageReport;
import com.transretail.crm.web.controller.util.ControllerResponse;

/**
 * @author ftopico
 */
@RequestMapping("/member/cardusage")
@Controller
public class MemberCardUsageController extends AbstractReportsController {

    private static final String PATH_MEMBER_CARD_USAGE = "/member/cardusage";
    
    @Autowired
    private MemberCardUsageReport memberCardUsageReport;
    @Autowired
    private StoreService storeService;
    @Autowired
    private MessageSource messageSource;
    
    @RequestMapping(produces="text/html")
    public String showBurnCardForm(Model model) {
        model.addAttribute("stores", storeService.getAllStores());
        model.addAttribute("reportTypes", ReportType.values());
        return PATH_MEMBER_CARD_USAGE;
    }
    
    @RequestMapping( value="/print/validate", produces="application/json; charset=utf-8" )
    public @ResponseBody ControllerResponse printValidate(
            @RequestParam String standardReportType,
            @RequestParam String year,
            @RequestParam(required=false) String week,
            @RequestParam(required=false) String startWeek,
            @RequestParam(required=false) String month,
            @RequestParam(required=false) String store,
            Model model) {

        ControllerResponse resp = new ControllerResponse();
        resp.setSuccess(true);
        List<String> errors = new ArrayList<String>();
        
        if (year == null || year.isEmpty())
            errors.add(messageSource.getMessage("report.year.required", null, LocaleContextHolder.getLocale()));
        if (standardReportType.equalsIgnoreCase("weekly") && week == null || week.isEmpty())
            errors.add(messageSource.getMessage("report.week.required", null, LocaleContextHolder.getLocale()));
        else if (standardReportType.equalsIgnoreCase("monthly") && month == null || month.isEmpty())
            errors.add(messageSource.getMessage("report.month.required", null, LocaleContextHolder.getLocale()));
        
        if (!errors.isEmpty()) {
            resp.setSuccess(false);
            resp.setResult(errors);
        }
            
        Map<String, String> filtersMap = new HashMap<String, String>();
        filtersMap.put("standardReportType", standardReportType);
        filtersMap.put("year", year);
        filtersMap.put("week", week);
        filtersMap.put("startWeek", startWeek);
        filtersMap.put("month", month);
        filtersMap.put("store", store);
        
        return resp;
    }
    
    @RequestMapping(value="/print", method=RequestMethod.GET)
    public void print(
            @RequestParam String reportType,
            @RequestParam String standardReportType,
            @RequestParam String year,
            @RequestParam(required=false) String week,
            @RequestParam(required=false) String startWeek,
            @RequestParam(required=false) String month,
            @RequestParam(required=false) String store,
            HttpServletResponse response) throws Exception {
        
        Map<String, String> filtersMap = new HashMap<String, String>();
        filtersMap.put("standardReportType", standardReportType);
        filtersMap.put("year", year);
        filtersMap.put("week", week);
        filtersMap.put("startWeek", startWeek);
        filtersMap.put("month", month);
        filtersMap.put("store", store);
        
        JRProcessor jrProcessor = memberCardUsageReport.createJrProcessor(filtersMap);
        
        if (reportType.equalsIgnoreCase(ReportType.PDF.name())) {
            renderReport(response, jrProcessor);
        } else if (reportType.equalsIgnoreCase(ReportType.EXCEL.name())) {
            String filename = null;
            
            if (standardReportType.equalsIgnoreCase("weekly")) {
                filename = "Member Report: Card Usage Weekly (" + LocalDate.now() +")";
            } else if (standardReportType.equalsIgnoreCase("monthly")) {
                filename = "Member Report: Card Usage Monthly (" + LocalDate.now() +")";
            }
            renderExcelReport(response, jrProcessor, filename);
        }
        
    }
}
