package com.transretail.crm.core.dto;

import java.util.ArrayList;
import java.util.List;

import org.springframework.util.StringUtils;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.mysema.query.types.expr.BooleanExpression;
import com.transretail.crm.common.service.dto.request.AbstractSearchFormDto;
import com.transretail.crm.core.entity.QStoreMember;
import com.transretail.crm.core.util.BooleanExprUtil;

/**
 * @author ftopico
 */
public class StoreMemberSearchDto extends AbstractSearchFormDto {

    private String store;
    private String company;
    private String cardType;
    
    @Override
    @JsonIgnore
    public BooleanExpression createSearchExpression() {

        List<BooleanExpression> expressions = new ArrayList<BooleanExpression>();
        QStoreMember storeMember = QStoreMember.storeMember;
        
        if (StringUtils.hasText(store)) {
            expressions.add(BooleanExprUtil.INSTANCE.isStringPropertyLike(storeMember.store, store));
        }
        
        if (StringUtils.hasText(company)) {
            expressions.add(BooleanExprUtil.INSTANCE.isStringPropertyLike(storeMember.company, company));
        }
        
        if (StringUtils.hasText(cardType)) {
            expressions.add(BooleanExprUtil.INSTANCE.isStringPropertyLike(storeMember.cardType, cardType));
        }
        
        return BooleanExpression.allOf(expressions.toArray(new BooleanExpression[expressions.size()]));
    }

    public String getStore() {
        return store;
    }

    public String getCompany() {
        return company;
    }

    public String getCardType() {
        return cardType;
    }

    public void setStore(String store) {
        this.store = store;
    }

    public void setCompany(String company) {
        this.company = company;
    }

    public void setCardType(String cardType) {
        this.cardType = cardType;
    }

}
