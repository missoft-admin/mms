CREATE TABLE CRM_IN_STORE (
    CODE VARCHAR(15) NOT NULL,
    CRM_PROXY_REST_PRFX VARCHAR(100) NOT NULL,
    CRM_RECONCLTN_REST_PRFX VARCHAR(100) NOT NULL,
    IP VARCHAR(50) NOT NULL,
    STORE_NAME VARCHAR(255) NOT NULL,
    TOMCAT_PORT INTEGER NOT NULL,
    PRIMARY KEY (CODE)
);

INSERT INTO CRM_IN_STORE (CODE,IP,STORE_NAME,TOMCAT_PORT,CRM_PROXY_REST_PRFX,CRM_RECONCLTN_REST_PRFX) SELECT CODE,IP,STORE_NAME,TOMCAT_PORT,CRM_PROXY_REST_PRFX,'/crm-reconciliation' FROM CRM_PROXY_INFO;

DROP TABLE CRM_PROXY_INFO;

alter table CRM_IN_STORE
    add constraint CRM_IN_STORE_UNQ_IP_PORT  unique (IP, TOMCAT_PORT);

alter table CRM_IN_STORE
    add constraint UK_3jrdv8ug6i2fcgk1iccrnjd44  unique (STORE_NAME);
