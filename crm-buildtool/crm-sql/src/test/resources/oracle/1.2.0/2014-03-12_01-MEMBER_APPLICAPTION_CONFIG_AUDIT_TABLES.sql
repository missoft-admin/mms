create table CRM_ADDRESS_AUD (
        id number(19, 0) not null,
        REV number(19, 0) not null,
        REVTYPE number(3, 0),
        BLOCK varchar2(255 char),
        BUILDING varchar2(255 char),
        CITY varchar2(255 char),
        DISTRICT varchar2(255 char),
        FLOOR number(10, 0),
        KM number(10, 0),
        POST_CODE varchar2(255 char),
        PROVINCE varchar2(50 char),
        ROOM varchar2(255 char),
        RT varchar2(255 char),
        RW varchar2(255 char),
        STREET varchar2(4000 char),
        STR_NO number(10, 0),
        SUBDISTRICT varchar2(255 char),
        VILLAGE varchar2(255 char),
        primary key (id, REV)
    );

create table CRM_AGE_OF_CHILDREN_AUD (
        REV number(19, 0) not null,
        ACCOUNT_ID number(19, 0) not null,
        AGE number(10, 0) not null,
        REVTYPE number(3, 0),
        primary key (REV, ACCOUNT_ID, AGE)
    );

 create table CRM_APPLICATION_CONFIG_AUD (
        id number(19, 0) not null,
        REV number(19, 0) not null,
        REVTYPE number(3, 0),
        CONFIG_KEY varchar2(255 char),
        CONFIG_VALUE varchar2(255 char),
        primary key (id, REV)
    );

create table CRM_MEMBER_AUD (
        id number(19, 0) not null,
        REV number(19, 0) not null,
        REVTYPE number(3, 0),
        ACCOUNT_ID varchar2(255 char),
        STATUS varchar2(20 char),
        TIME_TO_CALL varchar2(255 char),
        CARD_NO varchar2(50 char),
        ACCEPT_EMAIL char(1 char),
        ACCEPT_MAIL char(1 char),
        ACCEPT_PHONE char(1 char),
        ACCEPT_SMS char(1 char),
        SMS char(1 char),
        COMPANY_NAME varchar2(255 char),
        CONTACT varchar2(255 char),
        BIRTHDATE timestamp,
        CARS_OWNED number(10, 0),
        CHILDREN_COUNT number(10, 0),
        CLOSEST_STORE varchar2(255 char),
        CREDIT_CARD_OWNERSHIP char(1 char),
        DOMESTIC_HELPERS number(10, 0),
        FAX_NO varchar2(255 char),
        INSURANCE_OWNERSHIP char(1 char),
        NAME_NATIVE varchar2(120 char),
        PLACE_OF_BIRTH varchar2(255 char),
        POSTAL_CODE varchar2(255 char),
        EMAIL varchar2(255 char),
        ACCOUNT_ENABLED char(1 char),
        FIRST_NAME varchar2(255 char),
        KTP varchar2(255 char),
        LAST_NAME varchar2(255 char),
        LOYALTY_CARD_NO varchar2(255 char),
        CREATED_FROM varchar2(255 char),
        MIDDLE_NAME varchar2(255 char),
        NPWP_ADDRESS varchar2(255 char),
        NPWP_ID varchar2(255 char),
        NPWP_NAME varchar2(255 char),
        PARENT_ACCOUNT_ID varchar2(255 char),
        PASSWORD varchar2(255 char),
        PIN varchar2(255 char),
        BUSINESS_EMAIL varchar2(255 char),
        BUSINESS_LICENSE varchar2(255 char),
        BUSINESS_NAME varchar2(255 char),
        POSITION varchar2(255 char),
        RADIUS varchar2(255 char),
        TRANS_CODE_1 number(5, 0),
        TRANS_CODE_2 number(5, 0),
        TRANS_CODE_3 number(5, 0),
        TRANS_CODE_4 number(5, 0),
        TRANS_CODE_LONG1 number(5, 0),
        TRANS_CODE_LONG2 number(5, 0),
        TRANS_CODE_SHORT number(5, 0),
        ZONE varchar2(255 char),
        REGISTERED_STORE varchar2(255 char),
        STORE_NAME varchar2(255 char),
        TOTAL_POINTS double precision,
        username varchar2(255 char),
        VALIDATION_CODE varchar2(120 char),
        CARD_TYPE varchar2(15 char),
        EDUCATION varchar2(15 char),
        FAMILY_SIZE varchar2(15 char),
        GENDER varchar2(15 char),
        HOUSEHOLD_INCOME varchar2(15 char),
        MARITAL_STATUS varchar2(15 char),
        NATIONALITY varchar2(15 char),
        OCCUPATION varchar2(15 char),
        PERSONAL_INCOME varchar2(15 char),
        PREFERED_LANGUAGE varchar2(15 char),
        RELIGION varchar2(15 char),
        TITLE varchar2(15 char),
        EMP_TYPE varchar2(15 char),
        MEMBER_TYPE varchar2(15 char),
        BUSINESS_FIELD varchar2(15 char),
        CUSTOMER_GROUP varchar2(15 char),
        CUSTOMER_SEGREGATION varchar2(15 char),
        DEPARTMENT varchar2(15 char),
        primary key (id, REV)
    );

create table CRM_MEMBER_BANKS_AUD (
        REV number(19, 0) not null,
        ACCOUNT_ID number(19, 0) not null,
        CODE varchar2(15 char) not null,
        REVTYPE number(3, 0),
        primary key (REV, ACCOUNT_ID, CODE)
    );

create table CRM_MEMBER_FOOD_AUD (
        REV number(19, 0) not null,
        ACCOUNT_ID number(19, 0) not null,
        CODE varchar2(15 char) not null,
        REVTYPE number(3, 0),
        primary key (REV, ACCOUNT_ID, CODE)
    );

create table CRM_MEMBER_INTERESTS_AUD (
        REV number(19, 0) not null,
        ACCOUNT_ID number(19, 0) not null,
        CODE varchar2(15 char) not null,
        REVTYPE number(3, 0),
        primary key (REV, ACCOUNT_ID, CODE)
    );

create table CRM_MEMBER_PETS_AUD (
        REV number(19, 0) not null,
        ACCOUNT_ID number(19, 0) not null,
        CODE varchar2(15 char) not null,
        REVTYPE number(3, 0),
        primary key (REV, ACCOUNT_ID, CODE)
    );

alter table CRM_ADDRESS_AUD 
        add constraint FK_bf46udgrnqhbkmxvm5td9pwjk 
        foreign key (REV) 
        references CRM_REVINFO;

alter table CRM_AGE_OF_CHILDREN_AUD 
        add constraint FK_nqpfei3rr0qwjw2fhrtj8v6gr 
        foreign key (REV) 
        references CRM_REVINFO;

    alter table CRM_APPLICATION_CONFIG_AUD 
        add constraint FK_6h3dy60fsvw5wlel3qqr53osb 
        foreign key (REV) 
        references CRM_REVINFO;

    alter table CRM_MEMBER_AUD 
        add constraint FK_e1ah0ncgl6v9q3l55108dbv3n 
        foreign key (REV) 
        references CRM_REVINFO;

    alter table CRM_MEMBER_BANKS_AUD 
        add constraint FK_jwk9lwsqq2yehwa7lo3narayx 
        foreign key (REV) 
        references CRM_REVINFO;

    alter table CRM_MEMBER_FOOD_AUD 
        add constraint FK_j9253deupidw0cryd97oqa3fm 
        foreign key (REV) 
        references CRM_REVINFO;

    alter table CRM_MEMBER_INTERESTS_AUD 
        add constraint FK_1eqjs2chf44l8m35nkdmas2wa 
        foreign key (REV) 
        references CRM_REVINFO;

    alter table CRM_MEMBER_PETS_AUD 
        add constraint FK_ayt12hd0xdjyt1ujsiv6cwi3c 
        foreign key (REV) 
        references CRM_REVINFO;