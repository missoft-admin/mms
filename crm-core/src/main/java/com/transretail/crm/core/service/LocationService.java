package com.transretail.crm.core.service;

import java.util.List;

import com.transretail.crm.common.service.dto.response.ResultList;
import com.transretail.crm.core.dto.CityDto;
import com.transretail.crm.core.dto.CityProvinceSearchDto;
import com.transretail.crm.core.dto.ProvinceDto;
import com.transretail.crm.core.entity.City;
import com.transretail.crm.core.entity.Province;

public interface LocationService {

	ResultList<Province> listProvince(CityProvinceSearchDto searchDto);

	ResultList<City> listCityByProvince(CityProvinceSearchDto searchDto);

	void deleteProvince(Long id);

	void deleteCity(Long id);

	Province getProvince(Long id);

	void saveCity(CityDto cityDto);

	void saveProvince(ProvinceDto provinceDto);

	Iterable<City> getCities(Long provinceId);

	List<Province> getAllProvinces();

	List<Province> getProvincesByCityName(String cityName);

	City getCity(String name);

	Province getProvince(String name);

}
