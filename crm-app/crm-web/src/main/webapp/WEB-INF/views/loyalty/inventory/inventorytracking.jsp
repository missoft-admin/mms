<%@ include file="../../common/taglibs.jsp" %>

<style type="text/css">
<!--
#list_trackers .dataTable tr td:last-child {
    border-left: 0px;
}
#list_trackers .dataTable tr td:first-child {
    width: 100px;
}

.well .input {
  background-color: #fff;
  border: 1px solid #E8E8E8;
  box-shadow: none;    
  padding: 6px;
  height: 18px;
  margin-left: 5px;
}

.well select {
  margin: 0;
}
.well .input-group {
  margin: 5px;
}

-->
</style>

<div class="page-header page-header2">
    <h1><spring:message code="menutab_loyalty_card_tracking" /></h1>
</div>

<div class="well clearfix">
  <form:form id="inventoryTrackerForm" name="inventoryTrackerForm" modelAttribute="inventoryTrackerForm" action="${pageContext.request.contextPath}/loyaltycardinventory/tracker" method="POST" cssClass="form-reset" enctype="${ENCTYPE}" >
    <div class="input-group pull-left">
      <spring:message code="mo_tier_prop_name" var="moProduct" />
      <label for="">${moProduct}</label>
      <form:select path="product" items="${products}" itemLabel="description" itemValue="code" />
      <%--<select name="product">
        <option></option>
      <c:forEach items="${products}" var="item" >
        <option value="${item.code}">${item.description}</option>
      </c:forEach>
      </select> --%>
    </div>
    
    <div class="input-group pull-left">
      <spring:message code="inventory_prop_location" var="location" />
      <label for="">${location}</label>
      <%--<form:select path="location" items="${inventoryLocations}" itemLabel="key" itemValue="value" /> --%>
      <select name="location">
      <c:forEach items="${inventoryLocations}" var="item" >
        <c:choose>
        <c:when test="${inventoryTrackerForm.location == item.key}">
        <c:set var="selected" value="selected" />
        </c:when>
        <c:otherwise>
        <c:set var="selected" value="" />
        </c:otherwise>
        </c:choose>
        <option value="${item.key}" ${selected}>${item.key} - ${item.value}</option>
      </c:forEach>
      </select>
    </div>
    
    <div class="input-group pull-left">
      <spring:message code="label_from" var="from" />
      <label for="">${from}</label>
      <%--<input type="text" name="dateFrom"  placeholder="${from}" /> --%>
      <form:input path="dateFrom" placeholder="${from}" />
    </div>
    
    <div class="input-group pull-left">
      <spring:message code="label_to" var="to" />
      <label for="">${to}</label>
      <%--<input type="text" name="dateTo"  placeholder="${to}" /> --%>
      <form:input path="dateTo" placeholder="${to}" />
    </div>
    
    <div class="input-group pull-left">
    <label for="">&nbsp;</label>
    <input type="submit" id="inventoryTrackerSearchButton" value="<spring:message code="label_search" />" class="btn btn-primary"/>
	<input type="button" value="<spring:message code="label_clear" />" class="btn custom-reset" data-default-id="inventoryTrackerSearchButton" />
    </div>
    

    
  </form:form>
</div>

<div class="form-horizontal pull-left mb20">
  <select name="reportType" class="selectpicker" id="reportType" data-header="Select a Format">
    <c:forEach items="${reportTypes}" var="reportType">
      <option data-url="<c:url value="/loyaltycardinventory/print/tracker/${reportType.code}" />" value="${reportType.code}"><spring:message code="report_type_${reportType.code}" /></option>
    </c:forEach>
  </select>
  <button class="btn btn-print btn-print-small tiptip" id="inventoryTrackerPrintButton" value="<spring:message code="label_print" />" data-toggle="dropdown"><spring:message code="label_print" /></button>
</div>

<div class="clearfix"></div>

  <div id="content_trackerList">
    <div id="list_trackers"></div>
  </div>


  <link href="<spring:url value="/css/dataTables.css" />" rel="stylesheet">
  <script src="<spring:url value="/js/jquery/jquery.dataTables.js" />" ></script>
  <script src="<spring:url value="/js/jquery/jquery.dataTables.linkLengthMenu.js" />"></script>
  <script src="<spring:url value="/js/jquery/jquery.dataTables.customPaging.js" />"></script>
  <script src="<spring:url value="/js/jquery/ajax-datatable.js" />" ></script>
  <script src="<spring:url value="/js/common/form2js.js" />" ></script>
  <script src="<spring:url value="/js/common/json2.js" />" ></script>
  <script src="<spring:url value="/js/jquery/jquery.toObject.js" />" ></script>
  
  <script src="<spring:url value="/js/jquery/jquery.numberInput.js" />" ></script>

  <script src="<spring:url value="/js/viewspecific/loyalty/tracker.js"/>"></script>
  <link href="<c:url value="/styles/temp.css" />" rel="stylesheet" />
  
  <script src='<c:url value="/js/bootstrap/bootstrap-datepicker.js"/>'></script>
  <link href="<c:url value="/css/bootstrap/datepicker.css" />" rel="stylesheet" />
  
  
<script>
$(document).ready(function() {
	$list = $("#list_trackers");
	var columnHeaders = [
		"<spring:message code="tracker_prop_transactdate" />",  
		"<spring:message code="tracker_prop_begbal" />",
		"<spring:message code="tracker_status_transferin" />",
		"<spring:message code="tracker_status_transferout" />",
		"<spring:message code="tracker_status_burn" />",
		"<spring:message code="tracker_status_active" />",
		"<spring:message code="tracker_status_receive" />",
		"<spring:message code="tracker_status_physicalcount" />",
		"<spring:message code="tracker_prop_endbal" />"
	                    ];
	
	var modelFields = [
		{name : 'transactionDate', sortable : true},
		{name : 'begBal', sortable : false},
		{name : 'transferIn', sortable : false},
		{name : 'transferOut', sortable : false},
		{name : 'burn', sortable : false},
		{name : 'active', sortable : false},
		{name : 'receive', sortable : false},
		{name : 'physicalCount', sortable : false},
		{name : 'endBal', sortable : false},
	                   ];
	
	
	$list.ajaxDataTable({
	    'autoload'  : false,
	    'ajaxSource' : "<c:url value="/loyaltycardinventory/tracker/search" />",
	    'aaSorting'   : [[ 0, "desc" ]],
	    'columnHeaders' : columnHeaders
	    	
	    /* [
	         "<spring:message code="tracker_prop_transactdatetime" />",
	         "<spring:message code="tracker_prop_begbal" />",
	         "<spring:message code="tracker_prop_txntype" />",
	         "<spring:message code="tracker_prop_quantity" />",
	         "<spring:message code="tracker_prop_endbal" />"
	    ] */,
	    'modelFields' : modelFields
	    /* [
	     {name : 'createdDate', sortable : true},
	     {name : 'beginningBal', sortable : false},
	     {name : 'transaction', sortable : false},
	     {name : 'quantity', sortable : false},
	     {name : 'endingBal', sortable : false}
	     ] */
	});
	
	<c:if test="${loadTable}">
	
  	var formDataObj = $("#inventoryTrackerForm").toObject( { mode : 'first', skipEmpty : true } );
  	$btnSearch = $(this);
  	$btnSearch.prop('disabled', true);
  	$list.ajaxDataTable( 
  		'search', 
  		formDataObj, 
  		function() {
  			$btnSearch.prop( 'disabled', false );
  	});
  	
	</c:if>
	/*
	$("#inventoryTrackerSearchButton").click(function(e) {
		e.preventDefault();
		
	});
	*/
});
	
</script>
<script type="text/javascript">
 $('.selectpicker').selectpicker({
style: 'btn-print-drop',
size: 4,
showIcon: false
});
</script>