package com.transretail.crm.giftcard.service.impl;


import java.util.Comparator;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.SortedSet;
import java.util.TreeSet;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.BooleanUtils;
import org.apache.commons.lang3.StringUtils;
import org.joda.time.LocalDate;
import org.joda.time.format.DateTimeFormat;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.google.common.collect.Lists;
import com.google.common.collect.Sets;
import com.mysema.query.jpa.impl.JPAQuery;
import com.mysema.query.jpa.impl.JPAUpdateClause;
import com.mysema.query.types.ConstructorExpression;
import com.mysema.query.types.expr.BooleanExpression;
import com.transretail.crm.common.service.exception.GenericServiceException;
import com.transretail.crm.common.util.SimpleEncrytor;
import com.transretail.crm.core.dto.MessageDto;
import com.transretail.crm.core.util.generator.QrCodeService;
import com.transretail.crm.giftcard.GiftCardNotFoundException;
import com.transretail.crm.giftcard.GiftCardServiceResolvableException;
import com.transretail.crm.giftcard.dto.EGiftCardInfoDto;
import com.transretail.crm.giftcard.dto.EGiftCardProfileDto;
import com.transretail.crm.giftcard.dto.GiftCardInfo;
import com.transretail.crm.giftcard.dto.GiftCardTransactionInfo;
import com.transretail.crm.giftcard.entity.EGiftCardInfo;
import com.transretail.crm.giftcard.entity.GiftCardInventory;
import com.transretail.crm.giftcard.entity.QGiftCardInventory;
import com.transretail.crm.giftcard.entity.support.GiftCardInventoryStatus;
import com.transretail.crm.giftcard.repo.GiftCardInventoryRepo;
import com.transretail.crm.giftcard.service.EGiftCardManager;

import static com.mysema.query.group.GroupBy.set;


@Service
@Transactional
public class EGiftCardManagerImpl implements EGiftCardManager {

	private static final String ENC_SPLITTER = "::";
    protected static final long MAX_QUANTITY = 50; // TODO: if possible make it configurable

    @Autowired
    private GiftCardInventoryRepo inventoryRepo;
    @PersistenceContext
    private EntityManager em;
	@Autowired
	MessageSource messageSource;
    @Autowired
    private QrCodeService qrCodeService;



    @Transactional
    @Override
    public EGiftCardInfoDto update( EGiftCardInfoDto dto ) {
        GiftCardInventory gc = inventoryRepo.findByBarcode( dto.getBarcode() );
        if ( null == gc || BooleanUtils.isNotTrue( gc.getIsEgc() ) ) {
            throw new GiftCardNotFoundException( dto.getBarcode() );
        }

        try {
        	dto.setPin( SimpleEncrytor.getInstance().encrypt( dto.getPin() ) );
        	dto.setAltPin( SimpleEncrytor.getInstance().encrypt( dto.getAltPin() ) );
        } 
        catch ( SimpleEncrytor.EncryptDecryptException e ) {
            throw new GenericServiceException(e);
        }

        gc.setEgcInfo( dto.toModel() );
        inventoryRepo.save( gc );

        return dto;
    }

    @Transactional
    @Override
    public Boolean updatePin( String barcode, String oldPin, String pin ) {
    	GiftCardInventory gc = inventoryRepo.findByBarcode( barcode );
    	if ( null == gc || BooleanUtils.isNotTrue( gc.getIsEgc() ) ) {
			throw GiftCardServiceResolvableException.giftCardNotFoundException( barcode );
    	}

        try {
        	if ( null == gc.getEgcInfo() ) {
        		gc.setEgcInfo( new EGiftCardInfo() );
        	}

        	if ( !StringUtils.equalsIgnoreCase( SimpleEncrytor.getInstance().decrypt( gc.getEgcInfo().getPin() ), oldPin ) 
        			&& !StringUtils.equalsIgnoreCase( SimpleEncrytor.getInstance().decrypt( gc.getEgcInfo().getAltPin() ), oldPin ) ) {
    			throw GiftCardServiceResolvableException.invalidGiftCardOldPinException();
        	}
        	gc.getEgcInfo().setPin( SimpleEncrytor.getInstance().encrypt( pin ) );
            inventoryRepo.save( gc );
        	return true;
        } 
        catch ( SimpleEncrytor.EncryptDecryptException e ) {
            throw new GenericServiceException(e);
        }
    }

    @Override
    public Boolean validatePin( String barcode, String pin ) {
        GiftCardInventory gc = inventoryRepo.findByBarcode(barcode);

        if (null == gc) {
            throw GiftCardServiceResolvableException.giftCardNotFoundException(barcode);
        }
        if (BooleanUtils.isNotTrue(gc.getIsEgc())) {
            throw GiftCardServiceResolvableException.notAnElectronicGiftCard();
        }

        try {
            if (null != gc.getEgcInfo()) {
                if (StringUtils.equalsIgnoreCase(SimpleEncrytor.getInstance().decrypt(gc.getEgcInfo().getPin()), pin)) {
                    return true;
                }
            }
        } catch (SimpleEncrytor.EncryptDecryptException e) {
            throw new GenericServiceException(e);
        }

        return false;
    }

    @Override
    public Set<GiftCardInfo> inquire( String[] cardNos ) {
    	Set<GiftCardInfo> gcSet = Sets.newHashSet();
    	Iterable<GiftCardInventory> gcs = inventoryRepo.findAll( QGiftCardInventory.giftCardInventory.barcode.in( cardNos ) );
    	for ( GiftCardInventory gc : gcs ) {
    		gcSet.add( GiftCardInfo.convert( gc ) );
    	}
        return gcSet;
    }

    @Override
    public EGiftCardInfoDto get( String barcode ) {
    	GiftCardInventory gc = inventoryRepo.findByBarcode( barcode );
        return null == gc? null : new EGiftCardInfoDto( gc );
    }

	@Override
	public String encryptMobileUpdate( String barcode, String newMobileNo ) {
		try {
			return SimpleEncrytor.getInstance().encrypt( barcode + ENC_SPLITTER + newMobileNo );
		} 
		catch ( SimpleEncrytor.EncryptDecryptException e ) {
			throw new GenericServiceException(e);
		}
	}

	@Override
	public String proceedMobileUpdate( String encryptedNewMobileNo ) {
		try {
			String barcodeAndMobileNo = SimpleEncrytor.getInstance().decrypt( encryptedNewMobileNo );
			String[] arBarcodeAndMobileNo = barcodeAndMobileNo.split( ENC_SPLITTER );
	    	GiftCardInventory gc = inventoryRepo.findByBarcode( arBarcodeAndMobileNo[0] );
	    	if ( null == gc ) {
	    		return null;
	    	}
	    	gc.getEgcInfo().setMobileNo( arBarcodeAndMobileNo[1] );
	    	inventoryRepo.save( gc );
			return arBarcodeAndMobileNo[1];
		}
		catch ( SimpleEncrytor.EncryptDecryptException e ) {
			return null;
		}
	}

	@Override
	public Set<EGiftCardProfileDto> retrieveAvailableProfiles( String location ) {
		QGiftCardInventory qGc = QGiftCardInventory.giftCardInventory;
		List<EGiftCardProfileDto> gcProfiles = new JPAQuery( em )
			.from( qGc )
			.where( BooleanExpression.allOf( 
					qGc.isEgc.isTrue(),
					qGc.status.eq( GiftCardInventoryStatus.IN_STOCK ),
					qGc.location.equalsIgnoreCase( location ) ) )
			.groupBy( qGc.productCode, qGc.productName, qGc.faceValue )
			.list( ConstructorExpression.create( EGiftCardProfileDto.class, qGc.productCode, qGc.productName, qGc.faceValue, qGc.count() ) );
		return Sets.newHashSet(gcProfiles);
	}

	@Override
	public Set<GiftCardInfo> preactivate( GiftCardTransactionInfo<?> txInfo, String productCode, Long quantity ) {
            if (quantity > MAX_QUANTITY) {
                throw GiftCardServiceResolvableException.maximumQuantityReached(MAX_QUANTITY);
            }

		QGiftCardInventory qGc = QGiftCardInventory.giftCardInventory;
		BooleanExpression expr =  BooleanExpression.allOf( 
				qGc.isEgc.isTrue(),
				qGc.status.eq( GiftCardInventoryStatus.IN_STOCK ),
				qGc.productCode.equalsIgnoreCase( productCode ),
				qGc.location.equalsIgnoreCase( txInfo.getMerchantId() ) );

		List<Long> ids = new JPAQuery( em ).from( qGc ).where( expr ).limit( quantity ).list( qGc.id );
		List<EGiftCardInfoDto> egcs = null;
		if ( CollectionUtils.isEmpty( ids ) ) {
			throw GiftCardServiceResolvableException.noAvailableGiftCardException();
		}
		else if ( ids.size() < quantity ) {
			throw GiftCardServiceResolvableException.insufficientGiftCardException( ids.size() );
		}
		else {
			egcs = new JPAQuery( em ).from( qGc ).where( expr.and( qGc.id.in( ids ) ) )
					.list( ConstructorExpression.create( EGiftCardInfoDto.class, qGc.series, qGc.barcode, qGc.productCode, qGc.productName, qGc.faceValue, qGc.status, 
							qGc.profile.allowPartialRedeem, qGc.profile.allowReload ) );
	        new JPAUpdateClause( em, qGc ).set( qGc.status, GiftCardInventoryStatus.PREALLOCATED_EGC )
	        	.where( expr.and( qGc.id.in( ids ) ) )
	        	.execute();
	        em.clear();
	        em.flush();
		}

		SortedSet<GiftCardInfo> cardInfos = new TreeSet<GiftCardInfo>( new Comparator<GiftCardInfo>() {
            @Override
            public int compare(GiftCardInfo a, GiftCardInfo b) {
                return a.getSeries().compareTo(b.getSeries());
            }
        });
		cardInfos.addAll( egcs );

		return cardInfos;//new HashSet<GiftCardInfo>( egcs );
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	@Override
	public Set<GiftCardInfo> deallocate( GiftCardTransactionInfo<?> txInfo, Set<String> cards ) {
		QGiftCardInventory qGc = QGiftCardInventory.giftCardInventory;
		BooleanExpression expr =  BooleanExpression.allOf( 
				qGc.isEgc.isTrue(),
				qGc.location.equalsIgnoreCase( txInfo.getMerchantId() ),
				qGc.barcode.in( cards ) );

		if ( CollectionUtils.isNotEmpty( cards ) ) {
	        new JPAUpdateClause( em, qGc ).set( qGc.status, GiftCardInventoryStatus.IN_STOCK )
	        	.where( BooleanExpression.allOf( expr, qGc.status.eq( GiftCardInventoryStatus.PREALLOCATED_EGC ) ) ).execute();
	        em.clear();
	        em.flush();
	
			List gcs = new JPAQuery( em )
				.from( qGc )
				.where( expr )
				.list( ConstructorExpression.create( EGiftCardInfoDto.class, qGc.series, qGc.barcode, qGc.productCode, qGc.productName, qGc.faceValue, qGc.status, 
						qGc.profile.allowPartialRedeem, qGc.profile.allowReload ) );
			return new HashSet<GiftCardInfo>( gcs );
		} 
		return new HashSet<GiftCardInfo>();
	}

	@Override
	public List<GiftCardInfo> prepareMsgs( Set<GiftCardInfo> gcInfos, 
			String emailAd, String mobileNo, String pin, LocalDate birthdate, String pinPattern, String url ) {

		List<GiftCardInfo> egcDtos = Lists.newArrayList();
        for ( GiftCardInfo gcInfo : gcInfos ) {
            if ( BooleanUtils.isTrue( gcInfo.isElectronicGiftCard() ) ) {
                EGiftCardInfoDto dto = new EGiftCardInfoDto();
                dto.setBarcode( gcInfo.getCardNumber() );
            	BeanUtils.copyProperties( gcInfo, dto, new String[]{} );
                dto.setMobileNo( mobileNo );
                dto.setEmail( emailAd );
                dto.setPin( pin );
                dto.setAltPin( DateTimeFormat.forPattern( pinPattern ).print( birthdate )
                        .replace( "-", "" ) );

                StringBuilder sb = new StringBuilder();
                sb.append( messageSource.getMessage("gc.egc.abbr", null, LocaleContextHolder.getLocale() ) + ": " +
                        gcInfo.getCardNumber() + "<br/>" );
                sb.append( messageSource.getMessage( "gc.egc.balance.abbr", null,
                        LocaleContextHolder.getLocale() ) + ": " + gcInfo.getAvailableBalance() + "<br/>" );
                sb.append( messageSource.getMessage("gc.egc.expiration.date.abbr", null,
                        LocaleContextHolder.getLocale() ) + ": " + gcInfo.getExpireDate() + "<br/>" );

                MessageDto smsDto = new MessageDto();
                smsDto.setRecipients( new String[]{mobileNo} );
                smsDto.setMessage( sb.toString() );

                MessageDto emailDto = new MessageDto();
                emailDto.setRecipients( new String[]{emailAd} );
                emailDto.setMessage( sb.toString() );
                emailDto.setMessage1( this.setCodeForQr( url, gcInfo.getCardNumber(), emailAd ) );
                emailDto.setSubject( messageSource.getMessage("gc.egc", null, LocaleContextHolder.getLocale() ) + ": " + gcInfo.getCardNumber() );

                dto.setEmailMsg( emailDto );
                dto.setSmsMsg( smsDto );
                egcDtos.add( dto );
            }
        }
		return egcDtos;
	}



	private String setCodeForQr( String url, String cardNo, String email ) {
        final String SEPARATOR = "|";
        String infoUrl = url + cardNo;
        StringBuilder sb = new StringBuilder();
        sb.append(StringUtils.replace(infoUrl, " ", ""));
        sb.append(SEPARATOR);
        sb.append(messageSource.getMessage("gc.egc.abbr", null, LocaleContextHolder.getLocale()) + ": " + cardNo);
        sb.append(SEPARATOR);
        sb.append(messageSource.getMessage("gc.egc.email.abbr", null, LocaleContextHolder.getLocale()) + ": " + email);
        return sb.toString();
    }

}