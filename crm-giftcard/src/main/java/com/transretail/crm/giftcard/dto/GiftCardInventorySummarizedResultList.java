package com.transretail.crm.giftcard.dto;

import java.util.Collection;

import com.transretail.crm.common.service.dto.response.AbstractResultListDTO;

/**
 * @author Allan G. Ramirez (agramirez@exist.com)
 */
public class GiftCardInventorySummarizedResultList extends AbstractResultListDTO<GiftCardInventorySummarizedDto> {
    public GiftCardInventorySummarizedResultList(Collection<GiftCardInventorySummarizedDto> results) {
        super(results);
    }

    public GiftCardInventorySummarizedResultList(Collection<GiftCardInventorySummarizedDto> results, long totalElements, int pageNo,
        int pageSize) {
        super(results, totalElements, pageNo, pageSize);
    }
}