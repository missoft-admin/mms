create table CRM_SALES_ORD_PYMNT (
        id number(19,0) not null,
        CREATED_BY varchar2(255 char),
        CREATED_DATETIME timestamp,
        LAST_UPDATED_BY varchar2(255 char),
        LAST_UPDATED_DATETIME timestamp,
        PAYMENT_AMT number(19,2),
        PAYMENT_DATE date,
        PAYMENT_DTLS varchar2(255 char),
        SALES_ORDER number(19,0),
        PAYMENT_TYPE varchar2(15 char),
        STATUS varchar2(255 char),
        primary key (id)
    );

alter table CRM_SALES_ORD_PYMNT 
        add constraint FK_ivm5qe2mwrcgdvxbftl864hit 
        foreign key (SALES_ORDER) 
        references CRM_SALES_ORD;

    alter table CRM_SALES_ORD_PYMNT 
        add constraint FK_g5fu5juxr6dvn1g3lcma2dbln 
        foreign key (PAYMENT_TYPE) 
        references CRM_REF_LOOKUP_DTL;
        
        
alter table CRM_SALES_ORD drop column PAYMENT_AMT;
alter table CRM_SALES_ORD drop column PAYMENT_DATE;
alter table CRM_SALES_ORD drop column PAYMENT_TYPE;