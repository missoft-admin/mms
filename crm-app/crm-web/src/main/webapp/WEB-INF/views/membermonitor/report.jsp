<%@ include file="../common/taglibs.jsp"%>

<div id="printReport" class="modal hide  nofly modal-dialog">
    <div class="modal-dialog">
        <c:url value="/report/membermonitor/" var="url"/>
        <form:form id="reportForm" action="${url}">
        <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
              <h4><spring:message code="member_monitor_report" /></h4>
            </div>
            <div class="modal-body form-horizontal">
                <div class="control-group">
                    <label class="control-label"><spring:message code="label_member_monitor_date" /></label>
                    <div class="controls">
                        <input type="text" id="date" />
                    </div>
                </div>
                <div class="control-group">
                    <label class="control-label"><spring:message code="report_type" /></label>
                    <div class="controls">
                        <select name="reportType" id="reportType">
                          <c:forEach items="${reportTypes}" var="reportType">
                            <option value="${reportType.code}"><spring:message code="report_type_${reportType.code}" /></option>
                          </c:forEach>
                        </select>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" id="saveButton" class="btn btn-primary"><spring:message code="label_print"/></button>
                <button type="button" id="formCancel" data-dismiss="modal" class="btn btn-default"><spring:message code="label_cancel"/></button>
            </div>
        </div>
        </form:form>
    </div>
</div>

<script src='<c:url value="/js/bootstrap/bootstrap-datepicker.js"/>' type='text/javascript'><![CDATA[&nbsp; ]]></script>
<link href="<c:url value="/css/bootstrap/datepicker.css" />" rel="stylesheet" />

<style type="text/css">
#reportForm {margin: 0}
</style>

<script type="text/javascript">
$(document).ready(function() {
   reportDialog = $("#printReport");
   reportDialog.modal({
        show: false
   });
   var now = new Date();
   $("#date").datepicker({
		format		:	'dd-mm-yyyy',
		autoclose	:	true,
	});
   $("#date").val(now.getDate() + "-" + (now.getMonth() + 1) + "-" + now.getFullYear());
   $("#saveButton").click(print);
});

function print() {
    var form = $("#reportForm");
    var reportType = $("#reportType").val();
    var date = $("#date").val();
    var url = form.attr('action') + reportType + "/${role}/" + date;
    reportDialog.modal("hide");
    if(reportType == "pdf")
    	window.open( url, '_blank', 'toolbar=0,location=0,menubar=0' );
    else if(reportType == "excel")
  	  	window.location.href = url;
}
</script>