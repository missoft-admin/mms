var reInteger = /^\d+$/
var reLetterOrDigit = /^([a-zA-Z]|\d)$/
var reSignedInteger = /^[+-]?\d+$/
var reFloat = /^((\d+(\.\d*)?)|((\d*\.)?\d+))$/
var reSignedFloat = /^(([+-]?\d+(\.\d*)?)|([+-]?(\d*\.)?\d+))$/

// Returns true if the string is an integer
function isInteger(s) {
    if (isEmpty(s)) {
        return false;
    } else {
        return reInteger.test(s);
    }
}

// True if all characters in string s are numbers; leading + or - allowed.
function isSignedInteger(s) {
    if (isEmpty(s)) {
        return false;
    } else {
        return reSignedInteger.test(s);
    }
}

// Returns true if string is empty or null
function isEmpty(s) {
    return ((s == null) || (s.trim().length == 0))
}

// Returns true if character c is a letter or digit.
function isLetterOrDigit(c) {
    return reLetterOrDigit.test(c)
}

function isFloat(s) {
    return !isEmpty(s) && reFloat.test(s)
}

function isSignedFloat(s) {
    return !isEmpty(s) && reSignedFloat.test(s);
}