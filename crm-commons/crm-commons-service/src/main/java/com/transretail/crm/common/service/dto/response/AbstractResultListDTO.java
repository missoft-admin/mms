package com.transretail.crm.common.service.dto.response;

import java.util.Collection;

import org.springframework.data.domain.Page;

/**
 *
 */
public abstract class AbstractResultListDTO<T> {
    private Collection<T> results;
    private long numberOfElements;
    private long totalElements;
    private boolean hasPreviousPage;
    private boolean hasNextPage;

    public AbstractResultListDTO(Collection<T> results) {
        this.numberOfElements = results.size();
        this.totalElements = results.size();
        this.results = results;
    }

    public AbstractResultListDTO(Page<T> page) {
        this.numberOfElements = page.getNumberOfElements();
        this.totalElements = page.getTotalElements();
        this.results = page.getContent();
        this.hasPreviousPage = page.hasPreviousPage();
        this.hasNextPage = page.hasNextPage();
    }

    public AbstractResultListDTO(Collection<T> results, long totalElements, int pageNo, int pageSize) {
        this.results = results;
        this.numberOfElements = results.size();
        this.totalElements = totalElements;
        this.hasPreviousPage = pageNo > 0 && totalElements > results.size();
        int totalPages = pageSize == 0 ? 1 : (int) Math.ceil((double) totalElements / (double) pageSize);
        this.hasNextPage = pageNo + 1 < totalPages;
    }

    public AbstractResultListDTO(Collection<T> results, long totalElements, boolean hasPreviousPage, boolean hasNextPage) {
        this.results = results;
        this.numberOfElements = results.size();
        this.totalElements = totalElements;
        this.hasPreviousPage = hasPreviousPage;
        this.hasNextPage = hasNextPage;
    }

    public Collection<T> getResults() {
        return results;
    }

    /**
     * Returns the number of elements currently on this page.
     *
     * @return the number of elements currently on this page
     */
    public long getNumberOfElements() {
        return numberOfElements;
    }

    /**
     * Returns the total amount of elements.
     *
     * @return the total amount of elements
     */
    public long getTotalElements() {
        return totalElements;
    }

    public boolean isHasNextPage() {
        return hasNextPage;
    }

    public boolean isHasPreviousPage() {
        return hasPreviousPage;
    }
}
