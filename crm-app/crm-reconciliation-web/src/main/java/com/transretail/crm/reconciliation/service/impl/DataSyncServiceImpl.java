package com.transretail.crm.reconciliation.service.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.google.common.collect.Lists;
import com.transretail.crm.common.service.exception.MessageSourceResolvableException;
import com.transretail.crm.common.util.IOUtils;
import com.transretail.crm.devtool.dto.TableMetadataBean;
import com.transretail.crm.Constants;
import com.transretail.crm.reconciliation.service.DataSyncService;
import com.transretail.crm.reconciliation.service.DbConnectionProvider;
import com.transretail.crm.reconciliation.service.DiscrepancyToolService;
import com.transretail.crm.reconciliation.service.StoreInfoService;

/**
 * @author Allan G. Ramirez (agramirez@exist.com)
 */
@Service
public class DataSyncServiceImpl implements DataSyncService {
    private static final Logger _LOG = LoggerFactory.getLogger(DataSyncServiceImpl.class);
    private static final String THREAD_INTERRUPTED = "posinteg.wait.interrupted";
    private static final String POSINTEG_NOTRESPONDING = "posinteg.notresponding";
    // TODO: Modify if cron schedule for CRM Job in posinteg has changed or perhaps translate cron expression to milliseconds
    private static final long POSINTEG_WAIT_MS = 180000; // posinteg cron is every 2 minutes. 1 minute serves as an allowance
    @Autowired
    private DiscrepancyToolService discrepancyToolService;
    @Autowired
    private StoreInfoService storeInfoService;
    @Autowired
    private DbConnectionProvider connectionProvider;

    /**
     * {@inheritDoc}
     */
    @Override
    public Timestamp syncTxnRecords() throws MessageSourceResolvableException {
        List<TableMetadataBean> beans = Lists.newArrayList();
        try {
            Timestamp lastTxnSyncDate = storeInfoService.getLastTxnSyncDate();

            TableMetadataBean empTxnBean = new TableMetadataBean();
            empTxnBean.setTable("CRM_EMPLOYEE_PURCHASE_TXN");
            empTxnBean.setId("ID");
            empTxnBean.setCreatedDateTime(lastTxnSyncDate != null ? lastTxnSyncDate.toString() : null);
            beans.add(empTxnBean);

            TableMetadataBean pointsBean = new TableMetadataBean();
            pointsBean.setTable("CRM_POINTS");
            pointsBean.setId("ID");
            pointsBean.setCreatedDateTime(lastTxnSyncDate != null ? lastTxnSyncDate.toString() : null);
            beans.add(pointsBean);

            Map<TableMetadataBean, Timestamp> result = discrepancyToolService.getMinCreatedDateOfMissingRecordsInLocalDb(beans);
            Timestamp ts1 = result.get(empTxnBean);
            Timestamp ts2 = result.get(pointsBean);

            Timestamp minTs = null;
            if (ts1 != null && ts2 != null) {
                minTs = ts1.compareTo(ts2) < 0 ? ts1 : ts2;
            } else if (ts1 != null) {
                minTs = ts1;
            } else if (ts2 != null) {
                minTs = ts2;
            }
            if (minTs != null) {
                String storeCode = storeInfoService.getStoreCode();
                Connection con = null;
                Timestamp dbTs = null;
                try {
                    con = connectionProvider.createPosIntegDbConnection();
                    updatePosIntegLastExecutionDate(con, minTs, storeCode, Constants.JOBCONFIG_PUSH.toString());
                    waitForPosIntegUpdate();
                    dbTs = getPosIntegLastExecutionDate(con, storeCode, Constants.JOBCONFIG_PUSH.toString());
                } finally {
                    IOUtils.INSTANCE.close(con, false);
                }

                try {
                    if (dbTs.getTime() == minTs.getTime()) {
                        throw new MessageSourceResolvableException(POSINTEG_NOTRESPONDING, null,
                            "Last execution date of " + Constants.JOBCONFIG_PUSH + " wasn't updated. Maybe POSINTEG is down.");
                    } else {
                        return dbTs;
                    }
                } finally {
                    storeInfoService.updateLastTxnSyncDate(dbTs);
                }
            }
            return minTs;
        } catch (SQLException e) {
            _LOG.error(Constants.JDBC_ERROR_DEFAULT_MESSAGE.toString(), e);
            throw new MessageSourceResolvableException(Constants.JDBC_ERROR_CODE.toString(), new String[]{e.getMessage()},
                Constants.JDBC_ERROR_DEFAULT_MESSAGE.toString());
        }
    }

    @Override
    public Timestamp syncReferenceRecords() throws MessageSourceResolvableException {
        List<TableMetadataBean> beans = Lists.newArrayList();
        try {
            Timestamp lastRefSyncDate = storeInfoService.getLastRefSyncDate();

            TableMetadataBean bean = new TableMetadataBean();
            bean.setTable("CRM_ADDRESS");
            bean.setId("ID");
            bean.setCreatedDateTime(lastRefSyncDate != null ? lastRefSyncDate.toString() : null);
            beans.add(bean);

            Map<TableMetadataBean, Timestamp> result = discrepancyToolService.getMinCreatedDateOfMissingRecordsInLocalDb(beans);
            Timestamp ts = result.get(bean);
            if (ts != null) {
                String storeCode = storeInfoService.getStoreCode();
                Timestamp dbTs = null;
                Connection con = null;
                try {
                    con = connectionProvider.createPosIntegDbConnection();
                    updatePosIntegLastExecutionDate(con, ts, storeCode, Constants.JOBCONFIG_PULL.toString());
                    waitForPosIntegUpdate();
                    dbTs = getPosIntegLastExecutionDate(con, storeCode, Constants.JOBCONFIG_PULL.toString());
                } finally {
                    IOUtils.INSTANCE.close(con, false);
                }

                try {
                    if (dbTs.getTime() == ts.getTime()) {
                        throw new MessageSourceResolvableException(POSINTEG_NOTRESPONDING, null,
                            "Last execution date of " + Constants.JOBCONFIG_PULL + " wasn't updated. Maybe POSINTEG is down.");
                    } else {
                        return dbTs;
                    }
                } finally {
                    storeInfoService.updateLastRefSyncDate(dbTs);
                }

            }
            return ts;
        } catch (SQLException e) {
            _LOG.error(Constants.JDBC_ERROR_DEFAULT_MESSAGE.toString(), e);
            throw new MessageSourceResolvableException(Constants.JDBC_ERROR_CODE.toString(), new String[]{e.getMessage()},
                Constants.JDBC_ERROR_DEFAULT_MESSAGE.toString());
        }
    }

    protected void waitForPosIntegUpdate() throws MessageSourceResolvableException {
        try {
            Thread.sleep(POSINTEG_WAIT_MS); // Wait for pos integ to do the job
        } catch (InterruptedException e) {
            throw new MessageSourceResolvableException(THREAD_INTERRUPTED, null,
                POSINTEG_WAIT_MS + " milliseconds wait for posinteg update was interrupted. ", e);
        }
    }

    protected int updatePosIntegLastExecutionDate(Connection con, final Timestamp lastExecutionDate, final String storeCode,
        String jobConfig) throws SQLException {
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            ps = con.prepareStatement("UPDATE POSINTG_ROUTECONFIG " +
                "SET LAST_EXECUTION_DATE = ? WHERE JOB_CONFIGURATION = ? AND STORE_CODE = ?");
            ps.setTimestamp(1, lastExecutionDate);
            ps.setString(2, jobConfig);
            ps.setString(3, storeCode);
            int result = ps.executeUpdate();
            return result;
        } catch (SQLException e) {
            con.rollback();
            throw e;
        } finally {
            IOUtils.INSTANCE.close(rs);
            IOUtils.INSTANCE.close(ps);
        }
    }

    private Timestamp getPosIntegLastExecutionDate(final Connection con, final String storeCode, String jobConfig) throws
        SQLException {
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            ps = con.prepareStatement("SELECT LAST_EXECUTION_DATE FROM POSINTG_ROUTECONFIG " +
                "WHERE JOB_CONFIGURATION = ? AND STORE_CODE = ?");
            ps.setString(1, jobConfig);
            ps.setString(2, storeCode);
            rs = ps.executeQuery();
            rs.next();
            return rs.getTimestamp(1);
        } finally {
            IOUtils.INSTANCE.close(rs);
            IOUtils.INSTANCE.close(ps);
        }
    }

}
