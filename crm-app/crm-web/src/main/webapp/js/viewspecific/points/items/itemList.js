$(document).ready( function() {

	var $theItemDialog = $( "#content_itemList" );
	var $theItemList = $( "#list_item" );



	initDialog();
	initDataTable();




	function initDialog() {

		$theItemDialog.modal({ show : false });
		$theItemDialog.on( "show", function(e)  {
			if($theItemList.data( "txn-no" ) != null && $theItemList.data( "txn-no" ) != "") {
				var formDataObject = new Object();
			      formDataObject.posTransactionNo = $theItemList.data( "txn-no" );
			      $theItemList.ajaxDataTable( 'search', formDataObject );
			}
		      
		});
	}

	function initDataTable() {

		var theHeaders = { 
			itemCode: $theItemList.data( "hdr-itemcode" ),
			pluId: $theItemList.data( "hdr-pluid" ),
			sku: $theItemList.data( "hdr-sku" ),
			itemDesc: $theItemList.data( "hdr-itemdesc" ),
			itemPrice: $theItemList.data( "hdr-itemprice" ),
			quantity: $theItemList.data( "hdr-quantity" ),
			totalPrice: $theItemList.data( "hdr-totalprice" ),
			salesType: $theItemList.data( "hdr-salestype" )
		};

		$theItemList.ajaxDataTable({
			'autoload' 		: true,
			'ajaxSource' 	: $theItemList.data( "url" ),
			'columnHeaders' : [	
		        //theHeaders['pluId'],
		        theHeaders['sku'],
		        theHeaders['itemDesc'],
		        theHeaders['itemPrice'],
			 	theHeaders['quantity'],
			 	theHeaders['totalPrice'],
			 	theHeaders['salesType']
			],
			'modelFields' 	: [
			 	//{ name 	: 'pluId', sortable : false },
			 	{ name 	: 'sku', sortable : false },
			 	{ name 	: 'itemDesc', sortable : false },
			 	{ name 	: 'itemPrice', sortable : false },
			 	{ name 	: 'quantity', sortable : false },
			 	{ name 	: 'totalPrice', sortable : false },
			 	{ name 	: 'salesType', sortable : false }
			 ]
		});
	}
	
});