package com.transretail.crm.loyalty.dto;

import org.apache.commons.lang3.StringUtils;

import com.transretail.crm.core.entity.lookup.LookupDetail;
import com.transretail.crm.loyalty.entity.ManufactureOrder;
import com.transretail.crm.loyalty.entity.ManufactureOrderTier;


public class ManufactureOrderTierDto {

	private LookupDetail name;

    private String companyCode;
    
    private String companyDesc;
	
	private String startingSeries;
	
	private Long quantity;
	
	private Long received;
	
	private Long manufactureOrderId;
	
	private String batchNo;
	
	public ManufactureOrderTierDto() {}
	
	public ManufactureOrderTierDto(ManufactureOrderTier orderTier) {
//		BeanUtils.copyProperties(orderTier, this);
        name = orderTier.getName();
        if (orderTier.getCompany() != null) {
            companyCode = orderTier.getCompany().getCode();
            companyDesc = orderTier.getCompany().getDescription();
        }
        startingSeries = orderTier.getStartingSeries();
        quantity = orderTier.getQuantity();
        if (orderTier.getManufactureOrder() != null) {
            manufactureOrderId = orderTier.getManufactureOrder().getId();
        }
    }
	
	public ManufactureOrderTier toModel() {
//        BeanUtils.copyProperties(this, tier);
		ManufactureOrderTier tier = new ManufactureOrderTier();
        tier.setName(name);
        tier.setCompany(new LookupDetail(companyCode));
        tier.setStartingSeries(startingSeries);
        tier.setQuantity(quantity);
        if (manufactureOrderId != null) {
            tier.setManufactureOrder(new ManufactureOrder(manufactureOrderId));
        }
        tier.setBatchNo(batchNo);
		return tier;
	}

	public LookupDetail getName() {
		return name;
	}

	public void setName(LookupDetail name) {
		this.name = name;
	}


    public String getCompanyCode() {
        return companyCode;
    }

    public void setCompanyCode(String companyCode) {
        this.companyCode = companyCode;
    }

    public String getStartingSeries() {
		return startingSeries;
	}

	public void setStartingSeries(String startingSeries) {
		this.startingSeries = startingSeries;
	}

	public Long getQuantity() {
		return quantity;
	}

	public void setQuantity(Long quantity) {
		this.quantity = quantity;
	}

    public Long getManufactureOrderId() {
		return manufactureOrderId;
	}

	public void setManufactureOrderId(Long manufactureOrderId) {
		this.manufactureOrderId = manufactureOrderId;
	}

	public String getBatchNo() {
		return batchNo;
	}

	public void setBatchNo(String batchNo) {
		this.batchNo = batchNo;
	}

	public Long getReceived() {
		return received;
	}

	public void setReceived(Long received) {
		this.received = received;
	}

    public String getEndingSeries() {
        if (StringUtils.isNotBlank(startingSeries)) {
            Long startSeries = Long.valueOf(startingSeries);
            return String.format("%012d", startSeries + (quantity != null && quantity > 0 ? quantity - 1 : 0));
        }
        return "";
    }

	public String getCompanyDesc() {
		return companyDesc;
	}

	public void setCompanyDesc(String companyDesc) {
		this.companyDesc = companyDesc;
	}

    
}
