var MarketingChannel = null;
var MarketingChannelCommon = null;

$(document).ready( function() {

	initLinks();



	function initLinks() {
		$( "#link_draftEmail" ).click( function(e) {
			e.preventDefault();
			EmailDraftForm.create( $(this).data( "href" ), $(this).data( "modal-header" ) );
		});
		$( "#link_draftSms" ).click( function(e) {
			e.preventDefault();
			SmsDraftForm.create( $(this).data( "href" ), $(this).data( "modal-header" ) );
		});
	}



	MarketingChannel = {
		loadEmailDraftForm	: function( resp ) { 
			$( "#form_emailDraft" ).html( resp );
			EmailDraftForm.show();
		},
		loadSmsDraftForm	: function( resp ) { 
			$( "#form_smsDraft" ).html( resp );
			SmsDraftForm.show();
		},
		setResetTimepicker	: function( isSet ) {
			if ( isSet ) {
				$( ".bootstrap-timepicker-widget" ).find( "a[data-action='incrementMinute']" ).hide();
				$( ".bootstrap-timepicker-widget" ).find( "a[data-action='decrementMinute']" ).hide();
				$( ".bootstrap-timepicker-widget" ).find( "input.bootstrap-timepicker-minute" ).attr( "readonly", true );
				$( ".bootstrap-timepicker-widget" ).find( "input.bootstrap-timepicker-minute" ).val( "00" );
				$( ".bootstrap-timepicker-widget" ).find( "input.bootstrap-timepicker-minute" ).attr( "class", "dummy-class" );

				$( ".bootstrap-timepicker-widget" ).find( "a[data-action='incrementSecond']" ).hide();
				$( ".bootstrap-timepicker-widget" ).find( "a[data-action='decrementSecond']" ).hide();
				$( ".bootstrap-timepicker-widget" ).find( "input.bootstrap-timepicker-second" ).attr( "readonly", true );
				$( ".bootstrap-timepicker-widget" ).find( "input.bootstrap-timepicker-second" ).val( "00" );
				$( ".bootstrap-timepicker-widget" ).find( "input.bootstrap-timepicker-second" ).attr( "class", "dummy-class" );
			}
			else {
				//$( ".bootstrap-timepicker-widget" ).find( "input.bootstrap-timepicker-hour" ).val( "00" );
			}
		}
	};

	MarketingChannelCommon = {
        reset     : function( ele, isEmpty ) {
            $( ele ).find(':input').each( function() {
                switch(this.type) {
                    case 'password':
                    case 'select-one':
                    case 'text':
                    case 'textarea':
                    case 'file':
                    case 'hidden': $(this).val(''); break;
                    case 'select-multiple': isEmpty? $(this).empty() : $(this).val(""); break;
                    case 'checkbox':
                    case 'radio': this.checked = false;
                }
            });
        }	
	};
});