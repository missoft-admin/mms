package com.transretail.crm.report.template.impl;

import com.google.common.collect.ImmutableMap;
import com.transretail.crm.common.reporting.exception.ReportException;
import com.transretail.crm.common.reporting.jasper.JRProcessor;
import com.transretail.crm.common.reporting.service.ReportService;
import com.transretail.crm.report.template.CommonReportFilter;
import com.transretail.crm.report.template.ReportTemplate;
import java.io.File;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.sql.SQLException;
import java.util.Locale;
import javax.annotation.Resource;
import javax.sql.DataSource;
import net.sf.jasperreports.engine.JRParameter;
import org.apache.commons.io.FileUtils;
import org.h2.tools.RunScript;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.context.support.MessageSourceResourceBundle;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;


/**
 *
 * @author Monte Cillo Co (mco@exist.com)
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath*:/META-INF/spring/applicationContext*.xml",
    "/META-INF/spring/applicationContext-test.xml"})
@Transactional
public class CustomerCountReportIT {

    private static final Logger logger = LoggerFactory.getLogger(CustomerCountReportIT.class);

    @Autowired
    private ReportService reportService;
    @Autowired
    private MessageSource messageSource;
    @Resource(name = "customerCountReport")
    private ReportTemplate report;
    @Autowired
    private DataSource ds;

    @Before
    public void setup() throws SQLException {
        InputStream resource = Thread.currentThread().getContextClassLoader()
                .getResourceAsStream("scripts/customer-group-lookup.sql");
        RunScript.execute(ds.getConnection(), new InputStreamReader(resource));
        
        InputStream resource2 = Thread.currentThread().getContextClassLoader()
                .getResourceAsStream("scripts/customer-count-report-data.sql");

        RunScript.execute(ds.getConnection(), new InputStreamReader(resource2));
    }

    @Test
    public void generateReport() throws ReportException {
        ImmutableMap<String, String> param = ImmutableMap.of(
                CommonReportFilter.YEAR, "2014");
        JRProcessor jrProcessor = report.createJrProcessor(param);
//        Assert.assertFalse(((NoDataFoundSupport)report).isEmpty(param));
//        Assert.assertFalse("Report has no pages, expecting atleast 1 page.", jrProcessor.getJasperPrints().isEmpty());
        File testClassesDir = FileUtils.toFile(Thread.currentThread().getContextClassLoader().getResource(""));
        File output = new File(testClassesDir, "customer-count-report-output.pdf");
        logger.debug("Output location {}", output.getAbsolutePath());
        output.delete();
        Locale locale = LocaleContextHolder.getLocale();
        jrProcessor.addParameter(JRParameter.REPORT_LOCALE, locale);
        jrProcessor.addParameter(JRParameter.REPORT_RESOURCE_BUNDLE,
                new MessageSourceResourceBundle(messageSource, locale));
        reportService.exportPdfReport(jrProcessor, output);

        double bytes = output.length();
        double kilobytes = (bytes / 1024);
        logger.debug("Kilobytes: {}", kilobytes);
    }

}
