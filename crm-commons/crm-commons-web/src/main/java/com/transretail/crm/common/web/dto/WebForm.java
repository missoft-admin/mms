package com.transretail.crm.common.web.dto;

/**
 * @author aco
 */
public interface WebForm<TARGET> extends WebDto<TARGET> {

    WebFormInfo getWebFormInfo();

}
