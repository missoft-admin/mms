package com.transretail.crm.core.repo.custom;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.transretail.crm.core.entity.UserRoleModel;

public interface UserRoleRepoCustom {
    
    @Query("SELECT a FROM UserRoleModel a WHERE a.id IN ( :ids )")
    List<UserRoleModel> findByIdSet(@Param("ids") List<String> inIds);

    @Query("SELECT a from UserRoleModel a WHERE a.id = :id")
    List<UserRoleModel> findByCode(@Param("id") String inCode );
}
