package com.transretail.crm.giftcard.entity;

import java.io.Serializable;

/**
 *
 * @author Monte Cillo Co (mco@exist.com)
 */
public class PhysicalCountGiftCardStatusId implements Serializable {

    private long summary;
    private long inventory;

    public PhysicalCountGiftCardStatusId() {
    }

    public PhysicalCountGiftCardStatusId(long summary, long inventory) {
	this.summary = summary;
	this.inventory = inventory;
    }

    public long getSummary() {
	return summary;
    }

    public void setSummary(long summary) {
	this.summary = summary;
    }

    public long getInventory() {
	return inventory;
    }

    public void setInventory(long inventory) {
	this.inventory = inventory;
    }

    @Override
    public int hashCode() {
	int hash = 7;
	hash = 79 * hash + (int) (this.summary ^ (this.summary >>> 32));
	hash = 79 * hash + (int) (this.inventory ^ (this.inventory >>> 32));
	return hash;
    }

    @Override
    public boolean equals(Object obj) {
	if (obj == null) {
	    return false;
	}
	if (getClass() != obj.getClass()) {
	    return false;
	}
	final PhysicalCountGiftCardStatusId other = (PhysicalCountGiftCardStatusId) obj;
	if (this.summary != other.summary) {
	    return false;
	}
	if (this.inventory != other.inventory) {
	    return false;
	}
	return true;
    }

}
