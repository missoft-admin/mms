create table CRM_GC_REFUND (
        id number(19,0) not null,
        CREATED_BY varchar2(255 char),
        CREATED_DATETIME timestamp,
        LAST_UPDATED_BY varchar2(255 char),
        LAST_UPDATED_DATETIME timestamp,
        ENDING_SERIES varchar2(255 char),
        HANDLING_FEE number(19,2),
        REFUND_NO varchar2(255 char),
        STARTING_SERIES varchar2(255 char),
        STATUS varchar2(255 char),
        PROFILE number(19,0),
        RTN_REC number(19,0) not null,
        primary key (id)
    );


alter table CRM_GC_REFUND 
        add constraint FK_5wmln7mw8qtdmkf0ufh0ggmur 
        foreign key (PROFILE) 
        references CRM_GC_PROD_PROFILE;

    alter table CRM_GC_REFUND 
        add constraint FK_964x31hgv4d9jk4nkdn1w5j0u 
        foreign key (RTN_REC) 
        references CRM_GC_RETURN;