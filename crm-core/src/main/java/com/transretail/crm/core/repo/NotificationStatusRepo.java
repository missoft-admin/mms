package com.transretail.crm.core.repo;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.transretail.crm.common.repo.CrmQueryDslPredicateExecutor;
import com.transretail.crm.core.entity.Notification;
import com.transretail.crm.core.entity.NotificationStatus;
import com.transretail.crm.core.entity.UserModel;

@Repository
public interface NotificationStatusRepo extends CrmQueryDslPredicateExecutor<NotificationStatus, Long>{
	List<NotificationStatus> findByUser(UserModel user);
	List<NotificationStatus> findByNotification(Notification notification);
}
