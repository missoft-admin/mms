package com.transretail.crm.common.repo;

import static org.springframework.data.querydsl.QueryDslUtils.QUERY_DSL_PRESENT;

import java.io.Serializable;
import java.lang.reflect.Field;

import javax.persistence.EntityManager;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.support.JpaEntityInformation;
import org.springframework.data.jpa.repository.support.JpaRepositoryFactory;
import org.springframework.data.jpa.repository.support.LockModeRepositoryPostProcessor;
import org.springframework.data.jpa.repository.support.QueryDslJpaRepository;
import org.springframework.data.jpa.repository.support.SimpleJpaRepository;
import org.springframework.data.querydsl.QueryDslPredicateExecutor;
import org.springframework.data.repository.core.RepositoryMetadata;

/**
 * @author Allan G. Ramirez (agramirez@exist.com)
 */
public class CrmJpaRepositoryFactory extends JpaRepositoryFactory {
    private static final Logger _LOG = LoggerFactory.getLogger(CrmJpaRepositoryFactory.class);
    private LockModeRepositoryPostProcessor lockModePostProcessor;

    public CrmJpaRepositoryFactory(EntityManager entityManager) {
        super(entityManager);
        try {
            Field field = JpaRepositoryFactory.class.getDeclaredField("lockModePostProcessor");
            field.setAccessible(true);
            lockModePostProcessor = (LockModeRepositoryPostProcessor) field.get(this);
        } catch (Exception e) {
            _LOG.error("Failed to initialize CrmJpaRepositoryFactory", e);
        }
    }

    /**
     * Callback to create a {@link org.springframework.data.jpa.repository.JpaRepository} instance with the given {@link EntityManager}
     *
     * @param <T>
     * @param <ID>
     * @param entityManager
     * @return
     * @see #getTargetRepository(RepositoryMetadata)
     */
    @Override
    @SuppressWarnings({"unchecked", "rawtypes", "unused"})
    protected <T, ID extends Serializable> JpaRepository<?, ?> getTargetRepository(RepositoryMetadata metadata,
        EntityManager entityManager) {

        Class<?> repositoryInterface = metadata.getRepositoryInterface();
        JpaEntityInformation<?, Serializable> entityInformation = getEntityInformation(metadata.getDomainType());

        SimpleJpaRepository<?, ?> repo = null;
        if (isCrmQueryDslExecutor(repositoryInterface)) {
            repo = new CrmQueryDslJpaRepository(entityInformation, entityManager);
        } else if (isQueryDslExecutor(repositoryInterface)) {
            repo = new QueryDslJpaRepository(entityInformation, entityManager);
        } else {
            repo = new SimpleJpaRepository(entityInformation, entityManager);
        }
        repo.setLockMetadataProvider(lockModePostProcessor.getLockMetadataProvider());

        return repo;
    }

    @Override
    protected Class<?> getRepositoryBaseClass(RepositoryMetadata metadata) {
        Class<?> repositoryInterface = metadata.getRepositoryInterface();
        if (isCrmQueryDslExecutor(repositoryInterface)) {
            return CrmQueryDslJpaRepository.class;
        } else if (isQueryDslExecutor(repositoryInterface)) {
            return QueryDslJpaRepository.class;
        } else {
            return SimpleJpaRepository.class;
        }
    }

    private boolean isCrmQueryDslExecutor(Class<?> repositoryInterface) {
        return QUERY_DSL_PRESENT && CrmQueryDslPredicateExecutor.class.isAssignableFrom(repositoryInterface);
    }

    private boolean isQueryDslExecutor(Class<?> repositoryInterface) {
        return QUERY_DSL_PRESENT && QueryDslPredicateExecutor.class.isAssignableFrom(repositoryInterface);
    }
}
