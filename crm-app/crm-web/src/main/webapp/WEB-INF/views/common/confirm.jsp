<%@ include file="../common/taglibs.jsp" %>

<div class="modal hide  groupModal nofly span4" id="confirmbox" tabindex="-1" role="dialog" aria-hidden="true" style="top: 40%; left: 50%;">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4><span id="confirmMessage"></span></h4>
      </div>
      <div class="modal-footer">
        <button type="button" id="confirmTrue" class="btn btn-primary">OK</button>
        <button type="button" id="confirmFalse" class="btn btn-default"><spring:message code="label_cancel"/></button>
      </div>
      <%-- <div class="modal-footer container-fluid">
      	<div class="row-fluid">
      	<div class="span4 offset4">
        
        <button type="button" id="confirmFalse" class="btn btn-default pull-right" style="margin-left: 5px"><spring:message code="label_cancel"/></button>
      	<button type="button" id="confirmTrue" class="btn btn-primary pull-right">OK</button>
      	</div>
      	</div>
      </div> --%>
    </div>
  </div>
</div>

<style type="text/css">
  <!-- 
  .modal-footer { text-align: center !important; } 
  #confirmbox .modal-header { border-bottom: none; }
  #confirmbox h4 { color: #555; }
  #confirmbox .modal-footer { background: transparent; }
  #confirmbox .modal-footer .btn-primary { background: #fd6566; }
  -->
</style>
<script type="text/javascript">
function getConfirm(confirmMessage,callback){
    confirmMessage = confirmMessage || '';

    $('#confirmbox').modal({show:true,
                            keyboard: false,
    });

    $('#confirmMessage').html(confirmMessage);
    $('#confirmFalse').unbind("click").click(function(){
        $('#confirmbox').modal('hide');
        if (callback) callback(false);
    });
    $('#confirmTrue').unbind("click").click(function(){
        $('#confirmbox').modal('hide');
        if (callback) callback(true);
    });
}  

/** USAGE
getConfirm('Are you sure?',function(return) {
	if(return)
  		do somwting here
});

*/
</script>