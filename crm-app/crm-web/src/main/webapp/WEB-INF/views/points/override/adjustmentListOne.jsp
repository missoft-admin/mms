<%@ include file="../../common/taglibs.jsp" %>

<style type="text/css">
<!--
  #content_adjustment .dataTable tr td:first-child {
    min-width: 100px;
    border-right: 2px solid #CCCCCC;
  }
  #content_adjustment .table tbody tr td:last-child {
      border-left: none;
  }
-->
</style>

<div id="content_adjustmentList">

  <sec:authorize var="isMemberMgmt" ifAnyGranted="ROLE_CS, ROLE_CSS" />
  <c:if var="memberMgmt" test="${ ( not empty overrideType and overrideType == member and isMemberMgmt ) or ( empty overrideType and isMemberMgmt ) }" />
  <sec:authorize var="isEmployeeMgmt" ifAnyGranted="ROLE_HR, ROLE_HRS" />
  <c:if var="employeeMgmt" test="${ ( not empty overrideType and overrideType == employee and isEmployeeMgmt ) or ( empty overrideType and isEmployeeMgmt ) }" />
  

  <sec:authorize var="isStaff" ifAnyGranted="ROLE_HR, ROLE_CS">
    <spring:message var="processLink_delete" code="label_delete"/>
    <spring:message var="confirmMsg_delete" code="entity_delete_confirm"/>
    <c:set var="processStatus_delete" value="${statusDelete}" />
    <spring:message var="editLink" code="label_edit"/>
  </sec:authorize>
  <sec:authorize var="isHead" ifAnyGranted="ROLE_HRS, ROLE_CSS">
    <spring:message var="processLink_approve" code="label_approve"/>
    <spring:message var="confirmMsg_approve" code="points_msg_confirm_approve"/>
    <c:set var="processStatus_approve" value="${statusApprove}" />
    <spring:message var="processLink" code="label_process"/>
  </sec:authorize>
  <c:if test="${memberMgmt}"><spring:message var="hdrAccountId" code="points_label_member"/></c:if>
  <c:if test="${employeeMgmt}"><spring:message var="hdrAccountId" code="points_label_employee"/></c:if>
  <c:if test="${ memberMgmt and employeeMgmt }"><spring:message var="hdrAccountId" code="points_label_memberemployee"/></c:if>
  
  <div id="list_adjustment" class="data-table-01"
    data-url="<c:url value="/points/override/list/one/${id}" />" 
    data-hdr-accountid="${hdrAccountId}"
    data-hdr-transactionlocation="<spring:message code="points_label_storeloc"/>" 
    data-hdr-transactiondate="<spring:message code="points_prop_txndate"/>" 
    data-hdr-transactionno="<spring:message code="points_prop_txnno"/>" 
    data-hdr-transactionamount="<spring:message code="points_prop_txnamount"/>" 
    data-hdr-existingpoints="<spring:message code="points_label_pointsbefore"/>" 
    data-hdr-transactionpoints="<spring:message code="points_label_adjustment"/>" 

    data-hdr-requestedby="<spring:message code="points_label_requestor"/>" 
    data-hdr-requestcomment="<spring:message code="points_label_requestcomments"/>" 
    data-hdr-requestdate="<spring:message code="points_label_requestdate"/>"  

    data-hdr-approvedby="<spring:message code="points_label_approver"/>" 
    data-hdr-approvalcomment="<spring:message code="points_label_approvalcomments"/>" 
    data-hdr-approvaldate="<spring:message code="points_label_approvaldate"/>" 

    data-hdr-status="<spring:message code="points_prop_status"/>" 
    data-hdr-approvalreason="<spring:message code="points_prop_approvalreason"/>" 

    data-link-edit="${editLink}" 
    data-url-edit="<c:url value="/points/override/edit/" />"

    data-url-process="<c:url value="/points/override/process/" />" 
    data-msg-confirm="${confirmMsg}" 

    data-role-staff="${isStaff}"
    data-role-head="${isHead}"
    data-highest-appr-point="${highestApprovalPt}" >
  </div>



  <div id="btn_editAdjustment" data-url="<c:url value="/points/override/edit/" />" class="hide">
    <a href="#" class="btn pull-left tiptip icn-edit btn_editAdjustment" title="<c:out value="${editLink}" />" data-id="%ID%" ><c:out value="${editLink}" /></a>
  </div>
  <div id="btn_processAdjustment" data-url="<c:url value="/points/override/process/" />" data-msg-confirm="${confirmMsg_approve}" data-status-process="${processStatus_approve}" class="hide">
    <a href="#" class="btn btn-primary icn-edit tiptip pull-left btn_processAdjustment" title="<c:out value="${processLink}" />" data-id="%ID%" ><c:out value="${processLink}" /></a>
  </div>
  <div id="btn_deleteAdjustment" data-url="<c:url value="/points/override/process/" />" data-msg-confirm="${confirmMsg_delete}" data-status-process="${processStatus_delete}" class="hide">
    <a href="#" class="btn btn-primary icn-delete tiptip pull-left btn_deleteAdjustment" title="<c:out value="${processLink_delete}" />" data-id="%ID%" ><c:out value="${processLink_delete}" /></a>
  </div>
  <jsp:include page="../../common/confirm.jsp" />
  
  <link href="<spring:url value="/css/dataTables.css" />" rel="stylesheet">
  <script src="<spring:url value="/js/jquery/jquery.dataTables.js" />" ></script>
  <script src="<spring:url value="/js/jquery/jquery.dataTables.linkLengthMenu.js" />"></script>
  <script src="<spring:url value="/js/jquery/jquery.dataTables.customPaging.js" />"></script>
  <script src="<spring:url value="/js/jquery/ajax-datatable.js" />" ></script>
  <script src="<spring:url value="/js/common/form2js.js" />" ></script>
  <script src="<spring:url value="/js/common/json2.js" />" ></script>
  <script src="<spring:url value="/js/jquery/jquery.toObject.js" />" ></script>
  <script src="<spring:url value="/js/viewspecific/points/override/adjustmentList.js" />" type="text/javascript"></script>
  <style><!--.el-width-x { width: 80px !important; }--></style>


</div>