package com.transretail.crm.schedule.job.impl;

import java.util.Date;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.transretail.crm.giftcard.service.GiftCardService;
import com.transretail.crm.schedule.job.service.GiftCardTransactionSettlementService;

@Service("gcTxnSettlementService")
public class GiftCardTransactionSettlementServiceImpl implements
		GiftCardTransactionSettlementService {
	@Autowired
	private GiftCardService gcService;
	
	private static final Logger LOGGER = LoggerFactory.getLogger(GiftCardTransactionSettlementServiceImpl.class);
	
	@Override
	@Transactional
	public void settleGiftCardTransactions() {
		LOGGER.error( ":settleGCTransactions:: job executing at " + new Date().toString()  );
		gcService.settleGiftCardTransactions();
		LOGGER.error( ":settleGCTransactions:: job successfully executed at " + new Date().toString()  );
	}

}
