package com.transretail.crm.rest.pos;

import com.transretail.crm.core.repo.ApplicationConfigRepo;
import com.transretail.crm.core.security.service.MD5Service;
import com.transretail.crm.core.security.service.impl.MD5ServiceImpl;
import com.transretail.crm.core.service.*;
import com.transretail.crm.loyalty.service.*;
import com.transretail.crm.rest.common.MemberRestController;
import com.transretail.crm.web.controller.AbstractMockMvcTest;
import org.junit.Test;
import org.junit.runner.RunWith;

import static org.mockito.Mockito.*;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.AnnotationConfigWebContextLoader;
import org.springframework.test.context.web.WebAppConfiguration;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import org.springframework.web.servlet.config.annotation.EnableWebMvc;

import static org.hamcrest.Matchers.is;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.reset;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;

/**
 * @author Allan G. Ramirez (agramirez@exist.com)
 */
@RunWith(SpringJUnit4ClassRunner.class)
@WebAppConfiguration
@ContextConfiguration(loader = AnnotationConfigWebContextLoader.class)
public class MemberRestControllerTest extends AbstractMockMvcTest {
    @Configuration
    @EnableWebMvc
    static class Config {
        @Bean
        public MemberRestController memberRestController() {
            return new MemberRestController();
        }

        @Bean
        public MemberService memberService() {
            return mock(MemberService.class);
        }

        @Bean
        public PromotionService promotionService() {
            return mock(PromotionService.class);
        }

        @Bean
        public MessageSource messageSource() {
            return mock(MessageSource.class);
        }

        @Bean
        public CodePropertiesService codePropertiesService() {
            return mock(CodePropertiesService.class);
        }

        @Bean
        public PointsTxnManagerService pointsManagerService() {
            return mock(PointsTxnManagerService.class);
        }

        @Bean
        public LookupService lookupService() {
            return mock(LookupService.class);
        }

        @Bean
        public MemberGroupService memberGroupService() {
            return mock(MemberGroupService.class);
        }

        @Bean
        public PosTransactionService posTransactionService() {
            return mock(PosTransactionService.class);
        }

        @Bean
        public LoyaltyCardService loyaltyCardService() {
            return mock(LoyaltyCardService.class);
        }

        @Bean
        public LoyaltyCardHistoryService loyaltyCardHistoryService() {
            return mock(LoyaltyCardHistoryService.class);
        }

        @Bean
        public LoyaltyAnnualFeeService loyaltyAnnualFeeService() {
            return mock(LoyaltyAnnualFeeService.class);
        }

        @Bean
        public StoreMemberService storeMemberService() {
            return mock(StoreMemberService.class);
        }

        @Bean
        public MD5Service mD5Service() {
            return spy(new MD5ServiceImpl());
        }

        @Bean
        public ApplicationConfigRepo applicationConfigRepo() {
            return mock(ApplicationConfigRepo.class);
        }

        @Bean
        public RestUrlService restUrlService() {
            return mock(RestUrlService.class);
        }


    }



    @Autowired
    private MemberService memberService;
    @Autowired
    private PromotionService promotionService;
    @Autowired
    private MessageSource messageSource;
    @Autowired
    private CodePropertiesService codePropertiesService;
    @Autowired
    private PointsTxnManagerService pointsManagerService;
    @Autowired
    private LookupService lookupService;
    @Autowired
    private MemberGroupService memberGroupService;
    @Autowired
    private PosTransactionService posTransactionService;
    @Autowired
    private LoyaltyCardService loyaltyCardService;
    @Autowired
    private LoyaltyCardHistoryService loyaltyCardHistoryService;
    @Autowired
    private LoyaltyAnnualFeeService loyaltyAnnualFeeService;

    @Autowired
    private RestUrlService restUrlService;



    @Override
    public void doSetup() {
        reset(memberService);
        reset(promotionService);
        reset(messageSource);
        reset(codePropertiesService);
        reset(pointsManagerService);
        reset((lookupService));
        reset(memberGroupService);
        reset(posTransactionService);
        reset(loyaltyCardService);
        reset(loyaltyCardHistoryService);
        reset(loyaltyAnnualFeeService);
        reset(restUrlService);

    }

    @Test
    public void validatePinTest() throws Exception {
        String id = "id";
        String pin = "pin";
        when(memberService.validatePin(id, pin)).thenReturn(Boolean.TRUE);
        mockMvc.perform(get("/member/validate/pin/{id}/{pin}", id, pin))
            .andExpect(jsonPath("$.type", is("SUCCESS")))
            .andExpect(jsonPath("$.callingAction", is("VALIDATE_PIN")))
            .andExpect(status().isOk());
        verify(memberService).validatePin(id, pin);
    }
}
