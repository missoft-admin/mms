package com.transretail.crm.report.template.impl;

import com.google.common.collect.Maps;
import com.google.common.collect.Sets;
import com.mysema.query.jpa.impl.JPAQuery;
import com.mysema.query.types.Projections;
import com.transretail.crm.common.reporting.jasper.ClasspathFileResolver;
import com.transretail.crm.common.reporting.jasper.DefaultJRProcessor;
import com.transretail.crm.common.reporting.jasper.JRProcessor;
import com.transretail.crm.core.entity.QEmployeePurchaseTxnModel;
import com.transretail.crm.core.entity.QMemberModel;
import com.transretail.crm.core.entity.QPointsTxnModel;
import com.transretail.crm.core.entity.enums.PointTxnType;
import com.transretail.crm.core.entity.enums.ReportType;
import com.transretail.crm.core.entity.enums.TxnStatus;
import com.transretail.crm.core.entity.lookup.QLookupDetail;
import com.transretail.crm.core.entity.lookup.Store;
import com.transretail.crm.core.service.CodePropertiesService;
import com.transretail.crm.core.service.StoreService;
import com.transretail.crm.core.util.FormatterUtil;
import com.transretail.crm.report.template.*;
import com.transretail.crm.report.template.FilterField.DateTypeOption;
import java.util.List;
import java.util.Map;
import java.util.Set;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.JRParameter;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import net.sf.jasperreports.engine.fill.JRGzipVirtualizer;
import org.apache.commons.lang3.StringUtils;
import org.joda.time.DateTimeUtils;
import org.joda.time.YearMonth;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.MessageSourceAware;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.context.support.MessageSourceAccessor;
import org.springframework.stereotype.Service;

/**
 *
 * @author Monte Cillo Co (mco@exist.com)
 */
@Service("employeePurchaseAndRewardsReport")
public class EmployeePurchaseAndRewardsReport implements ReportTemplate, MessageSourceAware, ReportsCustomizer, NoDataFoundSupport {

    @PersistenceContext
    private EntityManager em;
    @Autowired
    private StoreService storeService;
    @Autowired
    private CodePropertiesService codePropertiesService;

    // NOTE: This is the name of the field in EmployeePurchaseAndRewardsReport$ReportBean
    // used in querydsl projection.
    protected static final String REWARD_FIELD = "reward";
    protected static final String PURCHASE_FIELD = "purchase";
    protected static final String YEAR_MONTH_FIELD = "yearMonth";
    protected static final String REG_STORE_CODE_FIELD = "regStoreCode";
    protected static final String ORGANIZATION_FIELD = "organization";
    protected static final String FIRST_NAME_FIELD = "firstName";
    protected static final String LAST_NAME_FIELD = "lastName";
    protected static final String EMPLOYEE_CODE_FIELD = "employeeCode";

    protected static final String TEMPLATE_NAME = "Employee Purchase and Reward Report";
    protected static final String REPORT_NAME = "reports/hr/employee-purchase-and-rewards-report.jasper";
    protected static final String FILTER_DATE_FROM = CommonReportFilter.DATE_FROM;
    protected static final String FILTER_DATE_TO = CommonReportFilter.DATE_TO;
    private static final DateTimeFormatter RAW_YEAR_MONTH_FORMATTER = DateTimeFormat.forPattern("yyyyMM")
            .withLocale(LocaleContextHolder.getLocale());
    private MessageSourceAccessor messageSource;

    @Override
    public String getName() {
        return TEMPLATE_NAME;
    }

    @Override
    public String getDescription() {
        return messageSource.getMessage("employee.purchase_and_rewards.title");
    }

    @Override
    public void setMessageSource(MessageSource messageSource) {
        this.messageSource = new MessageSourceAccessor(messageSource);
    }

    @Override
    public Set<FilterField> getFilters() {
        return Sets.newHashSet(FilterField.createDateField(FILTER_YEAR,
                messageSource.getMessage("employee.purchase_and_rewards.filter.year"), "YYYY", DateTypeOption.YEAR, true));
    }
    protected static final String FILTER_YEAR = "year";

    @Override
    public boolean canView(Set<String> permissions) {
        return permissions.contains("REPORT_EMPLOYEE_PURCHASE_AND_REWARD_SUMMARY_VIEW");
    }

    @Override
    public JRProcessor createJrProcessor(Map<String, String> map) {
        Map<String, Object> parameters = Maps.newHashMap();
        //parameters.put(JRParameter.REPORT_MAX_COUNT, 100);
        String yearString = map.get("year");
        parameters.put(JRParameter.REPORT_VIRTUALIZER, new JRGzipVirtualizer(10));
        parameters.put("SUBTITLE", "Jan - Dec " + yearString);
        DefaultJRProcessor jrProcessor = new DefaultJRProcessor(REPORT_NAME, createDataSource(Integer.parseInt(yearString)));
        jrProcessor.addParameters(parameters);
        jrProcessor.setFileResolver(new ClasspathFileResolver("reports"));
        return jrProcessor;
    }

    @Override
    public ReportType[] getReportTypes() {
        return new ReportType[]{ReportType.EXCEL};
    }

    protected JRDataSource createDataSource(int year) {
        List<ReportBean> datasource = getAllEmployeePurchase(year);
        datasource.addAll(getAllEmployeePoints(year));
        Map<String, String> cache = Maps.newHashMap();
        String[] months = DateTimeUtils.getDateFormatSymbols(
                LocaleContextHolder.getLocale()).getMonths();
        for (ReportBean rb : datasource) {
            final String storeCode = rb.getRegStoreCode();
            String storeName = cache.get(storeCode);
            if (StringUtils.isBlank(storeName)) {
                Store store = storeService.getStoreByCode(storeCode);
                if (store != null) {
                    storeName = store.getName();
                    cache.put(storeCode, storeName);
                }
            }
            rb.setRegStoreDesc(storeName);
        }

        if (datasource.iterator().hasNext()) {
            ReportBean base = datasource.iterator().next();
            for (int i = 1; i <= months.length - 1; i++) {
                ReportBean clone = new ReportBean();
                BeanUtils.copyProperties(base, clone);
		// dont copy the values of purchase and rewards,
                // set it to zero, the crosstab will sum it up to you
                clone.setPurchase(0.0);
                clone.setReward(0.0);
                clone.setYearMonth(year * 100 + i);
                datasource.add(clone);
            }
        }
        return new JRBeanCollectionDataSource(datasource);
    }

    public List<ReportBean> getAllEmployeePoints(int year) {
	//<editor-fold defaultstate="collapsed" desc="query">
        //	select
//  m.id as employee_code,
//  m.last_name as employee_name,
//  dtl.description as organization,
//  m.registered_store,
//  extract(YEAR from p.txn_date) * 100 + extract(MONTH from p.txn_date) as yearMonth,
//  sum(p.txn_points) as reward
//from
//  crm_points p
//left join
//  crm_member m
//  on p.member_id = m.id
//left join
//  crm_ref_lookup_dtl dtl
//  on m.department = dtl.code
//where 
//  extract(YEAR from p.txn_date)  = 2014
//  and p.txn_type='REWARD' and p.status = 'ACTIVE'
//  and m.member_type='MTYP002'
//group by
//  m.id,
//  m.last_name,
//  dtl.description,
//  m.registered_store, 
//  extract(YEAR from p.txn_date) * 100 + extract(MONTH from p.txn_date)); --19
//</editor-fold>
        QMemberModel qm = QMemberModel.memberModel;
        QLookupDetail qldt = QLookupDetail.lookupDetail;
        QPointsTxnModel qp = QPointsTxnModel.pointsTxnModel;
        List<ReportBean> list = createQueryForAllEmployeePoints(year)
                .groupBy(qm.accountId, qm.lastName, qm.firstName,
                        qldt.description, qm.registeredStore,
                        qp.incDateFrom.yearMonth())
                .list(Projections.bean(ReportBean.class,
                                qm.accountId.stringValue().as(EMPLOYEE_CODE_FIELD),
                                qm.lastName.as(LAST_NAME_FIELD),
                                qm.firstName.as(FIRST_NAME_FIELD),
                                qldt.description.as(ORGANIZATION_FIELD),
                                qm.registeredStore.as(REG_STORE_CODE_FIELD),
                                qp.incDateFrom.yearMonth().as(YEAR_MONTH_FIELD),
                                qp.transactionPoints.sum().as(REWARD_FIELD)));
        return list;
    }

    protected JPAQuery createQueryForAllEmployeePoints(int year) {
        QMemberModel qm = QMemberModel.memberModel;
        QLookupDetail qldt = QLookupDetail.lookupDetail;
        QPointsTxnModel qp = QPointsTxnModel.pointsTxnModel;
        final String MEMBER_TYPE_CODE = codePropertiesService.getDetailMemberTypeEmployee();
        JPAQuery query = new JPAQuery(em).from(qp)
                .leftJoin(qp.memberModel, qm)
                .leftJoin(qm.professionalProfile.department, qldt)
                .where(qp.incDateFrom.year().eq(year)
                        .and(qp.transactionType.eq(PointTxnType.REWARD)
                                .and(qp.status.eq(TxnStatus.ACTIVE))
                                .and(qm.memberType.code.eq(MEMBER_TYPE_CODE))));
        
        return query;
    }

    protected List<ReportBean> getAllEmployeePurchase(int year) {
        QMemberModel qm = QMemberModel.memberModel;
        QLookupDetail qldt = QLookupDetail.lookupDetail;
        QEmployeePurchaseTxnModel qpe = QEmployeePurchaseTxnModel.employeePurchaseTxnModel;

        List<ReportBean> list = createQueryForAllEmployeePurchase(year)
                .groupBy(qm.accountId, qm.lastName, qm.firstName,
                        qldt.description, qm.registeredStore,
                        qpe.transactionDateTime.yearMonth())
                .list(Projections.bean(ReportBean.class,
                                qm.accountId.stringValue().as(EMPLOYEE_CODE_FIELD),
                                qm.lastName.as(LAST_NAME_FIELD),
                                qm.firstName.as(FIRST_NAME_FIELD),
                                qldt.description.as(ORGANIZATION_FIELD),
                                qm.registeredStore.as(REG_STORE_CODE_FIELD),
                                qpe.transactionDateTime.yearMonth().as(YEAR_MONTH_FIELD),
                                qpe.transactionAmount.sum().as(PURCHASE_FIELD)));

        return list;
    }

    protected JPAQuery createQueryForAllEmployeePurchase(int year) {
        QMemberModel qm = QMemberModel.memberModel;
        QLookupDetail qldt = QLookupDetail.lookupDetail;
        QEmployeePurchaseTxnModel qpe = QEmployeePurchaseTxnModel.employeePurchaseTxnModel;

        JPAQuery query = new JPAQuery(em).from(qpe)
                .leftJoin(qpe.memberModel, qm)
                .leftJoin(qm.professionalProfile.department, qldt)
                .where(qpe.transactionDateTime.year().eq(year));

        return query;
    }

    @Override
    public Set<Option> customizerOption() {
        return Sets.newHashSet(Option.YEARLY);
    }

    @Override
    public boolean isEmpty(Map<String, String> filter) {
        String yearString = filter.get("year");
        int year = Integer.parseInt(yearString);
        
        return createQueryForAllEmployeePoints(year).notExists() 
                && createQueryForAllEmployeePurchase(year).notExists();
    }

    public static class ReportBean {

        private String employeeCode;
        private String lastName;
        private String firstName;
        private String organization;
        private String regStoreCode;
        private String regStoreDesc;
        private Integer yearMonth;
        private double purchase;
        private double reward;

        public String getEmployeeCode() {
            return employeeCode;
        }

        public void setEmployeeCode(String employeeCode) {
            this.employeeCode = employeeCode;
        }

        public String getEmployeeName() {
            return FormatterUtil.INSTANCE.formatName(lastName, firstName);
        }

        public String getOrganization() {
            return organization;
        }

        public void setOrganization(String organization) {
            this.organization = organization;
        }

        public String getRegStoreCode() {
            return regStoreCode;
        }

        public void setRegStoreCode(String regStoreCode) {
            this.regStoreCode = regStoreCode;
        }

        public String getRegStoreDesc() {
            return regStoreDesc;
        }

        public void setRegStoreDesc(String regStoreDesc) {
            this.regStoreDesc = regStoreDesc;
        }

        public Integer getYearMonth() {
            return yearMonth;
        }

        public YearMonth getYearMonthObject() {
            return YearMonth.parse(String.valueOf(yearMonth), RAW_YEAR_MONTH_FORMATTER);
        }

        public void setYearMonth(Integer yearMonth) {
            this.yearMonth = yearMonth;
        }

        public double getPurchase() {
            return purchase;
        }

        public void setPurchase(double purchase) {
            this.purchase = purchase;
        }

        public double getReward() {
            return reward;
        }

        public void setReward(double reward) {
            this.reward = reward;
        }

        public String getLastName() {
            return lastName;
        }

        public void setLastName(String lastName) {
            this.lastName = lastName;
        }

        public String getFirstName() {
            return firstName;
        }

        public void setFirstName(String firstName) {
            this.firstName = firstName;
        }

    }
}
