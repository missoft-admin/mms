	create table CRM_STORE_GROUP_AUD (
        id bigint not null,
        REV bigint not null,
        REVTYPE tinyint,
        NAME varchar(255),
        STORES varchar(4000),
        primary key (id, REV)
    );

	alter table CRM_STORE_GROUP_AUD 
        add constraint FK_6tf52dp4kcipdb7e4owwoq2th 
        foreign key (REV) 
        references CRM_REVINFO;