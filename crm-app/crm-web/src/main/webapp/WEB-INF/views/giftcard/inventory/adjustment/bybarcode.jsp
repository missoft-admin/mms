<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ include file="../../../common/taglibs.jsp" %>
<div class="modal hide  nofly" id="byBarcodeDialog" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-header">
      <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
      <h4><spring:message code="physical_count_by_barcode_only_label"/></h4>
    </div>
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-body">
	<div id="contentError" class="hide alert alert-error">
	  <button type="button" class="close" onclick="$(this).parent().hide();">&times;</button>
	  <div><form:errors path="*"/></div>
	</div>

	<form:form id="byBarcodeForm" name="byBarcodeForm" modelAttribute="giftCardPhysicalCountForm" action="${pageContext.request.contextPath}/giftcard/inventory/adjustment/adjust" method="POST" cssClass="form-reset form-horizontal" >
	  <div class="control-group">
	    <spring:message code="inventory_prop_location" var="locationStr" />
	    <label for="storeName" class="control-label">${locationStr}</label> 
	    <div class="controls">
	      <sec:authentication var="userLocation" property="principal.inventoryLocation" />
	      <sec:authentication var="userLocationName" property="principal.inventoryLocationName" />

	      <input type="text" name="locationDesc" value="${userLocation} - ${userLocationName}" class="form-control" placeholder="${locationStr}" disabled="disabled" />
	      <input type="hidden" name="location" value="${userLocation}" />
	    </div>
	  </div>
	  <div class="control-group">
	    <spring:message code="mo_tier_prop_barcode" var="barcodeLabel" />
	    <label for="storeName" class="control-label">${barcodeLabel}</label> 
	    <div class="controls">
	      <input type="text" name="barcode" id="barcode-interface" placeholder="${barcodeLabel}" maxlength="20" class="form-control"/>
	    </div>
	  </div>
	  <div class="control-group">
	    <div class="controls">
	      <form:select id="barcodes_select" path="barcodes" class="form-control" size="10"/>
	    </div>
	  </div>
	</form:form>
      </div>
      <div class="modal-footer">
	<spring:message code="label_save" var="btnLabel"/>
        <button type="button" id="countSubmit2" data-loading-text="Saving..." data-label="${btnLabel}" class="btn btn-primary" disabled="disabled">${btnLabel}</button>
        <button type="button" id="countCancel2" class="btn btn-default" data-dismiss="modal"><spring:message code="label_stop"/></button>
      </div>
    </div>
  </div>
</div>



