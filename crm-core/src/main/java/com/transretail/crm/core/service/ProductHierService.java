package com.transretail.crm.core.service;


import java.util.List;

import com.transretail.crm.core.dto.ProductHierDto;


public interface ProductHierService {

	List<ProductHierDto> get(String level);

	List<ProductHierDto> get(String level, Integer idx, Integer size);

	List<ProductHierDto> get(String type, String code);

	List<ProductHierDto> get(List<String> filterCodes, List<String> codes, List<String> levels);

}
