package com.transretail.crm.core.dto;

import com.transretail.crm.core.entity.MemberModel;

/**
 *
 */
public class MemberPointsReportDto {
    private String firstName;
    private String lastName;
    private String accountId;
    private Long totalPoints;
    private String storeCode;
    private String storeName;

    public MemberPointsReportDto(MemberModel memberModel) {
        firstName = memberModel.getFirstName();
        lastName = memberModel.getLastName();
        accountId = memberModel.getAccountId();
        totalPoints = memberModel.getTotalPoints().longValue();
        storeCode = memberModel.getRegisteredStore();
    }
    
    public MemberPointsReportDto() {
    	
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public String getAccountId() {
        return accountId;
    }

    public Long getTotalPoints() {
        return totalPoints;
    }

	public String getStoreCode() {
		return storeCode;
	}

	public String getStoreName() {
		return storeName;
	}
	
	public String getFormattedMemberName() {
		StringBuilder builder = new StringBuilder();
		if(firstName != null) {
			builder.append(firstName).append(" ");
		}
		if(lastName != null) {
			builder.append(lastName);
		}
		return builder.toString();
	}

	public void setStoreName(String storeName) {
		this.storeName = storeName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public void setAccountId(String accountId) {
		this.accountId = accountId;
	}

	public void setTotalPoints(Long totalPoints) {
		this.totalPoints = totalPoints;
	}

	public void setStoreCode(String storeCode) {
		this.storeCode = storeCode;
	}
	
	
    
}
