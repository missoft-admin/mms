<%@include file="../../common/taglibs.jsp" %>


<div id="content_cxProfileList">

  <div id="list_cxProfile" 
      data-url="<c:url value="${listUrl}" />" 
      data-hdr-cxid="<spring:message code="gc.cxprofile.cx.id"/>" 
      data-hdr-cxname="<spring:message code="gc.cxprofile.cx.name"/>" 
      data-hdr-contactperson="<spring:message code="gc.cxprofile.contact.person"/>" 
      data-hdr-contactno="<spring:message code="gc.cxprofile.contact.no"/>" 
      data-hdr-email="<spring:message code="gc.cxprofile.email"/>" 
      data-hdr-cxtype="<spring:message code="gc.cxprofile.cx.type"/>" 
      data-hdr-discounttype="<spring:message code="gc.cxprofile.discount.type"/>" ></div>

  <div id="btns" class="hide">
    <a href="<c:url value="/customer/complaint/form/view/%ID%" />" class="btn pull-left tiptip icn-view viewBtn" 
      title="<spring:message code="label_view" />" ><spring:message code="label_view" /></a>
    <a href="<c:url value="/giftcard/customer/profile/edit/%ID%" />" class="btn pull-left tiptip icn-edit editBtn" 
      title="<spring:message code="label_edit" />" ><spring:message code="label_edit" /></a>
    <a href="<c:url value="/giftcard/customer/profile/notes/%ID%" />" class="btn pull-left tiptip icn-edit notesBtn" 
      title="<spring:message code="gc.cxprofile.notes" />" data-id="%ID%" data-cxname="%CXNAME%" ><spring:message code="gc.cxprofile.notes" /></a>
  </div>


<link href="<spring:url value="/css/dataTables.css" />" rel="stylesheet">
<script src="<spring:url value="/js/jquery/jquery.dataTables.js" />" ></script>
<script src="<spring:url value="/js/jquery/jquery.dataTables.linkLengthMenu.js" />"></script>
<script src="<spring:url value="/js/jquery/jquery.dataTables.customPaging.js" />"></script>
<script src="<spring:url value="/js/jquery/ajax-datatable.js" />" ></script>
<script src="<spring:url value="/js/common/form2js.js" />" ></script>
<script src="<spring:url value="/js/common/json2.js" />" ></script>
<script src="<spring:url value="/js/jquery/jquery.toObject.js" />" ></script>

<script type="text/javascript">
var CxProfileList = null;

$(document).ready( function() {

    var $container = $( "#content_cxProfileList" );
    var $list = $container.find( "#list_cxProfile" );
    var $btns = $container.find( "#btns" );

    initDataTable();

    function initDataTable() {
        $list.ajaxDataTable({
            'autoload'      : true,
            'ajaxSource'    : $list.data( "url" ),
            'aaSorting'     : [[ 0, 'desc' ]],
            'columnHeaders' : [ 
                $list.data( "hdr-cxid" ),
                $list.data( "hdr-cxname" ),
                $list.data( "hdr-contactperson" ),
                $list.data( "hdr-contactno" ),
                $list.data( "hdr-email" ),
                $list.data( "hdr-cxtype" ),
                /* $list.data( "hdr-discounttype" ), */
                { text: "", className : "span5" }
            ],
            'modelFields'  : [
                { name  : 'customerId', sortable : true },
                { name  : 'name', sortable : true },
                { name  : 'contactFirstName', sortable : true, fieldCellRenderer : function (data, type, row) {
                    return ( row.contactFirstName? row.contactFirstName : "" ) + ( row.contactLastName? " " + row.contactLastName : "" );
                }},
                { name  : 'contactNo', sortable : true },
                { name  : 'email', sortable : true },
                { name  : 'customerType', sortable : true, fieldCellRenderer : function (data, type, row) {
                    return row.customerTypeDesc;
                }},
                /* { name  : 'discountType', sortable : true, fieldCellRenderer : function (data, type, row) {
                    return row.discountTypeDesc;
                }}, */
                { customCell : 
                    function ( innerData, sSpecific, json ) {
                        var btns = "";
                        if ( sSpecific == 'display' ) {
                            //btns += $btns.find( ".viewBtn" ).prop( "outerHTML" ).replace( new RegExp( "%ID%", "g" ), json.id );
                            //if ( $constants.data( "is-merchant-sup" ) ) {
                            	  btns += $btns.find( ".editBtn" ).prop( "outerHTML" ).replace( new RegExp( "%ID%", "g" ), json.id );
                                btns += $btns.find( ".notesBtn" ).prop( "outerHTML" )
                                      .replace( new RegExp( "%ID%", "g" ), json.id )
                                      .replace( new RegExp( "%CXNAME%", "g" ), json.name );
                            //}
                        }
                        return btns;
                    }
                }
            ]
        })
        .on( "click", ".viewBtn", view )
        .on( "click", ".editBtn", edit )
        .on( "click", ".notesBtn", viewNotes );
    }

    function view(e) {
        e.preventDefault();
        $.get( $(this).attr( "href" ), function( resp ) {}, "html" );
    }

    function edit(e) {
        e.preventDefault();
        $.get( $(this).attr( "href" ), function( resp ) { CxProfile.loadForm( resp ); }, "html" );
    }

    function viewNotes(e) {
        e.preventDefault();
        CxProfileNotes.show( $(this).data( "id" ), $(this).data( "cxname" ) );
    }

    CxProfileList = {
        reloadTable  : function() { $list.ajaxDataTable( "search" ); },
        filterTable  : function( filterForm, retFunction ) { $list.ajaxDataTable( "search", filterForm, retFunction ); }
    };

});
</script>


</div>