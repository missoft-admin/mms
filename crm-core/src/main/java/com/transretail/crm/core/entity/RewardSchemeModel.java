package com.transretail.crm.core.entity;


import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.transretail.crm.common.web.converter.CurrencySerializer;
import com.transretail.crm.core.entity.enums.Status;
import com.transretail.crm.core.entity.support.CustomAuditableEntity;


@Table(name = "CRM_REWARD_SCHEME")
@Entity
public class RewardSchemeModel extends CustomAuditableEntity<Long> implements Serializable {

    private static final long serialVersionUID = 12121L;

    @Column(name = "VALUE_FROM")
    private Double valueFrom;

    @Column(name = "VALUE_TO")
    private Double valueTo;

    @Column(name = "AMOUNT")
    private Double amount;

	@Column(name = "VALID_PERIOD")
    private int validPeriod;

    @NotNull
    @Column(name = "STATUS", length = 20, nullable = false)
    @Enumerated(EnumType.STRING)
    private Status status = Status.ACTIVE;

    public Double getValueFrom() {
        return valueFrom;
    }
    @JsonInclude(Include.ALWAYS)
	@JsonSerialize(using=CurrencySerializer.class)
    public Double getValueFromFormatted() {
    	return valueFrom;
    }

    public void setValueFrom(Double valueFrom) {
        this.valueFrom = valueFrom;
    }

    public Double getValueTo() {
        return valueTo;
    }
    @JsonInclude(Include.ALWAYS)
	@JsonSerialize(using=CurrencySerializer.class)
    public Double getValueToFormatted() {
    	return valueTo;
    }

    public void setValueTo(Double valueTo) {
        this.valueTo = valueTo;
    }

    public Double getAmount() {
        return amount;
    }

    @JsonInclude(Include.ALWAYS)
    @JsonSerialize(using = CurrencySerializer.class)
    public Double getAmountToFormatted() {
	return amount;
    }
    
    public void setAmount(Double amount) {
        this.amount = amount;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }
    
    public int getValidPeriod() {
		return validPeriod;
	}

	public void setValidPeriod(int validPeriod) {
		this.validPeriod = validPeriod;
	}
}
