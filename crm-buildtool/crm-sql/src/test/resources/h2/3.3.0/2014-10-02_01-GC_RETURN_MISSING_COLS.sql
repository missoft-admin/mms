alter table CRM_GC_RETURN_BY_CARDS_ITEM add (
BALANCE decimal(19,2),
FACE_AMT decimal(19,2),
STATUS varchar(255),
PRODUCT bigint
);
