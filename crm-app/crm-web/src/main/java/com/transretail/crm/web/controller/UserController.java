package com.transretail.crm.web.controller;


import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;
import java.util.UUID;
import java.util.regex.Pattern;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import com.transretail.crm.core.entity.enums.AuthenticationType;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import org.springframework.web.servlet.support.RequestContextUtils;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.transretail.crm.common.service.dto.response.NameIdPairDto;
import com.transretail.crm.common.service.dto.response.ResultList;
import com.transretail.crm.core.dto.UserDto;
import com.transretail.crm.core.dto.UserRolePermDto;
import com.transretail.crm.core.dto.UserSearchDto;
import com.transretail.crm.core.entity.UserModel;
import com.transretail.crm.core.entity.enums.UserSearchCriteria;
import com.transretail.crm.core.entity.lookup.LookupDetail;
import com.transretail.crm.core.entity.lookup.Store;
import com.transretail.crm.core.security.util.UserUtil;
import com.transretail.crm.core.service.CodePropertiesService;
import com.transretail.crm.core.service.LookupService;
import com.transretail.crm.core.service.StoreService;
import com.transretail.crm.core.service.UserRoleModelService;
import com.transretail.crm.core.service.UserRolePermissionService;
import com.transretail.crm.core.service.UserService;
import com.transretail.crm.core.util.AppConstants;
import com.transretail.crm.web.controller.util.ControllerResponse;
import com.transretail.crm.web.controller.util.ControllerResponseExceptionHandler;
import com.transretail.crm.web.dto.ChangePasswordDto;


@RequestMapping("/user")
@Controller
public class UserController extends ControllerResponseExceptionHandler {
    protected static final String KEY_SUCCESS = "successMessages";
    protected static final String DEFAULT_PASSWORD = "123qweasd";

    protected static final String FORM_ATTR_NAME = "form";
	@Autowired
    UserService userService;
	@Autowired
    UserRoleModelService userRoleModelService;
	@Autowired
	StoreService storeService;
    @Autowired
    UserRolePermissionService userRolePermissionService;
    @Autowired
    private MessageSource messageSource;

    @Autowired
    LookupService lookupService;
    @Autowired
    CodePropertiesService codePropertiesService;


	private static final String PATH_USERS 	 		= "/user";
	private static final String PATH_CREATEUSER  	= "/user/create";

	private static final String UIATTR_SEARCHFIELDS	= "userSearchFields";

	private static final String UIATTR_ROLES	 	= "userRoles";
	private static final String UIATTR_INVENTORIES 	= "inventoryLocations";
	private static final String UIATTR_STORES	 	= "stores";
	private static final String UIATTR_FORMACTION	= "action";
    private static final String UIATTR_AUTH_TYPES 	= "authenticationTypes";


    private static final String DIALOG_LABEL 		= "dialogLabel";
	
	private static final String USE_STORE_LOCATION = "selectedStore";



    @RequestMapping( produces = "text/html" )
    public String showUsers(
            @RequestParam(value = "page", required = false) Integer page,
            @RequestParam(value = "size", required = false) Integer size,
            Model inUiModel ) {

    	inUiModel.addAttribute( UIATTR_SEARCHFIELDS, UserSearchCriteria.values() );
        return PATH_USERS;
    }



    @RequestMapping(value = "/list", method = RequestMethod.POST)
    public @ResponseBody ResultList<UserDto> listUsers(@RequestBody UserSearchDto searchForm) {
    	return userService.searchUser( searchForm );
    }



    @RequestMapping( value = "/create", produces = "text/html" )
    public String createUser( 
    		@ModelAttribute("user") UserModel inUser, 
    		Model inUiModel ) {

        populateFields( inUiModel );
        inUiModel.addAttribute( UIATTR_FORMACTION, "/user/save" );
        inUiModel.addAttribute( DIALOG_LABEL, messageSource.getMessage("user_label_create", 
        		null, LocaleContextHolder.getLocale()) );
        return PATH_CREATEUSER;
    }

    @RequestMapping( value = "/edit/{id}", produces = "text/html" )
    public String editUser( 
    		@PathVariable(value = "id") Long inId,
    		@ModelAttribute("user") UserDto inUser, 
    		Model inUiModel ) {

    	inUiModel.addAttribute( "user", userService.findUserDto( inId ) );

        populateFields( inUiModel );
        inUiModel.addAttribute( UIATTR_FORMACTION, "/user/update" );
        inUiModel.addAttribute( DIALOG_LABEL, messageSource.getMessage("user_label_update", 
        		null, LocaleContextHolder.getLocale()) );
        return PATH_CREATEUSER;
    }
    
    @RequestMapping( value = "/assignexhibition/{id}", produces = "text/html" )
    public String assignExhibitionUser( 
    		@PathVariable(value = "id") Long inId,
    		Model inUiModel ) {
        inUiModel.addAttribute( "assignExhibitionForm", userService.getUserExhibition(inId) );
        return "/user/profile/assignexhibition";
    }
    
    

    @RequestMapping( value="/assignexhibition/save", produces="application/json; charset=utf-8", method=RequestMethod.POST )
	public @ResponseBody ControllerResponse saveUser( 
			@ModelAttribute("assignExhibitionForm") NameIdPairDto form,
    		BindingResult inBinding ) {

    	ControllerResponse theResp = new ControllerResponse();
		theResp.setSuccess( true );
		if (inBinding.hasErrors()) {
			theResp.setSuccess(false);
			theResp.setResult(inBinding.getAllErrors());
			return theResp;
		}

		if (theResp.isSuccess()) {
			userService.saveExhibition(form);
		}


		return theResp;
	}
    


    @RequestMapping( value="/save", produces="application/json; charset=utf-8", method=RequestMethod.POST )
	public @ResponseBody ControllerResponse saveUser( 
			@ModelAttribute("user") UserDto inUser,
    		BindingResult inBinding ) {

    	ControllerResponse theResp = new ControllerResponse();
		theResp.setSuccess( true );
		
		if(inUser.getInventoryLocation().equals(USE_STORE_LOCATION)) {
			inUser.setInventoryLocation(inUser.getStoreCode());
		}

    	if ( isValid( inUser, inBinding, true ) ) {
            userService.saveUserDto( inUser );
    		theResp.setSuccess( true );
    		theResp.setResult( inUser );
    	}
    	else {
    		theResp.setSuccess( false );
    		theResp.setResult( inBinding.getAllErrors() );
    	}

		return theResp;
	}
    
    @RequestMapping( value="/update", produces="application/json; charset=utf-8", method=RequestMethod.POST )
	public @ResponseBody ControllerResponse updateUser( 
			@ModelAttribute("user") UserDto inUser,
    		BindingResult inBinding ) {

    	ControllerResponse theResp = new ControllerResponse();
		theResp.setSuccess( true );

		if(inUser.getInventoryLocation().equals(USE_STORE_LOCATION)) {
			inUser.setInventoryLocation(inUser.getStoreCode());
		}
		
    	if ( isValid( inUser, inBinding, false ) ) {
    		userService.updateUserDto( inUser );
    		theResp.setSuccess( true );
    		theResp.setResult( inUser );
    	}
    	else {
    		theResp.setSuccess( false );
    		theResp.setResult( inBinding.getAllErrors() );
    	}

		return theResp;
	}

    @RequestMapping( value="/delete/{id}", produces="application/json; charset=utf-8", method=RequestMethod.POST )
	public @ResponseBody ControllerResponse deleteUser( 
    		@PathVariable(value = "id") Long inId,
			@ModelAttribute("user") UserDto inUser,
    		BindingResult inBinding ) {

    	ControllerResponse theResp = new ControllerResponse();
		theResp.setSuccess( true );
    	if ( null != inId ) {
    		userService.deleteUserModel( userService.findUserModel( inId ) );
    	}

		return theResp;
	}

    @RequestMapping(value = "/profile/password/change", method = RequestMethod.GET)
    public ModelAndView showPasswordChangePage() {
        return new ModelAndView("/user/profile/changePassword").addObject(FORM_ATTR_NAME, new ChangePasswordDto());
    }

    @RequestMapping(value="/profile/password/change/{id}", method=RequestMethod.GET, produces = "text/html")
    public String showPasswordChangeDialog(@PathVariable String id, Model uiModel) {
    	uiModel.addAttribute("id", id);
    	ChangePasswordDto dto = new ChangePasswordDto();
    	dto.setCurrentPassword(UUID.randomUUID().toString());
    	uiModel.addAttribute(FORM_ATTR_NAME, dto);
    	return "/user/profile/password/dialog";
    }
    
    @RequestMapping(value="/profile/password/change/{username}", method=RequestMethod.POST, produces="application/json")
    @ResponseBody
    public ControllerResponse savePasswordChange(@PathVariable String username, ChangePasswordDto changePasswordDto) {
        ControllerResponse response = new ControllerResponse();
    	
    	List<String> messages = Lists.newArrayList();
    	
    	if(StringUtils.isBlank(changePasswordDto.getNewPassword())) {
    		Locale locale = LocaleContextHolder.getLocale();
    		messages.add(messageSource.getMessage("propkey_msg_notempty", new Object[]{
					messageSource.getMessage("user_label_newpassword", null, locale)}, 
					locale));
    	}
    	
    	if(StringUtils.isBlank(changePasswordDto.getConfirmPassword())) {
    		Locale locale = LocaleContextHolder.getLocale();
    		messages.add(messageSource.getMessage("propkey_msg_notempty", new Object[]{
					messageSource.getMessage("user_label_confirmpassword", null, locale)}, 
					locale));
    	}

        if (!AppConstants.REGEX_PASSWORD.matcher(changePasswordDto.getNewPassword()).matches()) {
            messages.add(messageSource.getMessage("Password", null, LocaleContextHolder.getLocale()));
        }

        if(!messages.isEmpty()) {
    		response.setSuccess(false);
    		response.setResult(messages);
    		return response;
    	}
    	
    	if(!changePasswordDto.getNewPassword().equals(changePasswordDto.getConfirmPassword())) {
    		response.setSuccess(false);
    		Locale locale = LocaleContextHolder.getLocale();
    		messages.add(messageSource.getMessage("constraints.Confirm.message", new Object[]{
					messageSource.getMessage("user_prop_password", null, locale)}, 
					locale));
    		response.setResult(messages);
    		return response;
    	}
    	
    	userService.changePassword(username, changePasswordDto.getNewPassword());
    	response.setSuccess(true);
    	return response;
    }
    
    @RequestMapping(value = "/profile/password/change", method = RequestMethod.POST)
    public ModelAndView savePasswordChange(@ModelAttribute(FORM_ATTR_NAME) @Valid ChangePasswordDto changePasswordDto,
        BindingResult result, RedirectAttributes redirectAttributes, HttpServletRequest request) {
        ModelAndView mav = null;
        if (result.hasErrors()) {
            mav = new ModelAndView("/user/profile/changePassword");
        } else {
        	if(!userService.currentPasswordMatch(changePasswordDto.getCurrentPassword())) {
        		result.addError(new ObjectError("currentPassword", 
        				messageSource.getMessage("user_msg_incorrectpassword", null, LocaleContextHolder.getLocale())));
        		mav = new ModelAndView("/user/profile/changePassword");
        	}
        	else if(changePasswordDto.getCurrentPassword().equals(changePasswordDto.getNewPassword())) {
        		result.addError(new ObjectError("newPassword", 
        				messageSource.getMessage("user_msg_samecurrentnew", null, LocaleContextHolder.getLocale())));
        		mav = new ModelAndView("/user/profile/changePassword");
        	}
        	else {
        		userService.changePassword(UserUtil.getCurrentUser().getUsername(), changePasswordDto.getNewPassword());
	            redirectAttributes.addFlashAttribute(KEY_SUCCESS,
	                messageSource.getMessage("user_msg_passwordchange_success", null, RequestContextUtils.getLocale(request)));
	            mav = new ModelAndView("redirect:/user/profile/password/change");
        	}
        }
        return mav;
    }

    @RequestMapping(value = "/permroles/update/validate", produces="application/json; charset=utf-8", method = RequestMethod.POST)
    public @ResponseBody ControllerResponse checkSupervisorRoles( @RequestBody UserDto dto ) {
    	ControllerResponse resp = new ControllerResponse();
    	resp.setSuccess( true );

		List<String> errors = new ArrayList<String>();
    	List<String> roles = Lists.newArrayList();
    	List<String> rolePerms = dto.getRolePerm().getRolePermTuples();
		UserModel user = userService.findUserModel( dto.getId() );

		if ( CollectionUtils.isNotEmpty( rolePerms ) ) {
			for ( String rolePerm : rolePerms ) {
				roles.add( rolePerm.indexOf( "-" ) == -1? rolePerm : rolePerm.substring( 0, rolePerm.indexOf( "-" ) ) );
			}
		}

    	boolean isSupervisor = roles.contains( codePropertiesService.getCssCode() );
    	isSupervisor |= roles.contains( codePropertiesService.getMerchantServiceSupervisorCode() );
    	isSupervisor |= roles.contains( codePropertiesService.getAccountingCode() );
    	if ( isSupervisor ) {
    		if ( StringUtils.isBlank( user.getEmail() ) && StringUtils.isBlank( dto.getEmail() ) ) {
        		errors.add( messageSource.getMessage( "user_msg_emailforsupervisors", null, LocaleContextHolder.getLocale() ) );
    		}
    	}

    	if ( StringUtils.isNotBlank( dto.getEmail() ) && StringUtils.isNotBlank( user.getEmail() ) 
    			&& !dto.getEmail().equalsIgnoreCase( user.getEmail() )
    			|| ( StringUtils.isNotBlank( dto.getEmail() ) && StringUtils.isBlank( user.getEmail() ) ) ) {
    		if ( !Pattern.matches( AppConstants.EMAIL_PATTERN, dto.getEmail() ) ) {
    			errors.add( messageSource.getMessage( "user_msg_emailpatterninvalid", null, LocaleContextHolder.getLocale() ) );
    		}
    		else {
        		user.setEmail( dto.getEmail() );
        		userService.updateUserModel( user );
    		}
    	}
		if ( CollectionUtils.isNotEmpty( errors ) ) {
    		resp.setSuccess( false );
			resp.setResult( errors );
		}

    	return resp;
    }



	private boolean isValid( UserDto user, BindingResult binding, boolean isNew ) {

		if ( StringUtils.isBlank( user.getUsername() ) ) {
    		binding.reject( messageSource.getMessage( "user_msg_emptyusername", null, LocaleContextHolder.getLocale() ) );
    	}

        //if ldap, no need for password; just provide a default password
        boolean isLdap = AuthenticationType.LDAP == user.getAuthenticationType();

        if (isLdap) {
            user.setPassword(DEFAULT_PASSWORD);
        } else {
            if ( StringUtils.isBlank( user.getPassword() ) && null == user.getId()) {
                binding.reject( messageSource.getMessage( "user_msg_emptypword", null, LocaleContextHolder.getLocale() ) );
            } else if (isNew) {
                if (!AppConstants.REGEX_PASSWORD.matcher(user.getPassword()).matches()) {
                    binding.reject(messageSource.getMessage("Password", null, LocaleContextHolder.getLocale()));
                }
            }

        }



		if ( StringUtils.isNotBlank( user.getUsername() ) ) {
			UserModel theModel = userService.findUserModel( user.getUsername().toLowerCase() );
			if ( null != theModel && theModel.getUsername().equalsIgnoreCase( user.getUsername() ) ) {
				if ( null == user.getId() || ( null != user.getId() && theModel.getId().longValue() != user.getId().longValue() ) ) {
					binding.reject( messageSource.getMessage( "user_msg_duplicateusername", null, LocaleContextHolder.getLocale() ) );
				}
			}
		}

		if ( StringUtils.isBlank( user.getStoreCode() ) ) {
    		binding.reject( messageSource.getMessage( "user_msg_emptystore", null, LocaleContextHolder.getLocale() ) );
    	}

		if ( StringUtils.isNotBlank( user.getEmail() ) && !Pattern.matches( AppConstants.EMAIL_PATTERN, user.getEmail() ) ) {
			binding.reject( messageSource.getMessage( "user_msg_emailpatterninvalid", null, LocaleContextHolder.getLocale() ) );
		}

		if ( null != user.getId() ) {
			Map<UserRolePermDto, Set<UserRolePermDto>> userRolePerms = userRolePermissionService.listUserRolePermissions( user.getId() );
			List<String> roles = Lists.newArrayList();
			for ( Map.Entry<UserRolePermDto, Set<UserRolePermDto>> entry : userRolePerms.entrySet() ) {
				if ( entry.getKey().isCurrentRolePerm() ) {
					roles.add( entry.getKey().getCode() );
				}
			}
	    	boolean isSupervisor = roles.contains( codePropertiesService.getCssCode() );
	    	isSupervisor |= roles.contains( codePropertiesService.getMerchantServiceSupervisorCode() );
	    	isSupervisor |= roles.contains( codePropertiesService.getAccountingCode() );
			if ( isSupervisor && StringUtils.isBlank( user.getEmail() ) ) {
				binding.reject( messageSource.getMessage( "user_msg_emailforsupervisors", null, LocaleContextHolder.getLocale() ) );
			}
		}

    	return !binding.hasErrors();
	}

	private void populateFields( Model inUiModel ) {
		List<Store> stores = Lists.newArrayList(storeService.getAllStores());
		Map<String, String> inventories = Maps.newHashMap();
		LookupDetail headOffice = lookupService.getDetailByCode(codePropertiesService.getDetailInvLocationHeadOffice());
		inventories.put(headOffice.getCode(), headOffice.getDescription());
		inventories.put(USE_STORE_LOCATION, messageSource.getMessage("user_msg_userstorelocation", null, LocaleContextHolder.getLocale()));

        Map<String, String> authTypes = Maps.newHashMap();
        authTypes.put(AuthenticationType.INTERNAL.toString(), messageSource.getMessage("user_msg_user_auth_internal", null, LocaleContextHolder.getLocale()));
        authTypes.put(AuthenticationType.LDAP.toString(), messageSource.getMessage("user_msg_user_auth_ldap", null, LocaleContextHolder.getLocale()));



        inUiModel.addAttribute( UIATTR_ROLES, userRoleModelService.findAllUserRoleModels() );
        inUiModel.addAttribute( UIATTR_STORES, stores );
        inUiModel.addAttribute( UIATTR_INVENTORIES, inventories );
        inUiModel.addAttribute( UIATTR_AUTH_TYPES, authTypes );


	}
}
