ALTER TABLE CRM_SALES_ORD DROP COLUMN
IF EXISTS
DEPARTMENT;

alter table CRM_SALES_ORD
  add column CATEGORY varchar(15);
  
alter table CRM_SALES_ORD 
        add constraint FK_rsui465u9t488y6e9e6baxj6f 
        foreign key (CATEGORY) 
        references CRM_REF_LOOKUP_DTL;
  

INSERT INTO CRM_REF_LOOKUP_HDR(CODE, DESCRIPTION) VALUES ('GCX006', 'GC SO CATEGORY');
INSERT INTO CRM_REF_LOOKUP_DTL(CODE, DESCRIPTION, HDR, STATUS) VALUES ('GCC001', 'Margin', 'GCX006', 'ACTIVE');
INSERT INTO CRM_REF_LOOKUP_DTL(CODE, DESCRIPTION, HDR, STATUS) VALUES ('GCC002', 'Marketing Expense', 'GCX006', 'ACTIVE');
INSERT INTO CRM_REF_LOOKUP_DTL(CODE, DESCRIPTION, HDR, STATUS) VALUES ('GCC003', 'Other Genex', 'GCX006', 'ACTIVE');
INSERT INTO CRM_REF_LOOKUP_DTL(CODE, DESCRIPTION, HDR, STATUS) VALUES ('GCC004', 'Donation', 'GCX006', 'ACTIVE');
INSERT INTO CRM_REF_LOOKUP_DTL(CODE, DESCRIPTION, HDR, STATUS) VALUES ('GCC005', 'Employee Shopping Reward', 'GCX006', 'ACTIVE');
