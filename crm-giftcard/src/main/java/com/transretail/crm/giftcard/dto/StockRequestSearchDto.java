package com.transretail.crm.giftcard.dto;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.joda.time.LocalDate;

import com.mysema.query.types.expr.BooleanExpression;
import com.transretail.crm.common.service.dto.request.AbstractSearchFormDto;
import com.transretail.crm.giftcard.entity.QStockRequest;
import com.transretail.crm.giftcard.entity.support.StockRequestStatus;

/**
 *
 */
public class StockRequestSearchDto extends AbstractSearchFormDto {
	private String requestNo;
	private LocalDate date;
	private String requestBy;
	private List<StockRequestStatus> status;
	private StockRequestStatus singleStatus;
	private String allocateTo;
	private String sourceLocation;
	private LocalDate reqDateFrom;
	private LocalDate reqDateTo;

    @Override
    public BooleanExpression createSearchExpression() {
        List<BooleanExpression> expressions = new ArrayList<BooleanExpression>();
        QStockRequest stockRequest = QStockRequest.stockRequest;
        
        if(StringUtils.isNotBlank(requestNo)) {
        	expressions.add(stockRequest.requestNo.eq(requestNo));
        }
        
        if(singleStatus != null) {
            if (singleStatus.equals(StockRequestStatus.TRANSFERRED_OUT))
                singleStatus = StockRequestStatus.TRANSFERRED_IN;
        	expressions.add(stockRequest.status.eq(singleStatus));
        }
        else if(status != null) {
            if (status.contains(StockRequestStatus.TRANSFERRED_OUT)) {
                status.remove(StockRequestStatus.TRANSFERRED_OUT);
                status.add(StockRequestStatus.TRANSFERRED_IN);
            }
        	expressions.add(stockRequest.status.in(status));
        }
        
        if (StringUtils.isNotBlank(allocateTo) && StringUtils.isNotBlank(sourceLocation)) {
            expressions.add(stockRequest.allocateTo.eq(allocateTo).or(stockRequest.sourceLocation.eq(sourceLocation)));
        }
        
        if(reqDateFrom != null) {
        	expressions.add(stockRequest.requestDate.goe(reqDateFrom));
        }
        
        if(reqDateTo != null) {
        	expressions.add(stockRequest.requestDate.loe(reqDateTo));
        }
        
        if(StringUtils.isNotBlank(requestBy)) {
        	expressions.add(stockRequest.createUser.eq(requestBy));
        }
        
        return BooleanExpression.allOf(expressions.toArray(new BooleanExpression[expressions.size()]));
    }

	public String getRequestNo() {
		return requestNo;
	}

	public void setRequestNo(String requestNo) {
		this.requestNo = requestNo;
	}

	public LocalDate getDate() {
		return date;
	}

	public void setDate(LocalDate date) {
		this.date = date;
	}

	public List<StockRequestStatus> getStatus() {
		return status;
	}

	public void setStatus(List<StockRequestStatus> status) {
		this.status = status;
	}

    public String getAllocateTo() {
        return allocateTo;
    }

    public void setAllocateTo(String allocateTo) {
        this.allocateTo = allocateTo;
    }

    public String getSourceLocation() {
        return sourceLocation;
    }

    public void setSourceLocation(String sourceLocation) {
        this.sourceLocation = sourceLocation;
    }

	public LocalDate getReqDateFrom() {
		return reqDateFrom;
	}

	public void setReqDateFrom(LocalDate reqDateFrom) {
		this.reqDateFrom = reqDateFrom;
	}

	public LocalDate getReqDateTo() {
		return reqDateTo;
	}

	public void setReqDateTo(LocalDate reqDateTo) {
		this.reqDateTo = reqDateTo;
	}

	public String getRequestBy() {
		return requestBy;
	}

	public void setRequestBy(String requestBy) {
		this.requestBy = requestBy;
	}

	public StockRequestStatus getSingleStatus() {
		return singleStatus;
	}

	public void setSingleStatus(StockRequestStatus singleStatus) {
		this.singleStatus = singleStatus;
	}
	
}
