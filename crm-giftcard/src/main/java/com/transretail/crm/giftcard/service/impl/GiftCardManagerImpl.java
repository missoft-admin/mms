package com.transretail.crm.giftcard.service.impl;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.google.common.collect.Sets;
import com.transretail.crm.common.service.exception.GenericServiceException;
import com.transretail.crm.core.entity.MemberModel;
import com.transretail.crm.core.entity.PeopleSoftStore;
import com.transretail.crm.core.repo.PeopleSoftStoreRepo;
import com.transretail.crm.core.service.MemberService;
import com.transretail.crm.core.service.PsoftStoreService;
import com.transretail.crm.giftcard.GiftCardServiceResolvableException;
import com.transretail.crm.giftcard.dto.GiftCardInfo;
import com.transretail.crm.giftcard.dto.GiftCardTransactionInfo;
import com.transretail.crm.giftcard.entity.*;
import com.transretail.crm.giftcard.entity.GiftCardInventoryStock.GCIStockStatus;
import com.transretail.crm.giftcard.entity.support.GiftCardInventoryStatus;
import com.transretail.crm.giftcard.entity.support.GiftCardSaleTransaction;
import com.transretail.crm.giftcard.entity.support.GiftCardSalesType;
import com.transretail.crm.giftcard.repo.*;
import com.transretail.crm.giftcard.service.*;
import org.apache.commons.lang3.BooleanUtils;
import org.apache.commons.lang3.StringUtils;
import org.joda.time.DateTime;
import org.joda.time.LocalDate;
import org.joda.time.LocalDateTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.math.MathContext;
import java.util.*;

import static com.transretail.crm.giftcard.GiftCardServiceResolvableException.*;
import static com.transretail.crm.giftcard.entity.support.GiftCardSaleTransaction.*;
import static com.transretail.crm.giftcard.entity.support.GiftCardSalesType.B2C;
import static com.transretail.crm.giftcard.entity.support.GiftCardSalesType.REBATE;

@Service
@Transactional
public class GiftCardManagerImpl implements GiftCardManager {

	private static final Logger logger = LoggerFactory.getLogger(GiftCardManagerImpl.class);
	@Autowired
	private GiftCardInventoryRepo inventoryRepo;
	@Autowired
	private GiftCardTransactionRepo transactionRepo;
	@Autowired
	private GiftCardTransactionItemRepo transactionItemRepo;
	@Autowired
	private GiftCardInventoryStockService gciStockService;
	@Autowired
	private GiftCardAccountingService acctService;
	@Autowired
	private SoAllocService soAllocService;
	@Autowired
	private PeopleSoftStoreRepo peopleSoftStoreRepo;
	@Autowired
	private PsoftStoreService peopleSoftStoreService;
	@Autowired
	private MemberService memberService;
	@Autowired
	private ProductProfileUnitRepo giftCardProductBuRepo;
	@Autowired
	private GiftCardInventoryService inventoryService;
	@Autowired
	private SalesOrderRepo salesOrderRepo;

	protected static boolean checkExpiration(final LocalDate transactionDate, GiftCardInventory giftcard) {
		return transactionDate.isAfter(giftcard.getExpiryDate());
	}

	private static String createInternalTxNo() {
		return TXNNO_DT_FORMATTER.print(LocalDateTime.now());
	}

	public void setAcctService(GiftCardAccountingService acctService) {
		this.acctService = acctService;
	}

	@Transactional(readOnly = true)
	@Override
	public GiftCardInfo inquire(String transactionNo, String cardNo, String merchantId) {
		GiftCardInventory giftcard = retrieveGiftCardInventory(cardNo);

		checkProductAndBu(merchantId, giftcard);

		GiftCardInfo gci = GiftCardInfo.convert(giftcard);
		if (StringUtils.isNotBlank(transactionNo)) {
			GiftCardTransaction ongoingPreRedeemTx = transactionRepo.findByTransactionNoAndTransactionType(transactionNo, GiftCardSaleTransaction.PRE_REDEMPTION);
			GiftCardTransaction redemptionTx = transactionRepo.findByTransactionNoAndTransactionType(transactionNo, REDEMPTION);
			// If there is on-going transaction, calculate previous and available balance
			if (ongoingPreRedeemTx != null && redemptionTx == null) {
				double totalRedeemAmount = 0.0;
				double prevBalance = 0.0;
				for (GiftCardTransactionItem t : ongoingPreRedeemTx.getTransactionItems()) {
					if (t.getGiftCard().equals(giftcard)) {
						prevBalance = t.getCurrentAmount();
						totalRedeemAmount += t.getTransactionAmount();
					}
				}

				gci.withPreviousAvailableBalance(prevBalance);
				gci.withAvailableBalance(new BigDecimal(totalRedeemAmount).add(gci.getAvailableBalance()));
			}
		}

		addInfoGC(gci, cardNo);

		return gci;
	}

	@Override
	public GiftCardInfo preActivate(GiftCardSalesType salesType, final GiftCardTransactionInfo txInfo, final String cardNo, String accountId) {
		GiftCardInventory giftcard = this.validateActivation(txInfo, salesType, cardNo, accountId);
		String merchantId = txInfo.getMerchantId();

		// check if have valid bu
		checkProductAndBu(merchantId, giftcard);

		GiftCardTransaction gtx = new GiftCardTransaction().cashierId(txInfo.getCashierId()).merchantId(merchantId).terminalId(txInfo.getTerminalId()).transactionDate(LocalDateTime.now()).transactionNo(createInternalTxNo()).transactionType(GiftCardSaleTransaction.PRE_ACTIVATION).businessUnit(getPeoplesoftStoreMapping(merchantId)).addItem(new GiftCardTransactionItem().giftCard(giftcard).transactionAmount(giftcard.getFaceValue().doubleValue()));

		transactionRepo.save(gtx);

		return GiftCardInfo.convert(giftcard);
	}

	@Override
	public GiftCardInventory validateActivation(GiftCardTransactionInfo txInfo, String cardNo) {
		return this.validateActivation(txInfo, B2C, cardNo, null);
	}

	@Override
	public GiftCardInventory validateActivation(GiftCardTransactionInfo txInfo, GiftCardSalesType salesType, String cardNo, String accountId) {
		GiftCardInventory giftcard = retrieveGiftCardInventory(cardNo);
		return validateActivation(txInfo, salesType, giftcard, accountId);
	}

	GiftCardInventory validateActivation(GiftCardTransactionInfo txInfo, GiftCardSalesType salesType, GiftCardInventory giftcard, String accountId) {
		final String cardNo = giftcard.getBarcode();
		final String merchantId = txInfo.getMerchantId();
		Boolean isForPromo = giftcard.getIsForPromo();
		isForPromo = (isForPromo == null ? false : isForPromo);
		if (REBATE == salesType && !isForPromo) {
			throw invalidCardForFreeVoucher(cardNo);
		} else if (salesType == B2C && isForPromo) {
			throw invalidCardForB2C(cardNo);
		} else if (BooleanUtils.isTrue(giftcard.getIsEgc())) {
			throw egcActivationNotAllowedException();
		} else if (StringUtils.isNotBlank(accountId) && giftcard.getProfile().isAllowReload()
				&& !memberService.accountIdExists(accountId)) {
			throw accountIdNotFoundException(accountId);
		} /*else if (!merchantId.equals(giftcard.getLocation())) {
			  throw giftCardNotPresentInMerchantException(cardNo, merchantId);
			}*/ else if (!peopleSoftStoreService.isStoreAllowedForGcTransaction(merchantId)) {
			throw merchantNotAllowedForGCTransactionsException(merchantId);
		} else if (giftcard.getStatus() != GiftCardInventoryStatus.IN_STOCK) {
			if (GiftCardInventoryStatus.BURNED.equals(giftcard.getStatus())) {
				throw giftCardIsAlreadyBurnedException(cardNo);
			} else if (GiftCardInventoryStatus.DISABLED == giftcard.getStatus()) {
				throw giftCardIsDeactivatedException(cardNo);
			}
			throw giftCardStatusNotInStockStatusException(cardNo, giftcard.getStatus());
		} else if (giftcard.getActivationDate() != null) {
			throw giftCardActivatedException(cardNo);
			// todo comment this line
		} /*else if (giftcard.getExpiryDate() != null) {
			  throw giftCardExpiredException(cardNo);
			}
			*/
		return giftcard;
	}

	GiftCardInventory retrieveGiftCardInventory(String cardNo) throws GiftCardServiceResolvableException {
		GiftCardInventory giftcard = inventoryRepo.findByBarcode(cardNo);
		if (giftcard == null) {
			throw giftCardNotFoundException(cardNo);
		}
		return giftcard;
	}

	private Set<GiftCardInfo> doActivation(GiftCardTransactionInfo cardTransactionInfo, Set<String> cards, String accountId, GiftCardSalesType salesType, boolean ryns) {
		SortedSet<GiftCardInfo> cardInfos = new TreeSet<GiftCardInfo>(new Comparator<GiftCardInfo>() {
			@Override
			public int compare(GiftCardInfo a, GiftCardInfo b) {
				return a.getSeries().compareTo(b.getSeries());
			}
		});

		final String transactionNo = cardTransactionInfo.getTransactionNo();
		if (transactionRepo.transactionNoExists(transactionNo, ACTIVATION)) {
			throw transactionNoAlreadyExistException(transactionNo);
		}

		GiftCardTransaction giftCardTransaction = extractTransactionInfo(cardTransactionInfo);
		giftCardTransaction.setTransactionType(ACTIVATION);
		giftCardTransaction.setSalesType(salesType);

		double totalTxnAmount = 0.0;

		for (String card : cards) {
			GiftCardInventory giftcard = inventoryRepo.findByBarcode(card);
			if (REBATE == salesType && BooleanUtils.isFalse(giftcard.getIsForPromo())) {
				throw invalidCardForFreeVoucher(card);
			} else if (salesType == B2C && BooleanUtils.isTrue(giftcard.getIsForPromo())) {
				throw invalidCardForB2C(card);
			}

			/* final String merchantId = cardTransactionInfo.getMerchantId();
			if (!merchantId.equals(giftcard.getLocation())) {
			    throw giftCardNotPresentInMerchantException(card, merchantId);
			}*/

			giftcard.setActivationDate(LocalDate.now());
			// TODO set expiredate
			if (giftcard.getExpiryDate() == null) {
				giftcard.setExpiryDate(LocalDate.now().plusMonths(giftcard.getProfile().getEffectiveMonths().intValue()));
			}

			final double faceValue = giftcard.getFaceValue().doubleValue();
			totalTxnAmount += faceValue;

			giftcard.setPreviousBalance(0.0);
			giftcard.setBalance(faceValue);
			giftcard.setStatus(GiftCardInventoryStatus.ACTIVATED);
			giftcard.setSalesType(salesType);
			giftcard.setRyns(ryns);

			if (giftcard.getProfile().isAllowReload() && StringUtils.isNotBlank(accountId)) {
				MemberModel member = memberService.findByAccountId(accountId);
				giftcard.setOwningMember(member);
				giftcard.setAccountId(accountId); // accountId can be change per member, so it save just for reference
				giftcard.setRegistrationDate(DateTime.now());
			}

			GiftCardInventory updatedGiftCard = inventoryRepo.save(giftcard);
			cardInfos.add(GiftCardInfo.convert(updatedGiftCard));

			GiftCardTransactionItem item = new GiftCardTransactionItem();
			item.setGiftCard(updatedGiftCard);
			item.setTransactionAmount(faceValue);

			giftCardTransaction.addTransactionItem(item);
		}

		transactionRepo.save(giftCardTransaction);
		// TODO: where is the source of notes field?

		if (GiftCardSalesType.B2C == salesType) {
			acctService.forB2CActivation(giftCardTransaction.getTransactionDate().toDateTime(), new BigDecimal(totalTxnAmount), giftCardTransaction.getMerchantId(), String.format("Transaction No.: %s \n", giftCardTransaction.getTransactionNo()));
		} else if (GiftCardSalesType.REBATE == salesType) {
			acctService.forGiftToCust(new DateTime(), new BigDecimal(totalTxnAmount), giftCardTransaction.getMerchantId(), String.format("Transaction No.: %s", giftCardTransaction.getTransactionNo()));
		}

		return cardInfos;
	}

	@Override
	public Set<GiftCardInfo> activate(GiftCardTransactionInfo cardTransactionInfo, Set<String> cards, String accountId, GiftCardSalesType salesType, boolean ryns) {

		String merchant = cardTransactionInfo.getMerchantId();
		for (String card : cards) {
			checkProductAndBu(merchant, retrieveGiftCardInventory(card));
		}

		Set<GiftCardInfo> cardInfos = this.doActivation(cardTransactionInfo, cards, accountId, salesType, ryns);
		gciStockService.saveStocksFromB2Activation(cardInfos, GCIStockStatus.ACTIVATED);

		return cardInfos;
	}

	@Override
	public Set<GiftCardInfo> activate(GiftCardTransactionInfo cardTransactionInfo, Set<String> cards, GiftCardSalesType salesType, boolean ryns) {
		return this.activate(cardTransactionInfo, cards, null, salesType, ryns);
	}

	@Override
	public Set<GiftCardInfo> activateFreeVoucher(GiftCardTransactionInfo cardTransactionInfo, Set<String> cards) {
		for (String card : cards) {
			this.validateActivation(cardTransactionInfo, REBATE, card, null);
		}

		// TODO: save stocks @Elmo
		return this.doActivation(cardTransactionInfo, cards, null, GiftCardSalesType.REBATE, true);
	}

	@Override
	public Set<GiftCardInfo> voidActivation(GiftCardTransactionInfo cardTransactionInfo) {
		final String transactionNo = cardTransactionInfo.getTransactionNo();
		if (transactionRepo.transactionNoExists(transactionNo, VOID_ACTIVATED)) {
			throw transactionNoAlreadyExistException(transactionNo);
		}

		final String merchantId = cardTransactionInfo.getMerchantId();
		if (!peopleSoftStoreService.isStoreAllowedForGcTransaction(merchantId)) {
			throw merchantNotAllowedForGCTransactionsException(merchantId);
		}

		final String refTransactionNo = cardTransactionInfo.getRefTransactionNo();
		GiftCardTransaction refGiftCardTransaction = transactionRepo.findByTransactionNoAndTransactionType(refTransactionNo, ACTIVATION);
		if (refGiftCardTransaction == null) {
			throw refTransactionNoNotFoundException(refTransactionNo);
		}

		// TODO: we can remove this?
		GiftCardSaleTransaction transactionType = refGiftCardTransaction.getTransactionType();
		if (!ACTIVATION.equals(transactionType)) {
			throw refTransactionTypeNotMatchException(transactionType, ACTIVATION);
		}

		GiftCardTransaction giftCardTransaction = extractTransactionInfo(cardTransactionInfo);
		giftCardTransaction.setRefTransaction(refGiftCardTransaction);
		giftCardTransaction.setTransactionType(VOID_ACTIVATED);
		giftCardTransaction.setSalesType(refGiftCardTransaction.getSalesType());

		double totalTxAmount = 0.0;
		SortedSet<GiftCardInfo> cardInfos = new TreeSet<GiftCardInfo>(new Comparator<GiftCardInfo>() {
			@Override
			public int compare(GiftCardInfo a, GiftCardInfo b) {
				return a.getSeries().compareTo(b.getSeries());
			}
		});

		for (GiftCardTransactionItem item : refGiftCardTransaction.getTransactionItems()) {
			// revert gift card inventory
			GiftCardInventory giftcard = item.getGiftCard();
			// check first if there is no previous transaction e.g redeem
			if (giftcard.getBalance() != giftcard.getFaceValue().doubleValue()) {
				GiftCardTransaction redeemTx = transactionRepo.findEnclosingTransactionOfGiftCard(giftcard, REDEMPTION);
				GiftCardTransaction reloadTx = transactionRepo.findEnclosingTransactionOfGiftCard(giftcard, GiftCardSaleTransaction.RELOAD);
				if (redeemTx != null || reloadTx != null) {
					throw voidNotAllowedException();
				}
			}

			giftcard.setActivationDate(null);
			giftcard.setBalance(0.0);
			giftcard.setSalesType(null);
			giftcard.setExpiryDate(null);
			giftcard.setStatus(GiftCardInventoryStatus.IN_STOCK);
			giftcard.setOwningMember(null);
			giftcard.setAccountId(null);
			giftcard.setRegistrationDate(null);

			GiftCardInventory updatedGiftCard = inventoryRepo.save(giftcard);
			cardInfos.add(GiftCardInfo.convert(updatedGiftCard));

			GiftCardTransactionItem newItem = new GiftCardTransactionItem();
			newItem.setGiftCard(item.getGiftCard());
			final double transactionAmount = item.getTransactionAmount();
			totalTxAmount += transactionAmount;
			newItem.setCurrentAmount(transactionAmount);
			newItem.setTransactionAmount(transactionAmount * -1);
			giftCardTransaction.addTransactionItem(newItem);
		}

		transactionRepo.save(giftCardTransaction);
		gciStockService.saveStocksFromB2Activation(cardInfos, GCIStockStatus.VOID_ACTIVATION);
		acctService.forB2CSalesVoiding(giftCardTransaction.getTransactionDate().toDateTime(), new BigDecimal(totalTxAmount).negate(), giftCardTransaction.getMerchantId(), String.format("Transaction No.: %s \n Reference Transaction No.: %s", giftCardTransaction.getTransactionNo(), giftCardTransaction.getRefTransaction().getTransactionNo()));
		return cardInfos;
	}

	@Override
	public GiftCardInfo preRedeem(final GiftCardTransactionInfo txInfo, final String cardNo, Double redeemAmount) {
		GiftCardInventory giftcard = this.validateRedemption(txInfo, cardNo, redeemAmount);
		final String transactionNo = txInfo.getTransactionNo();

		GiftCardTransaction redemptionTx = transactionRepo.findByTransactionNoAndTransactionType(transactionNo, REDEMPTION);
		if (redemptionTx != null) {
			throw transactionIsAlreadyClosedException(transactionNo);
		}

		GiftCardTransaction preRedemptionTx = transactionRepo.findByTransactionNoAndTransactionType(transactionNo, GiftCardSaleTransaction.PRE_REDEMPTION);

		if (preRedemptionTx == null) {
			preRedemptionTx = new GiftCardTransaction();
		}

		String merchantId = txInfo.getMerchantId();
		preRedemptionTx.cashierId(txInfo.getCashierId()).merchantId(merchantId).terminalId(txInfo.getTerminalId()).transactionDate(txInfo.getTransactionTime()).transactionNo(transactionNo).transactionType(GiftCardSaleTransaction.PRE_REDEMPTION).businessUnit(getPeoplesoftStoreMapping(merchantId));

		GiftCardInfo cardInfo = GiftCardInfo.convert(giftcard);

		Double totalTxAmount = 0.0;
		for (GiftCardTransactionItem item : preRedemptionTx.getTransactionItems()) {
			if (item.getGiftCard().equals(giftcard)) {
				totalTxAmount += item.getTransactionAmount();

				if (giftcard.getBalance() - totalTxAmount < 0) {
					throw giftCardInsufficientBalanceException(cardNo, item.getCurrentAmount());
				}
			}
		}

		GiftCardTransactionItem currentTxItem = new GiftCardTransactionItem().giftCard(giftcard).currentAmount(giftcard.getBalance()).transactionAmount(redeemAmount * -1);
		currentTxItem.currentAmount(giftcard.getBalance() + totalTxAmount);
		currentTxItem.transactionAmount(redeemAmount * -1);

		preRedemptionTx.addItem(currentTxItem);

		cardInfo.withPreviousAvailableBalance(giftcard.getBalance() + totalTxAmount).withAvailableBalance(giftcard.getBalance() + (totalTxAmount - redeemAmount));

		transactionRepo.save(preRedemptionTx);

		return cardInfo;
	}

	@Override
	public GiftCardInventory validateRedemption(GiftCardTransactionInfo txInfo, String cardNo, Double redeemAmount) {
		GiftCardInventory giftcard = retrieveGiftCardInventory(cardNo);
		final String merchantId = txInfo.getMerchantId();
		if (!peopleSoftStoreService.isStoreAllowedForGcTransaction(merchantId)) {
			throw merchantNotAllowedForGCTransactionsException(merchantId);
		} else if (null == giftcard.getActivationDate() && GiftCardInventoryStatus.ACTIVATED != giftcard.getStatus()) {
			throw giftCardNotYetActivatedException(cardNo);
		} else if (GiftCardInventoryStatus.DISABLED == giftcard.getStatus()) {
			throw giftCardIsDeactivatedException(cardNo);
		} else if (checkExpiration(LocalDate.now(), giftcard)) {
			throw giftCardExpiredException(cardNo);
		} else if (!giftcard.getProfile().isAllowPartialRedeem()) {

			if (giftcard.getBalance() != giftcard.getFaceValue().doubleValue()) {
				throw giftCardNotAllowedForMultipleRedemptionException(cardNo);
			}

			if (redeemAmount > giftcard.getBalance() && redeemAmount != 0 && !(redeemAmount < 0)) {
				throw giftCardInsufficientBalanceException(cardNo, giftcard.getBalance());
			}

		} else if (redeemAmount > giftcard.getBalance() && redeemAmount != 0 && !(redeemAmount < 0)) {
			throw giftCardInsufficientBalanceException(cardNo, giftcard.getBalance());
		}

		return giftcard;
	}

	@Override
	public Set<GiftCardInfo> redeem(GiftCardTransactionInfo cardTransactionInfo, String cardNo, Double redeemAmount) {

		checkProductAndBu(cardTransactionInfo.getMerchantId(), retrieveGiftCardInventory(cardNo));

		final String transactionNo = cardTransactionInfo.getTransactionNo();
		if (transactionRepo.transactionNoExists(transactionNo, REDEMPTION)) {
			throw transactionNoAlreadyExistException(transactionNo);
		}

		//GiftCardTransaction giftCardTransaction = transactionRepo.findByTransactionNoAndTransactionType(transactionNo, REDEMPTION);
		/*if (giftCardTransaction != null) {
			giftCardTransaction.setTransactionDate(cardTransactionInfo.getTransactionTime()); // update transaction time
		} else {
			giftCardTransaction = extractTransactionInfo(cardTransactionInfo);
		}*/

		GiftCardTransaction giftCardTransaction = extractTransactionInfo(cardTransactionInfo);

		GiftCardInventory giftcard = this.validateRedemption(cardTransactionInfo, cardNo, redeemAmount);

		Set<GiftCardInfo> cardInfos = Sets.newHashSet();
		final LocalDate transactionDate = cardTransactionInfo.getTransactionTime().toLocalDate();
		if (checkExpiration(transactionDate, giftcard)) {
			throw giftCardExpiredException(giftcard.getBarcode());
		}

		giftcard.setRedemptionDate(LocalDate.now());
		final Double currentAmount = giftcard.getBalance();
		giftcard.setPreviousBalance(currentAmount);
		// redemption amount is negative by default!
		final Double redemptionAmount = Math.abs(redeemAmount) * -1;

		giftcard.setBalance(currentAmount + redemptionAmount);

		final GiftCardInfo info = GiftCardInfo.convert(giftcard);
		addInfoGC(info, cardNo);
		cardInfos.add(info);

		GiftCardTransactionItem newItem = new GiftCardTransactionItem();
		newItem.setGiftCard(giftcard);
		newItem.setCurrentAmount(currentAmount);
		newItem.setTransactionAmount(redemptionAmount);
		newItem.setTransaction(giftCardTransaction);
		// causing a duplicate record in succeeding redeem in same transaction
		// giftCardTransaction.getTransactionItems().add(newItem);
		// giftCardTransaction.addTransactionItem(newItem);

		transactionRepo.save(giftCardTransaction.transactionType(REDEMPTION));
		transactionItemRepo.save(newItem);

		acctService.forB2CRedemption(giftCardTransaction.getTransactionDate().toDateTime(), new BigDecimal(redemptionAmount), giftCardTransaction.getMerchantId(), String.format("Transaction No.: %s", giftCardTransaction.getTransactionNo()));

		return cardInfos;
	}

	@Override
	public Set<GiftCardInfo> voidRedemption(GiftCardTransactionInfo cardTransactionInfo) {
		final String transactionNo = cardTransactionInfo.getTransactionNo();
		if (transactionRepo.transactionNoExists(transactionNo, VOID_REDEMPTION)) {
			throw transactionNoAlreadyExistException(transactionNo);
		}

		final String merchantId = cardTransactionInfo.getMerchantId();
		if (!peopleSoftStoreService.isStoreAllowedForGcTransaction(merchantId)) {
			throw merchantNotAllowedForGCTransactionsException(merchantId);
		}

		final String refTransactionNo = cardTransactionInfo.getRefTransactionNo();
		GiftCardTransaction refGiftCardTransaction = transactionRepo.findByTransactionNoAndTransactionType(refTransactionNo, REDEMPTION);

		if (refGiftCardTransaction == null) {
			throw refTransactionNoNotFoundException(refTransactionNo);
		}

		// TODO: We can remove this?
		GiftCardSaleTransaction transactionType = refGiftCardTransaction.getTransactionType();
		if (!REDEMPTION.equals(transactionType)) {
			throw refTransactionTypeNotMatchException(transactionType, REDEMPTION);
		}

		GiftCardTransaction giftCardTransaction = extractTransactionInfo(cardTransactionInfo);
		giftCardTransaction.setRefTransaction(refGiftCardTransaction);
		giftCardTransaction.setTransactionType(GiftCardSaleTransaction.VOID_REDEMPTION);

		double totalRedemptionAmount = 0.0;
		Set<GiftCardInfo> cardInfos = Sets.newHashSet();
		for (GiftCardTransactionItem giftCardTransactionItem : Lists.newCopyOnWriteArrayList(refGiftCardTransaction.getTransactionItems())) {
			// revert gift card inventory
			GiftCardInventory giftcard = giftCardTransactionItem.getGiftCard();
			giftcard.setRedemptionDate(null);
			final Double currentAmount = giftcard.getBalance();
			final double redemptionAmount = giftCardTransactionItem.getTransactionAmount();
			totalRedemptionAmount += redemptionAmount;

			giftcard.setBalance(Math.abs(redemptionAmount) + currentAmount);
			giftcard.setPreviousBalance(currentAmount);

			GiftCardInventory updatedGiftCard = inventoryRepo.save(giftcard);
			cardInfos.add(GiftCardInfo.convert(updatedGiftCard));

			GiftCardTransactionItem newItem = new GiftCardTransactionItem();
			newItem.setGiftCard(giftCardTransactionItem.getGiftCard());
			newItem.setCurrentAmount(currentAmount);
			newItem.setTransactionAmount(Math.abs(redemptionAmount));
			giftCardTransaction.addTransactionItem(newItem);
		}

		transactionRepo.save(giftCardTransaction);
		acctService.forB2CRedemptionVoiding(giftCardTransaction.getTransactionDate().toDateTime(), new BigDecimal(totalRedemptionAmount).abs(), giftCardTransaction.getMerchantId(), String.format("Transaction No.: %s \n Reference Transaction No.: %s", giftCardTransaction.getTransactionNo(), giftCardTransaction.getRefTransaction().getTransactionNo()));
		return cardInfos;
	}

	@Override
	public GiftCardInfo preReload(String cardNo, String merchantId, Double amount) {
		if (!peopleSoftStoreService.isStoreAllowedForGcTransaction(merchantId)) {
			throw merchantNotAllowedForGCTransactionsException(merchantId);
		}

		GiftCardInventory giftcard = retrieveGiftCardInventory(cardNo);

		final LocalDate transactionDate = LocalDate.now();
		ProductProfile productProfile = giftcard.getProfile();
		List<PrepaidReloadInfo> reloadResponseInfos = productProfile.getPrepaidReloadInfos();

		if (GiftCardInventoryStatus.DISABLED == giftcard.getStatus()) {
			throw giftCardIsDeactivatedException(cardNo);
		} else if (null == giftcard.getActivationDate()) {
			throw giftCardNotYetActivatedException(cardNo);
		} else if (checkExpiration(transactionDate, giftcard)) {
			throw giftCardExpiredException(cardNo);
		} else if (!productProfile.isAllowReload()) {
			throw prepaidCardNotAllowedToReloadException(cardNo);
		} else if (amount != 0 && (giftcard.getBalance() + amount) > productProfile.getMaxAmount()) {
			throw reloadExceededMaxAmount(productProfile.getMaxAmount());
		} else if (reloadResponseInfos == null || reloadResponseInfos.isEmpty()) {
			throw reloadInfoIsEmptyException(productProfile.getCodeAndName());
		}

		if (amount != 0) { // used in pos lose
			boolean found = false;
			for (PrepaidReloadInfo ri : reloadResponseInfos) {
				if (ri.getReloadAmount().doubleValue() == amount) {
					found = true;
					break;
				}
			}

			if (!found) {
				throw invalidReloadAmount(reloadResponseInfos);
			}
		}
		return GiftCardInfo.convert(giftcard);
	}

	@Override
	@Transactional
	public Set<GiftCardInfo> reload(GiftCardTransactionInfo txInfo, String cardNo, Long reloadAmount) {
		final String transactionNo = txInfo.getTransactionNo();
		if (transactionRepo.transactionNoExists(transactionNo, RELOAD)) {
			throw transactionNoAlreadyExistException(transactionNo);
		}

		final String merchantId = txInfo.getMerchantId();
		if (!peopleSoftStoreService.isStoreAllowedForGcTransaction(merchantId)) {
			throw merchantNotAllowedForGCTransactionsException(merchantId);
		}

		GiftCardInventory giftcard = retrieveGiftCardInventory(cardNo);
		Double currAmount = giftcard.getBalance();

		final LocalDateTime transactionTime = txInfo.getTransactionTime();
		GiftCardTransaction gcTxn = new GiftCardTransaction().merchantId(merchantId).terminalId(txInfo.getTerminalId()).cashierId(txInfo.getCashierId()).businessUnit(getPeoplesoftStoreMapping(merchantId)).transactionNo(txInfo.getTransactionNo()).transactionDate(transactionTime).transactionType(GiftCardSaleTransaction.RELOAD).isLoseTransaction(txInfo.isLoseTransaction());

		GiftCardTransactionItem gcTxnItem = new GiftCardTransactionItem();
		gcTxnItem.setTransaction(gcTxn);
		gcTxnItem.setGiftCard(giftcard);
		gcTxnItem.setTransactionAmount(reloadAmount.doubleValue());
		gcTxnItem.setCurrentAmount(currAmount);

		gcTxn.addItem(gcTxnItem);
		transactionRepo.save(gcTxn);

		giftcard.setPreviousBalance(currAmount);
		giftcard.setBalance(new BigDecimal(currAmount, MathContext.DECIMAL128).add(new BigDecimal(reloadAmount)).doubleValue());

		ProductProfile productProfile = giftcard.getProfile();
		List<PrepaidReloadInfo> reloadResponseInfos = productProfile.getPrepaidReloadInfos();
		updateExpiryBasedOnReloadAmount(reloadResponseInfos, reloadAmount, giftcard, true);

		inventoryRepo.save(giftcard);

		acctService.forReloadSales(transactionTime.toDateTime(), new BigDecimal(reloadAmount), merchantId, String.format("Transaction No: %s", gcTxn.getTransactionNo()));

		return Sets.newHashSet(GiftCardInfo.convert(giftcard));
	}

	@Override
	public Set<GiftCardInfo> reloadVoid(GiftCardTransactionInfo txInfo) {
		final String transactionNo = txInfo.getTransactionNo();
		if (transactionRepo.transactionNoExists(transactionNo, VOID_RELOAD)) {
			throw transactionNoAlreadyExistException(transactionNo);
		}

		final String merchantId = txInfo.getMerchantId();
		if (!peopleSoftStoreService.isStoreAllowedForGcTransaction(merchantId)) {
			throw merchantNotAllowedForGCTransactionsException(merchantId);
		}
		final String refTransactionNo = txInfo.getRefTransactionNo();
		GiftCardTransaction refGiftCardTransaction = transactionRepo.findByTransactionNoAndTransactionType(refTransactionNo, RELOAD);
		if (refGiftCardTransaction == null) {
			throw refTransactionNoNotFoundException(refTransactionNo);
		}

		// TODO: we can remove this?
		GiftCardSaleTransaction transactionType = refGiftCardTransaction.getTransactionType();
		if (!GiftCardSaleTransaction.RELOAD.equals(transactionType)) {
			throw refTransactionTypeNotMatchException(transactionType, GiftCardSaleTransaction.RELOAD);
		}

		GiftCardTransaction gcTxn = extractTransactionInfo(txInfo);
		gcTxn.setRefTransaction(refGiftCardTransaction);
		gcTxn.setTransactionType(GiftCardSaleTransaction.VOID_RELOAD);

		GiftCardTransactionItem gcTxnItem = refGiftCardTransaction.getTransactionItems().iterator().next();
		GiftCardInventory giftcard = gcTxnItem.getGiftCard();

		Double currAmount = giftcard.getBalance();
		Double reloadAmount = gcTxnItem.getTransactionAmount();
		giftcard.setPreviousBalance(currAmount);
		giftcard.setBalance(new BigDecimal(currAmount, MathContext.DECIMAL128).subtract(new BigDecimal(reloadAmount)).doubleValue());

		updateExpiryBasedOnReloadAmount(giftcard.getProfile().getPrepaidReloadInfos(), reloadAmount.longValue(), giftcard, false);

		inventoryRepo.save(giftcard);

		GiftCardTransactionItem newItem = new GiftCardTransactionItem();
		newItem.setGiftCard(giftcard);
		newItem.setCurrentAmount(currAmount);
		newItem.setTransactionAmount(reloadAmount * -1);
		gcTxn.addTransactionItem(newItem);
		transactionRepo.save(gcTxn);

		acctService.forReloadSalesVoiding(gcTxn.getTransactionDate().toDateTime(), new BigDecimal(reloadAmount).negate(), merchantId, String.format("Transaction No: %s\nRef. Transaction No: %s\nReload amount %,f voided.", gcTxn.getTransactionNo(), gcTxn.getRefTransaction().getTransactionNo(), reloadAmount));

		return Sets.newHashSet(GiftCardInfo.convert(giftcard));
	}

	private GiftCardTransaction extractTransactionInfo(GiftCardTransactionInfo cardTransactionInfo) {
		GiftCardTransaction giftCardTransaction = new GiftCardTransaction();
		if (cardTransactionInfo != null) {
			final String merchantId = cardTransactionInfo.getMerchantId();
			giftCardTransaction.setMerchantId(merchantId);
			giftCardTransaction.setPeoplesoftStoreMapping(getPeoplesoftStoreMapping(merchantId));
			giftCardTransaction.setTerminalId(cardTransactionInfo.getTerminalId());
			giftCardTransaction.setCashierId(cardTransactionInfo.getCashierId());
			giftCardTransaction.setTransactionNo(cardTransactionInfo.getTransactionNo());
			giftCardTransaction.setTransactionDate(cardTransactionInfo.getTransactionTime());
			giftCardTransaction.setLoseTransaction(cardTransactionInfo.isLoseTransaction());
		}

		return giftCardTransaction;
	}

	@Override
	@Async
	@Transactional
	public void activateB2B(SalesOrder salesOrder, List<GiftCardInventory> allocatedCards) {
		logger.debug("Activating B2B asynchronously...");
		GiftCardTransaction txInfo = new GiftCardTransaction().transactionType(ACTIVATION).salesType(GiftCardSalesType.getSalesType(salesOrder.getOrderType())).merchantId(salesOrder.getPaymentStore()).businessUnit(getPeoplesoftStoreMapping(salesOrder.getPaymentStore())).transactionNo(createInternalTxNo()).transactionDate(LocalDateTime.now()).salesOrder(salesOrder).discountPercentage(salesOrder.getDiscountVal().doubleValue());

		for (GiftCardInventory gc : allocatedCards) {
			txInfo.addItem(new GiftCardTransactionItem().giftCard(gc).transaction(txInfo).currentAmount(0.0).transactionAmount(gc.getFaceValue().doubleValue()));
		}

		transactionRepo.saveAndFlush(txInfo);
		logger.debug("Finished activating B2Bs");
	}

	@Override
	@Async
	@Transactional
	public void returnB2B(SalesOrder salesOrder) {
		GiftCardTransaction tx = transactionRepo.findBySalesOrder(salesOrder);
		if (tx == null) {
			throw new GenericServiceException(String.format("Transaction with sales order no %s is not found.", salesOrder.getOrderNo()));
		}

		final String paymentStore = salesOrder.getPaymentStore();
		GiftCardTransaction giftCardTransaction = new GiftCardTransaction().merchantId(paymentStore).businessUnit(getPeoplesoftStoreMapping(paymentStore)).transactionNo(createInternalTxNo()).transactionDate(LocalDateTime.now()).transactionType(VOID_ACTIVATED).salesType(GiftCardSalesType.getSalesType(salesOrder.getOrderType())).refTransactionNo(tx).salesOrder(salesOrder);

		List<GiftCardInventory> allocatedCards = soAllocService.getAllocatedGcs(salesOrder.getId());
		for (GiftCardInventory gc : allocatedCards) {
			giftCardTransaction.addItem(new GiftCardTransactionItem().giftCard(gc).transaction(giftCardTransaction).currentAmount(gc.getFaceValue().doubleValue()).transactionAmount(gc.getFaceValue().negate().doubleValue()));
		}

		transactionRepo.save(giftCardTransaction);
	}

	@Override
	@Async
	@Transactional
	public void returnB2BByCard(SalesOrder salesOrder, Iterable<GiftCardInventory> cards) {
		GiftCardTransaction tx = transactionRepo.findBySalesOrder(salesOrder);
		if (tx == null) {
			throw new GenericServiceException(String.format("Transaction with sales order no %s is not found.", salesOrder.getOrderNo()));
		}

		final String paymentStore = salesOrder.getPaymentStore();
		GiftCardTransaction giftCardTransaction = new GiftCardTransaction().merchantId(paymentStore).businessUnit(getPeoplesoftStoreMapping(paymentStore)).transactionNo(TXNNO_DT_FORMATTER.print(LocalDateTime.now())).transactionDate(LocalDateTime.now()).transactionType(VOID_ACTIVATED).salesType(GiftCardSalesType.getSalesType(salesOrder.getOrderType())).refTransactionNo(tx).salesOrder(salesOrder);

		for (GiftCardInventory gc : cards) {
			giftCardTransaction.addItem(new GiftCardTransactionItem().giftCard(gc).transaction(giftCardTransaction).currentAmount(gc.getFaceValue().doubleValue()).transactionAmount(gc.getFaceValue().negate().doubleValue()));
		}

		transactionRepo.save(giftCardTransaction);
	}

	private PeopleSoftStore getPeoplesoftStoreMapping(String merchant) {
		PeopleSoftStore peopleSoftStore = peopleSoftStoreRepo.findByStoreCode(merchant);

		if (peopleSoftStore == null) {
			logger.warn("No People Store found for merchant {}.", merchant);
		}

		return peopleSoftStore;
	}

	/**
	 * @param reloadResponseInfos
	 * @param reloadAmount
	 * @param giftcard
	 * @param addOrDeductExpiryDate if true, will add number of months to expiry
	 *                              date. Deduct otherwise.
	 */
	private void updateExpiryBasedOnReloadAmount(final List<PrepaidReloadInfo> reloadResponseInfos, final Long reloadAmount,
	                                             final GiftCardInventory giftcard, boolean addOrDeductExpiryDate) {
		List<Long> availableReloadAmounts = Lists.newArrayList();
		Map<Long, Integer> reloadAmountEffMonthMap = Maps.newHashMap();
		for (PrepaidReloadInfo reloadInfo : reloadResponseInfos) {
			availableReloadAmounts.add(reloadInfo.getReloadAmount());
			reloadAmountEffMonthMap.put(reloadInfo.getReloadAmount(), reloadInfo.getMonthEffective());
		}
		Collections.sort(availableReloadAmounts);

		Integer effectivityMonth = null;
		int availableReloadAmountsSize = availableReloadAmounts.size();
		for (int i = 0; i < availableReloadAmountsSize; i++) {
			Long availableReloadAmount = availableReloadAmounts.get(i);
			if (reloadAmount.equals(availableReloadAmount)) {
				effectivityMonth = reloadAmountEffMonthMap.get(availableReloadAmount);
				break;
			} else if (reloadAmount < availableReloadAmount) {
				if (i > 0) {
					effectivityMonth = reloadAmountEffMonthMap.get(availableReloadAmounts.get(i - 1));
					break;
				}
			}
		}

		if (effectivityMonth == null) {
			Long availableReloadAmount = availableReloadAmounts.get(availableReloadAmountsSize - 1);
			if (availableReloadAmount < reloadAmount) {
				effectivityMonth = reloadAmountEffMonthMap.get(availableReloadAmount);
			}
		}

		if (effectivityMonth != null) {
			LocalDate expiryDate = giftcard.getExpiryDate();
			if (addOrDeductExpiryDate) {
				expiryDate = expiryDate.plusMonths(effectivityMonth);
			} else {
				expiryDate = expiryDate.minusMonths(effectivityMonth);
			}
			giftcard.setExpiryDate(expiryDate);
		}
	}

	private void checkProductAndBu(String store, GiftCardInventory cardInventory) {

		final List<ProductProfileUnit> products = giftCardProductBuRepo.findByProductProfile(cardInventory.getProfile());
		final PeopleSoftStore buStore = getPeoplesoftStoreMapping(store);
		final String gc = cardInventory.getProfile().getProductDesc();

		if (buStore == null) {
			logger.warn("store {} doesnt exist...", store);
			throw invalidBuGiftCardException(gc);
		}

		List<String> result = new ArrayList<String>();
		if (products != null)
			for (ProductProfileUnit ld : products) {
				result.add(ld.getBusinessUnit().getCode());
			}

		if (!result.contains(buStore.getBusinessUnit().getCode())) {
			logger.warn("invalid BU and merchant...");
			throw invalidBuGiftCardException(gc);
		}
	}

	private SalesOrder getInfoSalesOrder(String cardNo) {

		GiftCardInventory cardInventory = inventoryService.getGiftCardByBarcode(cardNo);

		return salesOrderRepo.findEnclosingSalesOrderOfGiftCardInventory(cardInventory);
	}

	private void addInfoGC(GiftCardInfo cardInfo, String cardNo) {

		final SalesOrder salesOrder = getInfoSalesOrder(cardNo);
		final String customerName = salesOrder == null ? "" : (salesOrder.getCustomer() == null ? "" : salesOrder.getCustomer().getName());
		final String note = salesOrder == null ? "" : (salesOrder.getNotes() == null ? "" : salesOrder.getNotes());
		final String discountType = salesOrder == null ? "" : (salesOrder.getDiscType() == null ? "" : salesOrder.getDiscType().toString());
		cardInfo.setCustomer(customerName);
		cardInfo.setDiscountType(discountType);
		cardInfo.setNote(note);
	}
}
