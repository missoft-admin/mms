package com.transretail.crm.core.util;

import org.springframework.util.StringUtils;

/**
 * Base class for all formatting logics to standardize the format used in the entire app
 *
 * @author Allan G. Ramirez (agramirez@exist.com)
 */
public enum FormatterUtil {
    INSTANCE;

    public String formatName(String lastName, String firstName) {
        StringBuilder builder = new StringBuilder();
        if (StringUtils.hasText(firstName)) {
            builder.append(firstName);
        }
        if (StringUtils.hasText(lastName)) {
            if (!builder.toString().isEmpty()) {
                builder.append(" ");
            }
            builder.append(lastName);
        }
        return builder.toString();
    }
}
