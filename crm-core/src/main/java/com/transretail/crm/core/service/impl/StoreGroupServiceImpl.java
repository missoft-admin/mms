package com.transretail.crm.core.service.impl;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.google.common.collect.Lists;
import com.mysema.query.types.expr.BooleanExpression;
import com.transretail.crm.common.service.dto.request.PageSortDto;
import com.transretail.crm.common.service.dto.response.ResultList;
import com.transretail.crm.common.service.util.SpringDataPagingUtil;
import com.transretail.crm.core.dto.StoreGroupDto;
import com.transretail.crm.core.dto.StoreGrpSearchDto;
import com.transretail.crm.core.entity.PromotionStoreGroup;
import com.transretail.crm.core.entity.QPromotionStoreGroup;
import com.transretail.crm.core.entity.lookup.QStore;
import com.transretail.crm.core.entity.lookup.Store;
import com.transretail.crm.core.entity.lookup.StoreGroup;
import com.transretail.crm.core.repo.PromotionStoreGroupRepo;
import com.transretail.crm.core.repo.StoreGroupRepo;
import com.transretail.crm.core.repo.StoreRepo;
import com.transretail.crm.core.service.CodePropertiesService;
import com.transretail.crm.core.service.StoreGroupService;

@Service("storeGroupService")
@Transactional
public class StoreGroupServiceImpl implements StoreGroupService {
	
	@Autowired
	StoreGroupRepo storeGroupRepo;
	@Autowired
	StoreRepo storeRepo;
	@Autowired
	PromotionStoreGroupRepo promotionStoreGroupRepo;
	@Autowired
	CodePropertiesService codePropertiesService;


	
	@Override
	public ResultList<StoreGroup> listStoreGroups(PageSortDto dto) {
    	Pageable pageable = SpringDataPagingUtil.INSTANCE.createPageable(dto.getPagination());
        Page<StoreGroup> page = storeGroupRepo.findAll(pageable);
        return new ResultList<StoreGroup>(page);
	}
	
	@Override
	public ResultList<Store> listStores(Long id, PageSortDto dto) {
		QStore qStore = QStore.store;
		StoreGroup storeGroup = findOne(id);
		BooleanExpression filters = qStore.code.in(storeGroup.getStoreList());
    	Pageable pageable = SpringDataPagingUtil.INSTANCE.createPageable(dto.getPagination());
        return new ResultList<Store>(storeRepo.findAll(filters, pageable));
	}

	@Override
	public ResultList<StoreGroupDto> search( StoreGrpSearchDto dto ) {
        Pageable pageable = SpringDataPagingUtil.INSTANCE.createPageable(dto.getPagination());
        BooleanExpression filter = dto.createSearchExpression();
        Page<StoreGroup> page = filter != null ? storeGroupRepo.findAll( filter, pageable ) : storeGroupRepo.findAll( pageable );
		return new ResultList<StoreGroupDto>( toDtoList( page.getContent() ), page.getTotalElements(), page.getNumber(), page.getSize() );		
	}

    private List<StoreGroupDto> toDtoList( List<StoreGroup> models ) {
        List<StoreGroupDto> dtos = Lists.newArrayList();

        Iterator<PromotionStoreGroup> it = promotionStoreGroupRepo.findAll( 
        		QPromotionStoreGroup.promotionStoreGroup.promotion.status.code.equalsIgnoreCase( 
        				codePropertiesService.getDetailStatusActive() ) ).iterator();
        List<Long> activeIds = Lists.newArrayList();
        while( it.hasNext() ) {
        	PromotionStoreGroup pGrp = it.next();
        	activeIds.add( pGrp.getStoreGroup().getId() );
        }

        for ( StoreGroup model : models ) {
        	StoreGroupDto dto = new StoreGroupDto( model );
        	if ( CollectionUtils.isNotEmpty( activeIds ) ) {
        		if ( activeIds.contains( dto.getId().longValue() ) ) {
        			dto.setIsUsed( true );
        		}
        	}
        	dtos.add( dto );
        }
        return dtos;
    }
	
	@Override
	public void saveStoreGroup(StoreGroup group) {
		storeGroupRepo.save(group);
	}
	
	@Override
	public void createStoreGroup(StoreGroup group) {
		storeGroupRepo.save(group);
	}
	
	@Override
	public List<StoreGroup> findAll() {
		return storeGroupRepo.findAll();
	}
	
	@Override
	public StoreGroup findOne(Long id) {
		return storeGroupRepo.findOne(id);
	}
	
	@Override
	public void delete(Long id) {
		storeGroupRepo.delete(id);
	}
	
	@Override
	public List<Store> getItems(Long id) {
		StoreGroup storeGroup = findOne(id);
		
		List<Store> stores = new ArrayList<Store>();
		
		for(String storeCode : storeGroup.getStoreList()) {
			stores.add(storeRepo.findByCode(storeCode));
		}
		
		return stores;
	}
	
	@Override
	public List<StoreGroup> getAllGroups() {
		List<StoreGroup> groups = findAll();
		
		for(StoreGroup group : groups) {
			group.setStoreModels(getItems(group.getId()));
		}
		
		return groups;
	}
	
	@Override
	public Iterable<Store> searchItems(String searchStr) {
		List<BooleanExpression> expressions = new ArrayList<BooleanExpression>();
		
		QStore qStore = QStore.store;
		
		if(StringUtils.isNotBlank(searchStr)) {
			if ( searchStr.contains( "*" ) ) {
				searchStr = searchStr.replace('*', '%');
				expressions.add(qStore.code.toLowerCase().like(searchStr.toLowerCase()));
				expressions.add(qStore.name.toLowerCase().like(searchStr.toLowerCase()));
			}
			else {
				expressions.add(qStore.code.containsIgnoreCase( searchStr ) );
				expressions.add(qStore.name.containsIgnoreCase( searchStr ) );
			}
		}
		
		return storeRepo.findAll(BooleanExpression.anyOf(expressions.toArray(new BooleanExpression[expressions.size()])));
	}
}
