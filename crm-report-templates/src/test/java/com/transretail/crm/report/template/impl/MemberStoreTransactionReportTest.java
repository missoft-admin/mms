package com.transretail.crm.report.template.impl;

import com.google.common.collect.Maps;
import com.transretail.crm.common.reporting.jasper.JRProcessor;
import com.transretail.crm.common.reporting.service.ReportService;
import com.transretail.crm.core.entity.MemberModel;
import com.transretail.crm.core.entity.PointsTxnModel;
import com.transretail.crm.core.entity.PosTransaction;
import com.transretail.crm.core.entity.embeddable.CustomerProfile;
import com.transretail.crm.core.entity.enums.MemberStatus;
import com.transretail.crm.core.entity.enums.PointTxnType;
import com.transretail.crm.core.entity.enums.Status;
import com.transretail.crm.core.entity.lookup.LookupDetail;
import com.transretail.crm.core.entity.lookup.LookupHeader;
import com.transretail.crm.core.entity.lookup.Store;
import com.transretail.crm.core.repo.*;
import com.transretail.crm.report.template.CommonReportFilter;
import com.transretail.crm.report.template.ReportTemplate;
import net.sf.jasperreports.engine.JRParameter;
import org.apache.commons.io.FileUtils;
import org.joda.time.LocalDateTime;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.context.support.MessageSourceResourceBundle;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.io.File;
import java.util.Date;
import java.util.Locale;
import java.util.Map;

/**
 *
 * @author Monte Cillo Co (mco@exist.com)
 */
@Transactional
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath*:/META-INF/spring/applicationContext*.xml",
    "/META-INF/spring/applicationContext-test.xml"})
public class MemberStoreTransactionReportTest {

    @Autowired
    private LookupHeaderRepo lookupHeaderRepo;
    @Autowired
    private LookupDetailRepo lookupDetailRepo;
    @Autowired
    private MemberRepo memberRepo;
    @Autowired
    private StoreRepo storeRepo;
    @Autowired
    private PointsTxnModelRepo pointsTxnModelRepo;
    @Autowired
    private PosTransactionRepo posTransactionRepo;
    @PersistenceContext
    private EntityManager em;
    @Resource(name = "memberStoreTransactionReport")
    private ReportTemplate memberStoreTransactionReport;
    @Autowired
    private ReportService reportService;
    @Autowired
    private MessageSource messageSource;
    private static final Logger logger = LoggerFactory.getLogger(MemberStoreTransactionReportTest.class);

    @Before
    public void setup() {
	LookupHeader memberType = new LookupHeader("MEM007");
	memberType.setDescription("member type");
	memberType = lookupHeaderRepo.saveAndFlush(memberType);

	lookupDetailRepo.save(new LookupDetail("MTYP001", "Individual", memberType, Status.ACTIVE));
	lookupDetailRepo.save(new LookupDetail("MTYP002", "Employee", memberType, Status.ACTIVE));
	lookupDetailRepo.save(new LookupDetail("MTYP003", "Professional", memberType, Status.ACTIVE));
	lookupDetailRepo.save(new LookupDetail("CARDTYPE", "REGULAR", memberType, Status.ACTIVE));

	Store store = new Store();
	store.setId(022);
	store.setCode("22022");
	store.setName("Lebak Bulus Test Store");
	storeRepo.save(store);

	Store storeReg = new Store();
	storeReg.setId(0223);
	storeReg.setCode("22022r");
	storeReg.setName("Goserindo Test Store");
	storeRepo.save(storeReg);

	MemberModel member = new MemberModel();
	final String ACCOUNT_ID = "23452345345";
	member.setAccountId(ACCOUNT_ID);
	member.setAccountStatus(MemberStatus.ACTIVE);
	member.setContact("contact");
	member.setPin("1234securepin");
	member.setUsername("test-member-emp");
	member.setRegisteredStore("22022r");
	member.setCardType(lookupDetailRepo.findByCode("CARDTYPE"));
	member.setMemberType(lookupDetailRepo.findByCode("MTYP001"));
	CustomerProfile customerProfile = new CustomerProfile();
	customerProfile.setBirthdate(new Date());
	member.setCustomerProfile(customerProfile);
	member = memberRepo.save(member);

	PosTransaction pt = new PosTransaction();
	pt.setId("txn220220001");
	pt.setCustomerId(ACCOUNT_ID);
	pt.setSalesDate(new LocalDateTime(2014, 05, 30, 13, 58));
	pt.setStartDate(new LocalDateTime(2014, 05, 30, 13, 58));
	pt.setStatus("COMPLETED");
	pt.setStore(store);
	pt.setTaxAmount(127389.1231);
	pt.setTotalAmount(41234123.3243);
	pt.setTotalDiscount(0.00);
	pt.setTransactionDate(new LocalDateTime(2014, 05, 30, 13, 58));
	pt.setType("SALE");

	PosTransaction savePt = posTransactionRepo.save(pt);

	PointsTxnModel ptm = new PointsTxnModel();
	ptm.setId("23453465");
	ptm.setTransactionType(PointTxnType.EARN);
	ptm.setTransactionPoints(12.00);
	ptm.setTransactionNo("txn220220001");
	ptm.setMemberModel(member);
	ptm.setTransactionAmount(123123.321312);
	ptm.setTransactionDateTime(new Date());

	pointsTxnModelRepo.save(ptm);
	em.flush();
    }

    @Test
    public void createJrProcessorTest() throws Exception {
	Map<String, String> map = Maps.newHashMap();
	map.put(CommonReportFilter.DATE_FROM, "30-05-2014");
	map.put(CommonReportFilter.DATE_TO, "30-05-2014");
	map.put(MemberStoreTransactionReport.FILTER_MEMBER_TYPE, "MTYP001");
	map.put(MemberStoreTransactionReport.FILTER_TX_STORE, "22022");
	JRProcessor jrProcessor = memberStoreTransactionReport.createJrProcessor(map);

	File testClassesDir = FileUtils.toFile(Thread.currentThread().getContextClassLoader().getResource(""));
	File output = new File(testClassesDir, "test-store-member-transaction-report-output.pdf");
	logger.debug("Output location {}", output.getAbsolutePath());
	output.delete();
	Locale locale = LocaleContextHolder.getLocale();
	jrProcessor.addParameter(JRParameter.REPORT_LOCALE, locale);
	jrProcessor.addParameter(JRParameter.REPORT_RESOURCE_BUNDLE, new MessageSourceResourceBundle(messageSource, locale));
	reportService.exportPdfReport(jrProcessor, output);

	double bytes = output.length();
	double kilobytes = (bytes / 1024);
	logger.debug("Kilobytes: {}", kilobytes);
    }

    @After
    public void cleanup() {
	pointsTxnModelRepo.deleteAll();
	posTransactionRepo.deleteAll();
	memberRepo.deleteAll();
	lookupDetailRepo.deleteAll();
	lookupHeaderRepo.deleteAll();
    }

}
