package com.transretail.crm.common.audit;

import java.util.Map;

/**
 * @author Allan G. Ramirez (agramirez@exist.com)
 */
public class AuditMessage {
    private String name;
    private String description;
    private String rawDesription;
    private Map<String, String> auditParamMap;

    public AuditMessage(String name, String description, String rawDescription, Map<String, String> auditParamMap) {
        this.name = name;
        this.description = description;
        this.rawDesription = rawDescription;
        this.auditParamMap = auditParamMap;
    }

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }

    public String getRawDesription() {
        return rawDesription;
    }

    public Map<String, String> getAuditParamMap() {
        return auditParamMap;
    }
}
