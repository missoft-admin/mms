package com.transretail.crm.core.dto;

import java.util.List;

import com.transretail.crm.core.entity.lookup.LookupDetail;

/**
 * @author Allan G. Ramirez (agramirez@exist.com)
 */
public class PointsConfigDto {
    private Long id;
    private Double pointValue;
    private Integer transactionCount;
    private List<LookupDetail> suspendedTypes;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Double getPointValue() {
        return pointValue;
    }

    public void setPointValue(Double pointValue) {
        this.pointValue = pointValue;
    }

    public Integer getTransactionCount() {
        return transactionCount;
    }

    public void setTransactionCount(Integer transactionCount) {
        this.transactionCount = transactionCount;
    }

    public List<LookupDetail> getSuspendedTypes() {
        return suspendedTypes;
    }

    public void setSuspendedTypes(List<LookupDetail> suspendedTypes) {
        this.suspendedTypes = suspendedTypes;
    }
}
