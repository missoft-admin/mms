<%@ include file="../common/taglibs.jsp" %>

<div id="content_storeMemberList">
  <div id="list_store_member"
    data-url="<c:url value="/storemember/list" />"
    data-store="<spring:message code="label_store_member_store"/>" 
    data-company="<spring:message code="label_store_member_company" />" 
    data-card-type="<spring:message code="label_store_member_card_type" />" >
  </div>
  
  <div id="storeMember-list-actions" class="hide">
    <a href="#" class="btn pull-left tiptip icn-edit btn_editStoreMember" data-url="<c:url value="/storemember/edit/" />" data-id="%ID%" title="<spring:message code="label_edit"/>"><spring:message code="label_store_member_form_update"/></a>
    <a href="#" class="btn pull-left tiptip icn-delete btn_deleteStoreMember" data-url="<c:url value="/storemember/delete/" />" data-msg-confirm="<spring:message code="entity_delete_confirm" />" data-id="%ID%" title="<spring:message code="label_delete" />"><spring:message code="label_delete" /></a>
    
  </div>
  
  <jsp:include page="../common/confirm.jsp" />
  
  <link href="<spring:url value="/css/dataTables.css" />" rel="stylesheet">
  <script src="<spring:url value="/js/jquery/jquery.dataTables.js" />" ></script>
  <script src="<spring:url value="/js/jquery/jquery.dataTables.linkLengthMenu.js" />"></script>
  <script src="<spring:url value="/js/jquery/jquery.dataTables.customPaging.js" />"></script>
  <script src="<spring:url value="/js/jquery/ajax-datatable.js" />" ></script>
  <script src="<spring:url value="/js/common/form2js.js" />" ></script>
  <script src="<spring:url value="/js/common/json2.js" />" ></script>
  <script src="<spring:url value="/js/jquery/jquery.toObject.js" />" ></script>
  
  <script src="<spring:url value="/js/viewspecific/storememberscheme/storeMemberList.js"/>"></script>
</div>