package com.transretail.crm.core.service.impl;

import java.util.Collection;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.google.common.collect.Lists;
import com.mysema.query.types.expr.BooleanExpression;
import com.transretail.crm.common.service.util.SpringDataPagingUtil;
import com.transretail.crm.core.dto.CrmInStoreDto;
import com.transretail.crm.core.dto.CrmInStoreResultList;
import com.transretail.crm.core.dto.CrmInStoreSearchForm;
import com.transretail.crm.core.entity.CrmInStore;
import com.transretail.crm.core.entity.QCrmInStore;
import com.transretail.crm.core.repo.CrmInStoreRepo;
import com.transretail.crm.core.service.CrmInStoreService;

/**
 * @author Allan G. Ramirez (agramirez@exist.com)
 */
@Service
public class CrmInStoreServiceImpl implements CrmInStoreService {
    @Autowired
    private CrmInStoreRepo crmInStoreRepo;

    @Transactional(readOnly = true)
    public CrmInStoreDto getCrmInStoreDto(String crmInStoreId) {
        CrmInStore crmInStore = crmInStoreRepo.findOne(crmInStoreId);
        return crmInStore != null ? new CrmInStoreDto(crmInStoreRepo.findOne(crmInStoreId)) : null;
    }

    @Transactional(readOnly = true)
    public CrmInStoreResultList getCrmInStores(CrmInStoreSearchForm searchForm) {
        Pageable pageable = SpringDataPagingUtil.INSTANCE.createPageable(searchForm.getPagination());
        BooleanExpression filter = searchForm.createSearchExpression();
        Page<CrmInStore> page = filter != null ? crmInStoreRepo.findAll(filter, pageable) : crmInStoreRepo.findAll(pageable);
        return new CrmInStoreResultList(toDtos(page.getContent()), page.getTotalElements(), page.hasPreviousPage(),
            page.hasNextPage());
    }

    @Transactional
    @Override
    public CrmInStore save(CrmInStore dto) {
        if (StringUtils.isNotBlank(dto.getPrevId()) && !dto.getPrevId().equalsIgnoreCase(dto.getId())) {
            CrmInStore prevCrmProxyInf = crmInStoreRepo.findOne(dto.getPrevId());
            crmInStoreRepo.delete(prevCrmProxyInf);
            crmInStoreRepo.flush();
        }
        return crmInStoreRepo.save(dto);
    }

    @Transactional(readOnly = true)
    @Override
    public boolean isCodeExists(String code) {
        QCrmInStore qCrmInStore = QCrmInStore.crmInStore;
        return crmInStoreRepo.count(qCrmInStore.id.equalsIgnoreCase(code)) > 0;
    }

    @Transactional(readOnly = true)
    @Override
    public boolean isNameExists(String excludeCode, String name) {
        QCrmInStore qCrmInStore = QCrmInStore.crmInStore;
        BooleanExpression predicate = qCrmInStore.name.equalsIgnoreCase(name);
        if (StringUtils.isNotBlank(excludeCode)) {
            predicate = predicate.and(qCrmInStore.id.notEqualsIgnoreCase(excludeCode));
        }
        return crmInStoreRepo.count(predicate) > 0;
    }

    @Transactional(readOnly = true)
    @Override
    public boolean isIpExists(String excludeCode, String ip) {
        QCrmInStore qCrmInStore = QCrmInStore.crmInStore;
        BooleanExpression predicate = qCrmInStore.ip.equalsIgnoreCase(ip);
        if (StringUtils.isNotBlank(excludeCode)) {
            predicate = predicate.and(qCrmInStore.id.notEqualsIgnoreCase(excludeCode));
        }
        return crmInStoreRepo.count(predicate) > 0;
    }

    private Collection<CrmInStoreDto> toDtos(Collection<CrmInStore> crmInStores) {
        List<CrmInStoreDto> lists = Lists.newArrayList();
        for (CrmInStore crmInStore : crmInStores) {
            lists.add(new CrmInStoreDto(crmInStore));
        }
        return lists;

    }
}
