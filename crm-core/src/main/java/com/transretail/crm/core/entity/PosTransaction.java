package com.transretail.crm.core.entity;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.Immutable;
import org.hibernate.annotations.Type;
import org.joda.time.LocalDateTime;
import org.springframework.format.annotation.DateTimeFormat;

import com.transretail.crm.core.entity.lookup.Store;

@Table(name = "POS_TRANSACTION")
@Entity
@Immutable
public class PosTransaction {
    @Id
    @Column(name = "ID")
    private String id;

    @Temporal(TemporalType.TIMESTAMP)
    @DateTimeFormat(style = "M-")
    @Column(name = "TRANSACTION_DATE")
    @Type(type = "org.jadira.usertype.dateandtime.joda.PersistentLocalDateTime")
    private LocalDateTime transactionDate;
    @Temporal(TemporalType.TIMESTAMP)
    @DateTimeFormat(style = "M-")
    @Column(name = "SALES_DATE")
    @Type(type = "org.jadira.usertype.dateandtime.joda.PersistentLocalDateTime")
    private LocalDateTime salesDate;
    
    @Temporal(TemporalType.TIMESTAMP)
    @DateTimeFormat(style = "M-")
    @Column(name = "START_DATE")
    @Type(type = "org.jadira.usertype.dateandtime.joda.PersistentLocalDateTime")
    private LocalDateTime startDate;

    @ManyToOne(targetEntity = Store.class, cascade = CascadeType.ALL)
    @JoinColumn(name = "STORE_ID")
    private Store store;

    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, targetEntity = PosTxItem.class, mappedBy = "posTransaction")
    private List<PosTxItem> orderItems = new ArrayList<PosTxItem>();
    
    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, targetEntity = PosPayment.class, mappedBy = "posTransaction")
    private List<PosPayment> payments = new ArrayList<PosPayment>();
    @Column(name="CUSTOMER_ID")
    private String customerId;
    @Column(name = "TOTAL_AMOUNT")
    private Double totalAmount;
    @Column(name = "TOTAL_TAXABLE_AMOUNT")
    private Double totalTaxableAmount;
    @Column(name = "VAT")
    private Double taxAmount;
    @Column(name = "TOTAL_DISCOUNT")
    private Double totalDiscount;
    @Column(name = "VOIDED_DISCOUNT")
    private Double voidedDiscount;
    @Column(name = "TOTAL_NON_MEMBER_MARKUP")
    private Double totalNonMemberMarkup;
    @Column(name = "TYPE")
    private String type;
    @Column(name = "status")
    private String status;

    //removing this as per DEN issue 97870
    //@Column(name = "BASE_REFERENCE")
    //private String baseReference;
        
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public LocalDateTime getTransactionDate() {
		return transactionDate;
	}

	public void setTransactionDate(LocalDateTime transactionDate) {
		this.transactionDate = transactionDate;
	}

	public Store getStore() {
		return store;
	}

	public void setStore(Store store) {
		this.store = store;
	}

	public List<PosTxItem> getOrderItems() {
		return orderItems;
	}

	public void setOrderItems(List<PosTxItem> orderItems) {
		this.orderItems = orderItems;
	}

	public List<PosPayment> getPayments() {
		return payments;
	}

	public void setPayments(List<PosPayment> payments) {
		this.payments = payments;
	}

	public LocalDateTime getStartDate() {
		return startDate;
	}

	public void setStartDate(LocalDateTime startDate) {
		this.startDate = startDate;
	}
	
	
	
	
    /*
    private String userId;
    
    @NotNull
    private String type;

    @NotNull
    @Size(max = 20)
    private String status;

    private double totalQuantity;

    private double totalAmount;

    @NotNull
    @Size(max = 20)
    private String customerId;

    private double totalAmountPaid;

    private double totalChange;
    
    */

    public String getCustomerId() {
	return customerId;
    }

    public void setCustomerId(String customerId) {
	this.customerId = customerId;
    }

    public Double getTotalAmount() {
	return totalAmount;
    }

    public void setTotalAmount(Double totalAmount) {
	this.totalAmount = totalAmount;
    }

    public Double getTotalTaxableAmount() {
	return totalTaxableAmount;
    }

    public void setTotalTaxableAmount(Double totalTaxableAmount) {
	this.totalTaxableAmount = totalTaxableAmount;
    }

    public Double getTaxAmount() {
	return taxAmount;
    }

    public void setTaxAmount(Double taxAmount) {
	this.taxAmount = taxAmount;
    }

    public Double getTotalDiscount() {
	return totalDiscount;
    }

    public void setTotalDiscount(Double totalDiscount) {
	this.totalDiscount = totalDiscount;
    }

    public Double getVoidedDiscount() {
	return voidedDiscount;
    }

    public void setVoidedDiscount(Double voidedDiscount) {
	this.voidedDiscount = voidedDiscount;
    }

    public Double getTotalNonMemberMarkup() {
	return totalNonMemberMarkup;
    }

    public void setTotalNonMemberMarkup(Double totalNonMemberMarkup) {
	this.totalNonMemberMarkup = totalNonMemberMarkup;
    }

    public String getType() {
	return type;
    }

    public void setType(String type) {
	this.type = type;
    }

    public String getStatus() {
	return status;
    }

    public void setStatus(String status) {
	this.status = status;
    }

    public LocalDateTime getSalesDate() {
	return salesDate;
    }

    public void setSalesDate(LocalDateTime salesDate) {
	this.salesDate = salesDate;
    }

    /*
	public String getBaseReference() {
		return baseReference;
	}

	public void setBaseReference(String baseReference) {
		this.baseReference = baseReference;
	}
	*/
}
