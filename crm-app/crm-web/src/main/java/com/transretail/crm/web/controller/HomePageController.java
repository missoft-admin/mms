package com.transretail.crm.web.controller;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import com.transretail.crm.core.service.EmployeeService;

/**
 * @author ftopico
 */
@Controller
@RequestMapping("/")
public class HomePageController {

    public static final String UI_ATTR_EMPLOYEE_STATS = "employeeStats";
    @Autowired
    private EmployeeService employeeService;
    
    @RequestMapping(value = "/")
    public String getEmployeeStats(Model uiModel) {
        
        Map<String, Object> employeeStats = new HashMap<String, Object>();
        long individuals = employeeService.countAllEmployeesAsIndividuals();
        long professionals = employeeService.countAllEmployeesAsProfessionals();
        
        employeeStats.put("numberOfEmployees", employeeService.countAllEmployees());
        employeeStats.put("numberOfIndividuals", individuals);
        employeeStats.put("numberOfProfessionals", professionals);
        employeeStats.put("numberOfTotalCustomers", individuals + professionals);
        employeeStats.put("numberOfStoreMembers", employeeService.countCurrentUserStoreMembers());
        uiModel.addAttribute(UI_ATTR_EMPLOYEE_STATS, employeeStats);
        
        return "index";
    }
}
