package com.transretail.crm.giftcard.service;

import java.math.BigDecimal;
import java.util.List;

import com.transretail.crm.common.reporting.jasper.JRProcessor;
import com.transretail.crm.common.service.dto.request.PageSortDto;
import com.transretail.crm.common.service.dto.response.ResultList;
import com.transretail.crm.core.entity.lookup.LookupDetail;
import com.transretail.crm.giftcard.dto.B2BExhibitionCountDto;
import com.transretail.crm.giftcard.dto.B2BExhibitionDto;



public interface B2bExhibitionService {

	List<Long> getProdProfByBarcode(String startingSeries, String endingSeries);

	List<B2BExhibitionCountDto> countProducts(B2BExhibitionDto exhibitionForm);

	List<LookupDetail> getPaymentTypes();

	B2BExhibitionDto saveExhTransaction(B2BExhibitionDto dto);

	B2BExhibitionDto getExhTransaction(Long id);

	ResultList<B2BExhibitionDto> listExhTransaction(PageSortDto sortDto);

	JRProcessor createJrProcessor(Long id);

	BigDecimal getTotalFaceAmount(List<B2BExhibitionCountDto> cnts);

	void activateGcs(B2BExhibitionDto dto);

	B2BExhibitionDto save(B2BExhibitionDto dto);

	JRProcessor createKwitansiJrProcessor(Long id);
	
	
}
