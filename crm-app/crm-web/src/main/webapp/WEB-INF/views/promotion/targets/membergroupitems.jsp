<%@ include file="../../common/taglibs.jsp" %>

 
 	<div class="contentTable">
		<table class="tableData targetTable">
			<thead>
				<tr>
					<th class="targetSquare"><![CDATA[&nbsp;]]></th>
					<th><spring:message code="label_group_membergroup"/></th>
				</tr>
			</thead>
			<tbody>
				<c:forEach var="item" items="${memberGroups}" >
				<tr class="memberGroupContainer">
					<td class="targetSquare"><input type="checkbox" class="targetCheckbox" name="targetMemberGroups" value="${item.id}"></td>
					<td><span class="targetText"><c:out value="${item.name}" /></span></td>
				</tr>
				</c:forEach>
			</tbody>
		</table>
	</div>
	
