package com.transretail.crm.core.entity;


import org.hibernate.annotations.Immutable;

import javax.persistence.*;


@Entity(name = "ELECTRONIC_FUND_TRANSFER")
@Immutable
public class PosEftPayment {

	@Id
	@Column(name = "EFT_ID")
    private String eftId;

    @OneToOne(targetEntity = PosPayment.class, cascade = CascadeType.ALL)
    @JoinColumn(name = "POS_PAYMENT_ID", unique=true)
    private PosPayment posPayment;

	@Column(name = "APPL_NAME")
    private String applName;

	@Column(name = "BANK_NAME")
    private String bankName;

    @Column(name = "CARD_NUM")
    private String cardNo;


	public String getEftId() {
		return eftId;
	}

	public void setEftId(String eftId) {
		this.eftId = eftId;
	}

	public PosPayment getPosPaymentId() {
		return posPayment;
	}

	public void setPosPaymentId(PosPayment posPayment) {
		this.posPayment = posPayment;
	}

	public String getApplName() {
		return applName;
	}

	public void setApplName(String applName) {
		this.applName = applName;
	}

	public String getBankName() {
		return bankName;
	}

	public void setBankName(String bankName) {
		this.bankName = bankName;
	}

    public String getCardNo() {
        return cardNo;
    }

    public void setCardNo(String cardNo) {
        this.cardNo = cardNo;
    }
}
