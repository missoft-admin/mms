package com.transretail.crm.marketing.dto;

import java.math.BigDecimal;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * @author Allan G. Ramirez (agramirez@exist.com)
 */
@JsonIgnoreProperties(value = "totalSpendBd")
public class DecileReportDto {
    private int no;
    private int numberOfCustomers;
    private double minSpend;
    private double maxSpend;
    private double aveTxnCount;
    private double aveTxnSize;
    private double aveSpendPerMonth;
    private BigDecimal totalSpendBd;
    private double percentage;
    private String memberIds;

    public int getNo() {
        return no;
    }

    public void setNo(int no) {
        this.no = no;
    }

    public int getNumberOfCustomers() {
        return numberOfCustomers;
    }

    public void setNumberOfCustomers(int numberOfCustomers) {
        this.numberOfCustomers = numberOfCustomers;
    }

    public double getMinSpend() {
        return minSpend;
    }

    public void setMinSpend(double minSpend) {
        this.minSpend = minSpend;
    }

    public double getMaxSpend() {
        return maxSpend;
    }

    public void setMaxSpend(double maxSpend) {
        this.maxSpend = maxSpend;
    }

    public double getAveTxnCount() {
        return aveTxnCount;
    }

    public void setAveTxnCount(double aveTxnCount) {
        this.aveTxnCount = aveTxnCount;
    }

    public double getAveTxnSize() {
        return aveTxnSize;
    }

    public void setAveTxnSize(double aveTxnSize) {
        this.aveTxnSize = aveTxnSize;
    }

    public double getAveSpendPerMonth() {
        return aveSpendPerMonth;
    }

    public void setAveSpendPerMonth(double aveSpendPerMonth) {
        this.aveSpendPerMonth = aveSpendPerMonth;
    }

    public BigDecimal getTotalSpendBd() {
        return totalSpendBd;
    }

    public void setTotalSpendBd(BigDecimal totalSpendBd) {
        this.totalSpendBd = totalSpendBd;
    }

    public double getTotalSpend() {
        return totalSpendBd.doubleValue();
    }

    public double getPercentage() {
        return percentage;
    }

    public void setPercentage(double percentage) {
        this.percentage = percentage;
    }

    public String getMemberIds() {
        return memberIds;
    }

    public void setMemberIds(String memberIds) {
        this.memberIds = memberIds;
    }
}
