<%@ include file="../common/taglibs.jsp" %>


  <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title"><spring:message code="label_com_transretail_crm_entity_customermodel_export_member"/></h4>
      </div>
    
      <div class="modal-body">
        <div id="contentError" class="hide alert alert-error">
          <button type="button" class="close" onclick="$(this).parent().hide();">&times;</button>
          <div><form:errors path="*"/></div>
        </div>
        <div class="span5 center">
          <div class="control-group">
            <label class="control-label"><spring:message code="label_com_transretail_crm_entity_customermodel_export_member_count" arguments="${memberCount}"/></label>
            <label class="control-label"><spring:message code="label_com_transretail_crm_entity_customermodel_export_batch"/></label>
          </div>
          
          <div class="control-group">
            <select id="segment" path="segment" class="input-small pull-left">
             <c:forEach var="item" items="${segments}">
              <option value="${item}">${item}</option>
             </c:forEach>
            </select>
          </div>
        </div>
      </div>

      <div class="modal-footer">
        <button type="button" id="print" class="btn btn-primary"><spring:message code="label_print"/></button>
        <button type="button" id="cancel" class="btn btn-default" data-dismiss="modal"><spring:message code="label_cancel"/></button>
      </div>
  </div>
  

<script type="text/javascript">
$(document).ready(function() {
  var CONTENT_ERROR = $("#contentError");
  initButtons();
  initForms();
  
  function initForms() {
  }

  function initButtons() {
    $("#print").click(function(e) {
      e.preventDefault();
/** Format of link
 ** /member/print/{segment}/{membertType}/{searchType}/{searchValue}/{status}/{store} 
 **/
	  var segment = "segment=" + $("#segment").val();
	  var memberType = "&memberType=" + $("#memberType").val();
	  var searchType = "&searchType=" + $("#search_memberCriteria").val();
	  var searchValue = "&searchValue=" + $("#search_memberField").val();
	  var status = "&status=" + $("#status").val();
	  var store = "&store=" + $("#registeredStore").val();
	  var url = ctx + "/report/member/print?"
        		+ segment
        		+ memberType
        		+ searchType;
	  
	  if ($("#search_memberField").val() != "") {
	    url = url + searchValue;
	  }
	  
	  if ($("#status").val() != "") {
	    url = url + status;
	  }
	  
	  if ($("#registeredStore").val() != "") {
	    url = url + store;
	  }
	  
      var completeUrl = url;
  	
      $("#print").attr("disabled", true);
      window.location.href = completeUrl;
      $("#cancel").click();
    });
  }

});
</script>