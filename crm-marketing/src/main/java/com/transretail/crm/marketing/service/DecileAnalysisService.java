package com.transretail.crm.marketing.service;

import java.util.List;

import com.transretail.crm.marketing.dto.DecileReportDto;
import com.transretail.crm.marketing.dto.YearMonthDto;

/**
 * @author Allan G. Ramirez (agramirez@exist.com)
 */
public interface DecileAnalysisService {
    List<DecileReportDto> getDecileReport(YearMonthDto yearMonth);
}
