<%@ include file="../../common/taglibs.jsp" %>
<div class="modal-dialog">
	<div class="modal-content">
		<div class="modal-header">
			<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
			<h4>
				<spring:message code="create_profile" />
			</h4>
		</div>
		<div class="modal-body">
			<div id="contentError" class="hide alert alert-error">
				<button type="button" class="close" onclick="$(this).parent().hide();">&times;</button>
				<div>
					<form:errors path="*" />
				</div>
			</div>
			<div class="row-fluid">
				<form:form id="profileForm" name="profileForm" modelAttribute="profileForm"
						   action="${pageContext.request.contextPath}/gc/productprofile/save/" method="POST"
						   cssClass="form-reset form-horizontal">
					<form:hidden path="id" />

					<div class="span6">
						<div class="control-group">
							<spring:message code="profile_product_code" var="prodCode" />
							<label for="productCode" class="control-label"><b class="required">*</b>${prodCode}</label>
							<div class="controls">
								<form:input path="productCode" maxlength="13" class="form-control intInput not-editable" placeholder="${prodCode}" />
							</div>
						</div>
						<div class="control-group">
							<spring:message code="profile_product_desc" var="prodDesc" />
							<label for="productDesc" class="control-label"><b class="required">*</b>${prodDesc}</label>
							<div class="controls">
								<form:input path="productDesc" class="form-control float" placeholder="${prodDesc}" />
							</div>
						</div>
						<div class="control-group">
							<spring:message code="profile_face_value" var="faceVal" />
							<label for="faceValue" class="control-label">${faceVal}</label>
							<div class="controls">
								<c:if test="${!edit}">
									<form:select path="faceValue" class="form-control">
										<c:forEach items="${faceValues}" var="item">
											<form:option value="${item.code}" label="${item.description}" />
										</c:forEach>
									</form:select>
								</c:if>
								<c:if test="${edit}">
									<form:select path="faceValue" class="form-control" disabled="true">
										<c:forEach items="${faceValues}" var="item">
											<form:option value="${item.code}" label="${item.description}" />
										</c:forEach>
									</form:select>
									<form:hidden path="faceValue" />
								</c:if>
							</div>
						</div>
						<div class="control-group">
							<spring:message code="profile_card_fee" var="crdFee" />
							<label for="cardFee" class="control-label">${crdFee}</label>
							<div class="controls">
								<form:input path="cardFee" class="form-control float" placeholder="${crdFee}" />
							</div>
						</div>
						<div class="control-group">
							<spring:message code="profile_max_amount" var="maxAmnt" />
							<label for="maxAmount" class="control-label">${maxAmnt}</label>
							<div class="controls">
								<form:input path="maxAmount" class="form-control intInput float" placeholder="${maxAmnt}" />
								<br /> <span class="text-error max-amount-err" style="display: none;"><spring:message code="required.maxamount" /></span>
							</div>
						</div>
						<div class="control-group">
							<spring:message code="profile_effective_months" var="effectvMos" />
							<label for="effectiveMonths" class="control-label">${effectvMos}</label>
							<div class="controls">
								<form:input path="effectiveMonths" class="form-control intInput float" placeholder="${effectvMos}" />
							</div>
						</div>
						<div class="control-group">
							<spring:message code="profile_unit_cost" var="unitCst" />
							<label for="unitCost" class="control-label"><b class="required">*</b>${unitCst}</label>
							<div class="controls">
								<form:input path="unitCost" class="form-control floatInput" placeholder="${unitCst}" />
							</div>
						</div>
						<div class="control-group">
							<label for="cardFee" class="control-label">&nbsp;</label>
							<div class="controls">
								<div class="span checkbox">
									<c:if test="${empty edit}">
										<form:checkbox path="allowReload" />
										<label for="allowReload"><spring:message code="profile_allow_reload" /></label>
									</c:if>
									<c:if test="${not empty edit}">
										<form:checkbox path="allowReload" disabled="true" />
										<label for="allowReload"><spring:message code="profile_allow_reload" /></label>
										<form:hidden path="allowReload" />
									</c:if>
								</div>
								<div class="span checkbox">
									<c:if test="${empty edit}">
										<form:checkbox path="allowPartialRedeem" />
										<label for="allowReload"><spring:message code="profile_allow_partial_redeem" /></label>
									</c:if>
									<c:if test="${not empty edit}">
										<form:checkbox path="allowPartialRedeem" disabled="true" />
										<label for="allowReload"><spring:message code="profile_allow_partial_redeem" /></label>
										<form:hidden path="allowPartialRedeem" />
									</c:if>
								</div>
								<div class="span checkbox">
									<c:if test="${empty edit}">
										<form:checkbox path="isEgc" />
										<label for="isEgc"><spring:message code="order_isegc" /></label>
									</c:if>
									<c:if test="${not empty edit}">
										<form:checkbox path="isEgc" disabled="true" />
										<label for="isEgc"><spring:message code="order_isegc" /></label>
										<form:hidden path="isEgc" />
									</c:if>
								</div>
							</div>
						</div>
					</div>
					<div class="span6">
						<div id="businessUnitDiv" class="control-group">
							<div class="pull-left" style="margin-right: 5px;">
								<button type="button" id="addUnitBtn" class="btn btn-small not-editable-hidden">+</button>
								<br />
								<button type="button" id="removeUnitBtn" class="btn btn-small not-editable-hidden">&ndash;</button>
							</div>
							<div class="pull-left" style="width: 90%;">
								<table id="buTable" class="table table-condensed table-compressed table-striped">
									<thead>
										<tr>
											<th></th>
											<th>Business Unit</th>
										</tr>
									</thead>
									<tbody>
										<c:forEach var="bu" items="${profileForm.productUnit}" varStatus="status">
											<tr>
												<td>
													<input type="checkbox" class="not-editable-hidden">
													<input type="hidden" name="buIds" value="${bu.id}" />
												</td>
												<td>
													<select name="productUnitIds" class="float">
														<c:forEach items="${businessUnits}" var="businessUnit">
															<option value="${businessUnit.code}" ${bu.businessUnit.code==businessUnit.code?"selected":""}>${businessUnit.codeAndDesc}</option>
														</c:forEach>
													</select>
												</td>
											</tr>
										</c:forEach>
									</tbody>
								</table>
							</div>
						</div>
						<div id="reloadDiv" class="control-group <c:if test="${!profileForm.allowReload}">hide</c:if>">
							<div class="pull-left" style="margin-right: 5px;">
								<button type="button" id="addReloadBtn" class="btn btn-small not-editable-hidden">+</button>
								<br />
								<button type="button" id="removeReloadBtn" class="btn btn-small not-editable-hidden">&ndash;</button>
							</div>
							<div class="pull-left" style="width: 90%;">
								<span class="max-amount-exceed-err text-error" style="display: none;"><spring:message code="exceeds.maxamount" /></span> <span class="amount-dup-err text-error" style="display: none;"><spring:message code="reload.amount.unique" /></span> <br />
								<table id="reloadTable" class="table table-condensed table-compressed table-striped">
									<thead>
										<tr>
											<th></th>
											<th><spring:message code="reload.amount" /></th>
											<th><b class="required">*</b>
											<spring:message code="effective.month" /></th>
										</tr>
									</thead>
									<tbody>
										<c:forEach var="reloadInfoDto" items="${profileForm.reloadInfoDtos}" varStatus="status">
											<tr>
												<td><input type="checkbox"> <input type="hidden" name="reloadIds" value="${reloadInfoDto.id}" /></td>
												<td><input type="text" class="reloadAmount" name="reloadAmount" value="${reloadInfoDto.reloadAmount}" /></td>
												<td><input type="text" name="reloadInfoDtos[${status.index}].monthEffective" value="${reloadInfoDto.monthEffective}" /></td>
											</tr>
										</c:forEach>
									</tbody>
								</table>
							</div>
						</div>
					</div>
				</form:form>
			</div>
		</div>
		<div class="modal-footer">
			<c:if test="${profileForm.status != 'SUBMITTED' and profileForm.status != 'APPROVED'}">
				<button type="button" data-status="SUBMITTED" class="profileSubmit btn btn-primary">
					<spring:message code="label_submit_for_approval" />
				</button>
				<button type="button" data-status="DRAFT" class="profileSubmit btn btn-primary">
					<spring:message code="label_draft" />
				</button>
			</c:if>
			<c:if test="${profileForm.status == 'SUBMITTED'}">
				<button type="button" data-status="APPROVED" class="profileSubmit btn btn-primary">
					<spring:message code="label_approve" />
				</button>
				<button type="button" data-status="REJECTED" class="profileSubmit btn btn-primary">
					<spring:message code="label_reject" />
				</button>
			</c:if>
			<c:if test="${profileForm.status != 'APPROVED'}">
				<button type="button" data-status="APPROVED" class="profileSubmit btn btn-primary">
					<spring:message code="label_save" />
				</button>
			</c:if>
			<c:if test="${edit}">
				<button type="button" data-status="APPROVED"  class="profileSubmit btn btn-primary">
					<spring:message code="label_save" />
				</button>
			</c:if>
			<button type="button" id="profileCancel" class="btn btn-default" data-dismiss="modal">
				<spring:message code="label_cancel" />
			</button>
		</div>
	</div>
</div>
<style>
	#reloadTable input[type="text"] {
		width: 150px;
	}
</style>
<script src="<spring:url value="/js/jquery/jquery.numberInput.js" />"></script>
<script>
	$(document).ready(function() {
		var CONTAINER_ERROR = "#contentError > div";
		var CONTENT_ERROR = "#contentError";
		var $form = $("#profileForm");
		var $orderDialog = $("#orderDialog");

		$(".profileSubmit").click(submitForm);

		if ("${edit}") {
			$(".not-editable").attr("readonly", true);
			$("select.not-editable").attr("disabled", true);
			$(".not-editable-hidden").css("display", "none");
		}

		function submitForm() {
			$.post($form.attr("action")+ $(this).data("status"),$form.serialize(),function(data) {
				if (data.success) {
					location.reload();
				} else {
					errorInfo = "";
					for (i = 0; i < data.result.length; i++) {
						errorInfo += "<br>"+ (i + 1)+ ". "+ (data.result[i].code != undefined ? data.result[i].code: data.result[i]);
					}
					$orderDialog.find(CONTAINER_ERROR).html("Please correct following errors: "+ errorInfo);
					$orderDialog.find(CONTENT_ERROR).show('slow');
				}
			});
		}

		$('.intInput', $orderDialog).numberInput({
			"type" : "int"
		});
		$('.floatInput', $orderDialog).numberInput({
			"type" : "float"
		});

		var $reloadDiv = $('#reloadDiv');
		var $reloadTable = $('#reloadTable');

		$('input[type="text"]', $reloadTable).numberInput();

		function computeTotalReloadAmount() {
			var total = 0;
			$('input.reloadAmount', $reloadTable).each(function(index, Element) {
				if (Element.value != '') {
					total += parseInt(Element.value);
				}
			});
			return total;
		}

		var totalReloadAmount = computeTotalReloadAmount();

		var $allowReload = $('input[name="allowReload"]', $form);
		var $allowPartialRedeem = $('input[name="allowPartialRedeem"]', $form);
		var $maxAmount = $('#maxAmount').keyup(function(e) {
			if ($allowReload.is(':checked')) {
				var maxAmount = e.target.value;
				if (maxAmount != '') {
					maxAmount = parseInt(maxAmount);
					if (maxAmount == 0) {
						$('.max-amount-err').show();
					} else if (maxAmount < totalReloadAmount) {
						$('.max-amount-exceed-err').show();
					} else {
						$('.max-amount-err').hide();
						$('.max-amount-exceed-err').hide();
					}
				} else {
					$('.max-amount-err').show();
				}
			}
		});
		$allowReload.click(function(e) {
			if (this.checked) {
				$allowPartialRedeem.prop('checked',true);
				var maxAmount = $maxAmount.val();
				if (maxAmount == '' || parseInt(maxAmount) <= 0) {
					$('.max-amount-err').show();
					e.preventDefault();
					return false;
				}
				$reloadDiv.removeClass('hide');
			} else {
				$reloadDiv.addClass('hide');
			}
		});
		$allowPartialRedeem.click(function(e) {
			if ($allowReload.prop('checked')) {
				$allowPartialRedeem.prop('checked', true);
			} else {
				$allowPartialRedeem.prop('checked', false);
			}
		});
		$reloadTable.on('blur','input.reloadAmount',function(e) {
			var maxAmount = $maxAmount.val();
			if (maxAmount != '')
				maxAmount = parseInt(maxAmount);
			else
				maxAmount = 0;

			var reloadAmount = e.target.value;
			if (reloadAmount != '') {
				reloadAmount = parseInt(reloadAmount);
				if (reloadAmount > 0) {
					totalReloadAmount += reloadAmount;
					if (reloadAmount > maxAmount)
						$('.max-amount-exceed-err',$reloadDiv).show();
					else
						$('.max-amount-exceed-err',$reloadDiv).hide();
					var total = 0;
					$('input.reloadAmount',$reloadTable).each(function(index,Element) {
						if (Element.value != ''&& reloadAmount == parseInt(Element.value)) {
							total += 1;
						}
					});
					if (total > 1) {
						$('.amount-dup-err',$reloadDiv).show();
					} else {
						$('.amount-dup-err',$reloadDiv).hide();
					}
				}
			}
		});

		$('#addReloadBtn').click(function() {
			var index = $('tbody tr',$reloadTable).length;
			var htmlString = '<tr>';
			htmlString += '<td><input type="checkbox"><input type="hidden" name="reloadIds" value="null"></td>';
			htmlString += '<td><input type="text" class="reloadAmount" name="reloadAmount"></td>';
			htmlString += '<td><input type="text" name="monthEffective"></td>';
			htmlString += '</tr>';
			var $htmlString = $(htmlString);
			$htmlString.appendTo($('tbody',$reloadTable));
			$('input[type="text"]', $htmlString).numberInput();
		});
		$('#removeReloadBtn').click(function() {
			$('input:checked', $reloadTable).each(function(index, Element) {
				$(Element).parents('tr').remove();
			});
		});
	});
</script>
<script type="text/javascript">
	$(document).ready(function(){
		var $reloadDiv = $('#businessUnitDiv');
		var $reloadTable = $('#buTable');

		$('#addUnitBtn').click(function() {
			var url="<c:url value='/lookup/list/STO000'/>";
			$.get(url,function(data){
				var select="<select name='productUnitIds'>";
				for(var i=0;i<data.results.length;i++){
					var look=data.results[i];
					select+="<option value='"+look.code+"'>"+look.codeAndDesc+"</option>";
				}
				select+="</select>";

				var index = $('tbody tr',$reloadTable).length;
				var htmlString = '<tr>';
				htmlString += '<td><input type="checkbox"><input type="hidden" name="buIds" value="null"/></td>';
				htmlString += '<td>'+select+'</td>';
				htmlString += '</tr>';
				var $htmlString = $(htmlString);
				$htmlString.appendTo($('tbody',$reloadTable));
			},"json");
		});
		$('#removeUnitBtn').click(function() {
			$('input:checked', $reloadTable).each(function(i, e) {
				$(e).parents('tr').remove();
			});
		});
	});
</script>