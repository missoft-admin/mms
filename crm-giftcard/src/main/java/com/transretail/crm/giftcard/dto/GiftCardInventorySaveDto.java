package com.transretail.crm.giftcard.dto;

import org.joda.time.LocalDate;

/**
 * @author Allan G. Ramirez (agramirez@exist.com)
 */
public class GiftCardInventorySaveDto {
    private LocalDate expiryDate;
    private GiftCardSeriesDto seriesFrom;
    private GiftCardSeriesDto seriesTo;
    private String locationCode;
    private String allocationCode;

    public LocalDate getExpiryDate() {
        return expiryDate;
    }

    public void setExpiryDate(LocalDate expiryDate) {
        this.expiryDate = expiryDate;
    }

    public GiftCardSeriesDto getSeriesFrom() {
        return seriesFrom;
    }

    /**
     * Series number should conform to format: yyMMddbbfsssssss
     *
     * @param seriesFrom series number
     */
    public void setSeriesFrom(String seriesFrom) {
        this.seriesFrom = new GiftCardSeriesDto(seriesFrom);
    }

    public GiftCardSeriesDto getSeriesTo() {
        return seriesTo;
    }

    /**
     * Series number should conform to format: yyMMddbbfsssssss
     *
     * @param seriesTo series number
     */
    public void setSeriesTo(String seriesTo) {
        this.seriesTo = new GiftCardSeriesDto(seriesTo);
    }

    public String getLocationCode() {
        return locationCode;
    }

    public void setLocationCode(String locationCode) {
        this.locationCode = locationCode;
    }

    public String getAllocationCode() {
        return allocationCode;
    }

    public void setAllocationCode(String allocationCode) {
        this.allocationCode = allocationCode;
    }
}
