package com.transretail.crm.core.dto;

import java.util.Date;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.joda.time.LocalDate;

import com.google.common.collect.Lists;
import com.mysema.query.types.expr.BooleanExpression;
import com.transretail.crm.common.service.dto.request.AbstractSearchFormDto;
import com.transretail.crm.core.entity.QPointsTxnModel;

public class MemberMonitorSearchDto extends AbstractSearchFormDto {
	private String memberType;
	private LocalDate date;
	private Integer transactionCount;
	
	public MemberMonitorSearchDto() {
		
	}
	
	@Override
	public BooleanExpression createSearchExpression() {
		QPointsTxnModel points = QPointsTxnModel.pointsTxnModel;
		List<BooleanExpression> exprs = Lists.newArrayList();
		
		if(StringUtils.isNotBlank(memberType)) {
			exprs.add(points.transactionContributer.memberType.code.eq(memberType));
		}
		
		LocalDate today = date == null ? LocalDate.now() : date;
		Date todayDate = today.toDateTimeAtStartOfDay().toDate();
		Date tomorrowDate = today.plusDays(1).toDateTimeAtStartOfDay().toDate();
		exprs.add(points.transactionDateTime.between(todayDate, tomorrowDate));
		return BooleanExpression.allOf(exprs.toArray(new BooleanExpression[exprs.size()]));
	}

	public String getMemberType() {
		return memberType;
	}

	public void setMemberType(String memberType) {
		this.memberType = memberType;
	}

	public LocalDate getDate() {
		return date;
	}

	public void setDate(LocalDate date) {
		this.date = date;
	}

	public Integer getTransactionCount() {
		return transactionCount;
	}

	public void setTransactionCount(Integer transactionCount) {
		this.transactionCount = transactionCount;
	}
}
