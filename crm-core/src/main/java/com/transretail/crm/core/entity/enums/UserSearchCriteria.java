package com.transretail.crm.core.entity.enums;

public enum UserSearchCriteria {

	username, email, name
}
