
alter table CRM_GC_INVENTORY 
        add constraint UK_7xxguncy50cj4hqdcgb2d2sod unique (BARCODE);

    alter table CRM_GC_INVENTORY 
        add constraint UK_3dfjvi916kc3x84lkadua7j13 unique (SERIES);


alter table CRM_GC_INVENTORY 
        add constraint FK_kxdawmmiocjfxhtam9cih1c4s 
        foreign key (ORDER_ID) 
        references CRM_GC_ORD;

    alter table CRM_GC_INVENTORY 
        add constraint FK_ade12xow89h6uc5f1bvxm6o35 
        foreign key (PROFILE_ID) 
        references CRM_GC_PROD_PROFILE;