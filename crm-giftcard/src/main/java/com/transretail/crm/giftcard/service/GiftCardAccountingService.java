package com.transretail.crm.giftcard.service;

import java.math.BigDecimal;

import org.joda.time.DateTime;

import com.transretail.crm.giftcard.dto.B2BExhibitionDto;
import com.transretail.crm.giftcard.dto.ReturnRecordDto;
import com.transretail.crm.giftcard.dto.SalesOrderDto;
import com.transretail.crm.core.entity.CrmForPeoplesoftConfig;
import com.transretail.crm.core.entity.CrmForPeoplesoftConfig.TransactionType;


public interface GiftCardAccountingService {

	void saveAcctTxn(TransactionType transactionType, String mmsAcct, String psAcct,
			String type, DateTime txnDate, BigDecimal txnAmt, String storeCode, String deptId, String notes, boolean setDefaultBu);

	void forGiftToCust(DateTime txnDate, BigDecimal txnAmt,
			String storeCode, String notes);

	CrmForPeoplesoftConfig getConfig(TransactionType txnType);

	void forB2BPayment(DateTime txnDate, BigDecimal txnAmt,
			String storeCode, String notes);


	void forAdvSoVerif(DateTime txnDate, BigDecimal txnAmt, String notes, String storeCode);

	void forAdvSoVerifApproval(DateTime txnDate, BigDecimal txnAmt, String notes);

	void forAdvSoVerifCancelled(DateTime txnDate, BigDecimal txnAmt,
			String notes);

	void forB2BInternalSOActivation(DateTime txnDate, BigDecimal txnAmt,
			String deptId, String notes);

	void forB2BReturn(String orderNo);

	void forB2BActivation(SalesOrderDto orderDto);
	
	void forB2CActivation(DateTime txnDate, BigDecimal txnAmt, String storeCode, String notes);
	
        void forReloadSales(DateTime txnDate, BigDecimal txnAmt, String storeCode, String notes);
        
	void forB2CSalesVoiding(DateTime txnDate, BigDecimal txnAmt, String storeCode, String notes);
        
	void forReloadSalesVoiding(DateTime txnDate, BigDecimal txnAmt, String storeCode, String notes);
        
	void forB2CRedemption(DateTime txnDate, BigDecimal txnAmt, String storeCode, String notes);
        
	void forB2CRedemptionVoiding(DateTime txnDate, BigDecimal txnAmt, String storeCode, String notes);
        
        void createAffiliatesAcctEntry();

		void forFreeVoucherActivation(SalesOrderDto orderDto);

		void forAdvB2BActivation(SalesOrderDto orderDto);

		void forB2BExhibition(B2BExhibitionDto exh);

		void forB2BReplacementActivation(SalesOrderDto orderDto);

		void forReplacementPayment(DateTime txnDate, BigDecimal txnAmt,
				String notes);

		void forB2BReturn(SalesOrderDto order, ReturnRecordDto returnByCards);
}
