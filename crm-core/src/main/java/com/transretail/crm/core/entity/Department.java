package com.transretail.crm.core.entity;

import com.transretail.crm.core.entity.lookup.Product;
import java.util.Date;
import java.util.Set;
import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import org.hibernate.annotations.Immutable;
import org.springframework.format.annotation.DateTimeFormat;

/**
 * POS Table
 * @author Monte Cillo Co (mco@exist.com)
 */
@Entity
@Table(name = "DEPARTMENT")
@Immutable
public class Department {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "dept_id", length = 16) //default to 16 as most are using this size -- override in implementing class as needed.
    private String id;

    public String getId() {
        return id;
    }
   

    public void setId(String id) {
        this.id = id;
    }

    // @NotNull
    @Temporal(TemporalType.TIMESTAMP)
    @DateTimeFormat(style = "M-")
    @Column(name = "UPDATE_DATE_AUD")
    private Date updateDateAud;

    @NotNull
    @Temporal(TemporalType.TIMESTAMP)
    @DateTimeFormat(style = "M-")
    @Column(name = "CREATE_DATE_AUD")
    private Date createDateAud;

    @NotNull
    @Size(max = 40)
    private String description;

    @NotNull
    @Size(max = 10)
    private String code;

    @ManyToOne(targetEntity = Division.class)
    @JoinColumn(name = "division_id")
    private Division division;
    
    @OneToMany(mappedBy = "department")
    private Set<Product> products;

    public Date getUpdateDateAud() {
        return updateDateAud;
    }

    public void setUpdateDateAud(Date updateDateAud) {
        this.updateDateAud = updateDateAud;
    }

    public Date getCreateDateAud() {
        return createDateAud;
    }

    public void setCreateDateAud(Date createDateAud) {
        this.createDateAud = createDateAud;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public Division getDivision() {
        return division;
    }

    public void setDivision(Division division) {
        this.division = division;
    }

    public Set<Product> getProducts() {
        return products;
    }

    public void setProducts(Set<Product> products) {
        this.products = products;
    }

}