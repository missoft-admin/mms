    create table CRM_STAMP (
        id number(19,0) not null,
        CREATED_BY varchar2(255 char),
        CREATED_DATETIME timestamp,
        LAST_UPDATED_BY varchar2(255 char),
        LAST_UPDATED_DATETIME timestamp,
        EXPIRY_DATE date,
        INCLUSIVE_DATE_FROM timestamp,
        INCLUSIVE_DATE_TO timestamp,
        LAST_REDEEM timestamp,
        TXN_TYPE varchar2(255 char),
        VALID_PERIOD number(10,0),
        VALIDATED_BY varchar2(255 char),
        VALIDATED_DATE timestamp,
        PROMOTION_ID number(19,0),
        TXN_HDR_ID number(19,0),
        primary key (id)
    );

    create table CRM_STAMP_AUD (
        id number(19,0) not null,
        REV number(19,0) not null,
        REVTYPE number(3,0),
        EXPIRY_DATE date,
        INCLUSIVE_DATE_FROM timestamp,
        INCLUSIVE_DATE_TO timestamp,
        LAST_REDEEM timestamp,
        TXN_TYPE varchar2(255 char),
        VALID_PERIOD number(10,0),
        VALIDATED_BY varchar2(255 char),
        VALIDATED_DATE timestamp,
        PROMOTION_ID number(19,0),
        primary key (id, REV)
    );

    create table CRM_STAMP_TXN_DTL (
        id number(19,0) not null,
        CREATED_BY varchar2(255 char),
        CREATED_DATETIME timestamp,
        LAST_UPDATED_BY varchar2(255 char),
        LAST_UPDATED_DATETIME timestamp,
        STAMPS_PER_MEDIA double precision,
        STATUS varchar2(255 char),
        TXN_AMOUNT double precision,
        TXN_HDR_ID number(19,0) not null,
        REF_MEDIA_ID varchar2(15 char),
        primary key (id)
    );

    create table CRM_STAMP_TXN_HDR (
        id number(19,0) not null,
        CREATED_BY varchar2(255 char),
        CREATED_DATETIME timestamp,
        LAST_UPDATED_BY varchar2(255 char),
        LAST_UPDATED_DATETIME timestamp,
        REASON varchar2(1000 char),
        RETURN_TXN_NO varchar2(255 char),
        STORE_CODE varchar2(255 char),
        TOTAL_STAMP double precision,
        TXN_DATE timestamp,
        TXN_NO varchar2(255 char),
        MEMBER_ID number(19,0),
        primary key (id)
    );

    alter table CRM_STAMP
        add constraint FK_51wohkkw0847x3ykduhui2p8p
        foreign key (PROMOTION_ID)
        references CRM_PROMOTION;

    alter table CRM_STAMP
        add constraint FK_ape8wpe8npkqblxjfrimqfp9u
        foreign key (TXN_HDR_ID)
        references CRM_STAMP_TXN_HDR;

    alter table CRM_STAMP_AUD
        add constraint FK_2tv2kq32k8r6lsr6ausun5ksh
        foreign key (REV)
        references CRM_REVINFO;

    alter table CRM_STAMP_TXN_DTL
        add constraint FK_cq5n5l0xpr2luep2n1kdndr6w
        foreign key (TXN_HDR_ID)
        references CRM_STAMP_TXN_HDR;

    alter table CRM_STAMP_TXN_DTL
        add constraint FK_haivduo0t75twsn4jyr568w1b
        foreign key (REF_MEDIA_ID)
        references CRM_REF_LOOKUP_DTL;

    alter table CRM_STAMP_TXN_HDR
        add constraint FK_lkfhh3nk2vqbsjx1m8hotexpp
        foreign key (MEMBER_ID)
        references CRM_MEMBER;
