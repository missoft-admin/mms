package com.transretail.crm.report.template.impl;

import java.awt.AlphaComposite;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.joda.time.DateTime;
import org.joda.time.LocalDate;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.MessageSourceAware;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.context.support.MessageSourceAccessor;
import org.springframework.format.datetime.DateFormatter;
import org.springframework.stereotype.Service;

import com.google.common.collect.Lists;
import com.mysema.query.BooleanBuilder;
import com.mysema.query.jpa.JPQLQuery;
import com.mysema.query.jpa.impl.JPAQuery;
import com.mysema.query.types.Projections;
import com.mysema.query.types.expr.BooleanExpression;
import com.transretail.crm.common.reporting.jasper.ClasspathFileResolver;
import com.transretail.crm.common.reporting.jasper.DefaultJRProcessor;
import com.transretail.crm.common.reporting.jasper.JRProcessor;
import com.transretail.crm.common.util.DateUtil;
import com.transretail.crm.core.entity.QPeopleSoftStore;
import com.transretail.crm.core.entity.enums.ReportType;
import com.transretail.crm.core.entity.lookup.LookupDetail;
import com.transretail.crm.core.entity.lookup.Store;
import com.transretail.crm.core.service.CodePropertiesService;
import com.transretail.crm.core.service.LookupService;
import com.transretail.crm.core.service.StoreService;
import com.transretail.crm.giftcard.dto.ProductProfileDto;
import com.transretail.crm.giftcard.entity.GiftCardInventoryStock.GCIStockStatus;
import com.transretail.crm.giftcard.entity.QGiftCardInventory;
import com.transretail.crm.giftcard.entity.QGiftCardInventoryStock;
import com.transretail.crm.giftcard.entity.support.GiftCardSalesType;
import com.transretail.crm.giftcard.repo.GiftCardInventoryStockRepo;
import com.transretail.crm.giftcard.service.ProductProfileService;
import com.transretail.crm.report.template.FilterField;
import com.transretail.crm.report.template.ReportTemplate;

/**
 * @author ftopico
 */

@Service("giftCardConsumedReport")
public class GiftCardConsumedReport implements ReportTemplate, MessageSourceAware {
    
    @PersistenceContext
    private EntityManager em;
    @Autowired
    private GiftCardInventoryStockRepo repo;
    @Autowired
    private ProductProfileService productProfileService;
    @Autowired
    private StoreService storeService;
    @Autowired
    private LookupService lookupService;
    @Autowired
    private CodePropertiesService codePropertiesService;
    
    private static final String PERMISSION_CODE = "GC_CONSUMED_REPORT";
    protected static final String TEMPLATE_NAME = "Consumed Gift Card Report";
    protected static final String ALL_STORES = "All Stores";
    protected static final String ALL_PRODUCTS = "All Products";
    protected static final String REPORT_NAME = "reports/gift_card_consumed_list_document.jasper";
    protected static final String REPORT_FILTER_PRODUCT = "PRODUCT";
    protected static final String REPORT_FILTER_SALES_TYPE = "SALESTYPE";
    protected static final String REPORT_FILTER_STORE = "STORE";
    protected static final String REPORT_FILTER_TRANSACTION_DATE_FROM = "DATEFROM";
    protected static final String REPORT_FILTER_TRANSACTION_DATE_TO = "DATETO";
    protected static final String REPORT_FILTER_AREA_REGION = "AREAREGION";
    protected static final String REPORT_FILTER_ORDER_STORE = "ORDERSTORE";
    //Header Params
    private static final String PARAM_PRINT_DATE = "printDate";
    private static final DateTimeFormatter FULL_DATE_PATTERN = DateTimeFormat.forPattern("dd-MM-yyyy");
    private static final DateTimeFormatter YEAR_PATTERN = DateTimeFormat.forPattern("yyyy");
    private MessageSourceAccessor messageSource;
    
    public GiftCardConsumedReport() {
    }
    
    public ReportType[] getReportTypes() {
        return ReportType.values();
    }
    
    public String getName() {
        return TEMPLATE_NAME;
    }
    
    public String getDescription() {
        return messageSource.getMessage("title_document_gift_card_consumed_list");
    }
    
    @Override
    public boolean canView(Set<String> permissions) {
        return permissions.contains(PERMISSION_CODE);
    }
    
    @Override
    public void setMessageSource(MessageSource messageSource) {
        this.messageSource = new MessageSourceAccessor(messageSource);
    }
    public Set<FilterField> getFilters() {
        
        Set<FilterField> filters = new LinkedHashSet<FilterField>();
        
        //BookDate/TransactionDate
        filters.add(FilterField.createDateField(REPORT_FILTER_TRANSACTION_DATE_FROM,
                messageSource.getMessage("gc_expirebalance_report_label_datefrom"),
                DateUtil.SEARCH_DATE_FORMAT, FilterField.DateTypeOption.PLAIN, true));
        filters.add(FilterField.createDateField(REPORT_FILTER_TRANSACTION_DATE_TO,
                messageSource.getMessage("gc_expirebalance_report_label_dateto"),
                DateUtil.SEARCH_DATE_FORMAT, FilterField.DateTypeOption.PLAIN, true));
        
        //Products
        Map<String, String> products = new LinkedHashMap<String, String>();
        List<ProductProfileDto> productProfiles = productProfileService.findAllDtos();
        products.put("", "");
        if (!productProfiles.isEmpty()) {
            for (ProductProfileDto dto : productProfiles) {
                products.put(dto.getProductCode(), dto.getProductDesc());
            }
        }
        filters.add(FilterField.createDropdownField(REPORT_FILTER_PRODUCT, messageSource.getMessage("gc_transaction_lbl_product"), products));
        
        //Stores
        Map<String, String> stores = new LinkedHashMap<String, String>();
        stores.put("", "");
        stores.put(codePropertiesService.getDetailInvLocationHeadOffice(), lookupService.getDetailByCode(codePropertiesService.getDetailInvLocationHeadOffice()).getDescription());
        for (Store store : storeService.getAllStores()) {
            stores.put(store.getCode(), store.getCode() + " - " + store.getName());
        }
        filters.add(FilterField.createDropdownField(REPORT_FILTER_STORE, messageSource.getMessage("gc.burncard.label.store.id"), stores));
        
        //Sales Type
        Map<String, String> salesType = new LinkedHashMap<String, String>();
        salesType.put("", "");
        for (GiftCardSalesType soType : GiftCardSalesType.values()) {
            salesType.put(soType.toString(), soType.toString());
        }
        filters.add(FilterField.createDropdownField(REPORT_FILTER_SALES_TYPE, messageSource.getMessage("gc_transaction_lbl_sales_type"), salesType));
        
        //Area/Region
        Map<String, String> region = new LinkedHashMap<String, String>();
        region.put("", "");
        List<LookupDetail> regionList = lookupService.getActiveDetailsByHeaderCode(codePropertiesService.getHeaderBusinessRegion());
        Collections.sort(regionList, ALPHA_ORDER);
        for (LookupDetail regionDetail : regionList) {
            region.put(regionDetail.getCode(), regionDetail.getDescription());
        }
        filters.add(FilterField.createDropdownField(REPORT_FILTER_AREA_REGION, messageSource.getMessage("gc_transaction_lbl_area"), region));
        
        return filters;
    }
    
    private static Comparator<LookupDetail> ALPHA_ORDER = new Comparator<LookupDetail>() {
        public int compare(LookupDetail ld1, LookupDetail ld2) {
            int x = String.CASE_INSENSITIVE_ORDER.compare(ld1.getDescription(), ld2.getDescription());
            if (x== 0) {
                x= ld1.getDescription().compareTo(ld2.getDescription());
            }
            return x;
        }
    };

    public static class ReportBean {
        
        private String activationYear;
        private Double january;
        private Double february;
        private Double march;
        private Double april;
        private Double may;
        private Double june;
        private Double july;
        private Double august;
        private Double september;
        private Double october;
        private Double november;
        private Double december;
        
        public ReportBean() {}

        public ReportBean(String activationYear, Double january,
                Double february, Double march, Double april, Double may,
                Double june, Double july, Double august, Double september,
                Double october, Double november, Double december) {
            super();
            this.activationYear = activationYear;
            this.january = january;
            this.february = february;
            this.march = march;
            this.april = april;
            this.may = may;
            this.june = june;
            this.july = july;
            this.august = august;
            this.september = september;
            this.october = october;
            this.november = november;
            this.december = december;
        }

        public String getActivationYear() {
            return activationYear;
        }

        public Double getJanuary() {
            return january;
        }

        public Double getFebruary() {
            return february;
        }

        public Double getMarch() {
            return march;
        }

        public Double getApril() {
            return april;
        }

        public Double getMay() {
            return may;
        }

        public Double getJune() {
            return june;
        }

        public Double getJuly() {
            return july;
        }

        public Double getAugust() {
            return august;
        }

        public Double getSeptember() {
            return september;
        }

        public Double getOctober() {
            return october;
        }

        public Double getNovember() {
            return november;
        }

        public Double getDecember() {
            return december;
        }

        public void setActivationYear(String activationYear) {
            this.activationYear = activationYear;
        }

        public void setJanuary(Double january) {
            this.january = january;
        }

        public void setFebruary(Double february) {
            this.february = february;
        }

        public void setMarch(Double march) {
            this.march = march;
        }

        public void setApril(Double april) {
            this.april = april;
        }

        public void setMay(Double may) {
            this.may = may;
        }

        public void setJune(Double june) {
            this.june = june;
        }

        public void setJuly(Double july) {
            this.july = july;
        }

        public void setAugust(Double august) {
            this.august = august;
        }

        public void setSeptember(Double september) {
            this.september = september;
        }

        public void setOctober(Double october) {
            this.october = october;
        }

        public void setNovember(Double november) {
            this.november = november;
        }

        public void setDecember(Double december) {
            this.december = december;
        }
        
    }

    @Override
    public JRProcessor createJrProcessor(Map<String, String> map) {
        
        Map<String, Object> parameters = new HashMap<String, Object>();
        parameters.put(PARAM_PRINT_DATE, FULL_DATE_PATTERN.print(new LocalDate()));
        parameters.put(REPORT_FILTER_TRANSACTION_DATE_FROM, map.get(REPORT_FILTER_TRANSACTION_DATE_FROM));
        parameters.put(REPORT_FILTER_TRANSACTION_DATE_TO, map.get(REPORT_FILTER_TRANSACTION_DATE_TO));
        
        //Product
        if (!map.get(REPORT_FILTER_PRODUCT).equalsIgnoreCase("")) {
            String product = null;
            ProductProfileDto productDto = productProfileService.findProductProfileByCode(map.get(REPORT_FILTER_PRODUCT));
            if (productDto != null)
                product = productDto.getProductCode() + " - " + productDto.getProductDesc();
            parameters.put(REPORT_FILTER_PRODUCT, product);
        } else {
            parameters.put(REPORT_FILTER_PRODUCT, ALL_PRODUCTS);
        }
        
        //Store
        if (!map.get(REPORT_FILTER_STORE).equalsIgnoreCase("")) {
            String store = null;
            if (map.get(REPORT_FILTER_STORE).equalsIgnoreCase(codePropertiesService.getDetailInvLocationHeadOffice())) {
                LookupDetail detail = lookupService.getDetailByCode(codePropertiesService.getDetailInvLocationHeadOffice());
                store = detail.getCode() + " - " + detail.getDescription();
            } else {
                Store modelStore = storeService.getStoreByCode(map.get(REPORT_FILTER_STORE));
                if (modelStore != null)
                    store = modelStore.getCodeAndName();
            }
            parameters.put(REPORT_FILTER_STORE, store);
        } else {
            parameters.put(REPORT_FILTER_STORE, ALL_STORES);
        }
            
        
        //Area/Region
        if (!map.get(REPORT_FILTER_AREA_REGION).equalsIgnoreCase("")) {
            String region = lookupService.getActiveDetailByCode(map.get(REPORT_FILTER_AREA_REGION)).getCode()
                     + " - "
                     + lookupService.getActiveDetailByCode(map.get(REPORT_FILTER_AREA_REGION)).getDescription();
            parameters.put(REPORT_FILTER_AREA_REGION, region);
        }
            
        parameters.put(REPORT_FILTER_SALES_TYPE, map.get(REPORT_FILTER_SALES_TYPE));
        
        List<ReportBean> beansDataSource = Lists.newArrayList();
        
        Map<Integer, Map<Integer, Double>> gcConsumed = getGiftCardConsumed(map);
        
        for (int ctr = Integer.parseInt(map.get(REPORT_FILTER_TRANSACTION_DATE_FROM).substring(map.get(REPORT_FILTER_TRANSACTION_DATE_FROM).length() - 4)); 
                ctr <= Integer.parseInt(map.get(REPORT_FILTER_TRANSACTION_DATE_TO).substring(map.get(REPORT_FILTER_TRANSACTION_DATE_TO).length() - 4)); 
                ctr++) {
            beansDataSource.add(new ReportBean(String.valueOf(ctr), 
                    gcConsumed.get(ctr).get(1),
                    gcConsumed.get(ctr).get(2),
                    gcConsumed.get(ctr).get(3),
                    gcConsumed.get(ctr).get(4),
                    gcConsumed.get(ctr).get(5),
                    gcConsumed.get(ctr).get(6),
                    gcConsumed.get(ctr).get(7),
                    gcConsumed.get(ctr).get(8),
                    gcConsumed.get(ctr).get(9),
                    gcConsumed.get(ctr).get(10),
                    gcConsumed.get(ctr).get(11),
                    gcConsumed.get(ctr).get(12)));
        }
        
        DefaultJRProcessor jrProcessor = new DefaultJRProcessor(REPORT_NAME, beansDataSource);
        jrProcessor.addParameters(parameters);
        jrProcessor.setFileResolver(new ClasspathFileResolver("reports"));
        
        return jrProcessor;
    }
    
    private Map<Integer, Map<Integer, Double>> getGiftCardConsumed(Map<String, String> map) {
        
        Map<Integer, Map<Integer, Double>> gcConsumedByYear =  new LinkedHashMap<Integer, Map<Integer,Double>>();
        QGiftCardInventoryStock qg = QGiftCardInventoryStock.giftCardInventoryStock;
        QGiftCardInventory qgi = QGiftCardInventory.giftCardInventory;
        QPeopleSoftStore qps = QPeopleSoftStore.peopleSoftStore;
        
        DateTime dateFrom = new DateTime(FULL_DATE_PATTERN.parseMillis(map.get(REPORT_FILTER_TRANSACTION_DATE_FROM)));
        DateTime dateTo = new DateTime(FULL_DATE_PATTERN.parseMillis(map.get(REPORT_FILTER_TRANSACTION_DATE_TO)));
        
        for (int year = dateFrom.getYear(); year<=dateTo.getYear(); year++) {
            Map<Integer, Double> gcConsumedMap = new LinkedHashMap<Integer, Double>();
            initializeMap(gcConsumedMap);
            
            int initialStartMonth = 1;
            if (year == dateFrom.getYear())
                initialStartMonth = dateFrom.getMonthOfYear();
            
            int initialEndMonth = 12;
            if (year == dateTo.getYear())
                initialEndMonth = dateTo.getMonthOfYear();
            
            for (int month = initialStartMonth; month <= initialEndMonth; month++) {
                BooleanBuilder expressionsActivated = new BooleanBuilder();
                BooleanBuilder expressionsReturned = new BooleanBuilder();
                
                if (!map.get(REPORT_FILTER_PRODUCT).equalsIgnoreCase("")) {
                    expressionsActivated.and(qg.cardType.eq(map.get(REPORT_FILTER_PRODUCT)));
                    expressionsReturned.and(qg.cardType.eq(map.get(REPORT_FILTER_PRODUCT)));
                }
                if (!map.get(REPORT_FILTER_STORE).equalsIgnoreCase("")) {
                    expressionsActivated.and(qg.location.eq(map.get(REPORT_FILTER_STORE)));
                    expressionsReturned.and(qg.location.eq(map.get(REPORT_FILTER_STORE)));
                }
                if (!map.get(REPORT_FILTER_SALES_TYPE).equalsIgnoreCase("")) {
                    expressionsActivated.and(qgi.salesType.eq(GiftCardSalesType.valueOf(map.get(REPORT_FILTER_SALES_TYPE))));
                    expressionsReturned.and(qgi.salesType.eq(GiftCardSalesType.valueOf(map.get(REPORT_FILTER_SALES_TYPE))));
                }
                
                int initialStartDay = 1;
                if (year == dateFrom.getYear() && month == dateFrom.getMonthOfYear())
                    initialStartDay = dateFrom.getDayOfMonth();
                
                DateTime startDate = new DateTime(year, month, initialStartDay, 0, 0, 0);
                DateTime endDate = startDate.dayOfMonth().withMaximumValue();
                if (year == dateTo.getYear() && month == dateTo.getMonthOfYear())
                    endDate = new DateTime(year, month, dateTo.getDayOfMonth(), 23, 59, 59);
                
                JPQLQuery query = new JPAQuery(em);
                List<Double> activated = new ArrayList<Double>();
                List<Double> inStock = new ArrayList<Double>();
                
                if (!map.get(REPORT_FILTER_AREA_REGION).equalsIgnoreCase("")) {
                    expressionsActivated.and(qps.businessRegion.code.eq(map.get(REPORT_FILTER_AREA_REGION)));
                    expressionsReturned.and(qps.businessRegion.code.eq(map.get(REPORT_FILTER_AREA_REGION)));
                    
                    //ACTIVATED
                    expressionsActivated.and(qg.created.between(startDate, endDate));
                    expressionsActivated.and(qg.status.eq(GCIStockStatus.ACTIVATED.name()));
                    expressionsActivated.and(qg.seriesStart.eq(qgi.series));
                    expressionsActivated.and(qg.location.eq(qps.store.code));
                    query = query.from(qg, qgi, qps)
                            .where(expressionsActivated.getValue())
                            .groupBy(qg.status);
                    activated = query.list(qg.quantity.sum().doubleValue());
                    
                    //RETURNED
                    expressionsReturned.and(qg.created.between(startDate, endDate));
                    expressionsReturned.and(qg.status.eq(GCIStockStatus.RETURNED.name()));
                    expressionsReturned.and(qg.seriesStart.eq(qgi.series));
                    expressionsReturned.and(qg.location.eq(qps.store.code));
                    query = new JPAQuery(em);
                    query = query.from(qg, qgi, qps)
                            .where(expressionsReturned.getValue())
                            .groupBy(qg.status);
                    inStock = query.list(qg.quantity.sum().doubleValue());
                } else {
                    //ACTIVATED
                    expressionsActivated.and(qg.created.between(startDate, endDate));
                    expressionsActivated.and(qg.status.eq(GCIStockStatus.ACTIVATED.name()));
                    expressionsActivated.and(qg.seriesStart.eq(qgi.series));
                    query = query.from(qg, qgi)
                            .where(expressionsActivated.getValue())
                            .groupBy(qg.status);
                    activated = query.list(qg.quantity.sum().doubleValue());
                    
                    //RETURNED
                    expressionsReturned.and(qg.created.between(startDate, endDate));
                    expressionsReturned.and(qg.status.eq(GCIStockStatus.RETURNED.name()));
                    expressionsReturned.and(qg.seriesStart.eq(qgi.series));
                    query = new JPAQuery(em);
                    query = query.from(qg, qgi)
                            .where(expressionsReturned.getValue())
                            .groupBy(qg.status);
                    inStock = query.list(qg.quantity.sum().doubleValue());
                }
                
                Double active = 0.0;
                if (activated.size() > 0)
                    active = activated.get(0);
                
                Double instock = 0.0;
                if (inStock.size() > 0)
                    instock = inStock.get(0); 
                
                gcConsumedMap.put(month, active - instock);
            }
            gcConsumedByYear.put(year, gcConsumedMap);
        }
        return gcConsumedByYear;
    }

    private void initializeMap(Map<Integer, Double> gcConsumedMap) {
        for (int ctr = 1; ctr <= 12; ctr++)
            gcConsumedMap.put(ctr,0.0);
    }
}
