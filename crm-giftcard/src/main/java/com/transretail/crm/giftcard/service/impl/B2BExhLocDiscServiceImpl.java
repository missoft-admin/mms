package com.transretail.crm.giftcard.service.impl;

import java.math.BigDecimal;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.joda.time.LocalDate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.mysema.query.BooleanBuilder;
import com.mysema.query.jpa.impl.JPAQuery;
import com.mysema.query.types.Projections;
import com.mysema.query.types.expr.BooleanExpression;
import com.transretail.crm.common.service.dto.response.NameIdPairDto;
import com.transretail.crm.common.service.dto.response.ResultList;
import com.transretail.crm.common.service.util.SpringDataPagingUtil;
import com.transretail.crm.core.security.util.UserUtil;
import com.transretail.crm.giftcard.dto.B2BExhDiscDto;
import com.transretail.crm.giftcard.dto.B2BExhLocDto;
import com.transretail.crm.giftcard.dto.B2bExhLocSearchDto;
import com.transretail.crm.giftcard.entity.B2BExhibitionDiscount;
import com.transretail.crm.giftcard.entity.B2BExhibitionLocation;
import com.transretail.crm.giftcard.entity.QB2BExhibitionDiscount;
import com.transretail.crm.giftcard.entity.QB2BExhibitionLocation;
import com.transretail.crm.giftcard.repo.B2BExhibitionDiscountRepo;
import com.transretail.crm.giftcard.repo.B2BExhibitionLocationRepo;
import com.transretail.crm.giftcard.service.B2bExhLocDiscService;
import com.transretail.crm.giftcard.service.DiscountSchemeService.DiscountDto;

@Service
@Transactional
public class B2BExhLocDiscServiceImpl implements B2bExhLocDiscService {
	@Autowired
	private B2BExhibitionLocationRepo locRepo;
	@Autowired
	private B2BExhibitionDiscountRepo discRepo;
	@PersistenceContext
	private EntityManager em;
	@Override
	public void saveLocation(B2BExhLocDto loc) {
		B2BExhibitionLocation location = new B2BExhibitionLocation();
		if(loc.getId() != null)
			location = locRepo.findOne(loc.getId());
		locRepo.save(loc.toModel(location));
	}
	@Override
	public void saveDiscount(B2BExhDiscDto disc) {
		B2BExhibitionDiscount discount = new B2BExhibitionDiscount();
		if(disc.getId() != null) {
			discount = discRepo.findOne(disc.getId());
		}
		discRepo.save(disc.toModel(discount));
	}
	@Override
	public ResultList<B2BExhLocDto> getLocations(B2bExhLocSearchDto searchForm) {
		Pageable pageable = SpringDataPagingUtil.INSTANCE.createPageable(searchForm.getPagination());
        BooleanExpression filter = searchForm.createSearchExpression();
        Page<B2BExhibitionLocation> page = filter != null ? locRepo.findAll(filter, pageable) : locRepo.findAll(pageable);
        return new ResultList<B2BExhLocDto>(B2BExhLocDto.toDto(page.getContent()), page.getTotalElements(), page.hasPreviousPage(),
            page.hasNextPage());
	}
	@Override
	public List<NameIdPairDto> getLocations(String nameLike) {
		QB2BExhibitionLocation qLoc = QB2BExhibitionLocation.b2BExhibitionLocation;
		return new JPAQuery(em).from(qLoc).where(qLoc.name.startsWithIgnoreCase(nameLike)).distinct().list(Projections.constructor(NameIdPairDto.class, 
				qLoc.name.toUpperCase(),
				qLoc.name.toUpperCase()));
	}
	
	@Override
	public ResultList<B2BExhDiscDto> getDiscounts(B2bExhLocSearchDto searchForm) {
		Pageable pageable = SpringDataPagingUtil.INSTANCE.createPageable(searchForm.getPagination());
		BooleanExpression filter = searchForm.createSearchExpression();
		Page<B2BExhibitionDiscount> page = filter != null ? discRepo.findAll(filter, pageable) : discRepo.findAll(pageable);
        return new ResultList<B2BExhDiscDto>(B2BExhDiscDto.toDto(page.getContent()), page.getTotalElements(), page.hasPreviousPage(),
            page.hasNextPage());
	}
	
	@Override
	public B2BExhLocDto getLocation(Long id) {
		return new B2BExhLocDto(locRepo.findOne(id));
	}
	
	@Override
	public B2BExhDiscDto getDiscount(Long id) {
		return new B2BExhDiscDto(discRepo.findOne(id));
	}
	@Override
	@Transactional
	public void deleteLocation(Long id) {
		locRepo.delete(id);
	}
	
	@Override
	@Transactional
	public void deleteDiscount(Long id) {
		discRepo.delete(id);
	}
	
	@Override
	public boolean isExistOverlappingLocation(B2BExhLocDto location) {
		QB2BExhibitionLocation qLoc = QB2BExhibitionLocation.b2BExhibitionLocation;
		BooleanBuilder f = new BooleanBuilder();
		f.and(qLoc.startDate.loe(location.getEndDate()));
		f.and(qLoc.endDate.goe(location.getStartDate()));
		f.and(qLoc.name.equalsIgnoreCase(location.getName()));
		if(location.getId() != null)
			f.and(qLoc.id.ne(location.getId()));
		return new JPAQuery(em).from(qLoc).where(f).exists();
	}
	@Override
	public boolean isExistOverlappingDisc(B2BExhDiscDto disc) {
		QB2BExhibitionDiscount qDis = QB2BExhibitionDiscount.b2BExhibitionDiscount;
		BooleanBuilder f = new BooleanBuilder();
		f.and(qDis.minAmount.loe(disc.getMaxAmount()));
		f.and(qDis.maxAmount.goe(disc.getMinAmount()));
		f.and(qDis.location.id.eq(disc.getLocId()));
		if(disc.getId() != null)
			f.and(qDis.id.ne(disc.getId()));
		return new JPAQuery(em).from(qDis).where(f).exists();
	}
	
	@Override
	public BigDecimal getDiscount(BigDecimal purchase) {
		LocalDate now = new LocalDate();
		QB2BExhibitionDiscount qDiscount = QB2BExhibitionDiscount.b2BExhibitionDiscount;
		BooleanBuilder filter = new BooleanBuilder().andAnyOf(
			qDiscount.minAmount.isNull().and(qDiscount.maxAmount.goe(purchase)),
			qDiscount.minAmount.loe(purchase).and(qDiscount.maxAmount.goe(purchase)),
			qDiscount.minAmount.loe(purchase).and(qDiscount.maxAmount.isNull())
		);
		
		filter.and(qDiscount.location.startDate.loe(now).and(qDiscount.location.endDate.goe(now)));
		filter.and(qDiscount.location.name.equalsIgnoreCase(UserUtil.getCurrentUser().getB2bLocation()));
		
		B2BExhibitionDiscount disc = discRepo.findOne(filter);
		return disc == null ? BigDecimal.ZERO : disc.getDiscount();
	}
}


