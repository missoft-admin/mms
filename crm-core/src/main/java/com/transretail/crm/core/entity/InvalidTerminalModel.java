package com.transretail.crm.core.entity;

import java.io.Serializable;
import javax.persistence.*;

/**
 *
 * @author Monte Cillo Co (mco@exist.com)
 */
@Entity
@Table(name = "CRM_INVALID_TERMINAL")
public class InvalidTerminalModel implements Serializable {

    @Id
    @Column(name = "TERMINAL_ID")
    private String terminalId;
    @Column(name = "STORE_CODE")
    private String storeCode;

    public String getTerminalId() {
	return terminalId;
    }

    public void setTerminalId(String terminalId) {
	this.terminalId = terminalId;
    }

    public String getStoreCode() {
	return storeCode;
    }

    public void setStoreCode(String storeCode) {
	this.storeCode = storeCode;
    }
}
