package com.transretail.crm.giftcard.entity;

import javax.persistence.*;
import javax.validation.constraints.Size;
import org.hibernate.annotations.Immutable;
import org.hibernate.annotations.Type;
import org.joda.time.LocalDateTime;
import org.springframework.format.annotation.DateTimeFormat;

@Entity
@Table(name = "GIFT_CARD_TRANSACTION")
@Immutable
public class PosGcTransaction {

    @Id
    @Column(name = "ID")
    private String id;
    @Temporal(TemporalType.TIMESTAMP)
    @DateTimeFormat(style = "M-")
    @Column(name = "TRANSACTION_DATE")
    @Type(type = "org.jadira.usertype.dateandtime.joda.PersistentLocalDateTime")
    private LocalDateTime transactionDate;
    @Column(name = "AMOUNT")
    private Double amount;
    @Column(name = "BALANCE")
    private Double balance;
    @Column(name = "PREVIOUS_BALANCE")
    private Double previousBalance;
    @Column(name = "CARD_NUMBER")
    private String cardNumber;
    @Column(name = "REQUEST_TYPE")
    private Character requestType;
    @Enumerated(EnumType.STRING)
    @Column(name = "gcServerType")
    @Size(max = 20)
    private ServerType gcServerType;
    @Column(name = "TX_ID")
    private String txId;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public LocalDateTime getTransactionDate() {
        return transactionDate;
    }

    public void setTransactionDate(LocalDateTime transactionDate) {
        this.transactionDate = transactionDate;
    }

    public Double getAmount() {
        return amount;
    }

    public void setAmount(Double amount) {
        this.amount = amount;
    }

    public Double getBalance() {
        return balance;
    }

    public void setBalance(Double balance) {
        this.balance = balance;
    }

    public Double getPreviousBalance() {
        return previousBalance;
    }

    public void setPreviousBalance(Double previousBalance) {
        this.previousBalance = previousBalance;
    }

    public String getCardNumber() {
        return cardNumber;
    }

    public void setCardNumber(String cardNumber) {
        this.cardNumber = cardNumber;
    }

    public Character getRequestType() {
        return requestType;
    }

    public void setRequestType(Character requestType) {
        this.requestType = requestType;
    }

    public String getTxId() {
        return txId;
    }

    public void setTxId(String txId) {
        this.txId = txId;
    }

    public ServerType getGcServerType() {
        return gcServerType;
    }

    public void setGcServerType(ServerType gcServerType) {
        this.gcServerType = gcServerType;
    }

    public enum ServerType {

        MMS, OGLOBA
    }
}
