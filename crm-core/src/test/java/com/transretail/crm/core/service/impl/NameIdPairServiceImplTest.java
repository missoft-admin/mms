package com.transretail.crm.core.service.impl;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.mysema.query.types.expr.BooleanExpression;
import com.transretail.crm.common.service.dto.request.PagingParam;
import com.transretail.crm.common.service.dto.request.SearchFormDto;
import com.transretail.crm.common.service.dto.response.NameIdPairDto;
import com.transretail.crm.common.service.dto.response.NameIdPairResultList;
import com.transretail.crm.core.entity.lookup.QStore;
import com.transretail.crm.core.entity.lookup.Store;
import com.transretail.crm.core.repo.StoreRepo;
import com.transretail.crm.core.service.NameIdPairService;

/**
 *
 */
@ContextConfiguration(locations = "classpath*:/META-INF/spring/applicationContext*.xml")
@RunWith(SpringJUnit4ClassRunner.class)
public class NameIdPairServiceImplTest {
    @Autowired
    private StoreRepo storeRepo;
    @Autowired
    private NameIdPairService nameIdPairService;

    private String expectedCode = "code";
    private String expectedName = "name";
    private Integer savedStoreCode;

    @Before
    public void onSetup() {
        Store store = new Store();
        store.setId(new Integer(1));
        store.setCode(expectedCode);
        store.setName(expectedName);
        savedStoreCode = storeRepo.save(store).getId();
    }

    @After
    public void tearDown() {
        if (savedStoreCode != null) {
            storeRepo.delete(savedStoreCode);
        }
    }

    @Test
    public void getNameIdPairsWithNullSearchForm() {
        QStore store = QStore.store;
        NameIdPairResultList resultList = nameIdPairService.getNameIdPairs(null, store, store.code, store.name, null);
        assertEquals(1, resultList.getTotalElements());
        assertEquals(1, resultList.getNumberOfElements());
        NameIdPairDto dto = resultList.getResults().iterator().next();
        assertEquals(expectedCode, dto.getId());
        assertEquals(expectedName, dto.getName());
    }

    @Test
    public void getNameIdPairsWithSearchForm() {
        final QStore store = QStore.store;
        NameIdPairResultList resultList = nameIdPairService.getNameIdPairs(new SearchFormDto() {
            @Override
            public BooleanExpression createSearchExpression() {
                return store.code.eq(expectedCode);
            }

            @Override
            public PagingParam getPagination() {
                return new PagingParam();
            }
        }, store, store.code, store.name, null);
        assertEquals(1, resultList.getTotalElements());
        assertEquals(1, resultList.getNumberOfElements());
        NameIdPairDto dto = resultList.getResults().iterator().next();
        assertEquals(expectedCode, dto.getId());
        assertEquals(expectedName, dto.getName());

        resultList = nameIdPairService.getNameIdPairs(new SearchFormDto() {
            @Override
            public BooleanExpression createSearchExpression() {
                return store.code.eq("somecode");
            }

            @Override
            public PagingParam getPagination() {
                return new PagingParam();
            }
        }, store, store.code, store.name, null);
        assertEquals(0, resultList.getTotalElements());
        assertEquals(0, resultList.getNumberOfElements());
        assertTrue(resultList.getResults().isEmpty());
    }
}
