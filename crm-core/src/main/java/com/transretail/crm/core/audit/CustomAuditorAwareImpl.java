package com.transretail.crm.core.audit;

import com.transretail.crm.core.security.model.impl.CustomSecurityUserDetailsImpl;
import com.transretail.crm.core.security.util.UserUtil;
import org.springframework.data.domain.AuditorAware;
import org.springframework.security.core.context.SecurityContextHolder;

/**
 * @author mhua
 */
public class CustomAuditorAwareImpl implements AuditorAware<String> {

    @Override
    public String getCurrentAuditor() {
        String currentAuditor = "SYSTEM";
        CustomSecurityUserDetailsImpl user = UserUtil.getCurrentUser();
        if (user != null) {
            currentAuditor = user.getUsername();
        }
        return currentAuditor;
    }

}
