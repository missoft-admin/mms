package com.transretail.crm.core.repo.custom;


import java.util.List;

import com.mysema.query.types.Order;
import com.transretail.crm.core.entity.RewardSchemeModel;


public interface RewardSchemeRepoCustom {

    List<RewardSchemeModel> findAllActivePointsSchemes();

	List<RewardSchemeModel> findAllActivePointsSchemesSortValueFrom(Order inSeq);
    
    public Double findLowestActivePointsSchemeValue();
    
    boolean checkRangeOverlap(Long id, Double from, Double to);
    
    boolean checkValueOverlap(Long id, Double value);

	RewardSchemeModel findByAmount(Double inAmount);
}
