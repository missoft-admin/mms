package com.transretail.crm.core.entity.embeddable;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;

import org.hibernate.envers.NotAudited;

import com.transretail.crm.core.entity.lookup.LookupDetail;

// TODO: *ToOne are eager by default. Need to mark this to lazy to improve performance in fetching members
@Embeddable
public class ProfessionalProfile {
	@Column(name="ZONE")
	private String zone;
	@Column(name="RADIUS")
	private String radius;
	@ManyToOne
	@JoinColumn(name="CUSTOMER_GROUP")
	private LookupDetail customerGroup;
	@ManyToOne
	@JoinColumn(name="CUSTOMER_SEGREGATION")
	private LookupDetail customerSegmentation;
    @ManyToOne
    @JoinColumn(name = "DEPARTMENT")
    private LookupDetail department;
    @Column(name="BUSINESS_NAME")
	private String businessName;
	@Column(name="BUSINESS_LICENSE")
	private String businessLicense;
	@JoinColumn(name="BUSINESS_FIELD")
	@ManyToOne
	private LookupDetail businessField;
	@JoinColumn(name="NO_OF_EMP")
	@ManyToOne
	private LookupDetail noOfEmp;
	@Column(name="POTENTIAL_BUYING_PER_MONTH")
	private String potentialBuyingPerMonth;
	@Column(name="BUSINESS_EMAIL")
	private String businessEmail;
	@Column(name="BUSINESS_PHONE")
	private String businessPhone;
	@Column(name="POSITION")
	private String position;
	@Column(name="REG_ID")
	private String registrationId;
//	@Column(name="TIME_TO_CALL")
//	@Temporal(TemporalType.TIMESTAMP)
//	private Date bestTimeToCall;
	@NotAudited
	@JoinColumn(name="BUSINESS_ADDRESS")
	@OneToOne(cascade = CascadeType.ALL)
	private Address businessAddress;
	@Column(name="TRANS_CODE_1")
	private Short transactionCode1;
	@Column(name="TRANS_CODE_2")
	private Short transactionCode2;
	@Column(name="TRANS_CODE_3")
	private Short transactionCode3;
	@Column(name="TRANS_CODE_4")
	private Short transactionCode4;
	@Column(name="TRANS_CODE_SHORT")
	private Short transactionCodeShort;
	@Column(name="TRANS_CODE_LONG1")
	private Short transactionCodeLong1;
	@Column(name="TRANS_CODE_LONG2")
	private Short transactionCodeLong2;
	
	public LookupDetail getCustomerGroup() {
		return customerGroup;
	}
	public void setCustomerGroup(LookupDetail customerGroup) {
		this.customerGroup = customerGroup;
	}
	public LookupDetail getCustomerSegmentation() {
		return customerSegmentation;
	}
	public void setCustomerSegmentation(LookupDetail customerSegmentation) {
		this.customerSegmentation = customerSegmentation;
	}

    public LookupDetail getDepartment() {
        return department;
    }

    public void setDepartment(LookupDetail department) {
        this.department = department;
    }

    public String getBusinessName() {
		return businessName;
	}
	public void setBusinessName(String businessName) {
		this.businessName = businessName;
	}
	public String getBusinessLicense() {
		return businessLicense;
	}
	public void setBusinessLicense(String businessLicense) {
		this.businessLicense = businessLicense;
	}
	public String getZone() {
		return zone;
	}
	public void setZone(String zone) {
		this.zone = zone;
	}
	public String getRadius() {
		return radius;
	}
	public void setRadius(String radius) {
		this.radius = radius;
	}
	public LookupDetail getBusinessField() {
		return businessField;
	}
	public void setBusinessField(LookupDetail businessField) {
		this.businessField = businessField;
	}
	public String getBusinessEmail() {
		return businessEmail;
	}
	public void setBusinessEmail(String businessEmail) {
		this.businessEmail = businessEmail;
	}
	public Address getBusinessAddress() {
		return businessAddress;
	}
	public void setBusinessAddress(Address businessAddress) {
		this.businessAddress = businessAddress;
	}
	public Short getTransactionCode1() {
		return transactionCode1;
	}
	public void setTransactionCode1(Short transactionCode1) {
		this.transactionCode1 = transactionCode1;
	}
	public Short getTransactionCode2() {
		return transactionCode2;
	}
	public void setTransactionCode2(Short transactionCode2) {
		this.transactionCode2 = transactionCode2;
	}
	public Short getTransactionCode3() {
		return transactionCode3;
	}
	public void setTransactionCode3(Short transactionCode3) {
		this.transactionCode3 = transactionCode3;
	}
	public Short getTransactionCode4() {
		return transactionCode4;
	}
	public void setTransactionCode4(Short transactionCode4) {
		this.transactionCode4 = transactionCode4;
	}
	public Short getTransactionCodeShort() {
		return transactionCodeShort;
	}
	public void setTransactionCodeShort(Short transactionCodeShort) {
		this.transactionCodeShort = transactionCodeShort;
	}
	public Short getTransactionCodeLong1() {
		return transactionCodeLong1;
	}
	public void setTransactionCodeLong1(Short transactionCodeLong1) {
		this.transactionCodeLong1 = transactionCodeLong1;
	}
	public Short getTransactionCodeLong2() {
		return transactionCodeLong2;
	}
	public void setTransactionCodeLong2(Short transactionCodeLong2) {
		this.transactionCodeLong2 = transactionCodeLong2;
	}
	public String getPosition() {
		return position;
	}
	public void setPosition(String position) {
		this.position = position;
	}
	public String getRegistrationId() {
		return registrationId;
	}
	public void setRegistrationId(String registrationId) {
		this.registrationId = registrationId;
	}
	public String getBusinessPhone() {
		return businessPhone;
	}
	public void setBusinessPhone(String businessPhone) {
		this.businessPhone = businessPhone;
	}
	public LookupDetail getNoOfEmp() {
		return noOfEmp;
	}
	public void setNoOfEmp(LookupDetail noOfEmp) {
		this.noOfEmp = noOfEmp;
	}
	public String getPotentialBuyingPerMonth() {
		return potentialBuyingPerMonth;
	}
	public void setPotentialBuyingPerMonth(String potentialBuyingPerMonth) {
		this.potentialBuyingPerMonth = potentialBuyingPerMonth;
	}

	
	
}
