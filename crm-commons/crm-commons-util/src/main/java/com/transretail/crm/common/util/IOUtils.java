package com.transretail.crm.common.util;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author Allan G. Ramirez (agramirez@exist.com)
 */
public enum IOUtils {
    INSTANCE;

    private static final Logger _LOG = LoggerFactory.getLogger(IOUtils.class);

    public void close(Statement st) {
        try {
            if (st != null) {
                st.close();
            }
        } catch (SQLException e) {
            _LOG.warn("Failed to close statement.", e);
        }
    }

    public void close(ResultSet rs) {
        try {
            if (rs != null) {
                rs.close();
            }
        } catch (SQLException e) {
            _LOG.warn("Failed to close resultset.", e);
        }
    }

    public void close(Connection con, boolean rollback) {
        try {
            if (con != null) {
                if (rollback) {
                    con.rollback();
                }
                con.close();
            }
        } catch (SQLException e) {
            _LOG.warn("Failed to close connection.", e);
        }
    }
}
