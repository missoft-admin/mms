package com.transretail.crm.core.dto;


import com.mysema.query.types.expr.BooleanExpression;
import com.transretail.crm.common.service.dto.request.AbstractSearchFormDto;
import com.transretail.crm.core.entity.QMemberModel;
import com.transretail.crm.core.entity.enums.MemberStatus;
import com.transretail.crm.core.entity.lookup.LookupDetail;
import com.transretail.crm.core.util.BooleanExprUtil;

import org.apache.commons.lang3.StringUtils;
import org.joda.time.DateTime;

import java.util.ArrayList;
import java.util.List;


public class MemberSearchDto extends AbstractSearchFormDto {
	private String accountId;
	private String contact;
	private String email;
	private String firstName;
	private String lastName;
	private String ktpId;
	private String name;
	
	private LookupDetail memberType;
	private MemberStatus status;
	private String registeredStore;
	private String businessName;
	
	//Member Card Usage Report
	private DateTime createdFrom;
	private DateTime createdTo;
	private Boolean loyaltyCardNoNotNull;
	
	public MemberSearchDto() {}
	
	@Override
	public BooleanExpression createSearchExpression() {
		QMemberModel qMemberModel = QMemberModel.memberModel;
		
		List<BooleanExpression> expressions = new ArrayList<BooleanExpression>();
		if(StringUtils.isNotBlank(accountId))
			expressions.add( BooleanExprUtil.INSTANCE.isStringPropertyLike( qMemberModel.accountId, accountId ) );
		if(StringUtils.isNotBlank(contact))
			expressions.add( BooleanExprUtil.INSTANCE.isStringPropertyLike( qMemberModel.contact, contact ) );
		if(StringUtils.isNotBlank(email))
			expressions.add( BooleanExprUtil.INSTANCE.isStringPropertyLike( qMemberModel.email, email ) );
		if(StringUtils.isNotBlank(firstName))
			expressions.add( BooleanExprUtil.INSTANCE.isStringPropertyLike( qMemberModel.firstName, firstName ) );
		if(StringUtils.isNotBlank(lastName))
			expressions.add( BooleanExprUtil.INSTANCE.isStringPropertyLike( qMemberModel.lastName, lastName ) );
		if(StringUtils.isNotBlank(ktpId))
			expressions.add( BooleanExprUtil.INSTANCE.isStringPropertyLike( qMemberModel.idNumber, ktpId ) );
		if ( StringUtils.isNotBlank( businessName ) ) {
			expressions.add( BooleanExprUtil.INSTANCE.isStringPropertyLike( qMemberModel.companyName, businessName ) );
		}
		
		if(memberType != null) 
			expressions.add(qMemberModel.memberType.eq(memberType));
		
		if(loyaltyCardNoNotNull != null && loyaltyCardNoNotNull)
		    expressions.add(qMemberModel.loyaltyCardNo.isNotNull());
		
		if(status != null && StringUtils.isNotBlank(status.toString()))
			expressions.add(qMemberModel.accountStatus.eq(status));
		if(StringUtils.isNotBlank(name)) {
			expressions.add(BooleanExprUtil.INSTANCE.isStringPropertyLike(qMemberModel.firstName, name)
					.or(BooleanExprUtil.INSTANCE.isStringPropertyLike(qMemberModel.lastName, name)));
		}
		
		if(StringUtils.isNotBlank(registeredStore))
			expressions.add(qMemberModel.registeredStore.eq(registeredStore));
		
		if(createdFrom != null && createdTo != null) {
		    expressions.add(qMemberModel.created.between(createdFrom, createdTo));
		}
		if(createdFrom != null) {
            expressions.add(qMemberModel.created.goe(createdFrom));
        }
		if(createdTo != null) {
            expressions.add(qMemberModel.created.loe(createdTo));
        }
		return BooleanExpression.allOf(expressions.toArray(new BooleanExpression[expressions.size()]));
	}

	public String getAccountId() {
		return accountId;
	}

	public void setAccountId(String accountId) {
		this.accountId = accountId;
	}

	public String getContact() {
		return contact;
	}

	public void setContact(String contact) {
		this.contact = contact;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public LookupDetail getMemberType() {
		return memberType;
	}

	public void setMemberType(LookupDetail memberType) {
		this.memberType = memberType;
	}

	public MemberStatus getStatus() {
		return status;
	}

	public void setStatus(MemberStatus status) {
		this.status = status;
	}

	public String getRegisteredStore() {
		return registeredStore;
	}

	public void setRegisteredStore(String registeredStore) {
		this.registeredStore = registeredStore;
	}

	public String getKtpId() {
		return ktpId;
	}

	public void setKtpId(String ktpId) {
		this.ktpId = ktpId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getBusinessName() {
		return businessName;
	}

	public void setBusinessName(String businessName) {
		this.businessName = businessName;
	}

    public Boolean getLoyaltyCardNoNotNull() {
        return loyaltyCardNoNotNull;
    }

    public void setLoyaltyCardNoNotNull(Boolean loyaltyCardNoNotNull) {
        this.loyaltyCardNoNotNull = loyaltyCardNoNotNull;
    }

    public DateTime getCreatedFrom() {
        return createdFrom;
    }

    public DateTime getCreatedTo() {
        return createdTo;
    }

    public void setCreatedFrom(DateTime createdFrom) {
        this.createdFrom = createdFrom;
    }

    public void setCreatedTo(DateTime createdTo) {
        this.createdTo = createdTo;
    }

}
