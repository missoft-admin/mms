package com.transretail.crm.schedule.service;


import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.quartz.Scheduler;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Configurable;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import com.transretail.crm.core.entity.ApplicationConfig;
import com.transretail.crm.core.entity.enums.AppKey;
import com.transretail.crm.core.repo.ApplicationConfigRepo;
import com.transretail.crm.schedule.trigger.PointsRewardSchedulerTrigger;


@Configurable
@ContextConfiguration(locations = "classpath*:/META-INF/spring/applicationContext*.xml")
@Transactional
@RunWith(SpringJUnit4ClassRunner.class)
public class JobSchedulerServiceTest {

    @Autowired
    JobSchedulerService jobSchedulerService;
    @Autowired
    private ApplicationConfigRepo applicationConfigRepo;

    private final static String CRONEXPRESSION = "0 0 11am 4 * ?";



    @Before
    public void setUp() {
        ApplicationConfig config = new ApplicationConfig();
        config.setKey(AppKey.REWARDS_JOB);
        config.setValue(CRONEXPRESSION);
        applicationConfigRepo.save(config);
    }

    @After
    public void tearDown() {
        applicationConfigRepo.deleteAll();
    }

    @Test
    public void testRescheduleJob() throws Exception {
        Scheduler theScheduler = jobSchedulerService.getScheduler();
        Assert.assertEquals( true, theScheduler.isStarted() );

        String theReturn = jobSchedulerService.rescheduleJob( PointsRewardSchedulerTrigger.TRIGGER_NAME, 
        		PointsRewardSchedulerTrigger.TRIGGER_GROUP, CRONEXPRESSION, AppKey.REWARDS_JOB );
        Assert.assertEquals( CRONEXPRESSION, theReturn );

        Assert.assertEquals( CRONEXPRESSION, jobSchedulerService.getCronExpression( PointsRewardSchedulerTrigger.TRIGGER_NAME, 
        		PointsRewardSchedulerTrigger.TRIGGER_GROUP ) );
    }

}
