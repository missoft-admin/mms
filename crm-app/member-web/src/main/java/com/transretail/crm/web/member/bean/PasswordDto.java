package com.transretail.crm.web.member.bean;

import org.hibernate.validator.constraints.NotEmpty;

/**
 *
 */
public class PasswordDto {
    @NotEmpty
    private String password;

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
