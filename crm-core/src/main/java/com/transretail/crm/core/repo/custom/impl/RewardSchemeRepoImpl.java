package com.transretail.crm.core.repo.custom.impl;


import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.util.StringUtils;

import com.mysema.query.BooleanBuilder;
import com.mysema.query.jpa.impl.JPAQuery;
import com.mysema.query.types.Order;
import com.mysema.query.types.expr.BooleanExpression;
import com.mysema.query.types.path.EnumPath;
import com.mysema.query.types.path.NumberPath;
import com.mysema.query.types.path.StringPath;
import com.transretail.crm.core.entity.QRewardSchemeModel;
import com.transretail.crm.core.entity.RewardSchemeModel;
import com.transretail.crm.core.entity.enums.Status;
import com.transretail.crm.core.repo.custom.RewardSchemeRepoCustom;


public class RewardSchemeRepoImpl implements RewardSchemeRepoCustom {

    @PersistenceContext
    EntityManager em;


    @Override
    public List<RewardSchemeModel> findAllActivePointsSchemes() {
        JPAQuery theQuery = new JPAQuery(em);
        QRewardSchemeModel theModel = QRewardSchemeModel.rewardSchemeModel;

        theQuery.from(theModel).where(new BooleanBuilder().
        		orAllOf(isActiveVoucherExpression(theModel)));
        return theQuery.list(theModel);
    }

    @Override
    public List<RewardSchemeModel> findAllActivePointsSchemesSortValueFrom( Order order ) {
        JPAQuery query = new JPAQuery(em);
        QRewardSchemeModel qModel = QRewardSchemeModel.rewardSchemeModel;

        query.from(qModel).where( new BooleanBuilder().
        		orAllOf( isActiveVoucherExpression( qModel ) ) ).
        		orderBy( order == Order.ASC? qModel.valueFrom.asc() : qModel.valueFrom.desc() );
        return query.list(qModel);
    }

    @Override
    public Double findLowestActivePointsSchemeValue() {
        JPAQuery theQuery = new JPAQuery(em);
        QRewardSchemeModel theModel = QRewardSchemeModel.rewardSchemeModel;
        
        return theQuery.from(theModel).where( new BooleanBuilder().and(
        		isActiveVoucherExpression(theModel) ) ).singleResult(theModel.valueFrom.min());
    }
    
    @Override
    public boolean checkRangeOverlap(Long id, Double from, Double to) {
    	JPAQuery query = new JPAQuery(em);
    	QRewardSchemeModel model = QRewardSchemeModel.rewardSchemeModel;
    	
    	if(id == null) id = 0L;
    	
    	query.from(model).where(
    			new BooleanBuilder(model.valueFrom.goe(from).and(model.valueTo.loe(to))
    					.and(model.id.ne(id)).and(isActiveVoucherExpression(model))
    			));
    	return query.count() > 0;
    }
    
    @Override
    public boolean checkValueOverlap(Long id, Double value) {
    	JPAQuery query = new JPAQuery(em);
    	QRewardSchemeModel model = QRewardSchemeModel.rewardSchemeModel;
    	
    	if(id == null) id = 0L;
    	
    	query.from(model).where(
    			new BooleanBuilder(model.valueFrom.loe(value).and(model.valueTo.goe(value))
    					.and(model.id.ne(id)).and(isActiveVoucherExpression(model))
    			));
    	return query.count() > 0;
    }
    
    private Double findHighestActivePointsSchemeValue() {
        JPAQuery theQuery = new JPAQuery(em);
        QRewardSchemeModel theModel = QRewardSchemeModel.rewardSchemeModel;
        
        return theQuery.from(theModel).where( new BooleanBuilder().and(
        		isActiveVoucherExpression(theModel) ) ).singleResult(theModel.valueFrom.max());
    }
    
    @Override
    public RewardSchemeModel findByAmount( Double inAmount ) {
    	JPAQuery theQuery = new JPAQuery(em);
    	QRewardSchemeModel theModel = QRewardSchemeModel.rewardSchemeModel;
        
        return theQuery.from(theModel).where( new BooleanBuilder().orAllOf(
        		isActiveVoucherExpression(theModel), 
        		theModel.valueFrom.loe(inAmount), 
        		isGreaterThanHighestValueFrom(theModel.valueTo, inAmount) ) ).singleResult(theModel);
    }
    

    private BooleanExpression isGreaterThanHighestValueFrom( NumberPath<Double> inNumPath, Double inAmount ) {
    	return inAmount < findHighestActivePointsSchemeValue() ? inNumPath.goe( inAmount ) : null;
    }
    
    private BooleanExpression isActiveVoucherExpression(QRewardSchemeModel inQModel) {
    	return isEnumPropertyIn(inQModel.status, Status.ACTIVE);
    }

	@SuppressWarnings({ "unchecked", "rawtypes" })
	private BooleanExpression isEnumPropertyIn(EnumPath inEnumPath, Enum inProp) {
		return inProp != null ? inEnumPath.in(inProp) : null;
	}
}
