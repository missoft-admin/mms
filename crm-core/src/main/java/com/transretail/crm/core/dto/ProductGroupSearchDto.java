package com.transretail.crm.core.dto;


import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.mysema.query.types.expr.BooleanExpression;
import com.transretail.crm.common.service.dto.request.AbstractSearchFormDto;
import com.transretail.crm.core.entity.QProductGroup;
import com.transretail.crm.core.entity.lookup.LookupDetail;


public class ProductGroupSearchDto extends AbstractSearchFormDto {

	private List<LookupDetail> productClassifications;
	private Long id;
	private String itemCode;
	private String division;
	private String department;
	private String group;
	private String family;
	private String subfamily;
	private Boolean valid;



	@Override
	@JsonIgnore
	public BooleanExpression createSearchExpression() {

		List<BooleanExpression> expr = new ArrayList<BooleanExpression>();
		QProductGroup qgroup = QProductGroup.productGroup;

		if ( null != valid ) {
			expr.add( BooleanExpression.anyOf( qgroup.valid.eq( valid ), 
					valid? qgroup.valid.isNull() : null ) );
		}

		return BooleanExpression.allOf( expr.toArray( new BooleanExpression[expr.size()] ) );
	}



	public List<LookupDetail> getProductClassifications() {
		return productClassifications;
	}

	public void setProductClassifications(List<LookupDetail> productClassifications) {
		this.productClassifications = productClassifications;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getItemCode() {
		return itemCode;
	}

	public void setItemCode(String itemCode) {
		this.itemCode = itemCode;
	}

	public String getDivision() {
		return division;
	}

	public void setDivision(String division) {
		this.division = division;
	}

	public String getDepartment() {
		return department;
	}

	public void setDepartment(String department) {
		this.department = department;
	}

	public String getGroup() {
		return group;
	}

	public void setGroup(String group) {
		this.group = group;
	}

	public String getFamily() {
		return family;
	}

	public void setFamily(String family) {
		this.family = family;
	}

	public String getSubfamily() {
		return subfamily;
	}

	public void setSubfamily(String subfamily) {
		this.subfamily = subfamily;
	}

	public Boolean isValid() {
		return valid;
	}

	public void setValid(Boolean valid) {
		this.valid = valid;
	}

}
