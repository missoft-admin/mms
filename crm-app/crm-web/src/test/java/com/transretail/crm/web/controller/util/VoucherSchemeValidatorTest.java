package com.transretail.crm.web.controller.util;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.context.MessageSource;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.validation.Errors;

import com.transretail.crm.core.entity.RewardSchemeModel;
import com.transretail.crm.core.entity.enums.Status;
import com.transretail.crm.core.service.PointsSchemeService;
import com.transretail.crm.web.rest.BaseMockController;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = "classpath*:META-INF/spring/applicationContext*.xml")
@WebAppConfiguration
public class VoucherSchemeValidatorTest extends BaseMockController {
	@Mock
	PointsSchemeService service;
	
	@Mock
	MessageSource messageSource;
	
	@InjectMocks
	VoucherSchemeValidator validator;
	
	@Mock
	Errors errors;
	
	RewardSchemeModel invalidModel;
	
	public VoucherSchemeValidatorTest() {
		invalidModel = new RewardSchemeModel();
		invalidModel.setAmount(-1.0);
		invalidModel.setStatus(Status.ACTIVE);
		invalidModel.setValidPeriod(-1);
		invalidModel.setValueFrom(-1.0);
		invalidModel.setValueTo(-3.0);
	}
	
	@Before
	public void setUp() {
		super.setUp();
	}
	
	@Test
	public void testValidateIncorrectFieldValues() {		
		validator.validateIncorrectFieldValues(invalidModel, errors);
		
		Mockito.verify(errors, Mockito.times(5)).reject(Mockito.anyString());
	}
	
	@Test
	public void testValidateOverlappingValues() {		
		Mockito.when(service.hasOverlappingRange(Mockito.anyLong(), 
				Mockito.anyDouble(), Mockito.anyDouble())).thenReturn(true);
		
		validator.validateOverlappingValues(invalidModel, errors);
		
		Mockito.verify(errors).reject(Mockito.anyString());
		
		Mockito.when(service.hasOverlappingValue(Mockito.anyLong(), 
				Mockito.anyDouble())).thenReturn(true);
		
		validator.validateOverlappingValues(invalidModel, errors);
		
		Mockito.verify(errors, Mockito.times(2)).reject(Mockito.anyString());
	}
}
