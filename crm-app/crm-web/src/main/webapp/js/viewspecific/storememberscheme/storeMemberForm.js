$(document).ready( function() {

  var theErrorContainer = $( "#content_store_member_form" ).find("#content_error");

  var $storeMemberDialog = $( "#content_store_member_form" );
  var $storeMemberForm = $( "#storeMemberForm" );



  initFormFields();
  initDialog();
  


  function initFormFields() {

    $("#saveButton").click( function(e) {
      e.preventDefault();
      saveUser($storeMemberForm.attr("action"));
    });

  }

  function initDialog() {

    $storeMemberDialog.modal({});
    $storeMemberDialog.on( "hide", function(e) { if ( e.target === this ) { theErrorContainer.hide(); } });
  }



  function saveUser( inUrl ) {
    $( "#content_store_member_form" ).find( "#saveButton" ).prop( "disabled", true );
    $.ajax({
      cache     : false, 
      dataType  : "json",
      type    : "POST", 
      url     : inUrl,
      data    : $storeMemberForm.serialize(),
      error     : function(jqXHR, textStatus, errorThrown) {},
      success   : function( inResponse ) {    
        if ( inResponse.success ) {
          $storeMemberDialog.modal( "hide" );
          $storeMemberDialog.remove();
          $( "#list_store_member" ).ajaxDataTable( "search" );//window.location.href = $theUserForm.data( "url" );
        }
        else {
          $( "#content_store_member_form" ).find( "#saveButton" ).prop( "disabled", false );
          var theErrorInfo = "";
          $.each( inResponse.result, function( key, value ) {
            theErrorInfo += ( ( key + 1 ) + ". " + value.code + "<br>" );
          });
          theErrorContainer.find( "div" ).html( theErrorInfo );
          theErrorContainer.show( "slow" );
        }
      }
    });
  }

});
