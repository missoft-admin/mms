<%@ include file="../../taglibs.jsp"%>


    <div class="tab-pane" id="work">
    <fieldset class="container-fluid">
      <div class="control-group">
        <form:label path="occupationCode" cssClass="control-label"
          cssErrorClass="control-label error">
          <spring:message code="occupation" />
        </form:label>
        <div class="controls">
          <form:select path="occupationCode" cssClass="input-xlarge">
            <form:option value="">&nbsp;</form:option>
            <form:options items="${occupations}" itemValue="code"
              itemLabel="description" />
          </form:select>
        </div>
      </div>

	    <div class="control-group">
	      <spring:message code="company_name" var="companyName" />
	      <label for="companyName" class="control-label"><c:out value="${companyName}" />:</label>
	      <div class="controls"><input type="text" name="companyName" placeholder="${companyName}" class="input-xlarge" /></div>
	    </div>

      <div class="control-group">
        <form:label path="businessFieldCode" cssClass="control-label"
          cssErrorClass="control-label error">
          <spring:message code="business_field" />
        </form:label>
        <div class="controls">
          <form:select path="businessFieldCode" cssClass="input-xlarge">
            <form:option value="">&nbsp;</form:option>
            <form:options items="${businessFields}" itemValue="code"
              itemLabel="description" />
          </form:select>
        </div>
      </div>

      <sec:authorize var="isEmployee" ifAnyGranted="MTYP002" />
      <c:if test="${isEmployee}">
      <div class="control-group">
        <form:label path="departmentCode" cssClass="control-label" cssErrorClass="control-label error">
          <spring:message code="department_field"/>
        </form:label>
        <div class="controls">
          <form:select path="departmentCode" cssClass="input-xlarge">
            <form:option value="">&nbsp;</form:option>
            <form:options items="${departmentField}" itemValue="code" itemLabel="description"/>
          </form:select>
        </div>
      </div>
      </c:if>

      <%-- <div class="control-group">
        <form:label path="businessEmail" cssClass="control-label"
          cssErrorClass="control-label error">
          <spring:message code="business_email" />
        </form:label>
        <div class="controls">
          <form:input path="businessEmail" cssClass="input-xlarge" />
        </div>
      </div> --%>

      <!-- <hr /> -->

      <fieldset class="container-fluid">
        <legend class=""><spring:message code="business_address" /></legend>
        <div class="row-fluid address-line">
          <spring:message code="street" var="street" />
          <fieldset class="span9">
            <label for="street">${street }</label>
            <form:input path="businessStreet" placeholder="${street}"
              cssClass="span12" />
          </fieldset>
          <spring:message code="streetNo" var="streetNumber" />
          <fieldset class="span3">
            <label for="streetNumber">${streetNumber }</label>
            <form:input path="businessStreetNumber" cssClass="span12"
              placeholder="${streetNumber}" />
          </fieldset>
        </div>
        <div class="row-fluid address-line">
          <spring:message code="block" var="block" />
          <fieldset class="span6">
            <label for="block">${block }</label>
            <form:input path="businessBlock" placeholder="${block}" cssClass="span12" />
          </fieldset>
          <spring:message code="km" var="km" />
          <fieldset class="span2">
            <label for="km">${km }</label>
            <form:input path="businessKm" placeholder="${km}" cssClass="span12" />
          </fieldset>
          <spring:message code="rt" var="rt" />
          <fieldset class="span2">
            <label for="rt">${rt }</label>
            <form:input path="businessRt" placeholder="${rt}" cssClass="span12" />
          </fieldset>
          <spring:message code="rw" var="rw" />
          <fieldset class="span2">
            <label for="rw">${rw }</label>
            <form:input path="businessRw" placeholder="${rw}" cssClass="span12" />
          </fieldset>
        </div>
        <div class="row-fluid address-line">
          <spring:message code="building" var="building" />
          <fieldset class="span8">
            <label for="building">${building }</label>
            <form:input path="businessBuilding" placeholder="${building}"
              cssClass="span12" />
          </fieldset>
          <spring:message code="floor" var="floor" />
          <fieldset class="span2">
            <label for="floor">${floor }</label>
            <form:input path="businessFloor" placeholder="${floor}" cssClass="span12" />
          </fieldset>
          <spring:message code="room" var="room" />
          <fieldset class="span2">
            <label for="room">${room }</label>
            <form:input path="businessRoom" placeholder="${room}" cssClass="span12" />
          </fieldset>
        </div>
        <div class="row-fluid address-line">
          <spring:message code="postcode" var="postCode" />
          <fieldset class="span2">
            <label for="postCode">${postCode }</label>
            <form:input path="businessPostCode" placeholder="${postCode}"
              cssClass="span12" />
          </fieldset>
          <spring:message code="subdistrict" var="subdistrict" />
          <fieldset class="span4">
            <label for="subdistrict">${subdistrict }</label>
            <form:input path="businessSubdistrict" placeholder="${subdistrict}"
              cssClass="span12" />
          </fieldset>
          <spring:message code="district" var="district" />
          <fieldset class="span6">
            <label for="district">${district }</label>
            <form:input path="businessDistrict" placeholder="${district}"
              cssClass="span12" />
          </fieldset>
        </div>
        <div class="row-fluid address-line">
          <spring:message code="province" var="province" />
          <fieldset class="span6">
            <label for="city">${province }</label>
            <form:input path="businessProvince" placeholder="${province}" cssClass="span12" />
          </fieldset>
          <spring:message code="city" var="city" />
          <fieldset class="span6">
            <label for="city">${city }</label>
            <form:input path="businessCity" placeholder="${city}" cssClass="span12" />
          </fieldset>
        </div>
      </fieldset>
    </fieldset>
    </div>