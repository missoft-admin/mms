package com.transretail.crm.report.template.impl;

import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import com.transretail.crm.core.entity.PeopleSoftStore;
import com.transretail.crm.core.entity.lookup.Store;
import com.transretail.crm.core.service.PsoftStoreService;
import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.JRParameter;
import net.sf.jasperreports.engine.JRVirtualizer;
import net.sf.jasperreports.engine.fill.JRSwapFileVirtualizer;
import net.sf.jasperreports.engine.util.JRSwapFile;

import org.joda.time.LocalDate;
import org.joda.time.LocalDateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.MessageSourceAware;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.context.support.MessageSourceAccessor;
import org.springframework.stereotype.Service;

import com.mysema.query.BooleanBuilder;
import com.mysema.query.jpa.impl.JPAQuery;
import com.mysema.query.types.Projections;
import com.mysema.query.types.QBean;
import com.transretail.crm.common.reporting.engine.data.JRQueryDSLDataSource;
import com.transretail.crm.common.reporting.jasper.ClasspathFileResolver;
import com.transretail.crm.common.reporting.jasper.DefaultJRProcessor;
import com.transretail.crm.common.reporting.jasper.JRProcessor;
import com.transretail.crm.common.util.DateUtil;
import com.transretail.crm.core.entity.QPeopleSoftStore;
import com.transretail.crm.core.entity.enums.ReportType;
import com.transretail.crm.core.entity.lookup.LookupDetail;
import com.transretail.crm.core.reporting.support.StoreFieldValueCustomizer;
import com.transretail.crm.core.security.util.UserUtil;
import com.transretail.crm.core.service.CodePropertiesService;
import com.transretail.crm.core.service.LookupService;
import com.transretail.crm.core.service.StoreService;
import com.transretail.crm.giftcard.dto.ProductProfileDto;
import com.transretail.crm.giftcard.entity.QGiftCardInventory;
import com.transretail.crm.giftcard.entity.QGiftCardTransaction;
import com.transretail.crm.giftcard.entity.QGiftCardTransactionItem;
import com.transretail.crm.giftcard.entity.QSalesOrder;
import com.transretail.crm.giftcard.entity.support.GiftCardSalesType;
import com.transretail.crm.giftcard.service.ProductProfileService;
import com.transretail.crm.report.template.FilterField;
import com.transretail.crm.report.template.NoDataFoundSupport;
import com.transretail.crm.report.template.ReportTemplate;

/**
 * @author ftopico
 */
@Service("transactionDetailReport")
public class TransactionDetailReport implements ReportTemplate, MessageSourceAware, NoDataFoundSupport {
    
    @PersistenceContext
    private EntityManager em;
    @Autowired
    private ProductProfileService productProfileService;
    @Autowired
    private LookupService lookupService;
    @Autowired
    private CodePropertiesService codePropertiesService;

    private static final String PERMISSION_CODE = "TRANSACTION_DETAIL_REPORT";
    protected static final String TEMPLATE_NAME = "Transaction Detail Report";
    protected static final String REPORT_NAME = "reports/transaction_detail_report.jasper";
    protected static final String REPORT_FILTER_DATE_FROM = "DATE_FROM";
    protected static final String REPORT_FILTER_DATE_TO = "DATE_TO";
    protected static final String REPORT_FILTER_PRODUCT = "PRODUCT";
    protected static final String REPORT_FILTER_BU = "BU";
    protected static final String REPORT_FILTER_SALES_TYPE = "SALES_TYPE";
    protected static final String REPORT_FILTER_AREA_REGION = "REGION";
    protected static final String REPORT_FILTER_DATE_FORMAT = "dd-MM-yyyy";
    //Header Params
    private static final String PARAM_PRINT_DATE = "printedDate";
    private static final String PARAM_PRINTED_BY = "printedBy";
    private static final String PARAM_TRANSACTION_DT_FROM = "transactionDateFrom";
    private static final String PARAM_TRANSACTION_DT_TO = "transactionDateTo";
    private static final String PARAM_PRODUCT_CODE = "productCode";
    private static final String PARAM_BU = "bu";
    private static final String PARAM_PRODUCT_NAME = "productName";
    private static final DateTimeFormatter FULL_DATE_PATTERN = DateTimeFormat.forPattern("dd-MM-yyyy").withLocale(LocaleContextHolder.getLocale());
    private MessageSourceAccessor messageSource;
    @Autowired
    private StoreService storeService;
    @Autowired
    private PsoftStoreService psoftStoreService;
    
    public TransactionDetailReport() {
    }
    
    @Override
    public void setMessageSource(MessageSource messageSource) {
        this.messageSource = new MessageSourceAccessor(messageSource);
    }

    @Override
    public String getName() {
        return TEMPLATE_NAME;
    }

    @Override
    public String getDescription() {
        return messageSource.getMessage("gc_transaction_lbl_title");
    }

    @Override
    public Set<FilterField> getFilters() {
        Set<FilterField> filters = new LinkedHashSet<FilterField>();
        filters.add(FilterField.createDateField(REPORT_FILTER_DATE_FROM,
                messageSource.getMessage("gc_expirebalance_report_label_datefrom"),
                DateUtil.SEARCH_DATE_FORMAT, FilterField.DateTypeOption.PLAIN, true));
        filters.add(FilterField.createDateField(REPORT_FILTER_DATE_TO,
                messageSource.getMessage("gc_expirebalance_report_label_dateto"),
                DateUtil.SEARCH_DATE_FORMAT, FilterField.DateTypeOption.PLAIN, true));
        Map<String, String> products = new LinkedHashMap<String, String>();
        List<ProductProfileDto> productProfiles = productProfileService.findAllDtos();
        if (!productProfiles.isEmpty()) {
            for (ProductProfileDto dto : productProfiles) {
                products.put(dto.getProductCode(), dto.getProductDesc());
            }
        }
        filters.add(FilterField.createDropdownField(REPORT_FILTER_PRODUCT, messageSource.getMessage("gc_transaction_lbl_product"), products));

        final Store currentStore = UserUtil.getCurrentUser().getStore();
        final PeopleSoftStore softStore = psoftStoreService.getStoreMapping(currentStore.getCode());
        //BU
        Map<String, String> bu = new LinkedHashMap<String, String>();
        if (!currentStore.getCode().equals("10007")) {

            final LookupDetail detail = softStore.getBusinessUnit();
            bu.put(detail.getCode(), detail.getDescription());

        } else {

            List<LookupDetail> buList = lookupService.getActiveDetailsByHeaderCode(codePropertiesService.getHeaderBusinessUnit());
            Collections.sort(buList, ALPHA_ORDER);
            for (LookupDetail buDetail : buList) {
                bu.put(buDetail.getCode(),buDetail.getDescription());
            }
        }
        filters.add(FilterField.createDropdownField(REPORT_FILTER_BU, messageSource.getMessage("gc_transaction_lbl_bu"), bu));
        
        
        //Sales Type
        Map<String, String> salesTypes = new LinkedHashMap<String, String>();
        for (GiftCardSalesType gcSalesType : GiftCardSalesType.values()) {
            salesTypes.put(gcSalesType.name(), gcSalesType.name());
        }
        filters.add(FilterField.createDropdownField(REPORT_FILTER_SALES_TYPE, messageSource.getMessage("gc_transaction_lbl_sales_type"), salesTypes));
        
        //Area/Region
        Map<String, String> region = new LinkedHashMap<String, String>();
        List<LookupDetail> regionList = lookupService.getActiveDetailsByHeaderCode(codePropertiesService.getHeaderBusinessRegion());
        Collections.sort(regionList, ALPHA_ORDER);
        for (LookupDetail regionDetail : regionList) {
            region.put(regionDetail.getCode(), regionDetail.getDescription());
        }
        filters.add(FilterField.createDropdownField(REPORT_FILTER_AREA_REGION, messageSource.getMessage("gc_transaction_lbl_area"), region));
        
        return filters;
    }

    private static Comparator<LookupDetail> ALPHA_ORDER = new Comparator<LookupDetail>() {
        public int compare(LookupDetail ld1, LookupDetail ld2) {
            int x = String.CASE_INSENSITIVE_ORDER.compare(ld1.getDescription(), ld2.getDescription());
            if (x== 0) {
                x= ld1.getDescription().compareTo(ld2.getDescription());
            }
            return x;
        }
    };
    
    @Override
    public boolean canView(Set<String> permissions) {
        return permissions.contains(PERMISSION_CODE);
    }

    @Override
    public ReportType[] getReportTypes() {
        return new ReportType[]{ReportType.EXCEL};
    }

    @Override
    public JRProcessor createJrProcessor(Map<String, String> map) {
        JRDataSource datasource = createDataSource(map);
        
        Map<String, Object> parameters = new HashMap<String, Object>();
        parameters.put(PARAM_PRINT_DATE, FULL_DATE_PATTERN.print(new LocalDate()));
        parameters.put(PARAM_PRINTED_BY, UserUtil.getCurrentUser().getUsername());
        parameters.put(PARAM_TRANSACTION_DT_FROM, map.get(REPORT_FILTER_DATE_FROM));
        parameters.put(PARAM_TRANSACTION_DT_TO, map.get(REPORT_FILTER_DATE_TO));
        parameters.put(PARAM_BU, map.get(REPORT_FILTER_BU));
        if (!map.get(REPORT_FILTER_PRODUCT).equalsIgnoreCase("")) {
            parameters.put(PARAM_PRODUCT_CODE, map.get(REPORT_FILTER_PRODUCT));
            parameters.put(PARAM_PRODUCT_NAME, 
                    productProfileService.findProductProfileByCode(map.get(REPORT_FILTER_PRODUCT)).getProductDesc());
        } else {
            parameters.put(PARAM_PRODUCT_CODE, "");
            parameters.put(PARAM_PRODUCT_NAME, "");
        }
            
        JRSwapFile swapFile = new JRSwapFile("/tmp", 1024, 1024);
        JRVirtualizer virtualizer = new JRSwapFileVirtualizer(50, swapFile, true);
        
        parameters.put("SUB_DATA_SOURCE", datasource);
        
        DefaultJRProcessor jrProcessor = new DefaultJRProcessor(REPORT_NAME/*, beansDataSource*/);
        jrProcessor.addParameter(JRParameter.REPORT_VIRTUALIZER, virtualizer);
        jrProcessor.addParameters(parameters);
        jrProcessor.setFileResolver(new ClasspathFileResolver("reports"));
        
        return jrProcessor;
    }
    
    private JRDataSource createDataSource(Map<String, String> map) {
        QGiftCardTransaction qtx = QGiftCardTransaction.giftCardTransaction;
        QGiftCardTransactionItem qtxi = QGiftCardTransactionItem.giftCardTransactionItem;
        QPeopleSoftStore qp = QPeopleSoftStore.peopleSoftStore;
        QGiftCardInventory qgc = QGiftCardInventory.giftCardInventory;
        
        QSalesOrder qso = QSalesOrder.salesOrder;
        
        BooleanBuilder exp = new BooleanBuilder(qtx.transactionDate.between(FULL_DATE_PATTERN.parseLocalDateTime(map.get(REPORT_FILTER_DATE_FROM)), 
                FULL_DATE_PATTERN.parseLocalDateTime(map.get(REPORT_FILTER_DATE_TO))));
        
        if (!map.get(REPORT_FILTER_PRODUCT).equalsIgnoreCase("")) {
            exp.and(qgc.productCode.eq(map.get(REPORT_FILTER_PRODUCT)));
        }
        
        if (!map.get(REPORT_FILTER_BU).equalsIgnoreCase("")) {
            exp.and(qp.businessUnit.code.eq(map.get(REPORT_FILTER_BU)));
        }
        
        if (!map.get(REPORT_FILTER_SALES_TYPE).equalsIgnoreCase("")) {
            exp.and(qtx.salesType.eq(GiftCardSalesType.valueOf(map.get(REPORT_FILTER_SALES_TYPE))));
        }
        
        if (!map.get(REPORT_FILTER_AREA_REGION).equalsIgnoreCase("")) {
            exp.and(qp.businessRegion.code.eq(map.get(REPORT_FILTER_AREA_REGION)));
        }
        
        JPAQuery query = createQuery(map).orderBy(qtx.transactionDate.desc());
        
        QBean<TransactionDetail> projection = Projections.fields(TransactionDetail.class,
                qtx.transactionDate.as("transactionDate"),
                qp.businessUnit.code.coalesce("").as("bu"),
                qtx.salesType.stringValue().as("salesType"),
                qtx.merchantId.as("storeName"),
                qso.orderNo.as("salesOrderNo"),
                qso.customer.name.as("customer"),
                qgc.barcode.as("cardNo"),
                qtx.terminalId.as("terminalId"),
                qtx.cashierId.as("cashierId"),
                qtx.transactionNo.as("transactionNo"),
                qgc.productName.as("productName"),
                qgc.productCode.as("productCode"),
                qtx.transactionType.stringValue().as("transaction"),
                qgc.faceValue.doubleValue().coalesce(0.0).as("faceAmount"),
                qtxi.transactionAmount.doubleValue().coalesce(0.0).as("redeemedAmount"),
                qtx.discountPercentage.doubleValue().coalesce(0.0).as("discount"));
        
        return new JRQueryDSLDataSource(query, projection,
                new StoreFieldValueCustomizer("storeName", storeService));
        
    }

    private JPAQuery createQuery(Map<String, String> map) {
        QGiftCardTransaction qtx = QGiftCardTransaction.giftCardTransaction;
        QGiftCardTransactionItem qtxi = QGiftCardTransactionItem.giftCardTransactionItem;
        QPeopleSoftStore qp = QPeopleSoftStore.peopleSoftStore;
        QGiftCardInventory qgc = QGiftCardInventory.giftCardInventory;
        
        QSalesOrder qso = QSalesOrder.salesOrder;
        
        BooleanBuilder exp = new BooleanBuilder(qtx.transactionDate.between(FULL_DATE_PATTERN.parseLocalDateTime(map.get(REPORT_FILTER_DATE_FROM)), 
                FULL_DATE_PATTERN.parseLocalDateTime(map.get(REPORT_FILTER_DATE_TO))));
        
        if (!map.get(REPORT_FILTER_PRODUCT).equalsIgnoreCase("")) {
            exp.and(qgc.productCode.eq(map.get(REPORT_FILTER_PRODUCT)));
        }


        final Store currentUserStore = UserUtil.getCurrentUser().getStore();
        final PeopleSoftStore storemapping = psoftStoreService.getStoreMapping(currentUserStore.getCode());

        if (!currentUserStore.getCode().equals("10007")) {

            exp.and(qtx.merchantId.eq(currentUserStore.getCode()));
            if (!map.get(REPORT_FILTER_BU).equalsIgnoreCase("")) {
                exp.and(qp.businessUnit.code.eq(storemapping.getBusinessUnit().getCode()));
            } else {
                exp.and(qp.businessUnit.code.eq(map.get(REPORT_FILTER_BU)));
            }

        } else {

            if (!map.get(REPORT_FILTER_BU).equalsIgnoreCase("")) {
                exp.and(qp.businessUnit.code.eq(map.get(REPORT_FILTER_BU)));
            }
        }

        if (!map.get(REPORT_FILTER_SALES_TYPE).equalsIgnoreCase("")) {
            exp.and(qtx.salesType.eq(GiftCardSalesType.valueOf(map.get(REPORT_FILTER_SALES_TYPE))));
        }
        
        if (!map.get(REPORT_FILTER_AREA_REGION).equalsIgnoreCase("")) {
            exp.and(qp.businessRegion.code.eq(map.get(REPORT_FILTER_AREA_REGION)));
        }
        
        JPAQuery query = new JPAQuery(em).from(qtx)
            .leftJoin(qtx.peoplesoftStoreMapping, qp)
            .leftJoin(qtx.transactionItems, qtxi)
            .leftJoin(qtxi.giftCard, qgc)
            .leftJoin(qtx.salesOrder, qso)
            .where(exp.getValue());
            
        return query;
    }
    
    public static class TransactionDetail {
        private LocalDateTime transactionDate;
        private String bu;
        private String salesType;
        private String storeName;
        private String salesOrderNo;
        private String customer;
        private String cardNo;
        private String terminalId;
        private String cashierId;
        private String transactionNo;
        private String productCode;
        private String productName;
        private String transaction;
        private Double faceAmount;
        private Double redeemedAmount;
        private Double discount;
        
        public TransactionDetail() {
        }

        public LocalDateTime getTransactionDate() {
            return transactionDate;
        }

        public String getBu() {
            return bu;
        }

        public String getSalesType() {
            return salesType;
        }

        public String getStoreName() {
            return storeName;
        }

        public String getSalesOrderNo() {
            return salesOrderNo;
        }

        public String getCustomer() {
            return customer;
        }

        public String getCardNo() {
            return cardNo;
        }

        public String getTerminalId() {
            return terminalId;
        }

        public String getCashierId() {
            return cashierId;
        }

        public String getTransactionNo() {
            return transactionNo;
        }

        public String getProductCode() {
            return productCode;
        }

        public String getProductName() {
            return productName;
        }

        public String getTransaction() {
            return transaction;
        }

        public Double getFaceAmount() {
            return faceAmount;
        }

        public Double getRedeemedAmount() {
            return redeemedAmount;
        }

        public Double getDiscount() {
            return discount;
        }

        public void setTransactionDate(LocalDateTime transactionDate) {
            this.transactionDate = transactionDate;
        }

        public void setBu(String bu) {
            this.bu = bu;
        }

        public void setSalesType(String salesType) {
            this.salesType = salesType;
        }

        public void setStoreName(String storeName) {
            this.storeName = storeName;
        }

        public void setSalesOrderNo(String salesOrderNo) {
            this.salesOrderNo = salesOrderNo;
        }

        public void setCustomer(String customer) {
            this.customer = customer;
        }

        public void setCardNo(String cardNo) {
            this.cardNo = cardNo;
        }

        public void setTerminalId(String terminalId) {
            this.terminalId = terminalId;
        }

        public void setCashierId(String cashierId) {
            this.cashierId = cashierId;
        }

        public void setTransactionNo(String transactionNo) {
            this.transactionNo = transactionNo;
        }

        public void setProductCode(String productCode) {
            this.productCode = productCode;
        }

        public void setProductName(String productName) {
            this.productName = productName;
        }

        public void setTransaction(String transaction) {
            this.transaction = transaction;
        }

        public void setFaceAmount(Double faceAmount) {
            this.faceAmount = faceAmount;
        }

        public void setRedeemedAmount(Double redeemedAmount) {
            this.redeemedAmount = redeemedAmount;
        }

        public void setDiscount(Double discount) {
            this.discount = discount;
        }

    }
    
    public static class ReportBean {
        
        private List<TransactionDetail> list;
        
        public ReportBean() {
        }
        
        public ReportBean(List<TransactionDetail> list) {
            this.list = list;
        }

        public List<TransactionDetail> getList() {
            return list;
        }

        public void setList(List<TransactionDetail> list) {
            this.list = list;
        }
        
    }

    @Override
    public boolean isEmpty(Map<String, String> filter) {
        return !createQuery(filter).exists();
    }
}
