import java.net.URL;
import java.security.ProtectionDomain;

import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.webapp.WebAppContext;

/**
 * @author Allan G. Ramirez (agramirez@exist.com)
 */
public final class EmbeddedJettyServer {
    public static void main(String[] args) throws Exception {
        System.out.println("Main");
        int port = Integer.parseInt(System.getProperty("port", "8888"));
        Server server = new Server(port);

        ProtectionDomain domain = EmbeddedJettyServer.class.getProtectionDomain();
        URL location = domain.getCodeSource().getLocation();

        WebAppContext webapp = new WebAppContext();
        webapp.setContextPath("/");
        webapp.setDescriptor(location.toExternalForm() + "/WEB-INF/web.xml");
        webapp.setServer(server);
        webapp.setWar(location.toExternalForm());

//        (Optional) Set the directory the war will extract to.
//        If not set, java.io.tmpdir will be used, which can cause problems
//        if the temp directory gets cleaned periodically.
//        Your build scripts should remove this directory between deployments
//        webapp.setTempDirectory(new File("/path/to/webapp-directory"));

        server.setHandler(webapp);
        server.start();
        server.join();
    }
}
