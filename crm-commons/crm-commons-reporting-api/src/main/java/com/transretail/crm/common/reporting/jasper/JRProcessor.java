package com.transretail.crm.common.reporting.jasper;

import java.util.List;
import java.util.Map;

import com.transretail.crm.common.reporting.exception.ReportException;

import net.sf.jasperreports.engine.JRExporterParameter;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.export.JRExportProgressMonitor;


/**
 * @author Allan G. Ramirez (agramirez@exist.com)
 */
public interface JRProcessor {
    /**
     * Get jasper document instances of {@link #getReportFileName() getReportFileName}
     * that can be viewed, printed or exported to other formats.
     *
     * @return jasper page-oriented document.
     * @throws ReportException
     */
    List<JasperPrint> getJasperPrints() throws ReportException;

    /**
     * Get the listener that will be invoked after the jasper report is generated.
     *
     * @return listener that will be invoked after the jasper report is generated.
     */
    JRExportProgressMonitor getAfterPageExportListener();

    /**
     * Set the listener that will be invoked after the jasper report is generated.
     *
     * @param afterPageExportListener listener that will be invoked after the jasper report is generated.
     */
    void setAfterPageExportListener(JRExportProgressMonitor afterPageExportListener);

    /**
     * Get the raw report (.jrxml) or compiled report (.jasper) file
     * that will be filled with data from report data sources and parameters.
     *
     * @return .jasper or .jrxml file
     */
    String getReportFileName();

    /**
     * Parameter passed ($P{param}) when jasper report is generated.
     *
     * @param name  parameter name ($P{param})
     * @param value parameter value
     */
    void addParameter(String name, Object value);

    /**
     * Parameters passed ($P{param}) when jasper report is generated.
     *
     * @param parameters additional parameters
     */
    void addParameters(Map<String, Object> parameters);

    /**
     * Check if parameter name already exists.
     *
     * @param name
     * @return true if parameter name already exists
     */
    boolean hasParameter(String name);

    /**
     * Return JRExporterParameter specific to this JRProcessor. This is usually used to override default JRExporterParameter in exporters.
     *
     * @return JRExporterParameter specific to this JRProcessor
     */
    Map<JRExporterParameter, Object> getJRExporterParameters();
}
