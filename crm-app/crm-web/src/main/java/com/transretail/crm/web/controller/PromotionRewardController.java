package com.transretail.crm.web.controller;


import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.transretail.crm.core.dto.PromotionRewardDto;
import com.transretail.crm.core.service.CodePropertiesService;
import com.transretail.crm.core.service.LookupService;
import com.transretail.crm.core.service.PromotionService;
import com.transretail.crm.web.controller.util.ControllerResponse;


@RequestMapping("/promotion/reward")
@Controller
public class PromotionRewardController {

	@Autowired
    PromotionService promotionService;
	@Autowired
	CodePropertiesService codePropertiesService;
	@Autowired
    LookupService lookupService;

	//private static final String PATH_LIST 	          = "/promotion/reward/list";
	private static final String PATH_CREATE           = "/promotion/reward/create";
	private static final String PATH_CREATELIST       = "/promotion/reward/create/list";
	private static final String PATH_CREATELISTENTRY  = "/promotion/reward/create/list/entry";

	//private static final String UIATTR_REWARDS        = "rewards";
	private static final String UIATTR_REWARDIDX      = "rewardIdx";
	private static final String UIATTR_REWARDSELTYPE  = "rewardSelType";
	private static final String UIATTR_REWARDPOINTS   = "rewardPoints";
	private static final String UIATTR_REWARDDISCOUNT = "rewardDiscount";
	private static final String UIATTR_REWARDITEMPT   = "rewardItemPoint";
	private static final String UIATTR_REWARDREDEEM   = "rewardRedeemItem";
	//private static final String UIATTR_REWARDFREEBIE  = "rewardFreebie";

	@InitBinder
	void initBinder(WebDataBinder binder) {
		DateFormat theFormat = new SimpleDateFormat( "dd-MM-yyyy" );
		theFormat.setLenient( false );
		binder.registerCustomEditor( Date.class, new CustomDateEditor( theFormat, true ) );
	}




	@RequestMapping( value="/list/{type}", produces = "text/html" )
    public String listPromotionReward(
    		@PathVariable( value="type") String inType,
    		@ModelAttribute(value="reward") PromotionRewardDto inPromotionReward,
            Model inUiModel ) {

    	populatePromoRewardTypes( inUiModel, inType, 0 );
        return PATH_CREATELIST;
    }

    @RequestMapping( value="/create/{type}/{idx}", produces = "text/html" )
    public String createPromotionReward(
    		@PathVariable( value="type") String inType,
    		@PathVariable( value="idx") Long inIdx,
    		@ModelAttribute(value="reward") PromotionRewardDto inPromotionReward,
            Model inUiModel ) {

    	populatePromoRewardTypes( inUiModel, inType, inIdx );
        return PATH_CREATELISTENTRY;
    }

    @RequestMapping( value="/save", produces="application/json; charset=utf-8", method=RequestMethod.POST )
	public @ResponseBody ControllerResponse savePromotionReward( 
			@ModelAttribute(value="reward") PromotionRewardDto inReward,
    		BindingResult inBinding ) {

		ControllerResponse theResp = new ControllerResponse();
		theResp.setSuccess( true );

		if ( StringUtils.isBlank( inReward.getRemark() ) ) {
    		inBinding.reject( "promotion_msg_nameempty" );
    	}

    	if ( inBinding.hasErrors() ) {
    		theResp.setSuccess( false );
    		theResp.setResult( inBinding.getAllErrors() );
    	}
    	else {
    		promotionService.savePromotionRewardDto( inReward );
    		theResp.setResult( inReward );
    	}

		return theResp;
	}

	@RequestMapping( value="/save", produces="text/html", method=RequestMethod.GET )
    public String savePromotionReward(
    		@ModelAttribute(value="reward") PromotionRewardDto inReward,
    		BindingResult inBinding,
            Model inUiModel ) {

    	if ( StringUtils.isBlank( inReward.getRemark() ) ) {
    		inBinding.reject( "promotion_msg_nameempty" );
    	}

    	if ( !inBinding.hasErrors() ) {
        	promotionService.savePromotionRewardDto( inReward );
    	}
        return PATH_CREATE;
    }

    @RequestMapping( value="/itempoint", produces="application/json; charset=utf-8", method=RequestMethod.GET )
	public @ResponseBody ControllerResponse getItemPoint() {
		ControllerResponse theResp = new ControllerResponse();
		theResp.setSuccess( true );
		theResp.setResult( lookupService.getDetailByCode( codePropertiesService.getDetailRewardTypeItemPoint() ) );
		return theResp;
	}



    private void populatePromoRewardTypes( Model uiModel, String type, long idx ) {
    	uiModel.addAttribute( UIATTR_REWARDSELTYPE, type );
    	uiModel.addAttribute( UIATTR_REWARDPOINTS, codePropertiesService.getDetailRewardTypePoints().equalsIgnoreCase( type ) );
    	uiModel.addAttribute( UIATTR_REWARDDISCOUNT, codePropertiesService.getDetailRewardTypeDiscount().equalsIgnoreCase( type ) );
		uiModel.addAttribute( UIATTR_REWARDITEMPT, codePropertiesService.getDetailRewardTypeItemPoint().equalsIgnoreCase( type ) );
		uiModel.addAttribute( UIATTR_REWARDREDEEM, codePropertiesService.getDetailRewardTypeExchangeItem().equalsIgnoreCase( type ) );
    	uiModel.addAttribute( UIATTR_REWARDIDX, idx );

		if ( StringUtils.equalsIgnoreCase( codePropertiesService.getDetailRewardTypeExchangeItem(), type ) ) {
			uiModel.addAttribute( RedemptionPromoController.IS_REDEMPTION, true );
		}
    }

}
