package com.transretail.crm.core.service;

import java.util.List;

import com.transretail.crm.common.service.dto.request.PageSortDto;
import com.transretail.crm.core.dto.MemberSearchDto;
import com.transretail.crm.core.dto.MemberDto;
import com.transretail.crm.core.dto.MemberResultList;
import com.transretail.crm.core.entity.MemberModel;
import com.transretail.crm.core.entity.embeddable.Address;
import com.transretail.crm.core.entity.lookup.LookupDetail;

public interface MemberService {

    boolean emailExists(String email);

    boolean emailExists(String excludeUsername, String email);

    boolean usernameExists(String username);

    boolean accountIdExists(String accountId);

    boolean contactExists(String contact);

    boolean contactExists(String excludeUsername, String contact);

    boolean ktpIdExists(String username, String ktpId);

    long countAllCustomerModels();

    void deleteCustomerModel(MemberModel customerModel);

    MemberModel findByAccountId(String accountId);
    
    MemberDto findDtoByAccountId(String accountId);

    MemberModel findByContact(String contact);

    List<MemberModel> findByEmail(String email);

    MemberModel findCustomerModel(Long id);
    
    MemberDto findCustomer(Long id);

    List<MemberModel> findAllCustomerModels();
    
    List<MemberDto> findAllCustomerModelsAsDto();

    List<MemberModel> findCustomerModelEntries(int firstResult, int maxResults);
    
    List<MemberDto> findCustomerModelEntriesAsDto(int firstResult, int maxResults);

    void saveCustomerModel(MemberModel customerModel);
    
    void saveCustomerModel(MemberDto customerModel);

    MemberModel updateCustomerModel(MemberModel customerModel);
    
    MemberDto updateCustomerModel(MemberDto customerModel);

    MemberModel findDuplicate(MemberModel inModel);

    MemberModel findDuplicateContact(MemberModel inModel);

    MemberModel findDuplicateUsername(MemberModel inModel);

    MemberModel findDuplicateCompany(MemberModel inModel);

    MemberModel findDuplicateName(MemberModel inModel);

    MemberModel findDuplicateEmail(MemberModel inModel);
    
    MemberDto findDuplicateContact(MemberDto memberDto);

    MemberDto findDuplicateUsername(MemberDto memberDto);

    MemberDto findDuplicateCompany(MemberDto memberDto);

    MemberDto findDuplicateName(MemberDto memberDto);

    MemberDto findDuplicateEmail(MemberDto memberDto);
    
    MemberDto findDuplicateNpwpId(MemberDto member);

    MemberModel generateInitModel();

    List<MemberModel> findByCriteria(MemberModel inModel, String employeeTypeCode);
    
    MemberModel getModelByValidationCode(String validationCode);

    void updatePin(Long id, String newPin);

    Boolean validatePin(String accountId, String pin);

    List<MemberDto> findSupplementaryMembersByParentId(String accountId);
    
    void addSupplementaryMember(String parentId, MemberModel memberSupplement);

	List<MemberDto> findByCriteria(MemberDto inModel,  String employeeTypeCode);

	MemberModel findActiveWithAccountId(String accountId);

	void saveMember(MemberModel theModel);
	
	Address saveMemberAddress(Address address);

	MemberResultList listMemberDto(MemberSearchDto searchDto);

    List<MemberModel> findAllByMemberType(LookupDetail type);

	MemberResultList filterMemberTypes(MemberSearchDto dto);

	MemberDto findDuplicateKtpId(MemberDto memberDto);

	void activateMember(long id, boolean activate);

	long countAllActive();

	List<MemberModel> findAll(List<MemberModel> members);

    Long countMembers(MemberSearchDto searchDto);

    MemberResultList listMemberDtoForPrint(MemberSearchDto memberSearchDto,
            Integer segment);

    Integer getMemberReportLimit();
}
