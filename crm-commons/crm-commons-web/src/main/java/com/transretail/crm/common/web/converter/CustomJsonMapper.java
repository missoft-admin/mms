package com.transretail.crm.common.web.converter;

import java.sql.Timestamp;
import java.util.Date;

import org.joda.time.DateTime;
import org.joda.time.LocalDate;
import org.joda.time.LocalDateTime;

import com.fasterxml.jackson.core.Version;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.module.SimpleModule;

/**
 *
 */
public class CustomJsonMapper extends ObjectMapper {
    public static final int JSON_VERSION = 2;

    public CustomJsonMapper() {
        SimpleModule module = new SimpleModule("JSONModule", new Version(JSON_VERSION, 0, 0, null, null, null));
        module.addDeserializer(Boolean.class, new BooleanDeserializer());
        module.addSerializer(DateTime.class, new DateTimeSerializer());
        module.addSerializer(Timestamp.class, new TimestampSerializer());
        module.addDeserializer(Timestamp.class, new TimestampDeserializer());
        module.addDeserializer(DateTime.class, new DateTimeDeserializer());
        module.addDeserializer(LocalDate.class, new LocalDateDeserializer());
        module.addSerializer(LocalDate.class, new LocalDateSerializer());
        module.addDeserializer(Date.class, new DateDeserializer());
        module.addSerializer(Date.class, new DateSerializer());
        module.addDeserializer(LocalDateTime.class, new LocalDateTimeDeserializer());
        module.addSerializer(LocalDateTime.class, new LocalDateTimeSerializer());
        registerModule(module);
    }
}
