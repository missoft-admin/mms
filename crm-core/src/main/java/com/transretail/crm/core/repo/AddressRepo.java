package com.transretail.crm.core.repo;

import com.transretail.crm.common.repo.CrmQueryDslPredicateExecutor;
import com.transretail.crm.core.entity.embeddable.Address;

public interface AddressRepo extends CrmQueryDslPredicateExecutor<Address, Long> {
	Address findById(Long id);
}
