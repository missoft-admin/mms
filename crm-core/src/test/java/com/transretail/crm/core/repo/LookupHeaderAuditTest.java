package com.transretail.crm.core.repo;

import static org.junit.Assert.assertEquals;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.hibernate.envers.AuditReader;
import org.hibernate.envers.AuditReaderFactory;
import org.hibernate.envers.RevisionType;
import org.hibernate.envers.query.AuditEntity;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.support.TransactionCallbackWithoutResult;
import org.springframework.transaction.support.TransactionTemplate;

import com.transretail.crm.core.entity.lookup.LookupHeader;

/**
 * @author Allan G. Ramirez (agramirez@exist.com)
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = "classpath*:/META-INF/spring/applicationContext*.xml")
public class LookupHeaderAuditTest {
    @PersistenceContext
    private EntityManager em;
    @Autowired
    private PlatformTransactionManager transactionManager;
    @Autowired
    private LookupHeaderRepo lookupHeaderRepo;

    private String headerCode = "CODE";
    private String headerDesc = "DESC";
    private String headerNewDesc = "NEWDESC";

    @Before
    @Transactional
    public void onSetup() {
        lookupHeaderRepo.save(new LookupHeader(headerCode, headerDesc));
    }

    @After
    public void tearDown() {
        lookupHeaderRepo.delete(headerCode);
    }

    @Test
    public void testHistory() {
        TransactionTemplate transactionTemplate = new TransactionTemplate(transactionManager);
        transactionTemplate.execute(new TransactionCallbackWithoutResult() {
            @Override
            protected void doInTransactionWithoutResult(TransactionStatus status) {
                AuditReader reader = AuditReaderFactory.get(em);
                List<LookupHeader> headers = reader.createQuery().forRevisionsOfEntity(LookupHeader.class, true, false)
                    .add(AuditEntity.revisionType().eq(RevisionType.ADD))
                    .getResultList();
                assertEquals(1, headers.size());
                headers = reader.createQuery().forRevisionsOfEntity(LookupHeader.class, true, false)
                    .add(AuditEntity.revisionType().eq(RevisionType.MOD))
                    .getResultList();
                assertEquals(0, headers.size());
            }
        });

        transactionTemplate = new TransactionTemplate(transactionManager);
        transactionTemplate.execute(new TransactionCallbackWithoutResult() {
            @Override
            protected void doInTransactionWithoutResult(TransactionStatus status) {
                LookupHeader header = lookupHeaderRepo.findOne(headerCode);
                header.setDescription(headerNewDesc);
                lookupHeaderRepo.saveAndFlush(header);
            }
        });

        transactionTemplate = new TransactionTemplate(transactionManager);
        transactionTemplate.execute(new TransactionCallbackWithoutResult() {
            @Override
            protected void doInTransactionWithoutResult(TransactionStatus status) {
                AuditReader reader = AuditReaderFactory.get(em);
                List<LookupHeader> headers =
                    reader.createQuery().forRevisionsOfEntity(LookupHeader.class, true, false).getResultList();
                assertEquals(2, headers.size());
                assertEquals(headerDesc, headers.get(0).getDescription());
                assertEquals(headerNewDesc, headers.get(1).getDescription());

                headers = reader.createQuery().forRevisionsOfEntity(LookupHeader.class, true, false)
                    .add(AuditEntity.revisionType().eq(RevisionType.ADD))
                    .getResultList();
                assertEquals(1, headers.size());
                headers = reader.createQuery().forRevisionsOfEntity(LookupHeader.class, true, false)
                    .add(AuditEntity.revisionType().eq(RevisionType.MOD))
                    .getResultList();
                assertEquals(1, headers.size());
            }
        });
    }
}
