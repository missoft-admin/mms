package com.transretail.crm.giftcard.service.impl;

import java.math.BigDecimal;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.apache.commons.lang.BooleanUtils;
import org.apache.commons.lang3.StringUtils;
import org.hibernate.StatelessSession;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.support.TransactionSynchronizationAdapter;
import org.springframework.transaction.support.TransactionSynchronizationManager;

import com.google.common.collect.Lists;
import com.mysema.query.Tuple;
import com.mysema.query.jpa.JPASubQuery;
import com.mysema.query.jpa.JPQLQuery;
import com.mysema.query.jpa.impl.JPAQuery;
import com.mysema.query.jpa.impl.JPAUpdateClause;
import com.mysema.query.types.Predicate;
import com.mysema.query.types.expr.BooleanExpression;
import com.transretail.crm.common.service.dto.response.ResultList;
import com.transretail.crm.common.service.util.SpringDataPagingUtil;
import com.transretail.crm.core.service.CodePropertiesService;
import com.transretail.crm.giftcard.dto.ReturnRecordDto;
import com.transretail.crm.giftcard.dto.ReturnSearchDto;
import com.transretail.crm.giftcard.dto.SalesOrderDto;
import com.transretail.crm.giftcard.entity.GiftCardInventory;
import com.transretail.crm.giftcard.entity.GiftCardInventoryStock.GCIStockStatus;
import com.transretail.crm.giftcard.entity.QGiftCardInventory;
import com.transretail.crm.giftcard.entity.QSalesOrderAlloc;
import com.transretail.crm.giftcard.entity.QSalesOrderItem;
import com.transretail.crm.giftcard.entity.ReturnRecord;
import com.transretail.crm.giftcard.entity.SalesOrder;
import com.transretail.crm.giftcard.entity.support.GiftCardInventoryStatus;
import com.transretail.crm.giftcard.entity.support.ReturnStatus;
import com.transretail.crm.giftcard.repo.GiftCardInventoryRepo;
import com.transretail.crm.giftcard.repo.ReturnRecordRepo;
import com.transretail.crm.giftcard.service.GiftCardAccountingService;
import com.transretail.crm.giftcard.service.GiftCardInventoryStockService;
import com.transretail.crm.giftcard.service.GiftCardManager;
import com.transretail.crm.giftcard.service.ReturnGiftCardService;
import com.transretail.crm.giftcard.service.SalesOrderService;



@Service("returnGiftCardService")
public class ReturnGiftCardServiceImpl implements ReturnGiftCardService {
	
        private static final Logger logger = LoggerFactory.getLogger(ReturnGiftCardServiceImpl.class);
	@Autowired
	private ReturnRecordRepo returnRepo;
	@PersistenceContext
	private EntityManager em;
	@Autowired
	private StatelessSession statelessSession;
	@Autowired
	private CodePropertiesService codePropertiesService;
	@Autowired
	private GiftCardInventoryRepo inventoryRepo;
	@Autowired
	private GiftCardInventoryStockService stockService;
        @Autowired
        private GiftCardManager giftCardManager;
	@Autowired
	private SalesOrderService salesOrderService;
    @Autowired
	private GiftCardAccountingService accountingService;
	@Autowired
    private MessageSource messageSource;
        	
    
        
	@Override
	public SalesOrderDto getSalesOrderByReturnNo(String returnNo) {
		ReturnRecord rec = returnRepo.findByRecordNo(returnNo);
		if(rec != null)
			return new SalesOrderDto(returnRepo.findByRecordNo(returnNo).getOrderNo(), false);
		return null;
	}
	
	@Override
	@Transactional
	public void saveRefund(ReturnRecordDto refund) {
		ReturnRecord ret = returnRepo.findOne(refund.getId());
		ret.setRefundAmount(refund.getRefundAmount());
		returnRepo.save(ret);
	}
	
	@Override
	@Transactional(readOnly = true)
	public boolean isValidSalesOrder(String orderNo) {
		QSalesOrderAlloc qAlloc = QSalesOrderAlloc.salesOrderAlloc;
//		Predicate filter = qAlloc.gcInventory.status.ne(GiftCardInventoryStatus.ACTIVATED)
//				.or(qAlloc.gcInventory.redemptionDate.isNotNull());

		QGiftCardInventory qGc = QGiftCardInventory.giftCardInventory;
		return BooleanUtils.negate(
				new JPAQuery(em).from(qAlloc)
				.where(qAlloc.soItem.order.orderNo.eq(orderNo)
						//.and(filter)
						.and( new JPASubQuery().from( qGc ).where( qGc.series.goe( qAlloc.seriesStart ).and( qGc.series.loe( qAlloc.seriesEnd ) )
								.and( BooleanExpression.anyOf( qGc.status.ne(GiftCardInventoryStatus.ACTIVATED), qGc.redemptionDate.isNotNull() ) ) ).exists() ) )
				.exists());
	}

	
	@Override
	@Transactional(readOnly = true)
	public ReturnRecordDto getReturn(Long id) {
		return new ReturnRecordDto(returnRepo.findOne(id), false);
	}
	
	@Override
	@Transactional(readOnly = true)
	public ReturnRecordDto getReturnByRecordNo(String returnNo) {
		ReturnRecord record = returnRepo.findByRecordNo(returnNo);
		if(record == null)
			return null;
		return new ReturnRecordDto(returnRepo.findByRecordNo(returnNo), false);
	}

    @Override
    @Transactional
    public void saveReturn(ReturnRecordDto dto) {
        if (StringUtils.isBlank(dto.getRecordNo())) {
            dto.setRecordNo(generateReturnNo());
        }
        
        populateDetails(dto);
        ReturnRecord record = new ReturnRecord();
        if (dto.getId() != null) {
            record = returnRepo.findOne(dto.getId());
        }
        
        returnRepo.save(dto.toModel(record));
        this.processReturnRecord(dto);
        accountingService.forB2BReturn(dto.getOrderNo());
    }
	
	private void populateDetails(ReturnRecordDto dto) {
		QSalesOrderItem qItem = QSalesOrderItem.salesOrderItem;
		Tuple tuple = new JPAQuery(em)
			.from(qItem)
			.groupBy(qItem.order.id, qItem.order.orderNo, qItem.order.discountVal, qItem.order.customer.id)
			.having(qItem.order.orderNo.eq(dto.getOrderNo()))
			.singleResult(qItem.order.id, qItem.faceAmount.sum(), qItem.order.discountVal, qItem.order.customer.id);
		dto.setOrderId(tuple.get(qItem.order.id));
		dto.setFaceAmount(tuple.get(qItem.faceAmount.sum()));
		dto.setCustomerId(tuple.get(qItem.order.customer.id));
		BigDecimal disc = tuple.get(qItem.order.discountVal);
		if(disc == null)
			disc = BigDecimal.ZERO;
		dto.setReturnAmount(dto.getFaceAmount().subtract(dto.getFaceAmount().multiply(disc.divide(BigDecimal.TEN))));
	}
	
	private String generateReturnNo() {
		DateTimeFormatter formatter = DateTimeFormat.forPattern("yyyyMMddHHmmss");
		return "RR" + formatter.print(new DateTime());
		//RRYYYYMMDDHHMMSS
	}
	
	@Override
    @Transactional(readOnly = true)
    public ResultList<ReturnRecordDto> getReturns(ReturnSearchDto searchForm) {
		searchForm.setByCards(false);
		Pageable pageable = SpringDataPagingUtil.INSTANCE.createPageable(searchForm.getPagination());
        BooleanExpression filter = searchForm.createSearchExpression();
        Page<ReturnRecord> page = filter != null ? returnRepo.findAll(filter, pageable) : returnRepo.findAll(pageable);
        return new ResultList<ReturnRecordDto>(ReturnRecordDto.toDto(page.getContent()), page.getTotalElements(), page.hasPreviousPage(),
            page.hasNextPage());
		
    }
	
	
	@Override
	@Transactional
	public void updateReturn(String returnNo, ReturnStatus status) {
		ReturnRecord returnRec = returnRepo.findByRecordNo(returnNo);
		returnRec.setStatus(status);
	}
	
	@Override
	@Transactional
	public void approveReturn(Long id, ReturnStatus status) {
		ReturnRecord record = returnRepo.findOne(id);
		record.setStatus(status);
		returnRepo.save(record);
	}
	
    private void processReturnRecord(final ReturnRecordDto record) {
        QGiftCardInventory qgc = QGiftCardInventory.giftCardInventory;
        final Predicate inventoryFilter = createInventoryFilterFromRecord(record, qgc);
        long start = System.currentTimeMillis();
        long affectedRecords = updateInventory(inventoryFilter, qgc);
        logger.debug("{} total affected records upon B2B Activation and took {} ms", 
                affectedRecords, System.currentTimeMillis() - start);
        if (TransactionSynchronizationManager.isSynchronizationActive()) {
            TransactionSynchronizationManager.registerSynchronization(new TransactionSynchronizationAdapter() {

                @Override
                public void afterCommit() {
                    logger.debug("Transaction::afterCommit started.");
                    logger.debug("Invoking returnB2b...running in separated thread.");
                    SalesOrder so = salesOrderService.get(record.getOrderId());
                    giftCardManager.returnB2B(so);
                    logger.debug("Updating stocks...running in separated thread.");
                    saveStock(inventoryFilter);
                    logger.debug("Transaction::afterCommit executed.");
                }
            });
        }
    }
	
	private Predicate createInventoryFilterFromRecord(ReturnRecordDto record, QGiftCardInventory qInventory) {
		QSalesOrderAlloc qAlloc = QSalesOrderAlloc.salesOrderAlloc;
//		return qInventory.id.in(new JPASubQuery().from(qAlloc)
//                        .where(qAlloc.soItem.order.orderNo.eq(record.getOrderNo()))
//                        .list(qAlloc.gcInventory.id));

		JPQLQuery query = new JPAQuery( em ).from( qAlloc ).where( qAlloc.soItem.order.orderNo.eq( record.getOrderNo() )
				.and( BooleanExpression.allOf( qAlloc.seriesStart.isNotNull(), qAlloc.seriesEnd.isNotNull() ) ) );
		List<Tuple> tuples = query.list( qAlloc.seriesStart, qAlloc.seriesEnd );
		List<BooleanExpression> exprs = Lists.newArrayList();
		for ( Tuple tuple : tuples ) {
			exprs.add( qInventory.series.goe( tuple.get( qAlloc.seriesStart ) ).and( qInventory.series.loe( tuple.get( qAlloc.seriesEnd ) ) ) );
		}
		return BooleanExpression.anyOf( exprs.toArray( new BooleanExpression[ exprs.size() ] ) );
	}
	
        private long updateInventory(Predicate filter, QGiftCardInventory qgc) {
            return new JPAUpdateClause(em, qgc)
                    .set(qgc.status, GiftCardInventoryStatus.IN_STOCK)
                    .set(qgc.ryns, Boolean.FALSE)
                    .set(qgc.previousBalance, 0.0)
                    .setNull(qgc.balance)
                    .setNull(qgc.salesType)
                    .setNull(qgc.activationDate)
                    .setNull(qgc.expiryDate)
                    .set(qgc.location, codePropertiesService.getDetailInvLocationHeadOffice())
                    .where(filter).execute();
        }
	
	private void saveStock(Predicate filter) {
		List<GiftCardInventory> inventories = Lists.newArrayList(inventoryRepo.findAll(filter));
		stockService.saveStocks(inventories, GCIStockStatus.RETURNED);
	}
	
	
	@Override
	@Transactional
	public void deleteReturn(Long id) {
		returnRepo.delete(id);
	}
}
