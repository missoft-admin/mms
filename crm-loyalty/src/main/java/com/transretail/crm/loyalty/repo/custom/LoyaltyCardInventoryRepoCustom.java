package com.transretail.crm.loyalty.repo.custom;



import java.util.List;
import java.util.Map;

import org.joda.time.LocalDate;


import com.transretail.crm.core.entity.MemberModel;
import com.transretail.crm.core.entity.lookup.LookupDetail;
import com.transretail.crm.loyalty.dto.AllocateLoyaltyCardsDto;
import com.transretail.crm.loyalty.dto.AllocationDto;
import com.transretail.crm.loyalty.dto.LoyaltyCardInventorySearchDto;
import com.transretail.crm.loyalty.dto.LoyaltyCardInventorySummaryResultList;
import com.transretail.crm.loyalty.dto.ManufactureOrderReceiveDto;
import com.transretail.crm.loyalty.dto.ReceiveOrderDto;
import com.transretail.crm.loyalty.entity.LoyaltyCardInventory;

public interface LoyaltyCardInventoryRepoCustom {

	LoyaltyCardInventorySummaryResultList searchLoyaltyCardInventories(LoyaltyCardInventorySearchDto searchDto);

	long receiveOrders(ReceiveOrderDto orderDto);

	long saveInventory(AllocateLoyaltyCardsDto dto, LookupDetail newStatus,
			LookupDetail oldStatus);

	AllocateLoyaltyCardsDto getInventory(String referenceNo, LookupDetail status);

	long allocateInventory(AllocateLoyaltyCardsDto dto, LookupDetail newStatus,
			LookupDetail oldStatus);

	List<LookupDetail> getCardTypes(String startingSeries, String endingSeries);

	long saveInventory(AllocationDto dto, LookupDetail status);

	Object[] getInventoriesForApproval(String location);

	Object[] getInventories(LookupDetail cardType,
			LookupDetail status, String location, String transferTo, String newTransferTo);

	long saveInventory(String referenceNo, LookupDetail newStatus);

	List<ManufactureOrderReceiveDto> getReceivedInventories(
			Long manufactureOrderId);

	Long countReceivableInvetories(Long manufactureOrderId);
	
	long expireInventories(LocalDate dateToExpire);

	Map<String, List<String>> getEmailsWithExpiry(LocalDate dateToExpire);

	List<MemberModel> getMembersWithExpiry(LocalDate dateToExpire);

	long sendAllocateToChangeForApproval(AllocationDto dto);

	long approveAllocateToChange(String referenceNo);

	long rejectAllocateToChange(String referenceNo);

	long saveForBurning(AllocationDto dto, Boolean forBurning);

	long approveBurnInventory(String referenceNo, Boolean approved);

	Object[] getInventoriesForBurning();

	List<String> getOrginalLocation(AllocationDto dto);
}
