package com.transretail.crm.report.template.impl;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.apache.commons.lang3.StringUtils;
import org.joda.time.LocalDateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.MessageSource;
import org.springframework.context.MessageSourceAware;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.context.support.MessageSourceAccessor;
import org.springframework.stereotype.Service;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.google.common.collect.Sets;
import com.mysema.query.BooleanBuilder;
import com.mysema.query.jpa.impl.JPAQuery;
import com.transretail.crm.common.reporting.jasper.ClasspathFileResolver;
import com.transretail.crm.common.reporting.jasper.DefaultJRProcessor;
import com.transretail.crm.common.reporting.jasper.JRProcessor;
import com.transretail.crm.common.util.DateUtil;
import com.transretail.crm.core.entity.enums.ReportType;
import com.transretail.crm.giftcard.entity.QGiftCardInventory;
import com.transretail.crm.giftcard.entity.QGiftCardTransactionItem;
import com.transretail.crm.giftcard.entity.QProductProfile;
import com.transretail.crm.giftcard.entity.support.GiftCardSaleTransaction;
import com.transretail.crm.giftcard.entity.support.GiftCardSalesType;
import com.transretail.crm.report.template.CommonReportFilter;
import com.transretail.crm.report.template.FilterField;
import com.transretail.crm.report.template.ReportTemplate;
import com.transretail.crm.report.template.ReportsCustomizer;

@Service("giftCardAvgTxnReport")
public class GiftCardAvgTxnReport implements ReportTemplate, ReportsCustomizer, MessageSourceAware {
	protected static final String TEMPLATE_NAME = "Gift Card Average Transaction Amount Report";
    protected static final String REPORT_NAME = "reports/gift_card_avg_txn_amt.jasper";
    protected static final String FILTER_DATE_TO = CommonReportFilter.DATE_TO;
    protected static final String FILTER_DATE_FROM = CommonReportFilter.DATE_FROM;
    
    private static final Logger LOGGER = LoggerFactory.getLogger(GiftCardAvgTxnReport.class);
    private static final DateTimeFormatter INPUT_DATE_PATTERN = DateTimeFormat.forPattern(DateUtil.SEARCH_DATE_FORMAT);
    
    
    @PersistenceContext
    private EntityManager em;
    private MessageSourceAccessor messageSource;
    @Override
    public void setMessageSource(MessageSource messageSource) {
    	this.messageSource = new MessageSourceAccessor(messageSource);
    }
    
    @Override
    public ReportType[] getReportTypes() {
    	return ReportType.values();
    }
    
    @Override
    public String getName() {
    	return TEMPLATE_NAME;
    }

    @Override
    public String getDescription() {
    	return messageSource.getMessage("gift.card.averagetxn.report.title", (Object[]) null, LocaleContextHolder.getLocale());
    }
    
    @Override
    public boolean canView(Set<String> permissions) {
    	return true;
    }
    
    @Override
	public Set<Option> customizerOption() {
    	return Sets.newHashSet(ReportsCustomizer.Option.DATE_RANGE_FULL_VALIDATION);
	}
    
    @Override
    public Set<FilterField> getFilters() {
    	
    	
		Set<FilterField> filters = Sets.newLinkedHashSet();
		filters.add(FilterField.createDateField(FILTER_DATE_FROM,
				messageSource.getMessage("label_date_from"),
				DateUtil.SEARCH_DATE_FORMAT, true));
		filters.add(FilterField.createDateField(FILTER_DATE_TO,
				messageSource.getMessage("label_date_to"),
				DateUtil.SEARCH_DATE_FORMAT, true));
		
		Map<String, String> products = new LinkedHashMap<String, String>();
		products.put("", "");
		for(Entry<String, String> e: getProducts().entrySet()) {
			products.put(e.getKey(), e.getValue());
		}
		/*products.putAll(getProducts());*/
		filters.add(FilterField.createDropdownField("product",
                messageSource.getMessage("gift.card.averagetxn.report.product", (Object[]) null, LocaleContextHolder.getLocale()), products));
		
		Map<String, String> salesTypes = new LinkedHashMap<String, String>();
		salesTypes.put("", "");
		for (GiftCardSalesType key : GiftCardSalesType.values()) {
			salesTypes.put(key.toString(), key.toString());
		}
		filters.add(FilterField.createDropdownField("salesType",
                messageSource.getMessage("gift.card.averagetxn.report.salestype", (Object[]) null, LocaleContextHolder.getLocale()), salesTypes));
		
		return filters;
    }
    private Map<String, String> getProducts() {
    	QProductProfile qPrd = QProductProfile.productProfile;
    	return new JPAQuery(em).from(qPrd).orderBy(qPrd.productDesc.asc()).map(qPrd.id.stringValue(), qPrd.productDesc);
    }
    @Override
    public JRProcessor createJrProcessor(Map<String, String> map) {
    	LocalDateTime startDate = INPUT_DATE_PATTERN.parseLocalDate(map.get(FILTER_DATE_FROM)).toDateTimeAtStartOfDay().toLocalDateTime();
    	LocalDateTime endDate = INPUT_DATE_PATTERN.parseLocalDate(map.get(FILTER_DATE_TO)).plusDays(1).toDateTimeAtStartOfDay().minusMillis(1).toLocalDateTime();
		GiftCardSalesType salesType = StringUtils.isNotBlank(map.get("salesType")) ? GiftCardSalesType.valueOf(map.get("salesType")) : null;
		Long productId = StringUtils.isNotBlank(map.get("product")) ? Long.valueOf(map.get("product")) : null;
		DefaultJRProcessor jrProcessor = new DefaultJRProcessor(REPORT_NAME, ImmutableList.copyOf(createDataSource(startDate, endDate, salesType, productId)));
		Map<String, Object> paramMap = prepareReportParameters(startDate, endDate, salesType);
		jrProcessor.addParameters(paramMap);
		jrProcessor.setFileResolver(new ClasspathFileResolver("reports"));
		return jrProcessor;
    }
    
    public List<ReportBean> createDataSource(LocalDateTime startDate, LocalDateTime endDate, GiftCardSalesType salesType, Long product) {
    	List<ReportBean> beans = Lists.newArrayList();
    	for(Entry<String, String> en: getProducts(startDate, endDate, product).entrySet()) {
    		String prodCode = en.getKey();
    		String prodDesc = en.getValue();
    		double activateSum = getSumFromTxn(GiftCardSaleTransaction.ACTIVATION, prodCode, startDate, endDate, salesType);
    		activateSum -= getSumFromTxn(GiftCardSaleTransaction.VOID_ACTIVATED, prodCode, startDate, endDate, salesType);
    		double redeemSum = getSumFromTxn(GiftCardSaleTransaction.REDEMPTION, prodCode, startDate, endDate, salesType);
    		redeemSum -= getSumFromTxn(GiftCardSaleTransaction.VOID_REDEMPTION, prodCode, startDate, endDate, salesType);
    		
    		long activateCount = getCount(GiftCardSaleTransaction.ACTIVATION, prodCode, startDate, endDate, salesType);
    		long redeemCount = getCountFromTxn(GiftCardSaleTransaction.REDEMPTION, prodCode, startDate, endDate, salesType);
    		beans.add(new ReportBean(prodCode, prodDesc, activateSum, redeemSum, new Double(0), activateCount, redeemCount));
    	}
    	return beans;
    }
    
    
    private Map<String, String> getProducts(LocalDateTime start, LocalDateTime end, Long productId) {
    	QGiftCardTransactionItem qTx = QGiftCardTransactionItem.giftCardTransactionItem;
    	BooleanBuilder filter = new BooleanBuilder(qTx.transaction.transactionDate.between(start, end));
    	if(productId != null)
    		filter.and(qTx.giftCard.profile.id.eq(productId));
    	return  new JPAQuery(em).from(qTx)
    			.where(filter)
    			.groupBy(qTx.giftCard.profile.productCode, qTx.giftCard.profile.productDesc)
    			.map(qTx.giftCard.profile.productCode, qTx.giftCard.profile.productDesc);
    }
    
    private double getSum(GiftCardSaleTransaction txnType, String prodCode, LocalDateTime startDate, LocalDateTime endDate, GiftCardSalesType salesType) { // applies to ACTIVATION only
    	QGiftCardInventory qInventory = QGiftCardInventory.giftCardInventory;
    	BooleanBuilder dateFilter = new BooleanBuilder().and(qInventory.activationDate.between(startDate.toLocalDate(), endDate.toLocalDate()))
    	.and(qInventory.profile.productCode.eq(prodCode));
    	if(salesType != null)
    		dateFilter.and(qInventory.salesType.eq(salesType));
    	BigDecimal ret = new JPAQuery(em).from(qInventory)
    			.where(dateFilter).singleResult(qInventory.faceValue.sum());
    	return ret != null ? ret.doubleValue() : 0; 
    }
    
    private long getCount(GiftCardSaleTransaction txnType, String prodCode, LocalDateTime startDate, LocalDateTime endDate, GiftCardSalesType salesType) { // applies to ACTIVATION only
    	QGiftCardInventory qInventory = QGiftCardInventory.giftCardInventory;
    	BooleanBuilder dateFilter = new BooleanBuilder().and(qInventory.activationDate.between(startDate.toLocalDate(), endDate.toLocalDate())) 
    	    	.and(qInventory.profile.productCode.eq(prodCode));
    	if(salesType != null)
    		dateFilter.and(qInventory.salesType.eq(salesType));
    	Long ret = new JPAQuery(em).from(qInventory)
    			.where(dateFilter).count();
    	return ret != null ? ret.longValue() : 0; 
    }

    
    private double getSumFromTxn(GiftCardSaleTransaction txnType, String prodCode, LocalDateTime startDate, LocalDateTime endDate, GiftCardSalesType salesType) {
    	QGiftCardTransactionItem transactionItem = QGiftCardTransactionItem.giftCardTransactionItem;
    	BooleanBuilder filter = new BooleanBuilder().and(transactionItem.transaction.transactionDate.between(startDate, endDate)) 
    			.and(transactionItem.giftCard.profile.productCode.eq(prodCode))
    			
    			.and(transactionItem.transaction.transactionType.eq(txnType));
    	if(salesType != null)
    		filter.and(transactionItem.giftCard.salesType.eq(salesType));
    	Double ret = new JPAQuery(em).from(transactionItem)
				.where(filter)
				.singleResult(transactionItem.transactionAmount.sum());
    	return ret != null ? ret.doubleValue() : 0; 
    }
    
    private long getCountFromTxn(GiftCardSaleTransaction txnType, String prodCode, LocalDateTime startDate, LocalDateTime endDate, GiftCardSalesType salesType) {
    	QGiftCardTransactionItem transactionItem = QGiftCardTransactionItem.giftCardTransactionItem;
    	BooleanBuilder filter = new BooleanBuilder().and(transactionItem.transaction.transactionDate.between(startDate, endDate)) 
    			.and(transactionItem.giftCard.profile.productCode.eq(prodCode))
    			.and(transactionItem.transaction.transactionType.eq(txnType));
    	if(salesType != null)
    		filter.and(transactionItem.giftCard.salesType.eq(salesType));
    	Long ret = new JPAQuery(em).from(transactionItem)
				.where(filter)
				.count();
    	return ret != null ? ret.longValue() : 0; 
    }
    
    private Map<String, Object> prepareReportParameters(LocalDateTime start, LocalDateTime end, GiftCardSalesType salesType) {
    	HashMap<String, Object> reportMap = Maps.newHashMap();
    	reportMap.put("START_DATE", start);
    	reportMap.put("END_DATE", end);
    	reportMap.put("SALES_TYPE", salesType == null ? "" : salesType.toString());
    	reportMap.put("NO_DATA", messageSource.getMessage("report.no.data", (Object[]) null, 
				LocaleContextHolder.getLocale()));
    	return reportMap;
    }
    
    public static class ReportBean {
    	private String genCode;
    	private String product;
    	private Double actSum;
    	private Double redSum;
    	private Double handlingFee;
    	private Long actCount;
    	private Long redCount;
    	public ReportBean() {}
    	
		public ReportBean(String genCode, String product, Double actSum,
				Double redSum, Double handlingFee, Long actCount, Long redCount) {
			super();
			this.genCode = genCode;
			this.product = product;
			this.actSum = actSum;
			this.redSum = redSum;
			this.handlingFee = handlingFee;
			this.actCount = actCount;
			this.redCount = redCount;
		}

		public String getGenCode() {
			return genCode;
		}
		public void setGenCode(String genCode) {
			this.genCode = genCode;
		}
		public String getProduct() {
			return product;
		}
		public void setProduct(String product) {
			this.product = product;
		}
		
		public Double getActSum() {
			return actSum;
		}
		public void setActSum(Double actSum) {
			this.actSum = actSum;
		}
		public Double getRedSum() {
			return redSum;
		}
		public void setRedSum(Double redSum) {
			this.redSum = redSum;
		}
		public Double getHandlingFee() {
			return handlingFee;
		}
		public void setHandlingFee(Double handlingFee) {
			this.handlingFee = handlingFee;
		}
		public Long getActCount() {
			return actCount;
		}
		public void setActCount(Long actCount) {
			this.actCount = actCount;
		}
		public Long getRedCount() {
			return redCount;
		}
		public void setRedCount(Long redCount) {
			this.redCount = redCount;
		}
		public Long getTransactionCount() {
			long txnCount = 0; 
			if(actCount != null)
				txnCount += actCount.longValue();
			if(redCount != null)
				txnCount += redCount.longValue();
			return txnCount;
		}
    }
    
}
