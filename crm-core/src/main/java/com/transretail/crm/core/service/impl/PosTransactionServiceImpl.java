package com.transretail.crm.core.service.impl;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import org.joda.time.format.DateTimeFormat;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.transretail.crm.common.reporting.jasper.ClasspathFileResolver;
import com.transretail.crm.common.reporting.jasper.DefaultJRProcessor;
import com.transretail.crm.common.reporting.jasper.JRProcessor;
import com.transretail.crm.common.service.dto.request.PageSortDto;
import com.transretail.crm.common.service.dto.request.PagingParam;
import com.transretail.crm.common.service.dto.response.ResultList;
import com.transretail.crm.common.service.util.SpringDataPagingUtil;
import com.transretail.crm.core.dto.PosTxItemDto;
import com.transretail.crm.core.dto.PosTxItemResultList;
import com.transretail.crm.core.dto.PosTxItemSearchDto;
import com.transretail.crm.core.dto.ShoppingListDto;
import com.transretail.crm.core.entity.ApplicationConfig;
import com.transretail.crm.core.entity.MemberModel;
import com.transretail.crm.core.entity.PosTransaction;
import com.transretail.crm.core.entity.PosTxItem;
import com.transretail.crm.core.entity.enums.AppConfigDefaults;
import com.transretail.crm.core.entity.enums.AppKey;
import com.transretail.crm.core.entity.enums.SalesType;
import com.transretail.crm.core.entity.lookup.Product;
import com.transretail.crm.core.repo.ApplicationConfigRepo;
import com.transretail.crm.core.repo.PosTransactionRepo;
import com.transretail.crm.core.repo.PosTxItemRepo;
import com.transretail.crm.core.repo.ProductRepo;
import com.transretail.crm.core.service.PosTransactionService;

@Service("posTransactionService")
@Transactional
public class PosTransactionServiceImpl implements PosTransactionService {

    @Autowired
    private PosTransactionRepo posTransactionRepo;
    @Autowired
    private ProductRepo productRepo;
    @Autowired
    private PosTxItemRepo posTxItemRepo;
    @Autowired
    private ApplicationConfigRepo appConfigRepo;
    @Autowired
    private MessageSource messageSource;
    
    @Override
    public PosTransaction getTransaction(String id) {
    	return posTransactionRepo.findOne(id);
    }

    @Override
    public PosTransaction getTransactionWithSoldItems(String id) {
        PosTransaction posTransaction = posTransactionRepo.findOne(id);

        PosTransaction retTransaction = new PosTransaction();
        BeanUtils.copyProperties(retTransaction, posTransaction, new String[]{"id", "orderItems"});

        List<PosTxItem> items = posTransaction.getOrderItems();
        List<PosTxItem> soldItems = new ArrayList<PosTxItem>();

        for (PosTxItem item : items) {
            if (item.getSalesType().equals(SalesType.SALE.toString()))
                soldItems.add(item);
        }

        retTransaction.setOrderItems(soldItems);

        return retTransaction;
    }

    @Override
    public List<Product> getSoldProductsFromTransaction(String id) {
        PosTransaction posTransaction = posTransactionRepo.findOne(id);

        List<Product> products = new ArrayList<Product>();

        for (PosTxItem item : posTransaction.getOrderItems()) {
            if (item.getSalesType().equals(SalesType.SALE.toString()))
                products.add(productRepo.findOne(item.getProductId()));
        }

        return products;
    }

    @Override
    @Transactional(readOnly = true)
    public PosTxItemResultList getPosTxItems(PosTxItemSearchDto dto) {
        Pageable pageable = SpringDataPagingUtil.INSTANCE.createPageable(dto.getPagination());
        Page<PosTxItem> page = posTxItemRepo.findAll(dto.createSearchExpression(), pageable);
        List<PosTxItemDto> dtos = Lists.newArrayList();
        for (PosTxItem posTxItem : page.getContent()) {
            dtos.add(new PosTxItemDto(posTxItem, productRepo.findOne(posTxItem.getProductId())));
        }
        return new PosTxItemResultList(dtos, page.getTotalElements(), page.hasPreviousPage(), page.hasNextPage());
    }
    
    @Override
    public Double getAverageQuantityPerTransaction(String accountId, String productId) {
    	return posTransactionRepo.getAverageTransactions(accountId, productId, 
    			getShoppingListInclusiveTimeFrame(), getShoppingListExclusiveTimeFrame());
    }
    
    @Override
    public ResultList<ShoppingListDto> getProductRankingsByTransaction(String accountId) {
    	List<ShoppingListDto> list = posTransactionRepo.rankItemsByTransactions(accountId, getShoppingListInclusiveTimeFrame(), 
    			getShoppingListExclusiveTimeFrame(), getShoppingListMinTransactions());
    	return new ResultList<ShoppingListDto>(list, list.size(), false, false);
    }
    
    @Override
    public ResultList<ShoppingListDto> getProductRankingsByTransaction(String accountId, PageSortDto pageSort) {
    	PagingParam pagination = pageSort.getPagination();
    	List<ShoppingListDto> list = posTransactionRepo.rankItemsByTransactions(accountId, getShoppingListInclusiveTimeFrame(), 
    			getShoppingListExclusiveTimeFrame(), getShoppingListMinTransactions(), pagination.getPageSize(), pagination.getPageNo());
    	return new ResultList<ShoppingListDto>(list, list.size(), pagination.getPageNo(), pagination.getPageSize());
    }
    
    @Override
    public Map<String, ShoppingListDto> getShoppingSuggestions(String accountId) {
    	return posTransactionRepo.getShoppingSuggestions(accountId, getShoppingListInclusiveTimeFrame(), 
    			getShoppingListExclusiveTimeFrame(), getShoppingListMinTransactions());
    }
    
    @Override
    public void setShoppingListInclusiveTimeFrame(Integer timeInMonths) {
    	ApplicationConfig time = appConfigRepo.findByKey(AppKey.SHOPPING_LIST_INCLUSIVE_TIME);
    	if(time == null) {
    		time = newAppConfig(AppKey.SHOPPING_LIST_INCLUSIVE_TIME);
    	}
    	time.setValue(timeInMonths.toString());
    	appConfigRepo.save(time);
    }
    
    @Override
    public Integer getShoppingListInclusiveTimeFrame() {
    	ApplicationConfig time = appConfigRepo.findByKey(AppKey.SHOPPING_LIST_INCLUSIVE_TIME);
    	if(time == null) {
    		time = newAppConfig(AppKey.SHOPPING_LIST_INCLUSIVE_TIME);
    		time.setValue(AppConfigDefaults.DEFAULT_SHOPPING_LIST_INCLUSIVE_TIME);
    		appConfigRepo.save(time);
    		// default value is 6 days
    		return 6;
    	}
    	return Integer.parseInt(time.getValue());
    }
    
    @Override
    public void setShoppingListExclusiveTimeFrame(Integer timeInDays) {
    	ApplicationConfig time = appConfigRepo.findByKey(AppKey.SHOPPING_LIST_EXCLUSIVE_TIME);
    	if(time == null) {
    		time = newAppConfig(AppKey.SHOPPING_LIST_EXCLUSIVE_TIME);
    	}
    	time.setValue(timeInDays.toString());
    	appConfigRepo.save(time);
    }
    
    @Override
    public Integer getShoppingListExclusiveTimeFrame() {
    	ApplicationConfig time = appConfigRepo.findByKey(AppKey.SHOPPING_LIST_EXCLUSIVE_TIME);
    	if(time == null) {
    		time = newAppConfig(AppKey.SHOPPING_LIST_EXCLUSIVE_TIME);
    		time.setValue(AppConfigDefaults.DEFAULT_SHOPPING_LIST_EXCLUSIVE_TIME);
    		appConfigRepo.save(time);
    		// default value is 7 days
    		return 7;
    	}
    	return Integer.parseInt(time.getValue());
    }
    
    @Override
    public void setShoppingListMinTransactions(Integer minTransactions) {
    	ApplicationConfig minTxn = appConfigRepo.findByKey(AppKey.SHOPPING_LIST_MIN_TXN);
    	if(minTxn == null) {
    		minTxn = newAppConfig(AppKey.SHOPPING_LIST_MIN_TXN);
    	}
    	minTxn.setValue(minTransactions.toString());
    	appConfigRepo.save(minTxn);
    }
    
    @Override
    public Integer getShoppingListMinTransactions() {
    	ApplicationConfig minTxn = appConfigRepo.findByKey(AppKey.SHOPPING_LIST_MIN_TXN);
    	if(minTxn == null) {
    		minTxn = newAppConfig(AppKey.SHOPPING_LIST_MIN_TXN);
    		minTxn.setValue(AppConfigDefaults.DEFAULT_SHOPPING_LIST_MIN_TXN);
    		appConfigRepo.save(minTxn);
    		return 2;
    	}
    	
    	return Integer.parseInt(minTxn.getValue());
    }
    
    public JRProcessor createJRProcessor(MemberModel member, InputStream logo) {
    	Map<String, Object> parameters = Maps.newHashMap();
		Locale locale = LocaleContextHolder.getLocale();
		
		List<ShoppingListDto> shoppingList = posTransactionRepo.rankItemsByTransactions(member.getAccountId(), 
				getShoppingListInclusiveTimeFrame(), getShoppingListExclusiveTimeFrame(), 
				getShoppingListMinTransactions());
		
		parameters.put("SUB_DATA_SOURCE", shoppingList);
		parameters.put("logo", logo);
		parameters.put("PRODUCT_ID", messageSource.getMessage("product.item.id", null, locale));
		parameters.put("PRODUCT_NAME", messageSource.getMessage("product.item.name", null, locale));
		parameters.put("PRODUCT_AVERAGE", messageSource.getMessage("product.average.bought", null, locale));
		parameters.put("FORMATTED_NAME", member.getFormattedMemberName());
		parameters.put("MEMBER_ACCT_ID", member.getAccountId());
		parameters.put("MEMBER_LABEL", messageSource.getMessage("member.label", null, locale));
		parameters.put("REPORT_TYPE", messageSource.getMessage("product.report.label", null, locale));
		
		DefaultJRProcessor jrProcessor = new DefaultJRProcessor("reports/shopping_list_report.jasper");
		jrProcessor.setParameters(parameters);
		jrProcessor.setFileResolver(new ClasspathFileResolver("reports"));
		return jrProcessor;
    }
    
    private ApplicationConfig newAppConfig(AppKey key) {
    	ApplicationConfig appConfig = new ApplicationConfig();
    	appConfig.setKey(key);
    	return appConfig;
    }
}
