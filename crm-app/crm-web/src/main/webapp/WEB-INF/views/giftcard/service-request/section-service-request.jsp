<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@include file="../../common/taglibs.jsp" %>
<div id="section-file-new-request" class="hide">
  <form:form id="giftCardServiceRequestForm" name="giftCardServiceRequestForm" modelAttribute="giftCardServiceRequestForm" 
	     action="/giftcard/service-request/file">
    <fieldset>
      <legend><spring:message code="gc.service.request.page.section.service.request"/></legend>
      <div id="content-error" class="hide alert alert-error">
	<button type="button" class="close" onclick="$(this).parent().hide();">&times;</button>
	<div><form:errors path="*"/></div>
      </div>
      <div id="content-success" class="hide alert alert-success">
	<button type="button" class="close" onclick="$(this).parent().hide();">&times;</button>
	<div></div>
      </div>
      <div class="form-group form-horizontal">
	<div class="control-group">
	  <form:label path="filedIn" name="detailsLabel" cssClass="control-label">
	    <spring:message code="gc.service.request.fieldin.store"/>
	  </form:label>
	  <div class="controls">
	    <form:select path="filedIn" items="${stores}" itemValue="code" itemLabel="codeAndName"/>
	  </div>	   
	</div>
	<div class="control-group">
	  <form:label path="details" name="detailsLabel" cssClass="control-label">
	    <spring:message code="gc.service.request.field.new.service.request"/>
	  </form:label>
	  <div class="controls">
	    <form:textarea path="details" name="details" cssClass="input-large"  maxlength="500"/>
	  </div>
	  <input type="hidden" name="giftcardNo"/>
	  <br />
	  <form:button name="fileServiceRequestBtn" class="btn btn-primary controls">
	    <spring:message code="gc.service.request.button.file.new" />
	  </form:button>
	</div>
      </div>
    </fieldset>
  </form:form>
</div>
<div class="clearfix"></div>
<div id="section-file-new-request-details" class="hide">
  <div class="accordion-group" id="show-table">
    <div class="accordion-heading">
      <a class="accordion-toggle" data-toggle="collapse" data-target="#accor-service-req-details">
	<div class="header" >
	  <h5>Show Details</h5>
	</div>
      </a>
    </div>
    <div id="accor-service-req-details" class="collapse">
      <div class="accordion-inner">
	<div id="content_giftcard_service_request_list">
	  <div id="list_service_request" 
	       data-column-date-filed="<spring:message code='gc.service.request.table.column.date.filed' />"
	       data-column-filed-by="<spring:message code='gc.service.request.table.column.filed.by' />"
	       data-column-filed-in="<spring:message code='gc.service.request.table.column.filed.in' />"
	       data-column-details="<spring:message code='gc.service.request.table.column.details' />">
	  </div>
	</div>
      </div>
    </div>
  </div>
</div>