// TODO: Invoke bindCheckboxes in corresponding pages that needs it.
// It's not a good practice to make application wide events unless we can't avoid it (eg. client requirement)
// because it's hard to debug.
$(document).ready(function() {

    $(".datepicker-generic").datepicker({
        changeMonth: true,
        changeYear: true,
        prevText: "&#9664;",
        nextText: "&#9654;",
        dateFormat: "dd-mm-yy"
    });
});

var NAME_PREFIX = "hidden_";

function bindCheckboxes(inName) {
	var theSelector;
	if (inName == null) {
		theSelector = "input[type=checkbox]:not(.manual)";
	} else {
		theSelector = "input:checkbox[name='" + inName + "']";
	}

	$(theSelector).each(function() {
		var theHiddenCheck = $("<input type='hidden' />").insertAfter($(this));

		var theValue = $.trim($(this).val()).length != 0 ? $(this).val() : "";
		if( theValue == "on" || theValue == "true" || theValue == "flase"
			|| theValue == "" || theValue == undefined ) {
			theValue = $(this).is(':checked');
		}
		$(theHiddenCheck).val(theValue);

		$(theHiddenCheck).attr("id", NAME_PREFIX + ($(this).attr("id") != undefined? $(this).attr("id") : ""));
		$(theHiddenCheck).attr("name", $(this).attr("name"));
		$(this).attr("name", NAME_PREFIX + $(this).attr("name"));

		$(this).change(function() {
			var theValue = $(this).is(':checked') ? $(this).val() : "";
			if( $(theHiddenCheck).val() == 'true' || $(theHiddenCheck).val() == 'false' ) {
				theValue = $(this).is(':checked');
			}
			$(theHiddenCheck).val(theValue);
		});
	});

}

function getBoundCheckboxes(inName) {
	var theValues = [];
	$("input:checkbox[name='" + NAME_PREFIX + inName + "']").each(function() {
		if (!$(this).is(':checked')) {
			return;
		}
		theValues.push($(this).val());
	});
	return theValues;
}

function doCheck( inId, inCheckboxName ) {

	$( "#" + inId ).change( function() {
    	if ( $(this).is(':checked') ) {
        	doCheckAll( inCheckboxName, true );
    	}
    	else {
    		doCheckAll( inCheckboxName, false );
    	}
    });
}

function doCheckAll( inName, doCheck ) {
	var theCheckBoxes = $("input:checkbox[name='" + NAME_PREFIX + inName + "']");
	$(theCheckBoxes).each(function() {
		this.checked = doCheck;
	});
}

function toggleContent(elemToToggle) {
	elemToToggle.style.display == "block" ? elemToToggle.style.display = "none"
			: elemToToggle.style.display = "block";
}

function displayContent(elemToToggle, display) {
	display ? $(elemToToggle).show() : $(elemToToggle).hide();
}


function allowNumberOnly( inId ) {
	$('#' + inId ).keyup(function(e) {
	    if (/\D/g.test(this.value) )
	    {
	        this.value = this.value.replace(/\D/g, '');
	    }
	});
}


/*function allowNumberOnly( inId ) {
	$('#' + inId ).keyup(function(e) {
		if ( this.value != '-' && this.value + 0 == NaN ) {
			this.value = this.value.replace( this.value.charAt( this.value.length ) - 1, '' );
		}
	});
}*/



/*jQuery functions*/
jQuery.fn.addHidden = function (name, value) {
    return this.each(function () {
        var input = $("<input>").attr("type", "hidden").attr("name", name).val(value);
        $(this).append($(input));
    });
};

Object.prototype.hasOwnProperty = function(property) {
    return typeof this[property] !== 'undefined';
};

Element.prototype.hasClassName = function(name) {
    return new RegExp("(?:^|\\s+)" + name + "(?:\\s+|$)").test(this.className);
};

Element.prototype.addClassName = function(name) {
    if (!this.hasClassName(name)) {
        this.className = this.className ? [this.className, name].join(' ') : name;
    }
};

Element.prototype.removeClassName = function(name) {
    if (this.hasClassName(name)) {
        var c = this.className;
        this.className = c.replace(new RegExp("(?:^|\\s+)" + name + "(?:\\s+|$)", "g"), "");
    }
};

function centerModal() {
    $(this).css('display', 'block');
    var $dialog = $(this).find(".modal-dialog");
    var offset = ($(document).height() - $dialog.height()) / 2;
    // Center modal vertically in window
    $dialog.css("margin-top", offset);
}