alter table CRM_APPROVAL_REMARK modify REMARKS varchar2(1000 char);
alter table CRM_CARD_HISTORY modify REASON varchar2(1000 char);
alter table CRM_COMPANY_PROFILE modify COPYRIGHT_TXT varchar2(1000 char);
alter table CRM_EMPLOYEE_PURCHASE_TXN modify ( REASON varchar2(1000 char), REMARKS varchar2(1000 char) );
alter table CRM_EMPLOYEE_VOUCHER_TXN modify ( COMMENTS varchar2(1000 char) );
alter table CRM_MO modify REASON varchar2(1000 char );
alter table CRM_POINTS modify ( REASON varchar2(1000 char), COMMENTS varchar2(1000 char) );
alter table CRM_PROMOTION_REWARD modify REMARK varchar2(1000 char);
alter table CRM_STOCK_REQUEST modify ( REASON_DISAPPROVED varchar2(1000 char), REASON_REJECTED varchar2(1000 char) );