package com.transretail.crm.marketing.service.impl;

import java.math.BigDecimal;
import java.math.MathContext;
import java.sql.Timestamp;
import java.util.List;

import org.hibernate.ScrollMode;
import org.hibernate.ScrollableResults;
import org.hibernate.StatelessSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.google.common.collect.Lists;
import com.mysema.query.jpa.hibernate.sql.HibernateSQLQuery;
import com.mysema.query.sql.ColumnMetadata;
import com.mysema.query.sql.DatePart;
import com.mysema.query.sql.SQLExpressions;
import com.mysema.query.sql.SQLSubQuery;
import com.mysema.query.support.Expressions;
import com.mysema.query.types.ConstructorExpression;
import com.mysema.query.types.expr.BooleanExpression;
import com.mysema.query.types.expr.DateTimeExpression;
import com.mysema.query.types.path.DateTimePath;
import com.mysema.query.types.path.NumberPath;
import com.transretail.crm.core.jpa.DbMetadataUtil;
import com.transretail.crm.generated.sources.SCrmPoints;
import com.transretail.crm.marketing.dto.DecileQueryResultDto;
import com.transretail.crm.marketing.dto.DecileReportDto;
import com.transretail.crm.marketing.dto.YearMonthDto;
import com.transretail.crm.marketing.service.DecileAnalysisService;

/**
 * @author Allan G. Ramirez (agramirez@exist.com)
 */
@Service
public class DecileAnalysisServiceImpl implements DecileAnalysisService {
    private static final MathContext ROUNDING_CONTEXT = MathContext.DECIMAL32;
    private static final int TEN = 10;
    @Autowired
    private StatelessSession statelessSession;
    @Autowired
    private DbMetadataUtil dbMetadataUtil;

    @Transactional(readOnly = true)
    @Override
    public List<DecileReportDto> getDecileReport(YearMonthDto yearMonthDto) {
        List<DecileReportDto> results = Lists.newArrayList();
        Long total = getTotalMembers(yearMonthDto);
        if (total != null && total > 0) {
            int[] decileSizes = getMemberDecileSizes(total);
            results = getDecileReportDto(decileSizes, yearMonthDto);
        }
        return results;
    }

    private Long getTotalMembers(final YearMonthDto yearMonthDto) {
        SCrmPoints sCrmPoints = SCrmPoints.crmPoints;
        return createHibernateSQLQuery(sCrmPoints, yearMonthDto).singleResult(sCrmPoints.memberId.countDistinct());
    }

    private List<DecileReportDto> getDecileReportDto(final int[] decileSizes, final YearMonthDto yearMonthDto) {
        List<DecileReportDto> results = Lists.newArrayList();

        SCrmPoints sCrmPoints1 = new SCrmPoints("crmPoints1");
        NumberPath<Long> memberIdPath =
            Expressions.numberPath(Long.class, ColumnMetadata.getColumnMetadata(sCrmPoints1.memberId).getName());
        SCrmPoints sCrmPoints2 = new SCrmPoints("crmPoints2");

        DateTimeExpression<Timestamp> sTxnMonth = SQLExpressions.datetrunc(DatePart.month, sCrmPoints2.txnDate);

        BigDecimal totalSpend = BigDecimal.ZERO;
        ScrollableResults scrollableResults = null;
        try {
            scrollableResults = createHibernateSQLQuery(sCrmPoints1, yearMonthDto)
                .orderBy(sCrmPoints1.txnAmount.sum().desc())
                .groupBy(sCrmPoints1.memberId)
                .createQuery(ConstructorExpression.create(DecileQueryResultDto.class,
                    memberIdPath,
                    sCrmPoints1.txnAmount.sum(),
                    sCrmPoints1.txnNo.count(),
                    new SQLSubQuery().from(sCrmPoints2)
                        .where(sCrmPoints1.memberId.eq(sCrmPoints2.memberId).and(createPredicate(sCrmPoints2.txnDate, yearMonthDto)))
                        .unique(sTxnMonth.countDistinct())
                )).scroll(ScrollMode.FORWARD_ONLY);

            StringBuilder memberIdsBuilder = new StringBuilder();

            for (int i = 0; i < decileSizes.length; i++) {
                int size = decileSizes[i];
                if (size == 0) {
                    break;
                }
                boolean noMoreRecords = false; // In case records were deleted while iterating
                Double minSpend = null;
                Double maxSpend = null;
                BigDecimal decileTotalSpend = BigDecimal.ZERO;
                Long totalTxnCount = 0L;
                BigDecimal aveTxnAmountPerVisitTotal = BigDecimal.ZERO;
                BigDecimal aveTxnAmountPerMonthTotal = BigDecimal.ZERO;

                int actualSize = size;
                while (size > 0) {
                    if (!scrollableResults.next()) {
                        noMoreRecords = true;
                        actualSize -= size;
                        break;
                    }

                    DecileQueryResultDto dto = (DecileQueryResultDto) scrollableResults.get(0);
                    memberIdsBuilder.append(dto.getMemberId());
                    memberIdsBuilder.append(",");
                    Double txnAmountTotal = dto.getTxnAmountTotal() == null ? 0.0 : dto.getTxnAmountTotal();
                    Long txnNoCount = dto.getTxnNoCount();
                    Long monthWithTxnCount = dto.getMonthWithTxnCount();

                    if (minSpend == null || minSpend > txnAmountTotal) {
                        minSpend = txnAmountTotal;
                    }
                    if (maxSpend == null || maxSpend < txnAmountTotal) {
                        maxSpend = txnAmountTotal;
                    }
                    BigDecimal bTxnAmountTotal = new BigDecimal(txnAmountTotal);
                    totalSpend = totalSpend.add(bTxnAmountTotal);
                    decileTotalSpend = decileTotalSpend.add(bTxnAmountTotal);

                    totalTxnCount += txnNoCount;

                    if(txnNoCount != 0) {
	                    aveTxnAmountPerVisitTotal = aveTxnAmountPerVisitTotal.add(
	                        new BigDecimal(txnAmountTotal).divide(new BigDecimal(txnNoCount), ROUNDING_CONTEXT));
                    }
                    if(monthWithTxnCount != 0) {
	                    aveTxnAmountPerMonthTotal = aveTxnAmountPerMonthTotal.add(
	                        new BigDecimal(txnAmountTotal).divide(new BigDecimal(monthWithTxnCount), ROUNDING_CONTEXT));
                    }

                    size--;
                }
                DecileReportDto decileReportDto = new DecileReportDto();
                String memberIds = memberIdsBuilder.toString();
                if (memberIds.endsWith(",")) {
                    memberIds = memberIds.substring(0, memberIds.length() - 1);
                }
                decileReportDto.setMemberIds(memberIds);
                decileReportDto.setNo(i + 1);
                decileReportDto.setNumberOfCustomers(actualSize);
                decileReportDto.setMinSpend(minSpend);
                decileReportDto.setMaxSpend(maxSpend);
                decileReportDto.setAveTxnCount(totalTxnCount / actualSize);
                decileReportDto
                    .setAveTxnSize(aveTxnAmountPerVisitTotal.divide(new BigDecimal(actualSize), ROUNDING_CONTEXT).doubleValue());
                BigDecimal aveSpendPerMonth =
                    aveTxnAmountPerMonthTotal.divide(new BigDecimal(yearMonthDto.getTotalMonths()), ROUNDING_CONTEXT);
                decileReportDto.setAveSpendPerMonth(aveSpendPerMonth.doubleValue());
                decileReportDto.setTotalSpendBd(decileTotalSpend);

                results.add(decileReportDto);
                if (noMoreRecords) {
                    break;
                }
            }
        } finally {
            if (scrollableResults != null) {
                scrollableResults.close();
            }
        }

        for (DecileReportDto dto : results) {
            BigDecimal decileTotalSpend = dto.getTotalSpendBd();
            BigDecimal percentage = decileTotalSpend.divide(totalSpend, ROUNDING_CONTEXT);
            dto.setPercentage(percentage.multiply(new BigDecimal(100)).doubleValue());
        }
        return results;
    }

    private HibernateSQLQuery createHibernateSQLQuery(final SCrmPoints sCrmPoints, final YearMonthDto yearMonthDto) {
        return new HibernateSQLQuery(statelessSession, dbMetadataUtil.getSqlTempates()).from(sCrmPoints)
            .where(createPredicate(sCrmPoints.txnDate, yearMonthDto));
    }

    private BooleanExpression createPredicate(DateTimePath<Timestamp> transactionDateTime, final YearMonthDto yearMonthDto) {
        return transactionDateTime.goe(new Timestamp(yearMonthDto.getDateFrom().toDateMidnight().getMillis()))
            .and(transactionDateTime.lt(new Timestamp(yearMonthDto.getDateTo().plusDays(1).toDateMidnight().getMillis())));
    }

    private int[] getMemberDecileSizes(final long total) {
        int[] decileSizes = new int[TEN];
        if (total <= TEN) {
            for (int i = 0; i < total; i++) {
                decileSizes[i] = 1;
            }
        } else {
            BigDecimal ten = new BigDecimal(TEN);
            BigDecimal quotient = new BigDecimal(total).divide(ten);
            int iPart = quotient.intValue();
            for (int i = 0; i < TEN; i++) {
                decileSizes[i] = iPart;
            }

            BigDecimal fPartInInt = quotient.subtract(new BigDecimal(iPart)).multiply(ten);
            int startIndex = ten.subtract(fPartInInt).intValue();
            for (int i = startIndex; i < TEN; i++) {
                decileSizes[i] += 1;
            }
        }
        return decileSizes;
    }
}
