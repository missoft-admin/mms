package com.transretail.crm.giftcard.dto;


import java.math.BigDecimal;

import org.apache.commons.lang3.StringUtils;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.transretail.crm.core.dto.MessageDto;
import com.transretail.crm.giftcard.entity.EGiftCardInfo;
import com.transretail.crm.giftcard.entity.GiftCardInventory;
import com.transretail.crm.giftcard.entity.support.GiftCardInventoryStatus;


@JsonInclude(JsonInclude.Include.NON_NULL)
public class EGiftCardInfoDto extends GiftCardInfo {

    private String mobileNo;
    private String email;
    @JsonIgnore
    private String pin;
    @JsonIgnore
    private String altPin;
    private Boolean isEmailSent;

    @JsonIgnore
    private Long id;
    private String barcode;

    @JsonIgnore
    private MessageDto emailMsg;
    @JsonIgnore
    private MessageDto smsMsg;



    public EGiftCardInfoDto(){}
    public EGiftCardInfoDto( Long series, String barcode, String productCode, String productName, BigDecimal faceValue, GiftCardInventoryStatus status, Boolean allowPartialRedeem, Boolean allowReload ){
    	this.barcode = barcode;
    	super.withProfile( productCode, productName )
    		.getProfile().withAllowPartialRedeem( allowPartialRedeem ).withAllowReload( allowReload );
    	super.withFaceValue( new Double( faceValue.doubleValue() ) );
    	super.withStatus( status );
    	super.withSeries( series );
    }
    public EGiftCardInfoDto( GiftCardInventory model ) {
    	if ( null != model.getEgcInfo() ) {
    		EGiftCardInfo egcInfo = model.getEgcInfo();
    		this.mobileNo = egcInfo.getMobileNo();
    		this.email = egcInfo.getEmail();
    		this.pin = egcInfo.getPin();
    		this.altPin = egcInfo.getAltPin();
    		this.isEmailSent = egcInfo.getIsEmailSent();
    	}
    	this.id = model.getId();
    	this.barcode = model.getBarcode();
    }
    public EGiftCardInfo toModel() {
    	EGiftCardInfo egcInfo = new EGiftCardInfo();
    	if ( StringUtils.isNotBlank( mobileNo ) ) {
    		egcInfo.setMobileNo( mobileNo );
    	}
    	if ( StringUtils.isNotBlank( email ) ) {
    		egcInfo.setEmail( email );
    	}
    	if ( StringUtils.isNotBlank( pin ) ) {
    		egcInfo.setPin( pin );
    	}
    	if ( StringUtils.isNotBlank( altPin ) ) {
    		egcInfo.setAltPin( altPin );
    	}
    	if ( null != isEmailSent ) {
    		egcInfo.setIsEmailSent( isEmailSent );
    	}
    	return egcInfo;
    }



	public String getMobileNo() {
		return mobileNo;
	}
	public void setMobileNo(String mobileNo) {
		this.mobileNo = mobileNo;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getPin() {
		return pin;
	}
	public void setPin(String pin) {
		this.pin = pin;
	}
	public String getAltPin() {
		return altPin;
	}
	public void setAltPin(String altPin) {
		this.altPin = altPin;
	}
	public Boolean getIsEmailSent() {
		return isEmailSent;
	}
	public void setIsEmailSent(Boolean isEmailSent) {
		this.isEmailSent = isEmailSent;
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getBarcode() {
		return barcode;
	}
	public void setBarcode(String barcode) {
		this.barcode = barcode;
	}
	public MessageDto getEmailMsg() {
		return emailMsg;
	}
	public void setEmailMsg(MessageDto emailMsg) {
		this.emailMsg = emailMsg;
	}
	public MessageDto getSmsMsg() {
		return smsMsg;
	}
	public void setSmsMsg(MessageDto smsMsg) {
		this.smsMsg = smsMsg;
	}

}
