package com.transretail.crm.product.service.impl;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

/**
 * @author Allan G. Ramirez (agramirez@exist.com)
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath*:/META-INF/spring/applicationContext*.xml", "classpath:applicationContext-test.xml"})
public class ProductHierSchedulerServiceImplTest {
    /*
    H2 doesn't support scroll

        @PersistenceContext
        private EntityManager em;
        @Autowired
        private PlatformTransactionManager transactionManager;
        @Autowired
        private ProductHierSchedulerService service;

        @Before
        public void onSetup() throws Exception {
            InputStream is = Thread.currentThread().getContextClassLoader().getResourceAsStream("scripts/producthier_test.sql");
            try {
                final Scanner in = new Scanner(is);
                TransactionTemplate transactionTemplate = new TransactionTemplate(transactionManager);
                transactionTemplate.execute(new TransactionCallback() {
                    public Object doInTransaction(TransactionStatus status) {
                        while (in.hasNext()) {
                            String script = in.nextLine();
                            if (StringUtils.isNotBlank(script)) {
                                em.createNativeQuery(script).executeUpdate();
                            }
                        }
                        return null;
                    }
                });
            } finally {
                IOUtils.closeQuietly(is);
            }
        }

        @Test
        public void updateCrmProductHierTest() {
            service.updateCrmProductHier();
        }
    */
    @Test
    public void test() {

    }
}
