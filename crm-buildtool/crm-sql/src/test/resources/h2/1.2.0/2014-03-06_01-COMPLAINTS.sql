create table CRM_COMPLAINT (
        id bigint generated by default as identity,
        CREATED_BY varchar(255),
        CREATED_DATETIME timestamp,
        LAST_UPDATED_BY varchar(255),
        LAST_UPDATED_DATETIME timestamp,
        ASSIGNED_TO varchar(20),
        COMPLAINTS varchar(255) not null,
        FILED_DATE timestamp,
        EMAIL varchar(50),
        FIRST_NAME varchar(100) not null,
        LAST_NAME varchar(100),
        MOBILE_NO varchar(20),
        priority varchar(20),
        status varchar(20),
        STORE_CODE varchar(20),
        TICKET_NO varchar(20),
        GENDER varchar(15) not null,
        primary key (id)
    );

alter table CRM_COMPLAINT 
        add constraint UK_bef8pis62u3ci04nifflw89j5 unique (TICKET_NO);

alter table CRM_COMPLAINT 
        add constraint FK_l6atb9iev9h13y57xjr0c8jnw 
        foreign key (GENDER) 
        references CRM_REF_LOOKUP_DTL;