package com.transretail.crm.web.controller.util;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

import com.transretail.crm.core.entity.RewardSchemeModel;
import com.transretail.crm.core.entity.enums.Status;
import com.transretail.crm.core.service.PointsSchemeService;

@Component
public class VoucherSchemeValidator implements Validator {
    @Autowired
    PointsSchemeService voucherSchemeService;
    
    @Autowired
    MessageSource messageSource;

	private final static String ITS_DEFAULTMSG_INVALID   = "Info given is invalid.";
	private final static String ITS_PROPKEY_EMPTYMSG     = "propkey_msg_notempty";
	private final static String ITS_PROPKEY_VALUEFROM    = "valueFrom";
	private final static String ITS_PROPKEY_VALUETO      = "valueTo";
	private final static String ITS_PROPKEY_AMOUNT       = "amount";
	private final static String ITS_PROPKEY_VALIDPERIOD  = "validPeriod";
	private final static String ITS_PROPKEY_STATUS       = "status";
	private final static String ITS_PROPKEY_PREARGUMENT  = "propkey_voucherscheme_";
	private final static String ITS_PROPKEY_WITHIN       = "propkey_msg_value_within_range";
	private final static String ITS_PROPKEY_ENCOMPASS    = "propkey_msg_value_contains_range";
	private final static String ITS_PROPKEY_INVALID      = "propkey_msg_invalid_value";
	private final static String ITS_PROPKEY_GREATER      = "propkey_msg_greater_than";
		

	public boolean supports(Class<?> clazz) {
		return RewardSchemeModel.class.isAssignableFrom(clazz);
	}

	public void validate( Object target, Errors inErrors ) {
		validateEmptyFields(inErrors);
		validateOverlappingValues((RewardSchemeModel) target, inErrors);
		validateIncorrectFieldValues((RewardSchemeModel) target, inErrors);
	}
	
	void validateIncorrectFieldValues(RewardSchemeModel model, Errors errors) {
		if(model.getValueFrom() != null && model.getValueTo() != null &&
				model.getValueFrom() > model.getValueTo()) {
			errors.reject(getMessage(ITS_PROPKEY_GREATER, 
					new Object[] {ITS_PROPKEY_PREARGUMENT + ITS_PROPKEY_VALUEFROM, 
					ITS_PROPKEY_PREARGUMENT + ITS_PROPKEY_VALUETO}));
		}
		
		if(model.getAmount() != null && model.getAmount() <= 0 ) {
			rejectIfInvalid(ITS_PROPKEY_AMOUNT, errors);
		}
		if(model.getValueFrom() != null && model.getValueFrom() < 0) {
			rejectIfInvalid(ITS_PROPKEY_VALUEFROM, errors);
		}
		if(model.getValueTo() != null && model.getValueTo() < 0) {
			rejectIfInvalid(ITS_PROPKEY_VALUETO, errors);
		}
		if(model.getValidPeriod() <= 0) {
			rejectIfInvalid(ITS_PROPKEY_VALIDPERIOD, errors);
		}
	}
	
	void validateOverlappingValues(RewardSchemeModel model, Errors errors) {
		if(model.getStatus() == Status.INACTIVE) return;
		
		Double from = model.getValueFrom();
		Double to = model.getValueTo();
		Long id = model.getId();
		
		if(from != null && to != null && voucherSchemeService.hasOverlappingRange(id, from, to)) {
			errors.reject(getMessage(ITS_PROPKEY_ENCOMPASS, 
					new Object[] {ITS_PROPKEY_PREARGUMENT + ITS_PROPKEY_VALUEFROM, 
					ITS_PROPKEY_PREARGUMENT + ITS_PROPKEY_VALUETO}));

		}
		else {
			if(from != null && voucherSchemeService.hasOverlappingValue(id, from)) {
				errors.reject(getMessage(ITS_PROPKEY_WITHIN, 
						new Object[] {ITS_PROPKEY_PREARGUMENT + ITS_PROPKEY_VALUEFROM}));
			}
			
			if(to != null && voucherSchemeService.hasOverlappingValue(id, to)) {
				errors.reject(getMessage(ITS_PROPKEY_WITHIN, 
						new Object[] {ITS_PROPKEY_PREARGUMENT + ITS_PROPKEY_VALUETO}));
			}
		}
	}
	
	void validateEmptyFields( Errors errors ) {
		rejectIfEmpty(ITS_PROPKEY_VALUEFROM, errors);
		rejectIfEmpty(ITS_PROPKEY_VALUETO, errors);
		rejectIfEmpty(ITS_PROPKEY_AMOUNT, errors);
		rejectIfEmpty(ITS_PROPKEY_VALIDPERIOD, errors);
		rejectIfEmpty(ITS_PROPKEY_STATUS, errors);
	}
	
	void rejectIfInvalid(String propertyKey, Errors errors) {
		errors.reject(getMessage(ITS_PROPKEY_INVALID, new Object[] { (ITS_PROPKEY_PREARGUMENT + propertyKey) } ));
	}
	
	void rejectIfEmpty(String propertyKey, Errors errors) {
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, propertyKey, 
				getMessage( ITS_PROPKEY_EMPTYMSG, new Object[] { (ITS_PROPKEY_PREARGUMENT + propertyKey) } ) );
	}
	
	private String getMessage(String inMsgCode, Object[] inArgMsgCodes) {
		for ( int i=0; i < inArgMsgCodes.length; i++ ) {
			inArgMsgCodes[i] = messageSource.getMessage( inArgMsgCodes[i].toString().toLowerCase(), null, LocaleContextHolder.getLocale());
		}
		return messageSource.getMessage(inMsgCode, inArgMsgCodes, ITS_DEFAULTMSG_INVALID, LocaleContextHolder.getLocale());
	}
}
