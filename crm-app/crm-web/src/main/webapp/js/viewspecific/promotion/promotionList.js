$(document).ready( function() {

	var $theLists = $( ".list_promotion" );
	var $theSearchForm = $( "#promotionSearchForm" );
	var $targetMember = $( "#content_targetMemberList" );
	var $targetMemberList = $targetMember.find( "#list_targetMember" );



	initLinks();
	initDialog();
	customizeDateColumn();
	//initDataTable();



	function initLinks() {

		$( ".link_promotionRemarks" ).each( function( idx, thePromotionRemarksLink ) {
			$( this ).click( function(e) {
				e.preventDefault();
				if ( $(this).attr("href") ) {
					$.get( $(this).attr("href"), function( resp ) {
						$("#form_promotionRemarks").html( resp );
					}, "html" );
				}
			});
		});

		$( ".link_overlap" ).each( function( idx, thePromotionOverlapLink ) {
			$( this ).click( function(e) {
				e.preventDefault();
				$overlappingDialog.modal( 'show' );
				OverlapPromotions.show( this.id );
			});
		});

		$( ".countMembersLink" ).each( function( idx ) {
			$( this ).click( function(e) {
				e.preventDefault();
				$targetMemberList.data( "url", $(this).attr( "href" ) );
				$targetMember.modal( "show" );
				/*$.get( $(this).attr( "href" ), 
					function( resp ) { 
						$( "#content_targetMemberCount" ).modal( "show" );
						$( "#content_targetMemberCount" ).find( "#noOfTargetMembers" ).html(resp.result);
					} 
				);*/
			});
		});
		
		$( ".promoParticipantsLink" ).click(function(e) {
			e.preventDefault();
			var printurl = $(this).attr("href");
			var validationUrl = $(this).data("validationUrl");
			$.get(validationUrl, function(data) {
				if(data.success) 
					window.open(printurl, '_blank', 'toolbar=0,location=0,menubar=0');
				else {
					getConfirm($("#content_promotionList").data("noParticipantsAlert"), function(result) { 
				      if(result) {
				      }
				    });
				}
			}, "json" );
			
		});
		
		
	}

	function initDialog() {
		$( "#content_targetMemberCount" ).modal({ show : false });
		$( "#content_targetMemberCount" ).on( "hide", function() { $( "#content_targetMemberCount" ).find( "#noOfTargetMembers" ).empty(); });

		$targetMember.modal({ show : false });
		$targetMember
			.on( "hide", function() {
				$targetMemberList.empty();
			})
			.on( "show", function() {
				$targetMemberList.ajaxDataTable({
		            'autoload'    : true,
		            'ajaxSource'  : $targetMemberList.data( "url" ),
		            'columnHeaders' : [ 
		                $targetMemberList.data( "hdr-accountno" ),
		                $targetMemberList.data( "hdr-username" ),
		                $targetMemberList.data( "hdr-lastname" ),
		                $targetMemberList.data( "hdr-firstname" ) ],
		            'modelFields'  : [
		                { name  : 'accountId', sortable : true },
		                { name  : 'username', sortable : true },
		                { name  : 'lastName', sortable : true },
		                { name  : 'firstName', sortable : true } ]
				});
			});
	}

	function customizeDateColumn() {
		$.each( $( ".promotion_endDate" ), function( idx ) {
			if ( $( this ).html() ) { $( this ).html( new Date( $( this ).html() - 0 ).customize(1) ); }
			else { $( this ).html( "" ); }
		});
	}

	function initDataTable() {

		$.each( $theLists, function( idx, thePromoList ) {

			var $thePromoList = $( thePromoList );
			var theHeaders = { 
				name: $thePromoList.data( "hdr-name" ),
				description: $thePromoList.data( "hdr-description" )
			};

			$thePromoList.ajaxDataTable({
				'autoload' 		: false,
				'ajaxSource' 	: $thePromoList.data( "url" ),
				'columnHeaders' : [	
			        theHeaders['name'],
				 	theHeaders['description']
				],
				'modelFields' 	: [
				 	{ name 	: 'name', sortable : true },
				 	{ name 	: 'description', sortable : true }
				 ]
			});
			$thePromoList.ajaxDataTable( 
					"search",
					$theSearchForm.toObject( { mode : 'first', skipEmpty : true } ),
					function () {});

		});
		/*$.each( $theLists, function( idx, thePromoList ) {
			var $thePromoList = $( thePromoList );
			if ( 0 < $thePromoList.find( ".dataTables_empty" ).length ) {
				$thePromoList.remove();
			}
		});*/
	}
});



