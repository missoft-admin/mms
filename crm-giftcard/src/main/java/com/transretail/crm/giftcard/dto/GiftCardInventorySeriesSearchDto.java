package com.transretail.crm.giftcard.dto;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;

import com.mysema.query.types.expr.BooleanExpression;
import com.transretail.crm.common.service.dto.request.AbstractSearchFormDto;
import com.transretail.crm.giftcard.entity.QGiftCardInventory;

/**
 * @author Allan G. Ramirez (agramirez@exist.com)
 */
public class GiftCardInventorySeriesSearchDto extends AbstractSearchFormDto {
    private Long seriesFrom;
    private Long seriesTo;
    private List<String> cards;

    public Long getSeriesFrom() {
        return seriesFrom;
    }

    public void setSeriesFrom(Long seriesFrom) {
        this.seriesFrom = seriesFrom;
    }

    public Long getSeriesTo() {
        return seriesTo;
    }

    public void setSeriesTo(Long seriesTo) {
        this.seriesTo = seriesTo;
    }

    public List<String> getCards() {
        return cards;
    }

    public void setCards(List<String> cards) {
        this.cards = cards;
    }

    @Override
    public BooleanExpression createSearchExpression() {
        List<BooleanExpression> expressions = new ArrayList<BooleanExpression>();
        QGiftCardInventory qInventory = QGiftCardInventory.giftCardInventory;
        if (getSeriesFrom() != null) {
            expressions.add(qInventory.series.goe(getSeriesFrom()));
        }
        if (getSeriesTo() != null) {
            expressions.add(qInventory.series.loe(getSeriesTo()));
        }
        return BooleanExpression.allOf(expressions.toArray(new BooleanExpression[expressions.size()]));
    }

}
