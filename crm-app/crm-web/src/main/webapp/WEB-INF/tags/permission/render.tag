<jsp:root xmlns:c="http://java.sun.com/jsp/jstl/core" xmlns:fn="http://java.sun.com/jsp/jstl/functions"
          xmlns:util="urn:jsptagdir:/WEB-INF/tags/util"
          xmlns:jsp="http://java.sun.com/JSP/Page" xmlns:spring="http://www.springframework.org/tags" version="2.0">

  <jsp:directive.tag import="com.transretail.crm.core.security.model.impl.CustomSecurityUserDetailsImpl"/>
  <jsp:directive.tag import="com.transretail.crm.core.security.util.UserUtil"/>

  <jsp:output omit-xml-declaration="yes"/>
  <jsp:directive.attribute name="userStore" type="java.lang.String" required="false" rtexprvalue="true" description=""/>
  <jsp:directive.attribute name="inventoryLoc" type="java.lang.String" required="false" rtexprvalue="true" description=""/>

  <jsp:scriptlet>
    <![CDATA[
      CustomSecurityUserDetailsImpl currentUser = UserUtil.getCurrentUser();

      String userStoreCode = (String) jspContext.getAttribute( "userStore" );
      if ( null != userStoreCode 
    		  && userStoreCode.trim().equalsIgnoreCase( null != currentUser.getStoreCode()? currentUser.getStoreCode() : "" ) ) {
          jspContext.setAttribute( "isAuthorizedUserStore", true );
      }

	String userInventoryLocCode = (String) jspContext.getAttribute( "inventoryLoc" );
      String[] inventory = userInventoryLocCode.split(",");
      if (inventory != null) {
        for (String check : inventory) {
            if (check.trim().equalsIgnoreCase( null != currentUser.getInventoryLocation()? currentUser.getInventoryLocation() : "" )) {
                jspContext.setAttribute( "isAuthorizedInventoryLoc", true );
            }
        }
      }
    ]]>
  </jsp:scriptlet>

  <c:if test="${empty userStore and empty inventoryLoc or isAuthorizedUserStore}"><jsp:doBody/></c:if>
  <c:if test="${empty inventoryLoc and empty userStore or isAuthorizedInventoryLoc}"><jsp:doBody/></c:if>

</jsp:root>
