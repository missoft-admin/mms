package com.transretail.crm.core.service.impl;


import java.util.ArrayList;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.EnumUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.mysema.query.types.expr.BooleanExpression;
import com.transretail.crm.common.service.dto.response.CodeDescDto;
import com.transretail.crm.common.service.dto.response.ResultList;
import com.transretail.crm.common.service.util.SpringDataPagingUtil;
import com.transretail.crm.core.dto.ApprovalRemarkDto;
import com.transretail.crm.core.dto.ComplaintDto;
import com.transretail.crm.core.dto.ComplaintSearchDto;
import com.transretail.crm.core.dto.MessageDto;
import com.transretail.crm.core.dto.MessageDto.MsgType;
import com.transretail.crm.core.entity.ApprovalRemark;
import com.transretail.crm.core.entity.Complaint;
import com.transretail.crm.core.entity.Complaint.ComplaintMemberField;
import com.transretail.crm.core.entity.Complaint.ComplaintStatus;
import com.transretail.crm.core.entity.MemberModel;
import com.transretail.crm.core.entity.QMemberModel;
import com.transretail.crm.core.entity.enums.ModelType;
import com.transretail.crm.core.entity.lookup.LookupDetail;
import com.transretail.crm.core.entity.lookup.Store;
import com.transretail.crm.core.repo.ApprovalRemarkRepo;
import com.transretail.crm.core.repo.ComplaintRepo;
import com.transretail.crm.core.repo.MemberRepo;
import com.transretail.crm.core.security.util.UserUtil;
import com.transretail.crm.core.service.CodePropertiesService;
import com.transretail.crm.core.service.ComplaintService;
import com.transretail.crm.core.service.LookupService;
import com.transretail.crm.core.service.StoreService;
import com.transretail.crm.core.util.generator.IdGeneratorService;


@Service("complaintService")
@Transactional
public class ComplaintServiceImpl implements ComplaintService {

	@Autowired
	ComplaintRepo repo;
	@Autowired
	MemberRepo memberRepo;
	@Autowired
	StoreService storeService;
	@Autowired
	LookupService lookupService;
	@Autowired
	CodePropertiesService codePropertiesService;
	@Autowired
	ApprovalRemarkRepo approvalRemarkRepo;
	@Autowired
	IdGeneratorService customIdGeneratorService;
	@Autowired
	MessageSource messageSource;



	@Override
	public ResultList<ComplaintDto> search( ComplaintSearchDto dto ) {
		Pageable pageable = SpringDataPagingUtil.INSTANCE.createPageable( dto.getPagination() );
        BooleanExpression filter = dto.createSearchExpression();
        Page<Complaint> page = ( filter != null )? repo.findAll( filter, pageable ) : repo.findAll( pageable );
        return new ResultList<ComplaintDto>( toDtoList( page.getContent() ), page.getTotalElements(), page.getNumber(), page.getSize() );
	}

	@Override
	public ComplaintDto getDto( Long id ) {
		if ( null == id ) {
			return null;
		}
		return new ComplaintDto( repo.findOne( id ) );
	}

	@Override
	public ComplaintDto saveDto( ComplaintDto dto, boolean isNew ) {
		Complaint model = dto.toModel();
		if ( isNew ) {
			model.setTicketNo( customIdGeneratorService.generateId() );
			if ( null != UserUtil.getCurrentUser().getStore() ) {
				model.setStoreCode( UserUtil.getCurrentUser().getStore().getCode() );
			}
		}

		model = repo.save( model );
		if ( isNew ) {
			dto.setId( model.getId() );
			dto.setStatus( codePropertiesService.getComplaintStatNew() );//dto.setStatus( ComplaintStatus.NEW );
			createRemark( dto );

			if ( StringUtils.isNotBlank( dto.getAssignedTo() ) ) {
				return assignDto( dto );
			}

			model.setStatus( dto.getStatus() );
			model = repo.save( model );
		}
		return new ComplaintDto( model );
	}

	@Override
	public ComplaintDto updateDto( ComplaintDto dto ) {
		if ( null == dto || null == dto.getId() ) {
			return null;
		}
		assignDto( dto );
		Complaint model = dto.toUpdatedModel( repo.findOne( dto.getId() ) );
		return new ComplaintDto( repo.save( model ) );
	}

	@Override
	public ComplaintDto assignDto( ComplaintDto dto ) {
		if ( null == dto || null == dto.getId() ) {
			return null;
		}

		Complaint model = repo.findOne( dto.getId() );

		ApprovalRemarkDto remarkDto = null;
		if ( StringUtils.isBlank( model.getAssignedTo() ) && StringUtils.isNotBlank( dto.getAssignedTo() ) ) {
			dto.setStatus( codePropertiesService.getComplaintStatAssigned() );//dto.setStatus( ComplaintStatus.ASSIGNED );
			dto.setRemarks( messageSource.getMessage( "complaint_assigned", null, LocaleContextHolder.getLocale() ) 
					+ ": " + dto.getAssignedTo()
					+ ( StringUtils.isNotBlank( dto.getRemarks() )? ". [" + dto.getRemarks() + "]" : "" ) );
			remarkDto = createRemark( dto );
		}
		else if ( StringUtils.isNotBlank( model.getAssignedTo() ) ) {
			if ( StringUtils.isNotBlank( dto.getAssignedTo() ) ) {
				boolean isReassigned = ( StringUtils.isNotBlank( model.getAssignedTo() ) 
						&& StringUtils.isNotBlank( dto.getAssignedTo() ) 
						&& !dto.getAssignedTo().equalsIgnoreCase( model.getAssignedTo() ) );
				//dto.setStatus( isReassigned? ComplaintStatus.REASSIGNED : ComplaintStatus.ASSIGNED );
				dto.setStatus( isReassigned? codePropertiesService.getComplaintStatReassigned() 
						: codePropertiesService.getComplaintStatAssigned() );
				dto.setRemarks( messageSource.getMessage( isReassigned? "complaint_reassigned" : "complaint_assigned", 
						null, LocaleContextHolder.getLocale() ) 
						+ ": " + dto.getAssignedTo()
						+ ( StringUtils.isNotBlank( dto.getRemarks() )? ". [" + dto.getRemarks() + "]" : "" ) );
				remarkDto = createRemark( dto );
			}
			else if ( StringUtils.isNotBlank( dto.getRemarks() ) ) {
				dto.setRemarks( dto.getRemarks() );
				remarkDto = createRemark( dto );
			}
		}

		if ( null != remarkDto ) {
			model.setStatus( dto.getStatus() );
			model.setAssignedTo( dto.getAssignedTo() );
			model = repo.save( model );
		}
		return new ComplaintDto( model );
	}

	@Override
	public ComplaintDto populateMember( String fieldVal, ComplaintMemberField field ) {
		if ( StringUtils.isNotBlank( fieldVal ) && null != field ) {
			MemberModel member = null;
			if ( field == ComplaintMemberField.MOBILENO ) {
				member = memberRepo.findOne( QMemberModel.memberModel.contact.equalsIgnoreCase( fieldVal ) );
			}
			else if ( field == ComplaintMemberField.EMAIL ) {
				member = memberRepo.findOne( QMemberModel.memberModel.email.equalsIgnoreCase( fieldVal ) );
			}

			if ( null != member ) {
				ComplaintDto dto = new ComplaintDto();
				dto.setMobileNo( member.getContact() );
				dto.setEmail( member.getEmail() );
				dto.setFirstName( member.getFirstName() );
				dto.setLastName( member.getLastName() );
				dto.setGenderCode( ( null != member.getCustomerProfile().getGender() )? member.getCustomerProfile().getGender().getCode() : null );
				return dto;
			}
		}

		return null;
	}

    @Override
    public ResultList<ApprovalRemarkDto> getHistoryDtos( Long id ) {
    	if ( null == id ) {
    		return null;
    	}
    	List<ApprovalRemarkDto> dtos = ApprovalRemarkDto.toDtoList( 
    			approvalRemarkRepo.findByModelTypeAndModelId( ModelType.COMPLAINT.toString(), id.toString() ) );
    	if ( CollectionUtils.isNotEmpty( dtos ) ) {
    		for ( ApprovalRemarkDto dto : dtos ) {
    			LookupDetail ld = lookupService.getDetailByCode( dto.getStatus() );
    			dto.setStatus( null != ld? ld.getDescription() : null );
    		}
    	}
    	return new ResultList<ApprovalRemarkDto>( dtos );
    }

    @Override
    public ComplaintDto saveRemarks( ComplaintDto dto ) {
    	if ( null == dto || null == dto.getId() ) {
    		return null;
    	}

    	Complaint model = repo.findOne( dto.getId() );
    	if ( StringUtils.isNotBlank( model.getAssignedTo() ) && StringUtils.isNotBlank( dto.getAssignedTo() ) 
    			&& model.getAssignedTo().equalsIgnoreCase( dto.getAssignedTo() ) 
    			&& model.getStatus() == dto.getStatus() && StringUtils.isBlank( dto.getRemarks() ) ) {
    		return null;
    	}
    	//else if ( ( dto.getStatus() == ComplaintStatus.ASSIGNED || dto.getStatus() == ComplaintStatus.REASSIGNED ) ) {
    	else if ( StringUtils.isNotBlank( dto.getStatus() ) && 
    			dto.getStatus().equalsIgnoreCase( codePropertiesService.getComplaintStatNew() )
					|| dto.getStatus().equalsIgnoreCase( codePropertiesService.getComplaintStatReassigned() )
    				|| dto.getStatus().equalsIgnoreCase( codePropertiesService.getComplaintStatAssigned() ) ) {
    		return assignDto( dto );
    	}

    	createRemark( dto );

		model.setStatus( dto.getStatus() );
		model.setAssignedTo( dto.getAssignedTo() );
		return new ComplaintDto( repo.save( model ) );
    }

    @Override
    public ComplaintDto processReply( Long id, MessageDto msgDto ) {
    	if ( null == id ) {
    		return null;
    	}

    	Complaint model = repo.findOne( id );
    	ComplaintDto dto = new ComplaintDto( model );
    	dto.setStatus( codePropertiesService.getComplaintStatInprogress() );//dto.setStatus( ComplaintStatus.INPROGRESS );
    	dto.setRemarks( messageSource.getMessage( "complaint_lbl_repliedto", 
				new String[]{ msgDto.getMsgType() == MsgType.email? model.getEmail() : model.getMobileNo() }, 
				LocaleContextHolder.getLocale() ) 
				+ " [" + msgDto.getMessage() + "]" );
    	createRemark( dto );

		model.setStatus( dto.getStatus() );
    	return new ComplaintDto( repo.save( model ) );
    }

    @Override
    public List<CodeDescDto> getStats( ComplaintDto dto ) {
    	List<CodeDescDto> stats = new ArrayList<CodeDescDto>();

    	List<LookupDetail> lds = lookupService.getActiveDetailsByHeaderCode( codePropertiesService.getComplaintStatHdr() );
    	if ( CollectionUtils.isNotEmpty( lds ) ) {
    		for ( LookupDetail ld : lds ) {
    			if ( null != dto && StringUtils.isNotBlank( dto.getStatus() ) ) {
    				if ( !ld.getCode().equalsIgnoreCase( dto.getStatus() )
    						&& !ld.getCode().equalsIgnoreCase( codePropertiesService.getComplaintStatInprogress() ) 
    						&& !ld.getCode().equalsIgnoreCase( codePropertiesService.getComplaintStatResolved() ) ) {
    					continue;
    				}
    			}
    			stats.add( new CodeDescDto( ld.getCode(), null != ld? ld.getDescription() : null ) );
    		}
    	}

    	return stats;
    }



	private List<ComplaintDto> toDtoList( List<Complaint> models ) {
		if ( CollectionUtils.isEmpty( models ) ) {
			return new ArrayList<ComplaintDto>();
		}

		List<ComplaintDto> dtos = new ArrayList<ComplaintDto>();
		for ( Complaint model : models ) {
			ComplaintDto dto = new ComplaintDto( model, codePropertiesService.getComplaintStatResolved() );
			if ( StringUtils.isNotBlank( dto.getStoreCode() ) ) {
				Store store = storeService.getStoreByCode( dto.getStoreCode() );
				dto.setStoreDesc( ( null != store )? store.getName() : "" );
			}
			if ( StringUtils.isNotBlank( dto.getGenderCode() ) ) {
				LookupDetail lookup = lookupService.getDetailByCode( dto.getGenderCode() );
				dto.setGenderDesc( ( null != lookup )? lookup.getDescription() : "" );
			}
			//if ( null != model.getStatus() ) {
			if ( StringUtils.isNotBlank( model.getStatus() ) ) {
				LookupDetail lookup = lookupService.getDetailByCode( model.getStatus() );
				dto.setDispStatus( null != lookup? lookup.getDescription() : null );
			}
			dtos.add( dto );
		}
		return dtos;
	}

	@SuppressWarnings("unused")
	private LookupDetail getLookupStatus( String strStatus ) {
		ComplaintStatus status = EnumUtils.getEnum( ComplaintStatus.class, strStatus );
		switch( status ) {
			case NEW: return lookupService.getDetailByCode( codePropertiesService.getComplaintStatNew() );
			case ASSIGNED: return lookupService.getDetailByCode( codePropertiesService.getComplaintStatAssigned() );
			case REASSIGNED: return lookupService.getDetailByCode( codePropertiesService.getComplaintStatReassigned() );
			case INPROGRESS: return lookupService.getDetailByCode( codePropertiesService.getComplaintStatInprogress() );
			case CLOSED: return lookupService.getDetailByCode( codePropertiesService.getComplaintStatResolved() );
			default : return null;
		}
	}

	private ApprovalRemarkDto createRemark( ComplaintDto dto ) {
		if ( null == dto || null == dto.getId() ) {
			return null;
		}
		ApprovalRemark remark = new ApprovalRemark();
		remark.setModelId( dto.getId().toString() );
		remark.setModelType( String.valueOf( ModelType.COMPLAINT ) );
		remark.setStatus( lookupService.getDetailByCode( dto.getStatus() ) );
		remark.setRemarks( dto.getRemarks() );
		return new ApprovalRemarkDto( approvalRemarkRepo.save( remark ) );
	}
}
