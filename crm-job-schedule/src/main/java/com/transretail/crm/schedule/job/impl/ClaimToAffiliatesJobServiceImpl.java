package com.transretail.crm.schedule.job.impl;

import com.transretail.crm.giftcard.service.GiftCardAccountingService;
import com.transretail.crm.schedule.job.service.ClaimToAffiliatesJobService;
import org.joda.time.LocalDateTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service("claimToAffiliatesJobService")
public class ClaimToAffiliatesJobServiceImpl implements ClaimToAffiliatesJobService {

    private static final Logger LOGGER = LoggerFactory.getLogger(ClaimToAffiliatesJobServiceImpl.class);

    @Autowired
    private GiftCardAccountingService accountingService;

    @Override
    public void execute() {
        LOGGER.info("Executing C4 Claim To/Redemption For Affiliates Job at {}.", LocalDateTime.now());
        accountingService.createAffiliatesAcctEntry();
        LOGGER.info("C4 Claim To/Redemption For Affiliates Job successfully executed at {}.", LocalDateTime.now());
    }
}