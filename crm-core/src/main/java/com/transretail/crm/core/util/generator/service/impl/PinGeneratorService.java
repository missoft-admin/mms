package com.transretail.crm.core.util.generator.service.impl;


import com.transretail.crm.core.util.generator.IdGeneratorService;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Map;
import java.util.Random;

/**
 * ID generator for member pin
 */
@Service
@Transactional
public class PinGeneratorService implements IdGeneratorService {


    /**
     * pin length defaults to 6 and numeric only
     *
     * @return
     */
    @Override
    public String generateId() {

        int defaultLength = 6;

        Random r = new Random();

        String number = "";

        int counter = 0;

        while (counter++ < defaultLength) {
            number += r.nextInt(9);

        }


        return number;
    }

    @Override
    public String generateId(int length, boolean alphanumeric) {
        return null;
    }

    @Override
    public String generateId(Map props) {
        return null;
    }


    @Override
	public String generateId(String prepend) {
		// TODO Auto-generated method stub
		return null;
	}
}
