package com.transretail.crm.giftcard.service.impl;

import com.google.common.collect.Lists;
import com.transretail.crm.core.entity.CrmForPeoplesoftConfig;
import com.transretail.crm.core.entity.CrmForPeoplesoftConfig.TransactionType;
import com.transretail.crm.core.repo.CrmForPeoplesoftConfigRepo;
import com.transretail.crm.giftcard.dto.AffiliateDto;
import com.transretail.crm.giftcard.entity.CrmForPeoplesoft;
import com.transretail.crm.giftcard.repo.CrmForPeoplesoftRepo;
import com.transretail.crm.giftcard.repo.GiftCardTransactionRepo;
import com.transretail.crm.giftcard.service.GiftCardAccountingService;
import java.util.List;
import org.fest.assertions.api.Assertions;
import org.fest.assertions.core.Condition;
import org.joda.time.YearMonth;
import org.junit.Before;
import org.junit.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import static org.mockito.Matchers.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 *
 * @author Monte Cillo Co (mco@exist.com)
 */
public class GiftCardAccountingServiceImplTest {

    @Mock
    private GiftCardTransactionRepo cardTransactionRepo;
    @Mock
    private CrmForPeoplesoftRepo acctRepo;
    @Mock
    private CrmForPeoplesoftConfigRepo configRepo;
    @InjectMocks
    private GiftCardAccountingService accountingService = new GiftCardAccountingServiceImpl();

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void createAffiliatesAcctEntryTest() {
        List<AffiliateDto> afTxns = Lists.newArrayList(
                new AffiliateDto("ID050", TransactionType.AFFILIATE_CLAIM, 100000.0),
                new AffiliateDto("ID050", TransactionType.AFFILIATE_REDEMPTION, -100000.0));

        when(cardTransactionRepo.getBusinessUnitTransactionTypeAmount(any(YearMonth.class)))
                .thenReturn(afTxns);
        when(configRepo.findByTransactionType(TransactionType.AFFILIATE_CLAIM))
                .thenReturn(new CrmForPeoplesoftConfig() {
                    {
                        setBusinessUnit("ID050");
                        setMmsAcct("20");
                        setPsAcct("PS90900");
                        setTransactionType(TransactionType.AFFILIATE_CLAIM);
                        setType("");
                    }
                });
        when(configRepo.findByTransactionType(TransactionType.AFFILIATE_REDEMPTION))
                .thenReturn(new CrmForPeoplesoftConfig() {
                    {
                        setBusinessUnit("ID050");
                        setMmsAcct("20");
                        setPsAcct("PS90900");
                        setTransactionType(TransactionType.AFFILIATE_REDEMPTION);
                        setType("");
                    }
                });

        ArgumentCaptor<List> ac = ArgumentCaptor.forClass(List.class);

        accountingService.createAffiliatesAcctEntry();

        verify(acctRepo).save(ac.capture());
        List<CrmForPeoplesoft> list = ac.getValue();
        Assertions.assertThat(list).hasSize(2)
                .areExactly(1, new Condition() {

                    @Override
                    public boolean matches(Object value) {
                        CrmForPeoplesoft entry = (CrmForPeoplesoft) value;
                        return "PS90900".equals(entry.getPsAcct())
                        && "ID050".equals(entry.getBusinessUnit())
                        && "20".equals(entry.getMmsAcct())
                        && entry.getTransactionType() == TransactionType.AFFILIATE_CLAIM
                        && entry.getTransactionAmt().doubleValue() == -100000;
                    }
                })
                .areExactly(1, new Condition() {

                    @Override
                    public boolean matches(Object value) {
                        CrmForPeoplesoft entry = (CrmForPeoplesoft) value;
                        return "PS90900".equals(entry.getPsAcct())
                        && "ID050".equals(entry.getBusinessUnit())
                        && "20".equals(entry.getMmsAcct())
                        && entry.getTransactionType() == TransactionType.AFFILIATE_REDEMPTION
                        && entry.getTransactionAmt().doubleValue() == 100000;
                    }
                });
        
        verify(cardTransactionRepo).getBusinessUnitTransactionTypeAmount(any(YearMonth.class));
        verify(cardTransactionRepo).tagAffiliatesTransactionAsPosted(any(YearMonth.class));
    }

}
