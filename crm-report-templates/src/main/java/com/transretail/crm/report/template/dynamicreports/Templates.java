package com.transretail.crm.report.template.dynamicreports;

import static net.sf.dynamicreports.report.builder.DynamicReports.margin;
import static net.sf.dynamicreports.report.builder.DynamicReports.stl;

import java.awt.Color;

import net.sf.dynamicreports.report.builder.MarginBuilder;
import net.sf.dynamicreports.report.builder.style.StyleBuilder;
import net.sf.dynamicreports.report.constant.HorizontalAlignment;
import net.sf.dynamicreports.report.constant.VerticalAlignment;

/**
 * @author Ricardo Mariaca (r.mariaca@dynamicreports.org)
 */
public class Templates {
    public static final StyleBuilder rootStyle;
    public static final StyleBuilder columnStyle;
    public static final StyleBuilder columnTitleStyle;
    public static final MarginBuilder DEFAULT_MARGIN = margin(70);

    static {
        rootStyle = stl.style();
        columnStyle = stl.style(rootStyle).setPadding(2).setVerticalAlignment(VerticalAlignment.MIDDLE);
        columnTitleStyle = stl.style(columnStyle)
            .setBorder(stl.pen1Point())
            .setHorizontalAlignment(HorizontalAlignment.CENTER)
            .setBackgroundColor(Color.LIGHT_GRAY)
            .bold();
    }
}