package com.transretail.crm.giftcard.service;

import java.util.List;

import org.joda.time.LocalDate;

import com.transretail.crm.giftcard.dto.GcInventorySafetyStockDto;
import com.transretail.crm.giftcard.dto.SafetyStockDto;

/**
 * @author Allan G. Ramirez (agramirez@exist.com)
 */
public interface SafetyStockService {
    SafetyStockDto getSafetyStock(Long productProfileId);

    void update(SafetyStockDto dto);

    List<GcInventorySafetyStockDto> checkSafetyStock(LocalDate date);
}
