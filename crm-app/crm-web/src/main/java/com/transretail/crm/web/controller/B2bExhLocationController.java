package com.transretail.crm.web.controller;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.google.common.collect.Lists;
import com.transretail.crm.common.service.dto.response.NameIdPairDto;
import com.transretail.crm.common.service.dto.response.ResultList;
import com.transretail.crm.giftcard.dto.B2BExhDiscDto;
import com.transretail.crm.giftcard.dto.B2BExhLocDto;
import com.transretail.crm.giftcard.dto.B2bExhLocSearchDto;
import com.transretail.crm.giftcard.dto.ProductProfileDto;
import com.transretail.crm.giftcard.dto.ProductProfileSearchDto;
import com.transretail.crm.giftcard.service.B2bExhLocDiscService;
import com.transretail.crm.web.controller.util.ControllerResponse;

@RequestMapping("/giftcard/b2b/location")
@Controller
public class B2bExhLocationController {
	@Autowired
	private B2bExhLocDiscService service;
	@Autowired
	private MessageSource messageSource;
	
	@RequestMapping(produces = "text/html")
    public String list(
            Model uiModel) {
        return "giftcard/b2b/location";
    }
	
	@RequestMapping(value = "/list", method = RequestMethod.POST)
    public @ResponseBody ResultList<B2BExhLocDto> listProvince(
    		@RequestBody B2bExhLocSearchDto dto) {
    	return service.getLocations(dto);
    }
	
	@RequestMapping( value="/list/{filter}", produces="application/json; charset=utf-8", method=RequestMethod.GET )
	public @ResponseBody List<NameIdPairDto> list( @PathVariable(value="filter") String filter ) {
    	return service.getLocations(filter);
    }
	
	@RequestMapping(value = "/discounts/list", method = RequestMethod.POST)
    public @ResponseBody ResultList<B2BExhDiscDto> listDiscounts(
    		@RequestBody B2bExhLocSearchDto dto) {
    	return service.getDiscounts(dto);
    }
	
	@RequestMapping(value = "/form/{id}", method = RequestMethod.GET, produces = "text/html")
    public String showForm(
    		@PathVariable("id") String id,
    		Model uiModel) {
		
		B2BExhLocDto locDto = new B2BExhLocDto();
		if(StringUtils.isNumeric(id)) {
			locDto = service.getLocation(Long.valueOf(id));
		}
		uiModel.addAttribute("locationForm", locDto);
		return "giftcard/b2b/location/form";	
    }
	
	@RequestMapping(value = "/discounts/form/{id}/{locId}", method = RequestMethod.GET, produces = "text/html")
    public String showDiscountForm(
    		@PathVariable("id") String id,
    		@PathVariable String locId,
    		Model uiModel) {
		
		B2BExhDiscDto locDto = new B2BExhDiscDto();
		if(StringUtils.isNumeric(id)) {
			locDto = service.getDiscount(Long.valueOf(id));
		}
		if(StringUtils.isNumeric(locId)) {
			locDto.setLocId(Long.valueOf(locId));
		}
		uiModel.addAttribute("discountForm", locDto);
		return "giftcard/b2b/discount/form";	
    }
	
	
	@RequestMapping(value = "/save", method = RequestMethod.POST, produces = "application/json; charset=utf-8")
	@ResponseBody
	public ControllerResponse ajaxSave(
			@ModelAttribute(value = "locationForm") B2BExhLocDto locDto,
			BindingResult result, HttpServletRequest request) {

		ControllerResponse theResponse = new ControllerResponse();
		theResponse.setSuccess(true);
		
		
		if(StringUtils.isBlank(locDto.getName())) {
			result.reject(getMessage("propkey_msg_notempty", new Object[]{"b2b.location.name"}));
		}
		if(locDto.getStartDate() == null) {
			result.reject(getMessage("propkey_msg_notempty", new Object[]{"b2b.location.startdate"}));
		}
		if(locDto.getEndDate() == null) {
			result.reject(getMessage("propkey_msg_notempty", new Object[]{"b2b.location.enddate"}));
		}
		if(locDto.getStartDate() != null && locDto.getEndDate() != null && locDto.getEndDate().isBefore(locDto.getStartDate())) {
			result.reject(getMessage("b2b.err.invaliddaterange", null));
		}
		if(locDto.getStartDate() != null && locDto.getEndDate() != null && StringUtils.isNotBlank(locDto.getName())) {
			if(service.isExistOverlappingLocation(locDto)) {
				result.reject(getMessage("b2b.err.overlappingevents", null));
			}	
		}
		
		
		if (result.hasErrors()) {
			theResponse.setSuccess(false);
			theResponse.setResult(result.getAllErrors());
			return theResponse;
		}

		if (theResponse.isSuccess()) {
			service.saveLocation(locDto);
		}

		return theResponse;
	}
	
	@RequestMapping(value = "/discounts/save", method = RequestMethod.POST, produces = "application/json; charset=utf-8")
	@ResponseBody
	public ControllerResponse ajaxSave(
			@ModelAttribute(value = "discountForm") B2BExhDiscDto discDto,
			BindingResult result, HttpServletRequest request) {

		ControllerResponse theResponse = new ControllerResponse();
		theResponse.setSuccess(true);
		
		
		if(discDto.getMinAmount() == null) {
			result.reject(getMessage("propkey_msg_notempty", new Object[]{"b2b.location.minamount"}));
		}
		
		if(discDto.getMinAmount() != null && discDto.getMaxAmount() != null && discDto.getMaxAmount().compareTo(discDto.getMinAmount()) < 0) {
			result.reject(getMessage("b2b.err.invalidamountrange", null));
		}
		if(discDto.getDiscount() == null) {
			result.reject(getMessage("propkey_msg_notempty", new Object[]{"b2b.location.rate"}));
		}
		
		if(discDto.getMinAmount() != null &&
				discDto.getMaxAmount() != null &&
				discDto.getDiscount() != null &&
				service.isExistOverlappingDisc(discDto)) {
			result.reject(getMessage("b2b.err.overlappingdiscount", null));
		}
		
		if (result.hasErrors()) {
			theResponse.setSuccess(false);
			theResponse.setResult(result.getAllErrors());
			return theResponse;
		}

		if (theResponse.isSuccess()) {
			service.saveDiscount(discDto);
		}

		return theResponse;
	}
	
	@RequestMapping(value = "/{id}", method = RequestMethod.DELETE, produces = "application/json; charset=utf-8")
	@ResponseBody
    public ControllerResponse deleteLocation(@PathVariable("id") Long id) {
        
        
        ControllerResponse response = new ControllerResponse();
        response.setSuccess(true);
        service.deleteLocation(id);
        return response;
    }
	
	@RequestMapping(value = "/discounts/{id}", method = RequestMethod.DELETE, produces = "application/json; charset=utf-8")
	@ResponseBody
    public ControllerResponse deleteDiscount(@PathVariable("id") Long id) {
        
        ControllerResponse response = new ControllerResponse();
        response.setSuccess(true);
        service.deleteDiscount(id);
        return response;
    }
	
	private String getMessage(String inMsgCode, Object[] inArgMsgCodes) {
        if (inArgMsgCodes != null) {
            for (int i = 0; i < inArgMsgCodes.length; i++) {
                inArgMsgCodes[i] =
                    messageSource.getMessage(inArgMsgCodes[i].toString().toLowerCase(), null, LocaleContextHolder.getLocale());
            }
        }
        return messageSource.getMessage(inMsgCode, inArgMsgCodes, "Info given is invalid.", LocaleContextHolder.getLocale());
    }
}
