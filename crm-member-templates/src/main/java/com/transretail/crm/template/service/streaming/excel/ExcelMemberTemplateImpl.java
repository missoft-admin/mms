package com.transretail.crm.template.service.streaming.excel;

import java.io.IOException;
import java.io.InputStream;
import java.io.PushbackInputStream;
import java.util.Locale;
import java.util.Map;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.math.NumberUtils;
import org.apache.poi.POIXMLDocument;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.openxml4j.exceptions.OpenXML4JException;
import org.apache.poi.openxml4j.opc.OPCPackage;
import org.apache.poi.xssf.eventusermodel.ReadOnlySharedStringsTable;
import org.apache.poi.xssf.eventusermodel.XSSFReader;
import org.apache.poi.xssf.model.StylesTable;
import org.joda.time.DateTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.AutowireCapableBeanFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.xml.sax.ContentHandler;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.XMLReader;

import com.google.common.collect.Maps;
import com.transretail.crm.common.service.exception.GenericServiceException;
import com.transretail.crm.core.security.model.impl.CustomSecurityUserDetailsImpl;
import com.transretail.crm.core.security.util.UserUtil;
import com.transretail.crm.core.service.CodePropertiesService;
import com.transretail.crm.template.service.MemberTemplateParseException;
import com.transretail.crm.template.service.MemberTemplateParser;
import com.transretail.crm.template.service.Operation;
import com.transretail.crm.template.service.streaming.excel.handler.HandlerException;
import com.transretail.crm.template.service.streaming.excel.handler.XlsxSheetHandler;
import com.transretail.crm.template.service.streaming.excel.handler.sheet.SheetHandler;
import com.transretail.crm.template.service.streaming.excel.handler.sheet.optional.IndOtherHandler;
import com.transretail.crm.template.service.streaming.excel.handler.sheet.optional.IndPersonalHandler;
import com.transretail.crm.template.service.streaming.excel.handler.sheet.optional.IndWorkDataHandler;
import com.transretail.crm.template.service.streaming.excel.handler.sheet.optional.ProfCompanyDetailsHandler;
import com.transretail.crm.template.service.streaming.excel.handler.sheet.optional.ProfMarketingHandler;
import com.transretail.crm.template.service.streaming.excel.handler.sheet.required.NewEmpInfoHandler;
import com.transretail.crm.template.service.streaming.excel.handler.sheet.required.NewIndInfoHandler;
import com.transretail.crm.template.service.streaming.excel.handler.sheet.required.NewProfHandler;
import com.transretail.crm.template.service.streaming.excel.handler.sheet.required.UpdateEmpHandler;
import com.transretail.crm.template.service.streaming.excel.handler.sheet.required.UpdateIndHandler;
import com.transretail.crm.template.service.streaming.excel.handler.sheet.required.UpdateProfHandler;

/**
 * @author Allan G. Ramirez (agramirez@exist.com)
 */
@Service("streamingExcelMemberTemplateImpl")
public class ExcelMemberTemplateImpl implements MemberTemplateParser, InitializingBean {
    private static final Logger _LOG = LoggerFactory.getLogger(ExcelMemberTemplateImpl.class);

    @Autowired
    private MessageSource appMessageSource;
    @Autowired
    private ApplicationContext applicationContext;
    @Autowired
    private CodePropertiesService codePropService;

    private AutowireCapableBeanFactory beanFactory;

    private Map<String, Class<? extends SheetHandler>> requiredSheetHandlerClassMap = Maps.newHashMap();
    private Map<String, Class<? extends SheetHandler>> optionalSheetHandlerClassMap = Maps.newHashMap();

    @Override
    public void afterPropertiesSet() throws Exception {
        beanFactory = applicationContext.getAutowireCapableBeanFactory();

        String codeInd = codePropService.getDetailMemberTypeIndividual();
        String codeEmp = codePropService.getDetailMemberTypeEmployee();
        String codeProf = codePropService.getDetailMemberTypeProfessional();

        requiredSheetHandlerClassMap.put(createKey(Operation.NEW_MEMBERS, codeInd), NewIndInfoHandler.class);
        requiredSheetHandlerClassMap.put(createKey(Operation.NEW_MEMBERS, codeEmp), NewEmpInfoHandler.class);
        requiredSheetHandlerClassMap.put(createKey(Operation.NEW_MEMBERS, codeProf), NewProfHandler.class);
        requiredSheetHandlerClassMap.put(createKey(Operation.UPDATE_EXISTING_MEMBERS, codeInd), UpdateIndHandler.class);
        requiredSheetHandlerClassMap.put(createKey(Operation.UPDATE_EXISTING_MEMBERS, codeEmp), UpdateEmpHandler.class);
        requiredSheetHandlerClassMap.put(createKey(Operation.UPDATE_EXISTING_MEMBERS, codeProf), UpdateProfHandler.class);

        optionalSheetHandlerClassMap.put(createKey("OTHERDATA", codeInd), IndOtherHandler.class);
        optionalSheetHandlerClassMap.put(createKey("PERSONALDATA", codeInd), IndPersonalHandler.class);
        optionalSheetHandlerClassMap.put(createKey("WORKDATA", codeInd), IndWorkDataHandler.class);
        optionalSheetHandlerClassMap.put(createKey("COMPANYDETAILS", codeProf), ProfCompanyDetailsHandler.class);
        optionalSheetHandlerClassMap.put(createKey("MARKETING", codeProf), ProfMarketingHandler.class);
    }

    @Transactional(rollbackFor = Exception.class)
    public int parseMembers(InputStream is, String memberTypeCode, Locale locale, Operation operation) throws
        MemberTemplateParseException, InvalidFormatException, IOException {
        OPCPackage pkg = null;
        InputStream sheetStreem = null;
        try {
            // If clearly doesn't do mark/reset, wrap up
            if (!is.markSupported()) {
                is = new PushbackInputStream(is, 8);
            }
            if (!POIXMLDocument.hasOOXMLHeader(is)) {
                _LOG.error("InputStream was neither an OLE2 stream, nor an OOXML stream");
                throw new MemberTemplateParseException(
                    appMessageSource.getMessage("member.template.messages.excel.invalid", null, locale));
            }

            pkg = OPCPackage.open(is);
            ReadOnlySharedStringsTable strings = new ReadOnlySharedStringsTable(pkg);
            XSSFReader xssfReader = new XSSFReader(pkg);

            StylesTable styles = xssfReader.getStylesTable();
            XSSFReader.SheetIterator iter = (XSSFReader.SheetIterator) xssfReader.getSheetsData();

            if (!iter.hasNext()) {
                throw new MemberTemplateParseException(
                    appMessageSource.getMessage("member.template.messages.excel.empty", null, locale));
            }

            sheetStreem = iter.next();

            DateTime currentDateTime = DateTime.now();
            CustomSecurityUserDetailsImpl currentUser = UserUtil.getCurrentUser();
            SheetHandler memberInfoHandler =
                instantiateHandler(currentDateTime, currentUser, iter.getSheetName(), operation, memberTypeCode);
            if (memberInfoHandler == null) {
                sheetStreem.close();
                throw new MemberTemplateParseException(
                    appMessageSource.getMessage("member.template.messages.excel.membertype.nohandler",
                        new String[]{memberTypeCode}, locale)
                );
            }
            processSheet(memberInfoHandler, styles, strings, sheetStreem);
            _LOG.info("Excel parser: Parsed {} rows in {}.", memberInfoHandler.getTotalRows(), iter.getSheetName());
            sheetStreem.close();

            while (iter.hasNext()) {
                sheetStreem = iter.next();
                SheetHandler optionalSheetHandler = instantiateHandler(iter.getSheetName(), memberTypeCode);
                if (optionalSheetHandler != null) {
                    processSheet(optionalSheetHandler, styles, strings, sheetStreem);
                    _LOG.info("Excel parser: Parsed {} rows in {}.", optionalSheetHandler.getTotalRows(), iter.getSheetName());
                } else {
                    throw new MemberTemplateParseException(appMessageSource.getMessage(
                        "member.template.messages.excel.sheet.unexpected", new String[]{iter.getSheetName()}, locale));
                }
                sheetStreem.close();
            }
            return memberInfoHandler.getTotalRowData();
        } catch (SAXException se) {
            throw new IOException(se);
        } catch (OpenXML4JException o4je) {
            throw new IOException(o4je);
        } catch (HandlerException e) {
            throw new MemberTemplateParseException(e.getMessage());
        } finally {
            IOUtils.closeQuietly(sheetStreem);
            if (pkg != null) {
                pkg.close();
            }
        }
    }

    /**
     * Processes the given sheet
     */
    public void processSheet(SheetHandler sheetHandler, StylesTable styles, ReadOnlySharedStringsTable strings,
        InputStream sheetInputStream)
        throws IOException, SAXException {

        InputSource sheetSource = new InputSource(sheetInputStream);
        SAXParserFactory saxFactory = SAXParserFactory.newInstance();
        try {
            SAXParser saxParser = saxFactory.newSAXParser();
            XMLReader sheetParser = saxParser.getXMLReader();
            ContentHandler handler = new XlsxSheetHandler(styles, strings, sheetHandler);
            sheetParser.setContentHandler(handler);
            sheetParser.parse(sheetSource);
            sheetHandler.cleanup();
        } catch (ParserConfigurationException e) {
            throw new IOException("SAX parser appears to be broken - " + e.getMessage());
        }
    }

    private SheetHandler instantiateHandler(DateTime currentDateTime, CustomSecurityUserDetailsImpl currentUser, String sheetName,
        Operation operation,
        String memberTypeCode) {
        SheetHandler result = null;
        String key = createKey(operation, memberTypeCode);
        Class<? extends SheetHandler> handlerClass = requiredSheetHandlerClassMap.get(key);
        if (handlerClass != null) {
            try {
                result = handlerClass.getConstructor(DateTime.class, CustomSecurityUserDetailsImpl.class)
                    .newInstance(currentDateTime, currentUser);
                beanFactory.autowireBean(result);
                result = (SheetHandler) beanFactory.initializeBean(result, handlerClass.getName());
                result.setSheetName(sheetName);
            } catch (Exception e) {
                // Should never happen
                throw new GenericServiceException(e);
            }
        }
        return result;
    }

    private SheetHandler instantiateHandler(String sheetName, String memberTypeCode) {
        SheetHandler result = null;
        String key = createKey(sheetName, memberTypeCode);
        Class<? extends SheetHandler> handlerClass = optionalSheetHandlerClassMap.get(key);
        if (handlerClass != null) {
            try {
                result = handlerClass.newInstance();
                beanFactory.autowireBean(result);
                result = (SheetHandler) beanFactory.initializeBean(result, handlerClass.getName());
                result.setSheetName(sheetName);
            } catch (Exception e) {
                // Should never happen
                throw new GenericServiceException(e);
            }
        }
        return result;
    }

    private String createKey(Operation operation, String memberTypeCode) {
        return operation + "-" + memberTypeCode;
    }

    private String createKey(String sheetName, String memberTypeCode) {
        return sheetName + "-" + memberTypeCode;
    }

    public static void main(String[] args) {
        System.out.println(NumberUtils.isNumber("11111.2222"));
    }
}
