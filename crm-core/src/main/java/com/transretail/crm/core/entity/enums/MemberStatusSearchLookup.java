package com.transretail.crm.core.entity.enums;

public enum MemberStatusSearchLookup {
	ACTIVE ( "Active", MemberStatus.ACTIVE ),
    TERMINATED ( "Inactive", MemberStatus.INACTIVE );

	private MemberStatusSearchLookup(String inDesc, MemberStatus inValue) {
		this.desc = inDesc;
		this.value = inValue;
	}

	private final String desc;
    private final MemberStatus value;
	public String getDesc() {
		return desc;
	}
	public MemberStatus getValue() {
		return value;
	}
}
