<%@ include file="../taglibs.jsp" %>

<div class="page-header page-header2">
    <h1><spring:message code="shopping.list" /></h1>
</div>

<c:url value="/member/report/shoppinglist" var="printUrl" />
<a href="${printUrl}" target="_blank" class="pull-left btn btn-print tiptip mb20"><spring:message code="print" /></a>
<br /><br />

<div id="shoppinglist"></div>

<link href="<spring:url value="/css/dataTables.css" />" rel="stylesheet">
<script src="<spring:url value="/js/jquery/jquery.dataTables.js" />"></script>
<script src="<spring:url value="/js/jquery/jquery.dataTables.linkLengthMenu.js" />"></script>
<script src="<spring:url value="/js/jquery/jquery.dataTables.customPaging.js" />"></script>
<script src="<spring:url value="/js/jquery/ajax-datatable.js" />"></script>
<script src="<spring:url value="/js/common/form2js.js" />"></script>
<script src="<spring:url value="/js/common/json2.js" />"></script>
<script src="<spring:url value="/js/jquery/jquery.toObject.js" />"></script>

<script type="text/javascript">
$(document).ready(function() {
	var columnHeaders = [
		'<spring:message code="product.item.id" />',
		'<spring:message code="product.item.name" />',
		'<spring:message code="product.average.bought" />'
	];

	var modelFields = [
		{name: "productId"},
		{name: "productName"},
		{name: "average"}
	];

	$( '#shoppinglist' ).ajaxDataTable( {
      'autoload' : true,
      'ajaxSource' : '<spring:url value="/member/shoppinglist" />',
      'columnHeaders' : columnHeaders,
      'modelFields' : modelFields
	});
});
</script>