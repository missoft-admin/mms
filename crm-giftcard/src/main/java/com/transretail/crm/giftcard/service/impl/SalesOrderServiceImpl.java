package com.transretail.crm.giftcard.service.impl;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.joda.time.DateTime;
import org.joda.time.LocalDate;
import org.joda.time.Months;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.format.number.NumberFormatter;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.mysema.query.BooleanBuilder;
import com.mysema.query.jpa.JPASubQuery;
import com.mysema.query.jpa.impl.JPAQuery;
import com.mysema.query.types.Order;
import com.mysema.query.types.OrderSpecifier;
import com.mysema.query.types.Predicate;
import com.transretail.crm.common.reporting.jasper.ClasspathFileResolver;
import com.transretail.crm.common.reporting.jasper.DefaultJRProcessor;
import com.transretail.crm.common.reporting.jasper.JRProcessor;
import com.transretail.crm.common.service.dto.response.ResultList;
import com.transretail.crm.common.service.util.SpringDataPagingUtil;
import com.transretail.crm.core.repo.UserRepo;
import com.transretail.crm.core.security.model.impl.CustomSecurityUserDetailsImpl;
import com.transretail.crm.core.security.util.UserUtil;
import com.transretail.crm.giftcard.dto.ReturnRecordDto;
import com.transretail.crm.giftcard.dto.SalesOrderDto;
import com.transretail.crm.giftcard.dto.SalesOrderItemDto;
import com.transretail.crm.giftcard.dto.SalesOrderSearchDto;
import com.transretail.crm.giftcard.entity.GiftCardCustomerProfile;
import com.transretail.crm.giftcard.entity.QGiftCardInventory;
import com.transretail.crm.giftcard.entity.QSalesOrder;
import com.transretail.crm.giftcard.entity.QSalesOrderAlloc;
import com.transretail.crm.giftcard.entity.QSalesOrderDeptAlloc;
import com.transretail.crm.giftcard.entity.QSalesOrderHistory;
import com.transretail.crm.giftcard.entity.QSalesOrderItem;
import com.transretail.crm.giftcard.entity.QSalesOrderPaymentInfo;
import com.transretail.crm.giftcard.entity.ReturnRecord;
import com.transretail.crm.giftcard.entity.SalesOrder;
import com.transretail.crm.giftcard.entity.SalesOrderDeptAlloc.DeptAllocStatus;
import com.transretail.crm.giftcard.entity.SalesOrderHistory;
import com.transretail.crm.giftcard.entity.support.PaymentInfoStatus;
import com.transretail.crm.giftcard.entity.support.ReturnStatus;
import com.transretail.crm.giftcard.entity.support.SalesOrderDiscountType;
import com.transretail.crm.giftcard.entity.support.SalesOrderStatus;
import com.transretail.crm.giftcard.entity.support.SalesOrderType;
import com.transretail.crm.giftcard.repo.ReturnRecordRepo;
import com.transretail.crm.giftcard.repo.SalesOrderHistoryRepo;
import com.transretail.crm.giftcard.repo.SalesOrderRepo;
import com.transretail.crm.giftcard.service.DiscountSchemeService;
import com.transretail.crm.giftcard.service.DiscountSchemeService.DiscountDto;
import com.transretail.crm.giftcard.service.ReturnGiftCardService;
import com.transretail.crm.giftcard.service.SalesOrderService;

@Service("salesOrderService")
public class SalesOrderServiceImpl implements SalesOrderService {

	private static final Logger LOG = LoggerFactory.getLogger(SalesOrderServiceImpl.class);

	private SalesOrderRepo orderRepo;
	private DiscountSchemeService discountService;
	private ReturnRecordRepo returnRepo;
	private ReturnGiftCardService returnService;
	private UserRepo userRepo;

	@PersistenceContext
	private EntityManager em;

	public static String BB_NO_PREPEND = "BBMMS";
	public static String BBADV_NO_PREPEND = "BBADVMMS";
	public static String YD_NO_PREPEND = "YDMMS";
	public static String PO_NO_PREPEND = "POMMS";
	public static String RP_NO_PREPEND = "BBRMMS";
	public static String FV_NO_PREPEND = "FVMMS";

	@Autowired
	private SalesOrderHistoryRepo soHistoryRepo;

	@Autowired
	public void setOrderRepo(SalesOrderRepo orderRepo) {
		this.orderRepo = orderRepo;
	}

	@Autowired
	public void setDiscountService(DiscountSchemeService discountService) {
		this.discountService = discountService;
	}

	@Autowired
	public void setReturnRepo(ReturnRecordRepo returnRepo) {
		this.returnRepo = returnRepo;
	}

	@Autowired
	public void setReturnService(ReturnGiftCardService returnService) {
		this.returnService = returnService;
	}

	@Autowired
	public void setUserRepo(UserRepo userRepo) {
		this.userRepo = userRepo;
	}

	@Override
	public BigDecimal getTotalFaceAmount(SalesOrderDto orderDto) {
		BigDecimal totalFaceAmt = BigDecimal.ZERO;
		for (SalesOrderItemDto item : orderDto.getItems()) {
			if (item.getFaceAmount() != null)
				totalFaceAmt = totalFaceAmt.add(item.getFaceAmount());
		}

		LOG.info("total face amount :: " + totalFaceAmt);
		return totalFaceAmt;
	}

	@Override
	public BigDecimal getTotalFaceAmount(Long orderId) {
		QSalesOrderItem qItem = QSalesOrderItem.salesOrderItem;
		return new JPAQuery(em).from(qItem).where(qItem.order.id.eq(orderId)).singleResult(qItem.faceAmount.sum());
	}

	@Override
	@Transactional(readOnly = true)
	public SalesOrderDto getOrderByOrderNo(String orderNo) {
		QSalesOrder qOrder = QSalesOrder.salesOrder;
		SalesOrder order = orderRepo.findOne(qOrder.orderNo.eq(orderNo));
		if (order != null) {
			LOG.info("order find :: " + order.toString());
			return new SalesOrderDto(order, true);
		}

		return null;
	}

	@Override
	@Transactional
	public Long saveOrder(SalesOrderDto orderDto) {

		if (orderDto.getOrderType().compareTo(SalesOrderType.REPLACEMENT) == 0) {
			ReturnRecordDto retRec = returnService.getReturnByRecordNo(orderDto.getReturnNo());
			if (retRec != null) {
				orderDto.setCustomerId(retRec.getCustomerId());
				orderDto.setContactPerson(retRec.getContactPerson());
				orderDto.setContactNumber(retRec.getContactNumber());
				orderDto.setContactEmail(retRec.getContactEmail());
				orderDto.setPaymentStore(retRec.getPaymentStore());
				orderDto.setReturnId(retRec.getId());
			}
		}

		SalesOrderType types = orderDto.getOrderType();
		if (types.equals(SalesOrderType.INTERNAL_ORDER) || types.equals(SalesOrderType.VOUCHER) || types.equals(SalesOrderType.YEARLY_DISCOUNT)) {
			String storeCode = userRepo.findByUsername(getcurrentPrincipal()).getStoreCode();
			orderDto.setPaymentStore(storeCode);
		}

		SalesOrder order = new SalesOrder();
		if (orderDto.getId() != null) {
			order = orderRepo.findOne(orderDto.getId());
		}

		if (StringUtils.isBlank(orderDto.getOrderNo())) {
			orderDto.setOrderNo(generateOrderNo(orderDto));
		}

		SalesOrder so = orderRepo.save(orderDto.toModel(order));
		LOG.info("so save :: " + so.toString());

		SalesOrderHistory soHistory = new SalesOrderHistory();
		soHistory.setOrder(order);
		soHistory.setStatus(SalesOrderStatus.FOR_APPROVAL);
		soHistory.setCreated(new DateTime());
		soHistory.setCreateUser(UserUtil.getCurrentUser().getFullName());
		soHistoryRepo.saveAndFlush(soHistory);
		return so.getId();
	}

	private String getcurrentPrincipal() {

		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		return auth.getName();
	}

	@Override
	@Transactional
	public Long save(SalesOrder order) {
		/*if( order.getOrderType().compareTo( SalesOrderType.REPLACEMENT ) == 0 ) {
			ReturnRecordDto retRec = returnService.getReturnByRecordNo( order.getReturnNo() );
			order.setCustomer( new GiftCardCustomerProfile( retRec.getCustomerId() ) );
			order.setContactPerson( retRec.getContactPerson() );
			order.setContactNumber( retRec.getContactNumber() );
			order.setContactEmail( retRec.getContactEmail() );
			order.setPaymentStore( retRec.getPaymentStore() );
		
			BigDecimal totalFaceAmount = BigDecimal.ZERO;
			if( CollectionUtils.isNotEmpty( order.getItems() ) ) {
				BigDecimal totalFaceAmt = BigDecimal.ZERO;
				for( SalesOrderItem item: order.getItems() ) {
					if( item != null && item.getProduct() != null && item.getFaceAmount() != null )
						totalFaceAmt = totalFaceAmt.add( item.getFaceAmount() );
				}
				totalFaceAmount = totalFaceAmt;
			}
			if( BooleanUtils.isNotTrue( order.getPaidReplacement() ) ) {
				order.setHandlingFee( retRec.getRemainingReturnAmount().subtract( totalFaceAmount ) );
			}
		}*/
		SalesOrder so = orderRepo.saveAndFlush(order);

		return so.getId();
	}

	@Override
	@Transactional(readOnly = true)
	public ResultList<SalesOrderDto> getSalesOrders(SalesOrderSearchDto searchForm) {
		CustomSecurityUserDetailsImpl userDetails = UserUtil.getCurrentUser();
		Set<String> userPerms = userDetails.getPermissions();
		BooleanBuilder permsFilter = new BooleanBuilder();
		List<SalesOrderStatus> status = Lists.newArrayList();
		QSalesOrder qOrder = QSalesOrder.salesOrder;
		if (!userPerms.contains("GC_SALES_ORDER")) {
			if (userPerms.contains("GC_SO_VERIFY_PAYMENT"))
				status.add(SalesOrderStatus.FOR_APPROVAL);
			if (userPerms.contains("GC_SO_APPROVE_PAYMENT"))
				status.add(SalesOrderStatus.PAYMENT_APPROVAL);
			permsFilter.and(qOrder.status.in(status));
		}
		if(!userDetails.getStore().getName().contains("HEAD"))
			permsFilter.and(qOrder.paymentStore.eq(userDetails.getStoreCode()));
			
		Pageable pageable = SpringDataPagingUtil.INSTANCE.createPageable(searchForm.getPagination());
		permsFilter.and(searchForm.createSearchExpression());
		Page<SalesOrder> page = permsFilter != null ? orderRepo.findAll(permsFilter, pageable) : orderRepo.findAll(pageable);
		return new ResultList<SalesOrderDto>(toDto(page.getContent()), page.getTotalElements(), page.hasPreviousPage(), page.hasNextPage());
	}

	@Override
	@Transactional(readOnly = true)
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public List<SalesOrderDto> getSalesOrdersForPrint(SalesOrderSearchDto searchForm) {
		CustomSecurityUserDetailsImpl userDetails = UserUtil.getCurrentUser();
		Set<String> userPerms = userDetails.getPermissions();
		BooleanBuilder permsFilter = new BooleanBuilder();
		List<SalesOrderStatus> status = Lists.newArrayList();
		QSalesOrder qOrder = QSalesOrder.salesOrder;
		if (!userPerms.contains("GC_SALES_ORDER")) {
			if (userPerms.contains("GC_SO_VERIFY_PAYMENT"))
				status.add(SalesOrderStatus.FOR_APPROVAL);
			if (userPerms.contains("GC_SO_APPROVE_PAYMENT"))
				status.add(SalesOrderStatus.PAYMENT_APPROVAL);
			permsFilter.and(qOrder.status.in(status));
		}
		Predicate predicate = permsFilter.and(searchForm.createSearchExpression()).getValue();
		QSalesOrder qs = QSalesOrder.salesOrder;
		List<SalesOrder> orders = Lists.newArrayList(orderRepo.findAll(predicate, new OrderSpecifier(Order.DESC, qs.lastUpdated)));
		return toDto(orders);
	}

	private List<SalesOrderDto> toDto(List<SalesOrder> orders) {
		List<SalesOrderDto> orderDtos = Lists.newArrayList();
		for (SalesOrder order : orders) {
			SalesOrderDto orderDto = new SalesOrderDto(order, false);
			if (orderDto.getStatus().compareTo(SalesOrderStatus.FOR_APPROVAL) == 0) {
				orderDto.setReadOnly(isForVerificationSOReadOnly(orderDto.getId()));
			}
			if (orderDto.getStatus().compareTo(SalesOrderStatus.FOR_ACTIVATION) == 0) {
				if (orderDto.getOrderType().compareTo(SalesOrderType.INTERNAL_ORDER) == 0) {
					orderDto.setReadOnly(isForApprovalInternalSOReadOnly(orderDto.getId()));
				} else {
					orderDto.setReadOnly(isForApprovalSOReadOnly(orderDto.getId()));
				}
			}
			orderDtos.add(orderDto);
		}
		return orderDtos;
	}

	private boolean isForVerificationSOReadOnly(Long id) {
		QSalesOrderPaymentInfo qPaymentInfo = QSalesOrderPaymentInfo.salesOrderPaymentInfo;
		return new JPAQuery(em).from(qPaymentInfo).where(qPaymentInfo.order.id.eq(id).and(qPaymentInfo.status.ne(PaymentInfoStatus.VERIFIED))).exists();
	}

	private boolean isForApprovalSOReadOnly(Long id) {
		QSalesOrderPaymentInfo qPaymentInfo = QSalesOrderPaymentInfo.salesOrderPaymentInfo;
		return new JPAQuery(em).from(qPaymentInfo).where(qPaymentInfo.order.id.eq(id).and(qPaymentInfo.status.ne(PaymentInfoStatus.APPROVED))).exists();
	}

	private boolean isForApprovalInternalSOReadOnly(Long id) {
		QSalesOrderDeptAlloc qAlloc = QSalesOrderDeptAlloc.salesOrderDeptAlloc;
		return new JPAQuery(em).from(qAlloc).where(qAlloc.order.id.eq(id).and(qAlloc.status.ne(DeptAllocStatus.APPROVED))).exists();
	}

	@Override
	@Transactional(readOnly = true)
	public SalesOrderDto getOrder(Long id) {
		return new SalesOrderDto(orderRepo.findOne(id), true);
	}

	@Override
	@Transactional(readOnly = true)
	public SalesOrder get(Long id) {
		return orderRepo.findOne(id);
	}

	@Override
	@Transactional
	public void deleteOrder(Long id) {
		orderRepo.delete(id);
	}

	@Override
	@Transactional
	public void processReplacement(Long id) {
		SalesOrderDto orderDto = getOrder(id);
		if (orderDto.getStatus().compareTo(SalesOrderStatus.APPROVED) == 0 && orderDto.getOrderType().compareTo(SalesOrderType.REPLACEMENT) == 0) {
			ReturnRecord retRec = returnRepo.findByRecordNo(orderDto.getReturnNo());
			BigDecimal replaceAmt = orderDto.getTotalFaceAmount();
			if (orderDto.getHandlingFee() != null)
				replaceAmt = replaceAmt.add(orderDto.getHandlingFee());
			retRec.setReplaceAmount(replaceAmt);

			if (CollectionUtils.isNotEmpty(retRec.getItems())) {
				retRec.setStatus(ReturnStatus.FOR_REPLACEMENT);
			}
			returnRepo.save(retRec);

		}
	}

	@Override
	@Transactional
	public void processDiscount(Long id, SalesOrderStatus status) {
		if (status.compareTo(SalesOrderStatus.APPROVED) == 0) {
			SalesOrder order = orderRepo.findOne(id);
			if (order.getOrderType().compareTo(SalesOrderType.B2B_SALES) == 0
					|| order.getOrderType().compareTo(SalesOrderType.B2B_ADV_SALES) == 0
					|| order.getOrderType().compareTo(SalesOrderType.REPLACEMENT) == 0) {
				DiscountDto discount = getDiscount(new SalesOrderDto(order, true), order.getDiscType());
				if (discount != null) {
					order.setDiscountVal(discount.getDiscount());
					order.setVoucherVal(discount.getVoucher());
					if (discount.getVoucher() != null) {
						if (discount.isGeneral()) {
							createVoucher(discount, order);
						} else {
							createYearlyDiscount(discount, order);
						}
					}
				}
				orderRepo.save(order);
			}
		}
	}

	@Transactional
	public void createVoucher(DiscountDto discount, SalesOrder salesOrder) {
		SalesOrder order = new SalesOrder();
		order.setOrderType(SalesOrderType.VOUCHER);
		order.setOrderDate(new LocalDate());
		order.setCustomer(new GiftCardCustomerProfile(salesOrder.getCustomer().getId()));
		order.setContactPerson(salesOrder.getContactPerson());
		order.setContactEmail(salesOrder.getContactEmail());
		order.setContactNumber(salesOrder.getContactNumber());
		order.setStatus(SalesOrderStatus.DRAFT);
		order.setPaymentStore(salesOrder.getPaymentStore());
		order.setParent(salesOrder);
		String notes = "Voucher generated "
				+ discount.getVoucher()
				+ "% of Rp"
				+ new NumberFormatter("#,##0").print(discount.getPurchase(), LocaleContextHolder.getLocale())
				+ " from Sales Order #"
				+ salesOrder.getOrderNo(); //TODO

		order.setNotes(notes);
		orderRepo.save(order);
	}

	@Transactional
	public void createYearlyDiscount(DiscountDto discount, SalesOrder salesOrder) {
		SalesOrder order = new SalesOrder();
		order.setOrderType(SalesOrderType.YEARLY_DISCOUNT);
		order.setOrderDate(new LocalDate());
		order.setCustomer(new GiftCardCustomerProfile(salesOrder.getCustomer().getId()));
		order.setContactPerson(salesOrder.getContactPerson());
		order.setContactEmail(salesOrder.getContactEmail());
		order.setContactNumber(salesOrder.getContactNumber());
		order.setStatus(SalesOrderStatus.DRAFT);
		order.setParent(salesOrder);

		String notes = "Generated by discount scheme "
				+ discount.getVoucher() + "% of Rp"
				+ new NumberFormatter("#,##0").print(discount.getPurchase(), LocaleContextHolder.getLocale())
				+ "  from Sales Order #" + salesOrder.getOrderNo(); //TODO

		order.setNotes(notes);
		orderRepo.save(order);
	}

	@Override
	public JRProcessor createJrProcessor(SalesOrderSearchDto searchDto) {
		Map<String, Object> parameters = Maps.newHashMap();
		List<SalesOrderDto> products = getSalesOrdersForPrint(searchDto);
		if (products.isEmpty()) {
			parameters.put("isEmpty", true);
			products.add(new SalesOrderDto());
		}
		DefaultJRProcessor jrProcessor = new DefaultJRProcessor("reports/sales_order.jasper", products);
		jrProcessor.addParameters(parameters);
		jrProcessor.setFileResolver(new ClasspathFileResolver("reports"));
		return jrProcessor;
	}

	@Override
	@Transactional(readOnly = true)
	public DiscountDto getDiscount(SalesOrderDto order, SalesOrderDiscountType discType) {
		BigDecimal purchase = BigDecimal.ZERO;
		for (SalesOrderItemDto item : order.getItems()) {
			purchase = purchase.add(item.getFaceAmount());
		}
		return discountService.getDiscount(order.getCustomerId(), purchase, order.getOrderDate(), discType);
	}

	@Override
	@Transactional
	public void approveOrder(Long id, SalesOrderStatus status) {
		SalesOrder order = orderRepo.findOne(id);
		/*if(status.compareTo(SalesOrderStatus.APPROVED) == 0) {
			order.setOrderNo(generateOrderNo(order));
		}*/
		order.setStatus(status);

		if (SalesOrderStatus.APPROVED.compareTo(status) == 0)
			order.setApprovalDate(new LocalDate());

		LocalDate pymntDate = getEarliestPaymentDate(order.getId());
		if (SalesOrderType.B2B_SALES.compareTo(order.getOrderType()) == 0
				&& pymntDate != null) {
			int diff = Months.monthsBetween(pymntDate.withDayOfMonth(1), order.getOrderDate().withDayOfMonth(1)).getMonths();
			if (diff > 0)
				order.setOrderType(SalesOrderType.B2B_ADV_SALES);
		}

		orderRepo.save(order);
	}

	@Transactional
	public String generateOrderNo(SalesOrderDto order) {

		final String SO = "SO";
		final String IO = "IO";
		final String RM = "RM";
		final String YL = "YL";
		final String VC = "VC";

		QSalesOrder qOrder = QSalesOrder.salesOrder;
		LocalDate monthBegin = order.getOrderDate().withDayOfMonth(1);
		LocalDate monthEnd = order.getOrderDate().plusMonths(1).withDayOfMonth(1).minusDays(1);

		String prepend = "";
		if ((order.getOrderType().equals(SalesOrderType.B2B_ADV_SALES)) || (order.getOrderType().equals(SalesOrderType.B2B_SALES))) {
			prepend = SO;
		} else if (order.getOrderType().equals(SalesOrderType.INTERNAL_ORDER)) {
			prepend = IO;
		} else if (order.getOrderType().equals(SalesOrderType.REPLACEMENT)) {
			prepend = RM;
		} else if (order.getOrderType().equals(SalesOrderType.YEARLY_DISCOUNT)) {
			prepend = YL;
		} else {
			prepend = VC;
		}

		//BBYYMMindex

		/* String prepend = "";
		if(order.getOrderType().compareTo(SalesOrderType.B2B_SALES) == 0) {
			LocalDate pymntDate = getEarliestPaymentDate(order.getId());
			if(pymntDate != null && pymntDate.isBefore(order.getOrderDate().minusMonths(1)))
				prepend = BBADV_NO_PREPEND;
			else
				prepend = BB_NO_PREPEND;
		} else if(order.getOrderType().compareTo(SalesOrderType.YEARLY_DISCOUNT) == 0) {
			prepend = YD_NO_PREPEND;
		} else if(order.getOrderType().compareTo(SalesOrderType.INTERNAL_ORDER) == 0) {
			prepend = PO_NO_PREPEND;
		} else if(order.getOrderType().compareTo(SalesOrderType.REPLACEMENT) == 0) {
			prepend = RP_NO_PREPEND;
		} else if(order.getOrderType().compareTo(SalesOrderType.VOUCHER) == 0) {
			prepend = FV_NO_PREPEND;
		}*/

		/*if(order.getOrderType().compareTo(SalesOrderType.REPLACEMENT) == 0) {
			String origOrderNo = order.getReturnRecord().getOrderNo().getOrderNo();
			String prefix = origOrderNo.substring(origOrderNo.length() - 7);
			String newOrderNo = prepend + prefix;
			long count = orderRepo.count(qOrder.orderNo.startsWith(newOrderNo));
			return count > 0 ? newOrderNo + "-" + count : newOrderNo;
		}*/

		DateTimeFormatter formatter = DateTimeFormat.forPattern("yyMM");
		String prefix = formatter.print(order.getOrderDate());

		Predicate fil = qOrder.orderDate.goe(monthBegin)
				.and(qOrder.orderDate.loe(monthEnd))
				.and(qOrder.status.isNotNull())
				.and(qOrder.orderNo.indexOf(prefix).eq(2))
				/*.and(qOrder.status.notIn(invalidStats))
				.and(qOrder.orderType.eq(order.getOrderType()))*/
				.and(qOrder.orderNo.startsWith(prepend));

		String lastOrdNo = new JPAQuery(em).from(qOrder).where(fil).singleResult(qOrder.orderNo.max());

		int count = 0;
		if (StringUtils.isNotBlank(lastOrdNo))
			count = Integer.parseInt(lastOrdNo.substring(lastOrdNo.length() - 3)) + 1;

		String suffix = String.format("%03d", count);

		/*String generated = prepend + prefix + suffix;
		String old = order.getOrderNo();
		String genSub = generated.substring(0, generated.length() - 3);
		String oldSub = old.substring(0, old.length() - 3);
		
		if(genSub.equals(oldSub))
			return old;*/

		/* String id = prepend + prefix + suffix;
		if (orderRepo.findTopByOrderNoOrderByOrderNoDesc(id)){
		    suffix = String.format("%03d", count + 1);
		}*/

		String id = prepend + prefix + suffix;
		LOG.info("ID will be given :: " + id);

		return id;
	}

	private LocalDate getEarliestPaymentDate(Long id) {
		QSalesOrderPaymentInfo qPaymentInfo = QSalesOrderPaymentInfo.salesOrderPaymentInfo;
		return new JPAQuery(em).from(qPaymentInfo).where(qPaymentInfo.order.id.eq(id)).singleResult(qPaymentInfo.paymentDate.min());
	}

	@Override
	public boolean isSalesOrderExist(Long id) {
		SalesOrderDto dto = getOrder(id);
		return dto != null;
	}

	@Override
	public boolean isValidForExtension(Long id) {
		QSalesOrderAlloc qAlloc = QSalesOrderAlloc.salesOrderAlloc;
		QGiftCardInventory qGc = QGiftCardInventory.giftCardInventory;
		//return BooleanUtils.negate(new JPAQuery(em).from(qAlloc).where(qAlloc.soItem.order.id.eq(id).and(qAlloc.gcInventory.expiryDate.isNull())).exists());
		return new JPAQuery(em).from(qAlloc).where(qAlloc.soItem.order.id.eq(id).and(
				new JPASubQuery().from(qGc).where(qGc.series.goe(qAlloc.seriesStart)
						.and(qGc.series.goe(qAlloc.seriesStart)).and(qGc.expiryDate.isNull())).exists()))
				.exists();

		/*boolean isValid = true;
		SalesOrder salesOrder = orderRepo.findOne(id);
		for (SalesOrderItem soItem : salesOrder.getItems()) {
		    for (SalesOrderAlloc salesOrderAlloc : soItem.getSoAlloc()) {
		        if (salesOrderAlloc.getGcInventory().getExpiryDate() == null) 
		            isValid = false;
		    }
		}
		return isValid;*/
	}

	@Override
	public List<SalesOrderHistory> getHistories(Long id) {
		QSalesOrderHistory qHistory = QSalesOrderHistory.salesOrderHistory;
		return new JPAQuery(em).from(qHistory).where(qHistory.order.id.eq(id)).list(qHistory);
	}

	@Override
	@Transactional
	public void processHistory(Long id, SalesOrderStatus stat) {
		SalesOrder salesOrder = orderRepo.findOne(id);
		if (salesOrder != null) {
			SalesOrderHistory soHistory = new SalesOrderHistory();
			soHistory.setOrder(salesOrder);
			soHistory.setStatus(stat);
			soHistory.setCreated(new DateTime());
			soHistory.setCreateUser(UserUtil.getCurrentUser().getFullName());
			soHistoryRepo.save(soHistory);
		}
	}

	public static class GcSoSummaryBean {
		private String serial;
		private String storeCode;
		private String storeName;
		private BigDecimal totalFaceAmount;
		private BigDecimal totalActualSales;
		private Float salesRate;
		private List<MonthlySummary> monthSummaries;

		public GcSoSummaryBean(
				String serial,
				String storeCode,
				String storeName,
				BigDecimal totalFaceAmount,
				BigDecimal totalActualSales,
				Float salesRate,
				List<MonthlySummary> monthSummaries) {
			this.serial = serial;
			this.storeCode = storeCode;
			this.storeName = storeName;
			this.totalFaceAmount = totalFaceAmount;
			this.totalActualSales = totalActualSales;
			this.salesRate = salesRate;
			this.monthSummaries = monthSummaries;
		}

		public static class MonthlySummary {
			private LocalDate date;
			private BigDecimal faceAmount;
			private BigDecimal actualSales;

			public MonthlySummary(LocalDate date, BigDecimal faceAmount, BigDecimal actualSales) {
				this.date = date;
				this.faceAmount = faceAmount;
				this.actualSales = actualSales;
			}

			public LocalDate getDate() {
				return date;
			}

			public BigDecimal getFaceAmount() {
				return faceAmount;
			}

			public BigDecimal getActualSales() {
				return actualSales;
			}
		}

		public String getSerial() {
			return serial;
		}

		public String getStoreCode() {
			return storeCode;
		}

		public String getStoreName() {
			return storeName;
		}

		public BigDecimal getTotalFaceAmount() {
			return totalFaceAmount;
		}

		public BigDecimal getTotalActualSales() {
			return totalActualSales;
		}

		public Float getSalesRate() {
			return salesRate;
		}

		public List<MonthlySummary> getMonthSummaries() {
			return monthSummaries;
		}
	}

	public static class CrosstabBean {

		private LocalDate date;
		private String column;
		private String row;
		private BigDecimal value;

		public CrosstabBean() {
		}

		public CrosstabBean(LocalDate date, String column, String row, BigDecimal value) {
			this.date = date;
			this.column = column;
			this.row = row;
			this.value = value;
		}

		public CrosstabBean(LocalDate date, String column, BigDecimal value) {
			this(date, column, "", value);
		}

		public LocalDate getDate() {
			return date;
		}

		public BigDecimal getValue() {
			return value;
		}

		public String getColumn() {
			return column;
		}

		public String getRow() {
			return row;
		}
	}

}
