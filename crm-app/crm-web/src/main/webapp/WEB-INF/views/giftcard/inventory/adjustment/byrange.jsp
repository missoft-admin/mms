<%@ include file="../../../common/taglibs.jsp" %>
<div class="modal hide  nofly" id="byRangeDialog" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-header">
      <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
      <h4><spring:message code="physical_count_by_series_range_label"/></h4>
    </div>
    <div class="modal-content">
      <div class="modal-body">
	<div id="contentError" class="hide alert alert-error">
	  <button type="button" class="close" onclick="$(this).parent().hide();">&times;</button>
	  <div>
	    <form:errors path="*"/>
	  </div>
	</div>
	<form:form id="byRangeForm" name="byRangeForm" 
		   modelAttribute="giftCardPhysicalCountForm" 
		   action="${pageContext.request.contextPath}/giftcard/inventory/adjustment/adjust" 
		   method="POST" cssClass="form-reset form-horizontal" >
	  <div class="control-group">
	    <spring:message code="inventory_prop_location" var="locationStr" />
	    <label for="storeName" class="control-label">${locationStr}</label> 
	    <div class="controls">
	      <sec:authentication var="userLocation" property="principal.inventoryLocation" />
	      <sec:authentication var="userLocationName" property="principal.inventoryLocationName" />
	      <input type="text" name="locationDesc" value="${userLocation} - ${userLocationName}" class="form-control" placeholder="${locationStr}" disabled="disabled" />
	      <input type="hidden" name="location" value="${userLocation}" />
	    </div>
	  </div>
	  <div class="control-group">
	    <spring:message code="mo_tier_prop_startingseries" var="startingSeriesStr" />
	    <label for="storeName" class="control-label">${startingSeriesStr}</label> 
	    <div class="controls">
	      <form:input path="startingSeries" maxlength="16" class="form-control" placeholder="${startingSeriesStr}" />
	    </div>
	  </div>
	  <div class="control-group">
	    <spring:message code="mo_tier_prop_endingseries" var="endingSeriesStr" />
	    <label for="storeName" class="control-label">${endingSeriesStr}</label> 
	    <div class="controls">
	      <form:input path="endingSeries" maxlength="16" class="form-control" placeholder="${endingSeriesStr}" />
	    </div>
	  </div>
	  <div class="control-group">
	    <spring:message code="gc.inventory.product.profile.label" var="cardTypeStr" />
	    <label for="storeName" class="control-label">${cardTypeStr}</label> 
	    <div class="controls">
	      <input type="text" name="productProfileName" class="form-control" placeholder="${cardTypeStr}" disabled="disabled" />
	      <input type="hidden" name="productProfile"/>
	    </div>
	  </div>
	  <div class="control-group">
	    <spring:message code="mo_tier_prop_quantity" var="quantityStr" />
	    <label for="quantity" class="control-label">${quantityStr}</label> 
	    <div class="controls">
	      <form:input path="quantity" class="form-control" readonly="true" placeholder="${quantityStr}" />
	    </div>
	  </div>
	</form:form>
      </div>
      <div class="modal-footer">
	<spring:message code="label_save" var="btnLabel"/>
        <button type="button" id="countSubmit" data-loading-text="Saving..."  data-mylabel="${btnLabel}" class="btn btn-primary">${btnLabel}</button>
        <button type="button" id="countCancel" class="btn btn-default" data-dismiss="modal"><spring:message code="label_stop"/></button>
      </div>
    </div>
  </div>
</div>