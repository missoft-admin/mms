-- Delete duplicate inventory with the same name and mo status is not approved.
DELETE FROM CRM_MO_TIER t
WHERE
NOT EXISTS (SELECT m.MO_NO FROM CRM_MO m where m.id = t.MO_NO and m.status = 'MOST003')
AND t.ID not in
(SELECT MIN(tier.ID)
FROM CRM_MO_TIER tier inner join CRM_MO mo on tier.MO_NO = mo.id WHERE mo.status != 'MOST003'
GROUP BY tier.MO_NO,tier.NAME)