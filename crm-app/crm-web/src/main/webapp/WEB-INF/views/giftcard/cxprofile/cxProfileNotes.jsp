<%@include file="../../common/taglibs.jsp"%>


<div id="content_cxProfileNotes" class="modal hide nofly modal-dialog content_cxProfile">

  <div class="modal-content">
  <c:url var="url_action" value="/" />
  <form:form id="cxProfileNotes" name="cxProfileNotes" modelAttribute="cxProfileNotes" method="POST" action="${url_action}" class="modal-form form-horizontal">

    <div class="modal-header">
      <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
      <h4><span id="cxProfileName"></span> - <spring:message code="gc.cxprofile.notes" /></h4>
    </div>


    <div class="modal-body">
      <div class="hide alert alert-error" id="content_error"><button type="button" class="close" onclick="$(this).parent().hide();">&times;</button><div><form:errors   path="*"/></div></div>

      <div class="main-block">
        <div class="control-group">
				  <div id="list_cxProfileNotes" 
				      data-url="<c:url value="/giftcard/customer/profile/notes/list/%ID%" />" 
				      data-hdr-created="<spring:message code="gc.cxprofile.notes.note.created"/>" 
				      data-hdr-notes="<spring:message code="gc.cxprofile.notes.note"/>" ></div>
        </div>


        <div class="control-group">
          <label for="" class="control-label"><spring:message code="gc.cxprofile.notes.note" /></label>
          <div class="controls"><textarea id="customerNotes" placeholder="<spring:message code="gc.cxprofile.notes" />" ></textarea></div>
        </div>
      </div>
    </div>


    <div class="modal-footer">
      <button id="saveBtn" class="btn btn-primary" type="submit" 
        data-url="<c:url value="/giftcard/customer/profile/notes/save/%ID%/%NOTES%" />" ><spring:message code="gc.cxprofile.notes.note.save" /></button>
      <button  id="cancelBtn" class="btn" type="button" data-dismiss="modal"><spring:message code="label_cancel" /></button>
    </div>

  </form:form>
  </div>


<style type="text/css"> 
<!-- 
  .content_cxProfile .modal-footer { text-align: center; } 
  .content_cxProfile .control-group .controls label { margin-top: 5px; }
--> 
</style>
<link href="<c:url value="/css/bootstrap/datepicker.css" />" rel="stylesheet" />
<script src='<c:url value="/js/bootstrap/bootstrap-datepicker.js"/>' type='text/javascript'><![CDATA[&nbsp; ]]></script>
<script type='text/javascript'>
var CxProfileNotes = null, CxProfileNotesList = null;

$(document).ready( function() {

    var errorContainer = $( "#content_cxProfileNotes" ).find( "#content_error" );
    var $dialog = $( "#content_cxProfileNotes" );
    var $form = $dialog.find( "#cxProfileNotes" );
    var $cxProfileName = $dialog.find( "#cxProfileName" );
    var $cxNotes = $dialog.find( "#customerNotes" );
    var $save = $dialog.find( "#saveBtn" );
    var $list = $dialog.find( "#list_cxProfileNotes" );

    initDialog();

    function initDialog() {
        $dialog.modal({ show : false });
        $dialog.on( "hide", function(e) { 
            if ( e.target === this ) {
                errorContainer.hide();
                $cxNotes.val( "" );
                $cxProfileName.html( "" );
                $list.html( "" );
            }
        });
    }

    function initFormFields() {
        $save.unbind( "click" ).click( function(e) {
            e.preventDefault();
            $save.prop( "disabled", true );

            if ( $cxNotes.val() ) {
                var url = $(this).data( "url" )
                    .replace( new RegExp( "%ID%", "g" ), $list.data( "id" ) )
                    .replace( new RegExp( "%NOTES%", "g" ), $cxNotes.val() );
                $.post( url, function( resp ) {
                    $save.prop( "disabled", false );
                    if ( resp.success ) {
                        $cxNotes.val( "" );
                        CxProfileNotesList.reloadTable();
                    }
                });
            }
        });
    }

    function initDataTable() {
        $list.ajaxDataTable({
            'autoload'      : true,
            'ajaxSource'    : $list.data( "url" ).replace( new RegExp( "%ID%", "g" ), $list.data( "id" ) ),
            'aaSorting'     : [[ 0, 'desc' ]],
            'columnHeaders' : [ 
                $list.data( "hdr-created" ),
                $list.data( "hdr-notes" )
            ],
            'modelFields'  : [
                { name  : 'created', sortable : true },
                { name  : 'remarks', sortable : false }
            ]
        });
        CxProfileNotesList = {
            reloadTable  : function() { $list.ajaxDataTable( "search" ); }
        };
    }

    CxProfileNotes = {
        show  : function( id, cxname ) { 
            $list.data( "id", id );
            $cxProfileName.html( cxname );
            initDataTable();
            initFormFields();
            $dialog.modal( "show" ); 
        }
    };

});
</script>


</div>