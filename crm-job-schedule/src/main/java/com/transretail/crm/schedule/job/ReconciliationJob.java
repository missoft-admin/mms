package com.transretail.crm.schedule.job;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.MessageSource;
import org.springframework.context.NoSuchMessageException;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.scheduling.quartz.QuartzJobBean;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.client.RestTemplate;

import com.transretail.crm.common.service.dto.request.PagingParam;
import com.transretail.crm.common.service.dto.response.ResultList;
import com.transretail.crm.common.service.exception.MessageSourceResolvableException;
import com.transretail.crm.core.dto.MessageDto;
import com.transretail.crm.core.entity.ApplicationConfig;
import com.transretail.crm.core.entity.CrmInStore;
import com.transretail.crm.core.entity.enums.AppKey;
import com.transretail.crm.core.repo.ApplicationConfigRepo;
import com.transretail.crm.core.repo.CrmInStoreRepo;
import com.transretail.crm.core.service.impl.CrmFileServiceImpl;
import com.transretail.crm.media.service.MailService;
import com.transretail.crm.reconciliation.service.ReconciliationService;
import com.transretail.crm.rest.pos.response.dto.GenericResponseBean;
import com.transretail.crm.rest.pos.response.dto.MissingEmpTxnBean;
import com.transretail.crm.rest.pos.response.dto.MissingPointsBean;
import com.transretail.crm.rest.request.MissingEmpTxnSearchDto;
import com.transretail.crm.rest.request.MissingPointsSearchDto;

/**
 * @author Allan G. Ramirez (agramirez@exist.com)
 */
public class ReconciliationJob extends QuartzJobBean implements InitializingBean {
    private static final Logger _LOG = LoggerFactory.getLogger(ReconciliationJob.class);

    private ReconciliationService reconciliationService;
    private CrmInStoreRepo crmInStoreRepo;
    private MessageSource messageSource;
    private RestTemplate restTemplate;
    private Integer crmWebPort;
    private String crmWebContextPath;
    private String crmWebRootUrl;

    private MailService mailService;
    @Autowired
    private ApplicationConfigRepo applicationConfigRepo;

    @Autowired
    public void setReconciliationService(ReconciliationService reconciliationService) {
        this.reconciliationService = reconciliationService;
    }

    @Autowired
    private void setCrmInStoreRepo(CrmInStoreRepo crmInStoreRepo) {
        this.crmInStoreRepo = crmInStoreRepo;
    }

    @Autowired
    public void setMessageSource(MessageSource messageSource) {
        this.messageSource = messageSource;
    }

    @Autowired
    public void setRestTemplate(RestTemplate restTemplate) {
        this.restTemplate = restTemplate;
    }

    @Value("#{'${crm.web.port}'}")
    public void setCrmWebPort(Integer crmWebPort) {
        this.crmWebPort = crmWebPort;
    }

    @Value("#{'${crm.web.contextpath}'}")
    public void setCrmWebContextPath(String crmWebContextPath) {
        this.crmWebContextPath = crmWebContextPath;
    }

    @Override
    public void afterPropertiesSet() throws Exception {
        crmWebRootUrl = "http://localhost:" + crmWebPort + crmWebContextPath;
    }

    @Autowired
    public void setMailService(MailService mailService) {
        this.mailService = mailService;
    }

    @Override
    @Transactional(readOnly = true)
    protected void executeInternal(JobExecutionContext context) throws JobExecutionException {
        List<CrmInStore> crmInStores = crmInStoreRepo.findAll();

        MissingPointsSearchDto pointsSearchDto = new MissingPointsSearchDto();
        MissingEmpTxnSearchDto empTxnSearchDto = new MissingEmpTxnSearchDto();

        StringBuilder message = new StringBuilder();
        StringBuilder content = new StringBuilder();

        ApplicationConfig card = applicationConfigRepo.findByKey(AppKey.RECONCILE_CARD);
        ApplicationConfig media = applicationConfigRepo.findByKey(AppKey.RECONCILE_MEDIA);
        for (CrmInStore crmInStore : crmInStores) {
            // Reconcile All Missing Points
            Map<String, Object> a = new HashMap<String, Object>();
            try {
                pointsSearchDto.setStoreCode(crmInStore.getStoreCode());
                PagingParam pointsPagination = pointsSearchDto.getPagination();
                pointsPagination.setPageSize(100);
                pointsPagination.setPageNo(0);
                ResultList<MissingPointsBean> resultList = reconciliationService.getMissingIndProfPoints(pointsSearchDto);
                while (resultList.isHasNextPage()) {
                    a.putAll(reconcileMissingPoints(resultList.getResults(), crmInStore.getId()));

                    pointsPagination.setPageNo(pointsPagination.getPageNo() + 1);
                    resultList = reconciliationService.getMissingIndProfPoints(pointsSearchDto);
                }
                if (resultList.getNumberOfElements() > 0) {
                    a.putAll(reconcileMissingPoints(resultList.getResults(), crmInStore.getId()));
                }
            } catch (MessageSourceResolvableException e) {
                logError(e);
            }

            // Reconcile All Missing Employee Purchase Transactions
            try {
                empTxnSearchDto.setStoreCode(crmInStore.getStoreCode());
                PagingParam empTxnPagination = empTxnSearchDto.getPagination();
                empTxnPagination.setPageSize(100);
                empTxnPagination.setPageNo(0);
                ResultList<MissingEmpTxnBean> resultList = reconciliationService.getMissingEmpTxns(empTxnSearchDto, card.getValue(), media.getValue());
                while (resultList.isHasNextPage()) {
                    reconcileMissingEmpTxn(resultList.getResults(), crmInStore.getId());

                    empTxnPagination.setPageNo(empTxnPagination.getPageNo() + 1);
                    resultList = reconciliationService.getMissingEmpTxns(empTxnSearchDto, card.getValue(), media.getValue());
                }
                if (resultList.getNumberOfElements() > 0) {
                    reconcileMissingEmpTxn(resultList.getResults(), crmInStore.getId());
                }
            } catch (MessageSourceResolvableException e) {
                logError(e);
            }

            message.append(crmInStore.getStoreCode());
            message.append(" - ");
            message.append(crmInStore.getName());
            message.append("Reconciled Transactions : Success (");
            message.append(a.get("x"));
            message.append(") - Failed (");
            message.append(a.get("x"));
            message.append(")\n");

            content.append(crmInStore.getStoreCode());
            content.append(" - ");
            content.append(crmInStore.getName());
            content.append("\n");

            for (String x : (List<String>) a.get("result")) {
                content.append(x);
                content.append("\n");
            }

        }

        MessageDto dto = new MessageDto();

        dto.setMessage(message.toString() + "\n\n\n\n\n" + content.toString());

        new CrmFileServiceImpl.MultipartFileImpl(content.toString().getBytes(), "", null);
        String subject = "Reconcile Job";
        try {
            subject = subject + InetAddress.getLocalHost().getHostAddress();
        } catch (UnknownHostException e) {
            e.printStackTrace();
        }
        dto.setSubject(subject);

        ApplicationConfig config = applicationConfigRepo.findByKey(AppKey.RECONCILE_MAIL);
        if (config != null && StringUtils.isNotBlank(config.getValue())) {
            dto.setRecipients((String[]) Arrays.asList(config.getValue().split("\\s*,\\s*")).toArray());
        }

        mailService.send(dto);
    }

    private Map<String, Object> reconcileMissingPoints(Collection<MissingPointsBean> beans, String crmInStoreId) {
        List<String> b = new ArrayList<String>();
        int x = 0;
        int y = 0;
        Collection<MissingPointsBean> afterBeans = new ArrayList<MissingPointsBean>();
        for (MissingPointsBean bean : beans) {
            boolean valid = true;
            for (MissingPointsBean bean2 : afterBeans) {
                if (bean.getTxnNo().equals(bean2.getTxnNo())) {
                    valid = false;
                }
            }
            if (valid) {
                afterBeans.add(bean);
            }
        }
        for (MissingPointsBean bean : afterBeans) {
            String reconcileTxnUrl = crmWebRootUrl + "/reconcilee/" + bean.getTxnNo() + "/in/crmstore/" + crmInStoreId;
            GenericResponseBean result = restTemplate.getForObject(reconcileTxnUrl, GenericResponseBean.class);
            if (result != null && (StringUtils.isNotEmpty(result.getErrorMessage()) || result.getException() != null)) {
                b.add(bean.getStringOutput() + ", Failed");
                y++;
            } else {
                b.add(bean.getStringOutput() + ", Success");
                x++;
            }
        }
        Map<String, Object> a = new HashMap<String, Object>();
        a.put("result", b);
        a.put("x", x);
        a.put("y", y);
        return a;
    }

    private void reconcileMissingEmpTxn(Collection<MissingEmpTxnBean> beans, String crmInStoreId) {
        for (MissingEmpTxnBean bean : beans) {
            String reconcileTxnUrl = crmWebRootUrl + "/reconcilee/" + bean.getTxnNo() + "/in/crmstore/" + crmInStoreId;
            restTemplate.getForObject(reconcileTxnUrl, GenericResponseBean.class);
        }
    }

    private void logError(MessageSourceResolvableException e) {
        String exceptionMessage = null;
        try {
            exceptionMessage = messageSource.getMessage(e.getLastCode(), e.getArguments(), LocaleContextHolder.getLocale());
        } catch (NoSuchMessageException e1) {
            exceptionMessage = e.getDefaultMessage();
        }
        _LOG.warn(exceptionMessage);
    }
}
