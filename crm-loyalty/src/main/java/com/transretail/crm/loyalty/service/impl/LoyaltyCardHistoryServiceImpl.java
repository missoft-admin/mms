package com.transretail.crm.loyalty.service.impl;


import java.util.List;
import java.util.Map;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.joda.time.DateTime;
import org.joda.time.LocalDate;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.task.AsyncTaskExecutor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.google.common.collect.Maps;
import com.mysema.query.types.expr.NumberExpression;
import com.transretail.crm.common.reporting.jasper.ClasspathFileResolver;
import com.transretail.crm.common.reporting.jasper.DefaultJRProcessor;
import com.transretail.crm.common.reporting.jasper.JRProcessor;
import com.transretail.crm.common.service.dto.response.ResultList;
import com.transretail.crm.core.audit.CustomAuditorAwareImpl;
import com.transretail.crm.core.entity.lookup.LookupDetail;
import com.transretail.crm.core.util.generator.IdGeneratorService;
import com.transretail.crm.loyalty.dto.LoyaltyCardTrackerDto;
import com.transretail.crm.loyalty.dto.LoyaltyCardTrackerFilterDto;
import com.transretail.crm.loyalty.dto.LoyaltyCardTrackingDto;
import com.transretail.crm.loyalty.dto.MultipleAllocationDto;
import com.transretail.crm.loyalty.entity.LoyaltyCardInventory;
import com.transretail.crm.loyalty.entity.LoyaltyCardInventoryHistory;
import com.transretail.crm.loyalty.entity.QLoyaltyCardInventory;
import com.transretail.crm.loyalty.repo.LoyaltyCardInventoryHistoryRepo;
import com.transretail.crm.loyalty.repo.LoyaltyCardInventoryRepo;
import com.transretail.crm.loyalty.service.LoyaltyCardHistoryService;
import com.transretail.crm.loyalty.service.LoyaltyCardService;


@Service("loyaltyCardHistoryService")
public class LoyaltyCardHistoryServiceImpl implements LoyaltyCardHistoryService {
	
	private static final String PARAM_CARD_TYPE = "cardType";
	private static final String PARAM_LOCATION = "location";
	private static final String PARAM_DATE_RANGE = "dateRange";
	private static final String PARAM_IS_EMPTY = "isEmpty";
	/*private static final String REPORT_NAME = "reports/loyalty_day-to-day_tracking.jasper";*/
	private static final String REPORT_NAME = "reports/loyalty_daily_tracking.jasper";
	private static final DateTimeFormatter FULL_DATETIME_PATTERN = DateTimeFormat.forPattern("MMMM d, yyyy HH:mm");
	private static final DateTimeFormatter FULL_DATE_PATTERN = DateTimeFormat.forPattern("MMMM d, yyyy");
	
	@Autowired
	LoyaltyCardInventoryHistoryRepo historyRepo;
	@Autowired
	LoyaltyCardInventoryRepo loyaltyCardInventoryRepo;
	@Autowired
	LoyaltyCardService loyaltyCardService;
	@Autowired
	IdGeneratorService customIdGeneratorService;
	@Value("#{'${hibernate.jdbc.batch_size}'}")
    private int batchSize;
    @PersistenceContext
    private EntityManager entityManager;
    @Autowired
    private AsyncTaskExecutor asyncTaskExecutor;
    
    private static final Logger LOGGER = LoggerFactory.getLogger(LoyaltyCardHistoryServiceImpl.class);
	
	@Override
	public List<MultipleAllocationDto> getTransaction(String transactionType, String username, LocalDate dateFrom, LocalDate dateTo) {
		return historyRepo.getTransactions(transactionType, username, dateFrom, dateTo);
	}
	
	@Override
	public void saveHistoryForInventory(LoyaltyCardInventory inventory, LookupDetail status, String location) {
		String username = new CustomAuditorAwareImpl().getCurrentAuditor();
		historyRepo.save(new LoyaltyCardInventoryHistory("LC_HIST_" + customIdGeneratorService.generateId() + inventory.getBarcode() + System.currentTimeMillis(), inventory, status, location, null, null, true, username, new DateTime()));
	}
	
	@Override
	public void saveHistoryForInventory(LoyaltyCardInventory inventory, LookupDetail status, String location, String transactionNo, Double annualFee, Double replacementFee, Boolean track) {
		String username = new CustomAuditorAwareImpl().getCurrentAuditor();
		historyRepo.save(new LoyaltyCardInventoryHistory("LC_HIST_" + customIdGeneratorService.generateId() + inventory.getBarcode() + System.currentTimeMillis(), inventory, status, location, null, transactionNo, annualFee, replacementFee, track, username, new DateTime()));
	}
	
	@Override
	public void saveHistoryForInventories(final String startingSeries, final String endingSeries, final LookupDetail status, final String location, final String transferTo, final String origin, final Boolean track) {
		
		Runnable task = new Runnable() {
			@Override
			public void run() {
				LOGGER.info("[SAVING LC INVENTORY HISTORY][STARTED] starting : " + startingSeries + " ending: " + endingSeries);
				nonAsyncSaveHistoryForInventories( startingSeries, endingSeries, status, location, transferTo, origin, track );
				LOGGER.info("[SAVING LC INVENTORY HISTORY][FINISHED] starting : " + startingSeries + " ending: " + endingSeries);
			}
		};
		
		asyncTaskExecutor.execute(task);
	}
	
	@Override
	@Transactional
	public void nonAsyncSaveHistoryForInventories(String startingSeries, String endingSeries, LookupDetail status, String location, String transferTo, String origin, Boolean track) {
		String username = new CustomAuditorAwareImpl().getCurrentAuditor();
		Long starting = Long.valueOf(startingSeries);
		Long ending = Long.valueOf(endingSeries);
		QLoyaltyCardInventory qInventory = QLoyaltyCardInventory.loyaltyCardInventory;
		NumberExpression<Long> seriesExp = qInventory.sequence.castToNum(Long.class);
		Iterable<LoyaltyCardInventory> inventories = loyaltyCardInventoryRepo.findAll(seriesExp.goe(starting).and(seriesExp.loe(ending)));
		DateTime created = new DateTime();
		int insertCounter = 0;
		for(LoyaltyCardInventory inventory : inventories) {
			LoyaltyCardInventoryHistory hist = new LoyaltyCardInventoryHistory("LC_HIST_" + customIdGeneratorService.generateId() + inventory.getBarcode() + System.currentTimeMillis(), inventory, status, location, transferTo, origin, track, username, created);
			if(insertCounter < batchSize) {
				historyRepo.save(hist);
                insertCounter++;
            } else {
            	historyRepo.saveAndFlush(hist);
                entityManager.clear();
                insertCounter = 0;
            }
		}
	}
	
	@Override
	public void saveHistoryForInventories(final String referenceNo, final LookupDetail status, final String location, final String transferTo, final String origin, final Boolean track) {
		Runnable task = new Runnable() {

			@Override
			public void run() {
				String username = new CustomAuditorAwareImpl().getCurrentAuditor();
				QLoyaltyCardInventory qInventory = QLoyaltyCardInventory.loyaltyCardInventory;
				Iterable<LoyaltyCardInventory> inventories = loyaltyCardInventoryRepo.findAll(qInventory.referenceNo.eq(referenceNo));
				DateTime created = new DateTime();
				int insertCounter = 0;
				for(LoyaltyCardInventory inventory : inventories) {
					LoyaltyCardInventoryHistory hist = new LoyaltyCardInventoryHistory("LC_HIST_" + customIdGeneratorService.generateId() + inventory.getBarcode() + System.currentTimeMillis(), inventory, status, location, transferTo, origin, track, username, created);
					if(insertCounter < batchSize) {
						historyRepo.save(hist);
		                insertCounter++;
		            } else {
		            	historyRepo.saveAndFlush(hist);
		                entityManager.clear();
		                insertCounter = 0;
		            }
				}
			}
			
		};
		asyncTaskExecutor.execute(task);
		
	}
	
	@Override
	public void saveHistoryForInventories(final LookupDetail status, final String location, final String transferTo, final String origin, final List<LoyaltyCardInventory> inventories, final Boolean track) {
		Runnable task = new Runnable() {

			@Override
			public void run() {
				int size = inventories.size();
				LOGGER.info("[SAVING LC INVENTORY HISTORY][STARTED] count : " + size);
				nonAsyncSaveHistoryForInventories(status, location, transferTo, origin, inventories, track);
				LOGGER.info("[SAVING LC INVENTORY HISTORY][FINISHED] count : " + size);
			}
			
		};
		asyncTaskExecutor.execute(task);
		
	}
	
	@Override
	@Transactional
	public void nonAsyncSaveHistoryForInventories(LookupDetail status, String location, String transferTo, String origin, List<LoyaltyCardInventory> inventories, Boolean track) {
		String username = new CustomAuditorAwareImpl().getCurrentAuditor();
		DateTime created = new DateTime();
		int insertCounter = 0;
		for(LoyaltyCardInventory inventory : inventories) {
			LoyaltyCardInventoryHistory hist = new LoyaltyCardInventoryHistory("LC_HIST_" + customIdGeneratorService.generateId() + inventory.getBarcode() + System.currentTimeMillis(), inventory, status, location, transferTo, origin, track, username, created);
			if(insertCounter < batchSize) {
				historyRepo.save(hist);
                insertCounter++;
            } else {
            	historyRepo.saveAndFlush(hist);
                entityManager.clear();
                insertCounter = 0;
            }
		}
	}
	
	@Override
	public void saveHistoryForInventories(final LookupDetail status, final List<LoyaltyCardInventory> inventories, final Boolean track) {
		Runnable task = new Runnable() {

			@Override
			public void run() {
				String username = new CustomAuditorAwareImpl().getCurrentAuditor();
				DateTime created = new DateTime();
				int insertCounter = 0;
				for(LoyaltyCardInventory inventory : inventories) {
					LoyaltyCardInventoryHistory hist = new LoyaltyCardInventoryHistory("LC_HIST_" + customIdGeneratorService.generateId() + inventory.getBarcode() + System.currentTimeMillis(), inventory, status, inventory.getLocation(), inventory.getTransferTo(), null, track, username, created);
					if(insertCounter < batchSize) {
						historyRepo.save(hist);
		                insertCounter++;
		            } else {
		            	historyRepo.saveAndFlush(hist);
		                entityManager.clear();
		                insertCounter = 0;
		            }
				}
			}
			
		};
		asyncTaskExecutor.execute(task);
		
	}

	@Override
	public JRProcessor createJrProcessor(LoyaltyCardTrackerFilterDto filterDto) {
		filterDto.getPagination().setPageSize(-1); //to retrieve all results
		
		ResultList<LoyaltyCardTrackerDto> results = trackInventory2(filterDto);
		
    	Map<String, Object> parameters = Maps.newHashMap();
    	StringBuffer dateStr = new StringBuffer();
    	if(filterDto.getDateFrom() != null) 
    		dateStr.append(FULL_DATE_PATTERN.print(filterDto.getDateFrom()));
    	dateStr.append(" - ");
    	if(filterDto.getDateTo() != null)
    		dateStr.append(FULL_DATE_PATTERN.print(filterDto.getDateTo()));
    	parameters.put(PARAM_DATE_RANGE, dateStr.toString());
    	parameters.put(PARAM_CARD_TYPE, filterDto.getProduct().getCode() + " - " + filterDto.getProduct().getDescription());
    	parameters.put(PARAM_LOCATION, filterDto.getLocation() + " - " + loyaltyCardService.getInventoryDesc(filterDto.getLocation()));
    	if (results.getResults().isEmpty()) {
            parameters.put(PARAM_IS_EMPTY, true);
            results.getResults().add(new LoyaltyCardTrackerDto());
        } 
    	/*else { #90877 removed conversion to ReportBean for optimization
        	for(LoyaltyCardTrackingDto tracker : results.getResults()) {
        		beansDataSource.add(new ReportBean(FULL_DATETIME_PATTERN.print(tracker.getCreatedDate()),
        				tracker.getBeginningBal(),
        				tracker.getTransaction(),
        				tracker.getQuantity(),
        				tracker.getEndingBal()));
        	}
        }*/
        DefaultJRProcessor jrProcessor = new DefaultJRProcessor(REPORT_NAME, results.getResults());
        jrProcessor.addParameters(parameters);
        jrProcessor.setFileResolver(new ClasspathFileResolver("reports"));
        return jrProcessor;
    }
	
	@Override
	public ResultList<LoyaltyCardTrackingDto> trackInventory(LoyaltyCardTrackerFilterDto filterDto) {
		return historyRepo.trackInventories2(filterDto);
	}
	
	@Override
	public ResultList<LoyaltyCardTrackerDto> trackInventory2(LoyaltyCardTrackerFilterDto filterDto) {
		return historyRepo.trackInventories(filterDto);
	}
	
	/*public static class ReportBean {
		private String date;
		private Long begBal;
		private String transaction;
		private Long quantity;
		private Long endBal;
		public ReportBean() {};
		public ReportBean(String date, Long begBal, String transaction,
				Long quantity, Long endBal) {
			super();
			this.date = date;
			this.begBal = begBal;
			this.transaction = transaction;
			this.quantity = quantity;
			this.endBal = endBal;
		}

		public String getDate() {
			return date;
		}
		public void setDate(String date) {
			this.date = date;
		}
		public Long getBegBal() {
			return begBal;
		}
		public void setBegBal(Long begBal) {
			this.begBal = begBal;
		}
		public String getTransaction() {
			return transaction;
		}
		public void setTransaction(String transaction) {
			this.transaction = transaction;
		}
		public Long getQuantity() {
			return quantity;
		}
		public void setQuantity(Long quantity) {
			this.quantity = quantity;
		}
		public Long getEndBal() {
			return endBal;
		}
		public void setEndBal(Long endBal) {
			this.endBal = endBal;
		}
		
	}*/
	
}
