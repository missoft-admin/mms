package com.transretail.crm.giftcard.service.impl;

import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Maps;
import com.mysema.query.BooleanBuilder;
import com.mysema.query.jpa.JPASubQuery;
import com.mysema.query.jpa.impl.JPAQuery;
import com.mysema.query.support.Expressions;
import com.mysema.query.types.ConstantImpl;
import com.mysema.query.types.Expression;
import com.mysema.query.types.Projections;
import com.mysema.query.types.QBean;
import com.mysema.query.types.expr.*;
import com.mysema.query.types.template.DateTemplate;
import com.transretail.crm.common.reporting.engine.data.JRQueryDSLDataSource;
import com.transretail.crm.common.reporting.jasper.ClasspathFileResolver;
import com.transretail.crm.common.reporting.jasper.DefaultJRProcessor;
import com.transretail.crm.common.reporting.jasper.JRProcessor;
import com.transretail.crm.common.util.DateUtil;
import com.transretail.crm.core.entity.PeopleSoftStore;
import com.transretail.crm.core.entity.QPeopleSoftStore;
import com.transretail.crm.core.entity.lookup.LookupDetail;
import com.transretail.crm.core.reporting.support.StoreFieldValueCustomizer;
import com.transretail.crm.core.security.util.UserUtil;
import com.transretail.crm.core.service.PsoftStoreService;
import com.transretail.crm.core.service.StoreService;
import com.transretail.crm.giftcard.entity.*;
import com.transretail.crm.giftcard.entity.support.GiftCardSaleTransaction;
import com.transretail.crm.giftcard.entity.support.GiftCardSalesType;
import com.transretail.crm.giftcard.service.ProductProfileService;
import com.transretail.crm.giftcard.service.SalesAccountingReportService;

import java.awt.*;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

import javax.persistence.Query;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import net.sf.dynamicreports.jasper.builder.JasperReportBuilder;
import net.sf.dynamicreports.report.builder.DynamicReports;
import net.sf.dynamicreports.report.builder.column.Columns;
import net.sf.dynamicreports.report.builder.style.StyleBuilder;
import net.sf.dynamicreports.report.constant.Calculation;
import net.sf.dynamicreports.report.constant.HorizontalAlignment;
import net.sf.dynamicreports.report.exception.DRException;
import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.JRParameter;
import net.sf.jasperreports.engine.JRVirtualizer;
import net.sf.jasperreports.engine.data.JRHibernateListDataSource;
import net.sf.jasperreports.engine.data.JRMapArrayDataSource;
import net.sf.jasperreports.engine.fill.JRSwapFileVirtualizer;
import net.sf.jasperreports.engine.util.JRSwapFile;
import org.apache.commons.lang3.StringUtils;
import org.joda.time.LocalDate;
import org.joda.time.LocalDateTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import static net.sf.dynamicreports.report.builder.DynamicReports.*;


@Service
@Transactional(readOnly = true)
public class SalesAccountingReportServiceImpl implements SalesAccountingReportService {

    private static final Logger LOG = LoggerFactory.getLogger(SalesAccountingReportServiceImpl.class);
    private static final String BOOKED_DATE_FROM_PARAM = "BOOKED_DATE_FROM";
    private static final String BOOKED_DATE_TO_PARAM = "BOOKED_DATE_TO";
    private static final String STORE_PARAM = "STORE";
    private static final String BUSINESS_UNIT_CODE_PARAM = "BU_CODE";
    private static final String SALES_TYPE_PARAM = "SALES_TYPE";
    private static final String PS_STORE_PARAM = "PS_STORE";
    private static final String REGION_PARAM = "REGION";
    private static final String TRANSACTION_TYPE_PARAM = "TRANSACTION_TYPE";
    private static final String SUB_DATASOURCE = "SUB_DATASOURCE";
    private static final String PRINTED_BY = "PRINTED_BY";

    @PersistenceContext
    private EntityManager em;

    private final StoreService storeService;
    private final PsoftStoreService psoftStoreService;
    private final ProductProfileService profileService;

    @Autowired
    public SalesAccountingReportServiceImpl(StoreService storeService, PsoftStoreService psoftStoreService, ProductProfileService profileService) {
        this.storeService = storeService;
        this.psoftStoreService = psoftStoreService;
        this.profileService = profileService;
    }

    @Override
    public JRProcessor printSalesSummary(LocalDate dateFrom, LocalDate dateTo, String businessUnit, GiftCardSaleTransaction transactionType, String store, GiftCardSalesType salesType) {
        QSalesOrder qso = QSalesOrder.salesOrder;
        QSalesOrderItem qsoi = QSalesOrderItem.salesOrderItem;
        QGiftCardInventory qgc = QGiftCardInventory.giftCardInventory;
        QPeopleSoftStore qps = QPeopleSoftStore.peopleSoftStore;

        QGiftCardTransaction qtx = QGiftCardTransaction.giftCardTransaction;
        QGiftCardTransactionItem qtxi = QGiftCardTransactionItem.giftCardTransactionItem;

        StringExpression storeExp = new CaseBuilder().when(qps.store.code.isNull())
                .then(qtx.merchantId).otherwise(qps.store.code.concat(" - ").concat(qps.store.name));
        final StringExpression regionExp = qps.businessRegion.code.concat(" - ").concat(qps.businessRegion.description);
        NumberExpression<Double> qty = new CaseBuilder()
                .when(qtx.transactionType.eq(GiftCardSaleTransaction.ACTIVATION))
                .then(Expressions.numberTemplate(Double.class, "1"))
                .otherwise(-1.).sum();

        Map<String, Object> params = Maps.newHashMap();
        BooleanBuilder expBuilder = new BooleanBuilder(qtx.transactionDate.goe(dateFrom.toDateTimeAtStartOfDay().toLocalDateTime())
                .and(qtx.transactionDate.loe(dateTo.plusDays(1).toDateTimeAtStartOfDay().toLocalDateTime().minusMillis(1))));

        if (transactionType == null) {
            expBuilder.and(qtx.transactionType.in(GiftCardSaleTransaction.ACTIVATION, GiftCardSaleTransaction.VOID_ACTIVATED));
        } else {
            expBuilder.and(qtx.transactionType.eq(transactionType));
            params.put(TRANSACTION_TYPE_PARAM, transactionType.name());
        }

        if (StringUtils.isNotBlank(businessUnit)) {
            expBuilder.and(qps.businessUnit.code.eq(businessUnit));
            params.put(BUSINESS_UNIT_CODE_PARAM, businessUnit);
        }

        if (StringUtils.isNotBlank(store)) {
            expBuilder.and(qps.store.code.eq(store));
            PeopleSoftStore storeMapping = psoftStoreService.getStoreMapping(store);
            if (storeMapping != null) {
                final LookupDetail businessRegion = storeMapping.getBusinessRegion();
                String region = businessRegion != null ? businessRegion.getCode() + " - " + businessRegion.getDescription() : "";
                final LookupDetail buLookup = storeMapping.getBusinessUnit();
                params.put(REGION_PARAM, region);
                String bu = buLookup != null ? buLookup.getCode() : "";
                params.put(BUSINESS_UNIT_CODE_PARAM, bu);
                params.put(PS_STORE_PARAM, storeMapping.getPeoplesoftCode());
            }
            params.put(STORE_PARAM, storeService.getStoreByCode(store).getCodeAndName());
        }

        if (salesType != null) {
            expBuilder.and(qtx.salesType.eq(salesType));
            params.put(SALES_TYPE_PARAM, salesType.name());
        }

        List<SalesSummaryBean> list2 = new JPAQuery(em).from(qtx)
                .innerJoin(qtx.transactionItems, qtxi)
                .leftJoin(qtx.salesOrder, qso)
                .leftJoin(qso.items, qsoi)
                .leftJoin(qtx.peoplesoftStoreMapping, qps)
                .innerJoin(qtxi.giftCard, qgc)
                .where(expBuilder)
                .groupBy(truncate(qtx.transactionDate, DD),
                        qps.businessUnit.code,
                        qtx.salesType.stringValue(),
                        //                        region, 
                        qps.businessRegion.code,
                        qps.businessRegion.description,
                        //                        store, 
                        qtx.merchantId,
                        qps.store.code,
                        qps.store.name,
                        qps.peoplesoftCode)
                .orderBy(truncate(qtx.transactionDate, DD).desc())
                .list(Projections.bean(SalesSummaryBean.class,
                                truncate(qtx.transactionDate, DD).as("bookedDateHandler"),
                                qps.businessUnit.code.as("bu"),
                                qtx.salesType.stringValue().as(SALES_TYPE_FIELD),
                                regionExp.as(REGION_FIELD),
                                storeExp.as(SALE_STORE_FIELD),
                                qps.peoplesoftCode.as(PS_STORE_FIELD),
                                // qtx.transactionType.nullif(GiftCardSaleTransaction.ACTIVATION).count()
                                // .subtract(qtx.transactionType.nullif(GiftCardSaleTransaction.VOID_ACTIVATED).count())
                                // .as(CARD_QUANTITY_FIELD),
                                qty.as(CARD_QUANTITY_FIELD),
                                qtxi.transactionAmount.sum().as(SALES_AMOUNT_FIELD),
                                qtx.discountPercentage.sum().doubleValue().as(DISCOUNT_FIELD),
                                Expressions.cases()
                                .when(qtx.transactionType.eq(GiftCardSaleTransaction.VOID_ACTIVATED))
                                .then(qso.shippingFee.negate())
                                .otherwise(qso.shippingFee).sum().doubleValue().as(SHIPPING_COST_FIELD),
                                Expressions.cases()
                                .when(qtx.transactionType.eq(GiftCardSaleTransaction.VOID_ACTIVATED))
                                .then(qsoi.printFee.negate())
                                .otherwise(qsoi.printFee).sum().doubleValue().as(CARD_COST_AMOUNT_FIELD)
                        ));

        JRProcessor processor = createDefaultJRProcessor("gc-acct-sales-summary", list2);
        processor.addParameters(params);
        processor.addParameter(BOOKED_DATE_FROM_PARAM, dateFrom.toString(DateUtil.SEARCH_DATE_FORMAT));
        processor.addParameter(BOOKED_DATE_TO_PARAM, dateTo.toString(DateUtil.SEARCH_DATE_FORMAT));

        return processor;
    }

    @Override
    public JRProcessor printRedeemDetail(LocalDate dateFrom, LocalDate dateTo, String businessUnit, String store, String product, String redeemType) {
        QGiftCardTransaction qtx = QGiftCardTransaction.giftCardTransaction;
        QGiftCardTransactionItem qti = QGiftCardTransactionItem.giftCardTransactionItem;
        QGiftCardInventory qgci = QGiftCardInventory.giftCardInventory;
        QProductProfile qpp = QProductProfile.productProfile;
        QPeopleSoftStore qps = QPeopleSoftStore.peopleSoftStore;

        Map<String, Object> params = Maps.newHashMap();
        StringExpression redeemTypeExp = new CaseBuilder()
                .when(qtx.loseTransaction.isTrue())
                .then(constant("MANUAL"))
                .otherwise(constant("NORMAL")).as(REDEEM_TYPE_FIELD);

        BooleanBuilder expBuilder = new BooleanBuilder(qtx.transactionType.eq(GiftCardSaleTransaction.REDEMPTION)
                .and(qtx.transactionDate.goe(dateFrom.toDateTimeAtStartOfDay().toLocalDateTime())
                        .and(qtx.transactionDate.loe(dateTo.plusDays(1)
                                        .toDateTimeAtStartOfDay().toLocalDateTime().minusMillis(1)))));

        if (StringUtils.isNotBlank(businessUnit)) {
            expBuilder.and(qps.businessUnit.code.eq(businessUnit));
            params.put(BUSINESS_UNIT_CODE_PARAM, businessUnit);
        }

        if (StringUtils.isNotBlank(store)) {
            expBuilder.and(qps.store.code.eq(store));
            params.put(STORE_PARAM, store);
            PeopleSoftStore storeMapping = psoftStoreService.getStoreMapping(store);
            if (storeMapping != null) {
                final LookupDetail businessRegion = storeMapping.getBusinessRegion();
                String region = businessRegion != null ? businessRegion.getCode() + " - " + businessRegion.getDescription() : "";
                params.put(REGION_PARAM, region);
                final LookupDetail buLookup = storeMapping.getBusinessRegion();
                String bu = buLookup != null ? buLookup.getCode() : "";
                params.put(BUSINESS_UNIT_CODE_PARAM, bu);
                params.put(PS_STORE_PARAM, storeMapping.getPeoplesoftCode());
            }
            params.put(STORE_PARAM, storeService.getStoreByCode(store).getCodeAndName());
        }

        if (StringUtils.isNotBlank(product)) {
            expBuilder.and(qpp.productCode.eq(product));
            params.put("PRODUCT", profileService.findProductProfileByCode(product).getProductDesc());
        }

        if (StringUtils.isNotBlank(redeemType)) {
            expBuilder.and(qtx.loseTransaction.eq("MANUAL".equals(redeemType)));
            params.put("REDEEM_TYPE", redeemType);
        }

        JPAQuery query = new JPAQuery(em).from(qtx)
                .join(qtx.transactionItems, qti)
                .join(qti.giftCard, qgci)
                .join(qgci.profile, qpp)
                .join(qtx.peoplesoftStoreMapping, qps)
                .where(expBuilder);

        QBean<RedeemDetailBean> projections = Projections.fields(RedeemDetailBean.class,
                qps.businessUnit.code.as("businessUnit"),
                qps.businessRegion.code.concat(" - ").concat(qps.businessRegion.description).as(REGION_FIELD),
                qps.peoplesoftCode.as(PS_STORE_FIELD),
                qtx.merchantId.as(REDEEM_STORE_FIELD),
                qtx.terminalId, qtx.cashierId, qtx.transactionNo.as(POS_TRANSACTION_NO_FIELD),
                qtx.transactionDate, qpp.productDesc.as(CARD_NAME_FIELD),
                qgci.barcode.as(CARD_NO_FIELD), qti.transactionAmount.as(REDEEM_AMOUNT_FIELD),
                redeemTypeExp);

        LOG.info(query.toString());
        JRQueryDSLDataSource datasource = new JRQueryDSLDataSource(query, projections,
                new StoreFieldValueCustomizer(REDEEM_STORE_FIELD, storeService));

        JRProcessor processor = createDefaultJRProcessor("gc-acct-redeem-detail");
        processor.addParameter(SUB_DATASOURCE, datasource);
        processor.addParameters(params);
        processor.addParameter(BOOKED_DATE_FROM_PARAM, dateFrom.toString(DateUtil.SEARCH_DATE_FORMAT));
        processor.addParameter(BOOKED_DATE_TO_PARAM, dateTo.toString(DateUtil.SEARCH_DATE_FORMAT));

        return processor;
    }

    @Override
    public JRProcessor printOutstandingBalance(LocalDate dateFrom, LocalDate dateTo, String store, String cardNo) {
        QSalesOrder qso = QSalesOrder.salesOrder;
        QSalesOrderItem qsoi = QSalesOrderItem.salesOrderItem;
        QSalesOrderAlloc qsoa = QSalesOrderAlloc.salesOrderAlloc;
        QGiftCardTransaction qtx = QGiftCardTransaction.giftCardTransaction;
        QGiftCardTransactionItem qti = QGiftCardTransactionItem.giftCardTransactionItem;
        QGiftCardInventory qgci = QGiftCardInventory.giftCardInventory;
        QGiftCardInventory qgci2 = new QGiftCardInventory("gc2");

        Map<String, Object> params = Maps.newHashMap();
        NumberExpression<Double> amount = new CaseBuilder()
                .when(qtx.transactionType.eq(GiftCardSaleTransaction.VOID_ACTIVATED)
                        .and(qso.isNotNull()))
                .then(qgci.faceValue.multiply(-1).doubleValue())
                .when(qtx.transactionType.ne(GiftCardSaleTransaction.VOID_ACTIVATED)
                        .and(qso.isNotNull()))
                .then(qgci.faceValue.doubleValue())
                .otherwise(qti.transactionAmount)
                .sum();

        BooleanBuilder expBuilder = new BooleanBuilder(qgci.lastUpdated.goe(dateFrom.toDateTimeAtStartOfDay().toDateTime())
                .and(qgci.lastUpdated.loe(dateTo.plusDays(1)
                                .toDateTimeAtStartOfDay().toDateTime().minusMillis(1))));

        if (StringUtils.isNotBlank(cardNo)) {
            expBuilder.and(qgci.barcode.eq(cardNo));
        }

        if (StringUtils.isNotBlank(store)) {
            expBuilder.and(qgci.location.eq(store));
            PeopleSoftStore storeMapping = psoftStoreService.getStoreMapping(store);
            if (storeMapping != null) {
                final LookupDetail businessRegion = storeMapping.getBusinessRegion();
                String region = businessRegion != null ? businessRegion.getCode() + " - " + businessRegion.getDescription() : "";
                final LookupDetail businessUnit = storeMapping.getBusinessUnit();
                String bu = businessUnit != null ? businessUnit.getCode() : "";
                params.put(REGION_PARAM, region);
                params.put(BUSINESS_UNIT_CODE_PARAM, bu);
                params.put(PS_STORE_PARAM, storeMapping.getPeoplesoftCode());
            }

            params.put(STORE_PARAM, storeService.getStoreByCode(store).getCodeAndName());
        }

        JPAQuery query = new JPAQuery(em).from(qgci).where(expBuilder);
        QBean<OutstandingBalanceBean> projections = Projections.fields(OutstandingBalanceBean.class,
                qgci.barcode.as(CARD_NO_FIELD),
                qgci.balance.as(AMOUNT_FIELD),
                constant("0").castToNum(Double.class).as(PERCENT_FIELD));

        JRProcessor processor = createDefaultJRProcessor("gc-acct-outstanding-balance-detail");
        processor.addParameter(SUB_DATASOURCE, new JRQueryDSLDataSource(query, projections));
        processor.addParameters(params);
        processor.addParameter(PRINTED_BY, UserUtil.getCurrentUser().getUsername());
        processor.addParameter(BOOKED_DATE_FROM_PARAM, dateFrom.toString(DateUtil.SEARCH_DATE_FORMAT));
        processor.addParameter(BOOKED_DATE_TO_PARAM, dateTo.toString(DateUtil.SEARCH_DATE_FORMAT));

        return processor;
    }

    private StringExpression constant(String constantValue) {
        return Expressions.stringTemplate(String.format("'%s'", constantValue));
    }

    // Good to have this in a util class	
    protected DateExpression<LocalDateTime> truncate(Expression<?> expression, String pattern) {
        return DateTemplate.create(LocalDateTime.class, "trunc({0}, '{1s}')", expression, ConstantImpl.create(pattern));
    }

    private JRProcessor createDefaultJRProcessor(String fileName, List<?> datasource) {
        return createDefaultJRProcessor(fileName,
                new JRMapArrayDataSource(new Map[]{ImmutableMap.of("details", datasource)}));
    }

    private JRProcessor createDefaultJRProcessor(String fileName, JRDataSource datasource) {
        DefaultJRProcessor processor = new DefaultJRProcessor(
                String.format("reports/giftcard/%s.jasper", fileName),
                datasource);

        processor.setFileResolver(new ClasspathFileResolver("reports/giftcard"));

        return processor;
    }

    private JRProcessor createDefaultJRProcessor(String fileName) {
        DefaultJRProcessor processor = new DefaultJRProcessor(
                String.format("reports/giftcard/%s.jasper", fileName));

        processor.setFileResolver(new ClasspathFileResolver("reports/giftcard"));

        return processor;
    }

    @Override
    public JRProcessor printSalesDetail(LocalDate dateFrom, LocalDate dateTo, String businessUnit, String store, GiftCardSalesType salesType, boolean b2bReturn) {

        QGiftCardTransaction qtx = QGiftCardTransaction.giftCardTransaction;
        QGiftCardTransactionItem qgcti = QGiftCardTransactionItem.giftCardTransactionItem;
        QSalesOrder qso = QSalesOrder.salesOrder;
        QSalesOrderItem qsoi = QSalesOrderItem.salesOrderItem;
        QSalesOrderAlloc qsoa = QSalesOrderAlloc.salesOrderAlloc;
        QGiftCardCustomerProfile qxp = QGiftCardCustomerProfile.giftCardCustomerProfile;
        QPeopleSoftStore qps = QPeopleSoftStore.peopleSoftStore;
        QGiftCardInventory qgci = QGiftCardInventory.giftCardInventory;

        BooleanBuilder booleanBuilder = new BooleanBuilder(
                qtx.transactionType.in(GiftCardSaleTransaction.ACTIVATION, GiftCardSaleTransaction.VOID_ACTIVATED));

        booleanBuilder.and(qtx.transactionDate.goe(dateFrom.toDateTimeAtStartOfDay().toLocalDateTime())
                .and(qtx.transactionDate.loe(dateTo.plusDays(1)
                                .toDateTimeAtStartOfDay().toLocalDateTime().minusMillis(1))));

        Map<String, Object> params = Maps.newHashMap();

        if (GiftCardSalesType.B2B == salesType && b2bReturn) {

            booleanBuilder.and(qtx.salesType.eq(salesType));
            booleanBuilder.and(qtx.transactionType.eq(GiftCardSaleTransaction.VOID_ACTIVATED));
            params.put(SALES_TYPE_PARAM, "B2B RETURN");

        } else if (salesType != null) {

            booleanBuilder.and(qtx.salesType.eq(salesType));
            params.put(SALES_TYPE_PARAM, salesType.name());

        } else {

            booleanBuilder.and(qtx.salesType.in(GiftCardSalesType.B2B, GiftCardSalesType.B2C,  GiftCardSalesType.INTERNAL, GiftCardSalesType.REBATE));
            params.put(SALES_TYPE_PARAM, "All Type");    

        }

        // check if this user if from HO
        PeopleSoftStore storeMapping;
        final String currentStore = UserUtil.getCurrentUser().getStore().getCode();
        if (!currentStore.equals("10007")) {

            storeMapping = psoftStoreService.getStoreMapping(store);

        } else {

            storeMapping = psoftStoreService.getStoreMapping(currentStore);

        }

        // get business unit
        if (StringUtils.isNotBlank(businessUnit)) {

            booleanBuilder.and(qps.businessUnit.code.eq(businessUnit));
            params.put(BUSINESS_UNIT_CODE_PARAM, businessUnit);

        } else if (!currentStore.equals("10007")){

            if (storeMapping != null) {

                final LookupDetail buLookup = storeMapping.getBusinessUnit();
                String bu = buLookup != null ? buLookup.getCode() : "";
                booleanBuilder.and(qps.businessUnit.code.eq(businessUnit));
                params.put(BUSINESS_UNIT_CODE_PARAM, bu);
            }

        } else {

            params.put(BUSINESS_UNIT_CODE_PARAM, "All Business Unit");
        }


        // check user strore
        if (StringUtils.isNotBlank(store)) {

            booleanBuilder.and(qps.store.code.eq(store));

            if (storeMapping != null) {
                final LookupDetail businessRegion = storeMapping.getBusinessRegion();
                String region = businessRegion != null ? businessRegion.getCode() + " - " + businessRegion.getDescription() : "";
                params.put(REGION_PARAM, region);
                params.put(PS_STORE_PARAM, storeMapping.getPeoplesoftCode());
            }
            params.put(STORE_PARAM, storeService.getStoreByCode(store).getCodeAndName());

        } else if (!currentStore.equals("10007")) {

            booleanBuilder.and(qps.store.code.eq(currentStore));
            PeopleSoftStore softStore = psoftStoreService.getStoreMapping(currentStore);
            if (softStore != null) {

                final LookupDetail buRegion = softStore.getBusinessRegion();
                String region = buRegion != null ? buRegion.getCode() + " - " + buRegion.getDescription() : "";
                params.put(REGION_PARAM, region);
                final LookupDetail buLookup = softStore.getBusinessUnit();
                String bu = buLookup != null ? buLookup.getCode() : "";
                params.put(BUSINESS_UNIT_CODE_PARAM, bu);
                params.put(PS_STORE_PARAM, softStore.getPeoplesoftCode());
            }

            params.put(STORE_PARAM, storeService.getStoreByCode(currentStore).getCodeAndName());

        } else {

            params.put(REGION_PARAM, "All Region");
            params.put(BUSINESS_UNIT_CODE_PARAM, "All Business Unit");
            params.put(STORE_PARAM,  "All Store");
        }

        NumberExpression<Long> cardQty = Expressions.cases()
                .when(qtx.salesType.in(GiftCardSalesType.B2B, GiftCardSalesType.INTERNAL, GiftCardSalesType.REBATE))
                .then(qsoa.seriesEnd.subtract(qsoa.seriesStart).add(1))
                .otherwise(qgci.barcode.count());
        NumberExpression<Long> cardQuantity=Expressions.cases()
        		.when(qtx.transactionType.in(GiftCardSaleTransaction.VOID_ACTIVATED,GiftCardSaleTransaction.VOID_REDEMPTION,GiftCardSaleTransaction.VOID_RELOAD))
        		.then(cardQty.multiply(-1))
        		.otherwise(cardQty);
        //NumberExpression<Long> cardQuantity=cardQty.multiply(-1);
        NumberExpression<Double> cardAddValue = qgci.faceValue.doubleValue();
        NumberExpression<Double> discountAmount = qtx.discountPercentage.multiply(cardAddValue).divide(100.0).multiply(cardQuantity);
        NumberExpression<Double> cardCostAmount = qsoi.printFee.doubleValue();
        NumberExpression<Double> salesAmount = cardQuantity.doubleValue().multiply(cardAddValue);


        QBean<SalesDetailBean> projection = Projections.bean(SalesDetailBean.class,
        		truncate(qtx.transactionDate, DD).as("bookedDateHandler"),
                qps.businessUnit.code.as("businessUnit"),
                qtx.salesType.stringValue().as("salesType"),
                qtx.transactionType.stringValue().as("transactionType"),
                qps.businessRegion.code.concat(" - ").concat(qps.businessRegion.description).as(REGION_FIELD),
                qtx.merchantId.as("salesStore"),
                qps.peoplesoftCode.as("psSalesStore"),
                qso.orderNo.as("orderNo"),
                qxp.name.as("customerName"),
                qgci.productCode.as("productCode"),
                qgci.productName.as("productName"),
                new JPASubQuery().from(qgci).where(qgci.series.eq(qsoa.seriesStart)).unique(qgci.barcode).as("beginCardNo"),
                new JPASubQuery().from(qgci).where(qgci.series.eq(qsoa.seriesEnd)).unique(qgci.barcode).as("endCardNo"),
                cardQuantity.as("cardQuantity"),
                cardAddValue.as("cardAddValue"),
                salesAmount.as("salesAmount"),
                discountAmount.as("discount"),
                qso.shippingFee.doubleValue().divide(qsoi.quantity).multiply(cardQuantity).doubleValue().as("shippingCost"),
                cardCostAmount.doubleValue().divide(qsoi.quantity).multiply(cardQuantity).doubleValue().as("cardCostAmount")
        );

        JPAQuery query = new JPAQuery(em).from(qtx)
                .innerJoin(qtx.transactionItems, qgcti)
                .leftJoin(qtx.salesOrder, qso)
                .leftJoin(qso.items, qsoi)
                .leftJoin(qsoi.soAlloc, qsoa)
                .leftJoin(qso.customer, qxp)
                .leftJoin(qtx.peoplesoftStoreMapping, qps)
                .innerJoin(qgcti.giftCard, qgci)
                .where(qgci.series.eq(Expressions.cases()
                                .when(qtx.salesOrder.isNull())
                                .then(qgci.series)
                                .otherwise(qsoa.seriesStart))
                        .and(booleanBuilder.getValue()))
                .groupBy(
                		truncate(qtx.transactionDate, DD),
                		qps.businessUnit.code,
                        qtx.salesType,
                        qtx.transactionType,
                        qps.businessRegion.code,
                        qps.businessRegion.description,
                        qtx.cashierId,
                        qtx.terminalId,
                        qtx.merchantId,
                        qps.store.code,
                        qps.store.name,
                        qps.peoplesoftCode,
                        qso.orderNo,
                        qxp.name,
                        qgci.productCode,
                        qgci.productName,
                        qsoa.seriesStart,
                        qsoa.seriesEnd,
                        qgci.faceValue,
                        qtx.discountPercentage,
                        qsoi.quantity,
                        qsoi.printFee,
                        qso.shippingFee
                       )
                .orderBy(qtx.salesType.stringValue().asc(),
                        truncate(qtx.transactionDate, DD).desc(),
                        qgci.faceValue.asc());

        JRQueryDSLDataSource datasource = new JRQueryDSLDataSource(query, projection, new StoreFieldValueCustomizer("salesStore", storeService));
        JRProcessor processor = createDefaultJRProcessor("gc-acct-sales-detail");
        processor.addParameters(params);
        processor.addParameter(SUB_DATASOURCE, datasource);
        processor.addParameter(PRINTED_BY, UserUtil.getCurrentUser().getUsername());
        processor.addParameter(BOOKED_DATE_FROM_PARAM, dateFrom.toString(DateUtil.SEARCH_DATE_FORMAT));
        processor.addParameter(BOOKED_DATE_TO_PARAM, dateTo.toString(DateUtil.SEARCH_DATE_FORMAT));

        final String tmp = System.getProperty("java.io.tmpdir");
        JRSwapFile swapFile = new JRSwapFile(tmp, 1024, 1024);
        JRVirtualizer virtualizer = new JRSwapFileVirtualizer(50, swapFile, true);
        processor.addParameter(JRParameter.REPORT_VIRTUALIZER, virtualizer);
        return processor;
    }

    public static class SalesSummaryBean {

        private LocalDate bookedDate;
        private LocalDateTime bookedDateHandler;
        private String region;
        private String saleStore;
        private String psStore;
        private Double discount = 0.0;
        private long cardQuantity;
        private Double salesAmount = 0.0;
        private Double cardCostAmount = 0.0;
        private Double shippingCost = 0.0;
        private String bu;
        private String salesType;

        public LocalDateTime getBookedDateHandler() {
            return bookedDateHandler;
        }

        public void setBookedDateHandler(LocalDateTime bookedDateHandler) {
            this.bookedDateHandler = bookedDateHandler;
            this.setBookedDate(bookedDateHandler.toLocalDate());
        }

        public String getSaleStore() {

            return saleStore;
        }

        public void setSaleStore(String saleStore) {
            this.saleStore = saleStore;
        }

        public Double getDiscount() {
            return discount;
        }

        public void setDiscount(Double discount) {
            this.discount = discount;
        }

        public long getCardQuantity() {
            return cardQuantity;
        }

        public void setCardQuantity(long cardQuantity) {
            this.cardQuantity = cardQuantity;
        }

        public Double getSalesAmount() {
            return salesAmount;
        }

        public void setSalesAmount(Double salesAmount) {
            this.salesAmount = salesAmount;
        }

        public Double getCardCostAmount() {
            return cardCostAmount;
        }

        public void setCardCostAmount(Double cardCostAmount) {
            this.cardCostAmount = cardCostAmount;
        }

        public Double getShippingCost() {
            return shippingCost;
        }

        public void setShippingCost(Double shippingCost) {
            this.shippingCost = shippingCost;
        }

        public LocalDate getBookedDate() {
            return bookedDate;
        }

        public void setBookedDate(LocalDate bookedDate) {
            this.bookedDate = bookedDate;
        }

        public String getBu() {
            return bu;
        }

        public void setBu(String bu) {
            this.bu = bu;
        }

        public String getSalesType() {
            return salesType;
        }

        public void setSalesType(String salesType) {
            this.salesType = salesType;
        }

        public String getRegion() {
            return region;
        }

        public void setRegion(String region) {
            this.region = region;
        }

        public String getPsStore() {
            return psStore;
        }

        public void setPsStore(String psStore) {
            this.psStore = psStore;
        }
    }

    public static class RedeemDetailBean {

        private String businessUnit;
        private String psStore;
        private String region;
        private String redeemStore;
        private String terminalId;
        private String cashierId;
        private String posTransactionNo;
        private LocalDateTime transactionDate;
        private String cardName;
        private String cardNo;
        private double redeemAmount;
        private String redeemType;

        public String getBusinessUnit() {
            return businessUnit;
        }

        public void setBusinessUnit(String businessUnit) {
            this.businessUnit = businessUnit;
        }

        public String getRedeemStore() {
            return redeemStore;
        }

        public void setRedeemStore(String redeemStore) {
            this.redeemStore = redeemStore;
        }

        public String getTerminalId() {
            return terminalId;
        }

        public void setTerminalId(String terminalId) {
            this.terminalId = terminalId;
        }

        public String getCashierId() {
            return cashierId;
        }

        public void setCashierId(String cashierId) {
            this.cashierId = cashierId;
        }

        public String getPosTransactionNo() {
            return posTransactionNo;
        }

        public void setPosTransactionNo(String posTransactionNo) {
            this.posTransactionNo = posTransactionNo;
        }

        public LocalDateTime getTransactionDate() {
            return transactionDate;
        }

        public void setTransactionDate(LocalDateTime transactionDate) {
            this.transactionDate = transactionDate;
        }

        public String getCardName() {
            return cardName;
        }

        public void setCardName(String cardName) {
            this.cardName = cardName;
        }

        public String getCardNo() {
            return cardNo;
        }

        public void setCardNo(String cardNo) {
            this.cardNo = cardNo;
        }

        public double getRedeemAmount() {
            return redeemAmount;
        }

        public void setRedeemAmount(double redeemAmount) {
            this.redeemAmount = redeemAmount;
        }

        public String getRedeemType() {
            return redeemType;
        }

        public void setRedeemType(String redeemType) {
            this.redeemType = redeemType;
        }

        public String getRegion() {
            return region;
        }

        public void setRegion(String region) {
            this.region = region;
        }

        public String getPsStore() {
            return psStore;
        }

        public void setPsStore(String psStore) {
            this.psStore = psStore;
        }
    }

    public static class OutstandingBalanceBean {

        private String cardNo;
        private double amount;
        private double percent;

        public String getCardNo() {
            return cardNo;
        }

        public void setCardNo(String cardNo) {
            this.cardNo = cardNo;
        }

        public double getAmount() {
            return amount;
        }

        public void setAmount(double amount) {
            this.amount = amount;
        }

        public double getPercent() {
            return percent;
        }

        public void setPercent(double percent) {
            this.percent = percent;
        }

    }

    public static class SalesDetailBeans {

        private String orderNo;
        private String status;
        private LocalDate orderDate;
        private String sellingStore;
        private String sallesman;
        private String customer;
        private String salesType;
        private String discountType;
        private String discountAmount;
        private Long totalQuantity;
        private Double totalFaceAmount;
        private Double totalPayableAmount;
        private Double shippingFee;
        private Double cardFee;
        private String cardVendor;



    }

    public static class SalesDetailBean {


        private LocalDate bookedDate;
        private LocalDateTime bookedDateHandler;
        private String businessUnit;
        private String transactionType;
        private String salesType;
        private Long salesOrderId;
        private String region;
        private String salesStore;
        private String psSalesStore;
        private String orderNo;
        private String customerName;
        private String productCode;
        private String productName;
        private String beginCardNo;
        private String endCardNo;
        private Long cardQuantity;
        private Double cardAddValue;
        private Double salesAmount;
        private Double discount = 0.0;
        private Double shippingCost = 0.0;
        private Double cardCostAmount = 0.0;

        public LocalDate getBookedDate() {
            return bookedDate;
        }

        public void setBookedDate(LocalDate bookedDate) {
            this.bookedDate = bookedDate;
        }

        public LocalDateTime getBookedDateHandler() {
            return bookedDateHandler;
        }

        public void setBookedDateHandler(LocalDateTime bookedDateHandler) {
            this.bookedDateHandler = bookedDateHandler;
            this.setBookedDate(bookedDateHandler.toLocalDate());
        }

        public String getBusinessUnit() {
            return businessUnit;
        }

        public void setBusinessUnit(String businessUnit) {
            this.businessUnit = businessUnit;
        }

        public String getSalesType() {
            return salesType;
        }

        public void setSalesType(String salesType) {
            this.salesType = salesType;
        }

        public String getTransactionType() {
            return transactionType;
        }

        public void setTransactionType(String transactionType) {
            this.transactionType = transactionType;
        }

        public Long getSalesOrderId() {
            return salesOrderId;
        }

        public void setSalesOrderId(Long salesOrderId) {
            this.salesOrderId = salesOrderId;
        }

        public String getRegion() {
            return region;
        }

        public void setRegion(String region) {
            this.region = region;
        }

        public String getSalesStore() {
            return salesStore;
        }

        public void setSalesStore(String salesStore) {
            this.salesStore = salesStore;
        }

        public String getPsSalesStore() {
            return psSalesStore;
        }

        public void setPsSalesStore(String psSalesStore) {
            this.psSalesStore = psSalesStore;
        }

        public String getOrderNo() {
            return orderNo;
        }

        public void setOrderNo(String orderNo) {
            this.orderNo = orderNo;
        }

        public String getCustomerName() {
            return customerName;
        }

        public void setCustomerName(String customerName) {
            this.customerName = customerName;
        }

        public String getProductCode() {
            return productCode;
        }

        public void setProductCode(String productCode) {
            this.productCode = productCode;
        }

        public String getProductName() {
            return productName;
        }

        public void setProductName(String productName) {
            this.productName = productName;
        }

        public String getBeginCardNo() {
            return beginCardNo;
        }

        public void setBeginCardNo(String beginCardNo) {
            this.beginCardNo = beginCardNo;
        }

        public String getEndCardNo() {
            return endCardNo;
        }

        public void setEndCardNo(String endCardNo) {
            this.endCardNo = endCardNo;
        }

        public Long getCardQuantity() {
            return this.cardQuantity;
        }

        public void setCardQuantity(Long cardQuantity) {
            this.cardQuantity = cardQuantity;
        }

        public double getCardAddValue() {
            return cardAddValue;
        }

        public void setCardAddValue(double cardAddValue) {
            this.cardAddValue = cardAddValue;
        }

        public Double getSalesAmount() {
            return salesAmount;
        }

        public void setSalesAmount(Double salesAmount) {
            this.salesAmount = salesAmount;
        }

        public Double getDiscount() {
            return discount;
        }

        public void setDiscount(Double discount) {
            this.discount = discount;
        }

        public Double getShippingCost() {
            return shippingCost;
        }

        public void setShippingCost(Double shippingCost) {
            this.shippingCost = shippingCost;
        }

        public Double getCardCostAmount() {
            return cardCostAmount;
        }

        public void setCardCostAmount(Double cardCostAmount) {
            this.cardCostAmount = cardCostAmount;
        }
    }

    //<editor-fold defaultstate="collapsed" desc="FIELD NAMES">
    protected static final String REDEEM_TYPE_FIELD = "redeemType";
    protected static final String CARD_NO_FIELD = "cardNo";
    protected static final String CARD_NAME_FIELD = "cardName";
    protected static final String REDEEM_AMOUNT_FIELD = "redeemAmount";
    protected static final String POS_TRANSACTION_NO_FIELD = "posTransactionNo";
    protected static final String REDEEM_STORE_FIELD = "redeemStore";
    protected static final String PERCENT_FIELD = "percent";
    protected static final String AMOUNT_FIELD = "amount";
    protected static final String BOOKED_DATE_FIELD = "bookedDate";
    protected static final String DISCOUNT_FIELD = "discount";
    protected static final String SHIPPING_COST_FIELD = "shippingCost";
    protected static final String SALE_STORE_FIELD = "saleStore";
    protected static final String CARD_COST_AMOUNT_FIELD = "cardCostAmount";
    protected static final String SALES_AMOUNT_FIELD = "salesAmount";
    protected static final String CARD_QUANTITY_FIELD = "cardQuantity";
    protected static final String SALES_TYPE_FIELD = "salesType";
    protected static final String PS_STORE_FIELD = "psStore";
    protected static final String REGION_FIELD = "region";
    //</editor-fold>
    protected static final String DATE_TO_PARAM = "DATE_TO";
    protected static final String DATE_FROM_PARAM = "DATE_FROM";
    protected static final String DD = "dd";
}
