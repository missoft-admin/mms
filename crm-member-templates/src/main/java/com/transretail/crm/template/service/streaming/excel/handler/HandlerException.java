package com.transretail.crm.template.service.streaming.excel.handler;

/**
 * @author Allan G. Ramirez (agramirez@exist.com)
 */
public class HandlerException extends RuntimeException {
    public HandlerException(Throwable e) {
        super(e);
    }

    public HandlerException(String message) {
        super(message);
    }
}
