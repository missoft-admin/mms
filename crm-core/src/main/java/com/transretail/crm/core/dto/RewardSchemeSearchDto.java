package com.transretail.crm.core.dto;


import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.mysema.query.types.expr.BooleanExpression;
import com.transretail.crm.common.service.dto.request.AbstractSearchFormDto;
import com.transretail.crm.core.entity.QRewardSchemeModel;


public class RewardSchemeSearchDto extends AbstractSearchFormDto {

	@Override
	@JsonIgnore
	public BooleanExpression createSearchExpression() {
		List<BooleanExpression> expr = new ArrayList<BooleanExpression>();
		//QRewardSchemeModel scheme = QRewardSchemeModel.rewardSchemeModel;

		return BooleanExpression.allOf( expr.toArray( new BooleanExpression[expr.size()] ) );
	}

}