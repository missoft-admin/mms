create table CRM_MARKETING_CHANNEL (
        id number(19,0) not null,
        CREATED_BY varchar2(255 char),
        CREATED_DATETIME timestamp,
        LAST_UPDATED_BY varchar2(255 char),
        LAST_UPDATED_DATETIME timestamp,
        code varchar2(20 char),
        description varchar2(100 char),
        ADVERTISEMENT_FILEID varchar2(20 char),
        ADVERTISEMENT_FILENAME varchar2(255 char),
        MEMBER_GROUP varchar2(255 char),
        message varchar2(1000 char),
        PROMOTION varchar2(255 char),
        SCHEDULE date,
        status varchar2(255 char),
        SCHEDULE_TIME date,
        type varchar2(255 char),
        primary key (id)
    );

alter table CRM_MARKETING_CHANNEL 
        add constraint UK_79d3b7b1cjfpm0keh83eu71mx unique (code);

create table CRM_FILE (
        id number(19,0) not null,
        CREATED_BY varchar2(255 char),
        CREATED_DATETIME timestamp,
        LAST_UPDATED_BY varchar2(255 char),
        LAST_UPDATED_DATETIME timestamp,
        FILE_OBJECT blob,
        FILETYPE varchar2(50 char),
        FILENAME varchar2(255 char),
        MODEL_ID varchar2(50 char),
        MODEL_TYPE varchar2(50 char) not null,
        primary key (id)
    );