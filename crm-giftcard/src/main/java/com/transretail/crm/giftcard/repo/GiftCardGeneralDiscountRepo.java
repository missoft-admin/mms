package com.transretail.crm.giftcard.repo;

import org.springframework.stereotype.Repository;

import com.transretail.crm.common.repo.CrmQueryDslPredicateExecutor;
import com.transretail.crm.giftcard.entity.GiftCardGeneralDiscount;

@Repository
public interface GiftCardGeneralDiscountRepo extends CrmQueryDslPredicateExecutor<GiftCardGeneralDiscount, Long> {

}
