package com.transretail.crm.giftcard.dto;

import org.joda.time.LocalDate;


/**
 *
 */
public class GiftCardOrderReceivedSeriesDto {
	
	private String profile;
	private Long startingSeries;
	private Long endingSeries;
	private String deliveryReceipt;
	private LocalDate receivedDate;
	private String receivedBy;
	
	
	public GiftCardOrderReceivedSeriesDto(String profile, Long startingSeries,
			Long endingSeries, String deliveryReceipt, LocalDate receivedDate,
			String receivedBy) {
		super();
		this.profile = profile;
		this.startingSeries = startingSeries;
		this.endingSeries = endingSeries;
		this.deliveryReceipt = deliveryReceipt;
		this.receivedDate = receivedDate;
		this.receivedBy = receivedBy;
	}
	public GiftCardOrderReceivedSeriesDto(String deliveryReceipt, 
			LocalDate receivedDate, String receivedBy) {
		super();
		this.deliveryReceipt = deliveryReceipt;
		this.receivedDate = receivedDate;
		this.receivedBy = receivedBy;
	}
	public String getProfile() {
		return profile;
	}
	public void setProfile(String profile) {
		this.profile = profile;
	}
	public Long getStartingSeries() {
		return startingSeries;
	}
	public void setStartingSeries(Long startingSeries) {
		this.startingSeries = startingSeries;
	}
	public Long getEndingSeries() {
		return endingSeries;
	}
	public void setEndingSeries(Long endingSeries) {
		this.endingSeries = endingSeries;
	}
	
	public String getDeliveryReceipt() {
		return deliveryReceipt;
	}
	public void setDeliveryReceipt(String deliveryReceipt) {
		this.deliveryReceipt = deliveryReceipt;
	}
	public LocalDate getReceivedDate() {
		return receivedDate;
	}
	public void setReceivedDate(LocalDate receivedDate) {
		this.receivedDate = receivedDate;
	}
	public Long getReceived() {
		return endingSeries - startingSeries + 1;
	}
	public String getReceivedBy() {
		return receivedBy;
	}
	public void setReceivedBy(String receivedBy) {
		this.receivedBy = receivedBy;
	}
	
}
