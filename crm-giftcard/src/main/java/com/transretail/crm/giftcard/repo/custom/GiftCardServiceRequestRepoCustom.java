package com.transretail.crm.giftcard.repo.custom;

import com.transretail.crm.giftcard.entity.GiftCardInventory;
import com.transretail.crm.giftcard.entity.GiftCardServiceRequest;
import java.util.List;

/**
 *
 * @author Monte Cillo Co (mco@exist.com)
 */
public interface GiftCardServiceRequestRepoCustom {

    List<GiftCardServiceRequest> findAllServiceRequestOf(GiftCardInventory giftcard);
}
