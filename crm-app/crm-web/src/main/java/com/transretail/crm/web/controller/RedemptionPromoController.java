package com.transretail.crm.web.controller;


import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.transretail.crm.common.util.DateUtil;
import com.transretail.crm.common.web.notification.NotificationType;
import com.transretail.crm.common.web.notification.annotation.Notifiable;
import com.transretail.crm.common.web.notification.annotation.NotificationUpdate;
import com.transretail.crm.core.dto.CampaignDto;
import com.transretail.crm.core.dto.ProductSearchDto;
import com.transretail.crm.core.dto.ProgramDto;
import com.transretail.crm.core.dto.PromotionDto;
import com.transretail.crm.core.dto.PromotionRewardDto;
import com.transretail.crm.core.entity.MemberGroup;
import com.transretail.crm.core.entity.lookup.LookupDetail;
import com.transretail.crm.core.entity.lookup.LookupHeader;
import com.transretail.crm.core.entity.lookup.Product;
import com.transretail.crm.core.entity.lookup.StoreGroup;
import com.transretail.crm.core.security.model.impl.CustomSecurityUserDetailsImpl;
import com.transretail.crm.core.security.util.UserUtil;
import com.transretail.crm.core.service.CodePropertiesService;
import com.transretail.crm.core.service.LookupService;
import com.transretail.crm.core.service.MemberGroupService;
import com.transretail.crm.core.service.ProductService;
import com.transretail.crm.core.service.PromotionService;
import com.transretail.crm.core.service.StoreGroupService;
import com.transretail.crm.web.controller.util.ControllerResponse;


@RequestMapping( "/promotion/redemption" )
@Controller
public class RedemptionPromoController {

	public static final String PATH_CREATE  	    	= "/promotion/redemption/create/view";
	public static final String PATH_VIEW  	    		= "/promotion/view";

	private static final String UIATTR_FORMACTION 		= "promotionFormAction";
	private static final String UIATTR_APPROVALSTATUSES = "promotionStatuses";
	private static final String UIATTR_MKTHEAD			= "isMarketingHead";
	private static final String UIATTR_PROMOPROGRAMS  	= "promotionPrograms";
	private static final String UIATTR_PROMOCAMPAIGNS 	= "promotionCampaigns";
	private static final String UIATTR_PROMODAYS 	  	= "promotionAffectedDays";
	private static final String UIATTR_PROGRAM			= "program";
	private static final String UIATTR_REWARDTYPE		= "rewardType";
	private static final String UIATTR_REWARDREDEEM  	= "rewardRedeemItem";
	private static final String UIATTR_MEMBER_GROUPS 	= "memberGroups";
	private static final String UIATTR_STORE_GROUPS 	= "storeGroups";

	public static final String IS_REDEMPTION 			= "isRedemptionPromo";



	@Autowired
	private PromotionService promotionService;
	@Autowired
	private LookupService lookupService;
	@Autowired
	private CodePropertiesService codePropertiesService;
	@Autowired
	private MemberGroupService memberGroupService;
	@Autowired
	private StoreGroupService storeGroupService;
	@Autowired
	private ProductService productService;
    @Autowired
    private MessageSource messageSource;



	@InitBinder
	void initBinder( WebDataBinder binder ) {
		DateFormat fmt = new SimpleDateFormat( "dd MMM yyyy" );
		fmt.setLenient( false );
		binder.registerCustomEditor( Date.class, new CustomDateEditor( fmt, true ) );

		fmt = new SimpleDateFormat( "dd MMM yyyy" );
		fmt.setLenient( false );
		binder.registerCustomEditor( DateTime.class, new CustomDateEditor( fmt, true ) );
	}



	@RequestMapping( value="/create", produces = "text/html" )
    public ModelAndView create( @ModelAttribute(value="promotion") PromotionDto dto ) {
    	ModelAndView mv = new ModelAndView( PATH_CREATE );
    	mv.addObject( UIATTR_FORMACTION, "/promotion/redemption/save" );

    	populateFields( mv.getModelMap(), dto );
        return mv;
    }

	@RequestMapping( value="/edit/{id}", produces = "text/html" )
    public ModelAndView edit( 
			@PathVariable(value = "id") Long id,
    		@ModelAttribute(value="promotion") PromotionDto dto ) {

    	ModelAndView mv = new ModelAndView( PATH_CREATE );
    	mv.addObject( UIATTR_FORMACTION, "/promotion/redemption/update" );

    	populateFields( mv.getModelMap(), dto );
    	populateAuthorities( mv.getModelMap() );
    	populateTargets( mv.getModelMap() );

		dto = promotionService.getPromotionDto( id );
		mv.addObject( "promotion", dto );
		mv.addObject( UIATTR_PROMOCAMPAIGNS,
				( null != dto.getProgram() )?
						promotionService.getCampaignDtos( dto.getProgram() ) : null );

		CustomSecurityUserDetailsImpl user = UserUtil.getCurrentUser();
		String mktHeadCode = codePropertiesService.getMktHeadCode();
		mv.addObject( UIATTR_MKTHEAD, user.hasAuthority( mktHeadCode ) );
		if ( ( StringUtils.equalsIgnoreCase( dto.getCreateUser(), user.getUsername() )  || user.hasAuthority( mktHeadCode ) ) 
				&& ( StringUtils.isNotBlank( dto.getStatus() ) && !dto.getStatus().equalsIgnoreCase( codePropertiesService.getDetailStatusActive() ) ) ) {
			return mv;
		}

		mv.setViewName( PATH_VIEW );
        return mv;
    }

    @Notifiable(type=NotificationType.REDEMPTION_PROMO)
    @RequestMapping( value="/save", produces="application/json; charset=utf-8", method=RequestMethod.POST )
	public @ResponseBody ControllerResponse save(
			@ModelAttribute(value="promotion") PromotionDto dto, 
			BindingResult br ) {

		ControllerResponse theResp = new ControllerResponse();

    	if ( isValid( dto, br ) ) {
			setPromotionRewards( dto.getPromotionRewards() );
    		promotionService.savePromotionDto( dto );
    		theResp.setSuccess( true );
    		theResp.setResult( dto );
    		theResp.setModelId(dto.getId().toString());
    		theResp.setNotificationCreation(true);
    	}
    	else {
    		theResp.setSuccess( false );
    		theResp.setResult( br.getAllErrors() );
    	}

		return theResp;
	}

    @NotificationUpdate( type=NotificationType.REDEMPTION_PROMO )
	@RequestMapping( value={"/update"}, produces="application/json; charset=utf-8", method=RequestMethod.POST )
    public @ResponseBody ControllerResponse updatePromotion(
    		@ModelAttribute(value="promotion") PromotionDto dto,
    		BindingResult inBinding ) {

		ControllerResponse resp = new ControllerResponse();

		if ( null != dto.getRemarks() && 
				dto.getRemarks().getStatus().equalsIgnoreCase( codePropertiesService.getDetailStatusDisabled() ) ) {
			promotionService.saveRemarkDto( dto.getRemarks() );
    		resp.setSuccess( true );
    		resp.setResult( dto );
		}
		else if ( isValid( dto, inBinding ) ) {
			setPromotionRewards( dto.getPromotionRewards() );
    		promotionService.updatePromotionDto( dto );
			promotionService.saveRemarkDto(dto.getRemarks() );
    		resp.setSuccess( true );
    		resp.setResult( dto );
    		resp.setModelId(dto.getId().toString());
    	}
    	else {
    		resp.setSuccess( false );
    		resp.setResult( inBinding.getAllErrors() );
    	}

		return resp;
    }

    @RequestMapping( value={"/product/name/{sku}"}, produces="application/json; charset=utf-8", method=RequestMethod.GET )
    public @ResponseBody ControllerResponse getProductName( @PathVariable(value = "sku") String sku ) {
    	if ( StringUtils.isNotBlank( sku ) ) {
    		ProductSearchDto searchDto = new ProductSearchDto();
    		searchDto.setSku( sku );
        	Product prd = productService.searchOne( searchDto );
        	if ( null != prd ) {
        		ControllerResponse resp = new ControllerResponse();
        		resp.setResult( prd.getDescription() );
        		resp.setSuccess( true );
        		return resp;
        	}
    	}
		return null;
    }



	private void populateFields( ModelMap mp, PromotionDto inDto ) {
    	populateOtherFields( mp );
    	populateApprovalStatuses( mp );
    	populateFormSelects( mp, inDto );
    	populateOtherForms( mp );
	}

	private void populateOtherFields( ModelMap mp ) {
		mp.addAttribute( IS_REDEMPTION, true );
	}

	private void populateApprovalStatuses( ModelMap mp ) {
		LookupDetail[] theStatuses = new LookupDetail[3];
		for ( LookupDetail theStatus : lookupService.getActiveDetailsByHeaderCode( codePropertiesService.getHeaderStatus() ) ) {
			if ( theStatus.getCode().equalsIgnoreCase( codePropertiesService.getDetailStatusActive() ) ) {
				theStatuses[0] = theStatus;
			}
			else if ( theStatus.getCode().equalsIgnoreCase( codePropertiesService.getDetailStatusRejected() ) ) {
				theStatuses[1] = theStatus;
			}
			else if ( theStatus.getCode().equalsIgnoreCase( codePropertiesService.getDetailStatusDisabled() ) ) {
				theStatuses[2] = theStatus;
			}
		}
		mp.addAttribute( UIATTR_APPROVALSTATUSES, theStatuses );
	}

	private void populateFormSelects( ModelMap mp, PromotionDto inDto ) {
		List<ProgramDto> thePrograms = promotionService.getProgramDtos();
		mp.addAttribute( UIATTR_PROMOPROGRAMS, thePrograms );
		List<CampaignDto> theCampaigns = CollectionUtils.isNotEmpty( thePrograms )?
				promotionService.getCampaignDtos( thePrograms.get(0).getId() ) : null;
		mp.addAttribute( UIATTR_PROMOCAMPAIGNS, theCampaigns );
		if ( CollectionUtils.isNotEmpty( theCampaigns ) ) {
			if ( null == inDto.getStartDate() && null !=  theCampaigns.get(0).getStartDate() ) {
				inDto.setStartDate( new Date( theCampaigns.get(0).getStartDate() ) );
			}
			if ( null == inDto.getEndDate() && null != theCampaigns.get(0).getEndDate() ) {
				inDto.setEndDate( new Date( theCampaigns.get(0).getEndDate() ) );
			}
		}
    	mp.addAttribute( UIATTR_PROMODAYS, 
    			lookupService.getActiveDetails( new LookupHeader( codePropertiesService.getHeaderDays() ) ) );
    	mp.addAttribute( UIATTR_REWARDTYPE, codePropertiesService.getDetailRewardTypeExchangeItem() );
    	mp.addAttribute( UIATTR_REWARDREDEEM, true );
	}

	private void populateOtherForms( ModelMap mp ) {
		mp.addAttribute( UIATTR_PROGRAM, new ProgramDto() );
	}

	private void populateAuthorities( ModelMap mp ) {
		CustomSecurityUserDetailsImpl user = UserUtil.getCurrentUser();
		String mktHeadCode = codePropertiesService.getMktHeadCode();
		mp.addAttribute( UIATTR_MKTHEAD, user.hasAuthority( mktHeadCode ) );
	}

	private void populateTargets( ModelMap mp ) {
		Map<Long, String> targetMember = new HashMap<Long, String>();
		if(CollectionUtils.isNotEmpty( memberGroupService.getAllMemberGroups() ) ) {
			for(MemberGroup group : memberGroupService.getAllMemberGroups() )
				targetMember.put(group.getId(), group.getName() );	
		}
		mp.addAttribute( UIATTR_MEMBER_GROUPS, targetMember );
    	
    	Map<Long, String> targetStore = new HashMap<Long, String>();
    	if(CollectionUtils.isNotEmpty( storeGroupService.getAllGroups() ) ) {
			for(StoreGroup group : storeGroupService.getAllGroups() )
				targetStore.put(group.getId(), group.getName() );
    	}
    	mp.addAttribute( UIATTR_STORE_GROUPS, targetStore );
	}

	private boolean isValid( PromotionDto dto, BindingResult br ) {

		CampaignDto theCampaign = promotionService.getCampaignDto( dto.getCampaign() );

		if ( StringUtils.isBlank( dto.getName() ) ) {
    		br.reject( messageSource.getMessage( "promotion_msg_nameempty", null, LocaleContextHolder.getLocale() ) );
    	}

    	if ( null == dto.getCampaign() ) {
    		br.reject( messageSource.getMessage( "promotion_msg_campaignempty", null, LocaleContextHolder.getLocale() ) );
    	}

    	if ( null != dto.getStartDate() && null != dto.getEndDate() ) {
    		if ( dto.getStartDate().after( dto.getEndDate() ) ) {
    			br.reject( messageSource.getMessage( "promotion_msg_daterangeinvalid", null, LocaleContextHolder.getLocale() ) );
    		}
    	}

		if ( null != dto.getStartDate() ) {
			if ( null != theCampaign && null !=  theCampaign.getStartDate() 
					&& dto.getStartDate().getTime() < ( theCampaign.getStartDate() ) ) {
				br.reject( messageSource.getMessage( "promotion_msg_startdatebefore", null, LocaleContextHolder.getLocale() ) );
			}
		}

		if ( null != dto.getEndDate() ) {
			if ( null != theCampaign && null !=  theCampaign.getEndDate() 
					&& dto.getEndDate().getTime() > ( theCampaign.getEndDate() ) ) {
				br.reject( messageSource.getMessage( "promotion_msg_enddateafter", null, LocaleContextHolder.getLocale() ) );
			}
		}

    	if ( StringUtils.isNotBlank( dto.getStartTime() ) && StringUtils.isNotBlank( dto.getEndTime() ) ) {
    		if ( DateUtil.convertLocalTimeString(DateUtil.TIME_FORMAT, dto.getStartTime() ).isAfter(
    				DateUtil.convertLocalTimeString(DateUtil.TIME_FORMAT, dto.getEndTime() ) ) ) {
    			br.reject( messageSource.getMessage( "promotion_msg_timerangeinvalid", null, LocaleContextHolder.getLocale() ) );
    		}
    	}

    	List<PromotionRewardDto> rewards = new ArrayList<PromotionRewardDto>();
    	if ( CollectionUtils.isNotEmpty( dto.getPromotionRewards() ) ) {
			for ( PromotionRewardDto theDto : dto.getPromotionRewards() ) {				
				if ( null == theDto.getBaseAmount() && null == theDto.getBaseFactor() && null == theDto.getDiscount()
					&& null == theDto.getMaxAmount() && null == theDto.getMaxBonus() && null == theDto.getMaxQty() 
					&& null == theDto.getMinAmount() && null == theDto.getMinQty() ) {
					continue;
				}
				rewards.add( theDto );
			}
			dto.setPromotionRewards( rewards );
		}
    	if ( CollectionUtils.isNotEmpty( dto.getPromotionRewards() ) ) {
    		dto.setRewardType( codePropertiesService.getDetailRewardTypeExchangeItem() );
    		if( dto.getRewardType().equalsIgnoreCase( codePropertiesService.getDetailRewardTypeExchangeItem() ) ) {
    			for ( PromotionRewardDto rwd : dto.getPromotionRewards() ) {
    				ProductSearchDto searchDto = new ProductSearchDto();
    				searchDto.setSku( rwd.getProductCode() );
    				if ( StringUtils.isNotBlank( rwd.getProductCode() ) && !productService.doExist( searchDto ) ) {
    					br.reject( messageSource.getMessage( "promo_redemption_msg_invalid_prdsku", null, LocaleContextHolder.getLocale() )
    							+ rwd.getProductCode() );
    				}
    				if ( null == rwd.getBaseFactor() ) {
    					br.reject( messageSource.getMessage( "promo_redemption_msg_empty_pt", null, LocaleContextHolder.getLocale() )
    							+ rwd.getProductCode() );
    				}
    			}	
    		}
    	}

    	return !br.hasErrors();
	}

	private void setPromotionRewards( List<PromotionRewardDto> dtos ) {
		if ( CollectionUtils.isEmpty( dtos ) ) {
			return;
		}

		for ( PromotionRewardDto dto : dtos ) {
			dto.setRewardType( codePropertiesService.getDetailRewardTypeExchangeItem() );
			dto.setFixed( true );
		}
	}

}
