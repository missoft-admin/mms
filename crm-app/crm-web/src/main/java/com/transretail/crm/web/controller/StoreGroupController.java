package com.transretail.crm.web.controller;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.google.common.collect.Lists;
import com.transretail.crm.common.service.dto.request.PageSortDto;
import com.transretail.crm.common.service.dto.response.ResultList;
import com.transretail.crm.core.dto.StoreGroupDto;
import com.transretail.crm.core.dto.StoreGrpSearchDto;
import com.transretail.crm.core.entity.lookup.Store;
import com.transretail.crm.core.entity.lookup.StoreGroup;
import com.transretail.crm.core.service.StoreGroupService;
import com.transretail.crm.core.service.StoreService;
import com.transretail.crm.web.controller.util.ControllerResponse;

@RequestMapping("/groups/store")
@Controller
public class StoreGroupController {
	private static final String UI_ATTR_GROUPS   	= "groupsList";
	private static final String UI_ATTR_ITEMS   	= "itemsList";
	
	private static final String UI_ATTR_GROUP_FORM  = "groupForm";
	
	@Autowired
	StoreGroupService storeGroupService;
	
	@Autowired
	StoreService storeService;
	
	@Autowired
	MessageSource messageSource;
	
	@RequestMapping(params={"page", "size"}, produces = "text/html")
    public String list(
            @RequestParam(value = "page", required = false) Integer page,
            @RequestParam(value = "size", required = false) Integer size,
            Model uiModel) {
		List<Store> stores = Lists.newArrayList(storeService.getAllStores());
		
		uiModel.addAttribute(UI_ATTR_ITEMS, stores);
		uiModel.addAttribute(UI_ATTR_GROUP_FORM, new StoreGroup());
        
		return "storegroups/list";
    }
	
	@RequestMapping(produces = "text/html")
    public String list(Model uiModel) {
		List<Store> stores = Lists.newArrayList(storeService.getAllStores());
		
		uiModel.addAttribute(UI_ATTR_ITEMS, stores);
		uiModel.addAttribute(UI_ATTR_GROUP_FORM, new StoreGroup());
        
		return "storegroups/list";
    }
	
	@RequestMapping(value = "/list", method = RequestMethod.POST)
	public @ResponseBody ResultList<StoreGroup> list(@RequestBody PageSortDto dto) {
		ResultList<StoreGroup>  list = storeGroupService.listStoreGroups(dto);
		for ( StoreGroup storeGroup : list.getResults() ) {
			storeGroup.setStoreModels( storeGroupService.getItems( storeGroup.getId() ) );
		}
		return list;
	}

    @RequestMapping(value = "/search/list", method = RequestMethod.POST)
    public @ResponseBody ResultList<StoreGroupDto> searchList( @RequestBody StoreGrpSearchDto dto ) {
    	return storeGroupService.search( dto );
    }
	
	@RequestMapping(value = "/items/list/{id}", method = RequestMethod.POST)
	public @ResponseBody ResultList<Store> list(
			@PathVariable("id") Long id,
			@RequestBody PageSortDto dto) {
		return storeGroupService.listStores(id, dto);
	}
	
	
	@RequestMapping(method = RequestMethod.POST, produces = "application/json; charset=utf-8")
	@ResponseBody
    public ControllerResponse create(@ModelAttribute("groupForm") StoreGroup group,
            BindingResult bindingResult) {
		
		ControllerResponse theResponse = new ControllerResponse();
		theResponse.setSuccess(true);
		
		validate(group, bindingResult);
		
		if (bindingResult.hasErrors()) {
			theResponse.setSuccess(false);
			theResponse.setResult(bindingResult.getAllErrors());
			return theResponse;
		}

		if (theResponse.isSuccess()) {
				storeGroupService.createStoreGroup(group);
		}

		return theResponse;
    }
	
	private void validate(StoreGroup group, BindingResult bindingResult) {
		if(StringUtils.isBlank(group.getName()))
			bindingResult.reject(getMessage("propkey_msg_notempty", new Object[] {"label_group_groupname"}));
		
		if(StringUtils.isBlank(group.getStores()))
			bindingResult.reject(getMessage("propkey_msg_notempty", new Object[] {"label_group_stores"}));
	}
	
	private String getMessage(String inMsgCode, Object[] inArgMsgCodes) {
		if(inArgMsgCodes != null) {
			for ( int i=0; i < inArgMsgCodes.length; i++ ) {
				inArgMsgCodes[i] = messageSource.getMessage( inArgMsgCodes[i].toString().toLowerCase(), null, LocaleContextHolder.getLocale() );
			}
		}
		return messageSource.getMessage(inMsgCode, inArgMsgCodes, "Info given is invalid.", LocaleContextHolder.getLocale());
	}
	
	@RequestMapping(value = "/{id}", method = RequestMethod.POST, produces = "application/json; charset=utf-8")
	@ResponseBody
    public ControllerResponse update(@ModelAttribute("groupForm") StoreGroup group,
    		@PathVariable("id") Long id,
            BindingResult bindingResult) {
		
		ControllerResponse theResponse = new ControllerResponse();
		theResponse.setSuccess(true);
		
		StoreGroup storeGroup = storeGroupService.findOne(id);
		storeGroup.setName(group.getName());
		storeGroup.setStores(group.getStores());
		
		validate(group, bindingResult);
		
		if (bindingResult.hasErrors()) {
			theResponse.setSuccess(false);
			theResponse.setResult(bindingResult.getAllErrors());
			return theResponse;
		}

		if (theResponse.isSuccess()) {
				storeGroupService.saveStoreGroup(storeGroup);
		}

		return theResponse;
    }
	
	@RequestMapping(value = "/populate/{id}", produces = "application/json; charset=utf-8")
    @ResponseBody
    public ControllerResponse ajaxPopulate(
    		@PathVariable("id") Long id) {

    	ControllerResponse theResponse = new ControllerResponse();    	
    	theResponse.setSuccess(true);

    	if (id != null) {
    			theResponse.setResult(storeGroupService.findOne(id));
    	}
    	
        return theResponse;
    }
	
    @RequestMapping(value = "/searchitems", params = {"searchStr"}, 
	    method = RequestMethod.GET, produces = "application/json; charset=utf-8")
    @ResponseBody
    public ControllerResponse searchItems(@RequestParam(value = "searchStr", required = false) String searchStr) {
	ControllerResponse theResponse = new ControllerResponse();
	theResponse.setSuccess(true);
	List<Store> stores = Lists.newArrayList(storeGroupService.searchItems(searchStr.trim()));
	Collections.sort(stores, new Comparator<Store>() {
	    @Override
	    public int compare(Store o1, Store o2) {
		return o1.getCode().compareTo(o2.getCode());
	    }
	});

	theResponse.setResult(stores);

	return theResponse;
    }
	
	@RequestMapping(value = "/{id}", method = RequestMethod.DELETE, produces = "text/html")
    public String delete(
    		@PathVariable("id") Long id,
            @RequestParam(value = "page", required = false) Integer page,
            @RequestParam(value = "size", required = false) Integer size,
            Model uiModel) {
        storeGroupService.delete(id);
        uiModel.asMap().clear();
        uiModel.addAttribute("page", (page == null) ? "1" : page.toString());
        uiModel.addAttribute("size", (size == null) ? "10" : size.toString());
        return "redirect:/groups/store";
    }
	
	@RequestMapping(value = "/items/{id}", produces = "text/html")
	public String show(
    		@PathVariable("id") Long id,
            Model uiModel) {
        
			uiModel.addAttribute(UI_ATTR_ITEMS, storeGroupService.getItems(id));
		
		return "storegroups/items";
    }
	
	@RequestMapping(value = "/dialog", method = RequestMethod.GET, produces = "text/html")
    public String dialog(Model uiModel) {
		List<Store> stores = Lists.newArrayList(storeService.getAllStores());
	
		uiModel.addAttribute(UI_ATTR_ITEMS, stores);
		uiModel.addAttribute(UI_ATTR_GROUP_FORM, new StoreGroup());
        
		return "storegroups/dialog";
    }

	@RequestMapping(value = "/list", method = RequestMethod.GET, produces = "application/json; charset=utf-8")
    @ResponseBody
	public ControllerResponse list() {
		ControllerResponse theResponse = new ControllerResponse();    	
    	theResponse.setSuccess(true);
    	
    	theResponse.setResult(storeGroupService.findAll());
    		
    	return theResponse;
	}

	@RequestMapping( value = "/view/{id}", produces = "text/html" )
	public String view(
    		@PathVariable("id") Long inId,
            Model inUiModel) {

		if ( null != inId ) {
			StoreGroup group = storeGroupService.findOne(inId);
			group.setStoreModels(storeGroupService.getItems(inId));
			inUiModel.addAttribute( "storeGroup", group);
		}
		return "/groups/store/view";
    }
}
