package com.transretail.crm.core.service.impl;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Configurable;
import org.springframework.data.domain.Page;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import com.transretail.crm.core.entity.UserRoleModel;
import com.transretail.crm.core.repo.UserRoleRepo;
import com.transretail.crm.core.service.UserRoleModelService;
import com.transretail.crm.core.service.impl.UserRoleModelServiceImpl;
import com.transretail.crm.core.util.StringUtility;

@Configurable
@ContextConfiguration(locations = "classpath*:/META-INF/spring/applicationContext*.xml")
@Transactional
@RunWith(SpringJUnit4ClassRunner.class)
public class UserRoleModelServiceTest {
	
	UserRoleModel userRoleModel;
	UserRoleModel userRoleModel2;
	List<UserRoleModel> userRoleModels;
	
	@Mock
	private UserRoleRepo userRoleRepo;
	
	@InjectMocks
	UserRoleModelService userRoleModelService = new UserRoleModelServiceImpl();
	
	@Before
	public void setup() {
        userRoleModel = new UserRoleModel("ROLE1");
        
        userRoleModel2 = new UserRoleModel("ROLE2");
        
        userRoleModels = new ArrayList<UserRoleModel>();
        userRoleModels.add(userRoleModel);
        userRoleModels.add(userRoleModel2);
        
        MockitoAnnotations.initMocks(this);
    }
	
	@Test
	public void testCountAllUserRoleModels() {
		long expectedCount = 1000L;
		Mockito.when(userRoleRepo.count()).thenReturn(expectedCount);
		long returned = userRoleModelService.countAllUserRoleModels();
		Assert.assertEquals(expectedCount, returned);
	}
	
	@Test
	public void testDeleteUserRoleModel() {
		userRoleModelService.deleteUserRoleModel(userRoleModel);
		Mockito.verify(userRoleRepo, Mockito.times(1)).delete(userRoleModel);
		Mockito.verifyNoMoreInteractions(userRoleRepo);
	}
	
	@Test
	public void testFindUserRoleModel() {
		String id = "100";
		Mockito.when(userRoleRepo.findOne(id)).thenReturn(userRoleModel);
		UserRoleModel returned = userRoleModelService.findUserRoleModel(id);
		Assert.assertEquals(userRoleModel, returned);
	}
	
	@Test
	public void testFindAllUserRoleModels() {
		Mockito.when(userRoleRepo.findAll()).thenReturn(userRoleModels);
		List<UserRoleModel> returned = userRoleModelService.findAllUserRoleModels();
		Assert.assertEquals(userRoleModels, returned);
		Assert.assertEquals(2, userRoleModels.size());
	}
	
	@Test
	public void testFindUserRoleModelEntries() {
		int firstResult = 0;
		int maxResult = 5;
		Page<UserRoleModel> userRoleModelPage = (Page<UserRoleModel>) mock(Page.class);
		
		when(userRoleRepo.findAll(new org.springframework.data.domain.PageRequest(firstResult / maxResult, maxResult))).thenReturn(userRoleModelPage);
		when(userRoleModelPage.getContent()).thenReturn(userRoleModels);
		
		List<UserRoleModel> returned = userRoleModelService.findUserRoleModelEntries(firstResult, maxResult);
		
		Assert.assertEquals(userRoleModels, returned);
		Assert.assertEquals(2, userRoleModels.size());
	}
	
	@Test
	public void testSaveUserRoleModel() {
		userRoleModelService.saveUserRoleModel(userRoleModel);
		Mockito.verify(userRoleRepo, Mockito.times(1)).save(userRoleModel);
		Mockito.verifyNoMoreInteractions(userRoleRepo);
	}
	
	@Test
	public void testUpdateUserRoleModel() {
		Mockito.when(userRoleRepo.save(userRoleModel)).thenReturn(userRoleModel2);
		UserRoleModel returned = userRoleModelService.updateUserRoleModel(userRoleModel);
		Assert.assertEquals(userRoleModel2, returned);
	}
	
	@Test
	public void testFindByIdSet() throws Exception {
		String[] ids = {"100", "101", "102"};
		HashSet<UserRoleModel> expected = new HashSet<UserRoleModel>(userRoleModels);
		Mockito.when(userRoleRepo.findByIdSet(Arrays.asList(ids))).thenReturn(userRoleModels);
		Set<UserRoleModel> returned = userRoleModelService.findByIdSet(ids);
		Assert.assertEquals(expected, returned);
	}

}
