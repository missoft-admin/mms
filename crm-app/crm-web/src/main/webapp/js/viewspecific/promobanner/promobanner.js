$(document).ready(function() {
	list = $("#promoBannerList");
	
	var columnHeaders = [
	    list.data("hdr-name"),
	    list.data('hdr-startdate'),
	    list.data('hdr-enddate'),
	    list.data('hdr-status'),
	    list.data('hdr-link'),
	    list.data('hdr-type'),
	    ''
	];
	
	var modelFields = [
	    {name: 'name', sortable: true},
	    {name: 'startDate', sortable: true, fieldCellRenderer: function(data, type, row) {
	    	return (row.startDateString ? row.startDateString : "");
	    }},
	    {name: 'endDate', sortable: true, fieldCellRenderer: function(data, type, row) {
	    	return row.endDateString ? row.endDateString : "";
	    }},
	    {name: 'status', fieldCellRenderer: function(data, type, row) {
	    	return row.status ? (row.status.description == 'ACTIVE' ? 
	    			'APPROVED' : row.status.description) : "";
	    }},
	    {name: 'link', sortable: true, fieldCellRenderer: function(data, type, row) {
	    	return row.link != null && row.link.length > 50 ? row.link.substring(0, 50) + "..." : row.link;
	    }},
	    {name: 'type', sortable: true},
	    {customCell: function(innerData, sSpecific, json) {
	    	if(sSpecific == 'display') {
	    		return $("#action_buttons").html().replace(new RegExp("%ID%", 'g'), json.id);
	    	}
	    	else {
	    		return '';
	    	}
	    }}
	];
	
	list.ajaxDataTable({
		'autoload': true,
		'ajaxSource': list.data('url'),
		'columnHeaders': columnHeaders,
		'modelFields': modelFields
	});
//	.on('click', '#promoUpdate', updatePromoBanner);
	
//	$("#create").click(updatePromoBanner);
});

function updatePromoBanner(e) {
	$.ajax({
		cache 		: false, 
		dataType 	: "html",
		type 		: "GET", 
		url 		: $(this).data("url") + $( this ).data( "id" ),
		error 		: function(jqXHR, textStatus, errorThrown) {}, 
		success 	: function( inResponse ) {
			$( "#form" ).html( inResponse );
		}
	});
}

function savePromoBanner() {
	var model = $("#promoBanner");
	$.ajax({
		cache	: false,
		dataType: 'json',
		type	: 'POST',
		url		: model.attr("action"),
		data	: model.serialize(),
		succsess: function(response) {
			if(!response.success) {
				return;
			}
			$("#updateForm").modal('hide');
			$("#updateForm").remove();
			window.location.reload();
		},
		error	: function(jqXHR, textStatus, errorThrown) {
			
		}
	});
}