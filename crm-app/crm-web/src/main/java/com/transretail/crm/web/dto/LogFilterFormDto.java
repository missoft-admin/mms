package com.transretail.crm.web.dto;

import org.hibernate.validator.constraints.NotEmpty;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.transretail.crm.common.web.dto.LogFilterDto;

/**
 * @author Allan G. Ramirez (agramirez@exist.com)
 */
public class LogFilterFormDto extends LogFilterDto {
    @NotEmpty
    private String crmInStoreId;

    public String getCrmInStoreId() {
        return crmInStoreId;
    }

    public void setCrmInStoreId(String crmInStoreId) {
        this.crmInStoreId = crmInStoreId;
    }
}
