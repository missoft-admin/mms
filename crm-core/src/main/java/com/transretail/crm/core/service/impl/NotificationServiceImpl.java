package com.transretail.crm.core.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.transretail.crm.core.entity.Notification;
import com.transretail.crm.core.entity.NotificationStatus;
import com.transretail.crm.core.entity.QNotification;
import com.transretail.crm.core.entity.QNotificationStatus;
import com.transretail.crm.core.entity.QUserModel;
import com.transretail.crm.core.entity.UserModel;
import com.transretail.crm.core.repo.NotificationRepo;
import com.transretail.crm.core.repo.NotificationStatusRepo;
import com.transretail.crm.core.repo.UserRepo;
import com.transretail.crm.core.service.NotificationService;
import com.transretail.crm.core.service.UserService;
import com.transretail.crm.core.util.CalendarUtil;

@Service("notificationService")
@Transactional
public class NotificationServiceImpl implements NotificationService {
	@Autowired
	private NotificationRepo notificationRepo;
	@Autowired
	private NotificationStatusRepo statusRepo;
	@Autowired
	private UserService userService;

	@Override
	public Notification saveNotification(Notification notification, List<UserModel> users) {
		if(notification.getItemId() == null || notificationRepo.hasDuplicateNotification(notification)) {
			return null;
		}
		notification = notificationRepo.save(notification);
		
		List<NotificationStatus> statuses = new ArrayList<NotificationStatus>();
		for(UserModel user : users) {
			NotificationStatus status = new NotificationStatus();
			status.setRead(false);
			status.setUser(user);
			status.setNotification(notification);
			statuses.add(status);
		}
		
		statusRepo.save(statuses);
		notification.setStatuses(statuses);
		return notificationRepo.save(notification);
	}

	@Override
	public List<Notification> getNotificationsByUser(UserModel user) {
		return notificationRepo.getNotifications(user);
	}
	
	public List<Notification> getNotificationByUser(UserModel user, int firstResult, int size) {
		return notificationRepo.getNotifications(user, firstResult / size, size);
	}

	@Override
	public List<Notification> getUnreadNotifications(UserModel user) {
		return notificationRepo.getUnreadNotifications(user);
	}
	
	public List<Notification> getUnreadNotifications(UserModel user, int firstResult, int size) {
		return notificationRepo.getUnreadNotifications(user, firstResult / size, size);
	}

	@Override
	public void setNotificationAsRead(Notification notification, UserModel user) {
		QNotificationStatus status = QNotificationStatus.notificationStatus;
		Iterable<NotificationStatus> list = statusRepo.findAll(status.notification.eq(notification)
				.and(status.user.eq(user)).and(status.read.eq(false)));
		
		for(NotificationStatus notificationStatus : list) {
			notificationStatus.setRead(true);
		}
		statusRepo.save(list);
	}

	@Override
	public void deleteNotification(String itemId) {
		deleteNotification(notificationRepo.findByItemId(itemId));
	}
	
/*	@Override
	public void deleteNotification(List<String> itemIds) {
		for(String id : itemIds) {
			deleteNotification(id);
		}
	}*/
	
	@Override
	public void deleteNotification(List<Notification> notifications) {
		if(CollectionUtils.isEmpty(notifications)) {
			return;
		}
		QNotificationStatus status = QNotificationStatus.notificationStatus;
		Iterable<NotificationStatus> list = statusRepo.findAll(status.notification.in(notifications));
		statusRepo.deleteInBatch(list);
		notificationRepo.delete(notifications);
	}
}
