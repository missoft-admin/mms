package com.transretail.crm.core.repo;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.transretail.crm.common.repo.CrmQueryDslPredicateExecutor;
import com.transretail.crm.core.entity.MemberGroup;
import com.transretail.crm.core.entity.MemberGroupField;

@Repository
public interface MemberGroupFieldRepo extends CrmQueryDslPredicateExecutor<MemberGroupField, Long> {
	List<MemberGroupField> findByMemberGroup(MemberGroup group);
}
