package com.transretail.crm.report.template;

import com.transretail.crm.common.reporting.engine.data.ReportDataSource;
import java.util.List;
import java.util.Map;

/**
 *
 * @author Monte Cillo Co (mco@exist.com)
 */
public interface ValidatableReportTemplate extends ReportTemplate {

    List<String> validate(Map<String, String> parameters);

}
