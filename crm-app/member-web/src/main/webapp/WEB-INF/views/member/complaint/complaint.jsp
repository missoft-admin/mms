<%@include file="../../taglibs.jsp" %>


<div id="content_complaint">

  <div class="page-header page-header2"><h1><spring:message code="complaint_pageheader" /></h1></div>

  <div class="">
    <div class="pull-right">
      <a id="link_create" data-action="<c:url value="${complaintAction}"/>" data-modal-header="${complaintHeader}" 
        href="<c:url value="/member/complaint/form/new" />" class="btn btn-primary">
        <spring:message code="complaint_lbl_create" />
      </a>
      <div id="form_complaint"><jsp:include page="complaintForm.jsp"/></div>
    </div>
    <div class="clearfix"></div>
  </div>

  <div id="list_complaints" class=""><jsp:include page="complaintList.jsp"/></div>


<style type="text/css"> <!-- #complaintSearchForm .btn { margin-left: 10px; } --> </style>
<script type='text/javascript'>
var Complaint = null;
var ComplaintCommon = null;
var ComplaintForm = null

$(document).ready( function() {

    var $main = $( "#content_complaint" );
    var $createLink = $main.find( "#link_create" );
    var $contentForm = $main.find( "#form_complaint" );

    initLinks();

    function initLinks() {
        $createLink.click( function(e) {
            e.preventDefault();
            ComplaintForm.show();
        });
    }

    Complaint = {
        loadForm  : function( resp ) {
            $contentForm.html( resp );
            ComplaintForm.show();
        }
    };

    ComplaintCommon = {
        processResp  : function( resp, $dialog, errorContainer, $save ) {
            $save.prop( "disabled", false );
            if ( resp.success ) {
                $dialog.modal( "hide" );
            }
            else {
                var errors = "";
                $( resp.result ).each( function( key, value ) { errors += ( ( key + 1 ) + ". " + value.code + "<br>" ); });
                errorContainer.find( "div" ).html( errors );
                errorContainer.show( "slow" );
            }
        },
        reset     : function( ele, isEmpty ) {
            $( ele ).find(':input').each( function() {
                switch(this.type) {
                    case 'password':
                    case 'select-one':
                    case 'text':
                    case 'textarea':
                    case 'file':
                    case 'hidden': $(this).val(''); break;
                    case 'select-multiple': isEmpty? $(this).empty() : $(this).val(""); break;
                    case 'checkbox':
                    case 'radio': this.checked = false;
                }
            });
        }
    };

    Date.prototype.customize= function( pattern ) {
    	  customizeMonth = function ( mth ) {
    	    switch(mth) {
    	    case(0): return "January";
    	    case(1): return "February";
    	    case(2): return "March";
    	    case(3): return "April";
    	    case(4): return "May";
    	    case(5): return "June";
    	    case(6): return "July";
    	    case(7): return "August";
    	    case(8): return "September";
    	    case(9): return "October";
    	    case(10): return "November";
    	    case(11): return "December";
    	    }
    	  };
    	  customizeDigit = function( dgt ) {
    	    if( dgt != null && dgt != undefined && ( dgt + "" ).length < 2 ) {
    	      return "0" + dgt;
    	    }
    	    return dgt;
    	  };
    	  customizeDay = function( day ) {
    	    return ( day + ( (day > 20 || day < 10) ? ([false, "st", "nd", "rd"])[(day%10)] || "th" : "th" ) );
    	  };

    	  if ( this ) {
    	    switch( pattern ) {
    	      case(0) : return customizeDay( this.getDate() ) 
    	        + " " + customizeMonth( this.getMonth() ) 
    	        + " " + this.getFullYear() 
    	        + " " + customizeDigit( this.getHours() ) 
    	        + ":" + customizeDigit( this.getMinutes() ) 
    	        + ":" + customizeDigit( this.getSeconds() );
    	      case(1) : return customizeDay( this.getDate() ) 
    	        + " " + customizeMonth( this.getMonth() ) 
    	        + " " + this.getFullYear();
    	      default : return this;
    	    }
    	    
    	  }
    	};

});
</script>


</div>