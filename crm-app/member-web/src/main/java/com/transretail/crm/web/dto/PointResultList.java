package com.transretail.crm.web.dto;

import java.util.Collection;

import com.transretail.crm.common.service.dto.response.AbstractResultListDTO;

/**
 * @author Allan G. Ramirez (agramirez@exist.com)
 */
public class PointResultList extends AbstractResultListDTO<PointDto> {
    public PointResultList(Collection<PointDto> results, long totalElements, boolean hasPreviousPage, boolean hasNextPage) {
        super(results, totalElements, hasPreviousPage, hasNextPage);
    }
}
