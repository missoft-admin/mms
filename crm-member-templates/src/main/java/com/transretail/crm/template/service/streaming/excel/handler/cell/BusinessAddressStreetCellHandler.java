package com.transretail.crm.template.service.streaming.excel.handler.cell;

import com.transretail.crm.core.entity.MemberModel;
import com.transretail.crm.core.entity.embeddable.Address;
import com.transretail.crm.core.entity.embeddable.ProfessionalProfile;
import com.transretail.crm.template.service.MemberTemplateParseException;

/**
 * @author Allan G. Ramirez (agramirez@exist.com)
 */
public class BusinessAddressStreetCellHandler implements CellHandler {
    @Override
    public void handle(MemberModel dto, String cellText) throws MemberTemplateParseException {
        ProfessionalProfile professionalProfile = dto.getProfessionalProfile();
        if (professionalProfile == null) {
            professionalProfile = new ProfessionalProfile();
            professionalProfile.setBusinessAddress(new Address());
            dto.setProfessionalProfile(professionalProfile);
        }
        if (professionalProfile.getBusinessAddress() == null) {
            professionalProfile.setBusinessAddress(new Address());
        }
        professionalProfile.getBusinessAddress().setStreet(cellText);
    }
}
