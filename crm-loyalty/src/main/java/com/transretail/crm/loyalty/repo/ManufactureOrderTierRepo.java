package com.transretail.crm.loyalty.repo;

import org.springframework.stereotype.Repository;

import com.transretail.crm.common.repo.CrmQueryDslPredicateExecutor;
import com.transretail.crm.loyalty.entity.ManufactureOrderTier;

@Repository
public interface ManufactureOrderTierRepo extends CrmQueryDslPredicateExecutor<ManufactureOrderTier, Long> {

}
