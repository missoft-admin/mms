package com.transretail.crm.core.entity.enums;

public enum DataAnalysisLMHAnalysisBy {

	SALES_VALUE("salesValue"),
	/*BASKET_SIZE_PER_VISIT("basketSizePerVisit"),*/
	SHOPPER_COUNT("shopperCount"),
	AVG_VISIT_FREQ("avgVisitFreq");

    private DataAnalysisLMHAnalysisBy(String code){
    	this.code = code;
    }

    private final String code;

	public String getCode() {
		return code;
	}
    
    
}
