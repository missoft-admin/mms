package com.transretail.crm.core.service.impl;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Configurable;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import com.transretail.crm.common.web.notification.NotificationType;
import com.transretail.crm.core.entity.Notification;
import com.transretail.crm.core.entity.NotificationStatus;
import com.transretail.crm.core.entity.UserModel;
import com.transretail.crm.core.dto.EmployeePurchaseTxnDto;
import com.transretail.crm.core.entity.Notification;
import com.transretail.crm.core.entity.NotificationStatus;
import com.transretail.crm.core.entity.UserModel;
import com.transretail.crm.core.entity.lookup.Store;
import com.transretail.crm.core.repo.NotificationRepo;
import com.transretail.crm.core.repo.NotificationStatusRepo;
import com.transretail.crm.core.repo.UserRepo;
import com.transretail.crm.core.service.NotificationService;

@Configurable
@ContextConfiguration(locations = "classpath*:/META-INF/spring/applicationContext*.xml")
@Transactional
@RunWith(SpringJUnit4ClassRunner.class)
public class NotificationServiceImplTest {
	@Autowired
	private NotificationRepo notificationRepo;
	@Autowired
	private NotificationStatusRepo statusRepo;
	@Autowired
	private UserRepo userRepo;
	@Autowired
	private NotificationService notificationService;
	
	private UserModel user, user2;
	private Notification notif;
	private String itemId;
	
	@Before
	public void setUp() {
		user = new UserModel();
		user.setUsername("a");
		user.setPassword("");
		user.setEnabled(true);
		user.setStoreCode( "store1" );
		user = userRepo.save(user);
		
		user2 = new UserModel();
		user2.setUsername("b");
		user2.setPassword("");
		user2.setEnabled(true);
		user2.setStoreCode( "store1" );
		user2 = userRepo.save(user2);
		
		itemId = "1";
		
		Notification notification = new Notification();
		notification.setType(NotificationType.PROMOTION);
		notification.setItemId(itemId);
		notification = notificationRepo.save(notification);
		notif = notification;
		
		Notification notification1 = new Notification();
		notification1.setType(NotificationType.PROMOTION);
		notification1 = notificationRepo.save(notification1);
		
		Notification notification2 = new Notification();
		notification2.setType(NotificationType.POINTS);
		notification2 = notificationRepo.save(notification2);
		
		List<NotificationStatus> statuses = new ArrayList<NotificationStatus>();
		NotificationStatus status = new NotificationStatus();
		status.setRead(false);
		status.setUser(user);
		status.setNotification(notification);
		statuses.add(statusRepo.save(status));
		notification.setStatuses(statuses);
		notificationRepo.save(notification);
		statuses = new ArrayList<NotificationStatus>();
		
		status = new NotificationStatus();
		status.setRead(true);
		status.setUser(user);
		status.setNotification(notification1);
		statuses.add(statusRepo.save(status));
		notification1.setStatuses(statuses);
		notificationRepo.save(notification1);
		statuses = new ArrayList<NotificationStatus>();
		
		status = new NotificationStatus();
		status.setRead(true);
		status.setUser(user2);
		status.setNotification(notification2);
		statuses.add(statusRepo.save(status));
		notification2.setStatuses(statuses);
		notificationRepo.save(notification2);
		statuses = new ArrayList<NotificationStatus>();
	}

	@Test
	public void testGetNotificationsByUser() {
		List<Notification> list = notificationService.getNotificationsByUser(user);
		assertEquals(2, list.size());
		for(Notification notif : list) {
			assertEquals(NotificationType.PROMOTION, notif.getType());
		}
		list = notificationService.getNotificationsByUser(user2);
		assertEquals(1, list.size());
		for(Notification notif : list) {
			assertEquals(NotificationType.POINTS, notif.getType());
		}
	}
	
	@Test
	public void testGetUnreadNotifications() {
		List<Notification> list = notificationService.getUnreadNotifications(user);
		assertEquals(1, list.size());
		for(Notification notif : list) {
			assertEquals(NotificationType.PROMOTION, notif.getType());
		}
		list = notificationService.getUnreadNotifications(user2);
		assertEquals(0, list.size());
	}
	
	@Test
	public void testSaveNotification() {
		Notification notification = new Notification();
		notification.setType(NotificationType.PURCHASETXN);
		notification.setItemId("1test");
		notification = notificationRepo.save(notification);
		
		List<UserModel> users = new ArrayList<UserModel>();
		users.add(user);
		users.add(user2);
		
		notificationService.saveNotification(notification, users);
		List<Notification> list = notificationService.getUnreadNotifications(user);
		assertEquals(2, list.size());
		list = notificationService.getUnreadNotifications(user2);
		assertEquals(1, list.size());
	}
	
	@Test
	public void testSetNotificationAsRead() {
		notificationService.setNotificationAsRead(notif, user);
		List<Notification> list = notificationService.getUnreadNotifications(user);
		assertEquals(0, list.size());
	}
	
	@Test
	public void testDeleteNotification() {
		notificationService.deleteNotification(itemId);
		List<Notification> list = notificationService.getNotificationsByUser(user);
		assertEquals(1, list.size());
	}
}
