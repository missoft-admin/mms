package com.transretail.crm.giftcard.repo;

import com.transretail.crm.giftcard.repo.custom.GiftCardServiceRequestRepoCustom;
import com.transretail.crm.common.repo.CrmQueryDslPredicateExecutor;
import com.transretail.crm.giftcard.entity.GiftCardServiceRequest;
import org.springframework.stereotype.Repository;

/**
 *
 * @author Monte Cillo Co (mco@exist.com)
 */
@Repository
public interface GiftCardServiceRequestRepo
	extends CrmQueryDslPredicateExecutor<GiftCardServiceRequest, Long>,
	GiftCardServiceRequestRepoCustom {
}