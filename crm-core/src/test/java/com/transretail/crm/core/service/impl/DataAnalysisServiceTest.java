package com.transretail.crm.core.service.impl;

import java.io.InputStream;
import java.math.BigDecimal;
import java.util.List;
import java.util.Scanner;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import junit.framework.Assert;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Configurable;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.support.TransactionCallback;
import org.springframework.transaction.support.TransactionTemplate;

import com.transretail.crm.core.dto.DataAnalysisLMHDto;
import com.transretail.crm.core.dto.DataAnalysisOverviewDto;
import com.transretail.crm.core.dto.DataAnalysisSearchDto;
import com.transretail.crm.core.entity.enums.DataAnalysis;
import com.transretail.crm.core.entity.enums.DataAnalysisLMHAnalysisBy;
import com.transretail.crm.core.entity.enums.DataAnalysisLMHBy;
import com.transretail.crm.core.entity.enums.DataAnalysisOverview;
import com.transretail.crm.core.repo.LookupDetailRepo;
import com.transretail.crm.core.repo.LookupHeaderRepo;
import com.transretail.crm.core.repo.MemberRepo;
import com.transretail.crm.core.repo.PointsTxnModelRepo;
import com.transretail.crm.core.service.DataAnalysisService;



@Configurable
@Transactional
@ContextConfiguration(locations = "classpath*:/META-INF/spring/applicationContext*.xml")
@RunWith(SpringJUnit4ClassRunner.class)
public class DataAnalysisServiceTest {
	@PersistenceContext
    private EntityManager em;
    @Autowired
    private PlatformTransactionManager transactionManager;
    @Autowired
    private DataAnalysisService dataAnalysisService;
    @Autowired
    private PointsTxnModelRepo pointsRepo;
    @Autowired
    private LookupHeaderRepo headerRepo;
    @Autowired
    private LookupDetailRepo detailRepo;
    @Autowired
    private MemberRepo memberRepo;
	
	@Before
    public void setup() throws Exception {
        InputStream is = Thread.currentThread().getContextClassLoader().getResourceAsStream("scripts/dataanalysis_test.sql");
        try {
            final Scanner in = new Scanner(is);
            TransactionTemplate transactionTemplate = new TransactionTemplate(transactionManager);
            transactionTemplate.execute(new TransactionCallback() {
                public Object doInTransaction(TransactionStatus status) {
                    while (in.hasNext()) {
                        String script = in.nextLine();
                        if (StringUtils.isNotBlank(script)) {
                            em.createNativeQuery(script).executeUpdate();
                        }
                    }
                    return null;
                }
            });
        } finally {
            IOUtils.closeQuietly(is);
        }
    }
	
	@After
	public void tearDown() {
		pointsRepo.deleteAll();
		memberRepo.deleteAll();
		detailRepo.deleteAll();
		headerRepo.deleteAll();
	}
	
	@Test
	public void testGetOverviewAnalysis() {
		DataAnalysisSearchDto searchDto = new DataAnalysisSearchDto();
		
		searchDto.setType(DataAnalysis.OVERVIEW.getCode());

		searchDto.setOverview(DataAnalysisOverview.SALES_VALUE.getCode());
		List<DataAnalysisOverviewDto> returned = dataAnalysisService.getOverviewAnalysis(searchDto);
		Assert.assertEquals(returned.size(), 12);
		Assert.assertEquals(returned.get(0).getMonth(), "January");
		Assert.assertTrue(returned.get(0).getCurrent() == 121.765);
		Assert.assertTrue(returned.get(0).getPrevious()== 166.086);
		
		Assert.assertEquals(returned.get(1).getMonth(), "February");
		Assert.assertTrue(returned.get(1).getCurrent() == 137.086);
		Assert.assertTrue(returned.get(1).getPrevious()== 159.853);
		
		searchDto.setOverview(DataAnalysisOverview.TXN_COUNT.getCode());
		returned = dataAnalysisService.getOverviewAnalysis(searchDto);
		Assert.assertEquals(returned.size(), 12);
		Assert.assertEquals(returned.get(0).getMonth(), "January");
		Assert.assertTrue(returned.get(0).getCurrent() == 31);
		Assert.assertTrue(returned.get(0).getPrevious()== 31);
		
		Assert.assertEquals(returned.get(1).getMonth(), "February");
		Assert.assertTrue(returned.get(1).getCurrent() == 28);
		Assert.assertTrue(returned.get(1).getPrevious()== 28);
		
		searchDto.setOverview(DataAnalysisOverview.SHOPPER_COUNT.getCode());
		returned = dataAnalysisService.getOverviewAnalysis(searchDto);
		Assert.assertEquals(returned.size(), 12);
		Assert.assertEquals(returned.get(0).getMonth(), "January");
		Assert.assertTrue(returned.get(0).getCurrent() == 5);
		Assert.assertTrue(returned.get(0).getPrevious()== 5);
		
		Assert.assertEquals(returned.get(1).getMonth(), "February");
		Assert.assertTrue(returned.get(1).getCurrent() == 5);
		Assert.assertTrue(returned.get(1).getPrevious()== 5);
		
		searchDto.setOverview(DataAnalysisOverview.AVG_VISIT_FREQ.getCode());
		returned = dataAnalysisService.getOverviewAnalysis(searchDto);
		Assert.assertEquals(returned.size(), 12);
		Assert.assertEquals(returned.get(0).getMonth(), "January");
		Assert.assertTrue(returned.get(0).getCurrent() == 6.2);
		Assert.assertTrue(returned.get(0).getPrevious()== 6.2);
		
		Assert.assertEquals(returned.get(1).getMonth(), "February");
		Assert.assertTrue(returned.get(1).getCurrent() == 5.6);
		Assert.assertTrue(returned.get(1).getPrevious()== 5.6);
	}
	
	@Test
	public void testGetLMHAnalysis() {
		DataAnalysisSearchDto searchDto = new DataAnalysisSearchDto();
		
		searchDto.setType(DataAnalysis.LMH.getCode());
		searchDto.setMonthFrom(1);
		searchDto.setMonthTo(2);
		searchDto.setYearFrom(2014);
		searchDto.setYearTo(2014);
		
		searchDto.setLmhBy(DataAnalysisLMHBy.TXN_AMOUNT.getCode());

		searchDto.setAnalysisBy(DataAnalysisLMHAnalysisBy.SALES_VALUE.getCode());
		List<DataAnalysisLMHDto> returned = dataAnalysisService.getLMHAnalysis(searchDto);
		Assert.assertEquals(returned.size(), 2);
		Assert.assertEquals(returned.get(0).getMonthYear(), "Jan 2014");
		Assert.assertTrue(returned.get(0).getLight().doubleValue() == 11.431);
		Assert.assertTrue(returned.get(0).getMedium().doubleValue() == 67.177);
		Assert.assertTrue(returned.get(0).getHeavy().doubleValue() == 43.157);
		
		Assert.assertEquals(returned.get(1).getMonthYear(), "Feb 2014");
		Assert.assertTrue(returned.get(1).getLight().doubleValue() == 12.193);
		Assert.assertTrue(returned.get(1).getMedium().doubleValue() == 45.099);
		Assert.assertTrue(returned.get(1).getHeavy().doubleValue() == 79.794);
		
		searchDto.setAnalysisBy(DataAnalysisLMHAnalysisBy.SHOPPER_COUNT.getCode());
		returned = dataAnalysisService.getLMHAnalysis(searchDto);
		Assert.assertEquals(returned.size(), 2);
		Assert.assertEquals(returned.get(0).getMonthYear(), "Jan 2014");
		Assert.assertTrue(returned.get(0).getLight().compareTo(new BigDecimal(1)) == 0);
		Assert.assertTrue(returned.get(0).getMedium().compareTo(new BigDecimal(3)) == 0);
		Assert.assertTrue(returned.get(0).getHeavy().compareTo(new BigDecimal(1)) == 0);
		
		Assert.assertEquals(returned.get(1).getMonthYear(), "Feb 2014");
		Assert.assertTrue(returned.get(1).getLight().compareTo(new BigDecimal(1)) == 0);
		Assert.assertTrue(returned.get(1).getMedium().compareTo(new BigDecimal(2)) == 0);
		Assert.assertTrue(returned.get(1).getHeavy().compareTo(new BigDecimal(2)) == 0);
		
		searchDto.setAnalysisBy(DataAnalysisLMHAnalysisBy.AVG_VISIT_FREQ.getCode());
		returned = dataAnalysisService.getLMHAnalysis(searchDto);
		Assert.assertEquals(returned.size(), 2);
		Assert.assertEquals(returned.get(0).getMonthYear(), "Jan 2014");
		Assert.assertTrue(returned.get(0).getLight().compareTo(new BigDecimal(2)) == 0);
		Assert.assertTrue(returned.get(0).getMedium().setScale(2, BigDecimal.ROUND_HALF_DOWN).doubleValue() == 6.67);
		Assert.assertTrue(returned.get(0).getHeavy().compareTo(new BigDecimal(9)) == 0);
		
		Assert.assertEquals(returned.get(1).getMonthYear(), "Feb 2014");
		Assert.assertTrue(returned.get(1).getLight().compareTo(new BigDecimal(3)) == 0);
		Assert.assertTrue(returned.get(1).getMedium().compareTo(new BigDecimal(5.5)) == 0);
		Assert.assertTrue(returned.get(1).getHeavy().compareTo(new BigDecimal(7)) == 0);
		
		
		searchDto.setLmhBy(DataAnalysisLMHBy.VISIT_FREQ.getCode());

		searchDto.setAnalysisBy(DataAnalysisLMHAnalysisBy.SALES_VALUE.getCode());
		returned = dataAnalysisService.getLMHAnalysis(searchDto);
		Assert.assertEquals(returned.size(), 2);
		Assert.assertEquals(returned.get(0).getMonthYear(), "Jan 2014");
		Assert.assertTrue(returned.get(0).getLight().doubleValue() == 11.431);
		Assert.assertTrue(returned.get(0).getMedium().doubleValue() == 67.177);
		Assert.assertTrue(returned.get(0).getHeavy().doubleValue() == 43.157);
		
		Assert.assertEquals(returned.get(1).getMonthYear(), "Feb 2014");
		Assert.assertTrue(returned.get(1).getLight().doubleValue() == 35.922);
		Assert.assertTrue(returned.get(1).getMedium().doubleValue() == 34.900);
		Assert.assertTrue(returned.get(1).getHeavy().doubleValue() == 66.264);
		
		searchDto.setAnalysisBy(DataAnalysisLMHAnalysisBy.SHOPPER_COUNT.getCode());
		returned = dataAnalysisService.getLMHAnalysis(searchDto);
		Assert.assertEquals(returned.size(), 2);
		Assert.assertEquals(returned.get(0).getMonthYear(), "Jan 2014");
		Assert.assertTrue(returned.get(0).getLight().compareTo(new BigDecimal(1)) == 0);
		Assert.assertTrue(returned.get(0).getMedium().compareTo(new BigDecimal(3)) == 0);
		Assert.assertTrue(returned.get(0).getHeavy().compareTo(new BigDecimal(1)) == 0);
		
		Assert.assertEquals(returned.get(1).getMonthYear(), "Feb 2014");
		Assert.assertTrue(returned.get(1).getLight().compareTo(new BigDecimal(2)) == 0);
		Assert.assertTrue(returned.get(1).getMedium().compareTo(new BigDecimal(1)) == 0);
		Assert.assertTrue(returned.get(1).getHeavy().compareTo(new BigDecimal(2)) == 0);
		
		searchDto.setAnalysisBy(DataAnalysisLMHAnalysisBy.AVG_VISIT_FREQ.getCode());
		returned = dataAnalysisService.getLMHAnalysis(searchDto);
		Assert.assertEquals(returned.size(), 2);
		Assert.assertEquals(returned.get(0).getMonthYear(), "Jan 2014");
		Assert.assertTrue(returned.get(0).getLight().compareTo(new BigDecimal(2)) == 0);
		Assert.assertTrue(returned.get(0).getMedium().setScale(2, BigDecimal.ROUND_HALF_DOWN).doubleValue() == 6.67);
		Assert.assertTrue(returned.get(0).getHeavy().compareTo(new BigDecimal(9)) == 0);
		
		Assert.assertEquals(returned.get(1).getMonthYear(), "Feb 2014");
		Assert.assertTrue(returned.get(1).getLight().compareTo(new BigDecimal(3.5)) == 0);
		Assert.assertTrue(returned.get(1).getMedium().compareTo(new BigDecimal(6)) == 0);
		Assert.assertTrue(returned.get(1).getHeavy().compareTo(new BigDecimal(7.5)) == 0);
	}
}
