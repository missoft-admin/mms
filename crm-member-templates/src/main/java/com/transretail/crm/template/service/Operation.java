package com.transretail.crm.template.service;

/**
 * @author Allan G. Ramirez (agramirez@exist.com)
 */
public enum Operation {
    NEW_MEMBERS, UPDATE_EXISTING_MEMBERS
}
