package com.transretail.crm.loyalty.repo.custom;

import java.util.List;

import org.joda.time.LocalDate;

import com.transretail.crm.common.service.dto.response.ResultList;
import com.transretail.crm.loyalty.dto.LoyaltyCardTrackerDto;
import com.transretail.crm.loyalty.dto.LoyaltyCardTrackerFilterDto;
import com.transretail.crm.loyalty.dto.LoyaltyCardTrackingDto;
import com.transretail.crm.loyalty.dto.MultipleAllocationDto;




public interface LoyaltyCardInventoryHistoryRepoCustom {

	ResultList<LoyaltyCardTrackerDto> trackInventories(LoyaltyCardTrackerFilterDto filterDto);

	ResultList<LoyaltyCardTrackingDto> trackInventories2(
			LoyaltyCardTrackerFilterDto filterDto);

	List<MultipleAllocationDto> getTransactions(String transactionType,
			String username, LocalDate dateFrom, LocalDate dateTo);
	
}
