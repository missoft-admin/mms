package com.transretail.crm.giftcard.dto;

import java.math.BigDecimal;
import java.util.List;

import com.google.common.collect.Lists;
import com.mysema.query.types.expr.BooleanExpression;
import com.transretail.crm.common.service.dto.request.AbstractSearchFormDto;
import com.transretail.crm.giftcard.entity.QGiftCardGeneralDiscount;

public class GiftCardGeneralDiscountSearchDto extends AbstractSearchFormDto {
	private BigDecimal minPurchase;
	private BigDecimal maxPurchase;
	private BigDecimal minDiscount;
	private BigDecimal maxDiscount;
	
	@Override
	public BooleanExpression createSearchExpression() {
		List<BooleanExpression> exprs = Lists.newArrayList();
		QGiftCardGeneralDiscount gcGenDiscount = QGiftCardGeneralDiscount.giftCardGeneralDiscount;
		
		if(minPurchase != null) {
			exprs.add(gcGenDiscount.minPurchase.goe(minPurchase));
		}
		
		if(maxPurchase != null) {
			exprs.add(gcGenDiscount.maxDiscount.loe(maxPurchase));
		}
		
		if(minDiscount != null) {
			exprs.add(gcGenDiscount.discount.goe(minDiscount));
		}
		
		if(maxDiscount != null) {
			exprs.add(gcGenDiscount.discount.loe(maxDiscount));
		}
		
		return BooleanExpression.allOf(exprs.toArray(new BooleanExpression[exprs.size()]));
	}

	public BigDecimal getMinPurchase() {
		return minPurchase;
	}

	public void setMinPurchase(BigDecimal minPurchase) {
		this.minPurchase = minPurchase;
	}

	public BigDecimal getMaxPurchase() {
		return maxPurchase;
	}

	public void setMaxPurchase(BigDecimal maxPurchase) {
		this.maxPurchase = maxPurchase;
	}

	public BigDecimal getMinDiscount() {
		return minDiscount;
	}

	public void setMinDiscount(BigDecimal minDiscount) {
		this.minDiscount = minDiscount;
	}

	public BigDecimal getMaxDiscount() {
		return maxDiscount;
	}

	public void setMaxDiscount(BigDecimal maxDiscount) {
		this.maxDiscount = maxDiscount;
	}
}
