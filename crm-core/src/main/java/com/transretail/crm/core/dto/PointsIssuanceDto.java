package com.transretail.crm.core.dto;

import com.mysema.query.annotations.QueryProjection;

/**
 *
 * @author Monte Cillo Co <mco@exist.com>
 */
public class PointsIssuanceDto {

    private String rewardType;
    private double totalPointsPerDay;
    private int day;

    @QueryProjection
    public PointsIssuanceDto(String rewardType, double totalPointsPerDay, int day) {
	this.rewardType = rewardType;
	this.totalPointsPerDay = totalPointsPerDay;
	this.day = day;
    }

    public int getDay() {
	return day;
    }

    public void setDay(int day) {
	this.day = day;
    }

    public String getRewardType() {
	return rewardType;
    }

    public void setRewardType(String rewardType) {
	this.rewardType = rewardType;
    }

    public double getTotalPointsPerDay() {
	return totalPointsPerDay;
    }

    public void setTotalPointsPerDay(double totalPointsPerDay) {
	this.totalPointsPerDay = totalPointsPerDay;
    }

}
