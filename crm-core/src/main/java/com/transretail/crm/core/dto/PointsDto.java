package com.transretail.crm.core.dto;


import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.transretail.crm.common.web.converter.CurrencySerializer;
import com.transretail.crm.common.web.converter.FormattedIntegerNumberSerializer;
import com.transretail.crm.core.entity.PointsTxnModel;
import com.transretail.crm.core.entity.enums.PointTxnType;
import com.transretail.crm.core.entity.enums.TxnStatus;
import org.apache.commons.lang3.StringUtils;
import org.joda.time.DateTime;
import org.joda.time.LocalDate;

import java.util.Date;


public class PointsDto {

//    private static final String[] IGNORE = {"transactionType", "status", "memberModel", "created", "transactionOwner"};

    private String id;
    @JsonSerialize(using = FormattedIntegerNumberSerializer.class)
    private Double transactionPoints;
    private String storeCode;
    private String storeName;
    private String transactionType;
    private String status;
    private String transactionNo;
    private StoreDto storeDto;
    private Date transactionDateTime;
    @JsonSerialize(using = FormattedIntegerNumberSerializer.class)
    private Double transactionAmount;
    private String transactionMedia;
    private String transactionMediaDesc;
    private MemberDto memberModel;
    private MemberDto transactionContributer;

    private String createUser;
    private String comment;
    private Date created;

    private String validatedBy;
    private String approvalReason;
    private Date validatedDate;

    private Long longTxnDate;
    private Long longCreated;
    
    private LocalDate expiryDate;
    private Double expireExcess;
    private DateTime lastRedeemTimestamp;
    private String memberAccountId;

    private String txnEftMediaDesc;
    private String cardNo;

    public PointsDto() {}
    public PointsDto( PointsTxnModel pointsTxn ) {
    	id = pointsTxn.getId();
        transactionPoints = pointsTxn.getTransactionPoints();
        storeCode = pointsTxn.getStoreCode();

        transactionDateTime = pointsTxn.getTransactionDateTime();
        transactionAmount = pointsTxn.getTransactionAmount();
        memberModel = new MemberDto( pointsTxn.getMemberModel() );
        if(pointsTxn.getTransactionContributer() != null) {
        	transactionContributer = new MemberDto(pointsTxn.getTransactionContributer());
        }

        transactionType = pointsTxn.getTransactionType() != null ? 
        		pointsTxn.getTransactionType().toString() : null;
        status = pointsTxn.getStatus() != null ? 
        		pointsTxn.getStatus().toString() : null;
                transactionNo = pointsTxn.getTransactionNo();

        validatedBy = pointsTxn.getValidatedBy();
        approvalReason = pointsTxn.getApprovalReason();
        validatedDate = pointsTxn.getValidatedDate();

        createUser = pointsTxn.getCreateUser();
        comment = pointsTxn.getComment();
        created = null != pointsTxn.getCreated() ? 
        		new Date( pointsTxn.getCreated().getMillis() ) : null ;
        transactionMedia = pointsTxn.getTransactionMedia();

        if ( null != transactionDateTime ) {
            longTxnDate = transactionDateTime.getTime();
        }
        if ( null != created ) {
            longCreated = created.getTime();
        }
        
        expiryDate = pointsTxn.getExpiryDate();
        expireExcess = pointsTxn.getExpireExcess();
        lastRedeemTimestamp = pointsTxn.getLastRedeemTimestamp();
        //memberAccountId = pointsTxn.getMemberAccountId();


    }

    public PointsTxnModel toModel() {
    	return toModel( new PointsTxnModel() );
    }

    public PointsTxnModel toModel( PointsTxnModel inModel ) {
    	if ( null == inModel ) {
    		inModel = new PointsTxnModel();
    	}
//    	BeanUtils.copyProperties( this, inModel, IGNORE );
        inModel.setTransactionPoints(this.transactionPoints);
        inModel.setStoreCode(this.storeCode);
        if ( StringUtils.isNotBlank( this.transactionType ) ) {
            inModel.setTransactionType( PointTxnType.valueOf( this.transactionType ) );
        }
        if ( StringUtils.isNotBlank( status ) ) {
            inModel.setStatus( TxnStatus.valueOf( status ) );
        }
        inModel.setTransactionNo(this.transactionNo);
        inModel.setTransactionDateTime(this.getTransactionDateTime());
        inModel.setTransactionAmount(this.getTransactionAmount());
        inModel.setTransactionMedia(this.getTransactionMedia());
        inModel.setComment(this.comment);
        inModel.setValidatedBy(this.validatedBy);
        inModel.setApprovalReason(this.approvalReason);
        inModel.setValidatedDate(this.validatedDate);
        inModel.setExpireExcess(expireExcess);
        inModel.setExpiryDate(expiryDate);
        inModel.setLastRedeemTimestamp(lastRedeemTimestamp);
        //inModel.setMemberAccountId(memberAccountId);

    	if ( null != created ) {
    		inModel.setCreated( new DateTime( created.getTime() ) );
    	}
    	return inModel;
    }





    public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public Double getTransactionPoints() {
		return transactionPoints;
	}

	public void setTransactionPoints(Double transactionPoints) {
		this.transactionPoints = transactionPoints;
	}

	public String getStoreCode() {
		return storeCode;
	}

	public void setStoreCode(String storeCode) {
		this.storeCode = storeCode;
	}

	public String getTransactionType() {
		return transactionType;
	}

	public void setTransactionType(String transactionType) {
		this.transactionType = transactionType;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getApprovalReason() {
		return approvalReason;
	}

	public void setApprovalReason(String approvalReason) {
		this.approvalReason = approvalReason;
	}

	public String getTransactionNo() {
		return transactionNo;
	}

	public void setTransactionNo(String transactionNo) {
		this.transactionNo = transactionNo;
	}

	public String getComment() {
		return comment;
	}

	public void setComment(String comment) {
		this.comment = comment;
	}

	public StoreDto getStoreDto() {
		return storeDto;
	}
	
	public void setStoreDto(StoreDto storeDto) {
		this.storeDto = storeDto;
	}

	public Date getTransactionDateTime() {
		return transactionDateTime;
	}

	public void setTransactionDateTime(Date transactionDateTime) {
		this.transactionDateTime = transactionDateTime;
	}

	public Double getTransactionAmount() {
		return transactionAmount;
	}

	public void setTransactionAmount(Double transactionAmount) {
		this.transactionAmount = transactionAmount;
	}

	@JsonInclude(Include.ALWAYS)
	@JsonSerialize(using=CurrencySerializer.class)
	public Double getTxnAmount() {
		return transactionAmount;
	}
	public MemberDto getMemberModel() {
		return memberModel;
	}

	public void setMemberModel(MemberDto memberModel) {
		this.memberModel = memberModel;
		this.memberAccountId = memberModel.getAccountId();
	}

	public Date getCreated() {
		return created;
	}

	public void setCreated(Date created) {
		this.created = created;
	}

	public String getStoreName() {
		return storeName;
	}

	public void setStoreName(String storeName) {
		this.storeName = storeName;
	}

	public String getTransactionMedia() {
		return transactionMedia;
	}

	public void setTransactionMedia(String transactionMedia) {
		this.transactionMedia = transactionMedia;
	}

	public String getTransactionMediaDesc() {
		return transactionMediaDesc;
	}

	public void setTransactionMediaDesc(String transactionMediaDesc) {
		this.transactionMediaDesc = transactionMediaDesc;
	}

	public String getCreateUser() {
		return createUser;
	}

	public void setCreateUser(String createUser) {
		this.createUser = createUser;
	}

	public String getValidatedBy() {
		return validatedBy;
	}

	public void setValidatedBy(String validatedBy) {
		this.validatedBy = validatedBy;
	}

	public Date getValidatedDate() {
		return validatedDate;
	}

	public void setValidatedDate(Date validatedDate) {
		this.validatedDate = validatedDate;
	}

    public Long getLongTxnDate() {
		return longTxnDate;
	}

    public Long getLongCreated() {
		return longCreated;
	}
	public MemberDto getTransactionContributer() {
		return transactionContributer;
	}
	public void setTransactionContributer(MemberDto transactionContributer) {
		this.transactionContributer = transactionContributer;
	}
	public LocalDate getExpiryDate() {
		return expiryDate;
	}
	public void setExpiryDate(LocalDate expiryDate) {
		this.expiryDate = expiryDate;
	}
	public Double getExpireExcess() {
		return expireExcess;
	}
	public void setExpireExcess(Double expireExcess) {
		this.expireExcess = expireExcess;
	}
	public DateTime getLastRedeemTimestamp() {
		return lastRedeemTimestamp;
	}
	public void setLastRedeemTimestamp(DateTime lastRedeemTimestamp) {
		this.lastRedeemTimestamp = lastRedeemTimestamp;
	}
	public String getMemberAccountId() {
		return memberAccountId;
	}
	public void setMemberAccountId(String memberAccountId) {
		this.memberAccountId = memberAccountId;
	}
	public String getTxnEftMediaDesc() {
		return txnEftMediaDesc;
	}
	public void setTxnEftMediaDesc(String txnEftMediaDesc) {
		this.txnEftMediaDesc = txnEftMediaDesc;
	}

    public String getCardNo() {
        return cardNo;
    }

    public void setCardNo(String cardNo) {
        this.cardNo = cardNo;
    }
}
