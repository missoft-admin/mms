alter table CRM_GC_TRANSACTION 
  add SALES_ORDER_ID number(19,0);

 alter table CRM_GC_TRANSACTION 
        add constraint FK_nv7kgolgx7y90lwj6wmaw2evx 
        foreign key (SALES_ORDER_ID) 
        references CRM_SALES_ORD;