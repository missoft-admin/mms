package com.transretail.crm.core.repo.custom;


import java.util.List;

import com.transretail.crm.core.dto.PromotionSearchDto;
import com.transretail.crm.core.entity.Promotion;


public interface PromotionRepoCustom {

	List<Promotion> find( PromotionSearchDto theDto );

}
