
package com.transretail.crm.giftcard.repo.custom;

import com.transretail.crm.giftcard.entity.GiftCardTransactionItem;
import org.joda.time.LocalDateTime;

import java.util.List;

/**
 * description here.
 *
 * @author Vincent Gerard Tan
 */
public interface GiftCardTransactionItemRepoCustom {
    List<GiftCardTransactionItem> findPrepaidTransactions(String cardNo, LocalDateTime dateFrom, LocalDateTime dateTo);
}
