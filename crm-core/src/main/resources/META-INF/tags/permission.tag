<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ tag import="com.transretail.crm.core.security.model.impl.CustomSecurityUserDetailsImpl" %>
<%@ tag import="com.transretail.crm.core.security.util.UserUtil" %>
<%@ attribute name="hasPermissions" description="Comma separated permission codes" %>
<%@ attribute name="var" description="Variable to which the result will be stored." %>
<%
  CustomSecurityUserDetailsImpl currentUser = UserUtil.getCurrentUser();
  boolean isAllowed = false;
  if(currentUser != null) {
    String codes = (String) jspContext.getAttribute("hasPermissions");
    for (String code : codes.split(",")) {
      if (currentUser.getPermissions().contains(code.trim())) {
        isAllowed = true;
        break;
      }
    }
  }
  jspContext.setAttribute("isAllowed", isAllowed);
  String variable = (String) jspContext.getAttribute("var");
  if (variable != null && !"".equals(variable.trim())) {
    jspContext.setAttribute(variable, isAllowed);
  }
%>
<c:if test="${isAllowed}">
  <jsp:doBody/>
</c:if>