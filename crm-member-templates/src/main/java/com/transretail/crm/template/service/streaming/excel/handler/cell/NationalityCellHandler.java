package com.transretail.crm.template.service.streaming.excel.handler.cell;

import org.apache.commons.lang3.StringUtils;

import com.transretail.crm.core.entity.MemberModel;
import com.transretail.crm.core.entity.embeddable.CustomerProfile;
import com.transretail.crm.core.entity.lookup.LookupDetail;
import com.transretail.crm.template.service.MemberTemplateParseException;
import com.transretail.crm.template.service.streaming.excel.handler.sheet.AbstractSheetHandler;

/**
 * @author Allan G. Ramirez (agramirez@exist.com)
 */
public class NationalityCellHandler implements CellHandler {
    private AbstractSheetHandler sheetHandler;

    public NationalityCellHandler(AbstractSheetHandler sheetHandler) {
        this.sheetHandler = sheetHandler;
    }

    @Override
    public void handle(MemberModel dto, String cellText) throws MemberTemplateParseException {
        if (dto.getCustomerProfile() == null) {
            dto.setCustomerProfile(new CustomerProfile());
        }
        LookupDetail detail = null;
        if (StringUtils.isNotBlank(cellText)) {
            detail = sheetHandler.getLoookupDetail(sheetHandler.getCodePropertiesService().getHeaderNationality(), cellText);
        }
        dto.getCustomerProfile().setNationality(detail);
    }
}
