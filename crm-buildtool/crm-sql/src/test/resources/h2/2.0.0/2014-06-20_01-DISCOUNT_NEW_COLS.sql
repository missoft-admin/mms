ALTER TABLE CRM_GC_DISCOUNT_SCHEME
ADD (
INCLUDE_BELOW_MIN_PURCHASE char(255),
DISCOUNT_TYPE varchar(255),
START_DATE date,
END_DATE date
);

ALTER TABLE CRM_GC_DISCOUNT_SCHEME DROP COLUMN START_YEAR;
ALTER TABLE CRM_GC_DISCOUNT_SCHEME DROP COLUMN END_YEAR;
