package com.transretail.crm.common.audit.annotation;

import static java.lang.annotation.ElementType.TYPE;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

import java.lang.annotation.Retention;
import java.lang.annotation.Target;

/**
 * @author Allan G. Ramirez (agramirez@exist.com)
 */
@Target(TYPE)
@Retention(RUNTIME)
public @interface AuditModule {
    String moduleName() default "";
}
