package com.transretail.crm.core.validator;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import javax.validation.Constraint;
import javax.validation.ReportAsSingleViolation;
import javax.validation.constraints.Pattern;

import org.hibernate.validator.constraints.NotEmpty;

import com.transretail.crm.core.util.AppConstants;

/**
 * @author Allan G. Ramirez (agramirez@exist.com)
 */
@Documented
@Constraint(validatedBy = {})
@Target({ElementType.FIELD})
@Retention(RetentionPolicy.RUNTIME)
@ReportAsSingleViolation
@NotEmpty
@Pattern(regexp = AppConstants.PASSWORD_PATTERN)
public @interface Password {
    public abstract String message() default "{constraints.Password.message}";

    public abstract Class<?>[] groups() default {};

    public abstract Class<?>[] payload() default {};
}
