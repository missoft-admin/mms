
alter table CRM_GC_SERVICE_REQUEST 
     add column FILED_IN integer null;

update crm_gc_service_request set FILED_IN = 1;

alter table CRM_GC_SERVICE_REQUEST 
     modify column FILED_IN integer not null;

alter table CRM_GC_SERVICE_REQUEST 
      add constraint FK_65wn0arfskrhnjr2xqga4g08j 
      foreign key (FILED_IN) 
      references STORE;

-- drop --
-- alter table CRM_GC_SERVICE_REQUEST 
--       drop constraint FK_65wn0arfskrhnjr2xqga4g08j;
-- alter table crm_gc_service_request drop column FILED_IN;