
package com.transretail.crm.core.service.impl;

import com.transretail.crm.core.entity.QInvalidTerminalModel;
import com.transretail.crm.core.repo.InvalidTerminalRepo;
import com.transretail.crm.core.service.InvalidTerminalService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class InvalidTerminalServiceImpl implements InvalidTerminalService {

    @Autowired
    private InvalidTerminalRepo repo;
	    
    @Override
    public boolean inList( String storeCode, String terminalId) {
	QInvalidTerminalModel qt =QInvalidTerminalModel.invalidTerminalModel;
	return repo.findOne(qt.storeCode.eq(storeCode).and(qt.terminalId.eq(terminalId))) != null;
    }
    
}
