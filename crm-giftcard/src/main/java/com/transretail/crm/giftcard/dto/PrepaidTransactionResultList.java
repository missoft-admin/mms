package com.transretail.crm.giftcard.dto;

import com.transretail.crm.common.service.dto.response.AbstractResultListDTO;

import java.util.Collection;

/**
 * description here.
 *
 * @author Vincent Gerard Tan
 */
public class PrepaidTransactionResultList extends AbstractResultListDTO<PrepaidTransactionHistoryDto> {
    public PrepaidTransactionResultList(Collection<PrepaidTransactionHistoryDto> results) {
        super(results);
    }
}
