/*
 * Copyright (c) 2012.
 * All rights reserved.
 */

package com.transretail.crm.core.security.util;

public interface SecurityContextAuthenticationPrincipalInterface {

    String getUsername();

    String getFullName();

    String getEmail();

}
