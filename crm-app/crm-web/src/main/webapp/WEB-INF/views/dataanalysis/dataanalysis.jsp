<%@include file="../common/taglibs.jsp" %>

<div class="page-header page-header2">
    <h1><spring:message code="label_data_analysis" /></h1>
</div>

<spring:url value="/dataanalysis/show" var="action" />
<form:form modelAttribute="searchDto" action="${action}" method="POST" id="searchDto" cssClass="form-reset form-horizontal row-fluid">




<div class="form-horizontal pull-left" id="printContainer">
  <select name="reportType" class="selectpicker" id="reportType" data-header="Select a Format">
    <c:forEach items="${reportTypes}" var="reportType">
      <option data-url="<c:url value="/dataanalysis/print/${reportType.code}" />" value="${reportType.code}"><spring:message code="report_type_${reportType.code}" /></option>
    </c:forEach>
  </select>
  <button type="button" class="btn btn-print btn-print-small tiptip" id="printButton" value="<spring:message code="label_print" />" data-toggle="dropdown"><spring:message code="label_print" /></button>
</div>

<div class="clearfix"></div>

<div id="searchForm" class="well clearfix">


  <div class="errorMessages error"></div>
<div class="control-group">

  <label for="" class="control-label"></label> 
  <div class="controls">
    <form:select path="type" id="typeDropdown">
      <c:forEach items="${types}" var="item">
      <form:option value="${item.code}"><spring:message code="label_analysis_${item.code}" /></form:option>
      </c:forEach>
    </form:select>
    <input type="button" id="searchBtn" class="btn btn-primary" value="<spring:message code="label_submit" />" />
    <div class="checkbox" id="advancedSearchChkContainer"><input type="checkbox" name="advancedSearch" id="advancedSearchChk" /><spring:message code="label_analysis_advancedsearch" /></div>
  </div>
  

</div>

<div class="clearfix"></div>

<div class="overview analysis hide">
<div class="span4">
<div class="control-group">
  <label for="" class="control-label"><spring:message code="label_search_analysisby" /></label> 
  <div class="controls">
    <form:select path="overview" >
      <c:forEach items="${overviewTypes}" var="item">
      <form:option value="${item.code}"><spring:message code="label_overview_${item.code}" /></form:option>
      </c:forEach>
    </form:select>
  </div>
</div>
</div>
</div>

<div class="lmh analysis hide">
<div class="span4">
<div class="control-group">
  <label for="" class="control-label"><spring:message code="label_search_yearfrom" /></label> 
  <div class="controls">
    <form:select path="yearFrom" >
      <c:forEach items="${years}" var="item">
      <form:option value="${item}">${item}</form:option>
      </c:forEach>
    </form:select>
  </div>
</div>

<div class="control-group">
  <label for="" class="control-label"><spring:message code="label_search_monthfrom" /></label> 
  <div class="controls">
    <form:select path="monthFrom" >
      <c:forEach items="${months}" var="item" varStatus="status">
      <c:if test="${not empty item}"><form:option value="${status.count}">${item}</form:option></c:if>
      </c:forEach>
    </form:select>
  </div>
</div>
<div class="control-group">
  <label for="" class="control-label"><spring:message code="label_search_lmhby" /></label> 
  <div class="controls">
    <form:select path="lmhBy" >
      <c:forEach items="${lmhTypes}" var="item">
      <form:option value="${item.code}"><spring:message code="label_lmh_${item.code}" /></form:option>
      </c:forEach>
    </form:select>
  </div>
</div>
</div>

<div class="span4">
<div class="control-group">
  <label for="" class="control-label"><spring:message code="label_search_yearto" /></label> 
  <div class="controls">
    <form:select path="yearTo" >
      <c:forEach items="${years}" var="item">
      <form:option value="${item}">${item}</form:option>
      </c:forEach>
    </form:select>
  </div>
</div>

<div class="control-group">
  <label for="" class="control-label"><spring:message code="label_search_monthto" /></label> 
  <div class="controls">
    <form:select path="monthTo" >
      <c:forEach items="${months}" var="item" varStatus="status">
      <c:if test="${not empty item}"><form:option value="${status.count}">${item}</form:option></c:if>
      </c:forEach>
    </form:select>
  </div>
</div>
<div class="control-group">
  <label for="" class="control-label"><spring:message code="label_search_analysisby" /></label> 
  <div class="controls">
    <form:select path="analysisBy" >
      <c:forEach items="${lmhAnalysisTypes}" var="item">
      <form:option value="${item.code}"><spring:message code="label_lmh_analysis_${item.code}" /></form:option>
      </c:forEach>
    </form:select>
  </div>
</div>
</div>
</div>

<div class="decile analysis hide">
  <div class="span4">
    <div class="control-group">
      <label for="decileYearFrom" class="control-label"><spring:message code="label_search_yearfrom"/></label>

      <div class="controls">
        <form:select path="decileYearFrom" id="decileYearFrom">
          <c:forEach items="${years}" var="item">
            <form:option value="${item}">${item}</form:option>
          </c:forEach>
        </form:select>
      </div>
    </div>

    <div class="control-group">
      <label for="decileMonthFrom" class="control-label"><spring:message code="label_search_monthfrom"/></label>

      <div class="controls">
        <form:select id="decileMonthFrom" path="decileMonthFrom">
          <c:forEach items="${months}" var="item" varStatus="status">
            <c:if test="${not empty item}"><form:option value="${status.count}">${item}</form:option></c:if>
          </c:forEach>
        </form:select>
      </div>
    </div>
  </div>
  <div class="span4">
    <div class="control-group">
      <label for="decileYearTo" class="control-label"><spring:message code="label_search_yearto"/></label>

      <div class="controls">
        <form:select path="decileYearTo" id="decileYearTo" items="${years}">
          <c:forEach items="${years}" var="item">
            <form:option value="${item}">${item}</form:option>
          </c:forEach>
        </form:select>
      </div>
    </div>

    <div class="control-group">
      <label for="decileMonthTo" class="control-label"><spring:message code="label_search_monthto"/></label>

      <div class="controls">
        <form:select id="decileMonthTo" path="decileMonthTo">
          <c:forEach items="${months}" var="item" varStatus="status">
            <c:if test="${not empty item}"><form:option value="${status.count}">${item}</form:option></c:if>
          </c:forEach>
        </form:select>
      </div>
    </div>
  </div>
</div>

<div class="clearfix"></div>

<div class="advancedSearch hide">
<div class="">
<div class="control-group">
  <label for="" class="control-label"><spring:message code="label_analysis_advancedsearch" /></label> 
  <div class="controls">
    <div class="span2">
    <div class="radio"><input type="radio" name="storeType" checked value="stores" /><spring:message code="label_analysis_advancedsearch_store" /></div>
    <div class="radio"><input type="radio" name="storeType" value="storeGroups" /><spring:message code="label_analysis_advancedsearch_storegroup" /></div>
    </div>
    <div class="span10">
      <div id="storeSel" class="">
        <div class="pull-left">
        <select multiple="multiple" name="storesD" id="storesD">
          <c:forEach items="${stores}" var="item">
          <option value="${item.code}">${item.name}</option>
          </c:forEach>
        </select>
        </div>
        <div class="pull-left" style="margin-left: 5px; margin-right: 5px;">
          <div><input type="button" id="storeAddBtn" class="btn" value=">>"/></div>
          <div><input type="button" id="storeRemoveBtn" class="btn" value="<<"/></div>      
        </div>
        <div class="pull-left">
        <select multiple="multiple" class="searchDropdown" name="stores" id="stores">
        </select>
        </div>
      </div>
      <div id="storeGroupSel" class="hide">
        <div class="pull-left">
        <select multiple="multiple" name="storeGroupsD" id="storeGroupsD" data-url="<spring:url value="/groups/store/list" />"></select>
        </div>
        <div class="pull-left" style="margin-left: 5px; margin-right: 5px;">
          <div><input type="button" id="storeGrpAddBtn" class="btn" value=">>"/></div>
          <div><input type="button" id="storeGrpRemoveBtn" class="btn" value="<<"/></div>      
        </div>
        <div class="pull-left">
        <select multiple="multiple" class="searchDropdown" name="storeGroups" id="storeGroups">
        </select>
        </div>
      </div>
    </div>
  </div>
</div>

<div class="control-group">
  <label for="" class="control-label"></label> 
  <div class="controls">
    <div class="span2">
    <%-- <div class="radio"><input type="radio" name="productType" checked value="products" /><spring:message code="label_analysis_advancedsearch_product" /></div> --%>
    <div class="radio"><input type="radio" name="productType" checked value="productGroups" style="visibility: hidden;" /><spring:message code="label_analysis_advancedsearch_productgroup" /></div>
    </div>
    <div class="span10">
      <div id="productSel" class="hide">
        <div class="pull-left">
        <%-- <select multiple="multiple" name="productsD" id="productsD">
          <c:forEach items="${products}" var="item">
          <option value="${item.id}">${item.name}</option>
          </c:forEach>
        </select> --%>
        </div>
        <div class="pull-left" style="margin-left: 5px; margin-right: 5px;">
          <div><input type="button" id="productAddBtn" class="btn" value=">>"/></div>
          <div><input type="button" id="productRemoveBtn" class="btn" value="<<"/></div>      
        </div>
        <div class="pull-left">
        <select multiple="multiple" class="searchDropdown" name="products" id="products">
        </select>
        </div>
      </div>
      <div id="productGroupSel" class="hde">
        <div class="pull-left">
        <select multiple="multiple" name="productGroupsD" id="productGroupsD" data-url="<spring:url value="/groups/product/list" />"></select>
        </div>
        <div class="pull-left" style="margin-left: 5px; margin-right: 5px;">
          <div><input type="button" id="productGrpAddBtn" class="btn" value=">>"/></div>
          <div><input type="button" id="productGrpRemoveBtn" class="btn" value="<<"/></div>      
        </div>
        <div class="pull-left">
        <select multiple="multiple" class="searchDropdown" name="productGroups" id="productGroups">
        </select>
        </div>
      </div>
    </div>
  </div>
</div>


</div>

</div>

</div>

</form:form>

<div id="analysisContainer"></div>

<link href="<c:url value="/css/jquery.jqplot.css" />" rel="stylesheet" />
<link href="<c:url value="/styles/temp.css" />" rel="stylesheet" />
<script src="<spring:url value="/js/jqplot/jquery.jqplot.js" />" ></script>
<script src="<spring:url value="/js/jqplot/jqplot.categoryAxisRenderer.js" />" ></script>
<script src="<spring:url value="/js/jqplot/jqplot.barRenderer.js" />" ></script>
<script src="<spring:url value="/js/jqplot/jqplot.pieRenderer.min.js" />" ></script>
<script src="<spring:url value="/js/common/form2js.js" />"></script>
<script src="<spring:url value="/js/common/json2.js" />"></script>
<script src="<spring:url value="/js/jquery/jquery.toObject.js" />"></script>
<script src="<spring:url value="/js/jquery/jquery.numberInput.js" />"></script>

<script>
$(document).ready(function() {
  $(".yearInput").numberInput({
    "type" : "int",
    'maxChar' : 4
  });

  var $errorMessagesContainer = $('#searchDto div.errorMessages' );
  var showErrorMessage = function(message) {
    $errorMessagesContainer.html('<div class="alert alert-error">' +
            '<button type="button" class="close" data-dismiss="alert">&times;</button>' +
            message +
            '</div>');
  };

	$("#searchBtn").click(function(e) {
		e.preventDefault();
		$(".searchDropdown option").prop("selected", "selected");

    var val = $("#typeDropdown").val();
    if('decile' == val) {
      var decileYearMonthDto = new Object();
      decileYearMonthDto.decileYearFrom = $('#decileYearFrom' ).val();
      decileYearMonthDto.decileMonthFrom = $('#decileMonthFrom' ).val();
      decileYearMonthDto.decileYearTo = $('#decileYearTo' ).val();
      decileYearMonthDto.decileMonthTo = $('#decileMonthTo' ).val();
      $.ajax( {
        "contentType" : 'application/json',
        "type" : "POST",
        "url" : '<spring:url value="/dataanalysis/decile/validate"/> ',
        "data" : JSON.stringify( decileYearMonthDto ),
        "success" : function ( errorMessages ) {
          if ( errorMessages.length > 0 ) {
            var errorInfo = "1. " + errorMessages[0];
            for ( var i = 1; i < errorMessages.length; i++ ) {
              errorInfo += "<br>" + (i + 1) + ". " + errorMessages[i];
            }
            showErrorMessage(errorInfo);
          } else {
            $( ".alert", $errorMessagesContainer ).alert( 'close' )

            var $form = $("#searchDto");
            $.post($form.attr("action") , $form.serialize(), function(html) {
              $("#analysisContainer").html(html);
            }, "html");
          }
        },
        "error" : function ( xhr, textStatus, error ) {
          if ( textStatus === 'timeout' ) {
            showErrorMessage( 'The server took too long to send the data.' );
          } else if ( error ) {
            showErrorMessage(error);
          } else {
            showErrorMessage( 'An error occurred on the server. Please try again.' );
          }
        }
      } );
    } else {
      var $form = $("#searchDto");
      $.post($form.attr("action") , $form.serialize(), function(html) {
        $("#analysisContainer").html(html);
      }, "html");
    }
	});

	function toggleAnalysis() {
		var val = $("#typeDropdown").val();
		$(".analysis").hide();
		$("." + val).show();
    if('decile' == val) {
      $('#advancedSearchChkContainer' ).hide();
      $('#printContainer' ).hide();
    } else {
      $('#advancedSearchChkContainer' ).show();
      $('#printContainer' ).show();
    }
	}
	
	$("#typeDropdown").change(toggleAnalysis);
	
	toggleAnalysis();
	
	$.get($("#storeGroupsD").data("url"), function(data) {
		var inReturnEntity = data.result;
		$("#storeGroupsD").empty();
		$.each(inReturnEntity, function(key, value) {
			var val = value.id;
			var label = value.name;
			$("<option>").attr("value", val).text(label).appendTo($("#storeGroupsD"));
		});	
	}, "json");
	
	$.get($("#productGroupsD").data("url"), function(data) {
		var inReturnEntity = data.result;
		$("#productGroupsD").empty();
		$.each(inReturnEntity, function(key, value) {
			var val = value.id;
			var label = value.name;
			$("<option>").attr("value", val).text(label).appendTo($("#productGroupsD"));
		});	
	}, "json");
	
	$("input[name='storeType']").change(function() {
		var val = $(this).val();
		if(val == 'stores') {
			$("#storeGroupSel").hide();
			$("#storeSel").show();
		} else if(val == 'storeGroups') {
			$("#storeGroupSel").show();
			$("#storeSel").hide();
		}
			
	});
	
	$("input[name='productType']").change(function() {
		var val = $(this).val();
		if(val == 'products') {
			$("#productGroupSel").hide();
			$("#productSel").show();
		} else if(val == 'productGroups') {
			$("#productGroupSel").show();
			$("#productSel").hide();
		}
			
	});
	
	$("#advancedSearchChk").click(function() {
		if($(this).is(":checked")) {
			$(".advancedSearch").show();	
		} else {
			$(".advancedSearch").hide();
		}
	});
	
	$("#productGrpAddBtn").click(function(e) {
		e.preventDefault();
		$.each($("#productGroupsD").find("option:selected"), function() {$("#productGroups").append($(this));});
	});
	$("#productGrpRemoveBtn").click(function(e) {
		e.preventDefault();
		$.each($("#productGroups").find("option:selected"), function() {$("#productGroupsD").append($(this));});
	});
	
	$("#storeGrpAddBtn").click(function(e) {
		e.preventDefault();
		$.each($("#storeGroupsD").find("option:selected"), function() {$("#storeGroups").append($(this));});
	});
	$("#storeGrpRemoveBtn").click(function(e) {
		e.preventDefault();
		$.each($("#storeGroups").find("option:selected"), function() {$("#storeGroupsD").append($(this));});
	});
	
	$("#storeAddBtn").click(function(e) {
		e.preventDefault();
		$.each($("#storesD").find("option:selected"), function() {$("#stores").append($(this));});
	});
	$("#storeRemoveBtn").click(function(e) {
		e.preventDefault();
		$.each($("#stores").find("option:selected"), function() {$("#storesD").append($(this));});
	});
	
	$("#productAddBtn").click(function(e) {
		e.preventDefault();
		$.each($("#productsD").find("option:selected"), function() {$("#products").append($(this));});
	});
	$("#productRemoveBtn").click(function(e) {
		e.preventDefault();
		$.each($("#products").find("option:selected"), function() {$("#productsD").append($(this));});
	});
	
	$("#printButton").click(function(e) {
		e.preventDefault();
		var url = $("#reportType option:selected").data("url") + "?" + $("#searchDto").serialize();
		var selectedReportType = $("select[name='reportType']").val();
		if(selectedReportType == "pdf")
			window.open(url, '_blank', 'toolbar=0,location=0,menubar=0' );
		else if(selectedReportType == "excel")
			window.location.href = url;
	});
});
</script>
<script type="text/javascript">
 $('.selectpicker').selectpicker({
style: 'btn-print-drop',
size: 4,
showIcon: false
});
</script>
<style>
#printContainer {
  margin-bottom: 20px;
}
#storeAddBtn, #storeGrpAddBtn, #productGrpAddBtn {
  margin-bottom: 5px;
  margin-top: 1px;
}
.well input.btn {
  margin-left: 5px;
}
.well {padding-bottom: 10px; padding-top: 30px}
</style>

