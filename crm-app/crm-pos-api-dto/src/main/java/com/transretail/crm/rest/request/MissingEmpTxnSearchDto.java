package com.transretail.crm.rest.request;

/**
 * @author Allan G. Ramirez (agramirez@exist.com)
 */
public class MissingEmpTxnSearchDto extends MissingPointsSearchDto {
    private String mediaType;

    public String getMediaType() {
        return mediaType;
    }

    public void setMediaType(String mediaType) {
        this.mediaType = mediaType;
    }
}
