package com.transretail.crm.report.template.impl;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.joda.time.LocalDate;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.MessageSourceAware;
import org.springframework.context.support.MessageSourceAccessor;
import org.springframework.stereotype.Service;

import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Lists;
import com.transretail.crm.common.reporting.jasper.ClasspathFileResolver;
import com.transretail.crm.common.reporting.jasper.DefaultJRProcessor;
import com.transretail.crm.common.reporting.jasper.JRProcessor;
import com.transretail.crm.common.service.dto.response.ResultList;
import com.transretail.crm.core.entity.enums.ReportType;
import com.transretail.crm.core.entity.lookup.Store;
import com.transretail.crm.core.security.util.UserUtil;
import com.transretail.crm.core.service.StoreService;
import com.transretail.crm.giftcard.dto.GiftCardInventoryDto;
import com.transretail.crm.giftcard.dto.GiftCardInventorySearchDto;
import com.transretail.crm.giftcard.repo.GiftCardInventoryRepo;
import com.transretail.crm.giftcard.service.GiftCardInventoryStockService;
import com.transretail.crm.report.template.FilterField;
import com.transretail.crm.giftcard.entity.QGiftCardInventory;
/**
 * @author ftopico
 */

@Service("giftCardInventoryStockListDocument")
public class GiftCardInventoryStockSummaryListDocument implements MessageSourceAware {
    
    @Autowired
    private GiftCardInventoryStockService giftCardInventoryStockService;
    @Autowired
    private StoreService storeService;
    @Autowired
    private GiftCardInventoryRepo giftCardInventoryRepo;

    protected static final String TEMPLATE_NAME = "Gift Card Inventory Stock List Document";
    protected static final String REPORT_NAME = "reports/gift_card_inventory_stock_list_document.jasper";
    //Header Params
    private static final String PARAM_PRINT_DATE = "printDate";
    private static final String PARAM_PRINT_BY = "printBy";
    private static final DateTimeFormatter FULL_DATE_PATTERN = DateTimeFormat.forPattern("MMMM d, yyyy");
    private MessageSourceAccessor messageSource;
    
    public ReportType[] getReportTypes() {
        return ReportType.values();
    }
    
    @Override
    public void setMessageSource(MessageSource messageSource) {
        this.messageSource = new MessageSourceAccessor(messageSource);
    }
    
    public String getName() {
        return TEMPLATE_NAME;
    }
    
    public String getDescription() {
        return messageSource.getMessage("title_document_inventory_stock_list");
    }
    
    public Set<FilterField> getFilters() {
        return null;
    }
    
    public JRProcessor createJrProcessor(GiftCardInventorySearchDto searchDto) {
    
    	ResultList<GiftCardInventoryDto> resultList = giftCardInventoryStockService.getGiftCardInventoriesForPrintSummary(searchDto);
    	
    	List<ReportBean> beansDataSource = Lists.newArrayList();
    	DecimalFormat df = new DecimalFormat("#,##0");
    	
    	String location = null;
    	String description = null;
    	Integer serial = 0;
    	Map<String, GiftCardSummaryReport> reportMap = new LinkedHashMap<String, GiftCardInventoryStockSummaryListDocument.GiftCardSummaryReport>();
    	List<GiftCardSummaryReportDetails> reportDetails = new ArrayList<GiftCardInventoryStockSummaryListDocument.GiftCardSummaryReportDetails>();
    	List<GiftCardSummaryReport> reportSummary = new ArrayList<GiftCardInventoryStockSummaryListDocument.GiftCardSummaryReport>();
    	
    	for (GiftCardInventoryDto giftCardInventoryStockDto : resultList.getResults()) {
    	    if (location == null || !location.equalsIgnoreCase(giftCardInventoryStockDto.getLocation())) {
    	        Store store = storeService.getStoreByCode(giftCardInventoryStockDto.getLocation());
                location = giftCardInventoryStockDto.getLocation();
    	        if (store != null) {
    	            description = store.getName();
    	        } else {
    	            if (UserUtil.getCurrentUser().getInventoryLocation().equals(giftCardInventoryStockDto.getLocation())) {
    	                description = UserUtil.getCurrentUser().getInventoryLocationName();
    	            }
    	        }
    	        serial = 1;
    	    }
    	    else if (location.equalsIgnoreCase(giftCardInventoryStockDto.getLocation()))
    	        serial++;
    	    
    	    Long count = null;
    	    QGiftCardInventory qModel = QGiftCardInventory.giftCardInventory;
    	    
    	    if (giftCardInventoryStockDto.getLocation() != null && giftCardInventoryStockDto.getStatus()!= null) {
        	    count = giftCardInventoryRepo.count(qModel.location.eq(giftCardInventoryStockDto.getLocation())
        	                                        .and(qModel.productCode.eq(giftCardInventoryStockDto.getProductCode())
                                                        .and(qModel.status.eq(giftCardInventoryStockDto.getStatus())
                                                                .and(qModel.allocateTo.isNull()))));
    	    }
    	    
            if (location != null && count != null) {
                reportDetails.add(new GiftCardSummaryReportDetails(
            	        serial.toString(),
            	        location,
            	        description,
            	        giftCardInventoryStockDto.getProductName(),
            	        giftCardInventoryStockDto.getProductCode(),
            	        df.format(count)));
                
                if (!reportMap.containsKey(giftCardInventoryStockDto.getProductCode())) {
                    GiftCardSummaryReport giftCardSummaryReport = new GiftCardSummaryReport();
                    giftCardSummaryReport.setProductCode(giftCardInventoryStockDto.getProductCode());
                    giftCardSummaryReport.setProductName(giftCardInventoryStockDto.getProductName());
                    giftCardSummaryReport.setTotalQuantity(count);
                    reportMap.put(giftCardInventoryStockDto.getProductCode(), giftCardSummaryReport);
                } else {
                    GiftCardSummaryReport giftCardSummaryReport = reportMap.get(giftCardInventoryStockDto.getProductCode());
                    giftCardSummaryReport.setTotalQuantity(giftCardSummaryReport.getTotalQuantity() + count);
                }
            }
        }
    	
    	Map<String, Object> parameters = ImmutableMap.<String, Object>builder()
    	        .put(PARAM_PRINT_DATE, FULL_DATE_PATTERN.print(new LocalDate()))
    	        .put(PARAM_PRINT_BY, UserUtil.getCurrentUser().getUsername()).build();
    	
    	reportSummary.addAll(reportMap.values());
    	beansDataSource.add(new ReportBean(reportDetails, reportSummary));
    	DefaultJRProcessor jrProcessor = new DefaultJRProcessor(REPORT_NAME, beansDataSource);
    	jrProcessor.addParameters(parameters);
    	jrProcessor.setFileResolver(new ClasspathFileResolver("reports"));
    	
    	return jrProcessor;
    	
    }
    
    public class GiftCardSummaryReportDetails {
        
        private String serial;
        private String location;
        private String description;
        private String productName;
        private String productCode;
        private String totalQuantity;
        
        public GiftCardSummaryReportDetails() {}
        
        public GiftCardSummaryReportDetails(String serial, 
                            String location,
                            String description,
                            String productName,
                            String productCode,
                            String totalQuantity) {
            super();
            this.serial = serial;
            this.location = location;
            this.description = description;
            this.productName = productName;
            this.productCode = productCode;
            this.totalQuantity = totalQuantity;
        }

        public String getSerial() {
            return serial;
        }

        public void setSerial(String serial) {
            this.serial = serial;
        }

        public String getLocation() {
            return location;
        }

        public void setLocation(String location) {
            this.location = location;
        }

        public String getDescription() {
            return description;
        }

        public void setDescription(String description) {
            this.description = description;
        }

        public String getProductName() {
            return productName;
        }

        public void setProductName(String productName) {
            this.productName = productName;
        }

        public String getProductCode() {
            return productCode;
        }

        public void setProductCode(String productCode) {
            this.productCode = productCode;
        }

        public String getTotalQuantity() {
            return totalQuantity;
        }

        public void setTotalQuantity(String totalQuantity) {
            this.totalQuantity = totalQuantity;
        }

    }
    
    public class GiftCardSummaryReport {
        private String productName;
        private String productCode;
        private Long totalQuantity;
        
        public GiftCardSummaryReport() {}
        
        public GiftCardSummaryReport(String productName,
                            String productCode,
                            Long totalQuantity) {
            super();
            this.productName = productName;
            this.productCode = productCode;
            this.totalQuantity = totalQuantity;
        }

        public String getProductName() {
            return productName;
        }

        public void setProductName(String productName) {
            this.productName = productName;
        }

        public String getProductCode() {
            return productCode;
        }

        public void setProductCode(String productCode) {
            this.productCode = productCode;
        }

        public Long getTotalQuantity() {
            return totalQuantity;
        }

        public void setTotalQuantity(Long totalQuantity) {
            this.totalQuantity = totalQuantity;
        }
    }
    
    public static class ReportBean {
        
        private List<GiftCardSummaryReportDetails> reportDetails;
        private List<GiftCardSummaryReport> reportSummary;
        
        public ReportBean() {}
        
        public ReportBean(List<GiftCardSummaryReportDetails> reportDetails,
                            List<GiftCardSummaryReport> reportSummary) {
            super();
            this.reportDetails = reportDetails;
            this.reportSummary = reportSummary;
        }

        public List<GiftCardSummaryReportDetails> getReportDetails() {
            return reportDetails;
        }

        public void setReportDetails(List<GiftCardSummaryReportDetails> reportDetails) {
            this.reportDetails = reportDetails;
        }

        public List<GiftCardSummaryReport> getReportSummary() {
            return reportSummary;
        }

        public void setReportSummary(List<GiftCardSummaryReport> reportSummary) {
            this.reportSummary = reportSummary;
        }

    }
}
