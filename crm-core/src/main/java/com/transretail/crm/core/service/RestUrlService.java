package com.transretail.crm.core.service;


import com.transretail.crm.common.service.dto.rest.ReturnMessage;

public interface RestUrlService {

    String getPrimaryWsUrl();


    boolean callPrimaryWsUrl(String mapper, String urlParameters, boolean httpGet, ReturnMessage ret, String serverIp);


	String getPrimaryWsUrlForStores();


	boolean callPrimaryWsUrl(String wsMapping, String urlParameters,
			boolean httpGet, ReturnMessage ret, String serverIp,
			String primaryUrl);


	boolean redirectToPrimary(String wsMapping, String urlParameters,
			boolean httpGet, ReturnMessage ret, String serverIp);

}
