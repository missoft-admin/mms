<%@ include file="../../common/taglibs.jsp" %>
<c:set var="discount" value="${discVal.toString()}" />
<c:set var="totalBeforeDisc" value="0" />
<c:set var="totalDisc" value="0" />
<c:set var="total" value="0" />
<c:set var="totalAmountDue" value="0"  scope="request" />
<c:forEach items="${countSummary}" var="item">
<c:set var="totalBeforeDisc">${item.totalAmount + totalBeforeDisc}</c:set>
<c:set var="discountedTotal">${(discount / 100) * item.totalAmount}</c:set>
<c:set var="totalDisc">${discountedTotal + totalDisc}</c:set>
<c:set var="total">${item.totalAmount - discountedTotal}</c:set>
<c:set var="totalAmount">${total + totalAmount}</c:set>
<tr>
  <td>${item.profileName}</td>
  <td style="font-size: x-small;">
  <c:forEach items="${item.gcs}" var="gcItem">
  ${gcItem.startingSeries} - ${gcItem.endingSeries}<br/>
  </c:forEach>
  </td>
  <td style="text-align: right;"><fmt:formatNumber type="number" pattern="#,##0" value="${item.faceValue}" /></td>
  <td style="text-align: right;">${item.count}</td>
  <td style="text-align: right;"><fmt:formatNumber type="number" pattern="#,##0" value="${item.totalAmount}" /></td>
  <td style="text-align: right;"><fmt:formatNumber type="number" pattern="#,##0" value="${discountedTotal}" /></td>
  <td style="text-align: right;"><fmt:formatNumber type="number" pattern="#,##0" value="${total}" /></td>
  <td>
  <c:if test="${empty readOnly or !readOnly}">
  <button type="button" class="tiptip btn pull-left icn-delete deleteProduct" data-profile-id="${item.profileId}" title="<spring:message code="label_delete"/>"></button>
  </c:if>
  </td>
</tr>
</c:forEach>
<c:if test="${not empty countSummary}">
<c:set var="taxVal">${totalAmount * (tax / 100)}</c:set>
<c:set var="totalAmountDue" scope="request">${totalAmount - taxVal}</c:set>
<tr>
  <td>&nbsp;</td>
  <td>&nbsp;</td>
  <td>&nbsp;</td>
  <td style="text-align: right;"><label><spring:message code="b2b.exhibition.total" /></label></td>
  <td style="border-top: solid;text-align: right;"><fmt:formatNumber type="number" pattern="#,##0" value="${totalBeforeDisc}" /></td>
  <td style="border-top: solid;text-align: right;"><fmt:formatNumber type="number" pattern="#,##0" value="${totalDisc}" /></td>
  <td style="border-top: solid;text-align: right;"><fmt:formatNumber type="number" pattern="#,##0" value="${totalAmount}" /></td>
  <td></td>
</tr>
<tr>
  <td>&nbsp;</td>
  <td>&nbsp;</td>
  <td>&nbsp;</td>
  <td>&nbsp;</td>
  <td>&nbsp;</td>
  <td style="text-align: right;"><label><spring:message code="b2b.exhibition.tax" arguments="${tax}" /></label></td>
  <td style="text-align: right;"><fmt:formatNumber type="number" pattern="#,##0" value="${taxVal}" /></td>
  <td></td>
</tr>
<tr>
  <td>&nbsp;</td>
  <td>&nbsp;</td>
  <td>&nbsp;</td>
  <td>&nbsp;</td>
  <td>&nbsp;</td>
  <td style="text-align: right;"><label><spring:message code="b2b.exhibition.amountdue" /></label></td>
  <td style="border-top: solid;text-align: right; border-bottom-style: double;"><fmt:formatNumber type="number" pattern="#,##0" value="${totalAmountDue}" /></td>
  <td></td>
</tr>
</c:if>
<input type="hidden" name="discount" value="${discount}" />
<input type="hidden" name="totalAmountDue" value="${totalAmountDue}" />
<input type="hidden" name="totalTaxableAmt" value="${totalAmount}" />
<input type="hidden" name="tax" value="${tax}" />


<script type="text/javascript">
$(document).ready(function() {
  
	$(".deleteProduct").click(function() {
		var prodId = $(this).data("profileId");
		$(".gc" + prodId).val("");
		$(".gcl" + prodId).remove();
		refreshProductsTable();
	});
  
  
});
</script>