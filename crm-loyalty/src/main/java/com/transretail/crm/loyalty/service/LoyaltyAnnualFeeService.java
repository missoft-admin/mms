package com.transretail.crm.loyalty.service;

import com.transretail.crm.common.service.dto.response.ResultList;
import com.transretail.crm.core.entity.lookup.LookupDetail;
import com.transretail.crm.loyalty.dto.LoyaltyAnnualFeeSchemeDto;
import com.transretail.crm.loyalty.dto.LoyaltyAnnualFeeSchemeSearchDto;
import com.transretail.crm.loyalty.entity.LoyaltyAnnualFeeScheme;





public interface LoyaltyAnnualFeeService {

	void saveScheme(LoyaltyAnnualFeeSchemeDto schemeDto);

	void updateScheme(LoyaltyAnnualFeeSchemeDto schemeDto);

	ResultList<LoyaltyAnnualFeeSchemeDto> listSchemes(LoyaltyAnnualFeeSchemeSearchDto sortDto);

	LoyaltyAnnualFeeSchemeDto getScheme(Long id);

	void deleteScheme(Long id);

	LoyaltyAnnualFeeScheme getScheme(LookupDetail company, LookupDetail cardType);

	LoyaltyAnnualFeeSchemeDto getScheme(String barcode);
	
	
}
