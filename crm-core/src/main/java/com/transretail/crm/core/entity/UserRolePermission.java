package com.transretail.crm.core.entity;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import org.hibernate.envers.Audited;

import com.transretail.crm.core.entity.support.CustomAuditableEntity;

/**
 * @author Allan G. Ramirez (agramirez@exist.com)
 */
@Audited
@Entity
@Table(name = "CRM_USER_ROLE_PERMISSION", uniqueConstraints = {@UniqueConstraint(columnNames = {"USER_ID", "ROLE_ID", "PERM_ID"})})
public class UserRolePermission extends CustomAuditableEntity<Long> implements Serializable {
    private static final long serialVersionUID = -2208961046806469283L;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "USER_ID", nullable = false)
    private UserModel userId;
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "ROLE_ID", nullable = false)
    private UserRoleModel roleId;
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "PERM_ID")
    private UserPermission permId;

    public UserRolePermission() {

    }

    public UserRolePermission(UserModel userId, UserRoleModel roleId, UserPermission permId) {
        this.userId = userId;
        this.roleId = roleId;
        this.permId = permId;
    }

    public UserModel getUserId() {
        return userId;
    }

    public void setUserId(UserModel userId) {
        this.userId = userId;
    }

    public UserRoleModel getRoleId() {
        return roleId;
    }

    public void setRoleId(UserRoleModel roleId) {
        this.roleId = roleId;
    }

    public UserPermission getPermId() {
        return permId;
    }

    public void setPermId(UserPermission permId) {
        this.permId = permId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;

        UserRolePermission that = (UserRolePermission) o;

        if (permId != null ? !permId.equals(that.permId) : that.permId != null) return false;
        if (!roleId.equals(that.roleId)) return false;
        if (!userId.equals(that.userId)) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + userId.hashCode();
        result = 31 * result + roleId.hashCode();
        result = 31 * result + (permId != null ? permId.hashCode() : 0);
        return result;
    }
}
