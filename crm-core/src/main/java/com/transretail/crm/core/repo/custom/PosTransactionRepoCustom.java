package com.transretail.crm.core.repo.custom;

import java.util.List;
import java.util.Map;

import com.transretail.crm.core.dto.ShoppingListDto;

public interface PosTransactionRepoCustom {
	Double getAverageTransactions(String accountId, String productId,
		Integer timeFrameIncludeInMonths, Integer timeFrameExcludeInDays);
	List<ShoppingListDto> rankItemsByTransactions(String accountId, Integer timeFrameIncludeInMonths, 
		Integer timeFrameExcludeInDays, Integer minTransactions);
	List<ShoppingListDto> rankItemsByTransactions(String accountId, Integer timeFrameIncludeInMonths, 
			Integer timeFrameExcludeInDays, Integer minTransactions, Integer limit, Integer offset);
	Map<String, ShoppingListDto> getShoppingSuggestions(String accountId, Integer timeFrameIncludeInMonths, 
		Integer timeFrameExcludeInDays, Integer minTransactions);
}
