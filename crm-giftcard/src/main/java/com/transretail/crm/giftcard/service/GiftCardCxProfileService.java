package com.transretail.crm.giftcard.service;


import java.util.List;

import com.transretail.crm.common.service.dto.request.PageSortDto;
import com.transretail.crm.common.service.dto.response.ResultList;
import com.transretail.crm.core.dto.ApprovalRemarkDto;
import com.transretail.crm.giftcard.dto.GiftCardCxProfileDto;
import com.transretail.crm.giftcard.dto.GiftCardCxProfileSearchDto;


public interface GiftCardCxProfileService {

	GiftCardCxProfileDto getDto(Long id);

	GiftCardCxProfileDto saveDto(GiftCardCxProfileDto dto);

	GiftCardCxProfileDto updateDto(GiftCardCxProfileDto dto);

	ResultList<GiftCardCxProfileDto> search(GiftCardCxProfileSearchDto dto);

	List<GiftCardCxProfileDto> getAllProfiles();

	List<GiftCardCxProfileDto> getProfiles(GiftCardCxProfileSearchDto dto);

	void saveCustomerNotes(Long cxProfileId, String notes);

	ResultList<ApprovalRemarkDto> searchNotes(Long id, PageSortDto searchDto);

	Long generateCustId();

}
