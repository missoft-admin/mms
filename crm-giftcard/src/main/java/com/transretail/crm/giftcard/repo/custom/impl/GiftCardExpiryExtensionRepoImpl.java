package com.transretail.crm.giftcard.repo.custom.impl;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.joda.time.LocalDate;

import com.mysema.query.jpa.impl.JPAQuery;
import com.transretail.crm.giftcard.entity.QGiftCardExpiryExtension;
import com.transretail.crm.giftcard.repo.custom.GiftCardBurnCardRepoCustom;
import com.transretail.crm.giftcard.repo.custom.GiftCardExpiryExtensionRepoCustom;

/**
 * @author ftopico
 */
public class GiftCardExpiryExtensionRepoImpl implements GiftCardExpiryExtensionRepoCustom {
    @PersistenceContext
    EntityManager em;

    @Override
    public String createExtendNo() {
        QGiftCardExpiryExtension expiryExtension = QGiftCardExpiryExtension.giftCardExpiryExtension;
        JPAQuery query = new JPAQuery(em);
        
        LocalDate today = LocalDate.now();
        StringBuilder builder = new StringBuilder(today.toString("yyyyMMdd"));
        Long count = query.from(expiryExtension).where(expiryExtension.createDate.eq(today))
                .uniqueResult(expiryExtension.extendNo.countDistinct()) + 1;
        builder.append(String.format("%04d", count != null ? count : 0));
        return builder.toString();
    }

}
