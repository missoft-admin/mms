package com.transretail.crm.web.member.service.impl;

import static org.mockito.Mockito.any;
import static org.mockito.Mockito.anyString;
import static org.mockito.Mockito.verify;
import static org.powermock.api.mockito.PowerMockito.doNothing;
import static org.powermock.api.mockito.PowerMockito.doReturn;
import static org.powermock.api.mockito.PowerMockito.mock;
import static org.powermock.api.mockito.PowerMockito.when;
import static org.powermock.api.mockito.PowerMockito.whenNew;

import java.io.StringWriter;
import java.io.Writer;
import java.util.HashMap;
import java.util.Locale;

import javax.mail.internet.MimeMessage;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.powermock.core.classloader.annotations.PowerMockIgnore;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.springframework.context.MessageSource;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.web.WebAppConfiguration;

import com.transretail.crm.web.member.service.EmailService;

import freemarker.template.Configuration;
import freemarker.template.Template;

@RunWith(PowerMockRunner.class)
@PowerMockIgnore({"javax.management.*", "javax.xml.parsers.*", "com.sun.org.apache.xerces.internal.jaxp.*", "ch.qos.logback.*", "org.slf4j.*"})
@PrepareForTest({MimeMessageHelper.class, StringWriter.class})
@ContextConfiguration(locations = "classpath*:/META-INF/spring/applicationContext*.xml")
@WebAppConfiguration
public class EmailServiceImplTest {
	@Mock
	JavaMailSender mailSender;
	
	@Mock
	Configuration config;
	
	@Mock
	MessageSource msgSource;
	
//	@Autowired
	@InjectMocks
    private EmailServiceImpl emailService = new EmailServiceImpl();
	
	@Before
	public void setUp() {
        emailService.setFromName("customer_support");
        emailService.setFromAddress("customer_support@transretail.co.id");
		MockitoAnnotations.initMocks(this);
	}

//	@Ignore
	@Test
	public void testSendValidationLink() throws Exception {
		Template template = mock(Template.class);
		Locale locale = Locale.ENGLISH;
		MimeMessage mimeMsg = mock(MimeMessage.class);
		MimeMessageHelper mockHelper = mock(MimeMessageHelper.class);
		StringWriter mockWriter = mock(StringWriter.class);
		whenNew(MimeMessageHelper.class).withArguments(any(MimeMessage.class)).thenReturn(mockHelper);
		whenNew(StringWriter.class).withNoArguments().thenReturn(mockWriter);
		when(config.getTemplate(anyString(), any(Locale.class))).thenReturn(template);
		Object[] nullObject = null;
		when(msgSource.getMessage(anyString(), Mockito.eq(nullObject), any(Locale.class))).thenReturn("test");
		when(mailSender.createMimeMessage()).thenReturn(mimeMsg);
		when(mockWriter.toString()).thenReturn("test");
		doReturn(mimeMsg).when(mockHelper).getMimeMessage();
		doNothing().when(mailSender).send(any(MimeMessage.class));
		
		emailService.sendValidationLink("test", "test", "test", new HashMap<String, Object>(), locale);
		
		verify(template).process(any(), any(Writer.class));
		verify(mailSender).send(any(MimeMessage.class));
	}

}
