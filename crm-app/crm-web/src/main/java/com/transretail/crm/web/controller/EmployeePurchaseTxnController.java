package com.transretail.crm.web.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.google.common.collect.Lists;
import com.transretail.crm.common.service.dto.request.PageSortDto;
import com.transretail.crm.common.service.dto.response.ResultList;
import com.transretail.crm.common.web.notification.NotificationType;
import com.transretail.crm.common.web.notification.annotation.Notifiable;
import com.transretail.crm.common.web.notification.annotation.NotificationUpdate;
import com.transretail.crm.core.dto.EmployeePurchaseTxnDto;
import com.transretail.crm.core.dto.MemberDto;
import com.transretail.crm.core.dto.PurchaseTxnSearchDto;
import com.transretail.crm.core.entity.enums.EmployeeSearchCriteriaTwo;
import com.transretail.crm.core.entity.enums.TxnStatus;
import com.transretail.crm.core.entity.enums.TxnStatusSearch;
import com.transretail.crm.core.entity.enums.VoucherTransactionType;
import com.transretail.crm.core.entity.lookup.Store;
import com.transretail.crm.core.security.model.impl.CustomSecurityUserDetailsImpl;
import com.transretail.crm.core.security.util.UserUtil;
import com.transretail.crm.core.service.EmployeePurchaseService;
import com.transretail.crm.core.service.EmployeeService;
import com.transretail.crm.core.service.HrOverridePermissionsService;
import com.transretail.crm.core.service.MemberService;
import com.transretail.crm.core.service.StoreService;
import com.transretail.crm.web.controller.util.ControllerResponse;
import com.transretail.crm.web.controller.util.ControllerResponseExceptionHandler;
//import com.transretail.crm.web.notifications.Notifiable;
//import com.transretail.crm.web.notifications.NotificationUpdate;

@RequestMapping("/employee/adjustments")
@Controller
public class EmployeePurchaseTxnController extends ControllerResponseExceptionHandler {
    private static final String UI_ATTR_MODELS = "employeePurchaseTxns";
    private static final String UI_ATTR_ID = "id";
	private static final String UI_ATTR_MODEL = "employeePurchaseTxn";
	private static final String UI_ATTR_IS_APPROVER = "isApprover";
	private static final String UI_ATTR_SEARCHFIELDS = "purchaseAdjustmentSearchFields";
	private static final String UI_ATTR_FILTER_STATUS = "purchaseAdjustmentFilterStatus";
	private static final String UI_ATTR_FILTER_TXN_LOCATION = "purchaseAdjustmentFilterTxnLocation";
	private static final String UI_ATTR_PURCHASES = "purchases";
	private static final String ACTION_APPROVE = "approve";
	private static final String ACTION_REJECT = "reject";
	
	@Autowired
	MemberService memberService;
	
	@Autowired
	EmployeeService employeeService;
	
	@Autowired
	StoreService storeService;
	
	@Autowired
	EmployeePurchaseService employeePurchaseService;
	
	@Autowired
	MessageSource messageSource;
	
	@Autowired
	HrOverridePermissionsService overridePermissionsService;
	
	@RequestMapping(value = "/purchases", params={"page", "size"}, produces = "text/html")
    public String list(
            @RequestParam(value = "page", required = false) Integer page,
            @RequestParam(value = "size", required = false) Integer size,
            Model uiModel) {
		
		uiModel.addAttribute(UI_ATTR_MODEL, new EmployeePurchaseTxnDto());
		uiModel.addAttribute(UI_ATTR_IS_APPROVER, overridePermissionsService.isApprover(UserUtil.getCurrentUser()));
		uiModel.addAttribute(UI_ATTR_SEARCHFIELDS, EmployeeSearchCriteriaTwo.values());
		populatePurhcaseAdjustmentFilters(uiModel);
		
        return "/employee/adjustments/purchases/list";
    }
	
	@RequestMapping(value = "/purchases", produces = "text/html")
    public String list(Model uiModel) {
		
		uiModel.addAttribute(UI_ATTR_MODEL, new EmployeePurchaseTxnDto());
		uiModel.addAttribute(UI_ATTR_IS_APPROVER, overridePermissionsService.isApprover(UserUtil.getCurrentUser()));
		uiModel.addAttribute(UI_ATTR_SEARCHFIELDS, EmployeeSearchCriteriaTwo.values());
		populatePurhcaseAdjustmentFilters(uiModel);
		
        return "/employee/adjustments/purchases/list";
    }
	
	@RequestMapping(value = "/list", method = RequestMethod.POST)
    public @ResponseBody ResultList<EmployeePurchaseTxnDto> listPurchaseTxn(@RequestBody PurchaseTxnSearchDto searchForm) {
    	return employeePurchaseService.searchPurchaseTxn( searchForm );
    }
	
	@RequestMapping(value = "/purchases/forapproval", params={"page", "size"}, produces = "text/html")
    public String listForApproval(
            @RequestParam(value = "page", required = false) Integer page,
            @RequestParam(value = "size", required = false) Integer size,
            Model uiModel) {
		
		uiModel.addAttribute(UI_ATTR_MODEL, new EmployeePurchaseTxnDto());
		uiModel.addAttribute(UI_ATTR_IS_APPROVER, overridePermissionsService.isApprover(UserUtil.getCurrentUser()));

        return "/employee/adjustments/purchases/forapproval/list";
    }
	
	@RequestMapping(value = "/purchases/forapproval/{id}", produces = "text/html")
    public String listForApproval(@PathVariable String id, Model uiModel) {
		
		uiModel.addAttribute(UI_ATTR_ID, id);
		uiModel.addAttribute(UI_ATTR_MODEL, new EmployeePurchaseTxnDto());
		uiModel.addAttribute(UI_ATTR_IS_APPROVER, overridePermissionsService.isApprover(UserUtil.getCurrentUser()));

        return "/employee/adjustments/purchases/forapproval/listOne";
    }
	
	@RequestMapping(value = "/purchases/getdetails/{id}", produces = "application/json; charset=utf-8", method = RequestMethod.GET)
	@ResponseBody
    public ControllerResponse getDetails(@PathVariable String id, Model uiModel) {
		ControllerResponse theResponse = new ControllerResponse();
		theResponse.setSuccess(true);
		List<EmployeePurchaseTxnDto> purchases = employeePurchaseService.getTransactionDetails(id);
		if(CollectionUtils.isNotEmpty(purchases)) {
			theResponse.setResult(purchases);
		} else {
			theResponse.setSuccess(false);
		}
        return theResponse;
    }
	
	@RequestMapping(value = "/forapproval/list", method = RequestMethod.POST)
	public @ResponseBody ResultList<EmployeePurchaseTxnDto> listForApproval(@RequestBody PageSortDto dto) {
		return employeePurchaseService.listTransactionDtoForApproval(dto);
	}
	
	@RequestMapping(value = "/list/{id}", method = RequestMethod.POST)
	public @ResponseBody ResultList<EmployeePurchaseTxnDto> list(@RequestBody PageSortDto dto, @PathVariable String id) {
		return new ResultList<EmployeePurchaseTxnDto>(Lists.newArrayList(employeePurchaseService.findById(id)));
	}
	

	private void validateAdjustment(BindingResult result, EmployeePurchaseTxnDto dto) {
		MemberDto theEmp = null;
		EmployeePurchaseTxnDto theTxn = null;
		if(StringUtils.isEmpty(dto.getAccountId()))
			result.reject(getMessage("propkey_msg_notempty", new Object[] {"purchasetxn_property_employeeid"}));
		else {
			theEmp = employeeService.findActiveEmployeeByAccountId(dto.getAccountId());
			if(theEmp == null) {
	            result.reject(messageSource.getMessage("emppurchasetxnad_error_no_emp", new Object[] {dto.getAccountId()}, LocaleContextHolder.getLocale()));
	        } else {
	        	dto.setMemberModel(theEmp);
	        	dto.setMemberAccountId(theEmp.getAccountId());
	        }	
		}

		if(StringUtils.isEmpty(dto.getTransactionNo()))
			result.reject(getMessage("propkey_msg_notempty", new Object[] {"purchasetxn_property_transactionno"}));
		else {
			theTxn = employeePurchaseService.findOneByTransactionNo(dto.getTransactionNo());
	        if(theTxn == null) {
	           result.reject(messageSource.getMessage("emppurchasetxnad_error_no_txn", new Object[] {dto.getTransactionNo()}, LocaleContextHolder.getLocale()));
	        } else {
	        	if(theEmp != null && !theEmp.getAccountId().equals(theTxn.getMemberModel().getAccountId())) {
	        		result.reject(messageSource.getMessage("emppurchasetxnad_error_invalid_txn", new Object[] {dto.getTransactionNo(), theEmp.getAccountId()}, LocaleContextHolder.getLocale()));
	            }
	        	
	        	dto.setTransactionDateTime(theTxn.getTransactionDateTime());
	        	dto.setStoreModel(theTxn.getStoreModel());
	        }	
		}
		
		
		
		if(dto.getTransactionAmount() == null)
			result.reject(getMessage("propkey_msg_notempty", new Object[] {"purchasetxn_property_transactionamount"}));
	}
	
	private String getMessage(String inMsgCode, Object[] inArgMsgCodes) {
		if(inArgMsgCodes != null) {
			for ( int i=0; i < inArgMsgCodes.length; i++ ) {
				inArgMsgCodes[i] = messageSource.getMessage( inArgMsgCodes[i].toString().toLowerCase(), null, LocaleContextHolder.getLocale() );
			}
		}
		return messageSource.getMessage(inMsgCode, inArgMsgCodes, "Info given is invalid.", LocaleContextHolder.getLocale());
	}
	
	@Notifiable(type=NotificationType.PURCHASETXN)
	@RequestMapping(value = "/purchases/create", method = RequestMethod.POST, produces = "application/json; charset=utf-8")
	@ResponseBody
	public ControllerResponse ajaxCreate(
			@ModelAttribute(value = UI_ATTR_MODEL) EmployeePurchaseTxnDto dto,
			BindingResult result, HttpServletRequest request) {
		validateAdjustment(result, dto);

		ControllerResponse theResponse = new ControllerResponse();
		theResponse.setSuccess(true);

		if (result.hasErrors()) {
			theResponse.setSuccess(false);
			theResponse.setResult(result.getAllErrors());
			return theResponse;
		}

		if (theResponse.isSuccess()) {
			dto.setStatus(TxnStatus.FORAPPROVAL);
			dto.setTransactionType(VoucherTransactionType.ADJUSTED);
			dto = employeePurchaseService.savePurchaseTxn(dto);
			theResponse.setResult(dto);
			theResponse.setModelId(dto.getId());
			theResponse.setNotificationCreation(true);
		}

		return theResponse;
	}

	@RequestMapping(value = "/purchases/update/{id}", method = RequestMethod.POST, produces = "application/json; charset=utf-8")
	@ResponseBody
	public ControllerResponse ajaxUpdate(@PathVariable("id") String id,
			@ModelAttribute(value = UI_ATTR_MODEL) EmployeePurchaseTxnDto dto,
			BindingResult result, HttpServletRequest request) {
		validateAdjustment(result, dto);
		
		ControllerResponse theResponse = new ControllerResponse();
		theResponse.setSuccess(true);
		
		EmployeePurchaseTxnDto theTxn = employeePurchaseService.findPurchaseTransactionById(id);
		theTxn.setAccountId(dto.getAccountId());
		theTxn.setTransactionNo(dto.getTransactionNo());
		theTxn.setTransactionAmount(dto.getTransactionAmount());
		theTxn.setReason(dto.getReason());
		
		if (result.hasErrors()) {
			theResponse.setSuccess(false);
			theResponse.setResult(result.getAllErrors());
			return theResponse;
		}

		if (theResponse.isSuccess()) {
			employeePurchaseService.savePurchaseTxn(theTxn);
		}

		return theResponse;
	}

	@NotificationUpdate(type=NotificationType.PURCHASETXN)
	@RequestMapping(value = "/purchases/override/{action}/{id}", method = RequestMethod.POST, produces = "application/json; charset=utf-8")
	@ResponseBody
	public ControllerResponse ajaxReject(@PathVariable("id") String id,
			@PathVariable("action") String action,
			@ModelAttribute(value = UI_ATTR_MODEL) EmployeePurchaseTxnDto dto,
			BindingResult result, HttpServletRequest request) {

		ControllerResponse theResponse = new ControllerResponse();
		theResponse.setSuccess(true);
		
		EmployeePurchaseTxnDto theTxn = employeePurchaseService.findPurchaseTransactionById(id);
		theTxn.setRemarks(dto.getRemarks());
		if(action.equals(ACTION_APPROVE))
			theTxn.setStatus(TxnStatus.ACTIVE);
		if(action.equals(ACTION_REJECT))
			theTxn.setStatus(TxnStatus.REJECTED);
		
		CustomSecurityUserDetailsImpl userDetails = UserUtil.getCurrentUser();
		theTxn.setApprovedBy(userDetails.getUsername());
		theTxn.setApprovalDate(new DateTime());
		
		if (result.hasErrors()) {
			theResponse.setSuccess(false);
			theResponse.setResult(result.getAllErrors());
			return theResponse;
		}

		if (theResponse.isSuccess()) {
			theResponse.setResult(employeePurchaseService.savePurchaseTxn(theTxn));
			theResponse.setModelId(id);
		}

		return theResponse;
	}

	@RequestMapping(value = "/purchases/populate/{id}", method = RequestMethod.POST, produces = "application/json; charset=utf-8")
    @ResponseBody
    public ControllerResponse ajaxPopulate(
    		@PathVariable("id") String id,
    		@ModelAttribute(value=UI_ATTR_MODEL) EmployeePurchaseTxnDto txnDto,
            BindingResult result,
            HttpServletRequest request) {

    	ControllerResponse theResponse = new ControllerResponse();    	
    	theResponse.setSuccess(true);

		EmployeePurchaseTxnDto theTxn = employeePurchaseService.findPurchaseTransactionById(id);
		if(theTxn == null)
			theTxn = new EmployeePurchaseTxnDto();
    	theResponse.setResult(theTxn);
    	
        return theResponse;
    }
	
	@NotificationUpdate(type=NotificationType.PURCHASETXN)
	@RequestMapping(value = "/purchases/{id}", method = RequestMethod.DELETE, produces = "text/html")
    public String delete(@PathVariable("id") String id,
            @RequestParam(value = "page", required = false) Integer page,
            @RequestParam(value = "size", required = false) Integer size,
            Model uiModel) {
        employeePurchaseService.deletePurchaseTransaction(id);
        uiModel.asMap().clear();
        uiModel.addAttribute("page", (page == null) ? "1" : page.toString());
        uiModel.addAttribute("size", (size == null) ? "10" : size.toString());
        return "redirect:/employee/adjustments/purchases";
    }
	
	private void populatePurhcaseAdjustmentFilters( Model inUiModel ) {
        
        inUiModel.addAttribute(UI_ATTR_FILTER_TXN_LOCATION, storeService.getAllStores());
        inUiModel.addAttribute(UI_ATTR_FILTER_STATUS, TxnStatusSearch.values());
        
    }
}
