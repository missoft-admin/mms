package com.transretail.crm.web.controller;


import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.BooleanUtils;
import org.apache.commons.lang3.StringUtils;
import org.joda.time.LocalDate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.google.common.collect.Lists;
import com.transretail.crm.core.dto.MemberDto;
import com.transretail.crm.core.dto.StampAccountPortaRestDto;
import com.transretail.crm.core.entity.MemberModel;
import com.transretail.crm.core.entity.UserModel;
import com.transretail.crm.core.entity.lookup.Store;
import com.transretail.crm.core.security.model.impl.CustomSecurityUserDetailsImpl;
import com.transretail.crm.core.security.util.UserUtil;
import com.transretail.crm.core.service.CodePropertiesService;
import com.transretail.crm.core.service.MemberService;
import com.transretail.crm.core.service.StampPromoService;
import com.transretail.crm.core.service.StoreService;
import com.transretail.crm.core.service.UserService;
import com.transretail.crm.loyalty.dto.LoyaltyAnnualFeeSchemeDto;
import com.transretail.crm.loyalty.entity.LoyaltyCardInventory;
import com.transretail.crm.loyalty.service.LoyaltyAnnualFeeService;
import com.transretail.crm.loyalty.service.LoyaltyCardHistoryService;
import com.transretail.crm.loyalty.service.LoyaltyCardService;
import com.transretail.crm.web.controller.util.ControllerResponse;


@RequestMapping("/loyaltycard")
@Controller
public class LoyaltyCardController {
	
	@Autowired
	LoyaltyCardService loyaltyCardService;
	@Autowired
	MemberService memberService;
	@Autowired
	CodePropertiesService codePropertiesService;
	@Autowired
	MessageSource messageSource;
	@Autowired
	StoreService storeService;
	@Autowired
	UserService userService;
	@Autowired
	LoyaltyCardHistoryService loyaltyCardHistoryService;
	@Autowired
	LoyaltyAnnualFeeService loyaltyAnnualFeeService;
    @Autowired
    StampPromoService stampPromoService;

	private static final Logger LOGGER = LoggerFactory.getLogger(LoyaltyCardController.class);

	//private static final String PATH 	 			= "/loyaltycard";
	private static final String PATH_ASSIGN			= "/loyaltycard/assign";
	private static final String PATH_SAVE			= "/loyaltycard/assign/save";

	private static final String UIATTR_MEMBER		= "loyaltyAssign";

	private static final String UIATTR_ACTION		= "url_action";



	@RequestMapping( value = "/assign/{id}", produces = "text/html" )
    public String assignLoyalty( 
    		@PathVariable(value="id") Long inId, 
    		Model inUiModel ) {

    	MemberDto theDto = memberService.findCustomer( inId );
    	if(StringUtils.isNotBlank(theDto.getRegisteredStore())) {
    		Store store = storeService.getStoreByCode(theDto.getRegisteredStore());
    		if(store != null)
    			theDto.setRegisteredStoreName(store.getName());
    	}
    	if ( StringUtils.isNotBlank( theDto.getCreatedBy() ) ) {
    		UserModel user = userService.findUserModel( theDto.getCreatedBy() );
    		if ( null != user ) {
    			theDto.setCreatedByDetails( user.getUsername() 
    					+ ( StringUtils.isNotBlank( user.getFirstName() ) || StringUtils.isNotBlank( user.getLastName() )? " - " : "" ) 
    					+ ( StringUtils.isNotBlank( user.getFirstName() )? user.getFirstName() : "" )
    					+ ( StringUtils.isNotBlank( user.getLastName() )? " " + user.getLastName() : "" ) );
    		}
    	}
    	
    	Map<String, String> cardDetails = new HashMap<String, String>();
    	
    	if(StringUtils.isNotBlank(theDto.getLoyaltyCardNo())) {
    		LoyaltyAnnualFeeSchemeDto scheme = loyaltyAnnualFeeService.getScheme(theDto.getLoyaltyCardNo());	
    		theDto.setCurrentFee(scheme.getAnnualFee());
    		cardDetails.put("company", scheme.getCompany().getDescription());
    		cardDetails.put("cardType", scheme.getCardType().getDescription());
    		
    		LoyaltyCardInventory inventory = loyaltyCardService.findLoyaltyCard(theDto.getLoyaltyCardNo());
    		cardDetails.put("expiryDate", inventory.getExpiryDate().toString("dd-MM-yyyy"));
    	}
    	
    	
    	inUiModel.addAttribute( UIATTR_MEMBER, theDto );
    	inUiModel.addAttribute("cardDetails", cardDetails);
    	inUiModel.addAttribute( UIATTR_ACTION, PATH_SAVE );

        return PATH_ASSIGN;
    }

    @RequestMapping( value="/assign/save", produces="application/json; charset=utf-8", method=RequestMethod.POST )
	public @ResponseBody ControllerResponse saveAssignedLoyalty(
			@ModelAttribute(UIATTR_MEMBER) MemberDto inDto,
    		BindingResult inBinding ) {

    	ControllerResponse theResp = new ControllerResponse();
		theResp.setSuccess( true );

		if ( isValid( inDto, inBinding ) ) {
			MemberModel theModel = memberService.findCustomerModel( inDto.getId() );
			String theOldCard = theModel.getLoyaltyCardNo();
			if ( StringUtils.isNotBlank( inDto.getLoyaltyCardNo() ) 
					&& !StringUtils.equalsIgnoreCase( inDto.getLoyaltyCardNo(), theOldCard ) ) {
				
				LoyaltyCardInventory oldCard = new LoyaltyCardInventory();
				
				if ( StringUtils.isNotBlank( theOldCard ) ) {
					oldCard = loyaltyCardService.replaceLoyaltyCard( theOldCard );
					loyaltyCardHistoryService.saveHistoryForInventory(oldCard, oldCard.getStatus(), oldCard.getLocation());
				}

				LoyaltyCardInventory theCard = loyaltyCardService.activateLoyaltyCard( inDto.getLoyaltyCardNo(), oldCard.getExpiryDate(), inDto.getId() );
				loyaltyCardHistoryService.saveHistoryForInventory(theCard, theCard.getStatus(), theCard.getLocation(), inDto.getTransactionNo(), inDto.getAnnualFee(), inDto.getReplacementFee(), true);
				
				String accountId = inDto.getLoyaltyCardNo();
				if(StringUtils.isBlank(theModel.getParentAccountId())) {
//					LOGGER.error(">>> PARENT MEMBER: " + theModel.getAccountId() );
					List<MemberDto> supplements = memberService.findSupplementaryMembersByParentId(theModel.getAccountId());
					for(MemberDto supplement : supplements) {
//						LOGGER.error(">>> UPDATING SUPPLEMENT: " + supplement.getAccountId());
						supplement.setParent(accountId);
						memberService.updateCustomerModel(supplement);
					}
//					LOGGER.error(">>> NEW ACCOUNT ID: " + accountId);
				}
				
				theModel.setAccountId(accountId);
				theModel.setLoyaltyCardNo( inDto.getLoyaltyCardNo() );
				/*if ( theModel.getMemberType().getCode().equalsIgnoreCase( codePropertiesService.getDetailMemberTypeIndividual() ) ) { removed: should also set card type on professional accounts
					theModel.setCardType( theCard.getProduct().getName() );
				}*/
				theModel.setCardType( theCard.getProduct().getName() );
				memberService.saveMember( theModel );

                if (BooleanUtils.isTrue(theModel.getStampUser())) {
                    StampAccountPortaRestDto dto = new StampAccountPortaRestDto(oldCard.getBarcode(), theCard.getBarcode(),
                        new Date(), "");
                    stampPromoService.callPortaStampAccount(dto);
                }
				
			}
		}
    	else {
    		theResp.setSuccess( false );
    		theResp.setResult( inBinding.getAllErrors() );
    	}

		return theResp;
	}

    @RequestMapping( value="/scan/{loyaltyCardNo}", produces="application/json; charset=utf-8", method=RequestMethod.POST )
	public @ResponseBody ControllerResponse scanCard(
    		@PathVariable(value="loyaltyCardNo") String inLoyaltyCardNo ) {

    	ControllerResponse theResp = new ControllerResponse();
		theResp.setSuccess( true );

		LoyaltyCardInventory theCard = loyaltyCardService.findLoyaltyCard( inLoyaltyCardNo );
		if ( null != theCard ) {
			Map<String, Object> returnMap = new HashMap<String, Object>();
			returnMap.put("barCode", theCard.getBarcode());
			LoyaltyAnnualFeeSchemeDto scheme = loyaltyAnnualFeeService.getScheme(theCard.getBarcode());
			returnMap.put("annualFee", scheme.getAnnualFee());
			returnMap.put("replacementFee", scheme.getReplacementFee());
			returnMap.put("company", scheme.getCompany().getDescription());
			returnMap.put("cardType", scheme.getCardType().getDescription());
			returnMap.put("expiryDate", new LocalDate().plusMonths(loyaltyCardService.getExpiryInMonths() + 1).withDayOfMonth(1).toString("dd-MM-yyyy"));
			theResp.setResult(returnMap);
		}
		else {
			List<String> theErrors = new ArrayList<String>();
			theErrors.add( messageSource.getMessage( "loyalty_msg_nullcard", null, LocaleContextHolder.getLocale() ) );
			theResp.setSuccess( false );
			theResp.setResult( theErrors );
		}

		return theResp;
	}

    private boolean isValid( MemberDto inDto, BindingResult inBinding ) {

    	LoyaltyCardInventory theCard = loyaltyCardService.findLoyaltyCard( inDto.getLoyaltyCardNo() );
    	if ( null == theCard ) {
    		inBinding.reject( messageSource.getMessage( "loyalty_msg_nullcard", null, LocaleContextHolder.getLocale() ) );
    	}
    	else {
        	/*if ( null != theCard.getStatus() && theCard.getStatus().getCode().equalsIgnoreCase( codePropertiesService.getDetailLoyaltyStatusActive() ) ) {
        		inBinding.reject( messageSource.getMessage( "loyalty_msg_activecard", null, LocaleContextHolder.getLocale() ) );
        	}
        	if ( null != theCard.getStatus() && theCard.getStatus().getCode().equalsIgnoreCase( codePropertiesService.getDetailLoyaltyStatusDisabled() ) ) {
        		inBinding.reject( messageSource.getMessage( "loyalty_msg_disabledcard", null, LocaleContextHolder.getLocale() ) );
        	}
        	
        	if(null != theCard.getStatus() && theCard.getStatus().getCode().equalsIgnoreCase(codePropertiesService.getDetailLoyaltyStatusMissing())) {
        		inBinding.reject(messageSource.getMessage("loyalty_msg_missingcard", null, LocaleContextHolder.getLocale()));
        	}
        	
        	if(null != theCard.getStatus() && theCard.getStatus().getCode().equalsIgnoreCase(codePropertiesService.getDetailLoyaltyStatusBurned())) {
        		inBinding.reject(messageSource.getMessage("loyalty_msg_burnedcard", null, LocaleContextHolder.getLocale()));
        	}*/
        	
        	String[] validStatus = new String[] {
        			codePropertiesService.getDetailLoyaltyStatusInactive(),
        			codePropertiesService.getDetailLoyaltyStatusFound(),
        			codePropertiesService.getDetailLoyaltyStatusTransferIn()
        	};
        	
        	if(!Lists.newArrayList(validStatus).contains(StringUtils.defaultString(theCard.getStatus().getCode())) || StringUtils.isNotBlank(theCard.getNewTransferTo())) {
        		inBinding.reject(messageSource.getMessage("loyalty_msg_invalidcard", null, LocaleContextHolder.getLocale()));
        	}
        	
        	String invLocation = getCurrentUserLocation();
        	if(invLocation == null) {
        		inBinding.reject( messageSource.getMessage( "loyalty_msg_invalidcard", null, LocaleContextHolder.getLocale() ) );
        	} else if(!invLocation.equals(theCard.getLocation())) {
        		inBinding.reject( messageSource.getMessage( "loyalty_msg_mismatchlocation", new Object[]{invLocation}, LocaleContextHolder.getLocale() ) );
        	}
    	}
    	
    	if(BooleanUtils.isNotTrue(inDto.getIsFree()) && StringUtils.isBlank(inDto.getTransactionNo())) {
    		String txnLbl = messageSource.getMessage("loyalty_prop_transactionno", null, LocaleContextHolder.getLocale());
    		inBinding.reject( messageSource.getMessage( "propkey_msg_notempty", new Object[]{txnLbl}, LocaleContextHolder.getLocale() ) );
    	}

    	return !inBinding.hasErrors();
    }
    
    private String getCurrentUserLocation() {
		CustomSecurityUserDetailsImpl userDetails = UserUtil.getCurrentUser();
		return userDetails.getInventoryLocation();
    }

}
