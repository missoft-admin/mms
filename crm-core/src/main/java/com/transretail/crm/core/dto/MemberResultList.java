package com.transretail.crm.core.dto;

import java.util.Collection;

import com.transretail.crm.common.service.dto.response.AbstractResultListDTO;

public class MemberResultList extends AbstractResultListDTO<MemberDto> {
	
	public MemberResultList(Collection<MemberDto> results, long totalElements, boolean hasPreviousPage, boolean hasNextPage) {
        super(results, totalElements, hasPreviousPage, hasNextPage);
    }
    
	public MemberResultList(Collection<MemberDto> results, long totalElements, int pageNo, int pageSize) {
		super(results, totalElements, pageNo, pageSize);
	}

}
