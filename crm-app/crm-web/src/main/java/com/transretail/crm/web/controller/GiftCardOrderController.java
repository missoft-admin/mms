package com.transretail.crm.web.controller;

import com.transretail.crm.common.reporting.jasper.JRProcessor;
import com.transretail.crm.core.security.util.UserUtil;
import com.transretail.crm.core.service.NameIdPairService;
import com.transretail.crm.giftcard.dto.*;
import com.transretail.crm.giftcard.entity.GiftCardInventoryStock.GCIStockStatus;
import com.transretail.crm.giftcard.entity.QCardVendor;
import com.transretail.crm.giftcard.entity.support.GiftCardOrderStatus;
import com.transretail.crm.giftcard.entity.support.ProductProfileStatus;
import com.transretail.crm.giftcard.service.GiftCardInventoryService;
import com.transretail.crm.giftcard.service.GiftCardInventoryStockService;
import com.transretail.crm.giftcard.service.GiftCardOrderService;
import com.transretail.crm.giftcard.service.ProductProfileService;
import com.transretail.crm.web.controller.util.ControllerResponse;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.joda.time.LocalDate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.core.io.FileSystemResource;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

/**
 *
 */
@Controller
@RequestMapping("/giftcard/order")
public class GiftCardOrderController extends AbstractReportsController {

    /* ---- field ---- */
    private MessageSource messageSource;
    private NameIdPairService nameIdPairService;
    private GiftCardOrderService giftCardOrderService;
    private ProductProfileService productProfileService;
    private GiftCardInventoryService inventoryService;
    private GiftCardInventoryStockService inventoryStockService;

    /* ------ IoC for field ------- */
    @Autowired
    public void setMessageSource(MessageSource messageSource) {
        this.messageSource = messageSource;
    }
    @Autowired
    public void setNameIdPairService(NameIdPairService nameIdPairService) {
        this.nameIdPairService = nameIdPairService;
    }
    @Autowired
    public void setGiftCardOrderService(GiftCardOrderService giftCardOrderService) {
        this.giftCardOrderService = giftCardOrderService;
    }
    @Autowired
    public void setProductProfileService(ProductProfileService productProfileService) {
        this.productProfileService = productProfileService;
    }
    @Autowired
    public void setInventoryService(GiftCardInventoryService inventoryService) {
        this.inventoryService = inventoryService;
    }
    @Autowired
    public void setInventoryStockService(GiftCardInventoryStockService inventoryStockService) {
        this.inventoryStockService = inventoryStockService;
    }



    /* ----- controller ----- */
    @RequestMapping(value = "/printenc/{id}", method = RequestMethod.GET)
	@ResponseBody
	public FileSystemResource getFile(@PathVariable("id") Long id, HttpServletResponse response) {
		File enc = giftCardOrderService.printEncryptedFile(id);
		String filename = messageSource.getMessage("gc_order", null, LocaleContextHolder.getLocale()) + "#" + id + ".txt";
		response.setHeader("Content-Disposition", "attachment;filename=\"" + filename + "\"");
	    return new FileSystemResource(enc); 
	}
    
    @RequestMapping(produces = "text/html")
    public String show(Model uiModel) {
    	populateSearch(uiModel);
        return "giftcard/manufactureorder/list";
    }
    
    @RequestMapping(value = "/search", method = RequestMethod.POST)
    @ResponseBody
    public GiftCardOrderResultList listGiftCardOrders(@RequestBody GiftCardOrderSearchDto searchForm) {
        return giftCardOrderService.getGiftCardOrders(searchForm);
    }

    @RequestMapping(value = "/form", method = RequestMethod.GET, produces = "text/html")
    public String showForm(Model uiModel) {
    	populateModel(new GiftCardOrderDto(), uiModel);
		
		return "giftcard/orderform";
    }
	
	@RequestMapping(value = "/form/{id}", method = RequestMethod.GET, produces = "text/html")
    public String showForm(
    		@PathVariable("id") String id,
    		Model uiModel) {
		GiftCardOrderDto orderDto = giftCardOrderService.getGiftCardOrder(Long.valueOf(id));
		populateModel(orderDto, uiModel);
		
		if(orderDto.getStatus().compareTo(GiftCardOrderStatus.FOR_APPROVAL) == 0 || orderDto.getStatus().compareTo(GiftCardOrderStatus.APPROVED) == 0)
			return "giftcard/orderapprovalform";
		
		return "giftcard/orderform";
    }
	
	@RequestMapping(value = "/receiveform/{id}", method = RequestMethod.GET, produces = "text/html")
	public String showReceiveForm(@PathVariable("id") String id, Model uiModel) {
		Long giftCardOrderId = Long.valueOf(id);
		GiftCardOrderDto orderDto = giftCardOrderService.getGiftCardOrder(giftCardOrderId);
		uiModel.addAttribute("orderForm", orderDto);
		GiftCardOrderReceiveDto receiveDto = new GiftCardOrderReceiveDto();
		receiveDto.setSeries(giftCardOrderService.getGiftCardSeries(giftCardOrderId));
		receiveDto.setReceivedSeries(giftCardOrderService.getGiftCardReceivedSeries(giftCardOrderId));
		uiModel.addAttribute("receiveForm", receiveDto);
		uiModel.addAttribute("stores", giftCardOrderService.getInventoryLocations());
		return "giftcard/receiveinventoriesform";
	}

    @RequestMapping(value = "/pendingMOReceiptReport")
    public void generatePendingMOReceiptReport(HttpServletResponse servletResponse) throws Exception {
        JRProcessor jrProcessor = giftCardOrderService.createPendingMOReceiptReport();
        renderExcelReport(servletResponse, jrProcessor, "Gift_Card_Pending_MO_Receipt_Report");
    }

	private void populateSearch(Model uiModel) {
		uiModel.addAttribute("orderSearchCriteria", OrderSearchCriteria.values());
		uiModel.addAttribute("orderStatus", GiftCardOrderStatus.values());
    	QCardVendor qCardVendor = QCardVendor.cardVendor;
    	uiModel.addAttribute("vendors", nameIdPairService.getNameIdPairs(null, qCardVendor, qCardVendor.id, qCardVendor.formalName, qCardVendor.defaultVendor.desc()));
	}

	/*
	* this private method for populated model in order gift card
	*/
	private void populateModel(GiftCardOrderDto order, Model uiModel) {

		ProductProfileSearchDto dto = new ProductProfileSearchDto();
        QCardVendor qCardVendor = QCardVendor.cardVendor;
		dto.setStatusCode( ProductProfileStatus.APPROVED.name() );
		dto.setIsEgc( false );

		/* model for form order gift */
        uiModel.addAttribute("orderForm", order);
		uiModel.addAttribute("vendors", nameIdPairService.getNameIdPairs(null, qCardVendor, qCardVendor.id, qCardVendor.formalName, qCardVendor.defaultVendor.desc()));
        uiModel.addAttribute( "profiles", productProfileService.findDtos(dto));
	}
	
    @RequestMapping(value = "/printreceive")
    public void printReceive(
	    @ModelAttribute(value = "orderForm") GiftCardOrderReceiveDto receiveDto,
	    HttpServletResponse response) throws Exception {
	GiftCardOrderReceiveDto giftCardReceipt = giftCardOrderService.getGiftCardReceipt(
		receiveDto.getId(), receiveDto.getDeliveryReceipt(), receiveDto.getReceivedDate(), UserUtil.getCurrentUser().getUsername());
	JRProcessor jrProcessor = giftCardOrderService.createJrProcessor(giftCardReceipt);
	renderReport(response, jrProcessor);
    }
	
	@RequestMapping(value = "/save/{status}", method = RequestMethod.POST, produces = "application/json; charset=utf-8")
	@ResponseBody
	public ControllerResponse ajaxSave(
			@PathVariable("status") String status,
			@ModelAttribute(value = "orderForm") GiftCardOrderDto orderDto,
			BindingResult result, HttpServletRequest request) {

		ControllerResponse theResponse = new ControllerResponse();
		theResponse.setSuccess(true);
		
		validateOrder(orderDto, result);
		
		
		if (result.hasErrors()) {
			theResponse.setSuccess(false);
			theResponse.setResult(result.getAllErrors());
			return theResponse;
		}

		if (theResponse.isSuccess()) {
			orderDto.setStatus(GiftCardOrderStatus.valueOf(status));
			giftCardOrderService.saveGiftCardOrder(orderDto);
		}

		return theResponse;
	}
	
	@RequestMapping(value = "/receiveform", method = RequestMethod.POST, produces = "application/json; charset=utf-8")
	@ResponseBody
	public ControllerResponse ajaxValidate(
			@ModelAttribute(value = "orderForm") GiftCardOrderReceiveDto receiveDto,
			BindingResult result, HttpServletRequest request) {

		ControllerResponse theResponse = new ControllerResponse();
		theResponse.setSuccess(true);
		
		validateReceiveOrder(receiveDto, result);
		
		if (result.hasErrors()) {
			theResponse.setSuccess(false);
			theResponse.setResult(result.getAllErrors());
			return theResponse;
		}
		if (theResponse.isSuccess()) {
			giftCardOrderService.receiveGiftCardInventories(receiveDto);
			giftCardOrderService.updateGiftCardStatus(receiveDto.getId());
			//inventoryStockService.saveStocks( giftCardOrderService.getInventories( receiveDto.getItems() ), GCIStockStatus.RECEIVED );
			inventoryStockService.saveStocksFromOrder( receiveDto.getItems(), GCIStockStatus.RECEIVED, receiveDto.getReceivedAt() );
		}

		return theResponse;
	}
	
	/*@RequestMapping(value = "/receiveform/stocks", method = RequestMethod.POST, produces = "application/json; charset=utf-8")
	@ResponseBody
	public ControllerResponse ajaxSaveStocks(
			@ModelAttribute(value = "orderForm") GiftCardOrderReceiveDto receiveDto,
			BindingResult result, HttpServletRequest request) {

		ControllerResponse theResponse = new ControllerResponse();
		theResponse.setSuccess(true);
		
		if (theResponse.isSuccess()) {
			inventoryStockService.saveStocks( giftCardOrderService.getInventories( receiveDto.getItems() ), GCIStockStatus.RECEIVED );
		}
		return theResponse;
	}*/
	
	private void validateReceiveOrder(GiftCardOrderReceiveDto receiveDto, BindingResult result) {
		
		if(StringUtils.isBlank(receiveDto.getReceivedAt())) {
			result.reject(getMessage("propkey_msg_notempty", new Object[]{"order_received_at"}));	
		}
		if(receiveDto.getReceivedDate() == null) {
			result.reject(getMessage("propkey_msg_notempty", new Object[]{"order_received_date"}));
		}
		
		if(StringUtils.isBlank(receiveDto.getDeliveryReceipt())) {
			result.reject(getMessage("propkey_msg_notempty", new Object[]{"order_delivery_receipt"}));
		}
		
		for(GiftCardOrderReceiveItemDto itemDto: receiveDto.getItems()) {
			if(!giftCardOrderService.isValidReceiveOrder(itemDto, receiveDto.getId())) {
				result.reject(getMessage("order_invalid_series", null));
				break;
			}
		}
	}
	
	private void validateOrder(GiftCardOrderDto orderDto, BindingResult result) {
		if(orderDto.getMoDate() == null) {
			result.reject(getMessage("propkey_msg_notempty", new Object[]{"order_mo_date"}));
		}
		if(StringUtils.isBlank(orderDto.getPoNumber())) {
			result.reject(getMessage("propkey_msg_notempty", new Object[]{"order_po_number"}));
		}
		if(orderDto.getPoDate() == null) {
			result.reject(getMessage("propkey_msg_notempty", new Object[]{"order_po_date"}));
		}
		Long totalQty = checkTotalQuantity(orderDto.getItems());
		Long maxQty = new Long(100000);
		if(totalQty == null) {
			result.reject(getMessage("propkey_msg_notempty", new Object[]{"order_item_quantity"}));
		} /*else if(totalQty.compareTo(maxQty) > 0){
			result.reject(messageSource.getMessage("order_err_max_quantity", new Object[]{maxQty}, LocaleContextHolder.getLocale()));
		}*/
		if(checkForDuplicateProfile(orderDto)) {
			result.reject(getMessage("order_duplicate_profile", null));
		}
		if ( CollectionUtils.isNotEmpty( orderDto.getItems() ) ) {
			boolean isQtyValid = true;
			for ( GiftCardOrderItemDto item : orderDto.getItems() ) {
				if ( null != item.getQuantity() && item.getQuantity().compareTo( maxQty ) > 0 ) {
					isQtyValid = false;
				}
			}
			if ( !isQtyValid ) {
				result.reject( getMessage( "gc.so.err.qty.exceeds.hundredk", null ) );
			}
		}
	}
	
	private boolean checkForDuplicateProfile(GiftCardOrderDto orderDto) {
		List<GiftCardOrderItemDto> items = orderDto.getItems();
		
		if(items.size() > 1) {
			Collections.sort(items, new Comparator<GiftCardOrderItemDto>() {
	            @Override
	            public int compare(GiftCardOrderItemDto o1, GiftCardOrderItemDto o2) {
	                return o1.getProfileId().compareTo(o2.getProfileId());
	            }
	        });
	        for(int i = 1; i < items.size(); i++) {
	        	if(items.get(i-1).getProfileId().compareTo(items.get(i).getProfileId()) == 0)
	        		return true;
	        }	
		}
        
		return false;
	}
	
	private Long checkTotalQuantity(List<GiftCardOrderItemDto> itemDtos) {
		long total = 0;
		for(GiftCardOrderItemDto item: itemDtos) {
			if(item.getQuantity() == null) {
				return null;
			} else {
				total += item.getQuantity(); 
			}
		}
		return total;
	}
	
	private String getMessage(String inMsgCode, Object[] inArgMsgCodes) {
        if (inArgMsgCodes != null) {
            for (int i = 0; i < inArgMsgCodes.length; i++) {
                inArgMsgCodes[i] =
                    messageSource.getMessage(inArgMsgCodes[i].toString().toLowerCase(), null, LocaleContextHolder.getLocale());
            }
        }
        return messageSource.getMessage(inMsgCode, inArgMsgCodes, "Info given is invalid.", LocaleContextHolder.getLocale());
    }
	
	@RequestMapping(value = "/generate/{id}", method = RequestMethod.POST, produces = "application/json; charset=utf-8")
	@ResponseBody
	public ControllerResponse ajaxGenerate(@PathVariable("id") String id) {
		ControllerResponse theResponse = new ControllerResponse();
		theResponse.setSuccess(true);
		if (theResponse.isSuccess()) {
			GiftCardOrderDto order = giftCardOrderService.getGiftCardOrder(Long.valueOf(id));
			if(order.getStatus().compareTo(GiftCardOrderStatus.APPROVED) == 0 && !giftCardOrderService.isExistBarcodingOrder())
				inventoryService.generateGiftCardInventory(order.getItems(), order.getMoDate(), order.getId());
		}
		return theResponse;
	}
	
	@RequestMapping(value = "/validategeneration", method = RequestMethod.POST, produces = "application/json; charset=utf-8")
	@ResponseBody
	public ControllerResponse ajaxValidateGeneration() {
		ControllerResponse theResponse = new ControllerResponse();
		theResponse.setSuccess(giftCardOrderService.isExistBarcodingOrder());
		return theResponse;
	}
	
	@RequestMapping(value = "/approve/{status}", method = RequestMethod.POST, produces = "application/json; charset=utf-8")
	@ResponseBody
	public ControllerResponse ajaxApprove(
			@PathVariable("status") String status,
			@ModelAttribute(value = "orderForm") GiftCardOrderDto orderDto,
			BindingResult result, HttpServletRequest request) {

		ControllerResponse theResponse = new ControllerResponse();
		theResponse.setSuccess(true);
		
		if (result.hasErrors()) {
			theResponse.setSuccess(false);
			theResponse.setResult(result.getAllErrors());
			return theResponse;
		}

		if (theResponse.isSuccess()) {
			giftCardOrderService.approveGiftCardOrder(orderDto.getId(), GiftCardOrderStatus.valueOf(status));
		}

		return theResponse;
	}
	
	@RequestMapping(value = "/{id}", method = RequestMethod.DELETE, produces = "application/json; charset=utf-8")
	@ResponseBody
    public ControllerResponse delete(@PathVariable("id") Long id,
            Model uiModel) {
		ControllerResponse theResponse = new ControllerResponse();
		theResponse.setSuccess(true);
		giftCardOrderService.deleteGiftCardOrder(id);
        return theResponse;
    }
	
	@RequestMapping(value="/reprintreceipt/{id}")
	public String showReprintReceiptForm(@PathVariable("id") String id, Model uiModel) {
		Long giftCardOrderId = Long.parseLong(id);
		List<GiftCardOrderReceivedSeriesDto> gcOrderReceivedList = 
				giftCardOrderService.getGiftCardReceipt(giftCardOrderId);
		uiModel.addAttribute("gcOrderReceived", gcOrderReceivedList);
		uiModel.addAttribute("id", id);
		return "giftcard/reprintreceipt";
	}
	
	@RequestMapping(value = "/reprintreceipt/{id}/{deliveryReceipt}/{receiveDate}/{receivedBy}")
	public void reprintReceipt(
			@PathVariable("id") String id, @PathVariable("deliveryReceipt") String deliveryReceipt,
			@PathVariable("receiveDate") LocalDate receiveDate, @PathVariable("receivedBy") String receivedBy,
			HttpServletResponse response) throws Exception {

		JRProcessor jrProcessor = giftCardOrderService.createJrProcessor(giftCardOrderService.getGiftCardReceipt(Long.parseLong(id), deliveryReceipt, receiveDate, receivedBy));
		renderReport(response, jrProcessor);
	}
	
    @RequestMapping("/reprint/transaction/{id}")
    public void reprintTransaction(@PathVariable String id, HttpServletResponse servletResponse) throws Exception {

		JRProcessor jrProcessor = giftCardOrderService.createReprintTransactionJrProcessor(id);
		renderReport(servletResponse, jrProcessor);
    }
    
	public static enum OrderSearchCriteria {
		MO("moNo"),
		PO("poNo"),
		MPO("mpoNo");
		
		private final String field;
		
		private OrderSearchCriteria(String field) {
			this.field = field;
		}

		public String getField() {
			return field;
		}

	}
	
	public static enum ProfileSearchCriteria {
		PROD_CODE("prodCode"),
		PROD_DESC("prodDesc"),
		FACE_VAL("faceVal"),
		CARD_FEE("cardFee");
		
		private final String field;
		
		private ProfileSearchCriteria(String field) {
			this.field = field;
		}

		public String getField() {
			return field;
		}

	}
	
}
