package com.transretail.crm.web.controller;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletResponse;

import org.joda.time.DateTime;
import org.joda.time.LocalDate;
import org.joda.time.format.DateTimeFormat;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.google.common.collect.Lists;
import com.transretail.crm.common.reporting.jasper.JRProcessor;
import com.transretail.crm.core.dto.MemberSearchDto;
import com.transretail.crm.core.entity.enums.MemberCreatedFromType;
import com.transretail.crm.core.entity.enums.ReportType;
import com.transretail.crm.core.entity.lookup.LookupDetail;
import com.transretail.crm.core.service.CodePropertiesService;
import com.transretail.crm.core.service.LookupService;
import com.transretail.crm.core.service.StoreService;
import com.transretail.crm.giftcard.entity.support.CustomerProfileType;
import com.transretail.crm.report.template.impl.MemberRegistrationByCustomerReport;
import com.transretail.crm.web.controller.util.ControllerResponse;

/**
 * @author ftopico
 */
@RequestMapping("/member/registration")
@Controller
public class MemberRegistrationController extends AbstractReportsController {

    private static final String PATH_MEMBER_REGISTRATION = "/member/registration";
    
    @Autowired
    private MemberRegistrationByCustomerReport memberRegistrationReport;
    @Autowired
    private StoreService storeService;
    @Autowired
    private LookupService lookupService;
    @Autowired
    private CodePropertiesService codePropertiesService;
    @Autowired
    private MessageSource messageSource;
    
    @RequestMapping(produces="text/html")
    public String showBurnCardForm(Model model) {
        model.addAttribute("reportTypes", ReportType.values());
        model.addAttribute("registerTypes", MemberCreatedFromType.values());
        model.addAttribute("stores", storeService.getAllStores());
        HashMap<String, String> customers = new LinkedHashMap<String, String>();
        for (LookupDetail detail : lookupService.getActiveDetailsByHeaderCode(codePropertiesService.getHeaderMemberType())) {
            if (!detail.getCode().equalsIgnoreCase("MTYP002")) {
                customers.put(detail.getCode(), detail.getDescription());
            }
        }
        model.addAttribute("customers", customers);
        return PATH_MEMBER_REGISTRATION;
    }
    
    @RequestMapping( value="/print/validate", produces="application/json; charset=utf-8" )
    public @ResponseBody ControllerResponse printValidate(
            @RequestParam String dateFrom,
            @RequestParam String dateTo,
            Model model) {

        ControllerResponse resp = new ControllerResponse();
        DateTime dateFromLocalDate = null;
        if (!dateFrom.equalsIgnoreCase("")) {
            dateFromLocalDate = DateTimeFormat.forPattern("dd-MM-yyyy").parseDateTime(dateFrom);
        }
        DateTime dateToLocalDate = null;
        if (!dateTo.equalsIgnoreCase("")) {
            dateToLocalDate = DateTimeFormat.forPattern("dd-MM-yyyy").parseDateTime(dateTo);
        }
        if (dateFromLocalDate == null || dateToLocalDate == null) {
            resp.setSuccess(false);
            resp.setResult(Lists.newArrayList(messageSource.getMessage("member.registration.error.date.is.empty", null, LocaleContextHolder.getLocale())));
        } else if (dateFromLocalDate.isAfter(dateToLocalDate)) {
            resp.setSuccess(false);
            resp.setResult(Lists.newArrayList(messageSource.getMessage("report.date.from.before.date.to", null, LocaleContextHolder.getLocale())));
        } else if (dateFromLocalDate.plusMonths(12).isBefore(dateToLocalDate)) {
            resp.setSuccess(false);
            resp.setResult(Lists.newArrayList(messageSource.getMessage("report.date.limit", null, LocaleContextHolder.getLocale())));
        } else {
            resp.setSuccess(true);
        }
        return resp;
    }
    
    @RequestMapping(value="/print", method=RequestMethod.GET)
    public void print(
            @RequestParam String reportType,
            @RequestParam String memberReportType,
            @RequestParam String dateFrom,
            @RequestParam String dateTo,
            @RequestParam(required=false) String customerType,
            @RequestParam(required=false) String store,
            HttpServletResponse response) throws Exception {
        
        Map<String, String> filtersMap = new HashMap<String, String>();
        filtersMap.put("memberReportType", memberReportType);
        filtersMap.put("dateFrom", dateFrom);
        filtersMap.put("dateTo", dateTo);
        filtersMap.put("customerType", customerType);
        filtersMap.put("store", store);
        
        JRProcessor jrProcessor = memberRegistrationReport.createJrProcessor(filtersMap);
        
        if (reportType.equalsIgnoreCase(ReportType.PDF.name())) {
            renderReport(response, jrProcessor);
        } else if (reportType.equalsIgnoreCase(ReportType.EXCEL.name())) {
            String filename = null;
            
            if (memberReportType.equalsIgnoreCase("store")) {
                filename = "Member Registration Report by Store (" + LocalDate.now() +")";
            } else if (memberReportType.equalsIgnoreCase("type")) {
                filename = "Member Registration Report by Registration Type (" + LocalDate.now() +")";
            } else if (memberReportType.equalsIgnoreCase("summary")) {
                filename = "Member Registration Report Summary (" + LocalDate.now() +")";
            }
            renderExcelReport(response, jrProcessor, filename);
        }
        
    }
}
