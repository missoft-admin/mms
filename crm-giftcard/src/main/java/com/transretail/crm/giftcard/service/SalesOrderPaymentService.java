package com.transretail.crm.giftcard.service;

import java.math.BigDecimal;
import java.util.List;

import org.joda.time.LocalDate;

import com.transretail.crm.common.reporting.jasper.JRProcessor;
import com.transretail.crm.common.service.dto.response.ResultList;
import com.transretail.crm.giftcard.dto.SalesOrderPaymentInfoDto;
import com.transretail.crm.giftcard.dto.SalesOrderPaymentSearchDto;
import com.transretail.crm.giftcard.dto.SalesOrderPaymentsDto;
import com.transretail.crm.giftcard.entity.support.PaymentInfoStatus;





public interface SalesOrderPaymentService {
	
	public static PaymentInfoStatus[] UI_STATUS = new PaymentInfoStatus[] {
		PaymentInfoStatus.FIRST_APPROVAL, 
		PaymentInfoStatus.SECOND_APPROVAL, 
		PaymentInfoStatus.VERIFIED, 
		PaymentInfoStatus.APPROVED 
		};
			
			

	SalesOrderPaymentsDto getPaymentsDto(Long orderId);

	void savePayments(SalesOrderPaymentsDto paymentsDto);

	long updateStatus(String orderNo, PaymentInfoStatus status);

	void processPayment(Long id, PaymentInfoStatus status);

	long updateStatus(Long orderId, PaymentInfoStatus status);

	SalesOrderPaymentInfoDto getPaymentsInfoDto(Long id);

	void createEmptyPayment(Long orderId, LocalDate orderDate);

	long updateStatus(Long[] ids, PaymentInfoStatus status);

	long verifyPayments(Long[] ids);

	void approvePayments(Long[] ids);

	ResultList<SalesOrderPaymentInfoDto> getPayments(
			SalesOrderPaymentSearchDto sortDto);

	void generateAcctForPeoplesoft(Long[] ids);

	void processPaymentsForScndApproval(Long orderId);

	BigDecimal getTotalPayments(Long orderId);

    JRProcessor createJrProcessor(SalesOrderPaymentSearchDto searchDto);

    List<SalesOrderPaymentInfoDto> getSalesOrdersPaymentForPrint(
            SalesOrderPaymentSearchDto searchDto);
	
	
	
	
}
