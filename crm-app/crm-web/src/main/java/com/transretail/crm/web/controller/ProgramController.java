package com.transretail.crm.web.controller;


import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.transretail.crm.core.dto.ProgramDto;
import com.transretail.crm.core.service.PromotionService;
import com.transretail.crm.web.controller.util.ControllerResponse;


@RequestMapping("/promotion/program")
@Controller
public class ProgramController {

	@Autowired
    PromotionService promotionService;

    @Autowired
    private MessageSource messageSource;



	private static final String PATH_LIST 	    = "/promotion/program/list";
	private static final String PATH_CREATE     = "/promotion/program/create";

	private static final String UIATTR_PROGRAMS = "programs";

	@InitBinder
	void initBinder(WebDataBinder binder) {
		DateFormat theFormat = new SimpleDateFormat( "dd MMM yyyy" );
		theFormat.setLenient( false );
		binder.registerCustomEditor( Date.class, new CustomDateEditor( theFormat, true ) );
	}




	/*@RequestMapping(value = "/listUser", method = RequestMethod.POST)
    @ResponseBody
    public UserResultList listUsers(@RequestBody UserSearchDto searchForm) {
    	return null;//promotionService.( searchForm );
    }*/

	@RequestMapping( value="/list", produces="text/html" )
	public String listPrograms( Model inUiModel ) {

		inUiModel.addAttribute( UIATTR_PROGRAMS, promotionService.getProgramDtos() );
		return PATH_LIST;
	}

    @RequestMapping( value="/create", produces="text/html" )
    public String createProgram(
    		@ModelAttribute(value="program") ProgramDto inProgram,
            Model inUiModel ) {

        return PATH_CREATE;
    }

    @RequestMapping( value="/save", produces="application/json; charset=utf-8", method=RequestMethod.POST )
	public @ResponseBody ControllerResponse saveProgram( 
			@ModelAttribute(value="program") ProgramDto inProgram,
    		BindingResult inBinding ) {

		ControllerResponse theResp = new ControllerResponse();

    	if ( isValid( inProgram, inBinding ) ) {
    		promotionService.saveProgramDto( inProgram );
    		theResp.setSuccess( true );
    		theResp.setResult( inProgram );
    	}
    	else {
    		theResp.setSuccess( false );
    		theResp.setResult( inBinding.getAllErrors() );
    	}

		return theResp;
	}



	private boolean isValid( ProgramDto inProgram,
    		BindingResult inBinding ) {

		if ( StringUtils.isBlank( inProgram.getName() ) ) {
    		inBinding.reject( messageSource.getMessage( "promotion_msg_nameempty", null, LocaleContextHolder.getLocale() ) );
    	}

		if ( StringUtils.isBlank( inProgram.getDescription() ) ) {
    		inBinding.reject( messageSource.getMessage( "promotion_msg_descempty", null, LocaleContextHolder.getLocale() ) );
    	}

    	if ( null == inProgram.getStartDate() ) {
    		inBinding.reject( messageSource.getMessage( "promotion_msg_startdateempty", null, LocaleContextHolder.getLocale() ) );
    	}

    	if ( null == inProgram.getEndDate() ) {
    		inBinding.reject( messageSource.getMessage( "promotion_msg_enddateempty", null, LocaleContextHolder.getLocale() ) );
    	}

    	if ( null != inProgram.getStartDate() && null != inProgram.getEndDate() ) {
    		if ( inProgram.getStartDate() > ( inProgram.getEndDate() ) ) {
    			inBinding.reject( messageSource.getMessage( "promotion_msg_daterangeinvalid", null, LocaleContextHolder.getLocale() ) );
    		}
    	}

    	return !inBinding.hasErrors();
	}
}
