<%@ include file="../../common/taglibs.jsp" %>

<div class="modal hide  nofly" id="physicalCountDialogByBarcode" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">

      <div class="modal-body">

	<div id="contentError" class="hide alert alert-error">
	  <button type="button" class="close" onclick="$(this).parent().hide();">&times;</button>
	  <div><form:errors path="*"/></div>
	</div>

	<form:form id="physicalCountFormByBarcode" name="physicalCountFormByBarcode" modelAttribute="physicalCountForm" action="${pageContext.request.contextPath}/loyaltycardinventory/physicalcount" method="POST" cssClass="form-reset form-horizontal" >
	  <div class="control-group">
	    <spring:message code="inventory_prop_location" var="locationStr" />
	    <label for="storeName" class="control-label">${locationStr}</label> 
	    <div class="controls">
	      <sec:authentication var="userLocation" property="principal.inventoryLocation" />
	      <sec:authentication var="userLocationName" property="principal.inventoryLocationName" />

	      <input type="text" name="locationDesc" value="${userLocation} - ${userLocationName}" class="form-control" placeholder="${locationStr}" disabled="disabled" />
	      <input type="hidden" name="location" value="${userLocation}" />
	    </div>
	  </div>
	  <div class="control-group">
	    <spring:message code="mo_tier_prop_barcode" var="barcodeLabel" />
	    <label for="storeName" class="control-label">${barcodeLabel}</label> 
	    <div class="controls">
	      <input type="text" name="barcode" id="barcode-interface" placeholder="${barcodeLabel}" maxlength="13" class="form-control"/>
	    </div>
	  </div>
	  <div class="control-group">
	    <div class="controls">
	      <form:select id="barcodes_select" path="barcodes" class="form-control" size="10"/>
	    </div>
	  </div>
	</form:form>
      </div>
      <div class="modal-footer">
        <button type="button" id="countSubmit2" class="btn btn-primary"><spring:message code="label_save" /></button>
        <button type="button" id="countCancel2" class="btn btn-default" data-dismiss="modal"><spring:message code="label_stop"/></button>
      </div>
    </div>
  </div>
</div>


