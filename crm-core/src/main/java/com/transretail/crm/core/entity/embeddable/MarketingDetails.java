package com.transretail.crm.core.entity.embeddable;

import javax.persistence.Column;
import javax.persistence.Embeddable;

@Embeddable
public class MarketingDetails {
	
	@Column(name="CORRESPONDENCE_ADDRESS")
	private String correspondenceAddress;
	
	@Column(name="INFORMED_ABOUT")
	private String informedAbout;
	
	@Column(name="INTERESTED_PRODUCTS")
	private String interestedProducts;

	@Column(name="VISITED_SUPERMARKETS")
	private String visitedSupermarkets;

	@Column(name="INTERESTED_ACTIVITIES")
	private String interestedActivities;

	@Column(name="TRIPS_TO_STORE")
	private Integer tripsToStore;

	public String getCorrespondenceAddress() {
		return correspondenceAddress;
	}

	public void setCorrespondenceAddress(String correspondenceAddress) {
		this.correspondenceAddress = correspondenceAddress;
	}

	public String getInformedAbout() {
		return informedAbout;
	}

	public void setInformedAbout(String informedAbout) {
		this.informedAbout = informedAbout;
	}

	public String getInterestedProducts() {
		return interestedProducts;
	}

	public void setInterestedProducts(String interestedProducts) {
		this.interestedProducts = interestedProducts;
	}

	public String getVisitedSupermarkets() {
		return visitedSupermarkets;
	}

	public void setVisitedSupermarkets(String visitedSupermarkets) {
		this.visitedSupermarkets = visitedSupermarkets;
	}

	public String getInterestedActivities() {
		return interestedActivities;
	}

	public void setInterestedActivities(String interestedActivities) {
		this.interestedActivities = interestedActivities;
	}

	public Integer getTripsToStore() {
		return tripsToStore;
	}

	public void setTripsToStore(Integer tripsToStore) {
		this.tripsToStore = tripsToStore;
	}

}
