<%@ include file="../../common/taglibs.jsp" %>
<div id="content_claimstoaffilatesschedule">
  <div class="page-header page-header2"><h1><spring:message code="menutab_gcmgmt_acct_claimstoaffiliates_schedule" /></h1></div>
  <div id="rewardGenSched">
    <div class="hide alert alert-error" id="content_error"><button type="button" class="close" onclick="$(this).parent().hide();">&times;</button><div><form:errors   path="*"/></div></div>
    <div class="well">
      <h3 style="margin-top: 0px">
        <a id="link_fetchdate" href="<c:url value="/appconfig/date/fetch" />" >
	   <spring:message code="gc.acct.claimstoaffilates.fetch.date" />:
        </a> <span id="serverDate"></span>
      </h3>
      <div>
	<h4>
          <spring:message code="gc.acct.claimstoaffilates.label.schedule" arguments=""/>: 
          <span id="scheduleLabel">${schedule}</span>
          <input id="schedule" type="hidden" value="${schedule}" />
        </h4>
      </div>
      <div>
        <h4>
          <span id="scheduleLabel">
            <spring:message code="gc.acct.claimstoaffilates.label.next.shedule" /><![CDATA[&nbsp;]]> ${nextSchedule}. <![CDATA[&nbsp;]]>
          </span>
        </h4>
      </div>
      <div>
        <h4>
          <span id="scheduleLabel">
            <spring:message code="gc.acct.claimstoaffilates.label.generatedfor" /><![CDATA[&nbsp;]]> ${nextScheduleCoverage}.
          </span>
        </h4>
      </div>
      <div>
        <div>
          <span><spring:message code="gc.acct.claimstoaffilates.generation.run" /><![CDATA[&nbsp;]]></span>
          <select id="schedDay" class="input-mini" data-value="${day}" >
            <c:forEach varStatus="varDay" begin="1" end="${days}">
              <option value="${varDay.index}">${varDay.index}</option>
            </c:forEach>
          </select>
          <span>
            <span id="schedDaySuffix"></span>
            <![CDATA[&nbsp;]]><spring:message code="gc.acct.claimstoaffilates.generation.run.day" /><![CDATA[&nbsp;&#44;]]>
          </span>
          <span><![CDATA[&nbsp;]]><spring:message code="gc.acct.claimstoaffilates.generation.run.at" /><![CDATA[&nbsp;]]></span>
          <input id="schedTime" type="text" class="input-mini" value="${time}" />
          <input id="rescheduleBtn" type="button" class="btn btn-primary rescheduleBtn mb10 ml10" value="<spring:message code="label_reschedule" />" 
		 data-url="<c:url value="reschedule?schedule=%CRONEXP%&day=%DAY%&time=%TIME%" />" />
        </div>
      </div>
    </div>
  </div>
  <link href="<c:url value="/css/bootstrap/bootstrap-timepicker.min.css" />" rel="stylesheet" />
  <script src="<c:url value="/js/bootstrap/bootstrap-timepicker.min.js"/>" type="text/javascript"><![CDATA[&nbsp; ]]></script>
  <script type="text/javascript">
      $(document).ready(function() {

	var SchedUtil = {
	  customizeDay: function(day) {
	    return (((day > 20 || day < 10) ? ([false, "st", "nd", "rd"])[(day % 10)] || "th" : "th"));
	  }
	};

	var errorContainer = $("#content_claimstoaffilatesschedule").find("#content_error");

	$main = $("#content_claimstoaffilatesschedule");
	$btn = $main.find(".rescheduleBtn");
	$input = $main.find("#rewardGenSchedInput");
	$value = $main.find("#schedule");
	$lbl = $main.find("#scheduleLabel");

	$dispTime = $main.find("#schedTime");
	$dispDay = $main.find("#schedDay");
	$dispDaySuffix = $main.find("#schedDaySuffix");

	initLinks();
	initFormFields();

	function initLinks() {
	  $("#link_fetchdate").click(function(e) {
	    e.preventDefault();
	    $.get($(this).attr("href"), function(resp) {
	      $("#serverDate").html(" " + resp.result);
	    }, "json");
	  }).click();
	}

	function initFormFields() {
	  $btn.click(function(e) {
	    var url = null;
	    if ($input.val() != '' && $(this).data("is-cron")) {
	      var cronExp = $input.val().replace("?", "%3F").replace("/", "%2F").replace("\\", "%5F").replace(" ", "%20");
	      url = $(this).data("url").replace(new RegExp("%CRONEXP%", "g"), cronExp)
		      .replace(new RegExp("%DAY%", "g"), "").replace(new RegExp("%TIME%", "g"), "");
	    }
	    else {
	      url = $(this).data("url").replace(new RegExp("%CRONEXP%", "g"), $value.val())
		      .replace(new RegExp("%DAY%", "g"), $dispDay.val())
		      .replace(new RegExp("%TIME%", "g"), $dispTime.val() ? $dispTime.val() : "00:00:00");
	    }

	    if (url) {
	      $.get(
		      url,
		      function(resp) {
			if (resp.success) {
			  errorContainer.hide("slow");
			  $lbl.html(resp.result);
			  $value.val(resp.result);
			  location.reload();
			}
			else {
			  var errors = resp.result;
			  //$( resp.result ).each( function( key, value ) { errors += ( ( key + 1 ) + ". " + value.code + "<br>" ); });
			  errorContainer.find("div").html(errors);
			  errorContainer.show("slow");
			}
		      }
	      );
	    }
	  });

	  $dispTime.timepicker({showMeridian: false, showSeconds: true, defaultTime: "00:00:00", showInputs: false/* , minuteStep : 60 */})
		  .on("changeTime", function(e) {
		    if (!$dispTime.val()) {
		      $dispTime.val("00:00:00");
		    }
		  });

	  $dispDay.change(function(e) {
	    if ($(this).data("value")) {
	      $(this).val($(this).data("value"));
	    }
	    $dispDaySuffix.html(SchedUtil.customizeDay($(this).val()));
	    $(this).data("value", "");
	  }).change();
	}

      });
  </script>
</div>