package com.transretail.crm.common.web.converter;

import java.io.IOException;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;

/**
 *
 */
public class BooleanSerializer extends JsonSerializer<Boolean> {
	
	private final String YES = "yes";
	private final String NO = "no";
	
	@Override
	public void serialize(Boolean bool, JsonGenerator jsonGenerator, SerializerProvider serializerProvider) throws
    IOException {
    if (bool != null && bool) {
        jsonGenerator.writeString(YES);
    } else {
        jsonGenerator.writeString(NO);
    }
}
}
