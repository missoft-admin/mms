package com.transretail.crm.report.template.impl.memberactivity;

import com.transretail.crm.common.reporting.jasper.AbstractJRProcessor;
import com.transretail.crm.common.util.IOUtils;
import com.transretail.crm.core.entity.enums.ReportType;
import net.sf.dynamicreports.jasper.builder.JasperReportBuilder;
import net.sf.dynamicreports.jasper.constant.JasperProperty;
import net.sf.dynamicreports.report.builder.column.ColumnBuilder;
import net.sf.dynamicreports.report.builder.column.TextColumnBuilder;
import net.sf.dynamicreports.report.builder.style.StyleBuilder;
import net.sf.dynamicreports.report.constant.HorizontalAlignment;
import net.sf.dynamicreports.report.constant.VerticalAlignment;
import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRField;
import net.sf.jasperreports.engine.export.JRExportProgressMonitor;
import net.sf.jasperreports.engine.export.JRXlsAbstractExporterParameter;
import net.sf.jasperreports.engine.export.JRXlsExporterParameter;
import org.joda.time.LocalDate;

import javax.sql.DataSource;
import java.awt.*;
import java.sql.*;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import org.apache.commons.lang3.StringUtils;

import static net.sf.dynamicreports.report.builder.DynamicReports.*;

/**
 * @author Allan G. Ramirez (agramirez@exist.com)
 */
public abstract class AbstractMemberActivityReport extends AbstractJRProcessor {
    protected static final StyleBuilder COLUMN_TITLE_STYLE = stl.style()
        .setVerticalAlignment(VerticalAlignment.MIDDLE)
        .setHorizontalAlignment(HorizontalAlignment.LEFT)
        .setBorder(stl.penThin())
        .setPadding(2)
        .bold()
        .setBackgroundColor(new Color(153, 204, 255));
    private static final DateFormat DATE_FORMAT = new SimpleDateFormat("MMMM d, yyyy");
    protected String title;
    protected String subTitle;
    protected LocalDate dateFrom;
    protected LocalDate dateTo;
    protected DataSource dataSource;
    protected ReportType reportType;
    protected String storeCode;

    protected AbstractMemberActivityReport(String reportFileName, String title, String subTitle, DataSource dataSource,
        ReportType reportType) {
        super(reportFileName);
        this.title = title;
        this.subTitle = subTitle;
        this.dataSource = dataSource;
        this.reportType = reportType;
        jRExporterParameters.put(JRXlsExporterParameter.IS_REMOVE_EMPTY_SPACE_BETWEEN_ROWS, Boolean.FALSE);
        jRExporterParameters.put(JRXlsAbstractExporterParameter.IS_COLLAPSE_ROW_SPAN, Boolean.FALSE);
    }

    public void setDateFrom(LocalDate dateFrom) {
        this.dateFrom = dateFrom;
    }

    public void setDateTo(LocalDate dateTo) {
        this.dateTo = dateTo;
    }

    public void setStoreCode(String storeCode) {
        this.storeCode = storeCode;
    }
    
    protected JasperReportBuilder createJRBuilderWithTitleAndSubTitle() {
        return createBaseJRBuilder()
            .setParameters(parameters)
            .title(cmp.horizontalList()
                .add(cmp.text(title).setStyle(stl.style().setFontSize(20).setHorizontalAlignment(HorizontalAlignment.CENTER)))
                .newRow()
                .add(cmp.text(""))
                .newRow()
                .add(cmp.text(subTitle).setStyle(stl.style().setFontSize(14))));
    }

    protected JasperReportBuilder createBaseJRBuilder() {
        JasperReportBuilder defaultReport = createDefaultJRBuilder();
        if (this.reportType.equals(ReportType.EXCEL)) {
           return defaultReport.ignorePagination();
        } else {
            return defaultReport;
        }
    }

    private JasperReportBuilder createDefaultJRBuilder() {
        return report()
                .setColumnTitleStyle(COLUMN_TITLE_STYLE)
                .setColumnStyle(stl.style().setVerticalAlignment(VerticalAlignment.TOP).setLeftPadding(2).setRightPadding(2))
                .addProperty("net.sf.jasperreports.export.xls.exclude.origin.keep.first.band.1", "title")
                .addProperty("net.sf.jasperreports.export.xls.exclude.origin.keep.first.band.2", "pageHeader")
                .addProperty("net.sf.jasperreports.export.xls.exclude.origin.keep.first.band.3", "columnHeader")
                .ignorePageWidth();
    }

    protected <T> ColumnBuilder createColumn(String title, String fieldName, Class<T> propertyClass, Integer width) {
        TextColumnBuilder<T> column = col.column(title, fieldName, propertyClass);
        if (ReportType.EXCEL == reportType) {
            column = column.setStretchWithOverflow(false);
            column = column.addProperty(JasperProperty.PRINT_KEEP_FULL_TEXT, "true");
            if (width != null) {
                column = column.setFixedWidth(width);
            }
        } else {
            column = column.setStretchWithOverflow(true);
            column = column.addProperty(JasperProperty.PRINT_KEEP_FULL_TEXT, "false");
            if (width != null) {
                column = column.setWidth(width);
            }
        }
        return column;
    }

    protected JRDataSource createJrDataSource() {
        return new JRDataSource() {
            Connection con;
            PreparedStatement ps;
            ResultSet rs;
            QueryResultBean bean;
            boolean firstQuery = true;

            @Override
            public boolean next() throws JRException {
                try {
                    if (firstQuery) {
                        StringBuilder builder = new StringBuilder("SELECT b.customer_number,"); // Account ID
                        builder.append("b.customer_name,");// Business Name
                        builder.append("c.company_name,");// Segmentation
                        builder.append("d.description,");// Member Name
                        builder
                            .append(
                                "sum(DECODE(TYPE,'RETURN',-1,'REFUND',-1,1)*a.total_amount - a.total_discount + a.voided_discount - abs(a.rounding_amount)),");// Total Sales
                        builder.append("sum(DECODE(TYPE,'RETURN',-1,'REFUND',-1,1)*a.tariff),");// Total Kena Pajak or Total Sales After VAT
                        builder.append("sum(DECODE(TYPE,'RETURN',-1,'REFUND',-1,1)*a.vat),"); // PPN or VAT
                        builder.append("sum(DECODE(TYPE,'RETURN',-1,'REFUND',-1,1)*a.total_non_member_markup),");// Total Markup
                        builder
                            .append(
                                "sum((DECODE(TYPE,'RETURN',-1,'REFUND',-1,1)*a.total_non_member_markup) - (DECODE(TYPE,'RETURN',-1,'REFUND',-1,1)*a.total_non_member_markup/1.1)),"); // PPN Mark up or VAT from Markup
                        builder.append("count(b.customer_number),"); // Freq Visit
                        builder.append("c.created_datetime,");// Registration Date
                        builder.append("max(a.transaction_date) ");// Last Visit Date
                        builder.append("FROM POS_TRANSACTION a ");
                        builder.append("LEFT JOIN tax_invoice b ON b.pos_txn_id = a.id ");
                        builder.append("left join crm_member c on b.customer_number=c.account_id ");
                        builder.append("left join crm_ref_lookup_dtl d on d.code=c.customer_segregation ");
                        builder.append("WHERE a.type in (?, ?, ?) AND a.status = ? ");
                        builder.append("AND a.CUSTOMER_ID IS NOT NULL ");
                        builder.append("AND b.customer_number IS NOT NULL ");
                        builder.append("AND a.id LIKE ? ");
                        if (dateFrom != null) {
                            builder.append("AND a.sales_date >= ? ");
                        }
                        if (dateTo != null) {
                            builder.append("AND a.sales_date <= ? ");
                        }
                        if(StringUtils.isNotBlank(storeCode)){
                            builder.append("AND a.store_id = (select p.store_id from store p where p.code=?) ");
                        }
                        builder
                            .append(
                                "GROUP BY b.customer_number, b.customer_name, c.company_name, d.description, c.created_datetime ");
                        builder.append("ORDER BY b.customer_number");

                        con = dataSource.getConnection();
                        ps = con.prepareStatement(builder.toString(), ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_READ_ONLY);
                        ps.setString(1, "SALE");
                        ps.setString(2, "REFUND");
                        ps.setString(3, "RETURN");
                        ps.setString(4, "COMPLETED");
                        ps.setString(5, "2000%");
                        if (dateFrom != null && dateTo != null ) {
                            ps.setTimestamp(6, new Timestamp(dateFrom.toDate().getTime()));
                            ps.setTimestamp(7, new Timestamp(dateTo.toDate().getTime()));
                            if(StringUtils.isNotBlank(storeCode))
                                ps.setString(8, storeCode);
                        } else if (dateFrom != null) {
                            ps.setTimestamp(6, new Timestamp(dateFrom.toDate().getTime()));
                        } else if (dateTo != null) {
                            ps.setTimestamp(6, new Timestamp(dateTo.toDate().getTime()));
                        } else if(StringUtils.isNotBlank(storeCode)){
                            ps.setString(6, storeCode);
                        }
                        rs = ps.executeQuery();

                        JRExportProgressMonitor afterPageExportListener = new JRExportProgressMonitor() {
                            @Override
                            public void afterPageExport() {
                                IOUtils.INSTANCE.close(ps);
                                IOUtils.INSTANCE.close(rs);
                                IOUtils.INSTANCE.close(con, true);
                            }
                        };
                        setAfterPageExportListener(afterPageExportListener);
                    }

                    boolean hasNext = rs.next();
                    if (hasNext) {
                        bean = new QueryResultBean();
                        bean.accountId = rs.getString(1);
                        bean.memberName = rs.getString(2);
                        bean.businessName = rs.getString(3);
                        bean.segmentation = rs.getString(4);
                        bean.totalSales = rs.getLong(5);
                        bean.totalSalesAfterVat = rs.getLong(6);
                        bean.ppn = rs.getLong(7);
                        bean.totalMarkup = rs.getLong(8);
                        bean.ppnMarkup = rs.getLong(9);
                        bean.freqVisit = rs.getLong(10);
                        Date date = rs.getDate(11);
                        if (date != null) {
                            bean.registrationDate = DATE_FORMAT.format(date);
                        }
                        date = rs.getDate(12);
                        if (date != null) {
                            bean.lastVisitDate = DATE_FORMAT.format(date);
                        }
                    } else if (firstQuery) {
                        hasNext = true;
                        bean = new QueryResultBean();
                    }
                    firstQuery = false;
                    return hasNext;
                } catch (SQLException e) {
                    IOUtils.INSTANCE.close(ps);
                    IOUtils.INSTANCE.close(rs);
                    IOUtils.INSTANCE.close(con, true);
                    throw new JRException(e);
                }
            }

            @Override
            public Object getFieldValue(JRField jrField) throws JRException {
                try {
                    return bean.getClass().getDeclaredField(jrField.getName()).get(bean);
                } catch (Exception e) {
                    throw new JRException(e);
                }
            }
        };

    }

    protected class QueryResultBean {
        protected String accountId;
        protected String memberName;
        protected String businessName;
        protected String segmentation;
        protected Long totalSales;
        protected Long totalSalesAfterVat;
        protected Long ppn;
        protected Long totalMarkup;
        protected Long ppnMarkup;
        protected Long freqVisit;
        protected String registrationDate;
        protected String lastVisitDate;
    }
}
