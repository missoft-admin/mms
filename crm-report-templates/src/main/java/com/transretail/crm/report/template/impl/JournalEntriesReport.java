package com.transretail.crm.report.template.impl;

import java.math.BigDecimal;
import java.util.Map;
import java.util.Set;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import com.mysema.query.BooleanBuilder;
import com.transretail.crm.core.entity.PeopleSoftStore;
import com.transretail.crm.core.entity.lookup.Store;
import com.transretail.crm.core.service.PsoftStoreService;
import net.sf.jasperreports.engine.JRDataSource;

import org.joda.time.DateTime;
import org.joda.time.LocalDate;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Service;

import com.google.common.collect.Sets;
import com.mysema.query.jpa.impl.JPAQuery;
import com.mysema.query.types.Projections;
import com.mysema.query.types.QBean;
import com.transretail.crm.common.reporting.engine.data.JRQueryDSLDataSource;
import com.transretail.crm.common.reporting.jasper.ClasspathFileResolver;
import com.transretail.crm.common.reporting.jasper.DefaultJRProcessor;
import com.transretail.crm.common.reporting.jasper.JRProcessor;
import com.transretail.crm.common.util.DateUtil;
import com.transretail.crm.core.entity.enums.ReportType;
import com.transretail.crm.core.reporting.support.StoreFieldValueCustomizer;
import com.transretail.crm.core.security.util.UserUtil;
import com.transretail.crm.core.service.StoreService;
import com.transretail.crm.giftcard.entity.QCrmForPeoplesoft;
import com.transretail.crm.report.template.CommonReportFilter;
import com.transretail.crm.report.template.FilterField;
import com.transretail.crm.report.template.NoDataFoundSupport;
import com.transretail.crm.report.template.ReportTemplate;

@Service("journalEntriesReport")
public class JournalEntriesReport implements ReportTemplate, NoDataFoundSupport {
	protected static final String TEMPLATE_NAME = "Journal Entries Report";
    protected static final String REPORT_NAME = "reports/journal_entries_report.jasper";
    protected static final String FILTER_DATE_TO = CommonReportFilter.DATE_TO;
    protected static final String FILTER_DATE_FROM = CommonReportFilter.DATE_FROM;
    
    private static final DateTimeFormatter INPUT_DATE_PATTERN = DateTimeFormat.forPattern(DateUtil.SEARCH_DATE_FORMAT);
    
    private static final Logger LOGGER = LoggerFactory.getLogger(JournalEntriesReport.class);
    
    @PersistenceContext
    private EntityManager em;
    @Autowired
    private MessageSource messageSource;
    @Autowired
    private StoreService storeService;
	@Autowired
	private PsoftStoreService psoftStoreService;
    
    @Override
    public ReportType[] getReportTypes() {
    	return ReportType.values();
    }
    
    @Override
    public String getName() {
    	return TEMPLATE_NAME;
    }

    @Override
    public String getDescription() {
    	return messageSource.getMessage("journal.entries.report", (Object[]) null, LocaleContextHolder.getLocale());
    }
    
    @Override
    public boolean canView(Set<String> permissions) {
    	return true;
    }
    
    @Override
    public Set<FilterField> getFilters() {
    	Set<FilterField> filters = Sets.newLinkedHashSet();
		filters.add(FilterField.createDateField(FILTER_DATE_FROM,
				messageSource.getMessage("label_date_from", (Object[]) null, LocaleContextHolder.getLocale()),
				DateUtil.SEARCH_DATE_FORMAT, true));
			filters.add(FilterField.createDateField(FILTER_DATE_TO,
				messageSource.getMessage("label_date_to", (Object[]) null, LocaleContextHolder.getLocale()),
				DateUtil.SEARCH_DATE_FORMAT, true));
			
		return filters;
    }
    
    @Override
    public JRProcessor createJrProcessor(Map<String, String> map) {
    	DateTime startDate = INPUT_DATE_PATTERN.parseLocalDate(map.get(FILTER_DATE_FROM)).toDateTimeAtStartOfDay().toDateTime();
		DateTime endDate = INPUT_DATE_PATTERN.parseLocalDate(map.get(FILTER_DATE_TO)).plusDays(1).toDateTimeAtStartOfDay().minusMillis(1).toDateTime();
		
		QCrmForPeoplesoft qjournal = QCrmForPeoplesoft.crmForPeoplesoft;

		final Store store = UserUtil.getCurrentUser().getStore();
		final PeopleSoftStore storeMapping = psoftStoreService.getStoreMapping(store.getCode());

		BooleanBuilder builder = new BooleanBuilder(qjournal.transactionDate.between(startDate, endDate));

		if (!store.getCode().equals("10007")) {
			builder.and(qjournal.storeCode.eq(store.getCode()));
			builder.and(qjournal.businessUnit.eq(storeMapping.getBusinessUnit().getCode()));
		}

		JPAQuery query = new JPAQuery(em).from(qjournal).where(builder).orderBy(qjournal.transactionDate.asc());
		
		QBean<ReportBean> projection = Projections.bean(ReportBean.class,
				qjournal.transactionDate.as("transactionDate"),
				qjournal.transactionAmt.as("transactionAmount"),
				qjournal.type.as("type"),
				qjournal.mmsAcct.as("mmsAct"),
				qjournal.psAcct.as("psAct"),
				qjournal.storeCode.as("store"),
				qjournal.businessUnit.as("bu"),
				qjournal.deptId.as("dept"),
				qjournal.notes.as("notes")
				);
		
		JRDataSource dataSource = new JRQueryDSLDataSource(query, projection,
                new StoreFieldValueCustomizer("store", storeService));
		
		
		DefaultJRProcessor jrProcessor = new DefaultJRProcessor(REPORT_NAME);
        jrProcessor.setFileResolver(new ClasspathFileResolver("reports"));
        jrProcessor.addParameter("SUB_DATA_SOURCE", dataSource);
        jrProcessor.addParameter("START_DATE", map.get(FILTER_DATE_FROM));
        jrProcessor.addParameter("END_DATE", map.get(FILTER_DATE_TO));
        jrProcessor.addParameter("printedBy", UserUtil.getCurrentUser().getUsername());
        jrProcessor.addParameter("printedDate", new LocalDate().toString("dd-MM-yyyy"));
        
        return jrProcessor;
    }
    
    public static class ReportBean {
    	private DateTime transactionDate;
    	private BigDecimal transactionAmount;
    	private String type;
    	private String mmsAct;
    	private String psAct;
    	private String store;
    	private String bu;
    	private String dept;
    	private String notes;
		public DateTime getTransactionDate() {
			return transactionDate;
		}
		public void setTransactionDate(DateTime transactionDate) {
			this.transactionDate = transactionDate;
		}
		public BigDecimal getTransactionAmount() {
			return transactionAmount;
		}
		public void setTransactionAmount(BigDecimal transactionAmount) {
			this.transactionAmount = transactionAmount;
		}
		public String getType() {
			return type;
		}
		public void setType(String type) {
			this.type = type;
		}
		public String getMmsAct() {
			return mmsAct;
		}
		public void setMmsAct(String mmsAct) {
			this.mmsAct = mmsAct;
		}
		public String getPsAct() {
			return psAct;
		}
		public void setPsAct(String psAct) {
			this.psAct = psAct;
		}
		public String getStore() {
			return store;
		}
		public void setStore(String store) {
			this.store = store;
		}
		public String getBu() {
			return bu;
		}
		public void setBu(String bu) {
			this.bu = bu;
		}
		public String getDept() {
			return dept;
		}
		public void setDept(String dept) {
			this.dept = dept;
		}
		public String getNotes() {
			return notes;
		}
		public void setNotes(String notes) {
			this.notes = notes;
		}
    	
    }

	@Override
	public boolean isEmpty(Map<String, String> map) {
		DateTime startDate = INPUT_DATE_PATTERN.parseLocalDate(map.get(FILTER_DATE_FROM)).toDateTimeAtStartOfDay().toDateTime();
		DateTime endDate = INPUT_DATE_PATTERN.parseLocalDate(map.get(FILTER_DATE_TO)).plusDays(1).toDateTimeAtStartOfDay().minusMillis(1).toDateTime();
		
		QCrmForPeoplesoft qjournal = QCrmForPeoplesoft.crmForPeoplesoft;
		
		return !new JPAQuery(em).from(qjournal).where(qjournal.transactionDate.between(startDate, endDate)).exists();
	}

}
