package com.transretail.crm.core.repo.custom.impl;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

import org.springframework.stereotype.Repository;

import com.transretail.crm.core.entity.UserRoleModel;

@Repository
public class UserRoleRepoCustomImpl {

    @PersistenceContext
    EntityManager itsEntityManager;

    public List<UserRoleModel> findByIdSet(List<Long> inIds) {

        StringBuffer theParam = new StringBuffer();

        for (Long theId : inIds) {
            theParam.append("'" + theId + "',");
        }
        theParam.deleteCharAt(theParam.length() - 1);
        TypedQuery<UserRoleModel> theQuery = itsEntityManager.createQuery(
                "SELECT a FROM UserRoleModel a WHERE a.id IN ( :ids )",
                UserRoleModel.class);
        theQuery.setParameter("ids", inIds);

        return theQuery.getResultList();
    }
}
