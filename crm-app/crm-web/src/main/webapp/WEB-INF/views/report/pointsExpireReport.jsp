<%@include file="/WEB-INF/views/common/taglibs.jsp" %>

<div class="form-horizontal">
	<div class="page-header page-header2"><h1><spring:message code="txn_report_label_points_expire_report_type" /></h1></div>
	
	<div class="well">
		<label class=""></label>
		<div class="">
			<select name="reportType" id="reportType">
			    <c:forEach items="${reportTypes}" var="reportType">
			    	<option value="${reportType.code}"><spring:message code="report_type_${reportType.code}" /></option>
			    </c:forEach>
		    </select>
		</div>
	</div>
	<spring:message var="msg_invaliddaterange" code="points_msg_invaliddaterange" /> <!--must be set the common error message-->
	<div class="row-fluid input-daterange" id="printDuration" data-msg-invaliddaterange="${msg_invaliddaterange}"/>
	
	<fieldset class="pull-left span6">
		<legend><spring:message code="txn_report_label_points_expire_report_type" /></legend>
		<div class="hide alert alert-error" id="content_error1">
		  <button type="button" class="close" onclick="$(this).parent().hide();">&times;</button>
		  <div><form:errors path="*"/>
		  </div>
		</div>
		<c:url value='/report/points/expire/' var="url"/>
		<form:form id="form" action="${url}">
		<div class="control-group">
			<label class="control-label">
				<span class="required">*</span> <spring:message code="label_date"/> <spring:message code="label_from" />
			</label>
			<div class="controls">
				<input type="text" id="dateFrom" class="date" />
			</div>
		</div>
		
		<div class="control-group">
			<label class="control-label">
				<span class="required">*</span> <spring:message code="label_date"/> <spring:message code="label_to" />
			</label>
			<div class="controls">
				<input type="text" id="dateTo" class="date" />
			</div>
		</div>
		
		<div class="">
			<label class="control-label"></label>
			<div class="control-group form-actions">
				<button type="button" class="btn btn-primary" id="view"><spring:message code="label_view"/></button>
			</div>
		</div>
		</form:form>
	</fieldset>
	
	<fieldset class="pull-left span6">
		<legend><spring:message code="txn_report_label_points_expire_future_report_type" /></legend>
		<div class="hide alert alert-error" id="content_error2">
		  <button type="button" class="close" onclick="$(this).parent().hide();">&times;</button>
		  <div><form:errors path="*"/>
		  </div>
		</div>
		<c:url value='/report/points/expire/future/' var="url"/>
		<form:form id="formFuture" action="${url}">
		<div class="control-group">
			<label class="control-label">
				<span class="required">*</span>  <spring:message code="label_date"/> <spring:message code="label_from" />
			</label>
			<div class="controls">
				<input type="text" id="dateFromFuture" class="futuredate" />
			</div>
		</div>
		
		<div class="control-group">
			<label class="control-label">
				<span class="required">*</span>  <spring:message code="label_date"/> <spring:message code="label_to" />
			</label>
			<div class="controls">
				<input type="text" id="dateToFuture" class="futuredate" />
			</div>
		</div>
		
		<div class="">
			<label class="control-label"></label>
			<div class="control-group form-actions">
				<button type="button" class="btn btn-primary" id="viewFuture"><spring:message code="label_view"/></button>
			</div>
		</div>
		</form:form>
	</fieldset>
</div>

<script src="<c:url value="/js/bootstrap/bootstrap-datepicker.js"/>"></script>
<link href="<c:url value="/css/bootstrap/datepicker.css" />" rel="stylesheet"/>

<script type="text/javascript">
$(document).ready(function() {
	var today = new Date();
	$(".date").datepicker({
		format: "dd-mm-yyyy",
		autoclose: true,
		endDate: "-" + today.getDate() + "d",
		viewMode: 'months',
		minViewMode: 'months'
	});

	$(".futuredate").datepicker({
		format: "dd-mm-yyyy",
		autoclose: true,
		startDate: "-0d",
		viewMode: 'months',
		minViewMode: 'months'
	});

	$("#dateFrom").change(function() {
		$("#dateTo").datepicker('setStartDate', $(this).datepicker('getDate'));
	});

	$("#dateFromFuture").change(function() {
		$("#dateToFuture").datepicker('setStartDate', $(this).datepicker('getDate'));
	});

	$("#view").click(function(e) {
		var dateFrom = $("#dateFrom").val();
		var dateTo = $("#dateTo").val();
	  e.preventDefault();
	  if (dateFrom === '' || dateTo === '' || dateFrom > dateTo) {
	    e.preventDefault();
	    var theErrorContainer = $("#content_error1");
	    theErrorContainer.find("div").html($("#printDuration").data("msg-invaliddaterange"));
	    theErrorContainer.show("slow");
	  }else{
		send(dateFrom, dateTo, $("#form").attr('action'));
	  }
	});
	$("#viewFuture").click(function(e) {
		var dateFrom = $("#dateFromFuture").val();
		var dateTo = $("#dateToFuture").val();
		  e.preventDefault();
	  if (dateFrom === '' || dateTo === '' || dateFrom > dateTo) {
	    e.preventDefault();
	    var theErrorContainer = $("#content_error2");
	    theErrorContainer.find("div").html($("#printDuration").data("msg-invaliddaterange"));
	    theErrorContainer.show("slow");
	  }else{
	    send(dateFrom, dateTo, $("#formFuture").attr('action'));
	  }
	});

	function send(dateFrom, dateTo, rootUrl) {
		var reportType = $("#reportType").val();

		if(!dateFrom) {
			dateFrom = null;
		}
		if(!dateTo) {
			dateTo = null;
		}

	    var url = rootUrl + reportType + "/" + dateFrom + "/" + dateTo;
	    if(reportType == "pdf")
	    	window.open( url, '_blank', 'toolbar=0,location=0,menubar=0' );
	    else if(reportType == "excel")
	  	  	window.location.href = url;
	}
});
</script>