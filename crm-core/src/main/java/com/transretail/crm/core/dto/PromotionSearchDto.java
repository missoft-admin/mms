package com.transretail.crm.core.dto;


import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.joda.time.LocalTime;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.google.common.collect.Lists;
import com.mysema.query.types.expr.BooleanExpression;
import com.transretail.crm.common.service.dto.request.AbstractSearchFormDto;
import com.transretail.crm.core.entity.QPromotion;
import com.transretail.crm.core.entity.enums.PromoModelType;
import com.transretail.crm.core.util.BooleanExprUtil;


public class PromotionSearchDto extends AbstractSearchFormDto {

	private Long id;
	private String status;
	private List<String> statuses;
	private String name;
    private String description;
    private String createdBy;
    private Date startDate;
    private Date endDate;
    private Date givenDate;

	private Long program;
	private Long campaign;

	private String affectedDays;
    private String rewardType;

    private String modelType;




	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public List<String> getStatuses() {
		return statuses;
	}

	public void setStatuses(List<String> statuses) {
		this.statuses = statuses;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public Date getStartDate() {
		return startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	public Date getEndDate() {
		return endDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	public Date getGivenDate() {
		return givenDate;
	}

	public void setGivenDate(Date givenDate) {
		this.givenDate = givenDate;
	}

	public Long getProgram() {
		return program;
	}

	public void setProgram(Long program) {
		this.program = program;
	}

	public Long getCampaign() {
		return campaign;
	}

	public void setCampaign(Long campaign) {
		this.campaign = campaign;
	}

	public String getAffectedDays() {
		return affectedDays;
	}

	public void setAffectedDays(String affectedDays) {
		this.affectedDays = affectedDays;
	}

	public String getRewardType() {
		return rewardType;
	}
	public void setRewardType(String rewardType) {
		this.rewardType = rewardType;
	}

	public String getModelType() {
		return modelType;
	}

	public void setModelType(String modelType) {
		this.modelType = modelType;
	}




	@Override
	@JsonIgnore
	public BooleanExpression createSearchExpression() {

		List<BooleanExpression> expressions = new ArrayList<BooleanExpression>();
		QPromotion thePromotion = QPromotion.promotion;
		
		if(id != null) {
			expressions.add(BooleanExprUtil.INSTANCE.isNumberPropertyEqual(thePromotion.id, id));
		}

        if ( StringUtils.isNotBlank( status ) ) {
        	expressions.add( BooleanExprUtil.INSTANCE.isStringPropertyEqual( thePromotion.status.code, status ) );
        }

        if ( CollectionUtils.isNotEmpty( statuses ) ) {
        	expressions.add( thePromotion.status.code.in( statuses ) );
        }

		if ( givenDate != null ) {
            expressions.add( BooleanExprUtil.INSTANCE.isDatePropertyLoe( thePromotion.startDate, givenDate ) );
            expressions.add( BooleanExprUtil.INSTANCE.isDatePropertyGoe( thePromotion.endDate, givenDate ) );
            
            LocalTime givenTime = new LocalTime(givenDate);
            
            if(givenTime != null) {
            	expressions.add( BooleanExprUtil.INSTANCE.isTimePropertyLoe( thePromotion.startTime, givenTime ) );
            	expressions.add( BooleanExprUtil.INSTANCE.isTimePropertyGoe( thePromotion.endTime, givenTime ) );
            }
            
        }

        if ( campaign != null ) {
            expressions.add( BooleanExprUtil.INSTANCE.isNumberPropertyEqual( thePromotion.campaign.id, campaign ) );
        }
        
        if ( StringUtils.isNotBlank( affectedDays ) )
        	expressions.add( BooleanExprUtil.INSTANCE.isStringPropertyLike(thePromotion.affectedDays, affectedDays) );

        if ( StringUtils.isNotBlank( name ) ) {
        	if ( StringUtils.isNotBlank( modelType ) && !modelType.equalsIgnoreCase( PromoModelType.PROMOTION.toString() ) ) {
        		if ( modelType.equalsIgnoreCase( PromoModelType.PROGRAM.toString() ) ) {
                	expressions.add( BooleanExprUtil.INSTANCE.isStringPropertyLike( thePromotion.campaign.program.name, name ) );
        		}
        		if ( modelType.equalsIgnoreCase( PromoModelType.CAMPAIGN.toString() ) ) {
                	expressions.add( BooleanExprUtil.INSTANCE.isStringPropertyLike( thePromotion.campaign.name, name ) );
        		}
        	}
        	else {
            	expressions.add( BooleanExprUtil.INSTANCE.isStringPropertyLike( thePromotion.name, name ) );
        	}
        }

        if ( StringUtils.isNotBlank( description ) ) {
        	if ( StringUtils.isNotBlank( modelType ) && !modelType.equalsIgnoreCase( PromoModelType.PROMOTION.toString() ) ) {
        		if ( modelType.equalsIgnoreCase( PromoModelType.PROGRAM.toString() ) ) {
                	expressions.add( BooleanExprUtil.INSTANCE.isStringPropertyLike( thePromotion.campaign.program.description, description ) );
        		}
        		if ( modelType.equalsIgnoreCase( PromoModelType.CAMPAIGN.toString() ) ) {
                	expressions.add( BooleanExprUtil.INSTANCE.isStringPropertyLike( thePromotion.campaign.description, description ) );
        		}
        	}
        	else {
            	expressions.add( BooleanExprUtil.INSTANCE.isStringPropertyLike( thePromotion.description, description ) );
        	}
        }

        if ( StringUtils.isNotBlank( createdBy ) ) {
        	if ( StringUtils.isNotBlank( modelType ) && !modelType.equalsIgnoreCase( PromoModelType.PROMOTION.toString() ) ) {
        		if ( modelType.equalsIgnoreCase( PromoModelType.PROGRAM.toString() ) ) {
                	expressions.add( BooleanExprUtil.INSTANCE.isStringPropertyEqual( thePromotion.campaign.program.createUser, createdBy ) );
        		}
        		if ( modelType.equalsIgnoreCase( PromoModelType.CAMPAIGN.toString() ) ) {
                	expressions.add( BooleanExprUtil.INSTANCE.isStringPropertyEqual( thePromotion.campaign.createUser, createdBy ) );
        		}
        	}
        	else {
            	expressions.add( BooleanExprUtil.INSTANCE.isStringPropertyEqual( thePromotion.createUser, createdBy ) );
        	}
        }
        
        if(startDate != null) {
        	expressions.add(thePromotion.startDate.goe(startDate));
		}
		if(endDate != null) {
			expressions.add(thePromotion.endDate.loe(endDate));
		}
		if(StringUtils.isNotBlank(rewardType)) {
			expressions.add(thePromotion.rewardType.code.eq(rewardType));
		}
        
        return BooleanExpression.allOf(expressions.toArray(new BooleanExpression[expressions.size()]));
	}
	
	/*public BooleanExpression getPromotionFilters(CampaignDto campaign) {
		QPromotion promotion = QPromotion.promotion;
		List<BooleanExpression> filters = Lists.newArrayList();
		if(campaign != null && campaign.getId() != null) {
			filters.add(promotion.campaign.id.eq(campaign.getId()));
		}
		if(StringUtils.defaultString(modelType).equalsIgnoreCase(PromoModelType.PROMOTION.toString()) && StringUtils.isNotBlank(name)) {
			filters.add(promotion.name.containsIgnoreCase(name));
		}
		if(StringUtils.defaultString(modelType).equalsIgnoreCase(PromoModelType.PROMOTION.toString()) && StringUtils.isNotBlank(description)) {
			filters.add(promotion.description.containsIgnoreCase(description));
		}
		if(StringUtils.isNotBlank(status)) {
			filters.add(promotion.status.code.eq(status));
		}
		if(CollectionUtils.isNotEmpty(statuses)) {
			filters.add(promotion.status.code.in(statuses));
		}
		if(StringUtils.isNotBlank(createdBy)) {
			filters.add(promotion.createUser.equalsIgnoreCase(createdBy));
		}
		if(startDate != null) {
			filters.add(promotion.startDate.goe(startDate));
		}
		if(endDate != null) {
			filters.add(promotion.endDate.loe(endDate));
		}
		if(StringUtils.isNotBlank(rewardType)) {
			filters.add(promotion.rewardType.code.eq(rewardType));
		}
		return BooleanExpression.allOf(filters.toArray(new BooleanExpression[filters.size()]));
	}*/



	//temp fix for search by promo
	public void filterPromoInProgram( List<ProgramDto> inPrograms ) {
		if ( id != null || StringUtils.isNotBlank( name ) || StringUtils.isNotBlank( description ) || StringUtils.isNotBlank( status ) 
				|| CollectionUtils.isNotEmpty( statuses ) || StringUtils.isNotBlank( createdBy ) ) {
        	for ( ProgramDto theProgram : inPrograms ) {
        		for ( CampaignDto theCampaign: theProgram.getCampaigns() ) {
        			List<PromotionDto> thePromotions = new ArrayList<PromotionDto>();
        			for ( PromotionDto thePromotion : theCampaign.getPromotions() ) {
        				if(id != null && thePromotion.getId().longValue() == id.longValue()) {
        					thePromotions.add(thePromotion);
        					break;
        				}
        				boolean doAdd = true;
        				if ( StringUtils.isNotBlank( name ) ) {
        					if ( StringUtils.isNotBlank( thePromotion.getName() ) && !StringUtils.containsIgnoreCase( thePromotion.getName(), name )
        							|| StringUtils.isBlank( thePromotion.getName() ) ) {    						
        						doAdd = doAdd && false;
        					}
        				}
        				if ( StringUtils.isNotBlank( description ) ) {
        					if ( StringUtils.isNotBlank( thePromotion.getDescription() ) && !StringUtils.containsIgnoreCase( thePromotion.getDescription(), description )
        							|| StringUtils.isBlank( thePromotion.getDescription() ) ) {
        						doAdd = doAdd && false;
        					}
        				}
        				if ( StringUtils.isNotBlank( status ) ) {
        					if ( StringUtils.isNotBlank( thePromotion.getStatus() ) && !StringUtils.equalsIgnoreCase( thePromotion.getStatus(), status )
        							|| StringUtils.isBlank( thePromotion.getStatus() ) ) {
        						doAdd = doAdd && false;
        					}
        				}
        		        if ( CollectionUtils.isNotEmpty( statuses ) ) {
        		        	if ( StringUtils.isNotBlank( thePromotion.getStatus() ) && !statuses.contains( thePromotion.getStatus() ) 
        							|| StringUtils.isBlank( thePromotion.getStatus() ) ) {
        		        		doAdd = doAdd && false;
        		        	}
        		        }
        		        if ( StringUtils.isNotBlank( createdBy ) ) {
        		        	if ( StringUtils.isNotBlank( thePromotion.getCreateUser() ) && !StringUtils.equalsIgnoreCase( thePromotion.getCreateUser(), createdBy )
        							|| StringUtils.isBlank( thePromotion.getCreateUser() ) ) {
        		        		doAdd = doAdd && false;
        		        	}
        		        }

        		        if ( doAdd ) {
            		        thePromotions.add( thePromotion );
        		        }
        			}
        			theCampaign.setPromotions( thePromotions );
        		}    		
        	}
    	}
	}
}
