package com.transretail.crm.report.template.impl;

import com.transretail.crm.common.reporting.jasper.AbstractJRProcessor;
import com.transretail.crm.common.util.IOUtils;
import com.transretail.crm.core.entity.enums.ReportType;
import com.transretail.crm.report.template.CommonReportFilter;
import com.transretail.crm.report.template.NoDataFoundSupport;
import net.sf.dynamicreports.jasper.builder.JasperReportBuilder;
import net.sf.dynamicreports.jasper.constant.JasperProperty;
import net.sf.dynamicreports.report.builder.column.ColumnBuilder;
import net.sf.dynamicreports.report.builder.column.TextColumnBuilder;
import net.sf.dynamicreports.report.builder.style.StyleBuilder;
import net.sf.dynamicreports.report.constant.HorizontalAlignment;
import net.sf.dynamicreports.report.constant.VerticalAlignment;
import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRField;
import net.sf.jasperreports.engine.export.JRExportProgressMonitor;
import net.sf.jasperreports.engine.export.JRXlsAbstractExporterParameter;
import net.sf.jasperreports.engine.export.JRXlsExporterParameter;
import org.apache.commons.lang3.StringUtils;
import org.joda.time.LocalDate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.sql.DataSource;
import java.awt.*;
import java.sql.*;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Map;

import static com.transretail.crm.report.template.ReportTemplate.INPUT_DATE_PATTERN;
import static net.sf.dynamicreports.report.builder.DynamicReports.*;

/**
 * @author Mike de Guzman
 */
public abstract class AbstractMemberPurchaseReport extends AbstractJRProcessor implements NoDataFoundSupport {

    private static final Logger logger = LoggerFactory.getLogger(AbstractMemberPurchaseReport.class);
    protected static final StyleBuilder COLUMN_TITLE_STYLE = stl.style()
            .setVerticalAlignment(VerticalAlignment.MIDDLE)
            .setHorizontalAlignment(HorizontalAlignment.LEFT)
            .setBorder(stl.penThin())
            .setPadding(2)
            .bold()
            .setBackgroundColor(new Color(153, 204, 255));
    private static final DateFormat DATE_FORMAT = new SimpleDateFormat("MMMM d, yyyy");
    protected String title;
    protected String subTitle;
    protected LocalDate dateFrom;
    protected LocalDate dateTo;
    protected String storeCode;
    protected DataSource dataSource;
    protected ReportType reportType;

    public AbstractMemberPurchaseReport(String reportFileName, String title, String subTitle, DataSource dataSource,
                                        ReportType reportType) {
        super(reportFileName);
        this.title = title;
        this.subTitle = subTitle;
        this.dataSource = dataSource;
        this.reportType = reportType;
        jRExporterParameters.put(JRXlsExporterParameter.IS_REMOVE_EMPTY_SPACE_BETWEEN_ROWS, Boolean.FALSE);
        jRExporterParameters.put(JRXlsAbstractExporterParameter.IS_COLLAPSE_ROW_SPAN, Boolean.FALSE);
    }

    public void setDateFrom(LocalDate dateFrom) {
        this.dateFrom = dateFrom;
    }

    public void setDateTo(LocalDate dateTo) {
        this.dateTo = dateTo;
    }

    protected JasperReportBuilder createJRBuilderWithTitleAndSubTitle() {
        return createBaseJRBuilder()
                .setParameters(parameters)
                .title(cmp.horizontalList()
                        .add(cmp.text(title).setStyle(stl.style().setFontSize(20).setHorizontalAlignment(HorizontalAlignment.CENTER)))
                        .newRow()
                        .add(cmp.text(""))
                        .newRow()
                        .add(cmp.text(subTitle).setStyle(stl.style().setFontSize(14))));
    }

    protected JasperReportBuilder createBaseJRBuilder() {
        JasperReportBuilder defaultReport = createDefaultJRBuilder();
        if (this.reportType.equals(ReportType.EXCEL)) {
            return defaultReport.ignorePagination();
        } else {
            return defaultReport;
        }
    }

    private JasperReportBuilder createDefaultJRBuilder() {
        return report()
                .setColumnTitleStyle(COLUMN_TITLE_STYLE)
                .setColumnStyle(stl.style().setVerticalAlignment(VerticalAlignment.TOP).setLeftPadding(2).setRightPadding(2))
                .addProperty("net.sf.jasperreports.export.xls.exclude.origin.keep.first.band.1", "title")
                .addProperty("net.sf.jasperreports.export.xls.exclude.origin.keep.first.band.2", "pageHeader")
                .addProperty("net.sf.jasperreports.export.xls.exclude.origin.keep.first.band.3", "columnHeader")
                .ignorePageWidth();
    }

    protected <T> ColumnBuilder createColumn(String title, String fieldName, Class<T> propertyClass, Integer width) {
        TextColumnBuilder<T> column = col.column(title, fieldName, propertyClass);
        if (ReportType.EXCEL == reportType) {
            column = column.setStretchWithOverflow(false);
            column = column.addProperty(JasperProperty.PRINT_KEEP_FULL_TEXT, "true");
            if (width != null) {
                column = column.setFixedWidth(width);
            }
        } else {
            column = column.setStretchWithOverflow(true);
            column = column.addProperty(JasperProperty.PRINT_KEEP_FULL_TEXT, "false");
            if (width != null) {
                column = column.setWidth(width);
            }
        }
        return column;
    }

    @Override
    public boolean isEmpty(Map<String, String> map) {
        LocalDate dateFrom = parseDate(map.get(CommonReportFilter.DATE_FROM));
        LocalDate dateTo = parseDate(map.get(CommonReportFilter.DATE_TO));
        String storeCode = map.get(CommonReportFilter.STORE);

        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;

        try {
            StringBuilder builder = new StringBuilder("SELECT b.customer_number,"); // Customer ID
            builder.append("b.customer_name,");// Customer Name
            builder.append("c.company_name,");// Business Name
            builder.append("d.description,");// Segmentation
            builder.append("c.contact,"); // Mobile Phone
            builder.append("c.business_phone,"); // Business Phone
            builder.append("c.home_phone,"); // Home Phone
            builder.append("addr.city as business_city, ");
            builder.append("addr.post_code as postal_code, ");
            builder.append("sum(DECODE(TYPE,'RETURN',-1,'REFUND',-1,1)*a.total_amount - a.total_discount + a.voided_discount - abs(a.rounding_amount)),"); // Total Amount
            builder.append("sum(DECODE(TYPE,'RETURN',-1,'REFUND',-1,1)*a.tariff),");// Amount VAT
            builder.append("sum(DECODE(TYPE,'RETURN',-1,'REFUND',-1,1)*a.vat),"); // PPN/Total VAT
            builder.append("sum(DECODE(TYPE,'RETURN',-1,'REFUND',-1,1)*a.total_non_member_markup),");// Amount Markup
            builder.append("sum((DECODE(TYPE,'RETURN',-1,'REFUND',-1,1)*a.total_non_member_markup) - (DECODE(TYPE,'RETURN',-1,'REFUND',-1,1)*a.total_non_member_markup/1.1)),"); // PPN/Total Markup
            builder.append("count(b.customer_number),"); // Freq Visit
            builder.append("c.created_datetime,");// Registration Date
            builder.append("max(a.transaction_date) ");// Last Visit Date
            builder.append("FROM POS_TRANSACTION a ");
            builder.append("LEFT JOIN tax_invoice b ON b.pos_txn_id = a.id ");
            builder.append("left join crm_member c on b.customer_number=c.account_id ");
            builder.append("left join crm_address addr on c.business_address = addr.id ");
            builder.append("left join crm_ref_lookup_dtl d on d.code=c.customer_segregation ");
            builder.append("WHERE a.type in (?, ?, ?) AND a.status = ? ");
            builder.append("AND a.CUSTOMER_ID IS NOT NULL ");
            builder.append("AND b.customer_number IS NOT NULL ");
            builder.append("AND a.id LIKE ? ");
            builder.append("AND a.sales_date between ? and ? ");
            if(StringUtils.isNotBlank(storeCode)){
                builder.append("AND a.store_id = (select p.store_id from store p where p.code=?) ");
            }
            builder.append("GROUP BY b.customer_number, b.customer_name, c.company_name, d.description, c.contact, c.business_phone, c.home_phone, addr.city, addr.post_code, c.created_datetime ");
            builder.append("ORDER BY b.customer_number");

            con = dataSource.getConnection();
            ps = con.prepareStatement(builder.toString(), ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_READ_ONLY);
            ps.setString(1, "SALE");
            ps.setString(2, "REFUND");
            ps.setString(3, "RETURN");
            ps.setString(4, "COMPLETED");
            ps.setString(5, "2000%");
            ps.setTimestamp(6, new Timestamp(dateFrom.toDate().getTime()));
            ps.setTimestamp(7, new Timestamp(dateTo.toDate().getTime()));
            if(StringUtils.isNotBlank(storeCode)){
                ps.setString(8, storeCode);
            }
            logger.info(ps.toString());
            rs = ps.executeQuery();

            if (!rs.isBeforeFirst() ) {
                return true;
            }
        } catch (SQLException e) {
            logger.error("SQL Error: {}", e);
        } finally {
            if (rs != null) {
                IOUtils.INSTANCE.close(rs);
            }
            if (ps != null) {
                IOUtils.INSTANCE.close(ps);
            }
            if (con != null) {
                IOUtils.INSTANCE.close(con, true);
            }
        }
        return false;
    }

    protected JRDataSource createJrDataSource() {
        return new JRDataSource() {
            Connection con;
            PreparedStatement ps;
            ResultSet rs;
            QueryResultBean bean;
            boolean firstQuery = true;

            @Override
            public boolean next() throws JRException {
                try {
                    if (firstQuery) {
                        StringBuilder builder = new StringBuilder("SELECT b.customer_number,"); // Customer ID
                        builder.append("b.customer_name,");// Customer Name
                        builder.append("c.company_name,");// Business Name
                        builder.append("d.description,");// Segmentation
                        builder.append("c.contact,"); // Mobile Phone
                        builder.append("c.business_phone,"); // Business Phone
                        builder.append("c.home_phone,"); // Home Phone
                        builder.append("addr.city as business_city, ");
                        builder.append("addr.post_code as postal_code, ");
                        builder.append("sum(DECODE(TYPE,'RETURN',-1,'REFUND',-1,1)*a.total_amount - a.total_discount + a.voided_discount - abs(a.rounding_amount)),"); // Total Amount
                        builder.append("sum(DECODE(TYPE,'RETURN',-1,'REFUND',-1,1)*a.tariff),");// Amount VAT
                        builder.append("sum(DECODE(TYPE,'RETURN',-1,'REFUND',-1,1)*a.vat),"); // PPN/Total VAT
                        builder.append("sum(DECODE(TYPE,'RETURN',-1,'REFUND',-1,1)*a.total_non_member_markup),");// Amount Markup
                        builder.append("sum((DECODE(TYPE,'RETURN',-1,'REFUND',-1,1)*a.total_non_member_markup) - (DECODE(TYPE,'RETURN',-1,'REFUND',-1,1)*a.total_non_member_markup/1.1)),"); // PPN/Total Markup
                        builder.append("count(b.customer_number),"); // Freq Visit
                        builder.append("c.created_datetime,");// Registration Date
                        builder.append("max(a.transaction_date) ");// Last Visit Date
                        builder.append("FROM POS_TRANSACTION a ");
                        builder.append("LEFT JOIN tax_invoice b ON b.pos_txn_id = a.id ");
                        builder.append("left join crm_member c on b.customer_number=c.account_id ");
                        builder.append("left join crm_address addr on c.business_address = addr.id ");
                        builder.append("left join crm_ref_lookup_dtl d on d.code=c.customer_segregation ");
                        builder.append("WHERE a.type in (?, ?, ?) AND a.status = ? ");
                        builder.append("AND a.CUSTOMER_ID IS NOT NULL ");
                        builder.append("AND b.customer_number IS NOT NULL ");
                        builder.append("AND a.id LIKE ? ");
                        builder.append("AND a.sales_date between ? and ? ");
                        if(StringUtils.isNotBlank(storeCode)){
                            builder.append("AND a.store_id = (select p.store_id from store p where p.code=?) ");
                        }
                        builder.append("GROUP BY b.customer_number, b.customer_name, c.company_name, d.description, c.contact, c.business_phone, c.home_phone, addr.city, addr.post_code, c.created_datetime ");
                        builder.append("ORDER BY b.customer_number");

                        con = dataSource.getConnection();
                        ps = con.prepareStatement(builder.toString(), ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_READ_ONLY);
                        ps.setString(1, "SALE");
                        ps.setString(2, "REFUND");
                        ps.setString(3, "RETURN");
                        ps.setString(4, "COMPLETED");
                        ps.setString(5, "2000%");
                        ps.setTimestamp(6, new Timestamp(dateFrom.toDate().getTime()));
                        ps.setTimestamp(7, new Timestamp(dateTo.toDate().getTime()));
                        if(StringUtils.isNotBlank(storeCode)){
                            ps.setString(8, storeCode);
                        }
                        logger.info(ps.toString());
                        rs = ps.executeQuery();

                        JRExportProgressMonitor afterPageExportListener = new JRExportProgressMonitor() {
                            @Override
                            public void afterPageExport() {
                                IOUtils.INSTANCE.close(ps);
                                IOUtils.INSTANCE.close(rs);
                                IOUtils.INSTANCE.close(con, true);
                            }
                        };
                        setAfterPageExportListener(afterPageExportListener);
                    }

                    boolean hasNext = rs.next();
                    if (hasNext) {
                        int counter = 0;
                        bean = new QueryResultBean();
                        bean.customerId = rs.getString(++counter);
                        bean.customerName = rs.getString(++counter);
                        bean.businessName = rs.getString(++counter);
                        bean.segmentation = rs.getString(++counter);
                        bean.mobilePhone = rs.getString(++counter);
                        bean.businessPhone = rs.getString(++counter);
                        bean.homePhone = rs.getString(++counter);
                        bean.businessCity = rs.getString(++counter);
                        bean.businessPostCode = rs.getString(++counter);
                        bean.totalAmount = rs.getLong(++counter);
                        bean.amountVat = rs.getLong(++counter);
                        bean.ppn = rs.getLong(++counter);
                        bean.totalMarkup = rs.getLong(++counter);
                        bean.ppnMarkup = rs.getLong(++counter);
                        bean.freqVisit = rs.getLong(++counter);
                        Date date = rs.getDate(++counter);
                        if (date != null) {
                            bean.registrationDate = DATE_FORMAT.format(date);
                        }
                        date = rs.getDate(15);
                        if (date != null) {
                            bean.lastVisitDate = DATE_FORMAT.format(date);
                        }
                    } else if (firstQuery) {
                        hasNext = true;
                        bean = new QueryResultBean();
                    }
                    firstQuery = false;
                    return hasNext;
                } catch (SQLException e) {
                    IOUtils.INSTANCE.close(ps);
                    IOUtils.INSTANCE.close(rs);
                    IOUtils.INSTANCE.close(con, true);
                    throw new JRException(e);
                }
            }

            @Override
            public Object getFieldValue(JRField jrField) throws JRException {
                try {
                    return bean.getClass().getDeclaredField(jrField.getName()).get(bean);
                } catch (Exception e) {
                    throw new JRException(e);
                }
            }
        };
    }

    public void setStoreCode(String storeCode) {
        this.storeCode = storeCode;
    }

    private LocalDate parseDate(String dateInstance) {
        return StringUtils.isNotBlank(dateInstance) ? INPUT_DATE_PATTERN.parseLocalDate(dateInstance) : null;
    }

    protected class QueryResultBean {
        protected String customerId;
        protected String customerName;
        protected String businessName;
        protected String segmentation;
        protected String mobilePhone;
        protected String businessPhone;
        protected String homePhone;
        protected Long totalAmount;
        protected Long amountVat;
        protected Long ppn;
        protected Long totalMarkup;
        protected Long ppnMarkup;
        protected Long freqVisit;
        protected String registrationDate;
        protected String lastVisitDate;
        protected String businessPostCode;
        protected String businessCity;
    }
}
