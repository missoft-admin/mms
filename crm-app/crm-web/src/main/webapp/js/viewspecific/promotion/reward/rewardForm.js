$(document).ready( function() {

	var theDialog = null;



	$( "#reward" ).submit( function(e) {
		e.preventDefault();
		save( $(this).attr("action"), theDialog );
	});





	function save( inUrl, theDialog ) {
		$.ajax({
			type 	: "POST", url 	: inUrl, cache	: false,
			data	: $( "#reward" ).serialize(),
			success : function( inResponse ) {				
				if ( inResponse.success ) {
					$( theDialog ).dialog( "close" );
					if ( $( "#promotionRewards" ) ) {
						updateRewardList( inResponse.result );
					}
					else {
						refreshList();
					}
				}
				else {
					$.each( inResponse.result, function( key, value ) {
						$("#content_error").append( (key + 1 ) + ". " + value.code + "<br>" );
					});
				}
			},
			error 	: function(jqXHR, textStatus, errorThrown) {}
		});
	}

	function refreshList() {
		var promotionRewards = $( "#promotionRewards" );
		$.ajax({
			type 	: "GET", cache : false, dataType : "html",
			url 	: $( promotionRewards ).data("url"), 
			success : function( inResponse ) {
				$( promotionRewards ).html( inResponse );
			},
			error 	: function(jqXHR, textStatus, errorThrown) {}
		});
	}

	function updateRewardList( inData ) {
		var thePromotionRewards = $("#promotionRewards");
		$( thePromotionRewards ).val( $( thePromotionRewards ).val() + "," + inData.id );

		var theTbody = $("#content_promotionRewardsList").find('tbody');
		if ( theTbody ) {
			$( theTbody )
				.append( $('<tr>')
					.append( $('<td>').html( inData.rewardType ) )
					.append( $('<td>').html( inData.minQty ) )
					.append( $('<td>').html( inData.maxQty ) )
					.append( $('<td>').html( inData.minAmount ) )
					.append( $('<td>').html( inData.maxAmount ) )
					.append( $('<td>').html( inData.baseAmount ) )
					.append( $('<td>').html( inData.baseFactor ) )
					.append( $('<td>').html( inData.maxBonus ) )
					.append( $('<td>').html( inData.discount ) )
				);
		}
	}
});




