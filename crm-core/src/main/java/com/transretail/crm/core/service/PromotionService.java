package com.transretail.crm.core.service;


import java.util.Date;
import java.util.List;

import com.transretail.crm.common.service.dto.response.ResultList;
import com.transretail.crm.common.service.dto.rest.ProductRestDto;
import com.transretail.crm.common.service.dto.rest.ReturnMessage;
import com.transretail.crm.common.service.dto.rest.ValidProductsDto;
import com.transretail.crm.core.dto.*;
import com.transretail.crm.core.entity.Campaign;
import com.transretail.crm.core.entity.MemberModel;
import com.transretail.crm.core.entity.Program;
import com.transretail.crm.core.entity.Promotion;
import com.transretail.crm.core.entity.TransactionAudit;


public interface  PromotionService {

	PromotionResultList searchPromotion(PromotionSearchDto inDto);

	List<ProgramDto> processSearch(PromotionSearchDto theSearchDto);


	List<ApprovalRemarkDto> getRemarkDtos(Long inPromoId);

	void saveRemarkDto(ApprovalRemarkDto inDto);


	List<Program> getPrograms();

    List<Program> getValidPrograms(Date date);

    List<Program> getValidPrograms();

	List<Campaign> getValidCampaigns(Long programId);

    void saveProgram( Program inProgram );

	void saveProgramDto(ProgramDto inProgram);

    List<ProgramDto> getValidProgramDtos();

	List<ProgramDto> getProgramDtos();

	ProgramDto getProgramDto(Long inId);


    List<Campaign> getValidCampaigns(Program program);

    List<Campaign> getValidCampaigns(Program program, Date date);

	void saveCampaign(Campaign inCampaign);

	void saveCampaignDto(CampaignDto inCampaign);

	List<CampaignDto> getCampaignDtos(Long inProgramId);

	CampaignDto getCampaignDto(Long inId);


	void savePromotionRewardDto(PromotionRewardDto inReward);


	PromotionDto getPromotionDto(Long inId);

    List<Promotion> getValidPromotions(Campaign campaign);

    List<Promotion> getValidPromotionsForCampaign(Campaign campaign, Date date, String rewardType);

    List<Promotion> getValidPromotionsForCampaign(Campaign campaign, Date date );

	Promotion savePromotion(Promotion inPromotion);

	void savePromotionDto(PromotionDto inPromotion);

	void updatePromotionDto(PromotionDto inPromotion);


    void processPromotions(MemberModel member, String paymentType, String storeCode, String transactionNo,
                           String transactionAmount, String cardNumber, String transactionDate, List<PosTxItemDto>
            itemList, ReturnMessage returnMessage);

    Double convertPointToCurrency(Long points);
    
    Double convertCurrencyToPoints(Double amount);


	PromoRewardDto retrievePromotionReward(String memberId, String paymentType,
			String storeCode, String transactionNo, Double transactionAmount,
			String cardNumber, Date transactionDate,
			List<TransactionAudit> audits, List<PosTxItemDto> itemList);


	List<Promotion> getValidPromotionsForMember(String memberId,
			String storeCode, String transactionNo, String paymentType,
			String cardNumber, Date transactionDate, List<PosTxItemDto> products);

	long countTargetMembers(long id);

	ResultList<MemberDto> getTargetMembers(PromotionSearchDto searchDto);

    void computePromotionRewards(String  memberId, PromoRewardDto reward, Promotion promotion, Double transactionAmount, String transactionNo, String storeCode, List<PosTxItemDto> itemList, List<TransactionAudit> audits);

    ValidProductsDto getValidItems(String id, String store);

    ProductRestDto getProductPromo(String pluId, String id, String storeCode);


    Promotion getPromotion(Long id);
}
