<%@ include file="../../common/taglibs.jsp" %>


  <fieldset class="form-horizontal container-fluid span3" id="content_mandatoryProfInfo">
    <legend class="memberDetails"></legend>
    <div class="pull-left mandatoryInfo" id="mandatoryProfInfo">
      <div class="control-group">
        <spring:message code="label_com_transretail_crm_entity_customermodel_firstname" var="reqd_firstName" />
        <label for="reqd_firstName" class="control-label"><b class="required">*</b><c:out value="${reqd_firstName}" /> </label>
        <div class="controls"><input type="text" data-field="firstName" id="reqd_firstName" class="form-control reqdField" placeholder="${reqd_firstName}" /></div>
      </div>
      <div class="control-group">
        <spring:message code="label_com_transretail_crm_entity_customermodel_birthdate" var="reqd_birthdate" />
        <label for="reqd_birthdate" class="control-label"><b class="required">*</b><c:out value="${reqd_birthdate}" /> </label>
        <div class="controls"><input type="text" data-field="customerProfile.birthdate" id="reqd_birthdate" class="form-control reqdField" placeholder="${reqdBirthdate}" /></div>
      </div>
       <div class="control-group">
	      <spring:message code="label_com_transretail_crm_entity_customermodel_pin" var="reqd_pin" />
	      <label for="reqd_pin" class="control-label"><b class="required">*</b><c:out value="${reqd_pin}" /> </label>
        <div class="controls"><input type="password" data-field="pin" id="reqd_pin" class="form-control reqdField" placeholder="${reqd_pin}" maxlength="6"/></div>
      </div>
      <div class="control-group">
        <spring:message code="label_address_postalcode" var="reqd_postCode" />
        <label for="reqd_postCode" class="control-label"><b class="required">*</b><c:out value="${reqd_postCode}" /> </label>
        <div class="controls"><input type="text" data-field="address.postCode" id="reqd_postCode" class="form-control reqdField" placeholder="${reqd_postCode}" /></div>
      </div>
      <div class="control-group">
        <spring:message code="label_com_transretail_crm_entity_customermodel_gender" var="reqd_gender" />
        <label for="reqd_gender" class="control-label"><b class="required">*</b><c:out value="${reqd_gender}" /> </label>
        <div class="controls"><select data-field="customerProfile.gender" id="reqd_gender" class="form-control reqdField" ><c:forEach var="item" items="${gender}"><option value="${item.code}">${item.description}</option></c:forEach></select></div>
      </div>
      <div class="control-group">
        <spring:message code="label_com_transretail_crm_entity_customermodel_religion" var="reqd_religion" />
        <label for="reqd_religion" class="control-label"><b class="required">*</b><c:out value="${reqd_religion}" /> </label>
        <div class="controls"><select data-field="customerProfile.religion" id="reqd_religion" class="form-control reqdField" ><c:forEach var="item" items="${religion}"><option value="${item.code}">${item.description}</option></c:forEach></select></div>
      </div>

      <div class="control-group">
        <spring:message code="label_com_transretail_crm_entity_customermodel_idnumber" var="reqd_idNumberTxt" />
        <label for="reqd_idNumberTxt" class="control-label"><b class="required">*</b><c:out value="${reqd_idNumberTxt}" /> </label>
        <div class="controls">
          <input type="text" data-field="idNumberTxt" id="reqd_idNumberTxt" class="form-control reqdField" placeholder="${reqd_idNumberTxt}" />
          <div class="radio mt10"><input type="radio" data-field="idNumberType" id="reqd_idNumberType" name="reqd_idNumberType" data-value="KTP" class="form-control reqdField"><label><spring:message code="member_label_ktp" /></label></div>
          <div class="radio"><input type="radio" data-field="idNumberType" id="reqd_idNumberType" name="reqd_idNumberType" data-value="SIM" class="form-control reqdField"><label><spring:message code="member_label_sim" /></label></div>
          <div class="radio"><input type="radio" data-field="idNumberType" id="reqd_idNumberType" name="reqd_idNumberType" data-value="PASSPORT" class="form-control reqdField"><label><spring:message code="member_label_passport" /></label></div>
        </div>
      </div>
      <div class="control-group">
        <spring:message code="label_com_transretail_crm_entity_customermodel_timetocall" var="reqd_bestTimeToCall" />
        <label for="reqd_bestTimeToCall" class="control-label"><b class="required">*</b><c:out value="${reqd_bestTimeToCall}" /> </label>
        <div class="controls">
          <c:forEach var="item" items="${bestTimeToCall}">
          <div class="checkbox"><input type="checkbox" data-field="bestTimeToCall" id="reqd_bestTimeToCall" name="reqd_bestTimeToCall" data-value="${item.code}" class="form-control reqdField"><label>${item.description}</label></div>
          </c:forEach>
        </div>
      </div>

      <div class="control-group">
        <spring:message code="label_com_transretail_crm_entity_customermodel_radius" var="reqd_radius" />
        <label for="reqd_radius" class="control-label"><b class="required">*</b><c:out value="${reqd_radius}" /> </label>
        <div class="controls"><select data-field="professionalProfile.radius" id="reqd_radius" class="form-control reqdField" ><c:forEach var="item" items="${radii}"><option value="${item.code}">${item.description}</option></c:forEach></select></div>
      </div>
      <div class="control-group">
        <spring:message code="label_com_transretail_crm_entity_customermodel_zone" var="reqd_zone" />
        <label for="reqd_zone" class="control-label"><b class="required">*</b><c:out value="${reqd_zone}" /> </label>
        <div class="controls"><input type="text" data-field="professionalProfile.zone" id="reqd_zone" class="form-control reqdField" placeholder="${reqd_zone}" /></div>
      </div>
      <div class="control-group">
        <spring:message code="label_com_transretail_crm_entity_customermodel_businessname" var="reqd_companyName" />
        <label for="reqd_companyName" class="control-label"><b class="required">*</b><c:out value="${reqd_companyName}" /> </label>
        <div class="controls"><input type="text" data-field="companyName" id="reqd_companyName" class="form-control reqdField" placeholder="${reqd_companyName}" /></div>
      </div>
      <div class="control-group">
        <spring:message code="label_com_transretail_crm_entity_customermodel_businesstype" var="reqd_customerGroup" />
        <label for="reqd_customerGroup" class="control-label"><b class="required">*</b><c:out value="${reqd_customerGroup}" /> </label>
        <div class="controls"><select data-field="professionalProfile.customerGroup" id="reqd_customerGroup" class="form-control reqdField" ><c:forEach var="item" items="${customerGroups}"><option value="${item.code}">${item.description}</option></c:forEach></select></div>
      </div>
      
      <div class="control-group">
        <spring:message code="label_address_street" var="reqd_street" />
        <label for="reqd_street" class="control-label"><b class="required">*</b><c:out value="${reqd_street}" /> </label>
        <div class="controls"><input type="text" data-field="professionalProfile.businessAddress.street" id="reqd_street" class="form-control reqdField" placeholder="${reqd_street}" /></div>
      </div>
      
      <div class="control-group">
        <spring:message code="label_address_postalcode" var="reqd_postCode" />
        <label for="reqd_postCode" class="control-label"><b class="required">*</b><c:out value="${reqd_postCode}" /> </label>
        <div class="controls"><input type="text" data-field="professionalProfile.businessAddress.postCode" id="reqd_postCode" class="form-control reqdField" placeholder="${reqd_postCode}" /></div>
      </div>

      <div class="control-group">
        <spring:message code="member_prop_contact" var="reqd_contact" />
        <label for="reqd_contact" class="control-label"><b class="required">*</b><c:out value="${reqd_contact}" /> </label>
        <div class="controls"><input type="text" data-field="contact" id="reqd_contact" class="form-control reqdField" placeholder="${reqd_contact}" /></div>
      </div>
    </div>

    <script type="text/javascript">
    var MandatoryProfInfo = null;
    $( document ).ready( function() {
    	  var $mandatory = $( "#mandatoryProfInfo" );
    	  var $memberDialog = $( "#professionalFormDialog" );

        initOthers();
    	  initFields();

    	  function initFields() {
            $.each( $mandatory.find( ".reqdField" ), function( idx, obj ) {
                var $el = $memberDialog.find( $( "[name='" + $(obj).data( "field" ) + "']" ) );
                $el.change( function() {
                	  if ( $(this).attr( "type" ) == "radio" || $(this).attr( "type" ) == "checkbox" ) {
                        var selIndx = $el.index( $(this) );
                        $( "input[name='reqd_" + $(this).attr( "name" ) + "']")[selIndx - 0].checked = this.checked;
                	  }
                	  else {
                        $( obj ).val( $(this).val() );
                	  }
                });
                $( obj ).change( function() {
                    if ( $el.attr( "type" ) == "radio" || $el.attr( "type" ) == "checkbox" ) {
                    	  var selIndx = $( "input[name='" + $(this).attr( "name" ) + "']").index( $(this) );
                    	  $el[selIndx - 0].checked = this.checked;
                    }
                    else {
                        $el.val( ( $( this ).data( "value" ) )? $( this ).data( "value" ) : $(this).val() );
                    }
                });
                $( obj ).val( "" );
            });
            $mandatory.find( "#reqd_birthdate" ).datepicker({ format    : 'dd-mm-yyyy', endDate   : '-0d', autoclose : true });
    	  }

    	  function initOthers() {
            $memberDialog.on( "hide", function(e) { if ( e.target === this ) { MandatoryProfInfo.reset( $memberDialog ); } });
    	  }

    	  MandatoryProfInfo = {
    			  fill : function ( key, value ) {
    				    if ( key == "customerProfile" || key == "address" || key == "professionalProfile" ) {
    				    	  $.each( value, function ( k, v ) {
    				    		    var $mandEl = $mandatory.find( "#reqd_" + k.substring( k.indexOf( "." ) + 1 ) );

    				    		    if ( v ) {
    				    		    	  if ( $mandEl.length ) {
    				    		    		    v = v.code? v.code : v;
		                            if ( k.indexOf( "birthdate" ) != -1 ) {
		                                try {
		                                    var bdate = v ? ( new Date( v ).getTime()? new Date( v ) 
		                                        : new Date( $.trim( value ).split( " " )[0] ).getTime()? new Date( $.trim( value ).split( " " )[0] ) : null ) 
		                                            : null;
		                                    $mandEl.datepicker( "update", bdate );
		                                } catch(e) {}
		                            }
		                            else {
		                                $mandEl.val( v );
		                            }
    				    		    	  }
		                        else if ( v && k.indexOf( "businessAddress" ) != -1 ) {
		                            $.each( v, function ( k1, v1 ) {
		                            	  $mandEl = $mandatory.find( "#reqd_" + k1.substring( k1.lastIndexOf( "." ) + 1 ) );
	                                  if ( $mandEl.length ) {
	                                	    $mandEl.val( v1 );
	                                  }
		                            });
		                        }
    				    		    }
                    });
    				    }
    				    else {
    				    	  if ( key == "idNumber" && value ) {
                        var valueType = value.substring( 0, value.indexOf( "-" ) );
                        $mandatory.find( "input[name='reqd_" + key + "Type'][data-value='" + valueType + "']" ).attr( "checked", true );

                        value = value.substring( value.indexOf( "-" ) + 1 );
    				    		    key = key + "Txt";
    			              $mandatory.find( "#reqd_" + key ).val( value );
    				    	  }
    				    	  else if ( key == "bestTimeToCall" && value ) {
                        $mandatory.find( "input[name='reqd_" + key + "'][data-value='" + value + "']" ).attr( "checked", true );
                    }
    				    	  else {
    			              $mandatory.find( "#reqd_" + key ).val( value );
    				    	  }
    				    }
    			  },
 		        reset     : function( ele, isEmpty ) {
 		            $( ele ).find(':input').each( function() {
 		                switch(this.type) {
 		                    case 'password':
 		                    case 'select-one':
 		                    case 'text':
 		                    case 'textarea':
 		                    case 'file':
 		                    case 'hidden': $(this).val(''); break;
 		                    case 'select-multiple': isEmpty? $(this).empty() : $(this).val(""); break;
 		                    case 'checkbox':
 		                    case 'radio': this.checked = false;
 		                }
 		            });
 		        }
    	  }
    });
    </script>
  </fieldset>
  
  
  