package com.transretail.crm.web.controller;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.transretail.crm.common.service.dto.response.ResultList;
import com.transretail.crm.core.service.CodePropertiesService;
import com.transretail.crm.core.service.LookupService;
import com.transretail.crm.loyalty.dto.LoyaltyAnnualFeeSchemeDto;
import com.transretail.crm.loyalty.dto.LoyaltyAnnualFeeSchemeSearchDto;
import com.transretail.crm.loyalty.entity.LoyaltyAnnualFeeScheme;
import com.transretail.crm.loyalty.service.LoyaltyAnnualFeeService;
import com.transretail.crm.web.controller.util.ControllerResponse;


@RequestMapping("/loyaltyannualfee")
@Controller
public class LoyaltyAnnualFeeController {
	
	@Autowired
	LoyaltyAnnualFeeService loyaltyAnnualFeeService;
	@Autowired
	LookupService lookupService;
	@Autowired
	CodePropertiesService codePropertiesService;
	@Autowired
	MessageSource messageSource;
	
	@RequestMapping(value = "/scheme/list", method = RequestMethod.POST)
    public @ResponseBody ResultList<LoyaltyAnnualFeeSchemeDto> listSchemes(@RequestBody LoyaltyAnnualFeeSchemeSearchDto sortDto) {
    	return loyaltyAnnualFeeService.listSchemes(sortDto);
    }
	
	@RequestMapping(value = "/scheme")
    public String list(Model uiModel) {
		getLookups(uiModel);
    	return "annualfeescheme/list";
    }
	
	@RequestMapping(value = "/scheme/form/{id}", method = RequestMethod.GET)
    public String showForm(
    		@PathVariable("id") String id,
    		Model uiModel) {
		LoyaltyAnnualFeeSchemeDto schemeDto = new LoyaltyAnnualFeeSchemeDto();
		if (id.matches("[0-9]*")) {
			schemeDto = loyaltyAnnualFeeService.getScheme(Long.valueOf(id));
		}
		uiModel.addAttribute("scheme", schemeDto);
		getLookups(uiModel);
    	return "annualfeescheme/form";
    }
	
	@RequestMapping(value = "/scheme/form", method = RequestMethod.POST, produces = "application/json; charset=utf-8")
    public @ResponseBody ControllerResponse showForm(
    		@ModelAttribute LoyaltyAnnualFeeSchemeDto scheme,
    		BindingResult result, HttpServletRequest request) {
		
		ControllerResponse response = new ControllerResponse();
		response.setSuccess(true);
		
		validateScheme(result, scheme);
		
		if (result.hasErrors()) {
			response.setSuccess(false);
			response.setResult(result.getAllErrors());
			return response;
		}

		if (response.isSuccess()) {
			if(scheme.getId() == null)
				loyaltyAnnualFeeService.saveScheme(scheme);
			else
				loyaltyAnnualFeeService.updateScheme(scheme);
		}

		return response;
    }
	
	private void validateScheme(BindingResult result, LoyaltyAnnualFeeSchemeDto scheme) {
		if(scheme.getCompany() == null) {
			result.reject(getMessage("propkey_msg_notempty", new Object[] {"loyalty_annual_company"}));
		}
		if(scheme.getCardType() == null) {
			result.reject(getMessage("propkey_msg_notempty", new Object[] {"loyalty_annual_cardtype"}));
		}
		if(scheme.getAnnualFee() == null) {
			result.reject(getMessage("propkey_msg_notempty", new Object[] {"loyalty_annual_annualfee"}));
		}
		if(scheme.getReplacementFee() == null) {
			result.reject(getMessage("propkey_msg_notempty", new Object[] {"loyalty_annual_replacementfee"}));
		}
		if(scheme.getCompany() != null && scheme.getCardType() != null) {
			LoyaltyAnnualFeeScheme sch = loyaltyAnnualFeeService.getScheme(scheme.getCompany(), scheme.getCardType());
			if(sch != null && !sch.getId().equals(scheme.getId())) {
				result.reject(messageSource.getMessage("loyalty_annual_duplicatescheme", new Object[] {scheme.getCompany().getDescription(), scheme.getCardType().getDescription()}, LocaleContextHolder.getLocale()));
			}
		}
	}
	
	private String getMessage(String msgCode, Object[] argMsgCodes) {
		if(argMsgCodes != null) {
			for ( int i=0; i < argMsgCodes.length; i++ ) {
				argMsgCodes[i] = messageSource.getMessage( argMsgCodes[i].toString().toLowerCase(), null, LocaleContextHolder.getLocale() );
			}
		}
		return messageSource.getMessage(msgCode, argMsgCodes, "Info given is invalid.", LocaleContextHolder.getLocale());
	}
	
	@RequestMapping(value = "/scheme/{id}", method = RequestMethod.DELETE)
    public @ResponseBody ControllerResponse delete(@PathVariable("id") Long id,
            Model uiModel) {
		ControllerResponse response = new ControllerResponse();
		response.setSuccess(true);
        loyaltyAnnualFeeService.deleteScheme(id);
        return response;
    }
	
	private void getLookups(Model uiModel) {
		uiModel.addAttribute("companies", lookupService.getActiveDetailsByHeaderCode(codePropertiesService.getHeaderCompany()));
		uiModel.addAttribute("cardTypes", lookupService.getActiveDetailsByHeaderCode(codePropertiesService.getHeaderMembershipTier()));
	}
	
}
