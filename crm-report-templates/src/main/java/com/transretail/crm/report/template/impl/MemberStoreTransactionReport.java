package com.transretail.crm.report.template.impl;

import com.google.common.collect.*;
import com.mysema.query.BooleanBuilder;
import com.mysema.query.jpa.impl.JPAQuery;
import com.mysema.query.types.Projections;
import com.mysema.query.types.QBean;
import com.mysema.query.types.expr.NumberExpression;
import com.transretail.crm.common.reporting.engine.data.JRQueryDSLDataSource;
import com.transretail.crm.common.reporting.engine.data.ReportDataSource;
import com.transretail.crm.common.reporting.jasper.*;
import com.transretail.crm.common.util.DateUtil;
import com.transretail.crm.core.entity.QMemberModel;
import com.transretail.crm.core.entity.QPointsTxnModel;
import com.transretail.crm.core.entity.QPosTransaction;
import com.transretail.crm.core.entity.enums.PointTxnType;
import com.transretail.crm.core.entity.enums.ReportType;
import com.transretail.crm.core.entity.lookup.*;
import com.transretail.crm.core.repo.*;
import com.transretail.crm.core.service.CodePropertiesService;
import com.transretail.crm.core.service.StoreService;
import com.transretail.crm.report.template.*;
import java.util.*;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.apache.commons.lang3.StringUtils;
import org.joda.time.DateTime;
import org.joda.time.LocalDate;
import org.joda.time.LocalDateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.MessageSourceAware;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.context.support.MessageSourceAccessor;
import org.springframework.stereotype.Service;

/**
 *
 * @author Monte Cillo Co (mco@exist.com)
 */
@Service("memberStoreTransactionReport")
public class MemberStoreTransactionReport implements ValidatableReportTemplate, MessageSourceAware, ReportDateFormatProvider {

    private MessageSourceAccessor messageSource;
    protected static final String FILTER_TX_STORE = "txStore";
    protected static final String FILTER_MEMBER_TYPE = "memberType";
    private static final String PERMISSION_KEY = "REPORT_MEMBER_STORE_TRANSACTION_VIEW";
    static final String TEMPLATE_NAME = "Member Store Transaction Report";
    static final String REPORT_NAME = "reports/member-store-transaction-report.jasper";

    @Autowired
    private StoreService storeService;
    @Autowired
    private CodePropertiesService codePropertiesService;
    @Autowired
    private LookupDetailRepo lookupDetailRepo;
    @PersistenceContext
    private EntityManager em;

    @Override
    public String getName() {
        return TEMPLATE_NAME;
    }

    @Override
    public String getDescription() {
        return messageSource.getMessage("member.store.transaction.report.title");
    }

    @Override
    public Set<FilterField> getFilters() {
        Set<FilterField> filters = Sets.newLinkedHashSetWithExpectedSize(3);
        String filterTxStore = messageSource.getMessage("member.store.transaction.report.filter.tx.store");
        String filterMemberType = messageSource.getMessage("member.store.transaction.report.filter.member.type");
        String filterSalesDateFrom = messageSource.getMessage("member.store.transaction.report.filter.date.from");
        String filterSalesDateTo = messageSource.getMessage("member.store.transaction.report.filter.date.to");

        filters.add(FilterField.createDropdownField(FILTER_TX_STORE, filterTxStore, getStoresOption()));
        filters.add(FilterField.createDropdownField(FILTER_MEMBER_TYPE, filterMemberType, getMemberTypeOption()));
        filters.add(FilterField.createDateField(CommonReportFilter.DATE_FROM, filterSalesDateFrom, filterSalesDateFrom, true) );
        filters.add(FilterField.createDateField(CommonReportFilter.DATE_TO, filterSalesDateTo, filterSalesDateTo, true));

        return Collections.unmodifiableSet(filters);
    }

    private Map<String, String> getMemberTypeOption() {
        String headerMemberType = codePropertiesService.getHeaderMemberType();
        QLookupDetail q = QLookupDetail.lookupDetail;
        Iterable<LookupDetail> result = lookupDetailRepo.findAll(q.header.code.eq(headerMemberType));
        Map<String, String> options = Maps.newLinkedHashMap();
        options.put("", "");

        for (LookupDetail lookupDetail : result) {
            options.put(lookupDetail.getCode(), lookupDetail.getDescription());
        }

        return Collections.unmodifiableMap(options);
    }

    private Map<String, String> getStoresOption() {
        return storeService.getAllStoresHashMap();
    }

    @Override
    public boolean canView(Set<String> permissions) {
        return permissions.contains(PERMISSION_KEY);
    }

    @Override
    public JRProcessor createJrProcessor(Map<String, String> map) {
        String selectedTxStore = map.get(FILTER_TX_STORE);
        String tmpDateFrom = map.get(CommonReportFilter.DATE_FROM);
        String tmpDateTo = map.get(CommonReportFilter.DATE_TO);

        DefaultJRProcessor defaultJRProcessor = new DefaultJRProcessor(REPORT_NAME);
        if(StringUtils.isNotBlank(selectedTxStore)) {
	        Store store = storeService.getStoreByCode(selectedTxStore);
	        defaultJRProcessor.addParameter("TRANSACTION_LOCATION", store.getCodeAndName());
        }
        defaultJRProcessor.addParameter("SALES_DATE_FROM", tmpDateFrom);
        defaultJRProcessor.addParameter("SALES_DATE_TO", tmpDateTo);
        defaultJRProcessor.addParameter("SUB_DATASOURCE", createDataSource(map));
        defaultJRProcessor.setFileResolver(new ClasspathFileResolver("reports"));

        return defaultJRProcessor;
    }

    @Override
    public ReportType[] getReportTypes() {
        return new ReportType[]{ReportType.EXCEL};
    }

    @Override
    public void setMessageSource(MessageSource messageSource) {
        this.messageSource = new MessageSourceAccessor(messageSource);
    }

    @Override
    public DateTimeFormatter getDefaultDateFormat() {
        return DateTimeFormat.forPattern(DateUtil.SEARCH_DATE_FORMAT);
    }

    @Override
    public List<String> validate(Map<String, String> parameters) {
        final Locale locale = LocaleContextHolder.getLocale();
        List<String> errors = Lists.newArrayList();
        final String dateFrom = parameters.get(CommonReportFilter.DATE_FROM);
        final String dateTo = parameters.get(CommonReportFilter.DATE_TO);
        if (StringUtils.isBlank(dateFrom) || StringUtils.isBlank(dateTo)) {
            errors.add(messageSource.getMessage("required_date_fields",  locale));
            return errors;
        }

        LocalDate df = LocalDate.parse(dateFrom, getDefaultDateFormat());
        LocalDate dt = LocalDate.parse(dateTo, getDefaultDateFormat());

        if (df.isAfter(dt)) {
            errors.add(messageSource.getMessage("report.date.from.invalid.message", locale));
        }

        if (dt.isBefore(df)) {
            errors.add(messageSource.getMessage("report.date.to.invalid.message", locale));
        }

        return errors;
    }

    public ReportDataSource createDataSource(Map<String, String> parameters) {
        String transactionLocation = parameters.get(FILTER_TX_STORE);
        String memberType = parameters.get(FILTER_MEMBER_TYPE);
        String tmpDateFrom = parameters.get(CommonReportFilter.DATE_FROM);
        String tmpDateTo = parameters.get(CommonReportFilter.DATE_TO);

        LocalDate txDateFrom = LocalDate.parse(tmpDateFrom, getDefaultDateFormat());
        LocalDate txDateTo = LocalDate.parse(tmpDateTo, getDefaultDateFormat());
        LocalDateTime from = txDateFrom.toDateTimeAtCurrentTime().toLocalDateTime();
        LocalDateTime to = txDateTo.toDateTimeAtCurrentTime().toLocalDateTime();

        from = from.millisOfDay().withMinimumValue();
        to = to.millisOfDay().withMaximumValue();

        final QPosTransaction ps = QPosTransaction.posTransaction;
        final QStore qstx = new QStore("tx_store");
        final QStore qsreg = new QStore("reg_store");
        final QLookupDetail qld = QLookupDetail.lookupDetail;
        final QPointsTxnModel qpoints = QPointsTxnModel.pointsTxnModel;
        final QMemberModel qm = QMemberModel.memberModel;

        final NumberExpression<Double> afterTax = ps.totalAmount.subtract(
                ps.totalDiscount.subtract(ps.voidedDiscount)); //.as(" TOTAL_AFTER_TAX ");
        final NumberExpression<Double> beforeTax = afterTax.subtract(ps.taxAmount); //.as(" TOTAL_BEFORE_TAX ");
        BooleanBuilder booleanBuilder = new BooleanBuilder(qm.id.eq(qpoints.memberModel.id)
                .and(qm.cardType.code.eq(qld.code))
                .and(ps.store.eq(qstx))
                .and(qm.registeredStore.eq(qsreg.code))
                .and(ps.id.eq(qpoints.transactionNo))
                .and(qpoints.transactionType.eq(PointTxnType.EARN))
                .and(ps.type.in("SALE", "REFUND", "RETURN"))
                .and(ps.status.eq("COMPLETED"))
                .and(ps.salesDate.between(from, to))
                .and(qstx.code.eq(transactionLocation)));

        if (StringUtils.isNotBlank(memberType)) {
            booleanBuilder.and(qm.memberType.code.eq(memberType));
        }

        JPAQuery query = new JPAQuery(em).from(ps, qm, qld, qsreg, qstx, qpoints)
                .where(booleanBuilder)
                .groupBy(qm.accountId, qm.memberType.code, qld.description,
                        qm.registeredStore, qsreg.name, qstx.code,
                        qstx.name, qpoints.transactionDateTime, ps.id,
                        beforeTax, ps.taxAmount, afterTax, ps.type)
                .orderBy(qm.accountId.asc(), qpoints.transactionDateTime.asc(), ps.type.asc());

        QBean<ReportBean> projections = Projections.bean(ReportBean.class,
                qm.accountId.as("accountNo"),
                qm.memberType.code.as("memberType"),
                qld.description.as("cardType"),
                qm.registeredStore.concat("-").concat(qsreg.name).as("registeredStore"),
                qstx.name.as("transactionLocation"),
                qpoints.transactionDateTime.as("transactionDateTime"),
                ps.id.as("transactionNo"),
                beforeTax.as("totalAmountBeforeTax"),
                ps.taxAmount.as("taxAmount"),
                afterTax.as("totalAmountAfterTax"),
                ps.type.as("transactionType"),
                qpoints.transactionPoints.sum().as("earnedPoint"));

        return new JRQueryDSLDataSource(query, projections);
    }

    public static class ReportBean {

        private String accountNo;
        private String cardType;
        private String registeredStore;
        private String transactionLocation;
        private String transactionDate;
        private String transactionTime;
        private DateTime transactionDateTime;
        private String transactionNo;
        private Double totalAmountBeforeTax;
        private Double taxAmount;
        private Double totalAmountAfterTax;
        private String transactionType;
        private String memberType;
        private Double earnedPoint;

        public String getAccountNo() {
            return accountNo;
        }

        public void setAccountNo(String accountNo) {
            this.accountNo = accountNo;
        }

        public String getCardType() {
            return cardType;
        }

        public void setCardType(String cardType) {
            this.cardType = cardType;
        }

        public String getRegisteredStore() {
            return registeredStore;
        }

        public void setRegisteredStore(String registeredStore) {
            this.registeredStore = registeredStore;
        }

        public String getTransactionLocation() {
            return transactionLocation;
        }

        public void setTransactionLocation(String transactionLocation) {
            this.transactionLocation = transactionLocation;
        }

        public String getTransactionDate() {
            return transactionDate;
        }

        public void setTransactionDate(String transactionDate) {
            this.transactionDate = transactionDate;
        }

        public String getTransactionTime() {
            return transactionTime;
        }

        public void setTransactionTime(String transactionTime) {
            this.transactionTime = transactionTime;
        }

        public DateTime getTransactionDateTime() {
            return transactionDateTime;
        }

        public void setTransactionDateTime(Date date) {
            this.transactionDateTime = LocalDateTime.fromDateFields(date).toDateTime();
            this.transactionDate = this.transactionDateTime.toString(DateUtil.SEARCH_DATE_FORMAT);
            this.transactionTime = this.transactionDateTime.toString(DateUtil.TIME_FORMAT);
        }

        public String getTransactionNo() {
            return transactionNo;
        }

        public void setTransactionNo(String transactionNo) {
            this.transactionNo = transactionNo;
        }

        public Double getTotalAmountBeforeTax() {
            return totalAmountBeforeTax;
        }

        public void setTotalAmountBeforeTax(Double totalAmountBeforeTax) {
            this.totalAmountBeforeTax = totalAmountBeforeTax;
        }

        public Double getTaxAmount() {
            return taxAmount;
        }

        public void setTaxAmount(Double taxAmount) {
            this.taxAmount = taxAmount;
        }

        public Double getTotalAmountAfterTax() {
            return totalAmountAfterTax;
        }

        public void setTotalAmountAfterTax(Double totalAmountAfterTax) {
            this.totalAmountAfterTax = totalAmountAfterTax;
        }

        public String getTransactionType() {
            return transactionType;
        }

        public void setTransactionType(String transactionType) {
            this.transactionType = transactionType;
        }

        public String getMemberType() {
            return memberType;
        }

        public void setMemberType(String memberType) {
            this.memberType = memberType;
        }

        public Double getEarnedPoint() {
            return earnedPoint;
        }

        public void setEarnedPoint(Double earnedPoint) {
            this.earnedPoint = earnedPoint;
        }

    }
}
