package com.transretail.crm.core.service.impl;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Configurable;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import com.transretail.crm.core.entity.CompanyProfile;
import com.transretail.crm.core.repo.CompanyProfileRepo;
import com.transretail.crm.core.service.CompanyProfileService;

@Configurable
@Transactional
@ContextConfiguration(locations = "classpath*:/META-INF/spring/applicationContext*.xml")
@RunWith(SpringJUnit4ClassRunner.class)
public class CompanyProfileServiceTest {

	@Autowired
	CompanyProfileRepo companyProfileRepo;
	
	@Autowired
	CompanyProfileService companyProfileService;
	
	@Before
	public void setup() {
		CompanyProfile companyProfile = new CompanyProfile();
		companyProfile.setName("test");
		companyProfile.setCopyrightText("test");
		companyProfile.setId("CRM");
		
		companyProfileRepo.saveAndFlush(companyProfile);
	}
	
	@After
	public void teardown() {
		companyProfileRepo.deleteAll();
	}
	
	@Test
	public void testSaveProfile() {
		Assert.assertEquals(1, companyProfileRepo.findAll().size());
		
		CompanyProfile companyProfile = new CompanyProfile();
		companyProfile.setName("test");
		companyProfile.setCopyrightText("test");
		
		companyProfileService.saveProfile(companyProfile);
		
		Assert.assertEquals(1, companyProfileRepo.findAll().size());
	}
	
	@Test
	public void testRetrieveCompany() {
		CompanyProfile companyProfile = companyProfileService.retrieveCompany();
		Assert.assertNotNull(companyProfile);
		Assert.assertEquals("test", companyProfile.getName());
		Assert.assertEquals("test", companyProfile.getCopyrightText());
	}
}
