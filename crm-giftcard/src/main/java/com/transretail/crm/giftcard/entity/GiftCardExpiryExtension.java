package com.transretail.crm.giftcard.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.Type;
import org.joda.time.LocalDate;

import com.transretail.crm.core.entity.support.CustomAuditableEntity;
import com.transretail.crm.giftcard.entity.support.GiftCardDateExtendStatus;
import com.transretail.crm.giftcard.entity.support.GiftCardHandlingFeeType;

/**
 * @author ftopico
 */
@Entity
@Table(name = "CRM_GC_DATE_EXPIRY_EXTENSION")
public class GiftCardExpiryExtension extends CustomAuditableEntity<Long> implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 7131940555282328503L;

    @Column(name = "EXTEND_NO")
    private String extendNo;
    
    @Column(name = "CREATE_DATE")
    @Temporal(TemporalType.DATE)
    @Type(type = "org.jadira.usertype.dateandtime.joda.PersistentLocalDate")
    private LocalDate createDate;
    
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "ORDER_ID")
    private SalesOrder order;
    
    @Column(name = "SERIES_TO", length = 16)
    private Long seriesTo;
    
    @Column(name = "SERIES_FROM", length = 16)
    private Long seriesFrom;
    
    @Column(name = "EXPIRY_DATE")
    @Temporal(TemporalType.DATE)
    @Type(type = "org.jadira.usertype.dateandtime.joda.PersistentLocalDate")
    private LocalDate expiryDate;
    
    @Column(name = "FILED_BY")
    private String filedBy;
    
    @Column(name="STATUS")
    @Enumerated(EnumType.STRING)
    private GiftCardDateExtendStatus status;
    
    @Column(name="HANDLING_FEE")
    private Double handlingFee;
    
    @Column(name="HANDLING_FEE_TYPE")
    @Enumerated(EnumType.STRING)
    private GiftCardHandlingFeeType handlingFeeType;

    public String getExtendNo() {
        return extendNo;
    }

    public void setExtendNo(String extendNo) {
        this.extendNo = extendNo;
    }

    public LocalDate getCreateDate() {
        return createDate;
    }

    public void setCreateDate(LocalDate createDate) {
        this.createDate = createDate;
    }

    public SalesOrder getOrder() {
        return order;
    }

    public void setOrder(SalesOrder order) {
        this.order = order;
    }

    public Long getSeriesTo() {
        return seriesTo;
    }

    public void setSeriesTo(Long seriesTo) {
        this.seriesTo = seriesTo;
    }

    public Long getSeriesFrom() {
        return seriesFrom;
    }

    public void setSeriesFrom(Long seriesFrom) {
        this.seriesFrom = seriesFrom;
    }

    public LocalDate getExpiryDate() {
        return expiryDate;
    }

    public void setExpiryDate(LocalDate expiryDate) {
        this.expiryDate = expiryDate;
    }

    public String getFiledBy() {
        return filedBy;
    }

    public void setFiledBy(String filedBy) {
        this.filedBy = filedBy;
    }

    public GiftCardDateExtendStatus getStatus() {
        return status;
    }

    public void setStatus(GiftCardDateExtendStatus status) {
        this.status = status;
    }

    public Double getHandlingFee() {
        return handlingFee;
    }

    public void setHandlingFee(Double handlingFee) {
        this.handlingFee = handlingFee;
    }

    public GiftCardHandlingFeeType getHandlingFeeType() {
        return handlingFeeType;
    }

    public void setHandlingFeeType(GiftCardHandlingFeeType handlingFeeType) {
        this.handlingFeeType = handlingFeeType;
    }

}
