package com.transretail.crm.giftcard.service.impl;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.apache.commons.collections.CollectionUtils;
import org.hibernate.StatelessSession;
import org.joda.time.DateTime;
import org.joda.time.LocalDate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.mysema.query.BooleanBuilder;
import com.mysema.query.jpa.JPASubQuery;
import com.mysema.query.jpa.hibernate.HibernateUpdateClause;
import com.mysema.query.jpa.impl.JPAQuery;
import com.mysema.query.types.Order;
import com.mysema.query.types.OrderSpecifier;
import com.mysema.query.types.Predicate;
import com.transretail.crm.common.reporting.jasper.ClasspathFileResolver;
import com.transretail.crm.common.reporting.jasper.DefaultJRProcessor;
import com.transretail.crm.common.reporting.jasper.JRProcessor;
import com.transretail.crm.common.service.dto.response.ResultList;
import com.transretail.crm.common.service.util.SpringDataPagingUtil;
import com.transretail.crm.core.entity.lookup.LookupDetail;
import com.transretail.crm.core.security.model.impl.CustomSecurityUserDetailsImpl;
import com.transretail.crm.core.security.util.UserUtil;
import com.transretail.crm.core.service.CodePropertiesService;
import com.transretail.crm.giftcard.dto.ReturnRecordDto;
import com.transretail.crm.giftcard.dto.SalesOrderDto;
import com.transretail.crm.giftcard.dto.SalesOrderPaymentInfoDto;
import com.transretail.crm.giftcard.dto.SalesOrderPaymentSearchDto;
import com.transretail.crm.giftcard.dto.SalesOrderPaymentsDto;
import com.transretail.crm.giftcard.entity.QSalesOrderPaymentInfo;
import com.transretail.crm.giftcard.entity.SalesOrder;
import com.transretail.crm.giftcard.entity.SalesOrderHistory;
import com.transretail.crm.giftcard.entity.SalesOrderPaymentInfo;
import com.transretail.crm.giftcard.entity.support.PaymentInfoStatus;
import com.transretail.crm.giftcard.entity.support.SalesOrderStatus;
import com.transretail.crm.giftcard.entity.support.SalesOrderType;
import com.transretail.crm.giftcard.repo.SalesOrderHistoryRepo;
import com.transretail.crm.giftcard.repo.SalesOrderPaymentInfoRepo;
import com.transretail.crm.giftcard.repo.SalesOrderRepo;
import com.transretail.crm.giftcard.service.GiftCardAccountingService;
import com.transretail.crm.giftcard.service.ReturnGiftCardService;
import com.transretail.crm.giftcard.service.SalesOrderPaymentService;
import com.transretail.crm.giftcard.service.SalesOrderService;

@Service("salesOrderPaymentService")
public class SalesOrderPaymentServiceImpl implements SalesOrderPaymentService {

	private SalesOrderPaymentInfoRepo repo;
	private SalesOrderRepo orderRepo;
	private StatelessSession statelessSession;
	private CodePropertiesService codePropertiesService;
	private GiftCardAccountingService accountingService;
	private SalesOrderService orderService;
	private ReturnGiftCardService returnService;

	@PersistenceContext
	private EntityManager em;

	@Autowired
	private SalesOrderHistoryRepo soHistoryRepo;

	@Autowired
	public void setRepo(SalesOrderPaymentInfoRepo repo) {
		this.repo = repo;
	}

	@Autowired
	public void setOrderRepo(SalesOrderRepo orderRepo) {
		this.orderRepo = orderRepo;
	}

	@Autowired
	public void setStatelessSession(StatelessSession statelessSession) {
		this.statelessSession = statelessSession;
	}

	@Autowired
	public void setCodePropertiesService(CodePropertiesService codePropertiesService) {
		this.codePropertiesService = codePropertiesService;
	}

	@Autowired
	public void setAccountingService(GiftCardAccountingService accountingService) {
		this.accountingService = accountingService;
	}

	@Autowired
	public void setOrderService(SalesOrderService orderService) {
		this.orderService = orderService;
	}

	@Autowired
	public void setReturnService(ReturnGiftCardService returnService) {
		this.returnService = returnService;
	}

	@Override
	public BigDecimal getTotalPayments(Long orderId) {
		QSalesOrderPaymentInfo qPayment = QSalesOrderPaymentInfo.salesOrderPaymentInfo;
		BigDecimal totalPayment = new JPAQuery(em).from(qPayment).where(qPayment.order.id.eq(orderId)).singleResult(qPayment.paymentAmount.sum());
		return totalPayment == null ? BigDecimal.ZERO : totalPayment;
	}

	@Override
	@Transactional(readOnly = true)
	public ResultList<SalesOrderPaymentInfoDto> getPayments(SalesOrderPaymentSearchDto sortDto) {
		CustomSecurityUserDetailsImpl userDetails = UserUtil.getCurrentUser();
		
		QSalesOrderPaymentInfo qPaymentInfo = QSalesOrderPaymentInfo.salesOrderPaymentInfo;
		BooleanBuilder params=new BooleanBuilder();
		params.and(sortDto.createSearchExpression());
		
		if(!userDetails.getStore().getName().contains("HEAD"))
			params.and(qPaymentInfo.order.paymentStore.eq(userDetails.getStoreCode()));
		
		Pageable pageable = SpringDataPagingUtil.INSTANCE.createPageable(sortDto.getPagination());
		Page<SalesOrderPaymentInfo> page = repo.findAll(params, pageable);
		return new ResultList<SalesOrderPaymentInfoDto>(SalesOrderPaymentInfoDto.toDtos(page.getContent()), page.getTotalElements(), page.hasPreviousPage(),page.hasNextPage());
	}

	@Override
	@Transactional(readOnly = true)
	public List<SalesOrderPaymentInfoDto> getSalesOrdersPaymentForPrint(SalesOrderPaymentSearchDto searchDto) {
		QSalesOrderPaymentInfo qPaymentInfo = QSalesOrderPaymentInfo.salesOrderPaymentInfo;
		List<SalesOrderPaymentInfo> orders = Lists.newArrayList(repo.findAll(
				qPaymentInfo.status.in(UI_STATUS).and(searchDto.createSearchExpression()), new OrderSpecifier(Order.DESC, qPaymentInfo.order.orderDate)));
		return SalesOrderPaymentInfoDto.toDtos(orders);
	}

	@Override
	@Transactional(readOnly = true)
	public SalesOrderPaymentsDto getPaymentsDto(Long orderId) {
		SalesOrder order = orderRepo.findOne(orderId);

		SalesOrderPaymentsDto paymentsDto = new SalesOrderPaymentsDto();
		paymentsDto.setOrderId(orderId);
		paymentsDto.setOrderNo(order.getOrderNo());
		paymentsDto.setOrderDate(order.getOrderDate());
		if (order.getCustomer() != null)
			paymentsDto.setCustomerName(order.getCustomer().getName());
		List<SalesOrderPaymentInfoDto> payments = SalesOrderPaymentInfoDto.toDtos(order.getPayments());
		if (CollectionUtils.isNotEmpty(payments))
			paymentsDto.setPayments(payments);

		return paymentsDto;
	}

	@Override
	@Transactional(readOnly = true)
	public SalesOrderPaymentInfoDto getPaymentsInfoDto(Long id) {
		return new SalesOrderPaymentInfoDto(repo.findOne(id));
	}

	@Override
	@Transactional
	public void savePayments(SalesOrderPaymentsDto paymentsDto) {
		Long orderId = paymentsDto.getOrderId();
		PaymentInfoStatus status = paymentsDto.getStatus();
		for (SalesOrderPaymentInfoDto paymentDto : paymentsDto.getPayments()) {
			paymentDto.setOrderId(orderId);
			paymentDto.setStatus(status);
			SalesOrderPaymentInfo payment = null;
			if (paymentDto.getId() != null)
				payment = repo.findOne(paymentDto.getId());
			repo.save(paymentDto.toModel(payment));
		}
	}

	@Override
	@Transactional
	public void processPaymentsForScndApproval(Long orderId) {
		SalesOrderDto order = orderService.getOrder(orderId);
		if (order.getOrderType().compareTo(SalesOrderType.B2B_SALES) == 0 || order.getOrderType().compareTo(SalesOrderType.B2B_ADV_SALES) == 0) {
			updateStatus(orderId, PaymentInfoStatus.SECOND_APPROVAL);
		} else if (order.getOrderType().compareTo(SalesOrderType.REPLACEMENT) == 0) {
			ReturnRecordDto returnDto = returnService.getReturnByRecordNo(order.getReturnNo());
			QSalesOrderPaymentInfo qPaymentInfo = QSalesOrderPaymentInfo.salesOrderPaymentInfo;
			Iterable<SalesOrderPaymentInfo> payments = repo.findAll(qPaymentInfo.order.id.eq(returnDto.getOrderId()));
			for (SalesOrderPaymentInfo payment : payments) {
				SalesOrderPaymentInfo newPayment = new SalesOrderPaymentInfo();
				newPayment.setOrder(new SalesOrder(orderId));
				newPayment.setPaymentAmount(payment.getPaymentAmount());
				newPayment.setPaymentDate(payment.getPaymentDate());
				newPayment.setPaymentDetails(payment.getPaymentDetails());
				newPayment.setPaymentType(payment.getPaymentType());
				newPayment.setStatus(PaymentInfoStatus.SECOND_APPROVAL);
				repo.save(newPayment);
			}
		}
	}

	@Override
	@Transactional
	public long updateStatus(Long orderId, PaymentInfoStatus status) {
		QSalesOrderPaymentInfo qPayment = QSalesOrderPaymentInfo.salesOrderPaymentInfo;
		HibernateUpdateClause clause = new HibernateUpdateClause(statelessSession, qPayment)
				.set(qPayment.status, status);
		Predicate exp = qPayment.order.id.eq(orderId);

		SalesOrderHistory soHistory = new SalesOrderHistory();
		soHistory.setOrder(orderRepo.findOne(orderId));
		soHistory.setStatus(SalesOrderStatus.DRAFT);
		soHistory.setCreated(new DateTime());
		soHistory.setCreateUser(UserUtil.getCurrentUser().getFullName());
		soHistoryRepo.save(soHistory);
		return clause.where(exp).execute();
	}

	@Override
	@Transactional
	public void createEmptyPayment(Long orderId, LocalDate orderDate) {
		SalesOrderPaymentInfo payment = new SalesOrderPaymentInfo();
		payment.setOrder(new SalesOrder(orderId));
		payment.setPaymentDate(orderDate);
		payment.setPaymentAmount(BigDecimal.ZERO);
		payment.setPaymentType(new LookupDetail(codePropertiesService.getDetailGcSoPaymentMethodCash()));
		payment.setStatus(PaymentInfoStatus.FIRST_APPROVAL);
		repo.save(payment);
	}

	@Override
	@Transactional
	public long updateStatus(String orderNo, PaymentInfoStatus status) {
		QSalesOrderPaymentInfo qPayment = QSalesOrderPaymentInfo.salesOrderPaymentInfo;
		HibernateUpdateClause clause = new HibernateUpdateClause(statelessSession, qPayment)
				.set(qPayment.status, status);
		Predicate exp = qPayment.order.orderNo.eq(orderNo);
		return clause.where(exp).execute();
	}

	@Override
	@Transactional
	public void processPayment(Long id, PaymentInfoStatus status) {
		SalesOrderPaymentInfo payment = repo.findOne(id);
		payment.setStatus(status);
		repo.save(payment);
	}

	@Override
	@Transactional
	public long updateStatus(Long[] ids, PaymentInfoStatus status) {
		QSalesOrderPaymentInfo qPaymentInfo = QSalesOrderPaymentInfo.salesOrderPaymentInfo;
		HibernateUpdateClause clause = new HibernateUpdateClause(statelessSession, qPaymentInfo)
				.set(qPaymentInfo.status, status);
		Predicate exp = qPaymentInfo.id.in(ids);
		return clause.where(exp).execute();
	}

	@Override
	@Transactional
	public long verifyPayments(Long[] ids) {
		QSalesOrderPaymentInfo qPaymentInfo = QSalesOrderPaymentInfo.salesOrderPaymentInfo;
		HibernateUpdateClause clause = new HibernateUpdateClause(statelessSession, qPaymentInfo)
				.set(qPaymentInfo.status, PaymentInfoStatus.VERIFIED)
				.set(qPaymentInfo.dateVerified, new LocalDate());
		Predicate exp = qPaymentInfo.id.in(ids)
				.and(qPaymentInfo.status.eq(PaymentInfoStatus.FIRST_APPROVAL));
		return clause.where(exp).execute();
	}

	@Override
	@Transactional
	public void approvePayments(Long[] ids) {
		QSalesOrderPaymentInfo qPaymentInfo = QSalesOrderPaymentInfo.salesOrderPaymentInfo;
		Predicate exp = qPaymentInfo.id.in(ids)
				.and(qPaymentInfo.status.eq(PaymentInfoStatus.SECOND_APPROVAL));
		List<SalesOrderDto> orders = SalesOrderDto.toDto(new JPAQuery(em).from(qPaymentInfo).where(exp).distinct().list(qPaymentInfo.order), true);
		for (SalesOrderDto orderDto : orders) {
			HibernateUpdateClause clause = new HibernateUpdateClause(statelessSession, qPaymentInfo)
					.set(qPaymentInfo.status, PaymentInfoStatus.APPROVED)
					.set(qPaymentInfo.dateApproved, new LocalDate());
			clause.where(qPaymentInfo.order.id.eq(orderDto.getId())).execute();

			BigDecimal shippingFee = orderDto.getShippingFee() != null ? orderDto.getShippingFee() : BigDecimal.ZERO;
			if (orderDto.getOrderType().compareTo(SalesOrderType.B2B_SALES) == 0) {
				accountingService.forB2BPayment(new DateTime(), orderDto.getTotalPayment().add(shippingFee), orderDto.getPaymentStore(), orderDto.getOrderNo());
			} else if (orderDto.getOrderType().compareTo(SalesOrderType.B2B_ADV_SALES) == 0) {
				accountingService.forAdvSoVerifApproval(new DateTime(), orderDto.getTotalPayment().add(shippingFee), orderDto.getOrderNo());
			} else if (orderDto.getOrderType().compareTo(SalesOrderType.REPLACEMENT) == 0) {
				accountingService.forReplacementPayment(new DateTime(), orderDto.getTotalPayment().add(shippingFee), orderDto.getOrderNo());
			}

		}

	}

	@Override
	public void generateAcctForPeoplesoft(Long[] ids) {
		QSalesOrderPaymentInfo qPaymentInfo = QSalesOrderPaymentInfo.salesOrderPaymentInfo;
		Predicate exp = qPaymentInfo.id.in(ids)
				.and(qPaymentInfo.status.eq(PaymentInfoStatus.VERIFIED))
				.and(qPaymentInfo.order.id.notIn(new JPASubQuery().from(qPaymentInfo).where(qPaymentInfo.status.eq(PaymentInfoStatus.FIRST_APPROVAL)).distinct().list(qPaymentInfo.order.id)));
		List<SalesOrderDto> orders = SalesOrderDto.toDto(new JPAQuery(em).from(qPaymentInfo).where(exp).distinct().list(qPaymentInfo.order), true);
		for (SalesOrderDto orderDto : orders) {
			if (orderDto.isAdvSo())
				accountingService.forAdvSoVerif(new DateTime(), orderDto.getTotalPayment(), orderDto.getCustomerDesc(), orderDto.getPaymentStore());
		}
	}

	@Override
	public JRProcessor createJrProcessor(SalesOrderPaymentSearchDto searchDto) {
		Map<String, Object> parameters = Maps.newHashMap();
		List<SalesOrderPaymentInfoDto> products = getSalesOrdersPaymentForPrint(searchDto);

		DefaultJRProcessor jrProcessor = new DefaultJRProcessor("reports/sales_order_payment_report.jasper", products);
		jrProcessor.addParameters(parameters);
		jrProcessor.setFileResolver(new ClasspathFileResolver("reports"));
		return jrProcessor;
	}
}
