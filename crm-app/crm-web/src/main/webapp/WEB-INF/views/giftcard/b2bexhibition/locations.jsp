<%@ include file="../../common/taglibs.jsp"%>

<style type="text/css">
<!--
  .well .input {
    background-color: #fff;
    border: 1px solid #E8E8E8;
    box-shadow: none;    
    padding: 6px;
    height: 18px;
    margin-left: 5px;
  }
  .well .btn-primary {
    margin-left: 10px;
  }
  .well select {
    margin: 5px;
  }
  #dateFrom {
    margin-right: 5px;
  }
  
-->
</style>

    <c:set var="delete_confirm_msg">
        <spring:escapeBody javaScriptEscape="true">
            <spring:message code="entity_delete_confirm" />
        </spring:escapeBody>
    </c:set>

  <div class="page-header page-header2">
      <h1><spring:message code="b2b.location.discount.matrix" /></h1>
  </div>
  
  <div id="form_searchForm" class="well">
    <div class="form-inline form-search">
    	<label class="label-single"><spring:message code="label_filter" />:</label>
        <input type="text" name="locationName" class="input searchField" placeholder="<spring:message code="b2b.location.name" />"/>
        <input type="text" name="startDate" id="dateFrom" class="input searchField" placeholder="<spring:message code="b2b.location.startdate" />"/>
        <input type="text" name="endDate" id="dateTo" class="input searchField" placeholder="<spring:message code="b2b.location.enddate" />"/>
        <input id="search" type="button" class="btn btn-primary" value="<spring:message code="button_search"/>"/>
        <input type="button" id="clear" value="<spring:message code="label_clear" />" class="btn custom-reset" data-form-id="form_searchForm" data-default-id="search" />
    </div>
  </div>
  
  
<div class="pull-right mb20">
<button type="button" class="btn btn-primary" id="createLocation"><spring:message code="b2b.location.create" /></button>
</div>

<div id="content_locationList">
  <div id="list_location"></div>
</div>


<div class="clearfix"></div>

<div id="discountMatrixContainer" class="hide">

<br/><br/>
<fieldset>
<legend class="block-stack-title"><span id="locationName"></span> <spring:message code="b2b.location.discountmatrix" /></legend>
<input type="hidden" name="locationId" id="locationId" />

<div class="clearfix"></div>
<button type="button" id="addDisc" class="btn btn-default pull-right"><spring:message code="b2b.discount.create"/></button><br/><br/>
<div class="clearfix"></div>

<div id="content_discountList">
  <div id="list_discount"></div>
</div>

</fieldset>

</div>

<div class="modal hide nofly" id="locationDialog" tabindex="-1" role="dialog" aria-hidden="true">
</div>

  <link href="<spring:url value="/css/dataTables.css" />" rel="stylesheet">
  <script src="<spring:url value="/js/jquery/jquery.dataTables.js" />" ></script>
  <script src="<spring:url value="/js/jquery/jquery.dataTables.linkLengthMenu.js" />"></script>
  <script src="<spring:url value="/js/jquery/jquery.dataTables.customPaging.js" />"></script>
  <script src="<spring:url value="/js/jquery/ajax-datatable.js" />" ></script>
  <script src="<spring:url value="/js/common/form2js.js" />" ></script>
  <script src="<spring:url value="/js/common/json2.js" />" ></script>
  <script src="<spring:url value="/js/jquery/jquery.toObject.js" />" ></script>
  <link href="<c:url value="/styles/temp.css" />" rel="stylesheet" />
  
  <script src="<spring:url value="/js/jquery/jquery.numberInput.js" />" ></script>
  <script src="<c:url value="/js/bootstrap/bootstrap-datepicker.js"/>" type='text/javascript'><![CDATA[&nbsp; ]]></script>
  <link href="<c:url value="/css/bootstrap/datepicker.css" />" rel="stylesheet" />
  <link href="<c:url value="/styles/temp.css" />" rel="stylesheet" />
  <script src="<spring:url value="/js/jquery/jquery.bindWithDelay.js" />"></script>
  <script src="<spring:url value="/js/jquery/jquery.ajaxQueue.js" />"></script>
  
  <jsp:include page="../../common/confirm.jsp" />
  
<script type="text/javascript">

var CONTAINER_ERROR = "#contentError > div";
var CONTENT_ERROR = "#contentError";

$(document).ready(function() {
	
	$searchForm = $("#form_searchForm");
	
	$locationDialog = $("#locationDialog").modal({
	    show: false
    }).on('hidden.bs.modal', function() {
      	$(this).empty();
    }).css({
      	"width": "500px",
    });
	
	$("#list_discount").ajaxDataTable({
	      'autoload'  : false,
	      'ajaxSource' : "<c:url value="/giftcard/b2b/location/discounts/list" />",
	      'columnHeaders' : [
	      "<spring:message code="b2b.location.minamount"/>",
	      "<spring:message code="b2b.location.maxamount"/>",
	      "<spring:message code="b2b.location.rate"/>",
	      ""
	      ],
	      'modelFields' : [
	         {name : 'minAmount', sortable : true, fieldCellRenderer : function (data, type, row) {
	             return (row.minAmount? toCurrency(row.minAmount) : row.minAmount);
	         }},
	         {name : 'maxAmount', sortable : true, fieldCellRenderer : function (data, type, row) {
	             return (row.maxAmount? toCurrency(row.maxAmount) : row.maxAmount);
	         }},
	         {name : 'discount', sortable : true},
	         {customCell : function ( innerData, sSpecific, json ) {
	              if ( sSpecific == 'display') {
	            	  var html = "";
	            	  
	            	  html += "<button class=\"updateDisc btn btn-primary pull-left tiptip icn-edit\" data-id=\""+json.id+"\" data-loc-id=\""+json.locId+"\" title=\"<spring:message code="label_update"/>\"></button>";
	            	  html += "<button class=\"deleteDisc btn btn-primary pull-left tiptip icn-delete\" data-id=\""+json.id+"\" title=\"<spring:message code="label_delete"/>\" ></button>";
	            	  
	            	  return html;
	            	  
	              } else {
	                return "";
	              }
	         }
	         }
	       ]
	}).on("click", ".updateDisc", function() {
    	$.get("<c:url value="/giftcard/b2b/location/discounts/form/" />" + $(this).data("id") + "/" + $(this).data("locId"), function(data) {
    		$locationDialog.modal("show").html(data);
        }, "html");
    }).on("click", ".deleteDisc", function() {
    	var url = "<c:url value="/giftcard/b2b/location/discounts/" />" + $(this).data("id");
    	getConfirm('${delete_confirm_msg}',function(result) {
    		if(result) {
    			$.post(url, {_method: "DELETE"}, function(data) {
    				location.reload();
    			  });	
    		}
    	});
    	
    });
	
	$("#list_location").ajaxDataTable({
	      'autoload'  : true,
	      'ajaxSource' : "<c:url value="/giftcard/b2b/location/list" />",
	      'columnHeaders' : [
	      "<spring:message code="b2b.location.name"/>",
	      "<spring:message code="b2b.location.address"/>",
	      "<spring:message code="b2b.location.startdate"/>",
	      "<spring:message code="b2b.location.enddate"/>",
	      ""
	      ],
	      'modelFields' : [
	         {name : 'name', sortable : true},
	         {name : 'address', sortable : true},
	         {name : 'startDate', sortable : true, dataType: "DATE"},
	         {name : 'endDate', sortable : true, dataType: "DATE"},
	         {customCell : function ( innerData, sSpecific, json ) {
	              if ( sSpecific == 'display') {
	            	  var html = "";
	            	  
	            	  html += "<button class=\"updateLocation btn btn-primary pull-left tiptip icn-edit\" data-id=\""+json.id+"\" title=\"<spring:message code="label_update"/>\"></button>";
	            	  html += "<button class=\"listDiscounts btn btn-primary pull-left tiptip icn-view\" data-name=\""+json.name+"\" data-id=\""+json.id+"\" title=\"<spring:message code="b2b.location.discountmatrix"/>\"></button>";
	            	  html += "<button class=\"deleteLocation btn btn-primary pull-left tiptip icn-delete\" data-id=\""+json.id+"\" title=\"<spring:message code="label_delete"/>\" ></button>";
	            	  
	            	  return html;
	            	  
	              } else {
	                return "";
	              }
	         }
	         }
	       ]
	}).on("click", ".updateLocation", function() {
    	$.get("<c:url value="/giftcard/b2b/location/form/" />" + $(this).data("id"), function(data) {
    		$locationDialog.modal("show").html(data);
        }, "html");
    }).on("click", ".deleteLocation", function() {
    	var url = "<c:url value="/giftcard/b2b/location/" />" + $(this).data("id");
    	getConfirm('${delete_confirm_msg}',function(result) {
    		if(result) {
    			$.post(url, {_method: "DELETE"}, function(data) {
    				location.reload();
    			  });	
    		}
    	});
    	
    }).on("click", ".listDiscounts", function() {
    	var formDataObj = new Object();
    	formDataObj.locId = $(this).data("id");
		$("#list_discount").ajaxDataTable('search', formDataObj);
		
		$("#locationName").text($(this).data("name"));
		$("#locationId").val($(this).data("id"));
		
		$("#discountMatrixContainer").show();
    });
	
	$("#search", $searchForm).click(function() {
		var formDataObj = $searchForm.toObject( { mode : 'first', skipEmpty : true } );
		$("#list_location").ajaxDataTable('search', formDataObj);
	});
	
	$("#dateFrom").datepicker({
    	autoclose : true,
		format: 'dd-mm-yyyy'
	}).on('changeDate', function(selected){
		startDate = new Date(selected.date.valueOf());
        startDate.setDate(startDate.getDate(new Date(selected.date.valueOf())));
        $('#dateTo').datepicker('setStartDate', startDate);
	});
	$("#dateTo").datepicker({
	  	autoclose : true,
		format: 'dd-mm-yyyy'
	}).on('changeDate', function(selected){
		FromEndDate = new Date(selected.date.valueOf());
        FromEndDate.setDate(FromEndDate.getDate(new Date(selected.date.valueOf())));
        $('#dateFrom').datepicker('setEndDate', FromEndDate);
	});
	
	$("#clear", $searchForm).click(function() {
		$(".searchField", $searchForm).val("");
	});
	
	$("#addDisc").click(function() {
		var locationId = $("#locationId").val(); 
		$.get("<c:url value="/giftcard/b2b/location/discounts/form/null/" />" + locationId, function(data) {
	      $locationDialog.modal("show").html(data);
	    }, "html");
	});
	
	$("#createLocation").click(function() {
		$.get("<c:url value="/giftcard/b2b/location/form/null" />", function(data) {
	      $locationDialog.modal("show").html(data);
	    }, "html");
	});
	
	function toCurrency(total) {
		var currency = parseFloat(total, 10).toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, "$1,").toString();
		return currency.slice( 0,-3 );
	}
	
});

</script>