<%@ include file="../taglibs.jsp"%>


<%@ include file="segments/accountData.jsp" %>
<%@ include file="segments/personalData.jsp" %>
<%@ include file="segments/occupationData.jsp" %>
<%@ include file="segments/correspondenceData.jsp" %>
<%@ include file="segments/marketingData.jsp" %>

    


    
    <c:if test="${isPrimary && form.memberTypeCode != 'MTYP002'}">
    <div class="tab-pane clearfix" id="cards">
    </div>
    </c:if>

    <div class="form-actions clearfix">
      <button type="submit" class="btn btn-primary">
        <spring:message code="button_submit" />
      </button>
      <a class="btn" href="<spring:url value="/"/>"><spring:message
          code="button_cancel" /></a>
    </div>
