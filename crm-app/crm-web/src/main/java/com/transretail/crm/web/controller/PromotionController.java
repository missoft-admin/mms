package com.transretail.crm.web.controller;


import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletResponse;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.joda.time.DateTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.transretail.crm.common.service.dto.response.ResultList;
import com.transretail.crm.common.util.DateUtil;
import com.transretail.crm.common.web.notification.NotificationType;
import com.transretail.crm.common.web.notification.annotation.Notifiable;
import com.transretail.crm.common.web.notification.annotation.NotificationUpdate;
import com.transretail.crm.core.dto.ApprovalRemarkDto;
import com.transretail.crm.core.dto.CampaignDto;
import com.transretail.crm.core.dto.MemberDto;
import com.transretail.crm.core.dto.OverlappingPromoDto;
import com.transretail.crm.core.dto.ProgramDto;
import com.transretail.crm.core.dto.PromotionDto;
import com.transretail.crm.core.dto.PromotionResultList;
import com.transretail.crm.core.dto.PromotionRewardDto;
import com.transretail.crm.core.dto.PromotionSearchDto;
import com.transretail.crm.core.entity.MemberGroup;
import com.transretail.crm.core.entity.ProductGroup;
import com.transretail.crm.core.entity.enums.AppKey;
import com.transretail.crm.core.entity.enums.PromoModelType;
import com.transretail.crm.core.entity.lookup.LookupDetail;
import com.transretail.crm.core.entity.lookup.LookupHeader;
import com.transretail.crm.core.entity.lookup.StoreGroup;
import com.transretail.crm.core.repo.PromotionRepo;
import com.transretail.crm.core.security.model.impl.CustomSecurityUserDetailsImpl;
import com.transretail.crm.core.security.util.UserUtil;
import com.transretail.crm.core.service.CodePropertiesService;
import com.transretail.crm.core.service.LookupService;
import com.transretail.crm.core.service.MemberGroupService;
import com.transretail.crm.core.service.MemberService;
import com.transretail.crm.core.service.OverlappingPromoService;
import com.transretail.crm.core.service.ProductGroupService;
import com.transretail.crm.core.service.ProductService;
import com.transretail.crm.core.service.PromoParticipantsService;
import com.transretail.crm.core.service.PromotionService;
import com.transretail.crm.core.service.StampPromoService;
import com.transretail.crm.core.service.StoreGroupService;
import com.transretail.crm.web.controller.util.ControllerResponse;
//import com.transretail.crm.web.notifications.Notifiable;
//import com.transretail.crm.web.notifications.NotificationUpdate;


@RequestMapping("/promotion")
@Controller
public class PromotionController extends AbstractReportsController {

	@Autowired
    PromotionService promotionService;
	@Autowired
    LookupService lookupService;
	@Autowired
	CodePropertiesService codePropertiesService;	
	@Autowired
	MemberGroupService memberGroupService;	
	@Autowired
	StoreGroupService storeGroupService;	
	@Autowired
	ProductGroupService productGroupService;	
	@Autowired
	ProductService productService;
	@Autowired
	MemberService memberService;
	@Autowired
	StampPromoService stampPromoService;
	@Autowired
    private OverlappingPromoService overlappingPromoService;
    @Autowired
    private MessageSource messageSource;
    @Autowired
    private PromoParticipantsService participantsService;



	protected static final String PATH_MAIN 	          = "/promotion";
	protected static final String PATH_LIST 	          = "/promotion/list";
	protected static final String PATH_CREATE  	      = "/promotion/create";
	protected static final String PATH_VIEW	  	      = "/promotion/view";
	protected static final String PATH_PROCESS  	      = "/promotion/process";

	protected static final String UIATTR_PROMOPROGRAMS  = "promotionPrograms";
	protected static final String UIATTR_PROMOCAMPAIGNS = "promotionCampaigns";
	protected static final String UIATTR_PROMODAYS 	  = "promotionAffectedDays";
	protected static final String UIATTR_REWARDTYPES 	  = "promotionRewardTypes";
	protected static final String UIATTR_PROMOREMARKS	  = "promotionRemarks";

	protected static final String UIATTR_REWARDPOINTS   = "rewardPoints";
	protected static final String UIATTR_REWARDDISCOUNT = "rewardDiscount";
	protected static final String UIATTR_REWARDITEMPT   = "rewardItemPoint";
	protected static final String UIATTR_REWARDSELTYPE  = "rewardSelType";

	protected static final String UIATTR_SEARCHTYPES 	  = "promotionModelTypes";
	protected static final String UIATTR_SEARCHFIELDS	  = "promotionFields";

	protected static final String UIATTR_PROGRAM	  	  = "program";

	protected static final String UIATTR_REMARKSTATUS	  = "promotionStatuses";

	protected static final String UIATTR_MEMBER_GROUPS 	    = "memberGroups";
	protected static final String UIATTR_STORE_GROUPS 	    = "storeGroups";
	protected static final String UIATTR_PRODUCT_GROUPS 	    = "productGroups";
	protected static final String UIATTR_PRODUCTS 	  	    = "products";
	protected static final String UIATTR_CLASSIFICATIONS 	    = "classifications";
	protected static final String UIATTR_SUBCLASSIFICATIONS   = "subclassifications";
	protected static final String UIATTR_PAYMENT_TYPES 	    = "paymentTypes";
	protected static final String UIATTR_PRODUCT_GROUP_SEARCH = "searchProductGroupForm";
	protected static final String UIATTR_EFT_CODE 			= "eftCode";

	/*private static final String UIATTR_PRDCFN_DIVISION		= "division";
	private static final String UIATTR_PRDCFN_DEPARTMENT	= "department";
	private static final String UIATTR_PRDCFN_GROUP			= "group";
	private static final String UIATTR_PRDCFN_FAMILY		= "family";
	private static final String UIATTR_PRDCFN_SUBFAMILY		= "subfamily";*/

	private static final String UIATTR_SEARCH_STAT_ALL	 	= "all";
	private static final String UIATTR_SEARCH_STAT_PROCESS 	= "forProccessing";
	private static final String UIATTR_SEARCH_STAT_MINE 	= "mine";

	protected static final String UIATTR_MKTHEAD			 	= "isMarketingHead";



	@InitBinder
	void initBinder(WebDataBinder binder) {
		DateFormat theFormat = new SimpleDateFormat( "dd MMM yyyy" );
		theFormat.setLenient( false );
		binder.registerCustomEditor( Date.class, new CustomDateEditor( theFormat, true ) );

		theFormat = new SimpleDateFormat( "dd MMM yyyy" );
		theFormat.setLenient( false );
		binder.registerCustomEditor( DateTime.class, new CustomDateEditor( theFormat, true ) );
	}



    @RequestMapping( value = "/search", produces = "text/html", method=RequestMethod.GET )
    public String searchPromotions( 
    		@ModelAttribute(value="promotionSearch") PromotionSearchDto inPromotion,
    		Model inUiModel ) {

    	inUiModel.addAttribute( UIATTR_PROMOPROGRAMS, promotionService.processSearch( inPromotion ) );
    	return PATH_LIST;
    }

    @RequestMapping( produces = "text/html" )
    public String show(
            @RequestParam(value = "page", required = false) Integer page,
            @RequestParam(value = "size", required = false) Integer size,
            Model inUiModel ) {

    	populateSearch( inUiModel );
    	populateStatusTypes( inUiModel );
    	PromotionSearchDto theSearchDto = new PromotionSearchDto();
    	theSearchDto.setModelType( PromoModelType.PROMOTION.toString() );
    	theSearchDto.setStatus( codePropertiesService.getDetailStatusActive() );
    	inUiModel.addAttribute( UIATTR_PROMOPROGRAMS, promotionService.processSearch( theSearchDto ) );
    	
    	

        return PATH_MAIN;
    }

    @RequestMapping( value = { "/{status}" }, produces = "text/html" )
    public String show(
			@PathVariable(value = "status") String inStatus,
            @RequestParam(value = "page", required = false) Integer page,
            @RequestParam(value = "size", required = false) Integer size,
            Model inUiModel ) {

    	populateSearch( inUiModel );
    	populateStatusTypes( inUiModel );
    	if ( StringUtils.isNotBlank( inStatus ) ) {
    		if ( inStatus.equalsIgnoreCase( UIATTR_SEARCH_STAT_ALL ) ) {
            	PromotionSearchDto theSearchDto = new PromotionSearchDto();
            	theSearchDto.setModelType( PromoModelType.PROMOTION.toString() );
    			/*inUiModel.addAttribute( UIATTR_PROMOPROGRAMS, promotionService.getProgramDtos() );*/
            	inUiModel.addAttribute( UIATTR_PROMOPROGRAMS, promotionService.processSearch( theSearchDto ) );
    		}
    		else if ( inStatus.equalsIgnoreCase( UIATTR_SEARCH_STAT_PROCESS ) ) {
            	PromotionSearchDto theSearchDto = new PromotionSearchDto();
            	theSearchDto.setModelType( PromoModelType.PROMOTION.toString() );
            	List<String> theStatuses = new ArrayList<String>();
            	//theStatuses.add( codePropertiesService.getDetailStatusApproval() );
            	theStatuses.add( codePropertiesService.getDetailStatusDraft() );
            	theSearchDto.setStatuses( theStatuses );
            	inUiModel.addAttribute( UIATTR_PROMOPROGRAMS, promotionService.processSearch( theSearchDto ) );
    		}
    		else if ( inStatus.equalsIgnoreCase( UIATTR_SEARCH_STAT_MINE ) ) {
            	PromotionSearchDto theSearchDto = new PromotionSearchDto();
            	theSearchDto.setModelType( PromoModelType.PROMOTION.toString() );
            	theSearchDto.setCreatedBy( UserUtil.getCurrentUser().getUsername() );
            	inUiModel.addAttribute( UIATTR_PROMOPROGRAMS, promotionService.processSearch( theSearchDto ) );
    		}
    	}

        return PATH_MAIN;
    }
    
    static Logger logger = LoggerFactory.getLogger(PromotionController.class);

    @RequestMapping(value="/view/{id}", produces="text/html")
    public String show(@PathVariable String id, Model uiModel) {
    	populateSearch( uiModel );
    	populateStatusTypes( uiModel );
    	PromotionSearchDto search = new PromotionSearchDto();
    	search.setId(Long.valueOf(id));
    	search.setModelType( PromoModelType.PROMOTION.toString() );
    	List<ProgramDto> list = promotionService.processSearch(search);
    	uiModel.addAttribute(UIATTR_PROMOPROGRAMS, list);
    	for(ProgramDto program : list) {
    		for(CampaignDto campaign : program.getCampaigns()) {
    			logger.debug("promotion count: " + campaign.getPromotions().size());
    			for(PromotionDto promotion : campaign.getPromotions()) {
    				logger.debug("promotion id: " + promotion.getId());
    			}
    		}
    	}
    	return PATH_MAIN;
    }
    
    @RequestMapping( value="/promoparticipants/validate/{id}", produces="application/json; charset=utf-8")
    public @ResponseBody ControllerResponse validatePromoParticipants(@PathVariable Long id) {
    	ControllerResponse response = new ControllerResponse();
    	response.setSuccess(participantsService.isExistParticipants(id));
    	return response;    	
    }
    
    @RequestMapping(value="/promoparticipants/{id}")
    public void printPromoParticipants(@PathVariable Long id, HttpServletResponse res) throws Exception {
    	renderExcelReport(res, participantsService.createJrProcessor(id), "promo-participants-" + id);	
    }
    
    @RequestMapping( value = "/list/{campaign}", method = RequestMethod.POST )
    public @ResponseBody PromotionResultList listPromotions( 
			@PathVariable(value = "campaign") Long inCampaignId,
			@RequestBody PromotionSearchDto inSearchForm) {

    	inSearchForm.setCampaign( inCampaignId );
    	return promotionService.searchPromotion( inSearchForm );
    }

	@RequestMapping( value="/create", produces = "text/html" )
    public String createPromotion(
    		@ModelAttribute(value="promotion") PromotionDto inPromotion,
            Model inUiModel ) {

		populateFormSelects( inUiModel, inPromotion );
    	populateTargets( inUiModel );
		populateOtherForms( inUiModel );
		inUiModel.addAttribute( "promotionFormAction", "/promotion/save" );
        return PATH_CREATE;
    }

	@RequestMapping( value="/edit/{id}", produces = "text/html" )
    public String editPromotion(
			@PathVariable(value = "id") Long inId,
    		@ModelAttribute(value="promotion") PromotionDto inPromotion,
            Model inUiModel ) {

		PromotionDto dto = promotionService.getPromotionDto( inId );
		inUiModel.addAttribute( "promotion", dto );
		populateFormSelects( inUiModel, inPromotion );
    	populateTargets( inUiModel );
		populateOtherForms( inUiModel );
		populateRewardTypes( inUiModel, dto.getRewardType() );
		inUiModel.addAttribute( UIATTR_PROMOCAMPAIGNS,
				( null != dto.getProgram() )?
						promotionService.getCampaignDtos( dto.getProgram() ) : null );
		inUiModel.addAttribute( "promotionFormAction", "/promotion/update" );

		CustomSecurityUserDetailsImpl user = UserUtil.getCurrentUser();
		String mktHeadCode = codePropertiesService.getMktHeadCode();
		inUiModel.addAttribute( UIATTR_MKTHEAD, user.hasAuthority( mktHeadCode ) );
		populateApprovalStatuses( inUiModel );
		if ( ( StringUtils.equalsIgnoreCase( dto.getCreateUser(), user.getUsername() )  || user.hasAuthority( mktHeadCode ) ) 
				&& ( StringUtils.isNotBlank( dto.getStatus() ) && !dto.getStatus().equalsIgnoreCase( codePropertiesService.getDetailStatusActive() ) ) ) {
			return PATH_CREATE;
		}

        return PATH_VIEW;
    }



    @Notifiable(type=NotificationType.PROMOTION)
	@RequestMapping( value={"/save"}, produces="application/json; charset=utf-8", method=RequestMethod.POST )
    public @ResponseBody ControllerResponse savePromotion(
    		@ModelAttribute(value="promotion") PromotionDto inPromotion,
    		BindingResult inBinding ) {

		ControllerResponse theResp = new ControllerResponse();

    	if ( isValid( inPromotion, inBinding ) ) {
    		promotionService.savePromotionDto( inPromotion );
    		theResp.setSuccess( true );
    		theResp.setResult( inPromotion );
    		theResp.setModelId(inPromotion.getId().toString());
    		theResp.setNotificationCreation(true);
    	}
    	else {
    		theResp.setSuccess( false );
    		theResp.setResult( inBinding.getAllErrors() );
    	}

		return theResp;
    }

    @NotificationUpdate(type=NotificationType.PROMOTION)
	@RequestMapping( value={"/update"}, produces="application/json; charset=utf-8", method=RequestMethod.POST )
    public @ResponseBody ControllerResponse updatePromotion(
    		@ModelAttribute(value="promotion") PromotionDto inPromotion,
    		BindingResult inBinding ) {

		ControllerResponse theResp = new ControllerResponse();

		if ( null != inPromotion.getRemarks() && 
				inPromotion.getRemarks().getStatus().equalsIgnoreCase( codePropertiesService.getDetailStatusActive() ) ) {
			if ( !stampPromoService.isPromoForApproval( AppKey.PORTA_URL_STAMP_CREATE, inPromotion.getId() ) ) {
				inBinding.reject( messageSource.getMessage( "promo.stamp.err.notconfigured.createpromo", null, LocaleContextHolder.getLocale() ) );
			}
			
		}

		if ( null != inPromotion.getRemarks() && 
				inPromotion.getRemarks().getStatus().equalsIgnoreCase( codePropertiesService.getDetailStatusDisabled() ) ) {
			promotionService.saveRemarkDto( inPromotion.getRemarks() );
    		theResp.setSuccess( true );
    		theResp.setResult( inPromotion );
		}
		else if ( isValid( inPromotion, inBinding ) ) {
    		promotionService.updatePromotionDto( inPromotion );
			promotionService.saveRemarkDto(inPromotion.getRemarks() );
    		theResp.setSuccess( true );
    		theResp.setResult( inPromotion );
    		theResp.setModelId(inPromotion.getId().toString());
    	}
    	else {
    		theResp.setSuccess( false );
    		theResp.setResult( inBinding.getAllErrors() );
    	}

		return theResp;
    }



	@RequestMapping( value="/process/{id}", produces="text/html", method=RequestMethod.GET )
	public String createRemarks(
			@PathVariable(value = "id") Long inId,
			@ModelAttribute(value="approvalRemark") ApprovalRemarkDto inApprovalRemark,
    		BindingResult inBinding,
            Model inUiModel ) {

		if ( null != inId ) {
			inApprovalRemark.setModelId( inId.toString() );
		}
		populateApprovalStatuses( inUiModel );
		inUiModel.addAttribute( "promotion", promotionService.getPromotionDto( inId ) );
		inUiModel.addAttribute( UIATTR_PROMOREMARKS, promotionService.getRemarkDtos( inId ) );
		return PATH_PROCESS;
	}

	@NotificationUpdate(type=NotificationType.PROMOTION)
	@RequestMapping( value="/process/save", produces="application/json; charset=utf-8", method=RequestMethod.POST )
	public @ResponseBody ControllerResponse saveRemarks(
			@ModelAttribute(value="approvalRemark") ApprovalRemarkDto inApprovalRemark,
    		BindingResult inBinding ) {

		ControllerResponse theResp = new ControllerResponse();

		if ( !inBinding.hasErrors() ) {
			promotionService.saveRemarkDto( inApprovalRemark );
			theResp.setSuccess( true );
			theResp.setResult( inApprovalRemark );
			theResp.setModelId(inApprovalRemark.getModelId());
		}
		
		return theResp;
	}



	@RequestMapping( value="/target/member/count/{id}", produces="application/json; charset=utf-8", method=RequestMethod.GET )
	public @ResponseBody ControllerResponse getMemberCount( @PathVariable(value = "id") Long id ) {
		ControllerResponse resp = new ControllerResponse();
		resp.setResult( promotionService.countTargetMembers(id) );
		resp.setSuccess(true);
		return resp;
	}

	@RequestMapping( value="/target/member/target/{id}", produces="application/json; charset=utf-8", method=RequestMethod.POST )
	public @ResponseBody ResultList<MemberDto> getMemberTarget( 
			@PathVariable(value = "id") Long id,
			@RequestBody PromotionSearchDto searchDto ) {

		searchDto.setId( id );
		return promotionService.getTargetMembers( searchDto );
	}



	private void populateSearch( Model inUiModel ) {
    	inUiModel.addAttribute( UIATTR_SEARCHTYPES, PromoModelType.values() );
    	inUiModel.addAttribute( UIATTR_SEARCHFIELDS, new String[]{ "name", "description" } );
    
    	List<LookupDetail> statuses = new ArrayList<LookupDetail>();
    	//statuses.add(lookupService.getDetailByCode(codePropertiesService.getDetailStatusApproval()));
    	statuses.add(lookupService.getDetailByCode(codePropertiesService.getDetailStatusDraft()));
    	statuses.add(lookupService.getDetailByCode(codePropertiesService.getDetailStatusActive()));
    	statuses.add(lookupService.getDetailByCode(codePropertiesService.getDetailStatusRejected()));
    	statuses.add(lookupService.getDetailByCode(codePropertiesService.getDetailStatusDisabled()));
    	inUiModel.addAttribute("statuses", statuses);
    	
    	List<LookupDetail> rewardTypes = lookupService.getActiveDetailsByHeaderCode(codePropertiesService.getHeaderRewardType());
    	inUiModel.addAttribute("rewardTypes", rewardTypes);
	}

	protected void populateTargets(Model uiModel) {
		Map<Long, String> targetMember = new HashMap<Long, String>();
		if(CollectionUtils.isNotEmpty(memberGroupService.getAllMemberGroups())) {
			for(MemberGroup group : memberGroupService.getAllMemberGroups())
				targetMember.put(group.getId(), group.getName());	
		}
    	uiModel.addAttribute(UIATTR_MEMBER_GROUPS, targetMember);
    	
    	Map<Long, String> targetStore = new HashMap<Long, String>();
    	if(CollectionUtils.isNotEmpty(storeGroupService.getAllGroups())) {
			for(StoreGroup group : storeGroupService.getAllGroups())
				targetStore.put(group.getId(), group.getName());
    	}
    	uiModel.addAttribute(UIATTR_STORE_GROUPS, targetStore);
    	
    	Map<Long, String> targetProduct = new HashMap<Long, String>();
    	if(CollectionUtils.isNotEmpty(productGroupService.getAllGroups())) {
			for(ProductGroup group : productGroupService.getAllGroups())
				targetProduct.put(group.getId(), group.getName());
    	}
    	uiModel.addAttribute(UIATTR_PRODUCT_GROUPS, targetProduct);
    	
    	Map<String, String> targetPayment = new HashMap<String, String>();
		for(LookupDetail detail : lookupService.getActiveDetails(new LookupHeader(codePropertiesService.getHeaderPaymentType())))
			targetPayment.put(detail.getCode(), detail.getDescription());
    	uiModel.addAttribute(UIATTR_PAYMENT_TYPES, targetPayment);
    	
    	Map<String, String> targetPaymentWildcards = new HashMap<String, String>();
		for(LookupDetail detail : lookupService.getActiveDetails(new LookupHeader(codePropertiesService.getHeaderPaymentTypeWildcard())))
			targetPaymentWildcards.put(detail.getCode(), detail.getDescription());
    	uiModel.addAttribute("paymentTypesWilcard", targetPaymentWildcards);

		uiModel.addAttribute(UIATTR_EFT_CODE, codePropertiesService.getDetailPaymentTypeEft());

		/*uiModel.addAttribute(UIATTR_PRODUCTS, productService.getAllProducts());
		uiModel.addAttribute(UIATTR_CLASSIFICATIONS, lookupService.getActiveDetails(new LookupHeader(codePropertiesService.getHeaderProductGrpClassification())));
		uiModel.addAttribute(UIATTR_SUBCLASSIFICATIONS, lookupService.getActiveDetails(new LookupHeader(codePropertiesService.getHeaderProductGrpSubclassification())));
		uiModel.addAttribute(UIATTR_PRODUCT_GROUP_SEARCH, new ProductGroup());

		uiModel.addAttribute( UIATTR_PRDCFN_DIVISION, codePropertiesService.getDetailProductCfnDivision() );
		uiModel.addAttribute( UIATTR_PRDCFN_DEPARTMENT, codePropertiesService.getDetailProductCfnDepartment() );
		uiModel.addAttribute( UIATTR_PRDCFN_GROUP, codePropertiesService.getDetailProductCfnGroup() );
		uiModel.addAttribute( UIATTR_PRDCFN_FAMILY, codePropertiesService.getDetailProductCfnFamily() );
		uiModel.addAttribute( UIATTR_PRDCFN_SUBFAMILY, codePropertiesService.getDetailProductCfnSubfamily() );*/
    }

	protected void populateFormSelects( Model inUiModel, PromotionDto inDto ) {
		List<ProgramDto> thePrograms = promotionService.getProgramDtos();
		inUiModel.addAttribute( UIATTR_PROMOPROGRAMS, thePrograms );
		List<CampaignDto> theCampaigns = CollectionUtils.isNotEmpty( thePrograms )?
				promotionService.getCampaignDtos( thePrograms.get(0).getId() ) : null;
		inUiModel.addAttribute( UIATTR_PROMOCAMPAIGNS, theCampaigns );
		if ( CollectionUtils.isNotEmpty( theCampaigns ) ) {
			if ( null == inDto.getStartDate() && null !=  theCampaigns.get(0).getStartDate() ) {
				inDto.setStartDate( new Date( theCampaigns.get(0).getStartDate() ) );
			}
			if ( null == inDto.getEndDate() && null != theCampaigns.get(0).getEndDate() ) {
				inDto.setEndDate( new Date( theCampaigns.get(0).getEndDate() ) );
			}
		}
		/*List<LookupDetail> rwdTypes = lookupService.getActiveDetails( 
				new LookupHeader( codePropertiesService.getHeaderRewardType() ) );*/
		List<LookupDetail> rwdTypes = new ArrayList<LookupDetail>();
		rwdTypes.add( lookupService.getDetailByCode( codePropertiesService.getDetailRewardTypePoints() ) );
		rwdTypes.add( lookupService.getDetailByCode( codePropertiesService.getDetailRewardTypeItemPoint() ) );
    	inUiModel.addAttribute( UIATTR_REWARDTYPES, rwdTypes );
    	inUiModel.addAttribute( UIATTR_PROMODAYS, 
    			lookupService.getActiveDetails( new LookupHeader( codePropertiesService.getHeaderDays() ) ) );
		
	}

	protected void populateOtherForms( Model inUiModel ) {
		inUiModel.addAttribute( UIATTR_PROGRAM, new ProgramDto() );
	}

	private void populateRewardTypes( Model inUiModel, String inType ) {
    	inUiModel.addAttribute( UIATTR_REWARDSELTYPE, inType );
		inUiModel.addAttribute( UIATTR_REWARDPOINTS, codePropertiesService.getDetailRewardTypePoints().equalsIgnoreCase( inType ) );
		inUiModel.addAttribute( UIATTR_REWARDDISCOUNT, codePropertiesService.getDetailRewardTypeDiscount().equalsIgnoreCase( inType ) );
		inUiModel.addAttribute( UIATTR_REWARDITEMPT, codePropertiesService.getDetailRewardTypeItemPoint().equalsIgnoreCase( inType ) );
	}

	private void populateStatusTypes( Model uiModel ) {
		uiModel.addAttribute( UIATTR_SEARCH_STAT_ALL, UIATTR_SEARCH_STAT_ALL );
		uiModel.addAttribute( UIATTR_SEARCH_STAT_PROCESS, UIATTR_SEARCH_STAT_PROCESS );
		uiModel.addAttribute( UIATTR_SEARCH_STAT_MINE, UIATTR_SEARCH_STAT_MINE );
	}

	protected void populateApprovalStatuses( Model inUiModel ) {
		LookupDetail[] theStatuses = new LookupDetail[3];
		for ( LookupDetail theStatus : lookupService.getActiveDetailsByHeaderCode( codePropertiesService.getHeaderStatus() ) ) {
			if ( theStatus.getCode().equalsIgnoreCase( codePropertiesService.getDetailStatusActive() ) ) {
				theStatuses[0] = theStatus;
			}
			else if ( theStatus.getCode().equalsIgnoreCase( codePropertiesService.getDetailStatusRejected() ) ) {
				theStatuses[1] = theStatus;
			}
			else if ( theStatus.getCode().equalsIgnoreCase( codePropertiesService.getDetailStatusDisabled() ) ) {
				theStatuses[2] = theStatus;
			}
		}
		inUiModel.addAttribute( UIATTR_REMARKSTATUS, theStatuses );
	}

	private boolean isValid( PromotionDto inPromotion, BindingResult inBinding ) {

		CampaignDto theCampaign = promotionService.getCampaignDto( inPromotion.getCampaign() );

		if ( StringUtils.isBlank( inPromotion.getName() ) ) {
    		inBinding.reject( messageSource.getMessage( "promotion_msg_nameempty", null, LocaleContextHolder.getLocale() ) );
    	}

    	/*if ( CollectionUtils.isEmpty( inPromotion.getTargetStoreGroups() ) ) {
    		inBinding.reject( messageSource.getMessage( "promotion_msg_storegrpempty", null, LocaleContextHolder.getLocale() ) );
    	}*/

    	/*if ( CollectionUtils.isNotEmpty( inPromotion.getTargetMemberGroups() ) ) {
    		List<MemberModel> members = new ArrayList<MemberModel>();
    		for ( long groupId : inPromotion.getTargetMemberGroups() ) {
    			members.addAll( memberGroupService.getMembers( memberGroupService.getMemberGroup( groupId ) ) );
    		}
    		if ( memberService.findAll( members ).size() == memberService.countAllActive() ) {
        		inBinding.reject( messageSource.getMessage( "member_err_cannottargetall", null, LocaleContextHolder.getLocale() ) );
    		}
    	}*/

    	if ( null == inPromotion.getCampaign() ) {
    		inBinding.reject( messageSource.getMessage( "promotion_msg_campaignempty", null, LocaleContextHolder.getLocale() ) );
    	}

    	if ( null != inPromotion.getStartDate() && null != inPromotion.getEndDate() ) {
    		if ( inPromotion.getStartDate().after( inPromotion.getEndDate() ) ) {
    			inBinding.reject( messageSource.getMessage( "promotion_msg_daterangeinvalid", null, LocaleContextHolder.getLocale() ) );
    		}
    	}

		if ( null != inPromotion.getStartDate() ) {
			if ( null != theCampaign && null !=  theCampaign.getStartDate() 
					&& inPromotion.getStartDate().getTime() < ( theCampaign.getStartDate() ) ) {
				inBinding.reject( messageSource.getMessage( "promotion_msg_startdatebefore", null, LocaleContextHolder.getLocale() ) );
			}
		}

		if ( null != inPromotion.getEndDate() ) {
			if ( null != theCampaign && null !=  theCampaign.getEndDate() 
					&& inPromotion.getEndDate().getTime() > ( theCampaign.getEndDate() ) ) {
				inBinding.reject( messageSource.getMessage( "promotion_msg_enddateafter", null, LocaleContextHolder.getLocale() ) );
			}
		}

    	if ( StringUtils.isNotBlank( inPromotion.getStartTime() ) && StringUtils.isNotBlank( inPromotion.getEndTime() ) ) {
    		if ( DateUtil.convertLocalTimeString(DateUtil.TIME_FORMAT, inPromotion.getStartTime() ).isAfter(
    				DateUtil.convertLocalTimeString(DateUtil.TIME_FORMAT, inPromotion.getEndTime() ) ) ) {
    			inBinding.reject( messageSource.getMessage( "promotion_msg_timerangeinvalid", null, LocaleContextHolder.getLocale() ) );
    		}
    	}

    	if ( StringUtils.isNotBlank( inPromotion.getRewardType() ) ) {
    		List<PromotionRewardDto> rewards = new ArrayList<PromotionRewardDto>();
    		if ( CollectionUtils.isNotEmpty( inPromotion.getPromotionRewards() ) ) {
    			for ( PromotionRewardDto theDto : inPromotion.getPromotionRewards() ) {				
    				if ( null == theDto.getBaseAmount() && null == theDto.getBaseFactor() && null == theDto.getDiscount()
    					&& null == theDto.getMaxAmount() && null == theDto.getMaxBonus() && null == theDto.getMaxQty() 
    					&& null == theDto.getMinAmount() && null == theDto.getMinQty() ) {
    					continue;
    				}
    				rewards.add( theDto );
    			}
    		}
    		if ( CollectionUtils.isEmpty( rewards ) ) {
    			inBinding.reject( messageSource.getMessage( "promotion_msg_rewardsempty", null, LocaleContextHolder.getLocale() ) );
    		}
    	}

    	List<PromotionRewardDto> theRewards = inPromotion.getPromotionRewards();
    	if ( CollectionUtils.isNotEmpty( theRewards )
    			&& StringUtils.isNotBlank( inPromotion.getRewardType() )) {
    		if(inPromotion.getRewardType().equalsIgnoreCase( codePropertiesService.getDetailRewardTypePoints() ) ) {
    			for ( PromotionRewardDto theReward : theRewards ) {
    				if ( null != theReward.getMinAmount() && null != theReward.getMaxAmount()
    						&& theReward.getMinAmount().doubleValue() > theReward.getMaxAmount().doubleValue() ) {
    					inBinding.reject( messageSource.getMessage( "promotion_msg_rewardrangeinvalid", null, LocaleContextHolder.getLocale() ) );
    				}
    			}	
    		} else if(inPromotion.getRewardType().equalsIgnoreCase( codePropertiesService.getDetailRewardTypeItemPoint() ) ) {
    			for ( PromotionRewardDto theReward : theRewards ) {
    				if ( null != theReward.getMinQty() && null != theReward.getMaxQty() ) {
    					if(theReward.getMinQty().doubleValue() > theReward.getMaxQty().doubleValue() ) {
    						inBinding.reject( messageSource.getMessage( "promotion_msg_rewardrangeinvalid", null, LocaleContextHolder.getLocale() ) );	
    					}
    					if(theReward.getBaseAmount() == null) {
    						inBinding.reject( messageSource.getMessage( "promotion_msg_rewardbaseinvalid", null, LocaleContextHolder.getLocale() ) );
    					}
    				}
    			}
    		}
    	}

    	return !inBinding.hasErrors();
	}



    @ResponseBody
    @RequestMapping(value = "/{promoId}/overlappingpromo")
    public List<OverlappingPromoDto> getOverlappingPromotions(@PathVariable("promoId") long promoId) {
        return overlappingPromoService.getOverlappingPromo(promoId);
    }

    /*
     * Test page showing list of promotions to be able to test overlapping feature.
     * TODO: Remove promotionRepo and testShowPromotionList and add promo overlap action to list of promitions once list of promo page is created.
     */
    @Autowired
    private PromotionRepo promotionRepo;
    @RequestMapping(value = "/test/list")
    public ModelAndView testShowPromotionList() {
        return new ModelAndView("/promotion/testList").addObject("promos", promotionRepo.findAll());
    }
}
