package com.transretail.crm.core.reporting.support;

import com.google.common.collect.Maps;
import com.transretail.crm.common.reporting.engine.data.JRQueryDSLDataSource.FieldValueCustomizer;
import com.transretail.crm.core.entity.lookup.Store;
import com.transretail.crm.core.service.StoreService;
import java.util.Map;
import org.apache.commons.lang3.StringUtils;

/**
 * {@link Store} instance field customizer. It returns
 * {@link Store#getCodeAndName()} When the value or the store code is
 * {@code INVT001}, it will return {@literal HEAD OFFICE}.
 *
 * @since 3.5
 * @author Monte Cillo Co (mco@exist.com)
 */
public class StoreFieldValueCustomizer implements FieldValueCustomizer {

    private static final String HO_CODE = "INVT001";
    final Map<String, String> storeMruCache = Maps.newHashMap();
    private final StoreService storeService;
    private final String targetPropertyName;

    /**
     * Create an immutable instance
     *
     * @param targetPropertyName - the expected bean property name to be
     * customize
     * @param storeService - the store service
     */
    public StoreFieldValueCustomizer(String targetPropertyName, StoreService storeService) {
        this.storeService = storeService;
        this.targetPropertyName = targetPropertyName;
    }

    @Override
    public Object customize(String propertyName, Object value) {
        if (StringUtils.equalsIgnoreCase(targetPropertyName, propertyName)) {
            final String storeCode = value != null ? value.toString() : "";
            if (HO_CODE.equals(storeCode)) {
                return  "HEAD OFFICE";
            } else {
                String storePresentation = storeMruCache.get(storeCode);
                if (storePresentation != null) {
                    return storePresentation;
                } else {
                    final Store store = storeService.getStoreByCode(storeCode);
                    storePresentation = store == null ? storeCode : store.getCodeAndName();
                    storeMruCache.put(storeCode, storePresentation);
                    return storePresentation;
                }
            }
        }
        
        return value;
    }
}
