package com.transretail.crm.template.service.streaming.excel.handler.sheet.optional;

import static org.apache.commons.lang3.StringUtils.isBlank;

import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.i18n.LocaleContextHolder;

import com.google.common.collect.Maps;
import com.mysema.query.jpa.impl.JPAQuery;
import com.mysema.query.jpa.impl.JPAUpdateClause;
import com.transretail.crm.core.entity.MemberModel;
import com.transretail.crm.core.entity.QMemberModel;
import com.transretail.crm.core.entity.embeddable.Address;
import com.transretail.crm.core.entity.embeddable.QAddress;
import com.transretail.crm.core.repo.AddressRepo;
import com.transretail.crm.template.service.streaming.excel.handler.HandlerException;
import com.transretail.crm.template.service.streaming.excel.handler.cell.AccountIdCellHandler;
import com.transretail.crm.template.service.streaming.excel.handler.cell.BusinessAddressStreetCellHandler;
import com.transretail.crm.template.service.streaming.excel.handler.cell.BusinessFieldCellHandler;
import com.transretail.crm.template.service.streaming.excel.handler.cell.CellHandler;
import com.transretail.crm.template.service.streaming.excel.handler.cell.CompanyNameCellHandler;
import com.transretail.crm.template.service.streaming.excel.handler.sheet.AbstractSheetHandler;

/**
 * @author Allan G. Ramirez (agramirez@exist.com)
 */
public class IndWorkDataHandler extends AbstractSheetHandler {
    private static final String HEADER_ACCOUNTID = "ACCOUNTNUMBER";
    private static final String HEADER_COMPANYNAME = "COMPANYNAME";
    private static final String HEADER_BUSINESSFIELD = "BUSINESSFIELD";
    private static final String HEADER_BUSINESSADD = "BUSINESSADDRESS";

    private Map<String, CellHandler> cellHandlerClassMap = Maps.newHashMap();
    private CellHandler[] cellHandlers = null;

    @Autowired
    private AddressRepo addressRepo;

    public IndWorkDataHandler() {
        cellHandlerClassMap.put(HEADER_ACCOUNTID, new AccountIdCellHandler());
        cellHandlerClassMap.put(HEADER_COMPANYNAME, new CompanyNameCellHandler());
        cellHandlerClassMap.put(HEADER_BUSINESSFIELD, new BusinessFieldCellHandler(this));
        cellHandlerClassMap.put(HEADER_BUSINESSADD, new BusinessAddressStreetCellHandler());
        cellHandlers = new CellHandler[cellHandlerClassMap.size()];
    }

    @Override
    protected void processHeaderCell(int i, String headerName) {
        CellHandler cellHandler = cellHandlerClassMap.get(headerName);
        if (cellHandler == null) {
            throw new HandlerException(getMessageSource()
                .getMessage("member.template.messages.excel.nohandler", new String[]{sheetName, headerName},
                    LocaleContextHolder.getLocale()));
        }
        cellHandlers[i] = cellHandler;
    }

    @Override
    protected CellHandler getCellHandler(int index) {
        return cellHandlers[index];
    }

    @Override
    protected void saveOrUpdateDto(MemberModel dto) throws HandlerException {
        String accountId = dto.getAccountId();
        if (isBlank(accountId)) {
            throw new HandlerException(getMessageSource().getMessage("member.template.messages.excel.columns.required",
                new String[]{sheetName, String.valueOf(currentRowNum), HEADER_ACCOUNTID},
                LocaleContextHolder.getLocale()));
        } else if (accountId.length() != 13 || accountId.charAt(2) != '0' || accountId.charAt(3) != '1') {
            throw new HandlerException(getMessageSource().getMessage("member.template.messages.excel.accountid.invalid",
                new String[]{sheetName, accountId, "01"},
                LocaleContextHolder.getLocale()));
        }
        if (!accountIdExists(dto.getAccountId())) {
            throw new HandlerException(getMessageSource().getMessage("member.template.messages.excel.accountid.notfound",
                new String[]{sheetName, String.valueOf(currentRowNum), dto.getAccountId()},
                LocaleContextHolder.getLocale()));
        }

        QMemberModel qMemberModel = QMemberModel.memberModel;
        Long addressId = saveOrUpdateAddress(dto.getProfessionalProfile().getBusinessAddress(), dto.getAccountId(), qMemberModel);

        new JPAUpdateClause(em, qMemberModel).where(qMemberModel.accountId.eq(dto.getAccountId()))
            .set(qMemberModel.companyName, dto.getCompanyName())
            .set(qMemberModel.professionalProfile.businessField.code, dto.getProfessionalProfile().getBusinessField().getCode())
            .set(qMemberModel.professionalProfile.businessAddress.id, addressId)
            .execute();

        if (currentRowNum % batchSize == 0) {
            cleanup();
        }
    }

    @Override
    public int getExpectedNumberOfColumns() {
        return cellHandlers.length;
    }

    private Long saveOrUpdateAddress(Address dto, String accountId, QMemberModel qMemberModel) {
        Long result = null;
        QAddress qAddress = new QAddress("businessAddress");
        Address address = new JPAQuery(em).from(qMemberModel)
            .rightJoin(qMemberModel.professionalProfile.businessAddress, qAddress)
            .where(qMemberModel.accountId.eq(accountId)).singleResult(qAddress);
        if (address == null) {
            if (StringUtils.isNotBlank(dto.getStreet())) {
                result = addressRepo.saveAndFlush(dto).getId();
            }
        } else {
            address.setStreet(dto.getStreet());
            result = addressRepo.saveAndFlush(address).getId();
        }
        return result;
    }

}
