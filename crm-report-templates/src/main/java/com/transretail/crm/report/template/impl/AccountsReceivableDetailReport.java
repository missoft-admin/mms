package com.transretail.crm.report.template.impl;

import java.math.BigDecimal;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import net.sf.jasperreports.engine.JRDataSource;

import org.apache.commons.lang3.StringUtils;
import org.joda.time.LocalDate;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.MessageSourceAware;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.context.support.MessageSourceAccessor;
import org.springframework.stereotype.Service;

import com.google.common.collect.Maps;
import com.google.common.collect.Sets;
import com.mysema.query.BooleanBuilder;
import com.mysema.query.jpa.JPASubQuery;
import com.mysema.query.jpa.impl.JPAQuery;
import com.mysema.query.types.Projections;
import com.mysema.query.types.QBean;
import com.mysema.query.types.expr.CaseBuilder;
import com.mysema.query.types.expr.NumberExpression;
import com.transretail.crm.common.reporting.engine.data.JRQueryDSLDataSource;
import com.transretail.crm.common.reporting.engine.data.JRQueryDSLDataSource.FieldValueCustomizer;
import com.transretail.crm.common.reporting.jasper.ClasspathFileResolver;
import com.transretail.crm.common.reporting.jasper.DefaultJRProcessor;
import com.transretail.crm.common.reporting.jasper.JRProcessor;
import com.transretail.crm.common.util.DateUtil;
import com.transretail.crm.core.entity.QPeopleSoftStore;
import com.transretail.crm.core.entity.enums.ReportType;
import com.transretail.crm.core.entity.lookup.LookupDetail;
import com.transretail.crm.core.entity.lookup.QStore;
import com.transretail.crm.core.entity.lookup.Store;
import com.transretail.crm.core.security.util.UserUtil;
import com.transretail.crm.core.service.CodePropertiesService;
import com.transretail.crm.core.service.LookupService;
import com.transretail.crm.core.service.StoreService;
import com.transretail.crm.giftcard.entity.QGiftCardCustomerProfile;
import com.transretail.crm.giftcard.entity.QSalesOrder;
import com.transretail.crm.giftcard.entity.QSalesOrderItem;
import com.transretail.crm.giftcard.entity.QSalesOrderPaymentInfo;
import com.transretail.crm.giftcard.entity.support.GiftCardSalesType;
import com.transretail.crm.giftcard.entity.support.PaymentInfoStatus;
import com.transretail.crm.giftcard.entity.support.SalesOrderType;
import com.transretail.crm.report.template.*;

@Service("accountsReceivableDetailReport")
public class AccountsReceivableDetailReport implements ReportTemplate, ReportsCustomizer, MessageSourceAware, NoDataFoundSupport {
	protected static final String TEMPLATE_NAME = "Accounts Receivable Detail Report";
    protected static final String REPORT_NAME = "reports/accounts_receivable_detail_report.jasper";
    
    protected static final String FILTER_DATE_TO = CommonReportFilter.DATE_TO;
    protected static final String FILTER_DATE_FROM = CommonReportFilter.DATE_FROM;
    protected static final String FILTER_CUSTOMER = "customer";
    protected static final String FILTER_STORE = "store";
    protected static final String FILTER_SALES_TYPE = "salesType";
    
    /*1. Book Date From
    2. Book Date To
    3. Customer
    4. Order Store
    5. Sales Type*/
    
    private static final DateTimeFormatter INPUT_DATE_PATTERN = DateTimeFormat.forPattern(DateUtil.SEARCH_DATE_FORMAT);
    
    private static final Logger LOGGER = LoggerFactory.getLogger(AccountsReceivableDetailReport.class);
    
    @PersistenceContext
    private EntityManager em;
    private MessageSourceAccessor messageSource;
    @Autowired
    private StoreService storeService;
    @Autowired
    private LookupService lookupService;
    @Autowired
    private CodePropertiesService codePropertiesService;
    @Override
    public void setMessageSource(MessageSource messageSource) {
    	this.messageSource = new MessageSourceAccessor(messageSource);
    }
    
    @Override
    public ReportType[] getReportTypes() {
    	return ReportType.values();
    }
    
    @Override
    public String getName() {
    	return TEMPLATE_NAME;
    }

    @Override
    public String getDescription() {
    	return messageSource.getMessage("accounts.receivable.report.title", (Object[]) null, LocaleContextHolder.getLocale());
    }
    
    @Override
    public boolean canView(Set<String> permissions) {
    	return true;
    }
    
    @Override
	public Set<Option> customizerOption() {
    	return Sets.newHashSet(ReportsCustomizer.Option.DATE_RANGE_FULL_VALIDATION);
	}
    
    @Override
    public Set<FilterField> getFilters() {
    	Set<FilterField> filters = Sets.newLinkedHashSet();
        filters.add(FilterField.createDateField(FILTER_DATE_FROM,
                messageSource.getMessage("label_from", (Object[]) null, LocaleContextHolder.getLocale()),
                DateUtil.SEARCH_DATE_FORMAT, true));
        filters.add(FilterField.createDateField(FILTER_DATE_TO,
                messageSource.getMessage("label_to", (Object[]) null, LocaleContextHolder.getLocale()),
                DateUtil.SEARCH_DATE_FORMAT, true));
        
        Map<String, String> salesTypes = new LinkedHashMap<String, String>();
		salesTypes.put("", "");
		for (GiftCardSalesType key : GiftCardSalesType.values()) {
			if(key.getOrderTypes() != null)
				salesTypes.put(key.toString(), key.toString());
		}
		filters.add(FilterField.createDropdownField(FILTER_SALES_TYPE,
                messageSource.getMessage("accounts.receivable.report.salestype"), salesTypes));
		filters.add(FilterField.createDropdownField(FILTER_CUSTOMER,
                messageSource.getMessage("accounts.receivable.report.customer"), getCustFilter()));
		filters.add(FilterField.createDropdownField(FILTER_STORE,
                messageSource.getMessage("accounts.receivable.report.store", (Object[]) null, LocaleContextHolder.getLocale()), getStoresFilter()));
        return filters;
    }
    private Map<String, String> getStoresFilter() {
    	QStore qstore = QStore.store;
    	Map<String, String> stores = Maps.newLinkedHashMap();
    	LookupDetail headOffice = lookupService.getDetailByCode(codePropertiesService.getDetailInvLocationHeadOffice());
    	stores.put(headOffice.getCode(), headOffice.getCode() + " - " + headOffice.getDescription());
    	stores.putAll(new JPAQuery(em).from(qstore).orderBy(qstore.code.asc()).map(qstore.code, qstore.code.append(" - ").append(qstore.name))); 
    	stores.put("", "");
    	return stores;
    }
    private Map<String, String> getCustFilter() {
    	QGiftCardCustomerProfile qcx = QGiftCardCustomerProfile.giftCardCustomerProfile;
    	Map<String, String> custMap = new JPAQuery(em).from(qcx).map(qcx.id.stringValue(), qcx.name); 
    	custMap.put("", "");
    	return custMap;
    }
    
    JPAQuery createQuery(FilterBean filter) {
        QSalesOrderPaymentInfo qpayment = QSalesOrderPaymentInfo.salesOrderPaymentInfo;
        QSalesOrder qso = QSalesOrder.salesOrder;
        QGiftCardCustomerProfile qcustomer = QGiftCardCustomerProfile.giftCardCustomerProfile;

        BooleanBuilder f = new BooleanBuilder();
        f.and(qso.orderDate.between(filter.startDate, filter.endDate));
        if (filter.salesType != null) {
            f.and(qso.orderType.in(filter.salesType.getOrderTypes()));
        }
        
        if (StringUtils.isNotBlank(filter.storeCode)) {
            f.and(qso.paymentStore.eq(filter.storeCode));
        }
        
        if (filter.customerId != null) {
            f.and(qcustomer.id.eq(filter.customerId));
        }

        JPAQuery query = new JPAQuery(em)
                .from(qpayment)
                .rightJoin(qpayment.order, qso)
                .leftJoin(qso.customer, qcustomer)
                .where(f);


        return query;
    }
    
    void aggregate(JPAQuery query) {
        QSalesOrderPaymentInfo qpayment = QSalesOrderPaymentInfo.salesOrderPaymentInfo;
        QSalesOrder qso = QSalesOrder.salesOrder;
        QGiftCardCustomerProfile qcustomer = QGiftCardCustomerProfile.giftCardCustomerProfile;

        query.groupBy(qso.id, qso.orderDate, qso.orderNo, qso.paymentStore,
                qso.activationDate, qso.approvalDate, qcustomer.name, qcustomer.customerId,
                qso.orderType)
                .orderBy(qso.orderDate.asc(), qso.orderNo.asc(), qcustomer.customerId.asc());
    }
    
    QBean createProjection() {
        QSalesOrderPaymentInfo qpayment = QSalesOrderPaymentInfo.salesOrderPaymentInfo;
        QSalesOrder qso = QSalesOrder.salesOrder;
        QGiftCardCustomerProfile qcustomer = QGiftCardCustomerProfile.giftCardCustomerProfile;

        QSalesOrderItem qitem = QSalesOrderItem.salesOrderItem;

        NumberExpression<BigDecimal> discVal = new CaseBuilder()
                .when(qitem.order.discountVal.isNull()).then(BigDecimal.ZERO)
                .otherwise(qitem.order.discountVal);

        NumberExpression<BigDecimal> shippingFee = new CaseBuilder()
                .when(qitem.order.shippingFee.isNull()).then(BigDecimal.ZERO)
                .otherwise(qitem.order.shippingFee);

        NumberExpression<BigDecimal> soAmtQ = new JPASubQuery()
                .from(qitem).where(qitem.order.id.eq(qso.id))
                .groupBy(qitem.order.orderNo, qitem.order.shippingFee, qitem.order.discountVal)
                .unique(qitem.faceAmount.sum().add(qitem.printFee.sum()).add(qitem.order.shippingFee.coalesce(BigDecimal.ZERO))
                        .subtract(qitem.faceAmount.sum().multiply(qitem.order.discountVal.coalesce(BigDecimal.ZERO).asNumber().divide(new BigDecimal(100)))));

        NumberExpression<BigDecimal> notVerified = new CaseBuilder()
                .when(qpayment.status.eq(PaymentInfoStatus.FIRST_APPROVAL)).then(qpayment.paymentAmount)
                .otherwise(BigDecimal.ZERO);
        NumberExpression<BigDecimal> notApproved = new CaseBuilder()
                .when(qpayment.status.in(PaymentInfoStatus.VERIFIED, PaymentInfoStatus.SECOND_APPROVAL)).then(qpayment.paymentAmount)
                .otherwise(BigDecimal.ZERO);
        NumberExpression<BigDecimal> approved = new CaseBuilder()
                .when(qpayment.status.eq(PaymentInfoStatus.APPROVED)).then(qpayment.paymentAmount)
                .otherwise(BigDecimal.ZERO);

        QBean<ReportBean> projection = Projections.bean(ReportBean.class,
                qso.paymentStore.as("psStoreCode"),
                qso.paymentStore.as("storeCode"),
                qso.approvalDate.as("approvalDate"),
                qso.activationDate.as("bookDate"),
                qcustomer.customerId.as("customerId"),
                qcustomer.name.as("customerName"),
                qso.orderNo.as("orderNo"),
                qso.orderType.as("orderType"),
                soAmtQ.as("salesAmount"),
                qpayment.paymentAmount.sum().as("collectAmount"),
                notVerified.sum().as("notVerified"),
                notApproved.sum().as("notApproved"),
                approved.sum().as("approved")
        );

        return projection;
    }
    @Override
    public JRProcessor createJrProcessor(Map<String, String> map) {
        QSalesOrderPaymentInfo qpayment = QSalesOrderPaymentInfo.salesOrderPaymentInfo;
        QSalesOrder qso = QSalesOrder.salesOrder;
        QGiftCardCustomerProfile qcustomer = QGiftCardCustomerProfile.giftCardCustomerProfile;

        FilterBean filter = FilterBean.create(map);
        JPAQuery query = createQuery(filter);
        aggregate(query);
        
        QBean projection = createProjection();
        final QPeopleSoftStore qpstore = QPeopleSoftStore.peopleSoftStore;

        JRDataSource dataSource = new JRQueryDSLDataSource(query, projection,
                new FieldValueCustomizer() {
                    private final Map<String, String> storeCacheMap = Maps.newHashMap();
                    private final Map<String, String> pStoreCacheMap = Maps.newHashMap();

                    @Override
                    public Object customize(String propertyName, Object value) {
                        final String fieldValue = value != null ? value.toString() : "";
                        if (value != null) {
                            if ("storeCode".equals(propertyName)) {
                                String storePresentation = storeCacheMap.get(fieldValue);
                                if (storePresentation != null) {
                                    return storePresentation;
                                } else {
                                    final Store store = storeService.getStoreByCode(fieldValue);
                                    storePresentation = store == null ? fieldValue : store.getCodeAndName();
                                    storeCacheMap.put(fieldValue, storePresentation);
                                    return storePresentation;
                                }
                            }

                            if ("psStoreCode".equals(propertyName)) {
                                String pStoreCode = pStoreCacheMap.get(fieldValue);
                                if (pStoreCode != null) {
                                    return pStoreCode;
                                }
                                pStoreCode = new JPAQuery(em).from(qpstore).where(qpstore.store.code.eq(fieldValue)).singleResult(qpstore.peoplesoftCode);
                                pStoreCacheMap.put(fieldValue, pStoreCode);
                                return pStoreCode;
                            }

                        }

                        return value;
                    }
                });

        DefaultJRProcessor jrProcessor = new DefaultJRProcessor(REPORT_NAME);
        jrProcessor.setFileResolver(new ClasspathFileResolver("reports"));
        jrProcessor.addParameter("SUB_DATA_SOURCE", dataSource);
        jrProcessor.addParameter("START_DATE", map.get(FILTER_DATE_FROM));
        jrProcessor.addParameter("END_DATE", map.get(FILTER_DATE_TO));
        
        Long custId = filter.customerId;
        jrProcessor.addParameter("SALES_TYPE", map.get(FILTER_SALES_TYPE));
        QGiftCardCustomerProfile qcust = QGiftCardCustomerProfile.giftCardCustomerProfile;
        jrProcessor.addParameter("CUSTOMER", custId == null ? "" : new JPAQuery(em).from(qcust).where(qcust.id.eq(custId)).singleResult(qcust.name));
        String storeCode = filter.storeCode;
        if (StringUtils.isNotBlank(storeCode)) {
            if (codePropertiesService.getDetailInvLocationHeadOffice().equalsIgnoreCase(storeCode)) {
                LookupDetail ho = lookupService.getActiveDetailByCode(codePropertiesService.getDetailInvLocationHeadOffice());
                jrProcessor.addParameter("STORE", ho.getCode() + " - " + ho.getDescription());
            } else {
                QStore qstore = QStore.store;
                jrProcessor.addParameter("STORE", new JPAQuery(em)
                        .from(qstore).where(qstore.code.eq(storeCode))
                        .singleResult(qstore.code.append(" - ").append(qstore.name)));
            }

        }

        jrProcessor.addParameter("printedBy", UserUtil.getCurrentUser().getUsername());
        jrProcessor.addParameter("printedDate", new LocalDate().toString("dd-MM-yyyy"));

        return jrProcessor;
    }

    @Override
    public boolean isEmpty(Map<String, String> filter) {
        return !createQuery(FilterBean.create(filter)).exists();
    }
    
     static class FilterBean {

        static FilterBean create(Map<String, String> filter) {
            LocalDate startDate = INPUT_DATE_PATTERN.parseLocalDate(filter.get(FILTER_DATE_FROM));
            LocalDate endDate = INPUT_DATE_PATTERN.parseLocalDate(filter.get(FILTER_DATE_TO));
            GiftCardSalesType salesType = StringUtils.isBlank(filter.get(FILTER_SALES_TYPE)) ? null : 
                    GiftCardSalesType.valueOf(filter.get(FILTER_SALES_TYPE));
            String storeCode = filter.get(FILTER_STORE);
            Long custId = StringUtils.isBlank(filter.get(FILTER_CUSTOMER)) ? null : Long.valueOf(filter.get(FILTER_CUSTOMER));
            return new FilterBean(startDate, endDate, storeCode, custId, salesType);
        }

        public FilterBean(LocalDate startDate, LocalDate endDate, String storeCode, Long customerId, GiftCardSalesType salesType) {
            this.startDate = startDate;
            this.endDate = endDate;
            this.storeCode = storeCode;
            this.customerId = customerId;
            this.salesType = salesType;
        }

        LocalDate startDate;
        LocalDate endDate;
        String storeCode;
        Long customerId;
        GiftCardSalesType salesType;
    }
     
    public static class ReportBean {
    	private String psStoreCode;
    	private String storeCode;
    	private LocalDate approvalDate;
    	private LocalDate bookDate;
    	private String customerId;
    	private String customerName;
    	private String orderNo;
    	private BigDecimal salesAmount;
    	private BigDecimal collectAmount;
    	private BigDecimal notVerified;
    	private BigDecimal notApproved;
    	private BigDecimal approved;
    	private SalesOrderType orderType;
    	
		public SalesOrderType getOrderType() {
			return orderType;
		}
		public void setOrderType(SalesOrderType orderType) {
			this.orderType = orderType;
		}
		public String getPsStoreCode() {
			return psStoreCode;
		}
		public void setPsStoreCode(String psStoreCode) {
			this.psStoreCode = psStoreCode;
		}
		public String getStoreCode() {
			return storeCode;
		}
		public void setStoreCode(String storeCode) {
			this.storeCode = storeCode;
		}
		public LocalDate getApprovalDate() {
			return approvalDate;
		}
		public void setApprovalDate(LocalDate approvalDate) {
			this.approvalDate = approvalDate;
		}
		public LocalDate getBookDate() {
			return bookDate;
		}
		public void setBookDate(LocalDate bookDate) {
			this.bookDate = bookDate;
		}
		public String getCustomerId() {
			return customerId;
		}
		public void setCustomerId(String customerId) {
			this.customerId = customerId;
		}
		public String getCustomerName() {
			return customerName;
		}
		public void setCustomerName(String customerName) {
			this.customerName = customerName;
		}
		public String getOrderNo() {
			return orderNo;
		}
		public void setOrderNo(String orderNo) {
			this.orderNo = orderNo;
		}
		public String getSalesType() {
			return GiftCardSalesType.getSalesType(this.orderType).toString();
		}
		public BigDecimal getSalesAmount() {
			if(salesAmount != null)
				return salesAmount;
			return BigDecimal.ZERO;
		}
		public void setSalesAmount(BigDecimal salesAmount) {
			this.salesAmount = salesAmount;
		}
		public BigDecimal getCollectAmount() {
			if(collectAmount != null)
				return collectAmount;
			return BigDecimal.ZERO;
		}
		public void setCollectAmount(BigDecimal collectAmount) {
			this.collectAmount = collectAmount;
		}
		public BigDecimal getNotVerified() {
			if(notVerified != null)
				return notVerified;
			return BigDecimal.ZERO;
		}
		public void setNotVerified(BigDecimal notVerified) {
			this.notVerified = notVerified;
		}
		public BigDecimal getNotApproved() {
			if(notApproved != null)
				return notApproved;
			return BigDecimal.ZERO;
		}
		public void setNotApproved(BigDecimal notApproved) {
			this.notApproved = notApproved;
		}
		public BigDecimal getApproved() {
			if(approved != null)
				return approved;
			return BigDecimal.ZERO;
		}
		public void setApproved(BigDecimal approved) {
			this.approved = approved;
		}
		public BigDecimal getOverPayment() {
			BigDecimal change = getChange();
			return BigDecimal.ZERO.compareTo(change) < 0 ? change : BigDecimal.ZERO;
		}
		public BigDecimal getNotYetCollected() {
			BigDecimal change = getChange();
			return BigDecimal.ZERO.compareTo(change) > 0 ? change.negate() : BigDecimal.ZERO;
		}
		public BigDecimal getChange() {
			/*if(this.salesAmount == null)
				return BigDecimal.ZERO;*/
			
			BigDecimal collectAmount = this.collectAmount == null ? BigDecimal.ZERO : this.collectAmount;
			BigDecimal salesAmount = this.salesAmount == null ? BigDecimal.ZERO : this.salesAmount;
			
			return collectAmount.subtract(salesAmount);
		}
    }
    

}
