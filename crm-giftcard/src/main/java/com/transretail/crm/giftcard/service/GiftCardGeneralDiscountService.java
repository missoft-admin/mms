package com.transretail.crm.giftcard.service;

import com.transretail.crm.common.service.dto.response.ResultList;
import com.transretail.crm.giftcard.dto.GiftCardGeneralDiscountDto;
import com.transretail.crm.giftcard.dto.GiftCardGeneralDiscountSearchDto;

public interface GiftCardGeneralDiscountService {
	GiftCardGeneralDiscountDto findById(Long id);
	void save(GiftCardGeneralDiscountDto dto);
	void delete(Long id);
	ResultList<GiftCardGeneralDiscountDto> search(GiftCardGeneralDiscountSearchDto searchDto);
	boolean checkForOverlap(GiftCardGeneralDiscountDto dto);
}
