package com.transretail.crm.web.controller;

import static org.mockito.Matchers.anyMap;
import static org.mockito.Matchers.eq;
import static org.mockito.Matchers.isA;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.reset;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.model;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.view;

import java.util.Set;

import org.apache.commons.io.output.ByteArrayOutputStream;
import org.hamcrest.core.Is;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentMatcher;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.AnnotationConfigWebContextLoader;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.collect.Sets;
import com.transretail.crm.common.reporting.jasper.JRProcessor;
import com.transretail.crm.common.reporting.service.ReportService;
import com.transretail.crm.common.service.dto.response.NameIdPairDto;
import com.transretail.crm.core.service.EmployeePurchaseService;
import com.transretail.crm.core.service.MemberService;
import com.transretail.crm.core.service.PointsRewardService;
import com.transretail.crm.core.service.PointsTxnManagerService;
import com.transretail.crm.report.template.FilterField;
import com.transretail.crm.report.template.ReportTemplate;

/**
 * Unit test for ReportsController.
 * <p>
 * This will cover the mvc part of ReportsController. All aggregate classes in ReportsController are mocked unlike ReportsControllerTest.
 * </p>
 *
 * @author Allan G. Ramirez (agramirez@exist.com)
 */
@RunWith(SpringJUnit4ClassRunner.class)
@WebAppConfiguration
@ContextConfiguration(loader = AnnotationConfigWebContextLoader.class)
public class ReportsControllerTest2 extends AbstractMockMvcTest {
    private static final String REPORT_TEMPLATE_NAME = "testReportTemplateName";
    private static final String REPORT_TEMPLATE_DESC = "testReportTemplateDesc";

    @Configuration
    @EnableWebMvc
    static class Config {
        @Bean
        public ReportsController reportsController() {
            return new ReportsController();
        }

        @Bean
        public ReportService reportService() {
            return mock(ReportService.class);
        }

        @Bean
        public MessageSource messageSource() {
            return mock(MessageSource.class);
        }

        @Bean
        public PointsTxnManagerService pointsManagerService() {
            return mock(PointsTxnManagerService.class);
        }

        @Bean
        public MemberService memberService() {
            return mock(MemberService.class);
        }

        @Bean
        public EmployeePurchaseService employeePurchaseService() {
            return mock(EmployeePurchaseService.class);
        }

        @Bean
        public PointsRewardService pointsRewardService() {
            return mock(PointsRewardService.class);
        }

        @Bean
        public ReportTemplate reportTemplate() {
            ReportTemplate template = mock(ReportTemplate.class);
            when(template.getName()).thenReturn(REPORT_TEMPLATE_NAME);
            when(template.getDescription()).thenReturn(REPORT_TEMPLATE_DESC);
            return template;
        }
    }

    @Autowired
    private ReportService reportService;
    @Autowired
    private MessageSource messageSource;
    @Autowired
    private PointsTxnManagerService pointsManagerService;
    @Autowired
    private MemberService memberService;
    @Autowired
    private EmployeePurchaseService employeePurchaseService;
    @Autowired
    private PointsRewardService pointsRewardService;
    @Autowired
    private ReportTemplate reportTemplate;

    public void doSetup() {
        reset(reportService);
        reset(messageSource);
        reset(pointsManagerService);
        reset(memberService);
        reset(employeePurchaseService);
        reset(pointsRewardService);
        reset(reportTemplate);
    }

    @Test
    public void templateListTest() throws Exception {
        when(reportTemplate.getName()).thenReturn(REPORT_TEMPLATE_NAME);
        when(reportTemplate.getDescription()).thenReturn(REPORT_TEMPLATE_DESC);

        mockMvc.perform(get("/report/template/list"))
            .andExpect(model().attribute(ReportsController.ATTR_REPORT_TEMPLATE,
                new Is<Set<NameIdPairDto>>(new ArgumentMatcher<Set<NameIdPairDto>>() {
                    @Override
                    public boolean matches(Object argument) {
                        Set<NameIdPairDto> set = (Set<NameIdPairDto>) argument;
                        NameIdPairDto dto = set.iterator().next();

                        return set.size() == 1 && REPORT_TEMPLATE_NAME.equals(dto.getId()) && REPORT_TEMPLATE_DESC
                            .equals(dto.getName());
                    }
                })))
            .andExpect(view().name("report/template/list"))
            .andExpect(status().isOk());
    }

    @Test
    public void getFiltersTest() throws Exception {
        String name = "name";
        String label = "label";
        Set<FilterField> expectedFilters = Sets.newHashSet(FilterField.createInputField(name, label));
        when(reportTemplate.getFilters()).thenReturn(expectedFilters);

        mockMvc.perform(get("/report/template/" + REPORT_TEMPLATE_NAME + "/filters"))
            .andExpect(status().isOk())
            .andExpect(content().string(new ObjectMapper().writeValueAsString(expectedFilters)));
    }

    @Test
    public void viewReportTest() throws Exception {
        JRProcessor expectedJrProcessor = mock(JRProcessor.class);
        when(reportTemplate.createJrProcessor(anyMap())).thenReturn(expectedJrProcessor);
        mockMvc.perform(get("/report/template/" + REPORT_TEMPLATE_NAME + "/view"))
            .andExpect(status().isOk());
        verify(reportService).exportPdfReport(eq(expectedJrProcessor), isA(ByteArrayOutputStream.class));
    }
}
