package com.transretail.crm.core.repo;

import org.springframework.stereotype.Repository;

import com.transretail.crm.common.repo.CrmQueryDslPredicateExecutor;
import com.transretail.crm.core.entity.RewardSchemeModel;
import com.transretail.crm.core.repo.custom.RewardSchemeRepoCustom;

@Repository
public interface RewardSchemeRepo extends CrmQueryDslPredicateExecutor<RewardSchemeModel, Long>, RewardSchemeRepoCustom {
	
}
