package com.transretail.crm.giftcard.service.impl;

import com.google.common.collect.Lists;
import com.transretail.crm.core.dto.MemberDto;
import com.transretail.crm.core.entity.MemberModel;
import com.transretail.crm.core.repo.MemberRepo;
import com.transretail.crm.core.security.util.UserUtil;
import com.transretail.crm.giftcard.dto.*;
import com.transretail.crm.giftcard.entity.GiftCardInventory;
import com.transretail.crm.giftcard.entity.GiftCardTransactionItem;
import com.transretail.crm.giftcard.entity.support.GiftCardInventoryStatus;
import com.transretail.crm.giftcard.repo.GiftCardInventoryRepo;
import com.transretail.crm.giftcard.repo.GiftCardTransactionItemRepo;
import com.transretail.crm.giftcard.service.MemberPrepaidService;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.joda.time.DateTime;
import org.joda.time.LocalDateTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.Errors;

import java.util.List;

/**
 * description here.
 *
 * @author Vincent Gerard Tan
 */
@Service("memberPrepaidService")
@Transactional
public class MemberPrepaidServiceImpl implements MemberPrepaidService {

    private static final Logger LOG = LoggerFactory.getLogger(MemberPrepaidServiceImpl.class);
    @Autowired
    private GiftCardInventoryRepo giftCardInventoryRepo;

    @Autowired
    private GiftCardTransactionItemRepo giftCardTransactionItemRepo;

    @Autowired
    private MemberRepo memberRepo;

    @Override
    public MemberPrepaidDto getMemberPrepaidDetails() {
        MemberPrepaidDto memberPrepaidDto = new MemberPrepaidDto();
        String username = UserUtil.getCurrentUser().getUsername();
        double totalBalance = 0.0;
        List<GiftCardInventory> inventoryList = giftCardInventoryRepo.findByOwningMember_Username(username);
        List<GiftCardInventoryDto> inventoryDtoList = Lists.newArrayList();

        if (!CollectionUtils.isEmpty(inventoryList)) {
            for (GiftCardInventory inventory : inventoryList) {
                GiftCardInventoryDto giftCardInventoryDto = new GiftCardInventoryDto(inventory);

                if (inventory.getBalance() != null) {
                    totalBalance += inventory.getBalance();
                }

                inventoryDtoList.add(giftCardInventoryDto);
            }

            memberPrepaidDto.setGiftCards(inventoryDtoList);
            memberPrepaidDto.setTotalBalance((long) totalBalance);
        }
        return memberPrepaidDto;
    }

    @Override
    public MemberDto getMemberDetails() {
        MemberDto memberDto = null;
        String username = UserUtil.getCurrentUser().getUsername();
        MemberModel model = memberRepo.findByUsername(username);

        if (model != null) {
            memberDto = new MemberDto(model);
        }

        return memberDto;
    }

    @Override
    public void registerCardToMember(MemberGiftCardDto giftCardDto, Errors errors) {
        LOG.info("register card to member");
        String username = UserUtil.getCurrentUser().getUsername();
        MemberModel member = memberRepo.findByUsername(username);
        if (StringUtils.isNotBlank(giftCardDto.getCardNo())) {
            if (member != null) {
                GiftCardInventory inventory = giftCardInventoryRepo.findByBarcode(giftCardDto.getCardNo());
                if (inventory != null) {
                    if (inventory.getOwningMember() != null) {
                        errors.reject("Not allowed to link this card, GC already assigned to other member.");
                    } else {
                        if (inventory.getProfile().isAllowReload()
                                && inventory.getStatus().equals(GiftCardInventoryStatus.ACTIVATED)) {
                            inventory.setOwningMember(member);
                            inventory.setRegistrationDate(DateTime.now());
                            inventory.setAccountId(member.getAccountId());
                            giftCardInventoryRepo.save(inventory);
                        } else {
                            if (!inventory.getProfile().isAllowReload()) {
                                errors.reject("Not allowed to link this card, GC is not reloadable.");
                            } else if (!GiftCardInventoryStatus.ACTIVATED.equals(inventory.getStatus())){
                                errors.reject("Not allowed to link this card, status not yet activated.");
                            } else {
                                errors.reject("Not allowed to link this card.");
                            }

                        }
                    }
                } else {
                    errors.reject("Card not found.");
                }
            }
        } else {
            errors.reject("No card defined.");
        }
    }

    @Override
    public PrepaidTransactionResultList retrieveTransactionHistory(PrepaidTransactionSearchDto searchDto) {
        LOG.info("retrieve transaction history");

        List<PrepaidTransactionHistoryDto> result = Lists.newArrayList();
        int noOfDays = searchDto.getDuration();
        if (noOfDays <= 0) {
            noOfDays = 30;
        }

        LocalDateTime currentDateTime = LocalDateTime.now();
        LocalDateTime startDateTime = currentDateTime.minusDays(noOfDays);
        List<GiftCardTransactionItem> transactionItems =
                giftCardTransactionItemRepo.findPrepaidTransactions(searchDto.getCardNo(), startDateTime, currentDateTime);

        if (!CollectionUtils.isEmpty(transactionItems)) {
            LOG.info("get txn items");
            for (GiftCardTransactionItem item : transactionItems) {
                PrepaidTransactionHistoryDto dto = new PrepaidTransactionHistoryDto();
                dto.setCardNo(item.getGiftCard().getBarcode());
                dto.setTransactionDateTime(item.getTransaction().getTransactionDate());
                dto.setPreviousBalance((long) item.getCurrentAmount());
                dto.setTransactionAmount((long) item.getTransactionAmount());
                dto.setCurrentAmount(dto.getPreviousBalance() + dto.getTransactionAmount());
                dto.setTransactionType(item.getTransaction().getTransactionType().toString());
                result.add(dto);
            }
        }

        PrepaidTransactionResultList resultList = new PrepaidTransactionResultList(result);
        return resultList;
    }

    @Override
    public PrepaidCardResultList retrieveCards(PrepaidCardSearchDto prepaidCardSearchDto) {
        LOG.info("retrieve card list");
        String username = UserUtil.getCurrentUser().getUsername();
        //Pageable pageable = SpringDataPagingUtil.INSTANCE.createPageable(prepaidCardSearchDto.getPagination());
        List<GiftCardInventory> inventoryList = giftCardInventoryRepo.findByOwningMember_Username(username);

        List<PrepaidCardDto> resultList = Lists.newArrayList();
        if (CollectionUtils.isNotEmpty(inventoryList)) {
            for (GiftCardInventory inventory : inventoryList) {
                PrepaidCardDto dto = new PrepaidCardDto();
                dto.setCardNo(inventory.getBarcode());
                dto.setDescription(inventory.getProductName());
                dto.setPreviousBalance(inventory.getPreviousBalance().longValue());
                dto.setCurrentBalance(inventory.getBalance().longValue());
                dto.setRegistrationDate(inventory.getRegistrationDate());
                resultList.add(dto);
            }
        }
        PrepaidCardResultList result = new PrepaidCardResultList(resultList);
        return result;
    }
    
    @Override
    public PrepaidCardResultList retrieveCardsByMember(MemberModel member) {
        LOG.info("retrieve card list");
        List<GiftCardInventory> inventoryList = giftCardInventoryRepo.findByOwningMember(member);

        List<PrepaidCardDto> resultList = Lists.newArrayList();
        if (CollectionUtils.isNotEmpty(inventoryList)) {
            for (GiftCardInventory inventory : inventoryList) {
                PrepaidCardDto dto = new PrepaidCardDto();
                dto.setCardNo(inventory.getBarcode());
                dto.setDescription(inventory.getProductName());
                if (inventory.getPreviousBalance() == null)
                    dto.setPreviousBalance(0L);
                else
                    dto.setPreviousBalance(inventory.getPreviousBalance().longValue());
                if (inventory.getBalance() == null)
                    dto.setCurrentBalance(0L);
                else
                    dto.setCurrentBalance(inventory.getBalance().longValue());
                dto.setRegistrationDate(inventory.getRegistrationDate());
                resultList.add(dto);
            }
        }
        PrepaidCardResultList result = new PrepaidCardResultList(resultList);
        return result;
    }
}
