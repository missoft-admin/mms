create table CRM_STORE_MEMBER_AUD (
	id bigint not null,
	REV bigint not null,
	REVTYPE tinyint,
	STORE varchar(10) not null,
    COMPANY varchar(10),
    CARDTYPE varchar(10),
    REMARKS varchar(255),
    primary key (id, REV)
);