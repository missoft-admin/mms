<%@ include file="/WEB-INF/views/common/taglibs.jsp" %>

<div class="page-header page-header2"><h1><spring:message code="crm.instore.details"/></h1></div>
<div id="crmInStoreContainer">
  <div class="messagesDiv">

  </div>
  <div class="syncTxnMessagesDiv">

  </div>
  <div class="searchForm form-horizontal row-fluid well" id="instore_details">
    <div class="control-group mb0">
      <label class="control-label"><spring:message code="label_search"/></label>

      <div class="controls">
        <input type="text" name="name" class="">

        <input type="button" value="<spring:message code="search"/>" class="btn btn-primary ml10" id="searchCrmInfos"/>
        <input type="button" value='<spring:message code="label_clear" />' class="btn custom-reset" data-form-id="instore_details" data-default-id="searchCrmInfos" />
      </div>
    </div>
    <%--
    <div class="control-group">
      <label class="control-label">
        <spring:message code="crm.instore.checkstatus"/>
        <div style="font-size: 0.7em; color: #ec1f28; text-transform: none"><spring:message code="crm.instore.checkstatus.warn"/></div>
      </label>

      <div class="controls">
        <input name="checkStatus" type="checkbox">
      </div>
    </div>
    --%>
  </div>
  <button type="button" class="btn btn-primary pull-right mb20" id="addCrmInfo"><%--<spring:message code="add"/>--%>Create new
    <spring:message code="crm.instore.details"/></button>
  <div class="list">

  </div>
</div>
<!-- Add Store Dialog -->
<div id="crmInStoreDialog" class="modal hide nofly modal-dialog" style="display: none">
  <div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
    <h4>Create new <spring:message code="crm.instore.details"/></h4>

    <div class="clearfix"></div>
  </div>
  <div class="modal-body">
    <div class="messagesDiv">

    </div>
    <div id="crmProxyAddForm" class="form-horizontal row-fluid addElements">
      <div class="span6">
        <div class="control-group">
          <label class="control-label"><span class="required">*</span><spring:message code="crm.instore.code"/></label>

          <div class="controls">
            <input type="text" name="id" class="">
            <input type="hidden" name="prevId">
          </div>
        </div>
        <div class="control-group">
          <label class="control-label"><span class="required">*</span><spring:message code="crm.instore.storename"/></label>

          <div class="controls">
            <input type="text" name="name" class="">
          </div>
        </div>
        
        <div class="control-group" style="margin-bottom:0px;">
          <label class="control-label"><span class="required">*</span><spring:message code="crm.instore.crmproxy.prefix"/></label>

          <div class="controls">
            <input type="text" name="crmProxyRestPrefix" class="" value="/crm-proxy/api">
          </div>
        </div>
        
        <div class="control-group">
          <label class="control-label"><span class="required">*</span><spring:message code="crm.instore.reconcile.restprefix"/></label>

          <div class="controls">
            <input type="text" name="crmReconciliationRestPrefix" class="" value="/crm-reconciliation">
          </div>
        </div>
        
      </div>
      <div class="span6">
        <div class="control-group">
          <label class="control-label"><span class="required">*</span><spring:message code="crm.instore.ip"/></label>

          <div class="controls">
            <input type="text" name="ip" class="input-small">
          </div>
        </div>
        <div class="control-group">
          <label class="control-label"><span class="required">*</span><spring:message code="crm.instore.port"/></label>

          <div class="controls">
            <input type="text" name="tomcatPort" class="input-small" value="9090">
          </div>
        </div>
        
      </div>
    </div>
  </div>
  <div class="modal-footer container-btn">
    <button type="button" class="btn btn-primary" id="saveCrmInfo"><spring:message code="label_save"/></button>
    <button type="button" class="btn" id="cancelAddCrmInfo"><spring:message code="label_close"/></button>
  </div>
</div>
<style>
  #crmInStoreContainer .list table tr td > a {
    color: #16aeec !important;
  }
</style>
<script src="<spring:url value="/js/jquery/jquery.dataTables.js" />"></script>
<script src="<spring:url value="/js/jquery/jquery.dataTables.linkLengthMenu.js" />"></script>
<script src="<spring:url value="/js/jquery/jquery.dataTables.customPaging.js" />"></script>
<script src="<spring:url value="/js/jquery/ajax-datatable.js" />"></script>
<script src="<spring:url value="/js/common/form2js.js" />"></script>
<script src="<spring:url value="/js/common/json2.js" />"></script>
<script src="<spring:url value="/js/jquery/jquery.toObject.js" />"></script>
<script src="<spring:url value="/js/jquery/jquery.ajaxQueue.js" />"></script>
<link href="<spring:url value="/css/bootstrap/datepicker.css" />" rel="stylesheet"/>
<link href="<spring:url value="/css/dataTables.css" />" rel="stylesheet">
<script>
$( document ).ready( function () {
  var clickableCell = function ( data, type, row ) {
    return '<a class="clickable" href="#" onclick="return false;"><div>' + data + '</div></a>';
  };
  var $crmInStoreContainer = $( '#crmInStoreContainer' );
  var $crmInStores = $( '.list', $crmInStoreContainer );
  var reconcileFeatDate = new Date( 2014, 05, 15 ); // June 15, 2014
  $crmInStores.ajaxDataTable( {
    'autoload' : true,
    'ajaxSource' : '<spring:url value="/crmproxyinfo/search" />',
    'columnHeaders' : [
      '<spring:message code="crm.instore.code" />',
      '<spring:message code="crm.instore.storename" />',
      <%--
      '<spring:message code="crm.instore.ip" />',
      '<spring:message code="crm.proxy.version" />',
      '<spring:message code="crm.proxy.shaId" />',
      '<spring:message code="crm.proxy.lastUpdate" />',
      --%>
      '<spring:message code="crm.proxy.status" />',
      '<spring:message code="action" />'
    ],
    'modelFields' : [
      {name : 'id', sortable : true, fieldCellRenderer : function ( data, type, row ) {
        var htmlString = '';
        htmlString += clickableCell( data, type, row );
        htmlString += '<input type="hidden" class="id" value="' + row.id + '" />';
        htmlString += '<input type="hidden" class="name" value="' + row.name + '" />';
        htmlString += '<input type="hidden" class="ip" value="' + row.ip + '" />';
        htmlString += '<input type="hidden" class="tomcatPort" value="' + row.tomcatPort + '" />';
        htmlString += '<input type="hidden" class="crmProxyRestPrefix" value="' + row.crmProxyRestPrefix + '" />';
        htmlString += '<input type="hidden" class="crmReconciliationRestPrefix" value="' + row.crmReconciliationRestPrefix + '" />';
        htmlString += '<input type="hidden" class="version" />';
        htmlString += '<input type="hidden" class="shaId" />';
        htmlString += '<input type="hidden" class="lastUpdate" />';
        return htmlString;
      } },
      {name : 'name', sortable : true, fieldCellRenderer : clickableCell},
      <%--
      {name : 'ip', sortable : false},
      {name : 'version', sortable : false},
      {name : 'shaId', sortable : false},
      {name : 'lastUpdate', sortable : false},
      --%>
      {name : 'status', sortable : true, fieldCellRenderer : function ( data, type, row ) {
        return '<img src="<spring:url value="/images/ajax-loader-bar.gif" />" style="width: 90%; height:95%;" />';
      } },
      { customCell : function ( innerData, sSpecific, json ) {
        return '<div class="actions"></div>';
      } }
    ],
    "fnCreatedRow" : function ( nRow, aData, iDataIndex ) {
      $.get( ctx + '/crmproxyinfo/' + aData.id, function ( dto ) {
        var status = 'ACTIVE';
        var actionHtmlString = '<a target="_blank" href="' + ctx + '/crminstore/' + aData.id + '/points/forreconciliation"><button class="btn btn-primary btn_reconcile tiptip pull-left icn-edit" title="<spring:message code="crm.proxy.btn.reconcile.points" />"></button></a>'
                + '<a target="_blank" href="' + ctx + '/crminstore/' + aData.id + '/emptxn/forreconciliation"><button class="btn btn-primary btn_reconcile tiptip pull-left icn-edit" title="<spring:message code="crm.proxy.btn.reconcile.emptxn" />"></button></a>';
        if ( dto.status == 'ACTIVE' ) {
          $( '.version', nRow ).val( dto.version );
          $( '.shaId', nRow ).val( dto.shaId );
          $( '.lastUpdate', nRow ).val( dto.lastUpdate );

          actionHtmlString += '<button class="btn btn-primary btn_syncrefdata tiptip pull-left icn-sync" title="<spring:message code="crm.proxy.btn.sync.memberdata" />"></button>'
                  + '<img src="<spring:url value="/images/ajax-loader-spinner-small.gif" />" style="display: none;"/>'
                  + '<button class="btn btn-primary btn_synctxndata tiptip pull-left icn-sync" title="<spring:message code="crm.proxy.btn.sync.transactiondata" />"></button>'
                  + '<img src="<spring:url value="/images/ajax-loader-spinner-small.gif" />" style="display: none;"/>';
        } else {
          status = "<spring:message code="crm.proxy.unreachable" />";
        }
        $( '.actions', nRow ).html( actionHtmlString );
        $( 'td:eq(2)', nRow ).html( status );
      } );
    }
  } );

  $( $crmInStores ).on( 'click', 'a.clickable', function () {
    var $tr = $( this ).parents( 'tr' );
    var id = $( '.id', $tr ).val();
    $( 'input[name="prevId"]', $crmInStoreDialog ).val( id );
    $( 'input[name="id"]', $crmInStoreDialog ).val( id );
    $( 'input[name="name"]', $crmInStoreDialog ).val( $( '.name', $tr ).val() );
    $( 'input[name="ip"]', $crmInStoreDialog ).val( $( '.ip', $tr ).val() );
    $( 'input[name="tomcatPort"]', $crmInStoreDialog ).val( $( '.tomcatPort', $tr ).val() );
    $( 'input[name="crmProxyRestPrefix"]', $crmInStoreDialog ).val( $( '.crmProxyRestPrefix', $tr ).val() );
    $( 'input[name="crmReconciliationRestPrefix"]', $crmInStoreDialog ).val( $( '.crmReconciliationRestPrefix', $tr ).val() );
    $crmInStoreDialog.modal( 'toggle' );
  } );

  $( '#addCrmInfo' ).click( function () {
    $( 'input[name="prevId"]', $crmInStoreDialog ).val( '' );
    $( 'input[name="id"]', $crmInStoreDialog ).val( '' );
    $( 'input[name="name"]', $crmInStoreDialog ).val( '' );
    $( 'input[name="ip"]', $crmInStoreDialog ).val( '' );
    $crmInStoreDialog.modal( 'toggle' );
  } );

  var $searchCrmInfos = $( '#searchCrmInfos' ).click( function () {
    var formDataObject = $( '.searchForm', $crmInStoreContainer ).toObject( {mode : 'first', skipEmpty : false} );
    var $btnSearch = $( this );
    $btnSearch.prop( 'disabled', true );
    $btnSearch.html( "<spring:message code="searching" />" );
    $crmInStores.ajaxDataTable( 'search', formDataObject, function () {
      $btnSearch.prop( 'disabled', false );
      $btnSearch.html( "<spring:message code="search" />" );
    } );
  } );

  var $crmInStoreDialog = $( '#crmInStoreDialog' ).modal( { show : false } );

  var displayMsg = function ( $el, message, issuccess, append ) {
    var htmlString = '<div class="alert ' + (issuccess ? 'alert-success' : 'alert-error') + '">' +
            '<button type="button" class="close" data-dismiss="alert">&times;</button>'
            + message + '</div>';
    if ( append ) {
      $el.append( htmlString );
    } else {
      $el.html( htmlString );
    }
  };

  var $messagesDiv = $( '.messagesDiv', $crmInStoreDialog );
  $( '#cancelAddCrmInfo' ).click( function () {
    $crmInStoreDialog.modal( 'toggle' );
    $( '.alert', $messagesDiv ).alert( 'close' );
  } );

  var $crmProxyAddForm = $( '#crmProxyAddForm' );
  $( '#saveCrmInfo' ).click( function () {
    $.ajax( {
      "contentType" : 'application/json',
      "type" : "POST",
      "url" : "<spring:url value="/crmproxyinfo/save" />",
      "data" : JSON.stringify( $crmProxyAddForm.toObject( {mode : 'first', skipEmpty : false} ) ),
      "success" : function ( data ) {
        var alertClass = 'alert-success';
        if ( !data.success ) {
          var htmlMessageString = '1. ' + data.result[0];
          for ( var i = 1; i < data.result.length; i++ ) {
            htmlMessageString += '<br />';
            htmlMessageString += (i + 1) + '. ' + data.result[i];
          }
          displayMsg( $messagesDiv, htmlMessageString, false );
        } else {
          displayMsg( $messagesDiv, data.message, true );
          $searchCrmInfos.click();
        }
      },
      "error" : function ( xhr, textStatus, error ) {
        if ( textStatus === 'timeout' ) {
          displayMsg( $messagesDiv, 'The server took too long to send the data.', false );
        } else if ( error ) {
          displayMsg( $messagesDiv, error, false );
        } else {
          displayMsg( $messagesDiv, 'An error occurred on the server. Please try again.', false );
        }
      }
    } );
  } );

  /********************** Start of Sync Ref Data scripts *******************/
  $( $crmInStores ).on( 'click', 'button.btn_syncrefdata', function () {
    var $tr = $( this ).parents( 'tr' );
    var id = $( '.id', $tr ).val();
    var $element = $( this );
    $element.hide();
    $element.next( 'img' ).show();
    var $msgsDiv = $( '.messagesDiv', $crmInStoreContainer );
    $.ajax( {
      "type" : "GET",
      "url" : ctx + '/sync/reference/records/in/crmstore/' + id,
      timeout : 900000, // 15 minutes
      "success" : function ( response ) {
        if ( response.hasOwnProperty( 'exception' ) && response.exception != null ) {
          displayMsg( $msgsDiv, response.exception.defaultMessage, false );
        } else if ( response.hasOwnProperty( 'errorMessage' ) ) {
          displayMsg( $msgsDiv, response.errorMessage, false );
        } else {
          displayMsg( $msgsDiv, response.successMessage, true );
        }
        $element.next( 'img' ).hide();
        $element.show();
      },
      "error" : function ( xhr, textStatus, error ) {
        if ( textStatus === 'timeout' ) {
          displayMsg( $msgsDiv, 'The server took too long to send the data.', false );
        } else if ( error ) {
          displayMsg( $msgsDiv, error, false );
        } else {
          displayMsg( $msgsDiv, 'An error occurred on the server. Please try again.', false );
        }
        $element.show();
        $element.next( 'img' ).hide();
      }
    } );
  } ).on( 'click', 'button.btn_synctxndata', function () {
    var $tr = $( this ).parents( 'tr' );
    var id = $( '.id', $tr ).val();
    var $element = $( this );
    $element.hide();
    $element.next( 'img' ).show();
    var $msgsDiv = $( '.syncTxnMessagesDiv', $crmInStoreContainer );
    $.ajax( {
      "type" : "GET",
      "url" : ctx + '/sync/transaction/records/in/crmstore/' + id,
      timeout : 900000, // 15 minutes
      "success" : function ( response ) {
        if ( response.hasOwnProperty( 'exception' ) && response.exception != null ) {
          displayMsg( $msgsDiv, response.exception.defaultMessage, false );
        } else if ( response.hasOwnProperty( 'errorMessage' ) ) {
          displayMsg( $msgsDiv, response.errorMessage, false );
        } else {
          displayMsg( $msgsDiv, response.successMessage, true );
        }
        $element.next( 'img' ).hide();
        $element.show();
      },
      "error" : function ( xhr, textStatus, error ) {
        if ( textStatus === 'timeout' ) {
          displayMsg( $msgsDiv, 'The server took too long to send the data.', false );
        } else if ( error ) {
          displayMsg( $msgsDiv, error, false );
        } else {
          displayMsg( $msgsDiv, 'An error occurred on the server. Please try again.', false );
        }
        $element.show();
        $element.next( 'img' ).hide();
      }
    } );
  } );

} );
</script>