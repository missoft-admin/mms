package com.transretail.crm.core.dto;


import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.time.DateUtils;
import org.joda.time.LocalDateTime;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.mysema.query.types.expr.BooleanExpression;
import com.transretail.crm.common.service.dto.request.AbstractSearchFormDto;
import com.transretail.crm.core.entity.Complaint.ComplaintCategory;
import com.transretail.crm.core.entity.QComplaint;
import com.transretail.crm.core.util.BooleanExprUtil;


public class ComplaintSearchDto extends AbstractSearchFormDto {

	public static enum ComplaintSearchField { ticketNo, name, email, mobileNo };

	private String ticketNo;
	private String name;
	private String firstName;
	private String lastName;
	private String email;
	private String mobileNo;
	private String storeCode;
	private String filedBy;
	private String assignedTo;
	private String status;
	private Long dateFiledFrom;
	private Long dateFiledTo;
	private ComplaintCategory category;


	@Override
	@JsonIgnore
	public BooleanExpression createSearchExpression() {
		List<BooleanExpression> expr = new ArrayList<BooleanExpression>();
		QComplaint qComplaint = QComplaint.complaint;

		if ( StringUtils.isNotBlank( ticketNo ) ) {
			expr.add( BooleanExprUtil.INSTANCE.isStringPropertyLike( qComplaint.ticketNo, ticketNo ) );
		}
		if ( StringUtils.isNotBlank( name ) ) {
			expr.add( 
            		BooleanExprUtil.INSTANCE.isStringPropertyLike( qComplaint.firstName, name).or( 
            		BooleanExprUtil.INSTANCE.isStringPropertyLike( qComplaint.lastName, name) ) );
		}
		if ( StringUtils.isNotBlank( email ) ) {
			expr.add( BooleanExprUtil.INSTANCE.isStringPropertyLike( qComplaint.email, email ) );
		}
		if ( StringUtils.isNotBlank( mobileNo ) ) {
			expr.add( BooleanExprUtil.INSTANCE.isStringPropertyLike( qComplaint.mobileNo, mobileNo ) );
		}
		if ( StringUtils.isNotBlank( storeCode ) ) {
			expr.add( qComplaint.storeCode.equalsIgnoreCase( storeCode ) );
		}
		if ( StringUtils.isNotBlank( filedBy ) ) {
			expr.add( BooleanExprUtil.INSTANCE.isStringPropertyLike( qComplaint.createUser, filedBy ) );
		}
		if ( StringUtils.isNotBlank( assignedTo ) ) {
			expr.add( BooleanExprUtil.INSTANCE.isStringPropertyLike( qComplaint.assignedTo, assignedTo ) );
		}
		if ( StringUtils.isNotBlank( status ) ) {
			expr.add( qComplaint.status.equalsIgnoreCase( status ) );
		}
		if ( null != dateFiledFrom ) {
			expr.add( qComplaint.dateFiled.goe( new LocalDateTime( DateUtils.truncate( new Date( dateFiledFrom ), Calendar.DATE ).getTime() ) ) );
		}
		if ( null != dateFiledTo ) {
			expr.add( qComplaint.dateFiled.loe( new LocalDateTime( 
					DateUtils.addMilliseconds( DateUtils.ceiling( new Date( dateFiledTo ), Calendar.DATE ), -1 ).getTime() ) ) );
		}
		if( category != null ) {
			expr.add( qComplaint.category.eq(category) );
		}

		return BooleanExpression.allOf( expr.toArray( new BooleanExpression[expr.size()] ) );
	}



	public String getTicketNo() {
		return ticketNo;
	}
	public void setTicketNo(String ticketNo) {
		this.ticketNo = ticketNo;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getMobileNo() {
		return mobileNo;
	}
	public void setMobileNo(String mobileNo) {
		this.mobileNo = mobileNo;
	}
	public String getStoreCode() {
		return storeCode;
	}
	public void setStoreCode(String storeCode) {
		this.storeCode = storeCode;
	}
	public String getFiledBy() {
		return filedBy;
	}
	public void setFiledBy(String filedBy) {
		this.filedBy = filedBy;
	}
	public String getAssignedTo() {
		return assignedTo;
	}
	public void setAssignedTo(String assignedTo) {
		this.assignedTo = assignedTo;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public Long getDateFiledFrom() {
		return dateFiledFrom;
	}
	public void setDateFiledFrom(Long dateFiledFrom) {
		this.dateFiledFrom = dateFiledFrom;
	}
	public Long getDateFiledTo() {
		return dateFiledTo;
	}
	public void setDateFiledTo(Long dateFiledTo) {
		this.dateFiledTo = dateFiledTo;
	}
	public ComplaintCategory getCategory() {
		return category;
	}
	public void setCategory(ComplaintCategory category) {
		this.category = category;
	}

}
