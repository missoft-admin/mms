package com.transretail.crm.common.repo;

import java.io.Serializable;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.querydsl.QueryDslPredicateExecutor;

import com.mysema.query.types.Predicate;

/**
 * @author Allan G. Ramirez (agramirez@exist.com)
 */
public interface CrmQueryDslPredicateExecutor<T, ID extends Serializable> extends JpaRepository<T, ID>, QueryDslPredicateExecutor<T> {
    Page<T> findAll(Predicate predicate, Pageable pageable, JoinDef... joinDefs);
}
