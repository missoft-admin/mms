<%@include file="../../../common/taglibs.jsp"%>


<div id="content_soPrintForm" class="modal hide nofly modal-dialog">

  <div class="modal-content">

    <div class="modal-header">
      <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
      <h4><spring:message code="gc.so.no" />: ${salesOrder.orderNo}</h4>
    </div>

    <div class="modal-body modal-form form-horizontal">
      <div class="hide alert alert-error" id="content_error"><button type="button" class="close" onclick="$(this).parent().hide();">&times;</button></div>

      <div class="main-block row-fluid"><div class="span12"><%@include file="sgmntSoInfo.jsp" %></div></div>
      <div class="main-block row-fluid"><div class="span12"><%@include file="tblSoItemAllocView.jsp" %></div></div>


      <c:if test="${salesOrder.status == 'ALLOCATED'}">
      <div class="main-block row-fluid  print-so-alloc">
        <div class="soorderprint-block">
          <select id="docType">
            <c:forEach var="item" items="${soDocTypes}" >
              <c:choose>
                <c:when test="${item == soDocTypeReceipt}"><option id="soDocTypeReceipt" value="${item}"><spring:message code="SO_DOCTYP_${item}" /></option></c:when>
                <c:otherwise><option value="${item}"><spring:message code="SO_DOCTYP_${item}" /></option></c:otherwise>
              </c:choose>
            </c:forEach>
          </select>
          <c:choose>
	          <c:when test="${empty salesOrder.receiptNo}">
	            <input type="text" id="soDocTypeReceiptField" placeholder="<spring:message code="gc.b2b.report.rct.no" />" class="hide" />
	          </c:when>
	          <c:otherwise>
	            <input type="text" id="soDocTypeReceiptField" placeholder="<spring:message code="gc.b2b.report.rct.no" />" class="hide" disabled="disabled" value="${salesOrder.receiptNo}" data-hasvalue="true" />
	          </c:otherwise>
          </c:choose>
        </div>
        <div class="soorderprint-block control-group checkElements">
          <c:forEach var="item" items="${soDocTypes}" >
          <div class="control-label">
            <label><input type="checkbox" id="checkbox${item}" value="${item}" disabled="disabled" class="docTypeCheckbox checkElement" /><spring:message code="SO_DOCTYP_${item}" /></label>
          </div>
          </c:forEach>
        </div>
      </div>
      </c:if>
    </div>


    <c:url var="url_action" value="/" />
    <form:form id="soPrintForm" name="soAlloc" modelAttribute="soPrintForm" method="POST" action="${url_action}" class="modal-form form-horizontal">

    <div class="modal-footer">
      <c:if test="${salesOrder.status == 'ALLOCATED'}">
	      <input id="printBtn" class="btn btn-primary" type="button" value="<spring:message code="label_print" />"
	        data-url="<c:url value="/giftcard/salesorder/allocate/save/${salesOrder.id}" />" data-soid="${salesOrder.id}" />
	      <input id="forpickupBtn" class="btn btn-primary hide" type="button" value="<spring:message code="label_update" />" data-soid="${salesOrder.id}" />
      </c:if>
      <input id="cancelBtn" class="btn" type="button" value="<spring:message code="label_cancel" />" data-dismiss="modal" />
    </div>
    </form:form>

  </div>


<style type="text/css"> 
<!-- 
  #content_soPrintForm .alert { padding-left: 0px !important; }
  #content_soPrintForm .soorderalloc-block { margin-top: 20px; }
  #content_soPrintForm .checkElement { float: left; margin-right: 5px; }
  #content_soPrintForm .checkElements .controls { margin-left: 150px !important; }
  #content_soPrintForm .checkElements .control-label { width: 150px !important; }
  #content_soPrintForm .pull-center { padding-left: 10px; padding-right: 10px; }
  #content_soPrintForm .modal-footer { text-align: center; } 
  #content_soPrintForm .control-group .controls { margin-top: 5px; margin-left: 110px; }
  #content_soPrintForm .control-group .control-label { width: 100px; text-align: left; }
  #content_soPrintForm .table-col-xlarge { width: 320px; }
--> 
</style>
<script type='text/javascript'>
var SoPrintForm = null;

$(document).ready( function() {

    var errorContainer = $( "#content_soPrintForm" ).find( "#content_error" );
    var $dialog = $( "#content_soPrintForm" );
    var $printBtn = $dialog.find( "input#printBtn" );
    var $forpickupBtn = $dialog.find( "input#forpickupBtn" );

    initDialog();
    initFormFields();

    function disableSaveBtns( disable ) {
    	  $saveBtn.prop( "disabled", disable );
        $cancelAllocBtn.prop( "disabled", disable );
    }

    function initDialog() {
        $dialog.modal({ show : true });
        $dialog.on( "hide", function(e) { if ( e.target === this ) { 
        	  errorContainer.hide(); 
        }});
    }

    function initFormFields() {
    	  $printBtn.click( function(e) {
    		    e.preventDefault();
    		    $.get( $(this).data( "url" ), function( resp ) {
    		    	  var ret = SoPickup.printPickupDocs( $printBtn.data( "soid" ), 
    		    			  $dialog.find( "#docType" ).val(), 
    		    			  $( "#soDocTypeReceiptField" ).val() );
    		    	  $dialog.find( "#checkbox" + $dialog.find( "#docType" ).val() ).prop( "checked", ret );
    		    	  $dialog.find( "#checkbox" + $dialog.find( "#docType" ).val() ).change();
    		    });
    	  });
    	  $dialog.find( ".docTypeCheckbox" ).change( function(e) {
    	      var docTypeChecked = true;
    		    $dialog.find( ".docTypeCheckbox" ).each( function() {
    		    	  docTypeChecked = docTypeChecked && this.checked;
    		    });
    		    if ( docTypeChecked ) {
    		    	  $forpickupBtn.show();
    		    	  $printBtn.hide();
    		    }
    	  });
    	  $dialog.find( "#docType" ).change( function(e) {
		        if ( $(this).find( "option:selected" ).attr( "id" ) == "soDocTypeReceipt" ) {
	              //$( "#soDocTypeReceiptField" ).show();
	              if ( !$( "#soDocTypeReceiptField" ).data( "hasvalue" ) ) {
	                  $printBtn.prop( "disabled", true );
	              }
		        }
		        else {
		            $( "#soDocTypeReceiptField" ).hide();
		            //$( "#soDocTypeReceiptField" ).val( "" );
		        }
    	  });
    	  $( "#soDocTypeReceiptField" ).change( function(e) {
    		    if ( $(this).val() ) {
    		    	  $printBtn.prop( "disabled", false );
    		    }
    		    else {
    		    	  $printBtn.prop( "disabled", true );
    		    }
    	  });
    	  $forpickupBtn.click( function(e) {
    		    e.preventDefault();
    		    SoPickup.updateForPickup( $(this).data( "soid" ), $dialog );
    	  });
    }

    SoPrintForm = {
        show  : function() {
            $dialog.modal( "show" );
        }
    };
  
});
</script>


</div>