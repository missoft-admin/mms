<jsp:root xmlns:c="http://java.sun.com/jsp/jstl/core" xmlns:fn="http://java.sun.com/jsp/jstl/functions"
          xmlns:util="urn:jsptagdir:/WEB-INF/tags/util"
          xmlns:jsp="http://java.sun.com/JSP/Page" xmlns:spring="http://www.springframework.org/tags" version="2.0">

  <jsp:directive.tag import="org.apache.commons.lang3.StringUtils"/>
	<jsp:directive.tag import="com.transretail.crm.core.service.impl.StoreServiceImpl"/>
	<jsp:directive.tag import="com.transretail.crm.core.service.impl.UserServiceImpl"/>
  <jsp:directive.tag import="org.apache.commons.lang3.BooleanUtils"/>
  <jsp:directive.tag import="com.transretail.crm.core.security.model.impl.CustomSecurityUserDetailsImpl"/>
  <jsp:directive.tag import="com.transretail.crm.core.security.util.UserUtil"/>

  <jsp:output omit-xml-declaration="yes"/>
  <jsp:directive.attribute name="userStore" type="java.lang.Boolean" required="false" rtexprvalue="true" description=""/>
  <jsp:directive.attribute name="inventoryLoc" type="java.lang.Boolean" required="false" rtexprvalue="true" description=""/>
  <jsp:directive.attribute name="var" type="java.lang.String" required="false" rtexprvalue="true" description=""/>

  <jsp:scriptlet>
    <![CDATA[
      CustomSecurityUserDetailsImpl currentUser = UserUtil.getCurrentUser();

      Boolean userStoreCode = (Boolean) jspContext.getAttribute( "userStore" );
      if ( BooleanUtils.toBoolean( userStoreCode ) && null != currentUser.getStore() ) {
          jspContext.setAttribute( "showUserStore", true );
          jspContext.setAttribute( "userStoreInfo", currentUser.getStore().getCode() + " - " + currentUser.getStore().getName() );
      }
      Boolean userInventoryLocCode = (Boolean) jspContext.getAttribute( "inventoryLoc" );
      if( BooleanUtils.toBoolean( userInventoryLocCode ) )
    	  jspContext.setAttribute( "showInventoryLoc", true );
      if ( BooleanUtils.toBoolean( userInventoryLocCode ) && StringUtils.isNotBlank(currentUser.getInventoryLocation()) ) {
          jspContext.setAttribute( "inventoryLocInfo", currentUser.getInventoryLocation() + " - " + currentUser.getInventoryLocationName() );
      }
    ]]>
  </jsp:scriptlet>

      <c:if test="${ showUserStore and not empty userStoreInfo }">
        <spring:message var="label_userStore" code="user_label_storelocation" />
        <c:set var="userStoreVar" value="${label_userStore} : ${userStoreInfo}" />
      </c:if>
      <c:choose>
      <c:when test="${ showInventoryLoc and not empty inventoryLocInfo }">
        <spring:message var="label_inventoryLoc" code="user_prop_inventory" />
        <c:set var="inventoryLocVar" value="${label_inventoryLoc} : ${inventoryLocInfo}" />
      </c:when>
      <c:when test="${ showInventoryLoc }">
        <spring:message var="label_inventoryLoc" code="user_prop_inventory" />
        <spring:message var="inventoryLocInfo" code="msg_not_specified" />
        <c:set var="inventoryLocVar" value="${label_inventoryLoc} : ${inventoryLocInfo}" />
      </c:when>
      </c:choose>
  <c:choose>
    <c:when test="${ empty var }"><c:out value="${userStoreVar} ${inventoryLocVar}" /></c:when>
    <c:otherwise></c:otherwise>
  </c:choose>

</jsp:root>