package com.transretail.crm.core.entity;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.envers.Audited;

/**
 * @author Allan G. Ramirez (agramirez@exist.com)
 */
@Audited
@Entity
@Table(name = "CRM_PROMOTION_PRODUCT_GRP")
public class PromotionProductGroup extends PromotionTargetGroup<String> implements Serializable {
    private static final long serialVersionUID = 2961695470633615954L;
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "PRODUCT_GRP_ID")
    private ProductGroup productGroup;

    public ProductGroup getProductGroup() {
        return productGroup;
    }

    public void setProductGroup(ProductGroup productGroup) {
        this.productGroup = productGroup;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        PromotionProductGroup that = (PromotionProductGroup) o;

        if (!productGroup.equals(that.productGroup)) return false;
        if (!getPromotion().equals(that.getPromotion())) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = getPromotion().hashCode();
        result = 31 * result + productGroup.hashCode();
        return result;
    }
}
