<%@ include file="../common/taglibs.jsp" %>

	<spring:message code="label_com_transretail_crm_entity_customermodel_membertype" var="memberType"/>
	<spring:message code="label_save" var="saveLabel" />
	<spring:message code="label_edit" var="editLabel" />
	<spring:message code="label_cancel" var="cancelLabel" />
	<div class="row-fluid">
	<fieldset class="">
	    <div class="page-header page-header2">
		    <h1><spring:message code="points_config" /></h1>
		</div>
		<div class="row-fluid well well-blue">
			<div class="span10">
				<h3><spring:message code="label_point_value" />: </h3>
				<spring:message code="label_point_value_description" arguments="${pointsConfig.pointValue}" /> 
				<p><![CDATA[&nbsp;]]></p>
				<h3 style="margin-top:20px"><spring:message code="label_point_suspended_types" />:</h3>
				<c:choose>
					<c:when test="${not empty pointsConfig.suspendedTypes}">
						<ul>
						<c:forEach items="${pointsConfig.suspendedTypes}" var="type">
							<li>${type.description}</li>
						</c:forEach>
						</ul>
					</c:when>
					<c:otherwise>
						<spring:message arguments="${memberType}" code="entity_not_found"/>
					</c:otherwise>
				</c:choose>
			</div>
			<div class="">
				<input type="button" class="btn btn-small pull-right" id="editConfig" value="${editLabel}"/>
			</div>
		</div>
	</fieldset>
	
	<div id="formDialog" class="modal  hide nofly">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
				  <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
					<h4><spring:message code="points_config_edit" /> </h4>
				</div>
				<div class="modal-body container-fluid">
					<div class="row-fluid">
					<form:form modelAttribute="pointsConfig" action="${pageContext.request.contextPath}/member/points/config">
						<div class="span6">
							<label for="pointValue"><b class="required">*</b><spring:message code="label_point_value" />:</label>
							<form:input path="pointValue" id="pointValue" cssClass="floatInput"/><br />
							<spring:message code="label_point_value_description_template" htmlEscape="false" arguments="<span id='tempPointsDesc'>${pointsConfig.pointValue} </span>"/>
						</div>
						<div class="span6">
							<label for="suspendedType"><spring:message code="label_point_suspended_types" />:</label>
							<div class="well" style="overflow-y: auto; min-height: 60px">
								<div class="checkbox">
									<form:checkboxes items="${memberTypes}" itemValue="code" itemLabel="description" path="suspendedTypes" />
								</div>
							</div>
						</div>
					</form:form>
					</div>
				</div>
				<div class="modal-footer container-btn">
				  <!--<input type="submit" class="btn btn-primary" id="button_save" value="${saveLabel}" onclick="javascript: saveConfig();" />
				  <input type="submit" class="btn" id="button_cancel" value="${cancelLabel}" />-->
				  <button type="submit" class="btn btn-primary" id="button_save" value="${saveLabel}" onclick="javascript: saveConfig();">${saveLabel}</button>
          <button type="button" id="formCancel" data-dismiss="modal" class="btn btn-default"><spring:message code="label_cancel"/></button>
       	 </div>
			</div>
		</div>
	</div>
	
	<fieldset class="">
		<jsp:include page="memberSearchbox.jsp" />
	
		<legend><spring:message code="points_config_eligible_member" /></legend>
		<div style="overflow-y: auto; height: 500px;">
        
        <div id="content_memberList">

          <div id="list_member"></div>
        
        </div>
		</div>
	</fieldset>
  
  <link href="<spring:url value="/css/dataTables.css" />" rel="stylesheet">
  <script src="<spring:url value="/js/jquery/jquery.dataTables.js" />" ></script>
  <script src="<spring:url value="/js/jquery/jquery.dataTables.linkLengthMenu.js" />"></script>
  <script src="<spring:url value="/js/jquery/jquery.dataTables.customPaging.js" />"></script>
  <script src="<spring:url value="/js/jquery/ajax-datatable.js" />" ></script>
  <script src="<spring:url value="/js/common/form2js.js" />" ></script>
  <script src="<spring:url value="/js/common/json2.js" />" ></script>
  <script src="<spring:url value="/js/jquery/jquery.toObject.js" />" ></script>
  <script src="<spring:url value="/js/jquery/jquery.numberInput.js" />" ></script>
  
        <script type="text/javascript">
        
        
        </script>
		
	<!--script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/viewspecific/points/config.js" /-->
	<script type="text/javascript">
	var form = null;
	$(document).ready(function(){
		var dataTable = $("#list_member").ajaxDataTable({
	          'autoload'  : true,
	          'ajaxSource' : "<c:url value="/member/points/config/list" />",
	          'columnHeaders' : [
	          "<spring:message code="label_com_transretail_crm_entity_customermodel_accountnumber"/>",
	          "<spring:message code="label_com_transretail_crm_entity_customermodel_username"/>",
	          "<spring:message code="label_com_transretail_crm_entity_customermodel_firstname"/>",
	          "<spring:message code="label_com_transretail_crm_entity_customermodel_lastname"/>"
	          ],
	          'modelFields' : [
	             {name : 'accountId', sortable : true},
	             {name : 'username', sortable : true},
	             {name : 'firstName', sortable : true},
	             {name : 'lastName', sortable : true}
	           ]
	        });  

		$("#search_submit").click(function(e) {
			e.preventDefault();
			
			$("#search_memberField").data("name", $("#search_memberCriteria").val());
			
			$(".memberSearchField").each(function() {
				$(this).attr("name", $(this).data("name"));
			});
			
			var formDataObj = $("#searchModel").toObject( { mode : 'first', skipEmpty : true } );
			
			dataTable.ajaxDataTable( 
				'search', 
				formDataObj);
			
			$(".memberSearchField").removeAttr("name");
		});
		
		form = $("#formDialog");
		form.modal({
			show	:	false
		});

// 		$("#pointsSearchClearButton").click(function() {
// 			$(".pointsSearchDropdown").val("");
// 			$("#search_memberField").val("");
// 		});

		$("#editConfig").click(function(){
			form.modal("show");
		});

		$("#button_cancel").click(function(){
			form.modal("hide");
		});

		$("#pointValue").change(function() {
			changeDesc($(this).val() + " ");
		});

		$("#pointValue").focusout(function() {
			changeDesc($(this).val() + " ");
		});
		$(".floatInput").numberInput({
            "type" : "float"
        });
		$(".intInput").numberInput({
            "type" : "int"
        });
	});

	function saveConfig() {
		$( "#button_save" ).prop( "disabled", true );
		$.ajax({
			type		: "POST",
			url			: $("#pointConfig").attr("action"),
			dataType	: "json",
			contentType	: "application/json",
			data		: parseData(),
			success		: function(response) {
				form.modal("hide");
			  $( "#button_save" ).prop( "disabled", false );
				window.location.reload();
			}
		});
	}

	function changeDesc(val) {
		$("#tempPointsDesc").html(val);
	}

	function parseData() {
		var data = new Object();
		data["pointValue"] = $("input[name='pointValue']").val();

		var suspended = [];
		$.each($("input:checked"), function(k, v) {
			suspended.push($(v).val());
		});
		data["suspendedTypes"] = suspended;

		return JSON.stringify(data);
	}
	</script>
	<style>
	h3 {
	    margin-top: 0;
	    line-height: 18px;
	}
	#DataTables_Table_0.table tbody tr td:last-child  {
        border-left: none;
    }
	</style>
	</div>
