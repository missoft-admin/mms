package com.transretail.crm.web.controller;


import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import com.sun.org.apache.bcel.internal.generic.I2F;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import com.google.common.collect.Lists;
import com.transretail.crm.core.service.CodePropertiesService;
import com.transretail.crm.core.service.LookupService;
import com.transretail.crm.giftcard.dto.GcAllocDto;
import com.transretail.crm.giftcard.dto.GiftCardInventoryDto;
import com.transretail.crm.giftcard.entity.support.GiftCardInventoryStatus;
import com.transretail.crm.giftcard.service.GiftCardServiceRequestService;
import com.transretail.crm.giftcard.service.SalesOrderService;
import com.transretail.crm.giftcard.service.SoAllocService;
import com.transretail.crm.giftcard.service.impl.SoAllocServiceImpl;
import com.transretail.crm.web.controller.util.ControllerResponse;

import static org.springframework.web.bind.annotation.RequestMethod.*;


@Controller
@RequestMapping( "/giftcard/salesorder/allocate" )
public class GiftCardSoAllocController {

	private static final String VIEW_FORM 		= "view/giftcard/salesorder/allocate";
	private static final String UIATTR_SO		= "salesOrder";
	private static final String BARCODE_RANGE_SEPARATOR = "barcodeRangeSeparator";
	private static final String BARCODE_RANGE_SEPARATOR_VALUE = SoAllocServiceImpl.BARCODE_RANGE_SEPARATOR_VALUE;



	@Autowired
	SoAllocService service;
	@Autowired
	SalesOrderService salesOrderService;
	@Autowired
	LookupService lookupService;
	@Autowired
	CodePropertiesService codePropertiesService;
    @Autowired
    private MessageSource messageSource;



    @InitBinder
	void initBinder( WebDataBinder binder ) {
		DateFormat df = new SimpleDateFormat( "dd MMM yyyy" );
		df.setLenient( false );
		binder.registerCustomEditor( Date.class, new CustomDateEditor( df, true ) );
	}

    @RequestMapping( value="/{id}", produces = "text/html" )
    public ModelAndView showForm( @PathVariable(value="id") Long id ) {
    	ModelAndView mv = new ModelAndView( VIEW_FORM );
    	mv.addObject( UIATTR_SO, service.setSoItemsAllocsDto( id ) );
    	mv.addObject( BARCODE_RANGE_SEPARATOR, BARCODE_RANGE_SEPARATOR_VALUE );
        return mv;
    }

    @RequestMapping( value="/scan/{barcodeStart}/{barcodeEnd}", produces="application/json; charset=utf-8", method= GET )
	public @ResponseBody ControllerResponse scan(
    		@PathVariable(value="barcodeStart") String barcodeStart,
    		@PathVariable(value="barcodeEnd") String barcodeEnd ) {

    	ControllerResponse resp = new ControllerResponse();
    	resp.setSuccess( true );
    	resp.setResult( service.getQuantity( barcodeStart, barcodeEnd ) );
    	return resp;
    }

    @RequestMapping( value="/scan/{barcode}", produces="application/json; charset=utf-8", method= GET )
	public @ResponseBody ControllerResponse scan(
    		@PathVariable(value="barcode") String barcode ) {

    	ControllerResponse resp = new ControllerResponse();
    	List<String> errs = Lists.newArrayList();
    	GiftCardInventoryDto gcDto = service.getGiftCard( barcode );
    	if ( null == gcDto ) {
    		errs.add( messageSource.getMessage( "gc.so.err.barcode.notexist", null, LocaleContextHolder.getLocale() ) );
    	}
    	else if ( gcDto.getStatus() != GiftCardInventoryStatus.IN_STOCK ) {
    		errs.add( messageSource.getMessage( "gc.so.err.barcode.notvalid.alloc", null, LocaleContextHolder.getLocale() ) );
    	}
    	if ( CollectionUtils.isNotEmpty( errs ) ) {
    		resp.setResult( errs );
    	}
    	else {
        	resp.setSuccess( true );
        	resp.setResult( service.getQuantity( barcode, null ) );
    	}
    	return resp;
    }

    @RequestMapping( value="/preallocate/{soId}/{barcodeStart}/{barcodeEnd}", produces="application/json; charset=utf-8", method= GET )
	public @ResponseBody ControllerResponse preallocate(
    		@PathVariable(value="soId") Long soId,
    		@PathVariable(value="barcodeStart") String barcodeStart,
    		@PathVariable(value="barcodeEnd") String barcodeEnd ) {

    	ControllerResponse resp = new ControllerResponse();
    	List<String> errs = Lists.newArrayList();
    	GcAllocDto dto = null;
    	if ( service.isValidAlloc( soId, barcodeStart, barcodeEnd, errs, dto ) ) {
        	resp.setResult( /*dto*/service.preallocate( soId, barcodeStart, barcodeEnd ) );
        	resp.setSuccess( true );
    	}
    	else {
    		resp.setResult( errs );
    		resp.setSuccess( false );
    	}
    	return resp;
    }

    @RequestMapping( value="/preallocate/{soId}/{barcode}", produces="application/json; charset=utf-8", method= GET )
	public @ResponseBody ControllerResponse preallocate(
    		@PathVariable(value="soId") Long soId,
    		@PathVariable(value="barcode") String barcode ) {

    	ControllerResponse resp = new ControllerResponse();
    	List<String> errs = Lists.newArrayList();
    	GcAllocDto dto = null;
    	if ( service.isValidAlloc( soId, barcode, null, errs, dto ) ) {
        	resp.setResult( /*dto*/service.preallocate( soId, barcode, null ) );
        	resp.setSuccess( true );
    	}
    	else {
    		resp.setResult( errs );
    		resp.setSuccess( false );
    	}
    	return resp;
    }

    @RequestMapping( value="/preallocate/cancel/{soId}", produces="application/json; charset=utf-8", method= POST )
	public @ResponseBody ControllerResponse cancelPreallocation(
    		@PathVariable(value="soId") Long soId,
    		@RequestParam(value = "barcodeRange[]") String[] barcodeRange ) {

    	ControllerResponse resp = new ControllerResponse();
    	service.cancelPreAllocation( soId, barcodeRange );
    	resp.setSuccess( true );
    	return resp;
    }

    @RequestMapping( value="/save/{soId}", produces="application/json; charset=utf-8", method= GET )
	public @ResponseBody ControllerResponse saveAllocation( @PathVariable(value="soId") Long soId ) {

    	ControllerResponse resp = new ControllerResponse();
    	service.saveAllocation( soId );
    	return resp;
    }

    @RequestMapping( value="/remove/{soId}", produces="application/json; charset=utf-8", method= GET )
	public @ResponseBody ControllerResponse removeAllocation( @PathVariable(value="soId") Long soId ) {

    	ControllerResponse resp = new ControllerResponse();
    	service.removeAllocation( soId );
    	return resp;
    }

    @RequestMapping(value = "/autoscan/{soId}", produces="application/json; charset=utf-8", method = GET)
    @ResponseBody
    public ControllerResponse autoScan(@PathVariable("soId") Long soId) {

    	ControllerResponse response = new ControllerResponse();
    	response.setSuccess(true);
    	List<String> errs = Lists.newArrayList();

    	List<GcAllocDto> allocDto = service.autoScanAndValidate(soId);

		if (allocDto.isEmpty()) {
			response.setSuccess(false);
			errs.add( messageSource.getMessage( "gc.so.err.barcode.notexist", null, LocaleContextHolder.getLocale() ) );
			response.setResult(errs);
		}

		List<GcAllocDto> validAlloc = Lists.newArrayList();

		for (GcAllocDto dto : allocDto) {

			if ( service.isValidAlloc( soId, dto.getBarcodeStart(), dto.getBarcodeEnd(), errs, dto ) ) {

				validAlloc.add(/*dto*/service.preallocate( soId, dto.getBarcodeStart(), dto.getBarcodeEnd() ) );
				response.setSuccess( true );
			}
			else {
				response.setResult( errs );
				response.setSuccess( false );
			}
		}

		if (response.isSuccess()) {

			response.setResult(validAlloc);
		}

        return response;
    }



    @Autowired
    public GiftCardServiceRequestService testSevice; 
    @RequestMapping( value="/test/{gcId}", produces="application/json; charset=utf-8", method= GET )
	public @ResponseBody ControllerResponse test( @PathVariable(value="gcId") String cardNo ) {

    	testSevice.retrieveGiftCardInfoByCardNo( cardNo );
    	return null;
    }

}