package com.transretail.crm.giftcard.service;


import java.util.List;

import com.transretail.crm.giftcard.dto.GcAllocDto;
import com.transretail.crm.giftcard.dto.GiftCardInventoryDto;
import com.transretail.crm.giftcard.dto.SalesOrderDto;
import com.transretail.crm.giftcard.entity.GiftCardInventory;


public interface SoAllocService {

	SalesOrderDto setSoItemsAllocsDto(Long soId);

	GcAllocDto getQuantity(String barcodeOrStart, String barcodeEnd);

	GcAllocDto preallocate(Long soId, String barcodeOrStart, String barcodeEnd);

	List<GcAllocDto> autoScanAndValidate(Long soId);

	void cancelPreAllocation(Long soId, String[] barcodeRange);

	boolean isValidAlloc(Long soId, String barcodeOrStart, String barcodeEnd, List<String> errs, GcAllocDto dto);

	void saveAllocation(Long soId);

	void removeAllocation(Long soId);

	List<GiftCardInventory> getAllocatedGcs(Long soId);

	GiftCardInventoryDto getGiftCard(String barcode);

}