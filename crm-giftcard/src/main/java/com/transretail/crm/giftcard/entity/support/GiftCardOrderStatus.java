package com.transretail.crm.giftcard.entity.support;

/**
 *
 */
public enum GiftCardOrderStatus {
    APPROVED, FOR_APPROVAL, DRAFT, PARTIAL, FULL, BARCODING, GENERATED
}
