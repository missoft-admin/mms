<%@ include file="../../common/taglibs.jsp" %>

    <style type="text/css">
  <!--
    #productTable tr td:last-child {
    border-left: 0px;
    }

    #detailsTable tr td:last-child {
    border-left: 0px;
    }
    
    #receivedTable tr td:last-child {
    border-left: 0px;
    }
  -->
  </style>
  <style type="text/css">
  <!--
    #content_moList .dataTable tr td:last-child {
      min-width: 150px;
      padding-top: 10px;
    }
  -->
  </style>
  
  
  <spring:message var="typeName" code='label_manufacture_order'/>
  <spring:url value="/resources/images/delete.png" var="delete_image_url" />
   
  <div class="page-header page-header2">
      <h1><spring:message code="label_manufacture_orders" /></h1>
  </div>

  <div>
  	<jsp:include page="mosearch.jsp" />
  </div>

  <div>
	<button class="btn btn-primary modalBtn pull-right" id="" data-url="${pageContext.request.contextPath}/manufactureorder/modal/">
		<spring:message code="global_menu_new" arguments="${typeName}"/>
	</button>
  </div>
  
  <div class="clearfix"></div><br/>
  
    <div id="content_moList">

    <div id="list_mo" 
      data-url="<c:url value="/manufactureorder/view/${id}" />"
      data-hdr-mo-no="<spring:message code="mo_prop_mono" />"
      data-hdr-mo-date="<spring:message code="mo_prop_modate" />"
      data-hdr-po-no="<spring:message code="mo_prop_pono" />" 
      data-hdr-supplier="<spring:message code="mo_prop_supplier" />" 
      data-hdr-order-date="<spring:message code="mo_prop_orderdate" />"
      data-hdr-status="<spring:message code="mo_prop_status" />"
      data-is-approver="${isApprover}"
      data-status-draft="${draftStatus}"
      data-status-approved="${approvedStatus}"
      data-status-for-approval="${forApprovalStatus}"></div>
  
    </div>
	
    <div class="hide">
      
      <div id="editBtn">
      <button type="button" id="" data-url="${pageContext.request.contextPath}/manufactureorder/modal/%ITEMID%" class="tiptip btn pull-left icn-edit modalBtn" title="<spring:message code="label_edit"/>"><spring:message code="label_edit"/></button>
      </div>
      
      <div id="viewBtn">
      <c:if test="${isApprover}">
      <button type="button" id="" data-url="${pageContext.request.contextPath}/manufactureorder/modal/%ITEMID%" class="tiptip btn pull-left icn-view modalBtn" title="<spring:message code="label_view"/>"><spring:message code="label_view"/></button>
      </c:if>
      </div>
      
      <div id="receiveBtn">
      <button type="button" id="" data-url="${pageContext.request.contextPath}/manufactureorder/receivemodal/%ITEMID%" class="tiptip btn pull-left icn-recieve modalBtn" title="<spring:message code="label_receive"/>"><spring:message code="label_receive"/></button>
      </div>
      
      <div id="otherBtn">
      
      <button type="button" id="" data-url="${pageContext.request.contextPath}/loyaltycardinventory/print/%MONO%" class="tiptip btn pull-left icn-print printBtn" title="<spring:message code="label_mo_print"/>"><spring:message code="label_mo_print"/></button>
      <button type="button" id="" data-url="${pageContext.request.contextPath}/manufactureorder/printmanufactureorder/%MONO%" class="tiptip btn pull-left icn-print printMoBtn" title="<spring:message code="label_mo_printmo"/>"><spring:message code="label_mo_printmo"/></button>
      <button type="button" id="" data-url="${pageContext.request.contextPath}/manufactureorder/printreceiveall/%MONO%" class="tiptip btn pull-left icn-print printMoBtn" title="<spring:message code="label_mo_printmoreceipt"/>"><spring:message code="label_mo_printmoreceipt"/></button>
      </div>
      
      <div id="deleteBtn">
      <c:set var="delete_confirm_msg"><spring:escapeBody 
            javaScriptEscape="true"><spring:message
            code="entity_delete_confirm" 
      /></spring:escapeBody></c:set><button type="button" id="" 
      class="tiptip btn pull-left icn-delete" title="<spring:message code="label_delete"/>" onclick="getConfirm('${delete_confirm_msg}',function(result) { if(result) $('form#deleteForm%ITEMID%').submit(); });"><spring:message code="label_delete"/></button>
      <div class="hide">
        <spring:url value="/manufactureorder/%ITEMID%"
          var="delete_form_url" /> 
        <form:form id="deleteForm%ITEMID%" action="${delete_form_url}" method="DELETE">
          <spring:message arguments="${typeName}" code="entity_delete"
            var="delete_label" htmlEscape="false" />
          <c:if test="${not empty param.page}">
            <input name="page" type="hidden" value="1" />
          </c:if>
          <c:if test="${not empty param.size}">
            <input name="size" type="hidden"
              value="${fn:escapeXml(param.size)}" />
          </c:if>
        </form:form>
        </div>
      </div>
    
    
    </div>
  
  <jsp:include page="../../common/confirm.jsp" />
  
  <div class="modal hide  groupModal nofly" data-keyboard="false" tabindex="-1" role="dialog" aria-hidden="true" id="manufactureOrderModal"></div>
  
  <link href="<spring:url value="/css/dataTables.css" />" rel="stylesheet">
  <script src="<spring:url value="/js/jquery/jquery.dataTables.js" />" ></script>
  <script src="<spring:url value="/js/jquery/jquery.dataTables.linkLengthMenu.js" />"></script>
  <script src="<spring:url value="/js/jquery/jquery.dataTables.customPaging.js" />"></script>
  <script src="<spring:url value="/js/jquery/ajax-datatable.js" />" ></script>
  <script src="<spring:url value="/js/common/form2js.js" />" ></script>
  <script src="<spring:url value="/js/common/json2.js" />" ></script>
  <script src="<spring:url value="/js/jquery/jquery.toObject.js" />" ></script>
  
  <script src="<spring:url value="/js/jquery/jquery.numberInput.js" />" ></script>
  <script src="<c:url value="/js/bootstrap/bootstrap-datepicker.js"/>" type='text/javascript'><![CDATA[&nbsp; ]]></script>
  <link href="<c:url value="/css/bootstrap/datepicker.css" />" rel="stylesheet" />
  <link href="<c:url value="/styles/temp.css" />" rel="stylesheet" />
  <script src="<spring:url value="/js/jquery/jquery.bindWithDelay.js" />"></script>
  <script src="<spring:url value="/js/jquery/jquery.ajaxQueue.js" />"></script>
  <script src="<spring:url value="/js/viewspecific/loyalty/manufactureorder.js"/>"></script>
