package com.transretail.crm.web.controller;

import java.util.Locale;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.io.output.ByteArrayOutputStream;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.context.support.MessageSourceResourceBundle;

import com.transretail.crm.common.reporting.exception.ReportException;
import com.transretail.crm.common.reporting.jasper.JRProcessor;
import com.transretail.crm.common.reporting.service.ReportService;

import net.sf.jasperreports.engine.JRParameter;

/**
 *
 */
public abstract class AbstractReportsController {
    protected Logger logger = LoggerFactory.getLogger(getClass());
    @Autowired
    private ReportService reportService;
    @Autowired
    private MessageSource messageSource;

    protected void renderReport(HttpServletResponse response, JRProcessor jrProcessor) throws ReportException {
        logger.debug("Generating [" + jrProcessor.getReportFileName() + "]");
        Locale locale = LocaleContextHolder.getLocale();
        if (!jrProcessor.hasParameter(JRParameter.REPORT_LOCALE)) {
            jrProcessor.addParameter(JRParameter.REPORT_LOCALE, locale);
        }
        if (!jrProcessor.hasParameter(JRParameter.REPORT_RESOURCE_BUNDLE)) {
            jrProcessor.addParameter(JRParameter.REPORT_RESOURCE_BUNDLE, new MessageSourceResourceBundle(messageSource, locale));
        }

        try {
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            reportService.exportPdfReport(jrProcessor, baos);

            response.setHeader("Cache-Control", "no-cache");
            // Write content type and also length (determined via byte array).
            response.setContentType("application/pdf");
            response.setContentLength(baos.size());
            // Flush byte array to servlet output stream.
            ServletOutputStream out = response.getOutputStream();
            baos.writeTo(out);
            out.flush();
        } catch (Exception e) {
            logger.error("Error in generaton of pdf report", e);
            throw new ReportException(e);
        }
    }
    
    protected void renderExcelReport(HttpServletResponse response, JRProcessor jrProcessor, String filename) throws ReportException {
        logger.debug("Generating [" + jrProcessor.getReportFileName() + "]");
        Locale locale = LocaleContextHolder.getLocale();
        if (!jrProcessor.hasParameter(JRParameter.REPORT_LOCALE)) {
            jrProcessor.addParameter(JRParameter.REPORT_LOCALE, locale);
        }
        if (!jrProcessor.hasParameter(JRParameter.REPORT_RESOURCE_BUNDLE)) {
            jrProcessor.addParameter(JRParameter.REPORT_RESOURCE_BUNDLE, new MessageSourceResourceBundle(messageSource, locale));
        }
        
        jrProcessor.addParameter(JRParameter.IS_IGNORE_PAGINATION, Boolean.TRUE);

        try {
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            reportService.exportExcelReport(jrProcessor, baos);

            response.setHeader("Content-Disposition", "attachment;filename=\"" + filename + ".xlsx\"");
            response.setContentType("application/vnd.ms-excel");
            response.setContentLength(baos.size());
            ServletOutputStream out = response.getOutputStream();
            baos.writeTo(out);
            out.flush();
        } catch (Exception e) {
            logger.error("Error in generaton of excel report", e);
            throw new ReportException(e);
        }
    }
}
