package com.transretail.crm.core.dto;


import com.transretail.crm.core.entity.ApprovalMatrixModel;
import com.transretail.crm.core.entity.UserModel;
import com.transretail.crm.core.entity.enums.ModelType;


public class ApprovalMatrixDto {

	private Long id;
	private String username;
    private String name;
    private String firstName;
    private String lastName;
    private ModelType modelType;
    private UserModel user;
    private Double amount;

	public ApprovalMatrixDto() {}
	public ApprovalMatrixDto( ApprovalMatrixModel inModel ) {
        this.id = inModel.getId();
        this.modelType = inModel.getModelType();
        this.amount = inModel.getAmount();
        this.user = inModel.getUser();
		if ( this.user != null ) {
            this.username = this.user.getUsername();
            this.firstName = this.user.getFirstName();
            this.lastName = this.user.getLastName();
            this.name = this.firstName + " " + this.lastName;
            this.user.setRolePermissions(null);
		}
	}

	public ApprovalMatrixModel toModel() {
		return toModel( new ApprovalMatrixModel() );
	}

	public ApprovalMatrixModel toModel( ApprovalMatrixModel inModel ) {
        inModel.setModelType(this.modelType);
		inModel.setAmount(this.amount);
		inModel.setUser(this.user);
		return inModel;
	}





	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public ModelType getModelType() {
		return modelType;
	}

	public void setModelType(ModelType modelType) {
		this.modelType = modelType;
	}

	public UserModel getUser() {
		return user;
	}

	public void setUser(UserModel user) {
		this.user = user;
	}

	public Double getAmount() {
		return amount;
	}

	public void setAmount(Double amount) {
		this.amount = amount;
	}
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
}
