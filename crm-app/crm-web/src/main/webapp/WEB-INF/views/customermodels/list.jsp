<?xml version="1.0" encoding="UTF-8" standalone="no"?>

<style type="text/css">
<!--
#list_member .dataTable tr td:last-child, #list_member .dataTable tr th:last-child {
  min-width: 225px;
}
#list_member .dataTable tr td:nth-child(5), #list_member .dataTable tr th:nth-child(5) {
  min-width: 64px !important;
}
#list_member .dataTable tr td:nth-child(3), #list_member .dataTable tr th:nth-child(3) {
  min-width: 200px;
}
#list_member .dataTable tr td:nth-child(2), #list_member .dataTable tr th:nth-child(2) {
  min-width: 90px !important;
}
#list_member .dataTable tr td:nth-child(1), #list_member .dataTable tr th:nth-child(1) {
  min-width: 90px !important;
}
-->
</style>
<%@ include file="../common/taglibs.jsp"%>

<sec:authorize var="isHelpDesk" ifAnyGranted="ROLE_HELPDESK"/>
<div id="isNotHelpDesk" data-grant="${!isHelpDesk}"></div>
<sec:authentication property="principal.inventoryLocation" var="inventoryLocation" />

<spring:url value="/resources/images/show.png" var="show_image_url" />
<spring:url value="/resources/images/update.png" var="update_image_url"/>
<spring:url value="/resources/images/delete.png" var="delete_image_url"/>

<div class="page-header page-header2">
<h1><spring:message code="menutab_membermgmt" /></h1>
</div>

<div id="contentError" class="hide alert alert-error">
  <button type="button" class="close" onclick="$(this).parent().hide();">&times;</button>
  <div><form:errors path="*"/></div>
</div>
          
<div class="control-group">
    <!-- var-form_url needed for the pointsSearchbox -->
    <c:set var="form_url" value="/customermodels/search" scope="request" />
    <jsp:include page="/WEB-INF/views/points/memberSearchbox.jsp" />
</div>
<c:if test="${!isHelpDesk}">
<div class="pull-right mb20">
    <button id="btn_createMember" class="btn btn-primary" onclick="javascript: createCustomer( '${pageContext.request.queryString}' );">
        <spring:message
            code="label_com_transretail_crm_entity_accountmodel_customer_individual" var="memberLabel" />
        <spring:message code="global_menu_new" arguments="${memberLabel}" />
    </button>
    <button class="btn btn-primary" onclick="javascript: createProfessional( '${pageContext.request.queryString}' );">
        <spring:message
            code="label_com_transretail_crm_entity_accountmodel_professional"
            var="argument" /> <spring:message code="global_menu_new"
            arguments="${argument}" />
    </button>
</div>
</c:if>

<div class="clearfix"></div>
<div class="modal hide groupModal nofly" tabindex="-1" role="dialog" aria-hidden="true" id="form_export"></div>

<button type="button" id="printList" class="btn btn-print" data-url="<spring:url value="/customermodels/printList/validate" />"><spring:message code="label_com_transretail_crm_entity_customermodel_print_list" /></button>

<div id="content_memberList">

  <div id="list_member"
    data-user-inventory="${inventoryLocation}"
    data-no-inventorylocation-msg="<spring:message code="loyalty_msg_invalidlocation" />"
    data-url="<c:url value="/customermodels/list" />"
    data-hdr-username="<spring:message code="label_com_transretail_crm_entity_customermodel_username" />"
    data-hdr-accountno="<spring:message code="label_com_transretail_crm_entity_customermodel_accountnumber" />"
    data-hdr-member="<spring:message code="label_com_transretail_crm_entity_customermodel" />"
    data-hdr-store="<spring:message code="label_com_transretail_crm_entity_customermodel_registeredstore" />"
    data-hdr-status="<spring:message code="label_com_transretail_crm_entity_customermodel_status" />"
    data-deactivate-confirm="<spring:message code="member_confirm_deactivate" />"
    ></div>

</div>

<div class="hide">

<span id="mainBtn" style="width: 15%">

    <spring:url value="/member/points/%ACCOUNTID%" var="url_showPoints"/>
    <a href="${url_showPoints}" class="btn btn-primary pull-left tiptip icn-view" title="<spring:message code="points_label_showpoints" />"><spring:message code="points_label_showpoints" /></a>

    <spring:message arguments="${memberLabel}" code="entity_update" var="update_label" htmlEscape="false"/>
    <a href="#" onclick="updateMember( '%ID%', '${pageContext.request.queryString}'  );" class="btn pull-left tiptip icn-edit" title="<spring:message code="label_update" />">
      <spring:message code="label_update" />
    </a>

  <div id="btnPrepaid">
    <input type="button" id="prepaidCard" data-url="<c:url value="customermodels/viewPrepaid" />/%ID%" value="<spring:message code="label_export" />" class="btn prepaidCardBtn icn-view tiptip pull-left" title="<spring:message code="label_prepaid_cards_view" />" />
  </div>
</span>

<span id="otherBtn">

      <spring:message arguments="${memberLabel}" code="entity_updatepin" var="updatepin_label" htmlEscape="false" />
  <a class="btn btn-primary tiptip pull-left icn-pin" href="#" onclick="updatePin( '%ID%', '${pageContext.request.queryString}'  );"
      alt="${fn:escapeXml(updatepin_label)}"
      title="${fn:escapeXml(updatepin_label)}">${updatepin_label}</a>
  <span id="supplementaryBtn">
  <spring:message arguments="${memberLabel}" code="entity_createsupplement" var="supplement_label" htmlEscape="false"/>
  <a class="btn tiptip pull-left icn-supplement" href="#" onclick="createSupplement( '%ID%', '${pageContext.request.queryString}'  );"
      alt="${fn:escapeXml(supplement_label)}" title="${fn:escapeXml(supplement_label)}">
      ${supplement_label}
  </a>
  </span>
  <a class="btn-primary pull-left tiptip icn-card" href="#" onclick="javascript: assignLoyaltyCard( '<spring:url value="/loyaltycard/assign/%ID%" />', '%ID%' );" title="<spring:message code="loyalty_label_assign" />">
    <spring:message code="loyalty_label_loyaltycard" />
  </a>

</span>

<span id="deleteBtn-list">
    <spring:url value="/customermodels/%ID%" var="delete_form_url" />
      <form:form action="${delete_form_url}" id="deleteForm%ACCOUNTID%" method="DELETE" cssClass="form-reset">
          <spring:message code="label_delete" var="label_delete"/>
          <c:set var="delete_confirm_msg">
              <spring:escapeBody javaScriptEscape="true">
                  <spring:message code="entity_delete_confirm" />
              </spring:escapeBody>
          </c:set>
          <input class="btn btn-primary icn-delete tiptip pull-left" type="button" alt="${fn:escapeXml(label_delete)}" data-toggle="tooltip" title="${fn:escapeXml(label_delete)}" value="${label_delete}" onclick="getConfirm('${delete_confirm_msg}',function(result) {if(result) $('form#deleteForm%ACCOUNTID%').submit();});" />
          <c:if test="${not empty param.page}">
              <input name="page" type="hidden" value="1" />
          </c:if>
          <c:if test="${not empty param.size}">
              <input name="size" type="hidden" value="${fn:escapeXml(param.size)}" />
          </c:if>
      </form:form>
</span>

	<div id="btn_activateMember" class="hide">
	  <a href="#" class="btn pull-left tiptip icn-approve btn_activateMember" data-url="<c:url value="/customermodels/activate/%ID%/%ACTIVATE%" />" title="<spring:message code="label_activate"/>" ><spring:message code="label_activate"/></a>
	</div>
	<div id="btn_deactivateMember" class="hide">
	  <a href="#" class="btn pull-left tiptip icn-delete btn_activateMember" data-url="<c:url value="/customermodels/activate/%ID%/%ACTIVATE%" />" title="<spring:message code="label_deactivate"/>" ><spring:message code="label_deactivate"/></a>
	</div>

</div>

<div class="modal hide  nofly" id="prepaidCardModal" tabindex="-1" role="dialog" aria-hidden="true"></div>
<%-- <div id="form_member" data-url="<c:url value="/member/create" />"></div><script type="text/javascript">$(document).ready( function() { $( "#btn_createMember" ).unbind( "click" ).click( function(e) { e.preventDefault(); $.get( $( "#form_member" ).data( "url" ), function(data) { $( "#form_member" ).html( data ); }, "html" ); }); });</script> --%>
<jsp:include page="form/memberform.jsp" />
<jsp:include page="form/professionalform.jsp" />
<jsp:include page="form/pinDialog.jsp" />
<jsp:include page="../common/confirm.jsp" />
<div id="form_loyaltyCardAssignment"></div>

  <link href="<spring:url value="/css/dataTables.css" />" rel="stylesheet">
  <script src="<spring:url value="/js/jquery/jquery.dataTables.js" />" ></script>
  <script src="<spring:url value="/js/jquery/jquery.dataTables.linkLengthMenu.js" />"></script>
  <script src="<spring:url value="/js/jquery/jquery.dataTables.customPaging.js" />"></script>
  <script src="<spring:url value="/js/jquery/ajax-datatable.js" />" ></script>
  <script src="<spring:url value="/js/common/form2js.js" />" ></script>
  <script src="<spring:url value="/js/common/json2.js" />" ></script>
  <script src="<spring:url value="/js/jquery/jquery.toObject.js" />" ></script>



  <script src="<c:url value="/js/viewspecific/customermodel/member.js" />" ></script>



