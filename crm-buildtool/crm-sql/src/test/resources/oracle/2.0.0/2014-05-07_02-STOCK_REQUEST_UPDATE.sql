drop table CRM_STOCK_REQUEST;

create table CRM_STOCK_REQUEST (
    id number(19,0) not null,
    CREATED_BY varchar2(255 char),
    CREATED_DATETIME timestamp,
    LAST_UPDATED_BY varchar2(255 char),
    LAST_UPDATED_DATETIME timestamp,
    ALLOCATE_TO varchar2(255 char),
    PRODUCT_CODE varchar2(255 char),
    QUANTITY number(10,0),
    REQUEST_DATE date,
    REQUEST_NO varchar2(12 char),
    SOURCE_LOCATION varchar2(255 char),
    STATUS varchar2(255 char),
    TRANSFER_REF_NO varchar2(255 char),
    primary key (id)
);