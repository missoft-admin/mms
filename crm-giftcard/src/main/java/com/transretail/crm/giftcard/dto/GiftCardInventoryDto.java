package com.transretail.crm.giftcard.dto;


import java.math.BigDecimal;

import org.joda.time.LocalDate;

import com.mysema.query.annotations.QueryProjection;
import com.transretail.crm.core.entity.enums.SalesType;
import com.transretail.crm.giftcard.entity.GiftCardInventory;
import com.transretail.crm.giftcard.entity.support.GiftCardInventoryStatus;
import com.transretail.crm.giftcard.entity.support.GiftCardSalesType;

/**
 * @author Allan G. Ramirez (agramirez@exist.com)
 */
public class GiftCardInventoryDto {
    private Long series;
    private String productCode;
    private String productName;
    private BigDecimal faceValue;
    private GiftCardInventoryStatus status;
    private String location;
    private String allocateTo;
    private LocalDate expiryDate;
    private LocalDate activationDate;
    private String barcode;
    private Double balance;
    private String boxNo;
    private GiftCardSalesType salesType;
    
    public GiftCardInventoryDto() {

    }
    
    @QueryProjection
    public GiftCardInventoryDto(
            String productCode,
            String productName,
            String location,
            GiftCardInventoryStatus status) {
        this.productCode = productCode;
        this.productName = productName;
        if (location != null)
            this.location = location;
        if (status != null)
            this.status = status;
    }

    @QueryProjection
    public GiftCardInventoryDto(
            String productCode,
            String productName,
            GiftCardSalesType salesType,
            String location,
            String barcode,
            GiftCardInventoryStatus status) {
        this.productCode = productCode;
        this.productName = productName;
        this.salesType = salesType;
        this.barcode = barcode;
        if (location != null)
            this.location = location;
        if (status != null)
            this.status = status;
    }
    
    public GiftCardInventoryDto(GiftCardInventory model) {
    	if(model == null) {
    		return;
    	}
        this.series = model.getSeries();
        this.productCode = model.getProductCode();
        this.productName = model.getProductName();
        this.faceValue = model.getFaceValue();
        this.status = model.getStatus();
        this.location = model.getLocation();
        this.allocateTo = model.getAllocateTo();
        this.expiryDate = model.getExpiryDate();
        this.activationDate = model.getActivationDate();
		this.barcode = model.getBarcode();
		this.balance = model.getBalance();
		this.boxNo = model.getBoxNo();
		this.salesType = model.getSalesType();
    }

    public Long getSeries() {
        return series;
    }

    public void setSeries(Long series) {
        this.series = series;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public BigDecimal getFaceValue() {
        return faceValue;
    }

    public void setFaceValue(BigDecimal faceValue) {
        this.faceValue = faceValue;
    }

    public GiftCardInventoryStatus getStatus() {
        return status;
    }

    public void setStatus(GiftCardInventoryStatus status) {
        this.status = status;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getAllocateTo() {
        return allocateTo;
    }

    public void setAllocateTo(String allocateTo) {
        this.allocateTo = allocateTo;
    }

    public LocalDate getExpiryDate() {
        return expiryDate;
    }

    public void setExpiryDate(LocalDate expiryDate) {
        this.expiryDate = expiryDate;
    }

    public String getBarcode() {
	return barcode;
    }

    public void setBarcode(String barcode) {
	this.barcode = barcode;
    }

    public Double getBalance() {
	return balance;
    }

    public void setBalance(Double balance) {
	this.balance = balance;
    }

    public LocalDate getActivationDate() {
	return activationDate;
    }

    public void setActivationDate(LocalDate activationDate) {
	this.activationDate = activationDate;
    }

    public String getBoxNo() {
        return boxNo;
    }

    public void setBoxNo(String boxNo) {
        this.boxNo = boxNo;
    }

    public String getProductCode() {
        return productCode;
    }

    public void setProductCode(String productCode) {
        this.productCode = productCode;
    }

    public GiftCardSalesType getSalesType() {
        return salesType;
    }

    public void setSalesType(GiftCardSalesType salesType) {
        this.salesType = salesType;
    }
    
}
