<%@include file="../common/taglibs.jsp" %>

<style type="text/css">
<!--
  #lmhTable tr td:last-child {
    border-left: 0px;    
  }
  
-->
</style>

<c:forEach items="${analysisDto}" var="item">
<c:set var="light">${light}<c:if test="${not empty light}">,</c:if>${item.light}</c:set>
<c:set var="medium">${medium}<c:if test="${not empty medium}">,</c:if>${item.medium}</c:set>
<c:set var="heavy">${heavy}<c:if test="${not empty heavy}">,</c:if>${item.heavy}</c:set>
<c:set var="ticks">${ticks}<c:if test="${not empty ticks}">,</c:if>'${item.monthYear}'</c:set>

<c:set var="monthHTML">${monthHTML}<th>${item.monthYear}</th></c:set>
<c:set var="lightHTML">${lightHTML}<td><div class="checkbox"><input type="checkbox" class="memberGroupChk" value="${item.lightMembers}" /><fmt:formatNumber type="number" pattern="${decimalFormat}" value="${item.light}" /></div></td></c:set>
<c:set var="mediumHTML">${mediumHTML}<td><div class="checkbox"><input type="checkbox" class="memberGroupChk" value="${item.mediumMembers}" /><fmt:formatNumber type="number" pattern="${decimalFormat}" value="${item.medium}" /></div></td></c:set>
<c:set var="heavyHTML">${heavyHTML}<td><div class="checkbox"><input type="checkbox" class="memberGroupChk" value="${item.heavyMembers}" /><fmt:formatNumber type="number" pattern="${decimalFormat}" value="${item.heavy}" /></div></td></c:set>
</c:forEach>

<div id="lmhChart"></div>

<br/>

<c:if test="${searchDto.analysisBy == 'salesValue'}">
<label><spring:message code="legend_one_is_to_thousand" /></label>
</c:if>

<input style="margin-bottom: 5px;" type="button" id="lmhGenerateGrpBtn" class="btn btn-primary" value="<spring:message code="label_lmh_generategroup" />" />

<div class="clearfix"></div>

<div style="overflow: auto; width: 100%">
<table id="lmhTable" class="table table-condensed table-compressed table-striped">
  <tr><th></th>${monthHTML}</tr>
  <tr><td><spring:message code="label_lmh_light" /></td>${lightHTML}</tr>
  <tr><td><spring:message code="label_lmh_medium" /></td>${mediumHTML}</tr>
  <tr><td><spring:message code="label_lmh_heavy" /></td>${heavyHTML}</tr>
</table>
</div>

<div class="modal hide  groupDialog nofly" tabindex="-1" role="dialog" aria-hidden="true" id="groupDialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title"><spring:message code="label_lmh_savegroup" /></h4>
      </div>
      <div class="modal-body">
        <div id="contentError" class="hide alert alert-error">
          <button type="button" class="close" onclick="$(this).parent().hide();">&times;</button>
          <div><form:errors path="*"/></div>
        </div>
        <form id="groupForm" name="groupForm" action="<spring:url value="/dataanalysis/generategroup" />" class="form-reset form-horizontal" method="POST">
          <div class="control-group">
            <label for="" class="control-label"><spring:message code="label_lmh_groupname" /></label> 
            <div class="controls">
              <input type="text" name="name" />
              <input type="hidden" id="members" name="members" />
            </div>
          </div>
        </form>
      </div>
      <div class="modal-footer">
        <button type="button" id="formSubmit" class="btn btn-primary"><spring:message code="label_submit" /></button>
        <button type="button" id="formCancel" data-dismiss="modal" class="btn btn-default"><spring:message code="label_cancel"/></button>
      </div>
    </div>
  </div>
</div>

<script>
$(document).ready(function() {
	var CONTAINER_ERROR = "#contentError > div";
	var CONTENT_ERROR = "#contentError";
	
	$("#groupDialog").modal({
		show: false,
		keyboard:	false, 
		backdrop: "static",
	})
	.on('hidden.bs.modal', function() {
		$("#groupForm input").val("");
	})
	.css({
		"width": "500px"
	})
	;
	
	var light = [${light}];
	var medium = [${medium}];
	var heavy = [${heavy}];
	var ticks = [${ticks}];
	
	$.jqplot('lmhChart', [light, medium, heavy], {
		title: '<spring:message code="label_lmh_analysis_${searchDto.analysisBy}"/>',
        seriesDefaults:{
            renderer:$.jqplot.BarRenderer,
            rendererOptions: {fillToZero: true}
        },
        series:[
            {label:'<spring:message code="label_lmh_light"/>'},
            {label:'<spring:message code="label_lmh_medium"/>'},
            {label:'<spring:message code="label_lmh_heavy"/>'}
        ],
        legend: {
            show: true,
            location: 'ne',
        },
        axes: {
            xaxis: {
                renderer: $.jqplot.CategoryAxisRenderer,
                ticks: ticks
            }
        }
    });
	
	$("#lmhGenerateGrpBtn").click(function(e) {
		e.preventDefault();
		var membersStr = "";
		
		$(".memberGroupChk:checked").each(function() {
			var val = $(this).val();
			var rg = new RegExp("\\[", 'g');
			val = val.replace(rg, "");
			rg = new RegExp("\\]", 'g');
			val = val.replace(rg, "");
			rg = new RegExp(" ", 'g');
			val = val.replace(rg, "");
			if(val != "") {
    			if(membersStr != "")
    				membersStr += ",";
    			membersStr += val;
    		}
		});
		
		$("#groupDialog #members").val(membersStr);
		
		$("#groupDialog").modal("show");
		
	});
	
	$("#formSubmit").click(function(e) {
		e.preventDefault();
		$form = $("#groupForm");
		$.post($form.attr("action") , $form.serialize(), function(inResponse) {
			if (inResponse.success) {
				$("#groupDialog").modal("hide");
			} 
			else {
				errorInfo = "";
				for (i = 0; i < inResponse.result.length; i++) {
					errorInfo += "<br>" + (i + 1) + ". "
							+ ( inResponse.result[i].code != undefined ? 
									inResponse.result[i].code : inResponse.result[i]);
				}
				$( CONTAINER_ERROR ).html("Please correct following errors: " + errorInfo);
				$( CONTENT_ERROR ).show('slow');
			}
		}, "json");
	});
});

</script>
