package com.transretail.crm.giftcard.dto;

import java.util.ArrayList;
import java.util.List;

import org.joda.time.LocalDate;
import org.springframework.util.StringUtils;

import com.mysema.query.types.expr.BooleanExpression;
import com.transretail.crm.common.service.dto.request.AbstractSearchFormDto;
import com.transretail.crm.common.util.DateUtil;
import com.transretail.crm.core.util.BooleanExprUtil;
import com.transretail.crm.giftcard.entity.GiftCardBurnCard;
import com.transretail.crm.giftcard.entity.GiftCardInventory;
import com.transretail.crm.giftcard.entity.support.GiftCardBurnCardStatus;
import com.transretail.crm.giftcard.entity.QGiftCardBurnCard;

/**
 * @author ftopico
 */
public class GiftCardBurnCardSearchDto extends AbstractSearchFormDto {

    private Integer storeId;
    private GiftCardBurnCardStatus status;
    private LocalDate dateFrom;
    private LocalDate dateTo;
    
    public Integer getStoreId() {
        return storeId;
    }
    public void setStoreId(Integer storeId) {
        this.storeId = storeId;
    }
    public GiftCardBurnCardStatus getStatus() {
        return status;
    }
    public void setStatus(GiftCardBurnCardStatus status) {
        this.status = status;
    }

    public LocalDate getDateFrom() {
        return dateFrom;
    }

    public void setDateFrom(LocalDate dateFrom) {
        this.dateFrom = dateFrom;
    }

    public LocalDate getDateTo() {
        return dateTo;
    }

    public void setDateTo(LocalDate dateTo) {
        this.dateTo = dateTo;
    }

    @Override
    public BooleanExpression createSearchExpression() {
        List<BooleanExpression> expressions = new ArrayList<BooleanExpression>();
        QGiftCardBurnCard qGiftCardBurnCard = QGiftCardBurnCard.giftCardBurnCard;
        
        if (storeId != null) {
            expressions.add(qGiftCardBurnCard.storeId.eq(storeId));
        }
        
        if (StringUtils.hasText(String.valueOf(status))) {
            expressions.add(BooleanExprUtil.INSTANCE.isEnumPropertyIn(qGiftCardBurnCard.status, status));
        }
        
        if(dateFrom != null && dateTo != null) {
            expressions.add(qGiftCardBurnCard.dateFiled.between(dateFrom, DateUtil.getEndOfDay(dateTo)));
        } else if (dateFrom != null){
            expressions.add(qGiftCardBurnCard.dateFiled.goe(dateFrom));
        } else if(dateTo != null) {
            expressions.add(qGiftCardBurnCard.dateFiled.loe(DateUtil.getEndOfDay(dateTo)));
        }
        return BooleanExpression.allOf(expressions.toArray(new BooleanExpression[expressions.size()]));
    }
    
}
