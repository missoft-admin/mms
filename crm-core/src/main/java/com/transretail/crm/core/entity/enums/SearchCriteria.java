package com.transretail.crm.core.entity.enums;

public enum SearchCriteria {

	ACCOUNTID ( "Account Number", "accountId" ),
	CONTACT ( "Contact", "contact" ),
	EMAIL ( "E-mail", "email" ),
	KTPID( "KTP ID", "ktpId" ),
	NAME( "First/Last Name", "name" ),
	BUSINESS_NAME( "Business Name", "businessName" );

    private SearchCriteria(String inDesc, String inValue){
        this.desc = inDesc;
        this.value = inValue;
    }

    private final String desc;
    private final String value;
    public String getDescription() {
    	return desc;
    }
    public String getValue() {
    	return value;
    }
}
