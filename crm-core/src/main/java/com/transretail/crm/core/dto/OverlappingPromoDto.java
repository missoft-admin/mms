package com.transretail.crm.core.dto;

import java.util.Set;

import org.joda.time.DateTime;

/**
 * @author Allan G. Ramirez (agramirez@exist.com)
 */
public class OverlappingPromoDto {
    private Long promotionId;
    private String promotionName;
    private DateTime beginDateTime;
    private DateTime endDateTime;
    private Set<String> memberGroupNames;
    private Set<String> products;
    private Set<String> stores;
    private Set<String> paymentTypes;
    private Set<String> channels;

    public Long getPromotionId() {
        return promotionId;
    }

    public void setPromotionId(Long promotionId) {
        this.promotionId = promotionId;
    }

    public String getPromotionName() {
        return promotionName;
    }

    public void setPromotionName(String promotionName) {
        this.promotionName = promotionName;
    }

    public DateTime getBeginDateTime() {
        return beginDateTime;
    }

    public void setBeginDateTime(DateTime beginDateTime) {
        this.beginDateTime = beginDateTime;
    }

    public DateTime getEndDateTime() {
        return endDateTime;
    }

    public void setEndDateTime(DateTime endDateTime) {
        this.endDateTime = endDateTime;
    }

    public Set<String> getMemberGroupNames() {
        return memberGroupNames;
    }

    public void setMemberGroupNames(Set<String> memberGroupNames) {
        this.memberGroupNames = memberGroupNames;
    }

    public Set<String> getProducts() {
        return products;
    }

    public void setProducts(Set<String> products) {
        this.products = products;
    }

    public Set<String> getStores() {
        return stores;
    }

    public void setStores(Set<String> stores) {
        this.stores = stores;
    }

    public Set<String> getPaymentTypes() {
        return paymentTypes;
    }

    public void setPaymentTypes(Set<String> paymentTypes) {
        this.paymentTypes = paymentTypes;
    }

    public Set<String> getChannels() {
        return channels;
    }

    public void setChannels(Set<String> channels) {
        this.channels = channels;
    }

}
