package com.transretail.crm.loyalty.dto;


import java.util.Collection;

import org.springframework.data.domain.Page;

import com.transretail.crm.common.service.dto.response.AbstractResultListDTO;


public class LoyaltyCardInventorySummaryResultList extends AbstractResultListDTO<LoyaltyCardInventoryListDto> {
    public LoyaltyCardInventorySummaryResultList(Collection<LoyaltyCardInventoryListDto> results, long totalElements, boolean hasPreviousPage, boolean hasNextPage) {
        super(results, totalElements, hasPreviousPage, hasNextPage);
    }
    
	public LoyaltyCardInventorySummaryResultList(Collection<LoyaltyCardInventoryListDto> results, long totalElements, int pageNo, int pageSize) {
		super(results, totalElements, pageNo, pageSize);
	}
    
    public LoyaltyCardInventorySummaryResultList( Page<LoyaltyCardInventoryListDto> page ) {
    	super(page);
    }
}
