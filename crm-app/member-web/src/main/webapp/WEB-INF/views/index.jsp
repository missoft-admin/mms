<%@ include file="taglibs.jsp" %>

<script type="text/javascript">
$(window).load(function () {
$('#onLoadShow').modal('show');
});
</script>
 
 <h1><div id="welcomeMsg" class="hide"><spring:message code="welcome"/>&nbsp;<sec:authentication property="principal.fullName"/></div></h1>
 
  <c:if test="${noLoyaltyCard && !employee}">
  <div class="modal fade bs-example-modal-sm" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true" id="onLoadShow">
   <div class="modal-dialog modal-sm">
     <div class="modal-content">
       <spring:message code="member.no.loyalty.card.prompt.message" />. Visit nearest Customer Service now.
       <div><button class="btn btn-primary" data-dismiss="modal">OK</button></div>
     </div>
   </div>
  </div>
  </c:if>
  <c:if test="${isLoyaltyCardExpired}">
  <div class="modal fade bs-example-modal-sm" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true" id="onLoadShow">
   <div class="modal-dialog modal-sm">
     <div class="modal-content">
       <spring:message code="member.expiring.loyalty.card.prompt.message" />
       <div><button class="btn btn-primary" data-dismiss="modal">OK</button></div>
     </div>
   </div>
  </div>
  </c:if>
  
  <%@ include file="/WEB-INF/views/common/content-v2.jsp" %>

<style>
  /*
    Workaround to hide the logo for now. We'll create a separate tile template if dashboard UI with banners is finalized.
  */
  .navbar-brand {
    display: none;
  }

  ul.banner {
    list-style-type: none;
    padding: 0;
    margin: 0;
  }
  
  #welcomeMsg {
      font-size: 24.5px;
      margin-top: 20px;
  }
  
  #isLoyaltyCardExpiredMessage,
  #noLoyaltyCardMessage {
    margin-top: 20px; 
  }
  
  #onLoadShow .modal-content{
    font-size: 20px;
    font-weight: lighter;
    line-height: 34px;
  }
  #onLoadShow .modal-content .btn{
    margin-top: 20px;
  }
  .modal-content {
      padding: 40px;
  }
</style>
<script type="text/javascript">
	$(document).ready( function() {
		checkPin();
		$("#promoBanners").carousel();
		function checkPin() {
			$.post( 
					  '<c:url value="/member/pin/check" />',
            function( isResponse, textStatus, jqXHR ) { 
            	if ( isResponse ) {
            		window.location.href='<c:url value="/member/pin/update" />';
            	}
            	else {
            		$( "#welcomeMsg" ).show();
            	}
            },
            "json" 
	    );
		}
	});
</script>
