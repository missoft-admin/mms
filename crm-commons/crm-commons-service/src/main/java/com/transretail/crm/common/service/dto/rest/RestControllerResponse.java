package com.transretail.crm.common.service.dto.rest;

import com.transretail.crm.common.enums.MessageType;

import java.io.Serializable;


/**
 *
 * Convenient DTO object to store  messages to be returned by a service (Rest calls)
 *
 */
public class RestControllerResponse implements Serializable {

    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	
	private MessageType type;
    private String callingAction;
    private String message;
    private String messageCode;
    private boolean success;
    private Object result;
	public MessageType getType() {
		return type;
	}
	public void setType(MessageType type) {
		this.type = type;
	}
	public String getCallingAction() {
		return callingAction;
	}
	public void setCallingAction(String callingAction) {
		this.callingAction = callingAction;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public String getMessageCode() {
		return messageCode;
	}
	public void setMessageCode(String messageCode) {
		this.messageCode = messageCode;
	}
	public boolean isSuccess() {
		return success;
	}
	public void setSuccess(boolean success) {
		this.success = success;
	}
	public Object getResult() {
		return result;
	}
	public void setResult(Object result) {
		this.result = result;
	}
    
    
    

}
