package com.transretail.crm.common.web.security;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.web.DefaultRedirectStrategy;
import org.springframework.security.web.RedirectStrategy;
import org.springframework.security.web.session.InvalidSessionStrategy;
import org.springframework.security.web.util.UrlUtils;
import org.springframework.util.Assert;

import com.transretail.crm.common.web.controller.support.RequestUtils;

/**
 * @author Allan G. Ramirez (agramirez@exist.com)
 */
public class AjaxAwareInvalidSessionStrategy implements InvalidSessionStrategy {
    private static final Logger _LOG = LoggerFactory.getLogger(AjaxAwareInvalidSessionStrategy.class);
    private final String destinationUrl;
    private final RedirectStrategy redirectStrategy = new DefaultRedirectStrategy();
    private boolean createNewSession = true;

    public AjaxAwareInvalidSessionStrategy(String invalidSessionUrl) {
        Assert.isTrue(UrlUtils.isValidRedirectUrl(invalidSessionUrl), "url must start with '/' or with 'http(s)'");
        this.destinationUrl = invalidSessionUrl;
    }

    @Override
    public void onInvalidSessionDetected(HttpServletRequest request, HttpServletResponse response) throws IOException,
        ServletException {
        if (RequestUtils.INSTANCE.isAjax(request)) {
            ajaxRedirect(response.getWriter(), request);
        } else {
            _LOG.debug("Starting new session (if required) and redirecting to '" + destinationUrl + "'");
            if (createNewSession) {
                request.getSession();
            }
            redirectStrategy.sendRedirect(request, response, destinationUrl);
        }
    }

    /**
     * Determines whether a new session should be created before redirecting (to avoid possible looping issues where
     * the same session ID is sent with the redirected request). Alternatively, ensure that the configured URL
     * does not pass through the {@code SessionManagementFilter}.
     *
     * @param createNewSession defaults to {@code true}.
     */
    public void setCreateNewSession(boolean createNewSession) {
        this.createNewSession = createNewSession;
    }

    private void ajaxRedirect(PrintWriter writer, HttpServletRequest request) {
        String targetUrl = RequestUtils.INSTANCE.getRootUrl(request);
        if (!destinationUrl.startsWith("/")) {
            targetUrl += "/";
        }
        targetUrl += destinationUrl;

        writer.println("<script type=\"text/javascript\">");
        writer.println("window.location='" + targetUrl + "';");
        writer.println("</script>");
        writer.flush();
    }
}
