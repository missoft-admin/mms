<%@ include file="../../common/taglibs.jsp" %>

<form name="exhibitionForm" action="<c:url value="/giftcard/b2b/exhibition/save" />" class="form-horizontal">

      <div id="contentError" class="hide alert alert-error">
        <button type="button" class="close" onclick="$(this).parent().hide();">&times;</button>
        <div><form:errors path="*"/></div>
      </div>
      

<div class="control-group">
  <label for="orderNo" class="control-label"><spring:message code="b2b.exhibition.transactionno" /></label> 
  <span class="control-label control-label-x label-input">${exhibitionForm.transactionNo}</span>
  <label for="orderNo" class="control-label"><spring:message code="b2b.exhibition.kwitansino" /></label> 
  <span class="control-label control-label-x label-input">${exhibitionForm.kwitansiNo}</span>
</div>

<div class="control-group">
  <label for="orderNo" class="control-label"><spring:message code="b2b.exhibition.transactiondate" /></label> 
  <span class="control-label control-label-x label-input">${exhibitionForm.createdFmt}</span>
</div>


<c:set var="readOnly" value="true" scope="request" />

<!-- PRODUCTS -->
<%-- <button type="button" id="scanProductBtn" class="btn btn-primary"><spring:message code="b2b.exhibition.scangc" /></button>
<br/><br/> --%>
<div>
  <table id="productTable" class="table table-condensed table-compressed table-striped">
    <thead>
      <tr>
        <th><spring:message code="b2b.exhibition.product" /></th>
        <th><spring:message code="b2b.exhibition.cards" /></th>
        <th><spring:message code="b2b.exhibition.facevalue" /></th>
        <th><spring:message code="b2b.exhibition.quantity" /></th>
        <th><spring:message code="b2b.exhibition.beforediscount" /></th>
        <th><spring:message code="b2b.exhibition.discount" /></th>
        <th><spring:message code="b2b.exhibition.total" /></th>
        <th></th>
      </tr>
    </thead>
    <tbody>
      <c:set var="countSummary" value="${exhibitionForm.countSummary}" scope="request" />
      <c:set var="discVal" value="${exhibitionForm.discount}" scope="request" />
      <c:set var="tax" value="${exhibitionForm.tax}" scope="request" />
      <jsp:include page="countlist.jsp" />
    </tbody>
  </table>
</div>


<!-- PAYMENTS -->
<br/><br/>
<%-- <button type="button" id="addPaymentsBtn" class="btn btn-primary"><spring:message code="b2b.exhibition.addpayments" /></button>
<br/><br/> --%>
<div class="row-fluid">
<div class="span8">
  <table id="paymentTable" class="table table-condensed table-compressed table-striped">
    <thead>
      <tr>
        <th><spring:message code="b2b.exhibition.type" /></th>
        <th><spring:message code="b2b.exhibition.amount" /></th>
        <th></th>
      </tr>
    </thead>
    <tbody>
      <c:set var="paymentSummary" value="${exhibitionForm.payments}" scope="request" />
      <jsp:include page="paymentlist.jsp" />
    </tbody>
  </table>
</div>

</div>

<div class="form-actions container-btn">
            <button type="button" id="printReceipt" data-id="${exhibitionForm.id}" class="btn btn-primary"><spring:message code="b2b.exhibition.printreceipt" /></button>
            <button type="button" id="printKwitansi" data-id="${exhibitionForm.id}" class="btn btn-primary"><spring:message code="b2b.exhibition.printkwitansi" /></button>
            <button type="button" id="cancelGetForm" class="btn"><spring:message code="label_reset" /></button>
</div>

<div class="hide">
  <div id="gcsForm">
  </div>
  <div id="pymntsForm">
  </div>
</div>
</form>

<script type="text/javascript">
$(document).ready(function() {
	
	$("#printReceipt").click(function() {
		window.open("<c:url value="/giftcard/b2b/exhibition/printreceipt/" />" + $(this).data("id"), '_blank', 'toolbar=0,location=0,menubar=0');
	});
	
	$("#printKwitansi").click(function() {
		window.open("<c:url value="/giftcard/b2b/exhibition/printkwitansi/" />" + $(this).data("id"), '_blank', 'toolbar=0,location=0,menubar=0');
	});
  
	$("#cancelGetForm").click(function() {
		location.reload();
	});
  
});
</script>
