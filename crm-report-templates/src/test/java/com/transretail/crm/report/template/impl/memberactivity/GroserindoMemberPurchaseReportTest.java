package com.transretail.crm.report.template.impl.memberactivity;

import com.google.common.collect.Maps;
import com.transretail.crm.common.reporting.jasper.JRProcessor;
import com.transretail.crm.common.reporting.service.ReportService;
import com.transretail.crm.report.template.CommonReportFilter;
import com.transretail.crm.report.template.ReportTemplate;
import net.sf.jasperreports.engine.JRParameter;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.TransactionCallbackWithoutResult;
import org.springframework.transaction.support.TransactionTemplate;

import javax.annotation.Resource;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Map;
import java.util.Scanner;

import static org.junit.Assert.assertEquals;

/**
 * @author Mike de Guzman
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath*:/META-INF/spring/applicationContext*.xml",
        "/META-INF/spring/applicationContext-test.xml"})
public class GroserindoMemberPurchaseReportTest {

    private static final Logger _LOG = LoggerFactory.getLogger(GroserindoMemberPurchaseReportTest.class);
    @PersistenceContext
    private EntityManager em;
    @Autowired
    private PlatformTransactionManager transactionManager;
    @Autowired
    private ReportService reportService;

    @Resource(name = "groserindoMemberPurchaseReport")
    private ReportTemplate reportTemplate;

    @Before
    public void doSetup() {
        TransactionTemplate transactionTemplate = new TransactionTemplate(transactionManager);
        transactionTemplate.execute(new TransactionCallbackWithoutResult() {
            @Override
            protected void doInTransactionWithoutResult(TransactionStatus status) {
                em.createNativeQuery("DROP TABLE POS_TRANSACTION IF EXISTS").executeUpdate();
                em.createNativeQuery("DROP TABLE TAX_INVOICE IF EXISTS").executeUpdate();
                em.createNativeQuery("DROP TABLE CRM_MEMBER IF EXISTS").executeUpdate();
                em.createNativeQuery("DROP TABLE CRM_REF_LOOKUP_DTL IF EXISTS").executeUpdate();

                em.createNativeQuery("CREATE TABLE POS_TRANSACTION (ID VARCHAR(255) NOT NULL, " +
                        "TOTAL_AMOUNT DOUBLE," +
                        "TOTAL_DISCOUNT DOUBLE," +
                        "VOIDED_DISCOUNT DOUBLE," +
                        "ROUNDING_AMOUNT DOUBLE," +
                        "TARIFF DOUBLE," +
                        "VAT DOUBLE," +
                        "TOTAL_NON_MEMBER_MARKUP DOUBLE," +
                        "TRANSACTION_DATE TIMESTAMP," +
                        "TYPE VARCHAR(255)," +
                        "SALES_DATE TIMESTAMP," +
                        "STATUS VARCHAR(255)," +
                        "CUSTOMER_ID VARCHAR(255)," +
                        "PRIMARY KEY (ID))").executeUpdate();
                em.createNativeQuery("CREATE TABLE TAX_INVOICE (CUSTOMER_NUMBER VARCHAR(255)," +
                        "CUSTOMER_NAME VARCHAR(255), " +
                        "POS_TXN_ID VARCHAR(255))").executeUpdate();
                em.createNativeQuery("CREATE TABLE CRM_MEMBER (COMPANY_NAME VARCHAR(255), " +
                        "CREATED_DATETIME TIMESTAMP," +
                        "CUSTOMER_SEGREGATION VARCHAR(255)," +
                        "ACCOUNT_ID VARCHAR(255)," +
                        "CUSTOMER_GROUP VARCHAR(255))").executeUpdate();
                em.createNativeQuery(
                        "CREATE TABLE CRM_REF_LOOKUP_DTL (CODE VARCHAR(255),DESCRIPTION VARCHAR(255),STATUS VARCHAR(8), HDR VARCHAR(255))")
                        .executeUpdate();

                InputStream is =
                        Thread.currentThread().getContextClassLoader().getResourceAsStream("scripts/groserindoMemberActivityReport.sql");
                try {
                    final Scanner in = new Scanner(is);
                    while (in.hasNext()) {
                        String script = in.nextLine();
                        if (StringUtils.isNotBlank(script)) {
                            em.createNativeQuery(script).executeUpdate();
                        }
                    }
                } finally {
                    IOUtils.closeQuietly(is);
                }
            }
        });
    }

    @Test
    public void testReport() throws Exception {
        assertEquals("Member Purchase Amount Report", reportTemplate.getName());
        assertEquals("Member Purchase Amount Report", reportTemplate.getDescription());
        assertEquals(4, reportTemplate.getFilters().size());

        Map<String, String> map = Maps.newHashMap();
        map.put(CommonReportFilter.DATE_FROM, "08-04-2012"); // April 8, 2012
        map.put(CommonReportFilter.DATE_TO, "21-11-2014"); // Nov 21, 2014
        map.put("activityReportType", "monthly");

        File testClassesDir = FileUtils.toFile(Thread.currentThread().getContextClassLoader().getResource(""));
        OutputStream excelOs = null;
        OutputStream pdfOs = null;
        try {
            excelOs = new FileOutputStream(new File(testClassesDir, "test-member-purchase-report-output.xlsx"));
            map.put(ReportTemplate.PARAM_REPORT_TYPE, "excel");
            JRProcessor jrProcessor = reportTemplate.createJrProcessor(map);
            jrProcessor.addParameter(JRParameter.REPORT_LOCALE, LocaleContextHolder.getLocale());
            reportService.exportExcelReport(jrProcessor, excelOs);

            pdfOs = new FileOutputStream(new File(testClassesDir, "test-member-purchase-report-output.pdf"));
            map.put(ReportTemplate.PARAM_REPORT_TYPE, "pdf");
            jrProcessor = reportTemplate.createJrProcessor(map);
            jrProcessor.addParameter(JRParameter.REPORT_LOCALE, LocaleContextHolder.getLocale());
            reportService.exportPdfReport(jrProcessor, pdfOs);
        } finally {
            IOUtils.closeQuietly(excelOs);
            IOUtils.closeQuietly(pdfOs);
        }
    }
}
