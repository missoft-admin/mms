package com.transretail.crm.common.dialect;

import java.sql.Types;

import org.hibernate.dialect.MySQL5InnoDBDialect;
import org.hibernate.dialect.function.StandardSQLFunction;

public class CRMMysqlDialect extends MySQL5InnoDBDialect {
    public CRMMysqlDialect() {
        super();
        registerFunction("to_date", new StandardSQLFunction("parsedatetime"));
        registerFunction("trunc", new StandardSQLFunction("truncate"));
        registerFunction("add_month", new StandardSQLFunction("add_month"));
        
        registerColumnType(Types.DOUBLE, "double");
    }
}