package com.transretail.crm.core.service.impl;


import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import com.google.common.collect.Lists;
import com.mysema.query.jpa.impl.JPAQuery;
import com.transretail.crm.core.dto.ProductDto;
import com.transretail.crm.core.dto.ProductSearchDto;
import com.transretail.crm.core.entity.QPeopleSoftStore;
import com.transretail.crm.core.entity.lookup.Product;
import com.transretail.crm.core.entity.lookup.QProduct;
import com.transretail.crm.core.repo.ProductRepo;
import com.transretail.crm.core.service.ProductService;


@Service("productService")
public class ProductServiceImpl implements ProductService {

	public static final String CODE_DELIMITER = ",";
	@Autowired
	ProductRepo productRepo;
	@PersistenceContext
    private EntityManager em;



	@Override
	public Iterable<Product> getAllProducts() {
		return productRepo.findAll();
	}

	@Override
	public boolean doExist( ProductSearchDto searchDto ) {
		return null != searchOne( searchDto );
	}

	@Override
	public Iterable<Product> getProducts( Integer idx, Integer size ) {
		Page<Product> pg = productRepo.findAll( new PageRequest( idx/size, size ) );
		return null != pg? pg.getContent() : null;
	}

	@Override
	public Product getByPluId( String pluId ) {
		if ( StringUtils.isBlank( pluId ) ) {
			return null;
		}
		ProductSearchDto searchDto = new ProductSearchDto();
		searchDto.setPluId( pluId );
		return searchOne( searchDto );
	}

	@Override
	public Product searchOne( ProductSearchDto searchDto ) {
		if ( null == searchDto ) {
			return null;
		}
		
		QProduct qp = QProduct.product;
		
		return new JPAQuery(em)
		.from(qp)
		.where(searchDto.createSearchExpression())
		.limit(1)
		.singleResult(qp);
		
		/*Iterable<Product> it = productRepo.findAll( searchDto.createSearchExpression() );
		
		if ( null != it && null != it.iterator() && it.iterator().hasNext() ) {
			return it.iterator().next();
		}
		return null;*/
	}

	@Override
	public List<ProductDto> search( ProductSearchDto searchDto ) {
		if ( null == searchDto ) {
			return null;
		}

		Iterable<Product> it = productRepo.findAll( searchDto.createSearchExpression() );
		if ( null != it && null != it.iterator() && it.iterator().hasNext() ) {
			return ProductDto.toDtoList( Lists.newArrayList( it ) );
		}
		return new ArrayList<ProductDto>();
	}
	
	@Override
	public List<ProductDto> search(ProductSearchDto searchDto, long limit) {
		if ( null == searchDto ) {
			return null;
		}
		
		QProduct qp = QProduct.product;
		List<Product> products = new JPAQuery(em)
			.from(qp)
			.where(searchDto.createSearchExpression())
			.limit(limit)
			.orderBy(qp.sku.asc())
			.list(qp);
				
		if(CollectionUtils.isNotEmpty(products)) 
			return ProductDto.toDtoList(products);
		
		return Lists.newArrayList();

	}

}
