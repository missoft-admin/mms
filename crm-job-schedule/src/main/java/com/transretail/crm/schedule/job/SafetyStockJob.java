package com.transretail.crm.schedule.job;

import java.util.List;
import java.util.Locale;
import java.util.Set;

import org.apache.commons.lang3.StringUtils;
import org.joda.time.DateTimeZone;
import org.joda.time.LocalDate;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.scheduling.quartz.QuartzJobBean;
import org.springframework.transaction.annotation.Transactional;

import com.transretail.crm.core.dto.MessageDto;
import com.transretail.crm.core.service.UserRolePermissionService;
import com.transretail.crm.giftcard.dto.GcInventorySafetyStockDto;
import com.transretail.crm.giftcard.service.SafetyStockService;
import com.transretail.crm.media.service.MailService;

/**
 * @author Allan G. Ramirez (agramirez@exist.com)
 */
public class SafetyStockJob extends QuartzJobBean {
    private static final Logger _LOG = LoggerFactory.getLogger(SafetyStockJob.class);
    private SafetyStockService safetyStockService;
    private MessageSource messageSource;
    private MailService mailService;
    private UserRolePermissionService userRolePermissionService;

    @Autowired
    public void setSafetyStockService(SafetyStockService safetyStockService) {
        this.safetyStockService = safetyStockService;
    }

    @Autowired
    public void setMessageSource(MessageSource messageSource) {
        this.messageSource = messageSource;
    }

    @Autowired
    public void setMailService(MailService mailService) {
        this.mailService = mailService;
    }

    @Autowired
    public void setUserRolePermissionService(UserRolePermissionService userRolePermissionService) {
        this.userRolePermissionService = userRolePermissionService;
    }

    @Override
    @Transactional(readOnly = true)
    protected void executeInternal(JobExecutionContext context) throws JobExecutionException {
        LocalDate date = getCurrentDate();
        _LOG.info("[Safety Stock Job] Checking safety stocks.");
        List<GcInventorySafetyStockDto> gcInventorySafetyStockDtoList = safetyStockService.checkSafetyStock(date);
        if (gcInventorySafetyStockDtoList != null && gcInventorySafetyStockDtoList.size() > 0) {
            Set<String> emailAddresses = userRolePermissionService.getMarchantServiceSupervisorEmails();
            if (emailAddresses != null && emailAddresses.size() > 0) {
                MessageDto messageDto = new MessageDto();
                messageDto.setRecipients(emailAddresses.toArray(new String[emailAddresses.size()]));

                Locale locale = LocaleContextHolder.getLocale();
                String subject = messageSource.getMessage("safetystock.belowstock.email.subject", null, locale);
                messageDto.setSubject(subject);

                String template = messageSource.getMessage("safetystock.belowstock.email.message.template",
                    new String[]{"tempProductDesc", "tempCurrMonth", "tempCount", "tempNextMonth", "tempSafetyStockQty"}, locale);

                StringBuilder builder = new StringBuilder();

                GcInventorySafetyStockDto dto = gcInventorySafetyStockDtoList.get(0);
                builder.append(template.replaceFirst("tempProductDesc", dto.getProductDesc())
                    .replaceFirst("tempCurrMonth", dto.getCurrMonth())
                    .replaceFirst("tempCount", String.valueOf(dto.getInventoryCount()))
                    .replaceFirst("tempNextMonth", dto.getNextMonth())
                    .replaceFirst("tempSafetyStockQty", String.valueOf(dto.getSafetyStockQty())));

                for (int i = 1; i < gcInventorySafetyStockDtoList.size(); i++) {
                    dto = gcInventorySafetyStockDtoList.get(i);
                    builder.append("<br />");
                    builder.append(template.replaceFirst("tempProductDesc", dto.getProductDesc())
                        .replaceFirst("tempCurrMonth", dto.getCurrMonth())
                        .replaceFirst("tempCount", String.valueOf(dto.getInventoryCount()))
                        .replaceFirst("tempNextMonth", dto.getNextMonth())
                        .replaceFirst("tempSafetyStockQty", String.valueOf(dto.getSafetyStockQty())));
                }
                messageDto.setMessage(builder.toString());

                if (_LOG.isInfoEnabled()) {
                    _LOG.info("[Safety Stock Job] Sending email to {}.", StringUtils.join(emailAddresses, ","));
                }
                mailService.sendWithHtml(messageDto);
            }
        }
    }

    protected LocalDate getCurrentDate() {
        return LocalDate.now(DateTimeZone.getDefault());
    }
}
