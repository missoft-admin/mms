package com.transretail.crm.reconciliation.service.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.sql.Types;

import javax.annotation.Resource;
import javax.sql.DataSource;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.stereotype.Service;

import com.transretail.crm.common.util.IOUtils;
import com.transretail.crm.Constants;
import com.transretail.crm.reconciliation.service.StoreInfoService;

/**
 * @author Allan G. Ramirez (agramirez@exist.com)
 */
@Service
public class StoreInfoServiceImpl implements StoreInfoService, InitializingBean {
    @Resource(name = "dataSource")
    private DataSource localDataSource;

    private String storeCode;
    private int storeId;
    private String lastTxnSyncDateKey;
    private String lastRefSyncDateKey;

    private String posIntegDbUsedKey;
    private String posIntegDbUsed;

    private String mainDbUsedKey;
    private String mainDbUsed;

    @Override
    public void afterPropertiesSet() throws Exception {
        Connection con = null;
        try {
            con = localDataSource.getConnection();
            con.setAutoCommit(false);

            readStoreInfo(con);

            lastTxnSyncDateKey = "STORE-" + storeCode + "-LAST-TXNSYNC-DATE";
            lastRefSyncDateKey = "STORE-" + storeCode + "-LAST-REFSYNC-DATE";
            posIntegDbUsedKey = "STORE-" + storeCode + "-POSINTEG-DB";
            mainDbUsedKey = "STORE-" + storeCode + "-MAIN-DB";

            checkSyncConfig(con, lastTxnSyncDateKey, -999l, null);
            checkSyncConfig(con, lastRefSyncDateKey, -998l, null);
            posIntegDbUsed = checkSyncConfig(con, posIntegDbUsedKey, -997l, Constants.DBEE.toString());
            mainDbUsed = checkSyncConfig(con, mainDbUsedKey, -996l, Constants.DBEE.toString());
        } finally {
            IOUtils.INSTANCE.close(con, false);
        }
    }

    private void readStoreInfo(Connection con) throws SQLException {
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            ps = con.prepareStatement(
                "SELECT CONFIG_VALUE, STORE_ID FROM CONFIG_LOCAL INNER JOIN STORE ON CODE = CONFIG_VALUE WHERE CONFIG_KEY = ?;");
            ps.setString(1, "STORE_CODE");
            rs = ps.executeQuery();
            // Will always return single result. Else, something is wrong during POS installation.
            rs.next();
            storeCode = rs.getString(1);
            storeId = rs.getInt(2);
        } finally {
            IOUtils.INSTANCE.close(rs);
            IOUtils.INSTANCE.close(ps);
        }
    }

    private String checkSyncConfig(Connection con, String syncDateKey, long idToSetIfNotExists, String initialValueIfNotExists) throws
        SQLException {
        String dbValue = initialValueIfNotExists;
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            ps = con.prepareStatement("SELECT CONFIG_VALUE FROM CRM_APPLICATION_CONFIG WHERE CONFIG_KEY = ?;");
            ps.setString(1, syncDateKey);
            rs = ps.executeQuery();
            if (!rs.next()) {
                IOUtils.INSTANCE.close(rs);
                IOUtils.INSTANCE.close(ps);
                ps = con.prepareStatement(
                    "INSERT INTO CRM_APPLICATION_CONFIG(ID,CREATED_DATETIME,CREATED_BY,CONFIG_KEY,CONFIG_VALUE) VALUES (?,?,?,?,?);");
                ps.setLong(1, idToSetIfNotExists);
                ps.setTimestamp(2, new Timestamp(System.currentTimeMillis()));
                ps.setString(3, "SYSTEM");
                ps.setString(4, syncDateKey);
                if (StringUtils.isNotBlank(initialValueIfNotExists)) {
                    ps.setString(5, initialValueIfNotExists);
                } else {
                    ps.setNull(5, Types.VARCHAR);
                }
                ps.executeUpdate();
                con.commit();
            } else {
                dbValue = rs.getString(1);
            }
            return dbValue;
        } finally {
            IOUtils.INSTANCE.close(rs);
            IOUtils.INSTANCE.close(ps);
        }
    }

    public String getStoreCode() {
        return storeCode;
    }

    public int getStoreId() {
        return storeId;
    }

    @Override
    public Timestamp getLastTxnSyncDate() throws SQLException {
        return getLastSyncDate(lastTxnSyncDateKey);
    }

    @Override
    public Timestamp getLastRefSyncDate() throws SQLException {
        return getLastSyncDate(lastRefSyncDateKey);
    }

    @Override
    public void updateLastTxnSyncDate(Timestamp timestamp) throws SQLException {
        updateLastSyncDate(timestamp, lastTxnSyncDateKey);
    }

    @Override
    public void updateLastRefSyncDate(Timestamp timestamp) throws SQLException {
        updateLastSyncDate(timestamp, lastRefSyncDateKey);
    }

    public String getPosIntegDbUsed() {
        return posIntegDbUsed;
    }

    public void updatePosIntegDbUsed(String posIntegDbUsed) throws SQLException {
        Connection con = null;
        PreparedStatement ps = null;
        try {
            con = localDataSource.getConnection();
            con.setAutoCommit(false);
            ps = con.prepareStatement("UPDATE CRM_APPLICATION_CONFIG SET CONFIG_VALUE = ? WHERE CONFIG_KEY = ?;");
            ps.setString(1, posIntegDbUsed);
            ps.setString(2, posIntegDbUsedKey);
            ps.executeUpdate();
            con.commit();

            this.posIntegDbUsed = posIntegDbUsed;
        } catch (SQLException e) {
            con.rollback();
            throw e;
        } finally {
            IOUtils.INSTANCE.close(ps);
            IOUtils.INSTANCE.close(con, false);
        }
    }

    @Override
    public String getMainDbUsed() {
        return mainDbUsed;
    }

    @Override
    public void updateMainDbUsed(String mainDbUsed) throws SQLException {
        Connection con = null;
        PreparedStatement ps = null;
        try {
            con = localDataSource.getConnection();
            con.setAutoCommit(false);
            ps = con.prepareStatement("UPDATE CRM_APPLICATION_CONFIG SET CONFIG_VALUE = ? WHERE CONFIG_KEY = ?;");
            ps.setString(1, mainDbUsed);
            ps.setString(2, mainDbUsedKey);
            ps.executeUpdate();
            con.commit();

            this.mainDbUsed = mainDbUsed;
        } catch (SQLException e) {
            con.rollback();
            throw e;
        } finally {
            IOUtils.INSTANCE.close(ps);
            IOUtils.INSTANCE.close(con, false);
        }
    }

    private Timestamp getLastSyncDate(String configKey) throws SQLException {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            con = localDataSource.getConnection();
            con.setAutoCommit(false);
            // com.ibm.ws.rsadapter.jdbc.WSJdbcConnection does not support readonly
            if (!con.getClass().getName().contains("WSJdbcConnection")) {
                con.setReadOnly(true);
            }
            ps = con.prepareStatement("SELECT CONFIG_VALUE FROM CRM_APPLICATION_CONFIG WHERE CONFIG_KEY = ?;");
            ps.setString(1, configKey);
            rs = ps.executeQuery();
            // Will always return single result. Else, something is wrong during POS installation.
            rs.next();
            String strTs = rs.getString(1);
            return StringUtils.isNotBlank(strTs) ? Timestamp.valueOf(strTs) : null;
        } finally {
            IOUtils.INSTANCE.close(rs);
            IOUtils.INSTANCE.close(ps);
            IOUtils.INSTANCE.close(con, true);
        }
    }

    private void updateLastSyncDate(Timestamp timestamp, String configKey) throws SQLException {
        Connection con = null;
        PreparedStatement ps = null;
        try {
            con = localDataSource.getConnection();
            con.setAutoCommit(false);
            ps = con.prepareStatement("UPDATE CRM_APPLICATION_CONFIG SET CONFIG_VALUE = ? WHERE CONFIG_KEY = ?;");
            ps.setString(1, timestamp.toString());
            ps.setString(2, configKey);
            ps.executeUpdate();
            con.commit();
        } catch (SQLException e) {
            con.rollback();
            throw e;
        } finally {
            IOUtils.INSTANCE.close(ps);
            IOUtils.INSTANCE.close(con, false);
        }
    }
}
