package com.transretail.crm.report.template;

import com.transretail.crm.core.service.StoreService;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author Monte Cillo Co (mco@exist.com)
 */
@Component
public class LOVFactory {

    @Autowired
    private StoreService storeService;

    @Transactional(readOnly = true)
    public Map<String, String> getStoreOptions() {
        return storeService.getAllStoresHashMap();
    }

}
