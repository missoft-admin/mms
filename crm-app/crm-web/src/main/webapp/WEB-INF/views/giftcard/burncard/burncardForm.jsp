<%@ include file="../../common/taglibs.jsp" %>
<%@include file="../../common/messages.jsp" %>

<style type="text/css">
  <!--

  #addBtn.btn-small {
    margin-bottom: 5px;
  }
  table select, textarea, table input[type="text"], input[type="password"], input[type="datetime"], input[type="datetime-local"], input[type="date"], input[type="month"], input[type="time"], input[type="week"], input[type="number"], input[type="email"], input[type="url"], input[type="search"], input[type="tel"], input[type="color"], .uneditable-input {
    margin-bottom: 0px;
    margin-top: 0px;
  }

  -->
</style>

<div id="content_burn_card" class="modal-dialog">

  <div class="modal-content">
    <div class="modal-header">
      <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
      <h4 class="modal-title"><c:out value="${dialogLabel}"/></h4>
    </div>

    <div class="modal-body">
      <form:form id="burnCardForm" modelAttribute="burnCardForm" name="burnCardForm" method="POST" action="/giftcard/burncard" >
        <div id="contentError" class="hide alert alert-error">
          <button type="button" class="close" onclick="$(this).parent().hide();">&times;</button>
          <div><form:errors path="*"/></div>
        </div>
        <div>

          <div class="span3">
            <div class="control-group">

              <label for="store" class="control-label"><spring:message code="gc.burncard.label.burn.no"/></label>
              <form:input type="text" class="input" path="burnNo" readonly="true"/>

              <label for="store" class="control-label"><spring:message code="gc.burncard.label.date.filed"/></label>
              <form:input type="text" class="input" path="dateFiled" readonly="true"/>

              <label for="store" class="control-label"><spring:message code="gc.burncard.label.time.filed"/></label>
              <form:input path="timeFiled" type="text" class="input" readonly="true"/>

              <label for="store" class="control-label"><spring:message code="gc.burncard.label.filed.by"/></label>
              <form:input type="text" class="input" path="filedBy" readonly="true"/>

              <label for="store" class="control-label"><spring:message code="gc.burncard.label.burn.reason"/><span class="required">*</span></label>
              <c:if test="${creation == true}">
                <form:select id="burnReason" path="burnReason">
                  <form:option value=""></form:option>
                  <c:forEach var="item" items="${burnReasons}">
                    <form:option value="${item.code}">${item.description}</form:option>
                  </c:forEach>
                </form:select>
              </c:if>
              <c:if test="${approval == true}">
                <form:select id="burnReason" path="burnReason" disabled="true">
                  <form:option value=""></form:option>
                  <c:forEach var="item" items="${burnReasons}">
                    <form:option value="${item.code}">${item.description}</form:option>
                  </c:forEach>
                </form:select>
              </c:if>
            </div>
          </div>

          <div class="control-group">
            <input type="radio" name="choose" value="series">
            <span style="margin-right: 10px"> By Series </span>
            <input type="radio" name="choose" value="so">
            <span> By SO </span>
          </div>

          <div class="control-group" id="startEndseries" hidden>
            <c:if test="${creation == true}">
              <spring:message code="gc.burncard.label.starting.series" var="lblStartBarcode"/>
              <form:input type="text" maxlength="16" class="input" id="seriesFrom" path="seriesFrom" placeholder="${lblStartBarcode}"/>
              <spring:message code="gc.burncard.label.ending.series" var="lblEndBarcode"/>
              <form:input type="text" maxlength="16" class="input" id="seriesTo" path="seriesTo" placeholder="${lblEndBarcode}"/>
            </c:if>
          </div>

          <div class="control-group" id="noso" hidden>
            <c:if test="${creation == true}">
              <spring:message code="gc.burncard.label.no.so" var="noSalesOrder"/>
              <form:input path="noSo" type="text" maxlength="9" class="input" id="salesorder" placeholder="${noSalesOrder}"/>
            </c:if>
          </div>

          <c:if test="${creation == true}">
            <div class="control-group">
              <input type="button" id="encodeButton" title="<spring:message code="gc.burncard.label.button.add"/>" class="btn btn-primary" value="<spring:message code="gc.burncard.label.button.add"/>"/>
              <input type="button" id="removeSeriesButton" title="<spring:message code="gc.burncard.label.remove.range"/>" class="btn btn-primary removeButton" value="<spring:message code="gc.burncard.label.remove.range"/>"/>
              <input type="button" id="clear" title="<spring:message code="label_clear"/>" class="btn" value="<spring:message code="label_clear"/>"/>
            </div>
          </c:if>

          <div class="control-group">
            <c:if test="${creation == true}">
              <form:select path="cards" id="cards">
                <c:forEach var="cards" items="${cards}">
                  <option value="${cards}">${cards}</option>
                </c:forEach>
              </form:select>
            </c:if>
            <c:if test="${approval == true}">
              <form:select path="cards" id="cards" disabled="true">
                <c:forEach var="cards" items="${cards}">
                  <option value="${cards}">${cards}</option>
                </c:forEach>
              </form:select>
            </c:if>
          </div>

          <div class="control-group">
            <div id="editForm" hidden="true">
              <input id="cardEdit" type="text" maxlength="16" class="input"></input>
              <input id="editConfirm" type="button" class="btn btn-primary" value="<spring:message code="button_update"/>">
              <input id="removeButton" type="button" class="btn btn-primary removeButton" value="<spring:message code="label_remove"/>">
            </div>
          </div>
        </div>
      </form:form>
    </div>

    <div class="modal-footer">
      <c:if test="${creation == true}">
        <button type="button" id="submit" class="btn btn-primary"><spring:message code="gc.burncard.label.file.gc.burn.request"/></button>
        <button type="button" id="save" class="btn btn-primary"><spring:message code="gc.burncard.label.save.gc.burn.request"/></button>
      </c:if>
      <c:if test="${approval == true && approver == true}">
        <button type="button" id="burn" data-msg-confirm="<spring:message code="gc.burncard.confirmation" />" class="btn btn-primary"><spring:message code="gc.burncard.label.button.burn"/></button>
        <button type="button" id="reject" class="btn btn-primary"><spring:message code="gc.burncard.label.reject.gc.burn.request"/></button>
      </c:if>
      <button type="button" id="" class="btn btn-default" data-dismiss="modal"><spring:message code="label_cancel"/></button>
    </div>
  </div>

</div>

<jsp:include page="../../common/confirm.jsp" />

<script src="<spring:url value="/js/jquery/jquery.numberInput.js" />" ></script>

<script type="text/javascript">
    $(document).ready(function() {
        var $seriesForm = $("#burnCardForm");
        var CONTENT_ERROR = $("#contentError");
        var cardsList =  $("#cards");
        var editConfirm = $("#editConfirm");
        initButtons();
        initForms();

        function initForms() {
            var cardEdit = $("#cardEdit");
            var editForm = $("#editForm");

            cardEdit.numberInput("type", "int");
            $("#seriesTo").numberInput("type", "int");
            $("#seriesFrom").numberInput("type", "int");

            cardsList.click(function(e) {
                e.preventDefault();
                if ($("#cards option:selected" ).text() != "") {
                    editForm.show("slow");
                    $(".removeButton").show("slow");
                    cardEdit.val($("#cards option:selected" ).text());
                }
            });

            cardEdit.keyup(function(e) {
                if (cardEdit.val() != $("#cards option:selected" ).text()) {
                    $(".removeButton").hide("slow");
                }
            });
        }

        function encodeNum(urlAction) {

            $("#cards option").prop('selected', true);
            var dataObject = $("#burnCardForm").serialize();
            $("#encodeButton").prop('disabled', true);
            $.post(
                urlAction,
                dataObject,
                function(response) {
                    if (response.success) {

                        cardsList.empty();
                        $.each(response.result, function(key, value) {
                            var o = new Option(value,value);
                            cardsList.append(o);
                        });
                        $("#encodeButton").prop('disabled', false);

                    } else {

                        var theErrorInfo = "";
                        $.each( response.result, function( key, value ) {
                            theErrorInfo += ( ( key + 1 ) + ". " + value.code + "<br>" );
                        });

                        CONTENT_ERROR.find( "div" ).html( theErrorInfo );
                        CONTENT_ERROR.show( "slow" );
                        $("#encodeButton").prop('disabled', false);
                    }

                    $("#editForm").hide("slow");
                }

            );
        }

        function initButtons() {

            $("#encodeButton").click(function(e) {

                e.preventDefault();
                if ($("#seriesFrom").val() !== "" || $("#seriesTo").val() !== "") {

                    var urlAction = ctx + $seriesForm.attr("action") + "/encode";
                    encodeNum(urlAction);

                } else if ($("#salesorder").val() !== "") {

                    var  url = ctx + $seriesForm.attr("action") + "/nosalesorder";
                    encodeNum(url);

                }
            });

            $("#removeSeriesButton").click(function(e) {
                e.preventDefault();

                if ($("#seriesFrom").val() != "" || $("#seriesTo").val() != "") {
                    var urlAction = ctx + $seriesForm.attr("action") + "/remove";
                    $("#cards option").prop('selected', true);
                    var dataObject = $("#burnCardForm").serialize();
                    $("#removeSeriesButton").prop('disabled', true);
                    $.post(
                        urlAction,
                        dataObject,
                        function(response) {
                            if (response.success) {
                                cardsList.empty();
                                $.each(response.result, function(key, value) {
                                    var o = new Option(value,value);
                                    cardsList.append(o);
                                });
                            } else {
                                var theErrorInfo = "";
                                $.each( response.result, function( key, value ) {
                                    theErrorInfo += ( ( key + 1 ) + ". " + value.code + "<br>" );
                                });
                                CONTENT_ERROR.find( "div" ).html( theErrorInfo );
                                CONTENT_ERROR.show( "slow" );
                            }
                            $("#removeSeriesButton").prop('disabled', false);
                            $("#editForm").hide("slow");
                        }
                    );
                }
            });

            $("#submit").click(function(e) {
                e.preventDefault();
                var urlAction = ctx + $seriesForm.attr("action") + "/saveForApproval";
                console.log(urlAction);
                $("#cards option").prop('selected', true);
                var dataObject = $("#burnCardForm").serialize();
                $.post(
                    urlAction,
                    dataObject,
                    function(response) {
                        if (response.success) {
                            cardsList.empty();
                            $("#cards option").prop('selected', false);
                            location.reload();
                        } else {
                            var theErrorInfo = "";
                            for (i=0; i < response.result.length; i++) {
                                theErrorInfo += (i + 1) + ". "
                                    + (response.result[i].code != undefined ? response.result[i].code : response.result[i])
                                    +"<br>";
                            }
                            CONTENT_ERROR.find( "div" ).html( theErrorInfo );
                            CONTENT_ERROR.show( "slow" );
                        }
                        $("#editForm").hide("slow");
                    }
                );
            });

            $("#save").click(function(e) {
                e.preventDefault();
                var urlAction = ctx + $seriesForm.attr("action") + "/saveDraft";
                console.log(urlAction);
                $("#cards option").prop('selected', true);
                var dataObject = $("#burnCardForm").serialize();
                $.post(
                    urlAction,
                    dataObject,
                    function(response) {
                        if (response.success) {
                            cardsList.empty();
                            $("#cards option").prop('selected', false);
                            location.reload();
                        } else {
                            var theErrorInfo = "";
                            for (i=0; i < response.result.length; i++) {
                                theErrorInfo += (i + 1) + ". "
                                    + (response.result[i].code != undefined ? response.result[i].code : response.result[i])
                                    +"<br>";
                            }
                            CONTENT_ERROR.find( "div" ).html( theErrorInfo );
                            CONTENT_ERROR.show( "slow" );
                        }
                        $("#editForm").hide("slow");
                    }
                );
            });

            $("#burn").click(function(e) {
                e.preventDefault();
                var urlAction = ctx + $seriesForm.attr("action") + "/burn";
                $("#cards option").prop('readOnly', false);
                $("#cards option").prop('selected', true);
                var dataObject = $("#burnCardForm").serialize();
                var message = $("#burn").data("msg-confirm");
                $("#burn").data("msg-confirm", message.replace("%NUM%", $("#cards option").size()));
                getConfirm($("#burn").data( "msg-confirm" ), function( result ) {
                    if(result) {
                        console.log("test");
                        $.ajax({
                            type: 'POST',
                            url: urlAction,
                            data: dataObject,
                            success: function(data, textStatus, jqXHR) {
                                cardsList.empty();
                                $("#cards option").prop('selected', false);
                            },
                            complete: function(jqXHR, textStatus) {
                                location.reload();
                            },
                            async:false
                        });
                    } else {
                        location.reload();
                    }
                });
            });

            $("#reject").click(function(e) {
                e.preventDefault();
                var urlAction = ctx + $seriesForm.attr("action") + "/reject";
                console.log(urlAction);
                $("#cards option").prop('selected', true);
                var dataObject = $("#burnCardForm").serialize();
                $.ajax({
                    type: 'POST',
                    url: urlAction,
                    data: dataObject,
                    success: function(data, textStatus, jqXHR) {
                        cardsList.empty();
                        $("#cards option").prop('selected', false);
                    },
                    complete: function(jqXHR, textStatus) {
                        location.reload();
                    },
                    async:false
                });
            });

            var cardEdit = $("#cardEdit");
            var editForm = $("#editForm");

            editConfirm.click(function(e) {
                e.preventDefault();

                if ( $("#cards option:selected").val() == cardEdit.val()) {
                    editForm.hide("slow");
                    return;
                }
                var urlAction = ctx + $seriesForm.attr("action") + "/validate/" + cardEdit.val();
                var toBeEdited = $("#cards option:selected").val();
                $("#cards option").prop('selected', true);
                var dataRequest = $("#burnCardForm").serialize();
                $.post(
                    urlAction,
                    dataRequest,
                    function(response) {
                        if (response.success) {
                            $("#cards option").prop('selected', false);
                            $("#cards option[value='" + toBeEdited + "']").prop('selected', true);
                            $("#cards option[value='" + toBeEdited + "']").text(cardEdit.val());
                            $("#cards option[value='" + toBeEdited + "']").val(cardEdit.val());
                            editForm.hide("slow");
                        } else {
                            $("#cards option").prop('selected', false);
                            $("#cards option[value='" + toBeEdited + "']").prop('selected', true);
                            var theErrorInfo = "";
                            $.each( response.result, function( key, value ) {
                                theErrorInfo += ( ( key + 1 ) + ". " + value.code + "<br>" );
                            });
                            CONTENT_ERROR.find( "div" ).html( theErrorInfo );
                            CONTENT_ERROR.show( "slow" );
                            $("#removeButton").show("slow");
                        }
                    },
                    "json"
                );
            });

            $("#removeButton").click(function(e) {
                e.preventDefault();
                $("#cards option:selected").remove();
                editForm.hide("slow");
            });

            $("#clear").click(function(e) {
                e.preventDefault();
                $("#cards option:selected").remove();
                $("#seriesFrom").val("");
                $("#seriesTo").val("");
                cardEdit.val("");
                cardsList.empty();
                editForm.hide("slow");
            });
        }

        $('input[name=choose]').on('change', function () {

            if ($(this).val() == 'series') {

                $("#startEndseries").show();
                $("#seriesFrom").val("");
                $("#seriesTo").val("");

                $("#noso").hide();
                $("#salesorder").val("");

            } else {

                $("#startEndseries").hide();
                $("#seriesFrom").val("");
                $("#seriesTo").val("");

                $("#noso").show();
                $("#salesorder").val("");
            }
        })

    });
</script>