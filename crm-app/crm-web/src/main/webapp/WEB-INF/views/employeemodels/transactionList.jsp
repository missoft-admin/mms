<%@ include file="../common/taglibs.jsp" %>

<style type="text/css">
<!--
  #content_txnList .table tbody tr td:last-child {
      border-left: none;
  }
-->
</style>
 
<div class="modal hide  groupModal nofly" tabindex="-1" role="dialog" aria-hidden="true" id="viewTxnDialog">
  <spring:message code="label_com_transretail_crm_entity_employeetransactionmodel" var="typeName" />

  <script>
  
  function reloadTxnTable(empId) {
	  
	  $("#list_txn").empty().ajaxDataTable({
	      'autoload'  : true,
	      'ajaxSource' : "<c:url value="/employeemodels/purchase/list/" />" + empId,
	      'columnHeaders' : [
		  "<spring:message code="label_com_transretail_crm_entity_employeetransactionmodel_storecode"/>",
	      "<spring:message code="label_com_transretail_crm_entity_employeetransactionmodel_transactionno"/>",
	      "<spring:message code="label_com_transretail_crm_entity_employeetransactionmodel_transactiondatetime"/>",
	      "<spring:message code="label_com_transretail_crm_entity_employeetransactionmodel_transactionamount"/>",
	      "<spring:message code="label_com_transretail_crm_entity_employeetransactionmodel_transactiontype"/>",
	      "<spring:message code="label_com_transretail_crm_entity_employeetransactionmodel_paymenttype"/>",
	      ""
	      ],
	      'modelFields' : [
			 {name : 'store', sortable : true},
	         {name : 'transactionNo', sortable : true},
	         {name : 'transactionDateTime', sortable : true, fieldCellRenderer : function (data, type, row) {
                 return ( (row.longTxnDate)? new Date( row.longTxnDate ).customize(2) : "" );
             }},
	         {name : 'transactionAmount', sortable : true, fieldCellRenderer : function (data, type, row) {
		           return row.transactionAmountFormatted;
		     }},
		     {name : 'transactionType', sortable: true},
		     {name : 'paymentType', sortable: false},
	         {customCell : function ( innerData, sSpecific, json ) {
	              if ( sSpecific == 'display') {
	            	return "<a class=\"btn btn-primary tiptip icn-view link_itemList\" data-txn-no=\""+json.transactionNo+"\" href=\"#\" title=\"<spring:message code="points_label_viewitems" />\"><spring:message code="points_label_viewitems" /></a>";
	              } else {
	                return '';
	              }
	              }
	         }
	       ]
	  }).on("click", ".link_itemList", function(e) {
		  	e.preventDefault();
	    	$( "#list_item" ).data( "txn-no", $( this ).data( "txn-no" ) );
	    	$( "#content_transactionNo" ).html( $( this ).data( "txn-no" ) );
	    	$("#viewTxnDialog").modal( "hide" );
	    	$( "#content_itemList" ).modal( "show" );
	    });  
  }
    
  </script>

  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
  		  <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
  		  <h4 class="isPrimary"><spring:message code="member_emp_prop_txn_currentmo" />: <span class="currentTxnTotal"></h4>
  		</div>  

      <div class="modal-body">

        <div id="content_txnList">
          <div id="list_txn"></div>
        </div>

      </div>
      <div class="modal-footer container-btn">
        <spring:url var="url_print" value="/report/employee/transaction/purchase/export/pdf" />
        <a href="#" id="contentPrint"
          onclick="openCalendar('${url_print}')"
          class="link01 btn btn-print">Print</a>
        <button type="button" class="btn" data-dismiss="modal"><spring:message code="label_close" /></button>
      </div>
    </div>
  </div>
</div>