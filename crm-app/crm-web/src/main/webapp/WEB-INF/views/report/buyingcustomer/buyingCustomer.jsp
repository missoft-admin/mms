<%@include file="/WEB-INF/views/common/taglibs.jsp" %>
<div class="form-horizontal">
    <div class="page-header page-header2"><h1><spring:message code="buying.customer.report.label"/>
    </h1></div>

    <div class="errorMessages error">&nbsp;</div>

    <div class="control-group">
        <label class="control-label"></label>
        <div class="controls">
            <select name="reportType" id="reportType">
                <c:forEach items="${reportTypes}" var="reportType">
                    <option value="${reportType.code}"><spring:message code="report_type_${reportType.code}"/></option>
                </c:forEach>
            </select>
        </div>
    </div>

    <fieldset>
        <legend><spring:message code="report.filters.label"/></legend>
        <form id="filters">
            <div class="control-group">
                <label class="control-label"><spring:message code="report.period"/></label>
                <div class="controls">
                    <select name="standardReportType" id="standardReportType">
                        <option value="weekly"><spring:message code="report.weekly"/></option>
                        <option value="monthly"><spring:message code="report.monthly"/></option>
                    </select>
                </div>
            </div>

            <div class="control-group">
                <label class="control-label"><span class="required">*</span> <spring:message code="member.registration.dateFrom"/></label>
                <div class="controls">
                    <input name="dateFrom" id="dateFrom" type="text" placeHolder="Date From"/>
                </div>
            </div>

            <div class="control-group">
                <label class="control-label"><span class="required">*</span> <spring:message code="member.registration.dateTo"/></label>
                <div class="controls">
                    <input name="dateTo" id="dateTo" type="text" placeHolder="Date To"/>
                </div>
            </div>

            <div class="control-group store">
                <label class="control-label"><spring:message code="report.store"/></label>
                <div class="controls">
                    <select name="store" id="store" class="searchDropdown">
                        <option value=""></option>
                        <c:forEach items="${stores}" var="store">
                            <option value="${store.code}">${store.code} - ${store.name}</option>
                        </c:forEach>
                    </select>
                </div>
            </div>

        </form>
        <div class="control-group form-actions">
            <button id="view" type="button" class="btn btn-primary" data-loading-text="Loading..." data-url="${exportUrl}"><spring:message code="view"/></button>
        </div>
    </fieldset>

    <jsp:include page="../../common/noDataFound.jsp"/>

</div>
<style>
    #templateName {
        width: 30%;
    }
</style>

<%--<jsp:include page="../../common/noDataFound.jsp"/>--%>
<script src="<c:url value="/js/bootstrap/bootstrap-datepicker.js"/>"></script>
<link href="<c:url value="/css/bootstrap/datepicker.css" />" rel="stylesheet"/>

<script>
    $(document).ready(function () {

        var today = new Date();
        var currMonth = today.getMonth();
        var currYear = today.getFullYear();
        var firstDayOfMonth = new Date(currYear, currMonth, 1);

        var errorMessagesContainer = $('div.errorMessages');

        initFields();
        initButtons();

        function initFields() {
            $('#standardReportType').change(function (e) {
                resetFields();
            });
            $('#dateFrom').datepicker({
                autoclose: true,
                format: 'dd-mm-yyyy',
                endDate: today
            });

            $('#dateTo').datepicker({
                autoclose: true,
                format: 'dd-mm-yyyy',
                endDate: today
            });
        }

        function initButtons() {
            $("#view").click(function (e) {
                $("#view").button("loading");

                var selectedReportType = "reportType=" + $("select[name='reportType']").val();
                var standardReportType = "&standardReportType=" + $("#standardReportType").val();
                var dateFrom = "&dateFrom=" + $("#dateFrom").val();
                var dateTo = "&dateTo=" + $("#dateTo").val();
                var store = "&store=" + $("#store").val();

                var url = ctx + "/member/buyingcustomer/view?" + selectedReportType + standardReportType;

                if ($("#dateFrom").val() != "") {
                    url = url + dateFrom;
                }
                if ($("#dateTo").val() != "") {
                    url = url + dateTo;
                }
                if ($("#store").val() != "") {
                    url = url + store;
                }

                var selectedTemplateName = 'Buying Customer Weekly and Monthly Report';
                var validateUrl = ctx + "/member/buyingcustomer/print/validate";
                var dataCheck = ctx + "/report/template/" + selectedTemplateName + "/data/check";
                var filters = $("#filters").serialize();

                $.post(dataCheck, filters, function (noData) {
                    if (noData) {
                        $("#noData").modal('show');
                        $("#view").button("reset");
                    } else {
                        $.post(validateUrl, filters, function (data) {
                            if (data.success) {
                                if ($("select[name='reportType']").val() == "pdf") {
                                    window.open(url, '_blank', 'toolbar=0,location=0,menubar=0');
                                } else if ($("select[name='reportType']").val() == "excel") {
                                    window.location.href = url;
                                }
                            } else {
                                if (data.result[0] === "No Data Found") {
                                    $("#noData").modal('show');
                                } else {
                                    var errorInfo = "1. " + data.result[0];
                                    for (var i = 1; i < data.result.length; i++) {
                                        errorInfo += "<br>" + (i + 1) + ". " + data.result[i];
                                    }
                                    showErrorMessage(errorInfo);
                                }
                            }
                            $("#view").button("reset");
                        });
                    }
                });

            });
        }

        var showErrorMessage = function (message) {
            errorMessagesContainer.html('<div class="alert alert-error">' + '<button type="button" class="close" data-dismiss="alert">&times;</button>' + message + '</div>');
        };

        function resetFields() {
            $("#dateFrom").val('')
            $("#dateTo").val('');
            var reportType = $('#standardReportType').val();

            if (reportType == 'monthly') {

                var today = new Date();
                // Find last day in month
                var nextMonth = new Date(currYear, currMonth + 1, 0);
                var lastDayOfMonth = nextMonth.getDate();

                // initialize to current month
                $('#dateFrom').datepicker('setDate', firstDayOfMonth)
                $('#dateTo').datepicker('setDate', new Date(currYear, currMonth, lastDayOfMonth));
            }
        }
    });
</script>