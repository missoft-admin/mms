package com.transretail.crm.core.service.impl;


import java.util.List;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Configurable;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import com.google.common.collect.Lists;
import com.transretail.crm.core.dto.ProductGroupDto;
import com.transretail.crm.core.dto.ProductHierDto;
import com.transretail.crm.core.entity.ProductGroup;
import com.transretail.crm.core.entity.lookup.Product;
import com.transretail.crm.core.repo.ProductGroupRepo;
import com.transretail.crm.core.repo.ProductHierRepo;
import com.transretail.crm.core.repo.ProductRepo;
import com.transretail.crm.core.service.ProductGroupService;
import com.transretail.crm.core.service.ProductHierService;
import com.transretail.crm.product.HierarchyLevel;
import com.transretail.crm.product.entity.CrmProductHier;


@Configurable
@Transactional
@ContextConfiguration(locations = "classpath*:/META-INF/spring/applicationContext*.xml")
@RunWith(SpringJUnit4ClassRunner.class)
public class ProductGroupServiceTest {
	
	@Autowired
	ProductRepo productRepo;	
	@Autowired
	ProductGroupRepo productGroupRepo;	
	@Autowired
	ProductGroupService productGroupService;

	@Autowired
	ProductHierService productHierService;
	@Autowired
	ProductHierRepo productHierRepo;

	private Long prgrp1;



	@Before
	public void setup() {
		Product product = new Product();
		product.setItemCode("001");
		product.setName("Product1");
		product.setId("001");
		productRepo.saveAndFlush(product);
		
		product = new Product();
		product.setItemCode("002");
		product.setName("Product2");
		product.setId("002");
		productRepo.saveAndFlush(product);
		
		product = new Product();
		product.setItemCode("003");
		product.setName("Product3");
		product.setId("003");
		productRepo.saveAndFlush(product);
		
		product = new Product();
		product.setItemCode("004");
		product.setName("Product4");
		product.setId("004");
		productRepo.saveAndFlush(product);
		
		ProductGroup productGroup = new ProductGroup();
		productGroup.setBrand("brand1");
		productGroup.setProducts("001,002");
		prgrp1 = productGroupRepo.saveAndFlush(productGroup).getId();
		
		productGroup = new ProductGroup();
		productGroup.setBrand("brand2");
		productGroup.setProducts("003,004");
		productGroupRepo.saveAndFlush(productGroup);
		
	}
	
	@After
	public void teardown() {
		productGroupRepo.deleteAll();
		productRepo.deleteAll();
	}
	
	@Test
	public void testSearchItems() {
		List<Product> products = Lists.newArrayList(productGroupService.searchItems("0000001"));
		Assert.assertEquals(0, products.size());
		
		products = Lists.newArrayList(productGroupService.searchItems("001"));
		Assert.assertEquals(1, products.size());
		
		products = Lists.newArrayList(productGroupService.searchItems("00*"));
		Assert.assertEquals(4, products.size());
		
		products = Lists.newArrayList(productGroupService.searchItems("Product*"));
		Assert.assertEquals(4, products.size());
	}
	
	@Test
	public void testFindAll() {
		List<ProductGroup> groups = productGroupService.findAll();
		Assert.assertNotNull(groups);
		Assert.assertEquals(2, groups.size());
 	}
	
	@Test
	public void testSave() {
		Assert.assertEquals(2, productGroupRepo.count());
		
		ProductGroup group = productGroupRepo.findOne(prgrp1);
		group.setBrand("newbrand");
		productGroupService.save(group);
		
		Assert.assertEquals(2, productGroupRepo.count());
		
		group = new ProductGroup();
		group.setBrand("brand3");
		group.setProducts("001,004");
		productGroupService.save(group);
		
		Assert.assertEquals(3, productGroupRepo.count());
	}
	
	@Test
	public void testFindOne() {
		ProductGroup group = productGroupService.findOne(prgrp1);
		
		Assert.assertNotNull(group);
		Assert.assertEquals("brand1", group.getBrand());
		Assert.assertEquals("001,002", group.getProducts());
	}
	
	@Test
	public void testDelete() {
		Assert.assertEquals(2, productGroupRepo.count());
		
		productGroupService.delete(prgrp1);
		Assert.assertEquals(1, productGroupRepo.count());
	}
	
	@Test
	public void testGetItems() {
		List<Product> products = productGroupService.getItems(prgrp1);
		
		Assert.assertNotNull(products);
		Assert.assertEquals(2, products.size());
	}

	@Test
	public void testSearch() {
		ProductGroup group = new ProductGroup();
		group.setBrand("brand1");
		group.setProducts("001");
		
		List<ProductGroup> productGroups = Lists.newArrayList(productGroupService.search(group));
		
		Assert.assertNotNull(productGroups);
		Assert.assertEquals(1, productGroups.size());
		
		group = new ProductGroup();
		group.setBrand("brand");
		
		productGroups = Lists.newArrayList(productGroupService.search(group));
		
		Assert.assertNotNull(productGroups);
		Assert.assertEquals(2, productGroups.size());
	}
	
	@Test
	public void testGetAllGroups() {
		List<ProductGroup> groups = productGroupService.getAllGroups();
		
		Assert.assertNotNull(groups);
		Assert.assertEquals(2, groups.size());
	}



	//TODO
	//@Test
	public void testSearchProductAndCfnsGetProductsSearchProducts() {
		setupProductAndClassification();

		List<ProductGroupDto.CodeDescBean> beans = productGroupService.searchProductAndCfns( "1", true, 
				HierarchyLevel.DIVISION.ordinal()
				+ "," + HierarchyLevel.DEPARTMENT.ordinal()
				+ "," + HierarchyLevel.GROUP_FAMILY.ordinal()
				+ "," + HierarchyLevel.FAMILY.ordinal()
				+ "," + HierarchyLevel.SUB_FAMILY.ordinal() );
		Assert.assertEquals( 6L, beans.size() );
		beans = productGroupService.searchProductAndCfns( "11", false, 
				HierarchyLevel.DIVISION.ordinal()
				+ "," + HierarchyLevel.DEPARTMENT.ordinal()
				+ "," + HierarchyLevel.GROUP_FAMILY.ordinal() );
		Assert.assertEquals( 2L, beans.size() );

		beans = productGroupService.getProducts( "111111111111, 111111111112",  "11" );
		Assert.assertEquals( 1L, beans.size() );
		beans = productGroupService.getProducts( "",  "11" );
		Assert.assertEquals( 1L, beans.size() );
		beans = productGroupService.getProducts( "",  "12" );
		Assert.assertEquals( 0L, beans.size() );

		beans = productGroupService.searchProducts( "11", "",  "1, 11, 111" );
		Assert.assertEquals( 1L, beans.size() );
		beans = productGroupService.searchProducts( "Pea", "",  "1, 11, 111" );
		Assert.assertEquals( 1L, beans.size() );
		beans = productGroupService.searchProducts( "Pea", "",  "12, 112" );
		Assert.assertEquals( 0L, beans.size() );
		beans = productGroupService.searchProducts( "Unknown", "",  "1, 11, 111" );
		Assert.assertEquals( 0L, beans.size() );

		teardownProductAndClassification();
	}

	private CrmProductHier toModel( ProductHierDto dto ) {
		CrmProductHier model = new CrmProductHier();
		model.setCode( dto.getCode() );
		model.setLevel( ProductHierDto.getHierarchyLevel( dto.getLevel() ) );
		model.setEnglishDesc( dto.getDescription() );
		return model;
	}

	private void setupProductAndClassification() {
		//productHierRepo
		ProductHierDto cfn = new ProductHierDto();
		cfn.setCode( "1" );
		cfn.setDescription( "Food" );
		cfn.setLevel( String.valueOf( HierarchyLevel.DIVISION ) );
		productHierRepo.save( toModel( cfn ) );

		cfn = new ProductHierDto();
		cfn.setCode( "11" );
		cfn.setDescription( "Sandwich Spread" );
		cfn.setLevel( String.valueOf( HierarchyLevel.DEPARTMENT ) );
		productHierRepo.save( toModel( cfn ) );

		cfn = new ProductHierDto();
		cfn.setCode( "111" );
		cfn.setDescription( "Peanut Butter" );
		cfn.setLevel( String.valueOf( HierarchyLevel.GROUP_FAMILY ) );
		productHierRepo.save( toModel( cfn ) );

		cfn = new ProductHierDto();
		cfn.setCode( "1111" );
		cfn.setDescription( "Crunchy Peanut Butter" );
		cfn.setLevel( String.valueOf( HierarchyLevel.FAMILY ) );
		productHierRepo.save( toModel( cfn ) );

		cfn = new ProductHierDto();
		cfn.setCode( "11111" );
		cfn.setDescription( "Crunchy Peanut Butter (Small)" );
		cfn.setLevel( String.valueOf( HierarchyLevel.SUB_FAMILY ) );
		productHierRepo.save( toModel( cfn ) );

		Product prd = new Product();
		prd.setId( "10001111111L" );
		prd.setItemCode( "111111111111" );
		prd.setDescription( "Peter Pan Crunchy Peanut Butter" );
		productRepo.save( prd );
	}

	private void teardownProductAndClassification() {
		productRepo.deleteAll();
		productHierRepo.deleteAll();
	}

}


















