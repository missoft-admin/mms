-- https://projects.exist.com/issues/show/97783
alter table crm_store_psoft SET REFERENTIAL_INTEGRITY FALSE;

update crm_ref_lookup_dtl set code='ID030', description='Carrefour' where code='STBU030';

update crm_store_psoft set bu_code='ID030' where bu_code='STBU030';

alter table crm_store_psoft SET REFERENTIAL_INTEGRITY TRUE;

-- https://projects.exist.com/issues/show/98006
INSERT INTO CRM_REF_LOOKUP_DTL(CODE, DESCRIPTION, HDR, STATUS) VALUES ('ID050', 'ARI', 'STO000', 'ACTIVE');
INSERT INTO CRM_REF_LOOKUP_DTL(CODE, DESCRIPTION, HDR, STATUS) VALUES ('ID110', 'TGI', 'STO000', 'ACTIVE');
