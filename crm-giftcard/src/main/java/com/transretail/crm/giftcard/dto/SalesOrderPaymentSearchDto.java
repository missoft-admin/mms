package com.transretail.crm.giftcard.dto;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.joda.time.LocalDate;

import com.mysema.query.types.expr.BooleanExpression;
import com.transretail.crm.common.service.dto.request.AbstractSearchFormDto;
import com.transretail.crm.giftcard.entity.QSalesOrderPaymentInfo;
import com.transretail.crm.giftcard.entity.support.PaymentInfoStatus;

/**
 *
 */
public class SalesOrderPaymentSearchDto extends AbstractSearchFormDto {
	
	private String salesOrderNo;
	private String customer;
	private LocalDate paymentDateFrom;
	private LocalDate paymentDateTo;
	private String status;
	
	public String getSalesOrderNo() {
		return salesOrderNo;
	}
	public void setSalesOrderNo(String salesOrderNo) {
		this.salesOrderNo = salesOrderNo;
	}
	public String getCustomer() {
		return customer;
	}
	public void setCustomer(String customer) {
		this.customer = customer;
	}
	public LocalDate getPaymentDateFrom() {
		return paymentDateFrom;
	}
	public void setPaymentDateFrom(LocalDate paymentDateFrom) {
		this.paymentDateFrom = paymentDateFrom;
	}
	public LocalDate getPaymentDateTo() {
		return paymentDateTo;
	}
	public void setPaymentDateTo(LocalDate paymentDateTo) {
		this.paymentDateTo = paymentDateTo;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	@Override
	public BooleanExpression createSearchExpression() {
		QSalesOrderPaymentInfo qPayment = QSalesOrderPaymentInfo.salesOrderPaymentInfo;
		List<BooleanExpression> expressions = new ArrayList<BooleanExpression>();
		
		if(StringUtils.isNotBlank(salesOrderNo)) {
			expressions.add(qPayment.order.orderNo.startsWithIgnoreCase(salesOrderNo));
		}
		if(StringUtils.isNotBlank(customer)) {
			expressions.add(qPayment.order.customer.name.containsIgnoreCase(customer));
		}
		
		if (paymentDateFrom != null && paymentDateTo != null) {
		    expressions.add(qPayment.paymentDate.between(paymentDateFrom, paymentDateTo));
		} else if(paymentDateFrom != null) {
			expressions.add(qPayment.paymentDate.goe(paymentDateFrom));
		} else if(paymentDateTo != null) {
			expressions.add(qPayment.paymentDate.loe(paymentDateTo));
		}
		
		if(StringUtils.isNotBlank(status)) {
			expressions.add(qPayment.status.eq(PaymentInfoStatus.valueOf(status)));
		}
		return BooleanExpression.allOf(expressions.toArray(new BooleanExpression[expressions.size()]));
	}
	
	
}
