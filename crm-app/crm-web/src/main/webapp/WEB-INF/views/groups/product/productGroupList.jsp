<%@ include file="../../common/taglibs.jsp" %>


<div id="content_productGroupList">

  <jsp:include page="../../common/confirm.jsp" />
  <div id="list_productGroup" 
    data-url="<c:url value="/groups/product/search/list/" />"
    data-hdr-groupname="<spring:message code="label_group_groupname"/>"
    data-hdr-productcfns="<spring:message code="group_product_andclassifications_selected"/>" 
    data-hdr-excproducts="<spring:message code="group_product_excluded"/>" ></div>

  <div id="btn_viewProductGroup" class="hide">
    <a href="#" class="btn tiptip icn-view btn_viewProductGroup pull-left" data-id="%ID%" data-name="%NAME%" title="<spring:message code="label_view"/>"><spring:message code="label_view"/></a>
  </div>
  <div id="btn_editProductGroup" class="hide">
    <a href="#" class="btn tiptip icn-edit btn_editProductGroup pull-left" data-url="<c:url value="/groups/product/edit/%ID%" />" title="<spring:message code="label_edit"/>"><spring:message code="label_edit"/></a>
  </div>
  <div id="btn_deleteProductGroup" class="hide">
    <a href="#" class="btn tiptip icn-delete btn_deleteProductGroup pull-left" title="<spring:message code="label_delete"/>" data-url="<c:url value="/groups/product/delete/%ID%" />" data-msg-confirm="<spring:message code="entity_delete_confirm" />" >
      <spring:message code="label_delete"/>
    </a>
  </div>


  <link href="<spring:url value="/css/dataTables.css" />" rel="stylesheet">
  <script src="<spring:url value="/js/jquery/jquery.dataTables.js" />" ></script>
  <script src="<spring:url value="/js/jquery/jquery.dataTables.linkLengthMenu.js" />"></script>
  <script src="<spring:url value="/js/jquery/jquery.dataTables.customPaging.js" />"></script>
  <script src="<spring:url value="/js/jquery/ajax-datatable.js" />" ></script>
  <script src="<spring:url value="/js/common/form2js.js" />" ></script>
  <script src="<spring:url value="/js/common/json2.js" />" ></script>
  <script src="<spring:url value="/js/jquery/jquery.toObject.js" />" ></script>

  <script src="<spring:url value="/js/viewspecific/groups/product/productGroupList.js"/>"></script>


</div>