package com.transretail.crm.core.entity;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.envers.Audited;

import com.transretail.crm.core.entity.lookup.StoreGroup;

/**
 * @author Allan G. Ramirez (agramirez@exist.com)
 */
@Audited
@Entity
@Table(name = "CRM_PROMOTION_STORE_GRP")
public class PromotionStoreGroup extends PromotionTargetGroup<String> implements Serializable {
    private static final long serialVersionUID = -9052721252479723069L;
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "STORE_GRP_ID")
    private StoreGroup storeGroup;

    public StoreGroup getStoreGroup() {
        return storeGroup;
    }

    public void setStoreGroup(StoreGroup storeGroup) {
        this.storeGroup = storeGroup;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        PromotionStoreGroup that = (PromotionStoreGroup) o;

        if (!storeGroup.equals(that.storeGroup)) return false;
        if (!getPromotion().equals(that.getPromotion())) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = getPromotion().hashCode();
        result = 31 * result + storeGroup.hashCode();
        return result;
    }
}
