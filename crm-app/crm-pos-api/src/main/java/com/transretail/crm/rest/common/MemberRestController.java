package com.transretail.crm.rest.common;

import com.transretail.crm.common.enums.MessageType;
import com.transretail.crm.common.service.dto.rest.ProfessionalRestDto;
import com.transretail.crm.common.service.dto.rest.ReturnMessage;
import com.transretail.crm.common.util.DateUtil;
import com.transretail.crm.common.util.StringUtil;
import com.transretail.crm.core.dto.MemberDto;
import com.transretail.crm.core.dto.MemberGroupDto;
import com.transretail.crm.core.dto.ShoppingListDto;
import com.transretail.crm.core.entity.MemberModel;
import com.transretail.crm.core.entity.embeddable.Address;
import com.transretail.crm.core.entity.embeddable.CustomerProfile;
import com.transretail.crm.core.entity.embeddable.Npwp;
import com.transretail.crm.core.entity.embeddable.ProfessionalProfile;
import com.transretail.crm.core.entity.enums.MemberCreatedFromType;
import com.transretail.crm.core.entity.enums.MemberStatus;
import com.transretail.crm.core.entity.enums.RetrieveType;
import com.transretail.crm.core.entity.lookup.LookupDetail;
import com.transretail.crm.core.security.service.MD5Service;
import com.transretail.crm.core.service.*;
import com.transretail.crm.loyalty.dto.LoyaltyAnnualFeeSchemeDto;
import com.transretail.crm.loyalty.entity.LoyaltyCardInventory;
import com.transretail.crm.loyalty.service.LoyaltyAnnualFeeService;
import com.transretail.crm.loyalty.service.LoyaltyCardHistoryService;
import com.transretail.crm.loyalty.service.LoyaltyCardService;
import org.hibernate.exception.ConstraintViolationException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import java.text.ParseException;
import java.util.*;

@Controller
@RequestMapping("/member")
public class MemberRestController {

    private static final String REQUEST_MAPPING = "member";
    private static final Logger LOGGER = LoggerFactory.getLogger(MemberRestController.class);
    private static final String GENDER_FEMALE = "F";
    private static final String GENDER_MALE = "M";
    private static final String GENDER_NOT_SELECTED = "N";

    @Autowired
    private MemberService memberService;
    @Autowired
    private PromotionService promotionService;
    @Autowired
    private MessageSource messageSource;
    @Autowired
    private CodePropertiesService codePropertiesService;
    @Autowired
    private PointsTxnManagerService pointsManagerService;
    @Autowired
    private LookupService lookupService;
    @Autowired
    private MemberGroupService memberGroupService;
    @Autowired
    private PosTransactionService posTransactionService;
    @Autowired
    private LoyaltyCardService loyaltyCardService;
    @Autowired
    private LoyaltyCardHistoryService historyService;
    @Autowired
    private LoyaltyAnnualFeeService schemeService;
    @Autowired
    private MD5Service mD5Service;
    @Autowired
    private StoreMemberService storeMemberService;

    @Autowired
    private RestUrlService restUrlService;

    @RequestMapping("/renewloyalty/{cardNo}/{transactionNo}/{validationKey}")
    @ResponseBody
    @Transactional
    public ReturnMessage renewLoyalty(@PathVariable("cardNo") String cardNo, @PathVariable("transactionNo") String
            transactionNo, @PathVariable String validationKey, HttpServletRequest request) {
        ReturnMessage ret = new ReturnMessage();

        StringBuffer keySignature = new StringBuffer();
        keySignature.append(cardNo);
        keySignature.append(transactionNo);

        if (!mD5Service.validateMd5(keySignature.toString(), validationKey)) {
            LOGGER.info("[RENEWLOYALTY][INVALID ACCESS] cardNo : " + cardNo + " transactionNo: " + transactionNo + " " +
                    "validationKey : " + validationKey);
            ret.setMessageCode(codePropertiesService.getDetailMessageCodeInvalidClientSignature());
            ret.setType(MessageType.ERROR);
            ret.setMessage(messageSource.getMessage("points_rs_error_invalid_client_signature", null, null));
            return ret;
        }

        //call primary ws url if applicable
        if (restUrlService.redirectToPrimary(REQUEST_MAPPING,"/renewloyalty/" + cardNo + "/" + transactionNo + "/" + validationKey, false, ret, request.getLocalAddr())) {
            return ret;
        }

        if(restUrlService.getPrimaryWsUrlForStores().contains(request.getLocalAddr())) { //execute only when called from primary
        	LoyaltyCardInventory inventory = loyaltyCardService.activateLoyaltyCard(cardNo, null, null);
            LoyaltyAnnualFeeSchemeDto scheme = schemeService.getScheme(cardNo);
            historyService.saveHistoryForInventory(inventory,
                    inventory.getStatus(), inventory.getLocation(),
                    transactionNo, scheme.getAnnualFee(), null, false);
            ret.setType(MessageType.SUCCESS);
            ret.setMessage("card no. " + cardNo + " will expire on " + inventory.getExpiryDate());
        }

        return ret;
        
    }


    @RequestMapping("/validate/pin/{id}/{pin}")
    @ResponseBody
    @Transactional(readOnly = true)
    public ReturnMessage validatePin(@PathVariable String id, @PathVariable String pin, HttpServletRequest request) {
        ReturnMessage ret = new ReturnMessage();
        //call primary ws url if applicable
        if (restUrlService.callPrimaryWsUrl(REQUEST_MAPPING,"/validate/pin/" + id + "/" + pin, true, ret, request.getLocalAddr())) {
            return ret;
        }

        LOGGER.info("[VALIDATE_PIN] id =" + id + " ;pin" + pin + " ;current date=" + new Date());

        boolean valid = memberService.validatePin(id, pin);





        ret.setCallingAction(RetrieveType.VALIDATE_PIN.toString());

        if (valid) {
            ret.setType(MessageType.SUCCESS);


        } else {

            ret.setType(MessageType.ERROR);
            ret.setMessageCode(codePropertiesService.getDetailMessageCodeMemberNotFound());
            ret.setMessage(messageSource.getMessage("points_rs_error_invalid_pin", new Object[]{pin}, null));

        }


        return ret;

    }


    @RequestMapping("/find/professional/all")
    @ResponseBody
    @Transactional(readOnly = true)
    public List<ProfessionalRestDto> findAllProfessionals() {


        String profTypeCode = codePropertiesService.getDetailMemberTypeProfessional();

        LookupDetail type = lookupService.getDetail(profTypeCode);

        List<MemberModel> members = memberService.findAllByMemberType(type);
        List<ProfessionalRestDto> ret = new ArrayList<ProfessionalRestDto>();

        LOGGER.info("[FIND PROFESSIONAL] professional type code=" + profTypeCode + " ;current date=" + new Date());


        for (MemberModel m : members) {
            ProfessionalRestDto dto = new ProfessionalRestDto();

            dto.setName(m.getFormattedMemberName());
            dto.setCustomerId(m.getAccountId());
            dto.setStatus(m.getAccountStatus().name());
            dto.setStoreName(m.getStoreName());

            if (m.getProfessionalProfile() != null) {
                dto.setBusinessName(m.getProfessionalProfile().getBusinessName());

            }

            ret.add(dto);
        }


        return ret;
    }


    @RequestMapping("/find/professional/id/{id}")
    @ResponseBody
    @Transactional(readOnly = true)
    public ProfessionalRestDto findProfessionalsById(@PathVariable String id) {
        ProfessionalRestDto dto = new ProfessionalRestDto();

        MemberModel member = memberService.findByAccountId(id);

        LOGGER.info("[FIND PROFESSIONAL BY ID] id =" + id + "  ;current date=" + new Date());


        String profType = codePropertiesService.getDetailMemberTypeProfessional();

        //only look for professional type
        if (member == null || !profType.equalsIgnoreCase(member.getMemberType().getCode())) {
            dto.setMessageCode(codePropertiesService.getDetailMessageCodeMemberNotFound());
            dto.setMessage(messageSource.getMessage("points_rs_error_no_member", new Object[]{id}, null));

        } else {

            dto.setCustomerId(member.getAccountId());
            dto.setStatus(member.getAccountStatus().name());
            dto.setName(member.getFormattedMemberName());
            if (member.getProfessionalProfile() != null) {
                dto.setBusinessName(member.getProfessionalProfile().getBusinessName());

            }

        }


        return dto;
    }


    @RequestMapping("/find/id/{id}/{store}")
    @ResponseBody
    @Transactional(readOnly = true)
    public ReturnMessage findById(@PathVariable String id, @PathVariable String store, HttpServletRequest request) {

        ReturnMessage ret = new ReturnMessage();

        //call primary ws url if applicable
        if (restUrlService.callPrimaryWsUrl(REQUEST_MAPPING,"/find/id/" + id + "/" + store, true, ret, request.getLocalAddr())) {
            return ret;
        }
        StringBuffer msgCode = new StringBuffer();
        if (!storeMemberService.isValidMember(store, id, msgCode)) {
            ret.setType(MessageType.ERROR);
            ret.setMessageCode(msgCode.toString());
            ret.setMessage(messageSource.getMessage("points_rs_error_invalid_storemember", new Object[]{id}, null));
            return ret;
        }

        return findById(id, request);
    }


    @RequestMapping("/find/id/{id}")
    @ResponseBody
    @Transactional(readOnly = true)
    public ReturnMessage findById(@PathVariable String id, HttpServletRequest request) {
        ReturnMessage ret = new ReturnMessage();

        //call primary ws url if applicable
        if (restUrlService.callPrimaryWsUrl(REQUEST_MAPPING,"/find/id/" + id, true, ret, request.getLocalAddr())) {
            return ret;
        }


        //INFO id here is the account id
        String trimId = StringUtil.trimEmpId(id);

        LOGGER.info("[FIND MEMBER BY ID] id =" + id + " ;trimId=" + trimId + " ;current date=" + new Date());

        MemberModel member = memberService.findByAccountId(trimId);



        ret.setCallingAction(RetrieveType.RETRIEVE_BY_ID.toString());


        if (member == null) {
            ret.setType(MessageType.ERROR);
            ret.setMessageCode(codePropertiesService.getDetailMessageCodeMemberNotFound());
            ret.setMessage(messageSource.getMessage("points_rs_error_no_member", new Object[]{trimId}, null));
            return ret;
        }

        populateReturnMessage(ret, member);

        return ret;

    }


    //This is a duplicated method.


    @RequestMapping("/find/account/{id}/{store}")
    @ResponseBody
    @Transactional(readOnly = true)
    public ReturnMessage findByAccountId(@PathVariable String id, @PathVariable String store, HttpServletRequest request) {


        ReturnMessage ret = new ReturnMessage();
        //call primary ws url if applicable
        if (restUrlService.callPrimaryWsUrl(REQUEST_MAPPING,"/find/account/" + id + "/" + store, true, ret, request.getLocalAddr())) {
            return ret;
        }

        StringBuffer msgCode = new StringBuffer();
        if (!storeMemberService.isValidMember(store, id, msgCode)) {
            ret.setType(MessageType.ERROR);
            ret.setMessageCode(msgCode.toString());
            ret.setMessage(messageSource.getMessage("points_rs_error_invalid_storemember", new Object[]{id}, null));
            return ret;
        }

        return findById(id, request);
    }


    @RequestMapping("/find/account/{id}")
    @ResponseBody
    @Transactional(readOnly = true)
    public ReturnMessage findByAccountId(@PathVariable String id, HttpServletRequest request) {
        return findById(StringUtil.trimEmpId(id), request);
    }

    @RequestMapping("/find/email/{email:.+}/{store}")
    @ResponseBody
    @Transactional(readOnly = true)
    public ReturnMessage findByEmail(@PathVariable String email, @PathVariable String store, HttpServletRequest request) {
        ReturnMessage ret = new ReturnMessage();
        //call primary ws url if applicable
        if (restUrlService.callPrimaryWsUrl(REQUEST_MAPPING,"/find/email/" + email + "/" + store, true, ret, request.getLocalAddr())) {
            return ret;
        }

        LOGGER.info("[FIND MEMBER BY EMAIL] email =" + email + "current date=" + new Date());


        if (email == null) {
            ret.setType(MessageType.ERROR);
            ret.setMessageCode(codePropertiesService.getDetailMessageCodeMemberNotFound());
            ret.setMessage(messageSource.getMessage("points_rs_error_no_member", new Object[]{email}, null));
            return ret;
        }

        List<MemberModel> members = memberService.findByEmail(email);
        LOGGER.info("[FIND MEMBER BY EMAIL] email =" + email + " ;members" + members);



        ret.setCallingAction(RetrieveType.RETRIEVE_BY_EMAIL.toString());
        if (members == null || members.size() == 0) {
            ret.setType(MessageType.ERROR);
            ret.setMessageCode(codePropertiesService.getDetailMessageCodeMemberNotFound());
            ret.setMessage(messageSource.getMessage("points_rs_error_no_member", new Object[]{email}, null));
            return ret;
        }

        MemberModel member = members.get(0);

        StringBuffer msgCode = new StringBuffer();
        if (!storeMemberService.isValidMember(store, member.getAccountId(), msgCode)) {
            ret.setType(MessageType.ERROR);
            ret.setMessageCode(msgCode.toString());
            ret.setMessage(messageSource.getMessage("points_rs_error_invalid_storemember",
                    new Object[]{member.getAccountId()}, null));
            return ret;
        }


        populateReturnMessage(ret, member);

        return ret;

    }


    @Deprecated
    @RequestMapping("/find/email/{email:.+}")
    @ResponseBody
    @Transactional(readOnly = true)
    public ReturnMessage findByEmail(@PathVariable String email, HttpServletRequest request) {
        ReturnMessage ret = new ReturnMessage();
        //call primary ws url if applicable
        if (restUrlService.callPrimaryWsUrl(REQUEST_MAPPING,"/find/email/" + email, true, ret, request.getLocalAddr())) {
            return ret;
        }

        LOGGER.info("[FIND MEMBER BY EMAIL] email =" + email + "current date=" + new Date());


        if (email == null) {
            ret.setType(MessageType.ERROR);
            ret.setMessageCode(codePropertiesService.getDetailMessageCodeMemberNotFound());
            ret.setMessage(messageSource.getMessage("points_rs_error_no_member", new Object[]{email}, null));
            return ret;
        }

        List<MemberModel> members = memberService.findByEmail(email);

        LOGGER.info("[FIND MEMBER BY EMAIL] email =" + email + " ;members" + members);


        ret.setCallingAction(RetrieveType.RETRIEVE_BY_EMAIL.toString());
        if (members == null || members.size() == 0) {
            ret.setType(MessageType.ERROR);
            ret.setMessageCode(codePropertiesService.getDetailMessageCodeMemberNotFound());
            ret.setMessage(messageSource.getMessage("points_rs_error_no_member", new Object[]{email}, null));
            return ret;
        }


        populateReturnMessage(ret, members.get(0));

        return ret;

    }

    @RequestMapping("/find/contact/{contact}/{store}")
    @ResponseBody
    @Transactional(readOnly = true)
    public ReturnMessage findByContact(@PathVariable String contact, @PathVariable String store, HttpServletRequest request) {
        ReturnMessage ret = new ReturnMessage();
        //call primary ws url if applicable
        if (restUrlService.callPrimaryWsUrl(REQUEST_MAPPING,"/find/contact/" + contact + "/" + store, true, ret, request.getLocalAddr())) {
            return ret;
        }
        LOGGER.info("[FIND MEMBER BY CONTACT] contact =" + contact + "current date=" + new Date());


        MemberModel member = memberService.findByContact(contact);



        ret.setCallingAction(RetrieveType.RETRIEVE_BY_CONTACT.toString());
        if (member == null) {
            ret.setType(MessageType.ERROR);
            ret.setMessageCode(codePropertiesService.getDetailMessageCodeMemberNotFound());
            ret.setMessage(messageSource.getMessage("points_rs_error_no_member_withcontact", new Object[]{contact}, null));
            return ret;
        }

        StringBuffer msgCode = new StringBuffer();
        if (!storeMemberService.isValidMember(store, member.getAccountId(), msgCode)) {
            ret.setType(MessageType.ERROR);
            ret.setMessageCode(msgCode.toString());
            ret.setMessage(messageSource.getMessage("points_rs_error_invalid_storemember",
                    new Object[]{member.getAccountId()}, null));
            return ret;
        }


        populateReturnMessage(ret, member);

        return ret;

    }


    @RequestMapping("/find/contact/{contact}")
    @ResponseBody
    @Transactional(readOnly = true)
    public ReturnMessage findByContact(@PathVariable String contact, HttpServletRequest request) {
        ReturnMessage ret = new ReturnMessage();

        //call primary ws url if applicable
        if (restUrlService.callPrimaryWsUrl(REQUEST_MAPPING,"/find/contact/" + contact, true, ret, request.getLocalAddr())) {
            return ret;
        }
        LOGGER.info("[FIND MEMBER BY CONTACT] contact =" + contact + "current date=" + new Date());

        MemberModel member = memberService.findByContact(contact);



        ret.setCallingAction(RetrieveType.RETRIEVE_BY_CONTACT.toString());
        if (member == null) {
            ret.setType(MessageType.ERROR);
            ret.setMessageCode(codePropertiesService.getDetailMessageCodeMemberNotFound());
            ret.setMessage(messageSource.getMessage("points_rs_error_no_member_withcontact", new Object[]{contact}, null));
            return ret;
        }
        populateReturnMessage(ret, member);

        return ret;

    }

    @RequestMapping("/shoppinglist/{id}")
    @ResponseBody
    @Transactional(readOnly = true)
    public ReturnMessage getShoppingListSuggestion(@PathVariable("id") String accountId, HttpServletRequest request) {
        ReturnMessage ret = new ReturnMessage();
        //call primary ws url if applicable
        if (restUrlService.callPrimaryWsUrl(REQUEST_MAPPING,"/shoppinglist/" + accountId, true, ret, request.getLocalAddr())) {
            return ret;
        }


        MemberModel member = memberService.findByAccountId(accountId);
        if (member == null) {
            ret.setType(MessageType.ERROR);
            ret.setMessageCode(codePropertiesService.getDetailMessageCodeMemberNotFound());
            ret.setMessage(messageSource.getMessage("points_rs_error_no_member", new Object[]{accountId}, null));
            return ret;
        }

        populateReturnMessage(ret, member);

        Map<String, ShoppingListDto> shoppingList = posTransactionService.getShoppingSuggestions(accountId);
        Locale locale = LocaleContextHolder.getLocale();
        StringBuilder builder = new StringBuilder();
        String newLine = "; ";
        builder.append(messageSource.getMessage("shopping_list_label", null, null)).append(": ");

        for (String productId : shoppingList.keySet()) {
            ShoppingListDto dto = shoppingList.get(productId);
            Long average;
            if (1 > dto.getAverage() && dto.getAverage() > 0) {
                average = 1L;
            } else {
                average = Math.round(dto.getAverage());
            }
            builder.append(messageSource.getMessage("shopping_list_suggestion",
                    new Object[]{productId, dto.getProductName(), average}, null));
            builder.append(newLine);
        }
        ret.setMessage(builder.toString());
        return ret;
    }


    private void populateReturnMessage(ReturnMessage returnMessage, MemberModel member) {
        returnMessage.setType(MessageType.SUCCESS);

        returnMessage.setAccountNumber(String.valueOf(member.getAccountId()));
        returnMessage.setMemberName(member.getFormattedMemberName());
        returnMessage.setFirstName(member.getFirstName());
        returnMessage.setLastName(member.getLastName());
        MemberStatus status = member.getAccountStatus();
        if (status != null) {
            returnMessage.setStatus(status.toString());
        }

        Long points = member.getTotalPoints() != null ? member.getTotalPoints().longValue() : 0L;
        returnMessage.setTotalPoints(points);

//        Double theTotalPts = pointsManagerService.synchronizePointsTxn(member);
//        Long points =  theTotalPts != null ? theTotalPts.longValue() : 0L;
//        returnMessage.setTotalPoints(theTotalPts != null ? theTotalPts.longValue() : 0L);

        if (member.getMemberType() != null) {
            returnMessage.setMemberType(member.getMemberType().getCode());
            returnMessage.setMemberTypeDesc(member.getMemberType().getDescription());
        }

        if (member.getCardType() != null) {
            returnMessage.setCardType(member.getCardType().getCode());
            returnMessage.setCardTypeDesc(member.getCardType().getDescription());
            List<MemberGroupDto> groups = memberGroupService.getQualifiedCardTypeGroups(member);
            String[] cardGroups = new String[groups.size()];
            for (int i = 0; i < cardGroups.length; i++) {
                cardGroups[i] = groups.get(i).getCardTypeGroupCode();
            }
            returnMessage.setProfitCodes(cardGroups);
        }

        returnMessage.setBusinessName(member.getCompanyName());

        Npwp npwp = member.getNpwp();
        if (npwp != null) {
            returnMessage.setNpwpId(npwp.getNpwpId());
            returnMessage.setNpwpAddress(npwp.getNpwpAddress());
            returnMessage.setNpwpName(npwp.getNpwpName());
        }
        returnMessage.setKtpId(member.getKtpId());
        returnMessage.setContact(member.getContact());

        if (pointsManagerService.getSuspendedMemberTypes().contains(member.getMemberType())) {
            returnMessage.setCurrencyAmount(0D);
        } else {
            returnMessage.setCurrencyAmount(promotionService.convertPointToCurrency(points));
        }


        if (member.getLoyaltyCardNo() != null) {
            returnMessage.setLoyaltyCardNo(member.getLoyaltyCardNo());
            LoyaltyCardInventory loyaltyCard = loyaltyCardService.findLoyaltyCard(member.getLoyaltyCardNo());

            if (loyaltyCard != null && loyaltyCard.getExpiryDate() != null) {
                returnMessage.setLoyaltyCardExpiration(loyaltyCard.getExpiryDate().toString());

                returnMessage.setLoyaltyCardExpired(loyaltyCardService.isCardExpired(loyaltyCard));
            }

        }


    }


      /*
       MD5 Signature = MD5(id + storecode + transactionno + secretKey)

        where secret key is provided by MMS. default is carrefourmmskey

        gender value :
        male = M
        female = F
      */

    @RequestMapping("/register/{fname}/{bday}/{gender}/{city}/{contact}/{email}/{password}/{store}/{validationKey}")
    @ResponseBody
    @Transactional(readOnly = true)
    public ReturnMessage registerMember(@PathVariable String fname, @PathVariable String bday,
                                        @PathVariable String gender, @PathVariable String city,
                                        @PathVariable String contact, @PathVariable String email,
                                        @PathVariable String password, @PathVariable String store,
                                        @PathVariable String validationKey, HttpServletRequest request) {
        ReturnMessage ret = new ReturnMessage();

        LOGGER.info("[REGISTER MEMBER][INIT] fname : {}  bday: {}  gender: {} city: {}  contact: {}  email: {} " +
                "password: {}  store: {} validationkey: {}", fname, bday, gender, city, contact, email, password,
                store, validationKey);

        StringBuffer keySignature = new StringBuffer();
        keySignature.append(fname);
        keySignature.append(bday);
        keySignature.append(gender);
        keySignature.append(city);
        keySignature.append(contact);
        keySignature.append(email);
        keySignature.append(password);
        keySignature.append(store);

        if (!mD5Service.validateMd5(keySignature.toString(), validationKey)) {
            LOGGER.info("[REGISTER MEMBER][INVALID ACCESS] fname : {}  bday: {}  gender: {} city: {}  contact: {}  email: {} " +
                    "password: {}  store: {} validationkey: {}", fname, bday, gender, city, contact, email, password,
                    store, validationKey);

            ret.setMessageCode(codePropertiesService.getDetailMessageCodeInvalidClientSignature());
            ret.setType(MessageType.ERROR);
            ret.setMessage(messageSource.getMessage("points_rs_error_invalid_client_signature", null, null));
            return ret;
        }

        Date birthDate = null;
        try {

            birthDate = DateUtil.convertDateString(DateUtil.REST_BIRTH_DATE_FORMAT, bday);
        } catch (ParseException e) {

            LOGGER.error("Error Parsing date parameter {} for customer  {} :: {}  ", bday,
                    fname, e.getMessage());
        }


        if (memberService.contactExists(contact)) {

            MemberModel updateModel = memberService.findByContact(contact);
            updateModel.setStampUser(true);
            memberService.updateCustomerModel(updateModel);
            ret.setType(MessageType.ERROR);
            ret.setMessageCode(codePropertiesService.getDetailMessageCodeContactNumberAlreadyExist());
            ret.setMessage(messageSource.getMessage("points_rs_error_contact_already_exist", new Object[]{contact, fname}, null));

            LOGGER.error("Contact number {} already exists for customer  {}   ", contact, fname);
            return ret;

        }

        MemberModel memberModel = new MemberModel();
        memberModel.setFirstName(fname);
        memberModel.setEmail(email);
        memberModel.setPassword(password);
        memberModel.setRegisteredStore(store);
        CustomerProfile profile = new CustomerProfile();
        LookupDetail genderLookup =  new LookupDetail(codePropertiesService.getDetailGenderNotSelected());
        if (gender.toUpperCase().startsWith(GENDER_FEMALE)) {
            genderLookup =  new LookupDetail(codePropertiesService.getDetailGenderFemale());
        } else if (gender.toUpperCase().startsWith(GENDER_MALE)) {
            genderLookup =  new LookupDetail(codePropertiesService.getDetailGenderMale());
        }


        profile.setGender(genderLookup);
        profile.setBirthdate(birthDate);
        memberModel.setCustomerProfile(profile);
        ProfessionalProfile professionalProfile = new ProfessionalProfile();
        memberModel.setProfessionalProfile(professionalProfile);

        Address address = new Address();
        address.setCity(city);
        memberModel.setAddress(address);
        memberModel.setContact(contact);
        memberModel.setMemberType(new LookupDetail(codePropertiesService.getDetailMemberTypeIndividual()));
        memberModel.setEnabled(true);
        memberModel.setMemberCreatedFromType(MemberCreatedFromType.FROM_STAMP);
        memberModel.setStampUser(true);

        MemberDto memberDto = new MemberDto(memberModel);


        memberService.saveCustomerModel(memberDto);




        ret.setType(MessageType.SUCCESS);
        ret.setFirstName(fname);
        ret.setMemberName(fname);
        return ret;
    }


}
