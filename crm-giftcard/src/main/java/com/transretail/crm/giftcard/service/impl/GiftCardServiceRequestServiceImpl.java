package com.transretail.crm.giftcard.service.impl;

import com.google.common.base.Function;
import com.google.common.base.Objects;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Iterables;
import com.google.common.collect.Lists;
import com.mysema.query.BooleanBuilder;
import com.mysema.query.jpa.impl.JPAQuery;
import com.mysema.query.types.Order;
import com.mysema.query.types.OrderSpecifier;
import com.mysema.query.types.Projections;
import com.transretail.crm.common.reporting.jasper.ClasspathFileResolver;
import com.transretail.crm.common.reporting.jasper.DefaultJRProcessor;
import com.transretail.crm.common.reporting.jasper.JRProcessor;
import com.transretail.crm.common.service.dto.request.PageSortDto;
import com.transretail.crm.common.service.dto.request.PagingParam;
import com.transretail.crm.common.service.dto.response.ResultList;
import com.transretail.crm.common.service.exception.GenericServiceException;
import com.transretail.crm.common.service.util.SpringDataPagingUtil;
import com.transretail.crm.core.entity.lookup.Store;
import com.transretail.crm.core.security.util.UserUtil;
import com.transretail.crm.core.service.StoreService;
import com.transretail.crm.giftcard.GiftCardNotFoundException;
import com.transretail.crm.giftcard.dto.GiftCardInfoDto;
import com.transretail.crm.giftcard.dto.GiftCardServiceRequestDto;
import com.transretail.crm.giftcard.dto.GiftCardTransactionDto;
import com.transretail.crm.giftcard.dto.ServiceRequestReportFilter;
import com.transretail.crm.giftcard.entity.*;
import com.transretail.crm.giftcard.entity.support.GiftCardInventoryStatus;
import com.transretail.crm.giftcard.entity.support.GiftCardSaleTransaction;
import com.transretail.crm.giftcard.entity.support.GiftCardSalesType;
import com.transretail.crm.giftcard.repo.GiftCardServiceRequestRepo;
import com.transretail.crm.giftcard.repo.GiftCardTransactionRepo;
import com.transretail.crm.giftcard.repo.SalesOrderRepo;
import com.transretail.crm.giftcard.service.GiftCardInventoryService;
import com.transretail.crm.giftcard.service.GiftCardServiceRequestService;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import javax.annotation.Nonnull;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.apache.commons.lang3.StringUtils;
import org.joda.time.DateTime;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Primary;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

/**
 *
 * @author Monte Cillo Co (mco@exist.com)
 * @since 2.0
 */
@Service
@Primary
public class GiftCardServiceRequestServiceImpl implements GiftCardServiceRequestService {

    static final String REPORT_NAME = "reports/giftcard/gc-service-request-report.jasper";
    @Autowired
    private GiftCardServiceRequestRepo giftCardServiceRequestRepo;
    @Autowired
    private GiftCardInventoryService inventoryService;
    @Autowired
    private GiftCardTransactionRepo transactionRepo;
    @Autowired
    private StoreService storeService;
    @Autowired
    private SalesOrderRepo salesOrderRepo;
    @PersistenceContext
    private EntityManager em;
    private static final Map<String, String> VIEW_MODEL_PROP_MAPPING = new ImmutableMap.Builder<String, String>()
            .put("createdBy", "createUser")
            .put("createdDateTime", "created")
            .put("store", "merchantId")
            .put("transactionTime", "transactionDate").build();

    @Transactional
    @Override
    public GiftCardServiceRequest fileNewServiceRequest(@Nonnull GiftCardInventory giftCard, @Nonnull Store storeFiledIn, String details) {
        Assert.notNull(giftCard, "GiftCardInventory argument is required; it must not be null");
        Assert.isTrue(StringUtils.isNotBlank(details), "Details argument is required; it must not be null or empty");

        GiftCardServiceRequest request = new GiftCardServiceRequest()
                .filedBy(UserUtil.getCurrentUser().getUsername())
                .filedIn(storeFiledIn)
                .details(details)
                .dateFiled(DateTime.now())
                .giftCard(giftCard);

        return giftCardServiceRequestRepo.save(request);
    }

    @Override
    public List<GiftCardServiceRequest> findAllServiceRequestOf(GiftCardInventory giftcard) {
        Assert.notNull(giftcard, "GiftCardInventory argument is required; it must not be null");
        return giftCardServiceRequestRepo.findAllServiceRequestOf(giftcard);
    }

    @Override
    public ResultList<GiftCardServiceRequestDto> findAllServiceRequestOf(String cardNo, PageSortDto pageSort) {
        Pageable pageable = SpringDataPagingUtil.INSTANCE.createPageable(pageSort.getPagination());
        QGiftCardServiceRequest qgc = QGiftCardServiceRequest.giftCardServiceRequest;
        Page<GiftCardServiceRequest> page = giftCardServiceRequestRepo.findAll(qgc.giftcard.barcode.eq(cardNo), pageable);
        return new ResultList<GiftCardServiceRequestDto>(convertToDto(page),
                page.getTotalElements(), page.hasPreviousPage(), page.hasNextPage());
    }

    private List<GiftCardServiceRequestDto> convertToDto(Page<GiftCardServiceRequest> page) {
        Iterable<GiftCardServiceRequestDto> list = Iterables.transform(page, new Function<GiftCardServiceRequest, GiftCardServiceRequestDto>() {

            @Override
            public GiftCardServiceRequestDto apply(GiftCardServiceRequest input) {
                GiftCardServiceRequestDto giftCardServiceRequestDto = new GiftCardServiceRequestDto();
                BeanUtils.copyProperties(input, giftCardServiceRequestDto, "giftcard");
                giftCardServiceRequestDto.setGiftcardNo(input.getGiftcard().getBarcode());
                giftCardServiceRequestDto.setFiledIn(input.getFiledIn().getCode());
                giftCardServiceRequestDto.setFiledInPresenter(input.getFiledIn().getCodeAndName());

                return giftCardServiceRequestDto;
            }
        });

        return Lists.newArrayList(list);
    }

    @Override
    public JRProcessor printServiceRequest(ServiceRequestReportFilter filter) {
        QGiftCardServiceRequest qgc = QGiftCardServiceRequest.giftCardServiceRequest;
        DateTime from = filter.getDateFiled().toDateTime(filter.getTimeFrom());
        DateTime to = filter.getDateFiled().toDateTime(filter.getTimeTo());
        BooleanBuilder booleanBuilder = new BooleanBuilder(qgc.dateFiled.between(from, to));
        if (StringUtils.isNotBlank(filter.getFiledBy())) {
            booleanBuilder.and(qgc.filedBy.eq(filter.getFiledBy()));
        }

        Iterable<GiftCardServiceRequest> results = giftCardServiceRequestRepo.findAll(booleanBuilder.getValue(),
                new OrderSpecifier<DateTime>(Order.ASC, qgc.dateFiled));
        List<ReportBean> datasource = Lists.newArrayList(Iterables.transform(results, new Function<GiftCardServiceRequest, ReportBean>() {

            @Override
            public ReportBean apply(GiftCardServiceRequest request) {
                return new ReportBean()
                        .cardNo(request.getGiftcard().getBarcode())
                        .details(request.getDetails())
                        .filedOn(request.getDateFiled())
                        .filedBy(request.getFiledBy())
                        .filedIn(request.getFiledIn().getCodeAndName());
            }
        }));

        if (!datasource.isEmpty()) {
            datasource.add(0, new ReportBean());
        }

        DefaultJRProcessor processor = new DefaultJRProcessor(REPORT_NAME, datasource);
        processor.addParameter("TIME_RANGE", String.format("%s (%s - %s)", filter.getDateFiled().toString("MMMM dd, yyyy"),
                filter.getTimeFrom().toString("hh:mm a"), filter.getTimeTo().toString("hh:mm a")));
        processor.setFileResolver(new ClasspathFileResolver("reports"));

        return processor;
    }

    @Override
    @Transactional
    public GiftCardInventoryStatus enabledDisableGiftCard(String cardNo) {
        GiftCardInventory giftcard = inventoryService.getGiftCardByBarcode(cardNo);
        if (giftcard.getStatus() != GiftCardInventoryStatus.ACTIVATED
                && giftcard.getStatus() != GiftCardInventoryStatus.DISABLED) {
            throw new GenericServiceException("Attempting to enable/disable a gift card when neither of the status is valid.");
        }

        if (giftcard.getStatus() == GiftCardInventoryStatus.ACTIVATED) {
            giftcard.setStatus(GiftCardInventoryStatus.DISABLED);
        } else if (giftcard.getStatus() == GiftCardInventoryStatus.DISABLED) {
            giftcard.setStatus(GiftCardInventoryStatus.ACTIVATED);
        }

        inventoryService.save(giftcard);
        return giftcard.getStatus();
    }

    @Transactional(readOnly = true)
    @Override
    public ResultList<GiftCardTransactionDto> findAllTransactionOf(String cardNo, PageSortDto pageSort) {
        final QGiftCardTransactionItem qgcti = QGiftCardTransactionItem.giftCardTransactionItem;
        final QGiftCardTransaction qgct = QGiftCardTransaction.giftCardTransaction;
        final QGiftCardInventory qgci = QGiftCardInventory.giftCardInventory;

        JPAQuery jpqQuery = new JPAQuery(em).from(qgct)
                .innerJoin(qgct.transactionItems, qgcti)
                .innerJoin(qgcti.giftCard, qgci)
                .where(qgci.barcode.eq(cardNo)
                        .and(qgct.transactionType
                                .notIn(GiftCardSaleTransaction.PRE_ACTIVATION,
                                        GiftCardSaleTransaction.PRE_REDEMPTION)));

        final PagingParam pagination = pageSort.getPagination();
        Map<String, Boolean> order = pagination.getOrder();

        // sorting from ui to model convertion
        for (Entry<String, String> entry : VIEW_MODEL_PROP_MAPPING.entrySet()) {
            final String viewProperty = entry.getKey();
            if (order.containsKey(viewProperty)) {
                Boolean ordering = order.get(viewProperty);
                order.remove(viewProperty);
                order.put(entry.getValue(), ordering);
            }
        }

        SpringDataPagingUtil.INSTANCE.applyPagination(jpqQuery, pageSort.getPagination(), GiftCardTransaction.class);
        List<GiftCardTransactionDto> list = jpqQuery.list(Projections.bean(GiftCardTransactionDto.class,
                qgct.created.as("createdDateTime"), qgct.createUser.as("createdBy"), qgct.cashierId.as("cashierId"),
                qgcti.currentAmount.as("previousBalance"), qgcti.transactionAmount.as("transactionAmount"),
                qgcti.currentAmount.add(qgcti.transactionAmount).as("balance"),
                qgct.merchantId.as("store"), qgct.terminalId.as("terminalId"),
                qgct.transactionDate.as("transactionTime"), qgct.transactionNo.as("transactionNo"),
                qgct.transactionType.stringValue().as("transactionType")));

        return new ResultList<GiftCardTransactionDto>(list, list.size(), pagination.getPageNo(), pagination.getPageSize());
    }

    @Transactional(readOnly = true)
    @Override
    public GiftCardInfoDto retrieveGiftCardInfoByCardNo(String cardNo) {
        GiftCardInventory result = inventoryService.getGiftCardByBarcode(cardNo);

        if (result == null) {
            throw new GiftCardNotFoundException(cardNo);
        }

        SalesOrder salesOrder = salesOrderRepo.findEnclosingSalesOrderOfGiftCardInventory(result);
        String orderNo = salesOrder == null ? "" : salesOrder.getOrderNo();
        String customerId = salesOrder == null ? "" : salesOrder.getCustomer().getName() + " - " + salesOrder.getCustomer().getCustomerId();
        GiftCardTransaction cardTransaction = null;

        if (GiftCardSalesType.B2C == result.getSalesType()) {
            cardTransaction = transactionRepo.findEnclosingTransactionOfGiftCard(
                    result, GiftCardSaleTransaction.ACTIVATION);
        } else {
            cardTransaction = transactionRepo.findEnclosingTransactionViaSalesOrder(result);
        }

        String saleStore = cardTransaction == null ? ""
                : getStorePresentation(cardTransaction.getMerchantId());
        String saleTerminalId = cardTransaction == null ? "" : cardTransaction.getTerminalId();

        return new GiftCardInfoDto()
                .activateUser(cardTransaction == null ? "" : cardTransaction.getCreateUser())
                .activationTime(cardTransaction == null ? null : cardTransaction.getTransactionDate())
                .allocatedTo(getStorePresentation(result.getAllocateTo()))
                .allowPartialRedeem(result.getProfile().isAllowPartialRedeem())
                .balance(result.getBalance())
                .previousBalance(Objects.firstNonNull(result.getPreviousBalance(), 0.0)) // support for older record
                .cardNo(result.getBarcode())
                .createdBy(result.getCreateUser())
                .createdDateTime(result.getCreated().toLocalDateTime())
                .customerId(customerId)
                .expiryDate(result.getExpiryDate())
                .faceValue(result.getFaceValue().doubleValue())
                .inventoryLocation(getStorePresentation(result.getLocation()))
                .moNo(result.getOrder().getMoNumber())
                .orderNo(orderNo)
                .product(result.getProfile().getCodeAndName())
                .reloadable(result.getProfile().isAllowReload() == null ? false : result.getProfile().isAllowReload())
                .saleStore(saleStore)
                .status(result.getStatus().name())
                .terminalId(saleTerminalId)
                .unitCost(result.getProfile().getUnitCost());
    }

    private String getStorePresentation(String storeCode) {
        Store store = storeService.getStoreByCode(storeCode);
        return store == null ? "" : store.getCodeAndName();
    }

    //<editor-fold defaultstate="collapsed" desc="ReportBean Body">
    public static class ReportBean {

        private String cardNo;
        private String filedOn;
        private String filedBy;
        private String filedIn;
        private String details;

        public String getCardNo() {
            return cardNo;
        }

        public String getFiledOn() {
            return filedOn;
        }

        public String getFiledBy() {
            return filedBy;
        }

        public String getDetails() {
            return details;
        }

        public String getFiledIn() {
            return filedIn;
        }

        public ReportBean filedOn(DateTime filedDate) {
            this.filedOn = filedDate.toString("hh:mm:ss a").trim();
            return this;
        }

        public ReportBean filedBy(String filedBy) {
            this.filedBy = filedBy;
            return this;
        }

        public ReportBean filedIn(String filedIn) {
            this.filedIn = filedIn;
            return this;
        }

        public ReportBean details(String details) {
            this.details = details;
            return this;
        }

        public ReportBean cardNo(String cardNo) {
            this.cardNo = cardNo;
            return this;
        }
    }
    //</editor-fold>
}
