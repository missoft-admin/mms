package com.transretail.crm.web.controller;

import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.reset;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.springframework.http.MediaType.APPLICATION_JSON;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.view;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.redirectedUrl;

import java.util.ArrayList;
import java.util.List;

import javax.swing.text.View;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.internal.util.collections.Sets;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.AnnotationConfigWebContextLoader;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.transretail.crm.common.service.dto.response.ResultList;
import com.transretail.crm.core.dto.StoreMemberDto;
import com.transretail.crm.core.dto.StoreMemberSearchDto;
import com.transretail.crm.core.entity.lookup.Store;
import com.transretail.crm.core.service.CodePropertiesService;
import com.transretail.crm.core.service.LookupService;
import com.transretail.crm.core.service.StoreMemberService;
import com.transretail.crm.core.service.StoreService;
import com.transretail.crm.web.controller.util.StoreMemberSchemeValidator;

/**
 * @author ftopico
 */
@RunWith(SpringJUnit4ClassRunner.class)
@WebAppConfiguration
@ContextConfiguration(loader = AnnotationConfigWebContextLoader.class)
public class StoreMemberSchemeControllerTest extends AbstractMockMvcTest {

    @Configuration
    @EnableWebMvc
    static class Config {
        @Bean
        public StoreMemberSchemeController storeMemberSchemeController() {
            return new StoreMemberSchemeController();
        }
        
        @Bean
        public StoreMemberService storeMemberService() {
            return mock(StoreMemberService.class);
        }
        
        @Bean
        public StoreService storeService() {
            return mock(StoreService.class);
        }
        
        @Bean
        public LookupService lookupService() {
            return mock(LookupService.class);
        }
        
        @Bean CodePropertiesService codePropertiesService() {
            return mock(CodePropertiesService.class);
        }
        
        @Bean StoreMemberSchemeValidator storeMemberSchemeValidator() {
            return mock(StoreMemberSchemeValidator.class);
        }
        
        @Bean MessageSource messageSource() {
            return mock(MessageSource.class);
        }
    }
    
    @Autowired
    private StoreMemberService storeMemberService;
    @Autowired
    StoreService storeService;
    @Autowired
    LookupService lookupService;
    @Autowired
    CodePropertiesService codePropertiesService;
    @Autowired
    StoreMemberSchemeValidator storeMemberSchemeValidator;
    @Autowired
    private MessageSource messageSource;
    
    @Override
    protected void doSetup() throws Exception {
        reset(storeMemberService);
        reset(storeService);
        reset(lookupService);
        reset(codePropertiesService);
        reset(storeMemberSchemeValidator);
        reset(messageSource);
    }
    
    @Test
    public void listStoreMemberSchemeTest() throws Exception {
        StoreMemberSearchDto searchDto = new StoreMemberSearchDto();
        ResultList<StoreMemberDto> storeMembers = new ResultList<StoreMemberDto>(new ArrayList<StoreMemberDto>());
        when(storeMemberService.searchStoreMembers(any(StoreMemberSearchDto.class))).thenReturn(storeMembers);
        
        ObjectMapper mapper = new ObjectMapper();
        mockMvc.perform(post("/storemember/list").content(mapper.writeValueAsBytes(new StoreMemberSearchDto())).contentType(APPLICATION_JSON))
            .andExpect(content().bytes(mapper.writeValueAsBytes(storeMembers)))
            .andExpect(status().isOk());
        verify(storeMemberService).searchStoreMembers(any(StoreMemberSearchDto.class));
    }

}
