

$(document).ready(function() {
	var CONTAINER_ERROR = "#contentError > div";
	var CONTENT_ERROR = "#contentError";
	var ADJUSTMENT_FORM_DIALOG = "#adjustmentFormDialog";
	var APPROVER_FORM_DIALOG = "#approverFormDialog";
	var ADJUSTMENT_FORM_ID = "employeePurchaseTxn";
	var APPROVER_FORM_ID = "approver";
	
	var $theSearchForm = $( "#form_SearchForm" );
  var $theSearchBtn = $( "#searchButton" );
	$list = $("#list_txn");
	
	initDialogs();
	initButtons();
	
	initDataTable();
	initFormFields();
	
	$("#employeepurchasetxnad_transactionno").change(showTxnDetails);
	
	function initFormFields() {

    $( "#searchField" ).change( function(e) {
      e.preventDefault();
      $( "#searchValue" ).attr( "name", $( this ).val() );
    }).change();
    
    $("#transactionDate").datepicker({
      autoclose : true,
      format : "dd-mm-yyyy",
    }).on('changeDate', function(selected){
      transactionDate = new Date(selected.date.valueOf());
      transactionDate.setDate(transactionDate.getDate(new Date(selected.date.valueOf())));
    });
  } 
	
	function showTxnDetails() {
		$.get($(this).data("url") + $(this).val(), function(data) {
			if(data.success) {
				var result = data.result;
				var transactionDate = "";
				$payments = $("#payments");
				var str = "";
				for(var i=0; i < result.length; i++) {
					transactionDate = result[i].transactionDateTime;
					if(str != "")
						str += "<br/>";
					str += "<b>" + result[i].paymentType + "</b> - <b>" + result[i].transactionAmount + "</b>";
				}	
				$("#adjTransactionDate").text(transactionDate);
				$("#payments").html(str);
				$("#detailsContainer").show();
			} else {
				$("#detailsContainer").hide();
			}
			
		}, "json");
	}

	function initDataTable() {
		  
	  var headers = { 
	    employee: $list.data("hdrEmployee"),
	    location: $list.data("hdrLocation"),
	    date: $list.data("hdrDate"),
	    txnNo: $list.data("hdrTxnNo"),
	    txnAmount: $list.data("hdrTxnAmount"),
	    status: $list.data("hdrStatus"),
	    requestedBy: $list.data("hdrRequestedBy"),
	    requestDate: $list.data("hdrRequestDate"),
	    approvedBy: $list.data("hdrApprovedBy"),
	    approvalDate: $list.data("hdrApprovalDate")
	  };
	  
	  $list.ajaxDataTable({
	    'autoload'  : true,
	    'ajaxSource' : $list.data("url"),
	    'aaSorting'		: [[ 1, "asc" ]],
	    'columnHeaders' : [
	         '',
	         headers['employee'],
	         headers['location'],
	         headers['date'],
	         headers['txnNo'],
	         headers['txnAmount'],
	         headers['status'],
	         headers['requestedBy'],
	         headers['requestDate'],
	         headers['approvedBy'],
	         headers['approvalDate'],
	    ],
	    'modelFields' : [
		{customCell : function ( innerData, sSpecific, json ) {
		    if ( sSpecific == 'display' && json.status == "FORAPPROVAL") {
		    	var btnHtml = $("#btn").html();
		        var rg = new RegExp("%ITEMID%", 'g');
				    btnHtml = btnHtml.replace(rg, json.id);
		        return btnHtml;
		      } else {
		        return '';
		      }
		      }
		 },
	     {name : 'memberModel.accountId', sortable : true, fieldCellRenderer : function (data, type, row) {
	           return row.memberModel.accountId + " - " + row.memberModel.formattedMemberName;
         }},
         {name : 'store', sortable : true, fieldCellRenderer : function (data, type, row) {
	           return row.storeModel.code + " - " + row.storeModel.name;
         }},
	     {name : 'transactionDateTime', sortable : true, fieldCellRenderer : function (data, type, row) {
	           return ( (row.longTxnDate)? new Date( row.longTxnDate ).customize(0) : "" );
	        }},
	     {name : 'transactionNo', sortable : true},
	     {name : 'transactionAmount', sortable : true, fieldCellRenderer : function (data, type, row) {
	           return row.transactionAmountFormatted;
	     }},
	     {name : 'status', sortable : true, fieldCellRenderer : function (data, type, row) {
             return (row.status == 'FORAPPROVAL')? "FOR APPROVAL" : row.status ;
         }},
	     {name : 'createUser', sortable : true},
	     {name : 'created', sortable : true},
	     {name : 'approvedBy', sortable : true},
	     {name : 'approvalDate', sortable : true}
	     ]
	  }).on("click", ".adjLnk", function(e) {
			e.preventDefault();
			adjustPurchase($(this).data("itemId"), $(this).data("url"), $(this).data("queryStr"));
		});
	  
	  initSearchForm($list);
	}
	
	function initDialogs() {
		$(".modal").modal({
			show: false
		});
		
		$(".floatInput").numberInput({
            "type" : "float"
        });
		$(".intInput").numberInput({
            "type" : "int"
        });
		$(".signedInput").numberInput({
            "type" : "signed"
        });
	}
	
	function initButtons() {
		$(".adjLnk").click(function(e) {
			e.preventDefault();
			adjustPurchase($(this).data("itemId"), $(this).data("url"), $(this).data("queryStr"));
		});
		
		$("#addApproverLnk").click(function(e) {
			e.preventDefault();
			addApprover();
		});
	}

	function initDialogButtons(id, inAjaxController, inQueryString, methodType) {
		var theParentForm = $("form[id='" + ADJUSTMENT_FORM_ID + "']");
		$(ADJUSTMENT_FORM_DIALOG).find("#button_save").unbind("click").click( function(e) {
		    e.preventDefault();
		    $( "#button_save" ).prop( "disabled", true );
			$.ajax({
				type 	: methodType,
				url 	: $(theParentForm).attr("action") + inAjaxController,
				dataType: 'json',
				data 	: $(theParentForm).serialize(),
				        
				success : function(inResponse) {
					processResults(inResponse, theParentForm, inQueryString);
				    $( "#button_save" ).prop( "disabled", false );
				},
				error : function(jqXHR, textStatus, errorThrown) {
				    $( "#button_save" ).prop( "disabled", false );
					$( CONTENT_ERROR ).html("Error: " + errorThrown);
				    $( "#button_save" ).prop( "disabled", false );
				}
			});
		});

		$(ADJUSTMENT_FORM_DIALOG).find("#button_cancel").unbind("click").click( function(e) {
			e.preventDefault();
	    	$(theParentForm).trigger( "reset" );
	    	$(".clearOnReset").html("").text("");
	    	$("#detailsContainer").hide();
	    	$(ADJUSTMENT_FORM_DIALOG).modal("hide");
	    	$(ADJUSTMENT_FORM_DIALOG).find( CONTAINER_ERROR ).html("");
	    	$(ADJUSTMENT_FORM_DIALOG).find( CONTENT_ERROR ).hide();
	    });
		
		$(ADJUSTMENT_FORM_DIALOG).find("#button_approve").unbind("click").click( function(e) {
			e.preventDefault();
		    $( "#button_approve" ).prop( "disabled", true );
			$.ajax({
				type 	: methodType,
				url 	: $(theParentForm).attr("action") + "/override/approve/" + id,
				dataType: 'json',
				data 	: $(theParentForm).serialize(),
				        
				success : function(inResponse) {
					processResults(inResponse, theParentForm, inQueryString);
				    $( "#button_approve" ).prop( "disabled", false );
				},
				error : function(jqXHR, textStatus, errorThrown) {
					$( CONTENT_ERROR ).html("Error: " + errorThrown);
				    $( "#button_approve" ).prop( "disabled", false );
				}
			});
	    });
		
		$(ADJUSTMENT_FORM_DIALOG).find("#button_reject").unbind("click").click( function(e) {
			e.preventDefault();
		    $( "#button_reject" ).prop( "disabled", true );
			$.ajax({
				type 	: methodType,
				url 	: $(theParentForm).attr("action") + "/override/reject/" + id,
				dataType: 'json',
				data 	: $(theParentForm).serialize(),
				        
				success : function(inResponse) {
					processResults(inResponse, theParentForm, inQueryString);
				    $( "#button_reject" ).prop( "disabled", false );
				},
				error : function(jqXHR, textStatus, errorThrown) {
					$( CONTENT_ERROR ).html("Error: " + errorThrown);
				    $( "#button_reject" ).prop( "disabled", false );
				}
			});
	    });
	}

	function processResults(inResponse, theParentForm, inQueryString) {
		if (inResponse.success) {
			$(ADJUSTMENT_FORM_DIALOG).find( CONTENT_ERROR ).hide('fast');
			$(theParentForm).unbind("submit").submit( function() {
				$(this).attr("action", $(this).attr("action") + "?" + inQueryString);
			});
			$(theParentForm).submit();
		} 
		else {
			errorInfo = "";
			for (i = 0; i < inResponse.result.length; i++) {
				errorInfo += "<br>" + (i + 1) + ". "
						+ ( inResponse.result[i].code != undefined ? 
								inResponse.result[i].code : inResponse.result[i]);
			}
			$(ADJUSTMENT_FORM_DIALOG).find( CONTAINER_ERROR ).html("Please correct following errors: " + errorInfo);
			$(ADJUSTMENT_FORM_DIALOG).find( CONTENT_ERROR ).show('slow');
		}
	}

	function initFormDialogButtons(id, queryString) {
		var controllerAction = "";
		var methodType = "POST";
		if(id == null || id == "") {
			controllerAction = "/create";
		} else {
			controllerAction = "/update/" + id;
		}
			
		initDialogButtons(id, controllerAction, queryString, methodType);
	}

	function populateForm(id) {
		var CONTROLLER_POPULATE = "/populate/";
		if(id == null || id == "") {
			CONTROLLER_POPULATE += null;
		} else {
			CONTROLLER_POPULATE += id;
		}
		populate( CONTROLLER_POPULATE );
	}

	function populate( inPopulateUrl ) {
	    var theParentForm = $("form[id='" + ADJUSTMENT_FORM_ID + "']");
	    
		$.ajax({
			type 	: "POST",
			url 	: $(theParentForm).attr("action") + inPopulateUrl,
			dataType: 'json',
			        	
			success : function(inResponse) {
				var inReturnEntity = inResponse.result;
				
				$.each(inReturnEntity, function(key, value) {
					
					var theInput = $("input[name='" + key + "']");
					if ( theInput != null ) {
						if ( $(theInput).is(':radio') ) {
							$("input:radio[name='" + key + "']").each( function() {
								$(this).attr( 'checked', $(this).val() == value );
								if($(this).val() == value) {
									$(this).click();
								}
							});
						}
						else {
							$(theInput).val( value );
						}
					}
					
					var theTextArea = $("textarea[name='" + key + "']");
					if ( theTextArea != null ) {
						$(theTextArea).val( value );
					}
				});
				
				$("#employeepurchasetxnad_transactionno").each(showTxnDetails);
			},
			error : function(jqXHR, textStatus, errorThrown) {
				$( CONTAINER_ERROR ).html("Error: " + errorThrown);
			}
		});
	}

	function adjustPurchase(id, url, queryString) {
		$(ADJUSTMENT_FORM_DIALOG).modal("show");
		populateForm(id);
		initFormDialogButtons(id, queryString);
		setApproverFields();
	}

	function addApprover() {
		var theParentForm = $("form[id='" + APPROVER_FORM_ID + "']");
		$(APPROVER_FORM_DIALOG).modal("show");
		
		$(APPROVER_FORM_DIALOG).find("#button_cancel").unbind("click").click( function(e) {
			e.preventDefault();
			$(theParentForm).trigger( "reset" );
	    	$(APPROVER_FORM_DIALOG).modal("hide");
	    });
	}

	function setApproverFields() {
		if(IS_APPROVER) {
			$("#employeepurchasetxnad_employeeid").attr("readonly", "readonly");
			$("#employeepurchasetxnad_transactionno").attr("readonly", "readonly");
			$("#employeepurchasetxnad_transactionamount").attr("readonly", "readonly");
			$("#employeepurchasetxnad_reason").attr("readonly", "readonly");
		}
	}
	
	function initSearchForm( inDataTable ) {
    $theSearchBtn.click( function() {
      var $theFormDataObj = $theSearchForm.toObject( { mode : 'first', skipEmpty : true } );
      var $btnSearch = $( this );
      $btnSearch.prop( 'disabled', true );
      inDataTable.ajaxDataTable( 
        'search', 
        $theFormDataObj, 
        function () {
          $btnSearch.prop( 'disabled', false );
      });
    });
    
    $("#clearButton").click(function(e) {
      e.preventDefault();
      $("#searchField option:first").attr('selected','selected');
      $("#searchValue").val("");
      $("#transactionDate").val("");
      $(".pointsSearchDropdown").val("");
      $("#txnNo").val("");
      $(".pointsSearchDropdown").val("");
      $("#searchButton").click();
    });
  }
});

