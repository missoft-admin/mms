<%@include file="../../common/taglibs.jsp" %>


<div id="content_smsDraftForm" class="modal hide  nofly modal-dialog">

  <div class="modal-content">
  <c:url var="url_action" value="${smsAction}" />
  <form:form id="smsDraftForm" name="smsDraft" modelAttribute="smsDraft" method="POST" action="${url_action}" class="modal-form form-horizontal">
    <div class="modal-header">
      <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
      <h4><c:out value="${smsDialogHeader}" /></h4>
    </div>


    <div class="modal-body">
      <div class="hide alert alert-error" id="content_error"><button type="button" class="close" onclick="$(this).parent().hide();">&times;</button><div><form:errors path="*"/></div></div>

      <div class="main-block">
        <c:if test="${not empty smsDraft.id}"><form:hidden path="id"/></c:if>
        <c:if test="${not empty smsDraft.code}">
        <div id="code" class="code control-group">
          <label for="code" class="control-label"><spring:message code="channel_code"/></label>
          <div  class="controls"><span>${smsDraft.code}</span></div>
        </div>
        </c:if>

        <div class="description control-group">
          <label for="description" class="control-label"><spring:message code="channel_description"/></label>
          <div  class="controls"><form:input id="description" path="description" /></div>
        </div>

        <%-- <div class="targetRecipients control-group">
          <label for="" class="control-label"><spring:message code="channel_target"/></label>
          <div class="controls">
	          <fieldset class="span4 row-fluid">
	            <label for="memberGroup"><spring:message code="channel_membergroup"/></label>
	            <form:select id="memberGroup" path="memberGroup" >
                <option/><form:options  items="${memberGroups}" itemValue="id" itemLabel="name" />
              </form:select>
	          </fieldset>
            <fieldset class="span4 row-fluid">
              <label for="promotion"><spring:message code="channel_promotion"/></label>
              <form:select id="promotion" path="promotion" >
                <option/><form:options  items="${promotions}" itemValue="id" itemLabel="name" />
              </form:select>
            </fieldset>
          </div>
        </div> --%>
        <div class="targetRecipients control-group">
          <label for="" class="control-label">
            <span><spring:message code="channel_target"/></span><br />
            <span>
              <a class="" id="link_targetCount" href="<c:url value="/marketing/channel/target/member/count?memberGrp=%MEMBERGRP%&promo=%PROMO%" />" >
                <spring:message code="channel_lbl_noofmembers" />
              </a>
              <span id="targetCount"></span>
            </span>
          </label>
          <div class="controls" style="margin-top: 5px">
	          <div id="targetMemberGroup" class="row-fluid">
	            <fieldset class="span4 pull-left">
	              <label for="memberGroup">
                  <spring:message code="channel_lbl_membergroups"/><![CDATA[&nbsp;]]>
                  <a id="link_viewMemberGroup" class="hide" href="<c:url value="/groups/member/view/" />">(<spring:message code="label_view" />)</a>
                </label>
	              <select id="memberGroupSelection" multiple="multiple" class="span12">
                  <c:forEach var="item" items="${memberGroups}">
                    <option value="${item.id}">${item.name}</option>
                  </c:forEach>
	              </select>
	            </fieldset>
	    
	            <fieldset class="span1 text-center container-01">
	              <div><input type="button" id="addGroupBtn" class="btn" value=">>"/></div>
	              <div><input type="button" id="removeGroupBtn" class="btn" value="<<"/></div>          
	            </fieldset>
	    
	            <fieldset class="span4 pull-left mb20">
	              <label for="memberGroup"><spring:message code="channel_lbl_selected"/></label>
	              <select id="memberGroupSelected" multiple="multiple" class="span12"></select>
                <form:hidden id="memberGroup" path="memberGroup" />
	            </fieldset>
	            <div class="clearfix"></div>
              <div id="targetMemberGroupView" class="hide span11"></div>
	          </div>

	          <div id="targetPromotion" class="row-fluid">
	            <fieldset class="span4 pull-left">
	              <label for="promotion"><spring:message code="channel_lbl_promotions"/></label>
	              <select id="promotionSelection" multiple="multiple" class="span12">
                  <c:forEach var="item" items="${promotions}">
                    <option value="${item.id}">${item.name}</option>
                  </c:forEach>
                </select>
	            </fieldset>
	    
	            <fieldset class="span1 text-center container-01">
	              <div><input type="button" id="addPromoBtn" class="btn" value=">>"/></div>
	              <div><input type="button" id="removePromoBtn" class="btn" value="<<"/></div>          
	            </fieldset>
	    
	            <fieldset class="span4 pull-left">
	              <label for="promotion"><spring:message code="channel_lbl_selected"/></label>
	              <select id="promotionSelected" multiple="multiple" class="span12"></select>
                <form:hidden id="promotion" path="promotion" />
	            </fieldset>
	            <div class="clearfix"></div>
	          </div>
          </div>
        </div>

        <div class="schedule control-group">
          <label for="schedule" class="control-label"><spring:message code="channel_schedule"/></label>
          <div  class="controls">
            <form:input id="schedule" path="schedule" />
            <form:hidden id="dispSchedule" path="dispSchedule" />
            <div class="input-append bootstrap-timepicker">
	            <form:input id="dispTime" path="dispTime" cssClass="input-mini" />
	            <span class="add-on"><i class="icon-time"></i></span>
            </div>
          </div>
        </div>

        <div class="file control-group">
          <label for="message" class="control-label">
            <spring:message code="channel_message"/>
            (<span id="messageCount">${perCount}/1</span>)
          </label>
          <div  class="controls">
            <spring:message var="maxCountMessage" code="label_max" arguments="${maxCount}" />
            <form:textarea path="message" id="message" data-max-count="${maxCount}" data-per-count="${perCount}" 
              placeholder="${maxCountMessage}" />
          </div>
        </div>
      </div>
    </div>


    <div class="modal-footer">
      <!--<input id="saveBtn" class="btn btn-primary" type="submit" value="<spring:message code="label_save" />" />
      <input id="cancelBtn" class="btn" type="button" value="<spring:message code="label_cancel" />" data-dismiss="modal" /> -->
      <button type="button" id="saveBtn" class="btn btn-primary"><spring:message code="label_save" /></button>
      <button type="button" id="cancelBtn" data-dismiss="modal" class="btn btn-default"><spring:message code="label_cancel" /></button>
    </div>
  </form:form>
  </div>


  <script src='<c:url value="/js/viewspecific/channels/sms/smsDraftForm.js"/>' type='text/javascript'><![CDATA[&nbsp; ]]></script>
  <script src='<c:url value="/js/bootstrap/bootstrap-datepicker.js"/>' type='text/javascript'><![CDATA[&nbsp; ]]></script>
  <script src='<c:url value="/js/bootstrap/bootstrap-timepicker.min.js"/>' type='text/javascript'><![CDATA[&nbsp; ]]></script>
  <link href="<c:url value="/css/bootstrap/datepicker.css" />" rel="stylesheet" />
  <link href="<c:url value="/css/bootstrap/bootstrap-timepicker.min.css" />" rel="stylesheet" />
  <style type="text/css"> 
    <!-- 
    .modal-footer { text-align: center; } 
    .bootstrap-timepicker-widget { z-index: 2000 !important; } 
    .control-label { width: 120px !important; } 
    .controls { margin-left: 140px !important; } 
    .container-01 { margin-top: 22px !important; }
    #targetCount { width: 120px !important;; }
    /* .bootstrap-timepicker input { border-top: 1px solid #ccc; border-bottom: 1px solid #ccc; border-left: 1px solid #ccc; } */
    .bootstrap-timepicker span { border-color: #eeeeee !important; }
    #smsDraftForm.form-horizontal .controls {
        margin-left: 130px !important;
    }
    --> 
  </style>


</div>