<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@include file="../../common/taglibs.jsp" %>
<div id="content_giftcard_service_request">
  <div class="page-header page-header2">
    <h1><spring:message code="gc.service.request.page.title" /></h1>
  </div>
  <div id="section-view-giftcard" class="form-horizontal">
    <div id="content-error" class="hide alert alert-error">
      <button type="button" class="close" onclick="$(this).parent().hide();">&times;</button>
      <div><form:errors path="*"/></div>
    </div>
    <fieldset class="form-horizontal">
      <div class="control-group ">
	      <spring:message code="gc.service.request.field.card.no" var="cardNoLabel" />
	      <label for="inputGiftCardNo" class="control-label pull-left">${cardNoLabel}</label> 
      	<div class="controls">
      	  <input type="text" name="cardNo" id="inputGiftCardNo" class="form-control" 
      		 placeholder="${cardNoLabel}" maxlength="20"/>
      	  
      	</div>
      </div>
      <div class="control-group form-actions">
        <button type="button" id="searchGiftCard" class="btn btn-primary form-control">
    	    <spring:message code="gc.service.request.button.view" />
    	  </button>
      </div>
    </fieldset>
  </div>
  
  <div class="clearfix"></div>
  <jsp:include page="section-service-request.jsp" />
  <div class="clearfix"></div>
  <jsp:include page="section-card-info.jsp" />
  <div class="clearfix"></div>
  <jsp:include page="section-egcinfo-update.jsp" />
  <div id="section-tx-info" class="hide">
    <fieldset>
      <legend>
	<spring:message code="gc.service.request.page.section.tx.info"/>
      </legend>
      <div id="content_giftcard_service_request_list">
        	<div id="list_gc_tx_info"
        	     data-column-created-time="<spring:message code='gc.service.request.table.column.created.time' />"
        	     data-column-tx-time="<spring:message code='gc.service.request.table.column.tx.time' />"
        	     data-column-book-date="<spring:message code='gc.service.request.table.column.created.by' />"
        	     data-column-store-id="<spring:message code='gc.service.request.table.column.store' />"
        	     data-column-terminal-id="<spring:message code='gc.service.request.table.column.terminal.id' />"
        	     data-column-cashier-id="<spring:message code='gc.service.request.table.column.cashier.id' />"
        	     data-column-tx-no="<spring:message code='gc.service.request.table.column.tx.no' />"
        	     data-column-tx-type="<spring:message code='gc.service.request.table.column.tx.type' />"
        	     data-column-tx-prev-bal="<spring:message code='gc.service.request.table.column.tx.prev.balance' />"
        	     data-column-tx-amount="<spring:message code='gc.service.request.table.column.tx.amount' />"
        	     data-column-tx-curr-bal="<spring:message code='gc.service.request.table.column.tx.curr.balance' />"
        	     data-column-tx-currency="<spring:message code='gc.service.request.table.column.currency' />">
        	</div>
      </div>
    </fieldset>
  </div>
  <div class="clearfix"></div>
  <br/>
  <div id="section-card-status" class="hide">
    <fieldset>
      <legend>
	<spring:message code="gc.service.request.section.card.status"/>
      </legend>
      <div id="content_giftcard_card_status_list">
	<div id="list_giftcard_status">
	</div>
      </div>
    </fieldset>
  </div>
</div>
<link href="<spring:url value="/css/dataTables.css" />" rel="stylesheet">
<script src="<spring:url value="/js/jquery/jquery.dataTables.js" />" ></script>
<script src="<spring:url value="/js/jquery/jquery.dataTables.linkLengthMenu.js" />"></script>
<script src="<spring:url value="/js/jquery/jquery.dataTables.customPaging.js" />"></script>
<script src="<spring:url value="/js/jquery/ajax-datatable.js" />" ></script>
<script src="<spring:url value="/js/common/form2js.js" />" ></script>
<script src="<spring:url value="/js/common/json2.js" />" ></script>
<script src="<spring:url value="/js/jquery/jquery.toObject.js" />" ></script>
<script src="<spring:url value="/js/jquery/jquery.numberInput.js" />" ></script>

<script src="<spring:url value="/js/viewspecific/giftcard/service-request.js"/>"></script>
