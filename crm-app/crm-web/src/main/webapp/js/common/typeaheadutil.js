$(document).ready( function() {
	var $style;
	TypeAhead = {
		process : function( $el_, $idEl_, url_, priDataProp, otherDataProps, updaterFunc, altLabel_, urlFunc ) {
			$el_.data( "url", url_ );
			$el_.typeahead({
				source: function( query, process ) {
					var map = {}, $cxs = [], $this = $el_;
					var thisUrl = $el_.data( "url" ) + $this.val();
					if ( urlFunc ) {
						thisUrl = urlFunc( thisUrl );
					}
					if ( $idEl_ ) {
						$idEl_.val( "" );
					}
	                if ( !$this.val().length || $this.val().length < 3 ) { return true; }
	                $.get(
	                	thisUrl,
	                    //{ query: query },
                        function( resp ) {
                            $.map ( resp, function( $resp ) {
                                var group = {
                                    value: $resp[ priDataProp.value ], label: $resp[ priDataProp.label ],
                                    toString: function () { return JSON.stringify(this); },
                                    toLowerCase: function () { return this.label.toLowerCase(); },
                                    indexOf: function (string) { return String.prototype.indexOf.call( this.label.replace( new RegExp(" ", 'g'), "" ), string ); },
                                    replace: function (string) {
                                    	var val = ( altLabel_? $resp[ altLabel_ ] + " - " : "" )  + this.label;
                                        if( typeof( this.label ) != 'undefined' ) {
				                            val += ( "<span class='pull-right hide'>" + ( altLabel_? $resp[ altLabel_ ] : this.label ) + "</span>" );
				                            return String.prototype.replace.apply( "<div style='padding: 10px; font-size: 1em;'>" + val + "</div>", string );
				                        }
				                    }
				                };
                                if ( otherDataProps ) {
                                    $.each( otherDataProps, function( idx, field ) {
                                        group[ field ] = $resp[ field ];
                                    });
                                }
				                $cxs.push( group );
				            });
				            process( $cxs );
				        },
				        "json"
	                );
				},
//				property: "name",
				items: 10,
//              minLength: 2,
	            updater: function( item ) {
	                var $item = JSON.parse( item );
	                if ( $idEl_ ) {
		                $idEl_.val( $item.value );
	                }
	                if ( updaterFunc ) {
		                updaterFunc( $item );
	                }
	                return altLabel_? $item[ altLabel_ ] : $item.label;
	            },
	            matcher: function( item ){
		        	var theEl = $el_.get(0);
		        	var isModal = $(theEl).closest(".modal").length > 0;
		        	$typeahead = $("ul.typeahead", $(theEl).parent());
		        	if(isModal) {
		        		var newPos = elPos( theEl );
				    	var minusHeight = 0;
				    	if ( ( newPos.y + $( "ul.typeahead" ).outerHeight() + $( theEl ).innerHeight() ) > $(window).height() ) {
				    		minusHeight = $( "ul.typeahead" ).outerHeight() + $( theEl ).innerHeight();
				    	}
				    	$( "ul.typeahead" ).css({ "top": ( newPos.y - minusHeight ) + "px !important" });
				    	if ( $style ) {
				    		$style.remove();
				    	}
				    	$style = $( "<style type='text/css'>" 
				    			+ ".typeahead { top: " + ( newPos.y + 35 - minusHeight ) + "px !important; left: " + newPos.x + "px !important; } "
				    				+ "</style>" ).appendTo( "head" );
				    	
		        	} else {
		        		var pos = $(theEl).offset();
			        	var top = pos.top + $(theEl).height()  + 1;
			            var left = pos.left;
			            $typeahead.css({
			            	"top" : top + "px !important",
			            	"left" : left + "px !important",
			            	"position" : "absolute"
			            });	
		        	}
		        	
		        	
	                return true;
	            }
	    	});

			function elPos( ele ){
			    var position = { x: ele.offsetLeft, y: ele.offsetTop };

		        var elScroll = ele;
		        while ( elScroll = elScroll.parentNode ) {
		            position.x -= elScroll.scrollLeft ? elScroll.scrollLeft : 0;
		            position.y -= elScroll.scrollTop ? elScroll.scrollTop : 0;
		        }

			    while( ele = ele.offsetParent ){
			        position.x += ele.offsetLeft;
			        position.y += ele.offsetTop;
			    };

			    var isIE = navigator.appName.indexOf('Microsoft Internet Explorer') != -1;
			    var scrollX_ = isIE? document.body.scrollLeft : window.pageXOffset;
			    var scrollY_ = isIE? document.body.scrollTop : window.pageYOffset;
	            position.x += scrollX_? scrollX_ : 0;
	            position.y += scrollY_? scrollY_ : 0;

			    return position;
			}

            $el_.parent().on( 'mousedown', 'ul.typeahead', function(e) { e.preventDefault(); });
		}
	};

	$( "<style type='text/css'>" 
			+ ".typeahead { background: #ffffff; min-width: 300px; color: #000 !important; max-height: 200px; overflow-y: auto; position: fixed; } "
			//+ ".typeahead > .dropdown-menu { z-index: 200000; } " 
			+ ".dropdown-menu>.active>a { background: #ffffff; color: #000 !important; } "
			+ ".dropdown-menu>.active>a:hover, .dropdown-menu>.active>a:focus { color: #0088cc !important; } "
			//+ ".modal-body { overflow: visible; } .modal-footer { position: relative; z-index: 1; } " 
				+ "</style>" ).appendTo( "head" );

});