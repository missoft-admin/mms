create table CRM_MONITOR_CONFIG (
    ID number(19,0) not null,
    TRANSACTION_COUNT number(10,0),
    MEMBER_TYPE varchar2(15 char),
    primary key (ID)
);

alter table CRM_MONITOR_CONFIG 
    add constraint FK_4ol2o7u5ovx7efxql4oqtdd4n 
    foreign key (MEMBER_TYPE) 
    references CRM_REF_LOOKUP_DTL;

INSERT INTO CRM_MONITOR_CONFIG(ID, TRANSACTION_COUNT, MEMBER_TYPE) VALUES (0, 3, 'MTYP001');
INSERT INTO CRM_MONITOR_CONFIG(ID, TRANSACTION_COUNT, MEMBER_TYPE) VALUES (1, 3, 'MTYP002');