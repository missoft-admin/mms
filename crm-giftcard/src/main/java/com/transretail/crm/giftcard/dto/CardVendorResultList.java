package com.transretail.crm.giftcard.dto;

import java.util.Collection;

import com.transretail.crm.common.service.dto.response.AbstractResultListDTO;

/**
 *
 */
public class CardVendorResultList extends AbstractResultListDTO<CardVendorDto> {
    public CardVendorResultList(Collection<CardVendorDto> results, long totalElements, boolean hasPreviousPage, boolean hasNextPage) {
        super(results, totalElements, hasPreviousPage, hasNextPage);
    }
}
