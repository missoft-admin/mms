package com.transretail.crm.impl;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashSet;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import junit.framework.Assert;

import org.joda.time.LocalDate;
import org.joda.time.LocalDateTime;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.transretail.crm.common.service.dto.response.ResultList;
import com.transretail.crm.core.entity.lookup.LookupDetail;
import com.transretail.crm.core.entity.lookup.LookupHeader;
import com.transretail.crm.core.repo.LookupDetailRepo;
import com.transretail.crm.core.repo.LookupHeaderRepo;
import com.transretail.crm.core.service.CodePropertiesService;
import com.transretail.crm.giftcard.dto.ReturnByCardsItemDto;
import com.transretail.crm.giftcard.dto.ReturnRecordDto;
import com.transretail.crm.giftcard.dto.ReturnSearchDto;
import com.transretail.crm.giftcard.entity.CardVendor;
import com.transretail.crm.giftcard.entity.GiftCardCustomerProfile;
import com.transretail.crm.giftcard.entity.GiftCardInventory;
import com.transretail.crm.giftcard.entity.GiftCardOrder;
import com.transretail.crm.giftcard.entity.GiftCardOrderItem;
import com.transretail.crm.giftcard.entity.GiftCardTransaction;
import com.transretail.crm.giftcard.entity.ProductProfile;
import com.transretail.crm.giftcard.entity.ProductProfile.Denomination;
import com.transretail.crm.giftcard.entity.ReturnByCardsItem;
import com.transretail.crm.giftcard.entity.ReturnByCardsItem.ItemType;
import com.transretail.crm.giftcard.entity.ReturnRecord;
import com.transretail.crm.giftcard.entity.SalesOrder;
import com.transretail.crm.giftcard.entity.SalesOrderItem;
import com.transretail.crm.giftcard.entity.support.GiftCardOrderStatus;
import com.transretail.crm.giftcard.entity.support.GiftCardSaleTransaction;
import com.transretail.crm.giftcard.entity.support.ProductProfileStatus;
import com.transretail.crm.giftcard.entity.support.ReturnStatus;
import com.transretail.crm.giftcard.entity.support.SalesOrderStatus;
import com.transretail.crm.giftcard.entity.support.SalesOrderType;
import com.transretail.crm.giftcard.repo.CardVendorRepo;
import com.transretail.crm.giftcard.repo.GiftCardCustomerProfileRepo;
import com.transretail.crm.giftcard.repo.GiftCardInventoryRepo;
import com.transretail.crm.giftcard.repo.GiftCardOrderRepo;
import com.transretail.crm.giftcard.repo.GiftCardTransactionRepo;
import com.transretail.crm.giftcard.repo.ProductProfileRepo;
import com.transretail.crm.giftcard.repo.ReturnRecordRepo;
import com.transretail.crm.giftcard.repo.SalesOrderRepo;
import com.transretail.crm.giftcard.service.GiftCardInventoryService;
import com.transretail.crm.giftcard.service.GiftCardOrderService;
import com.transretail.crm.giftcard.service.ReturnByCardsService;

/**
 *
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath*:/META-INF/spring/applicationContext*.xml",
    "classpath:META-INF/spring/applicationContext-test.xml"})
public class ReturnByCardsServiceTest {
	@Autowired
	private ReturnByCardsService service;
	@Autowired
	private ReturnRecordRepo repo;
	@Autowired
	private SalesOrderRepo orderRepo;
	@Autowired
	private ProductProfileRepo productRepo;
	@Autowired
	private GiftCardCustomerProfileRepo customerRepo;
	@Autowired
	private GiftCardInventoryRepo inventoryRepo;
	
	private SalesOrder order;
	private ReturnRecord record;
	private CardVendor savedVendor;
    private GiftCardOrder savedGiftCardOrder;
    private ProductProfile savedProfile;
    
    @Autowired
    private CardVendorRepo vendorRepo;
    @Autowired
    private ProductProfileRepo profileRepo;
    @Autowired
    private GiftCardOrderRepo giftCardOrderRepo;
    @Autowired
    private GiftCardOrderService orderService;
    @Autowired
    private GiftCardInventoryService giftCardInventoryService;
    @PersistenceContext
	private EntityManager em;
    @Autowired
    private GiftCardTransactionRepo transactionRepo;
    @Autowired
    private CodePropertiesService codePropertiesService;
    @Autowired
    private LookupHeaderRepo lookupHeaderRepo;
    @Autowired
    private LookupDetailRepo lookupDetailRepo;
	
	@Before
	public void setup() {
		
		LookupHeader hdr = new LookupHeader(codePropertiesService.getProductProfileFaceValue());
        hdr = lookupHeaderRepo.saveAndFlush(hdr);

        LookupDetail dtl = new LookupDetail("PPFV5");
        dtl.setDescription("500000");
        dtl.setHeader(hdr);
        dtl = lookupDetailRepo.saveAndFlush(dtl);
        
		savedProfile = new ProductProfile();
		savedProfile.setCardFee(1.5);
		savedProfile.setEffectiveMonths(12l);
		savedProfile.setFaceValue(dtl);
		savedProfile.setProductCode("0000000000001");
		savedProfile.setStatus(ProductProfileStatus.APPROVED);
		savedProfile.setProductDesc("PROD");
		savedProfile.setUnitCost(.5);
		savedProfile = productRepo.save(savedProfile);
		
		GiftCardCustomerProfile customer = new GiftCardCustomerProfile();
		customer.setContactFirstName("fname");
		customer.setContactLastName("lname");
		customer.setContactNo("contactno");
		customer.setName("name");
		customer.setInvoiceTitle("invoicetitle");
		customer.setCustomerId("customerid");
		customer = customerRepo.save(customer);
		
		order = new SalesOrder();
		order.setOrderNo("BBMMS0501001");
		order.setOrderDate(new LocalDate());
		order.setStatus(SalesOrderStatus.SOLD);
		order.setOrderType(SalesOrderType.B2B_SALES);
		order.setItems(new HashSet<SalesOrderItem>());
		order.setCustomer(customer);
		
		SalesOrderItem item = new SalesOrderItem();
		item.setProduct(savedProfile);
		item.setQuantity(10l);
		item.setFaceAmount(new BigDecimal(500000));
		item.setOrder(order);
		order.getItems().add(item);
		order = orderRepo.save(order);
		
		GiftCardTransaction transaction = new GiftCardTransaction();
		transaction.setTransactionNo("1123123");
		transaction.setTransactionDate(new LocalDateTime());
		transaction.setTransactionType(GiftCardSaleTransaction.ACTIVATION);
		transaction.salesOrder(order);
		transactionRepo.save(transaction);
		
		
		savedVendor = new CardVendor();
        savedVendor.setFormalName("name");
        savedVendor.setMailAddress("add");
        savedVendor.setEmailAddress("email");
        savedVendor.setEnabled(true);
        savedVendor.setRegion("region");
        savedVendor.setDefaultVendor(true);
        savedVendor = vendorRepo.save(savedVendor);
        
        savedGiftCardOrder = new GiftCardOrder();
        savedGiftCardOrder.setMoNumber("140403000001");
        savedGiftCardOrder.setPoNumber("0001");
        savedGiftCardOrder.setMoDate(new LocalDate());
        savedGiftCardOrder.setPoDate(new LocalDate());
        savedGiftCardOrder.setVendor(savedVendor);
        savedGiftCardOrder.setStatus(GiftCardOrderStatus.APPROVED);
        savedGiftCardOrder.setItems(new ArrayList<GiftCardOrderItem>());
        
        GiftCardOrderItem gcItem = new GiftCardOrderItem();
        gcItem.setProfile(savedProfile);
        gcItem.setProductCode(savedProfile.getProductCode());
        gcItem.setQuantity(10L);
        gcItem.setOrder(savedGiftCardOrder);
        savedGiftCardOrder.getItems().add(gcItem);
        
        savedGiftCardOrder = giftCardOrderRepo.save(savedGiftCardOrder);
        
        GiftCardInventory inventory = new GiftCardInventory();
        inventory.setProfile(savedProfile);
        inventory.setProductName(savedProfile.getProductDesc());
        inventory.setProductCode(savedProfile.getProductCode());
        inventory.setSeqNo(1);
        inventory.setBatchNo(1);
        inventory.setOrderDate(savedGiftCardOrder.getMoDate());
        inventory.setFaceValue(new BigDecimal(Denomination.D500K.getAmount()));
        inventory.setOrder(savedGiftCardOrder);
        inventory.setIsEgc(false);
        inventory.setSeries(Long.valueOf("1406180010000001"));
        inventory.setBarcode("14061800100000016093");
        inventoryRepo.save(inventory);
        
        inventory = new GiftCardInventory();
        inventory.setProfile(savedProfile);
        inventory.setProductName(savedProfile.getProductDesc());
        inventory.setProductCode(savedProfile.getProductCode());
        inventory.setSeqNo(2);
        inventory.setBatchNo(1);
        inventory.setOrderDate(savedGiftCardOrder.getMoDate());
        inventory.setFaceValue(new BigDecimal(Denomination.D500K.getAmount()));
        inventory.setOrder(savedGiftCardOrder);
        inventory.setIsEgc(false);
        inventory.setSeries(Long.valueOf("1406180010000002"));
        inventory.setBarcode("14061800100000029211");
        inventoryRepo.save(inventory);
        
        inventory = new GiftCardInventory();
        inventory.setProfile(savedProfile);
        inventory.setProductName(savedProfile.getProductDesc());
        inventory.setProductCode(savedProfile.getProductCode());
        inventory.setSeqNo(3);
        inventory.setBatchNo(1);
        inventory.setOrderDate(savedGiftCardOrder.getMoDate());
        inventory.setFaceValue(new BigDecimal(Denomination.D500K.getAmount()));
        inventory.setOrder(savedGiftCardOrder);
        inventory.setIsEgc(false);
        inventory.setSeries(Long.valueOf("1406180010000003"));
        inventory.setBarcode("14061800100000039350");
        inventoryRepo.save(inventory);
        
        inventory = new GiftCardInventory();
        inventory.setProfile(savedProfile);
        inventory.setProductName(savedProfile.getProductDesc());
        inventory.setProductCode(savedProfile.getProductCode());
        inventory.setSeqNo(4);
        inventory.setBatchNo(1);
        inventory.setOrderDate(savedGiftCardOrder.getMoDate());
        inventory.setFaceValue(new BigDecimal(Denomination.D500K.getAmount()));
        inventory.setOrder(savedGiftCardOrder);
        inventory.setIsEgc(false);
        inventory.setSeries(Long.valueOf("1406180010000004"));
        inventory.setBarcode("14061800100000048997");
        inventoryRepo.save(inventory);
        
        record = new ReturnRecord();
		record.setOrderNo(order);
		record.setRecordNo("111111111");
		record.setStatus(ReturnStatus.NEW);
		record.setItems(new ArrayList<ReturnByCardsItem>());
		
        String startingSeries = "14061800100000016093";
        String endingSeries = "14061800100000029211";
		
		ReturnByCardsItem retItem = new ReturnByCardsItem();
		retItem.setStartingSeries(startingSeries);
		retItem.setEndingSeries(endingSeries);
		retItem.setRecord(record);
		retItem.setType(ItemType.RETURN);
		record.getItems().add(retItem);
		
		record = repo.save(record);
		
	}
	@After
	public void teardown() {
		repo.deleteAll();
		transactionRepo.deleteAll();
		orderRepo.deleteAll();
		
		inventoryRepo.deleteAll();
        giftCardOrderRepo.deleteAll();
        vendorRepo.deleteAll();
        profileRepo.deleteAll();
	}
	@Test
	public void getReturnsTest() {
		ResultList<ReturnRecordDto> list = service.getReturns(new ReturnSearchDto());
		Assert.assertTrue(list.getNumberOfElements() == 1);
	}
	@Test
	public void getReturnTest() {
		ReturnRecordDto ret = service.getReturn(record.getId());
		Assert.assertNotNull(ret);
	}
	@Test
	public void getReturnByRecordNoTest() {
		ReturnRecordDto ret = service.getReturnByRecordNo(record.getRecordNo());
		Assert.assertNotNull(ret);
		ret = service.getReturnByRecordNo("00000000000");
		Assert.assertNull(ret);
		
	}
	
	@Test
	public void saveAndReturnTest() {
		ReturnRecordDto recDto = service.getReturnByRecordNo("111111111");
		recDto.setStatus(ReturnStatus.APPROVED);
		recDto.setOrderId(order.getId());
		recDto.setReturns(new ArrayList<ReturnByCardsItemDto>());
		
		ReturnByCardsItemDto item = new ReturnByCardsItemDto();
		item.setStartingSeries("14061800100000039350");
		item.setEndingSeries("14061800100000048997");
		recDto.getReturns().add(item);
		
		service.saveAndReturn(recDto);
	}
	
}

