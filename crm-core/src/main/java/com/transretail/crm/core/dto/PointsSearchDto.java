package com.transretail.crm.core.dto;


import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.ArrayUtils;
import org.joda.time.DateTime;
import org.springframework.util.StringUtils;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.mysema.query.types.expr.BooleanExpression;
import com.transretail.crm.common.service.dto.request.AbstractSearchFormDto;
import com.transretail.crm.common.util.DateUtil;
import com.transretail.crm.core.entity.MemberModel;
import com.transretail.crm.core.entity.QPointsTxnModel;
import com.transretail.crm.core.entity.enums.PointTxnType;
import com.transretail.crm.core.entity.enums.TxnStatus;
import com.transretail.crm.core.util.BooleanExprUtil;


public class PointsSearchDto extends AbstractSearchFormDto {

	private String accountId;
	private String name;
	private String email;
    private String contact;
    private String ktpId;
    private String location;
    private Date txnDate;
    private String txnNo;
	private String memberType;
    private String transactionType;
    private String status;
    private String id;
    private List<String> contributer;
    private String registeredStore;

    private boolean isForProcessing;
    private boolean isActive;

    private DateTime postingDateFrom;
    private DateTime postingDateTo;
    
    private Date transactionDateFrom;
    private Date transactionDateTo;

    private Long highestPoint;
	private List<String> customerTypes;
	private MemberModel txnContributer;
	private PointTxnType[] exceptTypes;
	
	public MemberModel getTxnContributer() {
		return txnContributer;
	}
	public void setTxnContributer(MemberModel txnContributer) {
		this.txnContributer = txnContributer;
	}




	@Override
	@JsonIgnore
	public BooleanExpression createSearchExpression() {
		
		List<BooleanExpression> expressions = new ArrayList<BooleanExpression>();
        QPointsTxnModel theQModel = QPointsTxnModel.pointsTxnModel;

        if ( ArrayUtils.isNotEmpty( exceptTypes ) ) {
        	expressions.add( theQModel.transactionType.notIn( exceptTypes ) );
        }
        if ( StringUtils.hasText( accountId ) ) {
//    		expressions.add( BooleanExprUtil.INSTANCE.isStringPropertyLike( theQModel.memberModel.accountId, accountId )
//    				.or(BooleanExprUtil.INSTANCE.isStringPropertyLike(theQModel.transactionContributer.accountId, accountId)));
        	expressions.add( 
        			BooleanExpression.anyOf( 
        					BooleanExprUtil.INSTANCE.isStringPropertyLike( theQModel.memberModel.accountId, accountId ), 
        					null != txnContributer? theQModel.transactionContributer.eq( txnContributer ) : null 
        			)
        	);
        }
        if ( StringUtils.hasText( contact ) ) {
            expressions.add( BooleanExprUtil.INSTANCE.isStringPropertyLike( theQModel.memberModel.contact, contact ) );
        }
        
        if ( StringUtils.hasText( ktpId ) ) {
            expressions.add( BooleanExprUtil.INSTANCE.isStringPropertyLike( theQModel.memberModel.idNumber, ktpId ) );
        }
        
        if ( StringUtils.hasText( email ) ) {
            expressions.add( BooleanExprUtil.INSTANCE.isStringPropertyLike( theQModel.memberModel.email, email ) );
        }
        
        if (StringUtils.hasText( name ) ) {
            expressions.add( 
                    BooleanExprUtil.INSTANCE.isStringPropertyLike( theQModel.memberModel.firstName, name).or( 
                    BooleanExprUtil.INSTANCE.isStringPropertyLike( theQModel.memberModel.lastName, name) ) );
        }
        
        if ( StringUtils.hasText( registeredStore ) ) {
            expressions.add( BooleanExprUtil.INSTANCE.isStringPropertyLike( theQModel.memberModel.registeredStore, registeredStore ) );
        }
        
        if ( StringUtils.hasText( location ) ) {
            expressions.add( BooleanExprUtil.INSTANCE.isStringPropertyLike( theQModel.storeCode, location ) );
        }
        
        if ( StringUtils.hasText( txnNo ) ) {
            expressions.add( BooleanExprUtil.INSTANCE.isStringPropertyLike( theQModel.transactionNo, txnNo ) );
        }
        
        if ( StringUtils.hasText( transactionType ) ) {
            expressions.add( BooleanExprUtil.INSTANCE.isEnumPropertyIn( theQModel.transactionType, PointTxnType.valueOf( transactionType ) ) );
        }
        
        if ( StringUtils.hasText( status ) ) {
            expressions.add( BooleanExprUtil.INSTANCE.isEnumPropertyIn( theQModel.status, TxnStatus.valueOf( status ) ) );
        }

        if ( StringUtils.hasText( status ) && !isForProcessing && !isActive ) {
            expressions.add( BooleanExprUtil.INSTANCE.isEnumPropertyIn( theQModel.status, TxnStatus.valueOf( status ) ) );
        }
        else if ( isForProcessing && isActive ) {
        	if ( StringUtils.hasText( status ) ) {
                expressions.add( 
                    	BooleanExprUtil.INSTANCE.isEnumPropertyIn( theQModel.status, TxnStatus.FORAPPROVAL )
                    	.or( BooleanExprUtil.INSTANCE.isEnumPropertyIn( theQModel.status, TxnStatus.REJECTED ) )
                    	.or( BooleanExprUtil.INSTANCE.isEnumPropertyIn( theQModel.status, TxnStatus.ACTIVE ) ).or( theQModel.status.isNull() )
                    	.or( BooleanExprUtil.INSTANCE.isEnumPropertyIn( theQModel.status, TxnStatus.valueOf( status ) ) ) );
        	}
        	else {
                expressions.add( 
                    	BooleanExprUtil.INSTANCE.isEnumPropertyIn( theQModel.status, TxnStatus.FORAPPROVAL )
                    	.or( BooleanExprUtil.INSTANCE.isEnumPropertyIn( theQModel.status, TxnStatus.REJECTED ) )
                    	.or( BooleanExprUtil.INSTANCE.isEnumPropertyIn( theQModel.status, TxnStatus.ACTIVE ) ).or( theQModel.status.isNull() ) );
        	}
        }
        else if ( isForProcessing ) {
        	if ( StringUtils.hasText( status ) ) {
                expressions.add( 
                    	BooleanExprUtil.INSTANCE.isEnumPropertyIn( theQModel.status, TxnStatus.FORAPPROVAL )
                    	.or( BooleanExprUtil.INSTANCE.isEnumPropertyIn( theQModel.status, TxnStatus.REJECTED ) )
                    	.or( BooleanExprUtil.INSTANCE.isEnumPropertyIn( theQModel.status, TxnStatus.valueOf( status ) ) ) );
        	}
        	else {
                expressions.add( 
                    	BooleanExprUtil.INSTANCE.isEnumPropertyIn( theQModel.status, TxnStatus.FORAPPROVAL )
                    	.or( BooleanExprUtil.INSTANCE.isEnumPropertyIn( theQModel.status, TxnStatus.REJECTED ) ) );
        	}
        }
        else if ( isActive ) {
        	if ( StringUtils.hasText( status ) ) {
                expressions.add( BooleanExprUtil.INSTANCE.isEnumPropertyIn( theQModel.status, TxnStatus.ACTIVE ).or( theQModel.status.isNull() )
                		.or( BooleanExprUtil.INSTANCE.isEnumPropertyIn( theQModel.status, TxnStatus.valueOf( status ) ) ) );
        	}
        	else {
                expressions.add( BooleanExprUtil.INSTANCE.isEnumPropertyIn( theQModel.status, TxnStatus.ACTIVE ).or( theQModel.status.isNull() ) );
        	}
        }

        if ( StringUtils.hasText( memberType ) && CollectionUtils.isEmpty( customerTypes ) ) {
            expressions.add( BooleanExprUtil.INSTANCE.isStringPropertyLike( theQModel.memberModel.memberType.code, memberType ) );
        }
        else if ( CollectionUtils.isNotEmpty( customerTypes ) ) {
        	if ( StringUtils.hasText( memberType ) ) {
        		customerTypes.add( memberType );
        	}
        	expressions.add( BooleanExprUtil.INSTANCE.isStringPropertyIn( theQModel.memberModel.memberType.code, customerTypes ) );
        }

        if ( null != highestPoint ) {
        	expressions.add( theQModel.transactionPoints.loe( highestPoint ) );
        }
        
        if(postingDateFrom != null && postingDateTo != null) {
        	expressions.add(theQModel._super.created.between(postingDateFrom, DateUtil.getEndOfDay(postingDateTo)));
        } else if (postingDateFrom != null) {
            expressions.add(theQModel._super.created.goe(postingDateFrom));
        } else if(postingDateTo != null) {
        	expressions.add(theQModel._super.created.loe(DateUtil.getEndOfDay(postingDateTo)));
        }
        
        if(transactionDateFrom != null && transactionDateTo != null) {
        	expressions.add(theQModel.transactionDateTime.between(transactionDateFrom, DateUtil.getEndOfDay(transactionDateTo)));
        } else if (transactionDateFrom != null){
            expressions.add(theQModel.transactionDateTime.goe(transactionDateFrom));
        } else if(transactionDateTo != null) {
            expressions.add(theQModel.transactionDateTime.loe(DateUtil.getEndOfDay(transactionDateTo)));
        }
        
        if ( null != txnDate ) {
            expressions.add( theQModel.transactionDateTime.eq( txnDate ) );
        }
        
        if(StringUtils.hasText(id)) {
        	expressions.add( BooleanExprUtil.INSTANCE.isStringPropertyLike( theQModel.id, id ) );
        }
        
        if(contributer != null && !contributer.isEmpty()) {
        	BooleanExpression contributerExpr = BooleanExprUtil.INSTANCE.isStringPropertyLike(
        			theQModel.transactionContributer.accountId, contributer.get(0));
        	for(int i = 1; i < contributer.size(); i++) {
        		contributerExpr = contributerExpr.or(BooleanExprUtil.INSTANCE.isStringPropertyLike(
        				theQModel.transactionContributer.accountId, contributer.get(i)));
        	}
        	expressions.add(contributerExpr);
        }
		return BooleanExpression.allOf(expressions.toArray(new BooleanExpression[expressions.size()]));
	}




	public String getAccountId() {
		return accountId;
	}

	public void setAccountId(String accountId) {
		this.accountId = accountId;
	}

	public String getMemberType() {
		return memberType;
	}

	public void setMemberType(String memberType) {
		this.memberType = memberType;
	}

	public String getTransactionType() {
		return transactionType;
	}

	public void setTransactionType(String transactionType) {
		this.transactionType = transactionType;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public boolean isForProcessing() {
		return isForProcessing;
	}

	public void setForProcessing(boolean isForProcessing) {
		this.isForProcessing = isForProcessing;
	}

	public boolean isActive() {
		return isActive;
	}

	public void setActive(boolean isActive) {
		this.isActive = isActive;
	}

	public Long getHighestPoint() {
		return highestPoint;
	}

	public void setHighestPoint(Long highestPoint) {
		this.highestPoint = highestPoint;
	}

	public List<String> getCustomerTypes() {
		return customerTypes;
	}

	public void setCustomerTypes(List<String> customerTypes) {
		this.customerTypes = customerTypes;
	}
	public DateTime getPostingDateFrom() {
		return postingDateFrom;
	}
	public void setPostingDateFrom(DateTime postingDateFrom) {
		this.postingDateFrom = postingDateFrom;
	}
	public DateTime getPostingDateTo() {
		return postingDateTo;
	}
	public void setPostingDateTo(DateTime postingDateTo) {
		this.postingDateTo = postingDateTo;
	}
	public Date getTransactionDateFrom() {
		return transactionDateFrom;
	}
	public void setTransactionDateFrom(Date transactinDateFrom) {
		this.transactionDateFrom = transactinDateFrom;
	}
	public Date getTransactionDateTo() {
		return transactionDateTo;
	}
	public void setTransactionDateTo(Date transactinDateTo) {
		this.transactionDateTo = transactinDateTo;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public List<String> getContributer() {
		return contributer;
	}

	public void setContributer(List<String> contributer) {
		this.contributer = contributer;
	}
    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }
    public String getEmail() {
        return email;
    }
    public void setEmail(String email) {
        this.email = email;
    }
    public String getContact() {
        return contact;
    }
    public void setContact(String contact) {
        this.contact = contact;
    }
    public String getKtpId() {
        return ktpId;
    }
    public void setKtpId(String ktpId) {
        this.ktpId = ktpId;
    }
    public String getLocation() {
        return location;
    }
    public void setLocation(String location) {
        this.location = location;
    }
    public Date getTxnDate() {
        return txnDate;
    }
    public void setTxnDate(Date txnDate) {
        this.txnDate = txnDate;
    }
    public String getTxnNo() {
        return txnNo;
    }
    public void setTxnNo(String txnNo) {
        this.txnNo = txnNo;
    }
    public String getRegisteredStore() {
        return registeredStore;
    }
    public void setRegisteredStore(String registeredStore) {
        this.registeredStore = registeredStore;
    }
	public PointTxnType[] getExceptTypes() {
		return exceptTypes;
	}
	public void setExceptTypes(PointTxnType[] exceptTypes) {
		this.exceptTypes = exceptTypes;
	}

}
