$( function() {


	var theRoles = $( ".content_role" );



	initMenu();



	function initMenu() {
		$.each( theRoles, function( idx) {
			$( this ).click( updateMenu );
		});
		if ( theRoles ) {
			$( theRoles[0] ).click();
		}
	}



	function updateMenu(e) {
		$( "#roleLabel" ).html( $(this).data( "role-label" ) );
		var allRoleAccesses = $( ".content_roleAccess" );
		$.each( allRoleAccesses, function( idx, aRoleAccess ) {
			$( this ).hide();
		});
		var theRoleAccesses = $( "span." + $(this).data( "role-access" ) );
		$.each( theRoleAccesses, function( idx, theRoleAccess ) {
			$( this ).show();
		});
	}

});


//for debugging purpose, so that you can see what is in the menu cookie
$('#debug').html('Cookie Content : ' + readCookie('menu'));

//if cookie menu exists
if (readCookie('menu')) {
	
	//loop through the menu item
	$('#menu a').each(function () {
		//match the correct link and add selected class to it
		if ($(this).html() == readCookie('menu')) $(this).addClass('selected');
	});	
}

$('#menu a').click(function () {

	//Set the cookie according to the text in the link
	createCookie('menu', $(this).html(),1);
});


/* Cookie Function */
function createCookie(name, value, days) {
if (days) {
	var date = new Date();
	date.setTime(date.getTime()+(days*24*60*60*1000));
	var expires = "; expires="+date.toGMTString();
}
else var expires = "";
document.cookie = name+"="+value+expires+"; path=/";
}

function readCookie(name) {
var nameEQ = name + "=";
var ca = document.cookie.split(';');
for(var i=0;i < ca.length;i++) {
	var c = ca[i];
	while (c.charAt(0)==' ') c = c.substring(1,c.length);
	if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length,c.length);
}
return null;
}

function eraseCookie(name) {
createCookie(name,"",-1);
}

/*function getCookie(c_name) {
    var x, y, ARRcookies = document.cookie.split(";");
    for( var i = 0; i < ARRcookies.length; i++ ) {

        x = ARRcookies[i].substr(0, ARRcookies[i].indexOf("="));
        y = ARRcookies[i].substr(ARRcookies[i].indexOf("=") + 1);
        x = x.replace(/^\s+|\s+$/g, "");
        if (x == c_name) {
            return unescape(y);
        }
    }
}

function setCookie(c_name, value, exdays) {
     var exdate = new Date();
     exdate.setDate(exdate.getDate() + exdays);
     var c_value = escape(value) + ((exdays === null) ? "" : "; expires=" + exdate.toUTCString());
     document.cookie = c_name + "=" + c_value;
}*/