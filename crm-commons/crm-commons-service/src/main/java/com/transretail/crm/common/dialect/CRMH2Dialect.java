package com.transretail.crm.common.dialect;

import org.hibernate.dialect.H2Dialect;
import org.hibernate.dialect.function.StandardSQLFunction;

public class CRMH2Dialect extends H2Dialect {
    public CRMH2Dialect() {
        super();
        registerFunction("to_date", new StandardSQLFunction("parsedatetime"));
        registerFunction("trunc", new StandardSQLFunction("trunc"));
    }
}