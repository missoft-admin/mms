alter table CRM_GC_TRANSACTION add SETTLEMENT boolean;

-- for local only
create table GIFT_CARD_TRANSACTION (
        ID varchar(255) not null,
        AMOUNT double,
        BALANCE double,
        CARD_NUMBER varchar(255),
        CASHIER_ID varchar(255),
        EXPIRE_DATE varchar(255),
        GC_ID varchar(255),
        GC_SERVER_TYPE varchar(255),
        GC_TX_ID varchar(255),
        MERCHANT_ID varchar(255),
        MOBILE_NUMBER varchar(255),
        NOTE varchar(255),
        PIN_CODE varchar(255),
        PREVIOUS_BALANCE double,
        REFERENCE_NUMBER varchar(255),
        REQUEST_TYPE char(255),
        STATUS char(255),
        TERMINAL_ID varchar(255),
        TRANSACTION_DATE timestamp,
        TX_ID varchar(255),
        TX_ORDER_INDEX integer,
        primary key (ID)
    );