package com.transretail.crm.core.dto;


import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.transretail.crm.core.entity.Campaign;
import com.transretail.crm.core.entity.Promotion;
import com.transretail.crm.core.entity.embeddable.Duration;
import com.transretail.crm.core.util.CalendarUtil;


public class CampaignDto {

//	private static final String[] IGNORE = { "status", "program", "promotions" };

	private Long id;
	private String status;
	private String name;
    private String description;
    private Long startDate;
    private Long endDate;
    private String dispStartDate;
    private String dispEndDate;

    private Long program;
    private List<PromotionDto> promotions;

	public CampaignDto() {}
	public CampaignDto( Campaign inCampaign, boolean renderPromo ) {
        this.id = inCampaign.getId();
        this.name = inCampaign.getName();
        this.description = inCampaign.getDescription();
		if ( null != inCampaign.getStatus() ) {
            this.status = inCampaign.getStatus().getCode();
		}
		if ( null != inCampaign.getDuration() ) {
			if ( null != inCampaign.getDuration().getStartDate() ) {
//				startDate = new DateTime(inCampaign.getDuration().getStartDate().getTime());
//				dispStartDate = inCampaign.getDuration().getStartDate();
                this.startDate = inCampaign.getDuration().getStartDate().getTime();
                this.dispStartDate = CalendarUtil.INSTANCE.getDateFormatter().print(this.startDate);
			}
			if ( null != inCampaign.getDuration().getEndDate() ) {
//				endDate = new DateTime(inCampaign.getDuration().getEndDate().getTime());
//				dispEndDate = inCampaign.getDuration().getEndDate();
                this.endDate = inCampaign.getDuration().getEndDate().getTime();
                this.dispEndDate = CalendarUtil.INSTANCE.getDateFormatter().print(this.endDate);
			}
		}
		if ( null != inCampaign.getProgram() ) {
            this.program = inCampaign.getProgram().getId();
		}
		if ( null != inCampaign.getPromotions() && renderPromo ) {
            this.promotions = new ArrayList<PromotionDto>();
			for ( Promotion thePromotion : inCampaign.getPromotions() ) {
                this.promotions.add( new PromotionDto( thePromotion ) );
			}
		}
	}

	public Campaign toModel() {
		Campaign theModel = new Campaign();
		theModel.setName(this.name);
        theModel.setDescription(this.description);
		Duration theDuration = new Duration();
		if ( null != this.startDate ) {
//			theDuration.setStartDate(startDate.toDate());
			theDuration.setStartDate( new Date( this.startDate ) );
		}
		if ( null != this.endDate ) {
//			theDuration.setEndDate(endDate.toDate());
			theDuration.setEndDate( new Date( this.endDate ) );
		}
		theModel.setDuration( theDuration );
		return theModel;
	}




	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Long getStartDate() {
		return startDate;
	}

	public void setStartDate(Long startDate) {
		this.startDate = startDate;
	}

	public Long getEndDate() {
		return endDate;
	}

	public void setEndDate(Long endDate) {
		this.endDate = endDate;
	}

	public String getDispStartDate() {
		return dispStartDate;
	}

	public void setDispStartDate(String dispStartDate) {
		this.dispStartDate = dispStartDate;
	}

	public String getDispEndDate() {
		return dispEndDate;
	}

	public void setDispEndDate(String dispEndDate) {
		this.dispEndDate = dispEndDate;
	}

	public Long getProgram() {
		return program;
	}

	public void setProgram(Long program) {
		this.program = program;
	}

	public List<PromotionDto> getPromotions() {
		return promotions;
	}

	public void setPromotions(List<PromotionDto> promotions) {
		this.promotions = promotions;
	}
}
