<%@ include file="../../common/taglibs.jsp"%>

<style type="text/css">
<!--
  .well .input {
    background-color: #fff;
    border: 1px solid #E8E8E8;
    box-shadow: none;    
    padding: 6px;
    height: 18px;
    margin-left: 5px;
  }
  .well .btn-primary {
    margin-left: 10px;
  }
  .well select {
    margin: 5px;
  }
  
-->
</style>

<form:form id="orderSearchDto" cssClass="">
<div class="well clearfix">
<div class="form-inline form-search" id="orderSearchForm">
	<spring:message code="label_search" var="searchLabel"/>
	<label class="label-single">${searchLabel}:</label>
	<select id="criteria" class="searchField">
		<option value=""><spring:message code="label_criteria" arguments="${searchLabel}" /></option>
		<c:forEach items="${orderSearchCriteria}" var="criteria">
		<option value="${criteria.field}"><spring:message code="order_search_${criteria.field}" /></option>
		</c:forEach>
	</select>
	<input type="text" id="criteriaValue" class="input searchField" placeholder="${searchLabel}"/>
	<input type="text" name="orderDate" id="orderDateSearch" class="input searchField" placeholder="<spring:message code="mo_prop_orderdate" />"/>
	<input id="search" type="button" class="btn btn-primary" value="<spring:message code="button_search"/>"/>
	<input type="button" value="<spring:message code="label_clear" />" class="btn custom-reset" data-form-id="orderSearchForm" data-default-id="search" />
</div>
<div class="form-inline form-search">
	<label class="label-single"><spring:message code="label_filter" />:</label>
	<select name="cardVendorId" class="searchField">
		<option value=""><spring:message code="mo_prop_supplier" /></option>
		<c:forEach items="${vendors.results}" var="supplier">
		<option value="${supplier.id}">${supplier.name}</option>
		</c:forEach>
	</select>
	<select name="statusCode" class="searchField">
		<option value=""><spring:message code="mo_prop_status" /></option>
		<c:forEach items="${orderStatus}" var="stat">
		<option value="${stat}"><spring:message code="gc_order_status_${stat}" /></option>
		</c:forEach>
	</select>
</div>
</div>
</form:form>

<script>
$(document).ready(function() {
	$orderSearchForm = $("#orderSearchDto");
	
	$("#orderDateSearch", $orderSearchForm).datepicker({
		format		:	'dd-mm-yyyy',
		endDate		:	'-0d',
 		autoclose	:	true
	});
	
	$("#clear", $orderSearchForm).click(function() {
		$(".searchField", $orderSearchForm).val("");
	});

	$("#search", $orderSearchForm).click(function() {
		$("#criteriaValue", $orderSearchForm).attr("name", $("#criteria", $orderSearchForm).val());
		var formDataObj = $orderSearchForm.toObject( { mode : 'first', skipEmpty : true } );
		console.log(formDataObj);
		$("#order_list_container").ajaxDataTable('search', formDataObj);
	});
});
</script>
