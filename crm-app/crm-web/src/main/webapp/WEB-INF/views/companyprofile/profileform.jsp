<%@ include file="../common/taglibs.jsp"%>

  <div class="page-header page-header2">
      <h1><spring:message code="menutab_programsetup_maintenance_company" /></h1>
  </div>

<c:set var="ENCTYPE" value="multipart/form-data"/>

<form:form cssClass="form-horizontal" id="companyProfile" name="companyProfile" modelAttribute="companyProfile"
	action="${pageContext.request.contextPath}/companyprofile" method="POST" enctype="${ENCTYPE}">
	
			<c:set var="errors"><form:errors path="*"/></c:set>
			<c:if test="${not empty errors}">
            <div class="alert alert-error">
            	<button type="button" class="close" data-dismiss="alert">&times;</button>
                <div>${errors}</div>
            </div>
            </c:if>
       
       <div class="control-group">  
       	   <spring:message code="label_company_name" var="companyName" />
           <label class="control-label" for="name">${companyName}</label>  
           <div class="controls"><form:input path="name" placeholder="${companyName}" /></div>  
       </div>
       
       <div class="control-group">  
       	   <spring:message code="label_company_copyrighttext" var="copyrightText" />
           <label class="control-label" for="copyrightText">${copyrightText}</label>  
           <div class="controls"><form:textarea path="copyrightText" placeholder="${copyrightText}" /></div>  
       </div>
       
       <div class="control-group">  
       	   <spring:message code="label_company_logo" var="logo" />
           <label class="control-label" for="file">${logo}</label>  
           <div class="controls"><input type="file" name="file" id="file"></input></div>  
       </div>
       
       <div class="form-actions">  
           <button type="submit" class="btn btn-primary"><spring:message code="label_save" /></button>  
           <button class="btn"><spring:message code="label_cancel" /></button>  
       </div>
</form:form>

<script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/viewspecific/groups/storegroup.js"><![CDATA[&nbsp;]]></script>
