    create table CRM_SAFETYSTOCK (
        CREATED_BY varchar2(255 char),
        CREATED_DATETIME timestamp,
        LAST_UPDATED_BY varchar2(255 char),
        LAST_UPDATED_DATETIME timestamp,
        QTY_PER_MONTH varchar2(255 char),
        PRODUCT_PROFILE_ID number(19,0) not null,
        primary key (PRODUCT_PROFILE_ID)
    );

    alter table CRM_SAFETYSTOCK
        add constraint FK_ny2nbfldool2pfw5lo0klm1mg
        foreign key (PRODUCT_PROFILE_ID)
        references CRM_GC_PROD_PROFILE;