package com.transretail.crm.giftcard.service.impl;

import java.util.List;
import java.util.Set;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import com.mysema.query.types.expr.BooleanExpression;
import com.mysema.query.types.expr.CaseBuilder;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.joda.time.DateTime;
import org.joda.time.LocalDate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.support.TransactionSynchronizationAdapter;
import org.springframework.transaction.support.TransactionSynchronizationManager;

import com.google.common.collect.Lists;
import com.mysema.query.jpa.JPASubQuery;
import com.mysema.query.jpa.impl.JPAQuery;
import com.mysema.query.jpa.impl.JPAUpdateClause;
import com.mysema.query.types.Expression;
import com.mysema.query.types.expr.DateExpression;
import com.mysema.query.types.template.DateTemplate;
import com.transretail.crm.core.dto.ApprovalRemarkDto;
import com.transretail.crm.core.entity.ApplicationConfig;
import com.transretail.crm.core.entity.ApprovalRemark;
import com.transretail.crm.core.entity.enums.AppKey;
import com.transretail.crm.core.entity.enums.ModelType;
import com.transretail.crm.core.jpa.DbMetadataUtil;
import com.transretail.crm.core.jpa.DbMetadataUtil.DbType;
import com.transretail.crm.core.repo.ApplicationConfigRepo;
import com.transretail.crm.core.repo.ApprovalRemarkRepo;
import com.transretail.crm.core.repo.LookupDetailRepo;
import com.transretail.crm.core.service.CodePropertiesService;
import com.transretail.crm.giftcard.dto.SalesOrderDeptAllocDto;
import com.transretail.crm.giftcard.dto.SalesOrderDto;
import com.transretail.crm.giftcard.dto.SalesOrderMultDeptAllocsDto;
import com.transretail.crm.giftcard.entity.GiftCardInventory;
import com.transretail.crm.giftcard.entity.GiftCardOrderHistory;
import com.transretail.crm.giftcard.entity.QGiftCardInventory;
import com.transretail.crm.giftcard.entity.QProductProfile;
import com.transretail.crm.giftcard.entity.QSalesOrderAlloc;
import com.transretail.crm.giftcard.entity.QSalesOrderItem;
import com.transretail.crm.giftcard.entity.SalesOrder;
import com.transretail.crm.giftcard.entity.SalesOrderHistory;
import com.transretail.crm.giftcard.entity.SalesOrderItem;
import com.transretail.crm.giftcard.entity.support.GiftCardInventoryStatus;
import com.transretail.crm.giftcard.entity.support.GiftCardSalesType;
import com.transretail.crm.giftcard.entity.support.ReturnStatus;
import com.transretail.crm.giftcard.entity.support.SalesOrderStatus;
import com.transretail.crm.giftcard.entity.support.SalesOrderType;
import com.transretail.crm.giftcard.repo.GiftCardInventoryRepo;
import com.transretail.crm.giftcard.repo.GiftCardOrderHistoryRepo;
import com.transretail.crm.giftcard.repo.GiftCardOrderRepo;
import com.transretail.crm.giftcard.repo.GiftCardTransactionRepo;
import com.transretail.crm.giftcard.repo.SalesOrderAllocRepo;
import com.transretail.crm.giftcard.repo.SalesOrderHistoryRepo;
import com.transretail.crm.giftcard.repo.SalesOrderRepo;
import com.transretail.crm.giftcard.service.GiftCardAccountingService;
import com.transretail.crm.giftcard.service.GiftCardInventoryStockService;
import com.transretail.crm.giftcard.service.GiftCardManager;
import com.transretail.crm.giftcard.service.ReturnByCardsService;
import com.transretail.crm.giftcard.service.SalesOrderDeptService;
import com.transretail.crm.giftcard.service.SalesOrderService;
import com.transretail.crm.giftcard.service.SoAllocService;
import com.transretail.crm.giftcard.service.SoApprovedTxnsService;

@Service("soPickupService")
public class SoApprovedTxnsServiceImpl implements SoApprovedTxnsService {

	private static final Logger logger = LoggerFactory.getLogger(SoApprovedTxnsServiceImpl.class);
	@Autowired
	private SalesOrderRepo salesOrderRepo;
	@Autowired
	private SalesOrderAllocRepo salesOrderAllocRepo;
	@Autowired
	private GiftCardInventoryRepo giftCardInventoryRepo;
	@Autowired
	private LookupDetailRepo lookupDetailRepo;
	@Autowired
	private ApprovalRemarkRepo approvalRemarkRepo;
	@Autowired
	private ApplicationConfigRepo applicationConfigRepo;
	@Autowired
	private SoAllocService soAllocService;
	@Autowired
	private GiftCardInventoryStockService giftCardInventoryStockService;
	@Autowired
	private CodePropertiesService codePropertiesService;
	@Autowired
	private GiftCardTransactionRepo giftCardTransactionRepo;
	@Autowired
	private SalesOrderService salesOrderService;
	@Autowired
	private GiftCardAccountingService accountingService;
	@Autowired
	private GiftCardManager giftCardManager;
	@PersistenceContext
	private EntityManager em;
	@Autowired
	private DbMetadataUtil dbMetadataUtil;
	@Autowired
	private SalesOrderDeptService deptService;
	@Autowired
	private ReturnByCardsService returnByCardsService;
	@Autowired
	private SalesOrderHistoryRepo soHistoryRepo;

	@Override
	@Transactional
	public SalesOrderDto updateStatus(Long id, SalesOrderStatus status) {
		if (null == id || null == status) {
			return null;
		}

		SalesOrder so = salesOrderRepo.findOne(id);
		so.setStatus(status);

		SalesOrderDto dto = new SalesOrderDto(salesOrderRepo.save(so), true);
		SalesOrderHistory soHistory = new SalesOrderHistory();
		soHistory.setOrder(so);
		soHistory.setCreated(new DateTime());
		soHistory.setStatus(status);
		soHistoryRepo.saveAndFlush(soHistory);
		return dto;
	}

	@Override
	public SalesOrderDto updateReceiptNo(Long id, String receiptNo) {
		if (null == id || StringUtils.isEmpty(receiptNo)) {
			return null;
		}

		SalesOrder so = salesOrderRepo.findOne(id);
		if (StringUtils.isNotBlank(so.getReceiptNo())) {
			return new SalesOrderDto(so, true);
		}
		so.setReceiptNo(receiptNo);
		return new SalesOrderDto(salesOrderRepo.save(so), true);
	}

	@Override
	@Transactional
	public SalesOrderDto activateB2bGcs(final Long soId) {
		if (null == soId) {
			return null;
		}

		final SalesOrder salesOrder = salesOrderRepo.findOne(soId);
		salesOrder.setActivationDate(new LocalDate());
		salesOrderRepo.saveAndFlush(salesOrder);

		SalesOrderType orderType = salesOrder.getOrderType();
		GiftCardSalesType salesType = GiftCardSalesType.getSalesType(orderType);
		boolean ryns = orderType.isRyns();

		long start = System.currentTimeMillis();

		QSalesOrderAlloc qSalesOrderAlloc = QSalesOrderAlloc.salesOrderAlloc;
		QGiftCardInventory qgc = QGiftCardInventory.giftCardInventory;
		QProductProfile qpp = new QProductProfile("a");

		QSalesOrderItem qItem = QSalesOrderItem.salesOrderItem;

		List<Long> prodIds = new JPAQuery(em).from(qItem).where(qItem.order.id.eq(soId)).distinct()
				.list(qItem.product.id);
		long affectedRows = 0;
		for (Long prodId : prodIds) {

			DateExpression<LocalDate> exp = new CaseBuilder().when(qgc.expiryDate.isNull())
					.then(addMonth(new JPASubQuery().from(qpp).where(qpp.id.eq(prodId)).unique(qpp.effectiveMonths)))
					.otherwise(qgc.expiryDate);

			affectedRows += new JPAUpdateClause(em, qgc).set(qgc.previousBalance, 0.0)
					.set(qgc.balance, qgc.faceValue.doubleValue())
					.set(qgc.activationDate, DateExpression.currentDate(LocalDate.class))
					.set(qgc.status, GiftCardInventoryStatus.ACTIVATED)
					.set(qgc.salesType, salesType)
					.set(qgc.ryns, ryns)
					.set(qgc.expiryDate, exp)
					// .where(qgc.in(new JPASubQuery().from(qSalesOrderAlloc)
					// .where(qSalesOrderAlloc.soItem.order.id.eq(soId).and(qSalesOrderAlloc.gcInventory.profile.id.eq(prodId)))
					// .list(qSalesOrderAlloc.gcInventory)))
					.where(new JPASubQuery().from(qSalesOrderAlloc)
							.where(qSalesOrderAlloc.soItem.order.id.eq(soId)
									.and(qgc.series.goe(qSalesOrderAlloc.seriesStart)
											.and(qgc.series.loe(qSalesOrderAlloc.seriesEnd)))
									.and(qgc.profile.id.eq(prodId)))
							.exists())
					.execute();

			em.flush();
		}

		logger.debug("{} total affected records upon B2B Activation and took {} ms", affectedRows,
				System.currentTimeMillis() - start);

		logger.debug("creating accounting entry for sales order {}", soId);
		saveAccounting(soId);

		if (TransactionSynchronizationManager.isSynchronizationActive()) {
			TransactionSynchronizationManager.registerSynchronization(new TransactionSynchronizationAdapter() {

				@Override
				public void afterCommit() {
					logger.debug("Transaction::afterCommit started.");
					List<GiftCardInventory> gcs = soAllocService.getAllocatedGcs(soId);
					logger.debug("Invoking activateB2b...running in separated thread.");
					giftCardManager.activateB2B(salesOrder, gcs);
					logger.debug("Updating stocks...running in separated thread.");
					giftCardInventoryStockService.saveStocks(gcs);
					logger.debug("Transaction::afterCommit executed.");
				}
			});
		}

		if (salesOrder.getOrderType().compareTo(SalesOrderType.REPLACEMENT) == 0
				&& StringUtils.isNotEmpty(salesOrder.getReturnNo()))
			returnByCardsService.updateByCardsRecord(salesOrder.getReturnNo(), ReturnStatus.REPLACED);

		return updateStatus(soId, SalesOrderStatus.SOLD);
	}

	private void saveAccounting(Long id) {
		SalesOrderDto orderDto = salesOrderService.getOrder(id);
		switch (orderDto.getOrderType()) {
		case B2B_SALES:
			accountingService.forB2BActivation(orderDto);
			break;
		case B2B_ADV_SALES:
			accountingService.forAdvB2BActivation(orderDto);
			break;
		case REPLACEMENT:
			accountingService.forB2BReplacementActivation(orderDto);
			break;
		case INTERNAL_ORDER:
			SalesOrderMultDeptAllocsDto allocsDto = deptService.getAllocations(id);
			DateTime txnDate = new DateTime();
			StringBuilder notes = new StringBuilder(orderDto.getOrderNo());
			if (orderDto.getCategory() != null) {
				notes.append(", ").append(orderDto.getCategory().getDescription());
			}
			for (SalesOrderDeptAllocDto allocDto : allocsDto.getDeptAllocs()) {
				accountingService.forB2BInternalSOActivation(txnDate, allocDto.getAllocAmount(), allocDto.getDeptId(),
						notes.toString());
			}
			break;
		case VOUCHER:
			accountingService.forFreeVoucherActivation(orderDto);
			break;
		case YEARLY_DISCOUNT:
			accountingService.forFreeVoucherActivation(orderDto);
			break;
		}
	}

	private DateExpression<LocalDate> addMonth(Expression<Long> effectivity) {
		final DbType dbType = dbMetadataUtil.getDbType();
		if (dbType == DbType.H2) {
			return DateTemplate.create(LocalDate.class, "DATEADD('MONTH',{0}, {1})", effectivity,
					DateExpression.currentDate(LocalDate.class));
		} else if (dbType == DbType.ORACLE) {
			return DateTemplate.create(LocalDate.class, "ADD_MONTHS({0}, {1})",
					DateExpression.currentDate(LocalDate.class), effectivity);
		} else if (dbType == DbType.MYSQL) {
			return DateTemplate.create(LocalDate.class, "ADD_MONTH({0}, {1})",
					DateExpression.currentDate(LocalDate.class), effectivity);
		}

		throw new UnsupportedOperationException("Database " + dbType + " is not yet supported.");
	}

	@Override
	public SalesOrderDto saveRemarks(ApprovalRemarkDto dto) {
		if (StringUtils.isBlank(dto.getModelId())) {
			return null;
		}

		ApprovalRemark model = dto.toModel();
		if (StringUtils.isNotBlank(dto.getStatus())) {
			model.setStatus(lookupDetailRepo.findByCode(dto.getStatus()));
		}
		model.setModelType(ModelType.SALES_ORDER.toString());
		approvalRemarkRepo.save(model);

		SalesOrderDto soDto = null;
		SalesOrder so = salesOrderRepo.findOne(Long.valueOf(dto.getModelId()));
		if (codePropertiesService.getDetailStatusCancelled().equalsIgnoreCase(dto.getStatus())) {
			soDto = cancelAllocations(so);
		}

		return soDto;
	}

	@Override
	public List<ApprovalRemark> getRemarks(Long soId) {
		if (null == soId) {
			return null;
		}
		return approvalRemarkRepo.findByModelTypeAndModelId(ModelType.SALES_ORDER.name(), soId.toString());
	}

	@Override
	public String generateInvoiceNo(Long soId) {
		if (null == soId) {
			return null;
		}
		SalesOrder so = salesOrderRepo.findOne(Long.valueOf(soId));
		if (StringUtils.isBlank(so.getReceiptNo())) {
			so.setReceiptNo(
					soId + /* LocalDateTime.now().toString( "YYMMddhhmmss" ) */ generateInvoiceNo());
			so = salesOrderRepo.save(so);
		}
		return so.getReceiptNo();
	}

	private SalesOrderDto cancelAllocations(SalesOrder so) {
		List<GiftCardInventory> gcs = soAllocService.getAllocatedGcs(so.getId());
		List<GiftCardInventory> activatedGcs = Lists.newArrayList();
		if (CollectionUtils.isNotEmpty(gcs)) {
			for (GiftCardInventory gc : gcs) {
				gc.setPreviousBalance(null);
				gc.setBalance(null);
				gc.setActivationDate(null);
				if (gc.getStatus() == GiftCardInventoryStatus.ACTIVATED) {
					activatedGcs.add(gc);
				}
				gc.setStatus(GiftCardInventoryStatus.IN_STOCK);
				gc.setExpiryDate(null);
			}
			giftCardInventoryRepo.save(gcs);
			giftCardInventoryStockService.saveStocks(activatedGcs);
		}

		Set<SalesOrderItem> soItems = so.getItems();
		if (CollectionUtils.isNotEmpty(soItems)) {
			List<Long> soItemIds = Lists.newArrayList();
			for (SalesOrderItem soItem : soItems) {
				soItemIds.add(soItem.getId());
			}
			salesOrderAllocRepo
					.delete(salesOrderAllocRepo.findAll(QSalesOrderAlloc.salesOrderAlloc.soItem.id.in(soItemIds)));
		}

		return updateStatus(so.getId(), SalesOrderStatus.CANCELLED);
	}

	private String generateInvoiceNo() {
		String dt = LocalDate.now().toString("YYMMdd");
		final String receiptCharPad = "0";
		final int receiptCharCount = 5;

		ApplicationConfig appConfig = applicationConfigRepo.findByKey(AppKey.GCSO_RECEIPT_COUNT);
		String appConfigVal = (appConfig.getValue().length() > dt.length())
				? appConfig.getValue().substring(dt.length()) : appConfig.getValue();
		if (appConfig.getValue().length() < dt.length()
				|| !dt.equalsIgnoreCase(appConfig.getValue().substring(0, dt.length()))) {
			appConfigVal = receiptCharPad;
		}
		int receiptCount = Integer.parseInt(appConfigVal);
		String receiptNo = dt + StringUtils.leftPad(Integer.toString(receiptCount), receiptCharCount, receiptCharPad);

		appConfig
				.setValue(dt + StringUtils.leftPad(Integer.toString(++receiptCount), receiptCharCount, receiptCharPad));
		applicationConfigRepo.save(appConfig);

		return receiptNo;
	}

}
