package com.transretail.crm.core.service;

import com.transretail.crm.core.entity.StampTxnModel;

import java.util.List;

/**
 * @author Mike de Guzman
 */
public interface StampService {

    List<StampTxnModel> retrieveStampsByTransactionNo(String transactionNo);

    StampTxnModel saveStamp(StampTxnModel model);

}
