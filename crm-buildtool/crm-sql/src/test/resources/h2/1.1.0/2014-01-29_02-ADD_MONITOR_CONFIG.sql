create table CRM_MONITOR_CONFIG (
    ID bigint not null,
    TRANSACTION_COUNT integer,
    MEMBER_TYPE varchar(15),
    primary key (ID)
);

alter table CRM_MONITOR_CONFIG 
    add constraint FK_4ol2o7u5ovx7efxql4oqtdd4n 
    foreign key (MEMBER_TYPE) 
    references CRM_REF_LOOKUP_DTL;

INSERT INTO CRM_MONITOR_CONFIG(ID, TRANSACTION_COUNT, MEMBER_TYPE) VALUES (0, 3, 'MTYP001');
INSERT INTO CRM_MONITOR_CONFIG(ID, TRANSACTION_COUNT, MEMBER_TYPE) VALUES (1, 3, 'MTYP002');