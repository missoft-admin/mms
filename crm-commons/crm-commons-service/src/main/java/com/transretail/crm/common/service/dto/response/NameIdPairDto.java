package com.transretail.crm.common.service.dto.response;

/**
 *
 */
public class NameIdPairDto {
    private String id;
    private String name;

    public NameIdPairDto() {

    }

    public NameIdPairDto(String id, String name) {
        this.id = id;
        this.name = name;
    }

    public Object getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
