package com.transretail.crm.core.security.service.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.google.common.collect.Sets;
import com.mysema.query.JoinType;
import com.mysema.query.jpa.impl.JPAQuery;
import com.transretail.crm.common.repo.JoinDef;
import com.transretail.crm.common.security.service.AbstractSecurityUserService;
import com.transretail.crm.core.entity.QUserPermission;
import com.transretail.crm.core.entity.QUserRoleModel;
import com.transretail.crm.core.entity.QUserRolePermission;
import com.transretail.crm.core.entity.UserRolePermission;
import com.transretail.crm.core.entity.UserModel;
import com.transretail.crm.core.entity.UserRoleModel;
import com.transretail.crm.core.entity.enums.Status;
import com.transretail.crm.core.entity.lookup.Store;
import com.transretail.crm.core.repo.ApprovalMatrixRepo;
import com.transretail.crm.core.repo.StoreRepo;
import com.transretail.crm.core.repo.UserRepo;
import com.transretail.crm.core.repo.UserRolePermissionRepo;
import com.transretail.crm.core.security.model.CustomSecurityRoleGrantedAuthority;
import com.transretail.crm.core.security.model.CustomSecurityUserDetails;
import com.transretail.crm.core.security.model.impl.CustomSecurityUserDetailsImpl;
import com.transretail.crm.core.security.service.CustomSecurityUserService;
import com.transretail.crm.core.service.CodePropertiesService;
import com.transretail.crm.core.service.LookupService;

/**
 * @author mhua
 */
@Service("customSecurityUserService")
public class CustomSecurityUserServiceImpl extends AbstractSecurityUserService<CustomSecurityUserDetails>
    implements CustomSecurityUserService {

    private static final Logger _LOG = LoggerFactory.getLogger(CustomSecurityUserServiceImpl.class);
    @PersistenceContext
    private EntityManager em;
    @Autowired
    private UserRepo userRepo;
    @Autowired
    private StoreRepo storeRepo;
    @Autowired
    ApprovalMatrixRepo approvalMatrixRepo;
    @Autowired
    CodePropertiesService codePropertiesService;
    @Autowired
    LookupService lookupService;
    @Autowired
    private UserRolePermissionRepo userRolePermissionRepo;

    @Override
    @Transactional(readOnly = true)
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {

        _LOG.debug("Attempting to load user by username: username=[{}]", username);

        CustomSecurityUserDetailsImpl userDetails;

//        UserCredentialModel userCredentialModel = userCredentialRepo.loadUserCredential(username);
        UserModel userModel = userRepo.findByUsername(username);

        if (userModel == null) {
        	return null;
            //throw new UsernameNotFoundException(String.format("User with username [%s] not found.", username));
        }
//
        userDetails = new CustomSecurityUserDetailsImpl();
        userDetails.setUserId(userModel.getId());
        userDetails.setUsername(userModel.getUsername());
        userDetails.setPassword(userModel.getPassword());
        userDetails.setEnabled(userModel.getEnabled());
        userDetails.setAuthenticationType(userModel.getAuthenticationType());
//        userDetails.setUserType(userCredentialModel.getUserType());
//        userDetails.setLandingPages(getLandingPages(userCredentialModel));

        setRolesAndPermissions(userDetails, userModel);

        userDetails.setStoreCode(userModel.getStoreCode());
        Store store = storeRepo.findByCode( userModel.getStoreCode());
        userDetails.setStore(store);
        userDetails.setInventoryLocation(userModel.getInventoryLocation());
        
        if(StringUtils.isNotBlank(userModel.getInventoryLocation())) {
	        if(userModel.getInventoryLocation().equalsIgnoreCase(userModel.getStoreCode()))
	        	userDetails.setInventoryLocationName(store.getName());
	        else
	        	userDetails.setInventoryLocationName(lookupService.getDetailByCode(codePropertiesService.getDetailInvLocationHeadOffice()).getDescription());
        }
        
        userDetails.setB2bLocation(userModel.getB2bLocation());
        
        StringBuilder fullName = new StringBuilder();
        fullName.append(userModel.getFirstName() != null ? userModel.getFirstName() : "").append(" ");
        fullName.append(userModel.getLastName() != null ? userModel.getLastName() : "");
        userDetails.setFullName(fullName.toString());

        return userDetails;
    }

    @Override
    public CustomSecurityUserDetails getCurrentUser() {
        //TODO
        return null;
    }

    private void setRolesAndPermissions(CustomSecurityUserDetailsImpl user, UserModel model) {
        Set<UserRoleModel> userRoleModels = Sets.newHashSet();
        Set<GrantedAuthority> authorities = Sets.newHashSet();
        Set<String> userPermissions = Sets.newHashSet();

        QUserRolePermission qUserRolePermission = QUserRolePermission.userRolePermission;
        QUserRoleModel qUserRoleModel = QUserRoleModel.userRoleModel;
        QUserPermission qUserPermission = QUserPermission.userPermission;

        List<UserRolePermission> rolePermissions = new JPAQuery(em).from(qUserRolePermission)
            .innerJoin(qUserRolePermission.roleId, qUserRoleModel)
            .leftJoin(qUserRolePermission.permId, qUserPermission)
            .where(
                qUserRolePermission.userId.eq(model)
                    .and(qUserRoleModel.enabled.isTrue())
                    .and(
                        qUserPermission.isNull()
                            .or(qUserPermission.status.eq(Status.ACTIVE))
                    )
            ).list(qUserRolePermission);

        for (UserRolePermission rolePermission : rolePermissions) {
            String roleCode = rolePermission.getRoleId().getCode();
            userRoleModels.add(new UserRoleModel(roleCode));
            authorities.add(new SimpleGrantedAuthority(roleCode));

            if (rolePermission.getPermId() != null) {
                String permCode = rolePermission.getPermId().getCode();
                userPermissions.add(permCode);
                // Can be used in url checking in applicationContext-security.xml
                String rolePermissionCode = roleCode + "_" + permCode;
                authorities.add(new SimpleGrantedAuthority(rolePermissionCode));
                userPermissions.add(rolePermissionCode);
            }
        }

        user.setRoles(userRoleModels);
        user.setAuthorities(authorities);
        user.setPermissions(userPermissions);
    }

    @Override
	public Authentication getCurrentAuth() {
		return SecurityContextHolder.getContext().getAuthentication();
	}

	@Override
	public List<String> getUserAuthorities() {

		List<String> theAuths = new ArrayList<String>();

		for ( GrantedAuthority theAuth: getCurrentAuth().getAuthorities() ) {
			theAuths.add( theAuth.getAuthority() );
		}

		return theAuths;
	}

}
