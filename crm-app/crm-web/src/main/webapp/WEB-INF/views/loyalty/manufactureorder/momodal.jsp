<%@ include file="../../common/taglibs.jsp" %>

<style type="text/css">
<!--

  #addProductBtn.btn-small {
    margin-bottom: 5px;
  }
  table select, textarea, table input[type="text"], input[type="password"], input[type="datetime"], input[type="datetime-local"], input[type="date"], input[type="month"], input[type="time"], input[type="week"], input[type="number"], input[type="email"], input[type="url"], input[type="search"], input[type="tel"], input[type="color"], .uneditable-input {
      margin-bottom: 0px;
      margin-top: 0px;
  }
  
-->
</style>



<c:set var="ENCTYPE" value="application/x-www-form-urlencoded"/>

<div class="modal-dialog">
  <div class="modal-content">
    <spring:message code="label_manufacture_order" var="typeName" />
    <div class="modal-header">
      <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
      <h4 class="modal-title"><spring:message code="global_menu_new" arguments="${typeName}"/></h4>
    </div>
 
    <div class="modal-body">
    
      <form:form id="manufactureOrder" name="manufactureOrder" modelAttribute="manufactureOrder" action="${pageContext.request.contextPath}/manufactureorder/save/" method="POST" enctype="${ENCTYPE}" cssClass="form-reset" >
      

      
      <div id="contentError" class="hide alert alert-error">
        <button type="button" class="close" onclick="$(this).parent().hide();">&times;</button>
        <div><form:errors path="*"/></div>
      </div>
      
      <div class="contentMain">
        <div class="row-fluid" style="margin-bottom: 10px">
          
          
          <div class="span4">
            <spring:message code="mo_prop_pono" var="poNumber" />
            <label for=""><b class="required">*</b> ${poNumber}</label>
            <form:input path="poNo" placeholder="${poNumber}" />
          </div>
          
          <div class="span4">
            <spring:message code="mo_prop_orderdate" var="orderDate" />
            <label for=""><b class="required">*</b> ${orderDate}</label>
            <form:input cssClass="moDate" path="orderDate" placeholder="${orderDate}" />
          </div>
          
          <div class="span4">
            <spring:message code="mo_prop_supplier" var="supplier" />
            <label for="">${supplier}</label>
            <form:select path="supplier" items="${vendors.results}" itemLabel="name" itemValue="id" />
          </div>
          
        </div>
        <div class="row-fluid" style="margin-bottom: 20px">
        
          <div class="span4">
            <spring:message code="mo_prop_mono" var="moNumber" />
            <label for="">${moNumber}</label>
            <form:input path="moNo" placeholder="${moNumber}" readonly="true" />
          </div>
          
          <div class="span4">
            <spring:message code="mo_prop_modate" var="moDate" />
            <label for="">${moDate}</label>
            <form:input cssClass="moDate" path="moDate" placeholder="${moDate}" />
          </div>
          
          <c:if test="${isApprover && manufactureOrder.status.code == forApprovalStatus}">
          <div class="span4">
            <spring:message code="mo_prop_reason" var="reason" />
            <label for="">${reason}</label>
            <form:textarea path="reason" placeholder="${reason}" />
          </div>
          </c:if>
        </div>
        
        <c:choose>
        <c:when test="${!isApprover || manufactureOrder.status.code != forApprovalStatus}">
          <c:set var="forApproval" value="false" />
        </c:when>
        <c:otherwise>
          <c:set var="forApproval" value="true" />
        </c:otherwise>
        </c:choose>
        
        <c:if test="${!forApproval}">
        <div class="pull-left" style="margin-right: 5px;">
          <c:if test="${!empty companies && !empty products}">
            <button type="button" id="addProductBtn" class="btn btn-small">+</button><br/>
            <button type="button" id="removeProductBtn" class="btn btn-small">&ndash;</button>
          </c:if>
        </div>
        </c:if>

        <%----%>

        <div <c:if test="${!forApproval}">class="pull-left" style="width:90%;"</c:if>>
          <div id="companyCardTypeStartSeriesContainer">
            <c:forEach var="map" items="${manufactureOrder.companyCardTypeStartSeriesMap}">
              <input id="${map.key}" type="hidden" value="${map.value}"/>
            </c:forEach>
          </div>
          <table id="productTable" class="table table-condensed table-compressed table-striped">
            <thead>
              <tr>
                <c:if test="${!forApproval}"><th></th></c:if>
                <th><b class="required">*</b> <spring:message code="mo_tier_company" /></th>
                <th><b class="required">*</b> <spring:message code="mo_tier_prop_name" /></th>
                <th><spring:message code="mo_tier_prop_batchno" /></th>
                <th><spring:message code="mo_tier_prop_quantity" /></th>
                <th><spring:message code="mo_tier_prop_startingseries" /></th>
                <th><spring:message code="mo_tier_prop_endingseries" /></th>
              </tr>
            </thead>
            <tbody>
            <c:choose>
              <c:when test="${empty companies || empty products}">
                <tr>
                  <td colspan="7"><spring:message code="loyalty_no_lookups" /></td>
                </tr>
              </c:when>
              <c:otherwise>
                <c:forEach var="item" items="${manufactureOrder.tiers}" varStatus="status" >
                  <tr data-index="${status.index}">
                    <c:if test="${!forApproval}"><td><input type="checkbox" class="productRemoveFlag" /></td></c:if>
                    <td>
                        <%--Feature #83978--%>
                      <c:choose>
                        <c:when test="${!forApproval}">
                          <select class="input-medium companyCode" name="tiers[${status.index}].companyCode">
                            <c:forEach items="${companies}" var="company" >
                              <option value="${company.code}" <c:if test="${company.code == item.companyCode}">selected</c:if>>${company.desc}</option>
                            </c:forEach>
                          </select>
                        </c:when>
                        <c:otherwise>
                          <input type="text" class="input-medium" value="${item.companyDesc}" readonly="readonly" />
                          <input type="hidden" name="tiers[${status.index}].companyCode" value="${item.companyCode}" />
                        </c:otherwise>
                      </c:choose>
                    </td>
                    <td>
                      <c:choose>
                        <c:when test="${!forApproval}">
                          <select class="input-medium cardType" name="tiers[${status.index}].name">
                            <c:forEach items="${products}" var="product" >
                              <c:choose>
                                <c:when test="${product.code == item.name.code}">
                                  <c:set var="selected" value="selected" />
                                </c:when>
                                <c:otherwise>
                                  <c:set var="selected" value="" />
                                </c:otherwise>
                              </c:choose>
                              <option value="${product.code}" ${selected} >${product.description}</option>
                            </c:forEach>
                          </select>
                        </c:when>
                        <c:otherwise>
                          <input type="text" class="input-medium" value="${item.name.description}" readonly="readonly" />
                          <input type="hidden" name="tiers[${status.index}].name" value="${item.name.code}" />
                        </c:otherwise>
                      </c:choose>

                    </td>
                    <td><input type="text" class="input-small" readonly="readonly" value="${item.batchNo}" name="tiers[${status.index}].batchNo" /></td>
                    <td><input type="text" class="input-mini quantity modalQuantity numeric" <c:if test="${forApproval}">readonly="readonly"</c:if> value="${item.quantity}" name="tiers[${status.index}].quantity" /></td>
                      <%--Feature #83978--%>
                      <%--<td><input type="text" class="input-medium modalStartingSeries numeric" <c:if test="${forApproval}">readonly="readonly"</c:if> value="${item.startingSeries}" name="tiers[${status.index}].startingSeries" maxlength="12" /></td>--%>
                      <%--<td><input type="text" class="input-medium modalEndingSeries numeric" <c:if test="${forApproval}">readonly="readonly"</c:if> value="" name="tiers[${status.index}].endingSeries" maxlength="12" /></td>--%>
                    <td><input type="text" class="input-medium startingSeries modalStartingSeries" readonly="readonly" value="${item.startingSeries}" name="tiers[${status.index}].startingSeries" maxlength="12" /></td>
                    <td><input type="text" class="input-medium endingSeries modalEndingSeries" readonly="readonly" value="${item.endingSeries}" name="tiers[${status.index}].endingSeries" maxlength="12" /></td>
                  </tr>
                </c:forEach>
              </c:otherwise>
            </c:choose>
            </tbody>
          </table>
        </div>
        
        
        
        <div class="clearFix"></div>
          
      </div>
      
      </form:form>
    
    </div>
    <div class="modal-footer" id="moOrderCtrlBtns">
    <c:if test="${!empty companies && !empty products}">
      <c:if test="${ manufactureOrder.status.code != forApprovalStatus }">
      <button type="button" id="" data-status="${draftStatus}" data-loading-text="<spring:message code="loading_text" />" data-mo-id="${manufactureOrder.id}" class="btn manufactureOrderSave"><spring:message code="label_draft" /></button>
      <button type="button" id="" data-status="${forApprovalStatus}" data-loading-text="<spring:message code="loading_text" />" data-mo-id="${manufactureOrder.id}" class="btn btn-primary manufactureOrderSave"><spring:message code="label_submit_for_approval" /></button>
      </c:if>
      
      <c:if test="${isApprover && manufactureOrder.status.code == forApprovalStatus}">
      <button type="button" id="" data-status="${draftStatus}" data-loading-text="<spring:message code="loading_text" />" data-mo-id="${manufactureOrder.id}" class="btn btn-primary manufactureOrderSave"><spring:message code="label_reject" /></button>
      <button type="button" id="" data-status="${barcodingStatus}" data-loading-text="<spring:message code="loading_text" />" data-mo-id="${manufactureOrder.id}" class="btn btn-primary manufactureOrderSave"><spring:message code="label_approve" /></button>
      </c:if>

    </c:if>
      <button type="button" id="" class="btn" data-dismiss="modal"><spring:message code="label_cancel"/></button>
    </div>
  </div>
</div>