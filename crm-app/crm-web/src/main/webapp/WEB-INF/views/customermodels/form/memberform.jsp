<%@ include file="../../common/taglibs.jsp"%>

<style type="text/css">
<!--
  .modal-fullscreen { left: 1px; bottom: 0px; top: 0% !important; width: 100%; margin-left: 0% !important; height: 95%; }
  .modal-fullscreen .modal-dialog { position: relative !important; }
  .modal-fullscreen .modal-body { position: absolute; width: 94%; top: 50px; bottom: 60px; }
  .modal-fullscreen .modal-footer { position: absolute !important; bottom: 0px; margin-bottom: 0% !important; width: 100%; }
  .form-horizontal .radio.controls, .form-horizontal .checkbox.controls { margin-left: 0; }
  .span12 { width: 91%; top: 8% !important; }
  .span12 .modal-body {}
  .control-group { margin-top: 10px !important; margin-bottom: 10px !important; }
  .control-group .controls { margin-right: 5px !important; }
  .checkbox .control-label { text-align: left !important; margin-left: 5px; text-transform: capitalize !important; }
  .required { font-size: inherit !important; }
  .checkbox { clear: both; }
  .space-top-01 { padding-top: 20px; }
  .input-mini { width: 40px; }
  #content_print .modal-body, #updatePinDialog .modal-body {height: auto !important;}
  #correspondencePane .controls {margin-left: 0;}
-->
</style>


<div id="memberFormDialog" class="modal hide nofly span12 row-fluid">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
			  <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
			  <h4 class="isPrimary"><spring:message code="label_member_primary" /></h4>
				<h4><span id="isSupplement"><spring:message code="label_member_supplement" /></span><span id="parent"></span></h4>
			</div>

			<div class="modal-body ">

    		  <!-- Tabs -->
    			<ul id="tabs" class="nav nav-tabs mb20">
            <li class="" id="firstTab">
              <a href="#mandatoryPane" data-toggle="pill"><spring:message code="member_lbl_mandatoryinfo" /></a>
            </li>
    				<li class="memberDetails">
              <a href="#profilePane" data-toggle="pill" class="memberDetails"><spring:message code="label_member_datapersonal" /></a>
    					<a href="#profilePane" data-toggle="pill" class="professionalDetails" ><spring:message code="label_member_dataprofessional" /></a>
            </li>
    				<li class="memberDetails">
              <a href="#workPane" data-toggle="pill"><spring:message code="label_member_datawork" /></a>
            </li>
            <li class="memberDetails">
              <a href="#correspondencePane" data-toggle="pill"><spring:message code="label_member_correspondence" /></a>
            </li>
            <li class="marketingPane">
              <a href="#marketingPane" data-toggle="pill"><spring:message code="label_member_marketinginfo" /></a>
            </li>
    				<li class="otherPane">
              <a href="#otherPane" data-toggle="pill"><spring:message code="label_member_dataothers" /></a>
            </li>
    				<li class="professionalDetails">
              <a href="#addParamPane" data-toggle="pill"><spring:message code="label_member_addparams" /></a>
            </li>
            <li>
              <a href="#mainPane" data-toggle="pill"><spring:message code="label_member_account" /></a>
            </li>
    				<c:if test="${not isEmployee}">
    				<li class="isPrimary">
              <a href="#cardPane" data-toggle="pill"><spring:message code="label_member_supplementaryaccounts" /></a>
            </li>
    				</c:if>
    			</ul>

			    <c:url var="url_action" value="/customermodels" />
          <c:if test="${isEmployee}"><c:url var="url_action" value="/employeemodels" /></c:if>
          <form:form id="memberForm" modelAttribute="customerModel" action="${url_action}" data-action="${url_action}">
		        <div class="tab-content control-group-01">
		          <!-- Errors -->
		          <div class="alert alert-error errors">
		            <button type="button" class="close" onclick="hideErrors();">&times;</button>
		            <div class="errorcontent"></div>
		          </div>

              <!-- Mandatory Pane -->
              <div id="mandatoryPane" class="tab-pane"><jsp:include page="mandatoryInfo.jsp" /></div>
		          <!-- Main Pane -->
		          <div id="mainPane" class="tab-pane"><jsp:include page="mainpane.jsp" /></div>
		          <!-- Personal Date -->
		          <div id="profilePane" class="tab-pane active"><jsp:include page="profilepane.jsp" /></div>
		          <!-- Occupation Details -->
		          <div id="workPane" class="tab-pane"><jsp:include page="workpane.jsp" /></div>
              <!-- Correspondence -->
              <div id="correspondencePane" class="tab-pane"><jsp:include page="correspondencepane.jsp" /></div>
		          <!-- Other Pane -->
		          <div id="otherPane" class="tab-pane"><jsp:include page="otherdatapane.jsp" /></div>
              <!-- Marketing Pane -->
              <div id="marketingPane" class="tab-pane"><jsp:include page="marketinginfopane.jsp" /></div>
		          <!-- Npwp Pane -->
		          <div id="cardPane" class="tab-pane"><jsp:include page="supplementpane.jsp" /></div>
		          <!-- Additional Parameters Pane -->
		          <div id="addParamPane" class="tab-pane"><jsp:include page="additionalParamsPane.jsp" /></div>
		        </div>
         </form:form>
			</div>

			<div class="modal-footer container-btn">
        <sec:authorize ifNotGranted="ROLE_HELPDESK">
          <!--<input type="button" class="btn btn-primary" id="save_form" value="Save" />-->
          <button type="saveButton" id="save_form" class="btn btn-primary"><spring:message code="label_save" /></button>
        </sec:authorize>
        <button type="button" id="formCancel" data-dismiss="modal" class="btn btn-default"><spring:message code="label_cancel"/></button>
			</div>
		</div>
	</div>
</div>


<link href="<c:url value="/css/bootstrap/datepicker.css" />" rel="stylesheet" />
<link href="<c:url value="/css/bootstrap/bootstrap-timepicker.min.css" />" rel="stylesheet" />
<link href="<c:url value="/styles/temp.css" />" rel="stylesheet" />
<script src='<c:url value="/js/bootstrap/bootstrap-timepicker.min.js"/>' type='text/javascript'><![CDATA[&nbsp; ]]></script>
<script src='<c:url value="/js/bootstrap/bootstrap-datepicker.js"/>' type='text/javascript'><![CDATA[&nbsp; ]]></script>
<script src="<spring:url value="/js/jquery/jquery.numberInput.js" />" ></script>
<script src="<c:url value="/js/viewspecific/customermodel/memberform.js" />" ></script>
<script src="<c:url value="/js/viewspecific/customermodel/configurableFields.js" />" ></script>

<script>
var customerSegmentation = new Object();

<c:forEach items="${customerSegmentation}" var="segmentMap">
	var map = new Object();
	<c:forEach items="${segmentMap.value}" var="details">
		map["${details.code}"] = "${details.description}";
	</c:forEach>
	customerSegmentation["${segmentMap.key}"] = map;
</c:forEach>

setCustomerSegmentation(customerSegmentation);



var provinceMap = new Object();
<c:forEach items="${provinces}" var="province">
var cities = [];
<c:forEach items="${province.cities}" var="city">
cities.push("${city.name}");
</c:forEach>
provinceMap["${province.name}"] = cities;
</c:forEach>

setProvinces(provinceMap);

</script>