package com.transretail.crm.giftcard.dto;

import java.util.List;

public class ReserveGcDto {
	private List<StockRequestReceiveDto> gcList;
	private Long stockRequestId;
	private int startIndex = 0;
	
	public ReserveGcDto() {
		
	}
	
	public ReserveGcDto(List<StockRequestReceiveDto> gcList, Long stockRequestId) {
		this.gcList = gcList;
		this.stockRequestId = stockRequestId;
		this.startIndex = gcList.size();
	}

	public List<StockRequestReceiveDto> getGcList() {
		return gcList;
	}

	public void setGcList(List<StockRequestReceiveDto> gcList) {
		this.gcList = gcList;
	}

	public Long getStockRequestId() {
		return stockRequestId;
	}

	public void setStockRequestId(Long stockRequestId) {
		this.stockRequestId = stockRequestId;
	}

	public int getStartIndex() {
		return startIndex;
	}

	public void setStartIndex(int startIndex) {
		this.startIndex = startIndex;
	}
}
