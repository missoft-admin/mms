package com.transretail.crm.core.repo;

import org.springframework.stereotype.Repository;

import com.transretail.crm.common.repo.CrmQueryDslPredicateExecutor;
import com.transretail.crm.core.entity.lookup.LookupHeader;

@Repository
public interface LookupHeaderRepo extends CrmQueryDslPredicateExecutor<LookupHeader, String> {

	LookupHeader findByDescription(String headerName);
	LookupHeader findByCode(String code);

}