$(document).ready(function() {
	var CONTAINER_ERROR = "#contentError > div";
	var CONTENT_ERROR = "#contentError";
	
	$("#sourceLocationMain").change(function() {
		var sourceLocation = $(this).val();
		$(".sourceLocation").val(sourceLocation);
	});

	$(".allocateTo").change(allocateToChange);
	$('.productCode').change(productChange);
	
	// trigger for the first line
	$(".allocateTo").change();
	$(".requestDate").val($("#requestDateMain").val());
	$(".sourceLocation").val($("#sourceLocationMain").val());
	$(".requestNo").val($("#requestNoMain").val());

	$("#requestContainer").on("click", "#addRowButton", function(e) {
		$productTbody = $("#requestTable > tbody");
		$tr = $productTbody.find("tr:first-child"); 
		if($tr.css('visibility') == 'hidden' || $tr.css('display') == 'none') {
			$tr.find("input").val("").prop('disabled', false);
			$tr.find("select").val("").prop('disabled', false);
			$tr.visible();
		} else {
			var index = $productTbody.find("tr").length;
			$row = $tr.clone();
			$row.find("input").val("").prop('disabled', false);
			$row.find("select").val("").prop('disabled', false);
			$row.find(".rowDelete").css('visibility', 'visible');
			var requestNo = $("#requestNoMain").val();
			$row.find(".requestNo").val(requestNo);
			$row.find(".requestDate").val($("#requestDateMain").val());
			$row.find(".sourceLocation").val($("#sourceLocationMain").val());
			var $allocateTo = $row.find(".allocateTo");
			var inventoryLocation = $("#inventoryLocation").val();
			if(inventoryLocation) {
				$allocateTo.val(inventoryLocation);
				$row.find(".transferRefNo").val(requestNo + inventoryLocation);
			}
			else {
				$row.find(".transferRefNo").val(requestNo + $allocateTo.val());
			}

			var rg = new RegExp("\\[0\\]", 'g');
			var html = $row.html();
			html = html.replace(rg, '['+index+']');
			$row.html(html);
			
			$productTbody.append($row);
			$row.find(".productCode").val('');
			$row.on('change', '.allocateTo', allocateToChange);
			$row.on('change', '.productCode', productChange);
			$allocateTo.change();
		}
	}).on("click", ".rowDelete", function(e) {
		e.preventDefault();
		$tr = $(this).closest("tr"); 
		if($tr.is(":first-child")) {
			$tr.find("input").val("");
			$tr.find("select").val("");
		} else {
			$tr.remove();
		}
			
	});

	$("#stockRequestSave").unbind("click").click(function(e) {
		e.preventDefault();
		$(this).prop( "disabled", true );
		$form = $("form#stockrequest");
		ajaxSubmit($form.attr("action") + "/new", $form.serialize(), function() {
			location.reload();
		});
//		$(this).prop( "disabled", false );
	}); 
	
	$(".approve").unbind("click").click(function(e) {
		e.preventDefault();
		$form = $("form#stockrequest");
		ajaxSubmit($form.attr("action") + $(this).data("status"), $form.serialize(), function() {
			location.reload();
		});
	});

	function ajaxSubmit(action, data, fn) {
		$.ajax({
			type 	: "POST",
			url 	: action,
			dataType: 'json',
			data 	: data,
			        
			success : function(inResponse) {
				if (inResponse.success) {
					fn(inResponse);
				} 
				else {
					errorInfo = "";
					console.log(inResponse.result);
					for (i = 0; i < inResponse.result.length; i++) {
						errorInfo += "<br>" + (i + 1) + ". "
								+ ( inResponse.result[i].code != undefined ? 
										inResponse.result[i].code : inResponse.result[i]);
					}
					$( CONTAINER_ERROR ).html("Please correct following errors: " + errorInfo);
					$( CONTENT_ERROR ).show('slow');
				}
			},
			error : function(jqXHR, textStatus, errorThrown) {
				$( CONTENT_ERROR ).html("Error: " + errorThrown);
			}
		});
	}
	
	function allocateToChange() {
		var val = $(this).val();
		
		$tr = $(this).closest("tr");
		var productCode = $tr.find(".productCode").val();
		console.log(val);
		console.log($(this).data('prev'));
		if($(this).data('prev')) {
			var i = allocateList[$(this).data('prev')].indexOf(productCode);
			if (i >= 0) {
				allocateList[$(this).data('prev')].splice(i, 1);
			}
		}
		var index = productCode && val ? allocateList[val].indexOf(productCode) : -1;
		
		if(index < 0) {
			if(val && productCode) {
				allocateList[val].push(productCode);
			}
			$(this).data('prev', val);
			$tr.find(".transferRefNo").val($("#requestNoMain").val() + val);
		}
		else {
			$(this).val("");
			alert(duplicateError);
		}
	}
	
	function productChange() {
		var val = $(this).val();
		$tr = $(this).closest("tr");
		var allocate = $tr.find('.allocateTo').val();
		if($(this).data('prev')) {
			var i = allocateList[allocate].indexOf($(this).data('prev'));
			if(i >= 0) {
				allocateList[allocate].splice(i, 1);
			}
		}
		
		var index = val && allocate ? allocateList[allocate].indexOf(val) : -1;
		
		if(index < 0) {
			if(allocate && val) {
				allocateList[allocate].push(val);
			}
			$(this).data('prev', val);
		}
		else {
			$(this).val('');
			alert(duplicateError);
		}
	}
});