package com.transretail.crm.core.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.transretail.crm.core.entity.lookup.LookupDetail;

@Entity
@Table(name="CRM_MONITOR_CONFIG")
public class MemberMonitorConfig {
	@Id
	@Column(name="ID")
	private Long id;
	
	@JoinColumn(name="MEMBER_TYPE")
	@OneToOne
	private LookupDetail memberType;
	
	@Column(name="TRANSACTION_COUNT")
	private Integer transactionCount;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public LookupDetail getMemberType() {
		return memberType;
	}

	public void setMemberType(LookupDetail memberType) {
		this.memberType = memberType;
	}

	public Integer getTransactionCount() {
		return transactionCount;
	}

	public void setTransactionCount(Integer transactionCount) {
		this.transactionCount = transactionCount;
	}
}
