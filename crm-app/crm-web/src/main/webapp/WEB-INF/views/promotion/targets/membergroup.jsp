<%@ include file="../../common/taglibs.jsp" %>

<style type="text/css">
<!--
  .cont-02 input { margin-bottom: 3px; } 
  label .link { font-weight: normal !important; }
-->
</style>



<spring:message code="operand_tooltip_label_info" var="info" />
<spring:message code="operand_tooltip_member_group" var="tooltip" />

<div class="modal hide  nofly" data-context-path="${pageContext.request.contextPath}" id="targetMemberGroupsDialog" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4>Add <spring:message code="target_member_group" /></h4>
      </div>  
      <div class="modal-body">
      
        <div class="mb10">
          <a id="createMemberGrpLnk" class="btn" href="${pageContext.request.contextPath}/groups/member/dialog"><spring:message code="label_create" /> <spring:message code="label_group_membergroup"/></a><a id="link_viewMemberGroup" class="hide" data-url-view="<c:url value="/groups/member/view/" />">, <spring:message code="label_view" /></a>
        </div>
        <div>
        	<div class="pull-left" style="width: 47%;">
        		<div style="width: 100%"><label for=""><spring:message code="label_group_membergroup"/></label></div>
        		<div style="width: 100%">
          		<select multiple="multiple" name="memberGroups" id="selectMemberGroups" style="width:100%;">
          		</select>
        		</div>
        	</div>
        	<div class="">
	       		<div class="cont-02" style="margin-top: 25px;">
	       		<input type="button" id="addMemberGrp" class="btn" value=">>"/>
	          <input type="button" id="removeMemberGrp" class="btn" value="<<"/>
	       		</div>
        	</div>
        	<div class="pull-left" style="width: 40%">
        		<div style="width: 100%"><label for=""><spring:message code="label_group_selection"/></label></div>
        		<div style="width: 100%">
          		<select multiple="multiple" class="items" name="targetMemberGroups" id="selectedMemberGroups" style="width:100%;">
          		</select>
        		</div>
        	</div>
        </div>
        <div id="viewMemberGroup"></div>
      </div>
      <div class="modal-footer container-btn">
        <button type="button" id="memberGroupSaveButton" class="btn btn-primary"><spring:message code="label_save"/></button>
        <button type="button" id="memberGroupCancelButton" class="btn " data-dismiss="modal"><spring:message code="label_cancel"/></button>
      </div>
    </div>
  </div>
</div>
