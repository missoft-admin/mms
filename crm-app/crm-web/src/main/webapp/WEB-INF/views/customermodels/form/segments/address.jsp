<%@ include file="../../../common/taglibs.jsp" %>


<!-- Address -->
<fieldset class="container-fluid pull-left">
  <legend class="memberDetails"><spring:message code="label_com_transretail_crm_entity_customermodel_address" /></legend>
  <legend class="professionalDetails"><spring:message code="label_com_transretail_crm_entity_customermodel_businessaddress" /></legend>
  <div class="row-fluid">
    <spring:message code="label_address_street" var="street"/>
    <fieldset class="pull-left span6">
      <label for="street"><b class="required">*</b>${street }</label>
      <form:input path="address.street" placeholder="${street}" cssClass="span12" />
    </fieldset>
    <spring:message code="label_address_number" var="streetNumber"/>
    <fieldset class="pull-left ml10">
    <label for="streetNumber">${streetNumber }</label>
       <form:input path="address.streetNumber" cssClass="span10" placeholder="${streetNumber}" />
    </fieldset>
  </div>
  <div class="row-fluid">
      <spring:message code="label_address_block" var="block"/>
      <fieldset class="pull-left">
       <label for="block">${block }</label>
        <form:input path="address.block" placeholder="${block}" cssClass="span11" />
      </fieldset>
      <spring:message code="label_address_km" var="km"/>
      <fieldset class="pull-left professionalDetails">
       <label for="km">${km }</label>
        <form:input path="address.km" placeholder="${km}" cssClass="span11 intInput" />
      </fieldset>
      <%-- <spring:message code="label_address_rtrw" var="rtrw"/>
      <fieldset class="span3">
        <label for="rt">${rtrw }</label>
        <form:input path="address.rt" placeholder="${rtrw}" cssClass="span11" />
      </fieldset> --%>
      <spring:message code="label_address_rt" var="rt"/>
      <fieldset class="pull-left">
       <label for="rt">${rt }</label>
        <form:input path="address.rt" placeholder="${rt}" cssClass="span11 intInput" />
      </fieldset>
      <spring:message code="label_address_rw" var="rw"/>
      <fieldset class="pull-left">
       <label for="rw">${rw }</label>
        <form:input path="address.rw" placeholder="${rw}" cssClass="span11 intInput" />
      </fieldset>
  </div>
  <div class="row-fluid">
      <spring:message code="label_address_building" var="building"/>
      <fieldset class="pull-left span6">
       <label for="building">${building }</label>
        <form:input path="address.building" placeholder="${building}" cssClass="span12" />
      </fieldset>
      <spring:message code="label_address_floor" var="floor"/>
      <fieldset class="pull-left professionalDetails">
       <label for="floor">${floor }</label>
        <form:input path="address.floor" placeholder="${floor}" cssClass="span10 intInput" />
      </fieldset>
      <spring:message code="label_address_room" var="room"/>
      <fieldset class="pull-left professionalDetails">
       <label for="room">${room }</label>
        <form:input path="address.room" placeholder="${room}" cssClass="span10" />
      </fieldset>
  </div>
  <div class="row-fluid">
      <spring:message code="label_address_postalcode" var="postCode"/>
      <fieldset class="pull-left">
        <label for="postCode">${postCode }</label>
        <form:input path="address.postCode" placeholder="${postCode}" cssClass="span11" />
      </fieldset>
      <spring:message code="label_address_subdistrict" var="subdistrict"/>
      <fieldset class="pull-left">
       <label for="subdistrict">${subdistrict }</label>
        <form:input path="address.subdistrict" placeholder="${subdistrict}" cssClass="span11" />
      </fieldset>
      <spring:message code="label_address_district" var="district"/>
      <fieldset class="pull-left">
       <label for="district">${district }</label>
        <form:input path="address.district" placeholder="${district}" cssClass="span11" />
      </fieldset>
  </div>
  <div class="row-fluid provinceCityBlock">
      <spring:message code="label_address_province" var="province"/>
      <fieldset class="pull-left">
        <label for="province">${province}</label>
        <select name="address.province" class="provinceSelect">
          <option value=""></option>
          <c:forEach items="${provinces}" var="province">
          <option value="${province.name}">${province.name}</option>
          </c:forEach>
        </select>
        <%--<form:input path="address.province" placeholder="${province}" cssClass="span10" /> --%>
      </fieldset>
      <spring:message code="label_address_city" var="city"/>
      <fieldset class="pull-left ml10">
        <label for="city">${city }</label>
        <select name="address.city" class="citySelect">
        <option value=""></option>
        </select>
        <%--<form:input path="address.city" placeholder="${city}" cssClass="span11" /> --%>
      </fieldset>
  </div>
</fieldset>