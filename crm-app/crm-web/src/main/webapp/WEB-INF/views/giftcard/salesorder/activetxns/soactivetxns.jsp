<%@include file="../../../common/taglibs.jsp"%>


<div id="content_soActiveTxns">

  <div id="soAllocForm" data-url="<c:url value="/giftcard/salesorder/allocate/" />"></div>
  <div id="soPrintCon" data-url="<c:url value="/giftcard/salesorder/pickup/print/" />" 
    data-url-form="<c:url value="/giftcard/salesorder/pickup/show/" />"></div>
  <div id="soPickupCon" data-url="<c:url value="/giftcard/salesorder/pickup/update/" />"></div>
  <div id="handoverCon" data-url="<c:url value="/giftcard/salesorder/pickup/handover/" />"
    data-msg-confirm="<spring:message code="gc.so.pickup.handedover.confirm" />" ></div>
  <div id="utilCon" data-url-so="<c:url value="/giftcard/salesorder" />" 
    data-url-foractivation="<c:url value="/giftcard/salesorder/activation/" />"
    data-url-foractivation-notif="<c:url value="/giftcard/salesorder/activation/notify/" />"
    data-msg-foractivation="<spring:message code="gc.so.paymentapproval.confirm" />" 
    data-url-activate="<c:url value="/giftcard/salesorder/activate/" />"
    data-msg-activate="<spring:message code="gc.so.activate.confirm" />" 
    data-url-cancelorder="<c:url value="/giftcard/salesorder/cancel/%ID%" />" 
    data-url-delivery="<c:url value="/giftcard/salesorder/delivery/%ID%" />" ></div>


<script type='text/javascript'>
var SoAlloc = null;
var SoPickup = null;
var SoOtherTxns = null;

$(document).ready( function() {

    var $container = $( "#content_soActiveTxns" );
    var $allocForm = $container.find( "#soAllocForm" );
    var $printCon = $container.find( "#soPrintCon" );
    var $pickupCon = $container.find( "#soPickupCon" );
    var $handoverCon = $container.find( "#handoverCon" );
    var $utilCon = $container.find( "#utilCon" );

    SoAlloc = {
        showSoAllocForm : function( soId ) {
            $.get( $allocForm.data( "url" ) + soId, function( resp ) {
            	  $allocForm.html( resp );
            }, "html" );
        }
    };

    SoPickup = {
    		showPrintForm   : function( soId ) {
    			  $.get( $printCon.data( "url-form" ) + soId, function( resp ) {
    				    $printCon.html( resp );
    			  }, "html" );
    		},
    		printPickupDocs : function( soId, doctype, receiptNo ) {
    			  window.open( $printCon.data( "url" ) + soId + "/" + doctype + "?receiptNo=" + receiptNo, '_blank', 'toolbar=0, location=0, menubar=0' );
    			  return true;
//     			  $.post( $printCon.data( "url" ) + soId + "/" + "SO_SHEET", function( resp ) {
    				    //TODO $pickupCon.html( resp );
//     				    if ( resp.success ) {
//     				    	  window.location.href = $utilCon.data( "url-so" );
//     				    }
//             }, "json" );
    		},
        updateForPickup : function( soId, inDialog ) {
            $.post( $pickupCon.data( "url" ) + soId, function( resp ) {
                if ( resp.success ) {
                	  inDialog.modal( "hide" );
                	  $("#sales_order_list_container").ajaxDataTable( "search" );//location.reload();//window.location.href = $utilCon.data( "url-so" );
                }
            }, "json" );
        },
    		handoverItemsAndDocs : function ( soId ) {
    		    getConfirm( $handoverCon.data( "msg-confirm" ), function( result ) { 
    		    	  if( result ) {
    		    		    $.post( $handoverCon.data( "url" ) + soId, function( resp ) {
    		    		    	  $("#sales_order_list_container").ajaxDataTable( "search" );//location.reload();
    		    		    });
    		        } else {
    		        	$("button:disabled").prop("disabled", false).removeClass("disabled");
    		        }
    		    	  
    		    });
    		    
    		}
    };

    SoOtherTxns = {
        approvePayment : function( soId ) {
            getConfirm( $utilCon.data( "msg-foractivation" ), function( result ) { 
                if( result ) {
                    $.post( $utilCon.data( "url-foractivation" ) + soId, function( resp ) {
                        $.post( $utilCon.data( "url-foractivation-notif" ) + soId, function( resp ) {});
                        $("#sales_order_list_container").ajaxDataTable( "search" );//location.reload();
                    });
                }
            });
            
        },
        activate : function( soId ) {
            getConfirm( $utilCon.data( "msg-activate" ), function( result ) { 
                if( result ) {
                    $.post( $utilCon.data( "url-activate" ) + soId, function( resp ) {
                    	  $("#sales_order_list_container").ajaxDataTable( "search" );//location.reload();
                    });
                } else {
		        	$("button:disabled").prop("disabled", false).removeClass("disabled");
		        }
            });
        },
        showCancelOrderForm   : function( soId ) {
            $.get( $utilCon.data( "url-cancelorder" ).replace( new RegExp( "%ID%", "g" ), soId ), function( resp ) {
                $utilCon.html( resp );
            }, "html" );
        },
        showDeliveryForm   : function( soId ) {
            $.get( $utilCon.data( "url-delivery" ).replace( new RegExp( "%ID%", "g" ), soId ), function( resp ) {
                $utilCon.html( resp );
            }, "html" );
        }
    };

});
</script>
</div>