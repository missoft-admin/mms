package com.transretail.crm.core.service;


import java.util.List;

import com.transretail.crm.common.service.dto.request.PageSortDto;
import com.transretail.crm.common.service.dto.response.ResultList;
import com.transretail.crm.core.dto.ProductGroupDto;
import com.transretail.crm.core.dto.ProductGroupDto.CodeDescBean;
import com.transretail.crm.core.dto.ProductGroupSearchDto;
import com.transretail.crm.core.entity.ProductGroup;
import com.transretail.crm.core.entity.lookup.Product;


public interface ProductGroupService {

	List<ProductGroup> findAll();

	List<ProductGroup> getAllGroups();

	ProductGroup findOne(Long id);

	ResultList<ProductGroup> listProductGroups(PageSortDto dto);

	Iterable<ProductGroup> search(ProductGroup group);

	void delete(Long id);

	void invalidate(Long id);

	void save(ProductGroup productGroup);

	void saveDto(ProductGroupDto dto);

	void updateDto(ProductGroupDto dto);



	List<Product> getItems(Long id);

	ResultList<Product> listProducts(Long id, PageSortDto dto);

	Iterable<Product> searchItems(String searchStr);



	List<CodeDescBean> filterProductAndCfns(String filterCode, String prdCodes, String cfnCodes);

	List<CodeDescBean> getProducts(String prdCodes, String cfnCodes);

	List<CodeDescBean> getProducts(String prdCodes, String cfnCodes, Integer idx, Integer size);

	ResultList<ProductGroupDto> search(ProductGroupSearchDto dto);

	ProductGroupDto findDto(Long id);

	List<CodeDescBean> searchProductAndCfns(String filterCode, boolean prd, String cfnCodes);

	List<CodeDescBean> searchProducts(String filterCode, String prdCodes, String cfnCodes);

	Iterable<Product> getProducts(String prdCfnCodes);

}
