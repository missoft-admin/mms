package com.transretail.crm.web.controller;


import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import com.transretail.crm.core.dto.PromotionDto;
import com.transretail.crm.core.security.model.impl.CustomSecurityUserDetailsImpl;
import com.transretail.crm.core.security.util.UserUtil;


@RequestMapping( "/promotion/stamp" )
@Controller
public class StampPromoController extends PromotionController {
	
	@RequestMapping( value="/create", produces = "text/html" )
    public String createPromotion(
    		@ModelAttribute(value="promotion") PromotionDto inPromotion,
            Model inUiModel ) {
		inPromotion.setRewardType(codePropertiesService.getDetailRewardTypeStamp());
		populateFormSelects( inUiModel, inPromotion );
    	populateTargets( inUiModel );
		populateOtherForms( inUiModel );
		inUiModel.addAttribute( "isStampPromo", true );
		inUiModel.addAttribute( "promotionFormAction", "/promotion/save" );
        return PATH_CREATE;
    }
	
	@RequestMapping( value="/edit/{id}", produces = "text/html" )
    public String editPromotion(
			@PathVariable(value = "id") Long inId,
    		@ModelAttribute(value="promotion") PromotionDto inPromotion,
            Model inUiModel ) {

		PromotionDto dto = promotionService.getPromotionDto( inId );
		inUiModel.addAttribute( "promotion", dto );
		populateFormSelects( inUiModel, inPromotion );
    	populateTargets( inUiModel );
		populateOtherForms( inUiModel );
		populateRewardTypes( inUiModel, dto.getRewardType() );
		inUiModel.addAttribute( UIATTR_PROMOCAMPAIGNS,
				( null != dto.getProgram() )?
						promotionService.getCampaignDtos( dto.getProgram() ) : null );
		inUiModel.addAttribute( "promotionFormAction", "/promotion/update" );

		CustomSecurityUserDetailsImpl user = UserUtil.getCurrentUser();
		String mktHeadCode = codePropertiesService.getMktHeadCode();
		inUiModel.addAttribute( UIATTR_MKTHEAD, user.hasAuthority( mktHeadCode ) );
		populateApprovalStatuses( inUiModel );
		if ( ( StringUtils.equalsIgnoreCase( dto.getCreateUser(), user.getUsername() )  || user.hasAuthority( mktHeadCode ) ) 
				&& ( StringUtils.isNotBlank( dto.getStatus() ) && !dto.getStatus().equalsIgnoreCase( codePropertiesService.getDetailStatusActive() ) ) ) {
			return PATH_CREATE;
		}

        return PATH_VIEW;
    }
	
	private void populateRewardTypes( Model inUiModel, String inType ) {
    	inUiModel.addAttribute( "isStampPromo", true );
	}
	
	
}
