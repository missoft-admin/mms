/*
 * Copyright (c) 2014. Medcurial, Inc.
 * All rights reserved.
 */
package com.transretail.crm.core.entity.enums;

/**
 * account status for CRM Member.
 *
 * @author Vincent Gerard Tan
 */
public enum MemberStatus {
    ACTIVE, INACTIVE, TERMINATED;
}
