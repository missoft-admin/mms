package com.transretail.crm.template.service.streaming.excel.handler.cell;

import org.apache.commons.lang3.StringUtils;

import com.transretail.crm.core.entity.MemberModel;
import com.transretail.crm.core.entity.lookup.LookupDetail;
import com.transretail.crm.template.service.MemberTemplateParseException;
import com.transretail.crm.template.service.streaming.excel.handler.sheet.AbstractSheetHandler;

/**
 * @author Allan G. Ramirez (agramirez@exist.com)
 */
public class BestTimeToCellHandler implements CellHandler {
    private AbstractSheetHandler sheetHandler;

    public BestTimeToCellHandler(AbstractSheetHandler sheetHandler) {
        this.sheetHandler = sheetHandler;
    }

    @Override
    public void handle(MemberModel dto, String cellText) throws MemberTemplateParseException {
        if (StringUtils.isNotBlank(cellText)) {
            LookupDetail detail = sheetHandler.getLoookupDetail(sheetHandler.getCodePropertiesService().getBestTimeToCall(), cellText);
            dto.setBestTimeToCall(detail.getCode());
        }
    }
}
