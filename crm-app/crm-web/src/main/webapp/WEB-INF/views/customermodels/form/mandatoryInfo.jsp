<%@ include file="../../common/taglibs.jsp" %>


  <fieldset class="form-horizontal container-fluid span3" id="content_mandatoryInfo">
    <legend class="memberDetails"></legend>
    <div class="pull-left mandatoryInfo" id="mandatoryInfo">
      <div class="control-group">
        <spring:message code="label_com_transretail_crm_entity_customermodel_firstname" var="reqd_firstName" />
        <label for="reqd_firstName" class="control-label"><b class="required">*</b><c:out value="${reqd_firstName}" /> </label>
        <div class="controls"><input type="text" data-field="firstName" id="reqd_firstName" class="form-control reqdField" placeholder="${reqd_firstName}" /></div>
      </div>
      <div class="control-group">
        <spring:message code="label_com_transretail_crm_entity_customermodel_birthdate" var="reqd_birthdate" />
        <label for="reqd_birthdate" class="control-label"><b class="required">*</b><c:out value="${reqd_birthdate}" /> </label>
        <div class="controls"><input type="text" data-field="customerProfile.birthdate" id="reqd_birthdate" class="form-control reqdField" placeholder="${reqdBirthdate}" /></div>
      </div>
      <div class="control-group">
	      <spring:message code="label_com_transretail_crm_entity_customermodel_pin" var="reqd_pin" />
	      <label for="reqd_pin" class="control-label"><b class="required">*</b><c:out value="${reqd_pin}" /> </label>
        <div class="controls"><input type="password" data-field="pin" id="reqd_pin" class="form-control reqdField" placeholder="${reqd_pin}" maxlength="6" /></div>
      </div>
      <div class="control-group">
        <spring:message code="label_com_transretail_crm_entity_customermodel_gender" var="reqd_gender" />
        <label for="reqd_gender" class="control-label"><b class="required">*</b><c:out value="${reqd_gender}" /> </label>
        <div class="controls"><select data-field="customerProfile.gender" id="reqd_gender" class="form-control reqdField" ><c:forEach var="item" items="${gender}"><option value="${item.code}">${item.description}</option></c:forEach></select></div>
      </div>
      <div class="control-group">
        <spring:message code="label_address_street" var="reqd_street" />
        <label for="reqd_street" class="control-label"><b class="required">*</b><c:out value="${reqd_street}" /> </label>
        <div class="controls"><input type="text" data-field="address.street" id="reqd_street" class="form-control reqdField" placeholder="${reqd_street}" /></div>
      </div>
      <div class="control-group">
        <spring:message code="member_prop_contact" var="reqd_contact" />
        <label for="reqd_contact" class="control-label"><b class="required">*</b><c:out value="${reqd_contact}" /> </label>
        <div class="controls"><input type="text" data-field="contact" id="reqd_contact" class="form-control reqdField" placeholder="${reqd_contact}" /></div>
      </div>
      <c:if test="${isEmployee}">
      <div class="control-group">
        <spring:message code="label_com_transretail_crm_entity_customermodel_accountnumber" var="reqd_accountId" />
        <label for="reqd_accountId" class="control-label"><b class="required">*</b><c:out value="${reqd_accountId}" /> </label>
        <div class="controls"><input type="text" data-field="accountId" id="reqd_accountId" class="form-control reqdField" placeholder="${reqd_accountId}" maxlength="12"/></div>
      </div>
      </c:if>
    </div>

    <script type="text/javascript">
    var MandatoryInfo = null;
    $( document ).ready( function() {
    	  var $mandatory = $( "#mandatoryInfo" );
    	  var $memberDialog = $( "#memberFormDialog" );

        initOthers();
        initFields();

    	  function initFields() {
            $.each( $mandatory.find( ".reqdField" ), function( idx, obj ) {
                var $el = $memberDialog.find( $( "[name='" + $(obj).data( "field" ) + "']" ) );
                $el = ( $el.length > 1 )? $el.first() : $el;
                $el.change( function() {
                    $( obj ).val( $(this).val() );
                });
                $( obj ).change( function() {
                	  $el.val( $(this).val() );
                });
                $( obj ).val( "" );
            });
            $mandatory.find( "#reqd_birthdate" ).datepicker({ format    : 'dd-mm-yyyy', endDate   : '-0d', autoclose : true })
            	.on('changeDate', function(e) {
                	var date = e.date.getDate();
                	date = date < 10 ? "0" + date : date;
                	var month = e.date.getMonth() + 1;
                	month = month < 10 ? "0" + month : month;
                	var year = e.date.getFullYear() % 100;
                	var val = date + month + year;
					console.log(val);
					$("#reqd_pin").val(val);
					$("#pin").val(val);
                });
    	  }

    	  function initOthers() {
    		    $memberDialog.on( "hide", function(e) { if ( e.target === this ) { MandatoryInfo.reset( $memberDialog ); } });
    	  }

    	  MandatoryInfo = {
    			  fill : function ( key, value ) {
    				    if ( key == "customerProfile" || key == "address" ) {
    				    	  $.each( value, function ( k, v ) {
    				    		    var $mandEl = $mandatory.find( "#reqd_" + k.substring( k.indexOf( "." ) + 1 ) );

    				    		    if ( v ) {
                            if ( $mandEl.length ) {
                                v = v.code? v.code : v;
                                if ( k.indexOf( "birthdate" ) != -1 ) {
                                    try {
                                        var dateValue = v ? ( new Date( v ).getTime()? new Date( v ) 
                                              : new Date( $.trim( value ).split( " " )[0] ).getTime()? new Date( $.trim( value ).split( " " )[0] ) : null ) 
                                          : null;
                                              $mandEl.datepicker( "update", dateValue );
                                    } catch(e) {}
                                }
                                else {
                                	$mandEl.val( v );
                                }
                            }
    				    		    }
                    });
    				    }
    				    else {
    				    	  $mandatory.find( "#reqd_" + key ).val( value );
    				    }
    			  },
            reset     : function( ele, isEmpty ) {
                $( ele ).find(':input').each( function() {
                    switch(this.type) {
                        case 'password':
                        case 'select-one':
                        case 'text':
                        case 'textarea':
                        case 'file':
                        case 'hidden': $(this).val(''); break;
                        case 'select-multiple': isEmpty? $(this).empty() : $(this).val(""); break;
                        case 'checkbox':
                        case 'radio': this.checked = false;
                    }
                });
            }
    	  }
    });
    </script>
  </fieldset>
  
  
  