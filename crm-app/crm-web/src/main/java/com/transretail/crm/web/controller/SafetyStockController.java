package com.transretail.crm.web.controller;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import com.transretail.crm.giftcard.dto.SafetyStockDto;
import com.transretail.crm.giftcard.service.SafetyStockService;

/**
 * @author Allan G. Ramirez (agramirez@exist.com)
 */
@Controller
@RequestMapping("/safetystock")
public class SafetyStockController {
    @Autowired
    private SafetyStockService service;

    @RequestMapping(value = "/{productProfileId}", method = RequestMethod.GET)
    @ResponseBody
    public SafetyStockDto getSafetyStock(@PathVariable("productProfileId") Long productProfileId) {
        return service.getSafetyStock(productProfileId);
    }

    @RequestMapping(value = "/save", method = RequestMethod.POST)
    @ResponseStatus(HttpStatus.OK)
    public void save(@Valid @RequestBody SafetyStockDto dto) {
        service.update(dto);
    }
}
