<%@ include file="../../common/taglibs.jsp" %>

<link href="<c:url value="/css/dataTables.css" />" rel="stylesheet">

<style type="text/css">
<!--
  .dataTables_scrollHeadInner {width:auto !important};
  .table {width:auto !important};
-->
</style>

<div id="supplements" 
    data-url="<c:url value="/customermodels/supplements/" />"
    data-hdr-username="<spring:message code="label_com_transretail_crm_entity_customermodel_username" />"
    data-hdr-accountno="<spring:message code="label_com_transretail_crm_entity_customermodel_accountnumber" />"
    data-hdr-member="<spring:message code="label_com_transretail_crm_entity_customermodel" />"
    data-hdr-points="<spring:message code="label_member_contributedpoints" />"></div>
    