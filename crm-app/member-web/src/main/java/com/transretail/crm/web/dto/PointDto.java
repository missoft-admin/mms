package com.transretail.crm.web.dto;

import java.util.Date;

import com.transretail.crm.core.entity.PointsTxnModel;
import com.transretail.crm.core.entity.enums.PointTxnType;

/**
 * @author Allan G. Ramirez (agramirez@exist.com)
 */
public class PointDto {
    private String storeCode;
    private PointTxnType transactionType;
    private Date transactionDateTime;
    private Date created;
    private String transactionNo;
    private Long transactionPoints;
    private Double transactionAmount;
    private String transactionMedia;
    private String transactionMediaDesc;
    private String memberModel;
    private String transactionContributer;
    private String memberModelId;
    private String transactionContributerId;

    private Long longTxnDate;
    private Long longCreated;

    public PointDto(String storeCode, PointTxnType transactionType, Date transactionDateTime, String transactionNo,
        Long transactionPoints,
        Double transactionAmount) {
        this.storeCode = storeCode;
        this.transactionType = transactionType;
        this.transactionDateTime = transactionDateTime;
        this.transactionNo = transactionNo;
        this.transactionPoints = transactionPoints;
        this.transactionAmount = transactionAmount;
        if ( null != transactionDateTime ) {
        	this.longTxnDate = transactionDateTime.getTime();
        }
    }

    public PointDto(PointsTxnModel point) {
            this.storeCode = point.getStoreCode();
            this.transactionType = point.getTransactionType();
            this.transactionDateTime = point.getTransactionDateTime();
            this.created = null != point.getCreated() ? 
            		new Date( point.getCreated().getMillis() ) : null ;
            this.transactionNo = point.getTransactionNo();
            this.transactionPoints = point.getTransactionPoints().longValue();
            this.transactionAmount = point.getTransactionAmount();
            this.transactionMedia = point.getTransactionMedia();
            if ( null != transactionDateTime ) {
            	this.longTxnDate = point.getTransactionDateTime().getTime();
            }
            if ( null != created ) {
            	this.longCreated = point.getCreated().getMillis();
            }
            
            this.memberModelId = point.getMemberModel().getAccountId();
            this.memberModel = this.memberModelId + " " + point.getMemberModel().getFormattedMemberName();
            
            
            if(point.getTransactionContributer() != null) {
            	this.transactionContributerId = point.getTransactionContributer().getAccountId();
            	this.transactionContributer = this.transactionContributerId
            			+ " - " + point.getTransactionContributer().getFormattedMemberName();
            }
        }

    public String getStoreCode() {
        return storeCode;
    }

    public void setStoreCode(String storeCode) {
        this.storeCode = storeCode;
    }

    public PointTxnType getTransactionType() {
        return transactionType;
    }

    public void setTransactionType(PointTxnType transactionType) {
        this.transactionType = transactionType;
    }

    public Date getTransactionDateTime() {
        return transactionDateTime;
    }

    public Date getCreated() {
		return created;
	}

	public void setCreated(Date created) {
		this.created = created;
	}

	public void setTransactionDateTime(Date transactionDateTime) {
        this.transactionDateTime = transactionDateTime;
    }

    public String getTransactionNo() {
        return transactionNo;
    }

    public void setTransactionNo(String transactionNo) {
        this.transactionNo = transactionNo;
    }

    public Long getTransactionPoints() {
        return transactionPoints;
    }

    public void setTransactionPoints(Long transactionPoints) {
        this.transactionPoints = transactionPoints;
    }

    public Double getTransactionAmount() {
        return transactionAmount;
    }

    public void setTransactionAmount(Double transactionAmount) {
        this.transactionAmount = transactionAmount;
    }

	public String getTransactionMedia() {
		return transactionMedia;
	}

	public void setTransactionMedia(String transactionMedia) {
		this.transactionMedia = transactionMedia;
	}

	public String getTransactionMediaDesc() {
		return transactionMediaDesc;
	}

	public void setTransactionMediaDesc(String transactionMediaDesc) {
		this.transactionMediaDesc = transactionMediaDesc;
	}

	public Long getLongTxnDate() {
		return longTxnDate;
	}

	public void setLongTxnDate(Long longTxnDate) {
		this.longTxnDate = longTxnDate;
	}

	public Long getLongCreated() {
		return longCreated;
	}

	public void setLongCreated(Long longCreated) {
		this.longCreated = longCreated;
	}

	public String getMemberModel() {
		return memberModel;
	}

	public void setMemberModel(String memberModel) {
		this.memberModel = memberModel;
	}

	public String getTransactionContributer() {
		return transactionContributer;
	}

	public void setTransactionContributer(String transactionContributer) {
		this.transactionContributer = transactionContributer;
	}

	public String getMemberModelId() {
		return memberModelId;
	}

	public void setMemberModelId(String memberModelId) {
		this.memberModelId = memberModelId;
	}

	public String getTransactionContributerId() {
		return transactionContributerId;
	}

	public void setTransactionContributerId(String transactionContributerId) {
		this.transactionContributerId = transactionContributerId;
	}

}
