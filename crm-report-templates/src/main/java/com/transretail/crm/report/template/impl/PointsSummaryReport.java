package com.transretail.crm.report.template.impl;

import com.google.common.base.Function;
import com.google.common.collect.*;
import com.mysema.query.support.Expressions;
import com.mysema.query.types.expr.CaseBuilder;
import com.mysema.query.types.expr.DateExpression;
import java.util.*;

import static com.mysema.query.group.GroupBy.*;

import com.mysema.query.jpa.impl.JPAQuery;
import com.mysema.query.types.ConstantImpl;
import com.mysema.query.types.Expression;
import com.mysema.query.types.Projections;
import com.mysema.query.types.expr.*;
import com.mysema.query.types.template.DateTemplate;
import com.transretail.crm.common.reporting.jasper.ClasspathFileResolver;
import com.transretail.crm.common.reporting.jasper.DefaultJRProcessor;
import com.transretail.crm.common.reporting.jasper.JRProcessor;
import com.transretail.crm.common.util.DateUtil;
import com.transretail.crm.core.entity.QApplicationConfig;

import com.transretail.crm.core.entity.QPointsTxnModel;

import com.transretail.crm.core.entity.QPromotion;
import com.transretail.crm.core.entity.QTransactionAudit;

import com.transretail.crm.core.entity.enums.PointTxnType;
import com.transretail.crm.core.entity.enums.ReportType;
import com.transretail.crm.core.entity.lookup.QLookupDetail;
import com.transretail.crm.core.service.CodePropertiesService;
import com.transretail.crm.report.template.FilterField;
import com.transretail.crm.report.template.ReportDateFormatProvider;
import com.transretail.crm.report.template.ReportTemplate;
import com.transretail.crm.report.template.ReportsCustomizer;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.apache.commons.collections.comparators.ComparatorChain;
import org.apache.commons.lang3.StringUtils;
import org.joda.time.DateTimeComparator;
import org.joda.time.Days;
import org.joda.time.LocalDate;
import org.joda.time.Months;
import org.joda.time.YearMonth;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.MessageSourceAware;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.context.support.MessageSourceAccessor;
import org.springframework.stereotype.Service;

import static com.mysema.query.alias.Alias.$;
import static com.mysema.query.alias.Alias.alias;
import static com.mysema.query.collections.CollQueryFactory.from;
import static com.transretail.crm.core.entity.QApplicationConfig.applicationConfig;
import static com.transretail.crm.core.entity.QPointsTxnModel.pointsTxnModel;
import static com.transretail.crm.core.entity.enums.AppKey.POINTS_VALUE;

/**
 *
 * @author Monte Cillo Co (mco@exist.com)
 */
public abstract class PointsSummaryReport implements ReportTemplate, MessageSourceAware {

    private static final String ITEM_POINT_REWARD_TYPE_CODE = "RWRD004";
    private static final String POINT_REWARD_TYPE_CODE = "RWRD001";
    private static final String POINT_REWARD_TYPE_REDEEM_ITEM_CODE = "RWRD005";
    @PersistenceContext
    protected EntityManager em;
    String reportPermission;
    String reportName;
    String descriptionCode;
    String templateName;
    MessageSourceAccessor messageSource;
    @Autowired
    private CodePropertiesService codePropertiesService;

    protected PointsSummaryReport(String templateName, String reportName, String descriptionCode, String reportPermission) {
        this.reportPermission = reportPermission;
        this.reportName = reportName;
        this.descriptionCode = descriptionCode;
        this.templateName = templateName;
    }

    @Override
    public ReportType[] getReportTypes() {
        return ReportType.values();
    }

    @Override
    public String getName() {
        return templateName;
    }

    @Override
    public String getDescription() {
        return messageSource.getMessage(descriptionCode);
    }

    @Override
    public boolean canView(Set<String> permissions) {
        if (permissions != null) {
            for (String permission : permissions) {
                if (permission.equalsIgnoreCase(reportPermission)) {
                    return true;
                }
            }
        }
        return false;
    }

    @Override
    public void setMessageSource(MessageSource ms) {
        this.messageSource = new MessageSourceAccessor(ms);
    }

    protected JRProcessor template(Map<String, Object> reportParam) {
        DefaultJRProcessor processor = new DefaultJRProcessor(reportName, getCollectionDatasource(reportParam));
        processor.addParameters(reportParam);
        processor.setFileResolver(new ClasspathFileResolver("reports"));

        return processor;
    }

    // expose the datasource so we can do testing later
    protected abstract Collection<?> getCollectionDatasource(Map<String, Object> reportParam);

    // Good to have this in a util class	
    protected DateExpression<Date> truncate(Expression<?> expression, String pattern) {
        return DateTemplate.create(Date.class, "trunc({0}, '{1s}')", expression, ConstantImpl.create(pattern));
    }

    public static int compareIndex(int x, int y) {
        return (x < y) ? -1 : ((x == y) ? 0 : 1);
    }

    // will use ordinal for sorting
    enum ReportCategory {

        TOTAL_POINTS_ISSUED,
        POINT,
        ITEM_POINT,
        EMPLOYEE_POINT,
        EMPLOYEE_RECON,
        TOTAL_POINTS_REDEEMED,
        POINTS_REDEEMED_PERCENTAGE,
        EXPIRE,
        VOID,
        RETURN,
        ADJUSTMENT,
        CLOSING_BALANCE;

        public String messageCode() {
            return String.format("points.summary.report.category.%s.label", name().toLowerCase());
        }
    }

    protected Double getPointValue() {
        final QApplicationConfig qa = applicationConfig;
        return Double.parseDouble(new JPAQuery(em).from(qa)
                .where(qa.key.eq(POINTS_VALUE)).uniqueResult(qa.value));
    }

    @Service("yearlyPointsSummaryReport")
    public static class Yearly extends PointsSummaryReport {

        protected static final String REPORT_PARAM_YEAR = "YEAR";
        protected static final String REPORT_FILTER_YEAR = "YEAR";

        public Yearly() {
            super("Point Summary Yearly Report",
                    "reports/point_summary_yearly_report.jasper",
                    "points.summary.yearly.report.title",
                    "REPORT_POINTS_SUMMARY_YEARLY_VIEW");
        }

        @Override
        public Set<FilterField> getFilters() {
            return Sets.newHashSet(FilterField.createDateField(REPORT_FILTER_YEAR,
                    messageSource.getMessage("points.summary.yearly.filter.year"),
                    REPORT_FILTER_YEAR, FilterField.DateTypeOption.YEAR, true));
        }

        @Override
        protected Collection<?> getCollectionDatasource(Map<String, Object> reportParam) {
            String temp = StringUtils.defaultIfBlank(String.valueOf(reportParam.get(REPORT_PARAM_YEAR)), "0");
            int targetYear = Integer.parseInt(temp);
            int prevYear = targetYear - 1;

            QPointsTxnModel qp = pointsTxnModel;
            List<CrosstabBean> ds = new ArrayList<CrosstabBean>();
            List<CrosstabBean> allPointTypes = new JPAQuery(em).from(qp)
                    .where(qp.transactionDateTime.year().between(prevYear, targetYear))
                    .groupBy(qp.transactionType.stringValue(), qp.transactionDateTime.year())
                    .list(Projections.constructor(CrosstabBean.class, qp.transactionDateTime.year(), 
                            qp.transactionType.stringValue(), qp.transactionPoints.sum()));

            CrosstabBean cb = alias(CrosstabBean.class, "cb");
            final Map<Date, Double> totalPointsMap = from(cb, allPointTypes)
                    .where($(cb.getRow()).ne(PointTxnType.REDEEM.name()))
                    .transform(groupBy($(cb.getDate()))
                            .as(sum($(cb.getValue()))));

            Collection<CrosstabBean> totalPoints = Collections2.transform(totalPointsMap.entrySet(),
                    crosstabFunction(ReportCategory.TOTAL_POINTS_ISSUED.name()));
            // extract POINTS and ITEM_POINTS
            QPromotion qpromo = QPromotion.promotion;
            QTransactionAudit qta = QTransactionAudit.transactionAudit;
            QLookupDetail qld = QLookupDetail.lookupDetail;

            final SimpleExpression<String> pointsType = new CaseBuilder()
                    .when(qld.code.eq(Expressions.constant(POINT_REWARD_TYPE_CODE)))
                    .then(Expressions.stringTemplate(String.format("'%s'", ReportCategory.POINT.name())))
                    .when(qld.code.equalsIgnoreCase(ITEM_POINT_REWARD_TYPE_CODE))
                    .then(Expressions.stringTemplate(String.format("'%s'", ReportCategory.ITEM_POINT.name())))
                    .when(qld.code.equalsIgnoreCase(POINT_REWARD_TYPE_REDEEM_ITEM_CODE))
                    .then(Expressions.stringTemplate(String.format("'%s'", ReportCategory.TOTAL_POINTS_REDEEMED.name())))
                    .otherwise(Expressions.stringTemplate(String.format("'%s'", "default")));

            final List<CrosstabBean> earnPointGroup = new JPAQuery(em).from(qp, qta, qpromo, qld)
                    .where(qp.transactionType.eq(PointTxnType.EARN)
                            .and(qp.transactionNo.eq(qta.transactionNo))
                            .and(qta.promotion.id.eq(qpromo.id))
                            .and(qpromo.rewardType.code.eq(qld.code))
                            .and(qp.transactionDateTime.year().between(prevYear, targetYear)))
                    .groupBy(qld.code, qp.transactionDateTime.year())
                    .list(Projections.constructor(CrosstabBean.class, qp.transactionDateTime.year(), 
                            pointsType, qp.transactionPoints.sum()));

            final Map<Date, Double> redeemMap = from(cb, allPointTypes)
                    .where($(cb.getRow()).eq(PointTxnType.REDEEM.name()))
                    .transform(groupBy($(cb.getDate()))
                            .as(sum($(cb.getValue()))));

            for (Entry<Date, Double> entry : redeemMap.entrySet()) {
                ds.add(new CrosstabBean(entry.getKey(), "",
                        ReportCategory.TOTAL_POINTS_REDEEMED.name(),
                        entry.getValue()));
            }

            Collection<CrosstabBean> redeemPercentage = Collections2.transform(redeemMap.entrySet(),
                    new Function<Map.Entry<Date, Double>, CrosstabBean>() {

                        @Override
                        public CrosstabBean apply(Entry<Date, Double> entry) {
                            Double redeem = redeemMap.get(entry.getKey());
                            redeem = (null == redeem) ? 0.0 : redeem;
                            Double totalPointsV = totalPointsMap.get(entry.getKey());
                            return new CrosstabBean(entry.getKey(), "", ReportCategory.POINTS_REDEEMED_PERCENTAGE.name(),
                                    Math.abs(redeem) / totalPointsV);
                        }
                    });

            final Map<Date, Double> epointsMap = from(cb, allPointTypes).where($(cb.getRow())
                    .in(PointTxnType.RECON.name(), PointTxnType.REWARD.name()))
                    .transform(groupBy($(cb.getDate()))
                            .as(sum($(cb.getValue()))));

            ds.addAll(Collections2.transform(epointsMap.entrySet(),
                    crosstabFunction(ReportCategory.EMPLOYEE_RECON.name())));

            List<CrosstabBean> allPointsWithoutRedeem = from(cb, allPointTypes).where($(cb.getRow())
                    .notIn(PointTxnType.REDEEM.name(), PointTxnType.EARN.name()))
                    .list($(cb));

            // update column referencing ReportCategory ordinal ordering
            for (CrosstabBean bean : allPointsWithoutRedeem) {
                if (PointTxnType.REDEEM.name().equalsIgnoreCase(bean.getRow())) {
                    bean.setRow(ReportCategory.TOTAL_POINTS_REDEEMED.name());
                } else if (PointTxnType.VOID.name().equalsIgnoreCase(bean.getRow())) {
                    bean.setRow(ReportCategory.VOID.name());
                } else if (PointTxnType.ADJUST.name().equalsIgnoreCase(bean.getRow())) {
                    bean.setRow(ReportCategory.ADJUSTMENT.name());
                } else if (PointTxnType.RETURN.name().equalsIgnoreCase(bean.getRow())) {
                    bean.setRow(ReportCategory.RETURN.name());
                } else if (PointTxnType.REWARD.name().equalsIgnoreCase(bean.getRow())) {
                    bean.setRow(ReportCategory.EMPLOYEE_POINT.name());
                }
            }

            ds.addAll(totalPoints);
            ds.addAll(redeemPercentage);
            ds.addAll(earnPointGroup);
            ds.addAll(allPointsWithoutRedeem);
            Map<Date, Double> transform = from(cb, ds).where($(cb.getRow())
                    .notIn(ReportCategory.POINTS_REDEEMED_PERCENTAGE.name(),
                            ReportCategory.TOTAL_POINTS_ISSUED.name(),
                            ReportCategory.EMPLOYEE_POINT.name()))
                    .transform(groupBy($(cb.getDate())).as(sum($(cb.getValue()))));

            ds.addAll(Collections2.transform(transform.entrySet(),
                    crosstabFunction(ReportCategory.CLOSING_BALANCE.name())));
            Double pointValue = getPointValue();
            DateTimeFormatter format = DateTimeFormat.forPattern("yyyy")
                    .withLocale(LocaleContextHolder.getLocale());
            List<CrosstabBean> tempList = new ArrayList<CrosstabBean>();
            fixData(ds, prevYear, targetYear);
            for (CrosstabBean c : ds) {
                String year = format.print(c.getDate().getTime());
                String ytdLabel = messageSource.getMessage("points.summary.report.column.points.ytd", new Object[]{year});
                String ytdRphLabel = messageSource.getMessage("points.summary.report.column.points.ytd.rph", new Object[]{year});

                c.setColumn(ytdLabel);
                tempList.add(new CrosstabBean(c.getDate(), ytdRphLabel, c.getRow(), c.getValue() * pointValue));
            }

            ds.addAll(tempList);

            Collections.sort(ds, createComparator());
            for (CrosstabBean c : ds) {
                c.setRow(messageSource.getMessage(
                        ReportCategory.valueOf(c.getRow()).messageCode()));
            }

            return ds;
        }

        @Override
        public JRProcessor createJrProcessor(Map<String, String> map) {
            Map<String, Object> reportParam = Maps.newHashMap();
            reportParam.put(REPORT_PARAM_YEAR, map.get(REPORT_PARAM_YEAR));

            return super.template(reportParam);
        }

        //<editor-fold defaultstate="collapsed" desc="Helpers">
        private void fixData(List<CrosstabBean> ds, int prevYear, int curYear) {
            HashSet<Date> dates = Sets.newHashSet();
            dates.add(new LocalDate(prevYear, 1, 1).toDate());
            dates.add(new LocalDate(curYear, 1, 1).toDate());

            for (Date date : dates) {
                for (ReportCategory reportCategory : ReportCategory.values()) {
                    ds.add(new CrosstabBean(date, "", reportCategory.name(), 0.0));
                }
            }
        }

        private Comparator<CrosstabBean> createComparator() {
            ComparatorChain cbc = new ComparatorChain();
            cbc.addComparator(new Comparator<CrosstabBean>() {

                @Override
                public int compare(CrosstabBean o1, CrosstabBean o2) {
                    ReportCategory r1 = ReportCategory.valueOf(o1.getRow());
                    ReportCategory r2 = ReportCategory.valueOf(o2.getRow());

                    return PointsSummaryReport.compareIndex(r1.ordinal(), r2.ordinal());
                }
            });

            cbc.addComparator(new Comparator<CrosstabBean>() {

                @Override
                public int compare(CrosstabBean o1, CrosstabBean o2) {
                    return DateTimeComparator.getDateOnlyInstance().compare(o1.getDate(), o2.getDate());
                }

            });
            return cbc;
        }

        private Function<Entry<Date, Double>, CrosstabBean> crosstabFunction(final String row) {
            return new Function<Map.Entry<Date, Double>, CrosstabBean>() {

                @Override
                public CrosstabBean apply(Entry<Date, Double> entry) {
                    return new CrosstabBean(entry.getKey(), "", row, entry.getValue());
                }
            };
        }
        //</editor-fold>
    }

    @Service("monthlyPointSummaryReport")
    public final static class Monthly extends PointsSummaryReport implements ReportsCustomizer, ReportDateFormatProvider {

        protected static final String REPORT_PARAM_DATE_FROM = "DATE_FROM";
        protected static final String REPORT_PARAM_DATE_TO = "DATE_TO";
        protected static final String REPORT_PARAM_EMPTY_DATASOURCE = "EMPTY_DATASOURCE";
        protected static final String FILTER_DATE_FROM = "dateFrom";
        protected static final String FILTER_DATE_TO = "dateTo";
        protected static final String FILTER_MONTH_YEAR_FORMAT = "MMM-yyyy";
        protected static final String REPORT_PARAM_POINT_VALUE = "POINT_VALUE";
        protected static final String REPORT_PARAM_TARGET_YEAR = "TARGET_YEAR";

        public Monthly() {
            super("Point Summary Monthly Report",
                    "reports/point_summary_monthly_report.jasper",
                    "points.summary.monthly.report.title",
                    "REPORT_POINTS_SUMMARY_MONTHLY_VIEW");
        }

        private List<CrosstabBean> getAllPoints(Date dateFrom, Date dateTo) {
            QPointsTxnModel qp = pointsTxnModel;
            List<CrosstabBean> list = new JPAQuery(em).from(qp)
                    .where(qp.transactionDateTime.between(dateFrom, dateTo))
                    .groupBy(qp.transactionType, qp.transactionDateTime.yearMonth())
                    .list(Projections.constructor(CrosstabBean.class, qp.transactionDateTime.yearMonth(),
                                    qp.transactionType.stringValue(), qp.transactionPoints.sum()));
            return list;
        }

        @Override
        protected Collection<?> getCollectionDatasource(Map<String, Object> reportParam) {
            DateTimeFormatter dtf = DateTimeFormat.forPattern("MMMM-yyyy")
                    .withLocale(LocaleContextHolder.getLocale());

            YearMonth fym = YearMonth.parse(String.valueOf(reportParam.get(REPORT_PARAM_DATE_FROM)), dtf);
            YearMonth tym = YearMonth.parse(String.valueOf(reportParam.get(REPORT_PARAM_DATE_TO)), dtf);
            Date dateFrom = fym.toLocalDate(1).toDate();
            int lastDayOfMonth = tym.toLocalDate(1).dayOfMonth().getMaximumValue();
            Date dateTo = tym.toLocalDate(lastDayOfMonth).toDate();

            List<CrosstabBean> ds = new ArrayList<CrosstabBean>();
            QPointsTxnModel qp = pointsTxnModel;
            List<CrosstabBean> allPointTypes = getAllPoints(dateFrom, dateTo);

            CrosstabBean cb = alias(CrosstabBean.class, "cb");
            final Map<Date, Double> totalPointsMap = from(cb, allPointTypes)
                    .where($(cb.getColumn()).ne(PointTxnType.REDEEM.name()))
                    .transform(groupBy($(cb.getDate()))
                            .as(sum($(cb.getValue()))));

            Collection<CrosstabBean> totalPoints = Collections2.transform(totalPointsMap.entrySet(),
                    crosstabFunction(ReportCategory.TOTAL_POINTS_ISSUED.name()));

            // extract POINTS and ITEM_POINTS
            QPromotion qpromo = QPromotion.promotion;
            QTransactionAudit qta = QTransactionAudit.transactionAudit;
            QLookupDetail qld = QLookupDetail.lookupDetail;
            final SimpleExpression<String> pointsType = new CaseBuilder()
                    .when(qld.code.eq(Expressions.constant(POINT_REWARD_TYPE_CODE)))
                    .then(Expressions.stringTemplate(String.format("'%s'", ReportCategory.POINT.name())))
                    .when(qld.code.equalsIgnoreCase(ITEM_POINT_REWARD_TYPE_CODE))
                    .then(Expressions.stringTemplate(String.format("'%s'", ReportCategory.ITEM_POINT.name())))
                    .when(qld.code.equalsIgnoreCase(POINT_REWARD_TYPE_REDEEM_ITEM_CODE))
                    .then(Expressions.stringTemplate(String.format("'%s'", ReportCategory.TOTAL_POINTS_REDEEMED.name())))
                    .otherwise(Expressions.stringTemplate(String.format("'%s'", "default")));

            final List<CrosstabBean> earnPointGroup = new JPAQuery(em).from(qp, qta, qpromo, qld)
                    .where(qp.transactionType.eq(PointTxnType.EARN)
                            .and(qp.transactionNo.eq(qta.transactionNo))
                            .and(qta.promotion.id.eq(qpromo.id))
                            .and(qpromo.rewardType.code.eq(qld.code))
                            .and(qp.transactionDateTime.between(dateFrom, dateTo)))
                    .groupBy(qld.code, qp.transactionDateTime.yearMonth())
                    .list(Projections.constructor(CrosstabBean.class, qp.transactionDateTime.yearMonth(),
                                    pointsType, qp.transactionPoints.sum()));

            final Map<Date, Double> redeemMap = from(cb, allPointTypes)
                    .where($(cb.getColumn()).eq(PointTxnType.REDEEM.name()))
                    .transform(groupBy($(cb.getDate()))
                            .as(sum($(cb.getValue()))));

            for (Entry<Date, Double> entry : redeemMap.entrySet()) {
                ds.add(new CrosstabBean(entry.getKey(), ReportCategory.TOTAL_POINTS_REDEEMED.name(),
                        entry.getValue()));
            }

            Collection<CrosstabBean> redeemPercentage = Collections2.transform(redeemMap.entrySet(),
                    new Function<Map.Entry<Date, Double>, CrosstabBean>() {

                        @Override
                        public CrosstabBean apply(Entry<Date, Double> entry) {
                            Double redeem = redeemMap.get(entry.getKey());
                            redeem = (null == redeem) ? 0.0 : redeem;
                            Double totalPointsV = totalPointsMap.get(entry.getKey());
                            Double percentage = (null == totalPointsV || totalPointsV == 0) ? null : Math.abs(redeem) / totalPointsV;
                            return new CrosstabBean(entry.getKey(), ReportCategory.POINTS_REDEEMED_PERCENTAGE.name(),
                                    percentage);
                        }
                    });

            final Map<Date, Double> epointsMap = from(cb, allPointTypes).where($(cb.getColumn())
                    .in(PointTxnType.RECON.name(), PointTxnType.REWARD.name()))
                    .transform(groupBy($(cb.getDate()))
                            .as(sum($(cb.getValue()))));

            ds.addAll(Collections2.transform(epointsMap.entrySet(),
                    crosstabFunction(ReportCategory.EMPLOYEE_RECON.name())));

            List<CrosstabBean> allPointsWithoutRedeem = from(cb, allPointTypes).where($(cb.getColumn())
                    .notIn(PointTxnType.REDEEM.name(), PointTxnType.EARN.name()))
                    .list($(cb));

            // update column referencing ReportCategory ordinal ordering
            for (CrosstabBean bean : allPointsWithoutRedeem) {
                if (PointTxnType.REDEEM.name().equalsIgnoreCase(bean.getColumn())) {
                    bean.setColumn(ReportCategory.TOTAL_POINTS_REDEEMED.name());
                } else if (PointTxnType.VOID.name().equalsIgnoreCase(bean.getColumn())) {
                    bean.setColumn(ReportCategory.VOID.name());
                } else if (PointTxnType.ADJUST.name().equalsIgnoreCase(bean.getColumn())) {
                    bean.setColumn(ReportCategory.ADJUSTMENT.name());
                } else if (PointTxnType.RETURN.name().equalsIgnoreCase(bean.getColumn())) {
                    bean.setColumn(ReportCategory.RETURN.name());
                } else if (PointTxnType.REWARD.name().equalsIgnoreCase(bean.getColumn())) {
                    bean.setColumn(ReportCategory.EMPLOYEE_POINT.name());
                }
            }

            ds.addAll(totalPoints);
            ds.addAll(redeemPercentage);
            ds.addAll(earnPointGroup);
            ds.addAll(allPointsWithoutRedeem);

            Map<Date, Double> transform = from(cb, ds).where($(cb.getColumn())
                    .notIn(ReportCategory.POINTS_REDEEMED_PERCENTAGE.name(),
                            ReportCategory.TOTAL_POINTS_ISSUED.name()))
                    .transform(groupBy($(cb.getDate())).as(sum($(cb.getValue()))));

            ds.addAll(Collections2.transform(transform.entrySet(),
                    crosstabFunction(ReportCategory.CLOSING_BALANCE.name())));

            Map<String, Double> ytdMap = from(cb, ds).transform(groupBy($(cb.getColumn())).as(sum($(cb.getValue()))));
            Double pointValue = getPointValue();
            String ytd = messageSource.getMessage("points.summary.report.column.points.ytd", new Object[]{String.valueOf(tym.getYear())});
            String ytd_rph = messageSource.getMessage("points.summary.report.column.points.ytd.rph", new Object[]{String.valueOf(tym.getYear())});
            Double ttp = 0.0;
            Double tpr = 0.0;
            Date latestDate = LocalDate.fromDateFields(dateTo).plusMonths(1).toDate();
            for (Entry<String, Double> entry : ytdMap.entrySet()) {
                if (entry.getKey().equalsIgnoreCase(ReportCategory.TOTAL_POINTS_ISSUED.name())) {
                    ttp = entry.getValue();
                } else if (entry.getKey().equalsIgnoreCase(ReportCategory.TOTAL_POINTS_REDEEMED.name())) {
                    tpr = entry.getValue();
                } else if (entry.getKey().equalsIgnoreCase(ReportCategory.POINTS_REDEEMED_PERCENTAGE.name())) {
                    continue;
                }

                ds.add(new CrosstabBean(latestDate, entry.getKey(), ytd, entry.getValue()));
                ds.add(new CrosstabBean(latestDate, entry.getKey(), ytd_rph, entry.getValue() * pointValue));
            }

            if (!ds.isEmpty()) {
                Double tr = tpr / ttp;
                ds.add(new CrosstabBean(latestDate, ReportCategory.POINTS_REDEEMED_PERCENTAGE.name(), ytd, tr));
                ds.add(new CrosstabBean(latestDate, ReportCategory.POINTS_REDEEMED_PERCENTAGE.name(), ytd_rph, tr));
            }

            fixData(ds, dateFrom, dateTo);
            Collections.sort(ds, createComparator());

            DateTimeFormatter ymformatter = DateTimeFormat.forPattern("MMM yyyy")
                    .withLocale(LocaleContextHolder.getLocale());
            // convert to il8n message
            for (CrosstabBean crosstabBean : ds) {
                crosstabBean.setColumn(messageSource.getMessage(
                        ReportCategory.valueOf(crosstabBean.getColumn()).messageCode()));

                if (crosstabBean.getDate() != latestDate) {
                    String row = YearMonth.fromDateFields(crosstabBean.getDate()).toString(ymformatter);
                    crosstabBean.setRow(row);
                }
            }

            return ds;
        }

        private Function<Entry<Date, Double>, CrosstabBean> crosstabFunction(final String columnName) {
            return new Function<Map.Entry<Date, Double>, CrosstabBean>() {

                @Override
                public CrosstabBean apply(Entry<Date, Double> entry) {
                    return new CrosstabBean(entry.getKey(), columnName, entry.getValue());
                }
            };
        }

        private Comparator<CrosstabBean> createComparator() {
            ComparatorChain cbc = new ComparatorChain();
            cbc.addComparator(new Comparator<CrosstabBean>() {

                @Override
                public int compare(CrosstabBean o1, CrosstabBean o2) {
                    ReportCategory r1 = ReportCategory.valueOf(o1.getColumn());
                    ReportCategory r2 = ReportCategory.valueOf(o2.getColumn());
                    return PointsSummaryReport.compareIndex(r1.ordinal(), r2.ordinal());
                }
            });
            // Sort by date in ascending order
            cbc.addComparator(new Comparator<CrosstabBean>() {

                @Override
                public int compare(CrosstabBean o1, CrosstabBean o2) {
                    return DateTimeComparator.getDateOnlyInstance().compare(o1.getDate(), o2.getDate());
                }
            });

            return cbc;
        }

        @Override
        public Set<FilterField> getFilters() {
            Set<FilterField> filters = Sets.newLinkedHashSetWithExpectedSize(2);
            filters.add(FilterField.createDateField(FILTER_DATE_FROM,
                    messageSource.getMessage("points.summary.commons.date.from.filter"),
                    FILTER_MONTH_YEAR_FORMAT, FilterField.DateTypeOption.YEAR_MONTH, true));
            filters.add(FilterField.createDateField(FILTER_DATE_TO,
                    messageSource.getMessage("points.summary.commons.date.to.filter"),
                    FILTER_MONTH_YEAR_FORMAT, FilterField.DateTypeOption.YEAR_MONTH, true));

            return filters;
        }

        @Override
        public JRProcessor createJrProcessor(Map<String, String> map) {
            // will be used first by getCollectionDataSource - for filtering
            Map<String, Object> reportParam = Maps.newHashMap();
            reportParam.put(REPORT_PARAM_DATE_FROM, map.get(FILTER_DATE_FROM));
            reportParam.put(REPORT_PARAM_DATE_TO, map.get(FILTER_DATE_TO));

            // TODO: Refactor API
            JRProcessor processor = super.template(reportParam);
            DateTimeFormatter formatter = DateTimeFormat.forPattern("MMMMM-yyyy")
                    .withLocale(LocaleContextHolder.getLocale());
            YearMonth ymf = YearMonth.parse(map.get(FILTER_DATE_FROM), formatter);
            YearMonth ymt = YearMonth.parse(map.get(FILTER_DATE_TO), formatter);
            // TODO: refactoring
            reportParam.put(REPORT_PARAM_DATE_FROM, ymf.toString("MMMMM yyyy", LocaleContextHolder.getLocale()));
            reportParam.put(REPORT_PARAM_DATE_TO, ymt.toString("MMMMM yyyy", LocaleContextHolder.getLocale()));
            reportParam.put(REPORT_PARAM_TARGET_YEAR, ymt.getYear());
            reportParam.put(REPORT_PARAM_POINT_VALUE, getPointValue());
            // override
            processor.addParameters(reportParam);
            return processor;
        }

        private void fixData(List<CrosstabBean> ds, Date dateFrom, Date dateTo) {
            final LocalDate start = LocalDate.fromDateFields(dateFrom);
            final LocalDate end = LocalDate.fromDateFields(dateTo);
            for (int i = 0; i <= Months.monthsBetween(start, end).getMonths(); i++) {
                LocalDate month = start.plusMonths(i);
                for (ReportCategory reportCategory : ReportCategory.values()) {
                    ds.add(new CrosstabBean(month.toDate(), reportCategory.name(), 0.0));
                }
            }
        }

        @Override
        public Set<Option> customizerOption() {
            return Sets.newHashSet(ReportsCustomizer.Option.DATE_RANGE_FULL_VALIDATION);
        }

        @Override
        public DateTimeFormatter getDefaultDateFormat() {
            return DateTimeFormat.forPattern("MMMMM-yyyy")
                    .withLocale(LocaleContextHolder.getLocale());
        }
    }

    @Service("dailyPointsSummaryReport")
    public final static class Daily extends PointsSummaryReport implements ReportsCustomizer, ReportDateFormatProvider {

        protected final static String FILTER_DATE_FROM = "dateFrom";
        protected final static String FILTER_DATE_TO = "dateTo";
        protected final static String REPORT_PARAM_DATE_FROM = "DATE_FROM";
        protected final static String REPORT_PARAM_DATE_TO = "DATE_TO";
        private static Map<String, Integer> newOrdering;
        private static final List<String> EXCLUDED = Lists.newArrayList(
                PointTxnType.RECON.name(), PointTxnType.REWARD.name(),
                PointTxnType.RETURN.name(), PointTxnType.VOID.name(),
                PointTxnType.RETRIEVE.name());

        public Daily() {
            super("Point Summary Daily Report",
                    "reports/point_summary_daily_report.jasper",
                    "points.summary.daily.report.title",
                    "REPORT_POINTS_SUMMARY_DAILY_VIEW");
        }

        @Override
        protected Collection<?> getCollectionDatasource(Map<String, Object> reportParam) {
            newOrdering = new ImmutableMap.Builder<String, Integer>()
                    .put(PointTxnType.EARN.name(), 1)
                    .put(messageSource.getMessage("points.summary.report.category.empl_issued.label"), 2)
                    .put(PointTxnType.ADJUST.name(), 3)
                    .put(messageSource.getMessage("points.summary.report.category.reversal.label"), 4)
                    .put(PointTxnType.REDEEM.name(), 5)
                    .put(PointTxnType.EXPIRE.name(), 6)
                    .build();

            QPointsTxnModel qp = pointsTxnModel;
            Date dateFrom = parseDate(reportParam.get(REPORT_PARAM_DATE_FROM)).toDate();
            Date dateTo = parseDate(reportParam.get(REPORT_PARAM_DATE_TO)).toDate();

            // trunc(dateInstance, 'DD') function was tested only on Oracle and H2
            final DateExpression<Date> txnDateExpression = super.truncate(qp.transactionDateTime, "DD");
            // get the sum of all points before the start date
            Double baseBalance = new JPAQuery(em).from(qp).where(qp.transactionDateTime
                    .before(dateFrom)).uniqueResult(qp.transactionPoints.sum());

            List<CrosstabBean> listByType = new JPAQuery(em).from(qp)
                    .where(qp.transactionDateTime.between(dateFrom, dateTo)
                            .and(qp.transactionPoints.ne(0.0)))
                    .groupBy(qp.transactionType.stringValue(), txnDateExpression)
                    .orderBy(txnDateExpression.asc())
                    .list(Projections.constructor(CrosstabBean.class,
                                    txnDateExpression,
                                    qp.transactionType.stringValue(),
                                    qp.transactionPoints.sum()));

            Double eb = getEndingBalanceFromAllTypes(listByType);
            Double endingBalance = (baseBalance == null ? 0.0 : baseBalance) + (eb == null ? 0.0 : eb);

            LocalDate lastDateWithData = null;
            if (listByType.iterator().hasNext()) {
                lastDateWithData = LocalDate.fromDateFields(
                        listByType.get(listByType.size() - 1).getDate());
            }

            ArrayList<CrosstabBean> ds = Lists.newArrayList();
            ds.addAll(getTotalPointsReversal(listByType));
            ds.addAll(getEmployeePointsIssued(listByType));
            ds.addAll(getStartingBalances(listByType, baseBalance));
            ds.addAll(filter(listByType, EXCLUDED));

            fixData(ds, dateFrom, dateTo, lastDateWithData, endingBalance);
            // Convert the extracted date since it has the format of MIDNIGHT
            // e.g 2014-03-06 12:00:00.0 [yyyy-MM-dd HH:mm:ss.s]
            // report need a format of [dd-MM-yyyy]
            // this formatting will not be handled by jasper even it can be
            // for future refactoring the same bean will be used in Monthly/Yearly report
            for (CrosstabBean bean : ds) {
                bean.setRow(LocalDate.fromDateFields(bean.getDate())
                        .toString(DateUtil.SEARCH_DATE_FORMAT));
            }

            // sort by date and column esp starting balance must be on first column
            Collections.sort(ds, createComparator());
            return ds;
        }

        @Override
        public Set<FilterField> getFilters() {
            LinkedHashSet<FilterField> filters = Sets.newLinkedHashSet();
            filters.add(FilterField.createDateField(FILTER_DATE_FROM,
                    messageSource.getMessage("points.summary.commons.date.from.filter"), FILTER_DATE_FROM, true));
            filters.add(FilterField.createDateField(FILTER_DATE_TO,
                    messageSource.getMessage("points.summary.commons.date.to.filter"), FILTER_DATE_TO, true));

            return filters;
        }

        @Override
        public JRProcessor createJrProcessor(Map<String, String> map) {
            Map<String, Object> reportParam = Maps.newHashMap();
            reportParam.put(REPORT_PARAM_DATE_FROM, map.get(FILTER_DATE_FROM));
            reportParam.put(REPORT_PARAM_DATE_TO, map.get(FILTER_DATE_TO));
            // TODO: Refactor API
            JRProcessor template = super.template(reportParam);
            // override report param
            template.addParameters(overrideReportParam(map));
            return template;
        }

        private Map<String, Object> overrideReportParam(Map<String, String> map) {
            DateTimeFormatter formatter = DateTimeFormat.forPattern("MMMM dd, yyyy")
                    .withLocale(LocaleContextHolder.getLocale());
            String dateFrom = formatter.print(parseDate(map.get(FILTER_DATE_FROM)));
            String dateTo = formatter.print(parseDate(map.get(FILTER_DATE_TO)));
            Map<String, Object> reportParam = Maps.newHashMap();
            reportParam.put(REPORT_PARAM_DATE_FROM, dateFrom);
            reportParam.put(REPORT_PARAM_DATE_TO, dateTo);
            return reportParam;
        }

        private LocalDate parseDate(Object parseableDate) {
            DateTimeFormatter df = DateTimeFormat.forPattern(DateUtil.SEARCH_DATE_FORMAT);
            return df.parseLocalDate(String.valueOf(parseableDate));
        }

        private Comparator<CrosstabBean> createComparator() {
            ComparatorChain cbc = new ComparatorChain();
            // Sort by date in ascending order
            cbc.addComparator(new Comparator<CrosstabBean>() {

                @Override
                public int compare(CrosstabBean o1, CrosstabBean o2) {
                    return DateTimeComparator.getDateOnlyInstance().compare(o1.getDate(), o2.getDate());
                }
            });

            // the opening/starting balance must be in the first column of the crosstab
            cbc.addComparator(new Comparator<CrosstabBean>() {

                @Override
                public int compare(CrosstabBean o1, CrosstabBean o2) {
                    return compareIndex(newIndex(o1.getColumn()), newIndex(o2.getColumn()));
                }

                private int newIndex(String ptt) {
                    Integer index = newOrdering.get(ptt);
                    return index == null ? 0 : index;
                }
            });

            return cbc;
        }

        private void fixData(List<CrosstabBean> ds, Date dateFrom, Date dateTo, LocalDate lastDateWithData, Double endingBalance) {
            final LocalDate start = LocalDate.fromDateFields(dateFrom);
            final LocalDate end = LocalDate.fromDateFields(dateTo);
            final String startBalanceLabel = messageSource.getMessage("point.summary.opening_balance.label");
            for (int i = 0; i <= Days.daysBetween(start, end).getDays(); i++) {
                LocalDate day = start.plusDays(i);
                Double value = 0.0;
                for (Entry<String, Integer> entry : newOrdering.entrySet()) {
                    ds.add(new CrosstabBean(day.toDate(), entry.getKey(), 0.0));
                }
                if (lastDateWithData == null || day.isAfter(lastDateWithData)) {
                    value = endingBalance;
                }
                ds.add(new CrosstabBean(day.toDate(), startBalanceLabel, value));
            }
        }

        private List<CrosstabBean> getTotalPointsReversal(List<CrosstabBean> listByType) {
            CrosstabBean cb = alias(CrosstabBean.class, "cb");
            Map<Date, Double> transform = from(cb, listByType)
                    .where($(cb.getColumn())
                            .in(PointTxnType.VOID.name(), PointTxnType.RETURN.name()))
                    .orderBy($(cb.getDate()).asc())
                    .transform(groupBy($(cb.getDate())).as(sum($(cb.getValue()))));
            String label = messageSource.getMessage("points.summary.report.category.reversal.label");
            return transform(transform, label);
        }

        private List<CrosstabBean> getEmployeePointsIssued(List<CrosstabBean> listByType) {
            CrosstabBean cb = alias(CrosstabBean.class, "cb");
            Map<Date, Double> transform = from(cb, listByType)
                    .where($(cb.getColumn())
                            .in(PointTxnType.REWARD.name(), PointTxnType.RECON.name()))
                    .orderBy($(cb.getDate()).asc())
                    .transform(groupBy($(cb.getDate())).as(sum($(cb.getValue()))));
            String label = messageSource.getMessage("points.summary.report.category.empl_issued.label");
            return transform(transform, label);
        }

        private List<CrosstabBean> transform(Map<Date, Double> map, final String columnName) {
            Collection<CrosstabBean> transformed = Collections2.transform(map.entrySet(),
                    new Function<Map.Entry<Date, Double>, CrosstabBean>() {

                        @Override
                        public CrosstabBean apply(Map.Entry<Date, Double> f) {
                            return new CrosstabBean(f.getKey(), columnName, f.getValue());
                        }

                    });
            return new ArrayList<CrosstabBean>(transformed);
        }

        private List<CrosstabBean> getStartingBalances(List<CrosstabBean> listByType, Double baseBalance) {
            CrosstabBean cb = alias(CrosstabBean.class, "cb");
            Map<Date, Double> transform = from(cb, listByType)
                    .orderBy($(cb.getDate()).asc())
                    .transform(groupBy($(cb.getDate())).as(sum($(cb.getValue()))));
            List<CrosstabBean> startingBalances = new ArrayList<CrosstabBean>();
            Double startingBalance = baseBalance == null ? 0.0 : baseBalance;
            final String startBalanceLabel = messageSource.getMessage("point.summary.opening_balance.label");
            for (Entry<Date, Double> entry : transform.entrySet()) {
                startingBalances.add(new CrosstabBean(entry.getKey(), startBalanceLabel, "", startingBalance));
                startingBalance += entry.getValue();
            }

            return startingBalances;
        }

        private Collection<? extends CrosstabBean> filter(List<CrosstabBean> listByType, List<String> excluded) {
            CrosstabBean cb = alias(CrosstabBean.class, "cb");
            return from(cb, listByType).where($(cb.getColumn())
                    .notIn(excluded)).list($(cb));
        }

        private Double getEndingBalanceFromAllTypes(List<CrosstabBean> listByType) {
            CrosstabBean cb = alias(CrosstabBean.class);
            return from(cb, listByType).uniqueResult($(cb.getValue()).sum());
        }

        @Override
        public DateTimeFormatter getDefaultDateFormat() {
            return DateTimeFormat.forPattern(DateUtil.SEARCH_DATE_FORMAT);
        }

        @Override
        public Set<Option> customizerOption() {
            return Sets.newHashSet(Option.DATE_RANGE_FULL_VALIDATION);
        }
    }

    public static class CrosstabBean {

        private Date date;
        private String column;
        private String row;
        private Double value;

        public CrosstabBean() {
        }

        public CrosstabBean(Date date, String column, String row, Double value) {
            this.date = date;
            this.column = column;
            this.row = row;
            this.value = value;
        }

        public CrosstabBean(int param, String column, Double value) {
            String param$ = "" + param;
            Date date = null;
            if (param$.length() > 4) {
                date = YearMonth.parse(param$, DateTimeFormat.forPattern("yyyyMM"))
                        .toLocalDate(1).toDate();
                this.column = column;
            } else {
                Calendar cal = Calendar.getInstance();
                cal.set(Calendar.YEAR, param);
                date = cal.getTime();
                this.row = column;
            }

            this.date = date;
            this.value = value;
        }

        public CrosstabBean(Date date, String column, Double value) {
            this(date, column, "", value);
        }

        public Date getDate() {
            return date;
        }

        public void setDate(Date date) {
            this.date = date;
        }

        public Double getValue() {
            return value;
        }

        public void setValue(Double value) {
            this.value = value;
        }

        public String getColumn() {
            return column;
        }

        public void setColumn(String column) {
            this.column = column;
        }

        public String getRow() {
            return row;
        }

        public void setRow(String row) {
            this.row = row;
        }
    }

}
