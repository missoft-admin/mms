package com.transretail.crm.core.dto;


import java.util.ArrayList;
import java.util.List;

import com.transretail.crm.core.entity.enums.AuthenticationType;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.BooleanUtils;
import org.apache.commons.lang3.StringUtils;

import com.transretail.crm.core.entity.UserModel;


public class UserDto {
	private Long id;
	private String username;
	private String password;
	private Boolean enabled = Boolean.FALSE;
	private String email;
	private String firstName;
	private String middleName;
	private String lastName;
	private String storeCode;
	private String storeDesc;
	private String inventoryLocation;
	private String b2bLocation;
    private AuthenticationType authenticationType;
	private UserRolePermRequestDto rolePerm;

    public UserDto() {
    }

    public UserDto(UserModel inUser) {
        this.id = inUser.getId();
        this.username = inUser.getUsername();
        this.enabled = inUser.getEnabled();
        this.email = inUser.getEmail();
        this.firstName = inUser.getFirstName();
        this.middleName = inUser.getMiddleName();
        this.lastName = inUser.getLastName();
        this.storeCode = inUser.getStoreCode();
        this.inventoryLocation = inUser.getInventoryLocation();
        this.authenticationType = inUser.getAuthenticationType();
        this.b2bLocation = inUser.getB2bLocation();
    }

	public UserModel toModel() {
		return toModel( new UserModel() );
	}

    public UserModel toModel(UserModel inUser) {
        inUser.setUsername(this.username);
        if (StringUtils.isNotBlank(this.password)) {
            inUser.setPassword(this.password);
        }
        inUser.setEnabled(this.enabled);
        inUser.setEmail(this.email);
        inUser.setFirstName(this.firstName);
        inUser.setMiddleName(this.middleName);
        inUser.setLastName(this.lastName);
        inUser.setStoreCode(this.storeCode);
        inUser.setInventoryLocation(this.inventoryLocation);
        inUser.setAuthenticationType(this.authenticationType);
        inUser.setB2bLocation(this.b2bLocation);
        return inUser;
    }

    public static List<UserDto> toDtoList( List<UserModel> models ) {
		if ( CollectionUtils.isEmpty( models ) ) {
			return new ArrayList<UserDto>();
		}

		List<UserDto> dtos = new ArrayList<UserDto>();
		for ( UserModel model : models ) {
			dtos.add( new UserDto( model ) );
		}
		return dtos;
	}





	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public Boolean getEnabled() {
		return enabled;
	}

	public void setEnabled(Boolean enabled) {
        this.enabled = BooleanUtils.toBoolean(enabled);
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getMiddleName() {
		return middleName;
	}

	public void setMiddleName(String middleName) {
		this.middleName = middleName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getStoreCode() {
		return storeCode;
	}

	public void setStoreCode(String storeCode) {
		this.storeCode = storeCode;
	}

	public String getStoreDesc() {
		return storeDesc;
	}

	public void setStoreDesc(String storeDesc) {
		this.storeDesc = storeDesc;
	}

	public String getInventoryLocation() {
		return inventoryLocation;
	}

	public void setInventoryLocation(String inventoryLocation) {
		this.inventoryLocation = inventoryLocation;
	}

	public UserRolePermRequestDto getRolePerm() {
		return rolePerm;
	}

	public void setRolePerm(UserRolePermRequestDto rolePerm) {
		this.rolePerm = rolePerm;
	}

	public String getStoreCodeAndUser() {
		return this.storeCode + " - " + this.username;
	}

	public String getStoreDescAndUser() {
		return this.storeDesc + " - " + this.username;
	}


    public AuthenticationType getAuthenticationType() {
        return authenticationType;
    }

    public void setAuthenticationType(AuthenticationType authenticationType) {
        this.authenticationType = authenticationType;
    }

	public String getB2bLocation() {
		return b2bLocation;
	}

	public void setB2bLocation(String b2bLocation) {
		this.b2bLocation = b2bLocation;
	}
    
}
