package com.transretail.crm.template.service.streaming.excel.handler.cell;

import java.util.Arrays;

import org.apache.commons.lang3.StringUtils;
import org.springframework.context.i18n.LocaleContextHolder;

import com.transretail.crm.core.entity.MemberModel;
import com.transretail.crm.core.entity.embeddable.Channel;
import com.transretail.crm.template.service.MemberTemplateParseException;
import com.transretail.crm.template.service.streaming.excel.handler.sheet.AbstractSheetHandler;

/**
 * @author Allan G. Ramirez (agramirez@exist.com)
 */
public class CommunicationCellHandler implements CellHandler {
    private AbstractSheetHandler sheetHandler;
    private String headerName;

    public CommunicationCellHandler(AbstractSheetHandler sheetHandler, String headerName) {
        this.sheetHandler = sheetHandler;
        this.headerName = headerName;
    }

    @Override
    public void handle(MemberModel dto, String cellText) throws MemberTemplateParseException {
        Channel channel = dto.getChannel();
        if (channel == null) {
            channel = new Channel();
            dto.setChannel(channel);
        }
        if (StringUtils.isNotBlank(cellText)) {
            for (String accept : cellText.split("/")) {
                accept = accept.toUpperCase();
                if ("SMS".equals(accept.toUpperCase())) {
                    channel.setAcceptSMS(true);
                } else if ("TELEPHONE".equals(accept.toUpperCase())) {
                    channel.setAcceptPhone(true);
                } else if ("TELEPON".equals(accept.toUpperCase())) {
                    channel.setAcceptPhone(true);
                } else if ("EMAIL".equals(accept.toUpperCase())) {
                    channel.setAcceptEmail(true);
//                } else if ("SURAT".equals(accept)) {
//                    channel.setAcceptMail(true);
                } else {
                    throw new MemberTemplateParseException(sheetHandler.getMessageSource().getMessage(
                        "member.template.messages.excel.list.values.invalid", new String[]{cellText, headerName,
                            StringUtils.join(Arrays.asList("SMS", "TELEPHONE", "TELEPON", "EMAIL"), ", ")},
                        LocaleContextHolder.getLocale()
                    ));
                }
            }
        }
    }
}
