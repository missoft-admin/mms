package com.transretail.crm.rest.pos.response.dto;

import java.util.List;

import com.transretail.crm.common.service.exception.MessageSourceResolvableException;

/**
 * @author Allan G. Ramirez (agramirez@exist.com)
 */
public class ForReconciliationBean {
    private List<MissingPointsBean> indProfMissingPoints;
    private List<MissingEmpTxnBean> empMissingTxn;
    private MessageSourceResolvableException exception;

    public ForReconciliationBean() {

    }

    public ForReconciliationBean(MessageSourceResolvableException exception) {
        this.exception = exception;
    }

    public ForReconciliationBean(List<MissingPointsBean> indProfMissingPoints,
        List<MissingEmpTxnBean> empMissingTxn) {
        this.indProfMissingPoints = indProfMissingPoints;
        this.empMissingTxn = empMissingTxn;
    }

    public List<MissingPointsBean> getIndProfMissingPoints() {
        return indProfMissingPoints;
    }

    public void setIndProfMissingPoints(List<MissingPointsBean> indProfMissingPoints) {
        this.indProfMissingPoints = indProfMissingPoints;
    }

    public List<MissingEmpTxnBean> getEmpMissingTxn() {
        return empMissingTxn;
    }

    public void setEmpMissingTxn(List<MissingEmpTxnBean> empMissingTxn) {
        this.empMissingTxn = empMissingTxn;
    }

    public MessageSourceResolvableException getException() {
        return exception;
    }

    public void setException(MessageSourceResolvableException exception) {
        this.exception = exception;
    }
}
