<%@ include file="../../common/taglibs.jsp" %>

<fieldset class="form-horizontal container-fluid">
  <legend>
    <spring:message code="member_prof_correspondenceaddress" />
  </legend>
  <c:forEach items="${correspondenceAddress}" var="item">
  
  <!--<label for="marketingDetails.correspondenceAddress" class="control-label"></label>-->
  <div class="radio controls">
    <input type="radio" name="marketingDetails.correspondenceAddress" value="${item.code}"><label
      class="">${item.description}</label>
  </div>
  
  </c:forEach>
</fieldset>


<fieldset class="form-horizontal container-fluid">
  <legend>
    <spring:message code="member_prof_communicationmode" />
  </legend>

  <label for="channel.acceptSms" class="case-cap"></label>
  <div class="checkbox controls">
    <input type="checkbox" name="channel.acceptSMS"> <label
      class="case-cap"><spring:message
        code="label_com_transretail_crm_entity_customermodel_acceptsms" /></label>
  </div>
  <label for="channel.acceptEmail" class="case-cap"></label>
  <div class="checkbox controls" id="acceptEmail">
    <input type="checkbox" name="channel.acceptEmail"> <label
      class="case-cap"><spring:message
        code="label_com_transretail_crm_entity_customermodel_acceptemail" /></label>
  </div>
  <%-- <label for="channel.acceptMail" class="control-label"></label>
  <div class="checkbox controls">
    <input type="checkbox" name="channel.acceptMail"> <label
      class="control-label"><spring:message
        code="label_com_transretail_crm_entity_customermodel_acceptmail" /></label>
  </div> --%>
  <label for="channel.acceptPhone" class="case-cap"></label>
  <div class="checkbox controls">
    <input type="checkbox" name="channel.acceptPhone"> <label
      class="case-cap"><spring:message
        code="label_com_transretail_crm_entity_customermodel_acceptphone" /></label>
  </div>
</fieldset>




<fieldset class="form-horizontal container-fluid">
  <legend>
    <spring:message code="member_prof_informedabout" />
  </legend>
  
  
  <c:forEach items="${informedAbout}" var="item">
  
  <label for="marketingDetails.informedAbout" class="case-cap"></label>
  <div class="checkbox controls">
    <input type="checkbox" name="marketingDetails.informedAbout" data-text="${item.description}" value="${item.code}"><label
      class="case-cap">${item.description}<c:if test="${item.description == 'OTHERS'}">&nbsp;&nbsp;<input type="text" name="marketingDetails.informedAbout" class="informedAboutTxt" disabled="disabled" /></c:if></label>
  </div>
        
  
  </c:forEach>
</fieldset>


<fieldset class="form-horizontal container-fluid">
  <legend>
    <spring:message code="member_prof_interestedproducts" />
  </legend>
  
  
  <c:forEach items="${interestedProducts}" var="item">
  
  <label for="marketingDetails.interestedProducts" class="case-cap"></label>
  <div class="checkbox controls">
    <input type="checkbox" name="marketingDetails.interestedProducts" value="${item.code}"><label
      class="case-cap">${item.description}</label>
  </div>
  
  </c:forEach>
  
</fieldset>


