create table CRM_ID_REF (
        id varchar2(255 char) not null,
        CREATED_DATETIME timestamp,
        UPDATED_DATETIME timestamp,
        ID_TYPE varchar2(50 char),
        MODEL_ID varchar2(50 char),
        primary key (id)
    );