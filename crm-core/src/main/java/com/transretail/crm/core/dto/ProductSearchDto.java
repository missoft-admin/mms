package com.transretail.crm.core.dto;


import java.util.ArrayList;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;

import com.mysema.query.types.expr.BooleanExpression;
import com.transretail.crm.common.service.dto.request.AbstractSearchFormDto;
import com.transretail.crm.core.entity.lookup.QProduct;


public class ProductSearchDto extends AbstractSearchFormDto {

    private String pluId;
    private String sku;
    private List<String> skus;
    private List<String> skuLikes;
    private List<String> itemCodes;
    private List<String> itemCodeLikes;
    private List<String> descLikes;
    private List<String> skuOrDescLikes;



    @Override
    public BooleanExpression createSearchExpression() {
		List<BooleanExpression> expr = new ArrayList<BooleanExpression>();
		QProduct qModel = QProduct.product;

        if ( StringUtils.isNotBlank( pluId ) ) {
        	expr.add( qModel.id.equalsIgnoreCase( pluId ) );
        }
        if ( StringUtils.isNotBlank( sku ) ) {
        	expr.add( qModel.sku.equalsIgnoreCase( sku ) );
        }

		if ( CollectionUtils.isNotEmpty( skus ) && CollectionUtils.isEmpty( skuLikes ) ) {
        	expr.add( qModel.sku.in( skus ) );
		}
		else if ( CollectionUtils.isEmpty( skus ) && CollectionUtils.isNotEmpty( skuLikes ) ) {
			List<BooleanExpression> orExprs = new ArrayList<BooleanExpression>();
			for ( String code : skuLikes ) {
				orExprs.add( qModel.sku.toLowerCase().like( code.toLowerCase() + "%" ) );
			}
			expr.add( BooleanExpression.anyOf( orExprs.toArray(new BooleanExpression[ orExprs.size() ] ) ) );
		}
		else if ( CollectionUtils.isNotEmpty( skus ) &&  CollectionUtils.isNotEmpty( skuLikes ) ) {
			List<BooleanExpression> orExprs = new ArrayList<BooleanExpression>();
			orExprs.add( qModel.sku.in( skus ) );
			for ( String code : skuLikes ) {
				orExprs.add( qModel.sku.toLowerCase().like( code.toLowerCase() + "%" ) );
			}
			expr.add( BooleanExpression.anyOf( orExprs.toArray(new BooleanExpression[ orExprs.size() ] ) ) );
		}

		if ( CollectionUtils.isNotEmpty( itemCodes ) && CollectionUtils.isEmpty( itemCodeLikes ) ) {
        	expr.add( qModel.itemCode.in( itemCodes ) );
		}
		else if ( CollectionUtils.isEmpty( itemCodes ) && CollectionUtils.isNotEmpty( itemCodeLikes ) ) {
			List<BooleanExpression> orExprs = new ArrayList<BooleanExpression>();
			for ( String code : itemCodeLikes ) {
				orExprs.add( qModel.itemCode.toLowerCase().like( code.toLowerCase() + "%" ) );
			}
			expr.add( BooleanExpression.anyOf( orExprs.toArray(new BooleanExpression[ orExprs.size() ] ) ) );
		}
		else if ( CollectionUtils.isNotEmpty( itemCodes ) && CollectionUtils.isNotEmpty( itemCodeLikes ) ) {
			List<BooleanExpression> orExprs = new ArrayList<BooleanExpression>();
			orExprs.add( qModel.itemCode.in( itemCodes ) );
			for ( String code : itemCodeLikes ) {
				orExprs.add( qModel.itemCode.toLowerCase().like( code.toLowerCase() + "%" ) );
			}
			expr.add( BooleanExpression.anyOf( orExprs.toArray(new BooleanExpression[ orExprs.size() ] ) ) );
		}

		if ( CollectionUtils.isNotEmpty( descLikes ) ) {
			List<BooleanExpression> orExprs = new ArrayList<BooleanExpression>();
			for ( String code : descLikes ) {
				orExprs.add( qModel.description.toLowerCase().like( "%" + code.toLowerCase() + "%" ) );
			}
			expr.add( BooleanExpression.anyOf( orExprs.toArray(new BooleanExpression[ orExprs.size() ] ) ) );
		}

		if ( CollectionUtils.isNotEmpty( skuOrDescLikes ) ) {
			List<BooleanExpression> orExprs = new ArrayList<BooleanExpression>();
			for ( String code : skuOrDescLikes ) {
				orExprs.add( qModel.sku.toLowerCase().like( code.toLowerCase() + "%" ) );
				orExprs.add( qModel.description.toLowerCase().like( "%" + code.toLowerCase() + "%" ) );
			}
			expr.add( BooleanExpression.anyOf( orExprs.toArray(new BooleanExpression[ orExprs.size() ] ) ) );
		}

		return BooleanExpression.allOf( expr.toArray(new BooleanExpression[ expr.size() ] ) );
    }



	public String getPluId() {
		return pluId;
	}
	public void setPluId(String pluId) {
		this.pluId = pluId;
	}
	public String getSku() {
		return sku;
	}
	public void setSku(String sku) {
		this.sku = sku;
	}
	public List<String> getSkus() {
		return skus;
	}
	public void setSkus(List<String> skus) {
		this.skus = skus;
	}
	public List<String> getSkuLikes() {
		return skuLikes;
	}
	public void setSkuLikes(List<String> skuLikes) {
		this.skuLikes = skuLikes;
	}
	public List<String> getItemCodes() {
		return itemCodes;
	}
	public void setItemCodes(List<String> itemCodes) {
		this.itemCodes = itemCodes;
	}
	public List<String> getItemCodeLikes() {
		return itemCodeLikes;
	}
	public void setItemCodeLikes(List<String> itemCodeLikes) {
		this.itemCodeLikes = itemCodeLikes;
	}
	public List<String> getDescLikes() {
		return descLikes;
	}
	public void setDescLikes(List<String> descLikes) {
		this.descLikes = descLikes;
	}
	public List<String> getSkuOrDescLikes() {
		return skuOrDescLikes;
	}
	public void setSkuOrDescLikes(List<String> skuOrDescLikes) {
		this.skuOrDescLikes = skuOrDescLikes;
	}

}
