package com.transretail.crm.core.repo;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.transretail.crm.common.repo.CrmQueryDslPredicateExecutor;
import com.transretail.crm.core.entity.EmployeePurchaseTxnModel;
import com.transretail.crm.core.repo.custom.EmployeePurchaseTxnRepoCustom;

@Repository
public interface EmployeePurchaseTxnRepo extends CrmQueryDslPredicateExecutor<EmployeePurchaseTxnModel, String>, EmployeePurchaseTxnRepoCustom {

	List<EmployeePurchaseTxnModel> findByTransactionNo( String inTransactionNo );

}