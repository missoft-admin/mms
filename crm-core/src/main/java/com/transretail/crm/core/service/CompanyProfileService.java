package com.transretail.crm.core.service;

import com.transretail.crm.core.entity.CompanyProfile;

public interface CompanyProfileService {

	void saveProfile(CompanyProfile companyProfile);

	CompanyProfile retrieveCompany();

}
