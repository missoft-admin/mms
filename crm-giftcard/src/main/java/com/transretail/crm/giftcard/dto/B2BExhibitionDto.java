package com.transretail.crm.giftcard.dto;

import java.math.BigDecimal;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.joda.time.DateTime;
import org.springframework.format.annotation.DateTimeFormat;

import com.google.common.collect.Lists;
import com.mysema.query.annotations.QueryProjection;
import com.transretail.crm.giftcard.entity.B2BExhibition;
import com.transretail.crm.giftcard.entity.B2BExhibitionGc;
import com.transretail.crm.giftcard.entity.B2BExhibitionPayments;


/**
 *
 */
public class B2BExhibitionDto {
	
	private List<B2BExhibitionGcDto> gcs = Lists.newArrayList();
	private List<B2BExhibitionPaymentsDto> payments = Lists.newArrayList();
	private List<B2BExhibitionCountDto> countSummary = Lists.newArrayList();
	private BigDecimal discount = BigDecimal.ZERO;
	private BigDecimal totalTaxableAmt = BigDecimal.ZERO;
	@DateTimeFormat(pattern="dd-MM-yyyy hh:mm:ss aa")
	private DateTime created;
	private Long id;
	private BigDecimal totalPayment;
	private BigDecimal change;
	private Double tax;
	
	private String transactionNo;
	private String kwitansiNo;
	private String b2bLocation;
	
	private String createdUser;
	
	public B2BExhibitionDto() {}
	@QueryProjection
	public B2BExhibitionDto(Long id, DateTime createdDate) {
		this.id = id;
		this.created = createdDate;
	}
	
	public B2BExhibitionDto(B2BExhibition exh) {
		this.discount = exh.getDiscount();
		this.payments = B2BExhibitionPaymentsDto.toDto(exh.getPayments());
		this.gcs = B2BExhibitionGcDto.toDto(exh.getGcs());
		this.created = exh.getCreated();
		this.tax = exh.getTax();
		this.totalPayment = exh.getTotalPayment();
		this.totalTaxableAmt = exh.getTaxableAmount();
		this.id = exh.getId();
		this.transactionNo = exh.getTransactionNo();
		this.kwitansiNo = exh.getKwitansiNo();
		this.b2bLocation = exh.getB2bLocation();
		this.createdUser = exh.getCreateUser();
	}
	
	public B2BExhibition toModel() {
		B2BExhibition exh = new B2BExhibition();
		exh.setDiscount(this.discount);
		exh.setTax(this.tax);
		exh.setTaxableAmount(this.totalTaxableAmt);
		exh.setTotalPayment(this.totalPayment);
		exh.setTransactionNo(this.transactionNo);
		exh.setKwitansiNo(this.kwitansiNo);
		exh.setCreated(this.created);
		exh.setB2bLocation(this.b2bLocation);
		if(exh.getGcs() != null) {
			exh.getGcs().clear();
			if(CollectionUtils.isNotEmpty(this.gcs)) {
				for(B2BExhibitionGcDto dto : this.gcs) {
	                if (dto != null && StringUtils.isNotBlank(dto.getStartingSeries())) {
	                	B2BExhibitionGc item = dto.toModel(exh);
	                    exh.getGcs().add(item);
	                }
	            }
			}
		} else {
			exh.setGcs(B2BExhibitionGcDto.toModel(this.gcs, exh));	
		}
		
		if(exh.getPayments() != null) {
			exh.getPayments().clear();
			if(CollectionUtils.isNotEmpty(this.payments)) {
				for(B2BExhibitionPaymentsDto dto : this.payments) {
	                if (dto != null && dto.getPaymentType() != null) {
	                	B2BExhibitionPayments item = dto.toModel(exh);
	                    exh.getPayments().add(item);
	                }
	            }
			}
		} else {
			exh.setPayments(B2BExhibitionPaymentsDto.toModel(this.payments, exh));	
		}
		
		return exh;
	}
	
	public List<B2BExhibitionGcDto> getGcs() {
		return gcs;
	}
	public void setGcs(List<B2BExhibitionGcDto> gcs) {
		this.gcs = gcs;
	}
	public List<B2BExhibitionPaymentsDto> getPayments() {
		return payments;
	}
	public void setPayments(List<B2BExhibitionPaymentsDto> payments) {
		this.payments = payments;
	}
	public BigDecimal getDiscount() {
		return discount;
	}
	public void setDiscount(BigDecimal discount) {
		this.discount = discount;
	}
	public BigDecimal getTotalTaxableAmt() {
		return totalTaxableAmt;
	}
	public void setTotalTaxableAmt(BigDecimal totalTaxableAmt) {
		this.totalTaxableAmt = totalTaxableAmt;
	}

	public List<B2BExhibitionCountDto> getCountSummary() {
		return countSummary;
	}

	public void setCountSummary(List<B2BExhibitionCountDto> countSummary) {
		this.countSummary = countSummary;
	}
	public DateTime getCreated() {
		return created;
	}
	public String getCreatedFmt() {
		if(created != null)
			return created.toString("dd-MM-yyyy hh:mm:ss aa");
		return "";
	}
	public void setCreated(DateTime created) {
		this.created = created;
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public BigDecimal getTaxedAmount() {
		return totalTaxableAmt.multiply(new BigDecimal(getTax()).divide(new BigDecimal(100)));
	}
	public BigDecimal getTotalAmountDue() {
		return totalTaxableAmt.subtract(getTaxedAmount());
	}
	public BigDecimal getTotalPayment() {
		return totalPayment;
	}
	public void setTotalPayment(BigDecimal totalPayment) {
		this.totalPayment = totalPayment;
	}
	public BigDecimal getChange() {
		return change;
	}
	public void setChange(BigDecimal change) {
		this.change = change;
	}
	public void setTax(Double tax) {
		this.tax = tax;
	}
	public Double getTax() {
		if(tax == null)
			return new Double(0);
		return tax;
	}
	public String getTransactionNo() {
		return transactionNo;
	}
	public void setTransactionNo(String transactionNo) {
		this.transactionNo = transactionNo;
	}
	public String getKwitansiNo() {
		return kwitansiNo;
	}
	public void setKwitansiNo(String kwitansiNo) {
		this.kwitansiNo = kwitansiNo;
	}
	public String getB2bLocation() {
		return b2bLocation;
	}
	public void setB2bLocation(String b2bLocation) {
		this.b2bLocation = b2bLocation;
	}
	public String getCreatedUser() {
		return createdUser;
	}
	public void setCreatedUser(String createdUser) {
		this.createdUser = createdUser;
	}
	
	
	
	
}

