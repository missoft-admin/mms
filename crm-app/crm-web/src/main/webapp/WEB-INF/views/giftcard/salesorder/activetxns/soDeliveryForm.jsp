<%@include file="../../../common/taglibs.jsp"%>


<div id="content_soShippingForm" class="modal hide nofly modal-dialog content_soForm">

  <div class="modal-content">

    <div class="modal-header">
      <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
      <h4><spring:message code="gc.so.no" />: ${salesOrder.orderNo}</h4>
    </div>

    <div class="modal-body modal-form form-horizontal">
      <div class="hide alert alert-error" id="content_error"><button type="button" class="close" onclick="$(this).parent().hide();">&times;</button><div></div></div>

      <div class="main-block row-fluid"><div class="span12"><%@include file="sgmntSoInfo.jsp" %></div></div>
      <div class="main-block row-fluid"><div class="span12"><%@include file="tblSoItemAllocView.jsp" %></div></div>


      <div class="main-block row-fluid">
        <div class="span12">    
        <form:form id="soShippingForm" name="soForm" modelAttribute="soForm" method="POST" action="/" class="modal-form form-horizontal">
          <div class="control-group span6">
            <label class="control-label"><spring:message code="gc.so.delivery.types" /></label>
            <div class="controls">
              <c:forEach var="item" items="${deliveryTypes}">
                <div class="radio"><form:radiobutton path="deliveryType" disabled="true" value="${item}" label="${item}" class="deliveryTypes" />
                </div>
              </c:forEach>
            </div>
          </div>

          <div class="control-group span6">
	          <c:if test="${salesOrder.deliveryType ne 'SHIP_TO' }">
	            <c:set var="classHide" value="hide" />
	          </c:if>
	          <div class="control-group ${classHide} shippingFields" id="content_shippingInfo">
	            <label class="control-label"><spring:message code="gc.so.delivery.shippinginfo" /></label>
	            <div class="controls"><form:textarea path="shippingInfo" id="shippingInfo" data-value="${salesOrder.shippingInfo}" cssClass=" span10" /></div>
	          </div>
            
              <c:if test="${salesOrder.orderType != 'VOUCHER' }">
                 <div class="control-group ${classHide} shippingFields" id="content_shippingFee">
                   <label class="control-label"><spring:message code="gc.so.delivery.shippingfee" /></label>
                   <div class="controls"><form:input path="shippingFee" id="shippingFee" data-value="${salesOrder.shippingFee}" readonly="true" cssClass=" span10" /></div>
                 </div>
              </c:if>
            
          </div>
        </form:form>
        </div>
      </div>

    </div>


    <div class="modal-footer">
      <input id="saveDeliveryBtn" class="btn btn-primary" type="button" value="<spring:message code="gc.so.delivery.update" />"
        data-id="${salesOrder.id}" data-url="<c:url value="/giftcard/salesorder/delivery/save/%ID%" />" />
      <input id="cancelBtn" class="btn" type="button" value="<spring:message code="label_cancel" />" data-dismiss="modal" />
    </div>

  </div>


<style type="text/css"> 
<!-- 
  .content_soForm .alert { padding-left: 0px !important; }
  .content_soForm .soorderalloc-block { margin-top: 20px; }
  .content_soForm .checkElement { float: left; margin-right: 5px; }
  .content_soForm .checkElements .controls { margin-left: 150px !important; }
  .content_soForm .checkElements .control-label { width: 150px !important; }
  .content_soForm .pull-center { padding-left: 10px; padding-right: 10px; }
  .content_soForm .modal-footer { text-align: center; } 
  .content_soForm .control-group .controls { margin-top: 5px; margin-left: 110px; }
  .content_soForm .control-group .control-label { width: 100px; text-align: left; }
  .content_soForm .table-col-xlarge { width: 320px; }
--> 
</style>
<script type='text/javascript'>
var SoShippingForm = null;

$(document).ready( function() {

    var errorContainer = $( "#content_soShippingForm" ).find( "#content_error" );
    var $dialog = $( "#content_soShippingForm" );
    var $updateBtn = $dialog.find( "#saveDeliveryBtn" );
    var $cancelBtn = $dialog.find( "#cancelBtn" );
    var $form = $dialog.find( "#soShippingForm" );
    var $deliveryTypes = $dialog.find( ".deliveryTypes" );
    var $deliveryType = $form.find( "input[name='deliveryType']" );
    var $shippingFields = $form.find( ".shippingFields" );
    var $shippingInfo = $form.find( "#shippingInfo" );
    var $shippingFee = $form.find( "#shippingFee" );

    initDialog();
    initFormFields();

    function initDialog() {
        $dialog.modal({ show : true });
        $dialog.on( "hide", function(e) { if ( e.target === this ) { 
            errorContainer.hide(); 
        }});
    }

    function initFormFields() {
        $updateBtn.click( function(e) {
            e.preventDefault();
            $updateBtn.prop( "disabled", true );
            errorContainer.hide(); 

            var url = $(this).data( "url" ).replace( new RegExp( "%ID%", "g" ), $(this).data( "id" ) );
            $.post( url, $form.serialize(), function( resp ) {
                $updateBtn.prop( "disabled", false );
                if ( resp.success ) {
                    $dialog.modal( "hide" );
                    $( "#sales_order_list_container" ).ajaxDataTable( "search" );
                }
            });
        });

        $shippingFee.numberInput({ "type" : "float" });

        $deliveryTypes.click( function(e) {
            if ( this.checked ) {
                if ( $(this).val() == "SHIP_TO" ) {
                    $shippingFields.show( "slow" );
                    $shippingInfo.val( $shippingInfo.data( "value" ) );
                    $shippingFee.val( $shippingFee.data( "value" ) );
                }
                else {
                    $shippingFields.hide();
                    $shippingInfo.val( "" );
                    $shippingFee.val( "" );
                }
            }
        })
    }

    SoShippingForm = {
        show  : function() {
            $dialog.modal( "show" );
        }
    };

});
</script>


</div>