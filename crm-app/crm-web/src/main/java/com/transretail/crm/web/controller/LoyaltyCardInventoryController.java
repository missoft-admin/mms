package com.transretail.crm.web.controller;

import com.google.common.base.Joiner;
import java.beans.PropertyEditorSupport;
import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.BooleanUtils;
import org.apache.commons.lang3.StringUtils;
import org.joda.time.LocalDate;
import org.joda.time.LocalDateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.core.io.FileSystemResource;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.google.common.collect.Lists;
import com.transretail.crm.common.reporting.jasper.JRProcessor;
import com.transretail.crm.common.service.dto.request.PageSortDto;
import com.transretail.crm.common.service.dto.response.ResultList;
import com.transretail.crm.common.util.DateUtil;
import com.transretail.crm.common.web.notification.NotificationType;
import com.transretail.crm.common.web.notification.annotation.Notifiable;
import com.transretail.crm.common.web.notification.annotation.NotificationUpdate;
import com.transretail.crm.core.dto.MessageDto;
import com.transretail.crm.core.entity.enums.ReportType;
import com.transretail.crm.core.entity.lookup.LookupDetail;
import com.transretail.crm.core.security.model.impl.CustomSecurityUserDetailsImpl;
import com.transretail.crm.core.security.util.UserUtil;
import com.transretail.crm.core.service.CodePropertiesService;
import com.transretail.crm.core.service.LookupService;
import com.transretail.crm.core.service.UserService;
import com.transretail.crm.core.util.generator.IdGeneratorService;
import com.transretail.crm.loyalty.dto.AllocationDto;
import com.transretail.crm.loyalty.dto.LCInvResultList;
import com.transretail.crm.loyalty.dto.LCInvSearchDto;
import com.transretail.crm.loyalty.dto.LoyaltyCardInventoryResultList;
import com.transretail.crm.loyalty.dto.LoyaltyCardInventorySearchDto;
import com.transretail.crm.loyalty.dto.LoyaltyCardInventorySummaryResultList;
import com.transretail.crm.loyalty.dto.LoyaltyCardTrackerDto;
import com.transretail.crm.loyalty.dto.LoyaltyCardTrackerFilterDto;
import com.transretail.crm.loyalty.dto.LoyaltyCardTrackingDto;
import com.transretail.crm.loyalty.dto.LoyaltyPhysicalCountDto;
import com.transretail.crm.loyalty.dto.LoyaltyPhysicalCountSummaryDto;
import com.transretail.crm.loyalty.dto.MultipleAllocationDto;
import com.transretail.crm.loyalty.entity.enums.LoyaltyInventoryTransaction;
import com.transretail.crm.loyalty.service.LoyaltyCardHistoryService;
import com.transretail.crm.loyalty.service.LoyaltyCardService;
import com.transretail.crm.loyalty.service.LoyaltyPhysicalCountService;
import com.transretail.crm.loyalty.service.ManufactureOrderService;
import com.transretail.crm.media.service.MailService;
import com.transretail.crm.report.template.impl.LoyaltyTransferDocument;
import com.transretail.crm.web.controller.util.ControllerResponse;
import java.util.*;
//import com.transretail.crm.web.notifications.Notifiable;

@RequestMapping("/loyaltycardinventory")
@Controller
public class LoyaltyCardInventoryController extends AbstractReportsController {
	
	private static final String UI_ATTR_STATUS = "loyaltyCardStatus";
	private static final String UI_ATTR_INACTIVE = "inactive";
	private static final String UI_ATTR_FOR_ALLOCATION = "forAllocation";
	private static final String UI_ATTR_ALLOCATED = "allocated";
	private static final String UI_ATTR_IN_TRANSIT = "inTransit";
	
	private static final String UI_ATTR_SEARCH_DTO = "searchDto";
	private static final String UI_ATTR_ALLOCATE_DTO = "allocateDto";
	private static final String UI_ATTR_MULT_ALLOCATE_DTO = "multipleAllocateDto";
	
	private static final String UI_ATTR_INV_LOCATIONS = "inventoryLocations";
	private static final String UI_ATTR_PRODUCTS = "products";
	private static final String UI_ATTR_ID = "id";
	
	private static final String UI_ATTR_REPORT_TYPE = "reportTypes";
	
	private static final String PARAM_REFERENCE_NO = "referenceNo";
	
	private static final DateTimeFormatter FILTER_DATE_PATTERN = DateTimeFormat.forPattern(DateUtil.SEARCH_DATE_FORMAT);
	
	@Autowired
	LoyaltyTransferDocument loyaltyTransferDocument;
	@Autowired
	LoyaltyCardService loyaltyCardService;
	@Autowired
	LookupService lookupService;
	@Autowired
	MessageSource messageSource;
	@Autowired
	CodePropertiesService codePropertiesService;
	@Autowired
	LoyaltyCardHistoryService loyaltyCardHistoryService;
	@Autowired
	ManufactureOrderService manufactureOrderService;
	@Autowired
	LoyaltyPhysicalCountService loyaltyPhysicalCountService;
	@Autowired
	IdGeneratorService referenceNoGeneratorService;
	@Autowired
	UserService userService;
    @Autowired
    private MailService mailService;
	
	private static final DateTimeFormatter FORMATTER = DateTimeFormat.forPattern("dd-MM-yyyy");
	
	@InitBinder
	void initBinder(WebDataBinder binder) {
		PropertyEditorSupport editor = new PropertyEditorSupport() {
			@Override
			public void setAsText(String text) throws IllegalArgumentException {
				if(StringUtils.isNotBlank(text))
					setValue(FORMATTER.parseLocalDate(text));
				else
					setValue(null);
			}

			@Override
			public String getAsText() throws IllegalArgumentException {
				if (getValue() != null)
					return FORMATTER.print((LocalDate) getValue());
				else
					return "";
			}
		};
		binder.registerCustomEditor(LocalDate.class, editor);
	}

	
	@RequestMapping(value = "/print/{moNo}", method = RequestMethod.GET)
	@ResponseBody
	public FileSystemResource getFile(@PathVariable("moNo") String moNo, HttpServletResponse response) {
		File enc = loyaltyCardService.printLoyaltyInventory(moNo);
		String filename = messageSource.getMessage("label_inventory", null, LocaleContextHolder.getLocale()) + "MO#" + moNo + ".txt";
		response.setHeader("Content-Disposition", "attachment;filename=\"" + filename + "\"");
	    return new FileSystemResource(enc); 
	}
	
	@RequestMapping(produces = "text/html")
    public String list(Model uiModel) {
		addAttrToModel(uiModel);
        return "loyalty/inventory";
    }
	
	private void addAttrToModel(Model uiModel) {
		List<LookupDetail> status = new ArrayList<LookupDetail>();
		status.add(lookupService.getDetailByCode(codePropertiesService.getDetailLoyaltyStatusInactive()));
		status.add(lookupService.getDetailByCode(codePropertiesService.getDetailLoyaltyStatusForAllocation()));
		status.add(lookupService.getDetailByCode(codePropertiesService.getDetailLoyaltyStatusAllocated()));
		status.add(lookupService.getDetailByCode(codePropertiesService.getDetailLoyaltyStatusInTransit()));
		status.add(lookupService.getDetailByCode(codePropertiesService.getDetailLoyaltyStatusTransferIn()));
		status.add(lookupService.getDetailByCode(codePropertiesService.getDetailLoyaltyStatusActive()));
		status.add(lookupService.getDetailByCode(codePropertiesService.getDetailLoyaltyStatusDisabled()));
		status.add(lookupService.getDetailByCode(codePropertiesService.getDetailLoyaltyStatusBurned()));
		status.add(lookupService.getDetailByCode(codePropertiesService.getDetailLoyaltyStatusMissing()));
		status.add(lookupService.getDetailByCode(codePropertiesService.getDetailLoyaltyStatusFound()));
		uiModel.addAttribute(UI_ATTR_STATUS, status);
		/*uiModel.addAttribute(UI_ATTR_STATUS, lookupService.getActiveDetails(lookupService.getHeader(codePropertiesService.getHeaderLoyaltyStatus())));*/
		uiModel.addAttribute(UI_ATTR_INACTIVE, codePropertiesService.getDetailLoyaltyStatusInactive());
		uiModel.addAttribute("transferIn", codePropertiesService.getDetailLoyaltyStatusTransferIn());
		uiModel.addAttribute(UI_ATTR_FOR_ALLOCATION, codePropertiesService.getDetailLoyaltyStatusForAllocation());
		uiModel.addAttribute(UI_ATTR_ALLOCATED, codePropertiesService.getDetailLoyaltyStatusAllocated());
		uiModel.addAttribute(UI_ATTR_IN_TRANSIT, codePropertiesService.getDetailLoyaltyStatusInTransit());
		uiModel.addAttribute("found", codePropertiesService.getDetailLoyaltyStatusFound());
		uiModel.addAttribute(UI_ATTR_PRODUCTS, lookupService.getActiveDetails(lookupService.getHeader(codePropertiesService.getHeaderMembershipTier())));
		uiModel.addAttribute(UI_ATTR_INV_LOCATIONS, loyaltyCardService.getInventoryLocations());
		uiModel.addAttribute("headOfficeLocation", codePropertiesService.getDetailInvLocationHeadOffice());
		
		uiModel.addAttribute("transactionTypes", LoyaltyInventoryTransaction.values());
	}
	
	@RequestMapping(value = "/show",produces = "text/html")
    public String listOne(
            @RequestParam(value = "cardType", required = false) LookupDetail cardType,
            @RequestParam(value = "status", required = false) LookupDetail status,
            @RequestParam(value = "location", required = false) String location,
            @RequestParam(value = "transferTo", required = false) String transferTo,
            Model uiModel) {
		addAttrToModel(uiModel);
		LoyaltyCardInventorySearchDto searchDto = new LoyaltyCardInventorySearchDto();
		if(cardType != null)
			searchDto.setManufactureOrderTier(cardType);
		if(status != null)
			searchDto.setStatus(status.getCode());
		if(StringUtils.isNotBlank(location))
			searchDto.setLocation(location);
		if(StringUtils.isNotBlank(transferTo))
			searchDto.setTransferTo(transferTo);
		uiModel.addAttribute("inventorySearchForm", searchDto);
        return "loyalty/inventory";
    }
	
	@RequestMapping(value="/show/{id}", produces = "text/html") //retain for old notifications
    public String list(
    		@PathVariable("id") String id,
            Model uiModel) {
		addAttrToModel(uiModel);
        uiModel.addAttribute(UI_ATTR_ID, id);
		return "loyalty/inventoryOne";
	}
	
	@RequestMapping(value = "/tracker",produces = "text/html")
    public String track(Model uiModel) {
		uiModel.addAttribute(UI_ATTR_PRODUCTS, lookupService.getActiveDetails(lookupService.getHeader(codePropertiesService.getHeaderMembershipTier())));
		uiModel.addAttribute(UI_ATTR_INV_LOCATIONS, loyaltyCardService.getInventoryLocationsForTracking());
		uiModel.addAttribute(UI_ATTR_REPORT_TYPE, ReportType.values());
		uiModel.addAttribute("inventoryTrackerForm", new LoyaltyCardTrackerFilterDto());
		uiModel.addAttribute("loadTable", false);
        return "loyalty/inventory/tracking";
    }
	
	@RequestMapping(value = "/tracker",produces = "text/html", method = RequestMethod.POST)
    public String track(@ModelAttribute LoyaltyCardTrackerFilterDto inventoryTrackerForm, Model uiModel) {
		uiModel.addAttribute(UI_ATTR_PRODUCTS, lookupService.getActiveDetails(lookupService.getHeader(codePropertiesService.getHeaderMembershipTier())));
		uiModel.addAttribute(UI_ATTR_INV_LOCATIONS, loyaltyCardService.getInventoryLocationsForTracking());
		uiModel.addAttribute(UI_ATTR_REPORT_TYPE, ReportType.values());
		uiModel.addAttribute("inventoryTrackerForm", inventoryTrackerForm);
		uiModel.addAttribute("loadTable", true);
        return "loyalty/inventory/tracking";
    }
	
	/*@RequestMapping(value = "/tracker/search", method = RequestMethod.POST)
    public @ResponseBody ResultList<LoyaltyCardTrackingDto> list(@RequestBody LoyaltyCardTrackerFilterDto filterDto) {
    	return loyaltyCardHistoryService.trackInventory(filterDto);
    }*/
	
	@RequestMapping(value = "/tracker/search", method = RequestMethod.POST)
    public @ResponseBody ResultList<LoyaltyCardTrackerDto> list(@RequestBody LoyaltyCardTrackerFilterDto filterDto) {
    	return loyaltyCardHistoryService.trackInventory2(filterDto);
    }
	
	@RequestMapping(value = "/search", method = RequestMethod.POST)
    public @ResponseBody LoyaltyCardInventorySummaryResultList list(@RequestBody LoyaltyCardInventorySearchDto searchDto) {
    	return loyaltyCardService.searchLoyaltyInventories(searchDto);
    }
	
	@RequestMapping(value = "/search/{id}", method = RequestMethod.POST)
    public @ResponseBody LoyaltyCardInventorySummaryResultList list(@PathVariable String id) {
    	LoyaltyCardInventorySearchDto searchDto = new LoyaltyCardInventorySearchDto();
    	searchDto.setBatchNo(id);
    	return loyaltyCardService.searchLoyaltyInventories(searchDto);
    }
	
	@RequestMapping(value = "/viewitems/{productId}/{status}/{location}/{transferTo}/{newTransferTo}/{forBurning}/{moNo}", method = RequestMethod.GET, produces = "text/html")
    public String viewItems(
    		@PathVariable("productId") String productId,
    		@PathVariable("status") String status,
    		@PathVariable("location") String location,
    		@PathVariable("transferTo") String transferTo,
    		@PathVariable("newTransferTo") String newTransferTo,
    		@PathVariable("forBurning") String forBurning,
    		@PathVariable("moNo") String moNo,
    		Model uiModel) {
		
		LoyaltyCardInventorySearchDto searchDto = new LoyaltyCardInventorySearchDto();
		searchDto.setManufactureOrderTierCode(productId);
		searchDto.setStatus(status);
		searchDto.setLocationCode(location);
		searchDto.setTransferToCode(transferTo);
		searchDto.setTransferTo(loyaltyCardService.getInventoryDesc(transferTo));
		searchDto.setNewTransferToCode(newTransferTo);
		searchDto.setNewTransferTo(loyaltyCardService.getInventoryDesc(newTransferTo));
		searchDto.setForBurning(forBurning);
		searchDto.setManufactureOrder(moNo);
		
		uiModel.addAttribute(UI_ATTR_SEARCH_DTO, searchDto);
		
		return "loyalty/inventory/viewitems";
    }
	
	@RequestMapping(value = "/allocate/{location}", method = RequestMethod.GET, produces = "text/html")
    public String allocate(
    		@PathVariable("location") String location,
    		Model uiModel) {
		
		CustomSecurityUserDetailsImpl userDetails = UserUtil.getCurrentUser();
		
		MultipleAllocationDto dto = new MultipleAllocationDto();
		dto.setLocation(location);
		dto.setLocationName(loyaltyCardService.getInventoryDesc(location));
		dto.setStatus(lookupService.getDetail(codePropertiesService.getDetailLoyaltyStatusInactive()));
		dto.setEncodedBy(userDetails.getUsername());
		
		uiModel.addAttribute(UI_ATTR_MULT_ALLOCATE_DTO, dto);
		uiModel.addAttribute(UI_ATTR_INV_LOCATIONS, loyaltyCardService.getInventoryLocations());
		uiModel.addAttribute(UI_ATTR_PRODUCTS, lookupService.getActiveDetails(lookupService.getHeader(codePropertiesService.getHeaderMembershipTier())));
		uiModel.addAttribute(UI_ATTR_INACTIVE, codePropertiesService.getDetailLoyaltyStatusInactive());
		uiModel.addAttribute(UI_ATTR_ALLOCATED, codePropertiesService.getDetailLoyaltyStatusAllocated());
		uiModel.addAttribute(UI_ATTR_FOR_ALLOCATION, codePropertiesService.getDetailLoyaltyStatusForAllocation());
		return "loyalty/inventory/allocate";
    }
	
	@RequestMapping(value = "/approvealloc/{productId}/{status}/{location}/{transferTo}", method = RequestMethod.GET, produces = "text/html")
    public String approveAllocation(
    		@PathVariable("productId") LookupDetail productId,
    		@PathVariable("status") LookupDetail status,
    		@PathVariable("location") String location,
    		@PathVariable("transferTo") String transferTo,
    		Model uiModel) {
		
		MultipleAllocationDto dto = loyaltyCardService.getInventories(productId, status, location, transferTo, null);
		logger.debug("batch no: " + dto.getBatchRefNo().toString());
		
		uiModel.addAttribute(UI_ATTR_MULT_ALLOCATE_DTO, dto);
		uiModel.addAttribute(UI_ATTR_INV_LOCATIONS, loyaltyCardService.getInventoryLocations());
		uiModel.addAttribute(UI_ATTR_PRODUCTS, lookupService.getActiveDetails(lookupService.getHeader(codePropertiesService.getHeaderMembershipTier())));
		uiModel.addAttribute(UI_ATTR_INACTIVE, codePropertiesService.getDetailLoyaltyStatusInactive());
		uiModel.addAttribute(UI_ATTR_ALLOCATED, codePropertiesService.getDetailLoyaltyStatusAllocated());
		uiModel.addAttribute(UI_ATTR_FOR_ALLOCATION, codePropertiesService.getDetailLoyaltyStatusForAllocation());
		
		return "loyalty/inventory/approveallocation";
    }
	
	@RequestMapping(value = "/approvechangealloc/{productId}/{status}/{location}/{transferTo}/{newTransferTo}", method = RequestMethod.GET, produces = "text/html")
    public String approveChangeAllocation(
    		@PathVariable("productId") LookupDetail productId,
    		@PathVariable("status") LookupDetail status,
    		@PathVariable("location") String location,
    		@PathVariable("transferTo") String transferTo,
    		@PathVariable("newTransferTo") String newTransferTo,
    		Model uiModel) {
		
		MultipleAllocationDto dto = loyaltyCardService.getInventories(productId, status, location, transferTo, newTransferTo);
		logger.debug("batch no: " + dto.getBatchRefNo().toString());
		
		uiModel.addAttribute(UI_ATTR_MULT_ALLOCATE_DTO, dto);
		uiModel.addAttribute(UI_ATTR_INV_LOCATIONS, loyaltyCardService.getInventoryLocations());
		uiModel.addAttribute(UI_ATTR_PRODUCTS, lookupService.getActiveDetails(lookupService.getHeader(codePropertiesService.getHeaderMembershipTier())));
		uiModel.addAttribute(UI_ATTR_INACTIVE, codePropertiesService.getDetailLoyaltyStatusInactive());
		uiModel.addAttribute(UI_ATTR_ALLOCATED, codePropertiesService.getDetailLoyaltyStatusAllocated());
		uiModel.addAttribute(UI_ATTR_FOR_ALLOCATION, codePropertiesService.getDetailLoyaltyStatusForAllocation());
		
		return "loyalty/inventory/approvechangeallocation";
    }
	
	@NotificationUpdate(type=NotificationType.LOYALTY_CHANGE_ALLOCATION)
	@RequestMapping(value = "/changealloc/approve/{status}", method = RequestMethod.POST, produces = "application/json; charset=utf-8")
	@ResponseBody
	public ControllerResponse ajaxApproveAllocate(
			@PathVariable("status") Boolean status,
			@ModelAttribute(value = UI_ATTR_MULT_ALLOCATE_DTO) MultipleAllocationDto dto,
			BindingResult result, HttpServletRequest request) {

		ControllerResponse theResponse = new ControllerResponse();
		theResponse.setSuccess(true);
		
		validateApproval(result, dto);
		
		if (result.hasErrors()) {
			theResponse.setSuccess(false);
			theResponse.setResult(result.getAllErrors());
			return theResponse;
		}

		if (theResponse.isSuccess()) {
			Set<String> remainingBatches = null;
			if(BooleanUtils.isTrue(status))
				remainingBatches = loyaltyCardService.approveAllocateToChange(dto);
			else
				remainingBatches = loyaltyCardService.rejectAllocateToChange(dto);
			
			for(String batchRefNo : dto.getBatchRefNo()) {
				if(!remainingBatches.contains(batchRefNo)) {
					setNotification(theResponse, batchRefNo, dto.getLocationName());
				}
			}
		}

		return theResponse;
	}
	
	@Notifiable(type=NotificationType.LOYALTY_ALLOCATION)
	@RequestMapping(value = "/saveallocation/{status}", method = RequestMethod.POST, produces = "application/json; charset=utf-8")
	@ResponseBody
	public ControllerResponse ajaxAllocate(
			@PathVariable("status") LookupDetail status,
			@ModelAttribute(value = UI_ATTR_MULT_ALLOCATE_DTO) MultipleAllocationDto dto,
			BindingResult result, HttpServletRequest request) {

		ControllerResponse theResponse = new ControllerResponse();
		theResponse.setSuccess(true);
		
		validateAllocation(result, dto);
		
		if (result.hasErrors()) {
			theResponse.setSuccess(false);
			theResponse.setResult(result.getAllErrors());
			return theResponse;
		}

		if (theResponse.isSuccess()) {
			String batchRefNo = referenceNoGeneratorService.generateId();
			loyaltyCardService.allocateInventory(dto, batchRefNo);
			setNotification(theResponse, batchRefNo, dto.getTransferToName());
			theResponse.setNotificationCreation(true);
		}

		return theResponse;
	}
	
	@RequestMapping(value = "/printallocation")
	public void printAllocation(@ModelAttribute(value = UI_ATTR_MULT_ALLOCATE_DTO) MultipleAllocationDto dto,
			HttpServletResponse response) throws Exception {
		JRProcessor jrProcessor = loyaltyCardService.createAllocateJrProcessor(dto);
		renderReport(response, jrProcessor);
	}
	
	private void setNotification(ControllerResponse response, String batchRefNo, String locationName) {
		response.setModelId(batchRefNo);
		response.setResult(locationName);
	}
	
	private void validateAllocation(BindingResult result, MultipleAllocationDto dto) {
		List<LookupDetail> status = Lists.newArrayList(new LookupDetail(codePropertiesService.getDetailLoyaltyStatusFound()),
				new LookupDetail(codePropertiesService.getDetailLoyaltyStatusInactive()), 
				new LookupDetail(codePropertiesService.getDetailLoyaltyStatusTransferIn()));
		String location = dto.getLocation();
		List<AllocationDto> allocations = dto.getAllocations();
		for(int i = 0; i < allocations.size(); i++) {
			AllocationDto product = allocations.get(i);
			if(product.getStartingSeries().length() != 12 || product.getEndingSeries().length() != 12) {
				result.reject(getMessage("loyalty_inv_invalid_series_length", null));
			} else {
				if(!loyaltyCardService.validateInventory(location, null, product.getCardType(), product.getStartingSeries(), product.getEndingSeries(), status)) {
					result.reject(getMessage("loyalty_inv_invalid_series", null));
				} 
				else {
					Long startingSeries = Long.valueOf(product.getStartingSeries());
					Long endingSeries = startingSeries + product.getQuantity() - 1;
					
					for(int j = i + 1; j < allocations.size(); j++) {
						AllocationDto toCompare = allocations.get(j);
						Long toCompareStarting = Long.valueOf(toCompare.getStartingSeries());
						Long toCompareEnding = toCompareStarting + toCompare.getQuantity() - 1;
						
						if((startingSeries >= toCompareStarting && startingSeries <= toCompareEnding)
								|| (endingSeries >= toCompareStarting && endingSeries <= toCompareEnding))
							result.reject(getMessage("loyalty_inv_invalid_series", null));
					}
				}
			}
		}
	}
	
	private String getMessage(String inMsgCode, Object[] inArgMsgCodes) {
		if(inArgMsgCodes != null) {
			for ( int i=0; i < inArgMsgCodes.length; i++ ) {
				inArgMsgCodes[i] = messageSource.getMessage( inArgMsgCodes[i].toString().toLowerCase(), null, LocaleContextHolder.getLocale() );
			}
		}
		return messageSource.getMessage(inMsgCode, inArgMsgCodes, "Info given is invalid.", LocaleContextHolder.getLocale());
	}
	
	
//	@Notifiable(type=NotificationType.LOYALTY_ALLOCATION)
	@NotificationUpdate(type=NotificationType.LOYALTY_ALLOCATION)
	@RequestMapping(value = "/saveallocation/approve/{status}", method = RequestMethod.POST, produces = "application/json; charset=utf-8")
	@ResponseBody
	public ControllerResponse ajaxApproveAllocate(
			@PathVariable("status") LookupDetail status,
			@ModelAttribute(value = UI_ATTR_MULT_ALLOCATE_DTO) MultipleAllocationDto dto,
			BindingResult result, HttpServletRequest request) {

		ControllerResponse theResponse = new ControllerResponse();
		theResponse.setSuccess(true);
		
		validateApproval(result, dto);
		
		if (result.hasErrors()) {
			theResponse.setSuccess(false);
			theResponse.setResult(result.getAllErrors());
			return theResponse;
		}

		if (theResponse.isSuccess()) {
			Set<String> remainingBatches = loyaltyCardService.saveInventory(dto, status);
			if(status.getCode().equals(codePropertiesService.getDetailLoyaltyStatusAllocated())
					|| status.getCode().equals(codePropertiesService.getDetailLoyaltyStatusInactive())) {
				logger.debug("APPROVE ALLOC: " + remainingBatches + " " + dto.getBatchRefNo().toString());
				
				for(String batchRefNo : dto.getBatchRefNo()) {
					if(!remainingBatches.contains(batchRefNo)) {
						setNotification(theResponse, batchRefNo, dto.getLocationName());
					}
				}
			}
		}

		return theResponse;
	}
	
	private void validateApproval(BindingResult result, MultipleAllocationDto dto) {
		boolean valid = false;
		for(AllocationDto allocation:dto.getAllocations()) {
			if(StringUtils.isNotBlank(allocation.getReferenceNo())) {
				valid = true;
				break;
			}
		}
		if(!valid) {
			result.reject(getMessage("loyalty_inv_empty_allocation", null));
		}
	}
	
	
	@RequestMapping(value = "/saveallocation/receive", method = RequestMethod.POST, produces = "application/json; charset=utf-8")
	@ResponseBody
	public ControllerResponse ajaxReceive(
			@ModelAttribute(value = UI_ATTR_MULT_ALLOCATE_DTO) MultipleAllocationDto dto,
			BindingResult result, HttpServletRequest request) {

		ControllerResponse theResponse = new ControllerResponse();
		theResponse.setSuccess(true);
		
		validateReceive(result, dto);
		
		if (result.hasErrors()) {
			theResponse.setSuccess(false);
			theResponse.setResult(result.getAllErrors());
			return theResponse;
		}

		if (theResponse.isSuccess()) {
			loyaltyCardService.receiveInventory(dto);
			Map<String, Long> map = new HashMap<String, Long>();
			Long[] counts = loyaltyCardService.countIncorrectTransferTo(dto);
			map.put("total", counts[0]);
			map.put("incorrect", counts[1]);
			theResponse.setResult(map);
		}

		return theResponse;
	}
	
	@Notifiable(type=NotificationType.LOYALTY_CHANGE_ALLOCATION)
	@RequestMapping(value = "/changeallocation", method = RequestMethod.POST, produces = "application/json; charset=utf-8")
	@ResponseBody
	public ControllerResponse ajaxChangeAllocation(
			@ModelAttribute(value = UI_ATTR_MULT_ALLOCATE_DTO) MultipleAllocationDto dto,
			BindingResult result, HttpServletRequest request) {

		ControllerResponse theResponse = new ControllerResponse();
		theResponse.setSuccess(true);
		
		if (result.hasErrors()) {
			theResponse.setSuccess(false);
			theResponse.setResult(result.getAllErrors());
			return theResponse;
		}

		if (theResponse.isSuccess()) {
			String batchRefNo = referenceNoGeneratorService.generateId();
            List<MessageDto> messageDtos = loyaltyCardService.sendAllocateToChangeForApproval(dto, batchRefNo);
            setNotification(theResponse, batchRefNo, dto.getLocationName());
            theResponse.setNotificationCreation(true);
            for (MessageDto messageDto : messageDtos) {
                mailService.send(messageDto);
            }
        }

		return theResponse;
	}
	
	@RequestMapping(value = "/printreceive")
	public void printReceive(@ModelAttribute(value = UI_ATTR_MULT_ALLOCATE_DTO) MultipleAllocationDto dto,
			HttpServletResponse response) throws Exception {
		JRProcessor jrProcessor = loyaltyCardService.createReceiveJrProcessor(dto);
		renderReport(response, jrProcessor);
	}
	
	private void validateReceive(BindingResult result, MultipleAllocationDto dto) {
		List<LookupDetail> status = Lists.newArrayList(new LookupDetail(codePropertiesService.getDetailLoyaltyStatusInTransit()));
		/*String location = dto.getLocation();*/
		LookupDetail cardType = dto.getCardType();
		for(AllocationDto product : dto.getAllocations()) {
			if(product.getStartingSeries().length() != 12 || product.getEndingSeries().length() != 12) {
				result.reject(getMessage("loyalty_inv_invalid_series_length", null));
			} else {
				if(!loyaltyCardService.validateInventory(null, null, cardType, product.getStartingSeries(), product.getEndingSeries(), status)) {
					result.reject(getMessage("loyalty_inv_invalid_series", null));
				}
			}
		}
	}
	
	@RequestMapping(value = "/transfer/{productId}/{status}/{location}/{transferTo}", method = RequestMethod.GET, produces = "text/html")
    public String transfer(
    		@PathVariable("productId") LookupDetail productId,
    		@PathVariable("status") LookupDetail status,
    		@PathVariable("location") String location,
    		@PathVariable("transferTo") String transferTo,
    		Model uiModel) {
		uiModel.addAttribute(UI_ATTR_MULT_ALLOCATE_DTO, loyaltyCardService.getInventories(productId, status, location, transferTo, null));
		return "loyalty/inventory/transfer";
    }
	
	@RequestMapping(value = "/receive/{productId}/{status}/{location}/{transferTo}", method = RequestMethod.GET, produces = "text/html")
    public String receive(
    		@PathVariable("productId") LookupDetail productId,
    		@PathVariable("status") LookupDetail status,
    		@PathVariable("location") String location,
    		@PathVariable("transferTo") String transferTo,
    		Model uiModel) {
		
		MultipleAllocationDto dto = new MultipleAllocationDto();
		dto.setCardType(productId);
		dto.setTransferTo(transferTo);
		dto.setTransferToName(loyaltyCardService.getInventoryDesc(transferTo));
		dto.setLocation(location);
		dto.setLocationName(loyaltyCardService.getInventoryDesc(location));
		dto.setStatus(status);
		
		uiModel.addAttribute(UI_ATTR_MULT_ALLOCATE_DTO, dto);
		uiModel.addAttribute(UI_ATTR_IN_TRANSIT, codePropertiesService.getDetailLoyaltyStatusInTransit());
		uiModel.addAttribute(UI_ATTR_PRODUCTS, lookupService.getActiveDetails(lookupService.getHeader(codePropertiesService.getHeaderMembershipTier())));
		return "loyalty/inventory/receive";
    }
	
	@RequestMapping(value = "/transfer", method = RequestMethod.POST, produces = "application/json; charset=utf-8")
	@ResponseBody
	public ControllerResponse transfer(@ModelAttribute(value = UI_ATTR_MULT_ALLOCATE_DTO) MultipleAllocationDto dto,
			BindingResult result, HttpServletRequest request) throws Exception {
		ControllerResponse theResponse = new ControllerResponse();
		theResponse.setSuccess(true);
		
		if (theResponse.isSuccess()) {
			loyaltyCardService.saveInventory(dto, new LookupDetail(codePropertiesService.getDetailLoyaltyStatusInTransit()));
		}
		
		return theResponse;
	}
	
	
	@RequestMapping(value = "/printtransfer")
	public void printTransfer(@ModelAttribute(value = UI_ATTR_MULT_ALLOCATE_DTO) MultipleAllocationDto dto,
			HttpServletResponse response) throws Exception {
		
		MultipleAllocationDto transferDto = loyaltyCardService.getInventories(
				dto.getCardType(), 
				new LookupDetail(codePropertiesService.getDetailLoyaltyStatusAllocated()),
				dto.getLocation(), 
				dto.getTransferTo(), null);

		JRProcessor jrProcessor = loyaltyTransferDocument.createJrProcessor(transferDto);
		renderReport(response, jrProcessor);
	}
	
	@RequestMapping(value = "/searchitems", method = RequestMethod.POST)
    public @ResponseBody LoyaltyCardInventoryResultList viewItems(@RequestBody LoyaltyCardInventorySearchDto searchDto) {
    	return loyaltyCardService.searchInventoryItems(searchDto);
    }
	
	@RequestMapping(value = "/reprinttransfer/{productId}/{status}/{location}/{transferTo}")
	public void reprintTransfer(
			@PathVariable("productId") LookupDetail productId,
    		@PathVariable("status") LookupDetail status,
    		@PathVariable("location") String location,
    		@PathVariable("transferTo") String transferTo,
			HttpServletResponse response) throws Exception {
		MultipleAllocationDto transferDto = loyaltyCardService.getInventories(productId, status, location, transferTo, null);
		JRProcessor jrProcessor = loyaltyTransferDocument.createJrProcessor(transferDto);
		renderReport(response, jrProcessor);
	}
	
	@RequestMapping(value = "/reprinttransaction/validate")
	@ResponseBody
    public List<String> validateReprintTxn(
            @RequestParam(required = false) String transactionType,
            @RequestParam(required = false) String username,
            @RequestParam(required = false) LocalDate dateFrom,
            @RequestParam(required = false) LocalDate dateTo) throws Exception {
		
		Locale locale = LocaleContextHolder.getLocale();
        List<String> result = Lists.newArrayList();
        if (StringUtils.isBlank(transactionType)) {
        	String label = messageSource.getMessage("reprint_tranction_type", null, locale);
            result.add(messageSource.getMessage("propkey_msg_notempty", new Object[] {label}, locale));
        } 

        if (StringUtils.isBlank(username)) {
        	String label = messageSource.getMessage("reprint_username", null, locale);
            result.add(messageSource.getMessage("propkey_msg_notempty", new Object[] {label}, locale));
        } else {
        	if(userService.findUserModelIgnoreCase(username) == null) {
        		result.add(messageSource.getMessage("loyalty_msg_invaliduser", null, locale));
        	}
        }

        if (dateFrom == null) {
        	String label = messageSource.getMessage("reprint_date_from", null, locale);
            result.add(messageSource.getMessage("propkey_msg_notempty", new Object[] {label}, locale));
        }
        
        if (dateTo == null) {
        	String label = messageSource.getMessage("reprint_date_to", null, locale);
            result.add(messageSource.getMessage("propkey_msg_notempty", new Object[] {label}, locale));
        }
        
        if(dateFrom != null && dateTo != null && dateFrom.isAfter(dateTo)) {
        	String labelFrom = messageSource.getMessage("reprint_date_From", null, locale);
        	String labelTo = messageSource.getMessage("reprint_date_to", null, locale);
        	result.add(messageSource.getMessage("propkey_msg_greater_than", new Object[] {labelFrom, labelTo}, locale));
        }

        return result;
    }
	
	@RequestMapping(value = "/reprinttransaction")
    public void reprintTxn(
            @RequestParam(required = false) String transactionType,
            @RequestParam(required = false) String username,
            @RequestParam(required = false) LocalDate dateFrom,
            @RequestParam(required = false) LocalDate dateTo,
            HttpServletResponse response) throws Exception {
		
		JRProcessor jrProcessor = loyaltyCardService.createReprintJrProcessor(transactionType, username, dateFrom, dateTo);
		renderReport(response, jrProcessor);
		
    }
	
	@RequestMapping(value = "/print/tracker/{reportType}")
	public void reprintTransfer(@RequestParam Map<String, String> allRequestParams,
			@PathVariable String reportType,
			HttpServletResponse response) throws Exception {
		
		LoyaltyCardTrackerFilterDto filterDto = new LoyaltyCardTrackerFilterDto();
		if(StringUtils.isNotBlank(allRequestParams.get("dateFrom")))
			filterDto.setDateFrom(FILTER_DATE_PATTERN.parseLocalDate(allRequestParams.get("dateFrom")));
		if(StringUtils.isNotBlank(allRequestParams.get("dateTo")))
			filterDto.setDateTo(FILTER_DATE_PATTERN.parseLocalDate(allRequestParams.get("dateTo")));
		filterDto.setProduct(lookupService.getDetail(allRequestParams.get("product")));
		filterDto.setLocation(allRequestParams.get("location"));
		
		JRProcessor jrProcessor = loyaltyCardHistoryService.createJrProcessor(filterDto);
		if(ReportType.PDF.getCode().equals(reportType))
        	renderReport(response, jrProcessor);
        else if(ReportType.EXCEL.getCode().equals(reportType))
        	renderExcelReport(response, jrProcessor, messageSource.getMessage("tracker_report_title", null, LocaleContextHolder.getLocale()));
	}
	
	@RequestMapping(value = "/physicalcount", produces = "text/html")
    public String showPhysicalCountPage(Model uiModel) {
		uiModel.addAttribute("physicalCountForm", new LoyaltyPhysicalCountDto());
		uiModel.addAttribute("reportTypes", ReportType.values());
		uiModel.addAttribute("summaryDates", loyaltyPhysicalCountService.getSummaryDates());
		return "loyalty/physicalcount";
    }
	
	@RequestMapping(value = "/physicalcount/list", method = RequestMethod.POST)
    public @ResponseBody ResultList<LoyaltyPhysicalCountDto> listPhysicalCount(@RequestBody PageSortDto pageDto) {
    	return loyaltyPhysicalCountService.listPhysicalCount(pageDto);
    }
	
	@RequestMapping(value = "/physicalcount/summary", method = RequestMethod.POST)
    public @ResponseBody ResultList<LoyaltyPhysicalCountSummaryDto> listPhysicalCountSummary(@RequestBody PageSortDto pageDto) {
    	return loyaltyPhysicalCountService.getSummary(pageDto);
    }
	
	@RequestMapping(value = "/physicalcount/cardtype/{startingSeries}/{endingSeries}", method = RequestMethod.GET)
	@ResponseBody
    public ControllerResponse getCardType(
    		@PathVariable String startingSeries,
    		@PathVariable String endingSeries) {
		ControllerResponse theResponse = new ControllerResponse();
		theResponse.setSuccess(true);
		
		List<LookupDetail> cardTypes = loyaltyCardService.getCardTypes(startingSeries, endingSeries);
		
		if(cardTypes.size() != 1) {
			theResponse.setSuccess(false);
		} else {
			theResponse.setResult(cardTypes.get(0));
		}
		
		return theResponse;
    }
    
	@RequestMapping(value = "/physicalcount", method = RequestMethod.POST, produces = "application/json; charset=utf-8")
	@ResponseBody
	public ControllerResponse savePhysicalCount(
		@ModelAttribute(value = "physicalCountForm") LoyaltyPhysicalCountDto countDto,
		BindingResult result, HttpServletRequest request) {

	    ControllerResponse theResponse = new ControllerResponse();
	    theResponse.setSuccess(true);
	    if (countDto.getBarcodes().isEmpty() && StringUtils.isNotBlank(countDto.getStartingSeries())) {
		if (countDto.getStartingSeries().length() != 12 || countDto.getEndingSeries().length() != 12 || countDto.getQuantity() < 1 || countDto.getCardType() == null) {
		    result.reject(getMessage("loyalty_inv_invalid_series", null));
		} else if (!loyaltyPhysicalCountService.validateSeries(countDto.getStartingSeries(), countDto.getEndingSeries())) {
		    result.reject(getMessage("loyalty_inv_invalid_series", null));
		}
	    }
	    
	    List<String> invalidBarcodes = new ArrayList<String>();
	    for (String barcode : countDto.getBarcodes()) {
		String series = barcode.substring(0, 12);
		if(!loyaltyPhysicalCountService.validateSeries(series, series)){
		    invalidBarcodes.add(barcode);
		}
	    }
	    
	    if(!invalidBarcodes.isEmpty())
		result.reject(messageSource.getMessage("loyalty_invalid_single_card", 
			new Object[]{Joiner.on(", ").join(invalidBarcodes)}, LocaleContextHolder.getLocale()));
	    
	    if (result.hasErrors()) {
		theResponse.setSuccess(false);
		theResponse.setResult(result.getAllErrors());
		return theResponse;
	    }

	    if (theResponse.isSuccess()) {
		for (String barcode : countDto.getBarcodes()) {
		    String series = barcode.substring(0, 12);
		    LoyaltyPhysicalCountDto dto = new LoyaltyPhysicalCountDto();
		    dto.setStartingSeries(series);
		    dto.setEndingSeries(series);
		    dto.setQuantity(1l);
		    dto.setBoxNo("INDV-NO-BOX"); // just default it
		    dto.setLocation(countDto.getLocation());
		    // TODO: check this code, it may be resource hungry
		    List<LookupDetail> cardTypes = loyaltyCardService.getCardTypes(series, series);
		    Iterator<LookupDetail> iterator =  cardTypes.iterator();
		    if(iterator.hasNext()){
			dto.setCardType(iterator.next());
		    }
		    loyaltyPhysicalCountService.savePhysicalCount(dto);
		}
		
		// if barcodes is empty it means it use the series range
		if (countDto.getBarcodes().isEmpty() && countDto.getQuantity() != null )
		    loyaltyPhysicalCountService.savePhysicalCount(countDto);
	    }

	    return theResponse;
	}
	
	@RequestMapping(value = "/physicalcount/process", method = RequestMethod.GET, produces = "application/json; charset=utf-8")
	@ResponseBody
	public ControllerResponse savePhysicalCount() {
		ControllerResponse theResponse = new ControllerResponse();
		theResponse.setSuccess(true);
		loyaltyPhysicalCountService.processInventory();
		return theResponse;
	}
	
	@RequestMapping(value = "/printphysicalcount/{reportType}")
	public void printPhysicalCount(@PathVariable String reportType,
			HttpServletResponse response) throws Exception {
		JRProcessor jrProcessor = loyaltyPhysicalCountService.createJrProcessor();
		if(ReportType.PDF.getCode().equals(reportType))
        	renderReport(response, jrProcessor);
        else if(ReportType.EXCEL.getCode().equals(reportType))
        	renderExcelReport(response, jrProcessor, messageSource.getMessage("physical_count_label", null, LocaleContextHolder.getLocale()));
	}
	
	@RequestMapping(value = "/printphysicalcount/{reportType}/{dateTime:.+}")
	public void printPhysicalCount(@PathVariable String reportType,
			@PathVariable String dateTime,
			HttpServletResponse response) throws Exception {
		LocalDateTime date = new LocalDateTime(dateTime);
		JRProcessor jrProcessor = loyaltyPhysicalCountService.createJrProcessor(date);
		if(ReportType.PDF.getCode().equals(reportType))
        	renderReport(response, jrProcessor);
        else if(ReportType.EXCEL.getCode().equals(reportType))
        	renderExcelReport(response, jrProcessor, messageSource.getMessage("physical_count_label", null, LocaleContextHolder.getLocale()));
	}
	
	@RequestMapping(value = "/multipleallocation", method = RequestMethod.GET, produces = "text/html")
	public String multipleAllocation(Model uiModel) {
		CustomSecurityUserDetailsImpl userDetails = UserUtil.getCurrentUser();
		MultipleAllocationDto dto = new MultipleAllocationDto();
		dto.setLocation(userDetails.getInventoryLocation());
		dto.setLocationName(loyaltyCardService.getInventoryDesc(userDetails.getInventoryLocation()));
		dto.setEncodedBy(userDetails.getUsername());
		uiModel.addAttribute(UI_ATTR_MULT_ALLOCATE_DTO, dto);
		uiModel.addAttribute(UI_ATTR_INV_LOCATIONS, loyaltyCardService.getInventoryLocations());
		uiModel.addAttribute(UI_ATTR_PRODUCTS, lookupService.getActiveDetails(lookupService.getHeader(codePropertiesService.getHeaderMembershipTier())));
		return "loyalty/multipleallocation";
	}
	
	@Notifiable(type=NotificationType.LOYALTY_ALLOCATION)
	@RequestMapping(value = "/multipleallocation", method = RequestMethod.POST, produces = "application/json; charset=utf-8")
	@ResponseBody
	public ControllerResponse ajaxMultipleAllocate(
			@ModelAttribute(value = UI_ATTR_MULT_ALLOCATE_DTO) MultipleAllocationDto dto,
			BindingResult result, HttpServletRequest request) {
		ControllerResponse theResponse = new ControllerResponse();
		theResponse.setSuccess(true);
		
		validateAllocation(result, dto);
		
		if (result.hasErrors()) {
			theResponse.setSuccess(false);
			theResponse.setResult(result.getAllErrors());
			return theResponse;
		}
		if (theResponse.isSuccess()) {
			String batchRefNo = referenceNoGeneratorService.generateId();
			loyaltyCardService.multipleAllocateInventory(dto, batchRefNo);
			dto.setStatus(new LookupDetail(codePropertiesService.getDetailLoyaltyStatusForAllocation()));
			setNotification(theResponse, batchRefNo, dto.getTransferToName());
			theResponse.setNotificationCreation(true);
		}
		return theResponse;
	}
	
	@RequestMapping(value = "/multipleapproval", method = RequestMethod.GET, produces = "text/html")
	public String multipleApproval(Model uiModel) {
		CustomSecurityUserDetailsImpl userDetails = UserUtil.getCurrentUser();
		MultipleAllocationDto dto = loyaltyCardService.getInventoriesForMultipleApproval(userDetails.getInventoryLocation());
		uiModel.addAttribute(UI_ATTR_MULT_ALLOCATE_DTO, dto);
		uiModel.addAttribute(UI_ATTR_INV_LOCATIONS, loyaltyCardService.getInventoryLocations());
		uiModel.addAttribute(UI_ATTR_INACTIVE, codePropertiesService.getDetailLoyaltyStatusInactive());
		uiModel.addAttribute(UI_ATTR_ALLOCATED, codePropertiesService.getDetailLoyaltyStatusAllocated());
		return "loyalty/multipleapproval";
	}
	
	@RequestMapping(value = "/burnapproval", method = RequestMethod.GET, produces = "text/html")
	public String burnApproval(Model uiModel) {
		MultipleAllocationDto dto = loyaltyCardService.getInventoriesForBurning();
		uiModel.addAttribute(UI_ATTR_MULT_ALLOCATE_DTO, dto);
		return "loyalty/burnapproval";
	}
	
	@RequestMapping(value = "/burn", method = RequestMethod.GET, produces = "text/html")
	public String burn(Model uiModel) {
		CustomSecurityUserDetailsImpl userDetails = UserUtil.getCurrentUser();
		MultipleAllocationDto dto = new MultipleAllocationDto();
		dto.setLocation(userDetails.getInventoryLocation());
		dto.setLocationName(loyaltyCardService.getInventoryDesc(userDetails.getInventoryLocation()));
		dto.setEncodedBy(userDetails.getUsername());
		uiModel.addAttribute(UI_ATTR_MULT_ALLOCATE_DTO, dto);
		uiModel.addAttribute(UI_ATTR_PRODUCTS, lookupService.getActiveDetails(lookupService.getHeader(codePropertiesService.getHeaderMembershipTier())));
		return "loyalty/burn";
	}
	
	@Notifiable(type=NotificationType.LOYALTY_BURN)
	@RequestMapping(value = "/burn", method = RequestMethod.POST, produces = "application/json; charset=utf-8")
	@ResponseBody
	public ControllerResponse ajaxBurn(
			@ModelAttribute(value = UI_ATTR_MULT_ALLOCATE_DTO) MultipleAllocationDto dto,
			BindingResult result, HttpServletRequest request) {
		ControllerResponse theResponse = new ControllerResponse();
		theResponse.setSuccess(true);
		if (theResponse.isSuccess()) {
			String batchRefNo = referenceNoGeneratorService.generateId();
            MessageDto messageDto = loyaltyCardService.saveForBurning(dto, true, batchRefNo);
            setNotification(theResponse, batchRefNo, dto.getLocationName());
            theResponse.setNotificationCreation(true);
            if (messageDto != null) {
                mailService.send(messageDto);
            }
        }
		return theResponse;
	}
	
	@NotificationUpdate(type=NotificationType.LOYALTY_BURN)
	@RequestMapping(value = "/approveburn/{approved}", method = RequestMethod.POST, produces = "application/json; charset=utf-8")
	@ResponseBody
	public ControllerResponse ajaxApproveBurn(
			@ModelAttribute(value = UI_ATTR_MULT_ALLOCATE_DTO) MultipleAllocationDto dto,
			@PathVariable Boolean approved,
			BindingResult result, HttpServletRequest request) {
		ControllerResponse theResponse = new ControllerResponse();
		theResponse.setSuccess(true);
		validateApproval(result, dto);
		if (result.hasErrors()) {
			theResponse.setSuccess(false);
			theResponse.setResult(result.getAllErrors());
			return theResponse;
		}
		if (theResponse.isSuccess()) {
			Set<String> remainingBatches = loyaltyCardService.burnInventory(dto, approved);
			if(dto.getBatchRefNo() != null) {
				for(String batchRefNo : dto.getBatchRefNo()) {
					if(!remainingBatches.contains(batchRefNo)) {
						setNotification(theResponse, batchRefNo, dto.getLocation());
					}
				}
			}
		}
		return theResponse;
	}
	
	@RequestMapping(value = "/validateburn", method = RequestMethod.POST, produces = "application/json; charset=utf-8")
	@ResponseBody
	public ControllerResponse ajaxBurnValidate(
			@ModelAttribute(value = UI_ATTR_MULT_ALLOCATE_DTO) MultipleAllocationDto dto,
			BindingResult result, HttpServletRequest request) {
		ControllerResponse theResponse = new ControllerResponse();
		theResponse.setSuccess(true);
		
		validateBurn(result, dto);
		
		if (result.hasErrors()) {
			theResponse.setSuccess(false);
			theResponse.setResult(result.getAllErrors());
			return theResponse;
		}
		return theResponse;
	}
	
	private void validateBurn(BindingResult result, MultipleAllocationDto dto) {
		List<LookupDetail> status = Lists.newArrayList(new LookupDetail(codePropertiesService.getDetailLoyaltyStatusInactive()),
				/*new LookupDetail(codePropertiesService.getDetailLoyaltyStatusForAllocation()),
				new LookupDetail(codePropertiesService.getDetailLoyaltyStatusAllocated()),*/
				new LookupDetail(codePropertiesService.getDetailLoyaltyStatusTransferIn()),
				new LookupDetail(codePropertiesService.getDetailLoyaltyStatusFound()));

		LookupDetail cardType = dto.getCardType();
		for(AllocationDto product : dto.getAllocations()) {
			if(product.getStartingSeries().length() != 12 || product.getEndingSeries().length() != 12) {
				result.reject(getMessage("loyalty_inv_invalid_series_length", null));
			} else {
				if(!loyaltyCardService.validateInventory(null, null, cardType, product.getStartingSeries(), product.getEndingSeries(), status)) {
					result.reject(getMessage("loyalty_inv_burn_series", null));
				}
			}
		}
	}

    @RequestMapping(value = "/search/all", method = RequestMethod.POST)
    @ResponseBody
    public LCInvResultList searchAll(@RequestBody LCInvSearchDto searchDto) {
        return loyaltyCardService.searchAll(searchDto);
    }

    @RequestMapping(value = "/validate/{barcode}")
    @ResponseBody
    public ControllerResponse validateBarcode(@PathVariable String barcode) { // TODO: rename to validateBarcode
	ControllerResponse response = new ControllerResponse();
	response.setSuccess(true);
	String series = barcode.substring(0, 12);

	if (!loyaltyPhysicalCountService.validateSeries(series, series)) {
	    response.setSuccess(false);
	    response.setResult(new String[]{messageSource.getMessage("loyalty_invalid_single_card",
		new Object[]{barcode}, LocaleContextHolder.getLocale())});
	}

	return response;
    }
    
}
