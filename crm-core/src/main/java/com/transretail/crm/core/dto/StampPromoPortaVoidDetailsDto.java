package com.transretail.crm.core.dto;

import java.io.Serializable;

/**
 * @author Mike de Guzman
 */
public class StampPromoPortaVoidDetailsDto implements Serializable {

    private Long id;
    private String name;
    private Double voidedStamps;
    private Double availableStamps;

    public StampPromoPortaVoidDetailsDto() {}

    public StampPromoPortaVoidDetailsDto(Long id, String name, Double voidedStamps, Double availableStamps) {
        this.id = id;
        this.name = name;
        this.voidedStamps = voidedStamps;
        this.availableStamps = availableStamps;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Double getVoidedStamps() {
        return voidedStamps;
    }

    public void setVoidedStamps(Double voidedStamps) {
        this.voidedStamps = voidedStamps;
    }

    public Double getAvailableStamps() {
        return availableStamps;
    }

    public void setAvailableStamps(Double availableStamps) {
        this.availableStamps = availableStamps;
    }
}
