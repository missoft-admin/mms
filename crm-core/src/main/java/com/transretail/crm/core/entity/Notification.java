package com.transretail.crm.core.entity;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotEmpty;

import com.transretail.crm.common.web.notification.NotificationType;
import com.transretail.crm.core.entity.enums.ModelType;
import com.transretail.crm.core.entity.support.CustomAuditableEntity;


@Entity
@Table(name="CRM_NOTIFICATIONS")
public class Notification extends CustomAuditableEntity<Long> implements Serializable{
	@Column(name="TYPE")
	@Enumerated(EnumType.STRING)
	private NotificationType type;
	
	@Column(name="ITEM_ID")
	private String itemId;
	
	@Column(name="INFO")
	private String info;
	
	@OneToMany
	@JoinColumn(name="STATUS")
	private List<NotificationStatus> statuses;

	public NotificationType getType() {
		return type;
	}

	public void setType(NotificationType type) {
		this.type = type;
	}

	public List<NotificationStatus> getStatuses() {
		return statuses;
	}

	public void setStatuses(List<NotificationStatus> statuses) {
		this.statuses = statuses;
	}

	public String getItemId() {
		return itemId;
	}

	public void setItemId(String itemId) {
		this.itemId = itemId;
	}

	public String getInfo() {
		return info;
	}

	public void setInfo(String info) {
		this.info = info;
	}
}
