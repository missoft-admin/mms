<%@ include file="../common/taglibs.jsp"%>

<style type="text/css">
<!--
  .well .input {
    background-color: #fff;
    border: 1px solid #E8E8E8;
    box-shadow: none;    
    padding: 6px;
    height: 18px;
    margin-left: 5px;
  }
  .well .btn-primary {
    margin-left: 10px;
  }
  
-->
</style>

    <spring:message code="label_delete" var="label_delete"/>
    <spring:message code="label_update" var="label_update"/>
    <spring:message code="label_view_cities" var="label_view_cities"/>
    <spring:message code="label_new_province" var="label_new_province"/>
    <spring:message code="label_update_province" var="label_update_province"/>
    <spring:message code="label_new_city" var="label_new_city"/>
    <spring:message code="label_update_city" var="label_update_city"/>
    
    <c:set var="delete_confirm_msg">
        <spring:escapeBody javaScriptEscape="true">
            <spring:message code="entity_delete_confirm" />
        </spring:escapeBody>
    </c:set>
    
  <div class="page-header page-header2">
      <h1><spring:message code="menutab_programsetup_maintenance_location" /></h1>
  </div>
  
  <div id="form_searchForm" class="well">
    <div id="control-group" class="form-inline form-search">

        <label for="" class="label-single pull-left"><h5><spring:message code="search_by"/></h5></label>
        <form name="searchForm" class="form-reset">
        <input type="hidden" id="searchField" />
        </form>
        <select id="nameTmp" class="" style="margin-left: 9px">
          <option value="province"><spring:message code="location_province" /></option>
          <option value="city"><spring:message code="location_city" /></option>
        </select>
        <div id="searchForm" class="inline" style="margin-left:5px; margin-top: 6px">
            <input id="valueTmp" class="input memberSearchField" />
            <input type="button" id="searchButton" value="<spring:message code="label_search" />" class="btn btn-primary"/>
            <input type="button" id="clearButton" value="<spring:message code="label_clear" />" class="btn custom-reset" data-form-id="form_searchForm" data-default-id="searchButton"/>
        </div>
      
    </div>
  </div>
  
  <div class="clearfix"></div>
  
  <button type="button" id="createProvince" onclick="javascript:createProvince();" class="btn btn-primary pull-right mb20"><spring:message code="label_new_province"/></button><br/><br/>
  <div class="clearfix"></div>

<div id="content_provinceList">
  <div id="list_province"></div>
</div>


<div class="clearfix"></div>

<br/><br/>

<fieldset id="city" class="hide">
<legend class="block-stack-title"><span id="provinceName"></span></legend>

<div class="clearfix"></div>
<button type="button" id="createCity" onclick="javascript:createCity();" class="btn btn-default pull-right"><spring:message code="label_new_city"/></button><br/><br/>
<div class="clearfix"></div>

<div id="content_cityList">
  <div id="list_city"></div>
</div>

</fieldset>

<jsp:include page="../common/confirm.jsp" />

  <link href="<spring:url value="/css/dataTables.css" />" rel="stylesheet">
  <script src="<spring:url value="/js/jquery/jquery.dataTables.js" />" ></script>
  <script src="<spring:url value="/js/jquery/jquery.dataTables.linkLengthMenu.js" />"></script>
  <script src="<spring:url value="/js/jquery/jquery.dataTables.customPaging.js" />"></script>
  <script src="<spring:url value="/js/jquery/ajax-datatable.js" />" ></script>
  <script src="<spring:url value="/js/common/form2js.js" />" ></script>
  <script src="<spring:url value="/js/common/json2.js" />" ></script>
  <script src="<spring:url value="/js/jquery/jquery.toObject.js" />" ></script>
  <link href="<c:url value="/styles/temp.css" />" rel="stylesheet" />
  
    
  
<div id="provinceBtn" class="hide">
<button class="btn btn-primary pull-left tiptip icn-view" title="${label_view_cities}" onclick="javascript:reloadCityTable('%ITEMID%', '%ITEMNAME%');"></button>
<button class="btn btn-primary pull-left tiptip icn-edit" title="${label_update}" onclick="javascript:editProvince('%ITEMID%', '%ITEMNAME%');"></button>
<button class="btn btn-primary pull-left tiptip icn-delete" title="${label_delete}" onclick="javascript:deleteProvince('<spring:url value="/location/province/%ITEMID%"/>');"></button>
</div>

<div id="cityBtn" class="hide">
<button class="btn btn-primary pull-left tiptip icn-edit" title="${label_update}" onclick="javascript:editCity('%PROVINCEID%', '%ITEMID%', '%ITEMNAME%');"></button>
<button class="btn btn-primary pull-left tiptip icn-delete" title="${label_delete}" onclick="javascript:deleteCity('<spring:url value="/location/city/%ITEMID%"/>', '%PROVINCEID%');"></button>
</div>


<div class="modal hide  groupModal nofly" tabindex="-1" role="dialog" aria-hidden="true" id="formDialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title" id="modalTitle"></h4>
      </div>
      <div class="modal-body">
        <div id="contentError" class="hide alert alert-error">
          <button type="button" class="close" onclick="$(this).parent().hide();">&times;</button>
          <div><form:errors path="*"/></div>
        </div>
        <form id="locationForm" class="form-reset">
          <input type="hidden" name="id" />
          <label for="description"><b class="required">*</b> <spring:message code="location_name"/></label><input type="text" name="name" placeholder="<spring:message code="location_name"/>" />
        </form>
      </div>
      <div class="modal-footer">
        <button type="button" id="formSubmit" class="btn btn-primary"><spring:message code="label_submit" /></button>
        <button type="button" id="formCancel" data-dismiss="modal" class="btn btn-default"><spring:message code="label_cancel"/></button>
      </div>
    </div>
  </div>
</div>

<script type="text/javascript">

var CONTAINER_ERROR = "#contentError > div";
var CONTENT_ERROR = "#contentError";

$(document).ready(function() {
	initProvinceList();
	initCityList();
	
	$("#formDialog").modal({
		show: false,
		keyboard:	false, 
		backdrop: "static",
	})
	.on('hidden.bs.modal', function() {
		$("#locationForm > input").val("");
		$("#formDialog").find(CONTENT_ERROR).hide();
		$("#formDialog").find(CONTAINER_ERROR).empty();
	})
	.css({
		"width": "300px"
	})
	;

	reloadProvinceTable();
	
	$("#formSubmit").click(submitForm);

	$("#clearButton").click(function() {
		$("#valueTmp").val("");
		$("#nameTmp").val("province");
	});
});

function submitForm(e) {
	e.preventDefault();
	$( "#formSubmit" ).prop( "disabled", true );
	$.post($("#locationForm").attr("action"), $("#locationForm").serialize(), function(data) {
		$( "#formSubmit" ).prop( "disabled", false );
		if (data.success) {
			location.reload();
		} 
		else {
			errorInfo = "";
			for (i = 0; i < data.result.length; i++) {
				errorInfo += "<br>" + (i + 1) + ". "
						+ ( data.result[i].code != undefined ? 
								data.result[i].code : data.result[i]);
			}
			$("#formDialog").find(CONTAINER_ERROR).html("Please correct following errors: " + errorInfo);
			$("#formDialog").find(CONTENT_ERROR).show('slow');
		}
	});	
}

function createProvince() {
	var action = "<spring:url value="/location/province"/>";
	$("#modalTitle").text("${label_new_province}");
	$("#locationForm").attr("action", action);
	$("#formDialog").modal("show");
}

function createCity() {
	var action = "<spring:url value="/location/city/"/>" + $("#createCity").data("provinceId");
	$("#modalTitle").text("${label_new_city}");
	$("#locationForm").attr("action", action);
	$("#formDialog").modal("show");
}

function editProvince(id, name) {
	var action = "<spring:url value="/location/province"/>";
	$("#modalTitle").text("${label_update_province}");
	$("#locationForm").attr("action", action);
	$("#locationForm > input[name='id']").val(id);
	$("#locationForm > input[name='name']").val(name);
	$("#formDialog").modal("show");
}

function editCity(provinceId, id, name) {
	var action = "<spring:url value="/location/city/"/>" + provinceId;
	$("#modalTitle").text("${label_update_city}");
	$("#locationForm").attr("action", action);
	$("#locationForm > input[name='id']").val(id);
	$("#locationForm > input[name='name']").val(name);
	$("#formDialog").modal("show");
}

function reloadProvinceTable() {
  //$("#searchButton").click();
  $("#city").hide();
  $("#searchField").attr("name", $("#nameTmp").val()).val($("#valueTmp").val());
	
	var formDataObj = $("form[name='searchForm']").toObject( { mode : 'first', skipEmpty : true } );
	$btnSearch = $("#searchButton");
	$btnSearch.prop('disabled', true);

	$("#list_province").ajaxDataTable( 
		'search', 
		formDataObj, 
		function() {
			$btnSearch.prop( 'disabled', false );
	});	
}

function initProvinceList() {
	$("#list_province").ajaxDataTable({
	      'autoload'  : false,
	      'ajaxSource' : "<c:url value="/location/province/list" />",
	      'columnHeaders' : [
	      "<spring:message code="location_province"/>",
	      ""
	      ],
	      'modelFields' : [
	         {name : 'name', sortable : true},
	         {customCell : function ( innerData, sSpecific, json ) {
	              if ( sSpecific == 'display') {
	                var btnHtml = $("#provinceBtn").html();
	                var rg = new RegExp("%ITEMID%", 'g');
	           btnHtml = btnHtml.replace(rg, json.id);
	           rg = new RegExp("%ITEMNAME%", 'g');
	           btnHtml = btnHtml.replace(rg, json.name);
	                  return btnHtml;
	              } else {
	                return "";
	              }
	         }
	         }
	       ]
	}); 
	
	$("#searchButton").click(function() {
		reloadProvinceTable();
	});
}

function initCityList() {
	$("#list_city").ajaxDataTable({
	      'autoload'  : false,
	      'ajaxSource' : "<c:url value="/location/city/list/" />",
	      'columnHeaders' : [
	      "<spring:message code="location_city"/>",
	      ""
	      ],
	      'modelFields' : [
	         {name : 'name', sortable : true},
	         {customCell : function ( innerData, sSpecific, json ) {
	        	 if ( sSpecific == 'display') {
	                 var btnHtml = $("#cityBtn").html();
	                 var rg = new RegExp("%ITEMID%", 'g');
	                  btnHtml = btnHtml.replace(rg, json.id);
	                  rg = new RegExp("%ITEMNAME%", 'g');
	                  btnHtml = btnHtml.replace(rg, json.name);
	                  rg = new RegExp("%PROVINCEID%", 'g');
	                  btnHtml = btnHtml.replace(rg, $("#createCity").data("provinceId"));
	                   return btnHtml;
	               } else {
	                 return "";
	               }
	         }
	         }
	       ],
	       "initCompleteCallback" : function (dataTable) {
	    	   $("#list_city").goTo();
         }
	  }); 
}

function reloadCityTable(id, name) {
  $("#city").show();
  $("#provinceName").text(name);
  reloadCityTableAjax(id);
}

function reloadCityTableAjax(id) {
	$("#createCity").data("provinceId", id);
	
	var formDataObj = $("form[name='searchForm']").toObject( { mode : 'first', skipEmpty : true } );
	formDataObj.provinceId = id;
	
	$("#list_city").ajaxDataTable( 
		'search', 
		formDataObj, 
		function() {});	
	
	// $("#list_city").goTo();
}

function deleteProvince(url) {
	getConfirm('${delete_confirm_msg}',function(result) {
		if(result) {
			$.post(url, {_method: "DELETE"}, function(data) {
		    	reloadProvinceTable();
			  });	
		}
	});
}


function deleteProvince(url) {
	getConfirm('${delete_confirm_msg}',function(result) {
		if(result) {
			$.post(url, {_method: "DELETE"}, function(data) {
		    	reloadProvinceTable();
			  });	
		}
	});
}

function deleteCity(url, id) {
	getConfirm('${delete_confirm_msg}',function(result) {
		if(result) {
			$.post(url, {_method: "DELETE"}, function(data) {
		    	reloadCityTableAjax(id);
			  });	
		}
	});
}

$.fn.goTo = function() {
    $('html, body').animate({
        scrollTop: $(this).offset().top + 'px'
    }, 'fast');
    return this;
}


</script>

<style type="text/css">
    #DataTables_Table_0.table tbody tr td:first-child, #DataTables_Table_1.table tbody tr td:first-child {
        width: 84%;
    }
</style>