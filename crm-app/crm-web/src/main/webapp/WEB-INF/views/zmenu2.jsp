<%@ include file="common/taglibs.jsp" %>
<%@ taglib prefix="permission" tagdir="/WEB-INF/tags/permission" %>
<%@ taglib prefix="crm" uri="http://www.carrefour.com/crm/permission/tags" %>

<spring:message code="role_admin" var="role_admin"/>
<spring:message code="role_customerservice" var="role_customerservice"/>
<spring:message code="role_css" var="role_css" />
<spring:message code="role_marketing" var="role_marketing" />
<spring:message code="role_marketing_head" var="role_marketing_head" />
<spring:message code="role_hr" var="role_hr" />
<spring:message code="role_hrs" var="role_hrs" />
<spring:message code="role_merchant_service" var="role_merchant_service" />
<spring:message code="role_merchant_service_supervisor" var="role_merchant_service_supervisor" />
<spring:message code="role_store_cashier" var="role_store_cashier" />
<spring:message code="role_store_cashier_head" var="role_store_cashier_head" />
<spring:message code="role_store_manager" var="role_store_manager" />
<spring:message code="role_helpdesk" var="role_helpdesk" />
<spring:message code="role_member" var="role_member" />
<spring:message code="role_treasury" var="role_treasury" />
<spring:message code="role_accounting" var="role_accounting" />
<sec:authorize var="hasaccess_menu_programsetup" ifAnyGranted="${role_admin}"/>
<sec:authorize var="hasaccess_menu_marketingmgmt" ifAnyGranted="${role_marketing}, ${role_marketing_head}, ${role_helpdesk}" />
<sec:authorize var="hasaccess_menu_giftcard" ifAnyGranted="${role_merchant_service}, ${role_merchant_service_supervisor}, ${role_store_cashier}, ${role_store_manager}, ${role_store_cashier_head}" />
<sec:authorize var="hasaccess_menu_giftcard_inventory" ifAnyGranted="${role_merchant_service_supervisor}, ${role_store_cashier_head}" />
<sec:authorize var="hasaccess_menu_giftcard_inventory_nonsup" ifAnyGranted="${role_merchant_service}" />
<sec:authorize var="hasaccess_menu_membermgmt" ifAnyGranted="${role_customerservice}, ${role_css}, ${role_store_manager}, ${role_helpdesk}" />
<sec:authorize var="hasaccess_menu_membermgmt_mgr" ifAnyGranted="${role_css}" />
<sec:authorize var="hasaccess_menu_employeemgmt" ifAnyGranted="${role_hr}, ${role_hrs}, ${role_helpdesk}" />
<sec:authorize var="hasaccess_menu_employeemgmt_mgr" ifAnyGranted="${role_hrs}, ${role_helpdesk}" />
<sec:authorize var="hasaccess_menu_programsetup" ifAnyGranted="${role_admin}" />
<sec:authorize var="hasaccess_menu_mediamgr" ifAnyGranted="${role_admin}" />
<sec:authorize var="hasaccess_menu_treasury" ifAnyGranted="${role_treasury}" />
<sec:authorize var="hasaccess_menu_accounting" ifAnyGranted="${role_accounting}" />

<ul id="nav-menu" class="nav">
  <c:if test="${hasaccess_menu_programsetup}">
    <li class="dropdown" id="menu_programSetup">
      <a href="#" class="dropdown-toggle nav2 btn btn-main-menu" data-toggle="dropdown" data-hover="dropdown" data-delay="200" data-close-others="true">
        <i class="icn-administrator"></i>
        <spring:message code="menutab_programsetup"/> </a>
      <ul class="dropdown-menu">
        <crm:permission hasPermissions="USER_MANAGEMENT,LOOKUP_MANAGEMENT, CITY_PROVINCE_LOOKUP, APP_CONFIGURATION, POINTS_CONFIGURATION,
                                        PEOPLESOFT_MAPPING, STORE_MEMBER_SCHEME, VIEW_STORE_LOGS, VIEW_CRM_PROXY_INFO, VENDOR_MANAGEMENT">
          <li class="nav-header"><spring:message code="menutab_programsetup_maintenance"/></li>
          <crm:permission hasPermissions="USER_MANAGEMENT">
            <li><a href="<spring:url value="/user" />"><spring:message code="menutab_programsetup_maintenance_user"/></a></li>
          </crm:permission>
          <crm:permission hasPermissions="LOOKUP_MANAGEMENT">
            <li><a href="<spring:url value="/lookup?page=1&size=10" />"><spring:message code="menutab_programsetup_maintenance_lookup"/></a></li>
          </crm:permission>
          <crm:permission hasPermissions="CITY_PROVINCE_LOOKUP">
            <li><a href="<spring:url value="/location" />"><spring:message code="menutab_programsetup_maintenance_location"/></a></li>
          </crm:permission>
          <crm:permission hasPermissions="APP_CONFIGURATION">
            <li><a href="<spring:url value="/appconfig" />"><spring:message code="menutab_programsetup_permissions_appconfig"/></a></li>
          </crm:permission>
          <crm:permission hasPermissions="POINTS_CONFIGURATION">
            <li><a href="<spring:url value="/member/points/config" />"><spring:message code="menutab_programsetup_maintenance_points"/></a></li>
          </crm:permission>
          <crm:permission hasPermissions="PEOPLESOFT_MAPPING">
            <li><a href="<spring:url value="/store/peoplesoft" />"><spring:message code="store.psoft.mapping"/></a>
          </crm:permission>
          <crm:permission hasPermissions="STORE_MEMBER_SCHEME">
            <li><a href="<spring:url value="/storemember" />"><spring:message code="menutab_programsetup_permissions_store_member_scheme"/></a></li>
          </crm:permission>
          <crm:permission hasPermissions="VIEW_STORE_LOGS">
            <li><a href="<spring:url value="/log/view" />"><spring:message code="admin.log.title"/></a></li>
          </crm:permission>
          <crm:permission hasPermissions="VIEW_CRM_PROXY_INFO">
            <li><a href="<spring:url value="/crmproxyinfo/view" />"><spring:message code="crm.instore.details"/></a></li>
          </crm:permission>
          <crm:permission hasPermissions="VENDOR_MANAGEMENT">
            <li><a href="<spring:url value="/giftcard/vendor/list" />"><spring:message code="menutab_giftcard_vendors"/></a></li>
          </crm:permission>
          <li class="divider"></li>
        </crm:permission>
        
        <crm:permission hasPermissions="HR_OVERRIDE_PERMISSIONS_SETUP, POINTS_OVVERIDE_PERMISSIONS_SETUP">
          <crm:permission hasPermissions="HR_OVERRIDE_PERMISSIONS_SETUP">
            <li class="nav-header"><spring:message code="menutab_programsetup_permissions"/></li>
            <li><a href="<spring:url value="/override/permissions?page=1&size=10" />"><spring:message code="menutab_programsetup_permissions_hroverridescheme"/></a></li>
          </crm:permission>
          <crm:permission hasPermissions="POINTS_OVVERIDE_PERMISSIONS_SETUP">
            <li><a href="<spring:url value="/points/override/approver" />"><spring:message code="menutab_programsetup_permissions_pointsoverridescheme"/></a></li>
          </crm:permission>
        </crm:permission>
      </ul>
    </li>
  </c:if>



  <c:if test="${hasaccess_menu_marketingmgmt}">
    <li class="dropdown" id="menu_marketingMgmt">
      <a href="#" class="dropdown-toggle nav2 btn btn-main-menu" data-toggle="dropdown" data-hover="dropdown" data-delay="500" data-close-others="true">
        <i class="icn-marketing"></i>
        <spring:message code="menutab_marketingmgmt"/> </a>
      <ul class="dropdown-menu">
        <crm:permission hasPermissions="CUSTOMER_GROUP_MANAGEMENT, STORE_GROUP_MANAGEMENT, PRODUCT_GROUP_MANAGEMENT">
          <li class="nav-header"><spring:message code="menutab_marketingmgmt_groups"/></li>
          <crm:permission hasPermissions="CUSTOMER_GROUP_MANAGEMENT">
            <li><a href="<spring:url value="/groups/member" />"><spring:message code="menutab_marketingmgmt_campaign_customergroup"/></a></li>
          </crm:permission>
          <crm:permission hasPermissions="STORE_GROUP_MANAGEMENT">
            <li><a href="<spring:url value="/groups/store?page=1&size=10" />"><spring:message code="menutab_marketingmgmt_config_storegroup"/></a></li>
          </crm:permission>
          <crm:permission hasPermissions="PRODUCT_GROUP_MANAGEMENT">
            <li><a href="<spring:url value="/groups/product" />"><spring:message code="menutab_marketingmgmt_config_productgroup"/></a></li>
          </crm:permission>
          <li class="divider"></li>
        </crm:permission>
        
        <crm:permission hasPermissions="PROMOTIONS_MANAGEMENT,CUSTOMER_HELPDESK, MEDIA_MGMT, DATA_ANALYSIS, PROMO_BANNER_MANAGEMENT,
                                        ROLE_MERCHANT_SERVICE_UPLOAD_IND_PROF_MEMBERS,ROLE_MERCHANT_SERVICE_SUPERVISOR_UPLOAD_IND_PROF_MEMBERS">
          <li class="nav-header"><spring:message code="menutab_marketingmgmt_tools"/></li>
          <crm:permission hasPermissions="PROMOTIONS_MANAGEMENT,CUSTOMER_HELPDESK">
            <li><a href="<spring:url value="/promotion" />"><spring:message code="menutab_marketingmgmt_config"/></a></li>
          </crm:permission>
          <crm:permission hasPermissions="MEDIA_MGMT">
            <li><a href="<spring:url value="/marketing/channel" />"><spring:message code="menutab_marketingmgmt_media"/></a></li>
          </crm:permission>
          <crm:permission hasPermissions="DATA_ANALYSIS">
            <li><a href="<spring:url value="/dataanalysis" />"><spring:message code="menutab_marketingmgmt_dataanalysis"/></a></li>
          </crm:permission>
          <crm:permission hasPermissions="PROMO_BANNER_MANAGEMENT">
            <li><a href="<spring:url value="/promobanner" />"><spring:message code="menutab_marketingmgmt_promo_banner"/></a></li>
          </crm:permission>
          <crm:permission hasPermissions="ROLE_MERCHANT_SERVICE_UPLOAD_IND_PROF_MEMBERS,ROLE_MERCHANT_SERVICE_SUPERVISOR_UPLOAD_IND_PROF_MEMBERS">
            <li><a href="<spring:url value="/member/upload" />"><spring:message code="member.upload"/></a></li>
          </crm:permission>
        </crm:permission>
      </ul>
    </li>
  </c:if>




  <c:if test="${hasaccess_menu_membermgmt}">
    <li class="dropdown" id="menu_memberMgmt">
      <a href="#" class="dropdown-toggle nav2 btn btn-main-menu" data-toggle="dropdown" data-hover="dropdown" data-delay="500" data-close-others="true">
        <i class="icn-cs"></i>
        <spring:message code="menutab_membermgmt"/></a>
      <ul class="dropdown-menu">
        <c:if test="${hasaccess_menu_membermgmt}">
          <li class="nav-header"><spring:message code="menutab_membermgmt"/></li>
          <crm:permission hasPermissions="IND_PROF_MEMBERS_MANAGEMENT,CUSTOMER_HELPDESK"><%--Bug #86439--%>
            <li><a href="<spring:url value="/customermodels?page=1&size=10" />"><spring:message code="menutab_membermgmt_loyalty_members"/></a></li>
          </crm:permission>
          <crm:permission hasPermissions="ROLE_CS_UPLOAD_IND_PROF_MEMBERS,ROLE_CSS_UPLOAD_IND_PROF_MEMBERS">
            <li><a href="<spring:url value="/member/upload" />"><spring:message code="member.upload"/></a></li>
          </crm:permission>
          <sec:authorize var="hasaccess_menu_membermgmt" ifAnyGranted="${role_customerservice}, ${role_css}">
            <crm:permission hasPermissions="MEMBER_MONITOR">
              <li><a href="<spring:url value="/member/monitor/cs" />"><spring:message code="menutab_programsetup_maintenance_membermonitor"/></a></li>
            </crm:permission>
          </sec:authorize>
          <crm:permission hasPermissions="COMPLAINTS_MGMT">
            <li><a href="<spring:url value="/customer/complaint" />"><spring:message code="menutab_membermgmt_complaints"/></a></li>
          </crm:permission>
          <crm:permission hasPermissions="IND_PROF_POINTS_ADJUSTMENT">
            <li><a href="<spring:url value="/points/override/member" />"><spring:message code="menutab_membermgmt_loyalty_adjustment"/></a></li>
          </crm:permission>
        </c:if>

      </ul>
    </li>
  </c:if>
  
  
  
  
  <c:if test="${hasaccess_menu_employeemgmt}">
    <li class="dropdown" id="menu_employeeMgmt">
      <a href="#" class="dropdown-toggle nav2 btn btn-main-menu" data-toggle="dropdown" data-hover="dropdown" data-delay="500" data-close-others="true">
        <i class="icn-hr"></i>
        <spring:message code="menutab_employeemgmt"/> </a>
      <ul class="dropdown-menu">
      
        <crm:permission hasPermissions="EMPLOYEE_MANAGEMENT,EMPLOYEE_MANAGEMENT,CUSTOMER_HELPDESK,UPLOAD_EMPLOYEES,
                                        MEMBER_MONITOR_EMP, EMP_PURCHASE_ADJUSTMENT, EMP_POINTS_ADJUSTMENT">
          <li class="nav-header"><spring:message code="menutab_employeemgmt_helpdesk"/></li>
          <crm:permission hasPermissions="EMPLOYEE_MANAGEMENT,EMPLOYEE_MANAGEMENT,CUSTOMER_HELPDESK">
            <li><a href="<spring:url value="/employeemodels?page=1&size=10" />"><spring:message code="menutab_employeemgmt_helpdesk_emplist"/></a></li>
          </crm:permission>
          <crm:permission hasPermissions="UPLOAD_EMPLOYEES">
            <li><a href="<spring:url value="/member/upload/employee" />"><spring:message code="member.upload.employee"/></a></li>
          </crm:permission>
          <crm:permission hasPermissions="MEMBER_MONITOR_EMP">
            <li><a href="<spring:url value="/member/monitor/hr" />"><spring:message code="menutab_programsetup_maintenance_membermonitor"/></a></li>
          </crm:permission>
          <crm:permission hasPermissions="EMP_PURCHASE_ADJUSTMENT">
            <li><a href="<spring:url value="/employee/adjustments/purchases?page=1&size=10" />"><spring:message code="menutab_employeemgmt_helpdesk_purchaseadjustment"/></a></li>
          </crm:permission>
          <crm:permission hasPermissions="EMP_POINTS_ADJUSTMENT">
            <li><a href="<spring:url value="/points/override/employee" />"><spring:message code="menutab_employeemgmt_helpdesk_pointsadjustment"/></a></li>
          </crm:permission>
          <li class="divider"></li>
        </crm:permission>
        
        <crm:permission hasPermissions="REWARDS_GENERATION_SCHEDULE, REWARDS_APPROVAL, EMP_REWARDS_SCHEME_MANAGEMENT,CUSTOMER_HELPDESK, RECON_MGMT">
          <c:if test="${hasaccess_menu_employeemgmt_mgr}">
            <li class="nav-header"><spring:message code="menutab_employeemgmt_manager"/></li>
            <crm:permission hasPermissions="REWARDS_GENERATION_SCHEDULE">
              <li><a href="<spring:url value="/pointsreward/generate/schedule" />"><spring:message code="menutab_employeemgmt_manager_rewardsched" text=""/></a></li>
            </crm:permission>
            <crm:permission hasPermissions="REWARDS_APPROVAL">
              <li><a href="<spring:url value="/pointsreward/forprocessing" />"><spring:message code="menutab_employeemgmt_manager_rewardforapproval" text=""/></a></li>
            </crm:permission>
            <crm:permission hasPermissions="EMP_REWARDS_SCHEME_MANAGEMENT,CUSTOMER_HELPDESK">
              <li><a href="<spring:url value="/voucherscheme?page=1&size=5" />"><spring:message code="menutab_employeemgmt_manager_rewardscheme" text=""/></a></li>
            </crm:permission>
          </c:if>
          <crm:permission hasPermissions="RECON_MGMT">
            <li><a href="<spring:url value="/points/recon" />"><spring:message code="menutab_cs_recon"/></a></li>
          </crm:permission>
        </crm:permission>
        
      </ul>
    </li>
  </c:if>
  
  
  
  <c:if test="${hasaccess_menu_giftcard}">
    <li class="dropdown" id="menu_giftCard">
      <a href="#" class="dropdown-toggle nav2 btn btn-main-menu" data-toggle="dropdown" data-hover="dropdown" data-delay="500" data-close-others="true">
        <i class="icn-loyalty"></i>
        <spring:message code="menutab_loyalty_management"/> </a>
      <ul class="dropdown-menu">
        <crm:permission hasPermissions="LC_ANNUAL_FEE_SCHEME">
          <li class="nav-header"><spring:message code="menutab_programsetup_maintenance"/></li>
          <c:if test="${hasaccess_menu_marketingmgmt}">
              <li><a href="<spring:url value="/loyaltyannualfee/scheme" />"><spring:message code="menutab_loyalty_annual_fee_scheme"/></a> </li>
          </c:if>
          <li class="divider"></li>
        </crm:permission>
        
        <crm:permission hasPermissions="LC_MO, LC_INV, LC_D2D_TRACKING, LC_PHYSL_COUNT">
          <li class="nav-header"><spring:message code="menutab_loyalty_card"/></li>
          <crm:permission hasPermissions="LC_MO">
            <permission:render inventoryLoc="INVT001">
              <li><a href="<spring:url value="/manufactureorder" />"><spring:message code="menutab_manufacture_order"/></a></li>
            </permission:render>
          </crm:permission>
          <crm:permission hasPermissions="LC_INV">
            <li><a href="<spring:url value="/loyaltycardinventory" />"><spring:message code="menutab_loyalty_card_inventory"/></a></li>
          </crm:permission>
          <crm:permission hasPermissions="LC_D2D_TRACKING">
            <li><a href="<spring:url value="/loyaltycardinventory/tracker" />"><spring:message code="menutab_loyalty_card_tracking"/></a></li>
          </crm:permission>
          <crm:permission hasPermissions="LC_PHYSL_COUNT">
            <sec:authentication var="userLocation" property="principal.inventoryLocation"/>
            <c:if test="${userLocation != null}">
              <li><a href="<spring:url value="/loyaltycardinventory/physicalcount" />"><spring:message code="menutab_loyalty_card_physical_count"/></a></li>
            </c:if>
          </crm:permission>
        </crm:permission>
      </ul>
    </li>
  </c:if>
  
  
  
  
  <c:if test="${hasaccess_menu_giftcard or hasaccess_menu_treasury or hasaccess_menu_accounting}">
    <li class="dropdown" id="menu_giftCard">
      <a href="#" class="dropdown-toggle nav2 btn btn-main-menu" data-toggle="dropdown" data-hover="dropdown" data-delay="900" data-close-others="true">
        <i class="icn-gift"></i>
        <spring:message code="menutab_giftcard_management"/> 
      </a>
      <ul class="dropdown-menu">        
        <li class="dropdown-submenu">
          <crm:permission hasPermissions="GC_PROD_PROF, GC_MO">
            <a tabindex="1" href="#" data-toggle="dropdown" data-hover="dropdown" data-delay="900" data-close-others="true"><spring:message code="menutab_giftcard_procurement"/></a>
            <ul class="dropdown-menu">
              <li class="nav-header"><spring:message code="menutab_giftcard_procurement"/></li>
              <crm:permission hasPermissions="GC_PROD_PROF">
                <li><a href="<spring:url value="/gc/productprofile" />"><spring:message code="menutab_giftcard_productprofile"/></a></li>
              </crm:permission>
              <c:if test="${hasaccess_menu_giftcard_inventory or hasaccess_menu_giftcard_inventory_nonsup}">
                <crm:permission hasPermissions="GC_MO">
                  <permission:render inventoryLoc="INVT001,99996,99997,99998">
                    <li><a href="<spring:url value="/giftcard/order" />"><spring:message code="menutab_giftcard_order"/></a></li>
                  </permission:render>
                </crm:permission>
              </c:if>
            </ul>
          </crm:permission>
        </li>
        <li class="dropdown-submenu">
          <crm:permission hasPermissions="GC_BURN_CARD, CLAIMS_TO_AFFILIATES_SCHEDULE, GC_EXPIRY_DATE_EXTENSIONl, GC_INV_STOCK, STOCK_REQUEST_MANAGEMENT, GC_ADJUST_INVENTORY">       
            <a tabindex="2" href="#" data-toggle="dropdown" data-hover="dropdown" data-delay="900" data-close-others="true"><spring:message code="menutab_giftcard_inventory"/></a>
            <ul class="dropdown-menu">
              <li class="nav-header"><spring:message code="menutab_giftcard_inventory"/></li>
              <crm:permission hasPermissions="GC_ADJUST_INVENTORY">
                <sec:authentication var="userLocation" property="principal.inventoryLocation"/>
                <c:if test="${userLocation != null}">
                  <li><a href="<spring:url value="/giftcard/inventory/adjustment" />"><spring:message code="gc.adjust.inventory.page"/></a></li>
                </c:if>
              </crm:permission> 
              <crm:permission hasPermissions="GC_BURN_CARD">
                <li><a href="<spring:url value="/giftcard/burncard" />"><spring:message code="menutab_giftcard_burncard"/></a></li>
              </crm:permission>
              <crm:permission hasPermissions="CLAIMS_TO_AFFILIATES_SCHEDULE">
          	    <li><a href="<spring:url value="/giftcard/acct/claimstoaffiliates/schedule" />"><spring:message code="menutab_gcmgmt_acct_claimstoaffiliates_schedule" text=""/></a></li>
              </crm:permission>
        	    <crm:permission hasPermissions="GC_EXPIRY_DATE_EXTENSION">
                <li><a href="<spring:url value="/giftcard/dateexpiry" />"><spring:message code="menutab_giftcard_date_expiry_extension"/></a></li>
              </crm:permission>
              <crm:permission hasPermissions="GC_INV_STOCK">
                <li><a href="<spring:url value="/giftcard/inventory/stock" />"><spring:message code="gc.invstock.page"/></a></li>
              </crm:permission>
              <crm:permission hasPermissions="STOCK_REQUEST_MANAGEMENT">
                <li><a href="<spring:url value="/stockrequest" />"><spring:message code="menutab_giftcard_stockrequest"/></a></li>
              </crm:permission>
            </ul>
          </crm:permission>
        </li>
        <li class="dropdown-submenu">
          <crm:permission hasPermissions="GC_CX_PROFILE, GC_GENERAL_DISCOUNT,GC_SALES_ORDER,GC_SO_VERIFY_PAYMENT,GC_SO_APPROVE_PAYMENT, GC_SO_VERIFY_PAYMENT
                                            GC_DISC_SCHEME, GC_B2B_EXHIBITION, GC_B2B_EXHIBITION_LOC">
            <a tabindex="3" href="#" data-toggle="dropdown" data-hover="dropdown" data-delay="900" data-close-others="true"><spring:message code="menutab_giftcard_sales"/></a>
            <ul class="dropdown-menu">  
              <li class="nav-header"><spring:message code="menutab_giftcard_sales"/></li>
              <sec:authentication var="b2bLocation" property="principal.b2bLocation"/>
              <c:if test="${b2bLocation != null}">
                <crm:permission hasPermissions="GC_B2B_EXHIBITION">
                  <li><a href="<spring:url value="/giftcard/b2b/exhibition" />"><spring:message code="b2b.exhibition"/></a></li>
                </crm:permission>
              </c:if>
              <crm:permission hasPermissions="GC_CX_PROFILE">
                <li><a href="<spring:url value="/giftcard/customer/profile" />"><spring:message code="gc.cxprofile.page"/></a></li>
              </crm:permission>
              <crm:permission hasPermissions="GC_GENERAL_DISCOUNT">
                <li><a href="<spring:url value="/giftcard/generaldiscount" />"><spring:message code="gc.general.discount.label"/></a></li>
              </crm:permission>
              <crm:permission hasPermissions="GC_SALES_ORDER,GC_SO_VERIFY_PAYMENT,GC_SO_APPROVE_PAYMENT">
                <li><a href="<spring:url value="/giftcard/salesorder" />"><spring:message code="menutab_giftcard_salesorder"/></a></li>
              </crm:permission>
              <crm:permission hasPermissions="GC_SO_VERIFY_PAYMENT">
                <li><a href="<spring:url value="/giftcard/salesorder/payments" />"><spring:message code="menutab_giftcard_salesorder_payments"/></a></li>
              </crm:permission>
              <crm:permission hasPermissions="GC_DISC_SCHEME">
                <li><a href="<spring:url value="/giftcard/discountscheme" />"><spring:message code="menutab_giftcard_discountscheme"/></a></li>
              </crm:permission>        
              <crm:permission hasPermissions="GC_B2B_EXHIBITION_LOC">
                <li><a href="<spring:url value="/giftcard/b2b/location" />"><spring:message code="b2b.location.discount.matrix"/></a></li>
              </crm:permission>
            </ul> 
          </crm:permission> 
        </li>
        <li class="dropdown-submenu">
          <crm:permission hasPermissions="GC_SERVICE_REQUEST,POS_LOSE_MANAGEMENT,GC_PRINT_SERVICE_REQUEST,GC_TRANSACTION_PERMISSION,GC_RETURN">
          <a tabindex="4" href="#" data-toggle="dropdown" data-hover="dropdown" data-delay="900" data-close-others="true"><spring:message code="menutab_giftcard_customer_service"/></a>
            <ul class="dropdown-menu">
              <li class="nav-header"><spring:message code="menutab_giftcard_customer_service"/></li>
              <crm:permission hasPermissions="GC_SERVICE_REQUEST">
                <li><a href="<spring:url value="/giftcard/service-request" />"><spring:message code="gc.service.request.page.title"/></a></li>
              </crm:permission>
              <crm:permission hasPermissions="GC_TRANSACTION_PERMISSION">
                <li><a href="<spring:url value="/report/template/Gift Card Transaction Report" />"><spring:message code="gc.transaction.report.title"/></a></li>
              </crm:permission>
              <crm:permission hasPermissions="POS_LOSE_MANAGEMENT">
                <li><a href="<spring:url value="/giftcard/pos-lose-tx" />"><spring:message code="gc.poslose"/></a></li>
              </crm:permission>
              <crm:permission hasPermissions="GC_PRINT_SERVICE_REQUEST">
                <li><a href="<spring:url value="/giftcard/service-request/print" />"><spring:message code="gc.print.service.request.page.title"/></a></li>
              </crm:permission>
              <crm:permission hasPermissions="GC_RETURN">
                <li><a href="<spring:url value="/giftcard/return" />"><spring:message code="gc_return"/></a></li>
              </crm:permission>
            </ul>
          </crm:permission>
        </li>   
      </ul>
    </li>
  </c:if>



  <sec:authorize ifAnyGranted="ROLE_REPORTS,ROLE_ACCOUNTING">
    <li class="dropdown" id="menu_reports">
      <a href="#" class="dropdown-toggle nav2 btn btn-main-menu" data-toggle="dropdown" data-hover="dropdown" data-delay="500" data-close-others="true">
        <i class="icn-reports"></i>
        <spring:message code="menutab_programsetup_maintenance_reports"/>
      </a>
      <ul class="dropdown-menu">
        <li class="dropdown-submenu">
          <crm:permission hasPermissions="REPORT_BUYING_CUSTOMER, REPORT_POINTS_SUMMARY_DAILY_VIEW, REPORT_EARN_CUSTOMER_LEVEL,
                              CUSTOMER_BUYING_ACTIVITY_PER_CUSTOMER_GROUP_PERMISSION, KPI_MONTHLY_REPORT_PERMISSION,
                              REPORT_AGE_GRP_CONTRIBUTION, REPORT_GENDER_CONTRIBUTION, REPORT_MARITAL_STATUS_CONTRIBUTION,
                              REPORT_NO_OF_CHILDREN_CONTRIBUTION, REPORT_OCCUPATION_CONTRIBUTION, REPORT_POINTS_EXPIRE,
                              REPORT_POINTS_SUMMARY_MONTHLY_VIEW, REPORT_POINTS_SUMMARY_YEARLY_VIEW, REPORT_POPULAR_STORES,
                              REPORT_PROMO_PERF_SALES_IMPACT, REPORT_REDEEM_CUSTOMER_LEVEL, REPORT_TOP_CUSTOMER_PER_PRODUCT,
                              REPORT_TOP_PRODUCT_PER_CUSTOMER, REPORT_POINTS_ISSUANCE_BY_TYPES_SUMMARY_VIEW">
            <a tabindex="-1" href="#" data-toggle="dropdown" data-hover="dropdown" data-delay="500" data-close-others="true"><spring:message code="menutab_marketingmgmt"/></a>
            <ul class="dropdown-menu">
              <li class="nav-header"><spring:message code="menutab_marketingmgmt"/></li>
              
              <crm:permission hasPermissions="REPORT_BUYING_CUSTOMER">
              <li><a href="<spring:url value="/member/buyingcustomer" />"><spring:message code="buying.customer.report.label"/></a></li>
              </crm:permission>
                          
              <crm:permission hasPermissions="REPORT_POINTS_SUMMARY_DAILY_VIEW">
                <li><a href="<spring:url value="/report/template/Point Summary Daily Report" />"><spring:message code="points.summary.daily.report.title"/></a></li>
              </crm:permission>
              
              <crm:permission hasPermissions="REPORT_EARN_CUSTOMER_LEVEL">
              <li><a href="<spring:url value="/report/template/Earn Point Report By Customer Level" />"><spring:message code="earn.point.report.title"/></a></li>
              </crm:permission>
              
              <crm:permission hasPermissions="CUSTOMER_BUYING_ACTIVITY_PER_CUSTOMER_GROUP_PERMISSION">
                <li><a href="<spring:url value="/report/template/customer-buying-activity-per-customer-group" />"><spring:message code="customer.buying.activity.per.group.report.title"/></a></li>
              </crm:permission>
              
              <crm:permission hasPermissions="KPI_MONTHLY_REPORT_PERMISSION">
                <li><a href="<spring:url value="/report/template/kpi-monthly-report" />"><spring:message code="kpi.report.title"/></a></li>
              </crm:permission>
              
              <crm:permission hasPermissions="CUSTOMER_REPORT_PERMISSION">
                <li><a href="<spring:url value="/report/template/customer-count" />"><spring:message code="customer.count.report.title"/></a></li>
              </crm:permission>
            
              <crm:permission hasPermissions="REPORT_AGE_GRP_CONTRIBUTION">
              <li><a href="<spring:url value="/report/template/Contribution by Age Group Report"/>"><spring:message code="age.contribution.report.label"/></a></li>
              </crm:permission>
              
              <crm:permission hasPermissions="REPORT_GENDER_CONTRIBUTION">
              <li><a href="<spring:url value="/report/template/Member Contribution By Gender Report"/>"><spring:message code="gender.contribution.report.label"/></a></li>
              </crm:permission>
              
              <crm:permission hasPermissions="REPORT_MARITAL_STATUS_CONTRIBUTION">
              <li><a href="<spring:url value="/report/template/Member Contribution By Marital Status Report"/>"><spring:message code="marital.status.contribution.report.label"/></a></li>
              </crm:permission>
              
              <crm:permission hasPermissions="REPORT_NO_OF_CHILDREN_CONTRIBUTION">
              <li><a href="<spring:url value="/report/template/No of Children Contribution Report"/>"><spring:message code="children.contribution.report.label"/></a></li>
              </crm:permission>
              
              
              <crm:permission hasPermissions="REPORT_OCCUPATION_CONTRIBUTION">
              <li><a href="<spring:url value="/report/template/Member Contribution By Occupation Report"/>"><spring:message code="occupation.contribution.report.label"/></a></li>
              </crm:permission>
                
              <crm:permission hasPermissions="REPORT_POINTS_EXPIRE">
                <li><a href="<spring:url value="/report/points/expire" />"><spring:message code="txn_report_label_points_expire_report_type"/></a></li>
              </crm:permission>
              <crm:permission hasPermissions="REPORT_POINTS_SUMMARY_MONTHLY_VIEW">
                <li><a href="<spring:url value="/report/template/Point Summary Monthly Report" />"><spring:message code="points.summary.monthly.report.title"/></a></li>
              </crm:permission>
              <crm:permission hasPermissions="REPORT_POINTS_SUMMARY_YEARLY_VIEW">
                <li><a href="<spring:url value="/report/template/Point Summary Yearly Report" />"><spring:message code="points.summary.yearly.report.title"/></a></li>
              </crm:permission>
              <crm:permission hasPermissions="REPORT_POPULAR_STORES">
                <li><spring:message var="popStore" code="report.popstores.title"/><a href="<c:url value="/report/template/${popStore}" />">${popStore}</a></li>
              </crm:permission>
              
              
              <crm:permission hasPermissions="REPORT_PROMO_PERF_SALES_IMPACT">
              <li><a href="<spring:url value="/report/template/Promotion Performance Total Sales Impact" />"><spring:message code="promo.perf.report.title"/></a></li>
              </crm:permission>
              
              
              <crm:permission hasPermissions="REPORT_REDEEM_CUSTOMER_LEVEL">
              <li><a href="<spring:url value="/report/template/Redeem Point Report By Customer Level" />"><spring:message code="redeem.point.report.title"/></a></li>
              </crm:permission>
              
              <crm:permission hasPermissions="REPORT_TOP_CUSTOMER_PER_PRODUCT">
                <li><a href="<spring:url value="/report/template/Top Customer Per Product Report" />"><spring:message code="report.top.customer.per.product.title"/></a></li>
              </crm:permission>
              <crm:permission hasPermissions="REPORT_TOP_PRODUCT_PER_CUSTOMER">
                <li><a href="<spring:url value="/report/template/Top Product Per Customer Report" />"><spring:message code="report.top.product.per.customer.title"/></a></li>
              </crm:permission>
              <crm:permission hasPermissions="REPORT_POINTS_ISSUANCE_BY_TYPES_SUMMARY_VIEW">
                <li><a href="<spring:url value="/report/template/Points Issuance Report" />"><spring:message code="points.issuance.report.title"/></a></li>
              </crm:permission>
              <%--<li><a href="<spring:url value="/member/buyingcustomer" />"><spring:message code="buying.customer.report.label"/></a></li>--%>
            </ul>
          </crm:permission>
        </li>
        
        <li class="dropdown-submenu">
          <crm:permission hasPermissions="REPORT_MEMBER_CARD_ACTIVATION_SUMMARY_VIEW, REPORT_MEMBER_CARD_USAGE, REPORT_MEMBER_REGISTRATION,
                                REPORT_MEMBER_REGISTRATION_SUMMARY_VIEW, REPORT_MEMBER_STORE_TRANSACTION_VIEW
                                REPORT_VISIT_FREQUENCY, REPORT_MEMBER_PURCHASE_AMOUNT">
            <a tabindex="-1" href="#" data-toggle="dropdown" data-hover="dropdown" data-delay="500" data-close-others="true"><spring:message code="menutab_membermgmt"/></a>
            <ul class="dropdown-menu">
              <li class="nav-header"><spring:message code="menutab_membermgmt"/></li>
  	    <%--
              <crm:permission hasPermissions="REPORT_MEMBER_ACTIVITY_VIEW">
                <li><a href="<spring:url value="/report/template/Member Activity" />"><spring:message code="member.activity.report.title"/></a></li>
              </crm:permission>--%>
              <crm:permission hasPermissions="REPORT_MEMBER_CARD_ACTIVATION_SUMMARY_VIEW">
                <li><a href="<spring:url value="/report/template/Card Activation Report Summary" />"><spring:message code="member.card.activation.report.summary.title"/></a></li>
              </crm:permission>
              <crm:permission hasPermissions="REPORT_MEMBER_CARD_USAGE">
                <li><a href="<spring:url value="/member/cardusage" />"><spring:message code="report.card.usage"/></a></li>
              </crm:permission>
              <crm:permission hasPermissions="REPORT_MEMBER_PURCHASE_AMOUNT">
                <li><a href="<spring:url value="/report/template/Member Purchase Amount Report" />"><spring:message code="member.purchase.report.print.label"/></a></li>
              </crm:permission>
              <crm:permission hasPermissions="REPORT_MEMBER_REGISTRATION">
                <li><a href="<spring:url value="/member/registration" />"><spring:message code="member.registration.menu.title"/></a></li>
              </crm:permission>
              <crm:permission hasPermissions="REPORT_MEMBER_REGISTRATION_SUMMARY_VIEW">
                <li><a href="<spring:url value="/report/template/Member Registration By Type" />"><spring:message code="member.registration.bytype.report.title"/></a></li>
              </crm:permission>
              <crm:permission hasPermissions="REPORT_MEMBER_STORE_TRANSACTION_VIEW">
                <li><a href="<spring:url value="/report/template/Member Store Transaction Report" />"><spring:message code="member.store.transaction.report.title"/></a></li>
              </crm:permission>
              <crm:permission hasPermissions="REPORT_VISIT_FREQUENCY">
                <li><a href="<spring:url value="/visit/frequency" />"><spring:message code="report.visit.frequency"/></a></li>
              </crm:permission>
            </ul>
          </crm:permission>
        </li>
        
        <li class="dropdown-submenu">
          <crm:permission hasPermissions="REPORT_EMPLOYEE_PURCHASE_AND_REWARD_SUMMARY_VIEW, REPORT_EMPLOYEE_PURCHASE_VIEW,
                              REPORT_EMPLOYEE_REGISTRATION_SUMMARY_VIEW, REPORT_REWARDS_SUMMARY_BY_MONTH_VIEW,
                              REPORT_EMPLOYEE_REWARDS_BY_LOCATION_SUMMARY_VIEW">
            <a tabindex="-1" href="#" data-toggle="dropdown" data-hover="dropdown" data-delay="500" data-close-others="true"><spring:message code="menutab_employeemgmt"/></a>
            <ul class="dropdown-menu">
              <li class="nav-header"><spring:message code="menutab_employeemgmt"/></li>
              <crm:permission hasPermissions="REPORT_EMPLOYEE_PURCHASE_AND_REWARD_SUMMARY_VIEW">
                <li><a href="<spring:url value="/report/template/Employee Purchase and Reward Report" />"><spring:message code="employee.purchase_and_rewards.title"/></a></li>
              </crm:permission>
              <crm:permission hasPermissions="REPORT_EMPLOYEE_PURCHASE_VIEW">
                <li><a href="<spring:url value="/report/template/Employee Purchase Report" />"><spring:message code="employee.purchase.report.title"/></a></li>
              </crm:permission>
              <crm:permission hasPermissions="REPORT_EMPLOYEE_REGISTRATION_SUMMARY_VIEW">
                <li><a href="<spring:url value="/report/template/Employee Registration Report" />"><spring:message code="employee.registration.report.title"/></a></li>
              </crm:permission>
              <crm:permission hasPermissions="REPORT_REWARDS_SUMMARY_BY_MONTH_VIEW">
                <li><a href="<spring:url value="/report/template/Rewards Summary By Month" />"><spring:message code="rewards.summary.by.month.report"/></a></li>
              </crm:permission>
              <crm:permission hasPermissions="REPORT_EMPLOYEE_REWARDS_BY_LOCATION_SUMMARY_VIEW">
                <li><a href="<spring:url value="/report/template/Employee Rewards by Location Report" />"><spring:message code="employee.rewards.by.location.report.title"/></a></li>
              </crm:permission>
            </ul>
          </crm:permission>
        </li>
        
        <li class="dropdown-submenu">
          <crm:permission hasPermissions="GC_ACCOUNT_RECEIVABLE_AGE, REPORT_ACCT_RCVBLE_DTL, GC_ACTIVATION_REPORT,
                                  GC_AFFILIATE_TRANSACTION_REDEMPTION, GC_AFFILIATE_TRANSACTION_SALES, GC_AGED_ANALYSIS,
                                  GC_B2B_CUSTOMER_PAYMENT_REPORT, GC_MANUFACTURE_COST_REPORT, REPORT_COLLECTION_DTL,
                                  GC_CONSUMED_REPORT, GC_DISCOUNT_RATE_REPORT, GC_DISCOUNT_SUMMARY_REPORT,
                                  GC_EXPIRE_AND_BALANCE_REPORT, GC_ABNORMAL_BALANCE_REPORT, GC_AVERAGE_TRANSACTION_AMT_REPORT,
                                  GC_ORDER_SUMMARY_REPORT, GC_SETTLEMENT_REPORT, GC_STORE_PERFORMANCE_REPORT, GC_JOURNAL_ENTRIES,
                                  GC_OUTSTANDING_REPORT, REPORT_GC_PRE_REACTIVATE_PREREDEEM_VIEW, GC_REDEEM_REPORT,
                                  REPORT_GC_ACCT_SALES_SUMMARY_VIEW, TRANSACTION_DETAIL_REPORT, TRANSACTION_SUMMARY_REPORT">
            <a tabindex="-1" href="#" data-toggle="dropdown" data-hover="dropdown" data-delay="500" data-close-others="true"><spring:message code="menutab_giftcard_management"/></a>
            <ul class="dropdown-menu">
              <li class="nav-header"><spring:message code="menutab_giftcard_management"/></li>
              <crm:permission hasPermissions="GC_ACCOUNT_RECEIVABLE_AGE">
                <li><spring:message var="acctRecvAge" code="report.gc.acctrecvage.title"/><a href="<c:url value="/report/template/Accounts Receivable Age" />">${acctRecvAge}</a></li>
              </crm:permission>
              
              
              <crm:permission hasPermissions="REPORT_ACCT_RCVBLE_DTL">
              <li><a href="<spring:url value="/report/template/Accounts Receivable Detail Report" />">Accounts Receivable Detail Report</a></li>
              </crm:permission>
              
              <crm:permission hasPermissions="GC_ACTIVATION_REPORT">
                <li><a href="<spring:url value="/report/template/Gift Card Add Value Summary By Month and Year" />"><spring:message code="gift.card.activation.report.menu"/></a></li>
              </crm:permission>
              <crm:permission hasPermissions="GC_AFFILIATE_TRANSACTION_REDEMPTION">
                <li><a href="<spring:url value="/report/template/Affiliate Transaction Redemption Report" />"><spring:message code="gc_affiliate_transaction_lbl_redemption_title"/></a></li>
              </crm:permission>
              <crm:permission hasPermissions="GC_AFFILIATE_TRANSACTION_SALES">
                <li><a href="<spring:url value="/report/template/Affiliate Transaction Sales Report" />"><spring:message code="gc_affiliate_transaction_lbl_sales_title"/></a></li>
              </crm:permission>
              <crm:permission hasPermissions="GC_AGED_ANALYSIS">
                <li><spring:message var="reportAgedAnalysis" code="gift.card.agedanalysis.report.menu"/><a href="<spring:url value="/report/template/Analysis Report" />">${reportAgedAnalysis}</a></li>
              </crm:permission>
              <crm:permission hasPermissions="GC_B2B_CUSTOMER_PAYMENT_REPORT">
                <li><a href="<spring:url value="/report/template/B2B Customer Payment Control Report" />"><spring:message code="gc.b2b.customer.payment.control.report.label"/></a></li>
              </crm:permission>
              <crm:permission hasPermissions="GC_MANUFACTURE_COST_REPORT">
                <li><a href="<spring:url value="/report/template/Card Manufacture Cost Report" />"><spring:message code="gc.mo.cost.report.label"/></a></li>
              </crm:permission>
              
              <crm:permission hasPermissions="REPORT_COLLECTION_DTL">
              <li><a href="<spring:url value="/report/template/Collection Detail Report" />"><spring:message code="collection.dtl.report"/></a></li>
              </crm:permission>
              
              <crm:permission hasPermissions="GC_CONSUMED_REPORT">
                <li><a href="<spring:url value="/report/template/Consumed Gift Card Report" />"><spring:message code="gc_consumed_report"/></a></li>
              </crm:permission>
              <crm:permission hasPermissions="GC_DISCOUNT_RATE_REPORT">
                <li><a href="<spring:url value="/report/template/Discount Rate Report" />"><spring:message code="gc.discount.rate.report.label"/></a></li>
              </crm:permission>
              <crm:permission hasPermissions="GC_DISCOUNT_SUMMARY_REPORT">
                <li><a href="<spring:url value="/report/template/Discount Summary Report" />"><spring:message code="gc.discount.summary.report.label"/></a></li>
              </crm:permission>
              <crm:permission hasPermissions="GC_EXPIRE_AND_BALANCE_REPORT">
                <li><a href="<spring:url value="/report/template/Expire and Balance Detail Report" />"><spring:message code="gc_expirebalance_report"/></a></li>
              </crm:permission>
              <crm:permission hasPermissions="GC_ABNORMAL_BALANCE_REPORT">
                <li><a href="<spring:url value="/report/template/Gift Card Abnormal Report" />"><spring:message code="gc.abnormal.balance.report.title"/></a></li>
              </crm:permission>
              <crm:permission hasPermissions="GC_AVERAGE_TRANSACTION_AMT_REPORT">
                <li><a href="<spring:url value="/report/template/Gift Card Average Transaction Amount Report" />"><spring:message code="gift.card.averagetxn.report.title"/></a></li>
              </crm:permission>
              <crm:permission hasPermissions="GC_ORDER_SUMMARY_REPORT">
                <li><spring:message var="orderSummary" code="report.gc.ordersummary.title"/><a href="<c:url value="/report/template/Gift Card Order Summary" />">${orderSummary}</a></li>
              </crm:permission>
              <crm:permission hasPermissions="GC_OUTSTANDING_REPORT">
                <li><a href="<spring:url value="/report/template/Gift Card Outstanding Value Summary By Month and Year" />"><spring:message code="gift.card.outstanding.report.menu"/></a></li>
              </crm:permission>
              <crm:permission hasPermissions="GC_REDEEM_REPORT">
                <li><a href="<spring:url value="/report/template/Gift Card Redeem Value Summary By Month and Year" />"><spring:message code="gift.card.redeem.report.menu"/></a></li>
              </crm:permission>
              <crm:permission hasPermissions="GC_SETTLEMENT_REPORT">
                <li><a href="<spring:url value="/report/template/Gift Card Settlement Report" />"><spring:message code="gc.settlement.report.label"/></a></li>
              </crm:permission>
              <crm:permission hasPermissions="GC_STORE_PERFORMANCE_REPORT">
                <li><a href="<spring:url value="/report/template/Gift Card Store Performance Report" />"><spring:message code="gc.store.performance.report.label"/></a></li>
              </crm:permission>
              <crm:permission hasPermissions="GC_JOURNAL_ENTRIES">
                <li><a href="<spring:url value="/report/template/Journal Entries Report" />"><spring:message code="journal.entries.report"/></a></li>
              </crm:permission>
              
              <crm:permission hasPermissions="REPORT_GC_PRE_REACTIVATE_PREREDEEM_VIEW">
                <li><a href="<spring:url value="/report/template/preActivatePreRedeemReport" />"><spring:message code="gc.pre.report.title"/></a></li>
              </crm:permission>
              
              <crm:permission hasPermissions="REPORT_GC_ACCT_SALES_SUMMARY_VIEW">
                <li><a href="<spring:url value="/giftcard/acct/sales/report" />"><spring:message code="gc.acct.sales.report.page.title"/></a></li>
              </crm:permission>
              <crm:permission hasPermissions="TRANSACTION_DETAIL_REPORT">
                <li><a href="<spring:url value="/report/template/Transaction Detail Report" />"><spring:message code="gc_transaction_lbl_title"/></a></li>
              </crm:permission>
              <crm:permission hasPermissions="TRANSACTION_SUMMARY_REPORT">
                <li><a href="<spring:url value="/report/template/Transaction Summary Report" />"><spring:message code="gc_transaction_summary_lbl_title"/></a></li>
              </crm:permission>
            </ul>
          </crm:permission>
        </li>
      </ul>
    </li>
  </sec:authorize>
</ul>

<style type="text/css">
<!--
.btn-main-menu, .btn-main-menu.menu2 {
  width: 200px;
  display: inline;
  display: inline-block;
}

.dropdown {
  display: inline;
  display: inline-block;
}

.navbar-inverse .nav li.dropdown > .dropdown-toggle .caret {
  border-bottom-color: #FFFFFF;
  border-top-color: #FFFFFF;
}

ul.nav li.dropdown:hover > ul.dropdown-menu {
  display: block;
}

ul.nav li.dropdown a {
  margin-bottom: 10px;
}

ul.nav li.dropdown ul.dropdown-menu {
  margin-top: 0px;
}

ul.nav {
  margin-bottom: 0px;
}
.navbar .pull-right > li > .dropdown-menu:after, .navbar .nav > li > .dropdown-menu.pull-right:after, .navbar .nav > li > .dropdown-menu:before {
    display: none;
}

<%--.content_menu { background-color: #014178; padding-bottom: 0px !important; } --%>
-->
</style>
<script type="text/javascript">
//<!--
$( document ).ready( function () {

    var COOKIE_NAME = "MENU_COOKIE_NAME";

    //unused
    function initMenuLinks() {
        $( "#nav-menu" ).find( "ul.dropdown-menu > li > a" ).click( function(e) {
            if ( e.target === this ) {
                console.log( "BEFORE SET " + $.cookie( COOKIE_NAME ) );
                $.cookie( COOKIE_NAME, ( $(this).closest( ".dropdown" ).attr( "id" ) ) );
                console.log( "SET " + $.cookie( COOKIE_NAME ) );
            }
        });
        if ( $.cookie( COOKIE_NAME ) ) {
            console.log( "GET " + $.cookie( COOKIE_NAME ) );
            $( "#nav-menu" ).find( "#" + $.cookie( COOKIE_NAME ) ).addClass( "active" );
        }
        var cookies = $.cookie();
        for(var cookie in cookies) {
            $.cookie( COOKIE_NAME, null, { path: '/' } );
            $.removeCookie( COOKIE_NAME, { path: '/' } );
        }
        $.cookie( COOKIE_NAME, null, { path: '/' } );
        $.removeCookie( COOKIE_NAME, { path: '/' } );
    }

    var $link = $( "a[href$='" + window.location.href.substring( window.location.href.indexOf( '<c:url value="/" />' ) ).replace( new RegExp( "%20", "g" ), " " ) + "']" );
    if ( $link.length ) {
        $link.closest( ".dropdown" ).addClass( "active" );
    }

});
//-->
</script>









