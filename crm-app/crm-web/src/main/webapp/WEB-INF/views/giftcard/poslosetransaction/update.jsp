<%@include file="/WEB-INF/views/common/taglibs.jsp" %>
<%@include file="/WEB-INF/views/common/confirm.jsp" %>

<div class="modal-dialog">
	<div class="modal-content">
		<div class="modal-header">
		  <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
			<spring:message code="gc.poslose" var="typeName" />
			<h4 class="modal-title"><spring:message code="global_menu_new" arguments="${typeName}"/></h4>
		</div>
		<div class="modal-body">
			<div id="contentError" class="hide alert alert-error">
		        <button type="button" class="close" onclick="$(this).parent().hide();">&times;</button>
		        <!-- <div><form:errors path="*"/></div>  -->
		        <div id="containerError">
		        </div>
	      	</div>
		
			<form:form id="poslosetxn" modelAttribute="poslosetxn" name="poslosetxn"
				action="${pageContext.request.contextPath}/poslose/" data-validate="validate">
				<form:hidden path="id"/>
				
				<div class="row-fluid">
					<div class="span4">
						<label class="control-label"><b class="required">*</b><spring:message code="gc.poslose.label.date.time" /></label>
						<form:hidden id="transactionDateTime" path="transactionDateTime" />
						<div class="row-fluid">
							<form:input path="dateString" id="dateInput" class="input-small"/>
							<form:input path="timeString" id="timeInput" class="input-small"/>
						</div>
					</div>
					<div class="span4">
						<label><b class="required">*</b><spring:message code="gc.poslose.label.store" /></label>
						<form:select path="storeCode" items="${stores}" itemValue="code" itemLabel="name"/>
					</div>
					<div class="span4">
						<label><b class="required">*</b><spring:message code="gc.poslose.label.gift.card.no" /></label>
						<form:input id="giftCardNo" path="giftCardNo" maxlength="20"/>
					</div>
				</div>
				<div class="row-fluid">
					<div class="span4">
						<label><label><b class="required">* </b><spring:message code="gc.poslose.label.transaction.no" /></label>
						<form:input path="transactionNo" />
					</div>
					<div id="div-reftx" class="span4 hide">
						<label><b class="required">* </b><spring:message code="gc.poslose.label.ref.transaction.no" /></label>
						<form:input id="refTransactionNo" path="refTransactionNo" />
					</div>
					<div class="span4">
						<label><label><b class="required">* </b><spring:message code="gc.poslose.label.pos.no" /></label>
						<form:input path="posNo" />
					</div>
					<div class="span4">
						<label><label><b class="required">* </b><spring:message code="gc.poslose.label.cashier.no" /></label>
						<form:input path="cashierNo" />
					</div>
				</div>
				<div class="row-fluid">
					<div class="span4">
						<label><spring:message code="gc.poslose.label.old.balance" /></label>
						<form:input id="oldBalance" path="oldBalance" readonly="true"/>
					</div>
					<div class="span4">
						<label><b class="required">* </b><spring:message code="gc.poslose.label.transaction.amount" /></label>
                        <div class="row-fluid">
                            <form:select id="type" path="type" items="${types}" class="input-small input-space-01"/>
    						<form:input id="transactionAmount" path="transactionAmount" class="input-small input-space-01"/>
                        </div>
					</div>
					<div class="span4">
						<label><spring:message code="gc.poslose.label.new.balance" /></label>
						<form:input id="newBalance" path="newBalance" readonly="true"/>
					</div>
				</div>
			</form:form>
		</div>
		<div class="modal-footer">			
    		<button type="button" id="saveButton" class="btn btn-primary" data-msg="<spring:message code="gc.poslose.confirm.save" />"><spring:message code="label_save" /></button>
    		<button type="button" id="cancelButton" class="btn btn-default" data-dismiss="modal"><spring:message code="label_cancel"/></button>
		</div>
	</div>
</div>

<script src='<c:url value="/js/bootstrap/bootstrap-datepicker.js"/>' type='text/javascript'><![CDATA[&nbsp; ]]></script>
<script src='<c:url value="/js/bootstrap/bootstrap-timepicker.min.js"/>' type='text/javascript'><![CDATA[&nbsp; ]]></script>
<link href="<c:url value="/css/bootstrap/datepicker.css" />" rel="stylesheet" />
<link href="<c:url value="/css/bootstrap/bootstrap-timepicker.min.css" />" rel="stylesheet" />
<link href="<c:url value="/styles/temp.css" />" rel="stylesheet" />
<script src="<spring:url value="/js/jquery/jquery.numberInput.js" />" ></script>

<script type="text/javascript">
$(document).ready(function() {
	$("#dateInput").datepicker({
		autoclose: true,
		format: "dd-mm-yyyy"
	});
	$("#timeInput").timepicker({ 
		showMeridian : false, 
		showSeconds : true,
		minuteStep: 1, 
		secondStep: 1,
		defaultTime: '00:00:01'
	});
	$("#saveButton").unbind("click").click(function(e) {
		e.preventDefault();
		$("#transactionDateTime").val($("#dateInput").val() + " " + $("#timeInput").val());
		$form = $("form#poslosetxn");
		var msg = $(this).data("msg");
		ajaxSubmit($form.attr("action") + $form.data('validate'), $form.serialize(), function(data) {
			getConfirm(msg, function(result) {
				if(result) {
					ajaxSubmit($form.attr("action") + "new", $form.serialize(), function() {
						location.reload();
					});
				}
			});
		});
	}); 

	$("#giftCardNo").change(checkGiftCardNo);
	$("#giftCardNo").focusout(checkGiftCardNo);
//	$("#transactionAmount").val(0);
	$("#oldBalance").val(0);
	$("#newBalance").val(0);
	$("#transactionAmount").change(computeNewBalance);
	
    $("#type").change(function(e){
      var selected = $(this).val();
      if('VOID_REDEMPTION' === selected || 'VOID_ACTIVATED' === selected) {
	$('#div-reftx').show();
      }else{
	$('#div-reftx').hide();	
      }
    }).change(computeNewBalance);
	//$("#transactionAmount").focusout(computeNewBalance);

	function checkGiftCardNo() {
		var gcNo = $(this).val();
		ajaxSubmit($("form#poslosetxn").attr("action") + "gc/" + gcNo, null, function(response) {
			if(response.success) {
				console.log(response.result);
				console.log(response.result.balance);
				$("#oldBalance").val(response.result.balance ? response.result.balance : 0);

				if('ACTIVATION' === $("#type option:selected").text()){
				  $("#transactionAmount").val(response.result.faceValue);
				}
				
				$("#transactionAmount").change();
			}
			else {
				console.log("bye");
				$("#oldBalance").val(0);
			}
		});
		  }

	function computeNewBalance() {
        var val = $("#transactionAmount").val() ? $("#transactionAmount").val() : 0;
        if($("#type").val() == 'REDEMPTION') {
            val = -1 * val;
        }
        
		$("#newBalance").val(parseInt($("#oldBalance").val()) + parseInt(val));
	}
});
</script>

<style type="text/css">
.bootstrap-timepicker-widget {
	z-index: 1051;
}
</style>