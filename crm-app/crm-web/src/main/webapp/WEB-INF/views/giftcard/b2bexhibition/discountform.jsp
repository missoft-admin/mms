<%@ include file="../../common/taglibs.jsp"%>

  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title" id="modalTitle"><spring:message code="b2b.discount" /></h4>
      </div>
      <div class="modal-body">
        <div id="contentError" class="hide alert alert-error">
          <button type="button" class="close" onclick="$(this).parent().hide();">&times;</button>
          <div><form:errors path="*"/></div>
        </div>
        <form:form id="discountForm" name="discountForm" modelAttribute="discountForm" action="${pageContext.request.contextPath}/giftcard/b2b/location/discounts/save" method="POST" enctype="${ENCTYPE}" cssClass="form-reset form-horizontal" >
          <form:hidden path="id"  />
          <form:hidden path="locId"  />
          
          <div class="control-group">
            <spring:message code="b2b.location.minamount" var="minAmount" />
            <label for="minAmount" class="control-label"><b class="required">* </b>${minAmount}</label> 
            <div class="controls">
              <form:input path="minAmount" class="form-control" placeholder="${minAmount}" />      
            </div>
          </div>
          
          <div class="control-group">
            <spring:message code="b2b.location.maxamount" var="maxAmount" />
            <label for="maxAmount" class="control-label">${maxAmount}</label> 
            <div class="controls">
              <form:input path="maxAmount" class="form-control" placeholder="${maxAmount}" />      
            </div>
          </div>
          
          <div class="control-group">
            <spring:message code="b2b.location.rate" var="discount" />
            <label for="discount" class="control-label"><b class="required">* </b>${discount}</label> 
            <div class="controls">
              <form:input path="discount" class="form-control" placeholder="${discount}" />      
            </div>
          </div>
        
        </form:form>
      </div>
      <div class="modal-footer">
        <button type="button" id="formSubmit" class="formBtn btn btn-primary"><spring:message code="label_submit" /></button>
        <button type="button" id="formCancel" data-dismiss="modal" class="btn btn-default"><spring:message code="label_cancel"/></button>
      </div>
    </div>
  </div>

<script type="text/javascript">

var CONTAINER_ERROR = "#contentError > div";
var CONTENT_ERROR = "#contentError";

$(document).ready(function() {
	
	$form = $("#discountForm");
	$locationDialog = $("#locationDialog");
	
	$("#formSubmit").click(submitForm);
	  
	  function submitForm() {
	    $(".formBtn").prop( "disabled", true );
	    $.post($form.attr("action"), $form.serialize(), function(data) {
	      $(".formBtn").prop( "disabled", false );
	      if (data.success) {
	        location.reload();
	      } 
	      else {
	        errorInfo = "";
	        for (i = 0; i < data.result.length; i++) {
	          errorInfo += "<br>" + (i + 1) + ". "
	              + ( data.result[i].code != undefined ? 
	                  data.result[i].code : data.result[i]);
	        }
	        $locationDialog.find(CONTAINER_ERROR).html("Please correct following errors: " + errorInfo);
	        $locationDialog.find(CONTENT_ERROR).show('slow');
	      }
	    }); 
	  }
});

</script>