-- Created Date: 2014-03-31
-- Updates:
---- 2014-04-02 - Added BANKS VARCHAR2 to CRM_MEMBER (2014-04-01_01-ALTERED_BANK_MEMBER_FIELD.sql);
---- 2014-04-14 - 2014-04-10_02-REFACTOR_PETS_FOOD_INTERESTS_MEMBER.sql
---- 2014-04-15 - 2014-04-08_01-ALTER_STR_NO_TO_VARCHAR.sql
    create table CRM_ADDRESS (
        id TT_BIGINT not null,
        CREATED_BY VARCHAR2(255),
        CREATED_DATETIME TIMESTAMP,
        LAST_UPDATED_BY VARCHAR2(255),
        LAST_UPDATED_DATETIME TIMESTAMP,
        BLOCK VARCHAR2(255),
        BUILDING VARCHAR2(255),
        CITY VARCHAR2(255),
        DISTRICT VARCHAR2(255),
        FLOOR TT_INTEGER,
        KM TT_INTEGER,
        POST_CODE VARCHAR2(255),
        PROVINCE VARCHAR2(50),
        ROOM VARCHAR2(255),
        RT VARCHAR2(255),
        RW VARCHAR2(255),
        STREET VARCHAR2(4000),
        STR_NO VARCHAR2(255),
        SUBDISTRICT VARCHAR2(255),
        VILLAGE VARCHAR2(255),
        primary key (id)
    );

    create table CRM_MEMBER_GRP (
        id TT_BIGINT not null,
        CREATED_BY VARCHAR2(255),
        CREATED_DATETIME TIMESTAMP,
        LAST_UPDATED_BY VARCHAR2(255),
        LAST_UPDATED_DATETIME TIMESTAMP,
        CARD_TYPE_GROUP_CODE VARCHAR2(255),
        MEMBERS VARCHAR2(255),
        NAME VARCHAR2(255),
        POINTS_ENABLED CHAR(1),
        POINTS_FROM TT_BIGINT,
        POINTS_TO TT_BIGINT,
        DATE_RANGE_FROM TT_DATE,
        DATE_RANGE_TO TT_DATE,
        ENABLED CHAR(1),
        FREQUENCY CHAR(1),
        FREQUENCY_FROM TT_BIGINT,
        FREQUENCY_TO TT_BIGINT,
        PRODUCT_GROUP TT_BIGINT,
        RECENCY VARCHAR2(255),
        RECENT_VALUE VARCHAR2(255),
        SPEND CHAR(1),
        SPEND_FROM TT_BIGINT,
        SPEND_PER_TRANSACTION CHAR(1),
        SPEND_TO TT_BIGINT,
        STORE_GROUP TT_BIGINT,
        primary key (id)
    );

    create table CRM_PROGRAM (
        id TT_BIGINT not null,
        CREATED_BY VARCHAR2(255),
        CREATED_DATETIME TIMESTAMP,
        LAST_UPDATED_BY VARCHAR2(255),
        LAST_UPDATED_DATETIME TIMESTAMP,
        DESCRIPTION VARCHAR2(255),
        endDate TIMESTAMP,
        startDate TIMESTAMP,
        NAME VARCHAR2(255),
        TYPE VARCHAR2(20) not null,
        STATUS VARCHAR2(15),
        primary key (id)
    );

    create table CRM_REF_LOOKUP_HDR (
        CODE VARCHAR2(15) not null,
        CREATED_BY VARCHAR2(255),
        CREATED_DATETIME TIMESTAMP,
        LAST_UPDATED_BY VARCHAR2(255),
        LAST_UPDATED_DATETIME TIMESTAMP,
        DESCRIPTION VARCHAR2(100),
        primary key (CODE)
    );

    create table CRM_STORE_GROUP (
        id TT_BIGINT not null,
        CREATED_BY VARCHAR2(255),
        CREATED_DATETIME TIMESTAMP,
        LAST_UPDATED_BY VARCHAR2(255),
        LAST_UPDATED_DATETIME TIMESTAMP,
        NAME VARCHAR2(255),
        STORES VARCHAR2(4000),
        primary key (id)
    );

    create table CRM_APPLICATION_CONFIG (
        id TT_BIGINT not null,
        CREATED_BY VARCHAR2(255),
        CREATED_DATETIME TIMESTAMP,
        LAST_UPDATED_BY VARCHAR2(255),
        LAST_UPDATED_DATETIME TIMESTAMP,
        CONFIG_KEY VARCHAR2(255) not null,
        CONFIG_VALUE VARCHAR2(255),
        primary key (id)
    );

    create table CRM_REF_LOOKUP_DTL (
        CODE VARCHAR2(15) not null,
        CREATED_BY VARCHAR2(255),
        CREATED_DATETIME TIMESTAMP,
        LAST_UPDATED_BY VARCHAR2(255),
        LAST_UPDATED_DATETIME TIMESTAMP,
        DESCRIPTION VARCHAR2(100) not null,
        STATUS VARCHAR2(8) not null,
        HDR VARCHAR2(15) not null,
        primary key (CODE)
    );

    create table CRM_PROMOTION_STORE_GRP (
        id VARCHAR2(255) not null,
        CREATED_BY VARCHAR2(255),
        CREATED_DATETIME TIMESTAMP,
        LAST_UPDATED_BY VARCHAR2(255),
        LAST_UPDATED_DATETIME TIMESTAMP,
        PROMOTION_ID TT_BIGINT,
        STORE_GRP_ID TT_BIGINT,
        primary key (id)
    );

    create table CRM_PROMOTION_REWARD (
        id TT_BIGINT not null,
        CREATED_BY VARCHAR2(255),
        CREATED_DATETIME TIMESTAMP,
        LAST_UPDATED_BY VARCHAR2(255),
        LAST_UPDATED_DATETIME TIMESTAMP,
        BASE_AMOUNT NUMBER(19,2),
        BASE_FACTOR BINARY_DOUBLE,
        DISCOUNT BINARY_DOUBLE,
        FIXED CHAR(1),
        MAX_AMOUNT NUMBER(19,2),
        MAX_BONUS NUMBER(19,2),
        MAX_QTY TT_BIGINT,
        MIN_AMOUNT NUMBER(19,2),
        MIN_QTY TT_BIGINT,
        REMARK VARCHAR2(1000),
        PROMO_ID TT_BIGINT,
        REWARD_TYPE VARCHAR2(15),
        primary key (id)
    );

    create table CRM_PROMOTION_PRODUCT_GRP (
        id VARCHAR2(255) not null,
        CREATED_BY VARCHAR2(255),
        CREATED_DATETIME TIMESTAMP,
        LAST_UPDATED_BY VARCHAR2(255),
        LAST_UPDATED_DATETIME TIMESTAMP,
        PROMOTION_ID TT_BIGINT,
        PRODUCT_GRP_ID TT_BIGINT,
        primary key (id)
    );

    create table CRM_PROMOTION_PAYMENTTYPE (
        id VARCHAR2(255) not null,
        CREATED_BY VARCHAR2(255),
        CREATED_DATETIME TIMESTAMP,
        LAST_UPDATED_BY VARCHAR2(255),
        LAST_UPDATED_DATETIME TIMESTAMP,
        WILDCARDS VARCHAR2(4000),
        PROMOTION_ID TT_BIGINT,
        PAYMENTTYPE_ID VARCHAR2(15),
        primary key (id)
    );

    create table CRM_PROMOTION_MEMBER_GRP (
        id VARCHAR2(255) not null,
        CREATED_BY VARCHAR2(255),
        CREATED_DATETIME TIMESTAMP,
        LAST_UPDATED_BY VARCHAR2(255),
        LAST_UPDATED_DATETIME TIMESTAMP,
        PROMOTION_ID TT_BIGINT,
        MEMBER_GRP_ID TT_BIGINT,
        primary key (id)
    );

    create table CRM_PROMOTION (
        id TT_BIGINT not null,
        CREATED_BY VARCHAR2(255),
        CREATED_DATETIME TIMESTAMP,
        LAST_UPDATED_BY VARCHAR2(255),
        LAST_UPDATED_DATETIME TIMESTAMP,
        AFFECTED_DAYS VARCHAR2(255),
        ACCEPT_EMAIL CHAR(1),
        ACCEPT_MAIL CHAR(1),
        ACCEPT_PHONE CHAR(1),
        ACCEPT_SMS CHAR(1),
        SMS CHAR(1),
        DESCRIPTION VARCHAR2(255),
        END_DATE TIMESTAMP,
        END_TIME TT_TIME,
        NAME VARCHAR2(255),
        START_DATE TIMESTAMP,
        START_TIME TT_TIME,
        CAMPAIGN_ID TT_BIGINT,
        REWARD_TYPE VARCHAR2(15),
        STATUS VARCHAR2(15),
        primary key (id)
    );

    create table CRM_CAMPAIGN (
        id TT_BIGINT not null,
        CREATED_BY VARCHAR2(255),
        CREATED_DATETIME TIMESTAMP,
        LAST_UPDATED_BY VARCHAR2(255),
        LAST_UPDATED_DATETIME TIMESTAMP,
        DESCRIPTION VARCHAR2(255),
        endDate TIMESTAMP,
        startDate TIMESTAMP,
        NAME VARCHAR2(255),
        PROJECT_ID TT_BIGINT,
        STATUS VARCHAR2(15),
        primary key (id)
    );

    create table CRM_PRODUCT_GRP (
        id TT_BIGINT not null,
        CREATED_BY VARCHAR2(255),
        CREATED_DATETIME TIMESTAMP,
        LAST_UPDATED_BY VARCHAR2(255),
        LAST_UPDATED_DATETIME TIMESTAMP,
        BRAND VARCHAR2(255),
        EXC_PRODUCTS VARCHAR2(4000),
        NAME VARCHAR2(255),
        PRODUCTS VARCHAR2(4000),
        PRODUCTS_CFNS VARCHAR2(4000),
        VALID CHAR(1),
        CLASSIFICATION VARCHAR2(15),
        SUBCLASSIFICATION VARCHAR2(15),
        primary key (id)
    );

    create table CRM_MEMBER_GRP_FIELD_VALUE (
        id TT_BIGINT not null,
        CREATED_BY VARCHAR2(255),
        CREATED_DATETIME TIMESTAMP,
        LAST_UPDATED_BY VARCHAR2(255),
        LAST_UPDATED_DATETIME TIMESTAMP,
        VALUE VARCHAR2(255),
        MEMBER_GRP_FIELD TT_BIGINT not null,
        OPERAND VARCHAR2(15),
        primary key (id)
    );

    create table CRM_MEMBER_GRP_FIELD (
        id TT_BIGINT not null,
        CREATED_BY VARCHAR2(255),
        CREATED_DATETIME TIMESTAMP,
        LAST_UPDATED_BY VARCHAR2(255),
        LAST_UPDATED_DATETIME TIMESTAMP,
        MEMBER_GRP TT_BIGINT not null,
        FIELD VARCHAR2(15),
        TYPE VARCHAR2(15),
        primary key (id)
    );

    create table CRM_MEMBER (
        id TT_BIGINT not null,
        CREATED_BY VARCHAR2(255),
        CREATED_DATETIME TIMESTAMP,
        LAST_UPDATED_BY VARCHAR2(255),
        LAST_UPDATED_DATETIME TIMESTAMP,
        ACCOUNT_ID VARCHAR2(255) not null,
        STATUS VARCHAR2(20) not null,
        TIME_TO_CALL VARCHAR2(255),
        CARD_NO VARCHAR2(50),
        ACCEPT_EMAIL CHAR(1),
        ACCEPT_MAIL CHAR(1),
        ACCEPT_PHONE CHAR(1),
        ACCEPT_SMS CHAR(1),
        SMS CHAR(1),
        COMPANY_NAME VARCHAR2(255),
        CONTACT VARCHAR2(255) not null,
        BANKS VARCHAR2(255),
        BIRTHDATE TIMESTAMP not null,
        CARS_OWNED TT_INTEGER,
        CHILDREN_COUNT TT_INTEGER,
        CLOSEST_STORE VARCHAR2(255),
        CREDIT_CARD_OWNERSHIP CHAR(1),
        DOMESTIC_HELPERS TT_INTEGER,
        FAX_NO VARCHAR2(255),
        INSURANCE_OWNERSHIP CHAR(1),
        INTERESTS VARCHAR2(255),
        NAME_NATIVE VARCHAR2(120),
        PLACE_OF_BIRTH VARCHAR2(255),
        POSTAL_CODE VARCHAR2(255),
        VEHICLES_OWNED VARCHAR2(255),
        EMAIL VARCHAR2(255),
        ACCOUNT_ENABLED CHAR(1) not null,
        FIRST_NAME VARCHAR2(255),
        HOME_PHONE VARCHAR2(255),
        ID_NUMBER VARCHAR2(255),
        KTP VARCHAR2(255),
        LAST_NAME VARCHAR2(255),
        LOYALTY_CARD_NO VARCHAR2(255),
        CORRESPONDENCE_ADDRESS VARCHAR2(255),
        INFORMED_ABOUT VARCHAR2(255),
        INTERESTED_ACTIVITIES VARCHAR2(255),
        INTERESTED_PRODUCTS VARCHAR2(255),
        TRIPS_TO_STORE TT_INTEGER,
        VISITED_SUPERMARKETS VARCHAR2(255),
        CREATED_FROM VARCHAR2(255),
        MIDDLE_NAME VARCHAR2(255),
        NPWP_ADDRESS VARCHAR2(255),
        NPWP_ID VARCHAR2(255),
        NPWP_NAME VARCHAR2(255),
        PARENT_ACCOUNT_ID VARCHAR2(255),
        PASSWORD VARCHAR2(255),
        PIN VARCHAR2(255) not null,
        BUSINESS_EMAIL VARCHAR2(255),
        BUSINESS_LICENSE VARCHAR2(255),
        BUSINESS_NAME VARCHAR2(255),
        BUSINESS_PHONE VARCHAR2(255),
        POSITION VARCHAR2(255),
        POTENTIAL_BUYING_PER_MONTH VARCHAR2(255),
        RADIUS VARCHAR2(255),
        REG_ID VARCHAR2(255),
        TRANS_CODE_1 TT_SMALLINT,
        TRANS_CODE_2 TT_SMALLINT,
        TRANS_CODE_3 TT_SMALLINT,
        TRANS_CODE_4 TT_SMALLINT,
        TRANS_CODE_LONG1 TT_SMALLINT,
        TRANS_CODE_LONG2 TT_SMALLINT,
        TRANS_CODE_SHORT TT_SMALLINT,
        ZONE VARCHAR2(255),
        REGISTERED_STORE VARCHAR2(255),
        SOCIAL_MEDIA_ACCTS VARCHAR2(255),
        STORE_NAME VARCHAR2(255),
        TOTAL_POINTS BINARY_DOUBLE,
        username VARCHAR2(255) not null,
        VALIDATION_CODE VARCHAR2(120),
        VERSION TT_INTEGER,
        ADDRESS TT_BIGINT,
        CARD_TYPE VARCHAR2(15),
        EDUCATION VARCHAR2(15),
        FAMILY_SIZE VARCHAR2(15),
        GENDER VARCHAR2(15),
        HOUSEHOLD_INCOME VARCHAR2(15),
        MARITAL_STATUS VARCHAR2(15),
        NATIONALITY VARCHAR2(15),
        OCCUPATION VARCHAR2(15),
        PERSONAL_INCOME VARCHAR2(15),
        PREFERED_LANGUAGE VARCHAR2(15),
        RELIGION VARCHAR2(15),
        TITLE VARCHAR2(15),
        EMP_TYPE VARCHAR2(15),
        MEMBER_TYPE VARCHAR2(15),
        BUSINESS_ADDRESS TT_BIGINT,
        BUSINESS_FIELD VARCHAR2(15),
        CUSTOMER_GROUP VARCHAR2(15),
        CUSTOMER_SEGREGATION VARCHAR2(15),
        DEPARTMENT VARCHAR2(15),
        NO_OF_EMP VARCHAR2(15),
        primary key (id)
    );

    create table CRM_LC_ANNUAL_FEE (
        id TT_BIGINT not null,
        CREATED_BY VARCHAR2(255),
        CREATED_DATETIME TIMESTAMP,
        LAST_UPDATED_BY VARCHAR2(255),
        LAST_UPDATED_DATETIME TIMESTAMP,
        ANNUAL_FEE BINARY_DOUBLE,
        REPLACEMENT_FEE BINARY_DOUBLE,
        CARD_TYPE VARCHAR2(15),
        COMPANY VARCHAR2(15),
        primary key (id)
    );

    create table CRM_LC_INVENTORY (
        id TT_BIGINT not null,
        CREATED_BY VARCHAR2(255),
        CREATED_DATETIME TIMESTAMP,
        LAST_UPDATED_BY VARCHAR2(255),
        LAST_UPDATED_DATETIME TIMESTAMP,
        ACTIVATION_DATE TT_DATE,
        ALLOCATE_DATE TT_DATE,
        BARCODE VARCHAR2(255),
        BATCH_REF_NO VARCHAR2(255),
        BOX_NO VARCHAR2(255),
        ENCODED_BY VARCHAR2(255),
        EXPIRY_DATE TT_DATE,
        FOR_BURNING CHAR(1),
        LOCATION VARCHAR2(255),
        NEW_TRANSFER_TO VARCHAR2(255),
        RECEIPT_NO VARCHAR2(255),
        RECEIVE_DATE TT_DATE,
        RECEIVED_AT VARCHAR2(255),
        REFERENCE_NO VARCHAR2(255),
        SEQUENCE VARCHAR2(255),
        TRANSFER_TO VARCHAR2(255),
        PRODUCT TT_BIGINT,
        STATUS VARCHAR2(15),
        primary key (id)
    );

    create table CRM_LC_INVENTORY_HIST (
        id VARCHAR2(255) not null,
        CREATED_BY VARCHAR2(255),
        CREATED_DATETIME TIMESTAMP,
        LAST_UPDATED_BY VARCHAR2(255),
        LAST_UPDATED_DATETIME TIMESTAMP,
        ANNUAL_FEE BINARY_DOUBLE,
        CREATED_DATE TT_DATE,
        LOCATION VARCHAR2(255) not null,
        ORIGIN VARCHAR2(255),
        REPLACEMENT_FEE BINARY_DOUBLE,
        TRACK CHAR(1),
        POS_TXN_NO VARCHAR2(255),
        TRANSFER_TO VARCHAR2(255),
        INVENTORY_NO TT_BIGINT not null,
        STATUS VARCHAR2(15) not null,
        primary key (id)
    );

    create table CRM_POINTS (
        id VARCHAR2(255) not null,
        CREATED_BY VARCHAR2(255),
        CREATED_DATETIME TIMESTAMP,
        LAST_UPDATED_BY VARCHAR2(255),
        LAST_UPDATED_DATETIME TIMESTAMP,
        REASON VARCHAR2(1000),
        COMMENTS VARCHAR2(1000),
        EXPIRE_EXCESS BINARY_DOUBLE,
        EXPIRY_DATE TT_DATE,
        INCLUSIVE_DATE_FROM TIMESTAMP,
        INCLUSIVE_DATE_TO TIMESTAMP,
        LAST_REDEEM TIMESTAMP,
        RETURN_TXN_NO VARCHAR2(255),
        STATUS VARCHAR2(255),
        STORE_CODE VARCHAR2(255),
        TXN_AMOUNT BINARY_DOUBLE,
        TXN_DATE TIMESTAMP,
        TXN_MEDIA VARCHAR2(255),
        TXN_NO VARCHAR2(255),
        TXN_POINTS BINARY_DOUBLE,
        TXN_TYPE VARCHAR2(255),
        VALID_PERIOD TT_INTEGER,
        VALIDATED_BY VARCHAR2(255),
        VALIDATED_DATE TIMESTAMP,
        MEMBER_ID TT_BIGINT,
        TXN_OWNER TT_BIGINT,
        primary key (id)
    );

    create table CRM_EMPLOYEE_PURCHASE_TXN (
        id VARCHAR2(255) not null,
        CREATED_BY VARCHAR2(255),
        CREATED_DATETIME TIMESTAMP,
        LAST_UPDATED_BY VARCHAR2(255),
        LAST_UPDATED_DATETIME TIMESTAMP,
        APPROVAL_DATE TIMESTAMP,
        APPROVED_BY VARCHAR2(255),
        REASON VARCHAR2(10000),
        REMARKS VARCHAR2(10000),
        STATUS VARCHAR2(20),
        TXN_AMOUNT BINARY_DOUBLE,
        TXN_DATE TIMESTAMP,
        TXN_NO VARCHAR2(255),
        TXN_TYPE VARCHAR2(255),
        ACCOUNT_ID TT_BIGINT,
        STORE_CODE TT_INTEGER,
        primary key (id)
    );

    create table CRM_POINTS_PROMOTION (
        id VARCHAR2(255) not null,
        CREATED_BY VARCHAR2(255),
        CREATED_DATETIME TIMESTAMP,
        LAST_UPDATED_BY VARCHAR2(255),
        LAST_UPDATED_DATETIME TIMESTAMP,
        TXN_NO VARCHAR2(255),
        TXN_POINTS TT_BIGINT,
        MEMBER_ID TT_BIGINT,
        PROMOTION TT_BIGINT,
        STORE_CODE TT_INTEGER,
        primary key (id)
    );

    alter table CRM_REF_LOOKUP_DTL
        add constraint FK_li5b4ep30vhpfnj3om1gji333
        foreign key (HDR)
        references CRM_REF_LOOKUP_HDR;

    alter table CRM_PROMOTION_STORE_GRP
        add constraint FK_a0uvmw1sro7sk8pg97uung23f
        foreign key (PROMOTION_ID)
        references CRM_PROMOTION;

    alter table CRM_PROMOTION_STORE_GRP
        add constraint FK_1tt1ofr8adikfskxg1xnbsied
        foreign key (STORE_GRP_ID)
        references CRM_STORE_GROUP;

    alter table CRM_PROMOTION_REWARD
        add constraint FK_8wjt393m5f7l8ucpdc0tow3dx
        foreign key (PROMO_ID)
        references CRM_PROMOTION;

    alter table CRM_PROMOTION_REWARD
        add constraint FK_ic7qy0mlsau879uaejuddypae
        foreign key (REWARD_TYPE)
        references CRM_REF_LOOKUP_DTL;

    alter table CRM_PROMOTION_PRODUCT_GRP
        add constraint FK_a4k83jqhk6ak6wkcbirp1gsii
        foreign key (PROMOTION_ID)
        references CRM_PROMOTION;

    alter table CRM_PROMOTION_PRODUCT_GRP
        add constraint FK_2jxb0wgm1df4vbx7j5ncr1957
        foreign key (PRODUCT_GRP_ID)
        references CRM_PRODUCT_GRP;

    alter table CRM_PROMOTION_PAYMENTTYPE
        add constraint FK_a412atnqflpw3f6agwd4dyvvw
        foreign key (PROMOTION_ID)
        references CRM_PROMOTION;

    alter table CRM_PROMOTION_PAYMENTTYPE
        add constraint FK_l2oopbeqw9bc0v4gd0cenv0b6
        foreign key (PAYMENTTYPE_ID)
        references CRM_REF_LOOKUP_DTL;

    alter table CRM_PROMOTION_MEMBER_GRP
        add constraint FK_50mq36pxrs5uw60ifjh5h2dv3
        foreign key (PROMOTION_ID)
        references CRM_PROMOTION;

    alter table CRM_PROMOTION_MEMBER_GRP
        add constraint FK_6tycqacsif50nayxws8vje9nr
        foreign key (MEMBER_GRP_ID)
        references CRM_MEMBER_GRP;

    alter table CRM_PROMOTION
        add constraint FK_6nitj6ny9rh8l0f8ebdr2aovu
        foreign key (CAMPAIGN_ID)
        references CRM_CAMPAIGN;

    alter table CRM_PROMOTION
        add constraint FK_lc6asi9wf8c1hbd2it2r0nkx4
        foreign key (REWARD_TYPE)
        references CRM_REF_LOOKUP_DTL;

    alter table CRM_PROMOTION
        add constraint FK_n445fppjdc9faok3hk1aw0two
        foreign key (STATUS)
        references CRM_REF_LOOKUP_DTL;

    alter table CRM_CAMPAIGN
        add constraint FK_jxj1bxcuit8mii9br935lhucn
        foreign key (PROJECT_ID)
        references CRM_PROGRAM;

    alter table CRM_CAMPAIGN
        add constraint FK_17sxuqgrl1ib3eycmv8ar7rhk
        foreign key (STATUS)
        references CRM_REF_LOOKUP_DTL;

    alter table CRM_PRODUCT_GRP
        add constraint FK_pb1vvf0bnf3bs5fuurbvvuq0w
        foreign key (CLASSIFICATION)
        references CRM_REF_LOOKUP_DTL;

    alter table CRM_PRODUCT_GRP
        add constraint FK_glsjjfvxd0vr9b0g64du349bf
        foreign key (SUBCLASSIFICATION)
        references CRM_REF_LOOKUP_DTL;

    alter table CRM_MEMBER_GRP_FIELD_VALUE
        add constraint FK_4lfwy5a89nw8hnjt3nkhfcf1p
        foreign key (MEMBER_GRP_FIELD)
        references CRM_MEMBER_GRP_FIELD;

    alter table CRM_MEMBER_GRP_FIELD_VALUE
        add constraint FK_tmarn7lvu9l4pudwbe8nsehqn
        foreign key (OPERAND)
        references CRM_REF_LOOKUP_DTL;

    alter table CRM_MEMBER_GRP_FIELD
        add constraint FK_cnol0drpb629nkwl28mt8pfbd
        foreign key (MEMBER_GRP)
        references CRM_MEMBER_GRP;

    alter table CRM_MEMBER_GRP_FIELD
        add constraint FK_mkh8i18qu98m9a4l4dtv2oami
        foreign key (FIELD)
        references CRM_REF_LOOKUP_HDR;

    alter table CRM_MEMBER_GRP_FIELD
        add constraint FK_407a445ylgg7cp2v7dwo83egg
        foreign key (TYPE)
        references CRM_REF_LOOKUP_DTL;

    alter table CRM_MEMBER
        add unique (ACCOUNT_ID);

    alter table CRM_MEMBER
        add unique (CONTACT);

    alter table CRM_MEMBER
        add unique (LOYALTY_CARD_NO);

    alter table CRM_MEMBER
        add unique (username);

    alter table CRM_POINTS
        add constraint FK_9syii89omui408wduaxod6sqc
        foreign key (MEMBER_ID)
        references CRM_MEMBER;

    alter table CRM_POINTS
        add constraint FK_9yx5tvnepyq8t0ffradnhw8b1
        foreign key (TXN_OWNER)
        references CRM_MEMBER;

    alter table CRM_EMPLOYEE_PURCHASE_TXN
        add constraint FK_kdfktytk6kc6axp81cpgctv91
        foreign key (ACCOUNT_ID)
        references CRM_MEMBER;

    alter table CRM_EMPLOYEE_PURCHASE_TXN
        add constraint FK_5sgu5qeswin1dm0vagq3wh7c5
        foreign key (STORE_CODE)
        references STORE;

    alter table CRM_POINTS_PROMOTION
        add constraint FK_2masxh59yidbn37u7otni2dq1
        foreign key (MEMBER_ID)
        references CRM_MEMBER;

    alter table CRM_POINTS_PROMOTION
        add constraint FK_owlbe368epw67v9gilqe1xqnr
        foreign key (PROMOTION)
        references CRM_PROMOTION;

    alter table CRM_POINTS_PROMOTION
        add constraint FK_p6stb9p43nq16b6rxh8ktjf5b
        foreign key (STORE_CODE)
        references STORE;