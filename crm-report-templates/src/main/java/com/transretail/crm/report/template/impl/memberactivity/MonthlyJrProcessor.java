package com.transretail.crm.report.template.impl.memberactivity;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.transretail.crm.common.reporting.exception.ReportException;
import com.transretail.crm.common.service.dto.response.CodeDescDto;
import com.transretail.crm.common.util.IOUtils;
import com.transretail.crm.core.entity.enums.ReportType;
import com.transretail.crm.report.template.dynamicreports.Templates;
import net.sf.dynamicreports.jasper.builder.JasperReportBuilder;
import net.sf.dynamicreports.report.builder.component.HorizontalListBuilder;
import net.sf.dynamicreports.report.builder.component.SubreportBuilder;
import net.sf.dynamicreports.report.builder.style.StyleBuilder;
import net.sf.dynamicreports.report.constant.HorizontalAlignment;
import net.sf.dynamicreports.report.constant.PageOrientation;
import net.sf.dynamicreports.report.exception.DRException;
import net.sf.jasperreports.engine.*;
import net.sf.jasperreports.engine.export.JRExportProgressMonitor;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.DateFormatSymbols;
import java.text.SimpleDateFormat;
import java.util.*;

import static net.sf.dynamicreports.report.builder.DynamicReports.*;

/**
 * @author Allan G. Ramirez (agramirez@exist.com)
 */
public class MonthlyJrProcessor extends AbstractMemberActivityReport {
    private static final String DYNAMIC_COL_PFX = "col";
    private static final DateFormat YEAR_MONTH_FORMATTER = new SimpleDateFormat("yyyyMM");
    private List<CodeDescDto> customerGroups;
    private String montlySegmentationSummaryReportTitle;
    private static String[] monthNames;

    static {
        DateFormatSymbols symbols = new DateFormatSymbols(Locale.ENGLISH);
        monthNames = symbols.getMonths();
    }

    public MonthlyJrProcessor(String reportFileName, String title, String subTitle, DataSource dataSource, ReportType reportType) {
        super(reportFileName, title, subTitle, dataSource, reportType);
    }

    public void setCustomerGroups(List<CodeDescDto> customerGroups) {
        this.customerGroups = customerGroups;
    }

    public void setMontlySegmentationSummaryReportTitle(String montlySegmentationSummaryReportTitle) {
        this.montlySegmentationSummaryReportTitle = montlySegmentationSummaryReportTitle;
    }

    @Override
    public List<JasperPrint> getJasperPrints() throws ReportException {
        List<JasperPrint> jasperPrints = Lists.newArrayList();
        jasperPrints.add(createMemberListReport());
        jasperPrints.add(createMontlyCustGroupSummaryReport());
        return jasperPrints;
    }

    private JasperPrint createMemberListReport() throws ReportException {
        try {
            if (dateFrom.toString("MMMM").toUpperCase().equals(dateTo.toString("MMMM").toUpperCase())) {
                this.title = this.title + "\n" + dateFrom.toString("MMMM").toUpperCase();
            } else {
                this.title = this.title + "\n" +  dateFrom.toString("MMMM").toUpperCase() + " to " + dateTo.toString("MMMM").toUpperCase();
            }


            return createJRBuilderWithTitleAndSubTitle()
                .setPageMargin(Templates.DEFAULT_MARGIN)
                .addColumn(createColumn("Account ID", "accountId", String.class, 100))
                .addColumn(createColumn("Member Name", "memberName", String.class, 200))
                .addColumn(createColumn("Business Name", "businessName", String.class, 200))
                .addColumn(createColumn("Business Segmentation", "segmentation", String.class, 150))
                .addColumn(createColumn("Total Sales", "totalSales", Long.class, 100))
                .addColumn(createColumn("Total Kena Pajak", "totalSalesAfterVat", Long.class, 100))
                .addColumn(createColumn("PPN", "ppn", Long.class, 100))
                .addColumn(createColumn("Total Mark up", "totalMarkup", Long.class, 100))
                .addColumn(createColumn("PPN Mark up", "ppnMarkup", Long.class, 100))
                .addColumn(createColumn("Freq Visit", "freqVisit", Long.class, 100))
                .addColumn(createColumn("Registration Date", "registrationDate", String.class, 150))
                .addColumn(createColumn("Last Visit Date", "lastVisitDate", String.class, 150))
                .setDataSource(createJrDataSource())
                .toJasperPrint();
        } catch (DRException e) {
            throw new ReportException(e);
        }
    }

    private JasperPrint createMontlyCustGroupSummaryReport() throws ReportException {
        final ConnectionHolder connectionHolder = new ConnectionHolder();
        try {
            Connection con = dataSource.getConnection();
            connectionHolder.con = con;
            // Close connection after page export
            JRExportProgressMonitor afterPageExportListener = new JRExportProgressMonitor() {
                @Override
                public void afterPageExport() {
                    IOUtils.INSTANCE.close(connectionHolder.con, true);
                }
            };
            setAfterPageExportListener(afterPageExportListener);
        } catch (SQLException e) {
            throw new ReportException(e);
        }


        JasperReportBuilder mainReport = report()
            .setPageFormat(1690, 2380, PageOrientation.PORTRAIT)
            .setPageMargin(Templates.DEFAULT_MARGIN)
            .title(cmp.horizontalList()
                .add(cmp.text("Groserindo Member Report").setStyle(stl.style().setFontSize(20)))
                .newRow())
            .setDataSource(new JREmptyDataSource(1));

        try {
            int currMonth = dateFrom.getMonthOfYear();
            int currYear = dateFrom.getYear();
            int endMonth = dateTo.getMonthOfYear();
            int endYear = dateTo.getYear();

            List<YearMonthTuple> yearMonthTuples = Lists.newLinkedList();
            while (true) {
                YearMonthTuple tuple = new YearMonthTuple();
                tuple.year = currYear;
                tuple.month = currMonth;
                yearMonthTuples.add(tuple);

                if (currMonth == 12) {
                    currMonth = 1;
                    currYear++;

                    mainReport = mainReport.addDetail(cmp.horizontalList().add(cmp.text("")),
                        // Registered Members
                        createCustGroupSubReport(connectionHolder.con, yearMonthTuples, false),
                        // Buying Customer
                        createCustGroupSubReport(connectionHolder.con, yearMonthTuples, true));

                    yearMonthTuples = Lists.newLinkedList();
                } else {
                    currMonth++;
                }

                if (currYear == endYear) {
                    if (currMonth == endMonth) {
                        tuple = new YearMonthTuple();
                        tuple.year = currYear;
                        tuple.month = currMonth;
                        yearMonthTuples.add(tuple);

                        mainReport = mainReport.addDetail(cmp.horizontalList().add(cmp.text("")),
                            // Registered Members
                            createCustGroupSubReport(connectionHolder.con, yearMonthTuples, false),
                            // Buying Customer
                            createCustGroupSubReport(connectionHolder.con, yearMonthTuples, true));
                        break;
                    }
                }
            }

            return mainReport.toJasperPrint();
        } catch (Exception e) {
            // Close the connection
            getAfterPageExportListener().afterPageExport();
            throw new ReportException(e);
        }
    }

    private SubreportBuilder createCustGroupSubReport(Connection con, List<YearMonthTuple> list, boolean buying) throws
        ReportException {
        int counter = 0;
        Iterator<YearMonthTuple> it = list.iterator();

        JasperReportBuilder builder = createBaseJRBuilder();

        int custGroupColWidth = 200;
        int monthHalfWidth = 50;
        if (buying) {
            builder.setColumnTitleStyle(stl.style().bold());
            builder = builder.addColumn(
                createColumn("Buying Customer", "customerGroupCol", String.class, custGroupColWidth));
            while (it.hasNext()) {
                it.next();
                builder = builder.addColumn(createColumn("", DYNAMIC_COL_PFX + (counter++), String.class, monthHalfWidth))
                    .addColumn(createColumn("", DYNAMIC_COL_PFX + (counter++), String.class, monthHalfWidth));
            }
            builder.title(cmp.horizontalList()
                .add(cmp.text("")));
        } else {
            YearMonthTuple tuple = it.next();
            String month = monthNames[tuple.month - 1];

            StyleBuilder centerWithThinBorders = stl.style()
                .setHorizontalAlignment(HorizontalAlignment.CENTER)
                .setBorder(stl.penThin());

            HorizontalListBuilder topColHdr = cmp.horizontalList()
                .add(cmp.text(String.valueOf(tuple.year)).setFixedWidth(custGroupColWidth)
                    .setStyle(centerWithThinBorders))
                .add(cmp.text(month).setFixedWidth(monthHalfWidth * 2)).setStyle(centerWithThinBorders);

            String month1stHalfLbl = "New";
            String month2ndHalfLbl = "Existing";
            builder = builder
                .addColumn(createColumn("Registered Member", "customerGroupCol", String.class, custGroupColWidth))
                .addColumn(createColumn(month1stHalfLbl, DYNAMIC_COL_PFX + (counter++), String.class, monthHalfWidth))
                .addColumn(createColumn(month2ndHalfLbl, DYNAMIC_COL_PFX + (counter++), String.class, monthHalfWidth));

            while (it.hasNext()) {
                tuple = it.next();
                month = monthNames[tuple.month - 1];
                topColHdr = topColHdr.add(cmp.text(month).setFixedWidth(monthHalfWidth * 2).setStyle(centerWithThinBorders));

                builder = builder
                    .addColumn(createColumn(month1stHalfLbl, DYNAMIC_COL_PFX + (counter++), String.class, monthHalfWidth))
                    .addColumn(createColumn(month2ndHalfLbl, DYNAMIC_COL_PFX + (counter++), String.class, monthHalfWidth));
            }
            builder.title(topColHdr);
        }

        return cmp.subreport(builder).setDataSource(createCustGroupJrDataSource(con, list, buying));
    }

    private JRDataSource createCustGroupJrDataSource(final Connection con, final List<YearMonthTuple> yearMonthTuples,
        final boolean buying) {
        return new JRDataSource() {
            int customerGroupIdx = 0;
            Map<String, Object> map = Maps.newHashMap();

            public boolean next() throws JRException {
                boolean hasNext = customerGroupIdx < customerGroups.size();
                if (hasNext) {
                    CodeDescDto customerGroup = customerGroups.get(customerGroupIdx);
                    int counter = 0;
                    map.put("customerGroupCol", customerGroup.getDesc());

                    StringBuilder newSqlString = new StringBuilder("SELECT COUNT(*) FROM CRM_MEMBER WHERE CUSTOMER_GROUP = ?");
                    newSqlString.append(" AND CREATED_DATETIME >= ? AND CREATED_DATETIME < ?");

                    StringBuilder existingSqlString = new StringBuilder("SELECT COUNT(*) FROM CRM_MEMBER WHERE CUSTOMER_GROUP = ?");
                    existingSqlString.append(" AND CREATED_DATETIME < ?");
                    for (YearMonthTuple tuple : yearMonthTuples) {
                        PreparedStatement ps = null;
                        ResultSet rs = null;
                        try {
                            if (buying) {
                                ps = con.prepareStatement(newSqlString.toString()
                                    + " AND EXISTS (SELECT 1 FROM POS_TRANSACTION WHERE CUSTOMER_ID = ACCOUNT_ID)");
                            } else {
                                ps = con.prepareStatement(newSqlString.toString());
                            }
                            ps.setString(1, customerGroup.getCode());
                            String year = String.valueOf(tuple.year);
                            String month = tuple.month < 10 ? "0" + tuple.month : String.valueOf(tuple.month);
                            Date from = YEAR_MONTH_FORMATTER.parse(year + month);
                            int nextMonth = tuple.month + 1;
                            if (nextMonth == 13) {
                                nextMonth = 1;
                                year = String.valueOf(tuple.year + 1);
                            }
                            month = nextMonth < 10 ? "0" + (nextMonth) : String.valueOf(nextMonth);
                            Date to = YEAR_MONTH_FORMATTER.parse(year + month);

                            ps.setDate(2, new java.sql.Date(from.getTime()));
                            ps.setDate(3, new java.sql.Date(to.getTime()));

                            rs = ps.executeQuery();
                            String fieldValue = null;
                            if (rs.next()) {
                                fieldValue = rs.getString(1);
                            } else {
                                fieldValue = "0";
                            }
                            map.put(DYNAMIC_COL_PFX + (counter++), fieldValue);

                            IOUtils.INSTANCE.close(rs);
                            IOUtils.INSTANCE.close(ps);

                            if (buying) {
                                ps = con.prepareStatement(existingSqlString.toString()
                                    + " AND EXISTS (SELECT 1 FROM POS_TRANSACTION WHERE CUSTOMER_ID = ACCOUNT_ID)");
                            } else {
                                ps = con.prepareStatement(existingSqlString.toString());
                            }
                            ps.setString(1, customerGroup.getCode());
                            ps.setDate(2, new java.sql.Date(from.getTime()));

                            rs = ps.executeQuery();
                            if (rs.next()) {
                                fieldValue = rs.getString(1);
                            } else {
                                fieldValue = "0";
                            }
                            map.put(DYNAMIC_COL_PFX + (counter++), fieldValue);
                        } catch (Exception e) {
                            throw new JRException(e);
                        } finally {
                            IOUtils.INSTANCE.close(rs);
                            IOUtils.INSTANCE.close(ps);
                        }
                    }
                }
                customerGroupIdx++;
                return hasNext;
            }

            @Override
            public Object getFieldValue(JRField jrField) throws JRException {
                return map.get(jrField.getName());
            }
        };
    }

    private class ConnectionHolder {
        Connection con;
    }

    private class YearMonthTuple {
        int year;
        int month;
    }

}
