package com.transretail.crm.core.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.google.common.collect.Lists;
import com.transretail.crm.common.service.dto.request.PageSortDto;
import com.transretail.crm.common.service.dto.response.ResultList;
import com.transretail.crm.common.service.util.SpringDataPagingUtil;
import com.transretail.crm.core.dto.ApprovalMatrixDto;
import com.transretail.crm.core.entity.ApprovalMatrixModel;
import com.transretail.crm.core.entity.QApprovalMatrixModel;
import com.transretail.crm.core.entity.RewardSchemeModel;
import com.transretail.crm.core.entity.enums.ModelType;
import com.transretail.crm.core.repo.ApprovalMatrixRepo;
import com.transretail.crm.core.security.model.impl.CustomSecurityUserDetailsImpl;
import com.transretail.crm.core.service.HrOverridePermissionsService;

@Service("hrOverridePermissionsService")
@Transactional
public class HrOverridePermissionsServiceImpl implements
		HrOverridePermissionsService {
	

	@Autowired
	ApprovalMatrixRepo approvalMatrixRepo;


	@Override
	public void deleteApprover(Long id) {
		approvalMatrixRepo.delete(id);
	}
	
	@Override
	public List<ApprovalMatrixModel> findApprovers() {
		
		return approvalMatrixRepo.findByModelType(ModelType.PURCHASETXN);
	}
	
	@Override
	public boolean isApprover(CustomSecurityUserDetailsImpl currentUser) {
		
		for(ApprovalMatrixModel approver : findApprovers()) {
			if(approver.getUser().getUsername().equals(currentUser.getUsername())) {
				return true;
			}
		}
		
		return false;
	}

	@Override
    public ResultList<ApprovalMatrixDto> listApprovers(PageSortDto dto) {
		QApprovalMatrixModel qModel = QApprovalMatrixModel.approvalMatrixModel;
    	Pageable pageable = SpringDataPagingUtil.INSTANCE.createPageable(dto.getPagination());
        Page<ApprovalMatrixModel> page=  approvalMatrixRepo.findAll(qModel.modelType.eq(ModelType.PURCHASETXN), pageable);
        return new ResultList<ApprovalMatrixDto>(convertToListDto(page.getContent()), page.getTotalElements(), page.hasPreviousPage(), page.hasNextPage());
    }
	
	private List<ApprovalMatrixDto> convertToListDto(List<ApprovalMatrixModel> models) {
		List<ApprovalMatrixDto> dtos = Lists.newArrayList();
		for(ApprovalMatrixModel model : models) {
			dtos.add(new ApprovalMatrixDto(model));
		}
		return dtos;
	}
}
