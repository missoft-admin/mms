<%@include file="../common/taglibs.jsp" %>
<%@include file="../common/messages.jsp" %>

<div class="page-header page-header2">
<h1><spring:message code="menutab_giftcard_list" /></h1>
</div>

<div id="gc_panel">
  <form id="gc_form" class="form-horizontal row-fluid well">
    <div class="span6">
      <div class="control-group">
        <label class="control-label" for="q_name"><spring:message code="gc.product.label.name"/></label>

        <div class="controls">
          <input type="text" id="q_name" name="name"/>
        </div>
      </div>
      <div class="control-group">
        <label class="control-label" for="q_currency"><spring:message code="gc.product.label.currency"/></label>

        <div class="controls">
          <input type="text" id="q_currency" name="currency"/>
        </div>
      </div>
      <div class="control-group">
        <label class="control-label" for="q_faceValue"><spring:message code="gc.product.label.faceValue"/></label>

        <div class="controls">
          <input type="text" id="q_faceValue" name="faceValue"/>
        </div>
      </div>
      <div class="control-group">
        <label class="control-label" for="q_luhnCheck"><spring:message code="gc.product.label.luhnCheck"/></label>

        <div class="controls">
          <select id="q_luhnCheck" name="luhnCheck">
            <option value=""></option>
            <option value="yes"><spring:message code="yes"/></option>
            <option value="no"><spring:message code="no"/></option>
          </select>
        </div>
      </div>
      <div class="control-group">
        <label class="control-label" for="q_reload"><spring:message code="gc.product.label.reload"/></label>

        <div class="controls">
          <select id="q_reload" name="reload">
            <option value=""></option>
            <option value="yes"><spring:message code="yes"/></option>
            <option value="no"><spring:message code="no"/></option>
          </select>
        </div>
      </div>
      <div class="control-group">
        <label class="control-label" for="q_allowToSale"><spring:message code="gc.product.label.allowToSale"/></label>

        <div class="controls">
          <select id="q_allowToSale" name="allowToSale">
            <option value=""></option>
            <option value="yes"><spring:message code="yes"/></option>
            <option value="no"><spring:message code="no"/></option>
          </select>
        </div>
      </div>
    </div>
    <div class="span6">
      <div class="control-group">
        <label class="control-label" for="q_genCode"><spring:message code="gc.product.label.genCode"/></label>

        <div class="controls">
          <input type="text" id="q_genCode" name="genCode"/>
        </div>
      </div>
      <div class="control-group">
        <label class="control-label" for="q_country"><spring:message code="gc.product.label.country"/></label>

        <div class="controls">
          <input type="text" id="q_country" name="country"/>
        </div>
      </div>
      <div class="control-group">
        <label class="control-label" for="q_fixValue"><spring:message code="gc.product.label.fixValue"/></label>

        <div class="controls">
          <select id="q_fixValue" name="fixValue">
            <option value=""></option>
            <option value="yes"><spring:message code="yes"/></option>
            <option value="no"><spring:message code="no"/></option>
          </select>
        </div>
      </div>
      <div class="control-group">
        <label class="control-label" for="q_vendorName"><spring:message code="gc.product.label.vendor"/></label>
        <%--TODO: Create and use vendor search instead of displaying everything in dropdown--%>
        <div class="controls">
          <select id="q_vendorName" name="vendorName">
            <option value="">&nbsp;</option>
            <c:forEach items="${vendors.results}" var="vendor">
              <option value="${vendor.name}">${vendor.name}</option>
            </c:forEach>
          </select>
        </div>
      </div>
      <div class="control-group">
        <label class="control-label" for="q_redemption"><spring:message code="gc.product.label.redemption"/></label>

        <div class="controls">
          <select id="q_redemption" name="redemption">
            <option value=""></option>
            <option value="yes"><spring:message code="yes"/></option>
            <option value="no"><spring:message code="no"/></option>
          </select>
        </div>
      </div>
      <div class="control-group">
        <label class="control-label" for="q_refund"><spring:message code="gc.product.label.refund"/></label>

        <div class="controls">
          <select id="q_refund" name="refund">
            <option value=""></option>
            <option value="yes"><spring:message code="yes"/></option>
            <option value="no"><spring:message code="no"/></option>
          </select>
        </div>
      </div>
    </div>
    <div class="clearfix"></div>
    <div class="form-cta">
      <div class="control-group">
        <button id="search" type="button" class="btn btn-primary"><spring:message code="search"/></button>
        <button id="add" type="button" class="btn"><spring:message code="add"/></button>
      </div>
    </div>
  </form>
  <div id="successMessages">&nbsp;</div>
  <div id="gc_list_container">
  </div>
</div>
<div id="addEditDialog" class="modal hide" style="display: none;">



<div class="modal-body">
<form id="addUpdateGcForm" class="row-fluid">
<div class="span12 errorMessages error">&nbsp;</div>
<div class="span6">
  <div class="control-group">
    <spring:message code="gc.product.label.lpoDigits" var="lpoDigits"/>
    <label class="control-label" for="lpoDigits" title="${lpoDigits}">${lpoDigits}</label>

    <div class="controls">
      <input id="lpoDigits" name="lpoDigits" title="${lpoDigits}" type="text" value="" class="intInput">
    </div>
  </div>
  <div class="control-group">
    <spring:message code="gc.product.label.randomNoDigits" var="randomNoDigits"/>
    <label class="control-label" for="randomNoDigits" title="${randomNoDigits}">${randomNoDigits}<span
            class="required">*</span></label>

    <div class="controls">
      <input id="randomNoDigits" name="randomNoDigits" title="${randomNoDigits}" type="text" value="" placeholder="100 - 999">
    </div>
  </div>
  <div class="control-group">
    <spring:message code="gc.product.label.luhnCheck" var="luhnCheck"/>
    <label class="control-label" for="luhnCheck" title="${allowToSale}"></label>

    <div class="controls">
      <input id="luhnCheck" name="luhnCheck" title="${luhnCheck}" type="checkbox" class="manual">
    </div>
  </div>
  <div class="control-group">
    <spring:message code="gc.product.label.graceDays" var="graceDays"/>
    <label class="control-label" for="graceDays" title="${graceDays}">${graceDays}</label>

    <div class="controls">
      <input id="graceDays" name="graceDays" title="${graceDays}" type="text" value="" class="intValue">
    </div>
  </div>
  <div class="control-group">
    <spring:message code="gc.product.label.allowToSale" var="allowToSale"/>
    <label class="control-label" for="allowToSale" title="${allowToSale}"></label>

    <div class="controls">
      <input id="allowToSale" name="allowToSale" title="${allowToSale}" type="checkbox" class="manual">
    </div>
  </div>
  <div class="control-group">
    <spring:message code="gc.product.label.redemption" var="redemption"/>
    <label class="control-label" for="redemption" title="${redemption}">${redemption}</label>

    <div class="controls">
      <input id="redemption" name="redemption" title="${redemption}" type="checkbox" class="manual">
    </div>
  </div>
  <div class="control-group">
    <spring:message code="gc.product.label.partialRedeem" var="partialRedeem"/>
    <label class="control-label" for="partialRedeem" title="${partialRedeem}">${partialRedeem}</label>

    <div class="controls">
      <input id="partialRedeem" name="partialRedeem" title="${partialRedeem}" type="checkbox" class="manual">
    </div>
  </div>
  <div class="control-group">
    <spring:message code="gc.product.label.reload" var="reload"/>
    <label class="control-label" for="reload" title="${reload}">${reload}</label>

    <div class="controls">
      <input id="reload" name="redemption" title="${reload}" type="checkbox" class="manual">
    </div>
  </div>


  <div class="control-group">
    <spring:message code="gc.product.label.maxAmount" var="maxAmount"/>
    <label class="control-label" for="maxAmount" title="${maxAmount}">${maxAmount}</label>

    <div class="controls">
      <input id="maxAmount" name="maxAmount" title="${maxAmount}" type="text" value="" class="floatInput">
    </div>
  </div>
  <div class="control-group">
    <spring:message code="gc.product.label.maxReloadCount" var="maxReloadCount"/>
    <label class="control-label" for="maxReloadCount" title="${maxReloadCount}">${maxReloadCount}</label>

    <div class="controls">
      <input id="maxReloadCount" name="maxReloadCount" title="${maxReloadCount}" type="text" value="" class="intInput">
    </div>
  </div>
  <div class="control-group">
    <spring:message code="gc.product.label.refund" var="refund"/>
    <label class="control-label" for="refund" title="${refund}">${refund}</label>

    <div class="controls">
      <input id="refund" name="refund" title="${refund}" type="checkbox" class="manual">
    </div>
  </div>
  <div class="control-group">
    <spring:message code="gc.product.label.effectivityMonths" var="effectivityMonths"/>
    <label class="control-label" for="effectivityMonths" title="${effectivityMonths}">${effectivityMonths}</label>

    <div class="controls">
      <input id="effectivityMonths" name="effectivityMonths" title="${effectivityMonths}" type="text" value="" class="intInput">
    </div>
  </div>
  <div class="control-group">
    <spring:message code="gc.product.label.bankTrustMonths" var="bankTrustMonths"/>
    <label class="control-label" for="bankTrustMonths" title="${bankTrustMonths}">${bankTrustMonths}</label>

    <div class="controls">
      <input id="bankTrustMonths" name="bankTrustMonths" title="${bankTrustMonths}" type="text" value="" class="intInput">
    </div>
  </div>
  <div class="control-group">
    <spring:message code="gc.product.label.yellowAlertSafetyPercent" var="yellowAlertSafetyPercent"/>
    <label class="control-label" for="yellowAlertSafetyPercent"
           title="${yellowAlertSafetyPercent}">${yellowAlertSafetyPercent}</label>

    <div class="controls">
      <input id="yellowAlertSafetyPercent" name="yellowAlertSafetyPercent" title="${yellowAlertSafetyPercent}" type="text" value="">
    </div>
  </div>
  <div class="control-group">
    <spring:message code="gc.product.label.redAlertSafetyPercent" var="redAlertSafetyPercent"/>
    <label class="control-label" for="redAlertSafetyPercent" title="${redAlertSafetyPercent}">${redAlertSafetyPercent}</label>

    <div class="controls">
      <input id="redAlertSafetyPercent" name="redAlertSafetyPercent" title="${redAlertSafetyPercent}" type="text" value="">
    </div>
  </div>
</div>
<div class="span6">
  <div class="control-group">
    <spring:message code="gc.product.label.seqNo" var="seqNo"/>
    <label class="control-label" for="seqNo" title="${seqNo}">${seqNo}</label>

    <div class="controls">
      <input id="seqNo" name="seqNo" title="${seqNo}" type="text" value="">
    </div>
  </div>
  <div class="control-group">
    <spring:message code="gc.product.label.type" var="type"/>
    <label class="control-label" for="type" title="${type}">${type}</label>

    <div class="controls">
      <input id="type" name="type" title="${type}" type="text" value="">
    </div>
  </div>
  <div class="control-group">
    <spring:message code="gc.product.label.name" var="name"/>
    <label class="control-label" for="name" title="${name}">${name}<span class="required">*</span></label>

    <div class="controls">
      <input id="name" name="name" title="${name}" type="text" value="">
    </div>
  </div>
  <div class="control-group">
    <spring:message code="gc.product.label.genCode" var="genCode"/>
    <label class="control-label" for="genCode" title="${genCode}">${genCode}</label>

    <div class="controls">
      <input id="genCode" name="genCode" title="${genCode}" type="text" value="">
    </div>
  </div>
  <div class="control-group">
    <spring:message code="gc.product.label.country" var="country"/>
    <label class="control-label" for="country" title="${country}">${country}</label>

    <div class="controls">
      <input id="country" name="country" title="${country}" type="text" value="">
    </div>
  </div>
  <div class="control-group">
    <spring:message code="gc.product.label.currency" var="currency"/>
    <label class="control-label" for="currency" title="${currency}">${currency}</label>

    <div class="controls">
      <input id="currency" name="currency" title="${currency}" type="text" value="">
    </div>
  </div>
  <div class="control-group">
    <spring:message code="gc.product.label.cardBarePrice" var="cardBarePrice"/>
    <label class="control-label" for="cardBarePrice" title="${cardBarePrice}">${cardBarePrice}</label>

    <div class="controls">
      <input id="cardBarePrice" name="cardBarePrice" title="${cardBarePrice}" type="text" value="" class="floatInput">
    </div>
  </div>
  <div class="control-group">
    <spring:message code="gc.product.label.fixValue" var="fixValue"/>
    <label class="control-label" for="fixValue" title="${fixValue}">${fixValue}</label>

    <div class="controls">
      <input id="fixValue" name="fixValue" title="${fixValue}" type="checkbox" class="manual">
    </div>
  </div>
  <div class="control-group">
    <spring:message code="gc.product.label.faceValue" var="faceValue"/>
    <label class="control-label" for="faceValue" title="${faceValue}">${faceValue}<span class="required">*</span></label>

    <div class="controls">
      <input id="faceValue" name="faceValue" title="${faceValue}" type="text" value="" placeholder="###.##" class="floatValue">
    </div>
  </div>
  <div class="control-group">
    <spring:message code="gc.product.label.includeLiner" var="includeLiner"/>
    <label class="control-label" for="includeLiner" title="${includeLiner}">${includeLiner}</label>

    <div class="controls">
      <input id="includeLiner" name="includeLiner" title="${includeLiner}" type="checkbox" class="manual">
    </div>
  </div>
  <div class="control-group">
    <spring:message code="gc.product.label.vendor" var="vendor"/>
    <label class="control-label" for="vendor" title="${vendor}">${vendor}<span class="required">*</span></label>

    <div class="controls">
      <%--TODO: Create and use vendor search instead of displaying everything in dropdown--%>
      <select id="vendor" name="vendor">
        <c:forEach items="${vendors.results}" var="vvendor">
          <option value="${vvendor.id}">${vvendor.name}</option>
        </c:forEach>
      </select>
    </div>
    <input id="vendorName" name="vendorName" type="hidden"/>
  </div>

  <div class="control-group">
    <spring:message code="gc.product.label.cardFee" var="cardFee"/>
    <label class="control-label" for="cardFee" title="${cardFee}">${cardFee}</label>

    <div class="controls">
      <input id="cardFee" name="cardFee" title="${cardFee}" type="text" value="" class="floatInput">
    </div>
  </div>
  <div class="control-group">
    <spring:message code="gc.product.label.cardNoLength" var="cardNoLength"/>
    <label class="control-label" for="cardNoLength" title="${cardNoLength}">${cardNoLength}</label>

    <div class="controls">
      <input id="cardNoLength" name="cardNoLength" title="${cardNoLength}" type="text" value="" class="intValue">
    </div>
  </div>
</div>
<%--<div class="control-group">--%>
<%--<spring:message code="gc.product.label.imageLocation" var="imageLocation"/>--%>
<%--<label for="imageLocation" title="${imageLocation}">${imageLocation}</label>--%>
<%--<input id="imageLocation" name="imageLocation" title="${imageLocation}" type="text" value="TO BE IMPLEMENTED"--%>
<%--readonly="readonly">--%>
<%--</div>--%>
</form>
<input id="gcId" type="hidden" value=""/>
</div>
<div class="modal-footer">
  <button id="save" class="btn" aria-hidden="true"><spring:message code="save"/></button>
  <button id="cancel" class="btn" aria-hidden="true"><spring:message code="cancel"/></button>
</div>
</div>
<style>
  #gc_list_container {
    overflow-y: hidden;
    overflow-x: scroll;
    height: auto;
  }

  #addEditDialog label {
    width: 130px;
    padding: 0;
    text-transform: none;
  }
  .table { float: left;}
</style>
<link href="<spring:url value="/css/dataTables.css" />" rel="stylesheet">
<script src="<spring:url value="/js/jquery/jquery.dataTables.js" />"></script>
<script src="<spring:url value="/js/jquery/jquery.dataTables.linkLengthMenu.js" />"></script>
<script src="<spring:url value="/js/jquery/jquery.dataTables.customPaging.js" />"></script>
<script src="<spring:url value="/js/jquery/ajax-datatable.js" />"></script>
<script src="<spring:url value="/js/common/form2js.js" />"></script>
<script src="<spring:url value="/js/common/json2.js" />"></script>
<script src="<spring:url value="/js/jquery/jquery.toObject.js" />"></script>


<script src="<spring:url value="/js/jquery/jquery.numberInput.js" />"></script>
<script>
  $( document ).ready( function () {
    $( '#randomNoDigits' ).numberInput( { 'maxChar' : 3 } );

    $(".floatInput").numberInput({
        "type" : "float"
    });
	$(".intInput").numberInput({
        "type" : "int"
    });

    if ( jQuery().ajaxDataTable ) {
      var gcList = $( '#gc_list_container' );
      $( '#search' ).click( function () {
        var formDataObject = $( '#gc_form' ).toObject( {mode : 'first', skipEmpty : false} );
        var $btnSearch = $( this );
        $btnSearch.prop( 'disabled', true );
        gcList.ajaxDataTable( 'search', formDataObject, function () {
          $btnSearch.prop( 'disabled', false );
        } );
      } );

      var rowIdPrefix = 'product-';

      var clickableCell = function ( data, type, row ) {
        return '<a class="clickable" href="#" onclick="return false;"><div>' + ( data == null || data === '' ? '&nbsp;' : data) + '</div></a>';
      };
      gcList.ajaxDataTable( {
        'autoload' : true,
        'ajaxSource' : '<spring:url value="/giftcard/list" />',
        'fnRowCallback' : function ( nRow, aData, iDisplayIndex ) {
          nRow.id = rowIdPrefix + aData.id;
          return nRow;
        },
        'columnHeaders' : [
          '<spring:message code="gc.product.label.seqNo" />',
          '<spring:message code="gc.product.label.type" />',
          '<spring:message code="gc.product.label.name" />',
          '<spring:message code="gc.product.label.genCode" />',
          '<spring:message code="gc.product.label.country" />',
          '<spring:message code="gc.product.label.currency" />',
          '<spring:message code="gc.product.label.cardBarePrice" />',
          '<spring:message code="gc.product.label.fixValue" />',
          '<spring:message code="gc.product.label.faceValue" />',
          '<spring:message code="gc.product.label.includeLiner" />',
          '<spring:message code="gc.product.label.vendor" />',
          '<spring:message code="gc.product.label.cardFee" />',
          '<spring:message code="gc.product.label.cardNoLength" />',
          '<spring:message code="gc.product.label.lpoDigits" />',
          '<spring:message code="gc.product.label.randomNoDigits" />',
          '<spring:message code="gc.product.label.luhnCheck" />',
          '<spring:message code="gc.product.label.graceDays" />',
          '<spring:message code="gc.product.label.allowToSale" />',
          '<spring:message code="gc.product.label.redemption" />',
          '<spring:message code="gc.product.label.partialRedeem" />',
          '<spring:message code="gc.product.label.reload" />',
          '<spring:message code="gc.product.label.maxAmount" />',
          '<spring:message code="gc.product.label.maxReloadCount" />',
          '<spring:message code="gc.product.label.refund" />',
          '<spring:message code="gc.product.label.effectivityMonths" />',
          '<spring:message code="gc.product.label.bankTrustMonths" />',
          '<spring:message code="gc.product.label.yellowAlertSafetyPercent" />',
          '<spring:message code="gc.product.label.redAlertSafetyPercent" />',
          <%--'<spring:message code="gc.product.label.imageLocation" />'--%>
        ],
        'modelFields' : [
          {name : 'seqNo', sortable : true, fieldCellRenderer : clickableCell},
          {name : 'type', sortable : false, fieldCellRenderer : clickableCell},
          {name : 'name', sortable : false, fieldCellRenderer : clickableCell},
          {name : 'genCode', sortable : false, fieldCellRenderer : clickableCell},
          {name : 'country', sortable : false, fieldCellRenderer : clickableCell},
          {name : 'currency', sortable : false, fieldCellRenderer : clickableCell},
          {name : 'cardBarePrice', sortable : false, fieldCellRenderer : clickableCell},
          {name : 'fixValue', sortable : false, fieldCellRenderer : clickableCell},
          {name : 'faceValue', sortable : false, fieldCellRenderer : clickableCell},
          {name : 'includeLiner', sortable : false, fieldCellRenderer : clickableCell},
          {name : 'vendorName', sortable : false, fieldCellRenderer : clickableCell},
          {name : 'cardFee', sortable : false, fieldCellRenderer : clickableCell},
          {name : 'cardNoLength', sortable : false, fieldCellRenderer : clickableCell},
          {name : 'lpoDigits', sortable : false, fieldCellRenderer : clickableCell},
          {name : 'randomNoDigits', sortable : false, fieldCellRenderer : clickableCell},
          {name : 'luhnCheck', sortable : false, fieldCellRenderer : clickableCell},
          {name : 'graceDays', sortable : false, fieldCellRenderer : clickableCell},
          {name : 'allowToSale', sortable : false, fieldCellRenderer : clickableCell},
          {name : 'redemption', sortable : false, fieldCellRenderer : clickableCell},
          {name : 'partialRedeem', sortable : false, fieldCellRenderer : clickableCell},
          {name : 'reload', sortable : false, fieldCellRenderer : clickableCell},
          {name : 'maxAmount', sortable : false, fieldCellRenderer : clickableCell},
          {name : 'maxReloadCount', sortable : false, fieldCellRenderer : clickableCell},
          {name : 'refund', sortable : false, fieldCellRenderer : clickableCell},
          {name : 'effectivityMonths', sortable : false, fieldCellRenderer : clickableCell},
          {name : 'bankTrustMonths', sortable : false, fieldCellRenderer : clickableCell},
          {name : 'yellowAlertSafetyPercent', sortable : false, fieldCellRenderer : clickableCell},
          {name : 'redAlertSafetyPercent', sortable : false, fieldCellRenderer : clickableCell}
//        {name : 'imageLocation', sortable : false, fieldCellRenderer : function ( data, type, row ) {
//          var htmlString = '<input type="hidden" value="' + data + '" />';
//          htmlString += '<button class="viewImage">View Image</button>';
//          return htmlString;
//        }}
        ]
      } );

      $( 'select#vendor' ).change( function () {
        $( this ).siblings( '#vendorName' ).val( $( this ).find( "option:selected" ).text() );
      } );
      $( 'select#vendor' ).change();

      var addUpdateGcForm = $( '#addUpdateGcForm' );
      var $dialog = $( '#addEditDialog' ).modal( { show : false } ) {
        addUpdateGcForm.formUtil( 'clearInputs' );
        $(".alert", addUpdateGcForm).alert('close')
      } );

      // Update
      $( gcList ).on( 'click', 'a.clickable', function () {
        var trId = $( this ).parents( 'tr' ).attr( 'id' );
        var gcId = trId.substring( rowIdPrefix.length );
        $.ajax( {
          "type" : "GET",
          "url" : '<spring:url value="/giftcard/"/>' + gcId,
          "success" : function ( data ) {
            for ( var propertyName in data ) {
              var input = $( '#' + propertyName, $dialog );
              // Read controls whose id is equal to property name of this data object
              // Ignore if no control found
              if ( input.length > 0 ) {
                var value = data[propertyName];
                if ( input.attr( 'type' ) == 'checkbox' ) {
                  input.prop( 'checked', value );
                } else {
                  input.val( value );
                }
              }
            }
            $( '#gcId' ).val( gcId );
            $dialog.modal( 'toggle' );
          },
          "error" : function ( xhr, textStatus, error ) {
            if ( textStatus === 'timeout' ) {
              alert( 'The server took too long to send the data.' );
            }
            else {
              alert( 'An error occurred on the server. Please try again.' );
            }
          }
        } );
      } );

      // Add
      $( '#add' ).click( function () {
        $( '#gcId' ).val( '' );
        $dialog.modal( 'toggle' );
      } );

      $( '#cancel' ).click( function () {
        $dialog.modal( 'toggle' );
      } );

      var showErrorMessage = function(message) {
        $('div.errorMessages' ).html('<div class="alert alert-error">' +
                '<button type="button" class="close" data-dismiss="alert">&times;</button>' +
                message +
                '</div>');
      };

      var showSuccessMessage = function(message) {
        $( '#successMessages' ).html('<div class="alert alert-success">' +
                '<button type="button" class="close" data-dismiss="alert">&times;</button>' +
                message +
                '</div>');
      };

      $( '#save' ).click( function () {
        var ajaxUrl = '';
        var selectedGcId = $( '#gcId' ).val();
        if ( selectedGcId ) {
          ajaxUrl = '<spring:url value="/giftcard/update/"/>' + selectedGcId;
        } else {
          // Add
          ajaxUrl = '<spring:url value="/giftcard/add"/>';
        }
        $.ajax( {
          "contentType" : 'application/json',
          headers : { 'Content-Type' : 'application/json' },
          "type" : "POST",
          "url" : ajaxUrl,
          "data" : JSON.stringify( addUpdateGcForm.toObject( {mode : 'first', skipEmpty : false} ) ),
          "success" : function ( data ) {
            if ( data.hasOwnProperty( 'errorMessages' ) ) {
              var errorInfo = "";
              for ( var i = 0; i < data.errorMessages.length; i++ ) {
                errorInfo += "<br>" + (i + 1) + ". " + data.errorMessages[i];
              }
              showErrorMessage("Please correct following errors: " + errorInfo);
            } else {
              showSuccessMessage(data.successMessages);
              $dialog.modal( 'toggle' );
              // Reload the list
              $( '#search' ).click();
            }
          },
          "error" : function ( xhr, textStatus, error ) {
            if ( textStatus === 'timeout' ) {
              showErrorMessage( 'The server took too long to send the data.' );
            } else if ( error ) {
              showErrorMessage(error);
            } else {
              showErrorMessage( 'An error occurred on the server. Please try again.' );
            }
          }
        } );
      } );
    } else {
      $( '#gc_panel' ).html( '<spring:message code="gc.product.load.error" />' );
    }
  } );
</script>