package com.transretail.crm.impl;

import org.junit.Before;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.transretail.crm.core.repo.StoreRepo;
import com.transretail.crm.giftcard.entity.StockRequest;
import com.transretail.crm.giftcard.repo.StockRequestRepo;
import com.transretail.crm.giftcard.service.StockRequestService;
import org.junit.Test;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath*:/META-INF/spring/applicationContext*.xml",
    "classpath:META-INF/spring/applicationContext-test.xml"})
public class StockRequestServiceTest {

    @Autowired
    private StockRequestService stockRequestService;
    @Autowired
    private StockRequestRepo stockRequestRepo;
    @Autowired
    private StoreRepo storeRepo;
    
    @Before
    public void setUp() {
	StockRequest request = new StockRequest();
    }
    
    @Test
    public void sampleEmptyTestMethod() {
	// TODO: replaceMe
    }
}
