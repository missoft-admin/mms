<%@ include file="../../common/taglibs.jsp" %>

<style type="text/css">
<!--
.table tbody tr td:last-child {
    min-width: 0px;
}
-->
</style>


<div id="rewardList" class="el-block-01 row-fluid pull-center" style="margin-top: 10px">

  <table class="table table-striped">
    <thead>
      <tr>
        <c:if test="${rewardItemPoint}">
	        <th><spring:message code="reward_prop_minqty"/></th>
	        <th><spring:message code="reward_prop_maxqty"/></th>
          <th><spring:message code="reward_prop_baseqty"/></th>
          <th><spring:message code="reward_prop_point"/></th>
        </c:if>

        <c:if test="${rewardPoints}">
	        <th><spring:message code="reward_prop_minamount"/></th>
	        <th><spring:message code="reward_prop_maxamount"/></th>
          <th><spring:message code="reward_prop_baseamount"/></th>
          <th class="content_rewardPoints"><spring:message code="reward_prop_basefactor"/></th>
          <th class="content_rewardPoints"></th>
        </c:if>

        <c:if test="${rewardDiscount}">
	        <th><spring:message code="reward_prop_minamount"/></th>
	        <th><spring:message code="reward_prop_maxamount"/></th>
          <th class="content_rewardDiscount"><spring:message code="reward_prop_discount"/></th>
        </c:if>

        <c:if test="${rewardFreebie}">
          <th><spring:message code="reward_prop_minqty"/></th>
          <th><spring:message code="reward_prop_maxqty"/></th>
	        <th><spring:message code="reward_prop_minamount"/></th>
	        <th><spring:message code="reward_prop_maxamount"/></th>
          <th class="content_rewardFreebie"><spring:message code="reward_prop_maxbonus"/></th>
        </c:if>

        <c:if test="${rewardRedeemItem}">
          <th><spring:message code="promo_redemption_prop_sku"/></th>
          <th><spring:message code="promo_redemption_prop_prdname"/></th>
          <th><spring:message code="promo_redemption_prop_points"/></th>
        </c:if>
        
        <!-- STAMP -->
        <c:if test="${rewardSelType eq 'stampReward'}">
          <th><spring:message code="reward_prop_minamount"/></th>
	        <th><spring:message code="reward_prop_maxamount"/></th>
          <th><spring:message code="reward_prop_baseamount"/></th>
          <th><spring:message code="stamp_promo_earn_stamps" /></th>
          <th></th>
        </c:if>
        
        <c:if test="${rewardSelType eq 'stampRedemption'}">
          <th><spring:message code="promo_redemption_prop_sku"/></th>
          <th><spring:message code="promo_redemption_prop_prdname"/></th>
          <th><spring:message code="stamp_promo_redeem_stamps"/></th>
          <th><spring:message code="stamp_promo_redeem_from"/></th>
          <th><spring:message code="stamp_promo_redeem_to"/></th>
        </c:if>
        <!-- END OF STAMP -->

        <th></th>
      </tr>
    </thead>

    <tbody id="promotionRewardsEntries">
      <c:if test="${rewardSelType eq 'stampReward'}">
      <c:forEach varStatus="varRewardIdx" begin="0" var="reward" items="${promotion.promotionRewards}" >
        <c:set var="rewardIdx" value="${varRewardIdx.index}" scope="request" />
	      <c:if test="${null != rewardIdx}">
	        <jsp:include page="rewardForm.jsp" />
	      </c:if>
      </c:forEach>
      </c:if>
      <c:if test="${rewardSelType eq 'stampRedemption'}">
      <c:forEach varStatus="varRedemptionIdx" begin="0" var="redemptions" items="${promotion.promotionRedemptions}" >
        <c:set var="rewardIdx" value="${varRedemptionIdx.index}" scope="request" />
        <c:if test="${null != rewardIdx}">
          <jsp:include page="rewardForm.jsp" />
        </c:if>
      </c:forEach>
      </c:if>
      <c:if test="${rewardSelType != 'stampReward' && rewardSelType != 'stampRedemption'}">
      <c:forEach varStatus="varRewardIdx" begin="0" var="reward" items="${promotion.promotionRewards}" >
        <c:set var="rewardIdx" value="${varRewardIdx.index}" scope="request" />
        <c:if test="${null != rewardIdx}">
          <jsp:include page="rewardForm.jsp" />
        </c:if>
      </c:forEach>
      
      </c:if>
      
      <c:if test="${empty promotion.promotionRewards}"><jsp:include page="rewardForm.jsp" /></c:if>
    </tbody>
  </table>


<script src='<c:url value="/js/viewspecific/promotion/reward/rewardList.js"/>' type='text/javascript'><![CDATA[&nbsp; ]]></script>
  

</div>




