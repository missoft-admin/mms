package com.transretail.crm.common.reporting.jasper;

import java.util.HashMap;
import java.util.Map;

import org.springframework.util.Assert;

import net.sf.jasperreports.engine.JRExporterParameter;
import net.sf.jasperreports.engine.export.JRExportProgressMonitor;

/**
 * @author Allan G. Ramirez (agramirez@exist.com)
 */
public abstract class AbstractJRProcessor implements JRProcessor {
    protected Map<String, Object> parameters = new HashMap<String, Object>();
    protected Map<JRExporterParameter, Object> jRExporterParameters = new HashMap<JRExporterParameter, Object>();
    private String reportFileName;
    private JRExportProgressMonitor afterPageExportListener;

    public AbstractJRProcessor(String reportFileName) {
        this.reportFileName = reportFileName;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getReportFileName() {
        return reportFileName;
    }

    /**
     * Parameters passed ($P{param}) when jasper report is generated.
     *
     * @param parameters report parameters
     */
    public void setParameters(Map<String, Object> parameters) {
        Assert.notNull(parameters);
        this.parameters = parameters;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void addParameter(String key, Object value) {
        parameters.put(key, value);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void addParameters(Map<String, Object> parameters) {
        this.parameters.putAll(parameters);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean hasParameter(String name) {
        return this.parameters.containsKey(name);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Map<JRExporterParameter, Object> getJRExporterParameters() {
        return jRExporterParameters;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void setAfterPageExportListener(JRExportProgressMonitor afterPageExportListener) {
        this.afterPageExportListener = afterPageExportListener;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public JRExportProgressMonitor getAfterPageExportListener() {
        return afterPageExportListener;
    }

}
