package com.transretail.crm.web.controller.admin;

import java.io.IOException;
import java.net.ConnectException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.client.ClientHttpRequest;
import org.springframework.http.client.ClientHttpResponse;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.client.RequestCallback;
import org.springframework.web.client.ResponseExtractor;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.servlet.ModelAndView;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.collect.Lists;
import com.transretail.crm.core.entity.CrmInStore;
import com.transretail.crm.core.repo.CrmInStoreRepo;
import com.transretail.crm.web.controller.util.ControllerResponse;
import com.transretail.crm.web.dto.LogFilterFormDto;

/**
 * @author Allan G. Ramirez (agramirez@exist.com)
 */
@Controller
@RequestMapping("/log")
public class LogController {
    protected static final String FORM_ATTR_NAME = "form";
    protected static final String KEY_ERROR = "errorMessages";

    @Autowired
    private MessageSource messageSource;

    @Autowired
    private CrmInStoreRepo storeInfoRepo;

    @Autowired
    private RestTemplate restTemplate;

    @Autowired
    private ObjectMapper objectMapper;

    @RequestMapping(value = "/view", method = RequestMethod.GET)
    public ModelAndView showViewLogPage() {
        return new ModelAndView("logs/view");
    }

    @RequestMapping(value = "/search", method = RequestMethod.POST)
    @ResponseBody
    public ControllerResponse searchLogs(@Valid @RequestBody final LogFilterFormDto dto) {
        ControllerResponse response = new ControllerResponse();
        response.setSuccess(true);
        try {
            RequestCallback callback = new RequestCallback() {
                @Override
                public void doWithRequest(ClientHttpRequest request) throws IOException {
                    request.getHeaders().setContentType(MediaType.APPLICATION_JSON);
                    objectMapper.writeValue(request.getBody(), dto);
                }
            };
            ResponseExtractor<List<String>> extractor = new ResponseExtractor<List<String>>() {
                @Override
                public List<String> extractData(ClientHttpResponse response) throws IOException {
                    List<String> result = Lists.newArrayList();
                    Scanner in = new Scanner(response.getBody());
                    while (in.hasNext()) {
                        result.add(in.nextLine());
                    }
                    return result;
                }
            };
            CrmInStore crmInStore = storeInfoRepo.findOne(dto.getCrmInStoreId());
            List<String> logs =
                restTemplate.execute(crmInStore.getCrmProxyWsUrl() + "/log/search", HttpMethod.POST, callback, extractor);
            if (logs == null || logs.isEmpty()) {
                response.setResult(
                    Arrays.asList(messageSource.getMessage("crm.proxy.log.empty", null, LocaleContextHolder.getLocale())));
            } else {
                response.setResult(logs);
            }
        } catch (RestClientException e) {
            response.setSuccess(false);
            String storeName = storeInfoRepo.findOne(dto.getCrmInStoreId()).getName();
            String message = null;
            if (e.getRootCause() instanceof ConnectException) {
                message = messageSource.getMessage("crm.proxy.connect.timeout",
                    new String[]{storeInfoRepo.findOne(dto.getCrmInStoreId()).getName()}, LocaleContextHolder.getLocale());
            } else {
                message = messageSource
                    .getMessage("crm.proxy.log.search.error", new String[]{storeName, e.getMessage()},
                        LocaleContextHolder.getLocale());
            }
            response.setResult(Arrays.asList(message));
        }
        return response;
    }

    @ExceptionHandler(MethodArgumentNotValidException.class)
    @ResponseBody
    public ControllerResponse handleMethodArgumentNotValidException(MethodArgumentNotValidException error) {
        ControllerResponse response = new ControllerResponse();
        BindingResult result = error.getBindingResult();
        List<String> errors = new ArrayList<String>();
        for (ObjectError oe : result.getAllErrors()) {
            errors.add(messageSource.getMessage(oe, LocaleContextHolder.getLocale()));
        }
        response.setResult(errors);
        return response;
    }
}
