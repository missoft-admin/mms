package com.transretail.crm.common.web.controller.support;

import javax.servlet.http.HttpServletRequest;

import org.springframework.util.StringUtils;

/**
 * @author Allan G. Ramirez (agramirez@exist.com)
 */
public enum RequestUtils {
    INSTANCE;

    /**
     * Get the current root url
     *
     * @param request current httpservletrequest
     * @return root url which is composed of scheme + host + port (if not 80) + contextpath (if not /)
     */
    public String getRootUrl(HttpServletRequest request) {
        StringBuilder url = new StringBuilder(request.getScheme());
        url.append("://").append(request.getServerName());
        int port = request.getServerPort();
        if (port != 80) {
            url.append(':').append(port);
        }
        if (StringUtils.hasText(request.getContextPath()) && !"/".equals(request.getContextPath().trim())) {
            url.append(request.getContextPath());
        }
        return url.toString();
    }

    /**
     * Determine if current request is an ajax request
     *
     * @param request current httpservletrequest
     * @return true if ajax request. Otherwise, false
     */
    public boolean isAjax(HttpServletRequest request) {
        return "XMLHttpRequest".equalsIgnoreCase(request.getHeader("X-Requested-With"));
    }
}
