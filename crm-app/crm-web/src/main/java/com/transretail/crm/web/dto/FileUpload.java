package com.transretail.crm.web.dto;

import org.springframework.web.multipart.MultipartFile;

import com.transretail.crm.template.service.Operation;

public class FileUpload {
    private MultipartFile file;
    private String memberTypeCode;
    private Operation operation;

    public MultipartFile getFile() {
        return file;
    }

    public void setFile(MultipartFile file) {
        this.file = file;
    }

    public String getMemberTypeCode() {
        return memberTypeCode;
    }

    public void setMemberTypeCode(String memberTypeCode) {
        this.memberTypeCode = memberTypeCode;
    }

    public Operation getOperation() {
        return operation;
    }

    public void setOperation(Operation operation) {
        this.operation = operation;
    }
}