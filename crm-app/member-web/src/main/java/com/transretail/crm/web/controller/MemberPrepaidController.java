package com.transretail.crm.web.controller;

import com.transretail.crm.giftcard.dto.*;
import com.transretail.crm.giftcard.service.MemberPrepaidService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;

/**
 * Controller for Member prepaid.
 *
 * @author Vincent Gerard Tan
 */
@RequestMapping("/member/prepaid")
@Controller
public class MemberPrepaidController {

    private static final Logger LOG = LoggerFactory.getLogger(MemberPrepaidController.class);

    private static final String PATH_MEMBER_PREPAID = "/member/prepaid";
    private static final String UI_ATTR_MEMBER_INFO = "memberInfo";
    private static final String UI_ATTR_PREPAID_CARD_INFO = "prepaidInfo";

    @Autowired
    private MemberPrepaidService memberPrepaidService;

    @RequestMapping(value = "/show", produces = "text/html")
    public String showPage(Model uiModel) {
        uiModel.addAttribute(UI_ATTR_MEMBER_INFO, memberPrepaidService.getMemberDetails());
        uiModel.addAttribute(UI_ATTR_PREPAID_CARD_INFO, memberPrepaidService.getMemberPrepaidDetails());
        return PATH_MEMBER_PREPAID + "/show";
    }

    @RequestMapping(value="/history/{cardNo}", produces = "text/html")
    public String showTransactionPage(@PathVariable("cardNo")String cardNo, Model uiModel) {
        LOG.info("GET for transaction history");
        uiModel.addAttribute("cardNo", cardNo);
        return PATH_MEMBER_PREPAID + "/history";
    }

    @RequestMapping(method = RequestMethod.POST, value = "/transactionHistory")
    public @ResponseBody
    PrepaidTransactionResultList retrieveTransactionHistory(@RequestBody PrepaidTransactionSearchDto searchDto) {
        LOG.info("POST for transaction history");
        PrepaidTransactionResultList result = memberPrepaidService.retrieveTransactionHistory(searchDto);
        return result;
    }

    @RequestMapping(method = RequestMethod.POST, value="/cardlist")
    public @ResponseBody
    PrepaidCardResultList retrieveCardList(@RequestBody PrepaidCardSearchDto searchDto) {
        PrepaidCardResultList result = memberPrepaidService.retrieveCards(searchDto);
        return result;
    }

    @RequestMapping(value="/link", method = RequestMethod.POST, produces = "application/json; charset=utf-8")
    @ResponseBody
    public ControllerResponse linkCard(@ModelAttribute("prepaidLinkForm") MemberGiftCardDto prepaidLinkForm,
                                       BindingResult result, HttpServletRequest request) {
        ControllerResponse response = new ControllerResponse();

        LOG.info("linkCard. dto={}", prepaidLinkForm);
        memberPrepaidService.registerCardToMember(prepaidLinkForm, result);

        if (result.hasErrors()) {
            response.setSuccess(false);
            response.setResult(result.getAllErrors());
        } else {
            response.setSuccess(true);
        }

        return response;
    }

    private class ControllerResponse {
        private boolean itsSuccess;
        private Object itsResult;

        public Boolean isSuccess() {
            return itsSuccess;
        }
        public void setSuccess(boolean inSuccess) {
            this.itsSuccess = inSuccess;
        }
        @SuppressWarnings("unused")
        public Object getResult() {
            return itsResult;
        }
        public void setResult(Object inResult) {
            this.itsResult = inResult;
        }
    }
}
