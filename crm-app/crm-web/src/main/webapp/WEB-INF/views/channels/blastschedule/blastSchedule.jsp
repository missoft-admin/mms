<%@include file="../../common/taglibs.jsp" %>


<div id="content_blastSchedule">

  <div class="page-header page-header2"><h1><spring:message code="channel_lbl_blastsched" /></h1></div>

  <div class="form-horizontal">
    <div id="" class="control-group">
      <label for="" class="control-label"><spring:message code="channel_email_blastsched" /></label>
      <div class="controls">
        <div id="emailSched" class="row-fluid">
          <%-- <fieldset class="pull-left">
            <label for=""><spring:message code="label_schedule"/></label>
            <input id="emailSchedule" type="text" class="input-medium" value="${emailSchedule}" />
          </fieldset> --%>
          <fieldset class="pull-left">
            <label for=""><spring:message code="label_cronexp"/></label>
            <input id="emailCronExp" type="text" class="input-medium" readonly="readonly" value="${emailCronExp}" />
          </fieldset>

          <input type="button" id="processBtn" data-save="<spring:message code="label_save" />" value="<spring:message code="label_edit" />" 
            class="btn btn-primary pull-left btn-small" data-url="/marketing/channel/blast/schedule/${emailType}?schedule=%SCHEDULE%" />
          <div class="clearfix"></div>
        </div>
      </div>
    </div>

  </div>


<script type="text/javascript">
$(document).ready( function() {

	  var $emailCronExp = $( "#content_blastSchedule" ).find( "#emailSched > #emailCronExp" );
	  var $emailProcessBtn = $( "#content_blastSchedule" ).find( "#emailSched > #processBtn" );

	  initFields();

	  function initFields() {
		  $emailProcessBtn.click( function(e) {
			    e.preventDefault();
			    if ( $(this).val() == $(this).data( "save" ) ) {
			    	  $.get( 
			    			  $(this).data( "url" ).replace( new RegExp( "%SCHEDULE%", "g" ), $emailCronExp.val() ),
			    			  function(resp) {
			    				    $emailCronExp.removeAttr( "readonly" );
			    			  });
			    }
			    else {
			    	  $emailCronExp.removeAttr( "readonly" );
			    }
		  });
	  }

	  
});
</script>
<style type="text/css"> 
<!-- 
	.row-fluid fieldset { margin-right: 10px; }
	.row-fluid label { font-size: 11px; margin-bottom: 0px; }
	.control-label { width: 200px !important; margin-top: 20px; }
	.btn-small { margin-top: 15px; }
--> 
</style>


</div>