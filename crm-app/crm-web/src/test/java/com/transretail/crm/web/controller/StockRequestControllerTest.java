package com.transretail.crm.web.controller;

import java.util.ArrayList;
import java.util.List;

import com.transretail.crm.common.reporting.service.ReportService;
import com.transretail.crm.core.entity.lookup.Store;
import com.transretail.crm.core.service.CodePropertiesService;
import com.transretail.crm.core.service.LookupService;
import com.transretail.crm.core.service.StoreService;
import com.transretail.crm.giftcard.dto.ProductProfileDto;
import com.transretail.crm.giftcard.dto.StockRequestDto;
import com.transretail.crm.giftcard.entity.support.StockRequestStatus;
import com.transretail.crm.giftcard.service.GiftCardInventoryService;
import com.transretail.crm.giftcard.service.ProductProfileService;
import com.transretail.crm.giftcard.service.StockRequestService;
import com.transretail.crm.report.template.impl.GiftCardTransferDocument;

import org.joda.time.LocalDate;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.AnnotationConfigWebContextLoader;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

import static org.mockito.Matchers.anyLong;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

/**
 * @author ftopico, mco
 */
@RunWith(SpringJUnit4ClassRunner.class)
@WebAppConfiguration
@ContextConfiguration(loader = AnnotationConfigWebContextLoader.class)
public class StockRequestControllerTest extends AbstractMockMvcTest {

    @Configuration
    @EnableWebMvc
    static class Config {
        @Bean
        public StockRequestController stockRequestController() {
            return new StockRequestController();
        }
        
        @Bean
        public GiftCardTransferDocument giftCardTransferDocument() {
            return new GiftCardTransferDocument();
        }
        
        @Bean
        public GiftCardInventoryService giftCardInventoryService() {
            return Mockito.mock(GiftCardInventoryService.class);
        }
        
        @Bean
        public StockRequestService stockRequestService() {
            return Mockito.mock(StockRequestService.class);
        }
        
        @Bean
        public StoreService storeService() {
            return Mockito.mock(StoreService.class);
        }
        
        @Bean
        public ProductProfileService productProfileService() {
            return Mockito.mock(ProductProfileService.class);
        }
        
        @Bean
        public CodePropertiesService codePropertiesService() {
            return Mockito.mock(CodePropertiesService.class);
        }
        
        @Bean 
        public LookupService lookupService() {
            return Mockito.mock(LookupService.class);
        }
        
        @Bean
        public ReportService reportService() {
            return Mockito.mock(ReportService.class);
        }
    }
    
    @Autowired
    GiftCardTransferDocument giftCardTransferDocument;
    @Autowired
    private StockRequestService stockRequestService;
    @Autowired
    private StoreService storeService;
    @Autowired 
    private ProductProfileService productProfileService;
    @Autowired
    private CodePropertiesService codePropertiesService;
    @Autowired
    private LookupService lookupService;
    @Autowired
    private ReportService reportService;
    
    @Override
    protected void doSetup() throws Exception {
        Mockito.reset(stockRequestService);
        Mockito.reset(storeService);
        Mockito.reset(productProfileService);
        Mockito.reset(codePropertiesService);
        Mockito.reset(lookupService);
        Mockito.reset(reportService);
    }
    
    @Test
    public void transferGiftCardStockTest() throws Exception {
        Long id = 0001L;
        mockMvc.perform(post("/stockrequest/transfer/{id}", id))
            .andExpect(status().isOk());
    }
    
    @Test
    public void shouldReprintTransferDocument() throws Exception {
	StockRequestDto stockRequestDto = new StockRequestDto();
	stockRequestDto.setAllocateTo("22010");
	stockRequestDto.setSourceLocation("INVT001");
	stockRequestDto.setRequestNo("1234");
	stockRequestDto.setId(1234L);
	stockRequestDto.setReceiveDate(new LocalDate().now());
	stockRequestDto.setCreateUser("test");
	stockRequestDto.setQuantity(1);
	stockRequestDto.setStatus(StockRequestStatus.IN_TRANSIT);
	when(stockRequestService.getStockRequest(anyLong())).thenReturn(stockRequestDto);
	Store store = new Store();
	store.setCode("INVT001");
	store.setId(1);
	store.setName("HO");
	when(storeService.getStoreByCode(anyString())).thenReturn(store);
	
	ProductProfileDto product = new ProductProfileDto();
	product.setProductCode("001");
	product.setProductDesc("test");
	when(productProfileService.findProductProfileByCode(stockRequestDto.getProductCode())).thenReturn(product);
	
	mockMvc.perform(get("/stockrequest/reprint/{id}", "1234"))
		.andExpect(status().isOk())
		.andExpect(content().contentType("application/pdf"));
	
	verify(stockRequestService).getStockRequest(anyLong());
    }
}
