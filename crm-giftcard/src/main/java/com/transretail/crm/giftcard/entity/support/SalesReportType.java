
package com.transretail.crm.giftcard.entity.support;

/**
 *
 * @author Monte Cillo Co (mco@exist.com)
 */
public enum SalesReportType {
    DETAIL,
    SUMMARY,
    REDEEM,
    OUTSTANDING_BALANCE
}
