package com.transretail.crm.core.service.impl;

import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.transretail.crm.core.entity.UserRoleModel;
import com.transretail.crm.core.repo.UserRoleRepo;
import com.transretail.crm.core.service.UserRoleModelService;
import com.transretail.crm.core.util.StringUtility;

@Transactional
@Service("userRoleModelService")
public class UserRoleModelServiceImpl implements UserRoleModelService {
    @Autowired
    UserRoleRepo userRoleRepo;

    public long countAllUserRoleModels() {
        return userRoleRepo.count();
    }

    public void deleteUserRoleModel(UserRoleModel userRoleModel) {
        userRoleRepo.delete(userRoleModel);
    }

    public UserRoleModel findUserRoleModel(String id) {
        return userRoleRepo.findOne(id);
    }

    public List<UserRoleModel> findAllUserRoleModels() {
        return userRoleRepo.findAll();
    }

    public List<UserRoleModel> findUserRoleModelEntries(int firstResult, int maxResults) {
        return userRoleRepo.findAll(new org.springframework.data.domain.PageRequest(firstResult / maxResults, maxResults)).getContent();
    }

    public void saveUserRoleModel(UserRoleModel userRoleModel) {
        userRoleRepo.save(userRoleModel);
    }

    public UserRoleModel updateUserRoleModel(UserRoleModel userRoleModel) {
        return userRoleRepo.save(userRoleModel);
    }

    @SuppressWarnings("unchecked")
    public Set<UserRoleModel> findByIdSet(String[] inIds) {
        return new HashSet<UserRoleModel>(userRoleRepo.findByIdSet(Arrays.asList(inIds)));
    }
}
