package com.transretail.crm.loyalty.repo;

import org.springframework.stereotype.Repository;

import com.transretail.crm.common.repo.CrmQueryDslPredicateExecutor;
import com.transretail.crm.loyalty.entity.LoyaltyCardInventoryHistory;
import com.transretail.crm.loyalty.repo.custom.LoyaltyCardInventoryHistoryRepoCustom;

@Repository
public interface LoyaltyCardInventoryHistoryRepo extends CrmQueryDslPredicateExecutor<LoyaltyCardInventoryHistory, Long>, LoyaltyCardInventoryHistoryRepoCustom {

}
