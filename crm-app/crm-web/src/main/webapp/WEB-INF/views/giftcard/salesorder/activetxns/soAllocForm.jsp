<%@include file="../../../common/taglibs.jsp"%>


<div id="content_soAllocForm" class="modal hide nofly modal-dialog">

  <div class="modal-content">

    <div class="modal-header">
      <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
      <h4><spring:message code="gc.so.no" />: ${salesOrder.orderNo}</h4>
    </div>

    <div class="modal-body modal-form form-horizontal">

      <div class="hide alert alert-error" id="content_error">
        <button type="button" class="close" onclick="$(this).parent().hide();">&times;</button>
        <div><form:errors path="*"/>
        </div>
      </div>

      <div class="main-block row-fluid">
        <div class="span12">
        <%@include file="sgmntSoInfo.jsp" %>
      </div>
      </div>
      <div class="main-block row-fluid">
        <div class="span12">
          <%@include file="tblSoItemAlloc.jsp" %>
        </div>
      </div>

      <c:if test="${salesOrder.status == 'APPROVED' || salesOrder.status == 'PARTIALLY_ALLOCATED'}">
      <div class="main-block row-fluid  manual-so-alloc">
        <div class="soorderalloc-block">
          <div class="control-group checkElements hide">
            <div class="controls">
              <input type="text" name="scanRangeFrom" id="scanRangeFrom" readonly="readonly" placeholder="scanRangeFrom" />
              <input type="text" name="scanRangeTo" id="scanRangeTo" readonly="readonly" placeholder="scanRangeTo" />
              <input type="text" name="scanBarcode" id="scanBarcode" readonly="readonly" placeholder="scanBarcode" />
            </div>
          </div>
          <div class="control-group checkElements alert alert-info">
            <div class="control-label">
              <label>
                <input type="radio" name="scanType" value="BYRANGE" checked="checked" class="scanType checkElement" />
                <spring:message code="gc.so.alloc.lbl.byrange" />
              </label>
              <label class="hide">
                <input type="radio" name="scanType" value="BYCARD" class="scanType checkElement" />
                <spring:message code="gc.so.alloc.lbl.pergc" />
              </label>
            </div>
            <div class="controls">
              <input type="text" name="scanValue" id="scanValue" class="input-medium" placeholder="<spring:message code="gc.so.alloc.lbl.scan" />" />
              <input type="button" id="scanBtn" class="btn" value="<spring:message code="gc.so.lbl.scan" />"
                data-url-scan-barcode="<c:url value="/giftcard/salesorder/allocate/scan/%BARCODE%" />" />
              <input type="button" id="clearBtn" class="btn" value="<spring:message code="gc.so.lbl.clear" />" />
              <hr>
              <span id="scanDisplay"></span>
              <input type="button" name="validateBtn" id="validateBtn" class="pull-right btn btn-primary"
                     value="<spring:message code="label_validate" />" data-so-id="${salesOrder.id}"
                data-url-scan-range="<c:url value="/giftcard/salesorder/allocate/scan/%BARCODESTART%/%BARCODEEND%" />"
                data-url-prealloc-barcode="<c:url value="/giftcard/salesorder/allocate/preallocate/%SOID%/%BARCODE%" />" 
                data-url-prealloc-range="<c:url value="/giftcard/salesorder/allocate/preallocate/%SOID%/%BARCODESTART%/%BARCODEEND%" />" />
            </div>
          </div>
            <div class="controls">
                <input type="button" name="autoScanBtn" id="autoScanBtn" class="pull-right btn btn-danger"
                       value="<spring:message code="label_auto_scan"/>"
                       data-url-auto-scan="<c:url value="/giftcard/salesorder/allocate/autoscan/${salesOrder.id}"/>"/>
            </div>
        </div>
      </div>
      </c:if>
    </div>


    <c:url var="url_action" value="${cxProfileAction}" />
    <form:form id="soAllocForm" name="soAlloc" modelAttribute="soAllocForm" method="POST" action="${url_action}" class="modal-form form-horizontal">

    <div class="modal-footer">
      <c:if test="${salesOrder.status == 'APPROVED' || salesOrder.status == 'PARTIALLY_ALLOCATED'}">
        <button  id="saveBtn" class="btn btn-primary" type="submit" 
          data-url="<c:url value="/giftcard/salesorder/allocate/save/${salesOrder.id}" />"><spring:message code="label_save" /></button>
        <button id="cancelAllocBtn" class="btn btn-primary" type="submit" data-disable="${salesOrder.status == 'PARTIALLY_ALLOCATED'}"
          data-url="<c:url value="/giftcard/salesorder/allocate/remove/${salesOrder.id}" />" ><spring:message code="gc.so.lbl.remove.alloc" />
        </button>
      </c:if>
      <button id="cancelBtn" class="btn" type="button" data-dismiss="modal"
          data-url-cancel-prealloc="<c:url value="/giftcard/salesorder/allocate/preallocate/cancel/${salesOrder.id}" />" 
          data-range-separator="${barcodeRangeSeparator}" ><spring:message code="label_cancel" /></button>
    </div>
    </form:form>

  </div>

<style type="text/css"> 
<!-- 
  #content_soAllocForm .alert { padding-left: 0px !important; }
  #content_soAllocForm .soorderalloc-block { margin-top: 20px; }
  #content_soAllocForm .checkElement { float: left; margin-right: 5px; }
  #content_soAllocForm .checkElements .controls { margin-left: 150px !important; }
  #content_soAllocForm .checkElements .control-label { width: 150px !important; }
  #content_soAllocForm .pull-center { padding-left: 10px; padding-right: 10px; }
  #content_soAllocForm .modal-footer { text-align: center; } 
  #content_soAllocForm .control-group .controls { margin-top: 5px; margin-left: 110px; }
  #content_soAllocForm .control-group .control-label { width: 100px; }
  #content_soAllocForm .table-col-xlarge { width: 320px; }
  #content_soAllocForm .table tbody tr td:last-child {border-left: none}
  #content_soAllocForm .table tbody tr td:first-child {min-width: 240px}
  #content_soAllocForm .table tbody tr td:nth-child(2) {min-width: 90px}
  #content_soAllocForm .table tbody tr td:nth-child(3) {min-width: 90px}
  #content_soAllocForm .table tbody tr td:nth-child(4) {min-width: 100px}  
  #content_soAllocForm .table tbody tr td:nth-child(5) {min-width: 70px}    
--> 
</style>
<script type='text/javascript'>
var SoAllocForm = null;

$(document).ready( function() {

    var errorContainer = $( "#content_soAllocForm" ).find( "#content_error" );
    var $dialog = $( "#content_soAllocForm" );
    var $form = $dialog.find( "#soAllocForm" );
    var $saveBtn = $dialog.find( "#saveBtn" );
    var $cancelAllocBtn = $dialog.find( "#cancelAllocBtn" );
    var $cancelBtn = $dialog.find( "#cancelBtn" );

    $dialog.modal({backdrop : 'static', keyboard  :false});

    initDialog();
    initFormFields();
    initScanFields();
    initSaveBtns();

    function initSaveBtns() {
    }

    function disableSaveBtns( disable ) {
        $saveBtn.prop( "disabled", disable );
        $cancelAllocBtn.prop( "disabled", disable );
        if ( $cancelAllocBtn.data( "disable" ) ) {
            $cancelAllocBtn.prop( "disabled", !$cancelAllocBtn.data( "disable" ) );
        }
    }

    function initDialog() {
        $dialog.modal({ show : true });
        $dialog.on( "hide", function(e) { if ( e.target === this ) {
            errorContainer.hide();
        }});
    }

    function initFormFields() {

        $saveBtn.click( function(e) {
            e.preventDefault();
            $.get( $(this).data( "url" ), function( resp ) {
                $dialog.modal( "hide" );
                location.reload();
            });
        });

        $cancelAllocBtn.click( function(e) {
            e.preventDefault();
            $.get( $(this).data( "url" ), function( resp ) {
                $( "div.allocations" ).html( "" );
            });
        });
    }

    function initScanFields() {
        var $scanRangeFrom = $( "input#scanRangeFrom" );
        var $scanRangeTo = $( "input#scanRangeTo" );
        var $scanValue = $( "input#scanValue" );
        var $scanBarcode = $( "input#scanBarcode" );
        var $scanType = $( "input.scanType" );
        var $scanBtn = $( "input#scanBtn" );
        var $clearBtn = $( "input#clearBtn" );
        var $validateBtn = $( "input#validateBtn" );
        var $scanDisplay = $( "span#scanDisplay" );
        var $autoScan = $( "input#autoScanBtn");

        $autoScan.click(function (e) {
            e.preventDefault();
            disableSaveBtns(true);
            $.get( $(this).data("url-auto-scan"), function (resp) {
                //console.log(resp);
                //console.log(resp.result);
                  if (resp.success) {
                      //alert(JSON.stringify(resp.result));
                      //console.log(resp.result)
                      var result = resp.result;

                      $.each(result, function (i, v) {
                          var $container = $( "#" + v.soItemId );
                         //console.log(v);
                          if ($.trim(($container.html()) !== "")){

                              $container.append( "<br/>" );
                          }

                          $container.append( v.barcodeStart
                              + " - "
                              + v.barcodeEnd
                              + " ("
                              + v.quantity
                              + ")");
                      });

                      disableSaveBtns(false);
                      errorContainer.hide( "slow" );
                      $.each(result, function (i, v) {
                          console.log(v.barcodeStart + $cancelBtn.data("range-separator") + v.barcodeEnd);
                          barcodeRange.push(v.barcodeStart + $cancelBtn.data("range-separator") + v.barcodeEnd);
                      });

                      $cancelBtn.off( "click" ).click( function(e) {

                        e.preventDefault();
                        var cancelUrl = $cancelBtn.data( "url-cancel-prealloc" );//.replace( new RegExp( "{barcodeRange}", "g" ), barcodeRange );
                        $.post( cancelUrl, {
                                'barcodeRange' : barcodeRange
                            },
                            function( resp ) {
                                alert(resp.success);
                                console.log(JSON.stringify(resp))
                            },
                            "application/json"
                        );
                    });
                  } else {
                      var errors = "";
                      if ( resp.result.length === 1 ) {
                          errors += ( 1 + ". " + resp.result );
                      } else {
                          $( resp.result ).each( function( key, value ) { errors += ( ( key + 1 ) + ". " + value + "<br>" ); });
                      }

                      errorContainer.find( "div" ).html( errors );
                      errorContainer.show( "slow" );
                  }
                }
            );
        });

        $validateBtn.prop( "disabled", true );
        disableSaveBtns( true );

        var $scanTypeVal = null;
        $scanType.change( function(e) {
            if ( this.checked ) {
                $scanTypeVal = $( this ).val();
            }
        }).change();
        $clearBtn.click( function(e) {
            e.preventDefault();
            $scanRangeFrom.val( "" );
            $scanRangeTo.val( "" );
            $scanBarcode.val( "" );
            $scanValue.val( "" );
            $scanDisplay.html( "" );
            $validateBtn.prop( "disabled", true );
        });
        $scanBtn.click( function(e) {
            e.preventDefault();
            if ( $scanValue.val() ) {
                $.get( $(this).data( "url-scan-barcode" ).replace( new RegExp( "%BARCODE%", "g" ), $scanValue.val() ),
                        function( resp ) {
                            if ( resp.success ) {
                                if ( $scanTypeVal && $scanTypeVal == "BYRANGE" ) {
                                    if ( !$scanRangeFrom.val() ) {
                                        $scanRangeFrom.val( $scanValue.val() );
                                        $scanDisplay.html( $scanValue.val() + " - ..." );
                                    }
                                    else {
                                        if ( ( $scanRangeFrom.val() - 0 ) < ( $scanValue.val() - 0 ) ) {
                                            $scanRangeTo.val( $scanValue.val() );
                                            $scanDisplay.html( $scanRangeFrom.val() + " - " + $scanValue.val() );
                                        }
                                        else {
                                            $scanRangeTo.val( $scanRangeFrom.val() );
                                            $scanRangeFrom.val( $scanValue.val() );
                                        }
                                    }
            
                                    if ( $scanRangeFrom.val() && $scanRangeTo.val() ) {
                                        $scanDisplay.html( $scanRangeFrom.val() + " - " + $scanRangeTo.val() );
                                        $validateBtn.prop( "disabled", false );
                                    }
                                }
                                else if ( $scanTypeVal && $scanTypeVal == "BYCARD" ) {
                                    $scanBarcode.val( $scanValue.val() );
                                    $scanDisplay.html( $scanValue.val() );
                                    $validateBtn.prop( "disabled", false );
                                }
                            }
                            else {
                                var errors = "";
                                if ( resp.result.length == 1 ) {
                                    errors += ( 1 + ". " + resp.result );
                                }
                                else {
                                    $( resp.result ).each( function( key, value ) { errors += ( ( key + 1 ) + ". " + value + "<br>" ); });
                                }
                                errorContainer.find( "div" ).html( errors );
                                errorContainer.show( "slow" );
                            }
                            $scanValue.val( "" );
                    }, "json" );
            }
        });
        var barcodeRange = new Array();
        $validateBtn.click( function(e) {
            e.preventDefault();
            disableSaveBtns( true );
            $(this).prop( "disabled", true );
            var url = null;
            if ( $scanTypeVal && $scanTypeVal == "BYRANGE" ) {
                url = $( this ).data( "url-prealloc-range" )
                        .replace( new RegExp( "%SOID%", "g" ), $(this).data( "so-id" ) )
                        .replace( new RegExp( "%BARCODESTART%", "g" ), $scanRangeFrom.val() )
                        .replace( new RegExp( "%BARCODEEND%", "g" ), $scanRangeTo.val() );
            }
            else if ( $scanTypeVal && $scanTypeVal == "BYCARD" ) {
                url = $( this ).data( "url-prealloc-barcode" )
                        .replace( new RegExp( "%SOID%", "g" ), $(this).data( "so-id" ) )
                        .replace( new RegExp( "%BARCODE%", "g" ), $scanBarcode.val() );
            }
            $.get( url, function( resp ) {
                if ( resp.success ) {
                    var result = resp.result;
                    var $container = $( "#" + result.soItemId );
                    if($.trim($container.html()) != "")
                      $container.append("<br/>")
                    $container.append( result.barcodeStart + " - " + result.barcodeEnd + " (" + result.quantity + ")" );
                    errorContainer.hide( "slow" );
                    disableSaveBtns( false );

                    barcodeRange.push( result.barcodeStart + $cancelBtn.data( "range-separator" ) + result.barcodeEnd );
                    $cancelBtn.unbind( "click" ).click( function(e) {
                    	e.preventDefault();
                    	var cancelUrl = $cancelBtn.data( "url-cancel-prealloc" );//.replace( new RegExp( "{barcodeRange}", "g" ), barcodeRange );
                    	$.post( cancelUrl, { 'barcodeRange' : barcodeRange }, function( resp ) { alert(resp.success); }, "application/json" );
                    });
                }
                else {
                    var errors = "";
                    if ( resp.result.length == 1 ) {
                        errors += ( 1 + ". " + resp.result );
                    }
                    else {
                        $( resp.result ).each( function( key, value ) { errors += ( ( key + 1 ) + ". " + value + "<br>" ); });
                    }
                    errorContainer.find( "div" ).html( errors );
                    errorContainer.show( "slow" );
                }
                $clearBtn.click();
                $(this).prop( "disabled", false );
            }, "json" );
        });
    }

    SoAllocForm = {
        show  : function() {
            $dialog.modal( "show" );
        }
    };
  
});
</script>


</div>