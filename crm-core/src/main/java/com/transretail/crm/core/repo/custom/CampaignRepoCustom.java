package com.transretail.crm.core.repo.custom;


import java.util.Date;
import java.util.List;

import com.transretail.crm.core.dto.PromotionSearchDto;
import com.transretail.crm.core.entity.Campaign;
import com.transretail.crm.core.entity.Program;


public interface CampaignRepoCustom {

	List<Campaign> find( Date inDate, String inStatus, Program inProgram );

}
