package com.transretail.crm.core.entity;

import java.util.Iterator;
import java.util.List;

import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Configurable;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import com.transretail.crm.core.repo.UserRepo;
import com.transretail.crm.core.service.UserService;

@Configurable
@Transactional
@ContextConfiguration(locations = {"classpath*:/META-INF/spring/applicationContext*.xml", "classpath*:ctx.xml"})
@RunWith(SpringJUnit4ClassRunner.class)
public class UserModelIntegrationTest {

    @Autowired
    UserModelDataOnDemand dod;
    @Autowired
    UserService userService;
    @Autowired
    UserRepo userRepo;

    @Test
    public void testMarkerMethod() {
    }

    @Test
    public void testCountAllUserModels() {
        Assert.assertNotNull("Data on demand for 'UserModel' failed to initialize correctly", dod.getRandomUserModel());
        long count = userService.countAllUserModels();
        Assert.assertTrue("Counter for 'UserModel' incorrectly reported there were no entries", count > 0);
    }

    @Test
    public void testFindUserModel() {
        UserModel expectedUser = dod.getRandomUserModel();
        Assert.assertNotNull("Data on demand for 'UserModel' failed to initialize correctly", expectedUser);
        Long id = expectedUser.getId();
        Assert.assertNotNull("Data on demand for 'UserModel' failed to provide an identifier", id);
        UserModel result = userService.findUserModel(id);
        Assert.assertNotNull("Find method for 'UserModel' illegally returned null for id '" + id + "'", expectedUser);
        Assert.assertEquals("Find method for 'UserModel' returned the incorrect user", expectedUser, result);
        
        result = userService.findUserModel(expectedUser.getUsername());
        Assert.assertEquals("Find method using username returned the incorrect user", expectedUser, result);
    }

    @Test
    public void testFindAllUserModels() {
        Assert.assertNotNull("Data on demand for 'UserModel' failed to initialize correctly", dod.getRandomUserModel());
        long count = userService.countAllUserModels();
        Assert.assertTrue("Too expensive to perform a find all test for 'UserModel', as there are " + count + " entries; set the findAllMaximum to exceed this value or set findAll=false on the integration test annotation to disable the test", count < 250);
        List<UserModel> result = userService.findAllUserModels();
        Assert.assertNotNull("Find all method for 'UserModel' illegally returned null", result);
        Assert.assertTrue("Find all method for 'UserModel' failed to return any data", result.size() > 0);
    }

    @Test
    public void testFindUserModelEntries() {
        Assert.assertNotNull("Data on demand for 'UserModel' failed to initialize correctly", dod.getRandomUserModel());
        long count = userService.countAllUserModels();
        if (count > 20) count = 20;
        int firstResult = 0;
        int maxResults = (int) count;
        List<UserModel> result = userService.findUserModelEntries(firstResult, maxResults);
        Assert.assertNotNull("Find entries method for 'UserModel' illegally returned null", result);
        Assert.assertEquals("Find entries method for 'UserModel' returned an incorrect number of entries", count, result.size());
    }

    @Test
    public void testFlush() {
        UserModel obj = dod.getRandomUserModel();
        Assert.assertNotNull("Data on demand for 'UserModel' failed to initialize correctly", obj);
        Long id = obj.getId();
        Assert.assertNotNull("Data on demand for 'UserModel' failed to provide an identifier", id);
        obj = userService.findUserModel(id);
        Assert.assertNotNull("Find method for 'UserModel' illegally returned null for id '" + id + "'", obj);
        boolean modified = dod.modifyUserModel(obj);
        Integer currentVersion = obj.getVersion();
        userRepo.flush();
        Assert.assertTrue("Version for 'UserModel' failed to increment on flush directive", (currentVersion != null && obj.getVersion() > currentVersion) || !modified);
    }

    @Test
    public void testUpdateUserModelUpdate() {
        UserModel obj = dod.getRandomUserModel();
        Assert.assertNotNull("Data on demand for 'UserModel' failed to initialize correctly", obj);
        Long id = obj.getId();
        Assert.assertNotNull("Data on demand for 'UserModel' failed to provide an identifier", id);
        obj = userService.findUserModel(id);
        boolean modified = dod.modifyUserModel(obj);
        Integer currentVersion = obj.getVersion();
        UserModel merged = userService.updateUserModel(obj);
        userRepo.flush();
        Assert.assertEquals("Identifier of merged object not the same as identifier of original object", merged.getId(), id);
        Assert.assertTrue("Version for 'UserModel' failed to increment on merge and flush directive", (currentVersion != null && obj.getVersion() > currentVersion) || !modified);
    }

    @Test
    public void testSaveUserModel() {
        Assert.assertNotNull("Data on demand for 'UserModel' failed to initialize correctly", dod.getRandomUserModel());
        UserModel obj = dod.getNewTransientUserModel(Integer.MAX_VALUE);
        Assert.assertNotNull("Data on demand for 'UserModel' failed to provide a new transient entity", obj);
        Assert.assertNull("Expected 'UserModel' identifier to be null", obj.getId());
        try {
            userService.saveUserModel(obj);
        } catch (final ConstraintViolationException e) {
            final StringBuilder msg = new StringBuilder();
            for (Iterator<ConstraintViolation<?>> iter = e.getConstraintViolations().iterator(); iter.hasNext(); ) {
                final ConstraintViolation<?> cv = iter.next();
                msg.append("[").append(cv.getRootBean().getClass().getName()).append(".").append(cv.getPropertyPath()).append(": ").append(cv.getMessage()).append(" (invalid value = ").append(cv.getInvalidValue()).append(")").append("]");
            }
            throw new IllegalStateException(msg.toString(), e);
        }
        userRepo.flush();
        Assert.assertNotNull("Expected 'UserModel' identifier to no longer be null", obj.getId());
    }
    
    @Test
    public void testSaveUserModelFailure() {
    	UserModel user = dod.getRandomUserModel();
    	Assert.assertNotNull("Data on demand for 'UserModel' failed to initialize correctly", user);
    	
    	try {
    		userService.saveUserModel(user);
    		Assert.fail("No exception thrown when saving a duplicate");
    	}
    	catch(Exception e) {
    		Assert.assertTrue("Wrong exception thrown when saving a duplicate", e instanceof DuplicateKeyException);
    	}
    }

    @Test
    public void testDeleteUserModel() {
        UserModel obj = dod.getRandomUserModel();
        Assert.assertNotNull("Data on demand for 'UserModel' failed to initialize correctly", obj);
        Long id = obj.getId();
        Assert.assertNotNull("Data on demand for 'UserModel' failed to provide an identifier", id);
        obj = userService.findUserModel(id);
        userService.deleteUserModel(obj);
        userRepo.flush();
        Assert.assertNull("Failed to remove 'UserModel' with identifier '" + id + "'", userService.findUserModel(id));
    }
}
