package com.transretail.crm.core.repo;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.transretail.crm.common.repo.CrmQueryDslPredicateExecutor;
import com.transretail.crm.core.entity.lookup.LookupDetail;
import com.transretail.crm.core.entity.lookup.LookupHeader;
import com.transretail.crm.core.repo.custom.LookupDetailRepoCustom;

@Repository
public interface LookupDetailRepo extends CrmQueryDslPredicateExecutor<LookupDetail, String>, LookupDetailRepoCustom {

    List<LookupDetail> findByHeader(LookupHeader header);

    LookupDetail findByCode(String code);

}