/*
 * Copyright (c) 2012.
 * All rights reserved.
 */

package com.transretail.crm.core.security.util;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;

import com.transretail.crm.core.security.model.impl.CustomSecurityUserDetailsImpl;

/**
 * @author rnodalo
 */
public final class UserUtil {

    private static final Logger _LOG = LoggerFactory.getLogger(UserUtil.class);

    private UserUtil() {
    }

    public static CustomSecurityUserDetailsImpl getCurrentUser() {
        SecurityContext ctx = SecurityContextHolder.getContext();

        CustomSecurityUserDetailsImpl user = null;
        if (ctx != null && ctx.getAuthentication() != null) {
            if (ctx.getAuthentication().getPrincipal() instanceof CustomSecurityUserDetailsImpl) {
                user = (CustomSecurityUserDetailsImpl) ctx.getAuthentication().getPrincipal();
            }
        }

        return user;
    }

}
