package com.transretail.crm.core.entity.enums;

public enum DataAnalysis {

	OVERVIEW("overview"),
	LMH("lmh"),
	DECILE("decile");

    private DataAnalysis(String code){
    	this.code = code;
    }

    private final String code;

	public String getCode() {
		return code;
	}
    
    
}
