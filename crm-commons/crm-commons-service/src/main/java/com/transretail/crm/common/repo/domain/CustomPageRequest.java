package com.transretail.crm.common.repo.domain;

import java.io.Serializable;

import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;

/**
 * Custom {@link org.springframework.data.domain.PageRequest} that does not throw exception if negative page or size is passed in
 * constructor.
 * <br />
 * Use this if jpa repositories' factor-class uses {@link com.transretail.crm.common.repo.CrmJpaRepositoryFactoryBean}. <br />
 * e.g.
 * <pre>
 *       <jpa:repositories base-package="com.transretail.crm.core.**.repo"
 *          factory-class="com.transretail.crm.common.repo.CrmJpaRepositoryFactoryBean"/>
 * </pre>
 *
 * @author Allan G. Ramirez (agramirez@exist.com)
 */
public class CustomPageRequest implements Pageable, Serializable {
    private static final long serialVersionUID = 5782696380818127956L;
    private final int page;
    private final int size;
    private final Sort sort;

    /**
     * Creates a new {@link CustomPageRequest}. Pages are zero indexed, thus providing 0 for {@code page} will return the first
     * page.
     *
     * @param size
     * @param page
     */
    public CustomPageRequest(int page, int size) {
        this(page, size, null);
    }

    /**
     * Creates a new {@link CustomPageRequest} with sort parameters applied.
     *
     * @param page
     * @param size
     * @param direction
     * @param properties
     */
    public CustomPageRequest(int page, int size, Sort.Direction direction, String... properties) {
        this(page, size, new Sort(direction, properties));
    }

    /**
     * Creates a new {@link CustomPageRequest} with sort parameters applied.
     *
     * @param page
     * @param size
     * @param sort can be {@literal null}.
     */
    public CustomPageRequest(int page, int size, Sort sort) {
        this.page = page;
        this.size = size;
        this.sort = sort;
    }

    /*
     * (non-Javadoc)
     * @see org.springframework.data.domain.Pageable#getPageSize()
     */
    public int getPageSize() {

        return size;
    }

    /*
     * (non-Javadoc)
     * @see org.springframework.data.domain.Pageable#getPageNumber()
     */
    public int getPageNumber() {
        return page;
    }

    /*
     * (non-Javadoc)
     * @see org.springframework.data.domain.Pageable#getOffset()
     */
    public int getOffset() {
        return page * size;
    }

    /*
     * (non-Javadoc)
     * @see org.springframework.data.domain.Pageable#getSort()
     */
    public Sort getSort() {
        return sort;
    }

    /*
     * (non-Javadoc)
     * @see org.springframework.data.domain.Pageable#hasPrevious()
     */
    public boolean hasPrevious() {
        return page > 0;
    }

    /*
     * (non-Javadoc)
     * @see org.springframework.data.domain.Pageable#next()
     */
    public Pageable next() {
        return new CustomPageRequest(page + 1, size, sort);
    }

    /*
     * (non-Javadoc)
     * @see org.springframework.data.domain.Pageable#previousOrFirst()
     */
    public Pageable previousOrFirst() {
        return hasPrevious() ? new CustomPageRequest(page - 1, size, sort) : this;
    }

    /*
     * (non-Javadoc)
     * @see org.springframework.data.domain.Pageable#first()
     */
    public Pageable first() {
        return new CustomPageRequest(0, size, sort);
    }

    /*
     * (non-Javadoc)
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(final Object obj) {

        if (this == obj) {
            return true;
        }

        if (!(obj instanceof CustomPageRequest)) {
            return false;
        }

        CustomPageRequest that = (CustomPageRequest) obj;

        boolean pageEqual = this.page == that.page;
        boolean sizeEqual = this.size == that.size;

        boolean sortEqual = this.sort == null ? that.sort == null : this.sort.equals(that.sort);

        return pageEqual && sizeEqual && sortEqual;
    }

    /*
     * (non-Javadoc)
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode() {

        int result = 17;

        result = 31 * result + page;
        result = 31 * result + size;
        result = 31 * result + (null == sort ? 0 : sort.hashCode());

        return result;
    }

    /*
     * (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return String.format("Page request [number: %d, size %d, sort: %s]", page, size,
            sort == null ? null : sort.toString());
    }
}
