package com.transretail.crm.loyalty.dto;

import org.springframework.beans.BeanUtils;

import com.transretail.crm.core.entity.lookup.LookupDetail;
import com.transretail.crm.loyalty.entity.LoyaltyPhysicalCount;
import java.util.ArrayList;
import java.util.List;

public class LoyaltyPhysicalCountDto {

    private String location;
    private LookupDetail cardType;
    private String startingSeries;
    private String endingSeries;
    private Long quantity;
    private String boxNo;
    private List<String> barcodes = new ArrayList<String>();

    public LoyaltyPhysicalCountDto() {
    }

    public LoyaltyPhysicalCountDto(LoyaltyPhysicalCount countModel) {
	BeanUtils.copyProperties(countModel, this);
    }

    public LoyaltyPhysicalCount toModel() {
	LoyaltyPhysicalCount countModel = new LoyaltyPhysicalCount();
	BeanUtils.copyProperties(this, countModel);
	return countModel;
    }

    public String getLocation() {
	return location;
    }

    public void setLocation(String location) {
	this.location = location;
    }

    public LookupDetail getCardType() {
	return cardType;
    }

    public void setCardType(LookupDetail cardType) {
	this.cardType = cardType;
    }

    public String getStartingSeries() {
	return startingSeries;
    }

    public void setStartingSeries(String startingSeries) {
	this.startingSeries = startingSeries;
    }

    public String getEndingSeries() {
	return endingSeries;
    }

    public void setEndingSeries(String endingSeries) {
	this.endingSeries = endingSeries;
    }

    public Long getQuantity() {
	return quantity;
    }

    public void setQuantity(Long quantity) {
	this.quantity = quantity;
    }

    public String getBoxNo() {
	return boxNo;
    }

    public void setBoxNo(String boxNo) {
	this.boxNo = boxNo;
    }

    public List<String> getBarcodes() {
	if (null == barcodes) // javascript will set this to null. As a good practice(as Java Developer) we will return an empty list.
	    this.barcodes = new ArrayList<String>();
	    
	return barcodes;
    }

    public void setBarcodes(List<String> barcodes) {
        this.barcodes = barcodes;
    }

}
