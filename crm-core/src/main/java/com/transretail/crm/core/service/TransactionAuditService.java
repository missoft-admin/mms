package com.transretail.crm.core.service;

import java.util.List;

import com.transretail.crm.core.entity.TransactionAudit;

public interface TransactionAuditService {

	void saveTransactionAudits(List<TransactionAudit> audits);

    TransactionAudit saveTransactionAudit(TransactionAudit audit);

    List<TransactionAudit> retrieveAuditsByTransactionNo(String transactionNo);

}
