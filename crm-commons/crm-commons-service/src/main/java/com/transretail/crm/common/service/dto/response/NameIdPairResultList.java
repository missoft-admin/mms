package com.transretail.crm.common.service.dto.response;

import java.util.ArrayList;
import java.util.Collection;

import org.springframework.data.domain.Page;

/**
 *
 */
public class NameIdPairResultList extends AbstractResultListDTO<NameIdPairDto> {
    public NameIdPairResultList(Page<NameIdPairDto> page) {
        super(page.getContent(), page.getTotalElements(), page.hasPreviousPage(), page.hasNextPage());
    }

    public NameIdPairResultList(Collection<NameIdPairDto> results, long totalElements, boolean hasPreviousPage, boolean hasNextPage) {
        super(results, totalElements, hasPreviousPage, hasNextPage);
    }

    public static NameIdPairResultList emptyList() {
        return new NameIdPairResultList(new ArrayList<NameIdPairDto>(), 0, false, false);
    }
}