<%@include file="../common/messages.jsp" %>

<style type="text/css">
<!--
  .well .input {
    background-color: #fff;
    border: 1px solid #E8E8E8;
    box-shadow: none;    
    padding: 6px;
    height: 18px;
    margin-left: 5px;
  }
  .well .btn-primary {
    margin-left: 5px;
  }
  .well select, .well .input {
    margin: 5px;
  }
  
-->
</style>

<div id="content_user">

  <div class="page-header page-header2"><h1><spring:message code="admin_user" /></h1></div>

  <div id="form_userSearchForm" class="form-horizontal block-search well">
    <div id="control-group" class="form-inline">
      <label for="" class="label-single"><h5><spring:message code="search_by"/></h5></label>
        <select id="searchField">
          <c:forEach var="field" items="${userSearchFields}">
            <option value="${field}"><spring:message code="field_${field}" /></option>
          </c:forEach>
        </select>
        <div id="userSearchForm" class="inline" style="margin-left:5px">
	          <input name="" id="searchValue" class="input" />
	          <input type="button" id="userSearchButton" value="<spring:message code="label_search" />" class="btn btn-primary"/>
	          <input type="button" value="<spring:message code="label_clear" />" class="btn custom-reset" data-form-id="form_userSearchForm" data-default-id="userSearchButton" />
        </div>
    </div>
  </div>
  
  
  <div class="">
      <div class="pull-right"><a href="#" class="btn btn-primary" data-url="<c:url value="/user/create"/>" id="link_createUser"><spring:message code="user_label_create" /></a></div>
  </div>
  <div class="clearfix"></div>
  <div id="form_user"></div>
  <div id="list_users" class="data-content-02"><jsp:include page="userList.jsp"/></div>


  <script src='<c:url value="/js/viewspecific/user/user.js"/>' type='text/javascript'><![CDATA[&nbsp; ]]></script>
  <link href="<c:url value="/styles/temp.css" />" rel="stylesheet" />

</div>