/*
 * Copyright (c) 2012.
 * All rights reserved.
 */

package com.transretail.crm.core.security.model;

import org.joda.time.LocalDate;

import com.transretail.crm.common.security.model.SecurityUserInfo;

/**
 * @author mhua
 */
public interface CustomSecurityUserInfo extends SecurityUserInfo {

    String getGender();

    LocalDate getBirthDate();

}
