<%@include file="../common/taglibs.jsp" %>


<div id="content_complaint">

  <div class="page-header page-header2"><h1><spring:message code="complaint_pageheader" /></h1></div>

  <div class="searchFilter">
    <div id="form_complaintSearch"><jsp:include page="forms/complaintSearchForm.jsp" /></div>
  </div>

  <div class="">
    <div class="pull-right mb20">
      <a id="link_create" data-action="<c:url value="${complaintAction}"/>" data-modal-header="${complaintHeader}" 
        href="<c:url value="/customer/complaint/form/new" />" class="btn btn-primary">
        <spring:message code="complaint_lbl_create" />
      </a>
      <div id="form_complaint"><%-- <jsp:include page="complaintForm.jsp" /> --%></div>
      <div id="view_complaint"></div>
    </div>
    <div class="clearfix"></div>
  </div>

  <div id="list_complaints" class=""><jsp:include page="complaintList.jsp"/></div>


<style type="text/css"> 
<!-- 
  #complaintSearchForm .btn.btn-primary { margin-left: 5px; } 
  select {
      margin-right: 0px;
  }
  .well #complaintSearchForm .row-fluid {
    margin: 0px;
  }
  .well #complaintSearchForm .input-small, .well #complaintSearchForm label, .well #complaintSearchForm select {margin: 0 5px 0 5px}

--> 
</style>
<script type='text/javascript'>
var Complaint = null;
var ComplaintCommon = null;

$(document).ready( function() {

	  var $main = $( "#content_complaint" );
	  var $createLink = $main.find( "#link_create" );
	  var $contentForm = $main.find( "#form_complaint" );
	  //var $contentView = $main.find( "#view_complaint" );

	  initLinks();

	  function initLinks() {
		    $createLink.click( function(e) {
			      e.preventDefault();
			      $.get( $( this ).attr( "href" ), function( resp )  {
			    	    Complaint.loadForm( resp );
			      }, "html" );
			      //ComplaintForm.create( $(this).data( "action" ), $(this).data( "modal-header" ) );
		    });
	  }

	  Complaint = {
		    loadForm  : function( resp ) { 
		    	  $contentForm.html( resp );
		    	  ComplaintForm.show();
		    }
	  };

	  ComplaintCommon = {
			  processResp  : function( resp, $dialog, errorContainer, $save ) {
				    $save.prop( "disabled", false );
				    if ( resp.success ) {
				    	  $dialog.modal( "hide" );
				    }
				    else {
					      var errors = "";
					      $( resp.result ).each( function( key, value ) { errors += ( ( key + 1 ) + ". " + value.code + "<br>" ); });
					      errorContainer.find( "div" ).html( errors );
					      errorContainer.show( "slow" );
				    }
				},
				reset     : function( ele, isEmpty ) {
					  $( ele ).find(':input').each( function() {
						    switch(this.type) {
			              case 'password':
			              case 'select-one':
			              case 'text':
			              case 'textarea':
			              case 'file':
			              case 'hidden': $(this).val(''); break;
			              case 'select-multiple': isEmpty? $(this).empty() : $(this).val(""); break;
			              case 'checkbox':
			              case 'radio': this.checked = false;
				        }
				    });
        },
        limitCharSize : function( e, charLength, limit, func ) {
            /*var value = "";
            if( window.chrome ){ value = $(this).val().replace( /(\r\n|\n|\r)/g, "  " ); }
            else{ value = $(this).val();}*/
            if ( charLength > limit ) {
                if ( $.inArray( e.keyCode, [46, 8, 9, 27] ) != -1 
                    || ( e.keyCode == 65 && e.ctrlKey === true ) 
                    || ( e.keyCode >= 35 && e.keyCode <= 39 ) ) {
                  return true;
                }
                return false;
            }
            return func();
        }
	  };

});
</script>


</div>