package com.transretail.crm.common.service.dto.rest;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.transretail.crm.common.enums.MessageType;

import java.io.Serializable;


/**
 *
 * Convenient DTO object to store  messages to be returned by a service (Rest calls)
 *
 */
@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
public class ReturnMessage implements Serializable {



    private MessageType type = MessageType.ERROR;

    private String callingAction ="";

    private String message = "";

    private String messageCode = "";
    
    private String firstName = "";
    
    private String lastName = "";

    private String memberName = "";

    private String accountNumber = "";

    private String memberType = "";

    private String memberTypeDesc = "";

    private String cardType = "";

    private String cardTypeDesc = "";

    private Long totalPoints = 0L;

    private Double earnedPoints = 0.0;

    private Double validPurchaseTxnAmount = 0.0;

    private Double currencyAmount = 0.0;

    private Double discount = 0.0;
    
    private String contact = "";
    
    private String npwpId = "";
    
    private String npwpName = "";
    
    private String npwpAddress = "";
    
    private String businessName = "";
    
    private String ktpId = "";
    
    private String[] profitCodes = new String[] {};

    private String loyaltyCardNo = "";

    private String loyaltyCardExpiration = "";

    private Boolean loyaltyCardExpired = new Boolean(false);

    private String status = "";

    private Double earnedStamps = 0.0;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getLoyaltyCardNo() {
        return loyaltyCardNo;
    }

    public void setLoyaltyCardNo(String loyaltyCardNo) {
        this.loyaltyCardNo = loyaltyCardNo;
    }

    public String getLoyaltyCardExpiration() {
        return loyaltyCardExpiration;
    }

    public void setLoyaltyCardExpiration(String loyaltyCardExpiration) {
        this.loyaltyCardExpiration = loyaltyCardExpiration;
    }

    public Boolean getLoyaltyCardExpired() {
        return loyaltyCardExpired;
    }

    public void setLoyaltyCardExpired(Boolean loyaltyCardExpired) {
        this.loyaltyCardExpired = loyaltyCardExpired;
    }

    public String getMemberTypeDesc() {
        return memberTypeDesc;
    }

    public void setMemberTypeDesc(String memberTypeDesc) {
        this.memberTypeDesc = memberTypeDesc;
    }

    public String getCardTypeDesc() {
        return cardTypeDesc;
    }

    public void setCardTypeDesc(String cardTypeDesc) {
        this.cardTypeDesc = cardTypeDesc;
    }

    public Double getDiscount() {
        return discount;
    }

    public void setDiscount(Double discount) {
        this.discount = discount;
    }

    public String getMemberName() {
        return memberName;
    }

    public void setMemberName(String memberName) {
        this.memberName = memberName;
    }

    public String getAccountNumber() {
        return accountNumber;
    }

    public void setAccountNumber(String accountNumber) {
        this.accountNumber = accountNumber;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public ReturnMessage withMessage(String message) {
        this.message = message;
        return this;
    }

    public String getCallingAction() {
        return callingAction;
    }

    public void setCallingAction(String callingAction) {
        this.callingAction = callingAction;
    }

    public MessageType getType() {
        return type;
    }

    public void setType(MessageType type) {
        this.type = type;
    }

    public ReturnMessage withType(MessageType type) {
        this.type = type;
        return this;
    }

    public Long getTotalPoints() {
        return totalPoints;
    }

    public void setTotalPoints(Long totalPoints) {
        this.totalPoints = totalPoints;
    }

    public String getMessageCode() {
        return messageCode;
    }

    public void setMessageCode(String messageCode) {
        this.messageCode = messageCode;
    }

    public String getMemberType() {
        return memberType;
    }

    public void setMemberType(String memberType) {
        this.memberType = memberType;
    }

    public String getCardType() {
        return cardType;
    }

    public void setCardType(String cardType) {
        this.cardType = cardType;
    }

    public Double getEarnedPoints() {
        return earnedPoints;
    }

    public void setEarnedPoints(Double earnedPoints) {
        this.earnedPoints = earnedPoints;
    }

    public Double getCurrencyAmount() {
        return currencyAmount;
    }

    public void setCurrencyAmount(Double currencyAmount) {
        this.currencyAmount = currencyAmount;
    }

    public Double getValidPurchaseTxnAmount() {
        return validPurchaseTxnAmount;
    }

    public void setValidPurchaseTxnAmount(Double validPurchaseTxnAmount) {
        this.validPurchaseTxnAmount = validPurchaseTxnAmount;
    }

	public String getContact() {
		return contact;
	}

	public void setContact(String contact) {
		this.contact = contact;
	}

	public String getNpwpId() {
		return npwpId;
	}

	public void setNpwpId(String npwpId) {
		this.npwpId = npwpId;
	}

	public String getNpwpName() {
		return npwpName;
	}

	public void setNpwpName(String npwpName) {
		this.npwpName = npwpName;
	}

	public String getNpwpAddress() {
		return npwpAddress;
	}

	public void setNpwpAddress(String npwpAddress) {
		this.npwpAddress = npwpAddress;
	}

	public String getKtpId() {
		return ktpId;
	}

	public void setKtpId(String ktpId) {
		this.ktpId = ktpId;
	}

	public String getBusinessName() {
		return businessName;
	}

	public void setBusinessName(String businessName) {
		this.businessName = businessName;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String[] getProfitCodes() {
		return profitCodes;
	}

	public void setProfitCodes(String[] profitCodes) {
		this.profitCodes = profitCodes;
	}

    public Double getEarnedStamps() {
        return earnedStamps;
    }

    public void setEarnedStamps(Double earnedStamps) {
        this.earnedStamps = earnedStamps;
    }
}
