package com.transretail.crm.giftcard.service.impl;


import java.util.ArrayList;
import java.util.List;

import junit.framework.Assert;

import org.apache.commons.collections.CollectionUtils;
import org.joda.time.LocalDate;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.transretail.crm.common.service.dto.response.ResultList;
import com.transretail.crm.giftcard.dto.GiftCardInventoryStockDto;
import com.transretail.crm.giftcard.dto.GiftCardInventoryStockSearchDto;
import com.transretail.crm.giftcard.entity.CardVendor;
import com.transretail.crm.giftcard.entity.GiftCardInventory;
import com.transretail.crm.giftcard.entity.GiftCardOrder;
import com.transretail.crm.giftcard.entity.ProductProfile;
import com.transretail.crm.giftcard.entity.support.GiftCardInventoryStatus;
import com.transretail.crm.giftcard.entity.support.GiftCardOrderStatus;
import com.transretail.crm.giftcard.entity.support.ProductProfileStatus;
import com.transretail.crm.giftcard.repo.CardVendorRepo;
import com.transretail.crm.giftcard.repo.GiftCardInventoryRepo;
import com.transretail.crm.giftcard.repo.GiftCardInventoryStockRepo;
import com.transretail.crm.giftcard.repo.GiftCardOrderRepo;
import com.transretail.crm.giftcard.repo.ProductProfileRepo;
import com.transretail.crm.giftcard.service.GiftCardInventoryStockService;


@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath*:/META-INF/spring/applicationContext*.xml", "classpath:META-INF/spring/applicationContext-test.xml"})
public class GiftCardInventoryStockServiceTest {

    @Autowired
    private GiftCardInventoryStockService service;
    @Autowired
    private GiftCardInventoryStockRepo repo;
    @Autowired
    private GiftCardInventoryRepo giftCardInventoryRepo;
    @Autowired
    private GiftCardOrderRepo giftCardOrderRepo;
    @Autowired
    private CardVendorRepo cardVendorRepo;
    @Autowired
    private ProductProfileRepo productProfileRepo;



    /*private static Long SAVED_ID 				= null;
    private static final String SAVED_CXID 		= "CX000";
    private static final String UPDATED_CXNAME 	= "CXNAME_UPDATED";*/



    @Before
    public void onSetup() {
    	CardVendor cv = new CardVendor();
    	cv.setFormalName( "FORMAL_NAME" );
    	cv.setMailAddress( "MAIL_ADDRESS" );
    	cv.setEmailAddress( "EMAIL_ADDRESS" );
    	cv.setEnabled( true );
    	cv.setRegion( "REGION" );
    	cv.setDefaultVendor( true );
    	cardVendorRepo.saveAndFlush( cv );

    	GiftCardOrder gco = new GiftCardOrder();
		gco.setVendor( cv );
		gco.setStatus( GiftCardOrderStatus.FULL );
		gco.setMoNumber( "MoNumber" );
		giftCardOrderRepo.saveAndFlush( gco );

		ProductProfile pf = new ProductProfile();
		pf.setProductCode( "PRD_PROF" );
		productProfileRepo.saveAndFlush( pf );

	    List<GiftCardInventory> inventories = new ArrayList<GiftCardInventory>();
	    GiftCardInventory inventory = new GiftCardInventory();
	    inventory.setSeqNo( 0 );
	    inventory.setBatchNo( 0 );
	    inventory.setOrderDate( new LocalDate() );
	    inventory.setSeries( 1404241000029959l );
	    inventory.setBarcode( "14042410100000029959" );
	    inventory.setProductCode( "PRODUCT_CODE" );
	    inventory.setProductName( "PRODUCT_NAME" );
	    inventory.setProfile( pf );
	    inventory.setOrder( gco );
	    inventory.setStatus( GiftCardInventoryStatus.IN_STOCK );
	    inventories.add( inventory );

	    inventory = new GiftCardInventory();
	    inventory.setSeqNo( 0 );
	    inventory.setBatchNo( 0 );
	    inventory.setOrderDate( new LocalDate() );
	    inventory.setSeries( 1404241000029960l );
	    inventory.setBarcode( "14042410100000029960" );
	    inventory.setProductCode( "PRODUCT_CODE" );
	    inventory.setProductName( "PRODUCT_NAME" );
	    inventory.setProfile( pf );
	    inventory.setOrder( gco );
	    inventory.setStatus( GiftCardInventoryStatus.IN_STOCK );
	    inventories.add( inventory );
	    giftCardInventoryRepo.save( inventories );
	    
	    //Tests for testGetAllDatesOfBurnedCards
	    inventory = new GiftCardInventory();
        inventory.setSeqNo(1234);
        inventory.setBatchNo(1234);
        inventory.setOrderDate(new LocalDate());
        inventory.setSeries(1404241000099999l);
        inventory.setBarcode("14042410100000099999");
        inventory.setProductCode("StocksTests");
        inventory.setProductName("StocksTests");
        inventory.setProfile(pf);
        inventory.setOrder(gco);
        inventory.setStatus(GiftCardInventoryStatus.BURNED);
        inventories.add(inventory);
        
        //Tests for testGetAllDatesOfBurnedCards
        inventory = new GiftCardInventory();
        inventory.setSeqNo(12345);
        inventory.setBatchNo(12345);
        inventory.setOrderDate(new LocalDate());
        inventory.setSeries(1404241000099998l);
        inventory.setBarcode("14042410100000099998");
        inventory.setProductCode("StocksTests");
        inventory.setProductName("StocksTests");
        inventory.setProfile(pf);
        inventory.setOrder(gco);
        inventory.setStatus(GiftCardInventoryStatus.BURNED);
        inventories.add(inventory);
        
        giftCardInventoryRepo.save( inventories );
    }

    @After
    public void tearDown() {
    	repo.deleteAll();
    	giftCardInventoryRepo.deleteAll();
    	productProfileRepo.deleteAll();
    	giftCardOrderRepo.deleteAll();
    	cardVendorRepo.deleteAll();
    	productProfileRepo.deleteAll();
    }



    @Test
    public void testSaveAndSearchStocks() {
    	Assert.assertEquals( true, CollectionUtils.isEmpty( repo.findAll() ) );
    	service.saveStocks( giftCardInventoryRepo.findAll() );
    	Assert.assertEquals( 4, repo.findAll().size() );

    	ResultList<GiftCardInventoryStockDto> dtos = service.searchStock( new GiftCardInventoryStockSearchDto() );
    	Assert.assertEquals( true, CollectionUtils.isNotEmpty( dtos.getResults() ) );
    }

    @Test
    public void testGetCardTypes() {
    	Assert.assertEquals( 0, service.getCardTypes().size() );
    	List<ProductProfile> pfs = productProfileRepo.findAll();
    	for ( ProductProfile pf : pfs ) {
    		pf.setStatus( ProductProfileStatus.APPROVED );
    	}
    	productProfileRepo.save( pfs );
    	Assert.assertEquals( 1, service.getCardTypes().size() );
    }
    
}
