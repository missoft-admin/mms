package com.transretail.crm.template.service.streaming.excel.handler.cell;

import com.transretail.crm.core.entity.MemberModel;
import com.transretail.crm.core.entity.embeddable.CustomerProfile;
import com.transretail.crm.template.service.MemberTemplateParseException;

/**
 * @author Allan G. Ramirez (agramirez@exist.com)
 */
public class ClosestStoreCellHandler implements CellHandler {
    @Override
    public void handle(MemberModel dto, String cellText) throws MemberTemplateParseException {
        if (dto.getCustomerProfile() == null) {
            dto.setCustomerProfile(new CustomerProfile());
        }
        dto.getCustomerProfile().setClosestStore(cellText);
    }
}
