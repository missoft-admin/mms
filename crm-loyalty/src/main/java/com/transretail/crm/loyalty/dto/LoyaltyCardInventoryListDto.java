package com.transretail.crm.loyalty.dto;

import org.joda.time.DateTime;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.mysema.query.annotations.QueryProjection;
import com.transretail.crm.common.web.converter.DateTimeSerializer;

public class LoyaltyCardInventoryListDto {
	
	private Long orderId;
	
	private String product;
	
	private String productId;
	
	private String status;
	
	private String statusCode;
	
	private String transferTo;
	
	private String transferToCode;
	
	private String location;
	
	private String locationCode;
	
	private String referenceNo;
	
	private Long quantity;
	
	private String newTransferTo;
	
	private String newTransferToCode;
	
	private Boolean forBurning = Boolean.FALSE;
	
	@JsonSerialize(using = DateTimeSerializer.class)
	private DateTime lastUpdateDate;
	
	public LoyaltyCardInventoryListDto() {}
	
	@QueryProjection
	public LoyaltyCardInventoryListDto(String product,
			String productId, String status, String statusCode,
			String transferToCode,
			String locationCode, Long quantity) {
		super();
		this.product = product;
		this.productId = productId;
		this.status = status;
		this.statusCode = statusCode;
		this.transferToCode = transferToCode;
		this.locationCode = locationCode;
		this.quantity = quantity;
	}
	
	@QueryProjection
	public LoyaltyCardInventoryListDto(String product,
			String productId, String status, String statusCode,
			String transferToCode,
			String locationCode, 
			String newTransferToCode,
			Boolean forBurning,
			Long quantity) {
		super();
		this.product = product;
		this.productId = productId;
		this.status = status;
		this.statusCode = statusCode;
		this.transferToCode = transferToCode;
		this.locationCode = locationCode;
		this.quantity = quantity;
		this.newTransferToCode = newTransferToCode;
		this.forBurning = forBurning;
	}

	public String getProduct() {
		return product;
	}

	public void setProduct(String product) {
		this.product = product;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getTransferTo() {
		return transferTo;
	}

	public void setTransferTo(String transferTo) {
		this.transferTo = transferTo;
	}

	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		this.location = location;
	}

	public Long getQuantity() {
		return quantity;
	}

	public void setQuantity(Long quantity) {
		this.quantity = quantity;
	}

	public String getStatusCode() {
		return statusCode;
	}

	public void setStatusCode(String statusCode) {
		this.statusCode = statusCode;
	}

	public String getTransferToCode() {
		return transferToCode;
	}

	public void setTransferToCode(String transferToCode) {
		this.transferToCode = transferToCode;
	}

	public String getLocationCode() {
		return locationCode;
	}

	public void setLocationCode(String locationCode) {
		this.locationCode = locationCode;
	}

	public Long getOrderId() {
		return orderId;
	}

	public void setOrderId(Long orderId) {
		this.orderId = orderId;
	}

	public String getProductId() {
		return productId;
	}

	public void setProductId(String productId) {
		this.productId = productId;
	}

	public String getReferenceNo() {
		return referenceNo;
	}

	public void setReferenceNo(String referenceNo) {
		this.referenceNo = referenceNo;
	}

	public DateTime getLastUpdateDate() {
		return lastUpdateDate;
	}

	public void setLastUpdateDate(DateTime lastUpdateDate) {
		this.lastUpdateDate = lastUpdateDate;
	}

	public String getNewTransferToCode() {
		return newTransferToCode;
	}

	public void setNewTransferToCode(String newTransferToCode) {
		this.newTransferToCode = newTransferToCode;
	}

	public String getNewTransferTo() {
		return newTransferTo;
	}

	public void setNewTransferTo(String newTransferTo) {
		this.newTransferTo = newTransferTo;
	}

	public Boolean getForBurning() {
		return forBurning;
	}

	public void setForBurning(Boolean forBurning) {
		this.forBurning = forBurning;
	}

	
	
	
}
