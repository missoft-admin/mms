package com.transretail.crm.giftcard.dto;

import java.util.List;

import org.apache.commons.lang3.StringUtils;

import com.google.common.collect.Lists;
import com.transretail.crm.giftcard.entity.B2BExhibition;
import com.transretail.crm.giftcard.entity.B2BExhibitionGc;
import com.transretail.crm.giftcard.entity.ProductProfile;



/**
 *
 */
public class B2BExhibitionGcDto {
	
	private String startingSeries;
	private String endingSeries;
	private Long product;
	
	public B2BExhibitionGcDto() {}
	public B2BExhibitionGcDto(B2BExhibitionGc gc) {
		this.startingSeries = gc.getStartingSeries();
		this.endingSeries = gc.getEndingSeries();
		if(gc.getProduct() != null)
			this.product = gc.getProduct().getId();
	}
	
	public static List<B2BExhibitionGcDto> toDto(List<B2BExhibitionGc> gcs) {
		List<B2BExhibitionGcDto> gcDs = Lists.newArrayList();
		for(B2BExhibitionGc gc: gcs) {
			gcDs.add(new B2BExhibitionGcDto(gc));
		}
		return gcDs;
	}
	
	public B2BExhibitionGc toModel(B2BExhibition exh) {
		B2BExhibitionGc gc = new B2BExhibitionGc();
		gc.setStartingSeries(startingSeries);
		gc.setEndingSeries(endingSeries);
		if(product != null)
			gc.setProduct(new ProductProfile(product));
		gc.setExh(exh);
		return gc;
	}
	
	public static List<B2BExhibitionGc> toModel(List<B2BExhibitionGcDto> gcs, B2BExhibition exh) {
		List<B2BExhibitionGc> gcMos = Lists.newArrayList();
		
		for(B2BExhibitionGcDto gcDto: gcs) {
			if (gcDto != null && StringUtils.isNotBlank(gcDto.getStartingSeries()))
				gcMos.add(gcDto.toModel(exh));
		}
		return gcMos;
		
	}
	
	public String getStartingSeries() {
		return startingSeries;
	}
	public void setStartingSeries(String startingSeries) {
		this.startingSeries = startingSeries;
	}
	public String getEndingSeries() {
		return endingSeries;
	}
	public void setEndingSeries(String endingSeries) {
		this.endingSeries = endingSeries;
	}
	public Long getProduct() {
		return product;
	}
	public void setProduct(Long product) {
		this.product = product;
	}
	
	
	
	
}
