<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<sec:authorize access="isAuthenticated()">
  <c:redirect url="/"/>
</sec:authorize>
<!DOCTYPE html>
<!--[if lt IE 7]>
<html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>
<html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>
<html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!-->
<html class="no-js"> <!--<![endif]-->
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
  <spring:message code="application_name" var="app_name" htmlEscape="false"/>
  <title><spring:message code="welcome_h3" arguments="${app_name}"/></title>
  <meta name="description" content="Member Portal">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <!-- for non html5 compliant browsers -->
  <script src="<spring:url value="/js/modernizr-2.6.2.min.js" />"></script>

  <link href="<c:url value="/images/favicon.ico" />" rel="SHORTCUT ICON"/>
  <%--<link href='http://fonts.googleapis.com/css?family=Lato' rel='stylesheet' type='text/css'>--%>
  <link href="<c:url value="/css/bootstrap/bootstrap.min.css" />" rel="stylesheet">
  <link href="<c:url value="/css/bootstrap/bootstrap-responsive.min.css" />" rel="stylesheet">
  <link href="<c:url value="/css/custom.css" />" rel="stylesheet">
  <link href="<c:url value="/css/anonymous.css" />" rel="stylesheet">

  <script src="<c:url value="/js/jquery/jquery-1.10.2.min.js" />"></script>

</head>
<body class="login">
<div class="container">
  <%@ include file="common/messages.jsp" %>
  <div class="logo-login text-center">
    <img width="350" src="<c:url value="/images/logo-login.png" />">
  </div>
  <form class="form-signin body-container" action="<spring:url value="/login-action"/>" method="post">
    <h3 class="form-signin-heading"><spring:message code="security_login_title"/></h3>
    <c:if test="${not empty param.login_error}">
      <div class="alert alert-error">
        <button type="button" class="close" data-dismiss="alert">&times;</button>
        <spring:message code="security_login_unsuccessful"/>
        <spring:message code="${SPRING_SECURITY_LAST_EXCEPTION.message}"/>
      </div>
    </c:if>
    <input type="text" class="input-block-level" placeholder="<spring:message code="security_login_form_name"/>" name="username">
    <input type="password" class="input-block-level" placeholder="<spring:message code="security_login_form_password"/>" name="password">
    <%--<label class="checkbox capitalize">--%>
    <%--<input type="checkbox" value="remember-me"> Remember me--%>
    <%--</label>--%>
    <button type="submit" class="btn btn-block btn-primary"><spring:message code="security_login"/></button>
    <br/>
  </form>
</div>
<div id="footer">
  <p>
    Version <spring:message code="project.version" />
    <spring:message var="commitId" code="git.commit.id.abbrev" />
    <c:if test="${!empty commitId}">
    &nbsp;rev. ${commitId}
  <div style="display: none;">
    Branch: <spring:message code="git.branch" />
    <br/>
    Last commit ID: <spring:message code="git.commit.id" />
    <br/>
    Last commit author: <spring:message code="git.commit.user.name" />
    <br/>
    Last commit time: <spring:message code="git.commit.time" />
    <br/>
    Last commit message: <spring:message code="git.commit.message.full" />
  </div>
  </c:if>
  </p>
</div>
<style>
  .body-container {
    max-width: 300px;
  }
  #footer {
    text-align: center;
    clear: both;
    color: #444;
  }
</style>
<!-- /container -->
<!-- Bootstrap core JavaScript
================================================== -->
<!-- Placed at the end of the document so the pages load faster -->
<script src="<c:url value="/js/bootstrap/bootstrap.min.js" />"></script>
</body>
</html>