alter table CRM_MEMBER modify ZONE varchar2(255 char);
alter table CRM_MEMBER modify RADIUS varchar2(255 char);
alter table CRM_ADDRESS modify RT varchar2(255 char);
alter table CRM_ADDRESS modify RW varchar2(255 char);