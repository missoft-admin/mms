package com.transretail.crm.core.repo;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.transretail.crm.common.repo.CrmQueryDslPredicateExecutor;
import com.transretail.crm.core.entity.UserRoleModel;
import com.transretail.crm.core.repo.custom.UserRoleRepoCustom;

@Repository
public interface UserRoleRepo extends CrmQueryDslPredicateExecutor<UserRoleModel, String>, UserRoleRepoCustom {

}
