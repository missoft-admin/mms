create table CRM_PROMO_BANNER (
    ID varchar2(255 char) not null,
    END_DATE date,
    IMAGE blob,
    LINK varchar2(255 char),
    NAME varchar2(255 char),
    START_DATE date,
    primary key (ID
));