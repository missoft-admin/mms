package com.transretail.crm.giftcard.service.impl;

import org.joda.time.LocalDate;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Matchers;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.transretail.crm.giftcard.entity.QSalesOrder;

import ch.qos.logback.core.boolex.Matcher;

import com.transretail.crm.common.service.exception.MessageSourceResolvableException;
import com.transretail.crm.giftcard.dto.GiftCardExpiryExtensionDto;
import com.transretail.crm.giftcard.dto.SalesOrderDto;
import com.transretail.crm.giftcard.entity.CardVendor;
import com.transretail.crm.giftcard.entity.GiftCardExpiryExtension;
import com.transretail.crm.giftcard.entity.GiftCardInventory;
import com.transretail.crm.giftcard.entity.GiftCardOrder;
import com.transretail.crm.giftcard.entity.ProductProfile;
import com.transretail.crm.giftcard.entity.SalesOrder;
import com.transretail.crm.giftcard.entity.support.GiftCardDateExtendStatus;
import com.transretail.crm.giftcard.entity.support.GiftCardInventoryStatus;
import com.transretail.crm.giftcard.entity.support.GiftCardOrderStatus;
import com.transretail.crm.giftcard.entity.support.SalesOrderStatus;
import com.transretail.crm.giftcard.entity.support.SalesOrderType;
import com.transretail.crm.giftcard.repo.CardVendorRepo;
import com.transretail.crm.giftcard.repo.GiftCardExpiryExtensionRepo;
import com.transretail.crm.giftcard.repo.GiftCardInventoryRepo;
import com.transretail.crm.giftcard.repo.GiftCardOrderRepo;
import com.transretail.crm.giftcard.repo.ProductProfileRepo;
import com.transretail.crm.giftcard.repo.SalesOrderRepo;
import com.transretail.crm.giftcard.service.GiftCardExpiryExtensionService;
import com.transretail.crm.giftcard.service.GiftCardInventoryService;
import com.transretail.crm.giftcard.service.SalesOrderService;

/**
 * @author ftopico
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath*:/META-INF/spring/applicationContext*.xml", "classpath:META-INF/spring/applicationContext-test.xml"})
public class GiftCardExpiryExtensionServiceTest {

    @Autowired
    private GiftCardExpiryExtensionService giftCardExpiryExtensionService;
    @Autowired
    private SalesOrderService salesOrderService;
    @Autowired
    private GiftCardExpiryExtensionRepo giftCardExpiryExtensionRepo;
    @Autowired
    private GiftCardInventoryRepo inventoryRepo;
    @Autowired
    private ProductProfileRepo productProfileRepo;
    @Autowired
    private GiftCardOrderRepo giftCardOrderRepo;
    @Autowired
    private CardVendorRepo cardVendorRepo;
    @Autowired
    private SalesOrderRepo orderRepo;
    @Autowired
    private GiftCardInventoryService giftCardInventoryService;
    
    @Before
    public void setup() {
        ProductProfile productProfile = new ProductProfile();
        productProfile.setProductCode("CODE01");
        productProfile.setProductDesc("TestProduct");
        productProfile = productProfileRepo.save(productProfile);
        
        CardVendor cardVendor = new CardVendor();
        cardVendor.setFormalName("cardFormalName");
        cardVendor.setMailAddress("CardMail");
        cardVendor.setEmailAddress("EmailVendor");
        cardVendor.setEnabled(true);
        cardVendor.setRegion("CardRegion");
        cardVendor.setDefaultVendor(true);
        cardVendor = cardVendorRepo.save(cardVendor);
        
        GiftCardOrder giftCardOrder = new GiftCardOrder();
        giftCardOrder.setVendor(cardVendor);
        giftCardOrder.setStatus(GiftCardOrderStatus.APPROVED);
        giftCardOrder = giftCardOrderRepo.save(giftCardOrder);
        
        GiftCardInventory giftCardInventory = new GiftCardInventory();
        giftCardInventory.setProfile(productProfile);
        giftCardInventory.setOrder(giftCardOrder);
        giftCardInventory.setProductName(productProfile.getProductCode());
        giftCardInventory.setProductCode(productProfile.getProductDesc());
        giftCardInventory.setSeqNo(0);
        giftCardInventory.setBatchNo(0);
        giftCardInventory.setOrderDate(LocalDate.now());
        giftCardInventory.setStatus(GiftCardInventoryStatus.IN_STOCK);
        giftCardInventory.setSeries(0000000000000001L);
        giftCardInventory.setBarcode("00000000000000010001");
        
        inventoryRepo.save(giftCardInventory);
        
        giftCardInventory = new GiftCardInventory();
        giftCardInventory.setProfile(productProfile);
        giftCardInventory.setOrder(giftCardOrder);
        giftCardInventory.setProductName(productProfile.getProductCode());
        giftCardInventory.setProductCode(productProfile.getProductDesc());
        giftCardInventory.setSeqNo(0);
        giftCardInventory.setBatchNo(0);
        giftCardInventory.setOrderDate(LocalDate.now());
        giftCardInventory.setStatus(GiftCardInventoryStatus.IN_STOCK);
        giftCardInventory.setSeries(0000000000000002L);
        giftCardInventory.setBarcode("00000000000000020002");
        inventoryRepo.save(giftCardInventory);
        
        SalesOrder order = new SalesOrder();
        order.setOrderNo("BBMMS0501001");
        order.setOrderDate(new LocalDate());
        order.setStatus(SalesOrderStatus.DRAFT);
        order.setOrderType(SalesOrderType.B2B_SALES);
        order = orderRepo.save(order);
        
        GiftCardExpiryExtension giftCardExpiryExtension = new GiftCardExpiryExtension();
        giftCardExpiryExtension.setExtendNo("00001");
        giftCardExpiryExtensionRepo.save(giftCardExpiryExtension);
    }
    
    @After
    public void cleanUp() {
        inventoryRepo.deleteAll();
        productProfileRepo.deleteAll();
        giftCardOrderRepo.deleteAll();
        cardVendorRepo.deleteAll();
        orderRepo.deleteAll();
        giftCardExpiryExtensionRepo.deleteAll();
    }
    
    @Test
    public void testSaveExpiryExtensionRequest() {
        GiftCardExpiryExtensionDto dto = new GiftCardExpiryExtensionDto();
        try {
            giftCardExpiryExtensionService.saveExpiryExtensionRequest(dto);
        } catch (MessageSourceResolvableException e) {
            Assert.assertNotNull(e.getMessage());
        }
        
        dto.setRangeType("so");
        try {
            giftCardExpiryExtensionService.saveExpiryExtensionRequest(dto);
        } catch (MessageSourceResolvableException e) {
            Assert.assertNotNull(e.getMessage());
        }
        
        dto.setRangeType("series");
        dto.setSeriesFrom(01L);
        try {
            giftCardExpiryExtensionService.saveExpiryExtensionRequest(dto);
        } catch (MessageSourceResolvableException e) {
            Assert.assertNotNull(e.getMessage());
        }
        
        dto.setSeriesFrom(null);
        try {
            giftCardExpiryExtensionService.saveExpiryExtensionRequest(dto);
        } catch (MessageSourceResolvableException e) {
            Assert.assertNotNull(e.getMessage());
        }
        
        dto = new GiftCardExpiryExtensionDto();
        dto.setRangeType("series");
        dto.setSeriesFrom(0000000000000001L);
        dto.setSeriesTo(0000000000000002L);
        try {
            giftCardExpiryExtensionService.saveExpiryExtensionRequest(dto);
        } catch (MessageSourceResolvableException e) {
            Assert.assertNotNull(e.getMessage());
        }
    }
    
    @Test
    public void testGetExpiryExtensionByExtendNo() {
        GiftCardExpiryExtensionDto dto = giftCardExpiryExtensionService.getExpiryExtensionByExtendNo("00001");
        Assert.assertNotNull(dto);
    }
    
    @Test
    public void testRejectRequest() {
        giftCardExpiryExtensionService.rejectRequest("00001");
        GiftCardExpiryExtensionDto dto = giftCardExpiryExtensionService.getExpiryExtensionByExtendNo("00001");
        Assert.assertTrue(dto.getStatus() == GiftCardDateExtendStatus.REJECTED);
    }
}
