alter table CRM_SALES_ORD add (
        PARENT_SALES_ORD number(19,0),
	VOUCHER_VAL number(19,2)
);


alter table CRM_SALES_ORD 
        add constraint FK_forrdafeip5ube7ab3o3t8wfl 
        foreign key (PARENT_SALES_ORD) 
        references CRM_SALES_ORD;
