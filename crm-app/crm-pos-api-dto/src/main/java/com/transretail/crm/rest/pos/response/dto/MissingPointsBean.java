package com.transretail.crm.rest.pos.response.dto;

import java.sql.Timestamp;

/**
 * @author Allan G. Ramirez (agramirez@exist.com)
 */
public class MissingPointsBean {
    private String accountId;
    private String txnNo;
    private Timestamp transactionDate;
    private Timestamp salesDate;
    private long totalAmount;
    private String status;
    private String type;
    private String mediaType;

    public String getStringOutput() {
        StringBuilder sb = new StringBuilder();
        sb.append(accountId);
        sb.append(",");
        sb.append(txnNo);
        sb.append(",");
        sb.append(transactionDate);
        sb.append(",");
        sb.append(salesDate);
        sb.append(",");
        sb.append(totalAmount);
        sb.append(",");
        sb.append(status);
        sb.append(",");
        sb.append(type);
        sb.append(",");
        sb.append(mediaType);
        return sb.toString();
    }

    public MissingPointsBean() {

    }

    public String getAccountId() {
        return accountId;
    }

    public void setAccountId(String accountId) {
        this.accountId = accountId;
    }

    public String getTxnNo() {
        return txnNo;
    }

    public void setTxnNo(String txnNo) {
        this.txnNo = txnNo;
    }

    public Timestamp getTransactionDate() {
        return transactionDate;
    }

    public void setTransactionDate(Timestamp transactionDate) {
        this.transactionDate = transactionDate;
    }

    public Timestamp getSalesDate() {
        return salesDate;
    }

    public void setSalesDate(Timestamp salesDate) {
        this.salesDate = salesDate;
    }

    public long getTotalAmount() {
        return totalAmount;
    }

    public void setTotalAmount(long totalAmount) {
        this.totalAmount = totalAmount;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getMediaType() {
        return mediaType;
    }

    public void setMediaType(String mediaType) {
        this.mediaType = mediaType;
    }
}
