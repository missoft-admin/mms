<%@include file="../common/messages.jsp" %>

<style type="text/css">
<!--
  #list_points .dataTable tr td:nth-child(4), #list_points .dataTable tr th:nth-child(4),
  #list_points .dataTable tr td:nth-child(3), #list_points .dataTable tr th:nth-child(3) {
    min-width: 150px;
  }
  #list_points .dataTable tr td:nth-child(1), #list_points .dataTable tr th:nth-child(1) {
    min-width: 200px;  
  }
  .data-content-03 {
      margin-bottom: 15px !important;
  }
  .table tbody tr td:last-child {
      min-width: auto;
      border-left: none;
  }
  #list_points .table tbody tr td:first-child {
      min-width: 200px;
  }
  
-->
</style>

<div id="content_points">

  <div class="page-header page-header2"><h1><spring:message code="member_points" /></h1></div>

  <div class="pull-left">
    <c:url var="url_printPoints" value="/report/points/export/${member.accountId}/" />
    <a href="#" data-url="${url_printPoints}" class="link_print btn btn-print"><spring:message code="label_print" /></a>
  </div>
  <div class="clearfix data-content-03"></div>

  <div id="form_points"></div>
  <div class="row-fluid form-inline well well-blue">
    <div class="control-group pull-left mb0">
      <label for="memberName" class="control-label"><spring:message code="points_prop_member" />:<![CDATA[&nbsp; ]]></label>
      <span class="control-label" id="memberName"><c:out value="${member.accountId} - ${member.formattedMemberName}" /></span><br/>
        <label for="registeredStore" class="control-label"><spring:message code="label_com_transretail_crm_entity_customermodel_registeredstore" />:<![CDATA[&nbsp; ]]></label>
        <span class="control-label" id="registeredStore"><c:out value="${member.registeredStore}"/> - <c:out value="${member.registeredStoreName}"/></span>
    </div>
    <div class="pull-right control-group">
      <c:choose>
      	<c:when test="${isSupplement}">
      		<label for="totalPoints" class="control-label"><spring:message code="points_label_contributedpoints" />:<![CDATA[&nbsp; ]]></label>
      	</c:when>
      	<c:otherwise>
      		<label for="totalPoints" class="control-label"><spring:message code="points_label_totalpoints" />:<![CDATA[&nbsp; ]]></label>
      	</c:otherwise>
      </c:choose>
      <span class="control-label" id="totalPoints">
      <fmt:formatNumber value="${member.totalPoints}" maxFractionDigits="0" minFractionDigits="0" />
      </span>

      <br/>
      <c:if test="${isEmployee}">
	      <label class="control-label"><spring:message code="points_lbl_totalrewards" />:<![CDATA[&nbsp; ]]></label>
	      <span class="control-label"><fmt:formatNumber value="${totalRewards}" maxFractionDigits="0" minFractionDigits="0" /></span><br/>
      </c:if>
      <label class="control-label"><spring:message code="points_lbl_totalpurchases" />:<![CDATA[&nbsp; ]]></label>
      <span class="control-label"><spring:message code="label_rupiah" /><fmt:formatNumber value="${totalPurchaseTxn}" maxFractionDigits="0" minFractionDigits="0" /></span><br/>
    </div>
  </div>

  <jsp:include page="print.jsp" />
  <jsp:include page="pointsSearchbox.jsp" />
  <div id="list_points" class="data-content-02"><jsp:include page="pointsList.jsp"/></div>


  <script src='<c:url value="/js/viewspecific/points/points.js"/>' type='text/javascript'><![CDATA[&nbsp; ]]></script>
  <link href="<c:url value="/styles/temp.css" />" rel="stylesheet" />


</div>