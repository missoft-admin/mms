package com.transretail.crm.schedule.job.service;

public interface GiftCardTransactionSettlementService {
	void settleGiftCardTransactions();
}
