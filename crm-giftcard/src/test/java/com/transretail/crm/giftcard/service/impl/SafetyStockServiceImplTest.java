package com.transretail.crm.giftcard.service.impl;

import static org.junit.Assert.assertEquals;

import java.util.List;

import org.joda.time.LocalDate;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.TransactionCallbackWithoutResult;
import org.springframework.transaction.support.TransactionTemplate;

import com.transretail.crm.core.service.CodePropertiesService;
import com.transretail.crm.giftcard.dto.GcInventorySafetyStockDto;
import com.transretail.crm.giftcard.dto.SafetyStockDto;
import com.transretail.crm.giftcard.entity.CardVendor;
import com.transretail.crm.giftcard.entity.GiftCardInventory;
import com.transretail.crm.giftcard.entity.GiftCardOrder;
import com.transretail.crm.giftcard.entity.ProductProfile;
import com.transretail.crm.giftcard.entity.support.GiftCardInventoryStatus;
import com.transretail.crm.giftcard.entity.support.GiftCardOrderStatus;
import com.transretail.crm.giftcard.entity.support.ProductProfileStatus;
import com.transretail.crm.giftcard.repo.CardVendorRepo;
import com.transretail.crm.giftcard.repo.GiftCardInventoryRepo;
import com.transretail.crm.giftcard.repo.GiftCardOrderRepo;
import com.transretail.crm.giftcard.repo.ProductProfileRepo;
import com.transretail.crm.giftcard.service.SafetyStockService;

/**
 * @author Allan G. Ramirez (agramirez@exist.com)
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(
    locations = {"classpath*:/META-INF/spring/applicationContext*.xml", "classpath:META-INF/spring/applicationContext-test.xml"})
public class SafetyStockServiceImplTest {
    @Autowired
    private SafetyStockService safetyStockService;
    @Autowired
    private ProductProfileRepo productProfileRepo;
    @Autowired
    private CardVendorRepo cardVendorRepo;
    @Autowired
    private GiftCardOrderRepo giftCardOrderRepo;
    @Autowired
    private GiftCardInventoryRepo giftCardInventoryRepo;
    @Autowired
    private PlatformTransactionManager transactionManager;
    @Autowired
    private CodePropertiesService codePropertiesService;

    private ProductProfile productProfile;

    @Before
    public void onSetup() {
        TransactionTemplate transactionTemplate = new TransactionTemplate(transactionManager);
        transactionTemplate.execute(new TransactionCallbackWithoutResult() {
            @Override
            protected void doInTransactionWithoutResult(TransactionStatus status) {
                productProfile = new ProductProfile();
                productProfile.setProductCode("code");
                productProfile.setProductDesc("desc");
                productProfile.setSafetyStocksEaMo(",,,,5,6,,8,,,,");
                productProfile.setStatus(ProductProfileStatus.APPROVED);
                productProfile = productProfileRepo.saveAndFlush(productProfile);
            }
        });
    }

    @After
    public void tearDown() {
        productProfileRepo.deleteAll();
    }

    @Test
    public void getSafetyStock() {
        SafetyStockDto dto = safetyStockService.getSafetyStock(productProfile.getId());
        assertEquals(",,,,5,6,,8,,,,", dto.getQuantityPerMonth());
        assertEquals(5, dto.getMonth5().intValue());
        assertEquals(6, dto.getMonth6().intValue());
        assertEquals(8, dto.getMonth8().intValue());
    }

    @Test
    public void saveOrUpdate() {
        SafetyStockDto dto = new SafetyStockDto();
        dto.setMonth1(1);
        dto.setMonth8(8);
        dto.setProductProfileId(productProfile.getId());
        safetyStockService.update(dto);

        ProductProfile actual = productProfileRepo.findOne(productProfile.getId());
        assertEquals("1,,,,,,,8,,,,", actual.getSafetyStocksEaMo());
    }

    @Test
    public void checkSafetyStockTest() {
        try {
            final String pp2Code = "code2";

            TransactionTemplate transactionTemplate = new TransactionTemplate(transactionManager);
            transactionTemplate.execute(new TransactionCallbackWithoutResult() {
                @Override
                protected void doInTransactionWithoutResult(TransactionStatus status) {
                    CardVendor vendor = new CardVendor();
                    vendor.setFormalName("name");
                    vendor.setMailAddress("address");
                    vendor.setEmailAddress("email");
                    vendor.setRegion("region");
                    cardVendorRepo.saveAndFlush(vendor);

                    GiftCardOrder order = new GiftCardOrder();
                    order.setVendor(vendor);
                    order.setStatus(GiftCardOrderStatus.APPROVED);
                    giftCardOrderRepo.saveAndFlush(order);

                    for (int i = 1; i <= 3; i++) {
                        GiftCardInventory inventory = new GiftCardInventory();
                        inventory.setProfile(productProfile);
                        inventory.setOrder(order);
                        inventory.setProductName(productProfile.getProductDesc());
                        inventory.setProductCode(productProfile.getProductCode());
                        inventory.setSeqNo(i);
                        inventory.setBatchNo(i);
                        inventory.setOrderDate(LocalDate.now());
                        inventory.setStatus(GiftCardInventoryStatus.IN_STOCK);
                        inventory.setSeries(Long.valueOf(i));
                        inventory.setBarcode(String.valueOf(i));
                        inventory.setLocation(codePropertiesService.getDetailInvLocationHeadOffice());
                        giftCardInventoryRepo.save(inventory);
                    }

                    ProductProfile pp2 = new ProductProfile();
                    pp2.setProductCode(pp2Code);
                    pp2.setProductDesc("desc2");
                    pp2.setSafetyStocksEaMo("1,,,4,5,,,,,,,12");
                    pp2.setStatus(ProductProfileStatus.APPROVED);
                    productProfileRepo.save(pp2);
                }
            });

            DateTimeFormatter df = DateTimeFormat.forPattern("yyyy-MM-dd");
            List<GcInventorySafetyStockDto> list = safetyStockService.checkSafetyStock(df.parseLocalDate("2014-04-08"));
            assertEquals(2, list.size());

            GcInventorySafetyStockDto dto = list.get(0);
            assertEquals(3, dto.getInventoryCount());
            assertEquals(5, dto.getSafetyStockQty());
            assertEquals(productProfile.getProductCode(), dto.getProductCode());
            assertEquals("April", dto.getCurrMonth());
            assertEquals("May", dto.getNextMonth());

            dto = list.get(1);
            assertEquals(0, dto.getInventoryCount());
            assertEquals(5, dto.getSafetyStockQty());
            assertEquals(pp2Code, dto.getProductCode());
            assertEquals("April", dto.getCurrMonth());
            assertEquals("May", dto.getNextMonth());
        } finally {
            giftCardInventoryRepo.deleteAll();
            giftCardOrderRepo.deleteAll();
            cardVendorRepo.deleteAll();

        }
    }
}
