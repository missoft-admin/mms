package com.transretail.crm.common.web.converter;

import java.io.IOException;
import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;

import org.apache.commons.lang3.StringUtils;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;

/**
 *
 */
public class TimestampDeserializer extends JsonDeserializer<Timestamp> {
    private static final SimpleDateFormat FORMATTER = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

    @Override
    public Timestamp deserialize(JsonParser jp, DeserializationContext ctxt) throws IOException {
        if (StringUtils.isNotBlank(jp.getText())) {
            try {
                return new Timestamp(FORMATTER.parse(jp.getText()).getTime());
            } catch (ParseException e) {
                return null;
            }
        } else {
            return null;
        }
    }

}

