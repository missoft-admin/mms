package com.transretail.crm.report.template.impl;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.apache.commons.lang3.StringUtils;
import org.joda.time.LocalDate;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Service;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.google.common.collect.Sets;
import com.mysema.query.Tuple;
import com.mysema.query.group.GroupBy;
import com.mysema.query.jpa.JPASubQuery;
import com.mysema.query.jpa.impl.JPAQuery;
import com.mysema.query.types.ConstructorExpression;
import com.mysema.query.types.expr.BooleanExpression;
import com.mysema.query.types.expr.CaseBuilder;
import com.mysema.query.types.expr.NumberExpression;
import com.transretail.crm.common.reporting.jasper.ClasspathFileResolver;
import com.transretail.crm.common.reporting.jasper.DefaultJRProcessor;
import com.transretail.crm.common.reporting.jasper.JRProcessor;
import com.transretail.crm.common.util.DateUtil;
import com.transretail.crm.core.entity.enums.ReportType;
import com.transretail.crm.core.entity.lookup.LookupDetail;
import com.transretail.crm.core.service.CodePropertiesService;
import com.transretail.crm.core.service.LookupService;
import com.transretail.crm.giftcard.entity.QGiftCardCustomerProfile;
import com.transretail.crm.giftcard.entity.QReturnRecord;
import com.transretail.crm.giftcard.entity.QSalesOrder;
import com.transretail.crm.giftcard.entity.QSalesOrderItem;
import com.transretail.crm.giftcard.entity.QSalesOrderPaymentInfo;
import com.transretail.crm.giftcard.entity.SalesOrder;
import com.transretail.crm.giftcard.entity.SalesOrderPaymentInfo;
import com.transretail.crm.giftcard.entity.support.SalesOrderStatus;
import com.transretail.crm.giftcard.entity.support.SalesOrderType;
import com.transretail.crm.report.template.CommonReportFilter;
import com.transretail.crm.report.template.FilterField;
import com.transretail.crm.report.template.ReportDateFormatProvider;
import com.transretail.crm.report.template.ReportTemplate;

@Service("b2bCustomerPaymentControlReport")
public class B2BCustomerPaymentControlReport implements ReportTemplate, ReportDateFormatProvider {
	protected static final String TEMPLATE_NAME = "B2B Customer Payment Control Report";
    protected static final String REPORT_NAME = "reports/b2b_customer_payment_control_report.jasper";
    
    // filters
    protected static final String FILTER_CUSTOMER_NO = "customerNo";
    protected static final String FILTER_TXN_DATE_FROM = CommonReportFilter.DATE_FROM;
    protected static final String FILTER_TXN_DATE_TO = CommonReportFilter.DATE_TO;
    protected static final String FILTER_ORDER_NO = "orderNo";
    
	private static final DateTimeFormatter INPUT_DATE_PATTERN = DateTimeFormat.forPattern("dd-MM-yyyy");
    private static final DateTimeFormatter FULL_DATE_PATTERN = DateTimeFormat.forPattern("dd-MM-yyyy");
    
    private static final Logger LOGGER = LoggerFactory.getLogger(B2BCustomerPaymentControlReport.class);
    
    private static final String ADVANCED_ORDER_PREFIX = "BBADVMMS";
    
    @PersistenceContext
    private EntityManager em;
    @Autowired
    private MessageSource messageSource;
    @Autowired
    private LookupService lookupService;
    @Autowired
    private CodePropertiesService codePropertiesService;
    private static BigDecimal toBigDecimal(BigDecimal val) {
    	return (val == null) ? BigDecimal.ZERO : val;
    }
    
    private LocalDate parseDate(String dateInstance) {
    	return INPUT_DATE_PATTERN.parseLocalDate(dateInstance);
    }
    
    @Override
    public DateTimeFormatter getDefaultDateFormat() {
    	return FULL_DATE_PATTERN;
    }
    
    @Override
    public ReportType[] getReportTypes() {
    	return ReportType.values();
    }
    
    @Override
    public String getName() {
    	return TEMPLATE_NAME;
    }

    @Override
    public String getDescription() {
    	return messageSource.getMessage("gc.b2b.customer.payment.control.report.label", 
			(Object[]) null, LocaleContextHolder.getLocale());
    }
    
    @Override
    public boolean canView(Set<String> permissions) {
    	return true;
    }
    
    @Override
    public Set<FilterField> getFilters() {
		Set<FilterField> filters = Sets.newLinkedHashSet();
		Locale locale = LocaleContextHolder.getLocale();
		Object[] transactionDate = new Object[]{messageSource.getMessage("gc.b2b.customer.payment.control.report.label.transaction.date", 
			(Object[]) null, locale)};
		filters.add(FilterField.createDateField(FILTER_TXN_DATE_FROM,
			messageSource.getMessage("gc.b2b.customer.payment.control.report.label.from", transactionDate, locale),
			DateUtil.SEARCH_DATE_FORMAT, true));
		filters.add(FilterField.createDateField(FILTER_TXN_DATE_TO,
			messageSource.getMessage("gc.b2b.customer.payment.control.report.label.to", transactionDate, locale),
			DateUtil.SEARCH_DATE_FORMAT, true));
		QGiftCardCustomerProfile customerProfile = QGiftCardCustomerProfile.giftCardCustomerProfile;
		Map<String, String> customers = Maps.newHashMap();
		customers.put("", "");
		customers.putAll(new JPAQuery(em).from(customerProfile).orderBy(customerProfile.customerId.asc()).transform(GroupBy.groupBy(customerProfile.customerId).as(
			customerProfile.customerId.append(" - ").append(customerProfile.name))));
		filters.add(FilterField.createDropdownField(FILTER_CUSTOMER_NO, 
			messageSource.getMessage("gc.b2b.customer.payment.control.report.label.customer",
				transactionDate, locale), customers));
		
		Map<String, String> paymentTypes = Maps.newHashMap();
		paymentTypes.put("", "");
		for(LookupDetail dtl : lookupService.getActiveDetailsByHeaderCode(codePropertiesService.getHeaderGcSoPaymentMethod())) {
			paymentTypes.put(dtl.getCode(), dtl.getDescription());
		}
		filters.add(FilterField.createDropdownField("paymentType", 
			messageSource.getMessage("gc.b2b.customer.payment.control.report.label.paymenttype",
					transactionDate, locale), paymentTypes));
	
		return filters;
    }
    
    @Override
    public JRProcessor createJrProcessor(Map<String, String> map) {
		DefaultJRProcessor jrProcessor = new DefaultJRProcessor(REPORT_NAME);
		LocalDate dateFrom = parseDate(map.get(FILTER_TXN_DATE_FROM));
		LocalDate dateTo = parseDate(map.get(FILTER_TXN_DATE_TO));
		String customerNo = map.get(FILTER_CUSTOMER_NO);
		String orderNo = map.get(FILTER_ORDER_NO);
		String paymentType = map.get("paymentType");
		Map<String, Object> paramMap = prepareReportParameters(dateFrom, dateTo, customerNo, orderNo);
		paramMap.put("SUB_DATA_SOURCE", createDatasource(dateFrom, dateTo, customerNo, orderNo, paymentType));
		jrProcessor.addParameters(paramMap);
		jrProcessor.setFileResolver(new ClasspathFileResolver("reports"));
		return jrProcessor;
    }
    
    public List<ReportBean> createDatasource(LocalDate dateFrom, LocalDate dateTo, String customerNo, String orderNo, String paymentType) {
    	QSalesOrderPaymentInfo soPaymentInfo = QSalesOrderPaymentInfo.salesOrderPaymentInfo;
    	QSalesOrderItem soItem = QSalesOrderItem.salesOrderItem;
    	QSalesOrder salesOrder = QSalesOrder.salesOrder;
    	QReturnRecord returnRecord = QReturnRecord.returnRecord;
    	List<BooleanExpression> exprs = Lists.newArrayList();
    	
    	exprs.add(salesOrder.orderDate.between(dateFrom, dateTo));
    	exprs.add(salesOrder.status.eq(SalesOrderStatus.SOLD));
    	exprs.add(salesOrder.payments.isNotEmpty());
    	
    	if(StringUtils.isNotBlank(customerNo)) {
    		exprs.add(salesOrder.customer.customerId.eq(customerNo));
    	}
    	
    	if(StringUtils.isNotBlank(orderNo)) {
    		exprs.add(salesOrder.orderNo.eq(orderNo));
    	}
    	
    	if(StringUtils.isNotBlank(paymentType)) {
    		exprs.add(soPaymentInfo.paymentType.code.eq(paymentType));
    	}
    	JPASubQuery paymentQuery = new JPASubQuery().from(soPaymentInfo)
    		.where(soPaymentInfo.order.id.eq(salesOrder.id));
    	JPASubQuery saleQuery = new JPASubQuery().from(soItem)
    		.where(soItem.order.id.eq(salesOrder.id));
    	JPASubQuery returnQuery = new JPASubQuery().from(returnRecord)
    		.where(returnRecord.orderNo.orderNo.eq(salesOrder.orderNo));
    	
    	/*public SubreportBean(String customerNo, String customerName, String orderNo, 
    		LocalDate transactionDate, BigDecimal salesSum, BigDecimal paymentAmountSum,
    		BigDecimal returnAmount, BigDecimal refundAmount) {*/
    	
    	List<SubreportBean> subreportList = new JPAQuery(em).from(soPaymentInfo)
    			.leftJoin(soPaymentInfo.order, salesOrder)
    		.where(BooleanExpression.allOf(exprs.toArray(new BooleanExpression[exprs.size()])))
    		.groupBy(salesOrder.customer.customerId, salesOrder.customer.name, 
    			salesOrder.orderNo, salesOrder.orderDate, salesOrder.returnNo, salesOrder.id)
    		.orderBy(salesOrder.orderDate.asc())
    		.list(ConstructorExpression.create(SubreportBean.class, salesOrder.customer.customerId, 
				salesOrder.customer.name, salesOrder.orderNo, salesOrder.orderDate,
				saleQuery.unique(soItem.faceAmount.sum().add(soItem.printFee.sum())), 
				/*paymentQuery.unique(soPaymentInfo.paymentAmount.sum())*/soPaymentInfo.paymentAmount.sum(), //paymentQuery.unique(soPaymentInfo.paymentAmount.sum()),
				returnQuery.unique(returnRecord.returnAmount.sum()),
				returnQuery.unique(returnRecord.refundAmount.sum()))
		);
    	
    	Map<String, ReportBean> reportMap = Maps.newHashMap();
    	List<ReportBean> reportList = Lists.newArrayList();
    	
    	for(SubreportBean sub : subreportList) {
    		ReportBean bean = null;
    		if(reportMap.containsKey(sub.getCustomerNo())) {
    			bean = reportMap.get(sub.getCustomerNo());
    		}
    		else {
    			bean = new ReportBean(sub.getCustomerNo(), sub.getCustomerName());
    			reportMap.put(sub.getCustomerNo(), bean);
    			reportList.add(bean);
    		}
    		
    		bean.getList().add(sub);
    		sub.setSerial(bean.getList().size());
    		
    		bean.setReceivedAmount(bean.getReceivedAmount().add(sub.getReceivedAmount()));
			bean.setAmountReceivable(bean.getAmountReceivable().add(sub.getAmountReceivable()));
			bean.setAdvancedReceivedAmount(bean.getAdvancedReceivedAmount().add(sub.getAdvancedReceivedAmount()));
			bean.setAdvancedReceivedRefund(bean.getAdvancedReceivedRefund().add(sub.getAdvancedReceivedRefund()));
			bean.setAdvancedReceivedStrike(bean.getAdvancedReceivedStrike().add(sub.getAdvancedReceivedStrike()));
			bean.setOverReceivedAmount(bean.getOverReceivedAmount().add(sub.getOverReceivedAmount()));
			bean.setOverReceivedRefund(bean.getOverReceivedRefund().add(sub.getOverReceivedRefund()));
			bean.setOverReceivedStrike(bean.getOverReceivedStrike().add(sub.getOverReceivedStrike()));
    	}
    	
    	return reportList;
    	
    }
    
    private Map<String, Object> prepareReportParameters(LocalDate dateFrom, LocalDate dateTo, String customerNo, String orderNo) {
    	HashMap<String, Object> reportMap = Maps.newHashMap();
    	reportMap.put("DATE_FROM", dateFrom.toString(FULL_DATE_PATTERN));
    	reportMap.put("DATE_TO", dateTo.toString(FULL_DATE_PATTERN));
    	reportMap.put("CUSTOMER_NO", StringUtils.isNotBlank(customerNo) ? customerNo : "ALL");
    	reportMap.put("ORDER_NO", StringUtils.isNotBlank(orderNo) ? orderNo : "ALL");
    	return reportMap;
    }
    
    public static class ReportBean extends SubreportBean {
    	private List<SubreportBean> list;
    	
    	public ReportBean(String customerNo, String customerName) {
    		super(customerNo, customerName);
    		list = Lists.newArrayList();
    	}
    	
		public List<SubreportBean> getList() {
			return list;
		}
		public void setList(List<SubreportBean> list) {
			this.list = list;
		}
    }
    
    public static class SubreportBean {
    	private String customerNo;
    	private String customerName;
    	private Integer serial;
    	private String transactionDate;
    	private String orderNo;
    	private BigDecimal sales;
    	private BigDecimal receivedAmount;
    	private BigDecimal amountReceivable;
    	private BigDecimal advancedReceivedAmount;
    	private BigDecimal advancedReceivedStrike;
    	private BigDecimal advancedReceivedRefund;
    	private BigDecimal advancedReceivedBalance;
    	private BigDecimal overReceivedAmount;
    	private BigDecimal overReceivedStrike;
    	private BigDecimal overReceivedRefund;
    	private BigDecimal overReceivedBalance;
    	private BigDecimal balance;
    	private String returnNo;
    	
    	public SubreportBean() {
    		sales = BigDecimal.ZERO;
    		receivedAmount = BigDecimal.ZERO;
    		amountReceivable = BigDecimal.ZERO;
    		advancedReceivedAmount = BigDecimal.ZERO;
    		advancedReceivedBalance = BigDecimal.ZERO;
    		advancedReceivedRefund = BigDecimal.ZERO;
    		advancedReceivedStrike = BigDecimal.ZERO;
    		overReceivedAmount = BigDecimal.ZERO;
    		overReceivedBalance = BigDecimal.ZERO;
    		overReceivedRefund = BigDecimal.ZERO;
    		overReceivedStrike = BigDecimal.ZERO;
    	}
    	
    	public SubreportBean(String customerNo, String customerName) {
    		this();
    		this.customerNo = customerNo;
    		this.customerName = customerName;
    	}
    	
    	/*public SubreportBean(String customerNo, String customerName, String orderNo, 
        		LocalDate transactionDate, BigDecimal salesSum, BigDecimal paymentAmountSum,
        		BigDecimal returnAmount, BigDecimal refundAmount, String returnNo) {
        		this(customerNo, customerName);
        		this.returnNo = returnNo;
        		paymentAmountSum = toBigDecimal(paymentAmountSum).setScale(0);
        		salesSum = toBigDecimal(salesSum).setScale(0);
        		refundAmount = toBigDecimal(refundAmount).setScale(0);
        		returnAmount = toBigDecimal(returnAmount).setScale(0);

        		this.orderNo = orderNo != null ? orderNo : "";
        		this.sales = salesSum;
        		this.transactionDate = transactionDate.toString(FULL_DATE_PATTERN);
    			this.amountReceivable = refundAmount.add(returnAmount).setScale(0);
    			this.receivedAmount = paymentAmountSum;
        		
        		if(this.orderNo.startsWith(ADVANCED_ORDER_PREFIX)) {
        			this.advancedReceivedAmount = receivedAmount;
        			this.advancedReceivedStrike = returnAmount;
        			this.advancedReceivedRefund = refundAmount;
        			this.advancedReceivedBalance = this.advancedReceivedAmount
    					.subtract(this.advancedReceivedStrike).subtract(this.advancedReceivedRefund).setScale(0);
        		} else if(salesSum.compareTo(paymentAmountSum) < 0) {
        			this.overReceivedAmount = receivedAmount;
        			this.overReceivedStrike = returnAmount;
        			this.overReceivedRefund = refundAmount;
        			this.overReceivedBalance = this.overReceivedAmount
        		}
        		
        		if(salesSum.compareTo(paymentAmountSum) < 0) {
        			BigDecimal diff = paymentAmountSum.subtract(salesSum);
        			this.overReceivedAmount = diff.setScale(0);
        			this.overReceivedBalance = this.overReceivedAmount
    					.subtract(this.overReceivedStrike).subtract(this.overReceivedRefund).setScale(0);
        		}
        		
        		this.balance = receivedAmount.subtract(amountReceivable).setScale(0);
        	}*/
    	
    	
    	public SubreportBean(String customerNo, String customerName, String orderNo, 
    		LocalDate transactionDate, BigDecimal salesSum, BigDecimal paymentAmountSum,
    		BigDecimal returnAmount, BigDecimal refundAmount) {
    		this(customerNo, customerName);
    		salesSum = toBigDecimal(salesSum).setScale(0);
    		refundAmount = toBigDecimal(refundAmount).setScale(0);
    		returnAmount = toBigDecimal(returnAmount).setScale(0);
    		paymentAmountSum = toBigDecimal(paymentAmountSum).setScale(0);
    		this.orderNo = orderNo != null ? orderNo : "";
    		this.sales = salesSum;
    		this.transactionDate = transactionDate.toString(FULL_DATE_PATTERN);
			this.amountReceivable = returnAmount;
			this.receivedAmount = paymentAmountSum;
    		
    		if(this.orderNo.startsWith(ADVANCED_ORDER_PREFIX)) {
    			this.advancedReceivedAmount = paymentAmountSum;
    			this.advancedReceivedStrike = returnAmount;
    			this.advancedReceivedRefund = refundAmount;
    			BigDecimal advancedReceivedBalance = BigDecimal.ZERO;
    			if(paymentAmountSum.compareTo(BigDecimal.ZERO) != 0) {
    				advancedReceivedBalance = paymentAmountSum.subtract(returnAmount.add(refundAmount));
    			}
    			this.advancedReceivedBalance = advancedReceivedBalance.setScale(0);
    		} else if(salesSum.compareTo(paymentAmountSum) < 0) {
    			this.overReceivedAmount = paymentAmountSum;
    			this.overReceivedStrike = returnAmount;
    			this.overReceivedRefund = refundAmount;
    			BigDecimal overReceivedBalance = BigDecimal.ZERO;
    			if(paymentAmountSum.compareTo(BigDecimal.ZERO) != 0) {
    				overReceivedBalance = paymentAmountSum.subtract(returnAmount.add(refundAmount));
    			}
    			this.overReceivedBalance = overReceivedBalance;
    		}
    		
    		this.balance = this.advancedReceivedBalance.add(this.overReceivedBalance);
    	}
    	
		public Integer getSerial() {
			return serial;
		}
		public void setSerial(Integer serial) {
			this.serial = serial;
		}
		public String getTransactionDate() {
			return transactionDate;
		}
		public void setTransactionDate(String transactionDate) {
			this.transactionDate = transactionDate;
		}
		public String getOrderNo() {
			return orderNo;
		}
		public void setOrderNo(String orderNo) {
			this.orderNo = orderNo;
		}
		public BigDecimal getSales() {
			return sales;
		}
		public void setSales(BigDecimal sales) {
			this.sales = sales;
		}
		public BigDecimal getReceivedAmount() {
			return receivedAmount;
		}
		public void setReceivedAmount(BigDecimal receivedAmount) {
			this.receivedAmount = receivedAmount;
		}
		public BigDecimal getAmountReceivable() {
			return amountReceivable;
		}
		public void setAmountReceivable(BigDecimal amountReceivable) {
			this.amountReceivable = amountReceivable;
		}
		public BigDecimal getAdvancedReceivedAmount() {
			return advancedReceivedAmount;
		}
		public void setAdvancedReceivedAmount(BigDecimal advancedReceivedAmount) {
			this.advancedReceivedAmount = advancedReceivedAmount;
		}
		public BigDecimal getAdvancedReceivedStrike() {
			return advancedReceivedStrike;
		}
		public void setAdvancedReceivedStrike(BigDecimal advancedReceivedStrike) {
			this.advancedReceivedStrike = advancedReceivedStrike;
		}
		public BigDecimal getAdvancedReceivedRefund() {
			return advancedReceivedRefund;
		}
		public void setAdvancedReceivedRefund(BigDecimal advancedReceivedRefund) {
			this.advancedReceivedRefund = advancedReceivedRefund;
		}
		public BigDecimal getAdvancedReceivedBalance() {
			return advancedReceivedBalance;
		}
		public void setAdvancedReceivedBalance(BigDecimal advancedReceivedBalance) {
			this.advancedReceivedBalance = advancedReceivedBalance;
		}
		public BigDecimal getOverReceivedAmount() {
			return overReceivedAmount;
		}
		public void setOverReceivedAmount(BigDecimal overReceivedAmount) {
			this.overReceivedAmount = overReceivedAmount;
		}
		public BigDecimal getOverReceivedStrike() {
			return overReceivedStrike;
		}
		public void setOverReceivedStrike(BigDecimal overReceivedStrike) {
			this.overReceivedStrike = overReceivedStrike;
		}
		public BigDecimal getOverReceivedRefund() {
			return overReceivedRefund;
		}
		public void setOverReceivedRefund(BigDecimal overReceivedRefund) {
			this.overReceivedRefund = overReceivedRefund;
		}
		public BigDecimal getOverReceivedBalance() {
			return overReceivedBalance;
		}
		public void setOverReceivedBalance(BigDecimal overReceivedBalance) {
			this.overReceivedBalance = overReceivedBalance;
		}
		public BigDecimal getBalance() {
			return balance;
		}
		public void setBalance(BigDecimal balance) {
			this.balance = balance;
		}

		public String getCustomerNo() {
			return customerNo;
		}

		public void setCustomerNo(String customerNo) {
			this.customerNo = customerNo;
		}

		public String getCustomerName() {
			return customerName;
		}

		public void setCustomerName(String customerName) {
			this.customerName = customerName;
		}

		public String getReturnNo() {
			return returnNo;
		}

		public void setReturnNo(String returnNo) {
			this.returnNo = returnNo;
		}
    }
}
