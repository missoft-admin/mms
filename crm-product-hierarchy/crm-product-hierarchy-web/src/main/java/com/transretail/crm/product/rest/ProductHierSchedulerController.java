package com.transretail.crm.product.rest;

import java.text.ParseException;

import org.quartz.Scheduler;
import org.quartz.SchedulerException;
import org.quartz.TriggerKey;
import org.quartz.impl.triggers.CronTriggerImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import com.transretail.crm.product.bean.CronBean;
import com.transretail.crm.product.service.impl.ProductHierSchedulerTrigger;

/**
 * @author Allan G. Ramirez (agramirez@exist.com)
 */
@Controller
@RequestMapping("/producthier/scheduler")
public class ProductHierSchedulerController {
    @Autowired
    private Scheduler scheduler;

    @Transactional
    @ResponseStatus(HttpStatus.OK)
    @RequestMapping("/reschedule")
    public void reScheduler(@RequestBody CronBean bean) throws SchedulerException, ParseException {
        TriggerKey triggerKey = new TriggerKey(ProductHierSchedulerTrigger.TRIGGER_NAME, ProductHierSchedulerTrigger.TRIGGER_GROUP);
        CronTriggerImpl productHierJobTrigger = (CronTriggerImpl) scheduler.getTrigger(triggerKey);
        productHierJobTrigger.setCronExpression(bean.getCronExpression());
        scheduler.rescheduleJob(triggerKey, productHierJobTrigger);
    }

    @ResponseBody
    @RequestMapping("/getschedule")
    public String getCronExpression() throws SchedulerException {
        TriggerKey triggerKey = new TriggerKey(ProductHierSchedulerTrigger.TRIGGER_NAME, ProductHierSchedulerTrigger.TRIGGER_GROUP);
        CronTriggerImpl productHierJobTrigger = (CronTriggerImpl) scheduler.getTrigger(triggerKey);
        return productHierJobTrigger.getCronExpression();
    }
}