package com.transretail.crm.impl;

import junit.framework.Assert;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Configurable;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import com.transretail.crm.common.service.dto.request.PageSortDto;
import com.transretail.crm.common.service.dto.response.ResultList;
import com.transretail.crm.core.entity.lookup.LookupDetail;
import com.transretail.crm.core.entity.lookup.LookupHeader;
import com.transretail.crm.core.repo.LookupDetailRepo;
import com.transretail.crm.core.repo.LookupHeaderRepo;
import com.transretail.crm.core.service.CodePropertiesService;
import com.transretail.crm.loyalty.dto.LoyaltyAnnualFeeSchemeDto;
import com.transretail.crm.loyalty.dto.LoyaltyAnnualFeeSchemeSearchDto;
import com.transretail.crm.loyalty.entity.LoyaltyAnnualFeeScheme;
import com.transretail.crm.loyalty.entity.LoyaltyCardInventory;
import com.transretail.crm.loyalty.entity.ManufactureOrderTier;
import com.transretail.crm.loyalty.repo.LoyaltyAnnualFeeSchemeRepo;
import com.transretail.crm.loyalty.repo.LoyaltyCardInventoryRepo;
import com.transretail.crm.loyalty.repo.ManufactureOrderTierRepo;
import com.transretail.crm.loyalty.service.LoyaltyAnnualFeeService;



@Configurable
@Transactional
@ContextConfiguration(locations = "classpath*:/META-INF/spring/applicationContext*.xml")
@RunWith(SpringJUnit4ClassRunner.class)
public class LoyaltyAnnualFeeServiceTest {
	
	@Autowired
	LoyaltyAnnualFeeService annualFeeService;
	@Autowired
	LoyaltyAnnualFeeSchemeRepo schemeRepo;
	@Autowired
	CodePropertiesService codePropertiesService;
	@Autowired
	LookupHeaderRepo headerRepo;
	@Autowired
	LookupDetailRepo detailRepo;
	@Autowired
	ManufactureOrderTierRepo tierRepo;
	@Autowired
	LoyaltyCardInventoryRepo inventoryRepo;
	
	private LookupDetail cardType;
	private LookupDetail company;
	private Long id;
	
	@Before
	public void setup() {
		LookupHeader header = new LookupHeader();
		header.setCode(codePropertiesService.getHeaderCompany());
		header.setDescription("COMPANY");
		headerRepo.save(header);
		
		LookupDetail detail = new LookupDetail();
		detail.setCode("COMP001");
		detail.setDescription("COMPANY 1");
		detail.setHeader(header);
		company = detailRepo.save(detail);
		
		header = new LookupHeader();
		header.setCode(codePropertiesService.getHeaderMembershipTier());
		header.setDescription("CARD TYPE");
		headerRepo.save(header);
		
		detail = new LookupDetail();
		detail.setCode("CARD001");
		detail.setDescription("CARD TYPE 1");
		detail.setHeader(header);
		cardType = detailRepo.save(detail);
		
		LoyaltyAnnualFeeScheme scheme = new LoyaltyAnnualFeeScheme();
		scheme.setCompany(company);
		scheme.setCardType(cardType);
		scheme.setAnnualFee(100D);
		scheme.setReplacementFee(10D);
		id = schemeRepo.save(scheme).getId();
		
		ManufactureOrderTier tier = new ManufactureOrderTier();
		tier.setCompany(company);
		tier.setName(cardType);
		tierRepo.save(tier);
		
		LoyaltyCardInventory inventory = new LoyaltyCardInventory();
		inventory.setBarcode("010100000000");
		inventory.setProduct(tier);
		inventoryRepo.save(inventory);
	}
	
	@After
	public void teardown() {
		schemeRepo.deleteAll();
		detailRepo.deleteAll();
		headerRepo.deleteAll();
	}
	
	@Test
	public void testSaveScheme() {
		LoyaltyAnnualFeeSchemeDto schemeDto = new LoyaltyAnnualFeeSchemeDto();
		schemeDto.setCompany(company);
		schemeDto.setCardType(cardType);
		schemeDto.setAnnualFee(100D);
		schemeDto.setReplacementFee(10D);
		annualFeeService.saveScheme(schemeDto);
		Assert.assertTrue(schemeRepo.count() == 2);
	}
	
	@Test
	public void testUpdateScheme() {
		LoyaltyAnnualFeeSchemeDto schemeDto = new LoyaltyAnnualFeeSchemeDto();
		schemeDto.setId(id);
		schemeDto.setCompany(company);
		schemeDto.setCardType(cardType);
		schemeDto.setAnnualFee(200D);
		schemeDto.setReplacementFee(20D);
		annualFeeService.updateScheme(schemeDto);
		
		Assert.assertTrue(schemeRepo.count() == 1);
		LoyaltyAnnualFeeScheme updated = schemeRepo.findOne(id);
		Assert.assertTrue(updated.getAnnualFee() == 200D);
		Assert.assertTrue(updated.getReplacementFee() == 20D);
	}
	
	@Test
	public void testListSchemes() {
		ResultList<LoyaltyAnnualFeeSchemeDto> schemes = annualFeeService.listSchemes(new LoyaltyAnnualFeeSchemeSearchDto());
		Assert.assertEquals(schemes.getNumberOfElements(), 1);
	}
	
	@Test
	public void testGetScheme() {
		LoyaltyAnnualFeeSchemeDto schemeDto = annualFeeService.getScheme(id);
		Assert.assertNotNull(schemeDto);
		Assert.assertTrue(schemeDto.getAnnualFee() == 100D);
		Assert.assertTrue(schemeDto.getReplacementFee() == 10D);
		
		LoyaltyAnnualFeeScheme scheme = annualFeeService.getScheme(company, cardType);
		Assert.assertNotNull(scheme);
		Assert.assertTrue(scheme.getAnnualFee() == 100D);
		Assert.assertTrue(scheme.getReplacementFee() == 10D);
		
		schemeDto = annualFeeService.getScheme("010100000000");
		Assert.assertNotNull(schemeDto);
		Assert.assertTrue(schemeDto.getAnnualFee() == 100D);
		Assert.assertTrue(schemeDto.getReplacementFee() == 10D);
	}
	
	@Test
	public void testDeleteScheme() {
		annualFeeService.deleteScheme(id);
		LoyaltyAnnualFeeScheme newScheme = schemeRepo.findOne(id);
		Assert.assertNull(newScheme);
	}
	
}
