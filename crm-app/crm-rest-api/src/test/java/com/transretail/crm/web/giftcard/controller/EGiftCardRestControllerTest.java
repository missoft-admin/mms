package com.transretail.crm.web.giftcard.controller;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.collect.Sets;
import com.transretail.crm.core.security.service.MD5Service;
import com.transretail.crm.core.service.CodePropertiesService;
import com.transretail.crm.core.service.LookupService;
import com.transretail.crm.core.util.generator.QrCodeService;
import com.transretail.crm.giftcard.GiftCardErrorCode;
import com.transretail.crm.giftcard.GiftCardServiceResolvableException;
import com.transretail.crm.giftcard.dto.EGiftCardProfileDto;
import com.transretail.crm.giftcard.dto.GiftCardTransactionInfo;
import com.transretail.crm.giftcard.service.EGiftCardManager;
import com.transretail.crm.giftcard.service.GiftCardInventoryService;
import com.transretail.crm.giftcard.service.GiftCardManager;
import com.transretail.crm.media.service.MailService;
import com.transretail.crm.media.service.SmsService;
import com.transretail.crm.web.BaseMvcConfig;
import com.transretail.crm.web.controller.AbstractMockMvcTest;
import com.transretail.crm.web.giftcard.controller.dto.EGiftCardPreactivateRequest;
import com.transretail.crm.web.giftcard.controller.dto.EGiftCardRedeemRequest;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.Set;
import org.hamcrest.Matchers;
import org.junit.Test;
import org.junit.runner.RunWith;

import static org.mockito.Mockito.*;
import static org.mockito.Matchers.*;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.task.AsyncTaskExecutor;
import org.springframework.http.MediaType;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.AnnotationConfigWebContextLoader;
import org.springframework.test.context.web.WebAppConfiguration;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

/**
 *
 * @author Monte Cillo Co (mco@exist.com)
 */
@RunWith(SpringJUnit4ClassRunner.class)
@WebAppConfiguration
@ContextConfiguration(loader = AnnotationConfigWebContextLoader.class)
public class EGiftCardRestControllerTest extends AbstractMockMvcTest {

    @Autowired
    private EGiftCardRestController sut;
    @Autowired
    private GiftCardRestController delegateController;
    @Autowired
    private ObjectMapper mapper;
    @Autowired
    private MD5Service mD5Service;
    @Autowired
    private EGiftCardManager emanager;

    @Test
    public void inquire_validateResponse() throws Exception {
        Set<EGiftCardProfileDto> inventory = Sets.newHashSet();
        EGiftCardProfileDto p100K = new EGiftCardProfileDto("312321", "Carrefour Voucher 100K", BigDecimal.valueOf(100000), 10l);
        EGiftCardProfileDto p200K = new EGiftCardProfileDto("312332", "Carrefour Voucher 200K", BigDecimal.valueOf(200000), 20l);
        inventory.add(p100K);
        inventory.add(p200K);

        when(emanager.retrieveAvailableProfiles(anyString()))
                .thenReturn(inventory);

        mockMvc.perform(get("/giftcard/egc/products/inquire/20001"))
                .andDo(print())
                .andExpect(jsonPath("$.availableProducts", Matchers.is(Matchers.iterableWithSize(2))))
                .andExpect(jsonPath("$..productCode", Matchers.contains(
                                        Matchers.equalTo("312321"),
                                        Matchers.equalTo("312332"))))
                .andExpect(jsonPath("$..productName", Matchers.contains(
                                        Matchers.equalTo("Carrefour Voucher 100K"),
                                        Matchers.equalTo("Carrefour Voucher 200K"))))
                .andExpect(jsonPath("$..faceValue", Matchers.contains(
                                        Matchers.equalTo(100000),
                                        Matchers.equalTo(200000))))
                .andExpect(jsonPath("$..quantity", Matchers.contains(
                                        Matchers.equalTo(10),
                                        Matchers.equalTo(20))));

        verify(emanager).retrieveAvailableProfiles(anyString());
    }

    @Test
    public void preactivate_overMaxQuantity() throws Exception {
        EGiftCardPreactivateRequest request = new EGiftCardPreactivateRequest();
        request.setTerminalId("terminalId");
        request.setMerchantId("20001");
        request.setCashierId("cashier");
        final String productCode = "9090000-121-10";
        request.setProductCode(productCode);
        request.setValidationKey("a-valid-validation-key");
        final long quantity = 55l;
        request.setQuantity(quantity);

        when(mD5Service.validateMd5(anyString(), anyString())).thenReturn(Boolean.TRUE);
        when(emanager.preactivate(any(GiftCardTransactionInfo.class), eq(productCode), eq(quantity)))
                .thenThrow(GiftCardServiceResolvableException.maximumQuantityReached(50l));
        mockMvc.perform(post("/giftcard/egc/pre-activate")
                .contentType(MediaType.APPLICATION_JSON)
                .content(convertObjectToJsonString(request)))
                .andDo(print())
                .andExpect(status().isUnprocessableEntity())
                .andExpect(jsonPath("$.errorCode",
                                Matchers.equalTo(GiftCardErrorCode.EGC_MAX_REQUESTED_QUANTITY.getErrorCode())));

        verify(emanager).preactivate(any(GiftCardTransactionInfo.class), eq(productCode), eq(quantity));
    }


    @Test
    public void redeem_shouldValidateRequiredFields() throws Exception {
        EGiftCardRedeemRequest request = new EGiftCardRedeemRequest();
        when(mD5Service.validateMd5(anyString(), anyString())).thenReturn(Boolean.TRUE);

        mockMvc.perform(post("/giftcard/egc/redeem")
                .contentType(MediaType.APPLICATION_JSON)
                .content(convertObjectToJsonString(request))).andDo(print())
                .andExpect(status().isUnprocessableEntity())
                .andExpect(jsonPath("$.errorCode", Matchers.equalTo("2000")))
                .andExpect(jsonPath("$.errorMessage", Matchers.equalTo("Validation Error")))
                // check if the required fields was thrown an exception
                .andExpect(jsonPath("$.fieldErrors[?(@.message=='may not be empty')].field",
                                Matchers.containsInAnyOrder(
                                        Matchers.equalTo("merchantId"),
                                        Matchers.equalTo("terminalId"),
                                        Matchers.equalTo("validationKey"),
                                        Matchers.equalTo("pin"),
                                        Matchers.equalTo("cashierId"),
                                        Matchers.equalTo("transactionNo"))))
                .andExpect(jsonPath("$.fieldErrors[?(@.message=='may not be null')].field",
                                Matchers.containsInAnyOrder(
                                        Matchers.equalTo("transactionDate"),
                                        Matchers.equalTo("redemptionAmount"))));
    }

    private byte[] convertObjectToJsonBytes(Object object) throws IOException {
        mapper.setSerializationInclusion(JsonInclude.Include.NON_NULL);

        return mapper.writeValueAsBytes(object);
    }

    private String convertObjectToJsonString(Object object) throws IOException {
        mapper.setSerializationInclusion(JsonInclude.Include.NON_NULL);
        return mapper.writerWithDefaultPrettyPrinter().writeValueAsString(object);
    }

    @Configuration
    static class Config extends BaseMvcConfig {

        @Bean
        public MessageSource messageSource() {
            return mock(MessageSource.class);
        }

        @Bean
        public CodePropertiesService codePropertiesService() {
            return mock(CodePropertiesService.class);
        }

        @Bean
        public LookupService lookupService() {
            return mock(LookupService.class);
        }

        @Bean
        public EGiftCardRestController eGiftCardRestController() {
            return new EGiftCardRestController();
        }

        @Bean
        public GiftCardRestController giftCardRestController() {
            return mock(GiftCardRestController.class);
        }

        @Bean
        public GiftCardManager giftCardManager() {
            return mock(GiftCardManager.class);
        }

        @Bean
        public EGiftCardManager egiftCardManager() {
            return mock(EGiftCardManager.class);
        }

        @Bean
        public GiftCardInventoryService giftCardInventoryService() {
            return mock(GiftCardInventoryService.class);
        }

        @Bean
        public MailService mailService() {
            return mock(MailService.class);
        }

        @Bean
        public SmsService smsService() {
            return mock(SmsService.class);
        }

        @Bean
        public AsyncTaskExecutor asyncTaskExecutor() {
            return mock(AsyncTaskExecutor.class);
        }

        @Bean
        public QrCodeService qrCodeService() {
            return mock(QrCodeService.class);
        }

        @Bean
        public MD5Service md5Service() {
            return mock(MD5Service.class);
        }
    }
}
