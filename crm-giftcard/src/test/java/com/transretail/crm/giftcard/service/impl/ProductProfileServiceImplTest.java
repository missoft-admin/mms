package com.transretail.crm.giftcard.service.impl;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.util.List;

import org.junit.After;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.TransactionCallbackWithoutResult;
import org.springframework.transaction.support.TransactionTemplate;

import com.transretail.crm.giftcard.dto.PrepaidReloadInfoDto;
import com.transretail.crm.giftcard.dto.ProductProfileDto;
import com.transretail.crm.giftcard.entity.PrepaidReloadInfo;
import com.transretail.crm.giftcard.entity.ProductProfile;
import com.transretail.crm.giftcard.entity.QProductProfile;
import com.transretail.crm.giftcard.repo.ProductProfileRepo;
import com.transretail.crm.giftcard.service.ProductProfileService;

/**
 * @author Allan G. Ramirez (agramirez@exist.com)
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath*:/META-INF/spring/applicationContext*.xml",
    "classpath:META-INF/spring/applicationContext-test.xml"})
public class ProductProfileServiceImplTest {
    @Autowired
    private PlatformTransactionManager transactionManager;
    @Autowired
    private ProductProfileRepo productProfileRepo;
    @Autowired
    private ProductProfileService productProfileService;

    @After
    public void tearDown() {
        productProfileRepo.deleteAll();
    }

    @Test
    public void saveDto() {
        final String productCode = "code";
        ProductProfileDto profileDto = new ProductProfileDto();

        PrepaidReloadInfoDto reloadInfoDto = new PrepaidReloadInfoDto();
        profileDto.setProductCode(productCode);
        reloadInfoDto.setMonthEffective(1);
        reloadInfoDto.setReloadAmount(1000l);
        profileDto.setAllowReload(true);
        profileDto.getReloadInfoDtos().add(reloadInfoDto);

        productProfileService.saveDto(profileDto);

        TransactionTemplate transactionTemplate = new TransactionTemplate(transactionManager);
        transactionTemplate.execute(new TransactionCallbackWithoutResult() {
            @Override
            protected void doInTransactionWithoutResult(TransactionStatus status) {
                QProductProfile qProductProfile = QProductProfile.productProfile;
                ProductProfile savedPp = productProfileRepo.findOne(qProductProfile.productCode.eq(productCode));
                assertEquals(productCode, savedPp.getProductCode());
                List<PrepaidReloadInfo> reloadInfoList = savedPp.getPrepaidReloadInfos();
                assertNotNull(reloadInfoList);
                assertEquals(1, reloadInfoList.size());
                assertEquals(1, reloadInfoList.get(0).getMonthEffective());
                assertEquals(1000l, reloadInfoList.get(0).getReloadAmount().longValue());
            }
        });
    }
}
