package com.transretail.crm.core.entity.enums;

public enum TxnStatusSearch {
    FORAPPROVAL, REJECTED, ACTIVE
}
