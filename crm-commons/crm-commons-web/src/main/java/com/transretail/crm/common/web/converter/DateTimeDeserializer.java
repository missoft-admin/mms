package com.transretail.crm.common.web.converter;

import java.io.IOException;

import org.apache.commons.lang3.StringUtils;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;

/**
 *
 */
public class DateTimeDeserializer extends JsonDeserializer<DateTime> {
    private static final DateTimeFormatter DATEWITHTIME = DateTimeFormat.forPattern("dd-MM-yyyy hh:mm:ss aa");
    private static final DateTimeFormatter DATEONLY = DateTimeFormat.forPattern("dd-MM-yyyy");

    @Override
    public DateTime deserialize(JsonParser jp, DeserializationContext ctxt) throws IOException {
        if (StringUtils.isNotBlank(jp.getText())) {
            try {
                return DATEWITHTIME.parseDateTime(jp.getText());
            } catch (IllegalArgumentException e) {
                // Try without time
                return DATEONLY.parseDateTime(jp.getText());
            }
        } else {
            return null;
        }
    }
}
