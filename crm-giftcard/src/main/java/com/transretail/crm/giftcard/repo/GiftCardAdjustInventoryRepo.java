package com.transretail.crm.giftcard.repo;

import com.transretail.crm.common.repo.CrmQueryDslPredicateExecutor;
import com.transretail.crm.giftcard.entity.GiftCardPhysicalCount;
import com.transretail.crm.giftcard.repo.custom.GiftCardAdjustInventoryRepoCustom;
import org.springframework.stereotype.Repository;

/**
 *
 * @author Monte Cillo Co (mco@exist.com)
 */
@Repository
public interface GiftCardAdjustInventoryRepo
	extends CrmQueryDslPredicateExecutor<GiftCardPhysicalCount, Long>,
	GiftCardAdjustInventoryRepoCustom {
}
