package com.transretail.crm.rest.store;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;
import java.util.concurrent.locks.ReentrantLock;
import java.util.regex.Pattern;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.joda.time.LocalDate;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.transretail.crm.common.web.dto.LogFilterDto;

/**
 * @author Allan G. Ramirez (agramirez@exist.com)
 */
@Controller
@RequestMapping("/log")
// TODO: Removed hardcoded filename prefix, filename pattern and date format by parsing LOG_FILE appender file and rollingPolicy.fileNamePattern properties
public class LogRestController implements InitializingBean {
    private static final DateTimeFormatter DATE_FORMATTER = DateTimeFormat.forPattern("yyyy-MM-dd");
    protected static final String LOG_PREFIX = "crm-pos-api";
    private static final Logger _LOG = LoggerFactory.getLogger(LogRestController.class);
    private static final Map<String, ReentrantLock> LOCKS = new HashMap<String, ReentrantLock>();

    @Value("#{'${project.version}'}")
    protected String projectVersion;

    protected File crmProxyLogBaseDir;

    @Value("#{'${crm.proxy.log.directory}'}")
    private String crmProxyLogDir;

    @Override
    public void afterPropertiesSet() throws Exception {
        crmProxyLogBaseDir = new File(crmProxyLogDir);
    }

    @RequestMapping(value = "/search", method = RequestMethod.POST)
    public void searchLogs(@RequestBody LogFilterDto dto, HttpServletResponse response) throws IOException {
        File file = getLogFile(dto.getDate());
        if (file != null) {
            File temp = File.createTempFile("log-" + System.currentTimeMillis(), ".tmp");
            FileWriter tempFileWriter = new FileWriter(temp);
            try {
                ReentrantLock lock = null;
                synchronized (LOCKS) {
                    lock = LOCKS.get(file.getAbsolutePath());
                    if (lock == null) {
                        lock = new ReentrantLock();
                        LOCKS.put(file.getAbsolutePath(), lock);
                    }
                }
                lock.lock();
                try {
                    parseFile(dto, file, tempFileWriter);
                } finally {
                    lock.unlock();
                    synchronized (LOCKS) {
                        if (!lock.isLocked()) {
                            LOCKS.remove(file.getAbsolutePath());
                        }
                    }
                }
                tempFileWriter.close();
                InputStream fis = new FileInputStream(temp);
                ServletOutputStream out = response.getOutputStream();
                IOUtils.copy(fis, out);
                out.flush();
            } finally {
                tempFileWriter.close();
                temp.delete();
            }
        }
    }

    @RequestMapping(value = "/filters/found", method = RequestMethod.POST)
    @ResponseBody
    public boolean logsContains(@RequestBody LogFilterDto dto) throws IOException {
        boolean found = false;
        File file = getLogFile(dto.getDate());
        if (file != null) {
            ReentrantLock lock = null;
            synchronized (LOCKS) {
                lock = LOCKS.get(file.getAbsolutePath());
                if (lock == null) {
                    lock = new ReentrantLock();
                    LOCKS.put(file.getAbsolutePath(), lock);
                }
            }
            lock.lock();
            try {
                found = hasLine(dto, file);
            } finally {
                lock.unlock();
                synchronized (LOCKS) {
                    if (!lock.isLocked()) {
                        LOCKS.remove(file.getAbsolutePath());
                    }
                }
            }
        }
        return found;
    }


    private File getLogFile(LocalDate date) {
        File result = null;
        String datePattern = "";
        if (LocalDate.now().equals(date)) {
            datePattern = projectVersion;
        } else {
            datePattern = DATE_FORMATTER.print(date);
        }

        for (File file : crmProxyLogBaseDir.listFiles()) {
            String name = file.getName();
            if (name.startsWith(LOG_PREFIX)) {
                if (name.contains(datePattern)) {
                    result = file;
                    break;
                }
            } else {
                _LOG.warn("[Log Parser] File {} does not start with {}", file.getAbsolutePath(), LOG_PREFIX);
            }
        }
        return result;
    }

    private void parseFile(LogFilterDto dto, File file, FileWriter output) throws IOException {
        if (file.getName().endsWith(".zip")) {
            ZipFile zipFile = new ZipFile(file);
            Enumeration<? extends ZipEntry> entries = zipFile.entries();
            while (entries.hasMoreElements()) {
                ZipEntry entry = entries.nextElement();
                InputStream is = zipFile.getInputStream(entry);
                try {
                    parseFile(dto, is, output);
                } finally {
                    IOUtils.closeQuietly(is);
                }
            }
        } else {
            // Assumed to be text file - eg .log
            InputStream is = new FileInputStream(file);
            try {
                parseFile(dto, is, output);
            } finally {
                IOUtils.closeQuietly(is);
            }
        }
    }

    private void parseFile(LogFilterDto dto, InputStream is, FileWriter output) throws IOException {
        Scanner scanner = new Scanner(is);
        String[] filters = dto.getFilters();
        Pattern pattern = StringUtils.isNotBlank(dto.getRegexFilter()) ? Pattern.compile(dto.getRegexFilter()) : null;
        while (scanner.hasNext()) {
            String line = scanner.nextLine();
            boolean matched = false;
            if (filters != null) {
                for (String filter : filters) {
                    if (line.toLowerCase().contains(filter.trim().toLowerCase())) {
                        matched = true;
                        break;
                    }
                }
            }
            if (pattern != null && !matched) {
                matched = pattern.matcher(line).matches();
            }
            if (matched) {
                output.write(line + "\r\n");
            }
        }
    }

    private boolean hasLine(LogFilterDto dto, File file) throws IOException {
        boolean result = false;
        if (file.getName().endsWith(".zip")) {
            ZipFile zipFile = new ZipFile(file);
            Enumeration<? extends ZipEntry> entries = zipFile.entries();
            while (entries.hasMoreElements()) {
                ZipEntry entry = entries.nextElement();
                InputStream is = zipFile.getInputStream(entry);
                try {
                    result = hasLine(dto, is);
                    if (result) {
                        break;
                    }
                } finally {
                    IOUtils.closeQuietly(is);
                }
            }
        } else {
            // Assumed to be text file - eg .log
            InputStream is = new FileInputStream(file);
            try {
                result = hasLine(dto, is);
            } finally {
                IOUtils.closeQuietly(is);
            }
        }
        return result;
    }

    private boolean hasLine(LogFilterDto dto, InputStream is) throws IOException {
        boolean matched = false;
        Scanner scanner = new Scanner(is);
        String[] filters = dto.getFilters();
        Pattern pattern = StringUtils.isNotBlank(dto.getRegexFilter()) ? Pattern.compile(dto.getRegexFilter()) : null;
        while (scanner.hasNext()) {
            String line = scanner.nextLine();
            if (filters != null) {
                for (String filter : filters) {
                    if (line.toLowerCase().contains(filter.trim().toLowerCase())) {
                        matched = true;
                        break;
                    }
                }
            }
            if (pattern != null && !matched) {
                matched = pattern.matcher(line).matches();
            }
            if (matched) {
                break;
            }
        }
        return matched;
    }
}
