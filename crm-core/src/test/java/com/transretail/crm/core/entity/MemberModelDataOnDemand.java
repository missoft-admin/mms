package com.transretail.crm.core.entity;

import com.transretail.crm.core.entity.embeddable.CustomerProfile;
import com.transretail.crm.core.entity.enums.MemberStatus;
import com.transretail.crm.core.entity.enums.Status;
import com.transretail.crm.core.entity.lookup.LookupDetail;
import com.transretail.crm.core.entity.lookup.LookupHeader;
import com.transretail.crm.core.repo.LookupDetailRepo;
import com.transretail.crm.core.repo.LookupHeaderRepo;
import com.transretail.crm.core.repo.MemberRepo;
import com.transretail.crm.core.repo.MemberTypeRepo;
import com.transretail.crm.core.service.MemberService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Configurable;
import org.springframework.stereotype.Component;

import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;
import java.security.SecureRandom;
import java.util.*;

@Configurable
@Component
public class MemberModelDataOnDemand {
    @Autowired
    MemberService memberService;
    @Autowired
    MemberRepo memberRepo;
    
    @Autowired
    MemberTypeRepo memberTypeRepo;
    
    @Autowired
	LookupHeaderRepo lookupHeaderRepo;
	
	@Autowired
	LookupDetailRepo lookupDetailRepo;
    
    private Random rnd = new SecureRandom();
    private List<MemberModel> data;

    static String employeeType  = "MTYP002";

    static String individualType  = "MTYP001";

    public MemberModel getNewTransientCustomerModel(int index) {
        MemberModel obj = new MemberModel();
        setEmail(obj, index);
        setEnabled(obj, index);
        setFirstName(obj, index);
        setLastName(obj, index);
        setMiddleName(obj, index);
        setPassword(obj, index);
        setUsername(obj, index);
        setContact(obj, index);
        setPin(obj, index);
        CustomerProfile customerProfile = new CustomerProfile();
        customerProfile.setBirthdate(new Date());
        obj.setCustomerProfile(customerProfile);
        obj.setAccountStatus(MemberStatus.ACTIVE);
        obj.setAccountId(String.valueOf(index));
        obj.setMemberType(new LookupDetail(individualType));
        return obj;
    }
    
    public void setPin(MemberModel obj, int index) {
        obj.setPin("1");
    }

    public void setContact(MemberModel obj, int index) {
        String contact = "contact" + index;
        obj.setContact(contact);
    }

    public void setEmail(MemberModel obj, int index) {
        String email = "foo" + index + "@bar.com";
        obj.setEmail(email);
    }

    public void setEnabled(MemberModel obj, int index) {
        Boolean enabled = Boolean.TRUE;
        obj.setEnabled(enabled);
    }

    public void setFirstName(MemberModel obj, int index) {
        String firstName = "firstName_" + index;
        obj.setFirstName(firstName);
    }

    public void setLastName(MemberModel obj, int index) {
        String lastName = "lastName_" + index;
        obj.setLastName(lastName);
    }

    public void setMiddleName(MemberModel obj, int index) {
        String middleName = "middleName_" + index;
        obj.setMiddleName(middleName);
    }

    public void setPassword(MemberModel obj, int index) {
        String password = "password_" + index;
        obj.setPassword(password);
    }

    public void setUsername(MemberModel obj, int index) {
        String username = "username_" + index;
        obj.setUsername(username);
    }

    public MemberModel getSpecificCustomerModel(int index) {
        init();
        if (index < 0) {
            index = 0;
        }
        if (index > (data.size() - 1)) {
            index = data.size() - 1;
        }
        MemberModel obj = data.get(index);
        Long id = obj.getId();
        return memberService.findCustomerModel(id);
    }

    public MemberModel getRandomCustomerModel() {
        init();
        MemberModel obj = data.get(rnd.nextInt(data.size()));
        Long id = obj.getId();
        return memberService.findCustomerModel(id);
    }

    public boolean modifyCustomerModel(MemberModel obj) {
        return false;
    }

    public void init() {
        int from = 0;
        int to = 10;
        data = memberService.findCustomerModelEntries(from, to);
        if (data == null) {
            throw new IllegalStateException("Find entries implementation for 'MemberModel' illegally returned null");
        }
        if (!data.isEmpty()) {
            return;
        }
        
        LookupHeader header = new LookupHeader();
    	header.setCode("membertype");
    	header.setDescription("membertype");
    	header = lookupHeaderRepo.saveAndFlush(header);
    	
    	LookupDetail theMemberType = new LookupDetail();
    	theMemberType.setHeader(header);
    	theMemberType.setCode(individualType);
    	theMemberType.setDescription(individualType);
    	theMemberType.setStatus(Status.ACTIVE);
    	theMemberType = lookupDetailRepo.saveAndFlush(theMemberType);

        data = new ArrayList<MemberModel>();
        for (int i = 0; i < 10; i++) {
            MemberModel obj = getNewTransientCustomerModel(i);
            try {
                memberService.saveCustomerModel(obj);
            } catch (final ConstraintViolationException e) {
                final StringBuilder msg = new StringBuilder();
                for (Iterator<ConstraintViolation<?>> iter = e.getConstraintViolations().iterator(); iter.hasNext(); ) {
                    final ConstraintViolation<?> cv = iter.next();
                    msg.append("[").append(cv.getRootBean().getClass().getName()).append(".").append(cv.getPropertyPath()).append(": ").append(cv.getMessage()).append(" (invalid value = ").append(cv.getInvalidValue()).append(")").append("]");
                }
                throw new IllegalStateException(msg.toString(), e);
            }
            memberRepo.flush();
            data.add(obj);
        }
    }
}
