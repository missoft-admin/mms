package com.transretail.crm.template.service.streaming.excel.handler.cell;

import java.util.Date;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;
import org.apache.poi.ss.usermodel.DateUtil;
import org.springframework.context.i18n.LocaleContextHolder;

import com.transretail.crm.core.entity.MemberModel;
import com.transretail.crm.core.entity.embeddable.CustomerProfile;
import com.transretail.crm.template.service.MemberTemplateParseException;
import com.transretail.crm.template.service.streaming.excel.handler.sheet.AbstractSheetHandler;

/**
 * @author Allan G. Ramirez (agramirez@exist.com)
 */
public class BirthDateCellHandler implements CellHandler {
    private AbstractSheetHandler sheetHandler;

    public BirthDateCellHandler(AbstractSheetHandler sheetHandler) {
        this.sheetHandler = sheetHandler;
    }

    @Override
    public void handle(MemberModel dto, String cellText) throws MemberTemplateParseException {
        if (dto.getCustomerProfile() == null) {
            dto.setCustomerProfile(new CustomerProfile());
        }
        Date birthdate = null;
        if (StringUtils.isNotBlank(cellText)) {
            if (NumberUtils.isDigits(cellText)) {
                birthdate = DateUtil.getJavaDate(Double.parseDouble(cellText));
            } else {
                try {
                    birthdate = sheetHandler.getDateTimeFormatter().parseLocalDate(cellText).toDate();
                } catch (IllegalArgumentException e) {
                    try {
                        birthdate = sheetHandler.getFallbackDateTimeFormatter().parseLocalDate(cellText).toDate();
                    } catch (IllegalArgumentException e2) {
                        throw new MemberTemplateParseException(sheetHandler.getMessageSource()
                            .getMessage("member.template.messages.excel.date.invalid",
                                new String[]{cellText, sheetHandler.getDatePattern() + " or " + sheetHandler.getFallbackDatePattern()},
                                LocaleContextHolder.getLocale()));
                    }
                }
            }
        }
        dto.getCustomerProfile().setBirthdate(birthdate);
    }
}
