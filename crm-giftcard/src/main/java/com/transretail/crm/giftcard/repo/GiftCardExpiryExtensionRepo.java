package com.transretail.crm.giftcard.repo;

import org.springframework.stereotype.Repository;

import com.transretail.crm.common.repo.CrmQueryDslPredicateExecutor;
import com.transretail.crm.giftcard.entity.GiftCardExpiryExtension;
import com.transretail.crm.giftcard.repo.custom.GiftCardExpiryExtensionRepoCustom;

/**
 * @author ftopico
 */
@Repository
public interface GiftCardExpiryExtensionRepo extends CrmQueryDslPredicateExecutor<GiftCardExpiryExtension, Long>, GiftCardExpiryExtensionRepoCustom {

}
