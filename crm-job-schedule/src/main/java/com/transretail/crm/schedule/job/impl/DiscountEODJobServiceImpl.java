package com.transretail.crm.schedule.job.impl;

import java.util.Date;

import org.joda.time.LocalDate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.transretail.crm.giftcard.service.DiscountSchemeService;
import com.transretail.crm.schedule.job.service.DiscountEODJobService;

@Service("discountEODJobSvc")
public class DiscountEODJobServiceImpl implements DiscountEODJobService {
	@Autowired
	public DiscountSchemeService discountService;
	private static final Logger LOGGER = LoggerFactory.getLogger(DiscountEODJobServiceImpl.class);
	
	@Override
	public void generateDiscVoucher() {
		LOGGER.error(":generateDiscVoucher:: job executing at " + new Date().toString());
		LocalDate eocDate = new LocalDate().minusDays(1);
		discountService.generateDiscVoucherForRegYearlyDisc(eocDate);
		discountService.generateDiscVoucherForSOYearlyDisc(eocDate);
		LOGGER.error(":generateDiscVoucher:: job successfully executed at " + new Date().toString());
	}
	
	
}
