package com.transretail.crm.giftcard.service.impl;


import java.math.BigDecimal;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.mysema.query.jpa.impl.JPAQuery;
import com.transretail.crm.giftcard.dto.SalesOrderDeptAllocDto;
import com.transretail.crm.giftcard.dto.SalesOrderDto;
import com.transretail.crm.giftcard.dto.SalesOrderMultDeptAllocsDto;
import com.transretail.crm.giftcard.entity.QSalesOrderDeptAlloc;
import com.transretail.crm.giftcard.entity.SalesOrder;
import com.transretail.crm.giftcard.entity.SalesOrderDeptAlloc;
import com.transretail.crm.giftcard.entity.SalesOrderDeptAlloc.DeptAllocStatus;
import com.transretail.crm.giftcard.repo.SalesOrderDeptAllocRepo;
import com.transretail.crm.giftcard.service.SalesOrderDeptService;
import com.transretail.crm.giftcard.service.SalesOrderService;


@Service("salesOrderDeptService")
public class SalesOrderDeptServiceImpl implements SalesOrderDeptService {
	@Autowired
	private SalesOrderDeptAllocRepo allocRepo;
	@Autowired
	private SalesOrderService orderService;
	@PersistenceContext
	private EntityManager em;
	
	@Override
	public SalesOrderMultDeptAllocsDto getAllocations(Long orderId) {
		SalesOrderDto orderDto = orderService.getOrder(orderId);
		SalesOrderMultDeptAllocsDto allocs = new SalesOrderMultDeptAllocsDto();
		allocs.setOrderId(orderDto.getId());
		allocs.setOrderNo(orderDto.getOrderNo());
		allocs.setOrderDate(orderDto.getOrderDate());
		allocs.setSalesOrderAmt(orderDto.getTotalFaceAmount());
		allocs.setCustomerName(orderDto.getCustomerDesc());
		allocs.setOrderStatus(orderDto.getStatus());
		
		QSalesOrderDeptAlloc qAlloc = QSalesOrderDeptAlloc.salesOrderDeptAlloc;
		Iterable<SalesOrderDeptAlloc> dptAllocs = allocRepo.findAll(qAlloc.order.id.eq(orderId));
		allocs.setDeptAllocs(SalesOrderDeptAllocDto.toDtos(dptAllocs));
		
		return allocs;
	}
	@Override
	@Transactional
	public void saveAllocations(SalesOrderMultDeptAllocsDto allocsDto) {
		deleteAllocBySo(allocsDto.getOrderId());
		Long orderId = allocsDto.getOrderId();
		DeptAllocStatus status = allocsDto.getStatus();
		for(SalesOrderDeptAllocDto allocDto: allocsDto.getDeptAllocs()) {
			allocDto.setOrderId(orderId);
			allocDto.setStatus(status);
			allocRepo.save(allocDto.toModel(null));
		}
	}
	@Override
	@Transactional
	public void deleteAllocBySo(Long orderId) {
		QSalesOrderDeptAlloc qAlloc = QSalesOrderDeptAlloc.salesOrderDeptAlloc;
		allocRepo.delete(allocRepo.findAll(qAlloc.order.id.eq(orderId)));
	}
	@Override
	@Transactional
	public void createEmptyAllocForRemaingAmt(Long orderId, BigDecimal totalSoAmt) {
		QSalesOrderDeptAlloc qAlloc = QSalesOrderDeptAlloc.salesOrderDeptAlloc;
		BigDecimal totalAllocAmt = new JPAQuery(em).from(qAlloc).where(qAlloc.order.id.eq(orderId)).singleResult(qAlloc.allocAmount.sum());
		BigDecimal remAmt = totalSoAmt;
		if(totalAllocAmt != null)
			remAmt = totalSoAmt.subtract(totalAllocAmt);
		
		if(remAmt.compareTo(BigDecimal.ZERO) > 0) {
			SalesOrderDeptAlloc alloc = new SalesOrderDeptAlloc();
			alloc.setAllocAmount(remAmt);
			alloc.setOrder(new SalesOrder(orderId));
			allocRepo.save(alloc);	
		}
		
	}
	
}
