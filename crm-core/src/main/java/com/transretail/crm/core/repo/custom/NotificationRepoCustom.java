package com.transretail.crm.core.repo.custom;

import java.util.List;

import com.transretail.crm.core.entity.Notification;
import com.transretail.crm.core.entity.UserModel;

public interface NotificationRepoCustom {
	List<Notification> getNotifications(UserModel user);
	List<Notification> getNotifications(UserModel user, int page, int size);
	List<Notification> getUnreadNotifications(UserModel user);
	List<Notification> getUnreadNotifications(UserModel user, int page, int size);
	boolean hasDuplicateNotification(Notification notification);
}
