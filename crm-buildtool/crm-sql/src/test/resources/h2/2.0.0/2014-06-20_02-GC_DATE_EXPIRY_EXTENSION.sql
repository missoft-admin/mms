create table CRM_GC_DATE_EXPIRY_EXTENSION (
    id bigint generated by default as identity,
    CREATED_BY varchar(255),
    CREATED_DATETIME timestamp,
    LAST_UPDATED_BY varchar(255),
    LAST_UPDATED_DATETIME timestamp,
    CREATE_DATE date,
    DATE_EXTENSION_TYPE varchar(255),
    EXTEND_NO varchar(255),
    FILED_BY varchar(255),
    SERIES bigint,
    STATUS varchar(255),
    ORDER_ID bigint,
    primary key (id)
);

alter table CRM_GC_DATE_EXPIRY_EXTENSION 
    add constraint FK_44wws7p36i81l7u7g2fam1652 
    foreign key (ORDER_ID) 
    references CRM_GC_ORD;