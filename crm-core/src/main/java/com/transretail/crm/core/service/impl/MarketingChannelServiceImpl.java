package com.transretail.crm.core.service.impl;


import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.IteratorUtils;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.BooleanUtils;
import org.apache.commons.lang3.EnumUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;
import org.joda.time.LocalDate;
import org.joda.time.LocalTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import com.google.common.collect.Lists;
import com.mysema.query.BooleanBuilder;
import com.mysema.query.jpa.JPQLQuery;
import com.mysema.query.jpa.impl.JPAQuery;
import com.mysema.query.types.expr.BooleanExpression;
import com.mysema.query.types.expr.StringExpression;
import com.transretail.crm.common.service.dto.response.ResultList;
import com.transretail.crm.common.service.util.SpringDataPagingUtil;
import com.transretail.crm.core.dto.ApprovalRemarkDto;
import com.transretail.crm.core.dto.MarketingChannelDto;
import com.transretail.crm.core.dto.MarketingChannelSearchDto;
import com.transretail.crm.core.dto.PromotionDto;
import com.transretail.crm.core.entity.ApplicationConfig;
import com.transretail.crm.core.entity.ApprovalRemark;
import com.transretail.crm.core.entity.CrmFile;
import com.transretail.crm.core.entity.MarketingChannel;
import com.transretail.crm.core.entity.MarketingChannel.ChannelStatus;
import com.transretail.crm.core.entity.MemberGroup;
import com.transretail.crm.core.entity.MemberModel;
import com.transretail.crm.core.entity.Promotion;
import com.transretail.crm.core.entity.PromotionMemberGroup;
import com.transretail.crm.core.entity.QMarketingChannel;
import com.transretail.crm.core.entity.QMemberModel;
import com.transretail.crm.core.entity.enums.AppKey;
import com.transretail.crm.core.entity.enums.ModelType;
import com.transretail.crm.core.entity.lookup.LookupDetail;
import com.transretail.crm.core.repo.ApplicationConfigRepo;
import com.transretail.crm.core.repo.ApprovalRemarkRepo;
import com.transretail.crm.core.repo.LookupDetailRepo;
import com.transretail.crm.core.repo.MarketingChannelRepo;
import com.transretail.crm.core.repo.MemberGroupRepo;
import com.transretail.crm.core.repo.MemberRepo;
import com.transretail.crm.core.repo.PromotionRepo;
import com.transretail.crm.core.service.CodePropertiesService;
import com.transretail.crm.core.service.CrmFileService;
import com.transretail.crm.core.service.MarketingChannelService;
import com.transretail.crm.core.service.MemberGroupService;
import com.transretail.crm.core.service.PromotionService;
import com.transretail.crm.core.util.generator.IdGeneratorService;


@Service("marketingChannelService")
@Transactional
public class MarketingChannelServiceImpl implements MarketingChannelService {

	private static final String DIR_CHANNEL = System.getProperty( "user.home" ) + "/crm-marketing-channel-email/";

	@Autowired
	MarketingChannelRepo repo;
	@Autowired
	ApprovalRemarkRepo approvalRemarkRepo;
	@Autowired
	LookupDetailRepo lookupDetailRepo;
	@Autowired
	MemberGroupRepo memberGroupRepo;
	@Autowired
	PromotionRepo promotionRepo;
	@Autowired
	MemberRepo memberRepo;
	@Autowired
	ApplicationConfigRepo applicationConfigRepo;
	@Autowired
	CrmFileService crmFileService;
	@Autowired
	CodePropertiesService codePropertiesService;
	@Autowired
	IdGeneratorService customIdGeneratorService;
	@Autowired
	MemberGroupService memberGroupService;
	@Autowired
	PromotionService promotionService;
	@Autowired
	private MessageSource messageSource;
    @PersistenceContext
    private EntityManager entityManager;



	@Override
	public MarketingChannelDto getDto( Long id ) {
		if ( null == id ) {
			return null;
		}

		MarketingChannelDto dto = new MarketingChannelDto( repo.findOne( id ) );
		if ( StringUtils.isNotBlank( dto.getMemberGroup() ) ) {
			for ( String strId : 
				( dto.getMemberGroup().indexOf( "," ) != -1 )? dto.getMemberGroup().split( "," ) : new String[]{ dto.getMemberGroup() } ) {
				MemberGroup memberGroup = memberGroupService.getMemberGroup( Long.valueOf( strId ) );
				if ( null != memberGroup ) {
					dto.setMemberGroupName( ( StringUtils.isNotBlank( dto.getMemberGroupName() )? dto.getMemberGroupName() + ", " : "" )
							 + memberGroup.getName() );
				}
			}
		}
		if ( StringUtils.isNotBlank( dto.getPromotion() ) ) {
			for ( String strId : 
				( dto.getPromotion().indexOf( "," ) != -1 )? dto.getPromotion().split( "," ) : new String[]{ dto.getPromotion() } ) {
				PromotionDto promotion = promotionService.getPromotionDto( Long.valueOf( strId ) );
				if ( null != promotion ) {
					dto.setDescription( ( StringUtils.isNotBlank( dto.getPromotionName() )? dto.getPromotionName() + ", " : "" )
							 + promotion.getDescription() );
				}
			}
		}
		
		dto.setTargetCount( countTargetMembers(dto.getMemberGroup(), dto.getPromotion()).intValue() );

		return dto;
	}

	@Override
	public MarketingChannel save( MarketingChannel model ) {
		return repo.save( model );
	}

	@Override
	public MarketingChannelDto saveDto( MarketingChannelDto dto, boolean isNew ) {
		MarketingChannel model = dto.toModel();
		if ( isNew ) {
			model.setStatus( ChannelStatus.DRAFT );
			model.setCode( customIdGeneratorService.generateId( model.getType().toString() ) );
		}
		save( model );

		return new MarketingChannelDto( model );
	}

	@Override
	public MarketingChannelDto saveFileAndDto( MarketingChannelDto dto, boolean isNew ) {
		CrmFile file = saveFile( dto.getFile() );
		if ( null != file ) {
			dto.setFileId( file.getId().toString() );
			dto.setFilename( file.getFilename() );
		}
		return saveDto( dto, true );
	}

	@Override
	public MarketingChannelDto updateDto( MarketingChannelDto dto ) {
		if ( null == dto ) {
			return null;
		}
		MarketingChannel model = dto.toModel( NumberUtils.isNumber( dto.getId() )? repo.findOne( Long.valueOf( dto.getId() ) ) : null );
		save( model );

		return new MarketingChannelDto( model );
	}

	@Override
	public MarketingChannelDto updateFileAndDto( MarketingChannelDto dto ) {
		CrmFile file = saveFile( dto.getFile() );
		if ( null != file ) {
			dto.setFileId( file.getId().toString() );
			dto.setFilename( file.getFilename() );
		}
		else {
			if ( BooleanUtils.isTrue( dto.getDeleteFile() ) ) {
				dto.setFileId( null );
				dto.setFilename( null );
			}
		}
		return updateDto( dto );
	}

	@Override
	public CrmFile saveFile( MultipartFile file ) {
		if ( null == file || file.getSize() == 0 ) {
			return null;
		}
		return crmFileService.saveMultipartFile( file, ModelType.MARKETINGCHANNEL.toString() );
	}

	@Override
	public List<String> getCreateUsers() {
		QMarketingChannel qChannel = QMarketingChannel.marketingChannel;
		JPQLQuery query = new JPAQuery(entityManager).from( qChannel );
		return query.distinct().list( qChannel.createUser );
	}

	@Override
	public ResultList<MarketingChannelDto> search( MarketingChannelSearchDto dto ) {
		Pageable pageable = SpringDataPagingUtil.INSTANCE.createPageable( dto.getPagination() );
        BooleanExpression filter = dto.createSearchExpression();
        Page<MarketingChannel> page = ( filter != null )? repo.findAll( filter, pageable ) : repo.findAll( pageable );
        return new ResultList<MarketingChannelDto>( MarketingChannelDto.toDtoList( page.getContent() ), page.getTotalElements(), page.getNumber(), page.getSize() );
	}

	@Override
	public MarketingChannelDto saveRemarksDto( ApprovalRemarkDto dto ) {
		if ( null == dto || StringUtils.isBlank( dto.getModelId() ) ) {
			return null;
		}
		ApprovalRemark remark = dto.toModel();
		remark.setModelType( ModelType.MARKETINGCHANNEL.toString() );
		remark.setStatus( getStatus( dto.getStatus() ) );
		remark = approvalRemarkRepo.save( remark );

		MarketingChannel model = repo.findOne( Long.valueOf( dto.getModelId() ) );
		model.setStatus( EnumUtils.getEnum( ChannelStatus.class, dto.getStatus() ) );
		return new MarketingChannelDto( repo.save( model ) );
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<MemberModel> getTargetRecipients( String commaDelimMemberGrpIds, String commaDelimPromoIds ) {
		Set<Long> members = new HashSet<Long>();

		String[] memberGrpIds = StringUtils.split( commaDelimMemberGrpIds, "," );
		if ( ArrayUtils.isNotEmpty( memberGrpIds ) ) {
			for ( String memberGrpId : memberGrpIds ) {
				MemberGroup memberGroup = NumberUtils.isNumber( memberGrpId )? 
						memberGroupService.getMemberGroup( Long.valueOf( memberGrpId ) ) : null;
    			List<Long> membersPerGrp = ( null != memberGroup )?
    					memberGroupService.getQualifiedMembers( memberGroup ) : null;
    			if ( null != membersPerGrp ) {
    				members.addAll(membersPerGrp);
    			}
			}
		}

		String[] promoIds = StringUtils.split( commaDelimPromoIds, "," );
		if ( ArrayUtils.isNotEmpty( promoIds ) ) {
			for ( String promoId : promoIds ) {
				Promotion promo = NumberUtils.isDigits( promoId )? promotionRepo.findOne( Long.valueOf( promoId ) ) : null;
				if ( null != promo && CollectionUtils.isNotEmpty( promo.getTargetMemberGroups() ) ) {
	        		for ( PromotionMemberGroup memGrp : promo.getTargetMemberGroups() ) {
	        			if ( !ArrayUtils.contains( memberGrpIds, memGrp.getMemberGroup().getId() ) ) {
	            			List<Long> membersPerGrp = memberGroupService.getQualifiedMembers( memGrp.getMemberGroup() );
	            			if ( null != membersPerGrp ) {
	            				members.addAll(membersPerGrp);
	            			}
	        				
	        			}	        				
	        		}
				}
				else {
					/*CollectionUtils.addAll( members, memberRepo.findAll().iterator() );*/
					return memberRepo.findAll();
				}
			}
		}
		
		QMemberModel qMemberModel = QMemberModel.memberModel;
		List<Long> membersList = Lists.newArrayList(members);
		
		BooleanBuilder exp = new BooleanBuilder();
		for(int i=0; i < members.size(); i += 1000) {
			int toIndex = i + 1000;
			if(toIndex > members.size())
				exp.or(qMemberModel.id.in(membersList.subList(i, members.size())));
			else
				exp.or(qMemberModel.id.in(membersList.subList(i, toIndex)));
		}
		
		Iterable<MemberModel> it = memberRepo.findAll(exp);
		return ( it != null )? IteratorUtils.toList( it.iterator() ) : null;
	}
	
	@Override
	public Long countTargetMembers( String commaDelimMemberGrpIds, String commaDelimPromoIds ) {
		Set<Long> members = new HashSet<Long>();
		
		String[] memberGrpIds = StringUtils.split( commaDelimMemberGrpIds, "," );
		if ( ArrayUtils.isNotEmpty( memberGrpIds ) ) {
			for ( String memberGrpId : memberGrpIds ) {
				MemberGroup memberGroup = NumberUtils.isNumber( memberGrpId )? 
						memberGroupService.getMemberGroup( Long.valueOf( memberGrpId ) ) : null;
    			List<Long> membersPerGrp = ( null != memberGroup )?
    					memberGroupService.getQualifiedMembers( memberGroup ) : null;
    			if ( CollectionUtils.isNotEmpty(membersPerGrp) ) {
    				members.addAll(membersPerGrp);
    			}
			}
		}

		String[] promoIds = StringUtils.split( commaDelimPromoIds, "," );
		if ( ArrayUtils.isNotEmpty( promoIds ) ) {
			for ( String promoId : promoIds ) {
				Promotion promo = NumberUtils.isDigits( promoId )? promotionRepo.findOne( Long.valueOf( promoId ) ) : null;
				if ( null != promo && CollectionUtils.isNotEmpty( promo.getTargetMemberGroups() ) ) {
	        		for ( PromotionMemberGroup memGrp : promo.getTargetMemberGroups() ) {
	        			if ( !ArrayUtils.contains( memberGrpIds, memGrp.getMemberGroup().getId() ) ) {
	            			List<Long> membersPerGrp = memberGroupService.getQualifiedMembers( memGrp.getMemberGroup() );
	            			if ( CollectionUtils.isNotEmpty(membersPerGrp) ) {
	            				members.addAll(membersPerGrp);
	            			}
	        			}	        				
	        		}
				}
				else {
					/*CollectionUtils.addAll( members, memberRepo.findAll().iterator() );*/
					return memberRepo.count();
				}
			}
		}
		
		return Long.valueOf(members.size());
	}

	@Override
	public String getAppConfig( String configKey ) {
		if ( EnumUtils.isValidEnum( AppKey.class, configKey ) ) {
			ApplicationConfig config = applicationConfigRepo.findByKey( EnumUtils.getEnum( AppKey.class, configKey ) );
			if ( null != config ) {
				return config.getValue();
			}
		}
		return null;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<MarketingChannel> getScheduled( Date jobDate ) {
		Iterable<MarketingChannel> channels = repo.findAll( 
			QMarketingChannel.marketingChannel.schedule.eq( new LocalDate( jobDate.getTime() ) )
				.and( QMarketingChannel.marketingChannel.time.eq( new LocalTime( jobDate.getTime() ) ) ) );
		return ( channels != null )? IteratorUtils.toList( channels.iterator() ) : null;
	}



	private String saveFileToDirectory( MultipartFile file ) {
		File transferFile = null;
		try {
			transferFile = new File( DIR_CHANNEL );
			if ( !transferFile.isDirectory() ) {
				transferFile.mkdir();
			}
			transferFile = new File( DIR_CHANNEL + new Date().getTime() + file.getOriginalFilename() );
			file.transferTo( transferFile );
		} 
		catch ( IOException ioe ) { return null; }
		catch ( Exception e ) { return null; }
		finally {}

		return transferFile.getPath();
	}

	private String updateFileInDirectory( MultipartFile file, String origFilename ) {
		if ( StringUtils.isNotBlank( origFilename ) && !file.getOriginalFilename().equalsIgnoreCase( origFilename ) ) {
			File transferFile = null;
			try {
				transferFile = new File( DIR_CHANNEL + origFilename );
				transferFile.delete();
			}
			catch ( Exception e ) { return null; }
			finally {}
		}
		return saveFileToDirectory( file );
	}

	private LookupDetail getStatus( String strStatus ) {
		ChannelStatus status = EnumUtils.getEnum( ChannelStatus.class, strStatus );
		switch( status ) {
			case ACTIVE: return lookupDetailRepo.findByCode( codePropertiesService.getDetailStatusActive() );
			case REJECTED: return lookupDetailRepo.findByCode( codePropertiesService.getDetailStatusRejected() );
			case DRAFT: return lookupDetailRepo.findByCode( codePropertiesService.getDetailStatusDraft() );
			case CANCELED: return lookupDetailRepo.findByCode( codePropertiesService.getDetailStatusDisabled() );
			default : return null;
		}
	}

}