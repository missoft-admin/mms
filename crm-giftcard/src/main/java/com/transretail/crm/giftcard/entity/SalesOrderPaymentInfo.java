package com.transretail.crm.giftcard.entity;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.hibernate.annotations.Type;
import org.joda.time.LocalDate;

import com.transretail.crm.core.entity.lookup.LookupDetail;
import com.transretail.crm.core.entity.support.CustomAuditableEntity;
import com.transretail.crm.giftcard.entity.support.PaymentInfoStatus;

/**
 *
 */
@Entity
@Table(name = "CRM_SALES_ORD_PYMNT")
public class SalesOrderPaymentInfo extends CustomAuditableEntity<Long> implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@ManyToOne
    @JoinColumn(name = "SALES_ORDER")
	private SalesOrder order;
	
	@Column(name = "PAYMENT_DATE")
	@Type(type = "org.jadira.usertype.dateandtime.joda.PersistentLocalDate")
	private LocalDate paymentDate;
	
	@ManyToOne
    @JoinColumn(name = "PAYMENT_TYPE")
	private LookupDetail paymentType;
	
	@Column(name = "PAYMENT_AMT")
	private BigDecimal paymentAmount;
	
	@Column(name = "PAYMENT_DTLS")
	private String paymentDetails;
	
	@Column(name = "VERIF_DATE")
	@Type(type = "org.jadira.usertype.dateandtime.joda.PersistentLocalDate")
	private LocalDate dateVerified;
	
	@Column(name = "APPROVAL_DATE")
	@Type(type = "org.jadira.usertype.dateandtime.joda.PersistentLocalDate")
	private LocalDate dateApproved;
	

	@Column(name = "STATUS")
	@Enumerated(EnumType.STRING)
	private PaymentInfoStatus status;

	public SalesOrder getOrder() {
		return order;
	}

	public void setOrder(SalesOrder order) {
		this.order = order;
	}

	public LocalDate getPaymentDate() {
		return paymentDate;
	}

	public void setPaymentDate(LocalDate paymentDate) {
		this.paymentDate = paymentDate;
	}

	public LookupDetail getPaymentType() {
		return paymentType;
	}

	public void setPaymentType(LookupDetail paymentType) {
		this.paymentType = paymentType;
	}

	public BigDecimal getPaymentAmount() {
		return paymentAmount;
	}

	public void setPaymentAmount(BigDecimal paymentAmount) {
		this.paymentAmount = paymentAmount;
	}

	public String getPaymentDetails() {
		return paymentDetails;
	}

	public void setPaymentDetails(String paymentDetails) {
		this.paymentDetails = paymentDetails;
	}

	public PaymentInfoStatus getStatus() {
		return status;
	}

	public void setStatus(PaymentInfoStatus status) {
		this.status = status;
	}

	public LocalDate getDateVerified() {
		return dateVerified;
	}

	public void setDateVerified(LocalDate dateVerified) {
		this.dateVerified = dateVerified;
	}

	public LocalDate getDateApproved() {
		return dateApproved;
	}

	public void setDateApproved(LocalDate dateApproved) {
		this.dateApproved = dateApproved;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;

		if (o == null || getClass() != o.getClass()) return false;

		SalesOrderItem that = (SalesOrderItem) o;

		return new EqualsBuilder()
				.appendSuper(super.equals(o))
				.isEquals();
	}

	@Override
	public int hashCode() {

		int hashcode = 17;

		hashcode += (null == getId()) ? 0 : getId().hashCode() * 31;

		return hashcode;
	}
}
