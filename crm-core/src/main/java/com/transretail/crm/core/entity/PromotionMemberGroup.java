package com.transretail.crm.core.entity;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.envers.Audited;

import com.transretail.crm.core.entity.support.CustomAuditableEntity;
import com.transretail.crm.core.entity.support.CustomIdAuditableEntity;

/**
 * @author Allan G. Ramirez (agramirez@exist.com)
 */
@Audited
@Entity
@Table(name = "CRM_PROMOTION_MEMBER_GRP")
public class PromotionMemberGroup extends PromotionTargetGroup<String> implements Serializable {
    private static final long serialVersionUID = -5082861084138355230L;
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "MEMBER_GRP_ID")
    private MemberGroup memberGroup;

    public MemberGroup getMemberGroup() {
        return memberGroup;
    }

    public void setMemberGroup(MemberGroup memberGroup) {
        this.memberGroup = memberGroup;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        PromotionMemberGroup that = (PromotionMemberGroup) o;

        if (!memberGroup.equals(that.memberGroup)) return false;
        if (!getPromotion().equals(that.getPromotion())) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = getPromotion().hashCode();
        result = 31 * result + memberGroup.hashCode();
        return result;
    }
}
