package com.transretail.crm.web.controller;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.sql.SQLException;

import javax.persistence.EntityManagerFactory;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import com.transretail.crm.core.entity.CompanyProfile;
import com.transretail.crm.core.service.CompanyProfileService;


@RequestMapping("/companyprofile")
@Controller
public class CompanyProfileController {
	private final static String ITS_DEFAULTMSG_INVALID   = "Info given is invalid.";
	
	private static final String UI_ATTR_MODEL = "companyProfile";
	
	@Autowired
	CompanyProfileService companyProfileService;
	
	@Autowired
    MessageSource messageSource;
	
	@Autowired
	EntityManagerFactory entityManagerFactory;
	
	@RequestMapping(produces = "text/html")
    public String list(
            @RequestParam(value = "page", required = false) Integer page,
            @RequestParam(value = "size", required = false) Integer size,
            Model uiModel) {
		
    	populateForm(uiModel, companyProfileService.retrieveCompany());

        return "companyprofile/details";
    }
	
	@RequestMapping(params = "form", produces = "text/html")
    public String updateForm(
            @RequestParam(value = "page", required = false) Integer page,
            @RequestParam(value = "size", required = false) Integer size,
            Model uiModel) {
		
    	populateForm(uiModel, companyProfileService.retrieveCompany());

        return "companyprofile/form";
    }
	
	@RequestMapping(method = RequestMethod.POST, produces = "text/html")
    public String create(
    		@ModelAttribute(value="companyProfile") CompanyProfile companyProfile,
    		@RequestParam("file") MultipartFile file,
            BindingResult bindingResult, 
            Model uiModel,
            MultipartHttpServletRequest httpServletRequest) {
		
		validate(companyProfile, file, bindingResult);
        
		if (bindingResult.hasErrors()) {
            populateForm(uiModel, companyProfile);
            return "companyprofile/form";
        }
        
		uiModel.asMap().clear();

		try {
			companyProfile.setLogo(file.getBytes());
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		companyProfileService.saveProfile(companyProfile);

		return "redirect:/companyprofile";
    }
	
	private void validate(CompanyProfile companyProfile, MultipartFile file, BindingResult bindingResult) {
		if (StringUtils.isBlank(companyProfile.getName())) {
            bindingResult.rejectValue("name", "propkey_msg_notempty", new Object[] {messageSource.getMessage("label_company_name", null, LocaleContextHolder.getLocale())}, ITS_DEFAULTMSG_INVALID);
        }
		
		if(file.getSize() > 0 && !(file.getContentType().equalsIgnoreCase("image/png") || file.getContentType().equalsIgnoreCase("image/jpeg"))) {
			bindingResult.rejectValue("name", "propkey_msg_invalid_file", new Object[] {messageSource.getMessage("label_company_logo", null, LocaleContextHolder.getLocale())}, ITS_DEFAULTMSG_INVALID);
		}
	}
	
	private void populateForm(Model uiModel, CompanyProfile companyProfile) {
		uiModel.addAttribute(UI_ATTR_MODEL, companyProfile);
	}
	
	@RequestMapping(value = "/logo", method = RequestMethod.GET)
    public void getDocumentFileContent(
    		HttpServletResponse response) 
    		throws IOException {
		
		CompanyProfile companyProfile = companyProfileService.retrieveCompany();
		
        try {
			writeFileContentToHttpResponse(companyProfile.getLogo(), response);
		} catch (SQLException e) {
			e.printStackTrace();
		}
    }

    private void writeFileContentToHttpResponse(byte[] image, HttpServletResponse response) throws IOException, SQLException {
    	if (image != null) {
            try {
                response.setContentType("image/jpeg");
                ServletOutputStream out = response.getOutputStream();
                IOUtils.copy(new ByteArrayInputStream(image), out);
                out.flush();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

}
