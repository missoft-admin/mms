<%@ include file="../common/taglibs.jsp" %>

<style type="text/css">
<!--
  .btn-small {
      margin-left: 4px;

  }
  .cont-02 input { margin-bottom: 3px; } 

-->
</style>

  <spring:message var="typeName" code='label_group_storegroup'/>
  <spring:message var="groupName" code='label_group_groupname'/>
  <spring:message var="stores" code='label_group_stores'/>
  <spring:message var="selection" code='label_group_selection'/>
  
<div class="modal hide  groupModal nofly" id="itemDialog" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog">
    
    <div class="modal-header">
      <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
      <h4>Update/<spring:message code="global_menu_new" arguments="${ typeName }"/></h4>
    </div>
    
    <div class="modal-content">
      <div class="modal-body">
      	<c:set var="ENCTYPE" value="application/x-www-form-urlencoded"/>
        <form:form id="groupForm" name="groupForm" modelAttribute="groupForm" action="${pageContext.request.contextPath}/groups/store" method="POST" enctype="${ENCTYPE}" >
        <div class="contentDialogBody">
        
       		<div id="contentError" class="hide alert alert-error">
       			<button type="button" class="close" onclick="$(this).parent().hide();">&times;</button>
                <div><form:errors path="*"/></div>
            </div>
        
        	<div>
        		<div>
	        		<div><label for="name"><b class="required">*</b> ${groupName}</label></div>
					<div><form:input path="name" placeholder="${groupName}" /></div>
				</div>
        	</div>
        	
        	<div>
        		<div class="form-horizontal">
                    <spring:message code="label_filter_args" var="filterLabel" arguments="${stores}" />
        			<input type="text" name="searchStr" /><input type="button" id="searchBtn" class="btn btn-small" value="${filterLabel}"/>
        		</div>
        	</div>
        	
        	<div style="padding-top: 10px;">
	        	<div class="pull-left" style="width: 44%;">
	        		<div style="width: 100%"><label for="">${stores}</label></div>
	        		<div style="width: 100%">
		        		<select multiple="multiple" name="selectItems" style="width:100%;" id="selectStoreGroups">
		        		</select>
	        		
	        		</div>
	        	</div>
	        	<div class="">
	        		<div class="cont-02" style="margin-top: 25px;">
	        		<input type="button" id="addBtn" class="btn" value=">>"/>
	        		<input type="button" id="removeBtn" class="btn" value="<<"/>
	        		</div>
	        	</div>
	        	
	        	<div class="pull-left" style="width: 44%">
	        	
	        		<div style="width: 100%"><label for=""><b class="required">*</b> ${selection}</label></div>
	        		<div style="width: 100%">
		        		<select multiple="multiple" class="items" name="stores" style="width:100%;" id="selectedStoreGroups">
		        		</select>
	        		</div>
	        	</div>
        	</div>
        	
        
		</div>
		</form:form>
      </div>
      <div class="modal-footer container-btn">
        <button type="button" id="button_save" class="btn btn-primary"><spring:message code="label_save" /></button>
        <button type="button" id="button_cancel" class="btn btn-default"><spring:message code="label_cancel" /></button>
      </div>
    </div>
  </div>
</div>
 
