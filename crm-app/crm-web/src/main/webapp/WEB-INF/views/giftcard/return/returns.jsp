<%@include file="../../common/taglibs.jsp" %>
<%@ taglib prefix="permission" tagdir="/WEB-INF/tags/permission" %>
<%@ taglib prefix="crm" uri="http://www.carrefour.com/crm/permission/tags" %>

<sec:authorize ifAnyGranted="ROLE_MERCHANT_SERVICE_SUPERVISOR" var="isHead" />
<sec:authorize ifAnyGranted="ROLE_TREASURY" var="isTreasury" />

  <style type="text/css">
<!--
  button.selectpicker {
      min-width: 240px;
  }
-->
</style>

<div class="page-header page-header2">
    <h1><spring:message code="gc_return" /></h1>
</div>

<div>
    <jsp:include page="returnsearch.jsp" />
</div>

<div class="form-horizontal pull-left mb20">
  <select name="reportType" class="selectpicker" id="reportType" data-header="Select a Format">
  <option value="b2b"><spring:message code="gc_return_min" /></option>
  <option value="b2bByCards"><spring:message code="gc_return_by_cards" /></option>
  </select>
</div>

<div class="pull-right mb20">
<button type="button" class="btn btn-primary" id="returnBtn"><spring:message code="gc_return_min" /></button>
<button type="button" class="btn btn-primary" id="returnByCardsBtn"><spring:message code="gc_return_by_cards" /></button>
</div>

<div id="return_list_container">
</div>

<div class="modal hide  nofly" id="formDialog" tabindex="-1" role="dialog" aria-hidden="true">
</div>

<jsp:include page="../../common/confirm.jsp" />
<jsp:include page="returnrefundform.jsp" />

  <link href="<spring:url value="/css/dataTables.css" />" rel="stylesheet">
  <script src="<spring:url value="/js/jquery/jquery.dataTables.js" />" ></script>
  <script src="<spring:url value="/js/jquery/jquery.dataTables.linkLengthMenu.js" />"></script>
  <script src="<spring:url value="/js/jquery/jquery.dataTables.customPaging.js" />"></script>
  <script src="<spring:url value="/js/jquery/ajax-datatable.js" />" ></script>
  <script src="<spring:url value="/js/common/form2js.js" />" ></script>
  <script src="<spring:url value="/js/common/json2.js" />" ></script>
  <script src="<spring:url value="/js/jquery/jquery.toObject.js" />" ></script>
  <link href="<c:url value="/styles/temp.css" />" rel="stylesheet" />
  
  
  <script src="<spring:url value="/js/jquery/jquery.numberInput.js" />" ></script>
  <script src="<c:url value="/js/bootstrap/bootstrap-datepicker.js"/>" type='text/javascript'><![CDATA[&nbsp; ]]></script>
  <link href="<c:url value="/css/bootstrap/datepicker.css" />" rel="stylesheet" />
  <link href="<c:url value="/styles/temp.css" />" rel="stylesheet" />
  <script src="<spring:url value="/js/jquery/jquery.bindWithDelay.js" />"></script>
  <script src="<spring:url value="/js/jquery/jquery.ajaxQueue.js" />"></script>

<script>
$(document).ready(function() {
	
	$('.selectpicker').selectpicker({
		style: 'btn-print-drop',
		size: 4,
		showIcon: false
	}).change(function() {
		var value = $(this).val();
		if(value == 'b2b') {
			listReturns();
		} else {
			listReturnsByCards();
		}
	});
	
	$formDialog = $("#formDialog").modal({
		show: false
	}).on('hidden.bs.modal', function() {
		$(this).empty();
	}).css({
		"width": "900px",
	});
	
	$("#returnBtn").click(function() {
		$.get("<c:url value="/giftcard/return/form/null" />", function(data) {
			$formDialog.modal("show").html(data);
		}, "html");
	});
	
	$("#returnByCardsBtn").click(function() {
		$.get("<c:url value="/giftcard/return/bycards/form/null" />", function(data) {
			$formDialog.modal("show").html(data);
		}, "html");
	});
	
	
	function listReturnsByCards() {
		$("#return_list_container").empty().ajaxDataTable({
			'autoload'  : true,
		    'aaSorting' :[[ 0, 'desc' ]],
			'ajaxSource' : "<c:url value="/giftcard/return/bycards/search" />",
			'columnHeaders' : [
			  { text: "", className : "hide" },
				"<spring:message code="gc_return_no"/>",
				"<spring:message code="gc_return_date"/>",
				"<spring:message code="gc_return_customer_name"/>",
				"<spring:message code="gc_return_order_no"/>",
				"<spring:message code="gc_return_amount"/>",
				"<spring:message code="gc_replace_amount"/>",
				"<spring:message code="gc_return_created_by"/>",
				"<spring:message code="gc_return_status"/>",
				""
			],
			'modelFields' : [
			  {name : 'lastUpdated', sortable: true, dataType: "DATE", aaClassname : "hide"},
				{name : 'recordNo', sortable : true},
				{name : 'returnDate', sortable : true},
				{name : 'customerName', sortable : false},
				{name : 'orderNo', sortable : false},
				{name : 'returnAmount', sortable : false, fieldCellRenderer : function (data, type, row) {
	                if (row.returnAmount !== null) {
	                    return commaSeparateNumber(row.returnAmount);
	                } else {
	                    return row.returnAmount;
	                }
	            }},
				{name : 'replaceAmount', sortable : false, fieldCellRenderer : function (data,type, row) {
	                if (row.replaceAmount !== null) {
	                    return commaSeparateNumber(row.replaceAmount);
	                } else {
	                    return row.replaceAmount;
	                }
	            }},
				{name : 'createdBy', sortable : false},
				{name : 'status', sortable : false},
				{customCell : function ( innerData, sSpecific, json ) {
					if (sSpecific == 'display') {
						var html = "";
						
						html += "<button class=\"viewRet btn btn-primary pull-left tiptip icn-view\" data-id=\""+json.id+"\" title=\"" + '<spring:message code="label_view"/>' + "\"></button>";
						
						if(json.status == 'NEW') {
							
						/* 	if(${isHead} && json.isReadyForApproval) {
								html += "<button class=\"approveOrder btn btn-primary pull-left tiptip icn-edit\" data-id=\""+json.id+"\" title=\"<spring:message code="label_process"/>\"></button>";
							} */
							<crm:permission hasPermissions="GC_REPLACE">
							html += "<button class=\"createReplace btn btn-primary pull-left tiptip icn-edit\" data-id=\""+json.id+"\" data-return-order=\""+json.recordNo+"\" title=\"<spring:message code="gc_replace_create"/>\"></button>";
							</crm:permission>
						}
						
						return html;
					} else {
						return "";
					}
				}
				}
			]
		}).on("click", ".createReplace", function() {
			var url = "<c:url value="/giftcard/salesorder" />";
			var form = $('<form action="' + url + '" method="POST">' +
			  '<input type="text" name="showDialog" value="true" />' +
			  '<input type="text" name="returnNo" value="'+$(this).data("returnOrder")+'" />' +
			  '<input type="text" name="byCards" value="true" />' +
			  '</form>');
			  $('body').append(form);
			$(form).submit();
		}).on("click", ".viewRet", function() {
			var id = $(this).data("id");
			$.get("<c:url value="/giftcard/return/bycards/view/" />" + id, function(data) {
				$formDialog.modal("show").html(data);
			}, "html");
		});
	}
	
	function listReturns() {
		$("#return_list_container").empty().ajaxDataTable({
			'autoload'  : true,
		    'aaSorting' :[[ 0, 'desc' ]],
			'ajaxSource' : "<c:url value="/giftcard/return/search" />",
			'columnHeaders' : [
			  { text: "", className : "hide" },
				"<spring:message code="gc_return_no"/>",
				"<spring:message code="gc_return_date"/>",
				"<spring:message code="gc_return_customer_name"/>",
				"<spring:message code="gc_return_order_no"/>",
				"<spring:message code="gc_return_amount"/>",
				"<spring:message code="gc_refund_amount"/>",
				"<spring:message code="gc_replace_amount"/>",
				"<spring:message code="gc_return_created_by"/>",
				"<spring:message code="gc_return_status"/>",
				""
			],
			'modelFields' : [
			  {name : 'lastUpdated', sortable: true, dataType: "DATE", aaClassname : "hide"},
				{name : 'recordNo', sortable : true},
				{name : 'returnDate', sortable : true},
				{name : 'customerName', sortable : false},
				{name : 'orderNo', sortable : false},
				{name : 'returnAmount', sortable : false, fieldCellRenderer : function (data, type, row) {
	                if (row.returnAmount !== null) {
	                    return commaSeparateNumber(row.returnAmount);
	                } else {
	                    return row.returnAmount;
	                }
	            }},
				{name : 'refundAmount', sortable : false, fieldCellRenderer : function (data, type, row) {
	                if (row.refundAmount !== null) {
	                    return commaSeparateNumber(row.refundAmount);
	                } else {
	                    return row.refundAmount;
	                }
	            }},
				{name : 'replaceAmount', sortable : false, fieldCellRenderer : function (data,type, row) {
	                if (row.replaceAmount !== null) {
	                    return commaSeparateNumber(row.replaceAmount);
	                } else {
	                    return row.replaceAmount;
	                }
	            }},
				{name : 'createdBy', sortable : false},
				{name : 'status', sortable : false},
				{customCell : function ( innerData, sSpecific, json ) {
					if (sSpecific == 'display') {
						var html = "";
						if(json.status == 'NEW') {
							if(${isHead} && json.isReadyForApproval) {
								html += "<button class=\"approveOrder btn btn-primary pull-left tiptip icn-edit\" data-id=\""+json.id+"\" title=\"<spring:message code="label_process"/>\"></button>";
							}
							
							<crm:permission hasPermissions="GC_REPLACE">
							html += "<button class=\"createReplace btn btn-primary pull-left tiptip icn-edit\" data-id=\""+json.id+"\" data-return-order=\""+json.recordNo+"\" title=\"<spring:message code="gc_replace_create"/>\"></button>";
							</crm:permission>

							<crm:permission hasPermissions="GC_REFUND">
							html += "<button class=\"createRefund btn btn-primary pull-left tiptip icn-edit\" data-id=\""+json.id+"\" data-refund-amount=\""+json.refundAmount+"\" data-replace-amount=\""+json.replaceAmount+"\" data-return-amount=\""+json.returnAmount+"\"" + 
								"title=\"<spring:message code="gc_refund_create"/>\"></button>";
							</crm:permission>
							
						}
						
						return html;
					} else {
						return "";
					}
				}
				}
			]
		}).on("click", ".updateOrder", function() {
			$.get("<c:url value="/giftcard/return/form/" />" + $(this).data("id"), function(data) {
				$formDialog.modal("show").html(data);
			}, "html");
		}).on("click", ".approveOrder", function() {
			$.get("<c:url value="/giftcard/return/approvalform/" />" + $(this).data("id"), function(data) {
				$formDialog.modal("show").html(data);
			}, "html");
		}).on("click", ".viewDetails", function() {
			$("#detailsDialog").modal("show");
			reloadTable($(this).data("id"));
		}).on("click", ".deleteOrder", function() {
			var itemId = $(this).data("id");
			getConfirm('<spring:message code="entity_delete_confirm" />', function(result) { 
				if(result) {
					$.post("<c:url value="/giftcard/return/" />" + itemId, {_method: 'DELETE'}, function(data) {
						location.reload();
					});
				}
			});
		}).on("click", ".createRefund", function() {
			$refundDialog = $("#refundDialog"); 
			$("#id", $refundDialog).val($(this).data("id"));
			$("#returnAmount", $refundDialog).val($(this).data("returnAmount"));
			$("#replaceAmount", $refundDialog).val($(this).data("replaceAmount"));
			$("#refundAmount", $refundDialog).val($(this).data("refundAmount"));
			$refundDialog.modal("show");
			
		}).on("click", ".createReplace", function() {
			var url = "<c:url value="/giftcard/salesorder" />";
			var form = $('<form action="' + url + '" method="POST">' +
			  '<input type="text" name="showDialog" value="true" />' +
			  '<input type="text" name="returnNo" value="'+$(this).data("returnOrder")+'" />' +
			  '<input type="text" name="byCards" value="false" />' +
			  '</form>');
			  $('body').append(form);
			$(form).submit();
		});
	}
	
	
	
	function initCommonFields(dialog) {
		$(".floatInput", $(dialog)).numberInput({ "type" : "float" });
		$(".intInput", $(dialog)).numberInput({ "type" : "int" });
	    $(".wholeInput", $(dialog)).numberInput({ "type" : "whole" });
	}

    function commaSeparateNumber(val){
        while (/(\d+)(\d{3})/.test(val.toString())){
            val = val.toString().replace(/(\d+)(\d{3})/, '$1'+','+'$2');
        }
        return val;
    }
    
    listReturns();
});
</script>
