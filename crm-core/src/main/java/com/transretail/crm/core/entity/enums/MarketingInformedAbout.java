package com.transretail.crm.core.entity.enums;

public enum MarketingInformedAbout {

	FRIENDS("friends"),
	ADS("ads"),
	WEBSITE("website"),
	SOCIAL_MEDIA("socialMedia"),
	OTHERS("");

    private MarketingInformedAbout(String code){
    	this.code = code;
    }

    private final String code;

	public String getCode() {
		return code;
	}
    
    
}
