<%@ include file="../../common/taglibs.jsp"%>

<style type="text/css">
<!--
#productTable tr td:last-child {
	border-left: 0px;
}

#addProductBtn.btn-small {
	margin-bottom: 5px;
}

table select, textarea, table input[type="text"], input[type="password"],
	input[type="datetime"], input[type="datetime-local"], input[type="date"],
	input[type="month"], input[type="time"], input[type="week"], input[type="number"],
	input[type="email"], input[type="url"], input[type="search"], input[type="tel"],
	input[type="color"], .uneditable-input {
	margin-bottom: 0px;
	margin-top: 0px;
}
-->
</style>
<c:set var="ENCTYPE" value="application/x-www-form-urlencoded" />
<div class="modal-dialog">
	<div class="modal-content">
		<div class="modal-header">
			<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
			<spring:message code="gc.poslose" var="typeName" />
			<h4 class="modal-title">
				<spring:message code="global_menu_new" arguments="${typeName}" />
			</h4>
		</div>
		<div class="modal-body">
			<div id="contentError" class="hide alert alert-error">
				<button type="button" class="close" onclick="$(this).parent().hide();">&times;</button>
				<div>
					<form:errors path="*" />
				</div>
			</div>
			<form:form id="posLoseTxForm" name="posLoseTxForm" modelAttribute="posLoseTxForm" action="${pageContext.request.contextPath}/giftcard/pos-lose-tx/save" method="POST" enctype="${ENCTYPE}"
				cssClass="form-reset form-horizontal">
				<div class="contentMain">
					<div class="row-fluid">
						<div class="span6 pull-left">
							<div class="control-group">
								<spring:message code="gc.poslose.label.store" var="merchantLabel" />
								<label for="merchantId" class="control-label"><b class="required">* </b>${merchantLabel}</label>
								<div class="controls">
									<%-- <form:select id="merchantId" path="merchantId" itemValue="code" itemLabel="codeAndName" items="${stores}" class="form-control" /> --%>
									<input id="paymentStoreName" class="form-control input-medium"/>
									<form:hidden path="merchantId" id="merchantId" />
								</div>
							</div>
							<div class="control-group">
								<spring:message code="gc.poslose.label.pos.no" var="terminalIdLabel" />
								<label for="terminalId" class="control-label"><b class="required">* </b>${terminalIdLabel}</label>
								<div class="controls">
									<form:input path="terminalId" class="form-control" placeholder="${terminalIdLabel}" maxlength="12" />
								</div>
							</div>
							<div class="control-group">
								<spring:message code="gc.poslose.label.cashier.no" var="cashierIdLabel" />
								<label for="cashierId" class="control-label"><b class="required">* </b>${cashierIdLabel}</label>
								<div class="controls">
									<form:input path="cashierId" class="form-control" placeholder="${cashierIdLabel}" maxlength="12" />
								</div>
							</div>
						</div>
						<div class="span6 pull-left">
							<div class="control-group">
								<spring:message code="gc.poslose.label.type" var="transactionTypeLabel" />
								<label for="transactionType" class="control-label"><b class="required">* </b>${transactionTypeLabel}</label>
								<div class="controls">
									<form:select id="transactionType" path="transactionType" items="${transactionTypes}" itemLabel="left" itemValue="right" class="form-control" />
								</div>
							</div>
							<div class="control-group">
								<spring:message code="gc.poslose.label.date.time" var="transactionTimeLabel" />
								<label for="transactionDate" class="control-label"><b class="required">* </b>${transactionTimeLabel}</label>
								<div class="controls">
									<form:hidden path="transactionDate" />
									<input type="text" class="form-control input-small" placeholder="dd/MM/yyyy" id="dateInput" /> <input type="text" class="form-control input-small" placeholder="hh:mm:ss" id="timeInput" />
								</div>
							</div>
							<div class="control-group">
								<spring:message code="gc.poslose.label.transaction.no" var="transactionNoLabel" />
								<label for="transactionNo" class="control-label"><b class="required">* </b>${transactionNoLabel}</label>
								<div class="controls">
									<form:input path="transactionNo" id="transactionNo" class="form-control" placeholder="${transactionNoLabel}" maxlength="20" />
								</div>
							</div>
							<div id="div-reftx" class="control-group hide">
								<spring:message code="gc.poslose.label.ref.transaction.no" var="refTransactionNo" />
								<label for="refTransactionNo" class="control-label"><b class="required">* </b>${refTransactionNo}</label>
								<div class="controls">
									<form:input path="refTransactionNo" id="refTransactionNo" class="form-control" placeholder="${refTransactionNo}" maxlength="15" />
								</div>
							</div>
						</div>
						<div class="clearfix"></div>
						<div id="tx-items-div" class="pull-center hide">
							<label class="block-stack-title" style="border-bottom: 1px solid #E8E8E8; margin-bottom: 10px; padding: 5px 0 4px;"> <spring:message code="gc.poslose.section.transaction.items" />
							</label>
							<div id="addRemoveButtons" class="pull-left" style="margin-right: 5px;">
								<button type="button" id="addProductBtn" class="btn btn-small">+</button>
								<br />
								<button type="button" id="removeProductBtn" class="btn btn-small">&ndash;</button>
							</div>
							<div class="pull-left span3" style="width: 95%;">
								<table id="productTable" class="table table-condensed table-compressed table-striped table-scrollable">
									<thead>
										<tr>
											<th></th>
											<th><b class="required">* </b>
											<spring:message code="gc.poslose.label.gift.card.no" /></th>
											<th><spring:message code="gc.poslose.label.old.balance" /></th>
											<th><b class="required">* </b>
											<spring:message code="gc.poslose.label.transaction.amount" /></th>
											<th><spring:message code="gc.poslose.label.new.balance" /></th>
										</tr>
									</thead>
									<tbody>
										<c:forEach var="item" items="${posLoseTxForm.items}" varStatus="status">
											<tr data-index="${status.index}">
												<td><input type="checkbox" class="productRemoveFlag" /></td>
												<td><input type="text" class="input-large text-right cardNo" value="${item.cardNo}" name="items[${status.index}].cardNo" maxlength="20" /></td>
												<td><input type="text" class="input-medium text-right previousBalance" value="${item.previousBalance}" name="items[${status.index}].previousBalance" readonly="readonly" /></td>
												<td><input type="text" class="input-medium text-right amount" value="${item.amount}" name="items[${status.index}].amount" /></td>
												<td><input type="text" class="input-medium text-right balance" value="${item.balance}" name="items[${status.index}].balance" readonly="readonly" /></td>
											</tr>
										</c:forEach>
									</tbody>
								</table>
							</div>
						</div>
					</div>
				</div>
			</form:form>
		</div>
		<div class="modal-footer">
			<button type="button" class="txSubmit btn btn-primary">
				<spring:message code="label_save" />
			</button>
			<button type="button" id="cancel" class="btn btn-default" data-dismiss="modal">
				<spring:message code="label_cancel" />
			</button>
		</div>
	</div>
</div>
<jsp:include page="../../common/confirm.jsp" />
<script>
	$(document)
			.ready(
					function() {
						var CONTAINER_ERROR = "#contentError > div";
						var CONTENT_ERROR = "#contentError";
						$form = $("#posLoseTxForm");
						$txDialog = $("#dialogBox").css({
							"width" : "850px"
						});

						$("#dateInput").datepicker({
							autoclose : true,
							format : "dd/mm/yyyy",
						}).datepicker('update', new Date());

						$("#timeInput").timepicker({
							showMeridian : true,
							showSeconds : true,
							minuteStep : 1,
							secondStep : 1
						}).timepicker('showWidget');

						$(".txSubmit").click(submitForm);

						function submitForm() {
							$("input[name='transactionDate']").val(
									$('#dateInput').val() + " "
											+ $('#timeInput').val());
							$(".txSubmit").prop("disabled", true);
							$
									.post(
											$form.attr("action"),
											$form.serialize(),
											function(data) {
												$(".txSubmit").prop("disabled",
														false);
												if (data.success) {
													if ($(
															"#tranactionType option:selected")
															.text() == 'Gift to Customers') {
														$
																.post(
																		"${pageContext.request.contextPath}/api/giftcard/activate/freevoucher",
																		$form
																				.serialize(),
																		function(
																				gcData) {

																		});
													}
													location.reload();
												} else {
													renderError(data);
												}
											});
						}

						function renderError(data) {
							errorInfo = "";
							for (i = 0; i < data.result.length; i++) {
								errorInfo += "<br>"
										+ (i + 1)
										+ ". "
										+ (data.result[i].code != undefined ? data.result[i].code
												: data.result[i]);
							}
							$txDialog.find(CONTAINER_ERROR).html(
									"Please correct following errors: "
											+ errorInfo);
							$txDialog.find(CONTENT_ERROR).show('slow');
						}

						function resetAndClearError() {
							$txDialog.trigger("reset");
							$txDialog.find(CONTENT_ERROR).hide();
							$txDialog.find(CONTAINER_ERROR).empty();
						}

						<c:if test="${empty posLoseTxForm.transactionDate}">
						$("input.transactionDate").datepicker("setDate",
								new Date());
						</c:if>
						$("#merchantId")
								.prepend(
										"<option value='' selected='selected'></option>")
								.change(
										function(e) {
											resetAndClearError();
											var transactionType = $(
													'#transactionType').val();
											console.log("transactionType: "
													+ transactionType);
											console
													.log("transactionType === '' : "
															+ transactionType !== '');
											if (transactionType !== '') {
												if ('VOID_REDEMPTION' === transactionType
														|| 'VOID_ACTIVATED' === transactionType) {
													$('#div-reftx').show();
													$('#tx-items-div').hide();
												} else {
													$('#div-reftx').hide();
													$('#tx-items-div').show();
												}
											}
										});

						$("#transactionType")
								.prepend(
										"<option value='' selected='selected'></option>")
								.change(
										function(e) {
											resetAndClearError();
											var merchant = $('#merchantId')
													.val();
											var selected = $(this).val();

											var emptyMerchant = merchant === undefined
													|| merchant === '';
											var emptyTxType = selected === undefined
													|| selected === '';
											if (emptyTxType) {
												$('#tx-items-div').hide();
												return; // TODO: hide/clear table
											}
											if (emptyMerchant && !emptyTxType) {
												var data = {
													result : [ 'Please select a merchant.' ]
												};
												renderError(data);
												return;
											}

											if ('VOID_REDEMPTION' === selected || 'VOID_ACTIVATED' === selected || 'VOID_RELOAD' === selected) {
												$('#div-reftx').show();
												$('#tx-items-div').hide();
												$("#addRemoveButtons").show();
											} else {
												if ('ACTIVATION' === selected || 'GIFT_TO_CUSTOMER' === selected || 'REDEMPTION' === selected) {
													$('.amount:input[type="text"]').attr("disabled", "disabled");
												}
												if ('RELOAD' === selected ) {
													$("#addRemoveButtons").hide();
												} else {
													$("#addRemoveButtons").show();
												}

												$('#div-reftx').hide();
												$('#tx-items-div').show();
											}
										});

						var $productTable = $('#productTable');

						var $productTbody = $("tbody", $productTable);

						$('#removeProductBtn', $txDialog)
								.click(
										function() {
											$("input.productRemoveFlag:checked")
													.each(
															function() {
																var $tr = $(
																		this)
																		.closest(
																				"tr");
																var length = $productTbody
																		.find("tr").length;
																if ($tr
																		.is(":first-child")
																		&& length === 1) {
																	$tr
																			.find(
																					"input.productRemoveFlag")
																			.prop(
																					'checked',
																					false);
																	$tr
																			.find(
																					"input")
																			.removeAttr(
																					'value')
																			.prop(
																					'disabled',
																					true);
																	$tr
																			.find(
																					"select")
																			.removeAttr(
																					'value')
																			.prop(
																					'disabled',
																					true);
																	$tr
																			.invisible();
																} else
																	$tr
																			.remove();
															});
										});

						$('#addProductBtn', $txDialog).click(
								function() {
									var $tr = $productTbody
											.find("tr:first-child");
									if ($tr.css('visibility') == 'hidden'
											|| $tr.css('display') == 'none') {
										$tr.find("input").removeAttr('value')
												.prop('disabled', false).prop(
														'checked', false);
										$tr.find("select").removeAttr('value')
												.prop('disabled', false);
										$tr.visible();
									} else {
										var $trLast = $productTbody
												.find("tr:last-child");
										var $row = $tr.clone();
										$row.find("input").removeAttr('value')
												.removeAttr("checked").prop(
														'disabled', false);
										$row.find("select").removeAttr('value')
												.prop('disabled', false);
										var index = +$trLast.data("index") + 1;
										var lastIndex = $tr.data("index");
										$row.data("index", index);
										var rg = new RegExp("\\[" + lastIndex
												+ "\\]", 'g');
										var html = $row.html();
										html = html.replace(rg, '[' + index
												+ ']');
										$row.html(html);

										$productTbody.append($row);
										initFields($row);
									}
								});

						initFields($txDialog);

						$('select[name=transactionType]')
								.change(
										function() {
											var newRow = undefined;
											$('tbody > tr', $productTable)
													.each(
															function(index) {
																console
																		.log("removing "
																				+ $(this));
																var $tr = $productTbody
																		.closest("tr");
																var length = $productTbody
																		.find("tr").length;
																if ($productTbody
																		.find(
																				"tr")
																		.is(
																				"tr:first-child")
																		&& length === 1) {
																	console
																			.log("firt child");
																	$tr = $productTbody
																			.find("tr:first-child");
																	$tr
																			.find(
																					"input.productRemoveFlag")
																			.prop(
																					'checked',
																					false);
																	$tr
																			.find(
																					"input")
																			.removeAttr(
																					'value')
																			.prop(
																					'disabled',
																					true);
																	$tr
																			.find(
																					"select")
																			.removeAttr(
																					'value')
																			.prop(
																					'disabled',
																					true);
																	$tr
																			.invisible();
																} else {
																	console
																			.log("removing....");
																	$(this)
																			.remove();
																}
															});

											var $tr = $productTbody
													.find("tr:first-child");
											if ($tr.css('visibility') == 'hidden'
													|| $tr.css('display') == 'none') {
												$tr.find("input").removeAttr(
														'value').prop(
														'disabled', false)
														.prop('checked', false);
												$tr.find("select").removeAttr(
														'value').prop(
														'disabled', false);
												$tr.visible();
											} else {
												var $trLast = $productTbody
														.find("tr:last-child");
												var $row = $tr.clone();
												$row.find("input").removeAttr(
														'value').removeAttr(
														"checked").prop(
														'disabled', false);
												$row.find("select").removeAttr(
														'value').prop(
														'disabled', false);
												var index = +$trLast
														.data("index") + 1;
												var lastIndex = $tr
														.data("index");
												$row.data("index", index);
												var rg = new RegExp("\\["
														+ lastIndex + "\\]",
														'g');
												var html = $row.html();
												html = html.replace(rg, '['
														+ index + ']');
												$row.html(html);
												newRow = $row;

												$productTbody.append(newRow);
												initFields(newRow);
											}

											var transactionType = $(this).val();
											if ('ACTIVATION' === transactionType
													|| 'GIFT_TO_CUSTOMER' === transactionType) {
												$('.amount').prop("disabled",
														"disabled");
											} else {
												$('.amount').prop("disabled",
														"");
											}
										});

						jQuery.fn.visible = function() {
							return this.css('visibility', 'visible');
						};

						jQuery.fn.invisible = function() {
							return this.css('visibility', 'hidden');
						};

						function updateTotalFceAmt() {
							var totalFceAmt = 0;
							$(".faceAmount").each(function() {
								totalFceAmt += +$(this).val();
							});
							$("#totalFaceAmount").val(toCurrency(totalFceAmt));
						}

						function toCurrency(total) {
							return parseFloat(total, 10).toFixed(2).replace(
									/(\d)(?=(\d{3})+\.)/g, "$1,").toString();
						}

						function initFields(e) {
							var selectTxnType = $('select[name=transactionType]');
							var transactionType = selectTxnType.val();

							if ('ACTIVATION' === transactionType
									|| 'GIFT_TO_CUSTOMER' === transactionType) {
								$('.amount').attr("disabled", "disabled");
							}

							$('.amount', $(e))
									.change(
											function() {
												transactionType = selectTxnType
														.val();
												if (transactionType === 'RELOAD') {
													$parent = $(this).closest(
															"tr");
													var prevBalance = $(
															".previousBalance",
															$parent).val();
													var txAmount = $(".amount",
															$parent).val();
													var newBalance = parseInt(prevBalance)
															+ parseInt(txAmount === '' ? 0
																	: txAmount);
													$(".balance", $parent).val(
															newBalance);
												} else {
													$parent = $(this).closest(
															"tr");
													var prevBalance = $(
															".previousBalance",
															$parent).val();
													var txAmount = $(".amount",
															$parent).val();

													var newBalance = prevBalance
															- txAmount;
													$(".balance", $parent).val(
															newBalance);
												}
											});

							$(".cardNo", $(e))
									.change(
											function() {
												$parent = $(this).closest("tr");
												transactionType = selectTxnType
														.val();
												var merchantId = $(
														'#merchantId').val();
												var cardNo = $(this).val();
												if (cardNo !== undefined) {
													$
															.get(
																	ctx
																			+ "/giftcard/pos-lose-tx/validate/"
																			+ cardNo
																			+ "?transactionType="
																			+ transactionType
																			+ "&merchantId="
																			+ merchantId,
																	function(
																			data) {
																		if (data.success) {
																			var cardInfo = data.result;
																			$(
																					".previousBalance",
																					$parent)
																					.val(
																							cardInfo.availableBalance);
																			$(
																					".amount",
																					$parent)
																					.change();
																			if (transactionType === 'ACTIVATION'
																					|| transactionType === 'GIFT_TO_CUSTOMER') {
																				$(
																						".previousBalance",
																						$parent)
																						.val(
																								0);
																				$(
																						".amount",
																						$parent)
																						.val(
																								cardInfo.faceValue);
																				$(
																						".balance",
																						$parent)
																						.val(
																								cardInfo.faceValue);
																			} else {
																				$(
																						".previousBalance",
																						$parent)
																						.val(
																								cardInfo.availableBalance);
																			}
																		} else {
																			renderError(data);
																		}

																	});
												}
											});
						}
					});
</script>
<script src="<spring:url value="/js/common/typeaheadutil.js" />"></script>
<script type='text/javascript'>
	$(document).ready(function() {
		var url = '<c:url value="/store/peoplesoft/list/gcallowed/" />';
		var $paymentStore = $("#merchantId");
		TypeAhead.process($("#paymentStoreName"), $paymentStore, url, {
			label : "codeAndName",
			value : "code"
		});
	});
</script>