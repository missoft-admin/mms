package com.transretail.crm.core.jpa;

import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.hibernate.cfg.AvailableSettings;
import org.hibernate.dialect.H2Dialect;
import org.hibernate.dialect.MySQL5Dialect;
import org.hibernate.dialect.Oracle10gDialect;
import org.hibernate.ejb.HibernateEntityManagerFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.mysema.query.sql.H2Templates;
import com.mysema.query.sql.MySQLTemplates;
import com.mysema.query.sql.OracleTemplates;
import com.mysema.query.sql.SQLTemplates;
import com.transretail.crm.common.dialect.CRMMysqlDialect;
import com.transretail.crm.common.service.exception.GenericServiceException;
import com.transretail.crm.timesten.TimesTenDialect1122;

/**
 * @author Allan G. Ramirez (agramirez@exist.com)
 */
@Component
public class DbMetadataUtil {
    private static final Logger _LOG = LoggerFactory.getLogger(DbMetadataUtil.class);

    public static enum DbType {
        H2, ORACLE, TIMESTEN,MYSQL
    }

    private DbType dbType;
    private SQLTemplates sqlTemplates;

    @Autowired
    public DbMetadataUtil(HibernateEntityManagerFactory entityManagerFactory) throws Exception {
        Map<String, Object> jpaProperties = entityManagerFactory.getProperties();
        String dialectClassName = (String) jpaProperties.get(AvailableSettings.DIALECT);
        if (StringUtils.isBlank(dialectClassName)) {
            throw new GenericServiceException("hibernate.dialect not set in jpaproperties of entitymanager.");
        }
        Class dialectClass = Class.forName(dialectClassName);
        if (H2Dialect.class.isAssignableFrom(dialectClass)) {
            dbType = DbType.H2;
            sqlTemplates = new H2Templates();
        } else if (Oracle10gDialect.class.isAssignableFrom(dialectClass)) {
            dbType = DbType.ORACLE;
            sqlTemplates = new OracleTemplates();
        } else if (TimesTenDialect1122.class.isAssignableFrom(dialectClass)) {
            dbType = DbType.TIMESTEN;
        } else if (CRMMysqlDialect.class.isAssignableFrom(dialectClass)) {
            dbType = DbType.MYSQL;
            sqlTemplates = new MySQLTemplates(true);
        }else {
            _LOG.warn("Dialect {} not supported.", dialectClassName);
        }
    }

    public DbType getDbType() {
        if (dbType == null) {
            throw new GenericServiceException("Current dialect is neither H2 nor ORACLE.");
        }
        return dbType;
    }

    public SQLTemplates getSqlTempates() {
        return sqlTemplates;
    }
}
