    package com.transretail.crm.giftcard.dto;

import com.mysema.query.types.expr.BooleanExpression;
import com.transretail.crm.giftcard.entity.QGiftCardInventory;
import com.transretail.crm.giftcard.entity.support.GiftCardInventoryStatus;

/**
 * @author Allan G. Ramirez (agramirez@exist.com)
 */
public class GiftCardInventoryReceivingSearchDto extends GiftCardInventorySeriesSearchDto {
    @Override
    public BooleanExpression createSearchExpression() {
        QGiftCardInventory qInventory = QGiftCardInventory.giftCardInventory;
        BooleanExpression booleanExpression = qInventory.location.isNull().and(qInventory.status.eq(GiftCardInventoryStatus.DISABLED));
        BooleanExpression seriesExpression = super.createSearchExpression();
        if (seriesExpression != null) {
            booleanExpression.and(seriesExpression);
        }
        return booleanExpression;
    }
}
