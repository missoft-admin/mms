$(document).ready( function() {

	var $theUserList = $( "#list_user" );
	var $theUserSearchForm = $( "#userSearchForm" );
	var $theUserSearchBtn = $( "#userSearchButton" );

	//initFormFields();
	initUserDataTable();



	function initUserDataTable() {

		var theHeaders = { 
			username: $theUserList.data( "hdr-username" ),
			firstName: $theUserList.data( "hdr-firstname" ),
			lastName: $theUserList.data( "hdr-lastname" ),
			enabled: $theUserList.data( "hdr-enabled" )
		};

		$theUserList.ajaxDataTable({
			'autoload' 		: true,
			'ajaxSource' 	: $theUserList.data( "url" ),
			'columnHeaders' : [	
		        theHeaders['username'],
			 	theHeaders['firstName'],
			 	theHeaders['lastName'],
			 	theHeaders['enabled'],
			 	{ text: "", className : "el-width-11" }
			],
			'modelFields' 	: [
			 	{ name 	: 'username', sortable : true },
			 	{ name 	: 'firstName', sortable : true },
			 	{ name 	: 'lastName', sortable : true },
			 	{ name 	: 'enabled', sortable : true },
		        { customCell : function ( innerData, sSpecific, json ) {
			 			if ( sSpecific == 'display' ) {
			 				return $( "#user-list-actions" ).html()
			 					.replace( new RegExp("%ID%", 'g'), json.id )
			 					.replace( new RegExp("%UNAME%", 'g'), json.username )
			 					.replace( new RegExp("%USER%", 'g'), json.username
			 							+ ( (json.firstName && json.lastName)? " - " : "" )
			 							+ ( json.firstName? json.firstName : "" )
			 							+ ( json.lastName? " " + json.lastName : "" ) );
				        } 
			 			else {
			 				return '';
				        }
			 		}
		        }  
			 ]
		})
		.on( "click", ".btn_editUser", editUser )
        .on( "click", ".btn_editUserRolePerms", editUserRolePerms )
		.on( "click", ".btn_deleteUser", deleteUser )
		.on( "click", ".btn_changePassword", editUser )
		.on( "click", ".btn_assignExh", editUser );

		initUserSearchForm( $theUserList );
	}

	function initUserSearchForm( inDataTable ) {
		$theUserSearchBtn.click( function() {
			var $theFormDataObj = $theUserSearchForm.toObject( { mode : 'first', skipEmpty : true } );
			/*if ( $theFormDataObj.username == undefined ) { return true; }*/
			var $btnSearch = $( this );
			$btnSearch.prop( 'disabled', true );
			inDataTable.ajaxDataTable( 
				'search', 
				$theFormDataObj, 
				function () {
					$btnSearch.prop( 'disabled', false );
			});
		});
	}
	
	function assignExh(e) {
		e.preventDefault();
		var url = $(this ).data('url') + $( this ).data( "id" );
		$.get(url, function(data) {
			$( "#form_user" ).html( inResponse );
        }, "html");
	}



	function editUser(e) {
		e.preventDefault();
        var url = $(this ).data('url');
        
		$.ajax({
			cache 		: false, 
			dataType 	: "html",
			type 		: "GET", 
			url 		: url + $( this ).data( "id" ),
			error 		: function(jqXHR, textStatus, errorThrown) {}, 
			success 	: function( inResponse ) {
				$( "#form_user" ).html( inResponse );
			}
		});
	}
	
    var $rolePermDialog = null;

    initRolePermDialog();

    function initRolePermDialog() {
        $rolePermDialog = $( '#rolePermDialog' ).modal( {
            show : false
        } );
        // Dirty hack to https://github.com/twbs/bootstrap/issues/2752 issue */
        $rolePermDialog.on( 'hide', function () {
            if ( !$rolePermDialog.data( 'accordion-clicked' ) ) {
                $( 'input[type="checkbox"]', $rolePermDialog ).prop( 'checked', false );
                $( '.selectedUserId', $rolePermDialog ).val( '' );
            }
            
            $rolePermDialog.find("#email").val("");
        } );
        
        $rolePermDialog.on( 'click', 'button.accordion-toggle', function ( e ) {
        	if($(this).text() == "-") {
        		$(this).text("+");
        	} else {
        		$(this).text("-");
        	}
        } );
        
        $rolePermDialog.on( 'click', 'button.accordion-toggle', function ( e ) {
            if (e.target.nodeName == 'INPUT' || e.target.nodeName == 'LABEL') {
                e.stopImmediatePropagation();
            } else {
                $rolePermDialog.data( 'accordion-clicked', true );
            }
        } );
        
        $( '.collapse', $rolePermDialog ).on( 'hidden', function (e) {
            $rolePermDialog.data( 'accordion-clicked', false );
        } );
        // End of hack
        
        $rolePermDialog.on( 'click', 'input[type="checkbox"].role', function () {
            var $group = $( this ).parents( '.accordion-group' );
            var selectAllPermissions = this.checked;
            $( 'input[type="checkbox"].permission', $group ).prop( 'checked', selectAllPermissions );
            $( 'input[type="checkbox"].supervisor', $group ).prop( 'checked', selectAllPermissions );
            $( 'input[type="checkbox"].supervisor', $group ).each(checkallSupervisorPerm);
        } );

        $rolePermDialog.on( 'click', 'input[type="checkbox"].permission', function () {
            var $group = $( this ).parents( '.accordion-group' );
            var isPermissionSelected = $( 'input[type="checkbox"].permission:checked', $group ).length > 0;
            $( 'input[type="checkbox"].role', $group ).prop( 'checked', isPermissionSelected );
        } );
        
        $rolePermDialog.on( 'click', 'input[type="checkbox"].supervisor', checkallSupervisorPerm );
        
        function checkallSupervisorPerm() {
            var $group = $( this ).parents( '.accordion-group' );
            var isSupervisorSelected = this.checked;
            /*$( 'li.supervisorPerm', $group ).toggle(isSupervisorSelected);*/
            $( 'input[type="checkbox"].supervisorPerm', $group ).prop( 'checked', isSupervisorSelected );
        }
        
        $('#saveRolePerm' ).click(function(e) {
            var o = new Object();
            var el = e.target;
            el.disabled = true;
            o.rolePermTuples = new Array();
            $( 'input[type="checkbox"]:checked', $rolePermDialog ).each( function ( index, Element ) {
              if (Element.id != "LCSubHeader" && Element.id != "GCPSubHeader" && Element.id != "GCISubHeader" && Element.id != "GCSSubHeader"
                    && Element.id != "GCCSubHeader" && Element.id != "MaSubHeader" && Element.id != "MeSubHeader" && Element.id != "EmSubHeader"
                      && Element.id != "GCSubHeader") {
                o.rolePermTuples.push( Element.id );
              }
            } );
            var doSave = true;
            if(o.rolePermTuples.length < 1) {
                doSave = confirm( "Are you sure you want to remove all role and permissions?" );
            }
            checkSupervisorRoles( o, function( done ) {
                if( doSave && done ) {
                    var selectedUserId = $( '.selectedUserId', $rolePermDialog ).val();
                    var jsonData = JSON.stringify( o );
                    $.ajax( {
                        "contentType" : 'application/json',
                        headers : { 'Content-Type' : 'application/json' },
                        "type" : "POST",
                        "url" : ctx + '/user/' + selectedUserId + '/permroles/update',
                        "data" : jsonData,
                        "success" : function () {
                            el.disabled = false;
                            $rolePermDialog.modal('toggle');
                        },
                        "error" : function ( xhr, textStatus, error ) {
                            el.disabled = false;
                            if ( textStatus === 'timeout' ) {
                                alert( 'The server took too long to send the data.' );
                            } else if ( error ) {
                                alert( error );
                            } else {
                                alert( 'An error occurred on the server. Please try again.' );
                            }
                        }
                    } );
                } else {
                    console.log('test');
                    el.disabled = false;
                }
            })
        });
    }

    function checkSupervisorRoles( permRoles, callback ) {
		var errorCon = $rolePermDialog.find( "#content_error" );
    	var user = new Object();
    	user.rolePerm = permRoles;
    	user.email = $rolePermDialog.find( '#email' ).val();
    	user.id = $( '.selectedUserId', $rolePermDialog ).val();

    	$.ajax({ 
    		contentType : 'application/json',
            headers : { 'Content-Type' : 'application/json' },
            type : "POST",
            url : ctx + "/user/permroles/update/validate/",
            data : JSON.stringify( user ),
            success : function( resp ) {
            	if ( !resp.success ) {
					var error = "";
					$.each( resp.result, function( key, value ) {
						error += ( ( key + 1 ) + ". " + value + "<br>" );
					});
					errorCon.find( "div" ).html( error );
					errorCon.show( "slow" );
					$rolePermDialog.find( '#content_email' ).show( "slow" );
            	}
            	else {
            		$rolePermDialog.find( '#content_email' ).hide( "slow" );
            		errorCon.hide( "slow" );
            	}
            	if ( callback ) callback( resp.success );
            },
            error	: function( xhr, textStatus, error ) {
                if ( textStatus === 'timeout' ) {
                    alert( 'The server took too long to send the data.' );
                } 
                else if ( error ) {
                    alert( error );
                } 
                else {
                    alert( 'An error occurred on the server. Please try again.' );
                }
            	callback( false );
            }
    	});
    }

    function editUserRolePerms() {
        //var $rolePermContainer = $('.modal-body', $rolePermDialog ).empty();
    	$( '#selectedUser', $rolePermDialog ).html( $( this ).data( 'user' ) );
    	var $rolePermContainer = $('#rolePerm', $rolePermDialog ).empty();
        var selectedUserId = $( this ).data( 'id' );
        var supervisorLbl = $rolePermContainer.data("supervisorLabel");
        $.ajax( {
            "type" : "GET",
            "url" : ctx + '/user/' + selectedUserId + '/permroles',
            "success" : function ( data ) {
                var htmlString = '';
                rolePermTuples = new Array();
                if ( data.length > 0 ) {
                	
                	var supervisorPerms = {};
                	
                	for(var i = 0; i < data.length; i++) {
                		var role = data[i].role;
                		var permissions = data[i].permissions;
                		if(role.staff != null && permissions.length > 0) {
	                		var roleCode = role.code;
	                		supervisorPerms[roleCode] = permissions;
                		}
                	}
                	
                	  //Part of static code salesOrderOnce
                	  var salesOrderOnce = false;
                    for(var i = 0; i < data.length; i++) {
                        var role = data[i].role;
                        if(role.staff == null) {
                        	var permissions = data[i].permissions;
                            var supervisor = role.supervisor;
                            var id = role.code;
                            rolePermTuples[id] = null;

                            htmlString += '<div class="accordion-group">';
                            htmlString += ' <div class="accordion-heading">';
                            htmlString += '  <a class="" style="display: block; padding: 8px 15px;" >';
                            htmlString += '   <div title="Filter Category" class="clearfix">';
                            htmlString += '    <div class="pull-right"><button style="display: inline-block; padding: 2px 10px; width: 30px;" class="btn btn-small accordion-toggle" data-toggle="collapse" data-target="#collapse-' + role.code + '" href="#">'+(i == 0 ? '-' : '+')+'</button>&nbsp;&nbsp;</div>';
                            htmlString += '    <div class="pull-left checkbox">';
                            var rChecked = '';
                            if(role.currentRolePerm) {
                                rChecked = 'checked';
                                rolePermTuples[id] = id;
                            }
                            htmlString += '     <label class="selec capitalize">' +
                                    '<input class="role" id="' + id + '" type="checkbox" ' + rChecked + ' /> ' + role.desc + '</label>';
                            
                            htmlString += '    </div>';
                           
                            htmlString += '   </div>';
                            htmlString += '  </a>';
                            htmlString += ' </div>';
                            htmlString += ' <div id="collapse-' + id + '" class="collapse ' + (i == 0 ? 'in' : '')+ '">';
                            htmlString += '  <div class="accordion-inner">';
                            htmlString += '   <ul>';
                            var temp = false;
                            var temp2 = false;
                            
                            for(var j = 0; j < permissions.length; j++) {
                              var perm = permissions[j];
                              var id = role.code + "-" + perm.code;
                              rolePermTuples[id] = null;
                              
                              //Start of static code//
                              var merchantServiceSubHeader = '';
                              var addHeader = false;
                              var subHeader = '';
                              var addHeader2 = false;
                              if (perm.code == 'LC_BURN_CARDS') {
                                merchantServiceSubHeader = 'Loyalty Card Section';
                                addHeader = true;
                                subHeader = "LCSubHeader";
                              }
                              else if (perm.code == 'GC_MO') {
                                merchantServiceSubHeader = 'Gift Card – Procurement Section';
                                addHeader = true;
                                subHeader = "GCPSubHeader";
                              }
                              else if (perm.code == 'GC_ADJUST_INVENTORY') {
                                merchantServiceSubHeader = 'Gift Card – Inventory Section';
                                addHeader = true;
                                subHeader = "GCISubHeader";
                              }
                              else if (perm.code == 'GC_GENERAL_DISCOUNT' && salesOrderOnce == false) {
                                merchantServiceSubHeader = 'Gift Card – Sales Section';
                                addHeader = true;
                                subHeader = "GCSSubHeader";
                                salesOrderOnce = true;
                              }
                              else if (perm.code == 'GC_B2B_EXHIBITION') {
                                merchantServiceSubHeader = 'Gift Card – Customer Service Section';
                                addHeader = true;
                                subHeader = "GCCSubHeader";
                                temp = true;
                              }
                              else if (perm.code == 'REPORT_BUYING_CUSTOMER') {
                                merchantServiceSubHeader = 'Marketing Section';
                                addHeader = true;
                                subHeader = "MaSubHeader";
                              }
                              else if (perm.code == 'REPORT_MEMBER_CARD_ACTIVATION_SUMMARY_VIEW') {
                                merchantServiceSubHeader = 'Member Section';
                                addHeader = true;
                                subHeader = "MeSubHeader";
                              }
                              else if (perm.code == 'REPORT_EMPLOYEE_PURCHASE_AND_REWARD_SUMMARY_VIEW') {
                                merchantServiceSubHeader = 'Employee Section';
                                addHeader = true;
                                subHeader = "EmSubHeader";
                              }
                              else if (perm.code == 'GC_ACCOUNT_RECEIVABLE_AGE') {
                                merchantServiceSubHeader = 'Gift Card Section';
                                addHeader = true;
                                subHeader = "GCSubHeader";
                                temp2 = true;
                              }

                              if (addHeader) {
                                if (j > 0) {
                                  htmlString += '    </ul></div></div></div>';
                                }
                                htmlString += '<div class="accordion-group-sub">';
                                htmlString += ' <div class="accordion-heading-sub">';
                                htmlString += '  <a class="" style="display: block; padding: 8px 15px;" >';
                                htmlString += '   <div title="Filter Category" class="clearfix">';
                                htmlString += '    <div class="pull-right"><button style="display: inline-block; padding: 2px 10px; width: 30px;" class="btn btn-small accordion-toggle" data-toggle="collapse" data-target="#collapse-' + subHeader + '" href="#">'+(i == 0 ? '-' : '+')+'</button>&nbsp;&nbsp;</div>';
                                htmlString += '    <div class="pull-left checkbox">';

                                var pChecked = '';
                                htmlString += '      <label class="selec">' +
                                  '<input class="permission" onclick="var i=$(this).attr(\'id\');$(\'#collapse-\'+i).find(\':checkbox\').prop(\'checked\',$(this).prop(\'checked\'));" id="' + subHeader + '" type="checkbox" ' + pChecked + ' /> ' + merchantServiceSubHeader + '</label>';

                                htmlString += '    </div>';

                                htmlString += '   </div>';
                                htmlString += '  </a>';
                                htmlString += ' </div>';
                                htmlString += ' <div id="collapse-' + subHeader + '" class="collapse ' + (i == 0 ? 'in' : '')+ '">';
                                htmlString += '  <div class="accordion-inner">';
                                htmlString += '   <ul>';

                              }
                              //End of static code//

                              htmlString += '    <li>';
                              htmlString += '     <div class="checkbox">';
                              var pChecked = '';
                              if ( perm.currentRolePerm ) {
                                  pChecked = 'checked';
                                  rolePermTuples[id] = id;
                              }
                              htmlString += '      <label class="selec">' +
                                      '<input class="permission" id="' + id + '" type="checkbox" ' + pChecked + ' /> ' + perm.desc + '</label>';
                              htmlString += '     </div>';
                              htmlString += '    </li>';
                            }
                            
                            if(supervisor != null && supervisor != "" && !temp) {
                            	rolePermTuples[supervisor.code] = null;
                            	htmlString += '    <li>';
                                htmlString += '     <div class="checkbox">';
                                var sChecked = '';
                                if(supervisor.currentRolePerm) {
                                	sChecked = 'checked';
                                    rolePermTuples[supervisor.code] = supervisor.code;
                                }
                                htmlString += '      <label class="selec">' +
                                        '<input class="supervisor" id="' + supervisor.code + '" type="checkbox" ' + sChecked + ' /> ' + supervisorLbl + '</label>';
                                htmlString += '     </div>';
                                htmlString += '    </li>';

                                var supPerms = supervisorPerms[supervisor.code];
                                
                                if(supPerms != null && supPerms.length > 0) {
                                	
                                	for(var k = 0; k < supPerms.length; k++) {
                                		var supPerm = supPerms[k];
                                		var id = supervisor.code + "-" + supPerm.code;
                                        rolePermTuples[id] = null;
                                        
                                		htmlString += '    <li class="supervisorPerm">';
                                        htmlString += '     <div class="checkbox">';
                                        var sPChecked = '';
                                        if(supPerm.currentRolePerm) {
                                        	sPChecked = 'checked';
                                            rolePermTuples[id] = id;
                                        }
                                        htmlString += '      <label class="selec">' +
                                                '<input class="supervisorPerm" id="' + id + '" type="checkbox" ' + sPChecked + ' style="visibility:hidden;" /> ' + supPerm.desc + '</label>';
                                        htmlString += '     </div>';
                                        htmlString += '    </li>';
                                        
                                	}
                                }

                            } else if(supervisor != null && supervisor != "" && temp) {
                              rolePermTuples[supervisor.code] = null;
                              htmlString += '    </ul></div></div>';
                              htmlString += '<div class="accordion-group-sub">';
                              htmlString += ' <div class="accordion-heading-sub">';
                              htmlString += '  <a class="" style="display: block; padding: 8px 15px;" >';
                              htmlString += '   <div title="Filter Category" class="clearfix">';
                              htmlString += '    <div class="pull-right"><button style="display: inline-block; padding: 2px 10px; width: 30px;" class="btn btn-small accordion-toggle" data-toggle="collapse" data-target="#collapse-Supervisor" href="#">'+(i == 0 ? '-' : '+')+'</button>&nbsp;&nbsp;</div>';
                              htmlString += '    <div class="pull-left checkbox">';

                              var sChecked = '';
                              if(supervisor.currentRolePerm) {
                                sChecked = 'checked';
                                rolePermTuples[supervisor.code] = supervisor.code;
                              }
                              htmlString += '      <label class="selec">' +
                                '<input class="supervisor" id="' + supervisor.code + '" type="checkbox" ' + sChecked + ' /> ' + supervisorLbl + '</label>';

                              htmlString += '    </div>';

                              htmlString += '   </div>';
                              htmlString += '  </a>';
                              htmlString += ' </div>';
                              htmlString += ' <div id="collapse-Supervisor" class="collapse ' + (i == 0 ? 'in' : '')+ '">';
                              htmlString += '  <div class="accordion-inner">';
                              htmlString += '   <ul>';

                              var supPerms = supervisorPerms[supervisor.code];

                              if(supPerms != null && supPerms.length > 0) {

                                for(var k = 0; k < supPerms.length; k++) {
                                  var supPerm = supPerms[k];
                                  var id = supervisor.code + "-" + supPerm.code;
                                  rolePermTuples[id] = null;

                                  htmlString += '    <li class="supervisorPerm">';
                                  htmlString += '     <div class="checkbox">';
                                  var sPChecked = '';
                                  if(supPerm.currentRolePerm) {
                                    sPChecked = 'checked';
                                    rolePermTuples[id] = id;
                                  }
                                  htmlString += '      <label class="selec">' +
                                    '<input class="supervisorPerm" id="' + id + '" type="checkbox" ' + sPChecked + ' style="visibility:hidden;" /> ' + supPerm.desc + '</label>';
                                  htmlString += '     </div>';
                                  htmlString += '    </li>';

                                }
                              }
                              htmlString += '   </ul>';
                              htmlString += '  </div>';
                            } else if (temp2) {
                              htmlString += '  </div></div></div>';
                            }
                            htmlString += '   </ul>';
                            htmlString += '  </div>';
                            htmlString += ' </div>';
                            htmlString += '</div></div>';
                        }
                        
                    }
                } else {
                    htmlString += 'No roles found';
                }
                $( '.selectedUserId', $rolePermDialog ).val( selectedUserId );
                $rolePermContainer.append( htmlString );
              if ($('#collapse-LCSubHeader input:checked').length == 5) { $('#LCSubHeader').prop('checked', true); }
              if ($('#collapse-GCPSubHeader input:checked', $rolePermContainer).length == 3) { $('#GCPSubHeader').prop('checked', true); }
              if ($('#collapse-GCISubHeader input:checked', $rolePermContainer).length == 6) { $('#GCISubHeader').prop('checked', true); }
              if ($('#collapse-GCSSubHeader input:checked', $rolePermContainer).length == 2) { $('#GCSSubHeader').prop('checked', true); }
              if ($('#collapse-GCCSubHeader input:checked', $rolePermContainer).length == 5) { $('#GCCSubHeader').prop('checked', true); }
              if ($('#collapse-MaSubHeader input:checked', $rolePermContainer).length == 20) { $('#MaSubHeader').prop('checked', true); }
              if ($('#collapse-MeSubHeader input:checked', $rolePermContainer).length == 7) { $('#MeSubHeader').prop('checked', true); }
              if ($('#collapse-EmSubHeader input:checked', $rolePermContainer).length == 5) { $('#EmSubHeader').prop('checked', true); }
              if ($('#collapse-GCSubHeader input:checked', $rolePermContainer).length == 24) { $('#GCSubHeader').prop('checked', true); }
                $rolePermDialog.modal( 'toggle' );
            },
            "error" : function ( xhr, textStatus, error ) {
                if ( textStatus === 'timeout' ) {
                    alert( 'The server took too long to send the data.' );
                } else if ( error ) {
                    alert( error );
                } else {
                    alert( 'An error occurred on the server. Please try again.' );
                }
            }
        } );
    }

	function deleteUser(e) {
		e.preventDefault();
		var deleteUrl = $( "#btn_deleteUser" ).data( "url" ) + $( this ).data( "id" );
		getConfirm( $( "#btn_deleteUser" ).data( "msg-confirm" ), function( result ) {
			if( result ) {
				$.post( 
						deleteUrl,
						function( inResponse, textStatus, jqXHR ) { $( "#list_user" ).ajaxDataTable( "search" ); },
						"json" );
			}
		});
	}

  function eventsubHeader(e) {
    if ($(e.target).prop('checked')) {
      $(e.target).siblings('ul').find("input[type='checkbox']").prop('checked', true);
    } else {
      $(e.target).siblings('ul').find("input[type='checkbox']").prop('checked', false);
    }
  }
});