<%@ include file="../../common/taglibs.jsp" %>

<style type="text/css">
<!--
  .well .input {
    background-color: #fff;
    border: 1px solid #E8E8E8;
    box-shadow: none;    
    padding: 6px;
    height: 18px;
    margin-left: 5px;
  }
  .well .btn-primary {
    margin-left: 5px;
  }
  .well select, .well .input {
    margin: 5px;
  }
  .label-single {
      width: 150px;
  }
  
-->
</style>

<div id="form_SearchForm" class="form-horizontal block-search well">
  <div id="control-group" class="form-inline">
  
    <div class="form-inline">
      <label for="" class="label-single"><h5><spring:message code="search_by"/></h5></label>
      
      <select id="searchField">
        <c:forEach var="item" items="${dateExpirySearchFields}">
          <option value="${item}"><spring:message code="gc_date_expiry_search_lbl_${item}" /></option>
        </c:forEach>
      </select>
      <input name="" id="searchValue" class="input" placeholder="Search"/>
      <input name="" id="searchValue2" class="input" placeholder="Series To" name="seriesTo" maxlength="16"/>
      
      <input type="button" id="searchButton" value="<spring:message code="label_search"/>" class="btn btn-primary"/>
      <input id="clearButton" type="button" value="<spring:message code="label_clear" />" class="btn"/>
    </div>
    
    <div class="form-inline">
      <label for="" class="label-single"><h5><spring:message code="search_filter"/>:</h5></label>
      <select name="status" class="searchDropdown">
        <option value=""><spring:message code="search_status"/></option>
          <c:forEach var="item" items="${status}">
            <option value="${item}">${item}</option>
          </c:forEach>
      </select>
      
      <spring:message code="search_date_from" var="dateFromLabel"/>
      <input type="text" id="dateFrom" name="dateFrom" placeholder="${dateFromLabel}" class="input"/>
      <spring:message code="search_date_to" var="dateToLabel"/>
      <input type="text" id="dateTo" name="dateTo" placeholder="${dateToLabel}" class="input"/>
    </div>
    
  </div>
</div>

<script src='<c:url value="/js/bootstrap/bootstrap-datepicker.js"/>' type='text/javascript'><![CDATA[&nbsp; ]]></script>
<link href="<c:url value="/css/bootstrap/datepicker.css" />" rel="stylesheet" />
<script type="text/javascript">
$(document).ready(function() {
  var $theSearchForm = $( "#form_SearchForm" );
  var $theSearchBtn = $( "#searchButton" );
  
  initFormFields();
  
  function initFormFields() {

    $("#searchField").change(function(e) {
			e.preventDefault();
			
			if ($("#searchField").val() == "seriesFrom") {
			  $("#searchValue").attr("placeholder","Series From");
			  $("#searchValue").attr("name",$(this).val());
			  $("#searchValue").attr("maxLength", "16");
			  $("#searchValue2").val("");
			  $("#searchValue2").show();
			} else {
			  $("#searchValue").attr("placeholder","Search");
			  $("#searchValue").attr("name",$(this).val());
			  $("#searchValue").removeAttr("maxLength");
			  $("#searchValue2").hide();
			}
	}).change();
    
    $("#dateFrom").datepicker({
      autoclose : true,
      format : "dd-mm-yyyy",
    }).on('changeDate', function(selected){
      transactionDate = new Date(selected.date.valueOf());
      transactionDate.setDate(transactionDate.getDate(new Date(selected.date.valueOf())));
      $("#dateTo").val("");
    });
    
    $("#dateTo").datepicker({
      autoclose : true,
      format : "dd-mm-yyyy",
    }).on('changeDate', function(selected){
      transactionDate = new Date(selected.date.valueOf());
      transactionDate.setDate(transactionDate.getDate(new Date(selected.date.valueOf())));
    });
    
    initSearchForm($("#list_date_expiry"));
  }
  
  function initSearchForm( inDataTable ) {
    $theSearchBtn.click( function() {
      var $theFormDataObj = $theSearchForm.toObject( { mode : 'first', skipEmpty : true } );
      var $btnSearch = $( this );
      $btnSearch.prop( 'disabled', true );
      inDataTable.ajaxDataTable( 
        'search', 
        $theFormDataObj, 
        function () {
          $btnSearch.prop( 'disabled', false );
      });
    });
    
    $("#clearButton").click(function(e) {
      e.preventDefault();
      $("#searchField option:first").attr('selected','selected');
      $("#dateFrom").val("");
      $("#dateTo").val("");
      $(".searchDropdown").val("");
      $("#searchValue").val("");
      $("#searchValue").attr("name","");
      $("#searchValue").attr("placeholder","Search");
      $("#searchValue2").val("");
      $("#searchValue2").hide();
      $("#searchButton").click();
    });
  }
});
</script>