    create table CRM_PROD_HIER (
        CODE varchar(255) not null,
        CREATED_DATETIME timestamp,
        ENGLI_DESC varchar(255),
        LAST_UPDATED_DATETIME timestamp,
        HIER_LEVEL integer,
        LOCAL_DESC varchar(255),
        PARENT_HIER_LEVEL varchar(255),
        primary key (CODE)
    );