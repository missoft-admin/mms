-- member 5 (transaction includes excluded product 111110000001 and included product 111110000002)

INSERT INTO POS_TRANSACTION (ID, TRANSACTION_DATE, STORE_ID) 
VALUES ('010', '2013-10-01 00:00:02', 1);

INSERT INTO POS_TX_ITEM (ID, PRODUCT_ID, QUANTITY, SALES_TYPE, POS_TXN_ID) 
VALUES ('015', '111110000001', 1, 'SALE', '010');

INSERT INTO POS_TX_ITEM (ID, PRODUCT_ID, QUANTITY, SALES_TYPE, POS_TXN_ID) 
VALUES ('016', '111110000002', 1, 'SALE', '010');

INSERT INTO CRM_POINTS (ID, STORE_CODE, TXN_AMOUNT, TXN_POINTS, TXN_DATE, TXN_NO, TXN_TYPE, MEMBER_ID)
VALUES ('1001', 'LEBAK_BULUS', 7922.0, 792, '2010-01-01 00:00:00', '010', 'EARN', 5);

-- member3 (excluded product sample transaction)

INSERT INTO POS_TRANSACTION (ID, TRANSACTION_DATE, STORE_ID) 
VALUES ('011', '2013-10-01 00:00:02', 1);

INSERT INTO POS_TX_ITEM (ID, PRODUCT_ID, QUANTITY, SALES_TYPE, POS_TXN_ID) 
VALUES ('017', '111110000001', 1, 'SALE', '011');

INSERT INTO CRM_POINTS (ID, STORE_CODE, TXN_AMOUNT, TXN_POINTS, TXN_DATE, TXN_NO, TXN_TYPE, MEMBER_ID)
VALUES ('1002', 'LEBAK_BULUS', 7922.0, 792, '2010-01-01 00:00:00', '011', 'EARN', 3);