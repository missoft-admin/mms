<%@ include file="../../common/taglibs.jsp" %>


<style type="text/css">
<!--
  #productTable tr td:last-child { border-left: 0px; }
  #addProductBtn.btn-small { margin-bottom: 5px; }
  table select, textarea, table input[type="text"], input[type="password"], input[type="datetime"], input[type="datetime-local"], input[type="date"], input[type="month"], input[type="time"], input[type="week"], input[type="number"], input[type="email"], input[type="url"], input[type="search"], input[type="tel"], input[type="color"], .uneditable-input {
      margin-bottom: 0px;
      margin-top: 0px;
  }
-->
</style>
<script src="<spring:url value="/js/common/typeaheadutil.js" />"></script>


<div class="modal-dialog" id="egcOrderContainer">
  <div class="modal-content">
    <div class="modal-header">
      <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
      <h4><spring:message code="gc_order_egc"/></h4>
    </div>

    <div class="modal-body">
      <form:form id="orderForm" name="orderForm" modelAttribute="orderForm" action="${pageContext.request.contextPath}/giftcard/egc/order/save/" method="POST" enctype="application/x-www-form-urlencoded" cssClass="form-reset form-horizontal" >
        <div id="contentError" class="hide alert alert-error">
          <button type="button" class="close" onclick="$(this).parent().hide();">&times;</button>
          <div><form:errors path="*"/></div>
        </div>

        <div class="contentMain">
          <form:hidden path="id"/>

          <div class="row-fluid">
            <div class="span6">
              <div class="control-group">
                <spring:message var="moNo" code="order_mo_number" />
                <label for="moNumber" class="control-label">${moNo}</label> 
                <div class="controls"><form:input path="moNumber" class="form-control intInput" readonly="true" placeholder="${moNo}" /></div>
              </div>
  
              <div class="control-group">
                <spring:message var="moDt" code="order_mo_date" />
                <label for="moDate" class="control-label">${moDt}</label> 
                <div class="controls"><form:input path="moDate" class="form-control orderDate" placeholder="${moDt}" /></div>
              </div>

              <div class="control-group">
                <spring:message var="poNo" code="order_po_number" />
                <label for="poNumber" class="control-label"><b class="required">*</b>${poNo}</label> 
                <div class="controls"><form:input path="poNumber" class="form-control intInput" placeholder="${poNo}" /></div>
              </div>
              
              <div class="control-group">
                <spring:message code="order_po_date" var="poDt" />
                <label for="poDate" class="control-label"><b class="required">*</b>${poDt}</label> 
                <div class="controls"><form:input path="poDate" class="form-control orderDate" placeholder="${poDt}" /></div>
              </div>
      
              <c:if test="${orderForm.isEgc}">
              <div class="control-group">
                <spring:message var="lbl_isEgc" code="order_isegc" />
                <label for="poDate" class="control-label">${lbl_isEgc}</label> 
                <div class="controls">
                  <input type="checkbox" ${orderForm.isEgc? 'checked' : ''} disabled="disabled" class="form-control checkbox" />
                  <form:hidden path="isEgc" />
                </div>
              </div>
              </c:if>
            </div>


            <div class="span6">
              <div class="pull-left" style="margin-right: 5px;">
                <button type="button" id="addProductBtn" class="btn btn-small">+</button><br/>
                <button type="button" id="removeProductBtn" class="btn btn-small">&ndash;</button>
              </div>
              <div class="pull-left" style="width:90%;">
                <table id="productTable" class="table table-condensed table-compressed table-striped">
                  <thead>
                  <tr>
                    <th></th>
                    <th><spring:message code="order_item_card_type"/></th>
                    <th><b class="required">*</b><spring:message code="order_item_quantity"/></th>
                  </tr>
                  </thead>
                  <tbody>
                  <c:forEach var="item" items="${orderForm.items}" varStatus="status">
                    <tr data-index="${status.index}">
                      <td><input type="checkbox" class="productRemoveFlag"/></td>
                      <td>
                        <%-- <select class="input-medium" name="items[${status.index}].profileId" class="profile">
                          <c:forEach items="${profiles}" var="profile">
                            <option value="${profile.id}" data-cost="${profile.unitCost}" <c:if test="${profile.id == item.profileId}">selected</c:if>>${profile.productDesc}</option>
                          </c:forEach>
                        </select>
                        <form:hidden path="items[${status.index}].unitCost" class="unit-cost"/> --%>
		                    <div>
		                      <form:input path="items[${status.index}].productName" id="profileDesc${status.index}" cssClass="productDesc" />
		                      <form:hidden path="items[${status.index}].profileId" id="profileId${status.index}" class="input-small productId"
		                         data-unit-cost="${item.unitCost}" data-face-value="${item.faceValue}" />
		                      <script type='text/javascript'>
		                      $(document).ready( function() {
		                          var url = '<c:url value="/gc/productprofile/list/" />';
		                          var $profileDesc = $( "#productTable" ).find( "input[name='items[${status.index}].productDesc']" );
		                          var $hiddenId = $( "#productTable" ).find( "input[name='items[${status.index}].productId']" );
		                          TypeAhead.process( $profileDesc, $hiddenId, url, { label: "productDesc", value: "id" }, 
		                              [ "faceValueDesc", "unitCost" ], 
		                              function( $item ) {
		                                  $hiddenId.data( "face-value", $item[ "faceValueDesc" ] );
		                                  $hiddenId.data( "unit-cost", $item[ "unitCost" ] );
		                                  $hiddenId.change();
		                          });
		                      });
		                      </script>
		                      <form:hidden path="items[${status.index}].unitCost" class="unit-cost"/>
		                    </div>
                      </td>
                      <td><input type="text" class="input-mini quantity" value="${item.quantity}" name="items[${status.index}].quantity"/>
                      </td>
                    </tr>
                  </c:forEach>
                  </tbody>
                </table>
              </div>
              <div class="clearFix"></div>
            </div>
          </div>
        </div>
      </form:form>
    </div>

    <div class="modal-footer" id="moOrderCtrlBtns">
      <button type="button" data-status="FOR_APPROVAL" class="orderSubmit btn btn-primary"><spring:message code="label_submit_for_approval" /></button>
      <button type="button" data-status="DRAFT" class="orderSubmit btn btn-primary"><spring:message code="label_draft" /></button>
      <button type="button" id="orderCancel" class="btn btn-default" data-dismiss="modal"><spring:message code="label_cancel"/></button>
    </div>
  </div>
</div>


<script>
$(document).ready(function() {

    $orderDialog = $( "#orderDialog" ).css( { "width": "900px" } );
    var $container = $( "#egcOrderContainer" );
    var CONTAINER_ERROR = $container.find( "#contentError > div" );
    var CONTENT_ERROR = $container.find( "#contentError" );
    var $form = $container.find( "#orderForm" );
    var $submit = $container.find( ".orderSubmit" );
    var $productTable = $container.find( "#productTable" );
    var $productTbody = $( "tbody", $productTable );
    var $removeProductBtn = $( "#removeProductBtn", $container );
    var $addProductBtn = $( "#addProductBtn", $container );

    initBtnsFields();
    function initBtnsFields() {
        $submit.click( function(e) {
            e.preventDefault();
            $submit.prop( "disabled", true );
            $.post( $form.attr("action") + $(this).data( "status" ), $form.serialize(), function(data) {
                $submit.prop( "disabled", false );
                if (data.success) { location.reload(); } 
                else {
                    var errorInfo = "";
                    for (i = 0; i < data.result.length; i++) {
                        errorInfo += "<br>" + (i + 1) + ". " + ( data.result[i].code != undefined ? data.result[i].code : data.result[i] );
                    }
                    CONTAINER_ERROR.html( "Please correct following errors: " + errorInfo );
                    CONTENT_ERROR.show( 'slow' );
                }
            });
        });

        $( "input.orderDate" ).datepicker({ format: 'dd-mm-yyyy', autoclose: true, endDate: '+0d' });
        $( "input[name='moDate']" ).datepicker( "setDate", new Date() ).datepicker( "setStartDate", new Date() );
        /* $form.find( "select" ).change( function() {
            $(this).siblings( ".unit-cost" ).val($(this).find(":selected").data( "cost" ) );
            console.log( $(this).siblings( ".unit-cost" ).val() );
        }).change(); */
        $form.find(".productId").change( function() {
            $(this).siblings( ".unit-cost" ).val( $(this).data( "unit-cost" ) );
            console.log( $(this).siblings(".unit-cost").val() );
        }).change();

        $removeProductBtn.click( function() {
            $( "input.productRemoveFlag:checked" ).each( function() {
                var $tr = $(this).closest("tr");
                var length = $productTbody.find("tr").length;
                if( $tr.is( ":first-child" ) && length == 1 ) {
                    $tr.find( "input.productRemoveFlag" ).prop( "checked", false );
                    $tr.find( "input" ).removeAttr( "value" ).prop( "disabled", true );
                    $tr.find( "select" ).removeAttr( "value" ).prop( "disabled", true );
                    $tr.invisible();
                } 
                else {
                    $tr.remove();
                }
            });
        });

        $addProductBtn.click( function() {
            var $tr = $productTbody.find("tr:first-child");
            if($tr.css('visibility') == 'hidden' || $tr.css('display') == 'none') {
                $tr.find("input").removeAttr('value').prop('disabled', false);
                $tr.find("select").removeAttr('value').prop('disabled', false);
                $tr.visible();
            } 
            else {
                var $trLast = $productTbody.find("tr:last-child");
                var  $row = $tr.clone();
                $row.find("input").removeAttr('value').prop('disabled', false);
                $row.find("select").removeAttr('value').prop('disabled', false);

                var index = +$trLast.data("index") + 1;
                var lastIndex = $tr.data("index");
                $row.data("index", index);

                var rg = new RegExp("\\["+lastIndex+"\\]", 'g');
                var html = $row.html();
                html = html.replace(rg, '['+index+']');
                $row.html(html);
                /* $row.find('select').change(function() {
				            $(this).siblings(".unit-cost").val($(this).find(":selected").data('cost'));
				            console.log($(this).siblings(".unit-cost").val());
		            }); */
		            $row.find(".productId").change( function() {
		                $(this).siblings( ".unit-cost" ).val( $(this).data( "unit-cost" ) );
		                console.log( $(this).siblings(".unit-cost").val() );
		            });
                $productTbody.append($row);
                initFields( $row );
            }
        });
    }

    initFields( $container );
    function initFields( $con ) {
        var url = '<c:url value="/gc/productprofile/list/true/" />';
        var $hiddenId = $($con).find( ".productId" );
        TypeAhead.process( $($con).find( ".productDesc" ), $hiddenId, url, { label: "productDesc", value: "id" }, 
            [ "faceValueDesc", "unitCost" ], 
            function( $item ) {
                $hiddenId.data( "face-value", $item[ "faceValueDesc" ] );
                $hiddenId.data( "unit-cost", $item[ "unitCost" ] );
                $hiddenId.change();
        });
        $('.quantity', $con).numberInput({"type" : "int"});
        $('.intInput', $con).numberInput({"type" : "int"});
        $('.floatInput', $con).numberInput({"type" : "float"});
    }

	  jQuery.fn.visible = function() { return this.css('visibility', 'visible'); };
	  jQuery.fn.invisible = function() { return this.css('visibility', 'hidden' ); };

});
</script>