package com.transretail.crm.core.dto;


import org.joda.time.LocalDate;

import com.transretail.crm.core.entity.enums.StampTxnType;


public class StampPromoTxnDto {

	private Long promoID;
	private String promo;
	private String storeCode;
	private String store;
	private LocalDate promoStart;
	private LocalDate promoEnd;
	private String sku;
	private String productName;
	private Long itemQty;
	private LocalDate redeemTo;
	private Long redeemedStamps;
	private Long availableStamps;
	private Long remainingStamps;
    private StampTxnType txnType;



	public StampPromoTxnDto() {}
	public StampPromoTxnDto( Long promoId, String promo, String storeCode, String storeName,
			String sku, String productDesc, LocalDate redeemTo, Double itemQty ) {
		this.promoID = promoId;
		this.promo = promo;
		this.storeCode = storeCode;
		this.store = storeName;
		this.sku = sku;
		this.productName = productDesc;
		this.redeemTo = redeemTo;
		this.itemQty = null != itemQty? itemQty.longValue() : 0l;
	}



	public Long getPromoID() {
		return promoID;
	}
	public void setPromoID(Long promoID) {
		this.promoID = promoID;
	}
	public String getStoreCode() {
		return storeCode;
	}
	public void setStoreCode(String storeCode) {
		this.storeCode = storeCode;
	}
	public String getStore() {
		return store;
	}
	public void setStore(String store) {
		this.store = store;
	}
	public String getPromo() {
		return promo;
	}
	public void setPromo(String promo) {
		this.promo = promo;
	}
	public LocalDate getPromoStart() {
		return promoStart;
	}
	public void setPromoStart(LocalDate promoStart) {
		this.promoStart = promoStart;
	}
	public LocalDate getPromoEnd() {
		return promoEnd;
	}
	public void setPromoEnd(LocalDate promoEnd) {
		this.promoEnd = promoEnd;
	}
	public String getSku() {
		return sku;
	}
	public void setSku(String sku) {
		this.sku = sku;
	}
	public String getProductName() {
		return productName;
	}
	public void setProductName(String productName) {
		this.productName = productName;
	}
	public Long getItemQty() {
		return itemQty;
	}
	public void setItemQty(Long itemQty) {
		this.itemQty = itemQty;
	}
	public LocalDate getRedeemTo() {
		return redeemTo;
	}
	public void setRedeemTo(LocalDate redeemTo) {
		this.redeemTo = redeemTo;
	}
	public Long getRedeemedStamps() {
		return redeemedStamps;
	}
	public void setRedeemedStamps(Long redeemedStamps) {
		this.redeemedStamps = redeemedStamps;
	}
	public Long getAvailableStamps() {
		return availableStamps;
	}
	public void setAvailableStamps(Long availableStamps) {
		this.availableStamps = availableStamps;
	}
	public Long getRemainingStamps() {
		return remainingStamps;
	}
	public void setRemainingStamps(Long remainingStamps) {
		this.remainingStamps = remainingStamps;
	}
	public StampTxnType getTxnType() {
		return txnType;
	}
	public void setTxnType(StampTxnType txnType) {
		this.txnType = txnType;
	}

}
