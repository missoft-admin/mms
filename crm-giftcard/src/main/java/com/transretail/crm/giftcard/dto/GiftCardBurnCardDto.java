package com.transretail.crm.giftcard.dto;

import java.util.List;

import com.transretail.crm.giftcard.entity.support.GiftCardInventoryStatus;
import com.transretail.crm.giftcard.entity.support.SalesOrderStatus;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.joda.time.LocalDate;
import org.joda.time.LocalTime;
import org.springframework.format.annotation.DateTimeFormat;

import com.mysema.query.annotations.QueryProjection;
import com.transretail.crm.giftcard.entity.GiftCardBurnCard;
import com.transretail.crm.giftcard.entity.support.GiftCardBurnCardStatus;
import org.joda.time.DateTime;

/**
 * @author ftopico
 */
public class GiftCardBurnCardDto {

    private DateTime lastUpdated;
    private String burnNo;
    private GiftCardInventoryDto inventory;
    private Integer storeId;
    private String store;
    @DateTimeFormat(pattern = "dd-MM-yyyy")
    private LocalDate dateFiled = new LocalDate();
    private String dateFiledString;
    private String filedBy;
    private String burnReason;
    private String burnReasonDescription;
    private Integer quantity;
    private GiftCardBurnCardStatus status;
    private String approvedBy;

    //for burn card creation request
    private Long seriesFrom;
    private Long seriesTo;
    private List<String> cards;

    // untuk menampilkan time (14:20:00)
    @DateTimeFormat(pattern = "hh:mm:ss aa")
    private LocalTime timeFiled;

    // untuk menampilkan status dari giftcard
    private SalesOrderStatus statusCard;

    // field untuk menampung no so
    private String noSo;

    // untuk menampilkan no so
    private String qtyCard;

    // untuk menampilkan apakah full atau partial
    private List<String> typeBurn;

    public GiftCardBurnCardDto() {

    }

    @QueryProjection
    public GiftCardBurnCardDto(
            DateTime lastUpdated,
            String burnNo,
            Integer storeId,
            LocalDate dateFiled,
            String filedBy,
            String burnReason,
            GiftCardBurnCardStatus status) {
        this.lastUpdated = lastUpdated;
        this.burnNo = burnNo;
        this.storeId = storeId;
        this.dateFiled = dateFiled;
        this.filedBy = filedBy;
        this.burnReason = burnReason;
        this.status = status;
    }

    public GiftCardBurnCardDto(GiftCardBurnCard giftCardBurnCard) {
        this.burnNo = giftCardBurnCard.getBurnNo();
        this.inventory = new GiftCardInventoryDto(giftCardBurnCard.getInventory());
        this.storeId = giftCardBurnCard.getStoreId();
        this.filedBy = giftCardBurnCard.getFiledBy();
        this.timeFiled = giftCardBurnCard.getTimeFiled();
        this.burnReason = giftCardBurnCard.getBurnReason();
        this.status = giftCardBurnCard.getStatus();

        if (giftCardBurnCard.getApprovedBy() != null) {
            this.approvedBy = giftCardBurnCard.getApprovedBy();
        }
    }

    public String getBurnNo() {
        return burnNo;
    }

    public void setBurnNo(String burnNo) {
        this.burnNo = burnNo;
    }

    public GiftCardInventoryDto getInventory() {
        return inventory;
    }

    public void setInventory(GiftCardInventoryDto inventory) {
        this.inventory = inventory;
    }

    public Integer getStoreId() {
        return storeId;
    }

    public void setStoreId(Integer storeId) {
        this.storeId = storeId;
    }

    public String getFiledBy() {
        return filedBy;
    }

    public void setFiledBy(String filedBy) {
        this.filedBy = filedBy;
    }

    public String getBurnReason() {
        return burnReason;
    }

    public void setBurnReason(String burnReason) {
        this.burnReason = burnReason;
    }

    public LocalDate getDateFiled() {
        return dateFiled;
    }

    public void setDateFiled(LocalDate dateFiled) {
        this.dateFiled = dateFiled;
    }

    public Integer getQuantity() {
        return quantity;
    }

    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }

    public String getStatus() {
        return status.toString();
    }

    public void setStatus(GiftCardBurnCardStatus status) {
        this.status = status;
    }

    public List<String> getCards() {
        return cards;
    }

    public void setCards(List<String> cards) {
        this.cards = cards;
    }

    public Long getSeriesFrom() {
        return seriesFrom;
    }

    public void setSeriesFrom(Long seriesFrom) {
        this.seriesFrom = seriesFrom;
    }

    public Long getSeriesTo() {
        return seriesTo;
    }

    public void setSeriesTo(Long seriesTo) {
        this.seriesTo = seriesTo;
    }

    public String getDateFiledString() {
        return dateFiled != null ? dateFiled.toString("dd-MM-yyyy")  : "";
    }

    public void setDateFiledString(String dateFiledString) {
        this.dateFiledString = dateFiledString;
    }

    public String getStore() {
        return store;
    }

    public void setStore(String store) {
        this.store = store;
    }

    public String getApprovedBy() {
        return approvedBy;
    }

    public void setApprovedBy(String approvedBy) {
        this.approvedBy = approvedBy;
    }

    public String getBurnReasonDescription() {
        return burnReasonDescription;
    }

    public void setBurnReasonDescription(String burnReasonDescription) {
        this.burnReasonDescription = burnReasonDescription;
    }

    public DateTime getLastUpdated() {
        return lastUpdated;
    }

    public void setLastUpdated(DateTime lastUpdated) {
        this.lastUpdated = lastUpdated;
    }

    public LocalTime getTimeFiled() {
        return timeFiled;
    }

    public void setTimeFiled(LocalTime timeFiled) {
        this.timeFiled = timeFiled;
    }

    public SalesOrderStatus getStatusCard() {
        return statusCard;
    }

    public void setStatusCard(SalesOrderStatus statusCard) {
        this.statusCard = statusCard;
    }

    public String getNoSo() {
        return noSo;
    }

    public void setNoSo(String noSo) {
        this.noSo = noSo;
    }

    public String getQtyCard() {
        return qtyCard;
    }

    public void setQtyCard(String qtyCard) {
        this.qtyCard = qtyCard;
    }

    public List<String> getTypeBurn() {
        return typeBurn;
    }

    public void setTypeBurn(List<String> typeBurn) {
        this.typeBurn = typeBurn;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;

        if (o == null || getClass() != o.getClass()) return false;

        GiftCardBurnCardDto that = (GiftCardBurnCardDto) o;

        return new EqualsBuilder()
                .append(burnNo, that.burnNo)
                .isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 37)
                .append(burnNo)
                .toHashCode();
    }
}
