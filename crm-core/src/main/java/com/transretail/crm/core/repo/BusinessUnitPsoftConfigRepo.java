package com.transretail.crm.core.repo;

import com.transretail.crm.common.repo.CrmQueryDslPredicateExecutor;
import com.transretail.crm.core.entity.BusinessUnitPsoftConfig;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author Monte Cillo Co (mco@exist.com)
 */
@Repository
public interface BusinessUnitPsoftConfigRepo extends CrmQueryDslPredicateExecutor<BusinessUnitPsoftConfig, Long> {

    @Transactional(readOnly = true)
    BusinessUnitPsoftConfig findByBusinessUnitCode(String businessUnitCode);

}
