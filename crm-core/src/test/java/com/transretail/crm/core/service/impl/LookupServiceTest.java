package com.transretail.crm.core.service.impl;

import java.util.List;

import junit.framework.Assert;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Configurable;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import com.transretail.crm.core.dto.LookupDetailResultList;
import com.transretail.crm.core.dto.LookupDetailSearchDto;
import com.transretail.crm.core.dto.LookupHeaderResultList;
import com.transretail.crm.core.dto.LookupHeaderSearchDto;
import com.transretail.crm.core.entity.enums.Status;
import com.transretail.crm.core.entity.lookup.LookupDetail;
import com.transretail.crm.core.entity.lookup.LookupHeader;
import com.transretail.crm.core.repo.LookupDetailRepo;
import com.transretail.crm.core.repo.LookupHeaderRepo;
import com.transretail.crm.core.service.CodePropertiesService;
import com.transretail.crm.core.service.LookupService;

@Configurable
@Transactional
@ContextConfiguration(locations = "classpath*:/META-INF/spring/applicationContext*.xml")
@RunWith(SpringJUnit4ClassRunner.class)
public class LookupServiceTest {
	
	@Autowired
	LookupService lookupService;
	
	@Autowired
	LookupDetailRepo lookupDetailRepo;
	
	@Autowired
	LookupHeaderRepo lookupHeaderRepo;

    @Autowired
    private CodePropertiesService codePropertiesService;

    private LookupHeader companyLookupHeader;
	
	@Before
	public void setup() {
		LookupHeader theHeader1 = new LookupHeader();
		theHeader1.setCode("MEM001");
		theHeader1.setDescription("TITLE");
		theHeader1 = lookupHeaderRepo.saveAndFlush(theHeader1);
		
		LookupHeader theHeader2 = new LookupHeader();
		theHeader2.setCode("MEM002");
		theHeader2.setDescription("GENDER");
		theHeader2 = lookupHeaderRepo.saveAndFlush(theHeader2);

        companyLookupHeader = lookupHeaderRepo.saveAndFlush(new LookupHeader(codePropertiesService.getHeaderCompany()));

		LookupDetail theDetail = new LookupDetail();
		theDetail.setCode("TITL001");
		theDetail.setDescription("MR");
		theDetail.setStatus(Status.ACTIVE);
		theDetail.setHeader(theHeader1);
		lookupDetailRepo.saveAndFlush(theDetail);
		
		theDetail = new LookupDetail();
		theDetail.setCode("TITL002");
		theDetail.setDescription("MRS");
		theDetail.setStatus(Status.ACTIVE);
		theDetail.setHeader(theHeader1);
		lookupDetailRepo.saveAndFlush(theDetail);
		
		theDetail = new LookupDetail();
		theDetail.setCode("TITL003");
		theDetail.setDescription("MISS");
		theDetail.setStatus(Status.ACTIVE);
		theDetail.setHeader(theHeader1);
		lookupDetailRepo.saveAndFlush(theDetail);
		
		theDetail = new LookupDetail();
		theDetail.setCode("TITL004");
		theDetail.setDescription("MADAM");
		theDetail.setStatus(Status.ACTIVE);
		theDetail.setHeader(theHeader1);
		lookupDetailRepo.saveAndFlush(theDetail);
		
		theDetail = new LookupDetail();
		theDetail.setCode("GEND001");
		theDetail.setDescription("MALE");
		theDetail.setStatus(Status.ACTIVE);
		theDetail.setHeader(theHeader2);
		lookupDetailRepo.saveAndFlush(theDetail);
		
		theDetail = new LookupDetail();
		theDetail.setCode("GEND002");
		theDetail.setDescription("FEMALE");
		theDetail.setStatus(Status.ACTIVE);
		theDetail.setHeader(theHeader2);
		lookupDetailRepo.saveAndFlush(theDetail);
		
		theDetail = new LookupDetail();
		theDetail.setCode("GEND003");
		theDetail.setDescription("NOT SELECTED");
		theDetail.setStatus(Status.ACTIVE);
		theDetail.setHeader(theHeader2);
		lookupDetailRepo.saveAndFlush(theDetail);

	}

    @After
    public void tearDown() {
        lookupDetailRepo.deleteAll();
        lookupHeaderRepo.deleteAll();
    }
	
	@Test
	public void testGetLookupHeaders() {
		LookupHeaderSearchDto theSearchDto = new LookupHeaderSearchDto();
		theSearchDto.setCode("MEM001");
		
		LookupHeaderResultList theHeaders = lookupService.getLookupHeaders(theSearchDto);
		Assert.assertNotNull(theHeaders);
		Assert.assertEquals(1, theHeaders.getTotalElements());
	}
	
	@Test
	public void testGetLookupDetails() {
		LookupDetailSearchDto theSearchDto = new LookupDetailSearchDto();
		theSearchDto.setCode("TITL003");
		
		LookupDetailResultList theDetails = lookupService.getLookupDetails(theSearchDto);
		Assert.assertNotNull(theDetails);
		Assert.assertEquals(1, theDetails.getTotalElements());
	}
	
	@Test
	public void testGetAllHeaders() {
		Assert.assertNotNull(lookupService.getAllHeaders());
		Assert.assertEquals(3, lookupService.getAllHeaders().size());
	}
	
	@Test
	public void testGetDetails() {
		List<LookupDetail> theDetails = lookupService.getDetails(new LookupHeader("MEM001"));
		Assert.assertNotNull(theDetails);
		Assert.assertEquals(4, theDetails.size());
		
		theDetails = lookupService.getDetails("GENDER");
		Assert.assertNotNull(theDetails);
		Assert.assertEquals(3, theDetails.size());
	}
	
	@Test
	public void testSaveDetail() {
		Assert.assertEquals(3, lookupService.getDetails(new LookupHeader("MEM002")).size());
		
		LookupDetail theDetail = new LookupDetail();
		theDetail.setCode("GEND004");
		theDetail.setDescription("TEMP GENDER");
		theDetail.setStatus(Status.ACTIVE);
		theDetail.setHeader(new LookupHeader("MEM002"));
		lookupService.saveDetail(theDetail);
		
		Assert.assertEquals(4, lookupService.getDetails(new LookupHeader("MEM002")).size());
	}
	
	@Test
	public void testIsDuplicateDetail() {
		Assert.assertTrue(lookupService.isDuplicateDetail("GEND003"));
		Assert.assertFalse(lookupService.isDuplicateDetail("GEND004"));
	}
	
	@Test
	public void testGetDetailsStr() {
		LookupDetail theDetail = lookupService.getDetail("TITL002");
		Assert.assertNotNull(theDetail);
		Assert.assertEquals("MRS", theDetail.getDescription());
	}
	
	@Test
	public void testGetMemberHeaders() {
		List<LookupHeader> theHeaders = lookupService.getMemberHeaders();
		Assert.assertNotNull(theHeaders);
		Assert.assertEquals(2, theHeaders.size());
	}
	
    @Test
    public void isValidCompanyDetailCodeTest() {
        lookupDetailRepo.saveAndFlush(new LookupDetail("COKE01", "COCA-COLA", companyLookupHeader, Status.ACTIVE));
        Assert.assertFalse(lookupService.isValidCompanyDetailCode("PEPS01"));
        Assert.assertFalse(lookupService.isValidCompanyDetailCode("01"));
        Assert.assertFalse(lookupService.isValidCompanyDetailCode("001"));
        Assert.assertFalse(lookupService.isValidCompanyDetailCode("201"));
        Assert.assertFalse(lookupService.isValidCompanyDetailCode("1201"));
        Assert.assertTrue(lookupService.isValidCompanyDetailCode("PEPS11"));
        Assert.assertTrue(lookupService.isValidCompanyDetailCode("11"));
        Assert.assertTrue(lookupService.isValidCompanyDetailCode("011"));
        Assert.assertTrue(lookupService.isValidCompanyDetailCode("211"));
        Assert.assertTrue(lookupService.isValidCompanyDetailCode("1211"));
    }

}
