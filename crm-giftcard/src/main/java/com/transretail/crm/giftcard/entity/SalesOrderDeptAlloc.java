package com.transretail.crm.giftcard.entity;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.transretail.crm.core.entity.lookup.LookupDetail;
import com.transretail.crm.core.entity.support.CustomAuditableEntity;
import org.apache.commons.lang3.builder.EqualsBuilder;

/**
 *
 */
@Entity
@Table(name = "CRM_SALES_DEPT_ALLOC")
public class SalesOrderDeptAlloc extends CustomAuditableEntity<Long> implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@ManyToOne
    @JoinColumn(name = "SALES_ORDER")
	private SalesOrder order;
	
	@ManyToOne
    @JoinColumn(name = "DEPT")
	private LookupDetail department;
	
	@Column(name = "ALLOC_AMT")
	private BigDecimal allocAmount;
	
	@Column(name = "STATUS")
	@Enumerated(EnumType.STRING)
	private DeptAllocStatus status = DeptAllocStatus.NEW;

	public SalesOrder getOrder() {
		return order;
	}

	public void setOrder(SalesOrder order) {
		this.order = order;
	}

	public LookupDetail getDepartment() {
		return department;
	}

	public void setDepartment(LookupDetail department) {
		this.department = department;
	}

	public BigDecimal getAllocAmount() {
		return allocAmount;
	}

	public void setAllocAmount(BigDecimal allocAmount) {
		this.allocAmount = allocAmount;
	}
	
	public DeptAllocStatus getStatus() {
		return status;
	}

	public void setStatus(DeptAllocStatus status) {
		this.status = status;
	}


	public enum DeptAllocStatus {
		NEW, APPROVED, REJECTED;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;

		if (o == null || getClass() != o.getClass()) return false;

		SalesOrderItem that = (SalesOrderItem) o;

		return new EqualsBuilder()
				.appendSuper(super.equals(o))
				.isEquals();
	}

	@Override
	public int hashCode() {

		int hashcode = 17;

		hashcode += (null == getId()) ? 0 : getId().hashCode() * 31;

		return hashcode;
	}
	
}
