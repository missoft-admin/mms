<%@ include file="../../common/taglibs.jsp" %>


<style type="text/css">
  .field-group select {
    max-height: 150px;
    width: 95%;
  }

  .field-group button {
    width: 100%;
    margin-bottom: 5px;
    padding-left: 0;
    padding-right: 0;
    text-align: center;
  }

</style>
<div class="form-horizontal container-fluid">
  <div class="memberConfigurableFieldsContainer">
    <div class="row-fluid">
      <div class="pull-left span3">
        <div class="row-fluid field-group">
          <label>${configurableFieldHeaders[0].description}</label>

          <div class="span9">
            <select multiple="" class="${configurableFieldHeaders[0].code} customField01">

            </select>
            <input type="hidden" name="configurableFields.customField01" class="${configurableFieldHeaders[0].code} customField01" value="${configurableFields.customField01}">
          </div>

          <div class="span3">
            <button type="button" class="btn btn-primary btn-small addCustomField"><spring:message code="label_add"/></button>
            <input type="hidden" class="headerCode" value="${configurableFieldHeaders[0].code}">
            <input type="hidden" class="headerDesc" value="${configurableFieldHeaders[0].description}">
            <button type="button" class="btn btn-small removeCustomField"><spring:message code="label_remove"/></button>
          </div>
        </div>
      </div>

      <c:forEach items="${configurableFieldHeaders}" var="field" varStatus="status" begin="1">
      <c:if test="${status.index % 4 == 0}">
    </div>
    <div class="row-fluid">
      </c:if>
      <div class="pull-left span3">
        <div class="row-fluid field-group">
          <label>${configurableFieldHeaders[status.index].description}</label>

          <div class="span9">
            <c:set var="customFieldName">
              customField<fmt:formatNumber pattern="00" value="${status.index + 1}"/>
            </c:set>
            <select multiple="" class="${configurableFieldHeaders[status.index].code} ${customFieldName}">

            </select>
            <input type="hidden" name="configurableFields.${customFieldName}" class="${configurableFieldHeaders[status.index].code} ${customFieldName}">
          </div>

          <div class="span3">
            <button type="button" class="btn btn-small btn-primary addCustomField"><spring:message code="label_add"/></button>
            <input type="hidden" class="headerCode" value="${configurableFieldHeaders[status.index].code}">
            <input type="hidden" class="headerDesc" value="${configurableFieldHeaders[status.index].description}">
            <button type="button" class="btn btn-small removeCustomField"><spring:message code="label_remove"/></button>
          </div>
        </div>
      </div>
      </c:forEach>
    </div>
  </div>
  <div class="clearfix"></div>
</div>