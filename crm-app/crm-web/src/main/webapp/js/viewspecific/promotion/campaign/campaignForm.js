$(document).ready( function() {

	var theErrorContainer = $( "#content_campaignForm" ).find("#content_error");

	var $theCampaignDialog = $( "#content_campaignForm" );
	var $theCampaignForm = $( "#campaignForm" );
	var $theCampaignDuration = $( "#campaignDuration" );
	var $theCampaignStartDate = $( "#campaignStartDate" );
	var $theCampaignEndDate = $( "#campaignEndDate" );

	var $thePromotionProgram = $( "#program" );
	var $thePromotionCampaign = $( "#campaign" );

	var $theCampaignPrograms = $( "#content_programs" );



	initFormFields();
	initDialog();
	


	function initFormFields() {

		$theCampaignForm.submit( function(e) {
			e.preventDefault();
			$( "#content_campaignForm" ).find( "#saveButton" ).prop( "disabled", true );
			if ( $( "#campaignDurationStart" ).val() != "" && $( "#campaignDurationStart" ).val() != null ) {
				$theCampaignStartDate.val( $( "#campaignDurationStart" ).datepicker( "getDate").getTime() );
			}
			if ( $( "#campaignDurationEnd" ).val() != "" && $( "#campaignDurationEnd" ).val() != null ) {
				$theCampaignEndDate.val( $( "#campaignDurationEnd" ).datepicker( "getDate").getTime() );
			}
			saveCampaignPost( $(this).attr("action") );
		});

		$theCampaignDuration.datepicker({
			autoclose	: true,
			format		: "dd M yyyy"
		});

		$theCampaignPrograms.val( $thePromotionProgram.val() );
		$theCampaignPrograms.change( function(e) {
			e.preventDefault();
			setCampaignDateDuration( $(this).find(':selected').data("min-date"), $(this).find(':selected').data("max-date") );
		}).change();

		setDateDifference();
	}

	function initDialog() {

		$theCampaignDialog.modal({});
		$theCampaignDialog.on( "hide", function() { theErrorContainer.hide(); });
	}



	function saveCampaignPost( inUrl ) {
		$.ajax({
			cache 		: false, 
			dataType 	: "json",
			type 		: "POST", 
			url 		: inUrl,
			data		: $theCampaignForm.serialize(),
			error 		: function(jqXHR, textStatus, errorThrown) { $( "#content_campaignForm" ).find( "#saveButton" ).prop( "disabled", false ); },
			success 	: function( inResponse ) {		
				if ( inResponse.success ) {
					$theCampaignDialog.modal( "hide" );
					$theCampaignDialog.remove();
					updateProgramCampaignList( inResponse.result );
				}
				else {
					var theErrorInfo = "";
					$.each( inResponse.result, function( key, value ) {
						theErrorInfo += ( ( key + 1 ) + ". " + value.code + "<br>" );
					});
					theErrorContainer.find( "div" ).html( theErrorInfo );
					theErrorContainer.show( "slow" );
				}
				$( "#content_campaignForm" ).find( "#saveButton" ).prop( "disabled", false );
			}
		});
	}

	function updateProgramCampaignList( inData ) {
		$thePromotionProgram.val( inData.program );
		/*$thePromotionProgram.change();
		$thePromotionCampaign.val( inData.id );
		$thePromotionCampaign.change();*/
		$thePromotionCampaign.append(
				"<option value='" + inData.id + "' " +
					"data-min-date='" + inData.startDate + "' " +
					"data-max-date='" + inData.endDate + "' >" + inData.name + "</option>");
		$thePromotionCampaign.val( inData.id );
		$thePromotionCampaign.data( "setDate", true );
		$thePromotionCampaign.change();
	}

	function setCampaignDateDuration( inMinDate, inMaxDate ) {
		var $theCampaignEnd = $( "#campaignDurationEnd" );
		$theCampaignEnd.datepicker( "setDate", inMaxDate? new Date( inMaxDate ) : "" );
		$theCampaignEnd.datepicker( "setStartDate", inMinDate? new Date( inMinDate ) : "" );
		$theCampaignEnd.datepicker( "setEndDate", inMaxDate? new Date( inMaxDate ) : "" );
		$theCampaignEnd.on( "changeDate", function(e) {
			$( this ).datepicker( "setDate", new Date( $( this ).datepicker( "getDate" ).getTime() + 24*60*60*999.999 ) );
		});

		var $theCampaignStart = $( "#campaignDurationStart" );
		$theCampaignStart.datepicker( "setStartDate", inMinDate? new Date( inMinDate ) : "" );
		$theCampaignStart.datepicker( "setEndDate", inMaxDate? new Date( inMaxDate ) : "" );
		$theCampaignStart.datepicker( "setDate", inMinDate? new Date( inMinDate ) : "" );
		$theCampaignStart.on( "changeDate", function(e) {
			$theCampaignEnd.datepicker( "setStartDate", $( this ).datepicker( "getDate") );
		});
	}

	function setDateDifference() {
		var $theCampaignEnd = $( "#campaignDurationEnd" ), $theCampaignStart = $( "#campaignDurationStart" );
		var conversionFactor = 24*60*60*1000;
		$theCampaignStart.change( function(e) {
			if ( $theCampaignStart.val() && $theCampaignEnd.val()
					&& $theCampaignStart.datepicker( "getDate").getTime() && $theCampaignEnd.datepicker( "getDate").getTime() 
					&& $theCampaignEnd.datepicker( "getDate").getTime() > $theCampaignStart.datepicker( "getDate").getTime() ) {
				$( "#noOfCampaignDays" ).html( 
					Math.round( Math.abs( 
						( $theCampaignEnd.datepicker( "getDate").getTime() - $theCampaignStart.datepicker( "getDate").getTime() ) )
							/( conversionFactor ) ) + $( "#noOfCampaignDays" ).data( "label" ) );
			}
			else {
				$( "#noOfCampaignDays" ).html( "" );
			}
		});
		$theCampaignEnd.change( function(e) {
			$theCampaignStart.change();
		}).trigger("changeDate");
	}

});