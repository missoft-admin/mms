package com.transretail.crm.report.template.impl;

import java.util.List;
import java.util.Map;
import java.util.Set;

import org.joda.time.LocalDate;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.MessageSourceAware;
import org.springframework.context.support.MessageSourceAccessor;
import org.springframework.stereotype.Service;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.transretail.crm.common.reporting.jasper.ClasspathFileResolver;
import com.transretail.crm.common.reporting.jasper.DefaultJRProcessor;
import com.transretail.crm.common.reporting.jasper.JRProcessor;
import com.transretail.crm.core.entity.enums.ReportType;
import com.transretail.crm.core.security.util.UserUtil;
import com.transretail.crm.loyalty.dto.AllocationDto;
import com.transretail.crm.loyalty.dto.MultipleAllocationDto;
import com.transretail.crm.loyalty.service.LoyaltyCardService;
import com.transretail.crm.report.template.FilterField;

@Service("loyaltyTransferDocument")
public class LoyaltyTransferDocument implements MessageSourceAware {
    protected static final String TEMPLATE_NAME = "Loyalty Inventory Transfer Document";
    protected static final String REPORT_NAME = "reports/loyalty_transfer_document.jasper";
    private static final String PARAM_LOCATION = "location";
    private static final String PARAM_TRANSFER_TO = "transferTo";
    private static final String PARAM_CARD_TYPE = "cardType";
    private static final String PARAM_IS_EMPTY = "isEmpty";
    private static final DateTimeFormatter FULL_DATE_PATTERN = DateTimeFormat.forPattern("MMMM d, yyyy");
    private MessageSourceAccessor messageSource;
    @Autowired
    LoyaltyCardService loyaltyCardService;
    
    public ReportType[] getReportTypes() {
    	return ReportType.values();
    }

    @Override
    public void setMessageSource(MessageSource messageSource) {
        this.messageSource = new MessageSourceAccessor(messageSource);
    }

    public String getName() {
        return TEMPLATE_NAME;
    }

    public String getDescription() {
        return messageSource.getMessage("loyalty_transfer_document_title");
    }

    public Set<FilterField> getFilters() {
        return null;
    }

    public JRProcessor createJrProcessor(MultipleAllocationDto transferDto) {
    	Map<String, Object> parameters = Maps.newHashMap();
    	parameters.put(PARAM_LOCATION, transferDto.getLocation() + " - " + loyaltyCardService.getInventoryDesc(transferDto.getLocation()));
    	parameters.put(PARAM_TRANSFER_TO, transferDto.getTransferTo() + " - " + loyaltyCardService.getInventoryDesc(transferDto.getTransferTo()));
    	parameters.put(PARAM_CARD_TYPE, transferDto.getCardType().getCode() + " - " + transferDto.getCardType().getDescription());
    	parameters.put("printDate", FULL_DATE_PATTERN.print(new LocalDate()));
    	parameters.put("endorsedBy", UserUtil.getCurrentUser().getFullName());
    	List<ReportBean> beansDataSource = Lists.newArrayList();
    	if (transferDto.getAllocations().isEmpty()) {
            parameters.put(PARAM_IS_EMPTY, true);
            beansDataSource.add(new ReportBean());
        } else {
        	for(AllocationDto seriesDto : transferDto.getAllocations()) {
        		beansDataSource.add(new ReportBean(seriesDto.getStartingSeries(), seriesDto.getEndingSeries(), seriesDto.getEncodedBy(), seriesDto.getAllocateDate()));	
        	}
        }
        DefaultJRProcessor jrProcessor = new DefaultJRProcessor(REPORT_NAME, beansDataSource);
        jrProcessor.addParameters(parameters);
        jrProcessor.setFileResolver(new ClasspathFileResolver("reports"));
        return jrProcessor;
    }
    
    public static class ReportBean {
    	private String startingSeries;
    	private String endingSeries;
    	private String encodedBy;
    	private String allocateDate;
    	
    	public ReportBean() {}
    	
		public ReportBean(String startingSeries, String endingSeries,
				String encodedBy, LocalDate allocateDate) {
			super();
			this.startingSeries = startingSeries;
			this.endingSeries = endingSeries;
			this.encodedBy = encodedBy;
			this.allocateDate = FULL_DATE_PATTERN.print(allocateDate);
		}

		public String getStartingSeries() {
			return startingSeries;
		}
		public void setStartingSeries(String startingSeries) {
			this.startingSeries = startingSeries;
		}
		public String getEndingSeries() {
			return endingSeries;
		}
		public void setEndingSeries(String endingSeries) {
			this.endingSeries = endingSeries;
		}
		public String getEncodedBy() {
			return encodedBy;
		}
		public void setEncodedBy(String encodedBy) {
			this.encodedBy = encodedBy;
		}
		public String getAllocateDate() {
			return allocateDate;
		}
		public void setAllocateDate(String allocateDate) {
			this.allocateDate = allocateDate;
		}
		
    	
    }

}
