package com.transretail.crm.loyalty.dto;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.joda.time.DateTime;
import org.joda.time.LocalDate;
import org.springframework.beans.BeanUtils;
import org.springframework.format.annotation.DateTimeFormat;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.google.common.collect.Maps;
import com.transretail.crm.common.web.converter.LocalDateSerializer;
import com.transretail.crm.core.entity.lookup.LookupDetail;
import com.transretail.crm.giftcard.entity.CardVendor;
import com.transretail.crm.loyalty.entity.ManufactureOrder;
import com.transretail.crm.loyalty.entity.ManufactureOrderTier;

public class ManufactureOrderDto {
	
	public static final String[] IGNORE = {"tiers", "supplier"};
	
	private Long id;
	
	private String moNo;
	
	private String poNo;
	
	private Long supplier;
	
	private String supplierName;
	
	@DateTimeFormat(pattern = "dd-MM-yyyy")
	@JsonSerialize(using = LocalDateSerializer.class)
	private LocalDate orderDate;
	
	@DateTimeFormat(pattern = "dd-MM-yyyy")
	@JsonSerialize(using = LocalDateSerializer.class)
	private LocalDate moDate;
	
	private LookupDetail status;
	
	private List<ManufactureOrderTierDto> tiers;
	
	private String reason;
	
	private boolean showReceive = true;
	
	private String createdBy;

    private DateTime lastUpdated;
	
	public ManufactureOrderDto() {}
	
	public ManufactureOrderDto(ManufactureOrder manufactureOrder) {
		BeanUtils.copyProperties(manufactureOrder, this, IGNORE);
		
		this.id = manufactureOrder.getId();
		this.createdBy = manufactureOrder.getCreateUser();
		this.lastUpdated = manufactureOrder.getLastUpdated();
		
		if(manufactureOrder.getSupplier() != null) {
			supplier = manufactureOrder.getSupplier().getId();
			supplierName  = manufactureOrder.getSupplier().getFormalName();
		}
		
		if(CollectionUtils.isNotEmpty(manufactureOrder.getTiers())) {
			tiers = new ArrayList<ManufactureOrderTierDto>();
			for(ManufactureOrderTier orderTier : manufactureOrder.getTiers()) {
				ManufactureOrderTierDto orderTierDto = new ManufactureOrderTierDto();
				BeanUtils.copyProperties(orderTier, orderTierDto);
				if(orderTier.getCompany() != null) {
					orderTierDto.setCompanyCode(orderTier.getCompany().getCode());
					orderTierDto.setCompanyDesc(orderTier.getCompany().getDescription());
				}
				tiers.add(orderTierDto);
			}
		}

    }
	
	public ManufactureOrder toModel() {
		ManufactureOrder manufactureOrder = new ManufactureOrder();
		
		BeanUtils.copyProperties(this, manufactureOrder, IGNORE);
		
		if(supplier != null) {
			manufactureOrder.setSupplier(new CardVendor(supplier));
		}
		
		if(CollectionUtils.isNotEmpty(tiers)) {
			List<ManufactureOrderTier> orderTiers = new ArrayList<ManufactureOrderTier>();
			for(ManufactureOrderTierDto orderTierDto : tiers) {
				if(orderTierDto != null) {
					ManufactureOrderTier orderTier = new ManufactureOrderTier();
					BeanUtils.copyProperties(orderTierDto, orderTier);
					if(StringUtils.isNotBlank(orderTierDto.getCompanyCode())) {
						orderTier.setCompany(new LookupDetail(orderTierDto.getCompanyCode()));
					}
					orderTier.setManufactureOrder(manufactureOrder);
					orderTiers.add(orderTier);
				}
			}
			manufactureOrder.setTiers(orderTiers);
		}

        return manufactureOrder;
    }
	
	public ManufactureOrder toModel(ManufactureOrder manufactureOrder) {
//		BeanUtils.copyProperties(this, manufactureOrder, IGNORE);
        manufactureOrder.setMoNo(moNo);
        manufactureOrder.setPoNo(poNo);
        manufactureOrder.setOrderDate(orderDate);
        manufactureOrder.setMoDate(moDate);
        manufactureOrder.setStatus(status);
        manufactureOrder.setReason(reason);
		
		if(supplier != null) {
			manufactureOrder.setSupplier(new CardVendor(supplier));
		}

		if(CollectionUtils.isNotEmpty(manufactureOrder.getTiers())) {
			manufactureOrder.getTiers().clear();
		} 
	
		if(CollectionUtils.isNotEmpty(tiers)) {
			for(ManufactureOrderTierDto dto : tiers) {
                if (dto != null) {
                    ManufactureOrderTier tier = new ManufactureOrderTier();
                    tier.setManufactureOrder(manufactureOrder);
                    tier.setBatchNo(dto.getBatchNo());
                    tier.setName(dto.getName());
                    tier.setCompany(new LookupDetail(dto.getCompanyCode()));
                    tier.setStartingSeries(dto.getStartingSeries());
                    tier.setQuantity(dto.getQuantity());
                    manufactureOrder.getTiers().add(tier);
                }
            }
		}
		return manufactureOrder;
	}
	

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getMoNo() {
		return moNo;
	}

	public void setMoNo(String moNo) {
		this.moNo = moNo;
	}

	public String getPoNo() {
		return poNo;
	}

	public void setPoNo(String poNo) {
		this.poNo = poNo;
	}
	
	public Long getSupplier() {
		return supplier;
	}

	public void setSupplier(Long supplier) {
		this.supplier = supplier;
	}
	
	public String getSupplierName() {
		return supplierName;
	}

	public void setSupplierName(String supplierName) {
		this.supplierName = supplierName;
	}

	public LocalDate getOrderDate() {
		return orderDate;
	}

	public void setOrderDate(LocalDate orderDate) {
		this.orderDate = orderDate;
	}

	public LookupDetail getStatus() {
		return status;
	}

	public void setStatus(LookupDetail status) {
		this.status = status;
	}

	public List<ManufactureOrderTierDto> getTiers() {
		return tiers;
	}

	public void setTiers(List<ManufactureOrderTierDto> tiers) {
		this.tiers = tiers;
	}

	public String getReason() {
		return reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	public LocalDate getMoDate() {
		return moDate;
	}

	public void setMoDate(LocalDate moDate) {
		this.moDate = moDate;
	}

	public boolean isShowReceive() {
		return showReceive;
	}

	public void setShowReceive(boolean showReceive) {
		this.showReceive = showReceive;
	}

    public Map<String, Integer> getCompanyCardTypeStartSeriesMap() {
        Map<String, Integer> map = Maps.newHashMap();
        if (tiers != null) {
            for (ManufactureOrderTierDto tier : tiers) {
                String startingSeries = tier.getStartingSeries();
                if (StringUtils.isNotBlank(startingSeries)) {
                    String key = startingSeries.substring(0, 4);
                    Integer s1 = map.get(key);
                    Integer s2 = Integer.parseInt(startingSeries.substring(4));
                    if (s1 == null || s1 > s2) {
                        map.put(key, s2);
                    }
                }
            }
        }
        return map;
    }

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public DateTime getLastUpdated() {
		return lastUpdated;
	}

	public void setLastUpdated(DateTime lastUpdated) {
		this.lastUpdated = lastUpdated;
	}

}
