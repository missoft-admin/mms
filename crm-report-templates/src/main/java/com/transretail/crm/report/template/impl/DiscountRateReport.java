package com.transretail.crm.report.template.impl;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.google.common.collect.Sets;
import com.mysema.query.group.GroupBy;
import com.mysema.query.jpa.impl.JPAQuery;
import com.mysema.query.types.ConstructorExpression;
import com.mysema.query.types.expr.BooleanExpression;
import com.mysema.query.types.expr.CaseBuilder;
import com.mysema.query.types.expr.StringExpression;
import com.mysema.query.types.path.EnumPath;
import com.transretail.crm.common.reporting.jasper.ClasspathFileResolver;
import com.transretail.crm.common.reporting.jasper.DefaultJRProcessor;
import com.transretail.crm.common.reporting.jasper.JRProcessor;
import com.transretail.crm.common.util.DateUtil;
import com.transretail.crm.core.entity.enums.ReportType;
import com.transretail.crm.core.entity.lookup.QStore;
import com.transretail.crm.core.entity.lookup.Store;
import com.transretail.crm.core.service.StoreService;
import com.transretail.crm.giftcard.dto.ProductProfileDto;
import com.transretail.crm.giftcard.entity.QProductProfile;
import com.transretail.crm.giftcard.entity.QSalesOrderItem;

import static com.transretail.crm.giftcard.entity.support.GiftCardSalesType.*;

import com.transretail.crm.giftcard.entity.support.GiftCardSalesType;
import com.transretail.crm.giftcard.entity.support.SalesOrderStatus;

import static com.transretail.crm.giftcard.entity.support.SalesOrderType.*;

import com.transretail.crm.giftcard.entity.support.SalesOrderType;
import com.transretail.crm.giftcard.service.ProductProfileService;
import com.transretail.crm.report.template.*;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.*;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import net.sf.jasperreports.engine.JRParameter;
import org.apache.commons.lang3.StringUtils;
import org.joda.time.LocalDate;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.context.support.MessageSourceResourceBundle;
import org.springframework.stereotype.Service;

@Service("discountRateReport")
public class DiscountRateReport implements ReportTemplate, ReportsCustomizer, ReportDateFormatProvider {

    protected static final String TEMPLATE_NAME = "Discount Rate Report";
    protected static final String REPORT_NAME = "reports/discount_rate_report.jasper";
    protected static final String FILTER_DATE_FROM = CommonReportFilter.DATE_FROM;
    protected static final String FILTER_DATE_TO = CommonReportFilter.DATE_TO;
    protected static final String FILTER_PRODUCT = "product";
    protected static final String FILTER_AREA = "area";
    protected static final String FILTER_SALES_TYPE = "salesType";
    private static final BigDecimal HUNDRED = BigDecimal.valueOf(100L);

    private static final DateTimeFormatter INPUT_DATE_PATTERN = DateTimeFormat.forPattern(DateUtil.SEARCH_DATE_FORMAT);
    private static final DateTimeFormatter FULL_DATE_PATTERN = DateTimeFormat.forPattern(DateUtil.SEARCH_DATE_FORMAT);

    private static final Logger LOGGER = LoggerFactory.getLogger(DiscountRateReport.class);

//    private MessageSourceAccessor messageSource;
    @PersistenceContext
    private EntityManager em;
    @Autowired
    private MessageSource messageSource;
    @Autowired
    private ProductProfileService productService;
    @Autowired
    private StoreService storeService;

    private LocalDate parseDate(String dateInstance) {
        return INPUT_DATE_PATTERN.parseLocalDate(dateInstance);
    }

    @Override
    public DateTimeFormatter getDefaultDateFormat() {
        return FULL_DATE_PATTERN;
    }

    @Override
    public ReportType[] getReportTypes() {
        return ReportType.values();
    }

    @Override
    public String getName() {
        return TEMPLATE_NAME;
    }

    @Override
    public String getDescription() {
        return messageSource.getMessage("gc.discount.rate.report.label", (Object[]) null, LocaleContextHolder.getLocale());
    }

    @Override
    public boolean canView(Set<String> permissions) {
        return true;
    }

    @Override
    public Set<Option> customizerOption() {
        return Sets.newHashSet(ReportsCustomizer.Option.DATE_RANGE_FULL_VALIDATION);
    }

    @Override
    public Set<FilterField> getFilters() {
        Set<FilterField> filters = Sets.newLinkedHashSet();
        filters.add(FilterField.createDateField(FILTER_DATE_FROM,
                messageSource.getMessage("label_from", (Object[]) null, LocaleContextHolder.getLocale()),
                DateUtil.SEARCH_DATE_FORMAT, true));
        filters.add(FilterField.createDateField(FILTER_DATE_TO,
                messageSource.getMessage("label_to", (Object[]) null, LocaleContextHolder.getLocale()),
                DateUtil.SEARCH_DATE_FORMAT, true));
        QProductProfile product = QProductProfile.productProfile;

        //Products
        Map<String, String> products = new LinkedHashMap<String, String>();
        List<ProductProfileDto> productProfiles = productService.findAllDtos();
        products.put("", "");
        if (!productProfiles.isEmpty()) {
            for (ProductProfileDto dto : productProfiles) {
                products.put(dto.getProductCode(), dto.getProductDesc());
            }
        }
        filters.add(FilterField.createDropdownField(FILTER_PRODUCT,
                messageSource.getMessage("gc.product.label.name", (Object[]) null,
                        LocaleContextHolder.getLocale()), products));

        //Stores
        Map<String, String> stores = new LinkedHashMap<String, String>();
        stores.put("", "");
        for (Store store : storeService.getAllStores()) {
            stores.put(store.getCode(), store.getCode() + " - " + store.getName());
        }

        filters.add(FilterField.createDropdownField(FILTER_AREA,
                messageSource.getMessage("label_group_store", (Object[]) null, LocaleContextHolder.getLocale()),
                stores));

        Map<String, String> salesTypes = Maps.newHashMap();
        salesTypes.put("", "");
        for (GiftCardSalesType type : GiftCardSalesType.values()) {
            salesTypes.put(type.name(), type.name());
        }

        filters.add(FilterField.createDropdownField(FILTER_SALES_TYPE,
                messageSource.getMessage("report.gc.ordersummary.salestype", null, "Sales Type",
                        LocaleContextHolder.getLocale()),
                salesTypes));

        return filters;
    }

    @Override
    public JRProcessor createJrProcessor(Map<String, String> map) {
        LocalDate dateFrom = parseDate(map.get(FILTER_DATE_FROM));
        LocalDate dateTo = parseDate(map.get(FILTER_DATE_TO));
        String productCode = map.get(FILTER_PRODUCT);
        String store = map.get(FILTER_AREA);
        final String insalesType = map.get(FILTER_SALES_TYPE);
        GiftCardSalesType salesType = null;
        if (StringUtils.isNotBlank(insalesType)) {
            salesType = GiftCardSalesType.valueOf(insalesType);
        }

        String area = null;

        if (StringUtils.isNotEmpty(store)) {
            area = new JPAQuery(em).from(QStore.store).where(QStore.store.code.eq(store))
                    .singleResult(QStore.store).getCodeAndName();
        }

        DefaultJRProcessor jrProcessor = new DefaultJRProcessor(REPORT_NAME);
        Map<String, Object> paramMap = prepareReportParameters(dateFrom, dateTo, productCode, area, salesType);
        paramMap.put("SUB_DATA_SOURCE", createDatasource(dateFrom, dateTo, productCode, store, salesType, paramMap));
        jrProcessor.addParameters(paramMap);
        jrProcessor.setFileResolver(new ClasspathFileResolver("reports"));
        return jrProcessor;
    }

    public List<DiscountRateBean> createDatasource(LocalDate dateFrom, LocalDate dateTo,
            String productCode, String store, GiftCardSalesType salesType, Map<String, Object> paramMap) {
        QSalesOrderItem salesOrderItem = QSalesOrderItem.salesOrderItem;

        List<DiscountRateBean> list = Lists.newArrayList();
        int numYears = dateTo.getYear() - dateFrom.getYear();

        BigDecimal totalDiscountAmount = BigDecimal.ZERO;
        BigDecimal totalSalesAmount = BigDecimal.ZERO;
        final EnumPath<SalesOrderType> orderType = salesOrderItem.order.orderType;

        for (int i = 0; i <= numYears; i++) {
            DiscountRateBean bean = new DiscountRateBean();
            bean.setYear(dateFrom.plusYears(i).toString("yyyy"));
            List<BooleanExpression> exprs = Lists.newArrayList();
            exprs.add(salesOrderItem.order.orderDate.between(dateFrom, dateTo));
            exprs.add(salesOrderItem.order.status.eq(SalesOrderStatus.SOLD));

            if (StringUtils.isNotEmpty(productCode) && !productCode.equalsIgnoreCase("all")) {
                exprs.add(salesOrderItem.product.productCode.eq(productCode));
            }
            if (StringUtils.isNotEmpty(store)) {
                exprs.add(salesOrderItem.order.paymentStore.eq(store));
            }

            if (null != salesType) {
                if (salesType == B2B) {
                    exprs.add(orderType.in(B2B_SALES, B2B_ADV_SALES, REPLACEMENT));
                } else if (salesType == REBATE) {
                    exprs.add(orderType.in(YEARLY_DISCOUNT, VOUCHER));
                } else if (salesType == INTERNAL) {
                    exprs.add(orderType.in(INTERNAL_ORDER));
                } else {
                    exprs.add(orderType.notIn(B2B_SALES, REPLACEMENT, YEARLY_DISCOUNT, VOUCHER, INTERNAL_ORDER));
                }
            }

            Map<Integer, DiscountRateMonthBean> map = new JPAQuery(em)
                    .from(salesOrderItem)
                    .where(BooleanExpression.allOf(
                                    exprs.toArray(new BooleanExpression[exprs.size()])))
                    .groupBy(salesOrderItem.order.orderDate.month())
                    .map(salesOrderItem.order.orderDate.month(),
                            ConstructorExpression.create(DiscountRateMonthBean.class,
                                    salesOrderItem.faceAmount.multiply(
                                            salesOrderItem.order.discountVal
                                            .divide(HUNDRED)).sum(),
                                    salesOrderItem.faceAmount.sum()));

            List<String> discountRates = Lists.newArrayList();
            BigDecimal discountAmountYear = BigDecimal.ZERO;
            BigDecimal salesAmountYear = BigDecimal.ZERO;
            for (int j = 1; j <= 12; j++) {
                if (!map.containsKey(j) && map.get(j) == null) {
                    discountRates.add("0.00%");
                    continue;
                }

                DiscountRateMonthBean monthBean = map.get(j);
                discountAmountYear = discountAmountYear.add(monthBean.getDiscountAmount());
                salesAmountYear = salesAmountYear.add(monthBean.getSalesAmount());
                discountRates.add(monthBean.getDiscountRate().toString() + "%");
            }

            bean.setMonthList(discountRates);
            if (salesAmountYear.equals(BigDecimal.ZERO)) {
                salesAmountYear = BigDecimal.ONE;
            }
            bean.setAverage(discountAmountYear.divide(salesAmountYear).multiply(HUNDRED).setScale(2).toString() + "%");
            totalDiscountAmount = totalDiscountAmount.add(discountAmountYear);
            totalSalesAmount = totalSalesAmount.add(salesAmountYear);
            list.add(bean);
        }

        paramMap.put("AVERAGE_VALUE", totalDiscountAmount.divide(totalSalesAmount, 2, RoundingMode.HALF_UP).multiply(HUNDRED).toString() + "%");

        return list;
    }

    private Map<String, Object> prepareReportParameters(LocalDate dateFrom, LocalDate dateTo, String productCode, String area, GiftCardSalesType salesType) {
        HashMap<String, Object> reportMap = Maps.newHashMap();
        reportMap.put("DATE_FROM", dateFrom.toString(FULL_DATE_PATTERN));
        reportMap.put("DATE_TO", dateTo.toString(FULL_DATE_PATTERN));
        reportMap.put("AREA", StringUtils.isNotEmpty(area) ? area : "ALL STORES");
        reportMap.put("PRODUCT", StringUtils.isNotEmpty(productCode) ? productService.findProductProfileByCode(productCode).getProductDesc() : "ALL PRODUCTS");
        Locale locale = LocaleContextHolder.getLocale();
        reportMap.put("REPORT_TYPE", messageSource.getMessage("gc.discount.rate.report.label", (Object[]) null, locale));
        reportMap.put("NO_DATA", messageSource.getMessage("report.no.data", (Object[]) null, locale));
        reportMap.put("AVERAGE", messageSource.getMessage("gc.discount.rate.report.label.ave", (Object[]) null, locale));
        reportMap.put("SALES_TYPE", salesType == null ? "ALL SALES TYPE" : salesType.name());
        reportMap.put(JRParameter.REPORT_RESOURCE_BUNDLE, new MessageSourceResourceBundle(messageSource, locale));
        return reportMap;
    }

    public static class DiscountRateMonthBean {

        private BigDecimal discountAmount;
        private BigDecimal salesAmount;

        public DiscountRateMonthBean() {

        }

        public DiscountRateMonthBean(BigDecimal discountAmount) {
            this.discountAmount = discountAmount;
        }

        public DiscountRateMonthBean(BigDecimal discountAmount,
                BigDecimal salesAmount) {
            super();
            this.discountAmount = discountAmount != null ? discountAmount : BigDecimal.ZERO;
            this.salesAmount = salesAmount != null ? salesAmount : BigDecimal.ZERO;
        }

        public BigDecimal getDiscountAmount() {
            return discountAmount;
        }

        public void setDiscountAmount(BigDecimal discountAmount) {
            this.discountAmount = discountAmount;
        }

        public BigDecimal getSalesAmount() {
            return salesAmount;
        }

        public void setSalesAmount(BigDecimal salesAmount) {
            this.salesAmount = salesAmount;
        }

        public BigDecimal getDiscountRate() {
            return discountAmount.divide(salesAmount).multiply(HUNDRED).setScale(2, BigDecimal.ROUND_HALF_EVEN);
        }
    }

    public static class DiscountRateBean {

        private String year;
        private List<String> monthList;
        private String average;

        public String getAverage() {
            return average;
        }

        public void setAverage(String average) {
            this.average = average;
        }

        public String getYear() {
            return year;
        }

        public void setYear(String year) {
            this.year = year;
        }

        public List<String> getMonthList() {
            return monthList;
        }

        public void setMonthList(List<String> monthList) {
            this.monthList = monthList;
        }
    }
}
