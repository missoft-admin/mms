package com.transretail.crm.web.controller.util;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;

public abstract class ControllerResponseExceptionHandler {
    private static final Logger _LOG = LoggerFactory.getLogger(ControllerResponseExceptionHandler.class);
    private String itsDefaultErrorMsg = "An error has occurred";
    
    
    @ExceptionHandler(Exception.class)
	@ResponseBody
	public Object handleThrowable(HttpServletRequest request,
			HttpServletResponse response, Exception ex) {
        _LOG.error(itsDefaultErrorMsg, ex);
    	List<String> theErrors = new ArrayList<String>();
    	
		if (isAjax((HttpServletRequest) request)) {
            theErrors.add(itsDefaultErrorMsg);
            
            ControllerResponse theResponse = new ControllerResponse();
            theResponse.setResult(theErrors);
            theResponse.setSuccess(false);
            return theResponse;
		} 
		else {
		}
		return itsDefaultErrorMsg;
	}
    

    /*private String ajaxErrorView;    
    @Override
    @ResponseBody
    @RequestMapping(produces= "application/json; charset=utf-8")
    protected ResponseEntity<Object> handleExceptionInternal(Exception ex,
    		Object body, HttpHeaders headers, HttpStatus status,
    		WebRequest request) {
    	
		if (isAjax((HttpServletRequest) request)) {
			String exceptionMessage = itsDefaultErrorMsg;
			exceptionMessage += "\n" + getExceptionMessage(ex);

			ModelAndView m = new ModelAndView(ajaxErrorView);
			m.addObject("exceptionMessage", exceptionMessage);
			return new ResponseEntity(exceptionMessage, HttpStatus.INTERNAL_SERVER_ERROR);
		} 
		else {
			return super.handleExceptionInternal(ex, body, headers, status, request);
		}
    }

	private String getExceptionMessage(Throwable e) {
		String message = "";
		while (e != null) {
			message += e.getMessage() + "\n";
			e = e.getCause();
		}
		return message;
	}*/

	private boolean isAjax(HttpServletRequest request) {
		return "XMLHttpRequest".equals(request.getHeader("X-Requested-With"));
	}
	
    public void setDefaultErrorMessage(String inMsg) {
        this.itsDefaultErrorMsg = inMsg;
    }

    public String getDefaultErrorMessage() {
    	return this.itsDefaultErrorMsg;
    }
}