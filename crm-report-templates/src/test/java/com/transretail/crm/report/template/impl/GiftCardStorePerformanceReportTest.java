package com.transretail.crm.report.template.impl;

import java.math.BigDecimal;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import com.google.common.collect.Sets;
import org.joda.time.LocalDate;
import org.joda.time.LocalDateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.google.common.collect.Lists;
import com.transretail.crm.core.entity.lookup.Store;
import com.transretail.crm.core.repo.StoreRepo;
import com.transretail.crm.giftcard.entity.CardVendor;
import com.transretail.crm.giftcard.entity.GiftCardInventory;
import com.transretail.crm.giftcard.entity.GiftCardOrder;
import com.transretail.crm.giftcard.entity.GiftCardTransaction;
import com.transretail.crm.giftcard.entity.GiftCardTransactionItem;
import com.transretail.crm.giftcard.entity.ProductProfile;
import com.transretail.crm.giftcard.entity.SalesOrder;
import com.transretail.crm.giftcard.entity.SalesOrderItem;
import com.transretail.crm.giftcard.entity.SalesOrderPaymentInfo;
import com.transretail.crm.giftcard.entity.support.GiftCardOrderStatus;
import com.transretail.crm.giftcard.entity.support.GiftCardSaleTransaction;
import com.transretail.crm.giftcard.entity.support.PaymentInfoStatus;
import com.transretail.crm.giftcard.entity.support.SalesOrderStatus;
import com.transretail.crm.giftcard.entity.support.SalesOrderType;
import com.transretail.crm.giftcard.repo.CardVendorRepo;
import com.transretail.crm.giftcard.repo.GiftCardInventoryRepo;
import com.transretail.crm.giftcard.repo.GiftCardOrderRepo;
import com.transretail.crm.giftcard.repo.GiftCardTransactionItemRepo;
import com.transretail.crm.giftcard.repo.GiftCardTransactionRepo;
import com.transretail.crm.giftcard.repo.ProductProfileRepo;
import com.transretail.crm.giftcard.repo.SalesOrderItemRepo;
import com.transretail.crm.giftcard.repo.SalesOrderPaymentInfoRepo;
import com.transretail.crm.giftcard.repo.SalesOrderRepo;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath*:/META-INF/spring/applicationContext*.xml",
    "/META-INF/spring/applicationContext-test.xml"})
public class GiftCardStorePerformanceReportTest {
	@PersistenceContext
    private EntityManager em;
	@Autowired
	private SalesOrderRepo salesOrderRepo;
	@Autowired
	private SalesOrderItemRepo salesOrderItemRepo;
	@Autowired
	private SalesOrderPaymentInfoRepo salesOrderPaymentInfoRepo;
	@Autowired
	private StoreRepo storeRepo;
	@Autowired
	private GiftCardInventoryRepo gcInvRepo;
	@Autowired
	private ProductProfileRepo productProfileRepo;
	@Autowired
	private GiftCardTransactionRepo gcTxnRepo;
	@Autowired
	private GiftCardTransactionItemRepo gcTxnItemRepo;
	@Autowired
	private CardVendorRepo cardVendorRepo;
	@Autowired
	private GiftCardOrderRepo gcOrderRepo;
	
	@Autowired
	private GiftCardStorePerformanceReport instance;
	
	private DateTimeFormatter formatter = DateTimeFormat.forPattern("dd-MM-yyyy");
	
	private Store store1, store2;

	
	@Before
	public void setUp() throws Exception {
		store1 = new Store(1);
		store1.setCode("store1");
		store1.setName("store1");
		store1 = storeRepo.save(store1);
		
		store2 = new Store(2);
		store2.setCode("store2");
		store2.setName("store2");
		store2 = storeRepo.save(store2);
		
		setupSalesOrder();
		setupGcInventory();
//		setupGcTransaction();
	}
	
	/**
	 * January, 2014:
	 * 	Order 1 - 50%, 3000; Print Fee - 300; Store - store1; b2b; payment amount - 5000
	 * 	Order 2 - 20%, 1000; Print Fee - 100; Store - store1; not b2b; payment amount - 500
	 * 	Discount Value: 1700
	 * 	Sales Value: 4000
	 *  Discount Rate: 42.5%
	 * February, 2014:
	 * 	Order 3 - 20%, 1000; Print Fee - 200; Store - store2; b2b; payment amount - 1000
	 * January, 2015:
	 * 	Order 4 - 20%, 1000; Print Fee - 100; Store - store2; b2b ; payment amount - 1000
	 */
	private void setupSalesOrder() {
		SalesOrder order = new SalesOrder();
		order.setOrderNo("1");
		order.setOrderDate(LocalDate.parse("01-01-2014", formatter));
		order.setDiscountVal(BigDecimal.valueOf(50L));
		order.setStatus(SalesOrderStatus.SOLD);
		order.setPaymentStore(store1.getCode());
		order.setOrderType(SalesOrderType.B2B_SALES);
		order = salesOrderRepo.save(order);
		
		List<SalesOrderItem> items = Lists.newArrayList();
		SalesOrderItem item = new SalesOrderItem();
		item.setFaceAmount(BigDecimal.valueOf(1000L));
		item.setPrintFee(BigDecimal.valueOf(100L));
		item.setOrder(order);
		items.add(salesOrderItemRepo.save(item));
		
		item = new SalesOrderItem();
		item.setPrintFee(BigDecimal.valueOf(200L));
		item.setFaceAmount(BigDecimal.valueOf(2000L));
		item.setOrder(order);
		items.add(salesOrderItemRepo.save(item));
		
		SalesOrderPaymentInfo paymentInfo = new SalesOrderPaymentInfo();
		paymentInfo.setStatus(PaymentInfoStatus.APPROVED);
		paymentInfo.setPaymentAmount(BigDecimal.valueOf(1000L));
		paymentInfo.setOrder(order);
		salesOrderPaymentInfoRepo.save(paymentInfo);
		
		paymentInfo = new SalesOrderPaymentInfo();
		paymentInfo.setStatus(PaymentInfoStatus.APPROVED);
		paymentInfo.setPaymentAmount(BigDecimal.valueOf(4000L));
		paymentInfo.setOrder(order);
		salesOrderPaymentInfoRepo.save(paymentInfo);
		
		order.setItems(Sets.newHashSet(items));
		
		order = new SalesOrder();
		order.setOrderNo("2");
		order.setOrderDate(LocalDate.parse("05-01-2014", formatter));
		order.setDiscountVal(BigDecimal.valueOf(20L));
		order.setStatus(SalesOrderStatus.SOLD);
		order.setPaymentStore(store1.getCode());
		order.setOrderType(SalesOrderType.INTERNAL_ORDER);
		order = salesOrderRepo.save(order);
		
		items = Lists.newArrayList();
		item = new SalesOrderItem();
		item.setPrintFee(BigDecimal.valueOf(100L));
		item.setFaceAmount(BigDecimal.valueOf(1000L));
		item.setOrder(order);
		items.add(salesOrderItemRepo.save(item));
		
		paymentInfo = new SalesOrderPaymentInfo();
		paymentInfo.setStatus(PaymentInfoStatus.APPROVED);
		paymentInfo.setPaymentAmount(BigDecimal.valueOf(5000L));
		paymentInfo.setOrder(order);
		salesOrderPaymentInfoRepo.save(paymentInfo);
		
		order.setItems(Sets.newHashSet(items));
		
		order = new SalesOrder();
		order.setOrderNo("3");
		order.setOrderDate(LocalDate.parse("02-02-2014", formatter));
		order.setDiscountVal(BigDecimal.valueOf(20L));
		order.setStatus(SalesOrderStatus.SOLD);
		order.setPaymentStore(store2.getCode());
		order.setOrderType(SalesOrderType.B2B_SALES);
		order = salesOrderRepo.save(order);
		
		items = Lists.newArrayList();
		item = new SalesOrderItem();
		item.setPrintFee(BigDecimal.valueOf(200L));
		item.setFaceAmount(BigDecimal.valueOf(1000L));
		item.setOrder(order);
		items.add(salesOrderItemRepo.save(item));
		
		paymentInfo = new SalesOrderPaymentInfo();
		paymentInfo.setStatus(PaymentInfoStatus.APPROVED);
		paymentInfo.setPaymentAmount(BigDecimal.valueOf(1000L));
		paymentInfo.setOrder(order);
		salesOrderPaymentInfoRepo.save(paymentInfo);
		
		order.setItems(Sets.newHashSet(items));
		
		order = new SalesOrder();
		order.setOrderNo("4");
		order.setOrderDate(LocalDate.parse("02-01-2015", formatter));
		order.setDiscountVal(BigDecimal.valueOf(20L));
		order.setStatus(SalesOrderStatus.SOLD);
		order.setPaymentStore(store2.getCode());
		order.setOrderType(SalesOrderType.B2B_SALES);
		order = salesOrderRepo.save(order);
		
		items = Lists.newArrayList();
		item = new SalesOrderItem();
		item.setPrintFee(BigDecimal.valueOf(100L));
		item.setFaceAmount(BigDecimal.valueOf(1000L));
		item.setOrder(order);
		items.add(salesOrderItemRepo.save(item));
		
		paymentInfo = new SalesOrderPaymentInfo();
		paymentInfo.setStatus(PaymentInfoStatus.APPROVED);
		paymentInfo.setPaymentAmount(BigDecimal.valueOf(1000L));
		paymentInfo.setOrder(order);
		salesOrderPaymentInfoRepo.save(paymentInfo);
		
		order.setItems(Sets.newHashSet(items));
	}

	/**
	 * Store1:
	 * 1 - 1000; 01-01-2014
	 * 2 - 2000; 05-01-2014
	 * 3 - 3000; 01-02-2014
	 * Store2:
	 * 4 - 4000; 01-01-2014
	 * 5 - 1000; 01-02-2014
	 */
	private void setupGcInventory() {
		ProductProfile product = new ProductProfile();
		product.setProductCode("product1");
		product.setProductDesc("product1");
		product = productProfileRepo.save(product);
		
		CardVendor cardVendor = new CardVendor();
		cardVendor.setFormalName("cardVendor");
		cardVendor.setMailAddress("cardVendor");
		cardVendor.setEmailAddress("test@test.com");
		cardVendor.setEnabled(true);
		cardVendor.setRegion("test");
		cardVendor.setDefaultVendor(true);
		cardVendor = cardVendorRepo.save(cardVendor);
		
		GiftCardOrder gcOrder = new GiftCardOrder();
		gcOrder.setMoNumber("1");
		gcOrder.setMoDate(LocalDate.parse("01-01-2014", formatter));
		gcOrder.setPoDate(LocalDate.parse("01-01-2014", formatter));
		gcOrder.setPoNumber("1");
		gcOrder.setVendor(cardVendor);
		gcOrder.setStatus(GiftCardOrderStatus.APPROVED);
		gcOrder = gcOrderRepo.saveAndFlush(gcOrder);
		
		GiftCardInventory gcInv = new GiftCardInventory();
		gcInv.setProfile(product);
		gcInv.setProductCode(product.getProductCode());
		gcInv.setProductName(product.getProductDesc());
		gcInv.setBarcode("1");
		gcInv.setLocation(store1.getCode());
		gcInv.setFaceValue(BigDecimal.valueOf(1000L));
		gcInv.setActivationDate(LocalDate.parse("01-01-2014", formatter));
		gcInv.setOrder(gcOrder);
		gcInv.setOrderDate(gcOrder.getMoDate());
		gcInv.setSeqNo(1);
		gcInv.setSeries(1L);
		gcInv.setDeliveryReceipt("1");
		gcInv.setReceiveDate(LocalDate.parse("01-01-2014", formatter));
		gcInv.setBatchNo(1);
		gcInvRepo.save(gcInv);
		
//		gcInv = new GiftCardInventory();
//		gcInv.setProfile(product);
//		gcInv.setProductCode(product.getProductCode());
//		gcInv.setProductName(product.getProductDesc());
//		gcInv.setBarcode("1");
//		gcInv.setLocation(store1.getCode());
//		gcInv.setFaceValue(BigDecimal.valueOf(1000L));
//		gcInv.setActivationDate(LocalDate.parse("05-01-2014", formatter));
//		gcInv.setOrder(gcOrder);
//		gcInv.setOrderDate(gcOrder.getMoDate());
//		gcInv.setSeqNo(1);
//		gcInv.setSeries(1L);
//		gcInv.setDeliveryReceipt("1");
//		gcInv.setReceiveDate(LocalDate.parse("01-01-2014", formatter));
//		gcInv.setBatchNo(1);
//		gcInvRepo.save(gcInv);
//		
//		gcInv = new GiftCardInventory();
//		gcInv.setProfile(product);
//		gcInv.setProductCode(product.getProductCode());
//		gcInv.setProductName(product.getProductDesc());
//		gcInv.setBarcode("2");
//		gcInv.setLocation(store1.getCode());
//		gcInv.setFaceValue(BigDecimal.valueOf(2000L));
//		gcInv.setActivationDate(LocalDate.parse("01-02-2014", formatter));
//		gcInv.setOrder(gcOrder);
//		gcInv.setOrderDate(gcOrder.getMoDate());
//		gcInv.setSeqNo(2);
//		gcInv.setSeries(2L);
//		gcInv.setDeliveryReceipt("1");
//		gcInv.setReceiveDate(LocalDate.parse("01-01-2014", formatter));
//		gcInv.setBatchNo(2);
//		gcInvRepo.save(gcInv);
//		
//		gcInv = new GiftCardInventory();
//		gcInv.setProfile(product);
//		gcInv.setProductCode(product.getProductCode());
//		gcInv.setProductName(product.getProductDesc());
//		gcInv.setBarcode("3");
//		gcInv.setLocation(store1.getCode());
//		gcInv.setFaceValue(BigDecimal.valueOf(3000L));
//		gcInv.setActivationDate(LocalDate.parse("01-01-2014", formatter));
//		gcInv.setOrder(gcOrder);
//		gcInv.setOrderDate(gcOrder.getMoDate());
//		gcInv.setSeqNo(3);
//		gcInv.setSeries(3L);
//		gcInv.setDeliveryReceipt("1");
//		gcInv.setReceiveDate(LocalDate.parse("01-01-2014", formatter));
//		gcInv.setBatchNo(3);
//		gcInvRepo.save(gcInv);
//		
//		gcInv = new GiftCardInventory();
//		gcInv.setProfile(product);
//		gcInv.setProductCode(product.getProductCode());
//		gcInv.setProductName(product.getProductDesc());
//		gcInv.setBarcode("4");
//		gcInv.setLocation(store2.getCode());
//		gcInv.setFaceValue(BigDecimal.valueOf(4000L));
//		gcInv.setActivationDate(LocalDate.parse("01-01-2014", formatter));
//		gcInv.setOrder(gcOrder);
//		gcInv.setOrderDate(gcOrder.getMoDate());
//		gcInv.setSeqNo(4);
//		gcInv.setSeries(4L);
//		gcInv.setDeliveryReceipt("1");
//		gcInv.setReceiveDate(LocalDate.parse("01-01-2014", formatter));
//		gcInv.setBatchNo(4);
//		gcInvRepo.save(gcInv);
//		
//		gcInv = new GiftCardInventory();
//		gcInv.setProfile(product);
//		gcInv.setProductCode(product.getProductCode());
//		gcInv.setProductName(product.getProductDesc());
//		gcInv.setBarcode("5");
//		gcInv.setLocation(store2.getCode());
//		gcInv.setFaceValue(BigDecimal.valueOf(1000L));
//		gcInv.setActivationDate(LocalDate.parse("01-02-2014", formatter));
//		gcInv.setOrder(gcOrder);
//		gcInv.setOrderDate(gcOrder.getMoDate());
//		gcInv.setSeqNo(5);
//		gcInv.setSeries(5L);
//		gcInv.setDeliveryReceipt("1");
//		gcInv.setReceiveDate(LocalDate.parse("01-01-2014", formatter));
//		gcInv.setBatchNo(5);
//		gcInvRepo.save(gcInv);
	}

	/**
	 * Store1:
	 * 01-01-2014: REDEMPTION, 2000
	 * 01-01-2014: ACTIVATION, 1000
	 * 01-02-2014: REDEMPTION, 1000
	 * Store2:
	 * 01-01-2014: REDEMPTION, 1000
	 */
	private void setupGcTransaction() {
		GiftCardTransaction transaction = new GiftCardTransaction();
		transaction.setTransactionDate(LocalDateTime.parse("01-01-2014", formatter));
		transaction.setTransactionType(GiftCardSaleTransaction.REDEMPTION);
		transaction.setMerchantId(store1.getCode());
		transaction.setLoseTransaction(false);
		transaction.setClaimedByAffiliates(false);
		transaction.setTransactionNo("1");
		transaction = gcTxnRepo.saveAndFlush(transaction);
		
		GiftCardTransactionItem gcTxnItem = new GiftCardTransactionItem();
		gcTxnItem.setTransaction(transaction);
		gcTxnItem.setTransactionAmount(1000D);
		gcTxnItemRepo.save(gcTxnItem);
		
		gcTxnItem = new GiftCardTransactionItem();
		gcTxnItem.setTransaction(transaction);
		gcTxnItem.setTransactionAmount(1000D);
		gcTxnItemRepo.save(gcTxnItem);
		
		transaction = new GiftCardTransaction();
		transaction.setTransactionDate(LocalDateTime.parse("01-02-2014", formatter));
		transaction.setTransactionType(GiftCardSaleTransaction.REDEMPTION);
		transaction.setMerchantId(store1.getCode());
		transaction.setLoseTransaction(false);
		transaction.setClaimedByAffiliates(false);
		transaction.setTransactionNo("2");
		transaction = gcTxnRepo.saveAndFlush(transaction);
		
		gcTxnItem = new GiftCardTransactionItem();
		gcTxnItem.setTransaction(transaction);
		gcTxnItem.setTransactionAmount(1000D);
		gcTxnItemRepo.save(gcTxnItem);
		
		transaction = new GiftCardTransaction();
		transaction.setTransactionDate(LocalDateTime.parse("01-01-2014", formatter));
		transaction.setTransactionType(GiftCardSaleTransaction.ACTIVATION);
		transaction.setMerchantId(store1.getCode());
		transaction.setLoseTransaction(false);
		transaction.setClaimedByAffiliates(false);
		transaction.setTransactionNo("3");
		transaction = gcTxnRepo.saveAndFlush(transaction);
		
		gcTxnItem = new GiftCardTransactionItem();
		gcTxnItem.setTransaction(transaction);
		gcTxnItem.setTransactionAmount(1000D);
		gcTxnItemRepo.save(gcTxnItem);
		
		transaction = new GiftCardTransaction();
		transaction.setTransactionDate(LocalDateTime.parse("01-01-2014", formatter));
		transaction.setTransactionType(GiftCardSaleTransaction.REDEMPTION);
		transaction.setMerchantId(store2.getCode());
		transaction.setLoseTransaction(false);
		transaction.setClaimedByAffiliates(false);
		transaction.setTransactionNo("4");
		transaction = gcTxnRepo.saveAndFlush(transaction);
		
		gcTxnItem = new GiftCardTransactionItem();
		gcTxnItem.setTransaction(transaction);
		gcTxnItem.setTransactionAmount(1000D);
		gcTxnItemRepo.save(gcTxnItem);
	}

	@Test
	public void test() {
		instance.createDatasource("", "", LocalDate.parse("01-01-2014", formatter), 
			LocalDate.parse("31-01-2014", formatter), LocalDate.parse("01-02-2014", formatter), 
			LocalDate.parse("28-02-2014", formatter),
			null);
	}
}
