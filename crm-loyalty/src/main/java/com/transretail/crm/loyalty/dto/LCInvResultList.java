package com.transretail.crm.loyalty.dto;

import java.util.Collection;

import org.springframework.data.domain.Page;

import com.transretail.crm.common.service.dto.response.AbstractResultListDTO;

/**
 * @author Allan G. Ramirez (agramirez@exist.com)
 */
public class LCInvResultList extends AbstractResultListDTO<LCInvResultDto> {
    public LCInvResultList(Collection<LCInvResultDto> results, long totalElements, boolean hasPreviousPage,
        boolean hasNextPage) {
        super(results, totalElements, hasPreviousPage, hasNextPage);
    }

    public LCInvResultList(Collection<LCInvResultDto> results, long totalElements, int pageNo, int pageSize) {
        super(results, totalElements, pageNo, pageSize);
    }

    public LCInvResultList(Page<LCInvResultDto> page) {
        super(page);
    }

}
