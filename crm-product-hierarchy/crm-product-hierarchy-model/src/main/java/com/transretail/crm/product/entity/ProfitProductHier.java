package com.transretail.crm.product.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.Immutable;
import org.hibernate.annotations.Parameter;
import org.hibernate.annotations.Type;
import org.joda.time.DateTime;

import com.transretail.crm.product.HierarchyLevel;

/**
 * @author Allan G. Ramirez (agramirez@exist.com)
 */
@Entity
@Table(name = "PROFIT_PROD_HIER")
@Immutable
public class ProfitProductHier {
    @Id
    @Column(name = "OMDHMDHCD")
    private String code;
    @Column(name = "OMDHEDESC")
    private String englishDesc;
    @Column(name = "OMDHLDESC")
    private String localDesc;
    @Column(name = "OMDHMLVNO")
    @Enumerated(EnumType.ORDINAL)
    private HierarchyLevel level;
    @Column(name = "OMDHPARCD")
    private String parentLevel;
    @Column(name = "OMDHCREDAT")
    @Type(type = "org.jadira.usertype.dateandtime.joda.PersistentDateTime",
        parameters = {@Parameter(name = "databaseZone", value = "jvm")})
    private DateTime created;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getEnglishDesc() {
        return englishDesc;
    }

    public void setEnglishDesc(String englishDesc) {
        this.englishDesc = englishDesc;
    }

    public String getLocalDesc() {
        return localDesc;
    }

    public void setLocalDesc(String localDesc) {
        this.localDesc = localDesc;
    }

    public HierarchyLevel getLevel() {
        return level;
    }

    public void setLevel(HierarchyLevel level) {
        this.level = level;
    }

    public String getParentLevel() {
        return parentLevel;
    }

    public void setParentLevel(String parentLevel) {
        this.parentLevel = parentLevel;
    }

    public DateTime getCreated() {
        return created;
    }

    public void setCreated(DateTime created) {
        this.created = created;
    }
}
