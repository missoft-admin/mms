package com.transretail.crm.giftcard.dto;

import com.transretail.crm.giftcard.entity.SalesOrderHistory;

public class SalesOrderHistoryDto {
	private Long id;
	private String createdDate;
	private String createdUser;
	private String status;

	public SalesOrderHistoryDto() {
	}

	public SalesOrderHistoryDto(SalesOrderHistory history) {
		this.id=history.getId();
		this.createdDate=history.getCreated().toString();
		this.createdUser=history.getCreateUser();
		this.status=history.getStatus().toString();
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(String createdDate) {
		this.createdDate = createdDate;
	}

	public String getCreatedUser() {
		return createdUser;
	}

	public void setCreatedUser(String createdUser) {
		this.createdUser = createdUser;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

}
