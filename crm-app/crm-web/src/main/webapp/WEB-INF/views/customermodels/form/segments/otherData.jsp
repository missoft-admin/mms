<%@ include file="../../../common/taglibs.jsp" %>


  <fieldset class="form-horizontal container-fluid span3">
    <legend class="memberDetails"></legend>
    <div class="control-group">
      <label for="vehiclesOwned" class=""> <spring:message code="member_prop_vehiclesowned" /></label>
      <div id="vehiclesOwned" class="checkbox">
        <form:checkboxes items="${vehiclesOwned}" path="customerProfile.vehiclesOwned" itemValue="code" itemLabel="description" />
      </div>
    </div>

    <hr/>

    <div class="checkbox">
      <input type="checkbox" name="customerProfile.insuranceOwnership">
      <label class=""><spring:message code="label_com_transretail_crm_entity_customermodel_insurance" /></label>
    </div>
    <div class="checkbox" class="hide">
      <input type="checkbox" id="hasCreditCard" name="customerProfile.creditCardOwnership" />
      <label class=""><spring:message code="member_prop_hascc" /></label>
    </div>
    <div class="row-fluid banks">
      <div class=""><label class=""><spring:message code="label_com_transretail_crm_entity_customermodel_banks" /></label></div>
      <form:select id="banks" path="customerProfile.banks" multiple="multiple" items="${bank}" itemValue="code" itemLabel="description" >
         <!--<option value="" class="bankOption" id="noBank">NONE</option>  
        <form:options items="${bank}" itemValue="code" itemLabel="description" /> -->
      </form:select>
    </div>
  </fieldset>




<script>
$(document).ready(function() {
//  var lastOption = null;
//  $(".bankOption").mouseup(function() {
//    lastOption = $(this).val();
//    console.log(lastOption);
//    if(lastOption) {
//     $("#noBank").prop("selected", false);
//      $("#hasCreditCard").prop("checked", true);
//  }
//  else {
//    $("#banks").val(lastOption);
//    $("#noBank").prop("selected", true);
//    $("#hasCreditCard").prop("checked", false);
//  }
//});
});
</script>