package com.transretail.crm.report.template.impl;

import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.apache.commons.lang3.StringUtils;
import org.joda.time.LocalDate;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.MessageSourceAware;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.context.support.MessageSourceAccessor;
import org.springframework.stereotype.Service;

import com.google.common.collect.Maps;
import com.google.common.collect.Sets;
import com.mysema.query.BooleanBuilder;
import com.mysema.query.jpa.JPASubQuery;
import com.mysema.query.jpa.impl.JPAQuery;
import com.mysema.query.types.Projections;
import com.mysema.query.types.QBean;
import com.mysema.query.types.expr.CaseBuilder;
import com.mysema.query.types.expr.NumberExpression;
import com.transretail.crm.common.reporting.jasper.ClasspathFileResolver;
import com.transretail.crm.common.reporting.jasper.DefaultJRProcessor;
import com.transretail.crm.common.reporting.jasper.JRProcessor;
import com.transretail.crm.common.util.DateUtil;
import com.transretail.crm.core.entity.enums.ReportType;
import com.transretail.crm.core.entity.lookup.LookupDetail;
import com.transretail.crm.core.entity.lookup.QStore;
import com.transretail.crm.core.security.util.UserUtil;
import com.transretail.crm.core.service.CodePropertiesService;
import com.transretail.crm.core.service.LookupService;
import com.transretail.crm.giftcard.entity.GiftCardInventoryStock.GCIStockStatus;
import com.transretail.crm.giftcard.entity.QGiftCardInventory;
import com.transretail.crm.giftcard.entity.QGiftCardInventoryStock;
import com.transretail.crm.giftcard.entity.QProductProfile;
import com.transretail.crm.report.template.CommonReportFilter;
import com.transretail.crm.report.template.FilterField;
import com.transretail.crm.report.template.ReportTemplate;
import com.transretail.crm.report.template.ReportsCustomizer;

@Service("giftCardDailyActivityReport")
public class GiftCardDailyActivityReport implements ReportTemplate, ReportsCustomizer, MessageSourceAware {
	protected static final String TEMPLATE_NAME = "Gift Card Daily Inventory Report";
    protected static final String REPORT_NAME = "reports/gift_card_daily_inventory.jasper";
    protected static final String FILTER_DATE_TO = "createdToStr";
    protected static final String FILTER_DATE_FROM = "createdFromStr";
    protected static final String FILTER_CARD_TYPE = "cardType";
    protected static final String FILTER_LOCATION = "location";
    
    private static final DateTimeFormatter INPUT_DATE_PATTERN = DateTimeFormat.forPattern("dd MMM yyyy");
    
    private static final Logger LOGGER = LoggerFactory.getLogger(GiftCardDailyActivityReport.class);
    
    @PersistenceContext
    private EntityManager em;
    private MessageSourceAccessor messageSource;
    @Autowired
    private LookupService lookupService;
    @Autowired
    private CodePropertiesService codePropertiesService;
    
    @Override
    public void setMessageSource(MessageSource messageSource) {
    	this.messageSource = new MessageSourceAccessor(messageSource);
    }
    
    @Override
    public ReportType[] getReportTypes() {
    	return ReportType.values();
    }
    
    @Override
    public String getName() {
    	return TEMPLATE_NAME;
    }

    @Override
    public String getDescription() {
    	return messageSource.getMessage("daily.inventory.title", (Object[]) null, LocaleContextHolder.getLocale());
    }
    
    @Override
    public boolean canView(Set<String> permissions) {
    	return true;
    }
    
    @Override
	public Set<Option> customizerOption() {
    	return Sets.newHashSet(ReportsCustomizer.Option.DATE_RANGE_FULL_VALIDATION);
	}
    
    @Override
    public Set<FilterField> getFilters() {
    	Set<FilterField> filters = Sets.newLinkedHashSet();
		filters.add(FilterField.createDateField(FILTER_DATE_FROM,
				messageSource.getMessage("label_date_from"),
				DateUtil.SEARCH_DATE_FORMAT, true));
			filters.add(FilterField.createDateField(FILTER_DATE_TO,
				messageSource.getMessage("label_date_to"),
				DateUtil.SEARCH_DATE_FORMAT, true));
			
			
			Map<String, String> products = Maps.newLinkedHashMap();
			products.putAll(getProducts());
			filters.add(FilterField.createDropdownField(FILTER_CARD_TYPE,
	                messageSource.getMessage("daily.inventory.cardtype", (Object[]) null, LocaleContextHolder.getLocale()), products));
			
			LookupDetail headOffice = lookupService.getDetailByCode(codePropertiesService.getDetailInvLocationHeadOffice());
			
			Map<String, String> stores = Maps.newLinkedHashMap();
			stores.put(headOffice.getCode(), headOffice.getCode() + " - " + headOffice.getDescription());
			stores.putAll(getStores());
			filters.add(FilterField.createDropdownField(FILTER_LOCATION,
	                messageSource.getMessage("daily.inventory.location", (Object[]) null, LocaleContextHolder.getLocale()), stores));
			
		return filters;
    }
    
    private Map<String, String> getProducts() {
    	QProductProfile qPrd = QProductProfile.productProfile;
    	return new JPAQuery(em).from(qPrd).orderBy(qPrd.productDesc.asc()).map(qPrd.productCode, qPrd.productDesc);
    }
    
    private Map<String, String> getStores() {
    	QStore qstore = QStore.store;
    	return new JPAQuery(em).from(qstore).orderBy(qstore.code.asc()).map(qstore.code, qstore.code.append(" - ").append(qstore.name));
    }
    
    @Override
    public JRProcessor createJrProcessor(Map<String, String> map) {
    	LocalDate startDate = null;
    	if(StringUtils.isNotBlank(map.get(FILTER_DATE_FROM)))
    		startDate = INPUT_DATE_PATTERN.parseLocalDate(map.get(FILTER_DATE_FROM));
		LocalDate endDate = null;
		if(StringUtils.isNotBlank(map.get(FILTER_DATE_TO)))
			endDate = INPUT_DATE_PATTERN.parseLocalDate(map.get(FILTER_DATE_TO));
		String prodCode = map.get(FILTER_CARD_TYPE);
		String location = map.get(FILTER_LOCATION);
		
		
		
		QGiftCardInventoryStock qstock = QGiftCardInventoryStock.giftCardInventoryStock;
		QGiftCardInventory qinventory = QGiftCardInventory.giftCardInventory;
		BooleanBuilder filter = new BooleanBuilder(
				qstock.seriesStart.eq(qinventory.series)
				.and(qstock.location.eq(location))
				.and(qinventory.productCode.eq(prodCode))
				);
		if(startDate != null)
			filter.and(qstock.createdDate.goe(startDate));
		if(endDate != null)
			filter.and(qstock.createdDate.loe(endDate));
		
		NumberExpression<Long> received = new CaseBuilder()
			.when(qstock.status.in(GCIStockStatus.RECEIVED.name(), GCIStockStatus.TRANSFERRED_IN.name())).then(qstock.quantity)
			.otherwise(0l);
		NumberExpression<Long> transferedOut = new CaseBuilder()
			.when(qstock.status.in(GCIStockStatus.TRANSFERRED_OUT.name())).then(qstock.quantity.negate())
			.otherwise(0l);
		NumberExpression<Long> activated = new CaseBuilder()
			.when(qstock.status.in(GCIStockStatus.ACTIVATED.name())).then(qstock.quantity.negate())
			.when(qstock.status.in(GCIStockStatus.VOID_ACTIVATION.name())).then(qstock.quantity)
			.otherwise(0l);
		NumberExpression<Long> returned = new CaseBuilder()
			.when(qstock.status.in(GCIStockStatus.RETURNED.name())).then(qstock.quantity)
			.otherwise(0l);
		NumberExpression<Long> burned = new CaseBuilder()
			.when(qstock.status.in(GCIStockStatus.BURNED.name())).then(qstock.quantity.negate())
			.otherwise(0l);
		NumberExpression<Long> physicalCount = new CaseBuilder()
			.when(qstock.status.in(GCIStockStatus.PHYSCOUNT_FOUND.name())).then(qstock.quantity)
			.when(qstock.status.in(GCIStockStatus.PHYSCOUNT_MISSING.name())).then(qstock.quantity.negate())
			.otherwise(0l);
		
		
		QGiftCardInventoryStock qstockA = new QGiftCardInventoryStock("stockA");
		QGiftCardInventory qinventoryA = new QGiftCardInventory("inventoryA");
		NumberExpression<Long> numExp = new CaseBuilder()
			.when(qstockA.status.equalsIgnoreCase(GCIStockStatus.RECEIVED.name())).then(qstockA.quantity)
			.when(qstockA.status.equalsIgnoreCase(GCIStockStatus.TRANSFERRED_OUT.name())).then(qstockA.quantity.negate())
			.when(qstockA.status.equalsIgnoreCase(GCIStockStatus.TRANSFERRED_IN.name())).then(qstockA.quantity)
			.when(qstockA.status.equalsIgnoreCase(GCIStockStatus.ACTIVATED.name())).then(qstockA.quantity.negate())
			.when(qstockA.status.equalsIgnoreCase(GCIStockStatus.VOID_ACTIVATION.name())).then(qstockA.quantity)
			.when(qstockA.status.equalsIgnoreCase(GCIStockStatus.RETURNED.name())).then(qstockA.quantity)
			.when(qstockA.status.equalsIgnoreCase(GCIStockStatus.BURNED.name())).then(qstockA.quantity.negate())
			.when(qstockA.status.equalsIgnoreCase(GCIStockStatus.PHYSCOUNT.name())).then(0L)
			.when(qstockA.status.equalsIgnoreCase(GCIStockStatus.PHYSCOUNT_MISSING.name())).then(qstockA.quantity.negate())
			.when(qstockA.status.equalsIgnoreCase(GCIStockStatus.PHYSCOUNT_FOUND.name())).then(qstockA.quantity)
			.otherwise(0l);
		NumberExpression<Long> begBal = new JPASubQuery().from(qstockA, qinventoryA)
				.where(qstockA.createdDate.lt(qstock.createdDate)
						.and(qstockA.seriesStart.eq(qinventoryA.series))
						.and(qinventoryA.productCode.eq(prodCode))
						.and(qstockA.location.eq(location)))
				.unique(numExp.sum());
		
		QBean<StockSummary> projection = Projections.bean(StockSummary.class,
			qstock.createdDate.as("transactionDate"),
			received.sum().as("received"),
			transferedOut.sum().as("transferredOut"),
			activated.sum().as("activated"),
			returned.sum().as("returned"),
			burned.sum().as("burned"),
			physicalCount.sum().as("physicalCount"),
			begBal.as("beginningBal")
			); 
		
		List<StockSummary> list = new JPAQuery(em).from(qstock, qinventory).where(filter)
			.groupBy(qstock.createdDate)
			.orderBy(qstock.createdDate.desc())
			.list(projection);
		
		DefaultJRProcessor jrProcessor = new DefaultJRProcessor(REPORT_NAME);
        jrProcessor.setFileResolver(new ClasspathFileResolver("reports"));
        jrProcessor.addParameter("SUB_DATA_SOURCE", list);
        Map<String, Object> params = Maps.newHashMap();
        for(Entry<String, String> e: map.entrySet()) {
        	params.put(e.getKey(), e.getValue());
        }
        
        if(StringUtils.isNotBlank(prodCode)) {
        	QProductProfile qprofile = QProductProfile.productProfile;
        	params.put("cardTypeStr", new JPAQuery(em).from(qprofile).where(qprofile.productCode.eq(prodCode)).singleResult(qprofile.productCode.append(" - ").append(qprofile.productDesc)));
        }
        if(StringUtils.isNotBlank(location)) {
        	if(location.equalsIgnoreCase(codePropertiesService.getDetailInvLocationHeadOffice())) {
        		LookupDetail ho = lookupService.getActiveDetailByCode(codePropertiesService.getDetailInvLocationHeadOffice());
        		params.put("locationStr", ho.getCode() + " - " + ho.getDescription());
        	} else {
        		QStore qstore = QStore.store;
        		params.put("locationStr", new JPAQuery(em).from(qstore).where(qstore.code.eq(location)).singleResult(qstore.code.append(" - ").append(qstore.name)));
        	}
        }
        
        jrProcessor.addParameters(params);
        jrProcessor.addParameter("printedBy", UserUtil.getCurrentUser().getUsername());
        jrProcessor.addParameter("printedDate", new LocalDate().toString("dd-MM-yyyy"));
        
        return jrProcessor;
    }
    
    public static class StockSummary {
    	private LocalDate transactionDate;
    	private Long received;
    	private Long transferredOut;
    	private Long activated;
    	private Long returned;
    	private Long burned;
    	private Long physicalCount;
    	
    	private Long beginningBal;
		
		public LocalDate getTransactionDate() {
			return transactionDate;
		}
		public void setTransactionDate(LocalDate transactionDate) {
			this.transactionDate = transactionDate;
		}
		public Long getReceived() {
			return received;
		}
		public void setReceived(Long received) {
			this.received = received;
		}
		
		public Long getTransferredOut() {
			return transferredOut;
		}
		public void setTransferredOut(Long transferredOut) {
			this.transferredOut = transferredOut;
		}
		public Long getActivated() {
			return activated;
		}
		public void setActivated(Long activated) {
			this.activated = activated;
		}
		public Long getReturned() {
			return returned;
		}
		public void setReturned(Long returned) {
			this.returned = returned;
		}
		public Long getBurned() {
			return burned;
		}
		public void setBurned(Long burned) {
			this.burned = burned;
		}
		public Long getPhysicalCount() {
			return physicalCount;
		}
		public void setPhysicalCount(Long physicalCount) {
			this.physicalCount = physicalCount;
		}
		public Long getBeginningBal() {
			return beginningBal;
		}
		public void setBeginningBal(Long beginningBal) {
			this.beginningBal = beginningBal;
		}
		public Long getEndingBal() {
			Long endingBal = 0l;
			if(beginningBal != null)
				endingBal += beginningBal;
			if(received != null)
				endingBal += received;
			if(transferredOut != null)
				endingBal += transferredOut;
			if(activated != null)
				endingBal += activated;
			if(returned != null)
				endingBal += returned;
			if(burned != null)
				endingBal += burned;
			if(physicalCount != null)
				endingBal += physicalCount;
			return endingBal;
		}
    	
    	
    	
    }
}
