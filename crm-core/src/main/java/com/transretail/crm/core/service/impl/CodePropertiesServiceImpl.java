package com.transretail.crm.core.service.impl;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.transretail.crm.core.service.CodePropertiesService;


@Service("codePropertiesService")
public class CodePropertiesServiceImpl implements CodePropertiesService {

	//********* USER PERMISSIONS ***********
	@Value("#{'${permission.promo.banner.management}'}")
	private String permissionPromoBannerMgmt;
	@Value("#{'${permission.promotions.management}'}")
	private String permissionPromotionsMgmt;
	@Value("#{'${permission.loyalty.card.manufacture.orders}'}")
	private String permissionLoyaltyCardManufactureOrders;
	@Value("#{'${permission.loyalty.card.inventory}'}")
	private String permissionLoyaltyCardInventory;
	@Value("#{'${permission.rewards.approval}'}")
	private String permissionEmployeeRewardsApproval;
	@Value("#{'${permission.member.points.adjustment}'}")
	private String permissionMemberPointsAdjustment;
	@Value("#{'${permission.employee.points.adjustment}'}")
	private String permissionEmployeePointsAdjustment;
	@Value("#{'${permission.employee.purchase.adjustment}'}")
	private String permissionEmployeePurchaseAdjustment;
    @Value("#{'${permission.media.mgmt}'}")
    private String permissionMediaMgmt;
    @Value("#{'${permission.complaints.mgmt}'}")
    private String permissionComplaintsMgmt;

    //********* HEADER CODES   ***********
    @Value("#{'${header.title}'}")
    private String headerTitle;

    @Value("#{'${header.gender}'}")
    private String headerGender;

    @Value("#{'${header.nationality}'}")
    private String headerNationality;

    @Value("#{'${header.marital.status}'}")
    private String headerMaritalStatus;

    @Value("#{'${header.preferred.language}'}")
    private String headerPreferredLanguage;

    @Value("#{'${header.account.status}'}")
    private String headerAccountStatus;

    @Value("#{'${header.member.type}'}")
    private String headerMemberType;

    @Value("#{'${header.membership.tier}'}")
    private String headerMembershipTier;

    @Value("#{'${header.education}'}")
    private String headerEducation;

    @Value("#{'${header.occupation}'}")
    private String headerOccupation;

    @Value("#{'${header.personal.income}'}")
    private String headerPersonalIncome;

    @Value("#{'${header.household.income}'}")
    private String headerHouseHoldIncome;

    @Value("#{'${header.family.size}'}")
    private String headerFamilySize;

    @Value("#{'${header.children}'}")
    private String headerChildren;

    @Value("#{'${header.pet.ownership}'}")
    private String headerPetOwnership;

    @Value("#{'${header.food.preference}'}")
    private String headerFoodPreference;

    @Value("#{'${header.interest}'}")
    private String headerInterest;

    @Value("#{'${header.payment.type}'}")
    private String headerPaymentType;
    
    @Value("#{'${header.payment.type.wildcard}'}")
    private String headerPaymentTypeWildcard;

    @Value("#{'${header.points.adjustment}'}")
    private String headerPointsAdjustment;

    @Value("#{'${header.age.filter}'}")
    private String headerAgeFilter;

    @Value("#{'${header.days}'}")
    private String headerDays;

    @Value("#{'${header.reward.type}'}")
    private String headerRewardType;

    @Value("#{'${header.inventory.location}'}")
    private String headerInventoryLocation;

    @Value("#{'${header.message.code}'}")
    private String headerMessageCode;
    
    @Value("#{'${header.operands}'}")
    private String headerOperands;
    
    @Value("#{'${header.general.status}'}")
    private String headerStatus;
    
    @Value("#{'${header.field.type}'}")
    private String headerFieldTypes;

    @Value("#{'${header.employee.approved.payment.media.code}'}")
    private String headerEmployeeApprovedPaymentMedia;

    @Value("#{'${header.employee.approved.card.filter.code}'}")
    private String headerEmployeeApprovedCardFilter;
    
    @Value("#{'${header.product.group.classification.code}'}")
    private String headerProductGrpClassification;
    
    @Value("#{'${header.product.group.subclassification.code}'}")
    private String headerProductGrpSubclassification;
    
    @Value("#{'${header.product.classification}'}")
    private String headerProductClassification;

    @Value("#{'${header.customer.group}'}")
    private String headerCustomerGroup;
    
    @Value("#{'${header.customer.segmentation}'}")
    private String headerCustomerSegmentation;
    
    @Value("#{'${header.manufacture.order.status}'}")
    private String headerMoStatus;
    
    @Value("#{'${header.loyalty.vendors}'}")
    private String headerVendors;
    
    @Value("#{'${header.employee.type}'}")
    private String empType;

    @Value("#{'${header.loyalty.invetory.status}'}")
    private String headerLoyaltyStatus;
    
    @Value("#{'${header.religion}'}")
    private String headerReligion;
    
    @Value("#{'${header.department.field}'}")
    private String headerDepartmentFields;
    
    @Value("#{'${header.business.field}'}")
    private String headerBusinessField;

    @Value("#{'${header.banks}'}")
    private String headerBanks;
    
    @Value("#{'${header.noOfEmp}'}")
    private String headerNoOfEmps;

    @Value("#{'${header.company}'}")
    private String headerCompany;
    
    @Value("#{'${header.card.type}'}")
    private String headerCardTypes;
    
    @Value("#{'${header.radius}'}")
    private String headerRadius;
    
    @Value("#{'${header.correspondence.address}'}")
    private String headerCorrespondenceAddress;
    
    @Value("#{'${header.informed.about}'}")
    private String headerInformedAbout;
    
    @Value("#{'${header.interested.products}'}")
    private String headerInterestedProds;
    

    //********* DETAIL CODES   ***********

    //******** TITLE
    @Value("#{'${title.mr.code}'}")
    private String detailTitleMr;

    @Value("#{'${title.mrs.code}'}")
    private String detailTitleMrs;

    @Value("#{'${title.miss.code}'}")
    private String detailTitleMiss;

    @Value("#{'${title.madam.code}'}")
    private String detailTitleMadam;

    //******** GENDER
    @Value("#{'${gender.male.code}'}")
    private String detailGenderMale;

    @Value("#{'${gender.female.code}'}")
    private String detailGenderFemale;

    @Value("#{'${gender.not.selected.code}'}")
    private String detailGenderNotSelected;

    //******** NATIONALITY
    @Value("#{'${nationality.indonesian.code}'}")
    private String detailNationalityIndonesian;

    @Value("#{'${nationality.filipino.code}'}")
    private String detailNationalityFilipino;

    @Value("#{'${nationality.american.code}'}")
    private String detailNationalityAmerican;

    @Value("#{'${nationality.chinese.code}'}")
    private String detailNationalityChinese;

    @Value("#{'${nationality.malaysian.code}'}")
    private String detailNationalityMalaysian;

    //******** MARITAL STATUS
    @Value("#{'${marital.status.single.code}'}")
    private String detailMaritalStatusSingle;

    @Value("#{'${marital.status.married.code}'}")
    private String detailMaritalStatusMarried;

    @Value("#{'${marital.status.divorce.code}'}")
    private String detailMaritalStatusDivorce;

    //******** PREFFERED LANGUAGE
    @Value("#{'${preferred.language.bahassa.code}'}")
    private String detailLanguageBahasa;

    @Value("#{'${preferred.language.english.code}'}")
    private String detailLanguageEnglish;

    @Value("#{'${preferred.language.chinese.code}'}")
    private String detailLanguageChinese;

    //******** ACCOUNT STATUS
    @Value("#{'${account.status.active.code}'}")
    private String detailAccountStatusActive;

    @Value("#{'${account.status.disable.code}'}")
    private String detailAccountStatusDisable;

    @Value("#{'${account.status.on.hold.code}'}")
    private String detailAccountStatusOnHold;


    //******** MEMBER TYPE
    @Value("#{'${member.type.individual.code}'}")
    private String detailMemberTypeIndividual;

    @Value("#{'${member.type.employee.code}'}")
    private String detailMemberTypeEmployee;

    @Value("#{'${member.type.company.code}'}")
    private String detailMemberTypeCompany;

    @Value("#{'${member.type.professional.code}'}")
    private String detailMemberTypeProfessional;

    //******** MEMBER TIER
    @Value("#{'${membership.tier.regular.code}'}")
    private String detailMemberTierRegular;

    @Value("#{'${membership.tier.silver.code}'}")
    private String detailMemberTierSilver;

    @Value("#{'${membership.tier.gold.code}'}")
    private String detailMemberTierGold;

    //******** EDUCATION
    @Value("#{'${education.no.certificate.code}'}")
    private String detailEducationNoCertificate;

    @Value("#{'${education.primary.school.code}'}")
    private String detailEducationPrimarySchool;

    @Value("#{'${education.secondary.school.code}'}")
    private String detailEducationSecondarySchool;

    @Value("#{'${education.tertiary.school.code}'}")
    private String detailEducationTertiarySchool;

    @Value("#{'${education.graduate.school.code}'}")
    private String detailEducationGraduateSchool;

    @Value("#{'${education.phd.code}'}")
    private String detailEducationPhd;


    //******** OCCUPATION
    @Value("#{'${occupation.jobless.code}'}")
    private String detailOccupationJobless;

    @Value("#{'${occupation.housewife.code}'}")
    private String detailOccupationHousewife;

    @Value("#{'${occupation.professional.code}'}")
    private String detailOccupationProfessional;

    @Value("#{'${occupation.student.code}'}")
    private String detailOccupationStudent;


    //******** PERSONAL INCOME
    @Value("#{'${personal.income.below.1000.code}'}")
    private String detailPersonalIncomeBelow100;

    @Value("#{'${personal.income.1001.3000.code}'}")
    private String detailPersonalIncome1001To3000;

    @Value("#{'${personal.income.3001.4000.code}'}")
    private String detailPersonalIncome3001To4000;

    @Value("#{'${personal.income.4001.above.code}'}")
    private String detailPersonalIncome4001Above;


    //******** HOUSEHOLD INCOME
    @Value("#{'${household.income.below.1000.code}'}")
    private String detailHouseholdIncomeBelow100;

    @Value("#{'${household.income.1001.3000.code}'}")
    private String detailHouseholdIncome1001To3000;

    @Value("#{'${household.income.3001.4000.code}'}")
    private String detailHouseholdIncome3001To4000;

    @Value("#{'${household.income.4001.above.code}'}")
    private String detailHouseholdIncome4001Above;


    //******** FAMILY SIZE
    @Value("#{'${family.size.1.only.code}'}")
    private String detailFamilySize1;

    @Value("#{'${family.size.2.3.members.code}'}")
    private String detailFamilySize2To3;

    @Value("#{'${family.size.3.5.members.code}'}")
    private String detailFamilySize3To5;

    @Value("#{'${family.size.6.or.more.code}'}")
    private String detailFamilySize6orMore;


    //******** CHILDREN
    @Value("#{'${children.no.children.code}'}")
    private String detailChildrenNone;

    @Value("#{'${children.1.child.code}'}")
    private String detailChildrenOne;

    @Value("#{'${children.2.3.children.code}'}")
    private String detailChildren2orMore;


    //******** PET OWNERSHIP
    @Value("#{'${pet.ownership.dog.code}'}")
    private String detailPetDog;

    @Value("#{'${pet.ownership.cat.code}'}")
    private String detailPetCat;

    @Value("#{'${pet.ownership.fish.code}'}")
    private String detailPetFish;


    //******** FOOD PREFERENCE
    @Value("#{'${food.preference.local.food.code}'}")
    private String detailFoodPrefLocal;

    @Value("#{'${food.preference.western.food.code}'}")
    private String detailFoodPrefWestern;

    @Value("#{'${food.preference.chinese.food.code}'}")
    private String detailFoodPrefChinese;


    @Value("#{'${food.preference.fast.food.code}'}")
    private String detailFoodPrefFast;


    //******** interest
    @Value("#{'${interest.reading.books.code}'}")
    private String detailInterestReading;

    @Value("#{'${interest.sports.code}'}")
    private String detailInterestSports;

    @Value("#{'${interest.outdoors.code}'}")
    private String detailInterestOutdoors;


    //******** PAYMENT TYPE
    @Value("#{'${payment.type.cash.code}'}")
    private String detailPaymentTypeCash;

    @Value("#{'${payment.type.gift.card.code}'}")
    private String detailPaymentTypeGiftCard;

    @Value("#{'${payment.type.voucher.code}'}")
    private String detailPaymentTypeVoucher;

    @Value("#{'${payment.type.loyalty.points.code}'}")
    private String detailPaymentTypeLoyaltyPoints;


    @Value("#{'${payment.type.flazz.code}'}")
    private String detailPaymentTypeFlazz;

    @Value("#{'${payment.type.coupon.code}'}")
    private String detailPaymentTypeCoupon;

    @Value("#{'${payment.type.installment.code}'}")
    private String detailPaymentTypeInstallment;

    @Value("#{'${payment.type.eft.code}'}")
    private String detailPaymentTypeEft;


    //******** POINTS ADJUSTMENT
    @Value("#{'${points.adjustment.point.not.issued.code}'}")
    private String detailPointsAdjNotIssued;

    @Value("#{'${points.adjustment.double.point.issue.code}'}")
    private String detailPointsAdjDoublePointIssued;

    @Value("#{'${points.adjustment.duplicate.point.code}'}")
    private String detailPointsAdjDuplicatePoint;

    @Value("#{'${points.adjustment.wrong.value.code}'}")
    private String detailPointsAdjWrongValue;


    //******** Age
    @Value("#{'${age.filter.below.13.years.code}'}")
    private String detailAgeBelow13;

    @Value("#{'${age.filter.14.18.years.code}'}")
    private String detailAge14To18;

    @Value("#{'${age.filter.19.30.years.code}'}")
    private String detailAge19To30;

    @Value("#{'${age.filter.31.45.years.code}'}")
    private String detailAge31To45;

    @Value("#{'${age.filter.46.and.older.code}'}")
    private String detailAge46Older;


    //****** Days
    @Value("#{'${days.monday.code}'}")
    private String detailDayMonday;

    @Value("#{'${days.tuesday.code}'}")
    private String detailDayTuesday;

    @Value("#{'${days.wednesday.code}'}")
    private String detailDayWednesday;

    @Value("#{'${days.thursday.code}'}")
    private String detailDayThursday;

    @Value("#{'${days.friday.code}'}")
    private String detailDayFriday;

    @Value("#{'${days.sunday.code}'}")
    private String detailDaySaturday;

    @Value("#{'${days.monday.code}'}")
    private String detailDaySunday;


    //****** Promotion Reward Type
    @Value("#{'${reward.type.points.code}'}")
    private String detailRewardTypePoints;

    @Value("#{'${reward.type.discount.code}'}")
    private String detailRewardTypeDiscount;

    @Value("#{'${reward.type.itempoint.code}'}")
    private String detailRewardTypeItemPoint;


    @Value("#{'${reward.type.exchangeitem.code}'}")
    private String detailRewardTypeExchangeItem;
    
    @Value("#{'${reward.type.stamp.code}'}")
    private String detailRewardTypeStamp;
    
    //****** Message Code
    @Value("#{'${message.code.member.not.found.code}'}")
    private String detailMessageCodeMemberNotFound;

    @Value("#{'${message.code.not.enough.points.code}'}")
    private String detailMessageCodeNotEnoughPoints;

    @Value("#{'${message.code.no.points.earned.code}'}")
    private String detailMessageCodeNoPointsEarned;

    @Value("#{'${message.code.no.stamps.earned.code}'}")
    private String detailMessageCodeNoStampsEarned;

    @Value("#{'${message.code.internal.error.code}'}")
    private String detailMessageCodeInternalError;


    @Value("#{'${message.code.no.payment.type.code}'}")
    private String detailMessageCodeNoPaymentType;

    @Value("#{'${message.code.payment.type.not.allowed}'}")
    private String detailMessageCodePaymentTypeNotAllowed;


    @Value("#{'${message.code.invalid.pin}'}")
    private String detailMessageCodeInvalidPin;

    @Value("#{'${message.code.invalid.parameter}'}")
    private String detailMessageCodeInvalidParameter;

    @Value("#{'${message.code.no.record.found}'}")
    private String detailMessageCodeNoRecordFound;

    @Value("#{'${message.code.supplementary.not.allowed.code}'}")
    private String detailMessageCodeSupplementaryNotAllowed;

    @Value("#{'${message.code.no.loyaltycard.code}'}")
    private String detailMessageCodeNoLoyaltyCard;

    @Value("#{'${message.code.loyaltycard.expired.code}'}")
    private String detailMessageCodeLoyaltyCardExpired;

    @Value("#{'${message.code.invalid.client.signature.code}'}")
    private String detailMessageCodeInvalidClientSignature;

    @Value("#{'${message.code.storemember.invalid.account.code}'}")
    private String detailMessageCodeInvalidStoreMemberAccount;

    @Value("#{'${message.code.storemember.invalid.company.code}'}")
    private String detailMessageCodeInvalidStoreMemberCompany;

    @Value("#{'${message.code.storemember.invalid.cardtype.code}'}")
    private String detailMessageCodeInvalidStoreMemberCardType;

    @Value("#{'${message.code.storemember.invalid.store.code}'}")
    private String detailMessageCodeInvalidStoreMemberStore;


    @Value("#{'${message.code.contactnumber.already.exist}'}")
    private String detailMessageCodeContactNumberAlreadyExist;

    @Value("#{'${message.code.invalid.card}'}")
    private String detailMessageCodeCInvalidCard;



    //****** Store
    @Value("#{'${store.headoffice}'}")
    private String detailStoreHeadOffice;

    @Value("#{'${store.exhibit}'}")
    private String detailStoreExhibit;

    @Value("#{'${role.merchantservicesupervisor.code}'}")
    private String merchantServiceSupervisor;
    @Value("#{'${role.hr.code}'}")
    private String hrCode;
    @Value("#{'${role.hrs.code}'}")
    private String hrsCode;
    @Value("#{'${role.cs.code}'}")
    private String csCode;
    @Value("#{'${role.css.code}'}")
    private String cssCode;
    @Value("#{'${role.marketinghead.code}'}")
    private String mktHeadCode;
    @Value("#{'${role.accounting.code}'}")
    private String accountingCode;
    
    //****** Operands
    @Value("#{'${operands.eq.code}'}")
    private String detailOperandEq;
    @Value("#{'${operands.ge.code}'}")
    private String detailOperandGe;
    @Value("#{'${operands.gt.code}'}")
    private String detailOperandGt;
    @Value("#{'${operands.le.code}'}")
    private String detailOperandLe;
    @Value("#{'${operands.lt.code}'}")
    private String detailOperandLt;
    
    //****** General Status Type
    @Value("#{'${status.type.active}'}")
    private String detailStatusActive;
    @Value("#{'${status.type.forapproval}'}")
    private String detailStatusApproval;
    @Value("#{'${status.type.rejected}'}")
    private String detailStatusRejected;
    @Value("#{'${status.type.expired}'}")
    private String detailStatusExpired;
    @Value("#{'${status.type.deleted}'}")
    private String detailStatusDeleted;
    @Value("#{'${status.type.draft}'}")
    private String detailStatusDraft;
    @Value("#{'${status.type.disabled}'}")
    private String detailStatusDisabled;
    @Value("#{'${status.type.cancelled}'}")
    private String detailStatusCancelled;

    //****** ComplaintStatus = NEW, ASSIGNED, REASSIGNED, INPROGRESS, RESOLVED
    @Value("#{'${status.complaint.header}'}")
    private String complaintStatHdr;
    @Value("#{'${status.type.complaint.new}'}")
    private String complaintStatNew;
    @Value("#{'${status.type.complaint.assigned}'}")
    private String complaintStatAssigned;
    @Value("#{'${status.type.complaint.reassigned}'}")
    private String complaintStatReassigned;
    @Value("#{'${status.type.complaint.inprogress}'}")
    private String complaintStatInprogress;
    @Value("#{'${status.type.complaint.resolved}'}")
    private String complaintStatResolved;

    //****** Field Types
    @Value("#{'${field.type.string.code}'}")
    private String detailFieldTypeString;
    @Value("#{'${field.type.numeric.code}'}")
    private String detailFieldTypeNumeric;
    
    //****** Manufacture Order Status
    @Value("#{'${mo.status.drafted}'}")
    private String detailMoStatusDrafted;
    @Value("#{'${mo.status.forapproval}'}")
    private String detailMoStatusForApproval;
    @Value("#{'${mo.status.approved}'}")
    private String detailMoStatusApproved;
    @Value("#{'${mo.status.barcoding}'}")
    private String detailMoStatusBarcoding;
    @Value("#{'${mo.status.receiving}'}")
    private String detailMoStatusReceiving;
    
    @Value("#{'${loyalty.inventory.status.inactive}'}")
    private String detailLoyaltyStatusInactive;
    @Value("#{'${loyalty.inventory.status.forallocation}'}")
    private String detailLoyaltyStatusForAllocation;
    @Value("#{'${loyalty.inventory.status.allocated}'}")
    private String detailLoyaltyStatusAllocated;
    @Value("#{'${loyalty.inventory.status.intransit}'}")
    private String detailLoyaltyStatusInTransit;
    @Value("#{'${loyalty.inventory.status.active}'}")
    private String detailLoyaltyStatusActive;
    @Value("#{'${loyalty.inventory.status.transferin}'}")
    private String detailLoyaltyStatusTransferIn;
    @Value("#{'${loyalty.inventory.status.disabled}'}")
    private String detailLoyaltyStatusDisabled;
    @Value("#{'${loyalty.inventory.status.missing}'}")
    private String detailLoyaltyStatusMissing;
    @Value("#{'${loyalty.inventory.status.burned}'}")
    private String detailLoyaltyStatusBurned;
    @Value("#{'${loyalty.inventory.status.found}'}")
    private String detailLoyaltyStatusFound;
    
    //****** Inventory Location
    @Value("#{'${inventory.location.headoffice}'}")
    private String detailInvLocationHeadOffice;
    @Value("#{'${inventory.location.lebakbulus}'}")
    private String detailInvLocationLebakBulus;
    @Value("#{'${inventory.location.denpasar}'}")
    private String detailInvLocationDenpasar;
    @Value("#{'${inventory.location.exhibit}'}")
    private String detailInvLocationExhibit;

    //****** Product Classification
    @Value("#{'${product.classification.division}'}")
    private String productCfnDivision;
    @Value("#{'${product.classification.department}'}")
    private String productCfnDepartment;
    @Value("#{'${product.classification.group}'}")
    private String productCfnGroup;
    @Value("#{'${product.classification.family}'}")
    private String productCfnFamily;
    @Value("#{'${product.classification.subfamily}'}")
    private String productCfnSubfamily;

    //******* Member Addl Lookup
    @Value("#{'${hdr.besttime.tocall}'}")
    private String bestTimeToCall;
    @Value("#{'${hdr.social.media}'}")
    private String socialMedia;
    @Value("#{'${hdr.visited.supermarkets}'}")
    private String visitedSupermarkets;
    @Value("#{'${hdr.interested.activities}'}")
    private String interestedActivities;
    @Value("#{'${hdr.vehicles.owned}'}")
    private String vehiclesOwned;

    //******* GC Customer Profile: Customer/Discount Types
    @Value("#{'${hdr.gc.cxprof.discounttype}'}")
    private String gcDiscountType;
    
    //******* GC Burn Reason
    @Value("#{'${gc.burn.reasons}'}")
    private String burnReasons;
    @Value("#{'${gc.return.reasons}'}")
    private String returnReasons;

    @Value("#{'${gc.burn.type}'}")
    private String burnType;
    
    //*****Product Profile Face Value
    @Value("#{'${product.profile.facevalue}'}")
    private String productProfileFaceValue;
    
  //******* GC SO Payment Method
    @Value("#{'${hdr.gcso.paymentmethod}'}")
    private String headerGcSoPaymentMethod;
    @Value("#{'${hdr.gcso.paymentmethod.cash}'}")
    private String detailGcSoPaymentMethodCash;
    @Value("#{'${hdr.gcso.paymentmethod.transfer}'}")
    private String detailGcSoPaymentMethodTransfer;
    
    //******* GC SO Category
    @Value("#{'${hdr.gcso.category}'}")
    private String headerGcSoCategory;
    //******* GC SO Category
    @Value("#{'${hdr.gcso.dept}'}")
    private String headerGcSoDepartment;
    
    //******* POS Lose Transaction Status
    @Value("#{'${header.poslose.status}'}")
    private String headerPosLoseStatus;
    @Value("#{'${poslose.status.new}'}")
    private String detailPosLoseStatusNew;
    @Value("#{'${poslose.status.accounted}'}")
    private String detailPosLoseStatusAccounted;
    @Value("#{'${poslose.status.approved}'}")
    private String detailPosLoseStatusApproved;
    @Value("#{'${poslose.status.rejected}'}")
    private String detailPosLoseStatusRejected;

    //******* Business Unit
    @Value("#{'${header.business.unit}'}")
    private String headerBusinessUnit;
    //******* Business Region
    @Value("#{'${header.business.region}'}")
    private String headerBusinessRegion;
    //******* Business Territory
    @Value("#{'${header.business.territory}'}")
    private String headerBusinessTerritory;
    @Value("#{'${gc.bu.c4.default}'}")
    private String dtlGcBuC4Default;
    
    @Value("#{'${b2b.exhibition.paymenttypes}'}")
    private String hdrExhibitionPaymentTypes; 
    

    public String getHeaderTitle() {
        return headerTitle;
    }

    public String getHeaderGender() {
        return headerGender;
    }

    public String getHeaderNationality() {
        return headerNationality;
    }

    public String getHeaderMaritalStatus() {
        return headerMaritalStatus;
    }

    public String getHeaderPreferredLanguage() {
        return headerPreferredLanguage;
    }

    public String getHeaderAccountStatus() {
        return headerAccountStatus;
    }

    public String getHeaderMemberType() {
        return headerMemberType;
    }

    public String getHeaderMembershipTier() {
        return headerMembershipTier;
    }

    public String getHeaderEducation() {
        return headerEducation;
    }

    public String getHeaderOccupation() {
        return headerOccupation;
    }

    public String getHeaderPersonalIncome() {
        return headerPersonalIncome;
    }

    public String getHeaderHouseHoldIncome() {
        return headerHouseHoldIncome;
    }

    public String getHeaderFamilySize() {
        return headerFamilySize;
    }

    public String getHeaderChildren() {
        return headerChildren;
    }

    public String getHeaderPetOwnership() {
        return headerPetOwnership;
    }

    public String getHeaderFoodPreference() {
        return headerFoodPreference;
    }

    public String getHeaderInterest() {
        return headerInterest;
    }

    public String getHeaderPaymentType() {
        return headerPaymentType;
    }

    public String getHeaderPointsAdjustment() {
        return headerPointsAdjustment;
    }

    public String getHeaderAgeFilter() {
        return headerAgeFilter;
    }

    public String getHeaderDays() {
        return headerDays;
    }

    public String getHeaderRewardType() {
        return headerRewardType;
    }

    public String getHeaderInventoryLocation() {
        return headerInventoryLocation;
    }

    public String getHeaderMessageCode() {
        return headerMessageCode;
    }

    public String getHeaderStatus() {
		return headerStatus;
	}

	public String getHeaderEmployeeApprovedPaymentMedia() {
        return headerEmployeeApprovedPaymentMedia;
    }

    public String getHeaderEmployeeApprovedCardFilter() {
        return headerEmployeeApprovedCardFilter;
    }
    
    public String getHeaderProductGrpClassification() {
		return headerProductGrpClassification;
	}

	public String getHeaderProductGrpSubclassification() {
		return headerProductGrpSubclassification;
	}

	public String getHeaderProductClassification() {
		return headerProductClassification;
	}
	
	public String getHeaderCustomerGroup() {
		return headerCustomerGroup;
	}
	
	public String getHeaderCustomerSegmentation() {
		return headerCustomerSegmentation;
	}
	
	public String getHeaderVendors() {
		return headerVendors;
	}

	public String getHeaderEmpType() {
		return empType;
	}
	
	public String getHeaderReligion() {
		return headerReligion;
	}
    
    public String getHeaderBusinessFields() {
    	return headerBusinessField;
    }

	public String getDetailTitleMr() {
        return detailTitleMr;
    }

    public String getDetailTitleMrs() {
        return detailTitleMrs;
    }

    public String getDetailTitleMiss() {
        return detailTitleMiss;
    }

    public String getDetailTitleMadam() {
        return detailTitleMadam;
    }

    public String getDetailGenderMale() {
        return detailGenderMale;
    }

    public String getDetailGenderFemale() {
        return detailGenderFemale;
    }

    public String getDetailGenderNotSelected() {
        return detailGenderNotSelected;
    }

    public String getDetailNationalityIndonesian() {
        return detailNationalityIndonesian;
    }

    public String getDetailNationalityFilipino() {
        return detailNationalityFilipino;
    }

    public String getDetailNationalityAmerican() {
        return detailNationalityAmerican;
    }

    public String getDetailNationalityChinese() {
        return detailNationalityChinese;
    }

    public String getDetailNationalityMalaysian() {
        return detailNationalityMalaysian;
    }

    public String getDetailMaritalStatusSingle() {
        return detailMaritalStatusSingle;
    }

    public String getDetailMaritalStatusMarried() {
        return detailMaritalStatusMarried;
    }

    public String getDetailMaritalStatusDivorce() {
        return detailMaritalStatusDivorce;
    }

    public String getDetailLanguageBahasa() {
        return detailLanguageBahasa;
    }

    public String getDetailLanguageEnglish() {
        return detailLanguageEnglish;
    }

    public String getDetailLanguageChinese() {
        return detailLanguageChinese;
    }

    public String getDetailAccountStatusActive() {
        return detailAccountStatusActive;
    }

    public String getDetailAccountStatusDisable() {
        return detailAccountStatusDisable;
    }

    public String getDetailAccountStatusOnHold() {
        return detailAccountStatusOnHold;
    }

    public String getDetailMemberTypeIndividual() {
        return detailMemberTypeIndividual;
    }

    public String getDetailMemberTypeEmployee() {
        return detailMemberTypeEmployee;
    }

    public String getDetailMemberTypeCompany() {
        return detailMemberTypeCompany;
    }

    public String getDetailMemberTypeProfessional() {
        return detailMemberTypeProfessional;
    }

    public String getDetailMemberTierRegular() {
        return detailMemberTierRegular;
    }

    public String getDetailMemberTierSilver() {
        return detailMemberTierSilver;
    }

    public String getDetailMemberTierGold() {
        return detailMemberTierGold;
    }

    public String getDetailEducationNoCertificate() {
        return detailEducationNoCertificate;
    }

    public String getDetailEducationPrimarySchool() {
        return detailEducationPrimarySchool;
    }

    public String getDetailEducationSecondarySchool() {
        return detailEducationSecondarySchool;
    }

    public String getDetailEducationTertiarySchool() {
        return detailEducationTertiarySchool;
    }

    public String getDetailEducationGraduateSchool() {
        return detailEducationGraduateSchool;
    }

    public String getDetailEducationPhd() {
        return detailEducationPhd;
    }

    public String getDetailOccupationJobless() {
        return detailOccupationJobless;
    }

    public String getDetailOccupationHousewife() {
        return detailOccupationHousewife;
    }

    public String getDetailOccupationProfessional() {
        return detailOccupationProfessional;
    }

    public String getDetailOccupationStudent() {
        return detailOccupationStudent;
    }

    public String getDetailPersonalIncomeBelow100() {
        return detailPersonalIncomeBelow100;
    }

    public String getDetailPersonalIncome1001To3000() {
        return detailPersonalIncome1001To3000;
    }

    public String getDetailPersonalIncome3001To4000() {
        return detailPersonalIncome3001To4000;
    }

    public String getDetailPersonalIncome4001Above() {
        return detailPersonalIncome4001Above;
    }

    public String getDetailHouseholdIncomeBelow100() {
        return detailHouseholdIncomeBelow100;
    }

    public String getDetailHouseholdIncome1001To3000() {
        return detailHouseholdIncome1001To3000;
    }

    public String getDetailHouseholdIncome3001To4000() {
        return detailHouseholdIncome3001To4000;
    }

    public String getDetailHouseholdIncome4001Above() {
        return detailHouseholdIncome4001Above;
    }

    public String getDetailFamilySize1() {
        return detailFamilySize1;
    }

    public String getDetailFamilySize2To3() {
        return detailFamilySize2To3;
    }

    public String getDetailFamilySize3To5() {
        return detailFamilySize3To5;
    }

    public String getDetailFamilySize6orMore() {
        return detailFamilySize6orMore;
    }

    public String getDetailChildrenNone() {
        return detailChildrenNone;
    }

    public String getDetailChildrenOne() {
        return detailChildrenOne;
    }

    public String getDetailChildren2orMore() {
        return detailChildren2orMore;
    }

    public String getDetailPetDog() {
        return detailPetDog;
    }

    public String getDetailPetCat() {
        return detailPetCat;
    }

    public String getDetailPetFish() {
        return detailPetFish;
    }

    public String getDetailFoodPrefLocal() {
        return detailFoodPrefLocal;
    }

    public String getDetailFoodPrefWestern() {
        return detailFoodPrefWestern;
    }

    public String getDetailFoodPrefChinese() {
        return detailFoodPrefChinese;
    }

    public String getDetailFoodPrefFast() {
        return detailFoodPrefFast;
    }

    public String getDetailInterestReading() {
        return detailInterestReading;
    }

    public String getDetailInterestSports() {
        return detailInterestSports;
    }

    public String getDetailInterestOutdoors() {
        return detailInterestOutdoors;
    }


    public String getMerchantServiceSupervisor() {
        return merchantServiceSupervisor;
    }
    @Override
    public String getAccountingCode() {
        return accountingCode;
    }

    public String getDetailPointsAdjNotIssued() {
        return detailPointsAdjNotIssued;
    }

    public String getDetailPointsAdjDoublePointIssued() {
        return detailPointsAdjDoublePointIssued;
    }

    public String getDetailPointsAdjDuplicatePoint() {
        return detailPointsAdjDuplicatePoint;
    }

    public String getDetailPointsAdjWrongValue() {
        return detailPointsAdjWrongValue;
    }

    public String getDetailAgeBelow13() {
        return detailAgeBelow13;
    }

    public String getDetailAge14To18() {
        return detailAge14To18;
    }

    public String getDetailAge19To30() {
        return detailAge19To30;
    }

    public String getDetailAge31To45() {
        return detailAge31To45;
    }

    public String getDetailAge46Older() {
        return detailAge46Older;
    }

    public String getDetailDayMonday() {
        return detailDayMonday;
    }

    public String getDetailDayTuesday() {
        return detailDayTuesday;
    }

    public String getDetailDayWednesday() {
        return detailDayWednesday;
    }

    public String getDetailDayThursday() {
        return detailDayThursday;
    }

    public String getDetailDayFriday() {
        return detailDayFriday;
    }

    public String getDetailDaySaturday() {
        return detailDaySaturday;
    }

    public String getDetailDaySunday() {
        return detailDaySunday;
    }

    public String getDetailRewardTypePoints() {
        return detailRewardTypePoints;
    }

    public String getDetailRewardTypeDiscount() {
        return detailRewardTypeDiscount;
    }

    public String getDetailRewardTypeItemPoint() {
        return detailRewardTypeItemPoint;
    }

    public String getDetailRewardTypeExchangeItem() {
        return detailRewardTypeExchangeItem;
    }
    @Override
    public String getDetailRewardTypeStamp() {
        return detailRewardTypeStamp;
    }

    @Override
    public String getDetailMessageCodeNoStampsEarned() {
        return detailMessageCodeNoStampsEarned;
    }

    public String getDetailMessageCodeMemberNotFound() {
        return detailMessageCodeMemberNotFound;
    }

    public String getDetailMessageCodeNotEnoughPoints() {
        return detailMessageCodeNotEnoughPoints;
    }

    public String getDetailMessageCodeNoPointsEarned() {
        return detailMessageCodeNoPointsEarned;
    }

    public String getDetailMessageCodeInternalError() {
        return detailMessageCodeInternalError;
    }

    public String getDetailMessageCodeNoPaymentType() {
        return detailMessageCodeNoPaymentType;
    }

    public String getDetailMessageCodePaymentTypeNotAllowed() {
        return detailMessageCodePaymentTypeNotAllowed;
    }

    public String getDetailMessageCodeInvalidPin() {
        return detailMessageCodeInvalidPin;
    }

    public String getDetailMessageCodeInvalidParameter() {
        return detailMessageCodeInvalidParameter;
    }

    public String getDetailMessageCodeNoRecordFound() {
        return detailMessageCodeNoRecordFound;
    }

    public String getDetailMessageCodeSupplementaryNotAllowed() {
        return detailMessageCodeSupplementaryNotAllowed;
    }

    public String getDetailMessageCodeNoLoyaltyCard() {
        return detailMessageCodeNoLoyaltyCard;
    }

    public String getDetailMessageCodeLoyaltyCardExpired() {
        return detailMessageCodeLoyaltyCardExpired;
    }

    public String getDetailMessageCodeInvalidClientSignature() {
        return detailMessageCodeInvalidClientSignature;
    }

    public String getDetailMessageCodeInvalidStoreMemberAccount() {
        return detailMessageCodeInvalidStoreMemberAccount;
    }

    public String getDetailMessageCodeInvalidStoreMemberCompany() {
        return detailMessageCodeInvalidStoreMemberCompany;
    }

    public String getDetailMessageCodeInvalidStoreMemberCardType() {
        return detailMessageCodeInvalidStoreMemberCardType;
    }

    public String getDetailMessageCodeInvalidStoreMemberStore() {
        return detailMessageCodeInvalidStoreMemberStore;
    }

    public String getDetailMessageCodeContactNumberAlreadyExist() {
        return detailMessageCodeContactNumberAlreadyExist;
    }

    public String getDetailMessageCodeInvalidCard() {
        return detailMessageCodeCInvalidCard;
    }

    public String getDetailStoreHeadOffice() {
        return detailStoreHeadOffice;
    }

    public String getDetailStoreExhibit() {
        return detailStoreExhibit;
    }

    public String getMerchantServiceSupervisorCode() {
        return merchantServiceSupervisor;
    }

	public String getHrCode() {
		return hrCode;
	}

	public String getHrsCode() {
		return hrsCode;
	}

	public String getCsCode() {
		return csCode;
	}

	public String getCssCode() {
		return cssCode;
	}

	public String getMktHeadCode() {
		return mktHeadCode;
	}

	public String getHeaderOperands() {
		return headerOperands;
	}

	public String getHeaderFieldTypes() {
		return headerFieldTypes;
	}

	public String getDetailOperandEq() {
		return detailOperandEq;
	}

	public String getDetailOperandGe() {
		return detailOperandGe;
	}

	public String getDetailOperandGt() {
		return detailOperandGt;
	}

	public String getDetailOperandLe() {
		return detailOperandLe;
	}

	public String getDetailOperandLt() {
		return detailOperandLt;
	}

	public String getDetailStatusActive() {
		return detailStatusActive;
	}

	public String getDetailStatusApproval() {
		return detailStatusApproval;
	}

	public String getDetailStatusRejected() {
		return detailStatusRejected;
	}

	public String getDetailStatusExpired() {
		return detailStatusExpired;
	}

	public String getDetailStatusDeleted() {
		return detailStatusDeleted;
	}

	public String getDetailStatusDraft() {
		return detailStatusDraft;
	}

	public String getDetailStatusDisabled() {
		return detailStatusDisabled;
	}

	@Override
	public String getDetailStatusCancelled() {
		return detailStatusCancelled;
	}

	public String getDetailFieldTypeString() {
		return detailFieldTypeString;
	}

	public String getDetailFieldTypeNumeric() {
		return detailFieldTypeNumeric;
	}


    public String getDetailPaymentTypeCash() {
        return detailPaymentTypeCash;
    }

    public String getDetailPaymentTypeGiftCard() {
        return detailPaymentTypeGiftCard;
    }

    public String getDetailPaymentTypeVoucher() {
        return detailPaymentTypeVoucher;
    }

    public String getDetailPaymentTypeLoyaltyPoints() {
        return detailPaymentTypeLoyaltyPoints;
    }

    public String getDetailPaymentTypeFlazz() {
        return detailPaymentTypeFlazz;
    }

    public String getDetailPaymentTypeCoupon() {
        return detailPaymentTypeCoupon;
    }

    public String getDetailPaymentTypeInstallment() {
        return detailPaymentTypeInstallment;
    }

    public String getDetailPaymentTypeEft() {
        return detailPaymentTypeEft;
    }

	public String getHeaderMoStatus() {
		return headerMoStatus;
	}

	public String getDetailMoStatusDrafted() {
		return detailMoStatusDrafted;
	}

	public String getDetailMoStatusForApproval() {
		return detailMoStatusForApproval;
	}

	public String getDetailMoStatusApproved() {
		return detailMoStatusApproved;
	}
	@Override
	public String getDetailMoStatusBarcoding() {
		return detailMoStatusBarcoding;
	}

	@Override
	public String getDetailMoStatusReceiving() {
		return detailMoStatusReceiving;
	}

	public String getDetailInvLocationHeadOffice() {
		return detailInvLocationHeadOffice;
	}

	public String getDetailInvLocationLebakBulus() {
		return detailInvLocationLebakBulus;
	}

	public String getDetailInvLocationDenpasar() {
		return detailInvLocationDenpasar;
	}

	public String getDetailInvLocationExhibit() {
		return detailInvLocationExhibit;
	}

	public String getHeaderLoyaltyStatus() {
		return headerLoyaltyStatus;
	}

	public String getDetailLoyaltyStatusInactive() {
		return detailLoyaltyStatusInactive;
	}

	public String getDetailLoyaltyStatusForAllocation() {
		return detailLoyaltyStatusForAllocation;
	}

	public String getDetailLoyaltyStatusAllocated() {
		return detailLoyaltyStatusAllocated;
	}

	public String getDetailLoyaltyStatusInTransit() {
		return detailLoyaltyStatusInTransit;
	}

	public String getDetailLoyaltyStatusActive() {
		return detailLoyaltyStatusActive;
	}

	public String getDetailLoyaltyStatusTransferIn() {
		return detailLoyaltyStatusTransferIn;
	}

	public String getDetailLoyaltyStatusDisabled() {
		return detailLoyaltyStatusDisabled;
	}
	
	public String getDetailLoyaltyStatusFound() {
		return detailLoyaltyStatusFound;
	}

	public String getHeaderBanks() {
		return headerBanks;
	}
	
	public String getHeaderNoOfEmps() {
		return headerNoOfEmps;
	}

    @Override
    public String getHeaderCompany() {
        return headerCompany;
    }

    public String getHeaderCardTypes() {
        return headerCardTypes;
    }
    
    public String getDetailLoyaltyStatusMissing() {
		return detailLoyaltyStatusMissing;
	}

	public String getDetailLoyaltyStatusBurned() {
		return detailLoyaltyStatusBurned;
	}

	public String getDetailProductCfnDivision() {
		return productCfnDivision;
	}

	public String getDetailProductCfnDepartment() {
		return productCfnDepartment;
	}

	public String getDetailProductCfnGroup() {
		return productCfnGroup;
	}

	public String getDetailProductCfnFamily() {
		return productCfnFamily;
	}

	public String getDetailProductCfnSubfamily() {
		return productCfnSubfamily;
	}

	public String getHeaderPaymentTypeWildcard() {
		return headerPaymentTypeWildcard;
	}

	public String getPermissionPromoBannerManagement() {
		return permissionPromoBannerMgmt;
	}

	public String getPermissionPromotionsManagement() {
		return permissionPromotionsMgmt;
	}

	public String getPermissionLoyaltyCardManufactureOrder() {
		return permissionLoyaltyCardManufactureOrders;
	}

	public String getPermissionLoyaltyCardInventory() {
		return permissionLoyaltyCardInventory;
	}

	public String getPermissionEmployeeRewardsApproval() {
		return permissionEmployeeRewardsApproval;
	}

	@Override
	public String getPermissionMemberPointsAdjustment() {
		return permissionMemberPointsAdjustment;
	}

	@Override
	public String getPermissionEmployeePointsAdjustment() {
		return permissionEmployeePointsAdjustment;
	}

	@Override
	public String getPermissionEmployeePurchaseAdjustment() {
		return permissionEmployeePurchaseAdjustment;
	}

	@Override
	public String getPermissionMediaMgmt() {
		return permissionMediaMgmt;
	}

	@Override
	public String getPermissionComplaintsMgmt() {
		return permissionComplaintsMgmt;
	}

	@Override
	public String getComplaintStatHdr() {
		return complaintStatHdr;
	}

	@Override
	public String getComplaintStatNew() {
		return complaintStatNew;
	}

	@Override
	public String getComplaintStatAssigned() {
		return complaintStatAssigned;
	}

	@Override
	public String getComplaintStatReassigned() {
		return complaintStatReassigned;
	}

	@Override
	public String getComplaintStatInprogress() {
		return complaintStatInprogress;
	}

	@Override
	public String getComplaintStatResolved() {
		return complaintStatResolved;
	}

    @Override
    public String getHeaderDepartmentFields() {
    	return headerDepartmentFields;
    }

    @Override
	public String getBestTimeToCall() {
		return bestTimeToCall;
	}

    @Override
	public String getSocialMedia() {
		return socialMedia;
	}

    @Override
	public String getVisitedSupermarkets() {
		return visitedSupermarkets;
	}

    @Override
	public String getInterestedActivities() {
		return interestedActivities;
	}

    @Override
	public String getVehiclesOwned() {
		return vehiclesOwned;
	}

    @Override
	public String getGcDiscountType() {
		return gcDiscountType;
	}

    @Override
	public String getHeaderRadius() {
		return headerRadius;
	}
    @Override
	public String getHeaderCorrespondenceAddress() {
		return headerCorrespondenceAddress;
	}
    @Override
	public String getHeaderInformedAbout() {
		return headerInformedAbout;
	}
    @Override
	public String getHeaderInterestedProds() {
		return headerInterestedProds;
	}
    
    public String getHeaderPosLoseStatus() {
    	return headerPosLoseStatus;
    }
    
    public String getDetailPosLoseStatusNew() {
    	return detailPosLoseStatusNew;
    }
    
    public String getDetailPosLoseStatusAccounted() {
    	return detailPosLoseStatusAccounted;
    }
    
    public String getDetailPosLoseStatusApproved() {
    	return detailPosLoseStatusApproved;
    }
    
    public String getDetailPosLoseStatusRejected() {
    	return detailPosLoseStatusRejected;
    }

    public String getBurnReasons() {
        return burnReasons;
    }

    public String getBurnType() {
        return burnType;
    }

    @Override
    public String getReturnReasons() {
    	return returnReasons;
    }
    @Override
    public String getProductProfileFaceValue() {
    	return productProfileFaceValue;
    }

    public void setBurnReasons(String burnReasons) {
        this.burnReasons = burnReasons;
    }
    
    @Override
	public String getHeaderGcSoCategory() {
		return headerGcSoCategory;
	}
    
    @Override
	public String getHeaderGcSoDepartment() {
		return headerGcSoDepartment;
	}

    @Override
	public String getHeaderGcSoPaymentMethod() {
		return headerGcSoPaymentMethod;
	}
    
    @Override
	public String getDetailGcSoPaymentMethodCash() {
		return detailGcSoPaymentMethodCash;
	}
    
    @Override
	public String getDetailGcSoPaymentMethodTransfer() {
		return detailGcSoPaymentMethodTransfer;
	}

    @Override
    public String getHeaderBusinessUnit() {
    	return headerBusinessUnit;
    }

    @Override
    public String getHeaderBusinessRegion() {
    	return headerBusinessRegion;
    }

    @Override
    public String getHeaderBusinessTerritory() {
    	return headerBusinessTerritory;
    }

    @Override
    public String getDtlGcBuC4Default() {
    	return dtlGcBuC4Default;
    }

    @Override
    public String getHdrExhibitionPaymentTypes() {
    	return hdrExhibitionPaymentTypes;
    }

}
