package com.transretail.crm.core.repo.custom.impl;

import java.util.Date;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.joda.time.LocalDate;

import com.mysema.query.jpa.impl.JPAQuery;
import com.mysema.query.types.ConstructorExpression;
import com.transretail.crm.core.dto.MemberDto;
import com.transretail.crm.core.entity.QEmployeePurchaseTxnModel;
import com.transretail.crm.core.entity.QPointsTxnModel;
import com.transretail.crm.core.entity.enums.PointTxnType;
import com.transretail.crm.core.entity.enums.VoucherTransactionType;
import com.transretail.crm.core.repo.custom.MemberMonitorConfigRepoCustom;

public class MemberMonitorConfigRepoImpl implements MemberMonitorConfigRepoCustom{
	@PersistenceContext
	EntityManager em;
	
	@Override
	public List<MemberDto> getMonitoredMembers(Integer transactionCount, LocalDate date) {
//		QMemberModel members  = QMemberModel.memberModel;
		QPointsTxnModel points = QPointsTxnModel.pointsTxnModel;
		JPAQuery query = new JPAQuery(em);
		
		LocalDate today = date == null ? LocalDate.now() : date;
		Date todayDate = today.toDateTimeAtStartOfDay().toDate();
		Date tomorrowDate = today.plusDays(1).toDateTimeAtStartOfDay().toDate();
		
		return query.from(points)
				.where(points.transactionDateTime.between(todayDate, tomorrowDate)
					.and(points.transactionType.eq(PointTxnType.EARN))
					.and(points.transactionContributer.memberType.code.ne("MTYP002")))
				.groupBy(points.transactionContributer)
				.having(points.transactionContributer.count().goe(transactionCount))
				.list(ConstructorExpression.create(MemberDto.class, points.transactionContributer, points.transactionContributer.count()));
	}
	
	@Override
	public List<MemberDto> getMonitoredEmployees(Integer transactionCount, LocalDate date) {
		QEmployeePurchaseTxnModel purchases = QEmployeePurchaseTxnModel.employeePurchaseTxnModel;
		JPAQuery query = new JPAQuery(em);
		
		LocalDate today = date == null ? LocalDate.now() : date;
		Date todayDate = today.toDateTimeAtStartOfDay().toDate();
		Date tomorrowDate = today.plusDays(1).toDateTimeAtStartOfDay().toDate();
		
		return query.from(purchases)
				.where(purchases.transactionDateTime.between(todayDate, tomorrowDate)
					.and(purchases.transactionType.eq(VoucherTransactionType.PURCHASE))
					.and(purchases.memberModel.memberType.code.eq("MTYP002")))
				.groupBy(purchases.memberModel)
				.having(purchases.memberModel.count().goe(transactionCount))
				.list(ConstructorExpression.create(MemberDto.class, purchases.memberModel, purchases.memberModel.count()));
	}
}
