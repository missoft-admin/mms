package com.transretail.crm.giftcard.dto;

import com.mysema.query.types.expr.BooleanExpression;
import com.transretail.crm.common.service.dto.request.AbstractSearchFormDto;

/**
 * description here.
 *
 * @author Vincent Gerard Tan
 */
public class PrepaidTransactionSearchDto extends AbstractSearchFormDto {
    private String cardNo;

    private int duration;

    public int getDuration() {
        return duration;
    }

    public void setDuration(int duration) {
        this.duration = duration;
    }

    public String getCardNo() {
        return cardNo;
    }

    public void setCardNo(String cardNo) {
        this.cardNo = cardNo;
    }

    @Override
    public BooleanExpression createSearchExpression() {
        return null;
    }
}
