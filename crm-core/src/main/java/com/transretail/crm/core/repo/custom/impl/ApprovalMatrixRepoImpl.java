package com.transretail.crm.core.repo.custom.impl;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.util.StringUtils;

import com.mysema.query.BooleanBuilder;
import com.mysema.query.jpa.impl.JPAQuery;
import com.mysema.query.types.expr.BooleanExpression;
import com.mysema.query.types.path.EnumPath;
import com.mysema.query.types.path.StringPath;
import com.transretail.crm.core.entity.ApprovalMatrixModel;
import com.transretail.crm.core.entity.QApprovalMatrixModel;
import com.transretail.crm.core.entity.UserModel;
import com.transretail.crm.core.entity.enums.ModelType;
import com.transretail.crm.core.repo.custom.ApprovalMatrixRepoCustom;
import com.transretail.crm.core.util.BooleanExprUtil;


public class ApprovalMatrixRepoImpl implements ApprovalMatrixRepoCustom {

    @PersistenceContext
    EntityManager em;


	@Override
	public Double findHighestAmountByUser( ModelType inModelType, UserModel inUser ) {

		JPAQuery theQuery = new JPAQuery(em);
		QApprovalMatrixModel theMatrix = QApprovalMatrixModel.approvalMatrixModel;

		return theQuery.from( theMatrix ).
				where( new BooleanBuilder().
						orAllOf( 
								theMatrix.user.username.in( inUser.getUsername() ),
								theMatrix.modelType.eq( inModelType ) ) ).
				singleResult( theMatrix.amount.max() );
	}


	@Override
	public Long findHighestPointsAmountByUser( UserModel inUser ) {

		Double theHighestPointsAmount = findHighestAmountByUser( ModelType.POINTS, inUser );

		if ( theHighestPointsAmount != null ) {
			return (long) theHighestPointsAmount.doubleValue();
		}

		return null;
	}
	
	@Override
	public List<ApprovalMatrixModel> findByUsername( String username ) {

		JPAQuery theQuery = new JPAQuery(em);
		QApprovalMatrixModel theMatrix = QApprovalMatrixModel.approvalMatrixModel;

		return theQuery.from( theMatrix ).
				where( theMatrix.user.username.in( username )).
				list( theMatrix );
	}
	
	@Override
	public List<UserModel> findUsers( ModelType inModelType ) {

		JPAQuery theQuery = new JPAQuery(em);
		QApprovalMatrixModel theMatrix = QApprovalMatrixModel.approvalMatrixModel;

		return theQuery.from( theMatrix ).
				where( new BooleanBuilder().and( BooleanExprUtil.INSTANCE.isEnumPropertyIn( theMatrix.modelType, inModelType ) ) ).
				list( theMatrix.user );
	}



	@SuppressWarnings("unused")
	private BooleanExpression isStringPropertyLikeUsingWildcard(StringPath inStrPath, String inProp, String wildcard) {
		if(!StringUtils.isEmpty(inProp))
			inProp = inProp.replaceAll(wildcard, "%");
        return !StringUtils.isEmpty(inProp) ? inStrPath.lower().like(inProp.toLowerCase()) : null;
    }

    @SuppressWarnings("unused")
	private BooleanExpression isStringPropertyLike(StringPath inStrPath, String inProp) {
        return !StringUtils.isEmpty(inProp) ? inStrPath.lower().contains(inProp.toLowerCase()) : null;
    }
    
    @SuppressWarnings("unused")
	private BooleanExpression isStringPropertyEquals(StringPath inStrPath, String inProp) {
        return !StringUtils.isEmpty(inProp) ? inStrPath.lower().eq(inProp.toLowerCase()) : null;
    }

	@SuppressWarnings({ "rawtypes", "unused" })
	private BooleanExpression isEnumPropertyIn(EnumPath inEnumPath, Enum inProp) {
		return isEnumPropertyNotIn( inEnumPath, inProp, false );
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	private BooleanExpression isEnumPropertyNotIn(EnumPath inEnumPath, Enum inProp, boolean inIsPropNotIn) {
		return inProp != null ? ( inIsPropNotIn? inEnumPath.notIn(inProp) : inEnumPath.in(inProp) ) : null;
	}

}
