ALTER TABLE CRM_MO_TIER ADD COMPANY VARCHAR2(255 CHAR);

alter table CRM_MO_TIER 
    add constraint FK_ownbrl6utr8sphqc2911jy5bp 
    foreign key (COMPANY) 
    references CRM_REF_LOOKUP_DTL;