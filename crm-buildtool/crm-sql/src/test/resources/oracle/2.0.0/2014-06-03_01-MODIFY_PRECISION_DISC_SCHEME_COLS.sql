alter table CRM_GC_DISCOUNT_SCHEME modify MIN_PURCHASE number(19,5);
alter table CRM_GC_DISCOUNT_SCHEME modify PURCHASE_FOR_MAX_DISC number(19,5);
