<%@include file="../../../common/taglibs.jsp"%>


<div id="content_soCancelForm" class="modal hide nofly modal-dialog content_soForm">

  <div class="modal-content">

    <div class="modal-header">
      <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
      <h4><spring:message code="gc.so.no" />: ${salesOrder.orderNo}</h4>
    </div>

    <div class="modal-body modal-form form-horizontal">
      <div class="hide alert alert-error" id="content_error"><button type="button" class="close" onclick="$(this).parent().hide();">&times;</button><div></div></div>

      <div class="main-block row-fluid"><div class="span12"><%@include file="sgmntSoInfo.jsp" %></div></div>
      <div class="main-block row-fluid"><div class="span12"><%@include file="tblSoItemAllocView.jsp" %></div></div>


      <div class="main-block row-fluid  cancel-so-order">
        <div class="control-group">
          <label class="control-label"><c:if test="${null == cancelRemarks}"><b class="required">*</b> </c:if>
            <spring:message code="label_cancel_remarks" />
          </label>
          <div class="controls">
            <c:if test="${null != cancelRemarks}">${cancelRemarks.remarks}</c:if>
            <c:if test="${null == cancelRemarks}">
              <textarea rows="" cols="" id="remarks" data-err-reqd="<spring:message code="gc.so.err.cancelremarks.reqd" />"></textarea>
            </c:if>
          </div>
        </div>
      </div>
    </div>


    <c:url var="url_action" value="/" />
    <form:form id="soCancelForm" name="soCancel" modelAttribute="soCancelForm" method="POST" action="${url_action}" class="modal-form form-horizontal">

    <div class="modal-footer">
      <c:if test="${null == cancelRemarks}">
      <input id="cancelOrderBtn" class="btn btn-primary" type="button" value="<spring:message code="label_cancel_order" />"
        data-id="${salesOrder.id}" data-url="<c:url value="/giftcard/salesorder/cancel/%ID%/%REMARKS%" />" />
      </c:if>
      <input id="cancelBtn" class="btn" type="button" value="<spring:message code="label_cancel" />" data-dismiss="modal" />
    </div>
    </form:form>

  </div>


<style type="text/css"> 
<!-- 
  .content_soForm .alert { padding-left: 0px !important; }
  .content_soForm .soorderalloc-block { margin-top: 20px; }
  .content_soForm .checkElement { float: left; margin-right: 5px; }
  .content_soForm .checkElements .controls { margin-left: 150px !important; }
  .content_soForm .checkElements .control-label { width: 150px !important; }
  .content_soForm .pull-center { padding-left: 10px; padding-right: 10px; }
  .content_soForm .modal-footer { text-align: center; } 
  .content_soForm .control-group .controls { margin-top: 5px; margin-left: 110px; }
  .content_soForm .control-group .control-label { width: 100px; text-align: left; }
  .content_soForm .table-col-xlarge { width: 320px; }
--> 
</style>
<script type='text/javascript'>
var SoCancelForm = null;

$(document).ready( function() {

    var errorContainer = $( "#content_soCancelForm" ).find( "#content_error" );
    var $dialog = $( "#content_soCancelForm" );
    var $cancelOrderBtn = $dialog.find( "input#cancelOrderBtn" );
    var $cancelBtn = $dialog.find( "input#cancelBtn" );

    initDialog();
    initFormFields();

    function initDialog() {
        $dialog.modal({ show : true });
        $dialog.on( "hide", function(e) { if ( e.target === this ) { 
            errorContainer.hide(); 
        }});
    }

    function initFormFields() {
        $cancelOrderBtn.click( function(e) {
            e.preventDefault();
            $cancelBtn.prop( "disabled", true );
            errorContainer.hide(); 

            if ( $dialog.find( "#remarks" ).val() ) {
                var url = $(this).data( "url" )
                    .replace( new RegExp( "%ID%", "g" ), $(this).data( "id" ) )
                    .replace( new RegExp( "%REMARKS%", "g" ), $dialog.find( "#remarks" ).val() );
                $.post( url, function( resp ) {
                    $cancelBtn.prop( "disabled", false );
                    if ( resp.success ) {
                        $dialog.modal( "hide" );
                        $("#sales_order_list_container").ajaxDataTable( "search" );//location.reload();
                    }
                });
            }
            else {
            	  errorContainer.find( "div" ).html( $dialog.find( "#remarks" ).data( "err-reqd" ) );
            	  errorContainer.show( "slow" );
                $cancelBtn.prop( "disabled", false );
            }
        });
    }

    SoCancelForm = {
        show  : function() {
            $dialog.modal( "show" );
        }
    };

});
</script>


</div>