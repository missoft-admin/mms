package com.transretail.crm.core.entity.enums;

public enum MarketingInterestedProds {

	APPLIANCES("appliances"),
	FRESH("fresh"),
	GROCERIES("groceries"),
	TEXTILE("textile"),
	HOUSEHOLD("household");

    private MarketingInterestedProds(String code){
    	this.code = code;
    }

    private final String code;

	public String getCode() {
		return code;
	}
    
    
}
