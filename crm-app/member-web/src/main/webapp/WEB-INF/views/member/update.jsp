<%@ include file="../taglibs.jsp"%>

<div class="page-header page-header2">
	<h1>
		<spring:message code="menu.memberupdate" /> - 
		<c:choose>
			<c:when test="${not empty form.parentAccountId}">
				<spring:message code="supplementary_account_label" 
				arguments="${form.parentName},${form.parentAccountId}" />
			</c:when>
			<c:otherwise>
				<spring:message code="primary_account_label" />
			</c:otherwise>
		</c:choose>
	</h1>
</div>

<ul class="nav nav-tabs">
    <c:choose>
      <c:when test="${isProffesional}">
        <jsp:include page="professionaltabs.jsp" />
      </c:when>
      <c:otherwise>
        <jsp:include page="individualtabs.jsp" />
      </c:otherwise>    
    </c:choose>
</ul>

<spring:url value="/member/update" var="actionUrl" />
<form:form id="form" modelAttribute="form" method="POST" action="${actionUrl}"
	cssClass="form-horizontal">
	<!--<div>
		<c:choose>
			<c:when test="${not empty form.parentAccountId}">
				<spring:message code="supplementary_account_label" 
				arguments="${form.parentName},${form.parentAccountId}" />
			</c:when>
			<c:otherwise>
				<spring:message code="primary_account_label" />
			</c:otherwise>
		</c:choose>
	</div>-->
	
	<div class="tab-content">
    
    <c:choose>
      <c:when test="${isProffesional}">
        <jsp:include page="professionaltabcontents.jsp" />
      </c:when>
      <c:otherwise>
        <jsp:include page="individualtabcontents.jsp" />
      </c:otherwise>    
    </c:choose>
  
	</div>
</form:form>
<style>
.category-group {
	width: 35em;
	background: #f5f5f5;
	border: 1px solid #e5e5e5;
	padding: 1em 1em 1em 3.5em;
}

.category-group label {
	text-transform: none;
}

.category-group label.checkbox {
	font-size: .8em;
}

.controls input {
  margin-bottom: 10px;
}

.address-line {
	margin-bottom: 5px;
}
</style>

<script src='<c:url value="/js/bootstrap/bootstrap-datepicker.js"/>'
	type='text/javascript'>
	<![CDATA[ & nbsp;
	]]>
</script>
<link href="<c:url value="/css/bootstrap/datepicker.css" />"
	rel="stylesheet" />
<script src="<spring:url value="/js/jquery/jquery.dataTables.js" />"></script>
<script src="<spring:url value="/js/jquery/jquery.dataTables.linkLengthMenu.js" />"></script>
<script src="<spring:url value="/js/jquery/jquery.dataTables.customPaging.js" />"></script>
<script src="<spring:url value="/js/jquery/ajax-datatable.js" />"></script>
<script src="<spring:url value="/js/common/form2js.js" />"></script>
<script src="<spring:url value="/js/common/json2.js" />"></script>
<script src="<spring:url value="/js/jquery/jquery.toObject.js" />"></script>

<script>
	custSegSelect = new Object();
	<c:forEach items="${customerSegmentations}" var="segmentMap">
		var map = new Object();
		<c:forEach items="${segmentMap.value}" var="details">
			map["${details.code}"] = "${details.description}";
		</c:forEach>
		custSegSelect["${segmentMap.key}"] = map;
	</c:forEach>

	ages = [];
	<c:forEach items="${form.childrenAges}" var="ages">
		ages.push("${ages}");
	</c:forEach>

	$('#memberTypeCode').change(function() {
		var selectedValue = $('option:selected', this).val();
		if (selectedValue == 'MTYP003') { // Company Code
			$('.professionalDetails').show();
			$('.individualDetails').hide();
		} else {
			$('.individualDetails').show();
			$('.professionalDetails').hide();
		}
	}).change();

	$("#customerGroup").change(function(){
		setSegmentationOptions($(this).val());
	});

	$("#birthDate").datepicker({
		format : 'dd-mm-yyyy',
		endDate : '-0d',
		autoclose : true
	});

	function addField(name) {
		$("#" + name)
				.append(
						$("<br class='added-field' /><input type='text' class='form-control added-field' name='" + name + "' style='margin-top: 1px;' />"));
	}

	function addFields() {
		addField('contact');
		addField('bestTimeToCall');
	}

	function setSegmentationOptions(code) {
		if(!code) return;
		var options = custSegSelect[code];
		$("#customerSegmentation").empty();
		$.each(options, function(key, value) {
			$("#customerSegmentation").append("<option value='" + key + "'>" + value + "</option>");
		});
	}

	function toggleAlertEmail(value) {
		if(value) {
			$("#alertEmail").show();
		}
		else {
			$("#alertEmail").hide();
		}
	}

	function childrenAgeFields(count) {
		$("#childrenAges").empty();
		for(var i = 0; i < count; i++) {
			$("#childrenAges").append($("<input type='text' name='childrenAges' style='margin-bottom: 1px'/><br />"));
		}
		if(count > 4) {
			$("#childrenAges").css('height', '100px');
		}
	}

	function initSupplementTable() {
		console.log("here");
		var table = $("#cards").ajaxDataTable({
			'autoload': true,
			'ajaxSource': '<spring:url value="/member/supplements" />',
			'columnHeaders': [
				'<spring:message code="member.update.accountno" />',
				'<spring:message code="username" />',
				'<spring:message code="name" />',
				'<spring:message code="points.transaction.contributedpoints" />'],
			'modelFields': [
				{name: 'accountId', sortable: true},
				{name: 'username', sortable: true},
				{name: 'firstName', sortable: true, fieldCellRenderer: function(data, type, row) {
					return row.formattedMemberName;
				}},
				{name: 'totalPoints', sortable: true}
			]
			});
	}
	
	$("[name='idNumber']").each(function() {
    	if($(this).val() != null && $(this).val() != "") {
    		var vals = $(this).val().split("-");
    		$parent = $(this).parent();
    		$parent.find("[name='idNumberType'][value='"+vals[0]+"']").prop("checked", true);
    		$parent.find("[name='idNumberTxt']").val(vals[1]);
    	}
    	
    });
	
	function toggleOthersInput() {
		if($(this).data("text") == "OTHERS") {
			$others = $(this).parent().find("input[type='text']");
			if($(this).is(":checked")) {
				$others.removeAttr("disabled");
			} else {
				$others.attr("disabled", "disabled");
				$others.val("");
			}	
		}
		
	}
	
	$("input[type='checkbox'][name='informedAbout']").click(toggleOthersInput);
    $("input[type='checkbox'][name='informedAbout']").each(toggleOthersInput);

	$(document).ready(function() {
		var s = "${form.contact}";
		var t = "${form.bestTimeToCall}";
		var contacts = s.substring(1, s.length - 1).split(',');
		var times = t.substring(1, t.length - 1).split(',');

		for (var i = 1; i < contacts.length; i++) {
			//addField('contact');
		}

		for (var i = 1; i < times.length; i++) {
			//addField('bestTimeToCall');
		}

		$.each($("[name='contact']"), function(k, v) {
			$(v).val(contacts[k]);
		});

		$.each($("[name='bestTimeToCall']"), function(k, v) {
			console.log(times[k]);
			$(v).val(times[k]);
		});

		$('.employeeDetails').hide();
		if('${form.memberTypeCode}' == 'MTYP003') { // Company Code
			$('.professionalDetails').show();
			$('.individualDetails').hide();
			$("#customerGroup").val("${form.customerGroupCode}");
			setSegmentationOptions("${form.customerGroupCode}");
			$("#customerSegmentation").val("${form.customerSegmentationCode}");
		} else {
			$('.individualDetails').show();
			$('.professionalDetails').hide();
			if('${form.memberTypeCode}' == 'MTYP002') {
				$('.employeeDetails').show();
			}
		}

		$("#childrenCount").change(function() {
			childrenAgeFields($(this).val());
		});

		$("#childrenCount").focusout(function() {
			childrenAgeFields($(this).val());
		});
		$("#childrenCount").change();

		$.each($("[name='childrenAges']"), function(k, v) {
			$(v).val(ages[k]);
		});

		$("#alertEmail").hide();
		$("#email").change(function() {
			toggleAlertEmail($(this).val());
		});
		$("#email").focusout(function() {
			toggleAlertEmail($(this).val());
		});
		$("#email").change();

		if($("#hasCreditCard").is(":checked")) {
			$("#banks").show();
		}
		else {
			$("#banks").hide();
		}

		$("#hasCreditCard").change(function() {
			if($(this).is(":checked")) {
				$("#banks").show();
			}
			else {
				$("#banks").hide();
			}
		});

		if("${isPrimary}" == 'true') {
			initSupplementTable();
		}
		
		$("#form").submit(function() {
			if($("[name='idNumberTxt']").val() != "" && $("[name='idNumberType']:checked").val() != "") {
				$("[name='idNumber']").val($("[name='idNumberType']:checked").val() +"-"+ $("[name='idNumberTxt']").val());
			}
		});

		$( "#form" ).find( ".checkboxFix" ).change( function(e) {
			if ( this.checked ) { $(this).val( $(this).data( "value" ) ); }
			else { $(this).val( "" ); }
		}).change();

		setNumericFields();
	  function setNumericFields() {
/* 	    $(".floatInput").numberInput({ "type" : "float" });
	    $(".intInput").numberInput({ "type" : "int" });
	    $(".wholeInput").numberInput({ "type" : "whole" }); */
	  }
	});
</script>