package com.transretail.crm.web.controller;

import com.google.common.collect.ImmutableMap;
import com.google.common.collect.ImmutableMap.Builder;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.transretail.crm.common.reporting.exception.ReportException;
import com.transretail.crm.common.reporting.jasper.JRProcessor;
import com.transretail.crm.common.util.DateUtil;
import com.transretail.crm.core.entity.PeopleSoftStore;
import com.transretail.crm.core.entity.enums.AppKey;
import com.transretail.crm.core.entity.enums.ReportType;
import com.transretail.crm.core.entity.lookup.LookupDetail;
import com.transretail.crm.core.security.util.UserUtil;
import com.transretail.crm.core.service.CodePropertiesService;
import com.transretail.crm.core.service.LookupService;
import com.transretail.crm.core.service.PsoftStoreService;
import com.transretail.crm.core.service.StoreService;
import com.transretail.crm.giftcard.dto.ProductProfileDto;
import com.transretail.crm.giftcard.entity.support.GiftCardSaleTransaction;
import com.transretail.crm.giftcard.entity.support.GiftCardSalesType;
import com.transretail.crm.giftcard.entity.support.SalesReportType;
import com.transretail.crm.giftcard.service.ProductProfileService;
import com.transretail.crm.giftcard.service.SalesAccountingReportService;
import com.transretail.crm.report.template.FilterField;
import com.transretail.crm.schedule.service.JobSchedulerService;
import com.transretail.crm.schedule.trigger.ClaimToAffiliatesScheduleTrigger;
import com.transretail.crm.web.controller.util.ControllerResponse;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.time.DateUtils;
import org.apache.log4j.Logger;
import org.joda.time.DateTime;
import org.joda.time.LocalDate;
import org.joda.time.LocalTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.quartz.CronExpression;
import org.quartz.SchedulerException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.*;
import org.springframework.web.bind.annotation.*;

/**
 *
 * @author Monte Cillo Co (mco@exist.com)
 */
@Controller
@RequestMapping("/giftcard/acct")
public class GiftCardAccountingController extends AbstractReportsController {

    private static final Logger LOG = Logger.getLogger(GiftCardAccountingController.class);

    @Autowired
    private JobSchedulerService jobSchedulerService;
    @Autowired
    private MessageSource messageSource;
    @Autowired
    private StoreService storeService;
    @Autowired
    private LookupService lookupService;
    @Autowired
    private CodePropertiesService codePropertiesService;
    @Autowired
    private ProductProfileService profileService;
    @Autowired
    private SalesAccountingReportService accountingReportService;

    private final PsoftStoreService psoftStoreService;

    private static final String UIATTR_RESCHED = "schedule";
    private static final String UIATTR_NEXT_SCHED = "nextSchedule";
    private static final String UIATTR_NEXT_SCHED_COVERED = "nextScheduleCoverage";
    private static final String UIATTR_DAYS = "days";
    private static final String UIATTR_DAY = "day";
    private static final String UIATTR_TIME = "time";

    @Autowired
    public GiftCardAccountingController(PsoftStoreService psoftStoreService) {
        this.psoftStoreService = psoftStoreService;
    }

    @RequestMapping("/claimstoaffiliates/schedule")
    public String claimsToAffiliatesSchedule(Model uiModel) throws SchedulerException, ParseException {
        String strCronExp = jobSchedulerService.getCronExpression(
                ClaimToAffiliatesScheduleTrigger.TRIGGER_NAME, ClaimToAffiliatesScheduleTrigger.TRIGGER_GROUP);
        uiModel.addAttribute(UIATTR_RESCHED, strCronExp);

        CronExpression cronExp = new CronExpression(strCronExp);
        DateTime dt = new DateTime(cronExp.getNextValidTimeAfter(new Date()).getTime());

        uiModel.addAttribute(UIATTR_DAYS, 28);
        uiModel.addAttribute(UIATTR_DAY, dt.getDayOfMonth());
        uiModel.addAttribute(UIATTR_TIME, dt.getHourOfDay() + ":" + dt.getMinuteOfHour() + ":" + dt.getSecondOfMinute());

        Date nextGeneration = cronExp.getNextValidTimeAfter(new Date());
        uiModel.addAttribute(UIATTR_NEXT_SCHED, new SimpleDateFormat("dd MMMM yyyy HH:mm:ss").format(nextGeneration));
        Date prevMonth = DateUtils.addMonths(nextGeneration, -1);
        uiModel.addAttribute(UIATTR_NEXT_SCHED_COVERED,
                new SimpleDateFormat("dd").format(DateUtils.truncate(prevMonth, Calendar.MONTH)) + " - "
                + new SimpleDateFormat("dd MMMM yyyy").format(DateUtils.addMilliseconds(DateUtils.ceiling(prevMonth, Calendar.MONTH), -1)));
        return "claimstoaffiliates/schedule";
    }

    @RequestMapping("/claimstoaffiliates/reschedule")
    @ResponseBody
    public ControllerResponse claimsToAffiliatesJobReschedule(
            @RequestParam(value = "schedule", required = false) String sched,
            @RequestParam(value = "day", required = false) String day,
            @RequestParam(value = "time", required = false) String time,
            Model uiModel) {

        ControllerResponse resp = new ControllerResponse();
        resp.setSuccess(false);

        if (StringUtils.isNotBlank(day) && StringUtils.isNotBlank(time) && StringUtils.isNotBlank(sched)) {
            try {
                LocalTime lt = new LocalTime(time);
                String[] cronExpSegs = StringUtils.split(sched);
                cronExpSegs[0] = String.valueOf(lt.getSecondOfMinute());
                cronExpSegs[1] = String.valueOf(lt.getMinuteOfHour());
                cronExpSegs[2] = String.valueOf(lt.getHourOfDay());
                cronExpSegs[3] = String.valueOf(day);
                sched = StringUtils.join(cronExpSegs, " ");
            } catch (Exception e) {
                resp.setResult(e.getMessage());
            }
        }

        if (!StringUtils.isBlank(sched)) {
            if (!CronExpression.isValidExpression(sched)) {
                resp.setResult(messageSource.getMessage("points_msg_invalidsched", null, LocaleContextHolder.getLocale()));
            } else {
                String ret = null;
                try {
                    ret = jobSchedulerService.rescheduleJob(ClaimToAffiliatesScheduleTrigger.TRIGGER_NAME,
                            ClaimToAffiliatesScheduleTrigger.TRIGGER_GROUP, sched, AppKey.GC_CLAIMS_TO_AFFILIATES_JOB);
                    sched = jobSchedulerService.getCronExpression(ClaimToAffiliatesScheduleTrigger.TRIGGER_NAME,
                            ClaimToAffiliatesScheduleTrigger.TRIGGER_GROUP);
                } catch (SchedulerException e) {
                    resp.setResult(e.getMessage());
                } catch (ParseException e) {
                    resp.setResult(e.getMessage());
                }

                if (StringUtils.isBlank(ret)) {
                    resp.setResult(messageSource.getMessage("points_msg_unsavedsched", null, LocaleContextHolder.getLocale()));
                }
            }
        }

        if (null == resp.getResult()) {
            resp.setSuccess(true);
            resp.setResult(sched);
        }

        return resp;
    }

    @RequestMapping("/sales/report")
    public String salesReportView(Model model) {
        model.addAttribute("reportTypes", new ReportType[]{ReportType.EXCEL});
        model.addAttribute("salesReportCategory", SalesReportType.values());

        return "sales/report";
    }

    @RequestMapping("/sales/report/export/{reportType}")
    public void printSalesReport(@RequestParam Map<String, String> requestParams, @PathVariable ReportType reportType,
            HttpServletResponse response) throws ReportException {
        JRProcessor processor = null;
        DateTimeFormatter formatter = DateTimeFormat.forPattern(DateUtil.SEARCH_DATE_FORMAT);
        String tmp = requestParams.get("salesReportType");
        SalesReportType salesReportType = SalesReportType.valueOf(tmp);

        tmp = requestParams.get(ATTR_BOOKED_DATE_FROM);
        LocalDate bookedDateFrom = LocalDate.parse(tmp, formatter);
        tmp = requestParams.get(ATTR_BOOKED_DATE_TO);
        LocalDate bookedDateTo = LocalDate.parse(tmp, formatter);

        String store = requestParams.get(ATTR_STORE);
        String businessUnit = requestParams.get(ATTR_BUSINESS_UNIT);

        LOG.info("sales report type : " + salesReportType);
        if (SalesReportType.DETAIL == salesReportType) {
            tmp = requestParams.get(ATTR_SALES_TYPE);
            GiftCardSalesType salesType = StringUtils.isNotBlank(tmp) ? B2B_RETURN.equals(tmp) 
                    ? GiftCardSalesType.B2B : GiftCardSalesType.valueOf(tmp) : null;
            processor = accountingReportService.printSalesDetail(bookedDateFrom, bookedDateTo, businessUnit, store, salesType, B2B_RETURN.equals(tmp));
        } else if (SalesReportType.SUMMARY == salesReportType) {
            tmp = requestParams.get(ATTR_TRANSACTION_TYPE);
            GiftCardSaleTransaction transactionType = null;
            if (StringUtils.isNotBlank(tmp)) {
                transactionType = GiftCardSaleTransaction.valueOf(tmp);
            }

            tmp = requestParams.get(ATTR_SALES_TYPE);
            GiftCardSalesType salesType = null;
            if (StringUtils.isNotBlank(tmp)) {
                salesType = GiftCardSalesType.valueOf(tmp);
            }

            processor = accountingReportService.printSalesSummary(bookedDateFrom, bookedDateTo, businessUnit, transactionType, store, salesType);
        } else if (SalesReportType.REDEEM == salesReportType) {
            String product = requestParams.get(ATTR_PRODUCT);
            String redeemType = requestParams.get(ATTR_REDEEM_TYPE);
            processor = accountingReportService.printRedeemDetail(bookedDateFrom, bookedDateTo, businessUnit, store, product, redeemType);
        } else if (SalesReportType.OUTSTANDING_BALANCE == salesReportType) {
            String cardNo = requestParams.get(ATTR_CARD_NO);
            processor = accountingReportService.printOutstandingBalance(bookedDateFrom, bookedDateTo, store, cardNo);
        }

        if (ReportType.EXCEL.equals(reportType)) {
            renderExcelReport(response, processor, createNameFrom(salesReportType, bookedDateFrom, bookedDateTo));
        } else {
            renderReport(response, processor);
        }
    }

    @RequestMapping("sales/report/filters/{saleReportType}")
    @ResponseBody
    public List<FilterField> getFilters(@PathVariable SalesReportType saleReportType) {
        List<FilterField> filters = Lists.newLinkedList();
        filters.add(FilterField.createDateField(ATTR_BOOKED_DATE_FROM, "Booked Date From", "Booked Date From", true));
        filters.add(FilterField.createDateField(ATTR_BOOKED_DATE_TO, "Booked Date To", "Booked Date To", true));

        if (saleReportType == SalesReportType.OUTSTANDING_BALANCE) {
            filters.add(FilterField.createDropdownField(ATTR_STORE, "Store", createStoreOptions()));
            filters.add(FilterField.createInputField(ATTR_CARD_NO, "Card Number"));
        } else if (saleReportType == SalesReportType.SUMMARY) {
            filters.add(FilterField.createDropdownField(ATTR_BUSINESS_UNIT, "Business Unit", createBUOptions()));
            filters.add(FilterField.createDropdownField(ATTR_TRANSACTION_TYPE, "Transaction Type", createTransactionTypeOptions()));
            filters.add(FilterField.createDropdownField(ATTR_STORE, "Store", createStoreOptions()));
            filters.add(FilterField.createDropdownField(ATTR_SALES_TYPE, "Sales Type", createSalesTypeOptions()));
        } else if (saleReportType == SalesReportType.REDEEM) {
            filters.add(FilterField.createDropdownField(ATTR_BUSINESS_UNIT, "Business Unit", createBUOptions()));
            filters.add(FilterField.createDropdownField(ATTR_STORE, "Redeem Store", createStoreOptions()));
            filters.add(FilterField.createDropdownField(ATTR_PRODUCT, "Product Name", createProductOptions()));
            filters.add(FilterField.createDropdownField(ATTR_REDEEM_TYPE, "Redeem Type", createRedeemTypeOptions()));
        } else if (saleReportType == SalesReportType.DETAIL) {
            filters.add(FilterField.createDropdownField(ATTR_STORE, "Store", createStoreOptions()));
            filters.add(FilterField.createDropdownField(ATTR_BUSINESS_UNIT, "Business Unit", createBUOptions()));
            filters.add(FilterField.createDropdownField(ATTR_SALES_TYPE, "Sales Type", createSalesTypeWithReturnOptions()));
        }

        return filters;
    }

    @RequestMapping("/sales/report/export/validate")
    @ResponseBody
    public List<String> validateSalesReportExport(@RequestParam Map<String, String> requestParams,
            HttpServletResponse response) throws ReportException {
        MapBindingResult bindingResult = new MapBindingResult(requestParams, "requestParam");

        if (StringUtils.isBlank(requestParams.get(ATTR_BOOKED_DATE_FROM))) {
            ValidationUtils.rejectIfEmpty(bindingResult, "bookeDateFrom", "filters.empty.bookedDateFrom", "Booked Date From is required.");
        } else if (StringUtils.isBlank(requestParams.get(ATTR_BOOKED_DATE_TO))) {
            ValidationUtils.rejectIfEmpty(bindingResult, "bookeDateFrom", "filters.empty.bookedDateTo", "Booked Date To is required.");
        }

        List<String> errors = Lists.newArrayList();
        if (bindingResult.hasErrors()) {
            for (FieldError fieldError : bindingResult.getFieldErrors()) {
                for (String errorCode : fieldError.getCodes()) {
                    String message = "";
                    try {
                        message = messageSource.getMessage(errorCode, fieldError.getArguments(), LocaleContextHolder.getLocale());
                    } catch (org.springframework.context.NoSuchMessageException e) {
                        message = fieldError.getDefaultMessage();
                    }

                    if (StringUtils.isNotEmpty(message)) {
                        errors.add(message);
                        break;
                    }
                }
            }
        }

        return errors;
    }

    private String createNameFrom(SalesReportType salesReportType, LocalDate dateFrom, LocalDate dateTo) {
        String name = "";
        switch (salesReportType) {
            case DETAIL:
                name = "Sales Detal";
                break;
            case SUMMARY:
                name = "Sales Summary";
                break;
            case REDEEM:
                name = "Redemption Detail";
                break;
            case OUTSTANDING_BALANCE:
                name = "Outstanding Detail";
                break;
            default:
                throw new AssertionError();
        }

        return String.format("%s - [%s-%s]", name, dateFrom, dateTo);
    }

    private Map<String, String> createBUOptions() {

        String hdr = codePropertiesService.getHeaderBusinessUnit();

        List<LookupDetail> result = lookupService.getDetailsByHeaderCode(hdr);

        final String currentStore = UserUtil.getCurrentUser().getStoreCode();
        PeopleSoftStore store = psoftStoreService.getStoreMapping(currentStore);
        LookupDetail lookupDetail = store.getBusinessUnit();

        Map<String, String> options = Maps.newLinkedHashMap();
        options.put("", "");

        if (!currentStore.equals("10007")) {

            options.put(lookupDetail.getCode(), String.format("%s - %s", lookupDetail.getCode(), lookupDetail.getDescription()));

            return ImmutableMap.copyOf(options);

        } else {

            for (LookupDetail detail : result) {
                options.put(detail.getCode(), String.format("%s - %s", detail.getCode(), detail.getDescription()));
            }

            return ImmutableMap.copyOf(options);
        }
    }

    private Map<String, String> createRedeemTypeOptions() {
        return ImmutableMap.<String, String>builder().put("", "")
                .put("NORMAL", "NORMAL")
                .put("MANUAL", "MANUAL").build();
    }

    private Map<String, String> createProductOptions() {
        List<ProductProfileDto> profiles = profileService.findAllDtos();
        LinkedHashMap<String, String> options = Maps.newLinkedHashMap();
        options.put("", "");
        for (ProductProfileDto profile : profiles) {
            options.put(profile.getProductCode(), profile.getProductDesc());
        }

        return ImmutableMap.copyOf(options);
    }

    private Map<String, String> createStoreOptions() {
        return ImmutableMap.<String, String> builder().put("", "").putAll(storeService.getStoreByUserStoreLoc()).build();
    }

    private Map<String, String> createSalesTypeOptions() {
        Builder<String, String> builder = ImmutableMap.<String, String>builder().put("", "");
        for (GiftCardSalesType value : GiftCardSalesType.values()) {
            builder.put(value.name(), value.name());
        }

        return builder.build();
    }

    private Map<String, String> createSalesTypeWithReturnOptions() {
        Map<String, String> salesType = this.createSalesTypeOptions();
        Map<String, String> withReturn = Maps.newLinkedHashMap(salesType);
        withReturn.put(B2B_RETURN, B2B_RETURN);

        return ImmutableMap.copyOf(withReturn);
    }

    private Map<String, String> createTransactionTypeOptions() {
        Builder<String, String> builder = ImmutableMap.<String, String>builder().put("", "")
                .put(GiftCardSaleTransaction.ACTIVATION.name(), GiftCardSaleTransaction.ACTIVATION.name())
                .put(GiftCardSaleTransaction.VOID_ACTIVATED.name(), GiftCardSaleTransaction.VOID_ACTIVATED.name());

        return builder.build();
    }

    static final String ATTR_CARD_NO = "cardNo";
    static final String ATTR_REDEEM_TYPE = "redeemType";
    static final String ATTR_PRODUCT = "product";
    static final String ATTR_SALES_TYPE = "salesType";
    static final String ATTR_TRANSACTION_TYPE = "transactionType";
    static final String ATTR_BUSINESS_UNIT = "buId";
    static final String ATTR_STORE = "store";
    static final String ATTR_BOOKED_DATE_TO = "bookedDateTo";
    static final String ATTR_BOOKED_DATE_FROM = "bookedDateFrom";
    static final String B2B_RETURN = "B2B-RETURN";

}
