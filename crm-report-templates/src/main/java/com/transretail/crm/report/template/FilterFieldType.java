package com.transretail.crm.report.template;

/**
 * @author Allan G. Ramirez (agramirez@exist.com)
 */
public enum FilterFieldType {
    INPUT, DROPDOWN, TOGGLE, DATE, DATE_TO, DATE_FROM, SELECT_MULTIPLE, DATE_TIME
}