package com.transretail.crm.core.dto;


import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.util.StringUtils;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.mysema.query.types.expr.BooleanExpression;
import com.transretail.crm.common.service.dto.request.AbstractSearchFormDto;
import com.transretail.crm.core.entity.QUserModel;
import com.transretail.crm.core.entity.UserModel;
import com.transretail.crm.core.entity.UserRoleModel;
import com.transretail.crm.core.util.BooleanExprUtil;


public class UserSearchDto extends AbstractSearchFormDto {

	private String username;
	private String email;
	private String name;
	private List<UserModel> notUsers;

	@Override
	@JsonIgnore
	public BooleanExpression createSearchExpression() {
		
		List<BooleanExpression> expressions = new ArrayList<BooleanExpression>();
        QUserModel theUser = QUserModel.userModel;

        if ( StringUtils.hasText( username ) ) {
            expressions.add( BooleanExprUtil.INSTANCE.isStringPropertyLike( theUser.username, username ) );
        }
        if ( StringUtils.hasText( email ) ) {
            expressions.add( BooleanExprUtil.INSTANCE.isStringPropertyLike( theUser.email, email ) );
        }
        if (StringUtils.hasText( name ) ) {
            expressions.add( 
            		BooleanExprUtil.INSTANCE.isStringPropertyLike( theUser.firstName, name).or( 
            		BooleanExprUtil.INSTANCE.isStringPropertyLike( theUser.lastName, name) ) );
        }
        if ( CollectionUtils.isNotEmpty( notUsers ) ) {
        	expressions.add( theUser.notIn( notUsers ) );
        }

		return BooleanExpression.allOf(expressions.toArray(new BooleanExpression[expressions.size()]));
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<UserModel> getNotUsers() {
		return notUsers;
	}

	public void setNotUsers(List<UserModel> notUsers) {
		this.notUsers = notUsers;
	}

}
