package com.transretail.crm.core.service.impl;

import com.transretail.crm.core.util.generator.IdGeneratorService;

import junit.framework.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Configurable;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;


@Configurable
@ContextConfiguration(locations = "classpath*:/META-INF/spring/applicationContext*.xml")
@Transactional
@RunWith(SpringJUnit4ClassRunner.class)
public class IdGeneratorServiceTest {

    @Autowired
    IdGeneratorService accountIdGeneratorService;

    @Autowired
    IdGeneratorService customIdGeneratorService;


    @Test
    public void testGenerateId() {

        String pin = accountIdGeneratorService.generateId();
        Assert.assertEquals(10, pin.length());

    }


    @Test
    public void testCustomIdGenerateId() {

    	String id = customIdGeneratorService.generateId( "TEST" );
        Assert.assertEquals( true, id.contains( "TEST" ) );
    }

}
