package com.transretail.crm.giftcard.dto;

import java.util.List;

import org.joda.time.LocalDate;
import org.springframework.format.annotation.DateTimeFormat;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.google.common.collect.Lists;
import com.transretail.crm.common.web.converter.LocalDateSerializer;
import com.transretail.crm.giftcard.entity.support.PaymentInfoStatus;

public class SalesOrderPaymentsDto {
	
	private Long orderId;
	private String orderNo;
	private String customerName;
	@DateTimeFormat(pattern = "dd-MM-yyyy")
	@JsonSerialize(using = LocalDateSerializer.class)
	private LocalDate orderDate;
	private PaymentInfoStatus status;
	private List<SalesOrderPaymentInfoDto> payments = Lists.newArrayList(new SalesOrderPaymentInfoDto());
	
	public Long getOrderId() {
		return orderId;
	}
	public void setOrderId(Long orderId) {
		this.orderId = orderId;
	}
	public String getOrderNo() {
		return orderNo;
	}
	public void setOrderNo(String orderNo) {
		this.orderNo = orderNo;
	}
	public String getCustomerName() {
		return customerName;
	}
	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}
	public LocalDate getOrderDate() {
		return orderDate;
	}
	public void setOrderDate(LocalDate orderDate) {
		this.orderDate = orderDate;
	}
	public List<SalesOrderPaymentInfoDto> getPayments() {
		return payments;
	}
	public void setPayments(List<SalesOrderPaymentInfoDto> payments) {
		this.payments = payments;
	}
	public PaymentInfoStatus getStatus() {
		return status;
	}
	public void setStatus(PaymentInfoStatus status) {
		this.status = status;
	}
	
	
	

}
