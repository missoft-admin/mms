package com.transretail.crm.core.dto;

import java.util.List;

/**
 * @author Allan G. Ramirez (agramirez@exist.com)
 */
public class UserRolePermRequestDto {
    private List<String> rolePermTuples;

    public List<String> getRolePermTuples() {
        return rolePermTuples;
    }

    public void setRolePermTuples(List<String> rolePermTuples) {
        this.rolePermTuples = rolePermTuples;
    }
}
