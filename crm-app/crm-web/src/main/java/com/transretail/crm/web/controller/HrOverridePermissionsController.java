package com.transretail.crm.web.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.transretail.crm.common.service.dto.request.PageSortDto;
import com.transretail.crm.common.service.dto.response.ResultList;
import com.transretail.crm.core.dto.ApprovalMatrixDto;
import com.transretail.crm.core.entity.ApprovalMatrixModel;
import com.transretail.crm.core.entity.UserModel;
import com.transretail.crm.core.entity.enums.ModelType;
import com.transretail.crm.core.service.ApprovalMatrixService;
import com.transretail.crm.core.service.HrOverridePermissionsService;
import com.transretail.crm.core.service.UserService;

@RequestMapping("/override/permissions")
@Controller
public class HrOverridePermissionsController {
	
	private static final String UI_ATTR_APPROVERS = "approvers";
	private static final String UI_ATTR_APPROVER = "approver";
	private static final String UI_ATTR_USERS = "users";
	
	@Autowired
	UserService userService;
	
	@Autowired
	ApprovalMatrixService approvalMatrixService;
	
	@Autowired
	HrOverridePermissionsService overridePermissionsService;

	
	private List<UserModel> getApproverDropdown() {
		List<UserModel> users = userService.retrieveHrsUsers();
		List<ApprovalMatrixModel> approvers = overridePermissionsService.findApprovers();
		
		for (ApprovalMatrixModel approver : approvers) {
			users.remove(approver.getUser());
		}	
		
		return users;
	}
	
	@RequestMapping(value = "/{id}", method = RequestMethod.DELETE, produces = "text/html")
    public String deleteApprover(@PathVariable("id") Long id,
            Model uiModel) {
        overridePermissionsService.deleteApprover(id);
        uiModel.asMap().clear();
        return "redirect:/override/permissions";
    }
	
	@RequestMapping(method = RequestMethod.POST, produces = "text/html")
    public String createApprover(ApprovalMatrixModel approver,
            BindingResult bindingResult, Model uiModel,
            HttpServletRequest httpServletRequest) {
        uiModel.asMap().clear();
        if(approver.getUser() != null && approver.getUser().getId() != null) {
	        approver.setModelType(ModelType.PURCHASETXN);
	        approvalMatrixService.save(approver);
        }
        return "redirect:/override/permissions";
    }
	
	@RequestMapping( produces = "text/html")
    public String listApprovers(
            Model uiModel) {
		
		uiModel.addAttribute(UI_ATTR_APPROVER, new ApprovalMatrixModel());
		uiModel.addAttribute(UI_ATTR_USERS, getApproverDropdown());
        uiModel.addAttribute(UI_ATTR_APPROVERS, overridePermissionsService.findApprovers());

        return "/employee/adjustments/purchases/permissionslist";
    }
	
	@RequestMapping(value = "/list", method = RequestMethod.POST)
	public @ResponseBody ResultList<ApprovalMatrixDto> list(@RequestBody PageSortDto dto) {
		return overridePermissionsService.listApprovers(dto);
	}

}
