package com.transretail.crm.giftcard.entity;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.hibernate.annotations.Type;
import org.joda.time.LocalDate;

import com.transretail.crm.core.entity.support.CustomAuditableEntity;
import com.transretail.crm.giftcard.entity.support.DiscountType;

/**
 *
 */
@Entity
@Table(name = "CRM_GC_DISCOUNT_SCHEME")
public class GiftCardDiscountScheme extends CustomAuditableEntity<Long> implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@JoinColumn(name="CUSTOMER")
	@OneToOne
	private GiftCardCustomerProfile customer;
	@Column(name = "MIN_PURCHASE", precision = 19, scale = 5)
	private BigDecimal minPurchase;
	@Column(name = "STARTING_DISCOUNT")
	private BigDecimal startingDiscount;
	@Column(name = "PURCHASE_FOR_MAX_DISC", precision = 19, scale = 5)
	private BigDecimal purchaseForMaxDiscount;
	@Column(name = "ENDING_DISCOUNT")
	private BigDecimal endingDiscount;
	
	@Column(name = "START_DATE")
    @Type(type = "org.jadira.usertype.dateandtime.joda.PersistentLocalDate")
	private LocalDate startDate;
	@Column(name = "END_DATE")
    @Type(type = "org.jadira.usertype.dateandtime.joda.PersistentLocalDate")
	private LocalDate endDate;
	
	@Column(name = "DISCOUNT_TYPE")
	@Enumerated(EnumType.STRING)
	private DiscountType discountType;
	
	@Column(name = "INCLUDE_BELOW_MIN_PURCHASE")
    @Type(type = "yes_no")
	private Boolean includeBelowMinPurchase;
	
	public GiftCardDiscountScheme() {}
	
	public GiftCardDiscountScheme(Long id) {
		setId(id);
	}
	
	public GiftCardCustomerProfile getCustomer() {
		return customer;
	}
	public void setCustomer(GiftCardCustomerProfile customer) {
		this.customer = customer;
	}
	public BigDecimal getMinPurchase() {
		return minPurchase;
	}
	public void setMinPurchase(BigDecimal minPurchase) {
		this.minPurchase = minPurchase;
	}
	public BigDecimal getStartingDiscount() {
		return startingDiscount;
	}
	public void setStartingDiscount(BigDecimal startingDiscount) {
		this.startingDiscount = startingDiscount;
	}
	public BigDecimal getPurchaseForMaxDiscount() {
		return purchaseForMaxDiscount;
	}
	public void setPurchaseForMaxDiscount(BigDecimal purchaseForMaxDiscount) {
		this.purchaseForMaxDiscount = purchaseForMaxDiscount;
	}
	public BigDecimal getEndingDiscount() {
		return endingDiscount;
	}
	public void setEndingDiscount(BigDecimal endingDiscount) {
		this.endingDiscount = endingDiscount;
	}
	@Transient
	public String getSchemeDesc() {
		if(minPurchase != null && startingDiscount != null)
			return minPurchase.toString() + " - " + startingDiscount.toString();
		return "";
	}

	public LocalDate getStartDate() {
		return startDate;
	}

	public void setStartDate(LocalDate startDate) {
		this.startDate = startDate;
	}

	public LocalDate getEndDate() {
		return endDate;
	}

	public void setEndDate(LocalDate endDate) {
		this.endDate = endDate;
	}

	public DiscountType getDiscountType() {
		return discountType;
	}

	public void setDiscountType(DiscountType discountType) {
		this.discountType = discountType;
	}

	public Boolean getIncludeBelowMinPurchase() {
		return includeBelowMinPurchase;
	}

	public void setIncludeBelowMinPurchase(Boolean includeBelowMinPurchase) {
		this.includeBelowMinPurchase = includeBelowMinPurchase;
	}
	
	
	
	
	
}
