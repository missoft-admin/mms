package com.transretail.crm.giftcard.service.impl;

import static org.junit.Assert.*;

import java.util.List;
import java.util.Map;

import org.joda.time.LocalDate;
import org.joda.time.LocalDateTime;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.google.common.collect.Lists;
import com.transretail.crm.core.entity.PosTransaction;
import com.transretail.crm.core.entity.lookup.Store;
import com.transretail.crm.core.repo.PosTransactionRepo;
import com.transretail.crm.core.repo.StoreRepo;
import com.transretail.crm.giftcard.entity.CardVendor;
import com.transretail.crm.giftcard.entity.GiftCardInventory;
import com.transretail.crm.giftcard.entity.GiftCardOrder;
import com.transretail.crm.giftcard.entity.GiftCardTransaction;
import com.transretail.crm.giftcard.entity.GiftCardTransactionItem;
import com.transretail.crm.giftcard.entity.PosGcTransaction;
import com.transretail.crm.giftcard.entity.ProductProfile;
import com.transretail.crm.giftcard.entity.support.GiftCardOrderStatus;
import com.transretail.crm.giftcard.entity.support.GiftCardSaleTransaction;
import com.transretail.crm.giftcard.repo.CardVendorRepo;
import com.transretail.crm.giftcard.repo.GiftCardInventoryRepo;
import com.transretail.crm.giftcard.repo.GiftCardOrderRepo;
import com.transretail.crm.giftcard.repo.GiftCardTransactionItemRepo;
import com.transretail.crm.giftcard.repo.GiftCardTransactionRepo;
import com.transretail.crm.giftcard.repo.PosGcTransactionRepo;
import com.transretail.crm.giftcard.repo.ProductProfileRepo;
import com.transretail.crm.giftcard.service.GiftCardService;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath*:/META-INF/spring/applicationContext*.xml",
    "classpath:META-INF/spring/applicationContext-test.xml"})
public class GiftCardTransactionSettlementTest {
	@Autowired
	private GiftCardService service;
	@Autowired
	private GiftCardInventoryRepo inventoryRepo;
	@Autowired
	private GiftCardTransactionRepo gcTxnRepo;
	@Autowired
	private GiftCardTransactionItemRepo gcTxnItemRepo;
	@Autowired
	private PosGcTransactionRepo posGcTxnRepo;
	@Autowired
	private PosTransactionRepo posTxnRepo;
	@Autowired
	private CardVendorRepo cardVendorRepo;
	@Autowired
	private ProductProfileRepo profileRepo;
	@Autowired
	private GiftCardOrderRepo orderRepo;
	@Autowired
	private StoreRepo storeRepo;
	
	private GiftCardInventory createGiftCardInventory(GiftCardOrder order, ProductProfile profile, 
			int seqNum, long seriesNum, String barcode) {
		GiftCardInventory inv = new GiftCardInventory();
		inv.setOrder(order);
		inv.setProfile(profile);
		inv.setProductCode(profile.getProductCode());
		inv.setProductName(profile.getProductDesc());
		inv.setSeqNo(seqNum);
		inv.setBatchNo(seqNum);
		inv.setOrderDate(LocalDate.now());
		inv.setSeries(seriesNum);
		inv.setBarcode(barcode);
		return inventoryRepo.save(inv);
	}

	private PosGcTransaction createPosGcTxn(String id, String txId, String cardNumber, LocalDateTime txnDate) {
		return createPosGcTxn(id, txId, cardNumber, txnDate, null, null, null);
	}
	
	private PosGcTransaction createPosGcTxn(String id, String txId, String cardNumber, LocalDateTime txnDate, Character type, Double amount, Double balance) {
		PosGcTransaction txn = new PosGcTransaction();
		txn.setId(id);
		txn.setTxId(txId);
		txn.setTransactionDate(txnDate);
		txn.setCardNumber(cardNumber);
		txn.setRequestType(type);
		txn.setAmount(amount);
		txn.setBalance(balance);
		
		return posGcTxnRepo.save(txn);
	}
	
	private PosTransaction createPosTxn(String id, Store store, String baseReference) {
		PosTransaction posTxn = new PosTransaction();
		posTxn.setId(id);
		posTxn.setStore(store);
		//posTxn.setBaseReference(baseReference);
		return posTxnRepo.save(posTxn);
	}
	
	@Before
	public void setUp() {
		Store store = new Store();
		store.setCode("store");
		store.setId(1);
		store.setName("store");
		store = storeRepo.save(store);
		
		ProductProfile profile = new ProductProfile();
		profile.setProductCode("product1");
		profile.setProductDesc("product1");
		profile = profileRepo.save(profile);
		
		CardVendor vendor = new CardVendor();
		vendor.setFormalName("vendor");
		vendor.setMailAddress("address");
		vendor.setEmailAddress("vendor@test.com");
		vendor.setEnabled(true);
		vendor.setRegion("region");
		vendor.setDefaultVendor(true);
		vendor = cardVendorRepo.save(vendor);
		
		GiftCardOrder order = new GiftCardOrder();
		order.setVendor(vendor);
		order.setStatus(GiftCardOrderStatus.FULL);
		order.setMoNumber("1");
		order.setPoNumber("1");
		order = orderRepo.save(order);
		
		GiftCardInventory[] inv = new GiftCardInventory[5];
		for(int i = 0; i < inv.length; i++) {
			inv[i] = createGiftCardInventory(order, profile, i, (long) i, i + "");
		}
		
		List<GiftCardTransactionItem> items = Lists.newArrayList();
		GiftCardTransaction gcTxn = new GiftCardTransaction().transactionNo("000000000000001").transactionDate(LocalDateTime.now());
		gcTxn.setLoseTransaction(false);
		gcTxn.setTransactionType(GiftCardSaleTransaction.ACTIVATION);
		GiftCardTransactionItem item = new GiftCardTransactionItem();
		item.setTransaction(gcTxn);
		item.setGiftCard(inv[0]);
		items.add(item);
		item = new GiftCardTransactionItem();
		item.setTransaction(gcTxn);
		item.setGiftCard(inv[1]);
		items.add(item);
		
		GiftCardTransaction gcTxn2 = new GiftCardTransaction().transactionNo("000000000000002").transactionDate(LocalDateTime.now());
		gcTxn2.setLoseTransaction(false);
		gcTxn2.setTransactionType(GiftCardSaleTransaction.REDEMPTION);
		item = new GiftCardTransactionItem();
		item.setTransaction(gcTxn2);
		item.setGiftCard(inv[2]);
		items.add(item);
		item = new GiftCardTransactionItem();
		item.setTransaction(gcTxn2);
		item.setGiftCard(inv[3]);
		items.add(item);
		
		gcTxnItemRepo.save(items);
		
		// in db
		LocalDateTime date = LocalDateTime.now();
		createPosGcTxn("1", "000000000000001", inv[0].getBarcode(), date);
		createPosGcTxn("2", "000000000000001", inv[1].getBarcode(), date);
		createPosGcTxn("3", "000000000000002", inv[2].getBarcode(), date);
		createPosGcTxn("4", "000000000000002", inv[3].getBarcode(), date);
		// not in db
		createPosTxn("000000000000003", store, null);
		createPosTxn("000000000000004", store, "000000000000003");
		createPosGcTxn("5", "000000000000003", inv[4].getBarcode(), date, 'P', 100D, 1000D);
		createPosGcTxn("6", "000000000000004", inv[0].getBarcode(), date, 'V', 100D, 900D);
		createPosGcTxn("7", "000000000000004", inv[1].getBarcode(), date, 'V', 100D, 800D);
	}
	
	@Test
	public void testFindMissingPosGcTransactions() {
		assertEquals(2L, gcTxnRepo.count());
		assertEquals(4L, gcTxnItemRepo.count());
		assertEquals(7L, posGcTxnRepo.count());
		
		Map<List<?>, List<PosGcTransaction>> map = gcTxnRepo.findMissingPosGcTransactions();
		assertEquals(2L, (long) map.size());
		for(List<?> key : map.keySet()) {
			if(key.get(0).equals("000000000000003")) {
				assertEquals(1L, (long) map.get(key).size());
			}
			else if(key.get(0).equals("000000000000004")) {
				assertEquals(2L, (long) map.get(key).size());
			}
			else {
				// this shouldn't happen
				fail("Incorrect missing transactions");
			}
		}
	}
}
