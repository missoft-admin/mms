package com.transretail.crm.web.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.security.authentication.InsufficientAuthenticationException;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.support.RequestContextUtils;

import com.google.common.collect.Maps;
import com.mysema.query.types.expr.BooleanExpression;
import com.transretail.crm.common.service.dto.request.PagingParam;
import com.transretail.crm.common.service.dto.request.SearchFormDto;
import com.transretail.crm.common.service.exception.GenericServiceException;
import com.transretail.crm.core.entity.enums.Status;
import com.transretail.crm.core.entity.lookup.QStore;
import com.transretail.crm.core.security.model.impl.CustomSecurityUserDetailsImpl;
import com.transretail.crm.core.security.util.UserUtil;
import com.transretail.crm.core.service.CodePropertiesService;
import com.transretail.crm.core.service.NameIdPairService;
import com.transretail.crm.giftcard.dto.GiftCardInventoryReceivingSearchDto;
import com.transretail.crm.giftcard.dto.GiftCardInventoryResultList;
import com.transretail.crm.giftcard.dto.GiftCardInventorySaveDto;
import com.transretail.crm.giftcard.dto.GiftCardInventorySearchDto;
import com.transretail.crm.giftcard.dto.GiftCardInventorySummarizedResultList;
import com.transretail.crm.giftcard.dto.GiftCardOrderAllocateDto;
import com.transretail.crm.giftcard.service.GiftCardInventoryService;

/**
 * @author Allan G. Ramirez (agramirez@exist.com)
 */
@Controller
@RequestMapping("/giftcard/inventory")
public class GiftCardInventoryController {
    protected static final String KEY_SUCCESS = "successMessages";
    private static final String KEY_ERROR = "errorMessages";
    private static final String KEY_USERSTORE_CODE = "userStore";
    @Autowired
    private MessageSource messageSource;
    @Autowired
    private NameIdPairService nameIdPairService;
    @Autowired
    private GiftCardInventoryService inventoryService;
    @Autowired
    private CodePropertiesService codePropertiesService;

    @RequestMapping(value = "/receive", method = RequestMethod.GET)
    public ModelAndView showReceiveNewGiftCardsPage() {
        return new ModelAndView("giftcard/inventory/receive");
    }

    @RequestMapping(value = "/list", method = RequestMethod.GET)
    public ModelAndView showInventoryListPage() {
        ModelAndView mav = new ModelAndView("giftcard/inventory/list");
        CustomSecurityUserDetailsImpl user = UserUtil.getCurrentUser();
        if (user.hasAuthority(codePropertiesService.getMerchantServiceSupervisorCode())) {
            final QStore qStore = QStore.store;
            mav.addObject("stores",
                nameIdPairService.getNameIdPairs(null, qStore, qStore.id, qStore.name, null));
        } else {
            if (StringUtils.isBlank(user.getStoreCode())) {
                throw new InsufficientAuthenticationException(
                    messageSource.getMessage("user.no.assigned.store", null, LocaleContextHolder.getLocale()));
            }
            mav.addObject(KEY_USERSTORE_CODE, user.getStoreCode());
        }
        return mav;
    }

    @ExceptionHandler(MethodArgumentNotValidException.class)
    @ResponseBody
    public Map<String, List<String>> handleMethodArgumentNotValidException(MethodArgumentNotValidException error) {
        BindingResult result = error.getBindingResult();
        List<String> errors = new ArrayList<String>();
        for (ObjectError oe : result.getAllErrors()) {
            errors.add(messageSource.getMessage(oe, LocaleContextHolder.getLocale()));
        }
        Map<String, List<String>> map = new HashMap<String, List<String>>();
        map.put(KEY_ERROR, errors);
        return map;
    }

    // Receiving
    @RequestMapping(value = "/receive/save", method = RequestMethod.POST)
    @ResponseBody
    public Map<String, Object> receiveNewGiftCards(@Valid @RequestBody GiftCardInventorySaveDto dto, HttpServletRequest request) {
        Map<String, Object> map = Maps.newHashMap();
        Locale locale = RequestContextUtils.getLocale(request);
        long totalItems = inventoryService.receiveNewGiftCards(dto);
        map.put(KEY_SUCCESS, messageSource.getMessage("gc.inventory.receive.successful", new Long[]{totalItems}, locale));
        return map;
    }

    @RequestMapping(value = "/receive/detail/list", method = RequestMethod.POST)
    @ResponseBody
    public GiftCardInventoryResultList getForReceiveGiftCardInventories(
        @Valid @RequestBody GiftCardInventoryReceivingSearchDto searchDto) {
        return inventoryService.getGiftCardInventories(searchDto);
    }

    @RequestMapping(value = "/receive/summary/list", method = RequestMethod.POST)
    @ResponseBody
    public GiftCardInventorySummarizedResultList getForReceiveSummarizedGiftCardInventories(
        @Valid @RequestBody GiftCardInventoryReceivingSearchDto searchDto) {
        return inventoryService.getSummarizedGiftCardInventories(searchDto);
    }

    @RequestMapping(value = "/detail/list", method = RequestMethod.POST)
    @ResponseBody
    public GiftCardInventoryResultList getGiftCardInventories(
        @Valid @RequestBody GiftCardInventorySearchDto searchDto) {
        CustomSecurityUserDetailsImpl user = UserUtil.getCurrentUser();
        if (!user.hasAuthority(codePropertiesService.getMerchantServiceSupervisorCode())) {
            if (StringUtils.isBlank(user.getStoreCode())) {
                throw new InsufficientAuthenticationException(
                    messageSource.getMessage("user.no.assigned.store", null, LocaleContextHolder.getLocale()));
            }
            searchDto.setLocationOrAllocateToCode(user.getStoreCode());
        }
        return inventoryService.getGiftCardInventories(searchDto);
    }

    @RequestMapping(value = "/summary/list", method = RequestMethod.POST)
    @ResponseBody
    public GiftCardInventorySummarizedResultList getSummarizedGiftCardInventories(
        @Valid @RequestBody GiftCardInventorySearchDto searchDto) {
        CustomSecurityUserDetailsImpl user = UserUtil.getCurrentUser();
        if (!user.hasAuthority(codePropertiesService.getMerchantServiceSupervisorCode())) {
            if (StringUtils.isBlank(user.getStoreCode())) {
                throw new GenericServiceException(
                    messageSource.getMessage("user.no.assigned.store", null, LocaleContextHolder.getLocale()));
            }
            searchDto.setLocationOrAllocateToCode(user.getStoreCode());
        }
        return inventoryService.getSummarizedGiftCardInventories(searchDto);
    }

    @RequestMapping(value = "/allocate", method = RequestMethod.POST)
    @ResponseBody
    public Map<String, Object> allocateGiftCards(@Valid @RequestBody GiftCardOrderAllocateDto dto, HttpServletRequest request) {
        Map<String, Object> map = Maps.newHashMap();
        Locale locale = RequestContextUtils.getLocale(request);
        Long totalItems = inventoryService.allocateGiftCards(dto);
        map.put(KEY_SUCCESS,
            messageSource.getMessage("gc.inventory.allocate.successful", new Object[]{totalItems, dto.getAllocateToDesc()}, locale));
        return map;
    }
}
