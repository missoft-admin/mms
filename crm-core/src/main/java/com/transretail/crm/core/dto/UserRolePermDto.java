package com.transretail.crm.core.dto;

import com.transretail.crm.common.service.dto.response.CodeDescDto;

/**
 * @author Allan G. Ramirez (agramirez@exist.com)
 */
public class UserRolePermDto extends CodeDescDto {
    private boolean currentRolePerm;
    private String staff;
    private UserRolePermDto supervisor;
    private String emailAddress;

    public UserRolePermDto(String code) {
        this(code, null);
    }

    public UserRolePermDto(String code, String desc) {
        setCode(code);
        setDesc(desc);
    }

    public boolean isCurrentRolePerm() {
        return currentRolePerm;
    }

    public void setCurrentRolePerm(boolean currentRolePerm) {
        this.currentRolePerm = currentRolePerm;
    }

	public String getStaff() {
		return staff;
	}

	public void setStaff(String staff) {
		this.staff = staff;
	}

	public UserRolePermDto getSupervisor() {
		return supervisor;
	}

	public void setSupervisor(UserRolePermDto supervisor) {
		this.supervisor = supervisor;
	}

    public String getEmailAddress() {
        return emailAddress;
    }

    public void setEmailAddress(String emailAddress) {
        this.emailAddress = emailAddress;
    }
}