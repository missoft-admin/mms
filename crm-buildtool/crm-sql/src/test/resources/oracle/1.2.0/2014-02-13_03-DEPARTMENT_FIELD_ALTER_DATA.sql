-- Task #85120
-- extract Business Field data for Employees and apply this to Department
UPDATE CRM_MEMBER m1 SET DEPARTMENT = (SELECT BUSINESS_FIELD FROM CRM_MEMBER m2 WHERE MEMBER_TYPE = 'MTYP002' AND m1.id = m2.id);
-- delete Business Field data for Employees
UPDATE CRM_MEMBER SET BUSINESS_FIELD = NULL WHERE MEMBER_TYPE = 'MTYP002';