package com.transretail.crm.giftcard.dto;

import com.mysema.query.types.expr.BooleanExpression;
import com.transretail.crm.common.service.dto.request.AbstractSearchFormDto;

/**
 * description here.
 *
 * @author Vincent Gerard Tan
 */
public class PrepaidCardSearchDto extends AbstractSearchFormDto {
    @Override
    public BooleanExpression createSearchExpression() {
        return null;
    }
}
