/*
 * Copyright (c) 2012.
 * All rights reserved.
 */

package com.transretail.crm.core.security.model.impl;

import java.util.Collection;
import java.util.Set;

import com.transretail.crm.core.entity.enums.AuthenticationType;
import org.apache.commons.lang3.StringUtils;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.util.Assert;

import com.google.common.collect.Sets;
import com.transretail.crm.common.security.model.AbstractSecurityUserDetails;
import com.transretail.crm.core.entity.UserRoleModel;
import com.transretail.crm.core.entity.lookup.Store;
import com.transretail.crm.core.security.model.CustomSecurityUserDetails;
import com.transretail.crm.core.security.model.CustomSecurityUserInfo;
import com.transretail.crm.core.security.util.SecurityContextAuthenticationPrincipalInterface;

/**
 * @author mhua
 */
public class CustomSecurityUserDetailsImpl extends AbstractSecurityUserDetails<CustomSecurityUserInfo>
        implements CustomSecurityUserDetails, SecurityContextAuthenticationPrincipalInterface {

    private static final long serialVersionUID = 1L;

    private AuthenticationType authenticationType;
    private Long userId;
    private String email;

    private CustomSecurityUserInfoImpl userInfo;
    private Collection<GrantedAuthority> authorities = Sets.newLinkedHashSet();
    private Collection<UserRoleModel> roles = Sets.newLinkedHashSet();

    private String password;
    private String username;
    private String status;

    private boolean accountExpired;
    private boolean accountLocked;
    private boolean enabled;
    private boolean credentialsExpired;
    private Long loginInfoId;
    /**
     * Value is taken from UserModel.storeCode which is mapped to Store.id
     */
    private String storeCode;
    private Store store;
    private String inventoryLocation;
    private String inventoryLocationName;
    private String fullName;
    
    private String b2bLocation;



    private Set<String> permissions = Sets.newHashSet();


    public AuthenticationType getAuthenticationType() {
        return authenticationType;
    }



    public Long getUserId() {
        return userId;
    }

    @Override
    public String getEmail() {
        return email;
    }

    @Override
    public CustomSecurityUserInfoImpl getUserInfo() {
        return userInfo;
    }

    @Override
    public boolean hasAuthority(String targetAuthority) {
        return findAuthority(targetAuthority) != null;
    }

    @Override
    public Collection<GrantedAuthority> getAuthorities() {
        return authorities;
    }

    @Override
    public String getPassword() {
        return password;
    }

    @Override
    public String getUsername() {
        return username;
    }

    @Override
    public Collection<UserRoleModel> getRoles() {
        return roles;
    }

    @Override
    public boolean isAccountNonExpired() {
        return !accountExpired;
    }

    @Override
    public boolean isAccountNonLocked() {
        return !accountLocked;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return !credentialsExpired;
    }

    @Override
    public boolean isEnabled() {
        return enabled;
    }

    public void setAuthenticationType(AuthenticationType authenticationType) {
        this.authenticationType = authenticationType;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setUserInfo(CustomSecurityUserInfoImpl userInfo) {
        this.userInfo = userInfo;
    }

    public void setAuthorities(Collection<GrantedAuthority> authorities) {
        this.authorities = authorities;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    @Override
    public void setRoles(Collection<UserRoleModel> roles) {
        this.roles = roles;
    }

    public void setAccountExpired(boolean accountExpired) {
        this.accountExpired = accountExpired;
    }

    public void setAccountLocked(boolean accountLocked) {
        this.accountLocked = accountLocked;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    public void setCredentialsExpired(boolean credentialsExpired) {
        this.credentialsExpired = credentialsExpired;
    }

    public Long getLoginInfoId() {
        return loginInfoId;
    }

    public void setLoginInfoId(Long loginInfoId) {
        Assert.notNull(loginInfoId);
        this.loginInfoId = loginInfoId;
    }

    public String getStoreCode() {
        return storeCode;
    }

    public void setStoreCode(String storeCode) {
        this.storeCode = storeCode;
    }

    public Store getStore() {
        return store;
    }

    public void setStore(Store store) {
        this.store = store;
    }

//  processName(name, userInfo.getLastName());
//  name.append(", ");
//  processName(name, userInfo.getFirstName());
//  name.append(" ");
//  processName(name, userInfo.getMiddleName());
//  return name.toString();
	public String getInventoryLocation() {
		return inventoryLocation;
	}

	public void setInventoryLocation(String inventoryLocation) {
		this.inventoryLocation = inventoryLocation;
	}

	@Override
    public String getFullName() {
		return fullName;
//        StringBuffer name = new StringBuffer();
//        processName(name, userInfo.getLastName());
//        name.append(", ");
//        processName(name, userInfo.getFirstName());
//        name.append(" ");
//        processName(name, userInfo.getMiddleName());
//        return name.toString();
    }
	
	public void setFullName(String fullName) {
		this.fullName = fullName;
	}

    private StringBuffer processName(StringBuffer name, String namePart) {
        String mask = "**********";
        if (StringUtils.isNotBlank(namePart)) {
            name.append(namePart.trim());
        } else {
            name.append(mask);
        }
        return name;
    }

	public Set<String> getPermissions() {
		return permissions;
	}

	public void setPermissions(Set<String> permissions) {
		this.permissions = permissions;
	}

	public String getInventoryLocationName() {
		return inventoryLocationName;
	}

	public void setInventoryLocationName(String inventoryLocationName) {
		this.inventoryLocationName = inventoryLocationName;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}



	public String getB2bLocation() {
		return b2bLocation;
	}



	public void setB2bLocation(String b2bLocation) {
		this.b2bLocation = b2bLocation;
	}
	

    
}
