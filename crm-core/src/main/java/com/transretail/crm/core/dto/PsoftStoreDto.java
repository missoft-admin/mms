package com.transretail.crm.core.dto;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;

import com.google.common.collect.Lists;
import com.transretail.crm.core.entity.PeopleSoftStore;
import com.transretail.crm.core.entity.lookup.LookupDetail;
import com.transretail.crm.core.entity.lookup.Store;


public class PsoftStoreDto {

	private Long id;
	private String peoplesoftCode;
	private Boolean allowGcTxn = Boolean.FALSE;
	private Integer storeId;
    private String storeCode;
    private String storeName;
	private String buCode;
	private String buName;
	private String brCode;
	private String brName;
	private String btCode;
	private String btName;

    private String store;
	private String businessUnit;
	private String businessRegion;



	public PsoftStoreDto() {}
	public PsoftStoreDto( PeopleSoftStore model ) {
		id = model.getId();
		peoplesoftCode = model.getPeoplesoftCode();
		allowGcTxn = model.getAllowGcTxn();
		if ( null != model.getStore() ) {
			storeId = model.getStore().getId();
			storeCode = model.getStore().getCode();
			storeName = model.getStore().getName();
		}
		if ( null != model.getBusinessUnit() ) {
			buCode = model.getBusinessUnit().getCode();
			buName = model.getBusinessUnit().getDescription();
		}
		if ( null != model.getBusinessRegion() ) {
			brCode = model.getBusinessRegion().getCode();
			brName = model.getBusinessRegion().getDescription();
		}
		if ( null != model.getBusinessTerritory() ) {
			btCode = model.getBusinessTerritory().getCode();
			btName = model.getBusinessTerritory().getDescription();
		}
	}

	public PeopleSoftStore update() {
		return this.update( null );
	}
	public PeopleSoftStore update( PeopleSoftStore model ) {
		if ( null == model ) {
			model = new PeopleSoftStore();
		}

		model.setPeoplesoftCode( peoplesoftCode );
		model.setAllowGcTxn( allowGcTxn );
		model.setStore( null != storeId? new Store( storeId ) : null );
		model.setBusinessUnit( StringUtils.isNotBlank( buCode )? new LookupDetail( buCode ) : null );
		model.setBusinessRegion( StringUtils.isNotBlank( brCode )? new LookupDetail( brCode ) : null );
		model.setBusinessTerritory( StringUtils.isNotBlank( btCode )? new LookupDetail( btCode ) : null );
		return model;
	}

	public static List<PsoftStoreDto> list( List<PeopleSoftStore> models ) {
		if ( CollectionUtils.isEmpty( models ) ) {
			return new ArrayList<PsoftStoreDto>();
		}

		List<PsoftStoreDto> dtos = Lists.newArrayList(); 
		for ( PeopleSoftStore model : models ) {
			dtos.add( new PsoftStoreDto( model ) );
		}
		return dtos;
	}



	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getPeoplesoftCode() {
		return peoplesoftCode;
	}
	public void setPeoplesoftCode(String peoplesoftCode) {
		this.peoplesoftCode = peoplesoftCode;
	}
	public Boolean getAllowGcTxn() {
		return allowGcTxn;
	}
	public void setAllowGcTxn(Boolean allowGcTxn) {
		this.allowGcTxn = allowGcTxn;
	}
	public Integer getStoreId() {
		return storeId;
	}
	public void setStoreId(Integer storeId) {
		this.storeId = storeId;
	}
	public String getStoreCode() {
		return storeCode;
	}
	public void setStoreCode(String storeCode) {
		this.storeCode = storeCode;
	}
	public String getStoreName() {
		return storeName;
	}
	public void setStoreName(String storeName) {
		this.storeName = storeName;
	}
	public String getBuCode() {
		return buCode;
	}
	public void setBuCode(String buCode) {
		this.buCode = buCode;
	}
	public String getBuName() {
		return buName;
	}
	public void setBuName(String buName) {
		this.buName = buName;
	}
	public String getBrCode() {
		return brCode;
	}
	public void setBrCode(String brCode) {
		this.brCode = brCode;
	}
	public String getBrName() {
		return brName;
	}
	public String getBtCode() {
		return btCode;
	}
	public void setBtCode(String btCode) {
		this.btCode = btCode;
	}
	public String getBtName() {
		return btName;
	}
	public String getStore() {
		return store;
	}
	public void setStore(String store) {
		this.store = store;
	}
	public String getBusinessUnit() {
		if ( StringUtils.isNotBlank( this.buCode ) || StringUtils.isNotBlank( this.buName ) ) {
			return StringUtils.join( this.buCode, " - ", this.buName );
		}
		return businessUnit;
	}
	public String getBusinessRegion() {
		if ( StringUtils.isNotBlank( this.brCode ) || StringUtils.isNotBlank( this.brName ) ) {
			return StringUtils.join( this.brCode, " - ", this.brName );
		}
		return businessRegion;
	}

}
