package com.transretail.crm.web.stamp.controller.dto;


import java.util.Set;

import org.apache.commons.collections.CollectionUtils;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.google.common.collect.Sets;
import com.transretail.crm.core.dto.StampPromoTxnDto;


@JsonInclude(JsonInclude.Include.NON_NULL)
public class StampPromoPreredeemResponse extends StampPromoResponse {

	private Set<StampPromoRedeemItem> redeemItems = Sets.newHashSet();



	public Set<StampPromoRedeemItem> getRedeemItems() {
		return redeemItems;
	}
	public void setRedeemItems(Set<StampPromoRedeemItem> redeemItems) {
		this.redeemItems = redeemItems;
	}

	public StampPromoPreredeemResponse withRedeemItems( Set<StampPromoTxnDto> redeemItems ) {
		if ( CollectionUtils.isNotEmpty( redeemItems ) ) {
			for ( StampPromoTxnDto dto : redeemItems ) {
				this.redeemItems.add( toResponseDto( dto ) );
			}
		}
		return this;
    }



	private StampPromoRedeemItem toResponseDto( StampPromoTxnDto dto ) {
		StampPromoRedeemItem item = new StampPromoRedeemItem();
		item.setPromoID( dto.getPromoID() );
		item.setPromo( dto.getPromo() );
		item.setPromoStart( dto.getPromoStart() );
		item.setPromoEnd( dto.getPromoEnd() );
		item.setRedeemTo( dto.getRedeemTo() );
		item.setSku( dto.getSku() );
		item.setProductName( dto.getProductName() );
		item.setItemQty( dto.getItemQty() );
		item.setRedeemStamps( dto.getRedeemedStamps() );
		item.setStore( dto.getStoreCode() + " - " + dto.getStore() );
		return item;
	}

}
