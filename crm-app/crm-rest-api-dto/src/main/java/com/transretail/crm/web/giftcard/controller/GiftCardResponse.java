package com.transretail.crm.web.giftcard.controller;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.google.common.collect.Sets;
import com.transretail.crm.common.web.converter.ISODateTimeSerializer;
import com.transretail.crm.giftcard.dto.GiftCardInfo;
import java.util.Set;
import org.joda.time.LocalDateTime;

/**
 *
 * @author Monte Cillo Co (mco@exist.com)
 */
public class GiftCardResponse {

    private boolean successful = true;
    @JsonSerialize(using = ISODateTimeSerializer.class)
    private LocalDateTime processedDateTime;
    private String message;

    public GiftCardResponse() {
        processedDateTime = LocalDateTime.now();
    }
    
    /**
     * reference number of the transaction from server
     */
    @JsonInclude(Include.NON_NULL)
    private String referenceNumber;
    @JsonInclude(Include.NON_EMPTY)
    private Set<GiftCardInfo> cardInfos = Sets.newHashSet();

    public GiftCardResponse withSuccessful(boolean successful) {
	this.successful = successful;
	return this;
    }

    public GiftCardResponse withMessage(String message) {
	this.message = message;
	return this;
    }
    
    public GiftCardResponse withProcessedDateTime(LocalDateTime processedDateTime) {
	this.processedDateTime = processedDateTime;
	return this;
    }

    public GiftCardResponse withReferenceNo(String refNo) {
	this.referenceNumber = refNo;
	return this;
    }

    public GiftCardResponse withCardInfos(Set<GiftCardInfo> cardInfos) {
	this.cardInfos = cardInfos;
	return this;
    }

    public GiftCardResponse addCardInfo(GiftCardInfo cardInfo) {
	cardInfos.add(cardInfo);
	return this;
    }

    public boolean isSuccessful() {
	return successful;
    }

    public String getReferenceNumber() {
	return referenceNumber;
    }

    public Set<GiftCardInfo> getCardInfos() {
	return cardInfos;
    }

    public void setSuccessful(boolean successful) {
	this.successful = successful;
    }

    public void setReferenceNumber(String referenceNumber) {
	this.referenceNumber = referenceNumber;
    }

    public void setCardInfos(Set<GiftCardInfo> cardInfos) {
	this.cardInfos = cardInfos;
    }

    public LocalDateTime getProcessedDateTime() {
	return processedDateTime;
    }

    public void setProcessedDateTime(LocalDateTime processedDateTime) {
	this.processedDateTime = processedDateTime;
    }
}
