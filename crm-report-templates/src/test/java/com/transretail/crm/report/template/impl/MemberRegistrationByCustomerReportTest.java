package com.transretail.crm.report.template.impl;

import java.io.File;
import java.util.Date;
import java.util.Locale;
import java.util.Map;

import net.sf.jasperreports.engine.JRParameter;

import org.apache.commons.io.FileUtils;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.context.support.MessageSourceResourceBundle;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import com.google.common.collect.Maps;
import com.transretail.crm.common.reporting.jasper.JRProcessor;
import com.transretail.crm.common.reporting.service.ReportService;
import com.transretail.crm.core.entity.MemberModel;
import com.transretail.crm.core.entity.embeddable.CustomerProfile;
import com.transretail.crm.core.entity.enums.Status;
import com.transretail.crm.core.entity.lookup.LookupDetail;
import com.transretail.crm.core.entity.lookup.LookupHeader;
import com.transretail.crm.core.repo.LookupDetailRepo;
import com.transretail.crm.core.repo.LookupHeaderRepo;
import com.transretail.crm.core.repo.MemberRepo;
import com.transretail.crm.report.template.CommonReportFilter;

/**
 * @author ftopico
 */
@Transactional
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath*:/META-INF/spring/applicationContext*.xml",
    "/META-INF/spring/applicationContext-test.xml"})
public class MemberRegistrationByCustomerReportTest {

    @Autowired
    private MemberRepo memberRepo;
    @Autowired
    private LookupDetailRepo lookupDetailRepo;
    @Autowired
    private LookupHeaderRepo lookupHeaderRepo;
    @Autowired
    private MemberRegistrationByCustomerReport memberRegistrationByCustomerReport;
    @Autowired
    private MessageSource messageSource;
    @Autowired
    private ReportService reportService;
    private static final Logger logger = LoggerFactory.getLogger(AffiliateTransactionSalesReport.class);
    
    protected static final String FILTER_CUSTOMER_TYPE = "customerType";
    protected static final String FILTER_STORE = "store";
    protected static final String FILTER_MEMBER_REPORT_TYPE = "memberReportType";
    protected static final String MEMBER_REPORT_TYPE_SUMMARY = "summary";
    protected static final String MEMBER_REPORT_TYPE_REGISTER_TYPE = "type";
    protected static final String MEMBER_REPORT_TYPE_STORE = "store";
    
    
    @Before
    public void setup() {
        LookupHeader header = lookupHeaderRepo.saveAndFlush(new LookupHeader("MEM001", "Member Type"));
        lookupDetailRepo.saveAndFlush(new LookupDetail("MTYP001", "Individual", header, Status.ACTIVE));
        lookupDetailRepo.saveAndFlush(new LookupDetail("MTYP003", "Professional", header, Status.ACTIVE));
        
        MemberModel memberModel = new MemberModel();
        memberModel.setFirstName("First name");
        memberModel.setLastName("Last name");
        memberModel.setMiddleName("Middle Name");
        memberModel.setPin("1234");
        memberModel.setPinEncryped(false);
        memberModel.setEmail("anyEmail@gmail.com");
        memberModel.setUsername("username");
        memberModel.setPassword("admin");
        memberModel.setAccountId("12341234");
        memberModel.setContact("012341231213");
        memberModel.setMemberType(lookupDetailRepo.findByCode("MTYP001"));
        
        CustomerProfile customerProfile = new CustomerProfile();
        customerProfile.setBirthdate(new Date());
        memberModel.setCustomerProfile(customerProfile);
        
        memberRepo.saveAndFlush(memberModel);
        
        memberModel = new MemberModel();
        memberModel.setFirstName("First name2");
        memberModel.setLastName("Last name2");
        memberModel.setMiddleName("Middle Name2");
        memberModel.setPin("12345");
        memberModel.setPinEncryped(false);
        memberModel.setEmail("any2Email@gmail.com");
        memberModel.setUsername("username2");
        memberModel.setPassword("admin2");
        memberModel.setAccountId("123412342");
        memberModel.setContact("0123412312132");
        memberModel.setMemberType(lookupDetailRepo.findByCode("MTYP003"));
        
        customerProfile = new CustomerProfile();
        customerProfile.setBirthdate(new Date());
        memberModel.setCustomerProfile(customerProfile);
        
        memberRepo.saveAndFlush(memberModel);
    }
    
    @After
    public void cleanup() {
        memberRepo.deleteAll();
    }
    
    @Test
    public void createJrProcessorSummaryTest() throws Exception {
        Map<String, String> map = Maps.newHashMap();
        map.put(CommonReportFilter.DATE_FROM, "01-01-2013");
        map.put(CommonReportFilter.DATE_TO, "01-01-2100");
        map.put(FILTER_CUSTOMER_TYPE, null);
        map.put(FILTER_STORE, null);
        map.put(FILTER_MEMBER_REPORT_TYPE, MEMBER_REPORT_TYPE_SUMMARY);
        JRProcessor jrProcessor = memberRegistrationByCustomerReport.createJrProcessor(map);
        
        File testClassesDir = FileUtils.toFile(Thread.currentThread().getContextClassLoader().getResource(""));
        File output = new File(testClassesDir, "test-member-registration-report-output.pdf");
        logger.debug("Output location {}", output.getAbsolutePath());
        output.delete();
        Locale locale = LocaleContextHolder.getLocale();
        jrProcessor.addParameter(JRParameter.REPORT_LOCALE, locale);
        jrProcessor.addParameter(JRParameter.REPORT_RESOURCE_BUNDLE, new MessageSourceResourceBundle(messageSource, locale));
        reportService.exportPdfReport(jrProcessor, output);

        double bytes = output.length();
        double kilobytes = (bytes / 1024);
        logger.debug("Kilobytes: {}", kilobytes);
    }
    
    @Test
    public void createJrProcessorStoreTest() throws Exception {
        Map<String, String> map = Maps.newHashMap();
        map.put(CommonReportFilter.DATE_FROM, "01-01-2013");
        map.put(CommonReportFilter.DATE_TO, "01-01-2100");
        map.put(FILTER_CUSTOMER_TYPE, null);
        map.put(FILTER_STORE, null);
        map.put(FILTER_MEMBER_REPORT_TYPE, MEMBER_REPORT_TYPE_STORE);
        JRProcessor jrProcessor = memberRegistrationByCustomerReport.createJrProcessor(map);
        
        File testClassesDir = FileUtils.toFile(Thread.currentThread().getContextClassLoader().getResource(""));
        File output = new File(testClassesDir, "test-member-registration-report-output.pdf");
        logger.debug("Output location {}", output.getAbsolutePath());
        output.delete();
        Locale locale = LocaleContextHolder.getLocale();
        jrProcessor.addParameter(JRParameter.REPORT_LOCALE, locale);
        jrProcessor.addParameter(JRParameter.REPORT_RESOURCE_BUNDLE, new MessageSourceResourceBundle(messageSource, locale));
        reportService.exportPdfReport(jrProcessor, output);

        double bytes = output.length();
        double kilobytes = (bytes / 1024);
        logger.debug("Kilobytes: {}", kilobytes);
    }
    
    @Test
    public void createJrProcessorTypeTest() throws Exception {
        Map<String, String> map = Maps.newHashMap();
        map.put(CommonReportFilter.DATE_FROM, "01-01-2013");
        map.put(CommonReportFilter.DATE_TO, "01-01-2100");
        map.put(FILTER_CUSTOMER_TYPE, null);
        map.put(FILTER_STORE, null);
        map.put(FILTER_MEMBER_REPORT_TYPE, MEMBER_REPORT_TYPE_REGISTER_TYPE);
        JRProcessor jrProcessor = memberRegistrationByCustomerReport.createJrProcessor(map);
        
        File testClassesDir = FileUtils.toFile(Thread.currentThread().getContextClassLoader().getResource(""));
        File output = new File(testClassesDir, "test-member-registration-report-output.pdf");
        logger.debug("Output location {}", output.getAbsolutePath());
        output.delete();
        Locale locale = LocaleContextHolder.getLocale();
        jrProcessor.addParameter(JRParameter.REPORT_LOCALE, locale);
        jrProcessor.addParameter(JRParameter.REPORT_RESOURCE_BUNDLE, new MessageSourceResourceBundle(messageSource, locale));
        reportService.exportPdfReport(jrProcessor, output);

        double bytes = output.length();
        double kilobytes = (bytes / 1024);
        logger.debug("Kilobytes: {}", kilobytes);
    }
}
