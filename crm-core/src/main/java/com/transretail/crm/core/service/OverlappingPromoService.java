package com.transretail.crm.core.service;

import java.util.List;

import com.transretail.crm.core.dto.OverlappingPromoDto;

/**
 * @author Allan G. Ramirez (agramirez@exist.com)
 */
public interface OverlappingPromoService {
    List<OverlappingPromoDto> getOverlappingPromo(long promotionId);
}
