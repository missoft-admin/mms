<%@ include file="../../common/taglibs.jsp" %>


<div id="content_campaignForm" class="modal hide  nofly modal-dialog">

  <div class="modal-content">
  <c:url var="url_campaignForm" value="/promotion/campaign/save"/>
  <form:form id="campaignForm" name="campaign" modelAttribute="campaign" action="${url_campaignForm}" class="modal-form form-horizontal">

    <div class="modal-header">
      <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
      <h4><spring:message code="campaign_label_create"/></h4>
    </div>

    <div class="modal-body">
      <div class="hide alert alert-error" id="content_error"><button type="button" class="close" onclick="$(this).parent().hide();">&times;</button><div><form:errors path="*"/></div></div>

      <div class="control-group">
        <label for="program" class="control-label"><spring:message code="campaign_prop_program"/></label>
        <div class="controls">
	        <form:select path="program" id="content_programs">
	          <c:forEach var="program" items="${programs}">
	            <form:option value="${program.id}" label="${program.name}" data-min-date="${program.startDate}" data-max-date="${program.endDate}"/>
	          </c:forEach>
	        </form:select>
        </div>
      </div>
      <div class="control-group">
        <label for="name" class="control-label"><b class="required">*</b> <spring:message code="campaign_prop_name"/></label>
        <div class="controls"><form:input path="name" id="name"/></div>
      </div>
      <div class="control-group">
        <label for="description" class="control-label"><b class="required">*</b> <spring:message code="campaign_prop_description"/></label>
        <div class="controls"><form:input path="description" id="description" /></div>
      </div>
      <div class="input-daterange" id="campaignDuration">
	      <div class="control-group">
	        <label for="" class="control-label"><b class="required">*</b> <spring:message code="promotion_label_startdate"/></label>
	        <div class="controls" id="">
	          <input type="text" id="campaignDurationStart" />
	        </div>
	      </div>
        <div class="control-group">
          <label for="" class="control-label"><b class="required">*</b> <spring:message code="promotion_label_enddate"/></label>
          <div class="controls" id="">
            <input type="text" id="campaignDurationEnd" />
          </div>
        </div>
        <form:hidden path="startDate" id="campaignStartDate"/>
        <form:hidden path="endDate" id="campaignEndDate"/>
      </div>
      <div class="control-group">
        <label for="noOfCampaignDays" class="control-label"><spring:message code="promotion_label_duration"/></label>
        <spring:message var="label_days" code="promotion_label_days" />
        <div class="controls data-content-04"><span id="noOfCampaignDays" data-label="&nbsp;${label_days}"></span></div>
      </div>
    </div>

    <div class="modal-footer">
      <button type="saveButton" id="saveButton" class="btn btn-primary"><spring:message code="label_save" /></button>
      <button type="button" class="btn" data-dismiss="modal" id="cancelButton"><spring:message code="label_cancel" /></button>
    </div>

  </form:form>
  </div>


  <script src='<c:url value="/js/viewspecific/promotion/campaign/campaignForm.js"/>' type='text/javascript'><![CDATA[&nbsp; ]]></script>


</div>