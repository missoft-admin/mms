package com.transretail.crm.core.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.transretail.crm.core.entity.lookup.Store;
import com.transretail.crm.core.entity.support.CustomIdAuditableEntity;

@Table(name = "CRM_POINTS_PROMOTION")
@Entity
public class TransactionAudit extends CustomIdAuditableEntity<String> {
	
	@Column(name = "TXN_NO")
	private String transactionNo;
	
	@Column(name = "TXN_POINTS")
	private Long transactionPoints;
	
	@ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "STORE_CODE")
    private Store store;
	
	@ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "MEMBER_ID")
    private MemberModel memberModel;
	
	@JoinColumn(name="PROMOTION")
    @ManyToOne(fetch = FetchType.LAZY)
    private Promotion promotion;

    @Column(name = "PRODUCT_PLU_ID")
    private String productId;


    public String getTransactionNo() {
        return transactionNo;
    }
    public void setTransactionNo(String transactionNo) {
        this.transactionNo = transactionNo;
    }
    public Long getTransactionPoints() {
        return transactionPoints;
    }
    public void setTransactionPoints(Long transactionPoints) {
        this.transactionPoints = transactionPoints;
    }
    public Store getStore() {
        return store;
    }
    public void setStore(Store store) {
        this.store = store;
    }
    public MemberModel getMemberModel() {
        return memberModel;
    }
    public void setMemberModel(MemberModel memberModel) {
        this.memberModel = memberModel;
    }
    public Promotion getPromotion() {
        return promotion;
    }
    public void setPromotion(Promotion promotion) {
        this.promotion = promotion;
    }

    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }
}
