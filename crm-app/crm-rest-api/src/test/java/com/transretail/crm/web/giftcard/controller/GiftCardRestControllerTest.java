package com.transretail.crm.web.giftcard.controller;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Sets;
import com.transretail.crm.core.entity.ApplicationConfig;
import com.transretail.crm.core.entity.enums.AppKey;
import com.transretail.crm.core.repo.ApplicationConfigRepo;
import com.transretail.crm.core.security.service.MD5Service;
import com.transretail.crm.core.security.service.impl.MD5ServiceImpl;
import com.transretail.crm.core.service.CodePropertiesService;
import com.transretail.crm.core.service.LookupService;
import com.transretail.crm.giftcard.dto.GiftCardInfo;
import com.transretail.crm.giftcard.dto.GiftCardTransactionInfo;
import com.transretail.crm.giftcard.entity.support.GiftCardInventoryStatus;
import com.transretail.crm.giftcard.entity.support.GiftCardSalesType;
import com.transretail.crm.giftcard.service.GiftCardAccountingService;
import com.transretail.crm.giftcard.service.GiftCardInventoryStockService;
import com.transretail.crm.giftcard.service.GiftCardManager;
import com.transretail.crm.web.BaseMvcConfig;
import com.transretail.crm.web.controller.AbstractMockMvcTest;
import com.transretail.crm.web.giftcard.controller.dto.PrepaidReloadRequest;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.Set;
import org.joda.time.LocalDate;
import org.joda.time.LocalDateTime;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.MessageSource;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Scope;
import org.springframework.http.MediaType;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.AnnotationConfigWebContextLoader;
import org.springframework.test.context.web.WebAppConfiguration;

import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

/**
 *
 * @author Monte Cillo Co (mco@exist.com)
 */
@RunWith(SpringJUnit4ClassRunner.class)
@WebAppConfiguration
@ContextConfiguration(loader = AnnotationConfigWebContextLoader.class)
public class GiftCardRestControllerTest extends AbstractMockMvcTest {

    private static final String ACTIVATE_URL = "/giftcard/activate";
    private static final ApplicationConfig DEFAULT_APP_CONFIG_KEY = new ApplicationConfig(AppKey.MD5_SECRET_KEY, "-secret-");
    private static final String VALID_CARD_NO = "14042410100000089200";
    private static final String ANY_CASHIER = "aCashierId";
    private static final String ANY_MERCHANT = "20001";
    private static final String ANY_TERMINAL = "aTerminalId";
    private static final String ANY_TRANSACTION_NO = "aTransactionNo";
    private static final String ANY_VALIDATION_KEY = "f317af8264579b3dceb9ecd7c4419090";

    @Autowired
    private GiftCardManager manager;
    @Autowired
    private MD5Service mD5Service;
    @Autowired
    private ApplicationConfigRepo configRepo;
    @Autowired
    private GiftCardRequest request;
    @Autowired
    private ObjectMapper mapper;

    @Before
    public void setup() {
        reset(manager, mD5Service); // in our case that uses spring singleton, we must reset the mock object to avoid unnecessary behavior in our test
        when(configRepo.findByKey(AppKey.MD5_SECRET_KEY)).thenReturn(DEFAULT_APP_CONFIG_KEY);
    }

    //<editor-fold defaultstate="collapsed" desc="Pre-activate section">
    @Test
    public void preActivate_shouldReturnStatusOk() throws Exception {
        when(mD5Service.validateMd5(anyString(), anyString())).thenReturn(Boolean.TRUE);
//        when(manager.preActivate(VALID_CARD_NO, ANY_MERCHANT))
//                .thenReturn(new GiftCardInfo().withCardNo(VALID_CARD_NO));

    }

    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="Activation Section">
    @Test
    public void activate_shouldReturnStatusOK() throws Exception {
        when(mD5Service.validateMd5(anyString(), anyString())).thenReturn(Boolean.TRUE);
        when(manager.activate(any(GiftCardTransactionInfo.class), any(Set.class),
                anyString(), eq(GiftCardSalesType.B2C), eq(false)))
                .thenReturn(ImmutableSet.of(new GiftCardInfo().withCardNo(VALID_CARD_NO)));

        request.setTransactionDate(LocalDateTime.now());
        request.setItems(ImmutableSet.of(new GiftCardRequest.CardItem(VALID_CARD_NO)));

        mockMvc.perform(post(ACTIVATE_URL).contentType(MediaType.APPLICATION_JSON)
                .content(convertObjectToJsonString(request)))
                .andDo(print()).andExpect(status().isOk());

        verify(mD5Service).validateMd5(anyString(), eq(ANY_VALIDATION_KEY));
        verify(manager).activate(any(GiftCardTransactionInfo.class), any(Set.class),
                anyString(), eq(GiftCardSalesType.B2C), eq(false));
    }

    @Test
    public void activate_shouldReturnAValidResponse() throws Exception {
        when(mD5Service.validateMd5(anyString(), anyString())).thenReturn(Boolean.TRUE);
        when(manager.activate(any(GiftCardTransactionInfo.class), any(Set.class), anyString(), eq(GiftCardSalesType.B2C), eq(false)))
                .thenReturn(ImmutableSet.of(new GiftCardInfo().withCardNo(VALID_CARD_NO)
                                .withFaceValue(500000.0)
                                .withCurrentBalance(500000.0)
                                .withAvailableBalance(500000.00).withPreviousAvailableBalance(BigDecimal.ZERO)
                                .withExpireDate(new LocalDate(2016, 05, 18))
                                .withProfile("12345678901", "GOLD")
                                .withStatus(GiftCardInventoryStatus.ACTIVATED)));

        request.setTransactionDate(LocalDateTime.now());
        request.setItems(ImmutableSet.of(new GiftCardRequest.CardItem(VALID_CARD_NO)));

        mockMvc.perform(post(ACTIVATE_URL).contentType(MediaType.APPLICATION_JSON)
                .content(convertObjectToJsonBytes(request)))
                .andDo(print()).andExpect(status().isOk())
                .andExpect(jsonPath("$.successful", is(true)))
                .andExpect(jsonPath("$.cardInfos", hasSize(1)))
                .andExpect(jsonPath("$.cardInfos[0].faceValue", is(500000.0)))
                .andExpect(jsonPath("$.cardInfos[0].previousAvailableBalance", is(0)))
                .andExpect(jsonPath("$.cardInfos[0].availableBalance", is(500000)))
                .andExpect(jsonPath("$.cardInfos[0].currentBalance", is(500000)))
                .andExpect(jsonPath("$.cardInfos[0].expireDate", is("2016-05-18")))
                .andExpect(jsonPath("$.cardInfos[0].profile.code", is("12345678901")))
                .andExpect(jsonPath("$.cardInfos[0].profile.description", is("GOLD")))
                .andExpect(jsonPath("$.cardInfos[0].cardNumber", is(VALID_CARD_NO)))
                .andExpect(jsonPath("$.cardInfos[0].status", is("ACTIVATED")));

        verify(mD5Service).validateMd5(anyString(), eq(ANY_VALIDATION_KEY));
        verify(manager).activate(any(GiftCardTransactionInfo.class), any(Set.class), anyString(), eq(GiftCardSalesType.B2C), eq(false));
    }

    @Test
    public void activate_shouldHandleInvalidValidationKey() throws IOException, Exception {
        when(mD5Service.validateMd5(anyString(), anyString())).thenReturn(Boolean.FALSE);

        request.setTransactionDate(LocalDateTime.now());
        request.setItems(ImmutableSet.of(new GiftCardRequest.CardItem(VALID_CARD_NO)));

        mockMvc.perform(post(ACTIVATE_URL)
                .contentType(MediaType.APPLICATION_JSON)
                .content(convertObjectToJsonBytes(request)))
                .andDo(print())
                .andExpect(status().isUnauthorized())
                .andExpect(jsonPath("$.status", is(401)))
                .andExpect(jsonPath("$.errorMessage", is(org.hamcrest.Matchers.notNullValue())));

        verify(mD5Service).validateMd5(anyString(), eq(ANY_VALIDATION_KEY));
        verifyZeroInteractions(manager);
    }

    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="Free Voucher Activation">
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="Redemption Section">
    @Test
    public void redeem_shouldReturnStatusOK() throws Exception {
        final double REDEEM_AMOUNT = 1000.00;
        when(mD5Service.validateMd5(anyString(), anyString())).thenReturn(Boolean.TRUE);
        when(manager.redeem(any(GiftCardTransactionInfo.class), eq(VALID_CARD_NO), eq(REDEEM_AMOUNT)))
                .thenReturn(ImmutableSet.of(new GiftCardInfo().withCardNo(VALID_CARD_NO)));

        GiftCardCommonRequest request = new GiftCardCommonRequest();
        request.setMerchantId(ANY_MERCHANT);
        request.setCashierId(ANY_CASHIER);
        request.setTerminalId(ANY_TERMINAL);
        request.setTransactionDate(LocalDateTime.now());
        request.setTransactionNo("200010290000103");
        request.setCardNo(VALID_CARD_NO);
        request.setRedemptionAmount(REDEEM_AMOUNT);
        request.setValidationKey(ANY_VALIDATION_KEY);

        mockMvc.perform(post("/giftcard/redeem").contentType(MediaType.APPLICATION_JSON)
                .content(convertObjectToJsonString(request)))
                .andDo(print()).andExpect(status().isOk());

        verify(mD5Service).validateMd5(anyString(), eq(ANY_VALIDATION_KEY));
        verify(manager).redeem(any(GiftCardTransactionInfo.class), eq(VALID_CARD_NO), eq(REDEEM_AMOUNT));
    }
    //</editor-fold>

    @Ignore // not a priority
    public void reloadTest() throws Exception {
        final String validationKey = "carrefour";
        final String merchantId = "test";
        final String terminalId = "test";
        final String cashierId = "test";
        final String transactionNo = "test";
        final LocalDateTime transactionDate = LocalDateTime.now();
        final String cardNo = "00000000000000000001";
        final long reloadAmount = 50000l;
        PrepaidReloadRequest prepaidRequest = new PrepaidReloadRequest();
        prepaidRequest.setValidationKey(validationKey);
        prepaidRequest.setMerchantId(merchantId);
        prepaidRequest.setTerminalId(terminalId);
        prepaidRequest.setCashierId(cashierId);
        prepaidRequest.setTransactionNo(transactionNo);
        prepaidRequest.setTransactionDate(transactionDate);
        prepaidRequest.setCardNo(cardNo);
        prepaidRequest.setReloadAmount(reloadAmount);

        when(mD5Service.validateMd5(prepaidRequest.getSignature(), prepaidRequest.getValidationKey())).thenReturn(Boolean.TRUE);

        Set<GiftCardInfo> cardInfos = Sets.newHashSet();
        cardInfos.add(new GiftCardInfo());
        // TODO: mco
//        when(manager.reload(argThat(new ArgumentMatcher<PrepaidReloadTransactionInfo>() {
//            @Override
//            public boolean matches(Object argument) {
//                PrepaidReloadTransactionInfo info = (PrepaidReloadTransactionInfo) argument;
//                return info.getMerchantId().equals(merchantId)
//                        && info.getCashierId().equals(cashierId)
//                        && info.getTransactionNo().equals(transactionNo)
//                        && info.getTransactionTime().equals(transactionDate)
//                        && info.getCardNo().equals(cardNo)
//                        && info.getReloadAmount().equals(reloadAmount);
//            }
//        }))).thenReturn(cardInfos);

        String responseAsString = mockMvc.perform(post("/giftcard/reload").contentType(MediaType.APPLICATION_JSON)
                .content(convertObjectToJsonString(prepaidRequest)))
                .andDo(print()).andExpect(status().isOk()).andReturn().getResponse().getContentAsString();
        GiftCardResponse response = mapper.readValue(responseAsString, GiftCardResponse.class);
        assertEquals(1, response.getCardInfos().size());
    }

    private byte[] convertObjectToJsonBytes(Object object) throws IOException {
        mapper.setSerializationInclusion(JsonInclude.Include.NON_NULL);

        return mapper.writeValueAsBytes(object);
    }

    private String convertObjectToJsonString(Object object) throws IOException {
        mapper.setSerializationInclusion(JsonInclude.Include.NON_NULL);
        return mapper.writerWithDefaultPrettyPrinter().writeValueAsString(object);
    }

    @Configuration
    static class Config extends BaseMvcConfig {

        @Bean
        public MessageSource messageSource() {
            return mock(MessageSource.class);
        }

        @Bean
        public CodePropertiesService codePropertiesService() {
            return mock(CodePropertiesService.class);
        }

        @Bean
        public LookupService lookupService() {
            return mock(LookupService.class);
        }

        @Bean
        public GiftCardRestController giftCardRestController() {
            return new GiftCardRestController();
        }

        @Bean
        public GiftCardManager giftCardManager() {
            return mock(GiftCardManager.class);
        }

        @Bean
        public ApplicationConfigRepo applicationConfigRepo() {
            return mock(ApplicationConfigRepo.class);
        }

        @Bean
        public MD5Service mD5Service() {
            return spy(new MD5ServiceImpl());
        }

        @Bean
        @Scope(value = BeanDefinition.SCOPE_PROTOTYPE)
        public GiftCardRequest giftCardRequest() {
            GiftCardRequest giftCardRequest = new GiftCardRequest();
            giftCardRequest.setCashierId(ANY_CASHIER);
            giftCardRequest.setMerchantId(ANY_MERCHANT);
            giftCardRequest.setTerminalId(ANY_TERMINAL);
            giftCardRequest.setTransactionNo(ANY_TRANSACTION_NO);
            giftCardRequest.setValidationKey(ANY_VALIDATION_KEY);
            return giftCardRequest;
        }

        @Bean
        public GiftCardAccountingService cardAccountingService() {
            return mock(GiftCardAccountingService.class);
        }

        @Bean
        public GiftCardInventoryStockService cardInventoryStockService() {
            return mock(GiftCardInventoryStockService.class);
        }

    }
}
