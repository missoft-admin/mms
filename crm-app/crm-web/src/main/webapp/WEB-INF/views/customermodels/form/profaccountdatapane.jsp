<%@ include file="../../common/taglibs.jsp" %>


<fieldset class="form-horizontal container-fluid">
	<legend>
		<spring:message code="label_field_logindetails" />
	</legend>
	<div class="control-group">
		<spring:message
			code="label_com_transretail_crm_entity_customermodel_username"
			var="username" />
		<label for="username" class="control-label"> <b
			class="required hasEmail">*</b>
		<c:out value="${username} " />
		</label>
		<div class="controls">
			<input type="text" name="username" class="form-control"
				placeholder="${username}" />
		</div>
	</div>
	<div class="control-group">
		<spring:message
			code="label_com_transretail_crm_entity_customermodel_password"
			var="password" />
		<label for="password" class="control-label"> <b
			class="required hasEmail">*</b>
		<c:out value="${password}" />
		</label>
		<div class="controls">
			<input type="password" name="password" class="form-control"
				placeholder="${password}" />
		</div>
	</div>
	<div>
		<input type="hidden" name="passwordCheck" />
	</div>
	<div class="control-group">
		<spring:message
			code="label_com_transretail_crm_entity_customermodel_pin" var="pin" />
		<label for="pin" class="control-label"><b class="required">*</b>
		<c:out value="${pin}" /> </label>
		<div class="controls">
			<form:password id="pin" path="pin" maxlength="6"
				cssClass="form-control intInput" placeholder="${pin}" />
		</div>
	</div>
  
    <div class="control-group">
      <label for="accountId" class="control-label col-sm-2"> <spring:message
          code="label_com_transretail_crm_entity_customermodel_accountnumber" />
      </label>
      <div class="controls">
        <span class="accountId label col-sm-10"> <spring:message
            code="msg_value_to_be_generated" /> <![CDATA[&nbsp;]]>
        </span>
      </div>
    </div>
    <div class="control-group">
      <label for="accountId" class="control-label col-sm-2"> <spring:message
          code="label_com_transretail_crm_entity_customermodel_memberbarcode" />
      </label>
      <div class="controls">
        <span class="label col-sm-10" id="memberBarcode"><![CDATA[&nbsp;]]>
        </span>
      </div>
    </div>
    
    <div id="parentIdGroup" class="control-group hidden">
      <label for="parent" class="control-label"> <spring:message
          code="label_com_transretail_crm_entity_customermodel_parent" />
      </label>
      <div class="controls">
        <form:input path="parent" cssClass="hidden" />
      </div>
    </div>
    
    <div id="memberTypeStatic" class="control-group">
        <label class="control-label"> <spring:message
            code="label_com_transretail_crm_entity_customermodel_membertype" />
        </label>
        <div class="controls">
          <span id="contentMemberType" class="label"></span>
        </div>
      </div>
      <div id="memberTypeSelect" class="control-group memberTypeSelect">
        <div class="contentInfo">
          <label for="memberType" class="control-label col-sm-2"><spring:message
              code="label_com_transretail_crm_entity_customermodel_membertype" />
          </label>
          <div class="controls" id="mainPaneMemberType">
            <form:select items="${memberType}" itemValue="code"
              itemLabel="description" path="memberType" multiple="false"
              cssClass="form-control" />
          </div>
        </div>
      </div>
      
      <div class="employeeDetails" id="accountEnabled">
        <div class="form-group">
          <label for="status" class="control-label"></label>
          <div class="checkbox controls">
            <input type="checkbox" name="enabled" /> <label
              class="control-label"><spring:message
                code="label_com_transretail_crm_entity_customermodel_portal_enabled" /></label>
          </div>
        </div>
      </div>
      <div class="control-group">
      	<label for="accountId" class="control-label col-sm-2"> <spring:message
            code="label_member_created_by" />
        </label>
        <div class="controls">
          <span id="createdBy" class="label"></span>
        </div>
      </div>
      <div class="control-group">
        <label for="accountId" class="control-label col-sm-2"> <spring:message
            code="label_member_registered_date" />
        </label>
        <div class="controls">
          <span id="registeredDate" class="label"></span>
        </div>
      </div>
      <div class="control-group">
        <label for="accountId" class="control-label col-sm-2"> <spring:message
            code="label_member_loyalty_card_issue_date" />
        </label>
        <div class="controls">
          <span id="loyaltyCardIssued" class="label"></span>
        </div>
      </div>
      <div class="control-group">
        <label for="accountId" class="control-label col-sm-2"> <spring:message
            code="label_member_loyalty_card_expiry_date" />
        </label>
        <div class="controls">
          <span id="loyaltyCardExpire" class="label"></span>
        </div>
      </div>
    
</fieldset>

<div class="clearfix"></div>
