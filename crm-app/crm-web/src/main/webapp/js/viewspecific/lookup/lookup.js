var FORM_ID = "detailForm";
var FORM_CONTAINER = "#" + FORM_ID;
var CONTAINER_ERROR = "#contentError > div";
var CONTENT_ERROR = "#contentError";

$(document).ready(function() {
	$(FORM_CONTAINER).find( CONTENT_ERROR ).hide();
	
	var headerCode = $("#headerCode").data("headerCode");
	$("#headerCode").val(headerCode);
	
	
	
	$("#button_add").click(function(e) {
		e.preventDefault();
		var theParentForm = $("form[id='" + FORM_ID + "']");
		var actionParam = $(theParentForm).data("headerCode");
		if($(theParentForm).attr("method") == "PUT") {
			actionParam = "update/" + actionParam;
		}
		$.ajax({
			type 	: "POST",
			url 	: $(theParentForm).attr("action") + actionParam,
			dataType: 'json',
			data 	: $(theParentForm).serialize(),
			        
			success : function(inResponse) {
				processResults(inResponse);
			},
			error : function(jqXHR, textStatus, errorThrown) {
				$( CONTENT_ERROR ).html("Error: " + errorThrown);
			}
		});
	});
	
	$("#button_reset").click(function(e) {
		e.preventDefault();
		resetForm();
	});
	
	$(".statusContainer").hide();
});


function resetForm() {
	var theParentForm = $("form[id='" + FORM_ID + "']");
	$(theParentForm).trigger("reset");
	$(theParentForm).attr("method", "POST");
	$(theParentForm).find("#detail_code").removeAttr("readonly");
	$(".statusContainer").hide();
    $('#oldCode' ).val('');
}

function processResults(inResponse) {
	if (inResponse.success) {
		$(FORM_CONTAINER).find( CONTENT_ERROR ).hide();
		reloadTable($("form[id='" + FORM_ID + "']").data("headerCode"));
	} 
	else {
		errorInfo = "";
		for (i = 0; i < inResponse.result.length; i++) {
			errorInfo += "<br>" + (i + 1) + ". "
					+ ( inResponse.result[i].code != undefined ? 
							inResponse.result[i].code : inResponse.result[i]);
		}
		$(FORM_CONTAINER).find( CONTAINER_ERROR ).html("Please correct following errors: " + errorInfo);
		$(FORM_CONTAINER).find( CONTENT_ERROR ).show();
	}
}

function showDetails( theHeader, theContextPath, theQueryString ) {
	var theUrl = theContextPath + "/lookup/" + theHeader.toLowerCase() + "?" + theQueryString;
	window.location = theUrl;
}

function createDetail( theHeader, theContextPath, theQueryString ) {
	
}

function editDetail (theCode, theDesc, theStatus ) {
	var theParentForm = $("form[id='" + FORM_ID + "']");
	$(theParentForm).find("#detail_code").val(theCode).attr("readonly", "readonly");
	$(theParentForm).find("#detail_description").val(theDesc);
	$(theParentForm).find("#detail_status").val(theStatus);
	$(theParentForm).attr("method", "PUT");
    $( '#oldCode', theParentForm ).val( theCode );
	$(".statusContainer").show();
}