package com.transretail.crm.core.service.impl;


import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.IteratorUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.mysema.query.jpa.impl.JPAQuery;
import com.mysema.query.jpa.impl.JPAUpdateClause;
import com.mysema.query.types.Projections;
import com.mysema.query.types.expr.BooleanExpression;
import com.transretail.crm.common.service.dto.response.NameIdPairDto;
import com.transretail.crm.common.service.dto.response.ResultList;
import com.transretail.crm.common.service.util.SpringDataPagingUtil;
import com.transretail.crm.core.dto.ServiceDto;
import com.transretail.crm.core.dto.UserDto;
import com.transretail.crm.core.dto.UserSearchDto;
import com.transretail.crm.core.entity.QUserModel;
import com.transretail.crm.core.entity.QUserPermission;
import com.transretail.crm.core.entity.QUserRolePermission;
import com.transretail.crm.core.entity.UserModel;
import com.transretail.crm.core.entity.UserPermission;
import com.transretail.crm.core.entity.UserRoleModel;
import com.transretail.crm.core.entity.UserRolePermission;
import com.transretail.crm.core.entity.enums.SvcReturnType;
import com.transretail.crm.core.entity.lookup.Store;
import com.transretail.crm.core.repo.UserPermissionRepo;
import com.transretail.crm.core.repo.UserRepo;
import com.transretail.crm.core.repo.UserRolePermissionRepo;
import com.transretail.crm.core.repo.UserRoleRepo;
import com.transretail.crm.core.security.util.UserUtil;
import com.transretail.crm.core.service.CodePropertiesService;
import com.transretail.crm.core.service.StoreService;
import com.transretail.crm.core.service.UserService;
import com.transretail.crm.logging.annotation.Loggable;


@Service("userService")
@Transactional
public class UserServiceImpl implements UserService {
    @PersistenceContext
    private EntityManager entityManager;
    @Autowired
    private UserRepo userRepo;
    @Autowired
    private UserRoleRepo userRoleRepo;
    @Autowired
    private UserRolePermissionRepo userRolePermissionRepo;
    @Autowired
    private UserPermissionRepo userPermissionRepo;
    @Autowired
    private StoreService storeService;
    @Autowired
    CodePropertiesService codePropertiesService;
    @Autowired
    private PasswordEncoder passwordEncoder;

    public UserModel updateUserModel(UserModel userModel) {
        // To be safe, passwords cannot be updated at this method
        UserModel oldUser = userRepo.findByUsername(userModel.getUsername());
        userModel.setPassword(oldUser.getPassword());
        return userRepo.save(userModel);
    }

    @Loggable
    public void saveUserModel(UserModel userModel) {
    	if(userRepo.findByUsername(userModel.getUsername()) != null) {
    		throw new DuplicateKeyException("Username already exists.");
    	}
        userModel.setPassword(passwordEncoder.encode(userModel.getPassword()));
        userRepo.save(userModel);
    }

    @Override
    public void saveUserDto(UserDto inUser) {
    	if ( null != inUser ) {
    		UserModel theModel = inUser.toModel();
        	saveUserModel( theModel );
    	}
    }

    @Override
    public void updateUserDto(UserDto inUser) {
    	if ( null != inUser ) {
    		UserModel theModel = userRepo.findOne( inUser.getId() );
    		theModel = inUser.toModel( theModel );
    		userRepo.save( theModel );
    	}
    }

    @Override
    public UserDto findUserDto(Long id) {
    	if( null == id ) {
    		return null;
    	}
    	UserModel theModel = userRepo.findOne(id);
    	if ( null != theModel ) {
            return new UserDto( theModel );
    	}
    	return null;
    }

    @Loggable
    public List<UserModel> findUserModelEntries(int firstResult, int maxResults) {
        return userRepo.findAll(new org.springframework.data.domain.PageRequest(firstResult / maxResults, maxResults)).getContent();
    }

    public List<UserModel> findAllUserModels() {
        return userRepo.findAll();
    }

    public UserModel findUserModel(Long id) {
        return userRepo.findOne(id);
    }

    public UserModel findUserModel(String inUsername) {
        return userRepo.findByUsername(inUsername);
    }
    
    @Override
    public UserModel findUserModelIgnoreCase(String inUsername) {
        return userRepo.findByUsernameIgnoreCase(inUsername);
    }

    public void deleteUserModel(UserModel userModel) {
        userRepo.delete(userModel);
    }

    @Loggable
    public long countAllUserModels() {
        return userRepo.count();
    }


    @Override
    public ServiceDto retrieveCssUsers() {

    	ServiceDto theSvcResp = new ServiceDto();

        List<UserModel> theCssUsers = userRepo.findByRoleCode("ROLE_CSS");

        if ( null != theCssUsers ) {
    		theSvcResp.setReturnCode( SvcReturnType.SUCCESS );
    		theSvcResp.setReturnObj( theCssUsers );
    	}
    	
    	return theSvcResp;

    }
    
    @Override
    public List<UserModel> retrieveHrsUsers() {
    	return userRepo.findByRoleCode(  "ROLE_HRS" );
    }

    @Override
    public ServiceDto searchUser( UserModel inUser ) {

    	ServiceDto theSvcResp = new ServiceDto();

    	List<UserModel> theUsers = userRepo.searchUsers( inUser );

    	theSvcResp.setReturnObj( theUsers );
    	theSvcResp.setReturnCode( SvcReturnType.SUCCESS );
    	return theSvcResp;
    }
    
    @Override
    public ServiceDto searchUser( UserModel inUser, Integer inPage, Integer inSize ) {

    	ServiceDto theSvcResp = new ServiceDto();

    	List<UserModel> theUsers = userRepo.searchUsers( inUser );
    	if ( null == theUsers ) {
    		theSvcResp.setReturnCode( SvcReturnType.ERROR );
    		return theSvcResp;
    	}

    	
     	Page<UserModel> thePage = new PageImpl<UserModel>( 
     			theUsers, new PageRequest( inPage,  inSize ), theUsers.size() );

    	Object[] theRespObj = new Object[2];
    	theRespObj[0] = thePage.getTotalPages();
    	theRespObj[1] = thePage.getContent().
    			subList( inPage * inSize, 
    					inPage * inSize + inSize < theUsers.size() ?
    							inPage * inSize + inSize : theUsers.size() );

    	theSvcResp.setReturnObj( theRespObj );
    	theSvcResp.setReturnCode( SvcReturnType.SUCCESS );
    	return theSvcResp;
    }

    @Override
    public ResultList<UserDto> searchUser( UserSearchDto theSearchDto ) {
        Pageable pageable = SpringDataPagingUtil.INSTANCE.createPageable(theSearchDto.getPagination());
        BooleanExpression filter = theSearchDto.createSearchExpression();
        Page<UserModel> page = filter != null ? userRepo.findAll(filter, pageable) : userRepo.findAll(pageable);
        return new ResultList<UserDto>( UserDto.toDtoList( page.getContent() ), page.getTotalElements(), page.getNumber(), page.getSize() );
    }
    
    @Override
    public List<UserModel> findUsersByRoleCode(String roleCode) {
    	return userRepo.findByRoleCode( roleCode );
    }

    @Override
    public List<UserRoleModel> findUserRoleModel(String code) {
    	return userRoleRepo.findByCode(code);
    }

    @Override
    public void changePassword(String username, String newPassword) {
        QUserModel qUserModel = QUserModel.userModel;
        JPAUpdateClause updateClause = new JPAUpdateClause(entityManager, qUserModel);
        updateClause.set(qUserModel.password, passwordEncoder.encode(newPassword));
        updateClause.where(qUserModel.username.eq(username));
        updateClause.execute();
    }
    
    @Override
    public boolean currentPasswordMatch(String currentPassword) {
    	UserModel user = userRepo.findByUsername(UserUtil.getCurrentUser().getUsername());
    	return passwordEncoder.matches(currentPassword, user.getPassword());
    }

    @SuppressWarnings("unchecked")
	@Override
    public List<UserDto> findCsByStoreCode( String storeCode, String currentlyAssigned ) {
    	List<UserModel> users = new ArrayList<UserModel>();

    	QUserModel qUser = QUserModel.userModel;
    	QUserRolePermission qRolePermission = QUserRolePermission.userRolePermission;
    	QUserPermission qPermission = QUserPermission.userPermission;

    	List<UserRolePermission> userRolePermissions = new ArrayList<UserRolePermission>();
    	Iterable<UserPermission> perms = userPermissionRepo.findAll( 
    			qPermission.id.equalsIgnoreCase( codePropertiesService.getPermissionComplaintsMgmt() ) );
    	if ( null != perms ) {
        	List<UserPermission> permissions = IteratorUtils.toList( perms.iterator() );
        	Iterable<UserRolePermission> userRolePerms = userRolePermissionRepo
        			.findAll( qRolePermission.permId.in( permissions ) );
        	if ( null != userRolePerms ) {
            	userRolePermissions = IteratorUtils.toList( userRolePerms.iterator() );
        	}
    	}

    	Iterable<UserModel> itUsers = null;
    	for ( UserRolePermission userRolePermission : userRolePermissions ) {
    		itUsers = userRepo.findAll( 
        			BooleanExpression.allOf( qUser.rolePermissions.contains( userRolePermission ), 
        				StringUtils.isNotBlank( storeCode )? 
        						qUser.storeCode.equalsIgnoreCase( storeCode ) : null ) );
        	if ( null != itUsers ) {
        		CollectionUtils.addAll( users, itUsers.iterator() );
        	}
    	}

    	if ( CollectionUtils.isNotEmpty( users ) ) {
        	itUsers = userRepo.findAll( BooleanExpression.allOf( 
        			qUser.in( users ), 
        			StringUtils.isNotBlank( currentlyAssigned )? qUser.username.notEqualsIgnoreCase( currentlyAssigned ) : null ), 
        			new PageRequest( 0, users.size(), new Sort( new Sort.Order( Sort.Direction.DESC, "storeCode" ) ) ) );
        	CollectionUtils.addAll( users, itUsers.iterator() );
        	List<UserDto> userDtos = ( null != itUsers )? UserDto.toDtoList( IteratorUtils.toList( itUsers.iterator() ) ) : null;
        	if ( CollectionUtils.isNotEmpty( userDtos ) ) {
        		for ( UserDto dto : userDtos ) {
        			if ( StringUtils.isNotBlank( dto.getStoreCode() ) ) {
        				Store store = storeService.getStoreByCode( dto.getStoreCode() );
        				dto.setStoreDesc( null != store? store.getName() : null );
        			}
        		}
        	}
        	return userDtos;
    	}
    	return null;
    }

    @Override
    public List<String> listSupervisorEmails( String storeCode, String supervisorCode ) {
        JPAQuery query = new JPAQuery( entityManager );
        QUserRolePermission qModel = QUserRolePermission.userRolePermission;

        return query.from( qModel )
        	.where( BooleanExpression.allOf( 
        				qModel.userId.email.isNotNull(),
        				qModel.userId.email.isNotEmpty(),
	        			StringUtils.isNotBlank( supervisorCode )? qModel.roleId.id.equalsIgnoreCase( supervisorCode ) : null,
	        			StringUtils.isNotBlank( storeCode )? qModel.userId.storeCode.equalsIgnoreCase( storeCode ) : null ) )
	        .list( qModel.userId.email );
    }
    
    @Override
    public List<String> listSupervisorEmails(String supervisorCode ) {
        JPAQuery query = new JPAQuery( entityManager );
        QUserRolePermission qModel = QUserRolePermission.userRolePermission;

        return query.from( qModel )
        	.where( BooleanExpression.allOf( 
        				qModel.userId.email.isNotNull(),
        				qModel.userId.email.isNotEmpty(),
	        			StringUtils.isNotBlank( supervisorCode )? qModel.roleId.id.equalsIgnoreCase( supervisorCode ) : null) )
	        			.distinct()
	        .list( qModel.userId.email );
    }

    @Override
    public NameIdPairDto getUserExhibition(Long id) {
    	QUserModel qUser = QUserModel.userModel;
    	return new JPAQuery(entityManager).from(qUser).where(qUser.id.eq(id)).singleResult(Projections.constructor(NameIdPairDto.class, 
    				qUser.id.stringValue(),
    				qUser.b2bLocation));
    }
    @Override
    public void saveExhibition(NameIdPairDto form) {
    	UserModel user = userRepo.findOne(Long.valueOf((String) form.getId()));
    	user.setB2bLocation(form.getName());
    	userRepo.save(user);
    }
}

