create table CRM_SALES_DEPT_ALLOC (
        id number(19,0) not null,
        CREATED_BY varchar2(255 char),
        CREATED_DATETIME timestamp,
        LAST_UPDATED_BY varchar2(255 char),
        LAST_UPDATED_DATETIME timestamp,
        ALLOC_AMT number(19,2),
        DEPT varchar2(15 char),
        SALES_ORDER number(19,0),
        STATUS varchar2(255 char),
        primary key (id)
    );


    alter table CRM_SALES_DEPT_ALLOC 
        add constraint FK_ody5gh0l92myq50f1gs6sf9c9 
        foreign key (DEPT) 
        references CRM_REF_LOOKUP_DTL;

    alter table CRM_SALES_DEPT_ALLOC 
        add constraint FK_njbl9c8iduude6mo3ind5bwgq 
        foreign key (SALES_ORDER) 
        references CRM_SALES_ORD;