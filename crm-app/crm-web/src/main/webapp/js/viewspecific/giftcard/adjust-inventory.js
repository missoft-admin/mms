$(document).ready(function() {
  var CONTAINER_ERROR = "#contentError > div";
  var CONTENT_ERROR = "#contentError";

  $byRangeDialog = $('#byRangeDialog');
  $byRangeForm = $('#byRangeForm');
  $byBarcodeDialog = $('#byBarcodeDialog');
  $byBarcodeForm = $('#byBarcodeForm');
  $summaryDialog = $('#summaryDialog');
  $historyDialog = $("#summary-list-dialog");

  $historyDialog.modal({
    show: false,
    keyboard: false,
    backdrop: "static"
  }).on('hidden.bs.modal', function() {
    $historyDialog.trigger("reset");
    $historyDialog.find(CONTENT_ERROR).hide();
    $historyDialog.find(CONTAINER_ERROR).empty();
  }).css({
    "width": "400px"
  });
  
  $byBarcodeDialog.modal({
    show: false,
    keyboard: false,
    backdrop: "static"
  }).on('hidden.bs.modal', function() {
    $byBarcodeForm.trigger("reset");
    $byBarcodeDialog.find(CONTENT_ERROR).hide();
    $byBarcodeDialog.find(CONTAINER_ERROR).empty();
  }).css({
    "width": "500px"
  });

  $byRangeDialog.modal({
    show: false,
    keyboard: false,
    backdrop: "static"
  }).on('hidden.bs.modal', function() {
    $byRangeForm.trigger("reset");
    $byRangeDialog.find(CONTENT_ERROR).hide();
//    $byRangeDialog.find(CONTAINER_ERROR).empty();
  }).css({
    "width": "550px"
  });

  $("input[name='startingSeries'], input[name='endingSeries'], input[name='barcode']").numberInput("type", "int");

  $('#barcode-interface').keypress(function(event) {
    var keycode = (event.keyCode ? event.keyCode : event.which);
    if (keycode == '13') {
      $barcodeDom = $("input[name='barcode']");
      var barcode = $barcodeDom.val();
      $.get(ctx + "/giftcard/inventory/adjustment/validate/" + barcode, function(data) {
	if (data.success) {
	  var barcodes = $('#barcodes_select');
	  barcodes.append($('<option>', {
	    text: barcode
	  }));
	  barcodes.change();
	  $('#barcode-interface').val("");
	  $byBarcodeDialog.find(CONTENT_ERROR).hide();
	  $byBarcodeDialog.find(CONTAINER_ERROR).empty();
	} else {
	  renderErrors($byBarcodeDialog, data);
	}
      });
    }
    event.stopPropagation();
  });

  $('#barcodes_select').change(function(event) {
    $('#countSubmit2').prop('disabled', $(this).has('option').length === 0);
  });

  function renderErrors(targetDialog, data) {
    errorInfo = "";
    for (i = 0; i < data.result.length; i++) {
      errorInfo += "<br>" + (i + 1) + ". "
	      + (data.result[i].code !== undefined ? data.result[i].code : data.result[i]);
    }
    targetDialog.find(CONTAINER_ERROR).html("Please correct following errors: " + errorInfo);
    targetDialog.find(CONTENT_ERROR).show('slow');
  }

  $('#countStart').click(function(e) {
    e.preventDefault();
    $byRangeDialog.modal('show');
  });

  $('#byRange').click(function(e) {
    e.preventDefault();
    $byRangeDialog.modal('show');
  });

  $('#byBarcode').click(function(e) {
    e.preventDefault();
    $byBarcodeDialog.modal("show");
  });

  $('#countSubmit').click(function(e) {
    e.preventDefault();
    var btn = $(this);
    btn.button('loading');

    $.post($byRangeForm.attr("action"), $byRangeForm.serialize())
	    .done(function(data) {
	      if (data.success) {
		reloadTable();

		$byRangeForm.trigger("reset");
		$('#barcodes_select').change();
		$byRangeDialog.find(CONTENT_ERROR).hide();
		$byRangeDialog.find(CONTAINER_ERROR).empty();
	      } else {
		renderErrors($byRangeDialog, data);
	      }
	      btn.button('reset');
	    })
	    .fail(function(data) {
	      $(CONTENT_ERROR).html("Error: " + data);
	    });


  });

  $("#countSubmit2").click(function(e) {
    e.preventDefault();
    var btn = $(this);
    btn.button('loading');

    $('#barcodes_select option').prop('selected', true);
    $.post($byBarcodeForm.attr("action"), $byBarcodeForm.serialize())
	    .done(function(data) {
	      if (data.success) {
		reloadTable();
		//manual reseting select box(actually remove all options)
		$byBarcodeForm.trigger("reset");
		$('#barcodes_select')[0].options.length = 0;
		$byBarcodeDialog.find(CONTENT_ERROR).hide();
		$byBarcodeDialog.find(CONTAINER_ERROR).empty();
		var label = btn.data('label');
		btn.data('loading-text', label).button('loading');
		$('#barcodes_select').trigger('change');
	      } else {
		btn.button('reset');
		renderErrors($byBarcodeDialog, data);
	      }
	    })
	    .fail(function(data) {
	      $(CONTENT_ERROR).html("Error: " + data);
	    });
  });

  $("#countEnd").click(function(e) {
    e.preventDefault();
    $summaryDialog.modal("show");
    reloadSummaryTable();
  });

  $("#showOlder").click(function (e) {
    e.preventDefault();
    $historyDialog.modal("show");
    reloadHistoryTable();
  });
  
  $("input[name='startingSeries'], input[name='endingSeries']")/*.change(computeQuantity)*/.change(getCardType);

  function computeQuantity() {
    var starting = $("input[name='startingSeries']").val();
    var ending = $("input[name='endingSeries']").val();
    var quantity = "";
    if (starting.length === 16 && ending.length === 16)
      quantity = +ending - +starting + 1;
    $("input[name='quantity']").val(quantity);
  }

  function getCardType() {
    var starting = $("input[name='startingSeries']").val();
    var ending = $("input[name='endingSeries']").val();

    $productProfile = $("input[name='productProfile']");
    $productProfileName = $("input[name='productProfileName']");
    $inventoryCount = $("input[name='quantity']");

    if (starting.length === 16 && ending.length === 16) {
      $.get(ctx + "/giftcard/inventory/adjustment/cardtype/" + starting + "/" + ending, function (data) {
	if (data.success) {
	  $productProfile.val(data.result['productProfileCode']);
	  $productProfileName.val(data.result['productProfileName']);
	  $inventoryCount.val(data.result[ 'inventoryCount' ]);
	  $byRangeDialog.find(CONTENT_ERROR).hide();
	  $byRangeDialog.find(CONTAINER_ERROR).empty();
	} else {
	  $productProfile.val("");
	  $productProfileName.val("");
	  renderErrors($byRangeDialog, data);
	}
      });
    } else {
      $productProfile.val("");
      $productProfileName.val("");
    }
  }

  $("#summaryProcess").click(function (e) {
    e.preventDefault();
    $.post($(this).data("url"), function (data) {
      if (data.success) {
	$summaryDialog.modal("hide");
	location.reload();
      } else {
	alert("An error occurred on the server. Please try again.");
      }
    });
  });

  $("#countCancel").click(hideProcessBtn);
  $("#countCancel2").click(function (e) {
    resetBarcodeForm();
    hideProcessBtn();
  });

  function resetBarcodeForm() {
    $byBarcodeForm.trigger("reset");
    $('#barcodes_select')[0].options.length = 0;
    $byBarcodeDialog.find(CONTENT_ERROR).hide();
    $byBarcodeDialog.find(CONTAINER_ERROR).empty();
    $('#countSubmit2').prop('disabled', true);
  }

  // TODO: report-{reportType} is dynamic, it is good if it can manually attach an event listener
  $("#report-PDF").click(function (e) {
    e.preventDefault();
    var url = $(this).data("target") + $("select[name='summaryDate']").val();
    console.log('Pdf url:' + url);
    window.open(url, '_blank', 'toolbar=0,location=0,menubar=0');
  });

  $("#report-EXCEL").click(function (e) {
    e.preventDefault();
    var url = $(this).data("target") + $("select[name='summaryDate']").val();
    window.location.href = url;
  });

  $("#summaryPrint").click(function (e) {
    e.preventDefault();
    var url = $("#reportType option:selected").data("url");
    var selectedReportType = $("select[name='reportType']").val();
    if (selectedReportType == "pdf")
      window.open(url, '_blank', 'toolbar=0,location=0,menubar=0');
    else if (selectedReportType == "excel")
      window.location.href = url;
  });
});

