package com.transretail.crm.loyalty.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.transretail.crm.core.entity.lookup.LookupDetail;
import com.transretail.crm.core.entity.support.CustomAuditableEntity;

@Table(name = "CRM_LC_PHYSICAL_COUNT")
@Entity
public class LoyaltyPhysicalCount extends CustomAuditableEntity<Long> implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Column(name = "LOCATION")
	private String location;
	@ManyToOne
	@JoinColumn(name = "CARD_TYPE")
	private LookupDetail cardType;
	@Column(name = "STARTING_SERIES")
	private String startingSeries;
	@Column(name = "ENDING_SERIES")
	private String endingSeries;
	@Column(name = "QUANTITY")
	private Long quantity;
	@Column(name = "BOX_NO")
	private String boxNo;
	
	public String getLocation() {
		return location;
	}
	public void setLocation(String location) {
		this.location = location;
	}
	public LookupDetail getCardType() {
		return cardType;
	}
	public void setCardType(LookupDetail cardType) {
		this.cardType = cardType;
	}
	public String getStartingSeries() {
		return startingSeries;
	}
	public void setStartingSeries(String startingSeries) {
		this.startingSeries = startingSeries;
	}
	public String getEndingSeries() {
		return endingSeries;
	}
	public void setEndingSeries(String endingSeries) {
		this.endingSeries = endingSeries;
	}
	public Long getQuantity() {
		return quantity;
	}
	public void setQuantity(Long quantity) {
		this.quantity = quantity;
	}
	public String getBoxNo() {
		return boxNo;
	}
	public void setBoxNo(String boxNo) {
		this.boxNo = boxNo;
	}
	
	
}
