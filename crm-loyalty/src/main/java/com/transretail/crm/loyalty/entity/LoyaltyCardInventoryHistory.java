package com.transretail.crm.loyalty.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.Parameter;
import org.hibernate.annotations.Type;
import org.joda.time.DateTime;
import org.joda.time.LocalDate;
import org.springframework.data.annotation.CreatedBy;
import org.springframework.data.annotation.CreatedDate;

import com.mysema.query.annotations.QueryInit;
import com.transretail.crm.core.entity.lookup.LookupDetail;
import com.transretail.crm.core.entity.support.CustomIdAuditableEntity;

@Table(name = "CRM_LC_INVENTORY_HIST")
@Entity
public class LoyaltyCardInventoryHistory extends CustomIdAuditableEntity<String> implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -1316334838791553284L;

	@ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "INVENTORY_NO", nullable = false)
	@QueryInit({"product.*", "status.*"})
	private LoyaltyCardInventory inventory;
	
	@ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "STATUS", nullable = false)
	private LookupDetail status;
	
    @Column(name = "LOCATION", nullable = false)
	private String location;
    
    @Column(name = "TRANSFER_TO")
   	private String transferTo;
	
    @Column(name = "ORIGIN")
	private String origin;
	
	@Column(name = "CREATED_DATE")
    @Type(type = "org.jadira.usertype.dateandtime.joda.PersistentLocalDate")
    private LocalDate createdDate; //for day-to-day tracking
	
	@Column(name = "POS_TXN_NO")
	private String transactionNo;
	@Column(name = "ANNUAL_FEE")
	private Double annualFee;
	@Column(name = "REPLACEMENT_FEE")
	private Double replacementFee;
	
	@Column(name = "TRACK")
    @Type(type = "yes_no")
	private Boolean track = Boolean.TRUE;
	
	public LoyaltyCardInventoryHistory() {}

	public LoyaltyCardInventoryHistory(String id,
			LoyaltyCardInventory inventory, LookupDetail status, String location, String transferTo, String origin, Boolean track, String createdUser, DateTime created) {
		super();
        setCreateUser(createdUser);
		setCreated(created);
		setId(id);
		this.inventory = inventory;
		this.status = status;
		this.location = location;
		this.origin = origin;
		this.transferTo = transferTo;
		this.track = track;
		this.createdDate = new LocalDate();
	}
	
	public LoyaltyCardInventoryHistory(String id,
			LoyaltyCardInventory inventory, LookupDetail status, String location,
			String origin, String transactionNo, Double annualFee, Double replacementFee, Boolean track, String createdUser, DateTime created) {
		super();
        setCreateUser(createdUser);
        setCreated(created);
		setId(id);
		this.inventory = inventory;
		this.status = status;
		this.location = location;
		this.origin = origin;
		this.transactionNo = transactionNo;
		this.annualFee = annualFee;
		this.replacementFee = replacementFee;
		this.track = track;
	}



	public LoyaltyCardInventory getInventory() {
		return inventory;
	}

	public void setInventory(LoyaltyCardInventory inventory) {
		this.inventory = inventory;
	}

	public LookupDetail getStatus() {
		return status;
	}

	public void setStatus(LookupDetail status) {
		this.status = status;
	}

	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		this.location = location;
	}

	public String getOrigin() {
		return origin;
	}

	public void setOrigin(String origin) {
		this.origin = origin;
	}

	public LocalDate getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(LocalDate createdDate) {
		this.createdDate = createdDate;
	}

	public String getTransactionNo() {
		return transactionNo;
	}

	public void setTransactionNo(String transactionNo) {
		this.transactionNo = transactionNo;
	}

	public Double getAnnualFee() {
		return annualFee;
	}

	public void setAnnualFee(Double annualFee) {
		this.annualFee = annualFee;
	}

	public Double getReplacementFee() {
		return replacementFee;
	}

	public void setReplacementFee(Double replacementFee) {
		this.replacementFee = replacementFee;
	}

	public Boolean getTrack() {
		return track;
	}

	public void setTrack(Boolean track) {
		this.track = track;
	}

	public String getTransferTo() {
		return transferTo;
	}

	public void setTransferTo(String transferTo) {
		this.transferTo = transferTo;
	}
	
	
	
	
}
