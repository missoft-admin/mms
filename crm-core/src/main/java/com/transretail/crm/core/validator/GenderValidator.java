package com.transretail.crm.core.validator;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import org.springframework.util.StringUtils;

/**
 * @author Allan G. Ramirez (agramirez@exist.com)
 */
public class GenderValidator implements ConstraintValidator<Gender, String> {
    private String[] validValues;

    public void initialize(Gender constraintAnnotation) {
        validValues = constraintAnnotation.values();
    }

    public boolean isValid(String object, ConstraintValidatorContext constraintContext) {
        if (!StringUtils.hasText(object)) {
            return true;
        }
        for (String value : validValues) {
            if (value.equalsIgnoreCase(object)) {
                return true;
            }
        }
        return false;
    }

}