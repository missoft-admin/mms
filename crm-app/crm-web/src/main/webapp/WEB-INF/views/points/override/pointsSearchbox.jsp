<%@ include file="/WEB-INF/views/common/taglibs.jsp" %>

    <c:set var="ENCTYPE" value="application/x-www-form-urlencoded"/>
    <c:if test="${multipart}"><c:set var="ENCTYPE" value="multipart/form-data"/></c:if>
    
    <c:set var="MODEL_ATTRIBUTE" value="${searchModelAttribute}"/>
    <c:url var="PATH" value="${form_url}?page=1&amp;size=10"/>
    <spring:message var="BUTTON_LABEL" code="button_search" text="Search"/>
    
    
    <form:form action="${PATH}" method="POST" id="${MODEL_ATTRIBUTE}" modelAttribute="${MODEL_ATTRIBUTE}" enctype="${ENCTYPE}">
          <div class="form-inline form-search">
          	<label class=""><h5><spring:message code="entity_search" />:</h5></label>
              <select id="search_memberCriteria" style="margin: 6px;">
                  <c:forEach var="criteria" items="${memberCriteria}">
                      <option value="${criteria.value}">${criteria.description}</option>
                  </c:forEach>
              </select>
              <div class="input-append">
                  <input id="search_memberField" class="search-query memberSearchField" />
                  <input id="search_submit" type="submit" value="${BUTTON_LABEL}" class="btn btn-primary" />
              </div>
              <input type="button" id="pointsSearchClearButton" value="<spring:message code="label_clear" />" class="btn btn-primary"/>
          </div>   
          <div class="form-inline form-search">
              <label class=""><h5><spring:message code="label_filter" />:</h5></label>
              <select class="pointsSearchDropdown memberSearchField" data-name="status" id="status" style="margin: 6px;">
                      <option value=""><spring:message code="label_com_transretail_crm_entity_customermodel_status" /></option>
                  <c:forEach var="item" items="${status}">
                      <option value="${item}">${item}</option>
                  </c:forEach>
              </select>
              <select class="pointsSearchDropdown memberSearchField" data-name="memberType" id="memberType" style="margin: 6px;">
                      <option value=""><spring:message code="label_com_transretail_crm_entity_customermodel_membertype" /></option>
                  <c:forEach var="item" items="${memberType}">
                      <option value="${item.code}">${item.description}</option>
                  </c:forEach>
              </select>
              <select class="pointsSearchDropdown memberSearchField" data-name="registeredStore" id="registeredStore" style="margin: 6px;">
                      <option value=""><spring:message code="label_com_transretail_crm_entity_customermodel_registeredstore" /></option>
                  <c:forEach var="item" items="${preferredStore}">
                      <option value="${item.code}">${item.codeAndName}</option>
                  </c:forEach>
              </select>
          </div>
    </form:form>
    
    
    <style type="text/css">
        #search_submit {
            border: 1px;
            padding-bottom: 4px;
            padding-top: 4px;
            padding-left: 12px;
            padding-right: 12px;
            margin-top: 0px;
            margin-right: 0px;
        }
        
        #search_memberField {
            padding-bottom: 4px;
        }
    </style>

