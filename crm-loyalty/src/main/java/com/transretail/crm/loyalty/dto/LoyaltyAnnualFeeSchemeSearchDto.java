package com.transretail.crm.loyalty.dto;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;

import com.mysema.query.types.expr.BooleanExpression;
import com.transretail.crm.common.service.dto.request.AbstractSearchFormDto;
import com.transretail.crm.loyalty.entity.QLoyaltyAnnualFeeScheme;

public class LoyaltyAnnualFeeSchemeSearchDto extends AbstractSearchFormDto {
	
	private String company;
	private String cardType;
	
	
	public String getCompany() {
		return company;
	}
	public void setCompany(String company) {
		this.company = company;
	}
	public String getCardType() {
		return cardType;
	}
	public void setCardType(String cardType) {
		this.cardType = cardType;
	}
	@Override
	public BooleanExpression createSearchExpression() {
		QLoyaltyAnnualFeeScheme qScheme = QLoyaltyAnnualFeeScheme.loyaltyAnnualFeeScheme;
		List<BooleanExpression> expressions = new ArrayList<BooleanExpression>();
		if(StringUtils.isNotBlank(company))
			expressions.add(qScheme.company.code.eq(company));
		if(StringUtils.isNotBlank(cardType))
			expressions.add(qScheme.cardType.code.eq(cardType));
 		return BooleanExpression.allOf(expressions.toArray(new BooleanExpression[expressions.size()]));
	}
	
	
}
