package com.transretail.crm.core.entity.embeddable;

import java.util.Date;
import java.util.List;

import javax.persistence.CollectionTable;
import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Embeddable;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.Past;

import org.apache.commons.lang3.BooleanUtils;
import org.hibernate.annotations.Type;
import org.joda.time.Instant;
import org.joda.time.Years;
import org.springframework.format.annotation.DateTimeFormat;

import com.transretail.crm.core.entity.lookup.LookupDetail;

@Embeddable
public class CustomerProfile {
    @Past
    @Temporal(TemporalType.TIMESTAMP)
    @DateTimeFormat(style = "M-")
    @Column(name = "BIRTHDATE", nullable = false)
    private Date birthdate;

	@JoinColumn(name="GENDER")
	@OneToOne
	private LookupDetail gender;
	
	@JoinColumn(name="NATIONALITY")
	@OneToOne
	private LookupDetail nationality;
	
	@JoinColumn(name="MARITAL_STATUS")
	@OneToOne
	private LookupDetail maritalStatus;
	
	@JoinColumn(name="TITLE")
	@OneToOne
	private LookupDetail title;
	
	@Column(name="POSTAL_CODE")
	private String postalCode;
	
	@JoinColumn(name="EDUCATION")
	@ManyToOne
	private LookupDetail education;
	
	@JoinColumn(name="OCCUPATION")
	@ManyToOne
	private LookupDetail occupation;
	
	@JoinColumn(name="HOUSEHOLD_INCOME")
	@ManyToOne
	private LookupDetail householdIncome;
	
	@JoinColumn(name="PERSONAL_INCOME")
	@ManyToOne
	private LookupDetail personalIncome;
	
	@JoinColumn(name="FAMILY_SIZE")
	@ManyToOne
	private LookupDetail familySize;
	
	@Column(name="CHILDREN_COUNT")
	private Integer children;
	
	@Column(name="DOMESTIC_HELPERS")
	private Integer domesticHelpers;
	
	@Column(name="CARS_OWNED")
	private Integer carsOwned;

	@Column(name="VEHICLES_OWNED")
	private String vehiclesOwned;
	
	@JoinColumn(name="RELIGION")
	@ManyToOne
	private LookupDetail religion;
	
	@ElementCollection
	@CollectionTable(name="CRM_AGE_OF_CHILDREN", joinColumns=@JoinColumn(name="ACCOUNT_ID"))
	@Column(name="AGE")
	private List<Integer> ageOfChildren;
	
	@Column(name="PLACE_OF_BIRTH")
	private String placeOfBirth;
	
	@Column(name="FAX_NO")
	private String faxNo;
	
	@Column(name="INSURANCE_OWNERSHIP", length = 1)
    @Type(type = "yes_no")
	private Boolean insuranceOwnership = Boolean.FALSE;
	
	@Column(name="CREDIT_CARD_OWNERSHIP", length = 1)
    @Type(type = "yes_no")
	private Boolean creditCardOwnership = Boolean.FALSE;
	
	@Column(name="CLOSEST_STORE")
	private String closestStore;
	
	@Column(name="INTERESTS")
	private String interests;
	
//	@ManyToMany
//	@JoinTable(name="CRM_MEMBER_BANKS",
//			joinColumns={@JoinColumn(name="ACCOUNT_ID", referencedColumnName="ACCOUNT_ID")},
//			inverseJoinColumns={@JoinColumn(name="CODE", referencedColumnName="CODE")})
	@Column(name="BANKS")
	private String banks;

    @Column(name = "NAME_NATIVE", length = 120)
    private String nameNative;
    
    @ManyToOne
    @JoinColumn(name = "PREFERED_LANGUAGE")
    private LookupDetail preferredLanguage;

    public LookupDetail getNationality() {
		return nationality;
	}

	public void setNationality(LookupDetail nationality) {
		this.nationality = nationality;
	}

	public LookupDetail getMaritalStatus() {
		return maritalStatus;
	}

	public void setMaritalStatus(LookupDetail maritalStatus) {
		this.maritalStatus = maritalStatus;
	}

	public String getPostalCode() {
		return postalCode;
	}

	public void setPostalCode(String postalCode) {
		this.postalCode = postalCode;
	}

	public LookupDetail getTitle() {
		return title;
	}

	public void setTitle(LookupDetail title) {
		this.title = title;
	}

	public LookupDetail getGender() {
		return gender;
	}

	public void setGender(LookupDetail gender) {
		this.gender = gender;
	}

	public LookupDetail getEducation() {
		return education;
	}

	public void setEducation(LookupDetail education) {
		this.education = education;
	}

	public LookupDetail getOccupation() {
		return occupation;
	}

	public void setOccupation(LookupDetail occupation) {
		this.occupation = occupation;
	}

	public LookupDetail getHouseholdIncome() {
		return householdIncome;
	}

	public void setHouseholdIncome(LookupDetail householdIncome) {
		this.householdIncome = householdIncome;
	}

	public LookupDetail getPersonalIncome() {
		return personalIncome;
	}

	public void setPersonalIncome(LookupDetail personalIncome) {
		this.personalIncome = personalIncome;
	}

	public LookupDetail getFamilySize() {
		return familySize;
	}

	public void setFamilySize(LookupDetail familySize) {
		this.familySize = familySize;
	}

	public Integer getChildren() {
		return children;
	}

	public void setChildren(Integer children) {
		this.children = children;
	}

	public Integer getDomesticHelpers() {
		return domesticHelpers;
	}

	public void setDomesticHelpers(Integer domesticHelpers) {
		this.domesticHelpers = domesticHelpers;
	}

	public Integer getCarsOwned() {
		return carsOwned;
	}

	public void setCarsOwned(Integer carsOwned) {
		this.carsOwned = carsOwned;
	}

	public String getVehiclesOwned() {
		return vehiclesOwned;
	}

	public void setVehiclesOwned(String vehiclesOwned) {
		this.vehiclesOwned = vehiclesOwned;
	}

    public String getNameNative() {
        return nameNative;
    }

    public void setNameNative(String nameNative) {
        this.nameNative = nameNative;
    }
    
    public LookupDetail getPreferredLanguage() {
        return preferredLanguage;
    }

    public void setPreferredLanguage(LookupDetail preferredLanguage) {
        this.preferredLanguage = preferredLanguage;
    }
    
    public Date getBirthdate() {
		return birthdate;
	}

	public void setBirthdate(Date birthdate) {
		this.birthdate = birthdate;
	}
	
	public Integer getAge() {
		if(birthdate != null ) {
	    	Years age = Years.yearsBetween(new Instant(birthdate.getTime()), new Instant());
	    	return age.getYears();
		}
		return 0;
    }
    
    public void setAge(Integer age) {
    	// do nothing, age based on birthdate
    }

	public LookupDetail getReligion() {
		return religion;
	}

	public void setReligion(LookupDetail religion) {
		this.religion = religion;
	}

	public List<Integer> getAgeOfChildren() {
		return ageOfChildren;
	}

	public void setAgeOfChildren(List<Integer> ageOfChildren) {
		this.ageOfChildren = ageOfChildren;
	}

	public String getPlaceOfBirth() {
		return placeOfBirth;
	}

	public void setPlaceOfBirth(String placeOfBirth) {
		this.placeOfBirth = placeOfBirth;
	}

	public String getFaxNo() {
		return faxNo;
	}

	public void setFaxNo(String faxNo) {
		this.faxNo = faxNo;
	}

	public Boolean getInsuranceOwnership() {
		return insuranceOwnership;
	}

	public void setInsuranceOwnership(Boolean insuranceOwnership) {
		this.insuranceOwnership = BooleanUtils.toBoolean(insuranceOwnership);
	}

	public Boolean getCreditCardOwnership() {
		return creditCardOwnership;
	}

	public void setCreditCardOwnership(Boolean creditCardOwnership) {
		this.creditCardOwnership = BooleanUtils.toBoolean(creditCardOwnership);
	}

	public String getClosestStore() {
		return closestStore;
	}

	public void setClosestStore(String closestStore) {
		this.closestStore = closestStore;
	}

	public String getBanks() {
		return banks;
	}

	public void setBanks(String banks) {
		this.banks = banks;
	}

	public String getInterests() {
		return interests;
	}

	public void setInterests(String interests) {
		this.interests = interests;
	}
}
