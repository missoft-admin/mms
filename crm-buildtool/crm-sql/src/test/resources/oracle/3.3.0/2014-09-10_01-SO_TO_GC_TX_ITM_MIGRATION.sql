INSERT INTO CRM_GC_TRANSACTION_ITEM
  (
    ID,
    TRANSACTION_ID,
    CARD_NO,
    CURR_AMOUNT,
    TXN_AMOUNT
  )
WITH
  DATA AS
  (
    SELECT
      GT.ID          AS TRANSACTION_ID,
      GCI.BARCODE    AS CARD_NO,
      GCI.FACE_VALUE AS CURR_AMOUNT,
      GCI.FACE_VALUE AS TXN_AMOUNT
    FROM
      CRM_SALES_ORD SO,
      CRM_GC_TRANSACTION GT,
      CRM_SALES_ORD_ALLOC SOA,
      CRM_SALES_ORD_ITEM SOI,
      CRM_GC_INVENTORY GCI
    WHERE
      GT.SALES_ORDER_ID = SO.ID
    AND SO.ID           = SOI.SALES_ORDER
    AND SOI.ID          =SOA.SO_ITEM
    AND GCI.ID          =SOA.GC_INVENTORY
    ORDER BY
      GT.ID ASC
  )
SELECT
  HIBERNATE_SEQUENCE.NEXTVAL,
  TRANSACTION_ID,
  CARD_NO,
  CURR_AMOUNT,
  TXN_AMOUNT
FROM
  DATA;