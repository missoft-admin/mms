package com.transretail.crm.common.reporting.service;

import java.io.File;
import java.io.OutputStream;

import com.transretail.crm.common.reporting.exception.ReportException;
import com.transretail.crm.common.reporting.jasper.JRProcessor;

/**
 * @author Allan G. Ramirez (agramirez@exist.com)
 */
public interface ReportService {
    void exportPdfReport(JRProcessor processor, OutputStream os) throws ReportException;
    
    void exportExcelReport(JRProcessor processor, OutputStream os) throws ReportException;

    void exportPdfReport(JRProcessor processor, File output) throws ReportException;
}
