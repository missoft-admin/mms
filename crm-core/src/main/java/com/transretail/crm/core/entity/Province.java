package com.transretail.crm.core.entity;

import java.io.Serializable;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.OneToMany;
import javax.persistence.OrderBy;
import javax.persistence.Table;

import org.hibernate.envers.Audited;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.transretail.crm.core.entity.support.CustomAuditableEntity;

@Audited
@Entity
@Table(name = "CRM_PROVINCE")
public class Province extends CustomAuditableEntity<Long> implements Serializable {
	
	
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 2909149914307978729L;

	@Column(name = "NAME")
	private String name;
	
	@JsonIgnore
	@OneToMany(mappedBy = "province", orphanRemoval = true, fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	@OrderBy("name ASC")
	private List<City> cities;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	public List<City> getCities() {
		return cities;
	}

	public void setCities(List<City> cities) {
		this.cities = cities;
	}
	

}
