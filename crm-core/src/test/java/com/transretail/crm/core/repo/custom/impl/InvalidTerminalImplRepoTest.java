package com.transretail.crm.core.repo.custom.impl;

import com.transretail.crm.core.entity.InvalidTerminalModel;
import com.transretail.crm.core.repo.InvalidTerminalRepo;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

/**
 *
 * @author Monte Cillo Co (mco@exist.com)
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = "classpath*:/META-INF/spring/applicationContext*.xml")
public class InvalidTerminalImplRepoTest {

    @Autowired
    private InvalidTerminalRepo repo;

    @Before
    public void setup() {
	InvalidTerminalModel invalidTerminalModel = new InvalidTerminalModel();
	invalidTerminalModel.setStoreCode("store_code");
	invalidTerminalModel.setTerminalId("890-123-678990");
	repo.save(invalidTerminalModel);
    }

    @Test
    public void terminalIdExists() {
	assertThat(repo.exists("890-123-678990"), is(true));
    }

    @After
    public void cleanup() {
	repo.deleteAll();
    }
}
