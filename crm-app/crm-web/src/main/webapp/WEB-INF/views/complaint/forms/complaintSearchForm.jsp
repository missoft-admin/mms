<%@include file="../../common/taglibs.jsp" %>


<div id="content_complaintSearchForm" class="form-horizontal block-search well">

  <form id="complaintSearchForm" method="POST">
  <div class="control-group">
    <div class="">

      <div class="main-block search-block">
        <fieldset class="row-fluid">
          <label><spring:message code="label_searchfield" /></label>
          <select id="searchField" class="input-medium">
            <option/>
            <c:forEach var="item" items="${searchFields}"><option value="${item}"><spring:message code="complaint_lbl_search_${item}" /></option></c:forEach>
          </select>
        </fieldset>
        <input type="text" id="searchValue" placeholder="<spring:message code="label_searchvalue" />" class="input-small" />
        <%-- <input id="searchBtn" class="btn btn-primary" type="submit" value="<spring:message code="label_search" />" /> --%>
      </div>
      <div class="main-block filter-block">
        <fieldset class="row-fluid">
          <label><spring:message code="complaint_store" /></label>
          <select name="storeCode" class="input-medium">
            <option/>
            <c:forEach var="item" items="${stores}"><option value="${item.code}">${item.code} - ${item.name}</option></c:forEach>
          </select>
        </fieldset>
        <fieldset class="row-fluid">
          <label><spring:message code="complaint_status" /></label>
          <select name="status" class="input-medium text-uppercase" >
            <option/>
            <c:forEach var="item" items="${stats}"><option value="${item.code}">${item.desc}</option></c:forEach>
          </select>
        </fieldset>

        <input type="text" name="filedBy" placeholder="<spring:message code="complaint_lbl_filedby" />" class="input-small" />
        <input type="text" name="assignedTo" placeholder="<spring:message code="complaint_assignedto" />" class="input-small" />

        <div class="input-daterange" id="duration">
          <fieldset class="row-fluid">
            <label><spring:message code="label_from" /></label>
            <input type="text" name="dateFiledFrom" id="dateFiledFrom" class="input-small" />
          </fieldset>
          <fieldset class="row-fluid">
            <label><spring:message code="label_to" /></label>
            <input type="text" name="dateFiledTo" id="dateFiledTo" class="input-small" />
          </fieldset>
        </div>

        <%-- <input id="filterBtn" class="btn btn-primary" type="submit" value="<spring:message code="label_filter" />" /> --%>
        <input id="searchBtn" class="btn btn-primary" type="submit" value="<spring:message code="label_search" />" />
        <input id="resetBtn" type="button" class="btn" value="<spring:message code="label_reset" />" />
      </div>
      
    </div>
  </div>
  </form>


<script src='<c:url value="/js/bootstrap/bootstrap-datepicker.js"/>' type='text/javascript'><![CDATA[&nbsp; ]]></script>
<script src="<spring:url value="/js/jquery/jquery.toObject.js" />" ></script>
<script type="text/javascript">
var ComplaintSearchForm = null;

$(document).ready( function() {

    var $form = $( "#complaintSearchForm" );
    var $duration = $form.find( "#duration" );
    var $durationFrom = $form.find( "#dateFiledFrom" );
    var $durationTo = $form.find( "#dateFiledTo" );
    var $search = $form.find( "#searchBtn" );
    var $filter = $form.find( "#filterBtn" );
    var $reset = $form.find( "#resetBtn" );
    var $searchField = $form.find( "#searchField" );
    var $searchValue = $form.find( "#searchValue" );

    initFormFields();

    function initFormFields() {
        $duration.datepicker({ autoclose : true, format : "dd M yyyy" });
        $form.submit( submit );

        $searchField.change( function(e) {
        	  $searchValue.attr( "name", $( this ).val() );
        });

        $reset.click( function(e) {
        	  ComplaintCommon.reset( $form );
        	  $form.submit();
        });
    }

    function submit(e) {
        e.preventDefault();
        var filterForm = $(this).toObject( { mode : 'first', skipEmpty : true } );
        filterForm.dateFiledFrom = ( $durationFrom.val() )? $durationFrom.datepicker( "getDate" ).getTime() : null;
        filterForm.dateFiledTo = ( $durationTo.val() )? $durationTo.datepicker( "getDate" ).getTime() : null;
        $search.prop( "disabled", true );

        ComplaintList.filterTable( filterForm, function() { $search.prop( "disabled", false ); } );
    }

});
</script>
<link href="<c:url value="/css/bootstrap/datepicker.css" />" rel="stylesheet" />
<style type="text/css">
  <!-- 
  /* .control-label { width: 50px !important; } 
  .controls { margin-left: 80px !important; } 
  .controls div { margin-bottom: 5px; } */
  select { margin-right: 5px; }
  .main-block { margin-bottom: 10px; }
  .row-fluid { width: 100px !important; display: inline-block; display: inline; }
  .input-daterange { display: inline-block; display: inline; }
  -->
</style>


</div>