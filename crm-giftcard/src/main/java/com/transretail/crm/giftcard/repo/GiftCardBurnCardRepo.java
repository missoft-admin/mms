package com.transretail.crm.giftcard.repo;

import org.springframework.stereotype.Repository;

import com.transretail.crm.common.repo.CrmQueryDslPredicateExecutor;
import com.transretail.crm.giftcard.entity.GiftCardBurnCard;
import com.transretail.crm.giftcard.repo.custom.GiftCardBurnCardRepoCustom;

/**
 * @author ftopico
 */
@Repository
public interface GiftCardBurnCardRepo extends CrmQueryDslPredicateExecutor<GiftCardBurnCard, Long>, GiftCardBurnCardRepoCustom {

}
