    create table CRM_PROXY_INFO (
        id number(19,0) not null,
        CRM_PROXY_REST_PRFX varchar2(100 char) not null,
        IP varchar2(50 char) not null,
        STORE_NAME varchar2(255 char) not null,
        TOMCAT_PORT number(10,0) not null,
        primary key (id)
    );

    alter table CRM_PROXY_INFO
        add constraint UK_8egxtwfn1w00x5h7129xmxh0j unique (IP);

    alter table CRM_PROXY_INFO
        add constraint UK_gtg3w5qqbkfvdkkgqqn9ohmqn unique (STORE_NAME);

INSERT INTO CRM_USER_PERMISSION(CODE, DESCRIPTION, STATUS,ROLE_CATEGORY) VALUES ('VIEW_STORE_LOGS','View Store Logs', 'ACTIVE','ROLE_ADMIN');