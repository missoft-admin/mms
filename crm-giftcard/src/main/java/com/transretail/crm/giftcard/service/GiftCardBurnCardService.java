package com.transretail.crm.giftcard.service;

import com.transretail.crm.common.service.dto.response.ResultList;
import com.transretail.crm.common.service.exception.MessageSourceResolvableException;
import com.transretail.crm.giftcard.dto.GiftCardBurnCardDto;
import com.transretail.crm.giftcard.dto.GiftCardBurnCardSearchDto;
import com.transretail.crm.giftcard.dto.GiftCardInventorySeriesSearchDto;

/**
 * @author ftopico
 */
public interface GiftCardBurnCardService {

    ResultList<GiftCardBurnCardDto> searchGiftCardBurnCard(GiftCardBurnCardSearchDto searchDto);

    String createBurnCardNumber();
    
    void saveBurncardRequest(GiftCardBurnCardDto dto) throws MessageSourceResolvableException;

    GiftCardBurnCardDto getBurnCardRequest(String burnNo);

    void rejectRequest(GiftCardBurnCardDto dto);
    
    void burnCards(GiftCardBurnCardDto dto);

    void isCardSubmitted(String seriesNo, String burnNo) throws MessageSourceResolvableException;
}
