package com.transretail.crm.giftcard.dto;

import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;

import com.transretail.crm.giftcard.entity.GiftCardInventory;

/**
 * @author Allan G. Ramirez (agramirez@exist.com)
 */
public class GiftCardSeriesDto {
    private Long seriesNo;
    private DateTime orderDate;
    private int batchNo;
    private int faceValue;
    private int seqNo;

    /**
     * Series number should conform to format: yyMMddbbfsssssss<br>
     * where:
     * <ul>
     * <li>yy - last 2 digits of year in orderDate</li>
     * <li>MM - month in orderDate</li>
     * <li>dd - day in orderDate</li>
     * <li>bb - batch number</li>
     * <li>f - face value per 100k</li>
     * <li>sssssss - series digits</li>
     * </ul>
     *
     * @param seriesNo series number
     * @see com.transretail.crm.giftcard.entity.GiftCardInventory#series
     */
    public GiftCardSeriesDto(String seriesNo) {
        this.seriesNo = Long.valueOf(seriesNo);
        String yyMMdd = seriesNo.substring(0, 6);
        String bb = seriesNo.substring(6, 8);
        String f = seriesNo.substring(8, 9);
        String seqDigits = seriesNo.substring(9);

        orderDate = DateTimeFormat.forPattern(GiftCardInventory.SERIES_DATETIME_FORMAT).parseDateTime(yyMMdd);
        batchNo = Integer.valueOf(bb);
        faceValue = Integer.valueOf(f) * GiftCardInventory.FACE_VALUE_MULTIPLIER;
        seqNo = Integer.valueOf(seqDigits);
    }

    public DateTime getOrderDate() {
        return orderDate;
    }

    public int getBatchNo() {
        return batchNo;
    }

    public int getFaceValue() {
        return faceValue;
    }

    public int getSeqNo() {
        return seqNo;
    }

    public Long getSeriesNo() {
        return seriesNo;
    }
}
