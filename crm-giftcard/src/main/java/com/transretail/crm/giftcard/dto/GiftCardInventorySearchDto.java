package com.transretail.crm.giftcard.dto;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.joda.time.LocalDate;

import com.mysema.query.types.expr.BooleanExpression;
import com.transretail.crm.giftcard.entity.QGiftCardInventory;
import com.transretail.crm.giftcard.entity.support.GiftCardInventoryStatus;

/**
 * @author Allan G. Ramirez (agramirez@exist.com)
 */
public class GiftCardInventorySearchDto extends GiftCardInventorySeriesSearchDto {
    private String productCode;
    private String productName;
    private String locationCode;
    private String allocateToCode;
    private String locationOrAllocateToCode;
    private String vendorFormalName;
    
    //Used for Expire and Balance Printing
    private GiftCardInventoryStatus status;
    private LocalDate expiryDateFrom;
    private LocalDate expiryDateTo;

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getLocationCode() {
        return locationCode;
    }

    public void setLocationCode(String locationCode) {
        this.locationCode = locationCode;
    }

    public String getAllocateToCode() {
        return allocateToCode;
    }

    public void setAllocateToCode(String allocateToCode) {
        this.allocateToCode = allocateToCode;
    }

    public String getLocationOrAllocateToCode() {
        return locationOrAllocateToCode;
    }

    public void setLocationOrAllocateToCode(String locationOrAllocateToCode) {
        this.locationOrAllocateToCode = locationOrAllocateToCode;
    }

    public String getProductCode() {
        return productCode;
    }

    public void setProductCode(String productCode) {
        this.productCode = productCode;
    }

    public GiftCardInventoryStatus getStatus() {
        return status;
    }

    public void setStatus(GiftCardInventoryStatus status) {
        this.status = status;
    }

    public LocalDate getExpiryDateFrom() {
        return expiryDateFrom;
    }

    public LocalDate getExpiryDateTo() {
        return expiryDateTo;
    }

    public void setExpiryDateFrom(LocalDate expiryDateFrom) {
        this.expiryDateFrom = expiryDateFrom;
    }

    public void setExpiryDateTo(LocalDate expiryDateTo) {
        this.expiryDateTo = expiryDateTo;
    }

    public String getVendorFormalName() {
        return vendorFormalName;
    }

    public void setVendorFormalName(String vendorFormalName) {
        this.vendorFormalName = vendorFormalName;
    }

    @Override
    public BooleanExpression createSearchExpression() {
        List<BooleanExpression> expressions = new ArrayList<BooleanExpression>();
        QGiftCardInventory qInventory = QGiftCardInventory.giftCardInventory;

        BooleanExpression seriesExpression = super.createSearchExpression();
        if (seriesExpression != null) {
            expressions.add(seriesExpression);
        }
        if (StringUtils.isNotBlank(productCode)) {
            expressions.add(qInventory.productCode.startsWithIgnoreCase(productCode));
        }
        if (StringUtils.isNotBlank(productName)) {
            expressions.add(qInventory.productName.startsWithIgnoreCase(productName));
        }
        if (StringUtils.isNotBlank(locationCode)) {
            expressions.add(qInventory.location.eq(locationCode));
        }
        if (StringUtils.isNotBlank(allocateToCode)) {
            expressions.add(qInventory.allocateTo.eq(allocateToCode));
        }
        if (StringUtils.isNotBlank(locationOrAllocateToCode)) {
            expressions.add(
                qInventory.location.eq(locationOrAllocateToCode).or(qInventory.allocateTo.eq(locationOrAllocateToCode)));
        }
        
        if (status != null) {
            expressions.add(qInventory.status.eq(status));
        }
        
        if (expiryDateFrom != null && expiryDateTo != null) {
            expressions.add(qInventory.expiryDate.between(expiryDateFrom, expiryDateTo));
        }
        
        if (vendorFormalName != null) {
            expressions.add(qInventory.order.vendor.formalName.eq(vendorFormalName));
        }
        return BooleanExpression.allOf(expressions.toArray(new BooleanExpression[expressions.size()]));
    }
}
