<%@ include file="../../common/taglibs.jsp" %>


<div id="content_approverList">

  <%-- <div id="form_approverSearchForm" class="form-horizontal block-horizontal pull-right" style="display: none;">
    <div id="approverSearchForm" class="block-horizontal"><input name="username" type="text" id="searchValue" /></div>
    <input type="button" id="approverSearchButton" value="<spring:message code="label_search" />" class="btn btn-primary"/>
  </div> --%>
  <div id="list_approver" 
    data-url="<c:url value="/points/override/approver/list" />"
    data-hdr-name="<spring:message code="points_prop_approver_name"/>"
    data-hdr-fname="<spring:message code="points_prop_approver_fname"/>"
    data-hdr-lname="<spring:message code="points_prop_approver_lname"/>"
    data-hdr-username="<spring:message code="points_prop_approver_username"/>" 
    data-hdr-amount="<spring:message code="points_prop_approver_maxpoints"/>" 
    class="el-block-02"></div>

  <div id="btn_editApprover" data-url="<c:url value="/points/override/approver/edit/" />" class="hide">
    <a href="#" title="<spring:message code="points_override_approver_update" />" class="btn pull-left icn-edit btn_editApprover tiptip" data-id="%ID%" title="<spring:message code="label_edit"/>"><spring:message code="label_edit"/></a>
  </div>
  <div id="btn_deleteApprover" data-url="<c:url value="/points/override/approver/delete/" />" data-msg-confirm="<spring:message code="entity_delete_confirm" />" class="hide">
    <a href="#" title="<spring:message code="points_override_approver_delete" />" class="btn btn-primary icn-delete pull-left btn_deleteApprover tiptip" data-id="%ID%" title="<spring:message code="label_delete"/>"><spring:message code="label_delete"/></a>
  </div>
  <jsp:include page="../../common/confirm.jsp" />


  <link href="<spring:url value="/css/dataTables.css" />" rel="stylesheet">
  <script src="<spring:url value="/js/jquery/jquery.dataTables.js" />"></script>
  <script src="<spring:url value="/js/jquery/jquery.dataTables.linkLengthMenu.js" />"></script>
  <script src="<spring:url value="/js/jquery/jquery.dataTables.customPaging.js" />"></script>
  <script src="<spring:url value="/js/jquery/ajax-datatable.js" />"></script>
  <script src="<spring:url value="/js/common/form2js.js" />"></script>
  <script src="<spring:url value="/js/common/json2.js" />"></script>
  <script src="<spring:url value="/js/jquery/jquery.toObject.js" />"></script>

  <script src="<spring:url value="/js/viewspecific/points/override/approverList.js" />"></script>
  <style><!--.el-width-x { width: 80px !important; }--></style>


</div>