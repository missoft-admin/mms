package com.transretail.crm.template.service.streaming.excel.handler.cell;

import org.apache.commons.lang3.StringUtils;

import com.transretail.crm.core.entity.MemberModel;
import com.transretail.crm.core.entity.embeddable.Npwp;
import com.transretail.crm.template.service.MemberTemplateParseException;

/**
 * @author Allan G. Ramirez (agramirez@exist.com)
 */
public class NpwpIdHandler implements CellHandler {
    @Override
    public void handle(MemberModel dto, String cellText) throws MemberTemplateParseException {
        if (dto.getNpwp() == null) {
            dto.setNpwp(new Npwp());
        }
        if(StringUtils.isNotBlank(cellText))
        	dto.getNpwp().setNpwpId(cellText);
        else 
        	dto.getNpwp().setNpwpId(Npwp.DEFAULT_NPWP_ID);
    }
}
