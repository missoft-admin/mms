package com.transretail.crm.core.service;

import java.io.InputStream;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.joda.time.LocalDate;

import com.transretail.crm.common.reporting.jasper.JRProcessor;
import com.transretail.crm.common.service.dto.rest.ReturnMessage;
import com.transretail.crm.core.dto.PointsConfigDto;
import com.transretail.crm.core.dto.PointsDto;
import com.transretail.crm.core.dto.PointsResultList;
import com.transretail.crm.core.dto.PointsSearchDto;
import com.transretail.crm.core.dto.PosTxItemDto;
import com.transretail.crm.core.dto.ServiceDto;
import com.transretail.crm.core.entity.MemberModel;
import com.transretail.crm.core.entity.PointsTxnModel;
import com.transretail.crm.core.entity.PosTxItem;
import com.transretail.crm.core.entity.enums.PointTxnType;
import com.transretail.crm.core.entity.lookup.LookupDetail;

public interface PointsTxnManagerService {

    public static final String PAYMENT_TYPE_KEY = "paymentType";
    public static final String AMOUNT_KEY = "amount";
    public static final String CARD_NUMBER_KEY = "cardNumber";



    boolean processPoints(PointsTxnModel pointsTxnModel, ReturnMessage returnMessage);

    Double retrieveTotalPoints(String accountId);
    
    Double retrieveTotalPoints(MemberModel member);
    
    List<PointsTxnModel> retrievePointsByTransactionNo(String transactionNo);
    
    List<PointsTxnModel> retrievePoints(MemberModel member);

    List<PointsTxnModel> retrievePointsByAcctId(String accountId);
    
    List<PointsTxnModel> retrievePoints(MemberModel member, Date fromDate, Date toDate);

    List<PointsTxnModel> retrievePoints(String accountId, Date fromDate, Date toDate);
    
    List<PointsTxnModel> retrievePoints(String accountId, int firstResult, int maxResult);


    List<PointsTxnModel>  retrievePoints(String transactionNo, PointTxnType inTxnType);


    PointsTxnModel savePointsTxnModel(PointsTxnModel model);

    PointsTxnModel retrievePointsById(String inId);
    
    long countPointsTransactions(String accountId);

    JRProcessor createPointsJrProcessor(MemberModel member, Date dateFrom, Date dateTo, InputStream logo);

	ServiceDto retrievePointsByTxnType(PointTxnType inTxnType);

	ServiceDto retrieveAdjustPointsForApprovalWithAuth();

	void deleteAdjustPoints(String inId);

	Double synchronizePointsTxn(MemberModel inMember);

    List<Map> retrieveValidPaymentTypes(String paymentType, String amount, String cardNumber, boolean checkFilters, ReturnMessage ret);

    PointsConfigDto getPointsConfig();
    
    void updatePointsConfig(Double pointValue, List<LookupDetail> suspendedTypes);
    
    Double getCurrencyAmountPerPoint();
    
    List<LookupDetail> getSuspendedMemberTypes();
    
    Integer getTransactionCount();

	PointsDto retrievePointsDtoById(String inId);

	PointsResultList searchPoints(PointsSearchDto theSearchDto);

	boolean savePointsDto(PointsDto inDto);
	
	List<PointsDto> getTransactionsToday(String accountId);
	
	List<PointsDto> getTransactionsByDate(String accountId, LocalDate date);

    boolean processReturnItems(String transactionNo, String newTransactionNo, Double amount, List<PosTxItemDto> itemDtoList, String store, ReturnMessage ret, boolean isRefund);

    List<PosTxItemDto>  removeReturnedItems(List<PosTxItem> orderItems, List<PosTxItemDto> returnedList);

    void expirePoints();
    
    void expirePoints(LocalDate expiryDate);
    
    PointsTxnModel expirePoints(MemberModel member, LocalDate expiryDate);
    
    void setDefaultValidPeriod(Integer validPeriodInMonths);
    
    Integer getDefaultValidPeriod();
    
    JRProcessor createExpirePointsJrProcessor(LocalDate startMonth, LocalDate endMonth, InputStream logo);

    JRProcessor createFutureExpirePointsJrProcessor(LocalDate startMonth, LocalDate endMonth, InputStream logo);

    PointsTxnModel expireAllPoints(String accountId);
    
    PointsTxnModel expireAllPoints(MemberModel member);

	PointsTxnModel expirePoints(MemberModel member,
			List<PointsTxnModel> pointsToExpireList, LocalDate expiryDate, boolean ind);

	Double retrieveTotal( PointTxnType type, String acctId );

	BigDecimal retrieveTotalPurchase(PointTxnType[] types, String acctId);

	List<Long> updateMemberPoints();
}
