package com.transretail.crm.core.entity.lookup;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

import org.hibernate.envers.Audited;

import com.transretail.crm.core.entity.support.AuditableEntity;

/**
 *
 */
@Audited
@Entity
@Table(name = "CRM_REF_LOOKUP_HDR")
public class LookupHeader extends AuditableEntity implements Serializable {
    private static final long serialVersionUID = 2495312673344330184L;
    @Id
    @Column(name = "CODE", length = 15)
    private String code;
    @Column(name = "DESCRIPTION", length = 100)
    private String description;
    
    public LookupHeader() {
    	
    }

    public LookupHeader(String code) {
        this(code, null);
    }

    public LookupHeader(String code, String description) {
        this.code = code;
        this.description = description;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
