package com.transretail.crm.core.service;

import com.transretail.crm.core.dto.CrmInStoreDto;
import com.transretail.crm.core.dto.CrmInStoreResultList;
import com.transretail.crm.core.dto.CrmInStoreSearchForm;
import com.transretail.crm.core.entity.CrmInStore;

/**
 * @author Allan G. Ramirez (agramirez@exist.com)
 */
public interface CrmInStoreService {

    CrmInStoreDto getCrmInStoreDto(String crmInStoreId);

    CrmInStoreResultList getCrmInStores(CrmInStoreSearchForm searchForm);

    CrmInStore save(CrmInStore dto);

    boolean isCodeExists(String code);

    boolean isNameExists(String excludeCode, String name);

    boolean isIpExists(String excludeCode, String ip);
}
