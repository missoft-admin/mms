$(document).ready( function() {

	var theErrorContainer = $( "#content_loyaltyAssignForm" ).find("#content_error");

	var $theDialog = $( "#content_loyaltyAssignForm" );
	var $theForm = $( "#loyaltyAssignForm" );
	var $theLoyaltyCardNo = $( "#loyaltyCardNo" );



	initFormFields();
	initDialog();
	initTempBarcodeSimulation();



	function initFormFields() {
		
		$("#saveButton").click(function() {
			$theForm.submit();
		});

		$theForm.submit( function(e) {
			e.preventDefault();
			$theLoyaltyCardNo.val( $( "#lbl_scanCard" ).val() );
			saveForm( $(this).attr("action") );
		});
		
		$("#freeChk").click(function() {
			var checked = this.checked;
			if(checked) {
				$("#annualFeeTxt").text("0");
				$("#annualFee").data("originalValue", $("#annualFee").val());
				$("#annualFee").val("0");
				
				$("#replacementFeeTxt").text("0");
				$("#replacementFee").data("originalValue", $("#replacementFee").val());
				$("#replacementFee").val("0");
				
				$(".hideFree").hide();
			} else {
				var annualFee = $("#annualFee").data("originalValue");
				$("#annualFee").val(annualFee);
				$("#annualFeeTxt").text(annualFee);
				var replacementFee = $("#replacementFee").data("originalValue");
				$("#replacementFee").val(replacementFee);
				$("#replacementFeeTxt").text(replacementFee);
				$(".hideFree").show();
			}
		});
	}

	function initDialog() {
		$theDialog.modal({});
		$theDialog.on( "hide", function() { theErrorContainer.hide(); });
	}

	function initTempBarcodeSimulation() {
		$( "#lbl_scanCard" ).change( function(e) {
			if ( $( this ).val() ) {
				$.ajax({
					cache 		: false, 
					dataType 	: "json",
					type 		: "POST", 
					url 		: $( this ).data( "url" ) + $( this ).val(),
					data		: $theForm.serialize(),
					error 		: function( jqXHR, textStatus, errorThrown ) {},
					success 	: function( inResponse ) {
						if ( inResponse.success ) {
							var result = inResponse.result;
							$theLoyaltyCardNo.val( result.barCode );
							$("#companyTxt").text(result.company);
							$("#cardTypeTxt").text(result.cardType);
							$("#expiryDateTxt").text(result.expiryDate);
							
							var annualFee = result.annualFee;
							var replacementFee = result.replacementFee;
							
							if($("#freeChk").is(":checked")) {
								$("#annualFeeTxt").text("0");
								$("#annualFee").data("originalValue", annualFee);
								$("#annualFee").val("0");
								
								$("#replacementFeeTxt").text("0");
								$("#replacementFee").data("originalValue", replacementFee);
								$("#replacementFee").val("0");
							} else {
								$("#annualFeeTxt").text(annualFee);
								$("#annualFee").val(annualFee);
								$("#replacementFeeTxt").text(replacementFee);
								$("#replacementFee").val(replacementFee);	
							} 
							theErrorContainer.hide();
						}
						else {
							$theLoyaltyCardNo.val("");
							var theErrorInfo = "";
							$.each( inResponse.result, function( key, value ) {
								theErrorInfo += ( ( key + 1 ) + ". " + value + "<br>" );
							});
							theErrorContainer.find( "div" ).html( theErrorInfo );
							theErrorContainer.show( "slow" );
						}
					}
				});
			}
		});
	}



	function saveForm( inUrl ) {
		$( "#content_loyaltyAssignForm" ).find( "#saveButton" ).prop( "disabled", true );
		$.ajax({
			cache 		: false, 
			dataType 	: "json",
			type 		: "POST", 
			url 		: inUrl,
			data		: $theForm.serialize(),
			error 		: function( jqXHR, textStatus, errorThrown ) { $( "#content_loyaltyAssignForm" ).find( "#saveButton" ).prop( "disabled", false ); },
			success 	: function( inResponse ) {
				if ( inResponse.success ) {
					$("#list_member").ajaxDataTable('search');
					$theDialog.modal( 'hide' );
					$theDialog.remove();
				}
				else {
					var theErrorInfo = "";
					$.each( inResponse.result, function( key, value ) {
						theErrorInfo += ( ( key + 1 ) + ". " + value.code + "<br>" );
					});
					theErrorContainer.find( "div" ).html( theErrorInfo );
					theErrorContainer.show( "slow" );
				}
				$( "#content_loyaltyAssignForm" ).find( "#saveButton" ).prop( "disabled", false );
			}
		});
	}

});



