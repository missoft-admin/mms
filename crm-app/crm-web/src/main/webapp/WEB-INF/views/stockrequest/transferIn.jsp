<%@include file="/WEB-INF/views/common/taglibs.jsp" %>

<style type="text/css">
<!--

  #addBtn.btn-small {
    margin-bottom: 5px;
  }
  table select, textarea, table input[type="text"], input[type="password"], input[type="datetime"], input[type="datetime-local"], input[type="date"], input[type="month"], input[type="time"], input[type="week"], input[type="number"], input[type="email"], input[type="url"], input[type="search"], input[type="tel"], input[type="color"], .uneditable-input {
      margin-bottom: 0px;
      margin-top: 0px;
  }
  
-->
</style>

<c:set var="ENCTYPE" value="application/x-www-form-urlencoded"/>

<div class="modal-dialog">
  <div class="modal-content">
    <div class="modal-header">
      <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
      <h4 class="modal-title"><spring:message code="gc.stock.label.transferin.title"/></h4>
  	</div>
  	<div class="modal-body">
      <c:url var="actionUrl" value="stockrequest/transferin/save"/>
      <form:form id="stockrequest" modelAttribute="stockrequest" action="${actionUrl}">
      
        <div id="contentError" class="hide alert alert-error">
          <button type="button" class="close" onclick="$(this).parent().hide();">&times;</button>
          <div><form:errors path="*"/></div>
        </div>
      
        <div class="contentMain well">
          <div class="row-fluid" style="margin-bottom: 10px">
            <form:hidden id="id" path="id"/>
            
            <div class="row-fluid">
              <div class="span4">
                <label><spring:message code="gc.stock.label.requestno" /></label>
                <form:input path="requestNo" readonly="true"/>
              </div>
              <div class="span4">
                <label><spring:message code="gc.stock.label.requestdate" /></label>
                <form:input path="requestDate" readonly="true"/>
              </div>
              <div class="span4">
                <label><spring:message code="gc.stock.label.requestedby" /></label>
                <form:input path="createUser" readonly="true"/>
              </div>
            </div>
            
            <div class="row-fluid">
              <div class="span4">
                <label><spring:message code="gc.stock.label.transferrefno" /></label>
                <form:input path="transferRefNo" id="transferRefNo" readonly="true"/>
              </div>
              <div class="span4">
                <label><spring:message code="gc.stock.label.receive.date" /></label>
                <form:input path="receiveDate" readonly="true"/>
              </div>
              <div class="span4">
                <label><spring:message code="gc.stock.label.receive.by" /></label>
                <form:input path="receiveBy" readonly="true"/>
              </div>
            </div>
          </div>
        </div>

        <div >
          <div class="pull-left" style="margin-right: 5px;">
            <button type="button" id="addProductBtn"
              class="btn btn-small">+</button>
            <br />
            <button type="button" id="removeProductBtn"
              class="btn btn-small">&ndash;</button>
          </div>
          <div class="pull-left" style="width: 90%;">
            <table id="stockTable"
              class="table table-condensed table-compressed table-striped">
              <thead>
                  <tr>
                    <th></th>
                    <th><b class="required">*</b> <spring:message code="gc.stock.label.quantity" /></th>
                    <th><b class="required">*</b> <spring:message code="gc.burncard.label.starting.series" /></th>
                    <th><spring:message code="gc.burncard.label.ending.series" /></th>
                    <th><spring:message code="gc.stock.label.product" /></th>
                    <th><spring:message code="gc.stock.label.box.no" /></th>
                  </tr>
              </thead>
              <tbody>
                <c:forEach var="receiveDtos" items="${stockrequest.receiveDtos}" varStatus="status" >
                <tr data-index="${status.index}">
                  <td>
                    <input type="checkbox" class="productRemoveFlag" />
                  </td>
                  <td>
                    <input type="text" class="quantity input-small numeric" value="${receiveDtos.quantity}" name="receiveDtos[${status.index}].quantity"/>
                  </td>
                  <td>
                    <input type="text" class="startingSeries input-medium numeric" maxlength="16" value="${receiveDtos.startingSeries}" name="receiveDtos[${status.index}].startingSeries"/>
                  </td>
                  <td>
                    <input type="text" class="endingSeries input-medium numeric" maxlength="16" value="${receiveDtos.endingSeries}" name="receiveDtos[${status.index}].endingSeries" readonly="readonly"/>
                  </td>
                  <td>
                    <input id="productCode[${status.index}]" type="hidden" value="${receiveDtos.productCode}" name="receiveDtos[${status.index}].productCode"/>
                    <input id="productDesc[${status.index}]" type="text" class="input-medium productDesc" value="${receiveDtos.productDesc}" name="receiveDtos[${status.index}].productDesc"/>
                  </td>
                  <td>
                    <input id="boxNo[${status.index}]" type="text" class="input-mini numeric" value="${receiveDtos.boxNo}" name="receiveDtos[${status.index}].boxNo"/>
                  </td>
                </tr>
                </c:forEach>
              </tbody>
            </table>
          </div>

          <div class="clearFix"></div>
        </div>
      </form:form>
    </div>
    <div class="modal-footer">
      <button type="button" id="stockRequestTransferIn" class="btn btn-primary"><spring:message code="gc.stock.label.transferin" /></button>
      <button type="button" id="" class="btn btn-default" data-dismiss="modal"><spring:message code="label_cancel"/></button>
    </div>
  </div>
</div>

<script src="<spring:url value="/js/jquery/jquery.numberInput.js" />" ></script>
  
<script type="text/javascript">
$(document).ready(function() {
  
  initForms();
  initButtons();
  
  function initButtons() {
    var $stockTable = $('#stockTable');
    var $stockTbody = $("tbody", $stockTable);
    
    $('#addProductBtn').click(function() {
      var $tr = $stockTbody.find("tr:first-child");
      if($tr.css('visibility') == 'hidden' || $tr.css('display') == 'none') {
          $tr.find("input").removeAttr('value').prop('disabled', false);
          $tr.show();
      } else {
          var $trLast = $stockTbody.find("tr:last-child");
          var  $row = $tr.clone();
          $row.find("input").removeAttr('value').prop('disabled', false);
          var index = $trLast.data("index") + 1;
          var lastIndex = $tr.data("index");
          $row.data("index", index);
          var rg = new RegExp("\\["+lastIndex+"\\]", 'g');
          var html = $row.html();
          html = html.replace(rg, '['+index+']');
          $row.html(html);

          $stockTbody.append($row);
          initRows();
      }
      $("#stockRequestTransferIn").attr("disabled", true);
  	});
    
    $('#removeProductBtn').click(function() {
      $("input.productRemoveFlag:checked").each(function() {
          var $tr = $(this).closest("tr");
          var length = $stockTbody.find("tr").length;
          if($tr.is(":first-child") && length == 1) {
              $tr.find("input.productRemoveFlag").prop('checked', false);
              $tr.find("input").removeAttr('value').prop('disabled', true);
              $tr.hide();
              $("#stockRequestTransferIn").attr("disabled", true);
              
          } else {
              $tr.remove();
              var isValid = true;
              $('#stockTable tr').each(function(){
                $(this).find(".productDesc").each(function(){
                  if ($(this).val() == "") {
                    isValid = false;
                  }
            	});
              });
              if (isValid == true)
              	$("#stockRequestTransferIn").removeAttr("disabled");
              else 
              	$("#stockRequestTransferIn").attr("disabled", true);
          }
      });
  });
  }
  
  function initForms() {
    initRows();
	$("#stockRequestTransferIn").unbind("click").click(function(e) {
	  var transferInUrl = $("#stockrequest").attr("action");
	  var dataObject = $("#stockrequest").serialize();
	  $("#stockRequestTransferIn").attr("disabled", true);
	  $.post(transferInUrl,
	      	dataObject,
	      	function(response, textStatus, jqXHR) {
	    		if (response.success) {
	    		  var printUrl = ctx + "/stockrequest/printtransfer/" + $("#id").val();
	    		  window.open(printUrl, '_blank', 'toolbar=0,location=0,menubar=0');
	    		  location.reload();
	    		  $("#stockRequestTransferIn").removeAttr("disabled");
	    		} else {
                  errorInfo = "";
                  for (i = 0; i < response.result.length; i++) {
                    errorInfo += "<br>" + (i + 1) + ". "
                    		+ ( response.result[i].code != undefined ? 
                    		    response.result[i].code : response.result[i]);
                  }
                  $("#contentError > div").html("Please correct following errors: " + errorInfo);
                  $("#contentError").show('slow');
                  $("#stockRequestTransferIn").removeAttr("disabled");
	    		}
	    	},
    		"json" );
	});
	
	$("#stockRequestTransferIn").attr("disabled", true);
  }
  
  function populateEndingSeries() {
    var startSeries = $(this).closest("tr").find(".startingSeries").val();
	var quantity = $(this).closest("tr").find(".quantity").val();
	if(startSeries != null && quantity != null && startSeries != "" && quantity != "") {
		var temp = +startSeries + +quantity - 1;
		var endingSeries = temp.toString();
		var padZero = 12 - endingSeries.length;
		if(padZero > 0) {
			for(var i = 0; i < padZero; i++)
				endingSeries = "0" + endingSeries;
		}
		$(this).closest("tr").find(".endingSeries").val(endingSeries);
		
		var validateUrl = ctx + "/stockrequest/validate/" + startSeries + "/" + endingSeries;
		var product = $(this).closest("tr").data("index");
		var dataObject = $("#stockrequest").serialize();
        $.get(validateUrl,
            dataObject,
            function(response, textStatus, jqXHR) {
              if (response.success) {
                $(".close").hide('slow');
                $("#contentError").hide('slow');
                $("#productCode\\["+product+"\\]").val(response.result.productCode);
                $("#productDesc\\["+product+"\\]").val(response.result.productName);
                $("#boxNo\\["+product+"\\]").val(response.result.boxNo);
                
                $("#stockRequestTransferIn").removeAttr("disabled");
                
                var isValid = true;
                $('#stockTable tr').each(function(){
                  $(this).find(".productDesc").each(function(){
                    console.log($(this).val());
                    if ($(this).val() == "") {
                      isValid = false;
                    }
              	});
                });
                console.log(isValid);
                if (isValid == true)
                	$("#stockRequestTransferIn").removeAttr("disabled");
                else 
                  	$("#stockRequestTransferIn").attr("disabled", true);
              } else {
                $("#productCode\\["+product+"\\]").val("");
                $("#productDesc\\["+product+"\\]").val("");
                $("#boxNo\\["+product+"\\]").val("");
    			errorInfo = "";
    			for (i = 0; i < response.result.length; i++) {
    				errorInfo += "<br>" + (i + 1) + ". "
    						+ ( response.result[i].code != undefined ? 
    						    response.result[i].code : response.result[i]);
    			}
    			$("#contentError > div").html("Please correct following errors: " + errorInfo);
    			$("#contentError").show('slow');
    			
    			$("#stockRequestTransferIn").attr("disabled", true);
    		  }
          		
        	},
        	"json"
        );
	} else {
	  $(this).closest("tr").find(".endingSeries").val("");
	}
	
  }
  
  function initRows() {
    $(".quantity").numberInput("type", "int");
    $(".startingSeries").numberInput("type", "int");
	$(".startingSeries").on("input", populateEndingSeries);
	$(".quantity").on("input", populateEndingSeries);
	$(".endingSeries").on("input", function(e) {
	  e.preventDefault();
	});
	$(".product").on("input", function(e) {
	  e.preventDefault();
	});
  }
});
</script>