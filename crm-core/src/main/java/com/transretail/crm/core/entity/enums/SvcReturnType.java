package com.transretail.crm.core.entity.enums;

public enum SvcReturnType {

    SUCCESS, NO_USER, NO_RIGHTS, ERROR

}
