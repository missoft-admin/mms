package com.transretail.crm.report.template.impl;

import java.io.File;
import java.math.BigDecimal;
import java.util.*;

import javax.annotation.Resource;

import net.sf.jasperreports.engine.JRParameter;

import org.apache.commons.io.FileUtils;
import org.joda.time.LocalDate;
import org.joda.time.LocalDateTime;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.context.support.MessageSourceResourceBundle;
import org.springframework.security.authentication.TestingAuthenticationToken;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.context.SecurityContextImpl;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.transretail.crm.common.reporting.exception.ReportException;
import com.transretail.crm.common.reporting.jasper.JRProcessor;
import com.transretail.crm.common.reporting.service.ReportService;
import com.transretail.crm.common.util.DateUtil;
import com.transretail.crm.core.entity.UserModel;
import com.transretail.crm.core.entity.lookup.LookupDetail;
import com.transretail.crm.core.entity.lookup.LookupHeader;
import com.transretail.crm.core.repo.LookupDetailRepo;
import com.transretail.crm.core.repo.LookupHeaderRepo;
import com.transretail.crm.core.repo.UserRepo;
import com.transretail.crm.core.security.model.impl.CustomSecurityUserDetailsImpl;
import com.transretail.crm.core.service.CodePropertiesService;
import com.transretail.crm.giftcard.entity.CardVendor;
import com.transretail.crm.giftcard.entity.GiftCardCustomerProfile;
import com.transretail.crm.giftcard.entity.GiftCardInventory;
import com.transretail.crm.giftcard.entity.GiftCardInventoryStock;
import com.transretail.crm.giftcard.entity.SalesOrder;
import com.transretail.crm.giftcard.entity.SalesOrderItem;
import com.transretail.crm.giftcard.entity.SalesOrderPaymentInfo;
import com.transretail.crm.giftcard.entity.GiftCardInventoryStock.GCIStockStatus;
import com.transretail.crm.giftcard.entity.GiftCardOrder;
import com.transretail.crm.giftcard.entity.GiftCardOrderItem;
import com.transretail.crm.giftcard.entity.ProductProfile;
import com.transretail.crm.giftcard.entity.ProductProfile.Denomination;
import com.transretail.crm.giftcard.entity.support.GiftCardOrderStatus;
import com.transretail.crm.giftcard.entity.support.GiftCardSalesType;
import com.transretail.crm.giftcard.entity.support.PaymentInfoStatus;
import com.transretail.crm.giftcard.entity.support.ProductProfileStatus;
import com.transretail.crm.giftcard.entity.support.SalesOrderStatus;
import com.transretail.crm.giftcard.entity.support.SalesOrderType;
import com.transretail.crm.giftcard.repo.CardVendorRepo;
import com.transretail.crm.giftcard.repo.GiftCardCustomerProfileRepo;
import com.transretail.crm.giftcard.repo.GiftCardInventoryRepo;
import com.transretail.crm.giftcard.repo.GiftCardInventoryStockRepo;
import com.transretail.crm.giftcard.repo.GiftCardOrderRepo;
import com.transretail.crm.giftcard.repo.ProductProfileRepo;
import com.transretail.crm.giftcard.repo.SalesOrderPaymentInfoRepo;
import com.transretail.crm.giftcard.repo.SalesOrderRepo;
import com.transretail.crm.report.template.CommonReportFilter;
import com.transretail.crm.report.template.ReportTemplate;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath*:/META-INF/spring/applicationContext*.xml",
    "/META-INF/spring/applicationContext-test.xml"})
public class AccountsReceivableDetailReportTest {

    private ProductProfile savedProfile;
    private SalesOrder order;
    
    @Autowired
    private ProductProfileRepo profileRepo;
    @Autowired
	private GiftCardCustomerProfileRepo customerRepo;
    @Autowired
	private SalesOrderRepo orderRepo;
    @Autowired
    private SalesOrderPaymentInfoRepo paymentRepo;
	@Autowired
    private ReportService reportService;
	@Autowired
    private MessageSource messageSource;
	
	
	@Resource(name = "accountsReceivableDetailReport")
    private ReportTemplate accountsReceivableDetailReport;
	
	private static final Logger logger = LoggerFactory.getLogger(AccountsReceivableDetailReportTest.class);
	
	@Autowired
	private CodePropertiesService codePropertiesService;
	@Autowired
	private LookupHeaderRepo lookupHeaderRepo;
	@Autowired
	private LookupDetailRepo lookupDetailRepo;
	@Autowired
	private UserRepo userRepo;

    
	
	@Before
	public void setUp() {
		
		UserModel currentUser = new UserModel();
    	currentUser.setUsername("loggedInUser");
    	currentUser.setPassword("");
    	currentUser.setEnabled(true);
    	currentUser.setInventoryLocation(codePropertiesService.getDetailInvLocationDenpasar());
    	currentUser.setStoreCode( "store1" );
    	userRepo.save(currentUser);

        CustomSecurityUserDetailsImpl user = new CustomSecurityUserDetailsImpl();
        user.setUserId(currentUser.getId());
        user.setUsername(currentUser.getUsername());
        List<GrantedAuthority> authorities = Lists.newArrayList();
        authorities.add(new SimpleGrantedAuthority("ROLE_CSS"));
        user.setAuthorities(authorities);
        user.setInventoryLocation(codePropertiesService.getDetailInvLocationDenpasar());

        SecurityContextImpl securityContext = new SecurityContextImpl();
        securityContext.setAuthentication(new TestingAuthenticationToken(user, ""));
        SecurityContextHolder.setContext(securityContext);
		
		LookupHeader hdr = new LookupHeader(codePropertiesService.getProductProfileFaceValue());
        hdr = lookupHeaderRepo.saveAndFlush(hdr);

        LookupDetail dtl = new LookupDetail("PPFV1");
        dtl.setDescription("100000");
        dtl.setHeader(hdr);
        dtl = lookupDetailRepo.saveAndFlush(dtl);
        
        hdr = new LookupHeader(codePropertiesService.getHeaderInventoryLocation());
        hdr = lookupHeaderRepo.saveAndFlush(hdr);

        dtl = new LookupDetail("INVT001");
        dtl.setDescription("HO");
        dtl.setHeader(hdr);
        dtl = lookupDetailRepo.saveAndFlush(dtl);
        
        hdr = new LookupHeader("PAYMENT INFO");
        hdr = lookupHeaderRepo.saveAndFlush(hdr);

        dtl = new LookupDetail("PF001");
        dtl.setDescription("CASH");
        dtl.setHeader(hdr);
        dtl = lookupDetailRepo.saveAndFlush(dtl);
        
		savedProfile = new ProductProfile();
		savedProfile.setCardFee(1.5);
		savedProfile.setEffectiveMonths(12l);
		savedProfile.setFaceValue(dtl);
		savedProfile.setProductCode("0000000000001");
		savedProfile.setStatus(ProductProfileStatus.APPROVED);
		savedProfile.setProductDesc("PROD");
		savedProfile.setUnitCost(.5);
		savedProfile = profileRepo.save(savedProfile);
		
		
		order = new SalesOrder();
		order.setOrderNo("BBMMS0501001");
		order.setOrderDate(new LocalDate());
		order.setStatus(SalesOrderStatus.SOLD);
		order.setOrderType(SalesOrderType.B2B_SALES);
		order.setItems(new HashSet<SalesOrderItem>());
		
		SalesOrderItem item = new SalesOrderItem();
		item.setProduct(savedProfile);
		item.setQuantity(10l);
		item.setFaceAmount(new BigDecimal(Denomination.D500K.getAmount() * 10));
		item.setOrder(order);
		order.getItems().add(item);
		order = orderRepo.save(order);
		
		SalesOrderPaymentInfo payment = new SalesOrderPaymentInfo();
		payment.setOrder(order);
		payment.setDateApproved(new LocalDate());
		payment.setDateVerified(new LocalDate());
		payment.setPaymentType(new LookupDetail("PF001"));
		payment.setPaymentDate(new LocalDate());
		payment.setPaymentDetails("TEST");
		payment.setStatus(PaymentInfoStatus.APPROVED);
		paymentRepo.save(payment);
	}
	
	@Test
	public void test() throws ReportException {
		
		Map<String, String> map = Maps.newHashMap();
		map.put(CommonReportFilter.DATE_FROM, new LocalDate().minusDays(1).toString(DateUtil.SEARCH_DATE_FORMAT));
		map.put(CommonReportFilter.DATE_TO, new LocalDate().plusDays(1).toString(DateUtil.SEARCH_DATE_FORMAT));
		JRProcessor jrProcessor = accountsReceivableDetailReport.createJrProcessor(map);

		File testClassesDir = FileUtils.toFile(Thread.currentThread().getContextClassLoader().getResource(""));
		File output = new File(testClassesDir, "test-collection-detail-output.pdf");
		logger.debug("Output location {}", output.getAbsolutePath());
		output.delete();
		Locale locale = LocaleContextHolder.getLocale();
		jrProcessor.addParameter(JRParameter.REPORT_LOCALE, locale);
		jrProcessor.addParameter(JRParameter.REPORT_RESOURCE_BUNDLE, new MessageSourceResourceBundle(messageSource, locale));
		reportService.exportPdfReport(jrProcessor, output);

		double bytes = output.length();
		double kilobytes = (bytes / 1024);
		logger.debug("Kilobytes: {}", kilobytes);
	}
}
