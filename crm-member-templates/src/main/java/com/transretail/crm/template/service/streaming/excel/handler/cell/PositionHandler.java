package com.transretail.crm.template.service.streaming.excel.handler.cell;

import java.util.Arrays;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.springframework.context.i18n.LocaleContextHolder;

import com.transretail.crm.core.entity.MemberModel;
import com.transretail.crm.core.entity.embeddable.ProfessionalProfile;
import com.transretail.crm.template.service.MemberTemplateParseException;
import com.transretail.crm.template.service.streaming.excel.handler.sheet.AbstractSheetHandler;

/**
 * @author Allan G. Ramirez (agramirez@exist.com)
 */
public class PositionHandler implements CellHandler {
    private static final List<String> ALLOWED_VALUES = Arrays.asList("OWNER", "PURCHASING", "CHIEF", "STAFF");
    private AbstractSheetHandler sheetHandler;
    private String headerName;

    public PositionHandler(AbstractSheetHandler sheetHandler, String headerName) {
        this.sheetHandler = sheetHandler;
        this.headerName = headerName;
    }

    @Override
    public void handle(MemberModel dto, String cellText) throws MemberTemplateParseException {
        if (dto.getProfessionalProfile() == null) {
            dto.setProfessionalProfile(new ProfessionalProfile());
        }
        if (StringUtils.isNotBlank(cellText)) {
            String position = cellText.toUpperCase();
            if (!ALLOWED_VALUES.contains(position)) {
                throw new MemberTemplateParseException(sheetHandler.getMessageSource()
                    .getMessage("member.template.messages.excel.value.invalid",
                        new String[]{cellText, headerName, StringUtils.join(ALLOWED_VALUES, ", ")}, LocaleContextHolder.getLocale()));
            }
            dto.getProfessionalProfile().setPosition(position);
        }
    }
}
