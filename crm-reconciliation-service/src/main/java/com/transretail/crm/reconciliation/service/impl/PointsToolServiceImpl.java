package com.transretail.crm.reconciliation.service.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.sql.DataSource;

import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.transretail.crm.common.util.DateUtil;
import com.transretail.crm.common.util.IOUtils;
import com.transretail.crm.reconciliation.service.PointsToolService;

/**
 * @author Allan G. Ramirez (agramirez@exist.com)
 */
@Service
public class PointsToolServiceImpl implements PointsToolService, InitializingBean {
    private static final Map<String, String> CRM_PAYMENT_TYPES = Maps.newHashMap();
    @Autowired
    private DataSource dataSource;

    @Override
    public void afterPropertiesSet() throws Exception {
        String hardCodedPosPaymentTypeMap = "{" +
            "\"CASH\"		    : \"PTYP001\"," +
            "\"GC\"			    : \"PTYP002\"," +
            "\"VOUCHER\" 	    : \"PTYP003\"," +
            "\"CRM_POINTS\"	    : \"PTYP004\"," +
            "\"FLAZZ\"		    : \"PTYP005\"," +
            "\"COUPON\"		    : \"PTYP006\"," +
            "\"INSTALLMENT\"	: \"PTYP007\"," +
            "\"EFT_ONLINE\"     : \"PTYP008\"," +
            "\"EDC_PAYMENT\"    : \"PTYP009\"," +
            "\"SODEXO\"         : \"PTYP010\"," +
            "\"DEBIT\"          : \"PTYP011\"," +
            "\"EFT_OFFLINE\"    : \"PTYP012\"," +
            "\"CMC_EFT_OFFLINE\": \"PTYP013\"," +
            "\"CMC_EFT_ONLINE\" : \"PTYP014\"," +
            "\"EDC_BCA\"        : \"PTYP015\"" +
            "}";

        Map<String, String> map =
            new ObjectMapper().readValue(hardCodedPosPaymentTypeMap, new TypeReference<HashMap<String, String>>() {
            });
        for (Map.Entry<String, String> entry : map.entrySet()) {
            CRM_PAYMENT_TYPES.put(entry.getKey().trim(), entry.getValue().trim());
        }
    }

    @Override
    public String createEarnWsUrl(String txnNo) throws SQLException {
        Assert.notNull(txnNo);
        Connection con = null;
        String earnWs = "/points/earnpoints/{id}/{store}/{paymentType}/{amount}/{cardNumber}/" + txnNo + "/{transactionDate}/{items}";
        try {
            con = dataSource.getConnection();
            con.setAutoCommit(false);
            // com.ibm.ws.rsadapter.jdbc.WSJdbcConnection does not support readonly
            if (!con.getClass().getName().contains("WSJdbcConnection")) {
                con.setReadOnly(true);
            }
            earnWs = fillIdStoreTransactionDate(con, earnWs, txnNo);
            earnWs = fillPaymentTypeAmountAndCardNumbers(con, earnWs, txnNo);
            earnWs = fillItems(con, earnWs, txnNo);
            return earnWs;
        } finally {
            IOUtils.INSTANCE.close(con, true);
        }
    }

    private String fillIdStoreTransactionDate(Connection con, String earnWs, String txnNo) throws SQLException {
        String query = "SELECT txn.CUSTOMER_ID, store.CODE, txn.TRANSACTION_DATE FROM POS_TRANSACTION txn " +
            "INNER JOIN STORE store ON store.STORE_ID = txn.STORE_ID  WHERE ID = ?";
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            ps = con.prepareStatement(query);
            ps.setString(1, txnNo);
            rs = ps.executeQuery();
            if (!rs.next()) {
                throw new SQLException("Transaction number [" + txnNo + "] does not exist in POS_TRANSACTION table.");
            }

            String customerId = rs.getString(1); // "CUSTOMER_ID" VARCHAR2(20 BYTE),
            String storeCode = rs.getString(2);
            Date txnDate = rs.getTimestamp(3); // "TRANSACTION_DATE" DATE
            // 00xx - employee length 11 digits , xx01 - individual, xx02 - professional
            if (customerId.startsWith("00")) {
                // According to Julz, trimming of customer_id to 11 digits is only applicable in employee
                if (customerId.length() > 11) {
                    customerId = customerId.substring(0, 11);
                }
            }
            earnWs = earnWs.replaceFirst("\\{id\\}", customerId);
            earnWs = earnWs.replaceFirst("\\{store\\}", storeCode);
            earnWs = earnWs.replaceFirst("\\{transactionDate\\}", DateUtil.convertDateToString(DateUtil.REST_DATE_FORMAT, txnDate));
            return earnWs;
        } finally {
            IOUtils.INSTANCE.close(rs);
            IOUtils.INSTANCE.close(ps);
        }
    }

    private String fillPaymentTypeAmountAndCardNumbers(Connection con, String earnWs, String txnNo) throws SQLException {
        String query = "SELECT MEDIA_TYPE, AMOUNT, ID FROM POS_PAYMENT WHERE POS_TXN_ID = ?";
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            ps = con.prepareStatement(query);
            ps.setString(1, txnNo);
            rs = ps.executeQuery();

            List<String> paymentIds = Lists.newArrayList();
            StringBuilder ptBldr = new StringBuilder();
            StringBuilder amountBldr = new StringBuilder();
            while (rs.next()) {
                String mediaType = rs.getString(1);
                if (ptBldr.length() > 0) {
                    // See PointsManagerServiceImpl#retrieveValidPaymentTypes
                    // line 548 List<String> paymentTypes = Arrays.asList(StringUtils.split(paymentType, ":"));
                    ptBldr.append(":");
                }
                ptBldr.append(CRM_PAYMENT_TYPES.get(mediaType));

                Integer amount = rs.getInt(2);
                if (amountBldr.length() > 0) {
                    // See PointsManagerServiceImpl#retrieveValidPaymentTypes
                    // line 549 List<String> amounts = Arrays.asList(StringUtils.split(amount, ":"));
                    amountBldr.append(":");
                }
                paymentIds.add(rs.getString(3));
                amountBldr.append(amount);
            }
            earnWs = earnWs.replaceFirst("\\{paymentType\\}", ptBldr.toString());
            earnWs = earnWs.replaceFirst("\\{amount\\}", amountBldr.toString());
            // Payments will never be empty
            earnWs = fillCardNumbers(con, earnWs, paymentIds);

            return earnWs;
        } finally {
            IOUtils.INSTANCE.close(rs);
            IOUtils.INSTANCE.close(ps);
        }
    }

    private String fillItems(Connection con, String earnWs, String txnNo) throws SQLException {
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            ps = con.prepareStatement("SELECT PRODUCT_ID,QUANTITY FROM POS_TX_ITEM WHERE POS_TXN_ID = ?");
            ps.setString(1, txnNo);
            rs = ps.executeQuery();
            StringBuilder builder = new StringBuilder();
            while (rs.next()) {
                // Format  <productId>!<qty>_<productId>!<qty>_<productId>!<qty>
                // See PointsManagerController.earnPoints
                if (builder.length() > 0) {
                    builder.append("_");
                }
                builder.append(rs.getString(1));
                builder.append("!");
                builder.append(rs.getInt(2));
            }
            earnWs = earnWs.replaceFirst("\\{items\\}", builder.length() > 0 ? builder.toString() : "null");
            return earnWs;
        } finally {
            IOUtils.INSTANCE.close(rs);
            IOUtils.INSTANCE.close(ps);
        }
    }


    private String fillCardNumbers(Connection con, String earnWs, List<String> paymentIds) throws SQLException {
        StringBuilder cardNoQuery = new StringBuilder("SELECT CARD_NUM FROM ELECTRONIC_FUND_TRANSFER WHERE POS_PAYMENT_ID IN (");
        for (int i = 0; i < paymentIds.size() - 1; i++) {
            cardNoQuery.append("?,");
        }
        cardNoQuery.append("?)");

        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            ps = con.prepareStatement(cardNoQuery.toString());
            for (int i = 0; i < paymentIds.size(); i++) {
                ps.setString(i + 1, paymentIds.get(i));
            }
            rs = ps.executeQuery();
            StringBuilder cardNoBuilder = new StringBuilder();
            while (rs.next()) {
                if (cardNoBuilder.length() > 0) {
                    // See PointsManagerServiceImpl#retrieveValidPaymentTypes
                    // line 554 cardNumbers = Arrays.asList(StringUtils.split(cardNumber, ":"));
                    cardNoBuilder.append(":");
                }
                cardNoBuilder.append(rs.getString(1));
            }
            earnWs = earnWs.replaceFirst("\\{cardNumber\\}", cardNoBuilder.length() > 0 ? cardNoBuilder.toString() : "null");
            return earnWs;
        } finally {
            IOUtils.INSTANCE.close(rs);
            IOUtils.INSTANCE.close(ps);
        }
    }
}
