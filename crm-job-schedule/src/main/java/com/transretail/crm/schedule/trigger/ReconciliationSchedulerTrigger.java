package com.transretail.crm.schedule.trigger;

import java.text.ParseException;

import javax.annotation.Resource;

import org.apache.commons.lang3.StringUtils;
import org.quartz.CronExpression;
import org.quartz.JobDetail;
import org.quartz.Scheduler;
import org.quartz.impl.triggers.CronTriggerImpl;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.quartz.JobDetailAwareTrigger;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.transretail.crm.common.service.exception.GenericServiceException;
import com.transretail.crm.core.entity.ApplicationConfig;
import com.transretail.crm.core.entity.enums.AppConfigDefaults;
import com.transretail.crm.core.entity.enums.AppKey;
import com.transretail.crm.core.repo.ApplicationConfigRepo;

/**
 * @author Allan G. Ramirez (agramirez@exist.com)
 */
@Service
public class ReconciliationSchedulerTrigger extends CronTriggerImpl implements InitializingBean {
    public static final String TRIGGER_NAME = "RECONCILIATION_JOB_TRIGGER";
    public static final String TRIGGER_GROUP = Scheduler.DEFAULT_GROUP;
    @Resource(name = "reconcilationJob")
    private JobDetail jobDetail;
    @Autowired
    private ApplicationConfigRepo applicationConfigRepo;

    @Override
    public void afterPropertiesSet() throws Exception {
        getJobDataMap().put(JobDetailAwareTrigger.JOB_DETAIL_KEY, jobDetail);
        setName(TRIGGER_NAME);
        setGroup(TRIGGER_GROUP);
        super.setCronExpression(getCronExpression());
        setJobKey(jobDetail.getKey());
    }

    @Override
    @Transactional(readOnly = true)
    public String getCronExpression() {
        ApplicationConfig config = applicationConfigRepo.findByKey(AppKey.RECONCILIATION_JOB);
        return config != null && StringUtils.isNotBlank(config.getValue()) ? config.getValue()
            : AppConfigDefaults.DEFAULT_RECONCILIATION_CRON;
    }

    @Override
    @Transactional
    public void setCronExpression(String cronExpression) throws ParseException {
        if (!CronExpression.isValidExpression(cronExpression)) {
            throw new GenericServiceException("Invalid cron express [" + cronExpression + "].");
        }
        super.setCronExpression(cronExpression);
        ApplicationConfig config = applicationConfigRepo.findByKey(AppKey.RECONCILIATION_JOB);
        if (null == config) {
            config = new ApplicationConfig();
            config.setKey(AppKey.RECONCILIATION_JOB);
        }
        config.setValue(cronExpression);
        applicationConfigRepo.save(config);
    }
}
