package com.transretail.crm.giftcard.dto;

import java.util.List;

import org.joda.time.LocalDate;

import com.google.common.collect.Lists;
import com.transretail.crm.giftcard.entity.B2BExhibitionLocation;



/**
 *
 */
public class B2BExhLocDto {
	
	private List<B2BExhDiscDto> discountMatrix = Lists.newArrayList();
	private String name;
	private String address;
	private LocalDate startDate;
	private LocalDate endDate;
	private Long id;
	
	public B2BExhLocDto() {}
	
	public B2BExhLocDto(B2BExhibitionLocation loc) {
		this.name = loc.getName();
		this.address = loc.getAddress();
		this.startDate = loc.getStartDate();
		this.endDate = loc.getEndDate();
		this.id = loc.getId();
	}
	
	public B2BExhibitionLocation toModel(B2BExhibitionLocation loc) {
		if(loc == null)
			loc = new B2BExhibitionLocation();
		
		loc.setName(this.name);
		loc.setAddress(this.address);
		loc.setStartDate(this.startDate);
		loc.setEndDate(this.endDate);
		
		return loc;
	}
	
	public static List<B2BExhLocDto> toDto(List<B2BExhibitionLocation> locs) {
		List<B2BExhLocDto> locDtos = Lists.newArrayList();
		for(B2BExhibitionLocation loc: locs) {
			locDtos.add(new B2BExhLocDto(loc));
		}
		return locDtos;
	}
	
	
	public List<B2BExhDiscDto> getDiscountMatrix() {
		return discountMatrix;
	}
	public void setDiscountMatrix(List<B2BExhDiscDto> discountMatrix) {
		this.discountMatrix = discountMatrix;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public LocalDate getStartDate() {
		return startDate;
	}
	public void setStartDate(LocalDate startDate) {
		this.startDate = startDate;
	}
	public LocalDate getEndDate() {
		return endDate;
	}
	public void setEndDate(LocalDate endDate) {
		this.endDate = endDate;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}
	
	
	
}

