package com.transretail.crm.core.dto;

import java.util.Collection;

import com.transretail.crm.common.service.dto.response.AbstractResultListDTO;

/**
 * @author Allan G. Ramirez (agramirez@exist.com)
 */
public class CrmInStoreResultList extends AbstractResultListDTO<CrmInStoreDto> {
    public CrmInStoreResultList(Collection<CrmInStoreDto> results, long totalElements, boolean hasPreviousPage,
        boolean hasNextPage) {
        super(results, totalElements, hasPreviousPage, hasNextPage);
    }
}
