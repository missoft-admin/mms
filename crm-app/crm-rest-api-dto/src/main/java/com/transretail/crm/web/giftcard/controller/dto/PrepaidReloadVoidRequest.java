package com.transretail.crm.web.giftcard.controller.dto;

import org.hibernate.validator.constraints.NotEmpty;

/**
 * @author Allan G. Ramirez (agramirez@exist.com)
 */
public class PrepaidReloadVoidRequest extends PrepaidReloadRequest {
    @NotEmpty
    private String refTransactionNo;

    public String getRefTransactionNo() {
        return refTransactionNo;
    }

    public void setRefTransactionNo(String refTransactionNo) {
        this.refTransactionNo = refTransactionNo;
    }
}
