package com.transretail.crm.product.service;

/**
 * @author Allan G. Ramirez (agramirez@exist.com)
 */
public interface ProductHierSchedulerService {
    void updateCrmProductHier();
}
