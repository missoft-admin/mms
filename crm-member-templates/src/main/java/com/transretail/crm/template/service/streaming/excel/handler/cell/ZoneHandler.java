package com.transretail.crm.template.service.streaming.excel.handler.cell;

import com.transretail.crm.core.entity.MemberModel;
import com.transretail.crm.core.entity.embeddable.ProfessionalProfile;
import com.transretail.crm.template.service.MemberTemplateParseException;

/**
 * @author Allan G. Ramirez (agramirez@exist.com)
 */
public class ZoneHandler implements CellHandler {
    @Override
    public void handle(MemberModel dto, String cellText) throws MemberTemplateParseException {
        if(dto.getProfessionalProfile() == null) {
            dto.setProfessionalProfile(new ProfessionalProfile());
        }
        dto.getProfessionalProfile().setZone(cellText);
    }
}
