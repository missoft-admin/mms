package com.transretail.crm.common.web.dto;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import org.joda.time.DateTime;

/**
 * @author aco
 */
public final class WebFormInfo {

    private final String id;

    private DateTime lastAccessed;

    public WebFormInfo(String id) {
        this.id = id;
    }

    public String getId() {
        return id;
    }

    public DateTime getLastAccessed() {
        return lastAccessed;
    }

    public void setLastAccessed(DateTime lastAccessed) {
        this.lastAccessed = lastAccessed;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this, ToStringStyle.SHORT_PREFIX_STYLE)
            .append("id", getId())
            .append("lastAccessed", getLastAccessed())
            .toString();
    }
}
