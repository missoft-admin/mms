package com.transretail.crm.loyalty.dto;

import com.transretail.crm.core.entity.lookup.LookupDetail;
import com.transretail.crm.loyalty.entity.LoyaltyAnnualFeeScheme;

public class LoyaltyAnnualFeeSchemeDto {
	private Long id;
	private LookupDetail company;
	private LookupDetail cardType;
	private Double annualFee;
	private Double replacementFee;
	
	public LoyaltyAnnualFeeSchemeDto() {}
	
	public LoyaltyAnnualFeeSchemeDto(LoyaltyAnnualFeeScheme scheme) {
		if(scheme != null) {
			this.id = scheme.getId();
			this.company = scheme.getCompany();
			this.cardType = scheme.getCardType();
			this.annualFee = scheme.getAnnualFee();
			this.replacementFee = scheme.getReplacementFee();	
		}
	}
	
	public LoyaltyAnnualFeeScheme toModel(LoyaltyAnnualFeeScheme scheme) {
		if(scheme == null)
			scheme = new LoyaltyAnnualFeeScheme();
		
		scheme.setCompany(this.company);
		scheme.setCardType(this.cardType);
		scheme.setAnnualFee(this.annualFee);
		scheme.setReplacementFee(this.replacementFee);
		
		return scheme;
	}
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public LookupDetail getCompany() {
		return company;
	}
	public void setCompany(LookupDetail company) {
		this.company = company;
	}
	public LookupDetail getCardType() {
		return cardType;
	}
	public void setCardType(LookupDetail cardType) {
		this.cardType = cardType;
	}
	public Double getAnnualFee() {
		return annualFee;
	}
	public void setAnnualFee(Double annualFee) {
		this.annualFee = annualFee;
	}
	public Double getReplacementFee() {
		return replacementFee;
	}
	public void setReplacementFee(Double replacementFee) {
		this.replacementFee = replacementFee;
	}
	
}
