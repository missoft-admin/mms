package com.transretail.crm.report.template.impl.memberactivity;

import com.google.common.collect.Lists;
import com.transretail.crm.common.reporting.exception.ReportException;
import com.transretail.crm.core.entity.enums.ReportType;
import com.transretail.crm.report.template.dynamicreports.Templates;
import com.transretail.crm.report.template.impl.AbstractMemberPurchaseReport;
import net.sf.dynamicreports.report.exception.DRException;
import net.sf.jasperreports.engine.JasperPrint;

import javax.sql.DataSource;
import java.text.DateFormatSymbols;
import java.util.List;
import java.util.Locale;
import java.util.Map;

/**
 * @author Mike de Guzman
 */
public class MonthlyMemberPurchaseJrProcessor extends AbstractMemberPurchaseReport {
    private static String[] monthNames;

    static {
        DateFormatSymbols symbols = new DateFormatSymbols(Locale.ENGLISH);
        monthNames = symbols.getMonths();
    }

    public MonthlyMemberPurchaseJrProcessor(String reportFileName, String title, String subTitle, DataSource dataSource, ReportType reportType) {
        super(reportFileName, title, subTitle, dataSource, reportType);
    }

    @Override
    public List<JasperPrint> getJasperPrints() throws ReportException {
        List<JasperPrint> jasperPrints = Lists.newArrayList();
        jasperPrints.add(createMemberListReport());
        return jasperPrints;
    }

    private JasperPrint createMemberListReport() throws ReportException {
        try {
            if (dateFrom.toString("MMMM").toUpperCase().equals(dateTo.toString("MMMM").toUpperCase())) {
                this.title = this.title + "\n" + dateFrom.toString("MMMM").toUpperCase();
            } else {
                this.title = this.title + "\n" +  dateFrom.toString("MMMM").toUpperCase() + " to " + dateTo.toString("MMMM").toUpperCase();
            }

            return createJRBuilderWithTitleAndSubTitle()
                    .setPageMargin(Templates.DEFAULT_MARGIN)
                    .addColumn(createColumn("Customer ID", "customerId", String.class, 100))
                    .addColumn(createColumn("Name", "customerName", String.class, 200))
                    .addColumn(createColumn("Business Name", "businessName", String.class, 200))
                    .addColumn(createColumn("Segmentation", "segmentation", String.class, 150))
                    .addColumn(createColumn("Mobile Phone", "mobilePhone", String.class, 150))
                    .addColumn(createColumn("Business Phones", "businessPhone", String.class, 150))
                    .addColumn(createColumn("Home Phones", "homePhone", String.class, 150))
                    .addColumn(createColumn("Business City", "businessCity", String.class, 150))
                    .addColumn(createColumn("Business Postal Code", "businessPostCode", String.class, 150))

                    .addColumn(createColumn("Total Amount", "totalAmount", Long.class, 100))
                    .addColumn(createColumn("Amount w/o VAT", "amountVat", Long.class, 100))
                    .addColumn(createColumn("PPN", "ppn", Long.class, 100))
                    .addColumn(createColumn("Total Mark up", "totalMarkup", Long.class, 100))
                    .addColumn(createColumn("PPN Mark up", "ppnMarkup", Long.class, 100))
                    .addColumn(createColumn("Freq Visit", "freqVisit", Long.class, 100))
                    .addColumn(createColumn("Registration Date", "registrationDate", String.class, 150))
                    .addColumn(createColumn("Last Visit Date", "lastVisitDate", String.class, 150))
                    .setDataSource(createJrDataSource())
                    .toJasperPrint();
        } catch (DRException e) {
            getAfterPageExportListener().afterPageExport();
            throw new ReportException(e);
        }
    }

    @Override
    public boolean isEmpty(Map<String, String> map) {
        return super.isEmpty(map);
    }
}
