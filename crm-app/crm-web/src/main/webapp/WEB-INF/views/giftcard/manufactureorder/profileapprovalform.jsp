<%@ include file="../../common/taglibs.jsp" %>

<div class="modal-dialog">
  <div class="modal-content">
 
    <div class="modal-header">
      <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
      <h4><spring:message code="create_profile"/></h4>
    </div>
    
    <div class="modal-body">
    
    <div id="contentError" class="hide alert alert-error">
      <button type="button" class="close" onclick="$(this).parent().hide();">&times;</button>
      <div><form:errors path="*"/></div>
    </div>
    
    <div class="row-fluid">
    
    <form:form id="profileForm" name="profileForm" modelAttribute="profileForm" action="${pageContext.request.contextPath}/gc/productprofile/approve/" method="POST" cssClass="form-reset form-horizontal" >
      <form:hidden path="id"/>
      <div class="span6">
      <div class="control-group">
        <spring:message code="profile_product_code" var="prodCode" />
        <label for="productCode" class="control-label">${prodCode}</label> 
        <div class="controls">
          <form:input path="productCode" maxlength="2" class="form-control" placeholder="${prodCode}" />
        </div>
      </div>
      
      <div class="control-group">
        <spring:message code="profile_product_desc" var="prodDesc" />
        <label for="productDesc" class="control-label">${prodDesc}</label> 
        <div class="controls">
          <form:input path="productDesc" class="form-control" placeholder="${prodDesc}" />
        </div>
      </div>
      
      <div class="control-group">
        <spring:message code="profile_face_value" var="faceVal" />
        <label for="faceValue" class="control-label">${faceVal}</label> 
        <div class="controls">
          <form:input path="faceValueDesc" class="form-control" placeholder="${faceVal}" />
        </div>
      </div>
      
      <div class="control-group">
        <spring:message code="profile_card_fee" var="crdFee" />
        <label for="cardFee" class="control-label">${crdFee}</label> 
        <div class="controls">
          <form:input path="cardFee" class="form-control" placeholder="${crdFee}" />
        </div>
      </div>
      
      
      <div class="control-group">
        <label for="cardFee" class="control-label">&nbsp;</label> 
        <div class="controls">
          <div class="span checkbox">
            <form:checkbox path="allowReload" /><label for="allowReload"><spring:message code="profile_allow_reload" /></label>
          </div>
          <div class="span checkbox">
            <form:checkbox path="allowPartialRedeem" /><label for="allowReload"><spring:message code="profile_allow_partial_redeem" /></label>
          </div>
          <div class="span checkbox">
            <form:checkbox path="isEgc" /><label for="isEgc"><spring:message code="order_isegc" /></label>
          </div>
        </div>
      </div>
      
      </div>
      
      <div class="span6">
      
      <div class="control-group">
        <spring:message code="profile_safety_stocks" var="safetyStocks" />
        <label for="safetyStocksEaMo" class="control-label">${safetyStocks}</label> 
        <div class="controls">
          <form:input path="safetyStocksEaMo" class="form-control" placeholder="${safetyStocks}" />
        </div>
      </div>
      
      <div class="control-group">
        <spring:message code="profile_max_amount" var="maxAmnt" />
        <label for="maxAmount" class="control-label">${maxAmnt}</label> 
        <div class="controls">
          <form:input path="maxAmount" class="form-control" placeholder="${maxAmnt}" />
        </div>
      </div>
      
      <div class="control-group">
        <spring:message code="profile_effective_months" var="effectvMos" />
        <label for="effectiveMonths" class="control-label">${effectvMos}</label> 
        <div class="controls">
          <form:input path="effectiveMonths" class="form-control" placeholder="${effectvMos}" />
        </div>
      </div>
      
      <div class="control-group">
        <spring:message code="profile_unit_cost" var="unitCst" />
        <label for="unitCost" class="control-label">${unitCst}</label> 
        <div class="controls">
          <form:input path="unitCost" class="form-control" placeholder="${unitCst}" />
        </div>
      </div>
      
      </div>
      
    </form:form>
    
    </div>
    
    </div>
    <div class="modal-footer">
      <button type="button" data-status="APPROVED" class="profileSubmit btn btn-primary"><spring:message code="label_approve" /></button>
      <button type="button" data-status="REJECTED" class="profileSubmit btn btn-primary"><spring:message code="label_reject" /></button>
      <button type="button" id="profileCancel" class="btn btn-default" data-dismiss="modal"><spring:message code="label_cancel"/></button>
    </div>
  </div>
</div>

<script>
$(document).ready(function() {
	
	var CONTAINER_ERROR = "#contentError > div";
	var CONTENT_ERROR = "#contentError";
	$form = $("#profileForm");
	$orderDialog = $("#orderDialog");
	
	$(".profileSubmit").click(submitForm);
	
	function submitForm() {
		$(".profileSubmit").prop( "disabled", true );
		$.post($form.attr("action") + $(this).data("status"), $form.serialize(), function(data) {
			$(".profileSubmit").prop( "disabled", false );
			if (data.success) {
				location.reload();
			} 
			else {
				errorInfo = "";
				for (i = 0; i < data.result.length; i++) {
					errorInfo += "<br>" + (i + 1) + ". "
							+ ( data.result[i].code != undefined ? 
									data.result[i].code : data.result[i]);
				}
				$orderDialog.find(CONTAINER_ERROR).html("Please correct following errors: " + errorInfo);
				$orderDialog.find(CONTENT_ERROR).show('slow');
			}
		});	
	}
	
});
</script>


