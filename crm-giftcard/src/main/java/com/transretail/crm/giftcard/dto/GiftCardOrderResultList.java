package com.transretail.crm.giftcard.dto;

import java.util.Collection;

import com.transretail.crm.common.service.dto.response.AbstractResultListDTO;

/**
 *
 */
public class GiftCardOrderResultList extends AbstractResultListDTO<GiftCardOrderDto> {
    public GiftCardOrderResultList(Collection<GiftCardOrderDto> results, long totalElements, boolean hasPreviousPage,
        boolean hasNextPage) {
        super(results, totalElements, hasPreviousPage, hasNextPage);
    }
}
