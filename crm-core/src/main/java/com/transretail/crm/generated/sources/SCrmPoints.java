package com.transretail.crm.generated.sources;

import static com.mysema.query.types.PathMetadataFactory.forVariable;

import javax.annotation.Generated;

import com.mysema.query.sql.ColumnMetadata;
import com.mysema.query.types.Path;
import com.mysema.query.types.PathMetadata;
import com.mysema.query.types.path.DatePath;
import com.mysema.query.types.path.DateTimePath;
import com.mysema.query.types.path.NumberPath;
import com.mysema.query.types.path.StringPath;


/**
 * SCrmPoints is a Querydsl query type for BCrmPoints
 */
@Generated("com.mysema.query.sql.codegen.MetaDataSerializer")
public class SCrmPoints extends com.mysema.query.sql.RelationalPathBase<BCrmPoints> {

    private static final long serialVersionUID = 1874520042;

    public static final SCrmPoints crmPoints = new SCrmPoints("CRM_POINTS");

    public final StringPath comments = createString("comments");

    public final StringPath createdBy = createString("createdBy");

    public final DateTimePath<java.sql.Timestamp> createdDatetime = createDateTime("createdDatetime", java.sql.Timestamp.class);

    public final NumberPath<Double> expireExcess = createNumber("expireExcess", Double.class);

    public final DatePath<java.sql.Date> expiryDate = createDate("expiryDate", java.sql.Date.class);

    public final StringPath id = createString("id");

    public final DateTimePath<java.sql.Timestamp> inclusiveDateFrom = createDateTime("inclusiveDateFrom", java.sql.Timestamp.class);

    public final DateTimePath<java.sql.Timestamp> inclusiveDateTo = createDateTime("inclusiveDateTo", java.sql.Timestamp.class);

    public final DateTimePath<java.sql.Timestamp> lastRedeem = createDateTime("lastRedeem", java.sql.Timestamp.class);

    public final StringPath lastUpdatedBy = createString("lastUpdatedBy");

    public final DateTimePath<java.sql.Timestamp> lastUpdatedDatetime = createDateTime("lastUpdatedDatetime", java.sql.Timestamp.class);

    public final NumberPath<Long> memberId = createNumber("memberId", Long.class);

    public final StringPath reason = createString("reason");

    public final StringPath status = createString("status");

    public final StringPath storeCode = createString("storeCode");

    public final NumberPath<Double> txnAmount = createNumber("txnAmount", Double.class);

    public final DateTimePath<java.sql.Timestamp> txnDate = createDateTime("txnDate", java.sql.Timestamp.class);

    public final StringPath txnMedia = createString("txnMedia");

    public final StringPath txnNo = createString("txnNo");

    public final NumberPath<Long> txnOwner = createNumber("txnOwner", Long.class);

    public final NumberPath<Double> txnPoints = createNumber("txnPoints", Double.class);

    public final StringPath txnType = createString("txnType");

    public final StringPath validatedBy = createString("validatedBy");

    public final DateTimePath<java.sql.Timestamp> validatedDate = createDateTime("validatedDate", java.sql.Timestamp.class);

    public final NumberPath<Integer> validPeriod = createNumber("validPeriod", Integer.class);

    public final com.mysema.query.sql.PrimaryKey<BCrmPoints> constraintDa4 = createPrimaryKey(id);

    public final com.mysema.query.sql.ForeignKey<BCrmMember> _9syii89omui408wduaxod6sqcFk = createForeignKey(memberId, "ID");

    public final com.mysema.query.sql.ForeignKey<BCrmMember> _9yx5tvnepyq8t0ffradnhw8b1Fk = createForeignKey(txnOwner, "ID");

    public SCrmPoints(String variable) {
        super(BCrmPoints.class, forVariable(variable), "PUBLIC", "CRM_POINTS");
        addMetadata();
    }

    public SCrmPoints(String variable, String schema, String table) {
        super(BCrmPoints.class, forVariable(variable), schema, table);
        addMetadata();
    }

    public SCrmPoints(Path<? extends BCrmPoints> path) {
        super(path.getType(), path.getMetadata(), "PUBLIC", "CRM_POINTS");
        addMetadata();
    }

    public SCrmPoints(PathMetadata<?> metadata) {
        super(BCrmPoints.class, metadata, "PUBLIC", "CRM_POINTS");
        addMetadata();
    }

    public void addMetadata() {
        addMetadata(comments, ColumnMetadata.named("COMMENTS").ofType(12).withSize(255));
        addMetadata(createdBy, ColumnMetadata.named("CREATED_BY").ofType(12).withSize(255));
        addMetadata(createdDatetime, ColumnMetadata.named("CREATED_DATETIME").ofType(93).withSize(23).withDigits(10));
        addMetadata(expireExcess, ColumnMetadata.named("EXPIRE_EXCESS").ofType(8).withSize(17));
        addMetadata(expiryDate, ColumnMetadata.named("EXPIRY_DATE").ofType(91).withSize(8));
        addMetadata(id, ColumnMetadata.named("ID").ofType(12).withSize(255).notNull());
        addMetadata(inclusiveDateFrom, ColumnMetadata.named("INCLUSIVE_DATE_FROM").ofType(93).withSize(23).withDigits(10));
        addMetadata(inclusiveDateTo, ColumnMetadata.named("INCLUSIVE_DATE_TO").ofType(93).withSize(23).withDigits(10));
        addMetadata(lastRedeem, ColumnMetadata.named("LAST_REDEEM").ofType(93).withSize(23).withDigits(10));
        addMetadata(lastUpdatedBy, ColumnMetadata.named("LAST_UPDATED_BY").ofType(12).withSize(255));
        addMetadata(lastUpdatedDatetime, ColumnMetadata.named("LAST_UPDATED_DATETIME").ofType(93).withSize(23).withDigits(10));
        addMetadata(memberId, ColumnMetadata.named("MEMBER_ID").ofType(-5).withSize(19));
        addMetadata(reason, ColumnMetadata.named("REASON").ofType(12).withSize(255));
        addMetadata(status, ColumnMetadata.named("STATUS").ofType(12).withSize(255));
        addMetadata(storeCode, ColumnMetadata.named("STORE_CODE").ofType(12).withSize(255));
        addMetadata(txnAmount, ColumnMetadata.named("TXN_AMOUNT").ofType(8).withSize(17));
        addMetadata(txnDate, ColumnMetadata.named("TXN_DATE").ofType(93).withSize(23).withDigits(10));
        addMetadata(txnMedia, ColumnMetadata.named("TXN_MEDIA").ofType(12).withSize(255));
        addMetadata(txnNo, ColumnMetadata.named("TXN_NO").ofType(12).withSize(255));
        addMetadata(txnOwner, ColumnMetadata.named("TXN_OWNER").ofType(-5).withSize(19));
        addMetadata(txnPoints, ColumnMetadata.named("TXN_POINTS").ofType(8).withSize(17));
        addMetadata(txnType, ColumnMetadata.named("TXN_TYPE").ofType(12).withSize(255));
        addMetadata(validatedBy, ColumnMetadata.named("VALIDATED_BY").ofType(12).withSize(255));
        addMetadata(validatedDate, ColumnMetadata.named("VALIDATED_DATE").ofType(93).withSize(23).withDigits(10));
        addMetadata(validPeriod, ColumnMetadata.named("VALID_PERIOD").ofType(4).withSize(10));
    }

}

