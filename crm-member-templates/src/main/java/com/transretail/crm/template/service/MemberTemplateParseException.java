package com.transretail.crm.template.service;

/**
 * @author Allan G. Ramirez (agramirez@exist.com)
 */
public class MemberTemplateParseException extends Exception {
    public MemberTemplateParseException(String message) {
        super(message);
    }

    public MemberTemplateParseException(String message, Throwable e) {
        super(message, e);
    }
}
