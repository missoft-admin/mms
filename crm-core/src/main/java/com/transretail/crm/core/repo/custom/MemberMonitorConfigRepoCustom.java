package com.transretail.crm.core.repo.custom;

import java.util.List;

import org.joda.time.LocalDate;

import com.transretail.crm.core.dto.MemberDto;

public interface MemberMonitorConfigRepoCustom {
	List<MemberDto> getMonitoredMembers(Integer transcationCount, LocalDate date);
	List<MemberDto> getMonitoredEmployees(Integer transactionCount, LocalDate date);
}
