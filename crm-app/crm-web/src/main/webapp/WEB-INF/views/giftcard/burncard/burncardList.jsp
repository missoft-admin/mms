<%@ include file="../../common/taglibs.jsp" %>

<style type="text/css">
  <!--
  .well .input {
    background-color: #fff;
    border: 1px solid #E8E8E8;
    box-shadow: none;    
    padding: 6px;
    height: 18px;
    margin-left: 5px;
  }
  .well .btn-primary {
    margin-left: 5px;
  }
  .well select, .well .input {
    margin: 5px;
  }

  -->
</style>

<div id="content_burncard">

  <div class="page-header page-header2"><h1><spring:message code="gc.burncard.header" /></h1></div>

  <jsp:include page="burnCardSearchForm.jsp"/>

  <div class="">
    <div class="pull-right"><a href="#" class="btn btn-primary" data-url="<c:url value="/giftcard/burncard/create"/>" id="link_create"><spring:message code="gc.burncard.label.submit.gc" /></a></div>
  </div>

  <div class="clearfix"></div>
  <div class="modal hide groupModal nofly" tabindex="-1" role="dialog" aria-hidden="true" id="form_burncard"></div>
  <div id="list_burncard_container" class="data-content-02">
    <div id="list_burncard"
	 data-url="<c:url value="/giftcard/burncard/list"/>"
	 data-burn_no="<spring:message code="gc.burncard.label.burn.no"/>"
	 data-store_id="<spring:message code="gc.burncard.label.store.id"/>" 
	 data-date_filed="<spring:message code="gc.burncard.label.date.filed"/>" 
	 data-filed_by="<spring:message code="gc.burncard.label.filed.by"/>" 
	 data-burn_reason="<spring:message code="gc.burncard.label.burn.reason"/>" 
	 data-quantity="<spring:message code="gc.burncard.label.quantity"/>" 
	 data-status="<spring:message code="gc.burncard.label.status"/>" >
    </div> 
  </div>

  <div id="burncard_list_actions" class="hide">
    <div id="view">
      <input type="button" data-url="<c:url value="/giftcard/burncard/view"/>/%ID%" class="btn view icn-view tiptip pull-left" title="<spring:message code="label_view" />">
    </div>
    <div id="edit">
      <input type="button" data-url="<c:url value="/giftcard/burncard/edit"/>/%ID%" class="btn edit icn-edit tiptip pull-left" title="<spring:message code="label_edit" />">
    </div>
    <div id="print">
      <input type="button" data-url="<c:url value="/giftcard/burncard/print"/>/%ID%/EXCEL" class="btn printExcel icn-print tiptip pull-left" title="<spring:message code="gc.burncard.label.print.excel" />">
      <input type="button" data-url="<c:url value="/giftcard/burncard/print"/>/%ID%/PDF" class="btn printPdf icn-print tiptip pull-left" title="<spring:message code="gc.burncard.label.print.pdf" />">
    </div>
  </div>

  <link href="<c:url value="/styles/temp.css" />" rel="stylesheet" />
  <link href="<spring:url value="/css/dataTables.css" />" rel="stylesheet">
  <script src="<spring:url value="/js/jquery/jquery.dataTables.js" />" ></script>
  <script src="<spring:url value="/js/jquery/jquery.dataTables.linkLengthMenu.js" />"></script>
  <script src="<spring:url value="/js/jquery/jquery.dataTables.customPaging.js" />"></script>
  <script src="<spring:url value="/js/jquery/ajax-datatable.js" />" ></script>
  <script src="<spring:url value="/js/common/form2js.js" />" ></script>
  <script src="<spring:url value="/js/common/json2.js" />" ></script>
  <script src="<spring:url value="/js/jquery/jquery.toObject.js" />" ></script>
</div>

<script type="text/javascript">
  $(document).ready(function() {

    var list = $("#list_burncard");
    var modalBurncard = $("#form_burncard");

    modalBurncard.modal({
      show: false
    }).on('hidden.bs.modal', function() {
      $(this).empty();
    });

    initButtons();
    initDataTable();

    function initDataTable() {
      var headers = [
	{text: "", className : "hide"},
	list.data("burn_no"),
	list.data("store_id"),
	list.data("date_filed"),
	list.data("filed_by"),
	list.data("burn_reason"),
	list.data("quantity"),
	list.data("status"),
	""
      ];

      list.ajaxDataTable({
	'autoload': true,
	'aaSorting': [[0, 'desc']],
	'ajaxSource': list.data("url"),
	'columnHeaders': headers,
	'modelFields': [{name: 'lastUpdated', sortable: true, aaClassname: "hide"},
	  {name: 'burnNo', sortable: true},
	  {name: 'store', sortable: false},
	  {name: 'dateFiled', sortable: true, fieldCellRenderer: function(data, type, row) {
	      return row.dateFiledString;
	    }},
	  {name: 'filedBy', sortable: false},
	  {name: 'burnReasonDescription'},
	  {name: 'quantity', sortable: false},
	  {name: 'status', sortable: true, fieldCellRenderer : function (data, type, row) {
          return (row.status === 'FORAPPROVAL')? 'FOR APPROVAL' : row.status;
      }},
	  {customCell: function(innerData, sSpecific, json) {
	      if (sSpecific === 'display') {
		var btnHtml = "";

		if (json.status === 'DRAFT') {
		  btnHtml += $("#edit").html();
		} else if (json.status === 'BURNED') {
		  btnHtml += $("#print").html();
		} else if (json.status !== 'REJECTED') {
		  btnHtml += $("#view").html();
		}
		var rg = new RegExp("%ID%", 'g');
		btnHtml = btnHtml.replace(rg, json.burnNo);

		return btnHtml;
	      } else {
		return '';
	      }
	    }}
	]
      })
	      .on("click", ".view", viewBurnCard)
	      .on("click", ".edit", editBurnCard)
	      .on("click", ".printPdf", printPdf)
	      .on("click", ".printExcel", printExcel);
    }
    ;

    function initButtons() {
      var link = $("#link_create");
      link.click(function(e) {
	e.preventDefault();
	createGCForBurning(link.data("url"));
      });
    }
    ;

    function viewBurnCard() {
      $.get($(this).data("url"), function(data) {
	modalBurncard.modal('show').html(data);
	modalBurncard.on('hidden.bs.modal', function() {
	  modalBurncard.empty();
	});
      }, "html");
    }
    ;

    function editBurnCard() {
      $.get($(this).data("url"), function(data) {
	modalBurncard.modal('show').html(data);
	modalBurncard.on('hidden.bs.modal', function() {
	  modalBurncard.empty();
	});
      }, "html");
    }
    ;

    function createGCForBurning(inUrl) {
      $.ajax({
	cache: false,
	dataType: "html",
	type: "GET",
	url: inUrl,
	error: function(jqXHR, textStatus, errorThrown) {
	},
	success: function(inResponse) {
	  modalBurncard.modal('show').html(inResponse);
	  modalBurncard.on('hidden.bs.modal', function() {
	    modalBurncard.empty();
	  });
	}
      });
    }
    ;

    function printPdf() {
      window.open($(this).data("url"), '_blank', 'toolbar=0,location=0,menubar=0');
    }

    function printExcel() {
      window.location.href = $(this).data("url");
    }
  });
</script>