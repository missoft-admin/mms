package com.transretail.crm.web.controller;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.joda.time.LocalDate;
import org.joda.time.format.DateTimeFormat;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.google.common.collect.Lists;
import com.mysema.query.types.expr.BooleanExpression;
import com.transretail.crm.common.service.dto.request.AbstractSearchFormDto;
import com.transretail.crm.common.service.dto.request.PagingParam;
import com.transretail.crm.common.service.dto.response.ResultList;
import com.transretail.crm.common.service.util.SpringDataPagingUtil;
import com.transretail.crm.core.dto.EmployeePurchaseTxnDto;
import com.transretail.crm.core.dto.MemberDto;
import com.transretail.crm.core.dto.MemberMonitorSearchDto;
import com.transretail.crm.core.dto.MemberResultList;
import com.transretail.crm.core.dto.PointsDto;
import com.transretail.crm.core.dto.PointsResultList;
import com.transretail.crm.core.entity.MemberModel;
import com.transretail.crm.core.entity.enums.ReportType;
import com.transretail.crm.core.security.model.impl.CustomSecurityUserDetailsImpl;
import com.transretail.crm.core.security.service.CustomSecurityUserService;
import com.transretail.crm.core.service.CodePropertiesService;
import com.transretail.crm.core.service.EmployeePurchaseService;
import com.transretail.crm.core.service.EmployeeService;
import com.transretail.crm.core.service.LookupService;
import com.transretail.crm.core.service.MemberMonitorService;
import com.transretail.crm.core.service.MemberService;
import com.transretail.crm.core.service.PointsTxnManagerService;
import com.transretail.crm.web.controller.util.ControllerResponse;

@RequestMapping("/member/monitor")
@Controller
public class MemberMonitorController {
	@Autowired
	private MemberMonitorService memberMonitorService;
	@Autowired
	private PointsTxnManagerService pointsTxnService;
	@Autowired
	private EmployeePurchaseService purchaseService;
	@Autowired
    private CustomSecurityUserService customSecurityUserService;
	@Autowired
	private CodePropertiesService codePropertiesService;
	@Autowired
    private MessageSource messageSource;
	@Autowired
	private MemberService memberService;
	@Autowired
	private LookupService lookupService;
	
	private static final Logger LOGGER = LoggerFactory.getLogger(MemberMonitorController.class);
	
	private static final String ATTR_TRANSCATION_COUNT_IND = "transactionCountInd";
	private static final String ATTR_TRANSCATION_COUNT_EMP = "transactionCountEmp";
	private static final String ATTR_TRANSCATION_COUNT = "transactionCount";
	private static final String ATTR_MEMBER = "member";
	private static final String ATTR_IS_HR = "isHr";
	private static final String ATTR_IS_CS = "isCs";
	private static final String ATTR_REPORT_TYPES = "reportTypes";
	private static final String ATTR_IS_SUPERVISOR = "isSupervisor";
	private static final String ATTR_DATE = "currentDate";
	private static final String PROPKEY_EMP_TXN_COUNT = "propkey_monitor_employee_transactioncount";
	private static final String PROPKEY_IND_TXN_COUNT = "propkey_monitor_member_transactioncount";
	private static final String INVALID_VALUE_MSG = "propkey_msg_invalid_value";
	private static final String INDIV_KEY = "individual";
	private static final String EMP_KEY = "employee";
	private static final String HR = "hr";
	private static final String CS = "cs";
	
	private static final String PAGE_MEMBER_LIST = "/member/monitor/list";
	private static final String PAGE_POINTS_LIST = "/member/monitor/points";
	private static final String PAGE_PURCHASE_LIST = "/member/monitor/purchases";
	
	public void populatePage(String date, Model uiModel) {
		String cssCode = codePropertiesService.getCssCode();
		String hrsCode = codePropertiesService.getHrsCode();
		CustomSecurityUserDetailsImpl currentUser = getCurrentUser();
//		uiModel.addAttribute(ATTR_IS_CS, currentUser.hasAuthority(cssCode) || currentUser.hasAuthority(codePropertiesService.getCsCode()));
//		uiModel.addAttribute(ATTR_IS_HR, currentUser.hasAuthority(hrsCode) || currentUser.hasAuthority(codePropertiesService.getHrCode()));
		uiModel.addAttribute(ATTR_TRANSCATION_COUNT_IND, memberMonitorService.getTransactionCountIndividual());
		uiModel.addAttribute(ATTR_TRANSCATION_COUNT_EMP, memberMonitorService.getTransactionCountEmployee());
		uiModel.addAttribute(ATTR_IS_SUPERVISOR, currentUser.hasAuthority(cssCode) || currentUser.hasAuthority(hrsCode));
		uiModel.addAttribute(ATTR_REPORT_TYPES, ReportType.values());
		uiModel.addAttribute(ATTR_DATE, date);
	}
	
	@RequestMapping(value="/hr", produces="text/html")
	public String listHr(Model uiModel) {
		return listHr(LocalDate.now().minusDays(1).toString("dd-MM-yyyy"), uiModel);
//		populatePage(LocalDate.now().minusDays(1).toString("dd-MM-yyyy"), uiModel);
//		uiModel.addAttribute(ATTR_TRANSCATION_COUNT, memberMonitorService.getTransactionCountEmployee());
//		uiModel.addAttribute("role", HR);
//		uiModel.addAttribute(ATTR_IS_HR, true);
//		uiModel.addAttribute(ATTR_IS_CS, false);
//		return PAGE_MEMBER_LIST;
	}
	
	@RequestMapping(value="/hr/{date}", produces="text/html")
	public String listHr(@PathVariable("date") String date, Model uiModel) {
		populatePage(date, uiModel);
		uiModel.addAttribute(ATTR_TRANSCATION_COUNT, memberMonitorService.getTransactionCountEmployee());
		uiModel.addAttribute("role", HR);
		uiModel.addAttribute(ATTR_IS_HR, true);
		uiModel.addAttribute(ATTR_IS_CS, false);
		return PAGE_MEMBER_LIST;
	}
	
	@RequestMapping(value="/cs", produces="text/html")
	public String listCs(Model uiModel) {
		return listCs(LocalDate.now().minusDays(1).toString("dd-MM-yyyy"), uiModel);
//		populatePage(LocalDate.now().minusDays(1).toString("dd-MM-yyyy"), uiModel);
//		uiModel.addAttribute(ATTR_TRANSCATION_COUNT, memberMonitorService.getTransactionCountIndividual());
//		uiModel.addAttribute("role", CS);
//		uiModel.addAttribute(ATTR_IS_HR, false);
//		uiModel.addAttribute(ATTR_IS_CS, true);
//		uiModel.addAttribute("memberTypes", Lists.newArrayList(
//				lookupService.getDetailByCode(codePropertiesService.getDetailMemberTypeIndividual()),
//				lookupService.getDetailByCode(codePropertiesService.getDetailMemberTypeProfessional())));
//		return PAGE_MEMBER_LIST;
	}
	
	@RequestMapping(value="/cs/{date}", produces="text/html")
	public String listCs(@PathVariable("date") String date, Model uiModel) {
		populatePage(date, uiModel);
		uiModel.addAttribute(ATTR_TRANSCATION_COUNT, memberMonitorService.getTransactionCountIndividual());
		uiModel.addAttribute("role", CS);
		uiModel.addAttribute(ATTR_IS_HR, false);
		uiModel.addAttribute(ATTR_IS_CS, true);
		uiModel.addAttribute("memberTypes", Lists.newArrayList(
				lookupService.getDetailByCode(codePropertiesService.getDetailMemberTypeIndividual()),
				lookupService.getDetailByCode(codePropertiesService.getDetailMemberTypeProfessional())));
		return PAGE_MEMBER_LIST;
	}
	
	@RequestMapping(value="/update/{role}", produces="application/json", method=RequestMethod.POST)
	@ResponseBody
	public MemberResultList update(
			@RequestBody MemberMonitorSearchDto dto,
			@PathVariable("role") String role, Model uiModel) {
		CustomSecurityUserDetailsImpl currentUser = getCurrentUser();
		boolean isCs = currentUser.hasAuthority(codePropertiesService.getCssCode()) || currentUser.hasAuthority(codePropertiesService.getCsCode());
		boolean isHr = currentUser.hasAuthority(codePropertiesService.getHrsCode()) || currentUser.hasAuthority(codePropertiesService.getHrCode());
		
		LocalDate date = dto.getDate();
		if(date == null) {
			date = LocalDate.now();
		}
		Pageable pageable = SpringDataPagingUtil.INSTANCE.createPageable(dto.getPagination());
		LOGGER.debug("MEMBER MONITOR DATE: " + date.toString("dd-MMMM-yyyy") + " " + dto.getTransactionCount() + " " + dto.getMemberType());
		LOGGER.debug("MEMBER MONITOR PAGINATION: " + pageable.getPageSize() + " " + pageable.getOffset());
		
		if(role.equals(HR) && isHr) {
			return memberMonitorService.getMonitoredEmployees(dto);
		}
		else if (role.equals(CS) && isCs) {
			return memberMonitorService.getMonitoredMembers(dto);
		}
		
		return new MemberResultList(null, 0, false, false);
	}
	
	@RequestMapping(value="/save", produces="application/json", method=RequestMethod.POST)
	@ResponseBody
	public ControllerResponse saveTransactionCount(@RequestBody Map<String, String> transactions) {
//		for(String key : transactions.keySet()) {
//			LOGGER.debug(key + " " + transactions.get(key));
//		}
		ControllerResponse response = new ControllerResponse();
		Integer transactionCountInd = null;
		Integer transactionCountEmp = null;
		List<String> errors = new ArrayList<String>();
		Locale locale = LocaleContextHolder.getLocale();
		
		try {
			if(transactions.containsKey(INDIV_KEY)) {
				transactionCountInd = Integer.parseInt(transactions.get(INDIV_KEY));
			}
		}
		catch(NumberFormatException ne) {
			String args = messageSource.getMessage(PROPKEY_IND_TXN_COUNT, null, locale);
			errors.add(messageSource.getMessage(INVALID_VALUE_MSG, 
					new Object[]{args}, locale));
		}
		
		try {
			if(transactions.containsKey(EMP_KEY)) {
				transactionCountEmp = Integer.parseInt(transactions.get(EMP_KEY));
			}
		}
		catch(NumberFormatException ne) {
			String args = messageSource.getMessage(PROPKEY_EMP_TXN_COUNT, null, locale);
			errors.add(messageSource.getMessage(INVALID_VALUE_MSG, 
					new Object[]{args}, locale));
		}
		
		if(errors.isEmpty()) {
			CustomSecurityUserDetailsImpl currentUser = getCurrentUser();
			boolean isCss = currentUser.hasAuthority(codePropertiesService.getCssCode());
			boolean isHrs = currentUser.hasAuthority(codePropertiesService.getHrsCode());
			
			if(isCss && transactionCountInd != null) {
				memberMonitorService.setMonitoredTransactionCountIndividual(transactionCountInd);
			}
			if(isHrs && transactionCountEmp != null) {
				memberMonitorService.setMonitoredTransactionCountEmployee(transactionCountEmp);
			}
			response.setSuccess(true);
		}
		else {
			response.setResult(errors);
			response.setSuccess(false);
		}
		
		return response;
	}
	
	@RequestMapping(value="/{id}/{date}", produces="text/html")
	public String transactionList(@PathVariable("id") String id, @PathVariable("date") LocalDate date, Model uiModel) {
		MemberDto member = new MemberDto(memberService.findByAccountId(id));
		uiModel.addAttribute(ATTR_MEMBER, member);
		uiModel.addAttribute(ATTR_DATE, date.toString("dd-MM-yyyy"));
		
		if(member.getMemberType().getCode().equals(codePropertiesService.getDetailMemberTypeEmployee())) {
			return PAGE_PURCHASE_LIST;
		}
		return PAGE_POINTS_LIST;
	}
	
	@RequestMapping(value="/points/{id}", method=RequestMethod.POST, produces="application/json")
	@ResponseBody
	public PointsResultList getPointsTransactionsToday(@PathVariable String id) {
		List<PointsDto> result = pointsTxnService.getTransactionsToday(id);
		return new PointsResultList(result, result.size(), false, false);
	}
	
	@RequestMapping(value="/points/{id}/{date}", method=RequestMethod.POST, produces="application/json")
	@ResponseBody
	public PointsResultList getPointsTransactions(@PathVariable("id") String id, @PathVariable("date") LocalDate date) {
		List<PointsDto> result = pointsTxnService.getTransactionsByDate(id, date);
		return new PointsResultList(result, result.size(), false, false);
	}
	
	@RequestMapping(value="/purchases/{id}", method=RequestMethod.POST, produces="application/json")
	@ResponseBody
	public ResultList<EmployeePurchaseTxnDto> getPurchaseTransactionsToday(@PathVariable String id) {
		List<EmployeePurchaseTxnDto> result = purchaseService.getTransactionsToday(id);
		LOGGER.debug("PURCHASES: " + result.size());
		return new ResultList<EmployeePurchaseTxnDto>(result, result.size(), false, false);
	}
	
	@RequestMapping(value="/purchases/{id}/{date}", method=RequestMethod.POST, produces="application/json")
	@ResponseBody
	public ResultList<EmployeePurchaseTxnDto> getPurchaseTransactions(@PathVariable("id") String id, @PathVariable("date") LocalDate date) {
		List<EmployeePurchaseTxnDto> result = purchaseService.getTransactionsByDate(id, date);
		LOGGER.debug("PURCHASES: " + result.size());
		return new ResultList<EmployeePurchaseTxnDto>(result, result.size(), false, false);
	}
	
	private CustomSecurityUserDetailsImpl getCurrentUser() {
		return (CustomSecurityUserDetailsImpl) customSecurityUserService.getCurrentAuth().getPrincipal();
	}
}
