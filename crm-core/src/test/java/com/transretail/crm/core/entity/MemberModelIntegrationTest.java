package com.transretail.crm.core.entity;

import com.transretail.crm.core.entity.enums.MemberStatus;
import com.transretail.crm.core.repo.MemberRepo;
import com.transretail.crm.core.service.MemberService;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Configurable;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;
import java.util.Iterator;
import java.util.List;

import static org.junit.Assert.assertEquals;

@Configurable
@Transactional
@ContextConfiguration(locations = "classpath*:/META-INF/spring/applicationContext*.xml")
@RunWith(SpringJUnit4ClassRunner.class)
public class MemberModelIntegrationTest {

    @Autowired
    MemberModelDataOnDemand dod;
    @Autowired
    MemberService memberService;
    @Autowired
    MemberRepo memberRepo;

    @Test
    public void testMarkerMethod() {
    }

    @Test
    public void testCountAllCustomerModels() {
        Assert.assertNotNull("Data on demand for 'MemberModel' failed to initialize correctly", dod.getRandomCustomerModel());
        long count = memberService.countAllCustomerModels();
        Assert.assertTrue("Counter for 'MemberModel' incorrectly reported there were no entries", count > 0);
    }

    @Test
    public void testFindCustomerModel() {
        MemberModel obj = dod.getRandomCustomerModel();
        Assert.assertNotNull("Data on demand for 'MemberModel' failed to initialize correctly", obj);
        Long id = obj.getId();
        Assert.assertNotNull("Data on demand for 'MemberModel' failed to provide an identifier", id);
        obj = memberService.findCustomerModel(id);
        Assert.assertNotNull("Find method for 'MemberModel' illegally returned null for id '" + id + "'", obj);
        Assert.assertEquals("Find method for 'MemberModel' returned the incorrect identifier", id, obj.getId());
    }

    @Test
    public void testFindAllCustomerModels() {
        Assert.assertNotNull("Data on demand for 'MemberModel' failed to initialize correctly", dod.getRandomCustomerModel());
        long count = memberService.countAllCustomerModels();
        Assert.assertTrue("Too expensive to perform a find all test for 'MemberModel', as there are " + count + " entries; set the findAllMaximum to exceed this value or set findAll=false on the integration test annotation to disable the test", count < 250);
        List<MemberModel> result = memberService.findAllCustomerModels();
        Assert.assertNotNull("Find all method for 'MemberModel' illegally returned null", result);
        Assert.assertTrue("Find all method for 'MemberModel' failed to return any data", result.size() > 0);
    }

    @Test
    public void testFindCustomerModelEntries() {
        Assert.assertNotNull("Data on demand for 'MemberModel' failed to initialize correctly", dod.getRandomCustomerModel());
        long count = memberService.countAllCustomerModels();
        if (count > 20) count = 20;
        int firstResult = 0;
        int maxResults = (int) count;
        List<MemberModel> result = memberService.findCustomerModelEntries(firstResult, maxResults);
        Assert.assertNotNull("Find entries method for 'MemberModel' illegally returned null", result);
        Assert.assertEquals("Find entries method for 'MemberModel' returned an incorrect number of entries", count, result.size());
    }

    @Test
    public void testFlush() {
        MemberModel obj = dod.getRandomCustomerModel();
        Assert.assertNotNull("Data on demand for 'MemberModel' failed to initialize correctly", obj);
        Long id = obj.getId();
        Assert.assertNotNull("Data on demand for 'MemberModel' failed to provide an identifier", id);
        obj = memberService.findCustomerModel(id);
        Assert.assertNotNull("Find method for 'MemberModel' illegally returned null for id '" + id + "'", obj);
        boolean modified = dod.modifyCustomerModel(obj);
        Integer currentVersion = obj.getVersion();
        memberRepo.flush();
        Assert.assertTrue("Version for 'MemberModel' failed to increment on flush directive", (currentVersion != null && obj.getVersion() > currentVersion) || !modified);
    }

    @Test
    public void testUpdateCustomerModelUpdate() {
        MemberModel obj = dod.getRandomCustomerModel();
        Assert.assertNotNull("Data on demand for 'MemberModel' failed to initialize correctly", obj);
        Long id = obj.getId();
        Assert.assertNotNull("Data on demand for 'MemberModel' failed to provide an identifier", id);
        obj = memberService.findCustomerModel(id);
        boolean modified = dod.modifyCustomerModel(obj);
        Integer currentVersion = obj.getVersion();
        MemberModel merged = memberService.updateCustomerModel(obj);
        memberRepo.flush();
        Assert.assertEquals("Identifier of merged object not the same as identifier of original object", merged.getId(), id);
        Assert.assertTrue("Version for 'MemberModel' failed to increment on merge and flush directive", (currentVersion != null && obj.getVersion() > currentVersion) || !modified);
    }

    @Test
    public void testSaveCustomerModel() {
        Assert.assertNotNull("Data on demand for 'MemberModel' failed to initialize correctly", dod.getRandomCustomerModel());
        MemberModel obj = dod.getNewTransientCustomerModel(Integer.MAX_VALUE);
        Assert.assertNotNull("Data on demand for 'MemberModel' failed to provide a new transient entity", obj);
        Assert.assertNull("Expected 'MemberModel' identifier to be null", obj.getId());
        try {
            memberService.saveCustomerModel(obj);
        } catch (final ConstraintViolationException e) {
            final StringBuilder msg = new StringBuilder();
            for (Iterator<ConstraintViolation<?>> iter = e.getConstraintViolations().iterator(); iter.hasNext(); ) {
                final ConstraintViolation<?> cv = iter.next();
                msg.append("[").append(cv.getRootBean().getClass().getName()).append(".").append(cv.getPropertyPath()).append(": ").append(cv.getMessage()).append(" (invalid value = ").append(cv.getInvalidValue()).append(")").append("]");
            }
            throw new IllegalStateException(msg.toString(), e);
        }
        memberRepo.flush();
        Assert.assertNotNull("Expected 'MemberModel' identifier to no longer be null", obj.getId());
    }

    @Test
    public void testDeleteCustomerModel() {
        MemberModel obj = dod.getRandomCustomerModel();
        assertEquals(MemberStatus.ACTIVE, obj.getAccountStatus());
        Assert.assertNotNull("Data on demand for 'MemberModel' failed to initialize correctly", obj);
        Long id = obj.getId();
        Assert.assertNotNull("Data on demand for 'MemberModel' failed to provide an identifier", id);
        memberService.deleteCustomerModel(obj);
        obj = memberService.findCustomerModel(id);
        assertEquals(MemberStatus.INACTIVE, obj.getAccountStatus());
    }
    
    @Test
    public void testSupplementaryMembers() {
    	MemberModel parent = dod.getSpecificCustomerModel(1);
    	MemberModel supplement = dod.getSpecificCustomerModel(2);
    	
    	memberService.addSupplementaryMember(parent.getAccountId(), supplement);
    	MemberModel savedSupplement = memberService.findByAccountId(supplement.getAccountId());
    	
    	Assert.assertSame(parent.getAccountId(), savedSupplement.getParentAccountId());
    	Assert.assertEquals(1, memberService.findSupplementaryMembersByParentId(parent.getAccountId()).size(), 0);
    }
}
