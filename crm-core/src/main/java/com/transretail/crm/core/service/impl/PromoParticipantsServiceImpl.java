package com.transretail.crm.core.service.impl;

import com.google.common.collect.Maps;
import com.mysema.query.jpa.JPASubQuery;
import com.mysema.query.jpa.impl.JPAQuery;
import com.mysema.query.types.ConstructorExpression;
import com.mysema.query.types.Projections;
import com.transretail.crm.common.reporting.engine.data.JRQueryDSLDataSource;
import com.transretail.crm.common.reporting.engine.data.JRQueryDSLDataSource.FieldValueCustomizer;
import com.transretail.crm.common.reporting.jasper.ClasspathFileResolver;
import com.transretail.crm.common.reporting.jasper.DefaultJRProcessor;
import com.transretail.crm.common.reporting.jasper.JRProcessor;
import com.transretail.crm.core.entity.*;
import com.transretail.crm.core.entity.lookup.QProduct;
import com.transretail.crm.core.service.PromoParticipantsService;
import com.transretail.crm.core.util.FormatterUtil;
import java.util.Date;
import java.util.Map;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import net.sf.jasperreports.engine.JRParameter;
import net.sf.jasperreports.engine.JRVirtualizer;
import net.sf.jasperreports.engine.fill.JRSwapFileVirtualizer;
import net.sf.jasperreports.engine.util.JRSwapFile;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service("promoParticipantsService")
@Transactional
public class PromoParticipantsServiceImpl implements PromoParticipantsService {
    @PersistenceContext
    private EntityManager em;
    
    @Override
    @Transactional(readOnly = true)
    public JRProcessor createJrProcessor(Long promotionId) {
        final QTransactionAudit qaudit = QTransactionAudit.transactionAudit;
        QMemberModel qmember = QMemberModel.memberModel;
        QPosPayment qpayment = QPosPayment.posPayment;
        QPosEftPayment qeft = QPosEftPayment.posEftPayment;
        final QProduct qproduct = QProduct.product;
        JPAQuery query = new JPAQuery(em).from(qaudit, qpayment)
                .leftJoin(qaudit.memberModel, qmember)
                .leftJoin(qpayment.eftPayment, qeft)
                .groupBy(qmember.accountId,
                        qmember.firstName,
                        qmember.lastName,
                        qmember.customerProfile.birthdate,
                        qmember.customerProfile.gender.description,
                        qmember.ktpId,
                        qmember.address.street,
                        qmember.contact,
                        qmember.email,
                        qaudit.productId,
                        qpayment.mediaType,
                        qeft.cardNo)
                .orderBy(qmember.accountId.asc(),
                        qaudit.productId.asc(),
                        qpayment.mediaType.asc(),
                        qeft.cardNo.asc())
                .where(qaudit.promotion.id.eq(promotionId)
                        .and(qaudit.transactionNo.eq(qpayment.posTransaction.id)));

        //<editor-fold defaultstate="collapsed" desc="Old Implementation - Retain for reference">
        //        JPAQuery query = new JPAQuery(em)
        //                .from(qaudit,
        //                        qpoint,
        //                        qpayment/*,
        //                 qeft,
        //                 qproduct*/
        //                )
        //                .leftJoin(qaudit.memberModel, qmember)
        //                .where(
        //                        qaudit.transactionNo.eq(qpoint.transactionNo).and(qaudit.transactionPoints.eq(qpoint.transactionPoints.longValue())),
        //                        qpayment.posTransaction.id.eq(qaudit.transactionNo).and(qpayment.amount.eq(qpoint.transactionAmount)),
        //                        /*qeft.posPayment.id.eq(qpayment.id),*/
        //                        /*qaudit.productId.eq(qproduct.id),*/
        //                        qaudit.promotion.id.eq(promotionId));
        ////                    .orderBy(qaudit.memberModel.id.asc(), qaudit.created.asc());
        //            /*.limit(500) for testing*/
        //</editor-fold>
        
        ConstructorExpression<PromoParticipants> expression = Projections.constructor(PromoParticipants.class,
                qmember.accountId,
                qmember.firstName,
                qmember.lastName,
                qmember.customerProfile.birthdate,
                qmember.customerProfile.gender.description,
                qmember.ktpId,
                qmember.address.street,
                qmember.contact,
                qmember.email,
//                new JPASubQuery().from(qproduct)
//                                .where(qproduct.id.eq(qaudit.productId))
//                                .unique(qproduct.description),
                qaudit.productId,
                qpayment.amount.sum(),
                qpayment.mediaType,
                qeft.cardNo,
                qaudit.transactionPoints.sum());
        
        JRQueryDSLDataSource datasource = new JRQueryDSLDataSource(query, expression //);
        , new FieldValueCustomizer() {
            private final Map<Object, Object> cache = Maps.newHashMap();

            @Override
            public Object customize(String propertyName, Object value) {
                if ("item".equals(propertyName) && value != null) {
                    final Object itemCache = cache.get(value);
                    if (itemCache == null) {
                        String description = new JPAQuery(em).from(qproduct)
                                .where(qproduct.id.eq(String.valueOf(value)))
                                .singleResult(qproduct.description);
                        cache.put(value, description);
                        return description;
                    }
                    return itemCache;
                }
                return value;
            }
        });
        
        DefaultJRProcessor jrProcessor = new DefaultJRProcessor("reports/promo_participants.jasper");
        QPromotion qpromo = QPromotion.promotion;
        
        Map<String, Object> params = Maps.newHashMap();
        params.put("PROMO_NAME", new JPAQuery(em).from(qpromo).where(qpromo.id.eq(promotionId)).singleResult(qpromo.name));
        params.put("PROMO_PARTICIPANTS", datasource);
        
        JRSwapFile swapFile = new JRSwapFile("/tmp", 1024, 1024);
        JRVirtualizer virtualizer = new JRSwapFileVirtualizer(50, swapFile, true);
        jrProcessor.addParameter(JRParameter.REPORT_VIRTUALIZER, virtualizer);
        
        jrProcessor.addParameters(params);
        jrProcessor.setFileResolver(new ClasspathFileResolver("reports"));
        return jrProcessor;
    }
	@Override
	public boolean isExistParticipants(Long promoId) {
		QTransactionAudit qtrans = QTransactionAudit.transactionAudit;
		return new JPAQuery(em).from(qtrans).where(qtrans.promotion.id.eq(promoId)).exists();
	}
	
	public static class PromoParticipants {
		private String accountId;
		private String firstName;
		private String lastName;
		private Date birthdate;
		private String gender;
		private String ktpId;
		private String address;
		private String mobileNo;
		private String email;
		private String item;
		private Double totalPurchase;
		private String paymentMedia;
		private String cardNo;
		private Long earnedPoints;
		
		public PromoParticipants(String accountId, String firstName,
				String lastName, Date birthdate, String gender, String ktpId,
				String address, String mobileNo, String email, String item,
				Double totalPurchase, String paymentMedia,
				Long earnedPoints) {
			super();
			this.accountId = accountId;
			this.firstName = firstName;
			this.lastName = lastName;
			this.birthdate = birthdate;
			this.gender = gender;
			this.ktpId = ktpId;
			this.address = address;
			this.mobileNo = mobileNo;
			this.email = email;
			this.item = item;
			this.totalPurchase = totalPurchase;
			this.paymentMedia = paymentMedia;
			this.earnedPoints = earnedPoints;
		}
		
		
		public PromoParticipants(String accountId, String firstName,
				String lastName, Date birthdate, String gender, String ktpId,
				String address, String mobileNo, String email, String item,
				Double totalPurchase, String paymentMedia, String cardNo,
				Long earnedPoints) {
			super();
			this.accountId = accountId;
			this.firstName = firstName;
			this.lastName = lastName;
			this.birthdate = birthdate;
			this.gender = gender;
			this.ktpId = ktpId;
			this.address = address;
			this.mobileNo = mobileNo;
			this.email = email;
			this.item = item;
			this.totalPurchase = totalPurchase;
			this.paymentMedia = paymentMedia;
			this.cardNo = cardNo;
			this.earnedPoints = earnedPoints;
		}
		public String getAccountId() {
			return accountId;
		}
		public void setAccountId(String accountId) {
			this.accountId = accountId;
		}
		public String getFirstName() {
			return firstName;
		}
		public void setFirstName(String firstName) {
			this.firstName = firstName;
		}
		public String getLastName() {
			return lastName;
		}
		public void setLastName(String lastName) {
			this.lastName = lastName;
		}
		
		public String getFullName() {
			return FormatterUtil.INSTANCE.formatName(lastName, firstName);
		}
		
		public Date getBirthdate() {
			return birthdate;
		}
		public void setBirthdate(Date birthdate) {
			this.birthdate = birthdate;
		}
		public String getGender() {
			return gender;
		}
		public void setGender(String gender) {
			this.gender = gender;
		}
		public String getKtpId() {
			return ktpId;
		}
		public void setKtpId(String ktpId) {
			this.ktpId = ktpId;
		}
		
		
		public String getAddress() {
			return address;
		}
		public void setAddress(String address) {
			this.address = address;
		}
		public String getMobileNo() {
			return mobileNo;
		}
		public void setMobileNo(String mobileNo) {
			this.mobileNo = mobileNo;
		}
		public String getEmail() {
			return email;
		}
		public void setEmail(String email) {
			this.email = email;
		}
		public String getItem() {
			return item;
		}
		public void setItem(String item) {
			this.item = item;
		}
		public Double getTotalPurchase() {
			return totalPurchase;
		}
		public void setTotalPurchase(Double totalPurchase) {
			this.totalPurchase = totalPurchase;
		}
		public String getPaymentMedia() {
			return paymentMedia;
		}
		public void setPaymentMedia(String paymentMedia) {
			this.paymentMedia = paymentMedia;
		}
		public String getCardNo() {
			return cardNo;
		}
		public void setCardNo(String cardNo) {
			this.cardNo = cardNo;
		}
		public Long getEarnedPoints() {
			return earnedPoints;
		}
		public void setEarnedPoints(Long earnedPoints) {
			this.earnedPoints = earnedPoints;
		}
	}
}