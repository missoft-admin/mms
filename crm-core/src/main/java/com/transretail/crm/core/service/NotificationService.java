package com.transretail.crm.core.service;

import java.util.List;

import com.transretail.crm.core.entity.Notification;
import com.transretail.crm.core.entity.UserModel;

public interface NotificationService {
	public Notification saveNotification(Notification notification, List<UserModel> users);
	public List<Notification> getNotificationsByUser(UserModel user);
	public List<Notification> getUnreadNotifications(UserModel user);
	public void setNotificationAsRead(Notification notification, UserModel user);
	public void deleteNotification(String itemId);
	public void deleteNotification(List<Notification> notifications);
}
