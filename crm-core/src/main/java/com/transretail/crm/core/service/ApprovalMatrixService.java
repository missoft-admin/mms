package com.transretail.crm.core.service;

import java.util.List;

import com.transretail.crm.core.dto.ApprovalMatrixDto;
import com.transretail.crm.core.dto.ApprovalMatrixResultList;
import com.transretail.crm.core.dto.ApprovalMatrixSearchDto;
import com.transretail.crm.core.dto.ServiceDto;
import com.transretail.crm.core.entity.ApprovalMatrixModel;
import com.transretail.crm.core.entity.UserModel;


public interface ApprovalMatrixService {

    ApprovalMatrixModel save( ApprovalMatrixModel inMatrix );

	Long findHighestApprovalPointsOfCurrentUser();

	ServiceDto retrieveAdjustPointsApprovalMatrix();

	ApprovalMatrixResultList searchApprover(ApprovalMatrixSearchDto theSearchDto);

	void saveDto(ApprovalMatrixDto inMatrix);

	ApprovalMatrixDto findDto(Long inId);

	void delete(Long inId);

	List<UserModel> findPointsOverrideApprovers();

}
