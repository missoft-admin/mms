package com.transretail.crm.product.rest;

import java.util.Map;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.google.common.collect.Maps;

/**
 * @author Allan G. Ramirez (agramirez@exist.com)
 */
@Controller
@RequestMapping("/app")
public class AppInfoController {
    @Value("#{'${project.version}'}")
    private String projectVersion;
    @Value("#{'${git.branch}'}")
    private String gitBranch;
    @Value("#{'${git.commit.id}'}")
    private String gitCommitId;
    @Value("#{'${git.commit.id.abbrev}'}")
    private String gitCommitIdAbbrev;
    @Value("#{'${git.commit.user.name}'}")
    private String gitCommitUserName;
    @Value("#{'${git.commit.message.full}'}")
    private String gitCommitMessage;
    @Value("#{'${git.commit.time}'}")
    private String gitCommitTime;

    @RequestMapping("/info")
    @ResponseBody
    public Map<String, String> getGitInfo() {
        Map<String, String> map = Maps.newHashMap();
        map.put("project.version", projectVersion);
        map.put("git.branch", gitBranch);
        map.put("git.commit.id", gitCommitId);
        map.put("git.commit.id.abbrev", gitCommitIdAbbrev);
        map.put("git.commit.user.name", gitCommitUserName);
        map.put("git.commit.message.full", gitCommitMessage);
        map.put("git.commit.time", gitCommitTime);
        return map;
    }
}
