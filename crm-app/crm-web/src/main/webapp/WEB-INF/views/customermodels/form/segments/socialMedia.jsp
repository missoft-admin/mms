<%@ include file="../../../common/taglibs.jsp" %>


  <!-- Contact Details -->
  <fieldset class="form-horizontal container-fluid span4">
    <legend><spring:message code="label_member_socialmedia" /></legend>
    <div id="socialMediaAccts" class="controls checkbox">
      <form:checkboxes items="${socialMedia}" path="socialMediaAccts" itemValue="code" itemLabel="description" />
      <form:input path="socialMediaAccts" id="socialMediaAcctsOthersVal" cssClass="hide input-small"/>
    </div>
  </fieldset>


<script>
$(document).ready( function() {
		var $socialMedia = $( "#socialMediaAccts" );
		var $others = $socialMedia.find( "#socialMediaAccts5" );
		var $othersVal = $socialMedia.find( "#socialMediaAcctsOthersVal" );
	
		initFields();
	
		function initFields() {
				$others.click( function(e) {
						if ( this.checked ) {
							  $othersVal.show();
						}
						else {
							  $othersVal.val("");
							  $othersVal.hide();
						}
				});
		}
});
</script>