<%@ include file="../common/taglibs.jsp" %>

  <style type="text/css">
  <!--
    #list_txn .dataTable tr.odd td:first-child {
      background-color: #DFE1E3;
    }
    #list_txn .dataTable tr.even td:first-child {
      background-color: #E8E8E8;
    }
    #list_txn .dataTable tr:hover td:first-child {
      background: #f4f4f4;
    }
    #list_txn .dataTable tr.odd td:nth-child(2) {
        background-color: #24A0D3;
        color: #FFFFFF;
    }
    #list_txn .dataTable tr.even td:nth-child(2) {
        background-color: #2EA8D9;
        color: #FFFFFF;
    }
    #list_txn .dataTable tr:hover td:nth-child(2) {
        background-color: #197195;
    }
    #list_txn .dataTable tr td:first-child {
    width: 75px;
    }
    #list_txn .dataTable tr td:last-child {
    border-left: 0px;
    }
  -->
  </style>
  
  <script type="text/javascript">
  var IS_APPROVER = ${isApprover};
  </script>
  
  <spring:message var="typeName" code='menu_item_employeepurchasetxnad_new_label'/>
  <spring:url value="/resources/images/update.png" var="update_image_url" />
  <spring:url value="/resources/images/delete.png" var="delete_image_url" />
  
  <div class="page-header page-header2">
      <h1><spring:message code="menutab_employeemgmt_helpdesk_purchaseadjustment" /></h1>
  </div>
  
  <c:if test="${!isApprover}">
  <div>
  <button class="btn btn-primary adjLnk" 
    data-url="${pageContext.request.contextPath}/employee/adjustments/purchases?form" 
    data-query-str="${pageContext.request.queryString}">
        <spring:message code="label_com_transretail_crm_entity_employeepurchasetxnmodel" var="argument"/>
        <spring:message code="global_menu_adjust" arguments="${ argument }"/>
  </button>
  </div>
  <br/>
  </c:if>
  
  <div id="content_txnList">

  <div id="list_txn" class="data-table-01"
    data-url="<c:url value="/employee/adjustments/forapproval/list" />"
    data-hdr-employee="<spring:message code="label_com_transretail_crm_entity_employeemodel" />"
    data-hdr-location="<spring:message code="label_com_transretail_crm_entity_employeepurchasetxnmodel_transactionlocation"/>"
    data-hdr-date="<spring:message code="label_com_transretail_crm_entity_employeepurchasetxnmodel_transactiondatetime"/>"
    data-hdr-txn-no="<spring:message code="label_com_transretail_crm_entity_employeepurchasetxnad_transactionno"/>" 
    data-hdr-txn-amount="<spring:message code="label_com_transretail_crm_entity_employeepurchasetxnad_transactionamount" />"
    data-hdr-requested-by="<spring:message code="label_com_transretail_crm_entity_employeepurchasetxnad_requestedby" />"
    data-hdr-request-date="<spring:message code="label_com_transretail_crm_entity_employeepurchasetxnad_requestdate" />"
    data-hdr-approved-by="<spring:message code="label_com_transretail_crm_entity_employeepurchasetxnad_approvedby" />"
    data-hdr-approval-date="<spring:message code="label_com_transretail_crm_entity_employeepurchasetxnad_approvaldate" />" 
    data-hdr-status="<spring:message code="purchase_prop_status" />"></div>

  </div>
  
  <div class="hide">
  <div id="btn">
    <spring:message arguments="${typeName}" code="entity_update" var="update_label" htmlEscape="false" />
    <a title="${fn:escapeXml(update_label)}" alt="${fn:escapeXml(update_label)}" class="adjLnk btn btn-primary pull-left icn-edit"
      data-item-id="%ITEMID%"
      data-url="${pageContext.request.contextPath}/employee/adjustments/purchases/%ITEMID%?form"
      data-query-str="${pageContext.request.queryString}"
      href="#" id="">
      <c:if test="${!isApprover}"><spring:message code="label_edit" /></c:if>
      <c:if test="${isApprover}"><spring:message code="label_process" /></c:if></a>
    <c:if test="${!isApprover}">
    <c:set var="delete_confirm_msg"><spring:escapeBody 
        javaScriptEscape="true"><spring:message
        code="entity_delete_confirm" /></spring:escapeBody></c:set>
    <input class="btn btn-primary icn-delete pull-left" type="button" alt="${fn:escapeXml(delete_label)}" title="${fn:escapeXml(delete_label)}" value="${delete_label}" onclick="getConfirm('${delete_confirm_msg}',function(result) {if(result) $('form#deleteForm%ITEMID%').submit();});" />
    <spring:url value="/employee/adjustments/purchases/%ITEMID%" var="delete_form_url" /> 
    <form:form action="${delete_form_url}" id="deleteForm%ITEMID%" method="DELETE" cssClass="form-reset">
      <spring:message arguments="${typeName}" code="entity_delete" var="delete_label" htmlEscape="false" />
      <c:if test="${not empty param.page}">
        <input name="page" type="hidden" value="1" />
      </c:if>
      <c:if test="${not empty param.size}">
        <input name="size" type="hidden" value="${fn:escapeXml(param.size)}" />
      </c:if>
    </form:form>
    </c:if>
  </div>
  </div>
   
  <link href="<spring:url value="/css/dataTables.css" />" rel="stylesheet">
  <script src="<spring:url value="/js/jquery/jquery.dataTables.js" />" ></script>
  <script src="<spring:url value="/js/jquery/jquery.dataTables.linkLengthMenu.js" />"></script>
  <script src="<spring:url value="/js/jquery/jquery.dataTables.customPaging.js" />"></script>
  <script src="<spring:url value="/js/jquery/ajax-datatable.js" />" ></script>
  <script src="<spring:url value="/js/common/form2js.js" />" ></script>
  <script src="<spring:url value="/js/common/json2.js" />" ></script>
  <script src="<spring:url value="/js/jquery/jquery.toObject.js" />" ></script>
  <script src="<spring:url value="/js/jquery/jquery.numberInput.js" />" ></script>
  <jsp:include page="../common/confirm.jsp" />
  <jsp:include page="adjustmentDialog.jsp" />
  <script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/viewspecific/empoyeepurchaseadjustments/adjustments.js"><![CDATA[&nbsp;]]></script>

  <link href="<c:url value="/styles/temp.css" />" rel="stylesheet" />