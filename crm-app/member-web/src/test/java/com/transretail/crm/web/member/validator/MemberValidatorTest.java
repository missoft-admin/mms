package com.transretail.crm.web.member.validator;

import static org.junit.Assert.assertEquals;

import java.util.Date;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.ImportResource;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.AnnotationConfigContextLoader;
import org.springframework.validation.BeanPropertyBindingResult;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import com.transretail.crm.core.entity.MemberModel;
import com.transretail.crm.core.entity.embeddable.CustomerProfile;
import com.transretail.crm.core.entity.enums.Status;
import com.transretail.crm.core.entity.lookup.LookupDetail;
import com.transretail.crm.core.entity.lookup.LookupHeader;
import com.transretail.crm.core.entity.lookup.MemberType;
import com.transretail.crm.core.repo.LookupDetailRepo;
import com.transretail.crm.core.repo.LookupHeaderRepo;
import com.transretail.crm.core.repo.MemberRepo;
import com.transretail.crm.web.member.bean.RegisterDto;

/**
 * @author Allan G. Ramirez (agramirez@exist.com)
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(loader = AnnotationConfigContextLoader.class)
public class MemberValidatorTest {
    @Configuration
    @ImportResource({"classpath:META-INF/spring/applicationContext-test.xml",
            "classpath:META-INF/spring/applicationContext-core.xml"})
    static class ContextConfiguration {
        @Bean
        public MemberValidator memberValidator() {
            return new MemberValidator();
        }
    }

    private static final String USERNAME = "agramirez";
    private static final String CONTACT = "agramirez";
    private static final String EMAIL = "agramirez@exist.com";
    @Autowired
    @Qualifier("memberValidator")
    private Validator validator;
    @Autowired
    private MemberRepo memberRepo;
    private Long savedMemberId;
    
    @Autowired
    LookupHeaderRepo lookupHeaderRepo;
    
    @Autowired
    LookupDetailRepo lookupDetailRepo; 

    @Before
    public void onSetup() {
    	LookupHeader header = new LookupHeader();
    	header.setCode("gender");
    	header.setDescription("gender");
    	header = lookupHeaderRepo.saveAndFlush(header);
    	
    	LookupDetail detail = new LookupDetail();
    	detail.setHeader(header);
    	detail.setCode("m");
    	detail.setDescription("m");
    	detail.setStatus(Status.ACTIVE);
    	detail = lookupDetailRepo.saveAndFlush(detail);
    	
        MemberType memberType = new MemberType("INDIVIDUAL");

        memberRepo.deleteAll();
        MemberModel model = new MemberModel();
        model.setPin("pinpinpin");
        model.setUsername(USERNAME);
        model.setLastName("ramirez");
        model.setFirstName("cloudx");
        model.setEmail(EMAIL);
        model.setPassword("Y3h3Y!");
        CustomerProfile profile = new CustomerProfile();
        profile.setGender(detail);
        profile.setBirthdate(new Date());
        model.setCustomerProfile(profile);
        model.setEnabled(true);
        model.setContact(CONTACT);
        model.setAccountId("accountId");
//        model.setMemberType(memberType);
        savedMemberId = memberRepo.saveAndFlush(model).getId();
    }

    @After
    public void tearDown() {
        if (savedMemberId != null) {
            memberRepo.delete(savedMemberId);
        }
    }

    @Test
    public void validateTest() {
        RegisterDto form = new RegisterDto();
        // Username and contact error
        form.setUsername(USERNAME);
        form.setContact(CONTACT);
        form.setEmail(EMAIL);
        Errors result = new BeanPropertyBindingResult(form, "form");
        validator.validate(form, result);
        assertEquals(4, result.getFieldErrorCount());
        assertEquals("member.username.exists", result.getFieldError("username").getCode());
        assertEquals("member.contact.exists", result.getFieldError("contact").getCode());
        assertEquals("member.email.exists", result.getFieldError("email").getCode());

        // Username error
        form.setUsername(USERNAME);
        form.setContact("12345");
        form.setEmail("ramirezag@gmail.com");
        result = new BeanPropertyBindingResult(form, "form");
        validator.validate(form, result);
        assertEquals(1, result.getFieldErrorCount());
        assertEquals("member.username.exists", result.getFieldError("username").getCode());

        // Contact error
        form.setUsername("user");
        form.setContact(CONTACT);
        form.setEmail("ramirezag@gmail.com");
        result = new BeanPropertyBindingResult(form, "form");
        validator.validate(form, result);
        assertEquals(2, result.getFieldErrorCount());
        assertEquals("member.contact.exists", result.getFieldError("contact").getCode());

        // No error
        form.setUsername("user");
        form.setContact("123 456-789");
        result = new BeanPropertyBindingResult(form, "form");
        validator.validate(form, result);
        assertEquals(0, result.getErrorCount());
    }
}
