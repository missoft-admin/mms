package com.transretail.crm.schedule.service.impl;

import com.transretail.crm.common.service.dto.request.PageSortDto;
import com.transretail.crm.common.service.dto.response.ResultList;
import com.transretail.crm.common.service.util.SpringDataPagingUtil;
import com.transretail.crm.core.dto.ApplicationConfigDto;
import com.transretail.crm.core.entity.ApplicationConfig;
import com.transretail.crm.core.entity.QApplicationConfig;
import com.transretail.crm.core.entity.enums.AppConfigDefaults;
import com.transretail.crm.core.entity.enums.AppKey;
import com.transretail.crm.core.repo.ApplicationConfigRepo;
import com.transretail.crm.schedule.service.AppConfigService;
import com.transretail.crm.schedule.trigger.*;
import org.quartz.Scheduler;
import org.quartz.SchedulerException;
import org.quartz.TriggerKey;
import org.quartz.impl.triggers.CronTriggerImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


@Service("appConfigService")
@Transactional
public class AppConfigServiceImpl implements AppConfigService {
	
	@Autowired
	private ApplicationConfigRepo appConfigRepo;
	@Autowired
    private Scheduler scheduler;
	
	private static Map<AppKey, Trigger> APP_CONFIG_TRIGGERS = new HashMap<AppKey, Trigger>();
	private static Map<AppKey, String> APP_CONFIGS_DEFAULTS = new HashMap<AppKey, String>();
	
	static {
		APP_CONFIG_TRIGGERS.put(AppKey.REWARDS_JOB, new Trigger(PointsRewardSchedulerTrigger.TRIGGER_NAME, PointsRewardSchedulerTrigger.TRIGGER_GROUP));
		APP_CONFIG_TRIGGERS.put(AppKey.EXPIRE_JOB, new Trigger(ExpireJobSchedulerTrigger.TRIGGER_NAME, ExpireJobSchedulerTrigger.TRIGGER_GROUP));
		APP_CONFIG_TRIGGERS.put(AppKey.LOYALTY_EXPIRY_NOTIFICATION, new Trigger(ExpireLoyaltySchedulerTrigger.TRIGGER_NAME, ExpireLoyaltySchedulerTrigger.TRIGGER_GROUP));
		APP_CONFIG_TRIGGERS.put(AppKey.MARKETING_CHANNEL, new Trigger(MarketingChannelSchedulerTrigger.TRIGGER_NAME, MarketingChannelSchedulerTrigger.TRIGGER_GROUP));
		APP_CONFIG_TRIGGERS.put(AppKey.MEMBER_MONITOR_NOTIF_JOB, new Trigger(MemberMonitorNotifSchedulerTrigger.TRIGGER_NAME, MemberMonitorNotifSchedulerTrigger.TRIGGER_GROUP));
        APP_CONFIG_TRIGGERS.put(AppKey.SAFETY_STOCK_JOB, new Trigger(SafetyStockSchedulerTrigger.TRIGGER_NAME, SafetyStockSchedulerTrigger.TRIGGER_GROUP));
		APP_CONFIG_TRIGGERS.put(AppKey.DISCOUNT_EOC_JOB, new Trigger(DiscountEOCSchedulerTrigger.TRIGGER_NAME, DiscountEOCSchedulerTrigger.TRIGGER_GROUP));
		APP_CONFIG_TRIGGERS.put(AppKey.GC_TXN_SETTLEMENT_JOB, new Trigger(GcTxnSettlementSchedulerTrigger.TRIGGER_NAME, GcTxnSettlementSchedulerTrigger.TRIGGER_GROUP));
		APP_CONFIG_TRIGGERS.put(AppKey.GC_CLAIMS_TO_AFFILIATES_JOB, new Trigger(ClaimToAffiliatesScheduleTrigger.TRIGGER_NAME, ClaimToAffiliatesScheduleTrigger.TRIGGER_GROUP));

		APP_CONFIGS_DEFAULTS.put(AppKey.REWARDS_JOB, AppConfigDefaults.DEFAULT_REWARDS_JOB_CRON);
		APP_CONFIGS_DEFAULTS.put(AppKey.PRODUCT_HIER_UPDATER_CRON, AppConfigDefaults.DEFAULT_PRODUCT_HIER_UPDATER_CRON);
		APP_CONFIGS_DEFAULTS.put(AppKey.POINTS_VALUE, AppConfigDefaults.DEFAULT_POINTS_VALUE);
		APP_CONFIGS_DEFAULTS.put(AppKey.SUSPENDED_FROM_POINTS, AppConfigDefaults.DEFAULT_SUSPENDED_FROM_POINTS);
		APP_CONFIGS_DEFAULTS.put(AppKey.TXN_COUNT_EMP, AppConfigDefaults.DEFAULT_TXN_COUNT_EMP);
		APP_CONFIGS_DEFAULTS.put(AppKey.TXN_COUNT_IND, AppConfigDefaults.DEFAULT_TXN_COUNT_IND);
		
		APP_CONFIGS_DEFAULTS.put(AppKey.MARKETING_CHANNEL, AppConfigDefaults.DEFAULT_MARKETING_CHANNEL_CRON);
		APP_CONFIGS_DEFAULTS.put(AppKey.VALID_PERIOD, AppConfigDefaults.DEFAULT_VALID_PERIOD);
		APP_CONFIGS_DEFAULTS.put(AppKey.EXPIRE_JOB, AppConfigDefaults.DEFAULT_EXPIRE_JOB);
		APP_CONFIGS_DEFAULTS.put(AppKey.SHOPPING_LIST_INCLUSIVE_TIME, AppConfigDefaults.DEFAULT_SHOPPING_LIST_INCLUSIVE_TIME);
		APP_CONFIGS_DEFAULTS.put(AppKey.SHOPPING_LIST_EXCLUSIVE_TIME, AppConfigDefaults.DEFAULT_SHOPPING_LIST_EXCLUSIVE_TIME);
		APP_CONFIGS_DEFAULTS.put(AppKey.SHOPPING_LIST_MIN_TXN, AppConfigDefaults.DEFAULT_SHOPPING_LIST_MIN_TXN);
		
		APP_CONFIGS_DEFAULTS.put(AppKey.REWARDS_JOB_DONE, AppConfigDefaults.DEFAULT_REWARDS_JOB_DONE_CRON);
		APP_CONFIGS_DEFAULTS.put(AppKey.LOYALTY_EXPIRY_NOTIFICATION, AppConfigDefaults.DEFAULT_LOYALTY_EXPIRY_JOB);
		APP_CONFIGS_DEFAULTS.put(AppKey.LOYALTY_EXPIRY_IN_MONTHS, AppConfigDefaults.DEFAULT_LOYALTY_EXPIRY_IN_MONTHS);
		APP_CONFIGS_DEFAULTS.put(AppKey.GIFT_CARD_EXPIRY_IN_MONTHS, AppConfigDefaults.DEFAULT_GIFT_CARD_EXPIRY_IN_MONTHS);

        APP_CONFIGS_DEFAULTS.put(AppKey.ALLOW_NO_LOYALTYCARD_TO_EARN, AppConfigDefaults.DEFAULT_ALLOW_NO_LOYALTYCARD_TO_EARN);
        APP_CONFIGS_DEFAULTS.put(AppKey.EXPIRE_POINTS_FOR_LOYALTY_IN_MONTHS, AppConfigDefaults.DEFAULT_LOYALTY_EXPIRE_POINTS_IN_MONTHS);

        APP_CONFIGS_DEFAULTS.put(AppKey.MD5_SECRET_KEY, AppConfigDefaults.DEFAULT_MD5_SECRET_KEY);

        APP_CONFIGS_DEFAULTS.put(AppKey.COMPUTE_POINTS_JOB, AppConfigDefaults.DEFAULT_COMPUTE_POINTS_CRON);
        APP_CONFIGS_DEFAULTS.put(AppKey.PRIMARY_WS_URL, AppConfigDefaults.DEFAULT_PRIMARY_WS_URL);
        APP_CONFIGS_DEFAULTS.put(AppKey.PRIMARY_WS_SOCKET_TIMEOUT, AppConfigDefaults.DEFAULT_PRIMARY_WS_SOCKET_TIMEOUT);
        APP_CONFIGS_DEFAULTS.put(AppKey.DISCOUNT_EOC_JOB, AppConfigDefaults.DEFAULT_DISCOUNT_EOC_JOB);

        APP_CONFIGS_DEFAULTS.put(AppKey.GCSO_RECEIPT_COUNT, AppConfigDefaults.DEFAULT_GCSO_RECEIPT_COUNT);

        APP_CONFIGS_DEFAULTS.put(AppKey.LOYALTY_CARD_RENEWAL_CODE, AppConfigDefaults.DEFAULT_LOYALTY_CARD_RENEWAL_CODE);

        APP_CONFIGS_DEFAULTS.put(AppKey.LOYALTY_CARD_REPLACEMENT_CODE, AppConfigDefaults.DEFAULT_LOYALTY_CARD_REPLACEMENT_CODE);

        APP_CONFIGS_DEFAULTS.put(AppKey.PRIMARY_WS_READ_TIMEOUT, AppConfigDefaults.DEFAULT_PRIMARY_WS_READ_TIMEOUT);
        APP_CONFIGS_DEFAULTS.put(AppKey.GC_TXN_SETTLEMENT_JOB, AppConfigDefaults.DEFAULT_GC_TXN_SETTLEMENT_CRON);
        APP_CONFIGS_DEFAULTS.put(AppKey.GC_CLAIMS_TO_AFFILIATES_JOB, AppConfigDefaults.DEFAULT_GC_CLAIMS_TO_AFFILIATES_CRON);

        APP_CONFIG_TRIGGERS.put(AppKey.RECONCILIATION_JOB, new Trigger(ReconciliationSchedulerTrigger.TRIGGER_NAME, ReconciliationSchedulerTrigger.TRIGGER_GROUP));
        APP_CONFIGS_DEFAULTS.put(AppKey.RECONCILIATION_JOB, AppConfigDefaults.DEFAULT_RECONCILIATION_CRON);
        
        APP_CONFIGS_DEFAULTS.put(AppKey.PRIMARY_WS_URL_FOR_STORES, AppConfigDefaults.DEFAULT_PRIMARY_WS_URL);
        
        APP_CONFIGS_DEFAULTS.put(AppKey.KWITANSI_TXN_CONTROL_NO, AppConfigDefaults.DEFAULT_KWITANSI_TXN_CONTROL_NO);
        APP_CONFIGS_DEFAULTS.put(AppKey.KWITANSI_TXN_PREFIX, AppConfigDefaults.DEFAULT_KWITANSI_TXN_PREFIX);
        APP_CONFIGS_DEFAULTS.put(AppKey.RECONCILE_MAIL, AppConfigDefaults.DEFAULT_RECONCILE_MAIL);
        APP_CONFIGS_DEFAULTS.put(AppKey.SMS_GATEWAY_URL, AppConfigDefaults.DEFAULT_SMS_GATEWAY_URL);
        APP_CONFIGS_DEFAULTS.put(AppKey.RECONCILE_CARD, AppConfigDefaults.DEFAULT_RECONCILE_CARD);
        APP_CONFIGS_DEFAULTS.put(AppKey.RECONCILE_MEDIA, AppConfigDefaults.DEFAULT_RECONCILE_MEDIA);

        APP_CONFIGS_DEFAULTS.put(AppKey.PORTA_URL_STAMP_CREATE, AppConfigDefaults.DEFAULT_PORTA_URL_STAMP_CREATE);
        APP_CONFIGS_DEFAULTS.put(AppKey.PORTA_URL_STAMP_REDEEM, AppConfigDefaults.DEFAULT_PORTA_URL_STAMP_REDEEM);
        APP_CONFIGS_DEFAULTS.put(AppKey.PRIMARY_WS_SOCKET_TIMEOUT, AppConfigDefaults.DEFAULT_WS_SOCKET_TIMEOUT);
        APP_CONFIGS_DEFAULTS.put(AppKey.PRIMARY_WS_READ_TIMEOUT, AppConfigDefaults.DEFAULT_WS_READ_TIMEOUT);

        APP_CONFIGS_DEFAULTS.put(AppKey.PORTA_URL_STAMP_ACCOUNT, AppConfigDefaults.DEFAULT_PORTA_URL_STAMP_ACCOUNT);
    }
	
	@Override
	public String getValue(AppKey key) {
		ApplicationConfig config = appConfigRepo.findByKey(key);
        return config != null ? config.getValue() : APP_CONFIGS_DEFAULTS.get(key);
	}
	
	@Override
	public ResultList<ApplicationConfigDto> list(PageSortDto sortDto) {
		QApplicationConfig qConfig = QApplicationConfig.applicationConfig;
		Pageable pageable = SpringDataPagingUtil.INSTANCE.createPageable(sortDto.getPagination());
		Page<ApplicationConfig> page = appConfigRepo.findAll(qConfig.key.ne(AppKey.PRODUCT_HIER_UPDATER_CRON), pageable);
		List<ApplicationConfigDto> list = toDto(page.getContent());
        return new ResultList<ApplicationConfigDto>(list, page.getTotalElements(), page.hasPreviousPage(),
            page.hasNextPage());
	}
	
	@Override
	@Transactional
	public void update(ApplicationConfigDto configDto) {
		ApplicationConfig config = appConfigRepo.findByKey(AppKey.valueOf(configDto.getKey()));
		appConfigRepo.save(configDto.toModel(config));
		
		Trigger triggerValues = APP_CONFIG_TRIGGERS.get(config.getKey()); 
		if(triggerValues != null) {
	        try {
	        	TriggerKey triggerKey = new TriggerKey(triggerValues.getName(), triggerValues.getGroup());
		        CronTriggerImpl trigger = (CronTriggerImpl) scheduler.getTrigger(triggerKey);
				trigger.setCronExpression(config.getValue());
				scheduler.rescheduleJob(triggerKey, trigger);
			} catch (ParseException e) {
				e.printStackTrace();
			} catch (SchedulerException e) {
				e.printStackTrace();
			}
		}
	}
	
	@Override
	@Transactional
	public void initAppConfigs() {
		for (AppKey key : AppKey.values()) {
			ApplicationConfig theConfig = appConfigRepo.findByKey(key);
	        if(theConfig == null) {
	        	theConfig = new ApplicationConfig();
	        	theConfig.setKey(key);
	        	theConfig.setValue(APP_CONFIGS_DEFAULTS.get(key));
	        	appConfigRepo.save(theConfig);
	        }
		}
	}
	
	private List<ApplicationConfigDto> toDto(Iterable<ApplicationConfig> configs) {
		List<ApplicationConfigDto> configDtos = new ArrayList<ApplicationConfigDto>();
		for(ApplicationConfig config: configs) {
			configDtos.add(new ApplicationConfigDto(config));
		}
		return configDtos;
	}
	
	public static class Trigger {
		private String name;
		private String group;
		public Trigger() {}
		public Trigger(String name, String group) {
			super();
			this.name = name;
			this.group = group;
		}
		public String getName() {
			return name;
		}
		public void setName(String name) {
			this.name = name;
		}
		public String getGroup() {
			return group;
		}
		public void setGroup(String group) {
			this.group = group;
		}
	}
}
