<%@ include file="../../common/taglibs.jsp" %>


  <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title"><spring:message code="label_member_grp_export"/></h4>
      </div>
    
      <div class="modal-body">
        <div id="contentError" class="hide alert alert-error">
          <button type="button" class="close" onclick="$(this).parent().hide();">&times;</button>
          <div><form:errors path="*"/></div>
        </div>
        <div class="span5 center">
          <div class="control-group">
            <label class="control-label"><spring:message code="label_com_transretail_crm_entity_customermodel_export_member_count" arguments="${memberGroupCount}"/></label>
            <label class="control-label"><spring:message code="label_com_transretail_crm_entity_customermodel_export_batch"/></label>
          </div>
          
          <div class="control-group">
            <select id="segment" path="segment" class="input-small pull-left">
            <option value=""></option>
             <c:forEach var="item" items="${segments}">
              <option value="${item}">${item}</option>
             </c:forEach>
            </select>
          </div>
        </div>
      </div>

      <div class="modal-footer">
        <button type="button" id="print" class="btn btn-primary"><spring:message code="label_print"/></button>
        <button type="button" id="cancel" class="btn btn-default" data-dismiss="modal"><spring:message code="label_cancel"/></button>
      </div>
  </div>
  

<script type="text/javascript">
$(document).ready(function() {
  initButtons();
  initForms();
  
  function initForms() {
    $("#print").attr("disabled", true);
    
  }
  
  function initButtons() {
    $("#print").click(function(e) {
      e.preventDefault();
/** Format of link
 ** groups/member/print/{segment}
 **/
	  var segment = "segment=" + $("#segment").val();
 	  var groupMember = "&groupMember=" + "${groupMember}";
	  var completeUrl = ctx + "/groups/member/print?"
        		+ segment
        		+ groupMember;
  	
      $("#print").attr("disabled", true);
      window.location.href = completeUrl;
      $("#cancel").click();
    });
    
    $("#segment").change(function(e) {
      if ($("#segment").val() != "") {
        $("#print").removeAttr("disabled");
      } else {
        $("#print").attr("disabled", true);
      }
    });
  }

});
</script>