<%@ include file="../common/taglibs.jsp" %>

<div>
	<div class="page-header page-header2">
		<h1><spring:message code="label_monitor_transactions_points" arguments="${currentDate}"/></h1>
	</div>
	<div class="">
		<div>
			<spring:message code="label_member" arguments="${member.accountId}, ${member.formattedMemberName}" />
		</div>
	</div>
</div>

<div id="content_pointsList">
	<div id="pointsList"
	  	data-url="<c:url value="/member/monitor/points/${member.accountId}/${currentDate}" />"
		data-hdr-storecode="<spring:message code="points_prop_transaction_location"/>"
		data-hdr-transactiondatetime="<spring:message code="points_prop_txndate"/>" 
		data-hdr-transactionno="<spring:message code="points_prop_txnno"/>" 
		data-hdr-transactionamount="<spring:message code="points_prop_amount"/>"
		data-hdr-transactionmedia="<spring:message code="points_prop_media"/>"></div>
</div>

<link href="<spring:url value="/css/dataTables.css" />" rel="stylesheet">
<script src="<spring:url value="/js/jquery/jquery.dataTables.js" />" ></script>
<script src="<spring:url value="/js/jquery/jquery.dataTables.linkLengthMenu.js" />"></script>
<script src="<spring:url value="/js/jquery/jquery.dataTables.customPaging.js" />"></script>
<script src="<spring:url value="/js/jquery/ajax-datatable.js" />" ></script>
<script src="<spring:url value="/js/common/form2js.js" />" ></script>
<script src="<spring:url value="/js/common/json2.js" />" ></script>
<script src="<spring:url value="/js/jquery/jquery.toObject.js" />" ></script>

<script type="text/javascript">
$(document).ready(function() {
	transactionList = $("#pointsList");

	var headers = {
		storeCode: transactionList.data("hdr-storecode") + "",
		transactionDateTime: transactionList.data("hdr-transactiondatetime") + "",
		transactionNo:	transactionList.data("hdr-transactionno") + "",
		transactionAmount: transactionList.data("hdr-transactionamount") + "",
		transactionMedia: transactionList.data("hdr-transactionmedia") + ""
	};

	var columnHeaders = [
		headers['storeCode'],
		headers['transactionDateTime'],
		headers['transactionNo'],
		headers['transactionAmount'],
		headers['transactionMedia']];

	var modelFields = [
		{name: 'storeCode', fieldCellRenderer: function(data, type, row) {
			if(row.storeCode) {
				return row.storeCode + " " + row.storeName;
			}
			return "";
		}},
		{name: 'transactionDateTime', fieldCellRenderer : function (data, type, row) {
	        return ( (row.longTxnDate)? new Date( row.longTxnDate ).customize(0) : "" );
        }},
        {name: 'transactionNo'},
	 	{name: 'transactionAmount', fieldCellRenderer : function (data, type, row) {
			return row.txnAmount;
        }},
        {name: 'transactionMedia', fieldCellRenderer : function (data, type, row) {
			if(row.transactionMedia != null && row.transactionMedia != "")
				return row.transactionMedia + " - " + row.transactionMediaDesc;
			else
				return "";
        }}];

	transactionList.ajaxDataTable({
		'autoload' : true,
		'ajaxSource' : transactionList.data("url"),
		'columnHeaders' : columnHeaders,
		'modelFields' : modelFields
	});

	console.log("${currentDate}");
	console.log(transactionList.data("url"));
});
</script>