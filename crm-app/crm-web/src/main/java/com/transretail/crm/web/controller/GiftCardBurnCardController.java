package com.transretail.crm.web.controller;

import java.util.Arrays;
import java.util.List;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;

import com.transretail.crm.core.entity.lookup.LookupDetail;
import com.transretail.crm.giftcard.entity.SalesOrderAlloc;
import lombok.extern.slf4j.Slf4j;
import org.joda.time.DateTime;
import org.joda.time.LocalDate;
import org.joda.time.LocalTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.google.common.collect.Lists;
import com.transretail.crm.common.reporting.jasper.JRProcessor;
import com.transretail.crm.common.service.dto.response.ResultList;
import com.transretail.crm.common.service.exception.MessageSourceResolvableException;
import com.transretail.crm.core.entity.enums.ReportType;
import com.transretail.crm.core.security.util.UserUtil;
import com.transretail.crm.core.service.CodePropertiesService;
import com.transretail.crm.core.service.LookupService;
import com.transretail.crm.core.service.StoreService;
import com.transretail.crm.giftcard.dto.GiftCardBurnCardDto;
import com.transretail.crm.giftcard.dto.GiftCardBurnCardSearchDto;
import com.transretail.crm.giftcard.dto.GiftCardInventorySeriesSearchDto;
import com.transretail.crm.giftcard.dto.GiftCardInventoryStockDto;
import com.transretail.crm.giftcard.entity.support.GiftCardBurnCardStatus;
import com.transretail.crm.giftcard.entity.support.GiftCardInventoryStatus;
import com.transretail.crm.giftcard.service.GiftCardBurnCardService;
import com.transretail.crm.giftcard.service.GiftCardInventoryService;
import com.transretail.crm.giftcard.service.GiftCardInventoryStockService;
import com.transretail.crm.report.template.impl.GiftCardBurnedDocument;
import com.transretail.crm.web.controller.util.ControllerResponse;

/**
 * @author ftopico
 */
@RequestMapping("/giftcard/burncard")
@Controller
@Slf4j
public class GiftCardBurnCardController extends AbstractReportsController {

    private static final String PATH_GIFTCARD_BURNCARD = "/giftcard/burncard";
    private static final String PATH_GIFTCARD_BURNCARD_CREATE = "/giftcard/burncard/create";

    private static final String UI_ATTRIB_DATES = "burnedDates";
    private static final String UI_ATTR_DIAlogger_LABEL   = "dialoggerLabel";
    private static final String UI_ATTR_STORES = "stores";
    private static final String UI_ATTR_STATUS = "status";
    private static final String ERROR_SERIES_NO_NOT_EXIST = "gc.burncard.error.series.no.not.exist";
    private static final String ERROR_NO_CARDS_FOUND = "gc.burncard.error.no.gift.card.found";
    private static final String ERROR_SERIES_NO_ALREADY_LISTED = "gc.burncard.error.already.listed";
    private static final String ERROR_SERIES_RANGE = "gc.burncard.error.series.range";
    private static final String ERROR_SERIES_RANGE_REMOVE = "gc.burncard.error.series.range.remove";
    private static final String ERROR_BURN_REASON_EMPTY = "gc.burncard.error.burn.reason.empty";
    private static final String ERROR_LIST_EMPTY = "gc.burncard.error.list.empty";
    private static final String ERROR_NO_SO = "gc.burncard.error.no.so";
    private static final String NO_SO_DOESN_EXIST_IN_ALLOCATE = "gc.burncard.error.no.so.doesnt.exist.in.allocate";

    @Autowired
    private GiftCardInventoryService giftCardInventoryService;
    @Autowired
    private GiftCardInventoryStockService giftCardInventoryStockService;
    @Autowired
    private GiftCardBurnedDocument giftCardBurnedDocument;
    @Autowired
    private GiftCardBurnCardService giftCardBurnCardService;
    @Autowired
    private CodePropertiesService codePropertiesService;
    @Autowired
    private StoreService storeService;
    @Autowired
    private LookupService lookupService;
    @Autowired
    private MessageSource messageSource;

    @RequestMapping(produces="text/html")
    public String showBurnCardForm(Model model) {
        model.addAttribute(UI_ATTR_STORES, storeService.getAllStores());
        model.addAttribute(UI_ATTR_STATUS, GiftCardBurnCardStatus.values());
        return PATH_GIFTCARD_BURNCARD;
    }

    @RequestMapping(value="/list", method = RequestMethod.POST)
    public @ResponseBody ResultList<GiftCardBurnCardDto> listBurnCardRequests(@RequestBody GiftCardBurnCardSearchDto searchForm) {
    	logger.debug("[Method Call from StockRequestController] storeMemberService.searchStoreMembers()");
        return giftCardBurnCardService.searchGiftCardBurnCard(searchForm);
    }

    @RequestMapping(value = "/create", produces="text/html")
    public String createBurnCardRequest(
            @ModelAttribute GiftCardBurnCardDto giftCardBurnCardDto,
            Model model) {

        List<LookupDetail> burnReason = lookupService.getActiveDetailsByHeaderCode(codePropertiesService.getBurnReasons());
        List<LookupDetail> typeBurn = lookupService.getActiveDetailsByHeaderCode(codePropertiesService.getBurnType());
        String dialoggerLabel = messageSource.getMessage("gc.burncard.label.submit.gc.burn.request", null, LocaleContextHolder.getLocale());

        logger.info("--> burnReason :: " + burnReason.toString());
        logger.info("--> typeBurn :: " + typeBurn.toString());
        logger.info("--> dialoggerLabel :: " + dialoggerLabel);

        GiftCardBurnCardDto dto = new GiftCardBurnCardDto();
        dto.setBurnNo(giftCardBurnCardService.createBurnCardNumber());
        dto.setDateFiled(LocalDate.now());
        dto.setFiledBy(UserUtil.getCurrentUser().getUsername());
        dto.setTimeFiled(LocalTime.now());

        model.addAttribute("burnCardForm", dto);
        model.addAttribute("creation", true);
        model.addAttribute("burnReasons", burnReason);
        model.addAttribute("typeBurn", typeBurn);
        model.addAttribute(UI_ATTR_DIAlogger_LABEL, dialoggerLabel);

        return PATH_GIFTCARD_BURNCARD_CREATE;
    }

    @RequestMapping(value = "/view/{burnNo}", produces="text/html")
    public String viewBurnCardRequest(@PathVariable String burnNo, Model model) {

        GiftCardBurnCardDto dto = giftCardBurnCardService.getBurnCardRequest(burnNo);
        model.addAttribute("burnCardForm", dto);
        model.addAttribute("cards", dto.getCards());
        model.addAttribute("approval", true);
        model.addAttribute("burnReasons", lookupService.getActiveDetailsByHeaderCode(codePropertiesService.getBurnReasons()));
        String inventoryLocation = UserUtil.getCurrentUser().getInventoryLocation();
        if(inventoryLocation.equals(codePropertiesService.getDetailInvLocationHeadOffice())) {
            model.addAttribute("approver", true);
        }
        model.addAttribute(UI_ATTR_DIAlogger_LABEL, messageSource.getMessage("gc.burncard.header",
                null, LocaleContextHolder.getLocale()));
        return PATH_GIFTCARD_BURNCARD_CREATE;
    }

    @RequestMapping(value = "/edit/{burnNo}", produces="text/html")
    public String editBurnCardRequest(@PathVariable String burnNo, Model model) {

        GiftCardBurnCardDto dto = giftCardBurnCardService.getBurnCardRequest(burnNo);
        model.addAttribute("burnCardForm", dto);
        model.addAttribute("cards", dto.getCards());
        model.addAttribute("creation", true);
        model.addAttribute("burnReasons", lookupService.getActiveDetailsByHeaderCode(codePropertiesService.getBurnReasons()));
        model.addAttribute(UI_ATTR_DIAlogger_LABEL, messageSource.getMessage("gc.burncard.label.edit.gc.burn.request",
                null, LocaleContextHolder.getLocale()));
        return PATH_GIFTCARD_BURNCARD_CREATE;
    }

    @RequestMapping(value="/saveForApproval", produces = "application/json; charset=utf-8", method = RequestMethod.POST)
    @ResponseBody
    public ControllerResponse saveRequest(
            @ModelAttribute("burnCardForm") GiftCardBurnCardDto dto,
            BindingResult bindingResult) {

        ControllerResponse response = new ControllerResponse();
        response.setSuccess(true);

        if (dto.getBurnReason() == null || dto.getBurnReason().isEmpty()) {
            response.setSuccess(false);
            bindingResult.reject(messageSource.getMessage(ERROR_BURN_REASON_EMPTY, null, LocaleContextHolder.getLocale()));
            response.setResult(bindingResult.getAllErrors());
        }

        if(bindingResult.hasErrors()) {
            response.setSuccess(false);
        } else {
            logger.debug("[Method Call from GiftCardBurnCardController] giftCardBurnCardService.saveBurncardRequest()");
            dto.setStatus(GiftCardBurnCardStatus.FORAPPROVAL);
            validateBurncard(dto, response);
        }

        return response;
    }

    @RequestMapping(value="/saveDraft", produces = "application/json; charset=utf-8", method = RequestMethod.POST)
    @ResponseBody
    public ControllerResponse saveDraftRequest(
            @ModelAttribute("burnCardForm") GiftCardBurnCardDto dto,
            BindingResult bindingResult) {

        ControllerResponse response = new ControllerResponse();
        response.setSuccess(true);

        if (dto.getBurnReason() == null || dto.getBurnReason().isEmpty()) {
            response.setSuccess(false);
            bindingResult.reject(messageSource.getMessage(ERROR_BURN_REASON_EMPTY, null, LocaleContextHolder.getLocale()));
            response.setResult(bindingResult.getAllErrors());
        }

        if(bindingResult.hasErrors()) {
            response.setSuccess(false);
        } else {
            logger.debug("[Method Call from GiftCardBurnCardController] giftCardBurnCardService.saveBurncardRequest()");
            dto.setStatus(GiftCardBurnCardStatus.DRAFT);
            validateBurncard(dto, response);
        }

        return response;
    }

    private void validateBurncard(@ModelAttribute("burnCardForm") GiftCardBurnCardDto dto, ControllerResponse response) {
        try {
            giftCardBurnCardService.saveBurncardRequest(dto);
        } catch (MessageSourceResolvableException e) {
            logger.debug(e.getMessage());
            response.setSuccess(false);
            response.setResult(Lists.newArrayList(messageSource.getMessage(e, LocaleContextHolder.getLocale())));
        }
    }

    @RequestMapping(value="/encode", produces = "application/json; charset=utf-8", method = RequestMethod.POST)
    @ResponseBody
    public ControllerResponse encodeSeries(
            @ModelAttribute("burnCardForm") GiftCardBurnCardDto dto,
            BindingResult bindingResult) {


        ControllerResponse response = new ControllerResponse();
        response.setSuccess(true);

        logger.debug("[Method Call from GiftCardBurnCardController] giftCardInventoryService.getGiftCardSeries()");
        if (dto.getSeriesFrom() == null || dto.getSeriesTo() == null) {
            response.setSuccess(false);
            bindingResult.reject(messageSource.getMessage(ERROR_SERIES_RANGE, null, LocaleContextHolder.getLocale()));
            response.setResult(bindingResult.getAllErrors());
        }

        GiftCardInventorySeriesSearchDto searchDto = new GiftCardInventorySeriesSearchDto();
        searchDto.setSeriesFrom(dto.getSeriesFrom());
        searchDto.setSeriesTo(dto.getSeriesTo());
        searchDto.setCards(dto.getCards());
        List<String> cards = giftCardInventoryService.getGiftCardSeries(searchDto);
        validateCards(bindingResult, response, cards);

        return response;
    }

    @RequestMapping(value = "/nosalesorder", produces = "application/json; charset=utf-8", method = RequestMethod.POST)
    @ResponseBody
    public ControllerResponse nosalesorder(@ModelAttribute("burnCardForm") GiftCardBurnCardDto dto, BindingResult bindingResult) {

        ControllerResponse response = new ControllerResponse();
        response.setSuccess(true);

        logger.debug("[method Call from GiftCardBurnCardController] ");
        if (dto.getNoSo() == null) {

            response.setSuccess(false);
            bindingResult.reject(messageSource.getMessage(ERROR_NO_SO, null, LocaleContextHolder.getLocale()));
            response.setResult(bindingResult.getAllErrors());
        }

        SalesOrderAlloc orderAlloc = giftCardInventoryService.getGiftCardSo(dto.getNoSo());
        if (orderAlloc == null) {

            response.setSuccess(false);
            bindingResult.reject(messageSource.getMessage(NO_SO_DOESN_EXIST_IN_ALLOCATE, null, LocaleContextHolder.getLocale()));
            response.setResult(bindingResult.getAllErrors());
        } else {

            GiftCardInventorySeriesSearchDto searchDto = new GiftCardInventorySeriesSearchDto();
            searchDto.setSeriesFrom(orderAlloc.getSeriesStart());
            searchDto.setSeriesTo(orderAlloc.getSeriesEnd());
            searchDto.setCards(dto.getCards());

            List<String> cards = giftCardInventoryService.getGiftCardSeries(searchDto);
            validateCards(bindingResult, response, cards);
        }


        return response;
    }

    private void validateCards(BindingResult bindingResult, ControllerResponse response, List<String> cards) {
        if (bindingResult.hasErrors() || cards.isEmpty()) {
            response.setSuccess(false);
            bindingResult.reject(messageSource.getMessage(ERROR_NO_CARDS_FOUND, null, LocaleContextHolder.getLocale()));
            response.setResult(bindingResult.getAllErrors());
        } else {
            response.setResult(cards);
        }
    }

    @RequestMapping(value="/remove", produces = "application/json; charset=utf-8", method = RequestMethod.POST)
    @ResponseBody
    public ControllerResponse removeSeries(
            @ModelAttribute("burnCardForm") GiftCardBurnCardDto dto,
            BindingResult bindingResult) {


        ControllerResponse response = new ControllerResponse();
        response.setSuccess(true);

        logger.debug("[Method Call from GiftCardBurnCardController] giftCardInventoryService.getGiftCardSeries()");
        if (dto.getSeriesFrom() == null || dto.getSeriesTo() == null) {
            response.setSuccess(false);
            bindingResult.reject(messageSource.getMessage(ERROR_SERIES_RANGE_REMOVE, null, LocaleContextHolder.getLocale()));
            response.setResult(bindingResult.getAllErrors());
        }

        if (dto.getCards() == null || dto.getCards().isEmpty()) {
            response.setSuccess(false);
            bindingResult.reject(messageSource.getMessage(ERROR_LIST_EMPTY, null, LocaleContextHolder.getLocale()));
            response.setResult(bindingResult.getAllErrors());
        } else {
            GiftCardInventorySeriesSearchDto searchDto = new GiftCardInventorySeriesSearchDto();
            searchDto.setSeriesFrom(dto.getSeriesFrom());
            searchDto.setSeriesTo(dto.getSeriesTo());
            searchDto.setCards(dto.getCards());
            List<String> cards = giftCardInventoryService.removeGiftCardSeriesFromList(searchDto);

            response.setResult(cards);
        }

        return response;
    }

    @RequestMapping(value="/validate/{seriesNo}", produces = "application/json; charset=utf-8", method = RequestMethod.POST)
    @ResponseBody
    public ControllerResponse validateSeries(
            @PathVariable(value = "seriesNo") String seriesNo,
            @ModelAttribute("seriesForm") GiftCardInventorySeriesSearchDto searchDto,
            BindingResult bindingResult) {

        ControllerResponse response = new ControllerResponse();
        response.setSuccess(true);

        if (bindingResult.hasErrors()) {
            response.setSuccess(false);
        } else {
            logger.debug("[Method Call from GiftCardBurnCardController] giftCardInventoryService.isSeriesNoExists()");
            if (!giftCardInventoryService.isSeriesNotExistsInStockStatus(seriesNo)) {
                response.setSuccess(false);
                Object[] args = {seriesNo};
                bindingResult.reject(messageSource.getMessage(ERROR_SERIES_NO_NOT_EXIST, args, LocaleContextHolder.getLocale()));
                response.setResult(bindingResult.getAllErrors());
            }

            logger.debug("[Method Call from GiftCardBurnCardController] giftCardInventoryService.isCardInTheList()");
            if (giftCardInventoryService.isCardInTheList(seriesNo, searchDto)) {
                response.setSuccess(false);
                Object[] args = {seriesNo};
                bindingResult.reject(messageSource.getMessage(ERROR_SERIES_NO_ALREADY_LISTED, args, LocaleContextHolder.getLocale()));
                response.setResult(bindingResult.getAllErrors());
            }
        }

        return response;
    }

    @RequestMapping(value="/burn", produces = "application/json; charset=utf-8", method = RequestMethod.POST)
    @ResponseBody
    public ControllerResponse burnCards(
            @ModelAttribute("burnCardForm") GiftCardBurnCardDto dto,
            BindingResult bindingResult) {

        ControllerResponse response = new ControllerResponse();
        response.setSuccess(true);

        if(bindingResult.hasErrors()) {
            response.setSuccess(false);
        } else {
            giftCardBurnCardService.burnCards(dto);
        }

        return response;
    }

    @RequestMapping(value="/reject", produces = "application/json; charset=utf-8", method = RequestMethod.POST)
    @ResponseBody
    public ControllerResponse rejectRequest(
            @ModelAttribute("burnCardForm") GiftCardBurnCardDto dto,
            BindingResult bindingResult) {

        ControllerResponse response = new ControllerResponse();
        response.setSuccess(true);

        if(bindingResult.hasErrors()) {
            response.setSuccess(false);
        } else {
            giftCardBurnCardService.rejectRequest(dto);
        }

        return response;
    }

    @RequestMapping(value = "/print/{burnNo}/{type}")
    public void print(
            @PathVariable String burnNo,
            @PathVariable String type,
            HttpServletResponse response) throws Exception {

        JRProcessor jrProcessor = giftCardBurnedDocument.createJrProcessor(burnNo);
        if (ReportType.EXCEL.name().equalsIgnoreCase(type)) {
            renderExcelReport(response, jrProcessor, "Gift Card Burn Document (" + burnNo +").xls");
        } else {
            renderReport(response, jrProcessor);
        }
    }

}
