package com.transretail.crm.report.template;

import java.util.Set;

/**
 *
 * @author Monte Cillo Co (mco@exist.com)
 */
public interface ReportsCustomizer {

    enum Option {
	DATE_RANGE_FULL_VALIDATION,
	DATE_FROM_OPTIONAL,
	EXCEL_REPORT_TYPE_ONLY,
	PDF_REPORT_TYPE_ONLY,
        YEARLY
    }

    Set<Option> customizerOption();
}
