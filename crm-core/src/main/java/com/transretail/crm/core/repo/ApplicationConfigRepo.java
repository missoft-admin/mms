package com.transretail.crm.core.repo;

import org.springframework.stereotype.Repository;

import com.transretail.crm.common.repo.CrmQueryDslPredicateExecutor;
import com.transretail.crm.core.entity.ApplicationConfig;
import com.transretail.crm.core.entity.enums.AppKey;

@Repository
public interface ApplicationConfigRepo extends CrmQueryDslPredicateExecutor<ApplicationConfig, Long> {
	
	ApplicationConfig findByKey(AppKey inAppKey);

}