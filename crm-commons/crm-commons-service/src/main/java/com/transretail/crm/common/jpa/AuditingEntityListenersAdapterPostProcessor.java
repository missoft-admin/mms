package com.transretail.crm.common.jpa;

import java.util.Map;

import org.hibernate.HibernateException;
import org.hibernate.SessionFactory;
import org.hibernate.ejb.HibernateEntityManagerFactory;
import org.hibernate.event.service.spi.EventListenerGroup;
import org.hibernate.event.service.spi.EventListenerRegistry;
import org.hibernate.event.spi.EventType;
import org.hibernate.event.spi.PersistEvent;
import org.hibernate.event.spi.PersistEventListener;
import org.hibernate.event.spi.PreUpdateEvent;
import org.hibernate.event.spi.PreUpdateEventListener;
import org.hibernate.internal.SessionFactoryImpl;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.config.InstantiationAwareBeanPostProcessorAdapter;
import org.springframework.core.PriorityOrdered;
import org.springframework.data.auditing.AuditingHandler;
import org.springframework.data.domain.AuditorAware;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;
import org.springframework.util.Assert;

/**
 * Thanks to Bear Giles for giving an idea on how to implement this
 *
 * @see { @link http://www.javacodegeeks.com/2013/10/spring-injected-beans-in-jpa-entitylisteners.html }
 */
public class AuditingEntityListenersAdapterPostProcessor extends InstantiationAwareBeanPostProcessorAdapter
    implements PriorityOrdered, InitializingBean {

    private CustomHibernateListener customHibernateListener;

    private AuditorAware auditorAware = new AuditorAware() {
        @Override
        public Object getCurrentAuditor() {
            return "SYSTEM";
        }
    };

    public void setAuditorAware(AuditorAware auditorAware) {
        this.auditorAware = auditorAware;
    }

    @Override
    public int getOrder() {
        return PriorityOrdered.LOWEST_PRECEDENCE;
    }

    @Override
    public void afterPropertiesSet() throws Exception {
        Assert.notNull(auditorAware);

        AuditingHandler auditingHandler = new AuditingHandler();
        auditingHandler.setAuditorAware(auditorAware);

        AuditingEntityListener auditingEntityListener = new AuditingEntityListener();
        auditingEntityListener.setAuditingHandler(auditingHandler);
        customHibernateListener = new CustomHibernateListener(auditingEntityListener);
    }

    @Override
    public Object postProcessAfterInitialization(Object bean, String beanName) throws BeansException {
        if (HibernateEntityManagerFactory.class.isAssignableFrom(bean.getClass())) {
            // get registry so we can add listeners.
            SessionFactory sf = ((HibernateEntityManagerFactory) bean).getSessionFactory();
            EventListenerRegistry registry = ((SessionFactoryImpl) sf).getServiceRegistry().getService(
                EventListenerRegistry.class);

            EventListenerGroup persistListenerGroup = registry.getEventListenerGroup(EventType.PERSIST);
            EventListenerGroup preUpdateListenerGroup = registry.getEventListenerGroup(EventType.PRE_UPDATE);

            boolean listenerExists = false;
            if (persistListenerGroup != null) {
                for (Object listener : persistListenerGroup.listeners()) {
                    if (listener instanceof CustomHibernateListener) {
                        listenerExists = true;
                    }
                }
            }
            if (!listenerExists) {
                persistListenerGroup.prependListener(customHibernateListener);
                preUpdateListenerGroup.prependListener(customHibernateListener);
            }
        }
        return bean;
    }

    private class CustomHibernateListener implements PreUpdateEventListener, PersistEventListener {
        private AuditingEntityListener entityListener;

        public CustomHibernateListener(AuditingEntityListener entityListener) {
            this.entityListener = entityListener;
        }

        @Override
        public boolean onPreUpdate(PreUpdateEvent event) {
            entityListener.touchForUpdate(event.getEntity());
            return false;
        }

        @Override
        public void onPersist(PersistEvent event) throws HibernateException {
            entityListener.touchForCreate(event.getObject());
        }

        @Override
        public void onPersist(PersistEvent event, Map createdAlready) throws HibernateException {

        }

    }
}

