<%@ include file="../taglibs.jsp" %>
<spring:url value="/member/${username}/password/reset" var="actionUrl"/>
<form:form modelAttribute="form" method="POST" action="${actionUrl}" cssClass="body-container form-horizontal">
  <div class="control-group">
    <spring:message code="password" var="password"/>
    <form:label path="password" title="${password}" cssClass="control-label" cssErrorClass="control-label error"><span class="required">*</span>${password}</form:label>
    <div class="controls">
      <form:password path="password" title="${password}" id="password"/>
    </div>
  </div>
  <div class="control-group">
    <spring:message code="confirm_password" var="confirmPassword"/>
    <form:label path="confirmPassword" title="confirmPassword" cssClass="control-label" cssErrorClass="control-label error"><span
            class="required">*</span>${confirmPassword}</form:label>
    <div class="controls">
      <form:password path="confirmPassword" title="${confirmPassword}"/>
    </div>
  </div>
  <div class="form-actions">
    <button type="submit" class="btn btn-primary pull-left"><spring:message code="button_submit" /></button>
    <div style="padding: 17px 0 0 10px" class="pull-left">Already have an account? <a class="" href="<spring:url value="/login"/>"><spring:message code="button_gotologin"/></a>.</div>
  </div>
</form:form>
<style>
    /* .form-actions { padding-left: 20px !important; } */
  .btn-block { width: 180px !important; }
  .pull-center { text-align: center !important; }
  .btn-default { padding-top: 12px; }


  .form-horizontal .control-label {
    width: 200px;
  }

  .form-horizontal .controls {
    margin-left: 130px;
  }

  .form-horizontal .form-actions {
    padding-left: 130px;
  }
</style>