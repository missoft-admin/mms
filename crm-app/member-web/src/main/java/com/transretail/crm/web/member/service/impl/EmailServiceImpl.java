package com.transretail.crm.web.member.service.impl;

import java.io.StringWriter;
import java.io.UnsupportedEncodingException;
import java.util.Locale;
import java.util.Map;

import javax.mail.MessagingException;

import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.core.env.Environment;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;

import com.transretail.crm.common.service.exception.MessageSourceResolvableException;
import com.transretail.crm.core.dto.MessageDto;
import com.transretail.crm.web.member.service.EmailService;

import freemarker.template.Configuration;
import freemarker.template.Template;

/**
 * @author Allan G. Ramirez (agramirez@exist.com)
 */
@Service("emailService")
public class EmailServiceImpl implements EmailService, ApplicationContextAware {
    private static final Logger _LOG = LoggerFactory.getLogger(EmailServiceImpl.class);
    @Autowired
    private Configuration freeMarkerCfg;
    @Autowired
    private JavaMailSender mailSender;
    private String fromName;
    private String fromAddress;
    private boolean isProduction;

	private static final Logger LOGGER = LoggerFactory.getLogger( EmailServiceImpl.class );

    @Value("#{'${mail.from.name}'}")
    public void setFromName(String fromName) {
        this.fromName = fromName;
    }

    @Value("#{'${mail.from.address}'}")
    public void setFromAddress(String fromAddress) {
        this.fromAddress = fromAddress;
    }

    @Override
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        Environment env = applicationContext.getEnvironment();
        String[] activeProfiles = env.getActiveProfiles();
        if (activeProfiles != null) {
            for (String profile : activeProfiles) {
                if (profile.equalsIgnoreCase("dev") || profile.equalsIgnoreCase("development")) {
                    isProduction = false;
                    break;
                }
            }
        }
    }

    public void sendValidationLink(String subject, String email, String templateName, Map<String, Object> data, Locale locale) throws
        MessageSourceResolvableException {
        try {
            Template template = freeMarkerCfg.getTemplate(templateName, locale);
            /* Merge data-model with template */
            StringWriter out = new StringWriter();
            template.process(data, out);

            MimeMessageHelper mimeMessageHelper = new MimeMessageHelper(mailSender.createMimeMessage());
            mimeMessageHelper.setSubject(subject);
            mimeMessageHelper.addTo(email);
            mimeMessageHelper.setFrom(fromAddress, fromName);
            mimeMessageHelper.setText(out.toString(), true);
            if (!isProduction) {
//                mimeMessageHelper.setBcc("Exist_PT_Trans_Retails_CRM@exist.com");
            }
            mailSender.send(mimeMessageHelper.getMimeMessage());
        } catch (Exception e) {
            String message = "Failed to send to [" + email + "]";
            _LOG.error(message, e);
            throw new MessageSourceResolvableException("member.password.change.mailsending.failed", new String[]{email}, message, e);
        }
    }

    @Override
    public boolean send( MessageDto dto ) {
		if ( ArrayUtils.isEmpty( dto.getRecipients() ) && StringUtils.isBlank( dto.getEmailAd() ) ) {
			return false;
		}

    	try {
    		LOGGER.info( ":send:: Email " );

			MimeMessageHelper helper = new MimeMessageHelper( mailSender.createMimeMessage(), true );
			helper.setFrom( fromAddress, fromName );
			if ( ArrayUtils.isNotEmpty( dto.getRecipients() ) ) {
				helper.setBcc( dto.getRecipients() );
			}
			else if ( StringUtils.isNotBlank( dto.getEmailAd() ) ) {
				helper.setTo( dto.getEmailAd() );
			}
			helper.setSubject( StringUtils.isNotBlank( dto.getSubject() )? dto.getSubject() : "" );
			helper.setText( dto.getMessage() );
            mailSender.send( helper.getMimeMessage() );

    		LOGGER.info( ":send:: Email successfully sent email to recipients:" + 
    				( ArrayUtils.isNotEmpty( dto.getRecipients() )? StringUtils.join( dto.getRecipients(), "," ) : 
    					StringUtils.isNotBlank( dto.getEmailAd() )? dto.getEmailAd() : "" ) );
            return true;
		} 
    	catch ( MessagingException e ) {
    		LOGGER.error( ":send:: Email MessagingException message: " + e.getMessage() );
		} 
    	catch ( UnsupportedEncodingException e ) {
    		LOGGER.error( ":send:: Email UnsupportedEncodingException message: " + e.getMessage() );
		}
    	catch ( Exception e ) {
    		LOGGER.error( ":send:: Email Exception message: " + e.getMessage() );
		}
    	return false;
    }
}
