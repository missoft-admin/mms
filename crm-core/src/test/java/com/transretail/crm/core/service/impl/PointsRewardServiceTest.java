package com.transretail.crm.core.service.impl;


import com.transretail.crm.core.entity.EmployeePurchaseTxnModel;
import com.transretail.crm.core.entity.MemberModel;
import com.transretail.crm.core.entity.RewardSchemeModel;
import com.transretail.crm.core.entity.enums.MemberStatus;
import com.transretail.crm.core.entity.enums.Status;
import com.transretail.crm.core.entity.lookup.LookupDetail;
import com.transretail.crm.core.entity.lookup.LookupHeader;
import com.transretail.crm.core.entity.lookup.Store;
import com.transretail.crm.core.repo.*;
import com.transretail.crm.core.service.PointsRewardService;
import com.transretail.crm.core.util.generator.IdGeneratorService;
import org.joda.time.DateTime;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Configurable;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;


@Configurable
@Transactional
@ContextConfiguration(locations = "classpath*:/META-INF/spring/applicationContext*.xml")
@RunWith(SpringJUnit4ClassRunner.class)
public class PointsRewardServiceTest {

	@Autowired
	PointsRewardService pointsRewardService;

	@Autowired
	StoreRepo storeRepo;
	@Autowired
	RewardSchemeRepo rewardSchemeRepo;
	@Autowired
	MemberRepo memberRepo;
	@Autowired
	MemberTypeRepo memberTypeRepo;
	@Autowired
	EmployeePurchaseTxnRepo employeePurchaseTxnRepo;
	
	@Autowired
	LookupHeaderRepo lookupHeaderRepo;
	
	@Autowired
	LookupDetailRepo lookupDetailRepo;
	
	@Autowired
    IdGeneratorService customIdGeneratorService;

	private static final String EMP1_ACCTID = "EMP001";
	private static final String EMP2_ACCTID = "EMP002";
	private static final String EMP3_INACID = "EMP003";
	private static final String MEM1_ACCTID = "IND001";

    private static String employeeType  = "MTYP002";
    private static String employeeTypeDesc = "Employee";
    private static String individualType  = "MTYP001";
    private static String individualTypeDesc = "Individual";

	@Test
	public void testGeneratePointsRewardForApproval() {
//
//		setupPointsRewardServiceTest();
//
//		/*ea20131017: previous month = 0 rewards*/
//		ServiceDto theSvcResp = pointsRewardService.generatePointsRewardForApproval();
//		Assert.assertTrue( CollectionUtils.isEmpty( ( (RewardBatchDto) theSvcResp.getReturnObj() ).getRewards() ) );
//
//		theSvcResp = pointsRewardService.generatePointsRewardForApproval( new Date(), new Date() );
//		Assert.assertEquals( 1, ( (RewardBatchDto) theSvcResp.getReturnObj() ).getRewards().size() );
	}
//
//	@Test
//	@Ignore //Ignore since getPointsRewardForProcessing no longer return list of rewards
//	public void testGetPointsRewardForProcessing() {
//
//		setupPointsRewardServiceTest();
//
//		pointsRewardService.generatePointsRewardForApproval( new Date(), new Date() );
//		ServiceDto theSvcResp = pointsRewardService.getPointsRewardForProcessing();
//		List<PointsTxnModel> theRewards = ( (RewardBatchDto) theSvcResp.getReturnObj() ).getRewards();
//		Assert.assertEquals( 1, theRewards.size()  );
//
//		List<String> theRewardIds = new ArrayList<String>();
//		for ( PointsTxnModel theReward : theRewards ) {
//			theRewardIds.add( theReward.getId() );
//		}
//		pointsRewardService.rejectPointsRewards( theRewardIds, new RewardBatchDto() );
//		theSvcResp = pointsRewardService.getPointsRewardForProcessing();
//		theRewards = ( (RewardBatchDto) theSvcResp.getReturnObj() ).getRewards();
//		Assert.assertEquals( 1, theRewards.size() );
//	}
//
//	@Test
//	public void testGetPointsRewardForApproval() {
//
//		setupPointsRewardServiceTest();
//
//		pointsRewardService.generatePointsRewardForApproval( new Date(), new Date() );
//		ServiceDto theSvcResp = pointsRewardService.getPointsRewardForApproval();
//		List<PointsTxnModel> theRewards = ( (RewardBatchDto) theSvcResp.getReturnObj() ).getRewards();
//		Assert.assertTrue( theRewards.size() == 1 );
//
//		List<String> theRewardIds = new ArrayList<String>();
//		for ( PointsTxnModel theReward : theRewards ) {
//			theRewardIds.add( theReward.getId() );
//		}
//		pointsRewardService.rejectPointsRewards( theRewardIds, new RewardBatchDto() );
//		theSvcResp = pointsRewardService.getPointsRewardForApproval();
//		theRewards = ( (RewardBatchDto) theSvcResp.getReturnObj() ).getRewards();
//		Assert.assertTrue( theRewards.size() == 0 );
//	}
//
//	@SuppressWarnings("rawtypes")
//	@Test
//	@Ignore //Ignore since getPointsRewardForProcessing no longer return list of rewards
//	public void testRejectPointsRewards() {
//
//		setupPointsRewardServiceTest();
//
//		pointsRewardService.generatePointsRewardForApproval( new Date(), new Date() );
//		ServiceDto theSvcResp = pointsRewardService.getPointsRewardForProcessing();
//		List<PointsTxnModel> theRewards = ( (RewardBatchDto) theSvcResp.getReturnObj() ).getRewards();
//
//		List<String> theRewardIds = new ArrayList<String>();
//		for ( PointsTxnModel theReward : theRewards ) {
//			theRewardIds.add( theReward.getId() );
//		}
//		theSvcResp = pointsRewardService.rejectPointsRewards( theRewardIds, new RewardBatchDto() );
//		Assert.assertNotNull( (List) theSvcResp.getReturnObj()  );
//		Assert.assertEquals( 1, ( (List) theSvcResp.getReturnObj() ).size() );
//	}
//
//	@Test
//	@Ignore //Ignore since getPointsRewardForProcessing no longer return list of rewards
//	public void testDeletePointsReward() {
//
//		setupPointsRewardServiceTest();
//
//		pointsRewardService.generatePointsRewardForApproval( new Date(), new Date() );
//		ServiceDto theSvcResp = pointsRewardService.getPointsRewardForProcessing();
//		List<PointsTxnModel> theRewards = ( (RewardBatchDto) theSvcResp.getReturnObj() ).getRewards();
//
//		List<String> theRewardIds = new ArrayList<String>();
//		for ( PointsTxnModel theReward : theRewards ) {
//			theRewardIds.add( theReward.getId() );
//		}
//		theSvcResp = pointsRewardService.deletePointsReward( theRewardIds.get(0) );
//		Assert.assertEquals( TxnStatus.DELETED, ( (PointsTxnModel)theSvcResp.getReturnObj() ).getStatus()  );
//
//		theSvcResp = pointsRewardService.getPointsRewardForProcessing();
//		theRewards = ( (RewardBatchDto) theSvcResp.getReturnObj() ).getRewards();
//		Assert.assertEquals(  0, theRewards.size() );
//
//		pointsRewardService.generatePointsRewardForApproval( new Date(), new Date() );
//		theSvcResp = pointsRewardService.getPointsRewardForProcessing();
//		theRewards = ( (RewardBatchDto) theSvcResp.getReturnObj() ).getRewards();
//		Assert.assertEquals( 1, theRewards.size() );
//	}




	private void setupPointsRewardServiceTest() {

		setupStores();
		setupRewardSchemes();
		setupMemberTypes();
		setupEmployeeMembers();
		setupInactiveEmployeeMembers();
		setupOtherMembers();
		setupPurchaseTxns();
	}

	private void setupStores() {

		Store theStore = new Store( "TEST" );
		theStore.setId(new Integer(1));
		storeRepo.save( theStore );
		
	}

	private void setupRewardSchemes() {

		RewardSchemeModel theScheme = new RewardSchemeModel();
		theScheme.setAmount( 1000D );
		theScheme.setStatus( Status.ACTIVE );
		theScheme.setValueFrom( 1000D );
		theScheme.setValueTo( 2000D );
		rewardSchemeRepo.saveAndFlush( theScheme );

		theScheme = new RewardSchemeModel();
		theScheme.setAmount( 2000D );
		theScheme.setStatus( Status.ACTIVE );
		theScheme.setValueFrom( 2001D );
		theScheme.setValueTo( 3000D );
		rewardSchemeRepo.saveAndFlush( theScheme );
	}

	private void setupMemberTypes() {
		
		LookupHeader header = new LookupHeader();
    	header.setCode("membertype");
    	header.setDescription("membertype");
    	header = lookupHeaderRepo.saveAndFlush(header);
    	
    	LookupDetail theMemberType = new LookupDetail();
    	theMemberType.setHeader(header);
    	theMemberType.setCode(employeeType);
    	theMemberType.setDescription(employeeTypeDesc);
    	theMemberType.setStatus(Status.ACTIVE);
    	lookupDetailRepo.saveAndFlush(theMemberType);
    	
    	theMemberType = new LookupDetail();
    	theMemberType.setHeader(header);
    	theMemberType.setCode(individualType);
    	theMemberType.setDescription(individualTypeDesc);
    	theMemberType.setStatus(Status.ACTIVE);
    	lookupDetailRepo.saveAndFlush(theMemberType);
	}

	private void setupEmployeeMembers() {

		MemberModel theMember = new MemberModel();
		theMember.setEnabled( true );
		theMember.setAccountId( EMP1_ACCTID );
		theMember.setUsername( EMP1_ACCTID );
		theMember.setPassword( EMP1_ACCTID );
		theMember.setPin( EMP1_ACCTID );
		theMember.setContact( "0920001" );
		theMember.setAccountStatus( MemberStatus.ACTIVE );
		theMember.setRegisteredStore( "TEST" );
		theMember.setMemberType( new LookupDetail(employeeType) );
		memberRepo.saveAndFlush( theMember );

		theMember = new MemberModel();
		theMember.setEnabled( true );
		theMember.setAccountId( EMP2_ACCTID );
		theMember.setUsername( EMP2_ACCTID );
		theMember.setPassword( EMP2_ACCTID );
		theMember.setPin( EMP2_ACCTID );
		theMember.setContact( "0920002" );
		theMember.setAccountStatus( MemberStatus.ACTIVE );
		theMember.setRegisteredStore( "TEST" );
		theMember.setMemberType( new LookupDetail(employeeType) );
		memberRepo.saveAndFlush( theMember );
	}

	private void setupInactiveEmployeeMembers() {

		MemberModel theMember = new MemberModel();
		theMember.setEnabled( true );
		theMember.setAccountId( EMP3_INACID );
		theMember.setUsername( EMP3_INACID );
		theMember.setPassword( EMP3_INACID );
		theMember.setPin( EMP3_INACID );
		theMember.setContact( "0920004" );
		theMember.setAccountStatus( MemberStatus.TERMINATED );
		theMember.setRegisteredStore( "TEST" );
		theMember.setMemberType( new LookupDetail(employeeType) );
		memberRepo.saveAndFlush( theMember );
	}

	private void setupOtherMembers() {

		MemberModel theMember = new MemberModel();
		theMember.setEnabled( true );
		theMember.setAccountId( MEM1_ACCTID );
		theMember.setUsername( MEM1_ACCTID );
		theMember.setPassword( MEM1_ACCTID );
		theMember.setPin( MEM1_ACCTID );
		theMember.setContact( "0920003" );
		theMember.setAccountStatus( MemberStatus.ACTIVE );
		theMember.setMemberType( new LookupDetail(individualType) );
		memberRepo.saveAndFlush( theMember );
	}

	private void setupPurchaseTxns() {

		EmployeePurchaseTxnModel theTxn = new EmployeePurchaseTxnModel();
		theTxn.setTransactionAmount( 1000D );
		theTxn.setTransactionNo( "LASTMONTH_1000D" );
		theTxn.setMemberModel( memberRepo.findByAccountId( EMP1_ACCTID ) );
		theTxn.setId(customIdGeneratorService.generateId());
		theTxn.setCreated(new DateTime()); //TODO temp solution to created datetime not being populated
		employeePurchaseTxnRepo.saveAndFlush( theTxn );

		theTxn = new EmployeePurchaseTxnModel();
		theTxn.setTransactionAmount( 2000D );
		theTxn.setTransactionNo( "LASTMONTH_2000D" );
		theTxn.setMemberModel( memberRepo.findByAccountId( EMP1_ACCTID ) );
		theTxn.setId(customIdGeneratorService.generateId());
		theTxn.setCreated(new DateTime());
		employeePurchaseTxnRepo.saveAndFlush( theTxn );

		theTxn = new EmployeePurchaseTxnModel();
		theTxn.setTransactionAmount( 900D );
		theTxn.setTransactionNo( "LASTMONTH_900D" );
		theTxn.setMemberModel( memberRepo.findByAccountId( EMP2_ACCTID ) );
		theTxn.setId(customIdGeneratorService.generateId());
		theTxn.setCreated(new DateTime());
		employeePurchaseTxnRepo.saveAndFlush( theTxn );

		theTxn = new EmployeePurchaseTxnModel();
		theTxn.setTransactionAmount( 1000D );
		theTxn.setTransactionNo( "LASTMONTH_1000D" );
		theTxn.setMemberModel( memberRepo.findByAccountId( EMP3_INACID ) );
		theTxn.setId(customIdGeneratorService.generateId());
		theTxn.setCreated(new DateTime());
		employeePurchaseTxnRepo.saveAndFlush( theTxn );

		theTxn = new EmployeePurchaseTxnModel();
		theTxn.setTransactionAmount( 2000D );
		theTxn.setTransactionNo( "LASTMONTH_2000D" );
		theTxn.setMemberModel( memberRepo.findByAccountId( EMP3_INACID ) );
		theTxn.setId(customIdGeneratorService.generateId());
		theTxn.setCreated(new DateTime());
		employeePurchaseTxnRepo.saveAndFlush( theTxn );
	}
}
