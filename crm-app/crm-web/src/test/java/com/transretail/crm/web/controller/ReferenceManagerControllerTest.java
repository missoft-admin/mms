package com.transretail.crm.web.controller;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.argThat;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.reset;
import static org.springframework.http.MediaType.APPLICATION_JSON;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentMatcher;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.AnnotationConfigWebContextLoader;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.transretail.crm.common.service.dto.response.CodeDescDto;
import com.transretail.crm.common.service.exception.MessageSourceResolvableException;
import com.transretail.crm.core.service.CodePropertiesService;
import com.transretail.crm.core.service.LookupService;
import com.transretail.crm.web.controller.util.ControllerResponse;

/**
 * @author Allan G. Ramirez (agramirez@exist.com)
 */
@RunWith(SpringJUnit4ClassRunner.class)
@WebAppConfiguration
@ContextConfiguration(loader = AnnotationConfigWebContextLoader.class)
public class ReferenceManagerControllerTest extends AbstractMockMvcTest {
    @Configuration
    @EnableWebMvc
    static class Config {
        @Bean
        public ReferenceManagerController referenceManagerController() {
            return new ReferenceManagerController();
        }

        @Bean
        public LookupService lookupService() {
            return mock(LookupService.class);
        }

        @Bean
        public MessageSource messageSource() {
            return mock(MessageSource.class);
        }

        @Bean
        public CodePropertiesService codePropertiesService() {
            return mock(CodePropertiesService.class);
        }

    }

    @Autowired
    private LookupService lookupService;
    @Autowired
    private MessageSource messageSource;
    @Autowired
    private CodePropertiesService codePropertiesService;

    public void doSetup() {
        reset(lookupService);
        reset(messageSource);
        reset(codePropertiesService);
    }

    @Test
    public void successfulUpdateHeaderTest() throws Exception {
        ObjectMapper mapper = new ObjectMapper();
        CodeDescDto dto = new CodeDescDto();
        dto.setCode("code");
        dto.setDesc("desc");
        String content =
            mockMvc.perform(post("/lookup/header/update").content(mapper.writeValueAsBytes(dto)).contentType(APPLICATION_JSON))
                .andExpect(status().isOk()).andReturn().getResponse().getContentAsString();
        ControllerResponse response = mapper.readValue(content, ControllerResponse.class);
        assertTrue(response.isSuccess());
    }

    @Test
    public void withErrorUpdateHeaderTest() throws Exception {
        ObjectMapper mapper = new ObjectMapper();
        CodeDescDto dto = new CodeDescDto();
        dto.setCode("code");
        dto.setDesc("desc");

        MessageSourceResolvableException ex = new MessageSourceResolvableException("ERROR", null, "ERROR");
        doThrow(ex).when(lookupService).updateHeader(argThat(new ArgumentMatcher<CodeDescDto>() {
            @Override
            public boolean matches(Object argument) {
                CodeDescDto codeDescDto = (CodeDescDto) argument;
                return codeDescDto.getCode().equals("code");
            }
        }));
        String content =
            mockMvc.perform(post("/lookup/header/update").content(mapper.writeValueAsBytes(dto)).contentType(APPLICATION_JSON))
                .andExpect(status().isOk()).andReturn().getResponse().getContentAsString();
        System.out.println(content);
        ControllerResponse response = mapper.readValue(content, ControllerResponse.class);
        assertFalse(response.isSuccess());
        assertEquals("ERROR", response.getMessage());
    }
}
