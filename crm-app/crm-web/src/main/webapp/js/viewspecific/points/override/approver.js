$(document).ready( function() {

	var $theApproverForm = $( "#form_approver" );



	initLinks();



	function initLinks() {

		var $theLink_createApprover = $( "#link_createApprover" );
		$theLink_createApprover.click( function(e) {
			e.preventDefault();
			createApprover( $theLink_createApprover.data( "url" ) );
		});
	}



	function createApprover( inUrl ) {
		$.ajax({
			cache 		: false, 
			dataType 	: "html",
			type 		: "GET", 
			url 		: inUrl,
			error 		: function(jqXHR, textStatus, errorThrown) {}, 
			success 	: function( inResponse ) {
				$theApproverForm.html( inResponse );
			}
		});
	}

});