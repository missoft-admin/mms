var EmailDraftForm = null;

$(document).ready( function() {

	var errorContainer = $( "#content_emailDraftForm" ).find( "#content_error" );

	var $dialog = $( "#content_emailDraftForm" );
	var $form = $( "#emailDraftForm" );
	var $schedule = $form.find( "#schedule" );
	var $dispSchedule = $form.find( "#dispSchedule" );
	var $dispTime = $form.find( "#dispTime" );
	var $save = $form.find( "#saveBtn" );

	var $file = $form.find( "#file" );
	var $deleteFile = $form.find( "#deleteFile" );
	var $previewLinkForUpload = $form.find( "#filePreviewForUpload" );
	var $clearLinkForUpload = $form.find( "#fileClearForUpload" );
	var $fileForUpload = $dialog.find( "#fileForUpload" );
	var $previewLinkUploaded = $form.find( "#filePreviewUploaded" );
	var $clearLinkUploaded = $form.find( "#fileClearUploaded" );
	var $uploadedFile = $form.find( "#uploadedFile" );

	var $message = $form.find( "#message" );
	var $messageCount = $form.find( "#messageCount" );

	var $memberGroup = $form.find( "#memberGroup" );
	var $promotion = $form.find( "#promotion" );

	var $memberGroupSelections = $form.find( "#memberGroupSelection" );
	var $memberGroupSelected = $form.find( "#memberGroupSelected" );
	var memberGroupSelections = null;
	var $promotionSelections = $form.find( "#promotionSelection" );
	var $promotionSelected = $form.find( "#promotionSelected" );
	var promotionSelections = null;



	initDialog();
	initFormFields();



	function initDialog() {
		$dialog.modal({ show : false });
		$dialog.on( "hide", function(e) { 
			if ( e.target === this ) { 
				errorContainer.hide();
				reset( $form );
				resetFields();
			}
		});
	}

	function initFormFields() {
		
		$form.submit( save );
		
		$save.click(function() {
			$form.submit();
		});

		$file.change( function(e) {
			if ( $(this).val() ) {
				$previewLinkForUpload.show();
				$clearLinkForUpload.show();
			}
			else {
				$previewLinkForUpload.hide();
				$clearLinkForUpload.hide();
			}
		});
		$previewLinkForUpload.click( function(e) {
			e.preventDefault();
			var $clone = $file.clone( true );
			$fileForUpload.html( $file.after( $clone ) );
			$fileForUpload.submit();
			$clone.after( $file );
			$clone.remove();
		});
		$clearLinkForUpload.click( function(e) {
			e.preventDefault();
			$file.val( "" ).change();
		});
		$clearLinkUploaded.click( function(e) {
			e.preventDefault();
			$deleteFile.val( true );
			$uploadedFile.remove();
		});

		$schedule.datepicker({ autoclose : true, format : "dd M yyyy", startDate : '-0d' })
			.on( "changeDate", function(e) { 
				$dispSchedule.val( ( $schedule.datepicker( "getDate" ).getTime() )? 
						$schedule.datepicker( "getDate" ).getTime() : new Date().getTime() ); });

		$dispTime.attr( "readonly", true );
		$dispTime.timepicker({ showMeridian : true, /*showSeconds : true, */defaultTime : "00:00:00", showInputs : false, minuteStep : 15 })
			.on( "changeTime", function(e) { if ( !$dispTime.val() ) { $dispTime.val( "00:00:00" ); }  });
		//$dispTime.change( function(e) { MarketingChannel.setResetTimepicker( true ); });

		initMessageField();
		initTargetFields();
	}

	function initTargetFields() {
		memberGroupSelections = $memberGroupSelections.find( "option" );
		$.each( memberGroupSelections, function() {
			if ( $memberGroup.val().split( "," ).indexOf( $(this).val() ) != -1 ) {
				$memberGroupSelected.append( $(this) );
			}
		});
		promotionSelections = $promotionSelections.find( "option" );
		$.each( promotionSelections, function() {
			if ( $promotion.val().split( "," ).indexOf( $(this).val() ) != -1 ) {
				$promotionSelected.append( $(this) );
			}
		});
		$memberGroupSelected.change( function(e) {
			$memberGroup.val( $memberGroupSelected.find( "option" ).map( function(){ return this.value; } ).get().join(",") );
		});
		$promotionSelected.change( function(e) {
			$promotion.val( $promotionSelected.find( "option" ).map( function(){ return this.value; } ).get().join(",") );
		});

		$memberGroupSelections.dblclick( function(e) {
			$form.find( "#addGroupBtn" ).trigger( "click" );
		});
		$memberGroupSelected.dblclick( function(e) {
			$form.find( "#removeGroupBtn" ).trigger( "click" );
		});
		$promotionSelections.dblclick( function(e) {
			$form.find( "#addPromoBtn" ).trigger( "click" );
		});
		$promotionSelected.dblclick( function(e) {
			$form.find( "#removePromoBtn" ).trigger( "click" );
		});

		$form.find( "#addGroupBtn" ).click( function(e) {
			e.preventDefault();
			$.each( $memberGroupSelections.find("option:selected"), function() { $memberGroupSelected.append( $(this) ); });
			$memberGroupSelected.change();
		});
		$form.find( "#removeGroupBtn" ).click( function(e) {
			e.preventDefault();
			$.each( $memberGroupSelected.find("option:selected"), function() { $memberGroupSelections.append( $(this) ); });
			$memberGroupSelected.change();
		});
		$form.find( "#addPromoBtn" ).click( function(e) {
			e.preventDefault();
			$.each( $promotionSelections.find("option:selected"), function() { $promotionSelected.append( $(this) ); });
			$promotionSelected.change();
		});
		$form.find( "#removePromoBtn" ).click( function(e) {
			e.preventDefault();
			$.each( $promotionSelected.find("option:selected"), function() { $promotionSelections.append( $(this) ); });
			$promotionSelected.change();
		});

		initViewGroup();
		initTargetCount();
	}

	function initViewGroup() {
		var isViewShown = false;
		var $viewMemberGroupLink = $form.find( "#link_viewMemberGroup" );
		var $viewMemberGroup = $form.find( "#targetMemberGroupView" );
		$viewMemberGroupLink.click( function(e) {
			e.preventDefault();
			$.get( $(this).attr( "href" ), function( resp ) {
				$viewMemberGroup.html( resp );
				$viewMemberGroup.show( "slow" );
				isViewShown = true;
			}, "html" );
		});

		var origLink = $viewMemberGroupLink.attr( "href" );
		$memberGroupSelections.find( "option" ).click( function(e) {
			$viewMemberGroupLink.show();
			$viewMemberGroupLink.attr( "href", origLink + $(this).val() );
			//if ( isViewShown ) { $viewMemberGroupLink.click(); }
		});
		$memberGroupSelected.find( "option" ).click( function(e) {
			$viewMemberGroupLink.show();
			$viewMemberGroupLink.attr( "href", origLink + $(this).val() );
			//if ( isViewShown ) { $viewMemberGroupLink.click(); }
		});

		$dialog.on( "hide", function(e) { 
			if ( e.target === this ) {
				$viewMemberGroup.empty();
				$viewMemberGroup.hide();
				isViewShown = false;
				$viewMemberGroupLink.attr( "href", origLink );
			}
		});
	}

	function initTargetCount() {
		$form.find( "#link_targetCount" ).click( function(e) {
			e.preventDefault();
			$.get( 
				$(this).attr( "href" )
					.replace( new RegExp( "%MEMBERGRP%", "g" ), $memberGroupSelected.find( "option" ).map( function(){ return this.value; } ).get().join(",") )
					.replace( new RegExp( "%PROMO%", "g" ), $promotionSelected.find( "option" ).map( function(){ return this.value; } ).get().join(",") ), 
				function( resp ) {
					$form.find( "#targetCount" ).html( " = " + resp.result );
			});
		});
	}

	function initMessageField() {
		var maxCount = $message.data( "max-count" );
		$message.bind( "keyup", function(e) {
			var length = $(this).val().length;
			return limitCharSize( e, length, maxCount, function() { 
				$messageCount.html( maxCount - length );
			});
		}).trigger( "keyup" );
		$message.bind( $.browser.opera ? "keypress" : "keydown", function(e) {
			var length = $(this).val().length;
			return limitCharSize( e, length, maxCount - 1, function() { 
				$messageCount.html( maxCount - length );
			});
		}).trigger( $.browser.opera ? "keypress" : "keydown" );
	}

	function limitCharSize( e, charLength, limit, func ) {
		/*var value = "";
		if( window.chrome ){ value = $(this).val().replace( /(\r\n|\n|\r)/g, "  " ); }
		else{ value = $(this).val();}*/
		if ( charLength > limit ) {
			if ( $.inArray( e.keyCode, [46, 8, 9, 27] ) != -1 || ( e.keyCode == 65 && e.ctrlKey === true ) || ( e.keyCode >= 35 && e.keyCode <= 39 ) ) {
				return true;
			}
			return false;
		}
		return func();
	}



	function save(e) {
		$save.prop( "disabled", true );

		$dispTime.val( ( $dispTime.val() )? $dispTime.val() + "" : "00:00:00" );
		if ( window.formData === undefined ) {
			e.preventDefault();
			$.ajax({
				url		: $form.attr( "action" ),
				type	: "POST",
				data	: new FormData(this),
				contentType : false,
				processData : false,
				dataType: "json",
				mimeType:"multipart/*",
				cache 	: false, 
				success : function( resp ) { processResp( resp ); }
			});
		}
		else {
			var iframeId = 'unique' + ( new Date().getTime() );
			var iframe = $( '<iframe src="javascript:false;" name="'+iframeId+'" />' );
			iframe.hide();
			$(this).attr( 'target', iframeId );
			iframe.appendTo( 'body' );
			iframe.load( function(e) {
				var doc = getDoc( iframe[0] );
				var docRoot = doc.body ? doc.body : doc.documentElement;
				var resp = JSON.parse( docRoot.innerText );				
				processResp( resp );
			});
		}
		
	}



	function reset( ele, isEmpty ) {
	    $( ele ).find(':input').each( function() {
	        switch(this.type) {
	            case 'password':
	            case 'select-one':
	            case 'text':
	            case 'textarea':
	            case 'file':
	            case 'hidden': $(this).val(''); break;
	            case 'select-multiple': isEmpty? $(this).empty() : $(this).val(""); break;
	            case 'checkbox':
	            case 'radio': this.checked = false;
	        }
	    });

	}

	function getDoc( frame ) {
		var doc = null;
		try {
			if ( frame.contentWindow ) { doc = frame.contentWindow.document; }
		}
		catch( err ) {
			try {
				doc = frame.contentDocument ? frame.contentDocument : frame.document;
			}
			catch(err) {
				doc = frame.document;
			}
		}
		return doc;
	}

	function processResp( resp ) {
		if ( resp.success ) {
			$dialog.modal( "hide" );
			MarketingChannelList.reloadTable();
		}
		else {
			var errors = "";
			$( resp.result ).each( function( key, value ) { errors += ( ( key + 1 ) + ". " + value.code + "<br>" ); });
			errorContainer.find( "div" ).html( errors );
			errorContainer.show( "slow" );
		}
		$save.prop( "disabled", false );		
	}

	function resetFields() {
		$memberGroupSelections.empty();
		$.each( memberGroupSelections, function() {
			$memberGroupSelections.append( $(this) );
		});
		$promotionSelections.empty();
		$.each( promotionSelections, function() {
			$promotionSelections.append( $(this) );
		});
		$form.find( "#targetCount" ).html( "" );
		$form.find( "#uploadedFile" ).remove();
	}



	EmailDraftForm = {
		show	: function( ) {
			$dialog.modal( "show" );
		},
		create	: function( action, modalHeader ) {
			$form.attr( "action", action );
			$form.find( "#code" ).remove();
			$form.find( ".modal-header > span" ).html( modalHeader );
			$dialog.modal( "show" );
		}
	};

});