package com.transretail.crm.rest.pos;

import com.google.common.collect.Lists;
import com.transretail.crm.common.service.dto.rest.ReturnMessage;
import com.transretail.crm.core.dto.StampPromoPortaVoidRestDto;
import com.transretail.crm.core.entity.*;
import com.transretail.crm.core.entity.enums.AppKey;
import com.transretail.crm.core.entity.enums.StampTxnType;
import com.transretail.crm.core.repo.ApplicationConfigRepo;
import com.transretail.crm.core.security.service.MD5Service;
import com.transretail.crm.core.security.service.impl.MD5ServiceImpl;
import com.transretail.crm.core.service.*;
import com.transretail.crm.core.util.generator.IdGeneratorService;
import com.transretail.crm.loyalty.entity.LoyaltyCardInventory;
import com.transretail.crm.loyalty.service.LoyaltyCardService;
import com.transretail.crm.rest.common.PointsManagerController;
import com.transretail.crm.web.controller.AbstractMockMvcTest;
import org.joda.time.DateTime;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.AnnotationConfigWebContextLoader;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

import java.util.List;

import static org.hamcrest.Matchers.is;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

/**
 * @author Allan G. Ramirez (agramirez@exist.com), Mike de Guzman
 */
@RunWith(SpringJUnit4ClassRunner.class)
@WebAppConfiguration
@ContextConfiguration(loader = AnnotationConfigWebContextLoader.class)
public class PointsManagerControllerTest extends AbstractMockMvcTest {

    @Configuration
    @EnableWebMvc
    static class Config {
        
        @Bean
        public PointsManagerController pointsManagerController() {
            return new PointsManagerController();
        }

        @Bean
        public EmployeeService employeeService() {
            return mock(EmployeeService.class);
        }

        @Bean
        public MemberService memberService() {
            return mock(MemberService.class);
        }

        @Bean
        public PointsTxnManagerService pointsManagerService() {
            return mock(PointsTxnManagerService.class);
        }

        @Bean
        public PromotionService promotionService() {
            return mock(PromotionService.class);
        }

        @Bean
        public MessageSource messageSource() {
            return mock(MessageSource.class);
        }

        @Bean
        public StoreService storeService() {
            return mock(StoreService.class);
        }

        @Bean
        public EmployeePurchaseService employeePurchaseService() {
            return mock(EmployeePurchaseService.class);
        }

        @Bean
        public CodePropertiesService codePropertiesService() {
            return mock(CodePropertiesService.class);
        }

        @Bean
        public UserService userService() {
            return mock(UserService.class);
        }

        @Bean
        public ApplicationConfigRepo applicationConfigRepo() {
            return mock(ApplicationConfigRepo.class);
        }

        @Bean
        public LoyaltyCardService loyaltyCardService() {
            return mock(LoyaltyCardService.class);
        }

        @Bean
        public MD5Service mD5Service() {
            return spy(new MD5ServiceImpl());
        }

        @Bean
        public StoreMemberService storeMemberService() {
            return mock(StoreMemberService.class);
        }

        @Bean
        public IdGeneratorService customIdGeneratorService() {
            return mock(IdGeneratorService.class);
        }

        @Bean
        public TransactionAuditService transactionAuditService() {
            return mock(TransactionAuditService.class);
        }

        @Bean
        public CrmIdRefService crmIdRefService() {
            return mock(CrmIdRefService.class);
        }

        @Bean
        public RestUrlService restUrlService() {
            return mock(RestUrlService.class);
        }

        @Bean
        public StampService stampService() {
            return mock(StampService.class);
        }

        @Bean
        public StampPromoService stampPromoService() {
            return mock(StampPromoService.class);
        }
    }

    @Autowired
    private EmployeeService employeeService;

    @Autowired
    private MemberService memberService;

    @Autowired
    private PointsTxnManagerService pointsManagerService;

    @Autowired
    private PromotionService promotionService;

    @Autowired
    private MessageSource messageSource;

    @Autowired
    private StoreService storeService;

    @Autowired
    private EmployeePurchaseService employeePurchaseService;

    @Autowired
    private CodePropertiesService codePropertiesService;

    @Autowired
    private UserService userService;

    @Autowired
    private ApplicationConfigRepo applicationConfigRepo;

    @Autowired
    private LoyaltyCardService loyaltyCardService;

    @Autowired
    private MD5Service mD5Service;

    @Autowired
    private StoreMemberService storeMemberService;

    @Autowired
    private IdGeneratorService customIdGeneratorService;

    @Autowired
    private TransactionAuditService transactionAuditService;

    @Autowired
    private CrmIdRefService crmIdRefService;

    @Autowired
    private RestUrlService restUrlService;

    @Autowired
    private StampService stampService;

    @Autowired
    private StampPromoService stampPromoService;

    @Override
    public void doSetup() {
        reset(employeeService);
        reset(memberService);
        reset(pointsManagerService);
        reset(promotionService);
        reset(messageSource);
        reset(storeService);
        reset(employeePurchaseService);
        reset(codePropertiesService);
        reset(userService);
        reset(applicationConfigRepo);
        reset(loyaltyCardService);
        reset(mD5Service);
        reset(storeMemberService);
        reset(customIdGeneratorService);
        reset(transactionAuditService);
        reset(crmIdRefService);
        reset(restUrlService);
        reset(stampService);
        reset(stampPromoService);
    }

    @Test
    public void earnPointsTest() throws Exception {
//        TODO: Validate returned content
//        mockMvc.perform(get("/points/earn/1/store/paymentType/amount/null/transactionNo/transactionDate")).andExpect(status().isOk());
    }

    @Test
    public void earnStampsTest() throws Exception {
        String id = "10001";
        String store = "STORE1";
        String paymentType = "PTYP001";
        String amount = "1000";
        String cardNumber = "12345";
        String transactionNo = "123456";
        String transactionDate = "01122014000001";
        String items = "ITEM1";

        when(memberService.findActiveWithAccountId(anyString())).thenReturn(mock(MemberModel.class));
        LoyaltyCardInventory loyaltyCard = mock(LoyaltyCardInventory.class);
        when(loyaltyCardService.findLoyaltyCard(anyString())).thenReturn(loyaltyCard);
        when(loyaltyCard.getExpiryDate()).thenReturn(null);
        ApplicationConfig appConfig = mock(ApplicationConfig.class);
        when(applicationConfigRepo.findByKey(AppKey.ALLOW_NO_LOYALTYCARD_TO_EARN)).thenReturn(appConfig);
        when(appConfig.getValue()).thenReturn("true");

        doNothing().when(promotionService).processPromotions(any(MemberModel.class), anyString(), anyString(), anyString(), anyString(),
                anyString(), anyString(), any(List.class), any(ReturnMessage.class));


        mockMvc.perform(get("/points/earnpoints/{id}/{store}/{paymentType}/{amount}/{cardNumber}/{transactionNo}/{transactionDate}/{items}",
                id, store, paymentType, amount, cardNumber, transactionNo, transactionDate, items))
                .andExpect(jsonPath("$.type", is("SUCCESS")))
                .andExpect(jsonPath("$.callingAction", is("EARN")))
                .andExpect(status().isOk());
    }


    @Test
    public void voidStampsTest() throws Exception {
        String transactionNo = "123456";
        String transactionDate = "01122014000001";

        when(restUrlService.callPrimaryWsUrl(anyString(), anyString(), anyBoolean(), any(ReturnMessage.class), anyString())).thenReturn(false);
        StampTxnModel stampTxnModel = new StampTxnModel();
        stampTxnModel.setTransactionType(StampTxnType.EARN);
        StampTxnHeader stampTxnHeader = new StampTxnHeader();
        stampTxnHeader.setTransactionNo(transactionNo);
        stampTxnHeader.setTotalTxnStamps(100D);
        MemberModel memberModel = new MemberModel();
        stampTxnHeader.setMemberModel(memberModel);
        StampTxnDetail stampTxnDetail = mock(StampTxnDetail.class);
        stampTxnHeader.getTransactionDetails().add(stampTxnDetail);
        stampTxnModel.setTransactionHeader(stampTxnHeader);

        List<StampTxnModel> stampTxnsMock = Lists.newArrayList(stampTxnModel);

        when(stampService.retrieveStampsByTransactionNo(transactionNo)).thenReturn(stampTxnsMock);

        StampTxnModel voidStampTnxMock = mock(StampTxnModel.class);
        StampTxnHeader voidStampTxnHeaderMock = mock(StampTxnHeader.class);
        MemberModel memberModelMock = mock(MemberModel.class);
        Promotion promotionMock = mock(Promotion.class);
        when(voidStampTnxMock.getTransactionHeader()).thenReturn(voidStampTxnHeaderMock);
        when(voidStampTnxMock.getStampPromotion()).thenReturn(promotionMock);
        when(voidStampTnxMock.getCreated()).thenReturn(DateTime.now());

        when(voidStampTxnHeaderMock.getMemberModel()).thenReturn(memberModelMock);
        when(voidStampTxnHeaderMock.getStoreCode()).thenReturn("STORE1");
        when(voidStampTxnHeaderMock.getTransactionNo()).thenReturn("123456");
        when(voidStampTxnHeaderMock.getTotalTxnStamps()).thenReturn(100D);
        when(memberModelMock.getId()).thenReturn(1L);
        when(promotionMock.getId()).thenReturn(1L);
        when(promotionMock.getName()).thenReturn("PROMO1");

        when(stampService.saveStamp(any(StampTxnModel.class))).thenReturn(voidStampTnxMock);


        doNothing().when(promotionService).processPromotions(any(MemberModel.class), anyString(), anyString(), anyString(), anyString(),
                anyString(), anyString(), any(List.class), any(ReturnMessage.class));

        mockMvc.perform(get("/points/void/{transactionNo}/{transactionDate}", transactionNo, transactionDate))
                .andExpect(jsonPath("$.type", is("SUCCESS")))
                .andExpect(jsonPath("$.callingAction", is("VOID")))
                .andExpect(status().isOk());

        verify(stampService).saveStamp(any(StampTxnModel.class));
        verify(stampPromoService).callPortaVoidStampPromo(any(StampPromoPortaVoidRestDto.class));
    }

}