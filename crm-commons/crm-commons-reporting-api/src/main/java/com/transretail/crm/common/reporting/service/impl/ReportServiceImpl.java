package com.transretail.crm.common.reporting.service.impl;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.Map;

import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import com.transretail.crm.common.reporting.exception.ReportException;
import com.transretail.crm.common.reporting.jasper.JRProcessor;
import com.transretail.crm.common.reporting.service.ReportService;

import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRExporter;
import net.sf.jasperreports.engine.JRExporterParameter;
import net.sf.jasperreports.engine.export.JRPdfExporter;
import net.sf.jasperreports.engine.export.JRXlsExporterParameter;
import net.sf.jasperreports.engine.export.ooxml.JRXlsxExporter;

/**
 * @author Allan G. Ramirez (agramirez@exist.com)
 */
@Service("reportService")
public class ReportServiceImpl implements ReportService {
    private Logger logger = LoggerFactory.getLogger(getClass());

    public void exportPdfReport(final JRProcessor processor, final File output) throws ReportException {
        if (logger.isDebugEnabled()) {
            logger.debug("Exporting [" + processor.getReportFileName() + "] report to pdf.");
        }
        boolean createdNewFile = false;
        OutputStream os = null;
        try {
            if (!output.exists()) {
                if (!output.createNewFile()) {
                    throw new ReportException("Failed to create file [" + output.getAbsolutePath() + "]");
                } else {
                    createdNewFile = true;
                }
            }
            os = new FileOutputStream(output);
            exportReport(new JRPdfExporter(), processor, os, true);
        } catch (Exception e) {
            if (createdNewFile) {
                IOUtils.closeQuietly(os);
                if (!output.delete()) {
                    logger.warn("Failed to delete file [" + output.getAbsolutePath() + "]");
                }
            }
            throw new ReportException(e);
        } finally {
            IOUtils.closeQuietly(os);
        }
    }

    public void exportPdfReport(JRProcessor processor, OutputStream os) throws ReportException {
        if (logger.isDebugEnabled()) {
            logger.debug("Exporting [" + processor.getReportFileName() + "] report to pdf.");
        }
        exportReport(new JRPdfExporter(), processor, os, false);
    }

    protected void exportReport(JRExporter exporter, JRProcessor processor, OutputStream os, boolean syncOutputStream)
        throws ReportException {
        try {
            // Set parameters specifict to JRProcessor
            for (Map.Entry<JRExporterParameter, Object> entry : processor.getJRExporterParameters().entrySet()) {
                exporter.setParameter(entry.getKey(), entry.getValue());
            }
            exporter.setParameter(JRExporterParameter.JASPER_PRINT_LIST, processor.getJasperPrints());
            exporter.setParameter(JRExporterParameter.OUTPUT_STREAM, os);
            exporter.exportReport();
            os.flush();
        } catch (JRException e) {
            throw new ReportException(e);
        } catch (IOException e) {
            throw new ReportException("Failed to flush to outputstream", e);
        } finally {
            // Ensure that JRExportProgressMonitor will always get called because some built-in jasper exporters don't invoke this
            if (processor.getAfterPageExportListener() != null) {
                processor.getAfterPageExportListener().afterPageExport();
            }
        }
    }

    public void exportExcelReport(JRProcessor processor, OutputStream os) throws ReportException {
        if (logger.isDebugEnabled()) {
            logger.debug("Exporting [" + processor.getReportFileName() + "] report to excel.");
        }
        JRXlsxExporter exporterXLS = new JRXlsxExporter();
        exporterXLS.setParameter(JRXlsExporterParameter.IS_ONE_PAGE_PER_SHEET, Boolean.FALSE);
        exporterXLS.setParameter(JRXlsExporterParameter.IS_WHITE_PAGE_BACKGROUND, Boolean.FALSE);
        exporterXLS.setParameter(JRXlsExporterParameter.IS_REMOVE_EMPTY_SPACE_BETWEEN_ROWS, Boolean.TRUE);
        exporterXLS.setParameter(JRXlsExporterParameter.IS_REMOVE_EMPTY_SPACE_BETWEEN_COLUMNS, Boolean.TRUE);
        exporterXLS.setParameter(JRXlsExporterParameter.IS_DETECT_CELL_TYPE, Boolean.TRUE);
        exporterXLS.setParameter(JRXlsExporterParameter.IGNORE_PAGE_MARGINS, Boolean.TRUE);
        exportReport(exporterXLS, processor, os, false);
    }
}
