package com.transretail.crm.common.web.converter;

import java.io.IOException;

import org.joda.time.LocalDate;
import org.joda.time.format.DateTimeFormatter;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import org.joda.time.format.ISODateTimeFormat;

/**
 *
 * @author Monte Cillo Co (mco@exist.com)
 */
public class ISODateDeserializer extends JsonDeserializer<LocalDate> {

    private static final DateTimeFormatter FORMATTER = ISODateTimeFormat.date();

    @Override
    public LocalDate deserialize(JsonParser jp, DeserializationContext ctxt) throws IOException {
	try{
	    return FORMATTER.parseLocalDate(jp.getText());
	}catch(RuntimeException ex){
	    return null;
	}
    }
}
