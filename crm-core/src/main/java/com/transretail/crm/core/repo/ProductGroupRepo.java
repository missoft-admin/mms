package com.transretail.crm.core.repo;

import com.transretail.crm.common.repo.CrmQueryDslPredicateExecutor;
import com.transretail.crm.core.entity.ProductGroup;

public interface ProductGroupRepo extends CrmQueryDslPredicateExecutor<ProductGroup, Long> {

}
