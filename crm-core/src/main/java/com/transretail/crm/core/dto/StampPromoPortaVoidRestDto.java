package com.transretail.crm.core.dto;

import com.google.common.collect.Lists;
import com.transretail.crm.core.service.StampPromoService;
import org.joda.time.DateTime;
import org.joda.time.LocalDate;

import java.io.Serializable;
import java.util.List;

import static org.apache.commons.lang3.StringUtils.isNotBlank;

/**
 * @author Mike de Guzman
 */
public class StampPromoPortaVoidRestDto implements Serializable {

    private String memberId;
    private String storeCode;
    private String transactionId;
    private String earnedTransactionId;
    private DateTime voidDate;
    private List<StampPromoPortaVoidDetailsDto> voidDetails = Lists.newArrayList();
    private String md5ValidationKey;

    public StampPromoPortaVoidRestDto() {}

    public StampPromoPortaVoidRestDto(String memberId, String storeCode, String transactionId, String earnedTransactionId,
            DateTime voidDate, String dateFormat) {

        this.memberId = memberId;
        this.storeCode = storeCode;
        this.transactionId = transactionId;
        this.earnedTransactionId = earnedTransactionId;
        this.voidDate = voidDate;
        this.md5ValidationKey = this.memberId + this.transactionId + voidDate.toString(isNotBlank(dateFormat) ? dateFormat : StampPromoService.MD5_DATEFORMAT);
        //<memberId><transactionId><earnDateYYYYMMDD>
    }

    public String getMemberId() {
        return memberId;
    }

    public void setMemberId(String memberId) {
        this.memberId = memberId;
    }

    public String getStoreCode() {
        return storeCode;
    }

    public void setStoreCode(String storeCode) {
        this.storeCode = storeCode;
    }

    public String getTransactionId() {
        return transactionId;
    }

    public void setTransactionId(String transactionId) {
        this.transactionId = transactionId;
    }

    public String getEarnedTransactionId() {
        return earnedTransactionId;
    }

    public void setEarnedTransactionId(String earnedTransactionId) {
        this.earnedTransactionId = earnedTransactionId;
    }

    public DateTime getVoidDate() {
        return voidDate;
    }

    public void setVoidDate(DateTime voidDate) {
        this.voidDate = voidDate;
    }

    public List<StampPromoPortaVoidDetailsDto> getVoidDetails() {
        return voidDetails;
    }

    public void setVoidDetails(List<StampPromoPortaVoidDetailsDto> voidDetails) {
        this.voidDetails = voidDetails;
    }

    public String getMd5ValidationKey() {
        return md5ValidationKey;
    }

    public void setMd5ValidationKey(String md5ValidationKey) {
        this.md5ValidationKey = md5ValidationKey;
    }
}
