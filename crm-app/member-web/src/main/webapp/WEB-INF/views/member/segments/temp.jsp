<%@ include file="../../taglibs.jsp"%>






      <div class="professionalDetails">
        <hr />
        <div class="control-group">
          <form:label path="customerSegmentationCode" cssClass="control-label" cssErrorClass="control-label error">
            <spring:message code="business_field" />
          </form:label>
          <div class="controls">
            <form:select id="customerGroup" path="customerGroupCode" items="${customerGroups}"
              title="Preferred Store" itemValue="code" itemLabel="description"
              cssClass="input-xlarge" />
            <select name="customerSegmentationCode" id="customerSegmentation"></select>
          </div>
        </div>
        <div class="control-group">
          <form:label path="companyName" cssClass="control-label"
            cssErrorClass="control-label error">
            <spring:message code="business_name" />
          </form:label>
          <div class="controls">
            <form:input path="companyName" cssClass="input-xlarge" />
          </div>
        </div>
        <div class="control-group">
          <form:label path="businessLicense" cssClass="control-label"
            cssErrorClass="control-label error">
            <spring:message code="business_license" />
          </form:label>
          <div class="controls">
            <form:input path="businessLicense" cssClass="input-xlarge" />
          </div>
        </div>
        <div class="control-group">
          <form:label path="npwp.npwpId" cssClass="control-label"
            cssErrorClass="control-label error">
            <spring:message code="id" />
          </form:label>
          <div class="controls">
            <form:input path="npwp.npwpId" cssClass="input-xlarge" id="npwpId"
              placeholder="#.###.###.#-###" />
          </div>
        </div>
        <div class="control-group">
          <form:label path="npwp.npwpName" cssClass="control-label"
            cssErrorClass="control-label error">
            <spring:message code="name" />
          </form:label>
          <div class="controls">
            <form:input path="npwp.npwpName" cssClass="input-xlarge"
              id="npwpName" />
          </div>
        </div>
        <div class="control-group">
          <form:label path="npwp.npwpAddress" cssClass="control-label"
            cssErrorClass="control-label error">
            <spring:message code="address" />
          </form:label>
          <div class="controls">
            <form:input path="npwp.npwpAddress" cssClass="input-xlarge"
              id="npwpAddress" />
          </div>
        </div>
      </div>


      <div class="professionalDetails">
        <div class="control-group">
          <form:label path="position" cssClass="control-label" cssErrorClass="control-label error">
            <spring:message code="position" />
          </form:label>
          <div class="controls">
            <form:input path="position" cssClass="input-xlarge" />
          </div>
        </div>
      </div>
      
      
      <div class="professionalDetails">
        <div class="control-group">
          <form:label path="storeName" cssClass="control-label"
            cssErrorClass="control-label error">
            <spring:message code="store_name" />
          </form:label>
          <div class="controls">
            <form:input path="storeName" cssClass="input-xlarge" />
          </div>
        </div>
        <div class="control-group">
          <form:label path="zone" cssClass="control-label"
            cssErrorClass="control-label error">
            <spring:message code="zone" />
          </form:label>
          <div class="controls">
            <form:input path="zone" cssClass="input-xlarge" />
          </div>
        </div>
        <div class="control-group">
          <form:label path="radius" cssClass="control-label"
            cssErrorClass="control-label error">
            <spring:message code="radius" />
          </form:label>
          <div class="controls">
            <form:input path="radius" cssClass="input-xlarge" />
          </div>
        </div>
      </div>
      




