package com.transretail.crm.giftcard.entity.support;

/**
 * @author ftopico
 */
public enum GiftCardExpirySearchCriteria {

    extendNo, seriesFrom, filedBy, orderNo
}
