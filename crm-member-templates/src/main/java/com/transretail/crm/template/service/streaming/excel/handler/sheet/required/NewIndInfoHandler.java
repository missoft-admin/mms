package com.transretail.crm.template.service.streaming.excel.handler.sheet.required;

import static org.apache.commons.lang3.StringUtils.isBlank;
import static org.apache.commons.lang3.StringUtils.isNotBlank;

import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.commons.lang3.StringUtils;
import org.joda.time.DateTime;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.transretail.crm.common.util.SimpleEncrytor;
import com.transretail.crm.core.entity.MemberModel;
import com.transretail.crm.core.entity.enums.MemberCreatedFromType;
import com.transretail.crm.core.entity.lookup.LookupDetail;
import com.transretail.crm.core.security.model.impl.CustomSecurityUserDetailsImpl;
import com.transretail.crm.template.service.streaming.excel.handler.HandlerException;
import com.transretail.crm.template.service.streaming.excel.handler.cell.AccountIdCellHandler;
import com.transretail.crm.template.service.streaming.excel.handler.cell.BirthDateCellHandler;
import com.transretail.crm.template.service.streaming.excel.handler.cell.CellHandler;
import com.transretail.crm.template.service.streaming.excel.handler.cell.EmailCellHandler;
import com.transretail.crm.template.service.streaming.excel.handler.cell.FirstNameCellHandler;
import com.transretail.crm.template.service.streaming.excel.handler.cell.GenderCellHandler;
import com.transretail.crm.template.service.streaming.excel.handler.cell.KtpIdCellHandler;
import com.transretail.crm.template.service.streaming.excel.handler.cell.LastNameCellHandler;
import com.transretail.crm.template.service.streaming.excel.handler.cell.MobilePhoneCellHandler;
import com.transretail.crm.template.service.streaming.excel.handler.cell.RegisteredStoreCellHandler;
import com.transretail.crm.template.service.streaming.excel.handler.sheet.AbstractSheetHandler;

/**
 * @author Allan G. Ramirez (agramirez@exist.com)
 */
public class NewIndInfoHandler extends AbstractSheetHandler {
    private static final String HEADER_ACCOUNTID = "ACCOUNTNUMBER";
    private static final String HEADER_PREFSTORE = "PREFERREDSTORE";
    private static final String HEADER_FIRSTNAME = "FIRSTNAME";
    private static final String HEADER_LASTNAME = "LASTNAME";
    private static final String HEADER_GENDER = "GENDER";
    private static final String HEADER_BIRTHDATE = "DATEOFBIRTH";
    private static final String HEADER_MOBILEPHONE = "MOBILEPHONE";
    private static final String HEADER_EMAIL = "EMAIL";
    private static final String HEADER_KTPID = "KTPID";

    @Resource(name = "passwordEncoder")
    private PasswordEncoder passwordEncoder;

    private Map<String, CellHandler> cellHandlerClassMap = Maps.newHashMap();
    private CellHandler[] cellHandlers = null;
    private DateTime currentDateTime;
    private CustomSecurityUserDetailsImpl currentUser;

    public NewIndInfoHandler(DateTime currentDateTime, CustomSecurityUserDetailsImpl currentUser) {
        this.currentDateTime = currentDateTime;
        this.currentUser = currentUser;

        cellHandlerClassMap.put(HEADER_ACCOUNTID, new AccountIdCellHandler());
        cellHandlerClassMap.put(HEADER_FIRSTNAME, new FirstNameCellHandler());
        cellHandlerClassMap.put(HEADER_LASTNAME, new LastNameCellHandler());
        cellHandlerClassMap.put(HEADER_PREFSTORE, new RegisteredStoreCellHandler());
        cellHandlerClassMap.put(HEADER_BIRTHDATE, new BirthDateCellHandler(this));
        cellHandlerClassMap.put(HEADER_GENDER, new GenderCellHandler(this));
        cellHandlerClassMap.put(HEADER_MOBILEPHONE, new MobilePhoneCellHandler());
        cellHandlerClassMap.put(HEADER_EMAIL, new EmailCellHandler());
        cellHandlerClassMap.put(HEADER_KTPID, new KtpIdCellHandler());
        cellHandlers = new CellHandler[cellHandlerClassMap.size()];
    }

    @Override
    protected void processHeaderCell(int i, String headerName) {
        CellHandler cellHandler = cellHandlerClassMap.get(headerName);
        if (cellHandler == null) {
            throw new HandlerException(getMessageSource()
                .getMessage("member.template.messages.excel.nohandler", new String[]{sheetName, headerName},
                    LocaleContextHolder.getLocale()));
        }
        cellHandlers[i] = cellHandler;
    }

    @Override
    protected CellHandler getCellHandler(int index) {
        return cellHandlers[index];
    }

    @Override
    protected void saveOrUpdateDto(MemberModel dto) throws HandlerException {
        Locale locale = LocaleContextHolder.getLocale();
        List<String> errors = Lists.newArrayList();

        Date birthDate = dto.getCustomerProfile().getBirthdate();
        String accountId = dto.getAccountId();
        if (isBlank(accountId)) {
            errors.add(getMessageSource().getMessage("member.template.messages.excel.columns.required",
                new String[]{sheetName, String.valueOf(currentRowNum),
                    StringUtils.join(new String[]{HEADER_ACCOUNTID}, ", ")},
                locale
            ));
        } else {
            if (accountId.length() != 13 || accountId.charAt(2) != '0' || accountId.charAt(3) != '1') {
                throw new HandlerException(getMessageSource().getMessage("member.template.messages.excel.accountid.invalid",
                    new String[]{sheetName, accountId, "01"},
                    LocaleContextHolder.getLocale()));
            } else if (accountIdExists(dto.getAccountId())) {
                errors.add(getMessageSource().getMessage("member.template.messages.excel.accountid.exists",
                    new String[]{sheetName, dto.getAccountId()}, locale));
            }
            if (contactExists(dto.getContact())) {
                errors.add(getMessageSource().getMessage("member.template.messages.excel.mobile.exists",
                    new String[]{sheetName, dto.getContact()}, locale));
            }
        }
        if (isBlank(dto.getContact())) {
            errors.add(getMessageSource().getMessage("member.template.messages.excel.columns.required",
                new String[]{sheetName, String.valueOf(currentRowNum),
                    StringUtils.join(new String[]{HEADER_MOBILEPHONE}, ", ")},
                locale
            ));
        }
        if (birthDate == null) {
            errors.add(getMessageSource().getMessage("member.template.messages.excel.columns.required",
                new String[]{sheetName, String.valueOf(currentRowNum),
                    StringUtils.join(new String[]{HEADER_BIRTHDATE}, ", ")},
                locale
            ));
        }
        if (isBlank(dto.getRegisteredStore())) {
            errors.add(getMessageSource().getMessage("member.template.messages.excel.columns.required",
                new String[]{sheetName, String.valueOf(currentRowNum),
                    StringUtils.join(new String[]{HEADER_PREFSTORE}, ", ")},
                locale
            ));
        }
// Task #88915
//        if (usernameExists(dto.getUsername())) {
//            errors.add(getMessageSource().getMessage("member.template.messages.excel.username.exists",
//                new String[]{sheetName, dto.getUsername()}, locale));
//        }
        if (isNotBlank(dto.getEmail())) {
            if (!validEmail(dto.getEmail())) {
                errors.add(getMessageSource()
                    .getMessage("member.template.messages.excel.email.invalid", new String[]{sheetName, dto.getEmail()}, locale));
            } else if (emailExists(dto.getEmail())) {
                errors.add(getMessageSource().getMessage("member.template.messages.excel.email.exists",
                    new String[]{sheetName, dto.getEmail()}, locale));
            }
        }

        if (isNotBlank(dto.getIdNumber()) && idNumberExists(dto.getIdNumber())) {
            errors.add(getMessageSource().getMessage("member.template.messages.excel.ktid.exists",
                new String[]{sheetName, dto.getIdNumber().substring("KTP-".length())}, locale));
        }

        if (errors.size() > 0) {
            throw new HandlerException(StringUtils.join(errors, "\r\n"));
        }

        // Task #88915
        dto.setUsername(dto.getContact());

        // Task #88901
        dto.setRegisteredStore(currentUser.getStoreCode());

        // Task #84568 / Task #88915
        String formattedBirthDate = BIRTHDATE_PASSWORD_FORMAT.format(birthDate);
        dto.setPassword(passwordEncoder.encode(formattedBirthDate));
        if (StringUtils.isBlank(dto.getPin())) {
            // Bug #85179
            try {
                dto.setPin(SimpleEncrytor.getInstance().encrypt(BIRTHDATE_PIN_FORMAT.format(birthDate)));
                dto.setPinEncryped(true);
            } catch (SimpleEncrytor.EncryptDecryptException e) {
                throw new HandlerException(e);
            }
        }

        dto.setCreated(currentDateTime);
        dto.setCreateUser(currentUser.getUsername());
        dto.setMemberType(new LookupDetail(getCodePropertiesService().getDetailMemberTypeIndividual()));
        dto.setMemberCreatedFromType(MemberCreatedFromType.FROM_UPLOAD);
        dto.setEnabled(true);

        em.persist(dto);

        if (currentRowNum % batchSize == 0) {
            cleanup();
        }
    }

    @Override
    public int getExpectedNumberOfColumns() {
        return cellHandlers.length;
    }
}
