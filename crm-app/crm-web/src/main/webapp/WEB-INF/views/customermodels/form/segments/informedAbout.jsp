<%@ include file="../../../common/taglibs.jsp" %>


  <!-- Informed About -->
	<fieldset class="form-horizontal container-fluid">
	  <legend><spring:message code="label_member_indi_informedabout" /></legend>

	  <c:forEach items="${informedAbout}" var="item">
	  <!--<label for="marketingDetails.informedAbout" class="control-label"></label>-->
	  <div class="checkbox controls">
	    <input type="checkbox" data-text="${item.description}" name="marketingDetails.informedAbout" value="${item.code}" />
      <label class="">${item.description}
        <c:if test="${item.description == 'OTHERS'}">&nbsp;&nbsp;
        <input type="text" name="marketingDetails.informedAbout" class="informedAboutTxt" disabled="disabled" />
        </c:if>
      </label>
	  </div>
	  </c:forEach>
	</fieldset>


<style> input.form-control-2 { margin-top: 10px !important; } </style>