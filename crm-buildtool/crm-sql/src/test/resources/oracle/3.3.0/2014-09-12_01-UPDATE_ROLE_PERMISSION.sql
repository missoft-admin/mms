--Loyalty Card Section
UPDATE CRM_USER_PERMISSION SET ORDERING=5001 WHERE CODE='LC_BURN_CARDS';
UPDATE CRM_USER_PERMISSION SET ORDERING=5002 WHERE CODE='LC_D2D_TRACKING';
UPDATE CRM_USER_PERMISSION SET ORDERING=5003 WHERE CODE='LC_INV';
UPDATE CRM_USER_PERMISSION SET ORDERING=5004 WHERE CODE='LC_MO';
UPDATE CRM_USER_PERMISSION SET ORDERING=5005 WHERE CODE='LC_PHYSL_COUNT';
--Gift Card Prodcurement Section
UPDATE CRM_USER_PERMISSION SET ORDERING=5006 WHERE CODE='GC_PROD_PROF';
UPDATE CRM_USER_PERMISSION SET ORDERING=5007 WHERE CODE='SAFETY_STOCK';
UPDATE CRM_USER_PERMISSION SET ORDERING=5008 WHERE CODE='GC_MO';
--Gift Card Inventory Section
UPDATE CRM_USER_PERMISSION SET ORDERING=5009 WHERE CODE='GC_INV_STOCK';
UPDATE CRM_USER_PERMISSION SET ORDERING=5010 WHERE CODE='STOCK_REQUEST_MANAGEMENT';
UPDATE CRM_USER_PERMISSION SET ORDERING=5011 WHERE CODE='GC_ADJUST_INVENTORY';
UPDATE CRM_USER_PERMISSION SET ORDERING=5012 WHERE CODE='GC_BURN_CARD';
UPDATE CRM_USER_PERMISSION SET ORDERING=5013 WHERE CODE='GC_EXPIRY_DATE_EXTENSION';
UPDATE CRM_USER_PERMISSION SET ORDERING=5014 WHERE CODE='CLAIMS_TO_AFFILIATES_SCHEDULE';
--Gift Card Sales Section
UPDATE CRM_USER_PERMISSION SET ORDERING=5015 WHERE CODE='GC_SALES_ORDER';
--Gift Card Customer Service Section
UPDATE CRM_USER_PERMISSION SET ORDERING=5016 WHERE CODE='GC_RETURN';
UPDATE CRM_USER_PERMISSION SET ORDERING=5017 WHERE CODE='GC_REFUND';
UPDATE CRM_USER_PERMISSION SET ORDERING=5018 WHERE CODE='GC_REPLACE';
UPDATE CRM_USER_PERMISSION SET ORDERING=5019 WHERE CODE='POS_LOSE_MANAGEMENT';
--Supervisor / Approver
UPDATE CRM_USER_PERMISSION SET ORDERING=5020 WHERE CODE='LC_APPROVE_BURN_CARDS';