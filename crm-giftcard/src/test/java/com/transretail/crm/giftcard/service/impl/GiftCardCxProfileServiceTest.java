package com.transretail.crm.giftcard.service.impl;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.transretail.crm.common.service.dto.request.PagingParam;
import com.transretail.crm.giftcard.dto.GiftCardCxProfileDto;
import com.transretail.crm.giftcard.dto.GiftCardCxProfileSearchDto;
import com.transretail.crm.giftcard.repo.GiftCardCustomerProfileRepo;
import com.transretail.crm.giftcard.service.GiftCardCxProfileService;

/**
 *
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath*:/META-INF/spring/applicationContext*.xml", "classpath:META-INF/spring/applicationContext-test.xml"})
public class GiftCardCxProfileServiceTest {

    @Autowired
    private GiftCardCustomerProfileRepo repo;
    @Autowired
    private GiftCardCxProfileService service;



    private static Long SAVED_ID 				= null;
    private static final String SAVED_CXID 		= "CX000";
    private static final String UPDATED_CXNAME 	= "CXNAME_UPDATED";
    private GiftCardCxProfileDto dto = null;



    @Before
    public void onSetup() {
    	dto = new GiftCardCxProfileDto();
    	dto.setCustomerId( SAVED_CXID );
    	dto.setName( "CXNAME" );
    	dto.setInvoiceTitle( "INV000" );
    }

    @After
    public void tearDown() {
    	repo.deleteAll();
    }

    @Test
    public void testSaveGetUpdate() {
    	Assert.assertEquals( false, null != dto.getId() );
    	dto = service.saveDto( dto );
    	Assert.assertEquals( true, null != dto.getId() );

    	SAVED_ID = dto.getId();
    	GiftCardCxProfileDto dtoTest = service.getDto( SAVED_ID );
    	Assert.assertEquals( true, dtoTest.getCustomerId().equalsIgnoreCase( SAVED_CXID ) );

    	dtoTest.setName( UPDATED_CXNAME );
    	dtoTest = service.updateDto( dtoTest );
    	Assert.assertEquals( true, dtoTest.getName().equalsIgnoreCase( UPDATED_CXNAME ) );
    }

    @Test
    public void testSearch() {
    	GiftCardCxProfileSearchDto searchDto = new GiftCardCxProfileSearchDto();
    	PagingParam pp = new PagingParam();
    	pp.setPageSize( -1 );
    	searchDto.setPagination( pp );
    	Assert.assertEquals( 0, service.search( searchDto ).getNumberOfElements() );

    	dto = service.saveDto( dto );
    	Assert.assertEquals( 1, service.search( searchDto ).getNumberOfElements() );

    	searchDto.setName( UPDATED_CXNAME );
    	Assert.assertEquals( 0, service.search( searchDto ).getNumberOfElements() );
    }

}
