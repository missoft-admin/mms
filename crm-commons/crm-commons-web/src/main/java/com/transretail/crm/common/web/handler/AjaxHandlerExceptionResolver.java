package com.transretail.crm.common.web.handler;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.MessageSource;
import org.springframework.context.MessageSourceAware;
import org.springframework.http.HttpStatus;
import org.springframework.web.client.HttpStatusCodeException;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.handler.AbstractHandlerExceptionResolver;
import org.springframework.web.servlet.support.RequestContextUtils;

import com.fasterxml.jackson.databind.exc.InvalidFormatException;
import com.transretail.crm.common.web.controller.support.RequestUtils;

/**
 *
 */
public class AjaxHandlerExceptionResolver extends AbstractHandlerExceptionResolver implements MessageSourceAware {
    private static final Logger _LOG = LoggerFactory.getLogger(AjaxHandlerExceptionResolver.class);
    private MessageSource messageSource;

    @Override
    public void setMessageSource(MessageSource messageSource) {
        this.messageSource = messageSource;
    }

    @Override
    protected ModelAndView doResolveException(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex) {
        if (RequestUtils.INSTANCE.isAjax(request)) {
            _LOG.error("Handling ajax request exception.", ex);
            String message = null;
            int statusCode = 0;
            if (HttpStatusCodeException.class.isAssignableFrom(ex.getClass())) {
                message = ((HttpStatusCodeException) ex).getResponseBodyAsString();
                statusCode = HttpStatus.INTERNAL_SERVER_ERROR.value();
            } else {
                Throwable root = ex;
                while (root.getCause() != null) {
                    root = root.getCause();
                }
                if (root instanceof InvalidFormatException) {
                    InvalidFormatException invalidFormatException = (InvalidFormatException) root;
                    Class type = invalidFormatException.getTargetType();
                    Object value = invalidFormatException.getValue();
                    message = messageSource.getMessage(InvalidFormatException.class.getName(), new Object[]{value, type},
                        invalidFormatException.getMessage(), RequestContextUtils.getLocale(request));
                    statusCode = HttpStatus.BAD_REQUEST.value();
                } else {
                    message = root.getMessage();
                    statusCode = HttpStatus.INTERNAL_SERVER_ERROR.value();
                }
            }
            response.setStatus(statusCode, message);
            return new ModelAndView();
        }
        return null;
    }

}
