<%@ include file="../../../common/taglibs.jsp" %>


	<fieldset class="form-horizontal container-fluid professionalDetails">
	  <legend>Company Profile</legend>
	
	  <div class="control-group">
	      <spring:message code="member_prop_companystore" var="storeName" />
	      <label for="storeName" class="control-label"> <c:out value="${storeName}:" /></label> 
	      <div class="controls">
	        <form:input path="storeName" id="storeName" class="form-control" placeholder="${storeName}" />
	      </div>
	  </div>
	  <div class="control-group">
	      <spring:message code="label_com_transretail_crm_entity_customermodel_zone" var="zone" />
	      <label for="zone" class="control-label"><b class="required">*</b> <c:out value="${zone}:" /></label> 
	      <div class="controls">
	        <form:input path="professionalProfile.zone" cssClass="form-control" placeholder="${zone}" />
	      </div>
	  </div>
	  <div class="control-group">
	      <spring:message
	          code="label_com_transretail_crm_entity_customermodel_radius" var="radius" />
	      <label for="radius" class="control-label"><b class="required">*</b> <c:out value="${radius}:" />
	      </label> 
	      <div class="controls">
	        <form:input path="professionalProfile.radius" id="radius" cssClass="form-control" placeholder="${radius}" />
	      </div>
	  </div>
	</fieldset>