<%@ include file="../taglibs.jsp" %>
<div class="page-header page-header2">
    <h1><spring:message code="points.label" /></h1>
</div>
<div class="well well-blue">
	<c:choose>
	<c:when test="${isSupplementary}">
		<h3><spring:message code="points.transaction.contributedpoints"/>&nbsp;=&nbsp;${totalPoints}</h3>
	</c:when>
	<c:otherwise>
		<h3><spring:message code="label_total_points"/>&nbsp;=&nbsp;${totalPoints}</h3>
	</c:otherwise>
	</c:choose>
</div>

<c:if test="${!isSupplementary}">
<div id="supplementFilter" class="well">
	<div class="form-inline form-search">
		<label style="margin-right:10px"><h5><spring:message code="label_filter_supplements" /></h5></label>
		<select name="contributerId" multiple="multiple" style="margin-right:10px">
			<option value=""></option>
			<c:forEach items="${supplements}" var="supplement">
				<option value="${supplement.accountId}">${supplement.formattedMemberName}</option>
			</c:forEach>
		</select>
		<input type="button" class="btn btn-primary" value="<spring:message code="search" />" id="filterButton">
	</div>
</div>
</c:if>

<div id="points-list">

</div>
<link href="<spring:url value="/css/dataTables.css" />" rel="stylesheet">
<script src="<spring:url value="/js/jquery/jquery.dataTables.js" />"></script>
<script src="<spring:url value="/js/jquery/jquery.dataTables.linkLengthMenu.js" />"></script>
<script src="<spring:url value="/js/jquery/jquery.dataTables.customPaging.js" />"></script>
<script src="<spring:url value="/js/jquery/ajax-datatable.js" />"></script>
<script src="<spring:url value="/js/common/form2js.js" />"></script>
<script src="<spring:url value="/js/common/json2.js" />"></script>
<script src="<spring:url value="/js/jquery/jquery.toObject.js" />"></script>
<script src="<spring:url value="/js/dateutil.js" />"></script>

<div id="transactionItemDialog" class="modal hide  nofly spanX">
  <div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
    <h4><spring:message code="points.transaction.items.header"/></h4>
  </div>
  <div class="modal-body">
    <p>

    </p>
  </div>
  <div class="modal-footer">
    <button class="btn" data-dismiss="modal" aria-hidden="true"><spring:message code="button_close"/></button>
  </div>
</div>
<spring:message code="points.transaction.viewitems" var="viewItemsLabel"/>
<script>
  var isSupplementary = "${isSupplementary}";
  
  $( document ).ready( function () {
	var columnHeaders = [
       '<spring:message code="points.transaction.location" />',
       '<spring:message code="points.transaction.type" />',
       '<spring:message code="points.transaction.created" />',
       '<spring:message code="points.transaction.datetime" />',
       '<spring:message code="points.transaction.number" />',
       '<spring:message code="points.transaction.amount" />',
       '<spring:message code="points.transaction.media" />',
       '<spring:message code="points.label" />'
     ];

    var modelFields = [
		  {name : 'storeCode', sortable: true},
		  {name : 'transactionType', sortable : true},
		  {name : 'created', sortable : true, dataType: "DATE", fieldCellRenderer : function (data, type, row) {
		      return ( (row.longCreated)? new Date( row.longCreated ).customize(0) : "" );
		  }},
		  {name : 'transactionDateTime', sortable : true, dataType: "DATE",  fieldCellRenderer : function (data, type, row) {
		      return ( (row.longTxnDate)? new Date( row.longTxnDate ).customize(0) : "" );
		  }},
		  {name : 'transactionNo', sortable : true},
		  {name : 'transactionAmount', sortable : true},
		  { name  : 'transactionMedia', sortable : true, fieldCellRenderer : function (data, type, row) {
		      if(row.transactionMedia != null && row.transactionMedia != "")
		          return row.transactionMedia + " - " + row.transactionMediaDesc;
		        else
		          return "";
		   }},
		  {name : 'transactionPoints', sortable : true}
		];

	if(isSupplementary == 'false') {
		columnHeaders[columnHeaders.length] = '<spring:message code="points.transaction.contributer" />';
		modelFields[modelFields.length] = {name: 'transactionContributer', sortable: true, 
				fieldCellRenderer: function(data, type, row) {
					if(row.transactionContributer != null && row.transactionContributerId != row.memberModelId) {
						return row.transactionContributer;
					}
					else {
						return "";
					}
				}};
	}

	columnHeaders[columnHeaders.length] = '';
	modelFields[modelFields.length] = {
		    customCell : function ( innerData, sSpecific, json ) {
			      if ( sSpecific == 'display' && json.transactionType != 'REDEEM') {
			        return '<a class="transaction-' + json.transactionNo + ' icn-view tiptip btn" href="#" onclick="return false;" title="${viewItemsLabel}">${viewItemsLabel}</a>';
			      } else {
			        return '';
			      }
			    }
			  };
	
	  
    var $pointsList = $( '#points-list' ).ajaxDataTable( {
      'autoload' : true,
      'aaSorting'		: [[ 2, "desc" ]],
      'ajaxSource' : '<spring:url value="/member/points" />',
      'columnHeaders' : columnHeaders,
      'modelFields' : modelFields
    } );

    var $transactionItemDialog = $( '#transactionItemDialog' ).modal( { show : false } ).draggable();

    var $transactionItemList = $( '.modal-body > p', $transactionItemDialog ).ajaxDataTable( {
      'autoload' : false,
      'ajaxSource' : '<spring:url value="/postransaction/item/list" />',
      'columnHeaders' : [
        /* '<spring:message code="points.transaction.items.itemcode" />', */
        '<spring:message code="points.transaction.items.pluid" />',
        '<spring:message code="points.transaction.items.productDesc" />',
        '<spring:message code="points.transaction.items.productPrice" />',
        '<spring:message code="points.transaction.items.quantity" />',
        '<spring:message code="points.transaction.items.totalPrice" />',
        '<spring:message code="points.transaction.items.salesType" />'
      ],
      'modelFields' : [
        {name : 'pluId', sortable : false},
        {name : 'itemDesc', sortable : false},
        {name : 'itemPrice', sortable : false},
        {name : 'quantity', sortable : false},
        {name : 'totalPrice', sortable : false},
        {name : 'salesType', sortable : false}
      ]
    } );

    $( $pointsList ).on( 'click', '[class^="transaction-"]', function () {
      var transactionNo = this.className.substring( 'transaction-'.length );
      var formDataObject = new Object();
      formDataObject.posTransactionNo = transactionNo;
      $transactionItemList.ajaxDataTable( 'search', formDataObject );
      $transactionItemDialog.modal( 'toggle' );
    } );

    $("#filterButton").click(function() {
        $pointsList.ajaxDataTable('search', $("#supplementFilter").toObject({ mode : 'first', skipEmpty : true }));
    });
  } 
  );
</script>
<style>
    .spanX { width: 60%; } 
    
    .dataTables_wrapper {overflow: auto; max-width: 1280px}
    .well {
        color: #666666;
        width: auto;
    }
    .table tbody tr td:nth-of-type(1), .table tbody tr th:nth-of-type(1) {
      min-width: 230px;
    }
    .table tbody tr td:nth-of-type(4), .table tbody tr th:nth-of-type(4) {
      min-width: 180px;
    }
</style>