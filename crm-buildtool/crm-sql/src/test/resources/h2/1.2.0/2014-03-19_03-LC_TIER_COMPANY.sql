ALTER TABLE CRM_MO_TIER ADD COLUMN COMPANY VARCHAR(255);

alter table CRM_MO_TIER 
    add constraint FK_ownbrl6utr8sphqc2911jy5bp 
    foreign key (COMPANY) 
    references CRM_REF_LOOKUP_DTL;