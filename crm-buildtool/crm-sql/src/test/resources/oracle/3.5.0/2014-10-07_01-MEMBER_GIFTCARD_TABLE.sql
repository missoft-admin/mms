
create table CRM_MEMBER_GIFT_CARD (
       ACCOUNT_ID varchar2(255 char),
       REG_DATE timestamp,
       MEMBER_ID number(19,0) not null,
       GC_ID number(19,0) not null,
       primary key (GC_ID)
   );

alter table CRM_MEMBER_GIFT_CARD 
     add constraint FK_isyuxpnxoalwb3jtyxnefwhyv 
     foreign key (MEMBER_ID) 
     references CRM_MEMBER;

 alter table CRM_MEMBER_GIFT_CARD 
     add constraint FK_ahtfv2mdt3b16l8jk0392d7nv 
     foreign key (GC_ID) 
     references CRM_GC_INVENTORY;