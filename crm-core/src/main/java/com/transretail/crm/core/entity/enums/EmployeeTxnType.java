package com.transretail.crm.core.entity.enums;



public enum EmployeeTxnType {

    PURCHASE, ADJUST, CREATE, UPDATESTATUS, REDEEM_VOUCHER, VOID_VOUCHERTXN
}
