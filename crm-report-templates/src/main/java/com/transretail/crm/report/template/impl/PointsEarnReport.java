package com.transretail.crm.report.template.impl;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.Collections;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;

import org.apache.commons.lang3.StringUtils;
import org.joda.time.LocalDate;
import org.joda.time.LocalDateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.MessageSourceAware;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.context.support.MessageSourceAccessor;
import org.springframework.stereotype.Service;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.google.common.collect.Sets;
import com.mysema.query.BooleanBuilder;
import com.mysema.query.jpa.impl.JPAQuery;
import com.mysema.query.types.Projections;
import com.mysema.query.types.QBean;
import com.transretail.crm.common.reporting.engine.data.JRQueryDSLDataSource;
import com.transretail.crm.common.reporting.engine.data.ReportDataSource;
import com.transretail.crm.common.reporting.jasper.ClasspathFileResolver;
import com.transretail.crm.common.reporting.jasper.DefaultJRProcessor;
import com.transretail.crm.common.reporting.jasper.JRProcessor;
import com.transretail.crm.common.util.DateUtil;
import com.transretail.crm.core.entity.QPointsTxnModel;
import com.transretail.crm.core.entity.enums.PointTxnType;
import com.transretail.crm.core.entity.enums.ReportType;
import com.transretail.crm.core.entity.lookup.LookupDetail;
import com.transretail.crm.core.entity.lookup.QLookupDetail;
import com.transretail.crm.core.entity.lookup.Store;
import com.transretail.crm.core.repo.LookupDetailRepo;
import com.transretail.crm.core.service.CodePropertiesService;
import com.transretail.crm.core.service.StoreService;
import com.transretail.crm.core.util.BooleanExprUtil;
import com.transretail.crm.report.template.CommonReportFilter;
import com.transretail.crm.report.template.FilterField;
import com.transretail.crm.report.template.ReportDateFormatProvider;
import com.transretail.crm.report.template.ValidatableReportTemplate;

@Service("pointsEarnReport")
public class PointsEarnReport implements ValidatableReportTemplate, MessageSourceAware, ReportDateFormatProvider {

    private MessageSourceAccessor messageSource;
    protected static final String FILTER_TX_STORE = "txStore";
    protected static final String FILTER_MEMBER_TYPE = "memberType";
    private static final String PERMISSION_KEY = "REPORT_MEMBER_STORE_TRANSACTION_VIEW";
    static final String TEMPLATE_NAME = "Earn Point Report By Customer Level";
    static final String REPORT_NAME = "reports/earn-point-report.jasper";

    @Autowired
    private StoreService storeService;
    @Autowired
    private CodePropertiesService codePropertiesService;
    @Autowired
    private LookupDetailRepo lookupDetailRepo;
    @PersistenceContext
    private EntityManager em;

    @Override
    public void setMessageSource(MessageSource messageSource) {
        this.messageSource = new MessageSourceAccessor(messageSource);
    }

    @Override
    public DateTimeFormatter getDefaultDateFormat() {
        return DateTimeFormat.forPattern(DateUtil.SEARCH_DATE_FORMAT);
    }

    @Override
    public List<String> validate(Map<String, String> parameters) {
        final Locale locale = LocaleContextHolder.getLocale();
        List<String> errors = Lists.newArrayList();
        final String dateFrom = parameters.get(CommonReportFilter.DATE_FROM);
        final String dateTo = parameters.get(CommonReportFilter.DATE_TO);
        if (StringUtils.isBlank(dateFrom) || StringUtils.isBlank(dateTo)) {
            errors.add(messageSource.getMessage("required_date_fields",  locale));
        } else {
            LocalDate df = LocalDate.parse(dateFrom, getDefaultDateFormat());
            LocalDate dt = LocalDate.parse(dateTo, getDefaultDateFormat());

            if (df.isAfter(dt)) {
                errors.add(messageSource.getMessage("report.date.from.invalid.message", locale));
            }

            if (dt.isBefore(df)) {
                errors.add(messageSource.getMessage("report.date.to.invalid.message", locale));
            }
        }
        
//        String memberType = parameters.get(FILTER_MEMBER_TYPE);
//        if (StringUtils.isBlank(memberType)) {
//            errors.add(messageSource.getMessage("required_member_type_fields",  locale));
//        }

        String ratePoint = parameters.get("ratePoint");
        try {
            if (StringUtils.isNotBlank(ratePoint)) {
                Double a = Double.valueOf(ratePoint);
            }
        } catch (NumberFormatException e) {
            errors.add(messageSource.getMessage("rate.point.invalid",  locale));
        }

        return errors;
    }

    public ReportDataSource createDataSource(Map<String, String> parameters) {
        String transactionLocation = parameters.get(FILTER_TX_STORE);
        String memberType = parameters.get(FILTER_MEMBER_TYPE);
        String tmpDateFrom = parameters.get(CommonReportFilter.DATE_FROM);
        String tmpDateTo = parameters.get(CommonReportFilter.DATE_TO);
        String ratePoint = parameters.get("ratePoint");

        LocalDate txDateFrom = LocalDate.parse(tmpDateFrom, getDefaultDateFormat());
        LocalDate txDateTo = LocalDate.parse(tmpDateTo, getDefaultDateFormat());
        LocalDateTime from = txDateFrom.toDateTimeAtCurrentTime().toLocalDateTime();
        LocalDateTime to = txDateTo.toDateTimeAtCurrentTime().toLocalDateTime();

        from = from.millisOfDay().withMinimumValue();
        to = to.millisOfDay().withMaximumValue();

        QPointsTxnModel ptm = QPointsTxnModel.pointsTxnModel;

        BooleanBuilder booleanBuilder = new BooleanBuilder().
            and(ptm.transactionType.eq(PointTxnType.EARN)).
            and(ptm.transactionDateTime.between(from.toDate(), to.toDate()));

        if (StringUtils.isNotBlank(ratePoint)) {
            booleanBuilder.and(ptm.transactionPoints.eq(Double.valueOf(ratePoint)));
        }

        if (StringUtils.isNotBlank(memberType)) {
            booleanBuilder.and(ptm.memberModel.memberType.code.eq(memberType));
        }

        if (StringUtils.isNotBlank(transactionLocation)) {
            booleanBuilder.and(ptm.storeCode.eq(transactionLocation));
        }

        JPAQuery query = new JPAQuery(em).from(ptm).where(booleanBuilder);

        QBean<ReportBean> projections = Projections.bean(ReportBean.class,
            ptm.id.as("id"),
            ptm.storeCode.as("storeCode"),
            ptm.storeCode.as("store"),
            ptm.memberModel.accountId.as("accountId"),
            ptm.memberModel.lastName.concat(",").concat(ptm.memberModel.firstName).as("name"),
            ptm.transactionPoints.as("transactionPoints"),
            ptm.transactionAmount.as("transactionAmount"));

        return new JRQueryDSLDataSource(query, projections,
            new JRQueryDSLDataSource.FieldValueCustomizer() {
                private final Map<String, String> storeCacheMap = Maps.newHashMap();
                private final Map<String, String> pStoreCacheMap = Maps.newHashMap();

                @Override
                public Object customize(String propertyName, Object value) {
                    final String fieldValue = value != null ? value.toString() : "";
                    if(value != null) {
                        if ("store".equals(propertyName)) {
                            String storePresentation = storeCacheMap.get(fieldValue);
                            if (storePresentation != null) {
                                return storePresentation;
                            } else {
                                final Store store = storeService.getStoreByCode(fieldValue);
                                storePresentation = store == null ? fieldValue : store.getCodeAndName();
                                storeCacheMap.put(fieldValue, storePresentation);
                                return storePresentation;
                            }
                        }
                    }

                    return value;
                }
            });
    }

    @Override
    public String getName() {
        return TEMPLATE_NAME;
    }

    @Override
    public String getDescription() {
        return messageSource.getMessage("earn.point.report.title");
    }

    @Override
    public Set<FilterField> getFilters() {
        Set<FilterField> filters = Sets.newLinkedHashSetWithExpectedSize(3);
        String filterTxStore = messageSource.getMessage("earn.point.report.filter.tx.store");
        String filterMemberType = messageSource.getMessage("earn.point.report.filter.member.type");
        String filterSalesDateFrom = messageSource.getMessage("earn.point.report.filter.date.from");
        String filterSalesDateTo = messageSource.getMessage("earn.point.report.filter.date.to");
        String filterRate = messageSource.getMessage("earn.point.report.filter.rate");

        filters.add(FilterField.createDropdownField(FILTER_TX_STORE, filterTxStore, getStoresOption()));
        filters.add(FilterField.createDropdownField(FILTER_MEMBER_TYPE, filterMemberType, getMemberTypeOption()));
        filters.add(FilterField.createDateField(CommonReportFilter.DATE_FROM, filterSalesDateFrom, filterSalesDateFrom, true) );
        filters.add(FilterField.createDateField(CommonReportFilter.DATE_TO, filterSalesDateTo, filterSalesDateTo, true));
        filters.add(FilterField.createInputField("ratePoint", filterRate));

        return Collections.unmodifiableSet(filters);
    }

    @Override
    public boolean canView(Set<String> permissions) {
        return true; //permissions.contains(PERMISSION_KEY);
    }

    @Override
    public JRProcessor createJrProcessor(Map<String, String> map) {
        String selectedTxStore = map.get(FILTER_TX_STORE);
        String memberType = map.get(FILTER_MEMBER_TYPE);
        String tmpDateFrom = map.get(CommonReportFilter.DATE_FROM);
        String tmpDateTo = map.get(CommonReportFilter.DATE_TO);

        DefaultJRProcessor defaultJRProcessor = new DefaultJRProcessor(REPORT_NAME);
        Store store = storeService.getStoreByCode(selectedTxStore);
        defaultJRProcessor.addParameter("TRANSACTION_LOCATION", store != null ? store.getCodeAndName() : "All");
        defaultJRProcessor.addParameter("SALES_DATE_FROM", tmpDateFrom);
        defaultJRProcessor.addParameter("SALES_DATE_TO", tmpDateTo);
        defaultJRProcessor.addParameter("MEMBER_TYPE", memberType);
        defaultJRProcessor.addParameter("SUB_DATASOURCE", createDataSource(map));
        defaultJRProcessor.setFileResolver(new ClasspathFileResolver("reports"));

        return defaultJRProcessor;
    }

    @Override
    public ReportType[] getReportTypes() {
        return new ReportType[]{ReportType.EXCEL};
    }

    private Map<String, String> getMemberTypeOption() {
        String headerMemberType = codePropertiesService.getHeaderMemberType();
        QLookupDetail q = QLookupDetail.lookupDetail;
        Iterable<LookupDetail> result = lookupDetailRepo.findAll(q.header.code.eq(headerMemberType));
        Map<String, String> options = Maps.newLinkedHashMap();

        for (LookupDetail lookupDetail : result) {
            options.put(lookupDetail.getCode(), lookupDetail.getDescription());
        }

        return Collections.unmodifiableMap(options);
    }

    private Map<String, String> getStoresOption() {
        return storeService.getAllStoresHashMap();
    }

    public static class ReportBean {

        private String id;
        private String storeCode;
        private String store;
        private String accountId;
        private String name;
        private Double transactionAmount;
        private Double transactionPoints;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getStoreCode() {
            return storeCode;
        }

        public void setStoreCode(String storeCode) {
            this.storeCode = storeCode;
        }

        public String getStore() {
            return store;
        }

        public void setStore(String store) {
            this.store = store;
        }

        public String getAccountId() {
            return accountId;
        }

        public void setAccountId(String accountId) {
            this.accountId = accountId;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public Double getTransactionAmount() {
            return transactionAmount;
        }

        public void setTransactionAmount(Double transactionAmount) {
            this.transactionAmount = transactionAmount;
        }

        public Double getTransactionPoints() {
            return transactionPoints;
        }

        public void setTransactionPoints(Double transactionPoints) {
            this.transactionPoints = transactionPoints;
        }

    }
}
