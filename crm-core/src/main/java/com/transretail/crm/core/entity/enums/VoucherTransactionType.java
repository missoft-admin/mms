package com.transretail.crm.core.entity.enums;

public enum VoucherTransactionType {


     ADJUSTED, REDEEM, VOID, PURCHASE, RETURN, EXPIRE, REFUND

}
