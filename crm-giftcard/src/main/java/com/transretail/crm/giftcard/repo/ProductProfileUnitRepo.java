package com.transretail.crm.giftcard.repo;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.transretail.crm.common.repo.CrmQueryDslPredicateExecutor;
import com.transretail.crm.giftcard.entity.ProductProfile;
import com.transretail.crm.giftcard.entity.ProductProfileUnit;

@Repository
public interface ProductProfileUnitRepo extends CrmQueryDslPredicateExecutor<ProductProfileUnit, Long> {

	List<ProductProfileUnit> findByProductProfile(ProductProfile profile);
}
