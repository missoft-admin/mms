    create table CRM_SALES_ORD_ALLOC (
        id number(19,0) not null,
        CREATED_BY varchar2(255 char),
        CREATED_DATETIME timestamp,
        LAST_UPDATED_BY varchar2(255 char),
        LAST_UPDATED_DATETIME timestamp,
        GC_INVENTORY number(19,0),
        SO_ITEM number(19,0),
        primary key (id)
    );



    alter table CRM_SALES_ORD_ALLOC 
        add constraint FK_d37ydiwjjjb05hf02grktf4ia 
        foreign key (GC_INVENTORY) 
        references CRM_GC_INVENTORY;

    alter table CRM_SALES_ORD_ALLOC 
        add constraint FK_kl3r0nn7fyh9fc3h3il6t6gu5 
        foreign key (SO_ITEM) 
        references CRM_SALES_ORD_ITEM;
