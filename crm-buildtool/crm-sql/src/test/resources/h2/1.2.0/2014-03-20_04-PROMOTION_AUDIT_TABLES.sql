create table CRM_PROMOTION_AUD (
        id bigint not null,
        REV bigint not null,
        REVTYPE tinyint,
        AFFECTED_DAYS varchar(255),
        ACCEPT_EMAIL char(1),
        ACCEPT_MAIL char(1),
        ACCEPT_PHONE char(1),
        ACCEPT_SMS char(1),
        SMS char(1),
        DESCRIPTION varchar(255),
        END_DATE timestamp,
        END_TIME time,
        NAME varchar(255),
        START_DATE timestamp,
        START_TIME time,
        CAMPAIGN_ID bigint,
        REWARD_TYPE varchar(15),
        STATUS varchar(15),
        primary key (id, REV)
    );

create table CRM_PROGRAM_AUD (
        id bigint not null,
        REV bigint not null,
        REVTYPE tinyint,
        DESCRIPTION varchar(255),
        endDate timestamp,
        startDate timestamp,
        NAME varchar(255),
        TYPE varchar(20),
        STATUS varchar(15),
        primary key (id, REV)
    );

create table CRM_CAMPAIGN_AUD (
        id bigint not null,
        REV bigint not null,
        REVTYPE tinyint,
        DESCRIPTION varchar(255),
        endDate timestamp,
        startDate timestamp,
        NAME varchar(255),
        PROJECT_ID bigint,
        STATUS varchar(15),
        primary key (id, REV)
    );

create table CRM_MEMBER_GRP_AUD (
        id bigint not null,
        REV bigint not null,
        REVTYPE tinyint,
        CARD_TYPE_GROUP_CODE varchar(255),
        MEMBERS varchar(255),
        NAME varchar(255),
        POINTS_ENABLED char(255),
        POINTS_FROM bigint,
        POINTS_TO bigint,
        DATE_RANGE_FROM date,
        DATE_RANGE_TO date,
        ENABLED char(255),
        FREQUENCY char(255),
        FREQUENCY_FROM bigint,
        FREQUENCY_TO bigint,
        PRODUCT_GROUP bigint,
        RECENCY varchar(255),
        RECENT_VALUE varchar(255),
        SPEND char(255),
        SPEND_FROM bigint,
        SPEND_PER_TRANSACTION char(255),
        SPEND_TO bigint,
        STORE_GROUP bigint,
        primary key (id, REV)
    );

create table CRM_PROMOTION_MEMBER_GRP_AUD (
        id varchar(255) not null,
        REV bigint not null,
        REVTYPE tinyint,
        MEMBER_GRP_ID bigint,
        primary key (id, REV)
    );

create table CRM_PROMOTION_PAYMENTTYPE_AUD (
        id varchar(255) not null,
        REV bigint not null,
        REVTYPE tinyint,
        WILDCARDS varchar(4000),
        PAYMENTTYPE_ID varchar(15),
        primary key (id, REV)
    );

create table CRM_PROMOTION_PRODUCT_GRP_AUD (
        id varchar(255) not null,
        REV bigint not null,
        REVTYPE tinyint,
        PRODUCT_GRP_ID bigint,
        primary key (id, REV)
    );

create table CRM_PROMOTION_REWARD_AUD (
        id bigint not null,
        REV bigint not null,
        REVTYPE tinyint,
        BASE_AMOUNT decimal(19,2),
        BASE_FACTOR double,
        DISCOUNT double,
        FIXED char(1),
        MAX_AMOUNT decimal(19,2),
        MAX_BONUS decimal(19,2),
        MAX_QTY bigint,
        MIN_AMOUNT decimal(19,2),
        MIN_QTY bigint,
        REMARK varchar(1000),
        PROMO_ID bigint,
        REWARD_TYPE varchar(15),
        primary key (id, REV)
    );

create table CRM_PROMOTION_STORE_GRP_AUD (
        id varchar(255) not null,
        REV bigint not null,
        REVTYPE tinyint,
        STORE_GRP_ID bigint,
        primary key (id, REV)
    );



alter table CRM_PROMOTION_AUD 
        add constraint FK_o28sj9ji0y3d0049o3ge62815 
        foreign key (REV) 
        references CRM_REVINFO;

alter table CRM_PROGRAM_AUD 
        add constraint FK_9yi3mdiqs8g2yyhoi4ie3s6r 
        foreign key (REV) 
        references CRM_REVINFO;

alter table CRM_CAMPAIGN_AUD 
        add constraint FK_4wldya65e07un6vroct5m0gkj 
        foreign key (REV) 
        references CRM_REVINFO;

alter table CRM_MEMBER_GRP_AUD 
        add constraint FK_27y5ivj27ysot88u1cwnb7lva 
        foreign key (REV) 
        references CRM_REVINFO;

alter table CRM_PROMOTION_MEMBER_GRP_AUD 
        add constraint FK_d2wwvsxg8xbxu9vpfp17xcmve 
        foreign key (REV) 
        references CRM_REVINFO;

alter table CRM_PROMOTION_PAYMENTTYPE_AUD 
        add constraint FK_tecmsx759ysukekxdwumss9tb 
        foreign key (REV) 
        references CRM_REVINFO;

alter table CRM_PROMOTION_PRODUCT_GRP_AUD 
        add constraint FK_tihcigdoqskeyf9pyoobedr14 
        foreign key (REV) 
        references CRM_REVINFO;

alter table CRM_PROMOTION_REWARD_AUD 
        add constraint FK_qb3r5nr97eybagxtptk0lgkgi 
        foreign key (REV) 
        references CRM_REVINFO;

alter table CRM_PROMOTION_STORE_GRP_AUD 
        add constraint FK_82clahnw50ovvp5heubyunhk0 
        foreign key (REV) 
        references CRM_REVINFO;