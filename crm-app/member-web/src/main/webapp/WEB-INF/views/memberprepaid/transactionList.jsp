<%@ include file="../taglibs.jsp" %>

<div class="page-header page-header2">
    <h1>Transaction History</h1>
</div>
<input type="hidden" id="cardNo" value="<c:out value="${cardNo}"/>"/>
<div class="contentMain well">
    <div class="inline">
        <select id="transactionDuration">
            <option value="30" selected="selected"><spring:message code="gc.prepaid.transaction.history.30"/></option>
            <option value="60"><spring:message code="gc.prepaid.transaction.history.60"/></option>
            <option value="90"><spring:message code="gc.prepaid.transaction.history.90"/></option>
        </select>
    </div>
</div>
<div id="prepaidTransactionList">
    <div id="list_transactions" class="data-table-01"
         data-url="<c:url value="/member/prepaid/transactionHistory" />"
         data-hdr-cardno="<spring:message code="gc.prepaid.transaction.history.cardno"/>"
         data-hdr-date="<spring:message code="gc.prepaid.transaction.history.txndate"/>"
         data-hdr-curramount="<spring:message code="gc.prepaid.transaction.history.curramt"/>"
         data-hdr-txnamount="<spring:message code="gc.prepaid.transaction.history.txnamt"/>"
         data-hdr-prevbal="<spring:message code="gc.prepaid.transaction.history.prevbal"/>"
         data-hdr-txntype="<spring:message code="gc.prepaid.transaction.history.txntype"/>">



        <link href="<spring:url value="/css/dataTables.css" />" rel="stylesheet">
        <script src="<spring:url value="/js/jquery/jquery.dataTables.js" />" ></script>
        <script src="<spring:url value="/js/jquery/jquery.dataTables.linkLengthMenu.js" />"></script>
        <script src="<spring:url value="/js/jquery/jquery.dataTables.customPaging.js" />"></script>
        <script src="<spring:url value="/js/jquery/ajax-datatable.js" />" ></script>
        <script src="<spring:url value="/js/common/form2js.js" />" ></script>
        <script src="<spring:url value="/js/common/json2.js" />" ></script>
        <script src="<spring:url value="/js/jquery/jquery.toObject.js" />" ></script>

        <script src="<spring:url value="/js/viewspecific/prepaid/prepaid.js"/>"></script>
    </div>
</div>

<style type="text/css">

#transactionDuration {
    width: 250px;
    margin-bottom: 0px;
}

</style>