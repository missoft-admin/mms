package com.transretail.crm.report.template.impl;

import java.util.Date;
import java.util.Map;
import java.util.Set;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import net.sf.jasperreports.engine.JRDataSource;

import org.apache.commons.lang3.StringUtils;
import org.joda.time.LocalDate;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Service;

import com.google.common.collect.Maps;
import com.google.common.collect.Sets;
import com.mysema.query.BooleanBuilder;
import com.mysema.query.jpa.JPASubQuery;
import com.mysema.query.jpa.impl.JPAQuery;
import com.mysema.query.support.Expressions;
import com.mysema.query.types.Projections;
import com.mysema.query.types.QBean;
import com.mysema.query.types.expr.CaseBuilder;
import com.mysema.query.types.expr.NumberExpression;
import com.mysema.query.types.path.NumberPath;
import com.transretail.crm.common.reporting.engine.data.JRQueryDSLDataSource;
import com.transretail.crm.common.reporting.jasper.ClasspathFileResolver;
import com.transretail.crm.common.reporting.jasper.DefaultJRProcessor;
import com.transretail.crm.common.reporting.jasper.JRProcessor;
import com.transretail.crm.common.util.DateUtil;
import com.transretail.crm.core.entity.QMemberModel;
import com.transretail.crm.core.entity.QPointsTxnModel;
import com.transretail.crm.core.entity.enums.ReportType;
import com.transretail.crm.core.entity.lookup.QLookupDetail;
import com.transretail.crm.core.entity.lookup.QStore;
import com.transretail.crm.core.security.util.UserUtil;
import com.transretail.crm.report.template.CommonReportFilter;
import com.transretail.crm.report.template.FilterField;
import com.transretail.crm.report.template.NoDataFoundSupport;
import com.transretail.crm.report.template.ReportTemplate;

@Service("noOfChildrenContributionReport")
public class NoOfChildrenContributionReport  implements ReportTemplate, NoDataFoundSupport {
	protected static final String TEMPLATE_NAME = "No of Children Contribution Report";
    protected static final String REPORT_NAME = "reports/children_contribution_report.jasper";
    
    protected static final String FILTER_DATE_TO = CommonReportFilter.DATE_TO;
    protected static final String FILTER_DATE_FROM = CommonReportFilter.DATE_FROM;
    protected static final String FILTER_CUSTOMER_TYPE = "customerType";
    protected static final String FILTER_STORE = "store";
    
    private static final DateTimeFormatter INPUT_DATE_PATTERN = DateTimeFormat.forPattern(DateUtil.SEARCH_DATE_FORMAT);
    
    private static final Logger LOGGER = LoggerFactory.getLogger(NoOfChildrenContributionReport.class);
    
    @PersistenceContext
    private EntityManager em;
    @Autowired
    private MessageSource messageSource;
    
    
    @Override
    public ReportType[] getReportTypes() {
    	return ReportType.values();
    }
    
    @Override
    public String getName() {
    	return TEMPLATE_NAME;
    }

    @Override
    public String getDescription() {
    	return messageSource.getMessage("children.contribution.report.label", (Object[]) null, LocaleContextHolder.getLocale());
    }
    
    @Override
    public boolean canView(Set<String> permissions) {
    	return true;
    }
    
    @Override
    public Set<FilterField> getFilters() {
    	Set<FilterField> filters = Sets.newLinkedHashSet();
        filters.add(FilterField.createDateField(FILTER_DATE_FROM,
                messageSource.getMessage("label_from", (Object[]) null, LocaleContextHolder.getLocale()),
                DateUtil.SEARCH_DATE_FORMAT, true));
        filters.add(FilterField.createDateField(FILTER_DATE_TO,
                messageSource.getMessage("label_to", (Object[]) null, LocaleContextHolder.getLocale()),
                DateUtil.SEARCH_DATE_FORMAT, true));

        QStore store = QStore.store;
        Map<String, String> stores = Maps.newLinkedHashMap();
        stores.put("", "");
        stores.putAll(new JPAQuery(em).from(store).distinct().orderBy(
                store.code.asc()).map(store.code, store.code.concat(" - ").concat(store.name)));

        filters.add(FilterField.createDropdownField(
                FILTER_STORE, messageSource.getMessage("label_store", (Object[]) null, LocaleContextHolder.getLocale()), stores));

        QLookupDetail lookupDetail = QLookupDetail.lookupDetail;
        Map<String, String> customerTypes = Maps.newLinkedHashMap();
        customerTypes.put("", "");
        customerTypes.putAll(new JPAQuery(em).from(lookupDetail).where(lookupDetail.header.code.eq("MEM007")).map(lookupDetail.code, lookupDetail.description));

        filters.add(FilterField.createDropdownField(
                FILTER_CUSTOMER_TYPE, messageSource.getMessage("label_customertype", (Object[]) null, LocaleContextHolder.getLocale()), customerTypes));
        return filters;
    }
    
    @Override
    public JRProcessor createJrProcessor(Map<String, String> map) {
    	Date startDate = INPUT_DATE_PATTERN.parseLocalDate(map.get(FILTER_DATE_FROM)).toDateTimeAtStartOfDay().toDate();
		Date endDate = INPUT_DATE_PATTERN.parseLocalDate(map.get(FILTER_DATE_TO)).plusDays(1).toDateTimeAtStartOfDay().minusMillis(1).toDate();
		String memberType = map.get(FILTER_CUSTOMER_TYPE);
		String storeCode = map.get(FILTER_STORE);
		
		QPointsTxnModel qpoints = QPointsTxnModel.pointsTxnModel;
		QMemberModel qmember = QMemberModel.memberModel;
		QStore qstore = QStore.store;
		
		BooleanBuilder f = new BooleanBuilder();
		f.and(qpoints.storeCode.eq(qstore.code));
		f.and(qpoints.transactionDateTime.goe(startDate));
		f.and(qpoints.transactionDateTime.loe(endDate));
		if(StringUtils.isNotBlank(memberType))
			f.and(qmember.memberType.code.eq(memberType));
		if(StringUtils.isNotBlank(storeCode))
			f.and(qpoints.storeCode.eq(storeCode));
		
		QPointsTxnModel qpointsA = new QPointsTxnModel("pointsA");
		f.and(qpoints.id.in(new JPASubQuery().from(qpointsA)
				.where(qpointsA.transactionDateTime.goe(startDate).and(qpointsA.transactionDateTime.loe(endDate)))
				.groupBy(qpointsA.memberModel.id, qpointsA.storeCode).list(qpointsA.id.min())));
		
		NumberExpression<Long> one = Expressions.numberTemplate(Long.class, "1");
		NumberExpression<Long> zero = Expressions.numberTemplate(Long.class, "0");
		
		NumberPath<Integer> childrenPath = qmember.customerProfile.children;
		NumberExpression<Long> zeroToOne = new CaseBuilder()
			.when(childrenPath.isNull().or(childrenPath.in(0, 1))).then(one)
			.otherwise(zero);
		NumberExpression<Long> twoToThree = new CaseBuilder()
			.when(childrenPath.in(2, 3)).then(one)
			.otherwise(zero);
		NumberExpression<Long> four = new CaseBuilder()
			.when(childrenPath.eq(4)).then(one)
			.otherwise(zero);
		NumberExpression<Long> gtFour = new CaseBuilder()
			.when(childrenPath.gt(4)).then(one)
			.otherwise(zero);
		
		JPAQuery query = new JPAQuery(em).from(qpoints, qstore)
				.leftJoin(qpoints.memberModel, qmember)
				.where(f)
				.groupBy(qpoints.storeCode, qstore.name)
				.orderBy(qpoints.storeCode.asc());
		
		QBean<ReportBean> projection = Projections.bean(ReportBean.class,
				qpoints.storeCode.as("storeCode"),
				qstore.name.as("storeName"),
				zeroToOne.sum().as("zeroToOne"),
				twoToThree.sum().as("twoToThree"),
				four.sum().as("four"),
				gtFour.sum().as("gtFour")
				);
		
		JRDataSource dataSource = new JRQueryDSLDataSource(query, projection, null);
		
		DefaultJRProcessor jrProcessor = new DefaultJRProcessor(REPORT_NAME);
        jrProcessor.setFileResolver(new ClasspathFileResolver("reports"));
        jrProcessor.addParameter("SUB_DATA_SOURCE", dataSource);
        jrProcessor.addParameter("START_DATE", map.get(FILTER_DATE_FROM));
        jrProcessor.addParameter("END_DATE", map.get(FILTER_DATE_TO));
        
        if(StringUtils.isNotBlank(storeCode)) {
        	jrProcessor.addParameter("STORE", new JPAQuery(em).from(qstore).where(qstore.code.eq(storeCode)).singleResult(qstore.code.append(" - ").append(qstore.name)));	
        }
        if(StringUtils.isNotBlank(memberType)) {
        	QLookupDetail qlookup = QLookupDetail.lookupDetail;
        	jrProcessor.addParameter("MEMBER_TYPE", new JPAQuery(em).from(qlookup).where(qlookup.code.eq(memberType)).singleResult(qlookup.code.append(" - ").append(qlookup.description)));
        }
        
        jrProcessor.addParameter("printedBy", UserUtil.getCurrentUser().getUsername());
        jrProcessor.addParameter("printedDate", new LocalDate().toString("dd-MM-yyyy"));
        
        return jrProcessor;
    }
    
    public static class ReportBean {
    	private String storeCode;
    	private String storeName;
    	private Long zeroToOne;
    	private Long twoToThree;
    	private Long four;
    	private Long gtFour;
    	
    	
		public String getStoreName() {
			return storeName;
		}
		public void setStoreName(String storeName) {
			this.storeName = storeName;
		}
		public String getStoreCode() {
			return storeCode;
		}
		public void setStoreCode(String storeCode) {
			this.storeCode = storeCode;
		}
		public Long getZeroToOne() {
			return zeroToOne;
		}
		public void setZeroToOne(Long zeroToOne) {
			this.zeroToOne = zeroToOne;
		}
		public Long getTwoToThree() {
			return twoToThree;
		}
		public void setTwoToThree(Long twoToThree) {
			this.twoToThree = twoToThree;
		}
		public Long getFour() {
			return four;
		}
		public void setFour(Long four) {
			this.four = four;
		}
		public Long getGtFour() {
			return gtFour;
		}
		public void setGtFour(Long gtFour) {
			this.gtFour = gtFour;
		}
		
    	
    }

	@Override
	public boolean isEmpty(Map<String, String> map) {
		Date startDate = INPUT_DATE_PATTERN.parseLocalDate(map.get(FILTER_DATE_FROM)).toDateTimeAtStartOfDay().toDate();
		Date endDate = INPUT_DATE_PATTERN.parseLocalDate(map.get(FILTER_DATE_TO)).plusDays(1).toDateTimeAtStartOfDay().minusMillis(1).toDate();
		String memberType = map.get(FILTER_CUSTOMER_TYPE);
		String storeCode = map.get(FILTER_STORE);
		
		QPointsTxnModel qpoints = QPointsTxnModel.pointsTxnModel;
		QMemberModel qmember = QMemberModel.memberModel;
		QStore qstore = QStore.store;
		
		BooleanBuilder f = new BooleanBuilder();
		f.and(qpoints.storeCode.eq(qstore.code));
		f.and(qpoints.transactionDateTime.goe(startDate));
		f.and(qpoints.transactionDateTime.loe(endDate));
		if(StringUtils.isNotBlank(memberType))
			f.and(qmember.memberType.code.eq(memberType));
		if(StringUtils.isNotBlank(storeCode))
			f.and(qpoints.storeCode.eq(storeCode));
		
		JPAQuery query = new JPAQuery(em).from(qpoints, qstore)
				.leftJoin(qpoints.memberModel, qmember)
				.where(f);
		
		return !query.exists();
	}

}
