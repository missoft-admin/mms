package com.transretail.crm.core.entity;


import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

import com.transretail.crm.core.entity.enums.AuthenticationType;
import com.transretail.crm.core.entity.enums.Status;
import org.apache.commons.lang3.BooleanUtils;
import org.apache.commons.lang3.builder.ReflectionToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import org.hibernate.annotations.Type;
import org.hibernate.envers.Audited;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.transretail.crm.common.web.converter.BooleanSerializer;
import com.transretail.crm.core.entity.support.CustomAuditableEntity;
import com.transretail.crm.core.util.AppConstants;


@Audited
@Table(name = "CRM_USER")
@Entity
public class UserModel extends CustomAuditableEntity<Long> implements Serializable {

    private static final long serialVersionUID = 1L;


    @Column(name = "AUTHENTICATION_TYPE", length = 50, nullable = false)
    @Enumerated(EnumType.STRING)
    private AuthenticationType authenticationType = AuthenticationType.INTERNAL;


    @NotNull
    @Column(unique = true)
    private String username;

    @NotNull
    private String password;

    @NotNull
    @Column(name = "ACCOUNT_ENABLED", length = 1)
    @Type(type = "yes_no")
    private Boolean enabled = Boolean.FALSE;

    @Version
    @Column(name = "version")
    private Integer version = 1;

    @Column(name = "EMAIL")
    //@Pattern(regexp = "^([0-9a-zA-Z]([-.\\w]*[0-9a-zA-Z])*@([0-9a-zA-Z][-\\w]*[0-9a-zA-Z]\\.)+[a-zA-Z]{2,9})")
    @Pattern(regexp = AppConstants.EMAIL_PATTERN )
    private String email;

    @Column(name = "FIRST_NAME")
    private String firstName;

    @Column(name = "MIDDLE_NAME")
    private String middleName;

    @Column(name = "LAST_NAME")
    private String lastName;

    @OneToMany(mappedBy = "userId")
    private Set<UserRolePermission> rolePermissions = new HashSet<UserRolePermission>();

    /**
     * Mapped to Store.code
     */
    @NotNull
    @Column(name = "STORE_CODE")
    private String storeCode;

    @Column(name = "INVENTORY_LOCATION")
    private String inventoryLocation;
    
    @Column(name = "B2B_LOCATION")
    private String b2bLocation;

    public UserModel() {

    }

    public UserModel(Long id) {
        setId(id);
    }


    public AuthenticationType getAuthenticationType() {
        return authenticationType;
    }

    public void setAuthenticationType(AuthenticationType authenticationType) {
        this.authenticationType = authenticationType;
    }

    public String getUsername() {
        return this.username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return this.password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @JsonSerialize(using = BooleanSerializer.class)
    public Boolean getEnabled() {
        return this.enabled;
    }

    public void setEnabled(Boolean enabled) {
        this.enabled = BooleanUtils.toBoolean(enabled);
    }

    public Integer getVersion() {
        return this.version;
    }

    public void setVersion(Integer version) {
        this.version = version;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getMiddleName() {
        return middleName;
    }

    public void setMiddleName(String middleName) {
        this.middleName = middleName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getStoreCode() {
        return storeCode;
    }

    public void setStoreCode(String storeCode) {
        this.storeCode = storeCode;
    }

    public String getInventoryLocation() {
        return inventoryLocation;
    }

    public void setInventoryLocation(String inventoryLocation) {
        this.inventoryLocation = inventoryLocation;
    }

    public String toString() {
        return ReflectionToStringBuilder.toString(this, ToStringStyle.SHORT_PREFIX_STYLE);
    }

    public Set<UserRolePermission> getRolePermissions() {
        return rolePermissions;
    }

    public void setRolePermissions(Set<UserRolePermission> rolePermissions) {
        this.rolePermissions = rolePermissions;
    }

	public String getB2bLocation() {
		return b2bLocation;
	}

	public void setB2bLocation(String b2bLocation) {
		this.b2bLocation = b2bLocation;
	}
    
    
}

