<%@ include file="../common/taglibs.jsp" %>

<style type="text/css">
<!--
  #list_group .dataTable tr td:last-child {
    min-width: 108px;
    vertical-align: bottom;
  }
  #DataTables_Table_1.table tbody tr td:last-child {
      border-left: none;
  }
  

-->
</style>
  
  <spring:message var="typeName" code='label_group_storegroup'/>
  <spring:url value="/resources/images/show.png" var="show_image_url" />
  <spring:url value="/resources/images/update.png" var="update_image_url" />
  <spring:url value="/resources/images/delete.png" var="delete_image_url" />
  
  <div class="page-header page-header2">
      <h1><spring:message code="label_group_storegroups" /></h1>
  </div>
  
  	<div>
	<button class="btn btn-primary pull-right mb20" id="groupsLnk" data-query-str="${pageContext.request.queryString}">
		<spring:message code="global_menu_new" arguments="${ typeName }"/>
	</button>
	</div>
	
	<div class="clearfix"></div>
  
  <div id="content_groupList">

  <div id="list_group" 
    data-url="<c:url value="/groups/store/search/list" />"></div>
  </div>
  
  <script>
    var columnHeaders = [
      '<spring:message code="label_group_storegroup" />',
      /*'<spring:message code="label_group_stores" />',*/
      { text: "", className : "el-width-12" }
    ];
    
    var modelFields = [
      {name : 'name', sortable : true},
      /*{ name  : 'stores', sortable : true, fieldCellRenderer : function (data, type, row) {
          if(row.storeModels != null) {
        	  var stores = "";
        	  for ( var i = 0 ; i < row.storeModels.length ; i++ ) {
        		  stores += ( row.storeModels[i].code + " - " + row.storeModels[i].name + "<br/>" );
        	  }
            return stores;
          }
          else
        	  return row.storeModels;
      }},*/
      {customCell : function ( innerData, sSpecific, json ) {
          if ( sSpecific == 'display' ) {
	            var btnHtml = $("#btnViewContainer").html();
	            if ( !json.isUsed ) {
	            	btnHtml += $("#btnContainerUpdate").html();
	            }
	            btnHtml = btnHtml.replace( new RegExp("%ITEMID%", 'g'), json.id );
	            return btnHtml;
          } 
          else {
              return '';
          }
      }}
    ];
  </script>
  <style><!--.el-width-12 { width: 108px !important; }--></style>

<div class="hide" id="btnViewContainer">
  <spring:message var="view_label" code="label_view_stores" htmlEscape="false" />
  <c:url var="url_viewStores" value="/groups/store/items/%ITEMID%" />
  <a id="" href="#" class="viewItemsLnk icn-view pull-left tiptip" data-item-id="%ITEMID%" data-url="${url_viewStores}" title="${fn:escapeXml(view_label)}"></a>
</div>

<div class="hide" id="btnContainerUpdate">
  <spring:message arguments="${typeName}" code="entity_update" var="update_label" htmlEscape="false" />
  <a title="${fn:escapeXml(update_label)}" data-item-id="%ITEMID%" data-query-str="${pageContext.request.queryString}" href="#" id="" class="editGroupLnk icn-edit pull-left tiptip" ></a>
  
  <spring:message arguments="${typeName}" code="entity_delete" var="delete_label" htmlEscape="false" />
  <c:set var="delete_confirm_msg">
    <spring:escapeBody javaScriptEscape="true"><spring:message code="entity_delete_confirm" /></spring:escapeBody>
  </c:set>
  <input class="btn btn-primary icn-delete pull-left tiptip" type="button" 
      alt="${fn:escapeXml(delete_label)}" title="${fn:escapeXml(delete_label)}" 
      value="${delete_label}" onclick="getConfirm('${delete_confirm_msg}',function(result) {if(result) $('form#deleteForm%ITEMID%').submit();});" />

  <div class="hide">
    <spring:url value="/groups/store/%ITEMID%" var="delete_form_url" /> 
    <form:form action="${delete_form_url}" id="deleteForm%ITEMID%" method="DELETE">
      <c:if test="${not empty param.page}">
        <input name="page" type="hidden" value="1" />
      </c:if>
      <c:if test="${not empty param.size}">
        <input name="size" type="hidden" value="${fn:escapeXml(param.size)}" />
      </c:if>
    </form:form>
  </div>
</div>


  <jsp:include page="storegroupform.jsp" />
  <jsp:include page="storelist.jsp" />
  <jsp:include page="../common/confirm.jsp" />
    
  <link href="<spring:url value="/css/dataTables.css" />" rel="stylesheet">
  <script src="<spring:url value="/js/jquery/jquery.dataTables.js" />" ></script>
  <script src="<spring:url value="/js/jquery/jquery.dataTables.linkLengthMenu.js" />"></script>
  <script src="<spring:url value="/js/jquery/jquery.dataTables.customPaging.js" />"></script>
  <script src="<spring:url value="/js/jquery/ajax-datatable.js" />" ></script>
  <script src="<spring:url value="/js/common/form2js.js" />" ></script>
  <script src="<spring:url value="/js/common/json2.js" />" ></script>
  <script src="<spring:url value="/js/jquery/jquery.toObject.js" />" ></script>
    
  <script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/viewspecific/groups/storegroup.js"><![CDATA[&nbsp;]]></script>
