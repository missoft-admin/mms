package com.transretail.crm.common.reporting.engine.data;

import com.mysema.query.jpa.impl.JPAQuery;
import com.mysema.query.types.Expression;
import java.util.Iterator;
import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRField;
import org.apache.commons.beanutils.PropertyUtils;

/**
 * Customize JRDataSource supporting QueryDSL. This implementation support
 * pagination like by setting up the {@code fetchSize}, by default it set to 500
 * rows.
 *
 * I supports only {@link Expression} projections and it uses reflections to
 * determine the field and value. It allows also to customize the field at
 * runtime thru {@link FieldValueCustomizer} - a callback interface.
 *
 * @since 3.5
 * @author Monte Cillo Co (mco@exist.com)
 */
public class JRQueryDSLDataSource implements ReportDataSource {

    public interface FieldValueCustomizer {

        Object customize(String propertyName, Object value);
    }

    private final int pageSize;
    private int pageCount;
    private static final int DEFAULT_FETCH_SIZE = 500;
    private final JPAQuery jpaQuery;
    private final Expression<?> expression;
    private Object currentRow;
    private Iterator<?> iterator;
    private final Class<?> beanType;
    private FieldValueCustomizer customizer;

    public JRQueryDSLDataSource(@Nonnull JPAQuery jpaQuery, @Nonnull Expression<?> expression, int pageSize, @Nullable FieldValueCustomizer customizer) {
        this.pageSize = pageSize;
        this.jpaQuery = jpaQuery;
        this.expression = expression;
        this.customizer = customizer;
        this.beanType = expression.getType();
        this.pageCount = 0;
    }

    public JRQueryDSLDataSource(JPAQuery jpaQuery, Expression<?> expression) {
        this(jpaQuery, expression, DEFAULT_FETCH_SIZE, null);
    }

    public JRQueryDSLDataSource(JPAQuery jpaQuery, Expression<?> expression, FieldValueCustomizer customizer) {
        this(jpaQuery, expression, DEFAULT_FETCH_SIZE, customizer);
    }

    @Override
    public boolean next() throws JRException {
        if (pageSize <= 0 && iterator == null) {
            iterator = jpaQuery.list(expression).iterator();
        } else {
            if (iterator == null || !iterator.hasNext()) {
                iterator = jpaQuery.offset(pageCount * pageSize)
                        .limit(pageSize).list(expression).iterator();
                ++pageCount;
            }
        }

        if (iterator.hasNext()) {
            currentRow = beanType.cast(iterator.next());
            return true;
        }

        return false;
    }

    @Override
    public Object getFieldValue(JRField jrf) throws JRException {
        final String propertyName = jrf.getName();
        Object value = getFieldValue(propertyName);
        if (customizer != null) {
            value = customizer.customize(propertyName, value);
        }

        return value;
    }

    public Object getCurrentRow() {
        return currentRow;
    }

    protected Object getFieldValue(final String propertyName) throws IllegalArgumentException, JRException {
        try {
            return PropertyUtils.getProperty(currentRow, propertyName);
        } catch (java.lang.IllegalAccessException e) {
            throw new JRException("Error retrieving field value from bean : " + propertyName, e);
        } catch (java.lang.reflect.InvocationTargetException e) {
            throw new JRException("Error retrieving field value from bean : " + propertyName, e);
        } catch (java.lang.NoSuchMethodException e) {
            throw new JRException("Error retrieving field value from bean : " + propertyName, e);
        } catch (IllegalArgumentException e) {
            //FIXME replace with NestedNullException when upgrading to BeanUtils 1.7
            if (!e.getMessage().startsWith("Null property value for ")) {
                throw e;
            }
        }

        return null;
    }

    @Override
    public boolean isEmpty() {
        return !jpaQuery.exists();
    }
}
