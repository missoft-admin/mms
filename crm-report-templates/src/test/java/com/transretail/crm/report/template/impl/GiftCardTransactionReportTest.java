package com.transretail.crm.report.template.impl;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.google.common.collect.Sets;
import com.transretail.crm.common.reporting.exception.ReportException;
import com.transretail.crm.common.reporting.jasper.JRProcessor;
import com.transretail.crm.common.reporting.service.ReportService;
import com.transretail.crm.core.entity.PeopleSoftStore;
import com.transretail.crm.core.entity.enums.Status;
import com.transretail.crm.core.entity.lookup.LookupDetail;
import com.transretail.crm.core.entity.lookup.LookupHeader;
import com.transretail.crm.core.entity.lookup.Store;
import com.transretail.crm.core.repo.LookupHeaderRepo;
import com.transretail.crm.core.repo.PeopleSoftStoreRepo;
import com.transretail.crm.core.repo.StoreRepo;
import com.transretail.crm.core.service.LookupService;
import com.transretail.crm.giftcard.dto.ProductProfileDto;
import com.transretail.crm.giftcard.entity.*;
import com.transretail.crm.giftcard.entity.support.*;
import com.transretail.crm.giftcard.repo.*;
import com.transretail.crm.giftcard.service.GiftCardInventoryService;
import com.transretail.crm.giftcard.service.ProductProfileService;
import com.transretail.crm.giftcard.service.SalesOrderService;
import com.transretail.crm.report.template.ReportTemplate;
import java.io.File;
import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Random;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import net.sf.jasperreports.engine.JRParameter;
import org.apache.commons.io.FileUtils;
import org.joda.time.DateTime;
import org.joda.time.DateTimeUtils;
import org.joda.time.LocalDate;
import org.joda.time.LocalDateTime;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.context.support.MessageSourceResourceBundle;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

/**
 *
 * @author Monte Cillo Co (mco@exist.com)
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath*:/META-INF/spring/applicationContext*.xml",
    "/META-INF/spring/applicationContext-test.xml"})
public class GiftCardTransactionReportTest {

    private static final Logger logger = LoggerFactory.getLogger(GiftCardTransactionReportTest.class);
    @Autowired
    private ReportService reportService;
    @Autowired
    private MessageSource messageSource;
    @PersistenceContext
    private EntityManager em;
    @Autowired
    private LookupService lookupService;
    @Autowired
    private LookupHeaderRepo headerRepo;
    @Autowired
    private ProductProfileService productProfileService;
    @Autowired
    private GiftCardInventoryService inventoryService;
    @Autowired
    private StoreRepo storeRepo;
    @Autowired
    private PeopleSoftStoreRepo peopleSoftStoreRepo;
    @Autowired
    private SalesOrderService salesOrderService;
    @Autowired
    private GiftCardTransactionRepo transactionRepo;
    @Autowired
    private GiftCardTransactionItemRepo transactionItemRepo;
    @Autowired
    @Qualifier("giftCardTransactionReport")
    private ReportTemplate report;
    @Autowired
    private CardVendorRepo cardVendorRepo;
    @Autowired
    private GiftCardOrderRepo giftCardOrderRepo;
    @Autowired
    private ProductProfileRepo productProfileRepo;

    @Before
    public void setup() {
        long fixedMillis = DateTimeUtils.getInstantMillis(new LocalDateTime(2014, 11, 9, 05, 35).toDateTime());
        DateTimeUtils.setCurrentMillisFixed(fixedMillis);

        final LookupHeader lookupHeader = new LookupHeader("PPH01", "Product Profile Lookup");
        headerRepo.save(lookupHeader);
        final LookupDetail lookupDetail = new LookupDetail("PP001", "500000", lookupHeader, Status.ACTIVE);
        lookupService.saveDetail(lookupDetail);

        ProductProfile productProfile = new ProductProfile();
        productProfile.setAllowPartialRedeem(Boolean.TRUE);
        productProfile.setCardFee(7000.00);
        productProfile.setEffectiveMonths(12L);
        productProfile.setFaceValue(lookupDetail);
        productProfile.setProductCode("9800992310001");
        productProfile.setProductDesc("Rp 500,000 Carrefour Voucher");
        productProfile.setStatus(ProductProfileStatus.APPROVED);
        productProfileRepo.saveAndFlush(productProfile);

        LookupHeader regionLkp = new LookupHeader("REG000", "Region Lookup");
        headerRepo.save(regionLkp);
        final LookupDetail region = new LookupDetail("REG001", "Region 1 - South Jakarta", lookupHeader, Status.ACTIVE);
        lookupService.saveDetail(region);

        LookupHeader buLkp = new LookupHeader("BU000", "Business Unit Header");
        headerRepo.save(buLkp);
        final LookupDetail bu = new LookupDetail("BU001", "Carrefour", buLkp, Status.ACTIVE);
        lookupService.saveDetail(bu);

        final Store store = new Store("20001");
        store.setId(13232);
        store.setName("Groserindo");
        storeRepo.save(store);

        PeopleSoftStore ps = new PeopleSoftStore();
        ps.setAllowGcTxn(Boolean.TRUE);
        ps.setBusinessRegion(region);
        ps.setBusinessUnit(bu);
        ps.setBusinessTerritory(bu);
        ps.setPeoplesoftCode("PS20001");
        ps.setStore(store);

        peopleSoftStoreRepo.save(ps);

        final SalesOrderItem salesOrderItem = new SalesOrderItem();
        salesOrderItem.setPrintFee(BigDecimal.TEN);
        salesOrderItem.setProduct(productProfile);
        salesOrderItem.setQuantity(10l);

        SalesOrder salesOrder = new SalesOrder();
        salesOrder.setActivationDate(LocalDate.now());
        salesOrder.setHandlingFee(BigDecimal.TEN);
        salesOrder.setOrderDate(LocalDate.now());
        salesOrder.setItems(Sets.newHashSet(salesOrderItem));
        salesOrder.setOrderType(SalesOrderType.B2B_SALES);

        salesOrderItem.setOrder(salesOrder);

        SalesOrderPaymentInfo sop = new SalesOrderPaymentInfo();
        sop.setDateApproved(LocalDate.now());
        sop.setDateVerified(LocalDate.now());
        sop.setOrder(salesOrder);
        sop.setPaymentAmount(new BigDecimal("1000000.00"));
        sop.setPaymentDate(LocalDate.now());
        sop.setStatus(PaymentInfoStatus.APPROVED);
        salesOrder.setPayments(Sets.newHashSet(sop));

        final Long seriesRange[][] = new Long[][]{
            {1410240010000001l, 1410240010000001l},
            {141024001000004l, 1410240010000008l},
            {1410240010000010l, 141024001000014l}};

        List<SalesOrderAlloc> alloc = Lists.newArrayListWithCapacity(seriesRange.length);
        for (Long[] seriesRange1 : seriesRange) {
            final Long start = seriesRange1[0];
            final Long end = seriesRange1[1];
            final SalesOrderAlloc soa = new SalesOrderAlloc();
            soa.setSeriesStart(start);
            soa.setSeriesEnd(end);
            soa.setSoItem(salesOrderItem);
            alloc.add(soa);
        }
        salesOrderItem.setSoAlloc(Sets.newHashSet(alloc));

        salesOrderService.save(salesOrder);

        Long barcodeCount = 1410240010000014l - 1410240010000001l + 1;
        GiftCardTransaction tx = new GiftCardTransaction().transactionDate(LocalDateTime.now())
                .transactionNo("12345678901234")
                .transactionType(GiftCardSaleTransaction.ACTIVATION)
                .cashierId("cTU20001")
                .terminalId("02")
                .merchantId("20001")
                .discountPercentage(0.0)
                .salesOrder(salesOrder);

        CardVendor vendor = new CardVendor();
        vendor.setFormalName("vendor");
        vendor.setMailAddress("ail@a.com");
        vendor.setEmailAddress("email");
        vendor.setEnabled(true);
        vendor.setRegion("region");
        vendor.setDefaultVendor(true);
        cardVendorRepo.save(vendor);

        GiftCardOrder gco = new GiftCardOrder();
        gco.setApprovalDate(DateTime.now());
        gco.setApprovedBy("ni system");
        gco.setMoDate(LocalDate.now());
        gco.setMoNumber("123719287392");
        gco.setVendor(vendor);
        gco.setStatus(GiftCardOrderStatus.APPROVED);
        giftCardOrderRepo.save(gco);

        for (int i = 0; i < barcodeCount; i++) {
            GiftCardInventory gc = new GiftCardInventory();
            Long series = 1410240010000000l + i;
            Long barcode = new Long(series + Integer.toString(new Random().nextInt(899) + 100));
            gc.setBarcode(barcode.toString());
            gc.setSeries(series);
            gc.setBatchNo(1);
            gc.setSeqNo(4);
            gc.setOrderDate(LocalDate.now());
            gc.setOrder(gco);
            gc.setFaceValue(new BigDecimal("500000"));
            gc.setLocation(store.getCode());
            gc.setProductCode(productProfile.getProductCode());
            gc.setProductName(productProfile.getProductDesc());
            gc.setProfile(productProfile);
            inventoryService.save(gc);
            GiftCardTransactionItem txi = new GiftCardTransactionItem()
                    .giftCard(gc)
                    .transactionAmount(gc.getFaceValue().doubleValue());
            tx.addTransactionItem(txi);
        }

        transactionRepo.save(tx);
    }

    @Test
    public void testSomeMethod() throws ReportException {
        HashMap<String, String> param = Maps.newHashMap();
        JRProcessor jrProcessor = report.createJrProcessor(param);

        File testClassesDir = FileUtils.toFile(Thread.currentThread().getContextClassLoader().getResource(""));
        File output = new File(testClassesDir, "test-gift-card-transaction-output.pdf");
        logger.debug("Output location {}", output.getAbsolutePath());
        output.delete();
        Locale locale = LocaleContextHolder.getLocale();
        jrProcessor.addParameter(JRParameter.REPORT_LOCALE, locale);
        jrProcessor.addParameter(JRParameter.REPORT_RESOURCE_BUNDLE, new MessageSourceResourceBundle(messageSource, locale));
        reportService.exportPdfReport(jrProcessor, output);

        double bytes = output.length();
        double kilobytes = (bytes / 1024);
        logger.debug("Kilobytes: {}", kilobytes);
    }
}