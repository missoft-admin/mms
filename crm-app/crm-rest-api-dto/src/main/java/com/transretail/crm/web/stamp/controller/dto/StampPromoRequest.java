package com.transretail.crm.web.stamp.controller.dto;


import org.hibernate.validator.constraints.NotEmpty;


public abstract class StampPromoRequest {

    @NotEmpty
    private String memberId;
    @NotEmpty
    private String validationKey;



    public String getMemberId() {
		return memberId;
	}
	public void setMemberId(String memberId) {
		this.memberId = memberId;
	}
	public String getValidationKey() {
		return validationKey;
	}
	public void setValidationKey(String validationKey) {
		this.validationKey = validationKey;
	}

	public abstract String getSignature();

}
