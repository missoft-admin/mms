<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@include file="../../common/taglibs.jsp" %>
<div id="content_giftcard_print_service_request">
  <div class="page-header page-header2">
    <h1>
      <spring:message code="gc.print.service.request.page.title"/>
    </h1>
  </div>
  <div id="content-filters">
    <fieldset class="form-horizontal">
      <legend>
	<spring:message code="common.report.legend.report.filters"/>
      </legend>
      <div id="content-error" class="hide alert alert-error">
	<button type="button" class="close" onclick="$(this).parent().hide();">&times;</button>
	<div><form:errors path="*"/></div>
      </div>
      <c:url var="printActionUrl" value='/giftcard/service-request/report/print/pdf' />
      <form:form id="filtersForm" action="${printActionUrl}" modelAttribute="filters">
	<div class="control-group">
	  <spring:message code="gc.print.service.request.filter.date.filed" var="dateFiledLabel" />
	  <label for="dateFiled" class="control-label"><span class="required">*</span> ${dateFiledLabel}:</label>
	  <div class="controls">
	    <input type="text" name="dateFiled" placeholder="${dateFiledLabel}"/>
	  </div>
	</div>
	<div class="control-group">
	  <spring:message code="gc.print.service.request.filter.time.from" var="timeFromLabel" />
	  <label for="timeFrom" class="control-label"><span class="required">*</span> ${timeFromLabel}:</label>
	  <div class="controls">
	    <input type="text" name="timeFrom" placeholder="${timeFromLabel}" maxlength="5"/>
	  </div>
	</div>
	<div class="control-group">
	  <spring:message code="gc.print.service.request.filter.time.to" var="timeToLabel" />
	  <label for="timeTo" class="control-label"><span class="required">*</span> ${timeToLabel}:</label>
	  <div class="controls">
	    <input type="text" name="timeTo" placeholder="${timeToLabel}" maxlength="5"/>
	  </div>
	</div>
	<div class="control-group">
	  <spring:message code="gc.print.service.request.filter.filed.by" var="filedByLabel" />
	  <label for="filedBy" class="control-label"> ${filedByLabel}</label>
	  <div class="controls">
	    <input type="text" name="filedBy" placeholder="${filedByLabel}"/>
	  </div>
	</div>
	
	<div class="control-group form-actions">
	  <spring:message code="common.report.print.button" var="printLabel" />
    <button type="button" id="printBtn" class="btn btn-print link_print" data-loading-text="Loading...">
      ${printLabel}
    </button>
  </div>
	
      </form:form>
    </fieldset>
  </div>
</div>
<link href="<c:url value="/css/bootstrap/bootstrap-clockpicker.css" />" rel="stylesheet" />
<script src='<c:url value="/js/bootstrap/bootstrap-clockpicker.js"/>' type='text/javascript'>< ![CDATA[ & nbsp; ]] ></script>
<link href="<c:url value="/css/bootstrap/datepicker.css" />" rel="stylesheet" />
<script src='<c:url value="/js/bootstrap/bootstrap-datepicker.js"/>' type='text/javascript'>< ![CDATA[ & nbsp; ]] ></script>
<link href="<spring:url value="/css/dataTables.css" />" rel="stylesheet">
<script src="<spring:url value="/js/common/form2js.js" />" ></script>
<script src="<spring:url value="/js/common/json2.js" />" ></script>
<script src="<spring:url value="/js/jquery/jquery.toObject.js" />" ></script>
<script src="<spring:url value="/js/jquery/jquery.numberInput.js" />" ></script>
<script>
	  $(document).ready(function() {
	    var CONTAINER_ERROR = "#content-error > div";
	    var CONTENT_ERROR = "#content-error";

	    $("input[name='dateFiled']").datepicker({
	      format: 'dd-mm-yyyy',
	      autoclose: true
	    }).datepicker("setEndDate", new Date());

	    $("input[name='timeFrom']").clockpicker({autoclose: true}).numberInput("type", "int");
	    $("input[name='timeTo']").clockpicker({autoclose: true}).numberInput("type", "int");

	    $("#printBtn").click(function(e) {
	      e.stopPropagation();

	      var filtersForm = $('#filtersForm');
	      $.get(ctx + "/giftcard/service-request/report/validate?" + filtersForm.serialize(), function(data) {
		var contentFilters = $("#content-filters");
		// clear previous errors
		contentFilters.find(CONTENT_ERROR).hide();
		contentFilters.find(CONTAINER_ERROR).empty();

		if (data.success) {
		  var url = filtersForm.attr("action") + "?" + filtersForm.serialize();
		  window.open(url, '_blank', 'toolbar=0,location=0,menubar=0');
		} else {
		  errorInfo = "";
		  for (i = 0; i < data.result.length; i++) {
		    errorInfo += "<br>" + (i + 1) + ". " + data.result[i].defaultMessage;
		  }
		  contentFilters.find(CONTAINER_ERROR).html("Please correct following errors: " + errorInfo);
		  contentFilters.find(CONTENT_ERROR).show('slow');
		}
	      });

	    });
	  });
</script>