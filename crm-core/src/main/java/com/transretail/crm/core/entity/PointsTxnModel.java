package com.transretail.crm.core.entity;


import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.Type;
import org.hibernate.envers.Audited;
import org.hibernate.envers.NotAudited;
import org.joda.time.DateTime;
import org.joda.time.LocalDate;

import com.transretail.crm.core.entity.enums.PointTxnType;
import com.transretail.crm.core.entity.enums.TxnStatus;
import com.transretail.crm.core.entity.support.CustomIdAuditableEntity;


@Table(name = "CRM_POINTS")
@Entity
@Audited
public class PointsTxnModel extends CustomIdAuditableEntity<String> {
    private static final long serialVersionUID = 11212L;

    @Column(name = "TXN_NO")
    private String transactionNo;

    @Column(name = "RETURN_TXN_NO")
    private String returnTransactionNo;

    @Column(name = "TXN_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date transactionDateTime;

    @Column(name = "TXN_AMOUNT")
    private Double transactionAmount;

    @Column(name = "TXN_POINTS")
    private Double transactionPoints;

    @Column(name = "TXN_MEDIA")
    private String transactionMedia;

    @Column(name = "INCLUSIVE_DATE_FROM")
    @Temporal(TemporalType.TIMESTAMP)
    private Date incDateFrom;

    @Column(name = "INCLUSIVE_DATE_TO")
    @Temporal(TemporalType.TIMESTAMP)
    private Date incDateTo;

    @Column(name = "TXN_TYPE")
    @Enumerated(EnumType.STRING)
    private PointTxnType transactionType;

    @Column(name = "STATUS")
    @Enumerated(EnumType.STRING)
    private TxnStatus status;

    @Column(name = "REASON", length = 1000)
    private String approvalReason;

    @Column(name = "STORE_CODE")
    private String storeCode;
//    @ManyToOne(fetch = FetchType.LAZY)
//    @JoinColumn(name = "STORE_CODE")
//    private Store store;

    /*
        TODO: Mark this as lazy because listing points might not necessarily
        need membermodel info. All *ToOne are eager by default
    */
    @ManyToOne
    @JoinColumn(name = "MEMBER_ID")
    @NotAudited
    private MemberModel memberModel;


    /*
      Requested by Iman for easy mapping of crm_point record to account id in cases where member record in crm_table gets deleted

      will just use views instead

    @Column(name = "MEMBER_ACCOUNT_ID")
    private String memberAccountId;
          */

    @Column(name = "VALIDATED_BY")
    private String validatedBy;

    @Column(name = "VALIDATED_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date validatedDate;

    @Column(name = "COMMENTS", length = 1000)
    private String comment;

    @ManyToOne
    @JoinColumn(name = "TXN_OWNER")
    @NotAudited
    private MemberModel transactionContributer;

    @Column(name = "EXPIRY_DATE")
    @Temporal(TemporalType.DATE)
    @Type(type = "org.jadira.usertype.dateandtime.joda.PersistentLocalDate")
    private LocalDate expiryDate;

    @Column(name = "LAST_REDEEM")
    @Temporal(TemporalType.TIMESTAMP)
    @Type(type = "org.jadira.usertype.dateandtime.joda.PersistentDateTime")
    private DateTime lastRedeemTimestamp;

    @Column(name = "EXPIRE_EXCESS")
    private Double expireExcess;

    @Column(name = "VALID_PERIOD")
    private Integer validPeriod;

    public String getTransactionNo() {
        return transactionNo;
    }

    public void setTransactionNo(String transactionNo) {
        this.transactionNo = transactionNo;
    }

    public String getReturnTransactionNo() {
        return returnTransactionNo;
    }

    public void setReturnTransactionNo(String returnTransactionNo) {
        this.returnTransactionNo = returnTransactionNo;
    }

    public Date getTransactionDateTime() {
        return transactionDateTime;
    }

    public void setTransactionDateTime(Date transactionDateTime) {
        this.transactionDateTime = transactionDateTime;
    }

    public Double getTransactionAmount() {
        return transactionAmount;
    }

    public void setTransactionAmount(Double transactionAmount) {
        this.transactionAmount = transactionAmount;
    }

    public Double getTransactionPoints() {
        return transactionPoints;
    }

    public void setTransactionPoints(Double transactionPoints) {
        this.transactionPoints = transactionPoints;
    }

    public PointTxnType getTransactionType() {
        return transactionType;
    }

    public void setTransactionType(PointTxnType transactionType) {
        this.transactionType = transactionType;
    }

    public Date getIncDateFrom() {
        return incDateFrom;
    }

    public void setIncDateFrom(Date incDateFrom) {
        this.incDateFrom = incDateFrom;
    }

    public Date getIncDateTo() {
        return incDateTo;
    }

    public void setIncDateTo(Date incDateTo) {
        this.incDateTo = incDateTo;
    }

    public TxnStatus getStatus() {
        return status;
    }

    public void setStatus(TxnStatus status) {
        this.status = status;
    }

    public String getApprovalReason() {
        return approvalReason;
    }

    public void setApprovalReason(String approvalReason) {
        this.approvalReason = approvalReason;
    }

    public String getStoreCode() {
        return storeCode;
    }

    public void setStoreCode(String storeCode) {
//        if(store == null) {
//            store = new Store();
//        }
//        store.setCode(storeCode);
        this.storeCode = storeCode;
    }

//    public Store getStore() {
//        return store;
//    }
//
//    public void setStore(Store store) {
//        if (store != null) {
//            storeCode = store.getCode();
//        }
//        this.store = store;
//    }

    public MemberModel getMemberModel() {
        return memberModel;
    }

    public void setMemberModel(MemberModel memberModel) {
        this.memberModel = memberModel;
    }

    public String getValidatedBy() {
        return validatedBy;
    }

    public void setValidatedBy(String validatedBy) {
        this.validatedBy = validatedBy;
    }

    public Date getValidatedDate() {
        return validatedDate;
    }

    public void setValidatedDate(Date validatedDate) {
        this.validatedDate = validatedDate;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public String getTransactionMedia() {
        return transactionMedia;
    }

    public void setTransactionMedia(String transactionMedia) {
        this.transactionMedia = transactionMedia;
    }

    public MemberModel getTransactionContributer() {
        return transactionContributer;
    }

    public void setTransactionContributer(MemberModel transactionContributer) {
        this.transactionContributer = transactionContributer;
    }

    public LocalDate getExpiryDate() {
        return expiryDate;
    }

    public void setExpiryDate(LocalDate expiryDate) {
        this.expiryDate = expiryDate;
    }

    public Double getExpireExcess() {
        return expireExcess;
    }

    public void setExpireExcess(Double expireExcess) {
        this.expireExcess = expireExcess;
    }

    public Integer getValidPeriod() {
        return validPeriod;
    }

    public void setValidPeriod(Integer validPeriod) {
        this.validPeriod = validPeriod;
    }

    public DateTime getLastRedeemTimestamp() {
        return lastRedeemTimestamp;
    }

    public void setLastRedeemTimestamp(DateTime lastRedeemTimestamp) {
        this.lastRedeemTimestamp = lastRedeemTimestamp;
    }
//
//
//    public String getMemberAccountId() {
//        return memberAccountId;
//    }
//
//    public void setMemberAccountId(String memberAccountId) {
//        this.memberAccountId = memberAccountId;
//    }


}
