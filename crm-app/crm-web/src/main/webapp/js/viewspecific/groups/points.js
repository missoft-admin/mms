$(document).ready(function() {
	var CONTAINER_ERROR = "#contentError > div";
	var CONTENT_ERROR = "#contentError";
	
	$("#pointsCheckbox").click(togglePoints);
	
	$(".float").numberInput({
        "type" : "float"
    });
	$(".int").numberInput({
        "type" : "int"
    });
	
});

function togglePoints() {
	if($(this).is(":checked")) {
		$("#pointsForm").show();	
	} else {
		$("#pointsForm").hide();
	}
}


