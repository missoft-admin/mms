<%@ include file="../taglibs.jsp" %>

<style>
<!--

-->


</style>

<spring:url value="/member/registration" var="actionUrl"/>
<form:form modelAttribute="form" method="POST" action="${actionUrl}" cssClass="body-container form-horizontal">
  <h3>Be a member today.</h3>
  <fieldset>
    <legend><spring:message code="membership_information"/></legend>
    <div class="control-group">
      <form:label path="username" title="Username" cssClass="control-label" cssErrorClass="control-label error"><span class="required">*</span><spring:message
              code="username"/></form:label>
      <div class="controls">
        <form:input path="username" title="Username" cssClass="input-xlarge"/>
      </div>
    </div>
    <div class="control-group">
      <form:label path="password" title="Password" cssClass="control-label" cssErrorClass="control-label error"><span class="required">*</span><spring:message
              code="password"/></form:label>
      <div class="controls">
        <form:password path="password" title="Password" cssClass="input-xlarge"/>
      </div>
    </div>
    <div class="control-group">
      <form:label path="confirmPassword" title="Confirm Password" cssClass="control-label" cssErrorClass="control-label error"><span
              class="required">*</span><spring:message code="confirm_password"/></form:label>
      <div class="controls">
        <form:password path="confirmPassword" title="Confirm Password" cssClass="input-xlarge"/>
      </div>
    </div>
    <div class="control-group">
      <form:label path="email" title="Email" cssClass="control-label" cssErrorClass="control-label error"><span
              class="required">*</span><spring:message code="email"/></form:label>
      <div class="controls">
        <form:input path="email" title="Email" cssClass="input-xlarge"/>
      </div>
    </div>
    <div class="control-group">
      <form:label path="contact" title="Contact" cssClass="control-label" cssErrorClass="control-label error"><span
              class="required">*</span><spring:message code="mobilephone"/></form:label>
      <div class="controls">
        <form:input path="contact" title="Contact" cssClass="input-xlarge"/>
      </div>
    </div>

    <legend><spring:message code="personal_information"/></legend>
    <div class="control-group">
      <form:label path="lastName" title="Last Name" cssClass="control-label" cssErrorClass="control-label error"><spring:message
              code="lastname"/></form:label>
      <div class="controls">
        <form:input path="lastName" title="Last Name" cssClass="input-xlarge"/>
      </div>
    </div>
    <div class="control-group">
      <form:label path="firstName" title="First Name" cssClass="control-label"
                  cssErrorClass="control-label error"><span class="required">*</span><spring:message code="firstname"/></form:label>
      <div class="controls">
        <form:input path="firstName" title="First Name" cssClass="input-xlarge"/>
      </div>
    </div>
    <%-- <div class="control-group">
      <form:label path="middleName" title="Middle Name" cssClass="control-label"
                  cssErrorClass="control-label error"><spring:message code="middlename"/></form:label>
      <div class="controls">
        <form:input path="middleName" title="Middle Name" cssClass="input-xlarge"/>
      </div>
    </div> --%>
    <div class="control-group">
      <form:label path="gender" title="Gender" cssClass="control-label"
                  cssErrorClass="control-label error"><span class="required">*</span><spring:message code="gender"/></form:label>
      <div class="controls">
        <form:select path="gender" items="${genders}" title="Gender" itemValue="code" itemLabel="description" cssClass="input-xlarge"/>
      </div>
    </div>
    <div class="control-group">
      <form:label path="birthDate" title="Birthdate" cssClass="control-label"
                  cssErrorClass="control-label error"><span class="required">*</span><spring:message code="birthdate"/></form:label>
      <div class="controls">
        <form:input path="birthDate" title="Birthdate (dd-MM-yyyy)" cssClass="input-xlarge" placeholder="dd-MM-yyyy"/>
      </div>
    </div>
    <div class="control-group">
      <form:label path="preferredStoreCode" title="Preferred Store" cssClass="control-label"
                  cssErrorClass="control-label error"><spring:message code="registered_store"/></form:label>
      <div class="controls">
        <form:select path="preferredStoreCode" items="${stores}" title="Registered Store" itemValue="code" itemLabel="name"
                     cssClass="input-xlarge"/>
      </div>
    </div>
    <div class="form-actions">
      <p>By clicking the button, you agree to the <a href="#">Terms </br>of Agreement</a>.</p>
      <button type="submit" class="btn btn-primary"><spring:message code="button_register"/></button>
      <p style="" class="">Already have an account? <a class="" href="<spring:url value="/login"/>"><spring:message code="button_gotologin"/></a>.</p>
    </div>
  </fieldset>


<script src='<c:url value="/js/bootstrap/bootstrap-datepicker.js"/>' type='text/javascript'><![CDATA[&nbsp; ]]></script>
<link href="<c:url value="/css/bootstrap/datepicker.css" />" rel="stylesheet" />

<script>
  $("#birthDate").datepicker({
    format    : 'dd-mm-yyyy',
    endDate   : '-0d',
    autoclose : true
  });
</script>
</form:form>