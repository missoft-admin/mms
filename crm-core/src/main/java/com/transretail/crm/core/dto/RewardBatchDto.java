package com.transretail.crm.core.dto;


import java.util.Date;
import java.util.List;

import com.transretail.crm.core.entity.PointsTxnModel;


public class RewardBatchDto {
	
	private String processId;
	private String message;
	private Date incDateFrom;
	private Date incDateTo;
	private Long totalRewards;
	private List<PointsTxnModel> rewards;

	public String getProcessId() {
		return processId;
	}

	public void setProcessId(String processId) {
		this.processId = processId;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public Date getIncDateFrom() {
		return incDateFrom;
	}

	public void setIncDateFrom(Date incDateFrom) {
		this.incDateFrom = incDateFrom;
	}

	public Date getIncDateTo() {
		return incDateTo;
	}

	public void setIncDateTo(Date incDateTo) {
		this.incDateTo = incDateTo;
	}

	public Long getTotalRewards() {
		return totalRewards;
	}

	public void setTotalRewards(Long totalRewards) {
		this.totalRewards = totalRewards;
	}

	public List<PointsTxnModel> getRewards() {
		return rewards;
	}

	public void setRewards(List<PointsTxnModel> rewards) {
		this.rewards = rewards;
	}

}
