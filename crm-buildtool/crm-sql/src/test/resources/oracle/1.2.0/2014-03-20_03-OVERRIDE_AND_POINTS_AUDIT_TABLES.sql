create table CRM_APPROVAL_MATRIX_AUD (
        id number(19,0) not null,
        REV number(19,0) not null,
        REVTYPE number(3,0),
        AMOUNT double precision,
        MODEL_TYPE varchar2(255 char),
        USERNAME number(19,0),
        primary key (id, REV)
    );

create table CRM_POINTS_AUD (
        id varchar2(255 char) not null,
        REV number(19,0) not null,
        REVTYPE number(3,0),
        REASON varchar2(1000 char),
        COMMENTS varchar2(1000 char),
        EXPIRE_EXCESS double precision,
        EXPIRY_DATE date,
        INCLUSIVE_DATE_FROM timestamp,
        INCLUSIVE_DATE_TO timestamp,
        LAST_REDEEM timestamp,
        RETURN_TXN_NO varchar2(255 char),
        STATUS varchar2(255 char),
        STORE_CODE varchar2(255 char),
        TXN_AMOUNT double precision,
        TXN_DATE timestamp,
        TXN_MEDIA varchar2(255 char),
        TXN_NO varchar2(255 char),
        TXN_POINTS double precision,
        TXN_TYPE varchar2(255 char),
        VALID_PERIOD number(10,0),
        VALIDATED_BY varchar2(255 char),
        VALIDATED_DATE timestamp,
        MEMBER_ID number(19,0),
        TXN_OWNER number(19,0),
        primary key (id, REV)
    );

alter table CRM_APPROVAL_MATRIX_AUD 
        add constraint FK_djhyw5w2f7xf7vutud318355r 
        foreign key (REV) 
        references CRM_REVINFO;

alter table CRM_POINTS_AUD 
        add constraint FK_2jvrt58lew0bjwjhfpsw3o03c 
        foreign key (REV) 
        references CRM_REVINFO;