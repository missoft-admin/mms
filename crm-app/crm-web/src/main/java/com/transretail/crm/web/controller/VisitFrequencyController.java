package com.transretail.crm.web.controller;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

import javax.servlet.http.HttpServletResponse;

import org.joda.time.DateTime;
import org.joda.time.LocalDate;
import org.joda.time.format.DateTimeFormat;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.google.common.collect.Lists;
import com.transretail.crm.common.reporting.jasper.JRProcessor;
import com.transretail.crm.core.entity.enums.MemberCreatedFromType;
import com.transretail.crm.core.entity.enums.ReportType;
import com.transretail.crm.core.entity.lookup.LookupDetail;
import com.transretail.crm.core.service.CodePropertiesService;
import com.transretail.crm.core.service.LookupService;
import com.transretail.crm.core.service.StoreService;
import com.transretail.crm.report.template.impl.VisitFrequencyReport;
import com.transretail.crm.web.controller.util.ControllerResponse;

/**
 * @author ftopico
 */
@RequestMapping("/visit/frequency")
@Controller
public class VisitFrequencyController extends AbstractReportsController {

    private static final String PATH_VISIT_FREQUENCY = "/visit/frequency";
    
    @Autowired
    private VisitFrequencyReport visitFrequencyReport;
    @Autowired
    private StoreService storeService;
    @Autowired
    private LookupService lookupService;
    @Autowired
    private CodePropertiesService codePropertiesService;
    @Autowired
    private MessageSource messageSource;
    
    @RequestMapping(produces="text/html")
    public String showBurnCardForm(Model model) {
        model.addAttribute("reportTypes", ReportType.values());
        model.addAttribute("registerTypes", MemberCreatedFromType.values());
        model.addAttribute("stores", storeService.getAllStores());
        HashMap<String, String> customers = new LinkedHashMap<String, String>();
        for (LookupDetail detail : lookupService.getActiveDetailsByHeaderCode(codePropertiesService.getHeaderMemberType())) {
            if (!detail.getCode().equalsIgnoreCase("MTYP002")) {
                customers.put(detail.getCode(), detail.getDescription());
            }
        }
        model.addAttribute("customers", customers);
        return PATH_VISIT_FREQUENCY;
    }
    
    @RequestMapping( value="/print/validate", produces="application/json; charset=utf-8" )
    public @ResponseBody ControllerResponse printValidate(
            @RequestParam String standardReportType,
            @RequestParam String dateFrom,
            @RequestParam String dateTo,
            @RequestParam(required=false) String customerType,
            @RequestParam(required=false) String store,
            Model model) {

        ControllerResponse resp = new ControllerResponse();
        DateTime dateFromLocalDate = null;
        if (!dateFrom.equalsIgnoreCase("")) {
            dateFromLocalDate = DateTimeFormat.forPattern("dd-MM-yyyy").parseDateTime(dateFrom);
        }
        DateTime dateToLocalDate = null;
        if (!dateTo.equalsIgnoreCase("")) {
            dateToLocalDate = DateTimeFormat.forPattern("dd-MM-yyyy").parseDateTime(dateTo);
        }
        if (dateFromLocalDate == null || dateToLocalDate == null) {
            resp.setSuccess(false);
            resp.setResult(Lists.newArrayList(messageSource.getMessage("report.date.from.date.to.required", null, LocaleContextHolder.getLocale())));
        } else if (dateFromLocalDate.isAfter(dateToLocalDate)) {
            resp.setSuccess(false);
            resp.setResult(Lists.newArrayList(messageSource.getMessage("report.date.from.before.date.to", null, LocaleContextHolder.getLocale())));
        } else if (dateFromLocalDate.plusMonths(12).isBefore(dateToLocalDate)) {
            resp.setSuccess(false);
            resp.setResult(Lists.newArrayList(messageSource.getMessage("report.date.limit", null, LocaleContextHolder.getLocale())));
        } else {
            resp.setSuccess(true);
        }
        
        Map<String, String> filtersMap = new HashMap<String, String>();
        filtersMap.put("standardReportType", standardReportType);
        filtersMap.put("dateFrom", dateFrom);
        filtersMap.put("dateTo", dateTo);
        filtersMap.put("customerType", customerType);
        filtersMap.put("store", store);
        
         if (!visitFrequencyReport.isDataFound(filtersMap) && resp.isSuccess()) {
             resp.setSuccess(false);
             resp.setResult(Lists.newArrayList(messageSource.getMessage("report.no.data.found", null, LocaleContextHolder.getLocale())));
         }
        
        return resp;
    }
    
    @RequestMapping(value="/print", method=RequestMethod.GET)
    public void print(
            @RequestParam String reportType,
            @RequestParam String standardReportType,
            @RequestParam String dateFrom,
            @RequestParam String dateTo,
            @RequestParam(required=false) String customerType,
            @RequestParam(required=false) String store,
            HttpServletResponse response) throws Exception {
        
        Map<String, String> filtersMap = new HashMap<String, String>();
        filtersMap.put("standardReportType", standardReportType);
        filtersMap.put("dateFrom", dateFrom);
        filtersMap.put("dateTo", dateTo);
        filtersMap.put("customerType", customerType);
        filtersMap.put("store", store);
        
        JRProcessor jrProcessor = visitFrequencyReport.createJrProcessor(filtersMap);
        
        if (reportType.equalsIgnoreCase(ReportType.PDF.name())) {
            renderReport(response, jrProcessor);
        } else if (reportType.equalsIgnoreCase(ReportType.EXCEL.name())) {
            String filename = null;
            
            if (standardReportType.equalsIgnoreCase("customerCount")) {
                filename = "Visit Frequency Customer Count Report (" + LocalDate.now() +")";
            } else if (standardReportType.equalsIgnoreCase("purchaseCount")) {
                filename = "Visit Frequency Customer Purchase Value Count Report (" + LocalDate.now() +")";
            }
            renderExcelReport(response, jrProcessor, filename);
        }
        
    }
}
