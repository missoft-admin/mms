package com.transretail.crm.report.template;

import com.transretail.crm.common.reporting.engine.data.ReportDataSource;

import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRField;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;

public class CollectionDataSource implements ReportDataSource {

    private final JRBeanCollectionDataSource beanCollectionDataSource;

    public CollectionDataSource(JRBeanCollectionDataSource beanCollectionDataSource) {
        this.beanCollectionDataSource = beanCollectionDataSource;
    }

    @Override
    public boolean isEmpty() {
        return beanCollectionDataSource.getData().isEmpty();
    }

    @Override
    public boolean next() throws JRException {
        return beanCollectionDataSource.next();
    }

    @Override
    public Object getFieldValue(JRField jrf) throws JRException {
        return beanCollectionDataSource.getFieldValue(jrf);
    }
}
