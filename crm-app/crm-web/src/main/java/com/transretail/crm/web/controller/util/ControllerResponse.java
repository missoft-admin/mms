package com.transretail.crm.web.controller.util;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.transretail.crm.common.enums.MessageType;

@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
public class ControllerResponse {
    private boolean itsSuccess;
    private Object itsResult;
    private String itsMessage;
    private String modelId;
    private List<String> modelIds;
    private boolean notificationCreation = false;

    public Boolean isSuccess() {
        return itsSuccess;
    }

    public void setSuccess(boolean inSuccess) {
        this.itsSuccess = inSuccess;
    }

    public Object getResult() {
        return itsResult;
    }

    public void setResult(Object inResult) {
        this.itsResult = inResult;
    }

    public String getMessage() {
        return itsMessage;
    }

    @JsonIgnore
    public void setMessage(MessageType inMessage) {
        if (inMessage != null) {
            this.itsMessage = inMessage.toString();
        }
    }

    public void setMessage(String message) {
        this.itsMessage = message;
    }

    public String getModelId() {
        return modelId;
    }

    public void setModelId(String id) {
        this.modelId = id;
    }

    public List<String> getModelIds() {
		return modelIds;
	}

	public void setModelIds(List<String> modelIds) {
		this.modelIds = modelIds;
	}

	public Boolean isNotificationCreation() {
        return notificationCreation;
    }

    public void setNotificationCreation(boolean notificationCreation) {
        this.notificationCreation = notificationCreation;
    }
}
