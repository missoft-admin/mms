package com.transretail.crm.giftcard.service.impl;

import com.google.common.base.Function;
import com.google.common.collect.*;
import com.mysema.query.jpa.JPASubQuery;
import com.mysema.query.jpa.JPQLQuery;
import com.mysema.query.jpa.impl.JPAQuery;
import com.mysema.query.types.Projections;
import com.mysema.query.types.expr.BooleanExpression;
import com.mysema.query.types.expr.NumberExpression;
import com.transretail.crm.common.reporting.jasper.ClasspathFileResolver;
import com.transretail.crm.common.reporting.jasper.DefaultJRProcessor;
import com.transretail.crm.common.reporting.jasper.JRProcessor;
import com.transretail.crm.common.service.dto.request.PageSortDto;
import com.transretail.crm.common.service.dto.request.PagingParam;
import com.transretail.crm.common.service.dto.response.ResultList;
import com.transretail.crm.common.service.exception.GenericServiceException;
import com.transretail.crm.common.service.util.SpringDataPagingUtil;
import com.transretail.crm.core.security.model.impl.CustomSecurityUserDetailsImpl;
import com.transretail.crm.core.security.util.UserUtil;
import com.transretail.crm.core.service.CodePropertiesService;
import com.transretail.crm.core.service.LookupService;
import com.transretail.crm.core.service.StoreService;
import com.transretail.crm.giftcard.dto.GiftCardPhysicalCountDto;
import com.transretail.crm.giftcard.dto.GiftCardPhysicalCountSummaryDto;
import com.transretail.crm.giftcard.entity.*;
import com.transretail.crm.giftcard.entity.support.GiftCardInventoryStatus;
import com.transretail.crm.giftcard.repo.GiftCardAdjustInventoryRepo;
import com.transretail.crm.giftcard.repo.GiftCardInventoryRepo;
import com.transretail.crm.giftcard.repo.GiftCardPhysicalCountSummaryRepo;
import com.transretail.crm.giftcard.repo.ProductProfileRepo;
import com.transretail.crm.giftcard.service.GiftCardAdjustmentInventoryService;
import com.transretail.crm.giftcard.service.GiftCardInventoryStockService;
import com.transretail.crm.giftcard.service.impl.GiftCardAdjustmentInventoryServiceImpl.ReportBean.Entry;
import java.util.*;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.joda.time.LocalDateTime;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import static com.transretail.crm.giftcard.entity.support.GiftCardInventoryStatus.IN_STOCK;
import static com.transretail.crm.giftcard.entity.support.GiftCardInventoryStatus.MISSING;

@Service
public class GiftCardAdjustmentInventoryServiceImpl implements GiftCardAdjustmentInventoryService {

    @Autowired
    private GiftCardAdjustInventoryRepo countRepo;
    @Autowired
    private GiftCardPhysicalCountSummaryRepo summaryRepo;
    @Autowired
    private GiftCardInventoryRepo inventoryRepo;
    @Autowired
    private GiftCardInventoryStockService stockService;
    @Autowired
    private ProductProfileRepo productProfileRepo;
    @Autowired
    private CodePropertiesService codePropertiesService;
    @Autowired
    private LookupService lookupService;
    @Autowired
    private StoreService storeService;
    @PersistenceContext
    private EntityManager em; // TODO: move to repo - mco

    @Override
    public boolean validateSeries(String startingSeries, String endingSeries) {
        long starting = Long.parseLong(startingSeries);
        long ending = Long.parseLong(endingSeries);

        if (ending < starting) {
            return false;
        }

        QGiftCardInventory qg = QGiftCardInventory.giftCardInventory;
        int expected = 2;
        if (starting == ending) {
            expected = 1;
        }

        ArrayList<GiftCardInventory> results = Lists.newArrayList(
                inventoryRepo.findAll(qg.series.in(starting, ending)
                        .and(qg.status.in(GiftCardInventoryStatus.IN_STOCK, GiftCardInventoryStatus.MISSING))));
        return results.size() == expected;
    }

    @Override
    public boolean isCounted(String startingSeries, String endingSeries) {
        long starting = Long.parseLong(startingSeries);
        long ending = Long.parseLong(endingSeries);
        QGiftCardPhysicalCount qpc = QGiftCardPhysicalCount.giftCardPhysicalCount;
        NumberExpression<Long> startingExp = qpc.startingSeries.castToNum(Long.class);
        NumberExpression<Long> endingExp = qpc.endingSeries.castToNum(Long.class);
        BooleanExpression filter = BooleanExpression.anyOf(
                startingExp.loe(starting).and(endingExp.goe(starting)),
                startingExp.loe(ending).and(endingExp.goe(ending)),
                startingExp.goe(starting).and(startingExp.loe(ending)),
                endingExp.goe(starting).and(endingExp.loe(ending)))
                .and(qpc.location.equalsIgnoreCase(getCurrentUserLocation()));

        final long count = countRepo.count(filter);
        return count > 0;
    }

    @Override
    public boolean isBarcodeExist(String barcode) {
        QGiftCardInventory qgci = QGiftCardInventory.giftCardInventory;
        GiftCardInventory result = inventoryRepo.findOne(qgci.barcode.eq(barcode));
        return result != null;
    }

    @Override
    @Transactional
    public void savePhysicalCount(GiftCardPhysicalCountDto dto) {
        countRepo.save(dto.toModel());
    }

    @Override
    @Transactional(readOnly = true)
    public ResultList<GiftCardPhysicalCountDto> listPhysicalCount(PageSortDto sortDto) {
        Pageable pageable = SpringDataPagingUtil.INSTANCE.createPageable(sortDto.getPagination());
        QGiftCardPhysicalCount qCount = QGiftCardPhysicalCount.giftCardPhysicalCount;
        BooleanExpression filter = qCount.location.eq(getCurrentUserLocation());
        Page<GiftCardPhysicalCount> page = countRepo.findAll(filter, pageable);
        return new ResultList<GiftCardPhysicalCountDto>(convertToDto(page),
                page.getTotalElements(), page.hasPreviousPage(), page.hasNextPage());
    }

    @Override
    @Transactional
    public void processPhysicalCount() {
        List<GiftCardPhysicalCount> physicalCounts = countRepo.findAll();
        QGiftCardInventory qinv = QGiftCardInventory.giftCardInventory;
        String location = getCurrentUserLocation();

        List<BooleanExpression> seriesFilter = Lists.newArrayList();
        for (GiftCardPhysicalCount pc : physicalCounts) {
            long starting = Long.parseLong(pc.getStartingSeries());
            long ending = Long.parseLong(pc.getEndingSeries());

            seriesFilter.add(qinv.series.goe(starting).and(qinv.series.loe(ending)));
        }

        // missing
        QGiftCardInventory qinv2 = QGiftCardInventory.giftCardInventory;
        List<GiftCardInventory> missingCards = new JPAQuery(em).from(qinv)
                .where(qinv.location.eq(location)
                        .and(qinv.status.in(IN_STOCK))
                        .and(qinv.allocateTo.isNull())
                        .and(qinv.notIn(new JPASubQuery().from(qinv2)
                                        .where(qinv2.location.eq(location)
                                                .and(qinv2.status.in(IN_STOCK))
                                                .and(qinv2.allocateTo.isNull())
                                                .and(anyOfExpression(seriesFilter)))
                                        .list(qinv2))))
                .list(qinv);

        List<GiftCardInventory> foundCards = Lists.newArrayList(inventoryRepo.findAll(qinv.location.eq(location)
                .and(qinv.status.eq(MISSING)).and(qinv.allocateTo.isNull()).and(anyOfExpression(seriesFilter))));

        ImmutableListMultimap<String, GiftCardInventory> missingItems = groupByProductProfile(missingCards);
        ImmutableListMultimap<String, GiftCardInventory> foundItems = groupByProductProfile(foundCards);
        List<GiftCardInventory> cardToAdjust = Lists.newArrayList(missingCards);
        cardToAdjust.addAll(foundCards);

        doSaveSummary(foundItems, missingItems);
        countRepo.updateReceivedInventories(physicalCounts);
        countRepo.updateMissingInventories(physicalCounts);

        stockService.saveStocksFromPhysicalCount(missingCards, foundCards);

        countRepo.deleteAll();
    }

    /**
     * Group the list of gift card inventory by Product Profile Code. Product
     * Profile Code as the key and gift card inventory as the value.
     *
     * @param foundCards
     * @return
     */
    private ImmutableListMultimap<String, GiftCardInventory> groupByProductProfile(List<GiftCardInventory> foundCards) {
        ImmutableListMultimap<String, GiftCardInventory> index = Multimaps.index(foundCards, new Function<GiftCardInventory, String>() {

            @Override
            public String apply(GiftCardInventory input) {
                return input.getProductCode();
            }
        });
        return index;
    }

    private List<GiftCardPhysicalCountDto> convertToDto(Page<GiftCardPhysicalCount> page) {
        ArrayList<GiftCardPhysicalCountDto> dtos = Lists.newArrayList();
        for (GiftCardPhysicalCount gc : page) {
            dtos.add(convertToDto(gc));
        }

        return dtos;
    }

    private GiftCardPhysicalCountDto convertToDto(GiftCardPhysicalCount model) {
        GiftCardPhysicalCountDto dto = new GiftCardPhysicalCountDto();
        BeanUtils.copyProperties(model, dto);

        QProductProfile qpp = QProductProfile.productProfile;
        ProductProfile result = productProfileRepo.findOne(qpp.productCode.eq(dto.getProductProfile()));
        dto.setProductProfileName(result.getProductDesc());
        if (StringUtils.isNotBlank(dto.getLocation())) {
            if (dto.getLocation().equalsIgnoreCase(codePropertiesService.getDetailInvLocationHeadOffice())) {
                dto.setLocationName(lookupService.getDetailByCode(dto.getLocation()).getDescription());
            } else {
                dto.setLocationName(storeService.getStoreByCode(dto.getLocation()).getName());
            }
        }

        return dto;
    }

    String getCurrentUserLocation() {
        CustomSecurityUserDetailsImpl userDetails = UserUtil.getCurrentUser();
        return userDetails.getInventoryLocation();
    }

    @Override
    public ResultList<GiftCardPhysicalCountSummaryDto> getSummary(PageSortDto pageDto) {
        return countRepo.getSummary(pageDto);
    }

    private BooleanExpression anyOfExpression(List<BooleanExpression> predicates) {
        return BooleanExpression.anyOf(predicates.toArray(new BooleanExpression[predicates.size()]));
    }

    private void doSaveSummary(Multimap<String, GiftCardInventory> foundItems,
            Multimap<String, GiftCardInventory> missingItems) {
        PageSortDto sortDto = new PageSortDto();
        sortDto.getPagination().setPageSize(-1); //to retrieve all results
        List<GiftCardPhysicalCountSummary> summaryList = Lists.newArrayList();
        LocalDateTime created = new LocalDateTime();
        final Collection<GiftCardPhysicalCountSummaryDto> results = getSummary(sortDto).getResults();
        QGiftCardInventory qgc = QGiftCardInventory.giftCardInventory;
        for (GiftCardPhysicalCountSummaryDto summaryDto : results) {
            final GiftCardPhysicalCountSummary model = summaryDto.toModel(created);
//	    final String profileCode = model.getProductProfile();
            if(CollectionUtils.isNotEmpty(summaryDto.getFoundItems()))
            	model.addFoundItems(new JPAQuery(em).from(qgc).where(qgc.barcode.in(summaryDto.getFoundItems())).list(qgc));
            if(CollectionUtils.isNotEmpty(summaryDto.getMissingItems()))
            	model.addMissingItems(new JPAQuery(em).from(qgc).where(qgc.barcode.in(summaryDto.getMissingItems())).list(qgc));

            summaryList.add(model);
        }

        summaryRepo.save(summaryList);
    }

    @Override
    public List<LocalDateTime> getSummaryDates(long maxResult) {
        QGiftCardPhysicalCountSummary qpcs = QGiftCardPhysicalCountSummary.giftCardPhysicalCountSummary;
        List<LocalDateTime> list = new JPAQuery(em).from(qpcs)
                .where(qpcs.location.eq(getCurrentUserLocation()))
                .distinct().orderBy(qpcs.created.desc())
                .limit(maxResult)
                .list(qpcs.created);
        return list;
    }

    @Override
    public JRProcessor createJRProcessor(LocalDateTime date) {
        QGiftCardPhysicalCountSummary qSummary = QGiftCardPhysicalCountSummary.giftCardPhysicalCountSummary;
        BooleanExpression filter = qSummary.created.eq(date);
        Iterable<GiftCardPhysicalCountSummary> list = summaryRepo.findAll(filter);
        return createJRProcessor(convertSummaryToDto(list));
    }

    @Override
    public JRProcessor createJRProcessor() {
        PageSortDto sortDto = new PageSortDto();
        sortDto.getPagination().setPageSize(-1); //to retrieve all results
        ResultList<GiftCardPhysicalCountSummaryDto> results = getSummary(sortDto);
        return createJRProcessor(results.getResults());
    }

    private List<GiftCardPhysicalCountSummaryDto> convertSummaryToDto(Iterable<GiftCardPhysicalCountSummary> list) {
        List<GiftCardPhysicalCountSummaryDto> summaryList = Lists.newArrayList();
        QProductProfile qpp = QProductProfile.productProfile;
        for (GiftCardPhysicalCountSummary summary : list) {
            GiftCardPhysicalCountSummaryDto summaryDto = new GiftCardPhysicalCountSummaryDto(summary);
            ProductProfile result = productProfileRepo.findOne(qpp.productCode.eq(summaryDto.getProductProfile()));
            summaryDto.setProductProfile(result.getProductCode());
            summaryDto.setProductProfileName(result.getProductDesc());

            for (PhysicalCountGiftCardStatus cardStatus : summary.getDetails()) {
                if (PhysicalCountGiftCardStatus.Status.FOUND.equals(cardStatus.getStatus())) {
                    summaryDto.addFoundItem(cardStatus.getInventory().getBarcode());
                } else {
                    summaryDto.addMissingItem(cardStatus.getInventory().getBarcode());
                }
            }
            summaryList.add(summaryDto);
        }

        return summaryList;
    }

    private JRProcessor createJRProcessor(Collection<GiftCardPhysicalCountSummaryDto> list) {
        Map<String, Object> parameters = Maps.newHashMap();
        String userLocation = getCurrentUserLocation();
        parameters.put("LOCATION", userLocation + " - " + UserUtil.getCurrentUser().getInventoryLocationName());

        ArrayList<ReportBean.Entry> details = Lists.newArrayList();
        for (GiftCardPhysicalCountSummaryDto summary : list) {
            final String profile = summary.getProductProfile() + " - " + summary.getProductProfileName();
            Entry entry = new ReportBean.Entry().productProfile(profile)
                    .foundItems(summary.getFoundItems())
                    .missingItems(summary.getMissingItems());
            details.add(entry);
        }

        ReportBean reportBean = new ReportBean().summary(Lists.newArrayList(list)).details(details);

        DefaultJRProcessor jrProcessor = new DefaultJRProcessor(REPORT_NAME, ImmutableList.of(reportBean));
        jrProcessor.addParameters(parameters);
        jrProcessor.setFileResolver(new ClasspathFileResolver("reports"));
        return jrProcessor;
    }

    @Override
    public GiftCardInventory getGiftCardInventoryByBarcode(String barcode) {
        return inventoryRepo.findByBarcode(barcode);
    }

    @Override
    public String getFormattedProductProfile(String barcode) {
        GiftCardInventory inventory = this.getGiftCardInventoryByBarcode(barcode);
        if (inventory == null) {
            throw new GenericServiceException("Gift Card barcode " + barcode + " does not exist");
        }
        return String.format("%s - %s", inventory.getProductCode(), inventory.getProductName());
    }

    @Override
    public List<GiftCardInventory> findInventoryBySeriesRange(String startingSeries, String endingSeries) {
        QGiftCardInventory qgci = QGiftCardInventory.giftCardInventory;
        return Lists.newArrayList(inventoryRepo.findAll(
                qgci.series.in(Long.valueOf(startingSeries), Long.valueOf(endingSeries))));
    }

    @Override
    public boolean validateCardByUserInventoryLocation(String startingSeries, String endingSeries) {
        if (codePropertiesService.getDetailInvLocationHeadOffice()
                .equalsIgnoreCase(getCurrentUserLocation())) {
            return true;
        }

        Set<String> gcLocations = countRepo.getStoreCodeIn(startingSeries, endingSeries);
        return gcLocations.contains(getCurrentUserLocation()) && gcLocations.size() == 1;
    }

    @Override
    public Map<String, GiftCardInventoryStatus> extractGiftCardStatus(String startingSeries, String endingSeries) {
        long starting = Long.parseLong(startingSeries);
        long ending = Long.parseLong(endingSeries);

        if (ending < starting) {
            throw new IllegalArgumentException("Series range is invalid!");
        }

        // TODO: remove to repo
        QGiftCardInventory qg = QGiftCardInventory.giftCardInventory;
        Map<String, GiftCardInventoryStatus> map = new JPAQuery(em).from(qg)
                .where(qg.series.in(starting, ending)
                        .and(qg.status.notIn(GiftCardInventoryStatus.IN_STOCK, GiftCardInventoryStatus.MISSING)))
                .map(qg.barcode.substring(0, 16), qg.status);

        return map;
    }

    @Override
    public Long countInventory(String startingSeries, String endingSeries) {
        long starting = Long.parseLong(startingSeries);
        long ending = Long.parseLong(endingSeries);

        QGiftCardInventory qg = QGiftCardInventory.giftCardInventory;
        return inventoryRepo.count(
                qg.series.goe(starting)
                .and(qg.series.loe(ending)
                        .and(qg.allocateTo.isNull())
                        .and(qg.status.in(GiftCardInventoryStatus.IN_STOCK, GiftCardInventoryStatus.MISSING))
                        .and(qg.location.equalsIgnoreCase(getCurrentUserLocation()))));
    }

    @Override
    public ResultList<History> getHistoryList(PageSortDto pageDto) {
        QGiftCardPhysicalCountSummary qpcs = QGiftCardPhysicalCountSummary.giftCardPhysicalCountSummary;
        JPAQuery query = new JPAQuery(em).from(qpcs)
                .where(qpcs.location.eq(getCurrentUserLocation()))
                .distinct()
                .orderBy(qpcs.created.desc());

        final PagingParam pagination = pageDto.getPagination();
        Long totalElements =  new JPAQuery(em).from(qpcs)
                .where(qpcs.location.eq(getCurrentUserLocation()))
                .uniqueResult(qpcs.created.countDistinct());
        
        SpringDataPagingUtil.INSTANCE.applyPagination(query, pagination, GiftCardPhysicalCountSummary.class);
        
        List<History> list = query.distinct().list(Projections.bean(History.class, qpcs.created.as("timePresentation")));
        return new ResultList<History>(list, totalElements, pagination.getPageNo(), pagination.getPageSize());
    }

    public static class ReportBean {

        private List<GiftCardPhysicalCountSummaryDto> summary;
        private List<Entry> details;

        public ReportBean summary(List<GiftCardPhysicalCountSummaryDto> summary) {
            this.summary = summary;
            return this;
        }

        public ReportBean details(List<Entry> details) {
            this.details = details;
            return this;
        }

        public List<GiftCardPhysicalCountSummaryDto> getSummary() {
            return summary;
        }

        public List<Entry> getDetails() {
            return details;
        }

        public static class Entry {

            private String productProfile;
            private List<String> missingItems;
            private List<String> foundItems;

            public Entry productProfile(String profile) {
                this.productProfile = profile;
                return this;
            }

            public Entry missingItems(List<String> missingItems) {
                this.missingItems = missingItems;
                return this;
            }

            public Entry foundItems(List<String> foundItems) {
                this.foundItems = foundItems;
                return this;
            }

            /**
             * @return the productProfile
             */
            public String getProductProfile() {
                return productProfile;
            }

            public List<String> getMissingItems() {
                return missingItems;
            }

            public List<String> getFoundItems() {
                return foundItems;
            }
        }
    }

    private static final String REPORT_NAME = "reports/giftcard_physical_inventory_report.jasper";
}
