package com.transretail.crm.core.service.impl;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import com.mysema.query.Tuple;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.google.common.collect.Lists;
import com.mysema.query.jpa.JPQLQuery;
import com.mysema.query.jpa.impl.JPAQuery;
import com.mysema.query.types.Expression;
import com.mysema.query.types.OrderSpecifier;
import com.mysema.query.types.expr.BooleanExpression;
import com.mysema.query.types.path.EntityPathBase;
import com.transretail.crm.common.service.dto.request.SearchFormDto;
import com.transretail.crm.common.service.dto.response.NameIdPairDto;
import com.transretail.crm.common.service.dto.response.NameIdPairResultList;
import com.transretail.crm.common.service.util.SpringDataPagingUtil;
import com.transretail.crm.core.service.NameIdPairService;

/**
 *
 */
@Service("nameIdPairService")
public class NameIdPairServiceImpl implements NameIdPairService {
    @PersistenceContext
    private EntityManager em;

    @Override
    @Transactional(readOnly = true)
    public NameIdPairResultList getNameIdPairs(SearchFormDto searchForm, EntityPathBase<?> qModel, Expression<?> idExp,
        Expression<?> nameExp, OrderSpecifier<?> orderByExp) {
        JPQLQuery query = null;
        JPQLQuery countQuery = null;
        Pageable pageable = null;
        if (searchForm == null) {
            query = createQuery(qModel);
            countQuery = createQuery(qModel);
        } else {
            query = createQuery(searchForm, qModel);
            countQuery = createQuery(searchForm, qModel);
            pageable = SpringDataPagingUtil.INSTANCE.createPageable(searchForm.getPagination());
            query.offset(pageable.getOffset());
            query.limit(pageable.getPageSize());
        }
        
        if(orderByExp != null)
        	query.orderBy(orderByExp);

        List<Tuple> rows = query.list(idExp, nameExp);
        if (rows.isEmpty()) {
            return NameIdPairResultList.emptyList();
        }

        List<NameIdPairDto> results = Lists.newArrayList();
        for (Tuple tuple : rows) {
            results.add(new NameIdPairDto(tuple.get(idExp).toString(), tuple.get(nameExp).toString()));
        }

        return new NameIdPairResultList(new PageImpl<NameIdPairDto>(results, pageable, countQuery.count()));
    }

    private JPAQuery createQuery(EntityPathBase<?> qModel) {
        JPAQuery query = new JPAQuery(em);
        query = query.from(qModel);
        return query;
    }

    private JPAQuery createQuery(SearchFormDto searchForm, EntityPathBase<?> qModel) {
        JPAQuery query = new JPAQuery(em);
        query = query.from(qModel);
        if (searchForm != null) {
            BooleanExpression filter = searchForm.createSearchExpression();
            if (filter != null) {
                query = query.where(filter);
            }
        }
        return query;
    }
}
