package com.transretail.crm.loyalty.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.mysema.query.types.expr.BooleanExpression;
import com.transretail.crm.common.service.dto.response.ResultList;
import com.transretail.crm.common.service.util.SpringDataPagingUtil;
import com.transretail.crm.core.entity.lookup.LookupDetail;
import com.transretail.crm.core.entity.lookup.QLookupDetail;
import com.transretail.crm.core.repo.LookupDetailRepo;
import com.transretail.crm.core.repo.LookupHeaderRepo;
import com.transretail.crm.core.service.CodePropertiesService;
import com.transretail.crm.loyalty.dto.LoyaltyAnnualFeeSchemeDto;
import com.transretail.crm.loyalty.dto.LoyaltyAnnualFeeSchemeSearchDto;
import com.transretail.crm.loyalty.entity.LoyaltyAnnualFeeScheme;
import com.transretail.crm.loyalty.entity.LoyaltyCardInventory;
import com.transretail.crm.loyalty.entity.QLoyaltyAnnualFeeScheme;
import com.transretail.crm.loyalty.repo.LoyaltyAnnualFeeSchemeRepo;
import com.transretail.crm.loyalty.service.LoyaltyAnnualFeeService;
import com.transretail.crm.loyalty.service.LoyaltyCardService;

@Service("loyaltyAnnualFeeService")
public class LoyaltyAnnualFeeServiceImpl implements LoyaltyAnnualFeeService {

	@Autowired
	LoyaltyAnnualFeeSchemeRepo schemeRepo;
	@Autowired
	LoyaltyCardService loyaltyCardService;
	@Autowired
	LookupDetailRepo detailRepo;
	@Autowired
	CodePropertiesService codePropertiesService;
	
	@Override
	public void saveScheme(LoyaltyAnnualFeeSchemeDto schemeDto) {
		schemeRepo.save(schemeDto.toModel(new LoyaltyAnnualFeeScheme()));
	}
	
	@Override
	public void updateScheme(LoyaltyAnnualFeeSchemeDto schemeDto) {
		LoyaltyAnnualFeeScheme scheme = schemeRepo.findOne(schemeDto.getId());
		schemeRepo.save(schemeDto.toModel(scheme));
	}
	
	@Override
	public ResultList<LoyaltyAnnualFeeSchemeDto> listSchemes(LoyaltyAnnualFeeSchemeSearchDto sortDto) {
		Pageable pageable = SpringDataPagingUtil.INSTANCE.createPageable(sortDto.getPagination());
		BooleanExpression filter = sortDto.createSearchExpression();
        Page<LoyaltyAnnualFeeScheme> page = filter != null ? schemeRepo.findAll(filter, pageable) : schemeRepo.findAll(pageable);
        return new ResultList<LoyaltyAnnualFeeSchemeDto>(toDto(page.getContent()), page.getTotalElements(), page.hasPreviousPage(),
                page.hasNextPage());
	}
	
	@Override
	public void deleteScheme(Long id) {
		schemeRepo.delete(id);
	}
	
	@Override
	public LoyaltyAnnualFeeScheme getScheme(LookupDetail company, LookupDetail cardType) {
		QLoyaltyAnnualFeeScheme qScheme = QLoyaltyAnnualFeeScheme.loyaltyAnnualFeeScheme;
		return schemeRepo.findOne(qScheme.company.eq(company).and(qScheme.cardType.eq(cardType)));
	}
	
	@Override
	public LoyaltyAnnualFeeSchemeDto getScheme(Long id) {
		return new LoyaltyAnnualFeeSchemeDto(schemeRepo.findOne(id));
	}
	
	private List<LoyaltyAnnualFeeSchemeDto> toDto(Iterable<LoyaltyAnnualFeeScheme> schemes) {
		List<LoyaltyAnnualFeeSchemeDto> schemeDtos = new ArrayList<LoyaltyAnnualFeeSchemeDto>();
		for(LoyaltyAnnualFeeScheme scheme: schemes) {
			schemeDtos.add(new LoyaltyAnnualFeeSchemeDto(scheme));
		}
		return schemeDtos;
	}
	
	@Override
	public LoyaltyAnnualFeeSchemeDto getScheme(String barcode) {
		QLoyaltyAnnualFeeScheme qScheme = QLoyaltyAnnualFeeScheme.loyaltyAnnualFeeScheme;
		List<BooleanExpression> filters = new ArrayList<BooleanExpression>();
		String companyStr = barcode.substring(0, 2);
		String cardTypeStr = barcode.substring(2, 4);
		
		QLookupDetail qDetail = QLookupDetail.lookupDetail;
		
		LookupDetail company = detailRepo.findOne(qDetail.code.endsWith(companyStr).and(qDetail.header.code.eq(codePropertiesService.getHeaderCompany())));
		if(company != null)
			filters.add(qScheme.company.eq(company));
		else
			filters.add(qScheme.company.isNull());

		LookupDetail cardType = detailRepo.findOne(qDetail.code.endsWith(cardTypeStr).and(qDetail.header.code.eq(codePropertiesService.getHeaderMembershipTier())));
		if(cardType != null)
			filters.add(qScheme.cardType.eq(cardType));
		else
			filters.add(qScheme.cardType.isNull());
		
		LoyaltyAnnualFeeScheme scheme = schemeRepo.findOne(BooleanExpression.allOf(filters.toArray(new BooleanExpression[filters.size()])));
		
		if(scheme == null) {
			LoyaltyAnnualFeeSchemeDto schemeDto = new LoyaltyAnnualFeeSchemeDto();
			schemeDto.setAnnualFee(0D);
			schemeDto.setReplacementFee(0D);
			schemeDto.setCompany(company);
			schemeDto.setCardType(cardType);
			return schemeDto;
		}
		return new LoyaltyAnnualFeeSchemeDto(scheme);
	}
}
