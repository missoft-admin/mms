package com.transretail.crm.core.dto;


import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.joda.time.LocalDate;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.mysema.query.types.expr.BooleanExpression;
import com.transretail.crm.common.service.dto.request.AbstractSearchFormDto;
import com.transretail.crm.core.entity.MarketingChannel.ChannelStatus;
import com.transretail.crm.core.entity.MarketingChannel.ChannelType;
import com.transretail.crm.core.entity.QMarketingChannel;


public class MarketingChannelSearchDto extends AbstractSearchFormDto {

	private String code;
	private String description;
	private String createUser;
	private Long scheduleFrom;
	private Long scheduleTo;
	private ChannelStatus status;
	private ChannelType type;



	@Override
	@JsonIgnore
	public BooleanExpression createSearchExpression() {
		List<BooleanExpression> expr = new ArrayList<BooleanExpression>();
		QMarketingChannel channel = QMarketingChannel.marketingChannel;

		if ( StringUtils.isNotBlank( code ) ) {
			expr.add( channel.code.containsIgnoreCase( code ) );
		}
		if ( StringUtils.isNotBlank( description ) ) {
			expr.add( channel.description.containsIgnoreCase( description ) );
		}
		if ( StringUtils.isNotBlank( createUser ) ) {
			expr.add( channel.createUser.containsIgnoreCase( createUser ) );
		}
		if ( null != scheduleFrom &&  null == scheduleTo ) {
			expr.add( channel.schedule.goe( new LocalDate( scheduleFrom ) ) );
		}
		else if ( null == scheduleFrom &&  null != scheduleTo ) {
			expr.add( channel.schedule.loe( new LocalDate( scheduleTo ) ) );
		}
		else if ( null != scheduleFrom &&  null != scheduleTo ) {
			expr.add( channel.schedule.between( new LocalDate( scheduleFrom ), new LocalDate( scheduleTo ) ) );
		}
		if ( null != status ) {
			expr.add( channel.status.eq( status ) );
		}
		if ( null != type ) {
			expr.add( channel.type.eq( type ) );
		}

		return BooleanExpression.allOf( expr.toArray( new BooleanExpression[expr.size()] ) );
	}



	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public Long getScheduleFrom() {
		return scheduleFrom;
	}
	public void setScheduleFrom(Long scheduleFrom) {
		this.scheduleFrom = scheduleFrom;
	}
	public Long getScheduleTo() {
		return scheduleTo;
	}
	public void setScheduleTo(Long scheduleTo) {
		this.scheduleTo = scheduleTo;
	}
	public String getCreateUser() {
		return createUser;
	}
	public void setCreateUser(String createUser) {
		this.createUser = createUser;
	}
	public ChannelStatus getStatus() {
		return status;
	}
	public void setStatus(ChannelStatus status) {
		this.status = status;
	}
	public ChannelType getType() {
		return type;
	}
	public void setType(ChannelType type) {
		this.type = type;
	}

}
