alter table CRM_GC_RETURN_BY_CARDS_ITEM add (
BALANCE number(19,2),
FACE_AMT number(19,2),
STATUS varchar2(255 char),
PRODUCT number(19,0)
);