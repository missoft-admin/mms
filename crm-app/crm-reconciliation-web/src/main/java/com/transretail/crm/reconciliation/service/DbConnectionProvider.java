package com.transretail.crm.reconciliation.service;

import java.sql.Connection;
import java.sql.SQLException;

/**
 * Connection provider that would always return new connection when create connections methods are invoked. Callers of create connection methods must close the connection to avoid connection leak.
 *
 * @author Allan G. Ramirez (agramirez@exist.com)
 */
public interface DbConnectionProvider {
    /**
     * Create Main Db Connection
     *
     * @return Main Db Connection
     * @throws SQLException if connection cannot be established
     */
    Connection createMainDbDbConnection() throws SQLException;

    /**
     * Create db connection used by posinteg.
     *
     * @return db connection used by posinteg.
     * @throws SQLException if connection cannot be established
     */
    Connection createPosIntegDbConnection() throws SQLException;
}
