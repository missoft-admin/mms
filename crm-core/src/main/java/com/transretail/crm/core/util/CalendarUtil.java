package com.transretail.crm.core.util;

import java.util.Calendar;
import java.util.Date;

import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;


public enum CalendarUtil {
    INSTANCE;
    
    private static final Date MIN_DATE = new Date(0);
    private static final Date MAX_DATE = new Date(Long.MAX_VALUE);
    private static final DateTimeFormatter DATE_FORMATTER = DateTimeFormat.forPattern("dd MMM yyyy");
    
    public Date getFromDate(Date from) {
    	return (from == null) ? MIN_DATE : from; 
    }
    
    public Date getToDate(Date to) {
    	return (to == null) ? MAX_DATE : to;
    }
    
    public Calendar getPrevMonthFromCal( Calendar inCal ) {
    	Calendar theDate = getFromCal( inCal );
    	theDate.add( Calendar.MONTH, -1 );
    	theDate.getTime();
    	
    	return theDate;
    }
    
    public Calendar getPrevMonthToCal( Calendar inCal ) {
    	Calendar theDate = getFromCal( inCal );
    	theDate.add( Calendar.MILLISECOND, -1 );
    	theDate.getTime();
    	
    	return theDate;
    }
    
    public Calendar getFromCal( Calendar inCal ) {
    	
    	Calendar theDate = Calendar.getInstance();
    	if ( inCal == null ) {
        	theDate.set( Calendar.DATE, 1 );
    	}
    	else {
        	theDate.setTime( inCal.getTime() );
    	}
    	theDate.set( Calendar.HOUR_OF_DAY, 0 );
    	theDate.set( Calendar.MINUTE, 0 );
    	theDate.set( Calendar.SECOND, 0 );
    	theDate.set( Calendar.MILLISECOND, 0 );
    	theDate.getTime();
    	
    	return theDate;
    }
    
    public Calendar getToCal( Calendar inCal ) {
    	
    	Calendar theDate;
    	if ( inCal == null ) {
    		theDate = getFromCal( inCal );
    		theDate.add( Calendar.MONTH, 1 );
    	}
    	else {
    		theDate = getFromCal( inCal );
        	theDate.add( Calendar.DATE, 1 );
    	}
    	theDate.add( Calendar.MILLISECOND, -1 );
    	theDate.getTime();
    	
    	return theDate;
    }



    public Date getStartDate( Date inDate ) {

    	Calendar theCal = Calendar.getInstance();
    	theCal.setTime( inDate );
    	theCal.set( Calendar.HOUR_OF_DAY, 0 );
    	theCal.set( Calendar.MINUTE, 0 );
    	theCal.set( Calendar.SECOND, 0 );
    	theCal.set( Calendar.MILLISECOND, 0 );

    	return theCal.getTime();
    }

    public Date getEndDate( Date inDate ) {

    	Calendar theCal = Calendar.getInstance();
    	theCal.setTime( getStartDate( inDate ) );
    	theCal.add( Calendar.DATE, 1 );
    	theCal.add( Calendar.MILLISECOND, -1 );

    	return theCal.getTime();
    }

    public Date getPrevMonthStartDate() {

    	Calendar theCal = Calendar.getInstance();
    	theCal.add( Calendar.MONTH, -1 );
    	theCal.set( Calendar.DATE, 1 );

    	return getStartDate( theCal.getTime() );
    }

    public Date getPrevMonthEndDate() {

    	Calendar theCal = Calendar.getInstance();
    	theCal.set( Calendar.DATE, 1 );
    	theCal.setTime( getStartDate( theCal.getTime() ) );
    	theCal.add( Calendar.MILLISECOND, -1 );

    	return theCal.getTime();
    }

    public Date getNextDate( Date inDate ) {

    	Calendar theCal = Calendar.getInstance();
    	theCal.setTime( getStartDate( inDate ) );
    	theCal.add( Calendar.DATE, 1 );

    	return theCal.getTime();
    }
    
    public DateTimeFormatter getDateFormatter() {
    	return DATE_FORMATTER;
    }
}
