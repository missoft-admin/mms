package com.transretail.crm.core.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;

import com.transretail.crm.core.entity.support.CustomAuditableEntity;

@Entity
@Table(name="CRM_NOTIFICATION_STATUS")
public class NotificationStatus extends CustomAuditableEntity<Long> implements Serializable {
	
	@Column(name="READ")
	private boolean read;
	
	@ManyToOne
	@JoinColumn(name="USER_ID")
	private UserModel user;
	
	@ManyToOne
	@JoinColumn(name="NOTIFICATION")
	private Notification notification;

	public boolean isRead() {
		return read;
	}

	public void setRead(boolean read) {
		this.read = read;
	}

	public UserModel getUser() {
		return user;
	}

	public void setUser(UserModel user) {
		this.user = user;
	}

	public Notification getNotification() {
		return notification;
	}

	public void setNotification(Notification notification) {
		this.notification = notification;
	}
}
