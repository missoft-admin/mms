package com.transretail.crm.giftcard.entity;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.transretail.crm.core.entity.support.CustomAuditableEntity;
import com.transretail.crm.giftcard.entity.support.GiftCardInventoryStatus;

/**
 *
 */
@Entity
@Table(name = "CRM_GC_RETURN_BY_CARDS_ITEM")
public class ReturnByCardsItem extends CustomAuditableEntity<Long> implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "REC")
	private ReturnRecord record;
	
	@Column(name = "STARTING_SERIES")
	private String startingSeries;
	@Column(name = "ENDING_SERIES")
	private String endingSeries;
	
	@Column(name = "TYPE")
	@Enumerated(EnumType.STRING)
	private  ItemType type;
	
	@ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "PRODUCT")
	private ProductProfile product;
	
	@Column(name = "BALANCE")
	private BigDecimal balance;
	
	@Column(name = "FACE_AMT")
	private BigDecimal faceAmount;
	
	@Column(name = "STATUS")
	@Enumerated(EnumType.STRING)
	private GiftCardInventoryStatus status;
	
	


	public ReturnRecord getRecord() {
		return record;
	}

	public void setRecord(ReturnRecord record) {
		this.record = record;
	}

	public String getStartingSeries() {
		return startingSeries;
	}

	public void setStartingSeries(String startingSeries) {
		this.startingSeries = startingSeries;
	}

	public String getEndingSeries() {
		return endingSeries;
	}

	public void setEndingSeries(String endingSeries) {
		this.endingSeries = endingSeries;
	}

	public ItemType getType() {
		return type;
	}

	public void setType(ItemType type) {
		this.type = type;
	}



	public static enum ItemType {
		RETURN, REPLACEMENT
	}



	public ProductProfile getProduct() {
		return product;
	}

	public void setProduct(ProductProfile product) {
		this.product = product;
	}

	public BigDecimal getBalance() {
		return balance;
	}

	public void setBalance(BigDecimal balance) {
		this.balance = balance;
	}

	public BigDecimal getFaceAmount() {
		return faceAmount;
	}

	public void setFaceAmount(BigDecimal faceAmount) {
		this.faceAmount = faceAmount;
	}

	public GiftCardInventoryStatus getStatus() {
		return status;
	}

	public void setStatus(GiftCardInventoryStatus status) {
		this.status = status;
	}

	
}
