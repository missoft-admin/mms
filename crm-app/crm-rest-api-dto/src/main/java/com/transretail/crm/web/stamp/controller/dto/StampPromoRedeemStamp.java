package com.transretail.crm.web.stamp.controller.dto;


import com.fasterxml.jackson.annotation.JsonInclude;


@JsonInclude(JsonInclude.Include.NON_NULL)
public class StampPromoRedeemStamp {

	private String promoID;
	private Long redeemedStamps;
	private Long availableStamps;
	private Long remainingStamps;



	public String getPromoID() {
		return promoID;
	}
	public void setPromoID(String promoID) {
		this.promoID = promoID;
	}
	public Long getRedeemedStamps() {
		return redeemedStamps;
	}
	public void setRedeemedStamps(Long redeemedStamps) {
		this.redeemedStamps = redeemedStamps;
	}
	public Long getAvailableStamps() {
		return availableStamps;
	}
	public void setAvailableStamps(Long availableStamps) {
		this.availableStamps = availableStamps;
	}
	public Long getRemainingStamps() {
		return remainingStamps;
	}
	public void setRemainingStamps(Long remainingStamps) {
		this.remainingStamps = remainingStamps;
	}

}
