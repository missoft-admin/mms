var ComplaintForm = null;

$(document).ready( function() {

	var errorContainer = $( "#content_complaintForm" ).find( "#content_error" );

	var $dialog = $( "#content_complaintForm" );
	var $form = $( "#complaintForm" );
	var $dateTime = $form.find( "#dateTime" );
	var $mobileNo = $form.find( "#mobileNo" );
	var $email = $form.find( "#email" );
	var $firstName = $form.find( "#firstName" );
	var $lastName = $form.find( "#lastName" );
	var $complaints = $form.find( "#complaints" );
	var $complaintsCount = $form.find( "#complaintsCount" );
	var $genderCode = $form.find( "#genderCode" );
	var $dispDate = $form.find( "#content_date > .controls > div" );
	var $save = $form.find( "#saveBtn" );



	initDialog();
	initFormFields();



	function initDialog() {
		$dialog.modal({ show : false });
		$dialog.on( "hide", function(e) { 
			if ( e.target === this ) { 
				errorContainer.hide();
				ComplaintCommon.reset( $form );
			}
		});
		$dialog.on( "show", function(e) { 
			if ( e.target === this ) {
				if ( $dateTime.val() ) {
					$dispDate.html( new Date( $dateTime.val() - 0 ).customize( 0 ) );
				}
				else {
					$dispDate.html( new Date().customize( 0 ) );
					$dateTime.val( new Date().getTime() );
				}
			}
		});
	}

	function initFormFields() {
		
		$form.submit( save );
		$save.click(function() {
			$form.submit();
		});

		$mobileNo.change( function(e) {
			$.get( 
				$(this).data( "url" ).replace( new RegExp( "%MOBILENO%", "g" ), $(this).val() )
					.replace( new RegExp( "%EMAIL%", "g" ), "" ), 
				function( resp ) {
					populateMemberFields( resp.result, $mobileNo );
			}, "json");
		});

		$email.change( function(e) {
			$.get( 
				$(this).data( "url" ).replace( new RegExp( "%EMAIL%", "g" ), $(this).val() )
					.replace( new RegExp( "%MOBILENO%", "g" ), "" ), 
				function( resp ) {
					populateMemberFields( resp.result, $email );
			}, "json");
		});

		var maxCount = $complaints.data( "count" ) - 0;
		$complaints.unbind( "keyup" ).bind( "keyup", function(e) {
              var length = $(this).val().length;
              return ComplaintCommon.limitCharSize( e, length, maxCount, function() { 
            	  $complaintsCount.html( maxCount - length );
              });
        });
		$complaints.unbind( $.browser.opera ? "keypress" : "keydown" ).bind( $.browser.opera ? "keypress" : "keydown", function(e) {
              var length = $(this).val().length;
              return ComplaintCommon.limitCharSize( e, length, maxCount - 1, function() { 
                  $complaintsCount.html( maxCount - length );
              });
        });
		$complaints.trigger( "keyup" ).trigger( $.browser.opera ? "keypress" : "keydown" );
	}



	function save(e) {
		$save.prop( "disabled", true );

		e.preventDefault();
		$.post( $form.attr( "action" ), $form.serialize(), function( resp ) { 
			ComplaintCommon.processResp( resp, $dialog, errorContainer, $save ); 
			ComplaintList.reloadTable();
		}, "json" );
	}

	function populateMemberFields( member, field ) {
		if ( member ) {
			if ( $mobileNo !== field ) {
				$mobileNo.val( ( member.mobileNo )? member.mobileNo : "" );
			}
			if ( $email !== field ) {
				$email.val( ( member.email )? member.email : "" );
			}
			$firstName.val( ( member.firstName )? member.firstName : "" );
			//$firstName.prop( "readonly", ( member.firstName )? true : false );
			$lastName.val( ( member.lastName )? member.lastName : "" );
			//$lastName.prop( "readonly", ( member.lastName )? true : false );
			$genderCode.val( ( member.genderCode )? member.genderCode : "" );
		}
	}



	ComplaintForm = {
		show	: function( ) {
			$dialog.modal( "show" );
		},
		create	: function( action, modalHeader ) {
			$form.attr( "action", action );
			$form.find( ".modal-header > span" ).html( modalHeader );
			$dialog.modal( "show" );
		}
	};

});