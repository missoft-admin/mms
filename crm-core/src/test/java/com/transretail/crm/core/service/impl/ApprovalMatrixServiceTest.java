package com.transretail.crm.core.service.impl;

import java.util.List;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Configurable;
import org.springframework.security.authentication.TestingAuthenticationToken;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.context.SecurityContextImpl;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import com.google.common.collect.Lists;
import com.transretail.crm.core.dto.ServiceDto;
import com.transretail.crm.core.entity.ApprovalMatrixModel;
import com.transretail.crm.core.entity.UserModel;
import com.transretail.crm.core.entity.enums.ModelType;
import com.transretail.crm.core.entity.lookup.Store;
import com.transretail.crm.core.repo.ApprovalMatrixRepo;
import com.transretail.crm.core.repo.StoreRepo;
import com.transretail.crm.core.repo.UserRepo;
import com.transretail.crm.core.security.model.impl.CustomSecurityUserDetailsImpl;
import com.transretail.crm.core.service.ApprovalMatrixService;



@Configurable
@Transactional
@ContextConfiguration(locations = "classpath*:/META-INF/spring/applicationContext*.xml")
@RunWith(SpringJUnit4ClassRunner.class)
public class ApprovalMatrixServiceTest {

    @Autowired
	UserRepo userRepo;
    @Autowired
	ApprovalMatrixRepo approvalMatrixRepo;
    @Autowired
    ApprovalMatrixService approvalMatrixService;


    static UserModel theLoggedIn;
    static UserModel theOtherUser;


	@Before
    public void setupCurrentUser() {
    	theLoggedIn = new UserModel();
    	theLoggedIn.setUsername( "loggedInUser" );
    	theLoggedIn.setPassword( "" );
    	theLoggedIn.setEnabled( true );
    	theLoggedIn.setStoreCode( "store1" );
    	userRepo.save( theLoggedIn );

    	theOtherUser = new UserModel();
    	theOtherUser.setUsername( "otherUser" );
    	theOtherUser.setPassword( "" );
    	theOtherUser.setEnabled( true );
    	theOtherUser.setStoreCode( "store1" );
    	userRepo.save( theOtherUser );
   
        CustomSecurityUserDetailsImpl user = new CustomSecurityUserDetailsImpl();
        user.setUserId( theLoggedIn.getId() );
        user.setUsername( theLoggedIn.getUsername() );
        List<GrantedAuthority> authorities = Lists.newArrayList();
        authorities.add(new SimpleGrantedAuthority("ROLE_CSS"));
        user.setAuthorities(authorities);

        SecurityContextImpl securityContext = new SecurityContextImpl();
        securityContext.setAuthentication(new TestingAuthenticationToken(user, ""));
        SecurityContextHolder.setContext(securityContext);
    }

    @AfterClass
    public static void removeCurrentUser() {
        SecurityContextHolder.clearContext();
    }


    @SuppressWarnings("unchecked")
    @Test
	public void testRetrieveAdjustPointsApprovalMatrix() {
    	setupApprovalMatrix();

    	ServiceDto theMatrices = approvalMatrixService.retrieveAdjustPointsApprovalMatrix();
    	Assert.assertTrue( 3 == ( (List<ApprovalMatrixModel>)theMatrices.getReturnObj() ).size() );
    }

    @Test
    public void testFindHighestApprovalPointsOfCurrentUser() {
    	setupApprovalMatrix();

    	Long theLoggedInApprovalAmt = approvalMatrixService.findHighestApprovalPointsOfCurrentUser();
    	Assert.assertTrue( 100L == theLoggedInApprovalAmt );
    }


    private void setupApprovalMatrix() {
    	ApprovalMatrixModel thePointsMatrix = new ApprovalMatrixModel();
    	thePointsMatrix.setModelType( ModelType.POINTS );
    	thePointsMatrix.setAmount( 100D );
    	thePointsMatrix.setUser( theLoggedIn );
    	approvalMatrixRepo.save( thePointsMatrix );

    	thePointsMatrix = new ApprovalMatrixModel();
    	thePointsMatrix.setModelType( ModelType.POINTS );
    	thePointsMatrix.setAmount( 50D );
    	thePointsMatrix.setUser( theLoggedIn );
    	approvalMatrixRepo.save( thePointsMatrix );

    	thePointsMatrix = new ApprovalMatrixModel();
    	thePointsMatrix.setModelType( ModelType.POINTS );
    	thePointsMatrix.setUser( theOtherUser );
    	thePointsMatrix.setAmount( 150D );
    	approvalMatrixRepo.save( thePointsMatrix );

    	thePointsMatrix = new ApprovalMatrixModel();
    	thePointsMatrix.setModelType( ModelType.PURCHASETXN );
    	thePointsMatrix.setUser( theOtherUser );
    	approvalMatrixRepo.save( thePointsMatrix );
    }

}
