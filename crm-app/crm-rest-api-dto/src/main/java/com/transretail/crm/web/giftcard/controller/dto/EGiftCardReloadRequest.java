package com.transretail.crm.web.giftcard.controller.dto;




public class EGiftCardReloadRequest extends PrepaidReloadRequest {

    private String email;
    private String mobileNo;



	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getMobileNo() {
		return mobileNo;
	}
	public void setMobileNo(String mobileNo) {
		this.mobileNo = mobileNo;
	}

}
