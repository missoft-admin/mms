package com.transretail.crm.report.template.impl;

import com.google.common.base.Optional;
import com.google.common.collect.Iterables;
import com.google.common.collect.Lists;
import com.google.common.collect.Sets;
import com.mysema.query.BooleanBuilder;
import com.mysema.query.jpa.impl.JPAQuery;
import com.mysema.query.support.Expressions;
import com.mysema.query.types.*;
import com.mysema.query.types.expr.StringExpression;
import com.transretail.crm.common.reporting.jasper.ClasspathFileResolver;
import com.transretail.crm.common.reporting.jasper.DefaultJRProcessor;
import com.transretail.crm.common.reporting.jasper.JRProcessor;
import com.transretail.crm.core.entity.QMemberModel;
import com.transretail.crm.core.entity.QPointsTxnModel;
import com.transretail.crm.core.entity.enums.ReportType;
import com.transretail.crm.core.entity.lookup.QLookupDetail;
import com.transretail.crm.core.entity.lookup.Store;
import com.transretail.crm.core.service.StoreService;
import com.transretail.crm.report.template.*;
import com.transretail.crm.report.template.FilterField.DateTypeOption;
import java.util.List;
import java.util.Map;
import java.util.Set;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.apache.commons.lang3.StringUtils;
import org.joda.time.YearMonth;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author Monte Cillo Co (mco@exist.com)
 */
@Service("customerCountReport")
public class CustomerCountReport implements ReportTemplate, NoDataFoundSupport {

    @Autowired
    private LOVFactory factory;
    @PersistenceContext
    private EntityManager em;
    @Autowired
    private StoreService storeService;

    @Override
    public String getName() {
        return "customer-count";
    }

    @Override
    public String getDescription() {
        return "Customer Report";
    }

    @Override
    public Set<FilterField> getFilters() {
        Set<FilterField> filters = Sets.newLinkedHashSet();
        filters.add(FilterField.createDateField(CommonReportFilter.YEAR, "Year", "yyyy", DateTypeOption.YEAR, true));
        filters.add(FilterField.createDropdownField(CommonReportFilter.STORE, "Store", factory.getStoreOptions()));
        return filters;
    }

    @Override
    public boolean canView(Set<String> permissions) {
        return permissions.contains("CUSTOMER_REPORT_PERMISSION");
    }

    @Override
    @Transactional(readOnly = true)
    public JRProcessor createJrProcessor(Map<String, String> parameters) {
        String handler = parameters.get(CommonReportFilter.YEAR);
        int year = Integer.valueOf(handler);
        String storeCode = parameters.get(CommonReportFilter.STORE);
        if (StringUtils.isBlank(storeCode)) {
            storeCode = null;
        }

        List<ReportBean> registeredCustomers = retrieveRegisteredCustomers(year, Optional.fromNullable(storeCode));
        List<ReportBean> activeCustomers = retrieveActivedCustomers(year, Optional.fromNullable(storeCode));
        List<ReportBean> newCustomers = retrieveNewCustomers(year, Optional.fromNullable(storeCode));

        Iterable<ReportBean> rds = Iterables.concat(registeredCustomers, activeCustomers, newCustomers);

        DefaultJRProcessor processor = new DefaultJRProcessor("reports/customer-count-report.jrxml", Lists.newArrayList(rds));
        processor.addParameter("YEAR", year);
        if (StringUtils.isNotBlank(storeCode)) {
            Store store = storeService.getStoreByCode(storeCode);
            String storeName = store == null ? "" : store.getName();
            processor.addParameter("STORE", storeName);
        }
        processor.setFileResolver(new ClasspathFileResolver("reports"));

        return processor;
    }

    @Override
    public ReportType[] getReportTypes() {
        return ReportType.values();
    }

    private List<ReportBean> retrieveRegisteredCustomers(int year, Optional<String> storeCode) {
        QMemberModel qm = QMemberModel.memberModel;
        QLookupDetail qdtl = QLookupDetail.lookupDetail;
        final int january = (year * 100) + 1;
        BooleanBuilder booleanBuilder = new BooleanBuilder(qdtl.header.code.eq("CMP001"));
        if (storeCode.isPresent()) {
            booleanBuilder.and(qm.registeredStore.eq(storeCode.get()));
        }

        List<ReportBean> result = Lists.newArrayListWithExpectedSize(12);
        for (int i = 1; i <= 12; i++) {
            final int targetMonth = (year * 100) + i;
            List<ReportBean> list = new JPAQuery(em).from(qm)
                    .leftJoin(qm.professionalProfile.customerGroup, qdtl)
                    .where(qm.created.yearMonth()
                            .between(january, targetMonth).and(booleanBuilder))
                    .groupBy(qdtl.description)
                    .list(Projections.bean(ReportBean.class,
                                    qdtl.description.as("row"),
                                    qm.count().as("value"),
                                    constant("registered").as("group")));

            if (list.isEmpty()) {
                // fill missing months
                List<ReportBean> filler = new JPAQuery(em).from(qdtl)
                        .where(qdtl.header.code.eq("CMP001"))
                        .list(Projections.bean(ReportBean.class,
                                        qdtl.description.as("row"),
                                        constant("registered").as("group")));
                list.addAll(filler);
            }

            for (ReportBean r : list) {
                r.setYearMonthHandler(targetMonth);
            }

            result.addAll(list);
        }

        return result;
    }

    private StringExpression constant(String constantValue) {
        return Expressions.stringTemplate(String.format("'%s'", constantValue));
    }

    private List<ReportBean> retrieveActivedCustomers(int year, Optional<String> storeCode) {
        QPointsTxnModel qp = QPointsTxnModel.pointsTxnModel;
        QMemberModel qm = QMemberModel.memberModel;
        QLookupDetail qdtl = QLookupDetail.lookupDetail;
        BooleanBuilder booleanBuilder = new BooleanBuilder(qdtl.header.code.eq("CMP001"))
                .and(qp.transactionDateTime.year().eq(year));

        if (storeCode.isPresent()) {
            booleanBuilder.and(qm.registeredStore.eq(storeCode.get()));
        }

        List<ReportBean> result = new JPAQuery(em).from(qp)
                .join(qp.memberModel, qm)
                .leftJoin(qm.professionalProfile.customerGroup, qdtl)
                .where(booleanBuilder)
                .groupBy(qdtl.description, qp.transactionDateTime.yearMonth())
                .list(Projections.bean(ReportBean.class,
                                qdtl.description.as("row"),
                                qm.count().as("value"),
                                qp.transactionDateTime.yearMonth().as("yearMonthHandler"),
                                constant("active").as("group")));

            // fill missing months and customer group
            List<ReportBean> filler = new JPAQuery(em).from(qdtl)
                    .where(qdtl.header.code.eq("CMP001"))
                    .list(Projections.bean(ReportBean.class,
                                    qdtl.description.as("row"),
                                    constant("active").as("group")));
            List<ReportBean> tmp = Lists.newArrayList();
            for (int i = 1; i <= 12; i++) {
                final int targetMonth = (year * 100) + i;
                for (ReportBean f : filler) {
                    f.setYearMonthHandler(targetMonth);
                    ReportBean reportBean = new ReportBean();
                    BeanUtils.copyProperties(f, reportBean);
                    tmp.add(reportBean);
                }
            }

            result.addAll(tmp);

        return result;
    }

    private List<ReportBean> retrieveNewCustomers(int year, Optional<String> storeCode) {
        QMemberModel qm = QMemberModel.memberModel;
        QLookupDetail qdtl = QLookupDetail.lookupDetail;
        BooleanBuilder booleanBuilder = new BooleanBuilder(qdtl.header.code.eq("CMP001"))
                .and(qm.created.year().eq(year));

        if (storeCode.isPresent()) {
            booleanBuilder.and(qm.registeredStore.eq(storeCode.get()));
        }

        List<ReportBean> result = new JPAQuery(em).from(qm)
                .leftJoin(qm.professionalProfile.customerGroup, qdtl)
                .where(booleanBuilder)
                .groupBy(qdtl.description, qm.created.yearMonth())
                .list(Projections.bean(ReportBean.class,
                                qdtl.description.as("row"),
                                qm.count().as("value"),
                                qm.created.yearMonth().as("yearMonthHandler"),
                                constant("new").as("group")));
        
            // fill missing months and customer groups
            List<ReportBean> filler = new JPAQuery(em).from(qdtl)
                    .where(qdtl.header.code.eq("CMP001"))
                    .list(Projections.bean(ReportBean.class,
                                    qdtl.description.as("row"),
                                    constant("new").as("group")));
            List<ReportBean> tmp = Lists.newArrayList();
            for (int i = 1; i <= 12; i++) {
                final int targetMonth = (year * 100) + i;
                for (ReportBean f : filler) {
                    f.setYearMonthHandler(targetMonth);
                    ReportBean reportBean = new ReportBean();
                    BeanUtils.copyProperties(f, reportBean);
                    tmp.add(reportBean);
                }
            }

            result.addAll(tmp);

        return result;
    }

    @Override
    public boolean isEmpty(Map<String, String> filter) {
        return false;
    }

    public static class ReportBean {

        private String row;
        private long value;
        private YearMonth yearMonth;
        private String group;
        private int yearMonthHandler;

        public ReportBean() {
        }

        public ReportBean(String row, long value, YearMonth yearMonth, String group) {
            this.row = row;
            this.value = value;
            this.yearMonth = yearMonth;
            this.group = group;
        }

        public int getYearMonthHandler() {
            return yearMonthHandler;
        }

        public void setYearMonthHandler(int yearMonthHandler) {
            this.yearMonthHandler = yearMonthHandler;
            this.yearMonth = ReportDateFormat.getYearMonth(yearMonthHandler);
        }

        public void setRow(String row) {
            this.row = row;
        }

        public void setValue(long value) {
            this.value = value;
        }

        public void setYearMonth(YearMonth yearMonth) {
            this.yearMonth = yearMonth;
        }

        public void setGroup(String group) {
            this.group = group;
        }

        public String getRow() {
            return row;
        }

        public long getValue() {
            return value;
        }

        public YearMonth getYearMonth() {
            return yearMonth;
        }

        public String getGroup() {
            return group;
        }

    }
}
