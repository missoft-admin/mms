<%@ include file="../../common/taglibs.jsp" %>

<style type="text/css">
<!--

  #productTable tr td:last-child {
    border-left: 0px;
  }

  #addProductBtn.btn-small {
    margin-bottom: 5px;
  }
  table select, textarea, table input[type="text"], input[type="password"], input[type="datetime"], input[type="datetime-local"], input[type="date"], input[type="month"], input[type="time"], input[type="week"], input[type="number"], input[type="email"], input[type="url"], input[type="search"], input[type="tel"], input[type="color"], .uneditable-input {
      margin-bottom: 0px;
      margin-top: 0px;
  }
  
-->
</style>
<script src="<spring:url value="/js/common/typeaheadutil.js" />"></script>



<c:set var="ENCTYPE" value="application/x-www-form-urlencoded"/>

<div class="modal-dialog">
  <div class="modal-content">
    
    <div class="modal-header">
      <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
      <h4><spring:message code="gc_replacement"/></h4>
    </div>
 
    <div class="modal-body">
    
      <div id="contentError" class="hide alert alert-error">
        <button type="button" class="close" onclick="$(this).parent().hide();">&times;</button>
        <div><form:errors path="*"/></div>
      </div>
    
      
      <form:form id="orderForm" name="orderForm" modelAttribute="orderForm" action="${pageContext.request.contextPath}/giftcard/salesorder/save/" method="POST" enctype="${ENCTYPE}" cssClass="form-reset form-horizontal" >
      <form:hidden path="id"/>
      <form:hidden path="orderType" />
      
      <div class="contentMain">
      
      <div class="row-fluid">
      
        
      
        <div class="span6">
        <div class="control-group">
          <spring:message code="sales_order_no" var="orderNum" />
          <label for="orderNo" class="control-label">${orderNum}</label> 
          <div class="controls">
            <form:input path="orderNo" class="form-control" placeholder="${orderNum}" readonly="true" />
          </div>
        </div>
        
        
        <div class="control-group">
          <spring:message code="sales_order_type" var="orderTyp" />
          <label for="orderNo" class="control-label">${orderTyp}</label> 
          <div class="controls">
            <input type="text" value="${orderForm.orderType}" class="form-control" disabled="disabled" />
          </div>
        </div>
        
        <div class="control-group">
          <spring:message code="sales_order_date" var="orderDt" />
          <label for="orderDate" class="control-label">${orderDt}</label> 
          <div class="controls">
            <form:input path="orderDate" class="form-control orderDate" placeholder="${orderDt}" />
          </div>
        </div>
        
        <div class="control-group">
          <spring:message code="sales_order_notes" var="notes" />
          <label for="customerId" class="control-label">${notes}</label> 
          <div class="controls">
            <form:textarea path="notes" class="form-control"  />
          </div>
        </div>
        
        <div class="control-group">
          <spring:message code="order_total_face_amount" var="totalFaceAmount" />
          <label for="totalFaceAmount" class="control-label">${totalFaceAmount}</label> 
          <div class="controls">
            <%-- <form:input path="totalFaceAmount" id="totalFaceAmount" class="form-control" placeholder="${totalFaceAmount}" readonly="true" /> --%>
            <form:input path="totalFaceAmountFmt" id="totalFaceAmountFmt" class="form-control" placeholder="${totalFaceAmount}" disabled="true" />
            <form:hidden path="totalFaceAmount" id="totalFaceAmount" />
          </div>
        </div>
        
        <div class="control-group">
          <spring:message code="sales_order_replacementfee" var="replacementFee" />
          <label for="totalFaceAmount" class="control-label"><b class="required">*</b>${replacementFee}</label> 
          <div class="controls">
            <form:input path="handlingFee" class="form-control floatInput" placeholder="${replacementFee}"  />
          </div>
        </div>
        
        
        </div>
        
        <div class="span6">
        
        <div class="control-group">
          <spring:message code="gc_return_no" var="returnNo" />
          <label for="returnNo" class="control-label"><b class="required">*</b>${returnNo}</label> 
          <div class="controls">
            <form:input path="returnNo" class="form-control" placeholder="${returnNo}" id="returnNo" />
          </div>
        </div>
        
        <div class="control-group">
          <spring:message code="sales_original_order_no" var="origOrderNo" />
          <label class="control-label">${origOrderNo}</label> 
          <div class="controls">
            <form:input path="origOrderNo" id="soOrderNo" class="form-control" placeholder="${origOrderNo}" readonly="true" />
          </div>
        </div>
        
        <div class="control-group">
          <spring:message code="sales_original_order_date" var="origOrderDate" />
          <label class="control-label">${origOrderDate}</label> 
          <div class="controls">
            <form:input path="origOrderDate" id="soOrderDate" class="form-control" placeholder="${origOrderDate}" readonly="true" />
          </div>
        </div>
        
        <div class="control-group">
          <spring:message code="gc_return_amount" var="returnAmount" />
          <label class="control-label">${returnAmount}</label> 
          <div class="controls">
            <form:input path="returnAmount" id="returnAmount" class="form-control" placeholder="${returnAmount}" disabled="true" />
          </div>
        </div>
        
        <div class="control-group">
          <spring:message code="gc_return_customer_name" var="customerName" />
          <label class="control-label">${customerName}</label> 
          <div class="controls">
            <form:input path="customerDesc" id="customerDesc" class="form-control" placeholder="${customerName}" disabled="true" />
          </div>
        </div>
        
        
        
        <div class="control-group">
          <div class="controls checkbox">
            <form:checkbox path="paidReplacement" class="form-control" /><label><spring:message code="order_paid_replacement" /></label>
          </div>
        </div>
        
        </div>
        
        <div class="clearfix"></div>
        
        <div class="span12">
        
        <div class="pull-left" style="margin-right: 5px;">
        <button type="button" id="addProductBtn" class="btn btn-small">+</button><br/>
        <button type="button" id="removeProductBtn" class="btn btn-small">&ndash;</button>
      </div>
      
        <div class="pull-left" style="width:90%;">
          <table id="productTable" class="table table-condensed table-compressed table-striped">
            <thead>
              <tr>
                <th></th>
                <th><b class="required">*</b><spring:message code="sales_order_item_product_name" /></th>
                <th><spring:message code="sales_order_item_face_value" /></th>
                <th><spring:message code="sales_order_item_quantity" /></th>
                <th><spring:message code="sales_order_item_face_amount" /></th>
                <th><spring:message code="sales_order_item_print_fee" /></th>
                <th></th>
              </tr>
            </thead>
            <tbody>
                <c:forEach var="item" items="${orderForm.items}" varStatus="status" >
                  <tr data-index="${status.index}">
                    <td><input type="checkbox" class="productRemoveFlag" /></td>
                    <td>
                          <%-- <select class="input-medium productId" name="items[${status.index}].productId">
                              <option></option>
                            <c:forEach items="${profiles}" var="profile" >
                              <option value="${profile.id}" data-face-value="${faceValues[profile.faceValue]}" data-unit-cost="${profile.unitCost == null ? 0 : profile.unitCost}" <c:if test="${profile.id == item.productId}">selected</c:if>>${profile.productDesc}</option>
                            </c:forEach>
                          </select> --%>
                      <div>
                        <form:input path="items[${status.index}].productDesc" id="profileDesc${status.index}" />
                        <form:hidden path="items[${status.index}].productId" id="profileId${status.index}" class="input-small productId"
                           data-unit-cost="${item.unitCost}" data-face-value="${item.faceValue}" />
                        <script type='text/javascript'>
                        $(document).ready( function() {
                            var url = '<c:url value="/gc/productprofile/list/" />';
                            var $profileDesc = $( "#productTable" ).find( "input[name='items[${status.index}].productDesc']" );
                            var $hiddenId = $( "#productTable" ).find( "input[name='items[${status.index}].productId']" );//$( "#profileId${status.index}" );
                            TypeAhead.process( $profileDesc, $hiddenId, url, { label: "productDesc", value: "id" }, 
                                [ "faceValueDesc", "unitCost" ], 
                                function( $item ) {
                                    $hiddenId.data( "face-value", $item[ "faceValueDesc" ] );
                                    $hiddenId.data( "unit-cost", $item[ "unitCost" ] );
                                    $hiddenId.change();
                            });
                        });
                        </script>
                      </div>
                    </td>
                    <td>
	                    <input type="text" class="input-small faceValueFmt" value="${item.faceValueFmt}" readonly="readonly" />
	                    <input type="hidden" class="faceValue" value="${item.faceValue}" name="items[${status.index}].faceValue" />
                      <%-- <input type="text" class="input-mini faceValue" value="${item.faceValue}" readonly="readonly" name="items[${status.index}].faceValue" /> --%>
                    </td>
                    <td><input type="text" class="input-mini quantity" value="${item.quantity}" name="items[${status.index}].quantity" /></td>
                    <td>
	                    <input type="text" class="input-medium faceAmountFmt" value="${item.faceAmountFmt}" readonly="readonly" />
	                    <input type="hidden" class="faceAmount" value="${item.faceAmount}" name="items[${status.index}].faceAmount" />
                      <%-- <input type="text" class="input-medium faceAmount" value="${item.faceAmount}" readonly="readonly" name="items[${status.index}].faceAmount" /> --%>
                    </td>
                    <td><input type="text" class="input-mini printFee" ${item.freePrintFee == true ? "readonly" : ""} value="${item.printFee}" name="items[${status.index}].printFee" /></td>
                    <td>
                    <div class="checkbox">
                      <input type="checkbox" ${item.freePrintFee == true ? "checked" : ""} class="freePrintFee" name="items[${status.index}].freePrintFee" />
                      <label class="" style="text-align:left"><spring:message code="order_item_free_print_fee" /></label>
                    </div>  
                    </td>
                  </tr>
                </c:forEach>
            </tbody>
          </table>
        </div>
        
        
        
        
        </div>
        
      </div>
      
      
      
      
          
      </div>
      
      </form:form>
    
    </div>
    <div class="modal-footer">
      <button type="button" data-status="FOR_APPROVAL" class="orderSubmit btn btn-primary"><spring:message code="label_submit_for_approval" /></button>
      <button type="button" data-status="DRAFT" class="orderSubmit btn btn-primary"><spring:message code="label_draft" /></button>
      <button type="button" id="orderCancel" class="btn btn-default" data-dismiss="modal"><spring:message code="label_cancel"/></button>
    </div>
  </div>
</div>

<script>
$(document).ready(function() {
  
  var CONTAINER_ERROR = "#contentError > div";
  var CONTENT_ERROR = "#contentError";
  $form = $("#orderForm");
  $orderDialog = $("#orderDialog").css({"width": "900px"});
  
  $(".orderSubmit").click(submitForm);
  
  function submitForm() {
    $(".orderSubmit").prop( "disabled", true );
    $.post($form.attr("action") + $(this).data("status"), $form.serialize(), function(data) {
      $(".orderSubmit").prop( "disabled", false );
      if (data.success) {
    	  reloadOrderTable();
      } 
      else {
        errorInfo = "";
        for (i = 0; i < data.result.length; i++) {
          errorInfo += "<br>" + (i + 1) + ". "
              + ( data.result[i].code != undefined ? 
                  data.result[i].code : data.result[i]);
        }
        $orderDialog.find(CONTAINER_ERROR).html("Please correct following errors: " + errorInfo);
        $orderDialog.find(CONTENT_ERROR).show('slow');
      }
    }); 
  }
  
    $("input.orderDate").datepicker({
		format: 'dd-mm-yyyy',
		autoclose: true
	})
	<c:if test="${empty orderForm.orderDate}">
	.datepicker("setDate", new Date())
	</c:if>
	.datepicker("setEndDate", new Date());

    var $productTable = $('#productTable');
	
	var  $productTbody = $("tbody", $productTable);
	
	$('#removeProductBtn', $orderDialog ).click(function() {
        $("input.productRemoveFlag:checked").each(function() {
            var $tr = $(this).closest("tr");
            var length = $productTbody.find("tr").length;
            if($tr.is(":first-child") && length == 1) {
                $tr.find("input.productRemoveFlag").prop('checked', false);
                $tr.find("input").removeAttr('value').prop('disabled', true);
                $tr.find("select").removeAttr('value').prop('disabled', true);
                $tr.invisible();
            } else
                $tr.remove();
        });
    });

    $('#addProductBtn', $orderDialog ).click(function() {
        var $tr = $productTbody.find("tr:first-child");
        if($tr.css('visibility') == 'hidden' || $tr.css('display') == 'none') {
            $tr.find("input").removeAttr('value').prop('disabled', false).prop('checked', false);
            $tr.find("select").removeAttr('value').prop('disabled', false);
            $tr.visible();
        } else {
            var $trLast = $productTbody.find("tr:last-child");
            var  $row = $tr.clone();
            $row.find("input").removeAttr('value').removeAttr("checked").prop('disabled', false);
            $row.find("select").removeAttr('value').prop('disabled', false);
            var index = +$trLast.data("index") + 1;
            var lastIndex = $tr.data("index");
            $row.data("index", index);
            var rg = new RegExp("\\["+lastIndex+"\\]", 'g');
            var html = $row.html();
            html = html.replace(rg, '['+index+']');
            $row.html(html);

            $productTbody.append($row);
            initFields($row);
        }
    });
    
    $("#customerIdField")
    	.change(populateContactField);
    
    function populateContactField() {
    	$select = $("option:selected", $(this));
    	/* $("#contactField").val($select.data("contactFullname") + " - " + $select.data("contactNo") + " - " + $select.data("email")); */
    	$("#contactPerson").val($select.data("contactFullname"));
    	$("#contactNumber").val($select.data("contactNo"));
    	$("#contactEmail").val($select.data("email"));
    }
    
    initFields($orderDialog);
	
	jQuery.fn.visible = function() {
	    return this.css('visibility', 'visible');
	};

	jQuery.fn.invisible = function() {
	    return this.css('visibility', 'hidden');
	};

	function toCurrency(total) {
		  var currency = parseFloat(total, 10).toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, "$1,").toString();
	    return currency.slice( 0,-3 );
	}

	function updateTotalFceAmt() {
		var totalFceAmt = 0;
		$(".faceAmount").each(function() {
			totalFceAmt += +$(this).val();
		});
	  $("#totalFaceAmountFmt").val(toCurrency(totalFceAmt));
		$("#totalFaceAmount").val(totalFceAmt);
	}
	
	function initFields(e) {
    	$('.quantity', $(e))
    		.numberInput({"type" : "int"})
    		.change(function() {
    			$parent = $(this).closest("tr");
    			$(".faceAmount", $parent).val($(".faceValue", $parent).val() * $(this).val());
    	    $(".faceAmountFmt", $parent).val(toCurrency( $(".faceValue", $parent).val() * $(this).val() ));
    			//$(".printFee", $parent).val($(".productId option:selected", $parent).data("unitCost") * $(this).val());
    	    $( ".printFee", $parent ).val (0);
    			updateTotalFceAmt();
    		});
        $('.intInput', $(e)).numberInput({"type" : "int"});
    	$('.floatInput', $(e)).numberInput({"type" : "float"});
    	
    	$(".productId", $(e)).change(function() {
    		$(".faceValue", $(this).closest("tr")).val(/* $("option:selected", $(this)).data("faceValue") */$(this).data("faceValue"));
        $(".faceValueFmt", $(this).closest("tr")).val(toCurrency($(this).data("faceValue")));
    	});
    	
    	$(".freePrintFee", $(e)).click(function() {
    		$parent = $(this).closest("tr");
    		if($(this).is(":checked")) {
    			$(".printFee", $parent).prop("readonly", "readonly").val("0");
    		} else {
    			$(".printFee", $parent)
    				.prop("readonly", "")
    				//.val($(".productId option:selected", $parent).data("unitCost") * $(".quantity", $parent).val());
    	      .val( $( ".productId", $parent).data( "unitCost" ) * $( ".quantity", $parent ).val() );
    		}
    		
    	});
    	
    }
	
	function showReturnDetails() {
		var retNo = $(this).val();
		if(retNo != "") {
			var url = "<c:url value="/giftcard/return/" />" + retNo;
			$.post(url, function(data) {
			      if (data.success) {
			    	  var orderDate = "";
			    	  var orderNo = "";
			    	  var returnAmount = "";
			    	  var custName;
			    	  if(data.result != null) {
			    		  orderDate = data.result.orderDate;
				    	  orderNo = data.result.orderNo;
				    	  returnAmount = data.result.returnAmount;
				    	  custName = data.result.customerName;
				    	  
			    	  }
			    	  $("#soOrderNo").val(orderNo);
			    	  $("#soOrderDate").val(orderDate);
			    	  $("#returnAmount").val(toCurrency(returnAmount));
			    	  $("#customerDesc").val(custName);
			      } 
			    });	
		}
		
	}
	
	$("#returnNo")
		.change(showReturnDetails)
		.each(showReturnDetails);
	
	
	
});
</script>