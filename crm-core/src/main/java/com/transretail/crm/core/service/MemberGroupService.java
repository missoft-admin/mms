package com.transretail.crm.core.service;

import java.util.List;

import com.mysema.query.types.expr.BooleanExpression;
import com.transretail.crm.common.service.dto.request.PageSortDto;
import com.transretail.crm.common.service.dto.response.ResultList;
import com.transretail.crm.core.dto.MemberDto;
import com.transretail.crm.core.dto.MemberGroupDto;
import com.transretail.crm.core.dto.MemberGroupFieldDto;
import com.transretail.crm.core.dto.MemberGrpSearchDto;
import com.transretail.crm.core.dto.MemberResultList;
import com.transretail.crm.core.entity.MemberGroup;
import com.transretail.crm.core.entity.MemberGroupField;
import com.transretail.crm.core.entity.MemberGroupFieldValue;
import com.transretail.crm.core.entity.MemberModel;

public interface MemberGroupService {
	List<MemberGroup> getAllMemberGroups();
	MemberGroup getMemberGroup(String name);
	MemberGroup getMemberGroup(Long id);
	MemberGroupDto getMemberGroupDto(Long id);
	MemberGroup saveMemberGroup(MemberGroup group);
	MemberGroup saveMemberGroup(MemberGroupDto group);
	List<MemberGroupFieldDto> getMemberGroupFieldsDto(MemberGroup group);
	List<MemberGroupField> getMemberGroupFields(MemberGroup group);
	List<MemberGroupField> saveMemberGroupFields(List<MemberGroupField> groupFields);
	MemberGroupField saveMemberGroupField(MemberGroupField groupField);
	MemberGroupField saveMemberGroupField(MemberGroupFieldDto groupField);
	List<MemberGroupFieldValue> getMemberGroupFieldValues(MemberGroupField groupField);
	List<MemberGroupFieldValue> saveMemberGroupFieldValues(List<MemberGroupFieldValue> fieldValues);
	MemberGroupFieldValue saveMemberGroupFieldValue(MemberGroupFieldValue fieldValue);
	void delete(MemberGroup group);
	void delete(MemberGroupField field);
	void delete(List<MemberGroupFieldValue> values);
	ResultList<MemberGroup> listMemberGroups(PageSortDto dto);
	ResultList<MemberDto> getQualifiedMembers(List<MemberGroupFieldDto> fields, MemberGroup group, PageSortDto sortDto);
	List<Long> getQualifiedMembers(List<MemberGroupFieldDto> fields, MemberGroup group);
	List<Long> getQualifiedMembers(MemberGroup group);
	List<MemberGroupDto> getQualifiedCardTypeGroups(MemberModel member);
	ResultList<MemberDto> getQualifiedMembers(MemberGroup group,
			PageSortDto sortDto);
	ResultList<MemberGroupDto> search(MemberGrpSearchDto searchDto);
	ResultList<MemberDto> getQualifiedMembers(MemberGroup group,
			PageSortDto sortDto, BooleanExpression filter);
    boolean isQualifiedMember(MemberGroup group, String accountId);
    List<MemberDto> getQualifiedMembersForPrint(MemberGroup group,
            Integer segment);
    Long getQualifiedMembersCount(MemberGroup group,
            BooleanExpression filter);
    Long getQualifiedMembersCount(List<MemberGroupFieldDto> fields,
            MemberGroup group);
    List<MemberDto> getQualifiedMembersForPrint(
            List<MemberGroupFieldDto> fields, MemberGroup group,
            PageSortDto sortDto);
}
