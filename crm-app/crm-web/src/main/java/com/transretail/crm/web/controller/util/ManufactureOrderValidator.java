package com.transretail.crm.web.controller.util;

import java.util.Iterator;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import com.transretail.crm.core.repo.LookupDetailRepo;
import com.transretail.crm.loyalty.dto.ManufactureOrderDto;
import com.transretail.crm.loyalty.dto.ManufactureOrderTierDto;
import com.transretail.crm.loyalty.service.ManufactureOrderService;

@Component
public class ManufactureOrderValidator implements Validator {

    @Autowired
    private ManufactureOrderService manufactureOrderService;

    @Autowired
    private LookupDetailRepo lookupDetailRepo;

    @Autowired
    private MessageSource messageSource;

    @Override
    public boolean supports(Class<?> clazz) {
        return ManufactureOrderDto.class.isAssignableFrom(clazz);
    }

    @Override
    public void validate(Object target, Errors errors) {
        validateMo(errors, (ManufactureOrderDto) target);
    }

    private void validateMo(Errors errors, ManufactureOrderDto orderDto) {
        if (StringUtils.isBlank(orderDto.getPoNo())) {
            errors.reject(getMessage("propkey_msg_notempty", new Object[]{"mo_prop_pono"}));
        }
        if (orderDto.getSupplier() == null) {
            errors.reject(getMessage("propkey_msg_notempty", new Object[]{"mo_prop_supplier"}));
        }
        if (orderDto.getOrderDate() == null) {
            errors.reject(getMessage("propkey_msg_notempty", new Object[]{"mo_prop_orderdate"}));
        }
        if (CollectionUtils.isNotEmpty(orderDto.getTiers())) {
            validateSeries(errors, orderDto.getTiers(), orderDto);
        }
    }

    private void validateSeries(Errors errors, List<ManufactureOrderTierDto> orderTierDtos, ManufactureOrderDto orderDto) {
        Iterator<ManufactureOrderTierDto> iter = orderTierDtos.iterator();
        while (iter.hasNext()) {
            ManufactureOrderTierDto orderTierDto = iter.next();
            if (orderTierDto.getName() == null && StringUtils.isBlank(orderTierDto.getStartingSeries())
                && orderTierDto.getQuantity() == null) {
                iter.remove();
            }
        }

        for (int i = 0; i < orderTierDtos.size(); i++) {
            ManufactureOrderTierDto tier = orderTierDtos.get(i);
            if (validateSeries(errors, tier)) {
                tier.setManufactureOrderId(orderDto.getId());
                String existingMoNo = manufactureOrderService.findDuplicateTier(tier);
                if (StringUtils.isNotBlank(existingMoNo)) {
                    String companyDesc = lookupDetailRepo.getDescriptionByCode(tier.getCompanyCode());
                    String cardTypeDesc = tier.getName().getDescription();
                    errors.reject(messageSource.getMessage("propkey_msg_mo_tier_exists",
                        new String[]{companyDesc, cardTypeDesc, existingMoNo}, LocaleContextHolder.getLocale()));
                }
            }
        }
    }

    private boolean validateSeries(Errors errors, ManufactureOrderTierDto tier) {
        boolean isValid = true;
        if (tier.getQuantity() == null || tier.getQuantity() < 1) {
            String companyDesc = lookupDetailRepo.getDescriptionByCode(tier.getCompanyCode());
            String cardTypeDesc = tier.getName().getDescription();
            errors.reject(messageSource.getMessage("propkey_msg_mo_tier_invalid_quantity", new String[]{companyDesc, cardTypeDesc},
                LocaleContextHolder.getLocale()));
        } else if (StringUtils.isBlank(tier.getStartingSeries())) {
            String companyDesc = lookupDetailRepo.getDescriptionByCode(tier.getCompanyCode());
            String cardTypeDesc = tier.getName().getDescription();
            errors.reject(messageSource.getMessage("propkey_msg_mo_tier_invalid_series", new String[]{companyDesc, cardTypeDesc},
                LocaleContextHolder.getLocale()));
        }
        return isValid;
    }

    private String getMessage(String inMsgCode, Object[] inArgMsgCodes) {
        if (inArgMsgCodes != null) {
            for (int i = 0; i < inArgMsgCodes.length; i++) {
                inArgMsgCodes[i] =
                    messageSource.getMessage(inArgMsgCodes[i].toString().toLowerCase(), null, LocaleContextHolder.getLocale());
            }
        }
        return messageSource.getMessage(inMsgCode, inArgMsgCodes, "Info given is invalid.", LocaleContextHolder.getLocale());
    }
}
