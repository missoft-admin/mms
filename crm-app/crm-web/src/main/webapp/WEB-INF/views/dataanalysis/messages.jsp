<%@include file="../common/taglibs.jsp" %>

<div class="alert alert-error">
  <%--<button type="button" class="close" data-dismiss="alert">&times;</button>--%>  
  <button type="button" class="close" onclick="$(this).parent().hide();">&times;</button>
  <c:forEach items="${errorMessages}" var="error">
    <spring:message code="${error.code}" />
  </c:forEach>
</div>
