package com.transretail.crm.common.web.converter;

import java.io.IOException;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;

/**
 *
 */
public class TimestampSerializer extends JsonSerializer<Timestamp> {
    private static final SimpleDateFormat FORMATTER = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

    public void serialize(Timestamp date, JsonGenerator jsonGenerator, SerializerProvider serializerProvider) throws
        IOException {
        if (date != null) {
            jsonGenerator.writeString(FORMATTER.format(date));
        } else {
            jsonGenerator.writeString("");
        }
    }

}

