package com.transretail.crm.giftcard.service.impl;

import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.mock;

import java.math.BigDecimal;
import java.util.Set;

import org.joda.time.DateTime;
import org.joda.time.LocalDate;
import org.joda.time.LocalDateTime;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.google.common.collect.Lists;
import com.transretail.crm.common.util.CrmAopUtils;
import com.transretail.crm.giftcard.GiftCardNotFoundException;
import com.transretail.crm.giftcard.dto.GiftCardInfo;
import com.transretail.crm.giftcard.dto.PrepaidReloadTransactionInfo;
import com.transretail.crm.giftcard.entity.CardVendor;
import com.transretail.crm.giftcard.entity.GiftCardInventory;
import com.transretail.crm.giftcard.entity.GiftCardOrder;
import com.transretail.crm.giftcard.entity.PrepaidReloadInfo;
import com.transretail.crm.giftcard.entity.ProductProfile;
import com.transretail.crm.giftcard.entity.support.GiftCardOrderStatus;
import com.transretail.crm.giftcard.repo.CardVendorRepo;
import com.transretail.crm.giftcard.repo.GiftCardInventoryRepo;
import com.transretail.crm.giftcard.repo.GiftCardOrderRepo;
import com.transretail.crm.giftcard.repo.GiftCardTransactionItemRepo;
import com.transretail.crm.giftcard.repo.GiftCardTransactionRepo;
import com.transretail.crm.giftcard.repo.ProductProfileRepo;
import com.transretail.crm.giftcard.service.GiftCardAccountingService;
import com.transretail.crm.giftcard.service.GiftCardManager;

/**
 * @author Monte Cillo Co (mco@exist.com)
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(
        locations = {"classpath*:/META-INF/spring/applicationContext*.xml", "classpath:META-INF/spring/applicationContext-test.xml"})
public class GiftCardManagerImplTest2 {

    @Autowired
    private GiftCardManager giftCardManager;
    @Autowired
    private ProductProfileRepo productProfileRepo;
    @Autowired
    private GiftCardOrderRepo giftCardOrderRepo;
    @Autowired
    private CardVendorRepo cardVendorRepo;
    @Autowired
    private GiftCardInventoryRepo giftCardInventoryRepo;
    @Autowired
    private GiftCardTransactionRepo giftCardTransactionRepo;
    @Autowired
    private GiftCardTransactionItemRepo giftCardTransactionItemRepo;
    private GiftCardOrder giftCardOrder;

    @Before
    public void onSetup() throws Exception {
        GiftCardAccountingService acctService = mock(GiftCardAccountingService.class);
        doNothing().when(acctService).forB2CActivation(any(DateTime.class), any(BigDecimal.class), anyString(), anyString());

        GiftCardManagerImpl giftCardManagerImpl = CrmAopUtils.INSTANCE.getRealObject(giftCardManager, GiftCardManagerImpl.class);
        giftCardManagerImpl.setAcctService(acctService);

        CardVendor cardVendor = new CardVendor();
        cardVendor.setFormalName("formalName");
        cardVendor.setMailAddress("mailAddress");
        cardVendor.setEmailAddress("emailAddress");
        cardVendor.setRegion("region");
        cardVendor.setDefaultVendor(true);
        cardVendor = cardVendorRepo.saveAndFlush(cardVendor);

        giftCardOrder = new GiftCardOrder();
        giftCardOrder.setVendor(cardVendor);
        giftCardOrder.setStatus(GiftCardOrderStatus.APPROVED);
        giftCardOrder = giftCardOrderRepo.saveAndFlush(giftCardOrder);
    }

    @After
    public void tearDown() {
        giftCardTransactionItemRepo.deleteAll();
        giftCardTransactionRepo.deleteAll();
        giftCardInventoryRepo.deleteAll();
        productProfileRepo.deleteAll();
        giftCardOrderRepo.deleteAll();
        cardVendorRepo.deleteAll();
    }

    @Test
    public void reloadWithErrorsTest() {
        PrepaidReloadTransactionInfo request = new PrepaidReloadTransactionInfo();
        String cardNo = "00000000000000000001";
        request = request.cardNo(cardNo);
        try {
            giftCardManager.reload(request, request.getCardNo(), request.getReloadAmount());
        } catch (Exception e) {
            assertEquals(GiftCardNotFoundException.class, e.getClass());
            assertEquals(String.format("Gift Card %s is not found.", cardNo), e.getMessage());
        }
    }

    @Test
    public void reloadTest() throws Exception {
        Long series = 0000000000000000l;
        String barCode = series + "9876";
        LocalDateTime nowDt = LocalDateTime.now();
        LocalDate now = nowDt.toLocalDate();
        LocalDate orderDate = now.minusMonths(12);
        LocalDate expiryDate = now.plusMonths(12);
        Double initialBalance = 1000d;

        PrepaidReloadInfo reloadInfo1 = new PrepaidReloadInfo();
        reloadInfo1.setReloadAmount(1000l);
        reloadInfo1.setMonthEffective(1);
        PrepaidReloadInfo reloadInfo2 = new PrepaidReloadInfo();
        reloadInfo2.setReloadAmount(2000l);
        reloadInfo2.setMonthEffective(2);

        Long prepaidCardId
                = createAndSaveGifCardInventoryForReload(series, barCode, orderDate, expiryDate, initialBalance, reloadInfo1, reloadInfo2);

        PrepaidReloadTransactionInfo request
                = new PrepaidReloadTransactionInfo().cardNo(barCode).transactionTime(nowDt).reloadAmount(1000l).merchantId(
                        "merchantId").terminalId("terminalId").cashierId("cashierId").transactionNo("txnno01");
        Set<GiftCardInfo> result = giftCardManager.reload(request, request.getCardNo(), request.getReloadAmount());
        assertEquals(1, result.size());
        GiftCardInfo responseInfo = result.iterator().next();
        LocalDate expectedExpiryDate = expiryDate.plusMonths(1);
        assertEquals(expectedExpiryDate, responseInfo.getExpireDate());
        assertEquals(expectedExpiryDate, giftCardInventoryRepo.findOne(prepaidCardId).getExpiryDate());

        request = request.reloadAmount(1800l).transactionNo("txnno02");
        result = giftCardManager.reload(request, request.getCardNo(), request.getReloadAmount());
        assertEquals(1, result.size());
        responseInfo = result.iterator().next();
        expectedExpiryDate = expectedExpiryDate.plusMonths(1);
        assertEquals(expectedExpiryDate, responseInfo.getExpireDate());
        assertEquals(expectedExpiryDate, giftCardInventoryRepo.findOne(prepaidCardId).getExpiryDate());

        request = request.reloadAmount(2000l).transactionNo("txnno03");
        result = giftCardManager.reload(request, request.getCardNo(), request.getReloadAmount());
        assertEquals(1, result.size());
        responseInfo = result.iterator().next();
        expectedExpiryDate = expectedExpiryDate.plusMonths(2);
        assertEquals(expectedExpiryDate, responseInfo.getExpireDate());
        assertEquals(expectedExpiryDate, giftCardInventoryRepo.findOne(prepaidCardId).getExpiryDate());

        request = request.reloadAmount(10000l).transactionNo("txnno04");
        result = giftCardManager.reload(request, request.getCardNo(), request.getReloadAmount());
        assertEquals(1, result.size());
        responseInfo = result.iterator().next();
        expectedExpiryDate = expectedExpiryDate.plusMonths(2);
        assertEquals(expectedExpiryDate, responseInfo.getExpireDate());
        assertEquals(expectedExpiryDate, giftCardInventoryRepo.findOne(prepaidCardId).getExpiryDate());
    }

    private Long createAndSaveGifCardInventoryForReload(Long series, String barCode, LocalDate orderDate, LocalDate expiryDate,
            Double balance, PrepaidReloadInfo... reloadInfos) {
        String productCode = "productCode";
        String productDesc = "productDesc";
        ProductProfile productProfile = new ProductProfile();
        productProfile.setProductCode(productCode);
        productProfile.setProductDesc(productDesc);
        productProfile.setMaxAmount(500000l);

        productProfile.setAllowReload(true);
        for (PrepaidReloadInfo reloadInfo : reloadInfos) {
            reloadInfo.setProductProfile(productProfile);
        }
        productProfile.setPrepaidReloadInfos(Lists.newArrayList(reloadInfos));
        productProfile = productProfileRepo.saveAndFlush(productProfile);

        GiftCardInventory inventory = new GiftCardInventory();
        inventory.setFaceValue(new BigDecimal("500000"));
        inventory.setProfile(productProfile);
        inventory.setOrder(giftCardOrder);
        inventory.setProductCode(productCode);
        inventory.setProductName(productDesc);
        inventory.setSeqNo(1);
        inventory.setBatchNo(1);
        inventory.setSeries(series);
        inventory.setBarcode(barCode);

        inventory.setActivationDate(orderDate);
        inventory.setOrderDate(orderDate);
        inventory.setExpiryDate(expiryDate);
        inventory.setBalance(balance);

        return giftCardInventoryRepo.saveAndFlush(inventory).getId();
    }
}
