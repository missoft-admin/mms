package com.transretail.crm.core.dto;

import com.transretail.crm.core.entity.City;


public class CityDto {
	private Long provinceId;
	private Long id;
	private String name;
	
	public CityDto() {}
	
	public CityDto(City city) {
		this.provinceId = city.getProvince().getId();
		this.id = city.getId();
		this.name= city.getName();
	}
	
	public Long getProvinceId() {
		return provinceId;
	}
	public void setProvinceId(Long provinceId) {
		this.provinceId = provinceId;
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	
}
