package com.transretail.crm.web.giftcard.controller.dto;


import org.hibernate.validator.constraints.NotEmpty;

import com.transretail.crm.web.giftcard.controller.GiftCardCommonRequest;


public class EGiftCardRedeemRequest extends GiftCardCommonRequest {

    @NotEmpty
    private String pin;



	public String getPin() {
		return pin;
	}
	public void setPin(String pin) {
		this.pin = pin;
	}

}
