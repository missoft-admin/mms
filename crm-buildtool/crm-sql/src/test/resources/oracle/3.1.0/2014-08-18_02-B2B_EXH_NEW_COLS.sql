alter table CRM_B2B_EXH add ( 
TAX double precision,
        TAXABLE_AMOUNT number(19,2),
        TOTAL_PAYMENT number(19,2)
);
