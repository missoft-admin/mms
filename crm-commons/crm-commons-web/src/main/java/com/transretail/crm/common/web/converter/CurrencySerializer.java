package com.transretail.crm.common.web.converter;

import java.io.IOException;
import java.text.DecimalFormat;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;

public class CurrencySerializer extends JsonSerializer<Number> {
	
	@Override
	public void serialize(Number number, JsonGenerator jsonGenerator, SerializerProvider serializerProvider) throws IOException {
		DecimalFormat fmt = new DecimalFormat("#,##0");
		jsonGenerator.writeString(fmt.format(number));
	}
}
