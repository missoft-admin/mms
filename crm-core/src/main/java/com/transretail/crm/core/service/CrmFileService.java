package com.transretail.crm.core.service;


import org.springframework.web.multipart.MultipartFile;

import com.transretail.crm.core.entity.CrmFile;


public interface CrmFileService {

	CrmFile get(long id);

	CrmFile saveMultipartFile(MultipartFile file, String modelType);

	String temporarilySaveFile(long id);

	MultipartFile getMultipartFile(long id);

}
