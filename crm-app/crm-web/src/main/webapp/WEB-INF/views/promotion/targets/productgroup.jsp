<%@ include file="../../common/taglibs.jsp" %>


<div id="content_productGroup" class="modal hide  nofly modal-dialog">

  <style type="text/css"><!--.cont-01 { margin-left: 12px !important; margin-top: 27px !important; } .cont-01 div { margin-bottom: 3px; } label .link { font-weight: normal !important; }--></style>

  <div class="modal-content">
    
    <div class="modal-header">
      <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
      <h4>Add <spring:message code="target_product_group" /></h4>
    </div>

    <div class="modal-body">
      <div class="hide alert alert-error" id="content_error"><button type="button" class="close" onclick="$(this).parent().hide();">&times;</button><div><form:errors path="*"/></div></div>

   	  <div class="mb10">
        <a id="link_createProductGroup" class="link btn" href="<c:url value="/groups/product/create" />"><spring:message code="label_create" /> <spring:message code="label_group_productgroups"/></a>
        <a id="link_viewProductGroup" class="hide link" data-id="" href="<c:url value="/groups/product/view/" />">, 
          <spring:message code="label_view" />
        </a>
      </div>

      <div>
        <%-- <div>
        <c:url var="url_action" value="/groups/product/search" />
        <form:form id="searchProductGroupForm" name="searchProductGroup" modelAttribute="searchProductGroup" action="${url_action}" class="modal-form form-horizontal">
          <div class="row-fluid">
	          <fieldset class="span2">
	             <c:url var="url_division" value="/product/classification/list?type=${division}&code=%CODE%" />
	            <label for="division"><a data-id="division" href="${url_division}" class="linkShowAll link"><spring:message code="label_showall" /></a>&nbsp;<spring:message code="group_product_division" /></label>
	            <select id="division" name="division" data-url="${url_division}" ></select>
	          </fieldset>
	          <fieldset class="span2">
	             <c:url var="url_department" value="/product/classification/list?type=${department}&code=%CODE%" />
	            <label for="department"><a data-id="department" href="${url_department}" class="linkShowAll"><spring:message code="label_showall" /></a>&nbsp;<spring:message code="group_product_department" /></label>
	            <select id="department" name="department" data-url="${url_department}" ></select>
	          </fieldset>
	          <fieldset class="span2">
	             <c:url var="url_group" value="/product/classification/list?type=${group}&code=%CODE%" />
	            <label for="group"><a data-id="group" href="${url_group}" class="linkShowAll"><spring:message code="label_showall" /></a>&nbsp;<spring:message code="group_product_group" /></label>
	            <select id="group" name="group" data-url="${url_group}" ></select>
	          </fieldset>
	          <fieldset class="span2">
	             <c:url var="url_family" value="/product/classification/list?type=${family}&code=%CODE%" />
	            <label for="family"><a data-id="family" href="${url_family}" class="linkShowAll"><spring:message code="label_showall" /></a>&nbsp;<spring:message code="group_product_family" /></label>
	            <select id="family" name="family" data-url="${url_family}" ></select>
	          </fieldset>
	          <fieldset class="span2">
	             <c:url var="url_subfamily" value="/product/classification/list?type=${subfamily}&code=%CODE%" />
	            <label for="subfamily"><a data-id="subfamily" href="${url_subfamily}" class="linkShowAll"><spring:message code="label_showall" /></a>&nbsp;<spring:message code="group_product_subfamily" /></label>
	            <select id="subfamily" name="subfamily" data-url="${url_subfamily}" ></select>
	          </fieldset>
          </div>

          <div>
            <input id="filterButton" class="btn btn-primary" type="submit" value="<spring:message code="label_filter" />" />
          </div>
        </form:form>
        </div> --%>

	      <div class="row-fluid">
	        <fieldset class="span6 pull-left">
	          <label for="productGroups" class="control-label" id="label_accountId">
              <spring:message code="label_group_productgroups"/>

            </label>
	          <select id="productGroups" multiple="multiple" class="span12 select_viewProductGroup" data-url="<c:url value="/groups/product/list" />"></select>
	        </fieldset>
	        <div class="">
	          <div class="cont-02" style="margin-top: 25px;">
	            <input type="button" id="addGroupBtn" class="btn" value=">>"/>
	            <input type="button" id="removeGroupBtn" class="btn" value="<<"/>
	          </div>          
	        </div>
	        <fieldset class="span5 pull-left">
	          <c:url var="url_getProductList" value="/groups/product/list/products?prdcode=%PRDCODE%&cfncode=%CFNCODE%" />
	          <label for="selectedProductGroups" class="control-label" id="label_accountId"><spring:message code="label_group_selection"/></label>
            <select id="selectedProductGroups" multiple="multiple" class="span12 select_viewProductGroup"></select>
	        </fieldset>
	        <div class="clearfix"></div>
	      </div>
      </div>

      <div id="viewProductGroup"></div>
    </div>

    <div class="modal-footer">
      <!--<input id="saveButton" class="btn btn-primary" type="button" value="<spring:message code="label_save" />" />
      <input id="cancelButton" class="btn " type="button" value="<spring:message code="label_cancel" />" data-dismiss="modal" />-->
      <button type="button" class="btn btn-primary" id="saveButton"><spring:message code="label_save" /></button>
      <button type="button" class="btn" data-dismiss="modal" id="cancelButton"><spring:message code="label_cancel" /></button>
    </div>

  </div>


  <script src='<c:url value="/js/viewspecific/promotion/targets/productGroup.js"/>' type='text/javascript'><![CDATA[&nbsp; ]]></script>


</div>


<div id="form_productGroup"></div>