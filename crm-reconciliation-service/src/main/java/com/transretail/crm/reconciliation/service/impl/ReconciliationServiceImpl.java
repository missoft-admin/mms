package com.transretail.crm.reconciliation.service.impl;

import com.google.common.collect.Lists;
import com.transretail.crm.Constants;
import com.transretail.crm.common.service.dto.request.PagingParam;
import com.transretail.crm.common.service.dto.response.ResultList;
import com.transretail.crm.common.service.exception.MessageSourceResolvableException;
import com.transretail.crm.common.util.IOUtils;
import com.transretail.crm.reconciliation.service.ReconciliationService;
import com.transretail.crm.rest.pos.response.dto.MissingEmpTxnBean;
import com.transretail.crm.rest.pos.response.dto.MissingPointsBean;
import com.transretail.crm.rest.request.MissingEmpTxnSearchDto;
import com.transretail.crm.rest.request.MissingPointsSearchDto;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.sql.DataSource;
import java.sql.*;
import java.util.List;

/**
 * @author Allan G. Ramirez (agramirez@exist.com)
 */
@Service
public class ReconciliationServiceImpl implements ReconciliationService {
    private static final Logger _LOG = LoggerFactory.getLogger(ReconciliationServiceImpl.class);

    @Autowired
    private DataSource dataSource;

    public ResultList<MissingPointsBean> getMissingIndProfPoints(MissingPointsSearchDto dto) throws MessageSourceResolvableException {
        ResultList<MissingPointsBean> result = null;
        List<MissingPointsBean> list = Lists.newArrayList();
        int totalRecords = 0;
        PagingParam pagination = dto.getPagination();
        Integer storeId = getStoreIdByStoreCode(dto.getStoreCode());
        if (storeId != null) {
            // Feature #92388 - 1. CRM_POINTS
            StringBuilder predicateQuery = new StringBuilder(" FROM pos_transaction c, pos_payment payment ");
            predicateQuery.append("where c.customer_id is not null ");
            predicateQuery.append("and c.STORE_ID = ? ");
            predicateQuery.append("and c.status = 'COMPLETED' ");
            predicateQuery.append("and c.type = 'SALE' ");
            predicateQuery.append("and c.id = payment.pos_txn_id ");
            predicateQuery.append("and exists (select 1 from POS_PAYMENT t where t.POS_TXN_ID = c.id) ");
            predicateQuery.append("and not exists (select id from crm_points p where p.txn_no = c.id) ");
            predicateQuery.append("and exists (select 1 from crm_member m where account_id = customer_id AND m.status = 'ACTIVE') ");
            List<Object> params = Lists.newArrayList();
            params.add(storeId);
            params.add("1201");
            params.add("1202");
            params.add("1203");
            if (StringUtils.isNotBlank(dto.getAccountId())) {
                predicateQuery.append("and c.customer_id LIKE ? ");
                params.add(dto.getAccountId() + "%");
            }
            if (StringUtils.isNotBlank(dto.getTransactionNo())) {
                predicateQuery.append("and c.id LIKE ? ");
                params.add(dto.getTransactionNo() + "%");
            }
            if (dto.getTxnDateFrom() != null) {
                predicateQuery.append("and c.TRANSACTION_DATE >= ? ");
                params.add(new Timestamp(dto.getTxnDateFrom().toDate().getTime()));
            }
            if (dto.getTxnDateTo() != null) {
                predicateQuery.append("and c.TRANSACTION_DATE <= ? ");
                params.add(new Timestamp(dto.getTxnDateTo().toDate().getTime()));
            }

            Connection con = null;
            PreparedStatement ps = null;
            ResultSet rs = null;
            try {
                con = dataSource.getConnection();
                con.setAutoCommit(false);
                // com.ibm.ws.rsadapter.jdbc.WSJdbcConnection does not support readonly
                if (!con.getClass().getName().contains("WSJdbcConnection")) {
                    con.setReadOnly(true);
                }

                // Get count
                ps = con.prepareStatement("SELECT COUNT(c.id) " + predicateQuery.toString());
                for (int i = 0; i < params.size(); i++) {
                    ps.setObject(i + 1, params.get(i));
                }
                rs = ps.executeQuery();

                if (rs.next()) {
                    totalRecords = rs.getInt(1);

                    IOUtils.INSTANCE.close(rs);
                    IOUtils.INSTANCE.close(ps);

                    int start = pagination.getPageNo() * pagination.getPageSize();
                    int end = start + pagination.getPageSize();

                    // This is so oracle and timesten specific
                    StringBuilder selectBuilder = new StringBuilder("SELECT * FROM ( ");
                    selectBuilder.append("SELECT c.id, c.customer_id, c.sales_date, c.total_amount, c.TRANSACTION_DATE,"
                            + "c.status,c.type,payment.media_type,");
                    selectBuilder.append("ROW_NUMBER() OVER (ORDER BY c.sales_date) Row_Num");
                    selectBuilder.append(predicateQuery.toString());
                    selectBuilder.append(") WHERE Row_Num BETWEEN ? and ?");

                    params.add(start + 1);
                    params.add(end);

                    ps = con.prepareStatement(selectBuilder.toString());
                    for (int i = 0; i < params.size(); i++) {
                        ps.setObject(i + 1, params.get(i));
                    }
                    rs = ps.executeQuery();

                    while (rs.next()) {
                        MissingPointsBean bean = new MissingPointsBean();
                        bean.setTxnNo(rs.getString(1));
                        bean.setAccountId(rs.getString(2));
                        bean.setSalesDate(rs.getTimestamp(3));
                        bean.setTotalAmount(rs.getLong(4));
                        bean.setTransactionDate(rs.getTimestamp(5));
                        bean.setStatus(rs.getString(6));
                        bean.setType(rs.getString(7));
                        bean.setMediaType(rs.getString(8));
                        list.add(bean);
                    }
                    result = new ResultList<MissingPointsBean>(list, totalRecords, pagination.getPageNo(), pagination.getPageSize());
                } else {
                    result = new ResultList<MissingPointsBean>(list, 0, false, false);
                }
            } catch (SQLException e) {
                _LOG.error(Constants.JDBC_ERROR_DEFAULT_MESSAGE.toString(), e);
                throw new MessageSourceResolvableException(Constants.JDBC_ERROR_CODE.toString(), null,
                    Constants.JDBC_ERROR_DEFAULT_MESSAGE.toString());
            } finally {
                IOUtils.INSTANCE.close(rs);
                IOUtils.INSTANCE.close(ps);
                IOUtils.INSTANCE.close(con, true);
            }
        } else {
            _LOG.warn("No corresponding store id for store code {}.", dto.getStoreCode());
            result = new ResultList<MissingPointsBean>(list, 0, false, false);
        }

        _LOG.info("[MISSING POINTS SEARCH] Total Records - {} : Total Result - {} : Page No - {}", totalRecords, list.size(),
            pagination.getPageNo());
        return result;
    }

    @Override
    public ResultList<MissingEmpTxnBean> getMissingEmpTxns(MissingEmpTxnSearchDto dto, String a, String b) throws MessageSourceResolvableException {
        ResultList<MissingEmpTxnBean> result = null;
        List<MissingEmpTxnBean> list = Lists.newArrayList();
        int totalRecords = 0;
        PagingParam pagination = dto.getPagination();
        Integer storeId = getStoreIdByStoreCode(dto.getStoreCode());
        if (storeId != null) {
            List<Object> params = Lists.newArrayList();
            params.add(storeId);
            params.add(storeId);
            params.add(dto.getStoreCode());

            if (StringUtils.isBlank(a)) {
                a = "'421408','627891','601900'";
            }
            if (StringUtils.isBlank(b)) {
                b = "'CASH','DEBIT','GC'";
            }
            // Feature #92388 - 2. CRM_EMPLOYEE_PURCHASE_TXN
            StringBuilder predicateQuery = new StringBuilder("FROM POS_PAYMENT a ");
            predicateQuery.append("left join ELECTRONIC_FUND_TRANSFER b on a.ID=b.POS_PAYMENT_ID ");
            predicateQuery.append("left join POS_TRANSACTION c on a.POS_TXN_ID=c.id ");
            predicateQuery.append("where substr(c.CUSTOMER_ID,1,2)='00' ");
            predicateQuery.append("and c.status = 'COMPLETED' ");
            predicateQuery.append("and c.type='SALE' ");
            predicateQuery.append(
                "and (substr(b.CARD_NUM,1,6) in (" + a + ") or a.MEDIA_TYPE in (" + b + ")) ");
            predicateQuery.append("and c.store_id = ? ");
            predicateQuery.append("and c.customer_id is not null and customer_id <> '1203000000011' ");
            predicateQuery.append("and c.customer_id not like '10025HD%' ");
            predicateQuery.append(
                "and not exists (select txn_no from crm_employee_purchase_txn where txn_no = c.id and store_code = ?) ");
            predicateQuery.append("and not exists (select 1 from crm_points where store_code = ? and txn_no = c.id) ");
            predicateQuery.append("and exists (select 1 from crm_member m where account_id = substr(customer_id,1,11) AND m.status = 'ACTIVE') ");
            if (StringUtils.isNotBlank(dto.getAccountId())) {
                predicateQuery.append("and c.customer_id LIKE ? ");
                params.add("%" + dto.getAccountId() + "%");
            }
            if (StringUtils.isNotBlank(dto.getTransactionNo())) {
                predicateQuery.append("and c.id LIKE ? ");
                params.add(dto.getTransactionNo() + "%");
            }
            if (dto.getTxnDateFrom() != null) {
                predicateQuery.append("and c.TRANSACTION_DATE >= ? ");
                params.add(new Timestamp(dto.getTxnDateFrom().toDate().getTime()));
            }
            if (dto.getTxnDateTo() != null) {
                predicateQuery.append("and c.TRANSACTION_DATE <= ? ");
                params.add(new Timestamp(dto.getTxnDateTo().toDate().getTime()));
            }
            if (StringUtils.isNotBlank(dto.getMediaType())) {
                predicateQuery.append("and a.MEDIA_TYPE like ? ");
                params.add(dto.getMediaType() + "%");
            }
            String[] columns = new String[]{"c.CUSTOMER_ID", "c.ID", "c.TYPE", "c.STATUS", "c.TRANSACTION_DATE", "a.MEDIA_TYPE"};

            Connection con = null;
            PreparedStatement ps = null;
            ResultSet rs = null;
            try {
                con = dataSource.getConnection();
                con.setAutoCommit(false);
                // com.ibm.ws.rsadapter.jdbc.WSJdbcConnection does not support readonly
                if (!con.getClass().getName().contains("WSJdbcConnection")) {
                    con.setReadOnly(true);
                }

                ps = con.prepareStatement(
                    "SELECT COUNT(DISTINCT " + StringUtils.join(columns, "||") + ") " + predicateQuery.toString());
                for (int i = 0; i < params.size(); i++) {
                    ps.setObject(i + 1, params.get(i));
                }
                rs = ps.executeQuery();
                if (rs.next()) {
                    totalRecords = rs.getInt(1);
                    IOUtils.INSTANCE.close(rs);
                    IOUtils.INSTANCE.close(ps);
                    int start = pagination.getPageNo() * pagination.getPageSize();
                    int end = start + pagination.getPageSize();

                    // This is so oracle and timesten specific
                    StringBuilder selectBuilder = new StringBuilder("SELECT * FROM ( ");
                    selectBuilder
                        .append("SELECT c.customer_id, c.TRANSACTION_DATE, c.ID, sum(a.AMOUNT), c.TYPE, c.STATUS, a.MEDIA_TYPE, ");
                    selectBuilder.append("ROW_NUMBER() OVER (ORDER BY c.TRANSACTION_DATE asc) Row_Num ");
                    selectBuilder.append(predicateQuery.toString() + " GROUP BY " + StringUtils.join(columns, ","));
                    selectBuilder.append(") WHERE Row_Num BETWEEN ? and ?");

                    params.add(start + 1);
                    params.add(end);

                    ps = con.prepareStatement(selectBuilder.toString());
                    for (int i = 0; i < params.size(); i++) {
                        ps.setObject(i + 1, params.get(i));
                    }
                    rs = ps.executeQuery();
                    while (rs.next()) {
                        MissingEmpTxnBean bean = new MissingEmpTxnBean();
                        bean.setAccountId(rs.getString(1));
                        bean.setTransactionDate(rs.getTimestamp(2));
                        bean.setTxnNo(rs.getString(3));
                        bean.setTotalPurchasesAmount(rs.getLong(4));
                        bean.setType(rs.getString(5));
                        bean.setStatus(rs.getString(6));
                        bean.setMediaType(rs.getString(7));
                        list.add(bean);
                    }
                    result = new ResultList<MissingEmpTxnBean>(list, totalRecords, pagination.getPageNo(), pagination.getPageSize());
                } else {
                    result = new ResultList<MissingEmpTxnBean>(list, 0, false, false);
                }
            } catch (SQLException e) {
                _LOG.error(Constants.JDBC_ERROR_DEFAULT_MESSAGE.toString(), e);
                throw new MessageSourceResolvableException(Constants.JDBC_ERROR_CODE.toString(), null,
                    Constants.JDBC_ERROR_DEFAULT_MESSAGE.toString());
            } finally {
                IOUtils.INSTANCE.close(rs);
                IOUtils.INSTANCE.close(ps);
                IOUtils.INSTANCE.close(con, true);
            }
        } else {
            _LOG.warn("No corresponding store id for store code {}.", dto.getStoreCode());
            result = new ResultList<MissingEmpTxnBean>(list, 0, false, false);
        }

        _LOG.info("[MISSING EMP TXN SEARCH] Total Records - {} : Total Result - {} : Page No - {}", totalRecords, list.size(),
            pagination.getPageNo());
        return result;
    }

    private Integer getStoreIdByStoreCode(String storeCode) throws MessageSourceResolvableException {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            con = dataSource.getConnection();
            con.setAutoCommit(false);
            // com.ibm.ws.rsadapter.jdbc.WSJdbcConnection does not support readonly
            if (!con.getClass().getName().contains("WSJdbcConnection")) {
                con.setReadOnly(true);
            }

            ps = con.prepareStatement("SELECT STORE_ID FROM STORE WHERE CODE = ?");
            ps.setString(1, storeCode);
            rs = ps.executeQuery();
            return rs.next() ? rs.getInt(1) : null;
        } catch (SQLException e) {
            _LOG.error(Constants.JDBC_ERROR_DEFAULT_MESSAGE.toString(), e);
            throw new MessageSourceResolvableException(Constants.JDBC_ERROR_CODE.toString(), null,
                Constants.JDBC_ERROR_DEFAULT_MESSAGE.toString());
        } finally {
            IOUtils.INSTANCE.close(rs);
            IOUtils.INSTANCE.close(ps);
            IOUtils.INSTANCE.close(con, true);
        }
    }
}
