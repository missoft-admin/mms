create table CRM_GC_PCS_CARD_STATUS (
	GC_INV_ID bigint,
	PCS_ID bigint,
	STATUS varchar(7) not null,
	primary key (GC_INV_ID, PCS_ID)
    );

alter table CRM_GC_PCS_CARD_STATUS 
    add constraint FK_dh6jmu91p6xnn7slgav5qoeli 
    foreign key (GC_INV_ID) 
    references CRM_GC_INVENTORY;

alter table CRM_GC_PCS_CARD_STATUS 
    add constraint FK_jervwe7mx7x2ymtrcf09x7uqb 
    foreign key (PCS_ID)
    references CRM_GC_PHYSICAL_COUNT_SUMMARY;

--
-- DROP
--
 
-- alter table CRM_GC_PCS_CARD_STATUS 
--     drop constraint FK_dh6jmu91p6xnn7slgav5qoeli;
-- 
-- alter table CRM_GC_PCS_CARD_STATUS 
--     drop constraint FK_jervwe7mx7x2ymtrcf09x7uqb;
-- 
-- drop table CRM_GC_PCS_CARD_STATUS if exists;