<%@include file="../../common/messages.jsp" %>

<div id="content_adjustment">

  <div class="page-header page-header2"><h1><spring:message code="points_label_override" /></h1></div>

  <sec:authorize var="isMemberMgmt"  ifAnyGranted="ROLE_CS" />
  <sec:authorize var="isEmpMgmt"  ifAnyGranted="ROLE_HR" />

  <c:if test="${ ( not empty overrideType and overrideType == member and isMemberMgmt ) or ( empty overrideType and isMemberMgmt ) }">
  <div class="pull-left">
    <spring:message var="lblAccountId" code="points_prop_member_id"/>
    <a href="#" class="btn btn-primary link_createAdjustment" 
        data-url="<c:url value="/points/override/create"/>" 
        data-action="<c:url value="/points/override/save/${member}"/>" 
        data-label-account-id="${lblAccountId}" >
      <spring:message code="points_label_createadjustpointsentry_member" />
    </a>
  </div>
  </c:if>

  <c:if test="${ ( not empty overrideType and overrideType == employee and isEmpMgmt ) or ( empty overrideType and isEmpMgmt ) }">
  <div class="pull-left">
    <spring:message var="lblAccountId" code="points_prop_member_empid"/>
    <a href="#" class="btn btn-primary btn-space-02 link_createAdjustment" 
        data-url="<c:url value="/points/override/create"/>" 
        data-action="<c:url value="/points/override/save/${employee}"/>" 
        data-label-account-id="${lblAccountId}">
      <spring:message code="points_label_createadjustpointsentry_emp" />
    </a>
  </div>
  </c:if>
  <div class="clearfix"></div>

  <div id="form_adjustment"><jsp:include page="adjustmentForm.jsp" /></div>
  <div id="list_adjustments" class="data-content-02"><jsp:include page="adjustmentListOne.jsp"/></div>


  <script src='<c:url value="/js/viewspecific/points/override/adjustment.js"/>' type='text/javascript'><![CDATA[&nbsp; ]]></script>
  <link href="<c:url value="/styles/temp.css" />" rel="stylesheet" />


</div>