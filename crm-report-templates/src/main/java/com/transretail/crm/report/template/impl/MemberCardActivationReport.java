package com.transretail.crm.report.template.impl;

import java.util.*;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import com.transretail.crm.core.service.StoreService;

import org.apache.commons.lang3.StringUtils;
import org.joda.time.LocalDate;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.MessageSourceAware;
import org.springframework.context.support.MessageSourceAccessor;
import org.springframework.stereotype.Service;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.google.common.collect.Sets;
import com.mysema.query.Tuple;
import com.mysema.query.jpa.impl.JPAQuery;
import com.mysema.query.types.expr.BooleanExpression;
import com.transretail.crm.common.reporting.jasper.ClasspathFileResolver;
import com.transretail.crm.common.reporting.jasper.DefaultJRProcessor;
import com.transretail.crm.common.reporting.jasper.JRProcessor;
import com.transretail.crm.common.util.DateUtil;
import com.transretail.crm.core.entity.QMemberModel;
import com.transretail.crm.core.entity.enums.ReportType;
import com.transretail.crm.core.entity.lookup.QLookupDetail;
import com.transretail.crm.core.repo.LookupDetailRepo;
import com.transretail.crm.core.service.CodePropertiesService;
import com.transretail.crm.loyalty.entity.QLoyaltyCardInventory;
import com.transretail.crm.report.template.CommonReportFilter;
import com.transretail.crm.report.template.FilterField;
import com.transretail.crm.report.template.ReportDateFormatProvider;
import com.transretail.crm.report.template.ReportTemplate;
import com.transretail.crm.report.template.ReportsCustomizer;

/**
 * @author Allan G. Ramirez (agramirez@exist.com)
 */
@Service("cardActivationReport")
public class MemberCardActivationReport implements ReportTemplate, MessageSourceAware, ReportsCustomizer, ReportDateFormatProvider {
    protected static final String TEMPLATE_NAME = "Card Activation Report Summary";
    protected static final String REPORT_NAME = "reports/member_card_activation.jasper";
    protected static final String FILTER_ACTIVATIONDATE_FROM = CommonReportFilter.DATE_FROM;
    protected static final String FILTER_ACTIVATIONDATE_TO = CommonReportFilter.DATE_TO;
    protected static final String FILTER_STATUS = "status";
    private static final String SEARCH_DATE_FORMAT = DateUtil.SEARCH_DATE_FORMAT;
    private static final DateTimeFormatter INPUT_DATE_PATTERN = DateTimeFormat.forPattern(SEARCH_DATE_FORMAT);
    private static final DateTimeFormatter FULL_DATE_PATTERN = DateTimeFormat.forPattern("MMMM d, yyyy");
    private static final String PARAM_ACTIVATION_DATERANGE = "activationDateRange";
    private static final String PARAM_IS_EMPTY = "isEmpty";
    private static final String PARAM_NO_DATA_FOUND = "noDataFound";
    private static final String PARAM_ALL_MEMBER_TYPES = "All Member Types - All Card Types";
    private MessageSourceAccessor messageSource;
    @PersistenceContext
    private EntityManager em;
    @Autowired
    private StoreService storeService;
    @Autowired
    private CodePropertiesService codePropertiesService;
    @Autowired
    private LookupDetailRepo lookupDetailRepo;

    @Override
    public void setMessageSource(MessageSource messageSource) {
        this.messageSource = new MessageSourceAccessor(messageSource);
    }
    
    @Override
    public ReportType[] getReportTypes() {
    	return ReportType.values();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getName() {
        return TEMPLATE_NAME;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getDescription() {
        return messageSource.getMessage("member.card.activation.report.summary.title");
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Set<FilterField> getFilters() {
        Set<FilterField> filters = new LinkedHashSet<FilterField>(2);
        filters.add(FilterField.createDateField(FILTER_ACTIVATIONDATE_FROM,
            messageSource.getMessage("member.card.activation.filter.activationdatefrom"), DateUtil.SEARCH_DATE_FORMAT, true));
        filters.add(FilterField.createDateField(FILTER_ACTIVATIONDATE_TO,
            messageSource.getMessage("member.card.activation.filter.activationdateto"), DateUtil.SEARCH_DATE_FORMAT, true));
        String activeStatusCode = codePropertiesService.getDetailLoyaltyStatusActive();
        String activeStatusDesc = lookupDetailRepo.findByCode(activeStatusCode).getDescription();
        Map<String, String> selectValues = Maps.newHashMap();
        selectValues.put("", "");
        selectValues.put(activeStatusCode, activeStatusDesc);
        filters.add(FilterField.createDropdownField(FILTER_STATUS, messageSource.getMessage("member.card.activation.filter.status"),
            selectValues));
        return filters;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean canView(Set<String> permissions) {
        if (permissions != null) {
            for (String permission : permissions) {
                if (permission.equalsIgnoreCase("REPORT_MEMBER_CARD_ACTIVATION_SUMMARY_VIEW")) {
                    return true;
                }
            }
        }
        return false;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public JRProcessor createJrProcessor(Map<String, String> map) {
        LocalDate activationDateFrom = toLocalDate(map.get(FILTER_ACTIVATIONDATE_FROM));
        LocalDate activationDateTo = toLocalDate(map.get(FILTER_ACTIVATIONDATE_TO));
        String statusCode = map.get(FILTER_STATUS);
        QLoyaltyCardInventory qLoyaltyCardInventory = QLoyaltyCardInventory.loyaltyCardInventory;
        QMemberModel qMemberModel = QMemberModel.memberModel;

        BooleanExpression predicate =
            qLoyaltyCardInventory.barcode.eq(qMemberModel.accountId).and(qLoyaltyCardInventory.activationDate.isNotNull());

        if (StringUtils.isNotBlank(statusCode)) {
            predicate = predicate.and(qLoyaltyCardInventory.status.code.eq(statusCode));
        }

        StringBuilder activationDateRange = new StringBuilder(50);
        if (activationDateFrom != null && activationDateTo != null) {
            predicate = predicate.and(qLoyaltyCardInventory.activationDate.goe(activationDateFrom));
            predicate = predicate.and(qLoyaltyCardInventory.activationDate.loe(activationDateTo));
            activationDateRange.append(FULL_DATE_PATTERN.print(activationDateFrom));
            activationDateRange.append(" - ");
            activationDateRange.append(FULL_DATE_PATTERN.print(activationDateTo));
        } else if (activationDateFrom != null) {
            predicate = predicate.and(qLoyaltyCardInventory.activationDate.goe(activationDateFrom));
            activationDateRange.append(messageSource.getMessage("member.card.activation.report.label.activationdatefrom"));
            activationDateRange.append(" ");
            activationDateRange.append(FULL_DATE_PATTERN.print(activationDateFrom));
        } else if (activationDateTo != null) {
            predicate = predicate.and(qLoyaltyCardInventory.activationDate.loe(activationDateTo));
            activationDateRange.append(messageSource.getMessage("member.card.activation.report.label.activationdateto"));
            activationDateRange.append(" ");
            activationDateRange.append(FULL_DATE_PATTERN.print(activationDateTo));
        }

        Map<String, Object> parameters = Maps.newHashMap();
        parameters.put(PARAM_ACTIVATION_DATERANGE, activationDateRange.toString());
        
        QLookupDetail qMemberType = new QLookupDetail("memberType");
        QLookupDetail qCardType = new QLookupDetail("cardType");

        List<Tuple> tuples = new JPAQuery(em).from(qLoyaltyCardInventory, qMemberModel)
        	.leftJoin(qMemberModel.memberType, qMemberType)
        	.leftJoin(qLoyaltyCardInventory.product.name, qCardType)
            .groupBy(qMemberType.description, qCardType.description, qMemberModel.registeredStore, qMemberModel.storeName)
            .orderBy(qMemberType.description.asc(), qCardType.description.asc(), qMemberModel.registeredStore.asc())
            .where(predicate)
            .list(qMemberType.description, qCardType.description,
                    qMemberModel.registeredStore, qMemberModel.storeName, qCardType.description.count());

        List<ReportBean> beansDataSource = Lists.newArrayList();

        if (!tuples.isEmpty()) {
            System.out.println("*******************************************");
            Map<String, ReportBean> reportBeanMap = Maps.newHashMap();
            for (Tuple tuple : tuples) {
                String store = tuple.get(qMemberModel.registeredStore);
                String storeName = tuple.get(qMemberModel.storeName);

                try {
                    storeName = storeService.getStoreByCode(store).getName();
                } catch(NullPointerException npe) {

                }

                String memberType = tuple.get(qMemberType.description);
                String cardType = tuple.get(qCardType.description);
                long memberTypeCount = 0;
                memberTypeCount = tuple.get(qCardType.description.count());

                StringBuilder memberCardTypeBuilder = new StringBuilder(100);
                memberCardTypeBuilder.append(memberType);
                memberCardTypeBuilder.append("-");
                memberCardTypeBuilder.append(cardType);
                String memberCardType = memberCardTypeBuilder.toString();

                long highestAll = 0;
                long highestTypes = 0;
                for(ReportBean beanMap : reportBeanMap.values()) {
                    if (beanMap.getMemberCardType().equalsIgnoreCase(memberCardType)) {
                        beanMap.incrementActivationTotal(memberTypeCount);
                        if (beanMap.getActivationTotal() > highestTypes)
                            highestTypes = beanMap.getActivationTotal();
                        else
                            beanMap.setActivationTotal(highestTypes);
                    } else if (beanMap.getMemberCardType().equalsIgnoreCase(PARAM_ALL_MEMBER_TYPES)) {
                        beanMap.incrementActivationTotal(memberTypeCount);
                        if (beanMap.getActivationTotal() > highestAll)
                            highestAll = beanMap.getActivationTotal();
                        else
                            beanMap.setActivationTotal(highestAll);
                    }
                }

                StringBuilder allTypesBuilder = new StringBuilder(100);
                allTypesBuilder.append(PARAM_ALL_MEMBER_TYPES);
                allTypesBuilder.append(":");
                allTypesBuilder.append(store);
                String keyAll = allTypesBuilder.toString();

                ReportBean beanAll = reportBeanMap.get(keyAll);
                if (beanAll == null) {
                    beanAll = new ReportBean(PARAM_ALL_MEMBER_TYPES, store, storeName);
                    reportBeanMap.put(keyAll, beanAll);
                }
                beanAll.incrementActivationCount(memberTypeCount);
                if (highestAll == 0)
                    beanAll.setActivationTotal(memberTypeCount);
                else
                    beanAll.setActivationTotal(highestAll);

                StringBuilder keyBuilder = new StringBuilder(100);
                keyBuilder.append(memberCardType);
                keyBuilder.append(":");
                keyBuilder.append(store);
                String key = keyBuilder.toString();

                ReportBean bean = reportBeanMap.get(key);
                if (bean == null) {
                    bean = new ReportBean(memberCardType, store, storeName);
                    reportBeanMap.put(key, bean);
                }
                bean.incrementActivationCount(memberTypeCount);
                if (highestTypes == 0)
                    bean.setActivationTotal(memberTypeCount);
                else
                    bean.setActivationTotal(highestTypes);
            }
            beansDataSource.addAll(reportBeanMap.values());
            Collections.sort(beansDataSource, new Comparator<ReportBean>() {
                @Override
                public int compare(ReportBean o1, ReportBean o2) {
                    String store1 = StringUtils.defaultString(o1.getMemberCardType());
                    String store2 = StringUtils.defaultString(o2.getMemberCardType());
                    return store1.compareTo(store2);
                }
            });

            List<ReportBean> toRemove = new ArrayList<ReportBean>();
            for (ReportBean bean : beansDataSource) {
                if (null==bean.getStore() || bean.getStore().equals("")) {
                    toRemove.add(bean);
                }
            }
            beansDataSource.removeAll(toRemove);
        }

        if (beansDataSource.isEmpty()) {
            parameters.put(PARAM_IS_EMPTY, true);
            parameters.put(PARAM_NO_DATA_FOUND, "No data found");
            beansDataSource.add(new ReportBean());
        }

        DefaultJRProcessor jrProcessor = new DefaultJRProcessor(REPORT_NAME, beansDataSource);
        jrProcessor.addParameters(parameters);
        jrProcessor.setFileResolver(new ClasspathFileResolver("reports"));
        return jrProcessor;
    }

    private LocalDate toLocalDate(String dateString) {
        if (StringUtils.isNotBlank(dateString)) {
            return INPUT_DATE_PATTERN.parseLocalDate(dateString);
        }
        return null;
    }

    @Override
    public DateTimeFormatter getDefaultDateFormat() {
	return INPUT_DATE_PATTERN;
    }

    @Override
    public Set<Option> customizerOption() {
	return Sets.newHashSet(Option.DATE_RANGE_FULL_VALIDATION);
    }

    /**
     * Bean used by member card activation jasper report. Access modifier is intentionallly set to public because jasper fill manager
     * cannot read protected or private classes
     */
    public static class ReportBean {
        private String memberCardType;
        private String store;
        private String storeName;
        private long activationCount;
        private long activationTotal;

        public ReportBean() {

        }

        public ReportBean(String memberCardType, String store, String storeName) {
            this.memberCardType = memberCardType;
            this.store = store;
            this.storeName = storeName;
        }

        public String getStore() {
            return store;
        }

        public void setStore(String store) {
            this.store = store;
        }

        public String getMemberCardType() {
            return memberCardType;
        }

        public void setMemberCardType(String memberCardType) {
            this.memberCardType = memberCardType;
        }

        public String getStoreName() {
            return storeName;
        }

        public void setStoreName(String storeName) {
            this.storeName = storeName;
        }

        public long getActivationCount() {
            return activationCount;
        }

        public void setActivationCount(long activationCount) {
            this.activationCount = activationCount;
        }

        public long getActivationTotal() {
            return activationTotal;
        }

        public void setActivationTotal(long activationTotal) {
            this.activationTotal = activationTotal;
        }

        public void incrementActivationCount(long incrementBy) {
            activationCount += incrementBy;
        }

        public void incrementActivationTotal(long incrementBy) {
            activationTotal += incrementBy;
        }
    }
}
