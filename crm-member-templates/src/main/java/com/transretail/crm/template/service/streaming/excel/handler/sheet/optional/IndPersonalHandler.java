package com.transretail.crm.template.service.streaming.excel.handler.sheet.optional;

import static org.apache.commons.lang3.StringUtils.isBlank;

import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.i18n.LocaleContextHolder;

import com.google.common.collect.Maps;
import com.mysema.query.jpa.impl.JPAQuery;
import com.mysema.query.jpa.impl.JPAUpdateClause;
import com.transretail.crm.core.entity.MemberModel;
import com.transretail.crm.core.entity.QMemberModel;
import com.transretail.crm.core.entity.embeddable.Address;
import com.transretail.crm.core.entity.embeddable.QAddress;
import com.transretail.crm.core.repo.AddressRepo;
import com.transretail.crm.template.service.streaming.excel.handler.HandlerException;
import com.transretail.crm.template.service.streaming.excel.handler.cell.AccountIdCellHandler;
import com.transretail.crm.template.service.streaming.excel.handler.cell.AddressCityCellHandler;
import com.transretail.crm.template.service.streaming.excel.handler.cell.AddressPostCodeCellHandler;
import com.transretail.crm.template.service.streaming.excel.handler.cell.AddressStreetCellHandler;
import com.transretail.crm.template.service.streaming.excel.handler.cell.BirthPlaceCellHandler;
import com.transretail.crm.template.service.streaming.excel.handler.cell.CellHandler;
import com.transretail.crm.template.service.streaming.excel.handler.cell.MaritalStatusCellHandler;
import com.transretail.crm.template.service.streaming.excel.handler.cell.NationalityCellHandler;
import com.transretail.crm.template.service.streaming.excel.handler.cell.ReligionCellHandler;
import com.transretail.crm.template.service.streaming.excel.handler.sheet.AbstractSheetHandler;

/**
 * @author Allan G. Ramirez (agramirez@exist.com)
 */
public class IndPersonalHandler extends AbstractSheetHandler {
    private static final String HEADER_ACCOUNTID = "ACCOUNTNUMBER";
    private static final String HEADER_STREET = "STREET";
    private static final String HEADER_CITY = "CITY";
    private static final String HEADER_ZIPCODE = "POSTCODE";
    private static final String HEADER_BIRTHPLACE = "PLACEOFBIRTH";
    private static final String HEADER_NATIONALITY = "NATIONALITY";
    private static final String HEADER_RELIGION = "RELIGION";
    private static final String HEADER_MARITALSTATUS = "MARITALSTATUS";

    private Map<String, CellHandler> cellHandlerClassMap = Maps.newHashMap();
    private CellHandler[] cellHandlers = null;

    @Autowired
    private AddressRepo addressRepo;

    public IndPersonalHandler() {
        cellHandlerClassMap.put(HEADER_ACCOUNTID, new AccountIdCellHandler());
        cellHandlerClassMap.put(HEADER_BIRTHPLACE, new BirthPlaceCellHandler());
        cellHandlerClassMap.put(HEADER_RELIGION, new ReligionCellHandler(this));
        cellHandlerClassMap.put(HEADER_NATIONALITY, new NationalityCellHandler(this));
        cellHandlerClassMap.put(HEADER_STREET, new AddressStreetCellHandler());
        cellHandlerClassMap.put(HEADER_CITY, new AddressCityCellHandler());
        cellHandlerClassMap.put(HEADER_ZIPCODE, new AddressPostCodeCellHandler());
        cellHandlerClassMap.put(HEADER_MARITALSTATUS, new MaritalStatusCellHandler(this));
        cellHandlers = new CellHandler[cellHandlerClassMap.size()];
    }

    @Override
    protected void processHeaderCell(int i, String headerName) {
        CellHandler cellHandler = cellHandlerClassMap.get(headerName);
        if (cellHandler == null) {
            throw new HandlerException(getMessageSource()
                .getMessage("member.template.messages.excel.nohandler", new String[]{sheetName, headerName},
                    LocaleContextHolder.getLocale()));
        }
        cellHandlers[i] = cellHandler;
    }

    @Override
    protected CellHandler getCellHandler(int index) {
        return cellHandlers[index];
    }

    @Override
    protected void saveOrUpdateDto(MemberModel dto) throws HandlerException {
        String accountId = dto.getAccountId();
        if (isBlank(accountId)) {
            throw new HandlerException(getMessageSource().getMessage("member.template.messages.excel.columns.required",
                new String[]{sheetName, String.valueOf(currentRowNum), HEADER_ACCOUNTID},
                LocaleContextHolder.getLocale()));
        } else if (accountId.length() != 13 || accountId.charAt(2) != '0' || accountId.charAt(3) != '1') {
            throw new HandlerException(getMessageSource().getMessage("member.template.messages.excel.accountid.invalid",
                new String[]{sheetName, accountId, "01"},
                LocaleContextHolder.getLocale()));
        }
        if (!accountIdExists(dto.getAccountId())) {
            throw new HandlerException(getMessageSource().getMessage("member.template.messages.excel.accountid.notfound",
                new String[]{sheetName, String.valueOf(currentRowNum), dto.getAccountId()},
                LocaleContextHolder.getLocale()));
        }

        QMemberModel qMemberModel = QMemberModel.memberModel;
        Long addressId = saveOrUpdateAddress(dto.getAddress(), dto.getAccountId(), qMemberModel);

        new JPAUpdateClause(em, qMemberModel).where(qMemberModel.accountId.eq(dto.getAccountId()))
            .set(qMemberModel.customerProfile.nationality.code, dto.getCustomerProfile().getNationality().getCode())
            .set(qMemberModel.customerProfile.religion.code, dto.getCustomerProfile().getReligion().getCode())
            .set(qMemberModel.customerProfile.maritalStatus.code, dto.getCustomerProfile().getMaritalStatus().getCode())
            .set(qMemberModel.customerProfile.placeOfBirth, dto.getCustomerProfile().getPlaceOfBirth())
            .set(qMemberModel.address.id, addressId)
            .execute();

        if (currentRowNum % batchSize == 0) {
            cleanup();
        }
    }

    @Override
    public int getExpectedNumberOfColumns() {
        return cellHandlers.length;
    }

    private Long saveOrUpdateAddress(Address dto, String accountId, QMemberModel qMemberModel) {
        Long result = null;
        QAddress qAddress = new QAddress("businessAddress");
        Address address = new JPAQuery(em).from(qMemberModel)
            .rightJoin(qMemberModel.address, qAddress)
            .where(qMemberModel.accountId.eq(accountId)).singleResult(
                qAddress);
        if (address == null) {
            if (StringUtils.isNotBlank(dto.getStreet()) ||
                StringUtils.isNotBlank(dto.getCity()) ||
                StringUtils.isNotBlank(dto.getPostCode())) {
                result = addressRepo.saveAndFlush(dto).getId();
            }
        } else {
            address.setStreet(dto.getStreet());
            address.setCity(dto.getCity());
            address.setPostCode(dto.getPostCode());
            result = addressRepo.saveAndFlush(address).getId();
        }
        return result;
    }

}
