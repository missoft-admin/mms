package com.transretail.crm.rest.pos.response.dto;

import java.sql.Timestamp;
import java.text.DecimalFormat;

/**
 * @author Allan G. Ramirez (agramirez@exist.com)
 */
public class MissingEmpTxnBean {
    private String accountId;
    private String txnNo;
    private Timestamp transactionDate;
    private long totalPurchasesAmount;
    private String type;
    private String status;
    private String mediaType;
    private String totalPurchasesAmountDisplay;

    public MissingEmpTxnBean() {

    }

    public String getTotalPurchasesAmountDisplay() {
        DecimalFormat formatter = new DecimalFormat("#,###");
        return formatter.format(formatter);
    }

    public String getAccountId() {
        return accountId;
    }

    public void setAccountId(String accountId) {
        this.accountId = accountId;
    }

    public String getTxnNo() {
        return txnNo;
    }

    public void setTxnNo(String txnNo) {
        this.txnNo = txnNo;
    }

    public Timestamp getTransactionDate() {
        return transactionDate;
    }

    public void setTransactionDate(Timestamp transactionDate) {
        this.transactionDate = transactionDate;
    }

    public long getTotalPurchasesAmount() {
        return totalPurchasesAmount;
    }

    public void setTotalPurchasesAmount(long totalPurchasesAmount) {
        this.totalPurchasesAmount = totalPurchasesAmount;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMediaType() {
        return mediaType;
    }

    public void setMediaType(String mediaType) {
        this.mediaType = mediaType;
    }

}
