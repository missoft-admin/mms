package com.transretail.crm.giftcard.repo.custom.impl;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.joda.time.LocalDate;

import com.mysema.query.jpa.impl.JPAQuery;
import com.transretail.crm.giftcard.entity.QStockRequest;
import com.transretail.crm.giftcard.repo.custom.StockRequestRepoCustom;

public class StockRequestRepoImpl implements StockRequestRepoCustom {
	@PersistenceContext
	EntityManager em;

	@Override
	public String createStockRequestNumber() {
		QStockRequest stockRequest = QStockRequest.stockRequest;
		JPAQuery query = new JPAQuery(em);
		
		LocalDate today = LocalDate.now();
		StringBuilder builder = new StringBuilder(today.toString("yyyyMMdd"));
		Long count = query.from(stockRequest).where(stockRequest.requestDate.eq(today))
				.uniqueResult(stockRequest.requestNo.countDistinct()) + 1;
		builder.append(String.format("%04d", count != null ? count : 0));
    	return builder.toString();
	}

}
