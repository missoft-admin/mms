package com.transretail.crm.decrypter;

import com.transretail.crm.common.util.FileCryptor;

import java.io.*;

public  class Start {


    public static void main(String[] args) {

        if (args.length > 2 || args.length == 0) {
            usage();
            return;
        }


        FileInputStream encryptedFile = null;
        try {
            encryptedFile = new FileInputStream(args[0]);
        } catch (FileNotFoundException e) {
            System.console().writer().println("File not found : " + args[0]);
            System.console().writer().println("Make sure file exists and correct path is specified");
            return;
        }

        File targetFile = null;

        if (args.length == 2) {
            targetFile  = new File(args[1]);
        } else {
            targetFile =  new File("decrypted-" + args[0]);

        }

        FileOutputStream decryptedFile = null;

        try {
            targetFile.createNewFile();
            decryptedFile = new FileOutputStream(targetFile);
            FileCryptor.decrypt(FileCryptor.DEFAULT_KEY, encryptedFile, decryptedFile);
        } catch (FileNotFoundException e) {
            System.console().writer().println("Make sure directory permission is set");

        } catch (IOException e) {
            System.console().writer().println("Unable to create target file: " + args[1]);
            System.console().writer().println("Make sure directory permission is set");
        } catch (Exception e) {
            System.console().writer().println("Error encountered in decrypting file : " + e.getMessage());

        }


        System.console().writer().println("File successfully decrypted : " + targetFile.getName());



    }


    private static void usage() {
        if (System.console() != null) {
            System.console().writer().println("USAGE :");
            System.console().writer().println("java -jar decrypter-standalone.jar <encrypted_file> [decrypted file]\n");

            System.console().writer().println("If no decrypted file is defined, same filename will be used but will have a prefix of 'decrypted-'\n");

            System.console().writer().println("Example 1 :");
            System.console().writer().println("java -jar decrypter-standalone.jar encrypted_file.txt");
            System.console().writer().println("Example 2 :");
            System.console().writer().println("java -jar decrypter-standalone.jar encrypted_file.txt decrypted_file.txt");
        }
    }

}