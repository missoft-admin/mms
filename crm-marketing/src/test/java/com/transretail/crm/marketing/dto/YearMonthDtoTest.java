package com.transretail.crm.marketing.dto;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertNotNull;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.BlockJUnit4ClassRunner;
import org.springframework.validation.BeanPropertyBindingResult;
import org.springframework.validation.Errors;
import org.springframework.validation.beanvalidation.LocalValidatorFactoryBean;

/**
 * @author Allan G. Ramirez (agramirez@exist.com)
 */
@RunWith(BlockJUnit4ClassRunner.class)
public class YearMonthDtoTest {
    private static final LocalValidatorFactoryBean VALIDATOR = new LocalValidatorFactoryBean();

    static {
        VALIDATOR.afterPropertiesSet();
    }

    @Test
    public void validTest() {
        YearMonthDto dto = new YearMonthDto();
        dto.setYearFrom("1984");
        dto.setMonthFrom("0");
        dto.setYearTo("1984");
        dto.setMonthTo("09");
        Errors errors = validate(dto);
        assertEquals(0, errors.getErrorCount());
        dto.setMonthFrom("10");
        dto.setMonthTo("12");
        errors = validate(dto);
        assertEquals(0, errors.getErrorCount());
    }

    @Test
    public void invalidTest() {
        YearMonthDto dto = new YearMonthDto();
        Errors errors = validate(dto);
        assertEquals(4, errors.getErrorCount());
        dto.setYearFrom("");
        dto.setMonthFrom("");
        dto.setYearTo("");
        dto.setMonthTo("");
        assertEquals(4, errors.getErrorCount());
        // Invalid month
        dto.setMonthTo("13");
        errors = validate(dto);
        assertNotNull(errors.getFieldError("monthTo"));
        dto.setMonthTo("1a");
        errors = validate(dto);
        assertNotNull(errors.getFieldError("monthTo"));
        dto.setMonthTo("a");
        errors = validate(dto);
        assertNotNull(errors.getFieldError("monthTo"));

        // Invalid Year
        dto.setYearTo("123");
        errors = validate(dto);
        assertNotNull(errors.getFieldError("yearTo"));
        dto.setYearTo("123a");
        errors = validate(dto);
        assertNotNull(errors.getFieldError("yearTo"));
        dto.setYearTo("12312");
        errors = validate(dto);
        assertNotNull(errors.getFieldError("yearTo"));
    }

    private Errors validate(YearMonthDto form) {
        Errors result = new BeanPropertyBindingResult(form, "yearMonthDto");
        VALIDATOR.validate(form, result);
        return result;
    }
}
