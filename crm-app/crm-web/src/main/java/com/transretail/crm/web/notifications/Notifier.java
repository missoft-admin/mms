package com.transretail.crm.web.notifications;


import java.util.ArrayList;
import java.util.Collection;
import java.util.EnumMap;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.inject.Inject;
import javax.inject.Named;
import javax.inject.Singleton;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.cometd.annotation.Configure;
import org.cometd.annotation.Listener;
import org.cometd.annotation.Service;
import org.cometd.annotation.Session;
import org.cometd.annotation.Subscription;
import org.cometd.bayeux.Message;
import org.cometd.bayeux.client.ClientSession;
import org.cometd.bayeux.server.BayeuxServer;
import org.cometd.bayeux.server.ConfigurableServerChannel;
import org.cometd.bayeux.server.ServerChannel;
import org.cometd.bayeux.server.ServerMessage;
import org.cometd.bayeux.server.ServerSession;
import org.cometd.server.authorizer.GrantAuthorizer;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.google.common.collect.Sets;
import com.transretail.crm.common.service.dto.rest.ReturnMessage;
import com.transretail.crm.common.web.notification.NotificationType;
import com.transretail.crm.common.web.notification.NotifierService;
import com.transretail.crm.common.web.notification.annotation.Notifiable;
import com.transretail.crm.common.web.notification.annotation.NotificationUpdate;
import com.transretail.crm.core.dto.ApprovalMatrixDto;
import com.transretail.crm.core.dto.ApprovalMatrixResultList;
import com.transretail.crm.core.dto.ApprovalMatrixSearchDto;
import com.transretail.crm.core.dto.EmployeePurchaseTxnDto;
import com.transretail.crm.core.dto.MemberDto;
import com.transretail.crm.core.dto.PointsDto;
import com.transretail.crm.core.dto.UserRolePermDto;
import com.transretail.crm.core.entity.ApprovalMatrixModel;
import com.transretail.crm.core.entity.MemberModel;
import com.transretail.crm.core.entity.Notification;
import com.transretail.crm.core.entity.UserModel;
import com.transretail.crm.core.entity.UserRoleModel;
import com.transretail.crm.core.entity.UserRolePermission;
import com.transretail.crm.core.entity.support.CustomAuditableEntity;
import com.transretail.crm.core.security.model.impl.CustomSecurityUserDetailsImpl;
import com.transretail.crm.core.security.service.CustomSecurityUserService;
import com.transretail.crm.core.service.ApprovalMatrixService;
import com.transretail.crm.core.service.CodePropertiesService;
import com.transretail.crm.core.service.HrOverridePermissionsService;
import com.transretail.crm.core.service.NotificationService;
import com.transretail.crm.core.service.UserRolePermissionService;
import com.transretail.crm.core.service.UserService;
import com.transretail.crm.web.controller.util.ControllerResponse;

//@Named
//@Singleton
@Aspect
@Service("notifier")
@Controller("/notification")
public class Notifier {
	@Session
	private ClientSession clientSession;
	@Inject
	private NotifierService notifierService;
	@Autowired
	private NotificationService notificationService;
	@Autowired
	private MessageSource messageSource;
	@Autowired
	private UserService userService;
	@Autowired
    private CustomSecurityUserService customSecurityUserService;
	@Autowired
	private ApprovalMatrixService approvalMatrix;
	@Autowired
	private CodePropertiesService codePropertiesService;
	@Autowired
	private HrOverridePermissionsService overridePermissionsService;
	@Autowired
	private UserRolePermissionService userRolePermissionService;
	
	private static final Logger LOGGER = LoggerFactory.getLogger(Notifier.class);
	
	private static DateTimeFormatter DATE_FORMAT = DateTimeFormat.forPattern("MMM d, yyyy - HH:mm");
	private static final String TYPE_FIELD = "type";
	private static final Map<NotificationType, String> URL_MAPPING = new EnumMap<NotificationType, String>(NotificationType.class);
	private static final Map<NotificationType, String> CHANNEL_MAPPING = Maps.newEnumMap(NotificationType.class);
	private static final String MAIN_PATH = "/notification/notify";
	private static final String UPDATE_PATH = "/notification/update";
	
	static {
		URL_MAPPING.put(NotificationType.PROMOTION, "/promotion/edit/");
		URL_MAPPING.put(NotificationType.REDEMPTION_PROMO, "/promotion/redemption/edit/");
		URL_MAPPING.put(NotificationType.POINTS, "/points/override/one/");
		URL_MAPPING.put(NotificationType.PURCHASETXN, "/employee/adjustments/purchases/forapproval/");
		URL_MAPPING.put(NotificationType.REWARDS, "/pointsreward/forprocessing");
		URL_MAPPING.put(NotificationType.MANUFACTURE_ORDER, "/manufactureorder/");
		URL_MAPPING.put(NotificationType.LOYALTY_ALLOCATION, "/loyaltycardinventory/show/");
		URL_MAPPING.put(NotificationType.MONITOR_MEMBER, "/member/monitor/cs/");
		URL_MAPPING.put(NotificationType.MONITOR_EMPLOYEE, "/member/monitor/hr/");
		URL_MAPPING.put(NotificationType.PROMO_BANNER, "/promobanner/update/");
		URL_MAPPING.put(NotificationType.LOYALTY_BURN, "/loyaltycardinventory/show/");
		URL_MAPPING.put(NotificationType.LOYALTY_CHANGE_ALLOCATION, "/loyaltycardinventory/show/");
		URL_MAPPING.put(NotificationType.STOCK_REQUEST, "/stockrequest/");
		
		CHANNEL_MAPPING.put(NotificationType.PROMOTION, "/promotion");
		CHANNEL_MAPPING.put(NotificationType.REDEMPTION_PROMO, "/promotion/redemption");
		CHANNEL_MAPPING.put(NotificationType.POINTS, "/points");
		CHANNEL_MAPPING.put(NotificationType.PURCHASETXN, "/purchasetxn");
		CHANNEL_MAPPING.put(NotificationType.REWARDS, "/rewards");
		CHANNEL_MAPPING.put(NotificationType.MANUFACTURE_ORDER, "/mo");
		CHANNEL_MAPPING.put(NotificationType.LOYALTY_ALLOCATION, "/loyalty");
		CHANNEL_MAPPING.put(NotificationType.MONITOR_MEMBER, "/monitor/cs");
		CHANNEL_MAPPING.put(NotificationType.MONITOR_EMPLOYEE, "/monitor/hr");
		CHANNEL_MAPPING.put(NotificationType.PROMO_BANNER, "/promobanner");
		CHANNEL_MAPPING.put(NotificationType.LOYALTY_BURN, "/loyalty/burn");
		CHANNEL_MAPPING.put(NotificationType.LOYALTY_CHANGE_ALLOCATION, "/loyalty/change");
		CHANNEL_MAPPING.put(NotificationType.STOCK_REQUEST, "/stockrequest");
	}
	
	@AfterReturning(value="@annotation(notifiable)", returning="retVal")
	public void processAnnotation(Notifiable notifiable, Object retVal) {
//		System.out.println("NOTIFICATION");
		NotificationType modelType = notifiable.type();
		Notification notification = new Notification();
        notification.setType(modelType);
        String channel = MAIN_PATH + CHANNEL_MAPPING.get(modelType);
        List<UserModel> users = null;
        Object result = null;
        String args = null;
        
        try {
			if(retVal instanceof ControllerResponse) {
				ControllerResponse response = (ControllerResponse) retVal;
				
				if(!response.isSuccess() || !response.isNotificationCreation()) {
					return;
				}
				LOGGER.debug("NOTIFICATION: " + modelType);
		        result = response.getResult();
		    	notification.setItemId(response.getModelId());
		        
		        if(modelType == NotificationType.POINTS) {
		        	if((Boolean) ((Object[])result)[0]) {
		        		channel += "/hr";
		        	}
		        	else {
		        		channel += "/cs";
		        	}
		        }
		        
				switch(modelType) {
					case LOYALTY_ALLOCATION:
					case LOYALTY_BURN:
					case LOYALTY_CHANGE_ALLOCATION:
					case MANUFACTURE_ORDER:
					case STOCK_REQUEST:
						args = response.getResult().toString();
						break;
					default:
						args = null;
				}
			}
			else if(retVal instanceof ModelAndView) {
				ModelAndView mv = (ModelAndView) retVal;
				if(!mv.getModel().containsKey("new")) {
					return;
				}
				notification.setItemId((String) (mv.getModel().get("itemId")));
			}
			
//			System.out.println("ITEM ID: " + notification.getItemId());
			LOGGER.debug("ITEM ID: " + notification.getItemId());
			users = findNotifiableUsers(modelType, result);
			
			notification.setInfo(args);
			notification = notificationService.saveNotification(notification, users);
			
			if(notification != null) {
				Map<String, Object> output = createNotificationOutput(notification, args);
//				System.out.println("Notification Channel: " + channel);
		        LOGGER.debug("Notification Channel: " + channel);
	        	notifierService.sendToChannel(channel, output);
	        }
        }
        catch(Exception e) {
        	LOGGER.error("Notification Error: " + e.getMessage(), e);
        }
	}
	
	private List<UserModel> findNotifiableUsers(NotificationType type, Object model) {
		List<UserModel> users = Lists.newArrayList();
		if (type == NotificationType.POINTS) {
			Object[] points = (Object[])model;
			
			ApprovalMatrixSearchDto searchFields = new ApprovalMatrixSearchDto();
			searchFields.setModelType(type.toString());
			searchFields.setAmount((Double) points[1]);
			
			String role = null;
			String permission = null;
			if((Boolean) points[0]) {
				LOGGER.debug("NOTIFICATION: HR");
				role = (codePropertiesService.getHrsCode());
				permission = codePropertiesService.getPermissionEmployeePointsAdjustment();
			}
			else {
				LOGGER.debug("NOTIFICATION: CS");
				role = (codePropertiesService.getCssCode());
				permission = codePropertiesService.getPermissionMemberPointsAdjustment();
			}
			
			Collection<ApprovalMatrixDto> list = approvalMatrix.searchApprover(searchFields).getResults();
			for(ApprovalMatrixDto item : list) {
				for(UserRolePermDto permissions : userRolePermissionService.listUserRolePermissions(
						item.getUser().getId()).get(new UserRolePermDto(role))) {
					if(permissions.getCode().equals(permission)) {
						if(permissions.isCurrentRolePerm()) {
							users.add(item.getUser());
						}
						break;
					}
				}
			}
		}
		else if (type == NotificationType.PURCHASETXN) {
			ApprovalMatrixSearchDto searchFields = new ApprovalMatrixSearchDto();
			searchFields.setModelType(type.toString());
			Collection<ApprovalMatrixDto> list = approvalMatrix.searchApprover(searchFields).getResults();
			for(ApprovalMatrixDto item : list) {
				users.add(item.getUser());
			}
		}
		else if (type == NotificationType.MONITOR_MEMBER || type == NotificationType.MONITOR_EMPLOYEE) {
			if(model.toString().equals(codePropertiesService.getDetailMemberTypeEmployee())) {
				users.addAll(userService.findUsersByRoleCode(codePropertiesService.getHrCode()));
				for(UserModel user : userService.findUsersByRoleCode(codePropertiesService.getHrsCode())) {
					if(!users.contains(user)) {
						users.add(user);
					}
				}
			}
			else {
				users.addAll(userService.findUsersByRoleCode(codePropertiesService.getCsCode()));
				for(UserModel user : userService.findUsersByRoleCode(codePropertiesService.getCssCode())) {
					if(!users.contains(user)) {
						users.add(user);
					}
				}
			}
		}
		else if (type == NotificationType.STOCK_REQUEST) {
			for(UserModel user : userService.findUsersByRoleCode(codePropertiesService.getMerchantServiceSupervisorCode())) {
				if(user.getInventoryLocation() != null && user.getInventoryLocation().equals(codePropertiesService.getDetailInvLocationHeadOffice())) {
					users.add(user);
				}
			}
		}
		else {
			String userRole = null;
			String permission = null;
			switch (type) {
				case REWARDS:
					userRole = codePropertiesService.getHrsCode();
					permission = codePropertiesService.getPermissionEmployeeRewardsApproval();
					break;
				case PROMOTION:
					userRole = codePropertiesService.getMktHeadCode();
					permission = codePropertiesService.getPermissionPromotionsManagement();
					break;
				case PROMO_BANNER:
					userRole = codePropertiesService.getMktHeadCode();
					permission = codePropertiesService.getPermissionPromoBannerManagement();
					break;
				case LOYALTY_ALLOCATION:
					permission = codePropertiesService.getPermissionLoyaltyCardInventory();
					userRole = codePropertiesService.getMerchantServiceSupervisorCode();
					break;
				case MANUFACTURE_ORDER:
					permission = codePropertiesService.getPermissionLoyaltyCardManufactureOrder();
					userRole = codePropertiesService.getMerchantServiceSupervisorCode();
					break;
				case LOYALTY_BURN:
					permission = codePropertiesService.getPermissionLoyaltyCardInventory();
					userRole = codePropertiesService.getMerchantServiceSupervisorCode();
					break;
				case LOYALTY_CHANGE_ALLOCATION:
					permission = codePropertiesService.getPermissionLoyaltyCardInventory();
					userRole = codePropertiesService.getMerchantServiceSupervisorCode();
					break;
			}
			
			if(userRole != null) {
	        	for(UserModel user : userService.findUsersByRoleCode(userRole)) {
//	        		LOGGER.info("NOTIFICATION USER: " + user.getUsername());
	        		for(UserRolePermission permissions : user.getRolePermissions()) {
//	        			LOGGER.info("PERMISSION: " + (permissions.getPermId() == null ? "null" : permissions.getPermId().getDescription()));
	        			if(permissions.getPermId() != null && 
	        					permissions.getPermId().getCode().equals(permission)) {
	        				users.add(user);
	        				break;
	        			}
	        		}
	        	}
	        }
	        else {
	        	users = userService.findAllUserModels();
	        }
		}
		return users;
	}
	
	@AfterReturning(value="@annotation(com.transretail.crm.common.web.notification.annotation.NotificationUpdate)", returning="retVal")
	public void deleteNotification(Object retVal) {
		LOGGER.debug("Delete notification on approve");
		String modelId = null;
		if(retVal != null) {
			if(retVal instanceof ControllerResponse) {
				ControllerResponse response = (ControllerResponse) retVal;
				if(!response.isSuccess() || response.isNotificationCreation()) {
					return;
				}
				
				LOGGER.debug("NOTIFICATION: DELETING");
				modelId = response.getModelId();
				if ( CollectionUtils.isNotEmpty( response.getModelIds() ) ) {
					for ( String modId : response.getModelIds() ) {
						LOGGER.debug("ID: " + modId);
						if(StringUtils.isNotBlank(modId)) {
							notificationService.deleteNotification(modId);
						}
					}
				}
			}
			else if(retVal instanceof ModelAndView) {
				ModelAndView mv = (ModelAndView) retVal;
				if(mv.getModel().containsKey("update")) {
					modelId = (String) (mv.getModel().get("itemId"));
				}
			}
			
			LOGGER.debug("ID: " + modelId);
			if(StringUtils.isNotBlank(modelId)) {
				notificationService.deleteNotification(modelId);
			}
		}
	}
	
	@RequestMapping(value="/delete/{type}/{id}", method=RequestMethod.DELETE, produces="application/json")
	@ResponseBody
	public ControllerResponse deleteNotification(@PathVariable("type") String type, @PathVariable("id") String id) {
		ControllerResponse response = new ControllerResponse();
		notificationService.deleteNotification(id);
		response.setSuccess(true);
		LOGGER.debug("delete notification : " + id);
		return response;
	}
	
	@RequestMapping(value="/list", method=RequestMethod.POST, produces="application/json")
	@ResponseBody
	public ControllerResponse getAllNotifications() {
		LOGGER.debug("GET NOTIFICATIONS");
		
		ControllerResponse response = new ControllerResponse();
		
		CustomSecurityUserDetailsImpl currentUser = 
    			(CustomSecurityUserDetailsImpl) customSecurityUserService.getCurrentAuth().getPrincipal();
		UserModel user = userService.findUserModel(currentUser.getUserId());
		List<Notification> notifications = notificationService.getNotificationsByUser(user);
		List<Map<String, Object>> output = new ArrayList<Map<String,Object>>();
		
		for(Notification notif : notifications) {
			output.add(createNotificationOutput(notif, notif.getInfo()));
		}
		
		response.setResult(output);
		response.setSuccess(true);
		
		LOGGER.debug("" + output.size());
		
		return response;
	}
	
	@RequestMapping(value="/channels", method=RequestMethod.POST, produces="application/json")
	@ResponseBody
	public ControllerResponse getAllowedChannels() {
		ControllerResponse response = new ControllerResponse();
		CustomSecurityUserDetailsImpl currentUser = 
    			(CustomSecurityUserDetailsImpl) customSecurityUserService.getCurrentAuth().getPrincipal();
		
		String hrsCode = codePropertiesService.getHrsCode();
		String cssCode = codePropertiesService.getCssCode();
		String gcsCode = codePropertiesService.getMerchantServiceSupervisorCode();
		String csCode = codePropertiesService.getCsCode();
		String hrCode = codePropertiesService.getHrCode();
		String mktCode = codePropertiesService.getMktHeadCode();
		
		List<String> channelList = new ArrayList<String>();
		
		if(overridePermissionsService.isApprover(currentUser)) {
			channelList.add(CHANNEL_MAPPING.get(NotificationType.PURCHASETXN));
		}
		
		boolean isPointsApprover = approvalMatrix.findHighestApprovalPointsOfCurrentUser() != null;
		
		if(currentUser.hasAuthority(hrsCode)) {
			channelList.add(CHANNEL_MAPPING.get(NotificationType.REWARDS));
			
			if(isPointsApprover) {
				channelList.add(CHANNEL_MAPPING.get(NotificationType.POINTS) + "/hr");
			}
		}
		
		if(currentUser.hasAuthority(hrsCode) || currentUser.hasAuthority(hrCode)) {
			channelList.add(CHANNEL_MAPPING.get(NotificationType.MONITOR_EMPLOYEE));
		}
		
		if(currentUser.hasAuthority(csCode) || currentUser.hasAuthority(cssCode)) {
			channelList.add(CHANNEL_MAPPING.get(NotificationType.MONITOR_MEMBER));
			
			if(isPointsApprover) {
				channelList.add(CHANNEL_MAPPING.get(NotificationType.POINTS) + "/cs");
			}
		}
		
		if(currentUser.hasAuthority(gcsCode)) {
			channelList.add(CHANNEL_MAPPING.get(NotificationType.LOYALTY_ALLOCATION));
			channelList.add(CHANNEL_MAPPING.get(NotificationType.MANUFACTURE_ORDER));
			channelList.add(CHANNEL_MAPPING.get(NotificationType.LOYALTY_BURN));
			channelList.add(CHANNEL_MAPPING.get(NotificationType.LOYALTY_CHANGE_ALLOCATION));
			channelList.add(CHANNEL_MAPPING.get(NotificationType.STOCK_REQUEST));
		}
		
		if(currentUser.hasAuthority(mktCode)) {
			channelList.add(CHANNEL_MAPPING.get(NotificationType.PROMOTION));
			channelList.add(CHANNEL_MAPPING.get(NotificationType.PROMO_BANNER));
		}
		
		response.setSuccess(true);
		response.setResult(channelList);
		
		return response;
	}
	
	
	private Map<String, Object> createNotificationOutput(Notification notification, Object args) {
		NotificationType modelType = notification.getType();
		String modelTypeDesc = modelType.toString().toLowerCase();
		Map<String, Object> output = new HashMap<String, Object>();
        output.put("message", messageSource.getMessage("label_notification_" + modelTypeDesc, new Object[]{args}, 
        		LocaleContextHolder.getLocale()));
        if(modelType == NotificationType.REWARDS) {
        	output.put("url", URL_MAPPING.get(modelType));
        }
        else if (modelType == NotificationType.MONITOR_MEMBER || modelType == NotificationType.MONITOR_EMPLOYEE) {
        	output.put("url", URL_MAPPING.get(modelType) + notification.getCreated().minusDays(1).toString("dd-MM-yyyy"));
        }
        else {
        	output.put("url", URL_MAPPING.get(modelType) + notification.getItemId());
        }
        output.put("time", notification.getCreated().toString(DATE_FORMAT));
        output.put("type", modelTypeDesc);
        output.put("id", notification.getItemId());
        return output;
	}
}
