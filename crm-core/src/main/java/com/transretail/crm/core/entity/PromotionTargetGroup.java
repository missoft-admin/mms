package com.transretail.crm.core.entity;

import java.io.Serializable;

import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.MappedSuperclass;

import com.transretail.crm.core.entity.support.CustomIdAuditableEntity;

/**
 * @author Allan G. Ramirez (agramirez@exist.com)
 */
@MappedSuperclass
public abstract class PromotionTargetGroup<PK extends Serializable> extends CustomIdAuditableEntity<PK> {
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "PROMOTION_ID")
    private Promotion promotion;

    public Promotion getPromotion() {
        return promotion;
    }

    public void setPromotion(Promotion promotion) {
        this.promotion = promotion;
    }
}
