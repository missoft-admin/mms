package com.transretail.crm.product;

/**
 * @author Allan G. Ramirez (agramirez@exist.com)
 */
public enum HierarchyLevel {
    NO_LEVEL("No Level"), DIVISION("Division"), DEPARTMENT("Department"), GROUP_FAMILY("Group Family"), FAMILY("Family"), SUB_FAMILY(
        "Sub Family");

    private String name;

    HierarchyLevel(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return name;
    }
}
