package com.transretail.crm.core.dto;


import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang3.StringUtils;

import com.transretail.crm.core.entity.Campaign;
import com.transretail.crm.core.entity.Program;
import com.transretail.crm.core.entity.embeddable.Duration;
import com.transretail.crm.core.entity.lookup.LookupDetail;
import com.transretail.crm.core.util.CalendarUtil;

public class ProgramDto {

//	private static final String[] IGNORE = { "status", "campaigns" };

	private Long id;
	private String status;
	private String name;
    private String description;
    private Long startDate;
    private Long endDate;
    private String dispStartDate;
    private String dispEndDate;

    private List<CampaignDto> campaigns;
    
	public ProgramDto() {}
	public ProgramDto( Program inProgram, boolean renderCampaign ) {
//		BeanUtils.copyProperties( inProgram, this, IGNORE );
        this.id = inProgram.getId();
        if ( null != inProgram.getStatus() ) {
            status = inProgram.getStatus().getCode();
        }
		this.name = inProgram.getName();
		this.description = inProgram.getDescription();
		if ( null != inProgram.getDuration() ) {
			if ( null != inProgram.getDuration().getStartDate() ) {
//				startDate = new DateTime(inProgram.getDuration().getStartDate().getTime());
//				dispStartDate = inProgram.getDuration().getStartDate();
				startDate = inProgram.getDuration().getStartDate().getTime();
				dispStartDate = CalendarUtil.INSTANCE.getDateFormatter().print(startDate);
			}
			if ( null != inProgram.getDuration().getEndDate() ) {
//				endDate = new DateTime(inProgram.getDuration().getEndDate().getTime());
//				dispEndDate = inProgram.getDuration().getEndDate();
				endDate = inProgram.getDuration().getEndDate().getTime();
				dispEndDate = CalendarUtil.INSTANCE.getDateFormatter().print(endDate);
			}
		}
		if ( null != inProgram.getCampaigns() && renderCampaign ) {
			campaigns = new ArrayList<CampaignDto>();
			for ( Campaign theCampaign : inProgram.getCampaigns() ) {
				campaigns.add( new CampaignDto( theCampaign, true ) );
			}
		}
	}

	public Program toModel() {
		Program theModel = new Program();
//		BeanUtils.copyProperties( this, theModel, IGNORE );
        if(StringUtils.isNotBlank(this.status)) {
            theModel.setStatus(new LookupDetail(this.status));
        }
        theModel.setName(this.name);
        theModel.setDescription(this.description);
		Duration theDuration = new Duration();
//		theDuration.setStartDate(startDate.toDate());
//		theDuration.setEndDate(endDate.toDate());
		theDuration.setStartDate( new Date( startDate ) );
		theDuration.setEndDate( new Date( endDate ) );
		theModel.setDuration( theDuration );
		return theModel;
	}




	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Long getStartDate() {
		return startDate;
	}

	public void setStartDate(Long startDate) {
		this.startDate = startDate;
	}

	public Long getEndDate() {
		return endDate;
	}

	public void setEndDate(Long endDate) {
		this.endDate = endDate;
	}

	public String getDispStartDate() {
		return dispStartDate;
	}

	public void setDispStartDate(String dispStartDate) {
		this.dispStartDate = dispStartDate;
	}

	public String getDispEndDate() {
		return dispEndDate;
	}

	public void setDispEndDate(String dispEndDate) {
		this.dispEndDate = dispEndDate;
	}

	public List<CampaignDto> getCampaigns() {
		return campaigns;
	}

	public void setCampaigns(List<CampaignDto> campaigns) {
		this.campaigns = campaigns;
	}
}
