var CONTAINER_ERROR = "#contentError > div";
var CONTENT_ERROR = "#contentError";
var ID_MEMBER_UPDATEFORM = "voucherScheme"; //reference = rendered id from memberDialog.jspx (modelAttribute value)

$(document).ready(function() {
	$(".floatInput").numberInput({
        "type" : "float"
    });
	$(".intInput").numberInput({
        "type" : "int"
    });
	
	$( "#wrapperDialog" ).modal({
		show: false
	});
	
    $("#cancelButton").click( function(event) {
        $('#wrapperDialog').modal("hide");
		$("#label_memberType").html( '' );
    	$("#contentCompanyName > div").hide();
    	$("form[id='" + ID_MEMBER_UPDATEFORM + "']").trigger( "reset" );
    	$( CONTENT_ERROR ).hide();
    	$( CONTAINER_ERROR ).empty();
    	$("input[name='memberType']").each( function() {
    		$(this).attr( 'checked', false) ;
    	});
    	$("input[name='username']").val("");
    });
});


function createVoucherScheme( theAction, inQueryString ) {

	$( '#wrapperDialog' ).modal("show");
	
	var CONTROLLER_POPULATEVS = "/populate/" + null;
	populate( CONTROLLER_POPULATEVS );
	
	var CONTROLLER_CREATEVS = "/create";	
	doAjaxControllerSaveMember( CONTROLLER_CREATEVS, inQueryString, "POST" );
}


function updateVoucherScheme( theMemberId, inQueryString ) {

	$( '#wrapperDialog' ).modal("show");
	var CONTROLLER_POPULATEVS= "/populate/" + theMemberId;
	populate( CONTROLLER_POPULATEVS );
	
	var CONTROLLER_UPDATEVS = "/update/" + theMemberId;
	doAjaxControllerSaveMember( CONTROLLER_UPDATEVS, inQueryString, "POST" );
}


function populate( inPopulateUrl ) {
    var theParentForm = $("form[id='" + ID_MEMBER_UPDATEFORM + "']");
    
	$.ajax({
		type 	: "POST",
		url 	: $(theParentForm).attr("action") + inPopulateUrl,
		dataType: 'json',
		        	
		success : function(inResponse) {
			var inReturnEntity = inResponse.result;
			
			$.each(inReturnEntity, function(key, value) {
				
//				if ( key == "accountId" ) {
//					//$("#contentInfoAccountId").html( value );
//					//$(theParentForm).addHidden(key, value)
//					return true;
//				}
				
				var theInput = $("input[name='" + key + "']");
				if ( theInput != null ) {
					if ( $(theInput).is(':radio') ) {
						$("input:radio[name='" + key + "']").each( function() {
							$(this).attr( 'checked', $(this).val() == value );
							if($(this).val() == value) {
								$(this).click();
							}
						});
					}
					else {
						$(theInput).val( value );
					}
				}
				
				
				var theSelect = $("select[name='" + key + "']");
				if(theSelect != null) {
					$(theSelect).val(value);
				}
			});
		},
		error : function(jqXHR, textStatus, errorThrown) {
			$( CONTAINER_ERROR ).html("Error: " + errorThrown);
			$( CONTENT_ERROR ).show('slow');
		}
	});
}

function doAjaxControllerSaveMember( inAjaxController, inQueryString, methodType ) {

    var theParentForm = $("form[id='" + ID_MEMBER_UPDATEFORM + "']");
    
	$("#proceed").unbind("click").click( function() {
		$( "#proceed" ).prop( "disabled", true );
	    $.ajax({
			type 	: methodType,
			url 	: $(theParentForm).attr("action") + inAjaxController,
			dataType: 'json',
			data 	: $(theParentForm).serialize(),
			        
			success : function(inResponse) {
				if (inResponse.success) {
					$( CONTENT_ERROR ).hide('fast');
					$(theParentForm).unbind("submit").submit( function() {
						$(this).attr("action", $(this).attr("action") + "?" + inQueryString);
					});
					$(theParentForm).submit();
				} 
				else {
					errorInfo = "";
					for (i = 0; i < inResponse.result.length; i++) {
						errorInfo += "<br>" + (i + 1) + ". "
								+ ( inResponse.result[i].code != undefined ? 
										inResponse.result[i].code : inResponse.result[i]);
					}
					$( CONTAINER_ERROR ).html("Please correct following errors: " + errorInfo);
					$( CONTENT_ERROR ).show('slow');
				}
				$( "#proceed" ).prop( "disabled", false );
			},
			error : function(jqXHR, textStatus, errorThrown) {
				$( CONTAINER_ERROR ).html("Error: " + errorThrown);
				$( CONTENT_ERROR ).show('slow');
				$( "#proceed" ).prop( "disabled", false );
			}
		});
	});


    $("#cancelButton").click( function() {
    	$(theParentForm).trigger( "reset" );
    });
}