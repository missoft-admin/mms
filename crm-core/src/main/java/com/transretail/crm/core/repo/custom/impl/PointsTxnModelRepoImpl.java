package com.transretail.crm.core.repo.custom.impl;


import com.google.common.collect.Lists;
import com.mysema.query.BooleanBuilder;
import com.mysema.query.Tuple;
import com.mysema.query.jpa.JPQLQuery;
import com.mysema.query.jpa.hibernate.HibernateSubQuery;
import com.mysema.query.jpa.impl.JPAQuery;
import com.mysema.query.types.ConstructorExpression;
import com.mysema.query.types.expr.BooleanExpression;
import com.transretail.crm.common.service.dto.request.PagingParam;
import com.transretail.crm.common.service.dto.response.ResultList;
import com.transretail.crm.common.service.util.SpringDataPagingUtil;
import com.transretail.crm.core.dto.PointsDto;
import com.transretail.crm.core.dto.PointsExpireDto;
import com.transretail.crm.core.dto.PointsExpireReportDto;
import com.transretail.crm.core.dto.PointsSearchDto;
import com.transretail.crm.core.entity.PointsTxnModel;
import com.transretail.crm.core.entity.QMemberModel;
import com.transretail.crm.core.entity.QPointsTxnModel;
import com.transretail.crm.core.entity.enums.MemberStatus;
import com.transretail.crm.core.entity.enums.PointTxnType;
import com.transretail.crm.core.entity.enums.TxnStatus;
import com.transretail.crm.core.repo.custom.PointsTxnRepoCustom;
import com.transretail.crm.core.util.BooleanExprUtil;

import org.apache.commons.lang3.StringUtils;
import org.joda.time.DateTime;
import org.joda.time.LocalDate;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;


public class PointsTxnModelRepoImpl implements PointsTxnRepoCustom {

    @PersistenceContext
    EntityManager em;


    @Override
    public List<PointsTxnModel> findPointsTxnByDateRange(String accountId, Date fromDate, Date toDate) {
        JPAQuery theQuery = new JPAQuery(em);
        QPointsTxnModel theModel = QPointsTxnModel.pointsTxnModel;

        theQuery.from(theModel).where(
                new BooleanBuilder().
                and(BooleanExprUtil.INSTANCE.isStringPropertyEqual(theModel.memberModel.accountId, accountId)).
                and(theModel.transactionDateTime.between(fromDate, toDate)));
        return theQuery.list(theModel);
    }

	@Override
	public List<PointsTxnModel> findAdjustPointsByTxnTypeAndHighestPoint( TxnStatus inType, Long inHighestPoint ) {

		return findByTxnTypeAndHighestPoint( PointTxnType.ADJUST, inType, inHighestPoint );
	}

	@Override
	public List<PointsTxnModel> findByTxnTypeAndLowestPoint( TxnStatus inType, Long inLowestPoint ) {

        JPAQuery theQuery = new JPAQuery(em);
        QPointsTxnModel thePoint = QPointsTxnModel.pointsTxnModel;

		return theQuery.from( thePoint ).
	        	where( new BooleanBuilder().orAllOf( 
	        			thePoint.status.eq( inType ),
	        			thePoint.transactionPoints.gt( inLowestPoint ) ) ).
	        	orderBy( thePoint.transactionPoints.desc() ).
	        	list( thePoint );
	}


	@Override
	public List<PointsTxnModel> findByIds( List<String> inIds ) {

        JPAQuery theQuery = new JPAQuery(em);
        QPointsTxnModel thePoint = QPointsTxnModel.pointsTxnModel;

		return theQuery.from( thePoint ).
	        	where( new BooleanBuilder().orAllOf( 
	        			thePoint.id.in( inIds ) ) ).
	        	list( thePoint );
	}


	@Override
	public Double getTotalPoints( String inAccountId ) {

        JPAQuery theQuery = new JPAQuery(em);
        QPointsTxnModel thePoint = QPointsTxnModel.pointsTxnModel;

        return theQuery.from( thePoint ).
        		where( new BooleanBuilder().orAllOf( 
        				BooleanExprUtil.INSTANCE.isStringPropertyEqual( thePoint.memberModel.accountId, inAccountId ),
        				BooleanExprUtil.INSTANCE.isEnumPropertyIn( thePoint.memberModel.accountStatus, MemberStatus.ACTIVE ),
        				new BooleanBuilder().andAnyOf( 
        						BooleanExprUtil.INSTANCE.isEnumPropertyIn( thePoint.status, TxnStatus.ACTIVE ),
        						thePoint.status.isNull() ) ) ).
        		singleResult( thePoint.transactionPoints.sum() );
	}
	
	@Override
	public Double getTotalPointsSupplement(String inAccountId) {
		JPAQuery theQuery = new JPAQuery(em);
		QPointsTxnModel thePoint = QPointsTxnModel.pointsTxnModel;
		
		return theQuery.from( thePoint ).
        		where( new BooleanBuilder().orAllOf( 
        				BooleanExprUtil.INSTANCE.isStringPropertyEqual( thePoint.transactionContributer.accountId, inAccountId ),
        				BooleanExprUtil.INSTANCE.isEnumPropertyIn( thePoint.memberModel.accountStatus, MemberStatus.ACTIVE ),
        				new BooleanBuilder().andAnyOf( 
        						BooleanExprUtil.INSTANCE.isEnumPropertyIn( thePoint.status, TxnStatus.ACTIVE ),
        						thePoint.status.isNull() ) ) ).
        		singleResult( thePoint.transactionPoints.sum() );
	}



	@Override
	public List<PointsTxnModel> findAdjustPointsTxnForProcessing() {

        return findPointsTxnForProcessing( PointTxnType.ADJUST, null, null );
	}


	@Override
	public Date getLatestRewardGenerationToDate() {

        JPAQuery theQuery = new JPAQuery(em);
        QPointsTxnModel thePointsTxn = QPointsTxnModel.pointsTxnModel;

        return theQuery.from( thePointsTxn ).
        		where( thePointsTxn.incDateTo.isNotNull() ).
        		singleResult( thePointsTxn.incDateTo.max() );
    }


	@Override
	public Double getTotalPointsRewardForProcessing( Date inFromDate, Date inToDate ) {

		JPAQuery theQuery = new JPAQuery(em);
        QPointsTxnModel theReward = QPointsTxnModel.pointsTxnModel;

        return theQuery.from( theReward ).
		        	where( new BooleanBuilder().orAllOf( 
    						BooleanExprUtil.INSTANCE.isEnumPropertyIn( theReward.transactionType, PointTxnType.REWARD ),
	                		new BooleanBuilder().andAnyOf( 
	                				BooleanExprUtil.INSTANCE.isEnumPropertyIn( theReward.status, TxnStatus.FORAPPROVAL ),
	                				BooleanExprUtil.INSTANCE.isEnumPropertyIn( theReward.status, TxnStatus.REJECTED ) ),
		                	BooleanExprUtil.INSTANCE.isDatePropertyGoe( theReward.incDateFrom, inFromDate ), 
		                	BooleanExprUtil.INSTANCE.isDatePropertyLoe( theReward.incDateTo, inToDate ) ) ).
		            singleResult( theReward.transactionPoints.sum() );
	}


	@Override
	public List<PointsTxnModel> findPointsRewardDeleted( Date inFromDate, Date inToDate ) {

        return findPointsTxnDeleted( PointTxnType.REWARD, inFromDate, inToDate );
	}


	@Override
	public List<PointsTxnModel> findPointsRewardForProcessing( Date inFromDate, Date inToDate ) {

		return findPointsTxnForProcessing( PointTxnType.REWARD, inFromDate, inToDate );
	}


	@Override
	public List<PointsTxnModel> findPointsRewardForApproval( Date inFromDate, Date inToDate ) {

		return findPointsTxnForApproval( PointTxnType.REWARD, inFromDate, inToDate );
	}




	private List<PointsTxnModel> findByTxnTypeAndHighestPoint( PointTxnType inTxnType, TxnStatus inType, Long inHighestPoint ) {

		JPAQuery theQuery = new JPAQuery(em);
        QPointsTxnModel thePoint = QPointsTxnModel.pointsTxnModel;

		return theQuery.from( thePoint ).
	        	where( new BooleanBuilder().orAllOf( 
	        			thePoint.transactionType.eq( inTxnType ),
	        			thePoint.status.eq( inType ),
	        			thePoint.transactionPoints.loe( inHighestPoint ) ) ).
	        	orderBy( thePoint.transactionPoints.desc() ).
	        	list( thePoint );
	}

	private List<PointsTxnModel> findPointsTxnDeleted( PointTxnType inTxnType, Date inFromDate, Date inToDate ) {

		JPAQuery theQuery = new JPAQuery(em);
        QPointsTxnModel thePoint = QPointsTxnModel.pointsTxnModel;
        
        return theQuery.from( thePoint ).
        		where(  new BooleanBuilder().orAllOf( 
	        				new BooleanBuilder().orAllOf( 
	        						BooleanExprUtil.INSTANCE.isEnumPropertyIn( thePoint.transactionType, inTxnType ),
	        						BooleanExprUtil.INSTANCE.isDatePropertyEqual( thePoint.incDateFrom, inFromDate ),
	        						BooleanExprUtil.INSTANCE.isDatePropertyEqual( thePoint.incDateTo, inToDate ) ),
	        				new BooleanBuilder().andAnyOf( 
	        						thePoint.status.in( TxnStatus.DELETED ) ) ) ).
        		orderBy( thePoint.transactionPoints.desc() ).
        		list( thePoint );
	}

	private List<PointsTxnModel> findPointsTxnForProcessing( PointTxnType inTxnType, Date inFromDate, Date inToDate ) {

		JPAQuery theQuery = new JPAQuery(em);
        QPointsTxnModel thePoint = QPointsTxnModel.pointsTxnModel;
        
        return theQuery.from( thePoint ).
        		where(  new BooleanBuilder().orAllOf( 
	        				new BooleanBuilder().orAllOf( 
	        						BooleanExprUtil.INSTANCE.isEnumPropertyIn( thePoint.transactionType, inTxnType ),
	        						BooleanExprUtil.INSTANCE.isDatePropertyEqual( thePoint.incDateFrom, inFromDate ),
	        						BooleanExprUtil.INSTANCE.isDatePropertyEqual( thePoint.incDateTo, inToDate ) ),
	        				new BooleanBuilder().andAnyOf( 
	        						thePoint.status.in( TxnStatus.FORAPPROVAL ), 
	                				thePoint.status.in( TxnStatus.REJECTED ) ) ) ).
        		orderBy( thePoint.transactionPoints.desc() ).
        		list( thePoint );
	}

	@Override
	public ResultList<PointsDto> listPointsTxnForProcessing( PointTxnType inTxnType, Date inFromDate, Date inToDate, PointsSearchDto sortDto ) {

        QPointsTxnModel qPoint = QPointsTxnModel.pointsTxnModel;
        
        JPQLQuery query = new JPAQuery(em).from( qPoint ).
        		where(  new BooleanBuilder().orAllOf( 
        				new BooleanBuilder().orAllOf( 
        						BooleanExprUtil.INSTANCE.isEnumPropertyIn( qPoint.transactionType, inTxnType ),
        						BooleanExprUtil.INSTANCE.isDatePropertyEqual( qPoint.incDateFrom, inFromDate ),
        						BooleanExprUtil.INSTANCE.isDatePropertyEqual( qPoint.incDateTo, inToDate ),
        						sortDto.createSearchExpression()),
        				new BooleanBuilder().andAnyOf( 
        						qPoint.status.in( TxnStatus.FORAPPROVAL ), 
                				qPoint.status.in( TxnStatus.REJECTED ) ) ) );

        Long count = query.count();
        PagingParam pagination = sortDto.getPagination();
        if (pagination != null) {
            query = SpringDataPagingUtil.INSTANCE.applyPagination(query, pagination, PointsTxnModel.class);
            return new ResultList<PointsDto>(convertPointsToDto(query.list(qPoint)),
                    count != null ? count : 0,
                    pagination.getPageNo(), pagination.getPageSize());
        } else {
            return new ResultList<PointsDto>(convertPointsToDto(query.list(qPoint)));
        }
        

	}
	
	private List<PointsDto> convertPointsToDto( Collection<PointsTxnModel> inItems ) {
        List<PointsDto> results = Lists.newArrayList();
        for (PointsTxnModel theItem : inItems ) {
        	PointsDto theDto = new PointsDto( theItem );
            results.add( theDto );
        }
        
        return results;
    }
	
	private List<PointsTxnModel> findPointsTxnForApproval( PointTxnType inTxnType, Date inFromDate, Date inToDate ) {

		JPAQuery theQuery = new JPAQuery(em);
        QPointsTxnModel thePoint = QPointsTxnModel.pointsTxnModel;
        
        return theQuery.from( thePoint ).
        		where(  new BooleanBuilder().orAllOf( 
	        				new BooleanBuilder().orAllOf( 
	        						BooleanExprUtil.INSTANCE.isEnumPropertyIn( thePoint.transactionType, inTxnType ),
	        						BooleanExprUtil.INSTANCE.isDatePropertyEqual( thePoint.incDateFrom, inFromDate ),
	        						BooleanExprUtil.INSTANCE.isDatePropertyEqual( thePoint.incDateTo, inToDate ) ),
	        				new BooleanBuilder().andAnyOf( 
	        						thePoint.status.in( TxnStatus.FORAPPROVAL ) ) ) ).
        		orderBy( thePoint.transactionPoints.desc() ).
        		list( thePoint );
	}
	
	@Override
	public List<PointsTxnModel> findRewardsByStatusAndDates( TxnStatus status, Date inFromDate, Date inToDate ) {
		return findPointsTxnByStatusAndDates(PointTxnType.REWARD, status, inFromDate, inToDate);
	}
	
	@Override
	public List<PointsTxnModel> findPointsTxnByStatusAndDates( PointTxnType inTxnType, TxnStatus status, Date inFromDate, Date inToDate ) {

		JPAQuery theQuery = new JPAQuery(em);
        QPointsTxnModel thePoint = QPointsTxnModel.pointsTxnModel;
        
        return theQuery.from( thePoint ).
        		where(  new BooleanBuilder().orAllOf( 
						BooleanExprUtil.INSTANCE.isEnumPropertyIn( thePoint.transactionType, inTxnType ),
						/*BooleanExprUtil.INSTANCE.isDatePropertyEqual( thePoint.incDateFrom, inFromDate ),
						BooleanExprUtil.INSTANCE.isDatePropertyEqual( thePoint.incDateTo, inToDate ) ),*/
						thePoint.created.goe( new DateTime( inFromDate.getTime() ) ),
						thePoint.created.loe( new DateTime( inToDate.getTime() ) ), 
						BooleanExprUtil.INSTANCE.isEnumPropertyIn( thePoint.status, status ) ) ).
        		orderBy( thePoint.transactionPoints.desc() ).
        		list( thePoint );
	}
	
	@Override
	public List<PointsTxnModel> findTransactionsToday(String accountId) {
		QPointsTxnModel points = QPointsTxnModel.pointsTxnModel;
//		QMemberModel members = QMemberModel.memberModel;
		JPAQuery query = new JPAQuery(em);
		
		LocalDate today = LocalDate.now();
		Date todayDate = today.toDateTimeAtStartOfDay().toDate();
		Date tomorrowDate = today.plusDays(1).toDateTimeAtStartOfDay().toDate();
		
		return query.from(points)
			.where(points.transactionDateTime.between(todayDate, tomorrowDate)
			.and(points.transactionContributer.accountId.eq(accountId)))
			.list(points);
	}
	
	@Override
	public List<PointsTxnModel> findTransactionsByDate(String accountId, LocalDate date) {
		QPointsTxnModel points = QPointsTxnModel.pointsTxnModel;
//		QMemberModel members = QMemberModel.memberModel;
		JPAQuery query = new JPAQuery(em);
		
		Date todayDate = date.toDateTimeAtStartOfDay().toDate();
		Date tomorrowDate = date.plusDays(1).toDateTimeAtStartOfDay().toDate();
		
		return query.from(points)
			.where(points.transactionDateTime.between(todayDate, tomorrowDate)
			.and(points.transactionContributer.accountId.eq(accountId)))
			.list(points);
	}

	@Override
	public List<PointsTxnModel> findExpiringTransactions(String accountId, LocalDate expiryDate) {
		QPointsTxnModel points = QPointsTxnModel.pointsTxnModel;
		JPAQuery query = new JPAQuery(em);
		
		return query.from(points).where(BooleanExpression.allOf(
				points.memberModel.accountId.eq(accountId),
				points.expiryDate.isNotNull(),
				points.expiryDate.eq(expiryDate)
			)).list(points);
	}
	
	@Override
	public PointsExpireReportDto findPastExpiringTransactions(LocalDate month) {
		LocalDate start = month.dayOfMonth().withMinimumValue();
		LocalDate end = month.dayOfMonth().withMaximumValue();
		
		QPointsTxnModel points = QPointsTxnModel.pointsTxnModel;
		JPAQuery query = new JPAQuery(em);
		
		return query.from(points).where(BooleanExpression.allOf(
				points.transactionType.eq(PointTxnType.EXPIRE),
				points.transactionDateTime.between(start.toDate(), end.toDate())
			))
			.singleResult(ConstructorExpression.create(PointsExpireReportDto.class, 
					points.transactionPoints.sum(), points.transactionAmount.sum()));
	}

	@Override
	public PointsTxnModel findLastExpireTransaction(String accountId) {
		QPointsTxnModel points = QPointsTxnModel.pointsTxnModel;
		JPAQuery query = new JPAQuery(em);
		
		List<PointsTxnModel> list = query.from(points).where(
				points.transactionType.eq(PointTxnType.EXPIRE)
				.and(points.memberModel.accountId.eq(accountId))
			)
			.orderBy(points.created.desc()).list(points);
		
		if(list != null && !list.isEmpty()) {
			return list.get(0);
		}
		return null;
	}

	@Override
	public List<PointsTxnModel> sumExpiringPoints(String accountId, LocalDate expiryDate) {
		QPointsTxnModel points = QPointsTxnModel.pointsTxnModel;
		JPAQuery query = new JPAQuery(em);
		
		return query.from(points).where(BooleanExpression.allOf(
				points.memberModel.accountId.eq(accountId),
				points.transactionPoints.isNotNull(),
				points.transactionPoints.ne(0D),
				points.expiryDate.eq(expiryDate)))
			.orderBy(points.transactionDateTime.asc())
			.list(points);
	}
	
	@Override
	public List<PointsTxnModel> sumPointsToExpireForLoyalty(String accountId, LocalDate expiryDate) {
		QPointsTxnModel points = QPointsTxnModel.pointsTxnModel;
		JPAQuery query = new JPAQuery(em);
		
		return query.from(points).where(BooleanExpression.allOf(
				points.memberModel.accountId.eq(accountId),
				points.transactionPoints.isNotNull(),
				points.transactionPoints.ne(0D),
				points.expiryDate.gt(expiryDate)))
			.list(points);
	}

	@Override
	public List<PointsTxnModel> findDeductablePoints(String accountId, DateTime startDate, LocalDate expiryDate) {
		QPointsTxnModel points = QPointsTxnModel.pointsTxnModel;
		JPAQuery query = new JPAQuery(em);
		
		return query.from(points)
			.where(BooleanExpression.allOf(
				points.memberModel.accountId.eq(accountId),
				// consider redeem and negative adjustments only
				points.transactionType.eq(PointTxnType.REDEEM)
					.or(points.transactionType.eq(PointTxnType.ADJUST)
						.and(points.transactionPoints.lt(0))
						),
				// from last transaction until expiry date
				points.created.gt(startDate)
					.and(points.created.loe(expiryDate.plusDays(1)
						.toDateTimeAtStartOfDay()))
				)
			)
			// order from most recent to least recent
			.orderBy(points.created.desc())
			.list(points);
	}
	
	@Override
	public Object[] sumDeductablePoints(String accountId, DateTime startDate, LocalDate expiryDate) {
		QPointsTxnModel points = QPointsTxnModel.pointsTxnModel;
		JPAQuery query = new JPAQuery(em);
		
		Tuple tuple = query.from(points)
			.where(BooleanExpression.allOf(
				points.memberModel.accountId.eq(accountId),
				// redeem, negative adjustment, return
				points.transactionType.eq(PointTxnType.REDEEM)
					.or(points.transactionType.eq(PointTxnType.ADJUST)
						.and(points.transactionPoints.lt(0))
					.or(points.transactionType.eq(PointTxnType.RETURN))
						),
				// from last transaction until expiry date
				points.created.gt(startDate)
					.and(points.created.loe(expiryDate.plusDays(1)
						.toDateTimeAtStartOfDay()))
				)
			)
			.singleResult(points.transactionPoints.sum(), points.created.max());
		
		return new Object[] {tuple.get(points.transactionPoints.sum()), tuple.get(points.created.max())};
	}
	
	@Override
	public List<PointsExpireDto> expirePoints(Date startDate, LocalDate expiryDate) {
		QPointsTxnModel points = QPointsTxnModel.pointsTxnModel;
		QMemberModel members = QMemberModel.memberModel;
		JPAQuery query = new JPAQuery(em);
		
		return query.from(members)
			.list(ConstructorExpression.create(PointsExpireDto.class, members,
				new HibernateSubQuery().from(points)
					.where(BooleanExpression.allOf(
						points.memberModel.accountId.eq(members.accountId),
						points.expiryDate.isNotNull(),
						points.expiryDate.eq(expiryDate)))
					.groupBy(members).list(points.transactionPoints.sum()),
				new HibernateSubQuery().from(points)
					.where(BooleanExpression.allOf(
						points.memberModel.accountId.eq(members.accountId),
						points.transactionType.eq(PointTxnType.REDEEM)
							.or(points.transactionType.eq(PointTxnType.ADJUST)
									.and(points.transactionPoints.loe(0))),
						points.transactionDateTime.gt(startDate)
								.and(points.transactionDateTime.loe(expiryDate.plusDays(1)
									.toDateTimeAtStartOfDay().toDate()))
						))
					.list(points.transactionPoints.sum())
				));
	}
	
	@Override
	public List<PointsTxnModel> hasExistingExpirePoints(String accountId, LocalDate expiryDate) {
		QPointsTxnModel points = QPointsTxnModel.pointsTxnModel;
		JPAQuery query = new JPAQuery(em);
		
		return query.from(points).where(BooleanExpression.allOf(
				points.memberModel.accountId.eq(accountId),
				points.transactionType.eq(PointTxnType.EXPIRE),
				points.transactionDateTime.eq(expiryDate.toDate())
			)).list(points);
	}
	
	@Override
	public Date findDateOfLastExpire() {
		QPointsTxnModel points = QPointsTxnModel.pointsTxnModel;
		JPAQuery query = new JPAQuery(em);
		
		return query.from(points)
			.where(points.transactionType.eq(PointTxnType.EXPIRE))
			.groupBy(points.transactionDateTime)
			.singleResult(points.transactionDateTime.max());
	}
}
