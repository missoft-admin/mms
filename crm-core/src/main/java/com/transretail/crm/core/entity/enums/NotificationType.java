package com.transretail.crm.core.entity.enums;

public enum NotificationType {
	POINTS_ADJUSTMENT, PURCHASE_ADJUSTMENT, PROMOTION_CREATION, REWARD_GENERATION
}
