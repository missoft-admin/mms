package com.transretail.crm.giftcard;

/**
 *
 * @author Monte Cillo Co (mco@exist.com)
 */
public enum GiftCardErrorCode {

    GC_NOT_FOUND("2001"),
    GC_NOT_PRESENT_IN_MERCHANT("2002"),
    GC_TX_NOT_ALLOWED_IN_MERCHANT("2003"),
    GC_BURNED("2004"),
    GC_DEACTIVATED("2005"),
    GC_NOT_IN_STOCK_STATUS("2006"),
    GC_ACTIVATED("2007"),
    GC_EXPIRED("2008"),
    VOID_REF_TX_TYPE_NOT_MATCH("2009"),
    GC_NOT_ALLOWED_FOR_MULTIPLE_REDEMPTION("2010"),
    INSUFFICIENT_BALANCE("2011"),
    GC_NOT_YET_ACTIVATED("2012"),
    GC_NOT_ALLOWED_TO_RELOAD("2013"),
    UNAUTHORIZED_VALIDATION_KEY("2014"),
    VOID_REF_TX_NOT_FOUND("2015"),
    GC_RELOAD_INVALID_DENOMINATION("2016"),
    GC_RELOAD_MAX_AMOUNT_EXCEEDED("2017"), 
    GC_ACCOUNT_ID_NOT_FOUND("2018"),
    GC_TX_TX_NO_EXIST("2019"),
    GC_NOT_AVAILABLE("2020"),
    GC_INSUFFICIENT("2021"), // TODO: RENAME
    GC_PIN_INVALID("2022"),
    GC_VOID_NOT_ALLOWED("2023"),
    GC_EGC_ACTIVATION_NOT_ALLOWED("2024"),
    GC_TX_ALREADY_CLOSED("2025"),
    GC_NO_PRE_REDEEM_TX("2026"), 
    GC_INVALID_CARD_NOT_FREE("2027"),
    GC_INVALID_CARD_NO_FOR_SALE("2028"),
    EGC_MAX_REQUESTED_QUANTITY("2029"),
    GC_NOT_EGC("2030"),
    GC_INVALID_BU("2031");
    
    final String errorCode;

    private GiftCardErrorCode(String errorCode) {
        this.errorCode = errorCode;
    }

    public String getErrorCode() {
        return errorCode;
    }

}