$(document).ready( function() {
	$targetStoreGroups = $("form[name='promotion']").find("select[name='targetStoreGroups']");
	$modalTargetStoreGroups = $("#targetStoreGroupsDialog").find("select[name='targetStoreGroups']");
	$modalStoreGroups = $("select[name='storeGroups']");
	$storeGroupsModal = $("#targetStoreGroupsDialog");
	$storeGroupModalContainer = $("#storeGroupContainer");
	
	initDialogs();
	
	initButtons();
	
	$("#promotionForm").submit(function() {
		$("form[name='promotion']").find("select[name='targetStoreGroups'] > option").prop("selected", "selected");
	});
	
	refreshStoreGroups();
	
	function refreshStoreGroups() {
		$.get($storeGroupsModal.data("contextPath") + "/groups/store/list", function(data) {
			var inReturnEntity = data.result;
			$modalStoreGroups.empty();
			$.each(inReturnEntity, function(key, value) {
				var val = value.id;
				var label = value.name;
				if($modalTargetStoreGroups.find("option[value='"+val+"']").length == 0)
					$("<option>").attr("value", val).text(label).appendTo($modalStoreGroups);
			});	
		}, "json");
	}
	
	function initDialogs() {
		$storeGroupsModal.modal({
			show : false
		});
		
		$storeGroupsModal.on('show.bs.modal', openGroupDialogAction);
	}
	
	function initButtons() {
		$("#storeGroupSaveButton").click(function(e) {
			e.preventDefault();

			$( this ).prop( "disabled", true );
			
			$targetStoreGroups.empty();
			
			$modalTargetStoreGroups.find("option").clone().appendTo($targetStoreGroups);

			hideAndResetModal();
		});
		
		$("#storeGroupCancelButton").click(function(e) {
			e.preventDefault();
			hideAndResetModal();
		})
		
		$(".storeGroupLnk").click(function(e) {
			e.preventDefault();
			showGroupsDialog();
		});

		$( "#selectStoreGroups" ).dblclick( function(e) {
			$( "#addStoreGrp" ).trigger( "click" );
		});

		$( "#selectedStoreGroups" ).dblclick( function(e) {
			$( "#removeStoreGrp" ).trigger( "click" );
		});
		
		$("#addStoreGrp").click(function(e) {
			e.preventDefault();
			$modalStoreGroups.find("option:selected").each(function() {
				$modalTargetStoreGroups.append($(this));
			});
		});
		
		$("#removeStoreGrp").click(function(e) {
			e.preventDefault();
			$modalTargetStoreGroups.find("option:selected").each(function() {
				$modalStoreGroups.append($(this));
			});
		});
		
		$("#createStoreGrpLnk").click(function(e) {
			e.preventDefault();
			$.get($(this).attr("href"), function(data) {
				$storeGroupModalContainer.html(data);
			}, "html").done(function(data) {
				initCreateModal();
				
				$storeGroupsModal.modal("hide");
				$storeGroupModalContainer.find("#itemDialog").modal();
				
				$storeGroupModalContainer.find("#itemDialog").on('hide.bs.modal', function () {
					showGroupsDialog();
				});
			});
		});
	}
	
	function hideAndResetModal() {
		$modalTargetStoreGroups.empty();
		refreshStoreGroups();
		$( "#storeGroupSaveButton" ).prop( "disabled", false );
		$storeGroupsModal.modal("hide");
	}
	
	function showGroupsDialog() {
		
		$storeGroupsModal.modal("show");
	}
	
	function openGroupDialogAction() {
		$targetStoreGroups.find("option").each(function() {
			$modalStoreGroups.find("option[value='"+$(this).val()+"']").appendTo($modalTargetStoreGroups);
		});
		refreshStoreGroups();
	}
	
	
	function initCreateModal() {
		var ITEM_DIALOG = "#itemDialog";
		var GROUP_FORM_ID = "groupForm";
		var CONTAINER_ERROR = "#contentError > div";
		var CONTENT_ERROR = "#contentError";
		
		searchItem(true);
		initDialogButtons();
		
		function searchItem(ind) {
			var theParentForm = $("form[id='" + GROUP_FORM_ID + "']");
			var searchStr = $("input[name='searchStr']").val();
			$.ajax({
				type 	: "GET",
				url 	: $(theParentForm).attr("action") + "/searchitems",
				dataType: 'json',
				data 	: "searchStr=" + searchStr,
				        
				success : function(inResponse) {
					processSearchResults(inResponse);
					
					if(ind) {
						$(ITEM_DIALOG).modal("show");
					}
				},
				error : function(jqXHR, textStatus, errorThrown) {
					$( CONTENT_ERROR ).html("Error: " + errorThrown);
				}
			});	
		}
		
		function processSearchResults(inResponse) {
			var inReturnEntity = inResponse.result;
			$("select[name='selectItems']").empty();
			
			$.each(inReturnEntity, function(key, value) {
				var val = "";
				if(value.code != null && value.code != "")
					val = value.code;
				else if(value.id != null && value.id != "")
					val = value.id;

				var label = value.name;
				if($("select.items option[value='"+val+"']").length == 0)
					$("<option>").attr("value", val).text(label).appendTo($("select[name='selectItems']"));
			});	
			
		}
		
		function initDialogButtons() {
			var theParentForm = $("form[id='" + GROUP_FORM_ID + "']");
			$(ITEM_DIALOG).find("#button_save").unbind("click").click( function(e) {
				e.preventDefault();
				
				$("select.items option").prop("selected", "selected");
				
				$.ajax({
					type 	: "POST",
					url 	: $(theParentForm).attr("action"),
					dataType: 'json',
					data 	: $(theParentForm).serialize(),
					        
					success : function(inResponse) {
						processResults(inResponse, theParentForm);
					},
					error : function(jqXHR, textStatus, errorThrown) {
						$( CONTENT_ERROR ).html("Error: " + errorThrown);
					}
				});
			});
			
			$(ITEM_DIALOG).find("#button_cancel").unbind("click").click( function(e) {
				e.preventDefault();
		    	$(ITEM_DIALOG).modal("hide");
		    	$storeGroupModalContainer.empty();
		    });
			
			$(ITEM_DIALOG).find("#addBtn").unbind("click").click(function(e) {
				e.preventDefault();
				$(ITEM_DIALOG).find("select[name='selectItems'] option:selected").each(function() {
					$(ITEM_DIALOG).find("select.items").append($(this));
				});
			});
			
			$(ITEM_DIALOG).find("#removeBtn").unbind("click").click(function(e) {
				e.preventDefault();
				$(ITEM_DIALOG).find("select.items option:selected").each(function() {
					$(ITEM_DIALOG).find("select[name='selectItems']").append($(this));
				});
			});
			
			$(ITEM_DIALOG).find("#searchBtn").unbind("click").click(function(e) {
				e.preventDefault();
				searchItem(false);
			});
		}
		
		function processResults(inResponse, theParentForm) {
			if (inResponse.success) {
				$(ITEM_DIALOG).modal("hide");
				$storeGroupModalContainer.empty();
				refreshStoreGroups();
			} 
			else {
				errorInfo = "";
				for (i = 0; i < inResponse.result.length; i++) {
					errorInfo += "<br>" + (i + 1) + ". "
							+ ( inResponse.result[i].code != undefined ? 
									inResponse.result[i].code : inResponse.result[i]);
				}
				$(ITEM_DIALOG).find( CONTAINER_ERROR ).html("Please correct following errors: " + errorInfo);
				$(ITEM_DIALOG).find( CONTENT_ERROR ).show('slow');
			}
		}

	} 



	initViewGroup();
	function initViewGroup() {

		var $link_viewGroup = $( "#link_viewStoreGroup" );
		var $url_viewGroup = $link_viewGroup.data( "url-view" );
		$link_viewGroup.attr( "href", "#" );

		$storeGroupsModal.on( "show.bs.modal", function() {

			$link_viewGroup.click( function(e) {
				e.preventDefault();
				$.get( 
					$link_viewGroup.data( "url-view" ), 
					function( data ) {
						$( "#viewStoreGroup" ).html( data );
					}, 
					"html" );
			});

			$( "select[name='storeGroups']" ).change( function(e) {
				$link_viewGroup.show();
				$link_viewGroup.data( "url-view", $url_viewGroup + $(this).val() );
			});
		});
		$storeGroupsModal.on( "hide.bs.modal", function() {
			$( "#viewStoreGroup" ).empty();
		});
	}

});