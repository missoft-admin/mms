package com.transretail.crm.core.repo;


import java.util.List;

import org.springframework.stereotype.Repository;

import com.transretail.crm.common.repo.CrmQueryDslPredicateExecutor;
import com.transretail.crm.core.entity.Campaign;
import com.transretail.crm.core.entity.Program;
import com.transretail.crm.core.repo.custom.CampaignRepoCustom;


@Repository
public interface CampaignRepo extends CrmQueryDslPredicateExecutor<Campaign, Long>, CampaignRepoCustom {

	List<Campaign> findByProgram( Program inProgram );

}
