<%@ include file="../../common/taglibs.jsp" %>
	
	<spring:message var="typeName" code="label_com_transretail_crm_entity_customermodel"/>
  
  <style type="text/css">
  <!--
    #memberListTable .table tbody tr td:last-child {
        border-left: none;
        min-width: auto;
    }
  -->
  </style>
  
  <script>
  
  function reloadItemsTable(groupId, groupName) {
    $("#groupName").html(groupName);
    $("#list_items").empty().ajaxDataTable({
        'autoload'  : true,
        'ajaxSource' : "<c:url value="/groups/member/items/list/" />" + groupId,
        'columnHeaders' : [
        "<spring:message code="label_com_transretail_crm_entity_customermodel_accountnumber"/>",
        "<spring:message code="label_com_transretail_crm_entity_customermodel_username"/>",
        "<spring:message code="label_com_transretail_crm_entity_customermodel_firstname"/>",
        "<spring:message code="label_com_transretail_crm_entity_customermodel_lastname"/>"
        ],
        'modelFields' : [
           {name : 'accountId', sortable : true},
           {name : 'username', sortable : true},
           {name : 'firstName', sortable : true},
           {name : 'lastName', sortable : true}
         ]
    });  
  }
    
  </script>
<div id="memberListTable" class="modal  hide nofly">	
    <div class="modal-dialog">
    	<div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
              <h4><span id="groupName"></span></h4>
            </div>
    		<div class="modal-body">
              <div id="content_itemList">
                <div id="list_items"></div>
              </div>
        <%--
                <c:choose>
                <c:when test="${not empty memberList}">
                    <table class="table table-hover">
                        <thead>
                            <tr>
                                <th><spring:message code="label_com_transretail_crm_entity_customermodel_accountnumber"/></th>
                                <th><spring:message code="label_com_transretail_crm_entity_customermodel_username"/></th>
                                <th><spring:message code="label_com_transretail_crm_entity_customermodel_firstname"/></th>
                                <th><spring:message code="label_com_transretail_crm_entity_customermodel_lastname"/></th>
                            </tr>
                        </thead>
                        <tbody>
                        <c:forEach items="${memberList}" var="member">
                            <tr>
                                <td>${member.accountId}</td>
                                <td>${member.username}</td>
                                <td>${member.firstName}</td>
                                <td>${member.lastName}</td>
                            </tr>
                        </c:forEach>
                        </tbody>
                    </table>
                </c:when>
                <c:otherwise>
                    <spring:message arguments="${typeName}" code="entity_not_found"/>
                </c:otherwise>
                </c:choose> --%>
            </div>
            <div class="modal-footer">
                <!--<input type="button" class="btn btn-primary pull-right" id="button_ok" value="OK" />-->
                <button type="button" class="btn btn-primary" data-dismiss="modal" aria-hidden="true">OK</button>
            </div>
    	</div>
    </div>
</div>