create table CRM_POS_LOSE_TXN (
    id number(19,0) not null,
    CREATED_BY varchar2(255 char),
    CREATED_DATETIME timestamp,
    LAST_UPDATED_BY varchar2(255 char),
    LAST_UPDATED_DATETIME timestamp,
    CASHIER_NO varchar2(255 char),
    GC_NUMBER varchar2(255 char),
    NEW_BALANCE double precision,
    OLD_BALANCE double precision,
    POS_NO varchar2(255 char),
    STORE varchar2(255 char),
    TXN_AMOUNT double precision,
    TXN_DATETIME timestamp,
    TXN_NO varchar2(255 char),
    STATUS varchar2(15 char),
    primary key (id)
);

alter table CRM_POS_LOSE_TXN 
    add constraint FK_fhfh8405hl5hfc36gtmucires 
    foreign key (STATUS) 
    references CRM_REF_LOOKUP_DTL;