<%@ include file="../../common/taglibs.jsp" %>


<div id="content_adjustmentForm" class="modal hide  nofly modal-dialog">

  <c:if test="${not empty overrideType and overrideType == member }">
    <spring:message var="lblAccountId" code="points_prop_member_id"/>
  </c:if>
  <c:if test="${not empty overrideType and overrideType == employee }">
    <spring:message var="lblAccountId" code="points_prop_member_empid"/>
  </c:if>

  <div class="modal-content">
  <c:url var="url_adjustment" value="/points/override" />
  <c:url var="url_action_process" value="/points/override/process" />
  <c:url var="url_action" value="${action}" />
  <form:form id="adjustmentForm" name="adjustment" modelAttribute="pointsAdjustment" action="${url_action}" class="modal-form form-horizontal" data-url="${url_adjustment}" data-action="${url_action_process}">

    <div class="modal-header">
      <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
      <h4><!--<spring:message code="points_label_override"/>--><spring:message code="points_label_createadjustpointsentry_emp" /></h4>
    </div>

    <div class="modal-body">
      <div class="hide alert alert-error" id="content_error"><button type="button" class="close" onclick="$(this).parent().hide();">&times;</button><div><form:errors path="*"/></div></div>


      <form:hidden path="id" />

      <sec:authorize  ifAnyGranted="ROLE_HR, ROLE_CS">
      <div class="edit-block">
        <div class="control-group">
          <label for="accountId" class="control-label"><b class="required">*</b> <span id="label_accountId"><c:out value="${lblAccountId}" /></span></label>
          <div class="controls"><form:input id="accountId" path="memberModel.accountId" cssClass="input-medium" /></div>
        </div>

        <div class="pull-left">
          <div class="control-group">
            <label for="storeCode" class="control-label"><spring:message code="points_label_storeloc"/></label>
            <div class="controls">
              <form:select path="storeCode" id="storeCode" cssClass="input-medium">
                <option />
                <c:forEach var="store" items="${stores}">
                  <form:option value="${store.code}" label="${store.codeAndName}" />
                </c:forEach>
              </form:select>
            </div>
          </div>
          <div class="control-group">
            <label for="transactionNo" class="control-label"><b class="required">*</b> <spring:message code="points_prop_txnno" /></label>
            <div class="controls"><form:input id="transactionNo" path="transactionNo" cssClass="input-medium" /></div>
          </div>
          <div class="control-group">
            <label for="transactionPoints" class="control-label"><b class="required">*</b> <spring:message code="points_prop_txnpoints" /></label>
            <div class="controls"><form:input id="transactionPoints" path="transactionPoints" cssClass="input-medium signedInput" /></div>
          </div>
        </div>

        <div class="pull-left">
          <div class="control-group">
            <label for="transactionDateTime" class="control-label"><spring:message code="points_prop_txndate" /></label>
            <div class="controls"><form:input id="transactionDateTime" path="transactionDateTime" cssClass="input-medium" /></div>
          </div>
          <div class="control-group">
            <label for="transactionAmount" class="control-label"><b class="required">*</b> <spring:message code="points_prop_txnamount" /></label>
            <fmt:formatNumber type="number" pattern="#,##0" value="${pointsAdjustment.transactionAmount}" var="transactionAmount"/>
            <div class="controls"><input id="transactionAmount" name="transactionAmount" value="${transactionAmount}" cssClass="input-medium floatInput" type="text"/></div>
          </div>
          <div class="control-group">
            <label for="comment" class="control-label"><spring:message code="points_prop_comment" /></label>
            <div class="controls"><form:textarea id="comment" path="comment" /></div>
          </div>
        </div>
        <div class="clearfix"></div>

      </div>
      </sec:authorize>

    </div>

    <div class="modal-footer">
      <sec:authorize ifAnyGranted="ROLE_HR, ROLE_CS">
        <!-- <input id="saveButton" class="btn btn-primary" type="submit" value="<spring:message code="label_save" />" /> -->
        <button type="saveButton" id="saveButton" class="btn btn-primary"><spring:message code="label_save" /></button>

      </sec:authorize>

      <!--<input id="cancelButton" class="btn " type="button" value="<spring:message code="label_cancel" />" data-dismiss="modal" />-->
      <button type="button" id="formCancel" data-dismiss="modal" class="btn btn-default"><spring:message code="label_cancel"/></button>
      <div class="clearfix"></div>
    </div>

  </form:form>
  </div>


  <script src='<c:url value="/js/bootstrap/bootstrap-datepicker.js"/>' type='text/javascript'><![CDATA[&nbsp; ]]></script>
  <link href="<c:url value="/css/bootstrap/datepicker.css" />" rel="stylesheet" />
  <script src="<spring:url value="/js/jquery/jquery.numberInput.js" />" ></script>
  <script src='<c:url value="/js/viewspecific/points/override/adjustmentForm.js"/>' type='text/javascript'><![CDATA[&nbsp; ]]></script>


</div>