package com.transretail.crm.core.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.Past;

import com.transretail.crm.core.entity.support.CustomAuditableEntity;

@Entity
@Table(name = "CRM_CARD_HISTORY")
public class CardHistoryModel extends CustomAuditableEntity<Long> implements Serializable {
	@Past
    @Column(name="TRANSACTION_DATE")
	@Temporal(TemporalType.TIMESTAMP)
	private Date transacationDate;
	
	@Column(name="CARD_NO")
	private String cardNo;
	
	@Column(name="REASON", length = 1000)
	private String reason;

	public Date getTransacationDate() {
		return transacationDate;
	}

	public void setTransacationDate(Date transacationDate) {
		this.transacationDate = transacationDate;
	}

	public String getCardNo() {
		return cardNo;
	}

	public void setCardNo(String cardNo) {
		this.cardNo = cardNo;
	}

	public String getReason() {
		return reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}
}
