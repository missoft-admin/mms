package com.transretail.crm.template.service.excel;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

import java.io.InputStream;
import java.util.Locale;
import java.util.Scanner;

import javax.annotation.Resource;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.joda.time.LocalDate;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.TestingAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.context.SecurityContextImpl;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.support.TransactionCallback;
import org.springframework.transaction.support.TransactionTemplate;

import com.transretail.crm.core.entity.MemberModel;
import com.transretail.crm.core.repo.AddressRepo;
import com.transretail.crm.core.repo.LookupDetailRepo;
import com.transretail.crm.core.repo.LookupHeaderRepo;
import com.transretail.crm.core.repo.MemberRepo;
import com.transretail.crm.core.security.model.impl.CustomSecurityUserDetailsImpl;
import com.transretail.crm.core.service.CodePropertiesService;
import com.transretail.crm.template.service.MemberTemplateParseException;
import com.transretail.crm.template.service.MemberTemplateParser;
import com.transretail.crm.template.service.Operation;

/**
 * @author Allan G. Ramirez (agramirez@exist.com)
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath*:/META-INF/spring/applicationContext*.xml",
    "classpath:META-INF/spring/applicationContext-test.xml"})
public class ExcelMemberTemplateImplTest {
    @PersistenceContext
    private EntityManager em;
    @Autowired
    private PlatformTransactionManager transactionManager;
    @Autowired
    private LookupHeaderRepo lookupHeaderRepo;
    @Autowired
    private LookupDetailRepo lookupDetailRepo;
    @Autowired
    private MemberRepo memberRepo;
    @Autowired
    private AddressRepo addressRepo;
    @Resource(name = "streamingExcelMemberTemplateImpl")
//    @Resource(name = "excelMemberTemplate")
    private MemberTemplateParser excelMemberTemplate;
    @Autowired
    private CodePropertiesService codePropertiesService;

    @BeforeClass
    public static void setupCurrentUser() {
        CustomSecurityUserDetailsImpl user = new CustomSecurityUserDetailsImpl();
        user.setUserId(1L);
        user.setUsername("CLOUDX");
        SecurityContextImpl securityContext = new SecurityContextImpl();
        securityContext.setAuthentication(new TestingAuthenticationToken(user, ""));
        SecurityContextHolder.setContext(securityContext);
    }
    
    @AfterClass
    public static void removeCurrentUser() {
        SecurityContextHolder.clearContext();
    }

    @Before
    public void onSetup() {

        setUpLookupDetails();
    }

    @After
    public void tearDown() {
        memberRepo.deleteAll();
        addressRepo.deleteAll();
        lookupDetailRepo.deleteAll();
        lookupHeaderRepo.deleteAll();
    }

    @Test
    public void parseValidIndividualExcelFile() throws Exception {
        Locale locale = new Locale("en");
        InputStream is = Thread.currentThread().getContextClassLoader().getResourceAsStream("valid_individual_template.xlsx");
        try {
            excelMemberTemplate.parseMembers(is, codePropertiesService.getDetailMemberTypeIndividual(), locale, Operation.NEW_MEMBERS);
            assertEquals(2, memberRepo.count());
            assertEquals(2, addressRepo.count());
            MemberModel member = memberRepo.findByAccountId("1001000000001");
            assertEquals("04081984", member.getPassword());
        } finally {
            IOUtils.closeQuietly(is);
        }
    }

    @Test
    public void updateIndividualTest() throws Exception {
        Locale locale = new Locale("en");
        InputStream is = Thread.currentThread().getContextClassLoader().getResourceAsStream("valid_individual_template.xlsx");
        try {
            excelMemberTemplate.parseMembers(is, codePropertiesService.getDetailMemberTypeIndividual(), locale, Operation.NEW_MEMBERS);
            assertEquals(2, memberRepo.count());
            assertEquals(2, addressRepo.count());
            MemberModel member = memberRepo.findByAccountId("1001000000001");

            DateTimeFormatter formatter = DateTimeFormat.forPattern("dd-MM-yyyy");
            assertEquals(LocalDate.parse("04-08-1984", formatter).toDate(), member.getCustomerProfile().getBirthdate());
            assertEquals("Cloud Xavier 1", member.getFirstName());
            assertEquals("04081984", member.getPassword());

            is.close();
            is = Thread.currentThread().getContextClassLoader().getResourceAsStream("valid_individual_template_update.xlsx");
            excelMemberTemplate
                .parseMembers(is, codePropertiesService.getDetailMemberTypeIndividual(), locale, Operation.UPDATE_EXISTING_MEMBERS);
            assertEquals(2, memberRepo.count());
            assertEquals(2, addressRepo.count());
            member = memberRepo.findByAccountId("1001000000001");
            assertEquals(LocalDate.parse("13-03-2012", formatter).toDate(), member.getCustomerProfile().getBirthdate());
            assertEquals("Cloud Xavier", member.getFirstName());
            assertEquals("04081984", member.getPassword());
        } finally {
            IOUtils.closeQuietly(is);
        }
    }

    @Test
    public void parseInvalidEmailIndExcelFile() throws Exception {
        Locale locale = new Locale("en");
        InputStream is = Thread.currentThread().getContextClassLoader().getResourceAsStream("invalidemail_individual_template.xlsx");
        try {
            excelMemberTemplate.parseMembers(is, codePropertiesService.getDetailMemberTypeIndividual(), locale, Operation.NEW_MEMBERS);
        } catch (MemberTemplateParseException e) {
            assertEquals("Failed to parse sheet MEMBERINFORMATION. Email xcloudx2@cloudx is invalid.", e.getMessage());
        } finally {
            IOUtils.closeQuietly(is);
        }
    }

    @Test
    public void parseValidEmployeeExcelFile() throws Exception {
        Locale locale = new Locale("en");
        InputStream is =
            Thread.currentThread().getContextClassLoader().getResourceAsStream("valid_employee_template.xlsx");
        try {
            excelMemberTemplate.parseMembers(is, codePropertiesService.getDetailMemberTypeEmployee(), locale, Operation.NEW_MEMBERS);
            assertEquals(9, memberRepo.count());
            assertEquals(8, addressRepo.count());
            MemberModel member = memberRepo.findByAccountId("00012110075");
            assertEquals("08041984", member.getPassword());
        } finally {
            IOUtils.closeQuietly(is);
        }
    }

    @Test
    public void updateEmployeeTest() throws Exception {
        Locale locale = new Locale("en");
        InputStream is = null;
        try {
            is = Thread.currentThread().getContextClassLoader().getResourceAsStream("valid_employee_template.xlsx");
            excelMemberTemplate.parseMembers(is, codePropertiesService.getDetailMemberTypeEmployee(), locale, Operation.NEW_MEMBERS);
            assertEquals(9, memberRepo.count());
            assertEquals(8, addressRepo.count());
            MemberModel member = memberRepo.findByAccountId("00012110075");
            assertEquals("08041984", member.getPassword());
            assertNull(member.getLastName());

            is.close();
            is = Thread.currentThread().getContextClassLoader().getResourceAsStream("valid_employee_template_update.xlsx");
            excelMemberTemplate
                .parseMembers(is, codePropertiesService.getDetailMemberTypeEmployee(), locale, Operation.UPDATE_EXISTING_MEMBERS);
            assertEquals(9, memberRepo.count());
            assertEquals(8, addressRepo.count());
            member = memberRepo.findByAccountId("00012110075");
            assertEquals("Ramirez", member.getLastName());
        } finally {
            IOUtils.closeQuietly(is);
        }
    }

    @Test
    public void parseValidEmployeeExcelFileBug85587() throws Exception {
        Locale locale = new Locale("en");
        InputStream is =
            Thread.currentThread().getContextClassLoader().getResourceAsStream("valid_employee_template-bug85587.xlsx");
        try {
            excelMemberTemplate.parseMembers(is, codePropertiesService.getDetailMemberTypeEmployee(), locale, Operation.NEW_MEMBERS);
            assertEquals(1, memberRepo.count());
            assertEquals(1, addressRepo.count());
        } finally {
            IOUtils.closeQuietly(is);
        }
    }

    @Test
    public void parseValidEmployeeExcelFileWithEmptyValidationDate() throws Exception {
        Locale locale = new Locale("en");
        InputStream is =
            Thread.currentThread().getContextClassLoader()
                .getResourceAsStream("valid_employee_template_with_empty_validationdate.xlsx");
        try {
            excelMemberTemplate.parseMembers(is, codePropertiesService.getDetailMemberTypeEmployee(), locale, Operation.NEW_MEMBERS);
            assertEquals(1, memberRepo.count());
            assertEquals(1, addressRepo.count());
        } finally {
            IOUtils.closeQuietly(is);
        }
    }

    @Test
    public void parseValidProfessionalExcelFile() throws Exception {
        Locale locale = new Locale("en");
        InputStream is = Thread.currentThread().getContextClassLoader().getResourceAsStream("valid_professional_template.xlsx");
        try {
            excelMemberTemplate
                .parseMembers(is, codePropertiesService.getDetailMemberTypeProfessional(), locale, Operation.NEW_MEMBERS);
            assertEquals(3, memberRepo.count());
            assertEquals(2, addressRepo.count());
            MemberModel member = memberRepo.findByAccountId("1002000000001");
            assertEquals("21111986", member.getPassword());
        } finally {
            IOUtils.closeQuietly(is);
        }
    }

    @Test
    public void updateProfessional() throws Exception {
        Locale locale = new Locale("en");
        InputStream is = Thread.currentThread().getContextClassLoader().getResourceAsStream("valid_professional_template.xlsx");
        try {
            excelMemberTemplate
                .parseMembers(is, codePropertiesService.getDetailMemberTypeProfessional(), locale, Operation.NEW_MEMBERS);
            assertEquals(3, memberRepo.count());
            assertEquals(2, addressRepo.count());
            MemberModel member = memberRepo.findByAccountId("1002000000001");
            DateTimeFormatter formatter = DateTimeFormat.forPattern("dd-MM-yyyy");
            assertEquals(LocalDate.parse("21-11-1986", formatter).toDate(), member.getCustomerProfile().getBirthdate());
            assertEquals("21111986", member.getPassword());

            is.close();

            is = Thread.currentThread().getContextClassLoader().getResourceAsStream("valid_professional_template_update.xlsx");
            excelMemberTemplate
                .parseMembers(is, codePropertiesService.getDetailMemberTypeProfessional(), locale, Operation.UPDATE_EXISTING_MEMBERS);
            assertEquals(3, memberRepo.count());
            assertEquals(2, addressRepo.count());
            member = memberRepo.findByAccountId("1002000000001");

            assertEquals(LocalDate.parse("13-03-2013", formatter).toDate(), member.getCustomerProfile().getBirthdate());
            assertEquals("21111986", member.getPassword());
            assertEquals("Cloud Xavier", member.getFirstName());
        } finally {
            IOUtils.closeQuietly(is);
        }
    }

    @Test
    public void parseValidProfessionalExcelFileWithManyTrailingEmptyRows() throws Exception {
        Locale locale = new Locale("en");
        InputStream is = Thread.currentThread().getContextClassLoader()
            .getResourceAsStream("valid_professional_template_with_many_trailing_rows.xlsx");
        try {
            excelMemberTemplate
                .parseMembers(is, codePropertiesService.getDetailMemberTypeProfessional(), locale, Operation.NEW_MEMBERS);
            assertEquals(1, memberRepo.count());
            assertEquals(1, addressRepo.count());
        } finally {
            IOUtils.closeQuietly(is);
        }
    }

    @Transactional
    private void setUpLookupDetails() {
        InputStream is1 = Thread.currentThread().getContextClassLoader().getResourceAsStream("A_LOOKUPHEADERS.sql");
        InputStream is2 = Thread.currentThread().getContextClassLoader().getResourceAsStream("B_LOOKUPDETAILS.sql");
        try {
            final Scanner in1 = new Scanner(is1);
            final Scanner in2 = new Scanner(is2);
            TransactionTemplate transactionTemplate = new TransactionTemplate(transactionManager);
            transactionTemplate.execute(new TransactionCallback() {
                public Object doInTransaction(TransactionStatus status) {
                    while (in1.hasNext()) {
                        String script = in1.nextLine();
                        if (StringUtils.isNotBlank(script)) {
                            em.createNativeQuery(script).executeUpdate();
                        }
                    }
                    while (in2.hasNext()) {
                        String script = in2.nextLine();
                        if (StringUtils.isNotBlank(script)) {
                            em.createNativeQuery(script).executeUpdate();
                        }
                    }
                    em.clear();
                    em.flush();
                    return null;
                }
            });
        } finally {
            IOUtils.closeQuietly(is1);
            IOUtils.closeQuietly(is2);
        }
    }
}
