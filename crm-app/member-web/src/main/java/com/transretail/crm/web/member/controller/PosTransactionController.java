package com.transretail.crm.web.member.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.transretail.crm.core.dto.PosTxItemResultList;
import com.transretail.crm.core.dto.PosTxItemSearchDto;
import com.transretail.crm.core.service.PosTransactionService;

/**
 * @author Allan G. Ramirez (agramirez@exist.com)
 */
@Controller
@RequestMapping("/postransaction")
public class PosTransactionController {
    @Autowired
    private PosTransactionService posTransactionService;

    @ResponseBody
    @RequestMapping(value = "/item/list", method = RequestMethod.POST)
    public PosTxItemResultList getPoints(@RequestBody PosTxItemSearchDto dto) {
        return posTransactionService.getPosTxItems(dto);
    }
}
