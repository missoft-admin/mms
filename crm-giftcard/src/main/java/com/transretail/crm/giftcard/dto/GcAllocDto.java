package com.transretail.crm.giftcard.dto;


import com.google.common.base.Objects;

public class GcAllocDto {

	private Long seriesStart;
	private Long seriesEnd;
	private String barcode;
	private String barcodeStart;
	private String barcodeEnd;
	private Long quantity;
	private Long productId;
	private Long soItemId;

	public GcAllocDto(){}
	public GcAllocDto( Long seriesStart, Long seriesEnd ) {
		this.seriesStart = seriesStart;
		this.seriesEnd = seriesEnd;
	}


	public Long getSeriesStart() {
		return seriesStart;
	}
	public void setSeriesStart(Long seriesStart) {
		this.seriesStart = seriesStart;
	}
	public Long getSeriesEnd() {
		return seriesEnd;
	}
	public void setSeriesEnd(Long seriesEnd) {
		this.seriesEnd = seriesEnd;
	}
	public String getBarcode() {
		return barcode;
	}
	public void setBarcode(String barcode) {
		this.barcode = barcode;
	}
	public String getBarcodeStart() {
		return barcodeStart;
	}
	public void setBarcodeStart(String barcodeStart) {
		this.barcodeStart = barcodeStart;
	}
	public String getBarcodeEnd() {
		return barcodeEnd;
	}
	public void setBarcodeEnd(String barcodeEnd) {
		this.barcodeEnd = barcodeEnd;
	}
	public Long getQuantity() {
		return quantity;
	}
	public void setQuantity(Long quantity) {
		this.quantity = quantity;
	}
	public Long getProductId() {
		return productId;
	}
	public void setProductId(Long productId) {
		this.productId = productId;
	}
	public Long getSoItemId() {
		return soItemId;
	}
	public void setSoItemId(Long soItemId) {
		this.soItemId = soItemId;
	}

	@Override
	public String toString() {
		return Objects.toStringHelper(this)
				.add("seriesStart", seriesStart)
				.add("seriesEnd", seriesEnd)
				.add("barcode", barcode)
				.add("barcodeStart", barcodeStart)
				.add("barcodeEnd", barcodeEnd)
				.add("quantity", quantity)
				.add("productId", productId)
				.add("soItemId", soItemId)
				.toString();
	}
}
