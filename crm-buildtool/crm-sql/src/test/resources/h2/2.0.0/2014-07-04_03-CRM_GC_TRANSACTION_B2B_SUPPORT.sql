
alter table crm_gc_transaction
  modify column CASHIER_ID varchar(12) null;
alter table crm_gc_transaction
  modify column TERMINAL_ID varchar(12) null;
alter table crm_gc_transaction
  modify column MERCHANT_ID varchar(255) null;