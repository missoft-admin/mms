package com.transretail.crm.web.controller;

import static org.hamcrest.Matchers.is;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.AnnotationConfigWebContextLoader;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

import com.transretail.crm.common.reporting.service.ReportService;
import com.transretail.crm.core.service.CodePropertiesService;
import com.transretail.crm.giftcard.repo.GiftCardExpiryExtensionRepo;
import com.transretail.crm.giftcard.repo.SalesOrderRepo;
import com.transretail.crm.giftcard.service.GiftCardExpiryExtensionService;
import com.transretail.crm.giftcard.service.GiftCardInventoryService;
import com.transretail.crm.giftcard.service.ProductProfileService;
import com.transretail.crm.report.template.impl.GiftCardDateExpiryDocument;

/**
 * @author ftopico
 */
@RunWith(SpringJUnit4ClassRunner.class)
@WebAppConfiguration
@ContextConfiguration(loader = AnnotationConfigWebContextLoader.class)
public class GiftCardExpiryControllerTest extends AbstractMockMvcTest {

    @Configuration
    @EnableWebMvc
    static class Config {
        @Bean
        public GiftCardExpiryController giftCardExpiryController() {
            return new GiftCardExpiryController();
        }
        @Bean
        public GiftCardExpiryExtensionService giftCardExpiryExtensionService() {
            return Mockito.mock(GiftCardExpiryExtensionService.class);
        }
        @Bean
        public CodePropertiesService codePropertiesService() {
            return Mockito.mock(CodePropertiesService.class);
        }
        @Bean
        public GiftCardDateExpiryDocument giftCardDateExpiryDocument() {
            return Mockito.mock(GiftCardDateExpiryDocument.class);
        }
        @Bean
        public MessageSource messageSource() {
            return Mockito.mock(MessageSource.class);
        }
        @Bean
        public GiftCardExpiryExtensionRepo giftCardExpiryExtensionRepo() {
            return Mockito.mock(GiftCardExpiryExtensionRepo.class);
        }
        @Bean
        public SalesOrderRepo salesOrderRepo() {
            return Mockito.mock(SalesOrderRepo.class);
        }
        @Bean
        public ProductProfileService productProfileService() {
            return Mockito.mock(ProductProfileService.class);
        }
        @Bean
        public GiftCardInventoryService giftCardInventoryService() {
            return Mockito.mock(GiftCardInventoryService.class);
        }
        @Bean
        public ReportService reportService() {
            return Mockito.mock(ReportService.class);
        }
        
    }
    
    @Autowired
    private GiftCardExpiryExtensionService giftCardExpiryExtensionService;
    @Autowired
    private CodePropertiesService codePropertiesService;
    @Autowired
    private GiftCardDateExpiryDocument giftCardDateExpiryDocument;
    @Autowired
    private MessageSource messageSource;
    @Override
    
    protected void doSetup() throws Exception {
        Mockito.reset(giftCardExpiryExtensionService);
        Mockito.reset(codePropertiesService);
        Mockito.reset(giftCardDateExpiryDocument);
        Mockito.reset(messageSource);
    }
    
    @Test
    public void testSave() throws Exception {
        mockMvc.perform(post("/giftcard/dateexpiry/save"))
            .andExpect(status().isOk())
            .andExpect(jsonPath("$.success", is(true)));
    }
    
    @Test
    public void testApprove() throws Exception {
        String extendNo = new String("00001");
        
        mockMvc.perform(post("/giftcard/dateexpiry/approve/{extendNo}", extendNo))
        .andExpect(status().isOk())
        .andExpect(jsonPath("$.success", is(true)));
    }
    
    @Test
    public void testReject() throws Exception {
        String extendNo = new String("00001");
        
        mockMvc.perform(post("/giftcard/dateexpiry/reject/{extendNo}", extendNo))
        .andExpect(status().isOk())
        .andExpect(jsonPath("$.success", is(true)));
    }
    
}
