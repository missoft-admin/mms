package com.transretail.crm.core.repo;

import com.transretail.crm.common.repo.CrmQueryDslPredicateExecutor;
import com.transretail.crm.core.entity.StoreMember;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface StoreMemberRepo extends CrmQueryDslPredicateExecutor<StoreMember, String> {


    List<StoreMember> findByStore(String store);

}
