<%@include file="/WEB-INF/views/common/taglibs.jsp" %>
<div id="sales-report" class="form-horizontal">
  <div class="page-header page-header2">
    <h1><spring:message code="gc.acct.sales.report.page.title" /></h1>
  </div>
  <div class="errorMessages error">&nbsp;</div>

  <c:choose>
    <c:when test="${fn:length(reportTemplates) > 1}">
      <div class="control-group">
        <label class="control-label"><spring:message code="member.card.activation.report.form.selecttemplate.label"/></label>

        <div class="controls">
          <select id="templateName">
            <c:forEach items="${reportTemplates}" var="reportTemplate">
              <option value="${reportTemplate.id}">${reportTemplate.name}</option>
            </c:forEach>
          </select>
        </div>
      </div>
    </c:when>
    <c:otherwise>
      <div class="">
	<select id="templateName" style="display: none;">
	  <c:forEach items="${reportTemplates}" var="reportTemplate">
	    <option value="${reportTemplate.id}">${reportTemplate.name}</option>
	  </c:forEach>
	</select>
      </div>
    </c:otherwise>
  </c:choose>

  <div class="control-group">
    <label class="control-label"></label>
    <div class="controls">
      <select name="reportType" id="reportType">
        <c:forEach items="${reportTypes}" var="reportType">
          <option value="${reportType.code}"><spring:message code="report_type_${reportType.code}" /></option>
        </c:forEach>
      </select>
    </div>
  </div>

  <fieldset>
    <legend><spring:message code="report.filters.label"/></legend>
    <form id="filters">
      <div class="control-group row-fluid">
	<label class="control-label"><spring:message code="gc.acct.sales.filter.report.type" /></label>
	<div class="controls-row" style="margin-top: 4px; color: #555555;">

	  <c:forEach items="${salesReportCategory}" var="type">
	    <!--<option value="${type}"><spring:message code="sales.report.category.${type}" /></option>-->
	    <input type="radio" id="salesReportType" name="salesReportType" value="${type}"/><spring:message code="sales.report.category.${type}" />
	  </c:forEach>
	</div>
      </div>
      <div id="filters-container" > </div>
    </form>
    <div id="view-button-container" class="control-group form-actions hide">
      <button id="view" type="button" class="btn btn-primary" data-url="${exportUrl}"><spring:message code="view"/></button>
    </div>
  </fieldset>


</div>
<style>
  #templateName {
    width: 30%;
  }
  input[type="radio"] {
    margin-left: 20px;
    margin-right: 4px;
    margin-top: 0;
    
  }
  
</style>
<script src="<c:url value="/js/bootstrap/bootstrap-datepicker.js"/>"></script>
<link href="<c:url value="/css/bootstrap/datepicker.css" />" rel="stylesheet"/>

<script>
  $(document).ready(function () {
    var templateFilterUrl = "<spring:url value="/report/template/reportTemplateName/filters"/>"
    var templateViewUrl = "<spring:url value="/report/template/reportType/reportTemplateName/view"/>"

    var CONTAINER_ERROR = "#contentError > div";
    var CONTENT_ERROR = "#contentError";
    var $errorMessagesContainer = $('div.errorMessages');
    var $filters = $('#filters-container');

    $("input[name=salesReportType]:radio").change(function () {
      $('#view-button-container').show();

      var value = $("input[name=salesReportType]:checked").val();

      $.get(ctx + "/giftcard/acct/sales/report/filters/" + value, function (filterFields) {
	var hasDate = false;
	var hasMultipleSelect = false;
	var dateClass = [];
	var selects = [];
	$filters.children().remove();

	var htmlString = '';
	for (var i = 0; i < filterFields.length; i++) {
	  htmlString += '<div class="control-group">';
	  htmlString += '<label class="control-label">'
		  + (filterFields[i].required ? '<span class="required">*</span> ' : '')
		  + filterFields[i].label + '</label>';
	  htmlString += '<div class="controls">';

	  if (filterFields[i].type === 'INPUT') {
	    htmlString += '<input name="' + filterFields[i].name + '" type="text" placeHolder="' + filterFields[i].placeHolder + '" />';
	  } else if (filterFields[i].type === 'DROPDOWN') {
	    htmlString += '<select name="' + filterFields[i].name + '" placeHolder="' + filterFields[i].placeHolder + '">';
	    var selectValues = filterFields[i].selectValues ? filterFields[i].selectValues : filterFields[i].dropdownValues;
	    if (filterFields[i].selectValues) {
	      for (var key in selectValues) {
		htmlString += '<option value="' + key + '">' + selectValues[key] + '</option>';
	      }
	    }
	    else if (filterFields[i].dropdownValues) {
	      for (var j = 0; j < selectValues.length; j++) {
		htmlString += '<option value="' + selectValues[j].key + '">' + selectValues[j].value + '</option>';
	      }
	    }
	    htmlString += '</select>';
	  } else if (filterFields[i].type === 'TOGGLE') {
	    htmlString += '<input name="' + filterFields[i].name + '" type="checkbox" placeHolder="' + filterFields[i].placeHolder + '" />';
	  } else if (filterFields[i].type === 'DATE') {
	    htmlString += addDate("", filterFields[i], dateClass);
	    hasDate = true;
	  } else if (filterFields[i].type === 'DATE_FROM') {
	    htmlString += addDate("startDate", filterFields[i], dateClass);
	    hasDate = true;
	  } else if (filterFields[i].type === 'DATE_TO') {
	    htmlString += addDate("endDate", filterFields[i], dateClass);
	    hasDate = true;
	  } else if (filterFields[i].type === 'SELECT_MULTIPLE') {
	    htmlString += '<select id="values' + filterFields[i].name + '" multiple="multiple" class="pull-left"> ';
	    var selectValues = filterFields[i].selectValues;
	    for (var key in selectValues) {
	      htmlString += '<option value="' + key + '">' + selectValues[key] + '</option>';
	    }
	    htmlString += '</select>';
	    htmlString += '<div class="cont-02"> <input id="sendButton' + filterFields[i].name + '" data-name="' + filterFields[i].name + '" type="button" class="btn" value=">>"/>'
	    htmlString += '<input id="returnButton' + filterFields[i].name + '" data-name="' + filterFields[i].name + '" type="button" class="btn" value="<<"/> </div>'
	    htmlString += '<select id="selected' + filterFields[i].name + '" placeHolder="' + filterFields[i].placeHolder + '" multiple="multiple" class="pull-left" />';
	    hasMultipleSelect = true;

	    selects.push(filterFields[i].name);
	  }

	  htmlString += '</div>';
	  htmlString += '</div>';
	}
	$filters.append(htmlString);
//$('#sales-report .input-daterange').datepicker({
//	  format: DEFAULT_DATE_FORMAT,
//	  startDate: '-3m',
//	  endDate: new Date(),
//	  todayBtn: "linked",
//	  autoclose: true
//	});
	if (hasDate) {
	  for (var i = 0; i < dateClass.length; i++) {
	    if (dateClass[i] === 'YEAR_MONTH') {
	      $('.date' + dateClass[i], $filters).datepicker({
		autoclose: true,
		format: 'MM-yyyy',
		startView: 2,
		minViewMode: 1
	      });
	    } else if (dateClass[i] === 'YEAR') {
	      $('.date' + dateClass[i], $filters).datepicker({
		autoclose: true,
		format: 'yyyy',
		startView: 2,
		minViewMode: 2
	      });
	    } else if (dateClass[i] === 'PLAIN') {
	      $('.date' + dateClass[i], $filters).datepicker({
		format: DEFAULT_DATE_FORMAT,
		startDate: '-12m',
		endDate: new Date(),
		todayBtn: "linked",
		autoclose: true
	      });
	    }
	  }

	  $(".startDate").change(function () {
	    $(".endDate").datepicker("setStartDate", $(this).datepicker("getDate"));
	  });
	}

	$("select").each(function () { //move empty option on first row and set as default
	  $('option[value=""]', this)
		  .prop("selected", "selected")
		  .prependTo(this);
	});


	if (hasMultipleSelect) {
	  for (var i = 0; i < selects.length; i++) {
	    $('#sendButton' + selects[i]).click(function () {
	      var values = $("select#values" + $(this).data('name') + "").val();
	      var texts = $("select#values" + $(this).data('name') + " option:selected").map(function () {
		return $(this).text();
	      }).get();

	      var result = $("select#selected" + $(this).data('name'));
	      var resultSelected = result.find("option").map(function () {
		return this.value;
	      }).get();
	      var toSelect = "";
	      for (var j = 0; j < values.length; j++) {
		if (resultSelected.indexOf(values[j]) == -1) {
		  toSelect += '<option value="' + values[j] + '" data-name="' + $(this).data('name') + '" class="multipleSelectVal">' + texts[j] + '</option>';
		}
	      }
	      //result.empty();
	      result.append(toSelect);
	    });

	    $('#returnButton' + selects[i]).click(function () {
	      $("select#selected" + $(this).data('name') + " option:selected").remove();
	    });

	    var selectname = selects[i];
	    $("select#values" + selectname).dblclick(function (e) {
	      $.each($(this).find("option:selected"), function () {
		//$( "select#selected" + selectname ).append( $( this ).clone() ); 
		$(this).data("index", $(this).index());
		$("select#selected" + selectname).append($(this));
	      });
	    });
	    $("select#selected" + selectname).dblclick(function (e) {
	      $.each($(this).find("option:selected"), function () {
		//$( this ).remove(); 
		$("select#values" + selectname + " option").eq($(this).data("index") - 0)
			.before($("<option></option>").val($(this).val()).html($(this).text()).data("index", $(this).index()));
		$(this).remove();
	      });
	    });
	  }
	}
      });
    });

    var showErrorMessage = function (message) {
      $errorMessagesContainer.html('<div class="alert alert-error">' +
	      '<button type="button" class="close" data-dismiss="alert">&times;</button>' +
	      message + '</div>');
    };

    $('#view').click(function () {
      var parameters = $('#filters').serialize();
      var stores = "";
      $(".multipleSelectVal").each(function (key, element) {
	console.log(key + " " + $(element).val());
	stores += $(element).val() + "!";
      });
      if (stores) {
	parameters += "&stores=" + stores;
      }
      console.log("parameters: " + parameters);

      $.get("<c:url value="/giftcard/acct/sales/report/export/validate" />", parameters, function (errorMessages) {
	if (errorMessages.length > 0) {
	  var errorInfo = "1. " + errorMessages[0];
	  for (var i = 1; i < errorMessages.length; i++) {
	    errorInfo += "<br>" + (i + 1) + ". " + errorMessages[i];
	  }
	  showErrorMessage(errorInfo);
	} else {
	  $(".alert", $errorMessagesContainer).alert('close');
	  var selectedReportType = $("select[name='reportType']").val();
	  var url = "<c:url value="/giftcard/acct/sales/report/export/" />" + selectedReportType.toUpperCase() + "?" + parameters;
	  console.log(url);
	  if (selectedReportType === "pdf") {
	    window.open(url, '_blank', 'toolbar=0,location=0,menubar=0');
	  }
	  else if (selectedReportType === "excel") {
	    window.location.href = url;
	  }
	}
	/* if (data.success) {
	 var selectedTemplateName = $( 'option:selected', $templateNameSelect ).val();
	 var selectedReportType = $("select[name='reportType']").val();
	 var url = templateViewUrl.replace( 'reportTemplateName', selectedTemplateName ).replace('reportType', selectedReportType);
	 var params = $filters.serialize();
	 if ( params != null && '' != params.trim() ) {
	 url += "?" + params;
	 }
	 console.log( url );
	 if(selectedReportType == "pdf")
	 window.open( url, '_blank', 'toolbar=0,location=0,menubar=0' );
	 else if(selectedReportType == "excel")
	 window.location.href = url;
	 } 
	 else {
	 errorInfo = "";
	 for (i = 0; i < data.result.length; i++) {
	 errorInfo += "<br>" + (i + 1) + ". "
	 + ( data.result[i].code != undefined ? 
	 data.result[i].code : data.result[i]);
	 }
	 $( CONTAINER_ERROR ).html("Please correct following errors: " + errorInfo);
	 $( CONTENT_ERROR ).show('slow');
	 } */
      }, "json");
    });
  });

  function addDate(className, filterFields, dateClass) {
    var htmlString = '';
    // for backward compatibilty
    if (!filterFields.dateOption) {
      htmlString += '<input class="' + className + ' date" name="' + filterFields.name + '" type="text" placeHolder="' + filterFields.placeHolder + '" />';
    } else {
      dateClass.push(filterFields.dateOption);
      htmlString += '<input class="' + className + ' date' + filterFields.dateOption + '"name="' + filterFields.name + '" type="text" data-format="yyyy-MM-dd" placeHolder="' + filterFields.placeHolder + '" />';
    }
    return htmlString;
  }
</script>