package com.transretail.crm.core.repo;

import com.transretail.crm.common.repo.CrmQueryDslPredicateExecutor;
import com.transretail.crm.core.entity.CrmForPeoplesoftConfig;
import com.transretail.crm.core.entity.CrmForPeoplesoftConfig.TransactionType;
import org.springframework.stereotype.Repository;

@Repository
public interface CrmForPeoplesoftConfigRepo extends CrmQueryDslPredicateExecutor<CrmForPeoplesoftConfig, Long> {

    CrmForPeoplesoftConfig findByTransactionType(TransactionType transactionType);
}
