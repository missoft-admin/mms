package com.transretail.crm.core.service;


import com.transretail.crm.common.reporting.jasper.JRProcessor;
import com.transretail.crm.common.service.dto.response.ResultList;
import com.transretail.crm.core.dto.*;
import com.transretail.crm.core.entity.PointsTxnModel;

import java.io.InputStream;
import java.util.Date;
import java.util.List;


public interface PointsRewardService {

	ServiceDto getPointsRewardForProcessing();

	ServiceDto getPointsRewardForProcessing(Date inFromDate, Date inToDate);

	ServiceDto getPointsRewardForApproval();

	ServiceDto getPointsRewardForApproval(Date inFromDate, Date inToDate);

	ServiceDto rejectPointsRewards(List<String> inRewardIds, RewardBatchDto inReward);

	ServiceDto approvePointsRewards(List<String> inRewardIds);

	ServiceDto deletePointsReward(String inRewardId);

	//JRProcessor createRewardsJrProcessor(InputStream logo);

	ResultList<PointsDto> listRewardsForProcessing(PointsSearchDto dto);



	ServiceDto generatePointsRewardForApproval();



	List<PointsDto> generateReconPoints(Date postedAfter, Date txnDateTo, String defaultCron);

	ResultList<PointsRewardDto> searchRecon(PointsRewardSearchDto dto);

	PointsTxnModel save(PointsTxnModel model);

	JRProcessor createRewardsJrProcessor(InputStream logo, Date dateFrom, Date dateTo);
	
	ServiceDto resetPointsRewards( List<String> inRewardIds );

    ServiceDto approveAllPointsRewards();
}