package com.transretail.crm.loyalty.dto;

import org.joda.time.LocalDate;
import org.springframework.format.annotation.DateTimeFormat;

import com.mysema.query.annotations.QueryProjection;
import com.transretail.crm.core.entity.lookup.LookupDetail;

public class AllocationDto {
	private String batchRefNo;
	private LookupDetail cardType;
	private String startingSeries;
	private String endingSeries;
	private String location;
	private String locationName;
	private String transferTo;
	private String transferToName;
	private String newTransferTo;
	private String newTransferToName;
	private String referenceNo;
	private String encodedBy;
	@DateTimeFormat(pattern = "dd-MM-yyyy")
	private LocalDate allocateDate;
	
	public AllocationDto() {}
	
	public AllocationDto(LookupDetail cardType, String startingSeries, String endingSeries,
			String referenceNo) {
		super();
		this.cardType = cardType;
		this.startingSeries = startingSeries;
		this.endingSeries = endingSeries;
		this.referenceNo = referenceNo;
	}

	@QueryProjection
	public AllocationDto(String startingSeries, String endingSeries,
			String location, String transferTo, String referenceNo,
			String encodedBy, LocalDate allocateDate) {
		super();
		this.startingSeries = startingSeries;
		this.endingSeries = endingSeries;
		this.location = location;
		this.transferTo = transferTo;
		this.referenceNo = referenceNo;
		this.encodedBy = encodedBy;
		this.allocateDate = allocateDate;
	}
	
	@QueryProjection
	public AllocationDto(String startingSeries, String endingSeries,
			String location, String transferTo, String referenceNo,
			String encodedBy, LocalDate allocateDate, String batchRefNo) {
		super();
		this.startingSeries = startingSeries;
		this.endingSeries = endingSeries;
		this.location = location;
		this.transferTo = transferTo;
		this.referenceNo = referenceNo;
		this.encodedBy = encodedBy;
		this.allocateDate = allocateDate;
		this.batchRefNo = batchRefNo;
	}

	public String getBatchRefNo() {
		return batchRefNo;
	}

	public void setBatchRefNo(String batchRefNo) {
		this.batchRefNo = batchRefNo;
	}

	public String getStartingSeries() {
		return startingSeries;
	}

	public void setStartingSeries(String startingSeries) {
		this.startingSeries = startingSeries;
	}

	public String getEndingSeries() {
		return endingSeries;
	}

	public void setEndingSeries(String endingSeries) {
		this.endingSeries = endingSeries;
	}

	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		this.location = location;
	}

	public String getTransferTo() {
		return transferTo;
	}

	public void setTransferTo(String transferTo) {
		this.transferTo = transferTo;
	}

	public String getReferenceNo() {
		return referenceNo;
	}

	public void setReferenceNo(String referenceNo) {
		this.referenceNo = referenceNo;
	}

	public String getEncodedBy() {
		return encodedBy;
	}

	public void setEncodedBy(String encodedBy) {
		this.encodedBy = encodedBy;
	}

	public LocalDate getAllocateDate() {
		return allocateDate;
	}

	public void setAllocateDate(LocalDate allocateDate) {
		this.allocateDate = allocateDate;
	}

	public LookupDetail getCardType() {
		return cardType;
	}

	public void setCardType(LookupDetail cardType) {
		this.cardType = cardType;
	}

	public Long getQuantity() {
		return Long.valueOf(endingSeries) - Long.valueOf(startingSeries) + 1;
	}

	public String getLocationName() {
		return locationName;
	}

	public void setLocationName(String locationName) {
		this.locationName = locationName;
	}

	public String getTransferToName() {
		return transferToName;
	}

	public void setTransferToName(String transferToName) {
		this.transferToName = transferToName;
	}

	public String getNewTransferTo() {
		return newTransferTo;
	}

	public void setNewTransferTo(String newTransferTo) {
		this.newTransferTo = newTransferTo;
	}

	public String getNewTransferToName() {
		return newTransferToName;
	}

	public void setNewTransferToName(String newTransferToName) {
		this.newTransferToName = newTransferToName;
	}
	
	
	

}
