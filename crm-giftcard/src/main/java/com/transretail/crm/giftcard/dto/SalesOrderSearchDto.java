package com.transretail.crm.giftcard.dto;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.apache.commons.lang3.StringUtils;
import org.joda.time.LocalDate;

import com.mysema.query.types.expr.BooleanExpression;
import com.transretail.crm.common.service.dto.request.AbstractSearchFormDto;
import com.transretail.crm.common.service.dto.request.PagingParam;
import com.transretail.crm.giftcard.entity.QSalesOrder;
import com.transretail.crm.giftcard.entity.support.SalesOrderStatus;
import com.transretail.crm.giftcard.entity.support.SalesOrderType;

/**
 *
 */
public class SalesOrderSearchDto extends AbstractSearchFormDto {
	private String orderNo;
	private String company;
	private String contactPerson;
	private LocalDate orderDateFrom;
	private LocalDate orderDateTo;
	private String status;
	private SalesOrderType orderType;
	private String paymentStore;

	public String getOrderNo() {
		return orderNo;
	}

	public void setOrderNo(String orderNo) {
		this.orderNo = orderNo;
	}

	public String getCompany() {
		return company;
	}

	public void setCompany(String company) {
		this.company = company;
	}

	public String getContactPerson() {
		return contactPerson;
	}

	public void setContactPerson(String contactPerson) {
		this.contactPerson = contactPerson;
	}

	public SalesOrderType getOrderType() {
		return orderType;
	}

	public void setOrderType(SalesOrderType orderType) {
		this.orderType = orderType;
	}

	@Override
	public BooleanExpression createSearchExpression() {
		QSalesOrder qOrder = QSalesOrder.salesOrder;
		List<BooleanExpression> expressions = new ArrayList<BooleanExpression>();
		if(StringUtils.isNotBlank(orderNo)) {
			expressions.add(qOrder.orderNo.like("%"+orderNo+"%"));
		}
		if (StringUtils.isNotBlank(company)) {
			expressions.add(qOrder.customer.name.containsIgnoreCase(company));
		}
		if (StringUtils.isNotBlank(contactPerson)) {
			expressions.add(qOrder.customer.contactFirstName.containsIgnoreCase(contactPerson).or(qOrder.customer.contactLastName.containsIgnoreCase(contactPerson)));
		}
		if (orderDateFrom != null) {
			expressions.add(qOrder.orderDate.goe(orderDateFrom));
		}
		if (orderDateTo != null) {
			expressions.add(qOrder.orderDate.loe(orderDateTo));
		}
		if (StringUtils.isNotBlank(status)) {
			expressions.add(qOrder.status.eq(SalesOrderStatus.valueOf(status)));
		}
		if (orderType != null) {
			expressions.add(qOrder.orderType.eq(orderType));
		}
		return BooleanExpression.allOf(expressions.toArray(new BooleanExpression[expressions.size()]));
	}

	public LocalDate getOrderDateFrom() {
		return orderDateFrom;
	}

	public void setOrderDateFrom(LocalDate orderDateFrom) {
		this.orderDateFrom = orderDateFrom;
	}

	public LocalDate getOrderDateTo() {
		return orderDateTo;
	}

	public void setOrderDateTo(LocalDate orderDateTo) {
		this.orderDateTo = orderDateTo;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getPaymentStore() {
		return paymentStore;
	}

	public void setPaymentStore(String paymentStore) {
		this.paymentStore = paymentStore;
	}

	@Override
	public PagingParam getPagination() {
		PagingParam pagination = super.getPagination();
		Map<String, Boolean> order = pagination.getOrder();
		Boolean flag = null;
		if (order != null)
			flag = order.get("customerDesc");
		if (flag != null) {
			order.put("customer.name", order.remove("customerDesc"));
		}
		return pagination;
	}

}
