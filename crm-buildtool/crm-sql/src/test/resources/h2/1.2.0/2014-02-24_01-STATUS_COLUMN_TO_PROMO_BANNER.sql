alter table CRM_PROMO_BANNER add STATUS varchar(15);

alter table CRM_PROMO_BANNER 
    add constraint FK_cnxq7lhx65h2fq7nhf2gh5tvg 
    foreign key (STATUS) 
    references CRM_REF_LOOKUP_DTL;