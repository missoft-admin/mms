/*
 * Copyright (c) 2012.
 * All rights reserved.
 */

package com.transretail.crm.common.security.service;

import org.springframework.security.authentication.encoding.PasswordEncoder;


public interface PasswordSupportService extends PasswordEncoder {

    String randomizePassword();

    String encodePassword(String rawPass);

    boolean isPasswordValid(String encPass, String rawPass);

}

