<%@ include file="common/taglibs.jsp" %>
<%@ taglib prefix="permission" tagdir="/WEB-INF/tags/permission" %>

<spring:url var="contextPath" value="/"/>
<spring:message code="role_admin" var="role_admin"/>
<spring:message code="role_customerservice" var="role_customerservice"/>
<spring:message code="role_css" var="role_css" />
<spring:message code="role_marketing" var="role_marketing" />
<spring:message code="role_marketing_head" var="role_marketing_head" />
<spring:message code="role_hr" var="role_hr" />
<spring:message code="role_hrs" var="role_hrs" />
<spring:message code="role_merchant_service" var="role_merchant_service" />
<spring:message code="role_merchant_service_supervisor" var="role_merchant_service_supervisor" />
<spring:message code="role_store_cashier_head" var="role_store_cashier_head" />
<spring:message code="role_store_manager" var="role_store_manager" />
<spring:message code="role_helpdesk" var="role_helpdesk" />
<spring:message code="role_member" var="role_member" />

<sec:authorize var="hasaccess_menu_marketingmgmt" ifAnyGranted="${role_marketing}, ${role_marketing_head}" />
<sec:authorize var="hasaccess_menu_giftcard" ifAnyGranted="${role_merchant_service}, ${role_merchant_service_supervisor}, ${role_store_manager}" />
<sec:authorize var="hasaccess_menu_giftcard_inventory" ifAnyGranted="${role_merchant_service_supervisor}, ${role_store_cashier_head}" />
<sec:authorize var="hasaccess_menu_membermgmt" ifAnyGranted="${role_customerservice}, ${role_css}" />
<sec:authorize var="hasaccess_menu_membermgmt_mgr" ifAnyGranted="${role_css}" />
<sec:authorize var="hasaccess_menu_employeemgmt" ifAnyGranted="${role_hr}, ${role_hrs}" />
<sec:authorize var="hasaccess_menu_employeemgmt_mgr" ifAnyGranted="${role_hrs}" />
<sec:authorize var="hasaccess_menu_programsetup" ifAnyGranted="${role_admin}" />
<sec:authorize var="hasaccess_menu_mediamgr" ifAnyGranted="${role_admin}" />



<li class="full-width last">

  <span class="arrow-icon-bottom"><span id="roleLabel"><spring:message code="menu_tab_role_admin" /></span></span>
  <div animate-in="slideDown" animate-out="slideUp" class="dropdown-menu row">
    <div class="content main-menu-dropdown">
      <c:if test="${hasaccess_menu_programsetup}">
      <div class="content_role" data-role-access="content_roleAccess1" data-role-label="<spring:message code="menu_tab_role_admin" />">
        <a class="btn btn-main-menu" href="#"><i class="icn-administrator"></i><p><spring:message code="menu_tab_role_admin" /></p></a>
      </div>
      </c:if>
      <c:if test="${hasaccess_menu_employeemgmt}">
      <div class="content_role" data-role-access="content_roleAccess2" data-role-label="<spring:message code="menu_tab_role_hr" />">
        <a class="btn btn-main-menu" href="#"><i class="icn-hr"></i><p><spring:message code="menu_tab_role_hr" /></p></a>
      </div>
      </c:if>
      <c:if test="${hasaccess_menu_marketingmgmt}">
      <div class="content_role" data-role-access="content_roleAccess3" data-role-label="<spring:message code="menu_tab_role_marketing" />">
        <a class="btn btn-main-menu" href="#"><i class="icn-marketing"></i><p><spring:message code="menu_tab_role_marketing" /></p></a>
      </div>
      </c:if>
      <c:if test="${hasaccess_menu_membermgmt}">
      <c:set var="roleAccess" value="" />
      <div class="content_role" data-role-access="content_roleAccess4" data-role-label="<spring:message code="menu_tab_role_customerservice" />">
        <a class="btn btn-main-menu" href="#"><i class="icn-cs"></i><p><spring:message code="menu_tab_role_customerservice" /></p></a>
      </div>
      </c:if>
      <c:if test="${hasaccess_menu_giftcard}">
      <div class="content_role" data-role-access="content_roleAccess5" data-role-label="<spring:message code="menu_tab_role_merchant_service" />">
        <a class="btn btn-main-menu" href="#"><i class="icn-merchant"></i><p><spring:message code="menu_tab_role_merchant_service" /></p></a>
      </div>
      </c:if>
      <%--<c:if test="${hasaccess_menu_giftcard_inventory}">--%>
      <%--<div class="content_role" data-role-access="content_roleAccess6" data-role-label="<spring:message code="menu_tab_role_merchant_supervisor" />">--%>
        <%--<a class="btn btn-main-menu" href="#"><i class="icn-merchant"></i><p><spring:message code="menu_tab_role_merchant_supervisor" /></p></a>--%>
      <%--</div>--%>
      <%--</c:if>--%>
    </div>
  </div>

</li>





  


  <c:if test="${hasaccess_menu_programsetup}">
  <li class="full-width last">
    <span class="arrow-icon-bottom content_roleAccess1 content_roleAccess" style="display: none;"><span><spring:message code="menutab_programsetup" /></span></span>
    <div animate-in="slideDown" animate-out="slideUp" class="dropdown-menu row content_roleAccess1 content_roleAccess">
      <div class="content main-menu-dropdown">
        <div class="">
          <a class="btn btn-main-menu menu2" href="<spring:url value="/report/template/list" />"><p><spring:message code="menutab_programsetup_maintenance_reports" /></p></a>
        </div>
        <div class="">
          <a class="btn btn-main-menu menu2" href="${contextPath}companyprofile"><p><spring:message code="menutab_programsetup_maintenance_company" /></p></a>
        </div>
        <div class="">
          <a class="btn btn-main-menu menu2" href="${contextPath}user"><p><spring:message code="menutab_programsetup_maintenance_user" /></p></a>
        </div>
        <%-- <div class="">
          <a class="btn btn-main-menu menu2" href="${contextPath}groups/store?page=1&amp;size=10"><p><spring:message code="menutab_programsetup_maintenance_store" /></p></a>
        </div> --%>
        <div class="">
          <a class="btn btn-main-menu menu2" href="${contextPath}lookup?page=1&amp;size=10"><p><spring:message code="menutab_programsetup_maintenance_lookup" /></p></a>
        </div>
        <%-- <div class="">
          <a class="btn btn-main-menu menu2" href="${contextPath}"><p><spring:message code="menutab_programsetup_maintenance_channels" /></p></a>
        </div> --%>
        <div class="">
          <a class="btn btn-main-menu menu2" href="${contextPath}member/points/config"><p><spring:message code="menutab_programsetup_maintenance_points" /></p></a>
        </div>
        <div class="">
          <a class="btn btn-main-menu menu2" href="${contextPath}override/permissions?page=1&amp;size=${empty param.size ? 10 : param.size}"><p><spring:message code="menutab_programsetup_permissions_hroverridescheme" /></p></a>
        </div>
        <div class="">
          <a class="btn btn-main-menu menu2" href="${contextPath}points/override/approver"><p><spring:message code="menutab_programsetup_permissions_pointsoverridescheme" /></p></a>
        </div>
      </div>
    </div>
  </li>
  </c:if>


  <c:if test="${hasaccess_menu_employeemgmt}">
  <li class="full-width last">
    <span class="arrow-icon-bottom content_roleAccess2 content_roleAccess" style="display: none;"><span><spring:message code="menutab_employeemgmt" /></span></span>
    <div animate-in="slideDown" animate-out="slideUp" class="dropdown-menu row content_roleAccess2 content_roleAccess">
      <div class="content main-menu-dropdown">
        <div class="">
          <a class="btn btn-main-menu menu2" href="${contextPath}employeemodels?page=1&amp;size=${empty param.size ? 10 : param.size}"><p><spring:message code="menutab_employeemgmt_helpdesk_emplist" /></p></a>
        </div>
        <div class="">
          <a class="btn btn-main-menu menu2" href="${contextPath}employee/adjustments/purchases?page=1&size=5"><p><spring:message code="menutab_employeemgmt_helpdesk_purchaseadjustment" /></p></a>
        </div>
        <div class="">
          <a class="btn btn-main-menu menu2" href="${contextPath}points/override"><p><spring:message code="menutab_employeemgmt_helpdesk_pointsadjustment" /></p></a>
        </div>

        <c:if test="${hasaccess_menu_employeemgmt_mgr}">
        <div class="">
          <a class="btn btn-main-menu menu2" href="${contextPath}pointsreward/generate/schedule"><p><spring:message code="menutab_employeemgmt_manager_rewardsched" /></p></a>
        </div>
        <div class="">
          <a class="btn btn-main-menu menu2" href="${contextPath}pointsreward/forprocessing"><p><spring:message code="menutab_employeemgmt_manager_rewardforapproval" /></p></a>
        </div>
        <div class="">
          <a class="btn btn-main-menu menu2" href="${contextPath}voucherscheme?page=1&size=5"><p><spring:message code="menutab_employeemgmt_manager_rewardscheme" /></p></a>
        </div>
        </c:if>
      </div>
    </div>
  </li>
  </c:if>


  <c:if test="${hasaccess_menu_marketingmgmt}">
  <li class="full-width last">
    <span class="arrow-icon-bottom content_roleAccess3 content_roleAccess" style="display: none;"><span><spring:message code="menutab_marketingmgmt" /></span></span>
    <div animate-in="slideDown" animate-out="slideUp" class="dropdown-menu row content_roleAccess3 content_roleAccess">
      <div class="content main-menu-dropdown">
        <div class="">
          <a class="btn btn-main-menu menu2" href="${contextPath}promotion"><p><spring:message code="menutab_marketingmgmt_config" /></p></a>
        </div>
        <div class="">
          <a class="btn btn-main-menu menu2" href="${contextPath}groups/store?page=1&amp;size=10"><p><spring:message code="menutab_marketingmgmt_config_storegroup" /></p></a>
        </div>
        <div class="">
          <a class="btn btn-main-menu menu2" href="${contextPath}groups/product?page=1&amp;size=10"><p><spring:message code="menutab_marketingmgmt_config_productgroup" /></p></a>
        </div>
        <div class="">
          <a class="btn btn-main-menu menu2" href="${contextPath}groups/member"><p><spring:message code="menutab_marketingmgmt_campaign_customergroup" /></p></a>
        </div>
      </div>
    </div>
  </li>
  </c:if>


  <c:if test="${hasaccess_menu_membermgmt}">
  <li class="full-width last">
    <span class="arrow-icon-bottom content_roleAccess4 content_roleAccess hide" style="display: none;"><span><spring:message code="menutab_membermgmt" /></span></span>
    <div animate-in="slideDown" animate-out="slideUp" class="dropdown-menu row content_roleAccess4 content_roleAccess">
      <div class="content main-menu-dropdown">
        <div class="">
          <a class="btn btn-main-menu menu2" href="${contextPath}customermodels?page=1&size=10"><p><spring:message code="menutab_membermgmt_loyalty_members" /></p></a>
        </div>
        <div class="">
          <a class="btn btn-main-menu menu2" href="${contextPath}points/override"><p><spring:message code="menutab_membermgmt_loyalty_adjustment" /></p></a>
        </div>
      </div>
    </div>
  </li>
  </c:if>


  <c:if test="${hasaccess_menu_giftcard}">
  <li class="full-width last">
    <span class="arrow-icon-bottom content_roleAccess5 content_roleAccess" style="display: none;"><span><spring:message code="menu_tab_role_merchant_service" /></span></span>
    <div animate-in="slideDown" animate-out="slideUp" class="dropdown-menu row content_roleAccess5 content_roleAccess">
      <div class="content main-menu-dropdown">
        <div class="">
          <a class="btn btn-main-menu menu2" href="<spring:url value="/giftcard/vendor/list" />"><p><spring:message code="menutab_giftcard_vendors" /></p></a>
        </div>
        <div class="">
          <a class="btn btn-main-menu menu2" href="<spring:url value="/giftcard/list" />"><p><spring:message code="menutab_giftcard_list" /></p></a>
        </div>
        <div class="">
          <a class="btn btn-main-menu menu2" href="<spring:url value="/manufactureorder" />"><p><spring:message code="menutab_manufacture_order" /></p></a>
        </div>
        <div class="">
          <a class="btn btn-main-menu menu2" href="<spring:url value="/loyaltycardinventory" />"><p><spring:message code="menutab_loyalty_card_inventory" /></p></a>
        </div>
        <%--<div class="">--%>
          <%--<a class="btn btn-main-menu menu2" href="<spring:url value="/giftcard/order/list" />"><p><spring:message code="menutab_giftcard_orders" /></p></a>--%>
        <%--</div>--%>
        <%--<div class="">--%>
          <%--<a class="btn btn-main-menu menu2" href="<spring:url value="/giftcard/stock/list"/>"><p><spring:message code="menutab_giftcard_stockrequest" /></p></a>--%>
        <%--</div>--%>
      </div>
    </div>
  </li>
  </c:if>

	<li class="right open">
		<span class="arrow-icon-bottom">Notifications</span>
		<div class="dropdown-menu minor-menu">
			<ul id="notifications" class="main-menu-dropdown">
			</ul>
		</div>
	</li>

  <%--<c:if test="${hasaccess_menu_giftcard_inventory}">--%>
  <%--<li class="full-width last">--%>
    <%--<span class="arrow-icon-bottom content_roleAccess6 content_roleAccess" style="display: none;"><span><spring:message code="menutab_giftcard_inventory" /></span></span>--%>
    <%--<div animate-in="slideDown" animate-out="slideUp" class="dropdown-menu row content_roleAccess6 content_roleAccess">--%>
      <%--<div class="content main-menu-dropdown">--%>
        <%--<div class="">--%>
          <%--<a class="btn btn-main-menu menu2" href="<spring:url value="/giftcard/stock/list"/>"><p><spring:message code="menutab_giftcard_stockrequest" /></p></a>--%>
        <%--</div>--%>
        <%--<div class="">--%>
          <%--<a class="btn btn-main-menu menu2" href="<spring:url value="/giftcard/inventory/list" />"><p><spring:message code="menutab_giftcard_inventory_list" /></p></a>--%>
        <%--</div>--%>
        <%--<c:if test="${role_merchant_service_supervisor}">--%>
        <%--<div class="">--%>
          <%--<a class="btn btn-main-menu menu2" href="<spring:url value="/giftcard/inventory/receive"/>"><p><spring:message code="menutab_giftcard_inventory_receive" /></p></a>--%>
        <%--</div>--%>
        <%--</c:if>--%>
      <%--</div>--%>
    <%--</div>--%>
  <%--</li>--%>
  <%--</c:if>--%>




<script src="<c:url value="/js/menu.js"/>" type="text/javascript"></script>
