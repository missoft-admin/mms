package com.transretail.crm.core.dto;


import org.joda.time.LocalDate;

import com.transretail.crm.core.entity.Promotion;
import com.transretail.crm.core.entity.PromotionRedemption;


public class PromotionRedemptionDto  {


	private Long id;
    private Long promotion;
    private Long qty;
    private String sku;
    private String productName;
    private LocalDate redeemFrom;
    private LocalDate redeemTo;

	public PromotionRedemptionDto() {}
	public PromotionRedemptionDto( PromotionRedemption model ) {
        this.id = model.getId();
        if (model.getPromotion() != null) {
            this.promotion = model.getPromotion().getId();
        }
        this.sku = model.getSku();
        this.qty = model.getQty();
        this.redeemFrom = model.getRedeemFrom();
        this.redeemTo = model.getRedeemTo();
	}

	public PromotionRedemption toModel() {
		return toModel( new PromotionRedemption() );
	}

	public PromotionRedemption toModel( PromotionRedemption model ) {
		if ( null == model ) {
			model = new PromotionRedemption();
		}
        if (promotion != null) {
            model.setPromotion(new Promotion(promotion));
        } else {
            model.setPromotion(null);
        }
       
        model.setSku(this.sku);
        model.setQty(this.qty);
        model.setRedeemFrom(this.redeemFrom);
        model.setRedeemTo(this.redeemTo);
        return model;
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Long getPromotion() {
		return promotion;
	}
	public void setPromotion(Long promotion) {
		this.promotion = promotion;
	}
	public Long getQty() {
		return qty;
	}
	public void setQty(Long qty) {
		this.qty = qty;
	}
	public String getSku() {
		return sku;
	}
	public void setSku(String sku) {
		this.sku = sku;
	}
	public String getProductName() {
		return productName;
	}
	public void setProductName(String productName) {
		this.productName = productName;
	}
	public LocalDate getRedeemFrom() {
		return redeemFrom;
	}
	public void setRedeemFrom(LocalDate redeemFrom) {
		this.redeemFrom = redeemFrom;
	}
	public LocalDate getRedeemTo() {
		return redeemTo;
	}
	public void setRedeemTo(LocalDate redeemTo) {
		this.redeemTo = redeemTo;
	}





	
}
