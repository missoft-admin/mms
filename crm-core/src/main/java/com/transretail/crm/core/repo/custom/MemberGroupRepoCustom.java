package com.transretail.crm.core.repo.custom;

import java.util.List;

import com.transretail.crm.common.service.dto.request.PageSortDto;
import com.transretail.crm.common.service.dto.response.ResultList;
import com.transretail.crm.core.dto.MemberDto;
import com.transretail.crm.core.dto.MemberGroupFieldDto;
import com.transretail.crm.core.entity.MemberGroup;

public interface MemberGroupRepoCustom {

	ResultList<MemberDto> getQualifiedMembers(List<MemberGroupFieldDto> fields,
			MemberGroup group, PageSortDto sortDto);
	
	List<Long> getQualifiedMembers(List<MemberGroupFieldDto> fields,
			MemberGroup group);
	
	List<MemberDto> getQualifiedMembersForPrint(
            List<MemberGroupFieldDto> fields, MemberGroup group,
            PageSortDto sortDto);
	
	List<MemberGroup> getCardTypeMemberGroups();

    boolean isQualifiedMember(List<MemberGroupFieldDto> fields, MemberGroup group, String accountId);

    Long getQualifiedMembersCount(List<MemberGroupFieldDto> fields,
            MemberGroup group);

}
