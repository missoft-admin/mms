<%@ include file="../common/taglibs.jsp" %>

<div id="rewardRejectDialog" class="modal hide  groupModal nofly" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
   
      <div class="modal-body">
      
      <c:set var="ENCTYPE" value="application/x-www-form-urlencoded"/>
      <c:if test="${multipart}"><c:set var="ENCTYPE" value="multipart/form-data"/></c:if>
      <c:url value="/pointsreward/reject/" var="path_reject"/>
      <form:form class="form-horizontal" id="rewardRejectForm" modelAttribute="rewardBatch" action="${path_reject}" method="POST" enctype="${ENCTYPE}">
      
            <div class="control-group">  
                <label class="control-label" for="message"><spring:message code="label_reject_msg" text=""/></label>  
                <div class="controls">  
                    <form:textarea id="rejectMessage" path="message" />
                    <form:hidden path="incDateFrom" />
                    <form:hidden path="incDateTo" />
                </div>  
            </div>

      </form:form>
      
      </div>
      <div class="modal-footer">
        <button type="button" id="cancelButton" class="btn " data-dismiss="modal"><spring:message code="label_cancel"/></button>
        <button type="button" id="rejectButton" class="btn btn-primary"><spring:message code="label_reject" /></button>
      </div>
    </div>
  </div>
</div>

  <script type="text/javascript" src="<c:url value="/js/viewspecific/points_reward/rewardRejectDialog.js" />"><![CDATA[&nbsp;]]></script>

