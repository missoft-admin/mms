package com.transretail.crm.giftcard.repo;

import org.springframework.stereotype.Repository;

import com.transretail.crm.common.repo.CrmQueryDslPredicateExecutor;
import com.transretail.crm.giftcard.entity.GiftCard;

/**
 *
 */
@Repository
public interface GiftCardRepo extends CrmQueryDslPredicateExecutor<GiftCard, Long> {
}
