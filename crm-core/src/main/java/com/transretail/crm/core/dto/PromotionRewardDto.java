package com.transretail.crm.core.dto;


import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.mysema.query.types.expr.BooleanExpression;
import com.transretail.crm.common.service.dto.request.AbstractSearchFormDto;
import com.transretail.crm.core.entity.Promotion;
import com.transretail.crm.core.entity.PromotionReward;
import com.transretail.crm.core.entity.lookup.LookupDetail;


public class PromotionRewardDto extends AbstractSearchFormDto {

//	private static final String[] IGNORE = { "promotion", "rewardType" };

	private Long id;

    private Long promotion;

    private String rewardType;
	private String remark;

    private BigDecimal minAmount;
    private BigDecimal maxAmount;
    private Long minQty;
    private Long maxQty;
    private BigDecimal baseAmount;
    private Double baseFactor;

    private BigDecimal maxBonus;
    private Double discount;
    
    private Boolean fixed;
    private String productCode;
    private String productName;

	public PromotionRewardDto() {}
	public PromotionRewardDto( PromotionReward inReward ) {
//		BeanUtils.copyProperties( inReward, this, IGNORE );
        this.id = inReward.getId();
        if (inReward.getPromotion() != null) {
            this.promotion = inReward.getPromotion().getId();
        }
        if ( null != inReward.getRewardType() ) {
			rewardType = inReward.getRewardType().getCode();
		}
        this.remark = inReward.getRemark();
        this.minAmount = inReward.getMinAmount();
        this.maxAmount = inReward.getMaxAmount();
        this.minQty = inReward.getMinQty();
        this.maxQty = inReward.getMaxQty();
        this.baseAmount = inReward.getBaseAmount();
        this.baseFactor = inReward.getBaseFactor();
		this.maxBonus = inReward.getMaxBonus();
        this.discount = inReward.getDiscount();
        this.fixed = inReward.getFixed();
        this.productCode = inReward.getProductCode();
	}

	public PromotionReward toModel() {
		return toModel( new PromotionReward() );
	}

	public PromotionReward toModel( PromotionReward inModel ) {
		if ( null == inModel ) {
			inModel = new PromotionReward();
		}
//		BeanUtils.copyProperties( this, inModel, IGNORE );
        if (promotion != null) {
            inModel.setPromotion(new Promotion(promotion));
        } else {
            inModel.setPromotion(null);
        }
        if(StringUtils.isNotBlank(rewardType)) {
            inModel.setRewardType(new LookupDetail(rewardType));
        } else {
            inModel.setRewardType(null);
        }
        inModel.setRemark(this.remark);
        inModel.setMinAmount(this.minAmount);
        inModel.setMaxAmount(this.maxAmount);
        inModel.setMinQty(this.minQty);
        inModel.setMaxQty(this.maxQty);
        inModel.setBaseAmount(this.baseAmount);
        inModel.setBaseFactor(this.baseFactor);
        inModel.setMaxBonus(this.maxBonus);
        inModel.setDiscount(this.discount);
        inModel.setFixed(this.fixed);
        inModel.setProductCode( this.productCode );
        return inModel;
	}





	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getPromotion() {
		return promotion;
	}

	public void setPromotion(Long promotion) {
		this.promotion = promotion;
	}

	public String getRewardType() {
		return rewardType;
	}
	public void setRewardType(String rewardType) {
		this.rewardType = rewardType;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	public BigDecimal getMinAmount() {
		return minAmount;
	}

	public void setMinAmount(BigDecimal minAmount) {
		this.minAmount = minAmount;
	}

	public BigDecimal getMaxAmount() {
		return maxAmount;
	}

	public void setMaxAmount(BigDecimal maxAmount) {
		this.maxAmount = maxAmount;
	}

	public Long getMinQty() {
		return minQty;
	}

	public void setMinQty(Long minQty) {
		this.minQty = minQty;
	}

	public Long getMaxQty() {
		return maxQty;
	}

	public void setMaxQty(Long maxQty) {
		this.maxQty = maxQty;
	}

	public BigDecimal getBaseAmount() {
		return baseAmount;
	}

	public void setBaseAmount(BigDecimal baseAmount) {
		this.baseAmount = baseAmount;
	}

	public Double getBaseFactor() {
		return baseFactor;
	}

	public void setBaseFactor(Double baseFactor) {
		this.baseFactor = baseFactor;
	}

	public BigDecimal getMaxBonus() {
		return maxBonus;
	}

	public void setMaxBonus(BigDecimal maxBonus) {
		this.maxBonus = maxBonus;
	}

	public Double getDiscount() {
		return discount;
	}

	public void setDiscount(Double discount) {
		this.discount = discount;
	}


	public Boolean getFixed() {
		return fixed;
	}
	public void setFixed(Boolean fixed) {
		this.fixed = fixed;
	}
	public String getProductCode() {
		return productCode;
	}
	public void setProductCode(String productCode) {
		this.productCode = productCode;
	}
	public String getProductName() {
		return productName;
	}
	public void setProductName(String productName) {
		this.productName = productName;
	}
	@Override
	@JsonIgnore
	public BooleanExpression createSearchExpression() {
		
		List<BooleanExpression> expressions = new ArrayList<BooleanExpression>();
		//QPromotionReward

        

		return BooleanExpression.allOf(expressions.toArray(new BooleanExpression[expressions.size()]));
	}
}
