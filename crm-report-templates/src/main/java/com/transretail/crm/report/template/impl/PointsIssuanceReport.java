package com.transretail.crm.report.template.impl;

import com.google.common.collect.Maps;
import com.google.common.collect.Sets;
import com.mysema.query.jpa.impl.JPAQuery;
import com.mysema.query.types.expr.StringExpression;
import com.transretail.crm.common.reporting.jasper.ClasspathFileResolver;
import com.transretail.crm.common.reporting.jasper.DefaultJRProcessor;
import com.transretail.crm.common.reporting.jasper.JRProcessor;
import com.transretail.crm.core.dto.PointsIssuanceDto;
import com.transretail.crm.core.dto.QPointsIssuanceDto;
import com.transretail.crm.core.entity.QPointsTxnModel;
import com.transretail.crm.core.entity.QPromotion;
import com.transretail.crm.core.entity.QTransactionAudit;
import com.transretail.crm.core.entity.enums.PointTxnType;
import com.transretail.crm.core.entity.enums.ReportType;
import com.transretail.crm.core.entity.lookup.QLookupDetail;
import com.transretail.crm.report.template.FilterField;
import com.transretail.crm.report.template.ReportTemplate;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.apache.commons.collections.comparators.ComparatorChain;
import org.apache.commons.lang3.StringUtils;
import org.joda.time.DateTimeUtils;
import org.joda.time.LocalDate;
import org.springframework.context.MessageSource;
import org.springframework.context.MessageSourceAware;
import org.springframework.context.NoSuchMessageException;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.context.support.MessageSourceAccessor;
import org.springframework.stereotype.Service;

/**
 *
 * @author Monte Cillo Co (mco@exist.com)
 */
@Service("pointsIssuanceReport")
public final class PointsIssuanceReport implements ReportTemplate, MessageSourceAware {

    protected static final String REPORT_POINTS_ISSUANCE_PERMISSION = "REPORT_POINTS_ISSUANCE_BY_TYPES_SUMMARY_VIEW";
    protected static final String TEMPLATE_NAME = "Points Issuance Report";
    protected static final String REPORT_NAME = "reports/points-issuance-report.jasper";
    protected static final String REPORT_PARAM_MONTH = "MONTH";
    protected static final String REPORT_PARAM_YEAR = "YEAR";
    protected static final String REPORT_PARAM_CHART_DATASOURCE = "CHART_DATA";
    protected static final String REPORT_PARAM_DATASOURCE_EMPTY = "DATASOURCE_EMPTY";
    protected static final String REPORT_PARAM_REWARD_TYPE_COMPARATOR = "REWARD_TYPE_COMPARATOR";
    protected static final String FILTER_TARGET_MONTH = "targetMonth";
    protected static final String FILTER_TARGET_YEAR = "targetYear";
    private static final QPointsTxnModel qpoints = QPointsTxnModel.pointsTxnModel;
    private static final QPromotion qpromo = QPromotion.promotion;
    private static final QTransactionAudit qta = QTransactionAudit.transactionAudit;
    private static final QLookupDetail qld = QLookupDetail.lookupDetail;
    private MessageSourceAccessor messageSource;
    @PersistenceContext
    private EntityManager em;
    
    @Override
    public ReportType[] getReportTypes() {
    	return ReportType.values();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getName() {
	return TEMPLATE_NAME;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getDescription() {
	return messageSource.getMessage("points.issuance.report.title");
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Set<FilterField> getFilters() {
	Set<FilterField> filters = Sets.newHashSetWithExpectedSize(2);
	filters.add(createMonthFilterFiled());
	filters.add(createYearFilterField());

	return filters;
    }

    private FilterField createYearFilterField() {
	final StringExpression yearAsStringExp = qpromo.startDate.year().stringValue();
	Map<String, String> availableYearsInPromotion = new JPAQuery(em)
		.from(qpromo).where(yearAsStringExp.isNotNull()).distinct().map(yearAsStringExp, yearAsStringExp);
	return FilterField.createDropdownField(FILTER_TARGET_YEAR,
		messageSource.getMessage("points.issuance.filter.year"), availableYearsInPromotion, true);
    }

    private FilterField createMonthFilterFiled() {
	String[] months = DateTimeUtils.getDateFormatSymbols(
		LocaleContextHolder.getLocale()).getMonths();
	Map<String, String> monthChoices = Maps.newHashMapWithExpectedSize(months.length - 1);
	for (int i = 1; i <= months.length - 1; i++) {
	    monthChoices.put(String.valueOf(i), months[i - 1]);
	}
	return FilterField.createDropdownField(FILTER_TARGET_MONTH,
		messageSource.getMessage("points.issuance.filter.month"), monthChoices, true);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean canView(Set<String> permissions) {
	if (permissions != null) {
	    for (String permission : permissions) {
		if (permission.equalsIgnoreCase(REPORT_POINTS_ISSUANCE_PERMISSION)) {
		    return true;
		}
	    }
	}
	return false;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public JRProcessor createJrProcessor(Map<String, String> map) {
	String month = StringUtils.defaultIfBlank(map.get(FILTER_TARGET_MONTH), "0");
	int monthIndex = Integer.valueOf(month);
	String year = StringUtils.defaultIfBlank(map.get(FILTER_TARGET_YEAR), "0");
	int yearInt = Integer.valueOf(year);

	final LocalDate baseDate = new LocalDate(yearInt, monthIndex, 1);
	Date startDate = baseDate.toDate();
	Date endDate = new LocalDate(yearInt, monthIndex,
		baseDate.dayOfMonth().withMaximumValue().getDayOfMonth()).toDate();

	List<PointsIssuanceDto> mainBeanDataSource = retrieveData(startDate, endDate);
	List<PointsIssuanceDto> rewards = retriveDataWithRewardTxnType(startDate, endDate);
	mainBeanDataSource.addAll(rewards); // merge
	boolean emptyOriginalDs = mainBeanDataSource.isEmpty();
	fixedData(startDate, endDate, mainBeanDataSource, !rewards.isEmpty());
	List<PointsIssuanceDto> chartData = getChartData(Collections.unmodifiableList(mainBeanDataSource));
	DefaultJRProcessor jrProcessor = new DefaultJRProcessor(REPORT_NAME, mainBeanDataSource);

	Map<String, Object> parameters = Maps.newHashMapWithExpectedSize(3);
	parameters.put(REPORT_PARAM_MONTH, baseDate.monthOfYear().getAsText(LocaleContextHolder.getLocale()));
	parameters.put(REPORT_PARAM_YEAR, year);
	parameters.put(REPORT_PARAM_DATASOURCE_EMPTY, emptyOriginalDs);
	parameters.put(REPORT_PARAM_CHART_DATASOURCE, chartData);
	parameters.put(REPORT_PARAM_REWARD_TYPE_COMPARATOR, new RewardTypeComparatorString());
	jrProcessor.addParameters(parameters);
	jrProcessor.setFileResolver(new ClasspathFileResolver("reports"));

	return jrProcessor;
    }

    private void fixedData(Date startDate, Date endDate, List<PointsIssuanceDto> mainBeanDataSource, boolean hasEPoints) {
	List<String> rewardType = new JPAQuery(em).distinct().from(qpoints, qta, qpromo, qld)
		.where(qpoints.transactionType.eq(PointTxnType.EARN)
			.and(qpoints.transactionNo.eq(qta.transactionNo))
			.and(qta.promotion.id.eq(qpromo.id))
			.and(qpromo.rewardType.code.eq(qld.code))
			.and(qpoints.transactionDateTime.between(startDate, endDate)))
		.list(qpromo.rewardType.description);
	if (hasEPoints) {
	    rewardType.add(PointTxnType.REWARD.name());
	}

	int lastDayOfMonth = new LocalDate(startDate.getTime())
		.dayOfMonth().withMaximumValue().getDayOfMonth();
	for (int i = 1; i <= lastDayOfMonth; i++) {
	    for (String r : rewardType) {
		mainBeanDataSource.add(new PointsIssuanceDto(r, 0, i));
	    }
	}
    }

    /**
     * Used by JasperReports, for Crosstabs RewardType Sorting
     */
    protected class RewardTypeComparatorString implements Comparator<String> {

	@Override
	public int compare(String o1, String o2) {
	    return PointsIssuanceReport.compare(toSortOrder(o1), toSortOrder(o2));
	}
    }
    
    public static int compare(int x, int y) {
	return (x < y) ? -1 : ((x == y) ? 0 : 1);
    }
    
    /**
     * Helper methods to translate in int order of the RewardType. It is easy to
     * sort int that a custom String ordering.
     *
     * @param s
     * @return
     */
    private int toSortOrder(String s) {
	// POINTS and ITEM POINT is in LookupDetail
	if ("POINTS".equals(s)) {
	    return -1;
	} else if ("ITEM POINT".equals(s)) {
	    return 0;
	} else if (PointTxnType.REWARD.toString().equals(s) // issue with Jasper chart component in rendering category series rb.
		|| s.equalsIgnoreCase(messageSource.getMessage("points.issuance.report.employee.reward.label"))) {
	    return 1;
	} else {
	    return 2;
	}
    }

    protected class RewardTypeComparator implements Comparator<PointsIssuanceDto> {

	@Override
	public int compare(PointsIssuanceDto o1, PointsIssuanceDto o2) {
	    return PointsIssuanceReport.compare(toSortOrder(o1.getRewardType()), toSortOrder(o2.getRewardType()));
	}
    }

    private List<PointsIssuanceDto> getChartData(List<PointsIssuanceDto> mainDatasource) throws NoSuchMessageException {
	Map<Integer, Map<String, Double>> perDayMap = new HashMap<Integer, Map<String, Double>>();
	for (PointsIssuanceDto p : mainDatasource) {
	    Map<String, Double> nmap = perDayMap.get(p.getDay());
	    if (nmap == null) {
		Map<String, Double> value = new HashMap<String, Double>();
		value.put(p.getRewardType(), p.getTotalPointsPerDay());
		perDayMap.put(p.getDay(), value);
	    } else {
		nmap = perDayMap.get(p.getDay());
		if (nmap.get(p.getRewardType()) == null) {
		    nmap.put(p.getRewardType(), p.getTotalPointsPerDay());
		} else {
		    Double totalPerDay = nmap.get(p.getRewardType());
		    nmap.put(p.getRewardType(), totalPerDay + p.getTotalPointsPerDay());
		}
		perDayMap.put(p.getDay(), nmap);
	    }
	}
	final String ePoints = messageSource.getMessage("points.issuance.report.employee.reward.label");
	List<PointsIssuanceDto> summary = new ArrayList<PointsIssuanceDto>();
	for (Map.Entry<Integer, Map<String, Double>> perDayEntry : perDayMap.entrySet()) {
	    Map<String, Double> byRewardMap = perDayEntry.getValue();

	    for (Map.Entry<String, Double> byRewardEntry : byRewardMap.entrySet()) {
		String rewardType = byRewardEntry.getKey();
		boolean isEmployeeReward = rewardType.equalsIgnoreCase(PointTxnType.REWARD.name());

		summary.add(new PointsIssuanceDto(isEmployeeReward ? ePoints : rewardType,
			byRewardEntry.getValue(), perDayEntry.getKey()));
	    }
	}
	// Total Points per Day 
	Map<Integer, Double> totalPerDay = Maps.newHashMap();

	for (PointsIssuanceDto p : mainDatasource) {
	    Double value = totalPerDay.get(p.getDay());
	    if (value == null) {
		totalPerDay.put(p.getDay(), p.getTotalPointsPerDay());
	    } else {
		totalPerDay.put(p.getDay(), value + p.getTotalPointsPerDay());
	    }
	}

	List<PointsIssuanceDto> list = new ArrayList<PointsIssuanceDto>(summary);
	final String totalPointsSeriesLabel = messageSource.getMessage("points.issuance.report.chart.series.label");
	for (Map.Entry<Integer, Double> entry : totalPerDay.entrySet()) {
	    list.add(new PointsIssuanceDto(totalPointsSeriesLabel, entry.getValue(), entry.getKey()));
	}
	// sort by day ASC
	ComparatorChain cc = new ComparatorChain(new RewardTypeComparator());
	cc.addComparator(new Comparator<PointsIssuanceDto>() {

	    @Override
	    public int compare(PointsIssuanceDto o1, PointsIssuanceDto o2) {
		return o1.getDay() - o2.getDay();
	    }
	});
	Collections.sort(list, cc);
	return list;
    }

    private List<PointsIssuanceDto> retriveDataWithRewardTxnType(Date startDate, Date endDate) {
	final List<PointsIssuanceDto> rewards = new JPAQuery(em).from(qpoints)
		.where(qpoints.transactionType.eq(PointTxnType.REWARD)
			.and(qpoints.transactionDateTime.between(startDate, endDate)))
		.groupBy(qpoints.transactionDateTime.dayOfMonth(), qpoints.transactionType.stringValue())
		.orderBy(qpoints.transactionDateTime.dayOfMonth().asc())
		.list(new QPointsIssuanceDto(qpoints.transactionType.stringValue(),
				qpoints.transactionPoints.sum(),
				qpoints.transactionDateTime.dayOfMonth()));

	return rewards;
    }

    private List<PointsIssuanceDto> retrieveData(Date startDate, Date endDate) {
	final List<PointsIssuanceDto> rows = new JPAQuery(em).from(qpoints, qta, qpromo, qld)
		.where(qpoints.transactionType.eq(PointTxnType.EARN)
			.and(qpoints.transactionNo.eq(qta.transactionNo))
			.and(qta.promotion.id.eq(qpromo.id))
			.and(qpromo.rewardType.code.eq(qld.code))
			.and(qpoints.transactionDateTime.between(startDate, endDate)))
		.groupBy(qld.description, qpromo.rewardType.code, qpoints.transactionDateTime.dayOfMonth())
		//		.orderBy(qpoints.transactionDateTime.asc())
		.list(new QPointsIssuanceDto(qld.description,
				qpoints.transactionPoints.sum(),
				qpoints.transactionDateTime.dayOfMonth()));

	return rows;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void setMessageSource(MessageSource messageSource) {
	this.messageSource = new MessageSourceAccessor(messageSource);
    }

}
