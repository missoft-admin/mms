
    create table CRM_GC_SERVICE_REQUEST (
        ID number(19,0) not null,
        DATE_FILED timestamp,
        DETAILS varchar2(500 char) not null,
        FILED_BY varchar2(255 char) not null,
        CARD_NO varchar2(20 char) not null,
        primary key (ID)
    );

    alter table CRM_GC_SERVICE_REQUEST 
        add constraint FK_mdmxwm9dgbvrt5hamm2lmxisr 
        foreign key (CARD_NO) 
        references CRM_GC_INVENTORY (BARCODE);