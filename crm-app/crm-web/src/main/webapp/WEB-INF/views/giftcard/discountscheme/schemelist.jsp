<%@include file="../../common/taglibs.jsp" %>

<spring:message code="gc_dcscheme_min" var="itemType" />

<div class="page-header page-header2">
    <h1><spring:message code="gc_dcscheme" /></h1>
</div>


  
<div class="pull-right mb20 clearfix">
<button type="button" id="createScheme" class="btn btn-primary"><spring:message code="create_gc_dcscheme" /></button>
</div>

<div class="badge clearfix" style="margin-top: 20px;">
* <spring:message code="gc_dcscheme_inbillionrupiah" />
</div>


<div id="scheme_list_container">
</div>


<div class="modal hide  nofly" id="schemeDialog" tabindex="-1" role="dialog" aria-hidden="true">
</div>

<jsp:include page="../../common/confirm.jsp" />

  <link href="<spring:url value="/css/dataTables.css" />" rel="stylesheet">
  <script src="<spring:url value="/js/jquery/jquery.dataTables.js" />" ></script>
  <script src="<spring:url value="/js/jquery/jquery.dataTables.linkLengthMenu.js" />"></script>
  <script src="<spring:url value="/js/jquery/jquery.dataTables.customPaging.js" />"></script>
  <script src="<spring:url value="/js/jquery/ajax-datatable.js" />" ></script>
  <script src="<spring:url value="/js/common/form2js.js" />" ></script>
  <script src="<spring:url value="/js/common/json2.js" />" ></script>
  <script src="<spring:url value="/js/jquery/jquery.toObject.js" />" ></script>
  <link href="<c:url value="/styles/temp.css" />" rel="stylesheet" />
  
  
  <script src="<spring:url value="/js/jquery/jquery.numberInput.js" />" ></script>
  <script src="<c:url value="/js/bootstrap/bootstrap-datepicker.js"/>" type='text/javascript'><![CDATA[&nbsp; ]]></script>
  <link href="<c:url value="/css/bootstrap/datepicker.css" />" rel="stylesheet" />
  <link href="<c:url value="/styles/temp.css" />" rel="stylesheet" />
  <script src="<spring:url value="/js/jquery/jquery.bindWithDelay.js" />"></script>
  <script src="<spring:url value="/js/jquery/jquery.ajaxQueue.js" />"></script>

<script>
$(document).ready(function() {
	
	$schemeDialog = $("#schemeDialog").modal({
		show: false
	}).on('hidden.bs.modal', function() {
		$(this).empty();
	}).css({
		"width": "900px",
	});
	
	
	$("#createScheme").click(function() {
		$.get("<c:url value="/giftcard/discountscheme/form" />", function(data) {
			$schemeDialog.modal("show").html(data);
		}, "html");
	});
	
	$("#scheme_list_container").ajaxDataTable({
		'autoload'  : true,
		'ajaxSource' : "<c:url value="/giftcard/discountscheme/search" />",
		'columnHeaders' : [
			"<spring:message code="gc_dcscheme_customer"/>",
			"<spring:message code="gc_dcscheme_minpurchase"/> *",
			"<spring:message code="gc_dcscheme_startingdiscount"/>",
			"<spring:message code="gc_dcscheme_requiredaccumulation"/> *",
			"<spring:message code="gc_dcscheme_endingdiscount"/>",
			"<spring:message code="gc_dcscheme_startdate"/>",
			"<spring:message code="gc_dcscheme_enddate"/>",
			""
		],
		'modelFields' : [
			{name : 'customerName', sortable : false},
			{name : 'minPurchaseFormatted', sortable : false},
			{name : 'startingDiscount', sortable : false, fieldCellRenderer : function (data, type, row) {
				if(row.startingDiscount != null)
            		return row.startingDiscount + "%";
				return "";
            }},
			{name : 'purchaseForMaxDiscountFormatted', sortable : false},
			{name : 'endingDiscount', sortable : false, fieldCellRenderer : function (data, type, row) {
				return row.endingDiscount + "%";
            }},
			{name : 'startDate', sortable : false},
			{name : 'endDate', sortable : false},
			{customCell : function ( innerData, sSpecific, json ) {
				if (sSpecific == 'display') {
					var html = '<button type="button" data-scheme-id="'+json.id+'" class="tiptip btn pull-left icn-edit schemeBtn" title="<spring:message code="label_update_arg" arguments="${itemType}" />"><spring:message code="label_update_arg" arguments="${itemType}" /></button>';
					if(!json.readOnly) {
						html += '<button type="button" data-scheme-id="'+json.id+'" class="tiptip btn pull-left icn-delete schemeDltBtn" title="<spring:message code="entity_delete" arguments="${itemType}" />"><spring:message code="entity_delete" arguments="${itemType}" /></button>';
					}
					return html; 
				} else {
					return "";
				}
			}
			}
		]
	}).on("click", ".schemeBtn", function() {
		$.get("<c:url value="/giftcard/discountscheme/form/" />" + $(this).data("schemeId"), function(data) {
			$schemeDialog.modal("show").html(data);
		}, "html");
	}).on("click", ".schemeDltBtn", function() {
		var itemId = $(this).data("schemeId");
		getConfirm('<spring:message code="entity_delete_confirm" />', function(result) { 
			if(result) {
				$.post("<c:url value="/giftcard/discountscheme/" />" + itemId, {_method: 'DELETE'}, function(data) {
					location.reload();
				});
			}
		});
	});
	
	
	
	function initCommonFields(dialog) {
		$(".floatInput", $(dialog)).numberInput({ "type" : "float" });
		$(".intInput", $(dialog)).numberInput({ "type" : "int" });
	    $(".wholeInput", $(dialog)).numberInput({ "type" : "whole" });
	}
});
</script>