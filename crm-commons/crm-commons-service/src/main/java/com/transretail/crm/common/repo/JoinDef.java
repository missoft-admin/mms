package com.transretail.crm.common.repo;

import org.springframework.util.Assert;

import com.mysema.query.JoinType;
import com.mysema.query.types.EntityPath;
import com.mysema.query.types.Path;

/**
 * @author Allan G. Ramirez (agramirez@exist.com)
 */
public class JoinDef<T> {
    private EntityPath<T> target;
    private Path<T> alias;
    private JoinType joinType;

    public JoinDef(EntityPath<T> target, JoinType joinType) {
        this(target, null, joinType);
    }

    public JoinDef(EntityPath<T> target, Path<T> alias, JoinType joinType) {
        Assert.notNull(target);
        Assert.notNull(joinType);
        Assert.isTrue(joinType != JoinType.DEFAULT, JoinType.DEFAULT + " is not allowed.");
        this.target = target;
        this.alias = alias;
        this.joinType = joinType;
    }

    public EntityPath<T> getTarget() {
        return target;
    }

    public Path<T> getAlias() {
        return alias;
    }

    public JoinType getJoinType() {
        return joinType;
    }
}
