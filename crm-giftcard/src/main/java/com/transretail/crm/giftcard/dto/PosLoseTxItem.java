package com.transretail.crm.giftcard.dto;

import javax.validation.constraints.NotNull;
import org.hibernate.validator.constraints.NotEmpty;
import org.springframework.format.annotation.NumberFormat;

/**
 *
 * @author Monte Cillo Co (mco@exist.com)
 */
public class PosLoseTxItem {

    // readonly
    @NumberFormat
    private Double previousBalance;
    // readonly
    @NumberFormat(pattern="#,##0.00")
    private Double balance;
    @NotEmpty
    private String cardNo;
    @NumberFormat(pattern="#,##0")
    @NotNull
    private Double amount;

    public void setCardNo(String cardNo) {
	this.cardNo = cardNo;
    }

    public void setAmount(Double amount) {
	this.amount = amount;
    }

    public String getCardNo() {
	return cardNo;
    }

    public Double getAmount() {
	return amount;
    }

    public Double getPreviousBalance() {
	return previousBalance;
    }

    public void setPreviousBalance(Double previousBalance) {
	this.previousBalance = previousBalance;
    }

    public Double getBalance() {
	return balance;
    }

    public void setBalance(Double balance) {
	this.balance = balance;
    }
}
