package com.transretail.crm.web.controller;


import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.google.common.collect.Lists;
import com.transretail.crm.core.dto.ProductDto;
import com.transretail.crm.core.dto.ProductSearchDto;
import com.transretail.crm.core.service.CodePropertiesService;
import com.transretail.crm.core.service.ProductHierService;
import com.transretail.crm.core.service.ProductService;
import com.transretail.crm.web.controller.util.ControllerResponse;


@RequestMapping("/product/classification")
@Controller
public class ProductClassificationController {

	//private static final String PATH_LIST		  	= "/product/classification/list/";



	@Autowired
	ProductService productService;
	@Autowired
	ProductHierService productHierService;
	@Autowired
	CodePropertiesService codePropertiesService;



	@RequestMapping( value="/list", produces="application/json; charset=utf-8", method=RequestMethod.GET )
	public @ResponseBody ControllerResponse getProductCfn( 
			@RequestParam(value = "type", required = true) String type, @RequestParam(value = "code", required = true) String code ) {

    	ControllerResponse theResp = new ControllerResponse();
		theResp.setResult( productHierService.get( type, code ) );
		theResp.setSuccess( true );
		return theResp;
	}

	@RequestMapping( value="/product/list/{filter}", produces="application/json; charset=utf-8", method=RequestMethod.GET )
	public @ResponseBody List<ProductDto> listCustomers( @PathVariable(value="filter") String filter ) {
    	ProductSearchDto searchDto = new ProductSearchDto();
    	searchDto.setSkuOrDescLikes( Lists.newArrayList( new String[]{ filter } ) );
    	List<ProductDto> dtos = productService.search( searchDto );
		return dtos;
    }
	
	@RequestMapping( value="/product/list/{limit}/{filter}", produces="application/json; charset=utf-8", method=RequestMethod.GET )
	public @ResponseBody List<ProductDto> listProducts(@PathVariable String filter,
			 @PathVariable long limit) {
    	ProductSearchDto searchDto = new ProductSearchDto();
    	searchDto.setSkuOrDescLikes(Lists.newArrayList(new String[]{filter}));
    	List<ProductDto> dtos = productService.search(searchDto, limit);
		return dtos;
    }

}
