$(document).ready( function() {
	var CNAME_CUSTOM_RESET = "custom-reset";
	var CommonUtil = {
		reset       : function( ele, isEmpty ) {
            $( ele ).find(':input').each( function() {
                switch(this.type) {
                    case 'password':
                    case 'select': $(this)[0].selectedIndex = 0; break;
                    case 'select-one': $(this)[0].selectedIndex = 0; break;
                    case 'text':
                    case 'textarea':
                    case 'file': 
                    case 'hidden': $(this).val(''); break;
                    case 'select-multiple': isEmpty? $(this).empty() : $(this).val(""); break;
                    case 'checkbox':
                    case 'radio': this.checked = false;
                }
            });
        }
	};

	initializeReset();

	function initializeReset() {
		$.each( $("." + CNAME_CUSTOM_RESET), function( idx, obj ) {
			$( obj ).click( function(e) {
				var resetForm = $(obj).closest( "form" );
				if ( !resetForm.length ) {
					resetForm = $( "#" + $( obj ).data( "form-id" ) );
				}

				if ( resetForm.length ) {
					CommonUtil.reset( resetForm );
					$( "#" + $( obj ).data( "default-id" ) ).click();
				}
				else {
					alert( "No parent-form found. Please indicate parent container-id." );
				}
			});
		});
	}
});