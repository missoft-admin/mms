package com.transretail.crm.core.dto;

import com.mysema.query.annotations.QueryProjection;

public class DataAnalysisLMHQueryResult {

	private Long memberId;
	private Double count;
	private Double analysisCount;
	@QueryProjection
	public DataAnalysisLMHQueryResult(Long memberId, Double count,
			Double analysisCount) {
		super();
		this.memberId = memberId;
		this.count = count;
		this.analysisCount = analysisCount;
	}
	public Long getMemberId() {
		return memberId;
	}
	public void setMemberId(Long memberId) {
		this.memberId = memberId;
	}
	public Double getCount() {
		return count;
	}
	public void setCount(Double count) {
		this.count = count;
	}
	public Double getAnalysisCount() {
		return analysisCount;
	}
	public void setAnalysisCount(Double analysisCount) {
		this.analysisCount = analysisCount;
	}
	
	
}
