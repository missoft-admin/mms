package com.transretail.crm.web.stamp.controller;


import javax.validation.Valid;

import org.joda.time.LocalDate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.NoSuchMessageException;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import com.google.common.collect.Sets;
import com.transretail.crm.common.service.exception.MessageSourceResolvableException;
import com.transretail.crm.core.dto.StampPromoPortaRedeemRestDto;
import com.transretail.crm.core.dto.StampPromoPortaRedeemRestDto.RedeemedStamps;
import com.transretail.crm.core.dto.StampPromoTxnDto;
import com.transretail.crm.core.entity.MemberModel;
import com.transretail.crm.core.security.service.MD5Service;
import com.transretail.crm.core.service.MemberService;
import com.transretail.crm.core.service.StampPromoService;
import com.transretail.crm.web.giftcard.controller.ErrorResponse;
import com.transretail.crm.web.stamp.controller.dto.StampPromoPreredeemResponse;
import com.transretail.crm.web.stamp.controller.dto.StampPromoRedeemItem;
import com.transretail.crm.web.stamp.controller.dto.StampPromoRedeemRequest;
import com.transretail.crm.web.stamp.controller.dto.StampPromoRedeemResponse;
import com.transretail.crm.web.stamp.controller.dto.StampPromoRequest;
import com.transretail.crm.web.stamp.controller.dto.StampPromoResponse;


@Controller
@RequestMapping(value = "/promotion/stamp", produces = MediaType.APPLICATION_JSON_VALUE)
public class StampPromoRestController {

	@Autowired
	private StampPromoService service;
	@Autowired
	private MemberService memberService;
	@Autowired
	private MD5Service mD5Service;
	@Autowired
	MessageSource messageSource;



	@RequestMapping(value = "/redeem/scheme/{memberId}", method = RequestMethod.GET)
    public @ResponseBody StampPromoResponse preredeem( @PathVariable String memberId ) {
		StampPromoPreredeemResponse resp = new StampPromoPreredeemResponse();
		resp.setMemberId( memberId );
		resp.withRedeemItems( Sets.newHashSet( service.retrieveRedeemableItems( memberId ) ) );
        return resp;
    }

	@RequestMapping(value = "/redeem/item", method = RequestMethod.POST)
    public @ResponseBody StampPromoResponse redeem( @Valid @RequestBody StampPromoRedeemRequest req) throws MessageSourceResolvableException {
		this.validateSignature( req );

		StampPromoRedeemResponse res = new StampPromoRedeemResponse();
		res.setMemberId( req.getMemberId() );
		res.setStoreCode( req.getStoreCode() );

		MemberModel member = memberService.findActiveWithAccountId( req.getMemberId() );
		LocalDate redeemDate = LocalDate.now();
		StampPromoPortaRedeemRestDto portaDto = new StampPromoPortaRedeemRestDto( req.getMemberId(), req.getStoreCode(), 
				req.getTransactionId(), redeemDate, null );
		for ( StampPromoRedeemItem item : req.getRedeemItems() ) {
			StampPromoTxnDto txnDto = service.redeemStamp( member, req.getStoreCode(), req.getTransactionId(), redeemDate, item.getItemQty(), item.getPromoID(), item.getSku() );
			res.addRedeemStamp( txnDto );
			portaDto.getRedeemedStamps().add( new RedeemedStamps( item.getPromoID().toString(), txnDto.getRedeemedStamps(), txnDto.getRemainingStamps() ) );
		}

		service.callPortaRedeemStampPromo( portaDto );

		return res;
    }



    @ResponseBody
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler(Exception.class)
    public ErrorResponse handleMessage(Exception ex) {
        ErrorResponse errorResponse = new ErrorResponse();
        errorResponse.setErrorMessage(ex.getMessage());
        errorResponse.setStatus(HttpStatus.BAD_REQUEST.value());

        return errorResponse;
    }

    @ResponseBody
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler(MessageSourceResolvableException.class)
    public ErrorResponse handleMessage(MessageSourceResolvableException ex) {
        ErrorResponse errorResponse = new ErrorResponse();
        String errorMessage = null;
        try {
            errorMessage = messageSource.getMessage(ex.getLastCode(), ex.getArguments(),
                    LocaleContextHolder.getLocale());
        } catch (NoSuchMessageException e) {
            errorMessage = ex.getDefaultMessage();
        }

        errorResponse.setErrorMessage(errorMessage);
        errorResponse.setStatus(HttpStatus.BAD_REQUEST.value());

        return errorResponse;
    }



    private void validateSignature( StampPromoRequest request ) throws MessageSourceResolvableException {
        if ( !mD5Service.validateMd5( request.getSignature(), request.getValidationKey() ) ) {
            throw new MessageSourceResolvableException( "", new String[]{},  "Failed to perform request. Request validation key does not match!" );
        }
    }

}