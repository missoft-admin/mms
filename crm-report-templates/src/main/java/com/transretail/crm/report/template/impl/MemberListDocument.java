package com.transretail.crm.report.template.impl;

import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Lists;
import com.mysema.query.BooleanBuilder;
import com.mysema.query.jpa.JPQLQuery;
import com.mysema.query.jpa.impl.JPAQuery;
import com.mysema.query.types.Order;
import com.mysema.query.types.OrderSpecifier;
import com.mysema.query.types.Projections;
import com.transretail.crm.common.reporting.jasper.ClasspathFileResolver;
import com.transretail.crm.common.reporting.jasper.DefaultJRProcessor;
import com.transretail.crm.common.reporting.jasper.JRProcessor;
import com.transretail.crm.core.dto.MemberSearchDto;
import com.transretail.crm.core.entity.ApplicationConfig;
import com.transretail.crm.core.entity.QMemberModel;
import com.transretail.crm.core.entity.embeddable.QAddress;
import com.transretail.crm.core.entity.enums.AppKey;
import com.transretail.crm.core.entity.enums.MemberStatus;
import com.transretail.crm.core.entity.enums.ReportType;
import com.transretail.crm.core.entity.lookup.LookupDetail;
import com.transretail.crm.core.entity.lookup.QLookupDetail;
import com.transretail.crm.core.repo.ApplicationConfigRepo;
import com.transretail.crm.core.service.CodePropertiesService;
import com.transretail.crm.core.service.LookupService;
import com.transretail.crm.core.service.StoreService;
import com.transretail.crm.report.template.FilterField;
import org.joda.time.DateTime;
import org.joda.time.LocalDate;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.MessageSourceAware;
import org.springframework.context.support.MessageSourceAccessor;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Set;
/**
 * @author ftopico
 */

@Service("memberListDocument")
public class MemberListDocument implements MessageSourceAware {
    
    @Autowired
    private LookupService lookupService;
    @Autowired
    private StoreService storeService;
    @Autowired
    private ApplicationConfigRepo applicationConfigRepo;
    @Autowired
    private CodePropertiesService codePropertiesService;
    
    @PersistenceContext
    private EntityManager em;

    protected static final String TEMPLATE_NAME = "Member List Document";
    protected static final String REPORT_NAME_EMPLOYEE = "reports/member_employee_list_document.jasper";
    protected static final String REPORT_NAME_INDIVIDUAL = "reports/member_individual_list_document.jasper";
    protected static final String REPORT_NAME_PROFESSIONAL = "reports/member_professional_list_document.jasper";
    //Header Params
    private static final String PARAM_PRINT_DATE = "printDate";
    private static final DateTimeFormatter FULL_DATE_PATTERN = DateTimeFormat.forPattern("MMMM d, yyyy");
    private MessageSourceAccessor messageSource;
    
    public ReportType[] getReportTypes() {
        return ReportType.values();
    }
    
    @Override
    public void setMessageSource(MessageSource messageSource) {
        this.messageSource = new MessageSourceAccessor(messageSource);
    }
    
    public String getName() {
        return TEMPLATE_NAME;
    }
    
    public String getDescription() {
        return messageSource.getMessage("title_document_employee_list");
    }
    
    public Set<FilterField> getFilters() {
        return null;
    }
    
    @SuppressWarnings({ "rawtypes", "unchecked" })
    public JRProcessor createJrProcessor(MemberSearchDto searchDto, Integer segment, String memberType, String status) {
        
        QMemberModel qm = QMemberModel.memberModel;
        QAddress qa = QAddress.address;
        QLookupDetail qlMemberType = QLookupDetail.lookupDetail;
        QLookupDetail qlEmployeeType = new QLookupDetail("qlEmployeeType");
        QLookupDetail qlDepartment = new QLookupDetail("qlDepartment");
        QLookupDetail qlGender = new QLookupDetail("qlGender");
        QLookupDetail qlReligion = new QLookupDetail("qlReligion");
        QLookupDetail qlNationality = new QLookupDetail("qlNationality");
        QLookupDetail qlMaritalStatus = new QLookupDetail("qlMaritalStatus");
        QLookupDetail qlEducation = new QLookupDetail("qlEducation");
        QLookupDetail qlCustomerGroup = new QLookupDetail("qlCustomerGroup");
        QAddress qba = QAddress.address;
        
        ApplicationConfig rowLimit = applicationConfigRepo.findByKey( AppKey.MEMBER_EXPORT_LIST_ROW_LIMIT );
        Long rowLimitValue = null;
        
        if (rowLimit.getValue() == null)
            rowLimitValue = 5000L;
        else
            rowLimitValue = Long.parseLong(rowLimit.getValue());
        
        BooleanBuilder exp = new BooleanBuilder(searchDto.createSearchExpression());
        if (memberType != null)
            exp.and(qlMemberType.code.eq(memberType));
        if (status != null)
            exp.and(qm.accountStatus.eq(MemberStatus.valueOf(status)));
        
        JPQLQuery query = new JPAQuery(em);
        query = query.from(qm)
                .leftJoin(qm.memberType, qlMemberType)
                .leftJoin(qm.address, qa)
                .leftJoin(qm.empType, qlEmployeeType)
                .leftJoin(qm.professionalProfile.department, qlDepartment)
                .leftJoin(qm.customerProfile.gender, qlGender)
                .leftJoin(qm.customerProfile.religion, qlReligion)
                .leftJoin(qm.customerProfile.nationality, qlNationality)
                .leftJoin(qm.customerProfile.maritalStatus, qlMaritalStatus)
                .leftJoin(qm.customerProfile.education, qlEducation)
                .leftJoin(qm.professionalProfile.customerGroup, qlCustomerGroup)
                .leftJoin(qm.professionalProfile.businessAddress, qba)
                .where(exp.getValue())
                .orderBy(new OrderSpecifier(Order.ASC, qm.created))
                .offset((segment-1)*rowLimitValue).limit(rowLimitValue);

    	List<ReportBean> beansDataSource = Lists.newArrayList();
    	List<MemberEmployee> memberList = query.list(Projections.fields(MemberEmployee.class,
                                	        qm.accountId.as("accountNumber"),
                                	        qm.firstName.as("firstName"),
                                	        qm.lastName.as("lastName"),
                                	        qm.terminationDate.as("terminationDate"),
                                	        qlEmployeeType.description.as("employeeType"),
                                	        qlDepartment.description.as("department"),
                                	        qm.registeredStore.as("registeredStore"),
                                	        qm.customerProfile.birthdate.as("dateOfBirth"),
                                	        qm.customerProfile.placeOfBirth.as("placeOfBirth"),
                                	        qlGender.description.as("gender"),
                                	        qlReligion.description.as("religion"),
                                	        qlNationality.description.as("nationality"),
                                	        qm.contact.as("mobilePhone"),
                                	        qa.street.as("street"),
                                	        qa.city.as("city"),
                                	        qa.postCode.as("postCode"),
                                	        qm.email.as("email"),
                                	        qlMaritalStatus.description.as("maritalStatus"),
                                	        qm.idNumber.as("ktpId"),
                                            qm.accountStatus.stringValue().as("status"),
                                            qm.contact.as("contactNumber"),
                                            qm.idNumber.as("idNumber"),
                                            qm.professionalProfile.businessName.as("businessName"),
                                            qm.professionalProfile.businessLicense.as("businessLicense"),
                                        	qm.npwp.npwpId.as("npwpId"),
                                        	qm.npwp.npwpName.as("npwpName"),
                                        	qm.npwp.npwpAddress.as("npwpAddress"),
                                        	qm.professionalProfile.position.as("position"),
                                            qm.bestTimeToCall.as("bestTimeToCall"),
                                            qm.customerProfile.children.stringValue().as("numberOfChildren"),
                                            qlEducation.description.as("education"),
                                            qm.professionalProfile.businessPhone.as("businessPhone"),
                                            qlCustomerGroup.description.as("customerGroup"),
                                            qba.street.append(", ").append(qba.city).as("businessAddress"),
                                            qba.postCode.as("businessPostCode"),
                                            qm.created.as("registrationDate")
                                        ));
    	    
    	Map<String, Object> parameters = ImmutableMap.<String, Object>builder()
    	        .put(PARAM_PRINT_DATE, FULL_DATE_PATTERN.print(new LocalDate())).build();
    	
    	Map<String, String> stores = storeService.getAllStoresHashMap();
    	LookupDetail headOffice = lookupService.getDetailByCode(codePropertiesService.getDetailInvLocationHeadOffice());
    	
    	for (MemberEmployee me : memberList) {
    	    if (me.getRegisteredStore() != null && !me.getRegisteredStore().equalsIgnoreCase("")) {
    	        if (me.getRegisteredStore().equalsIgnoreCase(headOffice.getCode())) {
    	            me.setRegisteredStoreName(headOffice.getDescription());
    	        } else if (stores.containsKey(me.getRegisteredStore())) {
    	            me.setRegisteredStoreName(stores.get(me.getRegisteredStore()));
    	        }
    	    }
    	}
    	
    	beansDataSource.add(new ReportBean(memberList));
    	
    	String reportType = null;
    	
        if (memberType.equalsIgnoreCase("MTYP001")) {
            reportType = REPORT_NAME_INDIVIDUAL;
        } else if (memberType.equalsIgnoreCase("MTYP002")) {
            reportType = REPORT_NAME_EMPLOYEE;
        } else if (memberType.equalsIgnoreCase("MTYP003")) {
            reportType = REPORT_NAME_PROFESSIONAL;
        }
    	
    	DefaultJRProcessor jrProcessor = new DefaultJRProcessor(reportType, beansDataSource);
    	jrProcessor.addParameters(parameters);
    	jrProcessor.setFileResolver(new ClasspathFileResolver("reports"));
    	
    	return jrProcessor;
    }
    
    public static class MemberEmployee {
        
        //Member Employee
        String accountNumber;
        String firstName;
        String lastName;
        LocalDate terminationDate;
        String employeeType;
        String department;
        String registeredStore;
        String registeredStoreName;
        Date dateOfBirth;
        String placeOfBirth;
        String gender;
        String religion;
        String nationality;
        String mobilePhone;
        String street;
        String city;
        String postCode;
        String email;
        String maritalStatus;
        String ktpId;
        
        //Individual
        String status;
        String contactNumber;
        String idNumber;
        
        //Professional
        String businessName;
        String businessLicense;
        String npwpId;
        String npwpName;
        String npwpAddress;
        String position;
        String bestTimeToCall;
        String numberOfChildren;
        String education;
        String businessPhone;
        String customerGroup;
        String businessAddress;
        String businessPostCode;
        DateTime registrationDate;

        public MemberEmployee() {
            
        }

        public String getAccountNumber() {
            return accountNumber;
        }

        public String getFirstName() {
            return firstName;
        }

        public String getLastName() {
            return lastName;
        }

        public LocalDate getTerminationDate() {
            return terminationDate;
        }

        public String getEmployeeType() {
            return employeeType;
        }

        public String getDepartment() {
            return department;
        }

        public String getRegisteredStore() {
            return registeredStore;
        }

        public String getRegisteredStoreName() {
            return registeredStoreName;
        }

        public String getPlaceOfBirth() {
            return placeOfBirth;
        }

        public String getGender() {
            return gender;
        }

        public String getReligion() {
            return religion;
        }

        public String getNationality() {
            return nationality;
        }

        public String getMobilePhone() {
            return mobilePhone;
        }

        public String getStreet() {
            return street;
        }

        public String getCity() {
            return city;
        }

        public String getPostCode() {
            return postCode;
        }

        public String getEmail() {
            return email;
        }

        public String getMaritalStatus() {
            return maritalStatus;
        }

        public String getKtpId() {
            if (ktpId != null && !ktpId.equalsIgnoreCase("")) {
                if (ktpId.substring(0, 3).equalsIgnoreCase("ktp")) {
                    return ktpId.substring(4);
                }
            } 
            return null;
        }

        public void setAccountNumber(String accountNumber) {
            this.accountNumber = accountNumber;
        }

        public void setFirstName(String firstName) {
            this.firstName = firstName;
        }

        public void setLastName(String lastName) {
            this.lastName = lastName;
        }

        public void setTerminationDate(LocalDate terminationDate) {
            this.terminationDate = terminationDate;
        }

        public void setEmployeeType(String employeeType) {
            this.employeeType = employeeType;
        }

        public void setDepartment(String department) {
            this.department = department;
        }

        public void setRegisteredStore(String registeredStore) {
            this.registeredStore = registeredStore;
        }

        public void setRegisteredStoreName(String registeredStoreName) {
            this.registeredStoreName = registeredStoreName;
        }

        public Date getDateOfBirth() {
            return dateOfBirth;
        }

        public void setDateOfBirth(Date dateOfBirth) {
            this.dateOfBirth = dateOfBirth;
        }

        public void setPlaceOfBirth(String placeOfBirth) {
            this.placeOfBirth = placeOfBirth;
        }

        public void setGender(String gender) {
            this.gender = gender;
        }

        public void setReligion(String religion) {
            this.religion = religion;
        }

        public void setNationality(String nationality) {
            this.nationality = nationality;
        }

        public void setMobilePhone(String mobilePhone) {
            this.mobilePhone = mobilePhone;
        }

        public void setStreet(String street) {
            this.street = street;
        }

        public void setCity(String city) {
            this.city = city;
        }

        public void setPostCode(String postCode) {
            this.postCode = postCode;
        }

        public void setEmail(String email) {
            this.email = email;
        }

        public void setMaritalStatus(String maritalStatus) {
            this.maritalStatus = maritalStatus;
        }

        public void setKtpId(String ktpId) {
            this.ktpId = ktpId;
        }

        public String getStatus() {
            return status;
        }

        public String getContactNumber() {
            return contactNumber;
        }

        public String getIdNumber() {
            return idNumber;
        }

        public void setStatus(String status) {
            this.status = status;
        }

        public void setContactNumber(String contactNumber) {
            this.contactNumber = contactNumber;
        }

        public void setIdNumber(String idNumber) {
            this.idNumber = idNumber;
        }

        public String getBusinessName() {
            return businessName;
        }

        public String getBusinessLicense() {
            return businessLicense;
        }

        public String getNpwpId() {
            return npwpId;
        }

        public String getNpwpName() {
            return npwpName;
        }

        public String getNpwpAddress() {
            return npwpAddress;
        }

        public String getPosition() {
            return position;
        }

        public String getBestTimeToCall() {
            return bestTimeToCall;
        }

        public String getNumberOfChildren() {
            return numberOfChildren;
        }

        public String getEducation() {
            return education;
        }

        public String getBusinessPhone() {
            return businessPhone;
        }

        public void setBusinessName(String businessName) {
            this.businessName = businessName;
        }

        public void setBusinessLicense(String businessLicense) {
            this.businessLicense = businessLicense;
        }

        public void setNpwpId(String npwpId) {
            this.npwpId = npwpId;
        }

        public void setNpwpName(String npwpName) {
            this.npwpName = npwpName;
        }

        public void setNpwpAddress(String npwpAddress) {
            this.npwpAddress = npwpAddress;
        }

        public void setPosition(String position) {
            this.position = position;
        }

        public void setBestTimeToCall(String bestTimeToCall) {
            this.bestTimeToCall = bestTimeToCall;
        }

        public void setNumberOfChildren(String numberOfChildren) {
            this.numberOfChildren = numberOfChildren;
        }

        public void setEducation(String education) {
            this.education = education;
        }

        public void setBusinessPhone(String businessPhone) {
            this.businessPhone = businessPhone;
        }

        public String getCustomerGroup() {
            return customerGroup;
        }

        public void setCustomerGroup(String customerGroup) {
            this.customerGroup = customerGroup;
        }

        public String getBusinessAddress() {
            return businessAddress;
        }

        public void setBusinessAddress(String businessAddress) {
            this.businessAddress = businessAddress;
        }

        public String getBusinessPostCode() {
            return businessPostCode;
        }

        public void setBusinessPostCode(String businessPostCode) {
            this.businessPostCode = businessPostCode;
        }

        public DateTime getRegistrationDate() {
            return registrationDate;
        }

        public void setRegistrationDate(DateTime registrationDate) {
            this.registrationDate = registrationDate;
        }
    }
    
    public static class ReportBean {
        
        private List<MemberEmployee> memberList;
        
        public ReportBean() {}
        
        public ReportBean(List<MemberEmployee> memberList) {
            super();
            this.memberList = memberList;
        }

        public List<MemberEmployee> getMemberList() {
            return memberList;
        }

        public void setMemberList(List<MemberEmployee> memberList) {
            this.memberList = memberList;
        }

    }
    
}
