    drop table CRM_SUSPENDED_FROM_POINTS;
    drop table CRM_POINTS_CONFIG;

    create table CRM_SUSPENDED_FROM_POINTS (
        id number(19,0) not null,
        CREATED_BY varchar2(255 char),
        CREATED_DATETIME timestamp,
        LAST_UPDATED_BY varchar2(255 char),
        LAST_UPDATED_DATETIME timestamp,
        POINTS_CONFIG number(19,0) not null,
        MEMBER_TYPE varchar2(15 char) not null,
        primary key (id)
    );

    create table CRM_POINTS_CONFIG (
        id number(19,0) not null,
        CREATED_BY varchar2(255 char),
        CREATED_DATETIME timestamp,
        LAST_UPDATED_BY varchar2(255 char),
        LAST_UPDATED_DATETIME timestamp,
        POINT_VALUE double precision,
        TRANSACTION_COUNT number(10,0),
        primary key (id)
    );

    alter table CRM_SUSPENDED_FROM_POINTS
        add constraint FK_qx927w0xb2iv4lxp8xkrf17yy
        foreign key (POINTS_CONFIG)
        references CRM_POINTS_CONFIG;

    alter table CRM_SUSPENDED_FROM_POINTS
        add constraint FK_91u9lac393dd25yj11ro6kd5d
        foreign key (MEMBER_TYPE)
        references CRM_REF_LOOKUP_DTL;

    INSERT INTO CRM_POINTS_CONFIG(ID, POINT_VALUE) VALUES (1, 1.0);