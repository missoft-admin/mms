alter table CRM_GC_ORD add column IS_EGC char(1);
alter table CRM_GC_PROD_PROFILE add column IS_EGC char(1);
alter table CRM_GC_ORD alter column VENDOR bigint null;

alter table CRM_GC_INVENTORY add (
        IS_EGC char(1),
        IS_EMAIL_SENT char(1),
        MOBILE_NO varchar(50),
        EMAIL varchar(50),
        PIN varchar(20)
);