package com.transretail.crm.giftcard.repo;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.transretail.crm.common.repo.CrmQueryDslPredicateExecutor;
import com.transretail.crm.giftcard.entity.ProductProfile;

/**
 *
 */
@Repository
public interface ProductProfileRepo extends CrmQueryDslPredicateExecutor<ProductProfile, Long> {

    List<ProductProfile> findByProductCode(String productCode);
}
