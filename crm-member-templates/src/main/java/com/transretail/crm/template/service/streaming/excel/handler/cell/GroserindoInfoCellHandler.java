package com.transretail.crm.template.service.streaming.excel.handler.cell;

import org.apache.commons.lang3.StringUtils;

import com.transretail.crm.core.entity.MemberModel;
import com.transretail.crm.core.entity.embeddable.MarketingDetails;
import com.transretail.crm.core.entity.lookup.LookupDetail;
import com.transretail.crm.template.service.MemberTemplateParseException;
import com.transretail.crm.template.service.streaming.excel.handler.sheet.AbstractSheetHandler;

/**
 * @author Allan G. Ramirez (agramirez@exist.com)
 */
public class GroserindoInfoCellHandler implements CellHandler {
    private AbstractSheetHandler sheetHandler;

    public GroserindoInfoCellHandler(AbstractSheetHandler sheetHandler) {
        this.sheetHandler = sheetHandler;
    }

    @Override
    public void handle(MemberModel dto, String cellText) throws MemberTemplateParseException {
        MarketingDetails marketingDetails = dto.getMarketingDetails();
        if (dto.getMarketingDetails() == null) {
            marketingDetails = new MarketingDetails();
            dto.setMarketingDetails(marketingDetails);
        }

        if (StringUtils.isNotBlank(cellText)) {
            LookupDetail detail =
                sheetHandler.getLoookupDetail(sheetHandler.getCodePropertiesService().getHeaderInformedAbout(), cellText);
            marketingDetails.setInformedAbout(detail.getCode());
        }
    }
}
