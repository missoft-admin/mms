package com.transretail.crm.web.controller;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.collect.Lists;
import com.transretail.crm.common.reporting.service.ReportService;
import com.transretail.crm.common.service.dto.request.PageSortDto;
import com.transretail.crm.common.service.dto.request.PagingParam;
import com.transretail.crm.common.service.dto.response.ResultList;
import com.transretail.crm.core.entity.lookup.Store;
import com.transretail.crm.core.service.StoreService;
import com.transretail.crm.giftcard.GiftCardNotFoundException;
import com.transretail.crm.giftcard.dto.GiftCardInfoDto;
import com.transretail.crm.giftcard.dto.GiftCardServiceRequestDto;
import com.transretail.crm.giftcard.entity.GiftCardInventory;
import com.transretail.crm.giftcard.entity.GiftCardOrder;
import com.transretail.crm.giftcard.entity.support.GiftCardInventoryStatus;
import com.transretail.crm.giftcard.repo.ProductProfileRepo;
import com.transretail.crm.giftcard.service.GiftCardInventoryService;
import com.transretail.crm.giftcard.service.GiftCardServiceRequestService;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.ArrayList;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.MediaType;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.AnnotationConfigWebContextLoader;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

import static org.hamcrest.Matchers.*;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 *
 * @author Monte Cillo Co (mco@exist.com)
 */
@RunWith(SpringJUnit4ClassRunner.class)
@WebAppConfiguration
@ContextConfiguration(loader = AnnotationConfigWebContextLoader.class)
public class GiftCardServiceRequestControllerTest extends AbstractMockMvcTest {

    private static final String CARD_NO = "14052500100000026193";
    @Autowired
    private GiftCardInventoryService inventoryService;
    @Autowired
    private GiftCardServiceRequestService requestService;
    @Autowired
    private MessageSource messageSource;
    private GiftCardInventory gc;

    @Before
    public void setup() {
	Mockito.reset(inventoryService, requestService, messageSource);

	gc = new GiftCardInventory();
	gc.setBarcode(CARD_NO);
	gc.setFaceValue(new BigDecimal(100000));
	gc.setOrder(new GiftCardOrder(12345l));
	gc.setProductCode("TNF-AP120");
	gc.setRefundable(Boolean.TRUE);
	gc.setLocation("INVT001"); // HEAD OFFICE
	gc.setStatus(GiftCardInventoryStatus.ACTIVATED);
    }

    @Test
    public void shouldSuccessfullyRetrieveAGiftCard() throws Exception {
	when(requestService.retrieveGiftCardInfoByCardNo(CARD_NO))
		.thenReturn(new GiftCardInfoDto().cardNo(CARD_NO)
			.faceValue(100000.00).orderNo("BBM1231"));

	mockMvc.perform(get("/giftcard/service-request/find/{cardNo}", CARD_NO))
		.andExpect(status().isOk())
		.andExpect(content().contentType(MediaType.APPLICATION_JSON))
		.andDo(print())
		.andExpect(jsonPath("$.success", is(true)))
		.andExpect(jsonPath("$.result.cardNo", is(equalTo(CARD_NO))))
		.andExpect(jsonPath("$.result.faceValue", is(equalTo("100000"))))
		.andExpect(jsonPath("$.result.orderNo", is(equalTo("BBM1231"))));

	verify(requestService).retrieveGiftCardInfoByCardNo(CARD_NO);
    }

    @Test
    public void shouldFailedToRetrieveAGiftCard() throws Exception {
	when(requestService.retrieveGiftCardInfoByCardNo(CARD_NO))
		.thenThrow(new GiftCardNotFoundException(CARD_NO));

	mockMvc.perform(get("/giftcard/service-request/find/{cardNo}", CARD_NO))
		.andExpect(status().isOk())
		.andExpect(content().contentType(MediaType.APPLICATION_JSON))
		.andDo(print())
		.andExpect(jsonPath("$.success", is(false)))
		.andExpect(jsonPath("$.message", is(not(empty()))));

	verify(requestService).retrieveGiftCardInfoByCardNo(CARD_NO);
    }

    @Test
    public void shouldSuccessfullyFileANewServiceRequest() throws Exception {
	String details = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. "
		+ "Aenean eu libero ut nisi feugiat mattis accumsan et nisl. "
		+ "Pellentesque eget blandit lacus, non rhoncus leo. "
		+ "Etiam nec dolor tellus. Vivamus tincidunt elit id lorem venenatis, "
		+ "at tincidunt ante semper. Proin euismod nunc eu tempor interdum. "
		+ "Proin aliquet massa justo, eget vestibulum velit rutrum et.";

	when(inventoryService.getGiftCardByBarcode(CARD_NO)).thenReturn(gc);
	mockMvc.perform(post("/giftcard/service-request/file", new Object())
		.param("giftcardNo", CARD_NO)
		.param("details", details))
		.andExpect(status().isOk());

	verify(inventoryService).getGiftCardByBarcode(CARD_NO);
	verify(requestService).fileNewServiceRequest(gc, Mockito.any(Store.class), details);
	verifyNoMoreInteractions(inventoryService, requestService);
    }

    @Test
    public void shouldFailToFileANewServiceRequest() throws Exception {
	String details = "";

	when(inventoryService.getGiftCardByBarcode(CARD_NO)).thenReturn(gc);
	mockMvc.perform(post("/giftcard/service-request/file", new Object())
		.param("giftcardNo", CARD_NO)
		.param("details", details))
		.andDo(print())
		.andExpect(status().isOk())
		.andExpect(jsonPath("$.result", hasSize(1)));

	verifyZeroInteractions(inventoryService, requestService);
    }

    @Test
    public void shouldSuccessfullyRetrieveAllServiceRequestOfGiftCard() throws Exception {
	PagingParam pagingParam = new PagingParam();
	pagingParam.setPageNo(1);
	pagingParam.setPageSize(10);

	PageSortDto pageSortDto = new PageSortDto();
	pageSortDto.setPagination(pagingParam);
	final GiftCardServiceRequestDto gc1 = new GiftCardServiceRequestDto();
	gc1.setGiftcardNo(CARD_NO);
	gc1.setFiledBy("JUNIT");
	final GiftCardServiceRequestDto gc2 = new GiftCardServiceRequestDto();
	gc2.setGiftcardNo(CARD_NO);
	gc2.setFiledBy("JUNIT");
	final ArrayList<GiftCardServiceRequestDto> list
		= Lists.newArrayList(gc1, gc2);

	ResultList<GiftCardServiceRequestDto> resultList
		= new ResultList<GiftCardServiceRequestDto>(list, 2, false, false);

	when(requestService.findAllServiceRequestOf(anyString(), Mockito.any(PageSortDto.class)))
		.thenReturn(resultList);

	mockMvc.perform(post("/giftcard/service-request/list/{cardNo}", CARD_NO)
		.contentType(MediaType.APPLICATION_JSON)
		.content(convertObjectToJsonBytes(pageSortDto)))
		.andDo(print())
		.andExpect(status().isOk())
		.andExpect(jsonPath("$.results", hasSize(2)))
		.andExpect(jsonPath("$.numberOfElements", is(2)))
		.andExpect(jsonPath("$.totalElements", is(2)));

	verify(requestService).findAllServiceRequestOf(anyString(), Mockito.any(PageSortDto.class));
    }

    private static byte[] convertObjectToJsonBytes(Object object) throws IOException {
	ObjectMapper mapper = new ObjectMapper();
	mapper.setSerializationInclusion(JsonInclude.Include.NON_NULL);
	return mapper.writeValueAsBytes(object);
    }

    @Configuration
    @EnableWebMvc
    static class Config {

	//<editor-fold defaultstate="collapsed" desc="Bean Definition">
	@Bean
	public ProductProfileRepo productProfileRepo() {
	    return mock(ProductProfileRepo.class);
	}

	@Bean
	public ReportService reportService() {
	    return mock(ReportService.class);
	}

	@Bean
	public MessageSource messageSource() {
	    return mock(MessageSource.class);
	}

	@Bean
	public GiftCardServiceRequestController giftCardServiceRequestController() {
	    return new GiftCardServiceRequestController();
	}

	@Bean
	public GiftCardServiceRequestService giftCardServiceRequestService() {
	    return mock(GiftCardServiceRequestService.class);
	}

	@Bean
	public GiftCardInventoryService giftCardInventoryService() {
	    return mock(GiftCardInventoryService.class);
	}

	@Bean
	public StoreService storeService() {
	    return mock(StoreService.class);
	}
	//</editor-fold>
    }
}
