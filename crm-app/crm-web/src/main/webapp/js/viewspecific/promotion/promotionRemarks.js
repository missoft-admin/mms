$(document).ready( function() {

	//var theErrorContainer = $( "#content_programForm" ).find( "#content_error");

	var $theRemarksDialog = $( "#content_promotionRemarks" );
	var $theRemarksForm = $( "#approvalRemarkForm" );
	var $theStatus = $( "#status" );



	initFormFields();
	initDialog();



	function initFormFields() {

		var $thePromotionStatuses = $( ".promotionStatus" );
		$.each( $thePromotionStatuses, function( idx, thePromotionStatus ) {
			$( this ).click( function(e) {
				$theStatus.val( $(this).data( "value" ) );
			});
		});

		$theRemarksForm.submit( function(e) {
			e.preventDefault();
			saveRemarksPost( $(this).attr("action") );
		});
	}

	function initDialog() {

		$theRemarksDialog.modal({});
	}



	function saveRemarksPost( inUrl ) {
		$.ajax({
			cache 		: false, 
			dataType 	: "json",
			type 		: "POST", 
			url 		: inUrl,
			data		: $theRemarksForm.serialize(),
			error 		: function(jqXHR, textStatus, errorThrown) {},
			success : function( inResponse ) {
				if ( inResponse.success ) {
					$theRemarksDialog.modal( "hide" );
					$theRemarksDialog.remove();
					$( "#status" + inResponse.result.modelId ).html( inResponse.result.dispStatus );
				}
			}
		});
	}

});