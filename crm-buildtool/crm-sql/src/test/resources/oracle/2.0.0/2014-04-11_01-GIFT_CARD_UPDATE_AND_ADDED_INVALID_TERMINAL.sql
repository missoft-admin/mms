-- UPDATE CRM_GC_INVENTORY
ALTER TABLE CRM_GC_INVENTORY ADD BALANCE FLOAT;
ALTER TABLE CRM_GC_INVENTORY ADD ACTIVATION_DATE TIMESTAMP;

 create table CRM_INVALID_TERMINAL (
        TERMINAL_ID varchar2(255 char) not null,
        STORE_CODE varchar2(255 char),
        primary key (TERMINAL_ID)
    );

   create table CRM_GC_POS_JOURNAL (
        ID number(19,0) not null,
        GC_BARCODE varchar2(255 char),
        STATUS varchar2(255 char),
        STORE_CODE varchar2(255 char),
        TERMINAL_ID varchar2(255 char),
        TX_DATE_TIME timestamp,
        primary key (ID)
    );