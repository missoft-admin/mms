package com.transretail.crm.giftcard.entity;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.transretail.crm.core.entity.support.CustomAuditableEntity;

/**
 * @author Allan G. Ramirez (agramirez@exist.com)
 */
@Entity
@Table(name = "CRM_PREPAID_RELOAD_INFO")
public class PrepaidReloadInfo extends CustomAuditableEntity<Long> implements Serializable {
    private static final long serialVersionUID = 6723733543931796732L;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "PRODUCT")
    private ProductProfile productProfile;

    private Long reloadAmount;

    /**
     * Effectivity month, 1 as January and 12 as December
     */
    private int monthEffective;

    public ProductProfile getProductProfile() {
        return productProfile;
    }

    public void setProductProfile(ProductProfile productProfile) {
        this.productProfile = productProfile;
    }

    public Long getReloadAmount() {
        return reloadAmount;
    }

    public void setReloadAmount(Long reloadAmount) {
        this.reloadAmount = reloadAmount;
    }

    public int getMonthEffective() {
        return monthEffective;
    }

    public void setMonthEffective(int monthEffective) {
        this.monthEffective = monthEffective;
    }
}
