package com.transretail.crm.core.dto;

import com.transretail.crm.common.service.dto.response.AbstractResultListDTO;
import com.transretail.crm.core.entity.lookup.LookupDetail;

import java.util.Collection;

import org.springframework.data.domain.Page;

public class LookupDetailResultList extends AbstractResultListDTO<LookupDetail> {
	public LookupDetailResultList(Page<LookupDetail> page) {
		super(page);
	}

	public LookupDetailResultList(Collection<LookupDetail> collection) {
		super(collection);
	}
}
