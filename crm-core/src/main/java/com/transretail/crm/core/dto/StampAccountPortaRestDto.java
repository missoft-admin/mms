package com.transretail.crm.core.dto;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.apache.commons.lang3.StringUtils;

public class StampAccountPortaRestDto {

    private static final String MD5_DATEFORMAT = "yyyymmDD";

    private String oldId;
    private String newId;
    private Date transactionDate;
    private String md5ValidationKey;

    public StampAccountPortaRestDto() {}

    public StampAccountPortaRestDto( String oldId, String newId, Date transactionDate, String dateFormat ) {
        this.oldId = oldId;
        this.newId = newId;
        this.transactionDate = transactionDate;

        DateFormat df = new SimpleDateFormat( StringUtils.isNotBlank(dateFormat)? dateFormat : MD5_DATEFORMAT );
        this.md5ValidationKey = oldId + newId + df.format( transactionDate ); //<old ID><new ID><transactDateYYYYMMDD>
    }

    public String getOldId() {
        return oldId;
    }

    public void setOldId(String oldId) {
        this.oldId = oldId;
    }

    public String getNewId() {
        return newId;
    }

    public void setNewId(String newId) {
        this.newId = newId;
    }

    public Date getTransactionDate() {
        return transactionDate;
    }

    public void setTransactionDate(Date transactionDate) {
        this.transactionDate = transactionDate;
    }

    public String getMd5ValidationKey() {
        return md5ValidationKey;
    }

    public void setMd5ValidationKey(String md5ValidationKey) {
        this.md5ValidationKey = md5ValidationKey;
    }
}
