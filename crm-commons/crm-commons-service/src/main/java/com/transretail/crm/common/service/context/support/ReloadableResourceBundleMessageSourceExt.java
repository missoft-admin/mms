package com.transretail.crm.common.service.context.support;

import java.util.Locale;

import org.apache.commons.lang3.text.WordUtils;
import org.springframework.context.support.ReloadableResourceBundleMessageSource;
import org.springframework.util.ObjectUtils;

/**
 *
 */
public class ReloadableResourceBundleMessageSourceExt extends ReloadableResourceBundleMessageSource {
    /**
     * Pattern used to split camel case words.
     * Eg.
     * camelCase will be splitted into ["camel", "Case"]
     * CamelCase will be splitted into ["Camel", "Case"]
     * jamesBond007 will be splitted into ["james", "Bond", "007]
     */
    private static final String CAMELCASE_PATTERN =
        "(?<!(^|[A-Z0-9]))(?=[A-Z0-9])|(?<!(^|[^A-Z]))(?=[0-9])|(?<!(^|[^0-9]))(?=[A-Za-z])|(?<!^)(?=[A-Z][a-z])";

    /**
     * Custom render the defaultMessage if alwaysUseMessageFormat is false and args is empty by doing the following:
     * <ul>
     * <li>If defaultMessage is in camel case, it'll be splitted, then the first word is capitalize and  the reset is converted to
     * lower case.
     * </li>
     * <li>Else, defaultMessage will be just be capitalize</li>
     * </ul>
     *
     * @param defaultMessage string to render
     * @param args           arguments to check
     * @param locale         locale
     * @return rendered message
     */
    @Override
    public String renderDefaultMessage(String defaultMessage, Object[] args, Locale locale) {
        if (defaultMessage != null && !isAlwaysUseMessageFormat() && ObjectUtils.isEmpty(args)) {
            StringBuilder result = new StringBuilder();
            String[] words = defaultMessage.split(CAMELCASE_PATTERN);
            result.append(WordUtils.capitalize(words[0]));
            for (int i = 1; i < words.length; i++) {
                result.append(" ");
                result.append(words[i].toLowerCase());
            }
            return result.toString();
        }
        return super.renderDefaultMessage(defaultMessage, args, locale);
    }
}
