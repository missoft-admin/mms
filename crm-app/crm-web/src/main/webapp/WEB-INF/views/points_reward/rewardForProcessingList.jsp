<%@ include file="../common/taglibs.jsp" %>


<div id="pointsRewardForProcessList">

	<spring:message code="role_hr" var="role_hr"/>
	<spring:message code="role_hrs" var="role_hrs"/>
	<c:set var="datePattern_1" value="yyyy-MM-dd HH:mm:ss"/>
	<c:set var="datePattern_2" value="yyyy-MM-dd"/>
	<spring:message code="label_delete" var="label_delete"/>
    
  <div class="clearfix"></div>

  <div id="content_rewardsList">
    <div id="list_rewards" class="data-table-01" data-url="<c:url value="/pointsreward/forprocessing/list" />"></div>
  </div>

  <div class="clearfix"></div>
  
  <div id="approverBtns">
		<sec:authorize ifAnyGranted="${role_hrs}">
		<div class="contentButton">
      <spring:message var="label_approve" code="label_approve"/>
		  <input id="rewardApproveButton" type="button" value="${label_approve}" class="btn" data-msg-confirm=""
		    onclick="javascript: approveRewards( '${pageContext.request.contextPath}', '<spring:message code="points_err_selecttoapprove" />' );"/>
      <spring:message var="label_reject" code="label_reject"/>
			<input id="rewardRejectButton" type="button" value="${label_reject}" class="btn" data-msg-confirm="" 
			  onclick="javascript: rejectRewards( '${pageContext.request.contextPath}', '<spring:message code="points_err_selecttoreject" />' );"/>
			<spring:message code="label_reset_rejected" var="label_reset"/>
			<input id="rewardResetButton" type="button" value="${label_reset}" class="btn" data-msg-confirm=""
				onclick="javascript: resetRewards( '${pageContext.request.contextPath}', '<spring:message code="points_err_selecttoreset" />' );"/>
		  <jsp:include page="rewardRejectDialog.jsp" />
          <spring:message var="label_approve_all" code="label_approve_all"/>
            <input id="rewardAppoveAllButton" type="button" value="${label_approve_all}" class="btn"
                   onclick="javascript: approveAllRewards('${pageContext.request.contextPath}');"/>
		</div>
		</sec:authorize>
  </div>
  <div id="processBtns" class="hide">
    <a class="btn btn-default tiptip icn-delete deleteBtn" title="${label_delete}" href="<c:url value="/pointsreward/delete/%ID%" />" 
      data-msg-confirm="<spring:message code="entity_delete_confirm" />" >${label_delete}</a>
  </div>
  <jsp:include page="../common/confirm.jsp" />


  
<link href="<spring:url value="/css/dataTables.css" />" rel="stylesheet">
<script src="<spring:url value="/js/jquery/jquery.dataTables.js" />" ></script>
<script src="<spring:url value="/js/jquery/jquery.dataTables.linkLengthMenu.js" />"></script>
<script src="<spring:url value="/js/jquery/jquery.dataTables.customPaging.js" />"></script>
<script src="<spring:url value="/js/jquery/ajax-datatable.js" />" ></script>
<script src="<spring:url value="/js/common/form2js.js" />" ></script>
<script src="<spring:url value="/js/common/json2.js" />" ></script>
<script src="<spring:url value="/js/jquery/jquery.toObject.js" />" ></script>
  
<script type="text/javascript">
var PointRewardsList = null;

$(document).ready( function() {

	  var $list = $("#list_rewards");
	  var $btns = $( "#processBtns" );
	  var $theSearchForm = $( "#form_SearchForm" );
	  var $theSearchBtn = $( "#searchButton" );
	
	  initDataTable();
	  initFormFields();

	  function initDataTable() {
		    var headers = [
            "<spring:message code="label_com_transretail_crm_entity_employeemodel"/>",
            "<spring:message code="points_prop_regstore"/>",
            "<spring:message code="points_prop_department"/>",
            "<spring:message code="points_prop_txntype"/>",
            /* "<spring:message code="points_prop_txnno"/>", */
            "<spring:message code="points_prop_totalpurchasetxnamount"/>",
            "<spring:message code="points_prop_rewardpoints"/>",
            /* "<spring:message code="points_prop_comment"/>", */
            "<spring:message code="points_prop_status"/>",
            "<spring:message code="points_prop_created"/>",
            /* "<spring:message code="points_prop_approvalreason"/>", */
            ""
            ];

		    var modelFields = [
            {name : 'memberModel.accountId', sortable : true, fieldCellRenderer : function (data, type, row) {
               return row.memberModel.accountId + " - " + row.memberModel.formattedMemberName;
            }},
            {name : 'memberModel.registeredStore', sortable : true, fieldCellRenderer : function (data, type, row) {
                return row.memberModel.registeredStore + " - " + row.memberModel.registeredStoreName;
            }},
	    {name : 'memberModel.professionalProfile', sortable : true, fieldCellRenderer : function (data, type, row) {
		var professionalProfile = row.memberModel.professionalProfile;
		var department;
		if(professionalProfile){
		  department = professionalProfile.department;
		  return department.code + " - " + department.description;
		}
		
		return "";
            }},	
            {name : 'transactionType', sortable : true},
            /* {name : 'transactionNo', sortable : true}, */
            {name : 'transactionAmount', sortable : true, alignment: 'right'},
            {name : 'transactionPoints', sortable : true, alignment: 'right'},
            /* {name : 'comment', sortable : true}, */
            {name : 'status', sortable : true, fieldCellRenderer : function (data, type, row) {
                return (row.status == 'FORAPPROVAL')? "FOR APPROVAL" : row.status ;
            }},
            {name : 'created', sortable : true},
            /* {name : 'approvalReason', sortable : true}, */
            {customCell : function ( innerData, sSpecific, json ) {
            if ( sSpecific == 'display' && json.status != "ACTIVE") {
	              return $btns.find( ".deleteBtn" ).prop( "outerHTML" ).replace( new RegExp( "%ID%", "g" ), json.id );
	              /* "<input class=\"btn btn-default tiptip icn-delete deleteBtn\" id=\"generateRewardButton\" title=\"${label_delete}\" type=\"button\" value=\"${label_delete}\""+
	                "onclick=\"location.href='${pageContext.request.contextPath}/pointsreward/delete/"+json.id+"';\"/>"; */
	              } else {
	                return '';
	              }
            }
        }];

		    <sec:authorize ifAnyGranted="${role_hrs}">
		    var checkAll = "<input type=\"checkbox\" id=\"checkAllRewards\" onclick=\"javascript: doCheck( this.id, 'id' );\"/>";
		    headers.unshift(checkAll);
		    var checkbox = {
		    		customCell : function ( innerData, sSpecific, json ) {
			          if ( sSpecific == 'display' && json.status != "ACTIVE") {
			              return "<input type=\"checkbox\" value=\""+json.id+"\" name=\"id\"/>";
			          } else {
			              return '';
			          }
			      }
		    };
		    modelFields.unshift(checkbox);
		    </sec:authorize>


		    $list.ajaxDataTable({
            'autoload'  : true,
            'ajaxSource' : $list.data( "url" ),
            'columnHeaders' : headers,
            'modelFields' : modelFields,
	    'aaSorting'   : [[ 1, "asc" ], [ 2, "asc" ]],
            'initCompleteCallback': function ( dataTable ) {
			          if($("#list_rewards").find("td:first").hasClass('dataTables_empty'))
			              $("#approverBtns").hide();
			          else
			              $("#approverBtns").show();
            }
        })
        .on( "click", ".deleteBtn", fndelete );
		    
	    initSearchForm($list);
	  }

	  function fndelete(e) {
		    e.preventDefault();
		    var url = $( this ).attr( "href" );
		    getConfirm( $( this ).data( "msg-confirm" ), function( result ) {
		        if( result ) {
		        	  $.get( url, function( resp ) { PointRewardsList.reloadTable(); } );
		        }
		    });
	  }

	  PointRewardsList = {
	      reloadTable  : function() { $list.ajaxDataTable( "search" ); }
    };
	
    function initFormFields() {
    
      $( "#searchField" ).change( function(e) {
        e.preventDefault();
        $( "#searchValue" ).attr( "name", $( this ).val() );
      }).change();
    }
    
  	function initSearchForm( inDataTable ) {
      $theSearchBtn.click( function() {
        var $theFormDataObj = $theSearchForm.toObject( { mode : 'first', skipEmpty : true } );
        var $btnSearch = $( this );
        $btnSearch.prop( 'disabled', true );
        inDataTable.ajaxDataTable( 
          'search', 
          $theFormDataObj, 
          function () {
            $btnSearch.prop( 'disabled', false );
        });
      });
      
      $("#clearButton").click(function(e) {
        e.preventDefault();
        $("#searchField option:first").attr('selected','selected');
        $("#searchValue").val("");
        $("#transactionDate").val("");
        $(".pointsSearchDropdown").val("");
        $("#txnNo").val("");
        $(".pointsSearchDropdown").val("");
        $("#searchButton").click();
      });
    }
});

</script>

<style type="text/css"> <!-- .data-table-01 { max-width: 99.99%; overflow-x: auto;  } --> </style>


</div>
