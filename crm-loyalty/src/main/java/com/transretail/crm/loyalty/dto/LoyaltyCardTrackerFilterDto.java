package com.transretail.crm.loyalty.dto;

import java.util.ArrayList;
import java.util.List;

import org.joda.time.LocalDate;
import org.springframework.format.annotation.DateTimeFormat;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.mysema.query.types.expr.BooleanExpression;
import com.transretail.crm.common.service.dto.request.AbstractSearchFormDto;
import com.transretail.crm.core.entity.lookup.LookupDetail;
import com.transretail.crm.loyalty.entity.QLoyaltyCardInventoryHistory;

public class LoyaltyCardTrackerFilterDto extends AbstractSearchFormDto {
	
	private LookupDetail product;
	private String location;
	@DateTimeFormat(pattern = "dd-MM-yyyy")
	private LocalDate dateFrom;
	@DateTimeFormat(pattern = "dd-MM-yyyy")
	private LocalDate dateTo;
	
	
	@Override
	@JsonIgnore
	public BooleanExpression createSearchExpression() {
		QLoyaltyCardInventoryHistory qHistory = QLoyaltyCardInventoryHistory.loyaltyCardInventoryHistory;
		List<BooleanExpression> expressions = new ArrayList<BooleanExpression>();
		
		if(product != null) {
			expressions.add(qHistory.inventory.product.name.code.eq(product.getCode()));
		}
		if(location != null) {
			expressions.add(qHistory.location.eq(location));
		}
		if(dateFrom != null) {
			expressions.add(qHistory.created.goe(dateFrom.toDateTimeAtStartOfDay()));
		}
		if(dateTo != null) {
			expressions.add(qHistory.created.loe(dateTo.plusDays(1).toDateTimeAtStartOfDay().minusMillis(1)));
		}
		
		return BooleanExpression.allOf(expressions.toArray(new BooleanExpression[expressions.size()]));
	}


	public LookupDetail getProduct() {
		return product;
	}


	public void setProduct(LookupDetail product) {
		this.product = product;
	}

	public String getLocation() {
		return location;
	}


	public void setLocation(String location) {
		this.location = location;
	}


	public LocalDate getDateFrom() {
		return dateFrom;
	}


	public void setDateFrom(LocalDate dateFrom) {
		this.dateFrom = dateFrom;
	}


	public LocalDate getDateTo() {
		return dateTo;
	}


	public void setDateTo(LocalDate dateTo) {
		this.dateTo = dateTo;
	}

}
