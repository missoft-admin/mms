package com.transretail.crm.core.service.impl;

import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.apache.commons.lang3.StringUtils;
import org.joda.time.DateTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.google.common.collect.Sets;
import com.mysema.query.Tuple;
import com.mysema.query.jpa.JPQLQuery;
import com.mysema.query.jpa.hibernate.HibernateSubQuery;
import com.mysema.query.jpa.impl.JPAQuery;
import com.mysema.query.support.Expressions;
import com.mysema.query.types.ConstantImpl;
import com.mysema.query.types.EntityPath;
import com.mysema.query.types.Expression;
import com.mysema.query.types.Ops;
import com.mysema.query.types.expr.BooleanExpression;
import com.mysema.query.types.path.StringPath;
import com.transretail.crm.core.dto.OverlappingPromoDto;
import com.transretail.crm.core.entity.ProductGroup;
import com.transretail.crm.core.entity.Promotion;
import com.transretail.crm.core.entity.PromotionMemberGroup;
import com.transretail.crm.core.entity.PromotionPaymentType;
import com.transretail.crm.core.entity.PromotionProductGroup;
import com.transretail.crm.core.entity.PromotionStoreGroup;
import com.transretail.crm.core.entity.PromotionTargetGroup;
import com.transretail.crm.core.entity.QPromotion;
import com.transretail.crm.core.entity.QPromotionMemberGroup;
import com.transretail.crm.core.entity.QPromotionPaymentType;
import com.transretail.crm.core.entity.QPromotionProductGroup;
import com.transretail.crm.core.entity.QPromotionStoreGroup;
import com.transretail.crm.core.entity.lookup.StoreGroup;
import com.transretail.crm.core.repo.PromotionRepo;
import com.transretail.crm.core.service.CodePropertiesService;
import com.transretail.crm.core.service.OverlappingPromoService;

/**
 * @author Allan G. Ramirez (agramirez@exist.com)
 */
@Service
public class OverlappingPromoServiceImpl implements OverlappingPromoService {
    private static final Logger _LOG = LoggerFactory.getLogger(OverlappingPromoServiceImpl.class);
    @Autowired
    private PromotionRepo promotionRepo;
    @PersistenceContext
    private EntityManager entityManager;
    @Autowired
    private CodePropertiesService codePropertiesService;

    @Override
    @Transactional
    public List<OverlappingPromoDto> getOverlappingPromo(long promotionId) {
        List<OverlappingPromoDto> results = Lists.newArrayList();
        Promotion promotion = promotionRepo.findOne(promotionId);
        if (promotion != null) {
            Map<MinPromoDto, Set<String>> promoMemberGroupNameMap = getPromoMemberGroupNameMap(promotion);
            Map<MinPromoDto, Set<String>> promoProductMap = getPromoProductMap(promotion);
            Map<MinPromoDto, Set<String>> promoStoreMap = getPromoStoreMap(promotion);
            Map<MinPromoDto, Set<String>> promoPaymentTypeMap = getPromoPaymentTypeMap(promotion);

            Set<MinPromoDto> commonPromos = Sets.newHashSet();
            commonPromos.addAll(promoMemberGroupNameMap.keySet());
            commonPromos.addAll(promoProductMap.keySet());
            commonPromos.addAll(promoStoreMap.keySet());
            commonPromos.addAll(promoPaymentTypeMap.keySet());

            for (MinPromoDto promo : commonPromos) {
                OverlappingPromoDto dto = new OverlappingPromoDto();
                dto.setPromotionId(promo.promotionId);
                dto.setPromotionName(promo.promotionName);
                dto.setBeginDateTime(new DateTime(promo.beginDateTime));
                dto.setEndDateTime(new DateTime(promo.endDateTime));

                dto.setMemberGroupNames(promoMemberGroupNameMap.get(promo));
                dto.setProducts(promoProductMap.get(promo));
                dto.setStores(promoStoreMap.get(promo));
                dto.setPaymentTypes(promoPaymentTypeMap.get(promo));

                results.add(dto);
            }
        } else {
            _LOG.warn("No promotion record found for id {}", promotionId);
        }
        return results;
    }

    private Map<MinPromoDto, Set<String>> getPromoMemberGroupNameMap(Promotion promotion) {
        QPromotionMemberGroup qPromoMemberGrp1 = QPromotionMemberGroup.promotionMemberGroup;

        Set<BooleanExpression> grpOrExps = Sets.newHashSet();
        for (PromotionMemberGroup grp : promotion.getTargetMemberGroups()) {
            grpOrExps.add(qPromoMemberGrp1.memberGroup.id.eq(grp.getMemberGroup().getId()));
        }

        BooleanExpression addlExpr = BooleanExpression.anyOf(grpOrExps.toArray(new BooleanExpression[grpOrExps.size()]));

        Map<MinPromoDto, Set<String>> result =
            getOverlappingPromotionTarget(promotion, qPromoMemberGrp1.promotion, addlExpr, qPromoMemberGrp1,
                qPromoMemberGrp1.memberGroup.name, new TargetNameProcessor() {
                @Override
                public void processName(Set<String> set, String name) {
                    set.add(name);
                }
            });

        QPromotionMemberGroup qPromoMemberGrp2 = QPromotionMemberGroup.promotionMemberGroup;
        result.putAll(getPromotionsWithNoTargets(promotion, QPromotion.promotion, qPromoMemberGrp2, qPromoMemberGrp2.promotion));

        return result;
    }

    private Map<MinPromoDto, Set<String>> getPromoProductMap(Promotion promotion) {
        QPromotionProductGroup qPromoProductGrp1 = QPromotionProductGroup.promotionProductGroup;

        Set<BooleanExpression> grpOrExps = Sets.newHashSet();
        Set<String> products = Sets.newHashSet();
        for (PromotionProductGroup grp : promotion.getTargetProductGroups()) {
            products.addAll(grp.getProductGroup().getProductList());
        }
        for (String product : products) {
            grpOrExps.add(qPromoProductGrp1.productGroup.products.contains(product));
        }

        BooleanExpression addlExpr = BooleanExpression.anyOf(grpOrExps.toArray(new BooleanExpression[grpOrExps.size()]));

        Map<MinPromoDto, Set<String>> result =
            getOverlappingPromotionTarget(promotion, qPromoProductGrp1.promotion, addlExpr, qPromoProductGrp1,
                qPromoProductGrp1.productGroup.products, new TargetNameProcessor() {
                @Override
                public void processName(Set<String> set, String productList) {
                    for (String product : StringUtils.split(productList, ProductGroup.PRODUCT_LIST_SEPARATOR)) {
                        set.add(product);
                    }
                }
            });

        QPromotionProductGroup qPromoProductGrp2 = QPromotionProductGroup.promotionProductGroup;
        result.putAll(getPromotionsWithNoTargets(promotion, QPromotion.promotion, qPromoProductGrp2, qPromoProductGrp2.promotion));

        return result;
    }

    private Map<MinPromoDto, Set<String>> getPromoStoreMap(Promotion promotion) {
        QPromotionStoreGroup qPromoStoreGrp1 = QPromotionStoreGroup.promotionStoreGroup;

        Set<BooleanExpression> grpOrExps = Sets.newHashSet();
        Set<String> stores = Sets.newHashSet();
        for (PromotionStoreGroup grp : promotion.getTargetStoreGroups()) {
            stores.addAll(grp.getStoreGroup().getStoreList());
        }
        for (String store : stores) {
            grpOrExps.add(qPromoStoreGrp1.storeGroup.stores.contains(store));
        }

        BooleanExpression addlExpr = BooleanExpression.anyOf(grpOrExps.toArray(new BooleanExpression[grpOrExps.size()]));

        Map<MinPromoDto, Set<String>> result =
            getOverlappingPromotionTarget(promotion, qPromoStoreGrp1.promotion, addlExpr, qPromoStoreGrp1,
                qPromoStoreGrp1.storeGroup.stores, new TargetNameProcessor() {
                @Override
                public void processName(Set<String> set, String storeList) {
                    for (String store : StringUtils.split(storeList, StoreGroup.STORE_LIST_SEPARATOR)) {
                        set.add(store);
                    }
                }
            });

        QPromotionStoreGroup qPromoStoreGrp2 = QPromotionStoreGroup.promotionStoreGroup;
        result.putAll(getPromotionsWithNoTargets(promotion, QPromotion.promotion, qPromoStoreGrp2, qPromoStoreGrp2.promotion));

        return result;
    }

    private Map<MinPromoDto, Set<String>> getPromoPaymentTypeMap(Promotion promotion) {
        QPromotionPaymentType qPromoPaymentType1 = QPromotionPaymentType.promotionPaymentType;

        Set<BooleanExpression> grpOrExps = Sets.newHashSet();
        for (PromotionPaymentType promotionPaymentType : promotion.getTargetPaymentTypes()) {
            grpOrExps.add(qPromoPaymentType1.lookupDetail.code.eq(promotionPaymentType.getLookupDetail().getCode()));
        }

        BooleanExpression addlExpr = BooleanExpression.anyOf(grpOrExps.toArray(new BooleanExpression[grpOrExps.size()]));

        Map<MinPromoDto, Set<String>> result =
            getOverlappingPromotionTarget(promotion, qPromoPaymentType1.promotion, addlExpr, qPromoPaymentType1,
                qPromoPaymentType1.lookupDetail.description, new TargetNameProcessor() {
                @Override
                public void processName(Set<String> set, String description) {
                    set.add(description);
                }
            });

        QPromotionPaymentType qPromoPaymentType2 = QPromotionPaymentType.promotionPaymentType;
        result.putAll(getPromotionsWithNoTargets(promotion, QPromotion.promotion, qPromoPaymentType2, qPromoPaymentType2.promotion));

        return result;
    }


    private Map<MinPromoDto, Set<String>> getOverlappingPromotionTarget(Promotion promotion, QPromotion qPromotion,
        BooleanExpression addlExpr, EntityPath<? extends PromotionTargetGroup> qTargetGroup, StringPath targetName,
        TargetNameProcessor targetNameProcessor) {
        Map<MinPromoDto, Set<String>> result = Maps.newHashMap();
        BooleanExpression overlapConditions = getOverlapConditions(qPromotion, promotion);
        JPQLQuery query = new JPAQuery(entityManager).from(qTargetGroup);

        if (addlExpr != null) {
            query = query.where(overlapConditions.and(addlExpr));
        } else {
            query = query.where(overlapConditions);
        }

        List<Tuple> tuples = query.list(qPromotion.id, qPromotion.name, qPromotion.startDate, qPromotion.endDate, targetName);
        for (Tuple tuple : tuples) {
            MinPromoDto key =
                new MinPromoDto(tuple.get(qPromotion.id), tuple.get(qPromotion.name), tuple.get(qPromotion.startDate),
                    tuple.get(qPromotion.endDate));
            Set<String> set = result.get(key);
            if (set == null) {
                set = Sets.newHashSet();
                result.put(key, set);
            }
            targetNameProcessor.processName(set, tuple.get(targetName));
        }
        return result;
    }

    private Map<MinPromoDto, Set<String>> getPromotionsWithNoTargets(Promotion promotion, QPromotion qPromotion,
        EntityPath<? extends PromotionTargetGroup> qTargetGroup, QPromotion targetGroupPromotion) {
        Map<MinPromoDto, Set<String>> result = Maps.newHashMap();
        BooleanExpression overlapConditions = getOverlapConditions(qPromotion, promotion);

        List<Tuple> tuples = new JPAQuery(entityManager).from(qPromotion)
            .where(
                overlapConditions
                    .and(new HibernateSubQuery().from(qTargetGroup).where(targetGroupPromotion.id.eq(qPromotion.id)).count().lt(1))
            )
            .where(new HibernateSubQuery().from(qTargetGroup).where(targetGroupPromotion.id.eq(qPromotion.id)).count().lt(1))
            .list(qPromotion.id, qPromotion.name, qPromotion.startDate, qPromotion.endDate);
        for (Tuple tuple : tuples) {
            MinPromoDto key =
                new MinPromoDto(tuple.get(qPromotion.id), tuple.get(qPromotion.name), tuple.get(qPromotion.startDate),
                    tuple.get(qPromotion.endDate));
            Set<String> set = result.get(key);
            if (set == null) {
                set = Sets.newHashSet();
                result.put(key, set);
            }
            set.add("ALL");
        }
        return result;
    }

    private BooleanExpression getOverlapConditions(QPromotion qPromotion, Promotion promotion) {
        Expression<Date> startDate = new ConstantImpl(promotion.getStartDate());
        Expression<Date> endDate = new ConstantImpl(promotion.getEndDate());
        return qPromotion.id.ne(promotion.getId())
            .and(qPromotion.status.code.eq(codePropertiesService.getDetailStatusActive())) // Bug#84954
            .and(Expressions.predicate(Ops.BETWEEN, qPromotion.startDate, startDate, endDate)
                .or(Expressions.predicate(Ops.BETWEEN, qPromotion.endDate, startDate, endDate)));
    }

    private class MinPromoDto {
        Long promotionId;
        String promotionName;
        DateTime beginDateTime;
        DateTime endDateTime;

        MinPromoDto(Long promotionId, String promotionName, Date beginDateTime, Date endDateTime) {
            this.promotionId = promotionId;
            this.promotionName = promotionName;
            this.beginDateTime = new DateTime(beginDateTime);
            this.endDateTime = new DateTime(endDateTime);
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;

            MinPromoDto that = (MinPromoDto) o;

            if (!promotionId.equals(that.promotionId)) return false;

            return true;
        }

        @Override
        public int hashCode() {
            return promotionId.hashCode();
        }
    }

    private interface TargetNameProcessor {
        void processName(Set<String> set, String name);
    }
}
