drop table CRM_GC_BURNCARD;
drop table CRM_GC_BURNCARD_AUD;

create table CRM_GC_BURNCARD (
    id number(19,0) not null,
    CREATED_BY varchar2(255 char),
    CREATED_DATETIME timestamp,
    LAST_UPDATED_BY varchar2(255 char),
    LAST_UPDATED_DATETIME timestamp,
    APPROVED_BY varchar2(255 char),
    BURN_NO varchar2(255 char),
    BURN_REASON varchar2(255 char),
    DATE_FILED date,
    FILED_BY varchar2(255 char),
    STATUS varchar2(255 char),
    STORE_ID number(10,0),
    INVENTORY_NO number(19,0) not null,
    primary key (id)
);

create table CRM_GC_BURNCARD_AUD (
    id number(19,0) not null,
    REV number(19,0) not null,
    REVTYPE number(3,0),
    APPROVED_BY varchar2(255 char),
    BURN_NO varchar2(255 char),
    BURN_REASON varchar2(255 char),
    DATE_FILED date,
    FILED_BY varchar2(255 char),
    STATUS varchar2(255 char),
    STORE_ID number(10,0),
    primary key (id, REV)
);

alter table CRM_GC_BURNCARD 
    add constraint FK_tc0p71b14ec31agvyxwqkte1y 
    foreign key (INVENTORY_NO) 
    references CRM_GC_INVENTORY;

alter table CRM_GC_BURNCARD_AUD 
    add constraint FK_sghfi9w6vgih09448tovdm9ye 
    foreign key (REV) 
    references CRM_REVINFO;