package com.transretail.crm.common.web.converter;

import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.apache.commons.lang3.StringUtils;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.springframework.format.datetime.DateFormatter;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;

/**
 *
 */
public class DateDeserializer extends JsonDeserializer<Date> {
	private static final DateFormat DATEWITHTIME = new SimpleDateFormat("dd-MM-yyyy hh:mm:ss aa");
    private static final DateFormat DATEONLY = new SimpleDateFormat("dd-MM-yyyy");
    
    @Override
    public Date deserialize(JsonParser jp, DeserializationContext ctxt) throws IOException, JsonProcessingException {
    	if (StringUtils.isNotBlank(jp.getText())) {
            try {
                return DATEWITHTIME.parse(jp.getText());
            } catch (Exception e) {
                try {return DATEONLY.parse(jp.getText());} catch (ParseException e1) {return null;}
            } 
        } else {
            return null;
        }
    }
}
