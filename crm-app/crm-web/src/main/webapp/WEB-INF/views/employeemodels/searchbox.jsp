<%@ include file="../common/taglibs.jsp" %>

<style type="text/css">
<!--
  .well .input {
    background-color: #fff;
    border: 1px solid #E8E8E8;
    box-shadow: none;    
    padding: 6px;
    height: 18px;
    margin-left: 5px;
  }
  .well .btn-primary {
    margin-left: 0px;
    margin-right: 0px;
  }
  .well select {
    margin: 5px;
  }
  
-->
</style>

    <c:set var="ENCTYPE" value="application/x-www-form-urlencoded"/>
    <c:if test="${multipart}"><c:set var="ENCTYPE" value="multipart/form-data"/></c:if>
    
    <c:set var="MODEL_ATTRIBUTE" value="searchEmployeeModel"/>
    <c:set var="form_url" value="/employeemodels/search"/>
    <c:url var="PATH" value="${form_url}?page=1&size=10"/>
    <spring:message var="BUTTON_LABEL" code="button_search" text="Search"/>
    <form:form action="${PATH}" method="POST" id="${MODEL_ATTRIBUTE}" modelAttribute="${MODEL_ATTRIBUTE}" enctype="${ENCTYPE}">
        <div class="contentMain well">
            <div class="form-inline">
                <label class="label-single"><h5><spring:message code="entity_search" />:</h5></label>
                <select id="search_employeeCriteria">
                    <c:forEach var="criteria" items="${employeeCriteria}">
                      <c:if test="${criteria.value ne searchExcept}">
                        <option value="${criteria.value}"><spring:message code="employee_label_search_${criteria.value}" /></option>
                      </c:if>
                    </c:forEach>
                </select>
                <div id="userSearchForm" class="inline">
                    <input id="search_employeeField" class="input" placeholder="${BUTTON_LABEL}" />              
                </div>
                <input type="submit" id="search_submit" value="${BUTTON_LABEL}" class="btn btn-primary"/>
                <input type="button" id="searchClearButton" value="<spring:message code="label_clear" />" class="btn "/>
            </div>   
            <div class="form-inline form-search">
              <label class="label-single"><h5><spring:message code="label_filter" />:</h5></label>
              <select id="status" class="pointsSearchDropdown memberSearchField" name="status" data-name="status">
                      <option value=""><spring:message code="label_com_transretail_crm_entity_customermodel_status" /></option>
                  <c:forEach var="item" items="${status}">
                      <option value="${item.value}">${item.desc}</option>
                  </c:forEach>
              </select>
              <select class="pointsSearchDropdown memberSearchField" name="registeredStore" data-name="registeredStore" id="registeredStore">
                      <option value=""><spring:message code="label_com_transretail_crm_entity_customermodel_registeredstore" /></option>
                  <c:forEach var="item" items="${preferredStore}">
                      <option value="${item.code}">${item.codeAndName}</option>
                  </c:forEach>
              </select>
          </div>
        </div>
    </form:form>
    <%--
    <script type="text/javascript">
    $(document).ready(function() {
        $( "form[id='${MODEL_ATTRIBUTE}']" ).submit( function() {
        	$("#search_employeeField").attr("name", $("#search_employeeCriteria").val());
        	$(this).submit();
        });
    });
    </script>
 --%>
