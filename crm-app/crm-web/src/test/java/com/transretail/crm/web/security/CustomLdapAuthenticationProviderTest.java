package com.transretail.crm.web.security;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Configurable;
import org.springframework.ldap.core.support.DirContextSource;
import org.springframework.security.authentication.AuthenticationServiceException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.ldap.authentication.BindAuthenticator;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import com.transretail.crm.core.security.model.CustomSecurityUserDetails;
import com.transretail.crm.core.security.model.impl.CustomSecurityUserDetailsImpl;
import com.transretail.crm.core.service.UserService;
import com.transretail.crm.core.service.impl.UserServiceImpl;

@Configurable
@ContextConfiguration(locations = "classpath:/META-INF/spring/applicationContext-test.xml")
@Transactional
@RunWith(SpringJUnit4ClassRunner.class)
public class CustomLdapAuthenticationProviderTest {
	
	final String PASSWORD = "password";
	final String INVALID_PASSWORD = "invalidpassword";
	final String USERNAME = "username";

	@Mock
	private UserDetailsService userDetailsService;
	
	@Mock
	private PasswordEncoder passwordEncoder;
	
	@InjectMocks
    CustomLdapAuthenticationProvider authenticationProvider = new CustomLdapAuthenticationProvider(new BindAuthenticator((new DirContextSource())));
	
	UserService userService = new UserServiceImpl();
	
	@Before
	public void setUp() {
		MockitoAnnotations.initMocks(this);
	}
	
	@Test
	public void testAuthenticate() {
		CustomSecurityUserDetails userDetails = mock(CustomSecurityUserDetailsImpl.class);
		when(userDetails.getUsername()).thenReturn(USERNAME);
		when(userDetails.getPassword()).thenReturn(PASSWORD);
		when(userDetails.isEnabled()).thenReturn(true);
		
		when(userDetailsService.loadUserByUsername(USERNAME)).thenReturn(userDetails);
		when(passwordEncoder.matches(PASSWORD, userDetails.getPassword())).thenReturn(true);
		
		Authentication authentication = new UsernamePasswordAuthenticationToken(USERNAME, PASSWORD);
		
		Authentication returned = authenticationProvider.authenticate(authentication);
		
		UsernamePasswordAuthenticationToken details = (UsernamePasswordAuthenticationToken)returned.getDetails();
		
		Assert.assertNotNull(returned);
		Assert.assertEquals(PASSWORD, details.getCredentials());
		Assert.assertEquals(USERNAME, details.getPrincipal());
	}
	
	@Test(expected=AuthenticationServiceException.class)
	public void testAuthenticateNullUser() {
		when(userDetailsService.loadUserByUsername(USERNAME)).thenReturn(null);
		Authentication authentication = new UsernamePasswordAuthenticationToken(USERNAME, PASSWORD);
		authenticationProvider.authenticate(authentication);
	}
	
	@Test(expected=AuthenticationServiceException.class)
	public void testAuthenticateUserNotEnabled() {
		CustomSecurityUserDetails userDetails = mock(CustomSecurityUserDetailsImpl.class);
		when(userDetails.getUsername()).thenReturn(USERNAME);
		when(userDetails.getPassword()).thenReturn(PASSWORD);
		when(userDetails.isEnabled()).thenReturn(false);
		
		when(userDetailsService.loadUserByUsername(USERNAME)).thenReturn(userDetails);
		
		Authentication authentication = new UsernamePasswordAuthenticationToken(USERNAME, PASSWORD);
		authenticationProvider.authenticate(authentication);
	}
	
	@Test(expected=AuthenticationServiceException.class)
	public void testAuthenticatePasswordsDoNotMatch() {
		CustomSecurityUserDetails userDetails = mock(CustomSecurityUserDetailsImpl.class);
		when(userDetails.getUsername()).thenReturn(USERNAME);
		when(userDetails.getPassword()).thenReturn(PASSWORD);
		when(userDetails.isEnabled()).thenReturn(true);
		
		when(userDetailsService.loadUserByUsername(USERNAME)).thenReturn(userDetails);
		when(passwordEncoder.matches(PASSWORD, userDetails.getPassword())).thenReturn(false);
		
		Authentication authentication = new UsernamePasswordAuthenticationToken(USERNAME, INVALID_PASSWORD);
		authenticationProvider.authenticate(authentication);
	}

}
