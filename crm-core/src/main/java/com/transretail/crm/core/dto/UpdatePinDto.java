package com.transretail.crm.core.dto;

import org.hibernate.validator.constraints.NotEmpty;

import com.transretail.crm.core.validator.Confirm;

/**
 * @author Allan G. Ramirez (agramirez@exist.com)
 */
@Confirm(field = "pin")
public class UpdatePinDto {
    @NotEmpty
    private String pin;
    @NotEmpty
    private String confirmPin;

    public String getPin() {
        return pin;
    }

    public void setPin(String pin) {
        this.pin = pin;
    }

    public String getConfirmPin() {
        return confirmPin;
    }

    public void setConfirmPin(String confirmPin) {
        this.confirmPin = confirmPin;
    }
}
