package com.transretail.crm.giftcard.service.impl;

import com.mysema.query.types.OrderSpecifier;
import com.mysema.query.types.expr.BooleanExpression;
import com.transretail.crm.common.util.FileCryptor;
import com.transretail.crm.giftcard.entity.GiftCardInventory;
import com.transretail.crm.giftcard.entity.GiftCardOrder;
import com.transretail.crm.giftcard.repo.GiftCardInventoryRepo;
import com.transretail.crm.giftcard.service.GiftCardOrderService;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import static org.fest.assertions.api.Assertions.assertThat;
import static org.mockito.Mockito.*;

/**
 *
 * @author Monte Cillo Co (mco@exist.com)
 */
public class GiftCardOrderServiceImplTest {

    @Mock
    private GiftCardInventoryRepo inventoryRepo; // DOC

    @InjectMocks
    private final GiftCardOrderService cardOrderService = new GiftCardOrderServiceImpl(); // SUT
    private static final int BATCH_NO = 1;
    public static final String ANY_PRODUCT_CODE = "aValidProductCode";
    public static final String ANY_PRODUCT_NAME = "aValidProductName";
    public static final String ANY_MO_NO = "aValidMONo";

    public static final GiftCardOrder ANY_GCO = new GiftCardOrder() {
	{
	    setId(Long.MAX_VALUE);
	    setMoNumber(ANY_MO_NO);
	}
    };

    private final List<GiftCardInventory> inventories = new ArrayList<GiftCardInventory>() {
	{
	    add(new GiftCardInventory() {
		{
		    setBatchNo(BATCH_NO);
		    setBarcode("14042410100000029959");
		    setProductCode(ANY_PRODUCT_CODE);
		    setProductName(ANY_PRODUCT_NAME);
		    setOrder(ANY_GCO);
		}
	    });
	    add(new GiftCardInventory() {
		{
		    setBatchNo(BATCH_NO);
		    setBarcode("14042410100000029960");
		    setProductCode(ANY_PRODUCT_CODE);
		    setProductName(ANY_PRODUCT_NAME);
		    setOrder(ANY_GCO);
		}
	    });
	}
    };

    @Before
    public void setup() {
	MockitoAnnotations.initMocks(this);
    }

    @Test
    public void shouldSuccessfullyCreatedAnEncryptedFileOfMO() throws IOException, Exception {

	when(inventoryRepo.findAll(any(BooleanExpression.class), any(OrderSpecifier.class)))
		.thenReturn(inventories);

	File encryptedFile = cardOrderService.printEncryptedFile(12l);
	File decryptedFile = File.createTempFile("decrypted", ".txt");

	FileInputStream fis2 = new FileInputStream(encryptedFile);
	FileOutputStream fos2 = new FileOutputStream(decryptedFile);
	FileCryptor.decrypt(FileCryptor.DEFAULT_KEY, fis2, fos2);

	assertThat(decryptedFile).hasContent(getExpectedContent());
	verify(inventoryRepo).findAll(any(BooleanExpression.class), any(OrderSpecifier.class));

	encryptedFile.delete();
	decryptedFile.delete();
    }

    private String getExpectedContent() {
	StringBuilder sb = new StringBuilder();
	for (int i = 0; i < inventories.size(); i++) {
	    GiftCardInventory in = inventories.get(i);
	    sb.append(String.format(GiftCardOrderService.STANDARD_ENCRYPTED_FILE_CONTENT_FORMAT,
		    i + 1, in.getOrder().getMoNumber(),
		    in.getBatchNo(), in.getBarcode(), in.getProductCode(), in.getProductName()));
	}

	return sb.toString();
    }

}
