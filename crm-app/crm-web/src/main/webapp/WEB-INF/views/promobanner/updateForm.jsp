<%@ include file="../common/taglibs.jsp"%>

<style type="text/css">
<!--
  .form-horizontal .form-actions {
      padding-left: 0px;
  }
-->
</style>


<c:set var="ENCTYPE" value="multipart/form-data"/>

<div class="page-header page-header2">
	<h1>Create New <spring:message code="label_promo_banner" /></h1>
</div>

<c:url value="/promobanner" var="action_url" />
<form:form id="promoBanner" name="promoBanner" modelAttribute="promoBanner" action="${action_url}" enctype="${ENCTYPE}">

<c:set var="errors"><form:errors path="*"/></c:set>
<c:if test="${not empty errors}">
	<div class="alert alert-error">
		<button type="button" class="close" data-dismiss="alert">&times;</button>
	    <div>${errors}</div>
	</div>
</c:if>

<div class="container-fluid">
	<div class="row-fluid">
		<div id="updateForm" class="">
			<div class="form-horizontal">
				<form:hidden path="id"/>
				<form:hidden path="statusCode" />
				
				<div class="control-group">
					<label class="control-label"><spring:message code="label_promo_banner_type"/></label>
					<div class="controls">
						<form:select id="type" path="type" items="${types}"/>
					</div>
				</div>
				
				<div class="control-group">
					<label class="control-label optional banner advertisment"><spring:message code="label_promo_banner_name"/></label>
                    <label class="control-label optional news"><spring:message code="label_promo_banner_headline"/><b class="required">*</b></label>
                    <label class="control-label optional product"><spring:message code="label_promo_banner_short_desc"/><b class="required">*</b></label>
					<div class="controls">
						<form:input path="name"/>
					</div>
				</div>
                
                <div class="control-group optional product news">
                    <label class="control-label optional product"><spring:message code="label_promo_banner_description"/><b class="required">*</b></label>
                    <label class="control-label optional news"><spring:message code="label_promo_banner_summary"/><b class="required">*</b></label>
                    <div class="controls">
                        <form:textarea path="info"/>
                    </div>
                </div>
                
                <div class="product optional">
                    <div class="control-group">
                        <label class="control-label"><spring:message code="label_promo_banner_quantity"/><b class="required">*</b></label>
                        <div class="controls">
                            <form:input path="productQuantity"/>
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label"><spring:message code="label_promo_banner_old_price"/><b class="required">*</b></label>
                        <div class="controls">
                            <form:input path="oldPrice"/>
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label"><spring:message code="label_promo_banner_new_price"/><b class="required">*</b></label>
                        <div class="controls">
                            <form:input path="newPrice"/>
                        </div>
                    </div>
                </div>
				
				<div class="control-group">
					<label class="control-label"><spring:message code="label_promo_banner_startdate"/><b
						class="required">*</b></label>
					<div class="controls">
						<form:input id="startDate" cssClass="date" path="startDate"/>
					</div>
				</div>
				
				<div class="control-group">
					<label class="control-label"><spring:message code="label_promo_banner_enddate"/><b
						class="required">*</b></label>
					<div class="controls">
						<form:input id="endDate" cssClass="date" path="endDate"/>
					</div>
				</div>
				
				<div class="control-group">
					<label class="control-label"><spring:message code="label_promo_banner_link"/><b
                        class="required banner advertisment news optional">*</b></label>
					<div class="controls">
						<form:input maxlength="200" path="link"/>
					</div>
				</div>
				
				<div class="control-group">
					<label class="control-label"><spring:message code="label_promo_banner_image"/><b
						class="required">*</b></label>
					<div class="controls">
						<input type="file" id="file" name="file" style="height: 31px"><br/>
						<span class="label">
							<c:forEach items="${types}" var="type">
								<div id="type_${type}" class="type">
									<spring:message code="label_promo_banner_image_details" arguments="${type.width}, ${type.height}"/>
								</div>
							</c:forEach>
						</span>
					</div>
				</div>
				
				<div class="form-actions container-btn">
					<c:choose>
						<c:when test="${forApproval}">
							<button id="approve" data-status="STAT001" type="button" class="btn btn-primary status">
								<spring:message code="label_approve" />
							</button>
							<button id="reject" data-status="STAT003" type="button" class="btn btn-primary status">
								<spring:message code="label_reject" />
							</button>
						</c:when>
						<c:otherwise>
						<button id="new" data-status="STAT002" type="button" class="btn btn-primary status">
							<spring:message code="label_submit_for_approval" />
						</button>
						</c:otherwise>
					</c:choose>
					
					<!-- 
					<button id="save" type="button" class="btn btn-primary">
						<spring:message code="label_save" />
					</button>
					 -->
					
					<a href="<c:url value="/promobanner" />" id="closeForm" class="btn"><spring:message code="label_cancel" /></a> 
				</div>
			</div>
		</div>
		
		<c:if test="${update}">
		<div class="span6">
			<label><spring:message code="label_promo_banner_image" />:</label>
			<div class="thumbnail">
				<img src="<c:url value="/promobanner/image/" />${id}" style="width: 360px; height: 270px"/>
			</div>
		</div>
		</c:if>
	</div>
</div>

</form:form>

<script src='<c:url value="/js/bootstrap/bootstrap-datepicker.js"/>' type='text/javascript'><![CDATA[&nbsp; ]]></script>
<link href="<c:url value="/css/bootstrap/datepicker.css" />" rel="stylesheet" />

<script>
$(document).ready(function() {
	$(".date").datepicker({
		format		:	'dd-mm-yyyy',
		autoclose	:	true,
		startDate	: 	'-0d'
	});

	$("#startDate").datepicker().on('changeDate', function() {
		var endDateStart = $("#startDate").datepicker('getDate');
		$("#endDate").datepicker('setStartDate', endDateStart);
	});

	var form = $("#promoBanner");
	var base_url = form.attr("action");
//	$("#new").click(function() {
//		form.attr("action", base_url + "/new");
//		form.submit();
//	});
//
//	$("#approve").click(function() {
//		form.attr("action", base_url + "/approve");
//		form.submit();
//	});

	$(".status").click(function() {
		var status = $(this).data('status');
		form.attr("action", base_url + "/update/status/" + status);
		form.submit();
	});
	
	$("#save").click(function() {
		form.attr("action", base_url + "/save");
		form.submit();
	});

	$("#type").change(function() {
		$(".type").hide();
        var val = $(this).val();
		$("#type_" + val).show();
        $(".optional").hide();
        $("." + val.toLowerCase()).show();
	});

	$("#type").change();
});
</script>