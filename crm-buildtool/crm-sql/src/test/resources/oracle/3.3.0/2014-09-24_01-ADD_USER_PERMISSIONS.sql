insert into CRM_USER_PERMISSION(CODE, DESCRIPTION, STATUS, ROLE_CATEGORY,ORDERING) values ('PEOPLESOFT_MAPPING', 'PeopleSoft Store Mapping', 'ACTIVE', 'ROLE_ADMIN', 1006);
insert into CRM_USER_PERMISSION(CODE, DESCRIPTION, STATUS, ROLE_CATEGORY,ORDERING) values ('GC_GENERAL_DISCOUNT', 'General Discount', 'ACTIVE', 'ROLE_REPORTS', 5022);
insert into CRM_USER_PERMISSION(CODE, DESCRIPTION, STATUS, ROLE_CATEGORY,ORDERING) values ('REPORT_POINTS_EXPIRE', 'Points Expired Report', 'ACTIVE', 'ROLE_REPORTS', 6026);
insert into CRM_USER_PERMISSION(CODE, DESCRIPTION, STATUS, ROLE_CATEGORY,ORDERING) values ('REPORT_POPULAR_STORES', 'Popular Stores to Visit', 'ACTIVE', 'ROLE_REPORTS', 6027);
insert into CRM_USER_PERMISSION(CODE, DESCRIPTION, STATUS, ROLE_CATEGORY,ORDERING) values ('GC_B2B_CUSTOMER_PAYMENT_REPORT', 'B2B Customer Payment Control Report', 'ACTIVE', 'ROLE_REPORTS', 6028);
insert into CRM_USER_PERMISSION(CODE, DESCRIPTION, STATUS, ROLE_CATEGORY,ORDERING) values ('GC_DISCOUNT_RATE_REPORT', 'Discount Rate Report', 'ACTIVE', 'ROLE_REPORTS', 6029);
insert into CRM_USER_PERMISSION(CODE, DESCRIPTION, STATUS, ROLE_CATEGORY,ORDERING) values ('GC_DISCOUNT_SUMMARY_REPORT', 'Discount Summary Report', 'ACTIVE', 'ROLE_REPORTS', 6030);
insert into CRM_USER_PERMISSION(CODE, DESCRIPTION, STATUS, ROLE_CATEGORY,ORDERING) values ('GC_ABNORMAL_BALANCE_REPORT', 'Gift Card Abnormal Balance Report', 'ACTIVE', 'ROLE_REPORTS', 6031);
insert into CRM_USER_PERMISSION(CODE, DESCRIPTION, STATUS, ROLE_CATEGORY,ORDERING) values ('GC_AVERAGE_TRANSACTION_AMT_REPORT', 'Gift Card Average Transaction Amount Report', 'ACTIVE', 'ROLE_REPORTS', 6032);
insert into CRM_USER_PERMISSION(CODE, DESCRIPTION, STATUS, ROLE_CATEGORY,ORDERING) values ('GC_ORDER_SUMMARY_REPORT', 'Gift Card Order Summary', 'ACTIVE', 'ROLE_REPORTS', 6033);
insert into CRM_USER_PERMISSION(CODE, DESCRIPTION, STATUS, ROLE_CATEGORY,ORDERING) values ('GC_SETTLEMENT_REPORT', 'Gift Card Settlement', 'ACTIVE', 'ROLE_REPORTS', 6034);
insert into CRM_USER_PERMISSION(CODE, DESCRIPTION, STATUS, ROLE_CATEGORY,ORDERING) values ('GC_STORE_PERFORMANCE_REPORT', 'Gift Card Store Performance Report', 'ACTIVE', 'ROLE_REPORTS', 6035);
insert into CRM_USER_PERMISSION(CODE, DESCRIPTION, STATUS, ROLE_CATEGORY,ORDERING) values ('GC_ACCOUNT_RECEIVABLE_AGE', 'Account Receivable Age', 'ACTIVE', 'ROLE_REPORTS', 6036);
--ADMIN
UPDATE CRM_USER_PERMISSION SET ORDERING=1001 WHERE CODE='USER_MANAGEMENT';
UPDATE CRM_USER_PERMISSION SET ORDERING=1002 WHERE CODE='LOOKUP_MANAGEMENT';
UPDATE CRM_USER_PERMISSION SET ORDERING=1003 WHERE CODE='CITY_PROVINCE_LOOKUP';
UPDATE CRM_USER_PERMISSION SET ORDERING=1004 WHERE CODE='APP_CONFIGURATION';
UPDATE CRM_USER_PERMISSION SET ORDERING=1005 WHERE CODE='POINTS_CONFIGURATION';
UPDATE CRM_USER_PERMISSION SET ORDERING=1006 WHERE CODE='PEOPLESOFT_MAPPING';
UPDATE CRM_USER_PERMISSION SET ORDERING=1007 WHERE CODE='STORE_MEMBER_SCHEME';
UPDATE CRM_USER_PERMISSION SET ORDERING=1008 WHERE CODE='VIEW_STORE_LOGS';
UPDATE CRM_USER_PERMISSION SET ORDERING=1009 WHERE CODE='VIEW_CRM_PROXY_INFO';
UPDATE CRM_USER_PERMISSION SET ORDERING=1010 WHERE CODE='VENDOR_MANAGEMENT';
UPDATE CRM_USER_PERMISSION SET ORDERING=1011 WHERE CODE='HR_OVERRIDE_PERMISSIONS_SETUP';
UPDATE CRM_USER_PERMISSION SET ORDERING=1012 WHERE CODE='POINTS_OVVERIDE_PERMISSIONS_SETUP';

--MARKETING
UPDATE CRM_USER_PERMISSION SET ORDERING=2001 WHERE CODE='CUSTOMER_GROUP_MANAGEMENT';
UPDATE CRM_USER_PERMISSION SET ORDERING=2002 WHERE CODE='STORE_GROUP_MANAGEMENT';
UPDATE CRM_USER_PERMISSION SET ORDERING=2003 WHERE CODE='PRODUCT_GROUP_MANAGEMENT';
UPDATE CRM_USER_PERMISSION SET ORDERING=2004 WHERE CODE='PROMOTIONS_MANAGEMENT';
UPDATE CRM_USER_PERMISSION SET ORDERING=2005 WHERE CODE='MEDIA_MGMT';
UPDATE CRM_USER_PERMISSION SET ORDERING=2006 WHERE CODE='DATA_ANALYSIS';
UPDATE CRM_USER_PERMISSION SET ORDERING=2007 WHERE CODE='PROMO_BANNER_MANAGEMENT';
UPDATE CRM_USER_PERMISSION SET ORDERING=2008 WHERE CODE='LC_ANNUAL_FEE_SCHEME';

--MEMBER MANAGEMENT
UPDATE CRM_USER_PERMISSION SET ORDERING=3001 WHERE CODE='IND_PROF_MEMBERS_MANAGEMENT';
UPDATE CRM_USER_PERMISSION SET ORDERING=3002 WHERE CODE='UPLOAD_IND_PROF_MEMBERS';
UPDATE CRM_USER_PERMISSION SET ORDERING=3003 WHERE CODE='MEMBER_MONITOR';
UPDATE CRM_USER_PERMISSION SET ORDERING=3004 WHERE CODE='COMPLAINTS_MGMT';
UPDATE CRM_USER_PERMISSION SET ORDERING=3005 WHERE CODE='IND_PROF_POINTS_ADJUSTMENT';
UPDATE CRM_USER_PERMISSION SET ORDERING=3006 WHERE CODE='GC_SERVICE_REQUEST';
UPDATE CRM_USER_PERMISSION SET ORDERING=3007 WHERE CODE='GC_PRINT_SERVICE_REQUEST';

--EMPLOYEE MANAGEMENT
UPDATE CRM_USER_PERMISSION SET ORDERING=4001 WHERE CODE='EMPLOYEE_MANAGEMENT';
UPDATE CRM_USER_PERMISSION SET ORDERING=4002 WHERE CODE='UPLOAD_EMPLOYEES';
UPDATE CRM_USER_PERMISSION SET ORDERING=4003 WHERE CODE='MEMBER_MONITOR_EMP';
UPDATE CRM_USER_PERMISSION SET ORDERING=4004 WHERE CODE='EMP_PURCHASE_ADJUSTMENT';
UPDATE CRM_USER_PERMISSION SET ORDERING=4005 WHERE CODE='EMP_POINTS_ADJUSTMENT';
UPDATE CRM_USER_PERMISSION SET ORDERING=4006 WHERE CODE='REWARDS_GENERATION_SCHEDULE';
UPDATE CRM_USER_PERMISSION SET ORDERING=4007 WHERE CODE='REWARDS_APPROVAL';
UPDATE CRM_USER_PERMISSION SET ORDERING=4008 WHERE CODE='EMP_REWARDS_SCHEME_MANAGEMENT';
UPDATE CRM_USER_PERMISSION SET ORDERING=4009 WHERE CODE='RECON_MGMT';

--GENERAL MERCHANT
UPDATE CRM_USER_PERMISSION SET ORDERING=5001 WHERE CODE='LC_BURN_CARDS';
UPDATE CRM_USER_PERMISSION SET ORDERING=5002 WHERE CODE='LC_D2D_TRACKING';
UPDATE CRM_USER_PERMISSION SET ORDERING=5003 WHERE CODE='LC_INV';
UPDATE CRM_USER_PERMISSION SET ORDERING=5004 WHERE CODE='LC_MO';
UPDATE CRM_USER_PERMISSION SET ORDERING=5005 WHERE CODE='LC_PHYSL_COUNT';
UPDATE CRM_USER_PERMISSION SET ORDERING=5006 WHERE CODE='GC_PROD_PROF';
UPDATE CRM_USER_PERMISSION SET ORDERING=5007 WHERE CODE='SAFETY_STOCK';
UPDATE CRM_USER_PERMISSION SET ORDERING=5008 WHERE CODE='GC_MO';
UPDATE CRM_USER_PERMISSION SET ORDERING=5009 WHERE CODE='GC_INV_STOCK';
UPDATE CRM_USER_PERMISSION SET ORDERING=5010 WHERE CODE='STOCK_REQUEST_MANAGEMENT';
UPDATE CRM_USER_PERMISSION SET ORDERING=5011 WHERE CODE='GC_ADJUST_INVENTORY';
UPDATE CRM_USER_PERMISSION SET ORDERING=5012 WHERE CODE='GC_CX_PROFILE';
UPDATE CRM_USER_PERMISSION SET ORDERING=5013 WHERE CODE='GC_BURN_CARD';
UPDATE CRM_USER_PERMISSION SET ORDERING=5014 WHERE CODE='GC_DISC_SCHEME';
UPDATE CRM_USER_PERMISSION SET ORDERING=5015 WHERE CODE='GC_EXPIRY_DATE_EXTENSION';
UPDATE CRM_USER_PERMISSION SET ORDERING=5016 WHERE CODE='CLAIMS_TO_AFFILIATES_SCHEDULE';
UPDATE CRM_USER_PERMISSION SET ORDERING=5017 WHERE CODE='GC_SALES_ORDER';
UPDATE CRM_USER_PERMISSION SET ORDERING=5018 WHERE CODE='GC_B2B_EXHIBITION';
UPDATE CRM_USER_PERMISSION SET ORDERING=5019 WHERE CODE='GC_RETURN';
UPDATE CRM_USER_PERMISSION SET ORDERING=5020 WHERE CODE='GC_REFUND';
UPDATE CRM_USER_PERMISSION SET ORDERING=5021 WHERE CODE='GC_REPLACE';
UPDATE CRM_USER_PERMISSION SET ORDERING=5022 WHERE CODE='GC_GENERAL_DISCOUNT';
UPDATE CRM_USER_PERMISSION SET ORDERING=5023 WHERE CODE='POS_LOSE_MANAGEMENT';
UPDATE CRM_USER_PERMISSION SET ORDERING=5024 WHERE CODE='LC_APPROVE_BURN_CARDS';

--REPORTS
UPDATE CRM_USER_PERMISSION SET ORDERING=6001 WHERE CODE='REPORT_EMPLOYEE_REWARDS_BY_LOCATION_SUMMARY_VIEW';
UPDATE CRM_USER_PERMISSION SET ORDERING=6002 WHERE CODE='REPORT_POINTS_ISSUANCE_BY_TYPES_SUMMARY_VIEW';
UPDATE CRM_USER_PERMISSION SET ORDERING=6003 WHERE CODE='REPORT_POINTS_SUMMARY_MONTHLY_VIEW';
UPDATE CRM_USER_PERMISSION SET ORDERING=6004 WHERE CODE='REPORT_POINTS_SUMMARY_YEARLY_VIEW';
UPDATE CRM_USER_PERMISSION SET ORDERING=6005 WHERE CODE='REPORT_POINTS_SUMMARY_DAILY_VIEW';
UPDATE CRM_USER_PERMISSION SET ORDERING=6006 WHERE CODE='REPORT_MEMBER_REGISTRATION_SUMMARY_VIEW';
UPDATE CRM_USER_PERMISSION SET ORDERING=6007 WHERE CODE='REPORT_MEMBER_ACTIVITY_VIEW';
UPDATE CRM_USER_PERMISSION SET ORDERING=6008 WHERE CODE='REPORT_MEMBER_STORE_TRANSACTION_VIEW';
UPDATE CRM_USER_PERMISSION SET ORDERING=6009 WHERE CODE='REPORT_MEMBER_CARD_ACTIVATION_SUMMARY_VIEW';
UPDATE CRM_USER_PERMISSION SET ORDERING=6010 WHERE CODE='REPORT_EMPLOYEE_REGISTRATION_SUMMARY_VIEW';
UPDATE CRM_USER_PERMISSION SET ORDERING=6011 WHERE CODE='REPORT_EMPLOYEE_PURCHASE_VIEW';
UPDATE CRM_USER_PERMISSION SET ORDERING=6012 WHERE CODE='REPORT_REWARDS_SUMMARY_BY_MONTH_VIEW';
UPDATE CRM_USER_PERMISSION SET ORDERING=6013 WHERE CODE='GC_MANUFACTURE_COST_REPORT';
UPDATE CRM_USER_PERMISSION SET ORDERING=6014 WHERE CODE='REPORT_EMPLOYEE_PURCHASE_AND_REWARD_SUMMARY_VIEW';
UPDATE CRM_USER_PERMISSION SET ORDERING=6015 WHERE CODE='GC_CONSUMED_REPORT';
UPDATE CRM_USER_PERMISSION SET ORDERING=6016 WHERE CODE='GC_ACTIVATION_REPORT';
UPDATE CRM_USER_PERMISSION SET ORDERING=6017 WHERE CODE='REPORT_GC_ACCT_SALES_SUMMARY_VIEW';
UPDATE CRM_USER_PERMISSION SET ORDERING=6018 WHERE CODE='GC_REDEEM_REPORT';
UPDATE CRM_USER_PERMISSION SET ORDERING=6019 WHERE CODE='GC_AGED_ANALYSIS';
UPDATE CRM_USER_PERMISSION SET ORDERING=6020 WHERE CODE='GC_OUTSTANDING_REPORT';
UPDATE CRM_USER_PERMISSION SET ORDERING=6021 WHERE CODE='GC_EXPIRE_AND_BALANCE_REPORT';
UPDATE CRM_USER_PERMISSION SET ORDERING=6022 WHERE CODE='TRANSACTION_DETAIL_REPORT';
UPDATE CRM_USER_PERMISSION SET ORDERING=6023 WHERE CODE='TRANSACTION_SUMMARY_REPORT';
UPDATE CRM_USER_PERMISSION SET ORDERING=6024 WHERE CODE='GC_AFFILIATE_TRANSACTION_REDEMPTION';
UPDATE CRM_USER_PERMISSION SET ORDERING=6025 WHERE CODE='GC_AFFILIATE_TRANSACTION_SALES';
UPDATE CRM_USER_PERMISSION SET ORDERING=6026 WHERE CODE='REPORT_POINTS_EXPIRE';
UPDATE CRM_USER_PERMISSION SET ORDERING=6027 WHERE CODE='REPORT_POPULAR_STORES';
UPDATE CRM_USER_PERMISSION SET ORDERING=6028 WHERE CODE='GC_B2B_CUSTOMER_PAYMENT_REPORT';
UPDATE CRM_USER_PERMISSION SET ORDERING=6029 WHERE CODE='GC_DISCOUNT_RATE_REPORT';
UPDATE CRM_USER_PERMISSION SET ORDERING=6030 WHERE CODE='GC_DISCOUNT_SUMMARY_REPORT';
UPDATE CRM_USER_PERMISSION SET ORDERING=6031 WHERE CODE='GC_ABNORMAL_BALANCE_REPORT';
UPDATE CRM_USER_PERMISSION SET ORDERING=6032 WHERE CODE='GC_AVERAGE_TRANSACTION_AMT_REPORT';
UPDATE CRM_USER_PERMISSION SET ORDERING=6033 WHERE CODE='GC_ORDER_SUMMARY_REPORT';
UPDATE CRM_USER_PERMISSION SET ORDERING=6034 WHERE CODE='GC_SETTLEMENT_REPORT';
UPDATE CRM_USER_PERMISSION SET ORDERING=6035 WHERE CODE='GC_STORE_PERFORMANCE_REPORT';
UPDATE CRM_USER_PERMISSION SET ORDERING=6036 WHERE CODE='GC_ACCOUNT_RECEIVABLE_AGE';

--TREASURY
UPDATE CRM_USER_PERMISSION SET ORDERING=7001 WHERE CODE='GC_SO_VERIFY_PAYMENT';
UPDATE CRM_USER_PERMISSION SET ORDERING=7002 WHERE CODE='GC_SO_APPROVE_PAYMEN';

--UKNOWN
UPDATE CRM_USER_PERMISSION SET ORDERING=8000 WHERE CODE='CUSTOMER_HELPDESK';
