package com.transretail.crm.reconciliation.service;

import java.sql.SQLException;

/**
 * @author Allan G. Ramirez (agramirez@exist.com)
 */
public interface PointsToolService {
    String createEarnWsUrl(String txnNo) throws SQLException;
}
