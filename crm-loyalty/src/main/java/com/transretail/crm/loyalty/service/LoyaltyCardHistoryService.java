package com.transretail.crm.loyalty.service;

import java.util.List;

import org.joda.time.LocalDate;

import com.transretail.crm.common.reporting.jasper.JRProcessor;
import com.transretail.crm.common.service.dto.response.ResultList;
import com.transretail.crm.core.entity.lookup.LookupDetail;
import com.transretail.crm.loyalty.dto.LoyaltyCardTrackerDto;
import com.transretail.crm.loyalty.dto.LoyaltyCardTrackerFilterDto;
import com.transretail.crm.loyalty.dto.LoyaltyCardTrackingDto;
import com.transretail.crm.loyalty.dto.MultipleAllocationDto;
import com.transretail.crm.loyalty.entity.LoyaltyCardInventory;




public interface LoyaltyCardHistoryService {

	ResultList<LoyaltyCardTrackingDto> trackInventory(
			LoyaltyCardTrackerFilterDto filterDto);

	JRProcessor createJrProcessor(LoyaltyCardTrackerFilterDto filterDto);

	void saveHistoryForInventory(LoyaltyCardInventory inventory,
			LookupDetail status, String location);

	void saveHistoryForInventories(String startingSeries, String endingSeries,
			LookupDetail status, String location, String transferTo, String origin, Boolean track);

	void saveHistoryForInventories(String referenceNo, LookupDetail status,
			String location, String transferTo, String origin, Boolean track);

	void saveHistoryForInventories(LookupDetail status, String location,
			String transferTo, String origin, List<LoyaltyCardInventory> inventories, Boolean track);

	void saveHistoryForInventory(LoyaltyCardInventory inventory,
			LookupDetail status, String location, String transactionNo,
			Double annualFee, Double replacementFee, Boolean track);

	void saveHistoryForInventories(LookupDetail status,
			List<LoyaltyCardInventory> inventories, Boolean track);

	List<MultipleAllocationDto> getTransaction(String transactionType, String username,
			LocalDate dateFrom, LocalDate dateTo);

	ResultList<LoyaltyCardTrackerDto> trackInventory2(
			LoyaltyCardTrackerFilterDto filterDto);

	void nonAsyncSaveHistoryForInventories(String startingSeries,
			String endingSeries, LookupDetail status, String location,
			String transferTo, String origin, Boolean track);

	void nonAsyncSaveHistoryForInventories(LookupDetail status,
			String location, String transferTo, String origin,
			List<LoyaltyCardInventory> inventories, Boolean track);
	
}
