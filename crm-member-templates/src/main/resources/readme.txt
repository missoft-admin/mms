Valid genders are   
  M, F,  MALE, FEMALE, UNKNOWN if english language is selected
  LAKI-LAKI, PEREMPUAN, TIDAK DIKETAHUI if bahasa language is selected

Accepted marital statuses are   
  SINGLE, MARRIED, DIVORCE if english language is selected
  SATU, MENIKAH, PERCERAIAN if bahasa language is selected
  
Valid nationalities are 
  IND for Indonesian
  FIL for Filipino
  AME for American
  CHI for Chinese
  MALAY for Malaysian
  
Valid religions are 
  ISLAM
  CHRISTIAN
  HINDU
  JEW
  HEBREW
  BUDDHIST
  ATHEIST

Valid BUSINESSFIELD values  
  OTHER, INFORMATION TECHNOLOGY, HUMAN RESOURCES, ACADEMICS if english is selected
  LAIN, TEKNOLOGI INFORMASI, HUMAN RESOURCES, AKADEMISI if bahasa language is selected. Modify property file if translation is wrong    

Valid STATUS values
  ACTIVE, TERMINATED

Valid EMPLOYEETYPE values
  PROBATIONARY ASSOCIATE, PERMANENT ASSOCIATE, CONTRACTED ASSOCIATED FULL-TIME, CONTRACTED ASSOCIATED PART-TIME if english is selected
  ASOSIASI PERCOBAAN, ASOSIASI TETAP, DIKONTRAK ASOSIASI FULL-WAKTU, DIKONTRAK ASOSIASI PART-TIME if bahasa language is selected. Modify property file if translation is wrong