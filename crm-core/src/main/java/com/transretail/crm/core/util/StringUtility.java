package com.transretail.crm.core.util;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.List;

public final class StringUtility {

    private StringUtility() {
    }

    @SuppressWarnings({ "rawtypes", "unchecked" })
    public static List generateList(Class inGeneric, String[] inArray) {

        List theList = new ArrayList();
        for (int i = 0; i < inArray.length; i++) {
            try {
                if (inArray[i] == null || inArray[i].isEmpty()) {
                    continue;
                }
                theList.add(inGeneric.getConstructor(String.class).newInstance(
                        inArray[i]));
            } catch (IllegalArgumentException e) {
                e.printStackTrace();
            } catch (SecurityException e) {
                e.printStackTrace();
            } catch (InstantiationException e) {
                e.printStackTrace();
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            } catch (InvocationTargetException e) {
                e.printStackTrace();
            } catch (NoSuchMethodException e) {
                e.printStackTrace();
            }
        }

        return theList;
    }
}
