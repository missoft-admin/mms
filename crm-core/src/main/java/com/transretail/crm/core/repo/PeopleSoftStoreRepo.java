package com.transretail.crm.core.repo;

import com.transretail.crm.common.repo.CrmQueryDslPredicateExecutor;
import com.transretail.crm.core.entity.PeopleSoftStore;
import com.transretail.crm.core.repo.custom.PeopleSoftStoreRepoCustom;
import org.springframework.stereotype.Repository;


@Repository
public interface PeopleSoftStoreRepo extends CrmQueryDslPredicateExecutor<PeopleSoftStore, Long>, PeopleSoftStoreRepoCustom {
    
}
