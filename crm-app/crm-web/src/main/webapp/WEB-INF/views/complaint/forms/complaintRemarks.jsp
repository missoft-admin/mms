<%@include file="../../common/taglibs.jsp" %>


<div id="content_complaintRemarks">

  <div>
		<fieldset>
		  <div class="page-header3">
		    <div><h4><spring:message code="complaint_lbl_history"/></h4></div>
		  </div>
		</fieldset>
  </div>
  <div id="list_remarks" 
    data-url="<c:url value="/customer/complaint/process/remarks/list/${complaintId}" />" 
    data-hdr-date="<spring:message code="complaint_date"/>" 
    data-hdr-user="<spring:message code="complaint_lbl_user"/>" 
    data-hdr-remarks="<spring:message code="complaint_lbl_remarks"/>"
    data-hdr-status="<spring:message code="complaint_status"/>" ></div>

  <div class="data-list-content">
    <table class="table table-bordered table-striped">
      <thead>
        <tr>
          <th><spring:message code="remarks_prop_created" /></th>
          <th><spring:message code="remarks_prop_createUser" /></th>
          <th><spring:message code="remarks_prop_remarks" /></th>
          <th><spring:message code="remarks_prop_status" /></th>
        </tr>
      </thead>
      <tbody>            
        <c:forEach var="remark" items="${remarks}">
        <tr>
          <td class="table-data-01"><c:out value="${remark.created}" /></td>
          <td><c:out value="${remark.createUser}" /></td>
          <td><c:out value="${remark.remarks}" /></td>
          <td><c:out value="${remark.dispStatus}" /></td>
        </tr>
        </c:forEach>
      </tbody>
    </table>
  </div>


<link href="<spring:url value="/css/dataTables.css" />" rel="stylesheet">
<script src="<spring:url value="/js/jquery/jquery.dataTables.js" />" ></script>
<script src="<spring:url value="/js/jquery/jquery.dataTables.linkLengthMenu.js" />"></script>
<script src="<spring:url value="/js/jquery/jquery.dataTables.customPaging.js" />"></script>
<script src="<spring:url value="/js/jquery/ajax-datatable.js" />" ></script>
<script src="<spring:url value="/js/common/form2js.js" />" ></script>
<script src="<spring:url value="/js/common/json2.js" />" ></script>
<script src="<spring:url value="/js/jquery/jquery.toObject.js" />" ></script>
<script type="text/javascript">
var ComplaintRemarks = null;

$(document).ready( function() {

    var $container = $( "#content_complaintRemarks" );
    var $list = $container.find( "#list_remarks" );
  
    //initDataTable();
  
    function initDataTable() {
        $list.ajaxDataTable({
            'autoload'    : true,
            'ajaxSource'  : $list.data( "url" ),
            'columnHeaders' : [ 
                $list.data( "hdr-date" ),
                $list.data( "hdr-user" ),
                $list.data( "hdr-remarks" ),
                $list.data( "hdr-status" )
            ],
            'modelFields'  : [
                { name  : 'created', sortable : true },
                { name  : 'createUser', sortable : true },
                { name  : 'remarks', sortable : true },
                { name  : 'status', sortable : true }
            ]
        });
    }

});
</script>


</div>
