package com.transretail.crm.core.repo.custom.impl;

import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.joda.time.DateTime;
import org.joda.time.LocalDate;

import com.google.common.collect.Maps;
import com.mysema.query.Tuple;
import com.mysema.query.group.GroupBy;
import com.mysema.query.jpa.hibernate.HibernateSubQuery;
import com.mysema.query.jpa.impl.JPAQuery;
import com.mysema.query.types.ConstructorExpression;
import com.mysema.query.types.expr.BooleanExpression;
import com.transretail.crm.core.dto.ShoppingListDto;
import com.transretail.crm.core.entity.QPointsTxnModel;
import com.transretail.crm.core.entity.QPosTransaction;
import com.transretail.crm.core.entity.QPosTxItem;
import com.transretail.crm.core.entity.lookup.QProduct;
import com.transretail.crm.core.repo.custom.PosTransactionRepoCustom;

public class PosTransactionRepoImpl implements PosTransactionRepoCustom {
	@PersistenceContext
	EntityManager em;
	
	@Override
	public Double getAverageTransactions(String accountId, String productId, 
			Integer timeFrameIncludeInMonths, Integer timeFrameExcludeInDays) {
		QPosTxItem posTxItems = QPosTxItem.posTxItem;
		QPointsTxnModel points = QPointsTxnModel.pointsTxnModel;
		JPAQuery query = new JPAQuery(em);
		
		Date minTransactionDate = LocalDate.now().minusMonths(timeFrameIncludeInMonths)
				.toDateTimeAtStartOfDay().toDate();
		Date maxTransactionDate = LocalDate.now().minusDays(timeFrameExcludeInDays)
				.toDateTimeAtStartOfDay().toDate();
		
		return query.from(posTxItems)
			.where(BooleanExpression.allOf(
				posTxItems.productId.eq(productId),
				posTxItems.posTransaction.id.in(
					new HibernateSubQuery().from(points)
						.where(BooleanExpression.allOf(
							points.transactionContributer.accountId.eq(accountId),
							points.transactionDateTime.between(minTransactionDate, maxTransactionDate)))
						.list(points.transactionNo))
				)
			)
			.singleResult(posTxItems.quantity.sum().divide(posTxItems.posTransaction.countDistinct()));
	}
	
	@Override
	public List<ShoppingListDto> rankItemsByTransactions(String accountId, Integer timeFrameIncludeInMonths, 
			Integer timeFrameExcludeInDays, Integer minTransactions) {
		QPosTxItem posTxItems = QPosTxItem.posTxItem;
		QProduct products = QProduct.product;
		QPointsTxnModel points = QPointsTxnModel.pointsTxnModel;
		JPAQuery query = new JPAQuery(em);
		
		Date minTransactionDate = LocalDate.now().minusMonths(timeFrameIncludeInMonths)
				.toDateTimeAtStartOfDay().toDate();
		Date maxTransactionDate = LocalDate.now().minusDays(timeFrameExcludeInDays)
				.toDateTimeAtStartOfDay().toDate();
		
		return query.from(posTxItems)
			.where(BooleanExpression.allOf(
				posTxItems.posTransaction.id.in(
					new HibernateSubQuery().from(points)
						.where(BooleanExpression.allOf(
							points.transactionContributer.accountId.eq(accountId),
							points.transactionDateTime.between(minTransactionDate, maxTransactionDate)))
						.list(points.transactionNo))
				)
			)
			.groupBy(posTxItems.productId)
			.having(posTxItems.posTransaction.countDistinct().gt(minTransactions))
			.orderBy(posTxItems.posTransaction.countDistinct().desc(), posTxItems.quantity.sum().desc())
			.list(ConstructorExpression.create(ShoppingListDto.class, posTxItems.productId, 
					new HibernateSubQuery().from(products)
						.where(posTxItems.productId.eq(products.id))
						.unique(products.name),
					posTxItems.posTransaction.countDistinct(), posTxItems.quantity.sum())
			);
	}
	
	@Override
	public List<ShoppingListDto> rankItemsByTransactions(String accountId, Integer timeFrameIncludeInMonths, 
			Integer timeFrameExcludeInDays, Integer minTransactions, Integer limit, Integer offset) {
		QPosTxItem posTxItems = QPosTxItem.posTxItem;
		QProduct products = QProduct.product;
		QPointsTxnModel points = QPointsTxnModel.pointsTxnModel;
		JPAQuery query = new JPAQuery(em);
		
		Date minTransactionDate = LocalDate.now().minusMonths(timeFrameIncludeInMonths)
				.toDateTimeAtStartOfDay().toDate();
		Date maxTransactionDate = LocalDate.now().minusDays(timeFrameExcludeInDays)
				.toDateTimeAtStartOfDay().toDate();
		
		return query.from(posTxItems)
			.where(BooleanExpression.allOf(
				posTxItems.posTransaction.id.in(
					new HibernateSubQuery().from(points)
						.where(BooleanExpression.allOf(
							points.transactionContributer.accountId.eq(accountId),
							points.transactionDateTime.between(minTransactionDate, maxTransactionDate)))
						.list(points.transactionNo))
				)
			)
			.groupBy(posTxItems.productId)
			.having(posTxItems.posTransaction.countDistinct().gt(minTransactions))
			.orderBy(posTxItems.posTransaction.countDistinct().desc(), posTxItems.quantity.sum().desc())
			.limit(limit).offset(offset)
			.list(ConstructorExpression.create(ShoppingListDto.class, posTxItems.productId, 
					new HibernateSubQuery().from(products)
						.where(posTxItems.productId.eq(products.id))
						.unique(products.name),
					posTxItems.posTransaction.countDistinct(), posTxItems.quantity.sum())
			);
	}
	
	@Override
	public Map<String, ShoppingListDto> getShoppingSuggestions(String accountId, Integer timeFrameIncludeInMonths, 
			Integer timeFrameExcludeInDays, Integer minTransactions) {
		QPosTxItem posTxItems = QPosTxItem.posTxItem;
		QProduct products = QProduct.product;
		QPointsTxnModel points = QPointsTxnModel.pointsTxnModel;
		JPAQuery query = new JPAQuery(em);
		
		Date minTransactionDate = LocalDate.now().minusMonths(timeFrameIncludeInMonths)
				.toDateTimeAtStartOfDay().toDate();
		Date maxTransactionDate = LocalDate.now().minusDays(timeFrameExcludeInDays)
				.toDateTimeAtStartOfDay().toDate();
		Date today = DateTime.now().toDate();
		
		Map<String, ShoppingListDto> shoppingListMap = query.from(posTxItems)
			.where(BooleanExpression.allOf(
				posTxItems.posTransaction.id.in(
					new HibernateSubQuery().from(points)
						.where(BooleanExpression.allOf(
							points.transactionContributer.accountId.eq(accountId),
							points.transactionDateTime.between(minTransactionDate, maxTransactionDate)))
						.list(points.transactionNo))
				)
			)
			.groupBy(posTxItems.productId)
			.having(posTxItems.posTransaction.countDistinct().gt(minTransactions))
			.transform(GroupBy.groupBy(posTxItems.productId)
				.as(ConstructorExpression.create(ShoppingListDto.class, 
						new HibernateSubQuery().from(products)
						.where(posTxItems.productId.eq(products.id))
						.unique(products.name),
					posTxItems.quantity.sum().divide(posTxItems.posTransaction.countDistinct())))
			);
		
		query = new JPAQuery(em);
		List<Tuple> recentItems = query.from(posTxItems)
			.where(BooleanExpression.allOf(
				posTxItems.posTransaction.id.in(
					new HibernateSubQuery().from(points)
						.where(BooleanExpression.allOf(
							points.transactionContributer.accountId.eq(accountId),
							points.transactionDateTime.between(maxTransactionDate, today)))
						.list(points.transactionNo))
				)
			)
			.groupBy(posTxItems.productId)
			.orderBy(posTxItems.posTransaction.countDistinct().desc(), posTxItems.quantity.sum().desc())
			.list(posTxItems.productId, posTxItems.quantity.sum());
		
		for(Tuple tuple : recentItems) {
			String productId = tuple.get(posTxItems.productId);
			if(shoppingListMap.containsKey(productId)) {
				ShoppingListDto dto = shoppingListMap.get(productId);
				double itemDiff = dto.getAverage() - tuple.get(posTxItems.quantity.sum());
				dto.setAverage(itemDiff >= 0 ? itemDiff : 0D);
			}
		}
		
		return shoppingListMap;
	}
}
