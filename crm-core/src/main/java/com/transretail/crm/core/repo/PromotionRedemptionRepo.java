package com.transretail.crm.core.repo;


import org.springframework.stereotype.Repository;

import com.transretail.crm.common.repo.CrmQueryDslPredicateExecutor;
import com.transretail.crm.core.entity.PromotionRedemption;


@Repository
public interface PromotionRedemptionRepo extends CrmQueryDslPredicateExecutor<PromotionRedemption, Long> {
}
