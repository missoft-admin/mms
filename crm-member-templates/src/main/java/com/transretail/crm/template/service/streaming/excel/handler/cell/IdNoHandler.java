package com.transretail.crm.template.service.streaming.excel.handler.cell;

import org.apache.commons.lang3.StringUtils;
import org.springframework.context.i18n.LocaleContextHolder;

import com.transretail.crm.core.entity.MemberModel;
import com.transretail.crm.template.service.MemberTemplateParseException;
import com.transretail.crm.template.service.streaming.excel.handler.sheet.AbstractSheetHandler;

/**
 * @author Allan G. Ramirez (agramirez@exist.com)
 */
public class IdNoHandler implements CellHandler {
    private AbstractSheetHandler sheetHandler;

    public IdNoHandler(AbstractSheetHandler sheetHandler) {
        this.sheetHandler = sheetHandler;
    }

    @Override
    public void handle(MemberModel dto, String cellText) throws MemberTemplateParseException {
        if (StringUtils.isNotBlank(cellText)) {
            if (!cellText.startsWith("KTP-") && !cellText.startsWith("SIM-") && !cellText.startsWith("PASSPORT-")) {
                throw new MemberTemplateParseException(sheetHandler.getMessageSource()
                    .getMessage("member.template.messages.excel.idno.invalid", new String[]{cellText},
                        LocaleContextHolder.getLocale()));
            }
            dto.setIdNumber(cellText);
        }
    }
}
