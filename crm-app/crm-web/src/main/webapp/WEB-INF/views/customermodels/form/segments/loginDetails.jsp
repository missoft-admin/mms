<%@ include file="../../../common/taglibs.jsp" %>


  <!-- Login Details -->
  <fieldset class="form-horizontal container-fluid pull-left">
    <legend><spring:message code="label_field_logindetails" /></legend>
    <div class="control-group">
      <spring:message
        code="label_com_transretail_crm_entity_customermodel_username"
        var="username" />
      <label for="username" class="control-label"> <b
        class="required hasEmail">*</b>
      <c:out value="${username} " />
      </label>
      <div class="controls">
        <input type="text" name="username" class="form-control"
          placeholder="${username}" />
      </div>
    </div>
    <div class="control-group">
      <spring:message
        code="label_com_transretail_crm_entity_customermodel_password"
        var="password" />
      <label for="password" class="control-label"> <b
        class="required hasEmail">*</b>
      <c:out value="${password}" />
      </label>
      <div class="controls">
        <input type="password" name="password" class="form-control"
          placeholder="${password}" />
      </div>
    </div>
    <div>
      <input type="hidden" name="passwordCheck" />
    </div>
    <div class="control-group">
      <spring:message
        code="label_com_transretail_crm_entity_customermodel_pin" var="pin" />
      <label for="pin" class="control-label"><b class="required">*</b>
      <c:out value="${pin}" /> </label>
      <div class="controls">
        <form:password id="pin" path="pin" maxlength="6"
          cssClass="form-control intInput" placeholder="${pin}" />
      </div>
    </div>
  </fieldset>