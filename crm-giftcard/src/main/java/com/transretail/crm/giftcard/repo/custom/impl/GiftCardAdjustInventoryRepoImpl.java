package com.transretail.crm.giftcard.repo.custom.impl;

import com.google.common.collect.Lists;
import com.google.common.collect.Sets;
import com.mysema.query.Tuple;
import com.mysema.query.jpa.JPASubQuery;
import com.mysema.query.jpa.hibernate.HibernateSubQuery;
import com.mysema.query.jpa.hibernate.HibernateUpdateClause;
import com.mysema.query.jpa.impl.JPAQuery;
import com.mysema.query.types.expr.BooleanExpression;
import com.mysema.query.types.expr.NumberExpression;
import com.transretail.crm.common.service.dto.request.PageSortDto;
import com.transretail.crm.common.service.dto.request.PagingParam;
import com.transretail.crm.common.service.dto.response.ResultList;
import com.transretail.crm.common.service.util.SpringDataPagingUtil;
import com.transretail.crm.core.security.util.UserUtil;
import com.transretail.crm.core.service.CodePropertiesService;
import com.transretail.crm.core.service.LookupService;
import com.transretail.crm.core.service.StoreService;
import com.transretail.crm.giftcard.dto.GiftCardPhysicalCountSummaryDto;
import com.transretail.crm.giftcard.entity.*;
import com.transretail.crm.giftcard.entity.support.GiftCardInventoryStatus;
import com.transretail.crm.giftcard.repo.custom.GiftCardAdjustInventoryRepoCustom;
import java.util.Collection;
import java.util.List;
import java.util.Set;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.apache.commons.lang3.StringUtils;
import org.hibernate.StatelessSession;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import static com.transretail.crm.giftcard.entity.support.GiftCardInventoryStatus.IN_STOCK;
import static com.transretail.crm.giftcard.entity.support.GiftCardInventoryStatus.MISSING;

public class GiftCardAdjustInventoryRepoImpl implements GiftCardAdjustInventoryRepoCustom {

    @PersistenceContext
    private EntityManager em;
    @Autowired
    private CodePropertiesService codePropertiesService;
    @Autowired
    private LookupService lookupService;
    @Autowired
    private StoreService storeService;
    @Autowired
    private StatelessSession session;
    private static final Logger logger = LoggerFactory.getLogger(GiftCardAdjustInventoryRepoImpl.class);

    @Override
    @Transactional
    public void updateReceivedInventories(Collection<GiftCardPhysicalCount> physicalCounts) {
        QGiftCardInventory qInventory = QGiftCardInventory.giftCardInventory;
        final String inventoryLocation = getInventoryLocation();

        HibernateUpdateClause clause = new HibernateUpdateClause(session, qInventory)
                .set(qInventory.location, inventoryLocation)
                .set(qInventory.status, IN_STOCK);

        List<BooleanExpression> filters = countToSeriesExpression(qInventory, physicalCounts);

        BooleanExpression filter = qInventory.in(new HibernateSubQuery().from(qInventory)
                .where(seriesFilterExpression(filters)
                        .andAnyOf(qInventory.status.eq(MISSING)
                                .and(qInventory.allocateTo.isNull())
                                .and(qInventory.location.eq(inventoryLocation))/*,
                         qInventory.location.ne(inventoryLocation)*/)).list(qInventory));

        long affectedRows = clause.where(filter).execute();
        logger.debug("Updating giftcard received inventories. {} records affected.", affectedRows);
    }

    @Override
    @Transactional
    public void updateMissingInventories(Collection<GiftCardPhysicalCount> physicalCounts) {
        QGiftCardInventory qInventory = QGiftCardInventory.giftCardInventory;
        HibernateUpdateClause clause = new HibernateUpdateClause(session, qInventory)
                .set(qInventory.location, getInventoryLocation())
                .set(qInventory.status, MISSING);

        List<BooleanExpression> filters = countToSeriesExpression(qInventory, physicalCounts);

        BooleanExpression filter = qInventory.location.eq(getInventoryLocation())
                .and(qInventory.status.eq(IN_STOCK))
                .and(qInventory.allocateTo.isNull())
                .and(qInventory.notIn(new HibernateSubQuery()
                                .from(qInventory)
                                .where(seriesFilterExpression(filters))
                                .list(qInventory)));

        long affectedRows = clause.where(filter).execute();
        logger.debug("Updating giftcard received inventories. {} records affected.", affectedRows);
    }

    @Override
    public ResultList<GiftCardPhysicalCountSummaryDto> getSummary(PageSortDto sortDto) {
        QGiftCardPhysicalCount qCount = QGiftCardPhysicalCount.giftCardPhysicalCount;
        QGiftCardInventory qinv = QGiftCardInventory.giftCardInventory;

        PagingParam pagination = sortDto.getPagination();
        final String inventoryLocation = getInventoryLocation();
        BooleanExpression filter = qinv.location.eq(inventoryLocation).and(qinv.status.in(stockStatuses()));
        QProductProfile qpp = QProductProfile.productProfile;
        JPAQuery jpaquery = new JPAQuery(em).from(qinv)
                .join(qinv.profile, qpp)
                .where(filter)
                .groupBy(qpp.productCode, qpp.productDesc, qinv.location);

        long totalCount = jpaquery.list(qpp.productCode, qpp.productDesc, qinv.location).size();
        SpringDataPagingUtil.INSTANCE.applyPagination(jpaquery, pagination, GiftCardPhysicalCount.class);

        List<Tuple> tuples = jpaquery.list(
                qpp.productCode, qpp.productDesc,
                qinv.location, qinv.count());

        List<GiftCardPhysicalCountSummaryDto> summaries = Lists.newArrayList();

        for (Tuple tuple : tuples) {
            GiftCardPhysicalCountSummaryDto summary = new GiftCardPhysicalCountSummaryDto();
            summary.setLocation(tuple.get(qinv.location));
            if (StringUtils.isNotBlank(summary.getLocation())) {
                if (summary.getLocation().equalsIgnoreCase(codePropertiesService.getDetailInvLocationHeadOffice())) {
                    summary.setLocationName(lookupService.getDetailByCode(summary.getLocation()).getDescription());
                } else {
                    summary.setLocationName(storeService.getStoreByCode(summary.getLocation()).getName());
                }
            }
            summary.setProductProfile(tuple.get(qpp.productCode));
            summary.setProductProfileName(tuple.get(qpp.productDesc));
            summary.setCurrentInventory(tuple.get(qinv.count()));
            final String productProfile = summary.getProductProfile();
            List<Tuple> list = new JPAQuery(em)
                    .from(qCount)
                    .where(qCount.location.eq(summary.getLocation())
                            .and(qCount.productProfile.eq(productProfile)))
                    .list(qCount.startingSeries, qCount.endingSeries, qCount.quantity);

            Long physicalCount = new JPAQuery(em)
                    .from(qCount).where(qCount.location.eq(summary.getLocation()))
                    .groupBy(qCount.productProfile)
                    .having(qCount.productProfile.eq(summary.getProductProfile()))
                    .singleResult(qCount.quantity.sum());

            List<BooleanExpression> seriesFilter = Lists.newArrayList();
            for (Tuple t : list) {
                long starting = Long.parseLong(t.get(qCount.startingSeries));
                long ending = Long.parseLong(t.get(qCount.endingSeries));

                seriesFilter.add(qinv.series.goe(starting).and(qinv.series.loe(ending)));
            }

            QGiftCardInventory inventoryInCount = QGiftCardInventory.giftCardInventory;

            if (!seriesFilter.isEmpty()) {
                List<String> missingCards = new JPAQuery(em).from(qinv)
                        .where(qinv.location.eq(inventoryLocation)
                                .and(qinv.status.in(IN_STOCK))
                                .and(qinv.allocateTo.isNull())
                                .and(qinv.productCode.eq(productProfile))
                                .and(qinv.notIn(new JPASubQuery().from(inventoryInCount)
                                                .where(inventoryInCount.location.eq(inventoryLocation)
                                                        .and(inventoryInCount.status.in(IN_STOCK))
                                                        .and(inventoryInCount.productCode.eq(productProfile))
                                                        .and(inventoryInCount.allocateTo.isNull())
                                                        .and(anyOfExpression(seriesFilter)))
                                                .list(inventoryInCount))))
                        .list(qinv.barcode);

                List<String> foundCards = new JPAQuery(em).from(qinv)
                        .where(qinv.location.eq(inventoryLocation)
                                .and(qinv.status.eq(MISSING))
                                .and(qinv.allocateTo.isNull())
                                .and(qinv.productCode.eq(productProfile))
                                .and(qinv.in(new JPASubQuery().from(inventoryInCount)
                                                .where(inventoryInCount.location.eq(inventoryLocation)
                                                        .and(inventoryInCount.status.in(MISSING))
                                                        .and(inventoryInCount.productCode.eq(productProfile))
                                                        .and(inventoryInCount.allocateTo.isNull())
                                                        .and(anyOfExpression(seriesFilter)))
                                                .list(inventoryInCount)))).list(qinv.barcode);

                summary.setFoundItems(foundCards);
                summary.setMissingItems(missingCards);
            } else {
                List<String> missingItems = new JPAQuery(em).from(qinv)
                        .where(qinv.location.eq(inventoryLocation)
                                .and(qinv.status.in(IN_STOCK, MISSING))
                                .and(qinv.allocateTo.isNull())
                                .and(qinv.productCode.eq(productProfile)))
                        .list(qinv.barcode);
                summary.setMissingItems(missingItems);
            }

            summary.setPhysicalCount(physicalCount == null ? 0L : physicalCount);

            summaries.add(summary);
        }

        return new ResultList<GiftCardPhysicalCountSummaryDto>(summaries, totalCount,
                pagination.getPageNo(), pagination.getPageSize());
    }

    private BooleanExpression anyOfExpression(List<BooleanExpression> predicates) {
        return BooleanExpression.anyOf(predicates.toArray(new BooleanExpression[predicates.size()]));
    }

    private List<BooleanExpression> countToSeriesExpression(QGiftCardInventory qInventory, Collection<GiftCardPhysicalCount> physicalCounts) throws NumberFormatException {
        List<BooleanExpression> filters = Lists.newArrayList();
        NumberExpression<Long> seriesExp = qInventory.series;
        for (GiftCardPhysicalCount count : physicalCounts) {
            Long starting = Long.valueOf(count.getStartingSeries());
            Long ending = Long.valueOf(count.getEndingSeries());
            filters.add(seriesExp.goe(starting).and(seriesExp.loe(ending)));
        }
        return filters;
    }

    private BooleanExpression seriesFilterExpression(List<BooleanExpression> expressions) {
        return BooleanExpression.anyOf(expressions.toArray(new BooleanExpression[expressions.size()]));
    }

    private String getInventoryLocation() {
        return UserUtil.getCurrentUser().getInventoryLocation();
    }

    private GiftCardInventoryStatus[] stockStatuses() {
        return new GiftCardInventoryStatus[]{IN_STOCK, MISSING};
    }

    @Override
    public Set<String> getStoreCodeIn(String startingSeries, String endingSeries) {
        QGiftCardInventory qgci = QGiftCardInventory.giftCardInventory;
        List<String> result = new JPAQuery(em).from(qgci).distinct()
                .where(qgci.series.in(Long.valueOf(startingSeries), Long.valueOf(endingSeries)))
                .list(qgci.location);
        return Sets.newHashSet(result);
    }
}
