package com.transretail.crm.web.controller.util;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;

import javax.validation.constraints.NotNull;

import org.springframework.context.MessageSource;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ValidationUtils;
import org.springframework.web.servlet.support.RequestContext;

import com.transretail.crm.core.entity.MemberModel;
import com.transretail.crm.core.entity.UserModel;

public class ControllerUtil {
	public static final String ITS_DEFAULT_MESSAGE = "An error occurred";

    public static void validateUser(BindingResult inError, RequestContext inCtx) {
        ValidationUtils.rejectIfEmpty(inError, "username", inCtx.getMessage("model_property_notempty", 
                new String[]{inCtx.getMessage("model_property_username")}, ITS_DEFAULT_MESSAGE));
        ValidationUtils.rejectIfEmpty(inError, "password",  inCtx.getMessage("model_property_notempty", 
                new String[]{inCtx.getMessage("model_property_password")}, ITS_DEFAULT_MESSAGE));
	}

    public static boolean isUpdated(UserModel inUser, String username) {
    	if(inUser == null || username == null) {
    		return false;
    	}
    	if(!inUser.getUsername().trim().equalsIgnoreCase(username.trim())) {
    		return true;
    	}
    	return false;
    }
    
    public static void validateMember(BindingResult inError, RequestContext inCtx) {
    	/*ValidationUtils.rejectIfEmpty(inError, "username", inCtx.getMessage("model_property_notempty", 
                new String[]{inCtx.getMessage("model_property_username")}, ITS_DEFAULT_MESSAGE));
        ValidationUtils.rejectIfEmpty(inError, "password",  inCtx.getMessage("model_property_notempty", 
                new String[]{inCtx.getMessage("model_property_password")}, ITS_DEFAULT_MESSAGE));*/
        ValidationUtils.rejectIfEmpty(inError, "pin",  inCtx.getMessage("model_property_notempty_pin", ITS_DEFAULT_MESSAGE));
        ValidationUtils.rejectIfEmpty(inError, "contact", inCtx.getMessage("model_property_notempty", 
                new String[]{inCtx.getMessage("model_property_contact")}, ITS_DEFAULT_MESSAGE));
        ValidationUtils.rejectIfEmpty(inError, "firstName",  inCtx.getMessage("model_property_notempty", 
                new String[]{inCtx.getMessage("model_property_firstname")}, ITS_DEFAULT_MESSAGE));
        ValidationUtils.rejectIfEmpty(inError, "lastName",  inCtx.getMessage("model_property_notempty", 
                new String[]{inCtx.getMessage("model_property_lastname")}, ITS_DEFAULT_MESSAGE));
        ValidationUtils.rejectIfEmpty(inError, "memberType",  inCtx.getMessage("model_property_membertype_empty", ITS_DEFAULT_MESSAGE));
	}

    
	/*public static boolean atLeastOneOfThemIsNotEmpty(Object o) {
    	   for(Field field : getFieldsAnnotatedWith(AtLeastOneOfThem.class, o) {
    	      if (field.get() != null && !field.get().empty()) {
    	         return true;
    	      }
    	   }
    	   return false;
    	}*/
    
	public static List<String> validate(Object inObj, MessageSource inMsgResource) {
		
		List<String> theErrors = new ArrayList<String>();
		
		Field[] fields = inObj.getClass().getDeclaredFields();
		for (int i = 0; i < fields.length; i++) {
			NotNull annotations = (NotNull) fields[i].getAnnotation(NotNull.class);
			if (annotations != null) {
				try {
					fields[i].setAccessible(true);
					/*String fieldStr = fields[i].toString();PropertyUtils.getProperty(inObj, );*/
					Object ob = fields[i].get(inObj);
					if ( ob == null || ob.toString().isEmpty() ) {
						theErrors.add(/*((NotNull) annotations).message()*/inMsgResource.getMessage("NotNull", new String[]{fields[i].toString()}, null));
					}
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}
		return theErrors;
	}

    public static void validateDuplicate(MemberModel inModel) {
    	
    }
}
