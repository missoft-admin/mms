package com.transretail.crm.giftcard.dto;

/**
 * @author ftopico
 */
public class StockRequestReceiveDto {

    private Integer quantity;
    private Long startingSeries;
    private Long endingSeries;
    private String productCode;
    private String productDesc;
    private String boxNo;
    
    public StockRequestReceiveDto() {
        
    }
    
    public Integer getQuantity() {
        return quantity;
    }
    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }
    public Long getStartingSeries() {
        return startingSeries;
    }
    public void setStartingSeries(Long startingSeries) {
        this.startingSeries = startingSeries;
    }
    public Long getEndingSeries() {
        return endingSeries;
    }
    public void setEndingSeries(Long endingSeries) {
        this.endingSeries = endingSeries;
    }
    public String getBoxNo() {
        return boxNo;
    }
    public void setBoxNo(String boxNo) {
        this.boxNo = boxNo;
    }

    public String getProductCode() {
        return productCode;
    }

    public void setProductCode(String productCode) {
        this.productCode = productCode;
    }

    public String getProductDesc() {
        return productDesc;
    }

    public void setProductDesc(String productDesc) {
        this.productDesc = productDesc;
    }
    
    
}
