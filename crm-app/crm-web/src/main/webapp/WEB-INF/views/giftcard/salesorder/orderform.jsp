<%@ include file="../../common/taglibs.jsp" %>

<style type="text/css">
<!--

  #productTable tr td:last-child {
    border-left: 0px;
  }

  #addProductBtn.btn-small {
    margin-bottom: 5px;
  }
  table select, textarea, table input[type="text"], input[type="password"], input[type="datetime"], input[type="datetime-local"], input[type="date"], input[type="month"], input[type="time"], input[type="week"], input[type="number"], input[type="email"], input[type="url"], input[type="search"], input[type="tel"], input[type="color"], .uneditable-input {
      margin-bottom: 0px;
      margin-top: 0px;
  }

-->
</style>
<script src="<spring:url value="/js/common/typeaheadutil.js" />"></script>



<c:set var="ENCTYPE" value="application/x-www-form-urlencoded"/>
<c:set var="freeSo" value="${orderForm.orderType == 'YEARLY_DISCOUNT' || orderForm.orderType == 'VOUCHER' }" />

<div class="modal-dialog">
  <div class="modal-content">

    <div class="modal-header">
      <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
      <h4>
      <c:if test="${orderForm.orderType == 'B2B_SALES' || orderForm.orderType == 'B2B_ADV_SALES' || orderForm.orderType == 'VOUCHER'}"><spring:message code="sales_order"/></c:if>
      <c:if test="${orderForm.orderType == 'YEARLY_DISCOUNT'}"><spring:message code="yearly_discount"/></c:if>
      <c:if test="${orderForm.orderType == 'INTERNAL_ORDER'}"><spring:message code="internal_order"/></c:if>
      <c:if test="${orderForm.orderType == 'REPLACEMENT'}"><spring:message code="gc_replacement"/></c:if>
      </h4>
    </div>

    <div class="modal-body">

      <div id="contentError" class="hide alert alert-error">
        <button type="button" class="close" onclick="$(this).parent().hide();">&times;</button>
        <div><form:errors path="*"/></div>
      </div>


      <form:form id="orderForm" name="orderForm" modelAttribute="orderForm"
                 action="${pageContext.request.contextPath}/giftcard/salesorder/save/"
                 method="POST" enctype="${ENCTYPE}" cssClass="form-reset form-horizontal" >

          <form:hidden path="id"/>
          <form:hidden path="orderType"/>
          <c:if test="${freeSo}">
              <form:hidden path="parentSoAmt" />
              <form:hidden path="parentVoucherVal" />
              <form:hidden path="parentFaceAmt" />
          </c:if>
          <div class="contentMain">
              <div class="row-fluid">
                  <div class="span6">
                      <div class="control-group">
                          <spring:message code="sales_order_no" var="orderNum" />
                          <label for="orderNo" class="control-label">${orderNum}</label>
                          <div class="controls">
                              <form:input path="orderNo" class="form-control" placeholder="${orderNum}" readonly="true" />
                          </div>
                      </div>
                      <div class="control-group">
                          <spring:message code="sales_order_type" var="orderTyp" />
                          <label for="orderNo" class="control-label">${orderTyp}</label>
                          <div class="controls">
                              <input type="text" value="${orderForm.orderType}" class="form-control" disabled="disabled" />
                          </div>
                      </div>
                      <div class="control-group">
                          <spring:message code="sales_order_date" var="orderDt" />
                          <label for="orderDate" class="control-label"><b class="required">*</b>${orderDt}</label>
                          <div class="controls">
                              <form:input path="orderDate" class="form-control orderDate" placeholder="${orderDt}" id="orderDateField" />
                          </div>
                      </div>
                      <div class="control-group">
                          <spring:message code="sales_order_notes" var="notes" />
                          <label for="customerId" class="control-label">${notes}</label>
                          <div class="controls">
                              <form:textarea path="notes" class="form-control"  />
                          </div>
                      </div>
                      <c:if test="${orderForm.orderType == 'INTERNAL_ORDER'}">
                          <div class="control-group">
                              <spring:message code="sales_order_category" var="category" />
                              <label for="category" class="control-label">${category}</label>
                              <div class="controls">
                                  <form:select path="category" items="${categories}" itemLabel="description" itemValue="code" class="form-control"  />
                              </div>
                          </div>
                      </c:if>
                      <c:if test="${orderForm.orderType == 'B2B_SALES' || orderForm.orderType == 'B2B_ADV_SALES' || orderForm.orderType == 'YEARLY_DISCOUNT'}">
                          <div class="control-group" id="discountTypeContainer">
                              <div class="controls">
                                  <c:forEach items="${discTypes}" var="item">
                                      <div class="radio">
                                          <form:radiobutton path="discType" value="${item}"/><label style="text-align:left"><spring:message code="sales_order_${item}" /></label>
                                      </div>
                                  </c:forEach>
                              </div>
                          </div>
                      </c:if>
                      <c:if test="${orderForm.orderType == 'B2B_SALES' || orderForm.orderType == 'B2B_ADV_SALES' || orderForm.orderType == 'REPLACEMENT'}">
                          <div class="control-group">
                              <spring:message code="gc.so.delivery.shippingfee" var="shippingFee" />
                              <label for="shippingFee" class="control-label">${shippingFee}</label>
                              <div class="controls">
                                  <form:input path="shippingFee" class="form-control" placeholder="${shippingFee}" />
                              </div>
                          </div>
                      </c:if>
                  </div>
                  <div class="span6">
                  	 <c:if test="${orderForm.orderType == 'B2B_SALES' || orderForm.orderType == 'B2B_ADV_SALES' || orderForm.orderType == 'VOUCHER'|| orderForm.orderType == 'INTERNAL_ORDER'}">
                          <div class="control-group ${orderForm.orderType == 'VOUCHER' ? 'hide' : ''} ">
                              <spring:message code="sales_order_payment_store" var="paymentStore" />
                              <label for="paymentStore" class="control-label"><b class="required">*</b>${paymentStore}</label>
                              <div class="controls">
                                <%-- <form:select path="paymentStore" items="${stores}" itemLabel="codeAndName" itemValue="code" class="form-control" /> --%>
                                <div>
                                    <form:input path="paymentStoreName" id="paymentStoreName" cssClass="form-control input-medium" />
                                    <form:hidden path="paymentStore" id="paymentStore"/>
                                </div>
                              </div>
                          </div>
                      </c:if>
                      <div class="control-group">
                          <spring:message code="sales_order_customer" var="customer" />
                          <label for="customerId" class="control-label"><b class="required">*</b>${customer}</label>
                          <div class="controls">
                                  <%-- <select class="form-control" id="customerIdField">
                                        <option></option>
                                  <c:forEach items="${customers}" var="item">
                                    <option value="${item.id}" ${item.id == orderForm.customerId ? "selected" : ""} label="${item.name}" data-contact-fullname="${item.contactFullName}" data-contact-no="${item.contactNo}" data-email="${item.email}">${item.name}</option>
                                    <form:option value="${item.id}" label="${item.name}" data-contact-fullname="${item.contactFullName}" data-contact-no="${item.contactNo}" data-email="${item.email}"/>
                                  </c:forEach>
                                    </select> --%>
                              <div>
                                  <div class="input-group">
                                      <form:input path="customerDesc" id="typeahead" cssClass="form-control input-medium" />
                                      <!-- <span class="input-group-addon">.00</span> -->
                                      <button id="customerSelectPopup" class="btn btn-small btn-primary" type='button'>
                                          <i class="icon-search icon-white"></i>
                                      </button>
                                  </div>
                                  <form:hidden path="customerId" id="customerIdField"/>
                              </div>
                          </div>
                      </div>

                      <div class="control-group">
                          <spring:message code="sales_order_contact_person" var="contactPerson" />
                          <label for="customerId" class="control-label">${contactPerson}</label>
                          <div class="controls">
                            <form:input path="contactPerson" id="contactPerson" class="form-control" placeholder="${contactPerson}" />
                          </div>
                      </div>

                      <div class="control-group">
                          <spring:message code="sales_order_contact_number" var="contactNumber" />
                          <label for="customerId" class="control-label">${contactNumber}</label>
                          <div class="controls">
                            <form:input path="contactNumber" id="contactNumber" class="form-control" placeholder="${contactNumber}" />
                          </div>
                      </div>

                      <div class="control-group">
                          <spring:message code="sales_order_email" var="email" />
                          <label for="customerId" class="control-label">${email}</label>
                          <div class="controls">
                            <form:input path="contactEmail" id="contactEmail" class="form-control" placeholder="${email}" />
                          </div>
                      </div>

                      <div class="control-group">
                          <spring:message code="order_total_face_amount" var="totalFaceAmount" />
                          <label for="totalFaceAmount" class="control-label">${totalFaceAmount}</label>
                          <div class="controls">
                            <form:input path="totalFaceAmountFmt" id="totalFaceAmountFmt" class="form-control" placeholder="${totalFaceAmount}" disabled="true" />
                            <form:hidden path="totalFaceAmount" id="totalFaceAmount" />
                          </div>
                      </div>
                      <c:if test="${orderForm.orderType == 'B2B_SALES' || orderForm.orderType == 'B2B_ADV_SALES'}">

                          <div class="control-group">
                              <spring:message code="order_discount" var="discount" />
                              <label for="discount" class="control-label">${discount}</label>
                              <div class="controls">
                                <input type="text" id="discountFmt" value="0" disabled="disabled" />
                              </div>
                          </div>
                      </c:if>
                  </div>
                  <div class="clearfix"></div>
                  <div class="span12">
                      <div class="pull-right" style="">
                          <button type="button" id="addProductBtn" class="btn btn-small">+</button><br/>
                          <button type="button" id="removeProductBtn" class="btn btn-small">&ndash;</button>
                      </div>
                      <div class="pull-left mb20" style="width: 94%; overflow: auto;">
                          <table id="productTable" class="table table-condensed table-compressed table-striped">
                              <thead>
                              <tr>
                                  <th></th>
                                        <th><b class="required">*</b><spring:message code="sales_order_item_product_name" /></th>
                                        <th><spring:message code="sales_order_item_face_value" /></th>
                                        <th><spring:message code="sales_order_item_quantity" /></th>
                                        <th><spring:message code="sales_order_item_face_amount" /></th>
                                        <th><spring:message code="sales_order_item_print_fee" /></th>
                                  <th></th>
                              </tr>
                              </thead>
                              <tbody>
                              <c:forEach var="item" items="${orderForm.items}" varStatus="status" >
                                  <tr data-index="${status.index}">
                                      <td><input type="checkbox" class="productRemoveFlag" /></td>
                                      <td class="forTypeAhead">
                                       <select class="productId" name="items[${status.index}].productId">
                                          <option></option>
                                        <c:forEach items="${profiles}" var="profile" >
                                          <option value="${profile.id}"
                                                  data-face-value="${profile.faceValue.description}"
                                                  data-unit-cost="${profile.unitCost == null ? 0 : profile.unitCost}"
                                                  <c:if test="${profile.id == item.productId}">selected</c:if>>${profile.productDesc}
                                          </option>
                                        </c:forEach>
                                      </select>
                                          <div>
                                              <%--<form:input path="items[${status.index}].productDesc" id="profileDesc${status.index}" cssClass="productDesc" />--%>
                                              <%--<form:hidden path="items[${status.index}].productId" id="profileId${status.index}" class="input-small productId"--%>
                                                           <%--data-unit-cost="${item.unitCost}" data-face-value="${item.faceValue}" />--%>
                                             <%-- <script type='text/javascript'>
                                                  $(document).ready( function() {
                                                      var url = '<c:url value="/gc/productprofile/list/" />';
                                                      var $profileDesc = $( "#productTable" ).find( "input[name='items[${status.index}].productDesc']" );
                                                      var $hiddenId = $( "#productTable" ).find( "input[name='items[${status.index}].productId']" );//$( "#profileId${status.index}" );
                                                      TypeAhead.process( $profileDesc, $hiddenId, url, { label: "productDesc", value: "id" }, [ "faceValueDesc", "unitCost" ],
                                                          function( $item ) {
                                                              $hiddenId.data( "face-value", $item[ "faceValueDesc" ] );
                                                              $hiddenId.data( "unit-cost", $item[ "unitCost" ] );
                                                              $hiddenId.change();
                                                      });
                                                  });
                                                  </script>--%>
                                          </div>
                                      </td>
                                      <td>
                                          <input type="text" class="input-small faceValueFmt" value="${item.faceValueFmt}" readonly="readonly" />
                                          <input type="hidden" class="faceValue" value="${item.faceValue}" name="items[${status.index}].faceValue" />
                                      </td>
                                      <td>
                                          <input type="text" class="input-mini quantity" value="${item.quantity}" name="items[${status.index}].quantity" /></td>
                                      <td>
                                          <input type="text" class="input-medium faceAmountFmt" value="${item.faceAmountFmt}" readonly="readonly" />
                                          <input type="hidden" class="faceAmount" value="${item.faceAmount}" name="items[${status.index}].faceAmount" />
                                      </td>
                                      <td>
                                          <input type="text" class="input-mini printFee" ${item.freePrintFee == true || freeSo ? "readonly" : ""} value="${freeSo ? 0 : item.printFee}" name="items[${status.index}].printFee" /></td>
                                      <td>
                                          <div class="checkbox">
                                              <input type="checkbox" ${item.freePrintFee == true || freeSo ? "checked" : ""} ${freeSo ? "disabled" : ""} class="freePrintFee" name="items[${status.index}].freePrintFee" />
                                              <label class="" style="text-align:left"><spring:message code="order_item_free_print_fee" /></label>
                                          </div>
                                      </td>
                                  </tr>
                              </c:forEach>
                              </tbody>
                          </table>
                      </div>
                  </div>
              </div>
          </div>
      </form:form>
    </div>
    <div class="modal-footer">
      <button type="button" data-status="FOR_APPROVAL" class="orderSubmit btn btn-primary"><spring:message code="label_submit_for_approval" /></button>
      <button type="button" data-status="DRAFT" class="orderSubmit btn btn-primary"><spring:message code="label_draft" /></button>
      <button type="button" id="orderCancel" class="btn btn-default" data-dismiss="modal"><spring:message code="label_cancel"/></button>
    </div>
  </div>
</div>

<script>
$(document).ready(function() {




  var CONTAINER_ERROR = "#contentError > div";
  var CONTENT_ERROR = "#contentError";
  $form = $("#orderForm");
  $orderDialog = $("#orderDialog").css({"width": "900px"});

  $(".orderSubmit").click(submitForm);

  function submitForm() {
    $(".orderSubmit").prop( "disabled", true );
    $.post($form.attr("action") + $(this).data("status"), $form.serialize(), function(data) {
      $(".orderSubmit").prop( "disabled", false );
      if (data.success) {
    	  reloadOrderTable();
      }
      else {
        errorInfo = "";
        for (i = 0; i < data.result.length; i++) {
          errorInfo += "<br>" + (i + 1) + ". "
              + ( data.result[i].code != undefined ?
                  data.result[i].code : data.result[i]);
        }
        $orderDialog.find(CONTAINER_ERROR).html("Please correct following errors: " + errorInfo);
        $orderDialog.find(CONTENT_ERROR).show('slow');
      }
    });
  }
  	$("input.paymentDate").datepicker({
		format: 'dd-mm-yyyy',
		autoclose: true
	});

    $("input.orderDate").datepicker({
		format: 'dd-mm-yyyy',
		autoclose: true
	});
	<c:if test="${empty orderForm.orderDate}">
	$("input.orderDate").datepicker("setDate", new Date());
	</c:if>

    var $productTable = $('#productTable');

	var  $productTbody = $("tbody", $productTable);

	$('#removeProductBtn', $orderDialog ).click(function() {
        $("input.productRemoveFlag:checked").each(function() {
            var $tr = $(this).closest("tr");
            var length = $productTbody.find("tr").length;
            if($tr.is(":first-child") && length == 1) {
                $tr.find("input.productRemoveFlag").prop('checked', false);
                $tr.find("input").removeAttr('value').prop('disabled', true);
                $tr.find("select").removeAttr('value').prop('disabled', true);
                $tr.invisible();
            } else
                $tr.remove();
        });
        updateTotalFceAmt();
    });

    $('#addProductBtn', $orderDialog ).click(function() {
    	  var $tbody = $("tbody", '#productTable');
        var $tr = $tbody.find("tr:first-child");

        if( $tr.css( 'visibility' ) == 'hidden' || $tr.css( 'display' ) == 'none' ) {
            $tr.find("input").removeAttr('value').prop('disabled', false).prop('checked', false);
            $tr.find("select").removeAttr('value').prop('disabled', false);
            $tr.visible();
        }
        else {
            /* var $row = $tr.clone();
            $row.find("input").removeAttr('value').removeAttr("checked").prop('disabled', false);
            $row.find("select").removeAttr('value').prop('disabled', false);

            var index = $tbody.find( "tr" ).length;
            var lastIndex = index - 1;
            var rg = new RegExp("\\["+lastIndex+"\\]", 'g');
            var html = $row.html();
            html = html.replace(rg, '['+index+']');
            $row.html( html );
            $row.data( "index", index );
            initFields( $row );
            $productTbody.append( $row ); */


            var $trLast = $productTbody.find("tr:last-child");
            var  $row = $tr.clone();
            $row.find("input").removeAttr('value').removeAttr("checked").prop('disabled', false);
            $row.find("select").removeAttr('value').prop('disabled', false);
            var index = +$trLast.data("index") + 1;
            var lastIndex = $tr.data("index");
            $row.data("index", index);
            var rg = new RegExp("\\["+lastIndex+"\\]", 'g');
            var html = $row.html();
            html = html.replace(rg, '['+index+']');
            $row.html(html);

            $productTbody.append($row);
            initFields($row);
        }
    });

    $("#customerIdField")
    	.change(populateContactField);

    $("#orderDateField, #customerIdField").change(toggleDiscountType);

    toggleDiscountType();

    function toggleDiscountType() {
    	var orderDate = $("#orderDateField").val();
    	var custId = $("#customerIdField").val();
    	if(orderDate != "" && custId != "") {
    		$.get("<c:url value="/giftcard/discountscheme/get/" />" + custId + "/" + orderDate, function(data) {
    	  		var result = data.result;
    	  		$container = $("#discountTypeContainer");
    	  		if(result == null || result == 'SO_YEARLY_DISCOUNT') {
    	  			$container.show();
    	  		} else {
    	  			$container.hide();
    	  			$("input[type='radio']", $container).prop("checked", false);
    	  		}
    	  	}, "json");
    	}
    }

    function populateContactField() {
    	$select = $( "#customerIdField" );//$("option:selected", $(this));//
    	/* $("#contactField").val($select.data("contactFullname") + " - " + $select.data("contactNo") + " - " + $select.data("email")); */
    	$("#contactPerson").val($select.data("contactFullname"));
    	$("#contactNumber").val($select.data("contactNo"));
    	$("#contactEmail").val($select.data("email"));
    }

    initFields($orderDialog);

	jQuery.fn.visible = function() {
	    return this.css('visibility', 'visible');
	};

	jQuery.fn.invisible = function() {
	    return this.css('visibility', 'hidden');
	};

	$("input[name='discType']").click(function() {
		updateDiscount();
	});

	$("input[name='customerId'], input[name='orderDate']").change(function() {
		updateDiscount();
	});

	function updateTotalFceAmt() {
		var totalFceAmt = 0;
		$(".faceAmount").each(function() {
			totalFceAmt += +$(this).val();
		});
		$("#totalFaceAmountFmt").val(toCurrency(totalFceAmt));
		$("#totalFaceAmount").val(totalFceAmt);

		updateDiscount();
	}

	updateDiscount();

	function updateDiscount() {
		var discType = $("input[name='discType']:checked").val();
		var custId = $("input[name='customerId']").val();
		var orderDate = $("input[name='orderDate']").val();
		var totalFceAmt = $("#totalFaceAmount").val();
		if(discType != null && custId != null && orderDate != null && totalFceAmt != null && totalFceAmt != 0) {
			$.get("<c:url value="/giftcard/discountscheme/get/beforeapproval/" />" + custId + "/" + orderDate + "/" + totalFceAmt + "/" + discType, function(data) {
		  		var result = data.result;

		  		if(result != null && result.discount != null) {
		  			var discount = result.discount;
		  			$("#discountFmt").val(toCurrency(totalFceAmt * discount / 100));
		  		}
		  	}, "json");
		}
	}

	function toCurrency(total) {
		var currency = parseFloat(total, 10).toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, "$1,").toString();
		return currency.slice( 0,-3 );
	}

	function initFields(e) {
    	$('.quantity', $(e))
    		.numberInput({"type" : "int"})
    		.change(function() {
    			$parent = $(this).closest("tr");
    			var faceAmount = $(".faceValue", $parent).val() * $(this).val();
    			$(".faceAmount", $parent).val(faceAmount);
    			$(".faceAmountFmt", $parent).val(toCurrency(faceAmount));
				<%--<c:if test="${!freeSo}">
                $(".printFee", $parent).val($( ".productId", $parent).data("unitCost") * $(this).val());
                </c:if>--%>

    			$(".printFee", $parent).val($(".productId option:selected", $parent).data("unitCost") * $(this).val());
    			updateTotalFceAmt();
    		});
        $('.intInput', $(e)).numberInput({"type" : "int"});
    	$('.floatInput', $(e)).numberInput({"type" : "float"});

    	$(".productId", $(e)).change(function() {
    		$parent = $(this).closest("tr");
    		var faceVal = $("option:selected", $(this)).data("faceValue"); //$(this).data("faceValue");
    		$(".faceValue", $(this).closest("tr")).val(faceVal);
    		$(".faceValueFmt", $(this).closest("tr")).val(toCurrency(faceVal));
    		var quantity = $('.quantity', $parent).val();
    		if(quantity != "" && faceVal != "") {
    			$('.quantity', $parent).change();
    		}
    	});

    	$(".freePrintFee", $(e)).click(function() {
    		$parent = $(this).closest("tr");
    		if($(this).is(":checked")) {
    			$(".printFee", $parent).prop("readonly", "readonly").val("0");
    		} else {
    			$(".printFee", $parent)
    				.prop("readonly", "")
    				//.val($(".productId option:selected", $parent).data("unitCost") * $(".quantity", $parent).val());
    			  .val( $( ".productId", $parent).data( "unitCost" ) * $( ".quantity", $parent ).val() );
    		}

    	});

    	var url = '<c:url value="/gc/productprofile/list/" />';
    	var $hiddenId = $(e).find( ".productId" );
      TypeAhead.process( $(e).find( ".productDesc" ), $hiddenId, url, { label: "productDesc", value: "id" },
          [ "faceValueDesc", "unitCost" ],
          function( $item ) {
              $hiddenId.data( "face-value", $item[ "faceValueDesc" ] );
              $hiddenId.data( "unit-cost", $item[ "unitCost" ] );
              $hiddenId.change();
      });


    }

	var $paymentStore = $( "#paymentStore" );
	var url = '<c:url value="/store/peoplesoft/list/gcallowed/" />';
	TypeAhead.process( $( "#paymentStoreName" ), $paymentStore, url, { label: "codeAndName", value: "code" },["code"],function($item){

		var url = '<c:url value="/giftcard/customer/profile/listbox/${orderForm.orderType}/" />'+$item["code"]+"/";
		var $customerId = $( "#customerIdField" );
		TypeAhead.process( $( "#typeahead" ), $customerId, url, { label: "name", value: "id" },[ "contactFullName", "contactNo", "email" ], function( $item ) {
			$customerId.data( "contact-fullname", $item[ "contactFullName" ] );
			$customerId.data( "contact-no", $item[ "contactNo" ] );
			$customerId.data( "email", $item[ "email" ] );
			$customerId.change();
		});

		$("#customerSelectPopup").selectPopup({
			columnHeaders : [
			           	      "<spring:message code="gc.cxprofile.cx.id"/>",
			        	      "<spring:message code="gc.cxprofile.cx.name"/>",
			        	      "<spring:message code="gc.cxprofile.contact.person"/>",
			        	      "<spring:message code="gc.cxprofile.cx.type"/>"
				        	    ],

	     	    modelFields : [
			         	      {name : 'customerId', sortable : true},
			        	      {name : 'name', sortable: true},
			        	      {name : 'contactFullName', sortable: true},
			        	      {name : 'customerType', sortable: true}
			        	      ],
	   	    objProps : ["id", "name","customerId", "contactFullName", "contactNo", "email"],
	   	    url :"<c:url value="/giftcard/customer/profile/list/${orderForm.orderType}/" />"+$item["code"],
	   	    callback : function(options) {
	   	    	var $customerId = $( "#customerIdField" );
	   	    	$("#typeahead").val(options[ "name" ]);
	   	    	$customerId.val(options[ "id" ]);
	   	    	$customerId.data( "contact-fullname", options[ "contactFullName" ] );
	   		    $customerId.data( "contact-no", options[ "contactNo" ] );
	   		    $customerId.data( "email", options[ "email" ] );
	   		    $customerId.change();
	   	    },
	   	    title: "<spring:message code="gc.cxprofile.page" />"
		});
	});
});
</script>



