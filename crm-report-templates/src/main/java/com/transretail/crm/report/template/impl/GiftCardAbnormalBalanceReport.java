package com.transretail.crm.report.template.impl;

import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.lang3.StringUtils;
import org.joda.time.LocalDateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.support.MessageSourceAccessor;
import org.springframework.stereotype.Service;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.google.common.collect.Sets;
import com.mysema.query.BooleanBuilder;
import com.transretail.crm.common.reporting.jasper.ClasspathFileResolver;
import com.transretail.crm.common.reporting.jasper.DefaultJRProcessor;
import com.transretail.crm.common.reporting.jasper.JRProcessor;
import com.transretail.crm.common.util.DateUtil;
import com.transretail.crm.core.entity.enums.ReportType;
import com.transretail.crm.giftcard.entity.GiftCardTransactionItem;
import com.transretail.crm.giftcard.entity.QGiftCardTransaction;
import com.transretail.crm.giftcard.entity.QGiftCardTransactionItem;
import com.transretail.crm.giftcard.entity.support.GiftCardSaleTransaction;
import com.transretail.crm.giftcard.repo.GiftCardTransactionItemRepo;
import com.transretail.crm.report.template.CommonReportFilter;
import com.transretail.crm.report.template.FilterField;
import com.transretail.crm.report.template.NoDataFoundSupport;
import com.transretail.crm.report.template.ReportTemplate;

/**
 * @author Allan G. Ramirez (agramirez@exist.com)
 */
@Service("giftCardAbnormalBalanceReport")
public class GiftCardAbnormalBalanceReport implements ReportTemplate, NoDataFoundSupport {
    protected static final String DATE_FROM_FILTR = CommonReportFilter.DATE_FROM;
    protected static final String DATE_TO_FILTR = CommonReportFilter.DATE_TO;

    @Autowired
    private GiftCardTransactionItemRepo giftCardTransactionItemRepo;

    private MessageSourceAccessor messageSource;

    @Autowired
    public void setMessageSource(MessageSource messageSource) {
        this.messageSource = new MessageSourceAccessor(messageSource);
    }

    @Override
    public String getName() {
        return "Gift Card Abnormal Report";
    }

    @Override
    public String getDescription() {
        return messageSource.getMessage("gc.abnormal.balance.report.title");
    }

    @Override
    public boolean isEmpty(Map<String, String> filters) {
        QGiftCardTransactionItem qGcTxnItem = QGiftCardTransactionItem.giftCardTransactionItem;
        QGiftCardTransaction qTransaction = qGcTxnItem.transaction;
        String cardNo = filters.get("cardNo");
        String abnormalType = filters.get("abnormalType");
        LocalDateTime dateFrom = INPUT_DATE_PATTERN.parseLocalDateTime(filters.get(CommonReportFilter.DATE_FROM));
        LocalDateTime dateTo = INPUT_DATE_PATTERN.parseLocalDateTime(filters.get(CommonReportFilter.DATE_TO))
                .plusDays(1)
                .withHourOfDay(0)
                .withMinuteOfHour(0)
                .withSecondOfMinute(0)
                .withMillisOfSecond(0);

        BooleanBuilder fil = new BooleanBuilder(
                qTransaction.transactionType.eq(GiftCardSaleTransaction.REDEMPTION)
                .and(qGcTxnItem.currentAmount.add(qGcTxnItem.transactionAmount).lt(0)) //negative abnormal type
                .and(qTransaction.transactionDate.goe(dateFrom)
                        .and(qTransaction.transactionDate.loe(dateTo)))
        );
        if (StringUtils.isNotBlank(cardNo)) {
            fil.and(qGcTxnItem.giftCard.barcode.eq(cardNo));
        }
        return giftCardTransactionItemRepo.count(fil) == 0;
    }
    
    public static enum AbnormalType {
    	NEGATIVE
    }

    @Override
    public Set<FilterField> getFilters() {
        Set<FilterField> filters = Sets.newLinkedHashSet();
        filters.add(FilterField.createDateField(DATE_FROM_FILTR, messageSource.getMessage("label_from"), DateUtil.SEARCH_DATE_FORMAT, true));
        filters.add(FilterField.createDateField(DATE_TO_FILTR, messageSource.getMessage("label_to"), DateUtil.SEARCH_DATE_FORMAT, true));
        
        Map<String, String> abnormalTypes = Maps.newLinkedHashMap();
        abnormalTypes.put("", "");
        for(AbnormalType type: AbnormalType.values()) {
        	abnormalTypes.put(type.toString(), type.toString());
        }
        filters.add(FilterField.createDropdownField("abnormalType", 
				messageSource.getMessage("gc.abnormal.balance.report.abnormal.type"), abnormalTypes)); ///For display only
        filters.add(FilterField.createInputField("cardNo", 
    			messageSource.getMessage("gc.abnormal.balance.report.card.no")));
        return filters;
    }

    @Override
    public boolean canView(Set<String> permissions) {
        return true;
    }

    @Override
    public JRProcessor createJrProcessor(Map<String, String> parameters) {
        QGiftCardTransactionItem qGcTxnItem = QGiftCardTransactionItem.giftCardTransactionItem;
        QGiftCardTransaction qTransaction = qGcTxnItem.transaction;
        String cardNo = parameters.get("cardNo");
        String abnormalType = parameters.get("abnormalType"); 
        LocalDateTime dateFrom = INPUT_DATE_PATTERN.parseLocalDateTime(parameters.get(CommonReportFilter.DATE_FROM));
        LocalDateTime dateTo = INPUT_DATE_PATTERN.parseLocalDateTime(parameters.get(CommonReportFilter.DATE_TO))
            .plusDays(1)
            .withHourOfDay(0)
            .withMinuteOfHour(0)
            .withSecondOfMinute(0)
            .withMillisOfSecond(0);
        
        BooleanBuilder fil = new BooleanBuilder(
            qTransaction.transactionType.eq(GiftCardSaleTransaction.REDEMPTION)
                .and(qGcTxnItem.currentAmount.add(qGcTxnItem.transactionAmount).lt(0)) //negative abnormal type
                .and(qTransaction.transactionDate.goe(dateFrom)
                    .and(qTransaction.transactionDate.loe(dateTo)))
        );
        if(StringUtils.isNotBlank(cardNo)) {
        	fil.and(qGcTxnItem.giftCard.barcode.eq(cardNo));
        }

        Iterable<GiftCardTransactionItem> itemIterable = giftCardTransactionItemRepo.findAll(fil);
        Map<String, Object> reportParameters = Maps.newHashMap();
        List<ReportBean> beans = Lists.newArrayList();
        if (!itemIterable.iterator().hasNext()) {
            beans.add(new ReportBean());
            reportParameters.put("isEmpty", true);
        } else {
            for (GiftCardTransactionItem item : itemIterable) {
                ReportBean bean = new ReportBean();
                bean.setAbnormalType(messageSource.getMessage("gc.abnormal.balance.report.abnormal.type.negative"));
                bean.setCardName(item.getGiftCard().getProductName());
                bean.setCardNo(item.getGiftCard().getBarcode());
                bean.setTxnDate(INPUT_DATE_PATTERN.print(item.getTransaction().getTransactionDate()));
                beans.add(bean);
            }
        }

        
        reportParameters.put("dateFrom", parameters.get(CommonReportFilter.DATE_FROM));
        reportParameters.put("dateTo", parameters.get(CommonReportFilter.DATE_TO));
        reportParameters.put("abnormalType", abnormalType);
        reportParameters.put("cardNo", cardNo);

        DefaultJRProcessor defaultJRProcessor = new DefaultJRProcessor("reports/gift_card_abnormal_balance.jasper", beans);
        defaultJRProcessor.setFileResolver(new ClasspathFileResolver("reports"));
        defaultJRProcessor.setParameters(reportParameters);
        return defaultJRProcessor;
    }

    @Override
    public ReportType[] getReportTypes() {
        return ReportType.values();
    }

    public static class ReportBean {
        private String txnDate;
        private String cardNo;
        private String cardName;
        private String abnormalType;

        public String getTxnDate() {
            return txnDate;
        }

        public void setTxnDate(String txnDate) {
            this.txnDate = txnDate;
        }

        public String getCardNo() {
            return cardNo;
        }

        public void setCardNo(String cardNo) {
            this.cardNo = cardNo;
        }

        public String getCardName() {
            return cardName;
        }

        public void setCardName(String cardName) {
            this.cardName = cardName;
        }

        public String getAbnormalType() {
            return abnormalType;
        }

        public void setAbnormalType(String abnormalType) {
            this.abnormalType = abnormalType;
        }
    }
}
