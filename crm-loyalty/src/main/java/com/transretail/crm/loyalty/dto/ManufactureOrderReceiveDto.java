package com.transretail.crm.loyalty.dto;

import org.joda.time.LocalDate;

import com.mysema.query.annotations.QueryProjection;


public class ManufactureOrderReceiveDto {

	private String cardType;	
	private String startingSeries;
	private String endingSeries;
	private String receiptNo;
	private LocalDate receiveDate;
	private String receivedBy;
	
	public ManufactureOrderReceiveDto() {}
	
	
	@QueryProjection
	public ManufactureOrderReceiveDto(String cardType, String startingSeries,
			String endingSeries, String receiptNo, LocalDate receiveDate) {
		super();
		this.cardType = cardType;
		this.startingSeries = startingSeries;
		this.endingSeries = endingSeries;
		this.receiptNo = receiptNo;
		this.receiveDate = receiveDate;
	}
	
	public String getCardType() {
		return cardType;
	}
	public void setCardType(String cardType) {
		this.cardType = cardType;
	}
	public String getStartingSeries() {
		if(startingSeries != null)
			return String.format("%012d", Long.valueOf(startingSeries));
		return "";
	}
	public void setStartingSeries(String startingSeries) {
		this.startingSeries = startingSeries;
	}
	public String getEndingSeries() {
		if(endingSeries != null)
			return String.format("%012d", Long.valueOf(endingSeries));
		return "";
	}
	public void setEndingSeries(String endingSeries) {
		this.endingSeries = endingSeries;
	}
	public String getReceiptNo() {
		return receiptNo;
	}
	public void setReceiptNo(String receiptNo) {
		this.receiptNo = receiptNo;
	}
	public LocalDate getReceiveDate() {
		return receiveDate;
	}
	public String getReceiveDateStr() {
		if(receiveDate != null)
			return receiveDate.toString();
		return "";
	}
	public void setReceiveDate(LocalDate receiveDate) {
		this.receiveDate = receiveDate;
	}
	public Long getQuantity() {
		if(endingSeries != null && startingSeries != null)
			return Long.valueOf(endingSeries) - Long.valueOf(startingSeries) + 1;
		return null;
	}
	public String getReceivedBy() {
		return receivedBy;
	}
	public void setReceivedBy(String receivedBy) {
		this.receivedBy = receivedBy;
	}
}
