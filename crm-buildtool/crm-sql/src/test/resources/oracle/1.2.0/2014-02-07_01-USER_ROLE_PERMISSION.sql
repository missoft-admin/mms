    DROP TABLE CRM_USER_X_ROLE;
    DROP TABLE CRM_USER_ROLE;

    create table CRM_USER_ROLE (
        CODE varchar2(255 char) not null,
        CREATED_BY varchar2(255 char),
        CREATED_DATETIME timestamp,
        LAST_UPDATED_BY varchar2(255 char),
        LAST_UPDATED_DATETIME timestamp,
        DESCRIPTION varchar2(255 char),
        ENABLED char(1 char),
        version number(10,0),
        primary key (CODE)
    );

    create table CRM_USER_PERMISSION (
        CODE varchar2(255 char) not null,
        CREATED_BY varchar2(255 char),
        CREATED_DATETIME timestamp,
        LAST_UPDATED_BY varchar2(255 char),
        LAST_UPDATED_DATETIME timestamp,
        DESCRIPTION varchar2(255 char) not null,
        STATUS varchar2(8 char),
        ROLE_CATEGORY varchar2(1000 char),
        primary key (CODE)
    );

    create table CRM_USER_ROLE_PERMISSION (
        id number(19,0) not null,
        CREATED_BY varchar2(255 char),
        CREATED_DATETIME timestamp,
        LAST_UPDATED_BY varchar2(255 char),
        LAST_UPDATED_DATETIME timestamp,
        PERM_ID varchar2(255 char),
        ROLE_ID varchar2(255 char) not null,
        USER_ID number(19,0) not null,
        primary key (id)
    );

    alter table CRM_USER_ROLE_PERMISSION
        add constraint UK_qmo20dja1amx6ctywpr8e3t12 unique (USER_ID, ROLE_ID, PERM_ID);

    alter table CRM_USER_ROLE_PERMISSION
        add constraint FK_q9uvoo61h5hcmfigefo3iwcfl
        foreign key (PERM_ID)
        references CRM_USER_PERMISSION;

    alter table CRM_USER_ROLE_PERMISSION
        add constraint FK_3uyygwu1e2i30o9huro3cjn0t
        foreign key (ROLE_ID)
        references CRM_USER_ROLE;

    alter table CRM_USER_ROLE_PERMISSION
        add constraint FK_eu57mwyu3r8y0yro30hphgtle
        foreign key (USER_ID)
        references CRM_USER;