package com.transretail.crm.web.controller;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.transretail.crm.common.service.dto.response.ResultList;
import com.transretail.crm.core.dto.CityDto;
import com.transretail.crm.core.dto.CityProvinceSearchDto;
import com.transretail.crm.core.dto.ProvinceDto;
import com.transretail.crm.core.entity.City;
import com.transretail.crm.core.entity.Province;
import com.transretail.crm.core.service.LocationService;
import com.transretail.crm.web.controller.util.ControllerResponse;

@RequestMapping("/location")
@Controller
public class LocationLookupController {
	
	@Autowired
	LocationService locationService;
	@Autowired
	MessageSource messageSource;
	
	@RequestMapping(produces = "text/html")
    public String list(
            Model uiModel) {
        return "location/list";
    }
	
	@RequestMapping(value = "/province/list", method = RequestMethod.POST)
    public @ResponseBody ResultList<Province> listProvince(
    		@RequestBody CityProvinceSearchDto dto) {
    	return locationService.listProvince(dto);
    }
	
	@RequestMapping(value = "/city/list", method = RequestMethod.POST)
    public @ResponseBody ResultList<City> listCity(
    		@RequestBody CityProvinceSearchDto dto) {
    	return locationService.listCityByProvince(dto);
    }
	
	@RequestMapping(value = "province/{id}", method = RequestMethod.DELETE, produces = "application/json; charset=utf-8")
	@ResponseBody
    public ControllerResponse deleteProvince(@PathVariable("id") Long id) {
        locationService.deleteProvince(id);
        ControllerResponse response = new ControllerResponse();
        response.setSuccess(true);
        return response;
    }
	
	@RequestMapping(value = "city/{id}", method = RequestMethod.DELETE, produces = "application/json; charset=utf-8")
	@ResponseBody
    public ControllerResponse deleteCity(@PathVariable("id") Long id) {
        locationService.deleteCity(id);
        ControllerResponse response = new ControllerResponse();
        response.setSuccess(true);
        return response;
    }

	@RequestMapping(value = "/province", method = RequestMethod.POST, produces = "application/json; charset=utf-8")
	@ResponseBody
    public ControllerResponse saveProvince(@ModelAttribute ProvinceDto province,
            BindingResult bindingResult) {
		
		ControllerResponse response = new ControllerResponse();
		response.setSuccess(true);
		
		validateProvince(bindingResult, province);
		
		if (bindingResult.hasErrors()) {
			response.setSuccess(false);
			response.setResult(bindingResult.getAllErrors());
			return response;
		}

		if (response.isSuccess()) {
			locationService.saveProvince(province);
		}

		return response;
    }
	
	private void validateProvince(BindingResult result, ProvinceDto provinceDto) {
		if(StringUtils.isBlank(provinceDto.getName())) {
			result.reject(getMessage("propkey_msg_notempty", new Object[] {"location_name"}));
		} else {
			Province province = locationService.getProvince(provinceDto.getName());
			if(province != null && (provinceDto.getId() == null || province.getId().compareTo(provinceDto.getId()) != 0)) {
				result.reject(getMessage("err_duplicate_location", new Object[] {"location_province"}));
			}
		}
	}
	
	private void validateCity(BindingResult result, CityDto cityDto) {
		if(StringUtils.isBlank(cityDto.getName())) {
			result.reject(getMessage("propkey_msg_notempty", new Object[] {"location_name"}));
		} else {
			City city = locationService.getCity(cityDto.getName());
			if(city != null && (cityDto.getId() == null || city.getId().compareTo(cityDto.getId()) != 0)) {
				result.reject(getMessage("err_duplicate_location", new Object[] {"location_city"}));
			}
		}
	}
	
	@RequestMapping(value = "/city/{province}", method = RequestMethod.POST, produces = "application/json; charset=utf-8")
	@ResponseBody
    public ControllerResponse saveCity(@ModelAttribute CityDto city,
    		@PathVariable("province") Long provinceId,
            BindingResult bindingResult) {
		
		ControllerResponse response = new ControllerResponse();
		response.setSuccess(true);
		
		validateCity(bindingResult, city);
		
		if (bindingResult.hasErrors()) {
			response.setSuccess(false);
			response.setResult(bindingResult.getAllErrors());
			return response;
		}

		if (response.isSuccess()) {
			city.setProvinceId(provinceId);
			locationService.saveCity(city);
		}

		return response;
    }
	
	private String getMessage(String inMsgCode, Object[] inArgMsgCodes) {
		if(inArgMsgCodes != null) {
			for ( int i=0; i < inArgMsgCodes.length; i++ ) {
				inArgMsgCodes[i] = messageSource.getMessage( inArgMsgCodes[i].toString().toLowerCase(), null, LocaleContextHolder.getLocale() );
			}
		}
		return messageSource.getMessage(inMsgCode, inArgMsgCodes, "Info given is invalid.", LocaleContextHolder.getLocale());
	}
}
