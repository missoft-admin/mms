package com.transretail.crm.core.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Version;

/**
 * @author Allan G. Ramirez (agramirez@exist.com)
 */
@Entity(name = "CRM_COUNTER")
public class CounterModel implements Serializable {
    public static final String PRFX_LCNO_COUNTER = "LC_COUNTER_COM_CARD_LAST_2_DIGITS-";
    private static final long serialVersionUID = -280475632844012890L;

    @Id
    @Column(name = "KEY")
    private String key;
    @Column(name = "VALUE")
    private Long value;
    @Version
    @Column(name = "VERSION")
    public Integer version;

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public Long getValue() {
        return value;
    }

    public void setValue(Long value) {
        this.value = value;
    }

    public Integer getVersion() {
        return version;
    }

    public void setVersion(Integer version) {
        this.version = version;
    }
}
