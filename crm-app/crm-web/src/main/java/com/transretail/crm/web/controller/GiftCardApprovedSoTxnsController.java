package com.transretail.crm.web.controller;


import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletResponse;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.transretail.crm.core.dto.ApprovalRemarkDto;
import com.transretail.crm.core.dto.MessageDto;
import com.transretail.crm.core.entity.ApprovalRemark;
import com.transretail.crm.core.security.util.UserUtil;
import com.transretail.crm.core.service.CodePropertiesService;
import com.transretail.crm.core.service.LookupService;
import com.transretail.crm.core.service.UserService;
import com.transretail.crm.giftcard.dto.SalesOrderDto;
import com.transretail.crm.giftcard.entity.SalesOrder;
import com.transretail.crm.giftcard.entity.support.PaymentInfoStatus;
import com.transretail.crm.giftcard.entity.support.SalesOrderStatus;
import com.transretail.crm.giftcard.entity.support.SalesOrderType;
import com.transretail.crm.giftcard.service.GiftCardAccountingService;
import com.transretail.crm.giftcard.service.SalesOrderPaymentService;
import com.transretail.crm.giftcard.service.SalesOrderService;
import com.transretail.crm.giftcard.service.SoAllocService;
import com.transretail.crm.giftcard.service.SoApprovedTxnsService;
import com.transretail.crm.giftcard.service.impl.SalesOrderServiceImpl;
import com.transretail.crm.media.service.MailService;
import com.transretail.crm.report.template.impl.GiftCardSoHandoverDocument;
import com.transretail.crm.report.template.impl.GiftCardSoHandoverDocument.HoDocType;
import com.transretail.crm.web.controller.util.ControllerResponse;


@Controller
@RequestMapping( "/giftcard/salesorder" )
public class GiftCardApprovedSoTxnsController extends AbstractReportsController {

	private static final String VIEW_FORM 				= "view/giftcard/salesorder/print";
	private static final String VIEW_CANCEL_FORM 		= "view/giftcard/salesorder/cancel";
	private static final String VIEW_DELIVERY_FORM 		= "view/giftcard/salesorder/delivery";

	private static final String UIATTR_SO				= "salesOrder";
	private static final String UIATTR_SO_DOCTYPE  		= "soDocTypes";
	private static final String UIATTR_SO_DOC_RECEIPT 	= "soDocTypeReceipt";
	private static final String UIATTR_FORM 			= "soForm";


	SoApprovedTxnsService service;
	private SoAllocService soAllocService;
    private SalesOrderService salesOrderService;
    UserService userService;
	private MailService mailService;
	LookupService lookupService;
	CodePropertiesService codePropertiesService;
	private GiftCardSoHandoverDocument giftCardSoHandoverDocument;
    private MessageSource messageSource;
    private SalesOrderPaymentService paymentService;
    private GiftCardAccountingService accountingService;


    /* ----- IoC ----- */
    @Autowired
    public void setService(SoApprovedTxnsService service) {
        this.service = service;
    }
    @Autowired
    public void setSoAllocService(SoAllocService soAllocService) {
        this.soAllocService = soAllocService;
    }
    @Autowired
    public void setSalesOrderService(SalesOrderService salesOrderService) {
        this.salesOrderService = salesOrderService;
    }
    @Autowired
    public void setUserService(UserService userService) {
        this.userService = userService;
    }
    @Autowired
    public void setMailService(MailService mailService) {
        this.mailService = mailService;
    }
    @Autowired
    public void setLookupService(LookupService lookupService) {
        this.lookupService = lookupService;
    }
    @Autowired
    public void setCodePropertiesService(CodePropertiesService codePropertiesService) {
        this.codePropertiesService = codePropertiesService;
    }
    @Autowired
    public void setGiftCardSoHandoverDocument(GiftCardSoHandoverDocument giftCardSoHandoverDocument) {
        this.giftCardSoHandoverDocument = giftCardSoHandoverDocument;
    }
    @Autowired
    public void setMessageSource(MessageSource messageSource) {
        this.messageSource = messageSource;
    }
    @Autowired
    public void setPaymentService(SalesOrderPaymentService paymentService) {
        this.paymentService = paymentService;
    }
    @Autowired
    public void setAccountingService(GiftCardAccountingService accountingService) {
        this.accountingService = accountingService;
    }

    @InitBinder
	void initBinder( WebDataBinder binder ) {
		DateFormat df = new SimpleDateFormat( "dd MMM yyyy" );
		df.setLenient( false );
		binder.registerCustomEditor( Date.class, new CustomDateEditor( df, true ) );
	}

    @RequestMapping( value="/pickup/show/{id}", produces = "text/html" )
    public ModelAndView showPrintForm( @PathVariable(value="id") Long id ) {
    	ModelAndView mv = new ModelAndView( VIEW_FORM );
    	SalesOrderDto dto = soAllocService.setSoItemsAllocsDto( id );
    	mv.addObject( UIATTR_SO, dto );
    	if ( null != dto && StringUtils.isBlank( dto.getReceiptNo() ) ) {
    		dto.setReceiptNo( service.generateInvoiceNo( id ) );
    	}

    	if ( SalesOrderType.INTERNAL_ORDER.equals( dto.getOrderType() ) ) {
        	mv.addObject( UIATTR_SO_DOCTYPE, new String[]{ GiftCardSoHandoverDocument.HoDocType.SO_SHEET.name(), 
        		GiftCardSoHandoverDocument.HoDocType.VOUCHER.name() } );
    	}
    	else {
        	mv.addObject( UIATTR_SO_DOCTYPE, GiftCardSoHandoverDocument.HoDocType.values() );
    	}
    	mv.addObject( UIATTR_SO_DOC_RECEIPT, GiftCardSoHandoverDocument.HoDocType.RECEIPT );
        return mv;
    }

    @RequestMapping( value="/cancel/{id}", produces = "text/html" )
    public ModelAndView showCancelForm( @PathVariable(value="id") Long id ) {
    	ModelAndView mv = new ModelAndView( VIEW_CANCEL_FORM );
    	mv.addObject( UIATTR_SO, soAllocService.setSoItemsAllocsDto( id ) );
    	mv.addObject( UIATTR_SO_DOCTYPE, GiftCardSoHandoverDocument.HoDocType.values() );
    	mv.addObject( UIATTR_SO_DOC_RECEIPT, GiftCardSoHandoverDocument.HoDocType.RECEIPT );

    	List<ApprovalRemark> remarks = service.getRemarks( id );
    	if ( CollectionUtils.isNotEmpty( remarks ) ) {
    		mv.addObject( "cancelRemarks", remarks.get( 0 ) );
    	}
        return mv;
    }

    @RequestMapping( value="/delivery/{id}", produces = "text/html" )
    public ModelAndView showShippingForm( @PathVariable(value="id") Long id ) {
    	ModelAndView mv = new ModelAndView( VIEW_DELIVERY_FORM );
    	SalesOrderDto dto = soAllocService.setSoItemsAllocsDto( id );
    	mv.addObject( UIATTR_SO, dto );
    	mv.addObject( UIATTR_SO_DOCTYPE, GiftCardSoHandoverDocument.HoDocType.values() );
    	mv.addObject( UIATTR_SO_DOC_RECEIPT, GiftCardSoHandoverDocument.HoDocType.RECEIPT );

		mv.addObject( UIATTR_FORM, dto );
		mv.addObject( "deliveryTypes", SalesOrder.DeliveryType.values() );
        return mv;
    }

    @RequestMapping( value="/pickup/print/{id}/{document}" )
    public void printHandoverDocs( 
    		@PathVariable(value="id") Long soId, 
    		@PathVariable(value="document") GiftCardSoHandoverDocument.HoDocType document,
			@RequestParam(value="receiptNo", required = false) String receiptNo,
            HttpServletResponse res ) throws Exception {

    	SalesOrderDto dto = soAllocService.setSoItemsAllocsDto( soId );
    	if ( document == HoDocType.SO_SHEET ) {
    		renderReport( res, giftCardSoHandoverDocument.createSoSheetJrProcessor( dto ) );
    	}
    	else if ( document == HoDocType.RECEIPT ) {
    		service.updateReceiptNo( soId, receiptNo );
    		dto = soAllocService.setSoItemsAllocsDto( soId );
    		renderReport( res, giftCardSoHandoverDocument.createReceiptJrProcessor( dto ) );
    	}
    	else if ( document == HoDocType.VOUCHER ) {
    		renderReport( res, giftCardSoHandoverDocument.createVoucherReqJrProcessor( dto ) );
    	}
    }

    @RequestMapping( value="/pickup/update/{id}", produces="application/json; charset=utf-8", method=RequestMethod.POST )
	public @ResponseBody ControllerResponse setSoForPickup( @PathVariable(value="id") Long id ) {
    	ControllerResponse resp = new ControllerResponse();
    	if ( null != id ) {
    		resp.setResult( service.updateStatus( id, SalesOrderStatus.FOR_PICKUP ) );
    		resp.setSuccess( true );
    	}
    	return resp;
    }

    @RequestMapping( value="/pickup/handover/{id}", produces="application/json; charset=utf-8", method=RequestMethod.POST )
	public @ResponseBody ControllerResponse setSoPaymentApproval( @PathVariable(value="id") Long id ) {
    	ControllerResponse resp = new ControllerResponse();
    	if ( null != id ) {
    		resp.setResult( service.updateStatus( id, SalesOrderStatus.FOR_ACTIVATION ) );
    		paymentService.processPaymentsForScndApproval(id);
    		notifyAccountingInternalActivation(salesOrderService.getOrder(id));
    		resp.setSuccess( true );
    	}
    	return resp;
    }

    @RequestMapping( value="/activation/{id}", produces="application/json; charset=utf-8", method=RequestMethod.POST )
	public @ResponseBody ControllerResponse setSoForActivation( @PathVariable(value="id") Long id ) {
    	ControllerResponse resp = new ControllerResponse();
    	if ( null != id ) {
    		SalesOrderDto dto = service.updateStatus( id, SalesOrderStatus.FOR_ACTIVATION );
    		resp.setResult( dto );
    		resp.setSuccess( true );
    	}
    	return resp;
    }

    @RequestMapping( value="/activation/notify/{id}", produces="application/json; charset=utf-8", method=RequestMethod.POST )
	public @ResponseBody ControllerResponse notifySoForActivation( @PathVariable(value="id") Long id ) {
    	ControllerResponse resp = new ControllerResponse();
    	if ( null != id ) {
    		SalesOrderDto dto = salesOrderService.getOrder( id );
    		notifySupervisor( dto );
    	}
    	return resp;
    }

    @RequestMapping( value="/activate/{id}", produces="application/json; charset=utf-8", method=RequestMethod.POST )
	public @ResponseBody ControllerResponse activate( @PathVariable(value="id") Long id ) {
    	ControllerResponse resp = new ControllerResponse();
    	if ( null != id ) {
    		resp.setResult( service.activateB2bGcs( id ) );
    		resp.setSuccess( true );
    	}
        
    	return resp;
    }
  
    @RequestMapping( value="/cancel/{id}/{remarks}", produces="application/json; charset=utf-8", method=RequestMethod.POST )
	public @ResponseBody ControllerResponse cancel(
			@PathVariable(value="id") Long id,
			@PathVariable(value="remarks") String remarks ) {

		ControllerResponse resp = new ControllerResponse();

		if ( null != id && StringUtils.isNotBlank( remarks ) ) {
			ApprovalRemarkDto remark = new ApprovalRemarkDto();
			remark.setModelId( id.toString() );
			remark.setRemarks( remarks );
			remark.setStatus( codePropertiesService.getDetailStatusCancelled() );
			service.saveRemarks( remark );
			resp.setSuccess( true );
			resp.setResult( remark );
			
			paymentService.updateStatus(id, PaymentInfoStatus.SO_CANCELLED);
			
			SalesOrderDto orderDto = salesOrderService.getOrder(id);
			if(SalesOrderType.B2B_ADV_SALES.compareTo(orderDto.getOrderType()) == 0) {
				accountingService.forAdvSoVerifCancelled(new DateTime(), orderDto.getTotalFaceAmount(), orderDto.getOrderNo());
			}
		}
		return resp;
    }

    @RequestMapping( value="/delivery/save/{id}", produces="application/json; charset=utf-8", method=RequestMethod.POST )
	public @ResponseBody ControllerResponse updateDelivery( 
			@PathVariable(value="id") Long id,
			@ModelAttribute(UIATTR_FORM) SalesOrderDto update, 
			BindingResult result ) {

    	ControllerResponse resp = new ControllerResponse();
    	if ( null != id ) {
    		SalesOrder model = salesOrderService.get( id );
    		/*model.setDeliveryType( update.getDeliveryType() );*/
    		model.setShippingInfo( update.getShippingInfo() );
    		/*model.setShippingFee( update.getShippingFee() );*/
    		resp.setResult( salesOrderService.save( model ) );
    		resp.setSuccess( true );
    	}
    	return resp;
    }


    /* ------------------- Helper Method --------------------------- */

    private void notifyAccountingInternalActivation(SalesOrderDto order) {
    	if(order.getOrderType().compareTo(SalesOrderType.INTERNAL_ORDER) == 0) {
    		List<String> rcpts = userService.listSupervisorEmails(codePropertiesService.getAccountingCode());
        	MessageDto msg = new MessageDto();
        	msg.setRecipients(rcpts.toArray(new String[rcpts.size()]));
        	msg.setMessage( messageSource.getMessage( "gc.so.foractivation.notif", new String[]{ order.getOrderNo() }, LocaleContextHolder.getLocale() ) );
        	msg.setSubject( messageSource.getMessage( "gc.so.foractivation.notif.subject", new String[]{ order.getOrderNo() }, LocaleContextHolder.getLocale() ) );
        	mailService.send(msg);	
    	}
    }

    private void notifySupervisor( SalesOrderDto dto ) {
    	MessageDto email = new MessageDto();
    	email.setMessage( messageSource.getMessage( "gc.so.foractivation.notif", new String[]{ dto.getOrderNo() }, LocaleContextHolder.getLocale() ) );
    	
    	List<String> emailAddresses = userService.listSupervisorEmails( UserUtil.getCurrentUser().getStoreCode(), codePropertiesService.getMerchantServiceSupervisorCode() );
    	email.setRecipients( emailAddresses.toArray( new String[ emailAddresses.size() ] ) );
    	email.setSubject( messageSource.getMessage( "gc.so.foractivation.notif.subject", new String[]{ dto.getOrderNo() }, LocaleContextHolder.getLocale() ) );
    	mailService.send( email );
    }

}
