package com.transretail.crm.giftcard.service.impl;

import java.math.BigDecimal;
import java.math.MathContext;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.joda.time.LocalDate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.google.common.collect.Lists;
import com.mysema.query.BooleanBuilder;
import com.mysema.query.jpa.JPASubQuery;
import com.mysema.query.jpa.impl.JPAQuery;
import com.mysema.query.types.Predicate;
import com.transretail.crm.common.service.dto.request.PageSortDto;
import com.transretail.crm.common.service.dto.response.ResultList;
import com.transretail.crm.common.service.util.SpringDataPagingUtil;
import com.transretail.crm.giftcard.dto.GiftCardDiscountSchemeDto;
import com.transretail.crm.giftcard.entity.GiftCardCustomerProfile;
import com.transretail.crm.giftcard.entity.GiftCardDiscountScheme;
import com.transretail.crm.giftcard.entity.GiftCardGeneralDiscount;
import com.transretail.crm.giftcard.entity.QGiftCardDiscountScheme;
import com.transretail.crm.giftcard.entity.QGiftCardGeneralDiscount;
import com.transretail.crm.giftcard.entity.QSalesOrder;
import com.transretail.crm.giftcard.entity.QSalesOrderItem;
import com.transretail.crm.giftcard.entity.SalesOrder;
import com.transretail.crm.giftcard.entity.support.DiscountType;
import com.transretail.crm.giftcard.entity.support.SalesOrderDiscountType;
import com.transretail.crm.giftcard.entity.support.SalesOrderStatus;
import com.transretail.crm.giftcard.entity.support.SalesOrderType;
import com.transretail.crm.giftcard.repo.GiftCardCustomerProfileRepo;
import com.transretail.crm.giftcard.repo.GiftCardDiscountSchemeRepo;
import com.transretail.crm.giftcard.repo.GiftCardGeneralDiscountRepo;
import com.transretail.crm.giftcard.repo.SalesOrderRepo;
import com.transretail.crm.giftcard.service.DiscountSchemeService;

/**
 *
 */
@Service("discountSchemeService")
public class DiscountSchemeServiceImpl implements DiscountSchemeService {
	@Autowired
	private GiftCardDiscountSchemeRepo schemeRepo;
	@Autowired
	private GiftCardCustomerProfileRepo customerRepo;
	@Autowired
	private SalesOrderRepo orderRepo;
	@Autowired
	private GiftCardGeneralDiscountRepo genDiscRepo;
	@PersistenceContext
	private EntityManager em;
	private static final MathContext ROUNDING_CONTEXT = MathContext.DECIMAL32;
	private static final BigDecimal BILLION_MULT = new BigDecimal(1000000000);

	@Override
	@Transactional(readOnly = true)
	public boolean validateForOverlappingDiscount(LocalDate startDate, LocalDate endDate, Long customerId, Long id) {
		QGiftCardDiscountScheme qScheme = QGiftCardDiscountScheme.giftCardDiscountScheme;
		BooleanBuilder idFilter = new BooleanBuilder();
		if(id != null)
			idFilter.and(qScheme.id.ne(id));
		return new JPAQuery(em).from(qScheme).where(
				qScheme.customer.id.eq(customerId)
				.and(idFilter)
				.and(qScheme.startDate.between(startDate, endDate)
						.or(qScheme.endDate.between(startDate, endDate))
						.or(qScheme.startDate.loe(startDate).and(qScheme.endDate.goe(startDate)))
						.or(qScheme.startDate.loe(endDate).and(qScheme.endDate.goe(endDate))))
				).exists();
	}

	@Override
	@Transactional
	public void saveScheme(GiftCardDiscountSchemeDto schemeDto) {
		GiftCardDiscountScheme scheme = new GiftCardDiscountScheme();
		if(schemeDto.getId() != null)
			scheme = schemeRepo.findOne(schemeDto.getId());
		schemeRepo.save(schemeDto.toModel(scheme));
	}
	
	@Override
	@Transactional(readOnly = true)
	public ResultList<GiftCardDiscountSchemeDto> list(PageSortDto sortDto) {
		Pageable pageable = SpringDataPagingUtil.INSTANCE.createPageable(sortDto.getPagination());
        Page<GiftCardDiscountScheme> page = schemeRepo.findAll(pageable);
        return new ResultList<GiftCardDiscountSchemeDto>(toDto(page.getContent()), page.getTotalElements(), page.hasPreviousPage(),
                page.hasNextPage());
	}
	
	private List<GiftCardDiscountSchemeDto> toDto(Iterable<GiftCardDiscountScheme> schemes) {
		List<GiftCardDiscountSchemeDto> schemeDtos = Lists.newArrayList();
		QSalesOrder qOrder = QSalesOrder.salesOrder;
		for(GiftCardDiscountScheme scheme: schemes) {
			GiftCardDiscountSchemeDto schemeDto = new GiftCardDiscountSchemeDto(scheme);
			if(orderRepo.count(qOrder.discount.id.eq(schemeDto.getId())) > 0)
				schemeDto.setReadOnly(true);
			schemeDtos.add(schemeDto);
		}
		return schemeDtos;
	}
	
	@Override
	@Transactional(readOnly = true)
	public GiftCardDiscountSchemeDto getSchemeDto(Long id) {
		return new GiftCardDiscountSchemeDto(schemeRepo.findOne(id));
	}
	
	@Override
	@Transactional
	public void deleteScheme(Long id) {
		schemeRepo.delete(id);
	}
	
	@Override
	@Transactional
	public DiscountDto getDiscountBeforeApproval(Long customerId, BigDecimal purchase, LocalDate orderDate, SalesOrderDiscountType discType) {
		GiftCardDiscountSchemeDto discount = getDiscountScheme(customerId, orderDate);

		if(discount == null) {
            return getGeneralDiscountDto(purchase, discType);
        } else if(discount.getDiscountType().compareTo(DiscountType.YEARLY_DISCOUNT_COMBO) == 0) {
			BigDecimal accumulatedPurchase = getAccumulatedPurchase(discount);
			return getDiscount(customerId, discount, purchase, accumulatedPurchase);
		} else if(discount.getDiscountType().compareTo(DiscountType.SO_YEARLY_DISCOUNT) == 0) {
            return getSOYearlyDisc(discount, purchase, discType);
        }

		return new DiscountDto();
	}
	
	@Override
	@Transactional
	public DiscountDto getDiscount(Long customerId, BigDecimal purchase, LocalDate orderDate, SalesOrderDiscountType discType) {
		GiftCardDiscountSchemeDto discount = getDiscountScheme(customerId, orderDate);
		if(discount == null)
			return getGeneralDiscountDto(purchase, discType);
		else if(discount.getDiscountType().compareTo(DiscountType.YEARLY_DISCOUNT_COMBO) == 0)
			return getDiscount(customerId, discount, purchase);
		else if(discount.getDiscountType().compareTo(DiscountType.SO_YEARLY_DISCOUNT) == 0)
			return getSOYearlyDisc(discount, purchase, discType);
		
		return new DiscountDto();
	}
	
	@Override
	@Transactional(readOnly = true)
	public GiftCardDiscountSchemeDto getDiscountScheme(Long customerId, LocalDate orderDate) {
		QGiftCardDiscountScheme qScheme = QGiftCardDiscountScheme.giftCardDiscountScheme;
		GiftCardDiscountScheme discount = schemeRepo.findOne(
				qScheme.customer.id.eq(customerId)
				.and(qScheme.startDate.loe(orderDate))
				.and(qScheme.endDate.goe(orderDate)));
		if(discount != null)
			return new GiftCardDiscountSchemeDto(discount);
		
		return null;
	}
	
	public DiscountDto getSOYearlyDisc(GiftCardDiscountSchemeDto schemeDto, BigDecimal purchase, SalesOrderDiscountType discType) {

		DiscountDto discount = new DiscountDto();
		discount.setPurchase(purchase);

		BigDecimal minPurchase = schemeDto.getMinPurchase().multiply(BILLION_MULT);
		BigDecimal startingDisc = schemeDto.getStartingDiscount();

		if(purchase.compareTo(minPurchase) >= 0) {
            if (discType.compareTo(SalesOrderDiscountType.DIRECT_DISCOUNT) == 0) {
                discount.setDiscount(startingDisc);
            } else if (discType.compareTo(SalesOrderDiscountType.VOUCHER) == 0) {
                discount.setVoucher(startingDisc);
            }
        }
		return discount;
	}
	
	
	
	@Override
	@Transactional(readOnly = true)
	public DiscountDto getGeneralDiscountDto(BigDecimal purchase, SalesOrderDiscountType discType) {
		GiftCardGeneralDiscount genDisc = getGeneralDiscount(purchase);
		DiscountDto discount = new DiscountDto();
		discount.setGeneral(true);
		if(genDisc != null) {
			if(discType.compareTo(SalesOrderDiscountType.DIRECT_DISCOUNT) == 0)
				discount.setDiscount(genDisc.getDiscount());
			else if(discType.compareTo(SalesOrderDiscountType.VOUCHER) == 0)
				discount.setVoucher(genDisc.getDiscount());
			discount.setPurchase(purchase);	
		}
		return discount;
	}

	@Override
	@Transactional(readOnly = true)
	public GiftCardGeneralDiscount getGeneralDiscount(BigDecimal purchase) {
		QGiftCardGeneralDiscount qDiscount = QGiftCardGeneralDiscount.giftCardGeneralDiscount;
		BooleanBuilder filter = new BooleanBuilder().andAnyOf(
				qDiscount.minPurchase.isNull().and(qDiscount.maxDiscount.goe(purchase)),
				qDiscount.minPurchase.loe(purchase).and(qDiscount.maxDiscount.goe(purchase)),
				qDiscount.minPurchase.loe(purchase).and(qDiscount.maxDiscount.isNull())
			);
		
		return genDiscRepo.findOne(filter);
	}
	
	@Override
	@Transactional
	public DiscountDto getDiscount(Long customerId, GiftCardDiscountSchemeDto discountDto, BigDecimal purchase) {
		BigDecimal accumulatedPurchase = getAccumulatedPurchase(discountDto);
		if(accumulatedPurchase != null) 
			accumulatedPurchase = accumulatedPurchase.subtract(purchase);
		return getDiscount(customerId, discountDto, purchase, accumulatedPurchase);
	}
	
	@Override
	@Transactional
	public DiscountDto getDiscount(Long customerId, GiftCardDiscountSchemeDto schemeDto, BigDecimal purchase, BigDecimal accumulatedPurchase) {
		BigDecimal minPurchase = schemeDto.getMinPurchase().multiply(BILLION_MULT);
		BigDecimal maxPurchase = schemeDto.getPurchaseForMaxDiscount().multiply(BILLION_MULT);
		if(minPurchase.compareTo(purchase) <= 0) {
			if(accumulatedPurchase == null)
				accumulatedPurchase = BigDecimal.ZERO;
			BigDecimal totalPurchase = accumulatedPurchase.add(purchase);
			DiscountDto discount = new DiscountDto();
			discount.setPurchase(totalPurchase);
			if(totalPurchase.compareTo(minPurchase) >= 0 && totalPurchase.compareTo(maxPurchase) < 0) {
				discount.setDiscount(schemeDto.getStartingDiscount());
			} else if(accumulatedPurchase.compareTo(maxPurchase) >= 0) {
				discount.setDiscount(schemeDto.getEndingDiscount());
			} else if(totalPurchase.compareTo(maxPurchase) >= 0 && accumulatedPurchase.compareTo(maxPurchase) < 0) {
				discount.setDiscount(schemeDto.getStartingDiscount());
				discount.setVoucher(schemeDto.getEndingDiscount().subtract(schemeDto.getStartingDiscount()));
			}
			return discount;
		}
		return new DiscountDto();
	}
	
	@Override
	public void generateDiscVoucherForRegYearlyDisc(LocalDate eocDate) {
		generateVoucher(eocDate, DiscountType.REGULAR_YEARLY_DISCOUNT);
	}
	
	@Transactional
	public void generateVoucher(LocalDate eocDate, DiscountType type) {
		QGiftCardDiscountScheme qScheme = QGiftCardDiscountScheme.giftCardDiscountScheme;
		Predicate filter = qScheme.discountType.eq(type)
				.and(qScheme.endDate.eq(eocDate));
		Iterable<GiftCardDiscountScheme> schemes = schemeRepo.findAll(filter);
		for(GiftCardDiscountScheme schm : schemes) {
			GiftCardDiscountSchemeDto schemeDto = new GiftCardDiscountSchemeDto(schm);
			GiftCardCustomerProfile cust = schm.getCustomer();
			BigDecimal accPurchase = getAccumulatedPurchase(schemeDto);
			if(accPurchase != null && accPurchase.compareTo(BigDecimal.ZERO) > 0 && accPurchase.compareTo(schemeDto.getPurchaseForMaxDiscount()) >= 0) {
				DiscountDto discount = new DiscountDto();
				discount.setVoucher(schm.getEndingDiscount());
				discount.setPurchase(accPurchase);
				createYearlyDiscount(discount, cust.getId(), cust.getContactFullName(), cust.getEmail(), cust.getContactNo());	
			}
		}
	}
	
	@Transactional
    void
    createYearlyDiscount(DiscountDto discount, Long custId, String custPerson, String custEmail, String custNo) {
		SalesOrder order = new SalesOrder();
		order.setOrderType(SalesOrderType.YEARLY_DISCOUNT);
		order.setOrderDate(new LocalDate());
		order.setCustomer(new GiftCardCustomerProfile(custId));
		order.setContactPerson(custPerson);
		order.setContactEmail(custEmail);
		order.setContactNumber(custNo);
		order.setStatus(SalesOrderStatus.DRAFT);
		String notes = "Generated yearly discount voucher "+discount.getVoucher()+"% of Rp" + discount.getPurchase() + "."; //TODO
		order.setNotes(notes);
		orderRepo.save(order);
	}
	
	@Transactional
	public BigDecimal getAccumulatedPurchase(GiftCardDiscountSchemeDto schm) {
		SalesOrderStatus[] invalidStats = new SalesOrderStatus[] {SalesOrderStatus.FOR_APPROVAL, SalesOrderStatus.DRAFT};
		SalesOrderType[] validTypes = new SalesOrderType[] {SalesOrderType.B2B_SALES, SalesOrderType.B2B_ADV_SALES};
		QSalesOrderItem qItem = QSalesOrderItem.salesOrderItem;
		BigDecimal minPurchase = schm.getMinPurchase().multiply(BILLION_MULT);
		BooleanBuilder minPurchaseFilter = new BooleanBuilder();
		if(!schm.getIncludeBelowMinPurchase()) {
			minPurchaseFilter.and(qItem.order.id.in( //include only so above or equal min purchase
				new JPASubQuery().from(qItem).where(qItem.order.customer.id.eq(schm.getCustomerId())).groupBy(qItem.order.id).having(qItem.faceAmount.sum().goe(minPurchase)).list(qItem.order.id)
			));
		}
		return new JPAQuery(em).from(qItem).where(
				qItem.order.customer.id.eq(schm.getCustomerId())
				.and(minPurchaseFilter)
				.and(qItem.order.orderDate.between(schm.getStartDate(), schm.getEndDate()))
				.and(qItem.order.status.isNotNull())
				.and(qItem.order.status.notIn(invalidStats))
				.and(qItem.order.lastUpdated.year().eq(LocalDate.now().getYear()))
				.and(qItem.order.orderType.in(validTypes))
				).singleResult(qItem.faceAmount.sum());
	}
	
	@Override
	public void generateDiscVoucherForSOYearlyDisc(LocalDate eocDate) {
		generateVoucher(eocDate, DiscountType.SO_YEARLY_DISCOUNT);
	}
	
}
