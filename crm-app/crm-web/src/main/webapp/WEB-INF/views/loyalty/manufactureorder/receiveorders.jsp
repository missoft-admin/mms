<%@ include file="../../common/taglibs.jsp" %>


<style type="text/css">
<!--
  #addProductBtn.btn-small {
    margin-bottom: 5px;
  }
-->
</style>

<c:set var="ENCTYPE" value="application/x-www-form-urlencoded"/>

<div class="modal-dialog">
  <div class="modal-content">
 
    <div class="modal-body">
    
      <form:form id="manufactureOrder" name="receiveOrderDto" modelAttribute="receiveOrderDto" data-print-url="${pageContext.request.contextPath}/manufactureorder/printreceive/" action="${pageContext.request.contextPath}/manufactureorder/receive/" method="POST" enctype="${ENCTYPE}" >
      
      <div id="contentError" class="hide alert alert-error">
        <button type="button" class="close" onclick="$(this).parent().hide();">&times;</button>
        <div><form:errors path="*"/></div>
      </div>
      
      <div class="contentMain">
      <div class="well">
        <div class="row-fluid">
          <div class="span4">
            <label for=""><spring:message code="label_purchase_order" /></label>
            <input type="text" value="${manufactureOrder.poNo} / ${manufactureOrder.orderDate}" disabled="disabled" />
          </div>
          
          <div class="span4">
            <label for=""><spring:message code="label_manufacture_order" /></label>
            <input type="text" value="${manufactureOrder.moNo} / ${manufactureOrder.moDate}" disabled="disabled" />
          </div>
        
          <div class="span4">
            <spring:message code="mo_prop_supplier" var="supplier" />
            <label for="">${supplier}</label>
            <input type="text" value="${manufactureOrder.supplierName}" disabled="disabled" />
          </div>
          
        </div>
        
        <div style="margin-top: 10px; margin-bottom: 10px;">
          <table id="detailsTable" class="table table-condensed table-compressed table-striped">
            <thead>
              <tr>
                <th><spring:message code="mo_tier_prop_name" /></th>
                <th><spring:message code="mo_tier_prop_startingseries" /></th>
                <th><spring:message code="mo_tier_prop_endingseries" /></th>
                <th><spring:message code="mo_tier_prop_ordered" /></th>
                <th><spring:message code="mo_tier_prop_served" /></th>
              </tr>
            </thead>
            <tbody>
              <c:forEach var="item" items="${manufactureOrder.tiers}" varStatus="status" >
              <tr>
                <td>${item.name.description}</td>
                <td>${item.startingSeries}</td>
                <td>${item.endingSeries}</td>
                <td>${item.quantity}</td>
                <td>${item.received}</td>
              </tr>
              </c:forEach>
            </tbody>
          </table>
        </div>
        
        <c:if test="${not empty receivedOrders}">
        <div style="margin-top: 10px; margin-bottom: 10px;">
          <table id="receivedTable" class="table table-condensed table-compressed table-striped">
            <thead>
              <tr>
                <th><spring:message code="mo_tier_prop_name" /></th>
                <th><spring:message code="mo_tier_prop_startingseries" /></th>
                <th><spring:message code="mo_tier_prop_endingseries" /></th>
                <th><spring:message code="mo_tier_prop_received" /></th>
                <th><spring:message code="loyalty_receive_receipt" /></th>
              </tr>
            </thead>
            <tbody>
              <c:forEach var="item" items="${receivedOrders}" varStatus="status" >
              <tr>
                <td>${item.cardType}</td>
                <td>${item.startingSeries}</td>
                <td>${item.endingSeries}</td>
                <td>${item.quantity}</td>
                <td>${item.receiptNo} (${item.receiveDate})</td>
              </tr>
              </c:forEach>
            </tbody>
          </table>
        </div>
        </c:if>
        </div>
        
        <div class="row-fluid">
          <div class="span4">
            <spring:message code="loyalty_receive_receivedat" var="receivedAt" />
            <label for="">${receivedAt}</label>
            <select name="receivedAt">
            <c:forEach items="${inventoryLocations}" var="item" >
              <option value="${item.key}">${item.value}</option>
            </c:forEach>
            </select>
          </div>
          
          <div class="span4">
            <spring:message code="loyalty_receive_receivedate" var="receiveDate" />
            <label for=""><b class="required">*</b> ${receiveDate}</label>
            <form:input cssClass="moDate" path="receiveDate" data-mo-date="${manufactureOrder.moDate}" placeholder="${receiveDate}" />
          </div>
          
          <div class="span4">
            <spring:message code="loyalty_receive_receipt" var="receipt" />
            <label for=""><b class="required">*</b> ${receipt}</label>
            <form:input path="receiptNo" placeholder="${receipt}" />
          </div>
        </div>
        
        
        
        <div class="pull-left" style="margin-right: 5px;">        
        <button type="button" id="addProductBtn" class="btn btn-small">+</button><br/>
        <button type="button" id="removeProductBtn" class="btn btn-small">&ndash;</button>
        </div>
        
    
        <div class="pull-left" style="width:90%;">
          <table id="productTable" class="table table-condensed table-compressed table-striped">
            <thead>
              <tr>
                <th></th>
                <th><b class="required">*</b> <spring:message code="mo_tier_prop_name" /></th>
                <th><spring:message code="mo_tier_prop_quantity" /></th>
                <th><spring:message code="mo_tier_prop_startingseries" /></th>
                <th><spring:message code="mo_tier_prop_endingseries" /></th>
                <th><spring:message code="mo_tier_prop_boxno" /></th>
              </tr>
            </thead>
            <tbody>
              <c:forEach var="item" items="${receiveOrderDto.products}" varStatus="status" >
              <tr data-index="${status.index}">
                <td><input type="checkbox" class="productRemoveFlag" /></td>
                <td>
                  <select class="input-medium" name="products[${status.index}].name">
                  <c:forEach items="${manufactureOrder.tiers}" var="tier" >
                    <option value="${tier.name.code}">${tier.name.description}</option>
                  </c:forEach>
                  </select>
                </td>
                <td><input type="text" class="input-mini modalQuantity numeric" value="${item.quantity}" name="products[${status.index}].quantity" /></td>
                <td><input type="text" class="input-medium modalStartingSeries numeric" value="${item.startingSeries}" name="products[${status.index}].startingSeries" maxlength="12" /></td>
                <td><input type="text" class="input-medium modalEndingSeries numeric" value="${item.endingSeries}" name="products[${status.index}].endingSeries" maxlength="12" /></td>
                <td><input type="text" class="input-mini" value="${item.boxNo}" name="products[${status.index}].boxNo" /></td>
              </tr>
              </c:forEach>
            </tbody>
          </table>
        </div>
      </div>
      </form:form>
    
    </div>
    <div class="modal-footer">
      <button type="button" id="" class="btn btn-default" data-dismiss="modal"><spring:message code="label_cancel"/></button>
      <button type="button" id="" data-mo-id="${manufactureOrder.id}" data-loading-text="<spring:message code="loading_text" />" class="btn btn-primary receiveOrderSave"><spring:message code="label_save" /></button>
    </div>
  </div>
</div>