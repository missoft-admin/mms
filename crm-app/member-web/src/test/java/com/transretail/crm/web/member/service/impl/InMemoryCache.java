package com.transretail.crm.web.member.service.impl;

import org.springframework.cache.concurrent.ConcurrentMapCache;
import org.springframework.cache.support.SimpleCacheManager;

import com.google.common.collect.Lists;

/**
 *
 */
public class InMemoryCache extends SimpleCacheManager {
    public InMemoryCache() {
        ConcurrentMapCache cache = new ConcurrentMapCache(MemberServiceExtImpl.CACHE_NAME);
        setCaches(Lists.newArrayList(cache));
    }
}
