package com.transretail.crm.giftcard.repo;

import com.transretail.crm.common.repo.CrmQueryDslPredicateExecutor;
import com.transretail.crm.giftcard.entity.GiftCardTransactionItem;
import com.transretail.crm.giftcard.repo.custom.GiftCardTransactionItemRepoCustom;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Repository;

/**
 *
 * @author Monte Cillo Co (mco@exist.com)
 */
@Repository
public interface GiftCardTransactionItemRepo extends CrmQueryDslPredicateExecutor<GiftCardTransactionItem, Long>, GiftCardTransactionItemRepoCustom {

    Page<GiftCardTransactionItem> findByGiftCardBarcode(String cardNo, Pageable page);
}
