package com.transretail.crm.marketing.dto;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

import org.joda.time.LocalDate;
import org.joda.time.Months;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.transretail.crm.core.util.AppConstants;

/**
 * @author Allan G. Ramirez (agramirez@exist.com)
 */
public class YearMonthDto {
    @NotNull
    @Pattern(regexp = AppConstants.YEAR_PATTERN)
    private String yearFrom;
    @NotNull
    @Pattern(regexp = AppConstants.MONTH_PATTERN)
    private String monthFrom;
    @NotNull
    @Pattern(regexp = AppConstants.YEAR_PATTERN)
    private String yearTo;
    @NotNull
    @Pattern(regexp = AppConstants.MONTH_PATTERN)
    private String monthTo;

    @JsonIgnore
    private LocalDate dateFrom;
    @JsonIgnore
    private LocalDate dateTo;

    public YearMonthDto() {

    }

    public YearMonthDto(String yearFrom, String monthFrom, String yearTo, String monthTo) {
        this.yearFrom = yearFrom;
        this.monthFrom = monthFrom;
        this.yearTo = yearTo;
        this.monthTo = monthTo;
    }

    public void setYearFrom(String yearFrom) {
        dateFrom = null;
        this.yearFrom = yearFrom;
    }

    public void setMonthFrom(String monthFrom) {
        dateFrom = null;
        this.monthFrom = monthFrom;
    }

    public void setYearTo(String yearTo) {
        dateTo = null;
        this.yearTo = yearTo;
    }

    public void setMonthTo(String monthTo) {
        dateTo = null;
        this.monthTo = monthTo;
    }

    public LocalDate getDateFrom() {
        if (dateFrom == null) {
            dateFrom = LocalDate.now()
                .withYear(Integer.valueOf(yearFrom))
                .withMonthOfYear(Integer.valueOf(monthFrom))
                .withDayOfMonth(1);
        }
        return dateFrom;
    }

    public LocalDate getDateTo() {
        if (dateTo == null) {
            dateTo = LocalDate.now()
                .withYear(Integer.valueOf(yearTo))
                .withMonthOfYear(Integer.valueOf(monthTo))
                .withDayOfMonth(1);
        }
        return dateTo;
    }

    public int getTotalMonths() {
        Months months = Months.monthsBetween(getDateFrom(), getDateTo());
        return months.getMonths() + 1;
    }
}
