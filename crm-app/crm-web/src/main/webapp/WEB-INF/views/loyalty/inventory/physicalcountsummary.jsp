<%@ include file="../../common/taglibs.jsp" %>

<style type="text/css">
<!--
  #content_summaryList .dataTable tr td:last-child {
    border-left: 0px;
  }
-->
</style>

<div class="modal hide  nofly" id="physicalCountSummaryDialog" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
   
      <div class="modal-body">
      
        <div class="form-horizontal pull-right">
          <select name="reportType" class="input-small" id="reportType">
            <c:forEach items="${reportTypes}" var="reportType">
              <option data-url="<c:url value="/loyaltycardinventory/printphysicalcount/${reportType.code}" />" value="${reportType.code}"><spring:message code="report_type_${reportType.code}" /></option>
            </c:forEach>
          </select>
          <input type="button" id="summaryPrint" value="<spring:message code="label_print" />" class="btn btn-primary"/>
        </div>
      
        <div id="content_summaryList">
          <div id="list_summary"></div>
        </div>
      
      </div>
      <div class="modal-footer">
        <button type="button" id="summaryCancel" class="btn btn-default" data-dismiss="modal"><spring:message code="label_cancel" /></button>
        <button type="button" id="summaryProcess" data-url="<c:url value="/loyaltycardinventory/physicalcount/process" />" class="btn btn-primary"><spring:message code="label_process" /></button>
      </div>
    </div>
  </div>
</div>

<script>
$(document).ready(function() {
  $list = $("#list_summary");
  $list.ajaxDataTable({
      'autoload'  : false,
      'ajaxSource' : "<c:url value="/loyaltycardinventory/physicalcount/summary" />",
      'aaSorting'   : [[ 0, "desc" ]],
      'columnHeaders' : [
           "<spring:message code="mo_tier_prop_name" />",
           "<spring:message code="physical_count_current_inventory" />",
           "<spring:message code="physical_count_label" />",
           "<spring:message code="physical_count_difference" />"
      ],
      'modelFields' : [
           {name : 'cardType', sortable : false, fieldCellRenderer : function (data, type, row) {
  	           return row.cardType.description;
           }},
           {name : 'currentInventory', sortable : false},
           {name : 'physicalCount', sortable : false},
           {name : 'difference', sortable : false}
       ]
  });
    
});

function reloadSummaryTable() {
  $("#list_summary").ajaxDataTable('search', new Object());  
}
  
</script>


