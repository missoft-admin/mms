package com.transretail.crm.core.security.service;


public interface MD5Service {

    boolean validateMd5(String keySignature, String validationKey);

    String createMD5Hash(String signature);
}
