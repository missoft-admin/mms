package com.transretail.crm.common.audit;

/**
 * @author Allan G. Ramirez (agramirez@exist.com)
 */
public enum AuditStatus {
    SUCCESS, ERROR
}
