<%@ include file="../common/taglibs.jsp"%>
<div id="content_print" class="modal hide  nofly modal-dialog">
  <div class="modal-content">
    <form:form id="printForm" cssClass="modal-form" >
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
	      <spring:message var="printArgs" code="points_label" />
	      <h4><spring:message code="label_print_args" arguments="${printArgs}" /></h4>

      </div>
      <div class="modal-body">
	<div class="hide alert alert-error" id="content_error"><button type="button" class="close" onclick="$(this).parent().hide();">&times;</button><div><form:errors path="*"/></div></div>
	<div class="row-fluid">
	  <label for="startDate" class="span4"><spring:message code="label_date_start" /></label>
	  <label for="endDate" class="span4"><spring:message code="label_date_end" /></label>
	  <label for="endDate" class="span4">&nbsp;</label>
	</div>
	<spring:message var="msg_invaliddaterange" code="points_msg_invaliddaterange" />
	<div class="row-fluid input-daterange controls" id="printDuration" data-msg-invaliddaterange="${msg_invaliddaterange}">
	  <input id="startDate" type="text" class="span4" />
	  <input id="endDate" type="text" class="span4" />
	  <select name="reportType" id="reportType">
	    <c:forEach items="${reportTypes}" var="reportType">
	      <option value="${reportType.code}"><spring:message code="report_type_${reportType.code}" /></option>
	    </c:forEach>
	  </select>
	</div>
      </div>
      <div class="modal-footer">
        <button type="saveButton" class="btn btn-primary" id="printBtn"><spring:message code="label_print"/></button>
        <button type="button" class="btn" data-dismiss="modal" id="cancelButton"><spring:message code="label_close" /></button>
      </div>
    </form:form>
  </div>
  <script src='<c:url value="/js/bootstrap/bootstrap-datepicker.js"/>' type='text/javascript'>< ![CDATA[ & nbsp; ]] ></script>
  <link href="<c:url value="/css/bootstrap/datepicker.css" />" rel="stylesheet" />
  <script type="text/javascript">
	    $(document).ready(function() {
      var $theDialog = $("#content_print");
      var $thePrintDuration = $("#printDuration");

      initFormFields();
      initDialog();
      initLinks();

      function initFormFields() {

	$thePrintDuration.datepicker({
	  autoclose: true,
	  format: "dd-mm-yyyy"
	});
      }

      function initDialog() {
	$theDialog.modal({show: false});
      }

      function initLinks() {
	$.each($(".link_print"), function(idx, aLink) {
	  $(aLink).click(function(e) {
	    print($(this).data("url"));
	    $("#content_print").modal("show");
	  });
	});
      }

      function print(inUrl) {
	$("#printForm").submit(function(e) {
	  e.preventDefault();
	  var startDateVal = $("#printForm").find("#startDate").val();
	  var endDateVal = $("#printForm").find("#endDate").val();

	  if (startDateVal === '' || endDateVal === '' || startDateVal > endDateVal) {
	    e.preventDefault();
	    var theErrorContainer = $("#content_print").find("#content_error");
	    theErrorContainer.find("div").html($("#printDuration").data("msg-invaliddaterange"));
	    theErrorContainer.show("slow");
	  } else {
	    //alert(inUrl + ( startDateVal? startDateVal : 0 ) + "/" + ( endDateVal? endDateVal : null ));
	    //$(this).attr( "action", inUrl + ( startDateVal? startDateVal : 0 ) + "/" + ( endDateVal? endDateVal : null ) );
	    $theDialog.modal("hide");
	    var selectedReportType = $("select[name='reportType']").val();
	    var url = inUrl + selectedReportType + "/" + (startDateVal ? startDateVal : 0) + "/" + (endDateVal ? endDateVal : null);
	    $(this).attr("action", url);
	    if (selectedReportType === "pdf") {
	      window.open(url, '_blank', 'toolbar=0,location=0,menubar=0');
	      $theDialog.dialog('destroy').remove();
	      return true;
	    } else if (selectedReportType === "excel") {
	      window.location.href = url;
	      return true;
	    }

	    return false;
	  }
	});
      }
    });
  </script>


</div>
