package com.transretail.crm.web.controller.util;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import com.transretail.crm.core.dto.StoreMemberDto;
import com.transretail.crm.core.service.CodePropertiesService;
import com.transretail.crm.core.service.StoreMemberService;

/**
 * @author ftopico
 */
@Component("storeMemberSchemeValidator")
public class StoreMemberSchemeValidator implements Validator {
    
    public static final String ERROR_DUPLICATE = "error_store_member_duplicate";
    public static final String ERROR_STORE_CODE_LENGTH = "error_store_member_store_length";

    @Autowired
    StoreMemberService storeMemberService;
    
    @Autowired
    MessageSource messageSource;

    @Autowired
    private CodePropertiesService codePropertiesService;

    @Override
    public boolean supports(Class<?> clazz) {
        return StoreMemberDto.class.isAssignableFrom(clazz);
    }

    @Override
    public void validate(Object target, Errors errors) {
        validateUniqueConstraints((StoreMemberDto) target,errors);
        validateStoreCodeLength((StoreMemberDto) target,errors);
    }

    private void validateStoreCodeLength(StoreMemberDto dto, Errors errors) {
        if (dto.getStore().length() > 10) {
            errors.reject(messageSource.getMessage(ERROR_STORE_CODE_LENGTH, null, LocaleContextHolder.getLocale()));
        }
    }

    private void validateUniqueConstraints(StoreMemberDto dto, Errors errors) {
        if (dto.getStore() != null && dto.getCompany() != null && dto.getCardType() != null) {
            if (storeMemberService.isStoreMemberSchemeExists(dto)) {
                Object[] args = {dto.getStore(), dto.getCompany(), dto.getCardType()};
                errors.reject(messageSource.getMessage(ERROR_DUPLICATE, args, LocaleContextHolder.getLocale()));
            }
        }
    }
    
}
