package com.transretail.crm.core.entity;

import com.transretail.crm.core.entity.lookup.LookupDetail;
import com.transretail.crm.core.entity.support.CustomAuditableEntity;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

import java.io.Serializable;

@Table(name = "CRM_MEMBER_GRP_FIELD_VALUE")
@Entity
public class MemberGroupFieldValue extends CustomAuditableEntity<Long> implements Serializable {

    @NotNull
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "MEMBER_GRP_FIELD", nullable = false)
    private MemberGroupField memberGroupField;

    @Column(name="VALUE")
    private String value;
    
    @JoinColumn(name="OPERAND")
    @ManyToOne
    private LookupDetail operand;

    public MemberGroupField getMemberGroupField() {
        return memberGroupField;
    }

    public void setMemberGroupField(MemberGroupField memberGroupField) {
        this.memberGroupField = memberGroupField;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

	public LookupDetail getOperand() {
		return operand;
	}

	public void setOperand(LookupDetail operand) {
		this.operand = operand;
	}
}
