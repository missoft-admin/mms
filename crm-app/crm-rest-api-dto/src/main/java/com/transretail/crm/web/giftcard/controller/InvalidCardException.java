package com.transretail.crm.web.giftcard.controller;


public class InvalidCardException extends RuntimeException {

    public InvalidCardException() {
	super();
    }

    public InvalidCardException(String message) {
	super(message);
    }

}
