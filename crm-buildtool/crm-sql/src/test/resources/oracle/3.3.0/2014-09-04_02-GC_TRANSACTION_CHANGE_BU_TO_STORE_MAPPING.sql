alter table CRM_GC_TRANSACTION 
  DROP column BU;

alter table CRM_GC_TRANSACTION 
  ADD PSOFT_STORE_MAPPING_ID number(19,0);

-- migration
update crm_gc_transaction t set psoft_store_mapping_id = 
  (select id from CRM_STORE_PSOFT where store_code = 
  (select store_id from store where code=t.merchant_id));