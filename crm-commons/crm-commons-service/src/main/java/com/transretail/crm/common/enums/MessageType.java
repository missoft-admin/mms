package com.transretail.crm.common.enums;


public enum MessageType {

    SUCCESS, ERROR, WARNING
}
