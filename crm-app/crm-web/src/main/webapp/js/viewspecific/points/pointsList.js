$(document).ready( function() {

	var $theList = $( "#list_point" );

	initDataTable(isEmployee, isSupplement);



	function initDataTable(employee, supplement) {
		var theHeaders = { 
				storeCode: $theList.data( "hdr-storecode" ),
				transactionType: $theList.data( "hdr-transactiontype" ),
				created: $theList.data( "hdr-created" ),
				transactionDateTime: $theList.data( "hdr-transactiondatetime" ),
				transactionNo: $theList.data( "hdr-transactionno" ),
				transactionPoints: $theList.data( "hdr-transactionpoints" ),
				transactionAmount: $theList.data( "hdr-transactionamount" ),
				transactionMedia: $theList.data( "hdr-transactionmedia" ),
				transactionContributer: $theList.data( "hdr-transactioncontributer" ),
				viewItems: $theList.data( "link-view-items" )
		};

		var theColumnHeaders = [	
		    			        theHeaders['storeCode'],
		    				 	theHeaders['transactionType'],
		    				 	theHeaders['created'],
		    				 	theHeaders['transactionDateTime'],
		    				 	theHeaders['transactionNo'],
		    				 	theHeaders['transactionAmount'],
		    				 	theHeaders['transactionMedia'],
		    				 	theHeaders['transactionPoints'],
                  ""
		    				];
		
		var theModelFields = [
		  				 	{ name 	: 'storeCode', sortable : true, fieldCellRenderer : function (data, type, row) {
		  				 		if(row.storeCode != null && row.storeCode != "")
			  		        		 return row.storeCode + " - " + row.storeName;
			  		        	else
			  		        		 return "";
		  			        }},
						 	{ name 	: 'transactionType', sortable : true },
						 	{ name 	: 'created', sortable : true, fieldCellRenderer : function (data, type, row) {
						           return ( (row.longCreated)? new Date( row.longCreated ).customize(2) : "" );
					        }},
						 	{ name 	: 'transactionDateTime', sortable : true, fieldCellRenderer : function (data, type, row) {
						           return ( (row.longTxnDate)? new Date( row.longTxnDate ).customize(2) : "" );
					        }},
						 	{ name 	: 'transactionNo', sortable : true },
						 	{ name 	: 'transactionAmount', sortable : true, fieldCellRenderer : function (data, type, row) {
						           return row.txnAmount;
					        }},
					        { name 	: 'transactionMedia', sortable : true, fieldCellRenderer : function (data, type, row) {
					        	 if(row.transactionMedia != null && row.transactionMedia != "") {
                       if (row.transactionMediaDesc !== null && row.txnEftMediaDesc !== null && row.cardNo !== null) {
                         return row.transactionMediaDesc + " - " + row.txnEftMediaDesc + " " + row.cardNo;
                       } else {
                         return row.transactionMediaDesc + ( row.txnEftMediaDesc? " - " + row.txnEftMediaDesc : "" );
                       }
                     } else {
                       return "";
                     }
					        }},
						 	{ name 	: 'transactionPoints', sortable : true, fieldCellRenderer : function (data, type, row) {
					        	 if(row.transactionPoints != null && row.transactionPoints != "")
			  		        		 return row.transactionPoints.toLocaleString();
			  		        	 else
			  		        		 return "";
					        }},
              { customCell : function(innerData, sSpecific, json) {
                if (sSpecific == 'display') {
                  console.log('display');
                  return '<a class="btn btn-primary tiptip icn-view link_itemList" data-txn-no="' + json.transactionNo + '" href="#" title="" data-original-title="View Points Transactions"></a>';
                } else {
                  return '';
                }
              }}
						 	
						 ];
		console.log((supplement === 'false') && (employee === 'false'));
		if((supplement === 'false') && (employee === 'false')) {
			theColumnHeaders[theColumnHeaders.length] = {text: theHeaders['transactionContributer'], className : "isSupplement"};
			theModelFields[theModelFields.length] = 
			{ name  : 'transactionContributer', sortable : true, fieldCellRenderer : function(data, type, row) {
		 		if(row.transactionContributer != null && row.transactionContributer.accountId != row.memberModel.accountId) {
		 			var supplement = row.transactionContributer;
		 			return supplement.accountId + " - " +  supplement.formattedMemberName;
		 		}
		 		else {
		 			return "";
		 		}
		 	}};
		}
		
		if ( $theList.data( "is-customer" ) ) {
			theColumnHeaders[theColumnHeaders.length] = "";
			theModelFields[theModelFields.length] = { customCell : function ( innerData, sSpecific, json ) {
		 			if ( sSpecific == 'display' ) {
		 				return "<div class='el-width-04' ><a class='btn pull-left icn-view tiptip' title='"+theHeaders["viewItems"]+"' onclick=\"javascript: viewPointsItems('" 
								+ json.transactionNo + "');\" href='#'>" 
								+ theHeaders['viewItems'] + "</a></div>";
			        } 
		 			else {
			              return '';
			        }       
		 		}
		    };
		}
		$theList.ajaxDataTable({
			'autoload' 		: false,
			'ajaxSource' 	: $theList.data( "url" ),
			'aaSorting'		: [[ 2, "desc" ]],
			'columnHeaders' : theColumnHeaders,
			'modelFields' 	: theModelFields
		}).on("click", ".link_itemList", function(e) {
      e.preventDefault();
      $( "#list_item" ).data( "txn-no", $( this ).data( "txn-no" ) );
      $( "#content_transactionNo" ).html( $( this ).data( "txn-no" ) );
      $("#viewTxnDialog").modal( "hide" );
      $( "#content_itemList" ).modal( "show" );
    });

		var formDataObject = new Object();
	    formDataObject.active = true;
	    //formDataObject.exceptTypes = [ 'REWARD' ];
	    $theList.ajaxDataTable( 'search', formDataObject );
	    
//	    if(supplement) {
//	    	$(".isSupplement").hide();
//	    }
	}
	
	$("#searchBtn").click(function(e) {
		e.preventDefault();
		var dateCriteria = $("#dateCriteria").val();
		$("#searchFrom").attr("name", dateCriteria + "From");
		$("#searchTo").attr("name", dateCriteria + "To");
		var formDataObj = $("#searchForm").toObject( { mode : 'first', skipEmpty : true } );
		$btnSearch = $(this);
		$btnSearch.prop('disabled', true);
		console.log(formDataObj);
		$theList.ajaxDataTable( 
			'search', 
			formDataObj, 
			function() {
				$btnSearch.prop( 'disabled', false );
		});
	});

});

function viewPointsItems( inTxnNo ) {
	$( "#list_item" ).data( "txn-no", inTxnNo );
	$( "#content_transactionNo" ).html( inTxnNo );
	$( "#content_itemList" ).modal( "show" );
}