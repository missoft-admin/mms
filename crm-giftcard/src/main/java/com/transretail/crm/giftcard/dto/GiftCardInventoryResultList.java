package com.transretail.crm.giftcard.dto;

import java.util.Collection;

import com.transretail.crm.common.service.dto.response.AbstractResultListDTO;

/**
 * @author Allan G. Ramirez (agramirez@exist.com)
 */
public class GiftCardInventoryResultList extends AbstractResultListDTO<GiftCardInventoryDto> {
    public GiftCardInventoryResultList(Collection<GiftCardInventoryDto> results, long totalElements, boolean hasPreviousPage,
        boolean hasNextPage) {
        super(results, totalElements, hasPreviousPage, hasNextPage);
    }
}
