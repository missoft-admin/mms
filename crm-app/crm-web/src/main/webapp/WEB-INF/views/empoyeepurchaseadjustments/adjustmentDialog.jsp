<%@ include file="../common/taglibs.jsp" %>

  <c:set var="ENCTYPE" value="application/x-www-form-urlencoded"/>
  <c:if test="${multipart}"><c:set var="ENCTYPE" value="multipart/form-data"/></c:if>

  <spring:message var="typeName" code="menu_item_employeepurchasetxnad_new_label"/>
  <spring:message var="dialogLabel" code="global_menu_new" arguments="${typeName}"/>

<div class="modal hide  groupModal nofly" id="adjustmentFormDialog" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog">
    
    <div class="modal-header">
      <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
      <h4>Adjust Employee Purchase Transaction</h4>
    </div>  
    
    <div class="modal-content">
   
      <div class="modal-body">
      <form:form id="employeePurchaseTxn" name="employeePurchaseTxn" modelAttribute="employeePurchaseTxn" action="${pageContext.request.contextPath}/employee/adjustments/purchases" method="POST" enctype="${ENCTYPE}" cssClass="form-reset">

            <div id="contentError" class="hide alert alert-error">
            	<button type="button" class="close" onclick="$(this).parent().hide();">&times;</button>
                <div><form:errors path="*"/></div>
            </div>
            
            <div class="contentMain row-fluid">
                <div class="row-fluid span8">
            	<div class="pull-left span6">
            		<div>
            			<spring:message code="label_com_transretail_crm_entity_employeepurchasetxnad_employeeid" var="employeeId"/>
		        		<div><label for="accountId"><b class="required">*</b> ${employeeId}</label></div>
						<div><form:input id="employeepurchasetxnad_employeeid" path="accountId" placeholder="${employeeId}" /></div>
					</div>
            	</div>
            	
            	<div class="pull-left span6">
            		<div>
            			<spring:message code="label_com_transretail_crm_entity_employeepurchasetxnad_transactionno" var="transactionNo"/>
		        		<div><label for="transactionNo"><b class="required">*</b> ${transactionNo}</label></div>
                        <spring:url value="/employee/adjustments/purchases/getdetails/" var="url" />
						<div><form:input data-url="${url}" id="employeepurchasetxnad_transactionno" path="transactionNo" placeholder="${transactionNo}" /></div>
					</div>
            	</div>
              
                
              

				<div class="clearfix"></div>            
				
				<div class="pull-left span6">
            		<div>
            			<spring:message code="employeepurchasetxnad_adjustmentamount" var="transactionAmount"/>
		        		<div><label for="transactionAmount"><b class="required">*</b> ${transactionAmount}</label></div>
						<div><form:input cssClass="signedInput" id="employeepurchasetxnad_transactionamount" path="transactionAmount" placeholder="${transactionAmount}" /></div>
					</div>
            	</div>
            	
            	<div class="pull-left span6">
            		<div>
            			<spring:message code="label_com_transretail_crm_entity_employeepurchasetxnad_reason" var="reason"/>
		        		<div><label for="reason">${reason}</label></div>
						<div><form:textarea id="employeepurchasetxnad_reason" path="reason" placeholder="${reason}" /></div>
					</div>
            	</div>
            	
            	<div class="clearfix"></div>
            	
                <c:if test="${isApprover}">
                <div class="pull-left span6">
            		<div>
            			<spring:message code="label_com_transretail_crm_entity_employeepurchasetxnad_remarks" var="remarks"/>
		        		<div><label for="reason">${remarks}</label></div>
						<div><form:textarea id="employeepurchasetxnad_remarks" path="remarks" placeholder="${remarks}" /></div>
					</div>
            	</div>
            	
            	<div class="clearfix"></div>
                </c:if>
                </div>
                
                 <div id="detailsContainer" class="row-fluid hide span4">
                 <div class="pull-left span12 well">
                    <label><spring:message code="label_com_transretail_crm_entity_employeepurchasetxnad_transactiondate" /></label>
                    <b><span id="adjTransactionDate" class="clearOnReset"></span></b><br/><br/>
                    <label><spring:message code="label_com_transretail_crm_entity_employeepurchasetxnad_paymentmedia" /></label>
                    <div class="clearOnReset" id="payments"></div>
                 </div>
                 </div>
                

            </div>
        
        </form:form>  
      
      </div>
      <div class="modal-footer container-btn">
        <c:if test="${!isApprover}">
        <button type="button" id="button_save" class="btn btn-primary"><spring:message code="label_save" /></button>
        </c:if>
        <c:if test="${isApprover}">
        <button type="button" id="button_reject" class="btn btn-primary"><spring:message code="label_reject" /></button>
        <button type="button" id="button_approve" class="btn btn-primary"><spring:message code="label_approve" /></button>
        </c:if>
        <button type="button" id="button_cancel" class="btn btn-default"><spring:message code="label_cancel"/></button>
      </div>
    </div>
  </div>
</div>
