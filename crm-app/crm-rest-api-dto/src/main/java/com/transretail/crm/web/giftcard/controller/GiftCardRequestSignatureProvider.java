package com.transretail.crm.web.giftcard.controller;

import com.fasterxml.jackson.annotation.JsonIgnore;

/**
 *
 * @author Monte Cillo Co (mco@exist.com)
 */
public interface GiftCardRequestSignatureProvider {

    @JsonIgnore
    String getSignature();
}
