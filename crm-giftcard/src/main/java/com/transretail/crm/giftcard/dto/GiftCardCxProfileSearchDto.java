package com.transretail.crm.giftcard.dto;


import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.mysema.query.types.expr.BooleanExpression;
import com.transretail.crm.common.service.dto.request.AbstractSearchFormDto;
import com.transretail.crm.giftcard.entity.QGiftCardCustomerProfile;
import com.transretail.crm.giftcard.entity.support.CustomerProfileType;


public class GiftCardCxProfileSearchDto extends AbstractSearchFormDto {

	public static enum CxProfileSearch { customerId, name, contactName, contactNo, email };

	private String email;
	private String customerId;
	private String name;
	private String contactName;
	private String contactNameLike;
	private String cxNameLike;
	private String contactNo;
	private CustomerProfileType customerType;
	private CustomerProfileType[] customerTypes;
	private String discountType;

	private String contactFullName;
	private String storeName;



	@Override
	@JsonIgnore
	public BooleanExpression createSearchExpression() {
		List<BooleanExpression> expr = new ArrayList<BooleanExpression>();
		QGiftCardCustomerProfile qModel = QGiftCardCustomerProfile.giftCardCustomerProfile;

		if ( StringUtils.isNotBlank( email ) ) {
			expr.add( qModel.email.containsIgnoreCase( email ) );
		}
		if ( StringUtils.isNotBlank( customerId ) ) {
			expr.add( qModel.customerId.containsIgnoreCase( customerId ) );
		}
		if ( StringUtils.isNotBlank( name ) ) {
			expr.add( qModel.name.containsIgnoreCase( name ) );
		}
		if ( StringUtils.isNotBlank( contactName ) ) {
			expr.add(
					qModel.contactFirstName.containsIgnoreCase( contactName )
					.or( qModel.contactLastName.containsIgnoreCase( contactName ) ) );
		}
		if ( StringUtils.isNotBlank( contactNameLike ) ) {
			expr.add(
					qModel.contactFirstName.startsWithIgnoreCase( contactNameLike )
					.or( qModel.contactLastName.startsWithIgnoreCase( contactNameLike ) ) );
		}
		if ( StringUtils.isNotBlank( cxNameLike ) ) {
			expr.add( qModel.name.containsIgnoreCase( cxNameLike ) );
		}
		if ( StringUtils.isNotBlank( contactNo ) ) {
			expr.add(
					qModel.contactNo.containsIgnoreCase( contactNo )
					.or( qModel.companyPhoneNo.containsIgnoreCase( contactNo ) ) );
		}
		if ( customerType != null ) {
			expr.add( qModel.customerType.eq( customerType ) );
		}
		if ( ArrayUtils.isNotEmpty( customerTypes ) ) {
			expr.add( qModel.customerType.in( customerTypes ) );
		}
		if ( StringUtils.isNotBlank( discountType ) ) {
			expr.add( qModel.discountType.code.equalsIgnoreCase( discountType ) );
		}

		if(StringUtils.isNotBlank(contactFullName)) {
			expr.add(
					qModel.contactFirstName.containsIgnoreCase( contactFullName )
					.or( qModel.contactLastName.containsIgnoreCase( contactFullName ) ) );
		}
		if(StringUtils.isNotBlank(storeName)) {
			expr.add(qModel.paymentStore.eq(storeName));
		}
		return BooleanExpression.allOf( expr.toArray( new BooleanExpression[ expr.size() ] ) );
	}



	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getCustomerId() {
		return customerId;
	}
	public void setCustomerId(String customerId) {
		this.customerId = customerId;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getContactName() {
		return contactName;
	}
	public void setContactName(String contactName) {
		this.contactName = contactName;
	}
	public String getContactNameLike() {
		return contactNameLike;
	}
	public void setContactNameLike(String contactNameLike) {
		this.contactNameLike = contactNameLike;
	}
	public String getCxNameLike() {
		return cxNameLike;
	}
	public void setCxNameLike(String cxNameLike) {
		this.cxNameLike = cxNameLike;
	}
	public String getContactNo() {
		return contactNo;
	}
	public void setContactNo(String contactNo) {
		this.contactNo = contactNo;
	}

	public String getDiscountType() {
		return discountType;
	}
	public void setDiscountType(String discountType) {
		this.discountType = discountType;
	}



	public CustomerProfileType getCustomerType() {
		return customerType;
	}



	public void setCustomerType(CustomerProfileType customerType) {
		this.customerType = customerType;
	}



	public CustomerProfileType[] getCustomerTypes() {
		return customerTypes;
	}



	public void setCustomerTypes(CustomerProfileType[] customerTypes) {
		this.customerTypes = customerTypes;
	}



	public String getContactFullName() {
		return contactFullName;
	}



	public void setContactFullName(String contactFullName) {
		this.contactFullName = contactFullName;
	}



	public String getStoreName() {
		return storeName;
	}



	public void setStoreName(String storeName) {
		this.storeName = storeName;
	}



}
