package com.transretail.crm.impl;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.transretail.crm.giftcard.dto.CardVendorDto;
import com.transretail.crm.giftcard.dto.CardVendorResultList;
import com.transretail.crm.giftcard.dto.CardVendorSearchDto;
import com.transretail.crm.giftcard.entity.CardVendor;
import com.transretail.crm.giftcard.repo.CardVendorRepo;
import com.transretail.crm.giftcard.service.CardVendorService;

/**
 *
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath*:/META-INF/spring/applicationContext*.xml",
    "classpath:META-INF/spring/applicationContext-test.xml"})
public class CardVendorServiceImplTest {
    @Autowired
    private CardVendorRepo repo;
    @Autowired
    private CardVendorService service;
    private Long savedCardVendorId;

    private static final String FORMAL_NAME = "name";
    private static final String MAIL = "mail";
    private static final String EMAIL = "email";
    private static final String REGION = "region";

    @Before
    public void onSetup() {
        CardVendor vendor = new CardVendor();
        vendor.setFormalName(FORMAL_NAME);
        vendor.setMailAddress(MAIL);
        vendor.setEmailAddress(EMAIL);
        vendor.setEnabled(true);
        vendor.setRegion(REGION);
        vendor.setDefaultVendor(true);
        savedCardVendorId = repo.save(vendor).getId();
    }

    @After
    public void tearDown() {
        if (savedCardVendorId != null) {
            repo.delete(savedCardVendorId);
        }
    }

    @Test
    public void getCardVendorsTest() {
        CardVendorSearchDto searchForm = new CardVendorSearchDto();
        searchForm.setFormalName(FORMAL_NAME);
        searchForm.setEnabled(true);

        CardVendorResultList resultList = service.getCardVendors(searchForm);
        assertEquals(1, resultList.getNumberOfElements());
        assertEquals(1, resultList.getTotalElements());
        assertEquals(REGION, resultList.getResults().iterator().next().getRegion());

        searchForm.setEnabled(false);
        resultList = service.getCardVendors(searchForm);
        assertEquals(0, resultList.getNumberOfElements());
        assertEquals(0, resultList.getTotalElements());
        assertEquals(0, resultList.getResults().size());
    }

    @Test
    public void saveCardVendorTest() {
        Long savedVendorId = null;
        String name = "xxxcloudxxx";
        try {
            CardVendorDto dto = new CardVendorDto();
            dto.setFormalName(name);
            dto.setMailAddress("mail1");
            dto.setEmailAddress("email1");
            dto.setEnabled(true);
            dto.setRegion("region1");
            dto.setDefaultVendor(true);

            savedVendorId = service.saveCardVendor(dto);

            assertNotNull(savedVendorId);
            assertTrue(repo.exists(savedVendorId));
        } finally {
            if (savedVendorId != null) {
                repo.delete(savedVendorId);
            }
        }
    }

    @Test
    public void updateCardVendorTest() {
        CardVendorDto dto = new CardVendorDto();
        dto.setFormalName("newname");
        dto.setMailAddress("newmail");
        dto.setEmailAddress("newemail");
        dto.setRegion("newregion");
        service.updateCardVendor(savedCardVendorId, dto);

        CardVendor fromDb = repo.findOne(savedCardVendorId);
        assertEquals("newname", fromDb.getFormalName());
        assertEquals("newmail", fromDb.getMailAddress());
        assertEquals("newemail", fromDb.getEmailAddress());
        assertEquals("newregion", fromDb.getRegion());
    }

    @Test
    public void getCardVendorByIdTest() {
        CardVendorDto dto = service.getCardVendorById(savedCardVendorId);
        assertEquals(FORMAL_NAME, dto.getFormalName());
        assertEquals(MAIL, dto.getMailAddress());
        assertEquals(EMAIL, dto.getEmailAddress());
        assertEquals(REGION, dto.getRegion());
    }
}
