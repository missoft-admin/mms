alter table CRM_STORE_PSOFT 
        drop constraint FK_278449ghmjrywd3es587lfx79;

alter table CRM_STORE_PSOFT modify STORE_CODE varchar(50);

update CRM_STORE_PSOFT a
    set a.STORE_CODE = (select b.CODE from STORE b where b.STORE_ID=a.STORE_CODE);

alter table CRM_STORE_PSOFT 
        add constraint UK_278449ghmjrywd3es587lfx79 unique (STORE_CODE);

insert into CRM_REF_LOOKUP_HDR(CODE, DESCRIPTION) VALUES ('STO002', 'TERRITORY');
insert into CRM_REF_LOOKUP_DTL(CODE, DESCRIPTION, HDR, STATUS) VALUES ('STER000', 'Territory 1', 'STO002', 'ACTIVE');
insert into CRM_REF_LOOKUP_DTL(CODE, DESCRIPTION, HDR, STATUS) VALUES ('STER001', 'Territory 2', 'STO002', 'ACTIVE');