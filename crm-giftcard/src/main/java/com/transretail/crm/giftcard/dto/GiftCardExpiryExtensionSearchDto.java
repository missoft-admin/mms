package com.transretail.crm.giftcard.dto;

import java.util.ArrayList;
import java.util.List;

import org.joda.time.LocalDate;
import org.springframework.util.StringUtils;

import com.mysema.query.types.expr.BooleanExpression;
import com.transretail.crm.common.service.dto.request.AbstractSearchFormDto;
import com.transretail.crm.common.util.DateUtil;
import com.transretail.crm.core.util.BooleanExprUtil;
import com.transretail.crm.giftcard.entity.QGiftCardExpiryExtension;
import com.transretail.crm.giftcard.entity.support.GiftCardDateExtendStatus;

/**
 * @author ftopico
 */
public class GiftCardExpiryExtensionSearchDto extends AbstractSearchFormDto {

    private String extendNo;
    private String orderNo;
    private Long seriesTo;
    private Long seriesFrom;
    private String filedBy;
    private GiftCardDateExtendStatus status;
    private LocalDate dateFrom;
    private LocalDate dateTo;
    
    public String getExtendNo() {
        return extendNo;
    }
    public String getFiledBy() {
        return filedBy;
    }
    public void setExtendNo(String extendNo) {
        this.extendNo = extendNo;
    }
    public void setFiledBy(String filedBy) {
        this.filedBy = filedBy;
    }
    public String getSalesOrder() {
        return orderNo;
    }
    public void setSalesOrder(String salesOrder) {
        this.orderNo = salesOrder;
    }
    public GiftCardDateExtendStatus getStatus() {
        return status;
    }
    public void setStatus(GiftCardDateExtendStatus status) {
        this.status = status;
    }
    public LocalDate getDateFrom() {
        return dateFrom;
    }
    public void setDateFrom(LocalDate dateFrom) {
        this.dateFrom = dateFrom;
    }
    public LocalDate getDateTo() {
        return dateTo;
    }
    public void setDateTo(LocalDate dateTo) {
        this.dateTo = dateTo;
    }
    public Long getSeriesTo() {
        return seriesTo;
    }
    public void setSeriesTo(Long seriesTo) {
        this.seriesTo = seriesTo;
    }
    public Long getSeriesFrom() {
        return seriesFrom;
    }
    public void setSeriesFrom(Long seriesFrom) {
        this.seriesFrom = seriesFrom;
    }
    @Override
    public BooleanExpression createSearchExpression() {
        List<BooleanExpression> expressions = new ArrayList<BooleanExpression>();
        QGiftCardExpiryExtension qGiftCardExpiryExtension = QGiftCardExpiryExtension.giftCardExpiryExtension;
        
        if (StringUtils.hasText(String.valueOf(extendNo))) {
            expressions.add(BooleanExprUtil.INSTANCE.isStringPropertyLike(qGiftCardExpiryExtension.extendNo, extendNo));
        }
        
        if (StringUtils.hasText(String.valueOf(orderNo))) {
            expressions.add(BooleanExprUtil.INSTANCE.isStringPropertyLike(qGiftCardExpiryExtension.order.orderNo, orderNo));
        }
        
        if(seriesFrom != null && seriesTo != null) {
            expressions.add(qGiftCardExpiryExtension.seriesFrom.goe(seriesFrom).and(qGiftCardExpiryExtension.seriesTo.loe(seriesTo)));
        } else if (seriesFrom != null){
            expressions.add(qGiftCardExpiryExtension.seriesFrom.goe(seriesFrom));
        } else if(seriesTo != null) {
            expressions.add(qGiftCardExpiryExtension.seriesTo.loe(seriesTo));
        }
        
        if (StringUtils.hasText(String.valueOf(filedBy))) {
            expressions.add(BooleanExprUtil.INSTANCE.isStringPropertyLike(qGiftCardExpiryExtension.filedBy, filedBy));
        }
        
        if (StringUtils.hasText(String.valueOf(status))) {
            expressions.add(BooleanExprUtil.INSTANCE.isEnumPropertyIn(qGiftCardExpiryExtension.status, status));
        }
        
        if(dateFrom != null && dateTo != null) {
            expressions.add(qGiftCardExpiryExtension.createDate.between(dateFrom, DateUtil.getEndOfDay(dateTo)));
        } else if (dateFrom != null){
            expressions.add(qGiftCardExpiryExtension.createDate.goe(dateFrom));
        } else if(dateTo != null) {
            expressions.add(qGiftCardExpiryExtension.createDate.loe(DateUtil.getEndOfDay(dateTo)));
        }
        
        return BooleanExpression.allOf(expressions.toArray(new BooleanExpression[expressions.size()]));
    }
    
}
