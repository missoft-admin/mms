<%@ include file="/WEB-INF/views/common/taglibs.jsp" %>
<form id="searchForm" class="form-horizontal row-fluid well">
  <div class="span6">
    <div class="control-group">
      <label class="control-label" for="q_cardOrderNo"><spring:message code="gc.order.label.cardOrderNo"/></label>

      <div class="controls">
        <input type="text" id="q_cardOrderNo" name="cardOrderNo"/>
      </div>
    </div>
    <div class="control-group">
      <label class="control-label" for="q_orderDate_from"><spring:message code="gc.order.label.orderDate.from"/></label>

      <div class="controls">
        <input type="text" id="q_orderDate_from" name="orderDateFrom" class="date" placeholder="dd-MM-yyyy"/>
      </div>
    </div>
    <div class="control-group">
      <label class="control-label" for="q_orderDate_to"><spring:message code="gc.order.label.orderDate.to"/></label>

      <div class="controls">
        <input type="text" id="q_orderDate_to" name="orderDateTo" class="date" placeholder="dd-MM-yyyy"/>
      </div>
    </div>
  </div>
  <div class="span6">
    <div class="control-group">
      <label class="control-label" for="q_vendor"><spring:message code="gc.order.label.vendor"/></label>

      <div class="controls">
        <%--TODO: Create and use vendor search instead of displaying everything in dropdown--%>
        <select id="q_vendor" name="vendorId">
          <option value="">&nbsp;</option>
          <c:forEach items="${vendors.results}" var="vendor">
            <option value="${vendor.id}">${vendor.name}</option>
          </c:forEach>
        </select>
      </div>
    </div>
    <c:if test="${fn:length(statuses) gt 1}">
      <div class="control-group">
        <label class="control-label" for="q_status"><spring:message code="gc.order.label.status"/></label>

        <div class="controls">
          <select id="q_status" name="status">
            <option value="">&nbsp;</option>
            <c:forEach items="${statuses}" var="status">
              <option value="${status}">${status}</option>
            </c:forEach>
          </select>
        </div>
      </div>
    </c:if>
  </div>
  <div class="form-cta">
        <button id="search" type="button" class="btn btn-primary"><spring:message code="search"/></button>
        <button id="addOrder" type="button" class="btn btn-primary"><spring:message code="add"/></button>
  </div>
</form>