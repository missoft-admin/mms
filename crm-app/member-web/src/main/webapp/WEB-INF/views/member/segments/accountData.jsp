<%@ include file="../../taglibs.jsp"%>


    <div class="tab-pane" id="membership">
    <fieldset class="container-fluid">
      <div class="control-group">
        <label class="control-label"><spring:message code="member.update.accountno" /></label>
        <div class="controls">
          <span class="bootstrap-input input-xlarge">${form.accountId}</span>
        </div>
      </div>

      <c:if test="${form.memberTypeCode != 'MTYP002'}">
      <div class="control-group">
        <label class="control-label"><spring:message code="card_no" /></label>
        <div class="controls">
          <span class="bootstrap-input input-xlarge">${form.accountId}</span>
        </div>
      </div>
      </c:if>

      <div class="control-group">
        <spring:message code="member_type" var="memberType" />
        <form:label path="memberTypeCode" title="${memberType}" cssClass="control-label" cssErrorClass="control-label error">${memberType}</form:label>
        <div class="controls">
          <c:forEach var="memType" items="${memberTypes}">
            <c:if test="${memType.code == form.memberTypeCode}">
              <span class="bootstrap-input input-xlarge">${memType.description}</span>
            </c:if>
          </c:forEach>
          <form:hidden path="memberTypeCode" />
          <%-- <form:select path="memberTypeCode" items="${memberTypes}" title="${memberType}" itemValue="code" itemLabel="description" cssClass="input-xlarge" /> --%>
        </div>
      </div>

      <div class="control-group employeeDetails">
        <form:label path="empTypeCode" title="Employee Type" cssClass="control-label" cssErrorClass="control-label error">
          <spring:message code="employee_type" />
        </form:label>
        <div class="controls">
          <c:forEach var="empType" items="${employeeTypes}">
            <c:if test="${empType.code == form.empTypeCode}">
              <span class="bootstrap-input input-xlarge">${empType.description}</span>
            </c:if>
          </c:forEach>
          <form:hidden path="empTypeCode" />
          <!--
          <form:select disabled="true" path="empTypeCode" items="${employeeTypes}"
            title="Employee Type" itemValue="code" itemLabel="description"
            cssClass="input-xlarge" /> -->
        </div>
      </div>

      <div class="control-group employeeDetails">
        <label class="control-label"><spring:message code="status" /></label>
        <div class="controls">
          <span class="bootstrap-input input-xlarge">${form.status}</span>
        </div>
      </div>

      <div class="control-group">
        <form:label path="preferedStoreCode" title="Registered Store"
          cssClass="control-label" cssErrorClass="control-label error">
          <spring:message code="registered_store" />
        </form:label>
        <div class="controls">
          <c:forEach var="registeredStore" items="${stores}">
            <c:if test="${registeredStore.code == form.preferedStoreCode}">
              <span class="bootstrap-input input-xlarge">${registeredStore.code} - ${registeredStore.name}</span>
            </c:if>
          </c:forEach>
          <form:hidden path="preferedStoreCode" />
          <!--
          <form:select path="preferedStoreCode" items="${stores}"
            title="Preferred Store" itemValue="code" itemLabel="name"
            cssClass="input-xlarge" /> -->
        </div>
      </div>

      <div class="control-group">
        <label class="control-label"><spring:message code="registration_date" /></label>
        <div class="controls">
          <span class="bootstrap-input input-xlarge">${form.registrationDate}</span>
        </div>
      </div>

      <c:if test="${form.memberTypeCode != 'MTYP002'}">
      <div class="control-group">
        <label class="control-label"><spring:message code="loyalty_card_issued" /></label>
        <div class="controls">
          <span class="bootstrap-input input-xlarge">${form.loyaltyCardIssued}</span>
        </div>
      </div>

      <div class="control-group">
        <label class="control-label"><spring:message code="loyalty_card_expiry" /></label>
        <div class="controls">
          <span class="bootstrap-input input-xlarge">${form.loyaltyCardExpiry}</span>
        </div>
      </div>
      </c:if>
    </fieldset>
    </div>