package com.transretail.crm.web.controller;


import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.transretail.crm.core.dto.MemberDto;
import com.transretail.crm.core.dto.MemberResultList;
import com.transretail.crm.core.dto.MemberSearchDto;
import com.transretail.crm.core.dto.PointsConfigDto;
import com.transretail.crm.core.dto.PointsResultList;
import com.transretail.crm.core.dto.PointsSearchDto;
import com.transretail.crm.core.dto.PosTxItemResultList;
import com.transretail.crm.core.dto.PosTxItemSearchDto;
import com.transretail.crm.core.entity.MemberModel;
import com.transretail.crm.core.entity.enums.MemberStatusSearchLookup;
import com.transretail.crm.core.entity.enums.PointTxnType;
import com.transretail.crm.core.entity.enums.PointsSearchCriteria;
import com.transretail.crm.core.entity.enums.ReportType;
import com.transretail.crm.core.entity.enums.SearchCriteria;
import com.transretail.crm.core.entity.enums.VoucherTransactionType;
import com.transretail.crm.core.entity.lookup.LookupDetail;
import com.transretail.crm.core.entity.lookup.Store;
import com.transretail.crm.core.service.CodePropertiesService;
import com.transretail.crm.core.service.EmployeePurchaseService;
import com.transretail.crm.core.service.LookupService;
import com.transretail.crm.core.service.MemberService;
import com.transretail.crm.core.service.PointsTxnManagerService;
import com.transretail.crm.core.service.PosTransactionService;
import com.transretail.crm.core.service.StoreService;
import com.transretail.crm.web.controller.util.ControllerResponse;


@RequestMapping("/member/points")
@Controller
public class PointsController {

	@Autowired
	PointsTxnManagerService pointsManagerService;
	@Autowired
	MemberService memberService;
	@Autowired
	CodePropertiesService codePropertiesService ;
    @Autowired
    PosTransactionService posTransactionService;
    @Autowired
    EmployeePurchaseService employeePurchaseService;
    @Autowired
    LookupService lookupService;
    @Autowired
    StoreService storeService;



	private static final String PATH_POINTS 	 		= "/member/points";

	private static final String UI_ATTR_POINTS_CONFIG 	= "pointsConfig";
	private static final String UI_ATTR_MEMBER_TYPES 	= "memberTypes";
	private static final String UI_ATTR_FIELD_SEARCHBY     = "memberCriteria";
	private static final String UI_ATTR_REGISTERED_STORE	= "preferredStore";
	private static final String UI_ATTR_STATUS			= "status";

	private static final String UIREF_DATEFORMAT 	= "dd-MM-yyyy";

	private static final Logger LOGGER = LoggerFactory.getLogger(PointsController.class);

	@InitBinder
	private void initBinder( WebDataBinder binder ) {
		SimpleDateFormat dateFormat = new SimpleDateFormat( UIREF_DATEFORMAT );
		dateFormat.setLenient(false);
		binder.registerCustomEditor(Date.class, new CustomDateEditor( dateFormat, true ) );
	}



	@RequestMapping( value="/{accountId}", produces = "text/html" )
    public String showPoints( @PathVariable( value="accountId" ) String inAccountId, Model inUiModel ) {

		MemberModel theMember = memberService.findByAccountId( inAccountId );
		if ( null == theMember ) {
			return PATH_POINTS;
		}
		theMember.setTotalPoints( pointsManagerService.synchronizePointsTxn( theMember ) );
		MemberDto memberDto = new MemberDto(theMember);
		if ( StringUtils.isNotBlank( theMember.getRegisteredStore() ) ) {
			Store store = storeService.getStoreByCode(theMember.getRegisteredStore());
			if ( null != store ) {
				memberDto.setRegisteredStoreName(store.getName());
			}
		}
    	inUiModel.addAttribute( "member", memberDto );
    	inUiModel.addAttribute( "isCustomer", theMember.getMemberType().getCode().equalsIgnoreCase( codePropertiesService.getDetailMemberTypeIndividual() ) || 
    			theMember.getMemberType().getCode().equalsIgnoreCase( codePropertiesService.getDetailMemberTypeCompany() ) );
    	inUiModel.addAttribute( "dateCriteria", PointsSearchCriteria.values() );
    	
    	inUiModel.addAttribute("reportTypes", ReportType.values());
    	inUiModel.addAttribute("isSupplement", StringUtils.isNotBlank(memberDto.getParent()));
    	boolean isEmployee = memberDto.getMemberType().getCode().equals(
    			codePropertiesService.getDetailMemberTypeEmployee());
    	inUiModel.addAttribute("isEmployee", isEmployee);
    	
    	List<MemberDto> supplements = memberService.findSupplementaryMembersByParentId(inAccountId);
    	LOGGER.debug("SUPPLEMENTS: " + supplements.size());
    	inUiModel.addAttribute("supplements", supplements);
    	Double totalRewards = pointsManagerService.retrieveTotal( PointTxnType.REWARD, inAccountId );
    	inUiModel.addAttribute( "totalRewards", null != totalRewards? totalRewards : 0D );
    	inUiModel.addAttribute( "totalPurchaseTxn", isEmployee? employeePurchaseService.getTotal( 
    		new VoucherTransactionType[]{ VoucherTransactionType.PURCHASE }, inAccountId ) :
    			pointsManagerService.retrieveTotalPurchase( new PointTxnType[]{ PointTxnType.EARN }, inAccountId ) );
        return PATH_POINTS;
    }


	@RequestMapping( value = "/list/{accountId}", method = RequestMethod.POST )
    public @ResponseBody PointsResultList listPoints( @PathVariable( value="accountId" ) String inAccountId,
    		@RequestBody PointsSearchDto inSearchForm ) {
		inSearchForm.setAccountId( inAccountId );
    	return pointsManagerService.searchPoints( inSearchForm );
    }



	@ResponseBody
    @RequestMapping(value = "/item/list", method = RequestMethod.POST)
    public PosTxItemResultList getPoints(@RequestBody PosTxItemSearchDto dto) {
        return posTransactionService.getPosTxItems(dto);
    }



	@RequestMapping(value="/config", produces="text/html")
	public String viewConfig(Model uiModel) {
		PointsConfigDto config = pointsManagerService.getPointsConfig();
		uiModel.addAttribute("searchModelAttribute", "searchModel");
		uiModel.addAttribute(UI_ATTR_POINTS_CONFIG, config);
		uiModel.addAttribute(UI_ATTR_MEMBER_TYPES, lookupService.getActiveDetailsByHeaderCode(codePropertiesService.getHeaderMemberType()));
		uiModel.addAttribute(UI_ATTR_FIELD_SEARCHBY, SearchCriteria.values());
		uiModel.addAttribute(UI_ATTR_STATUS, MemberStatusSearchLookup.values());
		uiModel.addAttribute(UI_ATTR_REGISTERED_STORE, storeService.getAllStores());
		List<LookupDetail> memberTypes = lookupService.getDetailsByHeaderCode(codePropertiesService.getHeaderMemberType());
		uiModel.addAttribute("memberType", memberTypes);
		return "/points/config";
	}
	
	@RequestMapping(value = "/config/list", method = RequestMethod.POST)
    public @ResponseBody MemberResultList list(
    		@RequestBody MemberSearchDto dto) {
    	return memberService.filterMemberTypes(dto);
    }

	@RequestMapping(value="/config", method=RequestMethod.POST, produces="application/json")
	@ResponseBody
	public ControllerResponse saveConfig(
			@RequestBody PointsConfigDto pointsConfig,
			Model uiModel) {
		LOGGER.debug("CONFIG: " + pointsConfig.getPointValue());
		LOGGER.debug("CONFIG: " + pointsConfig.getSuspendedTypes());
		ControllerResponse response = new ControllerResponse();
		response.setSuccess(true);
		
		pointsManagerService.updatePointsConfig(pointsConfig.getPointValue(), pointsConfig.getSuspendedTypes());
		return response;
	}
}
