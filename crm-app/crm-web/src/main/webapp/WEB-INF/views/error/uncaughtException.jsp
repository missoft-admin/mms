<%@ page language="java" isErrorPage="true" %>
<%@ include file="../common/taglibs.jsp" %>

<h2><spring:message code="error_uncaughtexception_title"/></h2>

<p>
  <spring:message code="error_uncaughtexception_problemdescription"/>
</p>
<button class="btn btn-primary" style="height: 40px; font-size: 14px;">Show Details</button>
<pre style="display: none;">
<%
  if (exception != null) {
    if (exception.getMessage() != null && exception.getMessage().contains("Could not open JPA")) {
      out.println("Unable to access DBEE");
    } else {
      exception.printStackTrace(new java.io.PrintWriter(out));
    }
  } else if ((Exception) request.getAttribute("javax.servlet.error.exception") != null) {
    if (((Exception) request.getAttribute("javax.servlet.error.exception")).getMessage() != null && ((Exception) request.getAttribute("javax.servlet.error.exception")).getMessage().contains("Could not open JPA")) {
      out.println("Unable to access DBEE");
    } else {
      ((Exception) request.getAttribute("javax.servlet.error.exception")).printStackTrace(new java.io.PrintWriter(out));
    }
  }
%>
</pre>
</div>
<script type="text/javascript">
  $( document ).ready( function () {
    $( 'button' ).click( function () {
      $( 'pre' ).show();
    } );
  } );
</script>