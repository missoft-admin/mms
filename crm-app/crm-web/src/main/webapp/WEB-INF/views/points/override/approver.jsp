<%@include file="../../common/messages.jsp" %>


<div id="content_approver">

  <div class="page-header page-header2"><h1><spring:message code="points_label_overridesetup" /></h1></div>

  <spring:message code="role_admin" var="role_admin"/>

  <security:authorize ifAnyGranted="${role_admin}">
  <div class="pull-right">
    <a href="#" class="btn btn-primary" data-url="<c:url value="/points/override/approver/create"/>" id="link_createApprover">
      <spring:message code="points_label_addadjustpointsapproverentry" />
    </a>
  </div>
  </security:authorize>
  <div class="clearfix"></div>

  <div id="form_approver"></div>
  <div id="list_approvers" class="data-content-02"><jsp:include page="approverList.jsp"/></div>


  <script src='<c:url value="/js/viewspecific/points/override/approver.js"/>' type='text/javascript'><![CDATA[&nbsp; ]]></script>
  <link href="<c:url value="/styles/temp.css" />" rel="stylesheet" />


</div>