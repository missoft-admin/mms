package com.transretail.crm.loyalty.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.Type;
import org.joda.time.LocalDate;

import com.transretail.crm.core.entity.lookup.LookupDetail;
import com.transretail.crm.core.entity.support.CustomAuditableEntity;

@Table(name = "CRM_LC_INVENTORY")
@Entity
public class LoyaltyCardInventory extends CustomAuditableEntity<Long> implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@Column(name = "SEQUENCE")
	private String sequence;
	
	// (sequence + ean13)
	@Column(name = "BARCODE")
	private String barcode;
	
	@Column(name = "LOCATION")
	private String location;
	
	@Column(name = "TRANSFER_TO")
	private String transferTo;
	
	@Column(name = "NEW_TRANSFER_TO")
	private String newTransferTo;
	
	@Column(name = "BATCH_REF_NO")
	private String batchRefNo;

    @ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "STATUS")
	private LookupDetail status;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "PRODUCT")
	private ManufactureOrderTier product;
	
	@Column(name = "RECEIVE_DATE")
    @Type(type = "org.jadira.usertype.dateandtime.joda.PersistentLocalDate")
	private LocalDate receiveDate;
	
	@Column(name = "ACTIVATION_DATE")
    @Type(type = "org.jadira.usertype.dateandtime.joda.PersistentLocalDate")
	private LocalDate activationDate;
	
	@Column(name = "EXPIRY_DATE")
    @Type(type = "org.jadira.usertype.dateandtime.joda.PersistentLocalDate")
	private LocalDate expiryDate;
	
	@Column(name = "RECEIVED_AT")
	private String receivedAt;
	
	@Column(name = "RECEIPT_NO")
	private String receiptNo;
	
	@Column(name = "ALLOCATE_DATE")
    @Type(type = "org.jadira.usertype.dateandtime.joda.PersistentLocalDate")
	private LocalDate allocateDate;;
	
	@Column(name = "REFERENCE_NO")
	private String referenceNo;
	
	@Column(name = "ENCODED_BY")
	private String encodedBy;
	
	@Column(name = "BOX_NO")
	private String boxNo;
	
	@Column(name = "FOR_BURNING")
    @Type(type = "yes_no")
	private Boolean forBurning;
	
	@Column(name = "MEMBER_ID")
	private Long memberId;
	
	public String getSequence() {
		return sequence;
	}

	public void setSequence(String sequence) {
		this.sequence = sequence;
	}

	public String getBarcode() {
		return barcode;
	}

	public void setBarcode(String barcode) {
		this.barcode = barcode;
	}

	public LookupDetail getStatus() {
		return status;
	}

	public void setStatus(LookupDetail status) {
		this.status = status;
	}

	public ManufactureOrderTier getProduct() {
		return product;
	}

	public void setProduct(ManufactureOrderTier product) {
		this.product = product;
	}

	public LocalDate getReceiveDate() {
		return receiveDate;
	}

	public void setReceiveDate(LocalDate receiveDate) {
		this.receiveDate = receiveDate;
	}

	public LocalDate getActivationDate() {
		return activationDate;
	}

	public void setActivationDate(LocalDate activationDate) {
		this.activationDate = activationDate;
	}

	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		this.location = location;
	}

	public String getTransferTo() {
		return transferTo;
	}

	public void setTransferTo(String transferTo) {
		this.transferTo = transferTo;
	}

	public String getReceivedAt() {
		return receivedAt;
	}

	public void setReceivedAt(String receivedAt) {
		this.receivedAt = receivedAt;
	}

	public String getReceiptNo() {
		return receiptNo;
	}

	public void setReceiptNo(String receiptNo) {
		this.receiptNo = receiptNo;
	}

	public LocalDate getAllocateDate() {
		return allocateDate;
	}

	public void setAllocateDate(LocalDate allocateDate) {
		this.allocateDate = allocateDate;
	}

	public String getReferenceNo() {
		return referenceNo;
	}

	public void setReferenceNo(String referenceNo) {
		this.referenceNo = referenceNo;
	}

	public String getEncodedBy() {
		return encodedBy;
	}

	public void setEncodedBy(String encodedBy) {
		this.encodedBy = encodedBy;
	}

	public String getBoxNo() {
		return boxNo;
	}

	public void setBoxNo(String boxNo) {
		this.boxNo = boxNo;
	}

	public String getBatchRefNo() {
		return batchRefNo;
	}

	public void setBatchRefNo(String batchRefNo) {
		this.batchRefNo = batchRefNo;
	}

	public LocalDate getExpiryDate() {
		return expiryDate;
	}

	public void setExpiryDate(LocalDate expiryDate) {
		this.expiryDate = expiryDate;
	}

	public String getNewTransferTo() {
		return newTransferTo;
	}

	public void setNewTransferTo(String newTransferTo) {
		this.newTransferTo = newTransferTo;
	}

	public Boolean getForBurning() {
		return forBurning;
	}

	public void setForBurning(Boolean forBurning) {
		this.forBurning = forBurning;
	}

	public Long getMemberId() {
		return memberId;
	}

	public void setMemberId(Long memberId) {
		this.memberId = memberId;
	}
	
	
}
