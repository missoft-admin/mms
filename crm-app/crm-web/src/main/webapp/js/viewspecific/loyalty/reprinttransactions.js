$(document).ready(function() {
	
	initFields();
	initButtons();
	
	var $errorMessagesContainer = $('div.errorMessages' );
	
	function initFields() {
		
		$("input[name='dateFrom']").datepicker({
			autoclose : true,
			format : 'dd-mm-yyyy'
		}).on('changeDate', function(selected) {
			startDate = new Date(selected.date.valueOf());
			startDate.setDate(startDate.getDate(new Date(selected.date.valueOf())));
			$("input[name='dateTo']").datepicker('setStartDate', startDate);
		});
		
		$("input[name='dateTo']").datepicker({
			autoclose : true,
			format : 'dd-mm-yyyy'
		}).on('changeDate', function(selected) {
			endDate = new Date(selected.date.valueOf());
			endDate.setDate(endDate.getDate(new Date(selected.date.valueOf())));
			$("input[name='dateFrom']").datepicker('setEndDate', endDate);
		});
	}
	
	function initButtons() {
		$("#reprintBtn").click(function(e) {
			e.preventDefault();
			print();
		});
	}
	
	var showErrorMessage = function(message) {
	      $errorMessagesContainer.html('<div class="alert alert-error">' +
	              '<button type="button" class="close" data-dismiss="alert">&times;</button>' +
	              message +
	              '</div>');
	};
	
	function print() {
		$.post($("#reprintTxn").attr("action") + "/validate" , $("#reprintTxn").serialize(), function(errorMessages) {
			if (errorMessages.length > 0) {
				var errorInfo = "1. " + errorMessages[0];
				for ( var i = 1; i < errorMessages.length; i++ ) {
					errorInfo += "<br>" + (i + 1) + ". " + errorMessages[i];
				}
				showErrorMessage(errorInfo);
			} else {
				var url = $("#reprintTxn").attr("action") + "?" + $("#reprintTxn").serialize();
				window.open(url, '_blank', 'toolbar=0,location=0,menubar=0' );
				$("#reprintModal").modal("hide");	
			}
			
        }, "json");
	}
});

