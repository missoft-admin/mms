package com.transretail.crm.impl;

import java.math.BigDecimal;
import java.util.List;

import junit.framework.Assert;

import org.joda.time.DateTime;
import org.joda.time.LocalDate;
import org.junit.After;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.TestingAuthenticationToken;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.context.SecurityContextImpl;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.google.common.collect.Lists;
import com.transretail.crm.common.service.dto.request.PageSortDto;
import com.transretail.crm.common.service.dto.response.ResultList;
import com.transretail.crm.core.security.model.impl.CustomSecurityUserDetailsImpl;
import com.transretail.crm.giftcard.dto.B2BExhibitionCountDto;
import com.transretail.crm.giftcard.dto.B2BExhibitionDto;
import com.transretail.crm.giftcard.dto.B2BExhibitionGcDto;
import com.transretail.crm.giftcard.entity.B2BExhibition;
import com.transretail.crm.giftcard.entity.CardVendor;
import com.transretail.crm.core.entity.CrmForPeoplesoftConfig;
import com.transretail.crm.core.entity.CrmForPeoplesoftConfig.TransactionType;
import com.transretail.crm.giftcard.entity.GiftCardInventory;
import com.transretail.crm.giftcard.entity.GiftCardOrder;
import com.transretail.crm.giftcard.entity.ProductProfile;
import com.transretail.crm.giftcard.entity.QGiftCardInventory;
import com.transretail.crm.giftcard.entity.support.GiftCardInventoryStatus;
import com.transretail.crm.giftcard.entity.support.GiftCardOrderStatus;
import com.transretail.crm.giftcard.entity.support.ProductProfileStatus;
import com.transretail.crm.giftcard.repo.B2BExhibitionRepo;
import com.transretail.crm.giftcard.repo.CardVendorRepo;
import com.transretail.crm.core.repo.CrmForPeoplesoftConfigRepo;
import com.transretail.crm.giftcard.repo.GiftCardInventoryRepo;
import com.transretail.crm.giftcard.repo.GiftCardOrderRepo;
import com.transretail.crm.giftcard.repo.GiftCardTransactionRepo;
import com.transretail.crm.giftcard.repo.ProductProfileRepo;
import com.transretail.crm.giftcard.service.B2bExhibitionService;

/**
 *
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath*:/META-INF/spring/applicationContext*.xml",
    "classpath:META-INF/spring/applicationContext-test.xml"})
public class B2BExhibitionServiceTest {
	
	@Autowired
	private B2bExhibitionService service;
	@Autowired
	private B2BExhibitionRepo repo;
	@Autowired
	private GiftCardInventoryRepo inventoryRepo;
	@Autowired
	private ProductProfileRepo profileRepo;
	@Autowired
	private GiftCardOrderRepo orderRepo;
	@Autowired
	private CardVendorRepo cardVendorRepo;
	@Autowired
	private CrmForPeoplesoftConfigRepo configRepo;
	@Autowired
	private GiftCardTransactionRepo txnRepo;
	
	private Long id;
	
	@BeforeClass
    public static void setupCurrentUser() {
        CustomSecurityUserDetailsImpl user = new CustomSecurityUserDetailsImpl();
        user.setUserId(1L);
        user.setUsername("user");
        user.setStoreCode("20001");
        List<GrantedAuthority> authorities = Lists.newArrayList();
        authorities.add(new SimpleGrantedAuthority("ROLE_MERCHANT_SERVICE"));
        user.setAuthorities(authorities);

        SecurityContextImpl securityContext = new SecurityContextImpl();
        securityContext.setAuthentication(new TestingAuthenticationToken(user, ""));
        SecurityContextHolder.setContext(securityContext);
    }
	
	
	@Before
	public void setup() {
		B2BExhibition exh = new B2BExhibition();
		exh.setTaxableAmount(new BigDecimal(180000));
		exh.setDiscount(BigDecimal.ZERO);
		exh.setTotalPayment(new BigDecimal(200000));
		id = repo.save(exh).getId();
		
		
		CrmForPeoplesoftConfig config = new CrmForPeoplesoftConfig();
		config.setTransactionType(TransactionType.B2C_ACTIVATION);
		configRepo.save(config);
		
		ProductProfile profile = new ProductProfile();
		profile.setProductCode("productcode");
		profile.setProductDesc("productdesc");
		profile.setStatus(ProductProfileStatus.APPROVED);
		profile.setEffectiveMonths(new Long(1));
		profile = profileRepo.save(profile);
		
		CardVendor cardVendor = new CardVendor();
		cardVendor.setMailAddress("mail add");
		cardVendor.setDefaultVendor(Boolean.TRUE);
		cardVendor.setEmailAddress("defaultVendor@sap.sanmiguil.com");
		cardVendor.setEnabled(Boolean.TRUE);
		cardVendor.setFormalName("ACME Vendor - San Miguel");
		cardVendor.setRegion("Manila");
		cardVendorRepo.save(cardVendor);
		
		GiftCardOrder order = new GiftCardOrder();
		order.setMoNumber("moNumber");
		order.setMoDate(new LocalDate());
		order.setStatus(GiftCardOrderStatus.APPROVED);
		order.setVendor(cardVendor);
		order = orderRepo.save(order);
		
		
		for(int i = 100000000; i < 100000010; i++) {
			GiftCardInventory gc = new GiftCardInventory();
			gc.setBarcode(Integer.toString(i));
			gc.setSeries(new Long(i));
			gc.setSeqNo(i - 100000000);
			gc.setBatchNo(1);
			gc.setOrderDate(new LocalDate());
			gc.setStatus(GiftCardInventoryStatus.IN_STOCK);
			gc.setFaceValue(new BigDecimal(100000));
			gc.setProfile(profile);
			gc.setOrder(order);
			gc.setProductCode("productcode");
			gc.setProductName("productdesc");
			gc.setLocation("20001");
			inventoryRepo.save(gc);
		}
	}
	@After
	public void teardown() {
		repo.deleteAll();
		txnRepo.deleteAll();
		inventoryRepo.deleteAll();
		orderRepo.deleteAll();
		cardVendorRepo.deleteAll();
		profileRepo.deleteAll();
		configRepo.deleteAll();
	}
	
	
	
	@Test
	public void saveExhTransactionTest() {
		Long count = repo.count(); 
		B2BExhibitionDto exhDto = new B2BExhibitionDto();
		service.saveExhTransaction(exhDto);
		Assert.assertTrue(repo.count() == count + 1);
	}
	@Test
	public void getExhTransactionTest() {
		B2BExhibitionDto exhDto = service.getExhTransaction(id);
		Assert.assertTrue(exhDto.getTotalTaxableAmt().compareTo(new BigDecimal(180000)) == 0);
		Assert.assertTrue(exhDto.getDiscount().compareTo(BigDecimal.ZERO) == 0);
		Assert.assertTrue(exhDto.getTotalPayment().compareTo(new BigDecimal(200000)) == 0);
	}
	@Test
	public void listExhTransactionTest() {
		ResultList<B2BExhibitionDto> lst = service.listExhTransaction(new PageSortDto());
		Assert.assertTrue(lst.getNumberOfElements() == 1);
	}
	@Test
	public void activateGcsTest() {
		B2BExhibitionDto exhDto = new B2BExhibitionDto();
		B2BExhibitionGcDto gcDto = new B2BExhibitionGcDto();
		gcDto.setStartingSeries("100000000");
		gcDto.setEndingSeries("100000009");
		exhDto.getGcs().add(gcDto);
		exhDto.setCreated(new DateTime());
		service.activateGcs(exhDto);
		QGiftCardInventory qIn = QGiftCardInventory.giftCardInventory;
		Assert.assertTrue(inventoryRepo.count(qIn.status.eq(GiftCardInventoryStatus.ACTIVATED)) == 10);
	}
	
	@Test
	public void countProductsTest() {
		B2BExhibitionDto exhDto = new B2BExhibitionDto();
		B2BExhibitionGcDto gcDto = new B2BExhibitionGcDto();
		gcDto.setStartingSeries("100000000");
		gcDto.setEndingSeries("100000009");
		exhDto.getGcs().add(gcDto);
		exhDto.setCreated(new DateTime());
		List<B2BExhibitionCountDto> lst = service.countProducts(exhDto);
		Assert.assertEquals(lst.get(0).getCount(), new Long(10));
	}
	
	@Test
	public void getTotalFaceAmountTest() {
		B2BExhibitionDto exhDto = new B2BExhibitionDto();
		B2BExhibitionGcDto gcDto = new B2BExhibitionGcDto();
		gcDto.setStartingSeries("100000000");
		gcDto.setEndingSeries("100000009");
		exhDto.getGcs().add(gcDto);
		exhDto.setCreated(new DateTime());
		List<B2BExhibitionCountDto> lst = service.countProducts(exhDto);
		
		BigDecimal total = service.getTotalFaceAmount(lst);
		Assert.assertTrue(total.compareTo(new BigDecimal(1000000)) == 0);
	}
	
}



