package com.transretail.crm.common.util;


import java.math.BigDecimal;
import java.text.DecimalFormat;


public final class NumberToWords {

	private NumberToWords() {}
	public static enum Converter { BAHASA };



	public static String convert( BigDecimal number, String and, NumberToWordsRef ref ) {
		if ( null == number ) {
			return "";
		}

		if ( null == ref ) {
			ref = new NumberToWordsRef();
		}

		//0 to 999 999 999 999
		if ( number.compareTo( new BigDecimal( 0 ) ) == 0 ) {
			return ref.getZero();
		}

		StringBuilder strNumber = new StringBuilder( NumberToWords.convert( (long) Math.floor( number.doubleValue() ), ref ) );
		String centVal = null;
		if ( number.toString().contains( "." ) ) {
			centVal = NumberToWords.convert( Long.parseLong( number.toString().substring( number.toString().lastIndexOf( "." ) + 1 ) ), 
					new NumberToWordsRef() );
		}
		strNumber.append( null != centVal && !centVal.trim().isEmpty()? and + centVal : "" );
		return strNumber.toString();
	}



	private static String convert( long number, NumberToWordsRef ref ) {

		String strNum = Long.toString( number );

		String mask = "000000000000";
		DecimalFormat df = new DecimalFormat( mask );
		strNum = df.format( number );

		int billions = Integer.parseInt( strNum.substring( 0, 3 ) );// XXXnnnnnnnnn
		int millions = Integer.parseInt( strNum.substring( 3, 6 ) );// nnnXXXnnnnnn
		int hundredThousands = Integer.parseInt( strNum.substring( 6, 9 ) );// nnnnnnXXXnnn
		int thousands = Integer.parseInt( strNum.substring( 9, 12 ) );// nnnnnnnnnXXX

		String strBillions;
		switch ( billions ) {
			case 0:
				strBillions = "";
				break;
			case 1:
				strBillions = convertLessThanOneThousand( billions, ref ) + ref.getBillion();
				break;
			default:
				strBillions = convertLessThanOneThousand( billions, ref ) + ref.getBillion();
		}
		String result = strBillions;

		String strMillions;
		switch ( millions ) {
			case 0:
				strMillions = "";
				break;
			case 1:
				strMillions = convertLessThanOneThousand( millions, ref ) + ref.getMillion();
				break;
			default:
				strMillions = convertLessThanOneThousand( millions, ref ) + ref.getMillion();
		}
		result = result + strMillions;

		String strHundredThousands;
		switch ( hundredThousands ) {
			case 0:
				strHundredThousands = "";
				break;
			case 1:
				strHundredThousands = ref.getOneThou();
				break;
			default:
				strHundredThousands = convertLessThanOneThousand( hundredThousands, ref ) + ref.getThou();
		}
		result = result + strHundredThousands;

		String strThousand;
		strThousand = convertLessThanOneThousand( thousands, ref );
		result = result + strThousand;

		return result.replaceAll( "^\\s+", "" ).replaceAll( "\\b\\s{2,}\\b", " " );
	}

	private static String convertLessThanOneThousand( int number, NumberToWordsRef ref ) {
		final String[] tensNames = ref.getTensNames();
		final String[] numNames = ref.getNumsNames();

		String result;
		if ( number % 100 < 20 ) {
			result = numNames[ number % 100 ];
			number /= 100;
		} 
		else {
			result = numNames[ number % 10 ];
			number /= 10;

			result = tensNames[ number % 10 ] + result;
			number /= 10;
		}
		if ( number == 0 )
			return result;

		return numNames[ number ] + " hundred" + result;
	}



	public static class NumberToWordsRef {
		private static final String BILLION = " billion ";
		private static final String MILLION = " million ";
		private static final String THOU = " thousand ";
		private static final String ONETHOU = "one thousand ";
		private static final String HUNDRED = " hundred";
		private static final String ZERO = " zero";
		private final String[] TENS_NAMES = { "", " ten", " twenty", " thirty", " forty", " fifty", 
			" sixty", " seventy", " eighty", " ninety" };
		private final String[] NUM_NAMES = { "", " one", " two", " three", " four", " five",
		    " six", " seven", " eight", " nine",
		    " ten", " eleven", " twelve", " thirteen", " fourteen",
		    " fifteen", " sixteen", " seventeen", " eighteen", " nineteen" };

		private String billion;
		private String million;
		private String thou;
		private String oneThou;
		private String hundred;
		private String zero;
		private String[] tensNames;
		private String[] numsNames;

		public NumberToWordsRef() {}
		public NumberToWordsRef( 
				String billion, 
				String million, 
				String thou, 
				String oneThou, 
				String[] tensNames, 
				String[] numsNames ) {
			
		}

		public String getBillion() {
			return null != billion && !billion.trim().isEmpty()? billion : BILLION;
		}
		public String getMillion() {
			return null != million && !million.trim().isEmpty()? million : MILLION;
		}
		public String getThou() {
			return null != thou && !thou.trim().isEmpty()? thou : THOU;
		}
		public String getOneThou() {
			return null != oneThou && !oneThou.trim().isEmpty()? oneThou : ONETHOU;
		}
		public String getHundred() {
			return null != hundred && !hundred.trim().isEmpty()? hundred : HUNDRED;
		}
		public String getZero() {
			return null != zero && !zero.trim().isEmpty()? zero : ZERO;
		}
		public String[] getTensNames() {
			return null != tensNames? tensNames : TENS_NAMES;
		}
		public String[] getNumsNames() {
			return null != numsNames? numsNames : NUM_NAMES;
		}
	}

}
