/*
 * Copyright (c) 2012.
 * All rights reserved.
 */
package com.transretail.crm.core.security.service;

import com.transretail.crm.common.security.service.SecurityUserService;
import com.transretail.crm.core.security.model.CustomSecurityUserDetails;

/**
 * @author mhua
 */
public interface CustomSecurityUserService extends SecurityUserService<CustomSecurityUserDetails> {
}
