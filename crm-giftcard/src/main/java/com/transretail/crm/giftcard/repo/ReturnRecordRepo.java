package com.transretail.crm.giftcard.repo;

import org.springframework.stereotype.Repository;

import com.transretail.crm.common.repo.CrmQueryDslPredicateExecutor;
import com.transretail.crm.giftcard.entity.ReturnRecord;

/**
 *
 */
@Repository
public interface ReturnRecordRepo extends CrmQueryDslPredicateExecutor<ReturnRecord, Long> {
	public ReturnRecord findByRecordNo(String recordNo);
}
