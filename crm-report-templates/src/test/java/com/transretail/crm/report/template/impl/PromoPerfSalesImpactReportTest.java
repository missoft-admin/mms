package com.transretail.crm.report.template.impl;

import java.io.File;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.annotation.Resource;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import net.sf.jasperreports.engine.JRParameter;

import org.apache.commons.io.FileUtils;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.context.support.MessageSourceResourceBundle;
import org.springframework.security.authentication.TestingAuthenticationToken;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.context.SecurityContextImpl;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.mysema.query.jpa.impl.JPAQuery;
import com.transretail.crm.common.reporting.exception.ReportException;
import com.transretail.crm.common.reporting.jasper.JRProcessor;
import com.transretail.crm.core.entity.QPromotion;
import com.transretail.crm.core.entity.UserModel;
import com.transretail.crm.core.repo.UserRepo;
import com.transretail.crm.core.security.model.impl.CustomSecurityUserDetailsImpl;
import com.transretail.crm.core.service.CodePropertiesService;
import com.transretail.crm.report.template.ReportTemplate;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath*:/META-INF/spring/applicationContext*.xml",
    "/META-INF/spring/applicationContext-test.xml"})
public class PromoPerfSalesImpactReportTest extends AbstractPointSummaryReportTest {

	@Resource(name = "promoPerfSalesImpactReport")
    private ReportTemplate promoPerfSalesImpactReport;
	@PersistenceContext
    private EntityManager em;
	@Autowired
	private CodePropertiesService codePropertiesService;
	@Autowired
	private UserRepo userRepo;
	private static final Logger logger = LoggerFactory.getLogger(PromoPerfSalesImpactReportTest.class);
	
	@Override
	@Test 
	public void testCreateJrProcessor() throws ReportException {
		UserModel currentUser = new UserModel();
    	currentUser.setUsername("loggedInUser");
    	currentUser.setPassword("");
    	currentUser.setEnabled(true);
    	currentUser.setInventoryLocation(codePropertiesService.getDetailInvLocationDenpasar());
    	currentUser.setStoreCode( "store1" );
    	userRepo.save(currentUser);

        CustomSecurityUserDetailsImpl user = new CustomSecurityUserDetailsImpl();
        user.setUserId(currentUser.getId());
        user.setUsername(currentUser.getUsername());
        List<GrantedAuthority> authorities = Lists.newArrayList();
        authorities.add(new SimpleGrantedAuthority("ROLE_CSS"));
        user.setAuthorities(authorities);
        user.setInventoryLocation(codePropertiesService.getDetailInvLocationDenpasar());

        SecurityContextImpl securityContext = new SecurityContextImpl();
        securityContext.setAuthentication(new TestingAuthenticationToken(user, ""));
        SecurityContextHolder.setContext(securityContext);
        
		QPromotion qpromo = QPromotion.promotion;
		Map<String, String> params = Maps.newHashMap();
		params.put("promotionId", new JPAQuery(em).from(qpromo).singleResult(qpromo.id.min().stringValue()));
		
		JRProcessor jrProcessor = promoPerfSalesImpactReport.createJrProcessor(params);
		File testClassesDir = FileUtils.toFile( Thread.currentThread().getContextClassLoader().getResource( "" ) );
		File output = new File(testClassesDir, "test-giftcard-so-handover-document.pdf");
		logger.debug( "Output location {}", output.getAbsolutePath() );
		output.delete();
		Locale locale = LocaleContextHolder.getLocale();
		jrProcessor.addParameter(JRParameter.REPORT_LOCALE, locale);
		jrProcessor.addParameter(JRParameter.REPORT_RESOURCE_BUNDLE, new MessageSourceResourceBundle( messageSourceRef, locale ) );
		reportService.exportPdfReport( jrProcessor, output );
	
		double bytes = output.length();
		double kilobytes = ( bytes / 1024 );
		logger.debug( "Kilobytes: {}", kilobytes );
		
		
	}
}
