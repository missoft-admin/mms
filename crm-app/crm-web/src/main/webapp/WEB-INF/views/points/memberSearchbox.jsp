<%@ include file="../common/taglibs.jsp" %>

<style type="text/css">
<!--
  .well .input {
    background-color: #fff;
    border: 1px solid #E8E8E8;
    box-shadow: none;    
    padding: 6px;
    height: 18px;
    margin-left: 6px;
  }
  .well .btn-primary {
    margin-left: 10px;
  }
  
-->
</style>

    <c:set var="ENCTYPE" value="application/x-www-form-urlencoded"/>
    <c:if test="${multipart}"><c:set var="ENCTYPE" value="multipart/form-data"/></c:if>
    
    <c:set var="MODEL_ATTRIBUTE" value="${searchModelAttribute}"/>
    <c:url var="PATH" value="${form_url}?page=1&amp;size=10"/>
    <spring:message var="BUTTON_LABEL" code="button_search" text="Search"/>
    
    
    <form:form action="${PATH}" method="POST" id="${MODEL_ATTRIBUTE}" modelAttribute="${MODEL_ATTRIBUTE}" enctype="${ENCTYPE}">
        <div class="well">
          <div class="form-inline form-search">
          	<label class="label-single"><h5><spring:message code="entity_search" />:</h5></label>
              <select id="search_memberCriteria" style="margin: 6px;">
                  <c:forEach var="criteria" items="${memberCriteria}">
                      <option value="${criteria.value}">${criteria.description}</option>
                  </c:forEach>
              </select>
              <input id="search_memberField" class="input memberSearchField" placeholder="${BUTTON_LABEL}" />
              <input id="search_submit" type="submit" value="${BUTTON_LABEL}" class="btn btn-primary" />
              <input type="button" value="<spring:message code="label_clear" />" class="btn custom-reset" data-default-id="search_submit" />
          </div>   
          <div class="form-inline form-search">
              <label class="label-single"><h5><spring:message code="label_filter" />:</h5></label>
              <select class="pointsSearchDropdown memberSearchField" data-name="status" id="status" style="margin: 6px;">
                      <option value=""><spring:message code="label_com_transretail_crm_entity_customermodel_status" /></option>
                  <c:forEach var="item" items="${status}">
                      <option value="${item.value}">${item.desc}</option>
                  </c:forEach>
              </select>
              <select class="pointsSearchDropdown memberSearchField" data-name="memberType" id="memberType" style="margin: 6px;">
                      <option value=""><spring:message code="label_com_transretail_crm_entity_customermodel_membertype" /></option>
                  <c:forEach var="item" items="${memberType}">
                      <option value="${item.code}">${item.description}</option>
                  </c:forEach>
              </select>
              <select class="pointsSearchDropdown memberSearchField" data-name="registeredStore" id="registeredStore" style="margin: 6px;">
                      <option value=""><spring:message code="label_com_transretail_crm_entity_customermodel_registeredstore" /></option>
                  <c:forEach var="item" items="${preferredStore}">
                      <option value="${item.code}">${item.codeAndName}</option>
                  </c:forEach>
              </select>
          </div>
        </div>
    </form:form>
    
    
    <style type="text/css">
        
        #search_memberField {
            padding-bottom: 4px;
        }
    </style>

