<%@include file="../common/taglibs.jsp" %>


<div id="content_emailDraftView" class="modal hide  nofly modal-dialog">

  <div class="modal-content modal-form form-horizontal">
    <div class="modal-header">
      <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
      <h4><spring:message code="channel_lbl_view" /></h4>
    </div>


    <div class="modal-body">
      <div class="main-block">
        <c:if test="${not empty marketingChannel.code}">
        <div id="code" class="code control-group">
          <label for="code" class="control-label"><spring:message code="channel_code"/></label>
          <div  class="controls"><span>${marketingChannel.code}</span></div>
        </div>
        </c:if>

        <div class="description control-group">
          <label for="description" class="control-label"><spring:message code="channel_description"/></label>
          <div  class="controls"><span>${marketingChannel.description}</span></div>
        </div>

        <div class="targetRecipients control-group">
          <label for="" class="control-label"><spring:message code="channel_target"/></label>
          <div class="controls">${marketingChannel.targetCount}</div>
        </div>

        <c:if test="${not empty marketingChannel.memberGroup}">
        <div class="memberGroup control-group">
          <label for="memberGroup" class="control-label"><spring:message code="channel_membergroup"/></label>
          <div  class="controls"><span>${marketingChannel.memberGroupName}</span></div>
        </div>
        </c:if>

        <c:if test="${not empty marketingChannel.promotion}">
        <div class="promotion control-group">
          <label for="promotion" class="control-label"><spring:message code="channel_promotion"/></label>
          <div  class="controls"><span>${marketingChannel.promotionName}</span></div>
        </div>
        </c:if>

        <div class="schedule control-group">
          <label for="schedule" class="control-label"><spring:message code="channel_schedule"/></label>
          <div  class="controls"><span id="dispSchedule">${marketingChannel.dispSchedule}</span> <span>${marketingChannel.dispTime}</span></div>
        </div>

        <div class="file control-group">
          <label for="message" class="control-label"><spring:message code="channel_message"/></label>
          <div  class="controls"><span>${marketingChannel.message}</span></div>
        </div>

        <c:if test="${not empty marketingChannel.filename}">
        <div class="file control-group">
          <label for="file" class="control-label"><spring:message code="channel_file_htm"/></label>
          <div  class="controls"><span>${marketingChannel.filename}</span></div>
        </div>
        </c:if>
      </div>
    </div>


    <div class="modal-footer">
      <!--<input id="cancelBtn" class="btn" type="button" value="<spring:message code="label_cancel" />" data-dismiss="modal" />-->
      <button type="button" id="cancelBtn" data-dismiss="modal" class="btn btn-default"><spring:message code="label_cancel"/></button>
    </div>
  </div>


<style type="text/css">
  <!-- 
    .modal-footer { text-align: center; } 
    .bootstrap-timepicker-widget { z-index: 2000 !important; } 
    .controls span { width: 99%; word-wrap: break-word; }
  -->
</style>
<script type="text/javascript">
<!--
var MarketingChannelView = null;

$(document).ready( function() {
    var $dialog = $( "#content_emailDraftView" );

    initDialog();

    function initDialog() {
		    $dialog.modal({ show : false });
		    $dialog.on( "hide", function(e) { 
		      if ( e.target === this ) {}
		    });
        $dialog.on( "show", function(e) { 
          if ( e.target === this ) {
              $dialog.find( "#dispSchedule" ).html( new Date( $dialog.find( "#dispSchedule" ).html() - 0 ).customize(1) );
          }
        });
	  }

    MarketingChannelView = {
        show  : function() { 
            $dialog.modal( "show" );
        }
    };
});
-->
</script>


</div>