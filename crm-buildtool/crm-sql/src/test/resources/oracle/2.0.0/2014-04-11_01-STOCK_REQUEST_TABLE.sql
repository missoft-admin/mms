drop table CRM_STOCK_REQUEST if exists;

create table CRM_STOCK_REQUEST (
    id number(19,0) not null,
    CREATED_BY varchar2(255 char),
    CREATED_DATETIME timestamp,
    LAST_UPDATED_BY varchar2(255 char),
    LAST_UPDATED_DATETIME timestamp,
    PRODUCT_CODE varchar2(255 char),
    QUANTITY number(10,0),
    REQUEST_DATE date,
    REQUEST_NO varchar2(12 char),
    STATUS varchar2(255 char),
    ALLOCATE_TO number(10,0),
    SOURCE_LOCATION number(10,0),
    primary key (id)
);

alter table CRM_STOCK_REQUEST 
    add constraint FK_bdydqssie75jlt60e4qocap05 
    foreign key (ALLOCATE_TO) 
    references STORE;

alter table CRM_STOCK_REQUEST 
    add constraint FK_so4mj8g405igxlcsri2y6722n 
    foreign key (SOURCE_LOCATION) 
    references STORE;