package com.transretail.crm.report.template.impl;

import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.time.DateUtils;
import org.joda.time.LocalDate;
import org.joda.time.LocalDateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.MessageSourceAware;
import org.springframework.context.support.MessageSourceAccessor;
import org.springframework.stereotype.Service;

import com.transretail.crm.common.reporting.jasper.ClasspathFileResolver;
import com.transretail.crm.common.reporting.jasper.DefaultJRProcessor;
import com.transretail.crm.common.reporting.jasper.JRProcessor;
import com.transretail.crm.common.util.DateUtil;
import com.transretail.crm.core.dto.PopularStoreDto;
import com.transretail.crm.core.entity.enums.ReportType;
import com.transretail.crm.core.service.StoreService;
import com.transretail.crm.report.template.CommonReportFilter;
import com.transretail.crm.report.template.FilterField;
import com.transretail.crm.report.template.NoDataFoundSupport;
import com.transretail.crm.report.template.ReportTemplate;

@Service("popularStoresToVisitReport")
public final class PopularStoresToVisitReport implements ReportTemplate, MessageSourceAware, NoDataFoundSupport {

    private MessageSourceAccessor messageSource;
    @PersistenceContext
    private EntityManager em;
    @Autowired
    StoreService storeService;

    @Override
    public void setMessageSource(MessageSource messageSource) {
        this.messageSource = new MessageSourceAccessor(messageSource);
    }

    @Override
    public ReportType[] getReportTypes() {
        return ReportType.values();
    }

    @Override
    public String getName() {
        return messageSource.getMessage("report.popstores.title");
    }

    @Override
    public String getDescription() {
        return messageSource.getMessage("report.popstores.title");
    }

    @Override
    public Set<FilterField> getFilters() {
        Set<FilterField> filters = new LinkedHashSet<FilterField>(2);
        filters.add(FilterField.createDateField(CommonReportFilter.DATE_FROM,
                messageSource.getMessage("report.misc.datefrom"), DateUtil.SEARCH_DATE_FORMAT, true));
        filters.add(FilterField.createDateField(CommonReportFilter.DATE_TO,
                messageSource.getMessage("report.misc.dateto"), DateUtil.SEARCH_DATE_FORMAT, true));
        return filters;
    }

    @Override
    public boolean canView(Set<String> permissions) {
        /*if (permissions != null) {
         for (String permission : permissions) {
         if (permission.equalsIgnoreCase( REPORT_POINTS_ISSUANCE_PERMISSION ) ) {
         return true;
         }
         }
         }*/
        return true;
    }

    @Override
    public JRProcessor createJrProcessor(Map<String, String> map) {
        LocalDate dateFrom = LocalDate.parse(map.get(CommonReportFilter.DATE_FROM), DateTimeFormat.forPattern(DateUtil.SEARCH_DATE_FORMAT));
        LocalDate dateTo = LocalDate.parse(map.get(CommonReportFilter.DATE_TO), DateTimeFormat.forPattern(DateUtil.SEARCH_DATE_FORMAT));
        Date startDate = null != dateFrom ? dateFrom.toDateTimeAtStartOfDay().toDate() : null;
        Date endDate = null != dateTo ? dateTo.toDateMidnight().toDate() : null;;
        if (null == startDate && null != endDate) {
            startDate = DateUtils.addDays(endDate, -30);
        } else if (null != startDate && null == endDate) {
            endDate = DateUtils.addDays(startDate, 30);
        } else if (null == startDate && null == endDate) {
            startDate = DateUtils.truncate(new Date(), Calendar.MONTH);
            endDate = DateUtils.ceiling(new Date(), Calendar.MONTH);
        }
        startDate = DateUtils.truncate(startDate, Calendar.DATE);
        endDate = DateUtils.addMilliseconds(DateUtils.ceiling(endDate, Calendar.DATE), -1);
        dateFrom = LocalDate.fromDateFields(startDate);
        dateTo = LocalDate.fromDateFields(endDate);

        Map<String, Object> params = new HashMap<String, Object>();
        DateTimeFormatter df = DateTimeFormat.forPattern("MMMM d, yyyy");
        params.put("dateFrom", df.print(dateFrom));
        params.put("dateTo", df.print(dateTo));
        params.put("printDate", df.print(new LocalDateTime()));

        List<PopularStoreDto> popularStores = createDataSource(map);
        params.put("isEmpty", CollectionUtils.isEmpty(popularStores));
        params.put("popularStoresList", popularStores);

        DefaultJRProcessor jrProcessor = new DefaultJRProcessor("reports/popular_stores_report.jasper");
        jrProcessor.setParameters(params);
        jrProcessor.setFileResolver(new ClasspathFileResolver("reports"));

        return jrProcessor;
    }

    @Override

    public boolean isEmpty(Map<String, String> filter) {

        return createDataSource(filter).isEmpty();
    }

    private List<PopularStoreDto> createDataSource(Map<String, String> map) {
        LocalDate dateFrom = LocalDate.parse(map.get(CommonReportFilter.DATE_FROM), DateTimeFormat.forPattern(DateUtil.SEARCH_DATE_FORMAT));
        LocalDate dateTo = LocalDate.parse(map.get(CommonReportFilter.DATE_TO), DateTimeFormat.forPattern(DateUtil.SEARCH_DATE_FORMAT));
        Date startDate = null != dateFrom ? dateFrom.toDateTimeAtStartOfDay().toDate() : null;
        Date endDate = null != dateTo ? dateTo.toDateMidnight().toDate() : null;;
        if (null == startDate && null != endDate) {
            startDate = DateUtils.addDays(endDate, -30);
        } else if (null != startDate && null == endDate) {
            endDate = DateUtils.addDays(startDate, 30);
        } else if (null == startDate && null == endDate) {
            startDate = DateUtils.truncate(new Date(), Calendar.MONTH);
            endDate = DateUtils.ceiling(new Date(), Calendar.MONTH);
        }
        startDate = DateUtils.truncate(startDate, Calendar.DATE);
        endDate = DateUtils.addMilliseconds(DateUtils.ceiling(endDate, Calendar.DATE), -1);
        dateFrom = LocalDate.fromDateFields(startDate);
        dateTo = LocalDate.fromDateFields(endDate);

        List<PopularStoreDto> popularStores = storeService.getPopularStores(startDate, endDate);

        return popularStores;
    }

}
