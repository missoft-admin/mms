<%@include file="/WEB-INF/views/common/taglibs.jsp" %>

<div class="modal-dialog">
    <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            <spring:message code="gc.poslose" var="typeName" />
            <h4 class="modal-title"><spring:message code="global_menu_new" arguments="${typeName}"/></h4>
        </div>
        <div class="modal-body">
            <div id="contentError" class="hide alert alert-error">
                <button type="button" class="close" onclick="$(this).parent().hide();">&times;</button>
                <!-- <div><form:errors path="*"/></div>  -->
                <div id="containerError">
                </div>
            </div>
        
            <form:form id="poslosetxn" modelAttribute="poslosetxn" name="poslosetxn"
                action="${pageContext.request.contextPath}/poslose/" data-validate="validate">
                <form:hidden path="id"/>
                
                <div class="row-fluid">
                    <div class="span4">
                        <label class="control-label"><spring:message code="gc.poslose.label.date.time" /></label>
                        <form:hidden id="transactionDateTime" path="transactionDateTime" />
                        <div class="row-fluid">
                            <form:input path="dateString" id="dateInput" class="input-small input-space-01" readonly="true"/>
                            <form:input path="timeString" id="timeInput" class="input-small input-space-01" readonly="true"/>
                        </div>
                    </div>
                    <div class="span4">
                        <label><spring:message code="gc.poslose.label.store" /></label>
                        <form:input path="storeDesc" readonly="true" />
                    </div>
                    <div class="span4">
                        <label><spring:message code="gc.poslose.label.gift.card.no" /></label>
                        <form:input id="giftCardNo" path="giftCardNo" readonly="true" />
                    </div>
                </div>
                <div class="row-fluid">
                    <div class="span4">
                        <label><spring:message code="gc.poslose.label.transaction.no" /></label>
                        <form:input path="transactionNo" readonly="true" />
                    </div>
                    <div class="span4">
                        <label><spring:message code="gc.poslose.label.pos.no" /></label>
                        <form:input path="posNo" readonly="true" />
                    </div>
                    <div class="span4">
                        <label><spring:message code="gc.poslose.label.cashier.no" /></label>
                        <form:input path="cashierNo" readonly="true" />
                    </div>
                </div>
                <div class="row-fluid">
                    <div class="span4">
                        <label><spring:message code="gc.poslose.label.old.balance" /></label>
                        <form:input id="oldBalance" path="oldBalance" readonly="true"/>
                    </div>
                    <div class="span4">
                        <label><spring:message code="gc.poslose.label.transaction.amount" /></label>
                        <form:input id="transactionAmount" path="transactionAmount" readonly="true" />
                    </div>
                    <div class="span4">
                        <label><spring:message code="gc.poslose.label.new.balance" /></label>
                        <form:input id="newBalance" path="newBalance" readonly="true"/>
                    </div>
                </div>
            </form:form>
        </div>
        <div class="modal-footer">          
            <button type="button" id="cancelButton" class="btn btn-default" data-dismiss="modal"><spring:message code="label_cancel"/></button>
        </div>
    </div>
</div>