package com.transretail.crm.web.controller.util;

import java.util.List;
import java.util.regex.Pattern;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.FieldError;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

import com.transretail.crm.core.dto.MemberDto;
import com.transretail.crm.core.entity.enums.MemberCreatedFromType;
import com.transretail.crm.core.service.CodePropertiesService;
import com.transretail.crm.core.service.MemberService;
import com.transretail.crm.core.util.AppConstants;

@Component
public class MemberValidator implements Validator {
    @Autowired
    MemberService memberService;
    @Autowired
    MessageSource messageSource;

    @Autowired
    private CodePropertiesService codePropertiesService;

	private final static String ITS_DEFAULTMSG_INVALID   = "Info given is invalid.";
	private final static String ITS_PROPKEY_EMPTYMSG     = "propkey_msg_notempty";
	private final static String ITS_PROPKEY_DUPLICATEMSG = "propkey_msg_duplicate";
	private final static String ITS_PROPKEY_INVALID      = "propkey_msg_invalid_value";
	private final static String ITS_PROPKEY_USERNAME   	 = "username";
	private final static String ITS_PROPKEY_PASSWORD     = "password";
	private final static String ITS_PROPKEY_CONTACT      = "contact";
	private final static String ITS_PROPKEY_PIN 	     = "pin";
	private final static String ITS_PROPKEY_FIRSTNAME    = "firstName";
	private final static String ITS_PROPKEY_LASTNAME     = "lastName";
	private final static String ITS_PROPKEY_BIRTHDATE    = "birthdate";
	private final static String ITS_PROPKEY_RELIGION    = "customerProfile.religion";
	private final static String ITS_PROPKEY_COMPANYNAME  = "companyName";
	private final static String ITS_PROPKEY_NAME         = "name";
	private final static String ITS_PROPKEY_EMAIL        = "email";
	private final static String ITS_PROPKEY_ADDRESS_ST   = "address.street";
	private final static String ITS_PROPKEY_POST_CODE    = "address.postCode";
	private final static String ITS_PROPKEY_PROFESSIONAL_CUSTOMER_GROUP    = "professionalProfile.customerGroup";
	private final static String ITS_PROPKEY_PROFESSIONAL_POST_CODE    = "professionalProfile.businessAddress.postCode";
	private final static String ITS_PROPKEY_NPWP_ADDRESS = "npwpAddress";
	private final static String ITS_PROPKEY_NPWP_ID		 = "npwpId";
	private final static String ITS_PROPKEY_NPWP_NAME	 = "npwpName";
	private final static String ITS_PROPKEY_KTP_ID	 	 = "ktpId";
	private final static String ITS_PROPKEY_ID_NUMBER	 = "idNumber";
	private final static String ITS_PROPKEY_ZONE	 	 = "zone";
	private final static String ITS_PROPKEY_RADIUS	 	 = "radius";
	private final static String ITS_PROPKEY_BEST_TIME_TO_CALL	 = "besttimetocall";
	private final static String ITS_PROPKEY_PREARGUMENT  = "propkey_member_";
	private final static String ITS_PROPKEY_PIN_LENGTH	 = "propkey_pin_invalid_length";
	private final static String ITS_PROPKEY_PIN_FORMAT	 = "propkey_pin_invalid_format";
	
	public final static String PATTERN_NPWP_ADDRESS 	 = "^[0-9]{2}[.][0-9]{3}[.][0-9]{3}[.][0-9][-][0-9]{3}[.][0-9]{3}?$";
	public final static String PATTERN_CONTACT 			 = "^[\\d() \\-\\+]+$";
	

	public boolean supports(Class<?> clazz) {
		return MemberDto.class.isAssignableFrom(clazz);
	}

	public void validate( Object target, Errors inErrors ) {
		validateInvalidValues(inErrors);
		validateEmptyFields(inErrors);
		validateConditional( (MemberDto) target, inErrors );
		validateDuplicate( (MemberDto) target, inErrors );
	}
	
	private void validateInvalidValues(Errors inErrors) {
		for(FieldError error : inErrors.getFieldErrors()) {
    		String fieldName = error.getField().replace('.', '_');
    		if(StringUtils.isNotBlank((String) error.getRejectedValue()))
    			inErrors.reject( getMessage( ITS_PROPKEY_INVALID, new Object[] { (ITS_PROPKEY_PREARGUMENT + fieldName) } ) );
    	}
		
	}
	
	private void validateEmptyFields( Errors inErrors ) {
		ValidationUtils.rejectIfEmptyOrWhitespace(inErrors, ITS_PROPKEY_PIN, 
				getMessage( ITS_PROPKEY_EMPTYMSG, new Object[] { (ITS_PROPKEY_PREARGUMENT + ITS_PROPKEY_PIN) } ) );
		ValidationUtils.rejectIfEmptyOrWhitespace(inErrors, ITS_PROPKEY_FIRSTNAME, 
				getMessage( ITS_PROPKEY_EMPTYMSG, new Object[] { (ITS_PROPKEY_PREARGUMENT + ITS_PROPKEY_FIRSTNAME) } ) );
//		ValidationUtils.rejectIfEmptyOrWhitespace(inErrors, ITS_PROPKEY_LASTNAME, 
//				getMessage( ITS_PROPKEY_EMPTYMSG, new Object[] { (ITS_PROPKEY_PREARGUMENT + ITS_PROPKEY_LASTNAME) } ) );
		ValidationUtils.rejectIfEmptyOrWhitespace(inErrors, "customerProfile." + ITS_PROPKEY_BIRTHDATE, 
				getMessage( ITS_PROPKEY_EMPTYMSG, new Object[] { (ITS_PROPKEY_PREARGUMENT + ITS_PROPKEY_BIRTHDATE) } ) );
		ValidationUtils.rejectIfEmptyOrWhitespace(inErrors, "customerProfile." + "gender", 
				getMessage( ITS_PROPKEY_EMPTYMSG, new Object[] { (ITS_PROPKEY_PREARGUMENT + "gender") } ) );
//		ValidationUtils.rejectIfEmptyOrWhitespace(inErrors, ITS_PROPKEY_ADDRESS_ST, 
//				getMessage( ITS_PROPKEY_EMPTYMSG, new Object[] { (ITS_PROPKEY_PREARGUMENT + "address_street") } ) );
//		ValidationUtils.rejectIfEmptyOrWhitespace(inErrors, ITS_PROPKEY_MEMBERTYPE, 
//				getMessage( ITS_PROPKEY_EMPTYMSG, new Object[] { (ITS_PROPKEY_PREARGUMENT + ITS_PROPKEY_MEMBERTYPE) } ) );
	}
	
	private void validateConditional( MemberDto inModel, Errors inErrors ) {
		if(inModel.getMemberType() != null && inModel.getMemberType().getCode().equals(codePropertiesService.getDetailMemberTypeCompany())) {
			if(CollectionUtils.isEmpty(inModel.getBestTimeToCall())) {
				inErrors.reject( getMessage( ITS_PROPKEY_EMPTYMSG, new Object[] { (ITS_PROPKEY_PREARGUMENT + ITS_PROPKEY_BEST_TIME_TO_CALL) } ) );
			} else {
				for(int i = 0; i<inModel.getContact().size(); i++) {
					if(StringUtils.isNotBlank(inModel.getContact().get(i)) && StringUtils.isBlank(inModel.getBestTimeToCall().get(i))) {
						inErrors.reject( getMessage( ITS_PROPKEY_EMPTYMSG, new Object[] { (ITS_PROPKEY_PREARGUMENT + ITS_PROPKEY_BEST_TIME_TO_CALL) } ) );
					}
				}	
			}

            /*if(StringUtils.isBlank(inModel.getKtpId())) {
                inErrors.reject( getMessage( ITS_PROPKEY_EMPTYMSG, new Object[] { (ITS_PROPKEY_PREARGUMENT + ITS_PROPKEY_KTP_ID) } ) );
            }*/
            
            if(StringUtils.isBlank(inModel.getIdNumber())) {
                inErrors.reject( getMessage( ITS_PROPKEY_EMPTYMSG, new Object[] { (ITS_PROPKEY_PREARGUMENT + ITS_PROPKEY_ID_NUMBER) } ) );
            }

            if(inModel.getProfessionalProfile() != null) {
				if(StringUtils.isBlank(inModel.getProfessionalProfile().getRadius())) {
					inErrors.reject( getMessage( ITS_PROPKEY_EMPTYMSG, new Object[] { (ITS_PROPKEY_PREARGUMENT + ITS_PROPKEY_RADIUS) } ) );
				}
				
				if(StringUtils.isBlank(inModel.getProfessionalProfile().getZone())) {
					inErrors.reject( getMessage( ITS_PROPKEY_EMPTYMSG, new Object[] { (ITS_PROPKEY_PREARGUMENT + ITS_PROPKEY_ZONE) } ) );
				}
			}
		}

		if ( !StringUtils.isBlank(inModel.getEmail()) ) {
			if ( Pattern.matches( AppConstants.EMAIL_PATTERN, inModel.getEmail() ) ) {				
				ValidationUtils.rejectIfEmptyOrWhitespace(inErrors, ITS_PROPKEY_USERNAME,
						getMessage( ITS_PROPKEY_EMPTYMSG, new Object[] { (ITS_PROPKEY_PREARGUMENT + ITS_PROPKEY_USERNAME) } ) );
	            if (!codePropertiesService.getDetailMemberTypeEmployee().equals(inModel.getMemberType().getCode())) { // Task #84357
	                if (null != inModel.getMemberCreatedFromType() && !inModel.getMemberCreatedFromType()
	                    .equals(MemberCreatedFromType.FROM_UPLOAD)) {
	                    if (StringUtils.isBlank(inModel.getPassword())) {
	                        inErrors
	                            .reject(getMessage(ITS_PROPKEY_EMPTYMSG, new Object[]{(ITS_PROPKEY_PREARGUMENT + ITS_PROPKEY_PASSWORD)}));
	                    }
	                }
	            }
			}
			else {
				inErrors.reject( getMessage( "member_err_invalidemail", new Object[] {} ) );
			}
        }

		if (inModel.getMemberType() != null &&
				inModel.getMemberType().getCode().equals(codePropertiesService.getDetailMemberTypeCompany())) {
			ValidationUtils.rejectIfEmptyOrWhitespace(inErrors, ITS_PROPKEY_COMPANYNAME, 
					getMessage( ITS_PROPKEY_EMPTYMSG, new Object[] { (ITS_PROPKEY_PREARGUMENT + ITS_PROPKEY_COMPANYNAME) } ) );
		}
		
		if ((StringUtils.isNotBlank(inModel.getNpwpId()) && (inModel.getNpwpId().length()!=15 
				|| !StringUtils.isNumeric(inModel.getNpwpId())))) {
			inErrors.reject( getMessage( ITS_PROPKEY_INVALID, new Object[] { (ITS_PROPKEY_PREARGUMENT + ITS_PROPKEY_NPWP_ID) } ) );
		}

		boolean isProfessional = inModel.getMemberType() != null &&
				inModel.getMemberType().getCode().equals(codePropertiesService.getDetailMemberTypeCompany());
		ValidationUtils.rejectIfEmptyOrWhitespace(inErrors, isProfessional? "professionalProfile.businessAddress.street" : ITS_PROPKEY_ADDRESS_ST, 
				getMessage( ITS_PROPKEY_EMPTYMSG, new Object[] { (
						ITS_PROPKEY_PREARGUMENT + ( isProfessional? "professionalprofile_businessaddress_street" : "address_street") ) } ) );
		
		if (inModel.getMemberType() != null &&
                inModel.getMemberType().getCode().equals(codePropertiesService.getDetailMemberTypeProfessional())) {
    		ValidationUtils.rejectIfEmptyOrWhitespace(inErrors, ITS_PROPKEY_POST_CODE, 
                    getMessage( ITS_PROPKEY_EMPTYMSG, new Object[] {("label_address_postalcode")}));
    		
    		ValidationUtils.rejectIfEmptyOrWhitespace(inErrors, ITS_PROPKEY_RELIGION, 
                    getMessage( ITS_PROPKEY_EMPTYMSG, new Object[] {("label_com_transretail_crm_entity_customermodel_religion")}));
    		
    		ValidationUtils.rejectIfEmptyOrWhitespace(inErrors, ITS_PROPKEY_PROFESSIONAL_CUSTOMER_GROUP, 
                    getMessage( ITS_PROPKEY_EMPTYMSG, new Object[] {("label_com_transretail_crm_entity_customermodel_businesstype")}));
    		
    		ValidationUtils.rejectIfEmptyOrWhitespace(inErrors, ITS_PROPKEY_PROFESSIONAL_POST_CODE, 
                    getMessage( ITS_PROPKEY_EMPTYMSG, new Object[] {("label_address_prof_postalcode")}));
    		
		}

		String pin = inModel.getPin();
		if(!StringUtils.isBlank(pin)) {
			if(pin.length() != 6) {
				inErrors.reject( getMessage( ITS_PROPKEY_PIN_LENGTH, 
						new Object[] { (ITS_PROPKEY_PREARGUMENT + ITS_PROPKEY_PIN) } ) );
			}
			
			if(!pin.matches("\\d+")) {
				inErrors.reject( getMessage( ITS_PROPKEY_PIN_FORMAT, 
						new Object[] { (ITS_PROPKEY_PREARGUMENT + ITS_PROPKEY_PIN) } ) );
			}
		}
		
		List<String> contacts = inModel.getContact();
		boolean isContactEmpty = true;
		if(contacts != null && contacts.size() > 0) {
			for(String contact : contacts) {
				if( StringUtils.isNotBlank( contact ) ) {
					if ( !contact.matches(PATTERN_CONTACT) ) {
						inErrors.reject( getMessage( ITS_PROPKEY_INVALID, new Object[] { (ITS_PROPKEY_PREARGUMENT + ITS_PROPKEY_CONTACT) } ) );
					}
					isContactEmpty = false;
				}
			}
		}
		if ( isContactEmpty ) {
			inErrors.reject( getMessage( ITS_PROPKEY_EMPTYMSG, new Object[] { (ITS_PROPKEY_PREARGUMENT + ITS_PROPKEY_CONTACT) } ) );
		}
		if( StringUtils.isNotBlank( inModel.getHomePhone() ) ) {
			if ( !inModel.getHomePhone().matches(PATTERN_CONTACT) ) {
				inErrors.reject( getMessage( ITS_PROPKEY_INVALID, new Object[] { "member_prop_homephone" } ) );
			}
		}
		
		if(inModel.getMemberType() != null && inModel.getMemberType().getCode().equals(codePropertiesService.getDetailMemberTypeEmployee())) {
			if(StringUtils.isBlank(inModel.getAccountId()) || inModel.getAccountId().length() < 11 || inModel.getAccountId().length() > 12 ) {
				inErrors.reject(messageSource.getMessage("propkey_msg_emp_account_id", null, LocaleContextHolder.getLocale()));
			}
		}
	}
	
	private void validateDuplicate( MemberDto inModel, Errors inErrors ) {
		List<String> contacts = inModel.getContact();
		if(contacts != null && contacts.size() > 0
				&& memberService.findDuplicateContact(inModel) != null) {
			inErrors.reject( getMessage( ITS_PROPKEY_DUPLICATEMSG, new Object[] { (ITS_PROPKEY_PREARGUMENT + ITS_PROPKEY_CONTACT) } ) );
		}
		
		if (!StringUtils.isBlank(inModel.getUsername())
				&& memberService.findDuplicateUsername(inModel) != null) {
			inErrors.reject( getMessage( ITS_PROPKEY_DUPLICATEMSG, new Object[] { (ITS_PROPKEY_PREARGUMENT + ITS_PROPKEY_USERNAME) } ) );
		}
		
		if (StringUtils.isNotBlank(inModel.getKtpId())
				&& memberService.findDuplicateKtpId(inModel) != null) {
			inErrors.reject( getMessage( ITS_PROPKEY_DUPLICATEMSG, new Object[] { (ITS_PROPKEY_PREARGUMENT + ITS_PROPKEY_KTP_ID) } ) );
		}
		
		if (!StringUtils.isBlank(inModel.getCompanyName())
				&& memberService.findDuplicateCompany(inModel) != null) {
			inErrors.reject( getMessage( ITS_PROPKEY_DUPLICATEMSG, new Object[] { (ITS_PROPKEY_PREARGUMENT + ITS_PROPKEY_COMPANYNAME) } ) );
		}

        /*
        Bug #89174
		if (!StringUtils.isBlank(inModel.getFirstName())
				&& !StringUtils.isBlank(inModel.getLastName())
				&& memberService.findDuplicateName(inModel) != null) {
			inErrors.reject( getMessage( ITS_PROPKEY_DUPLICATEMSG, new Object[] { (ITS_PROPKEY_PREARGUMENT + ITS_PROPKEY_NAME) } ) );
		}
		*/
		if (!StringUtils.isBlank(inModel.getEmail())
				&& memberService.findDuplicateEmail(inModel) != null) {
			inErrors.reject( getMessage( ITS_PROPKEY_DUPLICATEMSG, new Object[] { (ITS_PROPKEY_PREARGUMENT + ITS_PROPKEY_EMAIL) } ) );
		}
		
		if (!StringUtils.isBlank(inModel.getNpwpId()) 
				&& !StringUtils.containsOnly( inModel.getNpwpId(), "0" )
				&& memberService.findDuplicateNpwpId(inModel) != null) {
			inErrors.reject( getMessage( ITS_PROPKEY_DUPLICATEMSG, new Object[] {(ITS_PROPKEY_PREARGUMENT + ITS_PROPKEY_NPWP_ID)} ));
		}
	}
	
	private String getMessage(String inMsgCode, Object[] inArgMsgCodes) {
		for ( int i=0; i < inArgMsgCodes.length; i++ ) {
			inArgMsgCodes[i] = messageSource.getMessage( inArgMsgCodes[i].toString().toLowerCase(), null, LocaleContextHolder.getLocale() );
		}
		return messageSource.getMessage(inMsgCode, inArgMsgCodes, ITS_DEFAULTMSG_INVALID, LocaleContextHolder.getLocale());
	}
	
	private String formatNpwpId(String npwpId) {
    	if(!npwpId.matches(MemberValidator.PATTERN_NPWP_ADDRESS))
    		return npwpId.substring(0, 1) + '.' + npwpId.substring(1, 4) + '.' + npwpId.substring(4, 7) + '.' + npwpId.substring(7, 8) + '-' + npwpId.substring(8, 11);
    	else
    		return npwpId;
    }
}
