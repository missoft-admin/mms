package com.transretail.crm.core.entity;

import com.transretail.crm.core.entity.lookup.LookupDetail;
import com.transretail.crm.core.entity.lookup.LookupHeader;
import com.transretail.crm.core.entity.support.CustomAuditableEntity;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

import java.io.Serializable;
import java.util.List;

@Table(name = "CRM_MEMBER_GRP_FIELD")
@Entity
public class MemberGroupField extends CustomAuditableEntity<Long> implements Serializable {
	
	private static final long serialVersionUID = 1L;

	@NotNull
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "MEMBER_GRP", nullable = false)
    private MemberGroup memberGroup;

    @JoinColumn(name="FIELD")
    @ManyToOne
    private LookupHeader name;
    
    @JoinColumn(name="TYPE")
    @ManyToOne
    private LookupDetail type;
    
    public MemberGroup getMemberGroup() {
        return memberGroup;
    }

    public void setMemberGroup(MemberGroup memberGroup) {
        this.memberGroup = memberGroup;
    }

	public LookupHeader getName() {
		return name;
	}

	public void setName(LookupHeader name) {
		this.name = name;
	}

	public LookupDetail getType() {
		return type;
	}

	public void setType(LookupDetail type) {
		this.type = type;
	}
}
