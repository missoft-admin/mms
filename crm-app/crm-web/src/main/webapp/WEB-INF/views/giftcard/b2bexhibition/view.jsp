<%@ include file="../../common/taglibs.jsp" %>
<%@include file="../../common/messages.jsp" %>

<style type="text/css">
<!--

  #productTable tr td:last-child, #paymentTable tr td:last-child {
    border-left: 0px;
  }
  
  a.showTransaction {
    color: #0080FF !important;
  }
  
-->
</style>

<div class="page-header page-header2">
    <h1><spring:message code="b2b.exhibition" /> - <sec:authentication property="principal.b2bLocation"/></h1>
</div>

<div class="row-fluid">



<div class="span9" id="exhibitionFormContainer">
<form name="exhibitionForm" action="<c:url value="/giftcard/b2b/exhibition/save" />">

      <div id="contentError" class="hide alert alert-error">
        <button type="button" class="close" onclick="$(this).parent().hide();">&times;</button>
        <div><form:errors path="*"/></div>
      </div>

<!-- PRODUCTS -->
<button type="button" id="scanProductBtn" class="btn btn-primary"><spring:message code="b2b.exhibition.scangc" /></button>
<br/><br/>
<div>
  <table id="productTable" class="table table-condensed table-compressed table-striped">
    <thead>
      <tr>
        <th><spring:message code="b2b.exhibition.product" /></th>
        <th><spring:message code="b2b.exhibition.cards" /></th>
        <th><spring:message code="b2b.exhibition.facevalue" /></th>
        <th><spring:message code="b2b.exhibition.quantity" /></th>
        <th><spring:message code="b2b.exhibition.beforediscount" /></th>
        <th><spring:message code="b2b.exhibition.discount" /></th>
        <th><spring:message code="b2b.exhibition.total" /></th>
        <th></th>
      </tr>
    </thead>
    <tbody>
      <tr>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
      </tr>
    </tbody>
  </table>
</div>


<!-- PAYMENTS -->
<br/><br/>
<button type="button" id="addPaymentsBtn" class="btn btn-primary"><spring:message code="b2b.exhibition.addpayments" /></button>
<br/><br/>
<div class="row-fluid">
<div class="span8">
  <table id="paymentTable" class="table table-condensed table-compressed table-striped">
    <thead>
      <tr>
        <th><spring:message code="b2b.exhibition.type" /></th>
        <th><spring:message code="b2b.exhibition.amount" /></th>
        <th></th>
      </tr>
    </thead>
    <tbody>
      <tr>
        <td></td>
        <td></td>
        <td></td>
      </tr>
    </tbody>
  </table>
</div>

</div>

<div class="form-actions container-btn">
            <button type="button" id="submitForm" class="btn btn-primary"><spring:message code="label_submit" /></button>
            <button type="button" id="cancelForm" class="btn"><spring:message code="label_cancel" /></button>
</div>

<div class="hide">
  <div id="gcsForm">
  </div>
  <div id="pymntsForm">
  </div>
</div>
</form>
</div>

<div class="span3 well">
<jsp:include page="transactionlist.jsp" />
</div>


</div>




<jsp:include page="productform.jsp" />
<jsp:include page="paymentform.jsp" />

  <script src="<spring:url value="/js/jquery/jquery.numberInput.js" />" ></script>
  <script src="<c:url value="/js/bootstrap/bootstrap-datepicker.js"/>" type='text/javascript'><![CDATA[&nbsp; ]]></script>
  <link href="<c:url value="/css/bootstrap/datepicker.css" />" rel="stylesheet" />
  <link href="<c:url value="/styles/temp.css" />" rel="stylesheet" />
  <script src="<spring:url value="/js/jquery/jquery.bindWithDelay.js" />"></script>
  <script src="<spring:url value="/js/jquery/jquery.ajaxQueue.js" />"></script>


<script type="text/javascript">

var $exhibitionForm = $("form[name='exhibitionForm']");

$(document).ready(function() {
	var CONTAINER_ERROR = "#contentError > div";
	var CONTENT_ERROR = "#contentError";
	var $submitForm = $("#submitForm");
		
	
	
	$submitForm.click(function() {
		$submitForm.prop( "disabled", true );
	    $.post($exhibitionForm.attr("action"), $exhibitionForm.serialize(), function(data) {
	      $submitForm.prop( "disabled", false );
	      if (data.success) {
	    	  window.open("<c:url value="/giftcard/b2b/exhibition/printreceipt/" />" + data.result, '_blank', 'toolbar=0,location=0,menubar=0');
	    	  window.open("<c:url value="/giftcard/b2b/exhibition/printkwitansi/" />" + data.result, '_blank', 'toolbar=0,location=0,menubar=0');
	    	  location.reload();
	      } else {
	        errorInfo = "";
	        for (i = 0; i < data.result.length; i++) {
	            errorInfo += "<br>" + (i + 1) + ". "
	                + ( data.result[i].code != undefined ? 
	                    data.result[i].code : data.result[i]);
	          }
	        $exhibitionForm.find(CONTAINER_ERROR).html("Please correct following errors: " + errorInfo);
	        $exhibitionForm.find(CONTENT_ERROR).show('slow');
	      }
	    });
	});
	
	$("#cancelForm").click(function() {
		location.reload();
	});
	
	
	/* PRODUCTS */
	
	var $scanProductBtn = $("#scanProductBtn");
	var $addProductBtn = $("#addProductBtn");
	var $clearProductBtn = $("#clearProductBtn");
	var $productForm = $("form[name='productForm']");
	
	var $productDialog = $("#productDialog").modal({
		show: false
	}).on('hidden.bs.modal', function() {
		refreshProductsTable();
	});
	
	
	$scanProductBtn.click(function() {
		$productDialog.modal("show");
	});
	$addProductBtn.click(function() {
		$addProductBtn.prop( "disabled", true );
	    $.post($productForm.attr("action"), $productForm.serialize(), function(data) {
	      $addProductBtn.prop( "disabled", false );
	      if (data.success) {
	    	  var startingSeries = $("#startingSeries", $productForm).val();
	    	  var endingSeries = $("#endingSeries", $productForm).val();
	    	  addGcsToForm(startingSeries, endingSeries, data.result);
	    	  addGcsToList(startingSeries, endingSeries, data.result);
	    	  $clearProductBtn.click();
	      } else {
	        errorInfo = "";
	        for (i = 0; i < data.result.length; i++) {
	          errorInfo += "<br>" + (i + 1) + ". " + data.result[i];
	        }
	        $productDialog.find(CONTAINER_ERROR).html("Please correct following errors: " + errorInfo);
	        $productDialog.find(CONTENT_ERROR).show('slow');
	      }
	    });
		
	});
	
	$clearProductBtn.click(function() {
		$productForm.trigger("reset");
		$productDialog.find(CONTAINER_ERROR).html("");
        $productDialog.find(CONTENT_ERROR).hide();
	});
	
	function initFields() {
		$('.intInput').numberInput({"type" : "int"});
		$('.floatInput').numberInput({"type" : "float"});
	}
	
	function addGcsToForm(startingSeries, endingSeries, productId) {
		var $form = $("#gcsForm");
		var index = $("input", $form).length / 3; 
		var html = "<input type=\"hidden\" class=\"gc"+productId+"\" name=\"gcs["+index+"].product\" value=\""+productId+"\" />" + 
			"<input type=\"hidden\" class=\"gc"+productId+"\" name=\"gcs["+index+"].startingSeries\" value=\""+startingSeries+"\" />" +
		"<input type=\"hidden\" class=\"gc"+productId+"\" name=\"gcs["+index+"].endingSeries\" value=\""+endingSeries+"\" />";
		$form.append(html);
	}
	
	function addGcsToList(startingSeries, endingSeries, productId) {
		var $div = $("#gcListContainer");
		var html = "<span class=\"gcl"+productId+"\">";
		if($div.data("isEmpty")) {
			$div.empty();
			$div.data("isEmpty", false);
		} 
		html += startingSeries;
		if(endingSeries != "") 
			html += " - " + endingSeries;
		html += "<br/></span>";
		$div.append(html);
	}
	
	
	
	
	/* PAYMENT */
	
	var $addPaymentsBtn = $("#addPaymentsBtn");
	var $addPaymentBtn = $("#addPaymentBtn");
	var $clearPaymentBtn = $("#clearPaymentBtn");
	var $paymentForm = $("form[name='paymentForm']");
	
	var $paymentDialog = $("#paymentDialog").modal({
		show: false
	}).on('hidden.bs.modal', function() {
		refreshPaymentsTable();
	}).css({
		"width": "600px",
	});
	
	$addPaymentsBtn.click(function() {
		$paymentDialog.modal("show");
	});
	
	$addPaymentBtn.click(function() {
		$addPaymentBtn.prop( "disabled", true );
	    $.post($paymentForm.attr("action"), $paymentForm.serialize(), function(data) {
	      $addPaymentBtn.prop( "disabled", false );
	      if (data.success) {
	    	  var type = $("#type", $paymentForm).val();
	    	  var amount = $("#amount", $paymentForm).val();
	    	  var details = $("#details", $paymentForm).val();
	    	  addPaymentToForm(type, amount, details);
	    	  $clearPaymentBtn.click();
	      } else {
	        errorInfo = "";
	        for (i = 0; i < data.result.length; i++) {
	          errorInfo += "<br>" + (i + 1) + ". " + data.result[i];
	        }
	        $paymentDialog.find(CONTAINER_ERROR).html("Please correct following errors: " + errorInfo);
	        $paymentDialog.find(CONTENT_ERROR).show('slow');
	      }
	    });
		
	});
	
	$clearPaymentBtn.click(function() {
		$paymentForm.trigger("reset");
		$paymentDialog.find(CONTAINER_ERROR).html("");
        $paymentDialog.find(CONTENT_ERROR).hide();
	});
	
	function addPaymentToForm(type, amount, details) {
		var $form = $("#pymntsForm");
		var index = $("input", $form).length / 3;
		var cls = type + details + amount;
		var html = "<input type=\"hidden\" class=\"payment"+cls+"\" name=\"payments["+index+"].paymentType\" value=\""+type+"\" />" +
		"<input type=\"hidden\" class=\"payment"+cls+"\" name=\"payments["+index+"].total\" value=\""+amount+"\" />" +
		"<input type=\"hidden\" class=\"payment"+cls+"\" name=\"payments["+index+"].details\" value=\""+details+"\" />";
		$form.append(html);
	}
	
	
	
	
	initFields();
});

function refreshProductsTable() {
	$("tbody", $("#productTable")).empty();
	if($("#gcsForm").find("input").length > 0) {
		$.post("<c:url value="/giftcard/b2b/exhibition/refreshproductstable" />", $exhibitionForm.serialize(), function(data) {
			$("tbody", $("#productTable")).html(data);
			refreshPaymentsTable();
		}, "html");	
	}
	
}

function refreshPaymentsTable() {
	$("tbody", $("#paymentTable")).empty();
	if($("#pymntsForm").find("input").length > 0) {
		$.post("<c:url value="/giftcard/b2b/exhibition/refreshpaymentstable" />", $exhibitionForm.serialize(), function(data) {
			$("tbody", $("#paymentTable")).html(data);
		}, "html");	
	}
}

</script>