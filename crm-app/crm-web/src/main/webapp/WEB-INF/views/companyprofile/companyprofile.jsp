<%@ include file="../common/taglibs.jsp"%>

  <div class="page-header page-header2">
      <h1><spring:message code="menutab_programsetup_maintenance_company" /></h1>
  </div>

<div class="">

<p class="pull-right">
<button type="button" onclick="location.href='${pageContext.request.contextPath}/companyprofile?form';" class="btn btn-primary"><spring:message code="label_update_company" /></button>
</p>

<table class="table table-condensed pull-left">
	<tbody>
		<tr>
			<td class="span2"><label><spring:message code="label_company_name" /></label></td>
			<td>${companyProfile.name}</td>
		</tr>
		<tr>
			<td class="span2"><label><spring:message code="label_company_copyrighttext" /></label></td>
			<td>${companyProfile.copyrightText}</td>
		</tr>
		<tr>
			<td class="span2"><label><spring:message code="label_company_logo" /></label></td>
			<td><img src="${pageContext.request.contextPath}/companyprofile/logo" /></td>
		</tr>
	</tbody>
</table>

	
</div>

<script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/viewspecific/groups/storegroup.js"><![CDATA[&nbsp;]]></script>
