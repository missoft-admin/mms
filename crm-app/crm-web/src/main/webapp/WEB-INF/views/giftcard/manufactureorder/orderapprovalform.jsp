<%@ include file="../../common/taglibs.jsp" %>

<style type="text/css">
<!--

  #productTable tr td:last-child {
    border-left: 0px;
  }

  #addProductBtn.btn-small {
    margin-bottom: 5px;
  }
  table select, textarea, table input[type="text"], input[type="password"], input[type="datetime"], input[type="datetime-local"], input[type="date"], input[type="month"], input[type="time"], input[type="week"], input[type="number"], input[type="email"], input[type="url"], input[type="search"], input[type="tel"], input[type="color"], .uneditable-input {
      margin-bottom: 0px;
      margin-top: 0px;
  }
  
-->
</style>



<c:set var="ENCTYPE" value="application/x-www-form-urlencoded"/>

<div class="modal-dialog">
  <div class="modal-content">
    <spring:message code="label_manufacture_order" var="typeName" />
    
    <div class="modal-header">
      <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
      <h4><spring:message code="gc_order"/></h4>
    </div>
 
    <div class="modal-body">
    
      <div id="contentError" class="hide alert alert-error">
        <button type="button" class="close" onclick="$(this).parent().hide();">&times;</button>
        <div><form:errors path="*"/></div>
      </div>
    
      
      <form:form id="orderForm" name="orderForm" modelAttribute="orderForm" action="${pageContext.request.contextPath}/giftcard/order/approve/" method="POST" enctype="${ENCTYPE}" cssClass="form-reset form-horizontal" >
      <form:hidden path="id"/>
      
      <div class="contentMain">
      
      <div class="row-fluid">
      
        <div class="span6">
        <div class="control-group">
          <spring:message code="order_mo_number" var="moNo" />
          <label for="moNumber" class="control-label">${moNo}</label> 
          <div class="controls">
            <form:input path="moNumber" disabled="true" class="form-control" placeholder="${moNo}" />
          </div>
        </div>
        
         <div class="control-group">
          <spring:message code="order_mo_date" var="moDt" />
          <label for="moDate" class="control-label">${moDt}</label> 
          <div class="controls">
            <form:input path="moDate" disabled="true" class="form-control" placeholder="${moDt}" />
          </div>
        </div>
        
        
        <div class="control-group">
          <spring:message code="order_po_number" var="poNo" />
          <label for="poNumber" class="control-label">${poNo}</label> 
          <div class="controls">
            <form:input path="poNumber" disabled="true" class="form-control" placeholder="${poNo}" />
          </div>
        </div>
        
        <div class="control-group">
          <spring:message code="order_po_date" var="poDt" />
          <label for="poDate" class="control-label">${poDt}</label> 
          <div class="controls">
            <form:input path="poDate" disabled="true" class="form-control" placeholder="${poDt}" />
          </div>
        </div>

        <c:if test="${!orderForm.isEgc}">
        <div class="control-group">
          <spring:message code="order_supplier" var="supp" />
          <label for="vendorId" class="control-label">${supp}</label> 
          <div class="controls">
            <form:select path="vendorId" disabled="true" items="${vendors.results}" itemLabel="name" itemValue="id" />
          </div>
        </div>
        </c:if>

        <c:if test="${orderForm.isEgc}">
        <div class="control-group">
          <spring:message var="lbl_isEgc" code="order_isegc" />
          <label for="poDate" class="control-label">${lbl_isEgc}</label> 
          <div class="controls">
            <input type="checkbox" ${orderForm.isEgc? 'checked' : ''} disabled="disabled" class="form-control checkbox" />
            <form:hidden path="isEgc" />
          </div>
        </div>
        </c:if>
        
        
       
        
        
        
        </div>
        
        <div class="span6">
        <div style="width:400px;">
          <table id="productTable" class="table table-condensed table-compressed table-striped">
            <thead>
              <tr>
                <th><spring:message code="order_item_card_type" /></th>
                <th><spring:message code="order_item_quantity" /></th>
                <th>Expired Date</th>
              </tr>
            </thead>
            <tbody>
                <c:forEach var="item" items="${orderForm.items}" varStatus="status" >
                  <tr data-index="${status.index}">
                    <td>
                      <input type="text" class="input-medium" disabled="disabled" name="items[${status.index}].profileId" value="${item.productName}"/>
                    </td>
                    <td><input type="text" class="input-mini" disabled="disabled" value="${item.quantity}" name="items[${status.index}].quantity" /></td>
                    <td><input type="text" class="input-medium" disabled="disabled" value="${item.expiredDate}" name="items[${status.index}].expiredDate" /></td>
                  </tr>
                </c:forEach>
            </tbody>
          </table>
        </div>
        
        
        <div class="clearFix"></div>
        
        </div>
        
        
      </div>
      
        
          
      </div>
      
      </form:form>
    
    </div>
    <div class="modal-footer" id="moOrderCtrlBtns">
      <c:if test="${orderForm.status == 'FOR_APPROVAL'}" >
      <button type="button" data-status="APPROVED" class="orderSubmit btn btn-primary"><spring:message code="label_approve" /></button>
      <button type="button" data-status="DRAFT" class="orderSubmit btn btn-primary"><spring:message code="label_reject" /></button>
      </c:if>
      <c:if test="${orderForm.status == 'APPROVED'}" >
      <button type="button" class="generateInventories btn btn-primary"><spring:message code="generate_inventories" /></button>
      </c:if>
      <button type="button" id="orderCancel" class="btn btn-default" data-dismiss="modal"><spring:message code="label_cancel"/></button>
    </div>
  </div>
</div>

<script>
$(document).ready(function() {
	  
	  var CONTAINER_ERROR = "#contentError > div";
	  var CONTENT_ERROR = "#contentError";
	  $form = $("#orderForm");
	  $orderDialog = $("#orderDialog").css({"width": "900px"});
	  
	  $(".orderSubmit").click(submitForm);
	  
	  $(".generateInventories").click(function() {
		  	$(".generateInventories").prop( "disabled", true );
    	    $.post("<c:url value="/giftcard/order/validategeneration/" />", function(data) {
    	      $(".generateInventories").prop( "disabled", false );
    	      if (!data.success) {
    	          getConfirm('<spring:message code="generate_inventories_msg" />', function(result) { 
    	  			if(result) {
    	  				$.post("<c:url value="/giftcard/order/generate/" />" + $("input[name='id']").val(), function(data) {
    	  					location.reload();
    	  				});
    	  			}
    	  		  });
    	      } 
    	      else {
    	        $orderDialog.find(CONTAINER_ERROR).html("<spring:message code="generate_inventories_err_msg" />");
    	        $orderDialog.find(CONTENT_ERROR).show('slow');
    	      }
    	    }); 
	  });
	  
	  
	  
	  function submitForm() {
	    $(".orderSubmit").prop( "disabled", true );
	    $.post($form.attr("action") + $(this).data("status"), $form.serialize(), function(data) {
	      if (data.success) {
	        location.reload();
	      } 
	      else {
	        errorInfo = "";
	        for (i = 0; i < data.result.length; i++) {
	          errorInfo += "<br>" + (i + 1) + ". "
	              + ( data.result[i].code != undefined ? 
	                  data.result[i].code : data.result[i]);
	        }
	        $orderDialog.find(CONTAINER_ERROR).html("Please correct following errors: " + errorInfo);
	        $orderDialog.find(CONTENT_ERROR).show('slow');
	        $(".orderSubmit").prop( "disabled", false );
	      }
	    }); 
	  }
	  
});
</script>