package com.transretail.crm.core.dto;


import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang3.StringUtils;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.mysema.query.types.expr.BooleanExpression;
import com.transretail.crm.common.service.dto.request.AbstractSearchFormDto;
import com.transretail.crm.core.entity.QProgram;
import com.transretail.crm.core.util.BooleanExprUtil;


public class ProgramSearchDto extends AbstractSearchFormDto {

	private Long id;
	private String status;
	private String name;
    private String description;
    private Date startDate;
    private Date endDate;

    private String modelType;




	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Date getStartDate() {
		return startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	public Date getEndDate() {
		return endDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	public String getModelType() {
		return modelType;
	}

	public void setModelType(String modelType) {
		this.modelType = modelType;
	}




	@Override
	@JsonIgnore
	public BooleanExpression createSearchExpression() {
		
		List<BooleanExpression> expressions = new ArrayList<BooleanExpression>();
		QProgram theProgram = QProgram.program;

        if ( !StringUtils.isBlank( status ) ) {
        	expressions.add( BooleanExprUtil.INSTANCE.isStringPropertyEqual( theProgram.status.code, status ) );
        }

        if ( startDate != null ) {
            expressions.add( BooleanExprUtil.INSTANCE.isDatePropertyLoe( theProgram.duration.startDate, startDate ) );
        }

        if ( endDate != null ) {
            expressions.add( BooleanExprUtil.INSTANCE.isDatePropertyGoe( theProgram.duration.endDate, endDate ) );
        }

        if ( StringUtils.isNotBlank( name ) ) {
        	expressions.add( BooleanExprUtil.INSTANCE.isStringPropertyLike( theProgram.name, name ) );
		}

        if ( StringUtils.isNotBlank( description ) ) {
        	expressions.add( BooleanExprUtil.INSTANCE.isStringPropertyLike( theProgram.description, description ) );
		}

		return BooleanExpression.allOf(expressions.toArray(new BooleanExpression[expressions.size()]));
	}
}
