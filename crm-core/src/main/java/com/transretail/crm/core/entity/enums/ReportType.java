package com.transretail.crm.core.entity.enums;

public enum ReportType {

	PDF ("pdf"),
	EXCEL ("excel");

    private ReportType(String code){
        this.code = code;
    }
    private final String code;
	public String getCode() {
		return code;
	}
}
