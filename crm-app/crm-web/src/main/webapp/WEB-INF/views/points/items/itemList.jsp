<%@ include file="../../common/taglibs.jsp" %>

<div id="content_itemList" class="modal hide  nofly modal-dialog">

  <div class="modal-content">
    <div class="modal-header">
      <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
      <h4><spring:message code="points_label_items"/></h4>
    </div>

    <div class="modal-body">
      <div class="row-fluid form-inline">
		    <div class="span6 control-group">
		      <label for="transactionNo" class="control-label"><spring:message code="points_prop_txnno" />:<![CDATA[&nbsp; ]]></label>
		      <span class="control-label" id="content_transactionNo"></span>
		    </div>
		  </div>

		  <div id="list_item" 
		    data-txn-no=""
		    data-url="<c:url value="/member/points/item/list" />"
		    data-hdr-itemcode="<spring:message code="item_prop_item_code"/>"
        data-hdr-pluid="<spring:message code="item_prop_plu_id"/>"
        data-hdr-sku="<spring:message code="item_prop_sku"/>"
        data-hdr-itemdesc="<spring:message code="item_prop_item_desc"/>"
        data-hdr-itemprice="<spring:message code="item_prop_item_price"/>"
		    data-hdr-quantity="<spring:message code="item_prop_quantity"/>" 
        data-hdr-totalprice="<spring:message code="item_prop_total_cost"/>" 
		    data-hdr-salestype="<spring:message code="item_prop_salestype"/>" ></div>
    </div>
  </div>


  <link href="<spring:url value="/css/dataTables.css" />" rel="stylesheet">
  <script src="<spring:url value="/js/jquery/jquery.dataTables.js" />"></script>
  <script src="<spring:url value="/js/jquery/jquery.dataTables.linkLengthMenu.js" />"></script>
  <script src="<spring:url value="/js/jquery/jquery.dataTables.customPaging.js" />"></script>
  <script src="<spring:url value="/js/jquery/ajax-datatable.js" />"></script>
  <script src="<spring:url value="/js/common/form2js.js" />"></script>
  <script src="<spring:url value="/js/common/json2.js" />"></script>
  <script src="<spring:url value="/js/jquery/jquery.toObject.js" />"></script>

  <script src="<spring:url value="/js/viewspecific/points/items/itemList.js" />" type="text/javascript"></script>
  <link href="<c:url value="/styles/temp.css" />" rel="stylesheet" />


</div>