package com.transretail.crm.impl;

import com.google.common.collect.Lists;
import com.transretail.crm.common.reporting.jasper.JRProcessor;
import com.transretail.crm.core.entity.lookup.LookupDetail;
import com.transretail.crm.core.entity.lookup.LookupHeader;
import com.transretail.crm.core.repo.LookupDetailRepo;
import com.transretail.crm.core.repo.LookupHeaderRepo;
import com.transretail.crm.core.security.model.impl.CustomSecurityUserDetailsImpl;
import com.transretail.crm.core.service.CodePropertiesService;
import com.transretail.crm.giftcard.dto.GiftCardOrderDto;
import com.transretail.crm.giftcard.dto.GiftCardOrderItemDto;
import com.transretail.crm.giftcard.dto.GiftCardOrderResultList;
import com.transretail.crm.giftcard.dto.GiftCardOrderSearchDto;
import com.transretail.crm.giftcard.entity.*;
import com.transretail.crm.giftcard.entity.ProductProfile.Denomination;
import com.transretail.crm.giftcard.entity.support.GiftCardOrderStatus;
import com.transretail.crm.giftcard.repo.CardVendorRepo;
import com.transretail.crm.giftcard.repo.GiftCardInventoryRepo;
import com.transretail.crm.giftcard.repo.GiftCardOrderRepo;
import com.transretail.crm.giftcard.repo.ProductProfileRepo;
import com.transretail.crm.giftcard.service.GiftCardOrderService;
import org.joda.time.LocalDate;
import org.junit.*;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.security.authentication.TestingAuthenticationToken;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.context.SecurityContextImpl;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

import static junit.framework.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

/**
 *
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath*:/META-INF/spring/applicationContext*.xml",
    "classpath:META-INF/spring/applicationContext-test.xml"})
public class GiftCardOrderServiceTest {
    @Autowired
    private GiftCardOrderRepo giftCardOrderRepo;

    @Autowired
    private GiftCardInventoryRepo giftCardInventoryRepo;

    @Autowired
    private ProductProfileRepo productProfileRepo;

    @Autowired
    private CardVendorRepo vendorRepo;
    @Autowired
    private GiftCardOrderService service;
    @Autowired
    private ProductProfileRepo profileRepo;

    @Autowired
    private MessageSource messageSource;

    private CardVendor savedVendor;
    private GiftCardOrder savedGiftCardOrder;
    private ProductProfile savedProfile;
    

    @Autowired
    private CodePropertiesService codePropertiesService;
    @Autowired
    private LookupHeaderRepo lookupHeaderRepo;
    @Autowired
    private LookupDetailRepo lookupDetailRepo;

    @BeforeClass
    public static void setupCurrentUser() {
        CustomSecurityUserDetailsImpl user = new CustomSecurityUserDetailsImpl();
        user.setUserId(1L);
        user.setUsername("user");
        List<GrantedAuthority> authorities = Lists.newArrayList();
        authorities.add(new SimpleGrantedAuthority("ROLE_MERCHANT_SERVICE"));
        user.setAuthorities(authorities);

        SecurityContextImpl securityContext = new SecurityContextImpl();
        securityContext.setAuthentication(new TestingAuthenticationToken(user, ""));
        SecurityContextHolder.setContext(securityContext);
    }

    @AfterClass
    public static void removeCurrentUser() {
        SecurityContextHolder.clearContext();
    }

    @Before
    public void onSetup() {
        savedVendor = new CardVendor();
        savedVendor.setFormalName("name");
        savedVendor.setMailAddress("add");
        savedVendor.setEmailAddress("email");
        savedVendor.setEnabled(true);
        savedVendor.setRegion("region");
        savedVendor.setDefaultVendor(true);
        savedVendor = vendorRepo.save(savedVendor);
        
        
        LookupHeader hdr = new LookupHeader(codePropertiesService.getProductProfileFaceValue());
        hdr = lookupHeaderRepo.saveAndFlush(hdr);

        LookupDetail dtl = new LookupDetail("PPFV1");
        dtl.setDescription("100000");
        dtl.setHeader(hdr);
        dtl = lookupDetailRepo.saveAndFlush(dtl);
        
        savedProfile = new ProductProfile();
        savedProfile.setFaceValue(dtl);
        savedProfile.setProductCode("000000001");
        savedProfile.setProductDesc("PRODUCT PROFILE DESC");
        savedProfile = profileRepo.save(savedProfile);

        savedGiftCardOrder = new GiftCardOrder();
        savedGiftCardOrder.setMoNumber("140403000001");
        savedGiftCardOrder.setPoNumber("0001");
        savedGiftCardOrder.setMoDate(new LocalDate());
        savedGiftCardOrder.setPoDate(new LocalDate());
        savedGiftCardOrder.setVendor(savedVendor);
        savedGiftCardOrder.setStatus(GiftCardOrderStatus.DRAFT);
        savedGiftCardOrder.setItems(new ArrayList<GiftCardOrderItem>());
        
        GiftCardOrderItem item = new GiftCardOrderItem();
        item.setProfile(savedProfile);
        item.setProductCode(savedProfile.getProductCode());
        item.setQuantity(10L);
        item.setOrder(savedGiftCardOrder);
        savedGiftCardOrder.getItems().add(item);
        
        savedGiftCardOrder = giftCardOrderRepo.save(savedGiftCardOrder);
    }

    @After
    public void tearDown() {
    	giftCardOrderRepo.deleteAll();
    	vendorRepo.deleteAll();
    	profileRepo.deleteAll();
    	
    }

    @Test
    public void saveGiftCardOrderTest() {
        GiftCardOrderDto dto = new GiftCardOrderDto();
        dto.setPoNumber("0001");
        dto.setMoDate(new LocalDate());
        dto.setPoDate(new LocalDate());
        dto.setVendorId(savedVendor.getId());
        dto.setStatus(GiftCardOrderStatus.DRAFT);
        dto.setItems(new ArrayList<GiftCardOrderItemDto>());
        GiftCardOrderItemDto item = new GiftCardOrderItemDto();
        item.setProfileId(savedProfile.getId());
        item.setQuantity(10L);
        dto.getItems().add(item);
        
        service.saveGiftCardOrder(dto);

        assertTrue(giftCardOrderRepo.count() == 2);
    }


    @Test
    public void getGiftCardOrderTest() {
        GiftCardOrderDto dto = service.getGiftCardOrder(savedGiftCardOrder.getId());
        assertEquals("140403000001", dto.getMoNumber());
        assertEquals(savedVendor.getId(), dto.getVendorId());
        assertEquals(savedVendor.getFormalName(), dto.getVendorName());
    }

    @Test
    public void getGiftCardOrdersTest() {
        GiftCardOrderSearchDto searchForm = new GiftCardOrderSearchDto();
        GiftCardOrderResultList resultList = service.getGiftCardOrders(searchForm);
        assertEquals(1, resultList.getNumberOfElements());
        assertEquals(1, resultList.getTotalElements());
        GiftCardOrderDto dto = resultList.getResults().iterator().next();
        assertEquals("140403000001", dto.getMoNumber());
        assertEquals(savedVendor.getId(), dto.getVendorId());
        assertEquals(savedVendor.getFormalName(), dto.getVendorName());

        searchForm.setMoNo("unexxisting");
        resultList = service.getGiftCardOrders(searchForm);
        assertEquals(0, resultList.getNumberOfElements());
        assertEquals(0, resultList.getTotalElements());
        assertEquals(0, resultList.getResults().size());
    }
    
    @Test
    public void deleteGiftCardOrderTest() {
        service.deleteGiftCardOrder(savedGiftCardOrder.getId());
        assertTrue(giftCardOrderRepo.count() == 0);
    }
    
    @Test
    public void approveGiftCardOrderTest() {
        service.approveGiftCardOrder(savedGiftCardOrder.getId(), GiftCardOrderStatus.FOR_APPROVAL);
        assertTrue(giftCardOrderRepo.count(QGiftCardOrder.giftCardOrder.status.eq(GiftCardOrderStatus.FOR_APPROVAL)) == 1);
    }

    @Test
    @Transactional
    public void testReport() throws Exception {
        //test data
        CardVendor vendor = new CardVendor();
        vendor.setFormalName("name1");
        vendor.setMailAddress("add1");
        vendor.setEmailAddress("email1");
        vendor.setEnabled(true);
        vendor.setRegion("region1");
        vendor.setDefaultVendor(true);
        vendor = vendorRepo.save(vendor);

        ProductProfile productProfile = new ProductProfile();
        productProfile.setProductDesc("DESC");

        productProfileRepo.save(productProfile);
        GiftCardOrder cardOrder = new GiftCardOrder();
        cardOrder.setMoNumber("00001");
        cardOrder.setMoDate(LocalDate.now());
        cardOrder.setVendor(vendor);
        cardOrder.setStatus(GiftCardOrderStatus.PARTIAL);

        GiftCardInventory inventory = new GiftCardInventory();
        inventory.setOrderDate(LocalDate.now());
        inventory.setSeries(123456L);
        inventory.setOrder(cardOrder);
        inventory.setProfile(productProfile);
        inventory.setProductCode("001");
        inventory.setProductName("PROD NAME");
        inventory.setSeqNo(12345);
        inventory.setBarcode("12345");
        inventory.setBatchNo(123);

        giftCardOrderRepo.save(cardOrder);
        giftCardInventoryRepo.save(inventory);

        JRProcessor jrProcessor = service.createPendingMOReceiptReport();
        assertTrue(jrProcessor.hasParameter("SUB_DATA_SOURCE"));
    }
}
