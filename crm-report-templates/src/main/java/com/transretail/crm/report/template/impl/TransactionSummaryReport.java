package com.transretail.crm.report.template.impl;

import java.math.BigDecimal;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import com.transretail.crm.core.entity.PeopleSoftStore;
import com.transretail.crm.core.entity.lookup.Store;
import org.apache.commons.lang3.StringUtils;
import org.joda.time.DateTime;
import org.joda.time.LocalDate;
import org.joda.time.YearMonth;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.MessageSourceAware;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.context.support.MessageSourceAccessor;
import org.springframework.stereotype.Service;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.mysema.query.BooleanBuilder;
import com.mysema.query.jpa.JPQLQuery;
import com.mysema.query.jpa.impl.JPAQuery;
import com.mysema.query.types.Projections;
import com.mysema.query.types.expr.CaseBuilder;
import com.mysema.query.types.expr.NumberExpression;
import com.mysema.query.types.expr.StringExpression;
import com.transretail.crm.common.reporting.jasper.ClasspathFileResolver;
import com.transretail.crm.common.reporting.jasper.DefaultJRProcessor;
import com.transretail.crm.common.reporting.jasper.JRProcessor;
import com.transretail.crm.common.util.DateUtil;
import com.transretail.crm.core.dto.StoreDto;
import com.transretail.crm.core.entity.QPeopleSoftStore;
import com.transretail.crm.core.entity.enums.ReportType;
import com.transretail.crm.core.entity.lookup.LookupDetail;
import com.transretail.crm.core.security.util.UserUtil;
import com.transretail.crm.core.service.CodePropertiesService;
import com.transretail.crm.core.service.LookupService;
import com.transretail.crm.core.service.PsoftStoreService;
import com.transretail.crm.giftcard.entity.QGiftCardInventory;
import com.transretail.crm.giftcard.entity.QGiftCardTransaction;
import com.transretail.crm.giftcard.entity.QGiftCardTransactionItem;
import com.transretail.crm.giftcard.entity.QProductProfile;
import com.transretail.crm.giftcard.entity.QSalesOrder;
import com.transretail.crm.giftcard.entity.QSalesOrderAlloc;
import com.transretail.crm.giftcard.entity.QSalesOrderItem;
import com.transretail.crm.giftcard.entity.support.GiftCardSalesType;
import com.transretail.crm.giftcard.repo.GiftCardTransactionRepo;
import com.transretail.crm.report.template.CommonReportFilter;
import com.transretail.crm.report.template.FilterField;
import com.transretail.crm.report.template.NoDataFoundSupport;
import com.transretail.crm.report.template.ReportDateFormat;
import com.transretail.crm.report.template.ReportTemplate;
import com.transretail.crm.report.template.ReportsCustomizer;
import com.transretail.crm.report.template.ValidatableReportTemplate;

/**
 * @author ftopico
 */
@Service("transactionSummaryReport")
public class TransactionSummaryReport implements ValidatableReportTemplate, MessageSourceAware, NoDataFoundSupport {
    
    @PersistenceContext
    private EntityManager em;
    @Autowired
    private GiftCardTransactionRepo giftCardTransactionRepo;
    @Autowired
    private LookupService lookupService;
    @Autowired
    private CodePropertiesService codePropertiesService;

    private static final String PERMISSION_CODE = "TRANSACTION_SUMMARY_REPORT";
    protected static final String TEMPLATE_NAME = "Transaction Summary Report";
    protected static final String REPORT_NAME_MONTH = "reports/transaction_summary_month_report.jasper";
    protected static final String REPORT_NAME_PERIOD = "reports/transaction_summary_period_report.jasper";
    protected static final String REPORT_TYPE_MONTH = "MONTH";
    protected static final String REPORT_TYPE_PERIOD = "PERIOD";
    protected static final String SUBTOTAL_TYPE_PERIOD = "SUBTOTAL_SALES_TYPE";
    protected static final String REPORT_FILTER_DATE_FROM = "DATE_FROM";
    protected static final String REPORT_FILTER_DATE_TO = "DATE_TO";
    protected static final String REPORT_FILTER_REPORT_TYPES = "REPORT_TYPES";
    protected static final String REPORT_FILTER_SUBTOTAL_TYPES = "SUBTOTAL_TYPES";
    protected static final String REPORT_FILTER_AREA_REGION = "REGION";
    protected static final String REPORT_FILTER_BU = "BU";
    protected static final String REPORT_FILTER_SALES_TYPE = "SALES_TYPE";
    protected static final String REPORT_FILTER_DATE_FORMAT = "dd-MM-yyyy";
    protected static final String REPORT_FILTER_STORE = "STORE";
    
    //Header Params
    private static final String PARAM_PRINT_DATE = "printedDate";
    private static final String PARAM_PRINTED_BY = "printedBy";
    private static final String PARAM_TRANSACTION_DT_FROM = "transactionDateFrom";
    private static final String PARAM_TRANSACTION_DT_TO = "transactionDateTo";
    private static final String PARAM_SUBTOTAL_TYPE = "subTotalType";
    private static final DateTimeFormatter FULL_DATE_PATTERN = DateTimeFormat.forPattern("dd-MM-yyyy").withLocale(LocaleContextHolder.getLocale());
    private static final DateTimeFormatter RAW_YEAR_MONTH_FORMATTER = DateTimeFormat.forPattern("yyyyMM")
            .withLocale(LocaleContextHolder.getLocale());
    private MessageSourceAccessor messageSource;
    @Autowired
    private PsoftStoreService psoftStoreService;
    
    public TransactionSummaryReport() {
    }
    
    @Override
    public void setMessageSource(MessageSource messageSource) {
        this.messageSource = new MessageSourceAccessor(messageSource);
    }

    @Override
    public String getName() {
        return TEMPLATE_NAME;
    }

    @Override
    public String getDescription() {
        return messageSource.getMessage("gc_transaction_summary_lbl_title");
    }

    @Override
    public Set<FilterField> getFilters() {
        Set<FilterField> filters = new LinkedHashSet<FilterField>();
        //Transaction Dates
        filters.add(FilterField.createDateField(REPORT_FILTER_DATE_FROM,
                messageSource.getMessage("gc_expirebalance_report_label_datefrom"),
                DateUtil.SEARCH_DATE_FORMAT, FilterField.DateTypeOption.PLAIN, true));
        filters.add(FilterField.createDateField(REPORT_FILTER_DATE_TO,
                messageSource.getMessage("gc_expirebalance_report_label_dateto"),
                DateUtil.SEARCH_DATE_FORMAT, FilterField.DateTypeOption.PLAIN, true));
        
        //Report Types
        Map<String, String> reportTypes = new LinkedHashMap<String, String>();
        reportTypes.put(REPORT_TYPE_MONTH, messageSource.getMessage("gc_transaction_summary_lbl_report_type_month"));
        reportTypes.put(REPORT_TYPE_PERIOD, messageSource.getMessage("gc_transaction_summary_lbl_report_type_period"));
        filters.add(FilterField.createDropdownField(REPORT_FILTER_REPORT_TYPES, messageSource.getMessage("gc_transaction_summary_lbl_report_type"), reportTypes, true));
        
        //Subtotals
        Map<String, String> subtotals = new LinkedHashMap<String, String>();
        subtotals.put(SUBTOTAL_TYPE_PERIOD, messageSource.getMessage("gc_transaction_summary_lbl_subtotals_sales_type"));
        filters.add(FilterField.createDropdownField(REPORT_FILTER_SUBTOTAL_TYPES, messageSource.getMessage("gc_transaction_lbl_subtotal_type"), subtotals));


        final String currentUserStoreCode = UserUtil.getCurrentUser().getStoreCode();
        PeopleSoftStore storeMapping = psoftStoreService.getStoreMapping(currentUserStoreCode);

        // BU
        Map<String, String> bu = new LinkedHashMap<String, String>();
        if (!currentUserStoreCode.equals("10007")) {

            final LookupDetail detail = storeMapping.getBusinessUnit();
            bu.put(detail.getCode(), detail.getDescription());

        } else {

            List<LookupDetail> buList = lookupService.getActiveDetailsByHeaderCode(codePropertiesService.getHeaderBusinessUnit());
            Collections.sort(buList, ALPHA_ORDER);
            for (LookupDetail buDetail : buList) {
                bu.put(buDetail.getCode(),buDetail.getDescription());
            }
        }

        filters.add(FilterField.createDropdownField(REPORT_FILTER_BU, messageSource.getMessage("gc_transaction_lbl_bu"), bu));
        
        
        // Sales Type
        Map<String, String> salesTypes = new LinkedHashMap<String, String>();
        for (GiftCardSalesType gcSalesType : GiftCardSalesType.values()) {
            salesTypes.put(gcSalesType.name(), gcSalesType.name());
        }
        filters.add(FilterField.createDropdownField(REPORT_FILTER_SALES_TYPE, messageSource.getMessage("gc_transaction_lbl_sales_type"), salesTypes));
        
        // Area/Region
        Map<String, String> region = new LinkedHashMap<String, String>();
        List<LookupDetail> regionList = lookupService.getActiveDetailsByHeaderCode(codePropertiesService.getHeaderBusinessRegion());
        Collections.sort(regionList, ALPHA_ORDER);
        for (LookupDetail regionDetail : regionList) {
            region.put(regionDetail.getCode(), regionDetail.getDescription());
        }
        filters.add(FilterField.createDropdownField(REPORT_FILTER_AREA_REGION, messageSource.getMessage("gc_transaction_lbl_area"), region));

        // Store
        Map<String, String> storeMap = Maps.newLinkedHashMap();
        List<StoreDto> stores = psoftStoreService.getGcAllowedStores();
        if (!currentUserStoreCode.equals("10007")) {

            for (StoreDto dto : stores) {

                if (dto.getCode().equals(currentUserStoreCode)) {

                    storeMap.put(dto.getCode(), dto.getCodeAndName());
                }
            }

        } else {

            for (StoreDto store : stores) {
                storeMap.put(store.getCode(), store.getCodeAndName());
            }
        }

        filters.add(FilterField.createDropdownField(REPORT_FILTER_STORE, messageSource.getMessage("gc_transaction_lbl_store"), storeMap));
        
        return filters;
    }

    private static Comparator<LookupDetail> ALPHA_ORDER = new Comparator<LookupDetail>() {
        public int compare(LookupDetail ld1, LookupDetail ld2) {
            int x = String.CASE_INSENSITIVE_ORDER.compare(ld1.getDescription(), ld2.getDescription());
            if (x== 0) {
                x= ld1.getDescription().compareTo(ld2.getDescription());
            }
            return x;
        }
    };
    
    @Override
    public boolean canView(Set<String> permissions) {
        return permissions.contains(PERMISSION_CODE);
    }

    @Override
    public ReportType[] getReportTypes() {
        return ReportType.values();
    }

    @Override
    public JRProcessor createJrProcessor(Map<String, String> map) {
        
        List<TransactionSummary> giftCardTransactions = getGiftCardTransactions(map);
        List<ReportBean> beansDataSource = Lists.newArrayList();
        
        if (!giftCardTransactions.isEmpty())
            beansDataSource.add(new ReportBean(giftCardTransactions));
        
        Map<String, Object> parameters = new HashMap<String, Object>();
        parameters.put(PARAM_PRINT_DATE, FULL_DATE_PATTERN.print(new LocalDate()));
        parameters.put(PARAM_PRINTED_BY, UserUtil.getCurrentUser().getUsername());
        parameters.put(PARAM_TRANSACTION_DT_FROM, map.get(REPORT_FILTER_DATE_FROM));
        parameters.put(PARAM_TRANSACTION_DT_TO, map.get(REPORT_FILTER_DATE_TO));
        if (map.get(REPORT_FILTER_REPORT_TYPES).equalsIgnoreCase(REPORT_TYPE_MONTH) || map.get(REPORT_FILTER_REPORT_TYPES) == null || map.get(REPORT_FILTER_REPORT_TYPES).isEmpty()) {
            parameters.put(PARAM_REPORT_TYPE, messageSource.getMessage("gc_transaction_summary_lbl_report_type_month"));
        } else if (map.get(REPORT_FILTER_REPORT_TYPES).equalsIgnoreCase(REPORT_TYPE_PERIOD)) {
            parameters.put(PARAM_REPORT_TYPE, messageSource.getMessage("gc_transaction_summary_lbl_report_type_period"));
        }
        if (map.get(REPORT_FILTER_BU) != null && !map.get(REPORT_FILTER_BU).isEmpty()) {
            LookupDetail buDetail = lookupService.getDetailByCode(map.get(REPORT_FILTER_BU));
            parameters.put("bu", buDetail.getCodeAndDesc());
        }
        
        if (map.get(REPORT_FILTER_AREA_REGION) != null && !map.get(REPORT_FILTER_AREA_REGION).isEmpty()) {
            LookupDetail areaDetail = lookupService.getDetailByCode(map.get(REPORT_FILTER_AREA_REGION));
            parameters.put("area", areaDetail.getCodeAndDesc());
        }
        parameters.put(PARAM_SUBTOTAL_TYPE, messageSource.getMessage("gc_transaction_lbl_sales_type"));
        
        DefaultJRProcessor jrProcessor = null;
        if (map.get(REPORT_FILTER_REPORT_TYPES).equalsIgnoreCase(REPORT_TYPE_MONTH)) {
            jrProcessor = new DefaultJRProcessor(REPORT_NAME_MONTH, beansDataSource);
        } else if (map.get(REPORT_FILTER_REPORT_TYPES).equalsIgnoreCase(REPORT_TYPE_PERIOD)) {
            jrProcessor = new DefaultJRProcessor(REPORT_NAME_PERIOD, beansDataSource);
        }
        else {
        	//default for no-selection
        	jrProcessor = new DefaultJRProcessor(REPORT_NAME_MONTH, beansDataSource);
        }
        jrProcessor.addParameters(parameters);
        jrProcessor.setFileResolver(new ClasspathFileResolver("reports"));
        
        return jrProcessor;
    }
    
    private JPAQuery createQuery(Map<String, String> map) {
        QGiftCardTransaction qtx = QGiftCardTransaction.giftCardTransaction;
        QGiftCardTransactionItem qtxi = QGiftCardTransactionItem.giftCardTransactionItem;
        QGiftCardInventory qgc1 = QGiftCardInventory.giftCardInventory;
        QProductProfile qpp1 = QProductProfile.productProfile;
        QGiftCardInventory qgc2 = new QGiftCardInventory("qgc2");
        QProductProfile qpp2 = new QProductProfile("qpp2");
        QPeopleSoftStore qp = QPeopleSoftStore.peopleSoftStore;
        
        QSalesOrderAlloc qsoa = QSalesOrderAlloc.salesOrderAlloc;
        QSalesOrderItem qsoi = QSalesOrderItem.salesOrderItem;
        QSalesOrder qso = QSalesOrder.salesOrder;

        final Store currentUserStore = UserUtil.getCurrentUser().getStore();
        final PeopleSoftStore storeMapping = psoftStoreService.getStoreMapping(currentUserStore.getCode());

        // predicate
        BooleanBuilder exp = new BooleanBuilder(qtx.transactionDate.between(FULL_DATE_PATTERN.parseLocalDateTime(map.get(REPORT_FILTER_DATE_FROM)), 
                DateUtil.getEndOfDay(FULL_DATE_PATTERN.parseLocalDateTime(map.get(REPORT_FILTER_DATE_TO)))));

        String store = map.get(REPORT_FILTER_STORE);
        if(StringUtils.isNotBlank(store)) {
            exp.and(qtx.merchantId.eq(store));
        }

        // validation for user store login
        if (!currentUserStore.getCode().equals("10007")) {

            if (!map.get(REPORT_FILTER_BU).equalsIgnoreCase("")) {

                // bu
                exp.and(qp.businessUnit.code.eq(storeMapping.getBusinessUnit().getCode()));
                // store
                exp.and(qtx.merchantId.eq(currentUserStore.getCode()));

            }else {

                // bu
                exp.and(qp.businessUnit.code.eq(map.get(REPORT_FILTER_BU)));
                // store
                exp.and(qtx.merchantId.eq(store));
            }

        } else {

            // bu
            exp.and(qp.businessUnit.code.eq(map.get(REPORT_FILTER_BU)));
            // store
            exp.and(qtx.merchantId.eq(store));
        }
        
        if (!map.get(REPORT_FILTER_AREA_REGION).equalsIgnoreCase("")) {
            exp.and(qp.businessRegion.code.eq(map.get(REPORT_FILTER_AREA_REGION)));
        }
        if (!map.get(REPORT_FILTER_SALES_TYPE).equalsIgnoreCase("")) {
            exp.and(qgc1.salesType.eq(GiftCardSalesType.valueOf(map.get(REPORT_FILTER_SALES_TYPE)))
                    .or(qgc2.salesType.eq(GiftCardSalesType.valueOf(map.get(REPORT_FILTER_SALES_TYPE)))));
        }

        
        JPAQuery query = new JPAQuery(em);
        query = query.from(qtx, qgc2)
            .leftJoin(qtx.transactionItems, qtxi)
            .leftJoin(qtxi.giftCard, qgc1)
            .leftJoin(qgc1.profile, qpp1)
            .leftJoin(qtx.salesOrder, qso)
            .innerJoin(qso.items, qsoi)
            .innerJoin(qsoi.soAlloc, qsoa)
            .leftJoin(qgc2.profile, qpp2)
            .leftJoin(qtx.peoplesoftStoreMapping, qp)
            .where(exp.getValue(), qgc2.series.goe(qsoa.seriesStart).and(qgc2.series.loe(qsoa.seriesEnd)));
        
        return query;
    }
    
    private List<TransactionSummary> getGiftCardTransactions(
            Map<String, String> map) {
        
        QGiftCardTransaction qtx = QGiftCardTransaction.giftCardTransaction;
        QGiftCardTransactionItem qtxi = QGiftCardTransactionItem.giftCardTransactionItem;
        QGiftCardInventory qgc1 = QGiftCardInventory.giftCardInventory;
        QProductProfile qpp1 = QProductProfile.productProfile;
        QGiftCardInventory qgc2 = new QGiftCardInventory("qgc2");
        QProductProfile qpp2 = new QProductProfile("qpp2");
        QPeopleSoftStore qp = QPeopleSoftStore.peopleSoftStore;
        
        QSalesOrderAlloc qsoa = QSalesOrderAlloc.salesOrderAlloc;
        QSalesOrder qso = QSalesOrder.salesOrder;
        
        BooleanBuilder exp = new BooleanBuilder(qtx.transactionDate.between(FULL_DATE_PATTERN.parseLocalDateTime(map.get(REPORT_FILTER_DATE_FROM)), 
                DateUtil.getEndOfDay(FULL_DATE_PATTERN.parseLocalDateTime(map.get(REPORT_FILTER_DATE_TO)))));
        if (!map.get(REPORT_FILTER_BU).equalsIgnoreCase("")) {
            exp.and(qp.businessUnit.code.eq(map.get(REPORT_FILTER_BU)));
        }
        
        if (!map.get(REPORT_FILTER_AREA_REGION).equalsIgnoreCase("")) {
            exp.and(qp.businessRegion.code.eq(map.get(REPORT_FILTER_AREA_REGION)));
        }
        if (!map.get(REPORT_FILTER_SALES_TYPE).equalsIgnoreCase("")) {
            exp.and(qgc1.salesType.eq(GiftCardSalesType.valueOf(map.get(REPORT_FILTER_SALES_TYPE)))
                    .or(qgc2.salesType.eq(GiftCardSalesType.valueOf(map.get(REPORT_FILTER_SALES_TYPE)))));
        }
        String store = map.get(REPORT_FILTER_STORE);
        if(StringUtils.isNotBlank(store)) {
        	exp.and(qtx.merchantId.eq(store));
        }
        
        StringExpression salesType = new CaseBuilder().when(qgc1.salesType.isNotNull()).then(qgc1.salesType.stringValue()).otherwise(qgc2.salesType.stringValue());
        NumberExpression<BigDecimal> faceAmount = new CaseBuilder().when(qgc1.faceValue.isNotNull()).then(qgc1.faceValue).otherwise(qgc2.faceValue);
        NumberExpression<Double> cardFee = new CaseBuilder().when(qpp1.cardFee.isNotNull()).then(qpp1.cardFee)
                                            .when(qpp2.cardFee.isNotNull()).then(qpp2.cardFee).otherwise(Double.valueOf(0L));
        NumberExpression<BigDecimal> handlingFee = new CaseBuilder().when(qtx.salesOrder.isNotNull()).then(qtx.salesOrder.handlingFee).otherwise(BigDecimal.ZERO);
        
        JPAQuery query = createQuery(map).groupBy(salesType, 
                    qtx.transactionDate.yearMonth(),
                    qp.businessUnit.code)
            .orderBy(qtx.transactionDate.yearMonth().desc());
        
        return query.list(Projections.fields(TransactionSummary.class,
                            salesType.as("salesType"),
                            qtx.transactionDate.yearMonth().as("transactionDate"),
                            qp.businessUnit.code.coalesce("").as("bu"),
                            qsoa.count().coalesce(qtxi.count()).as("totalQuantity"),
                            faceAmount.doubleValue().sum().coalesce(0.0).as("faceAmount"),
                            qso.discountVal.doubleValue().avg().coalesce(0.0).as("prepaidDiscount"),
                            qtxi.transactionAmount.doubleValue().sum().coalesce(0.0).as("redeemedAmount"),
                            qso.shippingFee.doubleValue().sum().coalesce(0.0).as("shippingFee"),
                            cardFee.doubleValue().sum().coalesce(0.0).as("cardFee"),
                            handlingFee.doubleValue().sum().coalesce(0.0).as("handleFee")
                            ));
    }

    public static class TransactionSummary {
        private Integer transactionDate;
        private String bu;
        private String salesType;
        private String area;
        private Long totalQuantity;
        private Double faceAmount;
        private Double prepaidDiscount;
        private Double redeemedAmount;
        private Double shippingFee;
        private Double cardFee;
        private Double handleFee;
        
        public TransactionSummary() {
        }

        public Integer getTransactionDate() {
            return transactionDate;
        }

        public YearMonth getYearMonth() {
            return YearMonth.parse(String.valueOf(transactionDate), RAW_YEAR_MONTH_FORMATTER);
        }
        
        public String getBu() {
            return bu;
        }

        public String getSalesType() {
            return salesType;
        }

        public String getArea() {
            return area;
        }

        public Long getTotalQuantity() {
            return totalQuantity;
        }

        public Double getFaceAmount() {
            return faceAmount;
        }

        public Double getPrepaidDiscount() {
            return prepaidDiscount;
        }

        public Double getRedeemedAmount() {
            return redeemedAmount;
        }

        public Double getShippingFee() {
            return shippingFee;
        }

        public Double getCardFee() {
            return cardFee;
        }

        public Double getHandleFee() {
            return handleFee;
        }

        public void setTransactionDate(Integer transactionDate) {
            this.transactionDate = transactionDate;
        }

        public void setBu(String bu) {
            this.bu = bu;
        }

        public void setSalesType(String salesType) {
            this.salesType = salesType;
        }

        public void setArea(String area) {
            this.area = area;
        }

        public void setTotalQuantity(Long totalQuantity) {
            this.totalQuantity = totalQuantity;
        }

        public void setFaceAmount(Double faceAmount) {
            this.faceAmount = faceAmount;
        }

        public void setPrepaidDiscount(Double prepaidDiscount) {
            this.prepaidDiscount = prepaidDiscount;
        }

        public void setRedeemedAmount(Double redeemedAmount) {
            this.redeemedAmount = redeemedAmount;
        }

        public void setShippingFee(Double shippingFee) {
            this.shippingFee = shippingFee;
        }

        public void setCardFee(Double cardFee) {
            this.cardFee = cardFee;
        }

        public void setHandleFee(Double handleFee) {
            this.handleFee = handleFee;
        }

    }
    
    public static class ReportBean {
        
        private List<TransactionSummary> list;
        
        public ReportBean() {
        }
        
        public ReportBean(List<TransactionSummary> list) {
            this.list = list;
        }

        public List<TransactionSummary> getList() {
            return list;
        }

        public void setList(List<TransactionSummary> list) {
            this.list = list;
        }
        
    }

    @Override
    public boolean isEmpty(Map<String, String> filter) {
        return !createQuery(filter).exists();
    }

    @Override
    public List<String> validate(Map<String, String> parameters) {
        List<String> errors = Lists.newArrayList();
        
        String dtFrom = parameters.get(CommonReportFilter.DATE_FROM);
        String dtTo = parameters.get(CommonReportFilter.DATE_FROM);

        if (StringUtils.isNotBlank(dtFrom) && StringUtils.isNotBlank(dtTo)) {
            DateTime dateFrom = ReportDateFormat.DATE_FORMATTER.parseDateTime(dtFrom);
            DateTime dateTo = ReportDateFormat.DATE_FORMATTER.parseDateTime(dtTo);
            if (dateFrom.isAfter(dateTo) || dateTo.isBefore(dateFrom)) {
                errors.add(messageSource.getMessage("report.date.from.before.date.to"));
            }
        } else {
            errors.add(messageSource.getMessage("report.date.from.date.to.required"));
        }
        
        if(StringUtils.isBlank(parameters.get("reportType"))) {
            errors.add("Report Type is required.");
        }
        
        return errors;
    }
}
