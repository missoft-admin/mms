create table CRM_LC_PHYSICAL_COUNT_SUMMARY (
        ID number(19,0) not null,
        CREATED_BY varchar2(255 char),
        CREATED_DATETIME timestamp,
        CURRENT_INVENTORY number(19,0),
        PHYSICAL_COUNT number(19,0),
        CARD_TYPE varchar2(15 char) not null,
        LOCATION varchar2(15 char) not null,
        primary key (ID)
    );

alter table CRM_LC_PHYSICAL_COUNT_SUMMARY 
        add constraint FK_aauafloynb8trq9q116h9y4b2 
        foreign key (CARD_TYPE) 
        references CRM_REF_LOOKUP_DTL;

    alter table CRM_LC_PHYSICAL_COUNT_SUMMARY 
        add constraint FK_g98jeh61rmfxg4t2r6j4y6obm 
        foreign key (LOCATION) 
        references CRM_REF_LOOKUP_DTL;