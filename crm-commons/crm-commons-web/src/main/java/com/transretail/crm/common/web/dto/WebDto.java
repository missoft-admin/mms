package com.transretail.crm.common.web.dto;

import java.io.Serializable;

/**
 * @author aco
 */
public interface WebDto<TARGET> extends Serializable {
    TARGET target();
}
