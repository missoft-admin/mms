package com.transretail.crm.core.service;


import java.util.List;

import com.transretail.crm.common.service.dto.response.ResultList;
import com.transretail.crm.core.dto.StoreMemberDto;
import com.transretail.crm.core.dto.StoreMemberSearchDto;
import com.transretail.crm.core.entity.StoreMember;

public interface StoreMemberService {

    List<StoreMember> findByStore(String store);

    ResultList<StoreMemberDto> searchStoreMembers(StoreMemberSearchDto searchDto);
    
    StoreMember findById(Long id);
    
    boolean isValidMember(String store, String accountId, StringBuffer msgCode);
    
    void saveStoreMemberDto(StoreMemberDto storeMemberDto);

    void updateStoreMemberDto(StoreMemberDto storeMemberDto);

    void deleteStoreMemberModel(StoreMember storeMember);

    boolean isStoreMemberSchemeExists(StoreMemberDto dto);

    StoreMember findByIdAndPreparedForView(Long id);

}
