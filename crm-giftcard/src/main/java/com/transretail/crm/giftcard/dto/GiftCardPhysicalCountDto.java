package com.transretail.crm.giftcard.dto;

import java.util.List;

import org.springframework.beans.BeanUtils;

import com.google.common.collect.Lists;
import com.transretail.crm.giftcard.entity.GiftCardPhysicalCount;

/**
 *
 * @author Monte Cillo Co (mco@exist.com)
 */
public class GiftCardPhysicalCountDto {

    private String productProfile;
    private String productProfileName;
    private String location;
    private String locationName;
    private String startingSeries;
    private String endingSeries;
    private Long quantity;
    private List<String> barcodes;

    public String getLocation() {
	return location;
    }

    public void setLocation(String location) {
	this.location = location;
    }

    public String getLocationName() {
		return locationName;
	}

	public void setLocationName(String locationName) {
		this.locationName = locationName;
	}

	public String getStartingSeries() {
	return startingSeries;
    }

    public void setStartingSeries(String startingSeries) {
	this.startingSeries = startingSeries;
    }

    public String getEndingSeries() {
	return endingSeries;
    }

    public void setEndingSeries(String endingSeries) {
	this.endingSeries = endingSeries;
    }

    public Long getQuantity() {
	return quantity;
    }

    public void setQuantity(Long quantity) {
	this.quantity = quantity;
    }

    public List<String> getBarcodes() {
	if (barcodes == null) {
	    barcodes = Lists.newArrayList();
	}

	return barcodes;
    }

    public void setBarcodes(List<String> barcodes) {
	this.barcodes = barcodes;
    }

    public String getProductProfile() {
	return productProfile;
    }

    public void setProductProfile(String productProfile) {
	this.productProfile = productProfile;
    }

    public String getProductProfileName() {
	return productProfileName;
    }

    public void setProductProfileName(String productProfileName) {
	this.productProfileName = productProfileName;
    }

    public GiftCardPhysicalCount toModel() {
	GiftCardPhysicalCount model = new GiftCardPhysicalCount();
	BeanUtils.copyProperties(this, model);
	return model;
    }

}
