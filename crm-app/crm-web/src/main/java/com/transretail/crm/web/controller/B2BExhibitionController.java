package com.transretail.crm.web.controller;

import java.math.BigDecimal;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.google.common.collect.Lists;
import com.transretail.crm.common.reporting.jasper.JRProcessor;
import com.transretail.crm.common.service.dto.request.PageSortDto;
import com.transretail.crm.common.service.dto.response.ResultList;
import com.transretail.crm.core.entity.lookup.LookupDetail;
import com.transretail.crm.giftcard.dto.B2BExhibitionCountDto;
import com.transretail.crm.giftcard.dto.B2BExhibitionDto;
import com.transretail.crm.giftcard.dto.ReturnRecordDto;
import com.transretail.crm.giftcard.dto.ReturnSearchDto;
import com.transretail.crm.giftcard.entity.GiftCardGeneralDiscount;
import com.transretail.crm.giftcard.entity.GiftCardInventory;
import com.transretail.crm.giftcard.entity.support.ReturnStatus;
import com.transretail.crm.giftcard.service.B2bExhLocDiscService;
import com.transretail.crm.giftcard.service.B2bExhibitionService;
import com.transretail.crm.giftcard.service.DiscountSchemeService;
import com.transretail.crm.giftcard.service.GiftCardAccountingService;
import com.transretail.crm.giftcard.service.GiftCardInventoryService;
import com.transretail.crm.loyalty.dto.ReceiveOrderDto;
import com.transretail.crm.web.controller.util.ControllerResponse;

@Controller
@RequestMapping("/giftcard/b2b/exhibition")
public class B2BExhibitionController extends AbstractReportsController {
	@Autowired
	private MessageSource messageSource;
	@Autowired
	private GiftCardInventoryService giftCardInventoryService;
	@Autowired
	private B2bExhibitionService exhibitionService;
	@Autowired
	private DiscountSchemeService discService;
	@Autowired
	private GiftCardAccountingService accountingService;
	@Autowired
	private B2bExhLocDiscService locDiscService; 
	
	@RequestMapping(produces = "text/html")
    public String show(Model uiModel) {
		uiModel.addAttribute("paymentTypes", exhibitionService.getPaymentTypes());
        return "giftcard/b2b/exhibition";
    }
	
	@RequestMapping(value = "/list", method = RequestMethod.POST)
    public @ResponseBody ResultList<B2BExhibitionDto> search(@RequestBody PageSortDto filterDto) {
    	return exhibitionService.listExhTransaction(filterDto);
    }
	
	
	@RequestMapping(value = "/save", method = RequestMethod.POST, produces = "application/json; charset=utf-8")
	@ResponseBody
	public ControllerResponse ajaxSave(
			@ModelAttribute B2BExhibitionDto exhibitionForm,
			BindingResult result, HttpServletRequest request) {
	
		ControllerResponse theResponse = new ControllerResponse();
		theResponse.setSuccess(true);
		
		if(CollectionUtils.isEmpty(exhibitionForm.getGcs())) {
			result.reject(getMessage("b2b.exhibition.err.emptycards", null));
		}
		
		if(CollectionUtils.isEmpty(exhibitionForm.getPayments())) {
			result.reject(getMessage("b2b.exhibition.err.emptypayments", null));
		}
		
		if(CollectionUtils.isNotEmpty(exhibitionForm.getPayments()) &&
				CollectionUtils.isNotEmpty(exhibitionForm.getGcs()) &&
				exhibitionForm.getChange().compareTo(BigDecimal.ZERO) < 0) {
			result.reject(getMessage("b2b.exhibition.err.insufficientpayment", null));
		}
		
		if (result.hasErrors()) {
			theResponse.setSuccess(false);
			theResponse.setResult(result.getAllErrors());
			return theResponse;
		}
		
		if (theResponse.isSuccess()) {
			B2BExhibitionDto exh = exhibitionService.save(exhibitionForm); 
			theResponse.setResult(exh.getId());
		}

		return theResponse;
	}
	
	@RequestMapping(value = "/validate/payments", method = RequestMethod.POST, produces = "application/json; charset=utf-8")
	@ResponseBody
	public ControllerResponse ajaxValidate(
			@RequestParam LookupDetail type,
			@RequestParam BigDecimal amount) {
		ControllerResponse response = new ControllerResponse();
		List<String> result = Lists.newArrayList();
		validate(type, amount, result);
		if(result.size() == 0) {
			response.setSuccess(true);
		} else {
			response.setSuccess(false);
			response.setResult(result);
		}
		return response;
	}
	
	public void validate(LookupDetail type, BigDecimal amount, List<String> result) {
		if(type == null) {
			result.add(getMessage("propkey_msg_notempty", new Object[]{"b2b.exhibition.type"}));
		}
		
		if(amount == null) {
			result.add(getMessage("propkey_msg_notempty", new Object[]{"b2b.exhibition.amount"}));
		}
			
	}
	
	@RequestMapping(value = "/validate", method = RequestMethod.POST, produces = "application/json; charset=utf-8")
	@ResponseBody
	public ControllerResponse ajaxValidate(
			@RequestParam String startingSeries,
			@RequestParam String endingSeries) {
		ControllerResponse response = new ControllerResponse();
		List<String> result = Lists.newArrayList();
		Long profId = validate(startingSeries, endingSeries, result);
		if(result.size() == 0) {
			response.setSuccess(true);
			response.setResult(profId);
		} else {
			response.setSuccess(false);
			response.setResult(result);
		}
		return response;
	}
	
	private Long validate(String startingSeries, String endingSeries, List<String> result) {
		Long profId = null;
		if(StringUtils.isBlank(startingSeries)) {
			result.add(getMessage("propkey_msg_notempty", new Object[]{"b2b.exhibition.startingseries"}));
		} else {
			GiftCardInventory strtingGc = giftCardInventoryService.getGiftCardByBarcode(startingSeries);
			GiftCardInventory endingGc = null;
			if(strtingGc == null) {
				result.add(getMessage("b2b.exhibition.err.invalidstarting", null));
			} else {
				profId = strtingGc.getProfile().getId();
			}
			if(StringUtils.isNotBlank(endingSeries)) {
				endingGc = giftCardInventoryService.getGiftCardByBarcode(endingSeries);
				if(endingGc == null) {
					result.add(getMessage("b2b.exhibition.err.invalidending", null));
				} else {
					if(!strtingGc.getProductCode().equalsIgnoreCase(endingGc.getProductCode())) {
						result.add(getMessage("b2b.exhibition.err.invalidbarcoderange", null));	
					}
				}
			}
		}
		return profId;
			
	}
	
	
	@RequestMapping(value = "/refreshproductstable", method = RequestMethod.POST, produces = "text/html")
	public String ajaxRefreshProductTable(
			@ModelAttribute B2BExhibitionDto exhibitionForm,
			Model uiModel) {
		
		List<B2BExhibitionCountDto> cnts = exhibitionService.countProducts(exhibitionForm);
		uiModel.addAttribute("countSummary", cnts);
//		GiftCardGeneralDiscount discount = discService.getGeneralDiscount(exhibitionService.getTotalFaceAmount(cnts));
		uiModel.addAttribute("discVal", locDiscService.getDiscount(exhibitionService.getTotalFaceAmount(cnts)));
		uiModel.addAttribute("tax", new Double(0));
		
		return "giftcard/b2b/exhibition/countlist";
	}
	
	@RequestMapping(value = "/refreshpaymentstable", method = RequestMethod.POST, produces = "text/html")
	public String ajaxRefreshPaymentTable(
			@ModelAttribute B2BExhibitionDto exhibitionForm,
			Model uiModel) {
		
		uiModel.addAttribute("paymentSummary", exhibitionForm.getPayments());
		uiModel.addAttribute("totalTaxableAmt", exhibitionForm.getTotalTaxableAmt());
		uiModel.addAttribute("totalAmountDue", exhibitionForm.getTotalAmountDue());
		
		return "giftcard/b2b/exhibition/paymentlist";
	}
	
	private String getMessage(String inMsgCode, Object[] inArgMsgCodes) {
        if (inArgMsgCodes != null) {
            for (int i = 0; i < inArgMsgCodes.length; i++) {
                inArgMsgCodes[i] =
                    messageSource.getMessage(inArgMsgCodes[i].toString().toLowerCase(), null, LocaleContextHolder.getLocale());
            }
        }
        return messageSource.getMessage(inMsgCode, inArgMsgCodes, "Info given is invalid.", LocaleContextHolder.getLocale());
    }
	
	@RequestMapping(value = "/get/{id}", method = RequestMethod.POST, produces = "text/html")
	public String ajaxGetExhTransaction(
			@PathVariable Long id,
			Model uiModel) {
		
		uiModel.addAttribute("exhibitionForm", exhibitionService.getExhTransaction(id));
		
		return "giftcard/b2b/exhibition/get";
	}
	
	
	@RequestMapping(value = "/printreceipt/{id}")
	public void printReceipt(
			@PathVariable("id") Long id,
			HttpServletResponse response) throws Exception {
		JRProcessor jrProcessor = exhibitionService.createJrProcessor(id);
		renderReport(response, jrProcessor);
	}
	
	@RequestMapping(value = "/printkwitansi/{id}")
	public void printKwitansi(
			@PathVariable("id") Long id,
			HttpServletResponse response) throws Exception {
		JRProcessor jrProcessor = exhibitionService.createKwitansiJrProcessor(id);
		renderReport(response, jrProcessor);
	}
}

