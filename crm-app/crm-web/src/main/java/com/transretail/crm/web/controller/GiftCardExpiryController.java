package com.transretail.crm.web.controller;

import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.google.common.collect.Lists;
import com.transretail.crm.common.reporting.jasper.JRProcessor;
import com.transretail.crm.common.service.dto.response.ResultList;
import com.transretail.crm.common.service.exception.MessageSourceResolvableException;
import com.transretail.crm.core.security.util.UserUtil;
import com.transretail.crm.core.service.CodePropertiesService;
import com.transretail.crm.giftcard.dto.GiftCardExpiryExtensionDto;
import com.transretail.crm.giftcard.dto.GiftCardExpiryExtensionSearchDto;
import com.transretail.crm.giftcard.entity.support.GiftCardDateExtendStatus;
import com.transretail.crm.giftcard.entity.support.GiftCardExpirySearchCriteria;
import com.transretail.crm.giftcard.entity.support.GiftCardHandlingFeeType;
import com.transretail.crm.giftcard.service.GiftCardExpiryExtensionService;
import com.transretail.crm.report.template.impl.GiftCardDateExpiryDocument;
import com.transretail.crm.web.controller.util.ControllerResponse;

/**
 * @author ftopico
 */
@RequestMapping("/giftcard/dateexpiry")
@Controller
public class GiftCardExpiryController extends AbstractReportsController {

    private static final String PATH_GC_DATE_EXPIRY = "/giftcard/dateexpiry";
    private static final String PATH_GC_DATE_EXPIRY_CREATE = "/giftcard/dateexpiry/create";
    
    //Search Form
    private static final String UI_ATTR_STATUS = "status";
    private static final String UI_ATTR_HANDLING_FEE_TYPE = "handlingFeeType";
    private static final String UI_ATTR_SEARCH_FIELD = "dateExpirySearchFields";
    private static final String UI_ATTR_DIALOG_LABEL   = "dialogLabel";
    private static final String UI_ATTR_APPROVER = "approver";
    
    protected Logger logger = LoggerFactory.getLogger(getClass());
    
    @Autowired
    private GiftCardExpiryExtensionService giftCardExpiryExtensionService;
    @Autowired
    private CodePropertiesService codePropertiesService;
    @Autowired
    private GiftCardDateExpiryDocument giftCardDateExpiryDocument;
    @Autowired
    private MessageSource messageSource;
    
    @RequestMapping(produces="text/html")
    public String showDateExpiry(Model model) {
        model.addAttribute(UI_ATTR_SEARCH_FIELD, GiftCardExpirySearchCriteria.values());
        model.addAttribute(UI_ATTR_STATUS, GiftCardDateExtendStatus.values());
        return PATH_GC_DATE_EXPIRY;
    }
    
    @RequestMapping(value="/list", method = RequestMethod.POST)
    public @ResponseBody ResultList<GiftCardExpiryExtensionDto> listBurnCardRequests(@RequestBody GiftCardExpiryExtensionSearchDto searchDto) {
        logger.debug("[Method Call from GiftCardExpiryController] giftCardExpiryExtensionService.searchGiftCardDateExpiryExtensionRequests()");
        return giftCardExpiryExtensionService.searchGiftCardDateExpiryExtensionRequests(searchDto);
    }
    
    @RequestMapping(value = "/create", produces="text/html")
    public String createExpiryExtensionRequest(
            @ModelAttribute GiftCardExpiryExtensionDto giftCardExpiryExtensionDto,
            Model model) {
        
        GiftCardExpiryExtensionDto dto = new GiftCardExpiryExtensionDto();
        logger.debug("[Method Call from GiftCardExpiryController] giftCardExpiryExtensionService.createExtendNo()");
        dto.setExtendNo(giftCardExpiryExtensionService.createExtendNo());
        model.addAttribute("creation", true);
        model.addAttribute("expiryExtensionForm", dto);
        model.addAttribute(UI_ATTR_HANDLING_FEE_TYPE, GiftCardHandlingFeeType.values());
        model.addAttribute(UI_ATTR_DIALOG_LABEL, messageSource.getMessage("gc_date_expiry_form_title", 
                null, LocaleContextHolder.getLocale()));
        return PATH_GC_DATE_EXPIRY_CREATE;
    }
    
    @RequestMapping(value = "/view/{extendNo}")
    public String viewExpiryExtensionRequest(
            @PathVariable String extendNo,
            Model model) {
        
        logger.debug("[Method Call from GiftCardExpiryController] giftCardExpiryExtensionService.getExpiryExtensionByExtendNo()");
        GiftCardExpiryExtensionDto dto = giftCardExpiryExtensionService.getExpiryExtensionByExtendNo(extendNo);
        model.addAttribute("view", true);
        String inventoryLocation = UserUtil.getCurrentUser().getInventoryLocation();
        logger.debug("[Method Call from GiftCardExpiryController] codePropertiesService.getDetailInvLocationHeadOffice()");
        if(inventoryLocation.equals(codePropertiesService.getDetailInvLocationHeadOffice()))
            model.addAttribute(UI_ATTR_APPROVER, true);
        model.addAttribute("expiryExtensionForm", dto);
        model.addAttribute(UI_ATTR_HANDLING_FEE_TYPE, GiftCardHandlingFeeType.values());
        model.addAttribute(UI_ATTR_DIALOG_LABEL, messageSource.getMessage("gc_date_expiry_form_title", 
                null, LocaleContextHolder.getLocale()));
        
        return PATH_GC_DATE_EXPIRY_CREATE;
    }
    
    @RequestMapping(value="/save", produces = "application/json; charset=utf-8", method = RequestMethod.POST)
    @ResponseBody
    public ControllerResponse saveRequest(
            @ModelAttribute("expiryExtensionForm") GiftCardExpiryExtensionDto dto, 
            BindingResult bindingResult) {
        
        ControllerResponse response = new ControllerResponse();
        response.setSuccess(true);
        
        if(bindingResult.hasErrors()) {
            response.setSuccess(false);
        } else {
            try {
                logger.debug("[Method Call from GiftCardExpiryController] giftCardExpiryExtensionService.saveExpiryExtensionRequest()");
                giftCardExpiryExtensionService.saveExpiryExtensionRequest(dto);
            } catch (MessageSourceResolvableException e) {
                logger.debug(e.getMessage());
                response.setSuccess(false);
                response.setResult(Lists.newArrayList(messageSource.getMessage(e, LocaleContextHolder.getLocale())));
            }
        }
        
        return response;
    }
    
    @RequestMapping(value="/approve/{extendNo}", produces = "application/json; charset=utf-8", method = RequestMethod.POST)
    @ResponseBody
    public ControllerResponse approveRequest(
            @PathVariable String extendNo,
            @ModelAttribute("expiryExtensionForm") GiftCardExpiryExtensionDto dto, 
            BindingResult bindingResult) {
        
        ControllerResponse response = new ControllerResponse();
        response.setSuccess(true);
        
        if(bindingResult.hasErrors()) {
            response.setSuccess(false);
        } else {
            logger.debug("[Method Call from GiftCardExpiryController] giftCardExpiryExtensionService.approveRequest()");
            giftCardExpiryExtensionService.approveRequest(extendNo);
        }
        
        return response;
    }
    
    @RequestMapping(value="/reject/{extendNo}", produces = "application/json; charset=utf-8", method = RequestMethod.POST)
    @ResponseBody
    public ControllerResponse rejectRequest(
            @PathVariable String extendNo,
            @ModelAttribute("GiftCardExpiryExtensionDto") GiftCardExpiryExtensionDto dto, 
            BindingResult bindingResult) {
        
        ControllerResponse response = new ControllerResponse();
        response.setSuccess(true);
        
        if(bindingResult.hasErrors()) {
            response.setSuccess(false);
        } else {
            logger.debug("[Method Call from GiftCardExpiryController] giftCardExpiryExtensionService.rejectRequest()");
            giftCardExpiryExtensionService.rejectRequest(extendNo);
        }
        
        return response;
    }
    
    @RequestMapping(value = "/print/{extendNo}")
    public void print(
            @PathVariable String extendNo,
            HttpServletResponse response) throws Exception {
        
        JRProcessor jrProcessor = giftCardDateExpiryDocument.createJrProcessor(extendNo);
        renderReport(response, jrProcessor);
    }
}
