$(document).ready(function() {
  var list = $("#list_transactions");

  var headers = {
    cardNo: list.data("hdrCardno"),
    date: list.data("hdrDate"),
    currAmount: list.data("hdrCurramount"),
    txnAmount: list.data("hdrTxnamount"),
    prevBal: list.data("hdrPrevbal"),
    txnType: list.data("hdrTxntype")
  };

  var columnHeaders = [
    headers['date'],
    headers['cardNo'],
    headers['txnType'],
    headers['prevBal'],
    headers['txnAmount'],
    headers['currAmount']
  ];

  var modelFields = [
    {name : 'transactionDateTime', sortable: true},
    {name : 'cardNo', sortable: true},
    {name : 'transactionType'},
    {name : 'previousBalance', fieldCellRenderer : function(data, type, row) {
      return commaSeparateNumber(row.previousBalance);
    }},
    {name : 'transactionAmount', fieldCellRenderer : function(data, type, row) {
      return commaSeparateNumber(row.transactionAmount);
    }},
    {name : 'currentAmount', fieldCellRenderer : function(data, type, row) {
      return commaSeparateNumber(row.currentAmount);
    }}


  ]

  list.ajaxDataTable({
    'autoload' : false,
    'ajaxSource' : list.data("url"),
    'columnHeaders' : columnHeaders,
    'modelFields' : modelFields
  });

  var searchDto = new Object();
  searchDto.cardNo = $("#cardNo").val();
  searchDto.duration = $("#transactionDuration").val();
  list.ajaxDataTable('search', searchDto);

  $("#transactionDuration").change(function() {
    searchDto.cardNo = $("#cardNo").val();
    searchDto.duration = $(this).val();
    list.ajaxDataTable('search', searchDto);
  });

  function commaSeparateNumber(val) {
    while (/(\d+)(\d{3})/.test(val.toString())){
      val = val.toString().replace(/(\d+)(\d{3})/, '$1'+','+'$2');
    }
    return val;
  }
});
