package com.transretail.crm.web.async;

import javax.naming.NamingException;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.DisposableBean;
import org.springframework.beans.factory.FactoryBean;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.core.task.AsyncTaskExecutor;
import org.springframework.scheduling.commonj.WorkManagerTaskExecutor;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;

/**
 * AsyncTaskExecutor factory that would create WorkManagerTaskExecutor if {@link AsyncTaskExecutorFactoryBean#workManagerName} and jndi object that corresponds to the name is found.
 * Else, {@link org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor} is created
 *
 * @author Allan G. Ramirez (agramirez@exist.com)
 */
public class AsyncTaskExecutorFactoryBean implements FactoryBean<AsyncTaskExecutor>, InitializingBean, DisposableBean {
    private static final Logger _LOG = LoggerFactory.getLogger(AsyncTaskExecutorFactoryBean.class);
    private AsyncTaskExecutor asyncTaskExecutor = null;
    private String workManagerName = null;
    private int corePoolSize = 5;
    private int maxPoolSize = 10;
    private int queueCapacity = 25;

    /**
     * Set the JNDI name of the CommonJ WorkManager.
     */
    public void setWorkManagerName(String workManagerName) {
        this.workManagerName = workManagerName;
    }

    public void setCorePoolSize(int corePoolSize) {
        this.corePoolSize = corePoolSize;
    }

    public void setMaxPoolSize(int maxPoolSize) {
        this.maxPoolSize = maxPoolSize;
    }

    public void setQueueCapacity(int queueCapacity) {
        this.queueCapacity = queueCapacity;
    }

    @Override
    public void afterPropertiesSet() throws Exception {
        if (StringUtils.isNotBlank(workManagerName)) {
            WorkManagerTaskExecutor workManagerTaskExecutor = new WorkManagerTaskExecutor();
            workManagerTaskExecutor.setWorkManagerName(workManagerName);
            try {
                workManagerTaskExecutor.afterPropertiesSet();
                asyncTaskExecutor = workManagerTaskExecutor;
            } catch (NamingException e) {
                _LOG.warn("Failed to resolve jndi name {}. " +
                    "Will fallback to org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor", workManagerName);
            }
        }
        if (asyncTaskExecutor == null) {
            ThreadPoolTaskExecutor threadPoolTaskExecutor = new ThreadPoolTaskExecutor();
            threadPoolTaskExecutor.setCorePoolSize(corePoolSize);
            threadPoolTaskExecutor.setMaxPoolSize(maxPoolSize);
            threadPoolTaskExecutor.setQueueCapacity(queueCapacity);

            threadPoolTaskExecutor.afterPropertiesSet();
            asyncTaskExecutor = threadPoolTaskExecutor;
        }
    }

    @Override
    public void destroy() throws Exception {
        if (asyncTaskExecutor != null && asyncTaskExecutor instanceof ThreadPoolTaskExecutor) {
            ((ThreadPoolTaskExecutor) asyncTaskExecutor).shutdown();
        }
    }

    @Override
    public AsyncTaskExecutor getObject() throws Exception {
        return asyncTaskExecutor;
    }

    @Override
    public Class<?> getObjectType() {
        return AsyncTaskExecutor.class;
    }

    @Override
    public boolean isSingleton() {
        return true;
    }
}
