package com.transretail.crm.web.controller;

import com.google.common.base.Function;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.Lists;
import com.transretail.crm.common.reporting.exception.ReportException;
import com.transretail.crm.common.service.dto.request.PageSortDto;
import com.transretail.crm.common.service.dto.response.ResultList;
import com.transretail.crm.core.entity.lookup.Store;
import com.transretail.crm.core.service.StoreService;
import com.transretail.crm.giftcard.dto.*;
import com.transretail.crm.giftcard.entity.*;
import com.transretail.crm.giftcard.service.*;
import com.transretail.crm.web.controller.util.ControllerResponse;

import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.annotation.*;

/**
 * @author Monte Cillo Co (mco@exist.com)
 */
@Controller
@RequestMapping("/giftcard/service-request")
public class GiftCardServiceRequestController extends AbstractReportsController {

    @Autowired
    private GiftCardInventoryService inventoryService;
    @Autowired
    private MessageSource messageSource;
    @Autowired
    private StoreService storeService;

    @Autowired
    private GiftCardServiceRequestService cardServiceRequestService;

    @RequestMapping
    public String main(Model model) {
        model.addAttribute("giftCardServiceRequestForm", new GiftCardServiceRequestDto());
        model.addAttribute("fileNewRequestAction", "/giftcard/service-request/file");
        return "service-request/main";
    }

    @ModelAttribute("stores")
    public List<Store> stores() {
        return ImmutableList.copyOf(storeService.getAllStores());
    }

    @RequestMapping(value = "/find/{cardNo}", produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public ControllerResponse findGiftCardByCardNo(@PathVariable String cardNo) {
        ControllerResponse response = new ControllerResponse();
        response.setSuccess(true);

        try {
            response.setResult(cardServiceRequestService.retrieveGiftCardInfoByCardNo(cardNo));
        } catch (Exception e) {
            response.setResult(e.getMessage());
            response.setSuccess(false);
        }

        return response;
    }

    @RequestMapping(value = "/activate/{cardNo}", produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public ControllerResponse activate(@PathVariable String cardNo) {
        ControllerResponse response = new ControllerResponse();
        response.setSuccess(true);
        response.setResult(cardServiceRequestService.enabledDisableGiftCard(cardNo));
        return response;
    }

    @RequestMapping(value = "/file", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public ControllerResponse fileNewServiceRequest(
            @ModelAttribute(value = "giftCardServiceRequestForm")
            @Valid GiftCardServiceRequestDto serviceRequest,
            BindingResult result, HttpServletRequest request) {

        ControllerResponse response = new ControllerResponse();
        response.setSuccess(true);

        if (result.hasErrors()) {
            response.setResult(Lists.transform(result.getAllErrors(), new Function<ObjectError, String>() {

                @Override
                public String apply(ObjectError input) {
                    return input.getDefaultMessage();
                }
            }));
            response.setSuccess(false);
            return response;
        }

        GiftCardInventory giftCardInventory = inventoryService.getGiftCardByBarcode(serviceRequest.getGiftcardNo());
        Store storeFiledIn = storeService.getStoreByCode(serviceRequest.getFiledIn());
        cardServiceRequestService.fileNewServiceRequest(giftCardInventory, storeFiledIn, serviceRequest.getDetails());

        return response;
    }

    // TODO: rename path to /list/service-request/{cardNo}
    @RequestMapping(value = "/list/{cardNo}", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public ResultList<GiftCardServiceRequestDto> list(@PathVariable String cardNo, @RequestBody PageSortDto pageSort) {
        return cardServiceRequestService.findAllServiceRequestOf(cardNo, pageSort);
    }

    @RequestMapping(value = "/list/transactions/{cardNo}", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public ResultList<GiftCardTransactionDto> listTransaction(@PathVariable String cardNo, @RequestBody PageSortDto pageSort) {
        return cardServiceRequestService.findAllTransactionOf(cardNo, pageSort);
    }

    @RequestMapping(value = "/report/print/pdf", method = RequestMethod.GET)
    public void printServiceRequest(@ModelAttribute("filters") ServiceRequestReportFilter filter,
                                    HttpServletResponse response) throws ReportException {
        renderReport(response, cardServiceRequestService.printServiceRequest(filter));
    }

    @RequestMapping(value = "/report/validate", produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public ControllerResponse validateReportFilters(
            @ModelAttribute("filters") @Valid ServiceRequestReportFilter filter,
            BindingResult bindingResult, HttpServletResponse response) {

        ControllerResponse controllerResponse = new ControllerResponse();
        controllerResponse.setSuccess(true);

        if (bindingResult.hasErrors()) {
            controllerResponse.setSuccess(false);
            controllerResponse.setResult(bindingResult.getAllErrors());
        }

        return controllerResponse;
    }

    @RequestMapping("/print")
    public String showPrintPage() {
        return "service-request/print";
    }
}
