package com.transretail.crm.core.service.impl;

import com.transretail.crm.core.dto.MemberDto;
import com.transretail.crm.core.dto.MemberMonitorSearchDto;
import com.transretail.crm.core.dto.MemberResultList;
import com.transretail.crm.core.dto.PointsDto;
import com.transretail.crm.core.entity.ApplicationConfig;
import com.transretail.crm.core.entity.EmployeePurchaseTxnModel;
import com.transretail.crm.core.entity.MemberModel;
import com.transretail.crm.core.entity.PointsTxnModel;
import com.transretail.crm.core.entity.embeddable.CustomerProfile;
import com.transretail.crm.core.entity.enums.*;
import com.transretail.crm.core.entity.lookup.LookupDetail;
import com.transretail.crm.core.entity.lookup.LookupHeader;
import com.transretail.crm.core.repo.*;
import com.transretail.crm.core.service.MemberMonitorService;
import com.transretail.crm.core.service.PointsTxnManagerService;
import org.joda.time.DateTime;
import org.joda.time.LocalDate;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Configurable;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Configurable
@Transactional
@ContextConfiguration(locations = "classpath*:/META-INF/spring/applicationContext*.xml")
@RunWith(SpringJUnit4ClassRunner.class)
public class MemberMonitorTest {
	@Autowired
	private PointsTxnManagerService pointsManagerService;
	@Autowired
	private MemberRepo memberRepo;
	@Autowired
    private LookupHeaderRepo lookupHeaderRepo;
    @Autowired
    private LookupDetailRepo lookupDetailRepo;
    @Autowired
    private PointsTxnModelRepo pointsTxnModelRepo;
    @Autowired
    private EmployeePurchaseTxnRepo employeePurchaseTxnRepo;
    @Autowired
    private MemberMonitorService memberMonitorService;
    @Autowired
    private ApplicationConfigRepo appConfigRepo;
    
    private LookupDetail individual, employee;
    
    @Before
    public void setup() {
    	LookupHeader memberType = new LookupHeader("MEM007");
		memberType = lookupHeaderRepo.save(memberType);
		individual = new LookupDetail("MTYP001", "individual", memberType, Status.ACTIVE);
		employee = new LookupDetail("MTYP002", "employee", memberType, Status.ACTIVE);
		individual = lookupDetailRepo.save(individual);
		employee = lookupDetailRepo.save(employee);
    	
    	ApplicationConfig config = new ApplicationConfig();
    	config.setKey(AppKey.TXN_COUNT_IND);
    	config.setValue("3");
    	appConfigRepo.save(config);
    	config = new ApplicationConfig();
    	config.setKey(AppKey.TXN_COUNT_EMP);
    	config.setValue("3");
    	config = new ApplicationConfig();
		
		setupMembers();
    }
    
    public void setupMembers() {
    	MemberModel member1 = new MemberModel();
		member1.setMemberType(individual);
		member1.setAccountId("monitored1");
		member1.setContact("1000");
		member1.setPin("1000");
		member1.setUsername("monitored1");
		member1.setEnabled(true);
		member1.setAccountStatus(MemberStatus.ACTIVE);
        CustomerProfile customerProfile = new CustomerProfile();
        customerProfile.setBirthdate(new Date());
        member1.setCustomerProfile(customerProfile);
		member1 = memberRepo.saveAndFlush(member1);
		
		MemberModel member2 = new MemberModel();
		member2.setMemberType(individual);
		member2.setAccountId("monitored2");
		member2.setContact("1001");
		member2.setPin("1001");
		member2.setUsername("monitored2");
		member2.setEnabled(true);
		member2.setAccountStatus(MemberStatus.ACTIVE);
        customerProfile = new CustomerProfile();
        customerProfile.setBirthdate(new Date());
        member2.setCustomerProfile(customerProfile);
		member2 = memberRepo.saveAndFlush(member2);
		
		MemberModel member3 = new MemberModel();
		member3.setMemberType(individual);
		member3.setAccountId("monitored3");
		member3.setContact("1003");
		member3.setPin("1003");
		member3.setUsername("monitored3");
		member3.setEnabled(true);
		member3.setAccountStatus(MemberStatus.ACTIVE);
        customerProfile = new CustomerProfile();
        customerProfile.setBirthdate(new Date());
        member3.setCustomerProfile(customerProfile);
		member3 = memberRepo.saveAndFlush(member3);
		
		MemberModel member4 = new MemberModel();
		member4.setMemberType(employee);
		member4.setAccountId("monitored4");
		member4.setContact("1004");
		member4.setPin("1004");
		member4.setUsername("monitored4");
		member4.setEnabled(true);
		member4.setAccountStatus(MemberStatus.ACTIVE);
        customerProfile = new CustomerProfile();
        customerProfile.setBirthdate(new Date());
        member4.setCustomerProfile(customerProfile);
		member4 = memberRepo.saveAndFlush(member4);
		
		MemberModel member5 = new MemberModel();
		member5.setMemberType(employee);
		member5.setAccountId("monitored5");
		member5.setContact("1005");
		member5.setPin("1005");
		member5.setUsername("monitored5");
		member5.setEnabled(true);
		member5.setAccountStatus(MemberStatus.ACTIVE);
        customerProfile = new CustomerProfile();
        customerProfile.setBirthdate(new Date());
        member5.setCustomerProfile(customerProfile);
		member5 = memberRepo.saveAndFlush(member5);
		
		DateTime today = DateTime.now();
		DateTime yesterday = LocalDate.now().minusDays(1).toDateTimeAtStartOfDay().plusHours(5);
		
		List<PointsTxnModel> points = new ArrayList<PointsTxnModel>();
		for(int i = 0; i < 5; i++) {
			PointsTxnModel points1 = new PointsTxnModel();
			points1.setId("monitoredpoints" + i);
			points1.setTransactionNo("monitoredpoints" + i);
			points1.setMemberModel(member1);
			points1.setTransactionType(PointTxnType.EARN);
			points1.setTransactionDateTime(today.toDate());
			points1.setTransactionContributer(member1);
			points.add(points1);
		}
		points = pointsTxnModelRepo.save(points);
		
		points = new ArrayList<PointsTxnModel>();
		for(int i = 0; i < 3; i++) {
			PointsTxnModel points1 = new PointsTxnModel();
			points1.setId("monitoredpoints1" + i);
			points1.setTransactionNo("monitoredpoints1" + i);
			points1.setMemberModel(member2);
			points1.setTransactionType(PointTxnType.EARN);
			points1.setTransactionDateTime(today.toDate());
			points1.setTransactionContributer(member2);
			points.add(points1);
		}
		for(int i = 0; i < 2; i++) {
			PointsTxnModel points1 = new PointsTxnModel();
			points1.setId("monitoredpoints2" + i);
			points1.setTransactionNo("monitoredpoints2" + i);
			points1.setMemberModel(member2);
			points1.setTransactionType(PointTxnType.EARN);
			points1.setTransactionDateTime(yesterday.toDate());
			points1.setTransactionContributer(member2);
			points.add(points1);
		}
		points = pointsTxnModelRepo.save(points);
		
		points = new ArrayList<PointsTxnModel>();
		for(int i = 0; i < 5; i++) {
			PointsTxnModel points1 = new PointsTxnModel();
			points1.setId("monitoredpoints3" + i);
			points1.setTransactionNo("monitoredpoints3" + i);
			points1.setMemberModel(member3);
			points1.setTransactionType(PointTxnType.REDEEM);
			points1.setTransactionDateTime(today.toDate());
			points1.setTransactionContributer(member3);
			points.add(points1);
		}
		for(int i = 0; i < 5; i++) {
			PointsTxnModel points1 = new PointsTxnModel();
			points1.setId("monitoredpoints4" + i);
			points1.setMemberModel(member3);
			points1.setTransactionType(PointTxnType.EARN);
			points1.setTransactionDateTime(yesterday.toDate());
			points1.setTransactionContributer(member3);
			points.add(points1);
		}
		points = pointsTxnModelRepo.save(points);
		
		points = new ArrayList<PointsTxnModel>();
		List<EmployeePurchaseTxnModel> purchases = new ArrayList<EmployeePurchaseTxnModel>();
		for(int i = 0; i < 5; i++) {
			PointsTxnModel points1 = new PointsTxnModel();
			points1.setId("monitoredpoints5" + i);
			points1.setTransactionNo("monitoredpoints5" + i);
			points1.setMemberModel(member4);
			points1.setTransactionType(PointTxnType.EARN);
			points1.setTransactionDateTime(today.toDate());
			points1.setTransactionContributer(member4);
			points.add(points1);
			
			EmployeePurchaseTxnModel purchase = new EmployeePurchaseTxnModel();
			purchase.setId("monitoredpurchase5" + i);
			purchase.setTransactionNo("monitoredpurchase5" + i);
			purchase.setMemberModel(member4);
			purchase.setTransactionType(VoucherTransactionType.PURCHASE);
			purchase.setTransactionDateTime(today.toDate());
			purchases.add(purchase);
		}
		points = pointsTxnModelRepo.save(points);
		purchases = employeePurchaseTxnRepo.save(purchases);
		
		points = new ArrayList<PointsTxnModel>();
		purchases = new ArrayList<EmployeePurchaseTxnModel>();
		for(int i = 0; i < 3; i++) {
			PointsTxnModel points1 = new PointsTxnModel();
			points1.setId("monitoredpoints6" + i);
			points1.setTransactionNo("monitoredpoints6" + i);
			points1.setMemberModel(member5);
			points1.setTransactionType(PointTxnType.EARN);
			points1.setTransactionDateTime(today.toDate());
			points1.setTransactionContributer(member5);
			points.add(points1);
			
			EmployeePurchaseTxnModel purchase = new EmployeePurchaseTxnModel();
			purchase.setId("monitoredpurchase6" + i);
			purchase.setTransactionNo("monitoredpurchase6" + i);
			purchase.setMemberModel(member5);
			purchase.setTransactionType(VoucherTransactionType.PURCHASE);
			purchase.setTransactionDateTime(today.toDate());
			purchases.add(purchase);
		}
		for(int i = 0; i < 2; i++) {
			PointsTxnModel points1 = new PointsTxnModel();
			points1.setId("monitoredpoints7" + i);
			points1.setTransactionNo("monitoredpoints7" + i);
			points1.setMemberModel(member5);
			points1.setTransactionType(PointTxnType.EARN);
			points1.setTransactionDateTime(yesterday.toDate());
			points1.setTransactionContributer(member5);
			points.add(points1);
			
			EmployeePurchaseTxnModel purchase = new EmployeePurchaseTxnModel();
			purchase.setId("monitoredpurchase7" + i);
			purchase.setTransactionNo("monitoredpurchase7" + i);
			purchase.setMemberModel(member5);
			purchase.setTransactionType(VoucherTransactionType.PURCHASE);
			purchase.setTransactionDateTime(yesterday.toDate());
			purchases.add(purchase);
		}
		points = pointsTxnModelRepo.save(points);
		purchases = employeePurchaseTxnRepo.save(purchases);
    }
    
    @Test
	public void testGetMonitoredMembers() {
    	memberMonitorService.setMonitoredTransactionCountIndividual(3);
		MemberResultList monitoredMembers = memberMonitorService.getMonitoredMembers(LocalDate.now());
		for(MemberDto member : monitoredMembers.getResults()) {
			System.out.println(member.getAccountId() + " " + member.getTransactionCount());
		}
		Assert.assertEquals(2, monitoredMembers.getTotalElements());
		
		memberMonitorService.setMonitoredTransactionCountIndividual(5);
		monitoredMembers = memberMonitorService.getMonitoredMembers(LocalDate.now());
		for(MemberDto member : monitoredMembers.getResults()) {
			System.out.println(member.getAccountId() + " " + member.getTransactionCount());
		}
		Assert.assertEquals(1, monitoredMembers.getTotalElements());
		
		MemberMonitorSearchDto dto = new MemberMonitorSearchDto();
		dto.setDate(LocalDate.now());
		dto.setTransactionCount(3);
		monitoredMembers = memberMonitorService.getMonitoredMembers(dto);
		for(MemberDto member : monitoredMembers.getResults()) {
			System.out.println(member.getAccountId() + " " + member.getTransactionCount());
		}
		Assert.assertEquals(2, monitoredMembers.getTotalElements());
		
		dto.setTransactionCount(5);
		monitoredMembers = memberMonitorService.getMonitoredMembers(dto);
		for(MemberDto member : monitoredMembers.getResults()) {
			System.out.println(member.getAccountId() + " " + member.getTransactionCount());
		}
		Assert.assertEquals(1, monitoredMembers.getTotalElements());
	}
    
    @Test
    public void testGetMonitoredEmployees() {
    	memberMonitorService.setMonitoredTransactionCountEmployee(3);
		MemberResultList monitoredMembers = memberMonitorService.getMonitoredEmployees(LocalDate.now());
		for(MemberDto member : monitoredMembers.getResults()) {
			System.out.println(member.getAccountId() + " " + member.getTransactionCount());
		}
		Assert.assertEquals(2, monitoredMembers.getTotalElements());
		
		memberMonitorService.setMonitoredTransactionCountEmployee(5);
		monitoredMembers = memberMonitorService.getMonitoredEmployees(LocalDate.now());
		for(MemberDto member : monitoredMembers.getResults()) {
			System.out.println(member.getAccountId() + " " + member.getTransactionCount());
		}
		Assert.assertEquals(1, monitoredMembers.getTotalElements());
		
		MemberMonitorSearchDto dto = new MemberMonitorSearchDto();
		dto.setDate(LocalDate.now());
		dto.setTransactionCount(3);
		monitoredMembers = memberMonitorService.getMonitoredEmployees(dto);
		for(MemberDto member : monitoredMembers.getResults()) {
			System.out.println(member.getAccountId() + " " + member.getTransactionCount());
		}
		Assert.assertEquals(2, monitoredMembers.getTotalElements());
		
		dto.setTransactionCount(5);
		monitoredMembers = memberMonitorService.getMonitoredEmployees(dto);
		for(MemberDto member : monitoredMembers.getResults()) {
			System.out.println(member.getAccountId() + " " + member.getTransactionCount());
		}
		Assert.assertEquals(1, monitoredMembers.getTotalElements());
    }
    
    @Test
    public void testGetAllMonitoredMembers() {
    	memberMonitorService.setMonitoredTransactionCountEmployee(3);
    	memberMonitorService.setMonitoredTransactionCountIndividual(3);
    	MemberResultList monitoredMembers = memberMonitorService.getAllMonitoredMembers(LocalDate.now());
    	Assert.assertEquals(4, monitoredMembers.getTotalElements());
    	
    	memberMonitorService.setMonitoredTransactionCountEmployee(5);
    	memberMonitorService.setMonitoredTransactionCountIndividual(5);
    	monitoredMembers = memberMonitorService.getAllMonitoredMembers(LocalDate.now());
    	Assert.assertEquals(2, monitoredMembers.getTotalElements());
    	
    	memberMonitorService.setMonitoredTransactionCountEmployee(3);
    	memberMonitorService.setMonitoredTransactionCountIndividual(5);
    	monitoredMembers = memberMonitorService.getAllMonitoredMembers(LocalDate.now());
    	Assert.assertEquals(3, monitoredMembers.getTotalElements());
    	
    	memberMonitorService.setMonitoredTransactionCountEmployee(5);
    	memberMonitorService.setMonitoredTransactionCountIndividual(3);
    	monitoredMembers = memberMonitorService.getAllMonitoredMembers(LocalDate.now());
    	Assert.assertEquals(3, monitoredMembers.getTotalElements());
    }
    
    @Test
    public void testGetPointsTransactionsToday() {
    	List<PointsDto> pointsDetails = pointsManagerService.getTransactionsToday("monitored1");
		Assert.assertEquals(5, pointsDetails.size());
		
		pointsDetails = pointsManagerService.getTransactionsToday("monitored2");
		Assert.assertEquals(3, pointsDetails.size());
		
		pointsDetails = pointsManagerService.getTransactionsToday("monitored3");
		Assert.assertEquals(5, pointsDetails.size());
		
		pointsDetails = pointsManagerService.getTransactionsToday("monitored4");
		Assert.assertEquals(5, pointsDetails.size());
    }
    
    @Test
    public void testCountTransactionsToday() {
    	memberMonitorService.setMonitoredTransactionCountEmployee(3);
    	memberMonitorService.setMonitoredTransactionCountIndividual(5);
    	
    	Assert.assertTrue(memberMonitorService.isMonitored("monitored1", "MTYP001"));
    	System.out.println("TEST: " + memberMonitorService.isMonitored("monitored5", "MTYP002"));
    	Assert.assertFalse(memberMonitorService.isMonitored("monitored2", "MTYP001"));
    	Assert.assertTrue(memberMonitorService.isMonitored("monitored5", "MTYP002"));
    }
}
