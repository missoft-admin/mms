package com.transretail.crm.web.controller.admin;

import com.google.common.collect.Lists;
import com.transretail.crm.core.dto.CrmInStoreDto;
import com.transretail.crm.core.dto.CrmInStoreResultList;
import com.transretail.crm.core.dto.CrmInStoreSearchForm;
import com.transretail.crm.core.entity.CrmInStore;
import com.transretail.crm.core.entity.enums.Status;
import com.transretail.crm.core.service.CrmInStoreService;
import com.transretail.crm.web.controller.util.ControllerResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.servlet.ModelAndView;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Map;

/**
 * @author Allan G. Ramirez (agramirez@exist.com)
 */
@Controller
@RequestMapping("/crmproxyinfo")
public class CrmInStoreController {
    private static final Logger _LOG = LoggerFactory.getLogger(CrmInStoreController.class);
    private static final String POS_API_INFO = "/posapi/info";
    @Autowired
    private MessageSource messageSource;

    @Autowired
    private CrmInStoreService service;

    @Autowired
    private RestTemplate restTemplate;

    @RequestMapping(value = "/view", method = RequestMethod.GET)
    public ModelAndView showViewCrmInStorePage() {
        return new ModelAndView("crminstore/view");
    }

    @RequestMapping(value = "/search", method = RequestMethod.POST)
    @ResponseBody
    public CrmInStoreResultList search(@RequestBody CrmInStoreSearchForm searchForm) {
        return service.getCrmInStores(searchForm);
    }

    @RequestMapping(value = "/save", method = RequestMethod.POST)
    @ResponseBody
    public ControllerResponse save(@Valid @RequestBody CrmInStore dto) {
        ControllerResponse response = new ControllerResponse();
        response.setSuccess(true);
        List<String> errorMessages = Lists.newArrayList();

        Locale locale = LocaleContextHolder.getLocale();

        if (!dto.getId().equalsIgnoreCase(dto.getPrevId()) && service.isCodeExists(dto.getId())) {
            response.setSuccess(false);
            errorMessages.add(messageSource.getMessage("crm.proxy.code.exists", null, locale));
        }

        if (service.isNameExists(dto.getPrevId(), dto.getName())) {
            response.setSuccess(false);
            errorMessages.add(messageSource.getMessage("crm.instore.storename.exists", null, locale));
        }
        if (service.isIpExists(dto.getPrevId(), dto.getIp())) {
            response.setSuccess(false);
            errorMessages.add(messageSource.getMessage("crm.instore.ip.exists", null, locale));
        }

        if (response.isSuccess()) {
            response.setResult(service.save(dto));
            response.setMessage(messageSource.getMessage("crm.instore.save.sucessful", new String[]{dto.getName()}, locale));
        } else {
            response.setResult(errorMessages);
        }

        return response;
    }

    @RequestMapping(value = "/{crmInStoreId}", method = RequestMethod.GET)
    @ResponseBody
    public CrmInStoreDto getCrmInStoreDto(@PathVariable("crmInStoreId") String crmInStoreId) {
        CrmInStoreDto dto = service.getCrmInStoreDto(crmInStoreId);
        String url = dto.getCrmProxyWsUrl() + POS_API_INFO;
        try {
            Map<String, String> info = restTemplate.getForObject(url, Map.class);
            dto.setStatus(Status.ACTIVE);
            dto.setVersion(info.get("project.version"));
            dto.setShaId(info.get("git.commit.id.abbrev"));
            dto.setLastUpdate(info.get("git.commit.time"));
        } catch (RestClientException e) {
            String message = e.getRootCause() != null ? e.getRootCause().getMessage() : e.getMessage();
            _LOG.info("[CRM IN STORE] Failed to invoke {}. Reason: {}", url, message);
            dto.setStatus(Status.INACTIVE);
        }
        return dto;
    }

    @ExceptionHandler(MethodArgumentNotValidException.class)
    @ResponseBody
    public ControllerResponse handleMethodArgumentNotValidException(MethodArgumentNotValidException error) {
        ControllerResponse response = new ControllerResponse();
        BindingResult result = error.getBindingResult();
        List<String> errors = new ArrayList<String>();
        for (ObjectError oe : result.getAllErrors()) {
            errors.add(messageSource.getMessage(oe, LocaleContextHolder.getLocale()));
        }
        response.setResult(errors);
        return response;
    }
}
