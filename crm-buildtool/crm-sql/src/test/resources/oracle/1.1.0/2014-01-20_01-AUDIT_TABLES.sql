    create table CRM_REVINFO (
        id number(19,0) not null,
        revisionDate timestamp,
        primary key (id)
    );

    create table CRM_REF_LOOKUP_DTL_AUD (
        CODE varchar2(15 char) not null,
        REV number(10,0) not null,
        REVTYPE number(3,0),
        DESCRIPTION varchar2(100 char),
        STATUS varchar2(8 char),
        HDR varchar2(15 char),
        primary key (CODE, REV)
    );

    create table CRM_REF_LOOKUP_HDR_AUD (
        CODE varchar2(15 char) not null,
        REV number(10,0) not null,
        REVTYPE number(3,0),
        DESCRIPTION varchar2(100 char),
        primary key (CODE, REV)
    );

    alter table CRM_REF_LOOKUP_DTL_AUD
        add constraint FK_rp1ashv21w0jmye8xth31anmh
        foreign key (REV)
        references CRM_REVINFO;

    alter table CRM_REF_LOOKUP_HDR_AUD
        add constraint FK_p20ujkph8p90guogsg3q9r2c9
        foreign key (REV)
        references CRM_REVINFO;