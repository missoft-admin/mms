<%@include file="/WEB-INF/views/common/taglibs.jsp" %>

<spring:message code="gc.general.discount.label" var="typeName" />

<div class="page-header page-header2">
  <h1>${typeName}</h1>
</div>

<div class="pull-right mb20 clearfix">
<button type="button" id="createDisc" data-url="<c:url value="/giftcard/generaldiscount/update"/>" class="btn btn-primary"><spring:message code="gc.general.discount.create" /></button>
</div>

<div>
	<div id="discount-list"></div>
</div>

<div id="button-container" class="hide">
  <div id="update-button">
    <input type="button" 
    	data-url="<c:url value="/giftcard/generaldiscount/update"/>/%ID%" 
    	class="btn modalBtn icn-edit tiptip pull-left" 
    	title="<spring:message code="label_update" />">
  </div>
</div>

<div class="modal hide groupModal nofly" tabindex="1" role="dialog" 
	aria-hidden="true" id="dialog-box"></div>

<style type="text/css"><!-- .text-right { text-align: right !important; } --> </style>
<link href="<spring:url value="/css/dataTables.css" />" rel="stylesheet">
<script src="<spring:url value="/js/jquery/jquery.dataTables.js" />" ></script>
<script src="<spring:url value="/js/jquery/jquery.dataTables.linkLengthMenu.js" />"></script>
<script src="<spring:url value="/js/jquery/jquery.dataTables.customPaging.js" />"></script>
<script src="<spring:url value="/js/jquery/ajax-datatable.js" />" ></script>
<script src="<spring:url value="/js/common/form2js.js" />" ></script>
<script src="<spring:url value="/js/common/json2.js" />" ></script>
<script src="<spring:url value="/js/jquery/jquery.toObject.js" />" ></script>

<script type="text/javascript">
$(document).ready(function() {
	discountList = $("#discount-list");
	$dialog = $("#dialog-box");

	$dialog.modal({
      show: false
    }).on('hidden.bs.modal', function() {
      $(this).empty();
    });

    var headers = [
		"<spring:message code="gc.general.discount.label.discount" />",
		"<spring:message code="gc.general.discount.label.min.purchase" />",
		"<spring:message code="gc.general.discount.label.max.purchase" />",
		""
    ];

    var modelFields = [
        {name: 'discount', sortable: true, dataType: 'NUMBER'},
        {name: 'minPurchase', aaClassname: 'text-right', sortable: true, dataType: 'NUMBER', fieldCellRenderer : function (data, type, row) {
            return null != row.minPurchase? ( "" + row.minPurchase ).replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1, ") : "";
        }},
        {name: 'maxPurchase', aaClassname: 'text-right', sortable: true, dataType: 'NUMBER', fieldCellRenderer : function (data, type, row) {
            return null != row.maxPurchase? ( "" + row.maxPurchase ).replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1, ") : "";
        }},
        {customCell: function(innerData, sSpecific, json) {
        	if (sSpecific === 'display') {
        	    var btnHtml = $("#update-button").html();

        	    var rg = new RegExp("%ID%", 'g');
        	    btnHtml = btnHtml.replace(rg, json.id);

        	    return btnHtml;
        	}
        	else {
        	    return '';
        	}
        }}
    ];

    discountList.ajaxDataTable({
    	'autoload': true,
        'ajaxSource': "<spring:url value="/giftcard/generaldiscount/search" />",
        'columnHeaders': headers,
        'modelFields': modelFields
    })
    .on('click', '.modalBtn', showModal);
    
    
    $("#createDisc").click(showModal);
});

function showModal(e) {
    e.preventDefault();
    $.get($(this).data("url"), function(data) {
		var prev = "";
		$dialog.modal('show').html(data);
		$dialog.on('hidden.bs.modal', function(e) {
		  console.log(prev);
		  if ($(e.target).attr('id') != 'dialog-box') {
		    $dialog.modal('show').html(data);
		  }
		  else {
		    $dialog.empty();
		  }
		});
    }, "html");
  }

function ajaxSubmit(action, data, fn) {
    $.ajax({
      type: "POST",
      url: action,
      dataType: 'json',
      data: data,
      success: function(inResponse) {
		if (inResponse.success) {
		  fn(inResponse);
		}
		else {
		  errorInfo = "";
		  for (i = 0; i < inResponse.result.length; i++) {
		    errorInfo += "<br>" + (i + 1) + ". "
			    + (inResponse.result[i].code != undefined ?
				    inResponse.result[i].code : inResponse.result[i]);
		  }
		  $("#containerError").html("Please correct following errors: " + errorInfo);
		  $("#contentError").show('slow');
		}
	      },
	      error: function(jqXHR, textStatus, errorThrown) {
				$("#contentError").html("Error: " + errorThrown);
	      }
	    });
 }
</script>