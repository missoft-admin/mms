<%@include file="../common/messages.jsp" %>

<div class="page-header page-header2">
<h1><spring:message code="menutab_giftcard_stockrequest" /></h1>
</div>

<div id="stock_panel">
  <form id="stock_search_form" class="form-horizontal row-fluid well">
    <div class="span6">
      <div class="control-group">
        <label class="control-label" for="q_storeCode"><spring:message code="gc.order.label.storeName"/></label>

        <div class="controls">
          <%--TODO: Create and use vendor search instead of displaying everything in dropdown--%>
          <select id="q_storeCode" name="storeCode">
            <option value="">&nbsp;</option>
            <c:forEach items="${stores.results}" var="store">
              <option value="${store.id}">${store.name}</option>
            </c:forEach>
          </select>
        </div>
      </div>
    </div>
    <div class="span6">
      <div class="control-group">
        <label class="control-label" for="q_giftCardId"><spring:message code="gc.order.label.giftCardName"/></label>

        <div class="controls">
          <%--TODO: Create and use vendor search instead of displaying everything in dropdown--%>
          <select id="q_giftCardId" name="giftCardId">
            <option value="">&nbsp;</option>
            <c:forEach items="${gcs.results}" var="gc">
              <option value="${gc.id}">${gc.name}</option>
            </c:forEach>
          </select>
        </div>
      </div>
    </div>
    <div class="form-cta">
      <button id="search" type="button" class="btn btn-primary"><spring:message code="search"/></button>
      <button id="add" type="button" class="btn"><spring:message code="add"/></button>
    </div>
  </form>
  <div id="successMessages">&nbsp;</div>
  <div class="errorMessages error">&nbsp;</div>
  <div id="stock_list_container">
  </div>
</div>
<div id="addEditDialog" class="modal hide" style="display: none;">
  <div class="modal-body">
    <form id="addUpdateStockRequestForm" class="form-horizontal">
      <div class="errorMessages error">&nbsp;</div>
      <div class="control-group">
        <label class="control-label" for="giftCardId"><spring:message code="gc.order.label.giftCardName"/></label>

        <div class="controls">
          <%--TODO: Create and use vendor search instead of displaying everything in dropdown--%>
          <select id="giftCardId" name="giftCardId">
            <c:forEach items="${gcs.results}" var="gc">
              <option value="${gc.id}">${gc.name}</option>
            </c:forEach>
          </select>
          <input id="giftCardName" name="giftCardName" type="hidden"/>
        </div>
      </div>
      <div class="control-group">
        <spring:message code="gc.order.label.quantity" var="quantity"/>
        <label class="control-label" for="quantityRequested" title="${quantity}">${quantity}</label>

        <div class="controls">
          <input id="quantityRequested" name="quantityRequested" title="${quantity}" type="text">
        </div>
      </div>
      <div class="control-group">
        <label class="control-label" for="storeCode"><spring:message code="gc.order.label.storeName"/></label>

        <div class="controls">
          <%--TODO: Create and use vendor search instead of displaying everything in dropdown--%>
          <select id="storeCode" name="storeCode">
            <c:forEach items="${stores.results}" var="store">
              <option value="${store.id}">${store.name}</option>
            </c:forEach>
          </select>
          <input id="storeName" name="storeName" type="hidden"/>
        </div>
      </div>
    </form>
    <input id="stockRequestId" type="hidden" value=""/>
  </div>
  <div class="modal-footer">
    <spring:message code="save" var="saveLabel"/>
    <button id="save" class="btn" aria-hidden="true">${saveLabel}</button>
    <button id="cancel" class="btn" aria-hidden="true"><spring:message code="cancel"/></button>
  </div>
</div>
<style>
  #addEditDialog {
    /*width: 70%;*/
  }

  #addEditDialog label {
    width: 130px;
    padding: 0;
    text-transform: none;
  }


</style>
<link href="<spring:url value="/css/dataTables.css" />" rel="stylesheet">
<link href="<spring:url value="/styles/temp.css" />" rel="stylesheet">
<script src="<spring:url value="/js/jquery/jquery.dataTables.js" />"></script>
<script src="<spring:url value="/js/jquery/jquery.dataTables.linkLengthMenu.js" />"></script>
<script src="<spring:url value="/js/jquery/jquery.dataTables.customPaging.js" />"></script>
<script src="<spring:url value="/js/jquery/ajax-datatable.js" />"></script>
<script src="<spring:url value="/js/common/form2js.js" />"></script>
<script src="<spring:url value="/js/common/json2.js" />"></script>
<script src="<spring:url value="/js/jquery/jquery.toObject.js" />"></script>
<script>
$( document ).ready( function () {

  if ( jQuery().ajaxDataTable ) {
    var stockRequestList = $( '#stock_list_container' );
    $( '#search' ).click( function () {
      var formDataObject = $( '#stock_search_form' ).toObject( {mode : 'first', skipEmpty : false} );
      var $btnSearch = $( this );
      $btnSearch.prop( 'disabled', true );
      stockRequestList.ajaxDataTable( 'search', formDataObject, function () {
        $btnSearch.prop( 'disabled', false );
      } );
    } );

    var rowIdPrefix = 'stock-';

    var clickableCell = function ( data, type, row ) {
      return '<a class="clickable" href="#" onclick="return false;"><div>' + ( data == null || data === '' ? '&nbsp;' : data) + '</div></a>';
    };

    <spring:message code="submit" var="submitLabel" />
    <spring:message code="edit" var="editLable" />

    stockRequestList.ajaxDataTable( {
      'autoload' : true,
      'ajaxSource' : '<spring:url value="/giftcard/stock/list" />',
      'fnRowCallback' : function ( nRow, aData, iDisplayIndex ) {
        nRow.id = rowIdPrefix + aData.id;
        return nRow;
      },
      'columnHeaders' : [
        '<spring:message code="gc.stock.label.productdesc" />',
        '<spring:message code="gc.stock.label.facevalue" />',
        '<spring:message code="gc.stock.label.quantityrequested" />',
        '<spring:message code="gc.stock.label.store" />',
        '<spring:message code="gc.stock.label.requestedby" />',
        '<spring:message code="gc.stock.label.quantityonstock" />',
        '<spring:message code="gc.stock.label.status" />',
        '<spring:message code="gc.stock.label.action" />'
      ],
      'modelFields' : [
        {name : 'giftCardName', sortable : false, fieldCellRenderer : function ( data, type, row ) {
          var htmlString = data;
          htmlString += '<input type="hidden" class="giftCardId" value="' + row.giftCardId + '" />';
          return htmlString;
        }},
        {name : 'giftCardFaceValue', sortable : false},
        {name : 'quantityRequested', sortable : false, fieldCellRenderer : function ( data, type, row ) {
          var htmlString = data;
          htmlString += '<input type="hidden" class="quantityRequested" value="' + data + '" />';
          return htmlString;
        }},
        {name : 'storeName', sortable : false, fieldCellRenderer : function ( data, type, row ) {
          var htmlString = data;
          htmlString += '<input type="hidden" class="storeCode" value="' + row.storeCode + '" />';
          return htmlString;
        }},
        {name : 'requestedBy', sortable : false},
        {name : 'quantityOnStock', sortable : false},
        {name : 'status', sortable : false},
        {
          customCell : function ( innerData, sSpecific, json ) {
            if ( sSpecific == 'display' ) {
              var htmlString = '';
              if ( 'CREATED' == json.status ) {
                <sec:authorize access="hasAuthority('ROLE_STORE_CASHIER')">
                htmlString += '<a class="edit" href="#" onclick="return false">${editLable}</a>';
                </sec:authorize>
                <sec:authorize access="hasAuthority('ROLE_STORE_MANAGER')">
                htmlString += '<a class="submit" href="#" onclick="return false">${submitLabel}</a>';
                </sec:authorize>
              } else if ( 'SUBMITTED' == json.status ) {
                <sec:authorize access="hasAuthority('ROLE_MERCHANT_SERVICE')">
                htmlString += '<a class="validate" href="#" onclick="return false">Validate</a>';
                htmlString += '<a class="reject" href="#" onclick="return false">Reject</a>';
                </sec:authorize>
              } else if ( 'VALIDATED' == json.status ) {
                <sec:authorize access="hasAuthority('ROLE_MERCHANT_SERVICE_SUPERVISOR')">
                htmlString += '<a class="approve" href="#" onclick="return false">Approve</a>';
                htmlString += '<a class="disapprove" href="#" onclick="return false">Disapprove</a>';
                </sec:authorize>
              } else {

              }
              return htmlString;
            } else {
              return '';
            }
          }
        }
      ]
    } );

    var addUpdateStockRequestForm = $( '#addUpdateStockRequestForm' );
    var $dialog = $( '#addEditDialog' ).modal( { show : false } ) {
      $( '#save' ).val( '${saveLabel}' );
      $( '#giftCardId' ).prop( 'disabled', false );
      $( '#quantityRequested' ).prop( 'disabled', false );
      addUpdateStockRequestForm.formUtil( 'clearInputs' );
      $( ".alert", addUpdateStockRequestForm ).alert( 'close' )
    } );

    $( 'select#giftCardId' ).change(function () {
      $( this ).siblings( '#giftCardName' ).val( $( this ).find( "option:selected" ).text() );
    } ).change();

    $( 'select#storeCode' ).change(function () {
      $( this ).siblings( '#storeName' ).val( $( this ).find( "option:selected" ).text() );
    } ).change();

    // Update
    $( stockRequestList ).on( 'click', 'a.edit', function () {
      var tr = $( this ).parents( 'tr' );
      var trId = tr.attr( 'id' );
      var stockRequestId = trId.substring( rowIdPrefix.length );

      $( '#stockRequestId' ).val( stockRequestId );
      $( '#giftCardId' ).val( $( '.giftCardId', tr ).val() ).change();
      $( '#quantityRequested' ).val( $( '.quantityRequested', tr ).val() );
      $( '#storeCode' ).val( $( '.storeCode', tr ).val() ).change();

      $dialog.modal( 'toggle' );
    } );

    $( stockRequestList ).on( 'click', 'a.submit', function () {
      var tr = $( this ).parents( 'tr' );
      var trId = tr.attr( 'id' );
      var stockRequestId = trId.substring( rowIdPrefix.length );

      $( '#stockRequestId' ).val( stockRequestId );
      $( '#giftCardId' ).val( $( '.giftCardId', tr ).val() ).change().prop( 'disabled', true );
      $( '#quantityRequested' ).val( $( '.quantityRequested', tr ).val() );
      $( '#storeCode' ).val( $( '.storeCode', tr ).val() ).change().prop( 'disabled', true );
      $( '#save' ).val( '${submitLabel}' );
      $dialog.modal( 'toggle' );
    } );

    var ajaxSuccessCallback = function ( displaySuccessCallback ) {
      return function ( data, textStatus, jqXHR ) {
        if ( data.hasOwnProperty( 'errorMessages' ) ) {
          var errorInfo = "";
          for ( var i = 0; i < data.errorMessages.length; i++ ) {
            errorInfo += "<br>" + (i + 1) + ". " + data.errorMessages[i];
          }
          displaySuccessCallback( errorInfo, false )
        } else {
          displaySuccessCallback( data.successMessages, true )
        }
      }
    };

    var ajaxErrorCallback = function ( displayErrorCallback ) {
      return function ( xhr, textStatus, error ) {
        if ( textStatus === 'timeout' ) {
          displayErrorCallback( '<spring:message code="gc.stock.ajaxtimeout" />' );
        }
        else if ( error ) {
          displayErrorCallback( error );
        } else {
          displayErrorCallback( '<spring:message code="gc.stock.ajaxgenericerror" />' );
        }
      };
    };

    $( stockRequestList ).on( 'click', 'a.validate', function () {
      var stockRequestId = $( this ).parents( 'tr' ).attr( 'id' ).substring( rowIdPrefix.length );
      $.ajax( {
        "type" : "POST",
        "url" : '<spring:url value="/giftcard/stock/validate/"/>' + stockRequestId,
        "success" : ajaxSuccessCallback( function ( messages, isSuccess ) {
          if ( isSuccess ) {
            $( '#successMessages' ).html( messages );
            // Reload the list
            $( '#search' ).click();
          } else {

            $( 'div.errorMessages', addUpdateStockRequestForm ).html( messages );
          }
        } ),
        "error" : ajaxErrorCallback( function ( messages ) {
          alert( messages );
        } )
      } );
    } );

    $( stockRequestList ).on( 'click', 'a.approve', function () {
      var stockRequestId = $( this ).parents( 'tr' ).attr( 'id' ).substring( rowIdPrefix.length );
      $.ajax( {
        "type" : "POST",
        "url" : '<spring:url value="/giftcard/stock/approve/"/>' + stockRequestId,
        "success" : ajaxSuccessCallback( function ( messages, isSuccess ) {
          if ( isSuccess ) {
            $( '#successMessages' ).html( messages );
            // Reload the list
            $( '#search' ).click();
          } else {

            $( 'div.errorMessages', addUpdateStockRequestForm ).html( messages );
          }
        } ),
        "error" : ajaxErrorCallback( function ( messages ) {
          alert( messages );
        } )
      } );
    } );

    var $rejectDialog = $( '<div id="rejectDialog"></div>' )
            .append( '<div class="errorMessages error"></div><textarea class="comments" rows="10"/><input type="hidden" class="stockRequestId" />' )
            .dialog( {
              title : '<spring:message code="gc.stock.label.reason" />',
              draggable : false,
              autoOpen : false,
              width : 100,
              modal : true,
              buttons : [
                { text : "<spring:message code="button.ok" />", click : function () {
                  var thatDialog = $( this );
                  $.ajax( {
                    "contentType" : 'application/json',
                    headers : { 'Content-Type' : 'application/json' },
                    "type" : "POST",
                    "url" : '<spring:url value="/giftcard/stock/reject/"/>' + $( '.stockRequestId', thatDialog ).val()
                            + '?reason=' + $( '.comments', thatDialog ).val(),
                    "success" : ajaxSuccessCallback( function ( messages, isError ) {
                      if ( isError ) {
                        $( 'div.errorMessages', thatDialog ).html( messages );
                        alert( messages );
                      } else {
                        $( '#successMessages' ).html( messages );
                        thatDialog.modal( 'toggle' );
                        // Reload the list
                        $( '#search' ).click();
                      }
                    } ),
                    "error" : ajaxErrorCallback( function ( messages ) {
                      $( 'div.errorMessages', thatDialog ).html( messages );
                    } )
                  } );
                }},
                {text : "<spring:message code="button.cancel" />", click : function () {
                  $( this ).dialog( "close" );
                }}
              ]
            } );

    $( stockRequestList ).on( 'click', 'a.reject', function () {
      var tr = $( this ).parents( 'tr' );
      var trId = tr.attr( 'id' );
      var stockRequestId = trId.substring( rowIdPrefix.length );

      $( '.stockRequestId', $rejectDialog ).val( stockRequestId );
      $rejectDialog.modal( 'toggle' );
    } );

    var $disapproveDialog = $( '<div id="disapproveDialog"></div>' )
            .append( '<div class="errorMessages error"></div><textarea class="comments" rows="10"/><input type="hidden" class="stockRequestId" />' )
            .dialog( {
              title : '<spring:message code="gc.stock.label.reason" />',
              draggable : false,
              autoOpen : false,
              width : 100,
              modal : true,
              buttons : [
                { text : "<spring:message code="button.ok" />", click : function () {
                  var thatDialog = $( this );
                  $.ajax( {
                    "contentType" : 'application/json',
                    "type" : "POST",
                    "url" : '<spring:url value="/giftcard/stock/disapprove/"/>' + $( '.stockRequestId', thatDialog ).val()
                            + '?reason=' + $( '.comments', thatDialog ).val(),
                    "success" : ajaxSuccessCallback( function ( messages, isSuccess ) {
                      if ( isSuccess ) {
                        $( '#successMessages' ).html( messages );
                        thatDialog.modal( 'toggle' );
                        // Reload the list
                        $( '#search' ).click();
                      } else {
                        $( 'div.errorMessages', thatDialog ).html( messages );
                        alert( messages );
                      }
                    } ),
                    "error" : ajaxErrorCallback( function ( messages ) {
                      $( 'div.errorMessages', thatDialog ).html( messages );
                    } )
                  } );
                }},
                {text : "<spring:message code="button.cancel" />", click : function () {
                  $( this ).dialog( "close" );
                }}
              ]
            } );

    $( stockRequestList ).on( 'click', 'a.disapprove', function () {
      var tr = $( this ).parents( 'tr' );
      var trId = tr.attr( 'id' );
      var stockRequestId = trId.substring( rowIdPrefix.length );

      $( '.stockRequestId', $disapproveDialog ).val( stockRequestId );
      $disapproveDialog.modal( 'toggle' );
    } );

    // Add
    $( '#add' ).click( function () {
      $( '#stockRequestId' ).val( '' );
      $dialog.modal( 'toggle' );
    } );

    $( '#cancel' ).click( function () {
      $dialog.modal( 'toggle' );
    } );

    $( '#save' ).click( function () {
      var ajaxUrl = '';
      var selectedStockId = $( '#stockRequestId' ).val();
      if ( selectedStockId ) {
        if ( $( this ).val() == '${submit}' ) {
          var quantityRequested = $( '#quantityRequested' ).val();
          ajaxUrl = '<spring:url value="/giftcard/stock/submit/"/>' + selectedStockId + "/" + quantityRequested;
        } else {
          ajaxUrl = '<spring:url value="/giftcard/stock/update/"/>' + selectedStockId;
        }
      } else {
        // Add
        ajaxUrl = '<spring:url value="/giftcard/stock/add"/>';
      }
      $.ajax( {
        "contentType" : 'application/json',
        "type" : 'POST',
        "url" : ajaxUrl,
        "data" : JSON.stringify( addUpdateStockRequestForm.toObject( {mode : 'first', skipEmpty : false} ) ),
        "success" : ajaxSuccessCallback( function ( messages, isSuccess ) {
          if ( isSuccess ) {
            $( '#successMessages' ).html( messages );
            dialog.modal( 'toggle' );
            // Reload the list
            $( '#search' ).click();
          } else {
            $( 'div.errorMessages', addUpdateStockRequestForm ).html( messages );
          }
        } ),
        "error" : ajaxErrorCallback( function ( messages ) {
          $( 'div.errorMessages', addUpdateStockRequestForm ).html( messages );
        } )
      } );
    } );
  } else {
    $( '#stock_panel' ).html( '<spring:message code="gc.stock.load.error" />' );
  }

} );
</script>