<%@include file="../../common/taglibs.jsp" %>
<%@ taglib prefix="permission" tagdir="/WEB-INF/tags/permission" %>
<%@ taglib prefix="crm" uri="http://www.carrefour.com/crm/permission/tags" %>

  <style type="text/css">
<!--
  #sales_order_list_container .table tbody tr td:last-child {
      min-width: 146px;
  }
-->
</style>
<sec:authorize ifAnyGranted="ROLE_MERCHANT_SERVICE" var="isMerchant" />
<sec:authorize ifAnyGranted="ROLE_MERCHANT_SERVICE_SUPERVISOR" var="isHead" />
<sec:authorize ifAnyGranted="ROLE_TREASURY" var="isTreasury" />
<sec:authorize ifAnyGranted="ROLE_ACCOUNTING" var="isAccounting" />

<div class="page-header page-header2">
    <h1><spring:message code="sales_order" /></h1>
</div>

  <div>
    <jsp:include page="ordersearch.jsp" />
  </div>



<div class="form-horizontal pull-left mb20" id="printContainer">
  <select name="reportType" class="selectpicker" id="reportType" data-header="Select a Format">
    <c:forEach items="${reportTypes}" var="reportType">
      <option data-url="<c:url value="/giftcard/salesorder/printall?reportType=${reportType.code}" />" value="${reportType.code}"><spring:message code="report_type_${reportType.code}" /></option>
    </c:forEach>
  </select>
  <button class="btn btn-print btn-print-small tiptip" id="printBtn" value="<spring:message code="label_print" />" data-toggle="dropdown"><spring:message code="label_print" /></button>
</div>

<div class="pull-right mb20">
<c:if test="${isHead || isMerchant}">
<crm:permission hasPermissions="GC_REPLACE">
<button type="button" class="btn btn-primary" id="createReplacement"><spring:message code="create_replacement" /></button>
</crm:permission>
<crm:permission hasPermissions="GC_SALES_ORDER">
<button type="button" class="btn btn-primary" id="createPromoOrder"><spring:message code="create_internal_order" /></button>
<button type="button" class="btn btn-primary" id="createOrder"><spring:message code="create_sales_order" /></button>
</crm:permission>
</c:if>
</div>


<div id="sales_order_list_container">
</div>

<div class="modal hide  nofly" id="orderDialog" data-backdrop="static" >
</div>

<jsp:include page="../../common/selectpopup.jsp" />
<jsp:include page="../../common/confirm.jsp" />

  <link href="<spring:url value="/css/dataTables.css" />" rel="stylesheet">
  <script src="<spring:url value="/js/jquery/jquery.dataTables.js" />" ></script>
  <script src="<spring:url value="/js/jquery/jquery.dataTables.linkLengthMenu.js" />"></script>
  <script src="<spring:url value="/js/jquery/jquery.dataTables.customPaging.js" />"></script>
  <script src="<spring:url value="/js/jquery/ajax-datatable.js" />" ></script>
  <script src="<spring:url value="/js/common/form2js.js" />" ></script>
  <script src="<spring:url value="/js/common/json2.js" />" ></script>
  <script src="<spring:url value="/js/jquery/jquery.toObject.js" />" ></script>
  <link href="<c:url value="/styles/temp.css" />" rel="stylesheet" />


  <script src="<spring:url value="/js/jquery/jquery.numberInput.js" />" ></script>
  <script src="<c:url value="/js/bootstrap/bootstrap-datepicker.js"/>" type='text/javascript'><![CDATA[&nbsp; ]]></script>
  <link href="<c:url value="/css/bootstrap/datepicker.css" />" rel="stylesheet" />
  <link href="<c:url value="/styles/temp.css" />" rel="stylesheet" />
  <script src="<spring:url value="/js/jquery/jquery.bindWithDelay.js" />"></script>
  <script src="<spring:url value="/js/jquery/jquery.ajaxQueue.js" />"></script>

<script>
$(document).ready(function() {

	var loggedInUser = "<sec:authentication property="principal.username" />";

  $orderDialog = $("#orderDialog").modal({
    show: false
  }).on('hidden.bs.modal', function() {
    $(this).empty();
  }).css({
    "width": "900px",
  });

  if("${showDialog}" == "true") {
  	$.get("<c:url value="/giftcard/salesorder/form/fromreturn/" />" + "${returnNo}" + "/" + "${byCards}", function(data) {
  		$orderDialog.modal("show").html(data);
  	}, "html");
  }

  $("#createOrder").click(function() {
    $.get("<c:url value="/giftcard/salesorder/form" />", function(data) {
      $orderDialog.modal("show").html(data);
    }, "html");
  });

  $("#createPromoOrder").click(function() {
    $.get("<c:url value="/giftcard/salesorder/form/promoorder" />", function(data) {
      $orderDialog.modal("show").html(data);
    }, "html");
  });

  $("#createReplacement").click(function() {
	    $.get("<c:url value="/giftcard/salesorder/form/replacement" />", function(data) {
	      $orderDialog.modal("show").html(data);
	    }, "html");
	  });


  $("#sales_order_list_container").ajaxDataTable({
    'autoload'  : true,
    'aaSorting' :[[ 1, 'desc' ]],
    'ajaxSource' : "<c:url value="/giftcard/salesorder/search" />",
    'columnHeaders' : [
      "<spring:message code="sales_order_no"/>",
      "<spring:message code="sales_order_lastupdateddate"/>",
      "<spring:message code="sales_order_date"/>",
      "<spring:message code="sales_order_company"/>",
      "<spring:message code="sales_order_store"/>",
      "<spring:message code="sales_order_payment"/>",
      "<spring:message code="sales_order_soamount"/>",
      "<spring:message code="sales_order_type"/>",
      "<spring:message code="sales_order_status"/>",
      ""
    ],
    'modelFields' : [
      {name : 'orderNo', sortable : true},
      {name: 'lastUpdated', sortable: true, dataType: "DATE"},
      {name : 'orderDate', sortable : true, dataType: "DATE"},
      {name : 'customerDesc', sortable : false},
      {name : 'paymentStore',sortable:false},
      {name : 'totalPaymentFmt', sortable : false},
      {name : 'totalFaceAmountFmt', sortable : false},
      {name : 'orderType', sortable : true, fieldCellRenderer : function (data, type, row) {
          return ( row.orderType? row.orderType.replace( new RegExp( "_", "g" ), " " ) : row.orderType );
      }},
      {name : 'status', sortable : true, fieldCellRenderer : function (data, type, row) {
          return (row.status)? row.status.replace( new RegExp( "_", "g" ), " " ) : "";
      }},
      {customCell : function ( innerData, sSpecific, json ) {
        if (sSpecific == 'display') {
          var html = "";
          var isAllocated = json.status == 'ALLOCATED' || json.status == 'FOR_PICKUP' || json.status == 'PAYMENT_APPROVAL'
                            || json.status == 'FOR_ACTIVATION' || json.status == 'SOLD';
          var isSoActiveTxns = isAllocated || json.status == 'PARTIALLY_ALLOCATED';
          if(json.status != 'APPROVED' && !isSoActiveTxns  && (${isHead} || ${isMerchant})) {

            if(json.status == 'DRAFT' || json.status == 'REJECTED')
              html += "<button class=\"updateOrder btn btn-primary pull-left tiptip icn-edit\" data-id=\""+json.id+"\" title=\"<spring:message code="label_update"/>\"></button>";

            if(json.status == 'DRAFT' && (json.orderType == 'B2B_SALES' || json.orderType == 'B2B_ADV_SALES')) {
            	html += "<button class=\"paymentInfo btn btn-primary pull-left tiptip icn-edit\" data-id=\""+json.id+"\" title=\"<spring:message code="sales_order_payment_info"/>\"></button>";
            }


            <sec:authorize ifAnyGranted="ROLE_MERCHANT_SERVICE_SUPERVISOR">
            if(json.status == 'FOR_APPROVAL' && (((json.orderType == 'B2B_SALES' || json.orderType == 'B2B_ADV_SALES') && !json.readOnly) || (json.orderType != 'B2B_SALES' && json.orderType != 'B2B_ADV_SALES') ))
              html += "<button class=\"updateOrder btn btn-primary pull-left tiptip icn-approve\" data-id=\""+json.id+"\" title=\"<spring:message code="label_process"/>\"></button>";
            </sec:authorize>

            if ( json.status == 'CANCELLED') {
            	if(json.createdBy.toLowerCase() == loggedInUser.toLowerCase())
          	  	  html += "<button type=\"button\" class=\"tiptip btn pull-left icn-delete deleteOrder\" data-id=\""+json.id+"\" title=\"<spring:message code="label_delete"/>\"></button>";
              html += "<button class=\"soCancel btn btn-primary pull-left tiptip icn-view\" data-id=\""+json.id+"\" title=\"" + '<spring:message code="label_view"/>' + "\"></button>";
            }

            if(json.status == 'DRAFT' && json.createdBy.toLowerCase() == loggedInUser.toLowerCase()) {
            	html += "<button type=\"button\" class=\"tiptip btn pull-left icn-delete deleteOrder\" data-id=\""+json.id+"\" title=\"<spring:message code="label_delete"/>\"></button>";
            }
          }
          else if (json.status == 'APPROVED' || isSoActiveTxns) {
            var label_allocate = '<spring:message code="gc.so.allocate"/>';
            var class_edit = 'icn-edit';
            if ( isAllocated ) {
              label_allocate = '<spring:message code="label_view"/>';
              class_edit = 'icn-view';
              html += "<button class=\"soAllocate btn btn-primary pull-left tiptip " + class_edit + "\" data-id=\""+json.id+"\" title=\"" + label_allocate + "\"></button>";
            } else if(${isHead} || ${isMerchant}) {
            	html += "<button class=\"soAllocate btn btn-primary pull-left tiptip " + class_edit + "\" data-id=\""+json.id+"\" title=\"" + label_allocate + "\"></button>";
            }


            if(${isHead} || ${isMerchant}) {

                if ( json.status == 'ALLOCATED' ) {
                    html += "<button class=\"soPickup btn btn-primary pull-left tiptip icn-edit\" data-id=\""+json.id+"\" title=\"<spring:message code="gc.so.pickup.printdocs"/>\"></button>";
                }
                else if ( json.status == 'FOR_PICKUP' ) {
                    html += "<button class=\"soHandedOver btn btn-primary pull-left tiptip icn-edit\" data-id=\""+json.id+"\" title=\"<spring:message code="gc.so.pickup.handedover"/>\"></button>";
                }
                else if ( json.status == 'PAYMENT_APPROVAL') {
                    <sec:authorize ifAnyGranted="ROLE_TREASURY" >
                    <crm:permission hasPermissions="GC_SO_APPROVE_PAYMENT">
                        html += "<button class=\"soApprovePayment btn btn-primary pull-left tiptip icn-edit\" data-id=\""+json.id+"\" title=\"<spring:message code="gc.so.paymentapproval"/>\"></button>";
                        </crm:permission>
                    </sec:authorize>
                }
                else if ( json.status == 'FOR_ACTIVATION' && !json.readOnly && ${isHead}) {
                    html += "<button class=\"soActivate btn btn-primary pull-left tiptip icn-edit\" data-id=\""+json.id+"\" title=\"<spring:message code="gc.so.activate"/>\"></button>";
                }

                if ( json.status == 'ALLOCATED' || json.status == 'PARTIALLY_ALLOCATED' || json.status == 'APPROVED' ) {
                    html += "<button class=\"soCancel btn btn-primary pull-left tiptip icn-cancel\" data-id=\""+json.id+"\" title=\"" + '<spring:message code="label_cancel_order"/>' + "\"></button>";
                    html += "<button class=\"soDelivery btn btn-primary pull-left tiptip icn-cancel\" data-id=\""+json.id+"\" title=\"" + '<spring:message code="gc.so.delivery.update"/>' + "\"></button>";
                }
            }

          }

          if((json.status == 'DRAFT' || (json.status == 'FOR_ACTIVATION' && json.readOnly && ${isAccounting})) && json.orderType == 'INTERNAL_ORDER') {
          	html += "<button class=\"deptAlloc btn btn-primary pull-left tiptip icn-edit\" data-id=\""+json.id+"\" title=\"<spring:message code="sales_order_dept_alloc"/>\"></button>";
          }
          html += "<button class=\"soHistory btn btn-primary pull-left tiptip icn-view\" data-id=\""+json.id+"\" title=\"History\"></button>";
          return html;
        } else {
          return "";
        }
      }
      }
    ]
  }).on("click", ".updateOrder", function() {
    $.get("<c:url value="/giftcard/salesorder/form/" />" + $(this).data("id"), function(data) {
      $orderDialog.modal("show").html(data);
    }, "html");
  }).on("click", ".deleteOrder", function() {
    var itemId = $(this).data("id");
    getConfirm('<spring:message code="entity_delete_confirm" />', function(result) {
      if(result) {
        $.post("<c:url value="/giftcard/salesorder/" />" + itemId, {_method: 'DELETE'}, function(data) {
          location.reload();
        });
      }
    });
  })
  .on( "click", "button.soAllocate", function() { SoAlloc.showSoAllocForm( $(this).data( "id" ) ); } )
  .on( "click", "button.soPickup", function() { SoPickup.showPrintForm( $(this).data( "id" ) ); } )
  .on( "click", "button.soHandedOver", function() {
	  $(this).prop("disabled", true).addClass("disabled");
  		SoPickup.handoverItemsAndDocs( $(this).data( "id" ) );
  		} )
  .on( "click", "button.soApprovePayment", function() { SoOtherTxns.approvePayment( $(this).data( "id" ) ); } )
  .on( "click", "button.soActivate", function() {
	  	$(this).prop("disabled", true).addClass("disabled");
	  	SoOtherTxns.activate( $(this).data( "id" ) );
	  } )
  .on( "click", "button.soCancel", function() { SoOtherTxns.showCancelOrderForm( $(this).data( "id" ) ); } )
  .on( "click", "button.soDelivery", function() { SoOtherTxns.showDeliveryForm( $(this).data( "id" ) ); } )
  .on("click", "button.paymentInfo", function() {
	  $.get("<c:url value="/giftcard/salesorder/payments/form/" />" + $(this).data("id"), function(data) {
        $orderDialog.modal("show").html(data);
      }, "html");

  })
  .on("click", "button.deptAlloc", function() {
  	$.get("<c:url value="/giftcard/salesorder/deptalloc/form/" />" + $(this).data("id"), function(data) {
        $orderDialog.modal("show").html(data);
      }, "html");

  })
  .on("click","button.soHistory",function(){
	  $.get("<c:url value="/giftcard/salesorder/history/" />" + $(this).data("id"), function(data) {
		$orderDialog.modal("show").html(data);
	},"html");
  });

  function initCommonFields(dialog) {
    $(".floatInput", $(dialog)).numberInput({ "type" : "float" });
    $(".intInput", $(dialog)).numberInput({ "type" : "int" });
      $(".wholeInput", $(dialog)).numberInput({ "type" : "whole" });
  }

  $("#printBtn").click(function(e) {
		e.preventDefault();
		var url = $("#reportType option:selected").data("url");
		var selectedReportType = $("select[name='reportType']").val();
		var searchType = "&searchType=" + $("#criteria").val();
    	var searchValue = "&searchValue=" + $("#criteriaValue").val();
    	var dateFrom = "&dateFrom=" + $("#orderDateFrom").val();
    	var dateTo = "&dateTo=" + $("#orderDateTo").val();
    	var status = "&status=" + $("#status").val();
    	var orderType = "&orderType=" + $("#orderType").val();

        if ($("#criteria").val() != "") {
          url = url + searchType + searchValue;
        }

        if ($("#orderDateFrom").val() != "") {
          url = url + dateFrom;
        }

        if ($("#orderDateTo").val() != "") {
          url = url + dateTo;
        }

        if ($("#status").val() != "") {
          url = url + status;
        }

        if ($("#orderType").val() != "") {
          url = url + orderType;
        }

		if(selectedReportType == "pdf")
			window.open(url, '_blank', 'toolbar=0,location=0,menubar=0' );
		else if(selectedReportType == "excel")
			window.location.href = url;
	});
});
</script>
<script type="text/javascript">
 $('.selectpicker').selectpicker({
style: 'btn-print-drop',
size: 4,
showIcon: false
});
</script>

<jsp:include page="activetxns/soactivetxns.jsp" />
