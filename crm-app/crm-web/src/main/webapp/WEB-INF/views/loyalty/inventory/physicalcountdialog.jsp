<%@ include file="../../common/taglibs.jsp" %>

<div class="modal hide  nofly" id="physicalCountDialog" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
   
      <div class="modal-body">
      
      <div id="contentError" class="hide alert alert-error">
        <button type="button" class="close" onclick="$(this).parent().hide();">&times;</button>
        <div><form:errors path="*"/></div>
      </div>
      
      <form:form id="physicalCountForm" name="physicalCountForm" modelAttribute="physicalCountForm" action="${pageContext.request.contextPath}/loyaltycardinventory/physicalcount" method="POST" cssClass="form-reset form-horizontal" >
      	<div class="control-group">
          <spring:message code="inventory_prop_location" var="locationStr" />
          <label for="storeName" class="control-label">${locationStr}</label> 
          <div class="controls">
            <sec:authentication var="userLocation" property="principal.inventoryLocation" />
            <sec:authentication var="userLocationName" property="principal.inventoryLocationName" />

            <input type="text" name="locationDesc" value="${userLocation} - ${userLocationName}" class="form-control" placeholder="${locationStr}" disabled="disabled" />
            <input type="hidden" name="location" value="${userLocation}" />
          </div>
        </div>
        <div class="control-group">
          <spring:message code="mo_tier_prop_name" var="cardTypeStr" />
          <label for="storeName" class="control-label">${cardTypeStr}</label> 
          <div class="controls">
            <input type="text" name="cardTypeDesc" class="form-control" placeholder="${cardTypeStr}" disabled="disabled" />
            <input type="hidden" name="cardType" data-url="<c:url value="/loyaltycardinventory/physicalcount/cardtype" />" />
          </div>
        </div>
        <div class="control-group">
          <spring:message code="mo_tier_prop_startingseries" var="startingSeriesStr" />
          <label for="storeName" class="control-label">${startingSeriesStr}</label> 
          <div class="controls">
            <form:input path="startingSeries" maxlength="12" class="form-control" placeholder="${startingSeriesStr}" />
          </div>
        </div>
        <div class="control-group">
          <spring:message code="mo_tier_prop_endingseries" var="endingSeriesStr" />
          <label for="storeName" class="control-label">${endingSeriesStr}</label> 
          <div class="controls">
            <form:input path="endingSeries" maxlength="12" class="form-control" placeholder="${endingSeriesStr}" />
          </div>
        </div>
        <div class="control-group">
          <spring:message code="mo_tier_prop_quantity" var="quantityStr" />
          <label for="storeName" class="control-label">${quantityStr}</label> 
          <div class="controls">
            <form:input path="quantity" class="form-control" readonly="true" placeholder="${quantityStr}" />
          </div>
        </div>
        <div class="control-group">
          <spring:message code="mo_tier_prop_boxno" var="boxNoStr" />
          <label for="storeName" class="control-label">${boxNoStr}</label> 
          <div class="controls">
            <form:input path="boxNo" class="form-control" placeholder="${boxNoStr}" />
          </div>
        </div>
      </form:form>
      </div>
      <div class="modal-footer">
        <button type="button" id="countSubmit" class="btn btn-primary"><spring:message code="label_save" /></button>
        <button type="button" id="countCancel" class="btn btn-default" data-dismiss="modal"><spring:message code="label_stop"/></button>
      </div>
    </div>
  </div>
</div>


