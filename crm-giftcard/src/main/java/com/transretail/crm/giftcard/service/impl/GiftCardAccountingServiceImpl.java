package com.transretail.crm.giftcard.service.impl;

import com.google.common.collect.Lists;
import com.transretail.crm.core.entity.CrmForPeoplesoftConfig;
import com.transretail.crm.core.entity.CrmForPeoplesoftConfig.TransactionType;
import com.transretail.crm.core.entity.PeopleSoftStore;
import com.transretail.crm.core.entity.QPeopleSoftStore;
import com.transretail.crm.core.repo.CrmForPeoplesoftConfigRepo;
import com.transretail.crm.core.repo.PeopleSoftStoreRepo;
import com.transretail.crm.core.security.util.UserUtil;
import com.transretail.crm.core.service.CodePropertiesService;
import com.transretail.crm.giftcard.dto.AffiliateDto;
import com.transretail.crm.giftcard.dto.B2BExhibitionDto;
import com.transretail.crm.giftcard.dto.ReturnRecordDto;
import com.transretail.crm.giftcard.dto.SalesOrderDto;
import com.transretail.crm.giftcard.entity.*;
import com.transretail.crm.giftcard.repo.CrmForPeoplesoftRepo;
import com.transretail.crm.giftcard.repo.GiftCardTransactionRepo;
import com.transretail.crm.giftcard.repo.SalesOrderRepo;
import com.transretail.crm.giftcard.service.GiftCardAccountingService;
import java.math.BigDecimal;
import java.math.MathContext;
import java.util.Arrays;
import java.util.List;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;
import org.joda.time.DateTime;
import org.joda.time.YearMonth;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

import static com.transretail.crm.core.entity.CrmForPeoplesoftConfig.TransactionType.AFFILIATE_CLAIM;
import static com.transretail.crm.core.entity.CrmForPeoplesoftConfig.TransactionType.AFFILIATE_REDEMPTION;

@Service
@Transactional
public class GiftCardAccountingServiceImpl implements GiftCardAccountingService {

    private static final Logger logger = LoggerFactory.getLogger(GiftCardAccountingServiceImpl.class);

    private static final TransactionType[] AFFILIATES_TXN = new TransactionType[]{AFFILIATE_CLAIM, AFFILIATE_REDEMPTION};

    @Autowired
    private CrmForPeoplesoftRepo acctRepo;
    @Autowired
    private CrmForPeoplesoftConfigRepo configRepo;
    @Autowired
    private PeopleSoftStoreRepo peopleSoftStoreRepo;
    @Autowired
    private CodePropertiesService codePropertiesService;
    @Autowired
    private GiftCardTransactionRepo cardTransactionRepo;
    @Autowired
    private SalesOrderRepo salesOrderRepo;

    @Override
    @Transactional
    public void saveAcctTxn(TransactionType transactionType, String mmsAcct, String psAcct, String type, DateTime txnDate, BigDecimal txnAmt, String storeCode, String deptId, String notes, boolean setDefaultBu) {
        CrmForPeoplesoft acctEntry = new CrmForPeoplesoft();
        acctEntry.setTransactionType(transactionType);
        acctEntry.setMmsAcct(mmsAcct);
        acctEntry.setPsAcct(psAcct);
        acctEntry.setType(type);
        acctEntry.setTransactionDate(txnDate);
        acctEntry.setTransactionAmt(txnAmt);
        acctEntry.setStoreCode(storeCode);

        acctEntry.setDeptId(deptId);
        acctEntry.setNotes(notes);

        setBuPsStoreCode(acctEntry, storeCode, setDefaultBu);
        acctRepo.saveAndFlush(acctEntry);
    }

    private void setBuPsStoreCode(CrmForPeoplesoft acctEntry, String storeCode, boolean setDefaultBu) {
        PeopleSoftStore mapping = null;
        if (StringUtils.isNotBlank(storeCode)) {
            QPeopleSoftStore qPeopleSoftStore = QPeopleSoftStore.peopleSoftStore;
            mapping = peopleSoftStoreRepo.findOne(qPeopleSoftStore.store.code.eq(storeCode));
            if (mapping != null) {
                acctEntry.setBusinessUnit(mapping.getBusinessUnit().getCode());
                acctEntry.setPsStoreCode(mapping.getPeoplesoftCode());
            }
        }
        if (mapping == null && setDefaultBu) {
            acctEntry.setBusinessUnit(codePropertiesService.getDtlGcBuC4Default());
        }
    }

    @Override
    public void forAdvSoVerifApproval(DateTime txnDate, BigDecimal txnAmt, String notes) {
        CrmForPeoplesoftConfig config = getConfig(TransactionType.B2B_ADV_SO_APPROVAL);
        saveAcctTxn(TransactionType.B2B_ADV_SO_APPROVAL, config.getMmsAcct(), config.getPsAcct(), config.getType(), txnDate, txnAmt.negate(), null, null, notes, false);
    }

    @Override
    public void forReplacementPayment(DateTime txnDate, BigDecimal txnAmt, String notes) {
        CrmForPeoplesoftConfig config = getConfig(TransactionType.B2B_REP_SO_PAYMENT);
        saveAcctTxn(TransactionType.B2B_REP_SO_PAYMENT, config.getMmsAcct(), config.getPsAcct(), config.getType(), txnDate, txnAmt.negate(), null, null, notes, false);
    }

    @Override
    public void forAdvSoVerifCancelled(DateTime txnDate, BigDecimal txnAmt, String notes) {
        CrmForPeoplesoftConfig config = getConfig(TransactionType.B2B_ADV_SO_CANCEL_VERIF);
        saveAcctTxn(TransactionType.B2B_ADV_SO_CANCEL_VERIF, config.getMmsAcct(), config.getPsAcct(), config.getType(), txnDate, txnAmt.negate(), null, null, notes, false);
    }

    @Override
    public void forAdvSoVerif(DateTime txnDate, BigDecimal txnAmt, String notes, String storeCode) {
        CrmForPeoplesoftConfig config = getConfig(TransactionType.B2B_ADV_SO_VERIFICATION);
        saveAcctTxn(TransactionType.B2B_ADV_SO_VERIFICATION, config.getMmsAcct(), config.getPsAcct(), config.getType(), txnDate, txnAmt, storeCode, null, notes, false);
    }

    @Override
    public void forB2BInternalSOActivation(DateTime txnDate, BigDecimal txnAmt, String deptId, String notes) {
        CrmForPeoplesoftConfig config = getConfig(TransactionType.B2B_INTERNAL_SO_ACTIVATION);
        saveAcctTxn(TransactionType.B2B_INTERNAL_SO_ACTIVATION, config.getMmsAcct(), config.getPsAcct(), config.getType(), txnDate, txnAmt.negate(), null, deptId, notes, true);
    }

    @Override
    public void forGiftToCust(DateTime txnDate, BigDecimal txnAmt, String storeCode, String notes) {
        CrmForPeoplesoftConfig config = getConfig(TransactionType.GIFT_TO_CUSTOMER);
        saveAcctTxn(TransactionType.GIFT_TO_CUSTOMER, config.getMmsAcct(), config.getPsAcct(),
                config.getType(), txnDate, txnAmt, storeCode, config.getDeptId(), notes, false);
    }

    @Override
    public void forB2BPayment(DateTime txnDate, BigDecimal txnAmt, String storeCode, String notes) {
        CrmForPeoplesoftConfig config = getConfig(TransactionType.B2B_SO_PAYMENT);
        saveAcctTxn(TransactionType.B2B_SO_PAYMENT, config.getMmsAcct(), config.getPsAcct(), config.getType(), txnDate, txnAmt.negate(), storeCode, null, notes, false);
    }

    @Override
    public void forB2BExhibition(B2BExhibitionDto exh) {
        DateTime txnDate = exh.getCreated();
        String storeCode = UserUtil.getCurrentUser().getStoreCode();
        String notes = exh.getTransactionNo() + ", " + exh.getKwitansiNo();

        CrmForPeoplesoftConfig config = getConfig(TransactionType.EXHIBIT_B2B_PAYMENT);
        saveAcctTxn(TransactionType.EXHIBIT_B2B_PAYMENT, config.getMmsAcct(), config.getPsAcct(), config.getType(), txnDate, exh.getTotalPayment().negate(), storeCode, null, notes, false);

        config = getConfig(TransactionType.EXHIBIT_B2B_ACTIVATION);
        saveAcctTxn(TransactionType.EXHIBIT_B2B_ACTIVATION, config.getMmsAcct(), config.getPsAcct(), config.getType(), txnDate, exh.getTotalAmountDue().negate(), storeCode, null, notes, false);
    }

    @Override
    public void forB2BReturn(String orderNo) {
        QCrmForPeoplesoft qCrmForPeoplesoft = QCrmForPeoplesoft.crmForPeoplesoft;
        DateTime txnDate = new DateTime();
        CrmForPeoplesoftConfig config = null;

        CrmForPeoplesoft acctEntry = acctRepo.findOne(qCrmForPeoplesoft.notes.equalsIgnoreCase(orderNo).and(qCrmForPeoplesoft.transactionType.eq(TransactionType.B2B_SO_PAYMENT)));
        if (acctEntry != null) {
            config = getConfig(TransactionType.B2B_RETURN_REV_PAYMENT);
            saveAcctTxn(TransactionType.B2B_RETURN_REV_PAYMENT, config.getMmsAcct(), config.getPsAcct(), config.getType(), txnDate, acctEntry.getTransactionAmt().negate(), acctEntry.getStoreCode(), null, orderNo, false);
        }
        acctEntry = acctRepo.findOne(qCrmForPeoplesoft.notes.equalsIgnoreCase(orderNo).and(qCrmForPeoplesoft.transactionType.eq(TransactionType.B2B_SO_ACTIVATION)));
        if (acctEntry != null) {
            config = getConfig(TransactionType.B2B_RETURN_REV_ACTIVATION);
            saveAcctTxn(TransactionType.B2B_RETURN_REV_ACTIVATION, config.getMmsAcct(), config.getPsAcct(), config.getType(), txnDate, acctEntry.getTransactionAmt().negate(), acctEntry.getStoreCode(), null, orderNo, false);
        }
        acctEntry = acctRepo.findOne(qCrmForPeoplesoft.notes.equalsIgnoreCase(orderNo).and(qCrmForPeoplesoft.transactionType.eq(TransactionType.B2B_SO_DISC)));
        if (acctEntry != null) {
            config = getConfig(TransactionType.B2B_RETURN_REV_DISC);
            saveAcctTxn(TransactionType.B2B_RETURN_REV_DISC, config.getMmsAcct(), config.getPsAcct(), config.getType(), txnDate, acctEntry.getTransactionAmt().negate(), acctEntry.getStoreCode(), null, orderNo, false);
        }

        acctEntry = acctRepo.findOne(qCrmForPeoplesoft.notes.equalsIgnoreCase(orderNo).and(qCrmForPeoplesoft.transactionType.eq(TransactionType.B2B_SO_CARD_FEE)));
        if (acctEntry != null) {
            config = getConfig(TransactionType.B2B_RETURN_REV_CARD_FEE);
            saveAcctTxn(TransactionType.B2B_RETURN_REV_CARD_FEE, config.getMmsAcct(), config.getPsAcct(), config.getType(), txnDate, acctEntry.getTransactionAmt().negate(), acctEntry.getStoreCode(), null, orderNo, false);
        }
    }
    
    @Override
    public void forB2BReturn(SalesOrderDto order, ReturnRecordDto returnByCards) {
        DateTime txnDate = new DateTime();
        CrmForPeoplesoftConfig config = null;
        
        String orderNo = order.getOrderNo();
        String paymentStore = order.getPaymentStore();
        BigDecimal totalRetAmount = returnByCards.getReturnAmount();
        BigDecimal discVal = returnByCards.getFaceAmount().subtract(totalRetAmount);
        BigDecimal soCardFee = order.getTotalCardFee();
        BigDecimal retCardFee = soCardFee.multiply(order.getTotalFaceAmount().divide(returnByCards.getFaceAmount(), MathContext.DECIMAL32));

        config = getConfig(TransactionType.B2B_RETURN_REV_PAYMENT);
        saveAcctTxn(TransactionType.B2B_RETURN_REV_PAYMENT, config.getMmsAcct(), config.getPsAcct(), config.getType(), txnDate, totalRetAmount, paymentStore, null, orderNo, false);
       
        config = getConfig(TransactionType.B2B_RETURN_REV_ACTIVATION);
        saveAcctTxn(TransactionType.B2B_RETURN_REV_ACTIVATION, config.getMmsAcct(), config.getPsAcct(), config.getType(), txnDate, returnByCards.getFaceAmount().negate(), paymentStore, null, orderNo, false);

        config = getConfig(TransactionType.B2B_RETURN_REV_DISC);
        saveAcctTxn(TransactionType.B2B_RETURN_REV_DISC, config.getMmsAcct(), config.getPsAcct(), config.getType(), txnDate, discVal, paymentStore, null, orderNo, false);

        /*config = getConfig(TransactionType.B2B_RETURN_REV_CARD_FEE);
        saveAcctTxn(TransactionType.B2B_RETURN_REV_CARD_FEE, config.getMmsAcct(), config.getPsAcct(), config.getType(), txnDate, retCardFee, paymentStore, null, orderNo, false);*/
    }

    @Override
    public void forFreeVoucherActivation(SalesOrderDto orderDto) {
        String orderNo = orderDto.getOrderNo();

        BigDecimal discount = orderDto.getDiscountVal(); //TODO null discount for VOUCHER
        if (discount == null) {
            discount = BigDecimal.ZERO;
        }

        DateTime txnDate = new DateTime();
        String storeCode = orderDto.getPaymentStore();

        CrmForPeoplesoftConfig config = getConfig(TransactionType.FREE_VOUCHER_SO_ACTIVATION);
        saveAcctTxn(TransactionType.FREE_VOUCHER_SO_ACTIVATION, config.getMmsAcct(), config.getPsAcct(), config.getType(), txnDate, orderDto.getTotalFaceAmount().negate(), storeCode, null, orderNo, false);
    }

    @Override
    public void forAdvB2BActivation(SalesOrderDto orderDto) {
        String orderNo = orderDto.getOrderNo();

        BigDecimal discount = orderDto.getDiscountVal(); //TODO null discount for VOUCHER
        if (discount == null) {
            discount = BigDecimal.ZERO;
        }

        BigDecimal discVal = orderDto.getTotalFaceAmount().multiply(discount.divide(new BigDecimal(100)));
        BigDecimal discntdVal = orderDto.getTotalFaceAmount().subtract(discVal);
        DateTime txnDate = new DateTime();
        String storeCode = orderDto.getPaymentStore();

        CrmForPeoplesoftConfig config = getConfig(TransactionType.B2B_ADV_SO_ACTIVATION);
        saveAcctTxn(TransactionType.B2B_ADV_SO_ACTIVATION, config.getMmsAcct(), config.getPsAcct(), config.getType(), txnDate, orderDto.getTotalFaceAmount().negate(), storeCode, null, orderNo, false);

        if (discVal.compareTo(BigDecimal.ZERO) != 0) {
            config = getConfig(TransactionType.B2B_ADV_SO_DISC);
            saveAcctTxn(TransactionType.B2B_ADV_SO_DISC, config.getMmsAcct(), config.getPsAcct(), config.getType(), txnDate, discVal, storeCode, config.getDeptId(), orderNo, false);
        }

        BigDecimal shippingFee = orderDto.getShippingFee();
        if (shippingFee == null) {
            shippingFee = BigDecimal.ZERO;
        }
        if (shippingFee.compareTo(BigDecimal.ZERO) != 0) {
            config = getConfig(TransactionType.B2B_ADV_SO_SHIPPING);
            saveAcctTxn(TransactionType.B2B_ADV_SO_SHIPPING, config.getMmsAcct(), config.getPsAcct(), config.getType(), txnDate, shippingFee.negate(), storeCode, config.getDeptId(), orderNo, false);
        }

        if (orderDto.getTotalCardFee().compareTo(BigDecimal.ZERO) != 0) {
            config = getConfig(TransactionType.B2B_ADV_SO_CARD_FEE);
            saveAcctTxn(TransactionType.B2B_ADV_SO_CARD_FEE, config.getMmsAcct(), config.getPsAcct(), config.getType(), txnDate, orderDto.getTotalCardFee().negate(), storeCode, config.getDeptId(), orderNo, false);
        }

    }

    @Override
    public void forB2BActivation(SalesOrderDto orderDto) {
        String orderNo = orderDto.getOrderNo();

        BigDecimal discount = orderDto.getDiscountVal(); //TODO null discount for VOUCHER
        if (discount == null) {
            discount = BigDecimal.ZERO;
        }

        BigDecimal discVal = orderDto.getTotalFaceAmount().multiply(discount.divide(new BigDecimal(100)));
        BigDecimal discntdVal = orderDto.getTotalFaceAmount().subtract(discVal);
        DateTime txnDate = new DateTime();
        String storeCode = orderDto.getPaymentStore();

        CrmForPeoplesoftConfig config = getConfig(TransactionType.B2B_SO_ACTIVATION);
        saveAcctTxn(TransactionType.B2B_SO_ACTIVATION, config.getMmsAcct(), config.getPsAcct(), config.getType(), txnDate, orderDto.getTotalFaceAmount().negate(), storeCode, null, orderNo, false);

        config = getConfig(TransactionType.B2B_SO_DISC);
        saveAcctTxn(TransactionType.B2B_SO_DISC, config.getMmsAcct(), config.getPsAcct(), config.getType(), txnDate, discVal, storeCode, config.getDeptId(), orderNo, false);

        config = getConfig(TransactionType.B2B_SO_SHIPPING);
        BigDecimal shippingFee = orderDto.getShippingFee();
        if (shippingFee == null) {
            shippingFee = BigDecimal.ZERO;
        }
        saveAcctTxn(TransactionType.B2B_SO_SHIPPING, config.getMmsAcct(), config.getPsAcct(), config.getType(), txnDate, shippingFee.negate(), storeCode, config.getDeptId(), orderNo, false);

        config = getConfig(TransactionType.B2B_SO_CARD_FEE);
        saveAcctTxn(TransactionType.B2B_SO_CARD_FEE, config.getMmsAcct(), config.getPsAcct(), config.getType(), txnDate, orderDto.getTotalCardFee().negate(), storeCode, config.getDeptId(), orderNo, false);

    }

    @Override
    public void forB2BReplacementActivation(SalesOrderDto orderDto) {
        String notes = orderDto.getOrderNo() + ", " + orderDto.getReturnNo();

        BigDecimal discount = orderDto.getDiscountVal(); //TODO null discount for VOUCHER
        if (discount == null) {
            discount = BigDecimal.ZERO;
        }

        BigDecimal discVal = orderDto.getTotalFaceAmount().multiply(discount.divide(new BigDecimal(100)));
        BigDecimal discntdVal = orderDto.getTotalFaceAmount().subtract(discVal);
        DateTime txnDate = new DateTime();
        String storeCode = orderDto.getPaymentStore();

        CrmForPeoplesoftConfig config = getConfig(TransactionType.B2B_REP_SO_ACTIVATION);
        saveAcctTxn(TransactionType.B2B_REP_SO_ACTIVATION, config.getMmsAcct(), config.getPsAcct(), config.getType(), txnDate, orderDto.getTotalFaceAmount().negate(), storeCode, null, notes, false);

        config = getConfig(TransactionType.B2B_REP_SO_DISC);
        saveAcctTxn(TransactionType.B2B_REP_SO_DISC, config.getMmsAcct(), config.getPsAcct(), config.getType(), txnDate, discVal, storeCode, config.getDeptId(), notes, false);

        config = getConfig(TransactionType.B2B_REP_SO_SHIPPING);
        BigDecimal shippingFee = orderDto.getShippingFee();
        if (shippingFee == null) {
            shippingFee = BigDecimal.ZERO;
        }
        saveAcctTxn(TransactionType.B2B_REP_SO_SHIPPING, config.getMmsAcct(), config.getPsAcct(), config.getType(), txnDate, shippingFee.negate(), storeCode, config.getDeptId(), notes, false);

        config = getConfig(TransactionType.B2B_REP_SO_CARD_FEE);
        saveAcctTxn(TransactionType.B2B_REP_SO_CARD_FEE, config.getMmsAcct(), config.getPsAcct(), config.getType(), txnDate, orderDto.getTotalCardFee().negate(), storeCode, config.getDeptId(), notes, false);

        if (orderDto.getHandlingFee() != null) {
            config = getConfig(TransactionType.B2B_REP_SO_HANDLING_FEE);
            saveAcctTxn(TransactionType.B2B_REP_SO_HANDLING_FEE, config.getMmsAcct(), config.getPsAcct(), config.getType(), txnDate, orderDto.getHandlingFee(), storeCode, config.getDeptId(), notes, false);
        }
    }

    @Override
    public CrmForPeoplesoftConfig getConfig(TransactionType txnType) {
        return this.getConfig(txnType, null);
    }

    private CrmForPeoplesoftConfig getConfig(TransactionType txnType, String bu) {
        return configRepo.findByTransactionType(txnType);
    }

    @Override
    public void forB2CActivation(DateTime txnDate, BigDecimal txnAmt, String storeCode, String notes) {
        CrmForPeoplesoftConfig config = getConfig(TransactionType.B2C_ACTIVATION);
        Assert.notNull(config, String.format("%s configuration must not be null.", TransactionType.B2C_ACTIVATION));
        // TODO: better api - avoid too many paramters on a method. better encapsulation
        // check spring transaction pitfalls, calling a transactional aware method inside the calling object.
        saveAcctTxn(TransactionType.B2C_ACTIVATION,
                config.getMmsAcct(),
                config.getPsAcct(),
                config.getType(),
                txnDate,
                txnAmt,
                storeCode, null, notes, false);
    }

    @Override
    public void forReloadSales(DateTime txnDate, BigDecimal txnAmt, String storeCode, String notes) {
        CrmForPeoplesoftConfig config = getConfig(TransactionType.B2C_ACTIVATION);
        Assert.notNull(config, String.format("%s configuration must not be null.", TransactionType.B2C_ACTIVATION));
        // TODO: better api - avoid too many paramters on a method. better encapsulation
        // check spring transaction pitfalls, calling a transactional aware method inside the calling object.
        saveAcctTxn(TransactionType.RELOAD_SALES,
                config.getMmsAcct(),
                config.getPsAcct(),
                config.getType(),
                txnDate,
                txnAmt,
                storeCode, null, notes, false);
    }

    @Override
    public void forReloadSalesVoiding(DateTime txnDate, BigDecimal txnAmt, String storeCode, String notes) {
        CrmForPeoplesoftConfig config = getConfig(TransactionType.B2C_ACTIVATION);
        Assert.notNull(config, String.format("%s configuration must not be null.", TransactionType.B2C_ACTIVATION));
        // TODO: better api - avoid too many paramters on a method. better encapsulation
        // check spring transaction pitfalls, calling a transactional aware method inside the calling object.
        saveAcctTxn(TransactionType.RELOAD_SALES_VOID,
                config.getMmsAcct(),
                config.getPsAcct(),
                config.getType(),
                txnDate,
                txnAmt,
                storeCode, null, notes, false);
    }

    @Override
    public void forB2CSalesVoiding(DateTime txnDate, BigDecimal txnAmt, String storeCode, String notes) {
        CrmForPeoplesoftConfig config = getConfig(TransactionType.B2C_VOID_ACTIVATION);
        Assert.notNull(config, String.format("%s configuration must not be null.", TransactionType.B2C_VOID_ACTIVATION));
        // TODO: better api - avoid too many paramters on a method. better encapsulation
        // check spring transaction pitfalls, calling a transactional aware method inside the calling object.
        saveAcctTxn(TransactionType.B2C_VOID_ACTIVATION,
                config.getMmsAcct(),
                config.getPsAcct(),
                config.getType(),
                txnDate,
                txnAmt,
                storeCode, null, notes, false);
    }

    @Override
    public void forB2CRedemption(DateTime txnDate, BigDecimal txnAmt, String storeCode, String notes) {
        CrmForPeoplesoftConfig config = getConfig(TransactionType.REDEMPTION);
        Assert.notNull(config, String.format("%s configuration must not be null.", TransactionType.REDEMPTION));
        // TODO: better api - avoid too many paramters on a method. better encapsulation
        // check spring transaction pitfalls, calling a transactional aware method inside the calling object.
        saveAcctTxn(TransactionType.REDEMPTION,
                config.getMmsAcct(),
                config.getPsAcct(),
                config.getType(),
                txnDate,
                txnAmt,
                storeCode, null, notes, false);
    }

    @Override
    public void forB2CRedemptionVoiding(DateTime txnDate, BigDecimal txnAmt, String storeCode, String notes) {
        CrmForPeoplesoftConfig config = getConfig(TransactionType.VOID_REDEMPTION);
        Assert.notNull(config, String.format("%s configuration must not be null.", TransactionType.VOID_REDEMPTION));
        // TODO: better api - avoid too many paramters on a method. better encapsulation
        // check spring transaction pitfalls, calling a transactional aware method inside the calling object.
        saveAcctTxn(TransactionType.VOID_REDEMPTION,
                config.getMmsAcct(),
                config.getPsAcct(),
                config.getType(),
                txnDate,
                txnAmt,
                storeCode, null, notes, false);
    }

    @Override
    @Transactional
    public void createAffiliatesAcctEntry() {
        YearMonth prevMonth = YearMonth.now().minusMonths(1);
        List<AffiliateDto> result = cardTransactionRepo.getBusinessUnitTransactionTypeAmount(prevMonth);
        List<CrmForPeoplesoft> list = Lists.newArrayList();
        for (AffiliateDto dto : result) {
            list.add(createAffilatesAcctEntry(dto));
        }
        
        acctRepo.save(list);
        
        long affectedRecords = cardTransactionRepo.tagAffiliatesTransactionAsPosted(prevMonth);
        
        logger.debug("Tagging affiliates transaction for the month of {}. Total affected rows: {}.", prevMonth, affectedRecords);
    }

    /**
     * helper method to save the claims to affiliates. It will negate internally
     * the parameter amount, and it will resolve internally the PeopleSoft
     * Configuration
     *
     * @param type
     * @param amount
     */
    private CrmForPeoplesoft createAffilatesAcctEntry(AffiliateDto dto) {
        final TransactionType transactionType = dto.getTransactionType();
        Assert.isTrue(ArrayUtils.contains(AFFILIATES_TXN, transactionType),
                String.format("Transaction Type must be on of %s.", Arrays.toString(AFFILIATES_TXN)));
        logger.debug("Creating affiliate's accounting entry of transaction type \'{}\' for bu {}.", transactionType, dto.getBu());

        CrmForPeoplesoftConfig config = getConfig(transactionType);
        Assert.notNull(config, String.format("%s configuration must not be null.", transactionType));

        CrmForPeoplesoft acctEntry = new CrmForPeoplesoft();
        acctEntry.setTransactionType(transactionType);
        acctEntry.setMmsAcct(config.getMmsAcct());
        acctEntry.setPsAcct(config.getPsAcct());
        acctEntry.setType(config.getType());
        acctEntry.setTransactionDate(DateTime.now());
        acctEntry.setTransactionAmt(new BigDecimal(dto.getAmount()).negate());
        acctEntry.setStoreCode(null);
        acctEntry.setDeptId(null);
        acctEntry.setNotes(null);
        acctEntry.setBusinessUnit(dto.getBu());

        return acctEntry;
    }
}
