package com.transretail.crm.web.controller;

import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.reset;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.springframework.http.MediaType.APPLICATION_JSON;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.AnnotationConfigWebContextLoader;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.transretail.crm.giftcard.dto.SafetyStockDto;
import com.transretail.crm.giftcard.service.SafetyStockService;

/**
 * @author Allan G. Ramirez (agramirez@exist.com)
 */
@RunWith(SpringJUnit4ClassRunner.class)
@WebAppConfiguration
@ContextConfiguration(loader = AnnotationConfigWebContextLoader.class)
public class SafetyStockControllerTest extends AbstractMockMvcTest {
    @Configuration
    @EnableWebMvc
    static class Config {
        @Bean
        public SafetyStockController safetyStockController() {
            return new SafetyStockController();
        }

        @Bean
        public SafetyStockService safetyStockService() {
            return mock(SafetyStockService.class);
        }
    }

    @Autowired
    private SafetyStockService safetyStockService;

    @Override
    protected void doSetup() throws Exception {
        reset(safetyStockService);
    }

    @Test
    public void getSafetyStock() throws Exception {
        long id = 1l;
        SafetyStockDto expected = new SafetyStockDto();
        expected.setProductProfileId(1l);
        expected.setMonth1(120);

        when(safetyStockService.getSafetyStock(id)).thenReturn(expected);
        String response = mockMvc.perform(get("/safetystock/" + id)).andReturn().getResponse().getContentAsString();
        ObjectMapper mapper = new ObjectMapper();
        SafetyStockDto actual = mapper.readValue(response, SafetyStockDto.class);
        assertEquals(expected.getProductProfileId(), actual.getProductProfileId());
        assertEquals(expected.getMonth1(), actual.getMonth1());
    }

    @Test
    public void saveOrUpdate() throws Exception {
        SafetyStockDto dto = new SafetyStockDto();
        dto.setProductProfileId(1L);

        ObjectMapper mapper = new ObjectMapper();
        mockMvc.perform(post("/safetystock/save").content(mapper.writeValueAsBytes(dto)).contentType(APPLICATION_JSON))
            .andExpect(status().isOk());
        verify(safetyStockService).update(any(SafetyStockDto.class));
    }
}
