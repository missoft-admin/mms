package com.transretail.crm.report.template.impl;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.joda.time.LocalDate;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.MessageSourceAware;
import org.springframework.context.support.MessageSourceAccessor;
import org.springframework.stereotype.Service;

import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Lists;
import com.transretail.crm.common.reporting.jasper.ClasspathFileResolver;
import com.transretail.crm.common.reporting.jasper.DefaultJRProcessor;
import com.transretail.crm.common.reporting.jasper.JRProcessor;
import com.transretail.crm.common.service.dto.response.ResultList;
import com.transretail.crm.core.entity.enums.ReportType;
import com.transretail.crm.core.entity.lookup.Store;
import com.transretail.crm.core.security.util.UserUtil;
import com.transretail.crm.core.service.StoreService;
import com.transretail.crm.giftcard.dto.GiftCardInventoryDto;
import com.transretail.crm.giftcard.dto.GiftCardInventorySearchDto;
import com.transretail.crm.giftcard.repo.GiftCardInventoryRepo;
import com.transretail.crm.giftcard.service.GiftCardInventoryStockService;
import com.transretail.crm.report.template.FilterField;
/**
 * @author ftopico
 */

@Service("giftCardInventoryStockDetailedListDocument")
public class GiftCardInventoryStockDetailedListDocument implements MessageSourceAware {
    
    @Autowired
    private GiftCardInventoryStockService giftCardInventoryStockService;
    @Autowired
    private StoreService storeService;
    @Autowired
    private GiftCardInventoryRepo giftCardInventoryRepo;

    protected static final String TEMPLATE_NAME = "Gift Card Inventory Stock Detailed List Document";
    protected static final String REPORT_NAME = "reports/gift_card_inventory_stock_detailed_list_document.jasper";
    //Header Params
    private static final String PARAM_PRINT_DATE = "printDate";
    private static final String PARAM_PRINT_BY = "printBy";
    private static final DateTimeFormatter FULL_DATE_PATTERN = DateTimeFormat.forPattern("MMMM d, yyyy");
    private MessageSourceAccessor messageSource;
    
    public ReportType[] getReportTypes() {
        return ReportType.values();
    }
    
    @Override
    public void setMessageSource(MessageSource messageSource) {
        this.messageSource = new MessageSourceAccessor(messageSource);
    }
    
    public String getName() {
        return TEMPLATE_NAME;
    }
    
    public String getDescription() {
        return messageSource.getMessage("title_document_inventory_stock_detailed_list");
    }
    
    public Set<FilterField> getFilters() {
        return null;
    }
    
    public JRProcessor createJrProcessor(GiftCardInventorySearchDto searchDto) {

    	ResultList<GiftCardInventoryDto> resultList = giftCardInventoryStockService.getGiftCardInventoriesForPrintDetail(searchDto);
    	
    	List<ReportBean> beansDataSource = Lists.newArrayList();
    	DecimalFormat df = new DecimalFormat("#,##0");
    	
    	String location = null;
    	String description = null;
    	Long previousBarcodeSeries = null;
    	Long currentBarcodeSeries = null;
    	GiftCardInventoryDto previousInventory = null;
    	Map<String, GiftCardSummaryReport> reportMap = new LinkedHashMap<String, GiftCardInventoryStockDetailedListDocument.GiftCardSummaryReport>();
    	List<GiftCardSummaryReportDetails> reportDetails = new ArrayList<GiftCardInventoryStockDetailedListDocument.GiftCardSummaryReportDetails>();
    	List<GiftCardSummaryReport> reportSummary = new ArrayList<GiftCardInventoryStockDetailedListDocument.GiftCardSummaryReport>();
    	
    	Map<Integer, List<GiftCardInventoryDto>> tempHashList = new LinkedHashMap<Integer, List<GiftCardInventoryDto>>();
    	Integer counter = 0;
    	Integer serial = 1 ;
    	for (GiftCardInventoryDto giftCardInventoryStockDto : resultList.getResults()) {
    	    if (tempHashList.isEmpty()) {
    	        List<GiftCardInventoryDto> tempList = new ArrayList<GiftCardInventoryDto>();
    	        tempList.add(giftCardInventoryStockDto);
    	        tempHashList.put(counter, tempList);
    	    } else {
    	        List<GiftCardInventoryDto> tempList = tempHashList.get(counter);
    	        if (tempList.get(0).getLocation().equalsIgnoreCase(giftCardInventoryStockDto.getLocation())) {
    	            if (tempList.get(0).getProductCode().equalsIgnoreCase(giftCardInventoryStockDto.getProductCode())) {
    	                previousInventory = tempList.get(tempList.size() - 1);
    	                previousBarcodeSeries = Long.valueOf(previousInventory.getBarcode().substring(0, 16));
    	                currentBarcodeSeries = Long.valueOf(giftCardInventoryStockDto.getBarcode().substring(0, 16));
    	                if (previousBarcodeSeries + 1 == currentBarcodeSeries) {
    	                    tempList.add(giftCardInventoryStockDto);
    	                } else {
    	                    counter++;
    	                    tempList = new ArrayList<GiftCardInventoryDto>();
    	                    tempList.add(giftCardInventoryStockDto);
    	                    tempHashList.put(counter, tempList);
    	                }
    	            } else {
    	                addToReportDetails(tempHashList, location, description, serial, reportDetails, reportMap, df);
    	                counter = 0;
    	                serial++;
    	                tempList = new ArrayList<GiftCardInventoryDto>();
    	                tempHashList = new LinkedHashMap<Integer, List<GiftCardInventoryDto>>();
    	                tempList.add(giftCardInventoryStockDto);
    	                tempHashList.put(counter, tempList);
    	            }
    	        } else {
    	            addToReportDetails(tempHashList, location, description, serial, reportDetails, reportMap, df);
                    counter = 0;
                    serial = 1;
                    tempHashList = new LinkedHashMap<Integer, List<GiftCardInventoryDto>>();
                    tempList = new ArrayList<GiftCardInventoryDto>();
                    tempList.add(giftCardInventoryStockDto);
                    tempHashList.put(counter, tempList);
    	        }
    	    }
    	}
    	//last batch of list
    	addToReportDetails(tempHashList, location, description, serial, reportDetails, reportMap, df);
    	
    	Map<String, Object> parameters = ImmutableMap.<String, Object>builder()
    	        .put(PARAM_PRINT_DATE, FULL_DATE_PATTERN.print(new LocalDate()))
    	        .put(PARAM_PRINT_BY, UserUtil.getCurrentUser().getUsername()).build();
    	
    	reportSummary.addAll(reportMap.values());
    	beansDataSource.add(new ReportBean(reportDetails, reportSummary));
    	DefaultJRProcessor jrProcessor = new DefaultJRProcessor(REPORT_NAME, beansDataSource);
    	jrProcessor.addParameters(parameters);
    	jrProcessor.setFileResolver(new ClasspathFileResolver("reports"));
    	
    	return jrProcessor;
	
    }
    
    private void addToReportDetails(Map<Integer, List<GiftCardInventoryDto>> tempHashList, String location, String description, Integer serial,
            List<GiftCardSummaryReportDetails> reportDetails, Map<String, GiftCardSummaryReport> reportMap, DecimalFormat df) {
        for (int outerLoop = 0; outerLoop < tempHashList.size(); outerLoop++) {
            List<GiftCardInventoryDto> inventoryList = tempHashList.get(outerLoop);
            location = inventoryList.get(0).getLocation();
            Store store = storeService.getStoreByCode(location);
            if (store != null) {
                description = store.getName();
            } else {
                if (UserUtil.getCurrentUser().getInventoryLocation().equals(location)) {
                    description = UserUtil.getCurrentUser().getInventoryLocationName();
                }
            }
            
            String salesType = null;
            if (inventoryList.get(0).getSalesType() != null) {
                salesType = inventoryList.get(0).getSalesType().name();
            }
            if (outerLoop == 0) {
                reportDetails.add(new GiftCardSummaryReportDetails(
                        serial.toString(),
                        location,
                        description,
                        inventoryList.get(0).getProductName(),
                        inventoryList.get(0).getProductCode(),
                        salesType,
                        String.valueOf(inventoryList.get(0).getBarcode()),
                        String.valueOf(inventoryList.get(inventoryList.size() - 1).getBarcode()),
                        df.format(inventoryList.size())));
            } else {
                reportDetails.add(new GiftCardSummaryReportDetails(
                        null,
                        null,
                        null,
                        inventoryList.get(0).getProductName(),
                        inventoryList.get(0).getProductCode(),
                        salesType,
                        String.valueOf(inventoryList.get(0).getBarcode()),
                        String.valueOf(inventoryList.get(inventoryList.size() - 1).getBarcode()),
                        df.format(inventoryList.size())));
            }
            
            if (!reportMap.containsKey(inventoryList.get(0).getProductCode())) {
                GiftCardSummaryReport giftCardSummaryReport = new GiftCardSummaryReport();
                giftCardSummaryReport.setProductCode(inventoryList.get(0).getProductCode());
                giftCardSummaryReport.setProductName(inventoryList.get(0).getProductName());
                giftCardSummaryReport.setTotalQuantity(Long.valueOf(inventoryList.size()));
                reportMap.put(inventoryList.get(0).getProductCode(), giftCardSummaryReport);
            } else {
                GiftCardSummaryReport giftCardSummaryReport = reportMap.get(inventoryList.get(0).getProductCode());
                giftCardSummaryReport.setTotalQuantity(giftCardSummaryReport.getTotalQuantity() + Long.valueOf(inventoryList.size()));
            }
        }
    }
    
    public class GiftCardSummaryReportDetails {
        
        private String serial;
        private String location;
        private String description;
        private String productName;
        private String productCode;
        private String salesType;
        private String startingBarcode;
        private String endingBarcode;
        private String totalQuantity;
        
        public GiftCardSummaryReportDetails() {}

        public GiftCardSummaryReportDetails(String serial, String location,
                String description, String productName, String productCode, String salesType,
                String startingBarcode, String endingBarcode,
                String totalQuantity) {
            super();
            this.serial = serial;
            this.location = location;
            this.description = description;
            this.productName = productName;
            this.productCode = productCode;
            this.salesType = salesType;
            this.startingBarcode = startingBarcode;
            this.endingBarcode = endingBarcode;
            this.totalQuantity = totalQuantity;
        }

        public String getSerial() {
            return serial;
        }

        public String getLocation() {
            return location;
        }

        public String getDescription() {
            return description;
        }

        public String getProductName() {
            return productName;
        }

        public String getProductCode() {
            return productCode;
        }

        public String getSalesType() {
            return salesType;
        }

        public void setSalesType(String salesType) {
            this.salesType = salesType;
        }

        public String getStartingBarcode() {
            return startingBarcode;
        }

        public String getEndingBarcode() {
            return endingBarcode;
        }

        public String getTotalQuantity() {
            return totalQuantity;
        }

        public void setSerial(String serial) {
            this.serial = serial;
        }

        public void setLocation(String location) {
            this.location = location;
        }

        public void setDescription(String description) {
            this.description = description;
        }

        public void setProductName(String productName) {
            this.productName = productName;
        }

        public void setProductCode(String productCode) {
            this.productCode = productCode;
        }

        public void setStartingBarcode(String startingBarcode) {
            this.startingBarcode = startingBarcode;
        }

        public void setEndingBarcode(String endingBarcode) {
            this.endingBarcode = endingBarcode;
        }

        public void setTotalQuantity(String totalQuantity) {
            this.totalQuantity = totalQuantity;
        }
        
    }
    
    public class GiftCardSummaryReport {
        private String productName;
        private String productCode;
        private Long totalQuantity;
        
        public GiftCardSummaryReport() {}
        
        public GiftCardSummaryReport(String productName,
                            String productCode,
                            Long totalQuantity) {
            super();
            this.productName = productName;
            this.productCode = productCode;
            this.totalQuantity = totalQuantity;
        }

        public String getProductName() {
            return productName;
        }

        public void setProductName(String productName) {
            this.productName = productName;
        }

        public String getProductCode() {
            return productCode;
        }

        public void setProductCode(String productCode) {
            this.productCode = productCode;
        }

        public Long getTotalQuantity() {
            return totalQuantity;
        }

        public void setTotalQuantity(Long totalQuantity) {
            this.totalQuantity = totalQuantity;
        }
    }
    
    public static class ReportBean {
        
        private List<GiftCardSummaryReportDetails> reportDetails;
        private List<GiftCardSummaryReport> reportSummary;
        
        public ReportBean() {}
        
        public ReportBean(List<GiftCardSummaryReportDetails> reportDetails,
                            List<GiftCardSummaryReport> reportSummary) {
            super();
            this.reportDetails = reportDetails;
            this.reportSummary = reportSummary;
        }

        public List<GiftCardSummaryReportDetails> getReportDetails() {
            return reportDetails;
        }

        public void setReportDetails(List<GiftCardSummaryReportDetails> reportDetails) {
            this.reportDetails = reportDetails;
        }

        public List<GiftCardSummaryReport> getReportSummary() {
            return reportSummary;
        }

        public void setReportSummary(List<GiftCardSummaryReport> reportSummary) {
            this.reportSummary = reportSummary;
        }

    }
}
