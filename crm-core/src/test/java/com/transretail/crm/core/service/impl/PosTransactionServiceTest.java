package com.transretail.crm.core.service.impl;

import com.google.common.collect.Lists;
import com.transretail.crm.common.service.dto.response.ResultList;
import com.transretail.crm.core.dto.ShoppingListDto;
import com.transretail.crm.core.entity.MemberModel;
import com.transretail.crm.core.entity.PointsTxnModel;
import com.transretail.crm.core.entity.PosTransaction;
import com.transretail.crm.core.entity.PosTxItem;
import com.transretail.crm.core.entity.embeddable.CustomerProfile;
import com.transretail.crm.core.entity.enums.MemberStatus;
import com.transretail.crm.core.entity.enums.PointTxnType;
import com.transretail.crm.core.entity.enums.TxnStatus;
import com.transretail.crm.core.entity.lookup.Product;
import com.transretail.crm.core.repo.*;
import com.transretail.crm.core.service.PosTransactionService;
import org.joda.time.DateTime;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Configurable;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;
import java.util.Map;

import static org.junit.Assert.assertEquals;

@Configurable
@ContextConfiguration(locations = "classpath*:/META-INF/spring/applicationContext*.xml")
@Transactional
@RunWith(SpringJUnit4ClassRunner.class)
public class PosTransactionServiceTest {
	@Autowired
	PosTransactionService posTransactionService;
	@Autowired
	PosTransactionRepo posTransactionRepo;
	@Autowired
	PosTxItemRepo posTxItemRepo;
	@Autowired
	MemberRepo memberRepo;
	@Autowired
	PointsTxnModelRepo pointsRepo;
	@Autowired
	ProductRepo productRepo;
	
	private MemberModel member;
	
	@Before
	public void setUp() {
		setUpMembers();
		setUpItems();
		setUpPosTransactions();
		setUpPoints();
	}
	
	private void setUpMembers() {
		member = new MemberModel();
		member.setAccountId("test");
		member.setAccountStatus(MemberStatus.ACTIVE);
		member.setEnabled(true);
		member.setFirstName("test");
		member.setUsername("test");
		member.setPin("1");
		member.setContact("1");
		member.setTotalPoints(0D);
		CustomerProfile profile = new CustomerProfile();
		profile.setBirthdate(new Date());
		member.setCustomerProfile(profile);
		
		member = memberRepo.save(member);
	}
	
	private void setUpItems() {
		Product product1 = new Product();
		product1.setId("p1");
		product1.setName("p1");
		product1 = productRepo.save(product1);
		
		Product product2 = new Product();
		product2.setId("p2");
		product2.setName("p2");
		product2 = productRepo.save(product2);
		
		Product product3 = new Product();
		product3.setId("p3");
		product3.setName("p3");
		product3 = productRepo.save(product3);
	}
	
	/**
	 * 3 Transactions:
	 * Txn1: product p1 x 5
	 * Txn2: product p1 x 5, p2 x 5
	 * Txn3: product p1 x 5, p2 x 3, p3 x 7
	 * Averages: 	p1 - 5 per txn
	 * 				p2 - 4 per txn
	 * 				p3 - 7 per txn
	 */
	private void setUpPosTransactions() {
		// txn 1
		PosTransaction posTransaction = new PosTransaction();
		posTransaction.setId("txn1");
		posTransaction = posTransactionRepo.save(posTransaction);
		List<PosTxItem> items = Lists.newArrayList();
		
		PosTxItem posTxItem = new PosTxItem();
		posTxItem.setId("item1");
		posTxItem.setProductId("p1");
		posTxItem.setPosTransaction(posTransaction);
		posTxItem.setQuantity(5d);
		items.add(posTxItemRepo.save(posTxItem));
		
		posTransaction.setOrderItems(items);
		posTransactionRepo.save(posTransaction);
		
		// txn 2
		posTransaction = new PosTransaction();
		posTransaction.setId("txn2");
		posTransaction = posTransactionRepo.save(posTransaction);
		items = Lists.newArrayList();
		
		posTxItem = new PosTxItem();
		posTxItem.setId("item2");
		posTxItem.setProductId("p1");
		posTxItem.setPosTransaction(posTransaction);
		posTxItem.setQuantity(5d);
		items.add(posTxItemRepo.save(posTxItem));
		
		posTxItem = new PosTxItem();
		posTxItem.setId("item3");
		posTxItem.setProductId("p2");
		posTxItem.setPosTransaction(posTransaction);
		posTxItem.setQuantity(5d);
		items.add(posTxItemRepo.save(posTxItem));
		
		posTransaction.setOrderItems(items);
		posTransactionRepo.save(posTransaction);
		
		// txn 3
		posTransaction = new PosTransaction();
		posTransaction.setId("txn3");
		posTransaction = posTransactionRepo.save(posTransaction);
		items = Lists.newArrayList();
		
		posTxItem = new PosTxItem();
		posTxItem.setId("item4");
		posTxItem.setProductId("p1");
		posTxItem.setPosTransaction(posTransaction);
		posTxItem.setQuantity(5d);
		items.add(posTxItemRepo.save(posTxItem));
		
		posTxItem = new PosTxItem();
		posTxItem.setId("item5");
		posTxItem.setProductId("p2");
		posTxItem.setPosTransaction(posTransaction);
		posTxItem.setQuantity(3d);
		items.add(posTxItemRepo.save(posTxItem));
		
		posTxItem = new PosTxItem();
		posTxItem.setId("item6");
		posTxItem.setProductId("p3");
		posTxItem.setPosTransaction(posTransaction);
		posTxItem.setQuantity(7d);
		items.add(posTxItemRepo.save(posTxItem));
		
		posTransaction.setOrderItems(items);
		posTransactionRepo.save(posTransaction);
	}
	
	private void setUpPoints() {
		// 1 month ago
		DateTime date = DateTime.now().minusMonths(1);
		PointsTxnModel points = new PointsTxnModel();
		points.setId("txn1");
		points.setMemberModel(member);
		points.setTransactionType(PointTxnType.EARN);
		points.setTransactionPoints(1000D);
		points.setTransactionDateTime(date.toDate());
		points.setTransactionNo("txn1");
		points.setStatus(TxnStatus.ACTIVE);
		points.setTransactionContributer(member);
		pointsRepo.save(points);
		
		points = new PointsTxnModel();
		points.setId("txn2");
		points.setMemberModel(member);
		points.setTransactionType(PointTxnType.EARN);
		points.setTransactionPoints(2000D);
		points.setTransactionDateTime(date.toDate());
		points.setTransactionNo("txn2");
		points.setStatus(TxnStatus.ACTIVE);
		points.setTransactionContributer(member);
		pointsRepo.save(points);
		
		points = new PointsTxnModel();
		points.setId("txn3");
		points.setMemberModel(member);
		points.setTransactionType(PointTxnType.EARN);
		points.setTransactionPoints(3000D);
		points.setTransactionDateTime(date.toDate());
		points.setTransactionNo("txn3");
		points.setStatus(TxnStatus.ACTIVE);
		points.setTransactionContributer(member);
		pointsRepo.save(points);
	}
	
	@Test
	public void testGetAverageQuantityPerTransaction() {
		assertEquals(5D, posTransactionService.getAverageQuantityPerTransaction(member.getAccountId(), "p1").doubleValue(), 0D);
		assertEquals(4D, posTransactionService.getAverageQuantityPerTransaction(member.getAccountId(), "p2").doubleValue(), 0D);
		assertEquals(7D, posTransactionService.getAverageQuantityPerTransaction(member.getAccountId(), "p3").doubleValue(), 0D);
	}
	
	@Test
	public void testGetProductRankingsByTransaction() {
		// at least 2 transactions
		posTransactionService.setShoppingListMinTransactions(2);
		ResultList<ShoppingListDto> result = posTransactionService.getProductRankingsByTransaction(member.getAccountId());
		assertEquals(1, result.getNumberOfElements());
		for(ShoppingListDto dto : result.getResults()) {
			assertEquals("p1", dto.getProductId());
			assertEquals(3, dto.getTotalTransactions().intValue());
			assertEquals(15, dto.getTotalItemsBought().intValue());
		}
		
		// at least 1 transaction
		posTransactionService.setShoppingListMinTransactions(0);
		result = posTransactionService.getProductRankingsByTransaction(member.getAccountId());
		assertEquals(3, result.getTotalElements());
		
		ShoppingListDto[] results = result.getResults().toArray(new ShoppingListDto[3]);
		
		ShoppingListDto dto = results[0];
		assertEquals("p1", dto.getProductId());
		assertEquals(3, dto.getTotalTransactions().intValue());
		assertEquals(15, dto.getTotalItemsBought().intValue());
		
		dto = results[1];
		assertEquals("p2", dto.getProductId());
		assertEquals(2, dto.getTotalTransactions().intValue());
		assertEquals(8, dto.getTotalItemsBought().intValue());
		
		dto = results[2];
		assertEquals("p3", dto.getProductId());
		assertEquals(1, dto.getTotalTransactions().intValue());
		assertEquals(7, dto.getTotalItemsBought().intValue());
	}
	
	@Test
	public void testGetShoppingSuggestions() {
		PosTransaction posTransaction = new PosTransaction();
		posTransaction.setId("txn4");
		posTransaction = posTransactionRepo.save(posTransaction);
		List<PosTxItem> items = Lists.newArrayList();
		
		PosTxItem posTxItem = new PosTxItem();
		posTxItem.setId("newItem1");
		posTxItem.setProductId("p1");
		posTxItem.setPosTransaction(posTransaction);
		posTxItem.setQuantity(2D);
		items.add(posTxItemRepo.save(posTxItem));
		
		posTxItem = new PosTxItem();
		posTxItem.setId("newItem2");
		posTxItem.setProductId("p2");
		posTxItem.setPosTransaction(posTransaction);
		posTxItem.setQuantity(1D);
		items.add(posTxItemRepo.save(posTxItem));
		
		posTransaction.setOrderItems(items);
		posTransactionRepo.save(posTransaction);
		
		DateTime date = DateTime.now().minusDays(2);
		PointsTxnModel points = new PointsTxnModel();
		points.setId("txn4");
		points.setMemberModel(member);
		points.setTransactionType(PointTxnType.EARN);
		points.setTransactionPoints(1000D);
		points.setTransactionDateTime(date.toDate());
		points.setTransactionNo("txn4");
		points.setStatus(TxnStatus.ACTIVE);
		points.setTransactionContributer(member);
		pointsRepo.save(points);
		
		posTransaction = new PosTransaction();
		posTransaction.setId("txn5");
		posTransaction = posTransactionRepo.save(posTransaction);
		items = Lists.newArrayList();
		
		posTxItem = new PosTxItem();
		posTxItem.setId("newItem3");
		posTxItem.setProductId("p1");
		posTxItem.setPosTransaction(posTransaction);
		posTxItem.setQuantity(1D);
		items.add(posTxItemRepo.save(posTxItem));
		
		posTxItem = new PosTxItem();
		posTxItem.setId("newItem4");
		posTxItem.setProductId("p2");
		posTxItem.setPosTransaction(posTransaction);
		posTxItem.setQuantity(5D);
		items.add(posTxItemRepo.save(posTxItem));
		
		posTransaction.setOrderItems(items);
		posTransactionRepo.save(posTransaction);
		
		points = new PointsTxnModel();
		points.setId("txn5");
		points.setMemberModel(member);
		points.setTransactionType(PointTxnType.EARN);
		points.setTransactionPoints(1000D);
		points.setTransactionDateTime(date.toDate());
		points.setTransactionNo("txn5");
		points.setStatus(TxnStatus.ACTIVE);
		points.setTransactionContributer(member);
		pointsRepo.save(points);
		
		// txn4: p1 x 2, p2 x 1
		// txn5: p1 x 1, p2 x 5
		posTransactionService.setShoppingListMinTransactions(2);
		Map<String, ShoppingListDto> result = posTransactionService.getShoppingSuggestions(member.getAccountId());
		assertEquals(1, result.size());
		assertEquals(2D, result.get("p1").getAverage(), 0D);
		assertEquals("p1", result.get("p1").getProductName());
		
		posTransactionService.setShoppingListMinTransactions(0);
		result = posTransactionService.getShoppingSuggestions(member.getAccountId());
		assertEquals(3, result.size());
		assertEquals(2D, result.get("p1").getAverage(), 0D);
		assertEquals(0D, result.get("p2").getAverage(), 0D);
		assertEquals(7D, result.get("p3").getAverage(), 0D);
		assertEquals("p1", result.get("p1").getProductName());
		assertEquals("p2", result.get("p2").getProductName());
		assertEquals("p3", result.get("p3").getProductName());
	}
}
