package com.transretail.crm.core.service.impl;


import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.apache.commons.collections.CollectionUtils;
import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.mysema.query.jpa.impl.JPADeleteClause;
import com.transretail.crm.core.entity.CrmIdRef;
import com.transretail.crm.core.entity.QCrmIdRef;
import com.transretail.crm.core.repo.CrmIdRefRepo;
import com.transretail.crm.core.service.CrmIdRefService;
import com.transretail.crm.core.util.generator.IdGeneratorService;


@Service("idRefService")
@Transactional
public class CrmIdRefServiceImpl implements CrmIdRefService {

	public static enum IdRefType { POINT_EARNING_MEMBER };

	@Autowired
	CrmIdRefRepo repo;
	@Autowired
	IdGeneratorService customIdGeneratorService;

    @PersistenceContext
    private EntityManager em;
    @Value("#{'${hibernate.jdbc.batch_size}'}")
    private int batchSize;



	@Override
	public void saveEarningMember( Long modelId ) {
		if ( null == modelId ) {
			return;
		}
		save( modelId.toString(), IdRefType.POINT_EARNING_MEMBER.name() );
	}

	@Override
	public void deleteEarningMember( List<Long> ids ) {
		if ( CollectionUtils.isEmpty( ids ) ) {
			return;
		}
		List<String> modelIds = new ArrayList<String>();
		for ( Long id : ids ) {
			modelIds.add( id.toString() );
		}
		deleteAll( IdRefType.POINT_EARNING_MEMBER.name(), modelIds );
	}

	private CrmIdRef save( String modelId, String idType ) {
		CrmIdRef model = new CrmIdRef();
		model.setId( customIdGeneratorService.generateId( idType ) );
		model.setCreatedDate( new DateTime() );
		model.setIdType( idType );
		model.setModelId( modelId );
		return repo.save( model );
	}

	private long deleteAll( String idType, List<String> modelIds ) {
        QCrmIdRef qModel = QCrmIdRef.crmIdRef;
        JPADeleteClause query = new JPADeleteClause( em, qModel );
        List<String> batchModelIds = modelIds.subList( 0, ( modelIds.size() >= batchSize - 1? batchSize : modelIds.size() ) - 1 );
        if (batchModelIds.isEmpty()) {
            return query
                .where( qModel.idType.equalsIgnoreCase( idType ) )
                .execute();
        }
        return query
        		.where( qModel.idType.equalsIgnoreCase( idType ).and( qModel.modelId.in( batchModelIds ) ) )
        		.execute();
	}

}
