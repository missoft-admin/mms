package com.transretail.crm.report.template.impl;


import java.io.File;
import java.util.Locale;
import java.util.Map;

import javax.annotation.Resource;

import net.sf.jasperreports.engine.JRParameter;

import org.apache.commons.io.FileUtils;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.context.support.MessageSourceResourceBundle;

import com.google.common.collect.ImmutableMap;
import com.transretail.crm.common.reporting.exception.ReportException;
import com.transretail.crm.common.reporting.jasper.JRProcessor;
import com.transretail.crm.giftcard.dto.SalesOrderDto;


public class GiftCardSoHandoverDocumentTest extends AbstractPointSummaryReportTest {

    private static final Logger logger = LoggerFactory.getLogger(GiftCardSoHandoverDocumentTest.class);
    @Resource(name = "giftCardSoHandoverDocument")
    private GiftCardSoHandoverDocument soHandoverDocument;

    @Test @Override
    public void testCreateJrProcessor() throws ReportException {
    	SalesOrderDto dto = new SalesOrderDto();
    	dto.setId( 0000L );
    	dto.setOrderNo( "ORDER0000L" );
    	dto.setReceiptNo( "ORDER0000L" );
    	dto.setInvoiceTitle( "ORDER0000L" );
		final Map<String, String> param = new ImmutableMap.Builder<String, String>()
			.put( "orderNo", dto.getOrderNo() )
			.put( "receiptNo", dto.getReceiptNo() )
			.build();
	
		JRProcessor jrProcessor = soHandoverDocument.createReceiptJrProcessor( dto );
		File testClassesDir = FileUtils.toFile( Thread.currentThread().getContextClassLoader().getResource( "" ) );
		File output = new File(testClassesDir, "test-giftcard-so-handover-document.pdf");
		logger.debug( "Output location {}", output.getAbsolutePath() );
		output.delete();
		Locale locale = LocaleContextHolder.getLocale();
		jrProcessor.addParameter(JRParameter.REPORT_LOCALE, locale);
		jrProcessor.addParameter(JRParameter.REPORT_RESOURCE_BUNDLE, new MessageSourceResourceBundle( messageSourceRef, locale ) );
		reportService.exportPdfReport( jrProcessor, output );
	
		double bytes = output.length();
		double kilobytes = ( bytes / 1024 );
		logger.debug( "Kilobytes: {}", kilobytes );
    }

}
