drop table CRM_MEMBER_PETS;
drop table CRM_MEMBER_FOOD;
drop table CRM_MEMBER_INTERESTS;

alter table CRM_MEMBER add INTERESTS varchar(255);
alter table CRM_MEMBER_AUD add INTERESTS varchar(255);

delete from CRM_REF_LOOKUP_DTL where HDR = 'MEM015';
delete from CRM_REF_LOOKUP_DTL where HDR = 'MEM016';
delete from CRM_REF_LOOKUP_HDR where CODE = 'MEM015';
delete from CRM_REF_LOOKUP_HDR where CODE = 'MEM016';