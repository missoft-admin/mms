package com.transretail.crm.common.reporting.exception;

/**
 * @author Allan G. Ramirez (agramirez@exist.com)
 */
public class ReportException extends Exception {
    public ReportException(String message) {
        super(message);
    }

    public ReportException(Throwable e) {
        super(e);
    }

    public ReportException(String message, Throwable e) {
        super(message, e);
    }
}
