package com.transretail.crm.core.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.Type;
import org.hibernate.envers.Audited;
import org.joda.time.LocalDate;

import com.transretail.crm.core.entity.support.CustomAuditableEntity;

@Audited
@Table(name = "CRM_PROMOTION_REDEMPTION")
@Entity
public class PromotionRedemption extends CustomAuditableEntity<Long> implements Serializable {

    private static final long serialVersionUID = 1L;


    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "PROMO_ID")
    private Promotion promotion;

    @Column(name="SKU")
    private String sku;

    @Column(name = "REDEEM_FROM")
    @Type(type = "org.jadira.usertype.dateandtime.joda.PersistentLocalDate")
    private LocalDate redeemFrom;
    
    @Column(name = "REDEEM_TO")
    @Type(type = "org.jadira.usertype.dateandtime.joda.PersistentLocalDate")
    private LocalDate redeemTo;
    
    @Column(name = "QUANTITY") //points/stamp equivalent to redeem
    private Long qty;

	public Promotion getPromotion() {
		return promotion;
	}

	public void setPromotion(Promotion promotion) {
		this.promotion = promotion;
	}

	public String getSku() {
		return sku;
	}

	public void setSku(String sku) {
		this.sku = sku;
	}


	public LocalDate getRedeemFrom() {
		return redeemFrom;
	}

	public void setRedeemFrom(LocalDate redeemFrom) {
		this.redeemFrom = redeemFrom;
	}

	public LocalDate getRedeemTo() {
		return redeemTo;
	}

	public void setRedeemTo(LocalDate redeemTo) {
		this.redeemTo = redeemTo;
	}

	public Long getQty() {
		return qty;
	}

	public void setQty(Long qty) {
		this.qty = qty;
	}
    
	
}
