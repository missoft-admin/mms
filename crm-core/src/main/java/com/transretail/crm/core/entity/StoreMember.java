package com.transretail.crm.core.entity;

import com.transretail.crm.core.entity.support.CustomAuditableEntity;

import org.hibernate.envers.Audited;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.NotNull;

import java.io.Serializable;

/**
 *
 * the entity is de-normalize to simplify application in times ten environment
 *
 *
 */
@Audited
@Entity
@Table(name = "CRM_STORE_MEMBER")
public class StoreMember extends CustomAuditableEntity<Long> implements Serializable {
    private static final long serialVersionUID = 249531267345330184L;

    @NotNull
    @Column(name = "STORE", length = 10)
    private String store;
    @Column(name = "COMPANY", length = 10)
    private String company;
    @Column(name = "CARDTYPE", length = 10)
    private String cardType;

    @Column(name = "REMARKS", length = 255)
    private String remarks;

    public StoreMember() {

    }

    public StoreMember(Long id) {
        setId(id);
    }
    
    public String getStore() {
        return store;
    }

    public void setStore(String store) {
        this.store = store;
    }

    public String getCompany() {
        return company;
    }

    public void setCompany(String company) {
        this.company = company;
    }

    public String getCardType() {
        return cardType;
    }

    public void setCardType(String cardType) {
        this.cardType = cardType;
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }
}
