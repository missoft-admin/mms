package com.transretail.crm.media.service;


import com.transretail.crm.core.dto.MessageDto;


public interface MailService {

	boolean send(MessageDto dto);
	boolean sendWithHtml(MessageDto dto);
	boolean sendWithInlineFile(MessageDto dto);

}
