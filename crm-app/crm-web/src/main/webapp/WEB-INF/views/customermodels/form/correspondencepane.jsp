<%@ include file="../../common/taglibs.jsp"%>


<div class="clearfix"></div>

<div class="">
	<fieldset class="form-horizontal container-fluid span4">
	  <legend>
	    <spring:message code="member_prof_correspondenceaddress" />
	  </legend>
	  <c:forEach items="${correspondenceAddress}" var="item">
	  
	  <!-- <label for="marketingDetails.correspondenceAddress" class="control-label"></label> -->
	  <div class="radio controls">
	    <input type="radio" name="marketingDetails.correspondenceAddress" value="${item.code}"><label
	      class="">${item.description}</label>
	  </div>
	  
	  </c:forEach>
	</fieldset>


	<fieldset class="form-horizontal container-fluid span4">
	  <legend>
	    <spring:message code="member_prof_communicationmode" />
	  </legend>
	  
	  <!-- <label for="channel.acceptEmail" class="control-label"></label> -->
	  <!-- <label for="channel.acceptSms" class="control-label"></label> -->
	  <div class="checkbox controls">
	    <input type="checkbox" name="channel.acceptSMS"> <label
	      class=""><spring:message
	        code="label_com_transretail_crm_entity_customermodel_acceptsms" /></label>
	  </div>
	  <!-- <label for="channel.acceptMail" class="control-label"></label> -->
	  <div class="checkbox controls">
	    <input type="checkbox" name="channel.acceptEmail"> <label
	      class=""><spring:message
	        code="label_com_transretail_crm_entity_customermodel_acceptemail" /></label>
	  </div>
	  <!-- <label for="channel.acceptPhone" class="control-label"></label> -->
	  <div class="checkbox controls">
	    <input type="checkbox" name="channel.acceptPhone"> <label
	      class=""><spring:message
	        code="label_com_transretail_crm_entity_customermodel_acceptphone" /></label>
	  </div>
	</fieldset>

  <jsp:include page="segments/socialMedia.jsp" />
</div>

<div class="clearfix"></div>