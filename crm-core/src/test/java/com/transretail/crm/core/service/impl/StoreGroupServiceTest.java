package com.transretail.crm.core.service.impl;

import java.util.List;

import junit.framework.Assert;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Configurable;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import com.google.common.collect.Lists;
import com.transretail.crm.core.entity.lookup.Store;
import com.transretail.crm.core.entity.lookup.StoreGroup;
import com.transretail.crm.core.repo.StoreGroupRepo;
import com.transretail.crm.core.repo.StoreRepo;
import com.transretail.crm.core.service.StoreGroupService;

@Configurable
@Transactional
@ContextConfiguration(locations = "classpath*:/META-INF/spring/applicationContext*.xml")
@RunWith(SpringJUnit4ClassRunner.class)
public class StoreGroupServiceTest {
	
	@Autowired
	StoreGroupRepo storeGroupRepo;
	
	@Autowired
	StoreRepo storeRepo;
	
	@Autowired
	StoreGroupService storeGroupService;
	
	
	private Long strGrpId;
	
	@Before
	public void setup() {
		Store store = new Store();
		store.setId(new Integer(1));
		store.setCode("STR1");
		store.setName("STR1");
		storeRepo.saveAndFlush(store);
		
		store = new Store();
		store.setId(2);
		store.setCode("STR2");
		store.setName("STR2");
		storeRepo.saveAndFlush(store);
		
		store = new Store();
		store.setId(3);
		store.setCode("STR3");
		store.setName("STR3");
		storeRepo.saveAndFlush(store);
		
		StoreGroup storeGroup = new StoreGroup();
		storeGroup.setName("STRGRP1");
		storeGroup.setStores("STR1,STR2,STR3");
		strGrpId = storeGroupRepo.saveAndFlush(storeGroup).getId();
		
		storeGroup = new StoreGroup();
		storeGroup.setName("STRGRP2");
		storeGroup.setStores("STR2,STR3");
		storeGroupRepo.saveAndFlush(storeGroup);
	}
	
	@After
	public void teardown() {
		storeGroupRepo.deleteAll();
		storeRepo.deleteAll();
	}

	@Test
	public void testSearchItems() {
		List<Store> stores = Lists.newArrayList(storeGroupService.searchItems("STR100"));
		Assert.assertEquals(0, stores.size());
		
		stores = Lists.newArrayList(storeGroupService.searchItems("STR1"));
		Assert.assertEquals(1, stores.size());
		
		stores = Lists.newArrayList(storeGroupService.searchItems("STR*"));
		Assert.assertEquals(3, stores.size());
	}

	@Test
	public void testFindAll() {
		List<StoreGroup> storeGroups = storeGroupService.findAll();
		
		Assert.assertNotNull(storeGroups);
		Assert.assertEquals(2, storeGroups.size());
	}
	
	@Test
	public void testFindOne() {
		StoreGroup storeGroup = storeGroupService.findOne(strGrpId);
		
		Assert.assertNotNull(storeGroup);
		Assert.assertEquals("STRGRP1", storeGroup.getName());
		Assert.assertEquals("STR1,STR2,STR3", storeGroup.getStores());
	}
	

	
	@Test
	public void testGetItems() {
		List<Store> stores = storeGroupService.getItems(strGrpId);
		
		Assert.assertNotNull(stores);
		Assert.assertEquals(3, stores.size());
	}

    @Test
    public void testSaveStoreGroup() {
        Assert.assertEquals(2, storeGroupRepo.count());

        StoreGroup storeGroup = storeGroupRepo.findOne(strGrpId);
        storeGroup.setName("STRGRP3");
        storeGroup.setStores("STR1,STR3");
        storeGroupService.saveStoreGroup(storeGroup);

        Assert.assertEquals(2, storeGroupRepo.count());
    }
    
    @Test
    public void testCreateStoreGroup() {
    	 Assert.assertEquals(2, storeGroupRepo.count());

         StoreGroup storeGroup = new StoreGroup();
         storeGroup.setName("STRGRP3");
         storeGroup.setStores("STR1,STR3");
         storeGroupService.createStoreGroup(storeGroup);

         Assert.assertEquals(3, storeGroupRepo.count());
    }

    @Test
    public void testDelete() {
        Assert.assertEquals(2, storeGroupRepo.count());

        storeGroupService.delete(strGrpId);

        Assert.assertEquals(1, storeGroupRepo.count());
        Assert.assertNull(storeGroupRepo.findOne(strGrpId));
    }
    
    @Test
	public void testGetAllGroups() {
		List<StoreGroup> storeGrp = storeGroupService.getAllGroups();
		
		Assert.assertNotNull(storeGrp);
		Assert.assertEquals(2, storeGrp.size());
	}
}
