package com.transretail.crm.web.controller.admin;

import java.net.ConnectException;
import java.sql.SQLException;
import java.util.Arrays;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.StringUtils;
import org.apache.http.conn.ConnectTimeoutException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.servlet.ModelAndView;

import com.google.common.collect.Lists;
import com.transretail.crm.common.enums.MessageType;
import com.transretail.crm.common.reporting.jasper.ClasspathFileResolver;
import com.transretail.crm.common.reporting.jasper.DefaultJRProcessor;
import com.transretail.crm.common.service.dto.response.CodeDescDto;
import com.transretail.crm.common.service.dto.response.ResultList;
import com.transretail.crm.common.service.dto.rest.ReturnMessage;
import com.transretail.crm.common.service.exception.MessageSourceResolvableException;
import com.transretail.crm.common.web.controller.support.RequestUtils;
import com.transretail.crm.core.entity.ApplicationConfig;
import com.transretail.crm.core.entity.CrmInStore;
import com.transretail.crm.core.entity.enums.AppKey;
import com.transretail.crm.core.entity.enums.Status;
import com.transretail.crm.core.entity.lookup.LookupDetail;
import com.transretail.crm.core.entity.lookup.QLookupDetail;
import com.transretail.crm.core.repo.ApplicationConfigRepo;
import com.transretail.crm.core.repo.CrmInStoreRepo;
import com.transretail.crm.core.repo.LookupDetailRepo;
import com.transretail.crm.core.service.CodePropertiesService;
import com.transretail.crm.reconciliation.service.PointsToolService;
import com.transretail.crm.reconciliation.service.ReconciliationService;
import com.transretail.crm.rest.pos.response.dto.GenericResponseBean;
import com.transretail.crm.rest.pos.response.dto.MissingEmpTxnBean;
import com.transretail.crm.rest.pos.response.dto.MissingPointsBean;
import com.transretail.crm.rest.request.MissingEmpTxnSearchDto;
import com.transretail.crm.rest.request.MissingPointsSearchDto;
import com.transretail.crm.web.controller.AbstractReportsController;

import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;

/**
 * @author Allan G. Ramirez (agramirez@exist.com)
 */
@Controller
public class ReconciliationController extends AbstractReportsController {
    private static final Logger _LOG = LoggerFactory.getLogger(ReconciliationController.class);
    @Autowired
    private CrmInStoreRepo crmInStoreRepo;
    @Autowired
    private RestTemplate restTemplate;
    @Autowired
    private MessageSource messageSource;
    @Autowired
    private ReconciliationService reconciliationService;
    @Autowired
    private PointsToolService pointsToolService;
    @Value("#{'${reconciliation.port.override}'}")
    private String reconciliationPortOverride;
    @Autowired
    private CodePropertiesService codePropertiesService;
    @Autowired
    private LookupDetailRepo lookupDetailRepo;
    @Autowired
    private ApplicationConfigRepo applicationConfigRepo;

    @RequestMapping(value = "/crminstore/{crmInStoreId}/points/forreconciliation", method = RequestMethod.GET)
    public ModelAndView showPointsForReconcilationPage(@PathVariable("crmInStoreId") String crmInStoreId) {
    	CrmInStore crmInStore = crmInStoreRepo.findOne(crmInStoreId);
        return new ModelAndView("crminstore/missing/points")
        	.addObject("crmInStoreId", crmInStoreId)
        	.addObject("crmInStoreName", crmInStore.getName());
    }

    @RequestMapping(value = "/crminstore/{crmInStoreId}/points/forreconciliation", method = RequestMethod.POST)
    @ResponseBody
    public ResultList<MissingPointsBean> getPointsForReconciliation(@PathVariable("crmInStoreId") String crmInStoreId,
        @RequestBody final MissingPointsSearchDto dto) throws Exception {
        CrmInStore crmInStore = crmInStoreRepo.findOne(crmInStoreId);
        dto.setStoreCode(crmInStore.getStoreCode());
        return reconciliationService.getMissingIndProfPoints(dto);
    }

    @RequestMapping(value = "/crminstore/{crmInStoreId}/points/forreconciliation/export", method = RequestMethod.GET)
    public void exportPointsForReconciliation(@PathVariable("crmInStoreId") String crmInStoreId,
        @ModelAttribute MissingPointsSearchDto dto, HttpServletResponse response) throws Exception {
        CrmInStore crmInStore = crmInStoreRepo.findOne(crmInStoreId);
        dto.setStoreCode(crmInStore.getStoreCode());
        ResultList<MissingPointsBean> resultList = reconciliationService.getMissingIndProfPoints(dto);
        DefaultJRProcessor jrProcessor =
            new DefaultJRProcessor("reports/missingpoints.jasper", new JRBeanCollectionDataSource(
                resultList.getResults() != null && resultList.getResults().size() > 0 ? resultList.getResults() : Arrays
                    .asList(new MissingPointsBean())));
        jrProcessor.setFileResolver(new ClasspathFileResolver("reports"));

        renderExcelReport(response, jrProcessor, "missingpoints");
    }

    @RequestMapping(value = "/crminstore/{crmInStoreId}/emptxn/forreconciliation", method = RequestMethod.GET)
    public ModelAndView showEmptxnForReconciliationPage(@PathVariable("crmInStoreId") String crmInStoreId) {
    	CrmInStore crmInStore = crmInStoreRepo.findOne(crmInStoreId);
        return new ModelAndView("crminstore/missing/emptxn")
        		.addObject("crmInStoreId", crmInStoreId)
        		.addObject("paymentTypes", getAllPaymentTypes())
        		.addObject("crmInStoreName", crmInStore.getName());
    }

    @RequestMapping(value = "/crminstore/{crmInStoreId}/emptxn/forreconciliation", method = RequestMethod.POST)
    @ResponseBody
    public ResultList<MissingEmpTxnBean> getEmptxnForReconciliation(@PathVariable("crmInStoreId") String crmInStoreId,
        @RequestBody MissingEmpTxnSearchDto dto) throws Exception {
        CrmInStore crmInStore = crmInStoreRepo.findOne(crmInStoreId);
        dto.setStoreCode(crmInStore.getStoreCode());
        ApplicationConfig card = applicationConfigRepo.findByKey(AppKey.RECONCILE_CARD);
        ApplicationConfig media = applicationConfigRepo.findByKey(AppKey.RECONCILE_MEDIA);
        return reconciliationService.getMissingEmpTxns(dto, card.getValue(), media.getValue());
    }

    @RequestMapping(value = "/crminstore/{crmInStoreId}/emptxn/forreconciliation/export", method = RequestMethod.GET)
    public void exportEmptxnForReconciliation(@PathVariable("crmInStoreId") String crmInStoreId,
        @ModelAttribute MissingEmpTxnSearchDto dto, HttpServletResponse response) throws Exception {
        CrmInStore crmInStore = crmInStoreRepo.findOne(crmInStoreId);
        dto.setStoreCode(crmInStore.getStoreCode());
        ApplicationConfig card = applicationConfigRepo.findByKey(AppKey.RECONCILE_CARD);
        ApplicationConfig media = applicationConfigRepo.findByKey(AppKey.RECONCILE_MEDIA);
        ResultList<MissingEmpTxnBean> resultList = reconciliationService.getMissingEmpTxns(dto, card.getValue(), media.getValue());
        DefaultJRProcessor jrProcessor =
            new DefaultJRProcessor("reports/missingemptxn.jasper", new JRBeanCollectionDataSource(
                resultList.getResults() != null && resultList.getResults().size() > 0 ? resultList.getResults() : Arrays
                    .asList(new MissingEmpTxnBean())));
        jrProcessor.setFileResolver(new ClasspathFileResolver("reports"));
        renderExcelReport(response, jrProcessor, "missingemtxn");
    }

    @RequestMapping(value = "/reconcile/{txnNo}/in/crmstore/{crmInStoreId}", method = RequestMethod.GET)
    @ResponseBody
    public GenericResponseBean reconcileTransactionNumber(@PathVariable("crmInStoreId") String crmInStoreId,
        @PathVariable("txnNo") String txnNo, HttpServletRequest request) throws Exception {

        return reconcileTransaction(crmInStoreId, txnNo, request);
    }

    @RequestMapping(value = "/reconcilee/{txnNo}/in/crmstore/{crmInStoreId}", method = RequestMethod.GET)
    @ResponseBody
    public GenericResponseBean reconcileTransactionNumber2(@PathVariable("crmInStoreId") String crmInStoreId,
                                                          @PathVariable("txnNo") String txnNo, HttpServletRequest request) throws Exception {

        return reconcileTransaction(crmInStoreId, txnNo, request);
    }

    private GenericResponseBean reconcileTransaction(String crmInStoreId, String txnNo, HttpServletRequest request) {
        GenericResponseBean responseBean = new GenericResponseBean();
        _LOG.info("[Reconcile Transaction Number] {}.", txnNo);
        try {
            String earnWsUrl = pointsToolService.createEarnWsUrl(txnNo);
            if (_LOG.isDebugEnabled()) {
                _LOG.debug("[Reconcile Transaction Number] WSURL: {}.", earnWsUrl);
            }
            String crmProxyRestWsPrfx = RequestUtils.INSTANCE.getRootUrl(request) + "/api";
            try {
                // Change /api if crm-proxy is no longer bound to /api context path
                ReturnMessage returnMessage =
                    restTemplate.getForObject(crmProxyRestWsPrfx + earnWsUrl, ReturnMessage.class);
                if (returnMessage.getType() == MessageType.ERROR) {
                    _LOG.error("[Reconcile Transaction Number] {} Error - {}", txnNo, returnMessage.getMessage());
                    responseBean.setErrorMessage("Failed to reconcile " + txnNo + ". " + returnMessage.getMessage());
                } else {
                    _LOG.info("[Reconcile Transaction Number] {} Success - {}", txnNo, returnMessage.getMessage());
                    responseBean.setSuccessMessage(returnMessage.getMessage());
                }
            } catch (RestClientException e) {
                if (e.getRootCause() instanceof ConnectTimeoutException) {
                    _LOG.error("[Reconcile Transaction Number] Failed to invoke {}. Please check the connection.",
                        crmProxyRestWsPrfx + earnWsUrl);
                    responseBean.setException(
                        new MessageSourceResolvableException("crm.proxy.down", new String[]{txnNo, e.getMessage()},
                            "Failed to reconcile " + txnNo + ". CRM Proxy is down")
                    );
                } else {
                    _LOG.error("[Reconcile Transaction Number] {}", e.getMessage());
                    responseBean.setException(
                        new MessageSourceResolvableException("crm.proxy.earn.fail", new String[]{txnNo, e.getMessage()},
                            "Failed to reconcile " + txnNo + ". " + e.getMessage())
                    );
                }
            }
        } catch (SQLException e) {
            _LOG.error("[Reconcile Transaction Number] " + txnNo + " Error - " + e.getMessage(), e);
            responseBean.setException(
                new MessageSourceResolvableException("reconcile.fail", new String[]{txnNo, e.getMessage()},
                    "Failed to reconcile " + txnNo + ". " + e.getMessage())
            );
        }
        return responseBean;
    }

    @RequestMapping(value = "/sync/reference/records/in/crmstore/{crmInStoreId}", method = RequestMethod.GET)
    @ResponseBody
    public GenericResponseBean syncRefRecords(@PathVariable("crmInStoreId") String crmInStoreId,
        HttpServletRequest request) {
        GenericResponseBean result = null;
        CrmInStore crmInStore = crmInStoreRepo.findOne(crmInStoreId);
        String url = getCrmReconciliationWsUrl(crmInStore) + "/sync/reference/records";
        try {
            result = restTemplate.getForObject(url, GenericResponseBean.class);
        } catch (RestClientException e) {
            result = new GenericResponseBean();
            String message = e.getRootCause() != null ? e.getRootCause().getMessage() : e.getMessage();
            _LOG.info("[CRM RECONCILIATION] Failed to invoke {}. Reason: {}", url, message);
            if (e.getRootCause() instanceof ConnectException) {
                result.setErrorMessage(messageSource.getMessage("crm.proxy.connect.timeout",
                        new String[]{crmInStore.getName()}, request.getLocale())
                );
            } else {
                result.setErrorMessage(messageSource.getMessage("crm.reconciliation.ws.call.fail",
                        new String[]{crmInStore.getName()}, request.getLocale())
                );
            }
        }
        return result;
    }

    @RequestMapping(value = "/sync/transaction/records/in/crmstore/{crmInStoreId}", method = RequestMethod.GET)
    @ResponseBody
    public GenericResponseBean syncTransactionRecords(@PathVariable("crmInStoreId") String crmInStoreId,
        HttpServletRequest request) {
        GenericResponseBean result = null;
        CrmInStore crmInStore = crmInStoreRepo.findOne(crmInStoreId);
        String url = getCrmReconciliationWsUrl(crmInStore) + "/sync/transaction/records";
        try {
            result = restTemplate.getForObject(url, GenericResponseBean.class);
        } catch (RestClientException e) {
            result = new GenericResponseBean();
            String message = e.getRootCause() != null ? e.getRootCause().getMessage() : e.getMessage();
            _LOG.info("[CRM RECONCILIATION] Failed to invoke {}. Reason: {}", url, message);
            if (e.getRootCause() instanceof ConnectException) {
                result.setErrorMessage(messageSource.getMessage("crm.proxy.connect.timeout",
                        new String[]{crmInStore.getName()}, request.getLocale())
                );
            } else {
                result.setErrorMessage(messageSource.getMessage("crm.reconciliation.ws.call.fail",
                        new String[]{crmInStore.getName()}, request.getLocale())
                );
            }
        }
        return result;
    }

    public String getCrmReconciliationWsUrl(CrmInStore crmInStore) {
        StringBuilder stringBuilder = new StringBuilder("http://");
        stringBuilder.append(crmInStore.getIp().trim());
        stringBuilder.append(StringUtils.isNotBlank(reconciliationPortOverride) ? reconciliationPortOverride
            : crmInStore.getTomcatPort());
        stringBuilder.append(crmInStore.getCrmReconciliationRestPrefix().trim());
        return stringBuilder.toString();
    }

    private List<CodeDescDto> getAllPaymentTypes() {
        List<CodeDescDto> result = Lists.newArrayList();
        QLookupDetail qLookupDetail = QLookupDetail.lookupDetail;
        for (LookupDetail lookupDetail : lookupDetailRepo
            .findAll(qLookupDetail.status.eq(Status.ACTIVE).and(
                    qLookupDetail.header.code.eq(codePropertiesService.getHeaderPaymentType())
                        .or(qLookupDetail.header.code.eq(codePropertiesService.getHeaderPaymentTypeWildcard()))
                )
            )) {
            result.add(new CodeDescDto(lookupDetail.getCode(), lookupDetail.getDescription()));
        }
        return result;
    }
}
