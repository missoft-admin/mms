package com.transretail.crm.core.util;


import java.util.Date;
import java.util.List;

import org.joda.time.DateTime;
import org.joda.time.LocalTime;
import org.springframework.util.StringUtils;

import com.mysema.query.types.expr.BooleanExpression;
import com.mysema.query.types.path.DateTimePath;
import com.mysema.query.types.path.EnumPath;
import com.mysema.query.types.path.NumberPath;
import com.mysema.query.types.path.StringPath;
import com.mysema.query.types.path.TimePath;


public enum BooleanExprUtil {

	INSTANCE;

	public BooleanExpression isStringPropertyIn(StringPath inStrPath, List<String> inAcctIds) {
		return !StringUtils.isEmpty( inAcctIds ) ? inStrPath.in( inAcctIds ) : null;
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	public BooleanExpression isDatePropertyEqual(DateTimePath inPath, Date inProp) {
		return inProp != null ? ( inPath.eq( inProp ) ) : null;
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	public BooleanExpression isDatePropertyGoe(DateTimePath inPath, Date inProp) {
		return inProp != null ? ( inPath.goe( inProp ) ) : null;
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	public BooleanExpression isDatePropertyLoe(DateTimePath inPath, Date inProp) {
		return inProp != null ? ( inPath.loe( inProp ) ) : null;
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	public BooleanExpression isDateTimePropertyGoe(DateTimePath inPath, Date inProp) {
		return inProp != null ? ( inPath.goe( new DateTime( inProp ) ) ) : null;
	}
	
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public BooleanExpression isTimePropertyLoe(TimePath inPath, LocalTime inProp) {
		return inProp != null ? ( inPath.loe( inProp ) ) : null;
	}
	
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public BooleanExpression isTimePropertyGoe(TimePath inPath, LocalTime inProp) {
		return inProp != null ? ( inPath.goe( inProp ) ) : null;
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	public BooleanExpression isDateTimePropertyLoe(DateTimePath inPath, Date inProp) {
		return inProp != null ? ( inPath.loe( new DateTime( inProp ) ) ) : null;
	}

	public BooleanExpression isStringPropertyEqual(StringPath inStrPath, String inProp) {
		return !StringUtils.isEmpty(inProp) ? inStrPath.equalsIgnoreCase( inProp ) : null;
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	public BooleanExpression isNumberPropertyEqual(NumberPath inStrPath, Long inProp) {
		return !StringUtils.isEmpty(inProp) ? inStrPath.eq( inProp ) : null;
	}

	public BooleanExpression isStringPropertyLike(StringPath inStrPath, String inProp) {
		return !StringUtils.isEmpty(inProp) ? inStrPath.lower().contains( inProp.toLowerCase() ) : null;
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	public BooleanExpression isEnumPropertyIn(EnumPath inEnumPath, Enum inProp) {
		return inProp != null ? inEnumPath.in(inProp) : null;
	}
}
