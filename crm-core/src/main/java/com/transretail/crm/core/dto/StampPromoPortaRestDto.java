package com.transretail.crm.core.dto;


import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.apache.commons.lang3.StringUtils;

import com.transretail.crm.core.entity.Promotion;
import com.transretail.crm.core.service.impl.StampPromoServiceImpl;


public class StampPromoPortaRestDto {

	private String promoId;
	private Date promoStart;
	private Date promoEnd;
	private String md5ValidationKey;



	public StampPromoPortaRestDto() {}
	public StampPromoPortaRestDto( Promotion promo, String dateFormat ) {
		this.promoId = promo.getId().toString();
		this.promoStart = promo.getStartDate();
		this.promoEnd = promo.getEndDate();
		DateFormat df = new SimpleDateFormat( StringUtils.isNotBlank( dateFormat )? dateFormat : StampPromoServiceImpl.MD5_DATEFORMAT );
		this.md5ValidationKey = promo.getId() + df.format( this.promoStart ) + df.format( this.promoEnd ); //<promoID><startDateYYYYMMDD><endDateYYYYMMDD>
	}



	public String getPromoId() {
		return promoId;
	}
	public void setPromoId(String promoID) {
		this.promoId = promoID;
	}
	public Date getPromoStart() {
		return promoStart;
	}
	public void setPromoStart(Date promoStart) {
		this.promoStart = promoStart;
	}
	public Date getPromoEnd() {
		return promoEnd;
	}
	public void setPromoEnd(Date promoEnd) {
		this.promoEnd = promoEnd;
	}
	public String getMd5ValidationKey() {
		return md5ValidationKey;
	}
	public void setMd5ValidationKey(String md5ValidationKey) {
		this.md5ValidationKey = md5ValidationKey;
	}

}
