<%@include file="../../common/taglibs.jsp"%>


<div id="content_psoftStore" class="">

  <div class="page-header page-header2"><h1><spring:message code="store.psoft.mapping" /></h1></div>

  <%-- <div id="form_psoftStore" data-url="" class="well clearfix">
    <c:url var="url_save" value="/store/peoplesoft/save" />
    <form:form id="psoftStoreForm" name="psoftStore" modelAttribute="psoftStore" method="POST" action="${url_save}" class="">
      <div class="hide alert alert-error" id="content_error"><button type="button" class="close" onclick="$(this).parent().hide();">&times;</button><div><form:errors   path="*"/></div></div>
      <div>
        <div class="pull-left input-group"><spring:message var="msg_peoplesoftCode" code="store.psoft.code" />
          <div><label>${msg_peoplesoftCode}</label></div><div><form:input path="peoplesoftCode" placeholder="${msg_peoplesoftCode}" /></div>
        </div>
        <div class="pull-left input-group"><spring:message var="msg_storeCode" code="store.psoft.store" />
          <div><label>${msg_storeCode}</label></div><div><form:select path="storeCode" items="${stores}" itemValue="code" itemLabel="codeAndName" /></div>
        </div>
        <div class="pull-left input-group"><spring:message var="msg_buCode" code="store.psoft.bu" />
          <div><label>${msg_buCode}</label></div><div><form:select path="buCode" items="${businessUnits}" itemValue="code" itemLabel="description" /></div>
        </div>
        <div class="pull-left form-buttons">
          <button  id="saveBtn" class="btn btn-primary" data-url="" ><spring:message code="label_save" /></button>
          <button  id="resetBtn" class="btn" data-url="" ><spring:message code="label_reset" /></button>
        </div>
      </div>  
    </form:form>
  </div> --%>

  <div id="form_psoftStore" class="modal hide  nofly modal-dialog">
    <div class="modal-content">
    <form:form id="psoftStoreForm" name="psoftStore" modelAttribute="psoftStore" method="POST" class="modal-form form-horizontal">
	    <div class="modal-header">
	      <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
	      <h4 id="header" ><spring:message code="store.psoft.lbl.add" /></h4>
	    </div>
	    <div class="modal-body">
	      <div class="hide alert alert-error" id="content_error"><button type="button" class="close" onclick="$(this).parent().hide();">&times;</button><div><form:errors   path="*"/></div></div>
	      <div>
	        <div class="control-group"><spring:message var="msg_peoplesoftCode" code="store.psoft.code" />
	          <label class="control-label"><b class="required">*</b> ${msg_peoplesoftCode}</label>
	          <div><form:input path="peoplesoftCode" placeholder="${msg_peoplesoftCode}" /></div>
	        </div>
	        <div class="control-group"><c:url var="storeUrl" value="/store/peoplesoft/store/list" />
	          <label class="control-label"><b class="required">*</b> <spring:message code="store.psoft.store" /></label>
	          <div><form:select path="storeId" items="${stores}" itemValue="id" itemLabel="codeAndName"
	            data-url="${storeUrl}" /></div>
	        </div>
	        <div class="control-group">
	          <label class="control-label"><b class="required">*</b> <spring:message code="store.psoft.bu" /></label>
	          <div><form:select path="buCode" items="${businessUnits}" itemValue="code" itemLabel="description" /></div>
	        </div>
          <div class="control-group">
            <label class="control-label"><spring:message code="store.psoft.br" /></label>
            <div><form:select path="brCode" items="${businessRegions}" itemValue="code" itemLabel="description" /></div>
          </div>
          <div class="control-group">
            <label class="control-label"><spring:message code="store.psoft.allowgctxn" /></label>
            <div class="controls"><form:checkbox path="allowGcTxn" id="allowGcTxn" /></div>
          </div>
	      </div>
	    </div>
      <div class="modal-footer">
        <button id="saveBtn" class="btn btn-primary" ><spring:message code="label_save" /></button>
        <button id="cancelBtn" data-dismiss="modal" class="btn btn-default"><spring:message code="label_cancel"/></button>
      </div>
    </form:form>
    </div>
  </div>

  <div class="">
    <div class="pull-right mb20">
      <a id="addBtn" href="<c:url value="/store/peoplesoft" />" data-action="<c:url value="/store/peoplesoft/save" />" 
          class="btn btn-primary"><spring:message code="store.psoft.lbl.add" /></a>
    </div>
    <div class="clearfix"></div>
  </div>

  <div id="list_psoftStore" 
      data-url="<c:url value="/store/peoplesoft/list" />" 
      data-hdr-storecode="<spring:message code="store.psoft.storecode"/>" 
      data-hdr-storename="<spring:message code="store.psoft.storename"/>" 
      data-hdr-psoftcode="<spring:message code="store.psoft.code"/>" 
      data-hdr-bu="<spring:message code="store.psoft.bu"/>" 
      data-hdr-br="<spring:message code="store.psoft.br"/>" 
      data-hdr-allowgctxn="<spring:message code="store.psoft.allowgctxn"/>" ></div>

  <div id="btns" class="hide">
    <a href="<c:url value="/store/peoplesoft/delete/%ID%" />" class="btn pull-left tiptip icn-delete deleteBtn" 
      title="<spring:message code="label_delete" />" data-msg-confirm="<spring:message code="store.psoft.msg.delete" />" >
      <spring:message code="label_delete" /></a>
    <a href="<c:url value="/store/peoplesoft/edit/%ID%" />" class="btn pull-left tiptip icn-edit editBtn" 
      title="<spring:message code="label_edit" />" data-action="<c:url value="/store/peoplesoft/update/%ID%" />" >
      <spring:message code="store.psoft.lbl.edit" /></a>
  </div>

  <jsp:include page="../../common/confirm.jsp" />


<style type="text/css"> 
<!-- 
  #content_psoftStore .form-buttons { padding-top: 20px !important; margin-left: 10px; }
  #content_psoftStore .input-group { margin-right: 5px; }
  .modal-footer { text-align: center; } 
--> 
</style>
<link href="<spring:url value="/css/dataTables.css" />" rel="stylesheet">
<script src="<spring:url value="/js/jquery/jquery.dataTables.js" />" ></script>
<script src="<spring:url value="/js/jquery/jquery.dataTables.linkLengthMenu.js" />"></script>
<script src="<spring:url value="/js/jquery/jquery.dataTables.customPaging.js" />"></script>
<script src="<spring:url value="/js/jquery/ajax-datatable.js" />" ></script>
<script src="<spring:url value="/js/common/form2js.js" />" ></script>
<script src="<spring:url value="/js/common/json2.js" />" ></script>
<script src="<spring:url value="/js/jquery/jquery.toObject.js" />" ></script>
<script type='text/javascript'>
var PsoftStore = null;

$(document).ready( function() {

    var errorContainer = $( "#content_psoftStore" ).find( "#content_error" );
    var $container = $( "#content_psoftStore" );
    var $list = $container.find( "#list_psoftStore" );
    var $btns = $container.find( "#btns" );
    var $add = $container.find( "#addBtn" );
    var $form = $container.find( "#psoftStoreForm" );
    var $save = $form.find( "#saveBtn" );
    var $reset = $form.find( "#resetBtn" );
    var $store = $form.find( "#storeId" );

    var $dialog = $container.find( "#form_psoftStore" );

    PsoftStore = {
        reset       : function( ele, isEmpty ) {
            $( ele ).find(':input').each( function() {
                switch(this.type) {
                    case 'password':
                    case 'select':
                    case 'select-one':
                    case 'text':
                    case 'textarea':
                    case 'file': 
                    case 'hidden': $(this).val(''); break;
                    case 'select-multiple': isEmpty? $(this).empty() : $(this).val(""); break;
                    case 'checkbox':
                    case 'radio': this.checked = false;
                }
            });
        },
        set           : function( ele, result ) {
        	  $.each( result, function( key, val ) {
        		    $(ele).find( "[name='" + key + "']").each( function() {
                    switch(this.type) { 
                        case 'password':
                        case 'select':
                        case 'select-one':
                        case 'text':
                        case 'textarea':
                        case 'file': 
                        case 'hidden': 
                        case 'select-multiple': $(this).val( val ); break;
                        case 'checkbox':
                        case 'radio': this.checked = val;
                    }
                });
        	  });
        },
        refreshList   : function() { $list.ajaxDataTable( "search" ); },
        updateStores  : function() {
        	  $.get( $store.data( "url" ), function( resp ) {
        		    $store.empty();
        		    $.each( resp, function( idx ) { $store.append( new Option( this.codeAndName, this.id ) ); });
        		    $store.val("");
            }, "json" );
        }
    };

    initDialog();
    initDataTable();
    initBtnForms();

    function initDialog() {
        $dialog.modal({ show : false });
        $dialog.on( "hide", function(e) { 
          if ( e.target === this ) { 
            errorContainer.hide();
            PsoftStore.updateStores();
            PsoftStore.reset( $form );
          }
        });
        PsoftStore.reset( $form );
    }

    function initDataTable() {
        $list.ajaxDataTable({
        	  /* 'fnCreatedRow'  : function( nRow, aData, iDataIndex ) { $(nRow).attr( 'id', aData[0] ); }, */
            'autoload'      : true,
            'ajaxSource'    : $list.data( "url" ),
            'aaSorting'     : [[ 0, 'desc' ]],
            'columnHeaders' : [ 
                $list.data( "hdr-storecode" ),
                $list.data( "hdr-storename" ),
                $list.data( "hdr-psoftcode" ),
                $list.data( "hdr-bu" ),
                $list.data( "hdr-br" ),
                $list.data( "hdr-allowgctxn" ),
                { text: "", className : "span5" }
            ],
            'modelFields'  : [
                { name  : 'store', sortable : true, fieldCellRenderer : function (data, type, row) {
                    return (row.storeCode)? row.storeCode : "";
                }},
                { name  : 'store', sortable : true, fieldCellRenderer : function (data, type, row) {
                    return (row.storeName)? row.storeName : "";
                }},
                { name  : 'peoplesoftCode', sortable : true },
                { name  : 'businessUnit', sortable : true, fieldCellRenderer : function (data, type, row) {
                    return (row.buName)? row.buName : "";
                }},
                { name  : 'businessRegion', sortable : true, fieldCellRenderer : function (data, type, row) {
                    return (row.brName)? row.brName : "";
                }},
                { name  : 'allowGcTxn', sortable : true },
                { customCell : 
                    function ( innerData, sSpecific, json ) {
                        var btns = "";
                        if ( sSpecific == 'display' ) {
                            btns += $btns.find( ".editBtn" ).prop( "outerHTML" ).replace( new RegExp( "%ID%", "g" ), json.id );
                            btns += $btns.find( ".deleteBtn" ).prop( "outerHTML" ).replace( new RegExp( "%ID%", "g" ), json.id );
                        }
                        return btns;
                    }
                }
            ]
        })
        .on( "click", ".deleteBtn", function(e) {
        	  e.preventDefault();
        	  var $this = $(this);
        	  getConfirm( $this.data( "msg-confirm" ), function( result ) { 
                if( result ) { $.post( $this.attr( "href" ), function( resp ) { $list.ajaxDataTable( "search" ); }, "json" ); }
            });
            
        })
        .on( "click", ".editBtn", function(e) {
            e.preventDefault();
            var $this = $(this);
            $.post( $this.attr( "href" ), function( resp ) {
                if ( resp.success ) {
                	  var res = resp.result;
                	  if( res.storeCode ) {
                        $store.append( new Option( res.storeCode + " - " + res.storeName, res.storeId ) )
                	  }
                	  PsoftStore.set( $form, res ); 
                }
            }, "json" );
            $dialog.find( "#header" ).html( $(this).html() );
            $form.attr( "action", $this.data( "action" ) );
            $dialog.modal( "show" );
        });
        /* var ctr = 1; $("#ADDROW").click( function() { $list.ajaxDataTable( "addData", [ ctr+".1", ctr+".2", ctr+".3", ctr+".4" ] ); } ); */
    }

    function initBtnForms() {
        $add.click( function(e) {
            e.preventDefault();
            $dialog.find( "#header" ).html( $(this).html() );
            $form.attr( "action", $add.data( "action" ) );
            $dialog.modal( "show" );
        });
    	  $save.click( function(e) {
    		    e.preventDefault();
    		    $.post( $form.attr( "action" ), $form.serialize(), function( resp ) {
    		    	  if ( resp.success ) {
    		    		    $list.ajaxDataTable( "search" );
    		            $dialog.modal( "hide" );
    		    	  }
    		    	  else {
 		                var errors = "";
 		                $( resp.result ).each( function( key, value ) { errors += ( ( key + 1 ) + ". " + value.code + "<br>" ); });
 		                errorContainer.find( "div" ).html( errors );
 		                errorContainer.show( "slow" );
    		    	  }
   		      }, "json" );
    	  });
    }

});
</script>
</div>