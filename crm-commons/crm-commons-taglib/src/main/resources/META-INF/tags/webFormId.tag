<%@ tag import="com.transretail.crm.common.web.dto.WebForm" %>
<%@ tag import="com.transretail.crm.common.web.ext.WebFormSessionAttributeStoreUtil" %>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<%@ attribute name="commandName" required="true" rtexprvalue="false" %>

<c:set var="webFormPrefix"><%= WebFormSessionAttributeStoreUtil.WEB_FORM_ID_PREFIX%>
</c:set>
<c:set var="webFormSuffix"><%= WebFormSessionAttributeStoreUtil.WEB_FORM_ID_SUFFIX%>
</c:set>
<c:set var="webFormId"><%=((WebForm) request.getAttribute(commandName)).getWebFormInfo().getId()%>
</c:set>
<input type="hidden" name="${webFormPrefix}${commandName}${webFormSuffix}" value="${webFormId}"/>
