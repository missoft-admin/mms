package com.transretail.crm.web.controller;

import com.google.common.collect.Lists;
import com.transretail.crm.common.reporting.service.ReportService;
import com.transretail.crm.core.service.*;
import com.transretail.crm.core.util.generator.IdGeneratorService;
import com.transretail.crm.loyalty.dto.LoyaltyPhysicalCountDto;
import com.transretail.crm.loyalty.service.*;
import com.transretail.crm.media.service.MailService;
import com.transretail.crm.report.template.impl.LoyaltyTransferDocument;
import java.util.List;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.MediaType;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.AnnotationConfigWebContextLoader;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

import static org.hamcrest.Matchers.*;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 *
 * @author Monte Cillo Co (mco@exist.com)
 */
@RunWith(SpringJUnit4ClassRunner.class)
@WebAppConfiguration
@ContextConfiguration(loader = AnnotationConfigWebContextLoader.class)
public class LoyaltyCardInventoryControllerTest extends AbstractMockMvcTest {

    @Autowired
    private LoyaltyPhysicalCountService loyaltyPhysicalCountService;

    @Before
    public void setup() throws Exception {
	reset(loyaltyPhysicalCountService);
    }

    @Test
    public void validateBarcodeWithNoErrors() throws Exception {
	String barcode = "12323452345523452345";
	when(loyaltyPhysicalCountService.validateSeries(anyString(), anyString()))
		.thenReturn(Boolean.TRUE);

	mockMvc.perform(get("/loyaltycardinventory/validate/{barcode}", barcode))
		.andExpect(status().isOk())
		.andExpect(jsonPath("$.success", is(equalTo(Boolean.TRUE))))
		.andDo(print());

	verify(loyaltyPhysicalCountService).validateSeries(anyString(), anyString());
    }

    @Test
    public void validateBarcodeWithErrors() throws Exception {
	String barcode = "12323452345523452345";
	when(loyaltyPhysicalCountService.validateSeries(anyString(), anyString()))
		.thenReturn(Boolean.FALSE);

	mockMvc.perform(get("/loyaltycardinventory/validate/{barcode}", barcode))
		.andExpect(status().isOk())
		.andExpect(jsonPath("$.success", is(equalTo(Boolean.FALSE))))
		.andExpect(jsonPath("$.result[*]", hasSize(1)))
		.andDo(print());

	verify(loyaltyPhysicalCountService).validateSeries(anyString(), anyString());
    }

    @Test
    public void savePhysicalCount_bySeriesExpectSuccess() throws Exception {
	when(loyaltyPhysicalCountService.validateSeries(anyString(), anyString()))
		.thenReturn(Boolean.TRUE);
	mockMvc.perform(post("/loyaltycardinventory/physicalcount")
		.contentType(MediaType.APPLICATION_FORM_URLENCODED)
		.param("location", "HO")
		.param("cardType", "")
		.param("startingSeries", "010100000001")
		.param("endingSeries", "010100000010")
		.param("quantity", "12")
		.param("boxNo", "INDVIDUAL"))
		.andExpect(status().isOk())
		.andExpect(jsonPath("$.success", is(true)))
		.andDo(print());

	verify(loyaltyPhysicalCountService, atLeastOnce())
		.savePhysicalCount(Mockito.any(LoyaltyPhysicalCountDto.class));
    }

    @Test
    public void savePhysicalCount_bySeriesExpectFailure_seriesLengthIsNot12() throws Exception {
	mockMvc.perform(post("/loyaltycardinventory/physicalcount")
		.contentType(MediaType.APPLICATION_FORM_URLENCODED)
		.param("location", "HO")
		.param("cardType", "")
		.param("startingSeries", "0101000000011")
		.param("endingSeries", "0101000000101")
		.param("quantity", "12")
		.param("boxNo", "INDVIDUAL"))
		.andExpect(status().isOk())
		.andExpect(jsonPath("$.success", is(false)))
		.andExpect(jsonPath("$.result", hasSize(1)))
		.andDo(print());

	verifyZeroInteractions(loyaltyPhysicalCountService);
    }

    @Test
    public void savePhysicalCount_bySeriesExpectFailure_seriesValidation() throws Exception {
	when(loyaltyPhysicalCountService.validateSeries(anyString(), anyString()))
		.thenReturn(Boolean.FALSE);

	mockMvc.perform(post("/loyaltycardinventory/physicalcount")
		.contentType(MediaType.APPLICATION_FORM_URLENCODED)
		.param("location", "HO")
		.param("cardType", "")
		.param("startingSeries", "010100000001")
		.param("endingSeries", "010100000010")
		.param("quantity", "12")
		.param("boxNo", "INDVIDUAL"))
		.andExpect(status().isOk())
		.andExpect(jsonPath("$.success", is(false)))
		.andExpect(jsonPath("$.result", hasSize(1)))
		.andDo(print());

	verify(loyaltyPhysicalCountService).validateSeries(anyString(), anyString());
	verifyZeroInteractions(loyaltyPhysicalCountService);
    }

    @Test
    public void savePhysicalCount_byBarcodeExpectSuccess() throws Exception {
	when(loyaltyPhysicalCountService.validateSeries(anyString(), anyString()))
		.thenReturn(Boolean.TRUE);

	List<String> barcodes = Lists.newArrayList("010100000001", "010100000001", "010100000001");

	mockMvc.perform(post("/loyaltycardinventory/physicalcount")
		.contentType(MediaType.APPLICATION_FORM_URLENCODED)
		.param("location", "HO")
		.param("cardType", "")
		.param("barcodes", barcodes.toArray(new String[]{})))
		.andExpect(status().isOk())
		.andExpect(jsonPath("$.success", is(true)))
		.andDo(print());

	verify(loyaltyPhysicalCountService, times(3)).validateSeries(anyString(), anyString());
	verify(loyaltyPhysicalCountService, times(3))
		.savePhysicalCount(Mockito.any(LoyaltyPhysicalCountDto.class));
    }

    @Test
    public void savePhysicalCount_byBarcodeExpectFailure() throws Exception {
	when(loyaltyPhysicalCountService.validateSeries(anyString(), anyString()))
		.thenReturn(Boolean.FALSE);

	List<String> barcodes = Lists.newArrayList("010100000001", "010100000001", "010100000001");

	mockMvc.perform(post("/loyaltycardinventory/physicalcount")
		.contentType(MediaType.APPLICATION_FORM_URLENCODED)
		.param("location", "HO")
		.param("cardType", "")
		.param("barcodes", barcodes.toArray(new String[]{})))
		.andExpect(status().isOk())
		.andExpect(jsonPath("$.success", is(false)))
		.andExpect(jsonPath("$.result", hasSize(1)))
		.andDo(print());

	verify(loyaltyPhysicalCountService, times(3)).validateSeries(anyString(), anyString());
	verifyZeroInteractions(loyaltyPhysicalCountService);
    }

    @Configuration
    @EnableWebMvc
    static class Config {

	//<editor-fold defaultstate="collapsed" desc="Beans declaration">
	@Bean
	public ReportService reportService() {
	    return mock(ReportService.class);
	}

	@Bean
	public LoyaltyTransferDocument loyaltyTransferDocument() {
	    return mock(LoyaltyTransferDocument.class);
	}

	@Bean
	public LoyaltyCardService loyaltyCardService() {
	    return mock(LoyaltyCardService.class);
	}

	@Bean
	public LookupService lookupService() {
	    return mock(LookupService.class);
	}

	@Bean
	public MessageSource messageSource() {
	    return mock(MessageSource.class);
	}

	@Bean
	public CodePropertiesService codePropertiesService() {
	    return mock(CodePropertiesService.class);
	}

	@Bean
	public LoyaltyCardHistoryService methodLoyaltyCardHistoryService() {
	    return mock(LoyaltyCardHistoryService.class);
	}

	@Bean
	public ManufactureOrderService manufactureOrderService() {
	    return mock(ManufactureOrderService.class);
	}

	@Bean
	public IdGeneratorService idGeneratorService() {
	    return mock(IdGeneratorService.class);
	}

	@Bean
	public UserService userService() {
	    return mock(UserService.class);
	}

	@Bean
	public MailService mailService() {
	    return mock(MailService.class);
	}

	@Bean
	public LoyaltyCardInventoryController loyaltyCardInventoryController() {
	    return new LoyaltyCardInventoryController();
	}

	@Bean
	public LoyaltyPhysicalCountService loyaltyPhysicalCountService() {
	    return mock(LoyaltyPhysicalCountService.class);
	}
	//</editor-fold>
    }
}
