<%@ include file="../common/taglibs.jsp" %>
<script src="<spring:url value="/js/common/typeaheadutil.js" />"></script>
<div id="content_userForm" class="modal  nofly modal-dialog" style="display: none;">
	<div class="modal-content">
	<form:form id="assignExhibitionForm" modelAttribute="assignExhibitionForm" method="POST" cssClass="form-horizontal" style="margin: 0px;">
		<div class="modal-header">
		  <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
			<h4>
			  <spring:message code="user_label_assignexhibition"/>
			</h4>

		</div>
		<div class="modal-body">
			<div id="errorcontent" class="alert alert-error">
		      <button type="button" id="hideError" class="close">&times;</button>
		      <div id="errorlist"></div>
		    </div>
        
            <form:hidden path="id" />
			
            <div class="control-group">
              <spring:message code="user_label_location" var="loc" />
              <label for="name" class="control-label">${loc}</label> 
              <div class="controls">
                <input type="text" name="" id="locationName" value="${assignExhibitionForm.name}" />
                <form:hidden path="name" class="form-control" placeholder="${loc}" id="locationNameHidden" />      
              </div>
            </div>
            
            <script type='text/javascript'>
            $(document).ready( function() {
                var url = '<c:url value="/giftcard/b2b/location/list/" />';
                var $desc = $("#locationName");
                var $hidden = $("#locationNameHidden");
                TypeAhead.process( $desc, $hidden, url, { label: "name", value: "id" }, 
                		/* [ "faceValueDesc", "unitCost" ] */null, 
                    function( $item ) {
                });
            });
            </script>
      
		</div>
		<div class="modal-footer">
		  <button id="formSubmit" type="button" class="btn btn-primary formBtn" data-url="<c:url value="/user/assignexhibition/save" />"><spring:message code="button_submit" /></button>
		  <a class="btn " href="#" data-dismiss="modal"><spring:message code="button_cancel" /></a>
		</div>
	</form:form>
	</div>
</div>

<script src="<spring:url value="/js/common/form2js.js" />" ></script>
<script src="<spring:url value="/js/common/json2.js" />" ></script>
<script src="<spring:url value="/js/jquery/jquery.toObject.js" />" ></script>

<script>
$(document).ready(function() {
	
	$("#content_userForm").modal({show: true});
	$("#errorcontent").hide();
	
	$("#formSubmit").click(submitForm);
	$form = $("#assignExhibitionForm");
  function submitForm() {
    $(".formBtn").prop( "disabled", true );
    $.post($(this).data("url"), $form.serialize(), function(data) {
      $(".formBtn").prop( "disabled", false );
      if (data.success) {
    	  $("#content_userForm").modal("hide");
			$("#content_userForm").remove();
      } 
      else {
        errorInfo = "";
        for (i = 0; i < data.result.length; i++) {
          errorInfo += "<br>" + (i + 1) + ". "
              + ( data.result[i].code != undefined ? 
                  data.result[i].code : data.result[i]);
        }
        $locationDialog.find(CONTAINER_ERROR).html("Please correct following errors: " + errorInfo);
        $locationDialog.find(CONTENT_ERROR).show('slow');
      }
    }); 
  }
	
});
</script>