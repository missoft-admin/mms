    create table CRM_GC_CX_PROFILE (
        id number(19,0) not null,
        CREATED_BY varchar2(255 char),
        CREATED_DATETIME timestamp,
        LAST_UPDATED_BY varchar2(255 char),
        LAST_UPDATED_DATETIME timestamp,
        COMPANY_PHONE_NO varchar2(20 char),
        CONTACT_LNAME varchar2(50 char),
        CONTACT_FNAME varchar2(50 char),
        CONTACT_NO varchar2(20 char),
        FAX_NO varchar2(20 char),
        CX_ID varchar2(30 char) not null,
        EMAIL varchar2(50 char),
        INVOICE_TITLE varchar2(100 char) not null,
        ORDER_DATE timestamp,
        ORDER_NO varchar2(255 char),
        MAIL_ADDRESS varchar2(255 char),
        NAME varchar2(100 char) not null,
        SHIPPING_ADDRESS varchar2(255 char),
        CX_TYPE varchar2(15 char),
        DISCOUNT_TYPE varchar2(15 char),
        GENDER varchar2(15 char),
        primary key (id)
    );

    alter table CRM_GC_CX_PROFILE 
        add constraint FK_qekpx14mdaqkd38d2nyopqdhe 
        foreign key (CX_TYPE) 
        references CRM_REF_LOOKUP_DTL;

    alter table CRM_GC_CX_PROFILE 
        add constraint FK_niekvp4hlwmrt9un5n7j35lbq 
        foreign key (DISCOUNT_TYPE) 
        references CRM_REF_LOOKUP_DTL;

    alter table CRM_GC_CX_PROFILE 
        add constraint FK_739bimqqg1k938e5wlbsbnf2l 
        foreign key (GENDER) 
        references CRM_REF_LOOKUP_DTL;




INSERT INTO CRM_REF_LOOKUP_HDR(CODE, DESCRIPTION) VALUES ('GCX001', 'GC CUSTOMER TYPE');
INSERT INTO CRM_REF_LOOKUP_DTL(CODE, DESCRIPTION, HDR, STATUS) VALUES ('CXTP001', 'B2B', 'GCX001', 'ACTIVE');

INSERT INTO CRM_REF_LOOKUP_HDR(CODE, DESCRIPTION) VALUES ('GCX002', 'GC DISCOUNT TYPE');
INSERT INTO CRM_REF_LOOKUP_DTL(CODE, DESCRIPTION, HDR, STATUS) VALUES ('DCTP001', 'TransRetail Discount', 'GCX002', 'ACTIVE');
