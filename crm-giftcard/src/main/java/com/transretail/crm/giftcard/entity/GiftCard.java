package com.transretail.crm.giftcard.entity;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.apache.commons.lang3.BooleanUtils;
import org.hibernate.annotations.Type;

import com.transretail.crm.core.entity.support.CustomAuditableEntity;

/**
 *
 */
@Entity
@Table(name = "CRM_GIFTCARD")
public class GiftCard extends CustomAuditableEntity<Long> implements Serializable {
    private static final long serialVersionUID = -1343094755024924074L;
    // Part 1
    @Column(name = "SEQ_NO", length = 10)
    private String seqNo;
    @Column(name = "TYPE", length = 15)
    private String type;
    @Column(name = "CARD_NAME", length = 100, nullable = false)
    private String name;
    @Column(name = "GEN_CODE", length = 15)
    private String genCode;
    @Column(name = "COUNTRY", length = 20)
    private String country;
    @Column(name = "CURRENCY", length = 5)
    private String currency;
    @Column(name = "BARE_PRICE", precision = 12, scale = 4)
    private BigDecimal cardBarePrice;
    @Column(name = "FIX_VAL", length = 1)
    @Type(type = "yes_no")
    private Boolean fixValue = Boolean.FALSE;
    @Column(name = "FACE_VALUE", precision = 12, scale = 4, nullable = false)
    private BigDecimal faceValue;
    @Column(name = "INC_LINER", length = 1)
    @Type(type = "yes_no")
    private Boolean includeLiner = Boolean.FALSE;
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "VENDOR", nullable = false)
    private CardVendor vendor;
    /**
     * Must be equal to CardVendor#fornalName
     * <br />
     * We need this so that we don't need to join to CardVendor anymore when listing GiftCards
     */
    @Column(name = "VENDOR_NAME", nullable = false)
    private String vendorName;
    @Column(name = "CARD_FEE", precision = 12, scale = 4)
    private BigDecimal cardFee;
    @Column(name = "CARD_NO_LENGTH")
    private Integer cardNoLength;
    @Column(name = "LPO_DIGITS")
    private Integer lpoDigits;
    @Column(name = "RANDOMNO_DIGITS", length = 3, nullable = false)
    private Integer randomNoDigits;
    @Column(name = "LUHNCHK", length = 1)
    @Type(type = "yes_no")
    private Boolean luhnCheck = Boolean.FALSE;
    // Part 2
    @Column(name = "GRACE_DAYS")
    private Integer graceDays;
    @Column(name = "ALLOW_TO_SELL", length = 1)
    @Type(type = "yes_no")
    private Boolean allowToSale = Boolean.FALSE;
    @Column(name = "REDEMPTION")
    private Boolean redemption = Boolean.FALSE;
    @Column(name = "PARTL_REDEEM", length = 1)
    @Type(type = "yes_no")
    private Boolean partialRedeem = Boolean.FALSE;
    @Column(name = "RELOAD", length = 1)
    @Type(type = "yes_no")
    private Boolean reload = Boolean.FALSE;
    @Column(name = "MAX_AMOUNT", precision = 12, scale = 4)
    private BigDecimal maxAmount;
    @Column(name = "MAX_RELOAD_COUNT")
    private Integer maxReloadCount;
    @Column(name = "REFUND", length = 1)
    @Type(type = "yes_no")
    private Boolean refund = Boolean.FALSE;
    @Column(name = "EFCTV_MONTHS")
    private Integer effectivityMonths;
    @Column(name = "BANK_TRUST_MONTHS")
    private Integer bankTrustMonths;
    @Column(name = "YELW_ALRT_PRCNT", precision = 2, scale = 2)
    private Double yellowAlertSafetyPercent;
    @Column(name = "RED_ALRT_PRCNT", precision = 2, scale = 2)
    private Double redAlertSafetyPercent;
    @Column(name = "IMAGE_LOCATION", length = 120)
    private String imageLocation;

    public GiftCard() {

    }

    public GiftCard(Long id) {
        setId(id);
    }

    public String getSeqNo() {
        return seqNo;
    }

    public void setSeqNo(String seqNo) {
        this.seqNo = seqNo;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getGenCode() {
        return genCode;
    }

    public void setGenCode(String genCode) {
        this.genCode = genCode;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public BigDecimal getCardBarePrice() {
        return cardBarePrice;
    }

    public void setCardBarePrice(BigDecimal cardBarePrice) {
        this.cardBarePrice = cardBarePrice;
    }

    public Boolean getFixValue() {
        return fixValue;
    }

    public void setFixValue(Boolean fixValue) {
        this.fixValue = BooleanUtils.toBoolean(fixValue);
    }

    public BigDecimal getFaceValue() {
        return faceValue;
    }

    public void setFaceValue(BigDecimal faceValue) {
        this.faceValue = faceValue;
    }

    public Boolean getIncludeLiner() {
        return includeLiner;
    }

    public void setIncludeLiner(Boolean includeLiner) {
        this.includeLiner = BooleanUtils.toBoolean(includeLiner);
    }

    public CardVendor getVendor() {
        return vendor;
    }

    public void setVendor(CardVendor vendor) {
        this.vendor = vendor;
    }

    public String getVendorName() {
        return vendorName;
    }

    public void setVendorName(String vendorName) {
        this.vendorName = vendorName;
    }

    public BigDecimal getCardFee() {
        return cardFee;
    }

    public void setCardFee(BigDecimal cardFee) {
        this.cardFee = cardFee;
    }

    public Integer getCardNoLength() {
        return cardNoLength;
    }

    public void setCardNoLength(Integer cardNoLength) {
        this.cardNoLength = cardNoLength;
    }

    public Integer getLpoDigits() {
        return lpoDigits;
    }

    public void setLpoDigits(Integer lpoDigits) {
        this.lpoDigits = lpoDigits;
    }

    public Integer getRandomNoDigits() {
        return randomNoDigits;
    }

    public void setRandomNoDigits(Integer randomNoDigits) {
        this.randomNoDigits = randomNoDigits;
    }

    public Boolean getLuhnCheck() {
        return luhnCheck;
    }

    public void setLuhnCheck(Boolean luhnCheck) {
        this.luhnCheck = BooleanUtils.toBoolean(luhnCheck);
    }

    public Integer getGraceDays() {
        return graceDays;
    }

    public void setGraceDays(Integer graceDays) {
        this.graceDays = graceDays;
    }

    public Boolean getAllowToSale() {
        return allowToSale;
    }

    public void setAllowToSale(Boolean allowToSale) {
        this.allowToSale = BooleanUtils.toBoolean(allowToSale);
    }

    public Boolean getRedemption() {
        return redemption;
    }

    public void setRedemption(Boolean redemption) {
        this.redemption = BooleanUtils.toBoolean(redemption);
    }

    public Boolean getPartialRedeem() {
        return partialRedeem;
    }

    public void setPartialRedeem(Boolean partialRedeem) {
        this.partialRedeem = BooleanUtils.toBoolean(partialRedeem);
    }

    public Boolean getReload() {
        return reload;
    }

    public void setReload(Boolean reload) {
        this.reload = BooleanUtils.toBoolean(reload);
    }

    public BigDecimal getMaxAmount() {
        return maxAmount;
    }

    public void setMaxAmount(BigDecimal maxAmount) {
        this.maxAmount = maxAmount;
    }

    public Integer getMaxReloadCount() {
        return maxReloadCount;
    }

    public void setMaxReloadCount(Integer maxReloadCount) {
        this.maxReloadCount = maxReloadCount;
    }

    public Boolean getRefund() {
        return refund;
    }

    public void setRefund(Boolean refund) {
        this.refund = BooleanUtils.toBoolean(refund);
    }

    public Integer getEffectivityMonths() {
        return effectivityMonths;
    }

    public void setEffectivityMonths(Integer effectivityMonths) {
        this.effectivityMonths = effectivityMonths;
    }

    public Integer getBankTrustMonths() {
        return bankTrustMonths;
    }

    public void setBankTrustMonths(Integer bankTrustMonths) {
        this.bankTrustMonths = bankTrustMonths;
    }

    public Double getYellowAlertSafetyPercent() {
        return yellowAlertSafetyPercent;
    }

    public void setYellowAlertSafetyPercent(Double yellowAlertSafetyPercent) {
        this.yellowAlertSafetyPercent = yellowAlertSafetyPercent;
    }

    public Double getRedAlertSafetyPercent() {
        return redAlertSafetyPercent;
    }

    public void setRedAlertSafetyPercent(Double redAlertSafetyPercent) {
        this.redAlertSafetyPercent = redAlertSafetyPercent;
    }

    public String getImageLocation() {
        return imageLocation;
    }

    public void setImageLocation(String imageLocation) {
        this.imageLocation = imageLocation;
    }
    
    public enum Denomination{
	
	D100K(1,100), 
	D200K(2,200), 
	D500K(5,500), 
	D10K(6,10), 
	D25K(7,25), 
	D50K(9,50);
	
	private final int code;
	private final int base;
	private static final int MULTIPLIER = 1000;

	private Denomination(int code, int base) {
	    this.code = code;
	    this.base = base;
	}
	
	public int getAmount() {
	    return MULTIPLIER * base;
	}
	
	public int getCode() {
	    return code;
	}
	
	public static Denomination getByCode(int code) {
	    for (Denomination denomination : Denomination.values()) {
		if (denomination.code == code) {
		    return denomination;
		}
	    }

	    return null;
	}

	@Override
	public String toString() {
	    return this.name().substring(1);
	}
	
    }
}
