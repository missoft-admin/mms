package com.transretail.crm.core.dto;


import java.util.ArrayList;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.mysema.query.types.expr.BooleanExpression;
import com.transretail.crm.common.service.dto.request.AbstractSearchFormDto;
import com.transretail.crm.product.HierarchyLevel;
import com.transretail.crm.product.entity.QCrmProductHier;


public class ProductHierSearchDto extends AbstractSearchFormDto {

	private String level;
	private String code;
	private String codeStart;
	private List<String> codes;
	private List<String> codesStart;
	private List<String> descriptions;
	private List<String> levels;



	@Override
	@JsonIgnore
	public BooleanExpression createSearchExpression() {
		List<BooleanExpression> expr = new ArrayList<BooleanExpression>();
		QCrmProductHier qHier = QCrmProductHier.crmProductHier;

		if ( StringUtils.isNotBlank( level ) && null != ProductHierDto.getHierarchyLevel( level ) ) {
			expr.add( qHier.level.eq( ProductHierDto.getHierarchyLevel( level ) ) );
		}
		if ( StringUtils.isNotBlank( code ) ) {
			expr.add( qHier.code.equalsIgnoreCase( code ) );
		}
		if ( StringUtils.isNotBlank( codeStart ) ) {
			expr.add( qHier.code.startsWithIgnoreCase( codeStart ) );
		}
		if ( CollectionUtils.isNotEmpty( codes ) ) {
			expr.add( qHier.code.in( codes ) );
		}
		if ( CollectionUtils.isNotEmpty( codesStart ) ) {
			List<BooleanExpression> subExpr = new ArrayList<BooleanExpression>();
			for ( String subCodeStart : codesStart ) {
				subExpr.add( qHier.code.startsWithIgnoreCase( subCodeStart ) );
			}
			expr.add( BooleanExpression.anyOf( subExpr.toArray( new BooleanExpression[subExpr.size()] ) ) );
		}
		if ( CollectionUtils.isNotEmpty( descriptions ) ) {
			List<BooleanExpression> subExpr = new ArrayList<BooleanExpression>();
			for ( String desc : descriptions ) {
				subExpr.add( qHier.englishDesc.containsIgnoreCase( desc ) );
				subExpr.add( qHier.localDesc.containsIgnoreCase( desc ) );
			}
			expr.add( BooleanExpression.anyOf( subExpr.toArray( new BooleanExpression[subExpr.size()] ) ) );
		}
		if ( CollectionUtils.isNotEmpty( levels ) ) {
			List<HierarchyLevel> coLevels = new ArrayList<HierarchyLevel>();
			for ( String strLevel : levels ) {
				if ( null != ProductHierDto.getHierarchyLevel( strLevel ) ) {
					coLevels.add( ProductHierDto.getHierarchyLevel( strLevel ) );
				}
			}
			expr.add( qHier.level.in( coLevels ) );
		}

		return BooleanExpression.allOf( expr.toArray( new BooleanExpression[expr.size()] ) );
	}



	public String getLevel() {
		return level;
	}
	public void setLevel(String level) {
		this.level = level;
	}
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public String getCodeStart() {
		return codeStart;
	}
	public void setCodeStart(String codeStart) {
		this.codeStart = codeStart;
	}
	public List<String> getCodes() {
		return codes;
	}
	public void setCodes(List<String> codes) {
		this.codes = codes;
	}

	public List<String> getCodesStart() {
		return codesStart;
	}

	public void setCodesStart(List<String> codesStart) {
		this.codesStart = codesStart;
	}

	public List<String> getDescriptions() {
		return descriptions;
	}

	public void setDescriptions(List<String> descriptions) {
		this.descriptions = descriptions;
	}

	public List<String> getLevels() {
		return levels;
	}

	public void setLevels(List<String> levels) {
		this.levels = levels;
	}

}
