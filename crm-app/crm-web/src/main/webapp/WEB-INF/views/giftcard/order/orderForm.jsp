<%@include file="/WEB-INF/views/common/taglibs.jsp" %>
<div id="addEditOrderDialog" class="modal hide" style="display: none">
  <div class="modal-body">
    <form id="orderForm" class="row-fluid">
      <div class="span12 errorMessages error">&nbsp;</div>
      <div class="span6">
        <div>
          <spring:message code="gc.order.label.cardOrderNo" var="cardOrderNo"/>
          <label for="cardOrderNo" title="${cardOrderNo}">${cardOrderNo}</label>
          <input id="cardOrderNo" name="cardOrderNo" title="${cardOrderNo}" type="text" value="">
        </div>
        <div>
          <spring:message code="gc.order.label.status" var="status"/>
          <label for="status_display" title="${status}">${status}</label>
          <input id="status_display" name="status" title="${status}" type="text" disabled="">
          <input id="status" name="status" title="${status}" type="hidden" value="">
        </div>
        <div>
          <spring:message code="gc.order.label.statusLastUpdated" var="statusLastUpdated"/>
          <label for="statusLastUpdated" title="${statusLastUpdated}">${statusLastUpdated}</label>
          <input id="statusLastUpdated" name="statusLastUpdated" title="${statusLastUpdated}" type="text" value="" disabled="disabled">
        </div>
      </div>
      <div class="span6">
        <div>
          <label for="vendor"><spring:message code="gc.order.label.vendor"/></label>
          <%--TODO: Create and use vendor search instead of displaying everything in dropdown--%>
          <select id="vendor" name="vendor">
            <c:forEach items="${vendors.results}" var="vendor">
              <option value="${vendor.id}">${vendor.name}</option>
            </c:forEach>
          </select>
          <input id="vendorName" name="vendorName" type="hidden"/>
        </div>
        <div>
          <spring:message code="gc.order.label.memo" var="memo"/>
          <label for="memo" title="${memo}">${memo}</label>
          <input id="memo" name="memo" title="${memo}" type="text" value="">
        </div>
        <div>
          <spring:message code="gc.order.label.orderDate" var="orderDate"/>
          <label for="orderDate" title="${orderDate}">${orderDate}</label>
          <input id="orderDate" name="orderDate" type="text" class="date" title="${orderDate}" placeholder="dd-MM-yyyy">
        </div>
      </div>
      <br style="clear: both;"/>
      <div style="margin: 10px 0;">
        <button type="button" id="addNewItem" class="btn btn-primary"><spring:message code="add"/></button>
      </div>
      <div>
        <table style="width: 100%;" class="table table-compressed table-striped table-hover">
          <thead>
          <tr>
            <%--<th><spring:message code="gc.order.label.id"/></th>--%>
            <th><spring:message code="gc.order.label.giftCardName"/></th>
            <th><spring:message code="gc.order.label.lpo"/></th>
            <th><spring:message code="gc.order.label.generationType"/></th>
            <th><spring:message code="gc.order.label.quantity"/></th>
            <th><spring:message code="gc.order.label.storeName"/></th>
            <th><spring:message code="gc.order.label.action"/></th>
          </tr>
          </thead>
          <tbody id="itemList" class="empty">
          <tr>
            <td colspan="7">Nothing to display</td>
          </tr>
          </tbody>
        </table>
      </div>
    </form>
    <input id="orderId" type="hidden" value=""/>
  </div>
  <div class="modal-footer">
    <%-- ROLE_MERCHANT_SERVICE && ROLE_MERCHANT_SERVICE_SUPERVISOR should not be assigned to single user --%>
    <c:choose>
      <c:when test="${isMerchantService}">
        <button id="saveOrder" class="btn btn-primary" type="button"><spring:message code="save"/></button>
        <button id="submitOrder" class="btn btn-primary" type="button"><spring:message code="submit"/></button>
      </c:when>
      <c:when test="${isMerchantServiceSupervisor}">
        <button id="approveOrder" class="btn btn-primary" type="button"><spring:message code="approve"/></button>
        <button id="sendToFtp" class="btn btn-primary" type="button" style="display: none;"><spring:message code="gc.order.label.sendToFtp"/></button>
        <button id="rejectOrder" class="btn btn-primary" type="button"><spring:message code="reject"/></button>
        <button id="voidOrder" class="btn btn-primary" type="button"><spring:message code="void"/></button>
      </c:when>
    </c:choose>
    <button id="closeAddEditOrderDialog" class="btn btn-primary" type="button"><spring:message code="cancel"/></button>
  </div>
</div>
</div>