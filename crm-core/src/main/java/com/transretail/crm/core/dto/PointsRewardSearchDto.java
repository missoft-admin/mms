package com.transretail.crm.core.dto;


import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.joda.time.DateTime;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.mysema.query.types.expr.BooleanExpression;
import com.transretail.crm.common.service.dto.request.AbstractSearchFormDto;
import com.transretail.crm.core.entity.QPointsTxnModel;
import com.transretail.crm.core.entity.enums.PointTxnType;
import com.transretail.crm.core.entity.enums.TxnStatus;


public class PointsRewardSearchDto extends AbstractSearchFormDto {

	private String accountId;
	private Date txnDateFrom;
	private Date txnDateTo;
    private Date postingDateFrom;
    private Date postingDateTo;
    private PointTxnType txnType;
    private TxnStatus status;

    private PointTxnType[] txnTypes;



	@Override
	@JsonIgnore
	public BooleanExpression createSearchExpression() {		
		List<BooleanExpression> expr = new ArrayList<BooleanExpression>();
        QPointsTxnModel qModel = QPointsTxnModel.pointsTxnModel;

        if ( StringUtils.isNotBlank( accountId ) ) {
        	expr.add( qModel.memberModel.accountId.equalsIgnoreCase( accountId ) );
        }
        if ( null != txnDateFrom ) {
        	expr.add( qModel.transactionDateTime.goe( txnDateFrom ) );
        }
        if ( null != txnDateTo ) {
        	expr.add( qModel.transactionDateTime.goe( txnDateTo ) );
        }
        if ( null != postingDateFrom ) {
        	expr.add( qModel.created.goe( new DateTime( postingDateFrom ) ) );
        }
        if ( null != postingDateTo ) {
        	expr.add( qModel.created.goe( new DateTime( postingDateTo ) ) );
        }
        if ( null != txnType ) {
        	expr.add( qModel.transactionType.eq( txnType ) );
        }
        if ( null != status ) {
        	expr.add( qModel.status.eq( status ) );
        }
        if ( null != txnTypes ) {
        	expr.add( qModel.transactionType.in( txnTypes ) );
        }

		return BooleanExpression.allOf( expr.toArray( new BooleanExpression[ expr.size() ] ) );
	}


	
	public String getAccountId() {
		return accountId;
	}
	public void setAccountId(String accountId) {
		this.accountId = accountId;
	}
	public Date getTxnDateFrom() {
		return txnDateFrom;
	}
	public void setTxnDateFrom(Date txnDateFrom) {
		this.txnDateFrom = txnDateFrom;
	}
	public Date getTxnDateTo() {
		return txnDateTo;
	}
	public void setTxnDateTo(Date txnDateTo) {
		this.txnDateTo = txnDateTo;
	}
	public Date getPostingDateFrom() {
		return postingDateFrom;
	}
	public void setPostingDateFrom(Date postingDateFrom) {
		this.postingDateFrom = postingDateFrom;
	}
	public Date getPostingDateTo() {
		return postingDateTo;
	}
	public void setPostingDateTo(Date postingDateTo) {
		this.postingDateTo = postingDateTo;
	}
	public PointTxnType getTxnType() {
		return txnType;
	}
	public void setTxnType(PointTxnType txnType) {
		this.txnType = txnType;
	}
	public TxnStatus getStatus() {
		return status;
	}
	public void setStatus(TxnStatus status) {
		this.status = status;
	}
	public PointTxnType[] getTxnTypes() {
		return txnTypes;
	}
	public void setTxnTypes(PointTxnType[] txnTypes) {
		this.txnTypes = txnTypes;
	}

}
