<%@ include file="../common/taglibs.jsp" %>

<style type="text/css">
<!--
  #list_employee .dataTable tr td:last-child {
    min-width: 110px;
  }
  #list_employee .dataTable tr td:nth-child(4), #list_employee .dataTable tr th:nth-child(4) {
    min-width: 250px;
  }
  #list_employee .dataTable tr td:nth-child(2), #list_employee .dataTable tr th:nth-child(2) {
    width: 100px !important;
  }
  #list_employee .dataTable tr td:nth-child(3), #list_employee .dataTable tr th:nth-child(3) {
    width: 178px !important;
  }
-->
</style>
  <sec:authorize var="isHelpDesk" ifAnyGranted="ROLE_HELPDESK"/>

  <spring:message var="typeName" code='menu_item_employeemodel_new_label'/>
  <spring:url value="/resources/images/show.png" var="show_image_url" />
  <spring:url value="/resources/images/update.png" var="update_image_url"/>
  <spring:url value="/resources/images/delete.png" var="delete_image_url"/>
  <c:set var="path" value="/employeemodels" />
  
  <div class="page-header page-header2">
    <h1><spring:message code="label_com_transretail_crm_entity_employeemodel_plural" /></h1>
  </div>
  <div class="">
   <jsp:include page="searchbox.jsp" />
   </div>
  
  <div class="modal hide groupModal nofly" tabindex="-1" role="dialog" aria-hidden="true" id="form_export"></div>
  
  <div class="clearfix"/>
  
  <div class="mb20 clearfix">
    <div class="pull-left">
      <button type="button" class="btn btn-print link_print"><spring:message code="employee.purchase.report.print.label" /></button>
      <button type="button" id="printList" class="btn btn-print" data-url="<spring:url value="/employeemodels/printList" />"><spring:message code="label_com_transretail_crm_entity_customermodel_print_list_employee" /></button>
    </div>
  <c:if test="${!isHelpDesk}">
  <button class="btn btn-primary pull-right" id="createEmpLnk" data-query="${pageContext.request.queryString}">
        <spring:message code="label_com_transretail_crm_entity_employeemodel" var="argument"/>
      	<spring:message code="global_menu_new" arguments="${ argument }"/>
  </button>
  </c:if>
  </div>
  

  
<div id="content_employeeList">

  <div id="list_employee" 
    data-url="<c:url value="/employeemodels/list" />"
    data-hdr-username="<spring:message code="label_com_transretail_crm_entity_customermodel_username" />"
    data-hdr-accountno="<spring:message code="label_com_transretail_crm_entity_customermodel_accountnumber" />"
    data-hdr-member="<spring:message code="label_com_transretail_crm_entity_employeemodel" />"
    data-hdr-store="<spring:message code="label_com_transretail_crm_entity_customermodel_registeredstore" />" 
    data-hdr-status="<spring:message code="label_com_transretail_crm_entity_customermodel_status" />"
    data-hdr-txn-amount="<spring:message code="member_emp_prop_txn_previousmo" />"></div>

</div>
   
  <div class="hide">
  
  <div id="updateBtn">
      
  <spring:url value="${path}/%ITEMID%" var="update_form_url">
    <spring:param name="form"/>
  </spring:url>
  <spring:message arguments="${typeName}" code="entity_update" var="update_label" htmlEscape="false"/>
  <a href="#" class="btn tiptip pull-left icn-edit updateEmpLnk" data-member-id="%ITEMID%" data-query="${pageContext.request.queryString}"
    alt="${fn:escapeXml(update_label)}" title="${fn:escapeXml(update_label)}">
    <img alt="${fn:escapeXml(update_label)}" class="image" src="${update_image_url}"
         title="${fn:escapeXml(update_label)}"/>
  </a>
  </div>
  
  <div id="deleteBtn">
  <spring:url value="${path}/%ITEMID%" var="delete_form_url"/>
  <form:form action="${delete_form_url}" id="deleteForm%ITEMID%" method="DELETE" cssClass="form-reset">
    <spring:message arguments="${typeName}" code="entity_delete" var="delete_label" htmlEscape="false"/>
    <c:set var="delete_confirm_msg"><spring:escapeBody 
    javaScriptEscape="true"><spring:message 
    code="entity_delete_confirm"/></spring:escapeBody></c:set>
        <a href="#" onclick="getConfirm('${delete_confirm_msg}',function(result) {if(result) $('form#deleteForm%ITEMID%').submit();});" title="${fn:escapeXml(delete_label)}" class="tiptip btn pull-left icn-delete">
        </a>
    <c:if test="${not empty param.page}">
      <input name="page" type="hidden" value="1"/>
    </c:if>
    <c:if test="${not empty param.size}">
      <input name="size" type="hidden" value="${fn:escapeXml(param.size)}"/>
    </c:if>
  </form:form>
  </div>
  
  <div id="viewTxnBtn">
  <spring:message code="member_emp_label_viewcurrenttxns" var="viewtxn_label"
      htmlEscape="false" /> <a id="" data-current-txn-total="%CURRENTTOTAL%" href="#" class="viewTxnLnk tiptip pull-left btn icn-view" data-account-id="%ACCOUNTID%" 
      data-context-path="${pageContext.request.contextPath}" data-controller-path="purchase/" alt="${viewtxn_label}" title="${viewtxn_label}">
  </a>
  </div>
  
  <div id="viewPntsBtn">
  <spring:url value="/member/points/%ACCOUNTID%" var="show_points_url"/>
  <spring:message code="employees_viewpointstxn" var="viewpoints_label" htmlEscape="false" />
  <a href="${show_points_url}" alt="${fn:escapeXml(viewpoints_label)}" title="${fn:escapeXml(viewpoints_label)}" class="tiptip btn pull-left icn-view">
  </a>
  </div>
  </div>
  <jsp:include page="../customermodels/form/memberform.jsp" />
  <jsp:include page="calendarBox.jsp" />
  <jsp:include page="../common/confirm.jsp" />
  
   <link href="<spring:url value="/css/dataTables.css" />" rel="stylesheet">
  <script src="<spring:url value="/js/jquery/jquery.dataTables.js" />" ></script>
  <script src="<spring:url value="/js/jquery/jquery.dataTables.linkLengthMenu.js" />"></script>
  <script src="<spring:url value="/js/jquery/jquery.dataTables.customPaging.js" />"></script>
  <script src="<spring:url value="/js/jquery/ajax-datatable.js" />" ></script>
  <script src="<spring:url value="/js/common/form2js.js" />" ></script>
  <script src="<spring:url value="/js/common/json2.js" />" ></script>
  <script src="<spring:url value="/js/jquery/jquery.toObject.js" />" ></script>
  
  <script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/viewspecific/employeemodel/employee.js"><![CDATA[&nbsp;]]></script>
  <script src='<c:url value="/js/bootstrap/bootstrap-datepicker.js"/>' type='text/javascript'><![CDATA[&nbsp; ]]></script>
  <link href="<c:url value="/css/bootstrap/datepicker.css" />" rel="stylesheet" />

  <jsp:include page="../points/items/itemList.jsp" />
  <jsp:include page="transactionList.jsp" />
  <jsp:include page="print.jsp" />
  
