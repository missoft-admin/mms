<%@ include file="../../common/taglibs.jsp" %>


<style type="text/css">
<!--
    .control-group-02 { width: 277px !important; }
    #block_promotionDays.well { margin-left: 20px; }
    #content_target select { width: 202px; }
    .span5 { margin-left: 0px; }
    .block-stack-01 { margin-bottom: 0 !important; }
    #block_promotionDuration { margin-left: 20px; }
-->
</style>


<div id="content_promotionForm" class="">

  <div class="page-header page-header2"><h1><spring:message code="promo_redemption_create" /></h1></div>


  <c:url var="url_listPromotions" value="/promotion"/>
  <c:url var="url_promotionForm" value="${promotionFormAction}"/>
  <form:form id="promotionForm" name="promotion" modelAttribute="promotion" action="${url_promotionForm}" data-url="${url_listPromotions}" class="form-horizontal">
  <fieldset>    

    <div class="content_promo">

      <div class="hide alert alert-error" id="content_error"><button type="button" class="close" onclick="$(this).parent().hide();">&times;</button><div><form:errors path="*"/></div></div>

      <div class="">

        <div class="block-stack-01">
          <div class="pull-left">
            <div id="block_promotionProgram" class="control-group">
              <label for="program" class="control-label"><spring:message code="promotion_prop_program"/></label>
              <div class="controls">
                <c:url var="url_listPrograms" value="/promotion/campaign/list/"/>
                <form:select path="program" id="program" data-url="${url_listPrograms}">
                  <c:forEach var="program" items="${promotionPrograms}">
                    <form:option value="${program.id}" label="${program.name}" data-min-date="${program.startDate}" data-max-date="${program.endDate}" />
                  </c:forEach>
                </form:select>
    
                <a href="#" class="btn btn-small" id="link_promotionProgram" data-url="<c:url value="/promotion/program/create"/>">
                  <spring:message code="label_add"/>
                </a>    
              </div>
            </div>
    
            <div id="block_promotionCampaign" class="control-group">
              <label for="campaign" class="control-label"><spring:message code="promotion_prop_campaign"/></label>
              <div class="controls">
                <form:select path="campaign" id="campaign">
                  <c:forEach var="campaign" items="${promotionCampaigns}">
                    <form:option value="${campaign.id}" label="${campaign.name}" data-min-date="${campaign.startDate}" data-max-date="${campaign.endDate}" />
                  </c:forEach>
                </form:select>
    
                <a href="#" class="btn btn-small" id="link_promotionCampaign" data-url='<c:url value="/promotion/campaign/create"/>'>
                  <spring:message code="label_add"/>
                </a>
                <div id="form_promotionCampaign"></div>
              </div>
            </div>
    
            <div id="block_promotionName" class="control-group">
              <label for="name" class="control-label"><b class="required">*</b> <spring:message code="promotion_prop_name"/></label>
              <div class="controls">
                <form:input path="name" id="name"/>
                <form:hidden path="id"/>
              </div>
            </div>
    
            <div id="block_promotionDesc" class="control-group">
              <label for="description" class="control-label"><spring:message code="promotion_prop_description"/></label>
              <div class="controls">
                <form:input path="description" id="description"/>
              </div>
            </div>

            <c:if test="${not empty promotion.createUser}">
            <div id="" class="control-group">
              <label for="" class="control-label"><spring:message code="promotion_label_author"/></label>
              <div class="controls"><c:out value="${promotion.createUser}" /> - <c:out value="${promotion.createUserFullName}" /></div>
            </div>
            </c:if>

            <c:if test="${not empty promotion.dispCreated}">
            <div id="" class="control-group">
              <label for="" class="control-label"><spring:message code="promotion_label_createddate"/></label>
              <div class="controls"><span id="content_created">${promotion.dispCreated}</span></div>
            </div>
            </c:if>
          </div>


          <div class="pull-left control-group-01">
	          <div class="pull-left control-group-small control-group" id="block_promotionDuration">
	          <fieldset>
	            <label class="block-stack-title" style="border-bottom: 1px solid #E8E8E8; margin-bottom: 20px; padding: 5px 0 4px;"><spring:message code="promotion_prop_duration" /></label>
	    
	            <div class="input-daterange" id="duration">
		            <div class="control-group" style="margin-bottom: 10px;">
		              <label for="startDate" class="control-label case-cap"><spring:message code="promotion_prop_startdate"/></label>
	                <div class="controls"><form:input path="startDate" id="startDate" cssClass="input-space-01 input-small" /></div>
	              </div>
	              <div class="control-group">
	                <label for="endDate" class="control-label case-cap"><spring:message code="promotion_prop_enddate"/></label>
	                <div class="controls"><form:input path="endDate" id="endDate" cssClass="input-small"/></div>
	              </div>
	            </div>
	          </fieldset>
	          </div>
	
	          <div class="pull-left control-data-01">
              <div class="pull-left control-group-small">
              <fieldset>
                <label class="block-stack-title" style="border-bottom: 1px solid #E8E8E8; margin-bottom: 20px; padding: 5px 0 4px;"><spring:message code="reward_label_affecteddaysandtime" /></label>
        
                <div id="block_promotionTime" class="pull-left">
                  <div class="control-group" style="margin-bottom: 10px;">
                    <label for="startTime" class="control-label case-cap"><spring:message code="promotion_prop_starttime"/></label>
                    <div class="controls">
                      <form:input path="startTime" id="startTime" cssClass="input-small input-space-01" />
                    </div>
                  </div>
                  <div class="control-group">
                    <label for="endTime" class="control-label case-cap"><spring:message code="promotion_prop_endtime"/></label>
                    <div class="controls">
                      <form:input path="endTime" id="endTime" cssClass="input-small" />
                    </div>
                  </div>
                </div>
    
                <div class="clearfix"></div>
              </fieldset>
              </div>
      
              <div id="block_promotionDays" class="well pull-left">
                <div class="checkbox">
                    <div class="select-all-header">
                        <input type="checkbox" id="selectAllDays" /><label for="affectedDays" class=""><spring:message code="promotion_prop_selectall"/></label>
                    </div>
                    <div id="content_promotionDays" class="">
                      <c:forEach var="promotionDay" items="${promotionAffectedDays}">
                        <div class="capitalize" >
                          <c:if test="${null != promotion.affectedDays}">
                            <form:checkbox path="promotionDays" value="${promotionDay.code}" label="${promotionDay.description}" />
                          </c:if>
                          <c:if test="${null == promotion.affectedDays}">
                            <form:checkbox path="promotionDays" value="${promotionDay.code}" label="${promotionDay.description}" checked="checked" />
                          </c:if>
                        </div>
                      </c:forEach>
                    </div>
                </div>
              </div>

              <div class="clearfix"></div>
	          </div>

            <div class="clearfix"></div>
	        </div>


          <div class="clearfix"></div>
        </div>


        <div class="block-stack-01">          
          <div id="content_target" class="control-group"><jsp:include page="../targets/targets.jsp" /></div>
        </div>


        <div class="block-stack-01">
          <div class="control-group-01 rewardContainer">
          <fieldset>
            <legend class="block-stack-title"><spring:message code="promotion_prop_rewards" /></legend>

            <div id="block_promotionRewards" class="">
              <!-- <label for="rewardType" class="control-label"></label> -->
              <div id="content_rewardTypes" class="">
                <a href="#" class="btn btn-small" id="link_redemptionPromoReward"
                  data-list-url="<c:url value="/promotion/reward/list/" />" data-type="${rewardType}" data-url="<c:url value="/promotion/reward/create/"/>">
                  <spring:message code="promo_redemption_reward_add"/>
                </a>
              </div>
              <div id="list_promotionRewards"><c:if test="${not empty promotion.promotionRewards and null != promotion.rewardType}"><jsp:include page="../reward/rewardList.jsp" /></c:if></div>

            </div>
          </fieldset>
          </div>
        </div>

        <c:if test="${isMarketingHead}">
        <div class="block-stack-01">
          <div class="control-group-01">
          <fieldset>
            <legend class="block-stack-title"><spring:message code="remark_prop_remark" /></legend>
            <div class="controls"><form:textarea path="remarks.remarks" id="remarksRemarks"/></div>
            <form:hidden path="remarks.status" id="remarksStatus"/>
            <form:hidden path="remarks.modelId"/>
            <form:hidden path="remarks.modelType"/>
          </fieldset>
          </div>
        </div>
        </c:if>

      </div>

    </div>
  
  </fieldset>


  <div class="form-actions container-btn">
    <c:choose>
    <c:when test="${isMarketingHead}">
      <c:if test="${promotion.status ne promotionStatuses[0].code}">
        <!--<input id="promotionRemarksForm_approveButton" type="submit" data-loading-text="<spring:message code="loading_text" />" value="<spring:message code="label_${promotionStatuses[0].code}"/>" class="promotionStatus btn btn-primary" data-value="${promotionStatuses[0].code}" />
        <input id="promotionRemarksForm_rejectButton" type="submit" data-loading-text="<spring:message code="loading_text" />" value="<spring:message code="label_${promotionStatuses[1].code}"/>"  class="promotionStatus btn btn-primary" data-value="${promotionStatuses[1].code}" />-->
        <button class="btn btn-primary promotionStatus" id="promotionRemarksForm_approveButton" type="submit" data-loading-text="<spring:message code="loading_text" />" data-value="${promotionStatuses[0].code}"><spring:message code="label_${promotionStatuses[0].code}"/></button>
        <button class="btn btn-primary promotionStatus" id="promotionRemarksForm_rejectButton" type="submit" data-loading-text="<spring:message code="loading_text" />" data-value="${promotionStatuses[1].code}"><spring:message code="label_${promotionStatuses[1].code}"/></button>
      </c:if>
      <c:if test="${promotion.status eq promotionStatuses[0].code}">
        <!--<input id="promotionRemarksForm_disableButton" type="submit"  value="<spring:message code="label_${promotionStatuses[2].code}"/>"  class="promotionStatus btn btn-primary" data-value="${promotionStatuses[2].code}" />-->
        <button class="btn btn-primary promotionStatus" id="promotionRemarksForm_disableButton" type="submit" data-value="${promotionStatuses[2].code}" ><spring:message code="label_${promotionStatuses[2].code}"/></button>
      </c:if>
    </c:when>
    <c:otherwise>
      <!--<input id="promotionForm_saveButton" data-loading-text="<spring:message code="loading_text" />" class="btn btn-primary" type="submit" value='<spring:message code="label_submit_for_approval"/>' />-->
      <button class="btn btn-primary" id="promotionForm_saveButton" type="submit" data-loading-text="<spring:message code="loading_text" />" ><spring:message code="label_submit_for_approval"/></button>
    </c:otherwise>
    </c:choose>
    <button class="btn" id="promotionForm_cancelButton" type="button"><spring:message code="label_cancel"/></button>
  </div>

  </form:form>


	<div id="form_promotionProgram"><jsp:include page="../program/programForm.jsp" /></div>
	<jsp:include page="../targets/storegroup.jsp" />
	<jsp:include page="../targets/membergroup.jsp" />
  <div id="storeGroupContainer"></div>
  <div id="memberGroupContainer"></div>


  <script src='<c:url value="/js/viewspecific/promotion/promotionForm.js"/>' type='text/javascript'><![CDATA[&nbsp; ]]></script>
  <script src='<c:url value="/js/bootstrap/bootstrap-datepicker.js"/>' type='text/javascript'><![CDATA[&nbsp; ]]></script>
  <script src='<c:url value="/js/bootstrap/bootstrap-timepicker.min.js"/>' type='text/javascript'><![CDATA[&nbsp; ]]></script>
  <link href="<c:url value="/css/bootstrap/datepicker.css" />" rel="stylesheet" />
  <link href="<c:url value="/css/bootstrap/bootstrap-timepicker.min.css" />" rel="stylesheet" />
  <link href="<c:url value="/styles/temp.css" />" rel="stylesheet" />
  <script src="<spring:url value="/js/jquery/jquery.numberInput.js" />" ></script>
  <script src="<spring:url value="/js/common/typeaheadutil.js" />"></script>


</div>