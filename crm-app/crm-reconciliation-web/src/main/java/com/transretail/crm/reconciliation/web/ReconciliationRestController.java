package com.transretail.crm.reconciliation.web;

import java.sql.Timestamp;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Callable;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.google.common.collect.Maps;
import com.transretail.crm.common.service.exception.MessageSourceResolvableException;
import com.transretail.crm.common.web.RestConstants;
import com.transretail.crm.common.web.client.CrmRestTemplate;
import com.transretail.crm.devtool.dto.ColumnMetadataBean;
import com.transretail.crm.devtool.dto.TableMetadataBean;
import com.transretail.crm.reconciliation.service.DataSyncService;
import com.transretail.crm.reconciliation.service.DiscrepancyToolService;
import com.transretail.crm.rest.pos.response.dto.GenericResponseBean;

/**
 * @author Allan G. Ramirez (agramirez@exist.com)
 */
@Controller
public class ReconciliationRestController implements InitializingBean {
    private static final Logger _LOG = LoggerFactory.getLogger(ReconciliationRestController.class);
    protected String crmProxyRestWsPrfx;
    protected String logCheckerWs;
    @Autowired
    private DiscrepancyToolService discrepancyToolService;
    @Autowired
    private DataSyncService dataSyncService;

    @Value("#{'${crm.proxy.host}'}")
    private String crmProxyHost;

    @Value("#{'${crm.proxy.port}'}")
    private String crmProxyPort;

    @Override
    public void afterPropertiesSet() throws Exception {
        crmProxyRestWsPrfx = "http://" + crmProxyHost + ":" + crmProxyPort + "/crm-proxy/api";
        logCheckerWs = crmProxyRestWsPrfx + "/log/filters/found";
        _LOG.info("CRM PROXY REST PREFIX : {}", crmProxyRestWsPrfx);
        _LOG.info("CRM PROXY LOG REST WS : {}", logCheckerWs);
    }

    /**
     * Return ColumnMetadataBean of records present in main db but does not exist in local db.
     *
     * @param beans table metadata with table name and table id column name
     * @return ColumnMetadataBean of records present in main db but does not exist in local db.
     */
    @RequestMapping(value = "/localdb/missing/records", method = RequestMethod.POST)
    @ResponseBody
    public Map<String, List<ColumnMetadataBean>> getLocalDbMissingRecords(@RequestBody List<TableMetadataBean> beans) {
        try {
            return discrepancyToolService.getLocalDbMissingRecords(beans);
        } catch (MessageSourceResolvableException e) {
            _LOG.error("Failed to read localdb missing records.", e);
            Map<String, List<ColumnMetadataBean>> result = Maps.newHashMap();
            result.put(RestConstants.KEY_REST_ERROR.toString(), Arrays.asList(new ColumnMetadataBean(e)));
            return result;
        }
    }

    /**
     * Return ColumnMetadataBean of records present in local db but does not exist in server db.
     *
     * @param beans table metadata with table name and table id column name
     * @return ColumnMetadataBean of records present in main db but does not exist in local db.
     */
    @RequestMapping(value = "/maindb/missing/records", method = RequestMethod.POST)
    @ResponseBody
    public Map<String, List<ColumnMetadataBean>> mainDbMissingRecords(@RequestBody List<TableMetadataBean> beans) {
        try {
            return discrepancyToolService.getMainDbMissingRecords(beans);
        } catch (MessageSourceResolvableException e) {
            _LOG.error("Failed to read ttserver missing records.", e);
            Map<String, List<ColumnMetadataBean>> result = Maps.newHashMap();
            result.put(RestConstants.KEY_REST_ERROR.toString(), Arrays.asList(new ColumnMetadataBean(e)));
            return result;
        }
    }


    @RequestMapping(value = "/sync/reference/records", method = RequestMethod.GET)
    @ResponseBody
    public Callable<GenericResponseBean> syncRefRecords() {
        return new Callable<GenericResponseBean>() {
            @Override
            public GenericResponseBean call() throws Exception {
                GenericResponseBean result = new GenericResponseBean();
                try {
                    Timestamp ts = dataSyncService.syncReferenceRecords();
                    if (ts != null) {
                        _LOG.info("Reference data sync successful. Timestamp: {}", ts.toString());
                        result.setSuccessMessage("Data sync successful. Timestamp: " + ts);
                    } else {
                        result.setSuccessMessage("Data are already in-sync.");
                    }
                } catch (MessageSourceResolvableException e) {
                    _LOG.error("Failed to sync reference records.", e);
                    result.setException(e);
                }
                return result;
            }
        };
    }

    @RequestMapping(value = "/sync/transaction/records", method = RequestMethod.GET)
    @ResponseBody
    public Callable<GenericResponseBean> syncTransactionRecords() {
        return new Callable<GenericResponseBean>() {
            @Override
            public GenericResponseBean call() throws Exception {
                GenericResponseBean result = new GenericResponseBean();
                try {
                    Timestamp ts = dataSyncService.syncTxnRecords();
                    if (ts != null) {
                        _LOG.info("Transactions sync successful. Timestamp: {}", ts.toString());
                        result.setSuccessMessage("Transactions sync successful. Timestamp: " + ts);
                    } else {
                        result.setSuccessMessage("Transactions are already in-sync.");
                    }
                } catch (MessageSourceResolvableException e) {
                    _LOG.error("Failed to sync transaction records.", e);
                    result.setException(e);
                }
                return result;
            }
        };
    }

}
