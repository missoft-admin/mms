<%@ include file="../common/taglibs.jsp" %>


<div id="content_pointsList">

  <jsp:include page="items/itemList.jsp" />
  <div id="list_point" class="data-table-01" 
    data-url="<c:url value="/member/points/list/${member.accountId}" />"
    data-hdr-storecode="<spring:message code="points_prop_transaction_location"/>"
    data-hdr-transactiontype="<spring:message code="points_prop_txntype"/>" 
    data-hdr-created="<spring:message code="points_prop_created"/>" 
    data-hdr-transactiondatetime="<spring:message code="points_prop_txndate"/>" 
    data-hdr-transactionno="<spring:message code="points_prop_txnno"/>" 
    data-hdr-transactionpoints="<spring:message code="points_prop_txnpoints"/>" 
    data-hdr-transactionamount="<spring:message code="points_prop_amount"/>"
    data-hdr-transactionmedia="<spring:message code="points_prop_media"/>" 
    data-hdr-transactioncontributer="<spring:message code="points_label_contributed"/>"
    data-link-view-items="<spring:message code="points_label_viewitems"/>" 
    data-url-view-items="<c:url value="/user/edit/" />"
    data-is-customer="${isCustomer}" ></div>

	<script>
		isSupplement = "${isSupplement}";
		isEmployee = "${isEmployee}";
	</script>

  <link href="<spring:url value="/css/dataTables.css" />" rel="stylesheet">
  <script src="<spring:url value="/js/jquery/jquery.dataTables.js" />" ></script>
  <script src="<spring:url value="/js/jquery/jquery.dataTables.linkLengthMenu.js" />"></script>
  <script src="<spring:url value="/js/jquery/jquery.dataTables.customPaging.js" />"></script>
  <script src="<spring:url value="/js/jquery/ajax-datatable.js" />" ></script>
  <script src="<spring:url value="/js/common/form2js.js" />" ></script>
  <script src="<spring:url value="/js/common/json2.js" />" ></script>
  <script src="<spring:url value="/js/jquery/jquery.toObject.js" />" ></script>

  <script src="<spring:url value="/js/viewspecific/points/pointsList.js"/>"></script>


</div>