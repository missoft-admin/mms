package com.transretail.crm.giftcard.repo;

import org.springframework.stereotype.Repository;

import com.transretail.crm.common.repo.CrmQueryDslPredicateExecutor;
import com.transretail.crm.giftcard.entity.B2BExhibitionLocation;

@Repository
public interface B2BExhibitionLocationRepo extends CrmQueryDslPredicateExecutor<B2BExhibitionLocation, Long> {

}
