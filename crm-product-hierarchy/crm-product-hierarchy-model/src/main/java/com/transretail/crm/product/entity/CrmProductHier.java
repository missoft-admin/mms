package com.transretail.crm.product.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.Parameter;
import org.hibernate.annotations.Type;
import org.joda.time.DateTime;

import com.transretail.crm.product.HierarchyLevel;

/**
 * @author Allan G. Ramirez (agramirez@exist.com)
 */
@Entity
@Table(name = "CRM_PROD_HIER")
public class CrmProductHier {
    @Id
    @Column(name = "CODE")
    private String code;
    @Column(name = "ENGLI_DESC")
    private String englishDesc;
    @Column(name = "LOCAL_DESC")
    private String localDesc;
    @Column(name = "HIER_LEVEL")
    @Enumerated(EnumType.ORDINAL)
    private HierarchyLevel level;
    @Column(name = "PARENT_HIER_LEVEL")
    private String parentLevel;
    @Column(name = "LAST_UPDATED_DATETIME")
    @Type(type = "org.jadira.usertype.dateandtime.joda.PersistentDateTime",
        parameters = {@Parameter(name = "databaseZone", value = "jvm")})
    private DateTime lastUpdated;
    @Column(name = "CREATED_DATETIME")
    @Type(type = "org.jadira.usertype.dateandtime.joda.PersistentDateTime",
        parameters = {@Parameter(name = "databaseZone", value = "jvm")})
    private DateTime created;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getEnglishDesc() {
        return englishDesc;
    }

    public void setEnglishDesc(String englishDesc) {
        this.englishDesc = englishDesc;
    }

    public String getLocalDesc() {
        return localDesc;
    }

    public void setLocalDesc(String localDesc) {
        this.localDesc = localDesc;
    }

    public HierarchyLevel getLevel() {
        return level;
    }

    public void setLevel(HierarchyLevel level) {
        this.level = level;
    }

    public String getParentLevel() {
        return parentLevel;
    }

    public void setParentLevel(String parentLevel) {
        this.parentLevel = parentLevel;
    }

    public DateTime getLastUpdated() {
        return lastUpdated;
    }

    public void setLastUpdated(DateTime lastUpdated) {
        this.lastUpdated = lastUpdated;
    }

    public DateTime getCreated() {
        return created;
    }

    public void setCreated(DateTime created) {
        this.created = created;
    }
}
