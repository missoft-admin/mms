
-- ADD CLAIMS TO AFFILIATES PEOPLESOFT CONFIG
insert into CRM_FOR_PEOPLESOFT_CONFIG(MMS_ACCT, PS_ACCT, TXN_TYPE, TYPE, BU)
   values ('22', '26020300', 'CLAIM_TO_ARI', 'Claim To ARI', '050');

insert into CRM_FOR_PEOPLESOFT_CONFIG(MMS_ACCT, PS_ACCT, TXN_TYPE, TYPE, BU)
   values ('22', '26020300', 'CLAIM_TO_TGI', 'Claim To TGI', '110');

-- ADD CLAIMS TO AFFILIATES SCHEDULE PERMISSION
INSERT INTO CRM_USER_PERMISSION(CODE, DESCRIPTION, STATUS,ROLE_CATEGORY) 
  VALUES ('CLAIMS_TO_AFFILIATES_SCHEDULE','Manage claims to affiliates schedule', 'ACTIVE','ROLE_MERCHANT_SERVICE');

-- ADD TAGGING COLUMN FOR CRM_GC_TRANSACTION TABLE
alter table CRM_GC_TRANSACTION
  add column CLAIMED_BY_AF boolean;

update CRM_GC_TRANSACTION set CLAIMED_BY_AF = 0 where CLAIMED_BY_AF is null;

alter table CRM_GC_TRANSACTION
  modify column CLAIMED_BY_AF boolean not null;

-- UPDATE GC_TRANSACTION.TXN_TYPE VALUE DUE TO MISUSED OF CONSTANT
update CRM_GC_TRANSACTION set TXN_TYPE = 'VOID_ACTIVATED' where TXN_TYPE = 'VOID_ACTIVATION';
