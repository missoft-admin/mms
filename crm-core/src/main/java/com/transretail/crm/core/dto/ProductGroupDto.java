package com.transretail.crm.core.dto;


import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;

import com.transretail.crm.core.entity.ProductGroup;


public class ProductGroupDto {

	public static enum PRODUCT_OPTION { product };
//	private static final String[] IGNORE = { "itemCodeDesc", "excProductCodeDesc" };

	private Long id;
	private String name; 
	private List<CodeDescBean> itemCodeDesc;
	private List<CodeDescBean> excProductCodeDesc;
	private String products;
	private String productsCfns;
	private String excludedProducts;

	private Boolean isUsed;

	public ProductGroupDto() {}
	public ProductGroupDto( ProductGroup model ) {
//		BeanUtils.copyProperties( model, this, IGNORE );
        this.id = model.getId();
        this.name = model.getName();
		if ( StringUtils.isNotBlank( model.getProductsCfns() ) ) {
			itemCodeDesc = new ArrayList<CodeDescBean>();
			for ( String code : Arrays.asList( StringUtils.split( model.getProductsCfns(), ProductGroup.PRODUCT_LIST_SEPARATOR ) ) ) {
				if ( StringUtils.isNotBlank( code ) ) {
					itemCodeDesc.add( new CodeDescBean(code, null) );
				}
			}
		}

		if ( StringUtils.isNotBlank( model.getExcludedProducts() ) ) {
			excProductCodeDesc = new ArrayList<CodeDescBean>();
			for ( String code : Arrays.asList( StringUtils.split( model.getExcludedProducts(), ProductGroup.PRODUCT_LIST_SEPARATOR ) ) ) {
				if ( StringUtils.isNotBlank( code ) ) {
					excProductCodeDesc.add( new CodeDescBean(code, null) );
				}
			}
		}
        this.products = model.getProducts();
        this.productsCfns = model.getProductsCfns();
        this.excludedProducts = model.getExcludedProducts();
	}

	public ProductGroup toModel() { return toUpdatedModel( new ProductGroup() ); }
	public ProductGroup toUpdatedModel( ProductGroup model ) {
//		BeanUtils.copyProperties( this, model, IGNORE );
        model.setName(this.name);
//		if ( StringUtils.isNotBlank( products ) && products.length() <= 4000 ) {
//			model.setProducts( products );
//		}

		model.setProductsCfns( productsCfns );

		if ( StringUtils.isNotBlank( excludedProducts ) ) {
			model.setExcludedProducts( excludedProducts );
		}
		else if ( CollectionUtils.isNotEmpty( excProductCodeDesc ) ) {
			List<String> prdCodes = new ArrayList<String>();
			for ( CodeDescBean bean : excProductCodeDesc ) {
				if ( null != bean ) {
					CollectionUtils.addIgnoreNull( prdCodes, bean.getCode() );
				}
			}
			Collections.sort( prdCodes );
			model.setExcludedProducts( StringUtils.join( prdCodes, ProductGroup.PRODUCT_LIST_SEPARATOR ) );
		}
		else {
			model.setExcludedProducts( excludedProducts );
		}

		return model;
	}

	public static List<ProductGroupDto> toDtoList( List<ProductGroup> models ) {
		if ( CollectionUtils.isEmpty( models ) ) {
			return new ArrayList<ProductGroupDto>();
		}

		List<ProductGroupDto> dtos = new ArrayList<ProductGroupDto>();
		for ( ProductGroup model : models ) {
			dtos.add( new ProductGroupDto( model ) );
		}
		return dtos;
	}





	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<CodeDescBean> getItemCodeDesc() {
		return itemCodeDesc;
	}

	public void setItemCodeDesc(List<String> itemCodes) {
		if ( CollectionUtils.isNotEmpty( itemCodes ) ) {
			List<CodeDescBean> itemCodeDesc = new ArrayList<CodeDescBean>();
			for ( String code : itemCodes ) {
				itemCodeDesc.add( new CodeDescBean(code, null) );
			}
			this.itemCodeDesc = itemCodeDesc;
		}
		else {
			this.itemCodeDesc = new ArrayList<CodeDescBean>();
		}
	}

	public List<CodeDescBean> getExcProductCodeDesc() {
		return excProductCodeDesc;
	}

	public void setExcProductCodeDesc(List<String> excProductCodes) {
		if ( CollectionUtils.isNotEmpty( excProductCodes ) ) {
			List<CodeDescBean> itemCodeDesc = new ArrayList<CodeDescBean>();
			for ( String code : excProductCodes ) {
				itemCodeDesc.add( new CodeDescBean(code, null) );
			}
			this.excProductCodeDesc = itemCodeDesc;
		}
		else {
			this.excProductCodeDesc = new ArrayList<CodeDescBean>();
		}
	}

    public String getProducts() {
		return products;
	}

    public void setProducts(String products) {
		this.products = products;
	}

    public String getProductsCfns() {
		return productsCfns;
	}

    public void setProductsCfns(String productsCfns) {
		this.productsCfns = productsCfns;
	}

    public String getExcludedProducts() {
		return excludedProducts;
	}

    public void setExcludedProducts(String excludedProducts) {
		this.excludedProducts = excludedProducts;
	}

	public Boolean getIsUsed() {
		return isUsed;
	}

	public void setIsUsed(Boolean isUsed) {
		this.isUsed = isUsed;
	}





	public static class CodeDescBean {
        private String code;
        private String sku;
        private String description;
        private String type;
        private boolean isProduct;

        public CodeDescBean(String code, String description) {
            this.code = code;
            this.description = description;
        }

        public CodeDescBean(String code, String description, String type) {
            this.code = code;
            this.description = description;
            this.type = type;
        }

        public CodeDescBean(String code, String description, String type, boolean isProduct) {
            this.code = code;
            this.description = description;
            this.type = type;
            this.isProduct = isProduct;
        }

        public CodeDescBean(String code, String sku, String description, String type, boolean isProduct) {
            this.code = code;
            this.sku = sku;
            this.description = description;
            this.type = type;
            this.isProduct = isProduct;
        }

		public String getCode() {
			return code;
		}
		public void setCode(String code) {
			this.code = code;
		}
		public String getSku() {
			return sku;
		}
		public void setSku(String sku) {
			this.sku = sku;
		}
		public String getDescription() {
			return description;
		}
		public void setDescription(String description) {
			this.description = description;
		}
		public String getType() {
			return type;
		}
		public void setType(String type) {
			this.type = type;
		}
		public boolean isProduct() {
			return isProduct;
		}
		public void setProduct(boolean isProduct) {
			this.isProduct = isProduct;
		}
    }

}
