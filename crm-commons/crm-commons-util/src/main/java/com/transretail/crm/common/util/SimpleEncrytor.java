package com.transretail.crm.common.util;

import java.io.IOException;
import java.io.UnsupportedEncodingException;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.DESKeySpec;

import sun.misc.BASE64Decoder;
import sun.misc.BASE64Encoder;

/**
 * @author Allan G. Ramirez (agramirez@exist.com)
 */
public final class SimpleEncrytor {
    public static final String DEFAULT_KEY = "c@rr3f0ur_mm$";
    private static final String DEFAULT_ENCODING = "UTF-8";
    private static final BASE64Encoder BASE_64_ENCODER = new BASE64Encoder();
    private static final BASE64Decoder BASE_64_DECODER = new BASE64Decoder();

    private String encoding = DEFAULT_ENCODING;
    private Cipher ecipher;
    private Cipher dcipher;

    private static class SingletonHolder {
        private static final SimpleEncrytor INSTANCE = new SimpleEncrytor();

        static {
            try {
                DESKeySpec keySpec = new DESKeySpec(DEFAULT_KEY.getBytes(DEFAULT_ENCODING));
                SecretKeyFactory keyFactory = SecretKeyFactory.getInstance("DES");
                SecretKey key = keyFactory.generateSecret(keySpec);

                Cipher ecipher = Cipher.getInstance("DES");
                Cipher dcipher = Cipher.getInstance("DES");

                ecipher.init(Cipher.ENCRYPT_MODE, key);
                dcipher.init(Cipher.DECRYPT_MODE, key);

                INSTANCE.ecipher = ecipher;
                INSTANCE.dcipher = dcipher;
            } catch (Exception e) {
                throw new RuntimeException(e);
            }
        }
    }

    public static class EncryptDecryptException extends Exception {
        public EncryptDecryptException(Throwable e) {
            super(e);
        }
    }

    private SimpleEncrytor() {

    }

    public static SimpleEncrytor getInstance() {
        return SingletonHolder.INSTANCE;
    }

    public static SimpleEncrytor newInstance(String encoding) {
        return newInstance(DEFAULT_KEY, encoding);
    }

    public static SimpleEncrytor newInstance(String privateKey, String encoding) {
        try {
            SimpleEncrytor encrytor = new SimpleEncrytor();
            DESKeySpec keySpec = new DESKeySpec(privateKey.getBytes(encoding));
            SecretKeyFactory keyFactory = SecretKeyFactory.getInstance("DES");
            SecretKey key = keyFactory.generateSecret(keySpec);

            encrytor.encoding = encoding;
            encrytor.ecipher = Cipher.getInstance("DES");
            encrytor.dcipher = Cipher.getInstance("DES");

            encrytor.ecipher.init(Cipher.ENCRYPT_MODE, key);
            encrytor.dcipher.init(Cipher.DECRYPT_MODE, key);

            return encrytor;
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    public String encrypt(String rawText) throws EncryptDecryptException {
        try {
            return BASE_64_ENCODER.encode(ecipher.doFinal(rawText.getBytes(encoding)));
        } catch (UnsupportedEncodingException e) {
            throw new EncryptDecryptException(e);
        } catch (IllegalBlockSizeException e) {
            throw new EncryptDecryptException(e);
        } catch (BadPaddingException e) {
            throw new EncryptDecryptException(e);
        }
    }


    public String decrypt(String encryptedText) throws EncryptDecryptException {
        try {
            byte[] encrypedPwdBytes = BASE_64_DECODER.decodeBuffer(encryptedText);
            byte[] plainTextPwdBytes = dcipher.doFinal(encrypedPwdBytes);
            return new String(plainTextPwdBytes, encoding);
        } catch (IOException e) {
            throw new EncryptDecryptException(e);
        } catch (IllegalBlockSizeException e) {
            throw new EncryptDecryptException(e);
        } catch (BadPaddingException e) {
            throw new EncryptDecryptException(e);
        }
    }

}
