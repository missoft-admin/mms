package com.transretail.crm.giftcard.entity.support;


public enum SalesOrderDiscountType {

    DIRECT_DISCOUNT, VOUCHER, CASH_BACK
}
