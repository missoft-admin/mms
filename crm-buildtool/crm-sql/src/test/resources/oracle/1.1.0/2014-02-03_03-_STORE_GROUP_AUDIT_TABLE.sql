	create table CRM_STORE_GROUP_AUD (
        id number(19,0) not null,
        REV number(19,0) not null,
        REVTYPE number(3,0),
        NAME varchar2(255 char),
        STORES varchar2(4000 char),
        primary key (id, REV)
    );

	alter table CRM_STORE_GROUP_AUD 
        add constraint FK_6tf52dp4kcipdb7e4owwoq2th 
        foreign key (REV) 
        references CRM_REVINFO;