-- New MO Date column for CRM_MO table
ALTER TABLE CRM_MO ADD MO_DATE DATE;

-- New Batch column column for CRM_MO_TIER table
ALTER TABLE CRM_MO_TIER ADD BATCH_NO VARCHAR2(255 CHAR);

-- New Box no column for CRM_LC_INVENTORY table
ALTER TABLE CRM_LC_INVENTORY ADD BOX_NO VARCHAR2(255 CHAR);