ALTER TABLE CRM_SALES_ORD ADD (RETURN number(19,0)); 

    alter table CRM_SALES_ORD 
        add constraint FK_s6wkhm05wtt22hm5vkptsmtb7 
        foreign key (RETURN) 
        references CRM_GC_RETURN;