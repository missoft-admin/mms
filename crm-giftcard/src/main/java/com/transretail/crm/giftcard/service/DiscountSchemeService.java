package com.transretail.crm.giftcard.service;

import java.math.BigDecimal;

import org.joda.time.LocalDate;

import com.transretail.crm.common.service.dto.request.PageSortDto;
import com.transretail.crm.common.service.dto.response.ResultList;
import com.transretail.crm.giftcard.dto.GiftCardDiscountSchemeDto;
import com.transretail.crm.giftcard.entity.GiftCardGeneralDiscount;
import com.transretail.crm.giftcard.entity.support.SalesOrderDiscountType;
import com.transretail.crm.giftcard.service.DiscountSchemeService.DiscountDto;


/**
 *
 */
public interface DiscountSchemeService {

	void saveScheme(GiftCardDiscountSchemeDto schemeDto);

	ResultList<GiftCardDiscountSchemeDto> list(PageSortDto sortDto);

	GiftCardDiscountSchemeDto getSchemeDto(Long id);

	void deleteScheme(Long id);
	
	
	public class DiscountDto {
		private BigDecimal discount = BigDecimal.ZERO;
		private BigDecimal voucher;
		private BigDecimal purchase;
		private boolean isGeneral = false;
		public BigDecimal getDiscount() {
			return discount;
		}
		public void setDiscount(BigDecimal discount) {
			this.discount = discount;
		}
		public BigDecimal getVoucher() {
			return voucher;
		}
		public void setVoucher(BigDecimal voucher) {
			this.voucher = voucher;
		}
		public BigDecimal getPurchase() {
			return purchase;
		}
		public void setPurchase(BigDecimal purchase) {
			this.purchase = purchase;
		}
		public boolean isGeneral() {
			return isGeneral;
		}
		public void setGeneral(boolean isGeneral) {
			this.isGeneral = isGeneral;
		}
		
		
		
	}


	DiscountDto getDiscount(Long customerId, GiftCardDiscountSchemeDto discountScheme, BigDecimal purchase);

	DiscountDto getDiscount(Long customerId, GiftCardDiscountSchemeDto discountScheme,
			BigDecimal purchase, BigDecimal accumulatedPurchase);

	DiscountDto getDiscount(Long customerId, BigDecimal purchase,
			LocalDate orderDate, SalesOrderDiscountType discType);

	boolean validateForOverlappingDiscount(LocalDate startDate,
			LocalDate endDate, Long customerId, Long id);

	DiscountDto getGeneralDiscountDto(BigDecimal purchase,
			SalesOrderDiscountType discType);

	GiftCardGeneralDiscount getGeneralDiscount(BigDecimal purchase);

	void generateDiscVoucherForRegYearlyDisc(LocalDate eocDate);

	void generateDiscVoucherForSOYearlyDisc(LocalDate eocDate);

	GiftCardDiscountSchemeDto getDiscountScheme(Long customerId,
			LocalDate orderDate);

	DiscountDto getDiscountBeforeApproval(Long customerId, BigDecimal purchase,
			LocalDate orderDate, SalesOrderDiscountType discType);
	
	
}
