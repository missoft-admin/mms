alter table CRM_EMPLOYEE_PURCHASE_TXN drop column EXPIRE_EXCESS;
alter table CRM_EMPLOYEE_PURCHASE_TXN drop column EXPIRY_DATE;
alter table CRM_EMPLOYEE_PURCHASE_TXN drop column LAST_REDEEM;

alter table CRM_POINTS add VALID_PERIOD integer;