<%@ include file="../../common/taglibs.jsp" %>


<div id="content_productGroupItems" class="modal hide  nofly modal-dialog">

<style type="text/css">
<!-- 
  #list_productCfn { margin-bottom: 70px !important; } 
  .dataTables_info { width: 50%; }
  .content_groups .pull-left { margin-left: 10px}
  .content_groups .pull-right { margin-right: 10px; }
  .content_groups .pull-left, .content_groups .pull-right {width: 47%;}
  .dataTables_length { width: 65% !important; }
  .table-col-medium { width: 80px; }
  .content_groups {min-width: 680px}
-->
</style>

  <div class="modal-content">
    <div class="modal-header">
      <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
      <h4 id="productGroupName"></h4>
    </div>

    <div class="modal-body">
      <div class="content_groups">
	      <div class="pull-left">
	        <h4><spring:message code="group_product_andclassifications_selected" /></h4>
	        <div id="list_productCfn" data-url="<c:url value="/groups/product/productcfns" />"
	          data-hdr-code="<spring:message code="label_group_code"/>" data-hdr-desc="<spring:message code="label_group_description"/>"  ></div>
	      </div>
	
	      <div class="pull-right">
	        <h4><spring:message code="group_product_excluded" /></h4>
	        <div id="list_excProduct" data-url="<c:url value="/groups/product/excproducts" />"
	          data-hdr-code="<spring:message code="label_group_code"/>" data-hdr-desc="<spring:message code="label_group_description"/>"  ></div>
	      </div>
      </div>
    </div>

    <div class="modal-footer">
      <button type="button" class="btn btn-primary" data-dismiss="modal">OK</button>
    </div>
  </div>

</div>

<script type="text/javascript">
var ProductGroupItems = {
		show  : function( groupId, groupName ) {
			var searchObj = new Object();
			searchObj.id = groupId;
			$( "#content_productGroupItems" ).find( "#productGroupName" ).html( groupName );
		  $( "#content_productGroupItems" ).modal( "show" );
			$( "#content_productGroupItems" ).find( "#list_productCfn" ).ajaxDataTable( "search", searchObj );
			$( "#content_productGroupItems" ).find( "#list_excProduct" ).ajaxDataTable( "search", searchObj );
		}
};
$(document).ready( function() {
   var $dialog = $( "#content_productGroupItems" ), $productCfnList = $( "#list_productCfn" ), $excProductList = $( "#list_excProduct" );



    initDialog();
    initDataTable();



    function initDialog() {
	      $dialog.modal({ show : false });
	      $dialog.on( "show", function(e) { if ( e.target === this ) {} });
    }
    
    function initDataTable() {
	    	$productCfnList.ajaxDataTable({ 'autoload'    : false, 'ajaxSource'  : $productCfnList.data( "url" ),
		 	      'columnHeaders' : [ { text: $productCfnList.data( "hdr-code" ), className : "input-small" }, { text: $productCfnList.data( "hdr-desc" ), className : "input-medium" } ],
		 	      'modelFields'   : [ { name  : 'code', sortable : true }, { name  : 'description', sortable : true } ]
	    	});
	      $excProductList.ajaxDataTable({ 'autoload'    : false, 'ajaxSource'  : $excProductList.data( "url" ),
		        'columnHeaders' : [ { text: $excProductList.data( "hdr-code" ), className : "input-small" }, { text: $excProductList.data( "hdr-desc" ), className : "input-medium" } ],
		        'modelFields'   : [ { name  : 'code', sortable : true }, { name  : 'description', sortable : true } ]
	      });
    }

});
</script>