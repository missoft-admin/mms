<%@include file="/WEB-INF/views/common/taglibs.jsp" %>
<div class="modal hide  nofly in" id="safetyStockDialog" tabindex="-1" role="dialog" aria-hidden="false"
     style="none; margin-left: -450px;">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close cancel" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4><spring:message code="safetystock.dialog.title"/><span></span></h4>
      </div>
      <div class="modal-body">
        <div class="messagesDiv">

        </div>
        <div class="row-fluid form-horizontal form">
          <input type="hidden" name="productProfileId"/>

          <div class="span6">
            <div class="control-group">
              <label class="control-label"><spring:message code="january"/></label>

              <div class="controls">
                <input type="text" class="number" name="month1"/>
              </div>
            </div>
            <div class="control-group">
              <label class="control-label"><spring:message code="february"/></label>

              <div class="controls">
                <input type="text" class="number" name="month2"/>
              </div>
            </div>
            <div class="control-group">
              <label class="control-label"><spring:message code="march"/></label>

              <div class="controls">
                <input type="text" class="number" name="month3"/>
              </div>
            </div>
            <div class="control-group">
              <label class="control-label"><spring:message code="april"/></label>

              <div class="controls">
                <input type="text" class="number" name="month4"/>
              </div>
            </div>
            <div class="control-group">
              <label class="control-label"><spring:message code="may"/></label>

              <div class="controls">
                <input type="text" class="number" name="month5"/>
              </div>
            </div>
            <div class="control-group">
              <label class="control-label"><spring:message code="june"/></label>

              <div class="controls">
                <input type="text" class="number" name="month6"/>
              </div>
            </div>
          </div>
          <div class="span6">
            <div class="control-group">
              <label class="control-label"><spring:message code="july"/></label>

              <div class="controls">
                <input type="text" class="number" name="month7"/>
              </div>
            </div>
            <div class="control-group">
              <label class="control-label"><spring:message code="august"/></label>

              <div class="controls">
                <input type="text" class="number" name="month8"/>
              </div>
            </div>
            <div class="control-group">
              <label class="control-label"><spring:message code="september"/></label>

              <div class="controls">
                <input type="text" class="number" name="month9"/>
              </div>
            </div>
            <div class="control-group">
              <label class="control-label"><spring:message code="october"/></label>

              <div class="controls">
                <input type="text" class="number" name="month10"/>
              </div>
            </div>
            <div class="control-group">
              <label class="control-label"><spring:message code="november"/></label>

              <div class="controls">
                <input type="text" class="number" name="month11"/>
              </div>
            </div>
            <div class="control-group">
              <label class="control-label"><spring:message code="december"/></label>

              <div class="controls">
                <input type="text" class="number" name="month12"/>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary save"><spring:message code="save"/></button>
        <button type="button" class="btn cancel"><spring:message code="cancel"/></button>
      </div>
    </div>
  </div>
</div>
<script>
  $( document ).ready( function () {
    var $safetyStockDialog = $( '#safetyStockDialog' ).modal( {
      show : false
    } );
    $( "#product_profile_list_container" ).on( 'click', '.viewSafetyStock', function () {
      var productProfileId = $( this ).data( 'id' );
      var $tr = $( this ).parents( 'tr' );
      var tds = $( 'td', $tr );
      var productCode = tds[0].innerHTML;
      var productDesc = tds[1].innerHTML;

      $.get( ctx + '/safetystock/' + productProfileId, function ( dto ) {
        $( 'input[name="productProfileId"]', $safetyStockDialog ).val( productProfileId );
        $( '.modal-header h4 span', $safetyStockDialog ).text( ' - ' + productCode + ' - ' + productDesc );
        $safetyStockDialog.modal( 'toggle' );

        for ( var i = 1; i <= 12; i++ ) {
          var month = 'month' + i;
          $( 'input[name="' + month + '"]', $safetyStockDialog ).val( dto[month] );
        }
      }, 'json' );
    } );

    $( '.number', $safetyStockDialog ).numberInput();

    $( '.cancel', $safetyStockDialog ).click( function () {
      $safetyStockDialog.modal( 'toggle' );
    } );

    $( '.save', $safetyStockDialog ).click( function () {
      var formDataObject = $( '.form', $safetyStockDialog ).toObject( {mode : 'first', skipEmpty : false} );
      var showErrorMessage = function ( message ) {
        $( '.messagesDiv', $safetyStockDialog ).html( '<div class="alert alert-error">' +
                '<button type="button" class="close" data-dismiss="alert">&times;</button>' +
                message +
                '</div>' );
      };

      $.ajax( {
        "contentType" : 'application/json',
        headers : { 'Content-Type' : 'application/json' },
        "type" : "POST",
        "url" : ctx + '/safetystock/save',
        "data" : JSON.stringify( formDataObject ),
        "success" : function () {
          $safetyStockDialog.modal( 'toggle' );
        },
        "error" : function ( xhr, textStatus, error ) {
          if ( textStatus === 'timeout' ) {
            showErrorMessage( 'The server took too long to send the data.' );
          } else if ( error ) {
            showErrorMessage( error );
          } else {
            showErrorMessage( 'An error occurred on the server. Please try again.' );
          }
        }
      } );
    } );
  } );
</script>

