create table CRM_STORE_PSOFT (
        id number(19,0) not null,
        CREATED_BY varchar2(255 char),
        CREATED_DATETIME timestamp,
        LAST_UPDATED_BY varchar2(255 char),
        LAST_UPDATED_DATETIME timestamp,
        PSOFT_CODE varchar2(50 char),
        BU_CODE varchar2(15 char),
        STORE_CODE number(10,0),
        primary key (id)
    );

alter table CRM_STORE_PSOFT 
        add constraint FK_t0ia1pex0dgdbbi4mpo7yyeg9 
        foreign key (BU_CODE) 
        references CRM_REF_LOOKUP_DTL;

alter table CRM_STORE_PSOFT 
        add constraint FK_278449ghmjrywd3es587lfx79 
        foreign key (STORE_CODE) 
        references STORE;



INSERT INTO CRM_REF_LOOKUP_HDR ( CODE, DESCRIPTION ) VALUES ( 'STO000', 'BUSINESS UNIT' );
