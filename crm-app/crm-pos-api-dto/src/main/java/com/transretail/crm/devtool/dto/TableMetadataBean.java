package com.transretail.crm.devtool.dto;

/**
 * @author Allan G. Ramirez (agramirez@exist.com)
 */
public class TableMetadataBean extends ColumnMetadataBean {
    private String table;

    public String getTable() {
        return table;
    }

    public void setTable(String table) {
        this.table = table;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        TableMetadataBean that = (TableMetadataBean) o;

        if (!table.equals(that.table)) return false;

        return true;
    }

    @Override
    public int hashCode() {
        return table.hashCode();
    }
}
