package com.transretail.crm.web.giftcard.controller;

import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Lists;
import com.google.common.io.Files;
import com.transretail.crm.common.service.exception.GenericServiceException;
import com.transretail.crm.common.service.exception.MessageSourceResolvableException;
import com.transretail.crm.core.dto.MessageDto;
import com.transretail.crm.core.entity.CrmFile.FileType;
import com.transretail.crm.core.security.service.MD5Service;
import com.transretail.crm.core.service.CodePropertiesService;
import com.transretail.crm.core.service.impl.CrmFileServiceImpl;
import com.transretail.crm.core.util.generator.QrCodeService;
import com.transretail.crm.giftcard.GiftCardNotFoundException;
import com.transretail.crm.giftcard.GiftCardServiceResolvableException;
import com.transretail.crm.giftcard.dto.EGiftCardInfoDto;
import com.transretail.crm.giftcard.dto.GiftCardInfo;
import com.transretail.crm.giftcard.entity.GiftCardInventory;
import com.transretail.crm.giftcard.service.EGiftCardManager;
import com.transretail.crm.giftcard.service.GiftCardInventoryService;
import com.transretail.crm.giftcard.service.GiftCardManager;
import com.transretail.crm.media.service.MailService;
import com.transretail.crm.media.service.SmsService;
import com.transretail.crm.web.giftcard.controller.dto.*;
import com.transretail.crm.web.giftcard.controller.validation.RedemptionCheck;
import java.io.File;
import java.util.List;
import java.util.Set;
import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.BooleanUtils;
import org.apache.commons.lang3.StringUtils;
import org.joda.time.LocalDateTime;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.NoSuchMessageException;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.core.task.AsyncTaskExecutor;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

@Controller
@RequestMapping(value = "/giftcard/egc",
        consumes = MediaType.APPLICATION_JSON_VALUE,
        produces = MediaType.APPLICATION_JSON_VALUE)
public class EGiftCardRestController {

    @Autowired
    private GiftCardRestController giftCardRestController;
    @Autowired
    private EGiftCardManager egiftCardManager;
    @Autowired
    private GiftCardManager giftCardManager;
    @Autowired
    private GiftCardInventoryService giftCardInventoryService;
    @Autowired
    private MailService mailService;
    @Autowired
    private SmsService smsService;
    @Autowired
    private MessageSource messageSource;
    @Autowired
    private AsyncTaskExecutor asyncTaskExecutor;
    @Autowired
    private CodePropertiesService codePropertiesService;
    @Autowired
    private QrCodeService qrCodeService;
    @Autowired
    private MD5Service mD5Service;

    @RequestMapping(value = "/inquire/{barcode}")
    public @ResponseBody
    GiftCardResponse getEgc(@PathVariable String barcode) {
        GiftCardResponse res = new GiftCardResponse();
        if (StringUtils.isNotBlank(barcode)) {
            String[] barcodes = barcode.contains(",") ? StringUtils.split(barcode, ",") : new String[]{barcode};
            res.setCardInfos(egiftCardManager.inquire(barcodes));
            res.setSuccessful(true);
        }
        return res;
    }

    @RequestMapping(value = "/products/inquire/{merchantId}", consumes = MediaType.ALL_VALUE)
    @ResponseBody
    public GiftCardResponse inquire(@PathVariable(value = "merchantId") String location) {
        return new EGiftCardResponse()
                .withAvailableProducts(egiftCardManager.retrieveAvailableProfiles(location))
                .withSuccessful(true).withProcessedDateTime(LocalDateTime.now());
    }

    @RequestMapping(value = "/pre-activate", method = RequestMethod.POST)
    @ResponseBody
    public GiftCardResponse preActivateEgc(@Valid @RequestBody EGiftCardPreactivateRequest request) {
        this.validateSignature(request);
        final Set<GiftCardInfo> result = egiftCardManager.preactivate(request.toGiftCardTransactionInfo(),
                request.getProductCode(), request.getQuantity());

        return new GiftCardResponse().withCardInfos(result);
    }

    @RequestMapping(value = "/pre-activate/deallocate", method = RequestMethod.POST)
    public @ResponseBody
    GiftCardResponse deallocateEgc(@Valid @RequestBody EGiftCardDeallocateRequest request) {
        this.validateSignature(request);
        return new GiftCardResponse().withSuccessful(true).withProcessedDateTime(LocalDateTime.now())
                .withCardInfos(egiftCardManager.deallocate(request.toGiftCardTransactionInfo(), request.getCards()));
    }

    @RequestMapping(value = "/activate", method = RequestMethod.POST)
    @ResponseBody
    public  GiftCardResponse activateEgc(
            @Valid @RequestBody EGiftCardRequest eReq,
            HttpServletRequest req) throws MessageSourceResolvableException {

        GiftCardResponse gcRes = giftCardRestController.activate(eReq);
        if (gcRes.isSuccessful()) {
            EGiftCardResponse res = new EGiftCardResponse();
            BeanUtils.copyProperties(gcRes, res, new String[]{});

            List<GiftCardInfo> egcDtos = egiftCardManager.prepareMsgs(gcRes.getCardInfos(),
                    eReq.getEmail(), eReq.getMobileNo(), eReq.getPin(), eReq.getBirthdate(), EGiftCardRequest.PIN_PATTERN, this.getUrl(req));
            if (CollectionUtils.isNotEmpty(egcDtos)) {
                final Set<GiftCardInfo> smsDtos = ImmutableSet.copyOf(egcDtos);
                asyncTaskExecutor.execute(new Runnable() {
                    public void run() {
                        for (GiftCardInfo dto : smsDtos) {
                            try {
                                smsService.send(((EGiftCardInfoDto) dto).getSmsMsg());
                            } catch (Exception e) {
                            }
                        }
                    }
                });

                try {
                    for (GiftCardInfo dto : egcDtos) {
                        MessageDto email = ((EGiftCardInfoDto) dto).getEmailMsg();
                        email.setFile(this.createQrCode(this.setCode(req, email.getMessage1(), eReq.getEmail())));
                        email.setMessage1(StringUtils.EMPTY);
                        ((EGiftCardInfoDto) dto).setIsEmailSent(mailService.sendWithInlineFile(email));
                        egiftCardManager.update(((EGiftCardInfoDto) dto));//TODO update by bulk
                        //res.addCardInfo( dto );
                    }
                    res.setIsEmailSent(true);
                } catch (Exception e) {
                    throw new GiftCardServiceResolvableException(null, e.getMessage(), null, new Object[]{});
                }
            }
            return res;
        }
        return gcRes;
    }

    @RequestMapping(value = "/pin/change/{barcode}/{oldpin}/{pin}")
    @ResponseBody
    public GiftCardResponse updateEgcPin(@PathVariable String barcode, @PathVariable String oldpin, @PathVariable String pin) {
        return new EGiftCardResponse()
                .withMessage(messageSource.getMessage("gc.egc.msg.pinupdated", null, LocaleContextHolder.getLocale()))
                .withSuccessful(egiftCardManager.updatePin(barcode, oldpin, pin))
                .withProcessedDateTime(LocalDateTime.now());
    }

    @RequestMapping(value = "/redeem", method = RequestMethod.POST)
    @ResponseBody
    public GiftCardResponse redeem(@Validated(RedemptionCheck.class) @RequestBody EGiftCardRedeemRequest req) {
        EGiftCardResponse res = new EGiftCardResponse();
        final String cardNo = req.getCardNo();
        Boolean valid = egiftCardManager.validatePin(cardNo, req.getPin());
        if (BooleanUtils.isFalse(valid)) {
            throw GiftCardServiceResolvableException.invalidGiftCardPinException();
        }

        res.setSuccessful(true);
        GiftCardInventory inventory = giftCardManager.validateRedemption(
                req.toGiftCardTransactionInfo(), cardNo, req.getRedemptionAmount());
        res.addCardInfo(GiftCardInfo.convert(inventory));

        return res;
    }

    @RequestMapping(value = "/reload", method = RequestMethod.POST)
    public @ResponseBody
    GiftCardResponse reloadEgc(
            @Valid @RequestBody EGiftCardReloadRequest eReq,
            HttpServletRequest req) throws MessageSourceResolvableException {

        GiftCardResponse gcRes = giftCardRestController.reload(eReq);
        if (gcRes.isSuccessful()) {
            List<MessageDto> smsDtos = Lists.newArrayList();
            List<String> emailAdds = Lists.newArrayList();
            StringBuilder barcodes = new StringBuilder();
            MessageDto emailDto = new MessageDto();
            for (GiftCardInfo gcInfo : gcRes.getCardInfos()) {
                GiftCardInventory gc = giftCardInventoryService.getGiftCardByBarcode(gcInfo.getCardNumber());
                if (BooleanUtils.isTrue(gc.getIsEgc())) {
                    StringBuilder sb = new StringBuilder();
                    sb.append(messageSource.getMessage("gc.egc.abbr", null, LocaleContextHolder.getLocale()) + ": "
                            + gcInfo.getCardNumber() + "<br/>");
                    sb.append(messageSource.getMessage("gc.egc.reload.amt.abbr", null,
                            LocaleContextHolder.getLocale()) + ": " + eReq.getReloadAmount() + "<br/>");
                    sb.append(messageSource.getMessage("gc.egc.balance.abbr", null,
                            LocaleContextHolder.getLocale()) + ": " + gcInfo.getAvailableBalance() + "<br/>");
                    sb.append(messageSource.getMessage("gc.egc.expiration.date.abbr", null,
                            LocaleContextHolder.getLocale()) + ": " + gcInfo.getExpireDate() + "<br/>");

                    MessageDto smsDto = new MessageDto();
                    if (StringUtils.isNotBlank(eReq.getMobileNo())) {
                        List<String> mobileNos = Lists.newArrayList();
                        mobileNos.add(eReq.getMobileNo());
                        if (null != gc.getEgcInfo()
                                && !StringUtils.equalsIgnoreCase(eReq.getMobileNo(), gc.getEgcInfo().getMobileNo())) {
                            mobileNos.add(gc.getEgcInfo().getMobileNo());
                        }
                        smsDto.setRecipients(mobileNos.toArray(new String[mobileNos.size()]));
                    }
                    smsDto.setMessage(sb.toString());
                    smsDtos.add(smsDto);

                    barcodes.append(gcInfo.getCardNumber() + ", ");
                    emailDto.setMessage((StringUtils.isNotBlank(emailDto.getMessage()) ? emailDto.getMessage()
                            : StringUtils.EMPTY) + "<br/>" + sb.toString());
                    if (StringUtils.isNotBlank(eReq.getEmail()) && null != gc.getEgcInfo()
                            && !StringUtils.equalsIgnoreCase(eReq.getEmail(), gc.getEgcInfo().getEmail())) {
                        emailAdds.add(gc.getEgcInfo().getEmail());
                    }
                }
            }

            if (StringUtils.isNotBlank(eReq.getEmail())) {
                emailAdds.add(eReq.getEmail());
            }
            if (StringUtils.isNotBlank(barcodes)) {
                barcodes.setCharAt(barcodes.lastIndexOf(","), ' ');
            }
            String[] arEmails = emailAdds.toArray(new String[emailAdds.size()]);
            emailDto.setMessage1(this.setCode(req, barcodes.toString(), StringUtils.join(arEmails, ",")));
            emailDto.setRecipients(arEmails);
            emailDto.setSubject(messageSource.getMessage("gc.egc", null, LocaleContextHolder.getLocale()));
            send(emailDto, smsDtos);
        }
        return gcRes;
    }

    private void validateSignature(AbstractGiftCardRequest request) throws UnauthorizedException {
        if (!mD5Service.validateMd5(request.getSignature(), request.getValidationKey())) {
            throw new UnauthorizedException("Failed to perform request. Request validation key does not match!");
        }
    }

    private void send(final MessageDto emailDto, final List<MessageDto> smsDtos) {
        Runnable task = new Runnable() {
            public void run() {
                try {
                    if (StringUtils.isNotBlank(emailDto.getMessage1())) {
                        emailDto.setFile(createQrCode(emailDto.getMessage1()));
                        emailDto.setMessage1(StringUtils.EMPTY);
                    }
                    mailService.sendWithInlineFile(emailDto);
                } catch (Exception e) {

                }
                for (MessageDto smsDto : smsDtos) {
                    try {
                        smsService.send(smsDto);
                    } catch (Exception e) {

                    }
                }
            }
        };
        asyncTaskExecutor.execute(task);
    }

    private MultipartFile createQrCode(String code) {
        File baos = null;
        try {
            baos = qrCodeService.generateQrCode(code);
            return new CrmFileServiceImpl.MultipartFileImpl(Files.toByteArray(baos), "eGiftCard.png",
                    FileType.IMAGE.name());
        } catch (Exception e) {
        } finally {
            try {
            } catch (Exception e) {
            }
        }
        return null;
    }

    private String setCode(HttpServletRequest req, String cardNo, String email) {
        final String SEPARATOR = "|";
        String infoUrl = req.getScheme() + "://"
                + req.getLocalAddr() + ":" + req.getLocalPort()
                + req.getContextPath() + "/api/giftcard/egc/inquire/" + cardNo;
        StringBuilder sb = new StringBuilder();
        sb.append(StringUtils.replace(infoUrl, " ", ""));
        sb.append(SEPARATOR);
        sb.append(messageSource.getMessage("gc.egc.abbr", null, LocaleContextHolder.getLocale()) + ": " + cardNo);
        sb.append(SEPARATOR);
        sb.append(messageSource.getMessage("gc.egc.email.abbr", null, LocaleContextHolder.getLocale()) + ": " + email);
        return sb.toString();
    }

    private String getUrl(HttpServletRequest req) {
        StringBuilder sb = new StringBuilder();
        sb.append(req.getScheme())
                .append("://")
                .append(req.getLocalAddr())
                .append(":")
                .append(req.getLocalPort())
                .append(req.getContextPath())
                .append("/api/giftcard/egc/inquire/");
        return StringUtils.replace(sb.toString(), " ", "");
    }

    @ResponseBody
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler(MessageSourceResolvableException.class)
    public ErrorResponse handleMessage(MessageSourceResolvableException ex) {
        ErrorResponse errorResponse = new ErrorResponse();
        String errorMessage = null;
        try {
            errorMessage = messageSource.getMessage(ex.getLastCode(), ex.getArguments(),
                    LocaleContextHolder.getLocale());
        } catch (NoSuchMessageException e) {
            errorMessage = ex.getDefaultMessage();
        }

        errorResponse.setErrorMessage(errorMessage);
        errorResponse.setStatus(HttpStatus.BAD_REQUEST.value());

        return errorResponse;
    }

    @ResponseBody
    @ResponseStatus(HttpStatus.UNPROCESSABLE_ENTITY)
    @ExceptionHandler({GiftCardNotFoundException.class, GenericServiceException.class,
        MethodArgumentNotValidException.class, GiftCardServiceResolvableException.class})
    public ErrorResponse handleException(Exception ex) {
        ErrorResponse errorResponse = new ErrorResponse();
        errorResponse.setErrorCode("2000");
        errorResponse.setErrorMessage("Could not process the request. " + ex.getMessage());
        errorResponse.setStatus(HttpStatus.UNPROCESSABLE_ENTITY.value());

        if (ex instanceof MethodArgumentNotValidException) {
            BindingResult result = ((MethodArgumentNotValidException) ex).getBindingResult();
            errorResponse.setErrorMessage("Validation Error"); //TODO: 18ln
            for (FieldError fieldError : result.getFieldErrors()) {
                errorResponse.addFieldError(fieldError.getField(), fieldError.getDefaultMessage()); // TODO: Localize
                // Messages
            }
        } else if (ex instanceof GiftCardServiceResolvableException) {
            GiftCardServiceResolvableException gcse = (GiftCardServiceResolvableException) ex;
            errorResponse.setErrorCode(gcse.getErrorCode().getErrorCode());
            String errorMessage;
            try {
                errorMessage = messageSource.getMessage(gcse.getCodes()[0], gcse.getArguments(),
                        LocaleContextHolder.getLocale());
            } catch (NoSuchMessageException e) {
                errorMessage = gcse.getDefaultMessage();
            }

            errorResponse.setErrorMessage(errorMessage);
        }
        return errorResponse;
    }

    @ExceptionHandler({UnauthorizedException.class})
    @ResponseBody
    @ResponseStatus(HttpStatus.UNAUTHORIZED)
    public ErrorResponse handleUnauthorizedException(Exception ex) {
        ErrorResponse errorResponse = new ErrorResponse();
        errorResponse.setErrorCode(codePropertiesService.getDetailMessageCodeInvalidClientSignature()); // retain
        // error code and used this invalidClientSignature
        errorResponse.setStatus(HttpStatus.UNAUTHORIZED.value());
        errorResponse.setErrorMessage(ex.getMessage());

        return errorResponse;
    }

    @ExceptionHandler({InvalidCardException.class})
    @ResponseBody
    @ResponseStatus(HttpStatus.UNAUTHORIZED)
    public ErrorResponse handleInvalidCardException(Exception ex) {
        ErrorResponse errorResponse = new ErrorResponse();
        errorResponse.setErrorCode(codePropertiesService.getDetailMessageCodeInvalidCard()); // retain error code and
        // used this invalidClientSignature
        errorResponse.setStatus(HttpStatus.UNAUTHORIZED.value());
        errorResponse.setErrorMessage(ex.getMessage());

        return errorResponse;
    }
}
