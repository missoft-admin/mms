package com.transretail.crm.core.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.Parameter;
import org.hibernate.annotations.Type;
import org.hibernate.envers.Audited;
import org.hibernate.envers.NotAudited;
import org.joda.time.DateTime;
import org.joda.time.LocalDate;
import org.springframework.beans.BeanUtils;

import com.transretail.crm.core.dto.EmployeePurchaseTxnDto;
import com.transretail.crm.core.entity.enums.TxnStatus;
import com.transretail.crm.core.entity.enums.VoucherTransactionType;
import com.transretail.crm.core.entity.lookup.Store;
import com.transretail.crm.core.entity.support.CustomAuditableEntity;
import com.transretail.crm.core.entity.support.CustomIdAuditableEntity;

@Table(name = "CRM_EMPLOYEE_PURCHASE_TXN")
@Entity
@Audited
public class EmployeePurchaseTxnModel extends CustomIdAuditableEntity<String>  {

    private static final long serialVersionUID = 12121L;

    private static final String[] IGNORE = {"store", "storeModel", "memberModel"};
    
    @ManyToOne
    @JoinColumn(name = "ACCOUNT_ID")
    @NotAudited
    private MemberModel memberModel;

    /*
  Requested by Iman for easy mapping of crm_point record to account id in cases where member record in crm_table gets deleted

  will just use views instead

    @Column(name = "MEMBER_ACCOUNT_ID")
    private String memberAccountId;
     */
    @Column(name = "TXN_NO")
    private String transactionNo;

    @Column(name = "TXN_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date transactionDateTime;

    @Column(name = "TXN_AMOUNT")
    private Double transactionAmount;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "STORE_CODE")
    @NotAudited
    private Store store;
    
    @Column(name = "STATUS", length = 20)
    @Enumerated(EnumType.STRING)
    private TxnStatus status;
    
    @Column(name = "TXN_TYPE")
    @Enumerated(EnumType.STRING)
    private VoucherTransactionType transactionType;
    
    @Column(name = "REASON", length = 10000)
    private String reason;
    
    @Column(name = "REMARKS", length = 10000)
    private String remarks;
    
    @Column(name = "APPROVED_BY")
    private String approvedBy;
    
    @Column(name = "APPROVAL_DATE")
    @Type(type = "org.jadira.usertype.dateandtime.joda.PersistentDateTime",
    parameters = { @Parameter(name = "databaseZone", value = "jvm") })
    private DateTime approvalDate;
    
    public EmployeePurchaseTxnModel() {
    	
    }
    
    public EmployeePurchaseTxnModel(EmployeePurchaseTxnDto employeePurchaseTxnDto) {
    	setId(employeePurchaseTxnDto.getId());
    	BeanUtils.copyProperties(employeePurchaseTxnDto, this, IGNORE);
    	if(employeePurchaseTxnDto.getStoreModel() != null)
    		store = new Store(employeePurchaseTxnDto.getStoreModel());
    	if(employeePurchaseTxnDto.getMemberModel() != null)
    		memberModel = new MemberModel(employeePurchaseTxnDto.getMemberModel());
	}

  /*  public String getMemberAccountId() {
        return memberAccountId;
    }

    public void setMemberAccountId(String memberAccountId) {
        this.memberAccountId = memberAccountId;
    }*/

    public MemberModel getMemberModel() {
		return memberModel;
	}

	public void setMemberModel(MemberModel memberModel) {
		this.memberModel = memberModel;
	}

    public String getTransactionNo() {
        return transactionNo;
    }

    public void setTransactionNo(String transactionNo) {
        this.transactionNo = transactionNo;
    }

    public Date getTransactionDateTime() {
        return transactionDateTime;
    }

    public void setTransactionDateTime(Date transactionDateTime) {
        this.transactionDateTime = transactionDateTime;
    }

    public Double getTransactionAmount() {
        return transactionAmount;
    }

    public void setTransactionAmount(Double transactionAmount) {
        this.transactionAmount = transactionAmount;
    }

    public Store getStore() {
        return store;
    }

    public void setStore(Store store) {
        this.store = store;
    }

	public TxnStatus getStatus() {
		return status;
	}

	public void setStatus(TxnStatus status) {
		this.status = status;
	}

	public VoucherTransactionType getTransactionType() {
		return transactionType;
	}

	public void setTransactionType(VoucherTransactionType transactionType) {
		this.transactionType = transactionType;
	}

	public String getReason() {
		return reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	public String getRemarks() {
		return remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	public String getApprovedBy() {
		return approvedBy;
	}

	public void setApprovedBy(String approvedBy) {
		this.approvedBy = approvedBy;
	}

	public DateTime getApprovalDate() {
		return approvalDate;
	}

	public void setApprovalDate(DateTime approvalDate) {
		this.approvalDate = approvalDate;
	}


}
