package com.transretail.crm.giftcard.dto;

import java.math.BigDecimal;
import java.util.List;

import org.joda.time.DateTime;
import org.joda.time.LocalDate;
import org.springframework.format.annotation.DateTimeFormat;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.google.common.collect.Lists;
import com.transretail.crm.common.web.converter.CurrencySerializer;
import com.transretail.crm.common.web.converter.LocalDateSerializer;
import com.transretail.crm.core.entity.lookup.LookupDetail;
import com.transretail.crm.giftcard.entity.SalesOrder;
import com.transretail.crm.giftcard.entity.SalesOrderPaymentInfo;
import com.transretail.crm.giftcard.entity.support.PaymentInfoStatus;
import com.transretail.crm.giftcard.entity.support.SalesOrderType;

public class SalesOrderPaymentInfoDto {
	
	private Long id;
	
	private Long orderId;
	private String orderNo;
	private String customerName;
	private SalesOrderType orderType;
	
	
	private DateTime lastUpdated;
	
	@DateTimeFormat(pattern = "dd-MM-yyyy")
	@JsonSerialize(using = LocalDateSerializer.class)
	private LocalDate orderDate;
	
	@DateTimeFormat(pattern = "dd-MM-yyyy")
	@JsonSerialize(using = LocalDateSerializer.class)
	private LocalDate paymentDate;
	
	private LookupDetail paymentType;
	private BigDecimal paymentAmount;
	private String paymentDetails;
	
	private PaymentInfoStatus status;
	
	public SalesOrderPaymentInfoDto(){}
	
	public SalesOrderPaymentInfoDto(SalesOrderPaymentInfo payment) {
		this.id = payment.getId();
		
		if(payment.getOrder() != null) {
			SalesOrder order = payment.getOrder();
			this.orderId = order.getId();
			this.orderNo = order.getOrderNo();
			if(order.getCustomer() != null)
				this.customerName = order.getCustomer().getName();
			this.orderDate = order.getOrderDate();
			this.orderType = order.getOrderType();
		}
		
		this.paymentDate = payment.getPaymentDate();
		this.paymentType = payment.getPaymentType();
		this.paymentAmount = payment.getPaymentAmount();
		this.paymentDetails = payment.getPaymentDetails();
		this.status = payment.getStatus();
		
		this.lastUpdated = payment.getLastUpdated();
	}
	
	public SalesOrderPaymentInfo toModel(SalesOrderPaymentInfo payment) {
		if(payment == null)
			payment = new SalesOrderPaymentInfo();
		
		if(this.orderId != null)
			payment.setOrder(new SalesOrder(this.orderId));
		
		payment.setPaymentDate(this.paymentDate);
		payment.setPaymentType(this.paymentType);
		payment.setPaymentAmount(this.paymentAmount);
		payment.setPaymentDetails(this.paymentDetails);
		payment.setStatus(this.status);
		
		return payment;
	}
	
	public static List<SalesOrderPaymentInfoDto> toDtos(Iterable<SalesOrderPaymentInfo> payments) {
		List<SalesOrderPaymentInfoDto> paymentDtos = Lists.newArrayList();
		for(SalesOrderPaymentInfo payment: payments) {
			paymentDtos.add(new SalesOrderPaymentInfoDto(payment));
		}
		return paymentDtos;
	}
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getOrderId() {
		return orderId;
	}
	public void setOrderId(Long orderId) {
		this.orderId = orderId;
	}
	public String getOrderNo() {
		return orderNo;
	}
	public void setOrderNo(String orderNo) {
		this.orderNo = orderNo;
	}
	public String getCustomerName() {
		return customerName;
	}
	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}
	public LocalDate getOrderDate() {
		return orderDate;
	}
	public void setOrderDate(LocalDate orderDate) {
		this.orderDate = orderDate;
	}
	public LocalDate getPaymentDate() {
		return paymentDate;
	}
	public String getPaymentDateStr() {
		if(paymentDate != null)
			return paymentDate.toString("dd-MM-yyyy");
		return "";
	}
	public void setPaymentDate(LocalDate paymentDate) {
		this.paymentDate = paymentDate;
	}
	public LookupDetail getPaymentType() {
		return paymentType;
	}
	public void setPaymentType(LookupDetail paymentType) {
		this.paymentType = paymentType;
	}
	public BigDecimal getPaymentAmount() {
		return paymentAmount;
	}
	@JsonInclude(Include.ALWAYS)
    @JsonSerialize(using = CurrencySerializer.class)
	public BigDecimal getPaymentAmountFmt() {
		return paymentAmount;
	}
	public void setPaymentAmount(BigDecimal paymentAmount) {
		this.paymentAmount = paymentAmount;
	}
	public String getPaymentDetails() {
		return paymentDetails;
	}
	public void setPaymentDetails(String paymentDetails) {
		this.paymentDetails = paymentDetails;
	}

	public PaymentInfoStatus getStatus() {
		return status;
	}

	public void setStatus(PaymentInfoStatus status) {
		this.status = status;
	}

	public SalesOrderType getOrderType() {
		return orderType;
	}

	public void setOrderType(SalesOrderType orderType) {
		this.orderType = orderType;
	}

	public DateTime getLastUpdated() {
		return lastUpdated;
	}

	public void setLastUpdated(DateTime lastUpdated) {
		this.lastUpdated = lastUpdated;
	}
	
	public String getStatusName() {
	    if (this.status != null)
	        return status.name();
	    else
	        return null;
	}
	
	public String getPaymentTypeCode() {
	    if (this.paymentType != null)
	        return this.paymentType.getCode();
	    else
	        return null;
	}

	public String getPaymentTypeDescription() {
        if (this.paymentType != null)
            return this.paymentType.getDescription();
        else
            return null;
    }
	
	public String getOrderTypeName() {
	    if (this.orderType != null)
            return orderType.name();
        else
            return null;
	}
}
