/*
 * Copyright (c) 2012.
 * All rights reserved.
 */

package com.transretail.crm.common.security.model;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.springframework.security.core.GrantedAuthority;


public abstract class AbstractSecurityGrantedAuthority implements GrantedAuthority {

    private static final int _3 = 3;

    private static final int _17 = 17;

    @Override
    public int hashCode() {
        // Hashcode is based solely on the authority only
        return new HashCodeBuilder(_3, _17)
                .append(getAuthority())
                .toHashCode();
    }

    @Override
    public boolean equals(Object obj) {

        if (null == obj) {
            return false;
        }

        if (this == obj) {
            return true;
        }

        if (getClass() != obj.getClass()) {
            return false;
        }

        // Equality is based solely on the authority only.
        GrantedAuthority rhs = (GrantedAuthority) obj;
        return new EqualsBuilder()
                .append(getAuthority(), rhs.getAuthority())
                .isEquals();
    }

    @Override
    public String toString() {
        return getAuthority();
    }
}
