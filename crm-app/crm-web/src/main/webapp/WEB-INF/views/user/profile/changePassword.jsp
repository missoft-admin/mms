<%@ include file="../../common/taglibs.jsp" %>

<form:form modelAttribute="form" method="POST" cssClass="form-horizontal">
<div class="page-header page-header2">
  <h1><spring:message code="menu_user_changepassword"/></h1>
</div>

<jsp:include page="passwordPane.jsp" />

<div class="form-actions">
  <button type="submit" class="btn btn-primary"><spring:message code="button_submit" /></button>
  <a class="btn" href="<spring:url value="/" />"><spring:message code="button_cancel" /></a>
</div>
</form:form>
<!--<style>
form {
width: 500px;
margin: 0 auto;
}
</style>-->