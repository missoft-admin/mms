<%@ include file="../common/taglibs.jsp" %>
     
  <spring:message var="typeName" code='label_group_store'/>
  
  <script>
  
  function reloadItemsTable(groupId) {
    
    $("#list_items").empty().ajaxDataTable({
        'autoload'  : true,
        'ajaxSource' : "<c:url value="/groups/store/items/list/" />" + groupId,
        'columnHeaders' : [
        "<spring:message code="label_group_storecode"/>",
        "<spring:message code="label_group_storename"/>"
        ],
        'modelFields' : [
           {name : 'code', sortable : true},
           {name : 'name', sortable : true}
         ]
    });  
  }
    
  </script>
  
<div  class="modal hide  groupModal nofly" id="viewItemsDialog" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4>View Stores</h4>
      </div>
      
      <div class="modal-body">
        <div id="content_itemList">
          <div id="list_items"></div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary" data-dismiss="modal">OK</button>
      </div>
    </div>
  </div>
</div>
