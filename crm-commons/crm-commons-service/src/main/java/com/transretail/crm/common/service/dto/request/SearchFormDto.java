package com.transretail.crm.common.service.dto.request;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.mysema.query.types.expr.BooleanExpression;

/**
 *
 */
public interface SearchFormDto {
    @JsonIgnore
    BooleanExpression createSearchExpression();

    PagingParam getPagination();
}
