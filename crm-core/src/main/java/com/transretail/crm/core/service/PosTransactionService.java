package com.transretail.crm.core.service;

import java.io.InputStream;
import java.util.List;
import java.util.Map;

import com.transretail.crm.common.reporting.jasper.JRProcessor;
import com.transretail.crm.common.service.dto.request.PageSortDto;
import com.transretail.crm.common.service.dto.response.ResultList;
import com.transretail.crm.core.dto.PosTxItemResultList;
import com.transretail.crm.core.dto.PosTxItemSearchDto;
import com.transretail.crm.core.dto.ShoppingListDto;
import com.transretail.crm.core.entity.MemberModel;
import com.transretail.crm.core.entity.PosTransaction;
import com.transretail.crm.core.entity.lookup.Product;

public interface PosTransactionService {

    PosTransaction getTransactionWithSoldItems(String id);

    List<Product> getSoldProductsFromTransaction(String id);

    PosTxItemResultList getPosTxItems(PosTxItemSearchDto dto);

	PosTransaction getTransaction(String id);
	
	Double getAverageQuantityPerTransaction(String accountId, String productId);
	
	Map<String, ShoppingListDto> getShoppingSuggestions(String accountId);
	
	ResultList<ShoppingListDto> getProductRankingsByTransaction(String accountId);
	
	ResultList<ShoppingListDto> getProductRankingsByTransaction(String accountId, PageSortDto pageSort);
	
	void setShoppingListInclusiveTimeFrame(Integer timeInMonths);
    
    Integer getShoppingListInclusiveTimeFrame();
    
    void setShoppingListExclusiveTimeFrame(Integer timeInDays);
    
    Integer getShoppingListExclusiveTimeFrame();
    
    void setShoppingListMinTransactions(Integer minTransactions);
    
    Integer getShoppingListMinTransactions();
    
    JRProcessor createJRProcessor(MemberModel member, InputStream logo);
}
