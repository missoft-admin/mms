<%@ include file="../common/taglibs.jsp" %>
<div id="content_user">

  <div class="page-header page-header2"><h1><c:out value="${label_header}" /></h1></div>

  <form:form method="POST" commandName="fileUploadForm" enctype="multipart/form-data" class="form-horizontal">
    <div class="">
      <div class="control-group">
        <label class="control-label"><spring:message code="member.upload.select"/></label>

        <div class="controls">
          <input type="file" name="file" class=""/>
        </div>
      </div>
      <c:choose>
        <c:when test="${fn:length(memberTypes) > 1}">
          <div class="control-group">
            <label for="memberTypeCode" class="control-label"><spring:message code="memberType"/></label>

            <div class="controls">
              <form:select id="memberTypeCode" path="memberTypeCode" items="${memberTypes}" itemValue="id" itemLabel="name"/>
            </div>
          </div>
        </c:when>
        <c:otherwise>
          <%--There should always be 1 member type --%>
          <input type="hidden" name="memberTypeCode" value="${memberTypes[0].id}" />
        </c:otherwise>
      </c:choose>
      <div class="control-group">
        <label for="operation" class="control-label"><spring:message code="upload_operation"/></label>

        <div class="controls">
          <form:select id="operation" path="operation">
            <form:option value="NEW_MEMBERS" label="New" />
            <form:option value="UPDATE_EXISTING_MEMBERS" label="Update existing" />
          </form:select>
        </div>
      </div>

      <div class="control-group form-actions">
      <button  value="upload" class="btn btn-primary" class="btn btn-primary" type="submit">Upload</button><span><form:errors path="file" cssClass="error"/></span>
      </div>
    </div>
  </form:form>


  <style type="text/css"><!-- .data-content-04 { padding-top: 5px; } --></style>

</div>