
    create table CRM_ADDRESS (
        id number(19,0) not null,
        CREATED_BY varchar2(255 char),
        CREATED_DATETIME timestamp,
        LAST_UPDATED_BY varchar2(255 char),
        LAST_UPDATED_DATETIME timestamp,
        BLOCK varchar2(255 char),
        BUILDING varchar2(255 char),
        CITY varchar2(255 char),
        DISTRICT varchar2(255 char),
        FLOOR number(10,0),
        KM number(10,0),
        POST_CODE varchar2(255 char),
        ROOM varchar2(255 char),
        RT number(10,0),
        RW number(10,0),
        STREET varchar2(255 char),
        STR_NO number(10,0),
        SUBDISTRICT varchar2(255 char),
        VILLAGE varchar2(255 char),
        primary key (id)
    );

    create table CRM_AGE_OF_CHILDREN (
        ACCOUNT_ID number(19,0) not null,
        AGE number(10,0)
    );

    create table CRM_APPLICATION_CONFIG (
        id number(19,0) not null,
        CREATED_BY varchar2(255 char),
        CREATED_DATETIME timestamp,
        LAST_UPDATED_BY varchar2(255 char),
        LAST_UPDATED_DATETIME timestamp,
        CONFIG_KEY varchar2(20 char) not null,
        CONFIG_VALUE varchar2(255 char),
        primary key (id)
    );

    create table CRM_APPROVAL_MATRIX (
        id number(19,0) not null,
        CREATED_BY varchar2(255 char),
        CREATED_DATETIME timestamp,
        LAST_UPDATED_BY varchar2(255 char),
        LAST_UPDATED_DATETIME timestamp,
        AMOUNT double precision,
        MODEL_TYPE varchar2(255 char),
        USERNAME number(19,0),
        primary key (id)
    );

    create table CRM_APPROVAL_REMARK (
        id number(19,0) not null,
        CREATED_BY varchar2(255 char),
        CREATED_DATETIME timestamp,
        LAST_UPDATED_BY varchar2(255 char),
        LAST_UPDATED_DATETIME timestamp,
        REMARKS varchar2(255 char),
        PROMO_ID number(19,0),
        STATUS varchar2(15 char),
        primary key (id)
    );

    create table CRM_CAMPAIGN (
        id number(19,0) not null,
        CREATED_BY varchar2(255 char),
        CREATED_DATETIME timestamp,
        LAST_UPDATED_BY varchar2(255 char),
        LAST_UPDATED_DATETIME timestamp,
        DESCRIPTION varchar2(255 char),
        endDate timestamp,
        startDate timestamp,
        NAME varchar2(255 char),
        PROJECT_ID number(19,0),
        STATUS varchar2(15 char),
        primary key (id)
    );

    create table CRM_CARD_HISTORY (
        id number(19,0) not null,
        CREATED_BY varchar2(255 char),
        CREATED_DATETIME timestamp,
        LAST_UPDATED_BY varchar2(255 char),
        LAST_UPDATED_DATETIME timestamp,
        CARD_NO varchar2(255 char),
        REASON varchar2(255 char),
        TRANSACTION_DATE timestamp,
        primary key (id)
    );

    create table CRM_COMPANY_PROFILE (
        ID varchar2(255 char) not null,
        COPYRIGHT_TXT varchar2(255 char),
        LOGO blob,
        NAME varchar2(255 char),
        primary key (ID)
    );

    create table CRM_EMPLOYEE_PURCHASE_TXN (
        id varchar2(255 char) not null,
        CREATED_BY varchar2(255 char),
        CREATED_DATETIME timestamp,
        LAST_UPDATED_BY varchar2(255 char),
        LAST_UPDATED_DATETIME timestamp,
        REASON varchar2(255 char),
        REMARKS varchar2(255 char),
        STATUS varchar2(20 char),
        TXN_AMOUNT double precision,
        TXN_DATE timestamp,
        TXN_NO varchar2(255 char),
        TXN_TYPE varchar2(255 char),
        ACCOUNT_ID number(19,0),
        STORE_CODE number(10,0),
        primary key (id)
    );

    create table CRM_EMPLOYEE_VOUCHER_TXN (
        id number(19,0) not null,
        CREATED_BY varchar2(255 char),
        CREATED_DATETIME timestamp,
        LAST_UPDATED_BY varchar2(255 char),
        LAST_UPDATED_DATETIME timestamp,
        COMMENTS varchar2(255 char),
        STATUS varchar2(255 char),
        Store varchar2(255 char),
        TXN_AMOUNT double precision,
        TXN_DATE timestamp,
        TXN_NO varchar2(255 char),
        TXN_TYPE varchar2(255 char),
        primary key (id)
    );

    create table CRM_GC_INVENTORY (
        id number(19,0) not null,
        CREATED_BY varchar2(255 char),
        CREATED_DATETIME timestamp,
        LAST_UPDATED_BY varchar2(255 char),
        LAST_UPDATED_DATETIME timestamp,
        BARCODE varchar2(20 char) not null,
        BATCH_NO number(10,0) not null,
        CURR_VALUE number(12,4),
        EXPIRY_DATE date,
        FACE_VALUE number(12,4),
        ORDER_DATE timestamp not null,
        PRODUCT_NAME varchar2(100 char) not null,
        SEQ_NO number(10,0) not null,
        SERIES number(19,0) not null,
        STATUS varchar2(8 char) not null,
        ALLOCATE_TO number(10,0),
        LOCATION number(10,0),
        PRODUCT_CODE number(19,0) not null,
        primary key (id)
    );

    create table CRM_GC_ORD (
        id number(19,0) not null,
        CREATED_BY varchar2(255 char),
        CREATED_DATETIME timestamp,
        LAST_UPDATED_BY varchar2(255 char),
        LAST_UPDATED_DATETIME timestamp,
        CARD_ORDER_NO varchar2(255 char) not null,
        MEMO varchar2(300 char),
        ORDER_DATE timestamp not null,
        STATUS varchar2(15 char) not null,
        STATUS_LAST_UPDATED timestamp,
        VENDOR_NAME varchar2(255 char) not null,
        VENDOR number(19,0) not null,
        primary key (id)
    );

    create table CRM_GC_ORD_HIST (
        ID number(19,0) not null,
        CREATED_BY varchar2(255 char),
        CREATED_DATETIME timestamp,
        STATUS varchar2(15 char) not null,
        ORDER_NO number(19,0) not null,
        primary key (ID)
    );

    create table CRM_GC_ORD_ITEM (
        id number(19,0) not null,
        CREATED_BY varchar2(255 char),
        CREATED_DATETIME timestamp,
        LAST_UPDATED_BY varchar2(255 char),
        LAST_UPDATED_DATETIME timestamp,
        GENRATN_TYPE varchar2(15 char),
        GIFT_CARD_NAME varchar2(100 char),
        LPO varchar2(10 char),
        QUANTITY number(10,0) not null,
        STORE_NAME varchar2(255 char),
        GIFT_CARD number(19,0),
        GIFT_CARD_ORDER number(19,0),
        STORE_CODE number(10,0),
        primary key (id)
    );

    create table CRM_GC_TXNTRANSFER (
        id number(19,0) not null,
        CREATED_BY varchar2(255 char),
        CREATED_DATETIME timestamp,
        LAST_UPDATED_BY varchar2(255 char),
        LAST_UPDATED_DATETIME timestamp,
        PRODUCT_NAME varchar2(100 char) not null,
        SERIES varchar2(16 char) not null,
        GC_INVENTORY number(19,0),
        primary key (id)
    );

    create table CRM_GC_VENDOR (
        id number(19,0) not null,
        CREATED_BY varchar2(255 char),
        CREATED_DATETIME timestamp,
        LAST_UPDATED_BY varchar2(255 char),
        LAST_UPDATED_DATETIME timestamp,
        BANK_ACCT_NAME varchar2(50 char),
        BANK_ACCT_NO varchar2(20 char),
        BANK_NAME varchar2(30 char),
        DAYL_MANFTR_MAX number(10,0),
        DAYL_SHIP_MAX number(10,0),
        CNTACT_ADD varchar2(100 char),
        CNTACT_CELLNO varchar2(15 char),
        CNTACT_EMAIL varchar2(100 char),
        CNTACT_ID varchar2(15 char),
        CNTACT_NAME varchar2(20 char),
        CNTACT_PHNENO varchar2(15 char),
        CNTACT_TITLE varchar2(15 char),
        DAYS_ACCT_PAYMNT number(10,0),
        DEFAULT_VENDOR char(1 char) not null,
        EMAIL_ADD varchar2(20 char) not null,
        ENABLED char(1 char) not null,
        FAX varchar2(15 char),
        FORMAL_NAME varchar2(50 char) not null,
        INVOICE_TITLE varchar2(50 char),
        MAIL_ADD varchar2(120 char) not null,
        PAYMNT_METHOD varchar2(15 char),
        REGION varchar2(50 char) not null,
        REG_ADD1 varchar2(120 char),
        REG_PHONENO varchar2(15 char),
        SHORT_NAME varchar2(25 char),
        primary key (id)
    );

    create table CRM_GIFTCARD (
        id number(19,0) not null,
        CREATED_BY varchar2(255 char),
        CREATED_DATETIME timestamp,
        LAST_UPDATED_BY varchar2(255 char),
        LAST_UPDATED_DATETIME timestamp,
        ALLOW_TO_SELL char(1 char),
        BANK_TRUST_MONTHS number(10,0),
        BARE_PRICE number(12,4),
        CARD_FEE number(12,4),
        CARD_NO_LENGTH number(10,0),
        COUNTRY varchar2(20 char),
        CURRENCY varchar2(5 char),
        EFCTV_MONTHS number(10,0),
        FACE_VALUE number(12,4) not null,
        FIX_VAL char(1 char),
        GEN_CODE varchar2(15 char),
        GRACE_DAYS number(10,0),
        IMAGE_LOCATION varchar2(120 char),
        INC_LINER char(1 char),
        LPO_DIGITS number(10,0),
        LUHNCHK char(1 char),
        MAX_AMOUNT number(12,4),
        MAX_RELOAD_COUNT number(10,0),
        CARD_NAME varchar2(100 char) not null,
        PARTL_REDEEM char(1 char),
        RANDOMNO_DIGITS number(10,0) not null,
        RED_ALRT_PRCNT double precision,
        REDEMPTION number(1,0),
        REFUND char(1 char),
        RELOAD char(1 char),
        SEQ_NO varchar2(10 char),
        TYPE varchar2(15 char),
        VENDOR_NAME varchar2(255 char) not null,
        YELW_ALRT_PRCNT double precision,
        VENDOR number(19,0) not null,
        primary key (id)
    );

    create table CRM_LC_INVENTORY (
        id number(19,0) not null,
        CREATED_BY varchar2(255 char),
        CREATED_DATETIME timestamp,
        LAST_UPDATED_BY varchar2(255 char),
        LAST_UPDATED_DATETIME timestamp,
        ACTIVATION_DATE date,
        ALLOCATE_DATE date,
        BARCODE varchar2(255 char),
        ENCODED_BY varchar2(255 char),
        RECEIPT_NO varchar2(255 char),
        RECEIVE_DATE date,
        REFERENCE_NO varchar2(255 char),
        SEQUENCE varchar2(255 char),
        LOCATION varchar2(15 char),
        PRODUCT number(19,0),
        RECEIVED_AT varchar2(15 char),
        STATUS varchar2(15 char),
        TRANSFER_TO varchar2(15 char),
        primary key (id)
    );

    create table CRM_MEMBER (
        id number(19,0) not null,
        CREATED_BY varchar2(255 char),
        CREATED_DATETIME timestamp,
        LAST_UPDATED_BY varchar2(255 char),
        LAST_UPDATED_DATETIME timestamp,
        ACCOUNT_ID varchar2(255 char) not null,
        STATUS varchar2(20 char) not null,
        TIME_TO_CALL date,
        CARD_NO varchar2(50 char),
        ACCEPT_EMAIL char(1 char),
        ACCEPT_MAIL char(1 char),
        ACCEPT_PHONE char(1 char),
        ACCEPT_SMS char(1 char),
        SMS char(1 char),
        COMPANY_NAME varchar2(255 char),
        CONTACT varchar2(255 char) not null,
        BIRTHDATE timestamp,
        CARS_OWNED number(10,0),
        CHILDREN_COUNT number(10,0),
        CLOSEST_STORE varchar2(255 char),
        CREDIT_CARD_OWNERSHIP char(1 char),
        DOMESTIC_HELPERS number(10,0),
        FAX_NO varchar2(255 char),
        INSURANCE_OWNERSHIP char(1 char),
        NAME_NATIVE varchar2(120 char),
        PLACE_OF_BIRTH varchar2(255 char),
        POSTAL_CODE varchar2(255 char),
        EMAIL varchar2(255 char),
        ACCOUNT_ENABLED char(1 char) not null,
        FIRST_NAME varchar2(255 char),
        KTP varchar2(255 char),
        LAST_NAME varchar2(255 char),
        LOYALTY_CARD_NO varchar2(255 char),
        CREATED_FROM varchar2(255 char),
        MIDDLE_NAME varchar2(255 char),
        NPWP_ADDRESS varchar2(255 char),
        NPWP_ID varchar2(255 char),
        NPWP_NAME varchar2(255 char),
        PARENT_ACCOUNT_ID varchar2(255 char),
        PASSWORD varchar2(255 char),
        PIN varchar2(255 char) not null,
        BUSINESS_EMAIL varchar2(255 char),
        BUSINESS_LICENSE varchar2(255 char),
        BUSINESS_NAME varchar2(255 char),
        POSITION varchar2(255 char),
        RADIUS number(10,0),
        TRANS_CODE_1 number(5,0),
        TRANS_CODE_2 number(5,0),
        TRANS_CODE_3 number(5,0),
        TRANS_CODE_4 number(5,0),
        TRANS_CODE_LONG1 number(5,0),
        TRANS_CODE_LONG2 number(5,0),
        TRANS_CODE_SHORT number(5,0),
        ZONE number(10,0),
        REGISTERED_STORE varchar2(255 char),
        STORE_NAME varchar2(255 char),
        TOTAL_POINTS double precision,
        username varchar2(255 char) not null,
        VALIDATION_CODE varchar2(120 char),
        VERSION number(10,0),
        ADDRESS number(19,0),
        CARD_TYPE varchar2(15 char),
        EDUCATION varchar2(15 char),
        FAMILY_SIZE varchar2(15 char),
        GENDER varchar2(15 char),
        HOUSEHOLD_INCOME varchar2(15 char),
        MARITAL_STATUS varchar2(15 char),
        NATIONALITY varchar2(15 char),
        OCCUPATION varchar2(15 char),
        PERSONAL_INCOME varchar2(15 char),
        PREFERED_LANGUAGE varchar2(15 char),
        RELIGION varchar2(15 char),
        TITLE varchar2(15 char),
        EMP_TYPE varchar2(15 char),
        MEMBER_TYPE varchar2(15 char),
        BUSINESS_ADDRESS number(19,0),
        BUSINESS_FIELD varchar2(15 char),
        CUSTOMER_GROUP varchar2(15 char),
        CUSTOMER_SEGREGATION varchar2(15 char),
        primary key (id)
    );

    create table CRM_MEMBER_FOOD (
        ACCOUNT_ID varchar2(255 char) not null,
        CODE varchar2(15 char) not null
    );

    create table CRM_MEMBER_GRP (
        id number(19,0) not null,
        CREATED_BY varchar2(255 char),
        CREATED_DATETIME timestamp,
        LAST_UPDATED_BY varchar2(255 char),
        LAST_UPDATED_DATETIME timestamp,
        NAME varchar2(255 char),
        primary key (id)
    );

    create table CRM_MEMBER_GRP_FIELD (
        id number(19,0) not null,
        CREATED_BY varchar2(255 char),
        CREATED_DATETIME timestamp,
        LAST_UPDATED_BY varchar2(255 char),
        LAST_UPDATED_DATETIME timestamp,
        MEMBER_GRP number(19,0) not null,
        FIELD varchar2(15 char),
        TYPE varchar2(15 char),
        primary key (id)
    );

    create table CRM_MEMBER_GRP_FIELD_VALUE (
        id number(19,0) not null,
        CREATED_BY varchar2(255 char),
        CREATED_DATETIME timestamp,
        LAST_UPDATED_BY varchar2(255 char),
        LAST_UPDATED_DATETIME timestamp,
        VALUE varchar2(255 char),
        MEMBER_GRP_FIELD number(19,0) not null,
        OPERAND varchar2(15 char),
        primary key (id)
    );

    create table CRM_MEMBER_INTERESTS (
        ACCOUNT_ID varchar2(255 char) not null,
        CODE varchar2(15 char) not null
    );

    create table CRM_MEMBER_PETS (
        ACCOUNT_ID varchar2(255 char) not null,
        CODE varchar2(15 char) not null
    );

    create table CRM_MO (
        id number(19,0) not null,
        CREATED_BY varchar2(255 char),
        CREATED_DATETIME timestamp,
        LAST_UPDATED_BY varchar2(255 char),
        LAST_UPDATED_DATETIME timestamp,
        MO_NO varchar2(255 char),
        ORDER_DATE date,
        PO_NO varchar2(255 char),
        REASON varchar2(255 char),
        STATUS varchar2(15 char),
        SUPPLIER number(19,0),
        primary key (id)
    );

    create table CRM_MO_TIER (
        id number(19,0) not null,
        CREATED_BY varchar2(255 char),
        CREATED_DATETIME timestamp,
        LAST_UPDATED_BY varchar2(255 char),
        LAST_UPDATED_DATETIME timestamp,
        QUANTITY number(19,0),
        STARTING_SERIES varchar2(255 char),
        MO_NO number(19,0),
        NAME varchar2(15 char),
        primary key (id)
    );

    create table CRM_NOTIFICATIONS (
        id number(19,0) not null,
        CREATED_BY varchar2(255 char),
        CREATED_DATETIME timestamp,
        LAST_UPDATED_BY varchar2(255 char),
        LAST_UPDATED_DATETIME timestamp,
        TYPE varchar2(255 char),
        primary key (id)
    );

    create table CRM_NOTIFICATION_STATUS (
        id number(19,0) not null,
        CREATED_BY varchar2(255 char),
        CREATED_DATETIME timestamp,
        LAST_UPDATED_BY varchar2(255 char),
        LAST_UPDATED_DATETIME timestamp,
        READ number(1,0),
        NOTIFICATION number(19,0),
        USER_ID number(19,0),
        STATUS number(19,0),
        primary key (id)
    );

    create table CRM_POINTS (
        id varchar2(255 char) not null,
        CREATED_BY varchar2(255 char),
        CREATED_DATETIME timestamp,
        LAST_UPDATED_BY varchar2(255 char),
        LAST_UPDATED_DATETIME timestamp,
        REASON varchar2(255 char),
        COMMENTS varchar2(255 char),
        INCLUSIVE_DATE_FROM timestamp,
        INCLUSIVE_DATE_TO timestamp,
        STATUS varchar2(255 char),
        STORE_CODE varchar2(255 char),
        TXN_AMOUNT double precision,
        TXN_DATE timestamp,
        TXN_NO varchar2(255 char),
        TXN_POINTS double precision,
        TXN_TYPE varchar2(255 char),
        VALIDATED_BY varchar2(255 char),
        MEMBER_ID number(19,0),
        primary key (id)
    );

    create table CRM_POINTS_CONFIG (
        ID number(19,0) not null,
        POINT_VALUE double precision,
        primary key (ID)
    );

    create table CRM_PRODUCT_GRP (
        id number(19,0) not null,
        CREATED_BY varchar2(255 char),
        CREATED_DATETIME timestamp,
        LAST_UPDATED_BY varchar2(255 char),
        LAST_UPDATED_DATETIME timestamp,
        BRAND varchar2(255 char),
        NAME varchar2(255 char),
        PRODUCTS varchar2(255 char),
        CLASSIFICATION varchar2(15 char),
        SUBCLASSIFICATION varchar2(15 char),
        primary key (id)
    );

    create table CRM_PROGRAM (
        id number(19,0) not null,
        CREATED_BY varchar2(255 char),
        CREATED_DATETIME timestamp,
        LAST_UPDATED_BY varchar2(255 char),
        LAST_UPDATED_DATETIME timestamp,
        DESCRIPTION varchar2(255 char),
        endDate timestamp,
        startDate timestamp,
        NAME varchar2(255 char),
        TYPE varchar2(20 char) not null,
        STATUS varchar2(15 char),
        primary key (id)
    );

    create table CRM_PROMOTION (
        id number(19,0) not null,
        CREATED_BY varchar2(255 char),
        CREATED_DATETIME timestamp,
        LAST_UPDATED_BY varchar2(255 char),
        LAST_UPDATED_DATETIME timestamp,
        AFFECTED_DAYS varchar2(255 char),
        ACCEPT_EMAIL char(1 char),
        ACCEPT_MAIL char(1 char),
        ACCEPT_PHONE char(1 char),
        ACCEPT_SMS char(1 char),
        SMS char(1 char),
        DESCRIPTION varchar2(255 char),
        END_DATE timestamp,
        END_TIME date,
        NAME varchar2(255 char),
        START_DATE timestamp,
        START_TIME date,
        CAMPAIGN_ID number(19,0),
        REWARD_TYPE varchar2(15 char),
        STATUS varchar2(15 char),
        primary key (id)
    );

    create table CRM_PROMOTION_MEMBER_GRP (
        id varchar2(255 char) not null,
        CREATED_BY varchar2(255 char),
        CREATED_DATETIME timestamp,
        LAST_UPDATED_BY varchar2(255 char),
        LAST_UPDATED_DATETIME timestamp,
        MEMBER_GRP_ID number(19,0),
        PROMOTION_ID number(19,0),
        primary key (id)
    );

    create table CRM_PROMOTION_PAYMENTTYPE (
        id varchar2(255 char) not null,
        CREATED_BY varchar2(255 char),
        CREATED_DATETIME timestamp,
        LAST_UPDATED_BY varchar2(255 char),
        LAST_UPDATED_DATETIME timestamp,
        WILDCARDS varchar2(255 char),
        PAYMENTTYPE_ID varchar2(15 char),
        PROMOTION_ID number(19,0),
        primary key (id)
    );

    create table CRM_PROMOTION_PRODUCT_GRP (
        id varchar2(255 char) not null,
        CREATED_BY varchar2(255 char),
        CREATED_DATETIME timestamp,
        LAST_UPDATED_BY varchar2(255 char),
        LAST_UPDATED_DATETIME timestamp,
        PRODUCT_GRP_ID number(19,0),
        PROMOTION_ID number(19,0),
        primary key (id)
    );

    create table CRM_PROMOTION_REWARD (
        id number(19,0) not null,
        CREATED_BY varchar2(255 char),
        CREATED_DATETIME timestamp,
        LAST_UPDATED_BY varchar2(255 char),
        LAST_UPDATED_DATETIME timestamp,
        BASE_AMOUNT number(19,2),
        BASE_FACTOR float,
        DISCOUNT float,
        MAX_AMOUNT number(19,2),
        MAX_BONUS number(19,2),
        MAX_QTY number(19,0),
        MIN_AMOUNT number(19,2),
        MIN_QTY number(19,0),
        REMARK varchar2(255 char),
        PROMO_ID number(19,0),
        REWARD_TYPE varchar2(15 char),
        primary key (id)
    );

    create table CRM_PROMOTION_STORE_GRP (
        id varchar2(255 char) not null,
        CREATED_BY varchar2(255 char),
        CREATED_DATETIME timestamp,
        LAST_UPDATED_BY varchar2(255 char),
        LAST_UPDATED_DATETIME timestamp,
        PROMOTION_ID number(19,0),
        STORE_GRP_ID number(19,0),
        primary key (id)
    );

    create table CRM_REF_LOOKUP_DTL (
        CODE varchar2(15 char) not null,
        DESCRIPTION varchar2(100 char) not null,
        STATUS varchar2(8 char) not null,
        HDR varchar2(15 char) not null,
        primary key (CODE)
    );

    create table CRM_REF_LOOKUP_HDR (
        CODE varchar2(15 char) not null,
        DESCRIPTION varchar2(100 char),
        primary key (CODE)
    );

    create table CRM_REF_MEMBERTYPE (
        CODE varchar2(255 char) not null,
        COMPANY char(1 char),
        TYPE_NAME varchar2(255 char),
        primary key (CODE)
    );

    create table CRM_REWARD_SCHEME (
        id number(19,0) not null,
        CREATED_BY varchar2(255 char),
        CREATED_DATETIME timestamp,
        LAST_UPDATED_BY varchar2(255 char),
        LAST_UPDATED_DATETIME timestamp,
        AMOUNT double precision,
        STATUS varchar2(20 char) not null,
        VALID_PERIOD number(10,0),
        VALUE_FROM double precision,
        VALUE_TO double precision,
        primary key (id)
    );

    create table CRM_STOCK_REQUEST (
        id number(19,0) not null,
        CREATED_BY varchar2(255 char),
        CREATED_DATETIME timestamp,
        LAST_UPDATED_BY varchar2(255 char),
        LAST_UPDATED_DATETIME timestamp,
        REASON_DISAPPROVED varchar2(250 char),
        QUANTITY number(10,0) not null,
        REASON_REJECTED varchar2(250 char),
        STATUS varchar2(15 char) not null,
        STORE_NAME varchar2(255 char) not null,
        GIFTCARD number(19,0) not null,
        STORE number(10,0) not null,
        primary key (id)
    );

    create table CRM_STORE_GROUP (
        id number(19,0) not null,
        CREATED_BY varchar2(255 char),
        CREATED_DATETIME timestamp,
        LAST_UPDATED_BY varchar2(255 char),
        LAST_UPDATED_DATETIME timestamp,
        NAME varchar2(255 char),
        STORES varchar2(255 char),
        primary key (id)
    );

    create table CRM_SUSPENDED_FROM_POINTS (
        ID number(19,0) not null,
        MEMBER_TYPE varchar2(15 char) not null
    );

    create table CRM_TXN_AUDIT (
        id number(19,0) not null,
        CREATED_BY varchar2(255 char),
        CREATED_DATETIME timestamp,
        LAST_UPDATED_BY varchar2(255 char),
        LAST_UPDATED_DATETIME timestamp,
        TXN_NO varchar2(255 char),
        TXN_POINTS number(19,0),
        MEMBER_ID number(19,0),
        PROMOTION number(19,0),
        STORE_CODE number(10,0),
        primary key (id)
    );

    create table CRM_USER (
        id number(19,0) not null,
        CREATED_BY varchar2(255 char),
        CREATED_DATETIME timestamp,
        LAST_UPDATED_BY varchar2(255 char),
        LAST_UPDATED_DATETIME timestamp,
        EMAIL varchar2(255 char),
        ACCOUNT_ENABLED char(1 char),
        FIRST_NAME varchar2(255 char),
        LAST_NAME varchar2(255 char),
        MIDDLE_NAME varchar2(255 char),
        password varchar2(255 char),
        STORE_CODE varchar2(255 char),
        username varchar2(255 char),
        version number(10,0),
        primary key (id)
    );

    create table CRM_USER_ROLE (
        id number(19,0) not null,
        CREATED_BY varchar2(255 char),
        CREATED_DATETIME timestamp,
        LAST_UPDATED_BY varchar2(255 char),
        LAST_UPDATED_DATETIME timestamp,
        code varchar2(255 char),
        description varchar2(255 char),
        ENABLED char(1 char),
        version number(10,0),
        primary key (id)
    );

    create table CRM_USER_X_ROLE (
        CRM_USER number(19,0) not null,
        CRM_USER_ROLE number(19,0) not null,
        primary key (CRM_USER, CRM_USER_ROLE)
    );

    alter table CRM_APPLICATION_CONFIG 
        add constraint UK_japh2dcdabb05sijvhskwt9x5 unique (CONFIG_KEY);

    alter table CRM_GC_INVENTORY 
        add constraint UK_7xxguncy50cj4hqdcgb2d2sod unique (BARCODE);

    alter table CRM_GC_INVENTORY 
        add constraint UK_3dfjvi916kc3x84lkadua7j13 unique (SERIES);

    alter table CRM_GC_ORD 
        add constraint UK_pkyahu66624s0fu49yo58mdad unique (ORDER_DATE);

    alter table CRM_MEMBER 
        add constraint UK_2karw2in2rq5dri3rjd8hwmdi unique (ACCOUNT_ID);

    alter table CRM_MEMBER 
        add constraint UK_do169non7kpcx72f70u0tfb17 unique (CONTACT);

    alter table CRM_MEMBER 
        add constraint UK_t33au0pmegtvk7b5o03ltec2l unique (LOYALTY_CARD_NO);

    alter table CRM_MEMBER 
        add constraint UK_hhwa9gii7269raer27q0yce7r unique (username);

    alter table CRM_USER 
        add constraint UK_owf8ji33y313b3dw4tjpfy6o unique (username);

    alter table CRM_USER_ROLE 
        add constraint UK_spj48swqcgcvpdwvwjl9jsnp5 unique (code);

    alter table CRM_AGE_OF_CHILDREN 
        add constraint FK_sfwieikhyjx8ppvlj3qnjx6l0 
        foreign key (ACCOUNT_ID) 
        references CRM_MEMBER;

    alter table CRM_APPROVAL_MATRIX 
        add constraint FK_3gvjr6ug7fk8l6s7vqhva1rla 
        foreign key (USERNAME) 
        references CRM_USER;

    alter table CRM_APPROVAL_REMARK 
        add constraint FK_ntk34vsbmnnnk7t6m798hotr0 
        foreign key (PROMO_ID) 
        references CRM_PROMOTION;

    alter table CRM_APPROVAL_REMARK 
        add constraint FK_d4osybl0r5pbqipsq6bym566w 
        foreign key (STATUS) 
        references CRM_REF_LOOKUP_DTL;

    alter table CRM_CAMPAIGN 
        add constraint FK_jxj1bxcuit8mii9br935lhucn 
        foreign key (PROJECT_ID) 
        references CRM_PROGRAM;

    alter table CRM_CAMPAIGN 
        add constraint FK_17sxuqgrl1ib3eycmv8ar7rhk 
        foreign key (STATUS) 
        references CRM_REF_LOOKUP_DTL;

    alter table CRM_EMPLOYEE_PURCHASE_TXN 
        add constraint FK_kdfktytk6kc6axp81cpgctv91 
        foreign key (ACCOUNT_ID) 
        references CRM_MEMBER;

    alter table CRM_EMPLOYEE_PURCHASE_TXN 
        add constraint FK_5sgu5qeswin1dm0vagq3wh7c5 
        foreign key (STORE_CODE) 
        references STORE;

    alter table CRM_GC_INVENTORY 
        add constraint FK_evjesfydlcky8dsy55wwkiib6 
        foreign key (ALLOCATE_TO) 
        references STORE;

    alter table CRM_GC_INVENTORY 
        add constraint FK_7odx9nru2rdrle8k2mh8v61du 
        foreign key (LOCATION) 
        references STORE;

    alter table CRM_GC_INVENTORY 
        add constraint FK_gxbrefdg16l48igp67nf53r18 
        foreign key (PRODUCT_CODE) 
        references CRM_GIFTCARD;

    alter table CRM_GC_ORD 
        add constraint FK_mh73tpk4bulvvalerro5j4kko 
        foreign key (VENDOR) 
        references CRM_GC_VENDOR;

    alter table CRM_GC_ORD_HIST 
        add constraint FK_9xghwopdf2xr8dw3fjcir972f 
        foreign key (ORDER_NO) 
        references CRM_GC_ORD;

    alter table CRM_GC_ORD_ITEM 
        add constraint FK_o0schya0vmp4up3uiut49qaqk 
        foreign key (GIFT_CARD) 
        references CRM_GIFTCARD;

    alter table CRM_GC_ORD_ITEM 
        add constraint FK_8hqmur4ye631xk5e3e3j19nqy 
        foreign key (GIFT_CARD_ORDER) 
        references CRM_GC_ORD;

    alter table CRM_GC_ORD_ITEM 
        add constraint FK_qoje8hooxq1kmifpljpohgv1m 
        foreign key (STORE_CODE) 
        references STORE;

    alter table CRM_GC_TXNTRANSFER 
        add constraint FK_pv40wckv3rnaud6d1irr7evwy 
        foreign key (GC_INVENTORY) 
        references CRM_GC_INVENTORY;

    alter table CRM_GIFTCARD 
        add constraint FK_or17q9yfm6cgfaxa3g63ty6y0 
        foreign key (VENDOR) 
        references CRM_GC_VENDOR;

    alter table CRM_LC_INVENTORY 
        add constraint FK_bsmd1cg26r4xhysykegwecog9 
        foreign key (LOCATION) 
        references CRM_REF_LOOKUP_DTL;

    alter table CRM_LC_INVENTORY 
        add constraint FK_bagscfdc99rrm31tq9kyt9d4j 
        foreign key (PRODUCT) 
        references CRM_MO_TIER;

    alter table CRM_LC_INVENTORY 
        add constraint FK_7km6rhrq1qpwg6u5q55eiypii 
        foreign key (RECEIVED_AT) 
        references CRM_REF_LOOKUP_DTL;

    alter table CRM_LC_INVENTORY 
        add constraint FK_2v8qcommn03p7rcucxu0e0g8o 
        foreign key (STATUS) 
        references CRM_REF_LOOKUP_DTL;

    alter table CRM_LC_INVENTORY 
        add constraint FK_h4u1qbvasvl5c1o8yif18l5g9 
        foreign key (TRANSFER_TO) 
        references CRM_REF_LOOKUP_DTL;

    alter table CRM_MEMBER 
        add constraint FK_9g2gdkvdf1eqvnhqb6vctrabe 
        foreign key (ADDRESS) 
        references CRM_ADDRESS;

    alter table CRM_MEMBER 
        add constraint FK_apmylxk64j9pa5qxmrolr2igk 
        foreign key (CARD_TYPE) 
        references CRM_REF_LOOKUP_DTL;

    alter table CRM_MEMBER 
        add constraint FK_pcj0dlmt2qfoh1xtq1arascu7 
        foreign key (EDUCATION) 
        references CRM_REF_LOOKUP_DTL;

    alter table CRM_MEMBER 
        add constraint FK_eqghy0so0ghh3wj3vdahpfvm5 
        foreign key (FAMILY_SIZE) 
        references CRM_REF_LOOKUP_DTL;

    alter table CRM_MEMBER 
        add constraint FK_ogi5fongtiv666o1ji6lxg5dq 
        foreign key (GENDER) 
        references CRM_REF_LOOKUP_DTL;

    alter table CRM_MEMBER 
        add constraint FK_t7o0h47uubteb1w364k7vvxao 
        foreign key (HOUSEHOLD_INCOME) 
        references CRM_REF_LOOKUP_DTL;

    alter table CRM_MEMBER 
        add constraint FK_718jdme3p6lu5ciond1xf11p8 
        foreign key (MARITAL_STATUS) 
        references CRM_REF_LOOKUP_DTL;

    alter table CRM_MEMBER 
        add constraint FK_nwiq3gln8wfqi6cmuh9md5axl 
        foreign key (NATIONALITY) 
        references CRM_REF_LOOKUP_DTL;

    alter table CRM_MEMBER 
        add constraint FK_4exg8m61etnqd9yeygslnoais 
        foreign key (OCCUPATION) 
        references CRM_REF_LOOKUP_DTL;

    alter table CRM_MEMBER 
        add constraint FK_kbfj9y47ua8l7kndva29t9mln 
        foreign key (PERSONAL_INCOME) 
        references CRM_REF_LOOKUP_DTL;

    alter table CRM_MEMBER 
        add constraint FK_l7hq5ik7hqhqhfec2uj178qaw 
        foreign key (PREFERED_LANGUAGE) 
        references CRM_REF_LOOKUP_DTL;

    alter table CRM_MEMBER 
        add constraint FK_41ylwf6eukymcckem9u04s3xw 
        foreign key (RELIGION) 
        references CRM_REF_LOOKUP_DTL;

    alter table CRM_MEMBER 
        add constraint FK_arssxe5tp37ssexms93jvgyo6 
        foreign key (TITLE) 
        references CRM_REF_LOOKUP_DTL;

    alter table CRM_MEMBER 
        add constraint FK_nrxj9fv719nnwr7ce6obnpm5d 
        foreign key (EMP_TYPE) 
        references CRM_REF_LOOKUP_DTL;

    alter table CRM_MEMBER 
        add constraint FK_tmyf6hc8gsh0fypmdusg2yf4s 
        foreign key (MEMBER_TYPE) 
        references CRM_REF_LOOKUP_DTL;

    alter table CRM_MEMBER 
        add constraint FK_4mxqxn0cqmxkgp9fm7ffsgj50 
        foreign key (BUSINESS_ADDRESS) 
        references CRM_ADDRESS;

    alter table CRM_MEMBER 
        add constraint FK_sp3507ev7p9bkgtiwsli6bk2a 
        foreign key (BUSINESS_FIELD) 
        references CRM_REF_LOOKUP_DTL;

    alter table CRM_MEMBER 
        add constraint FK_1mvmv3hd8vwou3iln9vg4evqa 
        foreign key (CUSTOMER_GROUP) 
        references CRM_REF_LOOKUP_DTL;

    alter table CRM_MEMBER 
        add constraint FK_jxd18rnhjdj5f51h4xuehtqvr 
        foreign key (CUSTOMER_SEGREGATION) 
        references CRM_REF_LOOKUP_DTL;

    alter table CRM_MEMBER_FOOD 
        add constraint FK_dlfl6ul8i91dtwyqpo46o8vsf 
        foreign key (CODE) 
        references CRM_REF_LOOKUP_DTL;

    alter table CRM_MEMBER_GRP_FIELD 
        add constraint FK_cnol0drpb629nkwl28mt8pfbd 
        foreign key (MEMBER_GRP) 
        references CRM_MEMBER_GRP;

    alter table CRM_MEMBER_GRP_FIELD 
        add constraint FK_mkh8i18qu98m9a4l4dtv2oami 
        foreign key (FIELD) 
        references CRM_REF_LOOKUP_HDR;

    alter table CRM_MEMBER_GRP_FIELD 
        add constraint FK_407a445ylgg7cp2v7dwo83egg 
        foreign key (TYPE) 
        references CRM_REF_LOOKUP_DTL;

    alter table CRM_MEMBER_GRP_FIELD_VALUE 
        add constraint FK_4lfwy5a89nw8hnjt3nkhfcf1p 
        foreign key (MEMBER_GRP_FIELD) 
        references CRM_MEMBER_GRP_FIELD;

    alter table CRM_MEMBER_GRP_FIELD_VALUE 
        add constraint FK_tmarn7lvu9l4pudwbe8nsehqn 
        foreign key (OPERAND) 
        references CRM_REF_LOOKUP_DTL;

    alter table CRM_MEMBER_INTERESTS 
        add constraint FK_g2xlo4hcw7bv1q3nyh508lwgw 
        foreign key (CODE) 
        references CRM_REF_LOOKUP_DTL;

    alter table CRM_MEMBER_PETS 
        add constraint FK_4ddukcfnitlk7kgqi0a4aaiwd 
        foreign key (CODE) 
        references CRM_REF_LOOKUP_DTL;

    alter table CRM_MO 
        add constraint FK_7fe3n331lh73412mrq1docbil 
        foreign key (STATUS) 
        references CRM_REF_LOOKUP_DTL;

    alter table CRM_MO 
        add constraint FK_t8eqbq8o7gwlkia4o91bvf880 
        foreign key (SUPPLIER) 
        references CRM_GC_VENDOR;

    alter table CRM_MO_TIER 
        add constraint FK_kcexefseh6kwtdkqaypaajqpt 
        foreign key (MO_NO) 
        references CRM_MO;

    alter table CRM_MO_TIER 
        add constraint FK_bsx3mw6egueeldr1s7okb48bq 
        foreign key (NAME) 
        references CRM_REF_LOOKUP_DTL;

    alter table CRM_NOTIFICATION_STATUS 
        add constraint FK_lol6669uyp2te6upmnc0yyw7m 
        foreign key (NOTIFICATION) 
        references CRM_NOTIFICATIONS;

    alter table CRM_NOTIFICATION_STATUS 
        add constraint FK_ddao3imahrbjkpr5dy0851mu0 
        foreign key (USER_ID) 
        references CRM_USER;

    alter table CRM_NOTIFICATION_STATUS 
        add constraint FK_2ijnh0se56gas7taeqc1akwkx 
        foreign key (STATUS) 
        references CRM_NOTIFICATIONS;

    alter table CRM_POINTS 
        add constraint FK_9syii89omui408wduaxod6sqc 
        foreign key (MEMBER_ID) 
        references CRM_MEMBER;

    alter table CRM_PRODUCT_GRP 
        add constraint FK_pb1vvf0bnf3bs5fuurbvvuq0w 
        foreign key (CLASSIFICATION) 
        references CRM_REF_LOOKUP_DTL;

    alter table CRM_PRODUCT_GRP 
        add constraint FK_glsjjfvxd0vr9b0g64du349bf 
        foreign key (SUBCLASSIFICATION) 
        references CRM_REF_LOOKUP_DTL;

    alter table CRM_PROGRAM 
        add constraint FK_4m1m5g4neoxsliyd4c37lwnth 
        foreign key (STATUS) 
        references CRM_REF_LOOKUP_DTL;

    alter table CRM_PROMOTION 
        add constraint FK_6nitj6ny9rh8l0f8ebdr2aovu 
        foreign key (CAMPAIGN_ID) 
        references CRM_CAMPAIGN;

    alter table CRM_PROMOTION 
        add constraint FK_lc6asi9wf8c1hbd2it2r0nkx4 
        foreign key (REWARD_TYPE) 
        references CRM_REF_LOOKUP_DTL;

    alter table CRM_PROMOTION 
        add constraint FK_n445fppjdc9faok3hk1aw0two 
        foreign key (STATUS) 
        references CRM_REF_LOOKUP_DTL;

    alter table CRM_PROMOTION_MEMBER_GRP 
        add constraint FK_6tycqacsif50nayxws8vje9nr 
        foreign key (MEMBER_GRP_ID) 
        references CRM_MEMBER_GRP;

    alter table CRM_PROMOTION_MEMBER_GRP 
        add constraint FK_50mq36pxrs5uw60ifjh5h2dv3 
        foreign key (PROMOTION_ID) 
        references CRM_PROMOTION;

    alter table CRM_PROMOTION_PAYMENTTYPE 
        add constraint FK_l2oopbeqw9bc0v4gd0cenv0b6 
        foreign key (PAYMENTTYPE_ID) 
        references CRM_REF_LOOKUP_DTL;

    alter table CRM_PROMOTION_PAYMENTTYPE 
        add constraint FK_a412atnqflpw3f6agwd4dyvvw 
        foreign key (PROMOTION_ID) 
        references CRM_PROMOTION;

    alter table CRM_PROMOTION_PRODUCT_GRP 
        add constraint FK_2jxb0wgm1df4vbx7j5ncr1957 
        foreign key (PRODUCT_GRP_ID) 
        references CRM_PRODUCT_GRP;

    alter table CRM_PROMOTION_PRODUCT_GRP 
        add constraint FK_a4k83jqhk6ak6wkcbirp1gsii 
        foreign key (PROMOTION_ID) 
        references CRM_PROMOTION;

    alter table CRM_PROMOTION_REWARD 
        add constraint FK_8wjt393m5f7l8ucpdc0tow3dx 
        foreign key (PROMO_ID) 
        references CRM_PROMOTION;

    alter table CRM_PROMOTION_REWARD 
        add constraint FK_ic7qy0mlsau879uaejuddypae 
        foreign key (REWARD_TYPE) 
        references CRM_REF_LOOKUP_DTL;

    alter table CRM_PROMOTION_STORE_GRP 
        add constraint FK_a0uvmw1sro7sk8pg97uung23f 
        foreign key (PROMOTION_ID) 
        references CRM_PROMOTION;

    alter table CRM_PROMOTION_STORE_GRP 
        add constraint FK_1tt1ofr8adikfskxg1xnbsied 
        foreign key (STORE_GRP_ID) 
        references CRM_STORE_GROUP;

    alter table CRM_REF_LOOKUP_DTL 
        add constraint FK_li5b4ep30vhpfnj3om1gji333 
        foreign key (HDR) 
        references CRM_REF_LOOKUP_HDR;

    alter table CRM_STOCK_REQUEST 
        add constraint FK_r8156da0vx6qxlsm8j4y9y92w 
        foreign key (GIFTCARD) 
        references CRM_GIFTCARD;

    alter table CRM_STOCK_REQUEST 
        add constraint FK_65o98my9b7t1slki68loddkf3 
        foreign key (STORE) 
        references STORE;

    alter table CRM_SUSPENDED_FROM_POINTS 
        add constraint FK_91u9lac393dd25yj11ro6kd5d 
        foreign key (MEMBER_TYPE) 
        references CRM_REF_LOOKUP_DTL;

    alter table CRM_SUSPENDED_FROM_POINTS 
        add constraint FK_rj078qcjhqi2s5d2b17368ex0 
        foreign key (ID) 
        references CRM_POINTS_CONFIG;

    alter table CRM_TXN_AUDIT 
        add constraint FK_5gduj7htik5gi2k9nokgl48yp 
        foreign key (MEMBER_ID) 
        references CRM_MEMBER;

    alter table CRM_TXN_AUDIT 
        add constraint FK_tos7r081qs11cmhex75wr59sb 
        foreign key (PROMOTION) 
        references CRM_PROMOTION;

    alter table CRM_TXN_AUDIT 
        add constraint FK_1k4sjr46tv105klye7qeco5a0 
        foreign key (STORE_CODE) 
        references STORE;

    alter table CRM_USER_X_ROLE 
        add constraint FK_66c9sesfiicr8gtq6rtiiia7s 
        foreign key (CRM_USER_ROLE) 
        references CRM_USER_ROLE;

    alter table CRM_USER_X_ROLE 
        add constraint FK_7pxhiatskjnt131xiihfpr2ue 
        foreign key (CRM_USER) 
        references CRM_USER;