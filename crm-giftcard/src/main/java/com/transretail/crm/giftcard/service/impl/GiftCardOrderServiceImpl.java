package com.transretail.crm.giftcard.service.impl;

import static com.mysema.query.alias.Alias.$;
import static com.mysema.query.alias.Alias.alias;
import static com.mysema.query.collections.CollQueryFactory.from;
import static com.mysema.query.group.GroupBy.groupBy;
import static com.mysema.query.group.GroupBy.sum;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Map.Entry;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.BooleanUtils;
import org.apache.commons.lang3.StringUtils;
import org.hibernate.StatelessSession;
import org.joda.time.DateTime;
import org.joda.time.LocalDate;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.mysema.query.BooleanBuilder;
import com.mysema.query.jpa.JPQLQuery;
import com.mysema.query.jpa.hibernate.HibernateUpdateClause;
import com.mysema.query.jpa.impl.JPAQuery;
import com.mysema.query.types.ConstructorExpression;
import com.mysema.query.types.OrderSpecifier;
import com.mysema.query.types.Projections;
import com.mysema.query.types.expr.BooleanExpression;
import com.transretail.crm.common.reporting.jasper.ClasspathFileResolver;
import com.transretail.crm.common.reporting.jasper.DefaultJRProcessor;
import com.transretail.crm.common.reporting.jasper.JRProcessor;
import com.transretail.crm.common.service.exception.GenericServiceException;
import com.transretail.crm.common.service.util.SpringDataPagingUtil;
import com.transretail.crm.common.util.FileCryptor;
import com.transretail.crm.core.entity.lookup.LookupDetail;
import com.transretail.crm.core.entity.lookup.Store;
import com.transretail.crm.core.security.util.UserUtil;
import com.transretail.crm.core.service.CodePropertiesService;
import com.transretail.crm.core.service.LookupService;
import com.transretail.crm.core.service.StoreService;
import com.transretail.crm.giftcard.dto.GiftCardOrderDto;
import com.transretail.crm.giftcard.dto.GiftCardOrderReceiveDto;
import com.transretail.crm.giftcard.dto.GiftCardOrderReceiveItemDto;
import com.transretail.crm.giftcard.dto.GiftCardOrderReceivedSeriesDto;
import com.transretail.crm.giftcard.dto.GiftCardOrderResultList;
import com.transretail.crm.giftcard.dto.GiftCardOrderSearchDto;
import com.transretail.crm.giftcard.dto.GiftCardOrderSeriesDto;
import com.transretail.crm.giftcard.entity.GiftCardInventory;
import com.transretail.crm.giftcard.entity.GiftCardOrder;
import com.transretail.crm.giftcard.entity.GiftCardOrderItem;
import com.transretail.crm.giftcard.entity.QGiftCardInventory;
import com.transretail.crm.giftcard.entity.QGiftCardOrder;
import com.transretail.crm.giftcard.entity.QProductProfile;
import com.transretail.crm.giftcard.entity.support.GiftCardInventoryStatus;
import com.transretail.crm.giftcard.entity.support.GiftCardOrderStatus;
import com.transretail.crm.giftcard.repo.GiftCardInventoryRepo;
import com.transretail.crm.giftcard.repo.GiftCardOrderRepo;
import com.transretail.crm.giftcard.service.GiftCardOrderService;
import com.transretail.crm.giftcard.service.impl.GiftCardOrderServiceImpl.ReprintTxReportBean.OrderItem;
import com.transretail.crm.giftcard.service.impl.GiftCardOrderServiceImpl.ReprintTxReportBean.ReceivedItem;

/**
 *
 */
@Service("giftCardOrderService")
public class GiftCardOrderServiceImpl implements GiftCardOrderService {

    @Autowired
    private GiftCardOrderRepo orderRepo;
    @Autowired
    private GiftCardInventoryRepo inventoryRepo;
    @PersistenceContext
    private EntityManager em;
    @Autowired
    private StatelessSession statelessSession;
    @Autowired
    private LookupService lookupService;
    @Autowired
    private StoreService storeService;
    @Autowired
    private CodePropertiesService codePropertiesService;
    @Autowired
    private MessageSource messageSource;

    private static final DateTimeFormatter FULL_DATE_PATTERN = DateTimeFormat.forPattern("MMMM d, yyyy");
    private static final Logger LOGGER = LoggerFactory.getLogger(GiftCardOrderServiceImpl.class);
    private static final DateTimeFormatter TITLE_DATE_PATTERN = DateTimeFormat.forPattern("MMM dd, yyyy");

    @Override
    @Transactional(readOnly = true)
    public GiftCardOrderDto getGiftCardOrder(Long giftCardOrderId) {
	    return new GiftCardOrderDto(orderRepo.findOne(giftCardOrderId));
    }
    
    @Override
    @Transactional(readOnly = true)
    public boolean isExistBarcodingOrder() {
    	QGiftCardOrder qOrder = QGiftCardOrder.giftCardOrder;
    	return new JPAQuery(em).from(qOrder).where(qOrder.status.eq(GiftCardOrderStatus.BARCODING)).exists();
    }

    @Override
    @Transactional
    public void deleteGiftCardOrder(Long giftCardOrderId) {
	    orderRepo.delete(giftCardOrderId);
    }

    @Override
    @Transactional(readOnly = true)
    public List<GiftCardOrderSeriesDto> getGiftCardSeries(Long giftCardOrderId) {
        QGiftCardInventory qInventory = QGiftCardInventory.giftCardInventory;
        List<GiftCardOrderSeriesDto> seriesList = new JPAQuery(em).from(qInventory)
            .groupBy(qInventory.profile.productDesc, qInventory.orderDate)
            .where(qInventory.order.id.eq(giftCardOrderId))
            .list(ConstructorExpression.create(GiftCardOrderSeriesDto.class,
                    qInventory.profile.productDesc,
                    qInventory.series.min(),
                    qInventory.series.max()));

        for (GiftCardOrderSeriesDto series : seriesList) {
            series.setServed(inventoryRepo.count(qInventory.series.between(series.getStartingSeries(), series.getEndingSeries()).and(qInventory.receiveDate.isNotNull())));
        }

        return seriesList;
    }

    @Override
    @Transactional(readOnly = true)
    public List<GiftCardOrderReceivedSeriesDto> getGiftCardReceivedSeries(Long giftCardOrderId) {
	QGiftCardInventory qInventory = QGiftCardInventory.giftCardInventory;
	return new JPAQuery(em).from(qInventory)
		.groupBy(qInventory.profile.productDesc, qInventory.orderDate, qInventory.deliveryReceipt, qInventory.receiveDate, qInventory.receivedBy)
		.where(qInventory.order.id.eq(giftCardOrderId).and(qInventory.receiveDate.isNotNull()))
		.list(ConstructorExpression.create(GiftCardOrderReceivedSeriesDto.class,
				qInventory.profile.productDesc,
				qInventory.series.min(),
				qInventory.series.max(),
				qInventory.deliveryReceipt,
				qInventory.receiveDate,
				qInventory.receivedBy));
    }

    @Override
    @Transactional(readOnly = true)
    public List<GiftCardOrderReceivedSeriesDto> getGiftCardReceipt( Long giftCardOrderId ) {
	QGiftCardInventory qInventory = QGiftCardInventory.giftCardInventory;
	return new JPAQuery(em).from(qInventory)
		.groupBy(qInventory.deliveryReceipt, qInventory.receiveDate, qInventory.receivedBy)
		.where(qInventory.order.id.eq(giftCardOrderId).and(qInventory.receiveDate.isNotNull()))
		.distinct()
		.list(ConstructorExpression.create(GiftCardOrderReceivedSeriesDto.class,
				qInventory.deliveryReceipt,
				qInventory.receiveDate,
				qInventory.receivedBy));
    }

    @Override
    @Transactional(readOnly = true)
    public GiftCardOrderReceiveDto getGiftCardReceipt(Long id, String deliveryReceipt, LocalDate receiveDate, String receivedBy) {

        QGiftCardInventory gcInventory = QGiftCardInventory.giftCardInventory;
        final BooleanExpression predicate = BooleanExpression.allOf(
            gcInventory.order.id.eq(id), gcInventory.receiveDate.eq(receiveDate),
            StringUtils.isNotBlank( deliveryReceipt )? gcInventory.deliveryReceipt.eq(deliveryReceipt) : null,
            gcInventory.receivedBy.equalsIgnoreCase(receivedBy));

        List<GiftCardOrderReceiveItemDto> items = new JPAQuery(em).from(gcInventory)
            .where(predicate)
            .groupBy(gcInventory.boxNo, gcInventory.profile.productCode, gcInventory.profile.productDesc, gcInventory.expiryDate)
            .list(ConstructorExpression.create(GiftCardOrderReceiveItemDto.class,
                    gcInventory.profile.productCode, gcInventory.profile.productDesc,
                    gcInventory.id.count(), gcInventory.series.min(),
                    gcInventory.series.max(), gcInventory.boxNo, gcInventory.expiryDate));

        String location = new JPAQuery(em).from(gcInventory)
            .where(predicate)
            .groupBy(gcInventory.location)
            .singleResult(gcInventory.location);

        GiftCardOrderReceiveDto receiveDto = new GiftCardOrderReceiveDto();
        receiveDto.setDeliveryReceipt(deliveryReceipt);
        receiveDto.setReceivedDate(receiveDate);
        receiveDto.setId(id);
        receiveDto.setReceivedAt(location);
        receiveDto.setItems(items);
        receiveDto.setReceivedBy(receivedBy);
        return receiveDto;
    }

    @Override
    @Transactional(readOnly = true)
    public boolean isValidReceiveOrder(GiftCardOrderReceiveItemDto itemDto, Long giftCardOrderId) {
	QGiftCardInventory qInventory = QGiftCardInventory.giftCardInventory;
	if (itemDto.getQuantity() == null || itemDto.getStartingSeries() == null || itemDto.getEndingSeries() == null) {
	    return false;
	}
	return itemDto.getQuantity() == inventoryRepo.count(qInventory.series.between(itemDto.getStartingSeries(), itemDto.getEndingSeries())
		.and(qInventory.order.id.eq(giftCardOrderId)
			.and(qInventory.receiveDate.isNull())));
    }

    @Override
    @Transactional(readOnly = true)
    public GiftCardOrderResultList getGiftCardOrders(GiftCardOrderSearchDto searchForm) {
	Pageable pageable = SpringDataPagingUtil.INSTANCE.createPageable(searchForm.getPagination());
	BooleanExpression filter = searchForm.createSearchExpression();
	Page<GiftCardOrder> page = filter != null ? orderRepo.findAll(filter, pageable) : orderRepo.findAll(pageable);
	return new GiftCardOrderResultList(toDto(page.getContent()), page.getTotalElements(), page.hasPreviousPage(),
		page.hasNextPage());
    }

    @Override
    @Transactional
    public void saveGiftCardOrder(GiftCardOrderDto dto) {

		if (StringUtils.isBlank(dto.getMoNumber())) {
		    dto.setMoNumber(generateMoNumber(dto));
		}
	
		GiftCardOrder order = new GiftCardOrder();
		if (dto.getId() != null) {
		    order = orderRepo.findOne(dto.getId());
		}

		orderRepo.saveAndFlush(dto.toModel(order));
    }

    @Transactional(readOnly = true)
    public String generateMoNumber(GiftCardOrderDto dto) {

        QGiftCardOrder qOrder = QGiftCardOrder.giftCardOrder;
        LocalDate monthBegin = dto.getMoDate().withDayOfMonth(1);
        LocalDate monthEnd = dto.getMoDate().plusMonths(1).withDayOfMonth(1).minusDays(1);
        long count = orderRepo.count(qOrder.moDate.goe(monthBegin).and(qOrder.moDate.loe(monthEnd)));

        DateTimeFormatter formatter = DateTimeFormat.forPattern("yyMM");
        String prefix = formatter.print(dto.getMoDate());
        String suffix = String.format("%06d", count + 1);
        return prefix + suffix;

    }

    @Override
    @Transactional
    public long receiveGiftCardInventories(GiftCardOrderReceiveDto receiveDto) {

        QGiftCardInventory qInventory = QGiftCardInventory.giftCardInventory;
        long count = 0;
        for (GiftCardOrderReceiveItemDto itemDto : receiveDto.getItems()) {
            BooleanExpression filter = qInventory.series.between(itemDto.getStartingSeries(), itemDto.getEndingSeries());
            count += new HibernateUpdateClause(statelessSession, qInventory)
                .set(qInventory.receivedBy, UserUtil.getCurrentUser().getUsername())
                .set(qInventory.location, receiveDto.getReceivedAt())
                .set(qInventory.receiveDate, receiveDto.getReceivedDate())
                .set(qInventory.deliveryReceipt, receiveDto.getDeliveryReceipt())
                .set(qInventory.status, GiftCardInventoryStatus.IN_STOCK)
                .set(qInventory.boxNo, itemDto.getBoxNo())
                .where(filter).execute();
        }

        return count;

    }

    @Override
    @Transactional
    public void updateGiftCardStatus(Long giftCardOrderId) {

        QGiftCardInventory qInventory = QGiftCardInventory.giftCardInventory;
        long totalCount = inventoryRepo.count(qInventory.order.id.eq(giftCardOrderId));
        long receivedCount = inventoryRepo.count(qInventory.order.id.eq(giftCardOrderId).and(qInventory.receiveDate.isNotNull()));
        GiftCardOrder order = orderRepo.findOne(giftCardOrderId);
        if (totalCount == receivedCount) {
            order.setStatus(GiftCardOrderStatus.FULL);
            orderRepo.saveAndFlush(order);
        } else if (totalCount > receivedCount) {
            order.setStatus(GiftCardOrderStatus.PARTIAL);
            orderRepo.saveAndFlush(order);
        }

    }

    private List<GiftCardOrderDto> toDto(Collection<GiftCardOrder> orders) {

        List<GiftCardOrderDto> result = Lists.newArrayList();
        for (GiftCardOrder order : orders) {
            result.add(new GiftCardOrderDto(order));
        }

        return result;

    }

    @Override
    @Transactional
    public void approveGiftCardOrder(Long id, GiftCardOrderStatus status) {

        GiftCardOrder order = orderRepo.findOne(id);
        order.setStatus(status);
        order.setApprovalDate(new DateTime());
        order.setApprovedBy(UserUtil.getCurrentUser().getUsername());
        orderRepo.saveAndFlush(order);

    }

    @Override
    @Transactional(readOnly = true)
    public File printEncryptedFile(Long id) {

        QGiftCardInventory qInventory = QGiftCardInventory.giftCardInventory;
        Iterable<GiftCardInventory> inventories = inventoryRepo.findAll(
            qInventory.order.id.eq(id), qInventory.series.asc());

        StringBuilder content = new StringBuilder();
        int i = 0;
        for (GiftCardInventory in : inventories) {
            content.append(String.format(STANDARD_ENCRYPTED_FILE_CONTENT_FORMAT,
                ++i, in.getOrder().getMoNumber(),
                in.getBatchNo(), in.getBarcode(),
                in.getProductCode(), in.getProductName()));
        }

        return writeToFile(content.toString());

    }

    private File writeToFile(String content) throws GenericServiceException {

        FileOutputStream encFile = null;
        FileInputStream origFile = null;
        try {
            File original = File.createTempFile("temp", ".txt");
            File encrypted = File.createTempFile("tempenc", ".txt");

            PrintWriter out = new PrintWriter(original);
            out.print(content);
            out.close();

            encFile = new FileOutputStream(encrypted);
            origFile = new FileInputStream(original);
            FileCryptor.encrypt(FileCryptor.DEFAULT_KEY, origFile, encFile);

            original.deleteOnExit();
            encrypted.deleteOnExit();

            return encrypted;
        } catch (Exception e) {
            throw new GenericServiceException(e);
        } finally {
            IOUtils.closeQuietly(encFile);
            IOUtils.closeQuietly(origFile);
        }

    }

    @Override
    @Transactional(readOnly = true)
    public List<GiftCardInventory> getInventories(List<GiftCardOrderReceiveItemDto> items) {

        QGiftCardInventory qModel = QGiftCardInventory.giftCardInventory;
        BooleanBuilder filter = new BooleanBuilder();
        for (GiftCardOrderReceiveItemDto item : items) {
            filter.or(qModel.series.between(item.getStartingSeries(), item.getEndingSeries()));
        }
        JPQLQuery query = new JPAQuery(em).from(qModel).where(filter).orderBy( new OrderSpecifier( com.mysema.query.types.Order.ASC, qModel.series ) );
        return query.list(qModel);

    }

    @Override
    @Transactional(readOnly = true)
    public Map<String, String> getInventoryLocations() {

        Map<String, String> inv = Maps.newLinkedHashMap();

        LookupDetail headOffice = lookupService.getDetailByCode(codePropertiesService.getDetailInvLocationHeadOffice());
        inv.put(headOffice.getCode(), headOffice.getCode() + " - " + headOffice.getDescription());
        for (Store store : storeService.getAllStores()) {
            inv.put(store.getCode(), store.getCodeAndName());
        }

        return ImmutableMap.copyOf(inv);

    }

    @Override
    public JRProcessor createJrProcessor(GiftCardOrderReceiveDto receiveDto) {

		Map<String, Object> parameters = Maps.newHashMap();
		GiftCardOrderDto orderDto = getGiftCardOrder(receiveDto.getId());
		parameters.put("purchaseOrder", orderDto.getPoNumber() + " / " + FULL_DATE_PATTERN.print(orderDto.getPoDate()));
		parameters.put("manufactureOrder", orderDto.getMoNumber() + " / " + FULL_DATE_PATTERN.print(orderDto.getMoDate()));
		parameters.put("isEgc", BooleanUtils.isTrue( orderDto.getIsEgc() ) );
		parameters.put("supplier", ( BooleanUtils.isTrue( orderDto.getIsEgc() )?
				messageSource.getMessage( "order_isegc_msg_vendornotapplicable", null, null ): orderDto.getVendorName() ) );

		parameters.put("receivedAt", /*receiveDto.getReceivedAt() + " - " +*/ getInventoryLocations().get(receiveDto.getReceivedAt()));
		parameters.put("receiveDate", FULL_DATE_PATTERN.print(receiveDto.getReceivedDate()));
		parameters.put("receipt", receiveDto.getDeliveryReceipt());

		parameters.put("receivedBy", receiveDto.getReceivedBy());

		List<GiftCardOrderReceiveItemDto> products = receiveDto.getItems();
		if (products.isEmpty()) {
			parameters.put("isEmpty", true);
			products.add(new GiftCardOrderReceiveItemDto());
		}

		DefaultJRProcessor jrProcessor = new DefaultJRProcessor(
			"reports/giftcard_order_receive.jasper",
			Collections.singletonList(receiveDto));
		jrProcessor.addParameters(parameters);
		jrProcessor.setFileResolver(new ClasspathFileResolver("reports"));
		return jrProcessor;
    }

    @Override
    public JRProcessor createReprintTransactionJrProcessor(String orderId) {
        GiftCardOrder order = orderRepo.findOne(Long.valueOf(orderId));

        QGiftCardInventory qinv = QGiftCardInventory.giftCardInventory;
        QProductProfile qpp = QProductProfile.productProfile;

        final ConstructorExpression<ReceivedItem> qreceiveItem
            = Projections.constructor(ReprintTxReportBean.ReceivedItem.class,
                qinv.deliveryReceipt, qinv.receiveDate, qpp.productCode, qpp.productDesc,
                qinv.series.min(), qinv.series.max(), qinv.count(), qinv.expiryDate);

        List<ReceivedItem> receivedItems = new JPAQuery(em).from(qinv)
            .innerJoin(qinv.profile, qpp)
            .where(qinv.order.eq(order).and(qinv.deliveryReceipt.isNotNull()))
            .groupBy(qinv.deliveryReceipt, qinv.receiveDate, qpp.productCode, qpp.productDesc, qinv.expiryDate)
            .orderBy(qinv.deliveryReceipt.asc(), qinv.receiveDate.asc(), qpp.productCode.asc())
            .list(qreceiveItem);

        ReprintTxReportBean reportBean = new ReprintTxReportBean();
        reportBean.cardVendor( BooleanUtils.isTrue( order.getIsEgc() )? messageSource.getMessage( "order_isegc_msg_vendornotapplicable", null, null )
                : order.getVendor().getFormalName())
            .approvedDate(order.getApprovalDate() == null ? null : order.getApprovalDate().toLocalDate())
            .manufactureOrderDate(order.getMoDate())
            .orderedBy( order.getCreateUser() )
            .manufactureOrderNo(order.getMoNumber())
            .purchaseOrderDate(order.getPoDate())
            .purchaseOrderNo(order.getPoNumber())
            .status(order.getStatus().name());

        reportBean.receivedItems = receivedItems;
        reportBean.orderItems = Lists.newArrayList();
        Map<String, OrderItem> orderItemsMap = new JPAQuery(em).from(qinv)
            .innerJoin(qinv.profile, qpp)
            .where(qinv.order.eq(order))
            .groupBy(qpp.productCode, qpp.productDesc, qinv.expiryDate)
            .map(qpp.productCode, Projections.constructor(OrderItem.class,
                    qpp.productCode, qpp.productDesc, qinv.series.min(), qinv.series.max(),
                    qinv.count(), qinv.count(), qinv.expiryDate));

        ReceivedItem ri = alias(ReceivedItem.class, "ri");
        Map<String, Long> receivedByProduct = from(ri, receivedItems)
            .transform(groupBy($(ri.getProduct())).as(sum($(ri.getQtyReceived()))));
        for (Entry<String, OrderItem> entry : orderItemsMap.entrySet()) {
            final OrderItem item = entry.getValue();
            Long receivedCount = receivedByProduct.get(entry.getKey());
            if (receivedCount == null) {
            receivedCount = 0l;
            }
            final long qtyOrdered = item.getQtyOrdered();
            reportBean.addOrderItems(item
                .qtyServed(qtyOrdered == receivedCount ? receivedCount : qtyOrdered - receivedCount));
        }

        //<editor-fold defaultstate="collapsed" desc="manually sort by product">
        Collections.sort(reportBean.getOrderItems(), new Comparator<OrderItem>() {

            @Override
            public int compare(OrderItem o1, OrderItem o2) {
            return o1.getProduct().compareTo(o2.getProduct());
            }
        });
        //</editor-fold>
        DefaultJRProcessor jrProcessor = new DefaultJRProcessor(
            "reports/giftcard/reprint-manfacture-order-tx-report.jasper",
            ImmutableList.of(reportBean));
        jrProcessor.addParameter("DATE_FORMAT", "MMM dd, yyyy");
        jrProcessor.setFileResolver(new ClasspathFileResolver("reports/giftcard"));
        return jrProcessor;
    }

    @Override
    public JRProcessor createPendingMOReceiptReport() {
        DefaultJRProcessor jrProcessor = new DefaultJRProcessor("reports/manufacture_order_pending_receipt.jasper");

        Map<String, Object> paramMap = Maps.newHashMap();

        Locale locale = LocaleContextHolder.getLocale();
        paramMap.put("REPORT_TYPE", messageSource.getMessage("gc.pending.receipt.report.label", null, locale));
        paramMap.put("MO_DATE", messageSource.getMessage("gc.pending.receipt.report.date", null, locale));
        paramMap.put("MO_NUMBER", messageSource.getMessage("gc.pending.receipt.report.mono", null, locale));
        paramMap.put("VENDOR", messageSource.getMessage("gc.pending.receipt.report.vendor", null, locale));
        paramMap.put("PRODUCT", messageSource.getMessage("gc.pending.receipt.report.product", null, locale));
        paramMap.put("ORDER_QTY", messageSource.getMessage("gc.pending.receipt.report.orderedqty", null, locale));
        paramMap.put("SERVED_QTY", messageSource.getMessage("gc.pending.receipt.report.servedqty", null, locale));
        paramMap.put("PENDING", messageSource.getMessage("gc.pending.receipt.report.pending", null, locale));

        List<PendingReceiptReportBean> result = this.getDataSource();

        if (CollectionUtils.isEmpty(result)) {
            paramMap.put("NO_DATA", "No data found");
        } else {
            paramMap.put("SUB_DATA_SOURCE", result);
        }
        jrProcessor.addParameters(paramMap);
        jrProcessor.setFileResolver(new ClasspathFileResolver("reports"));
        return jrProcessor;
    }

    private List<PendingReceiptReportBean> getDataSource() {
        List<PendingReceiptReportBean> result = Lists.newArrayList();

        //get gift card orders
        List<GiftCardOrder> giftCardOrders = orderRepo.findByStatusIn(
                Lists.newArrayList(GiftCardOrderStatus.PARTIAL, GiftCardOrderStatus.GENERATED));

        if (!CollectionUtils.isEmpty(giftCardOrders)) {
            for (GiftCardOrder giftCardOrder : giftCardOrders) {
                Long giftCardOrderId = giftCardOrder.getId();

                List<GiftCardOrderSeriesDto> seriesDtos = this.getGiftCardSeries(giftCardOrderId);
                if (!CollectionUtils.isEmpty(seriesDtos)) {
                    for (GiftCardOrderSeriesDto seriesDto : seriesDtos) {
                        PendingReceiptReportBean reportBean = new PendingReceiptReportBean();
                        reportBean.setMoNo(giftCardOrder.getMoNumber());
                        reportBean.setMoDate(giftCardOrder.getMoDate().toString());
                        reportBean.setVendor(giftCardOrder.getVendor() != null ? giftCardOrder.getVendor().getFormalName() : "");
                        reportBean.setProduct(seriesDto.getProfile());
                        reportBean.setOrderQty(seriesDto.getOrdered());
                        reportBean.setServedQty(seriesDto.getServed());
                        reportBean.setPendingQty(reportBean.getOrderQty() - reportBean.getServedQty());
                        result.add(reportBean);
                    }
                }
            }
        }

        return result;
    }

    public static class PendingReceiptReportBean {
        private String moDate;
        private String moNo;
        private String vendor;
        private String product;
        private Long orderQty;
        private Long servedQty;
        private Long pendingQty;

        public String getMoDate() {
            return moDate;
        }

        public void setMoDate(String moDate) {
            this.moDate = moDate;
        }

        public String getMoNo() {
            return moNo;
        }

        public void setMoNo(String moNo) {
            this.moNo = moNo;
        }

        public String getVendor() {
            return vendor;
        }

        public void setVendor(String vendor) {
            this.vendor = vendor;
        }

        public String getProduct() {
            return product;
        }

        public void setProduct(String product) {
            this.product = product;
        }

        public Long getOrderQty() {
            return orderQty;
        }

        public void setOrderQty(Long orderQty) {
            this.orderQty = orderQty;
        }

        public Long getServedQty() {
            return servedQty;
        }

        public void setServedQty(Long servedQty) {
            this.servedQty = servedQty;
        }

        public Long getPendingQty() {
            return pendingQty;
        }

        public void setPendingQty(Long pendingQty) {
            this.pendingQty = pendingQty;
        }
    }

    //<editor-fold defaultstate="collapsed" desc="Report: Reprint Transaction Bean">
    public static class ReprintTxReportBean {

        String purchaseOrderNo;
        LocalDate purchaseOrderDate;
        String manufactureOrderNo;
        String orderedBy;
        LocalDate manufactureOrderDate;
        String supplier;
        LocalDate approvedDate;
        List<OrderItem> orderItems;
        List<ReceivedItem> receivedItems;
        String status;

        public ReprintTxReportBean purchaseOrderNo(String purchaseOrderNo) {
            this.purchaseOrderNo = purchaseOrderNo;
            return this;
        }

        public ReprintTxReportBean purchaseOrderDate(LocalDate purchaseOrderDate) {
            this.purchaseOrderDate = purchaseOrderDate;
            return this;
        }

        public ReprintTxReportBean manufactureOrderNo(String mo) {
            this.manufactureOrderNo = mo;
            return this;
        }

        public ReprintTxReportBean orderedBy(String orderedBy) {
            this.orderedBy = orderedBy;
            return this;
        }

        public ReprintTxReportBean manufactureOrderDate(LocalDate moDate) {
            this.manufactureOrderDate = moDate;
            return this;
        }

        public ReprintTxReportBean cardVendor(String cardVendor) {
            this.supplier = cardVendor;
            return this;
        }

        public ReprintTxReportBean approvedDate(LocalDate approvedDate) {
            this.approvedDate = approvedDate;
            return this;
        }

        public ReprintTxReportBean addOrderItems(OrderItem item) {
            if (this.orderItems == null) {
            this.orderItems = new ArrayList<OrderItem>();
            }

            this.orderItems.add(item);
            return this;
        }

        public ReprintTxReportBean addReceivedItem(ReceivedItem item) {
            if (this.receivedItems == null) {
            this.receivedItems = new ArrayList<ReceivedItem>();
            }
            this.receivedItems.add(item);
            return this;
        }

        public ReprintTxReportBean status(String status) {
            this.status = status;
            return this;
        }

        public String getPurchaseOrderNo() {
            return purchaseOrderNo;
        }

        public LocalDate getPurchaseOrderDate() {
            return purchaseOrderDate;
        }

        public String getManufactureOrderNo() {
            return manufactureOrderNo;
        }

        public String getOrderedBy() {
            return orderedBy;
        }

        public LocalDate getManufactureOrderDate() {
            return manufactureOrderDate;
        }

        public String getSupplier() {
            return supplier;
        }

        public LocalDate getApprovedDate() {
            return approvedDate;
        }

        public List<ReprintTxReportBean.OrderItem> getOrderItems() {
            return orderItems;
        }

        public List<ReprintTxReportBean.ReceivedItem> getReceivedItems() {
            return receivedItems;
        }

        public String getStatus() {
            return status;
        }

        static abstract class Item {

            long startingSeries, endingSeries;
            String product;
            String productDesc;
            String expiryDate;

            public Item(String product, String productDesc, long startingSeries, long endingSeries, LocalDate expiryDate) {
                this.product = product;
                this.productDesc = productDesc;
                this.startingSeries = startingSeries;
                this.endingSeries = endingSeries;
                if (expiryDate != null)
                    this.expiryDate = expiryDate.toString("dd-MM-yyyy");
                else
                    this.expiryDate = "";
            }

            public long getStartingSeries() {
            return startingSeries;
            }

            public long getEndingSeries() {
            return endingSeries;
            }

            public String getProduct() {
            return product;
            }

            public String getProductDesc() {
            return productDesc;
            }

            public String getExpiryDate() {
                return expiryDate;
            }
        }

        public static class OrderItem extends Item {

            long qtyOrdered, qtyServed;

            public OrderItem(String product, String productDesc, long startingSeries, long endingSeries,
                             long qtyOrdered, long qtyServered, LocalDate expiryDate) {
            super(product, productDesc, startingSeries, endingSeries, expiryDate);
            this.qtyOrdered = qtyOrdered;
            this.qtyServed = qtyServered;
            }

            public OrderItem() {
            super("", "", 0l, 0l, null);
            }

            public OrderItem product(String product) {
            super.product = product;
            return this;
            }

            public OrderItem productDesc(String productDesc) {
            super.productDesc = productDesc;
            return this;
            }

            public OrderItem startingSeries(Long startingSeries) {
            super.startingSeries = startingSeries;
            return this;
            }

            public OrderItem endingSeries(Long endingSeries) {
            super.endingSeries = endingSeries;
            return this;
            }

            public OrderItem qtyOrdered(long qtyOrdered) {
            this.qtyOrdered = qtyOrdered;
            return this;
            }

            public OrderItem qtyServed(long qtyServed) {
            this.qtyServed = qtyServed;
            return this;
            }

            public long getQtyOrdered() {
            return qtyOrdered;
            }

            public long getQtyServed() {
            return qtyServed;
            }
        }

        public static class ReceivedItem extends Item {

            String deliveryReceipt;
            LocalDate dateDelivered;
            long qtyReceived;

            public ReceivedItem() {
            super("", "", 0, 0, null);
            }

            public ReceivedItem(String deliveryReceipt, LocalDate dateDelivered, String product, String productDesc,
                                long startingSeries, long endingSeries, long qtyReceived, LocalDate expiryDate) {
            super(product, productDesc, startingSeries, endingSeries, expiryDate);
            this.deliveryReceipt = deliveryReceipt;
            this.dateDelivered = dateDelivered;
            this.qtyReceived = qtyReceived;
            }

            public long getQtyReceived() {
            return qtyReceived;
            }

            public String getDeliveryReceipt() {
            return deliveryReceipt;
            }

            public LocalDate getDateDelivered() {
            return dateDelivered;
            }
        }
    }

}
