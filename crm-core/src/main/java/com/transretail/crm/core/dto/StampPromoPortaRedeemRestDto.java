package com.transretail.crm.core.dto;


import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.joda.time.LocalDate;

import com.google.common.collect.Lists;
import com.transretail.crm.core.service.impl.StampPromoServiceImpl;


public class StampPromoPortaRedeemRestDto {

	private String memberId;
	private String storeCode;
	private String transactionId;
	private LocalDate redeemDate;
	private String md5ValidationKey;
	private List<StampPromoPortaRedeemRestDto.RedeemedStamps> redeemedStamps;



	public StampPromoPortaRedeemRestDto() {}
	public StampPromoPortaRedeemRestDto( String memberId, String storeCode, String transactionId, LocalDate redeemDate, String dateFormat ) {
		this.memberId = memberId;
		this.storeCode = storeCode;
		this.transactionId = transactionId;
		this.redeemDate = redeemDate;
		this.redeemedStamps = Lists.newArrayList();
		this.md5ValidationKey = this.transactionId + redeemDate.toString( StringUtils.isNotBlank( dateFormat )? dateFormat : StampPromoServiceImpl.MD5_DATEFORMAT ); //<transactionId><redeemDateYYYYMMDD>
	}



	public String getMemberId() {
		return memberId;
	}
	public void setMemberId(String memberId) {
		this.memberId = memberId;
	}
	public String getStoreCode() {
		return storeCode;
	}
	public void setStoreCode(String storeCode) {
		this.storeCode = storeCode;
	}
	public String getTransactionId() {
		return transactionId;
	}
	public void setTransactionId(String transactionId) {
		this.transactionId = transactionId;
	}
	public LocalDate getRedeemDate() {
		return redeemDate;
	}
	public void setRedeemDate(LocalDate redeemDate) {
		this.redeemDate = redeemDate;
	}
	public List<StampPromoPortaRedeemRestDto.RedeemedStamps> getRedeemedStamps() {
		return redeemedStamps;
	}
	public void setRedeemedStamps(List<StampPromoPortaRedeemRestDto.RedeemedStamps> redeemedStamps) {
		this.redeemedStamps = redeemedStamps;
	}
	public String getMd5ValidationKey() {
		return md5ValidationKey;
	}
	public void setMd5ValidationKey(String mD5validationkey) {
		this.md5ValidationKey = mD5validationkey;
	}

	public static class RedeemedStamps {
		private String promoId;
		private Long redeemedStamps;
		private Long availableStamps;
		public RedeemedStamps(){}
		public RedeemedStamps( String promoId, Long redeemedStamps, Long availableStamps ) {
			this.promoId = promoId;
			this.redeemedStamps = redeemedStamps;
			this.availableStamps = availableStamps;
		}

		public String getPromoId() {
			return promoId;
		}
		public Long getRedeemedStamps() {
			return redeemedStamps;
		}
		public Long getAvailableStamps() {
			return availableStamps;
		}
	}

}
