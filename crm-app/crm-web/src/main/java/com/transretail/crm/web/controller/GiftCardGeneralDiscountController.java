package com.transretail.crm.web.controller;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.transretail.crm.common.service.dto.response.ResultList;
import com.transretail.crm.giftcard.dto.GiftCardGeneralDiscountDto;
import com.transretail.crm.giftcard.dto.GiftCardGeneralDiscountSearchDto;
import com.transretail.crm.giftcard.entity.support.ProductProfileStatus;
import com.transretail.crm.giftcard.service.GiftCardGeneralDiscountService;
import com.transretail.crm.web.controller.util.ControllerResponse;

@RequestMapping("/giftcard/generaldiscount")
@Controller
public class GiftCardGeneralDiscountController {
	@Autowired
	private GiftCardGeneralDiscountService service;
	@Autowired
	private MessageSource messageSource;
	private static final String PAGE_MAIN = "generaldiscount/list";
	private static final String PAGE_UPDATE = "generaldiscount/update";
	private static final Logger LOGGER = LoggerFactory.getLogger(GiftCardGeneralDiscountController.class);
	
	@RequestMapping(produces="text/html")
	public String list(Model uiModel) {
		return PAGE_MAIN;
	}
	
	@RequestMapping(value="/update/{id}", produces="text/html")
	public String showDialogUpdate(@PathVariable("id") Long id, Model uiModel) {
		uiModel.addAttribute("discount", service.findById(id));
		return PAGE_UPDATE;
	}
	
	@RequestMapping(value="/update", produces="text/html")
	public String showDialogUpdate(Model uiModel) {
		uiModel.addAttribute("discount", new GiftCardGeneralDiscountDto());
		return PAGE_UPDATE;
	}
	
	@RequestMapping(value="/save", method=RequestMethod.POST, produces="application/json")
	@ResponseBody
	public ControllerResponse save(@ModelAttribute("discount") GiftCardGeneralDiscountDto dto,
			BindingResult result, HttpServletRequest request) {
		
		ControllerResponse theResponse = new ControllerResponse();
		theResponse.setSuccess(true);
		
		validate(result, dto);
		
		if (result.hasErrors()) {
			theResponse.setSuccess(false);
			theResponse.setResult(result.getAllErrors());
			return theResponse;
		}

		if (theResponse.isSuccess()) {
			service.save(dto);
		}

		return theResponse;
	}
	
	public void validate(BindingResult result, GiftCardGeneralDiscountDto dto) {
		if(dto.getDiscount() == null) {
			result.reject(getMessage("propkey_msg_notempty", new Object[]{"gc.general.discount.label.discount"}));
		}
		if(dto.getMinPurchase() == null && dto.getMaxPurchase() == null) {
			result.reject(getMessage("gc.general.discount.err.requiredpurchase", null));
		} else if(service.checkForOverlap(dto)) {
			result.reject(getMessage("gc.general.discount.err.overlap", null));
		}
	}
	
	@RequestMapping(value="/{id}", method=RequestMethod.DELETE)
	@ResponseBody
	public ControllerResponse delete(@PathVariable("id") String id) {
		service.delete(Long.valueOf(id));
		ControllerResponse response = new ControllerResponse();
		response.setSuccess(true);
		return response;
	}
	
	@RequestMapping(value="/search", method=RequestMethod.POST)
	@ResponseBody
	public ResultList<GiftCardGeneralDiscountDto> search(@RequestBody
			GiftCardGeneralDiscountSearchDto searchDto) { 
		return service.search(searchDto);
	}
	
	private String getMessage(String inMsgCode, Object[] inArgMsgCodes) {
        if (inArgMsgCodes != null) {
            for (int i = 0; i < inArgMsgCodes.length; i++) {
                inArgMsgCodes[i] =
                    messageSource.getMessage(inArgMsgCodes[i].toString().toLowerCase(), null, LocaleContextHolder.getLocale());
            }
        }
        return messageSource.getMessage(inMsgCode, inArgMsgCodes, "Info given is invalid.", LocaleContextHolder.getLocale());
    }
}
