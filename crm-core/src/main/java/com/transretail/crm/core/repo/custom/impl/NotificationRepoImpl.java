package com.transretail.crm.core.repo.custom.impl;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import com.mysema.query.jpa.impl.JPAQuery;
import com.transretail.crm.core.entity.Notification;
import com.transretail.crm.core.entity.QNotification;
import com.transretail.crm.core.entity.QNotificationStatus;
import com.transretail.crm.core.entity.UserModel;
import com.transretail.crm.core.repo.custom.NotificationRepoCustom;

public class NotificationRepoImpl implements NotificationRepoCustom {
	@PersistenceContext
	EntityManager em;

	@Override
	public List<Notification> getNotifications(UserModel user) {
		QNotification notification = QNotification.notification;
		QNotificationStatus status = QNotificationStatus.notificationStatus;
		JPAQuery query = new JPAQuery(em);
		
		return query.from(notification).innerJoin(notification.statuses, status)
			.where(status.user.eq(user)).orderBy(notification.created.asc()).list(notification);
	}

	@Override
	public List<Notification> getNotifications(UserModel user, int page,
			int size) {
		QNotification notification = QNotification.notification;
		QNotificationStatus status = QNotificationStatus.notificationStatus;
		JPAQuery query = new JPAQuery(em);
		
		return query.from(notification).innerJoin(notification, status.notification)
			.where(status.user.eq(user)).limit(size).offset(page).orderBy(notification.created.asc()).list(notification);
	}

	@Override
	public List<Notification> getUnreadNotifications(UserModel user) {
		QNotification notification = QNotification.notification;
		QNotificationStatus status = QNotificationStatus.notificationStatus;
		JPAQuery query = new JPAQuery(em);
		
		return query.from(notification).innerJoin(notification.statuses, status)
			.where(status.user.eq(user).and(status.read.eq(false))).orderBy(notification.created.asc()).list(notification);
	}

	@Override
	public List<Notification> getUnreadNotifications(UserModel user,
			int page, int size) {
		QNotification notification = QNotification.notification;
		QNotificationStatus status = QNotificationStatus.notificationStatus;
		JPAQuery query = new JPAQuery(em);
		
		return query.from(notification).innerJoin(notification.statuses, status)
			.where(status.user.eq(user).and(status.read.eq(false)))
			.limit(size).offset(page).orderBy(notification.created.asc()).list(notification);
	}

	@Override
	public boolean hasDuplicateNotification(Notification notification) {
		QNotification notifications = QNotification.notification;
		JPAQuery query = new JPAQuery(em);
		
		if(notification.getId() == null) {
			return !query.from(notifications).where(notifications.type.eq(notification.getType())
					.and(notifications.itemId.eq(notification.getItemId())))
					.list(notifications).isEmpty();
		}
		
		return !query.from(notifications).where(notifications.type.eq(notification.getType())
				.and(notifications.itemId.eq(notification.getItemId()))
				.and(notifications.id.ne(notification.getId())))
				.list(notifications).isEmpty();
	}
}
