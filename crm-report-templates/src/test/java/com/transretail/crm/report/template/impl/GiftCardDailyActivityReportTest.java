package com.transretail.crm.report.template.impl;

import java.io.File;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.annotation.Resource;

import net.sf.jasperreports.engine.JRParameter;

import org.apache.commons.io.FileUtils;
import org.joda.time.LocalDate;
import org.joda.time.LocalDateTime;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.context.support.MessageSourceResourceBundle;
import org.springframework.security.authentication.TestingAuthenticationToken;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.context.SecurityContextImpl;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.transretail.crm.common.reporting.exception.ReportException;
import com.transretail.crm.common.reporting.jasper.JRProcessor;
import com.transretail.crm.common.reporting.service.ReportService;
import com.transretail.crm.core.entity.UserModel;
import com.transretail.crm.core.entity.lookup.LookupDetail;
import com.transretail.crm.core.entity.lookup.LookupHeader;
import com.transretail.crm.core.repo.LookupDetailRepo;
import com.transretail.crm.core.repo.LookupHeaderRepo;
import com.transretail.crm.core.repo.UserRepo;
import com.transretail.crm.core.security.model.impl.CustomSecurityUserDetailsImpl;
import com.transretail.crm.core.service.CodePropertiesService;
import com.transretail.crm.giftcard.entity.CardVendor;
import com.transretail.crm.giftcard.entity.GiftCardInventory;
import com.transretail.crm.giftcard.entity.GiftCardInventoryStock;
import com.transretail.crm.giftcard.entity.GiftCardInventoryStock.GCIStockStatus;
import com.transretail.crm.giftcard.entity.GiftCardOrder;
import com.transretail.crm.giftcard.entity.GiftCardOrderItem;
import com.transretail.crm.giftcard.entity.ProductProfile;
import com.transretail.crm.giftcard.entity.ProductProfile.Denomination;
import com.transretail.crm.giftcard.entity.support.GiftCardOrderStatus;
import com.transretail.crm.giftcard.entity.support.GiftCardSalesType;
import com.transretail.crm.giftcard.entity.support.ProductProfileStatus;
import com.transretail.crm.giftcard.repo.CardVendorRepo;
import com.transretail.crm.giftcard.repo.GiftCardInventoryRepo;
import com.transretail.crm.giftcard.repo.GiftCardInventoryStockRepo;
import com.transretail.crm.giftcard.repo.GiftCardOrderRepo;
import com.transretail.crm.giftcard.repo.ProductProfileRepo;
import com.transretail.crm.report.template.ReportTemplate;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath*:/META-INF/spring/applicationContext*.xml",
    "/META-INF/spring/applicationContext-test.xml"})
public class GiftCardDailyActivityReportTest {

	private CardVendor savedVendor;
    private GiftCardOrder savedGiftCardOrder;
    private ProductProfile savedProfile;
    
    @Autowired
    private CardVendorRepo vendorRepo;
    @Autowired
    private ProductProfileRepo profileRepo;
    @Autowired
    private GiftCardOrderRepo giftCardOrderRepo;
	@Autowired
	private GiftCardInventoryRepo inventoryRepo;
	@Autowired
    private ReportService reportService;
	@Autowired
    private MessageSource messageSource;
	@Resource(name = "giftCardDailyActivityReport")
    private ReportTemplate giftCardDailyActivityReport;
	
	private static final Logger logger = LoggerFactory.getLogger(GiftCardDailyActivityReportTest.class);
	
	@Autowired
	private CodePropertiesService codePropertiesService;
	@Autowired
	private LookupHeaderRepo lookupHeaderRepo;
	@Autowired
	private LookupDetailRepo lookupDetailRepo;
	@Autowired
	private GiftCardInventoryStockRepo stockRepo;
	@Autowired
	private UserRepo userRepo;

	
	@Before
	public void setUp() {
		
		UserModel currentUser = new UserModel();
    	currentUser.setUsername("loggedInUser");
    	currentUser.setPassword("");
    	currentUser.setEnabled(true);
    	currentUser.setInventoryLocation(codePropertiesService.getDetailInvLocationDenpasar());
    	currentUser.setStoreCode( "store1" );
    	userRepo.save(currentUser);

        CustomSecurityUserDetailsImpl user = new CustomSecurityUserDetailsImpl();
        user.setUserId(currentUser.getId());
        user.setUsername(currentUser.getUsername());
        List<GrantedAuthority> authorities = Lists.newArrayList();
        authorities.add(new SimpleGrantedAuthority("ROLE_CSS"));
        user.setAuthorities(authorities);
        user.setInventoryLocation(codePropertiesService.getDetailInvLocationDenpasar());

        SecurityContextImpl securityContext = new SecurityContextImpl();
        securityContext.setAuthentication(new TestingAuthenticationToken(user, ""));
        SecurityContextHolder.setContext(securityContext);
		
		LookupHeader hdr = new LookupHeader(codePropertiesService.getProductProfileFaceValue());
        hdr = lookupHeaderRepo.saveAndFlush(hdr);

        LookupDetail dtl = new LookupDetail("PPFV1");
        dtl.setDescription("100000");
        dtl.setHeader(hdr);
        dtl = lookupDetailRepo.saveAndFlush(dtl);
        
        hdr = new LookupHeader(codePropertiesService.getHeaderInventoryLocation());
        hdr = lookupHeaderRepo.saveAndFlush(hdr);

        dtl = new LookupDetail("INVT001");
        dtl.setDescription("HO");
        dtl.setHeader(hdr);
        dtl = lookupDetailRepo.saveAndFlush(dtl);
        
		savedProfile = new ProductProfile();
		savedProfile.setCardFee(1.5);
		savedProfile.setEffectiveMonths(12l);
		savedProfile.setFaceValue(dtl);
		savedProfile.setProductCode("0000000000001");
		savedProfile.setStatus(ProductProfileStatus.APPROVED);
		savedProfile.setProductDesc("PROD");
		savedProfile.setUnitCost(.5);
		savedProfile = profileRepo.save(savedProfile);
		
		savedVendor = new CardVendor();
        savedVendor.setFormalName("name");
        savedVendor.setMailAddress("add");
        savedVendor.setEmailAddress("email");
        savedVendor.setEnabled(true);
        savedVendor.setRegion("region");
        savedVendor.setDefaultVendor(true);
        savedVendor = vendorRepo.save(savedVendor);
        
        savedGiftCardOrder = new GiftCardOrder();
        savedGiftCardOrder.setMoNumber("140403000001");
        savedGiftCardOrder.setPoNumber("0001");
        savedGiftCardOrder.setMoDate(new LocalDate());
        savedGiftCardOrder.setPoDate(new LocalDate());
        savedGiftCardOrder.setVendor(savedVendor);
        savedGiftCardOrder.setStatus(GiftCardOrderStatus.APPROVED);
        savedGiftCardOrder.setItems(new ArrayList<GiftCardOrderItem>());
        
        GiftCardOrderItem gcItem = new GiftCardOrderItem();
        gcItem.setProfile(savedProfile);
        gcItem.setProductCode(savedProfile.getProductCode());
        gcItem.setQuantity(10L);
        gcItem.setOrder(savedGiftCardOrder);
        savedGiftCardOrder.getItems().add(gcItem);
        
        savedGiftCardOrder = giftCardOrderRepo.save(savedGiftCardOrder);
		
        List<GiftCardInventory> inventories = Lists.newArrayList();
		for(int i = 0; i < 10; i++) {
			GiftCardInventory inventory = new GiftCardInventory();
	        inventory.setProfile(savedProfile);
	        inventory.setProductName(savedProfile.getProductDesc());
	        inventory.setProductCode(savedProfile.getProductCode());
	        inventory.setSeqNo(1);
	        inventory.setBatchNo(1);
	        inventory.setOrderDate(savedGiftCardOrder.getMoDate());
	        inventory.setFaceValue(new BigDecimal(Denomination.D500K.getAmount()));
	        inventory.setOrder(savedGiftCardOrder);
	        inventory.setIsEgc(false);
	        inventory.setSeries(Long.valueOf(i));
	        inventory.setBarcode(Integer.toString(i));
	        inventory.setSalesType(GiftCardSalesType.B2B);
	        inventory.setProductCode("0000000000001");
	        inventories.add(inventory);
		}
		inventoryRepo.save(inventories);
		
		GiftCardInventoryStock stock = new GiftCardInventoryStock();
		stock.setCreatedDate(new LocalDate());
		stock.setCompiledTimestamp(new LocalDateTime());
		stock.setStatus(GCIStockStatus.RECEIVED.name());
		stock.setLocation("INVTOO1");
		stock.setSeriesStart(0l);
		stock.setSeriesEnd(9l);
		stock.setQuantity(10l);
		stockRepo.save(stock);
		
		
		
	}
	
	@Test
	public void test() throws ReportException {
		
		Map<String, String> map = Maps.newHashMap();
		map.put("location", "INVT001");
		map.put("cardType", "0000000000001");
		JRProcessor jrProcessor = giftCardDailyActivityReport.createJrProcessor(map);

		File testClassesDir = FileUtils.toFile(Thread.currentThread().getContextClassLoader().getResource(""));
		File output = new File(testClassesDir, "test-gift-card-daily-inventory-output.pdf");
		logger.debug("Output location {}", output.getAbsolutePath());
		output.delete();
		Locale locale = LocaleContextHolder.getLocale();
		jrProcessor.addParameter(JRParameter.REPORT_LOCALE, locale);
		jrProcessor.addParameter(JRParameter.REPORT_RESOURCE_BUNDLE, new MessageSourceResourceBundle(messageSource, locale));
		reportService.exportPdfReport(jrProcessor, output);

		double bytes = output.length();
		double kilobytes = (bytes / 1024);
		logger.debug("Kilobytes: {}", kilobytes);
	}
}
