package com.transretail.crm.template.service.streaming.excel;

import org.apache.poi.xssf.eventusermodel.XSSFSheetXMLHandler;

/**
 * @author Allan G. Ramirez (agramirez@exist.com)
 */
public interface MemberSheetHandler extends XSSFSheetXMLHandler.SheetContentsHandler {
}
