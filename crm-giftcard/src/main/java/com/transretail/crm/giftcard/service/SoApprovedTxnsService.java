package com.transretail.crm.giftcard.service;


import java.util.List;

import com.transretail.crm.core.dto.ApprovalRemarkDto;
import com.transretail.crm.core.entity.ApprovalRemark;
import com.transretail.crm.giftcard.dto.SalesOrderDto;
import com.transretail.crm.giftcard.entity.support.SalesOrderStatus;


public interface SoApprovedTxnsService {

	SalesOrderDto updateStatus(Long id, SalesOrderStatus status);

	SalesOrderDto updateReceiptNo(Long id, String receiptNo);

	SalesOrderDto activateB2bGcs(Long soId);

	SalesOrderDto saveRemarks(ApprovalRemarkDto dto);

	List<ApprovalRemark> getRemarks(Long soId);

	String generateInvoiceNo(Long soId);

}
