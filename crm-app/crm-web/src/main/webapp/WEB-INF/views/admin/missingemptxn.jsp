<%@include file="/WEB-INF/views/common/taglibs.jsp" %>
<div class="page-header page-header2">
  <h1><spring:message code="crm.proxy.btn.reconcile.emptxn"/><span class="pull-right">Store : ${crmInStoreId} - ${crmInStoreName}</span></h1>
</div>
<div id="missing_txns_container">
  <div class="messages">&nbsp;</div>
  <form class="form-horizontal row-fluid well">
    <div class="span8">
      <div class="control-group">
        <label class="control-label"><spring:message code="member_prop_accountid"/></label>

        <div class="controls">
          <input type="text" name="accountId"/>
        </div>
      </div>
      
      <div class="control-group">
        <div class="pull-left">
          <label class="control-label">TRANSACTION DATE</label>
        </div>
      
        <div class="pull-left input-group">
          <div class="">
            <input type="text" name="txnDateFrom" class="datepicker" placeholder="FROM"/>
          </div>
        </div>
        <div class="pull-left input-group ml5">
          <div class="">
            <input type="text" name="txnDateTo" class="datepicker" placeholder="TO"/>
          </div>
        </div>
      </div>
      
      <div class="control-group">
        <label class="control-label"><spring:message code="loyalty_prop_transactionno"/></label>

        <div class="controls">
          <input type="text" name="transactionNo"/>
        </div>
      </div>
      
      <div class="control-group">
        <label class="control-label"><spring:message code="crm.proxy.reconcile.payment.mediatype"/></label>

        <div class="controls">
          <select name="mediaType">
            <c:forEach items="${paymentTypes}" var="paymentType">
              <option value="${paymentType.code}">${paymentType.desc}</option>
            </c:forEach>
          </select>
        </div>
      </div>
      <div class="control-group mb0">
        <label class="control-label">&nbsp;</label>

        <div class="controls">
          <input type="button" value="<spring:message code="search"/>" class="btn btn-primary search"/>
          <input type="button" id="clear" class="btn" value="<spring:message code="label_clear"/>" />
        </div>
      </div>
    </div>
  </form>
  <button type="button" class="btn btn-print pull-left mb20" disabled><spring:message code="gc.stock.label.print.list"/></button>
  <div class="modal hide nofly export-popup" tabindex="-1" role="dialog" aria-hidden="true" style="width: 500px;">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title"><spring:message code="crm.proxy.export.missing.emp.txn"/></h4>
      </div>

      <div class="modal-body">
        <div id="contentError" class="hide alert alert-error">
          <button type="button" class="close" onclick="$(this).parent().hide();">&times;</button>
          <div><form:errors path="*"/></div>
        </div>
        <div class="span5 center">
          <div class="control-group">
            <label class="control-label totalCount"><spring:message code="crm.proxy.export.missing.emp.txn.count"
                                                                    htmlEscape="false"/></label>
          </div>
          <div class="control-group" style="margin-top: 30px;">
            <label class="control-label"><spring:message code="label_com_transretail_crm_entity_batch_options"/></label>
          </div>
          <div class="control-group form-horizontal">
            <div class="control-group">
              <label class="control-label"><spring:message code="label_com_transretail_crm_entity_customermodel_export_batch"/></label>

              <div class="controls">
                <select class="batchNo">
                </select>
              </div>
            </div>
            <div class="control-group">
              <label class="control-label"><spring:message code="label_com_transretail_crm_entity_size_per_batch"/></label>

              <div class="controls">
                <input class="sizePerBatch" type="text" value="1000"/>
              </div>
            </div>
          </div>
        </div>
      </div>

      <div class="modal-footer">
        <button type="button" class="btn btn-primary doPrint"><spring:message code="label_print"/></button>
        <button type="button" class="btn btn-default" data-dismiss="modal"><spring:message code="label_cancel"/></button>
      </div>
    </div>
  </div>
  <div style="text-align: right">
    <button id="reconcileAll" class="btn btn-primary"><spring:message code="crm.proxy.btn.reconcile.all"/></button>
  </div>
  <div class="list" style="padding-top: 10px;">
  </div>
</div>
<script src="<spring:url value="/js/bootstrap/bootstrap-datepicker.js"/>"></script>
<script src="<spring:url value="/js/jquery/jquery.dataTables.js" />"></script>
<script src="<spring:url value="/js/jquery/jquery.dataTables.linkLengthMenu.js" />"></script>
<script src="<spring:url value="/js/jquery/jquery.dataTables.customPaging.js" />"></script>
<script src="<spring:url value="/js/jquery/ajax-datatable.js" />"></script>
<script src="<spring:url value="/js/jquery/jquery.numberInput.js" />"></script>
<script src="<spring:url value="/js/common/form2js.js" />"></script>
<script src="<spring:url value="/js/common/json2.js" />"></script>
<script src="<spring:url value="/js/jquery/jquery.toObject.js" />"></script>
<script src="<spring:url value="/js/jquery/jquery.ajaxQueue.js" />"></script>
<script src="<c:url value="/js/jquery/jquery.formUtil.js" />"></script>
<link href="<spring:url value="/css/bootstrap/datepicker.css" />" rel="stylesheet"/>
<link href="<spring:url value="/css/dataTables.css" />" rel="stylesheet">
<script>
  $( document ).ready( function () {
    $( ".datepicker" ).datepicker( {
      autoclose : true,
      changeMonth : true,
      changeYear : true,
      prevText : "&#9664;",
      nextText : "&#9654;",
      format : "dd-mm-yyyy"
    } );
    var $container = $( '#missing_txns_container' );
    var $list = $( '.list', $container );
    var $totalCountEl = $( '.totalCount span', $container );
    $list.ajaxDataTable( {
      'autoload' : true,
      'ajaxSource' : ctx + '/crminstore/${crmInStoreId}/emptxn/forreconciliation',
      'fnInfoCallback' : function ( oSettings, iStart, iEnd, iMax, iTotal, sPre ) {
        if ( iTotal > 0 ) {
          $( '.btn-print', $container ).removeAttr( 'disabled', 'disabled' );
        } else {
          $( '.btn-print', $container ).attr( 'disabled', 'disabled' );
        }
        $totalCountEl.text( iTotal );
        $( '.sizePerBatch', $container ).keyup();
        return sPre;
      },
      'columnHeaders' : [
        '<spring:message code="member_prop_accountid" />',
        '<spring:message code="loyalty_prop_transactionno" />',
        '<spring:message code="crm.proxy.reconcile.transaction.date" />',
        '<spring:message code="hdr_total_spend" />',
        '<spring:message code="sales_order_type" />',
        '<spring:message code="status" />',
        '<spring:message code="crm.proxy.reconcile.payment.mediatype" />',
        '<spring:message code="action" />'
      ],
      'modelFields' : [
        {name : 'accountId', sortable : false},
        {name : 'txnNo', sortable : false},
        {name : 'transactionDate', sortable : false},
        {name : 'totalPurchasesAmountDisplay', sortable : false},
        {name : 'type', sortable : false},
        {name : 'status', sortable : false},
        {name : 'mediaType', sortable : false},
        { customCell : function ( innerData, sSpecific, json ) {
          var htmlString = '<button class="btn btn-primary btn_reconcile">';
          htmlString += '<spring:message code="crm.proxy.btn.reconcile" />';
          htmlString += '</button>';
          htmlString += '<input type="hidden" value="' + json.txnNo + '">';
          htmlString += '<img src="<spring:url value="/images/ajax-loader-spinner-small.gif" />" style="display: none;"/>';
          return htmlString;
        } }
      ]
    } );

    $( '.search', $container ).click( function () {
      var formDataObject = $( 'form', $container ).toObject( {mode : 'first', skipEmpty : false} );
      var $btnSearch = $( this );
      $btnSearch.prop( 'disabled', true );
      $btnSearch.html( "<spring:message code="searching" />" );
      $list.ajaxDataTable( 'search', formDataObject, function () {
        $btnSearch.prop( 'disabled', false );
        $btnSearch.html( "<spring:message code="search" />" );
      } );
    } );

    var displayMsg = function ( $el, message, issuccess, append ) {
      var htmlString = '<div class="alert ' + (issuccess ? 'alert-success' : 'alert-error') + '">' +
              '<button type="button" class="close" data-dismiss="alert">&times;</button>'
              + message + '</div>';
      if ( append ) {
        $el.append( htmlString );
      } else {
        $el.html( htmlString );
      }
    };

    var $reconcileMessagesDiv = $( '.messages', $container );

    var queueReconcile = function ( $element ) {
      var txnNo = $element.siblings( 'input' ).val();
      var $loader = $element.siblings( 'img' );
      $element.hide();
      $loader.show();
      jQuery.ajaxQueue( {
        "type" : "GET",
        "url" : ctx + '/reconcile/' + txnNo + '/in/crmstore/${crmInStoreId}',
        dataType : "json",
        timeout : 900000 // 15 minutes
      }, function () {

      } ).done( function ( response ) {
        if ( response.hasOwnProperty( 'exception' ) && response.exception != null ) {
          $element.show();
          displayMsg( $reconcileMessagesDiv, response.exception.defaultMessage, false, true );
        } else if ( response.hasOwnProperty( 'errorMessage' ) ) {
          $element.show();
          displayMsg( $reconcileMessagesDiv, response.errorMessage, false, true );
        } else {
          displayMsg( $reconcileMessagesDiv, response.successMessage, true, true );
        }
        $loader.hide();
      } ).fail( function ( xhr, textStatus, error ) {
        if ( textStatus === 'timeout' ) {
          displayMsg( $reconcileMessagesDiv, 'The server took too long to send the data.', false );
        } else if ( error ) {
          displayMsg( $reconcileMessagesDiv, error, false );
        } else {
          displayMsg( $reconcileMessagesDiv, 'An error occurred on the server. Please try again.', false );
        }
        $element.show();
        $loader.hide();
      } );
    };

    $list.on( 'click', 'button.btn_reconcile', function () {
      $( '.alert', $reconcileMessagesDiv ).alert( 'close' );
      queueReconcile( $( this ) );
    } );

    $( '#reconcileAll' ).click( function () {
      $( '.alert', $reconcileMessagesDiv ).alert( 'close' );
      $( 'button.btn_reconcile', $list ).each( function ( index, element ) {
        queueReconcile( $( element ) );
      } );
    } );

    var $batchNo = $( '.batchNo', $container );
    var $sizePerBatch = $( '.sizePerBatch', $container ).numberInput().keyup( function () {
      var sizePerBatch = parseInt( this.value );
      var totalCount = parseInt( $totalCountEl.text() );
      var htmlString = '';

      if ( sizePerBatch > 0 && totalCount > 0 ) {
        var batchCount = 0;
        if ( totalCount <= sizePerBatch ) {
          batchCount = 1;
        } else {
          batchCount = parseInt( totalCount / sizePerBatch ) + 1;
        }
        for ( var i = 0; i < batchCount; i++ ) {
          htmlString += '<option value="' + i + '">' + (i + 1) + '</option>';
        }
      }
      $batchNo.empty().html( htmlString );
    } );

    $( '.btn-print', $container ).click( function () {
      $( '.export-popup', $container ).modal( 'toggle' );
    } );

    $( '.doPrint', $container ).click( function () {
      var formDataObject = $( 'form', $container ).toObject( {mode : 'first', skipEmpty : false} );

      var encodedSearchParam = $.param( formDataObject );
      encodedSearchParam += '&pagination.pageSize=' + parseInt( $sizePerBatch.val() );
      encodedSearchParam += '&pagination.pageNo=' + parseInt( $batchNo.val() );
      console.log(encodedSearchParam);
      var url = ctx + '/crminstore/${crmInStoreId}/emptxn/forreconciliation/export?' + encodedSearchParam;
      window.location.href = url;
    } );

    $('#clear' ).click(function() {
      $('form' ).formUtil( 'clearInputs' );
    });
  } )
</script>