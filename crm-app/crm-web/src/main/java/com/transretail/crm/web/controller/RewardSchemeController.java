package com.transretail.crm.web.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ValidationUtils;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.transretail.crm.common.service.dto.request.PageSortDto;
import com.transretail.crm.common.service.dto.request.PagingParam;
import com.transretail.crm.common.service.dto.response.ResultList;
import com.transretail.crm.core.entity.RewardSchemeModel;
import com.transretail.crm.core.entity.enums.Status;
import com.transretail.crm.core.service.PointsSchemeService;
import com.transretail.crm.web.controller.util.ControllerResponse;
import com.transretail.crm.web.controller.util.ControllerResponseExceptionHandler;
import com.transretail.crm.web.controller.util.VoucherSchemeValidator;

@RequestMapping("/voucherscheme")
@Controller
public class RewardSchemeController extends ControllerResponseExceptionHandler {
	private static Logger LOGGER = LoggerFactory
			.getLogger(RewardSchemeController.class);

	private static final String UI_ATTR_MODELS = "voucherSchemes";
	private static final String UI_ATTR_MODEL = "voucherScheme";
	private static final String UI_ATTR_STATUS_TYPES = "statusTypes";

	@Autowired
	private PointsSchemeService voucherSchemeService;
	
	@Autowired
	private VoucherSchemeValidator vsValidator;

	@RequestMapping(value = "/populate/{id}", method = RequestMethod.POST, produces = "application/json; charset=utf-8")
	@ResponseBody
	public ControllerResponse populate(@PathVariable String id,
			@ModelAttribute(value=UI_ATTR_MODEL) RewardSchemeModel voucherScheme,
			BindingResult result, HttpServletRequest request) {
		LOGGER.debug("populating voucher scheme");

		ControllerResponse response = new ControllerResponse();
		response.setSuccess(true);

		RewardSchemeModel model = null;
		
		if (id.matches("[0-9]*")) {
			model = voucherSchemeService.findPointsScheme(Long.valueOf(id));
		} 
		
		if (model == null) {
			model = new RewardSchemeModel();
			model.setStatus(Status.ACTIVE);
		}
		
		response.setResult(model);

		return response;
	}

	@RequestMapping(value = "/create", method = RequestMethod.POST, produces = "application/json; charset=utf-8")
	@ResponseBody
	public ControllerResponse createRewardScheme(
			@ModelAttribute(value = UI_ATTR_MODEL) RewardSchemeModel voucherScheme,
			BindingResult result, HttpServletRequest request) {

		LOGGER.debug("creating new voucher scheme " + voucherScheme.getId());
		
		ControllerResponse response = new ControllerResponse();
		response.setSuccess(true);

		ValidationUtils.invokeValidator(vsValidator, voucherScheme, result);

		if (result.hasErrors()) {
			response.setSuccess(false);
			response.setResult(result.getAllErrors());
			return response;
		}

		voucherSchemeService.saveAndUpdate(voucherScheme);
		return response;
	}

	@RequestMapping(value = "/update/{id}", method = RequestMethod.POST, produces = "application/json; charset=utf-8")
	@ResponseBody
	public ControllerResponse updateRewardScheme(@PathVariable("id") Long id,
			@ModelAttribute(value = UI_ATTR_MODEL) RewardSchemeModel voucherScheme,
			BindingResult result, HttpServletRequest request) {
		LOGGER.debug("updating voucher scheme " + id);

		ControllerResponse response = new ControllerResponse();
		response.setSuccess(true);

		RewardSchemeModel model = voucherSchemeService.findPointsScheme(id);
		double origFrom = model.getValueFrom();
		double origTo = model.getValueTo();
		int origPeriod = model.getValidPeriod();
		double origAmount = model.getAmount();
		Status origStatus = model.getStatus();
		
		model.setValueFrom(voucherScheme.getValueFrom());
		model.setValueTo(voucherScheme.getValueTo());
		model.setAmount(voucherScheme.getAmount());
		model.setValidPeriod(voucherScheme.getValidPeriod());
		model.setStatus(voucherScheme.getStatus());

		ValidationUtils.invokeValidator(vsValidator, model, result);

		if (result.hasErrors()) {
			LOGGER.debug("errors on update");
			response.setSuccess(false);
			response.setResult(result.getAllErrors());
			LOGGER.debug("revert");
			// revert model
			model.setValueFrom(origFrom);
			model.setValueTo(origTo);
			model.setAmount(origAmount);
			model.setValidPeriod(origPeriod);
			model.setStatus(origStatus);
		}

		voucherSchemeService.saveAndUpdate(model);
		return response;
	}

	void populateForm(Model uiModel,
			RewardSchemeModel voucherSchemeModel) {
		uiModel.addAttribute(UI_ATTR_MODEL, voucherSchemeModel);
		uiModel.addAttribute(UI_ATTR_STATUS_TYPES, Status.values());
	}

//	@RequestMapping(method = RequestMethod.POST, produces = "text/html")
//	public String create(@Valid VoucherSchemeModel voucherScheme,
//			BindingResult bindingResult, Model uiModel,
//			HttpServletRequest httpServletRequest) {
//		if (bindingResult.hasErrors()) {
//			uiModel.addAttribute(UI_ATTR_MODEL, voucherScheme);
//			return "voucherscheme/create";
//		}
//		uiModel.asMap().clear();
//		LOGGER.debug("Create method: creating new voucher scheme " + voucherScheme.getId());
//		voucherSchemeService.saveAndUpdate(voucherScheme);
//		return "redirect:/voucherscheme/";
//	}

	@RequestMapping(params = "form", produces = "text/html")
	public String createForm(Model uiModel) {
		populateForm(uiModel, new RewardSchemeModel());
		return "voucherscheme/create";
	}

	@RequestMapping(value = "/{id}", method = RequestMethod.DELETE, produces = "text/html")
	public String delete(@PathVariable Long id,
			@RequestParam(value = "page", required = false) Integer page,
			@RequestParam(value = "size", required = false) Integer size,
			Model uiModel) {
		LOGGER.debug("Deleting voucher scheme " + id);
		voucherSchemeService.deletePointsScheme(id);
		uiModel.asMap().clear();
		uiModel.addAttribute("page", (page == null) ? "1" : page.toString());
		uiModel.addAttribute("size", (size == null) ? "10" : size.toString());
		return "redirect:/voucherscheme";
	}

	@RequestMapping(produces = "text/html")
	public String list(
			@RequestParam(value = "page", required = false) Integer page,
			@RequestParam(value = "size", required = false) Integer size,
			Model uiModel) {
		populateForm(uiModel, new RewardSchemeModel());
		return "voucherscheme/list";
	}
	
	@RequestMapping(value = "/list", method = RequestMethod.POST)
	public @ResponseBody ResultList<RewardSchemeModel> list(@RequestBody PageSortDto dto) {
		return voucherSchemeService.listSchemeDto(dto);
	}
	
//	@RequestMapping(method = RequestMethod.PUT, produces = "text/html")
//    public String update(@Valid VoucherSchemeModel voucherScheme,
//            BindingResult bindingResult, Model uiModel,
//            HttpServletRequest httpServletRequest) {	
//        if (bindingResult.hasErrors()) {
//            populateForm(uiModel, voucherScheme);
//            return "voucherscheme/update/" + voucherScheme.getId();
//        }
//        uiModel.asMap().clear();
//        LOGGER.debug("Update method: updating voucher scheme " + voucherScheme.getId());
//        voucherSchemeService.saveAndUpdate(voucherScheme);
//        return "redirect:/voucherscheme/";
//    }
}
