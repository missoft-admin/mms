<%@ include file="../../../common/taglibs.jsp" %>


  <!-- Contact Details -->
  <fieldset class="form-horizontal container-fluid span3">
    <legend><spring:message code="label_field_contactdetails" /></legend>
    <div class="control-group">
      <spring:message code="member_prop_homephone" var="homePhone" />
      <label for="homePhone" class="control-label"> <c:out value="${homePhone}" /></label>
      <div id="homePhone" class="controls">
        <form:input path="homePhone" cssClass="form-control" placeholder="${homePhone}" />
      </div>
    </div>
    <div class="control-group">
      <spring:message code="member_prop_contact" var="contact" />
      <label for="contact" class="control-label"> <b class="required">*</b><c:out value="${contact}" /></label>
      <div id="contact" class="controls">
        <input type="text" name="contact" class="form-control" placeholder="${contact}" />
        <input type="text" name="contact" class="form-control form-control-2" placeholder="${contact} 2" />
        <%--<input type="button" onclick="addFields()" value="+"/> --%>
      </div>
    </div>
    <div class="control-group">
      <spring:message code="label_com_transretail_crm_entity_customermodel_timetocall" var="bestTimeToCallLbl" />
      <label for="bestTimeToCall" class="control-label"> <c:out value="${bestTimeToCallLbl}" /></label>
      <div id="bestTimeToCall" class="controls checkbox" style="margin-left: 170px;">
        <form:checkboxes items="${bestTimeToCall}" path="bestTimeToCall" itemValue="code" itemLabel="description" />
      </div>
    </div>
    <div class="control-group">
      <spring:message code="label_com_transretail_crm_entity_customermodel_email" var="email" />
      <label for="email" class="control-label"> <c:out value="${email}" /></label>
      <div class="controls"><input id="email" type="text" name="email" class="form-control email" placeholder="${email}" /></div>
    </div>
  </fieldset>


<style> input.form-control-2 { margin-top: 10px !important; } </style>