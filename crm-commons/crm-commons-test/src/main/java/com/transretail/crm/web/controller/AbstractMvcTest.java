package com.transretail.crm.web.controller;

import java.beans.PropertyDescriptor;

import org.springframework.beans.BeanWrapper;
import org.springframework.beans.BeanWrapperImpl;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;

/**
 * @author Allan G. Ramirez (agramirez@exist.com)
 */
public abstract class AbstractMvcTest {
    protected MockHttpServletRequestBuilder setParams(MockHttpServletRequestBuilder builder, Object modelAttribute) {
        BeanWrapper beanWrapper = new BeanWrapperImpl(modelAttribute);
        for (PropertyDescriptor descriptor : beanWrapper.getPropertyDescriptors()) {
            if (descriptor.getWriteMethod() != null) {
                String propertyName = descriptor.getName();
                Object value = beanWrapper.getPropertyValue(propertyName);
                builder.param(propertyName, value != null ? value.toString() : "");
            }
        }
        return builder;
    }
}
