package com.transretail.crm.reconciliation.service;

import java.sql.Timestamp;
import java.util.List;
import java.util.Map;

import com.transretail.crm.common.service.dto.response.ResultList;
import com.transretail.crm.common.service.exception.MessageSourceResolvableException;
import com.transretail.crm.devtool.dto.ColumnMetadataBean;
import com.transretail.crm.devtool.dto.TableMetadataBean;
import com.transretail.crm.rest.pos.response.dto.MissingEmpTxnBean;
import com.transretail.crm.rest.pos.response.dto.MissingPointsBean;
import com.transretail.crm.rest.request.MissingEmpTxnSearchDto;
import com.transretail.crm.rest.request.MissingPointsSearchDto;

/**
 * @author Allan G. Ramirez (agramirez@exist.com)
 */
public interface DiscrepancyToolService {
    /**
     * Return ColumnMetadataBean of records present in main db but does not exist in local db.
     *
     * @param beans table metadata with table name and table id column name
     * @return ColumnMetadataBean of records present in main db but does not exist in local db.
     */
    Map<String, List<ColumnMetadataBean>> getLocalDbMissingRecords(List<TableMetadataBean> beans) throws
        MessageSourceResolvableException;

    /**
     * Return ColumnMetadataBean of records present in local db but does not exist in server db.
     *
     * @param beans table metadata with table name and table id column name
     * @return ColumnMetadataBean of records present in main db but does not exist in local db.
     */
    Map<String, List<ColumnMetadataBean>> getMainDbMissingRecords(List<TableMetadataBean> beans) throws
        MessageSourceResolvableException;

    /**
     * Return ColumnMetadataBean as key and minimum created date time of records present in main db but does not exist in local db.
     *
     * @param beans table metadata with table name and table id column name
     * @return ColumnMetadataBean as key and minimum created date time of records present in main db but does not exist in local db.
     */
    Map<TableMetadataBean, Timestamp> getMinCreatedDateOfMissingRecordsInLocalDb(List<TableMetadataBean> beans) throws
        MessageSourceResolvableException;

}
