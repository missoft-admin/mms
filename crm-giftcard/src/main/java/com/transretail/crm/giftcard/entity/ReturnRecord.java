package com.transretail.crm.giftcard.entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.annotations.Type;
import org.joda.time.LocalDate;

import com.google.common.collect.Lists;
import com.transretail.crm.core.entity.lookup.LookupDetail;
import com.transretail.crm.core.entity.support.CustomAuditableEntity;
import com.transretail.crm.giftcard.entity.support.ReturnStatus;

/**
 *
 */
@Entity
@Table(name = "CRM_GC_RETURN")
public class ReturnRecord extends CustomAuditableEntity<Long> implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Column(name = "REC_NO")
	private String recordNo;

	@Column(name = "RETURN_DATE")
	@Type(type = "org.jadira.usertype.dateandtime.joda.PersistentLocalDate")
	private LocalDate returnDate;
	
	@ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "CUSTOMER")
	private GiftCardCustomerProfile customer;
	
	@ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "ORDER_NO")
	private SalesOrder orderNo;
	
	@Column(name = "FACE_AMOUNT")
	private BigDecimal faceAmount;
	@Column(name = "RETURN_AMOUNT")
	private BigDecimal returnAmount;
	@Column(name = "REFUND_AMOUNT")
	private BigDecimal refundAmount;
	@Column(name = "REPLACE_AMOUNT")
	private BigDecimal replaceAmount;
	
	@Column(name = "STATUS")
	@Enumerated(EnumType.STRING)
	private ReturnStatus status;
	
	@ManyToOne
    @JoinColumn(name = "REASON")
	private LookupDetail reason;
	
	@OneToMany(mappedBy = "record", orphanRemoval = true, fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	private List<ReturnByCardsItem> items = Lists.newArrayList();
	
	
	public ReturnRecord() {}
	public ReturnRecord(Long id) {
		setId(id);
	}
	public String getRecordNo() {
		return recordNo;
	}

	public void setRecordNo(String recordNo) {
		this.recordNo = recordNo;
	}

	public ReturnStatus getStatus() {
		return status;
	}

	public void setStatus(ReturnStatus status) {
		this.status = status;
	}

	public LocalDate getReturnDate() {
		return returnDate;
	}

	public void setReturnDate(LocalDate returnDate) {
		this.returnDate = returnDate;
	}

	public GiftCardCustomerProfile getCustomer() {
		return customer;
	}

	public void setCustomer(GiftCardCustomerProfile customer) {
		this.customer = customer;
	}

	public SalesOrder getOrderNo() {
		return orderNo;
	}

	public void setOrderNo(SalesOrder orderNo) {
		this.orderNo = orderNo;
	}

	public BigDecimal getFaceAmount() {
		return faceAmount;
	}

	public void setFaceAmount(BigDecimal faceAmount) {
		this.faceAmount = faceAmount;
	}

	public BigDecimal getReturnAmount() {
		return returnAmount;
	}

	public void setReturnAmount(BigDecimal returnAmount) {
		this.returnAmount = returnAmount;
	}

	public BigDecimal getRefundAmount() {
		return refundAmount;
	}

	public void setRefundAmount(BigDecimal refundAmount) {
		this.refundAmount = refundAmount;
	}

	public BigDecimal getReplaceAmount() {
		return replaceAmount;
	}

	public void setReplaceAmount(BigDecimal replaceAmount) {
		this.replaceAmount = replaceAmount;
	}
	public LookupDetail getReason() {
		return reason;
	}
	public void setReason(LookupDetail reason) {
		this.reason = reason;
	}
	public List<ReturnByCardsItem> getItems() {
		return items;
	}
	public void setItems(List<ReturnByCardsItem> items) {
		this.items = items;
	}
	
	

	
	
}
