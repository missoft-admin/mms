package com.transretail.crm.core.service.impl;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import com.transretail.crm.core.entity.enums.MemberStatus;
import org.joda.time.DateTime;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TemporaryFolder;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Configurable;
import org.springframework.context.MessageSource;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import com.transretail.crm.common.reporting.jasper.JRProcessor;
import com.transretail.crm.core.dto.EmployeePurchaseTxnDto;
import com.transretail.crm.core.entity.EmployeePurchaseTxnModel;
import com.transretail.crm.core.entity.MemberModel;
import com.transretail.crm.core.entity.embeddable.CustomerProfile;
import com.transretail.crm.core.entity.enums.Status;
import com.transretail.crm.core.entity.enums.TxnStatus;
import com.transretail.crm.core.entity.lookup.LookupDetail;
import com.transretail.crm.core.entity.lookup.LookupHeader;
import com.transretail.crm.core.entity.lookup.MemberType;
import com.transretail.crm.core.repo.EmployeePurchaseTxnRepo;
import com.transretail.crm.core.repo.LookupDetailRepo;
import com.transretail.crm.core.repo.LookupHeaderRepo;
import com.transretail.crm.core.repo.MemberRepo;
import com.transretail.crm.core.service.EmployeePurchaseService;
import com.transretail.crm.core.service.EmployeeService;
import com.transretail.crm.core.util.generator.IdGeneratorService;

@Configurable
@Transactional
@ContextConfiguration(locations = "classpath*:/META-INF/spring/applicationContext*.xml")
@RunWith(SpringJUnit4ClassRunner.class)
public class EmployeePurchaseServiceImplTest {
	@Mock
	MessageSource mockMessageSource;
	
	@Mock
	EmployeePurchaseTxnRepo mockEmployeePurchaseTxnRepo;
	
	@Mock
	EmployeeService mockEmployeeService;
	
	@Mock
	MemberRepo mockMemberRepo;
	
	@InjectMocks
	EmployeePurchaseService employeePurchaseServiceMock = new EmployeePurchaseServiceImpl();
	
	@Autowired
	EmployeePurchaseService employeePurchaseService;
	
	@Autowired
	EmployeePurchaseTxnRepo employeePurchaseTxnRepo;
	
	@Rule
    public TemporaryFolder tmpFolder = new TemporaryFolder();
	
	private String txnId;
	
	@Autowired
    JpaRepository<MemberType, String> memberTypeRepo;
	
	@Autowired
	MemberRepo memberRepo;
	
	@Autowired
	LookupHeaderRepo lookupHeaderRepo;
	
	@Autowired
	LookupDetailRepo lookupDetailRepo;
	
	@Autowired
    IdGeneratorService customIdGeneratorService;

    static String employeeType  = "MTYP002";
    static String employeeTypeDesc = "Employee";

	
	@Before
	public void setUp() {
		
		setupEmployeePurchases();
	}
	
	
	private void setupEmployeePurchases() {
    	LookupHeader header = new LookupHeader();
    	header.setCode("membertype");
    	header.setDescription("membertype");
    	header = lookupHeaderRepo.saveAndFlush(header);
    	
    	LookupDetail theMemberType = new LookupDetail();
    	theMemberType.setHeader(header);
    	theMemberType.setCode(employeeType);
    	theMemberType.setDescription(employeeTypeDesc);
    	theMemberType.setStatus(Status.ACTIVE);
    	theMemberType = lookupDetailRepo.saveAndFlush(theMemberType);

        MemberModel emp = new MemberModel();
        emp.setAccountId("1");
        emp.setFirstName("first1");
        emp.setLastName("last1");
        emp.setPin("pin1");
        emp.setUsername("username1");
        emp.setEnabled(true);
        emp.setContact("000001");
        emp.setAccountStatus(MemberStatus.ACTIVE);
        emp.setMemberType(theMemberType);
        CustomerProfile customerProfile = new CustomerProfile();
        customerProfile.setBirthdate(new Date());
        emp.setCustomerProfile(customerProfile);
        memberRepo.save(emp);
        
        EmployeePurchaseTxnModel model = new EmployeePurchaseTxnModel();
        model.setMemberModel(emp);
        model.setTransactionNo("1");
        model.setTransactionAmount(1000.0);
        model.setTransactionDateTime(new Date());
        model.setStatus(TxnStatus.ACTIVE);
        model.setId(customIdGeneratorService.generateId());
        model.setCreated(new DateTime()); //TODO temp fix
        txnId = employeePurchaseTxnRepo.save(model).getId();
        
        model = new EmployeePurchaseTxnModel();
        model.setMemberModel(emp);
        model.setTransactionNo("2");
        model.setTransactionAmount(2000.0);
        model.setTransactionDateTime(new Date());
        model.setStatus(TxnStatus.ACTIVE);
        model.setId(customIdGeneratorService.generateId());
        model.setCreated(new DateTime());
        employeePurchaseTxnRepo.save(model).getId();
        
        model = new EmployeePurchaseTxnModel();
        model.setMemberModel(emp);
        model.setTransactionNo("3");
        model.setTransactionAmount(3000.0);
        model.setTransactionDateTime(new Date());
        model.setStatus(TxnStatus.FORAPPROVAL);
        model.setId(customIdGeneratorService.generateId());
        model.setCreated(new DateTime());
        employeePurchaseTxnRepo.save(model);
        
        model = new EmployeePurchaseTxnModel();
        model.setMemberModel(emp);
        model.setTransactionNo("4");
        model.setTransactionAmount(4000.0);
        model.setTransactionDateTime(new Date());
        model.setStatus(TxnStatus.FORAPPROVAL);
        model.setId(customIdGeneratorService.generateId());
        model.setCreated(new DateTime());
        employeePurchaseTxnRepo.save(model);
	}
	
	
	@After
	public void teardown() {
		employeePurchaseTxnRepo.deleteAll();
		memberRepo.deleteAll();
		lookupDetailRepo.deleteAll();
		lookupHeaderRepo.deleteAll();
	}
	
	@Test
	public void testCreatePurchaseTxnJrProcessor() throws Exception {
		MockitoAnnotations.initMocks(this);
		Mockito.when(mockMessageSource.getMessage(Mockito.anyString(), Mockito.any(Object[].class), Mockito.any(Locale.class))).thenReturn("anystringlabel");
		Mockito.when(mockEmployeePurchaseTxnRepo.findAllActiveByIdAndPostingDates(Mockito.anyString(), Mockito.any(Calendar.class), Mockito.any(Calendar.class))).thenReturn(new ArrayList<EmployeePurchaseTxnModel>());
		Mockito.when(mockMemberRepo.findByAccountId(Mockito.anyString())).thenReturn(new MemberModel());
		JRProcessor jrProcessor = employeePurchaseServiceMock.createPurchaseTxnJrProcessor("2", new Date(), new Date(), null);
		Assert.assertNotNull(jrProcessor);
		Mockito.verify(mockEmployeePurchaseTxnRepo).findAllActiveByIdAndPostingDates(Mockito.anyString(), Mockito.any(Calendar.class), Mockito.any(Calendar.class));
		Mockito.verify(mockMemberRepo).findByAccountId("2");
		
		jrProcessor = employeePurchaseServiceMock.createPurchaseTxnJrProcessor("2", null, null, null);
		Assert.assertNotNull(jrProcessor);
	}
	
	@Test
	public void testFindPurchaseTransactionById() {
		EmployeePurchaseTxnDto theTxn = employeePurchaseService.findPurchaseTransactionById(txnId);
		Assert.assertNotNull(theTxn);
		Assert.assertEquals(Double.valueOf(1000.0), theTxn.getTransactionAmount());
		Assert.assertEquals("1", theTxn.getTransactionNo());
	}
	
	@Test
	public void testFindOneByTransactionNo() {
		EmployeePurchaseTxnDto theTxn = employeePurchaseService.findOneByTransactionNo("1");
		Assert.assertNotNull(theTxn);
		Assert.assertEquals(Double.valueOf(1000.0), theTxn.getTransactionAmount());
		Assert.assertEquals("1", theTxn.getTransactionNo());
	}
	
	@Test
	public void testCountAllPendingPurchaseAdjustments() {
		Assert.assertEquals(2, employeePurchaseService.countAllPendingPurchaseAdjustments());
	}
	
	@Test
	public void testFindAllPendingPurchasesAdjustments() {
		List<EmployeePurchaseTxnDto> theTxns = employeePurchaseService.findAllPendingPurchasesAdjustments();
		Assert.assertNotNull(theTxns);
		Assert.assertEquals(2, theTxns.size());
	}
	
	@Test
	public void testFindPurchaseTxnByIdAndPostingDates() {
		List<EmployeePurchaseTxnDto> theTxns = employeePurchaseService.findPurchaseTxnByIdAndPostingDates("1",null, null);
		Assert.assertNotNull(theTxns);
		Assert.assertEquals(2, theTxns.size());
	}

	@Test
	public void testDeletePurchaseTransaction() {
		Assert.assertEquals(4, employeePurchaseTxnRepo.count());
		employeePurchaseService.deletePurchaseTransaction(txnId);
		Assert.assertEquals(3, employeePurchaseTxnRepo.count());
	}
 }
