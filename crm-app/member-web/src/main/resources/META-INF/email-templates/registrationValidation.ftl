<html>
  <head>
    <title>Welcome!</title>
    <style>
      h1, h2 {color: #a67100}
      h1 {margin: 0}
      h2 {margin: 0 0 40px 0;}
    </style>
  </head>
  <body>
  <center>
    <h1>${membername}</h1>
    <h2>Selamat Datang di Membership Marketing System Transmart & Goserindo</h2>
    <h2>Terima Kasih telah mendaftar sebagai anggota. Pendaftaran Anda akan segera kami proses</h2>
    <p>
      Silahkan klik <a href="${link}?code=${queryString?url}">disini</a> untuk validasi email Anda<br />
      atau<br />
      klik alamat berikut<br/>
      ${link}?code=${queryString?url}</p>
    <br/>
  </center>
</body>
</html>