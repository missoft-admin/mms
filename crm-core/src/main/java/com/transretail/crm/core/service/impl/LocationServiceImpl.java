package com.transretail.crm.core.service.impl;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.google.common.collect.Lists;
import com.mysema.query.jpa.impl.JPAQuery;
import com.mysema.query.types.expr.BooleanExpression;
import com.transretail.crm.common.service.dto.response.ResultList;
import com.transretail.crm.common.service.util.SpringDataPagingUtil;
import com.transretail.crm.core.dto.CityDto;
import com.transretail.crm.core.dto.CityProvinceSearchDto;
import com.transretail.crm.core.dto.ProvinceDto;
import com.transretail.crm.core.entity.City;
import com.transretail.crm.core.entity.Province;
import com.transretail.crm.core.entity.QCity;
import com.transretail.crm.core.entity.QProvince;
import com.transretail.crm.core.repo.CityRepo;
import com.transretail.crm.core.repo.ProvinceRepo;
import com.transretail.crm.core.service.LocationService;

@Service("locationService")
@Transactional
public class LocationServiceImpl implements LocationService {
	
	@Autowired
	ProvinceRepo provinceRepo;
	@Autowired
	CityRepo cityRepo;
	@PersistenceContext
	EntityManager em;
	
	@Override
    public ResultList<Province> listProvince(CityProvinceSearchDto searchDto) {
		Pageable pageable = SpringDataPagingUtil.INSTANCE.createPageable(searchDto.getPagination());
		BooleanExpression filter = searchProvince(searchDto);
		Page<Province> page = filter != null ? provinceRepo.findAll(filter, pageable) : provinceRepo.findAll(pageable);
		return new ResultList<Province>(page);
    }
	
	private BooleanExpression searchProvince(CityProvinceSearchDto searchDto) {
		QProvince qProvince = QProvince.province;
		QCity qCity = QCity.city;
		if(StringUtils.isNotBlank(searchDto.getProvince())) {
			return qProvince.name.containsIgnoreCase(searchDto.getProvince());
		} else if(StringUtils.isNotBlank(searchDto.getCity())) {
			List<Long> provinceIds = new JPAQuery(em).from(qCity)
					.where(qCity.name.containsIgnoreCase(searchDto.getCity()))
					.distinct()
					.list(qCity.province.id);
			return qProvince.id.in(provinceIds);
		}
		return null;
	}
	
	@Override
    public ResultList<City> listCityByProvince(CityProvinceSearchDto searchDto) {
		BooleanExpression filter = searchCity(searchDto);
		Pageable pageable = SpringDataPagingUtil.INSTANCE.createPageable(searchDto.getPagination());
		Page<City> page = filter != null ? cityRepo.findAll(filter, pageable) : cityRepo.findAll(pageable);
		return new ResultList<City>(page);
    }
	
	private BooleanExpression searchCity(CityProvinceSearchDto searchDto) {
		QCity qCity = QCity.city;
		List<BooleanExpression> expressions = Lists.newArrayList();
		if(searchDto.getProvinceId() != null)
			expressions.add(qCity.province.id.eq(searchDto.getProvinceId()));
		if(StringUtils.isNotBlank(searchDto.getCity()))
			expressions.add(qCity.name.containsIgnoreCase(searchDto.getCity()));
		return BooleanExpression.allOf(expressions.toArray(new BooleanExpression[expressions.size()]));
	}
	
	@Override
	public void deleteProvince(Long id) {
		provinceRepo.delete(id);
	}
	
	@Override
	public void deleteCity(Long id) {
		cityRepo.delete(id);
	}
	
	@Override
	public void saveProvince(ProvinceDto provinceDto) {
		Province province = new Province();
		if(provinceDto.getId() != null)
			province = provinceRepo.findOne(provinceDto.getId());
		province.setName(provinceDto.getName());
		provinceRepo.save(province);
	}
	
	private List<CityDto> convertToDto(Iterable<City> cities) {
		List<CityDto> cityDtos = Lists.newArrayList();
		for(City city:cities) {
			cityDtos.add(new CityDto(city));
		}
		return cityDtos;
	}
	
	@Override
	public void saveCity(CityDto cityDto) {
		City city = new City();
		if(cityDto.getId() != null)
			city = cityRepo.findOne(cityDto.getId());
		if(cityDto.getProvinceId() != null) {
			Province province = provinceRepo.findOne(cityDto.getProvinceId());
			city.setProvince(province);	
		}
		city.setName(cityDto.getName());
		cityRepo.save(city);
	}

	@Override
	public Province getProvince(Long id) {
		return provinceRepo.findOne(id);
	}
	
	@Override
	public Iterable<City> getCities(Long provinceId) {
		QCity qCity = QCity.city;
		return cityRepo.findAll(qCity.province.id.eq(provinceId), qCity.name.asc());
	}
	
	@Override
	public List<Province> getAllProvinces() {
		QProvince qProvince = QProvince.province;
		return Lists.newArrayList(provinceRepo.findAll(null, qProvince.name.asc()));
	}

	@Override
	public List<Province> getProvincesByCityName(String cityName) {
		QCity qCity = QCity.city;
		return new JPAQuery(em).from(qCity)
				.where(qCity.name.equalsIgnoreCase(cityName))
				.distinct()
				.list(qCity.province);
	}
	
	@Override
	public Province getProvince(String name) {
		QProvince qProvince = QProvince.province;
		return provinceRepo.findOne(qProvince.name.equalsIgnoreCase(name));
	}
	
	@Override
	public City getCity(String name) {
		QCity qCity = QCity.city;
		return cityRepo.findOne(qCity.name.equalsIgnoreCase(name));
	}
}



