package com.transretail.crm.web.giftcard.controller.dto;


import java.util.Set;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.transretail.crm.giftcard.dto.GiftCardTransactionInfo;
import com.transretail.crm.web.giftcard.controller.AbstractGiftCardRequest;


@JsonInclude(JsonInclude.Include.NON_NULL)
public class EGiftCardDeallocateRequest extends AbstractGiftCardRequest {

    private Set<String> cards;



    public Set<String> getCards() {
		return cards;
	}
	public void setCards(Set<String> cards) {
		this.cards = cards;
	}

	@Override
    public String getSignature() {
        return String.format( "%s%s%s", getMerchantId(), getTerminalId(), getCashierId() );
    }

    @SuppressWarnings("rawtypes")
	@Override
    public GiftCardTransactionInfo toGiftCardTransactionInfo() {
        return super.templateBaseTransactionInfo();
    }

}
