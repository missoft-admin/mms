package com.transretail.crm.core.dto;

import org.joda.time.LocalDate;

public class PointsExpireReportDto {
	private String month;
	private Double totalPointsExpired;
	private Double totalAmount;
	private Double origLoyaltyIssued;
	private Double currLoyaltyUsed;
	private Integer cardsWithExpiry;
	
	public PointsExpireReportDto() {
		
	}
	
	public PointsExpireReportDto(Double totalPointsExpired) {
		this.totalPointsExpired = totalPointsExpired;
	}
	
	public PointsExpireReportDto(Double totalPointsExpired, Double totalAmount) {
		this.totalPointsExpired = totalPointsExpired;
		this.totalAmount = totalAmount;
	}
	
	public PointsExpireReportDto(Double origLoyaltyIssued, Integer cardsWithExpiry) {
		this.origLoyaltyIssued = origLoyaltyIssued;
		this.cardsWithExpiry = cardsWithExpiry;
	}
	
	public String getMonth() {
		return month;
	}
	public void setMonth(String month) {
		this.month = month;
	}
	public Double getTotalPointsExpired() {
		return totalPointsExpired;
	}
	public void setTotalPointsExpired(Double totalPointsExpired) {
		this.totalPointsExpired = totalPointsExpired;
	}
	public Double getTotalAmount() {
		return totalAmount;
	}
	public void setTotalAmount(Double totalAmount) {
		this.totalAmount = totalAmount;
	}

	public Double getOrigLoyaltyIssued() {
		return origLoyaltyIssued;
	}

	public void setOrigLoyaltyIssued(Double origLoyaltyIssued) {
		this.origLoyaltyIssued = origLoyaltyIssued;
	}

	public Double getCurrLoyaltyUsed() {
		return currLoyaltyUsed;
	}

	public void setCurrLoyaltyUsed(Double currLoyaltyUsed) {
		this.currLoyaltyUsed = currLoyaltyUsed;
	}

	public Integer getCardsWithExpiry() {
		return cardsWithExpiry;
	}

	public void setCardsWithExpiry(Integer cardsWithExpiry) {
		this.cardsWithExpiry = cardsWithExpiry;
	}
}
