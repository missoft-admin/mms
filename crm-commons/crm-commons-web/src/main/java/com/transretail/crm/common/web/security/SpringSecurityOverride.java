package com.transretail.crm.common.web.security;

import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.InstantiationAwareBeanPostProcessorAdapter;
import org.springframework.core.Ordered;
import org.springframework.core.PriorityOrdered;
import org.springframework.security.web.session.InvalidSessionStrategy;
import org.springframework.security.web.session.SessionManagementFilter;

/**
 * Class that would override the spring security configuration.
 * <p>
 * Some config cannot be set via xml eg <a href="https://jira.spring.io/browse/SEC-1894">XML support for InvalidSessionStrategy</a>
 * and declaring individual filters are quite tedius.
 * </p>
 *
 * @author Allan G. Ramirez (agramirez@exist.com)
 */
public class SpringSecurityOverride extends InstantiationAwareBeanPostProcessorAdapter implements PriorityOrdered {
    private boolean isInvalidSessionStrategySet = false;
    private InvalidSessionStrategy invalidSessionStrategy;

    /**
     * Get the priority order of this post processor. Will always return Integer.MAX_VALUE, meaning last in order.
     *
     * @return Integer.MAX_VALUE, this processor will be last in order
     */
    @Override
    public int getOrder() {
        return Ordered.LOWEST_PRECEDENCE;
    }

    @Override
    public Object postProcessAfterInitialization(Object bean, String beanName) throws BeansException {
        if (isInvalidSessionStrategySet && bean instanceof SessionManagementFilter) {
            ((SessionManagementFilter) bean).setInvalidSessionStrategy(invalidSessionStrategy);
        }
        return bean;
    }

    public void setInvalidSessionStrategy(InvalidSessionStrategy invalidSessionStrategy) {
        isInvalidSessionStrategySet = true;
        this.invalidSessionStrategy = invalidSessionStrategy;
    }
}
