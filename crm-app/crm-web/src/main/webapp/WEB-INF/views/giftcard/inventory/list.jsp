<%@include file="/WEB-INF/views/common/taglibs.jsp" %>

<div class="page-header page-header2">
<h1><spring:message code="menutab_giftcard_inventory_list" /></h1>
</div>

<sec:authorize ifAllGranted="ROLE_MERCHANT_SERVICE_SUPERVISOR">
  <c:set var="isMerchantServiceSupervisor" value="true"/>
</sec:authorize>
<div id="searchForm">
  <div>
    <spring:message code="gc.inventory.seriesFrom" var="seriesFrom"/>
    <label for="q_seriesFrom" title="${seriesFrom}">${seriesFrom}</label>
    <input id="q_seriesFrom" name="seriesFrom" title="${seriesFrom}" type="text" value="">
    <spring:message code="to"/>
    <input id="q_seriesTo" name="seriesTo" type="text" value="">
  </div>
  <div>
    <spring:message code="gc.inventory.productName" var="productName"/>
    <label for="q_productName" title="${productName}">${productName}</label>
    <input id="q_productName" name="productName" title="${productName}" type="text" value="">
  </div>
  <c:if test="${stores != null}">
    <div>
      <spring:message code="gc.inventory.location" var="location"/>
      <label for="q_locationCode" title="${location}">${location}</label>
        <%--TODO: Create and use vendor search instead of displaying everything in dropdown--%>
      <select id="q_locationCode" name="locationCode">
        <option value="">&nbsp;</option>
        <c:forEach items="${stores.results}" var="store">
          <option value="${store.id}">${store.name}</option>
        </c:forEach>
      </select>
    </div>
    <div>
      <spring:message code="gc.inventory.allocatedTo" var="allocatedTo"/>
      <label for="q_allocateToCode" title="${allocatedTo}">${allocatedTo}</label>
        <%--TODO: Create and use vendor search instead of displaying everything in dropdown--%>
      <select id="q_allocateToCode" name="allocateToCode">
        <option value="">&nbsp;</option>
        <c:forEach items="${stores.results}" var="store">
          <option value="${store.id}">${store.name}</option>
        </c:forEach>
      </select>
    </div>
  </c:if>
  <div>
    <label>&nbsp;</label>
      <span>
        <button id="search" type="button"><spring:message code="search"/></button>
      </span>
  </div>
</div>
<div id="errorMessages">&nbsp;</div>
<div id="successMessages">&nbsp;</div>
<div id="inventories_container">
</div>
<div id="allocateDialog" class="contentHidden" style="text-align: center;">
  <div id="allocateForm" class="contentDialogBody">
    <div id="seriesForm">
      <label for="seriesFrom" title="${seriesFrom}">${seriesFrom}</label>
      <input id="seriesFrom" name="seriesFrom" title="${seriesFrom}" type="text" value="">
      <spring:message code="to"/>
      <input id="seriesTo" name="seriesTo" type="text" value="">
    </div>
    <div style="margin-bottom: 10px;">
      <label>&nbsp;</label>
      <button id="detailedView" type="button"><spring:message code="gc.inventory.view.detail"/></button>
      <button id="summarizedView" type="button"><spring:message code="gc.inventory.view.summary"/></button>
    </div>
    <div>
      <spring:message code="gc.inventory.allocateTo" var="allocateTo"/>
      <label for="allocateToCode">${allocateTo}<span class="required">*</span></label>
      <select id="allocateToCode" name="allocateToCode">
        <c:forEach items="${stores.results}" var="store">
          <option value="${store.id}">${store.name}</option>
        </c:forEach>
      </select>
      <input id="allocateToDesc" name="allocateToDesc" type="hidden"/>
    </div>
    <div>
      <spring:message code="gc.inventory.expiry" var="expiryDate"/>
      <label for="expiryDate">${expiryDate}</label>
      <input id="expiryDate" name="expiryDate" class="datepicker-generic" title="${expiryDate}" type="text" value="">
    </div>
    <div id="summarized_container" class="contentHidden">

    </div>
    <div id="detailed_container" class="contentHidden">

    </div>
  </div>
  <div class="contentDialogBody contentDialogButtons">
    <div class="contentButton">
      <button id="proceed" class="buttonRed" type="button"><spring:message code="proceed"/></button>
    </div>
    <div class="contentButton">
      <button id="cancel" class="buttonRed" type="button"><spring:message code="cancel"/></button>
    </div>
    <hr class="floatClear">
  </div>
</div>
<link href="<spring:url value="/css/dataTables.css" />" rel="stylesheet">
<script src="<spring:url value="/js/jquery/jquery.dataTables.js" />"></script>
<script src="<spring:url value="/js/jquery/jquery.dataTables.linkLengthMenu.js" />"></script>
<script src="<spring:url value="/js/jquery/jquery.dataTables.customPaging.js" />"></script>
<script src="<spring:url value="/js/jquery/ajax-datatable.js" />"></script>
<script src="<spring:url value="/js/common/form2js.js" />"></script>
<script src="<spring:url value="/js/common/json2.js" />"></script>
<script src="<spring:url value="/js/jquery/jquery.toObject.js" />"></script>
<script>
$( document ).ready( function () {
  $( '#allocateToCode' ).change(function () {
    $( this ).siblings( '#allocateToDesc' ).val( $( this ).find( "option:selected" ).text() );
  } ).change();

  var $inventoriesList = $( "#inventories_container" ).ajaxDataTable( {
    "autoload" : true,
    "ajaxSource" : "<spring:url value="/giftcard/inventory/summary/list" />",
    "columnHeaders" : [
      "<spring:message code="gc.inventory.productName" />",
      "<spring:message code="gc.inventory.batchNo" />",
      "<spring:message code="gc.inventory.faceValue" />",
      "<spring:message code="gc.inventory.seriesFrom" />",
      "<spring:message code="gc.inventory.seriesTo" />",
      "<spring:message code="gc.inventory.quantity" />",
      "<spring:message code="gc.inventory.location" />",
      "<spring:message code="gc.inventory.allocatedTo" />",
      "<spring:message code="gc.inventory.status" />",
      "<spring:message code="action" />"
    ],
    "modelFields" : [
      {name : "productName", sortable : true},
      {name : "batchNo"},
      {name : "faceValue"},
      {name : "seriesFrom", fieldCellRenderer : function ( data, type, row ) {
        if ( type == 'display' ) {
          return "<span class='seriesFrom'>" + data + "</span>";
        } else {
          return data;
        }
      }},
      {name : "seriesTo", fieldCellRenderer : function ( data, type, row ) {
        if ( type == 'display' ) {
          return "<span class='seriesTo'>" + data + "</span>";
        } else {
          return data;
        }
      }},
      {name : "quantity"},
      {name : "location"},
      {name : "allocateTo"},
      {name : "status", fieldCellRenderer : function ( data, type, row ) {
        if ( status == 'IN_TRANSIT' ) {
          return "IN-TRANSIT to " + row.allocateTo;
        } else {
          return data;
        }
      }},
      {
        customCell : function ( innerData, sSpecific, json ) {
          if ( sSpecific == 'display' ) {
            var htmlString = '';
            <c:choose>
            <c:when test="${isMerchantServiceSupervisor}">
            if ( json.locationCode == 'HO' ) {
              if ( json.allocateToCode == null ) {
                if ( json.status != 'IN_TRANSIT' ) {
                  htmlString += '<a class="allocate" href="#" onclick="return false"><spring:message code="gc.inventory.allocate"/></a>';
                } else {
                  htmlString += '<a class="transferout" href="#" onclick="return false"><spring:message code="gc.inventory.transferout"/></a>';
                }
              }
            }
            </c:when>
            <c:otherwise>
            if ( json.status != 'IN_TRANSIT' ) {
              if ( json.allocateToCode == "${userStore}" ) {
                htmlString += '<a class="allocate" href="#" onclick="return false"><spring:message code="gc.inventory.allocate"/></a>';
              } else {
                htmlString += '<a class="transferout" href="#" onclick="return false"><spring:message code="gc.inventory.transferout"/></a>';
              }
            }
            </c:otherwise>
            </c:choose>
            return htmlString;
          } else {
            return '';
          }
        }
      }
    ]
  } );

  var $summarizedList = $( "#summarized_container" ).ajaxDataTable( {
    "autoload" : true,
    "ajaxSource" : "<spring:url value="/giftcard/inventory/summary/list" />",
    "columnHeaders" : [
      "<spring:message code="gc.inventory.productName" />",
      "<spring:message code="gc.inventory.batchNo" />",
      "<spring:message code="gc.inventory.faceValue" />",
      "<spring:message code="gc.inventory.seriesFrom" />",
      "<spring:message code="gc.inventory.seriesTo" />",
      "<spring:message code="gc.inventory.quantity" />",
      "<spring:message code="gc.inventory.location" />",
      "<spring:message code="gc.inventory.allocatedTo" />",
      "<spring:message code="gc.inventory.status" />"
    ],
    "modelFields" : [
      {name : "productName", sortable : true},
      {name : "batchNo"},
      {name : "faceValue"},
      {name : "seriesFrom"},
      {name : "seriesTo"},
      {name : "quantity"},
      {name : "location"},
      {name : "allocateTo"},
      {name : "status", fieldCellRenderer : function ( data, type, row ) {
        if ( status == 'IN_TRANSIT' ) {
          return "IN-TRANSIT to " + row.allocateTo;
        } else {
          return data;
        }
      }}
    ]
  } );

  var $detailedList = $( "#detailed_container" ).ajaxDataTable( {
    "autoload" : false,
    "ajaxSource" : "<spring:url value="/giftcard/inventory/detail/list" />",
    "columnHeaders" : [
      "<spring:message code="gc.inventory.series" />",
      "<spring:message code="gc.inventory.productName" />",
      "<spring:message code="gc.inventory.faceValue" />"
    ],
    "modelFields" : [
      {name : "series", sortable : true},
      {name : "productName", sortable : true},
      {name : "faceValue"}
    ]
  } );

  var $seriesForm = $( "#seriesForm" );
  $( "#detailedView" ).click( function () {
    var formDataObject = $seriesForm.toObject( {mode : "first", skipEmpty : false} );
    var $button = $( this );
    $button.prop( "disabled", true );
    $detailedList.ajaxDataTable( "search", formDataObject, function () {
      $summarizedList.hide();
      $detailedList.show();
      $button.prop( "disabled", false );
    } );
  } );

  $( "#summarizedView" ).click( function () {
    var formDataObject = $seriesForm.toObject( {mode : "first", skipEmpty : false} );
    var $button = $( this );
    $button.prop( "disabled", true );
    $summarizedList.ajaxDataTable( "search", formDataObject, function () {
      $detailedList.hide();
      $summarizedList.show();
      $button.prop( "disabled", false );
    } );
  } );

  var $search = $( "#search" ).click( function () {
    var formDataObject = $( "#searchForm" ).toObject( {mode : "first", skipEmpty : false} );
    var $button = $( this );
    $button.prop( "disabled", true );
    $inventoriesList.ajaxDataTable( "search", formDataObject, function () {
      $button.prop( "disabled", false );
    } );
  } );

  $allocateDialog = $( '#allocateDialog' ).dialog( {
    draggable : false,
    autoOpen : false,
    width : 750,
    modal : true,
    close : function ( event, ui ) {
      $summarizedList.hide();
      $detailedList.hide();
      clearInputs( "allocateForm" );
    }
  } );

  $inventoriesList.on( 'click', 'a.allocate', function () {
    var $tr = $( this ).parents( 'tr' );
    var seriesFrom = $( '.seriesFrom', $tr ).text();
    var seriesTo = $( '.seriesTo', $tr ).text();
    $( '#seriesFrom' ).val( seriesFrom );
    $( '#seriesTo' ).val( seriesTo );
    $allocateDialog.dialog( 'open' );
  } );

  $( "#proceed" ).click( function () {
    var jsonData = JSON.stringify( $( "#allocateForm" ).toObject( {mode : 'first', skipEmpty : false} ) );
    $.ajax( {
      "contentType" : 'application/json',
      headers : { "Content-Type" : "application/json" },
      "type" : "POST",
      "url" : "<spring:url value="/giftcard/inventory/allocate" />",
      "data" : jsonData,
      "success" : function ( data ) {
        if ( data.hasOwnProperty( 'errorMessages' ) ) {
          var errorInfo = "";
          for ( var i = 0; i < data.errorMessages.length; i++ ) {
            errorInfo += "<br>" + (i + 1) + ". " + data.errorMessages[i];
          }
          $( '#errorMessages' ).html( "<spring:message code="haserror.message" />: " + errorInfo );
        } else {
          $( '#errorMessages' ).html( '' );
          $( '#successMessages' ).html( data.successMessages );
          $search.click();
        }
        $allocateDialog.dialog( "close" );
      },
      "error" : function ( xhr, textStatus, error ) {
        if ( textStatus === 'timeout' ) {
          $( '#errorMessages' ).html( 'The server took too long to send the data.' );
        } else if ( error ) {
          $( '#errorMessages' ).html( error );
        } else {
          $( '#errorMessages' ).html( 'An error occurred on the server. Please try again.' );
        }
        $allocateDialog.dialog( 'close' );
      }
    } );
  } );

  $( "#cancel" ).click( function () {
    $allocateDialog.dialog( 'close' );
  } );
} );
</script>