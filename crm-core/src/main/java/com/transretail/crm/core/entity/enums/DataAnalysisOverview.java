package com.transretail.crm.core.entity.enums;

public enum DataAnalysisOverview {

	SALES_VALUE("salesValue"),
	TXN_COUNT("txnCount"),
	SHOPPER_COUNT("shopperCount"),
	AVG_VISIT_FREQ("avgVisitFreq");

    private DataAnalysisOverview(String code){
    	this.code = code;
    }

    private final String code;

	public String getCode() {
		return code;
	}
    
    
}
