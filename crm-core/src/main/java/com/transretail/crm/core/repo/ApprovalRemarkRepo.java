package com.transretail.crm.core.repo;


import java.util.List;

import org.springframework.stereotype.Repository;

import com.transretail.crm.common.repo.CrmQueryDslPredicateExecutor;
import com.transretail.crm.core.entity.ApprovalRemark;


@Repository
public interface ApprovalRemarkRepo extends CrmQueryDslPredicateExecutor<ApprovalRemark, Long> {

	List<ApprovalRemark> findByModelTypeAndModelId( String inModleType, String inModelId );

}
