package com.transretail.crm.schedule.service;


import java.text.ParseException;

import org.quartz.Scheduler;
import org.quartz.SchedulerException;

import com.transretail.crm.core.entity.enums.AppKey;


public interface JobSchedulerService  {

	String rescheduleJob(String triggerName, String triggerGrp, String newCronExp, AppKey appkey) throws SchedulerException, ParseException;

	String getCronExpression(String triggerName, String triggerGrp) throws SchedulerException;

	Scheduler getScheduler();

}
