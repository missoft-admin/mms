package com.transretail.crm.giftcard.entity.support;

/**
 *
 */
public enum PaymentInfoStatus {
    DRAFT, FOR_APPROVAL, VERIFIED, APPROVED, REJECTED, SO_CANCELLED, FIRST_APPROVAL, SECOND_APPROVAL
}
