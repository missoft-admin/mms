package com.transretail.crm.core.service.impl;


import java.io.InputStream;
import java.util.Scanner;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Configurable;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.support.TransactionCallback;
import org.springframework.transaction.support.TransactionTemplate;

import com.transretail.crm.core.service.PromoParticipantsService;


@Configurable
@Transactional
@ContextConfiguration(locations = "classpath*:/META-INF/spring/applicationContext*.xml")
@RunWith(SpringJUnit4ClassRunner.class)
public class PromoParticipantsServiceTest {
	@PersistenceContext
    private EntityManager em;
    @Autowired
    private PlatformTransactionManager transactionManager;
	@Autowired
	private PromoParticipantsService service;
	@Before
	public void setup() {
		InputStream is = Thread.currentThread().getContextClassLoader().getResourceAsStream("scripts/promoparticipants_test.sql");
        try {
            final Scanner in = new Scanner(is);
            TransactionTemplate transactionTemplate = new TransactionTemplate(transactionManager);
            transactionTemplate.execute(new TransactionCallback() {
                public Object doInTransaction(TransactionStatus status) {
                    while (in.hasNext()) {
                        String script = in.nextLine();
                        if (StringUtils.isNotBlank(script)) {
                            em.createNativeQuery(script).executeUpdate();
                        }
                    }
                    return null;
                }
            });
        } finally {
            IOUtils.closeQuietly(is);
        }
	}
	@After
	public void teardown() {
		
	}
	@Test
	public void isExistParticipantsTest() {
		Assert.assertTrue(service.isExistParticipants(1l));
		Assert.assertFalse(service.isExistParticipants(2l));
	}
	@Test
	public void createJrProcessorTest() {
		Assert.assertNotNull(service.createJrProcessor(1l));
	}
	
/*	JRProcessor createJrProcessor(Long promotionId);

	boolean isExistParticipants(Long promoId);*/
}
