package com.transretail.crm.rest.request;

import org.joda.time.LocalDate;

import com.transretail.crm.common.service.dto.request.PagingParam;

/**
 * @author Allan G. Ramirez (agramirez@exist.com)
 */
public class MissingPointsSearchDto {
    private PagingParam pagination = new PagingParam();
    private String accountId;
    private String transactionNo;
    private LocalDate txnDateFrom;
    private LocalDate txnDateTo;
    private String storeCode;

    public PagingParam getPagination() {
        return pagination;
    }

    public void setPagination(PagingParam pagination) {
        this.pagination = pagination;
    }

    public String getAccountId() {
        return accountId;
    }

    public void setAccountId(String accountId) {
        this.accountId = accountId;
    }

    public String getTransactionNo() {
        return transactionNo;
    }

    public void setTransactionNo(String transactionNo) {
        this.transactionNo = transactionNo;
    }

    public LocalDate getTxnDateFrom() {
        return txnDateFrom;
    }

    public void setTxnDateFrom(LocalDate txnDateFrom) {
        this.txnDateFrom = txnDateFrom;
    }

    public LocalDate getTxnDateTo() {
        return txnDateTo;
    }

    public void setTxnDateTo(LocalDate txnDateTo) {
        this.txnDateTo = txnDateTo;
    }

    public String getStoreCode() {
        return storeCode;
    }

    public void setStoreCode(String storeCode) {
        this.storeCode = storeCode;
    }
}
