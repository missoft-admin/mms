create table CRM_GC_RETURN_BY_CARDS (
        id number(19,0) not null,
        CREATED_BY varchar2(255 char),
        CREATED_DATETIME timestamp,
        LAST_UPDATED_BY varchar2(255 char),
        LAST_UPDATED_DATETIME timestamp,
        FACE_AMOUNT number(19,2),
        REC_NO varchar2(255 char),
        REPLACE_AMOUNT number(19,2),
        RETURN_AMOUNT number(19,2),
        RETURN_DATE date,
        STATUS varchar2(255 char),
        CUSTOMER number(19,0),
        ORDER_NO number(19,0),
        REASON varchar2(15 char),
        primary key (id)
    );

    create table CRM_GC_RETURN_BY_CARDS_ITEM (
        id number(19,0) not null,
        CREATED_BY varchar2(255 char),
        CREATED_DATETIME timestamp,
        LAST_UPDATED_BY varchar2(255 char),
        LAST_UPDATED_DATETIME timestamp,
        ENDING_SERIES varchar2(255 char),
        STARTING_SERIES varchar2(255 char),
        TYPE varchar2(255 char),
        REC number(19,0),
        primary key (id)
    );

alter table CRM_GC_RETURN_BY_CARDS 
        add constraint FK_hgh3v1n68efmndswu4jmd99dn 
        foreign key (CUSTOMER) 
        references CRM_GC_CX_PROFILE;

    alter table CRM_GC_RETURN_BY_CARDS 
        add constraint FK_pmdnmuop2stesp5dtjk1jinba 
        foreign key (ORDER_NO) 
        references CRM_SALES_ORD;

    alter table CRM_GC_RETURN_BY_CARDS 
        add constraint FK_kltnmktmeppvd9epwra871iso 
        foreign key (REASON) 
        references CRM_REF_LOOKUP_DTL;

    alter table CRM_GC_RETURN_BY_CARDS_ITEM 
        add constraint FK_svxax4cvdnrfryjxt26363ald 
        foreign key (REC) 
        references CRM_GC_RETURN_BY_CARDS;