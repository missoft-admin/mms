
package com.transretail.crm.report.template;

import org.joda.time.format.DateTimeFormatter;

/**
 * 
 * @author Monte Cillo Co (mco@exist.com)
 */
public interface ReportDateFormatProvider {
    DateTimeFormatter getDefaultDateFormat();
}
