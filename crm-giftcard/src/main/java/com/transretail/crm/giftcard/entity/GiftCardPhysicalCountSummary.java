package com.transretail.crm.giftcard.entity;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.*;
import org.hibernate.annotations.Type;
import org.joda.time.LocalDateTime;
import org.springframework.data.annotation.CreatedBy;

/**
 *
 * @author Monte Cillo Co (mco@exist.com)
 */
@Entity
@Table(name = "CRM_GC_PHYSICAL_COUNT_SUMMARY")
public class GiftCardPhysicalCountSummary implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "ID")
    private Long id;
    @Column(name = "CREATED_DATETIME", nullable = false)
    @Type(type = "org.jadira.usertype.dateandtime.joda.PersistentLocalDateTime")
    private LocalDateTime created;
    @Column(name = "CREATED_BY", nullable = false)
    @CreatedBy
    private String createUser;
    @Column(name = "PRODUCT_PROFILE", nullable = false)
    private String productProfile;
    @Column(name = "LOCATION", nullable = false)
    private String location;
    @Column(name = "CURRENT_INVENTORY")
    private Long currentInventory;
    @Column(name = "PHYSICAL_COUNT")
    private Long physicalCount;
    @OneToMany(mappedBy = "summary", orphanRemoval = true, fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private List<PhysicalCountGiftCardStatus> details;

    public Long getId() {
	return id;
    }

    public void setId(Long id) {
	this.id = id;
    }

    public LocalDateTime getCreated() {
	return created;
    }

    public void setCreated(LocalDateTime created) {
	this.created = created;
    }

    public String getCreateUser() {
	return createUser;
    }

    public void setCreateUser(String createUser) {
	this.createUser = createUser;
    }

    public String getProductProfile() {
	return productProfile;
    }

    public void setProductProfile(String productProfile) {
	this.productProfile = productProfile;
    }

    public String getLocation() {
	return location;
    }

    public void setLocation(String location) {
	this.location = location;
    }

    public Long getCurrentInventory() {
	return currentInventory;
    }

    public void setCurrentInventory(Long currentInventory) {
	this.currentInventory = currentInventory;
    }

    public Long getPhysicalCount() {
	return physicalCount;
    }

    public void setPhysicalCount(Long physicalCount) {
	this.physicalCount = physicalCount;
    }

    public List<PhysicalCountGiftCardStatus> getDetails() {
	return details;
    }

    public void setDetails(List<PhysicalCountGiftCardStatus> details) {
	this.details = details;
    }

    public GiftCardPhysicalCountSummary addFoundItem(GiftCardInventory item) {
	this.addItem(new PhysicalCountGiftCardStatus()
		.status(PhysicalCountGiftCardStatus.Status.FOUND)
		.inventory(item).summary(this));
	return this;
    }

    public GiftCardPhysicalCountSummary addMissingItem(GiftCardInventory item) {
	this.addItem(new PhysicalCountGiftCardStatus()
		.status(PhysicalCountGiftCardStatus.Status.MISSING)
		.inventory(item).summary(this));
	return this;
    }

    public GiftCardPhysicalCountSummary addFoundItems(Iterable<GiftCardInventory> items) {
	for (GiftCardInventory inventory : items) {
	    this.addFoundItem(inventory);
	}
	return this;
    }

    public GiftCardPhysicalCountSummary addMissingItems(Iterable<GiftCardInventory> items) {
	for (GiftCardInventory inventory : items) {
	    this.addMissingItem(inventory);
	}
	return this;
    }

    public GiftCardPhysicalCountSummary addItem(PhysicalCountGiftCardStatus item) {
	if (this.details == null) {
	    details = new ArrayList<PhysicalCountGiftCardStatus>();
	}

	details.add(item);
	return this;
    }
}