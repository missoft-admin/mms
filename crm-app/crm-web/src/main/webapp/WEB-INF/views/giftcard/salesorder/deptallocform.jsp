<%@ include file="../../common/taglibs.jsp" %>

<style type="text/css">
<!--

  #productTable tr td:last-child {
    border-left: 0px;
  }

  #addProductBtn.btn-small {
    margin-bottom: 5px;
  }
  table select, textarea, table input[type="text"], input[type="password"], input[type="datetime"], input[type="datetime-local"], input[type="date"], input[type="month"], input[type="time"], input[type="week"], input[type="number"], input[type="email"], input[type="url"], input[type="search"], input[type="tel"], input[type="color"], .uneditable-input {
      margin-bottom: 0px;
      margin-top: 0px;
  }
  
-->
</style>
<script src="<spring:url value="/js/common/typeaheadutil.js" />"></script>



<c:set var="ENCTYPE" value="application/x-www-form-urlencoded"/>

<div class="modal-dialog">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            <h4><spring:message code="sales_order_so_payment_info" /></h4>
        </div>
        <div class="modal-body">
            <div id="contentError" class="hide alert alert-error">
                <button type="button" class="close" onclick="$(this).parent().hide();">&times;</button>
                <div>
                    <form:errors path="*"/>
                </div>
            </div>
            <form:form id="allocForm" name="allocForm" modelAttribute="allocForm"
                       action="${pageContext.request.contextPath}/giftcard/salesorder/deptalloc/save/"
                       method="POST" enctype="${ENCTYPE}" cssClass="form-reset form-horizontal" >
                <form:hidden path="orderId"/>
                <div class="contentMain">
                    <div class="row-fluid">
                        <div class="span6">

                            <%-- ============= path for order no ================ --%>
                            <div class="control-group">
                                <spring:message code="sales_order_no" var="orderNum" />
                                <label for="orderNo" class="control-label">${orderNum}</label>
                                <div class="controls">
                                    <form:input path="orderNo" class="form-control" placeholder="${orderNum}" readonly="true" />
                                </div>
                            </div>

                                <%-- ==================== path for order date ================== --%>
                            <div class="control-group">
                                <spring:message code="sales_order_date" var="orderDt" />
                                <label for="orderDate" class="control-label">${orderDt}</label>
                                <div class="controls">
                                    <form:input path="orderDate" class="form-control" placeholder="${orderDt}" readonly="true" />
                                </div>
                            </div>
                        </div>
                        <div class="span6">

                            <%-- ================= path for customer name ================== --%>
                            <div class="control-group">
                                <spring:message code="sales_order_customer_name" var="contactPerson" />
                                <label for="customerId" class="control-label">${contactPerson}</label>
                                <div class="controls">
                                    <form:input path="customerName" class="form-control" placeholder="${contactPerson}" readonly="true" />
                                </div>
                            </div>
                                <%-- ================== path for sales order amount ============= --%>
                            <div class="control-group">
                                <spring:message code="order_total_face_amount" var="totalAmount" />
                                <label for="salesOrderAmt" class="control-label">${totalAmount}</label>
                                <div class="controls">
                                    <form:input path="salesOrderAmtFmt" class="form-control" placeholder="${totalAmount}" readonly="true" />
                                    <form:hidden path="salesOrderAmt" readonly="true" />
                                </div>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                        <div class="span12">
                            <div class="pull-left" style="margin-right: 5px;">
                                <button type="button" id="addProductBtn" class="btn btn-small">+</button><br/>
                                <button type="button" id="removeProductBtn" class="btn btn-small">&ndash;</button>
                            </div>
                            <div class="pull-left" style="width:90%;">
                                <table id="productTable" class="table table-condensed table-compressed table-striped">
                                    <thead>
                                    <tr>
                                        <th></th>
                                        <th><b class="required">*</b> <spring:message code="sales_order_department" /></th>
                                        <th><b class="required">*</b> <spring:message code="sales_order_amount" /></th>
                                    </tr>
                                    </thead>
                                    <c:forEach var="item" items="${allocForm.deptAllocs}" varStatus="status" >
                                        <tr data-index="${status.index}">
                                            <td>
                                                <input type="hidden" value="${item.id}" name="deptAllocs[${status.index}].id" />
                                                <input type="checkbox" class="productRemoveFlag" /></td>
                                            <td>
                                                    <%-- <input type="text" value="${item.deptId}" name="deptAllocs[${status.index}].deptId" /> --%>
                                                <div>
                                                    <form:input path="deptAllocs[${status.index}].department.description" />
                                                    <form:hidden path="deptAllocs[${status.index}].department.code" class="deptCodes" />
                                                    <script type='text/javascript'>
                                                        $(document).ready( function() {
                                                            var url = '<c:url value="/giftcard/salesorder/deptalloc/departments/list/null/" />';
                                                            var $deptDesc = $( "#productTable" ).find( "input[name='deptAllocs[${status.index}].department.description']" );
                                                            var $deptCode = $( "#productTable" ).find( "input[name='deptAllocs[${status.index}].department.code']" );
                                                            TypeAhead.process( $deptDesc, $deptCode, url, { label: "description", value: "code" }, null, null, null, null/* function( theUrl ) {
						                	      var excDeptCodes = $( ".deptCodes" ).map( function(){ return this.value; } ).get();
						                	      return theUrl.replace( new RegExp("%EXCDEPTS%", 'g'), excDeptCodes? excDeptCodes : "null" ); 
						                	  } */
                                                            );
                                                        });
                                                    </script>
                                                </div>
                                            </td>
                                            <td>
                                                <%--<input type="text" value="${item.allocAmount}" name="deptAllocs[${status.index}].allocAmount" />--%>
                                                <form:input path="deptAllocs[${status.index}].allocAmount"/>
                                            </td>
                                        </tr>
                                    </c:forEach>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </form:form>
        </div>
        <div class="modal-footer">
            <c:choose>
                <c:when test="${allocForm.orderStatus == 'FOR_ACTIVATION'}">
                    <button type="button" data-status='APPROVED' class="orderSubmit btn btn-primary"><spring:message code="label_approve" /></button>
                    <button type="button" data-status='REJECTED' class="orderSubmit btn btn-primary"><spring:message code="label_reject" /></button>
                </c:when>
                <c:otherwise>
                    <button type="button" data-status='NEW' class="orderSubmit btn btn-primary"><spring:message code="label_submit" /></button>
                </c:otherwise>
            </c:choose>
            <button type="button" id="orderCancel" class="btn btn-default" data-dismiss="modal"><spring:message code="label_cancel"/></button>
        </div>
    </div>
</div>

<script>
$(document).ready(function() {
  
  var CONTAINER_ERROR = "#contentError > div";
  var CONTENT_ERROR = "#contentError";
  $form = $("#allocForm");
  $orderDialog = $("#orderDialog").css({"width": "900px"});
  
  $(".orderSubmit").click(submitForm);
  
  function submitForm() {
    $(".orderSubmit").prop( "disabled", true );
    $.post($form.attr("action") + $(this).data("status"), $form.serialize(), function(data) {
      $(".orderSubmit").prop( "disabled", false );
      if (data.success) {
        location.reload();
      } 
      else {
        errorInfo = "";
        for (i = 0; i < data.result.length; i++) {
          errorInfo += "<br>" + (i + 1) + ". "
              + ( data.result[i].code != undefined ? 
                  data.result[i].code : data.result[i]);
        }
        $orderDialog.find(CONTAINER_ERROR).html("Please correct following errors: " + errorInfo);
        $orderDialog.find(CONTENT_ERROR).show('slow');
      }
    }); 
  }	
  	
  
    var $productTable = $('#productTable');
	
	var  $productTbody = $("tbody", $productTable);
	
	$('#removeProductBtn', $orderDialog ).click(function() {
        $("input.productRemoveFlag:checked").each(function() {
            var $tr = $(this).closest("tr");
            var length = $productTbody.find("tr").length;
            if($tr.is(":first-child") && length == 1) {
                $tr.find("input.productRemoveFlag").prop('checked', false);
                $tr.find("input").removeAttr('value');//.prop('disabled', true);
                $tr.find("select").removeAttr('value');//.prop('disabled', true);
                //$tr.invisible();
                var html = $tr.html().replace( new RegExp("\\["+$tr.data( "index" )+"\\]", 'g'), '['+ 0 +']' );
                $tr.html( html );
                $tr.data( "index", 0 )
            } else
                $tr.remove();
        });
    });

    $('#addProductBtn', $orderDialog ).click(function() {
        var $tr = $productTbody.find("tr:first-child");
        if($tr.css('visibility') == 'hidden' || $tr.css('display') == 'none') {
            $tr.find("input").removeAttr('value').prop('disabled', false).prop('checked', false);
            $tr.find("select").removeAttr('value').prop('disabled', false);
            $tr.visible();
        } else {
            var $trLast = $productTbody.find("tr:last-child");
            var  $row = $tr.clone();
            $row.find("input").removeAttr('value').removeAttr("checked").prop('disabled', false);
            $row.find("select").removeAttr('value').prop('disabled', false);
            var index = +$trLast.data("index") + 1;
            var lastIndex = $tr.data("index");
            $row.data("index", index);
            var rg = new RegExp("\\["+lastIndex+"\\]", 'g');
            var html = $row.html();
            html = html.replace(rg, '['+index+']');
            $row.html(html);

            $productTbody.append($row);
            initFields($row);
        }
    });
    
    
    initFields($orderDialog);
	
	jQuery.fn.visible = function() {
	    return this.css('visibility', 'visible');
	};

	jQuery.fn.invisible = function() {
	    return this.css('visibility', 'hidden');
	};
	
	
	function initFields(e) {
        $('.intInput', $(e)).numberInput({"type" : "int"});
    	$('.floatInput', $(e)).numberInput({"type" : "float"});
    	
    	$("input.paymentDate", $(e)).each(function() {
    		$(this).datepicker({
        		format: 'dd-mm-yyyy',
        		autoclose: true
        	});
    	});
    }
  
});
</script>