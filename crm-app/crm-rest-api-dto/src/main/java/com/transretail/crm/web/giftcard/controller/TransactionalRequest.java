package com.transretail.crm.web.giftcard.controller;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.transretail.crm.common.web.converter.ISODateTimeDeserializer;
import com.transretail.crm.common.web.converter.ISODateTimeSerializer;
import com.transretail.crm.giftcard.dto.GiftCardTransactionInfo;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import org.hibernate.validator.constraints.NotEmpty;
import org.joda.time.LocalDateTime;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.format.annotation.DateTimeFormat.ISO;

/**
 *
 * @author Monte Cillo Co (mco@exist.com)
 */
public class TransactionalRequest extends AbstractGiftCardRequest {

    @NotEmpty
    @Size(max = 20)
    private String transactionNo;
    @NotNull
    @DateTimeFormat(iso = ISO.DATE_TIME)
    @JsonSerialize(using = ISODateTimeSerializer.class)
    @JsonDeserialize(using = ISODateTimeDeserializer.class)
    private LocalDateTime transactionDate;

    public String getTransactionNo() {
        return transactionNo;
    }

    public void setTransactionNo(String transactionNo) {
        this.transactionNo = transactionNo;
    }

    public LocalDateTime getTransactionDate() {
        return transactionDate;
    }

    public void setTransactionDate(LocalDateTime transactionDate) {
        this.transactionDate = transactionDate;
    }

    @Override
    public String getSignature() {
        return String.format("%s%s%s%s", getMerchantId(), getTerminalId(), getCashierId(), getTransactionNo());
    }

    @Override
    public GiftCardTransactionInfo toGiftCardTransactionInfo() {
        return super.templateBaseTransactionInfo()
                .transactionNo(transactionNo)
                .transactionTime(transactionDate);
    }

}
