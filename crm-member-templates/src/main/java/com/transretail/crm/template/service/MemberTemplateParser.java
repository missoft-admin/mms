package com.transretail.crm.template.service;

import java.io.IOException;
import java.io.InputStream;
import java.util.Locale;

import org.apache.poi.openxml4j.exceptions.InvalidFormatException;

/**
 * @author Allan G. Ramirez (agramirez@exist.com)
 */
public interface MemberTemplateParser {
    int parseMembers(InputStream is, String memberTypeCode, Locale locale, Operation operation) throws MemberTemplateParseException,
        InvalidFormatException, IOException;
}
