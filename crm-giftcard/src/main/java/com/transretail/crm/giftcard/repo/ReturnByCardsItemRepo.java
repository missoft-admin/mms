package com.transretail.crm.giftcard.repo;

import com.transretail.crm.giftcard.repo.custom.SalesOrderRepoCustom;
import org.springframework.stereotype.Repository;

import com.transretail.crm.common.repo.CrmQueryDslPredicateExecutor;
import com.transretail.crm.giftcard.entity.ReturnByCardsItem;
import com.transretail.crm.giftcard.entity.SalesOrder;

/**
 *
 */
@Repository
public interface ReturnByCardsItemRepo extends CrmQueryDslPredicateExecutor<ReturnByCardsItem, Long>, SalesOrderRepoCustom {
}
