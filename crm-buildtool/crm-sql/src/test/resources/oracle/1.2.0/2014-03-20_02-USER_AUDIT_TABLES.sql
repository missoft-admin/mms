create table CRM_USER_AUD (
        id number(19,0) not null,
        REV number(19,0) not null,
        REVTYPE number(3,0),
        EMAIL varchar2(255 char),
        ACCOUNT_ENABLED char(1 char),
        FIRST_NAME varchar2(255 char),
        INVENTORY_LOCATION varchar2(255 char),
        LAST_NAME varchar2(255 char),
        MIDDLE_NAME varchar2(255 char),
        password varchar2(255 char),
        STORE_CODE varchar2(255 char),
        username varchar2(255 char),
        primary key (id, REV)
    );

create table CRM_USER_PERMISSION_AUD (
        CODE varchar2(255 char) not null,
        REV number(19,0) not null,
        REVTYPE number(3,0),
        DESCRIPTION varchar2(255 char),
        ROLE_CATEGORY varchar2(1000 char),
        STATUS varchar2(8 char),
        primary key (CODE, REV)
    );

create table CRM_USER_ROLE_AUD (
        CODE varchar2(255 char) not null,
        REV number(19,0) not null,
        REVTYPE number(3,0),
        DESCRIPTION varchar2(255 char),
        ENABLED char(1 char),
        primary key (CODE, REV)
    );

create table CRM_USER_ROLE_PERMISSION_AUD (
        id number(19,0) not null,
        REV number(19,0) not null,
        REVTYPE number(3,0),
        PERM_ID varchar2(255 char),
        ROLE_ID varchar2(255 char),
        USER_ID number(19,0),
        primary key (id, REV)
    );



alter table CRM_USER_AUD 
        add constraint FK_byfy6hi11asf3h8qvd1j4u45p 
        foreign key (REV) 
        references CRM_REVINFO;

alter table CRM_USER_PERMISSION_AUD 
        add constraint FK_e1dlsylkcdjyyr270x34erv45 
        foreign key (REV) 
        references CRM_REVINFO;

alter table CRM_USER_ROLE_AUD 
        add constraint FK_7f1lhw2xcqyey4elh13m12fth 
        foreign key (REV) 
        references CRM_REVINFO;

alter table CRM_USER_ROLE_PERMISSION_AUD 
        add constraint FK_5dlqrrlp218ai1k73sgbnw1af 
        foreign key (REV) 
        references CRM_REVINFO;