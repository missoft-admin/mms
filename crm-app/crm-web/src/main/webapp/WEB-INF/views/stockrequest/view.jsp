<%@include file="/WEB-INF/views/common/taglibs.jsp" %>

<div class="modal-dialog">
	<div class="modal-content">
		<div class="modal-header">
			<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
			<h4 class="modal-title"><spring:message code="label_view"/></h4>
		</div>
		<div class="modal-body form-horizontal">
			<form:form id="stockrequest" modelAttribute="stockrequest" 
				action="${pageContext.request.contextPath}/stockrequest/status/">
				<form:hidden path="id"/>
				
				<div class="control-group">
					<form:label path="requestNo" cssClass="control-label"><spring:message code="gc.stock.label.requestno" /></form:label>
					<div class="controls"><form:input path="requestNo" readonly="true"/> </div>
				</div>
				<div class="control-group">
					<form:label path="transferRefNo" cssClass="control-label"><spring:message code="gc.stock.label.transferrefno" /></form:label>
					<div class="controls"><form:input path="transferRefNo" readonly="true"/></div>
				</div>
				<div class="control-group">
					<form:label path="createUser" cssClass="control-label"><spring:message code="gc.stock.label.requestedby" /></form:label>
					<div class="controls"><form:input path="createUser" readonly="true"/> </div>
				</div>
				<div class="control-group">
					<form:label path="productDescription" cssClass="control-label"><spring:message code="gc.stock.label.cardtype" /></form:label>
					<form:hidden path="productCode"/>
					<div class="controls"><form:input path="productDescription" readonly="true"/> </div>
				</div>
				<div class="control-group">
					<form:label path="sourceLocationDescription" cssClass="control-label"><spring:message code="gc.stock.label.sourcelocation" /></form:label>
					<form:hidden path="sourceLocation"/>
					<div class="controls"><form:input path="sourceLocationDescription" readonly="true"/></div>
				</div>
				<div class="control-group">
					<form:label path="allocateToDescription" cssClass="control-label"><spring:message code="gc.stock.label.allocateto" /></form:label>
					<form:hidden path="allocateTo" />
					<div class="controls"><form:input path="allocateToDescription" readonly="true"/></div>
				</div>
				<div class="control-group">
					<form:label path="quantity" cssClass="control-label"><spring:message code="gc.stock.label.quantity" /></form:label>
					<div class="controls"><form:input path="quantity" readonly="true"/></div>
				<div class="control-group">
					<form:label path="quantity" cssClass="control-label"><spring:message code="gc.stock.label.isforpromo" /></form:label>
					<div class="controls">
						<input type="checkbox" disabled="disabled" ${stockrequest.isForPromo? 'checked' : '' }>
						<form:checkbox path="isForPromo" cssClass="hide"/>
					</div>
				</div>
			</form:form>
		</div>
		<div class="modal-footer">
			<c:if test="${forApproval && stockrequest.status != 'IN_TRANSIT'}">
				<button type="button" id="stockRequestApprove" data-status="APPROVED" class="approve btn btn-primary"><spring:message code="label_approve" /></button>
				<button type="button" id="stockRequestReject" data-status="REJECTED" class="approve btn btn-primary"><spring:message code="label_reject" /></button>
    		</c:if>
    		<button type="button" id="" class="btn btn-default" data-dismiss="modal"><spring:message code="label_cancel"/></button>
		</div>
	</div>
</div>

<script src="<spring:url value="/js/viewspecific/giftcard/stockrequest.js"/>"></script>
<link href="<spring:url value="/styles/temp.css" />" rel="stylesheet">