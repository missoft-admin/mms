<%@ include file="../common/taglibs.jsp"%>

<style type="text/css">
<!--
  .well .input {
    background-color: #fff;
    border: 1px solid #E8E8E8;
    box-shadow: none;    
    padding: 6px;
    height: 18px;
    margin-left: 5px;
    margin-right: 5px;
  }
  .well .btn-primary {
    margin-left: 5px;
  }
  .well select {
    margin: 5px;
  }
  
  #viewTransactionsContent .table tbody tr td:last-child {
    border-left: none;
  }
-->
</style>

<fieldset>
	<div class="page-header page-header2">
		<h1><spring:message code="menutab_programsetup_maintenance_membermonitor" /></h1>
	</div>
	<div class="row-fluid well well-blue">
		<div class="span10" style="margin-top:-20px">
			<c:if test="${isCs}">
				<h3><spring:message code="label_monitor_transactioncount_member" />: </h3>
				<spring:message code="label_member_monitor_transactioncount_description" arguments="${transactionCount}" />
			</c:if>
			<c:if test="${isHr}">
				<h3><spring:message code="label_monitor_transactioncount_employee" />: </h3>
				<spring:message code="label_member_monitor_transactioncount_description" arguments="${transactionCount}" />
			</c:if>
		</div>
		<c:if test="${isSupervisor}">
		<div class="">
			<spring:message code="label_edit" var="editLabel" />
			<input type="button" class="btn btn-small pull-right" id="editTransactionCount" value="${editLabel}"/>
		</div>
		</c:if>
	</div>
</fieldset>

<div id="formDialog" class="modal  hide nofly">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
			  <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h4><spring:message code="label_member_monitor_edit" /></h4>
			</div>
			<div class="modal-body form-horizontal">
				<div hidden="true" class="errors alert">
		            <button type="button" class="close" onclick="hideErrors();">&times;</button>
		            <div class="errorcontent"></div>
	          	</div>
	          	
			
				<c:if test="${isCs}">
                <div class="control-group" id="indField">
                    <label class="control-label"><spring:message code="label_monitor_transactioncount_member" /></label>
                    <div class="controls">
                        <input type="text" name="individual" id="transactionCountInd" value="${transactionCountInd}">
                    </div>
                </div>
                </c:if>
                <c:if test="${isHr}">
                <div class="control-group" id="empField">
                    <label class="control-label"><spring:message code="label_monitor_transactioncount_employee" /></label>
                    <div class="controls">
                        <input type="text" name="employee" id="transactionCountEmp" value="${transactionCountEmp}">
                    </div>
                </div>
                </c:if>
			</div>
			<div class="modal-footer">

			  	<button type="saveButton" id="button_save" class="btn btn-primary"><spring:message code="label_save" /></button>
          <button type="button" class="btn" data-dismiss="modal" id="cancelButton"><spring:message code="label_cancel" /></button>
			</div>
		</div>
	</div>
</div>

<fieldset id="monitorTable">
	<legend class=""><spring:message code="label_monitored_members" /></legend>
	  <form:form id="searchDto">
    <div class="form-inline form-search well clearfix">
           
      <div class="pull-left" style="margin-top:-7px; margin-bottom: -9px">
    	<label class="label-single"><h5><spring:message code="label_filter" />:</h5></label>
      <input type="text" id="dateSearch" name="date" class="input" />
      <spring:message code="label_member_transactioncount" var="txnCount"/>
      <input type="text" id="transactionCount" name="transactionCount" placeholder="${txnCount}"class="input"/>
      <c:if test="${isCs}">
      <select name="memberType">
      	<option value=""><spring:message code="member_monitor_report_membertype" /></option>
      	<c:forEach items="${memberTypes}" var="type">
      	<option value="${type.code}">${type.description}</option>
      	</c:forEach>
      </select>
      </c:if>
    	<input type="button" id="search" value="<spring:message code="button_search" />" class="btn btn-primary"/>
    	<input type="button" id="reset" value="<spring:message code="label_reset" />" class="btn "/>
    	</div>       
        
    </div>
    
    <div class="form-horizontal pull-left">
      <button type="button" id="printMemberMonitor" class="btn pull-right btn-print"><spring:message code="label_print"/></button>
    </div>
    <div class="clearfix"></div>
    </form:form>
    
    
	<div>
		<div id="monitoredMembers" 
	    data-url="<c:url value="/member/monitor/update/${role}" />"
	    data-hdr-username="<spring:message code="label_com_transretail_crm_entity_customermodel_username" />"
	    data-hdr-accountno="<spring:message code="label_com_transretail_crm_entity_customermodel_accountnumber" />"
	    data-hdr-member="<spring:message code="label_com_transretail_crm_entity_customermodel" />"
	    data-hdr-transactioncount="<spring:message code="label_member_transactioncount" />"></div>
	</div>
</fieldset>

<div class="hide">
<span id="mainBtn" style="width: 15%">
	<spring:url value="/member/monitor/%ACCOUNTID%" var="url_showPoints"/>
	<a href="#" class="btn btn-primary pull-left tiptip icn-view" title="<spring:message code="points_label_showtxns" />" 
		onclick="showTransactions('%ACCOUNTID%')">
		<spring:message code="points_label_showtxns" />
	</a>
</span>
</div>

<div class="modal hide  groupModal nofly" tabindex="-1" role="dialog" aria-hidden="true" id="viewTxnDialog">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-body" id="viewTransactionsContent">
			</div>
			<div class="modal-footer">
				<spring:message code="label_cancel" var="cancelLabel" />
			  	<!-- <input type="button" class="btn" value="${cancelLabel}" onclick="hideTransactions()"/> -->
          <button type="button" id="formCancel" data-dismiss="modal" class="btn btn-default"><spring:message code="label_cancel"/></button>
			</div>
		</div>
	</div>
</div>

<jsp:include page="report.jsp" />

<link href="<spring:url value="/css/dataTables.css" />" rel="stylesheet">
<script src="<spring:url value="/js/jquery/jquery.dataTables.js" />" ></script>
<script src="<spring:url value="/js/jquery/jquery.fnReloadAjax.js" />" ></script>
<script src="<spring:url value="/js/jquery/jquery.dataTables.linkLengthMenu.js" />"></script>
<script src="<spring:url value="/js/jquery/jquery.dataTables.customPaging.js" />"></script>
<script src="<spring:url value="/js/jquery/ajax-datatable.js" />" ></script>
<script src="<spring:url value="/js/common/form2js.js" />" ></script>
<script src="<spring:url value="/js/common/json2.js" />" ></script>
<script src="<spring:url value="/js/jquery/jquery.toObject.js" />" ></script>
<script src="<spring:url value="/js/jquery/jquery.numberInput.js" />" ></script>
<script src='<c:url value="/js/bootstrap/bootstrap-datepicker.js"/>' type='text/javascript'><![CDATA[&nbsp; ]]></script>
<link href="<c:url value="/css/bootstrap/datepicker.css" />" rel="stylesheet" />

<style type="text/css">

#dateSearch {
    padding-bottom: 4px;
}
.modal-footer {
    text-align: center !important;
}
</style>

<script type="text/javascript">

$(document).ready(function() {
	memberList = $("#monitoredMembers");
	containerErrors = $(".errors");
	
	var headers = {
		username: memberList.data("hdrUsername")  + "",
		accountno: memberList.data("hdrAccountno") + "",
		transactions: memberList.data("hdrTransactioncount") + "",
		name: memberList.data("hdrMember") + ""
	};
	
	table = memberList.ajaxDataTable({
		'autoload': true,
		'ajaxSource': memberList.data("url"),
		'columnHeaders': [
			headers['username'],
			headers['accountno'],
			headers['name'],
			headers['transactions'],
			""
		],
		'modelFields': [
		    {name: 'username'},
		    {name: 'accountId'},
		    {name : 'firstName', sortable : true, fieldCellRenderer : function (data, type, row) {
		           return row.formattedMemberName;
	         }},
		    {name: 'transactionCount'},
		    {customCell: function(innerData, sSpecific, json) {
			    if(sSpecific == 'display') {
				    var btnHtml = $("#mainBtn").html();
				    var regEx = new RegExp("%ACCOUNTID%", 'g');
				    btnHtml = btnHtml.replace(regEx, json.accountId);
					return btnHtml;
				}
			    return "";
			}}
		]
	});

	form = $("#formDialog");
	
	$("#editTransactionCount").click(function() {
		form.modal("show");
	});

	$("#button_cancel").click(function(){
		form.modal("hide");
		$("#transactionCountInd").val("${transactionCountInd}");
		$("#transactionCountEmp").val("${transactionCountEmp}");
		hideErrors();
	});

	$("#button_save").click(function() {
		saveTransactionCount();
	});

    $("#printMemberMonitor").click(function() {
        $("#printReport").modal('show');
    }); 

    var now = new Date();
    $("#dateSearch").datepicker({
 		format		:	'dd-mm-yyyy',
 		autoclose	:	true,
 	});
    $("#dateSearch").datepicker('setValue', "${currentDate}");
    $("#dateSearch").val("${currentDate}");
    $("#transactionCount").val("${transactionCount}");

    $("#search").click(function() {
    	var formObj = $("#searchDto").toObject( { mode : 'first', skipEmpty : true } );
        console.log(formObj);
        memberList.ajaxDataTable('search', formObj);
    });

    $("#reset").click(function() {
    	$("#dateSearch").val("${currentDate}");
        $("#transactionCount").val("${transactionCount}");
    });

    $("#search").click();
});

function showTransactions(accountId) {
	$.ajax({
		type	: "GET",
		url		: "${pageContext.request.contextPath}/member/monitor/" + accountId + "/" + $("#dateSearch").val(),
		dataType: "html",
		success	: function(response) {
			console.log($(response).find("#mainBody"));
			$("#viewTransactionsContent").html($(response).find("#mainBody"));
			$("#viewTxnDialog").modal("show");
		}
	});
}

function hideTransactions() {
	$("#viewTxnDialog").modal("hide");
}

function saveTransactionCount() {
    var data = new Object();
    data["individual"] = $("#transactionCountInd").val();
    data['employee'] = $("#transactionCountEmp").val();

	$.ajax({
		type		: "POST",
		url			: "${pageContext.request.contextPath}" + "/member/monitor/save/",
		dataType	: "json",
        data        : JSON.stringify(data),
		contentType	: "application/json",
		success		: function(response) {
			if(response.success) {
				form.modal("hide");
				window.location.reload();
			}
			else {
				errorInfo = "";
				var count = 0;
				for (var i = 0; i < response.result.length; i++) {
					count++;
					errorInfo += "<br>" + (count) + ". "
					+ ( response.result[i].code != undefined ? 
							response.result[i].code : response.result[i]);
				}
				$(".errorcontent").html("<strong>Please correct following errors: </strong>" + errorInfo);
				containerErrors.show('slow');
			}
		}
	});
}

function hideErrors() {
	containerErrors.hide("slow");	
}
</script>