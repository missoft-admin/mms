package com.transretail.crm.core.service;

import java.util.List;


import com.transretail.crm.common.reporting.jasper.JRProcessor;
import com.transretail.crm.core.dto.DataAnalysisLMHDto;
import com.transretail.crm.core.dto.DataAnalysisOverviewDto;
import com.transretail.crm.core.dto.DataAnalysisSearchDto;


public interface DataAnalysisService {

	List<DataAnalysisOverviewDto> getOverviewAnalysis(DataAnalysisSearchDto searchDto);

	List<Integer> getYears();

	List<DataAnalysisLMHDto> getLMHAnalysis(DataAnalysisSearchDto searchDto);

	JRProcessor createOverviewJrProcessor(DataAnalysisSearchDto searchDto);

	JRProcessor createLMHJrProcessor(DataAnalysisSearchDto searchDto);
	
	
}
