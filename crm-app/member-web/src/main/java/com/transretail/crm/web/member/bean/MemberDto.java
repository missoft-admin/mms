package com.transretail.crm.web.member.bean;

import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.validation.Valid;
import javax.validation.constraints.Past;
import javax.validation.constraints.Pattern;

import com.transretail.crm.core.entity.enums.MemberStatus;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.BooleanUtils;
import org.apache.commons.lang3.StringUtils;
import org.hibernate.validator.constraints.NotEmpty;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.util.AutoPopulatingList;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.transretail.crm.core.entity.MemberModel;
import com.transretail.crm.core.entity.embeddable.Address;
import com.transretail.crm.core.entity.embeddable.Channel;
import com.transretail.crm.core.entity.embeddable.CustomerProfile;
import com.transretail.crm.core.entity.embeddable.MarketingDetails;
import com.transretail.crm.core.entity.embeddable.Npwp;
import com.transretail.crm.core.entity.embeddable.ProfessionalProfile;
import com.transretail.crm.core.entity.enums.MarketingInformedAbout;
import com.transretail.crm.core.entity.enums.Status;
import com.transretail.crm.core.entity.lookup.LookupDetail;
import com.transretail.crm.core.util.StringUtility;

/**
 * @author Allan G. Ramirez (agramirez@exist.com)
 */
public class MemberDto {
    // Personal Info
    private String preferedStoreCode;
    private String username; // Read Only
    private String accountId; // Read Only
    private String cardNo; // Read Only
    private MemberStatus status; // Read Only
    @NotEmpty
    private List<String> contact;
    @NotEmpty
    private String memberTypeCode;
    private String companyName;
    @Valid
    private Npwp npwp = new Npwp();
    private String firstName;
    private String middleName;
    private String lastName;
    private String ktpId;
    private String storeName;
    private String empTypeCode;
    private String idNumber;
    private String homePhone;
    @Past
    @DateTimeFormat(pattern = "dd-MM-yyyy")
    private Date birthDate;
    @NotEmpty
    @Pattern(regexp = "^$|^([0-9a-zA-Z]([-.\\w]*[0-9a-zA-Z])*@([0-9a-zA-Z][-\\w]*[0-9a-zA-Z]\\.)+[a-zA-Z]{2,9})")
    private String email;
    private String postalCode;
    private String preferedLanguageCode;
    private List<String> bestTimeToCall;
    private boolean alertBySms;
    private boolean alertByEmail;
    private boolean alertByMail;
    private boolean alertByPhone;
    // Customer Profile
    private String nameNative;
    private String maritalStatusCode;
    private String educationCode;
    private String occupationCode;
    private String householdIncomeCode;
    private String personalIncomeCode;
    private String familySizeCode;
    private String religionCode;
    private String placeOfBirth;
    private String faxNo;
    private String closestStore;
    private String nationalityCode;
    private String gender;
    private boolean insuranceOwnership;
    private boolean creditCardOwnership;
    private Integer childrenCountCode;
    private Integer totalDomesticHelpers;
    private Integer totalCarsOwned;
    private List<Integer> childrenAges;
    // Professional Profile
    private String customerGroupCode;
    private String customerSegmentationCode;
    private String departmentCode;
    private String businessFieldCode;
    private String businessEmail;
    private String businessLicense;
    private String businessPhone;
    private String position;
    private String zone;
    private String radius;
    private Short transactionCode1;
    private Short transactionCode2;
    private Short transactionCode3;
    private Short transactionCode4;
    private Short transactionCodeShort;
    private Short transactionCodeLong1;
    private Short transactionCodeLong2;
    private String registrationId;
    private String noOfEmp;
    private String potentialBuyingPerMonth;
    // Address
	private String street;
	private String streetNumber;
	private String block;
	private String building;
	private Integer floor;
	private String room;
	private String village;
	private String subdistrict;
	private String district;
	private String city;
	private String postCode;
	private Integer km;
	private String rt;
	private String rw;
	private String province;
	
	// Business Address
	private String businessStreet;
	private String businessStreetNumber;
	private String businessBlock;
	private String businessBuilding;
	private Integer businessFloor;
	private String businessRoom;
	private String businessVillage;
	private String businessSubdistrict;
	private String businessDistrict;
	private String businessCity;
	private String businessPostCode;
	private Integer businessKm;
	private String businessRt;
	private String businessRw;
	private String businessProvince;
	private boolean hasLoyaltyCard;
	private String loyaltyCardIssued;
	private String loyaltyCardExpiry;
	private String registrationDate;
	
	private String parentAccountId;
	private String parentName;
	
	//Marketing Details
	private String correspondenceAddress;
	private List<String> informedAbout = new AutoPopulatingList<String>(String.class);
	private List<String> interestedProducts = new AutoPopulatingList<String>(String.class);
	private List<String> visitedSupermarkets = new AutoPopulatingList<String>(String.class);
	private List<String> interestedActivities = new AutoPopulatingList<String>(String.class);
    private Integer tripsToStore;
	private String informedAboutOthers;

	private List<String> vehiclesOwned = new AutoPopulatingList<String>(String.class);

//    private List<String> petCodes = new AutoPopulatingList<String>(String.class);
//    private List<String> cuisineCodes = new AutoPopulatingList<String>(String.class);
    private List<String> interestCodes = new AutoPopulatingList<String>(String.class);
    private List<String> bankCodes = new AutoPopulatingList<String>(String.class);

    private List<String> socialMediaAccts;

    private static final String COMMA = ",";

    public MemberDto() {

    }

    public MemberDto(MemberModel model) {
    	this.parentAccountId = model.getParentAccountId();
    	if(model.getCreated() != null) {
    		this.registrationDate = model.getCreated().toString("MM-dd-yyyy");
    	}
    	else {
    		this.registrationDate = "";
    	}
    	this.loyaltyCardExpiry = "";
    	this.loyaltyCardIssued = "";
        this.preferedStoreCode = model.getRegisteredStore();
        this.username = model.getUsername();
        this.accountId = model.getAccountId();
        this.cardNo = model.getCardNo();
        this.status = model.getAccountStatus();
        this.contact = StringUtility.generateList(String.class, model.getContact().split(","));
        this.memberTypeCode = getCode(model.getMemberType());
        this.companyName = model.getCompanyName();
        this.npwp = model.getNpwp();
        this.firstName = model.getFirstName();
        this.middleName = model.getMiddleName();
        this.lastName = model.getLastName();
        this.email = model.getEmail();
        this.ktpId = model.getKtpId();
        this.storeName = model.getStoreName();
        this.empTypeCode = getCode(model.getEmpType());
        this.idNumber = model.getIdNumber();
        this.homePhone = model.getHomePhone();
        if(model.getBestTimeToCall() != null) {
        	this.bestTimeToCall = StringUtility.generateList(String.class, model.getBestTimeToCall().split(","));
        }
        if ( StringUtils.isNotBlank( model.getSocialMediaAccts() ) ) {
        	socialMediaAccts = Lists.newArrayList( model.getSocialMediaAccts().split( "," ) );
        }

        Channel channel = model.getChannel();
        if (channel != null) {
            alertBySms = BooleanUtils.toBoolean(channel.getAcceptSMS());
            alertByEmail = BooleanUtils.toBoolean(channel.getAcceptEmail());
            alertByMail = BooleanUtils.toBoolean(channel.getAcceptMail());
            alertByPhone = BooleanUtils.toBoolean(channel.getAcceptPhone());
        }
        CustomerProfile profile = model.getCustomerProfile();
        if (profile != null) {
//            this.address = profile.getAddress();
        	this.gender = getCode(profile.getGender());
            this.postalCode = profile.getPostalCode();
            this.nameNative = profile.getNameNative();
            this.maritalStatusCode = getCode(profile.getMaritalStatus());
            this.educationCode = getCode(profile.getEducation());
            this.occupationCode = getCode(profile.getOccupation());
            this.householdIncomeCode = getCode(profile.getHouseholdIncome());
            this.personalIncomeCode = getCode(profile.getPersonalIncome());
            this.familySizeCode = getCode(profile.getFamilySize());
            this.childrenCountCode = (profile.getChildren());
            this.totalDomesticHelpers = profile.getDomesticHelpers();
            this.totalCarsOwned = profile.getCarsOwned();
            this.birthDate = profile.getBirthdate();
            this.preferedLanguageCode = getCode(profile.getPreferredLanguage());
            this.religionCode = getCode(profile.getReligion());
            this.placeOfBirth = profile.getPlaceOfBirth();
            this.faxNo = profile.getFaxNo();
            this.closestStore = profile.getClosestStore();
            this.nationalityCode = getCode(profile.getNationality());
            this.creditCardOwnership = BooleanUtils.toBoolean(profile.getCreditCardOwnership());
            this.insuranceOwnership = BooleanUtils.toBoolean(profile.getInsuranceOwnership());
            this.childrenAges = profile.getAgeOfChildren();
//            if (profile.getPets() != null) {
//                for (LookupDetail detail : profile.getPets()) {
//                    petCodes.add(detail.getCode());
//                }
//            }
//            if (profile.getFoodPreference() != null) {
//                for (LookupDetail detail : profile.getFoodPreference()) {
//                    cuisineCodes.add(detail.getCode());
//                }
//            }
            if (profile.getInterests() != null) {
                interestCodes = StringUtility.generateList(String.class, profile.getInterests().split(","));
            }
//            if (profile.getBanks() != null) {
//            	for (LookupDetail detail : profile.getBanks()) {
//            		bankCodes.add(detail.getCode());
//            	}
//            }
            if(profile.getBanks() != null) {
            	bankCodes = StringUtility.generateList(String.class, profile.getBanks().split(","));
        	}
        	if ( StringUtils.isNotBlank( profile.getVehiclesOwned() ) ) {
                for ( String str : profile.getVehiclesOwned().split( "," ) ) {
                    vehiclesOwned.add(str);
                }
            }
        }
        
        MarketingDetails marketing = model.getMarketingDetails();
        if(marketing != null) {
        	this.correspondenceAddress = marketing.getCorrespondenceAddress();
        	if (marketing.getInformedAbout() != null) {
                for (String str : marketing.getInformedAbout().split(",")) {
                	if(str.startsWith("INFO"))
                		informedAbout.add(str);
                	else
                		informedAboutOthers = str;
                }
            }
        	if (marketing.getInterestedProducts() != null) {
                for (String str : marketing.getInterestedProducts().split(",")) {
                    interestedProducts.add(str);
                }
            }
        	if (marketing.getVisitedSupermarkets() != null) {
                for (String str : marketing.getVisitedSupermarkets().split(",")) {
                    visitedSupermarkets.add(str);
                }
            }
        	if (marketing.getInterestedActivities() != null) {
                for (String str : marketing.getInterestedActivities().split(",")) {
                    interestedActivities.add(str);
                }
            }
        	tripsToStore = marketing.getTripsToStore();
        }
        ProfessionalProfile work = model.getProfessionalProfile();
        if(work != null) {
            departmentCode = getCode(work.getDepartment());
            businessFieldCode = getCode(work.getBusinessField());
        	businessEmail = work.getBusinessEmail();
        	position = work.getPosition();
        	businessLicense = work.getBusinessLicense();
        	zone = work.getZone();
        	radius = work.getRadius();
        	customerGroupCode = getCode(work.getCustomerGroup()); 
        	customerSegmentationCode = getCode(work.getCustomerSegmentation());
        	transactionCode1 = work.getTransactionCode1();
        	transactionCode2 = work.getTransactionCode2();
        	transactionCode3 = work.getTransactionCode3();
        	transactionCode4 = work.getTransactionCode4();
        	transactionCodeLong1 = work.getTransactionCodeLong1();
        	transactionCodeLong2 = work.getTransactionCodeLong2();
        	transactionCodeShort = work.getTransactionCodeShort();
        	registrationId = work.getRegistrationId();
        	businessPhone = work.getBusinessPhone();
        	noOfEmp = getCode(work.getNoOfEmp());
        	potentialBuyingPerMonth = work.getPotentialBuyingPerMonth();
        	
        	Address businessAddress = work.getBusinessAddress();
        	if(businessAddress != null) {
        		businessStreet = businessAddress.getStreet();
            	businessStreetNumber = businessAddress.getStreetNumber();
            	_LOG.debug("businessAddress: STREET NO=" + streetNumber);
            	businessBlock = businessAddress.getBlock();
            	businessKm = businessAddress.getKm();
            	businessRt = businessAddress.getRt();
            	businessRw = businessAddress.getRw();
            	businessBuilding = businessAddress.getBuilding();
            	businessFloor = businessAddress.getFloor();
            	businessRoom = businessAddress.getRoom();
            	businessSubdistrict = businessAddress.getSubdistrict();
            	businessDistrict = businessAddress.getDistrict();
            	businessCity = businessAddress.getCity();
            	businessPostCode = businessAddress.getPostCode();
            	businessProvince = businessAddress.getProvince();
        	}
        }
        
        Address address = model.getAddress();
        if(address != null) {
        	this.street = address.getStreet();
        	this.streetNumber = address.getStreetNumber();
        	_LOG.debug("ADDRESS: STREET NO=" + streetNumber);
        	this.block = address.getBlock();
        	this.km = address.getKm();
        	this.rt = address.getRt();
        	this.rw = address.getRw();
        	this.building = address.getBuilding();
        	this.floor = address.getFloor();
        	this.room = address.getRoom();
        	this.subdistrict = address.getSubdistrict();
        	this.district = address.getDistrict();
        	this.city = address.getCity();
        	this.postCode = address.getPostCode();
        	this.province = address.getProvince();
        }
    }
    
    public static final Logger _LOG = LoggerFactory.getLogger(MemberDto.class);

    public String getPreferedStoreCode() {
        return preferedStoreCode;
    }

    public void setPreferedStoreCode(String preferedStoreCode) {
        this.preferedStoreCode = preferedStoreCode;
    }

    public String getUsername() {
        return username;
    }

    public String getAccountId() {
        return accountId;
    }

    public String getCardNo() {
        return cardNo;
    }

    public MemberStatus getStatus() {
        return status;
    }

    public List<String> getContact() {
        return contact;
    }

    public void setContact(List<String> contact) {
        this.contact = contact;
    }

    public String getMemberTypeCode() {
        return memberTypeCode;
    }

    public void setMemberTypeCode(String memberTypeCode) {
        this.memberTypeCode = memberTypeCode;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public Npwp getNpwp() {
        return npwp;
    }

    public void setNpwp(Npwp npwp) {
        this.npwp = npwp;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getMiddleName() {
        return middleName;
    }

    public void setMiddleName(String middleName) {
        this.middleName = middleName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public Date getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(Date birthDate) {
        this.birthDate = birthDate;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPostalCode() {
        return postalCode;
    }

    public void setPostalCode(String postalCode) {
        this.postalCode = postalCode;
    }

    public String getPreferedLanguageCode() {
        return preferedLanguageCode;
    }

    public void setPreferedLanguageCode(String preferedLanguageCode) {
        this.preferedLanguageCode = preferedLanguageCode;
    }

    public boolean isAlertBySms() {
        return alertBySms;
    }

    public void setAlertBySms(boolean alertBySms) {
        this.alertBySms = alertBySms;
    }

    public boolean isAlertByEmail() {
        return alertByEmail;
    }

    public void setAlertByEmail(boolean alertByEmail) {
        this.alertByEmail = alertByEmail;
    }

    public boolean isAlertByMail() {
        return alertByMail;
    }

    public void setAlertByMail(boolean alertByMail) {
        this.alertByMail = alertByMail;
    }

    public boolean isAlertByPhone() {
        return alertByPhone;
    }

    public void setAlertByPhone(boolean alertByPhone) {
        this.alertByPhone = alertByPhone;
    }

    public String getNameNative() {
        return nameNative;
    }

    public void setNameNative(String nameNative) {
        this.nameNative = nameNative;
    }

    public String getMaritalStatusCode() {
        return maritalStatusCode;
    }

    public void setMaritalStatusCode(String maritalStatusCode) {
        this.maritalStatusCode = maritalStatusCode;
    }

    public String getEducationCode() {
        return educationCode;
    }

    public void setEducationCode(String educationCode) {
        this.educationCode = educationCode;
    }

    public String getOccupationCode() {
        return occupationCode;
    }

    public void setOccupationCode(String occupationCode) {
        this.occupationCode = occupationCode;
    }

    public String getHouseholdIncomeCode() {
        return householdIncomeCode;
    }

    public void setHouseholdIncomeCode(String householdIncomeCode) {
        this.householdIncomeCode = householdIncomeCode;
    }

    public String getPersonalIncomeCode() {
        return personalIncomeCode;
    }

    public void setPersonalIncomeCode(String personalIncomeCode) {
        this.personalIncomeCode = personalIncomeCode;
    }

    public String getFamilySizeCode() {
        return familySizeCode;
    }

    public void setFamilySizeCode(String familySizeCode) {
        this.familySizeCode = familySizeCode;
    }

    public Integer getChildrenCountCode() {
        return childrenCountCode;
    }

    public void setChildrenCountCode(Integer childrenCountCode) {
        this.childrenCountCode = childrenCountCode;
    }

    public Integer getTotalDomesticHelpers() {
        return totalDomesticHelpers;
    }

    public void setTotalDomesticHelpers(Integer totalDomesticHelpers) {
        this.totalDomesticHelpers = totalDomesticHelpers;
    }

    public Integer getTotalCarsOwned() {
        return totalCarsOwned;
    }

    public void setTotalCarsOwned(Integer totalCarsOwned) {
        this.totalCarsOwned = totalCarsOwned;
    }

    public List<String> getInterestCodes() {
        return interestCodes;
    }

    /**
     * Map version of interestCodes. This is used to optimize checking if interest code exists when rendering interests lookup
     *
     * @return map of interest codes
     */
    public Map<String, String> getInterestCodeMap() {
        Map<String, String> map = Maps.newHashMap();
        if (interestCodes != null) {
            for (String code : interestCodes) {
                map.put(code, code);
            }
        }
        return map;
    }

    public void setInterestCodes(List<String> interestCodes) {
        this.interestCodes = interestCodes;
    }

    public MemberDto withUsername(String username) {
        this.username = username;
        return this;
    }

    /**
     * Updates the member model parameter with the values of the fields in this DTO
     *
     * @param model
     * @return
     */
    public MemberModel update(MemberModel model) {
        CustomerProfile profile = model.getCustomerProfile();
        if (profile == null) {
            profile = new CustomerProfile();
            model.setCustomerProfile(profile);
        }
        Channel channel = model.getChannel();
        if (channel == null) {
            channel = new Channel();
            model.setChannel(channel);
        }
        ProfessionalProfile work = model.getProfessionalProfile();
        if(work == null) {
        	work = new ProfessionalProfile();
        	model.setProfessionalProfile(work);
        }
        MarketingDetails marketing = model.getMarketingDetails();
        if(marketing == null) {
        	marketing = new MarketingDetails();
        	model.setMarketingDetails(marketing);
        }
        
        marketing.setCorrespondenceAddress(correspondenceAddress);
        marketing.setInformedAbout(toString(informedAbout));
        marketing.setInterestedProducts(toString(interestedProducts));
        marketing.setVisitedSupermarkets(toString(visitedSupermarkets));
        marketing.setInterestedActivities(toString(interestedActivities));
        marketing.setTripsToStore( tripsToStore );
        
        // Personal Info
        model.setRegisteredStore(preferedStoreCode);
        model.setMemberType(createLookupDetail(memberTypeCode));
        model.setCompanyName(companyName);
        model.setNpwp(npwp);
        model.setFirstName(firstName);
        model.setMiddleName(middleName);
        model.setLastName(lastName);
        model.setEmail(email);
        model.setKtpId(ktpId);
        model.setStoreName(storeName);
        model.setEmpType(createLookupDetail(empTypeCode));
        model.setIdNumber(idNumber);
        model.setHomePhone(homePhone);
        
        if(contact != null && contact.size() > 0) {
        	StringBuilder strContact = new StringBuilder(contact.get(0));
            for(int i = 1; i < contact.size(); i++) {
            	if(!StringUtils.isBlank(contact.get(i))) strContact.append(COMMA).append(contact.get(i));
            }
            model.setContact(strContact.toString());
        }

        StringBuilder strTimeToCall = new StringBuilder();
        if(bestTimeToCall != null && bestTimeToCall.size() > 0) {
        	strTimeToCall = new StringBuilder( StringUtils.isNotBlank( bestTimeToCall.get(0) )? bestTimeToCall.get(0).trim() : "" );
            for(int i = 1; i < bestTimeToCall.size(); i++) {
            	if(StringUtils.isNotBlank(bestTimeToCall.get(i))) strTimeToCall.append(COMMA).append(bestTimeToCall.get(i).trim());
            }
        }
        model.setBestTimeToCall(strTimeToCall.toString());

        StringBuilder strSocialMedia = new StringBuilder();
        if(socialMediaAccts != null && socialMediaAccts.size() > 0) {
        	strSocialMedia = new StringBuilder( StringUtils.isNotBlank( socialMediaAccts.get(0) )? socialMediaAccts.get(0).trim() : "" );
            for(int i = 1; i < socialMediaAccts.size(); i++) {
            	if(StringUtils.isNotBlank(socialMediaAccts.get(i))) strSocialMedia.append(COMMA).append(socialMediaAccts.get(i).trim());
            }
        }
        model.setSocialMediaAccts( strSocialMedia.toString() );

        StringBuilder strVehiclesOwned = new StringBuilder();
        if( vehiclesOwned != null && vehiclesOwned.size() > 0 ) {
        	strVehiclesOwned = new StringBuilder( StringUtils.isNotBlank( vehiclesOwned.get(0) )? vehiclesOwned.get(0).trim() : "" );
            for(int i = 1; i < vehiclesOwned.size(); i++) {
            	if( StringUtils.isNotBlank( vehiclesOwned.get(i) ) ) strVehiclesOwned.append( COMMA ).append( vehiclesOwned.get(i).trim() );
            }
        }
        profile.setVehiclesOwned( strVehiclesOwned.toString() );

        profile.setPostalCode(postalCode);
        profile.setPreferredLanguage(createLookupDetail(preferedLanguageCode));
        channel.setAcceptSMS(alertBySms);
        channel.setAcceptEmail(alertByEmail);
        channel.setAcceptMail(alertByMail);
        channel.setAcceptPhone(alertByPhone);
        // Other Details
        profile.setBirthdate(birthDate);
        profile.setGender( StringUtils.isNotBlank( this.gender )? new LookupDetail( this.gender ) : null );
        profile.setNameNative(nameNative);
        profile.setMaritalStatus(createLookupDetail(maritalStatusCode));
        profile.setEducation(createLookupDetail(educationCode));
        profile.setOccupation(createLookupDetail(occupationCode));
        profile.setHouseholdIncome(createLookupDetail(householdIncomeCode));
        profile.setPersonalIncome(createLookupDetail(personalIncomeCode));
        profile.setFamilySize(createLookupDetail(familySizeCode));
        profile.setChildren((childrenCountCode));
        profile.setDomesticHelpers(totalDomesticHelpers);
        profile.setCarsOwned(totalCarsOwned);
        profile.setReligion(createLookupDetail(religionCode));
        profile.setClosestStore(closestStore);
        profile.setFaxNo(faxNo);
        profile.setPlaceOfBirth(placeOfBirth);
        profile.setInsuranceOwnership(insuranceOwnership);
        profile.setCreditCardOwnership(creditCardOwnership);
        profile.setNationality(createLookupDetail(nationalityCode));
        _LOG.debug("CHILDREN AGES: " + childrenAges);
        profile.setAgeOfChildren(childrenAges);
        // work
        work.setBusinessEmail(businessEmail);
        work.setDepartment(createLookupDetail(departmentCode));
        work.setBusinessField(createLookupDetail(businessFieldCode));
        work.setPosition(position);
        work.setBusinessLicense(businessLicense);
        work.setZone(zone);
        work.setRadius(radius);
        work.setCustomerGroup(createLookupDetail(customerGroupCode));
        work.setCustomerSegmentation(createLookupDetail(customerSegmentationCode));
        work.setTransactionCode1(transactionCode1);
        work.setTransactionCode2(transactionCode2);
        work.setTransactionCode3(transactionCode3);
        work.setTransactionCode4(transactionCode4);
        work.setTransactionCodeLong1(transactionCodeLong1);
        work.setTransactionCodeLong2(transactionCodeLong2);
        work.setTransactionCodeShort(transactionCodeShort);
        work.setRegistrationId(registrationId);
        work.setBusinessPhone(businessPhone);
        work.setNoOfEmp(createLookupDetail(noOfEmp));
        work.setPotentialBuyingPerMonth(potentialBuyingPerMonth);
        
        Address businessAddress = work.getBusinessAddress();
        if(businessAddress == null) {
        	businessAddress = new Address();
        	work.setBusinessAddress(businessAddress);
        }
        businessAddress.setStreet(businessStreet);
        businessAddress.setStreetNumber(businessStreetNumber);
        businessAddress.setBlock(businessBlock);
        businessAddress.setKm(businessKm);
        businessAddress.setRt(businessRt);
        businessAddress.setRw(businessRw);
        businessAddress.setBuilding(businessBuilding);
        businessAddress.setFloor(businessFloor);
        businessAddress.setRoom(businessRoom);
        businessAddress.setDistrict(businessDistrict);
        businessAddress.setSubdistrict(businessSubdistrict);
        businessAddress.setCity(businessCity);
        businessAddress.setPostCode(businessPostCode);
        businessAddress.setProvince(businessProvince);
        
//        List<LookupDetail> pets = profile.getPets();
//        if (pets == null) {
//            pets = Lists.newArrayList();
//            profile.setPets(pets);
//        } else {
//            pets.clear();
//        }
//        for (String code : petCodes) {
//            pets.add(new LookupDetail(code));
//        }
//
//        List<LookupDetail> cuisines = profile.getFoodPreference();
//        if (cuisines == null) {
//            cuisines = Lists.newArrayList();
//            profile.setFoodPreference(cuisines);
//        } else {
//            cuisines.clear();
//        }
//        for (String code : cuisineCodes) {
//            cuisines.add(new LookupDetail(code));
//        }

        if(interestCodes != null && interestCodes.size() > 0) {
        	StringBuilder strInterests = new StringBuilder(interestCodes.get(0));
            for(int i = 1; i < bankCodes.size(); i++) {
            	if(!StringUtils.isBlank(interestCodes.get(i))) strInterests.append(COMMA).append(interestCodes.get(i));
            }
            profile.setInterests(strInterests.toString());
        }
        
//        List<LookupDetail> banks = profile.getBanks();
//        if (banks == null) {
//        	banks = Lists.newArrayList();
//            profile.setBanks(banks);
//        } else {
//        	banks.clear();
//        }
//        for (String code : bankCodes) {
//        	banks.add(new LookupDetail(code));
//        }
        
        if(bankCodes != null && bankCodes.size() > 0) {
        	StringBuilder strBanks = new StringBuilder(bankCodes.get(0));
            for(int i = 1; i < bankCodes.size(); i++) {
            	if(!StringUtils.isBlank(bankCodes.get(i))) strBanks.append(COMMA).append(bankCodes.get(i));
            }
            profile.setBanks(strBanks.toString());
        }
        
        Address address = model.getAddress();
        if(address == null) {
        	address = new Address();
        	model.setAddress(address);
        }
        address.setStreet(street);
        address.setStreetNumber(streetNumber);
        address.setBlock(block);
        address.setKm(km);
        address.setRt(rt);
        address.setRw(rw);
        address.setBuilding(building);
        address.setFloor(floor);
        address.setRoom(room);
        address.setDistrict(district);
        address.setSubdistrict(subdistrict);
        address.setCity(city);
        address.setPostCode(postCode);
        address.setProvince(province);

        return model;
    }
    
    private String toString(List<String> strs) {
    	if(CollectionUtils.isNotEmpty(strs))
    		return strs.toString().replaceAll(" ", "").replaceAll("\\[", "").replaceAll("\\]", "");
    	return null;
    }

    private LookupDetail createLookupDetail(String code) {
        if (StringUtils.isNotBlank(code)) {
            return new LookupDetail(code);
        }
        return null;
    }

    private String getCode(LookupDetail detail) {
        return detail != null ? detail.getCode() : null;
    }

	public String getStreetNumber() {
		return streetNumber;
	}

	public void setStreetNumber(String streetNumber) {
		this.streetNumber = streetNumber;
	}

	public String getBlock() {
		return block;
	}

	public void setBlock(String block) {
		this.block = block;
	}

	public String getBuilding() {
		return building;
	}

	public void setBuilding(String building) {
		this.building = building;
	}

	public Integer getFloor() {
		return floor;
	}

	public void setFloor(Integer floor) {
		this.floor = floor;
	}

	public String getRoom() {
		return room;
	}

	public void setRoom(String room) {
		this.room = room;
	}

	public String getVillage() {
		return village;
	}

	public void setVillage(String village) {
		this.village = village;
	}

	public String getSubdistrict() {
		return subdistrict;
	}

	public void setSubdistrict(String subdistrict) {
		this.subdistrict = subdistrict;
	}

	public String getDistrict() {
		return district;
	}

	public void setDistrict(String district) {
		this.district = district;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getPostCode() {
		return postCode;
	}

	public void setPostCode(String postCode) {
		this.postCode = postCode;
	}

	public Integer getKm() {
		return km;
	}

	public void setKm(Integer km) {
		this.km = km;
	}

	public void setStatus(MemberStatus status) {
		this.status = status;
	}

	public String getStreet() {
		return street;
	}

	public void setStreet(String street) {
		this.street = street;
	}

	public String getKtpId() {
		return ktpId;
	}

	public void setKtpId(String ktpId) {
		this.ktpId = ktpId;
	}

	public String getReligionCode() {
		return religionCode;
	}

	public void setReligionCode(String religionCode) {
		this.religionCode = religionCode;
	}

	public String getPlaceOfBirth() {
		return placeOfBirth;
	}

	public void setPlaceOfBirth(String placeOfBirth) {
		this.placeOfBirth = placeOfBirth;
	}

	public String getFaxNo() {
		return faxNo;
	}

	public void setFaxNo(String faxNo) {
		this.faxNo = faxNo;
	}

	public String getClosestStore() {
		return closestStore;
	}

	public void setClosestStore(String closestStore) {
		this.closestStore = closestStore;
	}

	public boolean isInsuranceOwnership() {
		return insuranceOwnership;
	}

	public void setInsuranceOwnership(boolean insuranceOwnership) {
		this.insuranceOwnership = insuranceOwnership;
	}

	public boolean isCreditCardOwnership() {
		return creditCardOwnership;
	}

	public void setCreditCardOwnership(boolean creditCardOwnership) {
		this.creditCardOwnership = creditCardOwnership;
	}

    public String getDepartmentCode() {
        return departmentCode;
    }

    public void setDepartmentCode(String departmentCode) {
        this.departmentCode = departmentCode;
    }

    public String getBusinessFieldCode() {
		return businessFieldCode;
	}

	public void setBusinessFieldCode(String businessFieldCode) {
		this.businessFieldCode = businessFieldCode;
	}

	public String getBusinessEmail() {
		return businessEmail;
	}

	public void setBusinessEmail(String businessEmail) {
		this.businessEmail = businessEmail;
	}

	public String getPosition() {
		return position;
	}

	public void setPosition(String position) {
		this.position = position;
	}

	public void setCardNo(String cardNo) {
		this.cardNo = cardNo;
	}

	public List<String> getBestTimeToCall() {
		return bestTimeToCall;
	}

	public void setBestTimeToCall(List<String> bestTimeToCall) {
		this.bestTimeToCall = bestTimeToCall;
	}

	public String getNationalityCode() {
		return nationalityCode;
	}

	public void setNationalityCode(String nationalityCode) {
		this.nationalityCode = nationalityCode;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public String getBusinessLicense() {
		return businessLicense;
	}

	public void setBusinessLicense(String businessLicense) {
		this.businessLicense = businessLicense;
	}

	public String getStoreName() {
		return storeName;
	}

	public void setStoreName(String storeName) {
		this.storeName = storeName;
	}

	public String getZone() {
		return zone;
	}

	public void setZone(String zone) {
		this.zone = zone;
	}

	public String getRadius() {
		return radius;
	}

	public void setRadius(String radius) {
		this.radius = radius;
	}

	public Short getTransactionCode1() {
		return transactionCode1;
	}

	public void setTransactionCode1(Short transactionCode1) {
		this.transactionCode1 = transactionCode1;
	}

	public Short getTransactionCode2() {
		return transactionCode2;
	}

	public void setTransactionCode2(Short transactionCode2) {
		this.transactionCode2 = transactionCode2;
	}

	public Short getTransactionCode3() {
		return transactionCode3;
	}

	public void setTransactionCode3(Short transactionCode3) {
		this.transactionCode3 = transactionCode3;
	}

	public Short getTransactionCode4() {
		return transactionCode4;
	}

	public void setTransactionCode4(Short transactionCode4) {
		this.transactionCode4 = transactionCode4;
	}

	public Short getTransactionCodeShort() {
		return transactionCodeShort;
	}

	public void setTransactionCodeShort(Short transactionCodeShort) {
		this.transactionCodeShort = transactionCodeShort;
	}

	public Short getTransactionCodeLong1() {
		return transactionCodeLong1;
	}

	public void setTransactionCodeLong1(Short transactionCodeLong1) {
		this.transactionCodeLong1 = transactionCodeLong1;
	}

	public Short getTransactionCodeLong2() {
		return transactionCodeLong2;
	}

	public void setTransactionCodeLong2(Short transactionCodeLong2) {
		this.transactionCodeLong2 = transactionCodeLong2;
	}

	public String getBusinessStreet() {
		return businessStreet;
	}

	public void setBusinessStreet(String businessStreet) {
		this.businessStreet = businessStreet;
	}

	public String getBusinessStreetNumber() {
		return businessStreetNumber;
	}

	public void setBusinessStreetNumber(String businessStreetNumber) {
		this.businessStreetNumber = businessStreetNumber;
	}

	public String getBusinessBlock() {
		return businessBlock;
	}

	public void setBusinessBlock(String businessBlock) {
		this.businessBlock = businessBlock;
	}

	public String getBusinessBuilding() {
		return businessBuilding;
	}

	public void setBusinessBuilding(String businessBuilding) {
		this.businessBuilding = businessBuilding;
	}

	public Integer getBusinessFloor() {
		return businessFloor;
	}

	public void setBusinessFloor(Integer businessFloor) {
		this.businessFloor = businessFloor;
	}

	public String getBusinessRoom() {
		return businessRoom;
	}

	public void setBusinessRoom(String businessRoom) {
		this.businessRoom = businessRoom;
	}

	public String getBusinessVillage() {
		return businessVillage;
	}

	public void setBusinessVillage(String businessVillage) {
		this.businessVillage = businessVillage;
	}

	public String getBusinessSubdistrict() {
		return businessSubdistrict;
	}

	public void setBusinessSubdistrict(String businessSubdistrict) {
		this.businessSubdistrict = businessSubdistrict;
	}

	public String getBusinessDistrict() {
		return businessDistrict;
	}

	public void setBusinessDistrict(String businessDistrict) {
		this.businessDistrict = businessDistrict;
	}

	public String getBusinessCity() {
		return businessCity;
	}

	public void setBusinessCity(String businessCity) {
		this.businessCity = businessCity;
	}

	public String getBusinessPostCode() {
		return businessPostCode;
	}

	public void setBusinessPostCode(String businessPostCode) {
		this.businessPostCode = businessPostCode;
	}

	public Integer getBusinessKm() {
		return businessKm;
	}

	public void setBusinessKm(Integer businessKm) {
		this.businessKm = businessKm;
	}


	public String getEmpTypeCode() {
		return empTypeCode;
	}

	public void setEmpTypeCode(String empTypeCode) {
		this.empTypeCode = empTypeCode;
	}

	public String getCustomerGroupCode() {
		return customerGroupCode;
	}

	public void setCustomerGroupCode(String customerGroupCode) {
		this.customerGroupCode = customerGroupCode;
	}

	public String getCustomerSegmentationCode() {
		return customerSegmentationCode;
	}

	public void setCustomerSegmentationCode(String customerSegmentationCode) {
		this.customerSegmentationCode = customerSegmentationCode;
	}

	public String getProvince() {
		return province;
	}

	public void setProvince(String province) {
		this.province = province;
	}

	public String getBusinessProvince() {
		return businessProvince;
	}

	public void setBusinessProvince(String businessProvince) {
		this.businessProvince = businessProvince;
	}

	public String getRt() {
		return rt;
	}

	public void setRt(String rt) {
		this.rt = rt;
	}

	public String getRw() {
		return rw;
	}

	public void setRw(String rw) {
		this.rw = rw;
	}

	public String getBusinessRt() {
		return businessRt;
	}

	public void setBusinessRt(String businessRt) {
		this.businessRt = businessRt;
	}

	public String getBusinessRw() {
		return businessRw;
	}

	public void setBusinessRw(String businessRw) {
		this.businessRw = businessRw;
	}

	public List<Integer> getChildrenAges() {
		return childrenAges;
	}

	public void setChildrenAges(List<Integer> childrenAges) {
		this.childrenAges = childrenAges;
	}

	public List<String> getBankCodes() {
		return bankCodes;
	}

	public void setBankCodes(List<String> bankCodes) {
		this.bankCodes = bankCodes;
	}
	
	public Map<String, String> getBankCodeMap() {
        Map<String, String> map = Maps.newHashMap();
        if (bankCodes != null) {
            for (String code : bankCodes) {
                map.put(code, code);
            }
        }
        return map;
    }

	public String getRegistrationId() {
		return registrationId;
	}

	public void setRegistrationId(String registrationId) {
		this.registrationId = registrationId;
	}

	public String getBusinessPhone() {
		return businessPhone;
	}

	public void setBusinessPhone(String businessPhone) {
		this.businessPhone = businessPhone;
	}

	public String getNoOfEmp() {
		return noOfEmp;
	}

	public void setNoOfEmp(String noOfEmp) {
		this.noOfEmp = noOfEmp;
	}

	public String getPotentialBuyingPerMonth() {
		return potentialBuyingPerMonth;
	}

	public void setPotentialBuyingPerMonth(String potentialBuyingPerMonth) {
		this.potentialBuyingPerMonth = potentialBuyingPerMonth;
	}

	public String getIdNumber() {
		return idNumber;
	}

	public void setIdNumber(String idNumber) {
		this.idNumber = idNumber;
	}

	public String getHomePhone() {
		return homePhone;
	}

	public void setHomePhone(String homePhone) {
		this.homePhone = homePhone;
	}

	public String getCorrespondenceAddress() {
		return correspondenceAddress;
	}

	public void setCorrespondenceAddress(String correspondenceAddress) {
		this.correspondenceAddress = correspondenceAddress;
	}

	public List<String> getInformedAbout() {
		return informedAbout;
	}

	public void setInformedAbout(List<String> informedAbout) {
		this.informedAbout = informedAbout;
	}

	public List<String> getInterestedProducts() {
		return interestedProducts;
	}

	public void setInterestedProducts(List<String> interestedProducts) {
		this.interestedProducts = interestedProducts;
	}

	public List<String> getVisitedSupermarkets() {
		return visitedSupermarkets;
	}

	public void setVisitedSupermarkets(List<String> visitedSupermarkets) {
		this.visitedSupermarkets = visitedSupermarkets;
	}

	public List<String> getInterestedActivities() {
		return interestedActivities;
	}

	public void setInterestedActivities(List<String> interestedActivities) {
		this.interestedActivities = interestedActivities;
	}

	public Integer getTripsToStore() {
		return tripsToStore;
	}

	public void setTripsToStore(Integer tripsToStore) {
		this.tripsToStore = tripsToStore;
	}

	public Map<String, String> getInformedAboutMap() {
        Map<String, String> map = Maps.newHashMap();
        if (informedAbout != null) {
            for (String code : informedAbout) {
                map.put(code, code);
            }
        }
        return map;
    }
	
	public Map<String, String> getInterestedProdsMap() {
        Map<String, String> map = Maps.newHashMap();
        if (interestedProducts != null) {
            for (String code : interestedProducts) {
                map.put(code, code);
            }
        }
        return map;
    }

	public String getInformedAboutOthers() {
		return informedAboutOthers;
	}

	public void setInformedAboutOthers(String informedAboutOthers) {
		this.informedAboutOthers = informedAboutOthers;
	}

	public List<String> getVehiclesOwned() {
		return vehiclesOwned;
	}

	public void setVehiclesOwned(List<String> vehiclesOwned) {
		this.vehiclesOwned = vehiclesOwned;
	}

	public String getLoyaltyCardIssued() {
		return loyaltyCardIssued;
	}

	public void setLoyaltyCardIssued(String loyaltyCardIssued) {
		this.loyaltyCardIssued = loyaltyCardIssued;
	}

	public String getLoyaltyCardExpiry() {
		return loyaltyCardExpiry;
	}

	public void setLoyaltyCardExpiry(String loyaltyCardExpiry) {
		this.loyaltyCardExpiry = loyaltyCardExpiry;
	}

	public String getRegistrationDate() {
		return registrationDate;
	}

	public void setRegistrationDate(String registrationDate) {
		this.registrationDate = registrationDate;
	}

	public String getParentAccountId() {
		return parentAccountId;
	}

	public void setParentAccountId(String parentAccountId) {
		this.parentAccountId = parentAccountId;
	}

	public String getParentName() {
		return parentName;
	}

	public void setParentName(String parentName) {
		this.parentName = parentName;
	}

	public void setUsername(String username) {
		this.username = username;
	}

    public boolean hasLoyaltyCard() {
	return hasLoyaltyCard;
    }

    public void setHasLoyaltyCard(boolean hasLoyaltyCard) {
	this.hasLoyaltyCard = hasLoyaltyCard;
    }

	public List<String> getSocialMediaAccts() {
		return socialMediaAccts;
	}

	public void setSocialMediaAccts(List<String> socialMediaAccts) {
		this.socialMediaAccts = socialMediaAccts;
	}
}
