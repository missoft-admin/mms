package com.transretail.crm.web.controller;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.text.WordUtils;
import org.joda.time.format.DateTimeFormat;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.google.common.base.Predicate;
import com.google.common.collect.Collections2;
import com.transretail.crm.common.service.dto.request.PageSortDto;
import com.transretail.crm.common.service.dto.response.ResultList;
import com.transretail.crm.common.util.DateUtil;
import com.transretail.crm.core.dto.EmployeePurchaseTxnDto;
import com.transretail.crm.core.dto.MemberDto;
import com.transretail.crm.core.dto.MemberResultList;
import com.transretail.crm.core.dto.MemberSearchDto;
import com.transretail.crm.core.entity.MemberModel;
import com.transretail.crm.core.entity.Province;
import com.transretail.crm.core.entity.enums.EmployeeStatusSearchLookup;
import com.transretail.crm.core.entity.enums.ReportType;
import com.transretail.crm.core.entity.enums.SearchCriteria;
import com.transretail.crm.core.entity.lookup.LookupDetail;
import com.transretail.crm.core.entity.lookup.LookupHeader;
import com.transretail.crm.core.entity.lookup.Store;
import com.transretail.crm.core.service.CodePropertiesService;
import com.transretail.crm.core.service.EmployeePurchaseService;
import com.transretail.crm.core.service.EmployeeService;
import com.transretail.crm.core.service.LocationService;
import com.transretail.crm.core.service.LookupService;
import com.transretail.crm.core.service.MemberService;
import com.transretail.crm.core.service.StoreService;
import com.transretail.crm.core.util.CalendarUtil;
import com.transretail.crm.web.controller.util.ControllerResponse;
import com.transretail.crm.web.controller.util.MemberValidator;

import java.util.Collections;
import java.util.Comparator;

@RequestMapping("/employeemodels")
@Controller
public class EmployeeController {
	private static final Logger LOGGER = LoggerFactory.getLogger(EmployeeController.class);
	
	@Autowired
	MemberService memberService;;
	
	@Autowired
	EmployeeService employeeService;
	
	@Autowired
	EmployeePurchaseService employeePurchaseService;
	
	@Autowired
	MemberValidator memberValidator;
	
	@Autowired
	StoreService storeService;
	
	@Autowired
	LookupService lookupService;

    @Autowired
    private CodePropertiesService codePropertiesService;
    
    @Autowired
    MemberController memberController;
    
    @Autowired
    LocationService locationService;
	
    private static final String PATH_PRINT_FORM = "employeemodels/printList";
	private static final String UI_ATTR_MODEL_SEARCH = "searchEmployeeModel";
	private static final String UI_ATTR_MODEL              = "customerModel";
	private static final String UI_ATTR_MODELS = "employeeModels";
	private static final String UI_ATTR_TRANSACTION_MODELS = "employeeTransactions";
	private static final String UI_ATTR_FIELD_SEARCHBY = "employeeCriteria";
	private static final String UI_ATTR_CUST_GROUPS		   = "customerGroups";
	
	private static final String UI_ATTR_EMPLOYEE_FLAG = "isEmployee";
	
	private static final String UI_ATTR_DECIMAL_PATTERN = "employeeModel_decimal_format";
	private static final String UI_ATTR_DATE_PATTERN = "employeeModel_birthdate_date_format";
	
	private static final String UI_ATTR_FIELD_STORES       = "preferredStore";


	@InitBinder
	void initBinder(WebDataBinder binder) {
		DateFormat theFormat = new SimpleDateFormat( "dd-MM-yyyy" );
		theFormat.setLenient( false );
		binder.registerCustomEditor( Date.class, new CustomDateEditor( theFormat, true ) );
	}

	
	void populateForm(Model uiModel, MemberDto customerModel) {
        uiModel.addAttribute(UI_ATTR_MODEL, customerModel);
        uiModel.addAttribute(UI_ATTR_FIELD_STORES, storeService.getAllStores());
        uiModel.addAttribute(UI_ATTR_EMPLOYEE_FLAG, true);
        uiModel.addAttribute( "employeeType", lookupService.getDetailsByHeaderCode( codePropertiesService.getHeaderEmpType() ) );
        Map<String, List<LookupDetail>> lookups = new HashMap<String, List<LookupDetail>>();
        for(LookupHeader header : lookupService.getMemberHeaders()) {
        	String fieldName = formatWord(header.getDescription());
        	LOGGER.debug(fieldName);
        	lookups.put(fieldName, lookupService.getActiveDetails(header));
        }
        uiModel.addAttribute("configurableFieldHeaders", lookupService.getMemberConfigurableFields());
        lookups.get("memberType").remove(codePropertiesService.getDetailMemberTypeCompany());
		final List<LookupDetail> departments = lookups.get("department"); // refactor if all options is must be sorted in natural order
	
	Collections.sort(departments == null? Collections.EMPTY_LIST : departments, 
		new Comparator<LookupDetail>(){

	    @Override
	    public int compare(LookupDetail o1, LookupDetail o2) {
		return StringUtils.defaultIfBlank(o1.getDescription(),"")
			.compareToIgnoreCase(StringUtils.defaultIfBlank(o2.getDescription(), ""));
	    }	    
	});
	lookups.put("department", departments);
	
        List<LookupDetail> customerGroups = lookupService.getActiveDetails(lookupService.getHeader((codePropertiesService.getHeaderCustomerGroup())));
        List<LookupDetail> customerSegmentation = lookupService.getActiveDetails(lookupService.getHeader((codePropertiesService.getHeaderCustomerSegmentation())));
        lookups.put(UI_ATTR_CUST_GROUPS, customerGroups);
        Map<String, Collection<LookupDetail>> segmentMap = new HashMap<String, Collection<LookupDetail>>();
        for(LookupDetail detail : customerGroups) {
        	final String detailCode = detail.getCode();
        	Collection<LookupDetail> segmentation = Collections2.filter(customerSegmentation, new Predicate<LookupDetail>() {
				@Override
				public boolean apply(LookupDetail input) {
					
					char code = detailCode.charAt(detailCode.length() - 1);
					return (input.getCode().charAt(4) == code);
				}
        	});
        	segmentMap.put(detailCode, segmentation);
        }
        uiModel.addAllAttributes(lookups);
        uiModel.addAttribute("customerSegmentation", segmentMap);
        List<Province> provinces = locationService.getAllProvinces();
		uiModel.addAttribute("provinces", provinces);
        addDateTimeFormatPatterns(uiModel);
        uiModel.addAttribute("informedAbout", lookupService.getActiveDetailsByHeaderCode(codePropertiesService.getHeaderInformedAbout()));
    }
	
	private String formatWord(String word) {
    	word = word.toLowerCase();
    	return Character.toLowerCase(word.charAt(0)) + 
    			WordUtils.capitalize(word).replace(" ", "").substring(1);
    }
	
	private void addDateTimeFormatPatterns(Model uiModel) {
        uiModel.addAttribute(
                "customerModel_birthdate_date_format",
                DateTimeFormat.patternForStyle("M-",
                        LocaleContextHolder.getLocale()));
    }
	
	private void populateSearchBox(Model uiModel, MemberDto employeeDto) {
		uiModel.addAttribute(UI_ATTR_MODEL_SEARCH, employeeDto);
		uiModel.addAttribute( UI_ATTR_FIELD_SEARCHBY, SearchCriteria.values() );
		uiModel.addAttribute( "searchExcept", SearchCriteria.BUSINESS_NAME.getValue() );
		uiModel.addAttribute("status", EmployeeStatusSearchLookup.values());
	}
	
	private void addDataFormatPatterns(Model uiModel) {
        uiModel.addAttribute(UI_ATTR_DATE_PATTERN, DateTimeFormat.patternForStyle("M-", LocaleContextHolder.getLocale()));
        uiModel.addAttribute(UI_ATTR_DECIMAL_PATTERN, "###,###.00");
    }
	
	@RequestMapping(params={"page", "size"}, produces = "text/html")
    public String list(
            @RequestParam(value = "page", required = false) Integer page,
            @RequestParam(value = "size", required = false) Integer size,
            Model uiModel) {
		populateSearchBox(uiModel, new MemberDto());
		populateForm(uiModel, new MemberDto());

		uiModel.addAttribute("reportTypes", ReportType.values());
        return "employeemodels/list";
    }

	@RequestMapping(produces = "text/html")
    public String list(Model uiModel) {
		populateSearchBox(uiModel, new MemberDto());
		populateForm(uiModel, new MemberDto());

        return "employeemodels/list";
    }
	
	@RequestMapping(value = "/list", method = RequestMethod.POST)
    public @ResponseBody MemberResultList list(@RequestBody MemberSearchDto searchDto) {
		Calendar dateFrom = Calendar.getInstance();
		dateFrom.setTime( CalendarUtil.INSTANCE.getPrevMonthStartDate() );
		Calendar dateTo = Calendar.getInstance();
		dateTo.setTime( CalendarUtil.INSTANCE.getPrevMonthEndDate() );
    	return employeeService.listDto(searchDto, dateFrom, dateTo );
    }
	
	
	@RequestMapping(value = "/purchase/list/{employeeId}", method = RequestMethod.POST)
    public @ResponseBody ResultList<EmployeePurchaseTxnDto> list(
    		@PathVariable("employeeId") String employeeId,
    		@RequestBody PageSortDto searchDto) {
    	return employeePurchaseService.listTransactionDto(employeeId, searchDto);
    }
	
	
	@RequestMapping(value = "/search", produces = "text/html")
	public String list(@ModelAttribute(UI_ATTR_MODEL_SEARCH) MemberDto employee, Model uiModel) {
		uiModel.addAttribute(UI_ATTR_MODELS, employeeService.findByCriteria(employee, codePropertiesService.getDetailMemberTypeEmployee()));
		populateSearchBox(uiModel, employee);
		populateForm(uiModel, new MemberDto());
		return "employeemodels/list";
	}
	
	@RequestMapping(value = "/show/transaction/{type}/{id}/{postingdatefrom}/{postingdateto}", produces = "text/html")
	public String show(@PathVariable("id") String id,
			@PathVariable("type") String type,
			@PathVariable("postingdatefrom") String postingDateFrom,
			@PathVariable("postingdateto") String postingDateTo, 
			@RequestParam(value = "page", required = false) Integer page,
            @RequestParam(value = "size", required = false) Integer size,
            Model uiModel) {
		addDataFormatPatterns(uiModel);
		
		Date postingDateTimeFrom = null;
        Date postingDateTimeTo = null;
        
        if(page == null) page = 1;
        if(size == null) size = Integer.MAX_VALUE;
        int sizeNo = size == null ? 10 : size.intValue();
        final int firstResult = page == null ? 0 : (page.intValue() - 1) * sizeNo;
        
        
    	try {
    		postingDateTimeFrom = DateUtil.convertDateString(DateUtil.SEARCH_DATE_FORMAT, postingDateFrom);
        } catch (ParseException e) {
            LOGGER.error("Error Parsing date parameter {} ", postingDateFrom, e.getMessage());
            postingDateFrom = "";
        }
    	
    	try {
    		postingDateTimeTo = DateUtil.convertDateString(DateUtil.SEARCH_DATE_FORMAT, postingDateTo);
        } catch (ParseException e) {
            LOGGER.error("Error Parsing date parameter {} ", postingDateTo, e.getMessage());
            postingDateTo = "";
        }
		
		uiModel.addAttribute("itemId", id);
		uiModel.addAttribute("postingDateFrom", postingDateFrom);
        uiModel.addAttribute("postingDateTo", postingDateTo);
        uiModel.addAttribute("url", "/show/transaction/{type}/");
        uiModel.addAttribute("type", type);
        
        MemberDto theEmp = new MemberDto();
        
		if(type.equalsIgnoreCase("purchase")) {
			uiModel.addAttribute(UI_ATTR_TRANSACTION_MODELS, employeePurchaseService.findPurchaseTxnByIdAndPostingDates(id, postingDateTimeFrom, postingDateTimeTo));
		}
		
		uiModel.addAttribute(UI_ATTR_MODEL, theEmp);
		
		return "show";
    }
	
	@RequestMapping(value = "/{id}", method = RequestMethod.DELETE, produces = "text/html")
    public String delete(@PathVariable("id") Long id,
            @RequestParam(value = "page", required = false) Integer page,
            @RequestParam(value = "size", required = false) Integer size,
            Model uiModel) {
        MemberModel customerModel = memberService
                .findCustomerModel(id);
        memberService.deleteCustomerModel(customerModel);
        uiModel.asMap().clear();
        uiModel.addAttribute("page", (page == null) ? "1" : page.toString());
        uiModel.addAttribute("size", (size == null) ? "10" : size.toString());
        return "redirect:/employeemodels";
    }
	
	@RequestMapping(value = "/populate/{id}", method = RequestMethod.POST, produces = "application/json; charset=utf-8")
    @ResponseBody
    public ControllerResponse populateMember(
    		@PathVariable("id") String id,
    		@ModelAttribute(value=UI_ATTR_MODEL) MemberDto member,
            BindingResult result,
            HttpServletRequest request, Model uiModel) {

    	if (id.matches("[0-9]*")) {
    		return memberController.populateMember(id, member, result, request, uiModel);
    		
    	} else {
    		ControllerResponse response = new ControllerResponse();    	
        	response.setSuccess(true);
        	
    		MemberDto professional = new MemberDto(memberService.generateInitModel());
    		professional.setMemberType(lookupService.getDetail(codePropertiesService.getDetailMemberTypeEmployee()));
        	
        	response.setResult(professional);
        	return response;
    	}
    }
	
	@RequestMapping(value = "/new", method = RequestMethod.POST, produces = "application/json; charset=utf-8")
    @ResponseBody
    public ControllerResponse createMember(
    		@ModelAttribute(value=UI_ATTR_MODEL) 
    		MemberDto member,
            BindingResult result,
            HttpServletRequest request) {
		member.setMemberType(lookupService.getDetail(codePropertiesService.getDetailMemberTypeEmployee()));
        return memberController.createMember(member, result, request);
    }
	
	@RequestMapping(value = "/update/{id}", method = RequestMethod.POST, produces = "application/json; charset=utf-8")
    @ResponseBody
    public ControllerResponse updateMember(
    		@PathVariable("id") Long id,
    		@ModelAttribute(value=UI_ATTR_MODEL) MemberDto member,
            BindingResult result,
            HttpServletRequest request) {
		member.setMemberType(lookupService.getDetail(codePropertiesService.getDetailMemberTypeEmployee()));
        return memberController.updateMember(id, member, result, request);
    }
	
	@RequestMapping( value="/member/store/", produces="application/json; charset=utf-8" )
	public @ResponseBody ControllerResponse getStores() {

		ControllerResponse theResp = new ControllerResponse();

		List<Store> stores = new ArrayList<Store>();
		CollectionUtils.addAll( stores, storeService.getAllStores().iterator() );

		theResp.setResult( stores );
		theResp.setSuccess( true );

		return theResp;
	}

    @RequestMapping( value="/member/store/{id}", produces="application/json; charset=utf-8" )
	public @ResponseBody ControllerResponse getStores( @PathVariable(value = "id") Long id ) {

		ControllerResponse theResp = getStores();

		@SuppressWarnings("unchecked")
		List<Store> stores = (List<Store>) theResp.getResult();
		if ( null != id ) {
			MemberModel member = memberService.findCustomerModel( id );
			if ( StringUtils.isNotBlank( member.getRegisteredStore() ) ) {
				Store store = storeService.getStoreByCode( member.getRegisteredStore() );
				if ( !stores.contains( store ) ) {
					stores.add( store );
				}
			}
		}
		theResp.setResult( stores );
		theResp.setSuccess( true );

		return theResp;
	}

    @RequestMapping(value = "/printList", produces="text/html")
    public String printListForm(@ModelAttribute MemberSearchDto searchDto, Model model) {
        searchDto.setMemberType(lookupService.getDetailByCode("MTYP002"));
        Long count = memberService.countMembers(searchDto);
        List<Integer> segments = new ArrayList<Integer>();
        Integer segment = 0 ;
        for (segment = 1; count>(segment*memberService.getMemberReportLimit())+1; segment++)
            segments.add(segment);
        //last segment to be added
        segments.add(segment);
        
        model.addAttribute("memberCount", count);
        if (count > 0) {
            model.addAttribute("segments", segments);
        }
        return PATH_PRINT_FORM;
    }
}
