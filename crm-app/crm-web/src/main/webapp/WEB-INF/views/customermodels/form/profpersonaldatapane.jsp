<%@ include file="../../common/taglibs.jsp" %>


<fieldset class="form-horizontal container-fluid">
    <%--
    <div class="control-group"><spring:message
    		code="label_com_transretail_crm_entity_customermodel_ktpid"
    		var="ktpId" />
    	<label for="ktpId" class="control-label"><b class="required">*</b> 
        <c:out value="${ktpId}" /></label>
    	<div class="controls">
    		<form:input path="ktpId" cssClass="form-control"
    			placeholder="${ktpId}" />
    	</div>
    </div>
     --%>
     
    <div class="control-group"><spring:message
        code="label_com_transretail_crm_entity_customermodel_idnumber"
        var="idNumber" />
      <label for="idNumber" class="control-label"><b class="required">*</b> 
        <c:out value="${idNumber}" /></label>
      <div class="controls">
        <form:hidden path="idNumber" />
        <input type="text" name="idNumberTxt" class="form-control"
          placeholder="${idNumber}" />
          <div class="radio mt10"><input type="radio" name="idNumberType" value="KTPe"><label><spring:message code="member_label_ktp" /></label></div>
          <div class="radio"><input type="radio" name="idNumberType" value="SIM"><label><spring:message code="member_label_sim" /></label></div>
          <div class="radio"><input type="radio" name="idNumberType" value="PASSPORT"><label><spring:message code="member_label_passport" /></label></div>
      </div>
    </div>
    
    <div class="control-group">
      <spring:message
        code="label_com_transretail_crm_entity_customermodel_firstname"
        var="firstName" />
      <label for="firstName" class="control-label"> <b
        class="required">*</b>
      <c:out value="${firstName}" />
      </label>
      <div class="controls">
        <input type="text" name="firstName" class="form-control"
          placeholder="${firstName}" />
      </div>
    </div>
    <div class="control-group">
      <spring:message
        code="label_com_transretail_crm_entity_customermodel_lastname"
        var="lastName" />
      <label for="lastName" class="control-label">
      <c:out value="${lastName}" />
      </label>
      <div class="controls">
        <input type="text" name="lastName" class="form-control"
          placeholder="${lastName}" />
      </div>
    </div>
    
    
    <div class="control-group">
      <label for="birthdate" class="control-label"> <b
        class="required">*</b> <spring:message
          code="label_com_transretail_crm_entity_customermodel_birthdate" />
      </label>
      <div class="controls">
        <form:input path="customerProfile.birthdate" id="birthdate" cssClass="birthdate" />
      </div>
    </div>
    
    <div class="control-group">
      <label for="gender" class="control-label"> <b
        class="required">*</b> <spring:message
          code="label_com_transretail_crm_entity_customermodel_gender" />
      </label>
      <div class="controls">
        <form:select path="customerProfile.gender" items="${gender}"
          itemLabel="description" itemValue="code" />
      </div>
    </div>
    
    

</fieldset>



<!-- Address -->
<fieldset class="container-fluid">
    <legend><spring:message code="label_com_transretail_crm_entity_customermodel_address" /></legend>
    <div class="row-fluid">
      <spring:message code="label_address_street" var="street"/>
      <fieldset class="pull-left span6">
        <label for="street">${street }</label>
        <form:input path="address.street" placeholder="${street}" cssClass="span12" />
      </fieldset>
    	<spring:message code="label_address_number" var="streetNumber"/>
    	<fieldset class="pull-left ml10">
	    	<label for="streetNumber">${streetNumber }</label>
	        <form:input path="address.streetNumber" cssClass="span10" placeholder="${streetNumber}" />
      </fieldset>
    </div>
    <div class="row-fluid">
        <spring:message code="label_address_block" var="block"/>
        <fieldset class="pull-left">
	        <label for="block">${block }</label>
        	<form:input path="address.block" placeholder="${block}" cssClass="span11" />
        </fieldset>
        <spring:message code="label_address_km" var="km"/>
        <fieldset class="pull-left">
	        <label for="km">${km }</label>
        	<form:input path="address.km" placeholder="${km}" cssClass="span11 intInput" />
        </fieldset>
        <%-- <spring:message code="label_address_rtrw" var="rtrw"/>
        <fieldset class="span3">
          <label for="rt">${rtrw }</label>
          <form:input path="address.rt" placeholder="${rtrw}" cssClass="span11" />
        </fieldset> --%>
        <spring:message code="label_address_rt" var="rt"/>
        <fieldset class="pull-left">
	        <label for="rt">${rt }</label>
        	<form:input path="address.rt" placeholder="${rt}" cssClass="span11 intInput" />
        </fieldset>
        <spring:message code="label_address_rw" var="rw"/>
        <fieldset class="pull-left">
	        <label for="rw">${rw }</label>
        	<form:input path="address.rw" placeholder="${rw}" cssClass="span11 intInput" />
        </fieldset>
    </div>
    <div class="row-fluid">
        <spring:message code="label_address_building" var="building"/>
        <fieldset class="pull-left span6">
	        <label for="building">${building }</label>
        	<form:input path="address.building" placeholder="${building}" cssClass="span12" />
        </fieldset>
        <spring:message code="label_address_floor" var="floor"/>
        <fieldset class="pull-left ml10">
	        <label for="floor">${floor }</label>
        	<form:input path="address.floor" placeholder="${floor}" cssClass="span10 intInput" />
        </fieldset>
        <spring:message code="label_address_room" var="room"/>
        <fieldset class="pull-left">
	        <label for="room">${room }</label>
        	<form:input path="address.room" placeholder="${room}" cssClass="span10" />
        </fieldset>
    </div>
    <div class="row-fluid">
        <spring:message code="label_address_postalcode" var="postCode"/>
        <fieldset class="pull-left">
          <label for="postCode"><span class="required">*</span> ${postCode }</label>
          <form:input path="address.postCode" placeholder="${postCode}" cssClass="span11" />
        </fieldset>
        <spring:message code="label_address_subdistrict" var="subdistrict"/>
        <fieldset class="pull-left">
	        <label for="subdistrict">${subdistrict }</label>
        	<form:input path="address.subdistrict" placeholder="${subdistrict}" cssClass="span11" />
        </fieldset>
        <spring:message code="label_address_district" var="district"/>
        <fieldset class="pull-left">
	        <label for="district">${district }</label>
        	<form:input path="address.district" placeholder="${district}" cssClass="span11" />
        </fieldset>
    </div>
    <div class="row-fluid provinceCityBlock">
        <spring:message code="label_address_province" var="province"/>
        <fieldset class="pull-left">
          <label for="province">${province}</label>
          <select name="address.province" class="provinceSelect">
            <option value=""></option>
            <c:forEach items="${provinces}" var="province">
            <option value="${province.name}">${province.name}</option>
            </c:forEach>
          </select>
          <%--<form:input path="address.province" placeholder="${province}" cssClass="span10" /> --%>
        </fieldset>
        <spring:message code="label_address_city" var="city"/>
        <fieldset class="pull-left ml10">
          <label for="city">${city }</label>
          <select name="address.city" class="citySelect">
          <option value=""></option>
          </select>
          <%--<form:input path="address.city" placeholder="${city}" cssClass="span11" /> --%>
        </fieldset>
    </div>
</fieldset>

<fieldset class="form-horizontal container-fluid">
    <legend></legend>
    <div class="control-group">
      <label for="religion" class="control-label"><span class="required">*</span> <spring:message
          code="label_com_transretail_crm_entity_customermodel_religion" />:
      </label>
      <div class="controls">
        <form:select path="customerProfile.religion" items="${religion}"
          itemLabel="description" itemValue="code" />
      </div>
    </div>
    
    <div class="control-group">
      <label for="maritalStatus" class="control-label"> 
        <spring:message code="label_com_transretail_crm_entity_customermodel_maritalstatus" />:
      </label> 
      <div class="controls">
        <form:select path="customerProfile.maritalStatus" items="${maritalStatus}" itemLabel="description" itemValue="code" />
      </div>
    </div>
    
    <div class="control-group">
      <label for="children" class="control-label"> 
        <spring:message code="label_com_transretail_crm_entity_customermodel_children" />:
      </label> 
      <div class="controls">
        <form:input path="customerProfile.children" id="childrenCount" cssClass="intInput input-small"/>
      </div>
    </div>
    
    <div class="control-group">
      <label for="familySize" class="control-label"> 
        <spring:message code="label_com_transretail_crm_entity_customermodel_familysize" />:
      </label> 
      <div class="controls">
        <form:select path="customerProfile.familySize" items="${familySize}" itemLabel="description" itemValue="code" />
      </div>
    </div>
    
    <div class="control-group">
      <label for="nationality" class="control-label"> <spring:message
          code="label_com_transretail_crm_entity_customermodel_nationality" />:
      </label>
      <div class="controls">
        <form:select path="customerProfile.nationality"
          items="${nationality}" itemLabel="description" itemValue="code" />
      </div>
    </div>
    
    <div class="control-group">
      <label for="education" class="control-label"> 
        <spring:message code="label_com_transretail_crm_entity_customermodel_education" />:
      </label> 
      <div class="controls">
        <form:select path="customerProfile.education" items="${education}" itemLabel="description" itemValue="code" />
      </div>
    </div>
    
    <div class="control-group">
      <spring:message code="member_prop_contact" var="contact" />
      <label for="contact" class="control-label"> <b
        class="required">*</b>
      <c:out value="${contact}" />
      </label>
      <div id="contact" class="controls">
        <input type="text" name="contact" class="form-control"
          placeholder="${contact}" />
      </div>
    </div>
    
    <div class="control-group">
      <spring:message code="member_prop_homephone" var="homePhone" />
      <label for="homePhone" class="control-label">
      <c:out value="${homePhone}" />
      </label>
      <div id="homePhone" class="controls">
        <input type="text" name="homePhone" class="form-control"
          placeholder="${homePhone}" />
      </div>
    </div>
    
    <%-- <div class="control-group">
      <spring:message
        code="label_com_transretail_crm_entity_customermodel_timetocall"
        var="bestTimeToCall" />
      <label for="bestTimeToCall" class="control-label"> <b
		  class="required">*</b> <c:out
          value="${bestTimeToCall}" />
      </label>
      <div id="bestTimeToCall" class="controls bootstrap-timepicker">
        <form:input id="bestTimeToCall" path="bestTimeToCall"
          cssClass="form-control" placeholder="${bestTimeToCall}" />
      </div>
    </div> --%>
    <div class="control-group">
      <spring:message code="label_com_transretail_crm_entity_customermodel_timetocall" var="bestTimeToCallLbl" />
      <label for="bestTimeToCall" class="control-label"> <b class="required">*</b><c:out value="${bestTimeToCallLbl}" /></label>
      <div id="bestTimeToCall" class="controls checkbox" style="margin-left: 170px;">
        <form:checkboxes items="${bestTimeToCall}" path="bestTimeToCall" itemValue="code" itemLabel="description" />
      </div>
    </div>
    
    <div class="control-group">
      <spring:message
        code="label_com_transretail_crm_entity_customermodel_email"
        var="email" />
      <label for="email" class="control-label"> <c:out
          value="${email}" />
      </label>
      <div class="controls">
        <input id="email" type="text" name="email" class="form-control email"
          placeholder="${email}" />
      </div>
    </div>

  <div class="control-group">
    <spring:message
      code="label_com_transretail_crm_entity_customermodel_position"
      var="position" />
    <label for="professionalProfile.position" class="control-label">
      <c:out value="${position}" />
    </label>
    <div class="controls">
      <!-- <form:input path="professionalProfile.position"
        cssClass="form-control" placeholder="${position}" /> --> 
      <c:forEach var="item" items="${positions}">
	      <div class="radio">
	        <input type="radio" name="professionalProfile.position" value="${item}">
	        <label><spring:message code="member_position_${item}" /></label>
	      </div>
      </c:forEach>
    	<%-- <div class="radio">
    		<input type="radio" name="professionalProfile.position" value="OWNER">
    		<label><spring:message code="member_position_owner" /></label>
    	</div>
    	<div class="radio">
    		<input type="radio" name="professionalProfile.position" value="PURCHASING">
    		<label><spring:message code="member_position_purchasing" /></label>
    	</div>
    	<div class="radio">
    		<input type="radio" name="professionalProfile.position" value="CHIEF">
    		<label><spring:message code="member_position_chief" /></label>
    	</div>
    	<div class="radio">
    		<input type="radio" name="professionalProfile.position" value="STAFF">
    		<label><spring:message code="member_position_staff" /></label>
    	</div> --%>
    </div>
  </div>
  
  <div class="control-group">
    <label for="customerProfile.interests" class="control-label">
      <spring:message
      code="label_com_transretail_crm_entity_customermodel_interest"/>
    </label>
    <div class="controls">
      <div class="row-fluid">
        <div id="interests" class="well category-group span3">
          <c:forEach items="${interests}" var="interest">
            <div class="checkbox">
              <input type="checkbox" value="${interest.code}" multiple="multiple" name="customerProfile.interests" /><label class="checkbox-label">${interest.description}</label>
            </div>
          </c:forEach>
        </div>
      </div>
    </div>
  </div>
  
</fieldset>


<div class="clearfix"></div>
