alter table crm_gc_prod_profile drop constraint FK_RIA7U0MVJQKJHSQ11AK9UJL2I;
update crm_ref_lookup_dtl set code = 'PPFV05' where code = 'PPFV5';
update crm_ref_lookup_dtl set code = 'PPFV02' where code = 'PPFV2';
update crm_ref_lookup_dtl set code = 'PPFV01' where code = 'PPFV1';
update crm_ref_lookup_dtl set code = 'PPFV09' where code = 'PPFV9';
update crm_ref_lookup_dtl set code = 'PPFV07' where code = 'PPFV7';
update crm_ref_lookup_dtl set code = 'PPFV06' where code = 'PPFV6';

update crm_gc_prod_profile 
set face_value = (
  case 
  when face_value = 'PPFV5' then 'PPFV05'
  when face_value = 'PPFV2' then 'PPFV02'
  when face_value = 'PPFV1' then 'PPFV01'
  when face_value = 'PPFV9' then 'PPFV09'
  when face_value = 'PPFV7' then 'PPFV07'
  when face_value = 'PPFV6' then 'PPFV06'
  else ''
  end
);

alter table CRM_GC_PROD_PROFILE 
        add constraint FK_ria7u0mvjqkjhsq11ak9ujl2i 
        foreign key (FACE_VALUE) 
        references CRM_REF_LOOKUP_DTL;