<%@ include file="../../common/taglibs.jsp" %>

          <div class="control-group-01 rewardContainer">
          <fieldset>
            <legend class="block-stack-title"><spring:message code="promotion_prop_rewards" /></legend>

            <div id="block_promotionRewards" class="control-group">
              <label for="rewardType" class="control-label"><spring:message code="promotion_prop_rewardtype"/></label>
              <div id="content_rewardTypes" class="controls">
                <c:url var="url_createReward" value="/promotion/reward/list/"/>
                <c:if test="${ null != promotion.rewardType }"><c:set var="disabled" value="true" /> </c:if>
                <form:select path="rewardType" id="rewardType" data-url='${url_createReward}' class="" disabled="${disabled}" >
                  <option/>
                  <form:options items="${promotionRewardTypes}" itemValue="code" itemLabel="description" />
                </form:select>

                <a href="#" class="btn btn-small" id="link_promotionReward" data-url="<c:url value="/promotion/reward/create/"/>">
                  <spring:message code="reward_label_add"/>
                </a>
              </div>

              <div id="list_promotionRewards"><c:if test="${not empty promotion.promotionRewards or null != promotion.rewardType}"><jsp:include page="../reward/rewardList.jsp" /></c:if></div>

            </div>
          </fieldset>
          </div>
        
