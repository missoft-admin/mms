  -- Execute this to oracle with no interface.OBTMDHMDH interface. Eg. database used by exist staging
  create table PROFIT_PROD_HIER (
      OMDHMDHCD varchar2(255 char) not null,
      OMDHEDESC varchar2(255 char),
      OMDHMLVNO number(10,0),
      OMDHLDESC varchar2(255 char),
      OMDHPARCD varchar2(255 char),
      OMDHCREDAT timestamp,
      primary key (OMDHMDHCD)
  );