package com.transretail.crm.rest.common;

import com.mysema.util.MathUtils;
import com.transretail.crm.common.enums.MessageType;
import com.transretail.crm.common.service.dto.rest.ProductRestDto;
import com.transretail.crm.common.service.dto.rest.ReturnMessage;
import com.transretail.crm.common.service.dto.rest.ValidProductsDto;
import com.transretail.crm.common.util.DateUtil;
import com.transretail.crm.common.util.StringUtil;
import com.transretail.crm.core.dto.*;
import com.transretail.crm.core.entity.*;
import com.transretail.crm.core.entity.embeddable.Npwp;
import com.transretail.crm.core.entity.enums.*;
import com.transretail.crm.core.entity.lookup.LookupDetail;
import com.transretail.crm.core.entity.lookup.Store;
import com.transretail.crm.core.repo.ApplicationConfigRepo;
import com.transretail.crm.core.security.model.impl.CustomSecurityUserDetailsImpl;
import com.transretail.crm.core.security.service.MD5Service;
import com.transretail.crm.core.security.util.UserUtil;
import com.transretail.crm.core.service.*;
import com.transretail.crm.core.util.generator.IdGeneratorService;
import com.transretail.crm.loyalty.entity.LoyaltyCardInventory;
import com.transretail.crm.loyalty.service.LoyaltyCardService;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.BooleanUtils;
import org.apache.commons.lang3.StringUtils;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import java.text.ParseException;
import java.util.*;


@Controller
@RequestMapping("/points")
public class PointsManagerController {

    private static final String REQUEST_MAPPING = "points";

    private static final Logger LOGGER = LoggerFactory.getLogger(PointsManagerController.class);

    @Autowired
    private EmployeeService employeeService;

    @Autowired
    private MemberService memberService;

    @Autowired
    private PointsTxnManagerService pointsManagerService;

    @Autowired
    private PromotionService promotionService;

    @Autowired
    private MessageSource messageSource;

    @Autowired
    private StoreService storeService;

    @Autowired
    private EmployeePurchaseService employeePurchaseService;

    @Autowired
    private CodePropertiesService codePropertiesService;

    @Autowired
    private UserService userService;

    @Autowired
    private ApplicationConfigRepo applicationConfigRepo;

    @Autowired
    private LoyaltyCardService loyaltyCardService;

    @Autowired
    private MD5Service mD5Service;

    @Autowired
    private StoreMemberService storeMemberService;

    @Autowired
    private IdGeneratorService customIdGeneratorService;

    @Autowired
    private TransactionAuditService transactionAuditService;

    @Autowired
    private CrmIdRefService crmIdRefService;

    @Autowired
    private RestUrlService restUrlService;

    @Autowired
    private StampService stampService;

    @Autowired
    private StampPromoService stampPromoService;

    private static DateTimeFormatter DATE_FORMAT = DateTimeFormat.forPattern("MMM d, yyyy - HH:mm");
    private static DateTimeFormatter DATE_ONLY = DateTimeFormat.forPattern("MM-dd-yy");


    /*
itemList = List of product items with qty. format : <product_id>!<qty>_<product_id>!<qty>_<product_id>!<qty>  ;
ie: 11111!1_22222!2_33333!1

MD5 Signature = MD5(transactionno + store + secretKey)

where secret key is provided by MMS to POS team
*/
    @RequestMapping("/return/{transactionNo}/{newTransactionNo}/{transactionAmount}/{store}/{itemList}/{validationKey}")
    @ResponseBody
    @Transactional
    public ReturnMessage returnItems(@PathVariable String transactionNo, @PathVariable String newTransactionNo,
                                     @PathVariable String transactionAmount,
                                     @PathVariable String store, @PathVariable String itemList,
                                     @PathVariable String validationKey, HttpServletRequest request) {


        ReturnMessage ret = new ReturnMessage();


        //call primary ws url if applicable
        StringBuffer param = new StringBuffer("/return/");
        param.append(transactionNo);
        param.append("/");
        param.append(newTransactionNo);
        param.append("/");
        param.append(transactionAmount);
        param.append("/");
        param.append(store);
        param.append("/");
        param.append(itemList);
        param.append("/");
        param.append(validationKey);
        if (restUrlService.callPrimaryWsUrl(REQUEST_MAPPING,param.toString(), false, ret, request.getLocalAddr())) {
            return ret;
        }


        LOGGER.info("[ RETURN][INIT WITH VALIDATION] transactionNo=" + transactionNo + "; newTransactionNo=" +
                newTransactionNo + "; transactionAmount=" + transactionAmount + "; store=" + store +
                "; ITEMS=" + itemList + "; CURRENT DATE=" + new Date() + "; validationKey=" + validationKey);


        ret.setCallingAction(PointTxnType.REDEEM.toString());

        StringBuffer keySignature = new StringBuffer();
        keySignature.append(transactionNo);
        keySignature.append(store);


        if (!mD5Service.validateMd5(keySignature.toString(), validationKey)) {
            LOGGER.info("[RETURN][INVALID ACCESS] transactionNo : " + transactionNo + " storeCode: " + store + " " +
                    "validationKey : " + validationKey);
            ret.setMessageCode(codePropertiesService.getDetailMessageCodeInvalidClientSignature());
            ret.setType(MessageType.ERROR);
            ret.setMessage(messageSource.getMessage("points_rs_error_invalid_client_signature", null, null));
            return ret;
        }

        return returnItems(transactionNo, newTransactionNo, transactionAmount, store, itemList, request);

    }


    /*
      itemList = List of product items with qty. format : <product_id>!<qty>_<product_id>!<qty>_<product_id>!<qty>  ;
      ie: 11111!1_22222!2_33333!1
    */
    @RequestMapping("/return/{transactionNo}/{newTransactionNo}/{transactionAmount}/{store}/{itemList}")
    @ResponseBody
    @Transactional
    public ReturnMessage returnItems(@PathVariable String transactionNo, @PathVariable String newTransactionNo,
                                     @PathVariable String transactionAmount,
                                     @PathVariable String store, @PathVariable String itemList, HttpServletRequest request) {



        ReturnMessage ret = new ReturnMessage();

        //call primary ws url if applicable
        StringBuffer param = new StringBuffer("/return/");
        param.append(transactionNo);
        param.append("/");
        param.append(newTransactionNo);
        param.append("/");
        param.append(transactionAmount);
        param.append("/");
        param.append(store);
        param.append("/");
        param.append(itemList);
        if (restUrlService.callPrimaryWsUrl(REQUEST_MAPPING,param.toString(), false, ret, request.getLocalAddr())) {
            return ret;
        }


        LOGGER.info("[RETURN][INIT] transactionNo=" + transactionNo + "; newTransactionNo=" + newTransactionNo + "; " +
                "transactionAmount=" + transactionAmount + "; store=" + store +
                "; ITEMS=" + itemList + "; CURRENT DATE=" + new Date());

        LOGGER.info("[ RETURN][INIT][URL CALL] /return/" + transactionNo + "/" + newTransactionNo + "/" +
                transactionAmount + "/" + store + "/" + itemList);


        ret.setCallingAction(PointTxnType.RETURN.toString());


        //parse list of product items with the following pattern : <productId>!<qty>_<productId>!<qty>_<productId>!<qty>
        List<PosTxItemDto> itemDtoList = new ArrayList<PosTxItemDto>();

        if (itemList != null) {
            String[] pItems = itemList.split("_");
            for (String item : pItems) {

                String[] prodItems = item.split("!");

                try {
                    PosTxItemDto itemDto = new PosTxItemDto();
                    itemDto.setProductId(prodItems[0]);
                    itemDto.setQuantity(Double.parseDouble(prodItems[1]));
                    itemDtoList.add(itemDto);
                } catch (Exception e) {
                    LOGGER.error(e.getMessage());
                }
            }
        }

        Double amount = 0.0;

        try {
            amount = Double.parseDouble(transactionAmount);

        } catch (NumberFormatException e) {
            //default to 0
        }

        if (!pointsManagerService.processReturnItems(transactionNo, newTransactionNo, amount, itemDtoList, store,
                ret, false)) {

            if (ret.getMessageCode() == null) {
                ret.setType(MessageType.ERROR);
                ret.setMessageCode(codePropertiesService.getDetailMessageCodeInternalError());
                ret.setMessage(messageSource.getMessage("points_rs_error_processing_points", null, null));
            }

        }

        LOGGER.info("[RETURN][processReturnItems] accountId=" + ret.getAccountNumber() + ";transactionNo=" +
                transactionNo + "; amount=" + amount + "; store=" + store +
                "; itemDtoList=" + itemDtoList + "; earnedPoints=" + ret.getEarnedPoints() + "; MESSAGE " + ret
                .getMessage());

        return ret;

    }



    /*
  itemList = List of product items with qty. format : <product_id>!<qty>_<product_id>!<qty>_<product_id>!<qty>  ;
  ie: 11111!1_22222!2_33333!1

  MD5 Signature = MD5(transactionno + store + secretKey)

where secret key is provided by MMS to POS team
*/
    @RequestMapping("/refund/{transactionNo}/{newTransactionNo}/{transactionAmount}/{store}/{validationKey}")
    @ResponseBody
    @Transactional
    public ReturnMessage refundItems(@PathVariable String transactionNo,
                                     @PathVariable String newTransactionNo,
                                     @PathVariable String transactionAmount,
                                     @PathVariable String store,
                                     @PathVariable String validationKey, HttpServletRequest request) {


        ReturnMessage ret = new ReturnMessage();

        //call primary ws url if applicable
        StringBuffer param = new StringBuffer("/refund/");
        param.append(transactionNo);
        param.append("/");
        param.append(newTransactionNo);
        param.append("/");
        param.append(transactionAmount);
        param.append("/");
        param.append(store);
        param.append("/");
        param.append(validationKey);

        if (restUrlService.callPrimaryWsUrl(REQUEST_MAPPING,param.toString(), false, ret, request.getLocalAddr())) {
            return ret;
        }


        LOGGER.info("[RETURN][INIT] transactionNo=" + transactionNo + " newTransactionNo=" + newTransactionNo +
                " transactionAmount=" + transactionAmount + "; store=" + store +
                "; validationKey=" + validationKey + "; CURRENT DATE=" + new Date());

        LOGGER.info("[ RETURN][INIT][URL CALL] /refund/" + transactionNo + "/" + newTransactionNo + "/" +
                transactionAmount + "/" + store + "/" + validationKey);


        StringBuffer keySignature = new StringBuffer();
        keySignature.append(transactionNo);
        keySignature.append(store);


        if (!mD5Service.validateMd5(keySignature.toString(), validationKey)) {
            LOGGER.info("[RETURN][INVALID ACCESS] transactionNo : " + transactionNo + " storeCode: " + store + " " +
                    "validationKey : " + validationKey);
            ret.setMessageCode(codePropertiesService.getDetailMessageCodeInvalidClientSignature());
            ret.setType(MessageType.ERROR);
            ret.setMessage(messageSource.getMessage("points_rs_error_invalid_client_signature", null, null));
            return ret;
        }


        ret.setCallingAction(PointTxnType.RETURN.toString());



        Double amount = 0.0;

        try {
            amount = Double.parseDouble(transactionAmount);

        } catch (NumberFormatException e) {
            //default to 0
        }

        if (!pointsManagerService.processReturnItems(transactionNo, newTransactionNo, amount, null, store,
                ret, true)) {

            if (ret.getMessageCode() == null) {
                ret.setType(MessageType.ERROR);
                ret.setMessageCode(codePropertiesService.getDetailMessageCodeInternalError());
                ret.setMessage(messageSource.getMessage("points_rs_error_processing_points", null, null));
            }

        }

        LOGGER.info("[RETURN][processRefundItems] accountId=" + ret.getAccountNumber() + ";transactionNo=" +
                transactionNo + "; amount=" + amount + "; store=" + store +
                " earnedPoints=" + ret.getEarnedPoints() + "; MESSAGE " + ret
                .getMessage());

        return ret;

    }




    /*
items = List of product items with qty. format : <product_id>!<qty>_<product_id>!<qty>_<product_id>!<qty>  ; ie:
11111!1_22222!2_33333!1

 MD5 Signature = MD5(ID + store + transactionno + secretKey)

 where secret key is provided by MMS to POS team
*/
    @RequestMapping("/earnpoints/{id}/{store}/{paymentType}/{amount}/{cardNumber}/{transactionNo}/{transactionDate" +
            "}/{items}/{validationKey}")
    @ResponseBody
    @Transactional
    public ReturnMessage earnPoints(@PathVariable String id, @PathVariable String store,
                                    @PathVariable String paymentType,
                                    @PathVariable String amount, @PathVariable String cardNumber,
                                    @PathVariable String transactionNo,
                                    @PathVariable String transactionDate, @PathVariable String items,
                                    @PathVariable String validationKey, HttpServletRequest request) {

        ReturnMessage ret = new ReturnMessage();
        //call primary ws url if applicable
        StringBuffer param = new StringBuffer("/earnpoints/");
        param.append(id);
        param.append("/");
        param.append(store);
        param.append("/");
        param.append(paymentType);
        param.append("/");
        param.append(amount);
        param.append("/");
        param.append(cardNumber);
        param.append("/");
        param.append(transactionNo);
        param.append("/");
        param.append(transactionDate);
        param.append("/");
        param.append(items);
        param.append("/");
        param.append(validationKey);
        if (restUrlService.callPrimaryWsUrl(REQUEST_MAPPING,param.toString(), false, ret, request.getLocalAddr())) {
            return ret;
        }

        LOGGER.info("[ EARN][INIT WITH VALIDATION] ID=" + id + "; STORE=" + store + "; PAYMENTTTYPE=" + paymentType +
                ";AMOUNT=" +
                amount + ";CARDNO=" + cardNumber + ";TXNNO=" + transactionNo + ";TXNDATE=" + transactionDate + ";" +
                "ITEMS=" + items + " CURRENT DATE=" + new Date() + "; Validation key=" + validationKey);

        ret.setCallingAction(PointTxnType.EARN.toString());
        ret.setValidPurchaseTxnAmount(0.0);
        ret.setEarnedPoints(0D);
        ret.setEarnedStamps(0D);

        StringBuffer msgCode = new StringBuffer();
        if (!storeMemberService.isValidMember(store, id, msgCode)) {
            ret.setType(MessageType.ERROR);
            ret.setMessageCode(msgCode.toString());
            ret.setMessage(messageSource.getMessage("points_rs_error_invalid_storemember", new Object[]{id}, null));
            return ret;
        }


        StringBuffer keySignature = new StringBuffer();
        keySignature.append(id);
        keySignature.append(store);
        keySignature.append(transactionNo);
        if (!mD5Service.validateMd5(keySignature.toString(), validationKey)) {
            LOGGER.info("[EARN][INVALID ACCESS] account ID : " + id + " store: " + store + " txnNo :" + transactionNo
                    + " validationKey : " + validationKey);
            ret.setMessageCode(codePropertiesService.getDetailMessageCodeInvalidClientSignature());
            ret.setType(MessageType.ERROR);
            ret.setMessage(messageSource.getMessage("points_rs_error_invalid_client_signature", null, null));
            return ret;
        }

        return earnPoints(id, store, paymentType, amount, cardNumber, transactionNo, transactionDate, items, request);
    }


    //renamed url in case POSS is not updated before this is deployed
    /*
     items = List of product items with qty. format : <product_id>!<qty>_<product_id>!<qty>_<product_id>!<qty>  ; ie:
      11111!1_22222!2_33333!1
     */
    @RequestMapping("/earnpoints/{id}/{store}/{paymentType}/{amount}/{cardNumber}/{transactionNo}/{transactionDate}/{items}")
    @ResponseBody
    @Transactional
    public ReturnMessage earnPoints(@PathVariable String id, @PathVariable String store,
                                    @PathVariable String paymentType,
                                    @PathVariable String amount, @PathVariable String cardNumber,
                                    @PathVariable String transactionNo,
                                    @PathVariable String transactionDate, @PathVariable String items, HttpServletRequest request) {

        ReturnMessage ret = new ReturnMessage();

        //call primary ws url if applicable
        StringBuffer param = new StringBuffer("/earnpoints/");
        param.append(id);
        param.append("/");
        param.append(store);
        param.append("/");
        param.append(paymentType);
        param.append("/");
        param.append(amount);
        param.append("/");
        param.append(cardNumber);
        param.append("/");
        param.append(transactionNo);
        param.append("/");
        param.append(transactionDate);
        param.append("/");
        param.append(items);

        if (restUrlService.callPrimaryWsUrl(REQUEST_MAPPING,param.toString(), false, ret, request.getLocalAddr())) {
            return ret;
        }

        //trim last digit  for employee id - 12 digits if retrieve by POSS from barcode
        id = StringUtil.trimEmpId(id);

        LOGGER.info("[EARN][INIT] ID=" + id + "; STORE=" + store + "; PAYMENTTTYPE=" + paymentType + ";AMOUNT=" +
                amount + ";CARDNO=" + cardNumber + ";TXNNO=" + transactionNo + ";TXNDATE=" + transactionDate + ";" +
                "ITEMS=" + items + " CURRENT DATE=" + new Date());

        LOGGER.info("[EARN][INIT][URL CALL] /earnpoints/" + id + "/" + store + "/" + paymentType + "/" + amount + "/"
                + cardNumber + "/" + transactionNo + "/" + transactionDate + "/" + items);

        String memberNotFoundCode = codePropertiesService.getDetailMessageCodeMemberNotFound();

        ret.setCallingAction(PointTxnType.EARN.toString());
        ret.setValidPurchaseTxnAmount(0.0);
        ret.setEarnedPoints(0D);

        MemberModel member = memberService.findActiveWithAccountId(id);
        if (null == member) {
            ret.setMessageCode(memberNotFoundCode);
            ret.setType(MessageType.ERROR);
            ret.setMessage(messageSource.getMessage("points_rs_error_no_member", new Object[]{id}, null));
            return ret;
        }

        LoyaltyCardInventory loyaltyCard = loyaltyCardService.findLoyaltyCard(member.getLoyaltyCardNo());
        if (loyaltyCard != null && loyaltyCard.getExpiryDate() != null) {
            if (loyaltyCardService.isCardExpired(loyaltyCard)) {
                LOGGER.info("[EARN][CARD EXPIRED]" + " ID=" + member.getAccountId() + " CARD NO=" + member
                        .getLoyaltyCardNo());

                //record zero points txn
                PointsTxnModel pointsTxnModel = new PointsTxnModel();
                pointsTxnModel.setTransactionPoints(0.0);
                pointsTxnModel.setMemberModel(member);
                //pointsTxnModel.setMemberAccountId(member.getAccountId());  //need soft reference to member account
                // id as requested by iman
                pointsTxnModel.setTransactionNo(handleNull(transactionNo));
                pointsTxnModel.setStoreCode(handleNull(store));
                Date transDate = null;
                try {
                    transDate = DateUtil.convertDateString(DateUtil.REST_DATE_FORMAT, transactionDate);
                    pointsTxnModel.setTransactionDateTime(transDate);
                } catch (ParseException e) {
                    LOGGER.error("Error Parsing date parameter {} for transaction no {} :: {}  ", transactionDate,
                            transactionNo,
                            e.getMessage());
                }
                pointsTxnModel.setTransactionDateTime(transDate);
                List<String> allAmounts = Arrays.asList(StringUtils.split(amount, ":"));
                Double totalAmount = 0.0;
                for (String amt : allAmounts) {
                    try {
                        totalAmount += Double.parseDouble(amt);
                    } catch (NumberFormatException e) {
                        //ignore
                    }
                }
                pointsTxnModel.setTransactionAmount(totalAmount);
                pointsTxnModel.setTransactionType(PointTxnType.EARN);
                pointsManagerService.savePointsTxnModel(pointsTxnModel);

                ret.setMessageCode(codePropertiesService.getDetailMessageCodeLoyaltyCardExpired());
                ret.setType(MessageType.ERROR);
                ret.setMessage(messageSource.getMessage("points_rs_error_loyaltycard_expired",
                        new Object[]{member.getLoyaltyCardNo()}, null));
                return ret;
            }
        }

        String employeeTypeCode = codePropertiesService.getDetailMemberTypeEmployee();
        String memberTypeCode = member.getMemberType() != null ? member.getMemberType().getCode() : null;

        LOGGER.info("[EARN][MEMBERTYPECODE]" + " TXNNO=" + transactionNo + " MEMBER TYPE CODE=" + memberTypeCode);

        boolean isEmployee = null != memberTypeCode && employeeTypeCode.equalsIgnoreCase(memberTypeCode);

        //if no config definition is available
        ApplicationConfig config = applicationConfigRepo.findByKey(AppKey.ALLOW_NO_LOYALTYCARD_TO_EARN);
        if (config == null || StringUtils.isBlank(config.getValue()) || config.getValue().equals("0") || config
                .getValue().equalsIgnoreCase("false")) {
            //individuals / professionals with no loyaltcardno assigned will no earn points
            // addtitional condition not stamp user
            if (!isEmployee && StringUtils.isBlank(member.getLoyaltyCardNo()) && BooleanUtils.isNotTrue(member.getStampUser())) {
                LOGGER.info("[EARN][NO LOYALTY CARD] accountId=" + id);
                ret.setMessageCode(codePropertiesService.getDetailMessageCodeNoLoyaltyCard());
                ret.setType(MessageType.ERROR);
                ret.setMessage(messageSource.getMessage("points_rs_error_no_loyaltycard", new Object[]{id}, null));
                return ret;
            }
        }

        populateReturnMessageWithMemberAttr(member, ret);

        if (cardNumber.equalsIgnoreCase("null")) {
            cardNumber = null;
        }

        //check if employee
        if (isEmployee) {

            if (employeePurchaseService.findOneByTransactionNo(transactionNo) != null) {
                ret.setCallingAction(EmployeeTxnType.PURCHASE.toString());
                ret.setType(MessageType.ERROR);
                ret.setMessage(messageSource.getMessage("points_msg_duplicatetxnno", new Object[]{id}, null));
                return ret;
            }

            List<Map> payments = pointsManagerService.retrieveValidPaymentTypes(paymentType, amount, cardNumber,
                    true, ret);

            LOGGER.info("[EARN][retrieveValidPaymentTypes] PAYMENTTTYPE=" + paymentType + ";AMOUNT=" +
                    amount + ";CARDNO=" + cardNumber + ";checkFilters=true" + "PAYMENTS SIZE=" + payments.size());

            Double totalValidPurchaseAmt = 0.0;
            for (Map payment : payments) {

                String pType = (String) payment.get(PointsTxnManagerService.PAYMENT_TYPE_KEY);
                Double amt = (Double) payment.get(PointsTxnManagerService.AMOUNT_KEY);
                totalValidPurchaseAmt += amt;
                ret.setValidPurchaseTxnAmount(totalValidPurchaseAmt);

                LOGGER.info("[EARN][retrieveValidPaymentTypes][processEmployeePurchase] member=" + member + ";" +
                        "transactionDate=" +
                        transactionDate + ";transactionDate=" + transactionDate + ";transactionNo=" + transactionNo +
                        ";store=" + store + ";amt=" + amt);
                processEmployeePurchase(member, transactionDate, transactionNo, store, amt);

            }

            ret.setValidPurchaseTxnAmount(totalValidPurchaseAmt);
            ret.setCallingAction(EmployeeTxnType.PURCHASE.toString());
            ret.setMessage(messageSource.getMessage("emp_txn_rs_success_message", new Object[]{totalValidPurchaseAmt,
                    member.getAccountId()}, LocaleContextHolder.getLocale()));
            ret.setType(MessageType.SUCCESS);


        }


        //parse list of product items with the following pattern : <productId>!<qty>_<productId>!<qty>_<productId>!<qty>
        List<PosTxItemDto> itemList = new ArrayList<PosTxItemDto>();

        if (items != null) {
            String[] pItems = items.split("_");
            for (String item : pItems) {

                String[] prodItems = item.split("!");

                try {
                    PosTxItemDto itemDto = new PosTxItemDto();
                    itemDto.setProductId(prodItems[0]);
                    if (prodItems.length > 1 && StringUtils.isNotBlank(prodItems[1]))
                        itemDto.setQuantity(Double.parseDouble(prodItems[1]));
                    else
                        itemDto.setQuantity(1d);
                    itemList.add(itemDto);
                } catch (Exception e) {
                    LOGGER.error(e.getMessage());
                }
            }
        }


        //call member process even for employee in case there are promotions that also includes employee type; this
        // will also populate returnmessage with result data
        promotionService.processPromotions(member, paymentType, store, transactionNo,
                amount, cardNumber, transactionDate, itemList, ret);

        crmIdRefService.saveEarningMember(member.getId());

        LOGGER.info("[EARN][processPromotions] member=" + member + "; STORE=" + store + "; PAYMENTTTYPE=" +
                paymentType + ";AMOUNT=" +
                amount + ";CARDNO=" + cardNumber + ";TXNNO=" + transactionNo + ";TXNDATE=" + transactionDate + ";" +
                "ITEMS=" + items + ";EARNED POINTS=" + ret.getEarnedPoints());


        if (null == ret.getMessageCode() || ret.getMessage().trim().equals("")) {
            ret.setType(MessageType.SUCCESS);
            populateReturnMessageWithMemberAttr(member, ret);
            ret.setMessage(messageSource.getMessage("points_rs_success_earn_message", new Object[]{ret.getEarnedPoints(), id}, null));
            if (BooleanUtils.isTrue(member.getStampUser())) {
                ret.setMessage(ret.getMessage() + " " + messageSource.getMessage("points_rs_success_earn_stamp_message",
                        new Object[]{ret.getEarnedStamps(), id}, null));
            }
        }

//        Integer transactionCount = (isEmployee) ? memberMonitorService.getTransactionCountEmployee() :
//                memberMonitorService.getTransactionCountIndividual();
//        if (transactionCount == null) {
//            transactionCount = Integer.MAX_VALUE;
//        }
//        Long transactionsToday = memberMonitorService.countTransactionsToday(id, memberTypeCode);
//        if (transactionsToday == null) {
//            transactionsToday = 0L;
//        }
//        if (transactionsToday.intValue() == transactionCount) {
//            // monitorMember(ret, isEmployee);
//        }

        // Allan: Please do not remove this. This is an indicator that earn proceeded without exceptions.
        LOGGER.info("[EARN][RESULT] TXNNO=" + transactionNo + ";STATUS=" + ret.getType());
        return ret;


    }

//    public void monitorMember(ReturnMessage retMsg, boolean isEmployee) {
//        LOGGER.debug("NOTIFICATION: MEMBER MONITOR");
//
//        String memberType = retMsg.getMemberType();
//        String accountId = retMsg.getAccountNumber();
//        String channel = null;
//        List<UserModel> users = null;
//
//        if (isEmployee) {
//            channel = "/notification/notify/monitor/hr";
//            users = userService.findUsersByRoleCode(codePropertiesService.getHrCode());
////    		users.addAll(userService.findUsersByRoleCode(codePropertiesService.getHrsCode()));
//            for (UserModel user : userService.findUsersByRoleCode(codePropertiesService.getHrsCode())) {
//                if (!users.contains(user)) {
//                    users.add(user);
//                }
//            }
//        } else {
//            channel = "/notification/notify/monitor/cs";
//            users = userService.findUsersByRoleCode(codePropertiesService.getCsCode());
////    		users.addAll(userService.findUsersByRoleCode(codePropertiesService.getCssCode()));
//            for (UserModel user : userService.findUsersByRoleCode(codePropertiesService.getCssCode())) {
//                if (!users.contains(user)) {
//                    users.add(user);
//                }
//            }
//        }
//
//        Notification notification = new Notification();
//        notification.setItemId(accountId);
//        notification.setType(NotificationType.MEMBER);
//
//        notification = notificationService.saveNotification(notification, users);
//
//        if (notification != null) {
//            Map<String, Object> output = Maps.newHashMap();
//            output.put("message", messageSource.getMessage("label_notification_member", null,
//                    LocaleContextHolder.getLocale()));
//            output.put("url", "/member/monitor/" + accountId);
//            output.put("time", notification.getCreated().toString(DATE_FORMAT));
//
//            notifierService.sendToChannel(channel, output);
//        }
//    }


    // old call for earning points. remove this once POSS deploys update to include items in call
    @RequestMapping("/earn/{id}/{store}/{paymentType}/{amount}/{cardNumber}/{transactionNo}/{transactionDate}")
    @ResponseBody
    @Transactional
    @Deprecated
    public ReturnMessage earnPointsOld(@PathVariable String id, @PathVariable String store,
                                       @PathVariable String paymentType,
                                       @PathVariable String amount, @PathVariable String cardNumber,
                                       @PathVariable String transactionNo,
                                       @PathVariable String transactionDate, HttpServletRequest request) {

        return earnPoints(id, store, paymentType, amount, cardNumber, transactionNo, transactionDate, null, request);


    }


    /*
 MD5 Signature = MD5(transactionno + secretKey)

 where secret key is provided by MMS to POS team
 */
    @RequestMapping("/void/{transactionNo}/{transactionDate}/{validationKey}")
    @ResponseBody
    @Transactional
    public ReturnMessage voidTransactions(@PathVariable String transactionNo,
                                          @PathVariable String transactionDate, @PathVariable String validationKey, HttpServletRequest request) {


        ReturnMessage ret = new ReturnMessage();

        //call primary ws url if applicable
        StringBuffer param = new StringBuffer("/void/");
        param.append(transactionNo);
        param.append("/");
        param.append(transactionDate);
        param.append("/");
        param.append(validationKey);
        if (restUrlService.callPrimaryWsUrl(REQUEST_MAPPING,param.toString(), false, ret, request.getLocalAddr())) {
            return ret;
        }

        ret.setCallingAction(PointTxnType.VOID.toString());


        if (!mD5Service.validateMd5(transactionNo, validationKey)) {
            LOGGER.info("[VOID][INVALID ACCESS] transactionNo : " + transactionNo + " transactionNo: " +
                    transactionNo + " validationKey : " + validationKey);

            ret.setMessageCode(codePropertiesService.getDetailMessageCodeInvalidClientSignature());
            ret.setType(MessageType.ERROR);
            ret.setMessage(messageSource.getMessage("points_rs_error_invalid_client_signature", null, null));
            return ret;
        }


        return voidTransactions(transactionNo, transactionDate, request);
    }


    @RequestMapping("/void/{transactionNo}/{transactionDate}")
    @ResponseBody
    @Transactional
    public ReturnMessage voidTransactions(@PathVariable String transactionNo,
                                          @PathVariable String transactionDate, HttpServletRequest request) {

        ReturnMessage returnMessage = new ReturnMessage();

        //call primary ws url if applicable
        StringBuffer param = new StringBuffer("/void/");
        param.append(transactionNo);
        param.append("/");
        param.append(transactionDate);

        if (restUrlService.callPrimaryWsUrl(REQUEST_MAPPING,param.toString(), false, returnMessage, request.getLocalAddr())) {
            return returnMessage;
        }

        LOGGER.info("[VOID TRANSACTION] transactionNo=" + transactionNo + ";transactionDate=" + transactionDate + " " +
                "CURRENT DATE=" + new Date());


        LOGGER.info("[VOID TRANSACTION][URL CALL]/void/" + transactionNo + "/" + transactionDate);


        String internalErrorCode = codePropertiesService.getDetailMessageCodeInternalError();

        returnMessage.setCallingAction(PointTxnType.VOID.toString());


        MemberModel memberModel = voidMemberPoints(transactionNo, transactionDate, returnMessage);
        if (memberModel != null) {
            //transaction was done by a member
            populateReturnMessageWithMemberAttr(memberModel, returnMessage);
        }

        //also check if there is a employee transaction. This is for cases where promotion also applies to employee
        //where employee also has a purchase txn record
        EmployeePurchaseTxnDto employeePurchaseTxnDto = employeePurchaseService.findOneByTransactionNo
                (transactionNo);

        if (employeePurchaseTxnDto != null) {
            MemberDto memberDto = employeePurchaseTxnDto.getMemberModel();

            populateReturnMessageWithMemberAttr(memberDto, returnMessage);

            voidEmployeeVoidPurchase(transactionNo, transactionDate, returnMessage);
        }

        List<StampTxnModel> stampTxns = stampService.retrieveStampsByTransactionNo(transactionNo);
        if (CollectionUtils.isNotEmpty(stampTxns)) {
            memberModel = voidMemberStamps(stampTxns, transactionNo, transactionDate, returnMessage);
        } else {
            returnMessage.setMessageCode(codePropertiesService.getDetailMessageCodeInternalError());
            returnMessage.setMessage(messageSource.getMessage("points_rs_error_no_stamps_earned_transaction", new Object[]{transactionNo}, null));
        }

        if (null == returnMessage.getMessageCode()  || returnMessage.getMessage().trim().equals("")) {
            returnMessage.setType(MessageType.SUCCESS);
            returnMessage.setMessage(messageSource.getMessage("points_rs_success_void_message",
                    new Object[]{returnMessage.getEarnedPoints(), transactionNo}, null));
            if (memberModel != null && BooleanUtils.isTrue(memberModel.getStampUser())) {
                returnMessage.setMessage(returnMessage.getMessage() + " " +
                        messageSource.getMessage("points_rs_success_void_stamp_message", new Object[]{returnMessage.getEarnedStamps(), transactionNo}, null));
            }
        } else {
            returnMessage.setType(MessageType.ERROR);
            if (StringUtils.isBlank(returnMessage.getMessage())) {
                returnMessage.setMessage(messageSource.getMessage("points_rs_error_processing_points", null, null));
            }
            if (memberModel != null && BooleanUtils.isTrue(memberModel.getStampUser())) {
                StringBuilder errorMessage = new StringBuilder(returnMessage.getMessage())
                        .append(" ").append(messageSource.getMessage("points_rs_error_processing_stamps", null, null));
                if (CollectionUtils.isEmpty(stampTxns)) {
                    errorMessage.append(messageSource.getMessage("points_rs_error_no_stamps_earned_transaction", new Object[]{transactionNo}, null));
                }
                returnMessage.setMessage(errorMessage.toString());
            } else if (memberModel != null && BooleanUtils.isNotTrue(memberModel.getStampUser())) {
                StringBuilder errorMessage = new StringBuilder(returnMessage.getMessage())
                        .append(" ").append(messageSource.getMessage("points_rs_error_processing_stamps", null, null));
                errorMessage.append(messageSource.getMessage("points_rs_void_non_stamps_user", null, null));
                returnMessage.setMessage(errorMessage.toString());
            }
            returnMessage.setMessageCode(internalErrorCode);
        }
        return returnMessage;
    }


    @RequestMapping("/voidpoints/{id}/{store}/{amount}/{transactionNo}/{transactionDate}")
    @ResponseBody
    @Transactional
    @Deprecated
    public ReturnMessage voidPoints(@PathVariable String id, @PathVariable String store,
                                    @PathVariable Double amount, @PathVariable String transactionNo,
                                    @PathVariable String transactionDate) {

        //trim last digit  for employee id - 12 digits if retrieve by POSS from barcode
        id = StringUtil.trimEmpId(id);

        String memberNotFoundCode = codePropertiesService.getDetailMessageCodeMemberNotFound();
        String internalErrorCode = codePropertiesService.getDetailMessageCodeInternalError();

        String employeeTypeCode = codePropertiesService.getDetailMemberTypeEmployee();

        ReturnMessage ret = new ReturnMessage();
        ret.setCallingAction(PointTxnType.VOID.toString());


        MemberModel member = memberService.findActiveWithAccountId(id);
        if (null == member) {

            ret.setMessageCode(memberNotFoundCode);
            ret.setType(MessageType.ERROR);
            ret.setMessage(messageSource.getMessage("points_rs_error_no_member", new Object[]{id}, null));
            return ret;
        }

        populateReturnMessageWithMemberAttr(member, ret);

        //check if employee
        String memberTypeCode = member.getMemberType() != null ? member.getMemberType().getCode() : null;
        if (null != memberTypeCode && employeeTypeCode.equalsIgnoreCase(memberTypeCode)) {
            voidEmployeeVoidPurchase(transactionNo, transactionDate, ret);
        } else {
            voidMemberPoints(transactionNo, transactionDate, ret);
        }

        // process void stamps after
        List<StampTxnModel> stampTxns = stampService.retrieveStampsByTransactionNo(transactionNo);
        if (CollectionUtils.isNotEmpty(stampTxns)) {
            member = voidMemberStamps(stampTxns, transactionNo, transactionDate, ret);
        } else {
            ret.setMessageCode(codePropertiesService.getDetailMessageCodeInternalError());
            ret.setMessage(messageSource.getMessage("points_rs_error_no_stamps_earned_transaction", new Object[]{transactionNo}, null));
        }

        if (null == ret.getMessageCode()  || ret.getMessage().trim().equals("")) {
            ret.setType(MessageType.SUCCESS);
            ret.setMessage(messageSource.getMessage("points_rs_success_void_message",
                    new Object[]{ret.getEarnedPoints(), transactionNo}, null));
            if (member != null && BooleanUtils.isTrue(member.getStampUser())) {
                ret.setMessage(ret.getMessage() + " " +
                        messageSource.getMessage("points_rs_success_void_stamp_message", new Object[]{ret.getEarnedStamps(), transactionNo}, null));
            }
        } else {
            ret.setType(MessageType.ERROR);
            if (StringUtils.isBlank(ret.getMessage())) {
                ret.setMessage(messageSource.getMessage("points_rs_error_processing_points", null, null));
            }
            if (member != null && BooleanUtils.isTrue(member.getStampUser())) {
                StringBuilder errorMessage = new StringBuilder(ret.getMessage())
                        .append(" ").append(messageSource.getMessage("points_rs_error_processing_stamps", null, null));
                if (CollectionUtils.isEmpty(stampTxns)) {
                    errorMessage.append(messageSource.getMessage("points_rs_error_no_stamps_earned_transaction", new Object[]{transactionNo}, null));
                }
                ret.setMessage(errorMessage.toString());
            } else if (member != null && BooleanUtils.isNotTrue(member.getStampUser())) {
                StringBuilder errorMessage = new StringBuilder(ret.getMessage())
                        .append(" ").append(messageSource.getMessage("points_rs_error_processing_stamps", null, null));
                errorMessage.append(messageSource.getMessage("points_rs_void_non_stamps_user", null, null));
                ret.setMessage(errorMessage.toString());
            }
            ret.setMessageCode(internalErrorCode);
        }
        return ret;
    }


    @RequestMapping("/validitems/{store}")
    @ResponseBody
    @Transactional
    public ValidProductsDto getValidItems(@PathVariable String store) {

        LOGGER.info("[ CHECK VALID ITEMS][redeemPoints] " + ";store=" + store);


        return promotionService.getValidItems(null, store);
    }


    @RequestMapping("/validitems/{id}/{store}")
    @ResponseBody
    @Transactional
    public ValidProductsDto getValidItems(@PathVariable String id, @PathVariable String store) {

        LOGGER.info("[ CHECK VALID ITEMS][redeemPoints] id=" + id + ";store=" + store);


        return promotionService.getValidItems(id, store);
    }


    /*
MD5 Signature = MD5(id + storecode + transactionno + secretKey)

where secret key is provided by MMS to POS team


     items = List of product items with qty. format : <product_pluid>!<product_pluid>!<product_pluid>


     this service will assume that all items have been validated via getValidItems service
     meaning all items are valid items based on promotion and member points availability
*/
    @RequestMapping("/redeem/items/{id}/{storeCode}/{transactionNo}/{transactionDate}/{items}/{validationKey}")
    @ResponseBody
    @Transactional
    public ReturnMessage redeemItems(@PathVariable String id, @PathVariable String storeCode,
                                     @PathVariable String transactionNo,
                                     @PathVariable String transactionDate,
                                     @PathVariable String items, @PathVariable String validationKey, HttpServletRequest request) {


        ReturnMessage ret = new ReturnMessage();

        //call primary ws url if applicable
        StringBuffer param = new StringBuffer("/redeem/items/");
        param.append(id);
        param.append("/");
        param.append(storeCode);
        param.append("/");
        param.append(transactionNo);
        param.append("/");
        param.append(transactionDate);
        param.append("/");
        param.append(items);
        param.append("/");
        param.append(validationKey);

        if (restUrlService.callPrimaryWsUrl(REQUEST_MAPPING,param.toString(), false, ret, request.getLocalAddr())) {
            return ret;
        }

        LOGGER.info("[ REDEEM ITEMS][redeemPoints] transactionNo=" + transactionNo + ";transactionDate=" +
                transactionDate + ";accountId=" + id + "; storecode=" + storeCode + "; amount=" + transactionNo + " " +
                "items=" + items + " validationkey=" + validationKey);
        LOGGER.info("[ REDEEM ITEMS][URL CALL] /redeem/items/" + id + "/" + storeCode + "/" + transactionNo + "/" +
                transactionDate + "/" + items + "/" + validationKey);

        ret.setCallingAction(PointTxnType.REDEEM.toString());

        StringBuffer keySignature = new StringBuffer();
        keySignature.append(id);
        keySignature.append(storeCode);
        keySignature.append(transactionNo);


        if (!mD5Service.validateMd5(keySignature.toString(), validationKey)) {
            LOGGER.info("[REDEEM][INVALID ACCESS] id : " + id + " storeCode: " + storeCode + " transactionCode : " +
                    transactionNo + " validationKey : " + validationKey);

            ret.setMessageCode(codePropertiesService.getDetailMessageCodeInvalidClientSignature());
            ret.setType(MessageType.ERROR);
            ret.setMessage(messageSource.getMessage("points_rs_error_invalid_client_signature", null, null));
            return ret;
        }

        if (items != null) {
            String[] pItems = items.split("!");
            MemberModel memberModel = memberService.findActiveWithAccountId(id);


            if (memberModel == null) {
                ret.setType(MessageType.ERROR);
                ret.setMessageCode(codePropertiesService.getDetailMessageCodeMemberNotFound());
                ret.setMessage(messageSource.getMessage("points_rs_error_no_member", new Object[]{id}, null));
                return ret;
            }


            for (String item : pItems) {

                //if item is loyalty card, redeem can still process even if card is expired
                ApplicationConfig renewalCode = applicationConfigRepo.findByKey(AppKey.LOYALTY_CARD_RENEWAL_CODE);

                ApplicationConfig replacementCode = applicationConfigRepo.findByKey(AppKey.LOYALTY_CARD_REPLACEMENT_CODE);


                if ((renewalCode != null && item.equalsIgnoreCase(renewalCode.getValue())) || (replacementCode !=
                        null && item.equalsIgnoreCase(replacementCode.getValue()))) {
                   //skip if  item is either a replacement card or renewal card

                } else {
                    LoyaltyCardInventory loyaltyCard = loyaltyCardService.findLoyaltyCard(memberModel
                            .getLoyaltyCardNo());
                    if (loyaltyCard != null && loyaltyCard.getExpiryDate() != null) {
                        if (loyaltyCardService.isCardExpired(loyaltyCard)) {
                            ret.setMessageCode(codePropertiesService.getDetailMessageCodeLoyaltyCardExpired());
                            ret.setType(MessageType.ERROR);
                            ret.setMessage(messageSource.getMessage("points_rs_error_loyaltycard_expired",
                                    new Object[]{memberModel.getLoyaltyCardNo()}, null));
                            return ret;
                        }
                    }


                }


                ProductRestDto productRestDto = promotionService.getProductPromo(item, id, storeCode);

                Date transDate = null;

                try {

                    transDate = DateUtil.convertDateString(DateUtil.REST_DATE_FORMAT, transactionDate);
                } catch (ParseException e) {

                    LOGGER.error("Error Parsing date parameter {} for transaction no {} :: {}  ", transactionDate,
                            transactionNo,
                            e.getMessage());
                }

                if (productRestDto != null && DateUtil.betweenDates(transDate, productRestDto.getPromotionStartDate(),
                        productRestDto.getPromotionEndDate())) {

                    //create redeem record for each items
                    ret = redeemPoints(id, storeCode, productRestDto.getPoints(), transactionNo, transactionDate, true, request);


                    if (!ret.getType().equals(MessageType.ERROR)) {
                        //todo auditTransactionRewards - how do we audit points?

                        TransactionAudit audit = new TransactionAudit();

                        audit.setStore(storeService.getStoreByCode(storeCode));
                        audit.setTransactionNo(transactionNo);
                        audit.setTransactionPoints(productRestDto.getPoints() != null ? -productRestDto.getPoints()
                                .longValue() : null);
                        audit.setMemberModel(memberModel);
                        if (productRestDto.getPromotionId() != null) {
                            audit.setPromotion(promotionService.getPromotion(productRestDto.getPromotionId()));
                        }
                        audit.setProductId(productRestDto.getPluId());
                        audit.setId(customIdGeneratorService.generateId(storeCode));
                        audit.setCreated(new DateTime());


                        transactionAuditService.saveTransactionAudit(audit);
                    }

                } else {

                    ret.setType(MessageType.ERROR);
                    ret.setMessageCode(codePropertiesService.getDetailMessageCodeInternalError());

                    Date startDate =  null;
                    Date endDate = null;

                    if (productRestDto != null) {
                        startDate = productRestDto.getPromotionStartDate();
                        endDate = productRestDto.getPromotionEndDate();
                    }


                    ret.setMessage(messageSource.getMessage("points_rs_error_invalid_promotion_date",
                            new Object[]{transactionDate, startDate, endDate}, null));

                    LOGGER.error("[REDEEM ITEMS][ERROR] transactionNo= {} transDate= {} startdate= {} endDate {}" + transactionNo,transactionDate,
                            startDate, endDate);
                }


            }

        }


        return ret;
    }


    /*
MD5 Signature = MD5(id + storecode + transactionno + secretKey)

where secret key is provided by MMS to POS team
*/
    @RequestMapping("/redeem/payment/{id}/{storeCode}/{amount}/{transactionNo}/{transactionDate}/{validationKey}")
    @ResponseBody
    @Transactional
    public ReturnMessage redeemPoints(@PathVariable String id, @PathVariable String storeCode,
                                      @PathVariable Double amount,
                                      @PathVariable String transactionNo,
                                      @PathVariable String transactionDate, @PathVariable String validationKey, HttpServletRequest request) {

        ReturnMessage ret = new ReturnMessage();

        //call primary ws url if applicable
        StringBuffer param = new StringBuffer("/redeem/payment/");
        param.append(id);
        param.append("/");
        param.append(storeCode);
        param.append("/");
        param.append(amount);
        param.append("/");
        param.append(transactionNo);
        param.append("/");
        param.append(transactionDate);
        param.append("/");
        param.append(validationKey);

        if (restUrlService.callPrimaryWsUrl(REQUEST_MAPPING,param.toString(), false, ret, request.getLocalAddr())) {
            return ret;
        }

        LOGGER.info("[ REDEEM POINTS][redeemPoints] transactionNo=" + transactionNo + ";transactionDate=" +
                transactionDate + ";accountId=" + id + "; storecode=" + storeCode + "; amount=" + amount + "; " +
                "validationKey=" + validationKey);

        ret.setCallingAction(PointTxnType.REDEEM.toString());

        StringBuffer keySignature = new StringBuffer();
        keySignature.append(id);
        keySignature.append(storeCode);
        keySignature.append(transactionNo);


        if (!mD5Service.validateMd5(keySignature.toString(), validationKey)) {
            LOGGER.info("[REDEEM][INVALID ACCESS] id : " + id + " storeCode: " + storeCode + " transactionCode : " +
                    transactionNo + " validationKey : " + validationKey);

            ret.setMessageCode(codePropertiesService.getDetailMessageCodeInvalidClientSignature());
            ret.setType(MessageType.ERROR);
            ret.setMessage(messageSource.getMessage("points_rs_error_invalid_client_signature", null, null));
            return ret;
        }


        return redeemPoints(id, storeCode, amount, transactionNo, transactionDate, request);
    }


    @RequestMapping("/redeem/payment/{id}/{storeCode}/{amount}/{transactionNo}/{transactionDate}")
    @ResponseBody
    @Transactional
    public ReturnMessage redeemPoints(@PathVariable String id, @PathVariable String storeCode,
                                      @PathVariable Double amount,
                                      @PathVariable String transactionNo,
                                      @PathVariable String transactionDate, HttpServletRequest request) {

        ReturnMessage returnMessage = new ReturnMessage();

        //call primary ws url if applicable
        StringBuffer param = new StringBuffer("/redeem/payment/");
        param.append(id);
        param.append("/");
        param.append(storeCode);
        param.append("/");
        param.append(amount);
        param.append("/");
        param.append(transactionNo);
        param.append("/");
        param.append(transactionDate);


        LOGGER.info("[ REDEEM POINTS][redeemPoints] transactionNo=" + transactionNo + ";transactionDate=" +
                transactionDate + ";accountId=" + id + "; storecode=" + storeCode + "; amount=" + amount);
        LOGGER.info("[ REDEEM POINTS][URL CALL] /redeem/payment/" + id + "/" + storeCode + "/" + amount + "/" +
                transactionNo + "/" + transactionDate);


        return redeemPoints(id, storeCode, amount, transactionNo, transactionDate, false, request);
    }


    /*
    if redeemByItem is false, pay by points
    if redeemByItem is true, redeem product item

     */

    //@RequestMapping("/redeem/payment/{id}/{storeCode}/{amountOrPoints}/{transactionNo}/{transactionDate" +
    //        "}/{redeemByItem}")
    //@ResponseBody
    //@Transactional
    public ReturnMessage redeemPoints(String id, String storeCode,
                                      Double amountOrPoints,
                                      String transactionNo,
                                      String transactionDate, Boolean redeemItem, HttpServletRequest request) {


        ReturnMessage returnMessage = new ReturnMessage();

        //call primary ws url if applicable
        StringBuffer param = new StringBuffer("/redeem/payment/");
        param.append(id);
        param.append("/");
        param.append(storeCode);
        param.append("/");
        param.append(amountOrPoints);
        param.append("/");
        param.append(transactionNo);
        param.append("/");
        param.append(transactionDate);
        param.append("/");
        param.append(redeemItem);

       // LOGGER.info("[REDEEM POINTS][redeemPoints] transactionNo=" + transactionNo + ";transactionDate=" +
       //         transactionDate + ";accountId=" + id + "; storecode=" + storeCode + "; amountOrPoints=" +
       //         amountOrPoints + " redeemByItem=" + redeemItem);

        //trim last digit  for employee id - 12 digits if retrieve by POSS from barcode
        id = StringUtil.trimEmpId(id);

        String memberNotFoundCode = codePropertiesService.getDetailMessageCodeMemberNotFound();
        String internalErrorCode = codePropertiesService.getDetailMessageCodeInternalError();


        returnMessage.setCallingAction(PointTxnType.REDEEM.toString());

        MemberModel member = memberService.findActiveWithAccountId(id);
        if (null == member) {

            returnMessage.setMessageCode(memberNotFoundCode);
            returnMessage.setType(MessageType.ERROR);
            returnMessage.setMessage(messageSource.getMessage("points_rs_error_no_member", new Object[]{id}, null));
            return returnMessage;
        }

        LoyaltyCardInventory loyaltyCard = loyaltyCardService.findLoyaltyCard(member.getLoyaltyCardNo());
        if (loyaltyCard != null && loyaltyCard.getExpiryDate() != null) {
            if (loyaltyCardService.isCardExpired(loyaltyCard)) {
                returnMessage.setMessageCode(codePropertiesService.getDetailMessageCodeLoyaltyCardExpired());
                returnMessage.setType(MessageType.ERROR);
                returnMessage.setMessage(messageSource.getMessage("points_rs_error_loyaltycard_expired",
                        new Object[]{member.getLoyaltyCardNo()}, null));
                return returnMessage;
            }
        }

        populateReturnMessageWithMemberAttr(member, returnMessage);


        //check if memberType is suspended. This is a global configuration
        if (isSuspendedType(member)) {
            LOGGER.info("[REDEEM POINTS][suspeneded member] accountID = " + id);
            returnMessage.setType(MessageType.ERROR);
            returnMessage.setMessageCode(codePropertiesService.getDetailMessageCodePaymentTypeNotAllowed());
            String memberType = member.getMemberType() == null ? null : member.getMemberType().getDescription();
            returnMessage
                    .setMessage(messageSource.getMessage("points_rs_error_paymentType_not_allowed",
                            new Object[]{memberType}, null));

            return returnMessage;

        }

        //check if member is supplementary - not allowed to redeem

        if (StringUtils.isNotBlank(member.getParentAccountId())) {
            LOGGER.info("[REDEEM POINTS][Supplementary member] accountID = " + id);

            returnMessage.setType(MessageType.ERROR);
            returnMessage.setMessageCode(codePropertiesService.getDetailMessageCodeSupplementaryNotAllowed());
            returnMessage
                    .setMessage(messageSource.getMessage("points_rs_error_supplementary_not_allowed",
                            new Object[]{member.getAccountId()}, null));

            return returnMessage;

        }

        Long points = 0l;

        if (redeemItem) {

            points = amountOrPoints.longValue();
        } else {
            //convert amount to points
            points = promotionService.convertCurrencyToPoints(amountOrPoints).longValue();


        }


        if (points > member.getTotalPoints()) {
            returnMessage.setType(MessageType.ERROR);
            returnMessage.setMessageCode(codePropertiesService.getDetailMessageCodeNotEnoughPoints());
            returnMessage.setMessage(
                    messageSource.getMessage("points_rs_error_not_enough_points", new Object[]{transactionNo, 0,
                            points}, null));
            return returnMessage;
        }

        PointsTxnModel pointsTxnModel = new PointsTxnModel();
        pointsTxnModel.setTransactionPoints(-points.doubleValue());

        pointsTxnModel.setMemberModel(member);
        //pointsTxnModel.setMemberAccountId(member.getAccountId());
        pointsTxnModel.setTransactionNo(handleNull(transactionNo));
        pointsTxnModel.setStoreCode(handleNull(storeCode));

        Date transDate = null;

        try {

            transDate = DateUtil.convertDateString(DateUtil.REST_DATE_FORMAT, transactionDate);
            pointsTxnModel.setTransactionDateTime(transDate);
        } catch (ParseException e) {

            LOGGER.error("Error Parsing date parameter {} for transaction no {} :: {}  ", transactionDate,
                    transactionNo,
                    e.getMessage());
        }

        pointsTxnModel.setTransactionDateTime(transDate);

        double amount = 0.0;
        if (!redeemItem) {
            //if redeem items, this value is 0
            amount = amountOrPoints;
        }
        pointsTxnModel.setTransactionAmount(amount);
        pointsTxnModel.setTransactionType(PointTxnType.REDEEM);

        //call process points
        if (!pointsManagerService.processPoints(pointsTxnModel, returnMessage)) {

            if (returnMessage.getMessageCode() == null) {
                returnMessage.setType(MessageType.ERROR);
                returnMessage.setMessageCode(internalErrorCode);
                returnMessage.setMessage(messageSource.getMessage("points_rs_error_processing_points", null, null));
            }

        }


        if (null == returnMessage.getMessageCode() || returnMessage.getMessage().trim().equals("")) {
            returnMessage.setType(MessageType.SUCCESS);
            returnMessage.setMessage(messageSource.getMessage("points_rs_success_redeem_message",
                    new Object[]{points, id}, null));


        }

        return returnMessage;

    }

   /*
    @RequestMapping("/info/{id}")
    @Transactional(readOnly = true)
    public
    @ResponseBody
    ReturnMessage retrievePoints(@PathVariable(value = "id") String id) {
        //trim last digit  for employee id - 12 digits if retrieve by POSS from barcode
        id = StringUtil.trimEmpId(id);

        ReturnMessage returnMessage = new ReturnMessage();
        returnMessage.setCallingAction(PointTxnType.RETRIEVE.toString());

        MemberModel member = memberService.findActiveWithAccountId(id);
        if (null == member) {
            returnMessage.setMessageCode(codePropertiesService.getDetailMessageCodeMemberNotFound());
            returnMessage.setType(MessageType.ERROR);
            returnMessage.setMessage(messageSource.getMessage("points_rs_error_no_member", new Object[]{id}, null));
            return returnMessage;
        }

        returnMessage.setType(MessageType.SUCCESS);
        returnMessage.setAccountNumber(String.valueOf(member.getAccountId()));
        returnMessage.setMemberName(member.getFormattedMemberName());
        Double theTotalPts = pointsManagerService.synchronizePointsTxn(member);
        returnMessage.setTotalPoints(theTotalPts != null ? theTotalPts.longValue() : 0L);
        returnMessage.setMessage(messageSource.getMessage("points_rs_success_info_message",
                new Object[]{id, returnMessage.getTotalPoints()}, null));
        return returnMessage;
    }

*/

    private boolean isSuspendedType(MemberModel member) {
        boolean isSuspendedType = false;
        List<LookupDetail> suspendedTypes = pointsManagerService.getSuspendedMemberTypes();

        if (suspendedTypes == null) {
            return false;
        }

        String memberTypeCode = member.getMemberType() == null ? null : member.getMemberType().getCode();

        for (LookupDetail lookupDetail : suspendedTypes) {
            //if there is a suspended type and member type matches return true
            if (memberTypeCode.equalsIgnoreCase(lookupDetail.getCode())) {
                isSuspendedType = true;
                break;
            }

        }


        return isSuspendedType;
    }


    private void processEmployeePurchase(MemberModel member, String transactionDate, String transactionNo,
                                         String store,
                                         Double amount) {


        MemberDto employeeDto = new MemberDto(member);


        EmployeePurchaseTxnDto dto = new EmployeePurchaseTxnDto();
        dto.setMemberModel(employeeDto);
        try {
            dto.setTransactionDateTime(DateUtil.convertDateString(DateUtil.REST_DATE_FORMAT, transactionDate));
        } catch (ParseException e) {

            LOGGER.error("Error Parsing date parameter {} for transaction no {} :: {}  ", transactionDate,
                    transactionNo, e.getMessage());
        }
        dto.setTransactionNo(transactionNo);

        Store storeModel = storeService.getStoreByCode(store);
        if (storeModel != null) {
            dto.setStoreModel(new StoreDto(storeModel));
        } else {
            LOGGER.warn("Store code [" + store + "] does not exist.");
        }

        dto.setTransactionAmount(amount);
        dto.setStatus(TxnStatus.ACTIVE);
        dto.setTransactionType(VoucherTransactionType.PURCHASE);
        employeePurchaseService.savePurchaseTxn(dto);


    }


    private MemberModel voidMemberPoints(String transactionNo, String transactionDate, ReturnMessage returnMessage) {


        //retrieve pointtxn with transactionNo and type = earn;
        //a transaaction no can have multiple point transaction in cases where multiple payment type is use
        //List<PointsTxnModel> pointsTxns = pointsManagerService.retrievePoints(transactionNo, PointTxnType.EARN);
        List<PointsTxnModel> pointsTxns = pointsManagerService.retrievePointsByTransactionNo(transactionNo);

        LOGGER.info("[VOID TRANSACTION][voidMemberPoints] transactionNo=" + transactionNo + ";transactionDate=" +
                transactionDate + ";pointsTxns=" + pointsTxns);

        MemberModel memberModel = null;

        for (PointsTxnModel points : pointsTxns) {
            //only process points for earn and redeem
            if (points.getTransactionType().equals(PointTxnType.EARN) || points.getTransactionType().equals
                    (PointTxnType.REDEEM)) {

                Double transactionPoint = points.getTransactionPoints();
                PointsTxnModel pointsTxnModel = new PointsTxnModel();
                if (memberModel == null) {
                    memberModel = points.getMemberModel();

                }

                Date transDate = null;

                try {

                    transDate = DateUtil.convertDateString(DateUtil.REST_DATE_FORMAT, transactionDate);
                    pointsTxnModel.setTransactionDateTime(transDate);
                } catch (ParseException e) {

                    LOGGER.error("Error Parsing date parameter {} for transaction no {} :: {}  ", transactionDate,
                            transactionNo,
                            e.getMessage());
                }

                pointsTxnModel.setTransactionNo(handleNull(transactionNo));
                pointsTxnModel.setStoreCode(points.getStoreCode());
                if (points.getTransactionType().equals(PointTxnType.EARN)) {
                    pointsTxnModel.setTransactionAmount(-points.getTransactionAmount());
                    pointsTxnModel.setTransactionType(PointTxnType.VOID);
                    pointsTxnModel.setTransactionPoints(-transactionPoint);
                } else {
                    //for redeem just give back the negated points
                    //INFO might need to change the transaction type to indicate points is returned
                    pointsTxnModel.setTransactionAmount(points.getTransactionAmount());
                    pointsTxnModel.setTransactionType(PointTxnType.REDEEM);
                    pointsTxnModel.setTransactionPoints(Math.abs(transactionPoint));
                }

               /* pointsTxnModel.setExpiryDate(points.getExpiryDate());*/
                if (points.getExpiryDate() != null && points.getTransactionType().equals(PointTxnType.EARN)) {
                    points.setExpiryDate(null);
                    pointsManagerService.savePointsTxnModel(points);
                }
                pointsTxnModel.setMemberModel(memberModel);
                //pointsTxnModel.setMemberAccountId(memberModel.getAccountId());
                pointsTxnModel.setTransactionContributer(points.getTransactionContributer());

                //negate transaction point and create new points transaction with void type and negated points

                LOGGER.info("[VOID TRANSACTION][voidMemberPoints][pointsTxnModel] transactionNo=" + transactionNo +
                        ";" +
                        "transactionDate=" +
                        transactionDate + ";txnAmount=" + pointsTxnModel.getTransactionAmount() + ";txnPoints=" +
                        pointsTxnModel.getTransactionPoints());

                //call process points to create void transaction record
                if (!pointsManagerService.processPoints(pointsTxnModel, returnMessage)) {
                    if (returnMessage.getMessageCode() == null) {
                        returnMessage.setType(MessageType.ERROR);
                        returnMessage.setMessageCode(codePropertiesService.getDetailMessageCodeInternalError());
                        returnMessage.setMessage(messageSource.getMessage("points_rs_error_processing_points", null,
                                null));
                    }


                }
            }


        }


        //return memberModel to populate returnmessage in calling method
        return memberModel;

    }


    private void voidEmployeeVoidPurchase(String transactionNo,
                                          String transactionDate, ReturnMessage returnMessage) {

        LOGGER.info("[VOID TRANSACTION][voidEmployeeVoidPurchase] transactionNo=" + transactionNo + ";" +
                "transactionDate=" +
                transactionDate);

        //returnMessage.setCallingAction(EmployeeTxnType.PURCHASE.toString());

        List<EmployeePurchaseTxnModel> purchaseTxns = employeePurchaseService.findByTransactionNo(transactionNo);

        MemberDto employeeDto = null;
        double transactionAmount = 0;
        for (EmployeePurchaseTxnModel model : purchaseTxns) {

            EmployeePurchaseTxnDto dto = new EmployeePurchaseTxnDto(model);
            dto.setId(null); //create new record
            employeeDto = new MemberDto(model.getMemberModel());
            dto.setMemberModel(employeeDto);
            try {
                dto.setTransactionDateTime(DateUtil.convertDateString(DateUtil.REST_DATE_FORMAT, transactionDate));
            } catch (ParseException e) {

                LOGGER.error("Error Parsing date parameter {} for transaction no {} :: {}  ", transactionDate,
                        transactionNo, e.getMessage());
            }
            dto.setTransactionNo(transactionNo);
            if (model.getStore() != null) {
                dto.setStoreModel(new StoreDto(storeService.getStoreByCode(model.getStore().getCode())));

            }
            transactionAmount += model.getTransactionAmount();
            dto.setTransactionAmount(-model.getTransactionAmount());
            dto.setTransactionType(VoucherTransactionType.VOID);
            dto.setStatus(TxnStatus.ACTIVE);
            employeePurchaseService.savePurchaseTxn(dto);

            LOGGER.info("[VOID TRANSACTION][voidEmployeeVoidPurchase][EmployeePurchaseTxnDto] transactionNo=" +
                    transactionNo + ";" +
                    "transactionDate=" +
                    transactionDate + ";txnAmount=" + dto.getTransactionAmount() + ";employee=" + dto.getMemberModel());

        }


        String accountInfo = "";

        if (employeeDto != null) {
            accountInfo = employeeDto.getAccountId() + "-" + transactionNo;
        } else {
            accountInfo = transactionNo;
        }

        returnMessage.setMessage(messageSource.getMessage("emp_purchasetxn_rs_voidsuccess_message",
                new Object[]{accountInfo,
                        transactionAmount}, LocaleContextHolder.getLocale()));
        returnMessage.setType(MessageType.SUCCESS);


    }

    private MemberModel voidMemberStamps(List<StampTxnModel> stampTxns, String transactionNo, String transactionDate, ReturnMessage returnMessage) {

        LOGGER.info("[VOID TRANSACTION][voidMemberStamps] transactionNo=" + transactionNo + ";transactionDate=" + transactionDate
                + ";stampTxns=" + stampTxns);

        MemberModel memberModel = null;

        for (StampTxnModel stampTxn : stampTxns) {
            if (StampTxnType.EARN.equals(stampTxn.getTransactionType()))  {
                Double txnStamps = stampTxn.getTransactionHeader().getTotalTxnStamps();
                Double totalTxnAmount = 0D;

                memberModel = stampTxn.getTransactionHeader().getMemberModel();
                // workaround for create user/datetime not set
                DateTime currDateTime = DateTime.now();
                CustomSecurityUserDetailsImpl userDetails = UserUtil.getCurrentUser();
                String currUser = userDetails != null ? userDetails.getUsername() : "SYSTEM";

                StampTxnModel voidStampTxnModel = new StampTxnModel();
                StampTxnHeader voidStampTxnHeader = new StampTxnHeader();

                try {
                    Date transDate = DateUtil.convertDateString(DateUtil.REST_DATE_FORMAT, transactionDate);
                    voidStampTxnHeader.setTransactionDateTime(transDate);
                } catch (ParseException e) {
                    LOGGER.error("Error Parsing date parameter {} for transaction no {} :: {}  ", transactionDate, transactionNo, e.getMessage());
                }
                voidStampTxnHeader.setTransactionNo(handleNull(transactionNo));
                voidStampTxnHeader.setStoreCode(stampTxn.getTransactionHeader().getStoreCode());

                if (stampTxn.getTransactionType().equals(StampTxnType.EARN)) {
                    for (StampTxnDetail stampTxnDetail : stampTxn.getTransactionHeader().getTransactionDetails()) {
                        StampTxnDetail voidStampTxnDetail = new StampTxnDetail();
                        voidStampTxnDetail.setTransactionAmount(-stampTxnDetail.getTransactionAmount());
                        voidStampTxnDetail.setStampsPerMedia(-stampTxnDetail.getStampsPerMedia());
                        voidStampTxnDetail.setTransactionMedia(stampTxnDetail.getTransactionMedia());
                        voidStampTxnDetail.setStampTxnHeader(voidStampTxnHeader);
                        voidStampTxnDetail.setCreated(currDateTime);
                        voidStampTxnDetail.setCreateUser(currUser);
                        voidStampTxnHeader.getTransactionDetails().add(voidStampTxnDetail);
                        totalTxnAmount += stampTxnDetail.getTransactionAmount();
                    }
                    voidStampTxnHeader.setTotalTxnStamps(-txnStamps);
                    voidStampTxnModel.setTransactionType(StampTxnType.VOID);
                } else {
                    // TODO void REDEEM
                }

                voidStampTxnHeader.setMemberModel(stampTxn.getTransactionHeader().getMemberModel());
                voidStampTxnModel.setStampPromotion(stampTxn.getStampPromotion());
                voidStampTxnModel.setTransactionHeader(voidStampTxnHeader);
                voidStampTxnModel = stampService.saveStamp(voidStampTxnModel);
                callPortaVoidStampWS(voidStampTxnModel, stampTxn);

                LOGGER.info("[VOID TRANSACTION][voidMemberStamps][stampTxnModel] transactionNo=" + transactionNo + ";" +
                        "transactionDate=" + transactionDate + ";txnAmount=" + totalTxnAmount + ";txnStamps=" + txnStamps);

                returnMessage.setMessageCode(null);
                returnMessage.setEarnedStamps(-txnStamps);
            }
        }

        return memberModel;
    }


    private void populateReturnMessageWithMemberAttr(MemberDto member, ReturnMessage returnMessage) {
        returnMessage.setAccountNumber(String.valueOf(member.getAccountId()));
        returnMessage.setMemberName(member.getFormattedMemberName());
        returnMessage.setFirstName(member.getFirstName());
        returnMessage.setLastName(member.getLastName());
        Long points = member.getTotalPoints() != null ? member.getTotalPoints().longValue() : 0L;
        returnMessage.setTotalPoints(points);

        if (member.getMemberType() != null) {
            returnMessage.setMemberType(member.getMemberType().getCode());
            returnMessage.setMemberTypeDesc(member.getMemberType().getDescription());
        }

        if (pointsManagerService.getSuspendedMemberTypes().contains(member.getMemberType())) {
            returnMessage.setCurrencyAmount(0D);
        } else {
            returnMessage.setCurrencyAmount(promotionService.convertPointToCurrency(points));
        }

    }

    private void populateReturnMessageWithMemberAttr(MemberModel member, ReturnMessage returnMessage) {

        returnMessage.setAccountNumber(String.valueOf(member.getAccountId()));
        returnMessage.setMemberName(member.getFormattedMemberName());
        returnMessage.setFirstName(member.getFirstName());
        returnMessage.setLastName(member.getLastName());
        Long points = member.getTotalPoints() != null ? member.getTotalPoints().longValue() : 0L;
        returnMessage.setTotalPoints(points);

        if (member.getMemberType() != null) {
            returnMessage.setMemberType(member.getMemberType().getCode());
            returnMessage.setMemberTypeDesc(member.getMemberType().getDescription());
        }

        if (member.getCardType() != null) {
            returnMessage.setCardType(member.getCardType().getCode());
            returnMessage.setCardType(member.getCardType().getDescription());
        }

        returnMessage.setBusinessName(member.getCompanyName());

        Npwp npwp = member.getNpwp();
        if (npwp != null) {
            returnMessage.setNpwpId(npwp.getNpwpId());
            returnMessage.setNpwpAddress(npwp.getNpwpAddress());
            returnMessage.setNpwpName(npwp.getNpwpName());
        }
        returnMessage.setKtpId(member.getKtpId());
        returnMessage.setContact(member.getContact());

        if (pointsManagerService.getSuspendedMemberTypes().contains(member.getMemberType())) {
            returnMessage.setCurrencyAmount(0D);
        } else {
            returnMessage.setCurrencyAmount(promotionService.convertPointToCurrency(points));
        }

    }


    private String handleNull(String str) {

        if (null == str || str.equalsIgnoreCase("null")) {
            return null;
        }

        return str;
    }

    private void callPortaVoidStampWS(StampTxnModel voidStampTnx, StampTxnModel earnStampTnx) {
        StampPromoPortaVoidRestDto stampDto = new StampPromoPortaVoidRestDto(String.valueOf(voidStampTnx.getTransactionHeader().getMemberModel().getId()),
                voidStampTnx.getTransactionHeader().getStoreCode(), voidStampTnx.getTransactionHeader().getTransactionNo(),
                earnStampTnx.getTransactionHeader().getTransactionNo(), voidStampTnx.getCreated(), null);
        StampPromoPortaVoidDetailsDto voidDetail = new StampPromoPortaVoidDetailsDto(voidStampTnx.getStampPromotion().getId(),
                voidStampTnx.getStampPromotion().getName(), Math.abs(voidStampTnx.getTransactionHeader().getTotalTxnStamps()),
                MathUtils.difference(Math.abs(voidStampTnx.getTransactionHeader().getTotalTxnStamps()),
                        Math.abs(earnStampTnx.getTransactionHeader().getTotalTxnStamps())));
        stampDto.getVoidDetails().add(voidDetail);
        stampPromoService.callPortaVoidStampPromo(stampDto);
    }

}
