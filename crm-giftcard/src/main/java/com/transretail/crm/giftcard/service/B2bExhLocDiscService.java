package com.transretail.crm.giftcard.service;

import java.math.BigDecimal;
import java.util.List;

import com.transretail.crm.common.service.dto.response.NameIdPairDto;
import com.transretail.crm.common.service.dto.response.ResultList;
import com.transretail.crm.giftcard.dto.B2BExhDiscDto;
import com.transretail.crm.giftcard.dto.B2BExhLocDto;
import com.transretail.crm.giftcard.dto.B2bExhLocSearchDto;




public interface B2bExhLocDiscService {

	void saveLocation(B2BExhLocDto loc);

	void saveDiscount(B2BExhDiscDto disc);

	ResultList<B2BExhLocDto> getLocations(B2bExhLocSearchDto searchForm);

	ResultList<B2BExhDiscDto> getDiscounts(B2bExhLocSearchDto searchForm);

	B2BExhLocDto getLocation(Long id);

	B2BExhDiscDto getDiscount(Long id);

	void deleteLocation(Long id);

	void deleteDiscount(Long id);

	boolean isExistOverlappingLocation(B2BExhLocDto location);

	boolean isExistOverlappingDisc(B2BExhDiscDto disc);

	List<NameIdPairDto> getLocations(String nameLike);

	BigDecimal getDiscount(BigDecimal purchase);
	
	
}
