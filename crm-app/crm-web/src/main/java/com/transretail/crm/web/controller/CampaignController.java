package com.transretail.crm.web.controller;


import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.transretail.crm.core.dto.CampaignDto;
import com.transretail.crm.core.dto.ProgramDto;
import com.transretail.crm.core.entity.Program;
import com.transretail.crm.core.service.PromotionService;
import com.transretail.crm.web.controller.util.ControllerResponse;


@RequestMapping("/promotion/campaign")
@Controller
public class CampaignController {

	@Autowired
    PromotionService promotionService;

    @Autowired
    private MessageSource messageSource;



	private static final String PATH_LIST 	     = "/promotion/campaign/list";
	private static final String PATH_CREATE      = "/promotion/campaign/create";

	private static final String UIATTR_CAMPAIGNS = "campaigns";
	private static final String UIATTR_PROGRAMS  = "programs";

	@InitBinder
	void initBinder(WebDataBinder binder) {
		DateFormat theFormat = new SimpleDateFormat( "dd MMM yyyy" );
		theFormat.setLenient( false );
		binder.registerCustomEditor( Date.class, new CustomDateEditor( theFormat, true ) );
	}




	@RequestMapping( value="/list/{id}", produces="application/json; charset=utf-8" )
	public @ResponseBody ControllerResponse listCampaigns( @PathVariable(value = "id") Long inId ) {

		ControllerResponse theResp = new ControllerResponse();
		if ( null != inId ) {
			theResp.setResult( promotionService.getCampaignDtos( inId ) );
			theResp.setSuccess( true );
		}
		return theResp;
	}

	@RequestMapping( value="/list", produces="text/html" )
	public String listCampaigns( Model inUiModel ) {

		inUiModel.addAttribute( UIATTR_CAMPAIGNS, promotionService.getValidCampaigns( new Program() ) );
		return PATH_LIST;
	}

    @RequestMapping( value="/create", produces = "text/html" )
    public String createCampaign(
    		@ModelAttribute(value="campaign") CampaignDto inCampaign,
            Model inUiModel ) {

    	inUiModel.addAttribute( UIATTR_PROGRAMS, promotionService.getProgramDtos() );
        return PATH_CREATE;
    }

    @RequestMapping( value="/save", produces="application/json; charset=utf-8", method=RequestMethod.POST )
	public @ResponseBody ControllerResponse saveCampaign( 
			@ModelAttribute(value="campaign") CampaignDto inCampaign,
    		BindingResult inBinding ) {

		ControllerResponse theResp = new ControllerResponse();

		if ( isValid( inCampaign, inBinding ) ) {
    		promotionService.saveCampaignDto( inCampaign );
    		theResp.setSuccess( true );
    		theResp.setResult( inCampaign );
    	}
    	else {
    		theResp.setSuccess( false );
    		theResp.setResult( inBinding.getAllErrors() );
    	}

		return theResp;
	}



	private boolean isValid( CampaignDto inDto,
    		BindingResult inBinding ) {

		ProgramDto theProgram = promotionService.getProgramDto( inDto.getProgram() );

		if ( StringUtils.isBlank( inDto.getName() ) ) {
    		inBinding.reject( messageSource.getMessage( "promotion_msg_nameempty", null, LocaleContextHolder.getLocale() ) );
    	}

		if ( StringUtils.isBlank( inDto.getDescription() ) ) {
    		inBinding.reject( messageSource.getMessage( "promotion_msg_descempty", null, LocaleContextHolder.getLocale() ) );
    	}

    	if ( null == inDto.getStartDate() ) {
    		inBinding.reject( messageSource.getMessage( "promotion_msg_startdateempty", null, LocaleContextHolder.getLocale() ) );
    	}

    	if ( null == inDto.getEndDate() ) {
    		inBinding.reject( messageSource.getMessage( "promotion_msg_enddateempty", null, LocaleContextHolder.getLocale() ) );
    	}

    	if ( null != inDto.getStartDate() && null != inDto.getEndDate() ) {
    		if ( inDto.getStartDate() > ( inDto.getEndDate() ) ) {
    			inBinding.reject( messageSource.getMessage( "promotion_msg_daterangeinvalid", null, LocaleContextHolder.getLocale() ) );
    		}
    	}

		if ( null != inDto.getStartDate() ) {
			if ( null != theProgram && null !=  theProgram.getStartDate() 
					&& inDto.getStartDate() < ( theProgram.getStartDate() ) ) {
				inBinding.reject( messageSource.getMessage( "campaign_msg_startdatebefore", null, LocaleContextHolder.getLocale() ) );
			}
		}

		if ( null != inDto.getEndDate() ) {
			if ( null != theProgram && null !=  theProgram.getEndDate() 
					&& inDto.getEndDate() > ( theProgram.getEndDate() ) ) {
				inBinding.reject( messageSource.getMessage( "campaign_msg_enddateafter", null, LocaleContextHolder.getLocale() ) );
			}
		}

    	return !inBinding.hasErrors();
	}
}
