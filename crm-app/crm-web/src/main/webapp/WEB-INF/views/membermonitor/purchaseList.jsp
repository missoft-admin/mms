<%@ include file="../common/taglibs.jsp" %>

<div>
	<div class="page-header page-header2">
		<h1><spring:message code="label_monitor_transactions_purchase" arguments="${currentDate}" /></h1>
	</div>
	<div class="">
		<div>
			<spring:message code="label_member" arguments="${member.accountId}, ${member.formattedMemberName}" />
		</div>
	</div>
</div>

<div id="content_pointsList">
	<div id="purchaseList" class="data-table-01" 
	  	data-url="<c:url value="/member/monitor/purchases/${member.accountId}/${currentDate}" />"
		data-hdr-storecode="<spring:message code="points_prop_transaction_location"/>"
		data-hdr-transactiondatetime="<spring:message code="points_prop_txndate"/>" 
		data-hdr-transactionno="<spring:message code="points_prop_txnno"/>" 
		data-hdr-transactionamount="<spring:message code="points_prop_amount"/>"></div>
</div>

<link href="<spring:url value="/css/dataTables.css" />" rel="stylesheet">
<script src="<spring:url value="/js/jquery/jquery.dataTables.js" />" ></script>
<script src="<spring:url value="/js/jquery/jquery.dataTables.linkLengthMenu.js" />"></script>
<script src="<spring:url value="/js/jquery/jquery.dataTables.customPaging.js" />"></script>
<script src="<spring:url value="/js/jquery/ajax-datatable.js" />" ></script>
<script src="<spring:url value="/js/common/form2js.js" />" ></script>
<script src="<spring:url value="/js/common/json2.js" />" ></script>
<script src="<spring:url value="/js/jquery/jquery.toObject.js" />" ></script>

<script type="text/javascript">
$(document).ready(function() {
	transactionList = $("#purchaseList");

	var headers = {
		storeCode: transactionList.data("hdr-storecode") + "",
		transactionDateTime: transactionList.data("hdr-transactiondatetime") + "",
		transactionNo:	transactionList.data("hdr-transactionno") + "",
		transactionAmount: transactionList.data("hdr-transactionamount") + "",
	};

	var columnHeaders = [
		headers['storeCode'],
		headers['transactionDateTime'],
		headers['transactionNo'],
		headers['transactionAmount']];

	var modelFields = [
		{name: 'store'},
		{name: 'transactionDateTime', fieldCellRenderer : function (data, type, row) {
			return ( (row.longTxnDate)? new Date( row.longTxnDate ).customize(0) : "" );
        }},
        {name: 'transactionNo'},
	 	{name: 'transactionAmount', fieldCellRenderer : function (data, type, row) {
			return row.transactionAmountFormatted;
        }}];

	transactionList.ajaxDataTable({
		'autoload' : true,
		'ajaxSource' : transactionList.data("url"),
		'columnHeaders' : columnHeaders,
		'modelFields' : modelFields
	});
});
</script>