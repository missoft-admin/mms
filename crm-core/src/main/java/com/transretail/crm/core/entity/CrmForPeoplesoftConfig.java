package com.transretail.crm.core.entity;

import com.transretail.crm.core.entity.support.CustomAuditableEntity;
import java.io.Serializable;
import javax.persistence.*;

@Entity
@Table(name = "CRM_FOR_PEOPLESOFT_CONFIG")
public class CrmForPeoplesoftConfig extends CustomAuditableEntity<Long> implements Serializable {
	
	private static final long serialVersionUID = 1L;

	
	@Column(name = "TXN_TYPE", unique = true, nullable = false)
        @Enumerated(EnumType.STRING)
	private TransactionType transactionType;
	@Column(name = "MMS_ACCT")
	private String mmsAcct;
	@Column(name = "PS_ACCT")
	private String psAcct;
	@Column(name = "TYPE")
	private String type;
	@Column(name = "DEPT_ID")
	private String deptId;
	@Column(name = "NOTES")
	private String notes;
	@Column(name = "BU")
	private String businessUnit;
	
	public TransactionType getTransactionType() {
		return transactionType;
	}

	public void setTransactionType(TransactionType transactionType) {
		this.transactionType = transactionType;
	}

	public String getMmsAcct() {
		return mmsAcct;
	}

	public void setMmsAcct(String mmsAcct) {
		this.mmsAcct = mmsAcct;
	}

	public String getPsAcct() {
		return psAcct;
	}

	public void setPsAcct(String psAcct) {
		this.psAcct = psAcct;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getDeptId() {
		return deptId;
	}

	public void setDeptId(String deptId) {
		this.deptId = deptId;
	}
	

	public String getNotes() {
		return notes;
	}

	public void setNotes(String notes) {
		this.notes = notes;
	}
	
	public String getBusinessUnit() {
		return businessUnit;
	}

	public void setBusinessUnit(String bu) {
		this.businessUnit = bu;
	}


	public enum TransactionType {
		GIFT_TO_CUSTOMER,
		
		FREE_VOUCHER_SO_ACTIVATION,
		
		B2B_SO_PAYMENT,
		B2B_SO_ACTIVATION,
		B2B_SO_DISC,
		B2B_SO_SHIPPING,
		B2B_SO_CARD_FEE,
		B2B_SO_HANDLING_FEE,
		
		B2B_INTERNAL_SO_ACTIVATION,
		B2B_ADV_SO_VERIFICATION,
		B2B_ADV_SO_CANCEL_VERIF,
		B2B_ADV_SO_APPROVAL,
		
		B2B_ADV_SO_ACTIVATION,
		B2B_ADV_SO_DISC,
		B2B_ADV_SO_SHIPPING,
		B2B_ADV_SO_CARD_FEE,
		
		B2B_RETURN_REV_PAYMENT,
		B2B_RETURN_REV_ACTIVATION,
		B2B_RETURN_REV_DISC,
		B2B_RETURN_REV_CARD_FEE,
		
		B2C_ACTIVATION, // B2C Sales
		B2C_VOID_ACTIVATION, // B2C Sales Voiding
		REDEMPTION,
		VOID_REDEMPTION,
                
                AFFILIATE_CLAIM,
                AFFILIATE_REDEMPTION,
                
		EXPIRE_GC,
		
		EXHIBIT_B2B_PAYMENT,
		EXHIBIT_B2B_ACTIVATION,
		
		B2B_REP_SO_ACTIVATION,
		B2B_REP_SO_DISC,
		B2B_REP_SO_SHIPPING,
		B2B_REP_SO_CARD_FEE,
		B2B_REP_SO_HANDLING_FEE,
		B2B_REP_SO_PAYMENT,
                RELOAD_SALES, 
                RELOAD_SALES_VOID, 
                
                UNDEFINED // or null
	}
}

