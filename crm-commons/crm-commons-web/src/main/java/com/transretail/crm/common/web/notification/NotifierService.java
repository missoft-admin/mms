package com.transretail.crm.common.web.notification;

import java.util.Map;

import javax.inject.Inject;
import javax.inject.Named;
import javax.inject.Singleton;

import org.cometd.annotation.Configure;
import org.cometd.annotation.Service;
import org.cometd.annotation.Session;
import org.cometd.bayeux.server.BayeuxServer;
import org.cometd.bayeux.server.ConfigurableServerChannel;
import org.cometd.bayeux.server.ServerSession;
import org.cometd.server.authorizer.GrantAuthorizer;
import org.springframework.stereotype.Component;

@Named
@Singleton
@Component
@Service("notifierService")
public class NotifierService {
	@Inject
	private BayeuxServer bayeuxServer;
	@Session
	private ServerSession serverSession;
	
	@Configure("/service/notification")
	protected void configureNotificationRecieve(ConfigurableServerChannel channel) {
		channel.setPersistent(true);
		channel.addAuthorizer(GrantAuthorizer.GRANT_PUBLISH);
	}
	
	@Configure(value={"/notification/notify/points", "/notification/notify/promotion", "/notification/notify/mo",
			"/notification/notify/purchasetxn", "/notification/notify/rewards", "/notification/notify/loyalty",
			"/notification/notify/points/hr", "/notification/notify/points/cs", "/notification/notify/monitor/cs",
			"/notification/notify/monitor/hr", "/notification/notify/promobanner", "/notification/notify/loyalty/burn",
			"/notification/notify/loyalty/change", "/notification/notify/stockrequest"})
	protected void configureNotificationSend(ConfigurableServerChannel channel) {
		channel.addAuthorizer(GrantAuthorizer.GRANT_SUBSCRIBE);
	}
	
	@Configure(value={"/notification/update/points", "/notification/update/promotion", "/notification/update/mo",
			"/notification/update/purchasetxn", "/notification/update/rewards", "/notification/update/loyalty",
			"/notification/update/points/hr", "/notification/update/points/cs", "/notification/update/monitor"})
	protected void configureNotificationUpdate(ConfigurableServerChannel channel) {
		channel.addAuthorizer(GrantAuthorizer.GRANT_SUBSCRIBE_PUBLISH);
	}
	
	public void sendToChannel(String channel, Map<String, Object> message) {
		bayeuxServer.createChannelIfAbsent(channel);
		bayeuxServer.getChannel(channel).publish(serverSession, message, null);
	}
}
