package com.transretail.crm.core.entity.lookup;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.Immutable;
import org.springframework.beans.BeanUtils;
import org.springframework.util.Assert;

import com.transretail.crm.core.dto.StoreDto;

/**
 * @author Allan G. Ramirez (agramirez@exist.com)
 */
@Entity
@Table(name = "STORE")
@Immutable
public class Store implements Serializable {

    private static final long serialVersionUID = 1L;
    
    
    @Id
    @Column(name = "STORE_ID")
    private Integer id;
    @Column(name = "CODE")
    private String code;
    @Column(name = "NAME")
    private String name;

    public Store() {

    }

    public Store(StoreDto storeDto) {
        BeanUtils.copyProperties(storeDto, this);
    }

    public Store(String code) {
        Assert.notNull(code);
        this.code = code;
    }
    
    public Store(Integer id) {
        Assert.notNull(id);
        this.id = id;
    }
    
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return name;
    }

    public String getCodeAndName() {
        return code + " - " + name;
    }
}
