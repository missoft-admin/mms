package com.transretail.crm.giftcard.dto;

import org.joda.time.DateTime;

/**
 * description here.
 *
 * @author Vincent Gerard Tan
 */
public class PrepaidCardDto {
    private String cardNo;
    private String description;
    private Long previousBalance;
    private Long currentBalance;
    private DateTime registrationDate;
    
    public String getCardNo() {
        return cardNo;
    }

    public void setCardNo(String cardNo) {
        this.cardNo = cardNo;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Long getPreviousBalance() {
        return previousBalance;
    }

    public void setPreviousBalance(Long previousBalance) {
        this.previousBalance = previousBalance;
    }

    public Long getCurrentBalance() {
        return currentBalance;
    }

    public void setCurrentBalance(Long currentBalance) {
        this.currentBalance = currentBalance;
    }

    public DateTime getRegistrationDate() {
        return registrationDate;
    }

    public void setRegistrationDate(DateTime registrationDate) {
        this.registrationDate = registrationDate;
    }
    
    public String getRegDateString() {
        return registrationDate.toString("dd-MM-yyyy");
    }
}
