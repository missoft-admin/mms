package com.transretail.crm.core.dto;


import java.util.ArrayList;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;

import com.transretail.crm.core.entity.lookup.Product;


public class ProductDto {

	private String id;
	private String sku;
    private String itemCode;
    private String name;
    private String description;



    public ProductDto() {}
    public ProductDto( Product model ) {
    	this.id = model.getId();
    	this.sku = model.getSku();
    	this.itemCode = model.getItemCode();
    	this.name = model.getName();
    	this.description = model.getDescription();
    }

    public static List<ProductDto> toDtoList( List<Product> models ) {
		if ( CollectionUtils.isEmpty( models ) ) {
			return new ArrayList<ProductDto>();
		}

		List<ProductDto> dtos = new ArrayList<ProductDto>();
		for ( Product model : models ) {
			dtos.add( new ProductDto( model ) );
		}
		return dtos;
    }



	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getSku() {
		return sku;
	}
	public void setSku(String sku) {
		this.sku = sku;
	}
	public String getItemCode() {
		return itemCode;
	}
	public void setItemCode(String itemCode) {
		this.itemCode = itemCode;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}

}
