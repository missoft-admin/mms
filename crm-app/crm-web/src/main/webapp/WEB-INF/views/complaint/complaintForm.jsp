<%@include file="../common/taglibs.jsp" %>


<div id="content_complaintForm" class="modal hide  nofly modal-dialog">

  <div class="modal-content">
  <c:url var="url_action" value="${complaintAction}" />
  <form:form id="complaintForm" name="complaint" modelAttribute="complaint" method="POST" action="${url_action}" class="modal-form form-horizontal">

    <div class="modal-header">
      <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
      <h4><c:out value="${complaintHeader}" /></h4>
    </div>


    <div class="modal-body">
      <div class="hide alert alert-error" id="content_error"><button type="button" class="close" onclick="$(this).parent().hide();">&times;</button><div><form:errors   path="*"/></div></div>

      <div class="main-block">
        <div id="content_date" class="date control-group">
          <label for="dateTime" class="control-label"><spring:message code="complaint_date"/></label>
          <div  class="controls"><div>${complaint.dateTime}</div></div>
          <form:hidden path="dateTime" id="dateTime" />
        </div>

        <c:if test="${not empty complaint.id}"><form:hidden path="id"/></c:if>

        <c:if test="${not empty complaint.ticketNo}">
        <div id="content_ticketNo" class="ticketNo control-group">
          <label for="ticketNo" class="control-label"><spring:message code="complaint_ticketno"/></label>
          <div  class="controls"><div>${complaint.ticketNo}</div></div>
        </div>
        </c:if>

        <c:if test="${not empty complaint.status}">
        <div id="content_status" class="status control-group">
          <label for="status" class="control-label"><spring:message code="complaint_status"/></label>
          <div  class="controls"><div>${complaint.status}</div></div>
        </div>
        </c:if>

        <c:if test="${not empty store}">
        <div id="content_store" class="store control-group">
          <label for="store" class="control-label"><spring:message code="complaint_store"/></label>
          <div  class="controls"><div>${store.code} - ${store.name}</div></div>
        </div>
        </c:if>
        
        <div class="memberBarcode control-group">
        	<label for="memberBarcode" class="control-label"><spring:message code="complaint_member_barcode" /></label>
        	<div class="controls">
        		<form:input id="memberBarcode" path="memberBarcode"/>
        	</div>
        </div>

        <div class="contact control-group">
          <c:url var="url_populateMember" value="${populateMemberUrl}" />
          <label for="contact" class="control-label"><b class="required">*</b> <spring:message code="complaint_lbl_contact"/></label>
          <div class="controls" style="margin-top: 6px">
	          <fieldset class="row-fluid">
	            <label for="email"><spring:message code="complaint_email" /></label>
              <form:input id="email" path="email" data-url="${url_populateMember}" />
	          </fieldset>
            <fieldset class="row-fluid">
              <label for="mobileNo"><spring:message code="complaint_mobile" /></label>
              <form:input id="mobileNo" path="mobileNo" data-url="${url_populateMember}" cssClass="input-medium" />
            </fieldset>
          </div>
        </div>

        <div class="firstName control-group">
          <label for="firstName" class="control-label"><b class="required">*</b> <spring:message code="complaint_fname"/></label>
          <div  class="controls"><form:input id="firstName" path="firstName" /></div>
        </div>

        <div class="lastName control-group">
          <label for="lastName" class="control-label"><spring:message code="complaint_lname"/></label>
          <div  class="controls"><form:input id="lastName" path="lastName" /></div>
        </div>
        
        <div class="category control-group">
        	<label for="category" class="control-label"><spring:message code="complaint_category" /></label>
        	<div class="controls">
        		<form:select id="category" path="category" items="${categories}" />
        	</div>
        </div>

        <div class="complaints control-group">
          <label for="complaints" class="control-label">
            <b class="required">*</b> 
            <spring:message code="complaint_complaints"/>
            (<span id="complaintsCount">${complaintsCount}</span>)
          </label>
          <div  class="controls"><form:textarea id="complaints" path="complaints" data-count="${complaintsCount}" /></div>
        </div>

        <div class="genderCode control-group">
          <label for="genderCode" class="control-label"><b class="required">*</b> <spring:message code="complaint_gender"/></label>
          <div  class="controls">
            <form:select id="genderCode" path="genderCode">
              <option/>
              <form:options items="${genders}" itemValue="code" itemLabel="description" />
            </form:select>
          </div>
        </div>

        <div class="priority control-group">
          <label for="priority" class="control-label"><spring:message code="complaint_priority"/></label>
          <div  class="controls">
            <form:select id="priority" path="priority">
              <option/>
              <form:options items="${priorities}" />
            </form:select>
          </div>
        </div>

        <div class="control-group">
          <label for="priority" class="control-label">
            <c:if test="${empty complaint.assignedTo || not empty complaint.assignedTo and ( !complaint.info.isOwner() and !complaint.info.isCreator() )}">
              <spring:message code="complaint_assignedto"/>
            </c:if>
            <c:if test="${not empty complaint.assignedTo and ( complaint.info.isOwner() or complaint.info.isCreator() )}">
              <spring:message code="complaint_reassign"/>
            </c:if>
          </label>
          <div  class="controls">
            <c:choose>
              <c:when test="${empty complaint.assignedTo or complaint.info.isOwner() or complaint.info.isCreator()}">
	              <form:select id="assignedTo" path="assignedTo" >
	                <option/>
	                <%-- <c:forEach var="user" items="${users}">
                    <option value="${user.username}">${user.storeDesc} - ${user.username}</option>
	                </c:forEach> --%>
	                <form:options items="${users}" itemLabel="storeDescAndUser" itemValue="username" />
	              </form:select>
              </c:when>
              <c:otherwise>
                <div>${complaint.assignedTo}</div>
              </c:otherwise>
            </c:choose>
          </div>
        </div>

      </div>
    </div>


    <div class="modal-footer">
      <!-- <input id="saveBtn" class="btn btn-primary" type="submit" value="<spring:message code="label_save" />" />
      <input id="cancelBtn" class="btn" type="button" value="<spring:message code="label_cancel" />" data-dismiss="modal" /> -->
      <button type="button" id="saveBtn" class="btn btn-primary"><spring:message code="label_save" /></button>
      <button type="button" id="cancelBtn" data-dismiss="modal" class="btn btn-default"><spring:message code="label_cancel"/></button>
    </div>

  </form:form>
  </div>


  <script src='<c:url value="/js/viewspecific/complaint/complaintForm.js"/>' type='text/javascript'><![CDATA[&nbsp; ]]></script>
  <style type="text/css"> 
  <!-- 
    .contact .fieldset { padding-top: 5px; }
    .contact .fieldset .row-fluid label { /* font-size: 11px; */ }
    .modal-footer { text-align: center; } 
    .control-label { width: 130px !important; } 
    .controls { margin-left: 150px !important; } 
    .controls div { padding-top: 4px !important; }
  --> 
  </style>


</div>