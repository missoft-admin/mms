package com.transretail.crm.giftcard.dto;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import org.springframework.util.StringUtils;

import com.mysema.query.types.expr.BooleanExpression;
import com.transretail.crm.common.service.dto.request.AbstractSearchFormDto;
import com.transretail.crm.giftcard.entity.QGiftCard;

/**
 *
 */
public class GiftCardSearchDTO extends AbstractSearchFormDto {
    private String name;
    private String country;
    private String currency;
    private Boolean fixValue;
    private BigDecimal faceValue;
    private String vendorName;
    private Boolean luhnCheck;
    private Boolean redemption;
    private Boolean reload;
    private Boolean refund;
    private Boolean allowToSale;
    private String genCode;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public Boolean getFixValue() {
        return fixValue;
    }

    public void setFixValue(Boolean fixValue) {
        this.fixValue = fixValue;
    }

    public BigDecimal getFaceValue() {
        return faceValue;
    }

    public void setFaceValue(BigDecimal faceValue) {
        this.faceValue = faceValue;
    }

    public String getVendorName() {
        return vendorName;
    }

    public void setVendorName(String vendorName) {
        this.vendorName = vendorName;
    }

    public Boolean getLuhnCheck() {
        return luhnCheck;
    }

    public void setLuhnCheck(Boolean luhnCheck) {
        this.luhnCheck = luhnCheck;
    }

    public Boolean getRedemption() {
        return redemption;
    }

    public void setRedemption(Boolean redemption) {
        this.redemption = redemption;
    }

    public Boolean getReload() {
        return reload;
    }

    public void setReload(Boolean reload) {
        this.reload = reload;
    }

    public Boolean getRefund() {
        return refund;
    }

    public void setRefund(Boolean refund) {
        this.refund = refund;
    }

    public Boolean getAllowToSale() {
        return allowToSale;
    }

    public void setAllowToSale(Boolean allowToSale) {
        this.allowToSale = allowToSale;
    }

    public String getGenCode() {
        return genCode;
    }

    public void setGenCode(String genCode) {
        this.genCode = genCode;
    }

    @Override
    public BooleanExpression createSearchExpression() {
        List<BooleanExpression> expressions = new ArrayList<BooleanExpression>();
        QGiftCard qGiftCard = QGiftCard.giftCard;
        if (StringUtils.hasText(name)) {
            expressions.add(qGiftCard.name.startsWithIgnoreCase(name));
        }
        if (StringUtils.hasText(country)) {
            expressions.add(qGiftCard.country.startsWithIgnoreCase(country));
        }
        if (StringUtils.hasText(currency)) {
            expressions.add(qGiftCard.currency.startsWithIgnoreCase(currency));
        }
        if (fixValue != null) {
            expressions.add(qGiftCard.fixValue.eq(fixValue));
        }
        if (faceValue != null) {
            expressions.add(qGiftCard.faceValue.eq(faceValue));
        }
        if (StringUtils.hasText(vendorName)) {
            expressions.add(qGiftCard.vendorName.startsWithIgnoreCase(vendorName));
        }
        if (luhnCheck != null) {
            expressions.add(qGiftCard.luhnCheck.eq(luhnCheck));
        }
        if (redemption != null) {
            expressions.add(qGiftCard.redemption.eq(redemption));
        }
        if (reload != null) {
            expressions.add(qGiftCard.reload.eq(reload));
        }
        if (refund != null) {
            expressions.add(qGiftCard.refund.eq(refund));
        }
        if (allowToSale != null) {
            expressions.add(qGiftCard.allowToSale.eq(allowToSale));
        }
        if (StringUtils.hasText(genCode)) {
            expressions.add(qGiftCard.genCode.startsWithIgnoreCase(genCode));
        }
        return BooleanExpression.allOf(expressions.toArray(new BooleanExpression[expressions.size()]));
    }
}
