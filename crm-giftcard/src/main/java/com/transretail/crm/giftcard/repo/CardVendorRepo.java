package com.transretail.crm.giftcard.repo;

import org.springframework.stereotype.Repository;

import com.transretail.crm.common.repo.CrmQueryDslPredicateExecutor;
import com.transretail.crm.giftcard.entity.CardVendor;

/**
 *
 */
@Repository
public interface CardVendorRepo extends CrmQueryDslPredicateExecutor<CardVendor, Long> {
}
