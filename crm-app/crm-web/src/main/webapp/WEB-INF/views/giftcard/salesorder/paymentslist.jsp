<%@include file="../../common/taglibs.jsp" %>
<%@ taglib prefix="permission" tagdir="/WEB-INF/tags/permission" %>
<%@ taglib prefix="crm" uri="http://www.carrefour.com/crm/permission/tags" %>

<sec:authorize ifAnyGranted="ROLE_MERCHANT_SERVICE_SUPERVISOR" var="isHead" />
<sec:authorize ifAnyGranted="ROLE_TREASURY" var="isTreasury" />

<div class="page-header page-header2">
    <h1><spring:message code="sales_order_so_payment_info" /></h1>
</div>


  <div>
    <jsp:include page="paymentsearch.jsp" />
  </div>

<div class="form-horizontal pull-left mb20">
  <select name="reportType" class="selectpicker" id="reportType" data-header="Select a Format">
    <c:forEach items="${reportTypes}" var="reportType">
      <option data-url="<c:url value="/giftcard/salesorder/payments/printall?reportType=${reportType.code}" />" value="${reportType.code}"><spring:message code="report_type_${reportType.code}" /></option>
    </c:forEach>
  </select>
  <button class="btn btn-print btn-print-small tiptip" id="printBtn" value="<spring:message code="label_print" />" data-toggle="dropdown"><spring:message code="label_print" /></button>
</div>

<div class="pull-right mb20">
<button type="button" class="btn btn-primary" id="approvePayment"><spring:message code="label_approve" /></button>
</div>

<div id="payment_info_list_container">
</div>

<div class="modal hide  nofly" id="orderDialog" tabindex="-1" role="dialog" aria-hidden="true">
</div>

<jsp:include page="../../common/confirm.jsp" />

  <link href="<spring:url value="/css/dataTables.css" />" rel="stylesheet">
  <script src="<spring:url value="/js/jquery/jquery.dataTables.js" />" ></script>
  <script src="<spring:url value="/js/jquery/jquery.dataTables.linkLengthMenu.js" />"></script>
  <script src="<spring:url value="/js/jquery/jquery.dataTables.customPaging.js" />"></script>
  <script src="<spring:url value="/js/jquery/ajax-datatable.js" />" ></script>
  <script src="<spring:url value="/js/common/form2js.js" />" ></script>
  <script src="<spring:url value="/js/common/json2.js" />" ></script>
  <script src="<spring:url value="/js/jquery/jquery.toObject.js" />" ></script>
  <link href="<c:url value="/styles/temp.css" />" rel="stylesheet" />
  
  
  <script src="<spring:url value="/js/jquery/jquery.numberInput.js" />" ></script>
  <script src="<c:url value="/js/bootstrap/bootstrap-datepicker.js"/>" type='text/javascript'><![CDATA[&nbsp; ]]></script>
  <link href="<c:url value="/css/bootstrap/datepicker.css" />" rel="stylesheet" />
  <link href="<c:url value="/styles/temp.css" />" rel="stylesheet" />
  <script src="<spring:url value="/js/jquery/jquery.bindWithDelay.js" />"></script>
  <script src="<spring:url value="/js/jquery/jquery.ajaxQueue.js" />"></script>
  
<script>
$(document).ready(function() {
	
  $orderDialog = $("#orderDialog").modal({
    show: false
  }).on('hidden.bs.modal', function() {
    $(this).empty();
  }).css({
    "width": "900px",
  });
  
  var selectAllStr = "<div class=\"checkbox\"><input type=\"checkbox\" id=\"selectAllPayments\" /><spring:message code="sales_order_payment_selectall"/></div>";
  
  $list = $("#payment_info_list_container");
  
  $("#approvePayment").click(function() {
	  $checked = $(".paymentId:checked", $list);
	  if($checked.length > 0) {
		var checkedStr = $checked.map(function() {return this.value;}).get().join(',');
		var url = "<c:url value="/giftcard/salesorder/payments/approve/" />" + checkedStr;
		 getConfirm('<spring:message code="sales_order_payment_approvemultipleconfirm" />', function(result) { 
            if(result) {
            	$.post(url, function(data) {location.reload();});
            }
          });
	  }
  });
  
  $list.ajaxDataTable({
    'autoload'  : true,
    'ajaxSource' : "<c:url value="/giftcard/salesorder/payments/search" />",
    'aaSorting' :[[ 2, 'desc' ]],
    'columnHeaders' : [
      {text : selectAllStr},
      "<spring:message code="sales_order_no"/>",
      {text: "", className : "hide"},
      "<spring:message code="sales_order_date"/>",
      "<spring:message code="sales_order_customer_name"/>",
      "<spring:message code="sales_order_payment_type"/>",
      "<spring:message code="sales_order_payment_date"/>",
      "<spring:message code="sales_order_payment_amount"/>",
      "<spring:message code="sales_order_payment_details"/>",
      "<spring:message code="sales_order_type"/>",
      "<spring:message code="sales_order_status"/>",
      ""
    ],
    'modelFields' : [
      {customCell : function ( innerData, sSpecific, json ) {
        if (sSpecific == 'display') {
          var html = "";
          if(json.status == 'FIRST_APPROVAL' || json.status == 'SECOND_APPROVAL')
			  html += "<div class=\"checkbox\"><input type=\"checkbox\" name=\"id\" class=\"paymentId\" value=\""+json.id+"\" /></div>";          
          return html;
        } else {
          return "";
        }
      }
      },
      {name : 'orderNo', sortable : false},
      {name : 'lastUpdated', sortable: true, aaClassname : "hide", dataType: "DATE"},
      {name : 'orderDate', sortable : false},
      {name : 'customerName', sortable : false},
      {name : 'paymentType.code', sortable : true, fieldCellRenderer : function (data, type, row) {
          return row.paymentType.code + " - " + row.paymentType.description;
      }},
      {name : 'paymentDate', sortable : true},
      {name : 'paymentAmount', sortable : true, fieldCellRenderer : function (data, type, row) {
          return row.paymentAmountFmt;
      }},
      {name : 'paymentDetails', sortable : true},
      {name : 'orderType', sortable : false},
      {name : 'status', sortable : false, fieldCellRenderer : function (data, type, row) {
          return (row.status)? row.status.replace( new RegExp( "_", "g" ), " " ) : "";
      }},
      {customCell : function ( innerData, sSpecific, json ) {
        if (sSpecific == 'display') {
          var html = "";
          
          if(json.status == 'FIRST_APPROVAL')
          	html += "<button class=\"updateOrder btn btn-primary pull-left tiptip icn-delete\" data-order-id=\""+json.orderId+"\" title=\"<spring:message code="label_reject"/>\"></button>";
          
          return html;
        } else {
          return "";
        }
      }
      }
    ]
  }).on("click", ".updateOrder", function() {
	  var orderId = $(this).data("orderId");
	  var url = "<c:url value="/giftcard/salesorder/payments/reject/" />" + orderId;
	  getConfirm('<spring:message code="sales_order_payment_rejectconfirm" />', function(result) { 
        if(result) {
        	$.post(url, function(data) {location.reload();}); 
        }
      });
	  
  }).on("click", "#selectAllPayments", function() {
	  var isChecked = $(this).is(":checked");
	  $(".paymentId", $list).prop("checked", isChecked);
  }).on("click", ".paymentId", function() {
	  var isChecked = $(this).is(":checked");
	  if(!isChecked)
	 	 $("#selectAllPayments", $list).prop("checked", false);
  });
  
  $("#printBtn").click(function(e) {
		e.preventDefault();
		var url = $("#reportType option:selected").data("url");
		var selectedReportType = $("select[name='reportType']").val();
		var searchType = "&searchType=" + $("#criteria").val();
    	var searchValue = "&searchValue=" + $("#criteriaValue").val();
    	var dateFrom = "&dateFrom=" + $("#orderDateFrom").val();
    	var dateTo = "&dateTo=" + $("#orderDateTo").val();
    	var status = "&status=" + $("#status").val();
		 
        if ($("#criteria").val() != "") {
          url = url + searchType + searchValue;
        }
		  
        if ($("#orderDateFrom").val() != "") {
          url = url + dateFrom;
        }
		
        if ($("#orderDateTo").val() != "") {
          url = url + dateTo;
        }
        
        if ($("#status").val() != "") {
          url = url + status;
        }
		  
		  
		  
		if(selectedReportType == "pdf")
			window.open(url, '_blank', 'toolbar=0,location=0,menubar=0' );
		else if(selectedReportType == "excel")
			window.location.href = url;
	});
  
});
</script>
