package com.transretail.crm.giftcard.service;

import com.transretail.crm.common.reporting.jasper.JRProcessor;
import com.transretail.crm.giftcard.entity.support.GiftCardSaleTransaction;
import com.transretail.crm.giftcard.entity.support.GiftCardSalesType;
import net.sf.dynamicreports.jasper.builder.JasperReportBuilder;
import net.sf.dynamicreports.report.exception.DRException;
import org.joda.time.LocalDate;

/**
 *
 * @author Monte Cillo Co (mco@exist.com)
 */
public interface SalesAccountingReportService {
    
    JRProcessor printSalesDetail(LocalDate dateFrom, LocalDate dateTo, String businessUnit, String store, GiftCardSalesType salesType, boolean b2bReturn);
    
    JRProcessor printSalesSummary(LocalDate bookedDateFrom, LocalDate bookedDateTo, String businessUnit, GiftCardSaleTransaction transactionType, String store, GiftCardSalesType salesType);
    
    JRProcessor printRedeemDetail(LocalDate bookedDateFrom, LocalDate bookedDateTo, String businessUnit, String store, String product, String redeemType);
    
    JRProcessor printOutstandingBalance(LocalDate dateFrom, LocalDate dateTo, String store, String cardNo);
}
