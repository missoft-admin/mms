package com.transretail.crm.core.service.impl;


import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import com.transretail.crm.core.entity.CrmFile;
import com.transretail.crm.core.repo.CrmFileRepo;
import com.transretail.crm.core.service.CrmFileService;


@Service("crmFileService")
@Transactional
public class CrmFileServiceImpl implements CrmFileService {

	public static final String DIR_CHANNEL = System.getProperty( "java.io.tmpdir" ) + "/";	

	@Autowired
	CrmFileRepo repo;



	@Override 
	public CrmFile get( long id ) {
		return repo.findOne( id );
	}

	@Override
	public CrmFile saveMultipartFile( MultipartFile file, String modelType ) {
		CrmFile crmFile = new CrmFile();
		try {
			crmFile.setFile( file.getBytes() );
		} 
		catch (IOException e) {
			e.printStackTrace();
		}
		crmFile.setFilename( file.getOriginalFilename() );
		crmFile.setModelType( modelType );
		crmFile.setContentType( file.getContentType() );
		return repo.save( crmFile );
	}

	@Override
	public String temporarilySaveFile( long id ) {
		CrmFile crmFile = repo.findOne( id );
		MultipartFile file = new MultipartFileImpl( crmFile.getFile(), crmFile.getFilename(), crmFile.getContentType() );
		File transferFile = null;
		try {
			/*transferFile = new File( DIR_CHANNEL );
			if ( !transferFile.isDirectory() ) {
				transferFile.mkdir();
			}*/
			transferFile = new File( DIR_CHANNEL + crmFile.getId() + file.getOriginalFilename() );
			file.transferTo( transferFile );
		} 
		catch ( IOException ioe ) { return null; }
		catch ( Exception e ) { return null; }
		finally {}
		return transferFile.getPath();
	}

	@Override
	public MultipartFile getMultipartFile( long id ) {
		CrmFile crmFile = repo.findOne( id );
		if ( null == crmFile ) {
			return null;
		}

		return new MultipartFileImpl( crmFile.getFile(), crmFile.getFilename(), crmFile.getContentType() );
	}



	public static class MultipartFileImpl implements MultipartFile {

		private final byte[] fileBytes;
		private String filename;
		private String contentType;

		public MultipartFileImpl( byte[] fileBytes, String filename, String contentType ) {
			this.fileBytes = fileBytes;
			this.filename = filename;
			this.contentType = contentType;
		}

		@Override
		public String getName() { return null; }

		@Override
		public String getContentType() {
			return contentType; 
		}

		@Override
		public String getOriginalFilename() {
			return filename;
		}

		@Override
		public boolean isEmpty() { 
			return fileBytes == null || fileBytes.length == 0; 
		}

		@Override
		public long getSize() {
			return ( null != fileBytes )? fileBytes.length : 0;
		}

		@Override
		public byte[] getBytes() throws IOException {
			return fileBytes;
		}

		@Override
		public InputStream getInputStream() throws IOException {
			return new ByteArrayInputStream( fileBytes );
		}

		@Override
		public void transferTo(File dest) throws IOException, IllegalStateException {
			OutputStream stream = null;
			try {
				stream = new FileOutputStream(dest);
				stream.write( fileBytes );
			}
			catch ( IOException ioe ) {
				throw ioe;
			}
			catch ( IllegalStateException ise ) {
				throw ise;
			}
			finally {
				try { stream.close(); } catch ( Exception e ) {}
			}
		}
	}

}
