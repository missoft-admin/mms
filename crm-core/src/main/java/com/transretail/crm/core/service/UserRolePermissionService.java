package com.transretail.crm.core.service;

import java.util.List;
import java.util.Map;
import java.util.Set;

import com.transretail.crm.core.dto.UserRolePermDto;

/**
 * @author Allan G. Ramirez (agramirez@exist.com)
 */
public interface UserRolePermissionService {
    /**
     * List all roles and corresponding permissions if there's any and will also mark
     * {@link com.transretail.crm.core.dto.UserRolePermDto#isCurrentRolePerm} to true if role or permission
     * is current role or permission of current user.
     *
     * @param userId user id
     * @return Map<UserRolePermDto, Set<UserRolePermDto>> which contains all roles and permissions with indicator if user has that role or permission
     */
    Map<UserRolePermDto, Set<UserRolePermDto>> listUserRolePermissions(long userId);

    /**
     * Associate the dash separated (role-permission) tuple to current user
     *
     * @param userId             user id to update
     * @param currRolePermTuples dash separated list of role permission. Eg. ROLE_ADMIN-LOOKUP_MANAGEMENT
     */
    void saveUserRolePerm(long userId, List<String> currRolePermTuples);

    Set<String> getMarchantServiceSupervisorEmails();
}
