package com.transretail.crm.template.service.streaming.excel.handler.sheet.required;

import static org.apache.commons.lang3.StringUtils.isBlank;
import static org.apache.commons.lang3.StringUtils.isNotBlank;

import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.commons.lang3.StringUtils;
import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.mysema.query.jpa.impl.JPAQuery;
import com.transretail.crm.common.util.SimpleEncrytor;
import com.transretail.crm.core.entity.MemberModel;
import com.transretail.crm.core.entity.QMemberModel;
import com.transretail.crm.core.entity.embeddable.Address;
import com.transretail.crm.core.entity.embeddable.QAddress;
import com.transretail.crm.core.entity.enums.MemberCreatedFromType;
import com.transretail.crm.core.entity.lookup.LookupDetail;
import com.transretail.crm.core.repo.AddressRepo;
import com.transretail.crm.core.security.model.impl.CustomSecurityUserDetailsImpl;
import com.transretail.crm.template.service.streaming.excel.handler.HandlerException;
import com.transretail.crm.template.service.streaming.excel.handler.cell.AccountIdCellHandler;
import com.transretail.crm.template.service.streaming.excel.handler.cell.AddressCityCellHandler;
import com.transretail.crm.template.service.streaming.excel.handler.cell.AddressPostCodeCellHandler;
import com.transretail.crm.template.service.streaming.excel.handler.cell.AddressStreetCellHandler;
import com.transretail.crm.template.service.streaming.excel.handler.cell.BirthDateCellHandler;
import com.transretail.crm.template.service.streaming.excel.handler.cell.BirthPlaceCellHandler;
import com.transretail.crm.template.service.streaming.excel.handler.cell.CellHandler;
import com.transretail.crm.template.service.streaming.excel.handler.cell.DepartmentCellHandler;
import com.transretail.crm.template.service.streaming.excel.handler.cell.EmailCellHandler;
import com.transretail.crm.template.service.streaming.excel.handler.cell.EmployeeTypeCellHandler;
import com.transretail.crm.template.service.streaming.excel.handler.cell.FirstNameCellHandler;
import com.transretail.crm.template.service.streaming.excel.handler.cell.GenderCellHandler;
import com.transretail.crm.template.service.streaming.excel.handler.cell.KtpIdCellHandler;
import com.transretail.crm.template.service.streaming.excel.handler.cell.LastNameCellHandler;
import com.transretail.crm.template.service.streaming.excel.handler.cell.MaritalStatusCellHandler;
import com.transretail.crm.template.service.streaming.excel.handler.cell.MobilePhoneCellHandler;
import com.transretail.crm.template.service.streaming.excel.handler.cell.NationalityCellHandler;
import com.transretail.crm.template.service.streaming.excel.handler.cell.RegisteredStoreCellHandler;
import com.transretail.crm.template.service.streaming.excel.handler.cell.ReligionCellHandler;
import com.transretail.crm.template.service.streaming.excel.handler.cell.TerminationDateCellHandler;
import com.transretail.crm.template.service.streaming.excel.handler.sheet.AbstractSheetHandler;

/**
 * @author Allan G. Ramirez (agramirez@exist.com)
 */
public class NewEmpInfoHandler extends AbstractSheetHandler {
    private static final String HEADER_ACCOUNTID = "ACCOUNTNUMBER";
    private static final String HEADER_FIRSTNAME = "FIRSTNAME";
    private static final String HEADER_LASTNAME = "LASTNAME";
    private static final String HEADER_TERMINATIONDATE = "TERMINATIONDATE";
    private static final String HEADER_EMPTYPE = "EMPLOYEETYPE";
    private static final String HEADER_DEPARTMENT = "DEPARTMENT";
    private static final String HEADER_REGISTEREDSTORE = "REGISTEREDSTORE";
    private static final String HEADER_BIRTHDATE = "DATEOFBIRTH";
    private static final String HEADER_BIRTHPLACE = "PLACEOFBIRTH";
    private static final String HEADER_GENDER = "GENDER";
    private static final String HEADER_RELIGION = "RELIGION";
    private static final String HEADER_NATIONALITY = "NATIONALITY";
    private static final String HEADER_MOBILEPHONE = "MOBILEPHONE";
    private static final String HEADER_STREET = "STREET";
    private static final String HEADER_CITY = "CITY";
    private static final String HEADER_ZIPCODE = "POSTCODE";
    private static final String HEADER_EMAIL = "EMAIL";
    private static final String HEADER_MARITALSTATUS = "MARITALSTATUS";
    // Task #85818
    private static final String HEADER_KTPID = "KTPID";

    @Resource(name = "passwordEncoder")
    private PasswordEncoder passwordEncoder;

    private Map<String, CellHandler> cellHandlerClassMap = Maps.newHashMap();
    private CellHandler[] cellHandlers = null;
    private DateTime currentDateTime;
    private CustomSecurityUserDetailsImpl currentUser;

    @Autowired
    private AddressRepo addressRepo;

    public NewEmpInfoHandler(DateTime currentDateTime, CustomSecurityUserDetailsImpl currentUser) {
        this.currentDateTime = currentDateTime;
        this.currentUser = currentUser;
        cellHandlerClassMap.put(HEADER_ACCOUNTID, new AccountIdCellHandler());
        cellHandlerClassMap.put(HEADER_FIRSTNAME, new FirstNameCellHandler());
        cellHandlerClassMap.put(HEADER_LASTNAME, new LastNameCellHandler());
        cellHandlerClassMap.put(HEADER_TERMINATIONDATE, new TerminationDateCellHandler());
        cellHandlerClassMap.put(HEADER_EMPTYPE, new EmployeeTypeCellHandler(this));
        cellHandlerClassMap.put(HEADER_DEPARTMENT, new DepartmentCellHandler(this));
        cellHandlerClassMap.put(HEADER_REGISTEREDSTORE, new RegisteredStoreCellHandler());
        cellHandlerClassMap.put(HEADER_BIRTHDATE, new BirthDateCellHandler(this));
        cellHandlerClassMap.put(HEADER_BIRTHPLACE, new BirthPlaceCellHandler());
        cellHandlerClassMap.put(HEADER_GENDER, new GenderCellHandler(this));
        cellHandlerClassMap.put(HEADER_RELIGION, new ReligionCellHandler(this));
        cellHandlerClassMap.put(HEADER_NATIONALITY, new NationalityCellHandler(this));
        cellHandlerClassMap.put(HEADER_MOBILEPHONE, new MobilePhoneCellHandler());
        cellHandlerClassMap.put(HEADER_STREET, new AddressStreetCellHandler());
        cellHandlerClassMap.put(HEADER_CITY, new AddressCityCellHandler());
        cellHandlerClassMap.put(HEADER_ZIPCODE, new AddressPostCodeCellHandler());
        cellHandlerClassMap.put(HEADER_EMAIL, new EmailCellHandler());
        cellHandlerClassMap.put(HEADER_MARITALSTATUS, new MaritalStatusCellHandler(this));
        cellHandlerClassMap.put(HEADER_KTPID, new KtpIdCellHandler());
        cellHandlers = new CellHandler[cellHandlerClassMap.size()];
    }

    @Override
    protected void processHeaderCell(int i, String headerName) {
        CellHandler cellHandler = cellHandlerClassMap.get(headerName);
        if (cellHandler == null) {
            throw new HandlerException(getMessageSource()
                .getMessage("member.template.messages.excel.nohandler", new String[]{sheetName, headerName},
                    LocaleContextHolder.getLocale()));
        }
        cellHandlers[i] = cellHandler;
    }

    @Override
    protected CellHandler getCellHandler(int index) {
        return cellHandlers[index];
    }

    @Override
    protected void saveOrUpdateDto(MemberModel dto) throws HandlerException {
        Locale locale = LocaleContextHolder.getLocale();
        List<String> errors = Lists.newArrayList();

        Date birthDate = dto.getCustomerProfile().getBirthdate();
        if (isBlank(dto.getAccountId())) {
            errors.add(getMessageSource().getMessage("member.template.messages.excel.columns.required",
                new String[]{sheetName, String.valueOf(currentRowNum),
                    StringUtils.join(new String[]{HEADER_ACCOUNTID}, ",")}, locale
            ));
        } else {
            if (accountIdExists(dto.getAccountId())) {
                errors.add(getMessageSource().getMessage("member.template.messages.excel.accountid.exists",
                    new String[]{sheetName, dto.getAccountId()}, locale));
            }
        }
        if (birthDate == null) {
            errors.add(getMessageSource().getMessage("member.template.messages.excel.columns.required",
                new String[]{sheetName, String.valueOf(currentRowNum),
                    StringUtils.join(new String[]{HEADER_BIRTHDATE}, ",")}, locale
            ));
        }
        if (isBlank(dto.getContact())) {
            errors.add(getMessageSource().getMessage("member.template.messages.excel.columns.required",
                new String[]{sheetName, String.valueOf(currentRowNum),
                    StringUtils.join(new String[]{HEADER_MOBILEPHONE}, ",")}, locale
            ));
        } else {
            if (contactExists(dto.getContact())) {
                errors.add(getMessageSource().getMessage("member.template.messages.excel.mobile.exists",
                    new String[]{sheetName, dto.getContact()}, locale));
            }
        }
        if (isBlank(dto.getRegisteredStore())) {
            errors.add(getMessageSource().getMessage("member.template.messages.excel.columns.required",
                new String[]{sheetName, String.valueOf(currentRowNum),
                    StringUtils.join(new String[]{HEADER_REGISTEREDSTORE}, ",")}, locale
            ));
        }

        if (isNotBlank(dto.getEmail())) {
            if (!validEmail(dto.getEmail())) {
                errors.add(getMessageSource()
                    .getMessage("member.template.messages.excel.email.invalid", new String[]{sheetName, dto.getEmail()}, locale));
            } else if (emailExists(dto.getEmail())) {
                errors.add(getMessageSource().getMessage("member.template.messages.excel.email.exists",
                    new String[]{sheetName, dto.getEmail()}, locale));
            }
        }

        if (isNotBlank(dto.getIdNumber()) && idNumberExists(dto.getIdNumber())) {
            errors.add(getMessageSource().getMessage("member.template.messages.excel.ktid.exists",
                new String[]{sheetName, dto.getIdNumber().substring("KTP-".length())}, locale));
        }

        if (errors.size() > 0) {
            throw new HandlerException(StringUtils.join(errors, "\r\n"));
        }

        // Task #88915
        dto.setUsername(dto.getContact());

        // Task #84568 / Task #88915
        String formattedBirthDate = BIRTHDATE_PASSWORD_FORMAT.format(birthDate);
        dto.setPassword(passwordEncoder.encode(formattedBirthDate));
        if (StringUtils.isBlank(dto.getPin())) {
            // Bug #85179
            try {
                dto.setPin(SimpleEncrytor.getInstance().encrypt(BIRTHDATE_PIN_FORMAT.format(birthDate)));
                dto.setPinEncryped(true);
            } catch (SimpleEncrytor.EncryptDecryptException e) {
                throw new HandlerException(e);
            }
        }

        QMemberModel qMemberModel = QMemberModel.memberModel;
        Long addressId = saveOrUpdateAddress(dto.getAddress(), dto.getAccountId(), qMemberModel);
        if (addressId == null) {
            dto.setAddress(null);
        } else {
            dto.getAddress().setId(addressId);
        }

        dto.setCreated(currentDateTime);
        dto.setCreateUser(currentUser.getUsername());
        dto.setMemberType(new LookupDetail(getCodePropertiesService().getDetailMemberTypeEmployee()));
        dto.setMemberCreatedFromType(MemberCreatedFromType.FROM_UPLOAD);

        em.persist(dto);

        if (currentRowNum % batchSize == 0) {
            cleanup();
        }
    }

    @Override
    public int getExpectedNumberOfColumns() {
        return cellHandlers.length;
    }

    private Long saveOrUpdateAddress(Address dto, String accountId, QMemberModel qMemberModel) {
        Long result = null;
        QAddress qAddress = new QAddress("businessAddress");
        Address address = new JPAQuery(em).from(qMemberModel)
            .rightJoin(qMemberModel.address, qAddress)
            .where(qMemberModel.accountId.eq(accountId)).singleResult(
                qAddress);
        if (address == null) {
            if (StringUtils.isNotBlank(dto.getStreet()) ||
                StringUtils.isNotBlank(dto.getPostCode()) ||
                StringUtils.isNotBlank(dto.getCity())
                ) {
                result = addressRepo.saveAndFlush(dto).getId();
            }
        } else {
            address.setStreet(dto.getStreet());
            address.setCity(dto.getCity());
            address.setPostCode(dto.getPostCode());
            result = addressRepo.saveAndFlush(address).getId();
        }
        return result;
    }
}
