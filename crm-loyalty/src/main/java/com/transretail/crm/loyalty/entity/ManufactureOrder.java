package com.transretail.crm.loyalty.entity;

import com.transretail.crm.core.entity.lookup.LookupDetail;
import com.transretail.crm.core.entity.support.CustomAuditableEntity;
import com.transretail.crm.giftcard.entity.CardVendor;
import org.hibernate.annotations.Type;
import org.joda.time.LocalDate;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

@Table(name = "CRM_MO")
@Entity
public class ManufactureOrder extends CustomAuditableEntity<Long> implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Column(name = "MO_NO")
	private String moNo;
	
	@Column(name = "PO_NO")
	private String poNo;

	@ManyToOne
	@JoinColumn(name = "SUPPLIER")
	private CardVendor supplier;
	
	@Column(name = "ORDER_DATE")
    @Type(type = "org.jadira.usertype.dateandtime.joda.PersistentLocalDate")
	private LocalDate orderDate;
	
	@Column(name = "MO_DATE")
    @Type(type = "org.jadira.usertype.dateandtime.joda.PersistentLocalDate")
	private LocalDate moDate;
	
	@ManyToOne
	@JoinColumn(name = "STATUS")
	private LookupDetail status;
	
	@OneToMany(mappedBy = "manufactureOrder", orphanRemoval = true, fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	private List<ManufactureOrderTier> tiers;
	
	@Column(name = "REASON", length = 1000)
	private String reason;

    public ManufactureOrder() {

    }

    public ManufactureOrder(Long id) {
        setId(id);
    }

    public String getMoNo() {
		return moNo;
	}

	public void setMoNo(String moNo) {
		this.moNo = moNo;
	}

	public String getPoNo() {
		return poNo;
	}

	public void setPoNo(String poNo) {
		this.poNo = poNo;
	}

	public CardVendor getSupplier() {
		return supplier;
	}

	public void setSupplier(CardVendor supplier) {
		this.supplier = supplier;
	}

	public LocalDate getOrderDate() {
		return orderDate;
	}

	public void setOrderDate(LocalDate orderDate) {
		this.orderDate = orderDate;
	}

	public LookupDetail getStatus() {
		return status;
	}

	public void setStatus(LookupDetail status) {
		this.status = status;
	}

	public List<ManufactureOrderTier> getTiers() {
		return tiers;
	}

	public void setTiers(List<ManufactureOrderTier> tiers) {
		this.tiers = tiers;
	}

	public String getReason() {
		return reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	public LocalDate getMoDate() {
		return moDate;
	}

	public void setMoDate(LocalDate moDate) {
		this.moDate = moDate;
	}

}
