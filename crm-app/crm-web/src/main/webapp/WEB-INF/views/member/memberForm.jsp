<%@ include file="../common/taglibs.jsp" %>


<div id="content_memberForm" class="modal hide  nofly modal-dialog span12 row-fluid" data-url="<c:url value="/member/create" />">

  <div class="modal-content">
  <form:form id="memberForm" name="member" modelAttribute="member" action="${action}" class="modal-form form-horizontal" data-url="${action}">

    <div class="modal-header">
      <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
      <h4 id="isPrimary" class="label"><spring:message code="label_member_primary" /></h4>
      <h4><span id="isSupplement"><spring:message code="label_member_supplement" /></span><span id="parent"></span></h4>
    </div>

    <div class="modal-body">    
      <!-- Tabs -->
      <div class="pull-left">
      <ul class="nav nav-tabs mb20">
        <li class="active"><a href="#mainPane" data-toggle="pill"><spring:message code="label_member_account" text="Customer Information" /></a></li>
        <li>
          <a href="#profilePane" data-toggle="pill" class="memberDetails"><spring:message code="label_member_datapersonal" text="Personal Data" /></a>
          <a href="#profilePane" data-toggle="pill" class="professionalDetails" ><spring:message code="label_member_dataprofessional" text="Personal Data" /></a>
        </li>
        <li class="memberDetails"><a href="#workPane" data-toggle="pill"><spring:message code="label_member_datawork" text="Work Data" /></a></li>
        <li class="memberDetails"><a href="#otherPane" data-toggle="pill"><spring:message code="label_member_dataothers" text="Other Data" /></a></li>
        <li class="professionalDetails"><a href="#addParamPane" data-toggle="pill"><spring:message code="label_member_addparams" text="Additional Parameters" /></a></li>
        <li><a href="#cardPane" data-toggle="pill"><spring:message code="label_member_cards" text="Cards" /></a></li>
      </ul>
      </div>
      
      <div class="hide alert alert-error" id="content_error"><button type="button" class="close" onclick="$(this).parent().hide();">&times;</button><div><form:errors path="*"/></div></div>

      <div id="mainpane" class="tab-pane active"><jsp:include page="memberForm/mainpane.jsp" /></div>
      <div id="profilepane" class="tab-pane"><jsp:include page="memberForm/profilepane.jsp" /></div>
      <div id="workpane" class="tab-pane"><jsp:include page="memberForm/workpane.jsp" /></div>
      <div id="otherdatapane" class="tab-pane"><jsp:include page="memberForm/otherdatapane.jsp" /></div>
      <div id="cardPane" class="tab-pane"></div>
      <div id="addlpane" class="tab-pane"><jsp:include page="memberForm/addlpane.jsp" /></div>
    </div>

    <div class="modal-footer">
      <!--<input type="button" class="btn btn-primary" id="save_form" value="Save" />
      <input type="button" class="btn " id="close_form" value="Cancel" data-dismiss="modal" />-->
      <button type="button" id="save_form" class="btn btn-primary"><spring:message code="label_save" /></button>
      <button type="button" id="formCancel" data-dismiss="modal" class="btn btn-default"><spring:message code="label_cancel"/></button>
    </div>

  </form:form>
  </div>


  <script src='<c:url value="/js/bootstrap/bootstrap-datepicker.js"/>' type='text/javascript'><![CDATA[&nbsp; ]]></script>
  <script src='<c:url value="/js/bootstrap/bootstrap-timepicker.min.js"/>' type='text/javascript'><![CDATA[&nbsp; ]]></script>
  <link href="<c:url value="/css/bootstrap/datepicker.css" />" rel="stylesheet" />
  <link href="<c:url value="/css/bootstrap/bootstrap-timepicker.min.css" />" rel="stylesheet" />
  <style type="text/css">
  <!--
    .modal-fullscreen { left: 1px; bottom: 0px; top: 0% !important; width: 100%; margin-left: 0% !important; height: 95%; }
    .modal-fullscreen .modal-dialog { position: relative !important; }
    .modal-fullscreen .modal-body { position: absolute; width: 94%; top: 50px; bottom: 60px; }
    .modal-fullscreen .modal-footer { position: absolute !important; bottom: 0px; margin-bottom: 0% !important; width: 100%; }

    .span12 { width: 91%; top: 8% !important; }
    .span12 .modal-body {}
    .control-group { margin-top: 10px !important; margin-bottom: 10px !important; }
    .control-group .controls { margin-right: 5px !important; margin-left: 160px !important; }
    .checkbox .control-label { text-align: left !important; margin-left: 5px; text-transform: capitalize !important; }
    .required { font-size: inherit !important; }
    .checkbox { clear: both; }
    .space-top-01 { padding-top: 20px; }
    .input-mini { width: 40px; }
    
  -->
  </style>
  <%-- <script src='<c:url value="/js/viewspecific/user/memberForm.js"/>' type='text/javascript'><![CDATA[&nbsp; ]]></script> --%>
  <script type="text/javascript">
  $(document).ready( function() {

	  var theErrorContainer = $( "#content_userForm" ).find("#content_error");

	  var $theDialog = $( "#content_memberForm" );
	  var $theForm = $( "#memberForm" );

	  initLinks();
	  //initFormFields();
	  initDialog();

	  function initLinks() {

	  }

	  function initDialog() {

	    $theDialog.modal({ keyboard: false, backdrop: "static" });
	    $theDialog.on( "hide", function() { theErrorContainer.hide(); });
	  }

	  function save() {
	  }

  });
  </script>


</div>