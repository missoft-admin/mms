package com.transretail.crm.giftcard.dto;

import java.math.BigDecimal;
import java.util.List;

import org.apache.commons.lang3.StringUtils;

import com.google.common.collect.Lists;
import com.transretail.crm.core.entity.lookup.LookupDetail;
import com.transretail.crm.giftcard.entity.B2BExhibition;
import com.transretail.crm.giftcard.entity.B2BExhibitionPayments;


/**
 *
 */
public class B2BExhibitionPaymentsDto {
	
	private LookupDetail paymentType;
	private BigDecimal total;
	private String details;
	
	public B2BExhibitionPaymentsDto() {}
	public B2BExhibitionPaymentsDto(B2BExhibitionPayments pym) {
		this.paymentType = pym.getPaymentType();
		this.total = pym.getTotal();
		this.details = pym.getDetails();
	}
	
	public static List<B2BExhibitionPaymentsDto> toDto(List<B2BExhibitionPayments> pys) {
		List<B2BExhibitionPaymentsDto> pyds = Lists.newArrayList();
		for(B2BExhibitionPayments pym: pys) {
			pyds.add(new B2BExhibitionPaymentsDto(pym));
		}
		return pyds;
	}
	
	public B2BExhibitionPayments toModel(B2BExhibition exh) {
		B2BExhibitionPayments py = new B2BExhibitionPayments();
		py.setPaymentType(paymentType);
		py.setTotal(total);
		py.setDetails(details);
		py.setExh(exh);
		return py;
	}
	
	public static List<B2BExhibitionPayments> toModel(List<B2BExhibitionPaymentsDto> pymntsDto, B2BExhibition exh)  {
		List<B2BExhibitionPayments> pys = Lists.newArrayList();
		for(B2BExhibitionPaymentsDto pyDto: pymntsDto) {
			if (pyDto != null && pyDto.getPaymentType() != null)
				pys.add(pyDto.toModel(exh));
		}
		return pys;
	}
	public LookupDetail getPaymentType() {
		return paymentType;
	}
	public void setPaymentType(LookupDetail paymentType) {
		this.paymentType = paymentType;
	}
	public BigDecimal getTotal() {
		return total;
	}
	public void setTotal(BigDecimal total) {
		this.total = total;
	}
	public String getDetails() {
		return details;
	}
	public void setDetails(String details) {
		this.details = details;
	}
	public String getPaymentDesc() {
		return paymentType.getDescription() + (StringUtils.isNotBlank(details) ? " (" + details + ")" : "");
	}
	
	
	
	
	
}
