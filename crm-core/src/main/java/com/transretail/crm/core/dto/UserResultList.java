package com.transretail.crm.core.dto;

import java.util.Collection;

import org.springframework.data.domain.Page;

import com.transretail.crm.common.service.dto.response.AbstractResultListDTO;
import com.transretail.crm.core.entity.UserModel;

/**
 *
 */
public class UserResultList extends AbstractResultListDTO<UserModel> {
    public UserResultList(Collection<UserModel> results, long totalElements, boolean hasPreviousPage, boolean hasNextPage) {
        super(results, totalElements, hasPreviousPage, hasNextPage);
    }
    
    public UserResultList( Page<UserModel> page ) {
    	super(page);
    }
}
