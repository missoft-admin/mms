<%@ include file="../../common/taglibs.jsp" %>


<div id="content_approverForm" class="modal hide  nofly modal-dialog">

  <div class="modal-content">
  <c:url var="url_approver" value="/points/override/approver" />
  <c:url var="url_action" value="${action}" />
  <form:form id="approverForm" name="approver" modelAttribute="approver" action="${url_action}" class="modal-form form-horizontal" data-url="${url_approver}">

    <div class="modal-header">  
      <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
      <h4><spring:message code="points_label_createadapproversentry"/></h4>
    </div>

    <div class="modal-body">
      <div class="hide alert alert-error" id="content_error"><button type="button" class="close" onclick="$(this).parent().hide();">&times;</button><div><form:errors path="*"/></div></div>

      <div class="control-group">
        <form:hidden id="id" path="id" />
        <label for="user" class="control-label"><spring:message code="points_prop_approver_qualified"/></label>
        <div class="controls">
          <c:if test="${ null != approver.id }">
            <form:hidden id="username" path="user.username" />
            <label for="user" class="control-label data-content-01"><c:out value="${approver.user.username}" /> </label>
          </c:if>
          <c:if test="${ null == approver.id }">
	          <form:select id="user" path="user.username">
	              <option/>
	              <form:options items="${approvers}" itemLabel="username" itemValue="username"/>
	          </form:select>
          </c:if>
        </div>
      </div>
      <div class="control-group">
        <label for="amount" class="control-label"><spring:message code="points_prop_approver_maxpoints" /></label>
        <div class="controls"><form:input id="amount" path="amount" cssClass="floatInput" /></div>
        <form:hidden path="id" />
      </div>
    </div>

    <div class="modal-footer container-btn">
      <button type="saveButton" id="saveButton" class="btn btn-primary"><spring:message code="label_save" /></button>
      <button type="button" id="formCancel" data-dismiss="modal" class="btn btn-default"><spring:message code="label_cancel"/></button>
    </div>

  </form:form>
  </div>


  <script src="<spring:url value="/js/jquery/jquery.numberInput.js" />" ></script>
  <script src='<c:url value="/js/viewspecific/points/override/approverForm.js"/>' type='text/javascript'><![CDATA[&nbsp; ]]></script>
  
  


</div>