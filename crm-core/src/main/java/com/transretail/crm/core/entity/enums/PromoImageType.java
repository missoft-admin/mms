package com.transretail.crm.core.entity.enums;

public enum PromoImageType {
	BANNER(960, 350), 
	ADVERTISMENT(300, 120), 
	NEWS(280, 138), 
	PRODUCT(160, 160)
	;
	
	private int height;
	private int width;
	
	private PromoImageType(int width, int height) {
		this.width = width;
		this.height = height;
	}

	public int getHeight() {
		return height;
	}

	public void setHeight(int height) {
		this.height = height;
	}

	public int getWidth() {
		return width;
	}

	public void setWidth(int width) {
		this.width = width;
	}
}
