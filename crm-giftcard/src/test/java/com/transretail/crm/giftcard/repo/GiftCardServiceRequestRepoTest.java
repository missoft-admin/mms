package com.transretail.crm.giftcard.repo;

import com.transretail.crm.giftcard.entity.CardVendor;
import com.transretail.crm.giftcard.entity.GiftCardInventory;
import com.transretail.crm.giftcard.entity.GiftCardOrder;
import com.transretail.crm.giftcard.entity.GiftCardOrderItem;
import com.transretail.crm.giftcard.entity.GiftCardServiceRequest;
import com.transretail.crm.giftcard.entity.ProductProfile;
import com.transretail.crm.giftcard.entity.QGiftCardInventory;
import com.transretail.crm.giftcard.entity.support.GiftCardInventoryStatus;
import com.transretail.crm.giftcard.entity.support.GiftCardOrderStatus;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.fest.assertions.api.Assertions;
import org.joda.time.LocalDate;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;


/**
 *
 * @author Monte Cillo Co (mco@exist.com)
 */
@RunWith(SpringJUnit4ClassRunner.class)
@Transactional
@ContextConfiguration(locations = {"classpath*:/META-INF/spring/applicationContext*.xml",
    "classpath:META-INF/spring/applicationContext-test.xml"})
public class GiftCardServiceRequestRepoTest {

    @Autowired
    private GiftCardInventoryRepo inventoryRepo;
    @Autowired
    private GiftCardServiceRequestRepo serviceRequestRepo;
    @Autowired
    private CardVendorRepo cardVendorRepo;
    @Autowired
    private GiftCardOrderRepo giftCardOrderRepo;
    @Autowired
    private ProductProfileRepo productProfileRepo;
    @PersistenceContext
    private EntityManager em;
    private static final String VALID_BARCODE = "42384858973245";

    @Before
    public void setup() {
	final CardVendor cardVendor = new CardVendor();
	cardVendor.setMailAddress("mail add");
	cardVendor.setDefaultVendor(Boolean.TRUE);
	cardVendor.setEmailAddress("defaultVendor@sap.sanmiguil.com");
	cardVendor.setEnabled(Boolean.TRUE);
	cardVendor.setFormalName("ACME Vendor - San Miguel");
	cardVendor.setRegion("Manila");
	cardVendorRepo.save(cardVendor);

	ProductProfile productProfile = new ProductProfile();
	productProfile.setProductCode("PP01");
	productProfileRepo.save(productProfile);

	GiftCardOrder giftCardOrder = new GiftCardOrder();
	giftCardOrder.setMoDate(LocalDate.now());
	giftCardOrder.setMoNumber("279348xxx89");
	giftCardOrder.setPoDate(LocalDate.now());
	giftCardOrder.setPoNumber("POXX9000");
	giftCardOrder.setStatus(GiftCardOrderStatus.APPROVED);
	giftCardOrder.setVendor(cardVendor);

	GiftCardOrderItem giftCardOrderItem = new GiftCardOrderItem();
	giftCardOrderItem.setProfile(productProfile);
	giftCardOrderItem.setOrder(giftCardOrder);
	giftCardOrderItem.setProductCode("PROD100K");
	giftCardOrderItem.setQuantity(10l);
	giftCardOrderRepo.save(giftCardOrder);

	GiftCardInventory gc = new GiftCardInventory();
	gc.setBarcode(VALID_BARCODE);
	gc.setAllocateTo("22022");
	gc.setBatchNo(132);
	gc.setBatchRefNo("2345");
	gc.setSeqNo(9);
	gc.setSeries(4213434l);
	gc.setProductCode("PROD100K");
	gc.setProductName("PROD100K");
	gc.setOrder(giftCardOrder);
	gc.setOrderDate(LocalDate.now());
	gc.setProfile(productProfile);
	gc.setStatus(GiftCardInventoryStatus.ACTIVATED);
	inventoryRepo.save(gc);

	em.flush();
    }

    @Test
    public void findAllServiceRequestOfTest() {
	QGiftCardInventory qinv = QGiftCardInventory.giftCardInventory;
	GiftCardInventory gc = inventoryRepo.findOne(qinv.barcode.eq(VALID_BARCODE));

	GiftCardServiceRequest request = new GiftCardServiceRequest();
	request.setDetails("Sample test request details");
	request.setGiftcard(gc);
	request.setFiledBy("fileNewServiceRequestTest");
	serviceRequestRepo.saveAndFlush(request); // No need to test this, it is handled by spring data
	
	List<GiftCardServiceRequest> result = serviceRequestRepo.findAllServiceRequestOf(gc);
	Assertions.assertThat(result).isNotEmpty().hasSize(1)
		.contains(request);
    }

}
