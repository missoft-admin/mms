package com.transretail.crm.core.dto;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Transient;

import com.transretail.crm.core.entity.enums.MemberStatus;
import org.apache.commons.lang3.BooleanUtils;
import org.apache.commons.lang3.StringUtils;
import org.joda.time.LocalDate;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.google.common.collect.Lists;
import com.mysema.query.annotations.QueryProjection;
import com.transretail.crm.common.service.exception.GenericServiceException;
import com.transretail.crm.common.util.SimpleEncrytor;
import com.transretail.crm.common.web.converter.CurrencySerializer;
import com.transretail.crm.core.entity.MemberModel;
import com.transretail.crm.core.entity.embeddable.Address;
import com.transretail.crm.core.entity.embeddable.Channel;
import com.transretail.crm.core.entity.embeddable.ConfigurableFields;
import com.transretail.crm.core.entity.embeddable.CustomerProfile;
import com.transretail.crm.core.entity.embeddable.MarketingDetails;
import com.transretail.crm.core.entity.embeddable.ProfessionalProfile;
import com.transretail.crm.core.entity.enums.MemberCreatedFromType;
import com.transretail.crm.core.entity.enums.Status;
import com.transretail.crm.core.entity.lookup.LookupDetail;
import com.transretail.crm.core.util.FormatterUtil;

public class MemberDto {
//		private static final String[] IGNORE = {"parent", "parentModel", "pointsDetails", "prefStore",
//			"employeePurchaseTxnModels", "contact", "bestTimeToCall"};

    private Long id;
    private String firstName;
    private String middleName;
    private String lastName;
    private String pin;
    private String email;
    private String username;
    private String password;
    private Boolean enabled = Boolean.FALSE;
    private Boolean stampUser = Boolean.FALSE;
    private String accountId;
    private String companyName;
    private String storeName;
    private String registeredStore;
    private String registeredStoreName;
    private List<String> contact;
    private Double totalPoints;
    private String ktpId;
    private LookupDetail memberType;
    private LookupDetail empType;
    private MemberCreatedFromType memberCreatedFromType;
    private String validationCode;
    private String parent;
    private String parentStatus;
    private String parentName;
    private MemberDto parentModel;
    private List<EmployeePurchaseTxnDto> employeePurchaseTxnModels;
    private Double totalTransactionAmount;
    private Double currentTotalTransactionAmount;
    private MemberStatus accountStatus = MemberStatus.ACTIVE;
    private Channel channel;
    private CustomerProfile customerProfile;
    private ProfessionalProfile professionalProfile;
    private MarketingDetails marketingDetails;
    private Address address;
    private String npwpId;
    private String npwpName;
    private String npwpAddress;
    private String loyaltyCardNo;
    private List<String> bestTimeToCall;
    private Long transactionCount;
    private String passwordCheck;
    private String homePhone;
    private String idNumber;
    private String registrationDate;
    private String loyaltyCardDateIssued;
    private String loyaltyCardDateExpired;
    private String createdBy;
    private String createdByDetails;
    private LocalDate terminationDate;

    private String transactionNo; //for loyalty assign
    private Double currentFee;
    private Double annualFee;
    private Double replacementFee;
    private Boolean isFree = Boolean.FALSE;
    private ConfigurableFields configurableFields;


    private List<String> socialMediaAccts;

    public MemberDto() {

    }

    public MemberDto(MemberModel memberModel, Long transactionCount) {
        this(memberModel);
        this.transactionCount = transactionCount;
    }

    public MemberDto(String accountId, String username, String firstName,
                     String lastName, Long transactionCount) {
        this.accountId = accountId;
        this.username = username;
        this.firstName = firstName;
        this.lastName = lastName;
        this.transactionCount = transactionCount;
    }

    @QueryProjection
    public MemberDto(String accountId, String username, String firstName,
                     String lastName, LookupDetail memberType, Long transactionCount) {
        this.accountId = accountId;
        this.username = username;
        this.firstName = firstName;
        this.lastName = lastName;
        this.memberType = memberType;
        this.transactionCount = transactionCount;
    }

    @QueryProjection
    public MemberDto(String accountId, String username, String firstName,
                     String lastName, LookupDetail memberType, LookupDetail employeeType, Long transactionCount) {
        this.accountId = accountId;
        this.username = username;
        this.firstName = firstName;
        this.lastName = lastName;
        this.memberType = memberType;
        this.empType = employeeType;
        this.transactionCount = transactionCount;
    }

    public MemberDto(MemberModel memberModel) {
//	    	BeanUtils.copyProperties(memberModel, this, IGNORE);
        this.id = memberModel.getId();
        this.firstName = memberModel.getFirstName();
        this.middleName = memberModel.getMiddleName();
        this.lastName = memberModel.getLastName();
        try {
            if (StringUtils.isNotBlank(memberModel.getPin())) {
                this.pin = BooleanUtils.toBoolean(memberModel.getPinEncryped()) ? SimpleEncrytor.getInstance()
                        .decrypt(memberModel.getPin()) : memberModel.getPin();
            }
        } catch (SimpleEncrytor.EncryptDecryptException e) {
            throw new GenericServiceException(e);
        }
        this.email = memberModel.getEmail();
        this.username = memberModel.getUsername();
        this.password = memberModel.getPassword();
        this.enabled = memberModel.getEnabled();
        this.stampUser = memberModel.getStampUser();
        this.accountId = memberModel.getAccountId();
        this.companyName = memberModel.getCompanyName();
        this.storeName = memberModel.getStoreName();
        this.registeredStore = memberModel.getRegisteredStore();
//            this.registeredStoreName = memberModel.getRegisteredStore()
        if(memberModel.getContact() != null) {
            contact = Lists.newArrayList(memberModel.getContact().split(","));
        }
        this.totalPoints = memberModel.getTotalPoints();
        this.ktpId = memberModel.getKtpId();
        this.memberType = memberModel.getMemberType();
        this.empType = memberModel.getEmpType();
        this.memberCreatedFromType = memberModel.getMemberCreatedFromType();
        this.validationCode = memberModel.getValidationCode();
        this.parent = memberModel.getParentAccountId();
        this.accountStatus = memberModel.getAccountStatus();
        this.channel = memberModel.getChannel();
        this.customerProfile = memberModel.getCustomerProfile();
        this.professionalProfile = memberModel.getProfessionalProfile();
        this.address = memberModel.getAddress();
        this.marketingDetails = memberModel.getMarketingDetails();
        if (memberModel.getNpwp() != null) {
            this.npwpAddress = memberModel.getNpwp().getNpwpAddress();
            this.npwpId = memberModel.getNpwp().getNpwpId();
            this.npwpName = memberModel.getNpwp().getNpwpName();
        }
        this.loyaltyCardNo = memberModel.getLoyaltyCardNo();
        if (memberModel.getBestTimeToCall() != null) {
            bestTimeToCall = Lists.newArrayList(memberModel.getBestTimeToCall().split(","));
        }
        this.homePhone = memberModel.getHomePhone();
        this.idNumber = memberModel.getIdNumber();
        this.registrationDate = memberModel.getCreated() != null ? memberModel.getCreated().toString("MM-dd-yyyy") : "";
        this.loyaltyCardDateIssued = "";
        this.loyaltyCardDateExpired = "";
        if (StringUtils.isNotBlank(memberModel.getSocialMediaAccts())) {
            socialMediaAccts = Lists.newArrayList(memberModel.getSocialMediaAccts().split(","));
        }
        this.createdBy = StringUtils.isNotBlank(memberModel.getCreateUser()) ? memberModel.getCreateUser() : "";
        this.configurableFields = new ConfigurableFields(memberModel.getConfigurableFields());
        this.terminationDate = memberModel.getTerminationDate();

        this.stampUser = memberModel.getStampUser();
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getMiddleName() {
        return middleName;
    }

    public void setMiddleName(String middleName) {
        this.middleName = middleName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    /**
     * Get raw pin
     *
     * @return raw pin
     */
    public String getPin() {
        return pin;
    }

    /**
     * Set raw PIN
     *
     * @param pin pin
     */
    public void setPin(String pin) {
        this.pin = pin;
    }

    public String getEncryptedPin() {
        try {
            return StringUtils.isNotBlank(pin) ? SimpleEncrytor.getInstance().encrypt(pin) : null;
        } catch (SimpleEncrytor.EncryptDecryptException e) {
            throw new GenericServiceException(e);
        }
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getAccountId() {
        return accountId;
    }

    public void setAccountId(String accountId) {
        this.accountId = accountId;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public String getStoreName() {
        return storeName;
    }

    public void setStoreName(String storeName) {
        this.storeName = storeName;
    }

    public String getRegisteredStore() {
        return registeredStore;
    }

    public void setRegisteredStore(String registeredStore) {
        this.registeredStore = registeredStore;
    }

    public List<String> getContact() {
        return contact;
    }

    public void setContact(List<String> contact) {
        this.contact = contact;
    }

    public Double getTotalPoints() {
        return totalPoints;
    }

    public void setTotalPoints(Double totalPoints) {
        this.totalPoints = totalPoints;
    }

    public MemberCreatedFromType getMemberCreatedFromType() {
        return memberCreatedFromType;
    }

    public void setMemberCreatedFromType(MemberCreatedFromType memberCreatedFromType) {
        this.memberCreatedFromType = memberCreatedFromType;
    }

    public String getValidationCode() {
        return validationCode;
    }

    public void setValidationCode(String validationCode) {
        this.validationCode = validationCode;
    }

    public MemberDto getParentModel() {
        return parentModel;
    }

    public void setParentModel(MemberDto parent) {
        this.parentModel = parent;
        this.parent = (parentModel != null) ? parentModel.getAccountId() : null;
    }

    public String getParent() {
        return parent;
    }

    public void setParent(String parent) {
        this.parent = parent;
    }

    public String getParentStatus() {
        return parentStatus;
    }

    public void setParentStatus(String parentStatus) {
        this.parentStatus = parentStatus;
    }

    public Boolean getEnabled() {
        return enabled;
    }

    public void setEnabled(Boolean enabled) {
        this.enabled = BooleanUtils.toBoolean(enabled);
    }

    public Boolean getStampUser() {
        return stampUser;
    }

    public void setStampUser(Boolean stampUser) {
        this.stampUser = stampUser;
    }

    public List<EmployeePurchaseTxnDto> getEmployeePurchaseTxnModels() {
        return employeePurchaseTxnModels;
    }

    public void setEmployeePurchaseTxnModels(
            List<EmployeePurchaseTxnDto> employeePurchaseTxnModels) {
        this.employeePurchaseTxnModels = employeePurchaseTxnModels;
    }

    public Double getTotalTransactionAmount() {
        return totalTransactionAmount;
    }

    public void setTotalTransactionAmount(Double totalTransactionAmount) {
        this.totalTransactionAmount = totalTransactionAmount;
    }

    @JsonInclude(Include.ALWAYS)
    @JsonSerialize(using = CurrencySerializer.class)
    public Double getTxnAmount() {
        return totalTransactionAmount;
    }

    public MemberStatus getAccountStatus() {
        return accountStatus;
    }

    public void setAccountStatus(MemberStatus status) {
        this.accountStatus = status;
    }

    public Channel getChannel() {
        return channel;
    }

    public void setChannel(Channel channel) {
        this.channel = channel;
    }

    public CustomerProfile getCustomerProfile() {
        return customerProfile;
    }

    public void setCustomerProfile(CustomerProfile customerProfile) {
        this.customerProfile = customerProfile;
    }

    @Transient
    public String getFormattedMemberName() {
        return FormatterUtil.INSTANCE.formatName(lastName, firstName);

    }

    public String getNpwpId() {
        return npwpId;
    }

    public void setNpwpId(String npwpId) {
        this.npwpId = npwpId;
    }

    public String getNpwpName() {
        return npwpName;
    }

    public void setNpwpName(String npwpName) {
        this.npwpName = npwpName;
    }

    public String getNpwpAddress() {
        return npwpAddress;
    }

    public void setNpwpAddress(String npwpAddress) {
        this.npwpAddress = npwpAddress;
    }

    public String getLoyaltyCardNo() {
        return loyaltyCardNo;
    }

    public void setLoyaltyCardNo(String loyaltyCardNo) {
        this.loyaltyCardNo = loyaltyCardNo;
    }

    public LookupDetail getMemberType() {
        return memberType;
    }

    public void setMemberType(LookupDetail memberType) {
        this.memberType = memberType;
    }

    public LookupDetail getEmpType() {
        return empType;
    }

    public void setEmpType(LookupDetail empType) {
        this.empType = empType;
    }

    public ProfessionalProfile getProfessionalProfile() {
        return professionalProfile;
    }

    public void setProfessionalProfile(ProfessionalProfile professionalProfile) {
        this.professionalProfile = professionalProfile;
    }

    public Address getAddress() {
        return address;
    }

    public void setAddress(Address address) {
        this.address = address;
    }

    public String getKtpId() {
        return ktpId;
    }

    public void setKtpId(String ktpId) {
        this.ktpId = ktpId;
    }

    public String getParentName() {
        return parentName;
    }

    public void setParentName(String parentName) {
        this.parentName = parentName;
    }

    public List<String> getBestTimeToCall() {
        return bestTimeToCall;
    }

    public void setBestTimeToCall(List<String> bestTimeToCall) {
        this.bestTimeToCall = bestTimeToCall;
    }

    public String getRegisteredStoreName() {
        return registeredStoreName;
    }

    public void setRegisteredStoreName(String registeredStoreName) {
        this.registeredStoreName = registeredStoreName;
    }

    public Long getTransactionCount() {
        return transactionCount;
    }

    public void setTransactionCount(Long transactionCount) {
        this.transactionCount = transactionCount;
    }

    public String getPasswordCheck() {
        return passwordCheck;
    }

    public void setPasswordCheck(String passwordCheck) {
        this.passwordCheck = passwordCheck;
    }


    public static List<MemberDto> toDtoList(Iterable<MemberModel> iterable) {
        List<MemberDto> dtoList = new ArrayList<MemberDto>();
        for (MemberModel member : iterable) {
            MemberDto dto = new MemberDto(member);
            dtoList.add(dto);
        }
        return dtoList;
    }

    @JsonSerialize(using = CurrencySerializer.class)
    public Double getCurrentTotalTransactionAmount() {
        return currentTotalTransactionAmount;
    }

    public void setCurrentTotalTransactionAmount(
            Double currentTotalTransactionAmount) {
        this.currentTotalTransactionAmount = currentTotalTransactionAmount;
    }

    public String getHomePhone() {
        return homePhone;
    }

    public void setHomePhone(String homePhone) {
        this.homePhone = homePhone;
    }


    public MarketingDetails getMarketingDetails() {
        return marketingDetails;
    }

    public void setMarketingDetails(MarketingDetails marketingDetails) {
        this.marketingDetails = marketingDetails;
    }

    public String getIdNumber() {
        return idNumber;
    }

    public void setIdNumber(String idNumber) {
        this.idNumber = idNumber;
    }

    public String getRegistrationDate() {
        return registrationDate;
    }

    public void setRegistrationDate(String registrationDate) {
        this.registrationDate = registrationDate;
    }

    public String getLoyaltyCardDateIssued() {
        return loyaltyCardDateIssued;
    }

    public void setLoyaltyCardDateIssued(String loyaltyCardDateIssued) {
        this.loyaltyCardDateIssued = loyaltyCardDateIssued;
    }

    public String getLoyaltyCardDateExpired() {
        return loyaltyCardDateExpired;
    }

    public void setLoyaltyCardDateExpired(String loyaltyCardDateExpired) {
        this.loyaltyCardDateExpired = loyaltyCardDateExpired;
    }

    public String getTransactionNo() {
        return transactionNo;
    }

    public void setTransactionNo(String transactionNo) {
        this.transactionNo = transactionNo;
    }

    public Double getAnnualFee() {
        return annualFee;
    }

    public void setAnnualFee(Double annualFee) {
        this.annualFee = annualFee;
    }

    public Double getReplacementFee() {
        return replacementFee;
    }

    public void setReplacementFee(Double replacementFee) {
        this.replacementFee = replacementFee;
    }

    public List<String> getSocialMediaAccts() {
        return socialMediaAccts;
    }

    public void setSocialMediaAccts(List<String> socialMediaAccts) {
        this.socialMediaAccts = socialMediaAccts;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public String getCreatedByDetails() {
        return createdByDetails;
    }

    public void setCreatedByDetails(String createdByDetails) {
        this.createdByDetails = createdByDetails;
    }

    public Boolean getIsFree() {
        return isFree;
    }

    public void setIsFree(Boolean isFree) {
        this.isFree = isFree;
    }

    public Double getCurrentFee() {
        return currentFee;
    }

    public void setCurrentFee(Double currentFee) {
        this.currentFee = currentFee;
    }

    public ConfigurableFields getConfigurableFields() {
        return configurableFields;
    }

    public void setConfigurableFields(ConfigurableFields configurableFields) {
        this.configurableFields = configurableFields;
    }

    public LocalDate getTerminationDate() {
        return terminationDate;
    }

    public void setTerminationDate(LocalDate terminationDate) {
        this.terminationDate = terminationDate;
    }

    public void setFullStringContact(String contacts) {
        contact = Lists.newArrayList(contacts.split(","));
    }
}
