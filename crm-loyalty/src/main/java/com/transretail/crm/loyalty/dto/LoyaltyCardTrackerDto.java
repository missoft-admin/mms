package com.transretail.crm.loyalty.dto;

import org.joda.time.DateTime;
import org.joda.time.LocalDate;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.mysema.query.annotations.QueryProjection;
import com.transretail.crm.common.web.converter.LocalDateSerializer;

public class LoyaltyCardTrackerDto {
	private DateTime transactionDatetime;
	private Long begBal;
	private Long transferIn;
	private Long transferOut;
	private Long burn;
	private Long active;
	private Long receive;
	private Long physicalCount;
	
	
	private static final DateTimeFormatter FULL_DATE_PATTERN = DateTimeFormat.forPattern("MMMM d, yyyy");
	
	public LoyaltyCardTrackerDto() {}
	
	@QueryProjection
	public LoyaltyCardTrackerDto(
			DateTime transactionDatetime,
			Long transferIn, Long transferOut, Long burn, Long active,
			Long receive, Long physicalCount) {
		super();
		this.transactionDatetime = transactionDatetime;
		this.transferIn = transferIn;
		this.transferOut = -transferOut;
		this.burn = -burn;
		this.active = -active;
		this.receive = receive;
		this.physicalCount = physicalCount;
	}
	/*tracker_status_transferin=Transfer-In
	tracker_status_transferout=Transfer-Out
	tracker_status_burn=Burn
	tracker_status_active=Active
	tracker_status_receive=Receive
	tracker_status_physicalcount=Physical Count*/
	
	@JsonSerialize(using = LocalDateSerializer.class)
	public LocalDate getTransactionDate() {
		if(transactionDatetime != null)
			return transactionDatetime.toLocalDate();
		return null;
	}
	
	public String getTransactionDateStr() {
		LocalDate txnDate = getTransactionDate();
		if(txnDate != null)
			return FULL_DATE_PATTERN.print(getTransactionDate());
		return "";
	}
	
	public DateTime getTransactionDatetime() {
		return transactionDatetime;
	}
	public void setTransactionDatetime(DateTime transactionDatetime) {
		this.transactionDatetime = transactionDatetime;
	}
	public Long getBegBal() {
		return begBal;
	}
	public void setBegBal(Long begBal) {
		this.begBal = begBal;
	}
	public Long getEndBal() {
		Long endBal = 0l;
		if(begBal != null)
			endBal += begBal;
		if(transferIn != null)
			endBal += transferIn;
		if(transferOut != null)
			endBal += transferOut;
		if(burn != null)
			endBal += burn;
		if(active != null)
			endBal += active;
		if(receive != null)
			endBal += receive;
		if(physicalCount != null)
			endBal += physicalCount;
		return endBal;
	}
	public Long getTransferIn() {
		return transferIn;
	}
	public void setTransferIn(Long transferIn) {
		this.transferIn = transferIn;
	}
	public Long getTransferOut() {
		return transferOut;
	}
	public void setTransferOut(Long transferOut) {
		this.transferOut = transferOut;
	}
	public Long getBurn() {
		return burn;
	}
	public void setBurn(Long burn) {
		this.burn = burn;
	}
	public Long getActive() {
		return active;
	}
	public void setActive(Long active) {
		this.active = active;
	}
	public Long getReceive() {
		return receive;
	}
	public void setReceive(Long receive) {
		this.receive = receive;
	}
	public Long getPhysicalCount() {
		return physicalCount;
	}
	public void setPhysicalCount(Long physicalCount) {
		this.physicalCount = physicalCount;
	}
	
	
	/*
tracker_status_transferin=Transfer-In
tracker_status_transferout=Transfer-Out
tracker_status_burn=Burn
tracker_status_active=Active
tracker_status_receive=Receive
tracker_status_physicalcount=Physical Count
*/
	
		
	
}
