package com.transretail.crm.report.template.impl;

import java.math.BigDecimal;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import net.sf.jasperreports.engine.JRDataSource;

import org.apache.commons.lang3.StringUtils;
import org.joda.time.DateTime;
import org.joda.time.LocalDate;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.MessageSourceAware;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.context.support.MessageSourceAccessor;
import org.springframework.stereotype.Service;

import com.google.common.collect.Maps;
import com.google.common.collect.Sets;
import com.mysema.query.BooleanBuilder;
import com.mysema.query.jpa.JPASubQuery;
import com.mysema.query.jpa.impl.JPAQuery;
import com.mysema.query.types.Projections;
import com.mysema.query.types.QBean;
import com.mysema.query.types.expr.CaseBuilder;
import com.mysema.query.types.expr.NumberExpression;
import com.transretail.crm.common.reporting.engine.data.JRQueryDSLDataSource;
import com.transretail.crm.common.reporting.engine.data.JRQueryDSLDataSource.FieldValueCustomizer;
import com.transretail.crm.common.reporting.jasper.ClasspathFileResolver;
import com.transretail.crm.common.reporting.jasper.DefaultJRProcessor;
import com.transretail.crm.common.reporting.jasper.JRProcessor;
import com.transretail.crm.common.util.DateUtil;
import com.transretail.crm.core.entity.QPeopleSoftStore;
import com.transretail.crm.core.entity.enums.ReportType;
import com.transretail.crm.core.entity.lookup.LookupDetail;
import com.transretail.crm.core.entity.lookup.QStore;
import com.transretail.crm.core.entity.lookup.Store;
import com.transretail.crm.core.security.util.UserUtil;
import com.transretail.crm.core.service.CodePropertiesService;
import com.transretail.crm.core.service.LookupService;
import com.transretail.crm.core.service.StoreService;
import com.transretail.crm.giftcard.entity.QGiftCardCustomerProfile;
import com.transretail.crm.giftcard.entity.QReturnRecord;
import com.transretail.crm.giftcard.entity.QSalesOrderItem;
import com.transretail.crm.giftcard.entity.QSalesOrderPaymentInfo;
import com.transretail.crm.giftcard.entity.support.GiftCardSalesType;
import com.transretail.crm.giftcard.entity.support.SalesOrderType;
import com.transretail.crm.report.template.*;

@Service("collectionDetailReport")
public class CollectionDetailReport implements ReportTemplate, ReportsCustomizer, MessageSourceAware, NoDataFoundSupport {
    protected static final String TEMPLATE_NAME = "Collection Detail Report";
    protected static final String REPORT_NAME = "reports/collection_detail_report.jasper";
    
    protected static final String FILTER_SO_DATE_TO = CommonReportFilter.DATE_TO;
    protected static final String FILTER_SO_DATE_FROM = CommonReportFilter.DATE_FROM;
    protected static final String FILTER_PYMNT_DATE_TO = "paymentDateFrom";
    protected static final String FILTER_PYMNT_DATE_FROM = "paymentDateTo";
    protected static final String FILTER_SALES_TYPE = "salesType";
    protected static final String FILTER_CUST = "customer";
    protected static final String FILTER_SALES_STORE = "salesStore";
    
    private static final DateTimeFormatter INPUT_DATE_PATTERN = DateTimeFormat.forPattern(DateUtil.SEARCH_DATE_FORMAT);
    
    private static final Logger LOGGER = LoggerFactory.getLogger(CollectionDetailReport.class);
    
    @PersistenceContext
    private EntityManager em;
    private MessageSourceAccessor messageSource;
    @Autowired
    private StoreService storeService;
    @Autowired
    private LookupService lookupService;
    @Autowired
    private CodePropertiesService codePropertiesService;
    
    @Override
    public void setMessageSource(MessageSource messageSource) {
    	this.messageSource = new MessageSourceAccessor(messageSource);
    }
    
    @Override
    public ReportType[] getReportTypes() {
    	return new ReportType[]{ReportType.EXCEL};
    }
    
    @Override
    public String getName() {
    	return TEMPLATE_NAME;
    }

    @Override
    public String getDescription() {
    	return messageSource.getMessage("collection.dtl.report", (Object[]) null, LocaleContextHolder.getLocale());
    }
    
    @Override
    public boolean canView(Set<String> permissions) {
    	return true;
    }
    
    @Override
	public Set<Option> customizerOption() {
    	return Sets.newHashSet(ReportsCustomizer.Option.DATE_RANGE_FULL_VALIDATION);
	}
    
    @Override
    public Set<FilterField> getFilters() {
    	Set<FilterField> filters = Sets.newLinkedHashSet();

		filters.add(FilterField.createDateField(FILTER_SO_DATE_FROM,
				messageSource.getMessage("collection.dtl.pymntstartdate"),
				DateUtil.SEARCH_DATE_FORMAT, true));
		filters.add(FilterField.createDateField(FILTER_SO_DATE_TO,
				messageSource.getMessage("collection.dtl.pymntenddate"),
				DateUtil.SEARCH_DATE_FORMAT, true));

		filters.add(FilterField.createDateField(FILTER_PYMNT_DATE_FROM,
				messageSource.getMessage("collection.dtl.sostartdate"),
				DateUtil.SEARCH_DATE_FORMAT));
		filters.add(FilterField.createDateField(FILTER_PYMNT_DATE_TO,
				messageSource.getMessage("collection.dtl.soenddate"),
				DateUtil.SEARCH_DATE_FORMAT));

		Map<String, String> salesTypes = new LinkedHashMap<String, String>();
		salesTypes.put("", "");
		for (GiftCardSalesType key : GiftCardSalesType.values()) {
			salesTypes.put(key.toString(), key.toString());
		}
		filters.add(FilterField.createDropdownField(FILTER_SALES_TYPE,
                messageSource.getMessage("collection.dtl.salestype"), salesTypes));
		filters.add(FilterField.createDropdownField(FILTER_CUST,
                messageSource.getMessage("collection.dtl.customer"), getCustFilter()));
		filters.add(FilterField.createDropdownField(FILTER_SALES_STORE,
                messageSource.getMessage("collection.dtl.salesstore", (Object[]) null, LocaleContextHolder.getLocale()), getStoresFilter()));
		return filters;
    }
    
    private Map<String, String> getStoresFilter() {
    	QStore qstore = QStore.store;
    	Map<String, String> stores = Maps.newLinkedHashMap();
    	LookupDetail headOffice = lookupService.getDetailByCode(codePropertiesService.getDetailInvLocationHeadOffice());
    	stores.put(headOffice.getCode(), headOffice.getCode() + " - " + headOffice.getDescription());
    	stores.putAll(new JPAQuery(em).from(qstore).orderBy(qstore.code.asc()).map(qstore.code, qstore.code.append(" - ").append(qstore.name))); 
    	stores.put("", "");
    	return stores;
    }
    
    private Map<String, String> getCustFilter() {
    	QGiftCardCustomerProfile qcx = QGiftCardCustomerProfile.giftCardCustomerProfile;
    	Map<String, String> custMap = new JPAQuery(em).from(qcx).map(qcx.id.stringValue(), qcx.name); 
    	custMap.put("", "");
    	return custMap;
    }
    
    @Override
    public JRProcessor createJrProcessor(Map<String, String> map) {
        QSalesOrderPaymentInfo qpayment = QSalesOrderPaymentInfo.salesOrderPaymentInfo;

        QSalesOrderItem qitem = QSalesOrderItem.salesOrderItem;
        QReturnRecord qreturn = QReturnRecord.returnRecord;
        
        NumberExpression<BigDecimal> soAmtQ = new JPASubQuery()
                .from(qitem).where(qitem.order.orderNo.eq(qpayment.order.orderNo))
                .groupBy(qitem.order.orderNo, qitem.order.shippingFee, qitem.order.discountVal)
                .unique(qitem.faceAmount.sum().add(qitem.printFee.sum()).add(qitem.order.shippingFee.coalesce(BigDecimal.ZERO))
                        .subtract(qitem.faceAmount.sum().multiply(qitem.order.discountVal.coalesce(BigDecimal.ZERO).asNumber().divide(new BigDecimal(100)))));

        NumberExpression<BigDecimal> reverse = new CaseBuilder()
                .when(qpayment.order.orderType.eq(SalesOrderType.REPLACEMENT)).then(qpayment.paymentAmount.negate())
                .when(qpayment.order.orderNo.in(new JPASubQuery().from(qreturn).list(qreturn.orderNo.orderNo))).then(qpayment.paymentAmount.negate())
                .otherwise(qpayment.paymentAmount);

        NumberExpression<BigDecimal> collectionAmount = new CaseBuilder()
                .when(qpayment.paymentDate.eq(qpayment.order.orderDate)).then(qpayment.paymentAmount)
                .when(qpayment.order.orderType.eq(SalesOrderType.REPLACEMENT)).then(qpayment.paymentAmount.negate())
                .when(qpayment.order.orderNo.in(new JPASubQuery().from(qreturn).list(qreturn.orderNo.orderNo))).then(qpayment.paymentAmount.negate())
                .otherwise(BigDecimal.ZERO);
        QBean<ReportBean> projection = Projections.bean(ReportBean.class,
                qpayment.order.orderDate.as("orderDate"),
                qpayment.order.created.as("created"),
                qpayment.dateVerified.as("dateVerified"),
                qpayment.dateApproved.as("dateApproved"),
                qpayment.order.paymentStore.as("psStoreCode"),
                qpayment.order.paymentStore.as("salesStore"),
                qpayment.order.customer.name.as("customer"),
                qpayment.order.orderNo.as("orderNo"),
                qpayment.order.orderType.stringValue().as("orderType"),
                soAmtQ.as("soAmount"),
                collectionAmount.as("paymentAmount"),
                reverse.as("reverse"),
                qpayment.paymentType.description.as("paymentType"),
                qpayment.paymentDate.as("paymentDate"),
                qpayment.paymentDetails.as("paymentInfo"),
                qpayment.order.status.stringValue().as("orderStatus"),
                qpayment.status.stringValue().as("paymentStatus")
        );

        JPAQuery query = createQuery(map)
                .orderBy(qpayment.order.orderDate.asc(), qpayment.order.orderNo.asc(), qpayment.paymentDate.asc());

        final QPeopleSoftStore qpstore = QPeopleSoftStore.peopleSoftStore;
        
        JRDataSource dataSource = new JRQueryDSLDataSource(query, projection,
                new FieldValueCustomizer() {
                    private final Map<String, String> storeCacheMap = Maps.newHashMap();
                    private final Map<String, String> pStoreCacheMap = Maps.newHashMap();

                    @Override
                    public Object customize(String propertyName, Object value) {
                        final String fieldValue = value != null ? value.toString() : "";
                        if (value != null) {
                            if ("salesStore".equals(propertyName)) {
                                String storePresentation = storeCacheMap.get(fieldValue);
                                if (storePresentation != null) {
                                    return storePresentation;
                                } else {
                                    final Store store = storeService.getStoreByCode(fieldValue);
                                    storePresentation = store == null ? fieldValue : store.getCodeAndName();
                                    storeCacheMap.put(fieldValue, storePresentation);
                                    return storePresentation;
                                }
                            }

                            if ("psStoreCode".equals(propertyName)) {
                                String pStoreCode = pStoreCacheMap.get(fieldValue);
                                if (pStoreCode != null) {
                                    return pStoreCode;
                                }
                                pStoreCode = new JPAQuery(em).from(qpstore).where(qpstore.store.code.eq(fieldValue)).singleResult(qpstore.peoplesoftCode);
                                pStoreCacheMap.put(fieldValue, pStoreCode);
                                return pStoreCode;
                            }

                        }

                        return value;
                    }
                });
        
        GiftCardSalesType salesType = StringUtils.isNotBlank(map.get(FILTER_SALES_TYPE)) ? GiftCardSalesType.valueOf(map.get(FILTER_SALES_TYPE)) : null;
        Long custId = StringUtils.isNotBlank(map.get(FILTER_CUST)) ? Long.valueOf(map.get(FILTER_CUST)) : null;
        String salesStore = map.get(FILTER_SALES_STORE);
        
        DefaultJRProcessor jrProcessor = new DefaultJRProcessor(REPORT_NAME);
        jrProcessor.setFileResolver(new ClasspathFileResolver("reports"));
        jrProcessor.addParameter("SUB_DATA_SOURCE", dataSource);
        jrProcessor.addParameter("SO_START_DATE", map.get(FILTER_SO_DATE_FROM));
        jrProcessor.addParameter("SO_END_DATE", map.get(FILTER_SO_DATE_TO));

        jrProcessor.addParameter("PYMNT_START_DATE", map.get(FILTER_PYMNT_DATE_FROM));
        jrProcessor.addParameter("PYMNT_END_DATE", map.get(FILTER_PYMNT_DATE_TO));

        jrProcessor.addParameter("SALES_TYPE", map.get(FILTER_SALES_TYPE));
        QGiftCardCustomerProfile qcust = QGiftCardCustomerProfile.giftCardCustomerProfile;
        jrProcessor.addParameter("CUSTOMER", custId == null ? "" : new JPAQuery(em).from(qcust).where(qcust.id.eq(custId)).singleResult(qcust.name));

        if (StringUtils.isNotBlank(salesStore)) {
            if (codePropertiesService.getDetailInvLocationHeadOffice().equalsIgnoreCase(salesStore)) {
                LookupDetail ho = lookupService.getActiveDetailByCode(codePropertiesService.getDetailInvLocationHeadOffice());
                jrProcessor.addParameter("SALES_STORE", ho.getCode() + " - " + ho.getDescription());
            } else {
                QStore qstore = QStore.store;
                jrProcessor.addParameter("SALES_STORE", new JPAQuery(em)
                        .from(qstore).where(qstore.code.eq(salesStore))
                        .singleResult(qstore.code.append(" - ").append(qstore.name)));
            }

        }

        jrProcessor.addParameter("printedBy", UserUtil.getCurrentUser().getUsername());
        jrProcessor.addParameter("printedDate", new LocalDate().toString("dd-MM-yyyy"));

        return jrProcessor;
    }
    
    
    private JPAQuery createQuery(Map<String, String> map){
        LocalDate startPaymentDate = INPUT_DATE_PATTERN.parseLocalDate(map.get(FILTER_SO_DATE_FROM));
        LocalDate endPaymentDate = INPUT_DATE_PATTERN.parseLocalDate(map.get(FILTER_SO_DATE_TO));
        LocalDate startSoDate = StringUtils.isNotBlank(map.get(FILTER_PYMNT_DATE_FROM)) ? INPUT_DATE_PATTERN.parseLocalDate(map.get(FILTER_PYMNT_DATE_FROM)) : null;
        LocalDate endSoDate = StringUtils.isNotBlank(map.get(FILTER_PYMNT_DATE_TO)) ? INPUT_DATE_PATTERN.parseLocalDate(map.get(FILTER_PYMNT_DATE_TO)) : null;
        GiftCardSalesType salesType = StringUtils.isNotBlank(map.get(FILTER_SALES_TYPE)) ? GiftCardSalesType.valueOf(map.get(FILTER_SALES_TYPE)) : null;
        Long custId = StringUtils.isNotBlank(map.get(FILTER_CUST)) ? Long.valueOf(map.get(FILTER_CUST)) : null;
        String salesStore = map.get(FILTER_SALES_STORE);

        QSalesOrderPaymentInfo qpayment = QSalesOrderPaymentInfo.salesOrderPaymentInfo;
        BooleanBuilder f = new BooleanBuilder();

        f.and(qpayment.paymentDate.goe(startPaymentDate));
        f.and(qpayment.paymentDate.loe(endPaymentDate));

        if (startSoDate != null) {
			f.and(qpayment.order.orderDate.goe(startSoDate));
        }
        if (endSoDate != null) {
			f.and(qpayment.order.orderDate.loe(endSoDate));
        }
        if (salesType != null) {
            f.and(qpayment.order.orderType.in(salesType.getOrderTypes()));
        }
        if (custId != null) {
            f.and(qpayment.order.customer.id.eq(custId));
        }
        if (StringUtils.isNotBlank(salesStore)) {
            f.and(qpayment.order.paymentStore.eq(salesStore));
        }

        return new JPAQuery(em).from(qpayment).where(f);
    }

    @Override
    public boolean isEmpty(Map<String, String> filter) {
        return !createQuery(filter).exists();
    }
    
    public static class ReportBean {
    	private LocalDate orderDate;
    	private DateTime created;
    	private LocalDate dateVerified;
    	private LocalDate dateApproved;
    	private String psStoreCode;
    	private String salesStore;
    	private String customer;
    	private String orderNo;
    	private String orderType;
    	private BigDecimal soAmount;
    	private BigDecimal paymentAmount;
    	private BigDecimal reverse;
    	private String paymentType;
    	private LocalDate paymentDate;
    	private String paymentInfo;
    	private String orderStatus;
    	private String paymentStatus;
    	
		public LocalDate getOrderDate() {
			return orderDate;
		}
		public void setOrderDate(LocalDate orderDate) {
			this.orderDate = orderDate;
		}
		public DateTime getCreated() {
			return created;
		}
		public void setCreated(DateTime created) {
			this.created = created;
		}
		public LocalDate getDateVerified() {
			return dateVerified;
		}
		public void setDateVerified(LocalDate dateVerified) {
			this.dateVerified = dateVerified;
		}
		public LocalDate getDateApproved() {
			return dateApproved;
		}
		public void setDateApproved(LocalDate dateApproved) {
			this.dateApproved = dateApproved;
		}
		public String getPsStoreCode() {
			return psStoreCode;
		}
		public void setPsStoreCode(String psStoreCode) {
			this.psStoreCode = psStoreCode;
		}
		public String getSalesStore() {
			return salesStore;
		}
		public void setSalesStore(String salesStore) {
			this.salesStore = salesStore;
		}
		public String getCustomer() {
			return customer;
		}
		public void setCustomer(String customer) {
			this.customer = customer;
		}
		public String getOrderNo() {
			return orderNo;
		}
		public void setOrderNo(String orderNo) {
			this.orderNo = orderNo;
		}
		
		public String getOrderType() {
			return orderType;
		}
		public void setOrderType(String orderType) {
			this.orderType = orderType;
		}
		public BigDecimal getSoAmount() {
			return soAmount;
		}
		public void setSoAmount(BigDecimal soAmount) {
			this.soAmount = soAmount;
		}
		public BigDecimal getPaymentAmount() {
			return paymentAmount;
		}
		public void setPaymentAmount(BigDecimal paymentAmount) {
			this.paymentAmount = paymentAmount;
		}
		public BigDecimal getReverse() {
			return reverse;
		}
		public void setReverse(BigDecimal reverse) {
			this.reverse = reverse;
		}
		public String getPaymentType() {
			return paymentType;
		}
		public void setPaymentType(String paymentType) {
			this.paymentType = paymentType;
		}
		public LocalDate getPaymentDate() {
			return paymentDate;
		}
		public void setPaymentDate(LocalDate paymentDate) {
			this.paymentDate = paymentDate;
		}
		public String getPaymentInfo() {
			return paymentInfo;
		}
		public void setPaymentInfo(String paymentInfo) {
			this.paymentInfo = paymentInfo;
		}
		public String getOrderStatus() {
			return orderStatus;
		}
		public void setOrderStatus(String orderStatus) {
			this.orderStatus = orderStatus;
		}
		public String getPaymentStatus() {
			return paymentStatus;
		}
		public void setPaymentStatus(String paymentStatus) {
			this.paymentStatus = paymentStatus;
		}
		public String getSalesType() {
			return GiftCardSalesType.getSalesType(SalesOrderType.valueOf(this.orderType)).toString();
		}
		
    	
    	
    }

}
