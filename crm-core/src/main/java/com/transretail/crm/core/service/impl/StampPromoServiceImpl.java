package com.transretail.crm.core.service.impl;


import com.fasterxml.jackson.databind.ObjectMapper;
import com.mysema.query.jpa.JPQLQuery;
import com.mysema.query.jpa.impl.JPAQuery;
import com.mysema.query.types.ConstructorExpression;
import com.mysema.query.types.expr.BooleanExpression;
import com.transretail.crm.common.service.exception.MessageSourceResolvableException;
import com.transretail.crm.core.dto.*;
import com.transretail.crm.core.entity.*;
import com.transretail.crm.core.entity.enums.AppConfigDefaults;
import com.transretail.crm.core.entity.enums.AppKey;
import com.transretail.crm.core.entity.enums.StampTxnType;
import com.transretail.crm.core.entity.lookup.QProduct;
import com.transretail.crm.core.entity.lookup.QStore;
import com.transretail.crm.core.repo.ApplicationConfigRepo;
import com.transretail.crm.core.repo.PromotionRepo;
import com.transretail.crm.core.repo.StampTxnHeaderRepo;
import com.transretail.crm.core.repo.StampTxnModelRepo;
import com.transretail.crm.core.security.model.impl.CustomSecurityUserDetailsImpl;
import com.transretail.crm.core.security.util.UserUtil;
import com.transretail.crm.core.service.CodePropertiesService;
import com.transretail.crm.core.service.StampPromoService;
import org.apache.commons.beanutils.BeanUtils;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.joda.time.DateTime;
import org.joda.time.LocalDate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.lang.reflect.InvocationTargetException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.SocketTimeoutException;
import java.net.URL;
import java.util.List;


@Service("stampPromoService")
@Transactional
public class StampPromoServiceImpl implements StampPromoService {

	public static final String MD5_DATEFORMAT = "yyyymmDD"; //yyyyMMdd
    private static final Logger LOGGER = LoggerFactory.getLogger(StampPromoServiceImpl.class);

	@Autowired
	private ApplicationConfigRepo appConfigRepo;
	@Autowired
	private StampTxnModelRepo stampTxnModelRepo;
	@Autowired
	private StampTxnHeaderRepo stampTxnHeaderRepo;
	@Autowired
	private PromotionRepo promotionRepo;
	@Autowired
	private CodePropertiesService codePropertiesService;
    @PersistenceContext
    private EntityManager em;

	@Override
	public boolean callPortaCreateStampPromo( Promotion stampPromo ) {
		StampPromoPortaRestDto dto = new StampPromoPortaRestDto( stampPromo, MD5_DATEFORMAT );
		Object returnObj = new Object();
		return this.callPortaStampPromoWs( dto, returnObj, AppKey.PORTA_URL_STAMP_CREATE );
	}

	@Override
	public boolean callPortaRedeemStampPromo( StampPromoPortaRedeemRestDto portaDto ) {
		Object returnObj = new Object();
		return this.callPortaStampPromoWs( portaDto, returnObj, AppKey.PORTA_URL_STAMP_REDEEM );
	}

    @Override
    public boolean callPortaEarnStampPromo(StampPromoPortaEarnRestDto stampDto) {
        Object returnObj = new Object();
        return this.callPortaStampPromoWs(stampDto, returnObj, AppKey.PORTA_URL_STAMP_EARN);
    }

    @Override
    public boolean callPortaVoidStampPromo(StampPromoPortaVoidRestDto stampDto) {
        Object returnObj = new Object();
        return this.callPortaStampPromoWs(stampDto, returnObj, AppKey.PORTA_URL_STAMP_VOID);
    }

    @Override
	public List<StampPromoTxnDto> retrieveRedeemableItems( String memberId ) {
		return this.retrieveRedeemableItems( memberId, null, null, null, null );
	}

	@Override
	public StampPromoTxnDto redeemStamp( MemberModel member, String storeCode, String txnNo, 
			LocalDate redeemDate, Long redeemItemQty, Long promoId, String sku ) throws MessageSourceResolvableException {

		StampPromoTxnDto stampTxn = new StampPromoTxnDto();

		List<StampPromoTxnDto> redeemableItems = this.retrieveRedeemableItems( member.getAccountId(), promoId, null, storeCode, sku );
		if ( CollectionUtils.isEmpty( redeemableItems ) ) {
			throw new MessageSourceResolvableException( "promo.stamp.err.noredeemableitems", new String[]{ member.getAccountId(),  promoId.toString(), sku, storeCode}, null );
		}
		for ( StampPromoTxnDto dto : redeemableItems ) {
			if ( dto.getItemQty() < redeemItemQty ) {
				throw new MessageSourceResolvableException( "promo.stamp.err.redeemableitem.qty.less", new String[]{ member.getAccountId(),  
						promoId.toString(), sku, storeCode, redeemItemQty.toString() }, null );
			}
		}

        DateTime currDateTime = DateTime.now();
		CustomSecurityUserDetailsImpl userDetails = UserUtil.getCurrentUser();
        String currUser = userDetails != null ? userDetails.getUsername() : "SYSTEM";
        QPromotionRedemption qRedeem = QPromotionRedemption.promotionRedemption;
		JPQLQuery query = new JPAQuery( em )
			.from( qRedeem )
			.where( 
				BooleanExpression.allOf( 
						qRedeem.promotion.id.eq( promoId ),
						qRedeem.sku.equalsIgnoreCase( sku ) ) );
		long stampQty = query.singleResult( qRedeem.qty );
		Double redeemedStamps = new Double( redeemItemQty * stampQty );

        StampTxnHeader redeemStampTxnHeader = new StampTxnHeader();
        redeemStampTxnHeader.setStoreCode( storeCode );
        redeemStampTxnHeader.setTransactionNo( txnNo );
		redeemStampTxnHeader.setTotalTxnStamps( -redeemedStamps );
        redeemStampTxnHeader.setMemberModel( member );
        redeemStampTxnHeader.setCreated( currDateTime );
        redeemStampTxnHeader.setCreateUser( currUser );
        StampTxnModel redeemStampTxnModel = new StampTxnModel();
        redeemStampTxnModel.setTransactionType( StampTxnType.REDEEM );
        redeemStampTxnModel.setStampPromotion( promotionRepo.findOne( promoId ) );
        redeemStampTxnModel.setTransactionHeader( redeemStampTxnHeader );
        redeemStampTxnModel.setCreated( currDateTime );
        redeemStampTxnModel.setCreateUser( currUser );

        stampTxnHeaderRepo.save( redeemStampTxnHeader );
        stampTxnModelRepo.save( redeemStampTxnModel );

        stampTxn.setRedeemedStamps( redeemItemQty );
        stampTxn.setPromoID( promoId );
        stampTxn.setItemQty( redeemItemQty );
        stampTxn.setRemainingStamps( this.retrieveRemainingStamps( member.getAccountId(), promoId ) );
		return stampTxn;
	}

	@Override
	public boolean isPromoForApproval( AppKey appKey, Long promoId ) {
		QPromotion qPromo = QPromotion.promotion;
		long promoCnt = new JPAQuery( em ).from( qPromo )
				.where( qPromo.id.eq( promoId )
						.and( qPromo.rewardType.code.equalsIgnoreCase( codePropertiesService.getDetailRewardTypeStamp() ) ) )
				.count();
		if ( promoCnt > 0 ) {
	        ApplicationConfig config = appConfigRepo.findByKey( appKey );
	        if ( null == config || StringUtils.isBlank( config.getValue() ) ) {
	        	return false;
	        }
		}
		return true;
	}



	private List<StampPromoTxnDto> retrieveRedeemableItems( String memberId, Long promoId, Long qty, String storeCode, String sku ) {
		QStampTxnModel qStamp  = QStampTxnModel.stampTxnModel;
		QPromotionRedemption qRedeem = QPromotionRedemption.promotionRedemption;
		QProduct qProduct = QProduct.product;
		QStore qStore = QStore.store;

		JPQLQuery query = new JPAQuery( em )
			.from( qStamp, qProduct, qStore )
			.leftJoin( qStamp.stampPromotion.promotionRedemptions, qRedeem )
			.where( 
				BooleanExpression.allOf( 
					qRedeem.sku.equalsIgnoreCase( qProduct.sku ),
					qStamp.transactionHeader.storeCode.equalsIgnoreCase( qStore.code ),
					createRedeemFilter( memberId ),
					qRedeem.redeemFrom.loe( LocalDate.now() ),
					qRedeem.redeemTo.goe( LocalDate.now() ) ),
					null == promoId? null : qStamp.stampPromotion.id.eq( promoId ),
					null == storeCode? null : qStamp.transactionHeader.storeCode.equalsIgnoreCase( storeCode ),
					null == sku? null : qRedeem.sku.equalsIgnoreCase( sku ) )
			.groupBy( qRedeem.promotion.id, qRedeem.promotion.description, qStamp.transactionHeader.storeCode, qStore.name, 
					qRedeem.sku, qProduct.description, qRedeem.redeemTo, qRedeem.qty );

		if ( null == qty ) {
			query.having(
					qStamp.transactionHeader.totalTxnStamps.sum().goe( qRedeem.qty ) );
		}
		else {
			query.having(
					qStamp.transactionHeader.totalTxnStamps.sum().goe( qty ) );
		}

		List<StampPromoTxnDto> stampRedeems = query
			.list( ConstructorExpression.create( StampPromoTxnDto.class,
					qRedeem.promotion.id, qRedeem.promotion.description, qStamp.transactionHeader.storeCode, qStore.name, 
					qRedeem.sku, qProduct.description, qRedeem.redeemTo,
					qStamp.transactionHeader.totalTxnStamps.sum().divide( qRedeem.qty ) ) );

		return stampRedeems;
	}

	private Long retrieveRemainingStamps( String memberId, Long promoId ) {
		QStampTxnModel qStamp  = QStampTxnModel.stampTxnModel;
		QPromotionRedemption qRedeem = QPromotionRedemption.promotionRedemption;

		JPQLQuery query = new JPAQuery( em )
			.from( qStamp )
			.leftJoin( qStamp.stampPromotion.promotionRedemptions, qRedeem )
			.where( 
				BooleanExpression.allOf( 
					createRedeemFilter( memberId ),
					qRedeem.redeemFrom.loe( LocalDate.now() ),
					qRedeem.redeemTo.goe( LocalDate.now() ) ),
					qStamp.stampPromotion.id.eq( promoId ) )
			.groupBy( qStamp.stampPromotion.id );

		Double stampRedeems = query.singleResult( qStamp.transactionHeader.totalTxnStamps.sum() );

		return null != stampRedeems? stampRedeems.longValue() : 0L;
	}

	private BooleanExpression createRedeemFilter( String memberId ) {
		QStampTxnModel qStamp  = QStampTxnModel.stampTxnModel;
		return BooleanExpression.allOf( 
				qStamp.transactionHeader.memberModel.accountId.equalsIgnoreCase( memberId ),
				qStamp.stampPromotion.status.code.equalsIgnoreCase( codePropertiesService.getDetailStatusActive() ) );
				//qStamp.expiryDate.after( LocalDate.now() ), qStamp.transactionType.eq( StampTxnType.EARN ) );
	}



	@Override
	public boolean callPortaStampPromoWs( Object postData, Object returnObj, AppKey appKey ) {
        ApplicationConfig config = appConfigRepo.findByKey( appKey );
        if ( null == config || StringUtils.isBlank( config.getValue() ) ) {
            LOGGER.info("[" + appKey + "][INFO][PORTA_URL] = not configured " );
        	return false;
        }

		String PORTA_WS_URL = config.getValue(); //http://<host>:<port>/api/porta/stamp/createPromo
        LOGGER.info("[" + appKey + "][INFO][PORTA_URL] = " + PORTA_WS_URL );
		try {
			URL url = new URL(PORTA_WS_URL);
			HttpURLConnection conn = (HttpURLConnection) url.openConnection();

			int socketTimeout = Integer.parseInt( AppConfigDefaults.DEFAULT_WS_SOCKET_TIMEOUT );
			ApplicationConfig socketTimeoutConfig = appConfigRepo.findByKey(AppKey.WS_SOCKET_TIMEOUT);
			if ( null != socketTimeoutConfig ) {
				try {
					socketTimeout = Integer.parseInt( socketTimeoutConfig.getValue() );
				} 
				catch ( NumberFormatException nfe ) {}
			}

			int readTimeout = Integer.parseInt( AppConfigDefaults.DEFAULT_WS_READ_TIMEOUT );
			ApplicationConfig readTimeOutConfig = appConfigRepo.findByKey( AppKey.WS_READ_TIMEOUT );
			if ( null != readTimeOutConfig ) {
				try {
					readTimeout = Integer.parseInt( readTimeOutConfig.getValue() );
				} 
				catch ( NumberFormatException nfe ) {}
			}

			conn.setConnectTimeout(socketTimeout);
			conn.setReadTimeout( readTimeout );
			conn.setRequestMethod( "POST" );
			conn.setRequestProperty( "Accept", "application/json" );
			conn.setDoOutput( true );
			new PrintWriter( conn.getOutputStream() ).print( postData ); //sends post data

			if ( conn.getResponseCode() != 200 ) {
				return false;
			}

			BufferedReader br = new BufferedReader( new InputStreamReader( ( conn.getInputStream() ) ) );
			returnObj = new ObjectMapper().readValue( br, Object.class );
			Object ret = new Object();
			try {
				BeanUtils.copyProperties( ret, returnObj );
			} 
			catch ( IllegalAccessException e ) {
				LOGGER.error("[" + appKey + "][ERROR][COPYING BEAN1] message=  " + e);
			} 
			catch ( InvocationTargetException e ) {
				LOGGER.error("[" + appKey + "][ERROR][COPYING BEAN2] message=  " + e);
			}

			conn.disconnect();
			return true;
	    } 
		catch ( MalformedURLException e) {
	        LOGGER.info("[" + appKey + "][ERROR][MalformedURLException] ERROR_MESSAGE=  " + e.getMessage()+ "  urlStr: " + PORTA_WS_URL );
	    } 
		catch ( SocketTimeoutException e ) {
	        LOGGER.info("[" + appKey + "][ERROR][SocketTimeoutException] ERROR_MESSAGE=  " + e.getMessage()+ "  urlStr: " + PORTA_WS_URL );
	    } 
		catch ( IOException e ) {
	        LOGGER.info("[" + appKey + "][ERROR][IOException] ERROR_MESSAGE=  " + e.getMessage()+ "  urlStr: " + PORTA_WS_URL );
	    }

		return false;
	}


    @Override
    public boolean callPortaStampAccount(StampAccountPortaRestDto stampDto) {
        Object returnObj = new Object();
        return this.callPortaStampPromoWs(stampDto, returnObj, AppKey.PORTA_URL_STAMP_ACCOUNT);
    }
}
