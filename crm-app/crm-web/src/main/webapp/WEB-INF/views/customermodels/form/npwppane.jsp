<%@ include file="../../common/taglibs.jsp" %>

<style type="text/css">
<!--
  .input-npwp-1 {
    width: 18px;
  }
  .input-npwp-2 {
    width: 26px;
  }
  .input-npwp-3 {
    width: 34px;
  }
-->
</style>

<div class="form-horizontal">

<div class="control-group">
  <spring:message code="label_com_transretail_crm_entity_customermodel_npwpid" var="npwpId" />
  <label for="npwpId" class="control-label"><c:out value="${npwpId}:" /></label> 
  <div class="controls">
        <input type="text" maxlength="2" name="npwpId1" id="npwpId1" class="form-control npwp input-npwp-2"
          placeholder="##" /> . <input type="text" maxlength="3" name="npwpId2" id="npwpId2" class="form-control npwp input-npwp-3"
          placeholder="###" /> . <input type="text" maxlength="3" name="npwpId3" id="npwpId3" class="form-control npwp input-npwp-3"
          placeholder="###" /> . <input type="text" maxlength="1" name="npwpId4" id="npwpId4" class="form-control npwp input-npwp-1"
          placeholder="#" /> - <input type="text" maxlength="3" name="npwpId5" id="npwpId5" class="form-control npwp input-npwp-3"
          placeholder="###" /> . <input type="text" maxlength="3" name="npwpId6" id="npwpId6" class="form-control npwp input-npwp-3"
          placeholder="###" />
  </div>
    <input type="hidden" name="npwpId" id="npwpId" class="form-control"
      placeholder="${npwpId}" />
</div>
<div class="control-group">
  <spring:message code="label_com_transretail_crm_entity_customermodel_npwpname" var="npwpName" />
  <label for="npwpName" class="control-label"><c:out value="${npwpName}:" /></label> 
  <div class="controls">
    <input type="text" name="npwpName" id="npwpName" class="form-control"
      placeholder="${npwpName}" />
  </div>
</div>

<div class="control-group">
  <spring:message code="label_com_transretail_crm_entity_customermodel_npwpaddress" var="npwpAddress" />
  <label for="npwpAddress" class="control-label"><c:out value="${npwpAddress}:" /></label> 
  <div class="controls">
    <input type="text" name="npwpAddress" id="npwpAddress" class="form-control"
      placeholder="${npwpAddress}" />
  </div>
</div>

</div>

<script>
$("input.npwp").keyup(function () {
  if (/\D/g.test(this.value)) this.value = this.value.replace(/\D/g, '');
  if(!$(this).is(':last-child') && $(this).val().length == $(this).attr("maxlength")) $(this).next().focus();
});
</script>