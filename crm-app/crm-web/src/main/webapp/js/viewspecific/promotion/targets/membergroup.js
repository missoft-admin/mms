$(document).ready( function() {
	$targetMemberGroups = $("form[name='promotion']").find("select[name='targetMemberGroups']");
	$modalTargetMemberGroups = $("#targetMemberGroupsDialog").find("select[name='targetMemberGroups']");
	$modalMemberGroups = $("select[name='memberGroups']");
	$memberGroupsModal = $("#targetMemberGroupsDialog");
	$memberGroupModalContainer = $("#memberGroupContainer");
	
	initDialogs();
	
	initButtons();
	
	$("#promotionForm").submit(function() {
		$("form[name='promotion']").find("select[name='targetMemberGroups'] > option").prop("selected", "selected");
	});
	
	refreshMemberGroups();
	
	function refreshMemberGroups() {
		$.get($memberGroupsModal.data("contextPath") + "/groups/member/list", function(data) {
			var inReturnEntity = data.result;
			$modalMemberGroups.empty();
			$.each(inReturnEntity, function(key, value) {
				var val = value.id;
				var label = value.name;
				if($modalTargetMemberGroups.find("option[value='"+val+"']").length == 0)
					$("<option>").attr("value", val).text(label).appendTo($modalMemberGroups);
			});	
		}, "json");
	}
	
	function initDialogs() {
		$memberGroupsModal.modal({
			show : false
		});
		
		$memberGroupsModal.on('show.bs.modal', openGroupDialogAction);
	}
	
	function initButtons() {
		$("#memberGroupSaveButton").click(function(e) {
			e.preventDefault();
			
			$targetMemberGroups.empty();
			
			$modalTargetMemberGroups.find("option").clone().appendTo($targetMemberGroups);

			hideAndResetModal();
		});
		
		$("#memberGroupCancelButton").click(function(e) {
			e.preventDefault();
			hideAndResetModal();
		})
		
		$(".memberGroupLnk").click(function(e) {
			e.preventDefault();
			showGroupsDialog();
		});

		$( "#selectMemberGroups" ).dblclick( function(e) {
			$( "#addMemberGrp" ).trigger( "click" );
		});

		$( "#selectedMemberGroups" ).dblclick( function(e) {
			$( "#removeMemberGrp" ).trigger( "click" );
		});
		
		$("#addMemberGrp").click(function(e) {
			e.preventDefault();
			$modalMemberGroups.find("option:selected").each(function() {
				$modalTargetMemberGroups.append($(this));
			});
		});
		
		$("#removeMemberGrp").click(function(e) {
			e.preventDefault();
			$modalTargetMemberGroups.find("option:selected").each(function() {
				$modalMemberGroups.append($(this));
			});
		});
		
		$("#createMemberGrpLnk").click(function(e) {
			e.preventDefault();
			$.get($(this).attr("href"), function(data) {
				$memberGroupModalContainer.html(data);
			}, "html").done(function(data) {
				initCreateDialog();
				
				$memberGroupsModal.modal("hide");
				$memberGroupModalContainer.find("#itemDialog").modal();
				
				$memberGroupModalContainer.find("#itemDialog").on('hide.bs.modal', function () {
					showGroupsDialog();
				});
			});
		});
	}
	
	function initCreateDialog() {
		var CONTAINER_ERROR = "#contentError > div";
		var CONTENT_ERROR = "#contentError";
		
		$('#addField').unbind("click").click(function() {
			addNewField();
		});
		
		saveGroup('/new');
		
		function saveGroup(controllerPath) {
			$("#button_save").unbind("click").click(function(){
				$.ajax({
					type	: 'POST',
					contentType: 'application/json',
					url		: mainForm.attr("action") + controllerPath,
					dataType: 'json',
					data	: parseData(),
					success	: function(inResponse) {
						if (inResponse.success) {
							groupNameInput.val('');
							fieldsForm.empty();
							$memberGroupModalContainer.find("#itemDialog").modal("hide");
							refreshMemberGroups();
						} 
						else {
							errorInfo = "";
							for (i = 0; i < inResponse.result.length; i++) {
								errorInfo += "<br>" + (i + 1) + ". "
										+ ( inResponse.result[i].code != undefined ? 
												inResponse.result[i].code : inResponse.result[i]);
							}
							$( CONTAINER_ERROR ).html("Please correct following errors: " + errorInfo);
							$( CONTENT_ERROR ).show('slow');
						}
					}
				});
			});
		}
	}
	
	function hideAndResetModal() {
		$modalTargetMemberGroups.empty();
		refreshMemberGroups();
		$memberGroupsModal.modal("hide");
	}
	
	function showGroupsDialog() {
		
		$memberGroupsModal.modal("show");
	}
	
	function openGroupDialogAction() {
		$targetMemberGroups.find("option").each(function() {
			$modalMemberGroups.find("option[value='"+$(this).val()+"']").appendTo($modalTargetMemberGroups);
		});
		refreshMemberGroups();
	}



	initViewGroup();
	function initViewGroup() {

		var $link_viewGroup = $( "#link_viewMemberGroup" );
		var $url_viewGroup = $link_viewGroup.data( "url-view" );
		$link_viewGroup.attr( "href", "#" );

		$memberGroupsModal.on( "show.bs.modal", function() {

			$link_viewGroup.click( function(e) {
				e.preventDefault();
				$.get( 
					$link_viewGroup.data( "url-view" ), 
					function( data ) {
						$( "#viewMemberGroup" ).html( data );
					}, 
					"html" );
			});

			$( "select[name='memberGroups']" ).change( function(e) {
				$link_viewGroup.show();
				$link_viewGroup.data( "url-view", $url_viewGroup + $(this).val() );
			});
		});
		$memberGroupsModal.on( "hide.bs.modal", function() {
			$( "#viewMemberGroup" ).empty();
		});
	}
	
	
	
});