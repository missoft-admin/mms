create table CRM_LC_ANNUAL_FEE (
        id number(19,0) not null,
        CREATED_BY varchar2(255 char),
        CREATED_DATETIME timestamp,
        LAST_UPDATED_BY varchar2(255 char),
        LAST_UPDATED_DATETIME timestamp,
        ANNUAL_FEE double precision,
        REPLACEMENT_FEE double precision,
        CARD_TYPE varchar2(15 char),
        COMPANY varchar2(15 char),
        primary key (id)
    );
    
    alter table CRM_LC_ANNUAL_FEE 
        add constraint FK_gqo3cfkega6qyigiydl4uyjol 
        foreign key (CARD_TYPE) 
        references CRM_REF_LOOKUP_DTL;

    alter table CRM_LC_ANNUAL_FEE 
        add constraint FK_9gf9pnco0rn6b07bfrhgtkj0f 
        foreign key (COMPANY) 
        references CRM_REF_LOOKUP_DTL;
        
INSERT INTO CRM_USER_PERMISSION(CODE, DESCRIPTION, STATUS,ROLE_CATEGORY) VALUES ('LC_ANNUAL_FEE_SCHEME','Manage loyalty card annual fee scheme', 'ACTIVE','ROLE_MERCHANT_SERVICE,ROLE_MERCHANT_SERVICE_SUPERVISOR,ROLE_STORE_MANAGER');