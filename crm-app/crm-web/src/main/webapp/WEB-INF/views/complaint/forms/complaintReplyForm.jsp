<%@include file="../../common/taglibs.jsp" %>


<div id="content_complaintReplyForm" class="modal hide  nofly modal-dialog">

  <div class="modal-content">
  <c:url var="url_reply" value="/customer/complaint/process/reply/${complaintId}" />
  <form:form id="complaintReplyForm" name="complaintReply" modelAttribute="reply" method="POST" action="${url_reply}" class="modal-form form-horizontal">
    <div class="modal-header">
      <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
      <h4><spring:message code="complaint_lbl_reply" /></h4>
    </div>

    <div class="modal-body">
      <div class="hide alert alert-error" id="content_error"><button type="button" class="close" onclick="$(this).parent().hide();">&times;</button><div><form:errors   path="*"/></div></div>

      <c:if test="${not empty complaint.email && not empty complaint.mobileNo}">
      <div class="msgType control-group">
        <label for="msgType" class="control-label"></label>
        <div  class="controls">
          <c:if test="${not empty complaint.email}">
            <input id="emailBtn" class="btn replyBtn btn-primary" type="button" 
              value="<spring:message code="complaint_lbl_replyemail" />" data-is-email="true" data-reply-type="${emailReply}" data-for="replyToEmail" />
          </c:if>
          <c:if test="${not empty complaint.mobileNo}">
            <input id="mobileNoBtn" class="btn replyBtn btn-primary" type="button" 
              value="<spring:message code="complaint_lbl_replymobile" />" data-reply-type="${smsReply}" data-for="replyToMobile" />
          </c:if>
        </div>
      </div>
      </c:if>

      <c:if test="${not empty complaint.email && not empty complaint.mobileNo}"><c:set var="hide" value="hide" /></c:if>
      <div id="replyTo" class="control-group ${hide} replyTo">
        <label for="msgType" class="control-label"><spring:message code="complaint_lbl_replyto" /></label>
        <c:if test="${not empty complaint.email}">
          <div id="replyToEmail" class="controls ${hide} replyToVal">
            <div>${complaint.email}</div>
          </div>
          <c:set var="replyType" value="${emailReply}" />
        </c:if>
        <c:if test="${not empty complaint.mobileNo}">
          <div id="replyToMobile"class="controls ${hide} replyToVal"><div>${complaint.mobileNo}</div></div>
          <c:set var="replyType" value="${smsReply}" />
        </c:if>
        <c:if test="${not empty complaint.email && not empty complaint.mobileNo}"><c:set var="replyType" value="" /></c:if>
        <input type="hidden" id="msgType" name="msgType" value="${replyType}" />
      </div>

      <div class="message control-group">
        <label for="message" class="control-label">
          <b class="required">*</b> 
          <spring:message code="complaint_lbl_message"/>
          (<span id="emailCount">${maxCount}</span><span id="smsCount"></span>)
        </label>
        <div  class="controls">
          <form:textarea id="message" path="message" data-max-count="${maxCount}" data-per-count="${perCount}" />
        </div>
      </div>
    </div>

    <div class="modal-footer">
      <!-- <input id="sendBtn" class="btn btn-primary" type="submit" value="<spring:message code="label_send" />" />
      <input id="cancelBtn" class="btn" type="button" value="<spring:message code="label_cancel" />" data-dismiss="modal" /> -->
      <button type="button" id="sendBtn" class="btn btn-primary"><spring:message code="label_submit" /></button>
      <button type="button" id="cancelBtn" data-dismiss="modal" class="btn btn-default"><spring:message code="label_cancel"/></button>
    </div>
  </form:form>
  </div>


<style type="text/css"> 
<!-- 
  .clearfix { margin-top: 20px !important; }
  .modal-footer { text-align: center; } 
  .control-label { width: 100px !important; } 
  .controls { margin-left: 120px !important; } 
  .controls div { padding-top: 4px !important; }
--> 
</style>
<script type="text/javascript">
var ComplaintReplyForm = null;

$(document).ready( function() {

    var errorContainer = $( "#content_complaintReplyForm" ).find( "#content_error" );

    var $dialog = $( "#content_complaintReplyForm" );
    var $form = $dialog.find( "#complaintReplyForm" );
    var $save = $dialog.find( "#sendBtn" );
    var $message = $dialog.find( "#message" );
    //var $dispDate = $dialog.find( "#content_dateTime > .controls > div" );

    initDialog();
    initFormFields();

    function initDialog() {
        $dialog.modal({ show : false });
        $dialog.on( "hide", function(e) { 
            if ( e.target === this ) { 
                errorContainer.hide();
                ComplaintCommon.reset( $form );
            }
        });
    }

    function initFormFields() {
        $save.click( function(e) {
            e.preventDefault();
            $form.submit();
        });

        $form.submit( function(e) {
            $save.prop( "disabled", true );
            e.preventDefault();
            $.post( $form.attr( "action" ), $form.serialize(), function( resp ) { 
                ComplaintCommon.processResp( resp, $dialog, errorContainer, $save ); 
                ComplaintList.reloadTable();
            }, "json" );
        });

        $dialog.find( ".replyBtn" ).click( function(e) {
        	  e.preventDefault();
        	  $dialog.find( "#msgType" ).val( $(this).data( "reply-type" ) );
            $save.prop( "disabled", false );
        	  $dialog.find( "#replyTo" ).show();
        	  $dialog.find( ".replyToVal" ).hide();
        	  $dialog.find( "#" + $(this).data( "for" ) ).show();
        	  initMessageField( $(this).data( "is-email" ) );
        });
        initMessageField( true );

        if ( !$dialog.find( "#msgType" ).val() ) {
        	  $save.prop( "disabled", true );
        }

        //$dispDate.html( ( $dispDate.html() )? new Date( $dispDate.html() - 0 ).customize( 0 ) : "" );
    }

    function initMessageField( isEmail ) {
        var maxCount = $message.data( "max-count" );
        var $messageCount = $dialog.find( isEmail? "#emailCount" : "#smsCount" );
        $messageCount.show();
        $dialog.find( !isEmail? "#emailCount" : "#smsCount" ).hide();
        $messageCount.html( maxCount );
	    	if ( isEmail ) {
            $message.unbind( "keyup" ).bind( "keyup", function(e) {
	              var length = $(this).val().length;
	              return ComplaintCommon.limitCharSize( e, length, maxCount, function() { 
	            	    $messageCount.html( maxCount - length );
	              });
            });
            $message.unbind( $.browser.opera ? "keypress" : "keydown" ).bind( $.browser.opera ? "keypress" : "keydown", function(e) {
	              var length = $(this).val().length;
	              return ComplaintCommon.limitCharSize( e, length, maxCount - 1, function() { 
	                  $messageCount.html( maxCount - length );
	              });
            });
	    	}
	    	else {
	    	    var perCount = $message.data( "per-count" );
	    	    $message.unbind( "keyup" ).bind( "keyup", function(e) {
		    	      var length = $(this).val().length;
		    	      return ComplaintCommon.limitCharSize( e, length, maxCount, function() { 
		    	          $messageCount.html( ( perCount * Math.ceil( length/perCount ) - length ) + "/" + Math.ceil( length/perCount ) );
		    	      });
	    	    });
	    	    $message.unbind( $.browser.opera ? "keypress" : "keydown" ).bind( $.browser.opera ? "keypress" : "keydown", function(e) {
		    	      var length = $(this).val().length;
		    	      return ComplaintCommon.limitCharSize( e, length, maxCount - 1, function() { 
		    	          $messageCount.html( ( perCount * Math.ceil( length/perCount ) - length ) + "/" + Math.ceil( length/perCount ) );
		    	      });
	    	    });
	    	}
        if ( ComplaintReplyForm ) {
        	  $message.trigger( "keyup" ).trigger( $.browser.opera ? "keypress" : "keydown" );
        }
    }

    ComplaintReplyForm = {
        show : function() { $dialog.modal( "show" ); }
    };
});
</script>


</div>