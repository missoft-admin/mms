<%@include file="/WEB-INF/views/common/taglibs.jsp" %>
<div class="form-horizontal">
  <div class="page-header page-header2"><h1><spring:message code="report.visit.frequency"/></h1></div>
  
  <div class="errorMessages error">&nbsp;</div>

  <div class="control-group">
    <label class="control-label"></label>
    <div class="controls">
      <select name="reportType" id="reportType">
        <c:forEach items="${reportTypes}" var="reportType">
          <option value="${reportType.code}"><spring:message code="report_type_${reportType.code}" /></option>
        </c:forEach>
      </select>
    </div>
  </div>
  

  <fieldset>
    <legend><spring:message code="report.filters.label"/></legend>
    <form id="filters">
      <div class="control-group">
        <label class="control-label"><spring:message code="report.visit.frequency.type" /></label>
        <div class="controls">
          <select name="standardReportType" id="standardReportType">
            <option value="customerCount"><spring:message code="report.visit.frequency.customer.count"/></option>
            <option value="purchaseCount"><spring:message code="report.visit.frequency.customer.purchase"/></option>
          </select>
        </div>
      </div>
      
      <div class="control-group">
        <label class="control-label"><span class="required">*</span> <spring:message code="label_date_from" /></label>
        <div class="controls">
          <input name="dateFrom" id="dateFrom" type="text" placeHolder="Date From" />
        </div>
      </div>
      
      <div class="control-group">
        <label class="control-label"><span class="required">*</span> <spring:message code="label_date_to" /></label>
        <div class="controls">
          <input name="dateTo" id="dateTo" type="text" placeHolder="Date To" />
        </div>
      </div>
      
      <div class="control-group customer">
        <label class="control-label"><spring:message code="member.registration.customer" /></label>
        <div class="controls">
          <select name="customer" id="customer" class="searchDropdown">
            <option value=""></option>
            <c:forEach items="${customers}" var="customer">
              <option value="${customer.key}">${customer.value}</option>
            </c:forEach>
          </select>
        </div>
      </div>
      
      <div class="control-group store">
        <label class="control-label"><spring:message code="member.registration.store" /></label>
        <div class="controls">
          <select name="store" id="store" class="searchDropdown">
            <option value=""></option>
            <c:forEach items="${stores}" var="store">
              <option value="${store.code}">${store.code} - ${store.name}</option>
            </c:forEach>
          </select>
        </div>
      </div>
    </form>
    <div class="control-group form-actions">
        <button id="view" type="button" data-loading-text="Loading..." class="btn btn-primary"><spring:message code="view"/></button>
    </div>
  </fieldset>

<jsp:include page="../../common/noDataFound.jsp"/>

</div>
<style>
  #templateName {
    width: 30%;
  }
</style>
<script src="<c:url value="/js/bootstrap/bootstrap-datepicker.js"/>"></script>
<link href="<c:url value="/css/bootstrap/datepicker.css" />" rel="stylesheet"/>

<script>
$(document).ready(function() {
  var standardReportType = $("#standardReportType");
  var errorMessagesContainer = $('div.errorMessages' );
  
  initFields();
  initButtons();
  
  function initFields() {
    
    standardReportType.change(function(e) {
  	  //Add this part if needed
    });
    
    $("#dateFrom").datepicker({
      autoclose : true,
      format : 'dd-mm-yyyy'
    });
    
    $("#dateTo").datepicker({
      autoclose : true,
      format : 'dd-mm-yyyy'
  	});
  }
  
  function initButtons() {
    $("#view").click(function(e) {
      $("#view").button("loading");
  	  var selectedReportType = "reportType=" + $("select[name='reportType']").val();
      var standardReportType = "&standardReportType=" + $("#standardReportType").val();
  	  var customerType = "&customerType=" + $("#customer").val();
  	  var dateFrom = "&dateFrom=" + $("#dateFrom").val();
  	  var dateTo  = "&dateTo=" + $("#dateTo").val();
  	  var store = "&store=" + $("#store").val();
  	  
  	  if ($("#customer").val() == "") 
  	    customerType = "all";
  	  
  	  var url = ctx + "/visit/frequency/print?"
  	      		+ selectedReportType
          		+ standardReportType
          		+ dateFrom
          		+ dateTo;
  	  
  	  if ($("#store").val() != "")
  	    url = url + store;
  	  
  	  
  	  if ($("#customer").val() != "")
  	    url = url + customerType;
  	  
  	  var validateUrl = ctx + "/visit/frequency/print/validate";
  	  var filters = $("#filters").serialize();
  	  
  	  $.post(validateUrl, filters, function(data) {
  	    if (data.success) {
      	  if ($("select[name='reportType']").val() == "pdf")
            window.open(url, '_blank', 'toolbar=0,location=0,menubar=0');
          else if ($("select[name='reportType']").val() == "excel")
            window.location.href = url;
      	} else {
      	  if (data.result[0] === "No Data Found") {
      	    $("#noData").modal('show');
      	  } else {
        	  var errorInfo = "1. " + data.result[0];
            for ( var i = 1; i < data.result.length; i++ ) {
                errorInfo += "<br>" + (i + 1) + ". " + data.result[i];
            }
            showErrorMessage(errorInfo);
      	  }
      	}
  	    $("#view").button("reset");
  	  });
    });
  }
    
  var showErrorMessage = function(message) {
    errorMessagesContainer.html('<div class="alert alert-error">' +
        '<button type="button" class="close" data-dismiss="alert">&times;</button>' +
        message + '</div>');
  };

  //Reset of fields. Not used for this page
  function resetFields() {
    $(".searchDropdown").val("");
  }
  
});
</script>