package com.transretail.crm.core.entity;

import java.io.Serializable;

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Table;

import org.hibernate.envers.Audited;

import com.transretail.crm.core.entity.enums.Status;
import com.transretail.crm.core.entity.support.CustomIdAuditableEntity;

/**
 * @author Allan G. Ramirez (agramirez@exist.com)
 */
@Audited
@Entity
@Table(name = "CRM_USER_PERMISSION")
@AttributeOverrides({@AttributeOverride(name = "id", column = @Column(name = "CODE"))})
public class UserPermission extends CustomIdAuditableEntity<String> implements Serializable {

    private static final long serialVersionUID = 3703377096844821711L;
    @Column(name = "DESCRIPTION", nullable = false)
    private String description;
    @Column(name = "STATUS", length = 8, nullable = false)
    @Enumerated(EnumType.STRING)
    private Status status = Status.ACTIVE;

    /**
     * Comma separated role codes
     */
    @Column(name = "ROLE_CATEGORY", length = 1000)
    private String roleCategory;
    /**
     * For ordering of roles
     */
    @Column(name = "ORDERING")
    private Integer ordering;

    public UserPermission() {

    }

    public UserPermission(String code) {
        setId(code);
    }

    public UserPermission(String code, String description) {
        setId(code);
        this.description = description;
    }

    public String getCode() {
        return getId();
    }

    public void setCode(String code) {
        setId(code);
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public String getRoleCategory() {
        return roleCategory;
    }

    public void setRoleCategory(String roleCategory) {
        this.roleCategory = roleCategory;
    }

    public Integer getOrdering() {
        return ordering;
    }

    public void setOrdering(Integer ordering) {
        this.ordering = ordering;
    }
    
}
