package com.transretail.crm.core.repo;


import org.springframework.stereotype.Repository;

import com.transretail.crm.common.repo.CrmQueryDslPredicateExecutor;
import com.transretail.crm.product.entity.CrmProductHier;


@Repository
public interface ProductHierRepo extends CrmQueryDslPredicateExecutor<CrmProductHier, String> {
}
