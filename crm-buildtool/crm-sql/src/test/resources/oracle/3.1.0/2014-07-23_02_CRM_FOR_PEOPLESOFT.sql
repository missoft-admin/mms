create table CRM_FOR_PEOPLESOFT (
        id number(19,0) not null,
        CREATED_BY varchar2(255 char),
        CREATED_DATETIME timestamp,
        LAST_UPDATED_BY varchar2(255 char),
        LAST_UPDATED_DATETIME timestamp,
        BU varchar2(255 char),
        DEPT_ID varchar2(255 char),
        MMS_ACCT varchar2(255 char),
        NOTES varchar2(255 char),
        PS_ACCT varchar2(255 char),
        PS_STORE_CODE varchar2(255 char),
        STORE_CODE varchar2(255 char),
        TXN_AMT number(19,2),
        TXN_DATE timestamp,
        TYPE varchar2(255 char),
        TXN_TYPE varchar2(255 char),
        primary key (id)
    );