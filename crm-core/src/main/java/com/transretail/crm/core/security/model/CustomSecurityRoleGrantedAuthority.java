/*
 * Copyright (c) 2012.
 * All rights reserved.
 */

package com.transretail.crm.core.security.model;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import com.transretail.crm.common.security.model.AbstractSecurityGrantedAuthority;

/**
 * @author mhua
 */
public class CustomSecurityRoleGrantedAuthority extends AbstractSecurityGrantedAuthority {

    private static final long serialVersionUID = 1L;

    protected final String authority;

    public CustomSecurityRoleGrantedAuthority(String authority) {
        this.authority = authority;
    }

    @Override
    public String getAuthority() {
        return authority;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this, ToStringStyle.SIMPLE_STYLE)
                .append("authority", authority)
                .toString();
    }

}
