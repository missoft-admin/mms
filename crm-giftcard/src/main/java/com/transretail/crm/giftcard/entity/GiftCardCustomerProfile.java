package com.transretail.crm.giftcard.entity;


import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import javax.validation.constraints.NotNull;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.hibernate.annotations.Type;
import org.joda.time.DateTime;

import com.transretail.crm.core.entity.lookup.LookupDetail;
import com.transretail.crm.core.entity.support.CustomAuditableEntity;
import com.transretail.crm.core.util.FormatterUtil;
import com.transretail.crm.giftcard.entity.support.CustomerProfileType;


@Entity
@Table(name = "CRM_GC_CX_PROFILE")
public class GiftCardCustomerProfile extends CustomAuditableEntity<Long> implements Serializable {

	private static final long serialVersionUID = 1L;

	@Column(name = "CX_ID", length=30, nullable = false)
	private String customerId;

	@Column(name="CX_TYPE")
	@Enumerated(EnumType.STRING)
	@NotNull
    private CustomerProfileType customerType;

	@Column(name = "NAME", nullable=false, length=100)
	private String name;

	@Column(name = "INVOICE_TITLE", nullable=false, length=100)
	private String invoiceTitle;

	@Column(name = "CONTACT_LNAME", length=50)
	private String contactFirstName;

	@Column(name = "CONTACT_FNAME", length=50)
	private String contactLastName;

	@JoinColumn(name="GENDER")
	@OneToOne
	private LookupDetail gender;

	@Column(name = "CONTACT_NO", length=20)
	private String contactNo;

	@Column(name = "COMPANY_PHONE_NO", length=20)
	private String companyPhoneNo;

	@Column(name = "FAX_NO", length=20)
	private String faxNo;

	@Column(name = "EMAIL", length=50)
	private String email;

	@Column(name = "MAIL_ADDRESS")
	private String mailAddress;

	@Column(name = "SHIPPING_ADDRESS")
	private String shippingAddress;

	@JoinColumn(name="DISCOUNT_TYPE")
	@OneToOne
    private LookupDetail discountType;

	@Column(name = "ORDER_NO")
    private String lastOrderNo;

    @Temporal(TemporalType.TIMESTAMP)
	@Column(name = "ORDER_DATE")
    @Type(type = "org.jadira.usertype.dateandtime.joda.PersistentDateTime")
    private DateTime lastOrderDate;

    @Column(name = "TOTAL_DISC_PURCHASE")
    private BigDecimal accumulatedDiscountedPurchase;

    @Column(name = "PAYMENT_STORE")
	private String paymentStore;

    public GiftCardCustomerProfile() {}

    public GiftCardCustomerProfile(Long id) {
    	setId(id);
    }

    public String getCustomerId() {
		return customerId;
	}

	public void setCustomerId(String customerId) {
		this.customerId = customerId;
	}

	public CustomerProfileType getCustomerType() {
		return customerType;
	}

	public void setCustomerType(CustomerProfileType customerType) {
		this.customerType = customerType;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getInvoiceTitle() {
		return invoiceTitle;
	}

	public void setInvoiceTitle(String invoiceTitle) {
		this.invoiceTitle = invoiceTitle;
	}

	public String getContactFirstName() {
		return contactFirstName;
	}

	public void setContactFirstName(String contactFirstName) {
		this.contactFirstName = contactFirstName;
	}

	public String getContactLastName() {
		return contactLastName;
	}

	public void setContactLastName(String contactLastName) {
		this.contactLastName = contactLastName;
	}

	public LookupDetail getGender() {
		return gender;
	}

	public void setGender(LookupDetail gender) {
		this.gender = gender;
	}

	public String getContactNo() {
		return contactNo;
	}

	public void setContactNo(String contactNo) {
		this.contactNo = contactNo;
	}

	public String getCompanyPhoneNo() {
		return companyPhoneNo;
	}

	public void setCompanyPhoneNo(String companyPhoneNo) {
		this.companyPhoneNo = companyPhoneNo;
	}

	public String getFaxNo() {
		return faxNo;
	}

	public void setFaxNo(String faxNo) {
		this.faxNo = faxNo;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getMailAddress() {
		return mailAddress;
	}

	public void setMailAddress(String mailAddress) {
		this.mailAddress = mailAddress;
	}

	public String getShippingAddress() {
		return shippingAddress;
	}

	public void setShippingAddress(String shippingAddress) {
		this.shippingAddress = shippingAddress;
	}

	public LookupDetail getDiscountType() {
		return discountType;
	}

	public void setDiscountType(LookupDetail discountType) {
		this.discountType = discountType;
	}

	public String getLastOrderNo() {
		return lastOrderNo;
	}

	public void setLastOrderNo(String lastOrderNo) {
		this.lastOrderNo = lastOrderNo;
	}

	public DateTime getLastOrderDate() {
		return lastOrderDate;
	}

	public void setLastOrderDate(DateTime lastOrderDate) {
		this.lastOrderDate = lastOrderDate;
	}

	public BigDecimal getAccumulatedDiscountedPurchase() {
		return accumulatedDiscountedPurchase;
	}

	public void setAccumulatedDiscountedPurchase(
			BigDecimal accumulatedDiscountedPurchase) {
		this.accumulatedDiscountedPurchase = accumulatedDiscountedPurchase;
	}

	@Transient
    public String getContactFullName() {
        return FormatterUtil.INSTANCE.formatName(contactLastName, contactFirstName);

    }

	public String getPaymentStore() {
		return paymentStore;
	}

	public void setPaymentStore(String paymentStore) {
		this.paymentStore = paymentStore;
	}


	@Override
	public boolean equals(Object o) {
		if (this == o) return true;

		if (o == null || getClass() != o.getClass()) return false;

		GiftCardCustomerProfile that = (GiftCardCustomerProfile) o;

		return new EqualsBuilder()
				.append(customerId, that.customerId)
				.isEquals();
	}

	@Override
	public int hashCode() {
		return new HashCodeBuilder(17, 37)
				.append(customerId)
				.toHashCode();
	}
}
