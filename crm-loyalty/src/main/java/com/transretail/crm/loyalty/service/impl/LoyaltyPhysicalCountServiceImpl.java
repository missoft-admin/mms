package com.transretail.crm.loyalty.service.impl;


import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;

import org.joda.time.LocalDateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.mysema.query.types.expr.BooleanExpression;
import com.mysema.query.types.expr.NumberExpression;
import com.transretail.crm.common.reporting.jasper.ClasspathFileResolver;
import com.transretail.crm.common.reporting.jasper.DefaultJRProcessor;
import com.transretail.crm.common.reporting.jasper.JRProcessor;
import com.transretail.crm.common.service.dto.request.PageSortDto;
import com.transretail.crm.common.service.dto.response.ResultList;
import com.transretail.crm.common.service.util.SpringDataPagingUtil;
import com.transretail.crm.core.security.model.impl.CustomSecurityUserDetailsImpl;
import com.transretail.crm.core.security.util.UserUtil;
import com.transretail.crm.core.service.CodePropertiesService;
import com.transretail.crm.loyalty.dto.LoyaltyPhysicalCountDto;
import com.transretail.crm.loyalty.dto.LoyaltyPhysicalCountSummaryDto;
import com.transretail.crm.loyalty.entity.LoyaltyPhysicalCount;
import com.transretail.crm.loyalty.entity.LoyaltyPhysicalCountSummary;
import com.transretail.crm.loyalty.entity.QLoyaltyCardInventory;
import com.transretail.crm.loyalty.entity.QLoyaltyPhysicalCount;
import com.transretail.crm.loyalty.entity.QLoyaltyPhysicalCountSummary;
import com.transretail.crm.loyalty.repo.LoyaltyCardInventoryRepo;
import com.transretail.crm.loyalty.repo.LoyaltyPhysicalCountRepo;
import com.transretail.crm.loyalty.repo.LoyaltyPhysicalCountSummaryRepo;
import com.transretail.crm.loyalty.service.LoyaltyCardService;
import com.transretail.crm.loyalty.service.LoyaltyPhysicalCountService;


@Service("loyaltyPhysicalCountService")
public class LoyaltyPhysicalCountServiceImpl implements LoyaltyPhysicalCountService {
	
	private static final String PARAM_LOCATION = "location";
	private static final String PARAM_IS_EMPTY = "isEmpty";
	private static final String REPORT_NAME = "reports/loyalty_physical_count.jasper";
	
	@Autowired
	LoyaltyPhysicalCountRepo countRepo;
	@Autowired
	LoyaltyPhysicalCountSummaryRepo summaryRepo;
	@Autowired
	LoyaltyCardInventoryRepo inventoryRepo;
	@Autowired
	LoyaltyCardService loyaltyCardService;
	@Autowired
	CodePropertiesService codePropertiesService;
	
	@Override
	public void savePhysicalCount(LoyaltyPhysicalCountDto countDto) {
		countRepo.save(countDto.toModel());
	}
	
	@Override
	public ResultList<LoyaltyPhysicalCountDto> listPhysicalCount(PageSortDto sortDto) {
		Pageable pageable = SpringDataPagingUtil.INSTANCE.createPageable(sortDto.getPagination());
		QLoyaltyPhysicalCount qCount = QLoyaltyPhysicalCount.loyaltyPhysicalCount;
		BooleanExpression filter = qCount.location.eq(getCurrentUserLocation());
		Page<LoyaltyPhysicalCount> page = countRepo.findAll(filter, pageable);
		return new ResultList<LoyaltyPhysicalCountDto>(convertToDto(page), page.getTotalElements(), page.hasPreviousPage(),
	            page.hasNextPage());
	}

	@Override
	public ResultList<LoyaltyPhysicalCountSummaryDto> getSummary(PageSortDto sortDto) {
		return countRepo.getSummary(sortDto);
	}
	
	@Override
	public void processInventory() {
		saveSummary();
		QLoyaltyPhysicalCount qCount = QLoyaltyPhysicalCount.loyaltyPhysicalCount;
		BooleanExpression filter = qCount.location.eq(getCurrentUserLocation());
		Iterable<LoyaltyPhysicalCount> counts = countRepo.findAll(filter);
		countRepo.processReceivedInventory(counts);
		countRepo.processMissingInventory(counts);
		countRepo.delete(counts);
	}
	
	private void saveSummary() { //record process history
		PageSortDto sortDto = new PageSortDto();
		sortDto.getPagination().setPageSize(-1); //to retrieve all results
		List<LoyaltyPhysicalCountSummary> summaryList = Lists.newArrayList();
		LocalDateTime created = new LocalDateTime();
		for(LoyaltyPhysicalCountSummaryDto summaryDto : getSummary(sortDto).getResults()) {
			summaryList.add(summaryDto.toModel(created));
		}
		summaryRepo.save(summaryList);
	}
	
	@Override
	public List<LocalDateTime> getSummaryDates() {
		return countRepo.getSummaryDates();
	}
	
	@Override
	public JRProcessor createJrProcessor() {
		PageSortDto sortDto = new PageSortDto();
		sortDto.getPagination().setPageSize(-1); //to retrieve all results
		ResultList<LoyaltyPhysicalCountSummaryDto> results = getSummary(sortDto);
		return createJrProcessor(results.getResults());
    }
	
	@Override
	public JRProcessor createJrProcessor(LocalDateTime date) {
		QLoyaltyPhysicalCountSummary qSummary = QLoyaltyPhysicalCountSummary.loyaltyPhysicalCountSummary;
		BooleanExpression filter = qSummary.created.eq(date);
		Iterable<LoyaltyPhysicalCountSummary> list = summaryRepo.findAll(filter);
		return createJrProcessor(convertSummaryToDto(list));
    }
	
	private List<LoyaltyPhysicalCountSummaryDto> convertSummaryToDto(Iterable<LoyaltyPhysicalCountSummary> list) {
		List<LoyaltyPhysicalCountSummaryDto> summaryList = Lists.newArrayList();
		for(LoyaltyPhysicalCountSummary summary: list) {
			summaryList.add(new LoyaltyPhysicalCountSummaryDto(summary));
		}
		
		return summaryList;
	}
	
	private JRProcessor createJrProcessor(Collection<LoyaltyPhysicalCountSummaryDto> list) {
		Map<String, Object> parameters = Maps.newHashMap();
		String userLocation = getCurrentUserLocation();
    	parameters.put(PARAM_LOCATION, userLocation + " - " + loyaltyCardService.getInventoryDesc(userLocation));
    	List<LoyaltyPhysicalCountSummaryDto> beansDataSource = Lists.newArrayList();
    	if (list.isEmpty()) {
            parameters.put(PARAM_IS_EMPTY, true);
            beansDataSource.add(new LoyaltyPhysicalCountSummaryDto());
        } else {
        	beansDataSource.addAll(list);
        }
        DefaultJRProcessor jrProcessor = new DefaultJRProcessor(REPORT_NAME, beansDataSource);
        jrProcessor.addParameters(parameters);
        jrProcessor.setFileResolver(new ClasspathFileResolver("reports"));
        return jrProcessor;
	}
	
	@Override
	public Boolean validateSeries(String startingSeries, String endingSeries) {
		Long starting = Long.valueOf(startingSeries);
		Long ending = Long.valueOf(endingSeries);
		
		String[] invalidStatus = new String[] {
				codePropertiesService.getDetailLoyaltyStatusActive(),
				codePropertiesService.getDetailLoyaltyStatusDisabled()
		};
		
		QLoyaltyCardInventory qInventory = QLoyaltyCardInventory.loyaltyCardInventory;
		NumberExpression<Long> sequenceExp = qInventory.sequence.castToNum(Long.class);
		Long count = inventoryRepo.count(sequenceExp.goe(starting).and(sequenceExp.loe(ending)).and(qInventory.status.code.notIn(invalidStatus)));
		if(count != (ending - starting + 1))
			return false;
		
		QLoyaltyPhysicalCount qCount = QLoyaltyPhysicalCount.loyaltyPhysicalCount;
		NumberExpression<Long> startingExp = qCount.startingSeries.castToNum(Long.class);
		NumberExpression<Long> endingExp = qCount.endingSeries.castToNum(Long.class);
		BooleanExpression filter = BooleanExpression.anyOf(startingExp.loe(starting).and(endingExp.goe(starting)), 
				startingExp.loe(ending).and(endingExp.goe(ending)),
				startingExp.goe(starting).and(startingExp.loe(ending)),
				endingExp.goe(starting).and(endingExp.loe(ending)));
		
		List<LoyaltyPhysicalCount> counts = Lists.newArrayList(countRepo.findAll(filter));
		
		if(counts.size() > 0)
			return false;
		
		return true;
	}
	
	private List<LoyaltyPhysicalCountDto> convertToDto(Iterable<LoyaltyPhysicalCount> models) {
		List<LoyaltyPhysicalCountDto> dtos = new ArrayList<LoyaltyPhysicalCountDto>();
		for(LoyaltyPhysicalCount model: models) {
			dtos.add(new LoyaltyPhysicalCountDto(model));
		}
		return dtos;
	}
	
	private String getCurrentUserLocation() {
		CustomSecurityUserDetailsImpl userDetails = UserUtil.getCurrentUser();
		return userDetails.getInventoryLocation();
	}
	
}
