package com.transretail.crm.schedule.job.impl;

import java.util.Date;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.transretail.crm.core.service.PointsTxnManagerService;
import com.transretail.crm.giftcard.service.GiftCardInventoryService;
import com.transretail.crm.loyalty.service.LoyaltyCardService;
import com.transretail.crm.schedule.job.service.ExpireJobService;

@Service("expireJobService")
public class ExpireJobServiceImpl implements ExpireJobService {
	@Autowired
	private PointsTxnManagerService pointsTxnManagerService;
	@Autowired
	private LoyaltyCardService loyaltyCardService;
	@Autowired
	private GiftCardInventoryService gcInvService;
	private static final Logger LOGGER = LoggerFactory.getLogger(ExpireJobServiceImpl.class);

	@Override
	@Transactional
	public void expirePoints() {
		LOGGER.error( ":expirePoints:: job executing at " + new Date().toString()  );
		pointsTxnManagerService.expirePoints();
		loyaltyCardService.expirePoints();
		gcInvService.expireGiftCards();
		LOGGER.error( ":expirePoints:: job successfully executed at " + new Date().toString()  );
	}

}
