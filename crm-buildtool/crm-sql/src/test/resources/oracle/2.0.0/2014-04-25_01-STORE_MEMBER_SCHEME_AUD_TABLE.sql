create table CRM_STORE_MEMBER_AUD (
	id number(19,0) not null,
	REV number(19,0) not null,
	REVTYPE number(3,0),
	STORE varchar2(10 char) not null,
    COMPANY varchar2(10 char),
    CARDTYPE varchar2(10 char),
    REMARKS varchar2(255 char),
    primary key (id, REV)
);