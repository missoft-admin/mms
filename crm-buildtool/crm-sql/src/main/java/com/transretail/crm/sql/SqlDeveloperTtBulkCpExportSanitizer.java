package com.transretail.crm.sql;

import java.io.BufferedInputStream;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;
import java.util.regex.Pattern;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;

import com.google.common.collect.Lists;

/**
 * Removes illegal characters in dump file generated from sqldeveloper via ttbulkcp
 * <p>
 * Note: Bullet 7 of http://docs.oracle.com/cd/E15846_01/doc.21/e15859/toc.htm#CJABFBHF should be performed to have a timesten compatible dump
 * </p>
 * Sample column headers generated from sqldeveloper Version 3.2.20.09 via ttbulkcp format.
 * # 19. SUBDISTRICT
 * # 20. VILLAGE
 * Modify this if the output is not the same
 *
 * @author Allan G. Ramirez (agramirez@exist.com)
 */
public class SqlDeveloperTtBulkCpExportSanitizer {
    private static final Pattern PATTERN_TT_HEADERS =
        Pattern.compile("^##ttBulkCp.*|^#Generated.*|^#end.*|^# \\d\\.\\s.*|^# \\d\\d\\.\\s.*");
    private static final String SPLIT_REGEX = ",(?=([^\"]*\"[^\"]*\")*[^\"]*$)";

    private String absoluteFileName;

    public SqlDeveloperTtBulkCpExportSanitizer(String absoluteFileName) {
        this.absoluteFileName = absoluteFileName;
    }

    public static void main(String[] args) throws Exception {
        Scanner in = new Scanner(System.in);
        System.out.print("Enter dump path ( user.home/export.dump          : ");
        System.out.print("Enter sanitized dump path : ");

        final FileWriter writer =
            new FileWriter("/Users/aramirez/export-clean.dump");
        try {
            SqlDeveloperTtBulkCpExportSanitizer helper =
                new SqlDeveloperTtBulkCpExportSanitizer("/Users/aramirez/export.dump");
            helper.sanitize(new LineWriterDelegate() {
                @Override
                public void write(String line) throws Exception {
                    writer.write(line + "\r\n");
                }
            });
        } finally {
            IOUtils.closeQuietly(writer);
        }
    }

    public synchronized void sanitize(LineWriterDelegate delegate) throws Exception {
        FileInputStream is = null;
        try {
            is = new FileInputStream(absoluteFileName);
            Scanner scanner = new Scanner(is);
            parse(scanner, delegate);
        } finally {
            if (is != null) {
                is.close();
            }
        }
    }

    private void parse(Scanner scanner, LineWriterDelegate delegate) throws Exception {
        String lastHeader = null;
        String lastLine = null;
        // Parse Headers
        Integer numberOfColumns = null;
        while (scanner.hasNext()) {
            String str = scanner.nextLine();
            if (PATTERN_TT_HEADERS.matcher(str).matches()) {
                lastHeader = str;
                delegate.write(str);
                continue;
            }
            lastLine = str;
            numberOfColumns = Integer.valueOf(lastHeader.substring(lastHeader.indexOf(" ") + 1, lastHeader.indexOf('.')));
            break;
        }
        if (lastHeader != null) {
            // Parse first line
            parseData(scanner, delegate, numberOfColumns, lastLine);
            while (scanner.hasNext()) {
                // Parse remaining lines
                parseData(scanner, delegate, numberOfColumns, scanner.nextLine());
            }
        }
    }

    private void parseData(final Scanner scanner, final LineWriterDelegate delegate, final int numberOfColumns,
        final String initialLineToParse) throws Exception {
        if (StringUtils.isNotBlank(initialLineToParse)) {
            String result = "";
            String line = initialLineToParse;
            List<String> list = Arrays.asList(line.split(SPLIT_REGEX));
            if (list.size() == numberOfColumns) {
                for (String value : list) {
                    result += value.replace("\n", "").replace("\r", "");
                    result += ",";
                }
            } else {
                int i = numberOfColumns;
                ResultTuple tuple = parseBrokenLineString(line, false);
                for (String value : tuple.data) {
                    result += value.replace("\n", "").replace("\r", "");
                    result += ",";
                    i--;
                }

                while (i > 0) {
                    // Should never happen
                    if (!scanner.hasNext()) {
                        throw new RuntimeException("Failed to parse " + numberOfColumns + " data. Maybe the file is corrupted");
                    }
                    result = result.substring(0, result.length() - 1);
                    line = scanner.nextLine();
                    tuple = parseBrokenLineString(line, tuple.lastDataIsString);

                    for (String value : tuple.data) {
                        result += value.replace("\n", "").replace("\r", "");
                        result += ",";
                        if (!tuple.lastDataIsString) {
                            i--;
                        }
                    }
                }
            }
            result = result.substring(0, result.length() - 1);
            delegate.write(result);
        }
    }

    private ResultTuple parseBrokenLineString(final String line, boolean firstDataIsString) {
        ResultTuple result = new ResultTuple();
        String remainingLine = line;
        if (firstDataIsString) {
            if (remainingLine.indexOf("\",") != -1) {
                int idx = remainingLine.indexOf("\",");
                result.data.add(remainingLine.substring(0, idx + 1));
                remainingLine = remainingLine.substring(idx + 2);
                firstDataIsString = false;
            } else if (remainingLine.indexOf('"') != -1) {
                int idx = remainingLine.indexOf('"');
                result.data.add(remainingLine.substring(0, idx + 1));
                remainingLine = "";
                firstDataIsString = false;
            } else {
                int idx = remainingLine.length();
                result.data.add(remainingLine.substring(0, idx));
                remainingLine = "";
            }
        }

        result.lastDataIsString = firstDataIsString;

        while (remainingLine.length() > 0) {
            if (remainingLine.charAt(0) == '"') {
                result.lastDataIsString = true;
                remainingLine = remainingLine.substring(1);
                if (remainingLine.indexOf("\",") != -1) {
                    int idx = remainingLine.indexOf("\",");
                    result.data.add("\"" + remainingLine.substring(0, idx + 1));
                    remainingLine = remainingLine.substring(idx + 2);
                } else if (remainingLine.indexOf('"') != -1) {
                    int idx = remainingLine.indexOf('"');
                    result.data.add("\"" + remainingLine.substring(0, idx + 1));
                    remainingLine = "";
                } else {
                    result.data.add("\"" + remainingLine.substring(0, remainingLine.length()));
                    remainingLine = "";
                }
            } else {
                int idx = remainingLine.indexOf(',');
                if (idx != -1) {
                    result.data.add(remainingLine.substring(0, idx));
                    remainingLine = remainingLine.substring(idx + 1);
                } else {
                    result.data.add(remainingLine.substring(0, remainingLine.length()));
                    remainingLine = "";
                }
            }
        }
        return result;
    }

    private class ResultTuple {
        boolean lastDataIsString;
        List<String> data = Lists.newArrayList();
    }
}
