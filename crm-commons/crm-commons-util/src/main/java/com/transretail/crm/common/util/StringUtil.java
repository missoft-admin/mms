package com.transretail.crm.common.util;


public class StringUtil {


    /**
     *
     * trims employee id. Employee id should only have 11 digits. However, data from barcode will return 12 digits
     *  where last digit is the checked number.
     *
     *  if input length is other than 12 then ignore
     *
     *
     * @param input
     * @return
     */
    public static String trimEmpId(String input) {

        int length = 11;

        if (input != null && input.length() == 12 && input.startsWith("00")) {
            return input.substring(0,length);

        } else {
            return input;
        }

    }


    /**
     *
     * checks if input string is part of filter string. also handles wildcard.
     * checking is done by comparing the first 6 digits of the input or via wildcard comparison
     *
     * ie.  matches("512346", "5*") - check if input is a mastercard
     *
     * usage is for checking if card number matches the filter input
     *
     * @param input
     * @param filter
     * @param charRange - length of characters to be tested and compared against
     * @return
     */
    public static boolean  matches(String input, String filter, int charRange) {


        if (input == null || input.length() < filter.length() || input.length() < charRange) {
            //input should always be greater than the filter size
            return false;
        }

        //check if first six char of input matches the filter assuming there is no wildcard char
        if (filter.length() == charRange && !filter.contains("*")) {
            return  input.startsWith(filter);
        }

        //check if filter  has wildcard and filter length matches charRange
        //checking of match will be done per char. Corresponding wildcard char in input will accept all chars
        if (filter.length() == charRange && filter.contains("*")) {
            char[] filterChars =  filter.toCharArray();
            char[] inputChars = input.substring(0, charRange).toCharArray();

            for (int i=0; i < charRange; ++i) {
                //compare chars that are not wildcard
                if (filterChars[i] != '*') {
                    if (filterChars[i] != inputChars[i]) {
                             return false;
                    }
                }

            }

            return  true;
        }

        //if filter length is < charRange then it will return false if no wildcard is defined
        //only single wildcard is supported for filter with length less than charRange
        if (filter.length() < charRange) {
            boolean match = true;

            //check for wildcard the end
            char[] filterChars =  filter.toCharArray();
            char[] inputChars = input.substring(0, charRange).toCharArray();

            if (filter.endsWith("*")) {
                //check for filter where wildcard is place at the end
                for (int i=0; i < filterChars.length; ++i) {
                    //compare chars that are not wildcard
                    if (filterChars[i] != '*') {
                        if (filterChars[i] != inputChars[i]) {
                            match = false;
                            break;
                        }
                    } else {
                        break;
                    }
                }
            } else if (filter.startsWith("*")) {
                //check for wildcard at front
                int filterIndex = filter.length() - 1;
                for (int i=charRange - 1; i > 0; --i) {
                    //compare chars that are not wildcard
                    if (filterChars[filterIndex] != '*') {
                        if (filterChars[filterIndex] != inputChars[i]) {
                            match = false;
                            break;
                        }
                    } else {
                        break;
                    }
                    --filterIndex;
                }



            } else {
                //wildcard in between is not supported unless filter is of exact length
                match = false;

            }


            return match;


        }


        return false;

    }


    public static boolean  matches(String input, String filter) {


        return matches(input, filter, 6);
    }


    public static boolean isEmpty(String str) {

        return str == null || str.trim().length() == 0;
    }


}
