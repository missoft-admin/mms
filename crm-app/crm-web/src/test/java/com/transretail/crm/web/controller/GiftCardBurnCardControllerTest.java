package com.transretail.crm.web.controller;

import static org.hamcrest.Matchers.*;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.MediaType;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.AnnotationConfigWebContextLoader;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

import com.transretail.crm.common.reporting.service.ReportService;
import com.transretail.crm.common.service.dto.request.PagingParam;
import com.transretail.crm.common.service.dto.response.ResultList;
import com.transretail.crm.core.service.CodePropertiesService;
import com.transretail.crm.core.service.StoreService;
import com.transretail.crm.giftcard.dto.GiftCardBurnCardDto;
import com.transretail.crm.giftcard.dto.GiftCardBurnCardSearchDto;
import com.transretail.crm.giftcard.dto.GiftCardInventorySeriesSearchDto;
import com.transretail.crm.giftcard.repo.GiftCardBurnCardRepo;
import com.transretail.crm.giftcard.repo.GiftCardInventoryStockRepo;
import com.transretail.crm.giftcard.service.GiftCardBurnCardService;
import com.transretail.crm.giftcard.service.GiftCardInventoryService;
import com.transretail.crm.giftcard.service.GiftCardInventoryStockService;
import com.transretail.crm.report.template.impl.GiftCardBurnedDocument;

/**
 * @author ftopico
 */
@RunWith(SpringJUnit4ClassRunner.class)
@WebAppConfiguration
@ContextConfiguration(loader = AnnotationConfigWebContextLoader.class)
public class GiftCardBurnCardControllerTest extends AbstractMockMvcTest {

    @Configuration
    @EnableWebMvc
    static class Config {
        @Bean
        public GiftCardBurnCardController giftCardBurnCardController() {
            return new GiftCardBurnCardController();
        }
        
        @Bean
        public GiftCardInventoryService giftCardInventoryService() {
            return Mockito.mock(GiftCardInventoryService.class);
        }
        
        @Bean
        public GiftCardInventoryStockService giftCardInventoryStockService() {
            return Mockito.mock(GiftCardInventoryStockService.class);
        }
        
        @Bean
        public GiftCardBurnCardService giftCardBurnCardService() {
            return Mockito.mock(GiftCardBurnCardService.class);
        }
        
        @Bean
        public StoreService storeService() {
            return Mockito.mock(StoreService.class);
        }
        
        @Bean
        public CodePropertiesService codePropertiesService() {
            return Mockito.mock(CodePropertiesService.class);
        }
        
        @Bean
        public GiftCardBurnCardRepo giftCardBurnCardRepo() {
            return Mockito.mock(GiftCardBurnCardRepo.class);
        }
        
        @Bean
        public GiftCardInventoryStockRepo giftCardInventoryStockRepo() {
            return Mockito.mock(GiftCardInventoryStockRepo.class);
        }
        
        @Bean
        public GiftCardBurnedDocument giftCardBurnedDocument() {
            return Mockito.mock(GiftCardBurnedDocument.class);
        }
        
        @Bean
        public MessageSource messageSource() {
            return Mockito.mock(MessageSource.class);
        }
        
        @Bean
        public ReportService reportService() {
            return Mockito.mock(ReportService.class);
        }
    }
    
    @Autowired
    private GiftCardInventoryService giftCardInventoryService;
    @Autowired
    private GiftCardInventoryStockService giftCardInventoryStockService;
    @Autowired
    private GiftCardBurnCardService giftCardBurnCardService;
    @Autowired
    private GiftCardBurnedDocument giftCardBurnedDocument;
    @Autowired
    private MessageSource messageSource;
    
    @Override
    protected void doSetup() throws Exception {
        Mockito.reset(giftCardInventoryService);
        Mockito.reset(giftCardInventoryStockService);
        Mockito.reset(giftCardBurnedDocument);
        Mockito.reset(messageSource);
        Mockito.reset(giftCardBurnCardService);
    }
    
    @Test
    public void testEncodeSeriesFailure() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.post("/giftcard/burncard/encode")
                .contentType(MediaType.APPLICATION_FORM_URLENCODED)
                .param("seriesFrom", "001")
                .param("seriesTo", "002"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.success", is(false)))
                .andDo(MockMvcResultHandlers.print());
    }
    
    @Test
    public void testEncodeSeriesSuccess() throws Exception {
        List<String> mockList = new ArrayList<String>();
        mockList.add("string 1");
        mockList.add("string 2");
        
        when(giftCardInventoryService.getGiftCardSeries(Mockito.any(GiftCardInventorySeriesSearchDto.class))).thenReturn(mockList);
        
        mockMvc.perform(MockMvcRequestBuilders.post("/giftcard/burncard/encode")
                .contentType(MediaType.APPLICATION_FORM_URLENCODED)
                .param("seriesFrom", "1")
                .param("seriesTo", "2"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.success", is(true)))
                .andExpect(jsonPath("$.result", hasSize(2)))
                .andDo(MockMvcResultHandlers.print());
    }
    
    @Test
    public void testValidateSeriesSuccess() throws Exception {
        String seriesNo = new String("1");
        when(giftCardInventoryService.isSeriesNotExistsInStockStatus(seriesNo)).thenReturn(true);
        mockMvc.perform(post("/giftcard/burncard/validate/{seriesNo}", seriesNo))
            .andExpect(status().isOk())
            .andExpect(jsonPath("$.success", is(true)));
        
        when(giftCardInventoryService.isSeriesNotExistsInStockStatus(seriesNo)).thenReturn(false);
        mockMvc.perform(post("/giftcard/burncard/validate/{seriesNo}", seriesNo))
            .andExpect(status().isOk())
            .andExpect(jsonPath("$.success", is(false)));
        
        mockMvc.perform(post("/giftcard/burncard/validate/{seriesNo}", seriesNo))
        .andExpect(status().isOk())
        .andExpect(jsonPath("$.success", is(false)));
    }
    
    @Test
    public void testValidateSeriesSeriesNotExist() throws Exception {
        String seriesNo = new String("1");
        
        when(giftCardInventoryService.isSeriesNotExistsInStockStatus(seriesNo)).thenReturn(false);
        mockMvc.perform(post("/giftcard/burncard/validate/{seriesNo}", seriesNo))
            .andExpect(status().isOk())
            .andExpect(jsonPath("$.success", is(false)));
    }
    
    @Test
    public void testValidateSeriesCardInTheList() throws Exception {
        String seriesNo = new String("1");
        
        when(giftCardInventoryService.isCardInTheList(anyString(), Mockito.any(GiftCardInventorySeriesSearchDto.class))).thenReturn(true);
        
        mockMvc.perform(post("/giftcard/burncard/validate/{seriesNo}", seriesNo))
        .andExpect(status().isOk())
        .andExpect(jsonPath("$.success", is(false)));
    }
    
    @Test
    public void testSaveRequest() throws Exception {
        mockMvc.perform(post("/giftcard/burncard/saveForApproval"))
            .andExpect(status().isOk())
            .andExpect(jsonPath("$.success", is(true)));
    }
}
