package com.transretail.crm.report.template.impl;


import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.joda.time.LocalDateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.MessageSourceAware;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.context.support.MessageSourceAccessor;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Lists;
import com.transretail.crm.common.reporting.jasper.ClasspathFileResolver;
import com.transretail.crm.common.reporting.jasper.DefaultJRProcessor;
import com.transretail.crm.common.reporting.jasper.JRProcessor;
import com.transretail.crm.core.security.util.UserUtil;
import com.transretail.crm.core.service.CodePropertiesService;
import com.transretail.crm.core.service.LookupService;
import com.transretail.crm.core.service.StoreService;
import com.transretail.crm.core.util.BahasaNumberConverterUtil;
import com.transretail.crm.giftcard.dto.GcAllocDto;
import com.transretail.crm.giftcard.dto.SalesOrderDto;
import com.transretail.crm.giftcard.dto.SalesOrderItemDto;
import com.transretail.crm.giftcard.entity.SalesOrder;
import com.transretail.crm.giftcard.entity.support.SalesOrderType;
import com.transretail.crm.giftcard.service.ProductProfileService;
import com.transretail.crm.giftcard.service.SalesOrderService;
import com.transretail.crm.giftcard.service.SoAllocService;
import com.transretail.crm.giftcard.service.impl.SalesOrderServiceImpl;


@Service( "giftCardSoHandoverDocument" )
public class GiftCardSoHandoverDocument implements MessageSourceAware {

	@Autowired
    private StoreService storeService;
    @Autowired
    private ProductProfileService productProfileService;
    @Autowired
    private SalesOrderService salesOrderService;
    @Autowired
    private SoAllocService soAllocService;
    @Autowired
    private LookupService lookupService;
    @Autowired
    private CodePropertiesService codePropertiesService;

    private MessageSourceAccessor messageSource;



    public static enum HoDocType { SO_SHEET, RECEIPT, VOUCHER }

    protected static final String TEMPLATE_NAME 		= "Gift Card Sales Order Handover Document";
    protected static final String REPORT_NAME_SOSHEET 	= "reports/gift_card_sohodoc_sosheet.jasper";
    protected static final String REPORT_NAME_RECEIPT 	= "reports/gift_card_sohodoc_receipt.jasper";
    protected static final String REPORT_NAME_VOUCHER 	= "reports/gift_card_sohodoc_voucherreq.jasper";

    private static final DateTimeFormatter FULL_DATE_PATTERN = DateTimeFormat.forPattern( "MMMM d, yyyy" );



    @Override
    public void setMessageSource( MessageSource messageSource ) {
        this.messageSource = new MessageSourceAccessor( messageSource );
    }



    public JRProcessor createSoSheetJrProcessor( SalesOrderDto soDto ) {
		Assert.notNull( soDto, "Parameter 'soDto' expected not null" );
		
		ImmutableMap.Builder<String, Object> imParams = ImmutableMap.<String, Object>builder()
			.put( "orderNo", soDto.getOrderNo() )
			.put( "customerId", soDto.getCustomerId() )
			.put( "customerDesc", StringUtils.isNotBlank( soDto.getCustomerDesc() )? soDto.getCustomerDesc() : "" )
			.put( "contactPerson", StringUtils.isNotBlank( soDto.getContactPerson() )? soDto.getContactPerson() : "" )
			.put( "contactEmail", StringUtils.isNotBlank( soDto.getContactEmail() )? soDto.getContactEmail() : "" )
			.put( "contactNumber", StringUtils.isNotBlank( soDto.getContactNumber() )? soDto.getContactNumber() : "" )
			.put( "companyPhoneNo", StringUtils.isNotBlank( soDto.getCompanyPhoneNo() )? soDto.getCompanyPhoneNo() : "" )
			.put( "mailAddress", StringUtils.isNotBlank( soDto.getMailAddress() )? soDto.getMailAddress() : "" )
			.put( "faxNo", StringUtils.isNotBlank( soDto.getFaxNo() )? soDto.getFaxNo() : "" )
			.put( "discType", ( null != soDto.getDiscType() )? soDto.getDiscType().name() : "" )
			.put( "receiptNo", StringUtils.isNotBlank( soDto.getReceiptNo() )? soDto.getReceiptNo() : "" )
			.put( "shippingFee", null != soDto.getShippingFee()? soDto.getShippingFee() : new BigDecimal( 0 ) )
			.put( "deliveryInfo", StringUtils.isNotBlank( soDto.getDeliveryType() )? 
				messageSource.getMessage( "gc.so.delivery.types." + soDto.getDeliveryType(), LocaleContextHolder.getLocale() ) 
					+ ( SalesOrder.DeliveryType.SHIP_TO.name().equalsIgnoreCase( soDto.getDeliveryType() )?
							"\n" + soDto.getShippingInfo() : "" ) 
					: "" )
			.put( "orderDate", FULL_DATE_PATTERN.print( soDto.getOrderDate() ) )
			.put( "orderSheet", messageSource.getMessage( SalesOrderType.INTERNAL_ORDER.equals( soDto.getOrderType() )? 
					"gc.b2b.report.os.ordersheet.int" 
					: SalesOrderType.REPLACEMENT.equals( soDto.getOrderType() )? 
							"gc.b2b.report.os.ordersheet.rep" : "gc.b2b.report.os.ordersheet", LocaleContextHolder.getLocale() ) );

		if ( SalesOrderType.REPLACEMENT.equals( soDto.getOrderType() ) ) {
			imParams.put( "isReplacement", true )
				.put( "returnNo", soDto.getReturnNo() )
				.put( "replacedSoNo", soDto.getOrigOrderNo() )
				.put( "replacedSoDate", FULL_DATE_PATTERN.print( soDto.getOrigOrderDate() ) );
		}
		imParams.put( "paymentAmtLabel", messageSource.getMessage( 
			( SalesOrderType.REPLACEMENT.equals( soDto.getOrderType() ) || SalesOrderType.B2B_ADV_SALES.equals( soDto.getOrderType() ) )? 
				"gc.b2b.report.os.paymentamt" : "gc.b2b.report.os.amttaxed", LocaleContextHolder.getLocale() ) );
		

		long totalQty = 0;
		BigDecimal totalCardFee = new BigDecimal( 0 );
		BigDecimal totalFaceAmt = new BigDecimal( 0 );
		List<SoReportBean> beansDataSource = Lists.newArrayList();
		for ( SalesOrderItemDto item : soDto.getItems() ) {
			boolean isAllocated = null != item.getGcAllocs();
			beansDataSource.add( new SoReportBean( 
					item.getProductId() + " - " + item.getProductDesc(), 
					item.getProductDesc(),
					item.getQuantity(),
					item.getFaceValue(),
					item.getFaceAmount(),
					item.getPrintFee(),
					null, //isAllocated? item.getGcAlloc().getSeriesStart().toString() : null,
					null, //isAllocated? item.getGcAlloc().getSeriesEnd().toString() : null,
					null, //isAllocated? item.getGcAlloc().getQuantity() : null,
					isAllocated? item.getGcAllocs() : null ) );
			totalQty += ( null != item.getQuantity()? item.getQuantity() : 0 );
			totalCardFee = totalCardFee.add(item.getPrintFee());
			totalFaceAmt = totalFaceAmt.add(  ( null != item.getFaceAmount()? item.getFaceAmount() : new BigDecimal( 0 ) ) );
		}
		BigDecimal discountedAmt = ( null != soDto.getDiscountVal() )? totalFaceAmt.subtract( totalFaceAmt.multiply( soDto.getDiscountVal().divide( new BigDecimal( 100 ) ) ) ) : totalFaceAmt;
		discountedAmt = ( null != soDto.getShippingFee() )? discountedAmt.add( soDto.getShippingFee() ) : discountedAmt;
		discountedAmt = ( null != totalCardFee )? discountedAmt.add( totalCardFee ) : discountedAmt;
		imParams.put( "totalQty", totalQty )
			.put( "totalFaceAmt", totalFaceAmt )
			.put( "discountRate", ( null != soDto.getDiscountVal()? soDto.getDiscountVal() : new BigDecimal( 0 ) ) )
			.put( "discountedAmt", discountedAmt /*new DecimalFormat( "0.00" ).format( discountedAmt )*/ )
			.put( "discount", ( null != soDto.getDiscountVal() )? totalFaceAmt.multiply( soDto.getDiscountVal().divide( new BigDecimal( 100 ) ) ) : new BigDecimal( 0 ) )
			.put( "cardFee", totalCardFee )
			.put( "tableReport", beansDataSource );

		imParams.put( "printPerson", UserUtil.getCurrentUser().getUsername() )
			.put( "printDate", FULL_DATE_PATTERN.print( new LocalDateTime() ) );

		Map<String, Object> parameters = imParams.build();

		DefaultJRProcessor jrProcessor = new DefaultJRProcessor( REPORT_NAME_SOSHEET );
		jrProcessor.addParameters( parameters );
		jrProcessor.setFileResolver( new ClasspathFileResolver( "reports" ) );
		return jrProcessor;
    }

    public JRProcessor createReceiptJrProcessor( SalesOrderDto soDto ) {
		Assert.notNull( soDto, "Parameter 'soDto' expected not null" );

		ImmutableMap.Builder<String, Object> imParams = ImmutableMap.<String, Object>builder()
				.put( "orderNo", soDto.getOrderNo() )
				.put( "receiptNo", soDto.getReceiptNo() )
				.put( "printDate", FULL_DATE_PATTERN.print( new LocalDateTime() ) )
				.put( "invoiceTitle", soDto.getInvoiceTitle() );

		if ( SalesOrderType.REPLACEMENT.equals( soDto.getOrderType() ) ) {
			SalesOrderDto origSo = salesOrderService.getOrderByOrderNo( soDto.getOrigOrderNo() );
			imParams.put( "isReplacement", true )
				.put( "repReceiptNo", origSo.getReceiptNo() )
				.put( "repReceiptDate", FULL_DATE_PATTERN.print( origSo.getEarliestPaymentDate() ) )
				.put( "repPaid", origSo.getTotalPayment() );
		}

		BigDecimal totalFaceAmt = new BigDecimal( 0 );
		BigDecimal totalPrintFee = new BigDecimal( 0 );
		BigDecimal totalGcAmt = new BigDecimal( 0 ); //[TOTAL] netFaceAmount + netPrintFee = netGcAmount
		List<SoReportBean> beansDataSource = Lists.newArrayList();
		for ( SalesOrderItemDto item : soDto.getItems() ) {
			boolean isAllocated = null != item.getGcAllocs();
			for ( GcAllocDto alloc : item.getGcAllocs() ) {
				SoReportBean soBean = new SoReportBean( 
					item.getProductId() + " - " + item.getProductDesc(), 
					item.getProductDesc(),
					alloc.getQuantity(),
					item.getFaceValue(),
					item.getFaceAmount(),
					item.getPrintFee(),
					alloc.getSeriesStart().toString(),
					alloc.getSeriesEnd().toString(),
					alloc.getQuantity(),
					isAllocated? item.getGcAllocs() : null,
					messageSource );
				beansDataSource.add( soBean );

			}
			//totalPrintFee = totalPrintFee.add( null != soBean.getNetPrintFee()? soBean.getNetPrintFee() : new BigDecimal( 0 ) );
			totalPrintFee = totalPrintFee.add( null != item.getPrintFee()? item.getPrintFee() : new BigDecimal( 0 ) );
			//totalFaceAmt = totalFaceAmt.add( null != soBean.getNetFaceAmt()? soBean.getNetFaceAmt() : new BigDecimal( 0 ) );
			totalFaceAmt = totalFaceAmt.add(  ( null != item.getFaceAmount()? item.getFaceAmount() : new BigDecimal( 0 ) ) );
			//totalGcAmt = totalGcAmt.add( null != soBean.getNetGcAmt()? soBean.getNetGcAmt() : new BigDecimal( 0 ) );
			totalGcAmt = totalGcAmt.add( totalFaceAmt.add( totalPrintFee ) );
		}

		BigDecimal totalDiscount = null != soDto.getDiscountVal()? totalFaceAmt.multiply( soDto.getDiscountVal().divide( new BigDecimal( 100 ) ) ) : new BigDecimal( 0 );
		BigDecimal shippingFee = null != soDto.getShippingFee()? soDto.getShippingFee() : new BigDecimal( 0 );
		BigDecimal totalAmount = totalFaceAmt.add( totalPrintFee ).add( shippingFee ).subtract( totalDiscount );
		imParams
			.put( "totalPayment", soDto.getTotalPayment() )
			.put( "totalFaceAmt", totalFaceAmt )
			.put( "totalPrintFee", totalPrintFee )
			.put( "totalGcAmt", totalGcAmt )
			.put( "totalDiscount", totalDiscount )
			.put( "shippingFee", shippingFee )
			.put( "totalAmount", totalAmount )
			.put( "totalAmountWords", 
					null != soDto.getTotalPayment()?
						BahasaNumberConverterUtil.translateNumber( soDto.getTotalPayment().longValue() ) : "" )
					//NumberToWords.convert( totalAmount, messageSource.getMessage( "label_and" ), null ) )
			.put( "tableReport", beansDataSource );
		Map<String, Object> parameters = imParams.build();

		DefaultJRProcessor jrProcessor = new DefaultJRProcessor( REPORT_NAME_RECEIPT );
		jrProcessor.addParameters( parameters );
		jrProcessor.setFileResolver( new ClasspathFileResolver( "reports" ) );
		
		return jrProcessor;
    }

    public JRProcessor createVoucherReqJrProcessor( SalesOrderDto soDto ) {
		Assert.notNull( soDto, "Parameter 'soDto' expected not null" );
		
		ImmutableMap.Builder<String, Object> imParams = ImmutableMap.<String, Object>builder()
			.put( "orderNo", soDto.getOrderNo() )
			.put( "customerId", soDto.getCustomerId() )
			.put( "customerDesc", StringUtils.isNotBlank( soDto.getCustomerDesc() )? soDto.getCustomerDesc() : "" )
			.put( "contactPerson", StringUtils.isNotBlank( soDto.getContactPerson() )? soDto.getContactPerson() : "" )
			.put( "contactEmail", StringUtils.isNotBlank( soDto.getContactEmail() )? soDto.getContactEmail() : "" )
			.put( "contactNumber", StringUtils.isNotBlank( soDto.getContactNumber() )? soDto.getContactNumber() : "" )
			.put( "orderDate", FULL_DATE_PATTERN.print( soDto.getOrderDate() ) );

		if ( SalesOrderType.REPLACEMENT.equals( soDto.getOrderType() ) ) {
			SalesOrderDto origSo = salesOrderService.getOrderByOrderNo( soDto.getOrigOrderNo() );
			origSo = soAllocService.setSoItemsAllocsDto( origSo.getId() );
			List<SoReportBean> repGiftCards = Lists.newArrayList();
			for ( SalesOrderItemDto item : origSo.getItems() ) {
				boolean isAllocated = null != item.getGcAllocs();
				for ( GcAllocDto alloc : item.getGcAllocs() ) {
					SoReportBean soBean = new SoReportBean( 
							item.getProductId() + " - " + item.getProductDesc(), 
							item.getProductDesc(), 
							alloc.getQuantity(), //item.getQuantity(),
							item.getFaceValue(),
							item.getFaceAmount(),
							item.getPrintFee(),
							alloc.getSeriesStart().toString(), //isAllocated? item.getGcAlloc().getSeriesStart().toString() : null,
							alloc.getSeriesEnd().toString(), //isAllocated? item.getGcAlloc().getSeriesEnd().toString() : null,
							alloc.getQuantity(), //isAllocated? item.getGcAlloc().getQuantity() : null,
							isAllocated? item.getGcAllocs() : null,
							messageSource );
					repGiftCards.add( soBean );
				}
			}
			imParams.put( "isReplacement", true )
				.put( "repOrderNo", soDto.getOrigOrderNo() )
				.put( "repOrderDate", FULL_DATE_PATTERN.print( soDto.getOrigOrderDate() ) )
				.put( "repGiftCards", repGiftCards );
		}

		long totalQty = 0;
		BigDecimal totalCardFee = new BigDecimal( 0 );
		BigDecimal totalFaceAmt = new BigDecimal( 0 );
		//BigDecimal initNetAmt = new BigDecimal( 0 );
		List<SoReportBean> beansDataSource = Lists.newArrayList();
		for ( SalesOrderItemDto item : soDto.getItems() ) {
			boolean isAllocated = null != item.getGcAllocs();
			for ( GcAllocDto alloc : item.getGcAllocs() ) {
				SoReportBean soBean = new SoReportBean( 
						item.getProductId() + " - " + item.getProductDesc(), 
						item.getProductDesc(), 
						alloc.getQuantity(),
						item.getFaceValue(),
						item.getFaceAmount(),
						item.getPrintFee(),
						alloc.getSeriesStart().toString(),
						alloc.getSeriesEnd().toString(),
						alloc.getQuantity(),
						isAllocated? item.getGcAllocs() : null,
						messageSource );
				beansDataSource.add( soBean );
			}

			totalQty += ( null != item.getQuantity()? item.getQuantity() : 0 );
			totalCardFee = totalFaceAmt.add( null != item.getCardFee() && null != item.getQuantity()? 
					new BigDecimal( item.getCardFee().doubleValue() ).multiply( new BigDecimal( item.getQuantity() ) ) : new BigDecimal( 0 ) );
			totalFaceAmt = totalFaceAmt.add(  ( null != item.getFaceAmount()? item.getFaceAmount() : new BigDecimal( 0 ) ) );
			//initNetAmt = initNetAmt.add( null != soBean.getNetFaceAmt()? soBean.getNetFaceAmt() : new BigDecimal( 0 ) );
		}
		//BigDecimal discountedAmt = ( null != soDto.getDiscountVal() )? totalFaceAmt.subtract( totalFaceAmt.multiply( soDto.getDiscountVal() ) ) : totalFaceAmt;
		BigDecimal netDiscount =  null != soDto.getDiscountVal()? totalFaceAmt.multiply( soDto.getDiscountVal().divide( new BigDecimal( 100 ) ) ) : new BigDecimal( 0 );
		imParams.put( "totalQty", totalQty )
			.put( "totalFaceAmt", totalFaceAmt )
			//.put( "discountRate", ( null != soDto.getDiscountVal()? soDto.getDiscountVal() : new BigDecimal( 0 ) ) )
			//.put( "discountedAmt", new DecimalFormat( "0.00" ).format( discountedAmt ) )
			//.put( "discount", ( null != soDto.getDiscountVal() )? totalFaceAmt.multiply( soDto.getDiscountVal() ) : new BigDecimal( 0 ) )
			.put( "discount", netDiscount )
			.put( "netAmt", totalFaceAmt.subtract( netDiscount ) )
			.put( "cardFee", totalCardFee )
			.put( "tableReport", beansDataSource );

		imParams.put( "printPerson", UserUtil.getCurrentUser().getUsername() )
			.put( "printDate", FULL_DATE_PATTERN.print( new LocalDateTime() ) );

		Map<String, Object> parameters = imParams.build();

		DefaultJRProcessor jrProcessor = new DefaultJRProcessor( REPORT_NAME_VOUCHER );
		jrProcessor.addParameters( parameters );
		jrProcessor.setFileResolver( new ClasspathFileResolver( "reports" ) );
		return jrProcessor;
    }



    public static class SoReportBean {
        private String productName;
        private String productDesc;
        private Long quantity;
        private Integer faceValue;
        private BigDecimal faceAmount;
        private BigDecimal netFaceAmt;
        private BigDecimal netPrintFee;
        private BigDecimal netGcAmt;
        private BigDecimal printfee;
        private String cardNoFrom;
        private String cardNoTo;
        private Long allocQuantity;
        private String strAllocations;

		public SoReportBean() {}
        public SoReportBean( 
        		String productName, 
        		String productDesc, 
        		Long quantity, 
        		Integer faceValue, 
        		BigDecimal faceAmount, 
        		BigDecimal printfee,
        		String cardNoFrom,
        		String cardNoTo,
        		Long allocQuantity,
        		List<GcAllocDto> allocations ) {

            super();
            this.productName = productName;
            this.productDesc = productDesc;
            this.quantity = quantity;
            this.faceValue = faceValue;
            this.faceAmount = faceAmount;
            this.printfee = printfee;
            this.cardNoFrom = cardNoFrom;
            this.cardNoTo = cardNoTo;
            this.allocQuantity = allocQuantity;
            this.netFaceAmt = null != quantity && null != faceValue? new BigDecimal( faceValue ).multiply( new BigDecimal( quantity.longValue() ) )  : null;
            this.netPrintFee = null != quantity && null != printfee? printfee.multiply( new BigDecimal( quantity.longValue() ) )  : null;
            this.netGcAmt = null != this.netPrintFee? this.netFaceAmt.add( this.netPrintFee ) : this.netFaceAmt;
            if ( CollectionUtils.isNotEmpty( allocations ) ) {
            	StringBuilder sb = new StringBuilder();
            	for ( GcAllocDto alloc : allocations ) {
                	sb.append( alloc.getSeriesStart() + " - " + alloc.getSeriesEnd() + " (" + alloc.getQuantity() + ") " );
                }
            	strAllocations = sb.toString();
            }
        }

        /*TEMP FIX: resource bundle inside subDataSet of reports*/
        private String labelFrom, labelTo, labelQty, labelFaceAmt, labelTotal, labelRupiah, labelNominal, labelPcs;
        public String getLabelFrom() {
			return labelFrom;
		}
		public String getLabelTo() {
			return labelTo;
		}
		public String getLabelQty() {
			return labelQty;
		}
		public String getLabelFaceAmt() {
			return labelFaceAmt;
		}
		public String getLabelTotal() {
			return labelTotal;
		}
		public String getLabelRupiah() {
			return labelRupiah;
		}
        public String getLabelNominal() {
			return labelNominal;
		}
		public String getLabelPcs() {
			return labelPcs;
		}
		public SoReportBean( 
        		String productName, 
        		String productDesc, 
        		Long quantity, 
        		Integer faceValue, 
        		BigDecimal faceAmount, 
        		BigDecimal printfee,
        		String cardNoFrom,
        		String cardNoTo,
        		Long allocQuantity,
        		List<GcAllocDto> allocations,
        		MessageSourceAccessor messageSourceAccessor ) {

            this( productName, productDesc, quantity, faceValue, faceAmount, printfee, cardNoFrom, cardNoTo, allocQuantity, allocations );
            this.labelFrom = messageSourceAccessor.getMessage( "label_from" );
            this.labelTo = messageSourceAccessor.getMessage( "label_to" );
            this.labelQty = messageSourceAccessor.getMessage( "gc.so.item.qty" );
            this.labelFaceAmt = messageSourceAccessor.getMessage( "gc.so.item.faceamt" );
            this.labelTotal = messageSourceAccessor.getMessage( "label_total" );
            this.labelRupiah = messageSourceAccessor.getMessage( "label_rupiah" );
            this.labelNominal = messageSourceAccessor.getMessage( "label_nominal" );
            this.labelPcs = messageSourceAccessor.getMessage( "label_pcs" );
        }

		public String getProductName() {
			return productName;
		}
		public String getProductDesc() {
			return productDesc;
		}
		public Long getQuantity() {
			return quantity;
		}
		public Integer getFaceValue() {
			return faceValue;
		}
		public BigDecimal getFaceAmount() {
			return faceAmount;
		}
		public BigDecimal getNetFaceAmt() {
			return netFaceAmt;
		}
		public BigDecimal getNetPrintFee() {
			return netPrintFee;
		}
		public BigDecimal getNetGcAmt() {
			return netGcAmt;
		}
		public BigDecimal getPrintfee() {
			return printfee;
		}
		public String getCardNoFrom() {
			return cardNoFrom;
		}
		public String getCardNoTo() {
			return cardNoTo;
		}
		public Long getAllocQuantity() {
			return allocQuantity;
		}
		public String getStrAllocations() {
			return strAllocations;
		}
		public void setStrAllocations(String strAllocations) {
			this.strAllocations = strAllocations;
		}
    }

    public static class ReceiptReportBean {
        private String orderNo;
        private String invoiceTitle;

        public ReceiptReportBean() {}
        public ReceiptReportBean( 
        		String orderNo, 
        		String invoiceTitle ) {

            super();
            this.orderNo = orderNo;
            this.invoiceTitle = invoiceTitle;
        }

		public String getOrderNo() {
			return orderNo;
		}
		public void setOrderNo(String orderNo) {
			this.orderNo = orderNo;
		}
		public String getInvoiceTitle() {
			return invoiceTitle;
		}
		public void setInvoiceTitle(String invoiceTitle) {
			this.invoiceTitle = invoiceTitle;
		}
    }

}
