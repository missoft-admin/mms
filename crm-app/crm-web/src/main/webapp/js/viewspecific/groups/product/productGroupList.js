$(document).ready( function() {

	var $list = $( "#list_productGroup" );



	initDataTable();
	initExternalRef();



	function initDataTable() {
		var headers = {
	        groupName: 		$list.data( "hdr-groupname" ),
	        productCfns:	$list.data( "hdr-productcfns" ),
	        excProducts:	$list.data( "hdr-excproducts" )		    				 	
		};

		$list.ajaxDataTable({
			'autoload' 		: true,
			'ajaxSource' 	: $list.data( "url" ),
			'columnHeaders' : [	
		        headers['groupName'],
		        /*headers['productCfns'],
		        headers['excProducts'],*/
			 	{ text: "", className : "input-large" }
			],
			'modelFields' 	: [
			 	{ name 	: 'name', sortable : true },
			 	/*{ name 	: 'productsCfns', sortable : true },
			 	{ name 	: 'excludedProducts', sortable : true },*/
		        { customCell : function ( innerData, sSpecific, json ) {
			 			if ( sSpecific == 'display' ) {
			 				var btns = $( "#btn_viewProductGroup" ).html().replace( new RegExp("%NAME%", "g"), json.name );
			 				if ( !json.isUsed ) {
			 					btns += $( "#btn_editProductGroup" ).html();
			 					btns += $( "#btn_deleteProductGroup" ).html();
			 				}
			 				return btns.replace( new RegExp("%ID%", 'g'), json.id );
				        } 
			 			else {
			 				return '';
				        }
			 		}
		        }  
			 ]
		})
		.on( "click", ".btn_viewProductGroup", view )
		.on( "click", ".btn_editProductGroup", edit )
		.on( "click", ".btn_deleteProductGroup", remove );
	}

	function initExternalRef() {
		/*productGroupForm.jsp => id:content_productGroupForm*/
		$( "#content_productGroupForm" ).on( "hide", function(e) {
			$list.ajaxDataTable( "search" );
		});
	}



	function view(e) {
		e.preventDefault();
		ProductGroupItems.show( $(this).data( "id" ), $(this).data( "name" ) );
	}

	function edit(e) {
		e.preventDefault();
		/*productGroup.jsp => $( "#form_productGroup" ); productGroupForm.jsp => $( "#content_productGroupForm" )*/
		$.get( $(this).data( "url" ), 
			function(resp) { 
				$( "#form_productGroup" ).html( resp ); 
				$( "#content_productGroupForm" ).modal( "show" ); }, "html" );
	}

	function remove(e) {
		e.preventDefault();
		
		var $this = $(this);
		getConfirm( $this.data( "msg-confirm" ), function( proceed ) {
			if( proceed ) {
				$.get( $this.data( "url" ), function( resp, textStatus, jqXHR ) { $list.ajaxDataTable( "search" ); }, "json" );
			}
		});
	}

});