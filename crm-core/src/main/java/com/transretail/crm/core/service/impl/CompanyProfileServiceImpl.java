package com.transretail.crm.core.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.transretail.crm.core.entity.CompanyProfile;
import com.transretail.crm.core.repo.CompanyProfileRepo;
import com.transretail.crm.core.service.CompanyProfileService;

@Service("companyProfileService")
@Transactional
public class CompanyProfileServiceImpl implements CompanyProfileService {
	
	private static final String DEFAULT_ID = "CRM";
	
	@Autowired
	CompanyProfileRepo companyProfileRepo;
	
	@Override
	public void saveProfile(CompanyProfile companyProfile) {
		companyProfile.setId(DEFAULT_ID);
		
		if(companyProfile.getLogo() == null || companyProfile.getLogo().length == 0) {
			companyProfile.setLogo(retrieveCompany().getLogo());
		}
			
		companyProfileRepo.save(companyProfile);
	}

	
	@Override
	public CompanyProfile retrieveCompany() {
		CompanyProfile companyProfile = companyProfileRepo.findOne(DEFAULT_ID);
		return companyProfile;
	}
}
