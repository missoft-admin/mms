create table CRM_PROMOTION_AUD (
        id number(19,0) not null,
        REV number(19,0) not null,
        REVTYPE number(3,0),
        AFFECTED_DAYS varchar2(255 char),
        ACCEPT_EMAIL char(1 char),
        ACCEPT_MAIL char(1 char),
        ACCEPT_PHONE char(1 char),
        ACCEPT_SMS char(1 char),
        SMS char(1 char),
        DESCRIPTION varchar2(255 char),
        END_DATE timestamp,
        END_TIME date,
        NAME varchar2(255 char),
        START_DATE timestamp,
        START_TIME date,
        CAMPAIGN_ID number(19,0),
        REWARD_TYPE varchar2(15 char),
        STATUS varchar2(15 char),
        primary key (id, REV)
    );

create table CRM_PROGRAM_AUD (
        id number(19,0) not null,
        REV number(19,0) not null,
        REVTYPE number(3,0),
        DESCRIPTION varchar2(255 char),
        endDate timestamp,
        startDate timestamp,
        NAME varchar2(255 char),
        TYPE varchar2(20 char),
        STATUS varchar2(15 char),
        primary key (id, REV)
    );

create table CRM_CAMPAIGN_AUD (
        id number(19,0) not null,
        REV number(19,0) not null,
        REVTYPE number(3,0),
        DESCRIPTION varchar2(255 char),
        endDate timestamp,
        startDate timestamp,
        NAME varchar2(255 char),
        PROJECT_ID number(19,0),
        STATUS varchar2(15 char),
        primary key (id, REV)
    );

create table CRM_MEMBER_GRP_AUD (
        id number(19,0) not null,
        REV number(19,0) not null,
        REVTYPE number(3,0),
        CARD_TYPE_GROUP_CODE varchar2(255 char),
        MEMBERS varchar2(255 char),
        NAME varchar2(255 char),
        POINTS_ENABLED char(1 char),
        POINTS_FROM number(19,0),
        POINTS_TO number(19,0),
        DATE_RANGE_FROM date,
        DATE_RANGE_TO date,
        ENABLED char(1 char),
        FREQUENCY char(1 char),
        FREQUENCY_FROM number(19,0),
        FREQUENCY_TO number(19,0),
        PRODUCT_GROUP number(19,0),
        RECENCY varchar2(255 char),
        RECENT_VALUE varchar2(255 char),
        SPEND char(1 char),
        SPEND_FROM number(19,0),
        SPEND_PER_TRANSACTION char(1 char),
        SPEND_TO number(19,0),
        STORE_GROUP number(19,0),
        primary key (id, REV)
    );

create table CRM_PROMOTION_MEMBER_GRP_AUD (
        id varchar2(255 char) not null,
        REV number(19,0) not null,
        REVTYPE number(3,0),
        MEMBER_GRP_ID number(19,0),
        primary key (id, REV)
    );

create table CRM_PROMOTION_PAYMENTTYPE_AUD (
        id varchar2(255 char) not null,
        REV number(19,0) not null,
        REVTYPE number(3,0),
        WILDCARDS varchar2(4000 char),
        PAYMENTTYPE_ID varchar2(15 char),
        primary key (id, REV)
    );

create table CRM_PROMOTION_PRODUCT_GRP_AUD (
        id varchar2(255 char) not null,
        REV number(19,0) not null,
        REVTYPE number(3,0),
        PRODUCT_GRP_ID number(19,0),
        primary key (id, REV)
    );

create table CRM_PROMOTION_REWARD_AUD (
        id number(19,0) not null,
        REV number(19,0) not null,
        REVTYPE number(3,0),
        BASE_AMOUNT number(19,2),
        BASE_FACTOR double precision,
        DISCOUNT double precision,
        FIXED char(1 char),
        MAX_AMOUNT number(19,2),
        MAX_BONUS number(19,2),
        MAX_QTY number(19,0),
        MIN_AMOUNT number(19,2),
        MIN_QTY number(19,0),
        REMARK varchar2(1000 char),
        PROMO_ID number(19,0),
        REWARD_TYPE varchar2(15 char),
        primary key (id, REV)
    );

create table CRM_PROMOTION_STORE_GRP_AUD (
        id varchar2(255 char) not null,
        REV number(19,0) not null,
        REVTYPE number(3,0),
        STORE_GRP_ID number(19,0),
        primary key (id, REV)
    );



alter table CRM_PROMOTION_AUD 
        add constraint FK_o28sj9ji0y3d0049o3ge62815 
        foreign key (REV) 
        references CRM_REVINFO;

alter table CRM_PROGRAM_AUD 
        add constraint FK_9yi3mdiqs8g2yyhoi4ie3s6r 
        foreign key (REV) 
        references CRM_REVINFO;

alter table CRM_CAMPAIGN_AUD 
        add constraint FK_4wldya65e07un6vroct5m0gkj 
        foreign key (REV) 
        references CRM_REVINFO;

alter table CRM_MEMBER_GRP_AUD 
        add constraint FK_27y5ivj27ysot88u1cwnb7lva 
        foreign key (REV) 
        references CRM_REVINFO;

alter table CRM_PROMOTION_MEMBER_GRP_AUD 
        add constraint FK_d2wwvsxg8xbxu9vpfp17xcmve 
        foreign key (REV) 
        references CRM_REVINFO;

alter table CRM_PROMOTION_PAYMENTTYPE_AUD 
        add constraint FK_tecmsx759ysukekxdwumss9tb 
        foreign key (REV) 
        references CRM_REVINFO;

alter table CRM_PROMOTION_PRODUCT_GRP_AUD 
        add constraint FK_tihcigdoqskeyf9pyoobedr14 
        foreign key (REV) 
        references CRM_REVINFO;

alter table CRM_PROMOTION_REWARD_AUD 
        add constraint FK_qb3r5nr97eybagxtptk0lgkgi 
        foreign key (REV) 
        references CRM_REVINFO;

alter table CRM_PROMOTION_STORE_GRP_AUD 
        add constraint FK_82clahnw50ovvp5heubyunhk0 
        foreign key (REV) 
        references CRM_REVINFO;