package com.transretail.crm.giftcard.service;

import java.util.List;

import com.transretail.crm.common.service.dto.response.ResultList;
import com.transretail.crm.giftcard.dto.ProductProfileDto;
import com.transretail.crm.giftcard.dto.ProductProfileSearchDto;
import com.transretail.crm.giftcard.entity.support.ProductProfileStatus;

public interface ProductProfileService {

	ResultList<ProductProfileDto> list(ProductProfileSearchDto searchDto);

	void saveDto(ProductProfileDto profileDto);

	ProductProfileDto findDto(Long id);

	void approveProfile(Long id, ProductProfileStatus status);

	List<ProductProfileDto> findAllDtos();

	void deleteProfile(Long id);
	
	ProductProfileDto findProductProfileByCode(String productCode);

	boolean findDuplicateProductCode(String productCode, Long profileId);

	List<ProductProfileDto> findDtos(ProductProfileSearchDto dto);

	ProductProfileDto activate(Long id, boolean activate);
}
