package com.transretail.crm.core.service.impl;

import com.mysema.query.types.expr.BooleanExpression;
import com.transretail.crm.common.service.dto.response.ResultList;
import com.transretail.crm.common.service.util.SpringDataPagingUtil;
import com.transretail.crm.common.util.StringUtil;
import com.transretail.crm.core.dto.StoreDto;
import com.transretail.crm.core.dto.StoreMemberDto;
import com.transretail.crm.core.dto.StoreMemberSearchDto;
import com.transretail.crm.core.entity.QStoreMember;
import com.transretail.crm.core.entity.StoreMember;
import com.transretail.crm.core.entity.lookup.LookupDetail;
import com.transretail.crm.core.entity.lookup.Store;
import com.transretail.crm.core.repo.StoreMemberRepo;
import com.transretail.crm.core.repo.StoreRepo;
import com.transretail.crm.core.service.CodePropertiesService;
import com.transretail.crm.core.service.LookupService;
import com.transretail.crm.core.service.StoreMemberService;
import com.transretail.crm.logging.annotation.Loggable;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

@Service("storeMemberService")
public class StoreMemberServiceImpl implements StoreMemberService {

    @Autowired
    StoreMemberRepo storeMemberRepo;

    @Autowired
    CodePropertiesService codePropertiesService;

    @Autowired
    private StoreRepo storeRepo;
    
    @Autowired
    private LookupService lookupService;
    
    @Override
    public List<StoreMember> findByStore(String store) {
        return storeMemberRepo.findByStore(store);
    }


    /**
     *
     * checks if account id passes criteria to be allowed to transact in store
     *
     * @param store
     * @param accountId - same as loyalty card number
     * @param msgCode- returns message code for invalid members. Uses stringBuffer instead of string to allow passing of values as reference
     *               MSG014 - invalid account id
     *               MSG015 - invalid company code
     *               MSG016 - invalid card type
     * @return
     */
    @Override
    public boolean isValidMember(String store, String accountId, StringBuffer msgCode) {


        //validate account id/loyalty card
        if (accountId == null || accountId.length() < 4) {
            if (msgCode != null) {
                msgCode.append(codePropertiesService.getDetailMessageCodeInvalidStoreMemberAccount());
            }

            return false;
        }

        //parse account id; company- first 2 characters, cardType 3rd and 4th characters.
        String company = accountId.substring(0, 2);
        String cardType = accountId.substring(2, 4);

        if (company.equalsIgnoreCase("00")) {

            //allow access to employee to all stores
            return true;
        }

        List<StoreMember> storeMembers = storeMemberRepo.findByStore(store);

        if (storeMembers == null || storeMembers.size() == 0) {
            if (msgCode != null) {
                msgCode.append(codePropertiesService.getDetailMessageCodeInvalidStoreMemberStore());
            }

            return false;
        }

        boolean validCompany = false;
        boolean validCardType = false;
        //iterate thru all possible configuration for this store
        for (StoreMember storeMember : storeMembers) {

            //null value for company or cardType means all values are allowed
            if (StringUtil.isEmpty(storeMember.getCompany()) || storeMember.getCompany().equalsIgnoreCase(company)) {
                validCompany = true;

            }

            if (StringUtil.isEmpty(storeMember.getCardType()) || storeMember.getCardType().equalsIgnoreCase(cardType)) {
                validCardType = true;
            }

            if (validCardType && validCompany) {
                return true;
            }

        }

        if (msgCode != null) {
            if (!validCompany) {
                msgCode.append(codePropertiesService.getDetailMessageCodeInvalidStoreMemberCompany());

            } else {
                msgCode.append(codePropertiesService.getDetailMessageCodeInvalidStoreMemberCardType());

            }

        }




        return false;
    }


    @Override
    public ResultList<StoreMemberDto> searchStoreMembers(
            StoreMemberSearchDto searchDto) {
        prepareForSearching(searchDto);
        Pageable pageable = SpringDataPagingUtil.INSTANCE.createPageable(searchDto.getPagination());
        BooleanExpression filter = searchDto.createSearchExpression();
        Page<StoreMember> page = filter != null ? storeMemberRepo.findAll(filter, pageable) : storeMemberRepo.findAll(pageable);
        return new ResultList<StoreMemberDto>(convertToDto(page.getContent()), page.getTotalElements(), page.getNumber(), page.getSize());
    }

    @Override
    public void saveStoreMemberDto(StoreMemberDto storeMemberDto) {
        if (storeMemberDto != null) {
            StoreMember storeMemberModel = storeMemberDto.toModel();
            saveStoreMemberModel(storeMemberModel);
        }
    }

    @Loggable
    public void saveStoreMemberModel(StoreMember storeMemberModel) {
        prepareForSaving(storeMemberModel);
        storeMemberRepo.save(storeMemberModel);
    }

    @Override
    public void updateStoreMemberDto(StoreMemberDto storeMemberDto) {
        if (storeMemberDto != null && storeMemberDto.getId() != null) {
            StoreMember storeMember = storeMemberRepo.findOne(QStoreMember.storeMember.id.eq(storeMemberDto.getId()));
            storeMember = storeMemberDto.toModel(storeMember);
            prepareForSaving(storeMember);
            storeMemberRepo.save(storeMember);
        }
    }


    @Override
    public void deleteStoreMemberModel(StoreMember storeMember) {
        storeMemberRepo.delete(storeMember);
    }

    @Override
    public StoreMember findById(Long id) {
        return storeMemberRepo.findOne(QStoreMember.storeMember.id.eq(id));
    }
    
    @Override
    public boolean isStoreMemberSchemeExists(StoreMemberDto dto) {
        boolean exists = false;
        
        StoreMember storeMember = storeMemberRepo.findOne(QStoreMember.storeMember.store.eq(dto.getStore())
                .and(QStoreMember.storeMember.company.eq(dto.getCompany())
                .and(QStoreMember.storeMember.cardType.eq(dto.getCardType()))));
        
        if (storeMember !=null)
            exists = true;
        
        return exists;
    }


    @Override
    public StoreMember findByIdAndPreparedForView(Long id) {
        StoreMember model = findById(id);
        prepareForViewing(model);
        return model;
    }


    private void prepareForSaving (StoreMember model) {
        if (StringUtils.isNotEmpty(model.getCompany()))
            model.setCompany(convertCodesToTwoDigits(model.getCompany()));
            
        if (StringUtils.isNotEmpty(model.getCardType()))
            model.setCardType(convertCodesToTwoDigits(model.getCardType()));
    }

    private void prepareForSearching (StoreMemberSearchDto dto) {
        if (StringUtils.isNotEmpty(dto.getCompany()))
            dto.setCompany(convertCodesToTwoDigits(dto.getCompany()));
            
        if (StringUtils.isNotEmpty(dto.getCardType()))
            dto.setCardType(convertCodesToTwoDigits(dto.getCardType()));
    }

    private void prepareForViewing (StoreMember model) {
        if (StringUtils.isNotEmpty(model.getCompany())) {
            LookupDetail company = lookupService
                    .getDetailByTwoDigitCompanyNumber(model.getCompany());
            model.setCompany(company.getCode());
        }
        
        if (StringUtils.isNotEmpty(model.getCardType())) {
            LookupDetail cardType = lookupService
                    .getDetailByTwoDigitCardTypeNumber(model.getCardType());
            model.setCardType(cardType.getCode());
        }
    }

    private Collection<StoreMemberDto> convertToDto(List<StoreMember> models) {
        if (CollectionUtils.isEmpty(models)) {
            return new ArrayList<StoreMemberDto>();
        }
        
        List<StoreMemberDto> dtos = new ArrayList<StoreMemberDto>();
        for (StoreMember model : models) {
            StoreMemberDto dto = new StoreMemberDto(model);
            if (StringUtils.isNotBlank(dto.getStore())) {
                Store store = storeRepo.findByCode(dto.getStore());
                if (store != null)
                    dto.setStoreDto(new StoreDto(store));
            }
            
            if (StringUtils.isNotBlank(dto.getCompany())) {
                LookupDetail company = lookupService.getDetailByTwoDigitCompanyNumber(dto.getCompany());
                if (company != null)
                    dto.setCompanyDto(new LookupDetail(company.getCode(), company.getDescription()));
            }
            
            if (StringUtils.isNotBlank(dto.getCardType())) {
                LookupDetail cardType = lookupService.getDetailByTwoDigitCardTypeNumber(dto.getCardType());
                if (cardType != null)
                    dto.setCardTypeDto(new LookupDetail(cardType.getCode(), cardType.getDescription()));
            }
            dtos.add(dto);
        }
        return dtos;
    }

    private String convertCodesToTwoDigits(String code) {
        if (code != null && code.length() > 1) {
            code = code.substring(code.length() - 2);
        }
        return code;
    }

}
