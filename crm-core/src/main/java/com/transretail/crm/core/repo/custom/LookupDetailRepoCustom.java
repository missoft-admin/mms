package com.transretail.crm.core.repo.custom;

/**
 * @author Allan G. Ramirez (agramirez@exist.com)
 */
public interface LookupDetailRepoCustom {
    String getDescriptionByCode(String code);

    String getCodeByDescription(String headerCode, String description);
    
    Boolean hasDuplicateDescription(String code, String description, String headerCode);
}
