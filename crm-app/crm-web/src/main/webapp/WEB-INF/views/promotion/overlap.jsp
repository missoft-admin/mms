<div id="overlapping-container" class="modal hide  nofly modal-dialog" style="display: none;">

  <div class="modal-content">
    <div class="modal-header">
      <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
      <h4>Overlapping Promotions</h4>
    </div>

    <div class="modal-body">
      <table class="table table-bordered table-striped">
		    <thead>
		    <tr>
		      <th><spring:message code="promotion_overlap_promotion_id"/></th>
		      <th><spring:message code="promotion_overlap_promotion_name"/></th>
		      <th><spring:message code="promotion_overlap_start_date"/></th>
		      <th><spring:message code="promotion_overlap_end_date"/></th>
		      <th><spring:message code="promotion_overlap_membergroup"/></th>
		      <th><spring:message code="promotion_overlap_product"/></th>
		      <th><spring:message code="promotion_overlap_store"/></th>
		      <th><spring:message code="promotion_overlap_paymenttype"/></th>
		      <th><spring:message code="promotion_overlap_channel"/></th>
		    </tr>
		    </thead>
		    <tbody>
		    <tr>
		      <spring:url value="/images/ajax-loader-spinner-big.gif" var="spinnerImageUrl"/>
		      <td colspan="8" style="text-align: center"><img src="${spinnerImageUrl}"/></td>
		    </tr>
		    </tbody>
		  </table>
    </div>

  </div>

  
</div>

<style>
  .withOverlap {
    background: red;
  }

  #overlapping-container tbody > tr > td a {
    text-decoration: none;
    color: white;
  }

  #overlapping-container tbody > tr > td a.withOverlap > div {
    background: red;
  }

  #overlapping-container tbody > tr > td div {
    height: 25px;
    width: 70px;
  }
</style>
<script>
var $overlappingDialog = $( '#overlapping-container' );
var $tbody = $( 'tbody', $overlappingDialog );

var OverlapPromotions = {
  data : null,
  show : function ( _promoId ) {
    var self = this;
    var urlTemplate = "<spring:url value="/promotion/id/overlappingpromo"/>";
    $.get( urlTemplate.replace( "id", _promoId ), function ( data ) {
      self.data = data;
      var htmlString = '';
      if ( data.length > 0 ) {
        for ( var i = 0; i < data.length; i++ ) {
          htmlString += '<tr>';
          htmlString += '<td>' + data[i].promotionId + '</td>';
          htmlString += '<td>' + data[i].promotionName + '</td>';
          htmlString += '<td>' + data[i].beginDateTime + '</td>';
          htmlString += '<td>' + data[i].endDateTime + '</td>';
          htmlString += '<td><a href="#" onclick="return false;" class="' + (data[i].memberGroupNames != null && data[i].memberGroupNames.length > 0 ? "withOverlap" : "") + '" id="memberGroupNames' + i + '"><div>&nbsp;</div></a></td>';
          htmlString += '<td><a href="#" onclick="return false;" class="' + (data[i].products != null && data[i].products.length > 0 ? "withOverlap" : "") + '" id="products' + i + '"><div>&nbsp;</div></a></td>';
          htmlString += '<td><a href="#" onclick="return false;" class="' + (data[i].stores != null && data[i].stores.length > 0 ? "withOverlap" : "") + '" id="stores' + i + '"><div>&nbsp;</div></a></td>';
          htmlString += '<td><a href="#" onclick="return false;" class="' + (data[i].paymentTypes != null && data[i].paymentTypes.length > 0 ? "withOverlap" : "") + '" id="paymentTypes' + i + '"><div>&nbsp;</div></a></td>';
          htmlString += '<td><a href="#" onclick="return false;" class="' + (data[i].channels != null && data[i].channels.length > 0 ? "withOverlap" : "") + '" id="channels' + i + '"><div>&nbsp;</div></a></td>';
          htmlString += '</tr>';
        }
      } else {
        htmlString += '<tr><td colspan="8" style="text-align: center">No Overlapping Promotions.</td></tr>';
      }
      $tbody.html( htmlString );
    } );
  }
}

$(document).ready( function() {
	  $overlappingDialog.modal({ show : false });

	  $overlappingDialog.on( "hide", function ( event, ui ) {
	    OverlapPromotions.data = null;
	    $tbody.html( '<tr><td colspan="8" style="text-align: center"><img src="${spinnerImageUrl}"/></td></tr>' );
	  } );

	  $( $overlappingDialog ).on( "click", "a.withOverlap", function ( event ) {
	    if ( this.id.indexOf( 'memberGroupNames' ) != -1 ) {
	      var index = this.id.substring( 'memberGroupNames'.length );
	      alert( OverlapPromotions.data[parseInt( index )].memberGroupNames );
	    } else if ( this.id.indexOf( 'products' ) != -1 ) {
	      var index = this.id.substring( 'products'.length );
	      alert( OverlapPromotions.data[parseInt( index )].products );
	    } else if ( this.id.indexOf( 'stores' ) != -1 ) {
	      var index = this.id.substring( 'stores'.length );
	      alert( OverlapPromotions.data[parseInt( index )].stores );
	    } else if ( this.id.indexOf( 'paymentTypes' ) != -1 ) {
	      var index = this.id.substring( 'paymentTypes'.length );
	      alert( OverlapPromotions.data[parseInt( index )].paymentTypes );
	    } else if ( this.id.indexOf( 'channels' ) != -1 ) {
	      var index = this.id.substring( 'channels'.length );
	      alert( OverlapPromotions.data[parseInt( index )].channels );
	    }
	  } );
	
});
</script>