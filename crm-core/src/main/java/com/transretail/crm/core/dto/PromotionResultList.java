package com.transretail.crm.core.dto;


import java.util.Collection;

import org.springframework.data.domain.Page;

import com.transretail.crm.common.service.dto.response.AbstractResultListDTO;


public class PromotionResultList extends AbstractResultListDTO<PromotionDto> {

    public PromotionResultList( 
    		Collection<PromotionDto> results, 
    		long totalElements, 
    		boolean hasPreviousPage, 
    		boolean hasNextPage) {

        super(results, totalElements, hasPreviousPage, hasNextPage );
    }
    
    public PromotionResultList( Page<PromotionDto> page ) {
    	super(page);
    }
}
