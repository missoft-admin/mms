package com.transretail.crm.common.web.dto;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

/**
 * @author Allan G. Ramirez (agramirez@exist.com)
 * @author aco
 */
public abstract class AbstractSingleInstanceWebForm<T> implements WebForm<T> {

    private final WebFormInfo webFormInfo;

    protected AbstractSingleInstanceWebForm() {
        this.webFormInfo = new WebFormInfo(this.getClass().getSimpleName());
    }

    protected AbstractSingleInstanceWebForm(String webFormId) {
        this.webFormInfo = new WebFormInfo(webFormId);
    }

    @Override
    public WebFormInfo getWebFormInfo() {
        return webFormInfo;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this, ToStringStyle.DEFAULT_STYLE)
            .append("webFormInfo", webFormInfo)
            .append("this", this)
            .toString();
    }
}
