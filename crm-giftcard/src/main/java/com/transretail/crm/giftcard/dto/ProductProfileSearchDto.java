package com.transretail.crm.giftcard.dto;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.BooleanUtils;
import org.apache.commons.lang3.StringUtils;

import com.mysema.query.types.expr.BooleanExpression;
import com.transretail.crm.common.service.dto.request.AbstractSearchFormDto;
import com.transretail.crm.giftcard.entity.QProductProfile;
import com.transretail.crm.giftcard.entity.support.ProductProfileStatus;

/**
 *
 */
public class ProductProfileSearchDto extends AbstractSearchFormDto {
	
	private String prodCode;
	private String prodDesc;
	private String prodDescLike;
	private String faceVal;
	private String cardFee;
	private Boolean allowReload = Boolean.FALSE;
	private Boolean allowPartialRedeem = Boolean.FALSE;
	private String statusCode;
	private Boolean isEgc;
	
	private static final String DOUBLE_REGEX = "^\\d*\\.?\\d*$";


    @Override
    public BooleanExpression createSearchExpression() {
    	List<BooleanExpression> expressions = new ArrayList<BooleanExpression>();
    	QProductProfile qProfile = QProductProfile.productProfile;
    	if(StringUtils.isNotBlank(prodCode)) {
    		expressions.add(qProfile.productCode.containsIgnoreCase(prodCode));
    	}
    	if(StringUtils.isNotBlank(prodDesc)) {
    		expressions.add(qProfile.productDesc.containsIgnoreCase(prodDesc));
    	}
    	if ( StringUtils.isNotBlank( prodDescLike ) ) {
    		expressions.add( qProfile.productDesc.containsIgnoreCase( prodDescLike ) );
    	}
    	
    	if(StringUtils.isNotBlank(faceVal) && faceVal.matches(DOUBLE_REGEX)) {
    		expressions.add(qProfile.faceValue.description.eq(faceVal));	
    	}
    	if(cardFee != null && cardFee.matches(DOUBLE_REGEX)) {
    		expressions.add(qProfile.cardFee.eq(Double.valueOf(cardFee)));
    	}
    	if(allowReload != null && BooleanUtils.isTrue(allowReload)) {
    		expressions.add(qProfile.allowReload.isTrue());
    	}
    	if(allowPartialRedeem != null && BooleanUtils.isTrue(allowPartialRedeem)) {
    		expressions.add(qProfile.allowPartialRedeem.isTrue());
    	}
    	if(StringUtils.isNotBlank(statusCode)) {
    		expressions.add(qProfile.status.eq(ProductProfileStatus.valueOf(statusCode)));
    	}
    	if ( BooleanUtils.isTrue( isEgc ) ) {
    		expressions.add( qProfile.isEgc.eq( isEgc ) );
    	}
    	else if ( BooleanUtils.isFalse( isEgc ) ) {
    		expressions.add( qProfile.isEgc.isNull().or( qProfile.isEgc.eq( false ) ) );
    	}
    	return BooleanExpression.allOf(expressions.toArray(new BooleanExpression[expressions.size()]));
    }


	public String getProdCode() {
		return prodCode;
	}


	public void setProdCode(String prodCode) {
		this.prodCode = prodCode;
	}


	public String getProdDesc() {
		return prodDesc;
	}


	public void setProdDesc(String prodDesc) {
		this.prodDesc = prodDesc;
	}


	public String getProdDescLike() {
		return prodDescLike;
	}


	public void setProdDescLike(String prodDescLike) {
		this.prodDescLike = prodDescLike;
	}


	public String getFaceVal() {
		return faceVal;
	}


	public void setFaceVal(String faceVal) {
		this.faceVal = faceVal;
	}


	public String getCardFee() {
		return cardFee;
	}


	public void setCardFee(String cardFee) {
		this.cardFee = cardFee;
	}


	public Boolean getAllowReload() {
		return allowReload;
	}


	public void setAllowReload(Boolean allowReload) {
		this.allowReload = allowReload;
	}


	public Boolean getAllowPartialRedeem() {
		return allowPartialRedeem;
	}


	public void setAllowPartialRedeem(Boolean allowPartialRedeem) {
		this.allowPartialRedeem = allowPartialRedeem;
	}


	public String getStatusCode() {
		return statusCode;
	}


	public void setStatusCode(String statusCode) {
		this.statusCode = statusCode;
	}


	public Boolean getIsEgc() {
		return isEgc;
	}


	public void setIsEgc(Boolean isEgc) {
		this.isEgc = isEgc;
	}
    
}
