var CONTAINER_ERROR = "#contentError > div";
var ID_MEMBER_UPDATEFORM = "customerModel"; //reference = rendered id from memberDialog.jspx (modelAttribute value)
var ID_MEMBER_UPDATEPINFORM = "updatePinForm";
var MSG_ACCOUNTIDTOBEGENERATED = "";

$(document).ready(function() {
	bindCheckboxes();

	$( "#wrapperDialog" ).dialog({
		draggable: 	false,
		autoOpen: false,
		width: 670,
		modal: true,
		autoOpen: false,
		create: function(event, ui) {
			$(".ui-widget-overlay").click( function(event) {
				$('#wrapperDialog').dialog('close');
			});
        }
	});
	
	$( "#updatePinDialog" ).dialog({
		draggable: 	false,
		autoOpen: false,
		width: 670,
		modal: true,
		autoOpen: false,
		create: function(event, ui) {
			$(".ui-widget-overlay").click( function(event) {
				$('#updatePinDialog').dialog('close');
			});
        }
	});

	$("#contentMemberTab").tabs();
	
	$("select[name='memberType']").change( function() {
		$("#label_memberType").html( $(this).val() );
		
		if( $(this).find("option:selected").text() == "COMPANY" ) {
			$("#contentCompanyName > div").show();
		}
		else {
			$("#contentCompanyName > div").hide();
		}
	});
	$("#contentCompanyName > div").hide();
	$("#memberTypeRadio > div").show();
	$("#memberTypeText > div").hide();
	
    $("#cancelButton").click( function(event) {
        $('#wrapperDialog').dialog('close');
		$("#label_memberType").html( '' );
    	$("#contentCompanyName > div").hide();
    	$("form[id='" + ID_MEMBER_UPDATEFORM + "']").trigger( "reset" );
    	$( CONTAINER_ERROR ).hide();
    	$("input[name='memberTypeCode']").each( function() {
    		$(this).attr( 'checked', false) ;
    	});
    	$("input[name='username']").val("");
    });
    
    MSG_ACCOUNTIDTOBEGENERATED = $("#contentInfoAccountId").html() ;
});


function createCustomer( theAction, inQueryString ) {

	$( '#wrapperDialog' ).dialog('open');
	var CONTROLLER_POPULATEMEMBER = "/populatemember/" + null;
	populateMember( CONTROLLER_POPULATEMEMBER );
	
	var CONTROLLER_CREATEMEMBER = "/createmember";	
	doAjaxControllerSaveMember( CONTROLLER_CREATEMEMBER, inQueryString );
	
	displayContent( $("#contentMemberPassword"), true);
}

//function createSupplement( theMemberId, inQueryString ) {
//	var CONTROLLER_POPULATEMEMBER = "/supplement/new/" + theMemberId;
//	populateMember( CONTROLLER_POPULATEMEMBER, true );
//	
//	var CONTROLLER_CREATEMEMBER = "/createmember";	
//	doAjaxControllerSaveMember( CONTROLLER_CREATEMEMBER, inQueryString );
//	
//	displayContent( $("#contentMemberPassword"), true);
//}

//function updateMember( theMemberId, inQueryString ) {
//
//	$( '#wrapperDialog' ).dialog('open');
//	var CONTROLLER_POPULATEMEMBER = "/populatemember/" + theMemberId;
//	populateMember( CONTROLLER_POPULATEMEMBER, false );
//	
//	var CONTROLLER_UPDATEMEMBER = "/updatemember/" + theMemberId;
//	doAjaxControllerSaveMember( CONTROLLER_UPDATEMEMBER, inQueryString );
//	
//	displayContent( $("#contentMemberPassword"), false);
//    
//    $( "input[name='email']" ).change( function() {
//    	if( $(this).val() != "" ) {
//    		$( "#contentMemberPassword" ).show();
//    	}
//    });
//}

function updatePin( theMemberId, inQueryString ) {
	$("#updatePinDialog").find( CONTAINER_ERROR ).parent().hide();
	$( '#updatePinDialog' ).dialog('open');
	$("form[id='" + ID_MEMBER_UPDATEPINFORM + "']").trigger( "reset" );
	
	var CONTROLLER_UPDATEPINMEMBER = "/pin/update/" + theMemberId;
	doAjaxControllerUpdatePin( CONTROLLER_UPDATEPINMEMBER, inQueryString );
}

function doAjaxControllerUpdatePin( inAjaxController, inQueryString ) {

    var theParentForm = $("form[id='" + ID_MEMBER_UPDATEPINFORM + "']");
    
	$("#updatePinDialog").find("#proceed").unbind("click").click( function() {
	    $.ajax({
			type 	: "POST",
			url 	: $(theParentForm).attr("action") + inAjaxController,
			dataType: 'json',
			data 	: $(theParentForm).serialize(),
			        
			success : function(inResponse) {
				if (inResponse.success) {
					$("#updatePinDialog").find( CONTAINER_ERROR ).hide('fast');
					$(theParentForm).unbind("submit").submit( function() {
						$(this).attr("action", $(this).attr("action") + "?" + inQueryString);
					});
					$(theParentForm).submit();
				} 
				else {
					errorInfo = "";
					for (i = 0; i < inResponse.result.length; i++) {
						errorInfo += "<br>" + (i + 1) + ". "
								+ ( inResponse.result[i].code != undefined ? 
										inResponse.result[i].code : inResponse.result[i]);
					}
					$("#updatePinDialog").find( CONTAINER_ERROR ).html("Please correct following errors: " + errorInfo);
					$("#updatePinDialog").find( CONTAINER_ERROR ).parent().show('slow');
				}
			},
			error : function(jqXHR, textStatus, errorThrown) {
				$( CONTENT_ERROR ).html("Error: " + errorThrown);
			}
		});
	});


	$("#updatePinDialog").find("#cancelButton").click( function() {
    	$(theParentForm).trigger( "reset" );
    	$( '#updatePinDialog' ).dialog('close');
    });
}


function populateMember( inPopulateUrl, isSupplement ) {
    var theParentForm = $("form[id='" + ID_MEMBER_UPDATEFORM + "']");

    $.ajax({
		type 	: "POST",
		url 	: "/crm-web/customermodels" + inPopulateUrl,
		dataType: 'json',
		        
		success : function(inResponse) {
			//$("#memberUpdateBlock").html(inResponse);
			var inReturnEntity = inResponse.result;
			if(!inResponse.success) {
				$("#wrapperDialog").dialog('close');
				return;
			}
			
			$.each(inReturnEntity, function(key, value) {
				if ( key == "accountId" ) {
					if ( value != "" && value != null ) {
						$("#contentInfoAccountId").html( value );
					}
					else {
						$("#contentInfoAccountId").html( MSG_ACCOUNTIDTOBEGENERATED );
					}
					//$(theParentForm).addHidden(key, value)
					return true;
				}
				
				if ( key == "parent" ) {
					$(theParentForm).addHidden(key, value);
				}
				
				fillField(key, value);
				if (key == "lifestyle" || key == "channel" || key == "customerProfile") {
					$.each(value, function(k, v) {
						fillField(key + "." + k, v);
					});
				}

//				var theSelect = $("select[name='" + key + "']");
//				if ( theSelect != null ) {
//					$(theSelect).val( value );
//				}
			});
			
			$("select[name='memberType']").trigger("change");

			$( '#wrapperDialog' ).dialog('open');
			
			if(isSupplement) {
				var parent = inReturnEntity.parent + " - " + inReturnEntity.firstName + " " + inReturnEntity.lastName;
				$("#contentParentId").html(parent);
				if(inReturnEntity.companyName == null) {
					$("#contentMemberType").html("INDIVIDUAL");
				}
				else {
					$("#contentMemberType").html("COMPANY");
					$("#contentCompanyName > div").show();
				}
				
				$("#memberTypeRadio > div").hide();
				$("#memberTypeText > div").show();
			}
			else {
				$("#contentParentId").html("none");
				$("#memberTypeRadio > div").show();
				
				if(inReturnEntity.memberType != null && inReturnEntity.memberType.description == "COMPANY") {
					$("#contentCompanyName > div").show();
				}
				$("#memberTypeText > div").hide();
			}
		},
		error : function(jqXHR, textStatus, errorThrown) {
			$("#wrapperDialog").find( CONTAINER_ERROR ).html("Error: " + errorThrown);
		}
	});
}

function fillField(key, value) {
	var name = "[name='" + key + "']";
	var inputField = "";
	inputField = $("input[name='" + key + "']");
	

	if(({}).toString.call(value) == "[object Object]" && value.code !== undefined) {
		value = value.code;
	}
	
	if($.isArray(value)) {
		var proxy = [];
		for(var i = 0; i < value.length; i++) {
			proxy.push(value[i].code);
		}
		
		value = proxy;
	}
	
	if ( inputField != null ) {
		if ( $(inputField).is(':radio') ) {
			$("input:radio[name='" + key + "']").each( function() {
				$(this).attr( 'checked', $(this).val() == value );
				if($(this).val() == value) {
					$(this).click();
				}
			});
		}
//		else if($(inputField).attr('type') == "hidden") {
//			if(jQuery.type(value) === "boolean" && value == true) {
//				$("input[name='hidden_" + key + "']").prop('checked', true);
//			}
//		}
		else {
			$(name).val( value );
		}
	}
}


function doAjaxControllerSaveMember( inAjaxController, inQueryString ) {

    var theParentForm = $("form[id='" + ID_MEMBER_UPDATEFORM + "']");
    
    $("#wrapperDialog").find("#proceed").unbind("click").click( function() {
	    $.ajax({
			type 	: "POST",
			url 	: $(theParentForm).attr("action") + inAjaxController,
			dataType: 'json',
			data 	: $(theParentForm).serialize(),
			        
			success : function(inResponse) {
				if (inResponse.success) {
					$("#wrapperDialog").find( CONTAINER_ERROR ).hide('fast');
					$(theParentForm).unbind("submit").submit( function() {
						$(this).attr("action", $(this).attr("action") + "?" + inQueryString);
					});
					$(theParentForm).submit();
				} 
				else {
					errorInfo = "";
					for (i = 0; i < inResponse.result.length; i++) {
						errorInfo += "<br>" + (i + 1) + ". "
								+ ( inResponse.result[i].code != undefined ? 
										inResponse.result[i].code : inResponse.result[i]);
					}
					$("#wrapperDialog").find( CONTAINER_ERROR ).html("Please correct following errors: " + errorInfo);
					$("#wrapperDialog").find( CONTAINER_ERROR ).show('slow');
				}
			},
			error : function(jqXHR, textStatus, errorThrown) {
				$( CONTENT_ERROR ).html("Error: " + errorThrown);
			}
		});
	});


    $("#wrapperDialog").find("#cancelButton").click( function() {
    	$(theParentForm).trigger( "reset" );
    });
}


