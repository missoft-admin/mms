package com.transretail.crm.giftcard.service.impl;

import java.util.*;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import com.google.common.collect.Sets;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.mysema.query.jpa.JPQLQuery;
import com.mysema.query.jpa.impl.JPAQuery;
import com.mysema.query.types.expr.BooleanExpression;
import com.transretail.crm.common.service.dto.request.PagingParam;
import com.transretail.crm.common.service.dto.response.ResultList;
import com.transretail.crm.common.service.exception.MessageSourceResolvableException;
import com.transretail.crm.common.service.util.SpringDataPagingUtil;
import com.transretail.crm.core.entity.lookup.LookupDetail;
import com.transretail.crm.core.security.util.UserUtil;
import com.transretail.crm.core.service.CodePropertiesService;
import com.transretail.crm.core.service.LookupService;
import com.transretail.crm.core.service.StoreService;
import com.transretail.crm.giftcard.dto.GiftCardBurnCardDto;
import com.transretail.crm.giftcard.dto.GiftCardBurnCardSearchDto;
import com.transretail.crm.giftcard.dto.QGiftCardBurnCardDto;
import com.transretail.crm.giftcard.entity.QGiftCardBurnCard;
import com.transretail.crm.giftcard.entity.QGiftCardInventory;
import com.transretail.crm.giftcard.entity.GiftCardBurnCard;
import com.transretail.crm.giftcard.entity.GiftCardInventory;
import com.transretail.crm.giftcard.entity.support.GiftCardBurnCardStatus;
import com.transretail.crm.giftcard.entity.support.GiftCardInventoryStatus;
import com.transretail.crm.giftcard.repo.GiftCardBurnCardRepo;
import com.transretail.crm.giftcard.repo.GiftCardInventoryRepo;
import com.transretail.crm.giftcard.service.GiftCardBurnCardService;
import com.transretail.crm.giftcard.service.GiftCardInventoryService;
import com.transretail.crm.giftcard.service.GiftCardInventoryStockService;

/**
 * @author ftopico
 */
@Service("giftCardBurnCardService")
public class GiftCardBurnCardServiceImpl implements GiftCardBurnCardService {

    @Autowired
    private GiftCardBurnCardRepo repo;
    @Autowired
    private GiftCardInventoryRepo inventoryRepo;
    @Autowired
    private GiftCardInventoryStockService inventoryStockService;
    @Autowired
    private GiftCardInventoryService giftCardInventoryService;
    @Autowired
    private StoreService storeService;
    @Autowired
    private LookupService lookupService;
    @Autowired
    private CodePropertiesService codePropertiesService;
    @PersistenceContext
    private EntityManager em;
    
    @Override
    public ResultList<GiftCardBurnCardDto> searchGiftCardBurnCard(
            GiftCardBurnCardSearchDto searchDto) {
        final PagingParam pagination = searchDto.getPagination();
        Pageable pageable = SpringDataPagingUtil.INSTANCE.createPageable(pagination);
        
        BooleanExpression filter = searchDto.createSearchExpression();
        
        QGiftCardBurnCard qModel = QGiftCardBurnCard.giftCardBurnCard;
        JPQLQuery query = new JPAQuery(em).from(qModel);
        
        QGiftCardBurnCardDto result = new QGiftCardBurnCardDto(
                qModel.lastUpdated,
                qModel.burnNo,
                qModel.storeId,
                qModel.dateFiled,
                qModel.filedBy,
                qModel.burnReason,
                qModel.status);
        
        if (filter != null) {
            query.where(filter);
        }
        
        query.groupBy(
                qModel.lastUpdated,
                qModel.burnNo,
                qModel.storeId,
                qModel.dateFiled,
                qModel.filedBy,
                qModel.burnReason,
                qModel.status);

        final Map<String, Boolean> order = pagination.getOrder();
        if(order.keySet().contains("store")){
            order.remove("store");
        }
        
        long count =  Sets.newHashSet(query.list(result)).size();
        SpringDataPagingUtil.INSTANCE.applyPagination(query, pagination, GiftCardBurnCard.class);
        Set<GiftCardBurnCardDto> dtoList = Sets.newHashSet(query.distinct().list(result));
        return new ResultList<GiftCardBurnCardDto>(updateDto(dtoList), count, pageable.getPageNumber(), pageable.getPageSize());
    }

    private Collection<GiftCardBurnCardDto> updateDto(Set<GiftCardBurnCardDto> dtos) {

        if (CollectionUtils.isEmpty(dtos)) {

            return new HashSet<GiftCardBurnCardDto>();
        }
        
        for (GiftCardBurnCardDto dto : dtos) {

            QGiftCardBurnCard qGiftCardBurnCard = QGiftCardBurnCard.giftCardBurnCard;
            Long quantity = repo.count(qGiftCardBurnCard.burnNo.eq(dto.getBurnNo()));
            dto.setQuantity(quantity.intValue());
            LookupDetail detail = lookupService.getDetailByCode(dto.getBurnReason());
            if (detail != null)
                dto.setBurnReasonDescription(detail.getDescription());
            dto.setStore(storeService.findById(dto.getStoreId()).getCodeAndName());
        }

        return dtos;
    }

    @Transactional
    @Override
    public void saveBurncardRequest(GiftCardBurnCardDto dto) throws MessageSourceResolvableException {
        
        List<GiftCardBurnCard> listGiftCardBurnCard = new  ArrayList<GiftCardBurnCard>();
        QGiftCardBurnCard qModel = QGiftCardBurnCard.giftCardBurnCard;
        Iterable<GiftCardBurnCard> list = repo.findAll(qModel.burnNo.eq(dto.getBurnNo()));
        
        if (dto.getCards() == null)  {
            throw new MessageSourceResolvableException("gc.burncard.error.cards.empty", null, 
                    "There should be at least 1 gift card listed for burning.");
        }
        
        if (dto.getCards().size() > 0) {
            for (String seriesNo : dto.getCards()) {
                isCardSubmitted(seriesNo, dto.getBurnNo());
            }
                    
            for (GiftCardBurnCard gcBurnCard : list) {
                String series = gcBurnCard.getInventory().getSeries().toString();
                if (dto.getCards().contains(series)) {
                    dto.getCards().remove(series);
                } else {
                    repo.delete(gcBurnCard);
                }
            }
        }
        
        list = repo.findAll(qModel.burnNo.eq(dto.getBurnNo()));
        
        if (dto.getStatus().equals(GiftCardBurnCardStatus.FORAPPROVAL.toString()) && list.iterator().hasNext()) {
            for (GiftCardBurnCard model : list) {
                model.setStatus(GiftCardBurnCardStatus.FORAPPROVAL);
                repo.save(model);
            }
        }
        
        for (String seriesNumber : dto.getCards()) {
            GiftCardInventory gcInventory = giftCardInventoryService.getGiftCardBySeries(Long.valueOf(seriesNumber));
            if (gcInventory != null) {
                GiftCardBurnCard model = new GiftCardBurnCard();
                model.setBurnNo(dto.getBurnNo());
                model.setBurnReason(dto.getBurnReason());
                model.setFiledBy(dto.getFiledBy());
                model.setDateFiled(dto.getDateFiled());
                model.setTimeFiled(dto.getTimeFiled());
                if (dto.getStatus().equals(GiftCardBurnCardStatus.FORAPPROVAL.toString()))
                    model.setStatus(GiftCardBurnCardStatus.FORAPPROVAL);
                else if (dto.getStatus().equals(GiftCardBurnCardStatus.DRAFT.toString()))
                    model.setStatus(GiftCardBurnCardStatus.DRAFT);
                model.setStoreId(UserUtil.getCurrentUser().getStore().getId());
                model.setInventory(gcInventory);
                listGiftCardBurnCard.add(model);
            }
        }
        
        repo.save(listGiftCardBurnCard);
    }

    @Override
    public String createBurnCardNumber() {
        return repo.createBurnCardNumber();
    }

    @Override
    public GiftCardBurnCardDto getBurnCardRequest(String burnNo) {
        QGiftCardBurnCard qModel = QGiftCardBurnCard.giftCardBurnCard;
        Iterable<GiftCardBurnCard> list = repo.findAll(qModel.burnNo.eq(burnNo));
        
        GiftCardBurnCardDto dto = new GiftCardBurnCardDto();
        
        List<String> cards = new ArrayList<String>();
        for (GiftCardBurnCard model : list) {
            if (dto.getBurnNo() == null) {
                dto = new GiftCardBurnCardDto(model);
            }
            cards.add(model.getInventory().getSeries().toString());
        }
        dto.setCards(cards);
        return  dto;
    }

    @Override
    public void rejectRequest(GiftCardBurnCardDto dto) {
        QGiftCardBurnCard qModel = QGiftCardBurnCard.giftCardBurnCard;
        Iterable<GiftCardBurnCard> list = repo.findAll(qModel.burnNo.eq(dto.getBurnNo()));
        
        List<GiftCardBurnCard> modelList = new ArrayList<GiftCardBurnCard>();
        for (GiftCardBurnCard model : list) {
            model.setStatus(GiftCardBurnCardStatus.REJECTED);
            modelList.add(model);
        }
        repo.save(modelList);
    }

    @Transactional
    @Override
    public void burnCards(GiftCardBurnCardDto dto) {
        GiftCardBurnCardDto gcBurnCardDto = getBurnCardRequest(dto.getBurnNo());
        
        QGiftCardBurnCard qModel = QGiftCardBurnCard.giftCardBurnCard;
        Iterable<GiftCardBurnCard> list = repo.findAll(qModel.burnNo.eq(dto.getBurnNo()));
        
        if (list.iterator().hasNext()) {
            for (GiftCardBurnCard model : list) {
                model.setStatus(GiftCardBurnCardStatus.BURNED);
                model.setApprovedBy(UserUtil.getCurrentUser().getUsername());
                repo.save(model);
            }
        }
        
        List<String> cards = gcBurnCardDto.getCards();
        List<GiftCardInventory> inventories = new ArrayList<GiftCardInventory>();
        for (String series : cards) {
            GiftCardInventory gc = inventoryRepo.findOne(QGiftCardInventory.giftCardInventory.series.eq(Long.valueOf(series)));
            gc.setStatus(GiftCardInventoryStatus.BURNED);
            gc.setBalance(0.00);
            inventoryRepo.save(gc);
            inventories.add(gc);
        }
        inventoryStockService.saveStocks(inventories);
    }
    
    @Override
    public void isCardSubmitted(String seriesNo, String burnNo) throws MessageSourceResolvableException {
        GiftCardInventory gc = inventoryRepo.findOne(QGiftCardInventory.giftCardInventory.series.eq(Long.valueOf(seriesNo)));
        QGiftCardBurnCard qModel = QGiftCardBurnCard.giftCardBurnCard;
        GiftCardBurnCard burnCardRequest = repo.findOne(qModel.inventory.id.eq(gc.getId())
                                                            .and(qModel.burnNo.ne(burnNo))
                                                            .and(qModel.status.ne(GiftCardBurnCardStatus.REJECTED)));
        if (burnCardRequest != null) {
            String[] args = {seriesNo, burnCardRequest.getBurnNo()};
            throw new MessageSourceResolvableException("gc.burncard.error.card.already.submitted", args, 
                    "Series " + seriesNo + " is already part of "
                    + burnCardRequest.getBurnNo() + "burn card request. Remove it from "
                    + burnCardRequest.getBurnNo() + "to continue.");
        }
    }
}