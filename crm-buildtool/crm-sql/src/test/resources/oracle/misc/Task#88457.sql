-- Task #88457 - DB Script to handle updating uploaded Professional Member accounts with assigned Loyalty Cards
UPDATE CRM_LC_INVENTORY lc SET lc.status = 'LINV005', lc.expiry_date = ADD_MONTHS(trunc(CURRENT_DATE,'MON'), 14)
WHERE EXISTS (SELECT ID FROM CRM_MEMBER m WHERE m.id = lc.barcode and created_from = 'FROM_UPLOAD');