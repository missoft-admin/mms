-- https://projects.exist.com/issues/show/97783
ALTER TABLE crm_store_psoft DISABLE CONSTRAINT FK_t0ia1pex0dgdbbi4mpo7yyeg9;

update crm_ref_lookup_dtl set code='ID030', description='Carrefour' where code='STBU030';

update crm_store_psoft set bu_code='ID030' where bu_code='STBU030';

ALTER TABLE crm_store_psoft ENABLE CONSTRAINT FK_t0ia1pex0dgdbbi4mpo7yyeg9;

update CRM_FOR_PEOPLESOFT set bu='ID030' where bu='STBU030';

-- https://projects.exist.com/issues/show/98006
INSERT INTO CRM_REF_LOOKUP_DTL(CODE, DESCRIPTION, HDR, STATUS) VALUES ('ID050', 'ARI', 'STO000', 'ACTIVE');
INSERT INTO CRM_REF_LOOKUP_DTL(CODE, DESCRIPTION, HDR, STATUS) VALUES ('ID110', 'TGI', 'STO000', 'ACTIVE');



