package com.transretail.crm.giftcard.service.impl;

import java.math.BigDecimal;
import java.util.HashSet;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import net.sf.jasperreports.engine.data.JRBeanArrayDataSource;

import org.apache.commons.lang3.StringUtils;
import org.joda.time.DateTime;
import org.joda.time.LocalDateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Lists;
import com.mysema.query.BooleanBuilder;
import com.mysema.query.jpa.JPASubQuery;
import com.mysema.query.jpa.impl.JPAQuery;
import com.mysema.query.types.Predicate;
import com.transretail.crm.common.reporting.jasper.ClasspathFileResolver;
import com.transretail.crm.common.reporting.jasper.DefaultJRProcessor;
import com.transretail.crm.common.reporting.jasper.JRProcessor;
import com.transretail.crm.common.service.dto.request.PageSortDto;
import com.transretail.crm.common.service.dto.request.PagingParam;
import com.transretail.crm.common.service.dto.response.ResultList;
import com.transretail.crm.common.service.util.SpringDataPagingUtil;
import com.transretail.crm.core.entity.ApplicationConfig;
import com.transretail.crm.core.entity.enums.AppConfigDefaults;
import com.transretail.crm.core.entity.enums.AppKey;
import com.transretail.crm.core.entity.lookup.LookupDetail;
import com.transretail.crm.core.entity.lookup.QLookupDetail;
import com.transretail.crm.core.repo.ApplicationConfigRepo;
import com.transretail.crm.core.security.model.impl.CustomSecurityUserDetailsImpl;
import com.transretail.crm.core.security.util.UserUtil;
import com.transretail.crm.core.service.CodePropertiesService;
import com.transretail.crm.core.util.BahasaNumberConverterUtil;
import com.transretail.crm.giftcard.dto.B2BExhibitionCountDto;
import com.transretail.crm.giftcard.dto.B2BExhibitionDto;
import com.transretail.crm.giftcard.dto.B2BExhibitionGcDto;
import com.transretail.crm.giftcard.dto.GiftCardTransactionInfo;
import com.transretail.crm.giftcard.dto.QB2BExhibitionCountDto;
import com.transretail.crm.giftcard.dto.QB2BExhibitionDto;
import com.transretail.crm.giftcard.entity.B2BExhibition;
import com.transretail.crm.giftcard.entity.QB2BExhibition;
import com.transretail.crm.giftcard.entity.QGiftCardInventory;
import com.transretail.crm.giftcard.entity.support.GiftCardSalesType;
import com.transretail.crm.giftcard.repo.B2BExhibitionRepo;
import com.transretail.crm.giftcard.service.B2bExhibitionService;
import com.transretail.crm.giftcard.service.GiftCardAccountingService;
import com.transretail.crm.giftcard.service.GiftCardManager;

@Service
@Transactional
public class B2BExhibitionServiceImpl implements B2bExhibitionService {
	@PersistenceContext
	private EntityManager em;
	@Autowired
	private CodePropertiesService codePropertiesService;
	@Autowired
	private B2BExhibitionRepo repo;
	@Autowired
	private GiftCardManager gcManager;
	@Autowired
	private ApplicationConfigRepo appConfigRepo;
	@Autowired
	private GiftCardAccountingService accountingService;
	
	private static final DateTimeFormatter FULL_DATE_PATTERN = DateTimeFormat.forPattern("MMMM d, yyyy");
	private final String B2BEX_PREFIX = "BBEX";
	
	@Override
	@Transactional(readOnly = true)
	public List<Long> getProdProfByBarcode(String startingSeries, String endingSeries) {
		QGiftCardInventory qInventory = QGiftCardInventory.giftCardInventory;
		Predicate filter = null;
		if(StringUtils.isNotBlank(endingSeries)) {
			filter = qInventory.barcode.between(startingSeries, endingSeries);
		} else {
			filter = qInventory.barcode.eq(startingSeries);
		}
		return new JPAQuery(em).from(qInventory).where(filter).list(qInventory.profile.id);
	}
	
	@Override
	@Transactional(readOnly = true)
	public List<B2BExhibitionCountDto> countProducts(B2BExhibitionDto exhibitionForm) {
		QGiftCardInventory qInventory = QGiftCardInventory.giftCardInventory;
		BooleanBuilder filter = new BooleanBuilder();
		
		List<B2BExhibitionGcDto> gcs = exhibitionForm.getGcs(); 
		
		for(B2BExhibitionGcDto gcDto: gcs) {
			if(gcDto != null) {
				if(StringUtils.isNotBlank(gcDto.getEndingSeries())) {
					filter.or(qInventory.barcode.goe(gcDto.getStartingSeries()).and(qInventory.barcode.loe(gcDto.getEndingSeries())));
				} else if(StringUtils.isNotBlank(gcDto.getStartingSeries())) {
					filter.or(qInventory.barcode.eq(gcDto.getStartingSeries()));
				}	
			}
			
		}
		
		List<B2BExhibitionCountDto> counts = new JPAQuery(em).from(qInventory)
				.where(filter)
				.groupBy(qInventory.profile.productDesc, qInventory.profile.id, qInventory.faceValue)
				.list(new QB2BExhibitionCountDto(qInventory.profile.id, qInventory.profile.productDesc, qInventory.count(), qInventory.faceValue.sum(), qInventory.faceValue)); 
		
		
		for(B2BExhibitionCountDto cnt: counts) {
			cnt.setFilteredGcs(gcs);
		}
		
		return counts;
	}
	@Override
	@Transactional(readOnly = true)
	public List<LookupDetail> getPaymentTypes() {
		QLookupDetail qDtl = QLookupDetail.lookupDetail;
		return new JPAQuery(em).from(qDtl)
				.where(qDtl.code.in(new JPASubQuery().from(qDtl)
						.where(qDtl.header.code.eq(codePropertiesService.getHdrExhibitionPaymentTypes()))
						.list(qDtl.description)))
					.list(qDtl);
	}
	@Override
	@Transactional
	public B2BExhibitionDto save(B2BExhibitionDto dto) {
		B2BExhibitionDto exhDto = saveExhTransaction(dto);
		accountingService.forB2BExhibition(exhDto);
		activateGcs(exhDto);
		return exhDto;
	}
	
	@Override
	@Transactional
	public B2BExhibitionDto saveExhTransaction(B2BExhibitionDto dto) {
		DateTime created = new DateTime();
		dto.setCreated(created);
		dto.setTransactionNo(generateTransactionNo(created));
		dto.setKwitansiNo(generateKwitansiNo());
		dto.setB2bLocation(UserUtil.getCurrentUser().getB2bLocation());
		return new B2BExhibitionDto(repo.save(dto.toModel()));
	}
	@Override
	public void activateGcs(B2BExhibitionDto dto) {
		QGiftCardInventory qInventory = QGiftCardInventory.giftCardInventory;
		BooleanBuilder filter = new BooleanBuilder();
		for(B2BExhibitionGcDto gcDto: dto.getGcs()) {
			if(gcDto != null) {
				if(StringUtils.isNotBlank(gcDto.getEndingSeries())) {
					filter.or(qInventory.barcode.goe(gcDto.getStartingSeries()).and(qInventory.barcode.loe(gcDto.getEndingSeries())));
				} else if(StringUtils.isNotBlank(gcDto.getStartingSeries())) {
					filter.or(qInventory.barcode.eq(gcDto.getStartingSeries()));
				}	
			}
			
		}
		
		List<String> barcodes = new JPAQuery(em).from(qInventory)
				.where(filter).distinct().list(qInventory.barcode);
		
		String merchantId = getCurrentUserStore();
		String transactionNo = GiftCardManager.TXNNO_DT_FORMATTER.print(dto.getCreated());
		LocalDateTime transactionDate = dto.getCreated().toLocalDateTime();
		
		GiftCardTransactionInfo info = new GiftCardTransactionInfo()
		.merchantId(merchantId)
		.transactionNo(transactionNo)
		.transactionTime(transactionDate);
		
		gcManager.activate(info, new HashSet<String>(barcodes), GiftCardSalesType.B2B, false);
	}
	
	private String getCurrentUserStore() {
		CustomSecurityUserDetailsImpl userDetails = UserUtil.getCurrentUser();
		return userDetails.getInventoryLocation();
	}
	
	@Override
	public B2BExhibitionDto getExhTransaction(Long id) {
		B2BExhibitionDto dto = new B2BExhibitionDto(repo.findOne(id));
		dto.setCountSummary(countProducts(dto));
		return dto;
	}
	@Override
	public BigDecimal getTotalFaceAmount(List<B2BExhibitionCountDto> cnts) {
		BigDecimal totalFcAmt = BigDecimal.ZERO;
		for(B2BExhibitionCountDto cntDto: cnts) {
			totalFcAmt = totalFcAmt.add(cntDto.getTotalAmount());
		}
		return totalFcAmt;
	}
	
	@Override
	public ResultList<B2BExhibitionDto> listExhTransaction(PageSortDto sortDto) {
		PagingParam pagination = sortDto.getPagination();
		QB2BExhibition qEx = QB2BExhibition.b2BExhibition;
		JPAQuery q = new JPAQuery(em).from(qEx);
		Long totalElements = q.count();
		SpringDataPagingUtil.INSTANCE.applyPagination(q, pagination, B2BExhibition.class);
		List<B2BExhibitionDto> results = q.orderBy(qEx.created.desc()).list(new QB2BExhibitionDto(qEx.id, qEx.created));
		return new ResultList<B2BExhibitionDto>(results, totalElements, pagination.getPageNo(), pagination.getPageSize());
	}
	
	@Override
	public JRProcessor createJrProcessor(Long id) {
    	B2BExhibitionDto exh = getExhTransaction(id);
    	for(B2BExhibitionCountDto cnt: exh.getCountSummary()) {
    		StringBuilder gcStr = new StringBuilder();
    		for(B2BExhibitionGcDto gcDto: cnt.getGcs()) {
    			if(gcStr.length() != 0)
    				gcStr.append(" ");
    			gcStr.append(gcDto.getStartingSeries() + " - " + gcDto.getEndingSeries());
    		}
    		cnt.setCardsStr(gcStr.toString());
    	}
        DefaultJRProcessor jrProcessor = new DefaultJRProcessor("reports/b2b_exhibition_receipt.jasper", new JRBeanArrayDataSource(new B2BExhibitionDto[]{exh}));
        jrProcessor.setFileResolver(new ClasspathFileResolver("reports"));
        return jrProcessor;
    }
	@Override
	public JRProcessor createKwitansiJrProcessor(Long id) {
		B2BExhibitionDto exh = getExhTransaction(id);
		
		BigDecimal totalFaceAmt = BigDecimal.ZERO;
		
    	for(B2BExhibitionCountDto cnt: exh.getCountSummary()) {
    		StringBuilder gcStr = new StringBuilder();
    		for(B2BExhibitionGcDto gcDto: cnt.getGcs()) {
    			if(gcStr.length() != 0)
    				gcStr.append(" ");
    			gcStr.append(gcDto.getStartingSeries() + " - " + gcDto.getEndingSeries());
    		}
    		cnt.setCardsStr(gcStr.toString());
    		totalFaceAmt = totalFaceAmt.add(cnt.getTotalAmount());
    	}
    	
    	BigDecimal totalDiscount = totalFaceAmt.multiply(exh.getDiscount().divide(new BigDecimal(100)));
    	BigDecimal totalAmount = totalFaceAmt.subtract(totalDiscount);
    	BigDecimal totalPayment = exh.getTotalPayment();
    	BigDecimal change = totalPayment.subtract(totalAmount);
    	
    	ImmutableMap.Builder<String, Object> imParams = ImmutableMap.<String, Object>builder()
    			.put("tableReport", exh.getCountSummary())
    			.put("totalAmountWords", BahasaNumberConverterUtil.translateNumber(exh.getTotalAmountDue().longValue()))
    			.put("totalFaceAmt", totalFaceAmt)
    			.put("totalDiscount", totalDiscount)
    			.put("totalPrintFee", BigDecimal.ZERO)
    			.put("shippingFee", BigDecimal.ZERO)
    			.put("totalAmount", totalAmount)
    			.put("totalPayment", totalPayment)
    			.put("printDate", FULL_DATE_PATTERN.print(new LocalDateTime()))
    			.put("receiptNo", exh.getKwitansiNo())
    			.put("b2bLocation", exh.getB2bLocation())
    			.put("createdUser", exh.getCreatedUser())
    			.put("change", change);
    	
    	DefaultJRProcessor jrProcessor = new DefaultJRProcessor("reports/b2b_exhibition_kwitansi.jasper");
    	jrProcessor.addParameters(imParams.build());
    	jrProcessor.setFileResolver(new ClasspathFileResolver("reports"));
        return jrProcessor;
	}
	
	private String generateTransactionNo(DateTime created) {
		Long userId = UserUtil.getCurrentUser().getUserId();
		String createdStr = created.toString("yyyyMMddHHmmss");
		return B2BEX_PREFIX + Long.toString(userId) + createdStr;
	}
	
	private String generateKwitansiNo() {
		String prefix = AppConfigDefaults.DEFAULT_KWITANSI_TXN_PREFIX;
		ApplicationConfig config = appConfigRepo.findByKey(AppKey.KWITANSI_TXN_PREFIX);
		if(config != null && StringUtils.isNotBlank(config.getValue())) {
			prefix = config.getValue();
		}
		String controlNo = AppConfigDefaults.DEFAULT_KWITANSI_TXN_CONTROL_NO; 
		config = appConfigRepo.findByKey(AppKey.KWITANSI_TXN_CONTROL_NO);
		if(config != null && StringUtils.isNotBlank(config.getValue())) {
			controlNo = config.getValue();
		}
		QB2BExhibition qEx = QB2BExhibition.b2BExhibition;
		String lastTxNo = new JPAQuery(em).from(qEx).singleResult(qEx.kwitansiNo.max());
		if(StringUtils.isNotBlank(lastTxNo)) {
			int lastTxnInd = Integer.parseInt(lastTxNo.replaceFirst("^.*\\D",""));
			return prefix + String.format("%0"+controlNo.length()+"d", lastTxnInd + 1);
		}
		
		return prefix + controlNo;
		
	}
}
