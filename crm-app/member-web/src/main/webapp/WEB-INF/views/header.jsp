<%@ include file="taglibs.jsp" %>
<div id="header">
  <spring:url var="banner" value="/images/carrefour/carrefouridlg.png"/>
  <spring:url var="personal" value="/"/>
  <spring:message code="button_home" var="home_label" htmlEscape="false"/>
  <a href="${personal}" name="${fn:escapeXml(home_label)}" title="${fn:escapeXml(home_label)}">
    <img src="${banner}" width="180px"/>
  </a>
</div>
