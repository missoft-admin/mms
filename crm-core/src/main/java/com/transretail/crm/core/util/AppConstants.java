package com.transretail.crm.core.util;

import java.util.regex.Pattern;

public enum AppConstants {
    INSTANCE;

    public static final String PASSWORD_PATTERN = "((?=.*\\d)(?=.*[A-Z]).{8,30})";

    public static final String EMAIL_PATTERN =
        "^$|^([0-9a-zA-Z]([-.\\w]*[0-9a-zA-Z])*@([0-9a-zA-Z][-\\w]*[0-9a-zA-Z]\\.)+[a-zA-Z]{2,9})";

    public static final String NPWP_ID_PATTERN = "^$|^\\d{14,15}$|^\\d?\\d\\.\\d{3}\\.\\d{3}\\.\\d-\\d{3}\\.\\d{3}$";

    public static final String MONTH_PATTERN = "^((\\d)|(0\\d)|(1[012]?))$";
    public static final String YEAR_PATTERN = "^\\d{4}$";
    public static final Pattern REGEX_MONTH = Pattern.compile(MONTH_PATTERN);
    public static final Pattern REGEX_YEAR = Pattern.compile(YEAR_PATTERN);
    public static final Pattern REGEX_PASSWORD = Pattern.compile(PASSWORD_PATTERN);

    public static final String HEADER_CUSTMEMBERFLD_PFX = "CUSTMEMBERFLD";
}
