package com.transretail.crm.core.entity;

import java.security.SecureRandom;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Random;
import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Configurable;
import org.springframework.stereotype.Component;

import com.transretail.crm.core.entity.UserModel;
import com.transretail.crm.core.repo.UserRepo;
import com.transretail.crm.core.service.UserService;

@Configurable
@Component
public class UserModelDataOnDemand {
    @Autowired
    UserService userService;
    @Autowired
    UserRepo userRepo;
    private Random rnd = new SecureRandom();
    private List<UserModel> data;

    public UserModel getNewTransientUserModel(int index) {
        UserModel obj = new UserModel();
        setEnabled(obj, index);
        setPassword(obj, index);
        setUsername(obj, index);
        setStoreCode(obj, index);
        return obj;
    }

    public void setEnabled(UserModel obj, int index) {
        Boolean enabled = Boolean.TRUE;
        obj.setEnabled(enabled);
    }

    public void setPassword(UserModel obj, int index) {
        String password = "password_" + index;
        obj.setPassword(password);
    }

    public void setUsername(UserModel obj, int index) {
        String username = "username_" + index;
        obj.setUsername(username);
    }

    public void setStoreCode(UserModel obj, int index) {
        String storeCode = "store_" + index;
        obj.setStoreCode(storeCode);
    }

    public UserModel getSpecificUserModel(int index) {
        init();
        if (index < 0) {
            index = 0;
        }
        if (index > (data.size() - 1)) {
            index = data.size() - 1;
        }
        UserModel obj = data.get(index);
        Long id = obj.getId();
        return userService.findUserModel(id);
    }

    public UserModel getRandomUserModel() {
        init();
        UserModel obj = data.get(rnd.nextInt(data.size()));
        Long id = obj.getId();
        return userService.findUserModel(id);
    }

    public boolean modifyUserModel(UserModel obj) {
        return false;
    }

    public void init() {
        int from = 0;
        int to = 10;
        data = userService.findUserModelEntries(from, to);
        if (data == null) {
            throw new IllegalStateException("Find entries implementation for 'UserModel' illegally returned null");
        }
        if (!data.isEmpty()) {
            return;
        }

        data = new ArrayList<UserModel>();
        for (int i = 0; i < 10; i++) {
            UserModel obj = getNewTransientUserModel(i);
            try {
                userService.saveUserModel(obj);
            } catch (final ConstraintViolationException e) {
                final StringBuilder msg = new StringBuilder();
                for (Iterator<ConstraintViolation<?>> iter = e.getConstraintViolations().iterator(); iter.hasNext(); ) {
                    final ConstraintViolation<?> cv = iter.next();
                    msg.append("[").append(cv.getRootBean().getClass().getName()).append(".").append(cv.getPropertyPath()).append(": ").append(cv.getMessage()).append(" (invalid value = ").append(cv.getInvalidValue()).append(")").append("]");
                }
                throw new IllegalStateException(msg.toString(), e);
            }
            userRepo.flush();
            data.add(obj);
        }
    }
}
