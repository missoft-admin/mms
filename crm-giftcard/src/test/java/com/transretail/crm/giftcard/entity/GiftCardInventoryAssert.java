package com.transretail.crm.giftcard.entity;

import org.fest.assertions.api.AbstractAssert;
import org.fest.assertions.api.Assertions;

/**
 *
 * @author Monte Cillo Co (mco@exist.com)
 */
public class GiftCardInventoryAssert extends AbstractAssert<GiftCardInventoryAssert, GiftCardInventory> {

    public GiftCardInventoryAssert(GiftCardInventory actual) {
	super(actual, GiftCardInventoryAssert.class);
    }

    public static GiftCardInventoryAssert assertThat(GiftCardInventory actual) {
	return new GiftCardInventoryAssert(actual);
    }

    public GiftCardInventoryAssert hasBalance(Double expected) {
	isNotNull();

	Assertions.assertThat(actual.getBalance())
		.overridingErrorMessage("Expecting balance <%s> but got <%s>.", expected, actual.getBalance())
		.isEqualTo(expected);

	return this;
    }

    public GiftCardInventoryAssert hasPreviousBalance(Double expected) {
	isNotNull();

	Assertions.assertThat(actual.getPreviousBalance())
		.overridingErrorMessage("Expecting previous balance <%s> but got <%s>.", expected, actual.getPreviousBalance())
		.isEqualTo(expected);
	return this;
    }
}
