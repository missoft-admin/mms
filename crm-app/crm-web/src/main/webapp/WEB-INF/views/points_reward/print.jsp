<%@ include file="../common/taglibs.jsp"%>



<button type="button" id="content_print_link" class="btn btn-print pull-left mb20"><spring:message code="label_print" /></button>

<script type="text/javascript"></script>
<div id="content_print" class="modal hide  nofly modal-dialog">

  <div id="print" class="modal-content">
    <div class="modal-header">
      <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
      <spring:message var="printArgs" code="points_label" />
      <h4 id="header" data-header="<spring:message code="label_print_args" arguments="%PRINT_ARGS%" />">
        <spring:message code="label_print_args" arguments="%PRINT_ARGS%" />
      </h4>   
    </div>

    <div class="modal-body">
      <div class="hide alert alert-error" id="content_error"><button type="button" class="close" onclick="$(this).parent().hide();">&times;</button><div><form:errors path="*"/></div></div>

      <div id="" class="form-horizontal text-center">
        <div id="dateRange" class="input-daterange" data-err-invalid="<spring:message code="err_invaliddates" />">
          <fieldset class="row-fluid pull-left">
            <label for="dateFrom"><spring:message code="label_date_start" /></label>
            <input id="dateFrom" type="text" class="input-medium" />
          </fieldset>
          <fieldset class="row-fluid pull-left">
            <label for="dateTo"><spring:message code="label_date_end" /></label>
            <input id="dateTo" type="text" class="input-medium" />
          </fieldset>
        </div>
        <fieldset class="row-fluid no-label">
          <label for=""></label>
          <select id="reportType" name="reportType" data-url="<c:url value="/report/types/list" />" ></select>
        </fieldset>
      </div>
    </div>

    <div class="text-center modal-footer">
      <button type="saveButton" class="btn btn-primary" id="printBtn" 
        data-err-emptydate="<spring:message code="points_lbl_print_err_emptydate" />"><spring:message code="label_print"/></button>
      <button type="button" class="btn" data-dismiss="modal" id="cancelButton" ><spring:message code="label_cancel" /></button>
    </div>
  </div>


<style type="text/css">
<!-- 
  .modal-footer { text-align: center; } 
  .row-fluid { width: 100px; margin-right: 5px; } 
  .no-label { width: 100px !important; padding-top: 20px; }
--></style>
<link href="<c:url value="/css/bootstrap/datepicker.css" />" rel="stylesheet" />
<script src='<c:url value="/js/bootstrap/bootstrap-datepicker.js"/>' type='text/javascript'><![CDATA[&nbsp; ]]></script>
<script type="text/javascript">
var PrintUtil = null;
$(document).ready( function() {
	  var isInit = false;
    var errorContainer = $( "#content_print" ).find( "#content_error" );

    var $dialog = $( "#content_print" );
    var $form = $dialog.find( "#print" );
    var $header = $form.find( "#header" );
    var $dateRange = $form.find( "#dateRange" );
    var $reportType = $form.find( "#reportType" );
    var $printBtn = $form.find( "#printBtn" );
    var $printLink = $( "#content_print_link" );
    var PrintCommon = {
        reset     : function( ele, isEmpty ) {
            $( ele ).find(':input').each( function() {
                switch(this.type) {
                    case 'password':
                    case 'select-one':
                    case 'text':
                    case 'textarea':
                    case 'file':
                    case 'hidden': $(this).val(''); break;
                    case 'select-multiple': isEmpty? $(this).empty() : $(this).val(""); break;
                    case 'checkbox':
                    case 'radio': this.checked = false;
                }
            });
        },
        resetOthers : function() {
            errorContainer.find( "div" ).html();
            errorContainer.hide( "slow" );
        },
        processResp : function( resp ) {
            var error = "";
            $.each( resp.result, function( key, value ) {
              error += ( ( key + 1 ) + ". " + ( value.code? value.code : value ) + "<br>" );
            });
            errorContainer.find( "div" ).html( error );
            errorContainer.show( "slow" );
        },
        processErr : function( resp ) {
            var error = "";
            error += resp;
            errorContainer.find( "div" ).html( error );
            errorContainer.show( "slow" );
        }
    };

    initDialog();
    initFields();

    function initDialog() {
        $header.html( $header.data( "header" ).replace( new RegExp( "%PRINT_ARGS%", "g" ), "" ) );
        $dialog.modal({ show : false });
        $dialog.on( "hide", function(e) { 
            if ( e.target === this ) { 
                errorContainer.hide();
                PrintCommon.reset( $form );
                PrintCommon.resetOthers();
                initDialog();
                initFields();
            }
        });
    }

    function initFields() {
    	  $printLink.unbind( "click" ).click( function(e) { PrintUtil.show(); });
        $dateRange.datepicker({ autoclose : true, format : "dd M yyyy" });
        $.get( $reportType.data( "url" ), function(resp) {
            $( resp ).each( function( key, val ) {
            	  $reportType.append( new Option( val, val ) );
            });
        }, "json" );

        $printBtn.unbind( "click" ).click( function(e) {
        	  e.preventDefault();
        	  var dateFrom = $dateRange.find( "#dateFrom" ).val()? $dateRange.find( "#dateFrom" ).datepicker( "getDate").getTime() : null;
            var dateTo = $dateRange.find( "#dateTo" ).val()? $dateRange.find( "#dateTo" ).datepicker( "getDate").getTime() : null;
        	  if ( !dateFrom || !dateTo ) {
                PrintCommon.processErr( $(this).data( "err-emptydate" ) );
        		    return false;
        	  }
        	  else if( dateFrom > dateTo ) {
        		    PrintCommon.processErr( $dateRange.data( "err-invalid" ) );
        		    return false;
        	  }
        	  PrintCommon.resetOthers();
            var printUrl = $(this).data( "url" )
	              .replace( new RegExp( "%REPORTTYPE%", "g" ), $reportType.val() )
	              .replace( new RegExp( "%DATEFROM%", "g" ), $dateRange.find( "#dateFrom" ).val() )
                .replace( new RegExp( "%DATETO%", "g" ), $dateRange.find( "#dateTo" ).val() );
	              //.replace( new RegExp( "%DATEFROM%", "g" ), $dateRange.find( "#dateFrom" ).datepicker( "getDate").getTime() )
	              //.replace( new RegExp( "%DATETO%", "g" ), $dateRange.find( "#dateTo" ).datepicker( "getDate").getTime() );
        	  if( $reportType.val() == "PDF") {
        		    window.open( printUrl, '_blank', 'toolbar=0,location=0,menubar=0' );
        	  }
        	  else {
        		    window.location.href = printUrl;
	              /* $.get( printUrl, function( resp ) {
	                  if ( !resp.success ) {
	                      PrintCommon.processResp( resp );
	                  }
	              }, "json" ); */
        	  }
        	  $dialog.modal("hide");
        });
    }

    PrintUtil = {
        init : function( headerArgs, printUrl ) {
            $header.html( $header.data( "header" ).replace( new RegExp( "%PRINT_ARGS%", "g" ), headerArgs ) );
            $printBtn.data( "url", printUrl + "/%REPORTTYPE%/%DATEFROM%/%DATETO%" );
            isInit = true;
        },
        show : function() {
        	  if ( isInit ) {
                 $dialog.modal( "show" );
        	  }
        }
    };

});
</script>


</div>