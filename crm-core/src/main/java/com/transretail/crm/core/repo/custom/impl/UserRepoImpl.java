package com.transretail.crm.core.repo.custom.impl;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.apache.commons.lang3.StringUtils;

import com.mysema.query.BooleanBuilder;
import com.mysema.query.jpa.impl.JPAQuery;
import com.mysema.query.types.expr.BooleanExpression;
import com.mysema.query.types.path.EnumPath;
import com.mysema.query.types.path.StringPath;
import com.transretail.crm.core.entity.QUserModel;
import com.transretail.crm.core.entity.QUserRolePermission;
import com.transretail.crm.core.entity.UserModel;
import com.transretail.crm.core.repo.custom.UserRepoCustom;


public class UserRepoImpl implements UserRepoCustom {

    @PersistenceContext
    EntityManager em;

    @Override
    public List<UserModel> findByRoleCode(String roleCode) {
        QUserModel qUserModel = QUserModel.userModel;
        QUserRolePermission qUserRolePermission = QUserRolePermission.userRolePermission;
        return new JPAQuery(em).from(qUserModel).innerJoin(qUserModel.rolePermissions, qUserRolePermission)
            .where(qUserRolePermission.roleId.id.eq(roleCode))
            .distinct().list(qUserModel);
    }

    @Override
    public List<UserModel> searchUsers(UserModel inUser) {

        JPAQuery theQuery = new JPAQuery(em);
        QUserModel theUser = QUserModel.userModel;

        return theQuery.from(theUser).
            where(new BooleanBuilder().orAllOf(
                isStringPropertyLikeUsingWildcard(theUser.username, inUser.getUsername()),
                isStringPropertyLikeUsingWildcard(theUser.email, inUser.getEmail())).andAnyOf(
                isStringPropertyLikeUsingWildcard(theUser.firstName, inUser.getFirstName()),
                isStringPropertyLikeUsingWildcard(theUser.lastName, inUser.getFirstName()))).
            list(theUser);
    }


    private BooleanExpression isStringPropertyLikeUsingWildcard(StringPath inStrPath, String inProp) {
        if (!StringUtils.isBlank(inProp)) {
            inProp = inProp.replaceAll("\\*", "%");
            inProp = "%" + inProp + "%";
        }
        return !StringUtils.isBlank(inProp) ? inStrPath.lower().like(inProp.toLowerCase()) : null;
    }

    @SuppressWarnings("unused")
    private BooleanExpression isStringPropertyContained(StringPath inStrPath, String inProp) {
        return !StringUtils.isBlank(inProp) ? inStrPath.lower().contains(inProp.toLowerCase()) : null;
    }

    @SuppressWarnings("unused")
    private BooleanExpression isStringPropertyEquals(StringPath inStrPath, String inProp) {
        return !StringUtils.isBlank(inProp) ? inStrPath.lower().eq(inProp.toLowerCase()) : null;
    }

    @SuppressWarnings({"rawtypes", "unused"})
    private BooleanExpression isEnumPropertyIn(EnumPath inEnumPath, Enum inProp) {
        return isEnumPropertyNotIn(inEnumPath, inProp, false);
    }

    @SuppressWarnings({"unchecked", "rawtypes"})
    private BooleanExpression isEnumPropertyNotIn(EnumPath inEnumPath, Enum inProp, boolean inIsPropNotIn) {
        return inProp != null ? (inIsPropNotIn ? inEnumPath.notIn(inProp) : inEnumPath.in(inProp)) : null;
    }

}
