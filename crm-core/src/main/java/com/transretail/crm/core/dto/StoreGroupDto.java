package com.transretail.crm.core.dto;


import com.transretail.crm.core.entity.lookup.StoreGroup;


public class StoreGroupDto {

	private Long id;
	private String name; 

	private Boolean isUsed;



	public StoreGroupDto() {}
	public StoreGroupDto( StoreGroup model ) {
        this.id = model.getId();
        this.name = model.getName();
	}

	public StoreGroup toModel() { return update( new StoreGroup() ); }
	public StoreGroup update( StoreGroup model ) {
        model.setName(this.name);

		return model;
	}



	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Boolean getIsUsed() {
		return isUsed;
	}

	public void setIsUsed(Boolean isUsed) {
		this.isUsed = isUsed;
	}

}
