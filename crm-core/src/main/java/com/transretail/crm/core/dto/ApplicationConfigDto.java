package com.transretail.crm.core.dto;

import com.transretail.crm.core.entity.ApplicationConfig;
import com.transretail.crm.core.entity.enums.AppKey;


public class ApplicationConfigDto {

    private String key; //not editable
	private String value;
	
	public ApplicationConfigDto() {}

	public ApplicationConfigDto(ApplicationConfig config) {
		this.key = config.getKey().toString();
		this.value = config.getValue();
	}
	
	public ApplicationConfig toModel(ApplicationConfig config) { //config should not be null
		config.setValue(value);
		return config;
	}
	
	public String getKey() {
		return key;
	}

	public void setKey(String key) {
		this.key = key;
	}

	public String getValue() {
		return value;
	}
	public void setValue(String value) {
		this.value = value;
	}

	
}
