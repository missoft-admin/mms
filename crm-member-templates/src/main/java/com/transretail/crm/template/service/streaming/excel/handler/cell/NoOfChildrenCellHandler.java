package com.transretail.crm.template.service.streaming.excel.handler.cell;

import org.apache.commons.lang3.StringUtils;

import com.transretail.crm.core.entity.MemberModel;
import com.transretail.crm.core.entity.embeddable.CustomerProfile;
import com.transretail.crm.template.service.MemberTemplateParseException;

/**
 * @author Allan G. Ramirez (agramirez@exist.com)
 */
public class NoOfChildrenCellHandler implements CellHandler {
    @Override
    public void handle(MemberModel dto, String cellText) throws MemberTemplateParseException {
        if (dto.getCustomerProfile() == null) {
            dto.setCustomerProfile(new CustomerProfile());
        }
        dto.getCustomerProfile().setChildren(StringUtils.isNotBlank(cellText) ? Integer.valueOf(cellText) : null);
    }
}
