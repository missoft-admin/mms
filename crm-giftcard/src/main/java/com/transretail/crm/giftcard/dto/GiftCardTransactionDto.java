package com.transretail.crm.giftcard.dto;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.transretail.crm.common.web.converter.FormattedIntegerNumberSerializer;
import com.transretail.crm.common.web.converter.LocalDateTimeSerializer;
import org.joda.time.DateTime;
import org.joda.time.LocalDateTime;

/**
 *
 * @author Monte Cillo Co (mco@exist.com)
 */
public class GiftCardTransactionDto {

    @JsonSerialize(using = LocalDateTimeSerializer.class)
    private LocalDateTime transactionTime;
    private DateTime createdDateTime;
    private String createdBy;
    private String store;
    private String terminalId;
    private String transactionNo;
    private String transactionType;
    @JsonSerialize(using = FormattedIntegerNumberSerializer.class)
    private double transactionAmount;
    @JsonSerialize(using = FormattedIntegerNumberSerializer.class)
    private double previousBalance;
    @JsonSerialize(using = FormattedIntegerNumberSerializer.class)
    private double balance;
    private String cashierId;

    public DateTime getCreatedDateTime() {
        return createdDateTime;
    }

    public void setCreatedDateTime(DateTime createdDateTime) {
        this.createdDateTime = createdDateTime;
    }

    public String getTransactionNo() {
        return transactionNo;
    }

    public LocalDateTime getTransactionTime() {
        return transactionTime;
    }

    public GiftCardTransactionDto transactionNo(String transactionNo) {
        this.transactionNo = transactionNo;
        return this;
    }

    public GiftCardTransactionDto transactionTime(LocalDateTime transactionTime) {
        this.transactionTime = transactionTime;
        return this;
    }

    public GiftCardTransactionDto createdBy(String createdBy) {
        this.createdBy = createdBy;
        return this;
    }

    public GiftCardTransactionDto previousBalance(double previousBalance) {
        this.previousBalance = previousBalance;
        return this;
    }

    public GiftCardTransactionDto balance(double balance) {
        this.balance = balance;
        return this;
    }

    public String getStore() {
        return store;
    }

    public GiftCardTransactionDto store(String store) {
        this.store = store;
        return this;
    }

    public String getTerminalId() {
        return terminalId;
    }

    public GiftCardTransactionDto terminalId(String terminalId) {
        this.terminalId = terminalId;
        return this;
    }

    public String getTransactionType() {
        return transactionType;
    }

    public GiftCardTransactionDto transactionType(String transactionType) {
        this.transactionType = transactionType;
        return this;
    }

    public double getTransactionAmount() {
        return transactionAmount;
    }

    public GiftCardTransactionDto transactionAmount(double transactionAmount) {
        this.transactionAmount = transactionAmount;
        return this;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public double getPreviousBalance() {
        return previousBalance;
    }

    public double getBalance() {
        return balance;
    }

    public String getCashierId() {
        return cashierId;
    }

    public GiftCardTransactionDto cashierId(String currency) {
        this.cashierId = currency;
        return this;
    }

    public void setTransactionTime(LocalDateTime transactionTime) {
        this.transactionTime = transactionTime;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public void setStore(String store) {
        this.store = store;
    }

    public void setTerminalId(String terminalId) {
        this.terminalId = terminalId;
    }

    public void setTransactionNo(String transactionNo) {
        this.transactionNo = transactionNo;
    }

    public void setTransactionType(String transactionType) {
        this.transactionType = transactionType;
    }

    public void setTransactionAmount(double transactionAmount) {
        this.transactionAmount = transactionAmount;
    }

    public void setPreviousBalance(double previousBalance) {
        this.previousBalance = previousBalance;
    }

    public void setBalance(double balance) {
        this.balance = balance;
    }

    public void setCashierId(String cashierId) {
        this.cashierId = cashierId;
    }

}
