package com.transretail.crm.web.controller;

import com.transretail.crm.common.reporting.jasper.JRProcessor;
import com.transretail.crm.common.util.DateUtil;
import com.transretail.crm.core.entity.enums.ReportType;
import com.transretail.crm.core.service.StoreService;
import com.transretail.crm.report.template.impl.BuyingCustomerReport;
import com.transretail.crm.web.controller.util.ControllerResponse;
import org.apache.commons.lang.StringUtils;
import org.joda.time.LocalDate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletResponse;
import java.util.*;

/**
 * @author Mike de Guzman
 */
@RequestMapping("/member/buyingcustomer")
@Controller
public class BuyingCustomerController extends AbstractReportsController {

    private static final String PATH_MEMBER_BUYING_CUSTOMER = "/member/buyingcustomer";

    @Autowired
    private BuyingCustomerReport buyingCustomerReport;

    @Autowired
    private StoreService storeService;

    @Autowired
    private MessageSource messageSource;

    @RequestMapping(produces="text/html")
    public String showBurnCardForm(Model model) {
        model.addAttribute("stores", storeService.getAllStores());
        model.addAttribute("reportTypes", ReportType.values());
        return PATH_MEMBER_BUYING_CUSTOMER;
    }

    @RequestMapping(value = "/print/validate", produces = "application/json; charset=utf-8")
    public @ResponseBody ControllerResponse printValidate(
            @RequestParam String standardReportType,
            @RequestParam String dateFrom,
            @RequestParam String dateTo,
            @RequestParam(required = false) String store,
            Model model) throws Exception {

        ControllerResponse resp = new ControllerResponse();
        resp.setSuccess(true);
        List<String> errors = new ArrayList<String>();

        if (StringUtils.isBlank(dateFrom) || StringUtils.isBlank(dateTo)) {
            errors.add(messageSource.getMessage("report.date.from.date.to.required", null, LocaleContextHolder.getLocale()));
        } else {
            Date fromDate = DateUtil.convertDateString(DateUtil.SEARCH_DATE_FORMAT, dateFrom);
            Date toDate = DateUtil.convertDateString(DateUtil.SEARCH_DATE_FORMAT, dateTo);
            if (toDate.before(fromDate)) {
                errors.add(messageSource.getMessage("report.date.from.before.date.to", null, LocaleContextHolder.getLocale()));
            }
        }

        if (!errors.isEmpty()) {
            resp.setSuccess(false);
            resp.setResult(errors);
        }

        Map<String, String> filtersMap = new HashMap<String, String>();
        filtersMap.put("standardReportType", standardReportType);
        filtersMap.put("dateFrom", dateFrom);
        filtersMap.put("dateTo", dateTo);
        filtersMap.put("store", store);

        return resp;
    }

    @RequestMapping(value="/view", method= RequestMethod.GET)
    public void print(
            @RequestParam String reportType,
            @RequestParam String standardReportType,
            @RequestParam String dateFrom,
            @RequestParam String dateTo,
            @RequestParam(required = false) String store,
            HttpServletResponse response) throws Exception {

        Map<String, String> filtersMap = new HashMap<String, String>();
        filtersMap.put("standardReportType", standardReportType);
        filtersMap.put("dateFrom", dateFrom);
        filtersMap.put("dateTo", dateTo);
        filtersMap.put("store", store);

        JRProcessor jrProcessor = buyingCustomerReport.createJrProcessor(filtersMap);

        if (reportType.equalsIgnoreCase(ReportType.PDF.name())) {
            renderReport(response, jrProcessor);
        } else if (reportType.equalsIgnoreCase(ReportType.EXCEL.name())) {
            String filename = null;

            if (standardReportType.equalsIgnoreCase("weekly")) {
                filename = "Buying Customer Weekly Report (" + LocalDate.now() +")";
            } else if (standardReportType.equalsIgnoreCase("monthly")) {
                filename = "Buying Customer Weekly Report (" + LocalDate.now() +")";
            }
            renderExcelReport(response, jrProcessor, filename);
        }
    }
}
