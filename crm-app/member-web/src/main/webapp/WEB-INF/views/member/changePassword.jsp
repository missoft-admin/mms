<%@ include file="../taglibs.jsp" %>

<spring:url value="/member/password/change" var="actionUrl"/>

<div class="page-header page-header2">
    <h1><spring:message code="menu.passwordchange"/></h1>
</div>

<form:form modelAttribute="form" method="POST" action="${actionUrl}" cssClass="form-horizontal" autocomplete="off">
  <div class="control-group">
    <form:label path="oldPassword" title="Current Password" cssClass="control-label" cssErrorClass="control-label error"><span class="required">*</span>Current Password</form:label>
    <div class="controls">
      <form:password path="oldPassword" title="Current Password"/>
    </div>
  </div>
  <div class="control-group">
    <form:label path="newPassword" title="New Password" cssClass="control-label" cssErrorClass="control-label error"><span class="required">*</span>New Password</form:label>
    <div class="controls">
      <form:password path="newPassword" title="New Password"/>
    </div>
  </div>
  <div class="control-group">
    <form:label path="confirmPassword" title="Confirm Password" cssClass="control-label" cssErrorClass="control-label error"><span class="required">*</span>Confirm Password</form:label>
    <div class="controls">
      <form:password path="confirmPassword" title="Confirm Password"/>
    </div>
  </div>
  <div class="form-actions">
    <button type="submit" class="btn btn-primary"><spring:message code="button_submit" /></button>
    <a class="btn" href="<spring:url value="/" />"><spring:message code="button_cancel" /></a>
  </div>
</form:form>
<!--<style>
  form {
    width: 500px;
    margin: 0 auto;
  }
</style>-->