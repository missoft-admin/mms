package com.transretail.crm.report.template.impl;

import java.io.File;
import java.math.BigDecimal;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.annotation.Resource;

import net.sf.jasperreports.engine.JRParameter;

import org.apache.commons.io.FileUtils;
import org.joda.time.DateTime;
import org.joda.time.LocalDate;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.context.support.MessageSourceResourceBundle;
import org.springframework.security.authentication.TestingAuthenticationToken;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.context.SecurityContextImpl;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.transretail.crm.common.reporting.exception.ReportException;
import com.transretail.crm.common.reporting.jasper.JRProcessor;
import com.transretail.crm.common.reporting.service.ReportService;
import com.transretail.crm.common.util.DateUtil;
import com.transretail.crm.core.entity.UserModel;
import com.transretail.crm.core.repo.UserRepo;
import com.transretail.crm.core.security.model.impl.CustomSecurityUserDetailsImpl;
import com.transretail.crm.core.service.CodePropertiesService;
import com.transretail.crm.giftcard.entity.CrmForPeoplesoft;
import com.transretail.crm.giftcard.repo.CrmForPeoplesoftRepo;
import com.transretail.crm.report.template.CommonReportFilter;
import com.transretail.crm.report.template.ReportTemplate;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath*:/META-INF/spring/applicationContext*.xml",
    "/META-INF/spring/applicationContext-test.xml"})
public class JournalEntriesReportTest {

	@Autowired
    private ReportService reportService;
	@Autowired
    private MessageSource messageSource;
	@Resource(name = "journalEntriesReport")
    private ReportTemplate journalEntriesReport;
	
	private static final Logger logger = LoggerFactory.getLogger(JournalEntriesReportTest.class);
	
	@Autowired
	private CodePropertiesService codePropertiesService;
	@Autowired
	private UserRepo userRepo;
	@Autowired
	private CrmForPeoplesoftRepo repo;

	
	@Before
	public void setUp() {
		
		UserModel currentUser = new UserModel();
    	currentUser.setUsername("loggedInUser");
    	currentUser.setPassword("");
    	currentUser.setEnabled(true);
    	currentUser.setInventoryLocation(codePropertiesService.getDetailInvLocationDenpasar());
    	currentUser.setStoreCode( "store1" );
    	userRepo.save(currentUser);

        CustomSecurityUserDetailsImpl user = new CustomSecurityUserDetailsImpl();
        user.setUserId(currentUser.getId());
        user.setUsername(currentUser.getUsername());
        List<GrantedAuthority> authorities = Lists.newArrayList();
        authorities.add(new SimpleGrantedAuthority("ROLE_CSS"));
        user.setAuthorities(authorities);
        user.setInventoryLocation(codePropertiesService.getDetailInvLocationDenpasar());

        SecurityContextImpl securityContext = new SecurityContextImpl();
        securityContext.setAuthentication(new TestingAuthenticationToken(user, ""));
        SecurityContextHolder.setContext(securityContext);
		
        CrmForPeoplesoft jrnl = new CrmForPeoplesoft();
		jrnl.setTransactionDate(new DateTime());
		jrnl.setTransactionAmt(new BigDecimal("100000"));
		jrnl.setType("Adv SO T. Verified");
		jrnl.setMmsAcct("23");
		jrnl.setPsAcct("28000722ID");
		jrnl.setStoreCode("10007");
		jrnl.setBusinessUnit("STO0001");
		jrnl.setNotes("CUST1 NAME");
		repo.save(jrnl);
        
		jrnl = new CrmForPeoplesoft();
		jrnl.setTransactionDate(new DateTime());
		jrnl.setTransactionAmt(new BigDecimal("-100000"));
		jrnl.setType("Cancel Adv SO T. Verified");
		jrnl.setMmsAcct("23");
		jrnl.setPsAcct("28000722ID");
		jrnl.setNotes("BB1411014");
		repo.save(jrnl);
		
	}
	
	@Test
	public void test() throws ReportException {
		
		Map<String, String> map = Maps.newHashMap();
		map.put(CommonReportFilter.DATE_FROM, new LocalDate().minusDays(1).toString(DateUtil.SEARCH_DATE_FORMAT));
		map.put(CommonReportFilter.DATE_TO, new LocalDate().plusDays(1).toString(DateUtil.SEARCH_DATE_FORMAT));
		JRProcessor jrProcessor = journalEntriesReport.createJrProcessor(map);

		File testClassesDir = FileUtils.toFile(Thread.currentThread().getContextClassLoader().getResource(""));
		File output = new File(testClassesDir, "test-journal-entries-output.pdf");
		logger.debug("Output location {}", output.getAbsolutePath());
		output.delete();
		Locale locale = LocaleContextHolder.getLocale();
		jrProcessor.addParameter(JRParameter.REPORT_LOCALE, locale);
		jrProcessor.addParameter(JRParameter.REPORT_RESOURCE_BUNDLE, new MessageSourceResourceBundle(messageSource, locale));
		reportService.exportPdfReport(jrProcessor, output);

		double bytes = output.length();
		double kilobytes = (bytes / 1024);
		logger.debug("Kilobytes: {}", kilobytes);
	}
}
