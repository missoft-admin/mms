package com.transretail.crm.core.service.impl;

import java.io.InputStream;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.joda.time.DateTime;
import org.joda.time.LocalDate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.mysema.query.BooleanBuilder;
import com.mysema.query.jpa.impl.JPAQuery;
import com.mysema.query.types.expr.BooleanExpression;
import com.transretail.crm.common.reporting.jasper.ClasspathFileResolver;
import com.transretail.crm.common.reporting.jasper.DefaultJRProcessor;
import com.transretail.crm.common.reporting.jasper.JRProcessor;
import com.transretail.crm.common.service.dto.request.PageSortDto;
import com.transretail.crm.common.service.dto.response.ResultList;
import com.transretail.crm.common.service.util.SpringDataPagingUtil;
import com.transretail.crm.core.dto.EmployeePurchaseTxnDto;
import com.transretail.crm.core.dto.MemberDto;
import com.transretail.crm.core.dto.PurchaseTxnSearchDto;
import com.transretail.crm.core.entity.EmployeePurchaseTxnModel;
import com.transretail.crm.core.entity.MemberModel;
import com.transretail.crm.core.entity.PosPayment;
import com.transretail.crm.core.entity.QEmployeePurchaseTxnModel;
import com.transretail.crm.core.entity.QPosPayment;
import com.transretail.crm.core.entity.enums.TxnStatus;
import com.transretail.crm.core.entity.enums.VoucherTransactionType;
import com.transretail.crm.core.entity.lookup.Store;
import com.transretail.crm.core.repo.EmployeePurchaseTxnRepo;
import com.transretail.crm.core.repo.LookupDetailRepo;
import com.transretail.crm.core.repo.MemberRepo;
import com.transretail.crm.core.repo.PosPaymentRepo;
import com.transretail.crm.core.security.model.impl.CustomSecurityUserDetailsImpl;
import com.transretail.crm.core.security.util.UserUtil;
import com.transretail.crm.core.service.CodePropertiesService;
import com.transretail.crm.core.service.EmployeePurchaseService;
import com.transretail.crm.core.service.EmployeeService;
import com.transretail.crm.core.service.StoreService;
import com.transretail.crm.core.util.CalendarUtil;
import com.transretail.crm.core.util.generator.IdGeneratorService;

@Service("employeePurchaseService")
@Transactional
public class EmployeePurchaseServiceImpl implements EmployeePurchaseService {
	
	@Autowired
	EmployeePurchaseTxnRepo employeePurchaseTxnRepo;
	
	@Autowired
	EmployeeService employeeService;

    @Autowired
    private LookupDetailRepo lookupDetailRepo;

    @Autowired
    private CodePropertiesService codePropertiesService;

	@Autowired
	MessageSource messageSource;
	
	@Autowired
	MemberRepo memberRepo;
	
	@Autowired
    IdGeneratorService customIdGeneratorService;
	
	@Autowired
	StoreService storeService;
	
	@Autowired
	PosPaymentRepo posPaymentRepo;

    @PersistenceContext
    private EntityManager em;
	
	@Override
	public ResultList<EmployeePurchaseTxnDto> listTransactionDtoForApproval(PageSortDto searchDto) {
		QEmployeePurchaseTxnModel purchaseTxn = QEmployeePurchaseTxnModel.employeePurchaseTxnModel;
		BooleanBuilder filter = new BooleanBuilder(purchaseTxn.transactionType.eq(VoucherTransactionType.ADJUSTED))
				.and(purchaseTxn.status.eq(TxnStatus.FORAPPROVAL));
		Pageable pageable = SpringDataPagingUtil.INSTANCE.createPageable(searchDto.getPagination());
        Page<EmployeePurchaseTxnModel> page = employeePurchaseTxnRepo.findAll(filter, pageable);
        List<EmployeePurchaseTxnDto> list = convertEmployeePurchaseTxnModelToDtoList(page.getContent());
        return new ResultList<EmployeePurchaseTxnDto>(list, page.getTotalElements(), page.hasPreviousPage(),
            page.hasNextPage());
	}
	
	@Override
    public ResultList<EmployeePurchaseTxnDto> listTransactionDto(String employeeId, PageSortDto searchDto) {
		QEmployeePurchaseTxnModel qTxn = QEmployeePurchaseTxnModel.employeePurchaseTxnModel;
		
		Calendar calFrom = CalendarUtil.INSTANCE.getFromCal(null);
		Calendar calTo = CalendarUtil.INSTANCE.getToCal(null);
		
		BooleanExpression filter = qTxn.status.eq(TxnStatus.ACTIVE)
				.and(qTxn.memberModel.accountId.eq(employeeId))
				.and( qTxn.transactionDateTime.between( calFrom.getTime(), calTo.getTime() ) )
				/*.and(qTxn.cretated.goe(new DateTime(calFrom.getTime())))
				.and(qTxn.created.loe(new DateTime(calTo.getTime())))*/;
		
		Pageable pageable = SpringDataPagingUtil.INSTANCE.createPageable(searchDto.getPagination());
        Page<EmployeePurchaseTxnModel> page = employeePurchaseTxnRepo.findAll(filter, pageable);
        return new ResultList<EmployeePurchaseTxnDto>(convertModelToDtoList(page.getContent()), page.getTotalElements(), page.hasPreviousPage(),
            page.hasNextPage());
    }
	
	@Override
	public void deletePurchaseTransaction(String id) {
		employeePurchaseTxnRepo.delete(id);
	}
	
	
	@Override
    public List<EmployeePurchaseTxnDto> findPendingPurchasesAdjustmentsEntries(int firstResult, int maxResults) {
		QEmployeePurchaseTxnModel txns = QEmployeePurchaseTxnModel.employeePurchaseTxnModel;
		
        List<EmployeePurchaseTxnModel> txn = employeePurchaseTxnRepo.findAll(
				txns.status.eq(TxnStatus.FORAPPROVAL).or( txns.status.eq(TxnStatus.REJECTED) ), 
				new PageRequest(firstResult / maxResults, maxResults)).getContent();
        if(txn != null)
    		return convertEmployeePurchaseTxnModelToDtoList(txn);
    	else 
    		return null;
    }
	
	
	
	@Override
	public EmployeePurchaseTxnDto findPurchaseTransactionById(String id) {
		EmployeePurchaseTxnModel theTxn = employeePurchaseTxnRepo.findOne(id);
		
		if(theTxn != null)
			return new EmployeePurchaseTxnDto(theTxn);
		else
			return null;
	}

    @Override
    public List<EmployeePurchaseTxnModel> findByTransactionNo(String txnNo) {

        return employeePurchaseTxnRepo.findByTransactionNo(txnNo);
    }
	
	@Override
	public EmployeePurchaseTxnDto findOneByTransactionNo(String txnNo) {
		List<EmployeePurchaseTxnModel> theTxns = employeePurchaseTxnRepo.findByTransactionNo(txnNo);
		if(theTxns == null || theTxns.size() == 0)
			return null;
		else
			return new EmployeePurchaseTxnDto(theTxns.get(0));
	}
	
	@Override
	public long countAllPendingPurchaseAdjustments() {
		return employeePurchaseTxnRepo.count(QEmployeePurchaseTxnModel
				.employeePurchaseTxnModel.status.eq(TxnStatus.FORAPPROVAL));
	}
	
	@Override
	public List<EmployeePurchaseTxnDto> findAllPendingPurchasesAdjustments() {
		return convertEmployeePurchaseTxnModelToDtoList(employeePurchaseTxnRepo.findAllPendingAdjustments());
	}

	private List<EmployeePurchaseTxnDto> convertEmployeePurchaseTxnModelToDtoList(Iterable<EmployeePurchaseTxnModel> employeePurchaseTxnModels) {
    	List<EmployeePurchaseTxnDto> employeePurchaseTxnDtos = new ArrayList<EmployeePurchaseTxnDto>();
    	for(EmployeePurchaseTxnModel employeePurchaseTxnModel : employeePurchaseTxnModels) {
    		employeePurchaseTxnDtos.add(new EmployeePurchaseTxnDto(employeePurchaseTxnModel));
    	}
    	return employeePurchaseTxnDtos;
    }
	
	private List<EmployeePurchaseTxnDto> convertModelToDtoList(Iterable<EmployeePurchaseTxnModel> employeePurchaseTxnModels) {
    	List<EmployeePurchaseTxnDto> employeePurchaseTxnDtos = new ArrayList<EmployeePurchaseTxnDto>();
    	for(EmployeePurchaseTxnModel employeePurchaseTxnModel : employeePurchaseTxnModels) {
    		if(employeePurchaseTxnModel.getTransactionType() != null && employeePurchaseTxnModel.getTransactionType().toString().equals(VoucherTransactionType.PURCHASE.toString())) {
	    		List<PosPayment> paymentTypes = getPaymentsTypes( employeePurchaseTxnModel.getTransactionNo(), employeePurchaseTxnModel.getTransactionAmount() );
	    		//getPaymentsTypes(employeePurchaseTxnModel.getTransactionNo());
	    		if(paymentTypes.size() < 1) {
	    			EmployeePurchaseTxnDto dto = new EmployeePurchaseTxnDto(employeePurchaseTxnModel);
	    			employeePurchaseTxnDtos.add(dto);
	    		} else {
	    			for(PosPayment payment : paymentTypes) {
	    				EmployeePurchaseTxnDto dto = new EmployeePurchaseTxnDto(employeePurchaseTxnModel);
	    				/*dto.setPaymentType(lookupDetailRepo.findByCode(payment.getMediaType()).getDescription());*/ //Bug #90878: description is stored in media_type column of pos_payment
	    				dto.setPaymentType(payment.getMediaType());
	    				dto.setTransactionAmount(payment.getAmount());
	    				employeePurchaseTxnDtos.add(dto);
	    			}
	    		}
    		} else {
    			EmployeePurchaseTxnDto dto = new EmployeePurchaseTxnDto(employeePurchaseTxnModel);
    			employeePurchaseTxnDtos.add(dto);
    		}
    	}
    	return employeePurchaseTxnDtos;
    }
	
	private List<PosPayment> getPaymentsTypes(String transactionNo) {
		QPosPayment qPayment = QPosPayment.posPayment;
		return Lists.newArrayList(posPaymentRepo.findAll(qPayment.posTransaction.id.eq(transactionNo)));
	}
	
	private List<PosPayment> getPaymentsTypes(String transactionNo, double amount) {
		QPosPayment qPayment = QPosPayment.posPayment;
		return Lists.newArrayList(posPaymentRepo.findAll(qPayment.posTransaction.id.eq(transactionNo).and( qPayment.amount.eq(amount))));
	}
	
	@Override
	public List<EmployeePurchaseTxnDto> getTransactionDetails(String transactionNo) {
		QEmployeePurchaseTxnModel qPurchase = QEmployeePurchaseTxnModel.employeePurchaseTxnModel;
		Iterable<EmployeePurchaseTxnModel> purchases = employeePurchaseTxnRepo.findAll(qPurchase.transactionNo.eq(transactionNo).and(qPurchase.transactionType.eq(VoucherTransactionType.PURCHASE)));
		return convertModelToDtoList(purchases);
	}
	
	@Override
	public List<EmployeePurchaseTxnDto> findPurchaseTxnByIdAndPostingDates(String employeeId, Date postingDateFrom, Date postingDateTo) {
		return convertModelToDtoList(employeePurchaseTxnRepo.findAllActiveByIdAndPostingDates(employeeId, convertDateToCalendar(postingDateFrom), convertDateToCalendar(postingDateTo)));
	}
	
	private Calendar convertDateToCalendar(Date date) {
		Calendar theCalendar = null;
		if(date != null) {
			theCalendar = Calendar.getInstance();
			theCalendar.setTime(date);	
		}
		return theCalendar;
	}
	
	@Override
	@Transactional
    public JRProcessor createPurchaseTxnJrProcessor(String empId, Date dateFrom, Date dateTo, InputStream logo) {
		MemberDto employeeDto = new MemberDto(memberRepo.findByAccountId(empId));
		employeeDto.setTotalTransactionAmount(employeePurchaseTxnRepo.findTotalTxnAmountByAccountId(employeeDto.getAccountId(), convertDateToCalendar(dateFrom), convertDateToCalendar(dateTo)));
		if(StringUtils.isNotBlank(employeeDto.getRegisteredStore())) {
			Store memberStore = storeService.getStoreByCode(employeeDto.getRegisteredStore());	
			if(memberStore != null)
				employeeDto.setRegisteredStoreName(storeService.getStoreByCode(employeeDto.getRegisteredStore()).getName());
		}
		
		SimpleDateFormat formatter = new SimpleDateFormat("MMMM d, yyyy");
		
		List<EmployeePurchaseTxnDto> txnList = findPurchaseTxnByIdAndPostingDates(empId, dateFrom, dateTo);
        Map<String, EmployeePurchaseTxnDto> txnMap = Maps.newHashMap();
		for (EmployeePurchaseTxnDto employeePurchaseTxnDto : txnList) {
            EmployeePurchaseTxnDto mapEntry = txnMap.get(employeePurchaseTxnDto.getTransactionNo());
            StringBuilder paymentType = new StringBuilder(100);

            if (null != employeePurchaseTxnDto.getPaymentType() && !employeePurchaseTxnDto.getPaymentType().equalsIgnoreCase("")) {
                paymentType.append(employeePurchaseTxnDto.getPaymentType());
                paymentType.append(": ");
                paymentType.append(employeePurchaseTxnDto.getTransactionAmount());
            }
            if (null == mapEntry) {
                mapEntry = employeePurchaseTxnDto;
                txnMap.put(employeePurchaseTxnDto.getTransactionNo(), mapEntry);
            } else {
                paymentType.append("\n");
                paymentType.append(mapEntry.getPaymentType());
                mapEntry.setTransactionAmount(mapEntry.getTransactionAmount() + employeePurchaseTxnDto.getTransactionAmount());
            }

            mapEntry.setPaymentType(paymentType.toString());
        }

		Map<String, Object> parameters = new HashMap<String, Object>();
		if(CollectionUtils.isNotEmpty(txnMap.values())) {
			parameters.put("SUB_DATA_SOURCE", txnMap.values());
		} else {
			parameters.put("isEmpty", true);
            parameters.put("noDataFound", "No data found");
			parameters.put("SUB_DATA_SOURCE", Lists.newArrayList(new EmployeePurchaseTxnDto()));
		}
		
        parameters.put("logo", logo);
        parameters.put("REPORT_TYPE", messageSource.getMessage("txn_report_label_purchase_report_type", null, LocaleContextHolder.getLocale()));
        parameters.put("EMPLOYEE", messageSource.getMessage("txn_report_label_employee", null, LocaleContextHolder.getLocale()));
        parameters.put("TXN_TOTAL", messageSource.getMessage("txn_report_label_txn_total", null, LocaleContextHolder.getLocale()));
        parameters.put("STORE", messageSource.getMessage("txn_report_label_txn_store", null, LocaleContextHolder.getLocale()));
        parameters.put("DATETIME", messageSource.getMessage("txn_report_label_txn_datetime", null, LocaleContextHolder.getLocale()));
        parameters.put("TXN_NO", messageSource.getMessage("txn_report_label_txn_no", null, LocaleContextHolder.getLocale()));
        parameters.put("AMOUNT", messageSource.getMessage("txn_report_label_txn_amount", null, LocaleContextHolder.getLocale()));
        parameters.put("PAYMENT_TYPE", messageSource.getMessage("txn_report_label_txn_paymenttype", null, LocaleContextHolder.getLocale()));
        parameters.put("TXN_TYPE", messageSource.getMessage("txn_report_label_txn_type", null, LocaleContextHolder.getLocale()));
        if(dateFrom != null)
        	parameters.put("DATE_FROM", formatter.format(dateFrom));
        if(dateTo != null)
        	parameters.put("DATE_TO", formatter.format(dateTo));
        DefaultJRProcessor jrProcessor =
            new DefaultJRProcessor("reports/employee_purchase_txn_report.jasper", Arrays.asList(employeeDto));
        jrProcessor.setParameters(parameters);
        jrProcessor.setFileResolver(new ClasspathFileResolver("reports"));
        return jrProcessor;
    }
	
	@Override
    public EmployeePurchaseTxnDto savePurchaseTxn(EmployeePurchaseTxnDto employeePurchaseTxnDto) {
		MemberModel theEmployee = memberRepo.findByAccountId(employeePurchaseTxnDto.getMemberModel().getAccountId());
    	EmployeePurchaseTxnModel theTxn = null;
    	if(employeePurchaseTxnDto.getId() != null) {
    		theTxn = employeePurchaseTxnRepo.findOne(employeePurchaseTxnDto.getId());	
    		theTxn.setMemberModel(theEmployee);
            //if (theEmployee != null) {
            //    theTxn.setMemberAccountId(theEmployee.getAccountId());
            //}
        	theTxn.setReason(employeePurchaseTxnDto.getReason());
        	theTxn.setRemarks(employeePurchaseTxnDto.getRemarks());
        	theTxn.setStatus(employeePurchaseTxnDto.getStatus());
        	if(employeePurchaseTxnDto.getStoreModel() != null)
        		theTxn.setStore(new Store(employeePurchaseTxnDto.getStoreModel().getId()));
        	theTxn.setTransactionAmount(employeePurchaseTxnDto.getTransactionAmount());
        	theTxn.setTransactionDateTime(employeePurchaseTxnDto.getTransactionDateTime());
        	theTxn.setTransactionNo(employeePurchaseTxnDto.getTransactionNo());
        	theTxn.setTransactionType(employeePurchaseTxnDto.getTransactionType());	
        	theTxn.setApprovedBy(employeePurchaseTxnDto.getApprovedBy());
        	theTxn.setApprovalDate(employeePurchaseTxnDto.getApprovalDate());
    	} else {
    		theTxn = new EmployeePurchaseTxnModel(employeePurchaseTxnDto);

            if (theTxn.getStore().getCode() != null)  {
                theTxn.setId(customIdGeneratorService.generateId(theTxn.getStore().getCode()));

            } else {
                theTxn.setId(customIdGeneratorService.generateId(theEmployee.getRegisteredStore()));
            }


    		theTxn.setCreated(new DateTime()); //TODO temp fix
    		CustomSecurityUserDetailsImpl userDetails = UserUtil.getCurrentUser();
            if (userDetails != null) {
                theTxn.setCreateUser(userDetails.getUsername());
            } else {
                theTxn.setCreateUser("SYSTEM");
            }
            theTxn.setMemberModel(theEmployee);
            //if (theEmployee != null) {
            //    theTxn.setMemberAccountId(theEmployee.getAccountId());
            //}
    	}
    	
    	theTxn = employeePurchaseTxnRepo.save(theTxn);
    	
        return new EmployeePurchaseTxnDto(theTxn);
    }
	
	@Override
	public List<EmployeePurchaseTxnDto> getTransactionsToday(String accountId) {
		return convertEmployeePurchaseTxnModelToDtoList(employeePurchaseTxnRepo.findTransactionsToday(accountId));
	}
	
	@Override
	public List<EmployeePurchaseTxnDto> getTransactionsByDate(String accountId, LocalDate date) {
		return convertEmployeePurchaseTxnModelToDtoList(employeePurchaseTxnRepo.findTransactionsByDate(accountId, date));
	}
	
	public EmployeePurchaseTxnDto findById(String id) {
		EmployeePurchaseTxnModel model = employeePurchaseTxnRepo.findOne(id);
		if(model == null) return null;
		return new EmployeePurchaseTxnDto(model);
	}
	
	@Override
    public ResultList<EmployeePurchaseTxnDto> searchPurchaseTxn( PurchaseTxnSearchDto theSearchDto ) {
        Pageable pageable = SpringDataPagingUtil.INSTANCE.createPageable(theSearchDto.getPagination());
        QEmployeePurchaseTxnModel qTxn = QEmployeePurchaseTxnModel.employeePurchaseTxnModel;
        BooleanExpression filter = qTxn.transactionType.eq(VoucherTransactionType.ADJUSTED).and(theSearchDto.createSearchExpression());
        Page<EmployeePurchaseTxnModel> page = filter != null ? employeePurchaseTxnRepo.findAll(filter, pageable) : employeePurchaseTxnRepo.findAll(pageable);
        return new ResultList<EmployeePurchaseTxnDto>( EmployeePurchaseTxnDto.toDtoList( page.getContent() ), page.getTotalElements(), page.getNumber(), page.getSize() );
    }

	@Override
	public BigDecimal getTotal( VoucherTransactionType[] types, String acctId ) {
    	JPAQuery query = new JPAQuery( em );
    	QEmployeePurchaseTxnModel qModel = QEmployeePurchaseTxnModel.employeePurchaseTxnModel;
    	Double total = query.from( qModel )
	    	.where( qModel.memberModel.accountId.equalsIgnoreCase( acctId ).and( qModel.transactionType.in( types ) ) )
	    	.singleResult( qModel.transactionAmount.sum() );
    	return new BigDecimal( null != total? total : 0 );
	}
}
