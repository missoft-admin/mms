<%@include file="../../common/taglibs.jsp" %>


<div id="content_recon">

  <div class="page-header page-header2"><h1><spring:message code="recon_pageheader" /></h1></div>

  <div class="">
    <div class="pull-right mb20">
      <a id="link_generate" href="<c:url value="/points/recon/generate" />" class="btn btn-primary">
        <spring:message code="recon_lbl_reconcile" />
      </a>
    </div>
    <div class="clearfix"></div>
  </div>

  <div id="list_recons" class=""><jsp:include page="reconList.jsp"/></div>


<style type="text/css"> <!--  --> </style>
<script type='text/javascript'>
var Recon = null;
var ReconCommon = null;

$(document).ready( function() {

    var $main = $( "#content_recon" );
    var $generateLink = $main.find( "#link_generate" );

    initLinks();

    function initLinks() {
        $generateLink.click( function(e) {
            e.preventDefault();
            $.get( $( this ).attr( "href" ), function( resp )  {
                ReconList.reloadTable();
            }, "html" );
        });
    }

    Recon = {
        generate  : function( resp ) {}
    };

    ReconCommon = {
        processResp  : function( resp, $dialog, errorContainer, $save ) {
            $save.prop( "disabled", false );
            if ( resp.success ) {
                $dialog.modal( "hide" );
            }
            else {
                var errors = "";
                $( resp.result ).each( function( key, value ) { errors += ( ( key + 1 ) + ". " + value.code + "<br>" ); });
                errorContainer.find( "div" ).html( errors );
                errorContainer.show( "slow" );
            }
        },
        reset     : function( ele, isEmpty ) {
            $( ele ).find(':input').each( function() {
                switch(this.type) {
                    case 'password':
                    case 'select-one':
                    case 'text':
                    case 'textarea':
                    case 'file':
                    case 'hidden': $(this).val(''); break;
                    case 'select-multiple': isEmpty? $(this).empty() : $(this).val(""); break;
                    case 'checkbox':
                    case 'radio': this.checked = false;
                }
            });
        }
    };

});
</script>


</div>