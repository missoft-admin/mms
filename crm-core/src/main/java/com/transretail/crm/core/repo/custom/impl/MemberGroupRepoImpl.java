package com.transretail.crm.core.repo.custom.impl;

import com.google.common.collect.Lists;
import com.mysema.query.BooleanBuilder;
import com.mysema.query.jpa.JPASubQuery;
import com.mysema.query.jpa.impl.JPAQuery;
import com.mysema.query.types.Predicate;
import com.mysema.query.types.Projections;
import com.mysema.query.types.expr.BooleanExpression;
import com.mysema.query.types.path.ComparablePath;
import com.mysema.query.types.path.PathBuilder;
import com.mysema.query.types.path.StringPath;
import com.mysema.query.types.query.ListSubQuery;
import com.transretail.crm.common.service.dto.request.PageSortDto;
import com.transretail.crm.common.service.dto.request.PagingParam;
import com.transretail.crm.common.service.dto.response.ResultList;
import com.transretail.crm.common.service.util.SpringDataPagingUtil;
import com.transretail.crm.core.dto.MemberDto;
import com.transretail.crm.core.dto.MemberGroupFieldDto;
import com.transretail.crm.core.entity.*;
import com.transretail.crm.core.entity.embeddable.ConfigurableFields;
import com.transretail.crm.core.entity.enums.MemberStatus;
import com.transretail.crm.core.entity.lookup.QProduct;
import com.transretail.crm.core.entity.lookup.StoreGroup;
import com.transretail.crm.core.repo.MemberGroupFieldValueRepo;
import com.transretail.crm.core.repo.MemberRepo;
import com.transretail.crm.core.repo.custom.MemberGroupRepoCustom;
import com.transretail.crm.core.service.CodePropertiesService;
import com.transretail.crm.core.service.LookupService;
import com.transretail.crm.core.service.ProductGroupService;
import com.transretail.crm.core.service.StoreGroupService;
import com.transretail.crm.core.util.AppConstants;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.IteratorUtils;
import org.apache.commons.lang3.BooleanUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.text.WordUtils;
import org.apache.commons.lang3.time.DateUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class MemberGroupRepoImpl implements MemberGroupRepoCustom {
	@PersistenceContext
	EntityManager em;
	@Autowired
	CodePropertiesService codePropertiesService;
	@Autowired
	LookupService lookupService;
	@Autowired
	StoreGroupService storeGroupService;
	@Autowired
	ProductGroupService productGroupService;
	@Autowired
	MemberGroupFieldValueRepo fieldValueRepo;
	@Autowired
	MemberRepo memberRepo;
	
	private static Logger LOGGER = LoggerFactory.getLogger(MemberGroupRepoImpl.class);
	
	@Override
	public List<Long> getQualifiedMembers(List<MemberGroupFieldDto> fields, MemberGroup group) {
		QPointsTxnModel qPointsTxnModel = QPointsTxnModel.pointsTxnModel;
		QMemberModel qMemberModel = QMemberModel.memberModel;
        BooleanBuilder targetMembersExprBuilder = getTargetMembersExpr(fields, null);
        JPAQuery query = getQuery(group, targetMembersExprBuilder, qPointsTxnModel, qMemberModel);
		if(query == null)
			return Lists.newArrayList();
		return query.list(qMemberModel.id);
	}

    public boolean isQualifiedMember(List<MemberGroupFieldDto> fields, MemberGroup group, String accountId) {
        QPointsTxnModel qPointsTxnModel = QPointsTxnModel.pointsTxnModel;
        QMemberModel qMemberModel = QMemberModel.memberModel;
        BooleanBuilder targetMembersExprBuilder = getTargetMembersExpr(fields, accountId);
        JPAQuery query = getQuery(group, targetMembersExprBuilder, qPointsTxnModel, qMemberModel);
        return query == null ? false : query.distinct().list(qMemberModel.id).size() > 0;
    }

    @Override
	public ResultList<MemberDto> getQualifiedMembers( List<MemberGroupFieldDto> fields, MemberGroup group, PageSortDto sortDto ) {
		QPointsTxnModel qPointsTxnModel = QPointsTxnModel.pointsTxnModel;
		QMemberModel qMemberModel = QMemberModel.memberModel;
        BooleanBuilder targetMembersExprBuilder = getTargetMembersExpr(fields, null);
        JPAQuery query = getQuery(group, targetMembersExprBuilder, qPointsTxnModel, qMemberModel);
		if(query == null)
			return new ResultList<MemberDto>(new ArrayList<MemberDto>());
		Long totalElements = Long.valueOf(query.list(qMemberModel.id).size());
		PagingParam pagination = sortDto.getPagination();
		SpringDataPagingUtil.INSTANCE.applyPagination(query, pagination, MemberModel.class);
		List<Long> members = query.list(qMemberModel.id);
		return new ResultList<MemberDto>(convertMemberListToDtoList(members),
				totalElements != null ? totalElements : 0,
				/*CollectionUtils.isNotEmpty( members )? members.size() : 0,*/
				pagination.getPageNo(), pagination.getPageSize());
	}
    
    @Override
    public List<MemberDto> getQualifiedMembersForPrint( List<MemberGroupFieldDto> fields, MemberGroup group, PageSortDto sortDto) {
        QPointsTxnModel qPointsTxnModel = QPointsTxnModel.pointsTxnModel;
        QMemberModel qMemberModel = QMemberModel.memberModel;
        BooleanBuilder targetMembersExprBuilder = getTargetMembersExpr(fields, null);
        JPAQuery query = getQuery(group, targetMembersExprBuilder, qPointsTxnModel, qMemberModel);
        if(query == null)
            return new ArrayList<MemberDto>();
        PagingParam pagination = sortDto.getPagination();
        SpringDataPagingUtil.INSTANCE.applyPagination(query, pagination, MemberModel.class);
        List<Long> members = query.list(qMemberModel.id);
        
        JPAQuery query2 = new JPAQuery(em).from(qMemberModel).where(qMemberModel.id.in(members));
        List<MemberModel> modelList = query2.list(Projections.fields(MemberModel.class,
                        QMemberModel.memberModel.accountId.as("accountId"),
                        QMemberModel.memberModel.firstName.as("firstName"),
                        QMemberModel.memberModel.lastName.as("lastName"),
                        QMemberModel.memberModel.contact.as("contact"),
                        QMemberModel.memberModel.professionalProfile.as("professionalProfile"),
                        QMemberModel.memberModel.companyName.as("companyName")
                        ));
        List<MemberDto> memberDtoList = new ArrayList<MemberDto>();
        for (MemberModel model : modelList) {
            MemberDto dto = new MemberDto();
            dto.setAccountId(model.getAccountId());
            dto.setFirstName(model.getFirstName());
            dto.setLastName(model.getLastName());
            dto.setFullStringContact(model.getContact());
            dto.setProfessionalProfile(model.getProfessionalProfile());
            dto.setCompanyName(model.getCompanyName());
            memberDtoList.add(dto);
        }
        return memberDtoList;
    }
    
    @Override
    public Long getQualifiedMembersCount(List<MemberGroupFieldDto> fields, MemberGroup group) {
        QPointsTxnModel qPointsTxnModel = QPointsTxnModel.pointsTxnModel;
        QMemberModel qMemberModel = QMemberModel.memberModel;
        BooleanBuilder targetMembersExprBuilder = getTargetMembersExpr(fields, null);
        JPAQuery query = getQuery(group, targetMembersExprBuilder, qPointsTxnModel, qMemberModel);
        if(query == null)
            return 0L;
        List<Long> members = query.list(qMemberModel.id);
        return Long.valueOf(members.size());
    }

    private JPAQuery getQuery(MemberGroup memberGroup, BooleanBuilder targetMembersExprBuilder, QPointsTxnModel qPointsTxnModel, QMemberModel qMemberModel) {

        MemberGroupRFS rfs = memberGroup.getRfs();
        MemberGroupPoints points = memberGroup.getPoints();

        List<BooleanExpression> filters = Lists.newArrayList();
        List<BooleanExpression> groupFilter = Lists.newArrayList();

        Predicate condition = null;
        Predicate groupCondition = null;

        if(rfs != null && BooleanUtils.isTrue(rfs.getEnabled())) {
            if(rfs.getStoreGroup() != null) {
                StoreGroup group = storeGroupService.findOne(rfs.getStoreGroup());
                BooleanExpression storeGroupFilter = qPointsTxnModel.storeCode.in(group.getStoreList());
                filters.add(storeGroupFilter);
            }
            
            
            if(rfs.getProductGroup() != null) {
                QProduct qProd = QProduct.product;
                QPosTxItem qPosTx = QPosTxItem.posTxItem;
                ProductGroup group = productGroupService.findOne(rfs.getProductGroup());
                ListSubQuery<String> invalidProds = null;
                if(CollectionUtils.isNotEmpty(group.getExcProductList())) {
                    invalidProds = new JPASubQuery().from(qProd).where(qProd.itemCode.in(group.getExcProductList())).list(qProd.id);
                }
                ListSubQuery<String> validProds = null;
                if(CollectionUtils.isNotEmpty(group.getProductList())) {
                    validProds = new JPASubQuery().from(qProd).where(qProd.itemCode.in(group.getProductList())).list(qProd.id);

                } else if(CollectionUtils.isNotEmpty(group.getProductCfnList())) {
                    BooleanBuilder bld = new BooleanBuilder();
                    for(String cfnCode: group.getProductCfnList()) {
                        bld.or(qProd.itemCode.startsWith(cfnCode));
                    }
                    validProds = new JPASubQuery().from(qProd).where(bld).list(qProd.id);
                }
                
                BooleanBuilder subFilter = new BooleanBuilder();
                if(validProds != null) 
                	subFilter.and(qPosTx.productId.in(validProds));
                if(invalidProds != null)
                	subFilter.and(qPosTx.productId.notIn(invalidProds));
                
                ListSubQuery<String> validTxns = new JPASubQuery().from(qPosTx).where(subFilter).list(qPosTx.posTransaction.id);

                filters.add(qPointsTxnModel.transactionNo.in(validTxns));
            }
            
            
            if(StringUtils.defaultString(rfs.getRecency()).equalsIgnoreCase("dateRange")) {
                Date dateFrom = rfs.getDateRangeFrom().toDateTimeAtStartOfDay().toDate();
                Date dateTo = rfs.getDateRangeTo().plusDays(1).toDateTimeAtStartOfDay().minusMillis(1).toDate();
                filters.add(qPointsTxnModel.transactionDateTime.goe(dateFrom).and(qPointsTxnModel.transactionDateTime.loe(dateTo)));
            }

            
            
            if(BooleanUtils.isTrue(rfs.getFrequency())) {
            	
            	//WUT?
            	if(rfs.getFrequencyFrom() != null && rfs.getFrequencyTo() != null && rfs.getFrequencyFrom() == 0 && rfs.getFrequencyTo() == 0) {
            		return new JPAQuery(em).from(qMemberModel).where(targetMembersExprBuilder
            				.and(qMemberModel.id.notIn(
            						new JPASubQuery().from(qPointsTxnModel)
            						.where(BooleanExpression.allOf(filters.toArray(new BooleanExpression[filters.size()])))
            						.list(qPointsTxnModel.memberModel.id))));
            	}
            	
            	
                List<BooleanExpression> filt = Lists.newArrayList();
                if(rfs.getFrequencyFrom() != null) {
                    filt.add(qPointsTxnModel.transactionNo.countDistinct().lt(rfs.getFrequencyFrom()));
                }
                if(rfs.getFrequencyTo() != null) {
                    filt.add(qPointsTxnModel.transactionNo.countDistinct().gt(rfs.getFrequencyTo()));
                }
                BooleanExpression filter = BooleanExpression.anyOf(filt.toArray(new BooleanExpression[filt.size()]));

                ListSubQuery<Long> invalidMembers = new JPASubQuery().from(qPointsTxnModel)
                    .where(BooleanExpression.allOf(filters.toArray(new BooleanExpression[filters.size()])))
                    .groupBy(qPointsTxnModel.memberModel.id)
                    .having(filter)
                    .list(qPointsTxnModel.memberModel.id);
                
                groupFilter.add(qMemberModel.id.notIn(invalidMembers));
            }
            
            
            
            if(BooleanUtils.isTrue(rfs.getSpend())) {
            	
                if(BooleanUtils.isTrue(rfs.getSpendPerTransaction())) {
                    List<BooleanExpression> filt = Lists.newArrayList();
                    if(rfs.getSpendFrom() != null) {
                        filt.add(qPointsTxnModel.transactionAmount.sum().lt(rfs.getSpendFrom()));
                    }
                    if(rfs.getSpendTo() != null) {
                        filt.add(qPointsTxnModel.transactionAmount.sum().gt(rfs.getSpendTo()));
                    }
                    BooleanExpression filter = BooleanExpression.anyOf(filt.toArray(new BooleanExpression[filt.size()]));

                    ListSubQuery<Long> invalidMembers = new JPASubQuery().from(qPointsTxnModel)
                        .where(BooleanExpression.allOf(filters.toArray(new BooleanExpression[filters.size()])))
                        .groupBy(qPointsTxnModel.transactionNo, qPointsTxnModel.memberModel.id)
                        .having(filter)
                        .list(qPointsTxnModel.memberModel.id);
                    
                    groupFilter.add(qMemberModel.id.notIn(invalidMembers));
                    
                } else {
                    List<BooleanExpression> filt = Lists.newArrayList();
                    if(rfs.getSpendFrom() != null) {
                        filt.add(qPointsTxnModel.transactionAmount.sum().goe(rfs.getSpendFrom()));
                    }
                    if(rfs.getSpendTo() != null) {
                        filt.add(qPointsTxnModel.transactionAmount.sum().loe(rfs.getSpendTo()));
                    }
                    BooleanExpression filter = BooleanExpression.allOf(filt.toArray(new BooleanExpression[filt.size()]));

                    groupFilter.add(filter);
                }
            }
        }

        
        if(points != null && BooleanUtils.isTrue(points.getEnabled())) {
            List<BooleanExpression> filt = Lists.newArrayList();
            if(points.getPointsFrom() != null) {
                filt.add(qPointsTxnModel.transactionPoints.sum().goe(points.getPointsFrom()));
            }
            if(points.getPointsTo() != null) {
                filt.add(qPointsTxnModel.transactionPoints.sum().loe(points.getPointsTo()));
            }
            BooleanExpression filter = BooleanExpression.allOf(filt.toArray(new BooleanExpression[filt.size()]));
            groupFilter.add(filter);
        }


        if(CollectionUtils.isNotEmpty(filters) || CollectionUtils.isNotEmpty(groupFilter)) {
        	if(CollectionUtils.isNotEmpty(filters))
        		condition = BooleanExpression.allOf(filters.toArray(new BooleanExpression[filters.size()]));
        	if(CollectionUtils.isNotEmpty(groupFilter))
        		groupCondition = BooleanExpression.allOf(groupFilter.toArray(new BooleanExpression[groupFilter.size()]));
        	
        } else {
            /*condition = targetMembersExprBuilder;*/
        	return new JPAQuery(em).from(qMemberModel).where(targetMembersExprBuilder);
        }

        /*if(condition == null) {
            return null;
        }*/

        return new JPAQuery(em).from(qPointsTxnModel)
            .leftJoin(qPointsTxnModel.memberModel, qMemberModel)
            .where(targetMembersExprBuilder.and(condition))
            .groupBy(qMemberModel.id, qMemberModel.accountId, qMemberModel.username, qMemberModel.firstName, qMemberModel.lastName)
            .having(groupCondition);
    }
	
	private List<MemberDto> convertMemberListToDtoList(List<Long> memberIds) {
	    List<MemberDto> dtoList = new ArrayList<MemberDto>();
	    Iterable<MemberModel> modelList = memberRepo.findAll(memberIds);
	    if (modelList.iterator().hasNext()) {
    	    for (MemberModel model : modelList) {
    	        MemberDto dto = new MemberDto(model);
    	        dtoList.add(dto);
    	    }
	    }
    	return dtoList;
    }
	
	private long filterMembers( BooleanBuilder filter ) {
		return new JPAQuery( em ).from( QMemberModel.memberModel ).where( filter ).count();
	}
	
	@SuppressWarnings({ "rawtypes", "unchecked" })
	private BooleanBuilder createExpr( PathBuilder memberPath, MemberGroupFieldDto field ) {
		BooleanBuilder builder = new BooleanBuilder();

		String fieldName = convertToCamelCase( StringUtils.isBlank( field.getName().getDescription() )?
				lookupService.getHeader( field.getName().getCode() ).getDescription()
				: field.getName().getDescription() );
		List<MemberGroupFieldValue> values = field.getFieldValues();

		if ( null == field.getType() ) {
			field.setType( field.getIsLookup()? 
					lookupService.getDetail( codePropertiesService.getDetailFieldTypeString() )
					: lookupService.getDetail( codePropertiesService.getDetailFieldTypeNumeric() ) );
		}
		if( field.getType().getCode().equals( codePropertiesService.getDetailFieldTypeNumeric() ) ) {
			ComparablePath<Comparable> fieldPath = memberPath.getComparable( fieldName, Comparable.class );

			if ( fieldName.equalsIgnoreCase( "age" ) ) {
				fieldPath = memberPath.getComparable( "birthdate", Comparable.class );
				for( MemberGroupFieldValue value : values ) {
					createOperand( value.getOperand().getCode(), builder, fieldPath, 
							DateUtils.addYears( new Date(), -Integer.valueOf( value.getValue() ) ), true );
				}
			}
			else {
				for( MemberGroupFieldValue value : values ) {
					createOperand( value.getOperand().getCode(), builder, fieldPath, 
							Integer.valueOf( value.getValue() ), false );
				}
			}
		}
		else {
				
			PathBuilder<Object> fieldPath = memberPath.get( fieldName );
			
			if (fieldName.equalsIgnoreCase("bestTimeToCall")) {
				
				for(MemberGroupFieldValue value : values) {
					builder.or(fieldPath.eq(value.getValue()));
				}
				
			} else {
				for( MemberGroupFieldValue value : values ) {
					builder.or( fieldPath.eq( lookupService.getDetail( value.getValue() ) ) );
				}	
			}
			
			
		}
		return builder;
	}

	@SuppressWarnings("rawtypes")
	private void createOperand( String operand, BooleanBuilder builder, ComparablePath<Comparable> fieldPath, Comparable val, boolean isOpposite ) {
		if( operand.equals( codePropertiesService.getDetailOperandEq() ) ) {
			builder.or(fieldPath.eq( val ) );
		}
		else if ( operand.equals(codePropertiesService.getDetailOperandGe() ) ) {
			builder.or( isOpposite? fieldPath.loe( val ) : fieldPath.goe( val ) );
		}
		else if ( operand.equals( codePropertiesService.getDetailOperandGt() ) ) {
			builder.or( isOpposite? fieldPath.lt( val ) : fieldPath.gt( val ) );
		}
		else if ( operand.equals( codePropertiesService.getDetailOperandLt() ) ) {
			builder.or( isOpposite? fieldPath.gt( val ) : fieldPath.lt( val ) );
		}
		else if ( operand.equals( codePropertiesService.getDetailOperandLe() ) ) {
			builder.or( isOpposite? fieldPath.goe( val ) : fieldPath.loe( val ) );
		}
	}

	private String convertToCamelCase(String val) {
		String lowerCase = val.toLowerCase();
		String newVal = WordUtils.capitalize(lowerCase).replaceAll(" ", "").substring(1);
		return lowerCase.charAt(0) + newVal;
	}

	@SuppressWarnings("unchecked")
    private BooleanBuilder getTargetMembersExpr(List<MemberGroupFieldDto> fields, String accountId) {
        BooleanBuilder expr = new BooleanBuilder();
        if ( CollectionUtils.isNotEmpty( fields ) ) {
            PathBuilder<MemberModel> rootPath = new PathBuilder<MemberModel>( MemberModel.class, "memberModel" );
            for ( MemberGroupFieldDto dto : fields ) {
                if ( CollectionUtils.isEmpty( dto.getFieldValues() ) ) {
                    dto.setFieldValues( IteratorUtils.toList( fieldValueRepo.findAll(
                        QMemberGroupFieldValue.memberGroupFieldValue.memberGroupField.id.eq( dto.getId() ) ).iterator() ) );
                }
            }
            for ( MemberGroupFieldDto field : fields ) {
                if (field.getName().getCode().startsWith(AppConstants.HEADER_CUSTMEMBERFLD_PFX)) {
                    String suffix = field.getName().getCode().substring(AppConstants.HEADER_CUSTMEMBERFLD_PFX.length());
                    PathBuilder<ConfigurableFields> qConfigurableFields = rootPath.get("configurableFields", ConfigurableFields.class);
                    StringPath qCustomField = qConfigurableFields.getString("customField" + suffix);
                    List<BooleanExpression> exprs = new ArrayList<BooleanExpression>();
                    for (MemberGroupFieldValue fieldValue : field.getFieldValues()) {
                        exprs.add(qCustomField.contains(fieldValue.getValue()));
                    }
                    expr.and(BooleanExpression.anyOf(exprs.toArray(new BooleanExpression[exprs.size()])));
                } else {
                    // TODO: Is it possible to construct the expression based on header code value? This block wastes lots of resources.
                    BooleanBuilder expr1;
                    try {
                        expr1 = createExpr(rootPath.get("customerProfile"), field);
                        filterMembers(expr1);
                    } catch (Exception e) {
                        try {
                            expr1 = createExpr(rootPath.get("professionalProfile"), field);
                            filterMembers(expr1);
                        } catch (Exception e1) {
                            try {
                                expr1 = createExpr(rootPath, field);
                                filterMembers(expr1);
                            } catch (Exception e2) {
                                expr1 = new BooleanBuilder();
                            }
                        }
                    }
                    expr.and( expr1 );
                }
            }
        }
        QMemberModel qMemberModel = QMemberModel.memberModel;
        expr = expr.and(qMemberModel.accountStatus.eq(MemberStatus.ACTIVE));
        if (StringUtils.isNotBlank(accountId)) {
            expr.and(qMemberModel.accountId.eq(accountId));
        }

        return expr;
    }

	@Override
	public List<MemberGroup> getCardTypeMemberGroups() {
		QMemberGroup memberGroup = QMemberGroup.memberGroup;
		JPAQuery query = new JPAQuery(em);
		
		return query.from(memberGroup).where(memberGroup.cardTypeGroupCode.isNotNull()
				.and(memberGroup.cardTypeGroupCode.isNotEmpty()))
				.list(memberGroup);
	}
}
