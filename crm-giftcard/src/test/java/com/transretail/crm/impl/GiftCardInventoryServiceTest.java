package com.transretail.crm.impl;

import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.List;

import org.joda.time.LocalDate;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.TestingAuthenticationToken;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.context.SecurityContextImpl;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.google.common.collect.Lists;
import com.transretail.crm.core.entity.lookup.LookupDetail;
import com.transretail.crm.core.entity.lookup.LookupHeader;
import com.transretail.crm.core.repo.LookupDetailRepo;
import com.transretail.crm.core.repo.LookupHeaderRepo;
import com.transretail.crm.core.security.model.impl.CustomSecurityUserDetailsImpl;
import com.transretail.crm.core.service.CodePropertiesService;
import com.transretail.crm.giftcard.dto.GiftCardInventorySeriesSearchDto;
import com.transretail.crm.giftcard.dto.GiftCardOrderDto;
import com.transretail.crm.giftcard.dto.StockRequestDto;
import com.transretail.crm.giftcard.dto.StockRequestReceiveDto;
import com.transretail.crm.giftcard.entity.CardVendor;
import com.transretail.crm.giftcard.entity.GiftCardInventory;
import com.transretail.crm.giftcard.entity.GiftCardOrder;
import com.transretail.crm.giftcard.entity.GiftCardOrderItem;
import com.transretail.crm.giftcard.entity.ProductProfile;
import com.transretail.crm.giftcard.entity.support.GiftCardInventoryStatus;
import com.transretail.crm.giftcard.entity.support.GiftCardOrderStatus;
import com.transretail.crm.giftcard.repo.CardVendorRepo;
import com.transretail.crm.giftcard.repo.GiftCardInventoryRepo;
import com.transretail.crm.giftcard.repo.GiftCardOrderRepo;
import com.transretail.crm.giftcard.repo.ProductProfileRepo;
import com.transretail.crm.giftcard.service.GiftCardBurnCardService;
import com.transretail.crm.giftcard.service.GiftCardInventoryService;
import com.transretail.crm.giftcard.service.GiftCardOrderService;

/**
 *
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath*:/META-INF/spring/applicationContext*.xml",
    "classpath:META-INF/spring/applicationContext-test.xml"})
public class GiftCardInventoryServiceTest {
    @Autowired
    private GiftCardOrderRepo giftCardOrderRepo;
    @Autowired
    private CardVendorRepo vendorRepo;
    @Autowired
    private GiftCardInventoryService service;
    @Autowired
    private ProductProfileRepo profileRepo;
    @Autowired
    private GiftCardOrderService orderService;
    @Autowired
    private GiftCardInventoryRepo inventoryRepo;
    @Autowired
    private GiftCardBurnCardService giftCardBurnCardService;

    private CardVendor savedVendor;
    private GiftCardOrder savedGiftCardOrder;
    private ProductProfile savedProfile;
    

    @Autowired
    private CodePropertiesService codePropertiesService;
    @Autowired
    private LookupHeaderRepo lookupHeaderRepo;
    @Autowired
    private LookupDetailRepo lookupDetailRepo;
    
    @BeforeClass
    public static void setupCurrentUser() {
        CustomSecurityUserDetailsImpl user = new CustomSecurityUserDetailsImpl();
        user.setUserId(1L);
        user.setUsername("user");
        List<GrantedAuthority> authorities = Lists.newArrayList();
        authorities.add(new SimpleGrantedAuthority("ROLE_MERCHANT_SERVICE"));
        user.setAuthorities(authorities);

        SecurityContextImpl securityContext = new SecurityContextImpl();
        securityContext.setAuthentication(new TestingAuthenticationToken(user, ""));
        SecurityContextHolder.setContext(securityContext);
    }

    @AfterClass
    public static void removeCurrentUser() {
        SecurityContextHolder.clearContext();
    }

    @Before
    public void onSetup() {
        savedVendor = new CardVendor();
        savedVendor.setFormalName("name");
        savedVendor.setMailAddress("add");
        savedVendor.setEmailAddress("email");
        savedVendor.setEnabled(true);
        savedVendor.setRegion("region");
        savedVendor.setDefaultVendor(true);
        savedVendor = vendorRepo.save(savedVendor);
        
        LookupHeader hdr = new LookupHeader(codePropertiesService.getProductProfileFaceValue());
        hdr = lookupHeaderRepo.saveAndFlush(hdr);

        LookupDetail dtl = new LookupDetail("PPFV1");
        dtl.setDescription("100000");
        dtl.setHeader(hdr);
        dtl = lookupDetailRepo.saveAndFlush(dtl);
        
        savedProfile = new ProductProfile();
        savedProfile.setFaceValue(dtl);
        savedProfile.setProductCode("000000001");
        savedProfile.setProductDesc("PRODUCT PROFILE DESC");
        savedProfile = profileRepo.save(savedProfile);

        savedGiftCardOrder = new GiftCardOrder();
        savedGiftCardOrder.setMoNumber("140403000001");
        savedGiftCardOrder.setPoNumber("0001");
        savedGiftCardOrder.setMoDate(new LocalDate());
        savedGiftCardOrder.setPoDate(new LocalDate());
        savedGiftCardOrder.setVendor(savedVendor);
        savedGiftCardOrder.setStatus(GiftCardOrderStatus.APPROVED);
        savedGiftCardOrder.setItems(new ArrayList<GiftCardOrderItem>());
        
        GiftCardOrderItem item = new GiftCardOrderItem();
        item.setProfile(savedProfile);
        item.setProductCode(savedProfile.getProductCode());
        item.setQuantity(10L);
        item.setOrder(savedGiftCardOrder);
        savedGiftCardOrder.getItems().add(item);
        
        savedGiftCardOrder = giftCardOrderRepo.save(savedGiftCardOrder);
    }

    @After
    public void tearDown() {
    	inventoryRepo.deleteAll();
    	giftCardOrderRepo.deleteAll();
    	vendorRepo.deleteAll();
    	profileRepo.deleteAll();
    }
    
    @Test
    public void testGenerateGiftCardInventory() {
    	GiftCardOrderDto orderDto = orderService.getGiftCardOrder(savedGiftCardOrder.getId());
    	service.generateGiftCardInventory(orderDto.getItems(), orderDto.getMoDate(), orderDto.getId());
    	assertTrue(inventoryRepo.count() == 10);
    }
    
    @Test
    public void testGenerateBarcode() {
    	String barcode = service.generateBarcode("7992739871");
    	assertTrue(barcode.equals("79927398713"));
    }

    @Test
    public void testGetGiftCardSeries() {
        GiftCardOrderDto orderDto = orderService.getGiftCardOrder(savedGiftCardOrder.getId());
        service.generateGiftCardInventory(orderDto.getItems(), orderDto.getMoDate(), orderDto.getId());
        
        List<GiftCardInventory> list = inventoryRepo.findAll();
        GiftCardInventory giftCardInventory = list.get(0);
        giftCardInventory.setStatus(GiftCardInventoryStatus.IN_STOCK);
        inventoryRepo.save(giftCardInventory);
        
        GiftCardInventorySeriesSearchDto searchDto = new GiftCardInventorySeriesSearchDto();
        searchDto.setSeriesFrom(giftCardInventory.getSeries());
        
        assertTrue(service.getGiftCardSeries(searchDto).size() > 0);
    }
    
    @Test
    public void testIsSeriesNotExists() {
        GiftCardOrderDto orderDto = orderService.getGiftCardOrder(savedGiftCardOrder.getId());
        service.generateGiftCardInventory(orderDto.getItems(), orderDto.getMoDate(), orderDto.getId());
        
        List<GiftCardInventory> list = inventoryRepo.findAll();
        GiftCardInventory giftCardInventory = list.get(0);
        giftCardInventory.setStatus(GiftCardInventoryStatus.IN_STOCK);
        inventoryRepo.save(giftCardInventory);
        
        String seriesNo = String.valueOf(giftCardInventory.getSeries());
        
        assertTrue(service.isSeriesNotExistsInStockStatus(seriesNo));
    }
    
    @Test
    public void testIsCardInTheList() {
        GiftCardOrderDto orderDto = orderService.getGiftCardOrder(savedGiftCardOrder.getId());
        service.generateGiftCardInventory(orderDto.getItems(), orderDto.getMoDate(), orderDto.getId());
        
        List<GiftCardInventory> list = inventoryRepo.findAll();
        GiftCardInventory giftCardInventory = list.get(0);
        giftCardInventory.setStatus(GiftCardInventoryStatus.IN_STOCK);
        String seriesNo = String.valueOf(giftCardInventory.getSeries());
        
        GiftCardInventorySeriesSearchDto searchDto = new GiftCardInventorySeriesSearchDto();
        searchDto.setSeriesFrom(giftCardInventory.getSeries());
        searchDto.setCards(new ArrayList<String>());
        searchDto.getCards().add(String.valueOf(giftCardInventory.getSeries()));
        inventoryRepo.save(giftCardInventory);
        
        assertTrue(service.isCardInTheList(seriesNo, searchDto));
    }
    
    @Test
    public void testReserveGiftCards() {
    	GiftCardInventory gcInventory = new GiftCardInventory();
    	gcInventory.setOrder(savedGiftCardOrder);
    	gcInventory.setProfile(savedProfile);
    	gcInventory.setProductCode(savedProfile.getProductCode());
    	gcInventory.setProductName(savedProfile.getProductDesc());
    	gcInventory.setSeqNo(1);
    	gcInventory.setBatchNo(1);
    	gcInventory.setOrderDate(LocalDate.now());
    	gcInventory.setSeries(1L);
    	gcInventory.setBarcode("1");
    	gcInventory.setLocation("INVT001");
    	gcInventory = inventoryRepo.save(gcInventory);
    	Long gcId = gcInventory.getId();
    	
    	StockRequestReceiveDto receiveDto = new StockRequestReceiveDto();
    	receiveDto.setStartingSeries(1L);
    	receiveDto.setEndingSeries(1L);
    	
    	StockRequestDto stockRequestDto = new StockRequestDto();
    	stockRequestDto.setSourceLocation("INVT001");
    	stockRequestDto.setAllocateTo("DENPASAR");
    	
    	assertTrue(null == service.reserveGiftCards(receiveDto, stockRequestDto));
    	assertTrue(inventoryRepo.findOne(gcId).getAllocateTo().equals("DENPASAR"));
    	assertTrue(service.reserveGiftCards(receiveDto, stockRequestDto).equals("gc.reserve.error.allocated"));
    	
    	stockRequestDto.setSourceLocation("");
    	assertTrue(service.reserveGiftCards(receiveDto, stockRequestDto).equals("gc.reserve.error.source.mismatch"));
    }

    @Test
    public void testReservePromoGiftCards() {
    	GiftCardInventory gcInventory = new GiftCardInventory();
    	gcInventory.setOrder(savedGiftCardOrder);
    	gcInventory.setProfile(savedProfile);
    	gcInventory.setProductCode(savedProfile.getProductCode());
    	gcInventory.setProductName(savedProfile.getProductDesc());
    	gcInventory.setSeqNo(1);
    	gcInventory.setBatchNo(1);
    	gcInventory.setOrderDate(LocalDate.now());
    	gcInventory.setSeries(1L);
    	gcInventory.setBarcode("1");
    	gcInventory.setLocation("INVT001");
    	gcInventory = inventoryRepo.save(gcInventory);
    	Long gcId = gcInventory.getId();
    	
    	StockRequestReceiveDto receiveDto = new StockRequestReceiveDto();
    	receiveDto.setStartingSeries(1L);
    	receiveDto.setEndingSeries(1L);
    	
    	StockRequestDto stockRequestDto = new StockRequestDto();
    	stockRequestDto.setIsForPromo( true );
    	stockRequestDto.setSourceLocation("INVT001");
    	stockRequestDto.setAllocateTo("DENPASAR");
    	
    	assertTrue(null == service.reserveGiftCards(receiveDto, stockRequestDto));
    	GiftCardInventory gc = inventoryRepo.findOne(gcId);
    	assertTrue( gc.getIsForPromo() );
    }
}
