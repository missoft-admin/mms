package com.transretail.crm.common.web.converter;

import java.io.IOException;

import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;

/**
 *
 */
public class DateTimeSerializer extends JsonSerializer<DateTime> {
    private static final DateTimeFormatter FORMATTER = DateTimeFormat.forPattern("dd-MM-yyyy hh:mm:ss aa");

    public void serialize(DateTime dateTime, JsonGenerator jsonGenerator, SerializerProvider serializerProvider) throws
        IOException {
        if (dateTime != null) {
            jsonGenerator.writeString(FORMATTER.print(dateTime));
        } else {
            jsonGenerator.writeString("");
        }
    }
}
