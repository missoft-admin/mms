package com.transretail.crm.giftcard.entity.support;

/**
 *
 */
public enum SalesOrderStatus {
    APPROVED, FOR_APPROVAL, DRAFT,  
    ALLOCATED, PARTIALLY_ALLOCATED, FOR_PICKUP, PAYMENT_APPROVAL, FOR_ACTIVATION, SOLD,
    CANCELLED
}
