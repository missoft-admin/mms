package com.transretail.crm.giftcard.entity.support;

/**
 *
 * @author Monte Cillo Co (mco@exist.com)
 */
public enum GiftCardTxType {

    GC_ACTIVATION,
    GC_REDEMPTION
}
