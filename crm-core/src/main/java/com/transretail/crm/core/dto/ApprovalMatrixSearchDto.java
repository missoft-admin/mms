package com.transretail.crm.core.dto;


import java.util.ArrayList;
import java.util.List;

import org.springframework.util.StringUtils;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.mysema.query.types.expr.BooleanExpression;
import com.transretail.crm.common.service.dto.request.AbstractSearchFormDto;
import com.transretail.crm.core.entity.QApprovalMatrixModel;
import com.transretail.crm.core.entity.enums.ModelType;
import com.transretail.crm.core.util.BooleanExprUtil;


public class ApprovalMatrixSearchDto extends AbstractSearchFormDto {

	private String username;
	private String name;
	private String modelType;
	private Double amount;

	@Override
	@JsonIgnore
	public BooleanExpression createSearchExpression() {
		
		List<BooleanExpression> expressions = new ArrayList<BooleanExpression>();
        QApprovalMatrixModel theMatrix = QApprovalMatrixModel.approvalMatrixModel;
        if (StringUtils.hasText( modelType ) ) {
            expressions.add( BooleanExprUtil.INSTANCE.isEnumPropertyIn( theMatrix.modelType, ModelType.valueOf( modelType ) ) );
        }
        if (amount != null) {
        	expressions.add( theMatrix.amount.goe(amount) );
        }
		return BooleanExpression.allOf(expressions.toArray(new BooleanExpression[expressions.size()]));
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getModelType() {
		return modelType;
	}

	public void setModelType(String modelType) {
		this.modelType = modelType;
	}

	public Double getAmount() {
		return amount;
	}

	public void setAmount(Double amount) {
		this.amount = amount;
	}

}
