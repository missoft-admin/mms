package com.transretail.crm.web.member.controller;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyObject;
import static org.mockito.Matchers.anyString;
import static org.mockito.Matchers.argThat;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.doAnswer;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.reset;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.flash;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.model;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.view;

import java.util.Locale;

import javax.servlet.http.HttpServletRequest;

import org.hamcrest.core.IsInstanceOf;
import org.joda.time.DateTime;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentMatcher;
import org.mockito.internal.matchers.InstanceOf;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.BeanPostProcessor;
import org.springframework.beans.factory.config.InstantiationAwareBeanPostProcessorAdapter;
import org.springframework.context.MessageSource;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.convert.converter.Converter;
import org.springframework.format.FormatterRegistry;
import org.springframework.security.authentication.TestingAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.context.SecurityContextImpl;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.AnnotationConfigWebContextLoader;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

import com.transretail.crm.common.reporting.service.ReportService;
import com.transretail.crm.common.service.exception.MessageSourceResolvableException;
import com.transretail.crm.core.entity.lookup.LookupDetail;
import com.transretail.crm.core.security.model.impl.CustomSecurityUserDetailsImpl;
import com.transretail.crm.core.service.CodePropertiesService;
import com.transretail.crm.core.service.LookupService;
import com.transretail.crm.core.service.StoreService;
import com.transretail.crm.web.controller.AbstractMockMvcTest;
import com.transretail.crm.web.member.bean.ChangePasswordDto2;
import com.transretail.crm.web.member.bean.RegisterDto;
import com.transretail.crm.web.member.bean.UpdatePinDto;
import com.transretail.crm.web.member.service.MemberServiceExt;

/**
 * @author Allan G. Ramirez (agramirez@exist.com)
 */
@RunWith(SpringJUnit4ClassRunner.class)
@WebAppConfiguration
@ContextConfiguration(loader = AnnotationConfigWebContextLoader.class)
public class MemberControllerTest extends AbstractMockMvcTest {
    @Configuration
    @EnableWebMvc
    static class Config {
        @Bean
        public MemberController memberController() {
            return new MemberController();
        }

        @Bean
        public Validator memberValidator() {
            return mock(Validator.class);
        }

        @Bean
        public Validator memberUpdateValidator() {
            return mock(Validator.class);
        }

        @Bean
        public MessageSource messageSource() {
            return mock(MessageSource.class);
        }

        @Bean
        public MemberServiceExt memberServiceExt() {
            return mock(MemberServiceExt.class);
        }

        @Bean
        public StoreService storeService() {
            return mock((StoreService.class));
        }

        @Bean
        public LookupService lookupService() {
            return mock((LookupService.class));
        }

        @Bean
        public CodePropertiesService codePropertiesService() {
            return mock((CodePropertiesService.class));
        }

        @Bean
        public ReportService reportsService() {
            return mock(ReportService.class);
        }

        @Bean
        public BeanPostProcessor mvcPostProcessor() {
            return new InstantiationAwareBeanPostProcessorAdapter() {
                @Override
                public Object postProcessAfterInitialization(Object bean, String beanName) throws BeansException {
                    if (FormatterRegistry.class.isAssignableFrom(bean.getClass())) {
                        FormatterRegistry formatterRegistry = (FormatterRegistry) bean;
                        formatterRegistry.addConverter(new Converter<String, DateTime>() {
                            @Override
                            public DateTime convert(String source) {
                                return DateTime.parse(source);
                            }
                        });
                    }
                    return bean;
                }
            };
        }

    }

    private static final String CURRENT_USERNAME = "cloudx";
    private static final Long USER_ID = 1L;

    @Autowired
    private MemberServiceExt service;
    @Autowired
    private Validator memberValidator;
    @Autowired
    private MessageSource messageSource;
    @Autowired
    private StoreService storeService;
    @Autowired
    private LookupService lookupService;
    @Autowired
    private Validator memberUpdateValidator;
    @Autowired
    private CodePropertiesService codePropertiesService;
    @Autowired
    private ReportService reportService;

    @BeforeClass
    public static void setupCurrentUser() {
        CustomSecurityUserDetailsImpl user = new CustomSecurityUserDetailsImpl();
        user.setUserId(USER_ID);
        user.setUsername(CURRENT_USERNAME);

        SecurityContextImpl securityContext = new SecurityContextImpl();
        securityContext.setAuthentication(new TestingAuthenticationToken(user, ""));
        SecurityContextHolder.setContext(securityContext);
    }

    @AfterClass
    public static void removeCurrentUser() {
        SecurityContextHolder.clearContext();
    }

    public void doSetup() {
        reset(service);
        reset(memberValidator);
        reset(messageSource);
        reset(storeService);
        reset(lookupService);
        reset(memberUpdateValidator);
        reset(codePropertiesService);
        reset(reportService);
    }

//    @Test
//    public void showPointsPageTest() throws Exception {
//        Iterable<PointsTxnModel> points = Lists.newArrayList();
//        when(service.getUserPoints(USER_ID)).thenReturn(points);
//        when(service.getTotalPoints(USER_ID)).thenReturn(0L);
//        when(codePropertiesService.getHeaderGender()).thenReturn("MEM002");
//
//
//
//        mockMvc.perform(get("/member/points"))
//            .andExpect(model().attribute("list", points))
//            .andExpect(model().attribute("totalPoints", 0L))
//            .andExpect(view().name("member/pointsList"))
//            .andExpect(status().isOk());
//    }

    @Test
    public void showUpdatePinPageTest() throws Exception {
        mockMvc.perform(get("/member/pin/update"))
            .andExpect(model().attribute(MemberController.FORM_ATTR_NAME, new IsInstanceOf(UpdatePinDto.class)))
            .andExpect(view().name("member/changePin"))
            .andExpect(status().isOk());
    }

    @Test
    public void updatePinWithValidationErrorTest() throws Exception {
        UpdatePinDto dto = new UpdatePinDto();
        dto.setPin("111111");
        dto.setConfirmPin("222222");
        mockMvc.perform(setParams(post("/member/pin/update"), dto))
            .andExpect(view().name("member/changePin"))
            .andExpect(model().attributeHasFieldErrors(MemberController.FORM_ATTR_NAME, "pin"))
            .andExpect(status().isOk());

        verify(service, never()).updatePin(anyString(), anyString());
        verify(messageSource, never()).getMessage(anyString(), any(String[].class), any(Locale.class));
    }

    @Test
    public void updatePinSuccessTest() throws Exception {
        String pin = "123456";
        UpdatePinDto dto = new UpdatePinDto();
        dto.setPin(pin);
        dto.setConfirmPin(pin);
        when(messageSource.getMessage(eq("member.pin.change.successful"), any(String[].class), any(Locale.class)))
            .thenReturn("success");
        when(codePropertiesService.getHeaderGender()).thenReturn("MEM002");

        mockMvc.perform(setParams(post("/member/pin/update"), dto))
            .andExpect(view().name("member/changePin"))
            .andExpect(model().attributeExists("successMessages"))
            .andExpect(status().isOk());

        verify(service).updatePin(CURRENT_USERNAME, pin);
        verify(messageSource).getMessage(eq("member.pin.change.successful"), argThat(new ArgumentMatcher<Object[]>() {
            @Override
            public boolean matches(Object argument) {
                String[] args = (String[]) argument;
                return args != null && args.length == 1 && args[0].equals(CURRENT_USERNAME);
            }
        }), any(Locale.class));
    }

    @Test
    public void showRegistrationPageTest() throws Exception {
        ModelAndView view = mockMvc.perform(get("/member/registration")).andExpect(status().isOk()).andReturn().getModelAndView();
        assertEquals("member/registration", view.getViewName());
        Object form = view.getModel().get(MemberController.FORM_ATTR_NAME);
        assertTrue(form.getClass() + " is not an instance of " + RegisterDto.class, form instanceof RegisterDto);
    }

    @Test
    public void saveRegistrationWithValidationErrorTest() throws Exception {
        mockMvc.perform(
            post("/member/registration"))
            .andExpect(status().isOk())
            .andExpect(model().hasErrors())
            .andExpect(model().attributeHasFieldErrors(MemberController.FORM_ATTR_NAME, "username", "password", "contact"))
            .andExpect(view().name("member/registration"));

        verify(memberValidator, never()).validate(anyObject(), any(Errors.class));
        verify(service, never()).save(any(RegisterDto.class), any(HttpServletRequest.class));
        verify(messageSource, never()).getMessage(anyString(), any(String[].class), any(Locale.class));
    }

    @Test
    public void testSaveRegistrationWithMemberValidatorError() throws Exception {
        doAnswer(new Answer() {
            @Override
            public Object answer(InvocationOnMock invocation) throws Throwable {
                Object[] args = invocation.getArguments();
                Errors errors = (Errors) args[1];
                errors.rejectValue("username", "member.username.exists");
                errors.rejectValue("contact", "member.contact.exists");
                return null;
            }
        }).when(memberValidator).validate(argThat(new IsInstanceOf(RegisterDto.class)), argThat(new ArgumentMatcher<Errors>() {
            @Override
            public boolean matches(Object argument) {
                return Errors.class.isAssignableFrom(argument.getClass());
            }
        }));

        RegisterDto form = createForm();
        mockMvc.perform(
            setParams(post("/member/registration"), form))
            .andExpect(status().isOk())
            .andExpect(model().hasErrors())
            .andExpect(model().errorCount(2))
            .andExpect(model().attributeHasFieldErrors(MemberController.FORM_ATTR_NAME, "username", "contact"))
            .andExpect(view().name("member/registration"));

        verify(memberValidator).validate(argThat(new IsInstanceOf(RegisterDto.class)), argThat(new ArgumentMatcher<Errors>() {
            @Override
            public boolean matches(Object argument) {
                return Errors.class.isAssignableFrom(argument.getClass());
            }
        }));
        verify(service, never()).save(any(RegisterDto.class), any(HttpServletRequest.class));
        verify(messageSource, never()).getMessage(anyString(), any(String[].class), any(Locale.class));
    }

    @Test
    public void testSuccessfulSaveRegistration() throws Exception {
        RegisterDto form = createForm();
        mockMvc.perform(
            setParams(post("/member/registration"), form))
            .andExpect(status().isFound())
            .andExpect(model().hasNoErrors())
            .andExpect(view().name("redirect:/login"));
        verify(memberValidator).validate(argThat(new IsInstanceOf(RegisterDto.class)), argThat(new ArgumentMatcher<Errors>() {
            @Override
            public boolean matches(Object argument) {
                return Errors.class.isAssignableFrom(argument.getClass());
            }
        }));
        verify(service).save(any(RegisterDto.class), any(HttpServletRequest.class));
        verify(messageSource).getMessage(anyString(), any(String[].class), any(Locale.class));
    }

    @Test
    public void validateRegistrationInvalidCodeTest() throws Exception {
        String validationCode = "123456";
        when(messageSource.getMessage(eq("member.validation.error"), any(String[].class), any(Locale.class)))
            .thenReturn("error");
        mockMvc.perform(get("/member/validate?code=" + validationCode))
            .andExpect(view().name("redirect:/login"))
            .andExpect(flash().attributeExists("errorMessages"))
            .andExpect(flash().attributeCount(1))
            .andExpect(status().isFound());

        verify(service).validate(validationCode);
        verify(messageSource, never()).getMessage(eq("member.validation.confirmation"), any(String[].class), any(Locale.class));
        verify(messageSource).getMessage(eq("member.validation.error"), any(String[].class), any(Locale.class));
    }

    @Test
    public void validateRegistrationValidCodeTest() throws Exception {
        String validationCode = "123456";
        when(messageSource.getMessage(eq("member.validation.confirmation"), any(String[].class), any(Locale.class)))
            .thenReturn("success");
        when(service.validate(validationCode)).thenReturn(true);
        mockMvc.perform(get("/member/validate?code=" + validationCode))
            .andExpect(view().name("redirect:/login"))
            .andExpect(flash().attributeExists("successMessages"))
            .andExpect(flash().attributeCount(1))
            .andExpect(status().isFound());
        verify(service).validate(validationCode);
        verify(messageSource).getMessage(eq("member.validation.confirmation"), any(String[].class), any(Locale.class));
        verify(messageSource, never()).getMessage(eq("member.validation.error"), any(String[].class), any(Locale.class));
    }

    @Test
    public void showPasswordChangePageTest() throws Exception {
        mockMvc.perform(get("/member/password/change"))
            .andExpect(model().attribute("username", CURRENT_USERNAME))
            .andExpect(model().attribute(MemberController.FORM_ATTR_NAME, new InstanceOf(ChangePasswordDto2.class)))
            .andExpect(view().name("member/changePassword"))
            .andExpect(status().isOk());
    }

    @Test
    public void savePasswordChangeWithBindingErrorTest() throws Exception {
        // Save empty fields
        ChangePasswordDto2 dto = new ChangePasswordDto2();
        mockMvc.perform(
            setParams(post("/member/password/change"), dto))
            .andExpect(status().isOk())
            .andExpect(model().hasErrors())
            .andExpect(model().errorCount(3))
            .andExpect(
                model().attributeHasFieldErrors(MemberController.FORM_ATTR_NAME, "oldPassword", "newPassword", "confirmPassword"))
            .andExpect(view().name("member/changePassword"));

        verify(service, never())
            .sendChangePasswordValidation(anyString(), any(ChangePasswordDto2.class), any(HttpServletRequest.class));
        verify(messageSource, never()).getMessage(anyString(), any(String[].class), any(Locale.class));
    }

    @Test
    public void savePasswordChangeForUnexistingUserTest() throws Exception {
        // Save empty fields
        ChangePasswordDto2 dto = new ChangePasswordDto2();
        dto.setOldPassword("old");
        dto.setConfirmPassword("password");
        dto.setNewPassword("password");

        MessageSourceResolvableException exception =
            new MessageSourceResolvableException("member.username.notexist", new String[]{CURRENT_USERNAME},
                "User with username [" + CURRENT_USERNAME + " ] does not exist.");
        doThrow(exception).when(service)
            .sendChangePasswordValidation(eq(CURRENT_USERNAME), any(ChangePasswordDto2.class), any(HttpServletRequest.class));
        mockMvc.perform(
            setParams(post("/member/password/change"), dto))
            .andExpect(status().isOk())
            .andExpect(view().name("member/changePassword"));

        verify(service)
            .sendChangePasswordValidation(eq(CURRENT_USERNAME), any(ChangePasswordDto2.class), any(HttpServletRequest.class));
        verify(messageSource).getMessage(eq(exception), any(Locale.class));
    }

    @Test
    public void savePasswordChangeSuccessTest() throws Exception {
        ChangePasswordDto2 dto = new ChangePasswordDto2();
        dto.setOldPassword("old");
        dto.setConfirmPassword("pass");
        dto.setNewPassword("pass");

        mockMvc.perform(
            setParams(post("/member/password/change"), dto))
            .andExpect(status().isOk())//.andExpect(status().isFound())
            .andExpect(model().hasNoErrors())
            .andExpect(view().name("member/changePassword"));//.andExpect(view().name("redirect:/"));

        verify(service)
            .sendChangePasswordValidation(eq(CURRENT_USERNAME), argThat(new ArgumentMatcher<ChangePasswordDto2>() {
                @Override
                public boolean matches(Object argument) {
                    ChangePasswordDto2 dto = (ChangePasswordDto2) argument;
                    return dto.getOldPassword().equals("old") && dto.getPassword().equals("pass") && dto.getConfirmPassword()
                        .equals("pass");
                }
            }), any(HttpServletRequest.class));
        verify(messageSource).getMessage(eq("member.password.change.confirmation"), any(String[].class), any(Locale.class));
    }

    @Test
    @Ignore("Fix this")
    public void validatePasswordChangeWithErrorTest() throws Exception {
        String validationCode = "xcloudx12345";
        when(service.validateChangePassword(CURRENT_USERNAME, validationCode)).thenReturn(false);
        when(messageSource.getMessage(eq("member.password.change.validation.error"), any(String[].class), any(Locale.class)))
            .thenReturn("error");

        mockMvc.perform(get("/member/" + CURRENT_USERNAME + "/password/change").param("code", validationCode))
            .andExpect(model().attribute("errorMessages", "error"))
            .andExpect(view().name("member/changePasswordValidation"));
    }

    @Test
    @Ignore("Fix this")
    public void validatePasswordChangeSuccessTest() throws Exception {
        String validationCode = "xcloudx12345";
        when(service.validateChangePassword(CURRENT_USERNAME, validationCode)).thenReturn(true);
        when(messageSource.getMessage(eq("member.password.change.validation.successful"), any(String[].class), any(Locale.class)))
            .thenReturn("success");

        mockMvc.perform(get("/member/" + CURRENT_USERNAME + "/password/change").param("code", validationCode))
            .andExpect(model().attribute("successMessages", "success"))
            .andExpect(view().name("member/changePasswordValidation"));
    }

    private RegisterDto createForm() {
        RegisterDto form = new RegisterDto();
        form.setUsername("ramirezag");
        form.setLastName("ramirez");
        form.setFirstName("cloudx");
        form.setEmail("agramirez@exist.com");
        form.setPassword("Y3h3Y!");
        form.setConfirmPassword("Y3h3Y!");
        form.setGender(new LookupDetail("m"));
        form.setBirthDate(DateTime.now().minusYears(8));
        form.setContact("contact");
        return form;
    }
}
