alter table CRM_GC_CX_PROFILE 
        drop constraint FK_qekpx14mdaqkd38d2nyopqdhe;

alter table CRM_GC_CX_PROFILE modify CX_TYPE varchar2(255 char);

update CRM_GC_CX_PROFILE set cx_type = 'B2B' where cx_type = 'CXTP001';
update CRM_GC_CX_PROFILE set cx_type = 'INTERNAL' where cx_type = 'CXTP002';
update CRM_GC_CX_PROFILE set cx_type = 'GENERAL' where cx_type = 'CXTP003';

delete from CRM_REF_LOOKUP_DTL where hdr = 'GCX001';
delete from CRM_REF_LOOKUP_HDR where code = 'GCX001';