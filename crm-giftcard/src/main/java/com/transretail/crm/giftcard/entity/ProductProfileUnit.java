package com.transretail.crm.giftcard.entity;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.transretail.crm.core.entity.lookup.LookupDetail;
import com.transretail.crm.core.entity.support.CustomAuditableEntity;

@Entity
@Table(name = "CRM_GC_PROD_BU")
public class ProductProfileUnit extends CustomAuditableEntity<Long> implements Serializable {

	private static final long serialVersionUID = -2791740848439543948L;

	@ManyToOne
	@JoinColumn(name = "PROD_PROFILE")
	private ProductProfile productProfile;

	@ManyToOne
	@JoinColumn(name = "BUSINESS_UNIT")
	private LookupDetail businessUnit;

	public ProductProfile getProductProfile() {
		return productProfile;
	}

	public void setProductProfile(ProductProfile productProfile) {
		this.productProfile = productProfile;
	}

	public LookupDetail getBusinessUnit() {
		return businessUnit;
	}

	public void setBusinessUnit(LookupDetail businessUnit) {
		this.businessUnit = businessUnit;
	}

}
