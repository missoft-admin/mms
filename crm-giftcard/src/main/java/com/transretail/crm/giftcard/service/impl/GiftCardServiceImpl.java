package com.transretail.crm.giftcard.service.impl;

import com.google.common.collect.Lists;
import com.mysema.query.types.expr.BooleanExpression;
import com.transretail.crm.common.service.util.SpringDataPagingUtil;
import com.transretail.crm.core.entity.PeopleSoftStore;
import com.transretail.crm.core.entity.PosTransaction;
import com.transretail.crm.core.repo.PeopleSoftStoreRepo;
import com.transretail.crm.core.repo.PosTransactionRepo;
import com.transretail.crm.giftcard.dto.GiftCardDto;
import com.transretail.crm.giftcard.dto.GiftCardResultList;
import com.transretail.crm.giftcard.dto.GiftCardSearchDTO;
import com.transretail.crm.giftcard.entity.GiftCard;
import com.transretail.crm.giftcard.entity.GiftCardTransaction;
import com.transretail.crm.giftcard.entity.GiftCardTransactionItem;
import com.transretail.crm.giftcard.entity.PosGcTransaction;
import com.transretail.crm.giftcard.entity.support.GiftCardSaleTransaction;
import com.transretail.crm.giftcard.repo.GiftCardInventoryRepo;
import com.transretail.crm.giftcard.repo.GiftCardRepo;
import com.transretail.crm.giftcard.repo.GiftCardTransactionRepo;
import com.transretail.crm.giftcard.service.GiftCardAccountingService;
import com.transretail.crm.giftcard.service.GiftCardService;
import java.math.BigDecimal;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import org.joda.time.LocalDateTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 */
@Service("giftCardService")
public class GiftCardServiceImpl implements GiftCardService {

    private static final Logger logger = LoggerFactory.getLogger(GiftCardServiceImpl.class);
    @Autowired
    private GiftCardRepo repo;
    @Autowired
    private GiftCardTransactionRepo transactionRepo;
    @Autowired
    private PosTransactionRepo posTxnRepo;
    @Autowired
    private GiftCardInventoryRepo gcInventoryRepo;
    @Autowired
    private PeopleSoftStoreRepo peopleSoftStoreRepo;
    @Autowired
    private GiftCardAccountingService acctService;

    @Override
    @Transactional(readOnly = true)
    public com.transretail.crm.giftcard.dto.GiftCardDto getGiftCard(Long giftCardId) {
        return new GiftCardDto(repo.findOne(giftCardId));
    }

    @Override
    @Transactional
    public Long saveGiftCard(GiftCardDto dto) {
        return repo.save(dto.toModel()).getId();
    }

    @Override
    @Transactional
    public void updateGiftCard(Long giftCardId, GiftCardDto dto) {
        GiftCard gc = repo.findOne(giftCardId);
        dto.update(gc);
        repo.save(gc);
    }

    @Override
    @Transactional(readOnly = true)
    public GiftCardResultList getGiftCards(GiftCardSearchDTO searchForm) {
        Pageable pageable = SpringDataPagingUtil.INSTANCE.createPageable(searchForm.getPagination());
        BooleanExpression filter = searchForm.createSearchExpression();
        Page<GiftCard> page = filter != null ? repo.findAll(filter, pageable) : repo.findAll(pageable);
        return new GiftCardResultList(toDto(page.getContent()), page.getTotalElements(), page.hasPreviousPage(),
                page.hasNextPage());
    }

    private List<GiftCardDto> toDto(Collection<GiftCard> items) {
        List<GiftCardDto> results = Lists.newArrayList();
        for (GiftCard item : items) {
            results.add(new GiftCardDto(item));
        }
        return results;
    }

    @Override
    public void settleGiftCardTransactions() {
        Map<List<?>, List<PosGcTransaction>> missingPosGcTxns = transactionRepo.findMissingPosGcTransactions();

        for (List<?> key : missingPosGcTxns.keySet()) {
            String transactionId = key.get(0).toString();
            Character type = (Character) key.get(1);
            PosTransaction posTxn = posTxnRepo.findOne(transactionId);

            GiftCardTransaction giftCardTransaction = new GiftCardTransaction();
            giftCardTransaction.setTransactionNo(transactionId);
            final GiftCardSaleTransaction txType = parseTransactionType(type, transactionId);
            giftCardTransaction.setTransactionType(txType);
            final String merchantId = posTxn.getStore().getCode();
            giftCardTransaction.setMerchantId(merchantId);
            giftCardTransaction.setPeoplesoftStoreMapping(getPeoplesoftStoreMapping(merchantId));
            giftCardTransaction.setTransactionDate((LocalDateTime) key.get(2));
            giftCardTransaction.setSettlement(Boolean.TRUE);

            Double totalTxAmount = 0.0;
            for (PosGcTransaction i : missingPosGcTxns.get(key)) {
                GiftCardTransactionItem item = new GiftCardTransactionItem();
                item.setCurrentAmount(i.getBalance());
                final Double txAmount = i.getAmount();
                item.setTransactionAmount(txAmount);
                item.setGiftCard(gcInventoryRepo.findByBarcode(i.getCardNumber()));
                giftCardTransaction.addItem(item);

                totalTxAmount += txAmount;
            }

            if (txType == GiftCardSaleTransaction.ACTIVATION) {
                acctService.forB2CActivation(giftCardTransaction.getTransactionDate().toDateTime(),
                        new BigDecimal(totalTxAmount), giftCardTransaction.getMerchantId(),
                        String.format("Transaction No.: %s \n", giftCardTransaction.getTransactionNo()));
            } else if (txType == GiftCardSaleTransaction.VOID_ACTIVATED) {
                acctService.forB2CSalesVoiding(giftCardTransaction.getTransactionDate().toDateTime(),
                        new BigDecimal(totalTxAmount).negate(), giftCardTransaction.getMerchantId(),
                        String.format("Transaction No.: %s \n Reference Transaction No.: %s",
                                giftCardTransaction.getTransactionNo(), giftCardTransaction.getRefTransaction().getTransactionNo())); // TODO: Possible Runtime NPE??
            } else if (txType == GiftCardSaleTransaction.REDEMPTION) {
                acctService.forB2CRedemption(giftCardTransaction.getTransactionDate().toDateTime(),
                        new BigDecimal(totalTxAmount).negate(),
                        giftCardTransaction.getMerchantId(), String.format("Transaction No.: %s",
                                giftCardTransaction.getTransactionNo()));
            } else if (txType == GiftCardSaleTransaction.VOID_REDEMPTION) {
                acctService.forB2CRedemptionVoiding(giftCardTransaction.getTransactionDate().toDateTime(),
                        new BigDecimal(totalTxAmount).abs(), giftCardTransaction.getMerchantId(),
                        String.format("Transaction No.: %s \n Reference Transaction No.: %s",
                                giftCardTransaction.getTransactionNo(), giftCardTransaction.getRefTransaction().getTransactionNo()));
            } // TODO: mco - RELOAD is not yet implemented in POSS 

            transactionRepo.save(giftCardTransaction);
        }
    }

    GiftCardSaleTransaction parseTransactionType(Character type, String transactionId) {
        // TODO: mco - RELOAD is not yet implemented in POSS 
        GiftCardSaleTransaction txnType = null;
        switch (type) {
            case 'A':
                txnType = GiftCardSaleTransaction.ACTIVATION;
                break;
            case 'P':
                txnType = GiftCardSaleTransaction.REDEMPTION;
                break;
            case 'V':
                if (transactionRepo.findPosGcTransactionType(transactionId) == 'A') {
                    txnType = GiftCardSaleTransaction.VOID_ACTIVATED;
                } else {
                    txnType = GiftCardSaleTransaction.VOID_REDEMPTION;
                }
                break;
            case 'B':
                txnType = GiftCardSaleTransaction.INQUIRY;
                break;
        }

        return txnType;
    }

    private PeopleSoftStore getPeoplesoftStoreMapping(String merchant) {
        PeopleSoftStore peopleSoftStore = peopleSoftStoreRepo.findByStoreCode(merchant);

        if (peopleSoftStore == null) {
            logger.warn("No People Store found for merchant {}.", merchant);
        }

        return peopleSoftStore;
    }
}
