package com.transretail.crm.core.dto;

import java.util.ArrayList;
import java.util.List;

import com.mysema.query.types.expr.BooleanExpression;
import com.transretail.crm.common.service.dto.request.AbstractSearchFormDto;
import com.transretail.crm.core.entity.enums.Status;
import com.transretail.crm.core.entity.lookup.QLookupDetail;

import org.apache.commons.lang3.ArrayUtils;
import org.springframework.util.StringUtils;

/**
 *
 */
public class LookupDetailSearchDto extends AbstractSearchFormDto {
    private String code;
    private String description;
    private String headerCode;
    private String headerDescription;
    private Status status;
    private String descLike;
    private String[] excCodes;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getHeaderCode() {
        return headerCode;
    }

    public void setHeaderCode(String headerCode) {
        this.headerCode = headerCode;
    }

    public String getHeaderDescription() {
        return headerDescription;
    }

    public void setHeaderDescription(String headerDescription) {
        this.headerDescription = headerDescription;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public void setDescLike(String descLike) {
		this.descLike = descLike;
	}

    public void setExcCodes(String[] excCodes) {
		this.excCodes = excCodes;
	}

	@Override
    public BooleanExpression createSearchExpression() {
        List<BooleanExpression> expressions = new ArrayList<BooleanExpression>();
        QLookupDetail qLookupDetail = QLookupDetail.lookupDetail;
        if (StringUtils.hasText(code)) {
            expressions.add(qLookupDetail.code.startsWith(code));
        }
        if (StringUtils.hasText(description)) {
            expressions.add(qLookupDetail.description.startsWith(description));
        }
        if (StringUtils.hasText(headerCode)) {
            expressions.add(qLookupDetail.header.code.startsWith(headerCode));
        }
        if (StringUtils.hasText(headerDescription)) {
            expressions.add(qLookupDetail.header.description.startsWith(headerDescription));
        }
        if (status != null) {
            expressions.add(qLookupDetail.status.eq(status));
        }
        if ( StringUtils.hasText( descLike ) ) {
            expressions.add( qLookupDetail.description.startsWithIgnoreCase( descLike )
            		.or( qLookupDetail.description.containsIgnoreCase( descLike ) ) );
        }
        if ( ArrayUtils.isNotEmpty( excCodes ) ) {
            expressions.add( qLookupDetail.description.notIn( excCodes ) );
        }

        return BooleanExpression.allOf(expressions.toArray(new BooleanExpression[expressions.size()]));
    }
}
