package com.transretail.crm.core.entity;

import java.text.DateFormat;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.envers.RevisionEntity;
import org.hibernate.envers.RevisionNumber;
import org.hibernate.envers.RevisionTimestamp;

/**
 * @author Allan G. Ramirez (agramirez@exist.com)
 */
@RevisionEntity
@Entity
@Table(name = "CRM_REVINFO")
public class CrmRevInfo {

    @Id
    @GeneratedValue
    @RevisionNumber
    private long id;

    @RevisionTimestamp
    @Temporal(TemporalType.TIMESTAMP)
    private Date revisionDate;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public Date getRevisionDate() {
        return revisionDate;
    }

    public void setRevisionDate(Date revisionDate) {
        this.revisionDate = revisionDate;
    }

    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof CrmRevInfo)) return false;

        CrmRevInfo that = (CrmRevInfo) o;

        if (id != that.id) return false;
        if (revisionDate != that.revisionDate) return false;

        return true;
    }

    public int hashCode() {
        long result = id;
        result = 31 * result + (revisionDate.getTime() ^ (revisionDate.getTime() >>> 32));
        return (int) result;
    }

    public String toString() {
        return "CRM RevInfo(id = " + id + ", revisionDate = " + DateFormat.getDateTimeInstance().format(getRevisionDate()) + ")";
    }
}
