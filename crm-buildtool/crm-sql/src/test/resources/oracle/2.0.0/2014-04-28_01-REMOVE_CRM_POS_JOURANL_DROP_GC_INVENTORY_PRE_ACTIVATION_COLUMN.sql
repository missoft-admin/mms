drop table CRM_GC_POS_JOURNAL cascade constraints;

alter table CRM_GC_INVENTORY drop column PRE_ACTIVATION_KEY;
alter table CRM_GC_INVENTORY drop column PRE_ACTIVATION_DATE;