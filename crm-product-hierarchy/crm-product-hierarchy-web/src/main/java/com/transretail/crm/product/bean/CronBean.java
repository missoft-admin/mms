package com.transretail.crm.product.bean;

/**
 * @author Allan G. Ramirez (agramirez@exist.com)
 */
public class CronBean {
    private String cronExpression;

    public String getCronExpression() {
        return cronExpression;
    }

    public void setCronExpression(String cronExpression) {
        this.cronExpression = cronExpression;
    }
}
