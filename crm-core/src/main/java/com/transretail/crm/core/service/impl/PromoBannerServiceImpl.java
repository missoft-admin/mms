package com.transretail.crm.core.service.impl;

import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.joda.time.LocalDate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.google.common.collect.Lists;
import com.mysema.query.types.expr.BooleanExpression;
import com.transretail.crm.common.service.dto.request.PagingParam;
import com.transretail.crm.common.service.dto.response.ResultList;
import com.transretail.crm.common.service.util.SpringDataPagingUtil;
import com.transretail.crm.core.dto.PromoBannerDto;
import com.transretail.crm.core.dto.PromoBannerSearchDto;
import com.transretail.crm.core.entity.PromoBanner;
import com.transretail.crm.core.entity.QPromoBanner;
import com.transretail.crm.core.entity.enums.PromoImageType;
import com.transretail.crm.core.repo.PromoBannerRepo;
import com.transretail.crm.core.service.CodePropertiesService;
import com.transretail.crm.core.service.PromoBannerService;
import com.transretail.crm.core.util.generator.IdGeneratorService;

@Service("promoBannerService")
@Transactional
public class PromoBannerServiceImpl implements PromoBannerService {
	@Autowired
	private PromoBannerRepo promoBannerRepo;
	@Autowired
	private IdGeneratorService accountIdGeneratorService;
	@Autowired
	private CodePropertiesService codePropertiesService;

	@Override
	public PromoBannerDto save(PromoBanner promoBanner) {
		return new PromoBannerDto(promoBannerRepo.save(promoBanner));
	}
	
	@Override
	public PromoBannerDto save(PromoBannerDto dto) {
		if(dto.getImage() == null && StringUtils.isNotBlank(dto.getId())) {
			PromoBanner promoBanner = promoBannerRepo.findOne(dto.getId());
			if(promoBanner != null) {
				dto.setImage(promoBanner.getImage());
			}
		}
		
		if(StringUtils.isBlank(dto.getId())) {
			dto.setId(accountIdGeneratorService.generateId());
		}
		return new PromoBannerDto(promoBannerRepo.save(dto.toModel()));
	}

	@Override
	public PromoBannerDto findById(String id) {
		if(id != null) {
			PromoBanner result = promoBannerRepo.findOne(id);
			if(result != null) {
				return new PromoBannerDto(result);
			}
		}
		return null;
	}

	@Override
	public ResultList<PromoBannerDto> searchPromoBanner(PromoBannerSearchDto dto) {
		BooleanExpression filter = dto.createSearchExpression();
		PagingParam paging = dto.getPagination();
		
		if(paging.getPageSize() >= 0) {
			Pageable pageable = SpringDataPagingUtil.INSTANCE.createPageable(dto.getPagination());
			Page<PromoBanner> page = promoBannerRepo.findAll(filter, pageable);
			List<PromoBannerDto> list = convertToPromoBannerDtoList(page.getContent());
			return new ResultList<PromoBannerDto>(list, list.size(), page.hasPreviousPage(), page.hasNextPage());
		}
		
		if(paging.getOrder() != null && !paging.getOrder().isEmpty()) {
			List<PromoBannerDto> list = convertToPromoBannerDtoList(
					promoBannerRepo.findAll(filter, SpringDataPagingUtil.INSTANCE.createOrderSpecifier(
							paging, PromoBanner.class)));
			return new ResultList<PromoBannerDto>(list, list.size(), false, false);
		}
		
		List<PromoBannerDto> results = convertToPromoBannerDtoList(promoBannerRepo.findAll(filter));
		return new ResultList<PromoBannerDto>(results, results.size(), false, false);
	}
	
	@Override
	public List<PromoBannerDto> findCurrentPromoBanners() {
		QPromoBanner promoBanner = QPromoBanner.promoBanner;
		LocalDate today = LocalDate.now();
		
		return convertToPromoBannerDtoList(promoBannerRepo.findAll(
				promoBanner.startDate.loe(today)
				.and(promoBanner.endDate.goe(today))
				.and(promoBanner.status.code.eq(codePropertiesService.getDetailStatusActive()))
				.and(promoBanner.type.eq(PromoImageType.BANNER))));
	}
	
	@Override
	public List<PromoBannerDto> findActivePromoImagesByType(PromoImageType type) {
		QPromoBanner promoBanner = QPromoBanner.promoBanner;
		LocalDate today = LocalDate.now();
		
		return convertToPromoBannerDtoList(promoBannerRepo.findAll(
				promoBanner.startDate.loe(today)
				.and(promoBanner.endDate.goe(today))
				.and(promoBanner.status.code.eq(codePropertiesService.getDetailStatusActive()))
				.and(promoBanner.type.eq(type))));
	}
	
	@Override
	public void delete(String id) {
		promoBannerRepo.delete(id);
	}

	private List<PromoBannerDto> convertToPromoBannerDtoList(Iterable<PromoBanner> source) {
		List<PromoBannerDto> result = Lists.newArrayList();
		for(PromoBanner promoBanner : source) {
			result.add(new PromoBannerDto(promoBanner));
		}
		return result;
	}
}
