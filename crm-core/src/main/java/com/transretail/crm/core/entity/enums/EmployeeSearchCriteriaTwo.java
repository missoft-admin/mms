package com.transretail.crm.core.entity.enums;

public enum EmployeeSearchCriteriaTwo {

	ACCOUNTID ( "Employee Id", "accountId" ),
	CONTACT ( "Contact", "contact" ),
	EMAIL ( "E-mail", "email" ),
	KTPID( "KTP ID", "ktpId" ),
	NAME( "First/Last Name", "name" );

    private EmployeeSearchCriteriaTwo(String inDesc, String inValue){
        this.desc = inDesc;
        this.value = inValue;
    }

    private final String desc;
    private final String value;
    public String getDescription() {
    	return desc;
    }
    public String getValue() {
    	return value;
    }
}
