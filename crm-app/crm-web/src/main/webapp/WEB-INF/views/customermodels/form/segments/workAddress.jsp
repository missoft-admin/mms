<%@ include file="../../../common/taglibs.jsp" %>


  <!-- Address -->
	<fieldset class="form-horizontal container-fluid">
	  <legend><spring:message code="label_com_transretail_crm_entity_customermodel_businessaddress" /></legend>
    <div class="row-fluid">
      <spring:message code="label_address_street" var="street"/>
        <fieldset class="span7">
          <label for="street">${street }</label>
          <form:input path="professionalProfile.businessAddress.street" placeholder="${street}" cssClass="span11" />
        </fieldset>
      <spring:message code="label_address_number" var="streetNumber"/>
      <fieldset class="span5">
        <label for="streetNumber">${streetNumber }</label>
          <form:input path="professionalProfile.businessAddress.streetNumber" cssClass="span10" placeholder="${streetNumber}" />
        </fieldset>
    </div>
    <div class="row-fluid">
        <spring:message code="label_address_block" var="block"/>
        <fieldset class="span3">
          <label for="block">${block }</label>
          <form:input path="professionalProfile.businessAddress.block" placeholder="${block}" cssClass="span10" />
        </fieldset>
        <spring:message code="label_address_km" var="km"/>
        <fieldset class="span3">
          <label for="km">${km }</label>
          <form:input path="professionalProfile.businessAddress.km" placeholder="${km}" cssClass="span10 intInput" />
        </fieldset>
        <%-- <spring:message code="label_address_rtrw" var="rtrw"/>
        <fieldset class="span3">
          <label for="rt">${rtrw}</label>
          <form:input path="professionalProfile.businessAddress.rt" placeholder="${rtrw}" cssClass="span11" />
        </fieldset> --%>
        <spring:message code="label_address_rt" var="rt"/>
        <fieldset class="span3">
          <label for="rt">${rt }</label>
          <form:input path="professionalProfile.businessAddress.rt" placeholder="${rt}" cssClass="span10 intInput" />
        </fieldset>
        <spring:message code="label_address_rw" var="rw"/>
        <fieldset class="span3">
          <label for="rw">${rw }</label>
          <form:input path="professionalProfile.businessAddress.rw" placeholder="${rw}" cssClass="span11 intInput" />
        </fieldset>
    </div>
    <div class="row-fluid">
        <spring:message code="label_address_building" var="building"/>
        <fieldset class="span6">
          <label for="building">${building }</label>
          <form:input path="professionalProfile.businessAddress.building" placeholder="${building}" cssClass="span11" />
        </fieldset>
        <spring:message code="label_address_floor" var="floor"/>
        <fieldset class="span3">
          <label for="floor">${floor }</label>
          <form:input path="professionalProfile.businessAddress.floor" placeholder="${floor}" cssClass="span10 intInput" />
        </fieldset>
        <spring:message code="label_address_room" var="room"/>
        <fieldset class="span3">
          <label for="room">${room }</label>
          <form:input path="professionalProfile.businessAddress.room" placeholder="${room}" cssClass="span10" />
        </fieldset>
    </div>
    <div class="row-fluid">
        <spring:message code="label_address_postalcode" var="postCode"/>
        <fieldset class="span4">
          <label for="postCode">${postCode }</label>
          <form:input path="professionalProfile.businessAddress.postCode" placeholder="${postCode}" cssClass="span11" />
        </fieldset>
        <spring:message code="label_address_subdistrict" var="subdistrict"/>
        <fieldset class="span4">
          <label for="subdistrict">${subdistrict }</label>
          <form:input path="professionalProfile.businessAddress.subdistrict" placeholder="${subdistrict}" cssClass="span11" />
        </fieldset>
        <spring:message code="label_address_district" var="district"/>
        <fieldset class="span4">
          <label for="district">${district }</label>
          <form:input path="professionalProfile.businessAddress.district" placeholder="${district}" cssClass="span11" />
        </fieldset>
    </div>
    <div class="row-fluid provinceCityBlock">
        <spring:message code="label_address_province" var="province"/>
        <fieldset class="span6">
          <label for="province">${province }</label>
          <select name="professionalProfile.businessAddress.province" class="provinceSelect">
            <option value=""></option>
            <c:forEach items="${provinces}" var="province">
            <option value="${province.name}">${province.name}</option>
            </c:forEach>
          </select>
         <%-- <form:input path="professionalProfile.businessAddress.province" placeholder="${province}" cssClass="span11" /> --%>
        </fieldset>
        <spring:message code="label_address_city" var="city"/>
        <fieldset class="span6">
          <label for="city">${city }</label>
          <select name="professionalProfile.businessAddress.city" class="citySelect">
          <option value=""></option>
          </select>
          <%--<form:input path="professionalProfile.businessAddress.city" placeholder="${city}" cssClass="span11" /> --%>
        </fieldset>
    </div>
	</fieldset>