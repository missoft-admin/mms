package com.transretail.crm.web.controller;

import javax.servlet.http.HttpServletRequest;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;

import com.transretail.crm.core.entity.RewardSchemeModel;
import com.transretail.crm.core.service.PointsSchemeService;
import com.transretail.crm.web.controller.util.VoucherSchemeValidator;
import com.transretail.crm.web.rest.BaseMockController;


public class VoucherSchemeControllerTest extends BaseMockController {

	@Mock
	PointsSchemeService service;
	
	@Mock
	VoucherSchemeValidator validator;
	
	@Mock
	Model uiModel;
	
	@Mock
	BindingResult bindingResult;
	
	@Mock
	HttpServletRequest request;
	
	@Mock
	RewardSchemeModel vsModel;
	
	@InjectMocks
	RewardSchemeController controller;
	
	@Before
	public void setUp() {
		super.setUp();
	}
	
	@Test
	public void testCreateForm() {
		controller.createForm(uiModel);
		Mockito.verify(uiModel, Mockito.times(2)).addAttribute(Mockito.anyString(), Mockito.any());
	}

	@Test
	public void testDelete() {
		controller.delete(1L, null, null, uiModel);
		Mockito.verify(service).deletePointsScheme(Mockito.anyLong());
		Mockito.verify(uiModel, Mockito.times(2)).addAttribute(Mockito.anyString(), Mockito.any());
	}
	
	@Test
	public void testPopulate() {
		Mockito.when(service.findPointsScheme(Mockito.anyLong())).thenReturn(vsModel);
		controller.populate("1", vsModel, bindingResult, request);
		
	}
	
	@Test
	public void testCreate() {
		
	}
	
	@Test
	public void testUpdate() {
		
	}
}
