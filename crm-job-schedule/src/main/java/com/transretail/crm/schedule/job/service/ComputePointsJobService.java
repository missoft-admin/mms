package com.transretail.crm.schedule.job.service;


public interface ComputePointsJobService {

	void computeTotalPoints();

}