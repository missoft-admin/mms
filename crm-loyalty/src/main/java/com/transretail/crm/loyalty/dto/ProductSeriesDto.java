package com.transretail.crm.loyalty.dto;

import com.mysema.query.annotations.QueryProjection;
import com.transretail.crm.core.entity.lookup.LookupDetail;

public class ProductSeriesDto {
	
	private LookupDetail name;
	
	private String batchNo;
	
	private String startingSeries;
	
	private String endingSeries;
	
	private String boxNo;
	
	private Long quantity;
	
	private String referenceNo;
	
	public ProductSeriesDto() {}
	
	@QueryProjection
	public ProductSeriesDto(LookupDetail name, String startingSeries,
			String endingSeries) {
		super();
		this.name = name;
		this.startingSeries = startingSeries;
		this.endingSeries = endingSeries;
	}


	public LookupDetail getName() {
		return name;
	}

	public void setName(LookupDetail name) {
		this.name = name;
	}

	public String getStartingSeries() {
		return startingSeries;
	}

	public void setStartingSeries(String startingSeries) {
		this.startingSeries = startingSeries;
	}

	public String getEndingSeries() {
		return endingSeries;
	}

	public void setEndingSeries(String endingSeries) {
		this.endingSeries = endingSeries;
	}

	public String getBatchNo() {
		return batchNo;
	}

	public void setBatchNo(String batchNo) {
		this.batchNo = batchNo;
	}

	public String getBoxNo() {
		return boxNo;
	}

	public void setBoxNo(String boxNo) {
		this.boxNo = boxNo;
	}

	public Long getQuantity() {
		return quantity;
	}

	public void setQuantity(Long quantity) {
		this.quantity = quantity;
	}

	public String getReferenceNo() {
		return referenceNo;
	}

	public void setReferenceNo(String referenceNo) {
		this.referenceNo = referenceNo;
	}
	
	
	
}
