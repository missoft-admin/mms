<%-- 
    Document   : summary-dialog
    Created on : Nov 13, 2014, 9:22:38 AM
    Author     : Monte Cillo Co (mco@exist.com)
--%>

<%@ include file="../../../common/taglibs.jsp" %>
<!DOCTYPE html>
<style type="text/css">
  <!--
  #summary-dates-list .dataTable tr td:last-child {
    min-width: 20px;
  }
  -->  
</style>
<div id="summary-list-dialog" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
    <h4><spring:message code="physical_count_history_label" /></h4>
  </div>
  <div class="modal-body">
    <div id="summary-dates-list-container">
      <div id="summary-dates-list" ></div>
    </div>
  </div>
  <!--  <div class="modal-footer">
      <button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
      <button class="btn btn-primary">Save changes</button>
    </div>-->
</div>


<script>
  $(document).ready(function () {
    $list = $("#summary-dates-list");
    $list.ajaxDataTable({
      'autoload': false,
      'ajaxSource': "<c:url value="/giftcard/inventory/adjustment/history/list" />",
      'columnHeaders': [
	"<spring:message code="physical_count_summary_date_label" />",
	""
      ],
      'modelFields': [
	{name: 'timePresentation', sortable: false},
	{customCell: function (innerData, sSpecific, json) {
	    console.log('json::' + json);
	    if (sSpecific === 'display') {
	      return "<button type=\"button\"  data-target=\"" + ctx + "/giftcard/inventory/adjustment/print/pdf/" + json.isoFormat + "\" class=\"tiptip btn pull-left icn-print export-pdf\" title=\"<spring:message code="report_type_pdf"/>\">PDF</button>"
		      +
		      "<button type=\"button\"  data-target=\"" + ctx + "/giftcard/inventory/adjustment/print/excel/" + json.isoFormat + "\" class=\"tiptip btn pull-left icn-print export-excel\" title=\"<spring:message code="report_type_excel"/>\">EXCEL</button>";
	    }
	    else {
	      return '';
	    }
	  }
	  
	}
      ]
    }).on("click", ".export-pdf", function () {
      var url = $(this).data("target");
      window.open(url, '_blank', 'toolbar=0,location=0,menubar=0');
    }).on("click", ".export-excel", function () {
      var url = $(this).data("target");
      window.location.href = url;
    });
  });

  function reloadHistoryTable() {
    $("#summary-dates-list").ajaxDataTable('search', new Object());
  }

</script>
