<%@include file="/WEB-INF/views/common/taglibs.jsp" %>

<spring:message code="gc.stock.label.stockrequest" var="typeName" />
<sec:authentication var="userLocationCode" property="principal.inventoryLocation" />

<div class="page-header page-header2">
	<h1>${typeName}</h1>
</div>

<div id="requestSearchForm" class="well clearfix">
    <form:form id="searchForm" class="mb0">
      <div class="form-inline search-block">
        <label class="label-single">
          <h5><spring:message code="entity_search" />:</h5>
        </label>
            
        <select id="searchCategory">
          <c:forEach items="${searchcateg}" var="categ">
            <option value="${categ.key}">${categ.value}</option>
          </c:forEach>
        </select>
 
        <input id="searchField" name="requestedValue" type="text"/>
  
        <input type="button" id="searchButton" class="btn btn-primary ml5 mb10" value="<spring:message code="button_search"/>">
        <input type="button" id="clearButton" value="<spring:message code="label_clear" />" class="btn mb10 custom-reset" data-default-id="searchButton" />

        <div class="form-inline search-block">
          <label class="label-single">
          </label>
            <select id="status" name="singleStatus">
               <option value=""/><spring:message code="label_filter" /></option>
                <c:forEach items="${statuses}" var="status" varStatus="index">
                  <option value="${status}"><spring:message code="gc_stkreq_status_${status}" /></option>
                </c:forEach>
            </select> 
            <!--<label><spring:message code="gc.stock.label.requestdate" />:</label>-->
            <input type="text" id="searchDateFrom" name="reqDateFrom" class="input-small" placeholder="<spring:message code="label_from" />"/>
            <input type="text" id="searchDateTo" name="reqDateTo" class="input-small" placeholder="<spring:message code="label_to" />" />
        </div>
      </div>
    </form:form>
</div>

<c:if test="${isHeadOffice == true}">
  <button type="button" id="printList" class="btn btn-primary" data-url="<spring:url value="/stockrequest/printlist" />"><spring:message code="gc.stock.label.print.list" /></button>
</c:if>
<div class="pull-right">
  <button type="button" class="modalBtn btn btn-primary mb20" id="approveSelected" data-url=""><spring:message code="label_approve_selected" /></button>
	<button type="button" class="modalBtn btn btn-primary mb20" id="createStockRequest" data-url="<spring:url value="/stockrequest/create" />"><spring:message code="global_menu_new" arguments="${typeName}"/></button>	
</div>

<div class="clearfix"></div>

<div id="stockRequestList">
	<div id="list_stockrequest"
		data-url="<c:url value="/stockrequest/search/${id}" />"
		data-hdr-requestno="<spring:message code="gc.stock.label.requestno" />"
		data-hdr-transferrefno="<spring:message code="gc.stock.label.transferrefno" />"
		data-hdr-requestdate="<spring:message code="gc.stock.label.requestdate" />"
		data-hdr-sourcelocation="<spring:message code="gc.stock.label.sourcelocation" />"
		data-hdr-allocateto="<spring:message code="gc.stock.label.allocateto" />"
		data-hdr-status="<spring:message code="gc.stock.label.status" />"
		data-hdr-cardtype="<spring:message code="gc.stock.label.cardtype" />" 
        data-hdr-quantity="<spring:message code="gc.stock.label.quantity" />" 
		data-hdr-requestedby="<spring:message code="gc.stock.label.requestedby" />"
        data-hdr-lastupdated="<spring:message code="gc.stock.label.lastupdated" />"></div>
</div>

<div id="buttonContainer" class="hide">
	<div id="viewStockRequestBtnContainer">
		<input type="button" data-url="<c:url value="/stockrequest/view"/>/%ID%" class="btn modalBtn icn-view tiptip pull-left" title="<spring:message code="label_view_arg" arguments="${typeName}"/>">
	</div>
	<div id="editStockRequestBtnContainer">
		<input type="button" data-url="<c:url value="/stockrequest/edit"/>/%ID%" class="btn modalBtn icn-edit tiptip pull-left">
	</div>
	<div id="approveStockRequestBtnContainer">
		<input type="button" data-url="<c:url value="/stockrequest/approve"/>/%ID%" class="btn modalBtn icn-approve tiptip pull-left">
	</div>
  <div id="transferStockRequestBtnContainer">
    <input type="button" data-url="<c:url value="/stockrequest/transfer"/>" data-print="<c:url value="/stockrequest/printtransfer"/>" data-id="/%ID%" class="btn btnTransfer icn-transfer tiptip pull-left" title="<spring:message code="label_transfer_out"/>">
  </div>
  <div id="reprintBtnContainer">
    <input type="button" data-url="<c:url value="/stockrequest/reprint" />/%ID%" value="<spring:message code="label_lc_transfer_reprint" />" class="btn reprintBtn icn-print tiptip pull-left" title="<spring:message code="label_lc_transfer_reprint" />" />
  </div>
  <div id="receiveStockRequestBtnContainer">
    <input type="button" data-url="<c:url value="/stockrequest/transferin"/>/%ID%" class="btn btnTransferIn icn-recieve tiptip pull-left" title="<spring:message code="label_transfer_in"/>"/>
  </div>
  <div id="reserveGcBtnContainer">
    <input type="button" data-url="<c:url value="/stockrequest/reserve/gc"/>/%ID%" class="btn icn-recieve tiptip pull-left modalBtn" title="<spring:message code="gc.reserve.label"/>"/>
  </div>
</div>

<jsp:include page="../common/noDataFound.jsp"/>
<div class="modal hide groupModal nofly" tabindex="-1" role="dialog" aria-hidden="true" id="requestModal"></div>
<%@ include file="../common/confirm.jsp" %>

<script src='<c:url value="/js/bootstrap/bootstrap-datepicker.js"/>' type='text/javascript'><![CDATA[&nbsp; ]]></script>
<script src='<c:url value="/js/bootstrap/bootstrap-timepicker.min.js"/>' type='text/javascript'><![CDATA[&nbsp; ]]></script>
<link href="<c:url value="/css/bootstrap/datepicker.css" />" rel="stylesheet" />
<link href="<spring:url value="/css/dataTables.css" />" rel="stylesheet">
<link href="<spring:url value="/styles/temp.css" />" rel="stylesheet">
<script src="<spring:url value="/js/jquery/jquery.dataTables.js" />" ></script>
<script src="<spring:url value="/js/jquery/jquery.dataTables.linkLengthMenu.js" />"></script>
<script src="<spring:url value="/js/jquery/jquery.dataTables.customPaging.js" />"></script>
<script src="<spring:url value="/js/jquery/ajax-datatable.js" />" ></script>
<script src="<spring:url value="/js/common/form2js.js" />" ></script>
<script src="<spring:url value="/js/common/json2.js" />" ></script>
<script src="<spring:url value="/js/jquery/jquery.toObject.js" />" ></script>

<script type="text/javascript">
$(document).ready(function() {
    $("#searchDateFrom").datepicker({
    	autoclose: true, format: "dd-mm-yyyy"
    }).on('changeDate', function(selected){
        startDate = new Date(selected.date.valueOf());
        startDate.setDate(startDate.getDate(new Date(selected.date.valueOf())));
        $('#searchDateTo').datepicker('setStartDate', startDate);
    });

    $("#searchDateTo").datepicker({
    	autoclose: true, format: "dd-mm-yyyy"
    }).on('changeDate', function(selected){
        FromEndDate = new Date(selected.date.valueOf());
        FromEndDate.setDate(FromEndDate.getDate(new Date(selected.date.valueOf())));
        $('#searchDateFrom').datepicker('setEndDate', FromEndDate);
    });
    
	$stockRequestList = $("#list_stockrequest");
	$requestModal = $("#requestModal");
	$requestModal.modal({
		show: false
	}).on('hidden.bs.modal', function() {
		$(this).empty();
	});

	$("#createStockRequest").click(showModal);
	
	$("#printList").click(printList);
	
	var headers = [
	  { text : "<spring:message code="label_approve_all" /><br/>" + 
		      "<input type='checkbox' id='list_checkApproveAll' onclick='javascript: event.stopPropagation();' title='<spring:message code="label_approve_all" />?' />",
	    className : '${isHeadOffice}' == 'true'? "" : "hide"
	  },
	  { text: "", className : "hide" },
		$stockRequestList.data("hdrRequestno"),
		$stockRequestList.data("hdrTransferrefno"),
		$stockRequestList.data("hdrRequestdate"),
		$stockRequestList.data("hdrRequestedby"),
		$stockRequestList.data("hdrSourcelocation"),
		$stockRequestList.data("hdrAllocateto"),
		$stockRequestList.data("hdrStatus"),
		$stockRequestList.data("hdrCardtype"),
	  	$stockRequestList.data("hdrQuantity"),
	  	$stockRequestList.data("hdrLastupdated"),
		""
	];

	var modelFields = [
    {name: 'status', sortable: true, fieldCellRenderer: function(data, type, row) {
       return ( row.status == 'FORAPPROVAL' && '${isHeadOffice}' === 'true' )? "<input type='checkbox' data-id='" + row.id + "' class='list_checkApprove' />" : "";
    }, aaClassname : '${isHeadOffice}' === 'true'? "" : "hide" },
    {name: 'lastUpdated', sortable: true, dataType: "DATE", aaClassname : "hide"},
		{name: 'requestNo', sortable: false},
		{name: 'transferRefNo', sortable: true},
		{name: 'requestDate', sortable: true, fieldCellRenderer: function(data, type, row) {
			return row.requestDateString;
		}},
		{name: 'createUser', sortable: true},
		{name: 'sourceLocation', sortable: false, fieldCellRenderer: function(data, type, row) {
			return row.sourceLocationDescription;
		}},
		{name: 'allocateTo', sortable: true, fieldCellRenderer: function(data, type, row) {
			return row.allocateToDescription;
		}},
		{name: 'status', sortable: true, fieldCellRenderer: function(data, type, row) {
			if ('${isHeadOffice}' === "true") {
				//return row.statusForHO;
				return (row.statusForHO == 'FORAPPROVAL')? 'FOR APPROVAL' : ( (row.statusForHO)? row.statusForHO.replace( new RegExp( "_", "g" ), " " ) : "" );
			} else {
				//return row.status;
				return (row.status == 'FORAPPROVAL')? 'FOR APPROVAL' : ( (row.status)? row.status.replace( new RegExp( "_", "g" ), " " ) : "" );
			}
		}},
		{name: 'productCode', sortable: true, fieldCellRenderer: function(data, type, row) {
			return row.productDescription;
		}},
	  {name: 'quantity', sortable: true},
	  {name: 'lastUpdated', sortable: true, dataType: "DATE"},
		{customCell: function(innerData, sSpecific, json) {
			if(sSpecific == 'display') {
				var btnHtml = $("#viewStockRequestBtnContainer").html();

				if(json.status === "IN_TRANSIT" && json.allocateTo === '${userLocationCode}') {
				  btnHtml += $("#reprintBtnContainer").html();
				  btnHtml += $("#receiveStockRequestBtnContainer").html();
				}else if(json.status === "IN_TRANSIT") {
				  btnHtml += $("#reprintBtnContainer").html();
				}else if(json.status === "TRANSFERRED_IN") {
				  btnHtml += $("#reprintBtnContainer").html();
				}else if((json.status === "APPROVED" || json.status === "INCOMPLETE") && '${isHeadOffice}' === "true") {
				  btnHtml += $("#reserveGcBtnContainer").html();
				}
				else if(json.status === 'FOR_TRANSFER_OUT' && '${isHeadOffice}' === "true") {
					btnHtml += $("#transferStockRequestBtnContainer").html();
				}
				
				var rg = new RegExp("%ID%", 'g');
				btnHtml = btnHtml.replace(rg, json.id);
				
				return btnHtml;
			}
			else {
				return '';
			}
		}}
	];

	$stockRequestList.ajaxDataTable({
		'autoload': true,
	  'aaSorting' :[[ 1, 'desc' ]],
		'ajaxSource': $stockRequestList.data("url"),
		'columnHeaders': headers,
		'modelFields': modelFields
	})
	.on('click', '.modalBtn', showModal)
	.on('click', '.btnTransfer', transferStock)
	.on('click', '.reprintBtn', reprint)
	.on('click', '.btnTransferIn', transferInStock)
	.on( 'change', '#list_checkApproveAll', approveAll );

	initApproveSelected()
	function initApproveSelected() {
		  $( "#approveSelected" ).click( function(e) {
			    e.preventDefault();
			    var selectedReqs = $( ".list_checkApprove" ).map( function(){ if ( this.checked ) return $(this).data( "id" ); } ).get();
			    if ( selectedReqs.length ) {
		          getConfirm( '<spring:message code="label_approve_all" />?', function( result ) { 
		              if( result ) {
		                  $.post( 
		                      '<c:url value="/stockrequest/approveall/" />' + selectedReqs.join( "," ), 
		                      function( resp ) {
		                          if ( resp.success ) {
		                              location.reload();
		                          }
		                  });
		              }
		          });
			    }
		  });
	}
	function approveAll(e) {
		  e.preventDefault();
		  $( ".list_checkApprove" ).attr( "checked", this.checked );
		  /* if ( this.checked ) {
		      getConfirm( '<spring:message code="label_approve_all" />?', function( result ) { 
		          if( result ) {
		              $.post( 
		                  '<c:url value="/stockrequest/approveall/" />' + $( ".list_checkApprove" ).map( function(){ return $(this).data( "id" ); } ).get().join(","), 
		                  function( resp ) {
		                      if ( resp.success ) {
		                          location.reload();
		                      }
		              });
		          }
		      });
		  } */
	}

	$("#clearButton").click(function(e) {
	  	e.preventDefault();
        $("#searchField").val("");
        $("#searchDateFrom").val("");
        $("#searchDateTo").val("");
        $("#status").val("");
    });

    $("#searchButton").click(function(e) {
      	e.preventDefault();
        $("#searchField").attr('name', $("#searchCategory").val());
    	var formDataObj = $("#searchForm").toObject( { mode : 'first', skipEmpty : true } );
		$btnSearch = $(this);
		$btnSearch.prop('disabled', true);
        $stockRequestList.ajaxDataTable('search', formDataObj, function() {
                $btnSearch.prop( 'disabled', false );
        });
    });

	function showModal(e) {
		e.preventDefault();
		$.get($(this).data("url"), function(data) {
			$requestModal.modal('show').html(data);
			$requestModal.on('hidden.bs.modal', function() {
				$requestModal.empty();
			});
		}, "html");
	}
	
	function transferStock(e) {
	  e.preventDefault();
	  
	  var transferUrl = $(".btnTransfer").data("url") + $(this).data("id");
	  var printUrl = $(".btnTransfer").data("print") + $(this).data("id");
	  $(this).hide();
 	  $.post(
 	      transferUrl,
 	      function(response, status, jqXHR) {
 	      	window.open(printUrl, '_blank', 'toolbar=0,location=0,menubar=0');
 	    	$("#list_stockrequest").ajaxDataTable("search");
 	  	  },
 	  	  "json");
	}
	
	function reprint(e){
	  e.preventDefault();
	  window.open($(this).data("url"), '_blank', 'toolbar=0,location=0,menubar=0');
  	}
	
    function transferInStock() {
      var transferInUrl = $(this).data("url");
      $.get(transferInUrl, function(data) {
  		$requestModal.modal('show').html(data);
  		$requestModal.on('hidden.bs.modal', function() {
  			$requestModal.empty();
  		});
  		},
  		"html");
    }
    
    function printList() {
      var url = $(this).data("url");

      var serialize = $("#searchForm").serialize();

      serialize = serialize.replace("requestedValue", $("#searchForm").find("#searchCategory").val());

//       if (serialize !== null && serialize !== "" && serialize !== " ") {
//           window.location.href = url + "?" + serialize;
//       } else {
//           window.location.href = url;
//       }
      
      var validateUrl = ctx + "/stockrequest/print/validate";
  	  var filters = serialize.replace("requestedValue", $("#searchForm").find("#searchCategory").val());
  	  
  	  $.post(validateUrl, filters, function(data) {
  	    if (data.success) {
            window.location.href = url;
      	} else {
      	  if (data.result[0] === "No Data Found") {
      	    $("#noData").modal('show');
      	  }
      	}
  	  });
    }
});
</script>

<style>
    .row-fluid {
        display: inline;
        margin-right: 5px;
        width: auto !important;
    }
    .well select, .well .input {
        margin: 5px;
    }
</style>