package com.transretail.crm.web.stamp.controller.dto;


import java.lang.reflect.InvocationTargetException;
import java.util.Set;

import org.apache.commons.collections.CollectionUtils;
import org.hibernate.validator.constraints.NotEmpty;
import org.springframework.beans.BeanUtils;

import com.google.common.collect.Sets;
import com.transretail.crm.core.dto.StampPromoTxnDto;


public class StampPromoRedeemRequest extends StampPromoRequest {

    @NotEmpty
    private String storeCode;
    @NotEmpty
    private String transactionDate;
    @NotEmpty
    private String transactionId;
	private Set<StampPromoRedeemItem> redeemItems = Sets.newHashSet();



	public String getStoreCode() {
		return storeCode;
	}
	public void setStoreCode(String storeCode) {
		this.storeCode = storeCode;
	}
	public String getTransactionDate() {
		return transactionDate;
	}
	public void setTransactionDate(String transactionDate) {
		this.transactionDate = transactionDate;
	}
	public String getTransactionId() {
		return transactionId;
	}
	public void setTransactionId(String transactionId) {
		this.transactionId = transactionId;
	}
	public Set<StampPromoRedeemItem> getRedeemItems() {
		return redeemItems;
	}
	public void setRedeemItems(Set<StampPromoRedeemItem> redeemItems) {
		this.redeemItems = redeemItems;
	}

	public StampPromoRedeemRequest withRedeemItems( Set<StampPromoTxnDto> redeemItems ) throws IllegalAccessException, InvocationTargetException {
		if ( CollectionUtils.isNotEmpty( redeemItems ) ) {
			for ( StampPromoTxnDto dto : redeemItems ) {
				StampPromoRedeemItem item = new StampPromoRedeemItem();
				BeanUtils.copyProperties( dto, item );
				this.redeemItems.add( item );
			}
		}
		return this;
    }

	@Override
    public String getSignature() {
        return String.format( "%s%s%s", this.getMemberId(), this.getStoreCode(), this.getTransactionId() );
    }

}
