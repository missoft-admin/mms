alter table CRM_STORE_GROUP modify STORES varchar2(4000 char);
alter table CRM_PRODUCT_GRP modify PRODUCTS varchar2(4000 char);
alter table CRM_PROMOTION_PAYMENTTYPE modify WILDCARDS varchar2(4000 char);
alter table CRM_ADDRESS modify STREET varchar2(4000 char);