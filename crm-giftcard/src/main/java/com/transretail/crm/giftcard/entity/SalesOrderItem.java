package com.transretail.crm.giftcard.entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.google.common.base.Objects;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.hibernate.annotations.Type;

import com.mysema.query.annotations.QueryInit;
import com.transretail.crm.core.entity.support.CustomAuditableEntity;

/**
 *
 */
@Entity
@Table(name = "CRM_SALES_ORD_ITEM")
public class SalesOrderItem extends CustomAuditableEntity<Long> implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "SALES_ORDER", nullable = false)
	@QueryInit({"customer.*", "discount.*"})
	private SalesOrder order;
	
	@ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "PRODUCT")
	private ProductProfile product;
	
	@Column(name = "QUANTITY")
	private Long quantity;
	
	@Column(name = "FACE_AMOUNT")
	private BigDecimal faceAmount;
	
	@Column(name = "PRINT_FEE")
	private BigDecimal printFee;
	
	@Column(name = "FREE_PRNT_FEE")
    @Type(type = "yes_no")
	private Boolean freePrintFee;

    @OneToMany( mappedBy = "soItem", fetch = FetchType.LAZY, cascade = CascadeType.ALL )
	private Set<SalesOrderAlloc> soAlloc;

	public SalesOrder getOrder() {
		return order;
	}

	public void setOrder(SalesOrder order) {
		this.order = order;
	}

	public ProductProfile getProduct() {
		return product;
	}

	public void setProduct(ProductProfile product) {
		this.product = product;
	}

	public Long getQuantity() {
		return quantity;
	}

	public void setQuantity(Long quantity) {
		this.quantity = quantity;
	}

	public BigDecimal getFaceAmount() {
		return faceAmount;
	}

	public void setFaceAmount(BigDecimal faceAmount) {
		this.faceAmount = faceAmount;
	}

	public BigDecimal getPrintFee() {
		return printFee;
	}

	public void setPrintFee(BigDecimal printFee) {
		this.printFee = printFee;
	}

	public Set<SalesOrderAlloc> getSoAlloc() {
		return soAlloc;
	}

	public void setSoAlloc(Set<SalesOrderAlloc> soAlloc) {
		this.soAlloc = soAlloc;
	}

	public Boolean getFreePrintFee() {
		return freePrintFee;
	}

	public void setFreePrintFee(Boolean freePrintFee) {
		this.freePrintFee = freePrintFee;
	}

	@Override
	public String toString() {
		return Objects.toStringHelper(this)
				.add("product", product)
				.add("quantity", quantity)
				.add("faceAmount", faceAmount)
				.add("printFee", printFee)
				.add("freePrintFee", freePrintFee)
				.add("soAlloc", soAlloc)
				.toString();
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;

		if (o == null || getClass() != o.getClass()) return false;

		SalesOrderItem that = (SalesOrderItem) o;

		return new EqualsBuilder()
				.appendSuper(super.equals(o))
				.isEquals();
	}

	@Override
	public int hashCode() {

		int hashcode = 17;

		hashcode += (null == getId()) ? 0 : getId().hashCode() * 31;

		return hashcode;
	}
}
