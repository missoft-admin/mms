package com.transretail.crm.loyalty.dto;

import org.joda.time.DateTime;

import com.mysema.query.annotations.QueryProjection;

public class TransactionDto {
	private String startingSeries;
	private String endingSeries;
	private String location;
	private String transferTo;
	private DateTime transactionDate;
	private String referenceNo;
	
	public TransactionDto() {}
	
	public TransactionDto(String startingSeries, String endingSeries,
			String location, String transferTo, DateTime transactionDate, String referenceNo) {
		super();
		this.startingSeries = startingSeries;
		this.endingSeries = endingSeries;
		this.location = location;
		this.transferTo = transferTo;
		this.transactionDate = transactionDate;
		this.referenceNo = referenceNo;
	}



	public String getStartingSeries() {
		return startingSeries;
	}

	public void setStartingSeries(String startingSeries) {
		this.startingSeries = startingSeries;
	}

	public String getEndingSeries() {
		return endingSeries;
	}

	public void setEndingSeries(String endingSeries) {
		this.endingSeries = endingSeries;
	}

	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		this.location = location;
	}

	public String getTransferTo() {
		return transferTo;
	}

	public void setTransferTo(String transferTo) {
		this.transferTo = transferTo;
	}

	public DateTime getTransactionDate() {
		return transactionDate;
	}

	public void setTransactionDate(DateTime transactionDate) {
		this.transactionDate = transactionDate;
	}

	public String getReferenceNo() {
		return referenceNo;
	}

	public void setReferenceNo(String referenceNo) {
		this.referenceNo = referenceNo;
	}
	
	

}
