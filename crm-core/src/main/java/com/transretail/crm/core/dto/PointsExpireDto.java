package com.transretail.crm.core.dto;

import org.joda.time.LocalDate;

import com.transretail.crm.core.entity.MemberModel;

public class PointsExpireDto {
	private Double pointsToExpire;
	private Double pointsToDeduct;
	private LocalDate expiryDate;
	private MemberModel member;
	
	public PointsExpireDto() {}
	
	public PointsExpireDto(MemberModel member, Double pointsToExpire, Double pointsToDeduct) {
		this.member = member;
		this.pointsToExpire = pointsToExpire;
		this.pointsToDeduct = pointsToDeduct;
	}

	public Double getPointsToExpire() {
		return pointsToExpire;
	}

	public void setPointsToExpire(Double pointsToExpire) {
		this.pointsToExpire = pointsToExpire;
	}

	public Double getPointsToDeduct() {
		return pointsToDeduct;
	}

	public void setPointsToDeduct(Double pointsToDeduct) {
		this.pointsToDeduct = pointsToDeduct;
	}

	public LocalDate getExpiryDate() {
		return expiryDate;
	}

	public void setExpiryDate(LocalDate expiryDate) {
		this.expiryDate = expiryDate;
	}

	public MemberModel getMember() {
		return member;
	}

	public void setMember(MemberModel member) {
		this.member = member;
	}
}
