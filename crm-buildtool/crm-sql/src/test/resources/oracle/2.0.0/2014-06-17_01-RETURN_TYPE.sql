ALTER TABLE CRM_GC_REFUND ADD TYPE VARCHAR2(255);
ALTER TABLE CRM_GC_REFUND ADD RTN_REC_DTL number(19,0);

alter table CRM_GC_REFUND 
        add constraint FK_4gn2xb2b26o5e04vxnkvgwspc 
        foreign key (RTN_REC_DTL) 
        references CRM_GC_RETURN_DTL;
        
        
    create table CRM_GC_REPLACEMENT (
        id number(19,0) not null,
        CREATED_BY varchar2(255 char),
        CREATED_DATETIME timestamp,
        LAST_UPDATED_BY varchar2(255 char),
        LAST_UPDATED_DATETIME timestamp,
        ENDING_SERIES varchar2(255 char),
        STARTING_SERIES varchar2(255 char),
        REFUND_REC number(19,0) not null,
        primary key (id)
    );
    
    alter table CRM_GC_REPLACEMENT 
        add constraint FK_tmx69cqwkq6eom8a8u9ayjxxe 
        foreign key (REFUND_REC) 
        references CRM_GC_REFUND;