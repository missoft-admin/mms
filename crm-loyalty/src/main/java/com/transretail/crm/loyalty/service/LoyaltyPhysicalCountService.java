package com.transretail.crm.loyalty.service;


import java.util.List;

import org.joda.time.LocalDateTime;

import com.transretail.crm.common.reporting.jasper.JRProcessor;
import com.transretail.crm.common.service.dto.request.PageSortDto;
import com.transretail.crm.common.service.dto.response.ResultList;
import com.transretail.crm.loyalty.dto.LoyaltyPhysicalCountDto;
import com.transretail.crm.loyalty.dto.LoyaltyPhysicalCountSummaryDto;


public interface LoyaltyPhysicalCountService {

	void savePhysicalCount(LoyaltyPhysicalCountDto countDto);

	ResultList<LoyaltyPhysicalCountDto> listPhysicalCount(PageSortDto sortDto);

	ResultList<LoyaltyPhysicalCountSummaryDto> getSummary(PageSortDto sortDto);

	void processInventory();

	JRProcessor createJrProcessor();

	Boolean validateSeries(String startingSeries, String endingSeries);

	List<LocalDateTime> getSummaryDates();

	JRProcessor createJrProcessor(LocalDateTime date);
	
}
