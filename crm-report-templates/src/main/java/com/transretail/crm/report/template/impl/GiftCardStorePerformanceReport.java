package com.transretail.crm.report.template.impl;

import java.math.BigDecimal;
import java.math.MathContext;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import net.sf.jasperreports.engine.JRParameter;

import org.apache.commons.lang3.StringUtils;
import org.joda.time.LocalDate;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.context.support.MessageSourceResourceBundle;
import org.springframework.stereotype.Service;

import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.google.common.collect.Sets;
import com.mysema.query.BooleanBuilder;
import com.mysema.query.Tuple;
import com.mysema.query.group.Group;
import com.mysema.query.group.GroupBy;
import com.mysema.query.jpa.JPASubQuery;
import com.mysema.query.jpa.impl.JPAQuery;
import com.mysema.query.types.ConstructorExpression;
import com.mysema.query.types.Expression;
import com.mysema.query.types.expr.BooleanExpression;
import com.transretail.crm.common.reporting.jasper.ClasspathFileResolver;
import com.transretail.crm.common.reporting.jasper.DefaultJRProcessor;
import com.transretail.crm.common.reporting.jasper.JRProcessor;
import com.transretail.crm.common.util.DateUtil;
import com.transretail.crm.core.entity.QPeopleSoftStore;
import com.transretail.crm.core.entity.enums.ReportType;
import com.transretail.crm.core.entity.lookup.QLookupDetail;
import com.transretail.crm.core.entity.lookup.QStore;
import com.transretail.crm.core.entity.lookup.Store;
import com.transretail.crm.core.service.PsoftStoreService;
import com.transretail.crm.core.service.StoreService;
import com.transretail.crm.giftcard.entity.QGiftCardInventory;
import com.transretail.crm.giftcard.entity.QGiftCardTransactionItem;
import com.transretail.crm.giftcard.entity.QSalesOrder;
import com.transretail.crm.giftcard.entity.QSalesOrderItem;
import com.transretail.crm.giftcard.entity.QSalesOrderPaymentInfo;
import com.transretail.crm.giftcard.entity.support.GiftCardSaleTransaction;
import com.transretail.crm.giftcard.entity.support.GiftCardSalesType;
import com.transretail.crm.giftcard.entity.support.PaymentInfoStatus;
import com.transretail.crm.giftcard.entity.support.SalesOrderStatus;
import com.transretail.crm.giftcard.entity.support.SalesOrderType;
import com.transretail.crm.report.template.CommonReportFilter;
import com.transretail.crm.report.template.FilterField;
import com.transretail.crm.report.template.ReportDateFormatProvider;
import com.transretail.crm.report.template.ReportTemplate;

@Service("giftCardStorePerformanceReport")
public class GiftCardStorePerformanceReport implements ReportTemplate, ReportDateFormatProvider {
	protected static final String TEMPLATE_NAME = "Gift Card Store Performance Report";
    protected static final String REPORT_NAME = "reports/gift_card_store_performance_report.jasper";
    protected static final String FILTER_DATE_FROM = CommonReportFilter.DATE_FROM;
    protected static final String FILTER_DATE_TO = CommonReportFilter.DATE_TO;
    protected static final String BEGIN_DATE_A = "dateBeginA";
    protected static final String REGION = "region";
    protected static final String STORE = "store";
    protected static final String BEGIN_DATE_B = "dateBeginB";
    protected static final String END_DATE_A = "dateEndA";
    protected static final String END_DATE_B = "dateEndB";
    private static final BigDecimal HUNDRED = BigDecimal.valueOf(100L);
    private static final ColumnBean NULL = new ColumnBean(BigDecimal.ZERO, BigDecimal.ZERO);
    
    private static final DateTimeFormatter INPUT_DATE_PATTERN = DateTimeFormat.forPattern("dd-MM-yyyy");
    private static final DateTimeFormatter FULL_DATE_PATTERN = DateTimeFormat.forPattern("dd-MM-yyyy");
    
    private static final MathContext ROUNDING_CONTEXT = MathContext.DECIMAL32;
    
    @PersistenceContext
    private EntityManager em;
    @Autowired
    private MessageSource messageSource;
    @Autowired
    private PsoftStoreService psoftStoreService;
    @Autowired
    private StoreService storeService;
    
    private LocalDate parseDate(String dateInstance) {
    	return INPUT_DATE_PATTERN.parseLocalDate(dateInstance);
    }
    
    @Override
    public DateTimeFormatter getDefaultDateFormat() {
    	return FULL_DATE_PATTERN;
    }
    
    @Override
    public ReportType[] getReportTypes() {
    	return ReportType.values();
    }
    
    @Override
    public String getName() {
    	return TEMPLATE_NAME;
    }

    @Override
    public String getDescription() {
    	return messageSource.getMessage("gc.store.performance.report.label", (Object[]) null, LocaleContextHolder.getLocale());
    }
    
    @Override
    public boolean canView(Set<String> permissions) {
    	return true;
    }
    
    @Override
    public Set<FilterField> getFilters() {
        Set<FilterField> filters = Sets.newLinkedHashSet();
        Locale locale = LocaleContextHolder.getLocale();
        Object[] periodA = new Object[]{messageSource.getMessage("gc.store.performance.report.label.period.a", (Object[]) null, locale)};
        Object[] periodB = new Object[]{messageSource.getMessage("gc.store.performance.report.label.period.b", (Object[]) null, locale)};
        filters.add(FilterField.createDateField(BEGIN_DATE_A,
                messageSource.getMessage("gc.store.performance.report.label.begin.analysis", periodA, locale),
                DateUtil.SEARCH_DATE_FORMAT, true));
        filters.add(FilterField.createDateField(END_DATE_A,
                messageSource.getMessage("gc.store.performance.report.label.end.analysis", periodA, locale),
                DateUtil.SEARCH_DATE_FORMAT, true));
        filters.add(FilterField.createDateField(BEGIN_DATE_B,
                messageSource.getMessage("gc.store.performance.report.label.begin.analysis", periodB, locale),
                DateUtil.SEARCH_DATE_FORMAT, true));
        filters.add(FilterField.createDateField(END_DATE_B,
                messageSource.getMessage("gc.store.performance.report.label.end.analysis", periodB, locale),
                DateUtil.SEARCH_DATE_FORMAT, true));
        filters.add(FilterField.createDropdownField(REGION,
                messageSource.getMessage("gc.store.performance.report.label.region", null, locale),
                getRegions()));
        filters.add(FilterField.createDropdownField(STORE, 
                messageSource.getMessage("gc.store.performance.report.label.store", null, locale),
                getStoresOption()));
        
        Map<String, String> salesTypes = new LinkedHashMap<String, String>();
		salesTypes.put("", "");
		for (GiftCardSalesType key : GiftCardSalesType.values()) {
			salesTypes.put(key.toString(), key.toString());
		}
		filters.add(FilterField.createDropdownField("salesType",
                messageSource.getMessage("gc.store.performance.report.label.salestype", (Object[]) null, LocaleContextHolder.getLocale()), salesTypes));
        return filters;
    }
    
    private Map<String, String> getRegions() {
    	return ImmutableMap.<String, String>builder().put("", "")
                .putAll(psoftStoreService.getAllRegion()).build();
    }
    
    private Map<String, String> getStoresOption() {
	Map<String, String> stores = Maps.newLinkedHashMap();
        stores.put("", "");
        
	for (Store store : storeService.getAllStores()) {
	    stores.put(store.getCode(), store.getCodeAndName());
	}
        
        return stores;
    }
    
    @Override
    public JRProcessor createJrProcessor(Map<String, String> map) {
		LocalDate dateBeginA = parseDate(map.get(BEGIN_DATE_A));
		LocalDate dateBeginB = parseDate(map.get(BEGIN_DATE_B));
		LocalDate dateEndA = parseDate(map.get(END_DATE_A));
		LocalDate dateEndB = parseDate(map.get(END_DATE_B));
		String region = map.get(REGION);
        String store = map.get(STORE);
        GiftCardSalesType salesType = StringUtils.isNotBlank(map.get("salesType")) ? GiftCardSalesType.valueOf(map.get("salesType")) : null;
		DefaultJRProcessor jrProcessor = new DefaultJRProcessor(REPORT_NAME);
		Map<String, Object> paramMap = prepareReportParameters(dateBeginA, dateBeginB, dateEndA, dateEndB, region, store, salesType);
		paramMap.put("SUB_DATA_SOURCE", createDatasource(region, store, dateBeginA, dateBeginB, dateEndA, dateEndB, salesType));
		jrProcessor.addParameters(paramMap);
		jrProcessor.setFileResolver(new ClasspathFileResolver("reports"));
		return jrProcessor;
    }
    
    private Map<String, Object> prepareReportParameters(LocalDate dateBeginA, LocalDate dateBeginB,
    		LocalDate dateEndA, LocalDate dateEndB, String region, String store, GiftCardSalesType salesType) {
    	HashMap<String, Object> reportMap = Maps.newHashMap();

    	Locale locale = LocaleContextHolder.getLocale();
    	reportMap.put("DATE_BEGIN_A", dateBeginA.toString(FULL_DATE_PATTERN));
    	reportMap.put("DATE_BEGIN_B", dateBeginB.toString(FULL_DATE_PATTERN));
    	reportMap.put("DATE_END_A", dateEndA.toString(FULL_DATE_PATTERN));
    	reportMap.put("DATE_END_B", dateEndB.toString(FULL_DATE_PATTERN));
    	reportMap.put("REGION", StringUtils.isNotBlank(region) ? getRegions().get(region) : "");
    	reportMap.put("STORE", StringUtils.isNotBlank(store) ? storeService.getStoreByCode(store).getCodeAndName() : "");
    	reportMap.put("VARIANCE_PATTERN", "0.0%");
    	reportMap.put("VALUE_PATTERN", "#,##0");
    	reportMap.put("SALES_TYPE", salesType != null ? salesType.toString() : "");
    	Object[] periodA = new Object[]{messageSource.getMessage("gc.store.performance.report.label.period.a", (Object[]) null, locale)};
		Object[] periodB = new Object[]{messageSource.getMessage("gc.store.performance.report.label.period.b", (Object[]) null, locale)};
		reportMap.put("BEGIN_ANALYSIS_A", messageSource.getMessage("gc.store.performance.report.label.begin.analysis", periodA, locale));
		reportMap.put("BEGIN_ANALYSIS_B", messageSource.getMessage("gc.store.performance.report.label.begin.analysis", periodB, locale));
		reportMap.put("END_ANALYSIS_A", messageSource.getMessage("gc.store.performance.report.label.end.analysis", periodA, locale));
		reportMap.put("END_ANALYSIS_B", messageSource.getMessage("gc.store.performance.report.label.end.analysis", periodB, locale));
    	reportMap.put("REPORT_TYPE", messageSource.getMessage("gc.store.performance.report.label", (Object[]) null, locale));
    	reportMap.put("NO_DATA", messageSource.getMessage("report.no.data", (Object[]) null, locale));
    	reportMap.put(JRParameter.REPORT_RESOURCE_BUNDLE, new MessageSourceResourceBundle(messageSource, locale));
    	return reportMap;
    }
    
    public List<?> createDatasource(String region, String store,  LocalDate dateBeginA, LocalDate dateBeginB,
    		LocalDate dateEndA, LocalDate dateEndB, GiftCardSalesType salesType) {
    	QGiftCardInventory gcInv = QGiftCardInventory.giftCardInventory;
    	
    	QGiftCardTransactionItem gcTxnItem = QGiftCardTransactionItem.giftCardTransactionItem;
    	QSalesOrderPaymentInfo salesOrderPaymentInfo = QSalesOrderPaymentInfo.salesOrderPaymentInfo;
    	QStore qstore = QStore.store;
    	QSalesOrder qso = QSalesOrder.salesOrder;
    	
    	List<StorePerformanceBean> beanList = Lists.newArrayList();
    	
    	
    	BooleanExpression gcInvPeriodA = gcInv.activationDate.between(dateBeginA, dateEndA);
    	BooleanExpression gcInvPeriodB = gcInv.activationDate.between(dateBeginB, dateEndB);
        final JPAQuery storeQuery = new JPAQuery(em).from(qstore);

        if(!store.isEmpty()){
            storeQuery.where(qstore.code.eq(store));
        }
            
        Map<String, StorePerformanceBean> map = storeQuery.orderBy(qstore.code.asc())
    		.transform(GroupBy.groupBy(qstore.code).as(
    			ConstructorExpression.create(StorePerformanceBean.class, qstore)));
    	
        QPeopleSoftStore qps = QPeopleSoftStore.peopleSoftStore;
        
    	for(String storeCode : map.keySet()) {
    		StorePerformanceBean bean = map.get(storeCode);
                BooleanBuilder regionBuilder = new BooleanBuilder(gcInv.location.eq(storeCode));
                if(!region.isEmpty()){
                    regionBuilder.and(gcInv.location.in(new JPASubQuery().from(qps)
                            .where(qps.store.code.eq(storeCode)
                                    .and(qps.businessRegion.code.eq(region)))
                            .list(qps.store.code)));
                }
                
                BooleanExpression gcFilter = null;
                if(salesType != null)
                	gcFilter = gcInv.salesType.eq(salesType);
    		
                Tuple tupleA = new JPAQuery(em).from(gcInv).where(gcInvPeriodA.and(regionBuilder.getValue()).and(gcFilter))
                    .singleResult(gcInv.barcode.count(), gcInv.faceValue.sum());
    		Tuple tupleB = new JPAQuery(em).from(gcInv).where(gcInvPeriodB.and(regionBuilder.getValue()).and(gcFilter))
                    .singleResult(gcInv.barcode.count(), gcInv.faceValue.sum());
    		ColumnBean cardStoreValue = new ColumnBean(tupleA.get(gcInv.faceValue.sum()), tupleB.get(gcInv.faceValue.sum()));
    		ColumnBean cardQuantity = new ColumnBean(new BigDecimal(tupleA.get(gcInv.barcode.count())),
    			new BigDecimal(tupleB.get(gcInv.barcode.count())));
    		
                
    		ColumnBean cardCost = new ColumnBean(BigDecimal.ZERO, BigDecimal.ZERO);
            ColumnBean discountRate = new ColumnBean(BigDecimal.ZERO, BigDecimal.ZERO);
            ColumnBean discountAmount = new ColumnBean(BigDecimal.ZERO, BigDecimal.ZERO);
            ColumnBean shippingCosts = new ColumnBean(BigDecimal.ZERO, BigDecimal.ZERO);
            ColumnBean receivableAmount = new ColumnBean(BigDecimal.ZERO, BigDecimal.ZERO);
            
            //sales order
            if(salesType == null || salesType.compareTo(GiftCardSalesType.B2C) != 0) {
            	QSalesOrderItem salesOrderItem = QSalesOrderItem.salesOrderItem;
            	BooleanExpression soPeriodA = salesOrderItem.order.activationDate.between(dateBeginA, dateEndA);
            	BooleanExpression soPeriodB = salesOrderItem.order.activationDate.between(dateBeginB, dateEndB);
            	
            	BooleanExpression soFilter = null;
            	if(salesType != null)
            		soFilter = qso.orderType.in(salesType.getOrderTypes());
            	
            	BooleanBuilder salesStoreRegionBuilder = new BooleanBuilder(salesOrderItem.order.paymentStore.eq(storeCode));
                if(!region.isEmpty()){
                    salesStoreRegionBuilder.and(salesOrderItem.order.paymentStore
                            .in(new JPASubQuery().from(qps)
                            .where(qps.store.code.eq(storeCode)
                                    .and(qps.businessRegion.code.eq(region)))
                            .list(qps.store.code)));
                }
            	
        		tupleA = new JPAQuery(em).from(salesOrderItem)
        				.leftJoin(salesOrderItem.order, qso)
    	    		.where(BooleanExpression.allOf(soPeriodA, soFilter,
                                    salesOrderItem.order.status.eq(SalesOrderStatus.SOLD)
                                            .and(salesStoreRegionBuilder)))
    	    		.singleResult(salesOrderItem.printFee.sum(), salesOrderItem.faceAmount.sum());
        		tupleB = new JPAQuery(em).from(salesOrderItem)
        				.leftJoin(salesOrderItem.order, qso)
    	    		.where(BooleanExpression.allOf(soPeriodB, soFilter,
                                    salesOrderItem.order.status.eq(SalesOrderStatus.SOLD)
    	    			.and(salesStoreRegionBuilder)))
    	    		.singleResult(salesOrderItem.printFee.sum(), salesOrderItem.faceAmount.sum());
        		cardCost = new ColumnBean(tupleA.get(salesOrderItem.printFee.sum()), tupleB.get(salesOrderItem.printFee.sum()));
        		BigDecimal salesAmountA = tupleA.get(salesOrderItem.faceAmount.sum());
        		salesAmountA = salesAmountA != null ? salesAmountA : BigDecimal.ZERO;
        		BigDecimal salesAmountB = tupleB.get(salesOrderItem.faceAmount.sum());
        		salesAmountB = salesAmountB != null ? salesAmountB : BigDecimal.ZERO;
        		BigDecimal discountValA = new JPAQuery(em).from(salesOrderItem)
        				.leftJoin(salesOrderItem.order, qso)
    				.where(BooleanExpression.allOf(soPeriodA, soFilter, salesOrderItem.order.status.eq(SalesOrderStatus.SOLD)
    					.and(salesStoreRegionBuilder)))
    				.singleResult(salesOrderItem.faceAmount.multiply(salesOrderItem.order.discountVal.divide(HUNDRED)));
        		discountValA = discountValA == null ? BigDecimal.ZERO : discountValA;
        		BigDecimal discountValB = new JPAQuery(em).from(salesOrderItem)
        				.leftJoin(salesOrderItem.order, qso)
    				.where(BooleanExpression.allOf(soPeriodB, soFilter, salesOrderItem.order.status.eq(SalesOrderStatus.SOLD)
    					.and(salesStoreRegionBuilder)))
    				.singleResult(salesOrderItem.faceAmount.multiply(salesOrderItem.order.discountVal.divide(HUNDRED)));
        		discountValB = discountValB == null ? BigDecimal.ZERO : discountValB;
        		discountRate = new ColumnBean(
        			salesAmountA.compareTo(BigDecimal.ZERO) != 0 ? 
        				discountValA.divide(salesAmountA, ROUNDING_CONTEXT).multiply(HUNDRED) :
        				BigDecimal.ZERO, 
        			salesAmountB.compareTo(BigDecimal.ZERO) != 0 ? 
        				discountValB.divide(salesAmountB, ROUNDING_CONTEXT).multiply(HUNDRED) :
        				BigDecimal.ZERO, false);
        		discountAmount = new ColumnBean(discountValA, discountValB);
        		shippingCosts = new ColumnBean(BigDecimal.ZERO, BigDecimal.ZERO);    	
        		
        		
        		BooleanBuilder paymentStoreRegionBuilder = new BooleanBuilder(salesOrderPaymentInfo.order.paymentStore.eq(storeCode));
                if(!region.isEmpty()){
                    paymentStoreRegionBuilder.and(salesOrderPaymentInfo.order.paymentStore
                            .in(new JPASubQuery().from(qps)
                            .where(qps.store.code.eq(storeCode)
                                    .and(qps.businessRegion.code.eq(region)))
                            .list(qps.store.code)));
                }
        		
        		BigDecimal soPaymentA = new JPAQuery(em).from(salesOrderPaymentInfo)
        				.leftJoin(salesOrderPaymentInfo.order, qso)
    				.where(BooleanExpression.allOf(
    						soFilter,
    					salesOrderPaymentInfo.order.activationDate.between(dateBeginA, dateEndA),
    					salesOrderPaymentInfo.order.orderType.in(SalesOrderType.B2B_SALES, SalesOrderType.B2B_ADV_SALES),
    					salesOrderPaymentInfo.status.eq(PaymentInfoStatus.APPROVED)).
    					and(paymentStoreRegionBuilder.getValue()))
    				.singleResult(salesOrderPaymentInfo.paymentAmount.sum());
        		BigDecimal soPaymentB = new JPAQuery(em).from(salesOrderPaymentInfo)
        				.leftJoin(salesOrderPaymentInfo.order, qso)
    				.where(BooleanExpression.allOf(
    						soFilter,
    					salesOrderPaymentInfo.order.activationDate.between(dateBeginB, dateEndB),
    					salesOrderPaymentInfo.order.orderType.eq(SalesOrderType.B2B_SALES),
    					salesOrderPaymentInfo.status.eq(PaymentInfoStatus.APPROVED)).
    					and(paymentStoreRegionBuilder.getValue()))
    				.singleResult(salesOrderPaymentInfo.paymentAmount.sum());
        		BigDecimal recievableAmountA = salesAmountA.add(
        			soPaymentA != null ? soPaymentA : BigDecimal.ZERO);
        		BigDecimal recievableAmountB = salesAmountB.add(
        			soPaymentB != null ? soPaymentB : BigDecimal.ZERO);
        		receivableAmount = new ColumnBean(recievableAmountA, recievableAmountB);
            }
    		
    		
    		//end of salesorder
    		
                QLookupDetail qldtl = QLookupDetail.lookupDetail;
    		Double txnAmountA = new JPAQuery(em).from(gcTxnItem)
                        .join(gcTxnItem.transaction.peoplesoftStoreMapping, qps)
                        .join(qps.businessRegion, qldtl)
                        .join(gcTxnItem.giftCard, gcInv)
	    		.where(BooleanExpression.allOf(
	    				gcFilter,
        			gcTxnItem.transaction.transactionDate.between(dateBeginA.toDateTimeAtStartOfDay().toLocalDateTime(), 
        				dateEndA.plusDays(1).toDateTimeAtStartOfDay().toLocalDateTime())),
        			gcTxnItem.transaction.transactionType.eq(GiftCardSaleTransaction.REDEMPTION),
        			gcTxnItem.transaction.merchantId.eq(storeCode), qldtl.code.eq(region))
        		.singleResult(gcTxnItem.transactionAmount.sum());
    		Double txnAmountB = new JPAQuery(em).from(gcTxnItem)
                        .join(gcTxnItem.transaction.peoplesoftStoreMapping, qps)
                        .join(qps.businessRegion, qldtl)
                        .join(gcTxnItem.giftCard, gcInv)
	    		.where(BooleanExpression.allOf(
	    				gcFilter,
        			gcTxnItem.transaction.transactionDate.between(dateBeginB.toDateTimeAtStartOfDay().toLocalDateTime(), 
        				dateEndB.plusDays(1).toDateTimeAtStartOfDay().toLocalDateTime())),
        			gcTxnItem.transaction.transactionType.eq(GiftCardSaleTransaction.REDEMPTION),
        			gcTxnItem.transaction.merchantId.eq(storeCode), qldtl.code.eq(region))
        		.singleResult(gcTxnItem.transactionAmount.sum());
    		ColumnBean redeem = new ColumnBean(
    			txnAmountA != null ? new BigDecimal(txnAmountA) : BigDecimal.ZERO,
    			txnAmountB != null ? new BigDecimal(txnAmountB) : BigDecimal.ZERO);
    		
    		
    		bean.setCardCost(cardCost);
    		bean.setCardQuantity(cardQuantity);
    		bean.setCardStoreValue(cardStoreValue);
    		bean.setDiscountAmount(discountAmount);
    		bean.setDiscountPercentage(discountRate);
    		bean.setShippingCosts(shippingCosts);
    		bean.setRedeem(redeem);
    		bean.setReceivableAmount(receivableAmount);
    		beanList.add(bean);
    	}
    	return beanList;
    }
    
    private BigDecimal toBigDecimal(Map<String, Group> map, String key, Expression expr) {
    	if(!map.containsKey(expr)) {
    		return BigDecimal.ZERO;
    	}
    	
    	Object val = map.get(key).getOne(expr);
    	
    	if(val instanceof Long) {
    		return new BigDecimal((Long) val);
    	}
    	if(val instanceof Double) {
    		return new BigDecimal((Double) val);
    	}
    	if(val instanceof BigDecimal) {
    		return (BigDecimal) val;
    	}
    	return null;
    }
    
    public static class ColumnBean {
    	private BigDecimal valueA;
    	private BigDecimal valueB;
    	private BigDecimal variance;
    	
    	public ColumnBean() {
    		this.valueA = BigDecimal.ZERO;
    		this.valueB = BigDecimal.ZERO;
    	}
    	
    	public ColumnBean(BigDecimal valueA, BigDecimal valueB) {
    		this(valueA, valueB, true);
    	}
    	
    	public ColumnBean(BigDecimal valueA, BigDecimal valueB, boolean isInteger) {
    		if(valueA == null) {
    			valueA = BigDecimal.ZERO;
    		}
    		
    		if(valueB == null) {
    			valueB = BigDecimal.ZERO;
    		}
    		
    		if(isInteger) {
    			this.valueA = valueA;
        		this.valueB = valueB;
    		}
    		else {
    			this.valueA = valueA;
        		this.valueB = valueB;
    		}
    		
    		if(valueB.compareTo(BigDecimal.ZERO) != 0) {
                    this.variance = valueA.divide(valueB, ROUNDING_CONTEXT);
    		}
    		else {
    			this.variance = BigDecimal.ZERO;
    		}
    	}

		public BigDecimal getValueA() {
			return valueA;
		}

		public void setValueA(BigDecimal valueA) {
			this.valueA = valueA;
		}

		public BigDecimal getValueB() {
			return valueB;
		}

		public void setValueB(BigDecimal valueB) {
			this.valueB = valueB;
		}

		public BigDecimal getVariance() {
			return variance;
		}

		public void setVariance(BigDecimal variance) {
			this.variance = variance;
		}
    }
    
    public static class StorePerformanceBean {
    	private Store store;
    	private ColumnBean cardStoreValue;
    	private ColumnBean discountAmount;
    	private ColumnBean discountPercentage;
    	private ColumnBean shippingCosts;
    	private ColumnBean cardQuantity;
    	private ColumnBean cardCost;
    	private ColumnBean receivableAmount;
    	private ColumnBean redeem;
    	
    	public StorePerformanceBean() {
    		
    	}
    	
    	public StorePerformanceBean(Store store) {
    		this.store = store;
    	}

		public Store getStore() {
			return store;
		}

		public void setStore(Store store) {
			this.store = store;
		}

		public ColumnBean getCardStoreValue() {
			return cardStoreValue;
		}

		public void setCardStoreValue(ColumnBean cardStoreValue) {
			this.cardStoreValue = cardStoreValue;
		}

		public ColumnBean getDiscountAmount() {
			return discountAmount;
		}

		public void setDiscountAmount(ColumnBean discountAmount) {
			this.discountAmount = discountAmount;
		}

		public ColumnBean getDiscountPercentage() {
			return discountPercentage;
		}

		public void setDiscountPercentage(ColumnBean discountPercentage) {
			this.discountPercentage = discountPercentage;
		}

		public ColumnBean getShippingCosts() {
			return shippingCosts;
		}

		public void setShippingCosts(ColumnBean shippingCosts) {
			this.shippingCosts = shippingCosts;
		}

		public ColumnBean getCardQuantity() {
			return cardQuantity;
		}

		public void setCardQuantity(ColumnBean cardQuantity) {
			this.cardQuantity = cardQuantity;
		}

		public ColumnBean getCardCost() {
			return cardCost;
		}

		public void setCardCost(ColumnBean cardCost) {
			this.cardCost = cardCost;
		}

		public ColumnBean getReceivableAmount() {
			return receivableAmount;
		}

		public void setReceivableAmount(ColumnBean receivableAmount) {
			this.receivableAmount = receivableAmount;
		}

		public ColumnBean getRedeem() {
			return redeem;
		}

		public void setRedeem(ColumnBean redeem) {
			this.redeem = redeem;
		}
    }
}
