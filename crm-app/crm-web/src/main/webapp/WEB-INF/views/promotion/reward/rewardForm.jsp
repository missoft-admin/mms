<%@ include file="../../common/taglibs.jsp" %>



  <c:if test="${not empty rewardIdx}"><c:set var="deleteId" value="promotionRewardsEntry${rewardIdx}" /></c:if>
  <c:if test="${empty rewardIdx}"><c:set var="deleteId" value="promotionRewardsEntry0" /></c:if>
  <tr class="promotionRewardsEntry" id="${deleteId}">
    <c:choose>
      <c:when test="${not empty promotion.promotionRewards}"><form:hidden path="promotionRewards[${rewardIdx}].id" /></c:when>
      <c:otherwise><input type="hidden" name="promotionRewards[${rewardIdx}].id"/></c:otherwise>
    </c:choose>


    <c:if test="${rewardItemPoint}">
      <td>
        <c:choose>
          <c:when test="${not empty promotion.promotionRewards}"><form:input path="promotionRewards[${rewardIdx}].minQty" cssClass="input-medium wholeInput" /></c:when>
          <c:otherwise><input type="text" name="promotionRewards[${rewardIdx}].minQty"  class="input-medium wholeInput" /></c:otherwise>
        </c:choose>
      </td>
      <td>
        <c:choose>
          <c:when test="${not empty promotion.promotionRewards}"><form:input path="promotionRewards[${rewardIdx}].maxQty" cssClass="input-medium wholeInput" /></c:when>
          <c:otherwise><input type="text" name="promotionRewards[${rewardIdx}].maxQty"  class="input-medium wholeInput" /></c:otherwise>
        </c:choose>
      </td>
      <td>
        <c:choose>
          <c:when test="${not empty promotion.promotionRewards}"><form:input path="promotionRewards[${rewardIdx}].baseAmount" cssClass="input-medium wholeInput" /></c:when>
          <c:otherwise><input type="text" name="promotionRewards[${rewardIdx}].baseAmount" class="input-medium wholeInput" /></c:otherwise>
        </c:choose>
      </td>
      <td class="">
        <c:choose>
          <c:when test="${not empty promotion.promotionRewards}"><form:input path="promotionRewards[${rewardIdx}].baseFactor" cssClass="input-medium floatInput" /></c:when>
          <c:otherwise><input type="text" name="promotionRewards[${rewardIdx}].baseFactor"  class="input-medium floatInput" /></c:otherwise>
        </c:choose>
      </td>
    </c:if>

    <c:if test="${rewardPoints}">
	    <td>
	      <c:choose>
	        <c:when test="${not empty promotion.promotionRewards}"><form:input path="promotionRewards[${rewardIdx}].minAmount" cssClass="input-medium floatInput" /></c:when>
	        <c:otherwise><input type="text" name="promotionRewards[${rewardIdx}].minAmount"  class="input-medium floatInput" /></c:otherwise>
	      </c:choose>
	    </td>
	    <td>
	      <c:choose>
	        <c:when test="${not empty promotion.promotionRewards}"><form:input path="promotionRewards[${rewardIdx}].maxAmount" cssClass="input-medium floatInput" /></c:when>
	        <c:otherwise><input type="text" name="promotionRewards[${rewardIdx}].maxAmount"  class="input-medium floatInput" /></c:otherwise>
	      </c:choose>
	    </td>
      <td>
        <c:choose>
          <c:when test="${not empty promotion.promotionRewards}"><form:input path="promotionRewards[${rewardIdx}].baseAmount" cssClass="input-medium floatInput baseAmountInput" /></c:when>
          <c:otherwise><input type="text" name="promotionRewards[${rewardIdx}].baseAmount" class="input-medium floatInput baseAmountInput" /></c:otherwise>
        </c:choose>
      </td>
      <td class="">
        <c:choose>
          <c:when test="${not empty promotion.promotionRewards}"><form:input path="promotionRewards[${rewardIdx}].baseFactor" cssClass="input-medium intInput" /></c:when>
          <c:otherwise><input type="text" name="promotionRewards[${rewardIdx}].baseFactor"  class="input-medium floatInput" /></c:otherwise>
        </c:choose>
      </td>
      <td class="">
        <div class="checkbox">
        <c:choose>
          <c:when test="${not empty promotion.promotionRewards}"><form:checkbox cssClass="fixedCheckbox" path="promotionRewards[${rewardIdx}].fixed"/> <spring:message code="reward_prop_fixed" /></c:when>
          <c:otherwise><input type="checkbox" class="fixedCheckbox" name="promotionRewards[${rewardIdx}].fixed" /> <spring:message code="reward_prop_fixed" /></c:otherwise>
        </c:choose>
        </div>
      </td>
    </c:if>

    <c:if test="${rewardDiscount}">
	    <td>
	      <c:choose>
	        <c:when test="${not empty promotion.promotionRewards}"><form:input path="promotionRewards[${rewardIdx}].minAmount" cssClass="input-medium floatInput" /></c:when>
	        <c:otherwise><input type="text" name="promotionRewards[${rewardIdx}].minAmount"  class="input-medium floatInput" /></c:otherwise>
	      </c:choose>
	    </td>
	    <td>
	      <c:choose>
	        <c:when test="${not empty promotion.promotionRewards}"><form:input path="promotionRewards[${rewardIdx}].maxAmount" cssClass="input-medium floatInput" /></c:when>
	        <c:otherwise><input type="text" name="promotionRewards[${rewardIdx}].maxAmount"  class="input-medium floatInput" /></c:otherwise>
	      </c:choose>
	    </td>
      <td class="content_rewardDiscount">
        <c:choose>
          <c:when test="${not empty promotion.promotionRewards}"><form:input path="promotionRewards[${rewardIdx}].discount" cssClass="input-medium floatInput" /></c:when>
          <c:otherwise><input type="text" name="promotionRewards[${rewardIdx}].discount"  class="input-medium floatInput" /></c:otherwise>
        </c:choose>
      </td>
    </c:if>

    <c:if test="${rewardFreebie}">
      <td class="content_rewardFreebie"><td><input type="text" name="promotionRewards[${rewardIdx}].minQty"  class="input-medium floatInput" /></td>
      <td>
        <c:choose>
          <c:when test="${not empty promotion.promotionRewards}"><form:input path="promotionRewards[${rewardIdx}].maxQty" cssClass="input-medium floatInput" /></c:when>
          <c:otherwise><input type="text" name="promotionRewards[${rewardIdx}].maxQty"  class="input-medium floatInput"/></c:otherwise>
        </c:choose>
      </td>
      <td>
        <c:choose>
          <c:when test="${not empty promotion.promotionRewards}"><form:input path="promotionRewards[${rewardIdx}].minAmount" cssClass="input-medium floatInput" /></c:when>
          <c:otherwise><input type="text" name="promotionRewards[${rewardIdx}].minAmount"  class="input-medium floatInput" /></c:otherwise>
        </c:choose>
      </td>
      <td>
        <c:choose>
          <c:when test="${not empty promotion.promotionRewards}"><form:input path="promotionRewards[${rewardIdx}].maxAmount" cssClass="input-medium floatInput" /></c:when>
          <c:otherwise><input type="text" name="promotionRewards[${rewardIdx}].maxAmount"  class="input-medium floatInput" /></c:otherwise>
        </c:choose>
      </td>
      <td class="content_rewardFreebie">
        <c:choose>
          <c:when test="${not empty promotion.promotionRewards}"><form:input path="promotionRewards[${rewardIdx}].maxBonus" cssClass="input-medium floatInput" /></c:when>
          <c:otherwise><input type="text" name="promotionRewards[${rewardIdx}].maxBonus"  class="input-medium floatInput" /></c:otherwise>
        </c:choose>
      </td>
    </c:if>

    <c:if test="${rewardRedeemItem}">
      <td>
        <c:url var="url_getPrdName" value="/promotion/redemption/product/name/%PLUID%" />
        <c:choose>
          <c:when test="${not empty promotion.promotionRewards}">
            <form:input path="promotionRewards[${rewardIdx}].productCode" cssClass="input-medium redeemProductId" id="prdId${deleteId}" data-row-id="${deleteId}" data-url="${url_getPrdName}" />
          </c:when>
          <c:otherwise>
            <input type="text" name="promotionRewards[${rewardIdx}].productCode"  class="input-medium redeemProductId" id="prdId${deleteId}" data-row-id="${deleteId}" data-url="${url_getPrdName}" />
          </c:otherwise>
        </c:choose>
				<script type="text/javascript">
				$(document).ready( function() {
		        var url = '<c:url value="/product/classification/product/list/10/" />';
		        var $row = $( "#${deleteId}" );
		        TypeAhead.process( $( "#prdId${deleteId}" ), null, url, { value: "sku", label: "sku" }, 
		            [ "description", "sku" ], 
		            function( $item ) {
		        	$row.find( ".redeemProductName" ).html( $item[ "description" ] ); },
		            null//"sku"
		        );
		        /* $row.find( ".deleteRewardBtn" ).click( function(e) {
		            e.preventDefault();
		            $row.remove();
		        }); */
				});
				</script>
      </td>
      <td>
        <span class="redeemProductName">${promotion.promotionRewards[rewardIdx].productName}</span>
      </td>
      <td>
        <c:choose>
          <c:when test="${not empty promotion.promotionRewards}"><form:input path="promotionRewards[${rewardIdx}].baseFactor" cssClass="input-medium floatInput" /></c:when>
          <c:otherwise><input type="text" name="promotionRewards[${rewardIdx}].baseFactor"  class="input-medium floatInput" /></c:otherwise>
        </c:choose>
      </td>
    </c:if>
    
    <!-- STAMP -->
    <c:if test="${rewardSelType eq 'stampReward'}">
      <td>
        <c:choose>
          <c:when test="${not empty promotion.promotionRewards}"><form:input path="promotionRewards[${rewardIdx}].minAmount" cssClass="input-medium floatInput" /></c:when>
          <c:otherwise><input type="text" name="promotionRewards[${rewardIdx}].minAmount"  class="input-medium floatInput" /></c:otherwise>
        </c:choose>
      </td>
      <td>
        <c:choose>
          <c:when test="${not empty promotion.promotionRewards}"><form:input path="promotionRewards[${rewardIdx}].maxAmount" cssClass="input-medium floatInput" /></c:when>
          <c:otherwise><input type="text" name="promotionRewards[${rewardIdx}].maxAmount"  class="input-medium floatInput" /></c:otherwise>
        </c:choose>
      </td>
      <td>
        <c:choose>
          <c:when test="${not empty promotion.promotionRewards}"><form:input path="promotionRewards[${rewardIdx}].baseAmount" cssClass="input-medium floatInput baseAmountInput" /></c:when>
          <c:otherwise><input type="text" name="promotionRewards[${rewardIdx}].baseAmount" class="input-medium floatInput baseAmountInput" /></c:otherwise>
        </c:choose>
      </td>
      <td class="">
        <c:choose>
          <c:when test="${not empty promotion.promotionRewards}"><form:input path="promotionRewards[${rewardIdx}].baseFactor" cssClass="input-medium intInput" /></c:when>
          <c:otherwise><input type="text" name="promotionRewards[${rewardIdx}].baseFactor"  class="input-medium intInput" /></c:otherwise>
        </c:choose>
      </td>
      <td class="">
        <div class="checkbox">
        <c:choose>
          <c:when test="${not empty promotion.promotionRewards}"><form:checkbox cssClass="fixedCheckbox" path="promotionRewards[${rewardIdx}].fixed"/> <spring:message code="reward_prop_fixed" /></c:when>
          <c:otherwise><input type="checkbox" class="fixedCheckbox" name="promotionRewards[${rewardIdx}].fixed" /> <spring:message code="reward_prop_fixed" /></c:otherwise>
        </c:choose>
        </div>
      </td>
    </c:if>
    
    <c:if test="${rewardSelType eq 'stampRedemption'}">
      <td>
        <c:url var="url_getPrdName" value="/promotion/redemption/product/name/%PLUID%" />
        <c:choose>
          <c:when test="${not empty promotion.promotionRedemptions}">
            <form:input path="promotionRedemptions[${rewardIdx}].sku" cssClass="input-medium redeemProductId" id="prdId${deleteId}" data-row-id="${deleteId}" data-url="${url_getPrdName}" />
          </c:when>
          <c:otherwise>
            <input type="text" name="promotionRedemptions[${rewardIdx}].sku"  class="input-medium redeemProductId" id="prdId${deleteId}" data-row-id="${deleteId}" data-url="${url_getPrdName}" />
          </c:otherwise>
        </c:choose>
        
      </td>
      <td>
        <span class="redeemProductName">${promotion.promotionRedemptions[rewardIdx].productName}</span>
      </td>
      <td>
        <c:choose>
          <c:when test="${not empty promotion.promotionRedemptions}"><form:input path="promotionRedemptions[${rewardIdx}].qty" cssClass="input-medium intInput" /></c:when>
          <c:otherwise><input type="text" name="promotionRedemptions[${rewardIdx}].qty"  class="input-medium intInput" /></c:otherwise>
        </c:choose>
      </td>
      
      <td>
        <c:choose>
          <c:when test="${not empty promotion.promotionRedemptions}"><form:input path="promotionRedemptions[${rewardIdx}].redeemFrom" cssClass="input-medium" /></c:when>
          <c:otherwise><input type="text" name="promotionRedemptions[${rewardIdx}].redeemFrom"  class="input-medium" /></c:otherwise>
        </c:choose>
      </td>
      
      <td>
        <c:choose>
          <c:when test="${not empty promotion.promotionRedemptions}"><form:input path="promotionRedemptions[${rewardIdx}].redeemTo" cssClass="input-medium" /></c:when>
          <c:otherwise><input type="text" name="promotionRedemptions[${rewardIdx}].redeemTo"  class="input-medium" /></c:otherwise>
        </c:choose>
      </td>
      <script type="text/javascript">
        $(document).ready( function() {
            var url = '<c:url value="/product/classification/product/list/10/" />';
            var $row = $( "#${deleteId}" );
            TypeAhead.process( $( "#prdId${deleteId}" ), null, url, { value: "sku", label: "sku" }, 
                [ "description", "sku" ], 
                function( $item ) {
            	$row.find( ".redeemProductName" ).html( $item[ "description" ] ); },
                null//"sku"
            );
            /* $row.find( ".deleteRewardBtn" ).click( function(e) {
                e.preventDefault();
                $row.remove();
            }); */
            
            var $redeemFrom = $("input[name='promotionRedemptions[${rewardIdx}].redeemFrom']");
            var $redeemTo = $("input[name='promotionRedemptions[${rewardIdx}].redeemTo']");
            $redeemFrom.datepicker({
              autoclose : true,
          		format: 'dd-mm-yyyy'
          	}).on('changeDate', function(selected){
          		startDate = new Date(selected.date.valueOf());
                  startDate.setDate(startDate.getDate(new Date(selected.date.valueOf())));
                  $redeemTo.datepicker('setStartDate', startDate);
          	});
          	$redeemTo.datepicker({
          	  autoclose : true,
          		format: 'dd-mm-yyyy'
          	}).on('changeDate', function(selected){
          		FromEndDate = new Date(selected.date.valueOf());
                  FromEndDate.setDate(FromEndDate.getDate(new Date(selected.date.valueOf())));
                  $redeemFrom.datepicker('setEndDate', FromEndDate);
          	});
            
        });
        </script>
      
    
    </c:if>
    
    
    <!-- END OF STAMP -->



    <td>
    	<a href="#" data-delete-id="${deleteId}" class="btn pull-left tiptip icn-delete deleteRewardBtn" title="<spring:message code="delete" />"><spring:message code="delete" /></a>
        <c:if test="${isRedemptionPromo}"><input class="hidden" type="checkbox" checked name="promotionRewards[${rewardIdx}].fixed"  /></c:if>
    </td>
  </tr>


      <script type="text/javascript">
        $(document).ready( function() {
			$(".deleteRewardBtn").unbind("click").click(function(e) {
				e.preventDefault();
				$(this).closest(".promotionRewardsEntry").remove();
			});      
        });
        </script>
