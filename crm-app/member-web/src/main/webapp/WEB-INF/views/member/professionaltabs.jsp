<%@ include file="../taglibs.jsp"%>

  <li class="active"><a href="#businessinfo" data-toggle="tab"><spring:message
        code="business_information" /></a></li>
  <li><a href="#personalinfo" data-toggle="tab"><spring:message
        code="personal_information" /></a></li>
  <li><a href="#marketinginfo" data-toggle="tab"><spring:message
        code="marketing_information" /></a></li>
  <li><a href="#accountdata" data-toggle="tab"><spring:message
        code="membership_information" /></a></li>
  <li><a href="#additional" data-toggle="tab"><spring:message
        code="additional_parameter" /></a></li>
  <c:if test="${isPrimary}">
    <li><a href="#cards" data-toggle="tab"><spring:message code="supplementary_accts" /></a>
  </c:if>