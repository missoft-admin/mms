package com.transretail.crm.giftcard.entity.support;

/**
 * @author Allan G. Ramirez (agramirez@exist.com)
 */
public enum GiftCardInventoryStatus {

	/*VALID GiftCardInventoryStatus*/
	IN_STOCK,   /*received from supplier, transferred in, returned, Physical Count Found*/
	IN_TRANSIT, /*transferred out*/
	ACTIVATED,  /* B2C Sales, B2B Activation, Enable GC*/
    DISABLED,   /*Disable GC*/
    BURNED,     /*Burn GC*/
    MISSING,    /*Physical Count Lost*/
    PREALLOCATED, 
    ALLOCATED, 
    PREALLOCATED_EGC
}
