package com.transretail.crm.giftcard.entity;


import javax.persistence.Column;
import javax.persistence.Embeddable;

import org.hibernate.annotations.Type;


@Embeddable
public class EGiftCardInfo {

    @Column(name = "MOBILE_NO", length = 50)
    private String mobileNo;
    @Column(name = "EMAIL", length = 20)
    private String email;
    @Column(name = "PIN", length = 20)
    private String pin;
    @Column(name = "ALT_PIN", length = 20)
    private String altPin;

    @Column(name = "IS_EMAIL_SENT", length = 1)
    @Type(type = "yes_no")
    private Boolean isEmailSent;



	public String getMobileNo() {
		return mobileNo;
	}
	public void setMobileNo(String mobileNo) {
		this.mobileNo = mobileNo;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getPin() {
		return pin;
	}
	public void setPin(String pin) {
		this.pin = pin;
	}
	public String getAltPin() {
		return altPin;
	}
	public void setAltPin(String altPin) {
		this.altPin = altPin;
	}
	public Boolean getIsEmailSent() {
		return isEmailSent;
	}
	public void setIsEmailSent(Boolean isEmailSent) {
		this.isEmailSent = isEmailSent;
	}

}
