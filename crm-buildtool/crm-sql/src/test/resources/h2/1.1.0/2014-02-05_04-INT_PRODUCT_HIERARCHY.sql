    -- We don't have interface in H2 so we'll create the table instead
    create table PROFIT_PROD_HIER (
        OMDHMDHCD varchar(255) not null,
        OMDHEDESC varchar(255),
        OMDHMLVNO integer,
        OMDHLDESC varchar(255),
        OMDHPARCD varchar(255),
        primary key (OMDHMDHCD)
    );