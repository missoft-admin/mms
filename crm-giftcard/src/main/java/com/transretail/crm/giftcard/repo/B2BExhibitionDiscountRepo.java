package com.transretail.crm.giftcard.repo;

import org.springframework.stereotype.Repository;

import com.transretail.crm.common.repo.CrmQueryDslPredicateExecutor;
import com.transretail.crm.giftcard.entity.B2BExhibitionDiscount;

@Repository
public interface B2BExhibitionDiscountRepo extends CrmQueryDslPredicateExecutor<B2BExhibitionDiscount, Long> {

}
