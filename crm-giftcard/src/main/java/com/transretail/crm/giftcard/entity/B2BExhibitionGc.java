package com.transretail.crm.giftcard.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.transretail.crm.core.entity.support.CustomAuditableEntity;

/**
 *
 */
@Entity
@Table(name = "CRM_B2B_EXH_GC")
public class B2BExhibitionGc extends CustomAuditableEntity<Long> implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Column(name = "STARTING_SERIES")
	private String startingSeries;
	@Column(name = "ENDING_SERIES")
	private String endingSeries;
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "PRODUCT")
	private ProductProfile product;
	@ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "EXH", nullable = false)
	private B2BExhibition exh;
	public String getStartingSeries() {
		return startingSeries;
	}
	public void setStartingSeries(String startingSeries) {
		this.startingSeries = startingSeries;
	}
	public String getEndingSeries() {
		return endingSeries;
	}
	public void setEndingSeries(String endingSeries) {
		this.endingSeries = endingSeries;
	}
	public ProductProfile getProduct() {
		return product;
	}
	public void setProduct(ProductProfile product) {
		this.product = product;
	}
	public B2BExhibition getExh() {
		return exh;
	}
	public void setExh(B2BExhibition exh) {
		this.exh = exh;
	}
	
	
}
