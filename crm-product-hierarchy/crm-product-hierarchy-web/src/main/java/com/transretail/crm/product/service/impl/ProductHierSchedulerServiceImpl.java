package com.transretail.crm.product.service.impl;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.ScrollMode;
import org.hibernate.ScrollableResults;
import org.hibernate.StatelessSession;
import org.hibernate.engine.transaction.spi.TransactionContext;
import org.joda.time.DateTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.transretail.crm.core.entity.ProductGroup;
import com.transretail.crm.core.jpa.StatelessSessionFactoryBean;
import com.transretail.crm.product.entity.CrmProductHier;
import com.transretail.crm.product.entity.ProfitProductHier;
import com.transretail.crm.product.service.ProductHierSchedulerService;

/**
 * @author Allan G. Ramirez (agramirez@exist.com)
 */
@Service("productHierSchedulerServiceImpl")
public class ProductHierSchedulerServiceImpl implements ProductHierSchedulerService {
    private static final Logger _LOG = LoggerFactory.getLogger(ProductHierSchedulerServiceImpl.class);
    @Value("#{'${hibernate.jdbc.batch_size}'}")
    private int batchSize;
    @Autowired
    private StatelessSession statelessSession;

    @Transactional
    public void updateCrmProductHier() {
        DateTime currentDateTime = DateTime.now();
        saveOrUpdateCrmProductHier(statelessSession, currentDateTime);
        deleteOutdatedProductHier(statelessSession, currentDateTime);
    }

    private void saveOrUpdateCrmProductHier(final StatelessSession session, final DateTime currentDateTime) {
        // This won't work in h2
        ScrollableResults profitProductHiers = session.createQuery(
            "from ProfitProductHier p1 WHERE NOT EXISTS (SELECT p2 FROM ProfitProductHier p2 WHERE p1.created < p2.created)"
        ).scroll(ScrollMode.FORWARD_ONLY);
        try {
            int count = 0;
            int newCounter = 0;
            int updateCounter = 0;
            while (profitProductHiers.next()) {
                ProfitProductHier profitProductHier = (ProfitProductHier) profitProductHiers.get(0);
                CrmProductHier crmProductHier = (CrmProductHier) session.createQuery("from CrmProductHier where code = :code")
                    .setParameter("code", profitProductHier.getCode()).uniqueResult();
                if (crmProductHier == null) {
                    crmProductHier = new CrmProductHier();
                    copyValues(crmProductHier, profitProductHier);
                    crmProductHier.setCreated(currentDateTime);
                    session.insert(crmProductHier);
                    ++newCounter;
                } else {
                    copyValues(crmProductHier, profitProductHier);
                    crmProductHier.setLastUpdated(currentDateTime);
                    session.update(crmProductHier);
                    ++updateCounter;
                }

                ++count;
                if (count % batchSize == 0) {
                    ((TransactionContext) StatelessSessionFactoryBean.unwrap(session)).managedFlush();
                }
            }
            if (count % batchSize != 0) {
                ((TransactionContext) StatelessSessionFactoryBean.unwrap(session)).managedFlush();
            }
            _LOG.info("Total PROFIT product hierarchy records        : {}", count);
            _LOG.info("Total NEW CRM product hierarchy records       : {}", newCounter);
            _LOG.info("Total UPDATED CRM product hierarchy records   : {}", updateCounter);
        } finally {
            profitProductHiers.close();
        }
    }

    private void copyValues(CrmProductHier crmProductHier, ProfitProductHier profitProductHier) {
        crmProductHier.setCode(profitProductHier.getCode());
        crmProductHier.setEnglishDesc(profitProductHier.getEnglishDesc());
        crmProductHier.setLocalDesc(profitProductHier.getLocalDesc());
        crmProductHier.setLevel(profitProductHier.getLevel());
        crmProductHier.setParentLevel(profitProductHier.getParentLevel());
    }

    private void deleteOutdatedProductHier(final StatelessSession session, final DateTime currDt) {
        String fromClause = "FROM CrmProductHier WHERE " +
            "(lastUpdated IS NULL AND created < :currentDt) " +
            "OR (lastUpdated IS NOT NULL AND lastUpdated < :currentDt)";
        List<String> productCodes = session.createQuery("SELECT code " + fromClause).setParameter("currentDt", currDt).list();
        if (productCodes != null && productCodes.size() > 0) {
            updateRemovedProductsInProductGroup(session, productCodes);
            int total = session.createQuery("DELETE " + fromClause).setParameter("currentDt", currDt).executeUpdate();
            _LOG.info("Total DELETED CRM product hierarchy records   : {}", total);
        } else {
            _LOG.info("Total DELETED CRM product hierarchy records   : 0");
            _LOG.info("Total UPDATED product group records           : 0");
        }
    }

    private void updateRemovedProductsInProductGroup(final StatelessSession session, final List<String> productCodes) {
        StringBuilder queryBuilder = new StringBuilder("FROM ProductGroup WHERE ");
        queryBuilder.append("products LIKE ?");
        for (int i = 1; i < productCodes.size(); i++) {
            queryBuilder.append(" OR products LIKE ?");
        }
        Query query = session.createQuery(queryBuilder.toString());
        for (int i = 0; i < productCodes.size(); i++) {
            query = query.setString(i, "%" + productCodes.get(i) + "%");
        }

        ScrollableResults productGroups = query.scroll(ScrollMode.FORWARD_ONLY);
        try {
            int count = 0;
            while (productGroups.next()) {
                ProductGroup group = (ProductGroup) productGroups.get(0);
                String products = group.getProducts();
                for (int i = 0; i < productCodes.size(); i++) {
                    String code = productCodes.get(i);
                    products = removeProductCode(products, code);
                }
                group.setProducts(products);
                session.update(group);
                ++count;

                if (count % batchSize == 0) {
                    ((TransactionContext) StatelessSessionFactoryBean.unwrap(session)).managedFlush();
                }
            }
            if (count % batchSize != 0) {
                ((TransactionContext) StatelessSessionFactoryBean.unwrap(session)).managedFlush();
            }
            _LOG.info("Total UPDATED product group records           : {}", count);
        } finally {
            productGroups.close();
        }
    }

    private String removeProductCode(String haystack, String pin) {
        int idx = haystack.indexOf(pin);
        if (idx != -1) {
            if (idx == 0) {
                return haystack.substring(pin.length() + 1);
            } else {
                return haystack.replaceFirst("," + pin, "");
            }
        } else {
            return haystack;
        }
    }

}
