<%@ include file="../taglibs.jsp" %>
<spring:url value="/member/pin/update" var="actionUrl"/>

<div class="page-header page-header2">
    <h1><spring:message code="menu.pinupdate"/></h1>
</div>

<form:form modelAttribute="form" method="POST" action="${actionUrl}" cssClass="form-horizontal">
  <div class="control-group label label-info">
	<spring:message code="member.pin.update" />
  </div>
  <div class="control-group">
    <spring:message code="new_pin" var="pin" />
    <form:label path="pin" title="${pin}" cssClass="control-label" cssErrorClass="control-label error"><span class="required">*</span>${pin}</form:label>
    <div class="controls">
      <form:password maxlength="6" path="pin" title="${pin}" placeholder="${pin}"/>
    </div>
  </div>
  <div class="control-group">
    <spring:message code="confirm_pin" var="confirmPin" />
    <form:label path="confirmPin" title="${confirmPin}" cssClass="control-label" cssErrorClass="control-label error"><span class="required">*</span>${confirmPin}</form:label>
    <div class="controls">
      <form:password maxlength="6" path="confirmPin" title="${confirmPin}" placeholder="${confirmPin}"/>
    </div>
  </div>
  <div class="form-actions">
    <button type="submit" class="btn btn-primary"><spring:message code="button_submit" /></button>
    <a class="btn" href="<spring:url value="/" />"><spring:message code="button_cancel" /></a>
  </div>
</form:form>