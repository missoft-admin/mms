create table CRM_GC_PROD_PROFILE (
        id number(19,0) not null,
        CREATED_BY varchar2(255 char),
        CREATED_DATETIME timestamp,
        LAST_UPDATED_BY varchar2(255 char),
        LAST_UPDATED_DATETIME timestamp,
        ALLOW_PARTIAL_REDEEM char(1 char),
        ALLOW_RELOAD char(1 char),
        CARD_FEE double precision,
        EFFECTIVE_MONTHS number(19,0),
        FACE_VALUE varchar2(255 char),
        MAX_AMOUNT number(19,0),
        PRODUCT_CODE varchar2(255 char),
        PRODUCT_DESC varchar2(255 char),
        SAFETY_STOCKS_EA_MO number(19,0),
        UNIT_COST double precision,
        STATUS number(10,0),
        primary key (id)
    );