package com.transretail.crm.core.service;

import java.util.List;

import com.transretail.crm.common.service.dto.request.PageSortDto;
import com.transretail.crm.common.service.dto.response.ResultList;
import com.transretail.crm.core.dto.ApprovalMatrixDto;
import com.transretail.crm.core.entity.ApprovalMatrixModel;
import com.transretail.crm.core.security.model.impl.CustomSecurityUserDetailsImpl;

public interface HrOverridePermissionsService {

	List<ApprovalMatrixModel> findApprovers();
	
	void deleteApprover(Long id);

	boolean isApprover(CustomSecurityUserDetailsImpl currentUser);

	ResultList<ApprovalMatrixDto> listApprovers(PageSortDto dto);
}
