package com.transretail.crm.core.service;

import java.util.List;

import com.transretail.crm.common.service.dto.response.ResultList;
import com.transretail.crm.core.dto.PromoBannerDto;
import com.transretail.crm.core.dto.PromoBannerSearchDto;
import com.transretail.crm.core.entity.PromoBanner;
import com.transretail.crm.core.entity.enums.PromoImageType;

public interface PromoBannerService {
	PromoBannerDto save(PromoBanner promoBanner);
	PromoBannerDto save(PromoBannerDto promoBanner);
	PromoBannerDto findById(String id);
	ResultList<PromoBannerDto> searchPromoBanner(PromoBannerSearchDto dto);
	List<PromoBannerDto> findCurrentPromoBanners();
	List<PromoBannerDto> findActivePromoImagesByType(PromoImageType type);
	void delete(String id);
}
