package com.transretail.crm.template.service.streaming.excel.handler.sheet.optional;

import static org.apache.commons.lang3.StringUtils.isBlank;

import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.hibernate.engine.transaction.spi.TransactionContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.i18n.LocaleContextHolder;

import com.google.common.collect.Maps;
import com.mysema.query.jpa.impl.JPAQuery;
import com.mysema.query.jpa.impl.JPAUpdateClause;
import com.transretail.crm.core.entity.MemberModel;
import com.transretail.crm.core.entity.QMemberModel;
import com.transretail.crm.core.entity.embeddable.Address;
import com.transretail.crm.core.entity.embeddable.QAddress;
import com.transretail.crm.core.jpa.StatelessSessionFactoryBean;
import com.transretail.crm.core.repo.AddressRepo;
import com.transretail.crm.template.service.streaming.excel.handler.HandlerException;
import com.transretail.crm.template.service.streaming.excel.handler.cell.AccountIdCellHandler;
import com.transretail.crm.template.service.streaming.excel.handler.cell.AddressBldgCellHandler;
import com.transretail.crm.template.service.streaming.excel.handler.cell.AddressBlockCellHandler;
import com.transretail.crm.template.service.streaming.excel.handler.cell.AddressCityCellHandler;
import com.transretail.crm.template.service.streaming.excel.handler.cell.AddressDistrictCellHandler;
import com.transretail.crm.template.service.streaming.excel.handler.cell.AddressFloorCellHandler;
import com.transretail.crm.template.service.streaming.excel.handler.cell.AddressKmCellHandler;
import com.transretail.crm.template.service.streaming.excel.handler.cell.AddressPostCodeCellHandler;
import com.transretail.crm.template.service.streaming.excel.handler.cell.AddressRtCellHandler;
import com.transretail.crm.template.service.streaming.excel.handler.cell.AddressRwCellHandler;
import com.transretail.crm.template.service.streaming.excel.handler.cell.AddressStreetCellHandler;
import com.transretail.crm.template.service.streaming.excel.handler.cell.AddressStreetNumberCellHandler;
import com.transretail.crm.template.service.streaming.excel.handler.cell.AddressSubDistrictCellHandler;
import com.transretail.crm.template.service.streaming.excel.handler.cell.BusinessEmailCellHandler;
import com.transretail.crm.template.service.streaming.excel.handler.cell.BusinessPhoneCellHandler;
import com.transretail.crm.template.service.streaming.excel.handler.cell.BusinessSegmentationHandler;
import com.transretail.crm.template.service.streaming.excel.handler.cell.BusinessTypeHandler;
import com.transretail.crm.template.service.streaming.excel.handler.cell.CellHandler;
import com.transretail.crm.template.service.streaming.excel.handler.cell.PotentialBuyingCellHandler;
import com.transretail.crm.template.service.streaming.excel.handler.cell.RadiusHandler;
import com.transretail.crm.template.service.streaming.excel.handler.cell.ZoneHandler;
import com.transretail.crm.template.service.streaming.excel.handler.sheet.AbstractSheetHandler;

/**
 * @author Allan G. Ramirez (agramirez@exist.com)
 */
public class ProfCompanyDetailsHandler extends AbstractSheetHandler {
    private static final String HEADER_ACCOUNTID = "ACCOUNTNUMBER";
    private static final String HEADER_ZONE = "ZONE";
    private static final String HEADER_RADIUS = "RADIUS";
    private static final String HEADER_BUSINESS_ADDR_STREETNAME = "STREET";
    private static final String HEADER_BUSINESS_ADDR_STREETNUMBER = "NUMBER";
    private static final String HEADER_BUSINESS_ADDR_BLOCK = "BLOCK";
    private static final String HEADER_BUSINESS_ADDR_KM = "KM";
    private static final String HEADER_BUSINESS_ADDR_RT = "RT";
    private static final String HEADER_BUSINESS_ADDR_RW = "RW";
    private static final String HEADER_BUSINESS_ADDR_BLDG_NAME = "BUILDING";
    private static final String HEADER_BUSINESS_ADDR_BLDG_FLOOR = "FLOOR";
    private static final String HEADER_BUSINESS_ADDR_POSTCODE = "POSTCODE";
    private static final String HEADER_BUSINESS_ADDR_SUBDISTRICT = "SUB-DISTRICT";
    private static final String HEADER_BUSINESS_ADDR_DISTRICT = "DISTRICT";
    private static final String HEADER_BUSINESS_ADDR_CITY = "CITY";
    private static final String HEADER_BUSINESS_TYPE = "BUSINESSTYPE";
    private static final String HEADER_BUSINESS_SEGMENTATION = "BUSINESSSEGMENTATION";
    private static final String HEADER_POTENTIALBUYING = "POTENTIALBUYING";
    private static final String HEADER_BUSINESSPHONE = "BUSINESSPHONE";
    private static final String HEADER_BUSINESSEMAIL = "BUSINESSEMAIL";

    private Map<String, CellHandler> cellHandlerClassMap = Maps.newHashMap();
    private CellHandler[] cellHandlers = null;

    @Autowired
    private AddressRepo addressRepo;

    public ProfCompanyDetailsHandler() {
        cellHandlerClassMap.put(HEADER_ACCOUNTID, new AccountIdCellHandler());
        cellHandlerClassMap.put(HEADER_ZONE, new ZoneHandler());
        cellHandlerClassMap.put(HEADER_RADIUS, new RadiusHandler(this));
        cellHandlerClassMap.put(HEADER_BUSINESS_ADDR_STREETNAME, new AddressStreetCellHandler());
        cellHandlerClassMap.put(HEADER_BUSINESS_ADDR_STREETNUMBER, new AddressStreetNumberCellHandler());
        cellHandlerClassMap.put(HEADER_BUSINESS_ADDR_BLOCK, new AddressBlockCellHandler());
        cellHandlerClassMap.put(HEADER_BUSINESS_ADDR_KM, new AddressKmCellHandler());
        cellHandlerClassMap.put(HEADER_BUSINESS_ADDR_RT, new AddressRtCellHandler());
        cellHandlerClassMap.put(HEADER_BUSINESS_ADDR_RW, new AddressRwCellHandler());
        cellHandlerClassMap.put(HEADER_BUSINESS_ADDR_BLDG_NAME, new AddressBldgCellHandler());
        cellHandlerClassMap.put(HEADER_BUSINESS_ADDR_BLDG_FLOOR, new AddressFloorCellHandler());
        cellHandlerClassMap.put(HEADER_BUSINESS_ADDR_POSTCODE, new AddressPostCodeCellHandler());
        cellHandlerClassMap.put(HEADER_BUSINESS_ADDR_SUBDISTRICT, new AddressSubDistrictCellHandler());
        cellHandlerClassMap.put(HEADER_BUSINESS_ADDR_DISTRICT, new AddressDistrictCellHandler());
        cellHandlerClassMap.put(HEADER_BUSINESS_ADDR_CITY, new AddressCityCellHandler());
        cellHandlerClassMap.put(HEADER_BUSINESS_TYPE, new BusinessTypeHandler(this));
        cellHandlerClassMap.put(HEADER_BUSINESS_SEGMENTATION, new BusinessSegmentationHandler(this));
        cellHandlerClassMap.put(HEADER_POTENTIALBUYING, new PotentialBuyingCellHandler(this, HEADER_POTENTIALBUYING));
        cellHandlerClassMap.put(HEADER_BUSINESSPHONE, new BusinessPhoneCellHandler());
        cellHandlerClassMap.put(HEADER_BUSINESSEMAIL, new BusinessEmailCellHandler());
        cellHandlers = new CellHandler[cellHandlerClassMap.size()];
    }

    @Override
    protected void processHeaderCell(int i, String headerName) {
        CellHandler cellHandler = cellHandlerClassMap.get(headerName);
        if (cellHandler == null) {
            throw new HandlerException(getMessageSource()
                .getMessage("member.template.messages.excel.nohandler", new String[]{sheetName, headerName},
                    LocaleContextHolder.getLocale()));
        }
        cellHandlers[i] = cellHandler;
    }

    @Override
    protected CellHandler getCellHandler(int index) {
        return cellHandlers[index];
    }

    @Override
    protected void saveOrUpdateDto(MemberModel dto) throws HandlerException {
        String accountId = dto.getAccountId();
        if (isBlank(accountId)) {
            throw new HandlerException(getMessageSource().getMessage("member.template.messages.excel.columns.required",
                new String[]{sheetName, String.valueOf(currentRowNum), HEADER_ACCOUNTID},
                LocaleContextHolder.getLocale()));
        } else if (accountId.length() != 13 || accountId.charAt(2) != '0' || accountId.charAt(3) != '2') {
            throw new HandlerException(getMessageSource().getMessage("member.template.messages.excel.accountid.invalid",
                new String[]{sheetName, accountId, "02"},
                LocaleContextHolder.getLocale()));
        }
        if (!accountIdExists(dto.getAccountId())) {
            throw new HandlerException(getMessageSource().getMessage("member.template.messages.excel.accountid.notfound",
                new String[]{sheetName, String.valueOf(currentRowNum), dto.getAccountId()},
                LocaleContextHolder.getLocale()));
        }

        QMemberModel qMemberModel = QMemberModel.memberModel;
        Long addressId = saveOrUpdateAddress(dto.getAddress(), dto.getAccountId(), qMemberModel);

        new JPAUpdateClause(em, qMemberModel).where(qMemberModel.accountId.eq(dto.getAccountId()))
            .set(qMemberModel.professionalProfile.zone, dto.getProfessionalProfile().getZone())
            .set(qMemberModel.professionalProfile.radius, dto.getProfessionalProfile().getRadius())
            .set(qMemberModel.professionalProfile.customerGroup.code, dto.getProfessionalProfile().getCustomerGroup() != null ?
                dto.getProfessionalProfile().getCustomerGroup().getCode() : null)
            .set(qMemberModel.professionalProfile.customerSegmentation.code,
                dto.getProfessionalProfile().getCustomerSegmentation() != null ?
                dto.getProfessionalProfile().getCustomerSegmentation().getCode() : null)
            .set(qMemberModel.professionalProfile.potentialBuyingPerMonth, dto.getProfessionalProfile().getPotentialBuyingPerMonth())
            .set(qMemberModel.professionalProfile.businessPhone, dto.getProfessionalProfile().getBusinessPhone())
            .set(qMemberModel.professionalProfile.businessEmail, dto.getProfessionalProfile().getBusinessEmail())
            .set(qMemberModel.professionalProfile.businessAddress.id, addressId)
            .execute();

        if (currentRowNum % batchSize == 0) {
            cleanup();
        }
    }

    @Override
    public int getExpectedNumberOfColumns() {
        return cellHandlers.length;
    }

    private Long saveOrUpdateAddress(Address dto, String accountId, QMemberModel qMemberModel) {
        Long result = null;
        QAddress qAddress = new QAddress("businessAddress");
        Address address = new JPAQuery(em).from(qMemberModel)
            .rightJoin(qMemberModel.professionalProfile.businessAddress, qAddress)
            .where(qMemberModel.accountId.eq(accountId)).singleResult(
                qAddress);
        if (address == null) {
            if (StringUtils.isNotBlank(dto.getStreet()) ||
                dto.getStreetNumber() != null ||
                StringUtils.isNotBlank(dto.getBlock()) ||
                dto.getKm() != null ||
                StringUtils.isNotBlank(dto.getRt()) ||
                StringUtils.isNotBlank(dto.getRw()) ||
                StringUtils.isNotBlank(dto.getBuilding()) ||
                dto.getFloor() != null ||
                StringUtils.isNotBlank(dto.getPostCode()) ||
                StringUtils.isNotBlank(dto.getSubdistrict()) ||
                StringUtils.isNotBlank(dto.getDistrict()) ||
                StringUtils.isNotBlank(dto.getCity())) {

                result = addressRepo.saveAndFlush(dto).getId();
            }
        } else {
            address.setStreet(dto.getStreet());
            address.setStreetNumber(dto.getStreetNumber());
            address.setBlock(dto.getBlock());
            address.setKm(dto.getKm());
            address.setRt(dto.getRt());
            address.setRt(dto.getRt());
            address.setBuilding(dto.getBuilding());
            address.setFloor(dto.getFloor());
            address.setPostCode(dto.getPostCode());
            address.setSubdistrict(dto.getSubdistrict());
            address.setDistrict(dto.getDistrict());
            address.setCity(dto.getCity());
            result = addressRepo.saveAndFlush(address).getId();
        }
        return result;
    }

}
