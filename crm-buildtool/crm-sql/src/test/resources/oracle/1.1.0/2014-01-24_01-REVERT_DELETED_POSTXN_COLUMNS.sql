alter table POS_TRANSACTION ADD (
  CREATED_BY varchar(255 char),
	CREATED_DATETIME timestamp,
	LAST_UPDATED_BY varchar(255 char),
	LAST_UPDATED_DATETIME timestamp );