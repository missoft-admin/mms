<%-- 
    Document   : list
    Created on : May 5, 2014, 9:58:48 AM
    Author     : Monte Cillo Co (mco@exist.com)
--%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@include file="../../../common/taglibs.jsp" %>
<div id="content_giftcard_adjust_inventory">
  <div class="page-header page-header2">
    <h1><spring:message code="gc.adjust.inventory.page" /></h1>
  </div>
  <div class="btn-group">
    <button type="button" id="countStart" class="btn btn-primary"><spring:message code="label_start" /> - <spring:message code="physical_count_label" /></button>
    <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown">
      <span class="caret"></span>
      <span class="sr-only"></span>
    </button>
    <ul class="dropdown-menu" role="menu">
      <li><a id="byRange" href="#"><spring:message code="physical_count_by_series_range_label"/></a></li>
      <li><a id="byBarcode" href="#"><spring:message code="physical_count_by_barcode_only_label"/></a></li>
    </ul>
  </div>

  <button type="button" id="countEnd" class="btn btn-primary"><spring:message code="label_end" /> - <spring:message code="label_process" /></button>

  <c:if test="${not empty summaryDates}" >
    <div class="form-horizontal pull-right">
      <select name="summaryDate" class="input-medium" id="summaryDate">
	<c:forEach items="${summaryDates}" var="summaryDate">
	  <option value="${summaryDate}"><spring:eval expression="summaryDate" /></option>
	</c:forEach>
      </select>
         <div class="btn-group">
	<!--<button type="button" id="countStart" class="btn btn-print btn-print-small tiptip" title="Print"></button>-->
	<button type="button" class="btn btn-print btn-print-small dropdown-toggle tiptip" data-toggle="dropdown">
	  <span class="caret"></span>
	  <span class="sr-only"></span>
	</button>
	<ul class="dropdown-menu" role="menu">
	  <c:forEach items="${reportTypes}" var="reportType">
	    <li><a id="report-${reportType}" data-target="<c:url value='/giftcard/inventory/adjustment/print/${reportType.code}/'/>" href="#">
		<spring:message code="report_type_${reportType.code}" /></a></li>
	  </c:forEach>
	</ul>
      </div>

      <button type="button" id="showOlder" data-target="#summary-list-dialog" class="btn btn-primary">Show Older</button>
      <!--<button type="button" id="showOlder" class="btn btn-primary"><spring:message code='label_print'/></button>-->
    </div>
  </c:if>

  <div class="clearfix"></div>
  <br/>

  <div id="content_physicalCountList">
    <div id="list_physicalCount"></div>
  </div>
</div>

<link href="<spring:url value="/css/dataTables.css" />" rel="stylesheet">
<script src="<spring:url value="/js/jquery/jquery.dataTables.js" />" ></script>
<script src="<spring:url value="/js/jquery/jquery.dataTables.linkLengthMenu.js" />"></script>
<script src="<spring:url value="/js/jquery/jquery.dataTables.customPaging.js" />"></script>
<script src="<spring:url value="/js/jquery/ajax-datatable.js" />" ></script>
<script src="<spring:url value="/js/common/form2js.js" />" ></script>
<script src="<spring:url value="/js/common/json2.js" />" ></script>
<script src="<spring:url value="/js/jquery/jquery.toObject.js" />" ></script>
<script src="<spring:url value="/js/jquery/jquery.numberInput.js" />" ></script>
<link href="<c:url value="/styles/temp.css" />" rel="stylesheet" />
<script src='<c:url value="/js/bootstrap/bootstrap-datepicker.js"/>'></script>
<link href="<c:url value="/css/bootstrap/datepicker.css" />" rel="stylesheet" />

<script src="<spring:url value="/js/viewspecific/giftcard/adjust-inventory.js"/>"></script>
<jsp:include page="byrange.jsp" />
<jsp:include page="bybarcode.jsp" />
<jsp:include page="summary.jsp" />
<jsp:include page="summary-dialog.jsp" />

<script>
  $(document).ready(function() {
    $list = $("#list_physicalCount");
    $list.ajaxDataTable({
      'autoload': true,
      'ajaxSource': "<c:url value="/giftcard/inventory/adjustment/list" />",
      'aaSorting': [[0, "desc"]],
      'columnHeaders': [
	"<spring:message code='gc.inventory.product.profile.label' />",
	"<spring:message code='gc.inventory.seriesFrom' />",
	"<spring:message code='gc.inventory.seriesTo' />",
	"<spring:message code='gc.inventory.quantity.label' />"
      ],
      'modelFields': [
	{name: 'productProfile', sortable: false, fieldCellRenderer: function (data, type, row) {
	    return row.productProfile + " - " + row.productProfileName;
	  }
	},
	{name: 'startingSeries', sortable: false},
	{name: 'endingSeries', sortable: false},
	{name: 'quantity', sortable: false}
      ],
      'initCompleteCallback': function(dataTable) {
	hideProcessBtn();
      }
    });

  });

  function reloadTable() {
    $("#list_physicalCount").ajaxDataTable('search', new Object());
  }

  function hideProcessBtn() {
    if ($("#list_physicalCount").find("td:first").hasClass('dataTables_empty')) {
      $("#countEnd").hide();
    } else {
      $("#countEnd").show();
    }
  }

</script>