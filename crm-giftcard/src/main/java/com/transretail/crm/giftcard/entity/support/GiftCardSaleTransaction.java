package com.transretail.crm.giftcard.entity.support;

/**
 *
 * @author Monte Cillo Co (mco@exist.com)
 */
public enum GiftCardSaleTransaction {

    PRE_ACTIVATION,
    ACTIVATION,
    VOID_ACTIVATED,
    PRE_REDEMPTION,
    REDEMPTION,
    VOID_REDEMPTION,
    INQUIRY,
    RELOAD,
    VOID_RELOAD,
    
    //for pos lose transaction use only
    GIFT_TO_CUSTOMER
}
