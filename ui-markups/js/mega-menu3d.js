(function (f) {
    var i;
    var j = f(window).width();
    var e, l, o, p, d, m, r, n;
    var c, q, h, a, b, k;
    f.cssEase = {
        "default": "ease",
        "in": "ease-in",
        out: "ease-out",
        "in-out": "ease-in-out",
        snap: "cubic-bezier(0,1,.5,1)",
        easeOutCubic: "cubic-bezier(.215,.61,.355,1)",
        easeInOutCubic: "cubic-bezier(.645,.045,.355,1)",
        easeInCirc: "cubic-bezier(.6,.04,.98,.335)",
        easeOutCirc: "cubic-bezier(.075,.82,.165,1)",
        easeInOutCirc: "cubic-bezier(.785,.135,.15,.86)",
        easeInExpo: "cubic-bezier(.95,.05,.795,.035)",
        easeOutExpo: "cubic-bezier(.19,1,.22,1)",
        easeInOutExpo: "cubic-bezier(1,0,0,1)",
        easeInQuad: "cubic-bezier(.55,.085,.68,.53)",
        easeOutQuad: "cubic-bezier(.25,.46,.45,.94)",
        easeInOutQuad: "cubic-bezier(.455,.03,.515,.955)",
        easeInQuart: "cubic-bezier(.895,.03,.685,.22)",
        easeOutQuart: "cubic-bezier(.165,.84,.44,1)",
        easeInOutQuart: "cubic-bezier(.77,0,.175,1)",
        easeInQuint: "cubic-bezier(.755,.05,.855,.06)",
        easeOutQuint: "cubic-bezier(.23,1,.32,1)",
        easeInOutQuint: "cubic-bezier(.86,0,.07,1)",
        easeInSine: "cubic-bezier(.47,0,.745,.715)",
        easeOutSine: "cubic-bezier(.39,.575,.565,1)",
        easeInOutSine: "cubic-bezier(.445,.05,.55,.95)",
        easeInBack: "cubic-bezier(.6,-.28,.735,.045)",
        easeOutBack: "cubic-bezier(.175, .885,.32,1.275)",
        easeInOutBack: "cubic-bezier(.68,-.55,.265,1.55)"
    };
    f.fn.menu3d = function (s) {
        i = f(this);
        var t = f.extend({}, f.fn.menu3d.defaults, s);
        t.easing = f.cssEase[t.easing];
        e = t.responsive;
        l = t.clickable;
        o = t.skin;
        p = t.speed;
        d = t.vertical;
        m = t.easing;
        r = t.hoverIn;
        n = t.hoverOut;
        if (l) {
            f(document).click(function (v) {
                var u = f(v.target).parents();
                if (!f(u).hasClass("open")) {
                    f.fn.menu3d.closeDropdown()
                }
            })
        }
        if (t.responsive) {
            i.addClass("responsive")
        } else {
            i.removeClass("responsive")
        }
        f.fn.menu3d.init(i);
        f.fn.menu3d.setVertical(i, t.vertical, j);
        i.removeClass(function (u, v) {
            return (v.match(/\bskin-\S+/g) || []).join(" ")
        });
        i.addClass(t.skin);
        c = i.outerHeight();
        q = i.outerWidth();
        h = i.offset().top;
        a = i.offset().left;
        b = a + q;
        k = h + c;
        i.find("li > ul.sub").parent().prepend('<span class="arrow-icon"></span>');
        if (true) {
            i.find("> ul > li:not(.menu-non-dropdown):not(.no-link)").find(">a,>span").click(function () {
                var u = f(this).parent();
                var v = f(u).hasClass("open");
                if (!v) {
                    f.fn.menu3d.closeDropdown(u);
                    f.fn.menu3d.hoverIn(u);
                    f(u).addClass("open");
                    return false
                } else {
                    f.fn.menu3d.closeDropdown();
                    return false
                }
            });
            i.find("li > ul.sub").parent().click(function () {
                var u = f(this);
                var v = f(u).hasClass("open");
                var w = f(u).find("> ul");
                f(w).click(function () {
                    v = false;
                    f(u).removeClass("open")
                });
                if (v) {
                    f.fn.menu3d.hoverOutSub(this);
                    f(u).removeClass("open");
                    f(u).find("ul.dropdown-menu").hide()
                } else {
                    f.fn.menu3d.hoverInSub(this);
                    f(u).addClass("open")
                }
            })
        } else {
            i.find("li > ul.sub").parent().hover(function () {
                f.fn.menu3d.hoverInSub(this)
            }, function () {
                f.fn.menu3d.hoverOutSub(this)
            });
            i.find("> ul > li:not(.menu-non-dropdown):not(.no-link)").hover(function () {
                f.fn.menu3d.hoverIn(this)
            }, function () {
                f.fn.menu3d.hoverOut(this)
            })
        }
    };
    f.fn.menu3d.init = function (s) {
        i = f(s);
        i.prepend('<div class="menuToggle">Menu <span class="megaMenuToggle-icon"></span></div>');
        i.find(".txtSearch").focus(function () {
            f(this).val("")
        }).blur(function () {
            f(this).val("Search...")
        });
        i.find("> ul > li:not(.menu-non-dropdown):not(.no-link):last").addClass("last");
        i.find("> ul > li:first").addClass("first");
        i.find(".menuToggle").click(function () {
            i.find("> ul").toggle()
        })
    };
    f.fn.menu3d.closeDropdown = function (s) {
        var t = i.find(" > ul > .open");
        if (s) {
            t = i.find(" > ul > .open").not(s)
        }
        f(t).removeClass("open");
        f.fn.menu3d.hoverOut(t);
        f(t).find(".open").removeClass("open");
        f(t).find("ul.dropdown-menu").hide()
    };
    f.fn.menu3d.hoverIn = function (C) {
        var w = f(C).find("> div:not(.movingout)");
        var y = r;
        var A = n;
        if (f(w).attr("animate-in") != undefined && f(w).attr("animate-in").length != 0) {
            y = f(w).attr("animate-in")
        }
        if (f(w).attr("animate-out") != undefined && f(w).attr("animate-out").length != 0) {
            A = f(w).attr("animate-in")
        }
        if (y == "slideDown") {
            f(w).css("z-index", 100).removeClass(A).slideDown(p / 2).addClass(y)
        } else {
            f(w).css("z-index", 100).show().removeClass(A).css({
                "animation-fill-mode": "both",
                "animation-duration": p + "ms",
                "animation-timing-function": m
            }).addClass(y);
            var t = f(C).find("> .dropdown-menu");
            var u = f(t).outerWidth();
            var D = f(t).outerHeight();
            var v = f(t).parent().offset().left;
            var B = v + u;
            var z = f(t).parent().offset().top;
            var s = z + D;
            f(t).css("left", "");
            if (j > 768) {
                if (d) {
                    if (s >= k) {
                        var x = s - k + 1;
                        if (D > c) {
                            f(C).css("position", "static");
                            f(t).css("top", "0px")
                        } else {
                            f(t).css("top", -x)
                        }
                    }
                } else {
                    if (B >= b) {
                        f(t).css("right", "-1px")
                    }
                }
            }
        }
    };
    f.fn.menu3d.hoverOut = function (A) {
        if (f(A).length > 0) {
            var t = f(A).find("> div");
            var x = r;
            var y = n;
            if (f(t).attr("animate-in") != undefined && f(t).attr("animate-in").length != 0) {
                x = f(t).attr("animate-in")
            }
            if (f(t).attr("animate-out") != undefined && f(t).attr("animate-out").length != 0) {
                y = f(t).attr("animate-out")
            }
            var w = f(A).find("> div." + x);
            if (y == "none") {
                f(w).removeClass(x).hide((p / 2))
            } else {
                if (y == "slideUp") {
                    f(w).removeClass(x).slideUp((p / 2))
                } else {
                    f(w).css("z-index", 1).removeClass(x).css({
                        "animation-fill-mode": "both",
                        "animation-duration": (p / 2) + "ms",
                        "animation-timing-function": m
                    }).addClass(y + " movingout")
                }
            }
            g(w, y, p);
            var s = f(A).find(".dropdown-menu");
            var v = f(s).offset().left;
            var u = f(s).outerWidth();
            var z = v + u;
            if (j > 768) {
                if (z >= j) {
                    f(s).css("left", "-1px")
                }
            }
            f(s).parent().width("");
            f(s).width("")
        }
    };
    f.fn.menu3d.hoverInSub = function (s) {
        f(s).find("> ul").stop(true, true).slideDown(100);
        var v = f(s).find("> ul");
        var x = f(s).offset().left;
        var u = f(s).outerWidth();
        var t = x + u + u + 80;
        var w = f(window).width();
        if (w > 768) {
            if (t > w) {
                f(v).css("left", "auto");
                f(v).css("right", "100%")
            } else {
                f(v).css("left", "100%");
                f(v).css("right", "auto")
            }
        }
    };
    f.fn.menu3d.hoverOutSub = function (s) {
        f(s).find("> ul").stop(false, false).slideUp(100)
    };
    f.fn.menu3d.setVertical = function (u, s, t) {
        i = f(u);
        if (s && t > 768) {
            i.addClass("vertical");
            i.find("> ul > li").css({
                "float": "none",
                display: "block"
            })
        } else {
            i.removeClass("vertical")
        }
    };
    f.fn.menu3d.defaults = {
        responsive: true,
        clickable: true,
        skin: "skin-black",
        speed: 600,
        vertical: false,
        easing: "default",
        hoverIn: "flipInX",
        hoverOut: "slideUp"
    };

    function g(v, t, u) {
        setTimeout(function () {
            f(v).removeClass("movingout " + t).hide()
        }, u / 2)
    }
})(jQuery);