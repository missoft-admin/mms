package com.transretail.crm.core.service.impl;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.transretail.crm.common.enums.MessageType;
import com.transretail.crm.common.service.dto.rest.ReturnMessage;
import com.transretail.crm.core.entity.ApplicationConfig;
import com.transretail.crm.core.entity.enums.AppConfigDefaults;
import com.transretail.crm.core.entity.enums.AppKey;
import com.transretail.crm.core.repo.ApplicationConfigRepo;
import com.transretail.crm.core.service.RestUrlService;
import org.apache.commons.beanutils.BeanUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.lang.reflect.InvocationTargetException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.SocketTimeoutException;
import java.net.URL;

@Service("restUrlService")
public class RestUrlServiceImpl implements RestUrlService {


    private static final Logger LOGGER = LoggerFactory.getLogger(RestUrlServiceImpl.class);

    @Autowired
    private ApplicationConfigRepo appConfigRepo;

    @Override
    public String getPrimaryWsUrl() {

        ApplicationConfig config = appConfigRepo.findByKey(AppKey.PRIMARY_WS_URL);

        //if value of PRIMARY_WS_URL is LOCAL then ws calls will refer to proxy ws

        if (config != null) {
            return config.getValue();
        } else {
            return AppConfigDefaults.DEFAULT_PRIMARY_WS_URL;
        }


    }
    
    @Override
    public String getPrimaryWsUrlForStores() {

        ApplicationConfig config = appConfigRepo.findByKey(AppKey.PRIMARY_WS_URL_FOR_STORES);

        //if value of PRIMARY_WS_URL is LOCAL then ws calls will refer to proxy ws

        if (config != null) {
            return config.getValue();
        } else {
            return AppConfigDefaults.DEFAULT_PRIMARY_WS_URL;
        }


    }
    
    @Override
    public boolean callPrimaryWsUrl(String wsMapping, String urlParameters, boolean httpGet,  ReturnMessage ret, String serverIp, String primaryUrl) {
    	LOGGER.info("[ CALL_WS_PRIMARY_URL][INIT] wsMapping: " + wsMapping + "  urlParameters: " + urlParameters + "  httpGet: " + httpGet + " primaryURL: " + primaryUrl);

        if (primaryUrl.contains(serverIp) || primaryUrl.trim().equalsIgnoreCase("")) {
            //if calling server is same with DEFAULT_PRIMARY_WS_URL then no need for http call
            return false;
        }



        String urlStr = "";

        try {

            urlStr = composeUrl(primaryUrl, wsMapping, urlParameters);


            //urlStr = "http://localhost:8080/crm-proxy/api/member/find/id/1";
            URL url = new URL(urlStr);
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();


            ApplicationConfig config = appConfigRepo.findByKey(AppKey.PRIMARY_WS_SOCKET_TIMEOUT);

            int timeout = Integer.parseInt(AppConfigDefaults.DEFAULT_PRIMARY_WS_SOCKET_TIMEOUT);
            if (config != null) {
                try
                {
                    timeout = Integer.parseInt(config.getValue());
                }
                catch(NumberFormatException nfe) {
                  //use default value}
                }
            }


            ApplicationConfig readTimeOutConfig = appConfigRepo.findByKey(AppKey.PRIMARY_WS_READ_TIMEOUT);

            int readTimeout = Integer.parseInt(AppConfigDefaults.DEFAULT_PRIMARY_WS_READ_TIMEOUT);
            if (readTimeOutConfig != null) {
                try
                {
                    readTimeout = Integer.parseInt(readTimeOutConfig.getValue());
                }
                catch(NumberFormatException nfe) {
                    //use default value}
                }
            }

            conn.setConnectTimeout(timeout); //set timeout to 2 seconds
            conn.setReadTimeout(readTimeout);
            if (httpGet) {
                conn.setRequestMethod("GET");

            } else {
                conn.setRequestMethod("POST");

            }
            conn.setRequestProperty("Accept", "application/json");



            if (conn.getResponseCode() != 200) {

                LOGGER.info("[ PRIMARY_WS][ERROR][HTTP ERROR] responseCode=  " + conn.getResponseCode() + "  urlStr: " + urlStr);


                return false;
            }

            BufferedReader br = new BufferedReader(new InputStreamReader((conn.getInputStream())));
            ObjectMapper mapper = new ObjectMapper();

            ReturnMessage returnMessage = mapper.readValue(br, ReturnMessage.class);

            if (isDbError(returnMessage))  {
                return false;
            }

            try {
                BeanUtils.copyProperties(ret, returnMessage);
            } catch (IllegalAccessException e) {
                LOGGER.error("[ PRIMARY_WS][ERROR][COPYING BEAN] message=  " + e);
            } catch (InvocationTargetException e) {
                LOGGER.error("[ PRIMARY_WS][ERROR][COPYING BEAN2] message=  " + e);
            }
            conn.disconnect();




            return true;
        } catch (MalformedURLException e) {
            //e.printStackTrace();
            LOGGER.info("[ PRIMARY_WS][ERROR][MalformedURLException] ERROR_MESSAGE=  " + e.getMessage()+ "  urlStr: " + urlStr);

        } catch (SocketTimeoutException e) {
            LOGGER.info("[ PRIMARY_WS][ERROR][SocketTimeoutException] ERROR_MESSAGE=  " + e.getMessage()+ "  urlStr: " + urlStr);

        } catch (IOException e) {
            //e.printStackTrace();
            LOGGER.info("[ PRIMARY_WS][ERROR][IOException] ERROR_MESSAGE=  " + e.getMessage()+ "  urlStr: " + urlStr);

        }


        LOGGER.info("[ PRIMARY_WS][ERROR][PROXY CALL] Reverting to  proxy server ws call. Primary not available : {}", urlStr);
        return false;
    }

    @Override
    public boolean callPrimaryWsUrl(String wsMapping, String urlParameters, boolean httpGet,  ReturnMessage ret, String serverIp) {
        String primaryUrl= getPrimaryWsUrl();
        return callPrimaryWsUrl(wsMapping, urlParameters, httpGet, ret, serverIp, primaryUrl);
    }
    
    @Override
    public boolean redirectToPrimary(String wsMapping, String urlParameters, boolean httpGet,  ReturnMessage ret, String serverIp) {
        String primaryUrl= getPrimaryWsUrlForStores();
        return callPrimaryWsUrl(wsMapping, urlParameters, httpGet, ret, serverIp, primaryUrl);
    }

    private boolean isDbError(ReturnMessage ret) {
        if (ret != null) {
            //if DB error revert to store db
            if (ret.getType().equals(MessageType.ERROR) && ret.getMessage() != null )      {

                if (ret.getMessage().contains("ORA-") || ret.getMessage().contains("The Network Adapter could not " +
                        "establish the connection") || ret.getMessage().contains("DSRA0010E: SQL State") || ret.getMessage().contains("IO Error:")) {
                    LOGGER.info("[ PRIMARY_WS][ERROR][PRIMARY DB ERROR] Reverting to  proxy server ws call. Primary " +
                            "not available : {}", ret.getMessage());

                    return true;
                }


            } else {
                return false;
            }
        }
        return false;
    }


    private String composeUrl(String primary, String mapping, String params) {

        StringBuffer url = new StringBuffer(getPrimaryWsUrl());

        if (!url.toString().endsWith("/")) {
            url.append("/");
        }
        url.append(mapping);

        if (!params.startsWith("/")) {
            url.append("/");
        }
        url.append(params);

        return url.toString();
    }


}
