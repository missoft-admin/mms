<%@include file="../../common/taglibs.jsp" %>


<div id="content_cxProfile">

  <div class="page-header page-header2"><h1><spring:message code="gc.cxprofile.page" /></h1></div>

  <div>
    <div id="form_cxProfileSearch" class="well">
      <div class="search-block form-inline form-search">
        <label class="label-single">
          <h5><spring:message code="entity_search" />:</h5>
        </label>
        <select id="searchField" class="">
          <option value="${categ.key}"><spring:message code="label_searchfield" /></option>
          <c:forEach var="item" items="${searchFields}"><option value="${item}"><spring:message code="gc.cxprofile.sr.${item}" /></option></c:forEach>
        </select>
        <input type="text" id="searchValue" class="input" placeholder="<spring:message code="label_searchvalue" />" class="" />
        <input id="searchBtn" class="btn btn-primary mb10" type="submit" value="<spring:message code="label_search" />" />
      </div>
      <div class="filter-block form-inline form-search">
        <label class="label-single">
        </label>
        <select name="customerType" class="customerType">
          <option value="${categ.key}"><spring:message code="gc.cxprofile.cx.type" /></option>
          <c:forEach var="item" items="${customerTypes}"><option value="${item}">${item}</option></c:forEach>
        </select>
        <select name="discountType" class="discountType">
          <option value="${categ.key}"><spring:message code="gc.cxprofile.discount.type" /></option>
          <c:forEach var="item" items="${discountTypes}"><option value="${item.code}">${item.description}</option></c:forEach>
        </select>
        <input id="filterBtn" class="btn btn-primary mb10" type="submit" value="<spring:message code="label_filter" />" />
        <input id="resetBtn" type="button" class="btn mb10" value="<spring:message code="label_reset" />" />
      </div>
    </div>

    <div class="pull-right mb20">
      <a id="link_create" href="<c:url value="/giftcard/customer/profile/create" />" class="btn btn-primary"><spring:message code="gc.cxprofile.add" /></a>
    </div>
    <div id="form_cxProfile"></div>
    <div id="list_cxProfiles"><%@include file="cxProfileList.jsp" %></div>
    <div id="notes_cxProfile"><%@include file="cxProfileNotes.jsp" %></div>
  </div>


<style type="text/css">
  <!-- 
  .row-fluid { width: 100px !important; display: inline-block; display: inline; margin-right: 5px; }
  .well select, .well .input {
      margin: 5px;
  }
  -->
</style>
<script type='text/javascript'>
var CxProfile = null;

$(document).ready( function() {

    var $main = $( "#content_cxProfile" );
    var $createLink = $main.find( "#link_create" );
    var $contentForm = $main.find( "#form_cxProfile" );

    initLinks();
    initSearch();

    function initLinks() {
        $createLink.click( function(e) {
            e.preventDefault();
            $.get( $( this ).attr( "href" ), function( resp )  {
            	  CxProfile.loadForm( resp );
            }, "html" );
        });
    }

    function initSearch() {
        var $searchForm = $( "#form_cxProfileSearch" );
        var $search = $searchForm.find( "#searchBtn" );
        var $filter = $searchForm.find( "#filterBtn" );
        var $reset = $searchForm.find( "#resetBtn" );
        var $field = $searchForm.find( "#searchField" );
        var $value = $searchForm.find( "#searchValue" );

        $field.change( function(e) {
        	  $value.attr( "name", $(this).val() );
        });
        $search.click( function(e) {
            $search.prop( "disabled", true );
            $filter.prop( "disabled", true ); 
        	  CxProfileList.filterTable( $searchForm.toObject( { mode : 'first', skipEmpty : true } ), 
        			  function() { 
        		        $search.prop( "disabled", false );
                    $filter.prop( "disabled", false );
        		    });
        });
        $reset.click( function(e) {
            CxProfile.reset( $searchForm );
            $search.trigger( "click" );
        });
        $filter.click( function(e) { $search.trigger( "click" ); });
    }

	  CxProfile = {
        loadForm  : function( resp ) { 
            $contentForm.html( resp );
            CxProfileForm.show();
        },
        processResp  : function( resp, $dialog, errorContainer ) {
            if ( resp.success ) {
                $dialog.modal( "hide" );
            }
            else {
                var errors = "";
                $( resp.result ).each( function( key, value ) { errors += ( ( key + 1 ) + ". " + value.code + "<br>" ); });
                errorContainer.find( "div" ).html( errors );
                errorContainer.show( "slow" );
            }
        },
        reset     : function( ele, isEmpty ) {
            $( ele ).find(':input').each( function() {
                switch(this.type) {
                    case 'password':
                    case 'select-one':
                    case 'text':
                    case 'textarea':
                    case 'file':
                    case 'hidden': $(this).val(''); break;
                    case 'select-multiple': isEmpty? $(this).empty() : $(this).val(""); break;
                    case 'checkbox':
                    case 'radio': this.checked = false;
                }
            });
        }
    };

});
</script>


</div>