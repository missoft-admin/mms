<%@ include file="../../common/taglibs.jsp" %>

<c:set var="ENCTYPE" value="application/x-www-form-urlencoded"/>

<div class="modal-dialog">
  <div class="modal-content">
 
    <div class="modal-body">
      <form:form id="multipleAllocateDto" name="multipleAllocateDto" modelAttribute="multipleAllocateDto" data-print-url="${pageContext.request.contextPath}/loyaltycardinventory/printreceive?"
      data-changelocation-url="${pageContext.request.contextPath}/loyaltycardinventory/changeallocation" 
      action="${pageContext.request.contextPath}/loyaltycardinventory/saveallocation/" method="POST" enctype="${ENCTYPE}" cssClass="form-reset" >

      <div id="contentError" class="hide alert alert-error">
        <button type="button" class="close" onclick="$(this).parent().hide();">&times;</button>
        <div><form:errors path="*"/></div>
      </div>
      
      <div class="contentMain">
        <div class="row-fluid">
          <div class="span4">
            <spring:message code="mo_tier_prop_name" var="cardType" />
            <label for="">${cardType}</label>
            <input type="text" name="cardTypeDesc" value="${multipleAllocateDto.cardType.description}" disabled="disabled" /><input type="hidden" name="cardType" value="${multipleAllocateDto.cardType.code}" />
          </div>
          
          <div class="span4">
            <spring:message code="inventory_prop_location" var="locationLabel" />
            <label for="">${locationLabel}</label>
            <input type="text" name="locationDesc" value="${multipleAllocateDto.locationName}" disabled="disabled" /><input type="hidden" name="location" value="${multipleAllocateDto.location}" />
          </div>
          
          <div class="span4">
            <spring:message code="inventory_prop_allocateto" var="allocateToLabel" />
            <label for="">${allocateToLabel}</label>
            <input type="text" name="allocateToDesc" value="${multipleAllocateDto.transferToName}" disabled="disabled" /><input type="hidden" name="transferTo" value="${multipleAllocateDto.transferTo}" />
          </div>
          
        </div>
    
        <div id="productContainer">
          <table id="productTable" class="table table-condensed table-compressed table-striped">
            <thead>
              <tr>
                <th><spring:message code="mo_tier_prop_quantity" /></th>
                <th><spring:message code="mo_tier_prop_startingseries" /></th>
                <th><spring:message code="mo_tier_prop_endingseries" /></th>
                <th></th>
              </tr>
            </thead>
            <tbody>
              <c:forEach var="item" items="${multipleAllocateDto.allocations}" varStatus="status" >
              <tr>
                <td><input type="text" class="input-mini modalQuantity numeric" /></td>
                <td><input type="text" class="input-medium numeric modalStartingSeries" value="${item.startingSeries}" name="allocations[${status.index}].startingSeries" maxlength="12" /></td>
                <td><input type="text" class="input-medium numeric modalEndingSeries" value="${item.endingSeries}" name="allocations[${status.index}].endingSeries" maxlength="12" /></td>
                <td><button type="button" id="" class="tiptip btn rowDelete icn-delete hidden" title="<spring:message code="label_remove"/>"><spring:message code="label_remove"/></button></td>
              </tr>
              </c:forEach>
            </tbody>
          </table>
          <button type="button" id="addProductBtn" class="tiptip btn pull-right" style="margin-top: 5px;" title="<spring:message code="label_add"/>"><spring:message code="label_add"/></button>
        </div>
      </div>
      </form:form>
    
    </div>
    <div class="modal-footer">
      <button type="button" id="" class="btn btn-default" data-dismiss="modal"><spring:message code="label_cancel"/></button>
      <button type="button" id="" class="btn btn-primary receiveSave" data-msg="<spring:message code="incorrect_allocation_msg" />"><spring:message code="label_receive" /></button>
    </div>
  </div>
</div>

<script src="<spring:url value="/js/viewspecific/loyalty/allocate.js"/>"></script>