package com.transretail.crm.core.service.impl;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import org.apache.commons.lang.mutable.MutableInt;
import org.apache.commons.lang3.StringUtils;
import org.joda.time.LocalDate;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.google.common.collect.Maps;
import com.transretail.crm.common.reporting.jasper.ClasspathFileResolver;
import com.transretail.crm.common.reporting.jasper.DefaultJRProcessor;
import com.transretail.crm.common.reporting.jasper.JRProcessor;
import com.transretail.crm.common.service.dto.request.PagingParam;
import com.transretail.crm.common.service.util.SpringDataPagingUtil;
import com.transretail.crm.core.dto.MemberDto;
import com.transretail.crm.core.dto.MemberMonitorSearchDto;
import com.transretail.crm.core.dto.MemberResultList;
import com.transretail.crm.core.entity.ApplicationConfig;
import com.transretail.crm.core.entity.enums.AppKey;
import com.transretail.crm.core.entity.lookup.LookupDetail;
import com.transretail.crm.core.repo.ApplicationConfigRepo;
import com.transretail.crm.core.repo.MemberRepo;
import com.transretail.crm.core.service.CodePropertiesService;
import com.transretail.crm.core.service.MemberMonitorService;

@Service("memberMonitorService")
@Transactional
public class MemberMonitorServiceImpl implements MemberMonitorService {
    @Autowired
    private CodePropertiesService codePropertiesService;
    @Autowired
    private ApplicationConfigRepo appConfigRepo;
    @Autowired
    private MemberRepo memberRepo;
    @Autowired
    private MessageSource messageSource;
    
    @Override
    public MemberResultList getMonitoredMembers(LocalDate date) {
    	List<MemberDto> monitoredMembers = memberRepo.getMonitoredMembers(getTransactionCountIndividual(), date);
    	return new MemberResultList(monitoredMembers, monitoredMembers.size(), false, false);
    }
    
    @Override
    public MemberResultList getMonitoredMembers(LocalDate date, PagingParam paging) {
        MutableInt totalElements = new MutableInt(0);
    	Pageable pageable = SpringDataPagingUtil.INSTANCE.createPageable(paging);
    	List<MemberDto> monitoredMembers = memberRepo.getMonitoredMembers(getTransactionCountIndividual(), date, paging.getPageSize(), paging.getPageNo(), totalElements);
    	return new MemberResultList(monitoredMembers, totalElements.intValue(), false, false);
    }
    
    @Override
    public MemberResultList getMonitoredMembers(MemberMonitorSearchDto dto) {
    	if(dto.getTransactionCount() == null) {
    		dto.setTransactionCount(getTransactionCountIndividual());
    	}
    	
    	List<MemberDto> monitoredMembers = null;
    	MutableInt totalElements = new MutableInt(0);
    	PagingParam paging = dto.getPagination();
    	if(paging.getPageSize() >= 0) {
    		Pageable pageable = SpringDataPagingUtil.INSTANCE.createPageable(paging);
    		monitoredMembers = memberRepo.getMonitoredMembers(dto, pageable.getPageSize(), pageable.getOffset(), totalElements);
    		return new MemberResultList(monitoredMembers, totalElements.intValue(), pageable.getPageNumber(), pageable.getPageSize());
    	}
    	
    	monitoredMembers = memberRepo.getMonitoredMembers(dto);
    	return new MemberResultList(monitoredMembers, monitoredMembers.size(), false, false);
    }
    
    @Override
    public MemberResultList getAllMonitoredMembers(LocalDate date) {
    	List<MemberDto> monitoredMembers = new ArrayList<MemberDto>();
		monitoredMembers.addAll(memberRepo.getMonitoredMembers(getTransactionCountIndividual(), date));
		monitoredMembers.addAll(memberRepo.getMonitoredEmployees(getTransactionCountEmployee(), date));
    	return new MemberResultList(monitoredMembers, monitoredMembers.size(), false, false);
    }
    
    @Override
    public MemberResultList getMonitoredEmployees(LocalDate date) {
    	List<MemberDto> monitoredMembers = memberRepo.getMonitoredEmployees(getTransactionCountEmployee(), date);
    	return new MemberResultList(monitoredMembers, monitoredMembers.size(), false, false);
    }
    
    @Override
    public MemberResultList getMonitoredEmployees(LocalDate date, PagingParam paging) {
        MutableInt totalElements = new MutableInt(0);
        
    	List<MemberDto> monitoredMembers = memberRepo.getMonitoredEmployees(getTransactionCountEmployee(), date, paging.getPageSize(), paging.getPageNo(), totalElements);
    	return new MemberResultList(monitoredMembers, totalElements.intValue(), false, false);
    }
    
    @Override
    public MemberResultList getMonitoredEmployees(MemberMonitorSearchDto dto) {
    	if(dto.getTransactionCount() == null) {
    		dto.setTransactionCount(getTransactionCountEmployee());
    	}
    	
    	PagingParam paging = dto.getPagination();
    	if(paging.getPageSize() >= 0) {
    	    MutableInt totalElements = new MutableInt(0);
    		Pageable pageable = SpringDataPagingUtil.INSTANCE.createPageable(paging);
    		List<MemberDto> monitoredMembers = memberRepo.getMonitoredEmployees(dto.getTransactionCount(), dto.getDate(), pageable.getPageSize(), pageable.getOffset(), totalElements);
    		return new MemberResultList(monitoredMembers, totalElements.intValue(), pageable.getPageNumber(), pageable.getPageSize());
    	}
    	
    	List<MemberDto> monitoredMembers = memberRepo.getMonitoredEmployees(dto.getTransactionCount(), dto.getDate());
    	return new MemberResultList(monitoredMembers, monitoredMembers.size(), false, false);
    }
    
    @Override
    public void setMonitoredTransactionCountEmployee(Integer transactionCount) {
    	ApplicationConfig txnCount = appConfigRepo.findByKey(AppKey.TXN_COUNT_EMP);
    	if(txnCount == null) {
    		txnCount = new ApplicationConfig();
    		txnCount.setKey(AppKey.TXN_COUNT_EMP);
    	}
    	txnCount.setValue(transactionCount.toString());
    	appConfigRepo.save(txnCount);
    }
    
    @Override
    public void setMonitoredTransactionCountIndividual(Integer transactionCount) {
    	ApplicationConfig txnCount = appConfigRepo.findByKey(AppKey.TXN_COUNT_IND);
    	if(txnCount == null) {
    		txnCount = new ApplicationConfig();
    		txnCount.setKey(AppKey.TXN_COUNT_IND);
    	}
    	txnCount.setValue(transactionCount.toString());
    	appConfigRepo.save(txnCount);
    }
    
    @Override
    public void setMonitoredTransactionCount(Integer transactionCount, LookupDetail memberType) {
    	if(memberType.getCode().equals(codePropertiesService.getDetailMemberTypeEmployee())) {
    		setMonitoredTransactionCountEmployee(transactionCount);
    	}
    	else {
    		setMonitoredTransactionCountIndividual(transactionCount);
    	}
    }
    
    private Integer getTransactionCount(AppKey key) {
    	try {
			return Integer.parseInt(appConfigRepo.findByKey(key).getValue());
		}
		catch(Exception e) {
			return 0;
		}
    }

	@Override
	public Integer getTransactionCountIndividual() {
		return getTransactionCount(AppKey.TXN_COUNT_IND);
	}

	@Override
	public Integer getTransactionCountEmployee() {
		return getTransactionCount(AppKey.TXN_COUNT_EMP);
	}
	
	@Override
	public boolean isMonitored(String accountId, String memberTypeCode) {
		Integer transactionCount = memberTypeCode.equals(codePropertiesService.getDetailMemberTypeEmployee()) ? 
				getTransactionCountEmployee() : getTransactionCountIndividual();
		return memberRepo.countTranscactionsToday(accountId, memberTypeCode) >= transactionCount;
	}
	
	@Override
	public Long countTransactionsToday(String accountId, String memberTypeCode) {
		return memberRepo.countTranscactionsToday(accountId, memberTypeCode);
	}
	
	@Override
	public JRProcessor createJRProcessor(LocalDate date, InputStream logo, boolean isEmployee) {
		Map<String, Object> parameters = Maps.newHashMap();
		Integer transactionCount = null;
		Locale locale = LocaleContextHolder.getLocale();
		List<MemberDto> memberList = isEmployee ? memberRepo.getMonitoredEmployees(transactionCount = getTransactionCountEmployee(), date)
				: memberRepo.getMonitoredMembers(transactionCount = getTransactionCountIndividual(), date);
		
		parameters.put("DATE", date.toString(DateTimeFormat.forPattern("MMMM d, yyyy")));
		parameters.put("SUB_DATA_SOURCE", memberList);
		parameters.put("logo", logo);
		parameters.put("TXNLIMIT", transactionCount);
		parameters.put("REPORT_TYPE", messageSource.getMessage("member_monitor_report", null, locale));
		parameters.put("TXNCOUNTLABEL", messageSource.getMessage("member_monitor_report_txnlimit", null, locale));
		parameters.put("ACCOUNTID", messageSource.getMessage("member_monitor_report_accountid", null, locale));
		parameters.put("FIRSTNAME", messageSource.getMessage("member_monitor_report_firstname", null, locale));
		parameters.put("LASTNAME", messageSource.getMessage("member_monitor_report_lastname", null, locale));
		if (isEmployee) {
		    parameters.put("MEMBERTYPE", messageSource.getMessage("member_monitor_report_employeetype", null, locale));
		} else {
		    parameters.put("MEMBERTYPE", messageSource.getMessage("member_monitor_report_membertype", null, locale));
		}
		parameters.put("TXNCOUNT", messageSource.getMessage("member_monitor_report_txncount", null, locale));
		
		if(memberList.isEmpty()) {
			parameters.put("NO_DATA", "No data found");
		}
		
		DefaultJRProcessor jrProcessor = new DefaultJRProcessor("reports/member_monitor_report.jasper");
		jrProcessor.setParameters(parameters);
		jrProcessor.setFileResolver(new ClasspathFileResolver("reports"));
		return jrProcessor;
	}
}
