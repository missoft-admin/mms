package com.transretail.crm.reconciliation.service;

import java.sql.SQLException;
import java.sql.Timestamp;

/**
 * @author Allan G. Ramirez (agramirez@exist.com)
 */
public interface StoreInfoService {
    int getStoreId();

    String getStoreCode();

    Timestamp getLastTxnSyncDate() throws SQLException;

    Timestamp getLastRefSyncDate() throws SQLException;

    void updateLastTxnSyncDate(Timestamp timestamp) throws SQLException;

    void updateLastRefSyncDate(Timestamp timestamp) throws SQLException;

    String getPosIntegDbUsed();

    void updatePosIntegDbUsed(String posIntegDbUsed) throws SQLException;

    String getMainDbUsed();

    void updateMainDbUsed(String mainDbUsed) throws SQLException;
}
