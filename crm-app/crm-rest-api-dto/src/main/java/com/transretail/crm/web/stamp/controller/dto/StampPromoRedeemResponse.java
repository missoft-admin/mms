package com.transretail.crm.web.stamp.controller.dto;


import java.util.Set;

import org.apache.commons.collections.CollectionUtils;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.google.common.collect.Sets;
import com.transretail.crm.core.dto.StampPromoTxnDto;


@JsonInclude(JsonInclude.Include.NON_NULL)
public class StampPromoRedeemResponse extends StampPromoResponse {

	private Set<StampPromoRedeemStamp> redeemStamps = Sets.newHashSet();



	public Set<StampPromoRedeemStamp> getRedeemStamps() {
		return redeemStamps;
	}
	public void setRedeemStamps(Set<StampPromoRedeemStamp> redeemStamps) {
		this.redeemStamps = redeemStamps;
	}

	public StampPromoRedeemResponse withRedeemStamps( Set<StampPromoTxnDto> redeemStamps ) {
		if ( CollectionUtils.isNotEmpty( redeemStamps ) ) {
			for ( StampPromoTxnDto dto : redeemStamps ) {
				this.redeemStamps.add( toResponseDto( dto ) );
			}
		}
		return this;
    }
	public StampPromoRedeemResponse addRedeemStamp( StampPromoTxnDto dto ) {
		if ( null != dto ) {
			this.redeemStamps.add( toResponseDto( dto ) );
		}
		return this;
    }



	private StampPromoRedeemStamp toResponseDto( StampPromoTxnDto dto ) {
		StampPromoRedeemStamp item = new StampPromoRedeemStamp();
		item.setPromoID( dto.getPromoID().toString() );
		item.setRedeemedStamps( dto.getRedeemedStamps() );
		item.setRemainingStamps( dto.getRemainingStamps() );
		return item;
	}
}
