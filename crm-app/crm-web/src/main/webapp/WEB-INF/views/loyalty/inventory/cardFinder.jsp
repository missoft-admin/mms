<%@ include file="/WEB-INF/views/common/taglibs.jsp" %>
<div id="card-finder-container" class="modal hide  nofly modal-dialog" style="display: none;">
  <div class="modal-content">
    <div class="modal-header">
      <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
      <h4><spring:message code="label_find_card"/></h4>
    </div>
    <div class="modal-body form-horizontal">
      <div class="control-group">
        <spring:message code="label_enter_card_number" var="card_number_input"/>
        <label class="control-label" for="card_number_filter" title="${card_number_input}">${card_number_input}<span
                class="required">*</span></label>

        <div class="controls">
          <input id="card_number_filter" title="${card_number_input}" type="text">
          <input id="search_card" class="btn btn-primary"  type="button"  value="<spring:message code="search"/>">
        </div>
      </div>
      <div id="cardFinderList">
      </div>
    </div>
    <div class="modal-footer">
      <button type="button" class="btn btn-primary" data-dismiss="modal" aria-hidden="true">OK</button>
    </div>  
  </div>
</div>
<script>
  $( document ).ready( function () {
    var $cardFinderList = $( '#cardFinderList' );

    var $dialog = $( '#card-finder-container' )
            .modal( { show : false,
        		keyboard:	false, 
        		backdrop: "static" } )
            .on( 'hide.bs.modal', function () {
	             $(this).formUtil( 'clearInputs' );
	             $cardFinderList.empty();
	             $cardFinderList.ajaxDataTable( {
	                 'ajaxSource' : '<spring:url value="/loyaltycardinventory/search/all" />',
	                 'columnHeaders' : [
	                   '<spring:message code="mo_tier_card_no" />',
	                   '<spring:message code="mo_tier_prop_name" />',
	                   '<spring:message code="mo_prop_status" />',
	                   '<spring:message code="inventory_prop_location" />',
	                     '<spring:message code="loyalty_prop_annualfee" />'
	                 ],
	                 'modelFields' : [
	                   {name : 'cardNo', sortable : true},
	                   {name : 'cardType', sortable : false},
	                   {name : 'status', sortable : false, fieldCellRenderer : function ( status, type, row ) {
	                     if ( isEmpty( status ) && isEmpty( row.location ) ) {
	                       return "<spring:message code='loyalty_not_in_inventory' />"
	                     }
	                     return status;
	                   } },
	                   {name : 'location', sortable : false, fieldCellRenderer : function ( location, type, row ) {
	                     if ( row.locationName != null && row.locationName.trim() != '' ) {
	                       return location + " - " + row.locationName;
	                     }
	                     return location;
	                   } },
	                   {name : 'annualFee', sortable : false}
	                 ],
	                 'fieldModelPropertyMap' : {
	                   "cardNo" : "barcode"
	                 }
	               });
             //var dataTable = $cardFinderList.ajaxDataTable( 'getDataTableObject' );
             //dataTable.fnClearTable();
            } );

    $( '#showCardFinder' ).click( function () {
      $dialog.modal( 'show' );
    } );


    $( '#search_card' ).click( function () {
      var formDataObject = {
        "cardNo" : $( '#card_number_filter' ).val()
      }
      var $btnSearch = $( this );
      $btnSearch.prop( 'disabled', true );
      $cardFinderList.ajaxDataTable( 'search', formDataObject, function () {
        $btnSearch.prop( 'disabled', false );
      } );
    } );
  } );
</script>