<%@ include file="../../common/taglibs.jsp" %>


<div id="content_programForm" class="modal hide  nofly modal-dialog">

  <div class="modal-content">
  <c:url var="url_programForm" value="/promotion/program/save"/>
  <form:form id="programForm" name="program" modelAttribute="program" action="${url_programForm}" class="modal-form form-horizontal">

    <div class="modal-header">
      <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
      <h4><spring:message code="program_label_create"/></h4>      
    </div>

    <div class="modal-body">
      <div class="hide alert alert-error" id="content_error"><button type="button" class="close" onclick="$(this).parent().hide();">&times;</button><div><form:errors path="*"/></div></div>

      <div class="control-group">
        <label for="name" class="control-label"><b class="required">*</b> <spring:message code="program_prop_name" /></label>
        <div class="controls"><form:input path="name" id="name" /></div>
      </div>
      <div class="control-group">
        <label for="description" class="control-label"><b class="required">*</b> <spring:message code="program_prop_description"/></label>
        <div class="controls"><form:input path="description" id="description"/></div>
      </div>
      <div class="input-daterange" id="programDuration">
	      <div class="control-group">
	        <label for="" class="control-label"><b class="required">*</b> <spring:message code="promotion_label_startdate"/></label>
	        <div  class="controls">
	          <input type="text" id="programDurationStart" />
	        </div>
	      </div>
        <div class="control-group">
          <label for="" class="control-label"><b class="required">*</b> <spring:message code="promotion_label_enddate"/></label>
          <div  class="controls">
            <input type="text" id="programDurationEnd" />
          </div>
        </div>
        <form:hidden path="startDate" id="programStartDate"/>
        <form:hidden path="endDate" id="programEndDate"/>
      </div>
      <div class="control-group">
        <label for="" class="control-label"><spring:message code="promotion_label_duration"/></label>
        <spring:message var="label_days" code="promotion_label_days" />
        <div class="controls data-content-04"><span id="noOfProgramDays" data-label="&nbsp;${label_days}"></span></div>
      </div>
    </div>

    <div class="modal-footer">
      <button type="saveButton" id="saveButton" class="btn btn-primary"><spring:message code="label_save" /></button>
      <button type="button" class="btn" data-dismiss="modal" id="cancelButton"><spring:message code="label_cancel" /></button>
    </div>

  </form:form>
  </div>


  <script src='<c:url value="/js/viewspecific/promotion/program/programForm.js"/>' type='text/javascript'><![CDATA[&nbsp; ]]></script>


</div>