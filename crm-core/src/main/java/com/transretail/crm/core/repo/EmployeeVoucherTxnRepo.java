package com.transretail.crm.core.repo;

import org.springframework.stereotype.Repository;

import com.transretail.crm.common.repo.CrmQueryDslPredicateExecutor;
import com.transretail.crm.core.entity.EmployeeVoucherTxnModel;
import com.transretail.crm.core.repo.custom.EmployeeVoucherTxnRepoCustom;

@Repository
public interface EmployeeVoucherTxnRepo extends CrmQueryDslPredicateExecutor<EmployeeVoucherTxnModel, Long>, EmployeeVoucherTxnRepoCustom {
}