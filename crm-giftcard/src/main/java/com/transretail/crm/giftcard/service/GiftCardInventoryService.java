package com.transretail.crm.giftcard.service;

import java.util.List;

import com.transretail.crm.giftcard.entity.SalesOrderAlloc;
import org.joda.time.DateTime;
import org.joda.time.LocalDate;

import com.transretail.crm.common.service.dto.request.PageSortDto;
import com.transretail.crm.common.service.dto.response.ResultList;
import com.transretail.crm.common.service.exception.MessageSourceResolvableException;
import com.transretail.crm.core.dto.MemberDto;
import com.transretail.crm.core.dto.MemberGroupFieldDto;
import com.transretail.crm.core.entity.MemberGroup;
import com.transretail.crm.giftcard.dto.GiftCardInventoryDto;
import com.transretail.crm.giftcard.dto.GiftCardInventoryResultList;
import com.transretail.crm.giftcard.dto.GiftCardInventorySaveDto;
import com.transretail.crm.giftcard.dto.GiftCardInventorySeriesSearchDto;
import com.transretail.crm.giftcard.dto.GiftCardInventorySummarizedResultList;
import com.transretail.crm.giftcard.dto.GiftCardOrderAllocateDto;
import com.transretail.crm.giftcard.dto.GiftCardOrderItemDto;
import com.transretail.crm.giftcard.dto.StockRequestDto;
import com.transretail.crm.giftcard.dto.StockRequestReceiveDto;
import com.transretail.crm.giftcard.entity.GiftCardInventory;
import com.transretail.crm.giftcard.entity.StockRequest;
import com.transretail.crm.giftcard.entity.support.GiftCardInventoryStatus;

/**
 * @author Allan G. Ramirez (agramirez@exist.com)
 */
public interface GiftCardInventoryService {
	GiftCardInventory save(GiftCardInventory gcInv);

	GiftCardInventoryResultList getGiftCardInventories(GiftCardInventorySeriesSearchDto searchDto);

	GiftCardInventorySummarizedResultList getSummarizedGiftCardInventories(GiftCardInventorySeriesSearchDto searchDto);

	void saveGiftCardOrderItems(List<GiftCardOrderItemDto> itemDtos, DateTime orderDate);

	/**
	 * New manufactured gift cards arrived in Head Office. This method will tag the location of the series in inventory table that
	 * corresponds to the series of the new gift cards to Head Office. An expiry date can also be set here
	 * </br>
	 *
	 * @param dto dto
	 * @return total gift cards tagged in inventory table
	 */
	long receiveNewGiftCards(GiftCardInventorySaveDto dto);

	long allocateGiftCards(GiftCardOrderAllocateDto dto);

	GiftCardInventory getGiftCardByBarcode(String barcode);

	void generateGiftCardInventory(List<GiftCardOrderItemDto> items, LocalDate orderDate, Long orderId);

	String generateBarcode(String series);

	List<String> getGiftCardSeries(GiftCardInventorySeriesSearchDto searchDto);

	boolean isSeriesNotExistsInStockStatus(String seriesNo);

	boolean isSeriesNotExists(String seriesNo);

	boolean isCardInTheList(String seriesNo, GiftCardInventorySeriesSearchDto searchDto);

	GiftCardInventoryDto getGiftCardDtoByBarcode(String barcode);

	GiftCardInventoryDto getGiftCardSeriesProductForTransferIn(GiftCardInventorySeriesSearchDto searchDto, StockRequestDto stockRequest) throws MessageSourceResolvableException;

	void updateGiftCardLocations(StockRequestReceiveDto receiveDtos, StockRequest stockRequest);

	GiftCardInventory getGiftCardBySeries(Long series);

	List<String> reserveGiftCards(StockRequestReceiveDto receiveDto, StockRequestDto stockRequest);

	boolean isSeriesValidForExtension(String seriesNo);

	void updateStatus(String series, GiftCardInventoryStatus status);

	List<String> removeGiftCardSeriesFromList(GiftCardInventorySeriesSearchDto searchDto);

	void expireGiftCards();

	ResultList<MemberDto> removeNonPrepaidUserMembers(
			ResultList<MemberDto> members);

	Long getQualifiedMembersCountPrepaidUsers(
			List<MemberGroupFieldDto> memberGroupFieldsDto, MemberGroup group,
			PageSortDto pageSortDto);

	boolean isGiftCardForPromo(String barcode);

    SalesOrderAlloc getGiftCardSo(String noSo);
}
