package com.transretail.crm.web.controller;


import java.util.Date;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.transretail.crm.common.service.dto.request.PageSortDto;
import com.transretail.crm.common.service.dto.response.ResultList;
import com.transretail.crm.common.util.DateUtil;
import com.transretail.crm.core.dto.ApplicationConfigDto;
import com.transretail.crm.schedule.service.AppConfigService;
import com.transretail.crm.web.controller.util.ControllerResponse;


@RequestMapping("/appconfig")
@Controller
public class AppConfigController {
	
	@Autowired
	AppConfigService appConfigService;
	@Autowired
	MessageSource messageSource;
	
	@RequestMapping(value = "/list", method = RequestMethod.POST)
    public @ResponseBody ResultList<ApplicationConfigDto> list(@RequestBody PageSortDto sortDto) {
    	return appConfigService.list(sortDto);
    }
	
	
	@RequestMapping(value = "/update", method = RequestMethod.POST)
    public @ResponseBody ControllerResponse update(@ModelAttribute ApplicationConfigDto configDto,
    		BindingResult bindingResult) {
		ControllerResponse response = new ControllerResponse();
		response.setSuccess(true);
		
		if(StringUtils.isBlank(configDto.getValue())) {
			bindingResult.reject(getMessage("propkey_msg_notempty", new Object[] {"application_config_value"}));
		}
		
		if (bindingResult.hasErrors()) {
			response.setSuccess(false);
			response.setResult(bindingResult.getAllErrors());
			return response;
		}

		if (response.isSuccess()) {
			appConfigService.update(configDto);
		}

		return response;
    }
	
	@RequestMapping
    public String list() {
    	return "appconfig/list";
    }

    @RequestMapping( value="/date/fetch", produces="application/json; charset=utf-8", method=RequestMethod.GET )
	public @ResponseBody ControllerResponse fetchDate() {

    	ControllerResponse resp = new ControllerResponse();
    	resp.setResult( DateUtil.convertDateToString( "dd MMMM yyyy HH:mm:ss", new Date( System.currentTimeMillis() ) ) );
    	resp.setSuccess( true );
		return resp;
	}
	
	private String getMessage(String msgCode, Object[] argMsgCodes) {
		if(argMsgCodes != null) {
			for ( int i=0; i < argMsgCodes.length; i++ ) {
				argMsgCodes[i] = messageSource.getMessage( argMsgCodes[i].toString().toLowerCase(), null, LocaleContextHolder.getLocale() );
			}
		}
		return messageSource.getMessage(msgCode, argMsgCodes, "Info given is invalid.", LocaleContextHolder.getLocale());
	}
	
	
}
