$(document).ready(function() {

	$( "#rewardRejectDialog" ).modal({
		show: false
	});


	var theRejectForm = $( "form[id='rewardRejectForm']" );
	var theFormAction = $( theRejectForm ).attr( "action" );
	$("#rejectButton").click( function(event) {

		$(theRejectForm).unbind("submit").submit( function() {
			$( theRejectForm ).attr("action", 
					theFormAction + getBoundCheckboxes( "id" ) );
		});
		$( theRejectForm ).submit();
	});
	
});