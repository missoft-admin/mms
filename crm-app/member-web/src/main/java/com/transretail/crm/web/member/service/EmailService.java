package com.transretail.crm.web.member.service;

import java.util.Locale;
import java.util.Map;

import com.transretail.crm.common.service.exception.MessageSourceResolvableException;
import com.transretail.crm.core.dto.MessageDto;

/**
 * @author Allan G. Ramirez (agramirez@exist.com)
 */
public interface EmailService {
    void sendValidationLink(String subject, String email, String templateName, Map<String, Object> data, Locale locale) throws
        MessageSourceResolvableException;

	boolean send(MessageDto dto);
}
