<%@ include file="../../common/taglibs.jsp"%>

<fieldset class="form-horizontal container-fluid">
	<div class="control-group">
		<label for="short" class="control-label">
			<spring:message code="label_com_transretail_crm_entity_customermodel_trasactioncode_short" />
		</label>
		<div class="controls">
			<form:input path="professionalProfile.transactionCodeShort" id="short" cssClass="intInput" />
		</div>
	</div>
	
	<div class="control-group">
		<label for="long1" class="control-label">
			<spring:message code="label_com_transretail_crm_entity_customermodel_trasactioncode_long1" />
		</label>
		<div class="controls">
			<form:input path="professionalProfile.transactionCodeLong1" id="long1" cssClass="intInput" />
		</div>
	</div>
	
	<div class="control-group">
		<label for="long2" class="control-label">
			<spring:message code="label_com_transretail_crm_entity_customermodel_trasactioncode_long2" />
		</label>
		<div class="controls">
			<form:input path="professionalProfile.transactionCodeLong2" id="long2" cssClass="intInput" />
		</div>
	</div>
	
	<div class="control-group">
		<label for="" class="control-label">
			<spring:message code="label_com_transretail_crm_entity_customermodel_trasactioncode1" />
		</label>
		<div class="controls">
			<form:input path="professionalProfile.transactionCode1" cssClass="intInput" />
		</div>
	</div>
	
	<div class="control-group">
		<label for="" class="control-label">
			<spring:message code="label_com_transretail_crm_entity_customermodel_trasactioncode2" />
		</label>
		<div class="controls">
			<form:input path="professionalProfile.transactionCode2" cssClass="intInput" />
		</div>
	</div>
	
	<div class="control-group">
		<label for="" class="control-label">
			<spring:message code="label_com_transretail_crm_entity_customermodel_trasactioncode3" />
		</label>
		<div class="controls">
			<form:input path="professionalProfile.transactionCode3" cssClass="intInput" />
		</div>
	</div>
	
	<div class="control-group">
		<label for="" class="control-label">
			<spring:message code="label_com_transretail_crm_entity_customermodel_trasactioncode4" />
		</label>
		<div class="controls">
			<form:input path="professionalProfile.transactionCode4" cssClass="intInput" />
		</div>
	</div>
</fieldset>