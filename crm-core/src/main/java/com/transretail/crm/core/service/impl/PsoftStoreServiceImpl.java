package com.transretail.crm.core.service.impl;


import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.mysema.query.jpa.JPASubQuery;
import com.mysema.query.jpa.impl.JPADeleteClause;
import com.mysema.query.jpa.impl.JPAQuery;
import com.mysema.query.types.ConstructorExpression;
import com.mysema.query.types.expr.BooleanExpression;
import com.transretail.crm.common.service.dto.response.ResultList;
import com.transretail.crm.common.service.util.SpringDataPagingUtil;
import com.transretail.crm.core.dto.PsoftStoreDto;
import com.transretail.crm.core.dto.PsoftStoreSearchDto;
import com.transretail.crm.core.dto.StoreDto;
import com.transretail.crm.core.entity.PeopleSoftStore;
import com.transretail.crm.core.entity.QPeopleSoftStore;
import com.transretail.crm.core.entity.lookup.QStore;
import com.transretail.crm.core.repo.PeopleSoftStoreRepo;
import com.transretail.crm.core.repo.StoreRepo;
import com.transretail.crm.core.service.PsoftStoreService;
import java.util.Map;


@Service("psoftStoreService")
@Transactional
public class PsoftStoreServiceImpl implements PsoftStoreService {

	@Autowired
	PeopleSoftStoreRepo repo;
	@Autowired
	StoreRepo storeRepo;

    @PersistenceContext
    private EntityManager em;



	@Override
	public ResultList<PsoftStoreDto> search( PsoftStoreSearchDto dto ) {
		Pageable pageable = SpringDataPagingUtil.INSTANCE.createPageable( dto.getPagination() );
        BooleanExpression filter = dto.createSearchExpression();
        Page<PeopleSoftStore> page = ( filter != null )? repo.findAll( filter, pageable ) : repo.findAll( pageable );
        return new ResultList<PsoftStoreDto>( PsoftStoreDto.list( page.getContent() ), page.getTotalElements(), page.getNumber(), page.getSize() );
	}

	@Override
	public PsoftStoreDto save( PsoftStoreDto dto ) {
		if ( null == dto ) {
			return null;
		}
		PeopleSoftStore model = dto.update(  null == dto.getId()? null : get( dto.getId() ) );
		if ( null != dto.getStoreId() ) {
			model.setStore( storeRepo.findOne( dto.getStoreId() ) );
		}
		return new PsoftStoreDto( repo.save( model ) );
	}

	@Override
	public PeopleSoftStore get( Long id ) {
		if ( null == id ) {
			return null;
		}
		return repo.findOne( id );
	}

	@Override
	public PsoftStoreDto getDto( Long id ) {
		return new PsoftStoreDto( get( id ) );
	}

	@Override
	public void delete( Long id ) {
		if ( null == id ) {
			return;
		}
		QPeopleSoftStore qModel = QPeopleSoftStore.peopleSoftStore;
		JPADeleteClause query = new JPADeleteClause( em, qModel );
		query.where( qModel.id.eq( id ) );
		query.execute();
	}

	@Override
	public PeopleSoftStore getStoreMapping(String storeCode) {
		PeopleSoftStore mapping = null;
        if (StringUtils.isNotBlank(storeCode)) {
            QPeopleSoftStore qPeopleSoftStore = QPeopleSoftStore.peopleSoftStore;
            mapping = repo.findOne(qPeopleSoftStore.store.code.eq(storeCode));
        }
        return mapping;
	}

	@Override
	public List<StoreDto> getGcAllowedStores() {
		return this.getGcAllowedStores( null );
	}

	@Override
	public List<StoreDto> getGcAllowedStores( PsoftStoreSearchDto dto ) {
		QStore qStore = QStore.store;
		QPeopleSoftStore qPstore = QPeopleSoftStore.peopleSoftStore;
		JPAQuery query = new JPAQuery(em);
		return query.from( qPstore )
			.leftJoin( qPstore.store, qStore )
			.where(
					BooleanExpression.allOf( 
							null != dto? dto.createSearchExpression() : null,
							qPstore.allowGcTxn.isTrue(),
							qStore.isNotNull() ) )
			.distinct()
			.list( ConstructorExpression.create( StoreDto.class, qStore ) );
	}

	@Override
	public List<StoreDto> getUnmappedStores() {
		QStore qStore = QStore.store;
		QPeopleSoftStore qPstore = QPeopleSoftStore.peopleSoftStore;
		JPAQuery query = new JPAQuery(em);
		return query.from( qStore )
			.where( 
				qStore.id.notIn( new JPASubQuery().from( qPstore ).list( qPstore.store.id ) ) )
			.orderBy( qStore.code.asc() ).distinct()
			.list( ConstructorExpression.create( StoreDto.class, qStore ) );
	}

    @Override
    public boolean isStoreAllowedForGcTransaction(String storeCode) {
        QStore qStore = QStore.store;
        QPeopleSoftStore qPstore = QPeopleSoftStore.peopleSoftStore;
        Boolean result = new JPAQuery(em).from(qPstore)
                .join(qPstore.store, qStore)
                    .where(qStore.code.eq(storeCode))
                    .uniqueResult(qPstore.allowGcTxn);
        
        return result == null ? false : result;
    }

    @Override
    public Map<String, String> getAllRegion() {
        QPeopleSoftStore qPstore = QPeopleSoftStore.peopleSoftStore;
        return new JPAQuery(em).from(qPstore)
                .map(qPstore.businessRegion.code, qPstore.businessRegion.code.concat(" - ")
                        .concat(qPstore.businessRegion.description));
    }
}
