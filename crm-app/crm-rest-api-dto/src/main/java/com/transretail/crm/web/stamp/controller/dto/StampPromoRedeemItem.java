package com.transretail.crm.web.stamp.controller.dto;


import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotEmpty;
import org.joda.time.LocalDate;

import com.fasterxml.jackson.annotation.JsonInclude;


@JsonInclude(JsonInclude.Include.NON_NULL)
public class StampPromoRedeemItem {

	@NotNull
	private Long promoID;
	private String promo;
	private String store;
	private LocalDate promoStart;
	private LocalDate promoEnd;
	private LocalDate redeemTo;
	@NotEmpty
	private String sku;
	private String productName;
	@NotNull
	private Long itemQty;
	private Long redeemStamps;



	public Long getPromoID() {
		return promoID;
	}
	public void setPromoID(Long promoID) {
		this.promoID = promoID;
	}
	public String getPromo() {
		return promo;
	}
	public void setPromo(String promo) {
		this.promo = promo;
	}
	public String getStore() {
		return store;
	}
	public void setStore(String store) {
		this.store = store;
	}
	public LocalDate getPromoStart() {
		return promoStart;
	}
	public void setPromoStart(LocalDate promoStart) {
		this.promoStart = promoStart;
	}
	public LocalDate getPromoEnd() {
		return promoEnd;
	}
	public void setPromoEnd(LocalDate promoEnd) {
		this.promoEnd = promoEnd;
	}
	public LocalDate getRedeemTo() {
		return redeemTo;
	}
	public void setRedeemTo(LocalDate redeemTo) {
		this.redeemTo = redeemTo;
	}
	public String getSku() {
		return sku;
	}
	public void setSku(String sku) {
		this.sku = sku;
	}
	public String getProductName() {
		return productName;
	}
	public void setProductName(String productName) {
		this.productName = productName;
	}
	public Long getItemQty() {
		return itemQty;
	}
	public void setItemQty(Long itemQty) {
		this.itemQty = itemQty;
	}
	public Long getRedeemStamps() {
		return redeemStamps;
	}
	public void setRedeemStamps(Long redeemStamps) {
		this.redeemStamps = redeemStamps;
	}

}
