package com.transretail.crm.impl;

import static org.junit.Assert.assertEquals;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import junit.framework.Assert;

import org.hibernate.StatelessSession;
import org.joda.time.DateTime;
import org.joda.time.LocalDate;
import org.junit.After;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Configurable;
import org.springframework.context.MessageSource;
import org.springframework.security.authentication.TestingAuthenticationToken;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.context.SecurityContextImpl;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import com.google.common.collect.Lists;
import com.mysema.query.jpa.hibernate.HibernateUpdateClause;
import com.mysema.query.types.expr.BooleanExpression;
import com.mysema.query.types.expr.NumberExpression;
import com.sun.org.apache.bcel.internal.generic.ALOAD;
import com.transretail.crm.common.util.FileCryptor;
import com.transretail.crm.core.entity.ApplicationConfig;
import com.transretail.crm.core.entity.MemberModel;
import com.transretail.crm.core.entity.UserModel;
import com.transretail.crm.core.entity.embeddable.CustomerProfile;
import com.transretail.crm.core.entity.enums.AppConfigDefaults;
import com.transretail.crm.core.entity.enums.AppKey;
import com.transretail.crm.core.entity.lookup.LookupDetail;
import com.transretail.crm.core.entity.lookup.LookupHeader;
import com.transretail.crm.core.repo.ApplicationConfigRepo;
import com.transretail.crm.core.repo.LookupDetailRepo;
import com.transretail.crm.core.repo.LookupHeaderRepo;
import com.transretail.crm.core.repo.MemberRepo;
import com.transretail.crm.core.repo.UserRepo;
import com.transretail.crm.core.security.model.impl.CustomSecurityUserDetailsImpl;
import com.transretail.crm.core.service.CodePropertiesService;
import com.transretail.crm.loyalty.dto.AllocationDto;
import com.transretail.crm.loyalty.dto.LCInvResultList;
import com.transretail.crm.loyalty.dto.LCInvSearchDto;
import com.transretail.crm.loyalty.dto.LoyaltyCardInventoryResultList;
import com.transretail.crm.loyalty.dto.LoyaltyCardInventorySearchDto;
import com.transretail.crm.loyalty.dto.LoyaltyCardInventorySummaryResultList;
import com.transretail.crm.loyalty.dto.ManufactureOrderDto;
import com.transretail.crm.loyalty.dto.MultipleAllocationDto;
import com.transretail.crm.loyalty.dto.ProductSeriesDto;
import com.transretail.crm.loyalty.dto.ReceiveOrderDto;
import com.transretail.crm.loyalty.entity.LoyaltyCardInventory;
import com.transretail.crm.loyalty.entity.ManufactureOrder;
import com.transretail.crm.loyalty.entity.ManufactureOrderTier;
import com.transretail.crm.loyalty.entity.QLoyaltyCardInventory;
import com.transretail.crm.loyalty.repo.LoyaltyCardInventoryRepo;
import com.transretail.crm.loyalty.repo.ManufactureOrderRepo;
import com.transretail.crm.loyalty.service.LoyaltyCardService;

@Configurable
@Transactional
@ContextConfiguration(locations = "classpath*:/META-INF/spring/applicationContext*.xml")
@RunWith(SpringJUnit4ClassRunner.class)
public class LoyaltyCardServiceTest {
	
	@PersistenceContext
	EntityManager em;
	@Autowired
	MessageSource messageSource;
	@Autowired
	LoyaltyCardService loyaltyCardService;
	@Autowired
	CodePropertiesService codePropertiesService;
	@Autowired
	LookupHeaderRepo lookupHeaderRepo;
	@Autowired
	LookupDetailRepo lookupDetailRepo;
	@Autowired
	ManufactureOrderRepo manufactureOrderRepo;
	@Autowired
	LoyaltyCardInventoryRepo loyaltyCardInventoryRepo;
	@Autowired
	UserRepo userRepo;
	@Autowired
	MemberRepo memberRepo;
	@Autowired
	ApplicationConfigRepo configRepo;
	@Autowired
    StatelessSession statelessSession;
	
	private Long moId;
	
	@Before
	public void setup() {
		setupLookup();
		setupCurrentUser();
		setupMo();
	}
	
	@After
	public void teardown() {
		SecurityContextHolder.clearContext();
		loyaltyCardInventoryRepo.deleteAll();
		manufactureOrderRepo.deleteAll();
		lookupDetailRepo.deleteAll();
		lookupHeaderRepo.deleteAll();
	}
	
	@Test
	public void testGenerateCardNumber() {
		String ret = loyaltyCardService.generateCardNumber("131114987654");
		Assert.assertEquals("1311149876548", ret);
	}
	
	@Test
	public void testGenerateLoyaltyInventory() {
		ManufactureOrderDto dto = new ManufactureOrderDto();
		dto.setId(moId);
		Long initialCount = loyaltyCardInventoryRepo.count();
		loyaltyCardService.generateLoyaltyInventory(dto);
		Assert.assertEquals(initialCount + 60, loyaltyCardInventoryRepo.count());
	}
	
	@Test
	public void testPrintLoyaltyInventory() throws Exception {
		Long initialCount = loyaltyCardInventoryRepo.count();
		ManufactureOrderDto dto = new ManufactureOrderDto();
		dto.setId(moId);
		loyaltyCardService.generateLoyaltyInventory(dto);
		Assert.assertEquals(initialCount + 60, loyaltyCardInventoryRepo.count());
		
		File encrypted = loyaltyCardService.printLoyaltyInventory("123456");
		File decrypted = File.createTempFile("decrypted", ".txt");
		
		FileInputStream fis2 = new FileInputStream(encrypted);
        FileOutputStream fos2 = new FileOutputStream(decrypted);
        FileCryptor.decrypt(FileCryptor.DEFAULT_KEY, fis2, fos2);
        
        Assert.assertEquals(60, countLines(decrypted));
        encrypted.delete();
        decrypted.delete();
	}
	
	private int countLines(File file) throws Exception {
		BufferedReader reader = new BufferedReader(new FileReader(file));
		int lines = 0;
		while (reader.readLine() != null) lines++;
		reader.close();
		return lines;
	}
	
	@Test
	public void testSearchLoyaltyInventories() {
		LookupDetail regular = lookupDetailRepo.findByCode(codePropertiesService.getDetailMemberTierRegular());
		LookupDetail silver = lookupDetailRepo.findByCode(codePropertiesService.getDetailMemberTierSilver());
		LookupDetail gold = lookupDetailRepo.findByCode(codePropertiesService.getDetailMemberTierGold());
		
		QLoyaltyCardInventory qInv = QLoyaltyCardInventory.loyaltyCardInventory;
		Long initialCount = loyaltyCardInventoryRepo.count();
		
		// test loyaltyCardService.generateLoyaltyInventory
		ManufactureOrderDto dto = new ManufactureOrderDto();
		dto.setId(moId);
		loyaltyCardService.generateLoyaltyInventory(dto);
		Assert.assertEquals(initialCount + 60, loyaltyCardInventoryRepo.count());
		
		//test loyaltyCardService.receiveOrders
		loyaltyCardService.receiveOrders(getReceiveDto());
		Assert.assertEquals(initialCount + 60, loyaltyCardInventoryRepo.count());
		Assert.assertEquals(initialCount + 30, loyaltyCardInventoryRepo.count(qInv.status.code.eq(codePropertiesService.getDetailLoyaltyStatusInactive())));
		
		//test loyaltyCardService.searchLoyaltyInventories after receive
		LoyaltyCardInventorySearchDto searchDto = new LoyaltyCardInventorySearchDto();
		LoyaltyCardInventorySummaryResultList list = loyaltyCardService.searchLoyaltyInventories(searchDto);
		Assert.assertTrue(list.getResults().size() == 3);
		Assert.assertTrue(list.getTotalElements() == 3);
		
		//test loyaltyCardService.searchInventoryItems
		searchDto = new LoyaltyCardInventorySearchDto();
		searchDto.setManufactureOrderTierCode(codePropertiesService.getDetailMemberTierSilver());
		searchDto.setManufactureOrderId(moId);
		searchDto.setStatus(codePropertiesService.getDetailLoyaltyStatusInactive());
		searchDto.setTransferToCode("");
		searchDto.setLocationCode(codePropertiesService.getDetailInvLocationDenpasar());
		LoyaltyCardInventoryResultList items = loyaltyCardService.searchInventoryItems(searchDto);
		Assert.assertTrue(items.getResults().size() == 10);
		Assert.assertTrue(items.getTotalElements() == 10);
		
		// test loyaltyCardService.allocateInventory
		MultipleAllocationDto allocationDto = new MultipleAllocationDto();
		allocationDto.setLocation(codePropertiesService.getDetailInvLocationDenpasar());
		allocationDto.setTransferTo(codePropertiesService.getDetailInvLocationLebakBulus());
		List<AllocationDto> allocations = Lists.newArrayList();
		AllocationDto allocation = new AllocationDto();
		allocation.setCardType(regular);
		allocation.setStartingSeries("100000000000");
		allocation.setEndingSeries("100000000004");
		allocations.add(allocation);
		allocation = new AllocationDto();
		allocation.setCardType(gold);
		allocation.setStartingSeries("200000000000");
		allocation.setEndingSeries("200000000004");
		allocations.add(allocation);
		allocation = new AllocationDto();
		allocation.setCardType(silver);
		allocation.setStartingSeries("300000000000");
		allocation.setEndingSeries("300000000004");
		allocations.add(allocation);
		allocationDto.setAllocations(allocations);
		
		loyaltyCardService.allocateInventory(allocationDto, "1");
		Assert.assertEquals(initialCount + 60, loyaltyCardInventoryRepo.count());
		Assert.assertEquals(initialCount + 15, loyaltyCardInventoryRepo.count(qInv.status.code.eq(codePropertiesService.getDetailLoyaltyStatusInactive())));
		Assert.assertEquals(initialCount + 15, loyaltyCardInventoryRepo.count(qInv.status.code.eq(codePropertiesService.getDetailLoyaltyStatusForAllocation())));
		
		//test loyaltyCardService.getInventories
		allocationDto = loyaltyCardService.getInventories(regular, 
				new LookupDetail(codePropertiesService.getDetailLoyaltyStatusForAllocation()), 
				codePropertiesService.getDetailInvLocationDenpasar(), 
				codePropertiesService.getDetailInvLocationLebakBulus(), null);
		Assert.assertEquals(1, allocationDto.getAllocations().size());
		Assert.assertEquals("100000000000", allocationDto.getAllocations().get(0).getStartingSeries());
		Assert.assertEquals("100000000004", allocationDto.getAllocations().get(0).getEndingSeries());
		
		//test loyaltyCardService.saveInventory
		loyaltyCardService.saveInventory(allocationDto, new LookupDetail(codePropertiesService.getDetailLoyaltyStatusAllocated()));
		Assert.assertEquals(initialCount + 60, loyaltyCardInventoryRepo.count());
		Assert.assertEquals(initialCount + 15, loyaltyCardInventoryRepo.count(qInv.status.code.eq(codePropertiesService.getDetailLoyaltyStatusInactive())));
		Assert.assertEquals(initialCount + 10, loyaltyCardInventoryRepo.count(qInv.status.code.eq(codePropertiesService.getDetailLoyaltyStatusForAllocation())));
		Assert.assertEquals(initialCount + 5, loyaltyCardInventoryRepo.count(qInv.status.code.eq(codePropertiesService.getDetailLoyaltyStatusAllocated())));
		
		//test loyaltyCardService.receiveInventory
		loyaltyCardService.receiveInventory(allocationDto);
		Assert.assertEquals(initialCount + 60, loyaltyCardInventoryRepo.count());
		Assert.assertEquals(initialCount + 5, loyaltyCardInventoryRepo.count(qInv.status.code.eq(codePropertiesService.getDetailLoyaltyStatusTransferIn())));
		Assert.assertEquals(initialCount + 10, loyaltyCardInventoryRepo.count(qInv.status.code.eq(codePropertiesService.getDetailLoyaltyStatusForAllocation())));
		
		//test loyaltyCardService.multipleAllocateInventory
		allocationDto = new MultipleAllocationDto();
		allocationDto.setLocation(codePropertiesService.getDetailInvLocationDenpasar());
		allocations = Lists.newArrayList();
		allocation = new AllocationDto();
		allocation.setCardType(regular);
		allocation.setStartingSeries("100000000005");
		allocation.setEndingSeries("100000000009");
		allocation.setTransferTo(codePropertiesService.getDetailInvLocationLebakBulus());
		allocations.add(allocation);
		allocation = new AllocationDto();
		allocation.setCardType(gold);
		allocation.setStartingSeries("200000000005");
		allocation.setEndingSeries("200000000009");
		allocation.setTransferTo(codePropertiesService.getDetailInvLocationLebakBulus());
		allocations.add(allocation);
		allocation = new AllocationDto();
		allocation.setCardType(silver);
		allocation.setStartingSeries("300000000005");
		allocation.setEndingSeries("300000000009");
		allocation.setTransferTo(codePropertiesService.getDetailInvLocationLebakBulus());
		allocations.add(allocation);
		allocationDto.setAllocations(allocations);
		loyaltyCardService.multipleAllocateInventory(allocationDto, "1");
		
		Assert.assertEquals(initialCount + 60, loyaltyCardInventoryRepo.count());
		Assert.assertEquals(initialCount + 5, loyaltyCardInventoryRepo.count(qInv.status.code.eq(codePropertiesService.getDetailLoyaltyStatusTransferIn())));
		Assert.assertEquals(initialCount + 25, loyaltyCardInventoryRepo.count(qInv.status.code.eq(codePropertiesService.getDetailLoyaltyStatusForAllocation())));
		
		//test loyaltyCardService.getInventoriesForMultipleApproval
		allocationDto = loyaltyCardService.getInventoriesForMultipleApproval(codePropertiesService.getDetailInvLocationDenpasar());
		Assert.assertEquals(5, allocationDto.getAllocations().size());
		
		
		//test loyaltyCardService.validateInventory
		Assert.assertFalse(loyaltyCardService.validateInventory(codePropertiesService.getDetailInvLocationDenpasar(), 
				codePropertiesService.getDetailInvLocationHeadOffice(), silver, "300000000005", "300000000009", 
				Lists.newArrayList(new LookupDetail(codePropertiesService.getDetailLoyaltyStatusForAllocation()))));
		Assert.assertFalse(loyaltyCardService.validateInventory(codePropertiesService.getDetailInvLocationDenpasar(), 
				codePropertiesService.getDetailInvLocationLebakBulus(), regular, "300000000005", "300000000009", 
				Lists.newArrayList(new LookupDetail(codePropertiesService.getDetailLoyaltyStatusForAllocation()))));
		Assert.assertFalse(loyaltyCardService.validateInventory(codePropertiesService.getDetailInvLocationDenpasar(), 
				codePropertiesService.getDetailInvLocationLebakBulus(), silver, "300000000005", "300000000009", 
				Lists.newArrayList(new LookupDetail(codePropertiesService.getDetailLoyaltyStatusInactive()))));
		Assert.assertTrue(loyaltyCardService.validateInventory(codePropertiesService.getDetailInvLocationDenpasar(), 
				codePropertiesService.getDetailInvLocationLebakBulus(), silver, "300000000005", "300000000009", 
				Lists.newArrayList(new LookupDetail(codePropertiesService.getDetailLoyaltyStatusForAllocation()))));
		
		//test loyaltyCardService.burnInventory
		allocationDto = new MultipleAllocationDto();
		allocation = new AllocationDto();
		allocation.setStartingSeries("100000000000");
		allocation.setEndingSeries("100000000004");
		allocationDto.setAllocations(Lists.newArrayList(allocation));
		loyaltyCardService.saveForBurning(allocationDto, true, "test");
		Assert.assertTrue(initialCount == loyaltyCardInventoryRepo.count(qInv.status.code.eq(codePropertiesService.getDetailLoyaltyStatusBurned())));
		Assert.assertTrue(initialCount + 5 == loyaltyCardInventoryRepo.count(qInv.forBurning.isTrue()));
		
		/*updateToInTransit("100000000000", "100000000004", codePropertiesService.getDetailInvLocationDenpasar(), codePropertiesService.getDetailInvLocationHeadOffice());
		updateToInTransit("100000000005", "100000000009", codePropertiesService.getDetailInvLocationHeadOffice(), codePropertiesService.getDetailInvLocationDenpasar());*/
		
		allocationDto = new MultipleAllocationDto();
		allocationDto.setLocation(codePropertiesService.getDetailInvLocationHeadOffice());
		allocationDto.setAllocations(new ArrayList<AllocationDto>());
		
		allocation = new AllocationDto();
		allocation.setStartingSeries("100000000000");
		allocation.setEndingSeries("100000000009");
		allocation.setTransferTo(codePropertiesService.getDetailInvLocationLebakBulus());
		allocationDto.getAllocations().add(allocation);
		
		allocation = new AllocationDto();
		allocation.setStartingSeries("100000000010");
		allocation.setEndingSeries("100000000019");
		allocation.setTransferTo(codePropertiesService.getDetailInvLocationDenpasar());
		allocationDto.getAllocations().add(allocation);
		
		loyaltyCardService.multipleAllocateInventory(allocationDto, "000000");
		loyaltyCardService.saveInventory(allocationDto, new LookupDetail(codePropertiesService.getDetailLoyaltyStatusAllocated()));
		loyaltyCardService.saveInventory(allocationDto, new LookupDetail(codePropertiesService.getDetailLoyaltyStatusInTransit()));
		
		allocationDto = new MultipleAllocationDto();
		allocationDto.setTransferTo(codePropertiesService.getDetailInvLocationDenpasar());
		allocationDto.setAllocations(new ArrayList<AllocationDto>());
		
		allocation = new AllocationDto();
		allocation.setStartingSeries("100000000000");
		allocation.setEndingSeries("100000000004");
		allocationDto.getAllocations().add(allocation);
		
		allocation = new AllocationDto();
		allocation.setStartingSeries("100000000010");
		allocation.setEndingSeries("100000000014");
		allocationDto.getAllocations().add(allocation);
		
		loyaltyCardService.receiveInventory(allocationDto);
		searchDto = new LoyaltyCardInventorySearchDto();
		searchDto.setManufactureOrderTier(new LookupDetail(codePropertiesService.getDetailMemberTierRegular()));
		LoyaltyCardInventorySummaryResultList result = loyaltyCardService.searchLoyaltyInventories(searchDto);
		Assert.assertTrue(result.getTotalElements() == 2);
		
		
		loyaltyCardService.sendAllocateToChangeForApproval(allocationDto, "00000");
		result = loyaltyCardService.searchLoyaltyInventories(searchDto);
		Assert.assertTrue(result.getTotalElements() == 3);
		
		String refNo = "";
		for(AllocationDto alloc: allocationDto.getAllocations()) {
			if(alloc.getStartingSeries().equals("100000000000"))
				refNo = alloc.getReferenceNo();
		}
		
		allocationDto = new MultipleAllocationDto();
		allocationDto.setLocation(codePropertiesService.getDetailInvLocationHeadOffice());
		allocationDto.setAllocations(new ArrayList<AllocationDto>());
		allocation = new AllocationDto();
		allocation.setReferenceNo(refNo);
		allocationDto.getAllocations().add(allocation);
		
		loyaltyCardService.approveAllocateToChange(allocationDto);
		result = loyaltyCardService.searchLoyaltyInventories(searchDto);
		Assert.assertTrue(result.getTotalElements() == 3);
		
	}
	
	private ReceiveOrderDto getReceiveDto() {
		ReceiveOrderDto orderDto = new ReceiveOrderDto();
		orderDto.setManufactureOrder(moId);
		orderDto.setReceiptNo("1111111");
		orderDto.setReceivedAt(codePropertiesService.getDetailInvLocationDenpasar());
		orderDto.setReceiveDate(new LocalDate());
		
		List<ProductSeriesDto> products = new ArrayList<ProductSeriesDto>();
		ProductSeriesDto product = new ProductSeriesDto();
		product.setStartingSeries("100000000000");
		product.setEndingSeries("100000000009");
		product.setName(new LookupDetail(codePropertiesService.getDetailMemberTierRegular()));
		products.add(product);
		
		product = new ProductSeriesDto();
		product.setStartingSeries("200000000000");
		product.setEndingSeries("200000000009");
		product.setName(new LookupDetail(codePropertiesService.getDetailMemberTierGold()));
		products.add(product);
		
		product = new ProductSeriesDto();
		product.setStartingSeries("300000000000");
		product.setEndingSeries("300000000009");
		product.setName(new LookupDetail(codePropertiesService.getDetailMemberTierSilver()));
		products.add(product);
		orderDto.setProducts(products);
		
		return orderDto;
	}
	
	private void setupCurrentUser() {
		UserModel currentUser = new UserModel();
    	currentUser.setUsername("loggedInUser");
    	currentUser.setPassword("");
    	currentUser.setEnabled(true);
    	currentUser.setInventoryLocation(codePropertiesService.getDetailInvLocationDenpasar());
    	currentUser.setStoreCode( "store1" );
    	userRepo.save(currentUser);

        CustomSecurityUserDetailsImpl user = new CustomSecurityUserDetailsImpl();
        user.setUserId(currentUser.getId());
        user.setUsername(currentUser.getUsername());
        List<GrantedAuthority> authorities = Lists.newArrayList();
        authorities.add(new SimpleGrantedAuthority("ROLE_CSS"));
        user.setAuthorities(authorities);
        user.setInventoryLocation(codePropertiesService.getDetailInvLocationDenpasar());

        SecurityContextImpl securityContext = new SecurityContextImpl();
        securityContext.setAuthentication(new TestingAuthenticationToken(user, ""));
        SecurityContextHolder.setContext(securityContext);
    }
	
	private void setupMo() {
		ManufactureOrder mo = new ManufactureOrder();
		mo.setMoNo("123456");
		mo.setPoNo("123456");
		
		List<ManufactureOrderTier> listTier = new ArrayList<ManufactureOrderTier>();
		ManufactureOrderTier tier = new ManufactureOrderTier();
		tier.setManufactureOrder(mo);
		tier.setName(lookupDetailRepo.findByCode(codePropertiesService.getDetailMemberTierRegular()));
		tier.setStartingSeries("100000000000");
		tier.setQuantity(20L);
		listTier.add(tier);
		
		tier = new ManufactureOrderTier();
		tier.setManufactureOrder(mo);
		tier.setName(lookupDetailRepo.findByCode(codePropertiesService.getDetailMemberTierGold()));
		tier.setStartingSeries("200000000000");
		tier.setQuantity(20L);
		listTier.add(tier);
		
		tier = new ManufactureOrderTier();
		tier.setManufactureOrder(mo);
		tier.setName(lookupDetailRepo.findByCode(codePropertiesService.getDetailMemberTierSilver()));
		tier.setStartingSeries("300000000000");
		tier.setQuantity(20L);
		listTier.add(tier);
		
		mo.setTiers(listTier);
		
		moId = manufactureOrderRepo.save(mo).getId();
		
	}
	
	private void setupLookup() {

		LookupHeader header = new LookupHeader(codePropertiesService.getHeaderLoyaltyStatus());
		header.setDescription("MO STATUS");
		lookupHeaderRepo.save(header);
		
		LookupDetail detail = new LookupDetail(codePropertiesService.getDetailLoyaltyStatusActive());
		detail.setDescription("ACTIVE");
		detail.setHeader(header);
		lookupDetailRepo.save(detail);
		
		detail = new LookupDetail(codePropertiesService.getDetailLoyaltyStatusInactive());
		detail.setDescription("INACTIVE");
		detail.setHeader(header);
		lookupDetailRepo.save(detail);
		
		detail = new LookupDetail(codePropertiesService.getDetailLoyaltyStatusAllocated());
		detail.setDescription("ALLOCATED");
		detail.setHeader(header);
		lookupDetailRepo.save(detail);
		
		detail = new LookupDetail(codePropertiesService.getDetailLoyaltyStatusForAllocation());
		detail.setDescription("FOR ALLOCATION");
		detail.setHeader(header);
		lookupDetailRepo.save(detail);
		
		detail = new LookupDetail(codePropertiesService.getDetailLoyaltyStatusInTransit());
		detail.setDescription("IN TRANSIT");
		detail.setHeader(header);
		lookupDetailRepo.save(detail);
		
		detail = new LookupDetail(codePropertiesService.getDetailLoyaltyStatusTransferIn());
		detail.setDescription("TRANSFER IN");
		detail.setHeader(header);
		lookupDetailRepo.save(detail);
		
		detail = new LookupDetail(codePropertiesService.getDetailLoyaltyStatusBurned());
		detail.setDescription("BURNED");
		detail.setHeader(header);
		lookupDetailRepo.save(detail);
		
		header = new LookupHeader(codePropertiesService.getHeaderMembershipTier());
		header.setDescription("MEMBERSHIP TIER");
		lookupHeaderRepo.save(header);
		
		detail = new LookupDetail(codePropertiesService.getDetailMemberTierGold());
		detail.setDescription("GOLD");
		detail.setHeader(header);
		lookupDetailRepo.save(detail);
		
		detail = new LookupDetail(codePropertiesService.getDetailMemberTierSilver());
		detail.setDescription("SILVER");
		detail.setHeader(header);
		lookupDetailRepo.save(detail);
		
		detail = new LookupDetail(codePropertiesService.getDetailMemberTierRegular());
		detail.setDescription("REGULAR");
		detail.setHeader(header);
		lookupDetailRepo.save(detail);
		
		header = new LookupHeader(codePropertiesService.getHeaderInventoryLocation());
		header.setDescription("INV LOCATION");
		lookupHeaderRepo.save(header);
		
		detail = new LookupDetail(codePropertiesService.getDetailInvLocationDenpasar());
		detail.setDescription("DENPASAR");
		detail.setHeader(header);
		lookupDetailRepo.save(detail);
		
		detail = new LookupDetail(codePropertiesService.getDetailInvLocationLebakBulus());
		detail.setDescription("LEBAK BULUS");
		detail.setHeader(header);
		lookupDetailRepo.save(detail);
	}
	
	private void updateToInTransit(String startingSeries, String endingSeries, String location, String transferTo) {
		QLoyaltyCardInventory qInventory = QLoyaltyCardInventory.loyaltyCardInventory;
		LookupDetail status = new LookupDetail(codePropertiesService.getDetailLoyaltyStatusInTransit());

		NumberExpression<Long> seriesExp = qInventory.sequence.castToNum(Long.class);
		for(long i = Long.valueOf(startingSeries); i <= Long.valueOf(endingSeries); i++) {
			LoyaltyCardInventory inventory = loyaltyCardInventoryRepo.findOne(seriesExp.eq(i));
			inventory.setLocation(location);
			inventory.setTransferTo(transferTo);
			inventory.setStatus(status);
			loyaltyCardInventoryRepo.save(inventory);
		}
		
/*		HibernateUpdateClause clause = new HibernateUpdateClause(statelessSession, qInventory)
        .set(qInventory.status, status)
        .set(qInventory.transferTo, transferTo)
        .set(qInventory.location, location);

		NumberExpression<Long> seriesExp = qInventory.sequence.castToNum(Long.class);
		
		Long starting = Long.valueOf(startingSeries);
		Long ending = Long.valueOf(endingSeries);
		clause.where(seriesExp.goe(starting).and(seriesExp.loe(ending))).execute();*/
	}
	
	@Test
	public void testFindActivateAndDisableLoyaltyCard() {
		ApplicationConfig expiryConfig = new ApplicationConfig();
		expiryConfig.setKey(AppKey.LOYALTY_EXPIRY_IN_MONTHS);
		expiryConfig.setValue(AppConfigDefaults.DEFAULT_LOYALTY_EXPIRY_IN_MONTHS);
		configRepo.save(expiryConfig);
		
		LookupHeader header = new LookupHeader( codePropertiesService.getHeaderStatus() );
		lookupHeaderRepo.save(header);		
		LookupDetail detail = new LookupDetail( codePropertiesService.getDetailLoyaltyStatusActive() );
		detail.setHeader(header);
		detail.setDescription("ACTIVE");
		lookupDetailRepo.save(detail);
		detail = new LookupDetail( codePropertiesService.getDetailLoyaltyStatusDisabled() );
		detail.setHeader(header);
		detail.setDescription("REPLACED");
		lookupDetailRepo.save(detail);

		String theBarcode = "BARCODE001";
		LoyaltyCardInventory theCard = new LoyaltyCardInventory();
		theCard.setBarcode( theBarcode );
		loyaltyCardInventoryRepo.save( theCard );
		
		LocalDate expectedExpiry = new LocalDate().plusMonths(Integer.parseInt(AppConfigDefaults.DEFAULT_LOYALTY_EXPIRY_IN_MONTHS) + 1).withDayOfMonth(1);

		Assert.assertEquals( theCard.getId(), loyaltyCardService.findLoyaltyCard( theBarcode ).getId() );
		Assert.assertEquals( codePropertiesService.getDetailLoyaltyStatusActive(), 
				loyaltyCardService.activateLoyaltyCard( theBarcode, null, null ).getStatus().getCode() );
		Assert.assertEquals(theCard.getExpiryDate(), expectedExpiry);
		Assert.assertEquals( codePropertiesService.getDetailLoyaltyStatusDisabled(), 
				loyaltyCardService.replaceLoyaltyCard( theBarcode ).getStatus().getCode() );
		
		MemberModel memberModel = new MemberModel();
		memberModel.setEnabled(true);
		memberModel.setUsername("test");
		memberModel.setPassword("1234");
		memberModel.setTotalPoints(100D); // starting points
		memberModel.setAccountId("2");
		memberModel.setContact("2");
		memberModel.setPin("1");

        CustomerProfile customerProfile = new CustomerProfile();
        customerProfile.setBirthdate(new Date());
        memberModel.setCustomerProfile(customerProfile);
        memberModel.setLoyaltyCardNo(theBarcode);
		memberRepo.save(memberModel);
	}

    @Test
    public void searchAllTest() {
        LoyaltyCardInventory card = new LoyaltyCardInventory();
        card.setBarcode("001122");
        loyaltyCardInventoryRepo.save(card);
        card = new LoyaltyCardInventory();
        card.setBarcode("00112233");
        loyaltyCardInventoryRepo.save(card);
        card = new LoyaltyCardInventory();
        card.setBarcode("0011223344");
        loyaltyCardInventoryRepo.save(card);

        LCInvSearchDto dto = new LCInvSearchDto();
        LCInvResultList result = loyaltyCardService.searchAll(dto);
        assertEquals(3, result.getNumberOfElements());
        dto.setCardNo("33");
        result = loyaltyCardService.searchAll(dto);
        assertEquals(2, result.getNumberOfElements());
        dto.setCardNo("44");
        result = loyaltyCardService.searchAll(dto);
        assertEquals(1, result.getNumberOfElements());
    }
}
