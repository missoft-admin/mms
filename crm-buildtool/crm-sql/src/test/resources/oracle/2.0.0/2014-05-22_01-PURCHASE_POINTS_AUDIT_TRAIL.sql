create table CRM_EMPLOYEE_PURCHASE_TXN_AUD (
    id varchar2(255 char) not null,
    REV number(19,0) not null,
    REVTYPE number(3,0),
    APPROVAL_DATE timestamp,
    APPROVED_BY varchar2(255 char),
    REASON varchar2(1000 char),
    REMARKS varchar2(1000 char),
    STATUS varchar2(20 char),
    TXN_AMOUNT double precision,
    TXN_DATE timestamp,
    TXN_NO varchar2(255 char),
    TXN_TYPE varchar2(255 char),
    primary key (id, REV)
);


alter table CRM_EMPLOYEE_PURCHASE_TXN_AUD 
    add constraint FK_sh1vebd930hxlqy3r8valatra 
    foreign key (REV) 
    references CRM_REVINFO;