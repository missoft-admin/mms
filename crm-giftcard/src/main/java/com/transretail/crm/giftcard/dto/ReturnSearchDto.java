package com.transretail.crm.giftcard.dto;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.BooleanUtils;
import org.apache.commons.lang3.StringUtils;
import org.joda.time.LocalDate;

import com.mysema.query.types.expr.BooleanExpression;
import com.transretail.crm.common.service.dto.request.AbstractSearchFormDto;
import com.transretail.crm.giftcard.entity.QReturnRecord;

/**
 *
 */
public class ReturnSearchDto extends AbstractSearchFormDto {
	
	private String product;
	private String orderNo;
	private LocalDate returnDateFrom;
	private LocalDate returnDateTo;
	private Boolean byCards;
	
	public String getProduct() {
		return product;
	}
	public void setProduct(String product) {
		this.product = product;
	}
	public String getOrderNo() {
		return orderNo;
	}
	public void setOrderNo(String orderNo) {
		this.orderNo = orderNo;
	}
	
	public LocalDate getReturnDateFrom() {
		return returnDateFrom;
	}
	public void setReturnDateFrom(LocalDate returnDateFrom) {
		this.returnDateFrom = returnDateFrom;
	}
	public LocalDate getReturnDateTo() {
		return returnDateTo;
	}
	public void setReturnDateTo(LocalDate returnDateTo) {
		this.returnDateTo = returnDateTo;
	}
	public Boolean getByCards() {
		return byCards;
	}
	public void setByCards(Boolean byCards) {
		this.byCards = byCards;
	}
	@Override
	public BooleanExpression createSearchExpression() {
		List<BooleanExpression> expressions = new ArrayList<BooleanExpression>();
		QReturnRecord qRecord = QReturnRecord.returnRecord;
		if(BooleanUtils.isNotFalse(byCards))
			expressions.add(qRecord.items.isNotEmpty());
		else 
			expressions.add(qRecord.items.isEmpty());
		if(StringUtils.isNotBlank(product)) { //TODO
			
		}
		if(StringUtils.isNotBlank(orderNo)) {
			expressions.add(qRecord.orderNo.orderNo.startsWithIgnoreCase(orderNo));	
		}
		if(returnDateFrom != null) {
			expressions.add(qRecord.returnDate.goe(returnDateFrom));
		}
		if(returnDateTo != null) {
			expressions.add(qRecord.returnDate.loe(returnDateTo));
		}
		return BooleanExpression.allOf(expressions.toArray(new BooleanExpression[expressions.size()]));
	}

}
