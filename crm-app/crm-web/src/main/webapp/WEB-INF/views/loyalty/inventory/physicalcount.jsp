<%@ include file="../../common/taglibs.jsp" %>

<style type="text/css">
<!--
  #content_physicalCountList .dataTable tr td:last-child {
    border-left: 0px;
  }
-->
</style>

<div class="page-header page-header2">
    <h1><spring:message code="menutab_loyalty_card_physical_count" /></h1>
</div>

<div class="btn-group">
  <button type="button" id="countStart" class="btn btn-primary"><spring:message code="label_start" /> - <spring:message code="physical_count_label" /></button>
  <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown">
    <span class="caret"></span>
    <span class="sr-only"></span>
  </button>
  <ul class="dropdown-menu" role="menu">
    <li><a id="bySeries" href="#"><spring:message code="physical_count_by_series_range_label"/></a></li>
    <li><a id="byBarcode" href="#"><spring:message code="physical_count_by_barcode_only_label"/></a></li>
  </ul>
</div>
<!--<button type="button" id="countStart" class="btn btn-primary"><spring:message code="label_start" /> - <spring:message code="physical_count_label" /></button>-->
<button type="button" id="countEnd" class="btn btn-primary"><spring:message code="label_end" /> - <spring:message code="label_process" /></button>

<c:if test="${not empty summaryDates}" >
<div class="form-horizontal pull-right">
  <select name="summaryDate" id="summaryDate">
    <c:forEach items="${summaryDates}" var="summaryDate">
      <option value="${summaryDate}"><spring:eval expression="summaryDate" /></option>
    </c:forEach>
  </select>
  <select name="historyReportType" class="input-small" id="historyReportType">
    <c:forEach items="${reportTypes}" var="reportType">
      <option data-url="<c:url value="/loyaltycardinventory/printphysicalcount/${reportType.code}/" />" value="${reportType.code}"><spring:message code="report_type_${reportType.code}" /></option>
    </c:forEach>
  </select>
  <input type="button" id="summaryHistoryPrint" value="<spring:message code="label_print" />" class="btn btn-primary"/>
</div>
</c:if>

<div class="clearfix"></div>
<br/>

  <div id="content_physicalCountList">
    <div id="list_physicalCount"></div>
  </div>


  <link href="<spring:url value="/css/dataTables.css" />" rel="stylesheet">
  <script src="<spring:url value="/js/jquery/jquery.dataTables.js" />" ></script>
  <script src="<spring:url value="/js/jquery/jquery.dataTables.linkLengthMenu.js" />"></script>
  <script src="<spring:url value="/js/jquery/jquery.dataTables.customPaging.js" />"></script>
  <script src="<spring:url value="/js/jquery/ajax-datatable.js" />" ></script>
  <script src="<spring:url value="/js/common/form2js.js" />" ></script>
  <script src="<spring:url value="/js/common/json2.js" />" ></script>
  <script src="<spring:url value="/js/jquery/jquery.toObject.js" />" ></script>
  
  <script src="<spring:url value="/js/jquery/jquery.numberInput.js" />" ></script>

  <script src="<spring:url value="/js/viewspecific/loyalty/physicalcount.js"/>"></script>
  <link href="<c:url value="/styles/temp.css" />" rel="stylesheet" />
  
  <script src='<c:url value="/js/bootstrap/bootstrap-datepicker.js"/>'></script>
  <link href="<c:url value="/css/bootstrap/datepicker.css" />" rel="stylesheet" />
  
  <jsp:include page="physicalcountdialog.jsp" />
  <jsp:include page="physicalcountdialog_by_barcode.jsp" />
  <jsp:include page="physicalcountsummary.jsp" />
  
<script>
$(document).ready(function() {
	$list = $("#list_physicalCount");
	$list.ajaxDataTable({
	    'autoload'  : true,
	    'ajaxSource' : "<c:url value="/loyaltycardinventory/physicalcount/list" />",
	    'aaSorting'   : [[ 0, "desc" ]],
	    'columnHeaders' : [
	         "<spring:message code="mo_tier_prop_name" />",
	         "<spring:message code="mo_tier_prop_startingseries" />",
	         "<spring:message code="mo_tier_prop_endingseries" />",
	         "<spring:message code="mo_tier_prop_quantity" />",
	         "<spring:message code="mo_tier_prop_boxno" />"
	    ],
	    'modelFields' : [
    	     {name : 'cardType', sortable : false, fieldCellRenderer : function (data, type, row) {
  	           return row.cardType.description;
             }},
    	     {name : 'startingSeries', sortable : false},
    	     {name : 'endingSeries', sortable : false},
    	     {name : 'quantity', sortable : false},
    	     {name : 'boxNo', sortable : false}
	     ],
	     'initCompleteCallback': function(dataTable) {
	    	 hideProcessBtn();
	     }
	});
  	
});

function reloadTable() {
	$("#list_physicalCount").ajaxDataTable('search', new Object());	
}

function hideProcessBtn() {
	if($("#list_physicalCount").find("td:first").hasClass('dataTables_empty')) {
		$("#countEnd").hide();
	} else {
		$("#countEnd").show();
	}
}
	
</script>

