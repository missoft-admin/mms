<%@ include file="../common/taglibs.jsp" %>

<style type="text/css">
<!--
  input, select {
    margin-right: 10px;
  }
-->
</style>

<div class="modal hide  groupModal nofly" id="wrapperCalendarDialog" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      
      <div class="modal-header">
  		  <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
  		  <h4 class="isPrimary"><spring:message code="employees_printpurchasetxn" /></h4>
  		</div>
        
   
      <div class="modal-body">
      
      <form id="calendarPrint" target="_blank">
      	<div class="pull-left">
        	<div>
         		<div><label for="name"><spring:message code="label_from" /></label></div>
  			    <div><input type="text" id="content_calendarFrom"/ class="input-small"></div>
  		    </div>
     	  </div>
     	
       	<div class="pull-left">
        	<div>
         		<div><label for="name"><spring:message code="label_to" /></label></div>
  			    <div><input type="text" id="content_calendarTo"/ class="input-small"></div>
  		    </div>
       	</div>
      
      <div class="pull-left">
        <div>
          <div><label for="name">&nbsp;</label></div>
          <div>
          <select name="reportType" id="reportType">
            <c:forEach items="${reportTypes}" var="reportType">
              <option value="${reportType.code}"><spring:message code="report_type_${reportType.code}" /></option>
            </c:forEach>
          </select>
          </div>
        </div>
      </div>
      </form>
      </div>
      <div class="modal-footer">
        <button type="button" id="calendarSubmit" class="btn btn-primary"><spring:message code="employees_printpurchasetxn" /></button>
        <button type="button" id="calendarCancel" class="btn btn-default"><spring:message code="label_cancel"/></button>
      </div>
    </div>
  </div>
</div>

<script type="text/javascript">
  $(document).ready(function() {
	$("#wrapperCalendarDialog").modal({
		show: false
	});
	
	$("#calendarSubmit").unbind("click").click( function(e) {
		e.preventDefault();
         $('#wrapperCalendarDialog').modal('hide');
         var selectedReportType = $("select[name='reportType']").val();
         var url = $('#calendarPrint').attr("action") +
         	"/" + selectedReportType +
  		 	"/" + $('#content_calendarFrom').val() + 
   		 	"/" + $('#content_calendarTo').val();
         if(selectedReportType == "pdf")
         	window.open( url, '_blank', 'toolbar=0,location=0,menubar=0' );
         else if(selectedReportType == "excel")
       	  	window.location.href = url;
         /*
         $('#calendarPrint').unbind("submit").submit( function() {
             $(this).attr("action", $(this).attr("action") + 
           		  "/" + $('#content_calendarFrom').val() + 
           		  "/" + $('#content_calendarTo').val() );
         });

         $('#calendarPrint').submit();*/
	});
	
	$("#calendarCancel").unbind("click").click(function(e) {
		e.preventDefault();
		$('#wrapperCalendarDialog').modal('hide');
	});
	
	$("#content_calendarFrom").datepicker({
    autoclose : true,
		format: 'dd-mm-yyyy'
	}).on('changeDate', function(selected){
		startDate = new Date(selected.date.valueOf());
        startDate.setDate(startDate.getDate(new Date(selected.date.valueOf())));
        $('#content_calendarTo').datepicker('setStartDate', startDate);
	});
	$("#content_calendarTo").datepicker({
	  autoclose : true,
		format: 'dd-mm-yyyy'
	}).on('changeDate', function(selected){
		FromEndDate = new Date(selected.date.valueOf());
        FromEndDate.setDate(FromEndDate.getDate(new Date(selected.date.valueOf())));
        $('#content_calendarFrom').datepicker('setEndDate', FromEndDate);
	});
  
  });
  function openCalendar( inUrl ) {
  	  $('#calendarPrint').attr("action", inUrl + "/" + $("#contentPrint").data("accountId"));
  	  $("#viewTxnDialog").modal("hide");
  	  $('#wrapperCalendarDialog').modal('show');
  }
  
  </script>

