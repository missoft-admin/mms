package com.transretail.crm.core.dto;

import org.joda.time.LocalDate;

import com.transretail.crm.core.entity.PromoBanner;
import com.transretail.crm.core.entity.enums.PromoImageType;
import com.transretail.crm.core.entity.lookup.LookupDetail;

public class PromoBannerDto {
//	private String[] IGNORE = {"statusCode"};
	
	private String id;
	private LocalDate startDate;
	private LocalDate endDate;
	private String link;
	private String name;
	private byte[] image;
	private String startDateString;
	private String endDateString;
	private LookupDetail status;
	private String statusCode;
	private PromoImageType type;
	private String info;
	private String productQuantity;
	private Double oldPrice;
	private Double newPrice;
	
	public PromoBannerDto() {
		
	}
	
	public PromoBannerDto(PromoBanner promoBanner) {
		this.id = promoBanner.getId();
        this.startDate = promoBanner.getStartDate();
        this.endDate = promoBanner.getEndDate();
        this.link = promoBanner.getLink();
        this.name = promoBanner.getName();
        this.image = promoBanner.getImage();
		if(startDate != null) {
			startDateString = startDate.toString("dd-MM-yyyy");
		}
		if(endDate != null) {
			endDateString = endDate.toString("dd-MM-yyyy");
		}
        this.status = promoBanner.getStatus();
        if(this.status != null) {
            this.statusCode = this.status.getCode();
        }
        this.type = promoBanner.getType();
        this.info = promoBanner.getInfo();
        this.productQuantity = promoBanner.getProductQuantity();
        this.oldPrice = promoBanner.getOldPrice();
        this.newPrice = promoBanner.getNewPrice();
	}

    public PromoBanner toModel() {
        return update(null);
    }
    
    public PromoBanner update(PromoBanner model) {
    	if(model == null) {
    		model = new PromoBanner();
    	}
    	
        if (this.id != null) {
            model.setId(this.id);
        }
        model.setStartDate(this.startDate);
        model.setEndDate(this.endDate);
        model.setLink(this.link);
        model.setName(this.name);
        model.setImage(this.image);
        model.setType(this.type);
        if (statusCode != null) {
            model.setStatus(new LookupDetail(statusCode));
        }
        
        model.setOldPrice(oldPrice);
        model.setNewPrice(newPrice);
        model.setProductQuantity(productQuantity);
        model.setInfo(info);
        return model;
    }

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public LocalDate getStartDate() {
		return startDate;
	}

	public void setStartDate(LocalDate startDate) {
		this.startDate = startDate;
		if(startDate != null) {
			startDateString = startDate.toString("dd-MM-yyyy");
		}
	}

	public LocalDate getEndDate() {
		return endDate;
	}

	public void setEndDate(LocalDate endDate) {
		this.endDate = endDate;
		if(endDate != null) {
			endDateString = endDate.toString("dd-MM-yyyy");
		}
	}

	public String getLink() {
		return link;
	}

	public void setLink(String link) {
		this.link = link;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public byte[] getImage() {
		return image;
	}

	public void setImage(byte[] image) {
		this.image = image;
	}

	public String getStartDateString() {
		return startDateString;
	}

	public void setStartDateString(String startDateString) {
		this.startDateString = startDateString;
	}

	public String getEndDateString() {
		return endDateString;
	}

	public void setEndDateString(String endDateString) {
		this.endDateString = endDateString;
	}

	public LookupDetail getStatus() {
		return status;
	}

	public void setStatus(LookupDetail status) {
		this.status = status;
		if(status != null) {
			this.statusCode = status.getCode();
		}
	}

	public String getStatusCode() {
		return statusCode;
	}

	public void setStatusCode(String statusCode) {
		this.statusCode = statusCode;
	}

	public PromoImageType getType() {
		return type;
	}

	public void setType(PromoImageType type) {
		this.type = type;
	}

	public String getProductQuantity() {
		return productQuantity;
	}

	public void setProductQuantity(String productQuantity) {
		this.productQuantity = productQuantity;
	}

	public Double getOldPrice() {
		return oldPrice;
	}

	public void setOldPrice(Double oldPrice) {
		this.oldPrice = oldPrice;
	}

	public Double getNewPrice() {
		return newPrice;
	}

	public void setNewPrice(Double newPrice) {
		this.newPrice = newPrice;
	}

	public String getInfo() {
		return info;
	}

	public void setInfo(String info) {
		this.info = info;
	}
	
}
