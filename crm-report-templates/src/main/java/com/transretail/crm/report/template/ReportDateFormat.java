package com.transretail.crm.report.template;

import com.transretail.crm.common.util.DateUtil;
import org.joda.time.LocalDate;
import org.joda.time.YearMonth;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.springframework.context.i18n.LocaleContextHolder;

/**
 *
 * @author Monte Cillo Co (mco@exist.com)
 */
public final class ReportDateFormat {

    public static final String MONTH_YEAR_FORMAT = "MMMM-yyyy";
    public static final String DATE_FORMAT = DateUtil.SEARCH_DATE_FORMAT;
    public static final DateTimeFormatter RAW_YEAR_MONTH_FORMATTER = DateTimeFormat.forPattern("yyyyMM");
    public static final DateTimeFormatter MONTH_YEAR_FORMATTER = DateTimeFormat.forPattern(MONTH_YEAR_FORMAT)
            .withLocale(LocaleContextHolder.getLocale());
    public static final DateTimeFormatter DATE_FORMATTER = DateTimeFormat.forPattern(DATE_FORMAT)
            .withLocale(LocaleContextHolder.getLocale());

    public static int getQueryDSLYearMonth(String yearMonthString) {
        YearMonth yearMonth = getYearMonth(yearMonthString);
        return getQueryDSLYearMonth(yearMonth);
    }

    public static int getQueryDSLYearMonth(YearMonth yearMonth) {
        return yearMonth.getYear() * 100 + yearMonth.getMonthOfYear();
    }

    /**
     * QueryDSL yearMonth function return a format of yyyyMM. It is based on
     * (year * 100 + month)
     *
     * @param yearMonth
     * @return
     */
    public static YearMonth getYearMonth(int yearMonth) {
        return YearMonth.parse(String.valueOf(yearMonth), RAW_YEAR_MONTH_FORMATTER);
    }

    public static YearMonth getYearMonth(String yearMonthString) {
        return YearMonth.parse(yearMonthString, MONTH_YEAR_FORMATTER);
    }

    public static LocalDate parseDate(String date) {
        return LocalDate.parse(date, DATE_FORMATTER);
    }
}
