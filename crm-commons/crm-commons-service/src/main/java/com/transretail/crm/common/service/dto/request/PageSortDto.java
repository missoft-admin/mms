package com.transretail.crm.common.service.dto.request;

public class PageSortDto {
	private PagingParam pagination = new PagingParam();

    public PagingParam getPagination() {
        return pagination;
    }

    public void setPagination(PagingParam pagination) {
        this.pagination = pagination;
    }
}
