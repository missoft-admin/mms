package com.transretail.crm.common.web.dto;

import javax.validation.constraints.NotNull;

import org.apache.commons.lang3.StringUtils;
import org.joda.time.LocalDate;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.transretail.crm.common.web.converter.LocalDateDeserializer;
import com.transretail.crm.common.web.converter.LocalDateSerializer;

/**
 * @author Allan G. Ramirez (agramirez@exist.com)
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class LogFilterDto {
    /**
     * Search filters separated by |. Eg. [EARN][INIT] | TXN_NO=1234 | HELLO WORLD
     */
    private String filter;
    private String regexFilter;
    @NotNull
    @JsonSerialize(using = LocalDateSerializer.class)
    @JsonDeserialize(using = LocalDateDeserializer.class)
    private LocalDate date;

    @JsonIgnore
    public String[] getFilters() {
        if (StringUtils.isNotBlank(filter)) {
            String[] filters = filter.split("\\|");
            if (filter != null && filters.length > 0) {
                return filters;
            } else {
                return new String[]{filter};
            }
        }
        return null;
    }

    public String getFilter() {
        return filter;
    }

    public void setFilter(String filter) {
        this.filter = filter;
    }

    public String getRegexFilter() {
        return regexFilter;
    }

    public void setRegexFilter(String regexFilter) {
        this.regexFilter = regexFilter;
    }

    public LocalDate getDate() {
        return date;
    }

    public void setDate(LocalDate date) {
        this.date = date;
    }
}
