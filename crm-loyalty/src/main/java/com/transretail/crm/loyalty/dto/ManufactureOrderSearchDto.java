package com.transretail.crm.loyalty.dto;

import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.joda.time.LocalDate;

import com.google.common.collect.Lists;
import com.mysema.query.types.expr.BooleanExpression;
import com.transretail.crm.common.service.dto.request.AbstractSearchFormDto;
import com.transretail.crm.core.util.BooleanExprUtil;
import com.transretail.crm.loyalty.entity.QManufactureOrder;

public class ManufactureOrderSearchDto extends AbstractSearchFormDto {
	private String moNo;
	private String poNo;
	private String mpoNo;
	private Long cardVendorId;
	private String statusCode;
	private LocalDate orderDate;
	
	public ManufactureOrderSearchDto() {
		
	}

	@Override
	public BooleanExpression createSearchExpression() {
		List<BooleanExpression> exprs = Lists.newArrayList();
		BooleanExprUtil builder = BooleanExprUtil.INSTANCE;
		QManufactureOrder manufactureOrders = QManufactureOrder.manufactureOrder;
		
		if(StringUtils.isNotBlank(moNo)) {
			exprs.add(builder.isStringPropertyLike(manufactureOrders.moNo, moNo));
		}
		
		if(StringUtils.isNotBlank(poNo)) {
			exprs.add(builder.isStringPropertyLike(manufactureOrders.poNo, poNo));
		}
		
		if(StringUtils.isNotBlank(mpoNo)) {
			exprs.add(builder.isStringPropertyLike(manufactureOrders.moNo, mpoNo)
					.or(builder.isStringPropertyLike(manufactureOrders.poNo, mpoNo)));
		}
		
		if(StringUtils.isNotBlank(statusCode)) {
			exprs.add(manufactureOrders.status.code.eq(statusCode));
		}
		
		if(cardVendorId != null) {
			exprs.add(manufactureOrders.supplier.id.eq(cardVendorId));
		}
		
		if(orderDate != null) {
			exprs.add(manufactureOrders.orderDate.eq(orderDate));
		}
		
		return BooleanExpression.allOf(exprs.toArray(new BooleanExpression[exprs.size()]));
	}

	public String getMoNo() {
		return moNo;
	}

	public void setMoNo(String moNo) {
		this.moNo = moNo;
	}

	public String getPoNo() {
		return poNo;
	}

	public void setPoNo(String poNo) {
		this.poNo = poNo;
	}

	public Long getCardVendorId() {
		return cardVendorId;
	}

	public void setCardVendorId(Long cardVendorId) {
		this.cardVendorId = cardVendorId;
	}

	public String getStatusCode() {
		return statusCode;
	}

	public void setStatusCode(String statusCode) {
		this.statusCode = statusCode;
	}

	public LocalDate getOrderDate() {
		return orderDate;
	}

	public void setOrderDate(LocalDate orderDate) {
		this.orderDate = orderDate;
	}

	public String getMpoNo() {
		return mpoNo;
	}

	public void setMpoNo(String mpoNo) {
		this.mpoNo = mpoNo;
	}

}
