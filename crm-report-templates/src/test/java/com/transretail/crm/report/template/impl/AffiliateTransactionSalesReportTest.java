package com.transretail.crm.report.template.impl;

import java.io.File;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Locale;
import java.util.Map;

import net.sf.jasperreports.engine.JRParameter;

import org.apache.commons.io.FileUtils;
import org.joda.time.LocalDate;
import org.joda.time.LocalDateTime;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.context.support.MessageSourceResourceBundle;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import com.google.common.collect.Maps;
import com.transretail.crm.common.reporting.jasper.JRProcessor;
import com.transretail.crm.common.reporting.service.ReportService;
import com.transretail.crm.core.entity.lookup.LookupDetail;
import com.transretail.crm.core.entity.lookup.LookupHeader;
import com.transretail.crm.core.repo.LookupDetailRepo;
import com.transretail.crm.core.repo.LookupHeaderRepo;
import com.transretail.crm.core.service.CodePropertiesService;
import com.transretail.crm.giftcard.entity.CardVendor;
import com.transretail.crm.giftcard.entity.GiftCardInventory;
import com.transretail.crm.giftcard.entity.GiftCardOrder;
import com.transretail.crm.giftcard.entity.GiftCardOrderItem;
import com.transretail.crm.giftcard.entity.GiftCardTransaction;
import com.transretail.crm.giftcard.entity.GiftCardTransactionItem;
import com.transretail.crm.giftcard.entity.ProductProfile;
import com.transretail.crm.giftcard.entity.support.GiftCardOrderStatus;
import com.transretail.crm.giftcard.entity.support.GiftCardSaleTransaction;
import com.transretail.crm.giftcard.entity.support.GiftCardSalesType;
import com.transretail.crm.giftcard.entity.support.ProductProfileStatus;
import com.transretail.crm.giftcard.repo.CardVendorRepo;
import com.transretail.crm.giftcard.repo.GiftCardInventoryRepo;
import com.transretail.crm.giftcard.repo.GiftCardOrderRepo;
import com.transretail.crm.giftcard.repo.GiftCardTransactionItemRepo;
import com.transretail.crm.giftcard.repo.GiftCardTransactionRepo;
import com.transretail.crm.giftcard.repo.ProductProfileRepo;

/**
 * @author ftopico
 */
@Transactional
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath*:/META-INF/spring/applicationContext*.xml",
    "/META-INF/spring/applicationContext-test.xml"})
public class AffiliateTransactionSalesReportTest {

    @Autowired
    private GiftCardTransactionRepo giftCardTransactionRepo;
    @Autowired
    private GiftCardInventoryRepo giftCardInventoryRepo;
    @Autowired
    private GiftCardTransactionItemRepo giftCardTransactionItemRepo;
    @Autowired
    private ProductProfileRepo productProfileRepo;
    @Autowired
    private CardVendorRepo cardVendorRepo;
    @Autowired
    private GiftCardOrderRepo giftCardOrderRepo;
    @Autowired
    private AffiliateTransactionSalesReport affiliateTransactionSalesReport;
    @Autowired
    private MessageSource messageSource;
    @Autowired
    private ReportService reportService;
    private static final Logger logger = LoggerFactory.getLogger(AffiliateTransactionSalesReport.class);
    

    @Autowired
    private CodePropertiesService codePropertiesService;
    @Autowired
    private LookupHeaderRepo lookupHeaderRepo;
    @Autowired
    private LookupDetailRepo lookupDetailRepo;
    
    @Before
    public void setup() {
    	

    	LookupHeader hdr = new LookupHeader(codePropertiesService.getProductProfileFaceValue());
        hdr = lookupHeaderRepo.saveAndFlush(hdr);

        LookupDetail dtl = new LookupDetail("PPFV1");
        dtl.setDescription("100000");
        dtl.setHeader(hdr);
        dtl = lookupDetailRepo.saveAndFlush(dtl);
        
        ProductProfile product = new ProductProfile();
        product.setProductCode("000000000001");
        product.setProductDesc("profile 1");
        product.setFaceValue(dtl);
        product.setCardFee(0.0);
        product.setMaxAmount(100L);
        product.setEffectiveMonths(10L);
        product.setUnitCost(100D);
        product.setStatus(ProductProfileStatus.APPROVED);
        product = productProfileRepo.saveAndFlush(product);
        
        CardVendor vendor = new CardVendor();
        vendor.setFormalName("vendor");
        vendor.setMailAddress("ail@a.com");
        vendor.setEmailAddress("email");
        vendor.setEnabled(true);
        vendor.setRegion("region");
        vendor.setDefaultVendor(true);
        vendor = cardVendorRepo.saveAndFlush(vendor);
        
        GiftCardOrder order = new GiftCardOrder();
        order.setPoNumber("1");
        order.setPoDate(LocalDate.now());
        order.setMoNumber("1");
        order.setMoDate(LocalDate.now());
        order.setVendor(vendor);
        order.setItems(new ArrayList<GiftCardOrderItem>());
        order.setStatus(GiftCardOrderStatus.APPROVED);
        order = giftCardOrderRepo.saveAndFlush(order);
        
        GiftCardInventory gcInventory = new GiftCardInventory();
        gcInventory.setProfile(product);
        gcInventory.setOrder(order);
        gcInventory.setProductCode(product.getProductCode());
        gcInventory.setProductName(product.getProductDesc());
        gcInventory.setSeqNo(1);
        gcInventory.setBatchNo(1);
        gcInventory.setOrderDate(LocalDate.now());
        gcInventory.setSeries(10000L);
        gcInventory.setBarcode(String.format("%05d", 1));
        gcInventory.setDeliveryReceipt("1");
        gcInventory.setReceiveDate(LocalDate.now());
        gcInventory = giftCardInventoryRepo.saveAndFlush(gcInventory);
        
        
        GiftCardTransaction gcTransaction = new GiftCardTransaction();
        gcTransaction.setTransactionNo("001");
        gcTransaction.setTransactionDate(LocalDateTime.now());
        gcTransaction.setTransactionType(GiftCardSaleTransaction.ACTIVATION);
        gcTransaction.setSalesType(GiftCardSalesType.B2B);
        
        GiftCardTransactionItem gcTransactionItem = new GiftCardTransactionItem();
        gcTransactionItem.setCurrentAmount(1000.0);
        gcTransactionItem.setGiftCard(gcInventory);
        gcTransactionItem.setTransaction(gcTransaction);
        gcTransactionItem = giftCardTransactionItemRepo.save(gcTransactionItem);
        
        gcTransaction.setTransactionItems(new HashSet<GiftCardTransactionItem>());
        gcTransaction.getTransactionItems().add(gcTransactionItem);
        gcTransaction.setLoseTransaction(false);
        
        gcTransaction.setPeoplesoftStoreMapping(null);
        gcTransaction.setDiscountPercentage(5.0);
        
        giftCardTransactionRepo.saveAndFlush(gcTransaction);
    }
    
    @After
    public void cleanup() {
        giftCardTransactionRepo.deleteAll();
        giftCardInventoryRepo.deleteAll();
        productProfileRepo.deleteAll();
    }
    
    @Test
    public void createJrProcessorTest() throws Exception {
        Map<String, String> map = Maps.newHashMap();
        map.put("DATE_FROM", "01-01-2013");
        map.put("DATE_TO", "01-01-2100");
        map.put("BU", "");
        map.put("SALES_TYPE", "");
        map.put("SALES_STORE", "");
        JRProcessor jrProcessor = affiliateTransactionSalesReport.createJrProcessor(map);
        
        File testClassesDir = FileUtils.toFile(Thread.currentThread().getContextClassLoader().getResource(""));
        File output = new File(testClassesDir, "test-affiliate-sales-report-output.pdf");
        logger.debug("Output location {}", output.getAbsolutePath());
        output.delete();
        Locale locale = LocaleContextHolder.getLocale();
        jrProcessor.addParameter(JRParameter.REPORT_LOCALE, locale);
        jrProcessor.addParameter(JRParameter.REPORT_RESOURCE_BUNDLE, new MessageSourceResourceBundle(messageSource, locale));
        reportService.exportPdfReport(jrProcessor, output);

        double bytes = output.length();
        double kilobytes = (bytes / 1024);
        logger.debug("Kilobytes: {}", kilobytes);
    }
}
