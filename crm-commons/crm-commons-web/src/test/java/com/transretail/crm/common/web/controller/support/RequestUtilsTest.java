package com.transretail.crm.common.web.controller.support;

import static org.junit.Assert.assertEquals;

import org.junit.Test;
import org.springframework.mock.web.MockHttpServletRequest;

/**
 * @author Allan G. Ramirez (agramirez@exist.com)
 */
public class RequestUtilsTest {
    @Test
    public void getRootUrlTest() {
        MockHttpServletRequest request = new MockHttpServletRequest("GET", "/helloworld");
        request.setContextPath("/test");
        request.setServerPort(80);
        assertEquals("http://localhost/test", RequestUtils.INSTANCE.getRootUrl(request));

        request.setServerPort(8080);
        assertEquals("http://localhost:8080/test", RequestUtils.INSTANCE.getRootUrl(request));

        request.setContextPath("/");
        assertEquals("http://localhost:8080", RequestUtils.INSTANCE.getRootUrl(request));
    }
}
