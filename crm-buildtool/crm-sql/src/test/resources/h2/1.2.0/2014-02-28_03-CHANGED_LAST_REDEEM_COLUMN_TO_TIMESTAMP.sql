alter table CRM_POINTS drop column LAST_REDEEM;
alter table CRM_POINTS add column LAST_REDEEM timestamp;