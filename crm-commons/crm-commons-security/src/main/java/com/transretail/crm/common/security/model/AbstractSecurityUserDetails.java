/*
 * Copyright (c) 2012.
 * All rights reserved.
 */

package com.transretail.crm.common.security.model;

import org.apache.commons.lang3.Validate;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import org.springframework.security.core.GrantedAuthority;


public abstract class AbstractSecurityUserDetails<USER_INFO extends SecurityUserInfo>
        implements SecurityUserDetails<USER_INFO> {

    private static final int _7 = 7;

    private static final int _17 = 17;

    protected GrantedAuthority findAuthority(String targetAuthority) {
        Validate.notBlank(targetAuthority, "Target authority cannot be blank.");
        for (GrantedAuthority authority : getAuthorities()) {
            if (targetAuthority.equals(authority.getAuthority())) {
                return authority;
            }
        }

        return null;
    }

    @Override
    public int hashCode() {
        // Hashcode is based solely on the username only
        return new HashCodeBuilder(_7, _17)
                .append(getUsername())
                .toHashCode();
    }

    @Override
    public boolean equals(Object obj) {

        if (null == obj) {
            return false;
        }

        if (this == obj) {
            return true;
        }

        if (getClass() != obj.getClass()) {
            return false;
        }

        // Equality is based solely on the username only. Changes in ip address or patient pins should not affect equality.
        // This is to ensure that concurrent session management is on a per username basis only.
        SecurityUserDetails rhs = (SecurityUserDetails) obj;
        return new EqualsBuilder()
                .append(getUsername(), rhs.getUsername())
                .isEquals();
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this, ToStringStyle.SHORT_PREFIX_STYLE)
                .append("username", getUsername())
                .append("password.length", getPassword() == null ? null : getPassword().length())
                .append("authorities", getAuthorities())
                .append("userInfo", getUserInfo())
                .append("accountNonExpired", isAccountNonExpired())
                .append("accountNonLocked", isAccountNonLocked())
                .append("credentialsNonExpired", isCredentialsNonExpired())
                .append("enabled", isEnabled())
                .toString();
    }
}

