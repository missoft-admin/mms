package com.transretail.crm.web.giftcard.controller.dto;


import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotEmpty;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.transretail.crm.giftcard.dto.GiftCardTransactionInfo;
import com.transretail.crm.web.giftcard.controller.AbstractGiftCardRequest;


@JsonInclude(JsonInclude.Include.NON_NULL)
public class EGiftCardPreactivateRequest extends AbstractGiftCardRequest {

    @NotEmpty
    private String productCode;
    @NotNull
    private Long quantity;



	public String getProductCode() {
		return productCode;
	}
	public void setProductCode(String productCode) {
		this.productCode = productCode;
	}
	public Long getQuantity() {
		return quantity;
	}
	public void setQuantity(Long quantity) {
		this.quantity = quantity;
	}

	@Override
    public String getSignature() {
        return String.format( "%s%s%s%s", getMerchantId(), getTerminalId(), getCashierId(), productCode );
    }

    @SuppressWarnings("rawtypes")
	@Override
    public GiftCardTransactionInfo toGiftCardTransactionInfo() {
        return super.templateBaseTransactionInfo();
    }

}
