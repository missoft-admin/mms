package com.transretail.crm.core.dto;


import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.transretail.crm.core.entity.ApprovalRemark;


public class ApprovalRemarkDto {

//	private static final String[] IGNORE = { "promotion", "status", "created" };

	private Long id;
    private String modelId;
    private String modelType;
    private Long promotion;
    private String remarks;
    private String status;
    private String dispStatus;

    private String createUser;
    private Date created;

    public ApprovalRemarkDto() {}
    public ApprovalRemarkDto( ApprovalRemark inDto  ) {
        this.id = inDto.getId();
        this.modelId = inDto.getModelId();
        this.modelType = inDto.getModelType();
        this.remarks = inDto.getRemarks();
        this.createUser = inDto.getCreateUser();

		if ( null != inDto.getStatus() ) {
            this.status = inDto.getStatus().getCode();
            this.dispStatus = inDto.getStatus().getDescription();
		}
    	if ( null != inDto.getCreated() ) {
            this.created = inDto.getCreated().toDate();
    	}
    }

	public ApprovalRemark toModel() {
		ApprovalRemark theModel = new ApprovalRemark();
		theModel.setModelId(this.modelId);
        theModel.setModelType(this.modelType);
        theModel.setRemarks(this.remarks);
		return theModel;
	}

    public static List<ApprovalRemarkDto> toDtoList( List<ApprovalRemark> models ) {
    	List<ApprovalRemarkDto> dtos = new ArrayList<ApprovalRemarkDto>();
    	for ( ApprovalRemark model : models ) {
    		dtos.add( new ApprovalRemarkDto( model ) );
    	}
    	return dtos;
    }




	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getModelId() {
		return modelId;
	}

	public void setModelId(String modelId) {
		this.modelId = modelId;
	}

	public String getModelType() {
		return modelType;
	}

	public void setModelType(String modelType) {
		this.modelType = modelType;
	}

	public Long getPromotion() {
		return promotion;
	}

	public void setPromotion(Long promotion) {
		this.promotion = promotion;
	}

	public String getRemarks() {
		return remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getDispStatus() {
		return dispStatus;
	}

	public void setDispStatus(String dispStatus) {
		this.dispStatus = dispStatus;
	}

	public String getCreateUser() {
		return createUser;
	}

	public void setCreateUser(String createUser) {
		this.createUser = createUser;
	}

	public Date getCreated() {
		return created;
	}

	public void setCreated(Date created) {
		this.created = created;
	}
}
