package com.transretail.crm.core.entity;

import com.transretail.crm.core.entity.lookup.LookupDetail;
import com.transretail.crm.core.entity.support.CustomAuditableEntity;
import java.io.Serializable;
import javax.persistence.*;

/**
 *
 * @author Monte Cillo Co (mco@exist.com)
 */
@Entity
@Table(name = "CRM_BU_PSOFT_CONFIG", uniqueConstraints
        = @UniqueConstraint(columnNames = {"BU", "PSOFT_CONFIG_ID"}))
public class BusinessUnitPsoftConfig extends CustomAuditableEntity<Long> implements Serializable {

    @OneToOne(cascade = {CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REMOVE})
    @JoinColumn(name = "BU", referencedColumnName = "code")
    private LookupDetail businessUnit;
    @ManyToOne
    @JoinColumn(name = "PSOFT_CONFIG_ID")
    private CrmForPeoplesoftConfig configuration;

    public BusinessUnitPsoftConfig(LookupDetail businessUnit) {
        this.businessUnit = businessUnit;
    }

    public LookupDetail getBusinessUnit() {
        return businessUnit;
    }

    public void setBusinessUnit(LookupDetail businessUnit) {
        this.businessUnit = businessUnit;
    }

    public CrmForPeoplesoftConfig getConfiguration() {
        return configuration;
    }

    public void setConfiguration(CrmForPeoplesoftConfig configuration) {
        this.configuration = configuration;
    }
}
