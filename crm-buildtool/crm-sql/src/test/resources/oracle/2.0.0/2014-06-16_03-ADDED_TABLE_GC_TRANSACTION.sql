
create table CRM_GC_TRANSACTION (
        id number(19,0) not null,
        CREATED_BY varchar2(255 char),
        CREATED_DATETIME timestamp,
        LAST_UPDATED_BY varchar2(255 char),
        LAST_UPDATED_DATETIME timestamp,
        CASHIER_ID varchar2(12 char) not null,
        TERMINAL_ID varchar2(12 char) not null,
        TXN_DATE timestamp not null,
        TXN_NO varchar2(15 char) not null,
        TXN_TYPE varchar2(255 char) not null,
        MERCHANT_ID varchar2(255 char) not null,
        REF_TXN_NO varchar2(15 char),
        primary key (id)
    );

    create table CRM_GC_TRANSACTION_ITEM (
        id number(19,0) not null,
        CURR_AMOUNT double precision,
        TXN_AMOUNT double precision,
        CARD_NO varchar2(20 char) not null,
        transaction_id number(19,0),
        primary key (id)
    );

    alter table CRM_GC_TRANSACTION 
        add constraint UK_pnvsrlbnu551liawp7l0629vv unique (TXN_NO);

    alter table CRM_GC_TRANSACTION 
        add constraint FK_q1a3ub5qqw4h97fg1itqxeuap 
        foreign key (REF_TXN_NO) 
        references CRM_GC_TRANSACTION (TXN_NO);

    alter table CRM_GC_TRANSACTION_ITEM 
        add constraint FK_kg2heuggjfw9c9wjs484yb0qe 
        foreign key (CARD_NO) 
        references CRM_GC_INVENTORY (BARCODE);

    alter table CRM_GC_TRANSACTION_ITEM 
        add constraint FK_de3f2nksa6915s54931pr5c66 
        foreign key (transaction_id) 
        references CRM_GC_TRANSACTION;


-- DROP

--     drop table CRM_GC_TRANSACTION cascade constraints;
-- 
--     drop table CRM_GC_TRANSACTION_ITEM cascade constraints;