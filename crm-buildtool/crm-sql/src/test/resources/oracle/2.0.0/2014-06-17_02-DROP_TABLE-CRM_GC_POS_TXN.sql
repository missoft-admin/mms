
-- DROP TABLE CRM_GC_POS_TXN and CRM_GC_POS_TXN_DTL
-- this replaced by CRM_GC_TRANSACTION and CRM_GC_TRANSACTION_ITEM

drop table CRM_GC_POS_TXN_DTL cascade constraints;
drop table CRM_GC_POS_TXN cascade constraints;