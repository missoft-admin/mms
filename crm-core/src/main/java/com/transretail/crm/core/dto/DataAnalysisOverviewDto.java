package com.transretail.crm.core.dto;

import com.mysema.query.annotations.QueryProjection;





public class DataAnalysisOverviewDto {
	
	private String month;
	private Double current;
	private Double previous = new Double(0);
	private Double diff;
	private Double diffPercent;
	
	public DataAnalysisOverviewDto() {}
	
	
	@QueryProjection
	public DataAnalysisOverviewDto(String month, Double current, Double previous) {
		super();
		this.month = month;
		this.current = current;
		this.previous = previous;
	}



	public String getMonth() {
		return month;
	}
	public void setMonth(String month) {
		this.month = month;
	}
	public Double getCurrent() {
		return current;
	}
	public void setCurrent(Double current) {
		this.current = current;
	}
	public Double getPrevious() {
		if(previous == null)
			return 0D;
		
		return previous;
	}
	public void setPrevious(Double previous) {
		this.previous = previous;
	}
	public Double getDiff() {
		if(current != null)
			return current - getPrevious();
		else
			return null;
	}
	public void setDiff(Double diff) {
		this.diff = diff;
	}
	public Double getDiffPercent() {
		if(current == null)
			return null;
		else if(current == 0 && getPrevious() == 0)
			return 0D;
		else if(getPrevious() == 0)
			return 100D;
		else if(current == 0)
			return -100D;
		else
			return (getDiff()/current) * 100;
	}
	public void setDiffPercent(Double diffPercent) {
		this.diffPercent = diffPercent;
	}
	
	
}
