package com.transretail.crm.loyalty.dto;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.BooleanUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.mysema.query.types.expr.BooleanExpression;
import com.transretail.crm.common.service.dto.request.AbstractSearchFormDto;
import com.transretail.crm.core.entity.lookup.LookupDetail;
import com.transretail.crm.loyalty.entity.QLoyaltyCardInventory;

public class LoyaltyCardInventorySearchDto extends AbstractSearchFormDto {
	
	private String manufactureOrder;
	
	private Long manufactureOrderId;
	
	private LookupDetail manufactureOrderTier;
	
	private String manufactureOrderTierCode;
	
	private String location;
	
	private String locationCode;
	
	private String transferTo;
	
	private String transferToCode;
	
	private String newTransferToCode;
	
	private String newTransferTo;
	
	private String status;
	
	private String referenceNo;
	
	private String batchNo;
	
	private String forBurning;
	
	private Long id;
	
	@JsonIgnore
	public BooleanExpression createItemsSearchExpression() {
		QLoyaltyCardInventory qInventory = QLoyaltyCardInventory.loyaltyCardInventory;
		List<BooleanExpression> expressionsFields = new ArrayList<BooleanExpression>();
		
		if(StringUtils.isNotBlank(getManufactureOrder()) && !getManufactureOrder().equalsIgnoreCase("null")) {
			expressionsFields.add(qInventory.product.manufactureOrder.moNo.eq(getManufactureOrder()));	
		}
		
		if(getManufactureOrderTierCode() == null) {
				expressionsFields.add(qInventory.product.isNull());
		} else
			expressionsFields.add(qInventory.product.name.code.eq(getManufactureOrderTierCode()));
		
		
		if(StringUtils.isNotBlank(getLocationCode())) {
			if(getLocationCode().equalsIgnoreCase("null"))
				expressionsFields.add(qInventory.location.isNull());
			else
				expressionsFields.add(qInventory.location.eq(getLocationCode()));	
		}
		
		if(StringUtils.isNotBlank(getTransferToCode())) {
			if(getTransferToCode().equalsIgnoreCase("null")) {
				expressionsFields.add(qInventory.transferTo.isNull());
			} else {
				expressionsFields.add(qInventory.transferTo.eq(getTransferToCode()));	
			}	
		}
		
		if(StringUtils.isNotBlank(getNewTransferToCode())) {
			if(getNewTransferToCode().equalsIgnoreCase("null")) {
				expressionsFields.add(qInventory.newTransferTo.isNull());
			} else {
				expressionsFields.add(qInventory.newTransferTo.eq(getNewTransferToCode()));	
			}	
		}
		
		if(StringUtils.isNotBlank(getForBurning())) {
			if(getForBurning().equalsIgnoreCase("null")) {
				expressionsFields.add(qInventory.forBurning.isNull());
			} else {
				expressionsFields.add(qInventory.forBurning.eq(new Boolean(forBurning)));	
			}	
		}
		
		BooleanExpression fields = BooleanExpression.allOf(expressionsFields.toArray(new BooleanExpression[expressionsFields.size()]));
		
		List<BooleanExpression> expressions = new ArrayList<BooleanExpression>();
		
		if(StringUtils.isNotBlank(status)) {
			if(status.equalsIgnoreCase("null"))
				expressions.add(qInventory.status.isNull());
			else {
				expressions.add(qInventory.status.code.eq(status));
			}
		}
		
		BooleanExpression status = BooleanExpression.anyOf(expressions.toArray(new BooleanExpression[expressions.size()]));
		
		return BooleanExpression.allOf(fields, status);
	}
	
	@Override
	@JsonIgnore
	public BooleanExpression createSearchExpression() {
		QLoyaltyCardInventory qInventory = QLoyaltyCardInventory.loyaltyCardInventory;
		List<BooleanExpression> expressionsFields = new ArrayList<BooleanExpression>();
		
		if(StringUtils.isNotBlank(batchNo)) {
			expressionsFields.add(qInventory.batchRefNo.eq(batchNo));
		}
		
		if(StringUtils.isNotBlank(getManufactureOrder()))
			expressionsFields.add(qInventory.product.manufactureOrder.moNo.startsWithIgnoreCase(getManufactureOrder()));
		
		if(manufactureOrderTier != null)
			expressionsFields.add(qInventory.product.name.eq(manufactureOrderTier));
		
		if(location != null)
			expressionsFields.add(qInventory.location.eq(location));
		
		if(transferTo != null)
			expressionsFields.add(qInventory.transferTo.eq(transferTo));
		
		expressionsFields.add(qInventory.status.isNotNull());
		
		if(StringUtils.isNotBlank(getReferenceNo())) {
			expressionsFields.add(qInventory.referenceNo.eq(getReferenceNo()));	
		}
		
		return BooleanExpression.allOf(expressionsFields.toArray(new BooleanExpression[expressionsFields.size()]));
	}

	public String getManufactureOrder() {
		return manufactureOrder;
	}

	public void setManufactureOrder(String manufactureOrder) {
		this.manufactureOrder = manufactureOrder;
	}

	public LookupDetail getManufactureOrderTier() {
		return manufactureOrderTier;
	}

	public void setManufactureOrderTier(LookupDetail manufactureOrderTier) {
		this.manufactureOrderTier = manufactureOrderTier;
	}

	
	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		this.location = location;
	}

	public String getTransferTo() {
		return transferTo;
	}

	public void setTransferTo(String transferTo) {
		this.transferTo = transferTo;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getLocationCode() {
		return locationCode;
	}

	public void setLocationCode(String locationCode) {
		this.locationCode = locationCode;
	}

	public String getTransferToCode() {
		return transferToCode;
	}

	public void setTransferToCode(String transferToCode) {
		this.transferToCode = transferToCode;
	}

	public Long getManufactureOrderId() {
		return manufactureOrderId;
	}

	public void setManufactureOrderId(Long manufactureOrderId) {
		this.manufactureOrderId = manufactureOrderId;
	}

	public String getManufactureOrderTierCode() {
		return manufactureOrderTierCode;
	}

	public void setManufactureOrderTierCode(String manufactureOrderTierCode) {
		this.manufactureOrderTierCode = manufactureOrderTierCode;
	}

	public String getReferenceNo() {
		return referenceNo;
	}

	public void setReferenceNo(String referenceNo) {
		this.referenceNo = referenceNo;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getBatchNo() {
		return batchNo;
	}

	public void setBatchNo(String batchNo) {
		this.batchNo = batchNo;
	}

	public String getNewTransferToCode() {
		return newTransferToCode;
	}

	public void setNewTransferToCode(String newTransferToCode) {
		this.newTransferToCode = newTransferToCode;
	}

	public String getForBurning() {
		return forBurning;
	}

	public void setForBurning(String forBurning) {
		this.forBurning = forBurning;
	}

	public String getNewTransferTo() {
		return newTransferTo;
	}

	public void setNewTransferTo(String newTransferTo) {
		this.newTransferTo = newTransferTo;
	}


	
}
