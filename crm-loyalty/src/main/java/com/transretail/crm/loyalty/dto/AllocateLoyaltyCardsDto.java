package com.transretail.crm.loyalty.dto;

import java.util.ArrayList;
import java.util.List;

import org.joda.time.LocalDate;
import org.springframework.format.annotation.DateTimeFormat;

import com.transretail.crm.core.entity.lookup.LookupDetail;

public class AllocateLoyaltyCardsDto {
	
	private String location;
	
	private String allocateTo;
	
	@DateTimeFormat(pattern = "dd-MM-yyyy")
	private LocalDate allocateDate = new LocalDate();
	
	private String referenceNo;
	
	private String encodedBy;
	
	private String reason;
	
	private LookupDetail status;
	
	private List<ProductSeriesDto> products;
	
	public AllocateLoyaltyCardsDto() {
		List<ProductSeriesDto> products = new ArrayList<ProductSeriesDto>();
		products.add(new ProductSeriesDto());
		this.products = products;
	}

	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		this.location = location;
	}

	public String getAllocateTo() {
		return allocateTo;
	}

	public void setAllocateTo(String allocateTo) {
		this.allocateTo = allocateTo;
	}

	public LocalDate getAllocateDate() {
		return allocateDate;
	}

	public void setAllocateDate(LocalDate allocateDate) {
		this.allocateDate = allocateDate;
	}

	public String getReferenceNo() {
		return referenceNo;
	}

	public void setReferenceNo(String referenceNo) {
		this.referenceNo = referenceNo;
	}

	public String getEncodedBy() {
		return encodedBy;
	}

	public void setEncodedBy(String encodedBy) {
		this.encodedBy = encodedBy;
	}

	public List<ProductSeriesDto> getProducts() {
		return products;
	}

	public void setProducts(List<ProductSeriesDto> products) {
		this.products = products;
	}

	public String getReason() {
		return reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	public LookupDetail getStatus() {
		return status;
	}

	public void setStatus(LookupDetail status) {
		this.status = status;
	}


}
