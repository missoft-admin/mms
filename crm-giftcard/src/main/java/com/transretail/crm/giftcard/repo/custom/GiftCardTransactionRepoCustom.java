package com.transretail.crm.giftcard.repo.custom;

import com.transretail.crm.giftcard.dto.AffiliateDto;
import com.transretail.crm.giftcard.dto.LoseTransactionResultList;
import com.transretail.crm.giftcard.dto.PosLoseTransactionSearchDto;
import com.transretail.crm.giftcard.entity.GiftCardInventory;
import com.transretail.crm.giftcard.entity.GiftCardTransaction;
import com.transretail.crm.giftcard.entity.PosGcTransaction;
import com.transretail.crm.giftcard.entity.support.GiftCardSaleTransaction;
import java.util.List;
import java.util.Map;
import javax.annotation.Nonnull;
import org.joda.time.YearMonth;

/**
 *
 * @author Monte Cillo Co (mco@exist.com)
 */
public interface GiftCardTransactionRepoCustom {

    GiftCardTransaction findEnclosingTransactionOfGiftCard(
            GiftCardInventory cardInventory, GiftCardSaleTransaction saleTransaction);

    long countGiftCardGroupByTransaction(GiftCardInventory inventory, GiftCardSaleTransaction saleTransaction);

    /**
     * Find the enclosing transaction using of finding entry in
     * GiftCardOrderAlloc to find SalesOrder and used it to find the
     * transaction.
     *
     * @param inventory
     * @return
     */
    GiftCardTransaction findEnclosingTransactionViaSalesOrder(GiftCardInventory inventory);

    boolean transactionNoExists(String transactioNo, GiftCardSaleTransaction transaction);

    GiftCardTransaction findLoseTransactionByTransactionNo(String transactioNo);

    LoseTransactionResultList findAllLoseTransactions(PosLoseTransactionSearchDto pageSort);

    Map<List<?>, List<PosGcTransaction>> findMissingPosGcTransactions();

    Character findPosGcTransactionType(String transactionId);

    long tagAffiliatesTransactionAsPosted(YearMonth yearMonth);
    
    List<AffiliateDto> getBusinessUnitTransactionTypeAmount(YearMonth yearMonth);
    
    Double getTransactionAmount(@Nonnull String transactionNo, @Nonnull GiftCardSaleTransaction txType, @Nonnull String barcode);
    
}
