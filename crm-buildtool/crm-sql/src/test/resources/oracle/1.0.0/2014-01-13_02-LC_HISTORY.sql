create table CRM_LC_INVENTORY_HIST (
        ID number(19,0) not null,
        CREATED_BY varchar2(255 char),
        CREATED_DATETIME timestamp,
        CREATED_DATE date,
        INVENTORY_NO number(19,0) not null,
        LOCATION varchar2(15 char) not null,
        ORIGIN varchar2(15 char),
        STATUS varchar2(15 char) not null,
        primary key (ID)
    );
    
alter table CRM_LC_INVENTORY_HIST 
        add constraint FK_i3iiwly1t85qohu5spass1dll 
        foreign key (INVENTORY_NO) 
        references CRM_LC_INVENTORY;

alter table CRM_LC_INVENTORY_HIST 
        add constraint FK_qd1qfmxu4wt7uodo56byx8128 
        foreign key (LOCATION) 
        references CRM_REF_LOOKUP_DTL;

alter table CRM_LC_INVENTORY_HIST 
        add constraint FK_kvax1frplpgu6s0a4x36wxiei 
        foreign key (ORIGIN) 
        references CRM_REF_LOOKUP_DTL;

alter table CRM_LC_INVENTORY_HIST 
        add constraint FK_bx847i8ev4vlfj8ymnracrpmb 
        foreign key (STATUS) 
        references CRM_REF_LOOKUP_DTL;