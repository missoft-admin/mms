package com.transretail.crm.report.template.impl;

import com.google.common.collect.ImmutableMap;

import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.lang3.tuple.Pair;
import org.joda.time.LocalDate;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.MessageSourceAware;
import org.springframework.context.support.MessageSourceAccessor;
import org.springframework.stereotype.Service;

import com.google.common.collect.Lists;
import com.transretail.crm.common.reporting.jasper.ClasspathFileResolver;
import com.transretail.crm.common.reporting.jasper.DefaultJRProcessor;
import com.transretail.crm.common.reporting.jasper.JRProcessor;
import com.transretail.crm.core.dto.StoreDto;
import com.transretail.crm.core.entity.enums.ReportType;
import com.transretail.crm.core.entity.lookup.LookupDetail;
import com.transretail.crm.core.entity.lookup.Store;
import com.transretail.crm.core.service.CodePropertiesService;
import com.transretail.crm.core.service.LookupService;
import com.transretail.crm.core.service.StoreService;
import com.transretail.crm.giftcard.dto.ProductProfileDto;
import com.transretail.crm.giftcard.dto.StockRequestDto;
import com.transretail.crm.giftcard.dto.StockRequestReceiveDto;
import com.transretail.crm.giftcard.entity.support.StockRequestStatus;
import com.transretail.crm.giftcard.service.ProductProfileService;
import com.transretail.crm.report.template.FilterField;

import org.springframework.util.Assert;

/**
 * @author ftopico
 */

@Service("giftCardTransferDocument")
public class GiftCardTransferDocument implements MessageSourceAware {
    
    @Autowired
    private StoreService storeService;
    @Autowired
    private ProductProfileService productProfileService;
    @Autowired
    private LookupService lookupService;
    @Autowired
    private CodePropertiesService codePropertiesService;

    protected static final String TEMPLATE_NAME = "Gift Card Inventory Transfer Document";
    protected static final String REPORT_NAME = "reports/gift_card_transfer_document.jasper";
    //Header Params
    private static final String PARAM_REQUEST_NO = "requestNo";
    private static final String PARAM_REQUEST_DATE = "requestDate";
    private static final String PARAM_REQUESTED_BY = "requestedBy";
    private static final String PARAM_SOURCE_LOCATION = "sourceLocation";
    private static final String PARAM_PRINT_DATE = "printDate";
    private static final DateTimeFormatter FULL_DATE_PATTERN = DateTimeFormat.forPattern("MMMM d, yyyy");
    private MessageSourceAccessor messageSource;

    public ReportType[] getReportTypes() {
        return ReportType.values();
    }
    
    @Override
    public void setMessageSource(MessageSource messageSource) {
        this.messageSource = new MessageSourceAccessor(messageSource);
    }
    
    public String getName() {
        return TEMPLATE_NAME;
    }
    
    public String getDescription() {
        return messageSource.getMessage("title_document_transfer");
    }
    
    public Set<FilterField> getFilters() {
        return null;
    }
    
    public JRProcessor createJrProcessor(StockRequestDto stockRequest) {
	Assert.notNull(stockRequest, "Parameter 'stockRequestDto' expected not null");

	Store sourceLocation =  storeService.getStoreByCode(stockRequest.getSourceLocation());
    if (sourceLocation == null) {
        LookupDetail headOffice = lookupService.getDetailByCode(stockRequest.getSourceLocation());
        sourceLocation = new Store();
        sourceLocation.setCode(headOffice.getCode());
        sourceLocation.setName(headOffice.getDescription());
    }
	Store allocateTo =  storeService.getStoreByCode(stockRequest.getAllocateTo());
	
	Map<String, Object> parameters = ImmutableMap.<String, Object>builder()
		.put(PARAM_REQUEST_NO, stockRequest.getRequestNo())
		.put(PARAM_REQUEST_DATE, FULL_DATE_PATTERN.print(stockRequest.getRequestDate()))
		.put(PARAM_REQUESTED_BY, stockRequest.getCreateUser())
		.put(PARAM_SOURCE_LOCATION, sourceLocation.getCodeAndName())
		.put(PARAM_PRINT_DATE, FULL_DATE_PATTERN.print(new LocalDate())).build();
	
	
	List<ReportBean> beansDataSource = Lists.newArrayList();
	
	ProductProfileDto productProfileDto = productProfileService.findProductProfileByCode(stockRequest.getProductCode());
	String product = productProfileDto.getProductCode() + " - " + productProfileDto.getProductDesc();
	Integer quantity;
	String series = new String();
	StockRequestStatus status;
    
    if (stockRequest.getReceiveDtos() != null && stockRequest.getStatus() == StockRequestStatus.IN_TRANSIT) {
        status = StockRequestStatus.TRANSFERRED_IN;
    } else {
        status = stockRequest.getStatus();
    }
    
	if (stockRequest.getReceiveDtos() == null) {
        for (Pair<String, String> pair : stockRequest.getReservedSeriesList()) {
            Long quantityLong = Long.valueOf(pair.getRight()) - Long.valueOf(pair.getLeft()) + 1; 
            quantity = quantityLong.intValue();
            series = pair.getLeft() + " - " + pair.getRight();
            beansDataSource.add(new ReportBean(series, quantity, allocateTo.getCodeAndName(), product, status.name()));
        }
    } else {
        for (StockRequestReceiveDto receiveDto : stockRequest.getReceiveDtos()) {
            quantity = receiveDto.getQuantity();
            series = String.valueOf(receiveDto.getStartingSeries()) + " - " + String.valueOf(receiveDto.getEndingSeries());
            beansDataSource.add(new ReportBean(series, quantity, allocateTo.getCodeAndName(), product, status.name()));
        }
    }
	
	DefaultJRProcessor jrProcessor = new DefaultJRProcessor(REPORT_NAME, beansDataSource);
	jrProcessor.addParameters(parameters);
	jrProcessor.setFileResolver(new ClasspathFileResolver("reports"));
	
	return jrProcessor;
    }
    
    public static class ReportBean {
        private String series;
        private Integer quantity;
        private String allocateTo;
        private String product;
        private String status;
        
        public ReportBean() {}
        
        public ReportBean(String series, Integer quantity, String allocateTo, String product, String status) {
            super();
            this.series = series;
            this.quantity = quantity;
            this.allocateTo = allocateTo;
            this.product = product;
            this.status = status;
        }

        public String getSeries() {
            return series;
        }

        public void setSeries(String series) {
            this.series = series;
        }

        public Integer getQuantity() {
            return quantity;
        }

        public String getAllocateTo() {
            return allocateTo;
        }

        public void setQuantity(Integer quantity) {
            this.quantity = quantity;
        }

        public void setAllocateTo(String allocateTo) {
            this.allocateTo = allocateTo;
        }

        public String getProduct() {
            return product;
        }

        public void setProduct(String product) {
            this.product = product;
        }

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }

    }
}
