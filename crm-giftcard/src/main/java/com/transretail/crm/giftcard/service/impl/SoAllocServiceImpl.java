package com.transretail.crm.giftcard.service.impl;


import java.util.*;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.validation.constraints.NotNull;

import com.google.common.collect.Sets;
import com.mysema.query.BooleanBuilder;
import com.mysema.query.types.Predicate;
import com.transretail.crm.giftcard.entity.*;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.google.common.collect.Lists;
import com.mysema.query.Tuple;
import com.mysema.query.jpa.JPASubQuery;
import com.mysema.query.jpa.JPQLQuery;
import com.mysema.query.jpa.impl.JPADeleteClause;
import com.mysema.query.jpa.impl.JPAQuery;
import com.mysema.query.jpa.impl.JPAUpdateClause;
import com.mysema.query.types.expr.BooleanExpression;
import com.transretail.crm.core.security.util.UserUtil;
import com.transretail.crm.core.service.StoreService;
import com.transretail.crm.giftcard.dto.GcAllocDto;
import com.transretail.crm.giftcard.dto.GiftCardInventoryDto;
import com.transretail.crm.giftcard.dto.SalesOrderDto;
import com.transretail.crm.giftcard.dto.SalesOrderItemDto;
import com.transretail.crm.giftcard.entity.support.GiftCardInventoryStatus;
import com.transretail.crm.giftcard.entity.support.SalesOrderStatus;
import com.transretail.crm.giftcard.repo.GiftCardInventoryRepo;
import com.transretail.crm.giftcard.repo.SalesOrderAllocRepo;
import com.transretail.crm.giftcard.repo.SalesOrderItemRepo;
import com.transretail.crm.giftcard.repo.SalesOrderRepo;
import com.transretail.crm.giftcard.service.SoAllocService;

import static com.transretail.crm.giftcard.entity.support.GiftCardInventoryStatus.*;


@Service( "soAllocService" )
public class SoAllocServiceImpl implements SoAllocService {

	@Autowired
	SalesOrderAllocRepo salesOrderAllocRepo;
	@Autowired
	SalesOrderRepo salesOrderRepo;
	@Autowired
	SalesOrderItemRepo salesOrderItemRepo;
	@Autowired
	GiftCardInventoryRepo giftCardInventoryRepo;

    @Autowired
    private StoreService storeService;

	@Autowired
    private MessageSource messageSource;

    @PersistenceContext
    private EntityManager em;
    @Value("#{'${hibernate.jdbc.batch_size}'}")
    private int batchSize;

	public static final String BARCODE_RANGE_SEPARATOR_VALUE = ":::";

	private static final Logger _log = LoggerFactory.getLogger(SoAllocServiceImpl.class);


	@Override
	public SalesOrderDto setSoItemsAllocsDto( Long soId ) {
		SalesOrderDto so = new SalesOrderDto( salesOrderRepo.findOne( soId ), true );
		if(StringUtils.isNotBlank( so.getPaymentStore() ) )
			so.setPaymentStoreName( storeService.getStoreByCode( so.getPaymentStore() ).getName() );
		for ( SalesOrderItemDto soItem : so.getItems() ) {
			QSalesOrderAlloc qSoa = QSalesOrderAlloc.salesOrderAlloc;
			QGiftCardInventory qGc = new QGiftCardInventory( "qGc" );
			JPQLQuery query = new JPAQuery( em ).from( qSoa, qGc )
				.where( qSoa.soItem.id.eq( soItem.getId() ).and( qGc.series.goe( qSoa.seriesStart ).and( qGc.series.loe( qSoa.seriesEnd ) ) ) );
			Long gcIds = query.singleResult( /*qSoa.gcInventory.id*/qGc.id.count() );

			if ( null != gcIds && gcIds.compareTo( 0L ) > 0 ) {
				QGiftCardInventory qGiftCardInventory = QGiftCardInventory.giftCardInventory;
				query = new JPAQuery( em )
					.from( qSoa, qGiftCardInventory )
					//.leftJoin( qSoa.gcInventory )
					.where( qSoa.soItem.id.eq( soItem.getId() ).and( qGiftCardInventory.series.goe( qSoa.seriesStart ).and( qGiftCardInventory.series.loe( qSoa.seriesEnd ) ) ) )
					.orderBy( qGiftCardInventory.series.asc() );

				/*Tuple tuple = query.singleResult( qGiftCardInventory.series.min(), qGiftCardInventory.series.max(), qGiftCardInventory.series.count() );
				GcAllocDto dto = new GcAllocDto();
				dto.setSeriesStart( tuple.get( qGiftCardInventory.series.min() ) );
				dto.setSeriesEnd( tuple.get( qGiftCardInventory.series.max() ) );
				dto.setQuantity( tuple.get( qGiftCardInventory.series.count() ) );
				Map<Long, String> seriesBarcode = query.where( qGiftCardInventory.series.in( dto.getSeriesStart(), dto.getSeriesEnd() ) )
					.map( qGiftCardInventory.series, qGiftCardInventory.barcode );
				dto.setBarcodeStart( seriesBarcode.get( dto.getSeriesStart() ) );
				dto.setBarcodeEnd( seriesBarcode.get( dto.getSeriesEnd() ) );
				soItem.setGcAlloc( dto );*/

				Iterator<Long> series = query.list( qGiftCardInventory.series ).iterator();
				List<GcAllocDto> gcAllocs = Lists.newArrayList();
				GcAllocDto gcAlloc = null;
				int cnt = 0;
				while( series.hasNext() ) {
					Long ser = series.next();
					if ( null != gcAlloc ) {
						if ( ( ser - 1 ) == gcAlloc.getSeriesEnd() ) {
							gcAlloc.setSeriesEnd( ser );
							gcAlloc.setQuantity( new Long( ++cnt ) );
							if ( !series.hasNext() ) {
								gcAllocs.add( gcAlloc );
							}
						}
						else {
							gcAllocs.add( gcAlloc );
							cnt = 0;
						}
					}

					if ( cnt == 0 ) {
						gcAlloc = new GcAllocDto();
						gcAlloc.setSeriesStart( ser );
						gcAlloc.setSeriesEnd( ser );
						gcAlloc.setQuantity( new Long( ++cnt ) );
						if ( !series.hasNext() ) {
							gcAllocs.add( gcAlloc );
						}
					}
				}
				for ( GcAllocDto gcAl : gcAllocs ) {
					Map<Long, String> barcodes = new JPAQuery( em )
						.from( qGiftCardInventory )
						.where( qGiftCardInventory.series.in( gcAl.getSeriesStart(), gcAl.getSeriesEnd() ) )
						.map( qGiftCardInventory.series, qGiftCardInventory.barcode );
					gcAl.setBarcodeStart( barcodes.get( gcAl.getSeriesStart() ) );
					gcAl.setBarcodeEnd( barcodes.get( gcAl.getSeriesEnd() ) );
				}
				soItem.setGcAllocs( gcAllocs );
			}
		}
		return so;
	}

	@Override
	public GcAllocDto getQuantity( String barcodeOrStart, String barcodeEnd ) {
		GcAllocDto dto = new GcAllocDto();

		QGiftCardInventory qInventory = QGiftCardInventory.giftCardInventory;
		if ( StringUtils.isNotBlank( barcodeOrStart ) && StringUtils.isNotBlank( barcodeEnd ) ) {
			Long seriesStart = NumberUtils.toLong( barcodeOrStart.substring( 0, barcodeOrStart.length() - 4 ) );
			Long seriesEnd = NumberUtils.toLong( barcodeEnd.substring( 0, barcodeEnd.length() - 4 ) );

			JPQLQuery query = new JPAQuery( em )
				.from( qInventory )
				.where( 
					qInventory.status.eq( IN_STOCK )
					.and( qInventory.series.goe( seriesStart ) )
					.and( qInventory.series.loe( seriesEnd ) ) );
			Tuple tuple = query.singleResult( qInventory.series.min(), qInventory.series.max(), qInventory.profile.id, qInventory.series.count() );
			dto.setSeriesStart( tuple.get( qInventory.series.min() ) );
			dto.setSeriesEnd( tuple.get( qInventory.series.max() ) );
			dto.setProductId( tuple.get( qInventory.profile.id ) );
			dto.setQuantity( tuple.get( qInventory.series.count() ) );
			Map<Long, String> seriesBarcode = query.where( qInventory.series.in( dto.getSeriesStart(), dto.getSeriesEnd() ) )
				.map( qInventory.series, qInventory.barcode );
			dto.setBarcodeStart( seriesBarcode.get( dto.getSeriesStart() ) );
			dto.setBarcodeEnd( seriesBarcode.get( dto.getSeriesEnd() ) );
		}
		else if ( StringUtils.isNotBlank( barcodeOrStart ) && StringUtils.isBlank( barcodeEnd ) ) {
			Long seriesStart = NumberUtils.toLong( barcodeOrStart.substring( 0, barcodeOrStart.length() - 4 ) );
			JPQLQuery query = new JPAQuery( em )
				.from( qInventory )
				.where( 
					qInventory.status.eq( IN_STOCK )
					.and( qInventory.series.eq( seriesStart ) ) );
			Tuple tuple = query.singleResult( qInventory.barcode, qInventory.profile.id );
			dto.setBarcode( tuple.get( qInventory.barcode ) );
			dto.setProductId( tuple.get( qInventory.profile.id ) );
			if ( StringUtils.isNotBlank( dto.getBarcode() ) ) {
				dto.setQuantity( 1L );
			}
		}

		return dto;
	}

	@Override
	@Transactional
	public GcAllocDto preallocate( Long soId, String barcodeOrStart, String barcodeEnd ) {
		GcAllocDto dto = new GcAllocDto();

		QGiftCardInventory qInventory = QGiftCardInventory.giftCardInventory;
		if ( StringUtils.isNotBlank( barcodeOrStart ) && StringUtils.isNotBlank( barcodeEnd ) ) {
			Long seriesStart = NumberUtils.toLong( barcodeOrStart.substring( 0, barcodeOrStart.length() - 4 ) );
			Long seriesEnd = NumberUtils.toLong( barcodeEnd.substring( 0, barcodeEnd.length() - 4 ) );

			BooleanExpression filter = BooleanExpression.allOf( 
					qInventory.status.eq( IN_STOCK )
					.and( qInventory.location.equalsIgnoreCase( UserUtil.getCurrentUser().getInventoryLocation() ) )
					.and( qInventory.series.goe( seriesStart ) )
					.and( qInventory.series.loe( seriesEnd ) ) );

			JPQLQuery query = new JPAQuery( em )
				.from( qInventory )
				.where( filter )
				.groupBy( qInventory.profile.id );

			Tuple tuple = query.singleResult( qInventory.series.min(), qInventory.series.max(), qInventory.profile.id, qInventory.series.count() );
			dto.setSeriesStart( tuple.get( qInventory.series.min() ) );
			dto.setSeriesEnd( tuple.get( qInventory.series.max() ) );
			dto.setProductId( tuple.get( qInventory.profile.id ) );
			dto.setQuantity( tuple.get( qInventory.series.count() ) );
			Map<Long, String> seriesBarcode = new JPAQuery( em )
				.from( qInventory )
				.where( qInventory.series.in( dto.getSeriesStart(), dto.getSeriesEnd() ) )
				.map( qInventory.series, qInventory.barcode );
			dto.setBarcodeStart( seriesBarcode.get( dto.getSeriesStart() ) );
			dto.setBarcodeEnd( seriesBarcode.get( dto.getSeriesEnd() ) );
			if ( null != dto.getSeriesStart() && null != dto.getSeriesEnd() ) {
				QSalesOrderItem qSalesOrderItem = QSalesOrderItem.salesOrderItem;
				SalesOrderItem soItem = salesOrderItemRepo.findOne( 
						qSalesOrderItem.product.id.in( dto.getProductId() )
						.and( qSalesOrderItem.order.id.in( soId ) ) );
				dto.setSoItemId( soItem.getId() );

		        /*JPAUpdateClause updateClause = new JPAUpdateClause( em, qInventory );
		        updateClause.set( qInventory.status, GiftCardInventoryStatus.PREALLOCATED );
		        updateClause.where( filter );
		        updateClause.execute();
		        em.clear();
		        em.flush();*/

//				List<SalesOrderAlloc> soAllocs = Lists.newArrayList();
				List<GiftCardInventory> gcs = new JPAQuery( em ).from( qInventory ).where( filter ).list( qInventory );

		    	/*int insertCounter = 0;
				for ( GiftCardInventory gc : gcs ) {
					gc.setStatus( GiftCardInventoryStatus.PREALLOCATED );
		    		if ( insertCounter < batchSize ) {
		    			giftCardInventoryRepo.save( gc );
		                insertCounter++;
		            }
		    		else {
		    			giftCardInventoryRepo.saveAndFlush( gc );
				        em.clear();
		                insertCounter = 0;
		            }


//					SalesOrderAlloc soAlloc = new SalesOrderAlloc();
//					soAlloc.setSoItem( soItem );
//					if ( gc.getSeries().longValue() == seriesStart.longValue() || gc.getSeries().longValue() == seriesEnd.longValue() ) {
//						soAlloc.setIsRange( true );
//					}

//					soAlloc.setGcInventory( gc );
//					soAllocs.add( soAlloc );
				}*/

				prosessBatch(gcs, PREALLOCATED);

				SalesOrderAlloc soAlloc = new SalesOrderAlloc();
				soAlloc.setSoItem( soItem );
				soAlloc.setSeriesStart( seriesStart );
				soAlloc.setSeriesEnd( seriesEnd );
    			salesOrderAllocRepo.saveAndFlush( soAlloc );
//				soAllocs.add( soAlloc );

//                insertCounter = 0;
//				for ( SalesOrderAlloc soAlloc : soAllocs ) {
//		    		if ( insertCounter < batchSize ) {
//		    			salesOrderAllocRepo.save( soAlloc );
//		                insertCounter++;
//		            } 
//		    		else {
//		    			salesOrderAllocRepo.saveAndFlush( soAlloc );
//				        em.clear();
//		                insertCounter = 0;
//		            }
//				}
			}
		}
		else if ( StringUtils.isNotBlank( barcodeOrStart ) && StringUtils.isBlank( barcodeEnd ) ) {
			Long seriesStart = NumberUtils.toLong( barcodeOrStart.substring( 0, barcodeOrStart.length() - 3 ) );
			JPQLQuery query = new JPAQuery( em )
				.from( qInventory )
				.where( 
					qInventory.status.eq( IN_STOCK )
					.and( qInventory.series.eq( seriesStart ) ) );
			Tuple tuple = query.singleResult( qInventory.barcode, qInventory.profile.id );
			if ( null != tuple ) {
				dto.setBarcode( tuple.get( qInventory.barcode ) );
			}
			if ( StringUtils.isNotBlank( dto.getBarcode() ) ) {
				dto.setQuantity( 1L );
				QSalesOrderItem qSalesOrderItem = QSalesOrderItem.salesOrderItem;
				SalesOrderItem soItem = salesOrderItemRepo.findOne( 
						qSalesOrderItem.product.id.in( tuple.get( qInventory.profile.id ) )
						.and( qSalesOrderItem.order.id.in( soId ) ) );
				dto.setSoItemId( soItem.getId() );
				SalesOrderAlloc soAlloc = new SalesOrderAlloc();
				soAlloc.setSoItem( soItem );
				soAlloc.setSeriesStart( seriesStart );
				soAlloc.setSeriesEnd( seriesStart );
//				soAlloc.setGcInventory( giftCardInventoryRepo.findOne( qInventory.barcode.equalsIgnoreCase( dto.getBarcode() ) ) );
//				soAlloc.setIsRange( true );
				salesOrderAllocRepo.save( soAlloc );
				GiftCardInventory gc = giftCardInventoryRepo.findOne( qInventory.barcode.equalsIgnoreCase( dto.getBarcode() ) );
				gc.setStatus( PREALLOCATED );
				giftCardInventoryRepo.save( gc );
			}
		}

		return dto;
	}

	@Override
	@Transactional
	public void cancelPreAllocation( Long soId, String[] barcodeRanges ) {
		QSalesOrderAlloc qSalesOrderAlloc = QSalesOrderAlloc.salesOrderAlloc;
		QGiftCardInventory qGc = QGiftCardInventory.giftCardInventory;

		List<BooleanExpression> expr = new ArrayList<BooleanExpression>();
		for ( String barcodeRange : barcodeRanges ) {
			String[] barcodes = StringUtils.split( barcodeRange, BARCODE_RANGE_SEPARATOR_VALUE );
			expr.add( qSalesOrderAlloc.seriesStart.goe( NumberUtils.toLong( barcodes[0].substring( 0, barcodes[0].length() - 4 ) ) )
					.and( qSalesOrderAlloc.seriesEnd.loe( NumberUtils.toLong( barcodes[1].substring( 0, barcodes[1].length() - 4 ) ) ) ) );
		}
		
		JPADeleteClause delete = new JPADeleteClause( em, qSalesOrderAlloc );
		delete
			.where( qSalesOrderAlloc.id.in( new JPASubQuery()
				.from( qSalesOrderAlloc )
				.where( qSalesOrderAlloc.soItem.order.id.eq( soId ).and( BooleanExpression.anyOf( expr.toArray( new BooleanExpression[expr.size()] ) ) ) )
				.list( qSalesOrderAlloc.id ) ) );
		delete.execute();
//		delete.where( qSalesOrderAlloc.soItem.order.id.eq( soId )
//				.and( BooleanExpression.anyOf( expr.toArray( new BooleanExpression[expr.size()] ) ) ) ).execute();

        em.flush();
		em.clear();

		expr = new ArrayList<BooleanExpression>();
		for ( String barcodeRange : barcodeRanges ) {
			String[] barcodes = StringUtils.split( barcodeRange, BARCODE_RANGE_SEPARATOR_VALUE );
			expr.add( qGc.series.goe( NumberUtils.toLong( barcodes[0].substring( 0, barcodes[0].length() - 4 ) ) )
					.and( qGc.series.loe( NumberUtils.toLong( barcodes[1].substring( 0, barcodes[1].length() - 4 ) ) ) ) );
		}
		JPAUpdateClause update = new JPAUpdateClause( em, qGc );
		update.set( qGc.status, IN_STOCK )
			.where( qGc.id.in( new JPASubQuery()
				.from( qGc )
				.where( qGc.status.eq( PREALLOCATED ).and( BooleanExpression.anyOf( expr.toArray( new BooleanExpression[expr.size()] ) ) ) )
				.list( qGc.id ) ) );
		update.execute();


        em.flush();
		em.clear();
	}

	@Override
	public boolean isValidAlloc( Long soId, String barcodeOrStart, String barcodeEnd, List<String> errs, GcAllocDto dto ) {
		dto = null;

		if ( StringUtils.isNotBlank( barcodeOrStart ) && StringUtils.isNotBlank( barcodeEnd )
				|| StringUtils.isNotBlank( barcodeOrStart ) && StringUtils.isBlank( barcodeEnd ) ) {

			Long seriesStart = null;
			Long seriesEnd = null;
			if ( StringUtils.isNotBlank( barcodeOrStart ) && StringUtils.isNotBlank( barcodeEnd ) ) {
				seriesStart = NumberUtils.toLong( barcodeOrStart.substring( 0, barcodeOrStart.length() - 4 ) );
				seriesEnd = NumberUtils.toLong( barcodeEnd.substring( 0, barcodeEnd.length() - 4 ) );
			}

			QGiftCardInventory qInventory = QGiftCardInventory.giftCardInventory;
			BooleanExpression filter = BooleanExpression.allOf( 
					qInventory.status.eq( IN_STOCK ),
					qInventory.location.equalsIgnoreCase( UserUtil.getCurrentUser().getInventoryLocation() ),
					null != seriesStart? qInventory.series.goe( seriesStart ) : null,
					null != seriesEnd? qInventory.series.loe( seriesEnd ) : null,
					null == seriesStart && null == seriesEnd? qInventory.barcode.equalsIgnoreCase( barcodeOrStart ) : null );
			JPQLQuery query = new JPAQuery( em )
				.from( qInventory )
				.where( filter )
				.groupBy( qInventory.profile.id );
			Tuple tuple = query.singleResult( qInventory.profile.id, qInventory.id.count() );
			Long profileId = null != tuple? tuple.get( qInventory.profile.id ) : null;
			if ( null == profileId ) {
				errs.add( messageSource.getMessage( "gc.so.err.barocderange.invalid", null, LocaleContextHolder.getLocale() ) ); //gc.so.err.prodprof.invalid
				return false;
			}

			QSalesOrderItem qSalesOrderItem = QSalesOrderItem.salesOrderItem;
			SalesOrderItem soItem = salesOrderItemRepo.findOne( 
					qSalesOrderItem.product.id.eq( profileId )
					.and( qSalesOrderItem.order.id.eq( soId ) ) );
			if ( null == soItem ) {
				errs.add( messageSource.getMessage( "gc.so.err.prodprof.invalid", null, LocaleContextHolder.getLocale() ) );
				return false;
			}

			Long gcCount = null != tuple? tuple.get( qInventory.id.count() ) : null;
			QSalesOrderAlloc qSoAlloc = QSalesOrderAlloc.salesOrderAlloc;
			Long soAllocCt = new JPAQuery( em )
				.from( qSoAlloc ).where( qSoAlloc.soItem.id.eq( soItem.getId() ) )
				.singleResult( qSoAlloc.seriesEnd.add( 1l ).subtract( qSoAlloc.seriesStart ).sum() );
					//salesOrderAllocRepo.count( qSoAlloc.soItem.id.eq( soItem.getId() ) );
			if ( null != gcCount ) {
				if ( gcCount.compareTo( soItem.getQuantity() - ( null != soAllocCt? soAllocCt : 0 ) ) > 0 ) {
					errs.add( messageSource.getMessage( "gc.so.err.range.exceed", null, LocaleContextHolder.getLocale() ) );
					return false;
				}
			}

			if ( null != soAllocCt && soItem.getQuantity().compareTo( soAllocCt ) == 0 ) {
				errs.add( messageSource.getMessage( "gc.so.err.barocderange.prodprofile.allocated", null, LocaleContextHolder.getLocale() ) );
				return false;
			}

			dto = new GcAllocDto();
			tuple = query.singleResult( qInventory.series.min(), qInventory.series.max(), qInventory.profile.id, qInventory.series.count() );
			dto.setSeriesStart( tuple.get( qInventory.series.min() ) );
			dto.setSeriesEnd( tuple.get( qInventory.series.max() ) );
			dto.setProductId( tuple.get( qInventory.profile.id ) );
			dto.setQuantity( tuple.get( qInventory.series.count() ) );
			Map<Long, String> seriesBarcode = new JPAQuery( em )
				.from( qInventory )
				.where( qInventory.series.in( dto.getSeriesStart(), dto.getSeriesEnd() ) )
				.map( qInventory.series, qInventory.barcode );
			dto.setBarcodeStart( seriesBarcode.get( dto.getSeriesStart() ) );
			dto.setBarcodeEnd( seriesBarcode.get( dto.getSeriesEnd() ) );

			return true;
		}
	
		return false;
	}

	@Override
	public void saveAllocation( Long soId ) {
		QSalesOrderAlloc qSalesOrderAlloc = QSalesOrderAlloc.salesOrderAlloc;
		QGiftCardInventory qGc = QGiftCardInventory.giftCardInventory;
		JPQLQuery query = new JPAQuery( em ).from( qSalesOrderAlloc, qGc )
				.where( qSalesOrderAlloc.soItem.order.id.eq( soId )
						.and( qGc.series.goe( qSalesOrderAlloc.seriesStart )
						.and( qGc.series.loe( qSalesOrderAlloc.seriesEnd ) ) ) );
		List<GiftCardInventory> gcs = query.list( qGc );

		prosessBatch(gcs, ALLOCATED);
		//giftCardInventoryRepo.save( gcs );

		SalesOrder so = salesOrderRepo.findOne( soId );
		if ( CollectionUtils.isNotEmpty( gcs ) ) {
			int itemCnt = 0;
			for ( SalesOrderItem item : so.getItems() ) {
				itemCnt += null != item.getQuantity()? item.getQuantity() : 0 ;
			}
			if ( itemCnt > gcs.size() ) {
				so.setStatus( SalesOrderStatus.PARTIALLY_ALLOCATED );
			}
			else if ( itemCnt == gcs.size() ) {
				so.setStatus( SalesOrderStatus.ALLOCATED );
			}
		}
		salesOrderRepo.save( so );
	}

	@Override
	public void removeAllocation( Long soId ) {
		QSalesOrderAlloc qSalesOrderAlloc = QSalesOrderAlloc.salesOrderAlloc;
		QGiftCardInventory qGc = QGiftCardInventory.giftCardInventory;
		JPQLQuery query = new JPAQuery( em ).from( qSalesOrderAlloc, qGc )
				.where( qSalesOrderAlloc.soItem.order.id.eq( soId ).and( qGc.series.goe( qSalesOrderAlloc.seriesStart ).and( qGc.series.loe( qSalesOrderAlloc.seriesEnd ) ) ) );
		List<GiftCardInventory> gcs = query.list( qGc );
//		JPAQuery query = new JPAQuery( em )
//			.from( qSalesOrderAlloc )
//			.where( qSalesOrderAlloc.soItem.order.id.eq( soId ) );
//		List<GiftCardInventory> gcs = query.list( qSalesOrderAlloc.gcInventory );

		prosessBatch(gcs, IN_STOCK);
		//giftCardInventoryRepo.save( gcs );

		Set<SalesOrderAlloc> allocs = Sets.newConcurrentHashSet(query.list( qSalesOrderAlloc ) );
		salesOrderAllocRepo.delete(allocs);
	}

	@Transactional(readOnly = true)
	@Override
	public List<GiftCardInventory> getAllocatedGcs( Long soId ) {
		QSalesOrderAlloc qSalesOrderAlloc = QSalesOrderAlloc.salesOrderAlloc;
		QGiftCardInventory qGc = QGiftCardInventory.giftCardInventory;
		JPQLQuery query = new JPAQuery( em ).from( qSalesOrderAlloc, qGc )
				.where( qSalesOrderAlloc.soItem.order.id.eq( soId ).and( qGc.series.goe( qSalesOrderAlloc.seriesStart ).and( qGc.series.loe( qSalesOrderAlloc.seriesEnd ) ) ) );
		return query.list( qGc );
//		JPAQuery query = new JPAQuery( em )
//			.from( qSalesOrderAlloc )
//			.where( qSalesOrderAlloc.soItem.order.id.eq( soId ) );
//		return query.list( qSalesOrderAlloc.gcInventory );
	}

	@Override
	public GiftCardInventoryDto getGiftCard( String barcode ) {
		if ( StringUtils.isBlank( barcode ) ) {
			return null;
		}
		GiftCardInventory gc = giftCardInventoryRepo.findOne( QGiftCardInventory.giftCardInventory.barcode.equalsIgnoreCase( barcode ) );
		return null != gc? new GiftCardInventoryDto( gc ) : null;
	}

	@Override
	public List<GcAllocDto> autoScanAndValidate(Long soId) {

		QSalesOrderItem qSalesOrderItem = QSalesOrderItem.salesOrderItem;
		QGiftCardInventory qGiftCardInventory = QGiftCardInventory.giftCardInventory;

		List<GcAllocDto> dto = Lists.newArrayList();

		final List<SalesOrderItem> orderItem = new JPAQuery(em).from(qSalesOrderItem)
				.where(qSalesOrderItem.order.id.eq(soId)).list(qSalesOrderItem);

		BooleanBuilder builder = new BooleanBuilder();

		for (SalesOrderItem item : orderItem) {

            builder.and(qGiftCardInventory.profile.eq(item.getProduct()))
                    .and(qGiftCardInventory.status.eq(IN_STOCK));

			final String startBarcode = new JPAQuery(em).from(qGiftCardInventory)
					.where(builder)
					.singleResult(qGiftCardInventory.barcode.min());
			_log.warn("start barcode :: " + startBarcode);

			final long startSeriesNeed = Long.valueOf(startBarcode != null ? startBarcode.substring(0, 16) : "") + (item.getQuantity() - 1);
			_log.warn("start series :: " + startSeriesNeed);

			final String endBarcode = new JPAQuery(em).from(qGiftCardInventory)
                    .where(builder.and(qGiftCardInventory.series.eq(startSeriesNeed)))
                    .singleResult(qGiftCardInventory.barcode.min());
			_log.warn("end barcode :: " + endBarcode);

			GcAllocDto allocDto = new GcAllocDto();
			allocDto.setBarcodeStart(startBarcode);
			allocDto.setBarcodeEnd(endBarcode);
			dto.add(allocDto);
		}

		return dto;
	}

	private void prosessBatch(@NotNull List<GiftCardInventory> inventories, GiftCardInventoryStatus status) {

		for (int i = 0; i < inventories.size(); i++) {

			GiftCardInventory inventory = inventories.get(i);
			inventory.setStatus(status);
			giftCardInventoryRepo.save(inventory);
			if (i % batchSize == 0) {
				giftCardInventoryRepo.flush();
				em.clear();
			}
		}
	}
}