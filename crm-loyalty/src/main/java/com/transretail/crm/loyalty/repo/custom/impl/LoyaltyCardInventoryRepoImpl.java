package com.transretail.crm.loyalty.repo.custom.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.BooleanUtils;
import org.apache.commons.lang3.StringUtils;
import org.hibernate.StatelessSession;
import org.joda.time.DateTime;
import org.joda.time.LocalDate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.transaction.annotation.Transactional;

import com.google.common.collect.Lists;
import com.google.common.collect.Sets;
import com.mysema.query.BooleanBuilder;
import com.mysema.query.Tuple;
import com.mysema.query.jpa.JPQLQuery;
import com.mysema.query.jpa.hibernate.HibernateUpdateClause;
import com.mysema.query.jpa.impl.JPAQuery;
import com.mysema.query.jpa.impl.JPAUpdateClause;
import com.mysema.query.types.Order;
import com.mysema.query.types.OrderSpecifier;
import com.mysema.query.types.expr.BooleanExpression;
import com.mysema.query.types.expr.NumberExpression;
import com.transretail.crm.common.service.dto.request.PagingParam;
import com.transretail.crm.core.entity.MemberModel;
import com.transretail.crm.core.entity.QMemberModel;
import com.transretail.crm.core.entity.lookup.LookupDetail;
import com.transretail.crm.core.entity.lookup.QLookupDetail;
import com.transretail.crm.core.security.model.impl.CustomSecurityUserDetailsImpl;
import com.transretail.crm.core.security.util.UserUtil;
import com.transretail.crm.core.service.CodePropertiesService;
import com.transretail.crm.core.service.LookupService;
import com.transretail.crm.loyalty.dto.AllocateLoyaltyCardsDto;
import com.transretail.crm.loyalty.dto.AllocationDto;
import com.transretail.crm.loyalty.dto.LoyaltyCardInventoryListDto;
import com.transretail.crm.loyalty.dto.LoyaltyCardInventorySearchDto;
import com.transretail.crm.loyalty.dto.LoyaltyCardInventorySummaryResultList;
import com.transretail.crm.loyalty.dto.ManufactureOrderReceiveDto;
import com.transretail.crm.loyalty.dto.ProductSeriesDto;
import com.transretail.crm.loyalty.dto.QAllocationDto;
import com.transretail.crm.loyalty.dto.QLoyaltyCardInventoryListDto;
import com.transretail.crm.loyalty.dto.QManufactureOrderReceiveDto;
import com.transretail.crm.loyalty.dto.QProductSeriesDto;
import com.transretail.crm.loyalty.dto.ReceiveOrderDto;
import com.transretail.crm.loyalty.entity.LoyaltyCardInventory;
import com.transretail.crm.loyalty.entity.QLoyaltyCardInventory;
import com.transretail.crm.loyalty.entity.QLoyaltyCardInventoryHistory;
import com.transretail.crm.loyalty.entity.QManufactureOrderTier;
import com.transretail.crm.loyalty.repo.custom.LoyaltyCardInventoryRepoCustom;
import com.transretail.crm.loyalty.service.LoyaltyCardHistoryService;
import com.transretail.crm.loyalty.service.LoyaltyCardService;

public class LoyaltyCardInventoryRepoImpl implements LoyaltyCardInventoryRepoCustom {
	@PersistenceContext
	EntityManager em;
	
	@Autowired
	CodePropertiesService codePropertiesService;
	@Autowired
	LookupService lookupService;
	@Autowired
    StatelessSession statelessSession;
	@Autowired
	LoyaltyCardService loyaltyCardService;
	@Autowired
	LoyaltyCardHistoryService loyaltyCardHistoryService;
	@Value("#{'${hibernate.jdbc.batch_size}'}")
    private int batchSize;
	
	static Logger logger = LoggerFactory.getLogger(LoyaltyCardInventoryRepoImpl.class);
	
	private String getCurrentUserLocation() {
		CustomSecurityUserDetailsImpl userDetails = UserUtil.getCurrentUser();
		return userDetails.getInventoryLocation();
	}
	
	@Override
	public LoyaltyCardInventorySummaryResultList searchLoyaltyCardInventories(LoyaltyCardInventorySearchDto searchDto) {
		QLoyaltyCardInventory qInventory = QLoyaltyCardInventory.loyaltyCardInventory;
		QLookupDetail qStatus = new QLookupDetail("status");
		QManufactureOrderTier qProduct = new QManufactureOrderTier("product");
		QLookupDetail qProductDtl = new QLookupDetail("productDtl");
		
		JPAQuery jpaQuery = new JPAQuery(em);
		
		String userLocation = getCurrentUserLocation();

		BooleanExpression locationFilter = null;
		if(userLocation != null)
			if ( userLocation.equalsIgnoreCase( codePropertiesService.getDetailInvLocationHeadOffice() ) )
				locationFilter = qInventory.location.isNotNull()
					.or(qInventory.location.isNull())
					.or(qInventory.status.code.eq(codePropertiesService.getDetailLoyaltyStatusInTransit()))
					.or(qInventory.status.code.eq(codePropertiesService.getDetailLoyaltyStatusInTransit()));
			else
				locationFilter = qInventory.location.eq(userLocation)
					.or(qInventory.status.code.eq(codePropertiesService.getDetailLoyaltyStatusAllocated()).and(qInventory.transferTo.eq(userLocation)))
					.or(qInventory.status.code.eq(codePropertiesService.getDetailLoyaltyStatusInTransit()).and(qInventory.transferTo.eq(userLocation)))
					.or(qInventory.newTransferTo.eq(userLocation));
		else
			locationFilter = qInventory.location.isNull()
				.or(qInventory.status.code.eq(codePropertiesService.getDetailLoyaltyStatusAllocated()).and(qInventory.transferTo.isNull()))
				.or(qInventory.status.code.eq(codePropertiesService.getDetailLoyaltyStatusInTransit()).and(qInventory.transferTo.isNull()));
		

		PagingParam pagination = searchDto.getPagination();
		
		BooleanBuilder burnedFilter = new BooleanBuilder();
		if(StringUtils.isBlank(searchDto.getStatus()) || !searchDto.getStatus().contains(codePropertiesService.getDetailLoyaltyStatusBurned())) {
			burnedFilter.and(qInventory.status.code.ne(codePropertiesService.getDetailLoyaltyStatusBurned()));
		}
		
		List<BooleanExpression> expressions = new ArrayList<BooleanExpression>();
		if(StringUtils.isNotBlank(searchDto.getStatus())) {
			if(StringUtils.isNotBlank(searchDto.getStatus())) {
				String[] stat = searchDto.getStatus().split(",");
				for(String statusCode : stat) {
					if(!statusCode.equals("changeAllocation") && !statusCode.equals("forBurning"))
						expressions.add(qInventory.status.code.eq(statusCode));
					else if(statusCode.equals("changeAllocation"))
						expressions.add(qInventory.newTransferTo.isNotNull());
					else if(statusCode.equals("forBurning"))
						expressions.add(qInventory.forBurning.isTrue());		
				}
			}
		}
		BooleanExpression statusFilter = BooleanExpression.anyOf(expressions.toArray(new BooleanExpression[expressions.size()]));
		
		JPQLQuery query = jpaQuery
				.from(qInventory)
				.where(locationFilter.and(searchDto.createSearchExpression()).and(burnedFilter).and(statusFilter))
				.leftJoin(qInventory.status, qStatus)
				.leftJoin(qInventory.product, qProduct)
				.leftJoin(qInventory.product.name, qProductDtl)
				.groupBy(qProductDtl.code, qStatus.code, qInventory.location, qInventory.transferTo, qInventory.newTransferTo, qInventory.forBurning,
						qProductDtl.description, qStatus.description);
		
		/*Long totalElements = query.singleResult(qProductDtl.code.count());*/
		
		Long totalElements = new Long(query.list(new QLoyaltyCardInventoryListDto(qProductDtl.description, qProductDtl.code,
				qStatus.description, qStatus.code,
				qInventory.transferTo,
				qInventory.location,
				qInventory.newTransferTo,
				qInventory.forBurning,
				qInventory.count())).size());

		JPQLQuery selectQuery = applyPagination(query, pagination, qProductDtl);

		List<LoyaltyCardInventoryListDto> list = selectQuery
				.list(new QLoyaltyCardInventoryListDto(qProductDtl.description, qProductDtl.code,
						qStatus.description, qStatus.code,
						qInventory.transferTo,
						qInventory.location,
						qInventory.newTransferTo,
						qInventory.forBurning,
						qInventory.count()));
		
		for(LoyaltyCardInventoryListDto dto : list) {
			if(StringUtils.isNotBlank(dto.getLocationCode()))
				dto.setLocation(loyaltyCardService.getInventoryDesc(dto.getLocationCode()));
			if(StringUtils.isNotBlank(dto.getTransferToCode()))
				dto.setTransferTo(loyaltyCardService.getInventoryDesc(dto.getTransferToCode()));
			if(StringUtils.isNotBlank(dto.getNewTransferToCode()))
				dto.setNewTransferTo(loyaltyCardService.getInventoryDesc(dto.getNewTransferToCode()));
			dto.setLastUpdateDate(getLastUpdate(dto));
		}

		return new LoyaltyCardInventorySummaryResultList(list,
				totalElements != null ? totalElements : 0,
				pagination.getPageNo(), pagination.getPageSize());
	}
	
	private DateTime getLastUpdate(LoyaltyCardInventoryListDto dto) {
		QLoyaltyCardInventoryHistory qHistory = QLoyaltyCardInventoryHistory.loyaltyCardInventoryHistory;
		BooleanBuilder filter = new BooleanBuilder(qHistory.inventory.product.name.code.eq(dto.getProductId()))
			.and(qHistory.inventory.status.code.eq(dto.getStatusCode()));
		if(StringUtils.isBlank(dto.getTransferToCode()))
			filter.and(qHistory.inventory.transferTo.isNull());
		else
			filter.and(qHistory.inventory.transferTo.eq(dto.getTransferToCode()));
		if(StringUtils.isBlank(dto.getLocationCode()))
			filter.and(qHistory.inventory.location.isNull());
		else
			filter.and(qHistory.inventory.location.eq(dto.getLocationCode()));
		return new JPAQuery(em).from(qHistory).where(filter).singleResult(qHistory.created.max());
	}
	
	@SuppressWarnings({"rawtypes", "unchecked"})
	public JPQLQuery applyPagination(JPQLQuery query, PagingParam pagination, QLookupDetail qProductDtl) {
        if (pagination != null && pagination.getPageSize() >= 0 && pagination.getPageNo() >= 0) {
            query.offset(pagination.getPageNo() * pagination.getPageSize());
            query.limit(pagination.getPageSize());
            if (pagination.getOrder() != null) {
                for (Map.Entry<String, Boolean> entry : pagination.getOrder().entrySet()) {
                    query.orderBy(new OrderSpecifier(entry.getValue() ? Order.ASC
                        : Order.DESC, qProductDtl.code));
                }
            }
        }
        return query;
    }
	
	@Override
	@Transactional
	public long receiveOrders(ReceiveOrderDto orderDto) {
		QLoyaltyCardInventory qLoyaltyCardInventory = QLoyaltyCardInventory.loyaltyCardInventory;
		String receiptNo = orderDto.getReceiptNo();
		LocalDate receiptDate = orderDto.getReceiveDate();
		String receivedAt = orderDto.getReceivedAt();
		LookupDetail inactive = new LookupDetail(codePropertiesService.getDetailLoyaltyStatusInactive());
		NumberExpression<Long> seriesExp = qLoyaltyCardInventory.sequence.castToNum(Long.class);
		long results = 0;
		for(ProductSeriesDto productDto : orderDto.getProducts()) {
			Long starting = Long.valueOf(productDto.getStartingSeries());
			Long ending = Long.valueOf(productDto.getEndingSeries());
			JPAUpdateClause clause = new JPAUpdateClause( em, qLoyaltyCardInventory )
		        .set(qLoyaltyCardInventory.receiptNo, receiptNo)
		        .set(qLoyaltyCardInventory.receiveDate, receiptDate)
		        .set(qLoyaltyCardInventory.receivedAt, receivedAt)
		        .set(qLoyaltyCardInventory.location, receivedAt)
		        .set(qLoyaltyCardInventory.status, inactive)
				.set(qLoyaltyCardInventory.boxNo, productDto.getBoxNo());
			List<BooleanExpression> expressions = new ArrayList<BooleanExpression>();
			expressions.add(seriesExp.goe(starting).and(seriesExp.loe(ending)));
			results += clause.where(BooleanExpression.anyOf(expressions.toArray(new BooleanExpression[expressions.size()])).and(qLoyaltyCardInventory.status.isNull())).execute();
		}
		return results;
	}
	
	@Override
	@Transactional
	public long expireInventories(LocalDate dateToExpire) {
		QLoyaltyCardInventory qLoyaltyCardInventory = QLoyaltyCardInventory.loyaltyCardInventory;
		
		BooleanExpression filters = qLoyaltyCardInventory.status.code.eq(codePropertiesService.getDetailLoyaltyStatusActive()).and(
        		qLoyaltyCardInventory.expiryDate.eq(dateToExpire));
		
		List<LoyaltyCardInventory> inventories = new JPAQuery(em).from(qLoyaltyCardInventory).where(filters).list(qLoyaltyCardInventory);
		
		loyaltyCardHistoryService.saveHistoryForInventories(new LookupDetail(codePropertiesService.getDetailLoyaltyStatusDisabled()), inventories, true);
		
		return new HibernateUpdateClause(statelessSession, qLoyaltyCardInventory)
	        .set(qLoyaltyCardInventory.status, new LookupDetail(codePropertiesService.getDetailLoyaltyStatusDisabled()))
	        .where(filters)
	        .execute();
	}
	
	@Override
	public Map<String, List<String>> getEmailsWithExpiry(LocalDate dateToExpire) {
		QLoyaltyCardInventory qInventory = QLoyaltyCardInventory.loyaltyCardInventory;
		QMemberModel qMember = QMemberModel.memberModel;
		
		List<String> expiredBarcodes = new JPAQuery(em).from(qInventory).where(qInventory.status.code.eq(codePropertiesService.getDetailLoyaltyStatusActive())
				.and(qInventory.expiryDate.eq(dateToExpire)))
				.list(qInventory.barcode);
		
		Map<String, List<String>> contacts = new HashMap<String, List<String>>();
		contacts.put("email", new ArrayList<String>());
		contacts.put("sms", new ArrayList<String>());
		
		if(CollectionUtils.isNotEmpty(expiredBarcodes)) {
			BooleanExpression filter = qMember.loyaltyCardNo.in(expiredBarcodes);
			contacts.get("email").addAll(new JPAQuery(em).from(qMember)
					.where(filter.and(qMember.email.isNotEmpty()).and(qMember.channel.acceptEmail.isTrue()))
					.list(qMember.email));
			contacts.get("sms").addAll(new JPAQuery(em).from(qMember)
					.where(filter.and(qMember.contact.isNotEmpty()).and(qMember.channel.acceptSMS.isTrue()))
					.list(qMember.contact));	
		}

		return contacts;
	}

	
	@Override
	public List<MemberModel> getMembersWithExpiry(LocalDate dateToExpire) {
		QLoyaltyCardInventory qInventory = QLoyaltyCardInventory.loyaltyCardInventory;
		QMemberModel qMember = QMemberModel.memberModel;
		return new JPAQuery(em).from(qInventory, qMember).where(qMember.loyaltyCardNo.eq(qInventory.barcode).and(qInventory.expiryDate.eq(dateToExpire))).list(qMember);
	}
	
	
	@Override
	@Transactional
	public long allocateInventory(AllocateLoyaltyCardsDto dto, LookupDetail newStatus, LookupDetail oldStatus) {
		QLoyaltyCardInventory qLoyaltyCardInventory = QLoyaltyCardInventory.loyaltyCardInventory;
		
		HibernateUpdateClause clause = new HibernateUpdateClause(statelessSession, qLoyaltyCardInventory)
        .set(qLoyaltyCardInventory.transferTo, dto.getAllocateTo())
        .set(qLoyaltyCardInventory.location, dto.getLocation())
        .set(qLoyaltyCardInventory.referenceNo, dto.getReferenceNo())
        .set(qLoyaltyCardInventory.allocateDate, dto.getAllocateDate())
        .set(qLoyaltyCardInventory.encodedBy, dto.getEncodedBy())
        .set(qLoyaltyCardInventory.status, newStatus);

		List<BooleanExpression> expressions = new ArrayList<BooleanExpression>();
		NumberExpression<Long> seriesExp = qLoyaltyCardInventory.sequence.castToNum(Long.class);
		
		for(ProductSeriesDto productDto : dto.getProducts()) {
			Long starting = Long.valueOf(productDto.getStartingSeries());
			Long ending = Long.valueOf(productDto.getEndingSeries());
			expressions.add(seriesExp.goe(starting).and(seriesExp.loe(ending)).and(qLoyaltyCardInventory.status.eq(oldStatus)).and(qLoyaltyCardInventory.location.eq(dto.getLocation())));
		}
		return clause.where(BooleanExpression.anyOf(expressions.toArray(new BooleanExpression[expressions.size()]))).execute();
	}
	
	@Override
	@Transactional
	public long saveInventory(AllocateLoyaltyCardsDto dto, LookupDetail newStatus, LookupDetail oldStatus) {
		QLoyaltyCardInventory qLoyaltyCardInventory = QLoyaltyCardInventory.loyaltyCardInventory;
		
		HibernateUpdateClause clause = new HibernateUpdateClause(statelessSession, qLoyaltyCardInventory)
        .set(qLoyaltyCardInventory.transferTo, dto.getAllocateTo())
        .set(qLoyaltyCardInventory.location, dto.getLocation())
        .set(qLoyaltyCardInventory.status, newStatus);
		
		if(newStatus.getCode().equalsIgnoreCase(codePropertiesService.getDetailLoyaltyStatusInactive()))
			clause.setNull(qLoyaltyCardInventory.referenceNo);

		List<BooleanExpression> expressions = new ArrayList<BooleanExpression>();
		NumberExpression<Long> seriesExp = qLoyaltyCardInventory.sequence.castToNum(Long.class);
		
		for(ProductSeriesDto productDto : dto.getProducts()) {
			Long starting = Long.valueOf(productDto.getStartingSeries());
			Long ending = Long.valueOf(productDto.getEndingSeries());
			expressions.add(seriesExp.goe(starting).and(seriesExp.loe(ending)).and(qLoyaltyCardInventory.status.eq(oldStatus)).and(qLoyaltyCardInventory.referenceNo.eq(dto.getReferenceNo())));
		}
		return clause.where(BooleanExpression.anyOf(expressions.toArray(new BooleanExpression[expressions.size()]))).execute();
	}
	
	@Override
	@Transactional
	public long saveInventory(String referenceNo, LookupDetail newStatus) {
		QLoyaltyCardInventory qLoyaltyCardInventory = QLoyaltyCardInventory.loyaltyCardInventory;
		
		HibernateUpdateClause clause = new HibernateUpdateClause(statelessSession, qLoyaltyCardInventory)
		.set(qLoyaltyCardInventory.lastUpdateUser, UserUtil.getCurrentUser().getUsername())
		.set(qLoyaltyCardInventory.lastUpdated, new DateTime()) //temp fix
        .set(qLoyaltyCardInventory.status, newStatus);
		
		if(newStatus.getCode().equalsIgnoreCase(codePropertiesService.getDetailLoyaltyStatusInactive()) ||
				newStatus.getCode().equalsIgnoreCase(codePropertiesService.getDetailLoyaltyStatusTransferIn()))
			clause.setNull(qLoyaltyCardInventory.referenceNo)
			.setNull(qLoyaltyCardInventory.transferTo)
			.setNull(qLoyaltyCardInventory.batchRefNo);
		
		return clause.where(qLoyaltyCardInventory.referenceNo.eq(referenceNo)).execute();
	}
	
	@Override
	@Transactional
	public long approveBurnInventory(String referenceNo, Boolean approved) {
		QLoyaltyCardInventory qLoyaltyCardInventory = QLoyaltyCardInventory.loyaltyCardInventory;
		
		HibernateUpdateClause clause = new HibernateUpdateClause(statelessSession, qLoyaltyCardInventory)
			.setNull(qLoyaltyCardInventory.forBurning);
		
		LookupDetail status = new LookupDetail(codePropertiesService.getDetailLoyaltyStatusBurned());
		
		if(BooleanUtils.isTrue(approved)) {
			clause.set(qLoyaltyCardInventory.lastUpdateUser, UserUtil.getCurrentUser().getUsername())
			.set(qLoyaltyCardInventory.lastUpdated, new DateTime()) //temp fix
			.set(qLoyaltyCardInventory.status, status);	
		} else {
			clause.setNull(qLoyaltyCardInventory.encodedBy)
			.setNull(qLoyaltyCardInventory.allocateDate);
		}
		
		return clause.where(qLoyaltyCardInventory.referenceNo.eq(referenceNo)).execute();
	}
	
	@Override
	public Object[] getInventories(LookupDetail cardType, LookupDetail status, String location, String transferTo, String newTransferTo) {
		QLoyaltyCardInventory qInventory = QLoyaltyCardInventory.loyaltyCardInventory;
		
		List<BooleanExpression> filters = Lists.newArrayList();
		if(cardType != null) 
			filters.add(qInventory.product.name.eq(cardType));
		else
			filters.add(qInventory.product.name.isNull());
		if(status != null)
			filters.add(qInventory.status.eq(status));
		else
			filters.add(qInventory.status.isNull());
		if(location != null)
			filters.add(qInventory.location.eq(location));
		else
			filters.add(qInventory.location.isNull());
		if(transferTo != null)
			filters.add(qInventory.transferTo.eq(transferTo));
		else
			filters.add(qInventory.transferTo.isNull());
		if(newTransferTo != null)
			filters.add(qInventory.newTransferTo.eq(newTransferTo));
		else
			filters.add(qInventory.newTransferTo.isNull());
		BooleanExpression filter = BooleanExpression.allOf(filters.toArray(new BooleanExpression[filters.size()]));
		
		JPQLQuery query = new JPAQuery(em)
			.from(qInventory)
			.where(filter)
			.groupBy(qInventory.referenceNo, qInventory.encodedBy, qInventory.allocateDate, qInventory.batchRefNo);
		
		List<Tuple> tuples = query
				.list(qInventory.referenceNo, qInventory.encodedBy, qInventory.allocateDate, qInventory.sequence.min(), qInventory.sequence.max(), qInventory.batchRefNo);
		
		List<AllocationDto> list = Lists.newArrayList();
		Set<String> batches = Sets.newHashSet();
		for(Tuple tuple: tuples) {
			AllocationDto dto = new AllocationDto();
			dto.setAllocateDate(tuple.get(qInventory.allocateDate));
			dto.setReferenceNo(tuple.get(qInventory.referenceNo));
			dto.setEncodedBy(tuple.get(qInventory.encodedBy));
			dto.setStartingSeries(tuple.get(qInventory.sequence.min()));
			dto.setEndingSeries(tuple.get(qInventory.sequence.max()));
			dto.setBatchRefNo(tuple.get(qInventory.batchRefNo));
			batches.add(dto.getBatchRefNo());
			dto.setCardType(cardType);
			dto.setLocation(location);
			dto.setLocationName(loyaltyCardService.getInventoryDesc(location));
			dto.setTransferTo(transferTo);
			dto.setTransferToName(loyaltyCardService.getInventoryDesc(transferTo));
			dto.setNewTransferTo(newTransferTo);
			dto.setNewTransferToName(loyaltyCardService.getInventoryDesc(newTransferTo));
			list.add(dto);
		}
		
		return new Object[] {list, batches};
	}
	
	@Override
	public AllocateLoyaltyCardsDto getInventory(String referenceNo, LookupDetail status) {
		QLoyaltyCardInventory qProdInventory = new QLoyaltyCardInventory("productInventory");
		QLoyaltyCardInventory qRefInventory = new QLoyaltyCardInventory("referenceNoInventory");
		QLookupDetail qStatus = new QLookupDetail("status");
		QManufactureOrderTier qProduct = new QManufactureOrderTier("product");
		QLookupDetail qProductDtl = new QLookupDetail("productDtl");
		JPAQuery jpaQuery = new JPAQuery(em);
		
		JPQLQuery refQuery = jpaQuery
				.from(qRefInventory)
				.where(qRefInventory.referenceNo.eq(referenceNo).and(qRefInventory.status.eq(status)))
				.leftJoin(qRefInventory.status, qStatus)
				.groupBy(qRefInventory.location, qRefInventory.transferTo, qRefInventory.allocateDate, qRefInventory.encodedBy, qRefInventory.referenceNo, qStatus);
		
		List<Tuple> refRetList = refQuery
				.list(qRefInventory.location, qRefInventory.transferTo, qRefInventory.allocateDate, qRefInventory.encodedBy, qRefInventory.referenceNo, qStatus.code);
		
		if(CollectionUtils.isNotEmpty(refRetList)) {
			Tuple ref = refRetList.get(0);
			AllocateLoyaltyCardsDto dto = new AllocateLoyaltyCardsDto();
			
			dto.setAllocateTo(ref.get(qRefInventory.transferTo));
			dto.setLocation(ref.get(qRefInventory.location));
			dto.setAllocateDate(ref.get(qRefInventory.allocateDate));
			dto.setReferenceNo(ref.get(qRefInventory.referenceNo));
			dto.setEncodedBy(ref.get(qRefInventory.encodedBy));
			dto.setStatus(lookupService.getDetail(ref.get(qStatus.code)));
			
			jpaQuery = new JPAQuery(em);
			
			JPQLQuery query = jpaQuery
					.from(qProdInventory)
					.where(qProdInventory.referenceNo.eq(referenceNo).and(qProdInventory.status.eq(status)))
					.leftJoin(qProdInventory.product, qProduct)
					.leftJoin(qProdInventory.product.name, qProductDtl)
					.groupBy(qProduct, qProductDtl, qProductDtl.description, qProductDtl.header, qProductDtl.status);
			
			List<ProductSeriesDto> list = query
					.list(new QProductSeriesDto(qProductDtl, qProdInventory.sequence.min(), qProdInventory.sequence.max()));
			dto.setProducts(list);
			
			return dto;
		}

		return null;
		
	}
	
	@Override
	public List<LookupDetail> getCardTypes(String startingSeries, String endingSeries) {
		QLoyaltyCardInventory qInventory = QLoyaltyCardInventory.loyaltyCardInventory;
		QManufactureOrderTier qProduct = new QManufactureOrderTier("product");
		QLookupDetail qProductDtl = new QLookupDetail("productDtl");
		
		Long starting = Long.valueOf(startingSeries);
		Long ending = Long.valueOf(endingSeries);
		NumberExpression<Long> seriesExp = qInventory.sequence.castToNum(Long.class);
		
		List<String> codes = new JPAQuery(em).from(qInventory)
				.where(seriesExp.goe(starting).and(seriesExp.loe(ending)))
				.leftJoin(qInventory.product, qProduct)
				.leftJoin(qInventory.product.name, qProductDtl)
				.groupBy(qProductDtl.code)
				.list(qProductDtl.code);
		
		List<LookupDetail> details = Lists.newArrayList();
		for(String code : codes) {
			details.add(lookupService.getDetailByCode(code));
		}
		return details;
	}
	
	
	@Override
	@Transactional
	public long saveInventory(AllocationDto dto, LookupDetail status) {
		QLoyaltyCardInventory qLoyaltyCardInventory = QLoyaltyCardInventory.loyaltyCardInventory;
		
		HibernateUpdateClause clause = new HibernateUpdateClause(statelessSession, qLoyaltyCardInventory)
		.set(qLoyaltyCardInventory.lastUpdateUser, UserUtil.getCurrentUser().getUsername())
		.set(qLoyaltyCardInventory.lastUpdated, new DateTime()) //temp fix
		.set(qLoyaltyCardInventory.location, dto.getLocation())
        .set(qLoyaltyCardInventory.transferTo, dto.getTransferTo())
        .set(qLoyaltyCardInventory.referenceNo, dto.getReferenceNo())
        .set(qLoyaltyCardInventory.allocateDate, dto.getAllocateDate())
        .set(qLoyaltyCardInventory.encodedBy, dto.getEncodedBy())
        .set(qLoyaltyCardInventory.batchRefNo, dto.getBatchRefNo())
        .set(qLoyaltyCardInventory.status, status);

		List<BooleanExpression> expressions = new ArrayList<BooleanExpression>();
		NumberExpression<Long> seriesExp = qLoyaltyCardInventory.sequence.castToNum(Long.class);
		
		Long starting = Long.valueOf( dto.getStartingSeries());
		Long ending = Long.valueOf( dto.getEndingSeries());
		expressions.add(seriesExp.goe(starting).and(seriesExp.loe(ending)));
		
		if(status.getCode().equals(codePropertiesService.getDetailLoyaltyStatusTransferIn())) { // receive only inventory with correct transferTo location
			expressions.add(qLoyaltyCardInventory.transferTo.eq(dto.getLocation()));
		}
		
		return clause.where(BooleanExpression.allOf(expressions.toArray(new BooleanExpression[expressions.size()]))).execute();
	}

	
	@Override
	@Transactional
	public long saveForBurning(AllocationDto dto, Boolean forBurning) {
		QLoyaltyCardInventory qLoyaltyCardInventory = QLoyaltyCardInventory.loyaltyCardInventory;
		
		HibernateUpdateClause clause = new HibernateUpdateClause(statelessSession, qLoyaltyCardInventory)
	        .set(qLoyaltyCardInventory.forBurning, forBurning)
	        .set(qLoyaltyCardInventory.referenceNo, dto.getReferenceNo())
	        .set(qLoyaltyCardInventory.encodedBy, dto.getEncodedBy())
	        .set(qLoyaltyCardInventory.allocateDate, dto.getAllocateDate())
	        .set(qLoyaltyCardInventory.batchRefNo, dto.getBatchRefNo());

		NumberExpression<Long> seriesExp = qLoyaltyCardInventory.sequence.castToNum(Long.class);
		
		Long starting = Long.valueOf( dto.getStartingSeries());
		Long ending = Long.valueOf( dto.getEndingSeries());
		return clause.where(seriesExp.goe(starting).and(seriesExp.loe(ending))).execute();
	}
	
	@Override
	public Object[] getInventoriesForApproval(String location) {
		QLoyaltyCardInventory qInventory = QLoyaltyCardInventory.loyaltyCardInventory;
		QManufactureOrderTier qProduct = new QManufactureOrderTier("product");
		QLookupDetail qProductDtl = new QLookupDetail("productDtl");
		
		BooleanExpression locationFilter = null;
		
		if(!codePropertiesService.getDetailInvLocationHeadOffice().equals(getCurrentUserLocation())) {
			locationFilter = qInventory.location.eq(location);
		}
		
		JPQLQuery refQuery = new JPAQuery(em)
				.from(qInventory)
				.where(qInventory.status.code.eq(codePropertiesService.getDetailLoyaltyStatusForAllocation())
						.and(locationFilter))
				.groupBy(qInventory.referenceNo);
		
		List<String> references = refQuery.list(qInventory.referenceNo);
		
		List<AllocationDto> inventories = Lists.newArrayList();
		Set<String> batches = Sets.newHashSet();
		for(String referenceNo: references) {
			JPQLQuery query = new JPAQuery(em)
					.from(qInventory)
					.where(qInventory.referenceNo.eq(referenceNo).and(qInventory.status.code.eq(codePropertiesService.getDetailLoyaltyStatusForAllocation())))
					.leftJoin(qInventory.product, qProduct)
					.leftJoin(qInventory.product.name, qProductDtl)
					.groupBy(qInventory.location, qInventory.transferTo, qInventory.referenceNo, qInventory.encodedBy, qInventory.allocateDate, qInventory.batchRefNo);
			List<AllocationDto> ins = query
					.list(new QAllocationDto(qInventory.sequence.min(), qInventory.sequence.max(), qInventory.location, 
							qInventory.transferTo, qInventory.referenceNo, qInventory.encodedBy, qInventory.allocateDate, qInventory.batchRefNo));
			
			for(AllocationDto dto : ins) {
				dto.setLocationName(loyaltyCardService.getInventoryDesc(dto.getLocation()));
				dto.setTransferToName(loyaltyCardService.getInventoryDesc(dto.getTransferTo()));
				batches.add(dto.getBatchRefNo());
			}
			
			inventories.addAll(ins);
			
		}
		
		return new Object[] {inventories, batches};
	}
	
	@Override
	public Object[] getInventoriesForBurning() {
		QLoyaltyCardInventory qInventory = QLoyaltyCardInventory.loyaltyCardInventory;
		QManufactureOrderTier qProduct = new QManufactureOrderTier("product");
		QLookupDetail qProductDtl = new QLookupDetail("productDtl");
		
		Set<String> batches = Sets.newHashSet();
	
		List<AllocationDto> inventories = new JPAQuery(em)
				.from(qInventory)
				.where(qInventory.forBurning.isTrue())
				.leftJoin(qInventory.product, qProduct)
				.leftJoin(qInventory.product.name, qProductDtl)
				.groupBy(qInventory.location, qInventory.transferTo, qInventory.referenceNo, qInventory.encodedBy, qInventory.allocateDate, qInventory.batchRefNo)
				.list(new QAllocationDto(qInventory.sequence.min(), qInventory.sequence.max(), qInventory.location, 
				qInventory.transferTo, qInventory.referenceNo, qInventory.encodedBy, qInventory.allocateDate, qInventory.batchRefNo));
		
		for(AllocationDto dto : inventories) {
			dto.setLocationName(loyaltyCardService.getInventoryDesc(dto.getLocation()));
			dto.setTransferToName(loyaltyCardService.getInventoryDesc(dto.getTransferTo()));
			batches.add(dto.getBatchRefNo());
		}
		
		return new Object[] {inventories, batches};
	}
	
	@Override
	public List<ManufactureOrderReceiveDto> getReceivedInventories(Long manufactureOrderId) {
		QLoyaltyCardInventory qInventory = QLoyaltyCardInventory.loyaltyCardInventory;
		QLoyaltyCardInventoryHistory qHistory = QLoyaltyCardInventoryHistory.loyaltyCardInventoryHistory;
		
		NumberExpression<Long> seriesExp = qInventory.sequence.castToNum(Long.class);
		List<ManufactureOrderReceiveDto> list = new JPAQuery(em).from(qInventory).where(qInventory.product.manufactureOrder.id.eq(manufactureOrderId).and(qInventory.status.isNotNull()))
			.groupBy(qInventory.product.name.description, qInventory.receiptNo, qInventory.receiveDate)
			.list(new QManufactureOrderReceiveDto(qInventory.product.name.description, seriesExp.min().stringValue(), seriesExp.max().stringValue(), qInventory.receiptNo, qInventory.receiveDate));
		
		for(ManufactureOrderReceiveDto receiveDto: list) { //received by
			BooleanExpression filter = qHistory.status.code.eq(codePropertiesService.getDetailLoyaltyStatusInactive())
				.and(qHistory.inventory.sequence.eq(receiveDto.getStartingSeries()));
			receiveDto.setReceivedBy(new JPAQuery(em).from(qHistory).where(filter).singleResult(qHistory.createUser));
		}
		
		return list;
	}

	@Override
	public Long countReceivableInvetories(Long manufactureOrderId) {
		QLoyaltyCardInventory qInventory = QLoyaltyCardInventory.loyaltyCardInventory;
		return new JPAQuery(em).from(qInventory).where(qInventory.product.manufactureOrder.id.eq(manufactureOrderId).and(qInventory.status.isNull()))
				.singleResult(qInventory.count());
	}
	
	@Override
	@Transactional
	public long sendAllocateToChangeForApproval(AllocationDto dto) {
		QLoyaltyCardInventory qInventory = QLoyaltyCardInventory.loyaltyCardInventory;
		
		HibernateUpdateClause clause = new HibernateUpdateClause(statelessSession, qInventory)
	        .set(qInventory.newTransferTo, dto.getTransferTo())
	        .set(qInventory.referenceNo, dto.getReferenceNo())
	        .set(qInventory.batchRefNo, dto.getBatchRefNo());

		NumberExpression<Long> seriesExp = qInventory.sequence.castToNum(Long.class);
		
		Long starting = Long.valueOf( dto.getStartingSeries());
		Long ending = Long.valueOf( dto.getEndingSeries());
		
		BooleanExpression filter = seriesExp.goe(starting).and(seriesExp.loe(ending))
				.and(qInventory.transferTo.ne(dto.getTransferTo()))
				.and(qInventory.status.code.eq(codePropertiesService.getDetailLoyaltyStatusInTransit()));
		
		return clause.where(filter).execute();
	}
	
	@Override
	public List<String> getOrginalLocation(AllocationDto dto) {
		QLoyaltyCardInventory qInventory = QLoyaltyCardInventory.loyaltyCardInventory;
		
		NumberExpression<Long> seriesExp = qInventory.sequence.castToNum(Long.class);
		
		Long starting = Long.valueOf( dto.getStartingSeries());
		Long ending = Long.valueOf( dto.getEndingSeries());
		
		BooleanExpression filter = seriesExp.goe(starting).and(seriesExp.loe(ending))
				.and(qInventory.transferTo.ne(dto.getTransferTo()))
				.and(qInventory.status.code.eq(codePropertiesService.getDetailLoyaltyStatusInTransit()));
		
		return new JPAQuery(em).from(qInventory).where(filter).distinct().list(qInventory.transferTo);
	}
	
	@Override
	@Transactional
	public long approveAllocateToChange(String referenceNo) {
		QLoyaltyCardInventory qInventory = QLoyaltyCardInventory.loyaltyCardInventory;
		
		HibernateUpdateClause clause = new HibernateUpdateClause(statelessSession, qInventory)
        .set(qInventory.transferTo, qInventory.newTransferTo)
        .setNull(qInventory.newTransferTo);

		return clause.where(qInventory.referenceNo.eq(referenceNo)).execute();
		
	}
	
	@Override
	@Transactional
	public long rejectAllocateToChange(String referenceNo) {
		QLoyaltyCardInventory qInventory = QLoyaltyCardInventory.loyaltyCardInventory;
		
		HibernateUpdateClause clause = new HibernateUpdateClause(statelessSession, qInventory)
        .setNull(qInventory.newTransferTo);
		
		return clause.where(qInventory.referenceNo.eq(referenceNo)).execute();
		
	}
}
