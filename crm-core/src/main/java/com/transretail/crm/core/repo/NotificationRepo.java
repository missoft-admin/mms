package com.transretail.crm.core.repo;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.transretail.crm.common.repo.CrmQueryDslPredicateExecutor;
import com.transretail.crm.core.entity.Notification;
import com.transretail.crm.core.entity.enums.NotificationType;
import com.transretail.crm.core.repo.custom.NotificationRepoCustom;

@Repository
public interface NotificationRepo extends CrmQueryDslPredicateExecutor<Notification, Long>, NotificationRepoCustom {
	List<Notification> findByItemId(String id);
}
