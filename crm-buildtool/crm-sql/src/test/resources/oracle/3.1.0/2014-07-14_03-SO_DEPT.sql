ALTER TABLE CRM_SALES_ORD ADD DEPARTMENT varchar2(15 char);

    alter table CRM_SALES_ORD 
        add constraint FK_7c0f6qxiuba19t9br1vllpuwq 
        foreign key (DEPARTMENT) 
        references CRM_REF_LOOKUP_DTL;