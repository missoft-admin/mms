package com.transretail.crm.core.config;

import static org.junit.Assert.assertEquals;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

/**
 *
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = "classpath*:/META-INF/spring/applicationContext*.xml")
public class PropertyConfigTest {
    @Value("#{'${member.type.individual.code}'}")
    private String individualMemberType;

    @Test
    public void testCodes() {
        assertEquals("MTYP001", individualMemberType);
    }

}
