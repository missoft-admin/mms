package com.transretail.crm.core.entity;


import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.Type;
import org.hibernate.envers.Audited;
import org.joda.time.LocalTime;

import com.transretail.crm.core.entity.embeddable.Channel;
import com.transretail.crm.core.entity.lookup.LookupDetail;
import com.transretail.crm.core.entity.support.CustomAuditableEntity;


@Audited
@Table(name = "CRM_PROMOTION")
@Entity
public class Promotion extends CustomAuditableEntity<Long> implements Serializable {


    @ManyToOne
    @JoinColumn(name = "CAMPAIGN_ID")
    private Campaign campaign;

    @Column(name = "NAME")
    private String name;

    @Column(name = "DESCRIPTION")
    private String description;

    @ManyToOne
    @JoinColumn(name = "STATUS")
    private LookupDetail status;

    /**
     * comma separated value of days lookup detail - MKT004
     */
    @Column(name = "AFFECTED_DAYS")
    private String affectedDays;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "START_DATE")
    private Date startDate;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "END_DATE")
    private Date endDate;

    @Column(name = "START_TIME")
    @Type(type = "org.jadira.usertype.dateandtime.joda.PersistentLocalTime")
    private LocalTime startTime;

    @Column(name = "END_TIME")
    @Type(type = "org.jadira.usertype.dateandtime.joda.PersistentLocalTime")
    private LocalTime endTime;

    //header.reward.type=MKT005, 
    //reward.type.points.code=RWRD001, reward.type.discount.code=RWRD002
    @ManyToOne
    @JoinColumn(name = "REWARD_TYPE")
    private LookupDetail rewardType;

    @OneToMany(mappedBy = "promotion", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private List<PromotionReward> promotionRewards;

    @OneToMany(mappedBy = "promotion")
    private Set<PromotionMemberGroup> targetMemberGroups;

    @OneToMany(mappedBy = "promotion")
    private Set<PromotionProductGroup> targetProductGroups;

    @OneToMany(mappedBy = "promotion")
    private Set<PromotionStoreGroup> targetStoreGroups;

    @OneToMany(mappedBy = "promotion")
    private Set<PromotionPaymentType> targetPaymentTypes;
    
    @OneToMany(mappedBy = "promotion", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private Set<PromotionRedemption> promotionRedemptions;

    /*@OneToMany(mappedBy = "promotion", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    List<ApprovalRemark> approvalRemarks;*/

    @Embedded
    private Channel channel;

    public Promotion() {

    }

    public Promotion(Long id) {
        setId(id);
    }

    public Campaign getCampaign() {
        return campaign;
    }

    public void setCampaign(Campaign campaign) {
        this.campaign = campaign;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public LookupDetail getStatus() {
        return status;
    }

    public void setStatus(LookupDetail status) {
        this.status = status;
    }

    public String getAffectedDays() {
        return affectedDays;
    }

    public void setAffectedDays(String affectedDays) {
        this.affectedDays = affectedDays;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

	public LocalTime getStartTime() {
		return startTime;
	}

	public void setStartTime(LocalTime startTime) {
		this.startTime = startTime;
	}

	public LocalTime getEndTime() {
		return endTime;
	}

	public void setEndTime(LocalTime endTime) {
		this.endTime = endTime;
	}

	public LookupDetail getRewardType() {
        return rewardType;
    }

    public void setRewardType(LookupDetail rewardType) {
        this.rewardType = rewardType;
    }

    public List<PromotionReward> getPromotionRewards() {
        return promotionRewards;
    }

    public void setPromotionRewards(List<PromotionReward> promotionRewards) {
        this.promotionRewards = promotionRewards;
    }

    public Set<PromotionMemberGroup> getTargetMemberGroups() {
        return targetMemberGroups;
    }

    public void setTargetMemberGroups(Set<PromotionMemberGroup> targetMemberGroups) {
        this.targetMemberGroups = targetMemberGroups;
    }

    public Set<PromotionProductGroup> getTargetProductGroups() {
        return targetProductGroups;
    }

    public void setTargetProductGroups(Set<PromotionProductGroup> targetProductGroups) {
        this.targetProductGroups = targetProductGroups;
    }

    public Set<PromotionPaymentType> getTargetPaymentTypes() {
        return targetPaymentTypes;
    }

    public void setTargetPaymentTypes(Set<PromotionPaymentType> targetPaymentTypes) {
        this.targetPaymentTypes = targetPaymentTypes;
    }

	public Set<PromotionStoreGroup> getTargetStoreGroups() {
        return targetStoreGroups;
    }

    public void setTargetStoreGroups(Set<PromotionStoreGroup> targetStoreGroups) {
        this.targetStoreGroups = targetStoreGroups;
    }

	public Set<PromotionRedemption> getPromotionRedemptions() {
		return promotionRedemptions;
	}

	public void setPromotionRedemptions(
			Set<PromotionRedemption> promotionRedemptions) {
		this.promotionRedemptions = promotionRedemptions;
	}
    
}
