<%@include file="../../taglibs.jsp" %>


<div id="content_complaintForm" class="modal hide  nofly modal-dialog">

  <div class="modal-content">
  <c:url var="url_action" value="/member/complaint/save" />
  <form:form id="complaintForm" name="complaint" modelAttribute="complaint" method="POST" action="${url_action}" class="modal-form form-horizontal">
    <div class="modal-header">
      <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
      <h4><spring:message code="complaint_lbl_create" /></h4>
    </div>

    <div class="modal-body">
      <div class="hide alert alert-error" id="content_error"><button type="button" class="close" onclick="$(this).parent().hide();">&times;</button><div><form:errors   path="*"/></div></div>

      <div class="complaints control-group" style="margin-top: 10px">
        <label for="complaints" class="control-label">
          <b class="required">*</b> 
          <spring:message code="complaint_lbl_message"/>
          (<span id="messageCount">${maxCount}</span>)
        </label>
        <div  class="controls"><form:textarea id="complaints" path="complaints" data-max-count="${maxCount}" data-per-count="${perCount}" style="width: 350px; height: 200px" /></div>
      </div>
    </div>

    <div class="modal-footer">
      <button class="btn btn-primary" id="sendBtn" type="saveButton"><spring:message code="label_send" /></button>
      <button class="btn" id="cancelBtn" type="button" data-dismiss="modal"><spring:message code="label_cancel" /></button>
    </div>
  </form:form>
  </div>


<style type="text/css"> 
<!-- 
  .clearfix { margin-top: 20px !important; }
  .modal-footer { text-align: center; } 
  .control-label { width: 100px !important; } 
  .controls { margin-left: 120px !important; } 
  .controls div { padding-top: 4px !important; }
--> 
</style>
<script type="text/javascript">
var ComplaintForm = null;

$(document).ready( function() {

    var matched, browser;
    initOldBrowserFn();

    var errorContainer = $( "#content_complaintForm" ).find( "#content_error" );

    var $dialog = $( "#content_complaintForm" );
    var $form = $dialog.find( "#complaintForm" );
    var $save = $dialog.find( "#sendBtn" );
    var $message = $dialog.find( "#complaints" );
    var $messageCount = $dialog.find( "#messageCount" );
    //var $dispDate = $dialog.find( "#content_dateTime > .controls > div" );

    initDialog();
    initFormFields();

    function initDialog() {
        //$dialog.modal({ show : false }).draggable({ handle: ".modal-header" });
        $dialog.on( "hide", function(e) { 
            if ( e.target === this ) { 
                errorContainer.hide();
                ComplaintCommon.reset( $form );
            }
        });
    }

    function initFormFields() {
        $form.submit( function(e) {
            e.preventDefault();
            $save.prop( "disabled", true );
            e.preventDefault();
            $.post( $form.attr( "action" ), $form.serialize(), function( resp ) { 
                ComplaintCommon.processResp( resp, $dialog, errorContainer, $save ); 
                ComplaintList.reloadTable();
            }, "json" );
        });

        var maxCount = $message.data( "max-count" );
        var perCount = $message.data( "per-count" );
        $message.unbind( "keyup" ).bind( "keyup", function(e) {
            var length = $(this).val().length;
            return ComplaintForm.limitCharSize( e, length, maxCount, function() { 
                $messageCount.html( maxCount - length );
            });
        });
        $message.unbind( $.browser.opera ? "keypress" : "keydown" ).bind( $.browser.opera ? "keypress" : "keydown", function(e) {
            var length = $(this).val().length;
            return ComplaintForm.limitCharSize( e, length, maxCount - 1, function() { 
                $messageCount.html( maxCount - length );
            });
        });
        if ( ComplaintForm ) {
            $message.trigger( "keyup" ).trigger( $.browser.opera ? "keypress" : "keydown" );
        }

        //$dispDate.html( ( $dispDate.html() )? new Date( $dispDate.html() - 0 ).customize( 0 ) : "" );
    }

    ComplaintForm = {
        show : function() { $dialog.modal( "show" ); },
        limitCharSize : function( e, charLength, limit, func ) {
            /*var value = "";
            if( window.chrome ){ value = $(this).val().replace( /(\r\n|\n|\r)/g, "  " ); }
            else{ value = $(this).val();}*/
            if ( charLength > limit ) {
                if ( $.inArray( e.keyCode, [46, 8, 9, 27] ) != -1 
                    || ( e.keyCode == 65 && e.ctrlKey === true ) 
                    || ( e.keyCode >= 35 && e.keyCode <= 39 ) ) {
                  return true;
                }
                return false;
            }
            return func();
        }
    };

    function initOldBrowserFn() {
        jQuery.uaMatch = function( ua ) {
            ua = ua.toLowerCase();

            var match = /(chrome)[ \/]([\w.]+)/.exec( ua ) ||
                /(webkit)[ \/]([\w.]+)/.exec( ua ) ||
                /(opera)(?:.*version|)[ \/]([\w.]+)/.exec( ua ) ||
                /(msie) ([\w.]+)/.exec( ua ) ||
                ua.indexOf("compatible") < 0 && /(mozilla)(?:.*? rv:([\w.]+)|)/.exec( ua ) ||
                [];

            return {
                browser: match[ 1 ] || "",
                version: match[ 2 ] || "0"
            };
        };

        matched = jQuery.uaMatch( navigator.userAgent );
        browser = {};

        if ( matched.browser ) {
            browser[ matched.browser ] = true;
            browser.version = matched.version;
        }

        // Chrome is Webkit, but Webkit is also Safari.
        if ( browser.chrome ) {
            browser.webkit = true;
        } 
        else if ( browser.webkit ) {
            browser.safari = true;
        }
        jQuery.browser = browser;
    }

});
</script>


</div>