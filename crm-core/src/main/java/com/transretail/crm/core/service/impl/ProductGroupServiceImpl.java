package com.transretail.crm.core.service.impl;


import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.google.common.collect.Lists;
import com.mysema.query.Tuple;
import com.mysema.query.jpa.impl.JPAQuery;
import com.mysema.query.types.OrderSpecifier;
import com.mysema.query.types.expr.BooleanExpression;
import com.transretail.crm.common.service.dto.request.PageSortDto;
import com.transretail.crm.common.service.dto.response.ResultList;
import com.transretail.crm.common.service.util.SpringDataPagingUtil;
import com.transretail.crm.core.dto.ProductGroupDto;
import com.transretail.crm.core.dto.ProductGroupDto.CodeDescBean;
import com.transretail.crm.core.dto.ProductGroupSearchDto;
import com.transretail.crm.core.dto.ProductHierDto;
import com.transretail.crm.core.dto.ProductSearchDto;
import com.transretail.crm.core.entity.ProductGroup;
import com.transretail.crm.core.entity.PromotionProductGroup;
import com.transretail.crm.core.entity.QProductGroup;
import com.transretail.crm.core.entity.QPromotionProductGroup;
import com.transretail.crm.core.entity.lookup.Product;
import com.transretail.crm.core.entity.lookup.QProduct;
import com.transretail.crm.core.repo.ProductGroupRepo;
import com.transretail.crm.core.repo.ProductRepo;
import com.transretail.crm.core.repo.PromotionProductGroupRepo;
import com.transretail.crm.core.service.CodePropertiesService;
import com.transretail.crm.core.service.ProductGroupService;
import com.transretail.crm.core.service.ProductHierService;


@Service("productGroupService")
public class ProductGroupServiceImpl implements ProductGroupService {

	@Autowired
	ProductGroupRepo productGroupRepo;
	@Autowired
	ProductRepo productRepo;
	@Autowired
	PromotionProductGroupRepo promotionProductGroupRepo;

	@Autowired
	ProductHierService productHierService;
	@Autowired
	CodePropertiesService codePropertiesService;

    @PersistenceContext
    private EntityManager em;



	@Override
	public List<ProductGroup> findAll() {
		return productGroupRepo.findAll();
	}

	@Override
	public List<ProductGroup> getAllGroups() {
		List<ProductGroup> groups = findAll();
		
		for(ProductGroup group : groups) {
			group.setProductModels(getItems(group.getId()));
		}
		
		return groups;
	}

	@Override
	public ProductGroup findOne(Long id) {
		return productGroupRepo.findOne(id);
	}

	@Override
	public ResultList<ProductGroup> listProductGroups(PageSortDto dto) {
    	Pageable pageable = SpringDataPagingUtil.INSTANCE.createPageable(dto.getPagination());
        Page<ProductGroup> page = productGroupRepo.findAll(pageable);
        return new ResultList<ProductGroup>(page);
	}

	@Override
	public Iterable<ProductGroup> search(ProductGroup group) {
		List<BooleanExpression> expressions = new ArrayList<BooleanExpression>();
		
		QProductGroup qProductGroup = QProductGroup.productGroup;
		
		if(StringUtils.isNotBlank(group.getName()))
			expressions.add(qProductGroup.name.startsWithIgnoreCase(group.getName()));
		if(StringUtils.isNotBlank(group.getBrand()))
			expressions.add(qProductGroup.brand.startsWithIgnoreCase(group.getBrand()));
		if(group.getClassification() != null)
			expressions.add(qProductGroup.classification.code.eq(group.getClassification().getCode()));
		if(group.getSubclassification() != null)
			expressions.add(qProductGroup.subclassification.code.eq(group.getSubclassification().getCode()));
		if(StringUtils.isNotBlank(group.getProducts()))
			expressions.add(qProductGroup.products.contains(group.getProducts()));
		
		Iterable<ProductGroup> productGroups = productGroupRepo.findAll(BooleanExpression.allOf(expressions.toArray(new BooleanExpression[expressions.size()])));
		populateProducts(productGroups);
		return productGroups;
			
	}

	@Override
	public void delete(Long id) {
		productGroupRepo.delete(id);
	}

	@Override
	public void invalidate(Long id) {
		ProductGroup model = productGroupRepo.findOne(id);
		model.setValid(false);
		productGroupRepo.save( model );
	}

	@Override
	public void save(ProductGroup productGroup) {
		productGroupRepo.save(productGroup);
	}

	@Override
	public void saveDto( ProductGroupDto dto ) {
		productGroupRepo.save( dto.toModel() );
	}

	@Override
	public void updateDto( ProductGroupDto dto ) {
		ProductGroup model = productGroupRepo.findOne( dto.getId() );
		productGroupRepo.save( dto.toUpdatedModel( model ) );
	}



	@Override
	public List<Product> getItems( Long id ) {
		ProductGroup productGroup = findOne( id );

		List<Product> products = new ArrayList<Product>();

		if ( null != productGroup && CollectionUtils.isNotEmpty( productGroup.getProductList() ) ) {
			for( String productId : productGroup.getProductList() ) {
				products.add(productRepo.findOne( productId ) );
			}
		}

		return products;
	}

	@Override
	public ResultList<Product> listProducts(Long id, PageSortDto dto) {
		QProduct qProduct = QProduct.product;
		ProductGroup productGroup = findOne(id);
		BooleanExpression filters = qProduct.id.in(productGroup.getProductList());
    	Pageable pageable = SpringDataPagingUtil.INSTANCE.createPageable(dto.getPagination());
        return new ResultList<Product>(productRepo.findAll(filters, pageable));
	}

	@Override
	public Iterable<Product> searchItems(String searchStr) {
		List<BooleanExpression> expressions = new ArrayList<BooleanExpression>();
		
		QProduct qProduct = QProduct.product;
		
		if(StringUtils.isNotBlank(searchStr)) {
			searchStr = searchStr.replace('*', '%');
			expressions.add(qProduct.sku.toLowerCase().like(searchStr.toLowerCase()));
			expressions.add(qProduct.name.toLowerCase().like(searchStr.toLowerCase()));
			expressions.add(qProduct.description.toLowerCase().like(searchStr.toLowerCase()));
		}
		
		return productRepo.findAll(BooleanExpression.anyOf(expressions.toArray(new BooleanExpression[expressions.size()])));
	}



	@Override
	public List<CodeDescBean> filterProductAndCfns( String filterCode, String prdCodes, String cfnCodes ) {
		if ( StringUtils.isBlank( filterCode ) ) {
			return null;
		}

		List<CodeDescBean> prdCfns = new ArrayList<CodeDescBean>();
		//ea20140701: SKUs
		ProductSearchDto dto = new ProductSearchDto();
		dto.setSkuOrDescLikes( toList( filterCode ) );
		dto.setSkus( toList( prdCodes ) );

		/*for ( Product prd : productRepo.findAll( dto.createSearchExpression() ) ) {
			prdCfns.add( new CodeDescBean( prd.getSku(), prd.getSku(), prd.getDescription(), ProductGroupDto.PRODUCT_OPTION.product.toString(), true ) );
		}*/
		QProduct qp = QProduct.product;
		List<Tuple> prds = new JPAQuery( em ).from( qp ).where( dto.createSearchExpression() ).distinct().orderBy( qp.sku.asc() ).list( qp.sku, qp.description );
		for ( Tuple prd : prds ) {
			prdCfns.add( new CodeDescBean( prd.get( qp.sku ), prd.get( qp.sku ), prd.get( qp.description ), 
					ProductGroupDto.PRODUCT_OPTION.product.toString(), true ) );
		}
		for ( ProductHierDto cfn : productHierService.get( toList( filterCode ), toList( cfnCodes ), null ) ) {
			prdCfns.add( new CodeDescBean( cfn.getCode(), cfn.getDescription(), cfn.getLevel(), false ) );
		}
		return prdCfns;
	}

	@Override
	public List<CodeDescBean> searchProductAndCfns( String filterCode, boolean findProducts, String cfnCodes ) {
		if ( StringUtils.isBlank( filterCode ) ) {
			return null;
		}

		List<CodeDescBean> prdCfns = new ArrayList<CodeDescBean>();
		if ( findProducts ) {
			//ea20140701: SKUs
			ProductSearchDto dto = new ProductSearchDto();
			dto.setSkuOrDescLikes( toList( filterCode ) );
			/*for ( Product prd : productRepo.findAll( dto.createSearchExpression() ) ) {
				prdCfns.add( new CodeDescBean( prd.getSku(), prd.getSku(), prd.getDescription(), ProductGroupDto.PRODUCT_OPTION.product.toString(), true ) );
			}*/
			QProduct qp = QProduct.product;
			List<Tuple> prds = new JPAQuery( em ).from( qp ).where( dto.createSearchExpression() ).distinct().orderBy( qp.sku.asc() ).list( qp.sku, qp.description );
			for ( Tuple prd : prds ) {
				prdCfns.add( new CodeDescBean( prd.get( qp.sku ), prd.get( qp.sku ), prd.get( qp.description ), 
						ProductGroupDto.PRODUCT_OPTION.product.toString(), true ) );
			}
		}
		List<ProductHierDto> dtos = productHierService.get( toList( filterCode ), null, toList( cfnCodes ) );
		if ( CollectionUtils.isNotEmpty( dtos ) ) {
			for ( ProductHierDto cfn : dtos ) {
				prdCfns.add( new CodeDescBean( cfn.getCode(), cfn.getDescription(), cfn.getLevel(), false ) );
			}
		}
		return prdCfns;
	}

	@Override 
	public List<CodeDescBean> getProducts( String prdCodes, String cfnCodes ) {
		if ( StringUtils.isBlank( prdCodes ) && StringUtils.isBlank( cfnCodes ) ) {
			return null;
		}
		List<CodeDescBean> products = new ArrayList<CodeDescBean>();
		//ea20140701: SKUs
		ProductSearchDto dto = new ProductSearchDto();
		dto.setSkus( toList( prdCodes ) );
		dto.setSkuLikes( toList( cfnCodes ) );
		/*for ( Product prd : productRepo.findAll( dto.createSearchExpression() ) ) {
			products.add(  new CodeDescBean( prd.getSku(), prd.getSku(), prd.getDescription(), ProductGroupDto.PRODUCT_OPTION.product.toString(), true )  );
		}*/
		QProduct qp = QProduct.product;
		List<Tuple> prds = new JPAQuery( em ).from( qp ).where( dto.createSearchExpression() ).distinct().list( qp.sku, qp.description );
		for ( Tuple prd : prds ) {
			products.add( new CodeDescBean( prd.get( qp.sku ), prd.get( qp.sku ), prd.get( qp.description ), ProductGroupDto.PRODUCT_OPTION.product.toString(), true ) );
		}
		return products;
	}

	@Override
	public List<CodeDescBean> getProducts( String prdCodes, String cfnCodes, Integer idx, Integer size ) {
		if ( StringUtils.isBlank( prdCodes ) && StringUtils.isBlank( cfnCodes ) ) {
			return null;
		}
		else if ( null == idx || null == size  ) {
			return getProducts( prdCodes, cfnCodes );
		}

		List<CodeDescBean> products = new ArrayList<CodeDescBean>();
		//ea20140701: SKUs
		ProductSearchDto dto = new ProductSearchDto();
		dto.setSkus( toList( prdCodes ) );
		dto.setSkuLikes( toList( cfnCodes ) );
		/*Page<Product> pg = productRepo.findAll( dto.createSearchExpression(), new PageRequest( idx/size, size ) );
		if ( null != pg && null != pg.getContent() ) {
			for ( Product prd : pg.getContent() ) {
				products.add(  new CodeDescBean( prd.getSku(), prd.getSku(), prd.getDescription(), ProductGroupDto.PRODUCT_OPTION.product.toString(), true )  );
			}
		}*/
		QProduct qp = QProduct.product;
		JPAQuery query = new JPAQuery( em );
		query.from( qp ).where( dto.createSearchExpression() ).distinct();
		query.offset( idx ).limit( size ).orderBy( new OrderSpecifier<String>( com.mysema.query.types.Order.ASC, qp.sku ) );
		List<Tuple> prds = query.list( qp.sku, qp.description );
		for ( Tuple prd : prds ) {
			products.add( new CodeDescBean( prd.get( qp.sku ), prd.get( qp.sku ), prd.get( qp.description ), ProductGroupDto.PRODUCT_OPTION.product.toString(), true ) );
		}
		return products;
	}

	@Override 
	public Iterable<Product> getProducts( String prdCfnCodes ) {
		//ea20140701: SKUs
		ProductSearchDto dto = new ProductSearchDto();
		dto.setSkus( toList( prdCfnCodes ) );
		dto.setSkuLikes( toList( prdCfnCodes ) );
		return productRepo.findAll( dto.createSearchExpression() );
	}
	
	@Override 
	public List<CodeDescBean> searchProducts( String filterCode, String prdCodes, String cfnCodes ) {
		List<CodeDescBean> products = new ArrayList<CodeDescBean>();
		//ea20140701: SKUs
		ProductSearchDto dto = new ProductSearchDto();
		dto.setSkuOrDescLikes( toList( filterCode ) );
		dto.setSkus( toList( prdCodes ) );
		dto.setSkuLikes( toList( cfnCodes ) );
		/*for ( Product prd : productRepo.findAll( dto.createSearchExpression() ) ) {
			products.add(  new CodeDescBean( prd.getSku(), prd.getSku(), prd.getDescription(), ProductGroupDto.PRODUCT_OPTION.product.toString(), true )  );
		}*/
		QProduct qp = QProduct.product;
		List<Tuple> prds = new JPAQuery( em ).from( qp ).where( dto.createSearchExpression() ).distinct().list( qp.sku, qp.description );
		for ( Tuple prd : prds ) {
			products.add( new CodeDescBean( prd.get( qp.sku ), prd.get( qp.sku ), prd.get( qp.description ), 
					ProductGroupDto.PRODUCT_OPTION.product.toString(), true ) );
		}
		return products;
	}

	@Override
	public ResultList<ProductGroupDto> search( ProductGroupSearchDto dto ) {
        Pageable pageable = SpringDataPagingUtil.INSTANCE.createPageable(dto.getPagination());
        BooleanExpression filter = dto.createSearchExpression();
        Page<ProductGroup> page = filter != null ? productGroupRepo.findAll( filter, pageable ) : productGroupRepo.findAll( pageable );
		return new ResultList<ProductGroupDto>( toDtoList( page.getContent() ), page.getTotalElements(), page.getNumber(), page.getSize() );		
	}

    private List<ProductGroupDto> toDtoList( List<ProductGroup> models ) {
        List<ProductGroupDto> dtos = Lists.newArrayList();

        Iterator<PromotionProductGroup> it = promotionProductGroupRepo.findAll( 
        		QPromotionProductGroup.promotionProductGroup.promotion.status.code.equalsIgnoreCase( 
        				codePropertiesService.getDetailStatusActive() ) ).iterator();
        List<Long> activeIds = Lists.newArrayList();
        while( it.hasNext() ) {
        	PromotionProductGroup pGrp = it.next();
        	activeIds.add( pGrp.getProductGroup().getId() );
        }

        for ( ProductGroup model : models ) {
        	ProductGroupDto dto = new ProductGroupDto( model );
        	if ( CollectionUtils.isNotEmpty( activeIds ) ) {
        		if ( activeIds.contains( dto.getId().longValue() ) ) {
        			dto.setIsUsed( true );
        		}
        	}
        	dtos.add( dto );
        }
        return dtos;
    }

	@Override
	public ProductGroupDto findDto(Long id) {
		ProductGroup model = productGroupRepo.findOne( id );
		if ( null == model ) {
			return null;
		}

		ProductGroupDto dto = new ProductGroupDto( model );
		if ( StringUtils.isNotBlank( dto.getProducts() ) || StringUtils.isNotBlank( dto.getProductsCfns() ) ) {
			dto.setItemCodeDesc( null );

			if ( StringUtils.isNotBlank( dto.getProducts() ) || StringUtils.isNotBlank( dto.getProductsCfns() ) ) {
				ProductSearchDto sdto = new ProductSearchDto();
				sdto.setSkus( toList( dto.getProductsCfns() ) );
				/*for ( Product prd : productRepo.findAll( sdto.createSearchExpression() ) ) {
					dto.getItemCodeDesc().add( new CodeDescBean( prd.getSku(), prd.getSku(), prd.getDescription(), ProductGroupDto.PRODUCT_OPTION.product.toString(), true ) );
				}*/
				QProduct qp = QProduct.product;
				List<Tuple> prds = new JPAQuery( em ).from( qp ).where( sdto.createSearchExpression() ).distinct().list( qp.sku, qp.description );
				for ( Tuple prd : prds ) {
					dto.getItemCodeDesc().add( new CodeDescBean( prd.get( qp.sku ), prd.get( qp.sku ), prd.get( qp.description ), 
							ProductGroupDto.PRODUCT_OPTION.product.toString(), true ) );
				}
			}
			List<ProductHierDto> cfns = productHierService.get( null, toList( dto.getProductsCfns() ), null );
			if ( CollectionUtils.isNotEmpty( cfns ) ) {
				for ( ProductHierDto cfn : cfns ) {
					dto.getItemCodeDesc().add( new CodeDescBean( cfn.getCode(), cfn.getDescription(), null, false ) );
				}
			}
		}
		if ( StringUtils.isNotBlank( dto.getExcludedProducts() ) ) {
			dto.setExcProductCodeDesc( null );
			ProductSearchDto sdto = new ProductSearchDto();
			sdto.setSkus( toList( dto.getExcludedProducts() ) );
			/*for ( Product prd : productRepo.findAll( sdto.createSearchExpression() ) ) {
				dto.getExcProductCodeDesc().add( new CodeDescBean( prd.getSku(), prd.getSku(), prd.getDescription(), ProductGroupDto.PRODUCT_OPTION.product.toString(), true ) );
			}*/
			QProduct qp = QProduct.product;
			List<Tuple> prds = new JPAQuery( em ).from( qp ).where( sdto.createSearchExpression() ).distinct().list( qp.sku, qp.description );
			for ( Tuple prd : prds ) {
				dto.getExcProductCodeDesc().add( new CodeDescBean( prd.get( qp.sku ), prd.get( qp.sku ), prd.get( qp.description ), 
						ProductGroupDto.PRODUCT_OPTION.product.toString(), true ) );
			}
		}
		return dto;
	}



	private List<String> toList( String commaDelimCodes ) {
		if ( StringUtils.isNotBlank( commaDelimCodes ) ) {
			List<String> codes = new ArrayList<String>();
			for ( String code : commaDelimCodes.indexOf( ProductGroup.PRODUCT_LIST_SEPARATOR ) != -1? commaDelimCodes.split( ProductGroup.PRODUCT_LIST_SEPARATOR ) : new String[]{ commaDelimCodes } ) {
				codes.add(code);
			}
			return codes;
		}
		return new ArrayList<String>();
	}

	private void populateProducts( Iterable<ProductGroup> productGroups ) {
		for(ProductGroup group : productGroups) {
			List<Product> products = new ArrayList<Product>();
			for(String productId : group.getProductList()) {
				
				products.add(productRepo.findOne(productId));
			}
			group.setProductModels(products);
		}
	}

}
