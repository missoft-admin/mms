$(document).ready( function() {

	var $theUserForm = $( "#form_user" );



	initFormFields();
	initLinks();



	function initFormFields() {

		$( "#searchField" ).change( function(e) {
			e.preventDefault();
			$( "#searchValue" ).attr( "name", $( this ).val() );
		}).change();
	} 

	function initLinks() {

		var $theLink_createUser = $( "#link_createUser" );
		$theLink_createUser.click( function(e) {
			e.preventDefault();
			createUser( $theLink_createUser.data( "url" ) );
		});
	}



	function createUser( inUrl ) {
		$.ajax({
			cache 		: false, 
			dataType 	: "html",
			type 		: "GET", 
			url 		: inUrl,
			error 		: function(jqXHR, textStatus, errorThrown) {}, 
			success 	: function( inResponse ) {
				$theUserForm.html( inResponse );
			}
		});
	}

});



