package com.transretail.crm.core.repo.custom;

import java.util.Date;
import java.util.List;

import org.apache.commons.lang.mutable.MutableInt;
import org.joda.time.LocalDate;

import com.transretail.crm.core.dto.MemberDto;
import com.transretail.crm.core.dto.MemberMonitorSearchDto;
import com.transretail.crm.core.entity.MemberModel;

public interface MemberRepoCustom {

    List<MemberModel> findByCriteria(MemberModel inModel, String employeeTypeCode);
    
    List<MemberModel> findEmployeeByCriteria(MemberModel inModel, String employeeTypeCode);

	List<MemberModel> findRewardQualifiedEmp(Double inAmtFrom, Double inAmtTo,
			Date inIncDateFrom, Date inIncDateTo, String employeeTypeCode);

	List<String> findDeletedRewardQualifiedEmpIds(Date inFromDate, Date inToDate);

	List<MemberModel> findByNpwpId(String npwpId);
	
	List<MemberDto> getMonitoredMembers(Integer transcationCount, LocalDate date);
	
	List<MemberDto> getMonitoredEmployees(Integer transactionCount, LocalDate date);
	
	List<MemberDto> getMonitoredMembers(Integer transcationCount, LocalDate date, int limit, int offset, MutableInt totalElements);
	
	List<MemberDto> getMonitoredEmployees(Integer transactionCount, LocalDate date, int limit, int offset, MutableInt totalElements);
	
	List<MemberDto> getMonitoredMembers(MemberMonitorSearchDto dto, int limit, int offset, MutableInt totalElements);
	
	List<MemberDto> getMonitoredMembers(MemberMonitorSearchDto dto);
	
	Long countTranscactionsToday(String accountId, String memberTypeCode);

    MemberModel findByAccountIdAndMemberTypeCode(String accountId, String memberTypeCode);
    
    List<MemberModel> findMembersWithExpiringTransactions(LocalDate expiryDate);

}
