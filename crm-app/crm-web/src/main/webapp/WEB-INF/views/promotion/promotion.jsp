<%@include file="../common/messages.jsp" %>
<%@ include file="../common/taglibs.jsp" %>

<sec:authorize var="isHelpDesk" ifAnyGranted="ROLE_HELPDESK"/>

<div id="content_promotion">

  <div class="page-header page-header2">
      <h1><spring:message code="marketing_promotion" /></h1>
  </div>

  
  <div class="" id="search_promotion"><jsp:include page="promotionSearchForm.jsp" /></div>
  
  <div class="pull-right">
    <a href='<c:url value="/promotion/${mine}"/>' class="btn btn-default"><spring:message code="promotion_label_display_mine" /></a>
    <a href='<c:url value="/promotion/${all}"/>' class="btn btn-default"><spring:message code="promotion_label_display_all" /></a>
    <a href='<c:url value="/promotion/${forProccessing}"/>' class="btn btn-default"><spring:message code="promotion_label_display_forprocessing" /></a>
    <c:if test="${!isHelpDesk}">
      <a href='<c:url value="/promotion/create"/>' class="btn btn-primary"><spring:message code="promotion_label_create" /></a>
      <a href='<c:url value="/promotion/redemption/create"/>' class="btn btn-primary"><spring:message code="promo_redemption_create" /></a>
      <a href='<c:url value="/promotion/stamp/create"/>' class="btn btn-primary"><spring:message code="stamp_promo_create" /></a>
    </c:if>
  </div>
  
  <div class="clearfix"></div>

  <div id="list_promotions"><jsp:include page="promotionList.jsp"/></div>


  <script src='<c:url value="/js/viewspecific/promotion/promotion.js"/>' type='text/javascript'><![CDATA[&nbsp; ]]></script>
  <link href="<c:url value="/styles/temp.css" />" rel="stylesheet" />


</div>