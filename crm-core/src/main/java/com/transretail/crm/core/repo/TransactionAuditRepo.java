package com.transretail.crm.core.repo;

import org.springframework.stereotype.Repository;

import com.transretail.crm.common.repo.CrmQueryDslPredicateExecutor;
import com.transretail.crm.core.entity.TransactionAudit;

import java.util.List;

@Repository
public interface TransactionAuditRepo extends CrmQueryDslPredicateExecutor<TransactionAudit, Long> {

    List<TransactionAudit> findByTransactionNo(String transactionNo);


}
