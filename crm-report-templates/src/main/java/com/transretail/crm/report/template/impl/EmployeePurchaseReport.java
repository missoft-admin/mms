package com.transretail.crm.report.template.impl;

import com.google.common.base.Function;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.google.common.collect.Sets;
import com.mysema.query.Tuple;
import com.mysema.query.group.GroupBy;
import com.mysema.query.jpa.impl.JPAQuery;
import com.mysema.query.types.ConstantImpl;
import com.mysema.query.types.Expression;
import com.mysema.query.types.QTuple;
import com.mysema.query.types.expr.DateExpression;
import com.mysema.query.types.template.DateTemplate;
import com.transretail.crm.common.reporting.jasper.ClasspathFileResolver;
import com.transretail.crm.common.reporting.jasper.DefaultJRProcessor;
import com.transretail.crm.common.reporting.jasper.JRProcessor;
import com.transretail.crm.common.util.DateUtil;
import com.transretail.crm.core.entity.QEmployeePurchaseTxnModel;
import com.transretail.crm.core.entity.QMemberModel;
import com.transretail.crm.core.entity.QPointsTxnModel;
import com.transretail.crm.core.entity.enums.ReportType;
import com.transretail.crm.core.entity.lookup.QLookupDetail;
import com.transretail.crm.core.entity.lookup.Store;
import com.transretail.crm.core.service.StoreService;
import com.transretail.crm.report.template.CommonReportFilter;
import com.transretail.crm.report.template.FilterField;
import com.transretail.crm.report.template.NoDataFoundSupport;
import com.transretail.crm.report.template.ReportTemplate;
import java.util.*;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import net.sf.jasperreports.engine.JRParameter;
import net.sf.jasperreports.engine.fill.JRGzipVirtualizer;
import org.apache.commons.lang3.StringUtils;
import org.joda.time.LocalDate;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.MessageSourceAware;
import org.springframework.context.support.MessageSourceAccessor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Report Template for 'Employee Purchase Report
 *
 * @author Monte Cillo Co (mco@exist.com)
 */
@Service("employeePurchaseReport")
@Transactional(readOnly = true)
public class EmployeePurchaseReport implements ReportTemplate, MessageSourceAware, NoDataFoundSupport {

    private static final String PERMISSION_CODE = "REPORT_EMPLOYEE_PURCHASE_VIEW";
    protected static final String TEMPLATE_NAME = "Employee Purchase Report";
    protected static final String REPORT_NAME = "reports/employee_purchase_report.jasper";
    protected static final String FILTER_DATE_FROM = CommonReportFilter.DATE_FROM;
    protected static final String FILTER_DATE_TO = CommonReportFilter.DATE_TO;

    private static final DateTimeFormatter INPUT_DATE_PATTERN = DateTimeFormat.forPattern(DateUtil.SEARCH_DATE_FORMAT);
    private static final DateTimeFormatter FULL_DATE_PATTERN = DateTimeFormat.forPattern("dd-MMM-yyyy");
    @Autowired
    private StoreService storeService;
    private MessageSourceAccessor messageSource;
    @PersistenceContext
    protected EntityManager em;

    @Override
    public ReportType[] getReportTypes() {
        return ReportType.values();
    }

    @Override
    public String getName() {
        return TEMPLATE_NAME;
    }

    @Override
    public String getDescription() {
        return messageSource.getMessage("employee.purchase.report.title");
    }

    @Override
    public Set<FilterField> getFilters() {
        Set<FilterField> filters = Sets.newLinkedHashSet();
        filters.add(FilterField.createDateField(FILTER_DATE_FROM,
                messageSource.getMessage("employee.purchase.report.filter.purchase.date.from"),
                DateUtil.SEARCH_DATE_FORMAT, true));
        filters.add(FilterField.createDateField(FILTER_DATE_TO,
                messageSource.getMessage("employee.purchase.report.filter.purchase.date.to"),
                DateUtil.SEARCH_DATE_FORMAT, true));

        return filters;
    }

    @Override
    public boolean canView(Set<String> permissions) {
        return permissions.contains(PERMISSION_CODE);
    }

    private LocalDate parseDate(String dateInstance) {
        return INPUT_DATE_PATTERN.parseLocalDate(dateInstance);
    }

    @Override
    public JRProcessor createJrProcessor(Map<String, String> map) {
        LocalDate dateFrom = parseDate(map.get(FILTER_DATE_FROM));
        LocalDate dateTo = parseDate(map.get(FILTER_DATE_TO));
        Collection<?> dataSource = createDatasource(dateFrom, dateTo);
        DefaultJRProcessor jrProcessor = new DefaultJRProcessor(REPORT_NAME, createDatasource(dateFrom, dateTo));
        Map<String, Object> parameters = prepareReportParameters(dateFrom, dateTo);
        if (dataSource.size() == 1) {
            parameters.put("isEmpty", true);
            parameters.put("noDataFound", "No data found");
        }

        parameters.put(JRParameter.REPORT_VIRTUALIZER, new JRGzipVirtualizer(10));
        jrProcessor.addParameters(parameters);
        jrProcessor.setFileResolver(new ClasspathFileResolver("reports"));
        return jrProcessor;
    }

    @Override
    public void setMessageSource(MessageSource messageSource) {
        this.messageSource = new MessageSourceAccessor(messageSource);
    }

    private Collection<?> createDatasource(LocalDate dateFrom, LocalDate dateTo) {
        QEmployeePurchaseTxnModel qe = QEmployeePurchaseTxnModel.employeePurchaseTxnModel;
        QLookupDetail qldt = QLookupDetail.lookupDetail;
        QMemberModel qm = QMemberModel.memberModel;

        QTuple qtuple = new QTuple(qm.registeredStore, qm.id, qm.accountId,
                qm.firstName, qm.lastName, qldt.description.as("organization"),
                GroupBy.sum(qe.transactionAmount));

        List<Tuple> list = createQuery(dateFrom, dateTo)
                .orderBy(qm.registeredStore.asc(), qm.id.asc())
                .transform(GroupBy.groupBy(qm.registeredStore, qm.id).list(qtuple));

        List<ReportBean> beans = Lists.transform(list, new ReportBeanTupleConverter());

        QPointsTxnModel pm = QPointsTxnModel.pointsTxnModel;
        for (ReportBean bean : beans) {
            QTuple a = new QTuple(GroupBy.sum(pm.transactionAmount));

            List<Tuple> b = new JPAQuery(em).from(pm)
                    .where(pm.memberModel.accountId.substring(1, 11).eq(bean.getEmployeeId()))
                    .orderBy(pm.memberModel.registeredStore.asc(), pm.memberModel.id.asc())
                    .transform(GroupBy.groupBy(pm.memberModel.registeredStore, pm.memberModel.id).list(a));

            List<Double> c = Lists.transform(b, new ReportBeanTupleConverter2());

            bean.setTotalPurchaseAmount2(c.isEmpty() ? 0 : c.get(0));

        }
        List<ReportBean> ds = Lists.newArrayListWithCapacity(beans.size() + 1);
        ds.add(new ReportBean()); // add dummy bean - this will be ignore on report
        ds.addAll(beans);

        return ds;
    }

    JPAQuery createQuery(LocalDate dateFrom, LocalDate dateTo) {
        QEmployeePurchaseTxnModel qe = QEmployeePurchaseTxnModel.employeePurchaseTxnModel;
        QLookupDetail qldt = QLookupDetail.lookupDetail;
        QMemberModel qm = QMemberModel.memberModel;
        return new JPAQuery(em).from(qe)
                .leftJoin(qe.memberModel, qm)
                .leftJoin(qm.professionalProfile.department, qldt)
                .where(truncate(qe.transactionDateTime, "DD").between(dateFrom.toDate(), dateTo.toDate()));
    }

    // Good to have this in a util class	
    protected DateExpression<Date> truncate(Expression<?> expression, String pattern) {
        return DateTemplate.create(Date.class, "trunc({0}, '{1s}')", expression, ConstantImpl.create(pattern));
    }

    private Map<String, Object> prepareReportParameters(LocalDate dateFrom, LocalDate dateTo) {
        HashMap<String, Object> reportMap = Maps.newHashMap();
        reportMap.put("DATE_FROM", dateFrom.toString(FULL_DATE_PATTERN));
        reportMap.put("DATE_TO", dateTo.toString(FULL_DATE_PATTERN));

        return reportMap;
    }

    @Override
    public boolean isEmpty(Map<String, String> filter) {
        LocalDate dateFrom = parseDate(filter.get(FILTER_DATE_FROM));
        LocalDate dateTo = parseDate(filter.get(FILTER_DATE_TO));
        return createQuery(dateFrom, dateTo).notExists();
    }

    public static class ReportBean {

        private String storeCode;
        private String storeDesc;
        private String employeeId;
        private String employeeName;
        private String organization;
        private double totalPurchaseAmount;
        private double totalPurchaseAmount2;

        protected ReportBean withStore(String storeCode) {
            this.storeCode = storeCode;
            return this;
        }

        protected ReportBean withStoreDesc(String storeDesc) {
            this.storeDesc = storeDesc;
            return this;
        }

        protected ReportBean withEmployeeId(String employeeId) {
            this.employeeId = employeeId;
            return this;
        }

        protected ReportBean withEmployeeName(String employeeName) {
            this.employeeName = employeeName;
            return this;
        }

        protected ReportBean withOrganization(String organization) {
            this.organization = organization;
            return this;
        }

        protected ReportBean withTotalPurchaseAmount(double amount) {
            this.totalPurchaseAmount = amount;
            return this;
        }

        public String getStoreCode() {
            return storeCode;
        }

        public String getStoreDesc() {
            return storeDesc;
        }

        public String getEmployeeId() {
            return employeeId;
        }

        public String getEmployeeName() {
            return employeeName;
        }

        public String getOrganization() {
            return organization;
        }

        public double getTotalPurchaseAmount() {
            return totalPurchaseAmount;
        }

        public double getTotalPurchaseAmount2() {
            return totalPurchaseAmount2;
        }

        public void setTotalPurchaseAmount2(double totalPurchaseAmount2) {
            this.totalPurchaseAmount2 = totalPurchaseAmount2;
        }

        @Override
        public String toString() {
            return "ReportBean{" + "storeId=" + storeCode + ", storeDesc=" + storeDesc + ", employeeId=" + employeeId + ", employeeName=" + employeeName + ", totalPurchaseAmount=" + totalPurchaseAmount + '}';
        }

    }

    private class ReportBeanTupleConverter implements Function<Tuple, ReportBean> {

        public ReportBeanTupleConverter() {
        }

        @Override
        public ReportBean apply(Tuple tuple) {
            final String storeCode = tuple.get(0, String.class);
            Store store = storeService.getStoreByCode(storeCode);
            return new ReportBean().withStore(storeCode)
                    .withStoreDesc(store == null ? "" : store.getName())
                    .withEmployeeId(tuple.get(2, String.class))
                    .withEmployeeName(String.format("%s %s",
                                    StringUtils.defaultString(tuple.get(3, String.class)),
                                    StringUtils.defaultString(tuple.get(4, String.class))))
                    .withOrganization(tuple.get(5, String.class))
                    .withTotalPurchaseAmount(tuple.get(6, Double.class));
        }
    }

    private class ReportBeanTupleConverter2 implements Function<Tuple, Double> {

        public ReportBeanTupleConverter2() {
        }

        @Override
        public Double apply(Tuple tuple) {
            return tuple.get(0, Double.class);
        }
    }
}
