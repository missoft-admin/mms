Build Process :
	The code structure of the application uses the maven modular project structure. 
	The application is divided into logical sub-projects where each sub-projects generates its own jar / war output.

	As mentioned, the application uses maven as its build tool (http://maven.apache.org/).

Pre-Requesite for building the application :
	1. JDK 1.6
	2. Maven 3.0.0
	3. GIT
	4. Internet access to download the necessary dependencies / or if local repository is already setup, access to this reposistory
	5. Database Access requirements 
		a. H2


Build Structure :

	crm-buildtools (component for build related classes/configuration files, SQL scripts are also placed here)
		SQL SCRIPTS
			/src/main/resources/h2/create.sql - FOR H2

	crm-core (model/entities, persistence and dao framework, has dependency in edmi-file-commons)


Build Steps:

	1. checkout source code :

	     git clone git@git.exist.com:transretail-crm.git

	2. build the code:

	   On your terminal, go to project root directory and type :

		mvn clean install

    3. generating and reseting development database (H2).

        On your terminal, go to <ROOT>\crm-buildtool\crm-sql then type

        mvn clean install -PrecreateDb


        Note that this will delete any existing data you may have and will recreate the entire database with the current model settings.
        Call this command when there are new updates on the db tables so it will automatically recreate the db and insert the default values.

    4. Running the application.

       CRM Web Portal:

          On your terminal, go to <ROOT>\crm-app\crm-web then type :

               mvn jetty:run

          Wait until you see the following lines

          2013-11-18 13:39:11.847:INFO:oejs.AbstractConnector:Started SelectChannelConnector@0.0.0.0:8081
          [INFO] Started Jetty Server

          then open your browser and go to

              http://localhost:8081/crm-web/

          sample users  are admin, hr, cs. Default password is "user"


       Member Portal :

          On your terminal, go to <ROOT>\crm-app\member-web then type :

                mvn jetty:run



         Wait until you see the following lines

          2013-11-18 13:39:11.847:INFO:oejs.AbstractConnector:Started SelectChannelConnector@0.0.0.0:8082
          [INFO] Started Jetty Server

          then open your browser and go to

          http://localhost:8082/member-web/

          sample users  are member1, member2, member3. Default password is "user"



	Other build options:

	For H2 Development
		mvn clean install

		-properties used by default build
		<hibernate.dialect>org.hibernate.dialect.H2Dialect</hibernate.dialect>
		<jdbc.driverClassName>org.h2.Driver</jdbc.driverClassName>
		<jdbc.groupId>com.h2database</jdbc.groupId>
		<jdbc.artifactId>h2</jdbc.artifactId>
		<jdbc.version>${h2.version}</jdbc.version>
		<jdbc.url><![CDATA[jdbc:h2:file:~/transretail/crmdb]]></jdbc.url>
		<jdbc.username>sa</jdbc.username>
		<jdbc.password></jdbc.password>

		this profile also uses: hibernate.hbm2ddl.auto=create


    Generating DB script


	    -> crm-core >  mvn install -PrunScript

	 This will generate a DDL script at crm-core\tartget\schema.sql  .


	Run H2 scripts
		-> mvn install -P h2-scripts
	
	 Generate DB schema then run H2 scripts
	
		-> go to root directory (transretail-crm) -> mvn install -Ddbscripts



	 Web Services API:

     Service API to expose points processing (earn, redeem, void, adjust, retrieve) and member search are available via
     RESTFull services. Webservice will return a returnMessage object in JSON format

     To Test :

     EARN API


     REST URL : /points/earnpoints/{id}/{store}/{paymentType}/{amount}/{cardNumber}/{transactionNo}/{transactionDate}/{items}/MD5validationkey
     ie. http://localhost:8081/crm-web/api/points/earn/10/STORE1/PTYP001/2500.00/null/1000001/15092013155711/11111!1_22222!2_33333!1/thisismd5hashoutput

     where
     10 is the member account id
     STORE1 = store code
     PTYP001 = payment type. (PTYP001=cash, PTYP002= gift card, PTYP003=loyalty points, PTYP004=credit card)
     null = card number. null if no card and value of first 6 digit of debit/credit card
     2500.00 = transaction amount
     1000001 = transaction no
     15092013155711 = transaction date with format : ddMMyyyyHHmmss
     11111!1_22222!2_33333!1 = list of product items with qty. format : <product_id>!<qty>_<product_id>!<qty>_<product_id>!<qty>
     thisismd5hashoutput = MD5 hash output with signature MD5(ID + store + txnNo + MMS secretKey)

     Return JSON object with the following properties:

      type  - ie. ERROR or SUCCESS
      callingAction -  calling module ie. EARN
      message    - resulting message of the ws call
      messageCode  - messageCode (MSGC001 = member not found, MSGC002 = not enough points, MSGC003 = no points earned, MSGC004 = internal error)
      memberName  - formatted name of the member
      memberId   - member account id
      memberType - member type (MTYP001=Personal, MTYP002=employee)
      cardType   - card tier (TIER001=regular, TIER002=silver, TIER003=gold)
      totalPoints  - current total points of the member
      earnedPoints - earned points for this transaction
      currencyAmount - equivelant currency amount for the total points

      Sample return message :

      Successful transaction :
      {"type":"SUCCESS","callingAction":"EARN","message":"Successfully added 10 points for customer  7502766218", "messageCode":null, "totalPoints":1000,.......}

      Error in transaction :
      {"type":"ERROR","callingAction":"EARN","message":"No member found with id 7502766218", "messageCode":MSGC001, "totalPoints":1000,.......}


     Member type (personal, employee will be determined via the membertype field in member. Employees will not earn points per transactions
     but will just accumulate the purchase transactions



    RETURN POINTS

    REST URL :/return/{transactionNo}/{newTransactionNo}/{transactionAmount}/{store}/{itemList}/validationKey
    ie. http://localhost:8081/crm-web/api/points/return/1001/2222/100.00/STORE1/11111!1_22222!2_33333!1/thisismd5hashoutput
    where
         1001 =  transaction no  (prev)
         2222 = new txn no (pos creates new txn no for return item)
         100.00 =  amount  of returned item
         STORE1 = store code
         11111!1_22222!2_33333!1 = list of product items with qty. format : <product_id>!<qty>_<product_id>!<qty>_<product_id>!<qty>
         thisismd5hashoutput = MD5 hash output with signature MD5(transactionno + store + secretKey)






     VOID API without memberID

     REST URL : points/void/{transactionNo}/{transactionDate}/validationKey
     ie. http://localhost:8081/crm-web/api/points/void/1000001/15092013155711/thisismd5hashoutput

    where
     1000001 = transaction no
     15092013155711 = transaction date with format : ddMMyyyyHHmmss
     thisismd5hashoutput = MD5 hash output with signature MD5(transactionno  + secretKey)

     Returns JSON object with the following properties:

      type  - ie. ERROR or SUCCESS
      callingAction -  calling module ie. EARN
      message    - resulting message of the ws call
      messageCode  - for error messages, message codes will be provided. messageCode (MSGC001 = member not found, MSGC002 = not enough points, MSGC003 = no points earned, MSGC004 = internal error)
      memberName  - formatted name of the member
      memberId   - member account id
      memberType - member type (MTYP001=Personal, MTYP002=employee)
      cardType   - card tier (TIER001=regular, TIER002=silver, TIER003=gold)
      totalPoints  - current total points of the member after void
      earnedPoints - voided points negated value
      currencyAmount - equivalent currency amount for the total points after voided


      Sample return message :

      Successful transaction :
      {"type":"SUCCESS","callingAction":"VOID","message":"Successfully voided 10 points for customer  7502766218-1000001", "messageCode":null, "totalPoints":1000,"earnedPoints":-10 .......}






     REDEEM API

    REST URL : points/redeem/payment/{id}/{store}/{amount}/{transactionNo}/{transactionDate}/validationKey
     ie. http://localhost:8081/crm-web/api/points/redeem/payment/10/STORE1/2500.00/1000001/15092013155711/thisismd5hashoutput

    where
     10 is the member account id
     STORE1 = store code
     2500.00 = transaction amount
     1000001 = transaction no
     15092013155711 = transaction date with format : ddMMyyyyHHmmss
     thisismd5hashoutput = MD5 hash output with signature MD5(id + storecode + transactionno + secretKey)

     Returns JSON object with the following properties:

      type  - ie. ERROR or SUCCESS
      callingAction -  calling module ie. EARN
      message    - resulting message of the ws call
      messageCode  - for error messages, message codes will be provided. messageCode (MSGC001 = member not found, MSGC002 = not enough points, MSGC003 = no points earned, MSGC004 = internal error)
      memberName  - formatted name of the member
      memberId   - member account id
      memberType - member type (MTYP001=Personal, MTYP002=employee)
      cardType   - card tier (TIER001=regular, TIER002=silver, TIER003=gold)
      totalPoints  - current total points of the member after void
      earnedPoints - voided points negated value
      currencyAmount - equivalent currency amount for the total points after voided



     REDEEMING AN ITEM

     Redeeming an item is a two call process. First POSS retrieves all valid items

     Retrieve all valid items from store

     REST URL : points/validitems/{id}/{store}
      ie  http://localhost:8081/crm-web/api/points/validitems/120200001222/STORE1



    where
     120200001222 is the member account id. If product point is greater than total points of member then it will not be retrieved
     STORE1 = store code

   Successful transaction :
{"memberPoints":100.0,"productRestDtos":[{"pluId":"121212121212121121","name":"wewew","promotionId":1,"promotionName":null,"points":2.0},{"pluId":"121212121212121121","name":"wewew","promotionId":1,"promotionName":null,"points":2.0}]}



   Redeem item

    REST URL : points/redeem/items/{id}/{storeCode}/{transactionNo}/{transactionDate}/{items}/{validationKey}
     ie. http://localhost:8081/crm-web/api/points/redeem/payment/10/STORE1/1000001/15092013155711/11111!2222/thisismd5hashoutput

     this service will assume that all items have been validated via getValidItems service
     meaning all items are valid items based on promotion and member points availability. This transaction will also be audited in the CRM_POINTS_PROMOTION table


    where
     10 is the member account id
     STORE1 = store code
     1000001 = transaction no
     15092013155711 = transaction date with format : ddMMyyyyHHmmss
     11111!2222 = list of product items with qty. format : <product_pluid>!<product_pluid>!<product_pluid>
     thisismd5hashoutput = MD5 hash output with signature MD5(id + storecode + transactionno + secretKey)






      POINTS INFO API

      REST URL : points/info/{id}
      ie. http://localhost:8081/crm-web/api/points/info/10

      where:
        id = account ID/loyalty number from scanned barcode

      returns:
        type  - ie. ERROR or SUCCESS
        callingAction -  calling module ie. RETRIEVE
        message    - resulting message of the ws call
        memberName  - formatted name of the member




     PROFESSIONAL SEARCH

     1. Retrieve all Professional members

     REST URL : /find/professional/all
     ie. http://localhost:8081/crm-web/api/member/find/professional/all


     Returns JSON objects with the following properties
     CustomerId: customer id
     name:  customer name (firstname + lastname)
     businessName : name of business
     storeName : store's name
     status : status of customer - ACTIVE / INACTIVE
     messageCode: error message in cases where call is not successful or no member is found
     message : error message description


     Sample result:
    [{"customerId":"105","name":"member5 member5","businessName":"Business name","storeName":null,"status":"ACTIVE","message":null,"messageCode":null}]




      2. Retrieve Profeessional member by id

     REST URL : /find/professional/id/{id}
     http://localhost:8081/crm-web/api/member/find/professional/id/105

     where 105 is the customer account ID



     Returns JSON objects with the following properties
     CustomerId: customer id
     name:  customer name (firstname + lastname)
     businessName : name of business
     storeName : store's name
     status : status of customer - ACTIVE / INACTIVE
     messageCode: error message in cases where call is not successful or no member is found
     message : error message description
     Sample result:
     {"customerId":"105","name":"member5 member5","businessName":"Business name","storeName":null,"status":"ACTIVE","message":null,"messageCode":null}




     MEMBER VALIDATE PIN

     Validates a member's pin

     REST URL : member/validate/pin/{id}/{pin}
     ie. http://localhost:8081/crm-web/api/member/validate/pin/1001/12345

     where
           1001 is the member account ID
           1001 is the member PIN

     Returns JSON object with the following properties:

           type  - ie. ERROR or SUCCESS
           callingAction -  calling module VALIDATE_PIN
           message    - resulting message of the ws call
           messageCode  - for error messages, message codes will be provided. messageCode (MSGC001 = member not found, MSGC002 = not enough points, MSGC003 = no points earned, MSGC004 = internal error)
           ....





     MEMBER SEARCH


      Retrieve by id/accountid

     REST URL : member/find/id/{id}/{store},  member/find/account/{id}/{store}
     ie. http://localhost:8081/crm-web/api/member/find/id/10/10036
     ie. http://localhost:8081/crm-web/api/member/find/account/10/10036

    where
     10 is the member account id
     10036 is the store code from the calling POS terminal

     Please note that there is an added validation in place where only members with company and cardtype defined in the CRM_STORE_MEMBER  will be allowed to be processed

     Returns JSON object with the following properties:

      type  - ie. ERROR or SUCCESS
      callingAction -  calling module ie. EARN
      message    - resulting message of the ws call
      messageCode  - for error messages, message codes will be provided. messageCode (MSGC001 = member not found, MSGC002 = not enough points, MSGC003 = no points earned, MSGC004 = internal error)
      memberName  - formatted name of the member
      memberId   - member account id
      memberType - member type (MTYP001=Personal, MTYP002=employee)
      cardType   - card tier (TIER001=regular, TIER002=silver, TIER003=gold)
      totalPoints  - current total points of the member after void
      earnedPoints - voided points negated value
      currencyAmount - equivalent currency amount for the total points after voided

      Sample return message :

      Successful transaction :
      {"type":"SUCCESS","callingAction":"RETRIEVE_BY_ID","message":null, "messageCode":null, "totalPoints":1000,"memberName":"john doe", "memberId":"10","memberType":"MTYP001","cardType":"TIER001","currencyAmount":"1100.00","earnedPoint":"0"}

      Error in transaction :
      {"type":"ERROR","callingAction":"RETRIEVE_BY_ID","message":"No member found with id 7502766218", "messageCode":MSGC001, "totalPoints":1000,.......}




       Retrieve by email

      REST URL : member/find/email/{email}/
      ie. http://localhost:8081/crm-web/api/member/find/email/user@admin.com/

     where
      user@admin.com is the member email address

      Returns JSON object with the following properties:

       type  - ie. ERROR or SUCCESS
       callingAction -  calling module ie. EARN
       message    - resulting message of the ws call
       messageCode  - for error messages, message codes will be provided. messageCode (MSGC001 = member not found, MSGC002 = not enough points, MSGC003 = no points earned, MSGC004 = internal error)
       memberName  - formatted name of the member
       memberId   - member account id
       memberType - member type (MTYP001=Personal, MTYP002=employee)
       cardType   - card tier (TIER001=regular, TIER002=silver, TIER003=gold)
       totalPoints  - current total points of the member after void
       earnedPoints - voided points negated value
       currencyAmount - equivalent currency amount for the total points after voided

       Sample return message :

       Successful transaction :
       {"type":"SUCCESS","callingAction":"RETRIEVE_BY_EMAIL","message":null, "messageCode":null, "totalPoints":1000,"memberName":"john doe", "memberId":"10","memberType":"MTYP001","cardType":"TIER001","currencyAmount":"1100.00","earnedPoint":"0"}

       Error in transaction :
       {"type":"ERROR","callingAction":"RETRIEVE_BY_EMAIL","message":"No member found with id 7502766218", "messageCode":MSGC001, "totalPoints":1000,.......}


       IMPORTANT NOTE :  There is a known issue on how period is handled by the spring rest service. To workaround this make sure to add a trailing slash on the url



     Create Employee

     REST URL: api/employee/save/{empId}/{firstName}/{lastName}
     
     ie. http://localhost:8081/crm-web/api/employee/save/100/firstName/lastName


     Update Employee Status

     REST URL: api/employee/updatestatus/{empId}/{status}

     ie. http://localhost:8081/crm-web/api/employee/updatestatus/100/INACTIVE
     allows only ACTIVE and INACTIVE as status parameter


     
     
     USERS (password: user):
      cs
      css
      marketing
      marketinghead
      hr
      hrs
      gc
      gcs
      sc
      sch
      sm
      helpdesk
      member


      EXPIRE POINTS (testing purposes only)
      to set valid period (in months): api/points/validperiod/set/{validperiodinmonths}
      example: http://localhost:8081/crm-web/api/points/validperiod/set/25 
        -> valid transactions will expire 25 months after transactionDate

      to expire points for a user: api/points/expire/{date}/id/{accountId}
      example: http://localhost:8081/crm-web/api/points/expire/01-30-14/id/102
        -> expire points of member with account number 102 for the date 1-30-14
        -> date format: MM-dd-yy (e.g. 01-30-14 => January 30, 2014)

      to expire points for all users on a certail date: api/points/expire/{date}
      example: http://localhost:8081/crm-web/api/points/expire/01-30-14/
        -> date format: MM-dd-yy (e.g. 01-30-14 => January 30, 2014)