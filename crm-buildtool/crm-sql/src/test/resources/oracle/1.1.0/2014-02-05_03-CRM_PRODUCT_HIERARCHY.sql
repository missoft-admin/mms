    create table CRM_PROD_HIER (
        CODE varchar2(255 char) not null,
        CREATED_DATETIME timestamp,
        ENGLI_DESC varchar2(255 char),
        LAST_UPDATED_DATETIME timestamp,
        HIER_LEVEL number(10,0),
        LOCAL_DESC varchar2(255 char),
        PARENT_HIER_LEVEL varchar2(255 char),
        primary key (CODE)
    );