package com.transretail.crm.giftcard.service.impl;

import com.transretail.crm.giftcard.service.GiftCardRefNoProvider;
import org.joda.time.LocalDateTime;
import org.springframework.stereotype.Component;

@Component("giftCardRefNoProvider")
public class DefaultGiftCardRefNoProvider implements GiftCardRefNoProvider {

    static final String TIMESTAMP_FORMAT = "yyyyMMddHHmmssSSS";

    @Override
    public String getReferenceNo(String merchantId, String terminalId, String cashierId) {
	return String.format("%s%s%s%s", merchantId, terminalId, cashierId,
		LocalDateTime.now().toString(TIMESTAMP_FORMAT));
    }

}
