package com.transretail.crm.report.template.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.apache.commons.lang3.StringUtils;
import org.joda.time.DateTime;
import org.joda.time.LocalDate;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.MessageSourceAware;
import org.springframework.context.support.MessageSourceAccessor;
import org.springframework.stereotype.Service;

import com.google.common.base.Predicate;
import com.google.common.collect.Collections2;
import com.google.common.collect.Lists;
import com.mysema.query.BooleanBuilder;
import com.mysema.query.jpa.JPQLQuery;
import com.mysema.query.jpa.impl.JPAQuery;
import com.mysema.query.types.Projections;
import com.transretail.crm.common.reporting.jasper.ClasspathFileResolver;
import com.transretail.crm.common.reporting.jasper.DefaultJRProcessor;
import com.transretail.crm.common.reporting.jasper.JRProcessor;
import com.transretail.crm.common.util.DateUtil;
import com.transretail.crm.core.entity.QMemberModel;
import com.transretail.crm.core.entity.QPointsTxnModel;
import com.transretail.crm.core.entity.enums.ReportType;
import com.transretail.crm.core.entity.lookup.LookupDetail;
import com.transretail.crm.core.entity.lookup.Store;
import com.transretail.crm.core.security.util.UserUtil;
import com.transretail.crm.core.service.CodePropertiesService;
import com.transretail.crm.core.service.LookupService;
import com.transretail.crm.core.service.StoreService;
import com.transretail.crm.report.template.CommonReportFilter;
/**
 * @author ftopico
 */

@Service("visitFrequencyReport")
public class VisitFrequencyReport implements MessageSourceAware {
    
    @PersistenceContext
    private EntityManager em;
    @Autowired
    private StoreService storeService;
    @Autowired
    private CodePropertiesService codePropertiesService;
    @Autowired
    private LookupService lookupService;

    protected static final String REPORT_NAME_CUSTOMER_COUNT = "reports/visit_frequency_customer_count_report.jasper";
    protected static final String REPORT_NAME_PURCHASE_COUNT = "reports/visit_frequency_purchase_count_report.jasper";
    protected static final String FILTER_ACTIVATIONDATE_FROM = CommonReportFilter.DATE_FROM;
    protected static final String FILTER_ACTIVATIONDATE_TO = CommonReportFilter.DATE_TO;
    protected static final String FILTER_CUSTOMER_TYPE = "customerType";
    protected static final String FILTER_STORE = "store";
    protected static final String FILTER_STANDARD_REPORT_TYPE = "standardReportType";
    protected static final String VISIT_FREQ_REPORT_TYPE_COUNT = "customerCount";
    protected static final String VISIT_FREQ_REPORT_TYPE_PURCHASE = "purchaseCount";
    
    
    //Header Params
    private static final String PARAM_PRINT_DATE = "printedDate";
    private static final String PARAM_PRINT_BY = "printedBy";
    private static final DateTimeFormatter FULL_DATE_PATTERN = DateTimeFormat.forPattern("MMMM d, yyyy");
    private static final String SEARCH_DATE_FORMAT = DateUtil.SEARCH_DATE_FORMAT;
    private static final DateTimeFormatter INPUT_DATE_PATTERN = DateTimeFormat.forPattern(SEARCH_DATE_FORMAT);
    private MessageSourceAccessor messageSource;
    
    public ReportType[] getReportTypes() {
        return ReportType.values();
    }
    
    @Override
    public void setMessageSource(MessageSource messageSource) {
        this.messageSource = new MessageSourceAccessor(messageSource);
    }
    
    public boolean isDataFound(Map<String, String> map) {
        DateTime activationDateFrom = toDateTime(map.get(FILTER_ACTIVATIONDATE_FROM));
        DateTime activationDateTo = DateUtil.getEndOfDay(toDateTime(map.get(FILTER_ACTIVATIONDATE_TO)));
        String customerType = map.get(FILTER_CUSTOMER_TYPE);
        String store = map.get(FILTER_STORE);
        QPointsTxnModel qPointsTxn = QPointsTxnModel.pointsTxnModel;
        QMemberModel qMemberModel = QMemberModel.memberModel;
        
        BooleanBuilder predicate = new BooleanBuilder(qPointsTxn.created.between(activationDateFrom, DateUtil.getEndOfDay(activationDateTo))
                                                    .and(qPointsTxn.storeCode.isNotNull())
                                                    .and(qMemberModel.isNotNull())
                                                    .and(qMemberModel.memberType.code.ne("MTYP002")));
        
        if (StringUtils.isNotBlank(customerType)) {
            predicate.and(qMemberModel.memberType.code.eq(customerType));
        }
        
        if (StringUtils.isNotBlank(store)) {
            predicate.and(qPointsTxn.storeCode.eq(store));
        }
        
        JPQLQuery query = new JPAQuery(em).from(qPointsTxn)
                .rightJoin(qPointsTxn.memberModel, qMemberModel)
                .where(predicate.getValue());
            
       return query.exists();
        
    }
    
    public JRProcessor createJrProcessor(Map<String, String> map) {

        DateTime activationDateFrom = toDateTime(map.get(FILTER_ACTIVATIONDATE_FROM));
        DateTime activationDateTo = toDateTime(map.get(FILTER_ACTIVATIONDATE_TO));
        String customerType = map.get(FILTER_CUSTOMER_TYPE);
        String store = map.get(FILTER_STORE);
        String standardReportType = map.get(FILTER_STANDARD_REPORT_TYPE);
        QPointsTxnModel qPointsTxn = QPointsTxnModel.pointsTxnModel;
        QMemberModel qMemberModel = QMemberModel.memberModel;
        
        BooleanBuilder predicate = new BooleanBuilder(qPointsTxn.created.between(activationDateFrom, DateUtil.getEndOfDay(activationDateTo))
                                                    .and(qPointsTxn.storeCode.isNotNull())
                                                    .and(qMemberModel.memberType.code.ne("MTYP002")));
        
        if (StringUtils.isNotBlank(customerType)) {
            predicate.and(qMemberModel.memberType.code.eq(customerType));
        }
        
        if (StringUtils.isNotBlank(store)) {
            predicate.and(qPointsTxn.storeCode.eq(store));
        }
        
        JPQLQuery query = new JPAQuery(em);
        Map<String, Object> parameters = new HashMap<String, Object>();
        List<VisitFrequencyReportModel> list = new ArrayList<VisitFrequencyReport.VisitFrequencyReportModel>();
        
        if (standardReportType.equalsIgnoreCase(VISIT_FREQ_REPORT_TYPE_COUNT)) {
            query = query.from(qPointsTxn)
                .innerJoin(qPointsTxn.memberModel, qMemberModel)
                .where(predicate.getValue());
            
            List<String> storeCodes = query.distinct().list(qPointsTxn.storeCode);
            Map<String, Store> storeMap = storeService.getAllStoresMap();
            
            for (String storeCode : storeCodes) {
                VisitFrequencyReportModel reportModel = new VisitFrequencyReportModel();
                BooleanBuilder reportPredicate = new BooleanBuilder(predicate);
                
                reportModel.setStore(storeCode);
                if (storeCode.equalsIgnoreCase(codePropertiesService.getDetailInvLocationHeadOffice())) {
                    reportModel.setStoreName(lookupService.getDetailByCode(codePropertiesService.getDetailInvLocationHeadOffice()).getDescription());
                } else {
                    Store tempStore = storeMap.get(storeCode);
                    if (tempStore != null)
                    reportModel.setStoreName(tempStore.getName());
                }
                
                if (storeCodes.size() != 1)
                    reportPredicate.and(qPointsTxn.storeCode.eq(storeCode));
                
                query = new JPAQuery(em).from(qPointsTxn)
                        .innerJoin(qPointsTxn.memberModel, qMemberModel)
                        .where(reportPredicate)
                        .groupBy(qPointsTxn.memberModel.id);
                List<VisitFrequencyCustomerCountModel> customerCounts 
                    = query.list(Projections.fields(VisitFrequencyCustomerCountModel.class,
                            qPointsTxn.memberModel.id.as("memberId"),
                            qPointsTxn.transactionNo.countDistinct().as("visitCount")));
                
                reportModel.setZeroToTwo(Collections2.filter(customerCounts, zeroToTwo()).size());
                reportModel.setThreeToFive(Collections2.filter(customerCounts, threeToFive()).size());
                reportModel.setSixToEight(Collections2.filter(customerCounts, sixToEight()).size());
                reportModel.setOverNine(Collections2.filter(customerCounts, overNine()).size());

                list.add(reportModel);
            }
        } else if (standardReportType.equalsIgnoreCase(VISIT_FREQ_REPORT_TYPE_PURCHASE)) {
            query = query.from(qPointsTxn)
                    .innerJoin(qPointsTxn.memberModel, qMemberModel)
                    .where(predicate.getValue());
                
            List<String> storeCodes = query.distinct().list(qPointsTxn.storeCode);
            Map<String, Store> storeMap = storeService.getAllStoresMap();
            
            for (String storeCode : storeCodes) {
                VisitFrequencyReportModel reportModel = new VisitFrequencyReportModel();
                BooleanBuilder reportPredicate = new BooleanBuilder(predicate);
                
                reportModel.setStore(storeCode);
                if (storeCode.equalsIgnoreCase(codePropertiesService.getDetailInvLocationHeadOffice())) {
                    reportModel.setStoreName(lookupService.getDetailByCode(codePropertiesService.getDetailInvLocationHeadOffice()).getDescription());
                } else {
                    Store tempStore = storeMap.get(storeCode);
                    if (tempStore != null)
                    reportModel.setStoreName(tempStore.getName());
                }
                
                if (storeCodes.size() != 1)
                    reportPredicate.and(qPointsTxn.storeCode.eq(storeCode));
                
                query = new JPAQuery(em).from(qPointsTxn)
                        .innerJoin(qPointsTxn.memberModel, qMemberModel)
                        .where(reportPredicate)
                        .groupBy(qPointsTxn.memberModel.id);
                
                List<VisitFrequencyPurchaseValueCountModel> purchaseValueCounts 
                    = query.list(Projections.fields(VisitFrequencyPurchaseValueCountModel.class,
                            qPointsTxn.memberModel.id.as("memberId"),
                            qPointsTxn.transactionAmount.sum().as("totalPurchaseAmount")));
                
                
                reportModel.setZeroToOneHundred(Collections2.filter(purchaseValueCounts, zeroToOneHundred()).size());
                reportModel.setOneHundredOneToTwoHundred(Collections2.filter(purchaseValueCounts, oneHundredOneToTwoHundred()).size());
                reportModel.setTwoHundredOneToThreeHundred(Collections2.filter(purchaseValueCounts, twoHundredOneToThreeHundred()).size());
                reportModel.setThreeHundredOneToFourHundred(Collections2.filter(purchaseValueCounts, threeHundredOneToFourHundred()).size());
                reportModel.setFourHundredOneToFiveHundred(Collections2.filter(purchaseValueCounts, fourHundredOneToFiveHundred()).size());
                reportModel.setFiveHundredOneToSixHundred(Collections2.filter(purchaseValueCounts, fiveHundredOneToSixHundred()).size());
                reportModel.setSixHundredOneToSevenHundred(Collections2.filter(purchaseValueCounts, sixHundredOneToSevenHundred()).size());
                reportModel.setSevenHundredOneToEightHundred(Collections2.filter(purchaseValueCounts, sevenHundredOneToEightHundred()).size());
                reportModel.setEightHundredOneToNineHundred(Collections2.filter(purchaseValueCounts, eightHundredOneToNineHundred()).size());
                reportModel.setNineHundredOneToOneThousand(Collections2.filter(purchaseValueCounts, nineHundredOneToOneThousand()).size());
                reportModel.setOverOneThousand(Collections2.filter(purchaseValueCounts, overOneThousand()).size());
                
                list.add(reportModel);
            }
        }
        
        parameters.put(FILTER_ACTIVATIONDATE_FROM, activationDateFrom);
        parameters.put(FILTER_ACTIVATIONDATE_TO, activationDateTo);
        if (customerType != null) {
            LookupDetail customer = lookupService.getActiveDetailByCode(customerType);
            parameters.put(FILTER_CUSTOMER_TYPE, customer.getDescription());
//            parameters.put(FILTER_CUSTOMER_TYPE, customer.getCode() + " - " + customer.getDescription());
        }
        if (store != null) {
            parameters.put(FILTER_STORE, storeService.getStoreByCode(store).getName());
//            parameters.put(FILTER_STORE, storeService.getStoreByCode(store).getCodeAndName());
        }
        parameters.put(PARAM_PRINT_DATE, FULL_DATE_PATTERN.print(new LocalDate()));
        parameters.put(PARAM_PRINT_BY, getCurrentUser());
        
        List<ReportBean> beansDataSource = Lists.newArrayList();
        
        if (!list.isEmpty())
            beansDataSource.add(new ReportBean(list));
    	
    	DefaultJRProcessor jrProcessor = null;
    	if (standardReportType.equalsIgnoreCase(VISIT_FREQ_REPORT_TYPE_COUNT))
    	    jrProcessor = new DefaultJRProcessor(REPORT_NAME_CUSTOMER_COUNT, beansDataSource);
    	else if (standardReportType.equalsIgnoreCase(VISIT_FREQ_REPORT_TYPE_PURCHASE))
    	    jrProcessor = new DefaultJRProcessor(REPORT_NAME_PURCHASE_COUNT, beansDataSource);
    	jrProcessor.addParameters(parameters);
    	jrProcessor.setFileResolver(new ClasspathFileResolver("reports"));
    	
    	return jrProcessor;
	
    }
    
    private String getCurrentUser() {
        if (UserUtil.getCurrentUser() != null) {
            return UserUtil.getCurrentUser().getUsername();
        } else {
            return null;
        }
    }

    private DateTime toDateTime(String dateString) {
        if (StringUtils.isNotBlank(dateString)) {
            return INPUT_DATE_PATTERN.parseDateTime(dateString);
        }
        return null;
    }
    
    public static class VisitFrequencyPurchaseValueCountModel {
        private Long memberId;
        private Double totalPurchaseAmount = 0.0;
        
        public Double getTotalPurchaseAmount() {
            return totalPurchaseAmount;
        }
        public void setTotalPurchaseAmount(Double totalPurchaseAmount) {
            this.totalPurchaseAmount = totalPurchaseAmount;
        }
        public Long getMemberId() {
            return memberId;
        }
        public void setMemberId(Long memberId) {
            this.memberId = memberId;
        }
        
    }
    
    public static class VisitFrequencyCustomerCountModel {
        private Long memberId;
        private Long visitCount = 0L;
        
        public Long getVisitCount() {
            return visitCount;
        }
        public void setVisitCount(Long visitCount) {
            this.visitCount = visitCount;
        }
        public Long getMemberId() {
            return memberId;
        }
        public void setMemberId(Long memberId) {
            this.memberId = memberId;
        }
        
    }
    
    public static class VisitFrequencyReportModel {
        private String store;
        private String storeName;
        //Customer Count
        private long zeroToTwo = 0;
        private long threeToFive = 0;
        private long sixToEight = 0;
        private long overNine = 0;

        //Purchase Count
        private long zeroToOneHundred = 0;
        private long oneHundredOneToTwoHundred = 0;
        private long twoHundredOneToThreeHundred = 0;
        private long threeHundredOneToFourHundred = 0;
        private long fourHundredOneToFiveHundred = 0;
        private long fiveHundredOneToSixHundred = 0;
        private long sixHundredOneToSevenHundred = 0;
        private long sevenHundredOneToEightHundred = 0 ;
        private long eightHundredOneToNineHundred = 0 ;
        private long nineHundredOneToOneThousand = 0;
        private long overOneThousand = 0;
        
        public String getStore() {
            return store;
        }
        public String getStoreName() {
            return storeName;
        }
        public long getZeroToTwo() {
            return zeroToTwo;
        }
        public long getThreeToFive() {
            return threeToFive;
        }
        public long getSixToEight() {
            return sixToEight;
        }
        public long getOverNine() {
            return overNine;
        }
        public void setStore(String store) {
            this.store = store;
        }
        public void setStoreName(String storeName) {
            this.storeName = storeName;
        }
        public void setZeroToTwo(long zeroToTwo) {
            this.zeroToTwo = zeroToTwo;
        }
        public void setThreeToFive(long threeToFive) {
            this.threeToFive = threeToFive;
        }
        public void setSixToEight(long sixToEight) {
            this.sixToEight = sixToEight;
        }
        public void setOverNine(long overNine) {
            this.overNine = overNine;
        }
        public long getZeroToOneHundred() {
            return zeroToOneHundred;
        }
        public long getOneHundredOneToTwoHundred() {
            return oneHundredOneToTwoHundred;
        }
        public long getTwoHundredOneToThreeHundred() {
            return twoHundredOneToThreeHundred;
        }
        public long getThreeHundredOneToFourHundred() {
            return threeHundredOneToFourHundred;
        }
        public long getFourHundredOneToFiveHundred() {
            return fourHundredOneToFiveHundred;
        }
        public long getFiveHundredOneToSixHundred() {
            return fiveHundredOneToSixHundred;
        }
        public long getSixHundredOneToSevenHundred() {
            return sixHundredOneToSevenHundred;
        }
        public long getSevenHundredOneToEightHundred() {
            return sevenHundredOneToEightHundred;
        }
        public long getEightHundredOneToNineHundred() {
            return eightHundredOneToNineHundred;
        }
        public long getNineHundredOneToOneThousand() {
            return nineHundredOneToOneThousand;
        }
        public long getOverOneThousand() {
            return overOneThousand;
        }
        public void setZeroToOneHundred(long zeroToOneHundred) {
            this.zeroToOneHundred = zeroToOneHundred;
        }
        public void setOneHundredOneToTwoHundred(long oneHundredOneToTwoHundred) {
            this.oneHundredOneToTwoHundred = oneHundredOneToTwoHundred;
        }
        public void setTwoHundredOneToThreeHundred(long twoHundredOneToThreeHundred) {
            this.twoHundredOneToThreeHundred = twoHundredOneToThreeHundred;
        }
        public void setThreeHundredOneToFourHundred(long threeHundredOneToFourHundred) {
            this.threeHundredOneToFourHundred = threeHundredOneToFourHundred;
        }
        public void setFourHundredOneToFiveHundred(long fourHundredOneToFiveHundred) {
            this.fourHundredOneToFiveHundred = fourHundredOneToFiveHundred;
        }
        public void setFiveHundredOneToSixHundred(long fiveHundredOneToSixHundred) {
            this.fiveHundredOneToSixHundred = fiveHundredOneToSixHundred;
        }
        public void setSixHundredOneToSevenHundred(long sixHundredOneToSevenHundred) {
            this.sixHundredOneToSevenHundred = sixHundredOneToSevenHundred;
        }
        public void setSevenHundredOneToEightHundred(long sevenHundredOneToEightHundred) {
            this.sevenHundredOneToEightHundred = sevenHundredOneToEightHundred;
        }
        public void setEightHundredOneToNineHundred(long eightHundredOneToNineHundred) {
            this.eightHundredOneToNineHundred = eightHundredOneToNineHundred;
        }
        public void setNineHundredOneToOneThousand(long nineHundredOneToOneThousand) {
            this.nineHundredOneToOneThousand = nineHundredOneToOneThousand;
        }
        public void setOverOneThousand(long overOneThousand) {
            this.overOneThousand = overOneThousand;
        }
    }
    
    public static class ReportBean {
        
        private List<VisitFrequencyReportModel> list;
        
        public ReportBean() {}

        
        public ReportBean(List<VisitFrequencyReportModel> list) {
            super();
            this.list = list;
        }

        public List<VisitFrequencyReportModel> getList() {
            return list;
        }

        public void setList(List<VisitFrequencyReportModel> list) {
            this.list = list;
        }
        
    }
    
    private Predicate<VisitFrequencyCustomerCountModel> zeroToTwo() {
        return new Predicate<VisitFrequencyReport.VisitFrequencyCustomerCountModel>() {

            @Override
            public boolean apply(VisitFrequencyCustomerCountModel input) {
                return input.getVisitCount()>=0 && input.getVisitCount()<=2;
            }
        };
    }
    
    private Predicate<VisitFrequencyCustomerCountModel> threeToFive() {
        return new Predicate<VisitFrequencyReport.VisitFrequencyCustomerCountModel>() {

            @Override
            public boolean apply(VisitFrequencyCustomerCountModel input) {
                return input.getVisitCount()>=3 && input.getVisitCount()<=5;
            }
        };
    }
    
    private Predicate<VisitFrequencyCustomerCountModel> sixToEight() {
        return new Predicate<VisitFrequencyReport.VisitFrequencyCustomerCountModel>() {

            @Override
            public boolean apply(VisitFrequencyCustomerCountModel input) {
                return input.getVisitCount()>=6 && input.getVisitCount()<=9;
            }
        };
    }
    
    private Predicate<VisitFrequencyCustomerCountModel> overNine() {
        return new Predicate<VisitFrequencyReport.VisitFrequencyCustomerCountModel>() {

            @Override
            public boolean apply(VisitFrequencyCustomerCountModel input) {
                return input.getVisitCount()>=9;
            }
        };
    }
    
    private Predicate<VisitFrequencyPurchaseValueCountModel> zeroToOneHundred() {
        return new Predicate<VisitFrequencyReport.VisitFrequencyPurchaseValueCountModel>() {

            @Override
            public boolean apply(VisitFrequencyPurchaseValueCountModel input) {
                return input.getTotalPurchaseAmount()>=0 && input.getTotalPurchaseAmount()<=100000;
            }
        };
    }
    
    private Predicate<VisitFrequencyPurchaseValueCountModel> oneHundredOneToTwoHundred() {
        return new Predicate<VisitFrequencyReport.VisitFrequencyPurchaseValueCountModel>() {

            @Override
            public boolean apply(VisitFrequencyPurchaseValueCountModel input) {
                return input.getTotalPurchaseAmount()>=100001 && input.getTotalPurchaseAmount()<=200000;
            }
        };
    }
    
    private Predicate<VisitFrequencyPurchaseValueCountModel> twoHundredOneToThreeHundred() {
        return new Predicate<VisitFrequencyReport.VisitFrequencyPurchaseValueCountModel>() {

            @Override
            public boolean apply(VisitFrequencyPurchaseValueCountModel input) {
                return input.getTotalPurchaseAmount()>=200001 && input.getTotalPurchaseAmount()<=300000;
            }
        };
    }
    
    private Predicate<VisitFrequencyPurchaseValueCountModel> threeHundredOneToFourHundred() {
        return new Predicate<VisitFrequencyReport.VisitFrequencyPurchaseValueCountModel>() {

            @Override
            public boolean apply(VisitFrequencyPurchaseValueCountModel input) {
                return input.getTotalPurchaseAmount()>=300001 && input.getTotalPurchaseAmount()<=400000;
            }
        };
    }
    
    private Predicate<VisitFrequencyPurchaseValueCountModel> fourHundredOneToFiveHundred() {
        return new Predicate<VisitFrequencyReport.VisitFrequencyPurchaseValueCountModel>() {

            @Override
            public boolean apply(VisitFrequencyPurchaseValueCountModel input) {
                return input.getTotalPurchaseAmount()>=400001 && input.getTotalPurchaseAmount()<=500000;
            }
        };
    }
    
    private Predicate<VisitFrequencyPurchaseValueCountModel> fiveHundredOneToSixHundred() {
        return new Predicate<VisitFrequencyReport.VisitFrequencyPurchaseValueCountModel>() {

            @Override
            public boolean apply(VisitFrequencyPurchaseValueCountModel input) {
                return input.getTotalPurchaseAmount()>=500001 && input.getTotalPurchaseAmount()<=600000;
            }
        };
    }
    
    private Predicate<VisitFrequencyPurchaseValueCountModel> sixHundredOneToSevenHundred() {
        return new Predicate<VisitFrequencyReport.VisitFrequencyPurchaseValueCountModel>() {

            @Override
            public boolean apply(VisitFrequencyPurchaseValueCountModel input) {
                return input.getTotalPurchaseAmount()>=600001 && input.getTotalPurchaseAmount()<=700000;
            }
        };
    }
    
    private Predicate<VisitFrequencyPurchaseValueCountModel> sevenHundredOneToEightHundred() {
        return new Predicate<VisitFrequencyReport.VisitFrequencyPurchaseValueCountModel>() {

            @Override
            public boolean apply(VisitFrequencyPurchaseValueCountModel input) {
                return input.getTotalPurchaseAmount()>=700001 && input.getTotalPurchaseAmount()<=800000;
            }
        };
    }
    
    private Predicate<VisitFrequencyPurchaseValueCountModel> eightHundredOneToNineHundred() {
        return new Predicate<VisitFrequencyReport.VisitFrequencyPurchaseValueCountModel>() {

            @Override
            public boolean apply(VisitFrequencyPurchaseValueCountModel input) {
                return input.getTotalPurchaseAmount()>=800001 && input.getTotalPurchaseAmount()<=900000;
            }
        };
    }
    
    private Predicate<VisitFrequencyPurchaseValueCountModel> nineHundredOneToOneThousand() {
        return new Predicate<VisitFrequencyReport.VisitFrequencyPurchaseValueCountModel>() {

            @Override
            public boolean apply(VisitFrequencyPurchaseValueCountModel input) {
                return input.getTotalPurchaseAmount()>=900001 && input.getTotalPurchaseAmount()<=1000000;
            }
        };
    }
    
    private Predicate<VisitFrequencyPurchaseValueCountModel> overOneThousand() {
        return new Predicate<VisitFrequencyReport.VisitFrequencyPurchaseValueCountModel>() {

            @Override
            public boolean apply(VisitFrequencyPurchaseValueCountModel input) {
                return input.getTotalPurchaseAmount()>=1000001;
            }
        };
    }
}
