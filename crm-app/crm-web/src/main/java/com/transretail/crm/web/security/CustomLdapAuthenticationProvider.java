package com.transretail.crm.web.security;

import com.transretail.crm.core.entity.enums.AuthenticationType;
import org.apache.commons.lang3.EnumUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.security.authentication.AuthenticationServiceException;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.ldap.authentication.LdapAuthenticationProvider;
import org.springframework.security.ldap.authentication.LdapAuthenticator;

import com.transretail.crm.common.service.exception.GenericServiceException;
import com.transretail.crm.core.entity.enums.Status;
import com.transretail.crm.core.security.model.impl.CustomSecurityUserDetailsImpl;

/**
 * @author mhua
 */
public class CustomLdapAuthenticationProvider extends LdapAuthenticationProvider implements InitializingBean {

    private static final Logger LOG = LoggerFactory.getLogger(CustomLdapAuthenticationProvider.class);
    private UserDetailsService userDetailsService;
    private PasswordEncoder passwordEncoder;
    private LdapAuthenticator authenticator;


    public CustomLdapAuthenticationProvider(LdapAuthenticator authenticator) {
        super(authenticator);
    }

    public void setUserDetailsService(UserDetailsService userDetailsService) {
        this.userDetailsService = userDetailsService;
    }

    public void setPasswordEncoder(PasswordEncoder passwordEncoder) {
        this.passwordEncoder = passwordEncoder;
    }

    public LdapAuthenticator getAuthenticator() {
        return authenticator;
    }

    public void setAuthenticator(LdapAuthenticator authenticator) {
        this.authenticator = authenticator;
    }


    @Override
    public void afterPropertiesSet() throws Exception {
        if (userDetailsService == null) {
            throw new GenericServiceException("UserDetailsService is required.");
        }
        if (passwordEncoder == null) {
            throw new GenericServiceException("PasswordEncoder is required.");
        }
    }

    /**
     * Performs authentication with the same contract as
     * {@link org.springframework.security.authentication.AuthenticationManager#authenticate
     * (org.springframework.security.core.Authentication)}
     * .
     *
     * @param authentication the authentication request object.
     * @return a fully authenticated object including credentials. May return
     *         <code>null</code> if the <code>AuthenticationProvider</code> is
     *         unable to support authentication of the passed
     *         <code>Authentication</code> object. In such a case, the next
     *         <code>AuthenticationProvider</code> that supports the presented
     *         <code>Authentication</code> class will be tried.
     * @throws org.springframework.security.core.AuthenticationException
     *          if authentication fails.
     */
    @Override
    public Authentication authenticate(Authentication authentication)
        throws AuthenticationException {
        LOG.info("[AUTHENTICATING] {}", authentication.getName());

        final UsernamePasswordAuthenticationToken userToken = (UsernamePasswordAuthenticationToken)authentication;

        String username = (authentication.getPrincipal() == null) ? "NONE_PROVIDED" : authentication.getName();
        String password = authentication.getCredentials().toString();

        CustomSecurityUserDetailsImpl user = (CustomSecurityUserDetailsImpl) userDetailsService.loadUserByUsername(username);

        if (user == null) {
            throw new AuthenticationServiceException("security_login_db_usernotfound");
        }

        if (!user.isEnabled()) {
            throw new AuthenticationServiceException("security_login_userdisabled");
        }

        if ( null != EnumUtils.getEnum( Status.class, user.getStatus() )
                && Status.ACTIVE != EnumUtils.getEnum( Status.class, user.getStatus() ) ) {
            throw new AuthenticationServiceException("security_login_userdeactivated");
        }


        if (user.getAuthenticationType() != null && AuthenticationType.LDAP.equals(user.getAuthenticationType()))  {
            //Ldap authentication
            try {

                doAuthentication(userToken);
            } catch (BadCredentialsException notFound) {
                throw new AuthenticationServiceException("security_login_ldap_usernotvalid");
            } catch (UsernameNotFoundException notFound) {
                throw new AuthenticationServiceException("security_login_ldap_usernotvalid");

            } catch (Exception e) {
                LOG.error("LDAP Authentication problem for user {} : {} ",username,e.getMessage());
                throw new AuthenticationServiceException("security_login_error");

            }


        } else {

            //internal db authentication if no authentication type is defined
            if (StringUtils.isBlank(user.getPassword()) || !passwordEncoder.matches(password.trim(), user.getPassword())) {
                throw new AuthenticationServiceException("security_login_usernotvalid");
            }
        }





        UsernamePasswordAuthenticationToken token = new UsernamePasswordAuthenticationToken(
            user, user.getPassword(), user.getAuthorities());
        token.setDetails(authentication);

        return token;
    }

    /**
     * Returns <code>true</code> if this <Code>AuthenticationProvider</code>
     * supports the indicated <Code>Authentication</code> object.
     * <p>
     * Returning <code>true</code> does not guarantee an
     * <code>AuthenticationProvider</code> will be able to authenticate the
     * presented instance of the <code>Authentication</code> class. It simply
     * indicates it can support closer evaluation of it. An
     * <code>AuthenticationProvider</code> can still return <code>null</code>
     * from the
     * {@link #authenticate(org.springframework.security.core.Authentication)}
     * method to indicate another <code>AuthenticationProvider</code> should be
     * tried.
     * </p>
     * <p>
     * Selection of an <code>AuthenticationProvider</code> capable of performing
     * authentication is conducted at runtime the <code>ProviderManager</code>.
     * </p>
     *
     * @param authentication
     * @return <code>true</code> if the implementation can more closely evaluate
     *         the <code>Authentication</code> class presented
     */
    @Override
    public boolean supports(Class<?> authentication) {
        return UsernamePasswordAuthenticationToken.class
            .isAssignableFrom(authentication);
    }

}
