package com.transretail.crm.web.service.impl;

import static org.junit.Assert.assertEquals;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.util.List;

import javax.annotation.Resource;
import javax.sql.DataSource;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.AnnotationConfigContextLoader;

import com.google.common.collect.Lists;
import com.transretail.crm.common.util.DateUtil;
import com.transretail.crm.common.util.IOUtils;
import com.transretail.crm.reconciliation.service.PointsToolService;
import com.transretail.crm.reconciliation.service.impl.PointsToolServiceImpl;

/**
 * @author Allan G. Ramirez (agramirez@exist.com)
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(loader = AnnotationConfigContextLoader.class)
public class PointsToolServiceImplTest {
    @Configuration
    static class ContextConfiguration {
        @Bean
        public PointsToolService pointsToolService() {
            return new PointsToolServiceImpl();
        }

        @Bean(name = "dataSource")
        public DataSource dataSource() {
            DriverManagerDataSource ds = new DriverManagerDataSource();
            ds.setDriverClassName("org.h2.Driver");
            ds.setUrl("jdbc:h2:mem:db2");
            ds.setUsername("sa");
            return ds;
        }
    }

    @Autowired
    private PointsToolService service;
    @Resource(name = "dataSource")
    private DataSource dataSource;

    private String customerId = "000990601232";
    private int storeId = 1;
    private String storeCode = "10019";
    private String mediaType1 = "EFT_ONLINE";
    private double amount1 = 21050d;
    private String mediaType2 = "CASH";
    private double amount2 = 21051d;
    private String txnNo = "100190890003707";
    private Timestamp transactionDate = new Timestamp(System.currentTimeMillis());
    private String productId1 = "100198996006852045";
    private int quantity1 = 1;
    private String productId2 = "100198991234535494";
    private int quantity2 = 1;
    private String cardNumber = "627891xxxxxx6306";

    private Connection con = null;

    @Before
    public void onSetup() throws Exception {
        List<String> createTableScripts = Lists.newArrayList();
        createTableScripts.add("CREATE TABLE STORE (STORE_ID NUMBER(10,0), CODE VARCHAR(5), PRIMARY KEY (STORE_ID));");
        createTableScripts.add(
            "CREATE TABLE POS_TRANSACTION (ID VARCHAR(255), CUSTOMER_ID VARCHAR(20), STORE_ID NUMBER(10,0), TRANSACTION_DATE TIMESTAMP, PRIMARY KEY (ID));");
        createTableScripts.add(
            "CREATE TABLE POS_PAYMENT (ID VARCHAR(16), MEDIA_TYPE VARCHAR(255), AMOUNT FLOAT(126), POS_TXN_ID VARCHAR(255), PRIMARY KEY (ID));");
        createTableScripts.add(
            "CREATE TABLE POS_TX_ITEM (ID VARCHAR(16), PRODUCT_ID VARCHAR(255), QUANTITY FLOAT(126), POS_TXN_ID VARCHAR(255), PRIMARY KEY (ID));");
        createTableScripts.add(
            "CREATE TABLE ELECTRONIC_FUND_TRANSFER (EFT_ID VARCHAR(15), TRANSACTION_ID VARCHAR(15), CARD_NUM VARCHAR(16), PRIMARY KEY (EFT_ID));");

        con = dataSource.getConnection();
        con.setAutoCommit(false);

        Statement st = null;
        try {
            for (String script : createTableScripts) {
                st = con.createStatement();
                st.execute(script);
                IOUtils.INSTANCE.close(st);
            }
            con.commit();
        } finally {
            IOUtils.INSTANCE.close(st);
        }

        PreparedStatement storePs = null;
        try {
            storePs = con.prepareStatement("INSERT INTO STORE(STORE_ID, CODE) VALUES (?,?)");
            storePs.setInt(1, storeId);
            storePs.setString(2, storeCode);
            storePs.executeUpdate();
            con.commit();
        } finally {
            IOUtils.INSTANCE.close(storePs);
        }

        PreparedStatement txnPs = null;
        try {
            txnPs = con.prepareStatement("INSERT INTO POS_TRANSACTION(ID, CUSTOMER_ID, STORE_ID, TRANSACTION_DATE) VALUES (?,?,?,?)");
            txnPs.setString(1, txnNo);
            txnPs.setString(2, customerId);
            txnPs.setInt(3, storeId);
            txnPs.setTimestamp(4, transactionDate);
            ;
            txnPs.executeUpdate();
            con.commit();
        } finally {
            IOUtils.INSTANCE.close(txnPs);
        }

        PreparedStatement paymentPs = null;
        try {
            paymentPs = con.prepareStatement("INSERT INTO POS_PAYMENT(ID, MEDIA_TYPE, AMOUNT, POS_TXN_ID) VALUES (?,?,?,?)");
            paymentPs.setString(1, "1");
            paymentPs.setString(2, mediaType1);
            paymentPs.setDouble(3, amount1);
            paymentPs.setString(4, txnNo);
            paymentPs.executeUpdate();

            paymentPs = con.prepareStatement("INSERT INTO POS_PAYMENT(ID, MEDIA_TYPE, AMOUNT, POS_TXN_ID) VALUES (?,?,?,?)");
            paymentPs.setString(1, "2");
            paymentPs.setString(2, mediaType2);
            paymentPs.setDouble(3, amount2);
            paymentPs.setString(4, txnNo);
            paymentPs.executeUpdate();
            con.commit();
        } finally {
            IOUtils.INSTANCE.close(paymentPs);
        }

        PreparedStatement txItemPs = null;
        try {
            String insertTxItem = "INSERT INTO POS_TX_ITEM(ID, PRODUCT_ID, QUANTITY, POS_TXN_ID) VALUES (?,?,?,?)";
            txItemPs = con.prepareStatement(insertTxItem);
            txItemPs.setString(1, "1");
            txItemPs.setString(2, productId1);
            txItemPs.setInt(3, quantity1);
            txItemPs.setString(4, txnNo);
            txItemPs.executeUpdate();
            IOUtils.INSTANCE.close(txItemPs);

            txItemPs = con.prepareStatement(insertTxItem);
            txItemPs.setString(1, "2");
            txItemPs.setString(2, productId2);
            txItemPs.setInt(3, quantity2);
            txItemPs.setString(4, txnNo);
            txItemPs.executeUpdate();
            con.commit();
        } finally {
            IOUtils.INSTANCE.close(txItemPs);
        }

        PreparedStatement eftPs = null;
        try {
            String insertTxItem = "INSERT INTO ELECTRONIC_FUND_TRANSFER(EFT_ID, CARD_NUM, TRANSACTION_ID) VALUES (?,?,?)";
            eftPs = con.prepareStatement(insertTxItem);
            eftPs.setString(1, "1");
            eftPs.setString(2, cardNumber);
            eftPs.setString(3, txnNo);
            eftPs.executeUpdate();
            ;
            con.commit();
        } finally {
            IOUtils.INSTANCE.close(eftPs);
        }
    }

    @After
    public void tearDown() throws SQLException {
        StringBuilder dropQuery = new StringBuilder();
        dropQuery.append("DROP TABLE STORE; ");
        dropQuery.append("DROP TABLE POS_TRANSACTION; ");
        dropQuery.append("DROP TABLE POS_PAYMENT; ");
        dropQuery.append("DROP TABLE POS_TX_ITEM; ");
        dropQuery.append("DROP TABLE ELECTRONIC_FUND_TRANSFER; ");

        Statement ttSt = null;
        try {
            ttSt = con.createStatement();
            ttSt.execute(dropQuery.toString());
            con.commit();
        } finally {
            IOUtils.INSTANCE.close(con, true);
        }
    }

    @Test
    public void createEarnWsUrlTest() throws Exception {
        String ws = service.createEarnWsUrl(txnNo);
        System.out.println(ws);
        assertEquals("/points/earnpoints" +
            "/00099060123" +
            "/10019" +
            "/PTYP008:PTYP001" +
            "/21050:21051" +
            "/627891xxxxxx6306" +
            "/100190890003707" +
            "/" + DateUtil.convertDateToString(DateUtil.REST_DATE_FORMAT, transactionDate) +
            "/100198996006852045!1_100198991234535494!1", ws);
    }
}
