package com.transretail.crm.core.dto;


import java.util.ArrayList;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.joda.time.DateTime;

import com.transretail.crm.core.entity.PointsTxnModel;
import com.transretail.crm.core.entity.enums.TxnStatus;


public class PointsRewardDto {

	private String id;
    private MemberDto memberModel;
    private DateTime created;
    private Long dispCreatedDate;
    private Long dispTxnDate;
    private Double txnAmount;
    private Double rewardPoints;
    private Double reconPoints;

    private Boolean isForProcessing;



    public PointsRewardDto(){}
    public PointsRewardDto( PointsTxnModel model ) {
    	if ( null == model ) {
    		model = new PointsTxnModel();
    	}

    	if ( StringUtils.isNotBlank( model.getId() ) ) {
    		id = model.getId();
    	}
    	if ( null != model.getMemberModel() ) {
    		memberModel = new MemberDto( model.getMemberModel() );
    	}
    	if ( null != model.getCreated() ) {
    		created = model.getCreated();
    	}
    	if ( null != model.getCreated() ) {
    		dispCreatedDate = model.getCreated().toDate().getTime();
    	}
    	if ( null != model.getTransactionDateTime() ) {
    		dispTxnDate = model.getTransactionDateTime().getTime();
    	}
    	if ( null != model.getTransactionAmount() ) {
    		txnAmount = model.getTransactionAmount();
    	}
    	if ( null != model.getTransactionPoints() ) {
    		rewardPoints = model.getTransactionPoints();
    	}
    	if ( null != model.getStatus() ) {
    		isForProcessing = model.getStatus() == TxnStatus.FORAPPROVAL;
    	}
    }

    public static List<PointsRewardDto> toDtoList( List<PointsTxnModel> models ) {
    	if ( CollectionUtils.isEmpty( models ) ) {
    		return new ArrayList<PointsRewardDto>();
    	}

    	List<PointsRewardDto> dtos = new ArrayList<PointsRewardDto>();

    	for ( PointsTxnModel model : models ) {
    		dtos.add( new PointsRewardDto( model ) );
    	}

    	return dtos;
    }



    public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
    public MemberDto getMemberModel() {
		return memberModel;
	}
	public void setMemberModel(MemberDto memberModel) {
		this.memberModel = memberModel;
	}
	public DateTime getCreated() {
		return created;
	}
	public void setCreated(DateTime created) {
		this.created = created;
	}
	public Long getDispCreatedDate() {
		return dispCreatedDate;
	}
	public void setDispCreatedDate(Long dispCreatedDate) {
		this.dispCreatedDate = dispCreatedDate;
	}
	public Long getDispTxnDate() {
		return dispTxnDate;
	}
	public void setDispTxnDate(Long dispTxnDate) {
		this.dispTxnDate = dispTxnDate;
	}
	public Double getTxnAmount() {
		return txnAmount;
	}
	public void setTxnAmount(Double txnAmount) {
		this.txnAmount = txnAmount;
	}
	public Double getRewardPoints() {
		return rewardPoints;
	}
	public void setRewardPoints(Double rewardPoints) {
		this.rewardPoints = rewardPoints;
	}
	public Double getReconPoints() {
		return reconPoints;
	}
	public void setReconPoints(Double reconPoints) {
		this.reconPoints = reconPoints;
	}
	public Boolean isForProcessing() {
		return isForProcessing;
	}
	public void setForProcessing(Boolean isForProcessing) {
		this.isForProcessing = isForProcessing;
	}

}
