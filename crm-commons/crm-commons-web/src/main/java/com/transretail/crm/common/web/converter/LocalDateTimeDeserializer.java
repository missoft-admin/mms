package com.transretail.crm.common.web.converter;

import java.io.IOException;

import org.apache.commons.lang3.StringUtils;
import org.joda.time.LocalDateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;

/**
 *
 */
public class LocalDateTimeDeserializer extends JsonDeserializer<LocalDateTime> {
    private static final DateTimeFormatter FORMATTER = DateTimeFormat.forPattern("dd-MM-yyyy hh:mm:ss aa");

    @Override
    public LocalDateTime deserialize(JsonParser jp, DeserializationContext ctxt) throws IOException {
        if (StringUtils.isNotBlank(jp.getText())) {
            return FORMATTER.parseLocalDateTime(jp.getText());
        } else {
            return null;
        }
    }
}
