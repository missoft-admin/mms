<%@ include file="../taglibs.jsp" %>
<style><!-- 
    .spanX { width: 60%; } 
    
    .dataTables_wrapper {overflow: auto; max-width: 1280px}
    .well {
        color: #666666;
        width: auto;
    }
--></style>

<div class="page-header page-header2">
    <h1><spring:message code="purchase.label" /></h1>
</div>
<div class="well">
	<h3><spring:message code="label_total_purchase_amount"/>&nbsp;=&nbsp;<fmt:formatNumber type="number" pattern="#,##0.00" value="${totalAmount != null ? totalAmount : 0}"/></h3>
</div>

<div id="purchase-list">

</div>

<div id="transactionItemDialog" class="modal hide  nofly spanX">
  <div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
    <h4><spring:message code="purchase.transaction.items.header"/></h4>
  </div>
  <div class="modal-body">
    <p>

    </p>
  </div>
  <div class="modal-footer">
    <button class="btn" data-dismiss="modal" aria-hidden="true"><spring:message code="button_close"/></button>
  </div>
</div>

<link href="<spring:url value="/css/dataTables.css" />" rel="stylesheet">
<script src="<spring:url value="/js/jquery/jquery.dataTables.js" />"></script>
<script src="<spring:url value="/js/jquery/jquery.dataTables.linkLengthMenu.js" />"></script>
<script src="<spring:url value="/js/jquery/jquery.dataTables.customPaging.js" />"></script>
<script src="<spring:url value="/js/jquery/ajax-datatable.js" />"></script>
<script src="<spring:url value="/js/common/form2js.js" />"></script>
<script src="<spring:url value="/js/common/json2.js" />"></script>
<script src="<spring:url value="/js/jquery/jquery.toObject.js" />"></script>
<script src="<spring:url value="/js/dateutil.js" />"></script>

<spring:message code="purchase.transaction.viewitems" var="viewItemsLabel"/>

<script>
      var $transactionItemDialog = $( '#transactionItemDialog' );
      //.modal( { show : false } ).draggable();
      
      var $transactionItemList = $( '.modal-body > p', $transactionItemDialog ).ajaxDataTable( {
        'autoload' : false,
        'ajaxSource' : '<spring:url value="/postransaction/item/list" />',
        'columnHeaders' : [
          /* '<spring:message code="points.transaction.items.itemcode" />', */
          '<spring:message code="points.transaction.items.pluid" />',
          '<spring:message code="points.transaction.items.productDesc" />',
          '<spring:message code="points.transaction.items.productPrice" />',
          '<spring:message code="points.transaction.items.quantity" />',
          '<spring:message code="points.transaction.items.totalPrice" />',
          '<spring:message code="points.transaction.items.salesType" />'
        ],
        'modelFields' : [
          {name : 'pluId', sortable : false},
          {name : 'itemDesc', sortable : false},
          {name : 'itemPrice', sortable : false},
          {name : 'quantity', sortable : false},
          {name : 'totalPrice', sortable : false},
          {name : 'salesType', sortable : false}
        ]
      } );

      $("#purchase-list").ajaxDataTable({
        'autoload'  : true,
        'ajaxSource' : "<spring:url value="/member/purchases" />",
        'columnHeaders' : [
    	  "<spring:message code="purchase.storecode"/>",
          "<spring:message code="purchase.transactionno"/>",
          "<spring:message code="purchase.transactiondatetime"/>",
          "<spring:message code="purchase.transactionamount"/>",
          "<spring:message code="purchase.transactiontype"/>",
          "<spring:message code="purchase.paymenttype"/>",
          ""
        ],
        'modelFields' : [
    		 {name : 'store', sortable : true},
           {name : 'transactionNo', sortable : true},
           {name : 'transactionDateTime', sortable : true, fieldCellRenderer : function (data, type, row) {
               return ( (row.longTxnDate)? new Date( row.longTxnDate ).customize(0) : "" );
           }},
           {name : 'transactionAmount', sortable : true, fieldCellRenderer : function (data, type, row) {
    	           return row.transactionAmountFormatted;
    	     }},
    	     {name : 'transactionType', sortable: true},
    	     {name : 'paymentType', sortable: false},
           {customCell : function ( innerData, sSpecific, json ) {
                if ( sSpecific == 'display') {
              	return '<a class="transaction icn-view tiptip btn " data-transaction-no="'+json.transactionNo+'" href="#" onclick="return false;" title="${viewItemsLabel}">${viewItemsLabel}</a>';
                } else {
                  return '';
                }
                }
           }
        ]
      }).on( 'click', '.transaction', function () {
	    var transactionNo = $(this).data("transactionNo");
	    var formDataObject = new Object();
	    formDataObject.posTransactionNo = transactionNo;
	    $transactionItemList.ajaxDataTable( 'search', formDataObject );
	    $transactionItemDialog.modal( 'show' );
	  } );;
  
 

</script>