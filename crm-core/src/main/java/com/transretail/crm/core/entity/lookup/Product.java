package com.transretail.crm.core.entity.lookup;

import com.transretail.crm.core.entity.Department;
import java.io.Serializable;
import javax.persistence.*;
import org.hibernate.annotations.Immutable;

@Entity
@Table(name = "PRODUCT")
@Immutable
public class Product implements Serializable {
	
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "PLU_ID")
	private String id;

	@Column(name = "SKU")
	private String sku;

	@Column(name = "ITEM_CODE")
    private String itemCode;

	@Column(name = "NAME")
    private String name;

	@Column(name = "DESCRIPTION")
    private String description;

	@Column(name = "SHORT_DESC")
    private String shortDesc;

	@Column(name = "CATEGORY_ID")
    private String categoryId;

	@Column(name = "CURRENT_PRICE")
    private double currentPrice;
    
	@Column(name = "STORE_CODE")
    private String storeCode;
        @ManyToOne(fetch = FetchType.LAZY)
        @JoinColumn(name="department_code")
        private Department department;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getSku() {
		return sku;
	}

	public void setSku(String sku) {
		this.sku = sku;
	}

	public String getItemCode() {
		return itemCode;
	}

	public void setItemCode(String itemCode) {
		this.itemCode = itemCode;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getShortDesc() {
		return shortDesc;
	}

	public void setShortDesc(String shortDesc) {
		this.shortDesc = shortDesc;
	}

	public String getCategoryId() {
		return categoryId;
	}

	public void setCategoryId(String categoryId) {
		this.categoryId = categoryId;
	}

	public double getCurrentPrice() {
		return currentPrice;
	}

	public void setCurrentPrice(double currentPrice) {
		this.currentPrice = currentPrice;
	}

	public String getStoreCode() {
		return storeCode;
	}

	public void setStoreCode(String storeCode) {
		this.storeCode = storeCode;
	}

    public Department getDepartment() {
        return department;
    }

    public void setDepartment(Department department) {
        this.department = department;
    }
}
