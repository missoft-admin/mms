<%@ include file="../taglibs.jsp" %>
<%-- <%@ include file="../common/messages.jsp" %> --%>
<spring:url value="/member/password/reset" var="actionUrl"/>
<form:form modelAttribute="form" method="POST" action="${actionUrl}" cssClass="body-container form-horizontal">
  <h3>Password Helper</h3>
  <div class="control-group">
    <spring:message code="birthdate" var="birthDate"/>
    <form:label path="birthDate" title="${birthDate}" cssClass="control-label" cssErrorClass="control-label error"><span class="required">*</span>${birthDate}</form:label>
    <div class="controls">
      <form:input path="birthDate" title="${birthDate}" id="birthDate" class="input-xlarge"/>
    </div>
  </div>
  <div class="control-group">
    <spring:message code="email" var="email"/>
    <form:label path="email" title="Email" cssClass="control-label" cssErrorClass="control-label error"><span
            class="required">*</span>${email}</form:label>
    <div class="controls">
      <form:input path="email" title="${email}" class="input-xlarge"/>
    </div>
  </div>
  <div class="form-actions">
    <button type="submit" class="btn btn-primary pull-left"><spring:message code="button_submit" /></button>
    <p style="padding-top: 10px; clear: both" class="">I remember my password. <a class="" href="<spring:url value="/login"/>"><spring:message code="button_gotologin"/></a>.</p>
  </div>

  <%-- <div class="form-actions pull-center">
    <button type="submit" class="btn btn-block btn-primary"><spring:message code="button_submit" /></button>
    <a class="btn btn-block btn-default" href="<spring:url value="/login"/>"><spring:message code="button_gotologin" /></a>
  </div> --%>
</form:form>
<style>
  /* .form-actions { padding-left: 20px !important; } */
  .btn-block { width: 180px !important; }
  .pull-center { text-align: center !important; }
  .btn-default { padding-top: 12px; }


  .form-horizontal .control-label {
    width: 100px;
  }

  .form-horizontal .controls {
    margin-left: 130px;
  }

  .form-horizontal .form-actions {
    padding-left: 130px;
  }
</style>

<script src='<c:url value="/js/bootstrap/bootstrap-datepicker.js"/>' type='text/javascript'><![CDATA[&nbsp; ]]></script>
<link href="<c:url value="/css/bootstrap/datepicker.css" />" rel="stylesheet" />
<script type="text/javascript">
$(document).ready( function() {
	initFormFields();

	function initFormFields() {
    $( "#birthDate" ).datepicker({
      autoclose : true,
      format    : "mm-dd-yyyy"
    });
  }
});
</script>





