package com.transretail.crm.giftcard.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.Type;
import org.hibernate.envers.Audited;
import org.hibernate.envers.NotAudited;
import org.hibernate.envers.RelationTargetAuditMode;
import org.joda.time.LocalDate;

import com.mysema.query.annotations.QueryInit;
import com.transretail.crm.core.entity.support.CustomAuditableEntity;
import com.transretail.crm.giftcard.entity.support.GiftCardBurnCardStatus;
import org.joda.time.LocalTime;

/**
 * @author ftopico
 */
@Audited
@Entity
@Table(name = "CRM_GC_BURNCARD")
public class GiftCardBurnCard extends CustomAuditableEntity<Long> implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 1L;

    @Column(name = "BURN_NO")
    private String burnNo;
    
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "INVENTORY_NO", nullable = false)
    @NotAudited
    @Audited(targetAuditMode = RelationTargetAuditMode.NOT_AUDITED)
    private GiftCardInventory inventory;
    
    @Column(name = "STORE_ID")
    private Integer storeId;
    
    @Column(name="DATE_FILED")
    @Temporal(TemporalType.DATE)
    @Type(type = "org.jadira.usertype.dateandtime.joda.PersistentLocalDate")
    private LocalDate dateFiled;

    @Column(name = "TIME_FILED")
    @Temporal(TemporalType.TIME)
    @Type(type = "org.jadira.usertype.dateandtime.joda.PersistentLocalTime")
    private LocalTime timeFiled;
    
    @Column(name = "FILED_BY")
    private String filedBy;
    
    @Column(name = "BURN_REASON")
    private String burnReason;
    
    @Column(name="STATUS")
    @Enumerated(EnumType.STRING)
    private GiftCardBurnCardStatus status;
    
    @Column(name="APPROVED_BY")
    private String approvedBy;

    public String getBurnNo() {
        return burnNo;
    }

    public void setBurnNo(String burnNo) {
        this.burnNo = burnNo;
    }

    public GiftCardInventory getInventory() {
        return inventory;
    }

    @NotAudited
    public void setInventory(GiftCardInventory inventory) {
        this.inventory = inventory;
    }

    public Integer getStoreId() {
        return storeId;
    }

    public void setStoreId(Integer storeId) {
        this.storeId = storeId;
    }

    public String getFiledBy() {
        return filedBy;
    }

    public void setFiledBy(String filedBy) {
        this.filedBy = filedBy;
    }

    public String getBurnReason() {
        return burnReason;
    }

    public void setBurnReason(String burnReason) {
        this.burnReason = burnReason;
    }

    public GiftCardBurnCardStatus getStatus() {
        return status;
    }

    public void setStatus(GiftCardBurnCardStatus status) {
        this.status = status;
    }

    public LocalDate getDateFiled() {
        return dateFiled;
    }

    public void setDateFiled(LocalDate dateFiled) {
        this.dateFiled = dateFiled;
    }

    public LocalTime getTimeFiled() {
        return timeFiled;
    }

    public void setTimeFiled(LocalTime timeFiled) {
        this.timeFiled = timeFiled;
    }

    public String getApprovedBy() {
        return approvedBy;
    }

    public void setApprovedBy(String approvedBy) {
        this.approvedBy = approvedBy;
    }

}
