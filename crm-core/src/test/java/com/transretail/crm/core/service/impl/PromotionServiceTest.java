package com.transretail.crm.core.service.impl;


import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashSet;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.time.DateUtils;
import org.joda.time.LocalTime;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Configurable;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import com.transretail.crm.common.service.dto.rest.ReturnMessage;
import com.transretail.crm.common.util.DateUtil;
import com.transretail.crm.core.dto.PosTxItemDto;
import com.transretail.crm.core.dto.PromoRewardDto;
import com.transretail.crm.core.dto.PromotionSearchDto;
import com.transretail.crm.core.entity.Campaign;
import com.transretail.crm.core.entity.MemberGroup;
import com.transretail.crm.core.entity.MemberGroupField;
import com.transretail.crm.core.entity.MemberGroupFieldValue;
import com.transretail.crm.core.entity.MemberModel;
import com.transretail.crm.core.entity.PointsTxnModel;
import com.transretail.crm.core.entity.PosTransaction;
import com.transretail.crm.core.entity.PosTxItem;
import com.transretail.crm.core.entity.ProductGroup;
import com.transretail.crm.core.entity.Program;
import com.transretail.crm.core.entity.Promotion;
import com.transretail.crm.core.entity.PromotionMemberGroup;
import com.transretail.crm.core.entity.PromotionPaymentType;
import com.transretail.crm.core.entity.PromotionProductGroup;
import com.transretail.crm.core.entity.PromotionReward;
import com.transretail.crm.core.entity.PromotionStoreGroup;
import com.transretail.crm.core.entity.QProgram;
import com.transretail.crm.core.entity.TransactionAudit;
import com.transretail.crm.core.entity.embeddable.CustomerProfile;
import com.transretail.crm.core.entity.embeddable.Duration;
import com.transretail.crm.core.entity.enums.PromoModelType;
import com.transretail.crm.core.entity.enums.SalesType;
import com.transretail.crm.core.entity.lookup.LookupDetail;
import com.transretail.crm.core.entity.lookup.LookupHeader;
import com.transretail.crm.core.entity.lookup.Product;
import com.transretail.crm.core.entity.lookup.StoreGroup;
import com.transretail.crm.core.repo.CampaignRepo;
import com.transretail.crm.core.repo.LookupDetailRepo;
import com.transretail.crm.core.repo.LookupHeaderRepo;
import com.transretail.crm.core.repo.MemberGroupFieldRepo;
import com.transretail.crm.core.repo.MemberGroupFieldValueRepo;
import com.transretail.crm.core.repo.MemberGroupRepo;
import com.transretail.crm.core.repo.MemberRepo;
import com.transretail.crm.core.repo.PointsTxnModelRepo;
import com.transretail.crm.core.repo.PosTransactionRepo;
import com.transretail.crm.core.repo.PosTxItemRepo;
import com.transretail.crm.core.repo.ProductGroupRepo;
import com.transretail.crm.core.repo.ProductRepo;
import com.transretail.crm.core.repo.ProgramRepo;
import com.transretail.crm.core.repo.PromotionMemberGroupRepo;
import com.transretail.crm.core.repo.PromotionPaymentTypeRepo;
import com.transretail.crm.core.repo.PromotionProductGroupRepo;
import com.transretail.crm.core.repo.PromotionRepo;
import com.transretail.crm.core.repo.PromotionStoreGroupRepo;
import com.transretail.crm.core.repo.StoreGroupRepo;
import com.transretail.crm.core.repo.TransactionAuditRepo;
import com.transretail.crm.core.service.CodePropertiesService;
import com.transretail.crm.core.service.PointsTxnManagerService;
import com.transretail.crm.core.service.PromotionService;
import com.transretail.crm.core.util.CalendarUtil;


@Configurable
@Transactional
@ContextConfiguration(locations = "classpath*:/META-INF/spring/applicationContext*.xml")
@RunWith(SpringJUnit4ClassRunner.class)
public class PromotionServiceTest {

    @Autowired
    private PromotionService promotionService;

    @Autowired
    private PointsTxnManagerService pointsTxnManagerService;

    @Autowired
    private CodePropertiesService codePropertiesService;

    @Autowired
    private LookupDetailRepo lookupDetailRepo;
    
    @Autowired
    MemberRepo memberRepo;

    @Autowired
    private LookupHeaderRepo lookupHeaderRepo;

    @Autowired
    private MemberGroupRepo  memberGroupRepo;

    @Autowired
    private MemberGroupFieldRepo  memberGroupRepoFieldRepo;

    @Autowired
    private MemberGroupFieldValueRepo  memberGroupRepoFieldValueRepo;

    @Autowired
    private ProgramRepo programRepo;

    @Autowired
    private CampaignRepo campaignRepo;

    @Autowired
    private PromotionRepo promotionRepo;

    @Autowired
    private PointsTxnModelRepo pointsTxnModelRepo;
    @Autowired
    PromotionPaymentTypeRepo promotionPaymentTypeRepo;
    
    @Autowired
    PromotionProductGroupRepo promotionProductGroupRepo;
    
    @Autowired
    PromotionMemberGroupRepo promotionMemberGroupRepo;
    
    @Autowired
    PromotionStoreGroupRepo promotionStoreGroupRepo;
    
    @Autowired
    StoreGroupRepo storeGroupRepo;
    
    @Autowired
    ProductGroupRepo productGroupRepo;
    
    @Autowired
    PosTransactionRepo posTransactionRepo;
    
    @Autowired
    PosTxItemRepo posTxItemRepo;
    
    @Autowired
    ProductRepo productRepo;



    @Autowired
    TransactionAuditRepo transactionAuditRepo;
    
    Duration currentMonth;

    Program program;
    Promotion promotion2;

    Campaign campaign;

    Campaign campaignExchangeItem;

    Promotion promotion;
    Promotion promotionItemPoint;
    List<PosTxItem> itemPointItems = new ArrayList<PosTxItem>();

    private static final String PROGRAM_SEARCH_NAME = "Program1";
    private static final String CAMPAIGN_SEARCH_NAME = "Campaign1";
    private static final String CAMPAIGN_EXCHANGE_ITEM = "Campaign2";
    private static final String PROMOTION_SEARCH_NAME = "Promotion1";
    private static final String PROMOTION_EXCHANGE_ITEM =  "Promotion2";



    @Before
    public void setupPromotionServiceTest() {
        setupDuration();
        setupLookups();
        setupMember();
        setupPrograms();
        setupCampaigns();
        setupPromotions();
    }

    @After
    public void tearDown() {
        transactionAuditRepo.deleteAll();
        pointsTxnModelRepo.deleteAll();
    	memberRepo.deleteAll();
	     
    	productGroupRepo.deleteAll();
    	promotionProductGroupRepo.deleteAll();
 
    	memberGroupRepoFieldValueRepo.deleteAll();
    	memberGroupRepoFieldRepo.deleteAll();
    	memberGroupRepo.deleteAll();
    	promotionMemberGroupRepo.deleteAll();
 
    	storeGroupRepo.deleteAll();
    	promotionStoreGroupRepo.deleteAll();
 
    	promotionPaymentTypeRepo.deleteAll();

    	promotionRepo.deleteAll();
        campaignRepo.deleteAll();
        programRepo.deleteAll();
        lookupDetailRepo.deleteAll();
        lookupHeaderRepo.deleteAll();
    }



    @Test
    public void testGetValidItems() {



        String id = "1";
        String store = "ortigas";
        String invalidStore = "invalid";
        String invalidId = "999999";

        Assert.assertNotNull(promotionService.getValidItems(id, store));



        //test no items

        Assert.assertEquals(0,promotionService.getValidItems(id, store).getProductRestDtos().size());




        //test with valid items for store
        setupPromotion2RewardsItemExchange();

        Assert.assertNotNull(promotionService.getValidItems(id, store));

        Assert.assertEquals(1,promotionService.getValidItems(id, store).getProductRestDtos().size());



        //test with  items for invalid store and
        setupPromotionWithTargetStoreGroup();
        Assert.assertNotNull(promotionService.getValidItems(id, invalidStore));
        Assert.assertEquals(0,promotionService.getValidItems(id, invalidStore).getProductRestDtos().size());


        //test with  items for valid store and
        Assert.assertNotNull(promotionService.getValidItems(id, store));
        Assert.assertEquals(1,promotionService.getValidItems(id, store).getProductRestDtos().size());



        setupPromotionWithTargetMemberGroup();
        //test with  items for valid store and  invalid id
        Assert.assertNotNull(promotionService.getValidItems(invalidId, store));
        Assert.assertEquals(0,promotionService.getValidItems(invalidId, store).getProductRestDtos().size());

        //test with  items for valid store and  valid id
        Assert.assertNotNull(promotionService.getValidItems(id, store));
        Assert.assertEquals(1,promotionService.getValidItems(id, store).getProductRestDtos().size());
    }



    @Test
    public void testGetValidPrograms() {    	
        Assert.assertEquals(3, promotionService.getPrograms().size());
        Assert.assertEquals(1, promotionService.getValidPrograms().size());
        Assert.assertEquals(0, promotionService.getValidPrograms(
                DateUtils.addMonths(new Date(), 1)).size());

        Assert.assertEquals(1, promotionService.getValidProgramDtos().size());
    }

    @Test
    public void testGetValidCampaigns() {
        Assert.assertEquals(3, promotionService.getCampaignDtos( program.getId() ).size());
        Assert.assertEquals(2, promotionService.getValidCampaigns(program).size());
        Assert.assertEquals(0, promotionService.getValidCampaigns(program,
                DateUtils.addMonths(new Date(), 1)).size());
    }

    @Test
    public void testGetValidPromotions() {

        Assert.assertEquals(1, promotionService.getValidPromotions(campaign).size());
        Assert.assertEquals(1, promotionService.getValidPromotionsForCampaign(campaign, new Date()).size());
        Assert.assertEquals(0, promotionService.getValidPromotionsForCampaign(campaign,
                DateUtils.addMonths(new Date(), 1)).size());
    }

    @Test
    public void testGetValidPromotionsForMember() {


        //check when no target groups or parameters defined. Should return true
        List<Promotion> promotions = promotionService.getValidPromotionsForMember("1", "ortigas", "111","PTYP001", null, new Date(), null);
        Assert.assertEquals(2, promotions.size());


        //check for valid payment type

        setupPromotionWithTargetPaymentType();

        promotions = promotionService.getValidPromotionsForMember("1", "ortigas", "111","PTYP001", null, new Date(), null);
        Assert.assertEquals(2, promotions.size());

        promotions = promotionService.getValidPromotionsForMember("1", "ortigas", "111","PTYP002", null, new Date(), null);
        Assert.assertEquals(0, promotions.size());


        //check for valid store
        setupPromotionWithTargetStoreGroup();

        promotions = promotionService.getValidPromotionsForMember("1", "ortigas", "111","PTYP001", null, new Date(), null);
        Assert.assertEquals(2, promotions.size());

        promotions = promotionService.getValidPromotionsForMember("1", "invalidStorecode", "111","PTYP001", null, new Date(), null);
        Assert.assertEquals(0, promotions.size());

        setupPromotionWithTargetMemberGroup();
        
        promotions = promotionService.getValidPromotionsForMember("1", "ortigas", "111","PTYP001", null, new Date(), null);
        Assert.assertEquals(2, promotions.size());
        
        setupPromotionWithTargetProductGroup();
        
        promotions = promotionService.getValidPromotionsForMember("1", "ortigas", "111","PTYP001", null, new Date(), null);
        Assert.assertEquals(2, promotions.size());

    }

    @Test
    public void testRetrievePromotionReward() {

        setupPromotionItemPoint();

        List<PosTxItemDto> items = new ArrayList<PosTxItemDto>();
        for ( PosTxItem item : itemPointItems ) {
        	items.add( new PosTxItemDto( item ) );
        }
        PromoRewardDto reward = promotionService.retrievePromotionReward( "1", "PTYP001", "ortigas", "112", 1000D,  null, new Date(), 
        		new ArrayList<TransactionAudit>(), items );
        Assert.assertEquals( 12L, reward.getPoints().longValue() );
    }

    @Test
    public void testProcessPromotions() {

    	setupPromotionWithTargetPaymentType();
    	setupPromotionWithTargetStoreGroup();
    	setupPromotionWithTargetMemberGroup();
    	setupPromotionWithTargetProductGroup();
    	

    	ReturnMessage returnMessage = new ReturnMessage();
    	
    	MemberModel member = memberRepo.findByAccountId("1");
    	
    	Calendar calendar = Calendar.getInstance();
    	calendar.setTime(currentMonth.getStartDate());
    	calendar.add(Calendar.DATE, 1);
    	
    	DateFormat sdf = new SimpleDateFormat(DateUtil.REST_DATE_FORMAT);
    	String date = sdf.format(calendar.getTime());
    	
    	promotionService.processPromotions(member, "PTYP001:PTYP008", "ortigas", "111", "500:500", "123456*******89", date, null, returnMessage);
    	
    	Assert.assertNotNull(returnMessage);
    	Assert.assertTrue(returnMessage.getEarnedPoints() == 10L);

    	promotionService.processPromotions(member, "PTYP008", "ortigas", "121", "500", "124646*******89", date, null, returnMessage);
    	
    	Assert.assertNotNull(returnMessage);
    	Assert.assertTrue(returnMessage.getEarnedPoints() == 0L);

        List<PointsTxnModel> points = pointsTxnManagerService.retrievePointsByTransactionNo("121");

        //assert that points is still saved even for 0 points
        Assert.assertEquals(1, points.size());

    }

    @Test
    public void testConvertPointToCurrency() {

        //todo


    }


    private void setupDuration() {

        currentMonth = new Duration();
        currentMonth.setStartDate(DateUtils.addMonths(CalendarUtil.INSTANCE.getPrevMonthStartDate(), 1));
        currentMonth.setEndDate(DateUtils.addMilliseconds(DateUtils.ceiling(currentMonth.getStartDate(),
                Calendar.MONTH), -1));
    }

    private void setupLookups() {

        LookupHeader theLookupHeader = new LookupHeader(codePropertiesService.getHeaderStatus());
        lookupHeaderRepo.saveAndFlush(theLookupHeader);

        LookupDetail theLookupDetail = new LookupDetail(codePropertiesService.getDetailStatusActive());
        theLookupDetail.setDescription("STATUS ACTIVE");
        theLookupDetail.setHeader(lookupHeaderRepo.findOne(codePropertiesService.getHeaderStatus()));
        lookupDetailRepo.saveAndFlush(theLookupDetail);
        
        //reward type
        theLookupHeader = new LookupHeader(codePropertiesService.getHeaderRewardType());
        lookupHeaderRepo.saveAndFlush(theLookupHeader);

        theLookupDetail = new LookupDetail(codePropertiesService.getDetailRewardTypePoints());
        theLookupDetail.setDescription("POINTS");
        theLookupDetail.setHeader(lookupHeaderRepo.findOne(codePropertiesService.getHeaderRewardType()));
        lookupDetailRepo.saveAndFlush(theLookupDetail);

        theLookupDetail = new LookupDetail(codePropertiesService.getDetailRewardTypeItemPoint());
        theLookupDetail.setDescription("ITEM POINTS");
        theLookupDetail.setHeader(lookupHeaderRepo.findOne(codePropertiesService.getHeaderRewardType()));
        lookupDetailRepo.saveAndFlush(theLookupDetail);


        theLookupDetail = new LookupDetail(codePropertiesService.getDetailRewardTypeExchangeItem());
        theLookupDetail.setDescription("Exchange Item");
        theLookupDetail.setHeader(lookupHeaderRepo.findOne(codePropertiesService.getHeaderRewardType()));
        lookupDetailRepo.saveAndFlush(theLookupDetail);

        //operand
        theLookupHeader = new LookupHeader(codePropertiesService.getHeaderOperands());
        lookupHeaderRepo.saveAndFlush(theLookupHeader);
        
        theLookupDetail = new LookupDetail(codePropertiesService.getDetailOperandEq());
        theLookupDetail.setDescription("EQ");
        theLookupDetail.setHeader(lookupHeaderRepo.findOne(codePropertiesService.getHeaderOperands()));
        lookupDetailRepo.saveAndFlush(theLookupDetail);
        
        //member group field type
        theLookupHeader = new LookupHeader(codePropertiesService.getHeaderFieldTypes());
        lookupHeaderRepo.saveAndFlush(theLookupHeader);

        theLookupDetail = new LookupDetail(codePropertiesService.getDetailFieldTypeString());
        theLookupDetail.setDescription("STRING");
        theLookupDetail.setHeader(lookupHeaderRepo.findOne(codePropertiesService.getHeaderFieldTypes()));
        lookupDetailRepo.saveAndFlush(theLookupDetail);
        
        //payment type cash
        theLookupHeader = new LookupHeader(codePropertiesService.getHeaderPaymentType());
        lookupHeaderRepo.saveAndFlush(theLookupHeader);

        theLookupDetail = new LookupDetail(codePropertiesService.getDetailPaymentTypeCash());
        theLookupDetail.setDescription("paymenttype cash");
        theLookupDetail.setHeader(lookupHeaderRepo.findOne(codePropertiesService.getHeaderPaymentType()));
        lookupDetailRepo.saveAndFlush(theLookupDetail);
        
        theLookupDetail = new LookupDetail(codePropertiesService.getDetailPaymentTypeEft());
        theLookupDetail.setDescription("paymenttype eft");
        theLookupDetail.setHeader(lookupHeaderRepo.findOne(codePropertiesService.getHeaderPaymentType()));
        lookupDetailRepo.saveAndFlush(theLookupDetail);


        //membet type individual

        theLookupHeader = new LookupHeader(codePropertiesService.getHeaderMemberType());
        lookupHeaderRepo.saveAndFlush(theLookupHeader);

        theLookupDetail = new LookupDetail(codePropertiesService.getDetailMemberTypeIndividual());
        theLookupDetail.setDescription("member type individual");
        theLookupDetail.setHeader(lookupHeaderRepo.findOne(codePropertiesService.getHeaderMemberType()));
        lookupDetailRepo.saveAndFlush(theLookupDetail);
        
        //title
        theLookupHeader = new LookupHeader(codePropertiesService.getHeaderTitle());
        theLookupHeader.setDescription("TITLE");
        lookupHeaderRepo.saveAndFlush(theLookupHeader);
        
        theLookupDetail = new LookupDetail(codePropertiesService.getDetailTitleMadam());
        theLookupDetail.setDescription("MADAM");
        theLookupDetail.setHeader(lookupHeaderRepo.findOne(codePropertiesService.getHeaderTitle()));
        lookupDetailRepo.saveAndFlush(theLookupDetail);
        
        theLookupDetail = new LookupDetail(codePropertiesService.getDetailTitleMiss());
        theLookupDetail.setDescription("MISS");
        theLookupDetail.setHeader(lookupHeaderRepo.findOne(codePropertiesService.getHeaderTitle()));
        lookupDetailRepo.saveAndFlush(theLookupDetail);
        //days
        
        theLookupHeader = new LookupHeader(codePropertiesService.getHeaderDays());
        lookupHeaderRepo.saveAndFlush(theLookupHeader);
        
        theLookupDetail = new LookupDetail(codePropertiesService.getDetailDayMonday());
        theLookupDetail.setDescription("MONDAY");
        theLookupDetail.setHeader(lookupHeaderRepo.findOne(codePropertiesService.getHeaderDays()));
        lookupDetailRepo.saveAndFlush(theLookupDetail);
        
        theLookupDetail = new LookupDetail(codePropertiesService.getDetailDayTuesday());
        theLookupDetail.setDescription("TUESDAY");
        theLookupDetail.setHeader(lookupHeaderRepo.findOne(codePropertiesService.getHeaderDays()));
        lookupDetailRepo.saveAndFlush(theLookupDetail);
        
        theLookupDetail = new LookupDetail(codePropertiesService.getDetailDayWednesday());
        theLookupDetail.setDescription("WEDNESDAY");
        theLookupDetail.setHeader(lookupHeaderRepo.findOne(codePropertiesService.getHeaderDays()));
        lookupDetailRepo.saveAndFlush(theLookupDetail);
        
        theLookupDetail = new LookupDetail(codePropertiesService.getDetailDayThursday());
        theLookupDetail.setDescription("THURSDAY");
        theLookupDetail.setHeader(lookupHeaderRepo.findOne(codePropertiesService.getHeaderDays()));
        lookupDetailRepo.saveAndFlush(theLookupDetail);
        
        theLookupDetail = new LookupDetail(codePropertiesService.getDetailDayFriday());
        theLookupDetail.setDescription("FRIDAY");
        theLookupDetail.setHeader(lookupHeaderRepo.findOne(codePropertiesService.getHeaderDays()));
        lookupDetailRepo.saveAndFlush(theLookupDetail);
        
        theLookupDetail = new LookupDetail(codePropertiesService.getDetailDaySaturday());
        theLookupDetail.setDescription("SATURDAY");
        theLookupDetail.setHeader(lookupHeaderRepo.findOne(codePropertiesService.getHeaderDays()));
        lookupDetailRepo.saveAndFlush(theLookupDetail);
        
        theLookupDetail = new LookupDetail(codePropertiesService.getDetailDaySunday());
        theLookupDetail.setDescription("SUNDAY");
        theLookupDetail.setHeader(lookupHeaderRepo.findOne(codePropertiesService.getHeaderDays()));
        lookupDetailRepo.saveAndFlush(theLookupDetail);
        
    }

    private void setupPrograms() {

        program = new Program();
        program.setName( PROGRAM_SEARCH_NAME );
        program.setDescription( PROGRAM_SEARCH_NAME + " Description" );
        program.setStatus(lookupDetailRepo.findByCode(codePropertiesService.getDetailStatusActive()));
        program.setDuration(currentMonth);
        programRepo.save(program);

        //INVALID - no status
        Program theProgram = new Program();
        theProgram.setName( "Program2" );
        theProgram.setDescription( "Program2 Description" );
        theProgram.setDuration(currentMonth);
        programRepo.save(theProgram);

        //Others
        theProgram = new Program();
        theProgram.setName( "Program3" );
        theProgram.setDescription( "Program3 Description" );
        theProgram.setDuration(currentMonth);
        programRepo.save(theProgram);
    }

    private void setupCampaigns() {

        campaign = new Campaign();
        campaign.setStatus(lookupDetailRepo.findByCode(codePropertiesService.getDetailStatusActive()));
        campaign.setDuration(currentMonth);
        campaign.setProgram(program);
        campaign.setName( CAMPAIGN_SEARCH_NAME );
        campaignRepo.save(campaign);

        //INVALID - no status
        Campaign theCampaign = new Campaign();
        theCampaign.setProgram(program);
        theCampaign.setDuration(currentMonth);
        theCampaign.setName( "Program1-Campaign2" );
        campaignRepo.save(theCampaign);

        theCampaign = new Campaign();
        theCampaign.setProgram( programRepo.findOne( QProgram.program.name.eq( "Program2" ) ) );
        theCampaign.setDuration(currentMonth);
        theCampaign.setName( "Program2-Campaign1" );
        campaignRepo.save(theCampaign);

        campaignExchangeItem = new Campaign();
        campaignExchangeItem.setStatus(lookupDetailRepo.findByCode(codePropertiesService.getDetailStatusActive()));
        campaignExchangeItem.setDuration(currentMonth);
        campaignExchangeItem.setProgram(program);
        campaignExchangeItem.setName( CAMPAIGN_EXCHANGE_ITEM );
        campaignRepo.save(campaignExchangeItem);
    }

    private void setupPromotions() {

        promotion = new Promotion();
        promotion.setName( PROMOTION_SEARCH_NAME );
        promotion.setStatus(lookupDetailRepo.findByCode(codePropertiesService.getDetailStatusActive()));
        promotion.setCampaign(campaign);
        StringBuffer affectedDays = new StringBuffer(codePropertiesService.getDetailDayMonday())
        	.append(",").append(codePropertiesService.getDetailDayTuesday())
        	.append(",").append(codePropertiesService.getDetailDayWednesday())
        	.append(",").append(codePropertiesService.getDetailDayThursday())
        	.append(",").append(codePropertiesService.getDetailDayFriday())
        	.append(",").append(codePropertiesService.getDetailDaySaturday())
        	.append(",").append(codePropertiesService.getDetailDaySunday());
        promotion.setAffectedDays(affectedDays.toString());
        promotion.setStartDate(currentMonth.getStartDate());
        promotion.setEndDate(currentMonth.getEndDate());
        promotion.setRewardType(lookupDetailRepo.findByCode(codePropertiesService.getDetailRewardTypePoints()));
        promotion.setStartTime(LocalTime.MIDNIGHT);
        promotion.setEndTime(LocalTime.MIDNIGHT.minusMillis(1));
        
        PromotionReward promotionReward = new PromotionReward();
        promotionReward.setBaseAmount(new BigDecimal(100));
        promotionReward.setBaseFactor(1d);
        promotionReward.setMinAmount(new BigDecimal(200));
        promotionReward.setMaxAmount(new BigDecimal(1000));
        List<PromotionReward> rewards = new ArrayList<PromotionReward>();
        rewards.add(promotionReward);
        
        promotion.setPromotionRewards(rewards);
        promotion = promotionRepo.save(promotion);


        //for exchange item
        promotion2 = new Promotion();
        promotion2.setName( PROMOTION_EXCHANGE_ITEM );
        promotion2.setStatus(lookupDetailRepo.findByCode(codePropertiesService.getDetailStatusActive()));
        promotion2.setCampaign(campaignExchangeItem);
        promotion2.setAffectedDays(affectedDays.toString());
        promotion2.setStartDate(currentMonth.getStartDate());
        promotion2.setEndDate(currentMonth.getEndDate());
        promotion2.setRewardType(lookupDetailRepo.findByCode(codePropertiesService.getDetailRewardTypePoints()));
        promotion2.setStartTime(LocalTime.MIDNIGHT);
        promotion2.setEndTime(LocalTime.MIDNIGHT.minusMillis(1));

        promotion2.setRewardType(lookupDetailRepo.findByCode(codePropertiesService.getDetailRewardTypeExchangeItem()));

        promotion2 = promotionRepo.save(promotion2);


        Promotion thePromo = new Promotion();
        thePromo.setName( "Program2-Campaign1-Promo" );
        thePromo.setDescription( "Program2-Campaign1-Promo" );
        promotionRepo.save( thePromo );
    }


    private void setupPromotion2RewardsItemExchange() {
        PromotionReward promotionReward2 = new PromotionReward();
        promotionReward2.setBaseFactor(5d);

        promotionReward2.setFixed(true);
        promotionReward2.setProductCode("1234567890123");
        List<PromotionReward> rewards  = new ArrayList<PromotionReward>();
        rewards.add(promotionReward2);
        promotion2.setPromotionRewards(rewards);
        promotion2 = promotionRepo.save(promotion2);

    }

    private void setupPromotionItemPoint() {

        promotionItemPoint = new Promotion();
        promotionItemPoint.setName( PROMOTION_SEARCH_NAME + "ITEMPOINT" );
        promotionItemPoint.setStatus(lookupDetailRepo.findByCode(codePropertiesService.getDetailStatusActive()));
        promotionItemPoint.setCampaign(campaign);
        StringBuffer affectedDays = new StringBuffer(codePropertiesService.getDetailDayMonday())
        	.append(",").append(codePropertiesService.getDetailDayTuesday())
        	.append(",").append(codePropertiesService.getDetailDayWednesday())
        	.append(",").append(codePropertiesService.getDetailDayThursday())
        	.append(",").append(codePropertiesService.getDetailDayFriday())
        	.append(",").append(codePropertiesService.getDetailDaySaturday())
        	.append(",").append(codePropertiesService.getDetailDaySunday());
        promotionItemPoint.setAffectedDays(affectedDays.toString());
        promotionItemPoint.setStartDate(currentMonth.getStartDate());
        promotionItemPoint.setEndDate(currentMonth.getEndDate());
        promotionItemPoint.setRewardType(lookupDetailRepo.findByCode(codePropertiesService.getDetailRewardTypeItemPoint()));
        promotionItemPoint.setStartTime(LocalTime.MIDNIGHT);
        promotionItemPoint.setEndTime(LocalTime.MIDNIGHT.minusMillis(1));
        
        PromotionReward promotionReward = new PromotionReward();
        promotionReward.setBaseAmount( new BigDecimal( 100 ) );
        promotionReward.setBaseFactor( 1d );
        promotionReward.setMinQty( new Long( 200 ) );
        promotionReward.setMaxQty( new Long( 1000 ) );
        List<PromotionReward> rewards = new ArrayList<PromotionReward>();
        rewards.add(promotionReward);
        
        promotionItemPoint.setPromotionRewards(rewards);
        promotionItemPoint = promotionRepo.save(promotionItemPoint);


    	PosTransaction transaction = new PosTransaction();
    	transaction.setId("112");

    	PosTxItem item = new PosTxItem();
    	item.setId("11");
    	item.setProductId("111111111111");
    	item.setQuantity( 200d );
    	item.setSalesType(SalesType.SALE.toString());
    	itemPointItems.add(item);
    	
    	item = new PosTxItem();
    	item.setId("12");
    	item.setProductId("121111111111");
    	item.setQuantity( 200d );
    	item.setSalesType(SalesType.SALE.toString());
    	itemPointItems.add(item);

    	transaction.setOrderItems(itemPointItems);
    	posTransactionRepo.save(transaction);
    	
    	Product product = new Product();
    	product.setId("111111111111");
    	product.setItemCode("111111111111");
    	productRepo.save(product);
    	
    	product = new Product();
    	product.setId("121111111111");
    	product.setItemCode("121111111111");
    	productRepo.save(product);
    	
    	promotionItemPoint.setTargetProductGroups(new HashSet<PromotionProductGroup>());
    	
    	ProductGroup productGroup = new ProductGroup();
    	productGroup.setName( "ProductGroupTest" );
    	productGroup.setProductsCfns( "111111111111, 2" );
    	productGroup.setExcludedProducts( "121111111111, 131111111111" );
    	productGroup = productGroupRepo.save(productGroup);

    	PromotionProductGroup promotionProductGroup = new PromotionProductGroup();
    	promotionProductGroup.setPromotion( promotionItemPoint );
    	promotionProductGroup.setProductGroup(productGroup);
    	promotionProductGroup.setId(String.valueOf( promotionItemPoint.getId()) + String.valueOf(productGroup.getId() ) );
    	promotionItemPoint.getTargetProductGroups().add(promotionProductGroupRepo.save(promotionProductGroup));
    }


    private void setupPromotionWithTargetPaymentType() {
    	
    	promotion.setTargetPaymentTypes(new HashSet<PromotionPaymentType>());
        promotion2.setTargetPaymentTypes(new HashSet<PromotionPaymentType>());


        PromotionPaymentType promotionPaymentType = new PromotionPaymentType();
        promotionPaymentType.setPromotion(promotion);
        promotionPaymentType.setLookupDetail(lookupDetailRepo.findByCode(codePropertiesService.getDetailPaymentTypeCash()));
        promotionPaymentType.setId(promotion.getId() + promotionPaymentType.getLookupDetail().getCode());
        promotion.getTargetPaymentTypes().add(promotionPaymentTypeRepo.save(promotionPaymentType));
        promotion2.getTargetPaymentTypes().add(promotionPaymentTypeRepo.save(promotionPaymentType));

        promotionPaymentType = new PromotionPaymentType();
        promotionPaymentType.setPromotion(promotion);
        promotionPaymentType.setLookupDetail(lookupDetailRepo.findByCode(codePropertiesService.getDetailPaymentTypeEft()));
        promotionPaymentType.setId(promotion.getId() + promotionPaymentType.getLookupDetail().getCode());
        promotionPaymentType.setWildcards("123*");
        promotion.getTargetPaymentTypes().add(promotionPaymentTypeRepo.save(promotionPaymentType));
        promotion = promotionRepo.save(promotion);

        promotion2.getTargetPaymentTypes().add(promotionPaymentTypeRepo.save(promotionPaymentType));

        promotion2 = promotionRepo.save(promotion2);



    }

    private void setupPromotionWithTargetStoreGroup() {
    	promotion.setTargetStoreGroups(new HashSet<PromotionStoreGroup>());
        promotion2.setTargetStoreGroups(new HashSet<PromotionStoreGroup>());


        PromotionStoreGroup promotionStoreGroup = new PromotionStoreGroup();

        StoreGroup storeGroup = new StoreGroup();
        storeGroup.setName("ncr");
        storeGroup.setStores("ortigas,makati");
        storeGroup = storeGroupRepo.save(storeGroup);
        
        promotionStoreGroup.setPromotion(promotion);
        promotionStoreGroup.setStoreGroup(storeGroup);
        promotionStoreGroup.setId(String.valueOf(promotion.getId()) + String.valueOf(storeGroup.getId()));
        promotion.getTargetStoreGroups().add(promotionStoreGroupRepo.save(promotionStoreGroup));
        
        promotion = promotionRepo.save(promotion);

        promotion2.getTargetStoreGroups().add(promotionStoreGroupRepo.save(promotionStoreGroup));

        promotion2 = promotionRepo.save(promotion2);



    }

    private void setupPromotionWithTargetMemberGroup() {
    	
    	promotion.setTargetMemberGroups(new HashSet<PromotionMemberGroup>());
        promotion2.setTargetMemberGroups(new HashSet<PromotionMemberGroup>());


        MemberGroup memberGroup = new MemberGroup();
    	memberGroup.setName("member group 1");
    	memberGroup = memberGroupRepo.save(memberGroup);
    	
    	MemberGroupField memberGroupField = new MemberGroupField();
        memberGroupField.setMemberGroup(memberGroup);
        memberGroupField.setName(lookupHeaderRepo.findByCode(codePropertiesService.getHeaderTitle()));
        memberGroupField.setType(lookupDetailRepo.findByCode(codePropertiesService.getDetailFieldTypeString()));
        memberGroupField = memberGroupRepoFieldRepo.save(memberGroupField);
        
        MemberGroupFieldValue fieldValue = new MemberGroupFieldValue();
        fieldValue.setMemberGroupField(memberGroupField);
        fieldValue.setOperand(lookupDetailRepo.findByCode(codePropertiesService.getDetailOperandEq()));
        fieldValue.setValue(codePropertiesService.getDetailTitleMadam());
        memberGroupRepoFieldValueRepo.save(fieldValue);
        
        PromotionMemberGroup promotionMemberGroup = new PromotionMemberGroup();
        promotionMemberGroup.setPromotion(promotion);
        promotionMemberGroup.setMemberGroup(memberGroup);
        promotionMemberGroup.setId(String.valueOf(promotion.getId()) + String.valueOf(memberGroup.getId()));
        promotion.getTargetMemberGroups().add(promotionMemberGroupRepo.save(promotionMemberGroup));

        promotion = promotionRepo.save(promotion);

        promotion2.getTargetMemberGroups().add(promotionMemberGroupRepo.save(promotionMemberGroup));

        promotion2 = promotionRepo.save(promotion2);

    }

    private void setupPromotionWithTargetProductGroup() {
    	PosTransaction transaction = new PosTransaction();
    	transaction.setId("111");

    	List<PosTxItem> items = new ArrayList<PosTxItem>();
    	PosTxItem item = new PosTxItem();
    	item.setId("1");
    	item.setProductId("1");
    	item.setSalesType(SalesType.SALE.toString());
    	items.add(item);
    	
    	item = new PosTxItem();
    	item.setId("2");
    	item.setProductId("2");
    	item.setSalesType(SalesType.SALE.toString());
    	items.add(item);
    	transaction.setOrderItems(items);
    	
    	posTransactionRepo.save(transaction);
    	
    	Product product = new Product();
    	product.setId("1");
    	product.setItemCode("1");
    	productRepo.save(product);
    	
    	product = new Product();
    	product.setId("2");
    	product.setItemCode("2");
    	productRepo.save(product);
    	
    	promotion.setTargetProductGroups(new HashSet<PromotionProductGroup>());
        promotion2.setTargetProductGroups(new HashSet<PromotionProductGroup>());

        ProductGroup productGroup = new ProductGroup();
    	productGroup.setName("coke");
    	productGroup.setBrand("coke");
    	productGroup.setProducts("1,2");
    	productGroup = productGroupRepo.save(productGroup);

    	PromotionProductGroup promotionProductGroup = new PromotionProductGroup();
    	promotionProductGroup.setPromotion(promotion);
    	promotionProductGroup.setProductGroup(productGroup);
    	promotionProductGroup.setId(String.valueOf(promotion.getId()) + String.valueOf(productGroup.getId()));
    	promotion.getTargetProductGroups().add(promotionProductGroupRepo.save(promotionProductGroup));

        promotion = promotionRepo.save(promotion);

        promotion2.getTargetProductGroups().add(promotionProductGroupRepo.save(promotionProductGroup));

        promotion2 = promotionRepo.save(promotion2);

    }

    private void setupMember() {
    	MemberModel member = new MemberModel();
    	member.setAccountId("test");
    	member.setEnabled(true);
    	member.setAccountId("1");
    	member.setContact("0000000000000");
    	member.setPin("1234");
        member.setTotalPoints(100d);
    	member.setUsername("username");
    	CustomerProfile customerProfile = new CustomerProfile();
        customerProfile.setBirthdate(new Date());
    	customerProfile.setTitle(lookupDetailRepo.findByCode(codePropertiesService.getDetailTitleMadam()));
    	member.setCustomerProfile(customerProfile);
    	
    	memberRepo.save(member);
    }
    

    @Test
    public void testProcessSearch() {
    	PromotionSearchDto theDto = new PromotionSearchDto();

    	theDto.setModelType( PromoModelType.PROGRAM.toString() );
    	Assert.assertEquals( 3, promotionService.processSearch( theDto ).size() );

    	theDto.setName( PROGRAM_SEARCH_NAME );
    	Assert.assertEquals( 1, promotionService.processSearch( theDto ).size() );

    	theDto.setModelType( PromoModelType.CAMPAIGN.toString() );
    	theDto.setName( null );
    	Assert.assertEquals( 2, promotionService.processSearch( theDto ).size() );

    	//expected: program's Campaign1, Program2-Campaign1
    	theDto.setName( CAMPAIGN_SEARCH_NAME );
    	Assert.assertEquals( 2, promotionService.processSearch( theDto ).size() );

    	theDto.setName( CAMPAIGN_SEARCH_NAME + "xxxx" );
    	Assert.assertEquals( true, CollectionUtils.isEmpty( promotionService.processSearch( theDto ) )  );

    	theDto.setModelType( PromoModelType.PROMOTION.toString() );
    	theDto.setName( null );
    	Assert.assertEquals( 1, promotionService.processSearch( theDto ).size() );

    	theDto.setName( PROMOTION_SEARCH_NAME + "xxxx" );
    	Assert.assertEquals( true, CollectionUtils.isEmpty( promotionService.processSearch( theDto ) )  );
    }
}
