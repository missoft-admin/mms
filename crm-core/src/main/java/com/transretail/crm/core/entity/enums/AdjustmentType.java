package com.transretail.crm.core.entity.enums;

public enum AdjustmentType {

	CREDIT, DEBIT

}
