<%@include file="/WEB-INF/views/common/taglibs.jsp" %>

<style type="text/css">
  <!--
  .well .input {
    background-color: #fff;
    border: 1px solid #E8E8E8;
    box-shadow: none;    
    padding: 6px;
    height: 18px;
    margin-left: 5px;
  }
  .well .btn-primary {
    margin-left: 10px;
  }
  .well select, .well .input {
      margin: 5px;
  }
  -->
</style>
<spring:message code="gc.poslose" var="typeName" />

<div class="page-header page-header2">
  <h1>${typeName}</h1>
</div>

<div class="well clearfix">
  <form:form id="searchForm">
    <div class="form-inline form-search">
      <label class="label-single">
        <h5><spring:message code="entity_search" />:</h5>
      </label>
      <select id="searchCategory" class="searchField">
      	<c:forEach items="${searchcateg}" var="categ">
      	  <option value="${categ.key}">${categ.value}</option>
      	</c:forEach>
      </select>
      <input id="searchField" type="text" class="input searchField"/>
      <input type="button" id="searchButton" class="btn btn-primary ml5" value="<spring:message code="button_search"/>">
      <input type="button" id="clearButton" value="<spring:message code="label_clear" />" class="btn custom-reset" data-default-id="searchButton" />
    </div>

    <div class="form-inline form-search">    
      <label class="label-single">
        <!--<h5><spring:message code="gc.poslose.search.create" />:</h5>-->
      </label>
      <input type="text" id="searchDateFrom" name="dateFrom" class="input searchField" placeholder="<spring:message code="label_from" />"/>
      <input type="text" id="searchDateTo" name="dateTo" class="input searchField" placeholder="<spring:message code="label_to" />" />
    </div>
  </form:form>
</div>

<div class="pull-right mb20">  
  <button type="button" id="createPosLoseTransaction" class="modalBtn btn btn-primary"
	  data-url="<spring:url value="/giftcard/pos-lose-tx/new" />"><spring:message code="global_menu_new" arguments="${typeName}"/>
  </button>	
</div>

<div id="transactionList">
  <div id="transactionContent" 
       data-url="<spring:url value="/giftcard/pos-lose-tx/search" />"
       data-hdr-cashierno="<spring:message code="gc.poslose.label.cashier.no" />"
       data-hdr-posno="<spring:message code="gc.poslose.label.pos.no" />"
       data-hdr-transactionno="<spring:message code="gc.poslose.label.transaction.no" />"
       data-hdr-store="<spring:message code="gc.poslose.label.store" />"
       data-hdr-transactiondatetime="<spring:message code="gc.poslose.label.date.time" />"
       data-hdr-transaction-type="<spring:message code="gc.poslose.label.type" />"
       />
</div>
<div id="buttonContainer" class="hide">
  <div id="viewButton">
    <input type="button" data-url="<c:url value="/giftcard/pos-lose-tx/view"/>/%transactionNo%" class="btn modalBtn icn-view tiptip pull-left" title="<spring:message code="label_view" />">
  </div>
</div>

<div class="modal hide groupModal nofly" tabindex="1" role="dialog" aria-hidden="true" id="dialogBox"></div>

<link href="<spring:url value="/css/dataTables.css" />" rel="stylesheet">
<script src='<c:url value="/js/bootstrap/bootstrap-datepicker.js"/>' type='text/javascript'><![CDATA[&nbsp; ]]></script>
<script src='<c:url value="/js/bootstrap/bootstrap-timepicker.min.js"/>' type='text/javascript'><![CDATA[&nbsp; ]]></script>
<link href="<c:url value="/css/bootstrap/datepicker.css" />" rel="stylesheet" />
<script src="<spring:url value="/js/jquery/jquery.dataTables.js" />" ></script>
<script src="<spring:url value="/js/jquery/jquery.dataTables.linkLengthMenu.js" />"></script>
<script src="<spring:url value="/js/jquery/jquery.dataTables.customPaging.js" />"></script>
<script src="<spring:url value="/js/jquery/ajax-datatable.js" />" ></script>
<script src="<spring:url value="/js/common/form2js.js" />" ></script>
<script src="<spring:url value="/js/common/json2.js" />" ></script>
<script src="<spring:url value="/js/jquery/jquery.toObject.js" />" ></script>

<script type="text/javascript">
  $(document).ready(function() {
//    $posLoseList = $("#posLoseTransactionContent");
    $posLoseList = $("#transactionContent");
    $dialog = $("#dialogBox");
    $dialog2 = $('#posLoseTransactionDialog');
    $dialog.modal({
      show: false
    }).on('hidden.bs.modal', function() {
      $(this).empty();
    });

    $("#searchDateFrom").datepicker({
      autoclose: true, format: "dd-mm-yyyy"
    }).on('changeDate', function(selected) {
      startDate = new Date(selected.date.valueOf());
      startDate.setDate(startDate.getDate(new Date(selected.date.valueOf())));
      $('#searchDateTo').datepicker('setStartDate', startDate);
    });

    $("#searchDateTo").datepicker({
    	autoclose: true, format: "dd-mm-yyyy"
    }).on('changeDate', function(selected) {
      FromEndDate = new Date(selected.date.valueOf());
      FromEndDate.setDate(FromEndDate.getDate(new Date(selected.date.valueOf())));
      $('#searchDateFrom').datepicker('setEndDate', FromEndDate);
    });

    $("#createPosLoseTransaction").click(showModal);

    var headers = [
      "Created By", "Booked Date", //TODO:
      $posLoseList.data("hdrTransactiondatetime"),
      $posLoseList.data("hdrTransactionno"),
      $posLoseList.data("hdr-transaction-type"),
      $posLoseList.data("hdrStore"),
      $posLoseList.data("hdrPosno"),
      $posLoseList.data("hdrCashierno"),
      ""
    ];

    var modelFields = [
      {name: 'createdBy', sortable: true},
      {name: 'bookedDate', sortable: true},
      {name: 'transactionDate', sortable: true},
      {name: 'transactionNo', sortable: true},
      {name: 'transactionType', sortable: true},
      {name: 'merchantId', sortable: true, fieldCellRenderer: function(data, type, row) {
	  return row.merchant.codeAndName;
	}
      },
      {name: 'terminalId', sortable: true},
      {name: 'cashierId', sortable: true},
      {customCell: function(innerData, sSpecific, json) {
	  if (sSpecific === 'display') {
	    var btnHtml = $("#viewButton").html();

	    var rg = new RegExp("%transactionNo%", 'g');
	    btnHtml = btnHtml.replace(rg, json.transactionNo);

	    return btnHtml;
	  }
	  else {
	    return '';
	  }
	}}
    ];

    $posLoseList.ajaxDataTable({
      'autoload': true,
      'ajaxSource': $posLoseList.data("url"),
      'aaSorting': [[1, 'desc'], [2, 'desc']],
      'columnHeaders': headers,
      'modelFields': modelFields
    })
	    .on('click', '.modalBtn', showModal);

    $("#clearButton").click(function() {
      $("#searchField").val("");
      $("#searchDateFrom").val("");
      $("#searchDateTo").val("");
      $("#status").val("");
    });

    $("#searchButton").click(function() {
      $("#searchField").attr('name', $("#searchCategory").val());
      var formDataObj = $("#searchForm").toObject({mode: 'first', skipEmpty: true});
      $btnSearch = $(this);
      $btnSearch.prop('disabled', true);
      console.log(formDataObj);
      $posLoseList.ajaxDataTable('search', formDataObj, function() {
	$btnSearch.prop('disabled', false);
      });
    });

    function showModal(e) {
      e.preventDefault();
      $.get($(this).data("url"), function(data) {
		var prev = "";
		$dialog.modal('show').html(data);
		$dialog.on('hidden.bs.modal', function(e) {
		  console.log(prev);
		  if ($(e.target).attr('id') != 'dialogBox') {
		    $dialog.modal('show').html(data);
		  }
		  else {
		    $dialog.empty();
		  }
		});
      }, "html");
    }
  });


  function ajaxSubmit(action, data, fn) {
    $.ajax({
      type: "POST",
      url: action,
      dataType: 'json',
      data: data,
      success: function(inResponse) {
	if (inResponse.success) {
	  fn(inResponse);
	}
	else {
	  errorInfo = "";
	  for (i = 0; i < inResponse.result.length; i++) {
	    errorInfo += "<br>" + (i + 1) + ". "
		    + (inResponse.result[i].code != undefined ?
			    inResponse.result[i].code : inResponse.result[i]);
	  }
	  $("#containerError").html("Please correct following errors: " + errorInfo);
	  $("#contentError").show('slow');
	}
      },
      error: function(jqXHR, textStatus, errorThrown) {
	$("#contentError").html("Error: " + errorThrown);
      }
    });
  }
</script>