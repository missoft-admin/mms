<%@ include file="../../common/taglibs.jsp" %>

<c:set var="ENCTYPE" value="application/x-www-form-urlencoded"/>
<div class="modal hide nofly" tabindex="-1" role="dialog" aria-hidden="true" id="reprintModal">
<div class="modal-dialog">
  <div class="modal-content">
  
    <div class="modal-header">
      <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
      <h4 class="modal-title"><spring:message code="label_reprint_transactions" /></h4>
    </div>
 
    <div class="modal-body">
    
    <div class="errorMessages error">&nbsp;</div>
    
    <form id="reprintTxn" action="<spring:url value="/loyaltycardinventory/reprinttransaction" />" method="POST" id="reprintTxn" class="form-reset form-horizontal row-fluid">
    
    
    <div class="control-group">
      <label for="" class="control-label"><spring:message code="reprint_tranction_type" /></label> 
      <div class="controls">
        <select name="transactionType">
          <c:forEach items="${transactionTypes}" var="item">
            <option value="${item}"><spring:message code="transaction_${item}" /></option>
          </c:forEach>
        </select>
      </div>
    </div>
    
    
    <div class="control-group">
      <spring:message code="reprint_username" var="username" />
      <label for="" class="control-label">${username}</label> 
      <div class="controls">
        <input type="text" name="username" placeholder="${username}" />
      </div>
    </div>
    
    <div class="control-group">
      <spring:message code="reprint_date_from" var="dateFrom" />
      <label for="" class="control-label">${dateFrom}</label> 
      <div class="controls">
        <input type="text" name="dateFrom" placeholder="${dateFrom}" />
      </div>
    </div>
    
    <div class="control-group">
      <spring:message code="reprint_date_to" var="dateTo" />
      <label for="" class="control-label">${dateTo}</label> 
      <div class="controls">
        <input type="text" name="dateTo" placeholder="${dateTo}" />
      </div>
    </div>

    
    </form>
        
    
    </div>
    <div class="modal-footer">
      <button type="button" id="reprintBtn" class="btn btn-primary"><spring:message code="label_print" /></button>
      <button type="button" id="" class="btn btn-default" data-dismiss="modal"><spring:message code="label_cancel"/></button>
    </div>
  </div>
</div>
</div>

<script src="<spring:url value="/js/viewspecific/loyalty/reprinttransactions.js"/>"></script>