package com.transretail.crm.common.reporting.engine.data;

import net.sf.jasperreports.engine.JRDataSource;

/**
 *
 * @author Monte Cillo Co (mco@exist.com)
 */
public interface ReportDataSource extends JRDataSource{
    boolean isEmpty();
}
