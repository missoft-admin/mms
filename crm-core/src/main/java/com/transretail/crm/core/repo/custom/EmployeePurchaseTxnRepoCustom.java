package com.transretail.crm.core.repo.custom;


import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.joda.time.LocalDate;

import com.transretail.crm.core.entity.EmployeePurchaseTxnModel;

public interface EmployeePurchaseTxnRepoCustom {

	List<EmployeePurchaseTxnModel> findAllPendingAdjustments();

	Double findTotalTxnAmountByAccountId(String inEmpId, Calendar inCalFrom,
			Calendar inCalTo);

	List<EmployeePurchaseTxnModel> findAllActiveByIdAndPostingDates(
			String employeeId, Calendar inCalFrom, Calendar inCalTo);

	Double findTotalAmount(Long inMemberId, Date inFromDate, Date inToDate);
	
	List<EmployeePurchaseTxnModel> findTransactionsToday(String accountId);
	
	List<EmployeePurchaseTxnModel> findTransactionsByDate(String accountId, LocalDate date);

}
