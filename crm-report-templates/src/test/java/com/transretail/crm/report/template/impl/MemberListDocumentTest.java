package com.transretail.crm.report.template.impl;

import java.util.Date;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.transretail.crm.common.reporting.jasper.JRProcessor;
import com.transretail.crm.core.dto.MemberSearchDto;
import com.transretail.crm.core.entity.MemberModel;
import com.transretail.crm.core.entity.embeddable.CustomerProfile;
import com.transretail.crm.core.entity.enums.Status;
import com.transretail.crm.core.entity.lookup.LookupDetail;
import com.transretail.crm.core.entity.lookup.LookupHeader;
import com.transretail.crm.core.repo.LookupDetailRepo;
import com.transretail.crm.core.repo.LookupHeaderRepo;
import com.transretail.crm.core.repo.MemberRepo;

/**
 * @author ftopico
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath*:/META-INF/spring/applicationContext*.xml",
    "/META-INF/spring/applicationContext-test.xml"})
public class MemberListDocumentTest {

    @Autowired
    private MemberListDocument memberDocument;
    @Autowired
    private MemberRepo memberRepo;
    @Autowired
    private LookupDetailRepo lookupDetailRepo;
    @Autowired
    private LookupHeaderRepo lookupHeaderRepo;
    
    @Before
    public void setup() {
        
        LookupHeader header = lookupHeaderRepo.saveAndFlush(new LookupHeader("MEM001", "Member Type"));
        lookupDetailRepo.saveAndFlush(new LookupDetail("MTYP001", "Individual", header, Status.ACTIVE));
        lookupDetailRepo.saveAndFlush(new LookupDetail("MTYP003", "Professional", header, Status.ACTIVE));
        
        MemberModel memberModel = new MemberModel();
        memberModel.setFirstName("First name");
        memberModel.setLastName("Last name");
        memberModel.setMiddleName("Middle Name");
        memberModel.setPin("1234");
        memberModel.setPinEncryped(false);
        memberModel.setEmail("anyEmail@gmail.com");
        memberModel.setUsername("username");
        memberModel.setPassword("admin");
        memberModel.setAccountId("12341234");
        memberModel.setContact("012341231213");
        memberModel.setMemberType(lookupDetailRepo.findByCode("MTYP001"));
        
        CustomerProfile customerProfile = new CustomerProfile();
        customerProfile.setBirthdate(new Date());
        memberModel.setCustomerProfile(customerProfile);
        
        memberRepo.saveAndFlush(memberModel);
        
        memberModel = new MemberModel();
        memberModel.setFirstName("First name2");
        memberModel.setLastName("Last name2");
        memberModel.setMiddleName("Middle Name2");
        memberModel.setPin("12345");
        memberModel.setPinEncryped(false);
        memberModel.setEmail("any2Email@gmail.com");
        memberModel.setUsername("username2");
        memberModel.setPassword("admin2");
        memberModel.setAccountId("123412342");
        memberModel.setContact("0123412312132");
        memberModel.setMemberType(lookupDetailRepo.findByCode("MTYP003"));
        
        customerProfile = new CustomerProfile();
        customerProfile.setBirthdate(new Date());
        memberModel.setCustomerProfile(customerProfile);
        
        memberRepo.saveAndFlush(memberModel);
    }
    
    @After
    public void cleanUp() {
        memberRepo.deleteAll();
    }
    
    @Test
    public void createMemberIndividualDocument() {
        MemberSearchDto searchDto = new MemberSearchDto();
        searchDto.setMemberType(new LookupDetail("MTYP001", "Individual"));
        JRProcessor jrProcessor = memberDocument.createJrProcessor(searchDto, 1, "MTYP001", null);
        Assert.assertNotNull(jrProcessor);
    }
    
    @Test
    public void createMemberProfessionalDocument() {
        MemberSearchDto searchDto = new MemberSearchDto();
        searchDto.setMemberType(new LookupDetail("MTYP003", "Professional"));
        JRProcessor jrProcessor = memberDocument.createJrProcessor(searchDto, 1, "MTYP003", null);
        Assert.assertNotNull(jrProcessor);
    }
    
}
