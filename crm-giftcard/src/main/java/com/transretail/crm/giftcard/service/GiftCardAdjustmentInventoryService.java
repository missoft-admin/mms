package com.transretail.crm.giftcard.service;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.transretail.crm.common.reporting.jasper.JRProcessor;
import com.transretail.crm.common.service.dto.request.PageSortDto;
import com.transretail.crm.common.service.dto.response.ResultList;
import com.transretail.crm.common.web.converter.ISODateTimeSerializer;
import com.transretail.crm.common.web.converter.LocalDateTimeSerializer;
import com.transretail.crm.giftcard.dto.GiftCardPhysicalCountDto;
import com.transretail.crm.giftcard.dto.GiftCardPhysicalCountSummaryDto;
import com.transretail.crm.giftcard.entity.GiftCardInventory;
import com.transretail.crm.giftcard.entity.support.GiftCardInventoryStatus;
import java.util.List;
import java.util.Map;
import org.joda.time.LocalDateTime;

/**
 *
 * @author Monte Cillo Co (mco@exist.com)
 */
public interface GiftCardAdjustmentInventoryService {

    boolean isBarcodeExist(String barcode);

    GiftCardInventory getGiftCardInventoryByBarcode(String barcode);

    String getFormattedProductProfile(String barcode);

    boolean validateSeries(String startingSeries, String endingSeries);

    Map<String, GiftCardInventoryStatus> extractGiftCardStatus(String startingSeries, String endingSeries);

    boolean isCounted(String startingSeries, String endingSeries);

    void savePhysicalCount(GiftCardPhysicalCountDto dto);

    ResultList<GiftCardPhysicalCountDto> listPhysicalCount(PageSortDto sortDto);

    void processPhysicalCount();

    ResultList<GiftCardPhysicalCountSummaryDto> getSummary(PageSortDto pageDto);

    ResultList<History> getHistoryList(PageSortDto pageDto);

    List<LocalDateTime> getSummaryDates(long maxResult);

    JRProcessor createJRProcessor();

    JRProcessor createJRProcessor(LocalDateTime createdDate);

    List<GiftCardInventory> findInventoryBySeriesRange(String startingSeries, String endingSeries);

    boolean validateCardByUserInventoryLocation(String startingSeries, String endingSeries);

    Long countInventory(String startingSeries, String endingSeries);

    static class History {

        @JsonSerialize(using = LocalDateTimeSerializer.class)
        private LocalDateTime timePresentation;
        private String isoFormat;

        public LocalDateTime getTimePresentation() {
            return timePresentation;
        }

        public void setTimePresentation(LocalDateTime timePresentation) {
            this.timePresentation = timePresentation;

            this.isoFormat = timePresentation.toString();
        }

        public String getIsoFormat() {
            return isoFormat;
        }

        public void setIsoFormat(String isoFormat) {
            this.isoFormat = isoFormat;
        }
    }
}
