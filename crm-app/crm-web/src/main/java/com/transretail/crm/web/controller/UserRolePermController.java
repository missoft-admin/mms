package com.transretail.crm.web.controller;

import java.util.List;
import java.util.Map;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import com.google.common.collect.Lists;
import com.transretail.crm.core.dto.UserRolePermDto;
import com.transretail.crm.core.dto.UserRolePermRequestDto;
import com.transretail.crm.core.service.UserRolePermissionService;

/**
 * @author Allan G. Ramirez (agramirez@exist.com)
 */
@Controller
@RequestMapping("/user")
public class UserRolePermController {
    @Autowired
    private UserRolePermissionService userRolePermissionService;

    /**
     * List all roles and corresponding permissions if there's any and will also mark
     * {@link com.transretail.crm.core.dto.UserRolePermDto#isCurrentRolePerm} to true if role or permission
     * is current role or permission of current user.
     *
     * @return List which contains all roles, permissions of each role and indicator if user has that role or permission
     */
    @RequestMapping(value = "/{userId}/permroles", method = RequestMethod.GET)
    @ResponseBody
    public List<RolePermissionSetTuple> getUserRolePermissions(@PathVariable("userId") long userId) {
        List<RolePermissionSetTuple> result = Lists.newArrayList();
        Map<UserRolePermDto, Set<UserRolePermDto>> map = userRolePermissionService.listUserRolePermissions(userId);
        for (Map.Entry<UserRolePermDto, Set<UserRolePermDto>> entry : map.entrySet()) {
            result.add(new RolePermissionSetTuple(entry.getKey(), entry.getValue()));
        }
        return result;
    }

    /**
     * Associate the dash separated (role-permission) tuple to current user
     *
     * @param dto UserRolePermRequestDto with list of dash separated list of role permission. Eg. ROLE_ADMIN-LOOKUP_MANAGEMENT
     */
    @RequestMapping(value = "/{userId}/permroles/update", method = RequestMethod.POST)
    @ResponseStatus(HttpStatus.OK)
    public void saveUserRolePermissions(@PathVariable("userId") long userId, @RequestBody UserRolePermRequestDto dto) {
        userRolePermissionService.saveUserRolePerm(userId, dto.getRolePermTuples());
    }

    public static class RolePermissionSetTuple {
        private UserRolePermDto role;
        private Set<UserRolePermDto> permissions;

        public RolePermissionSetTuple(UserRolePermDto role, Set<UserRolePermDto> permissions) {
            this.role = role;
            this.permissions = permissions;
        }

        public Set<UserRolePermDto> getPermissions() {
            return permissions;
        }

        public void setPermissions(Set<UserRolePermDto> permissions) {
            this.permissions = permissions;
        }

        public UserRolePermDto getRole() {
            return role;
        }

        public void setRole(UserRolePermDto role) {
            this.role = role;
        }
    }
}
