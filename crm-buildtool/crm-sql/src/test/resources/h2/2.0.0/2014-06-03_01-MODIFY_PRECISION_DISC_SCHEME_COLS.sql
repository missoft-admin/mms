alter table CRM_GC_DISCOUNT_SCHEME alter column MIN_PURCHASE decimal(19,5);
alter table CRM_GC_DISCOUNT_SCHEME alter column PURCHASE_FOR_MAX_DISC decimal(19,5);