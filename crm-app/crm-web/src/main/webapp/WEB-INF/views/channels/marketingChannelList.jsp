<%@include file="../common/taglibs.jsp" %>


<div id="content_marketingChannel">

  <div id="list_marketingChannel" 
      data-url="<c:url value="/marketing/channel/list" />" 
      data-hdr-code="<spring:message code="channel_code"/>" 
      data-hdr-description="<spring:message code="channel_description"/>" 
      data-hdr-schedule="<spring:message code="channel_schedule"/>" 
      data-hdr-type="<spring:message code="channel_type"/>" 
      data-hdr-status="<spring:message code="channel_status"/>" ></div>

  <div class="btns hide">
    <a href="<c:url value="/marketing/channel/edit/%ID%" />" class="btn pull-left tiptip icn-edit editMarketingChannelBtn" 
      title="<spring:message code="label_edit" />" data-type="%TYPE%" ><spring:message code="label_edit" /></a>
    <a href="<c:url value="/marketing/channel/update/%ID%?status=%STATUS%" />" class="btn pull-left tiptip icn-edit updateMarketingChannelStatBtn" 
      title="<spring:message code="label_submit_for_approval" />"><spring:message code="label_submit_for_approval" /></a>
    <a href="<c:url value="/marketing/channel/view/%ID%" />" class="btn pull-left tiptip icn-view viewMarketingChannelBtn" 
      title="<spring:message code="label_view" />"><spring:message code="label_view" /></a>
    <a href="<c:url value="/marketing/channel/view/%ID%?process=true" />" class="btn pull-left tiptip icn-approve processMarketingChannelBtn" 
      title="<spring:message code="label_process" />"><spring:message code="label_view" /></a>
  </div>

  <sec:authorize var="head" ifAnyGranted="ROLE_MARKETING_HEAD" />
  <sec:authorize var="staff" ifAnyGranted="ROLE_MARKETING" />
  <div id="constants" class="hide" 
      data-stat-draft="${draft}" data-stat-forapproval="${forapproval}" data-stat-active="${active}" data-stat-canceled="${canceled}" data-stat-active="${rejected}" 
      data-role-staff="${staff}" data-role-head="${head}"
      data-statdisp-forapproval="<spring:message code="label_forapproval" />" ></div>
  <div id="view_marketingChannel"></div>


<link href="<spring:url value="/css/dataTables.css" />" rel="stylesheet">
<script src="<spring:url value="/js/jquery/jquery.dataTables.js" />" ></script>
<script src="<spring:url value="/js/jquery/jquery.dataTables.linkLengthMenu.js" />"></script>
<script src="<spring:url value="/js/jquery/jquery.dataTables.customPaging.js" />"></script>
<script src="<spring:url value="/js/jquery/ajax-datatable.js" />" ></script>
<script src="<spring:url value="/js/common/form2js.js" />" ></script>
<script src="<spring:url value="/js/common/json2.js" />" ></script>
<script src="<spring:url value="/js/jquery/jquery.toObject.js" />" ></script>

<script type="text/javascript">
var MarketingChannelList = null;

$(document).ready( function() {

    var $container = $( "#content_marketingChannel" );
    var $btns = $container.find( ".btns" );
	  var $list = $( "#list_marketingChannel" );

	  var $constants = $container.find( "#constants" );
	  var DRAFT = $constants.data( "stat-draft" );
	  var FORAPPROVAL = $constants.data( "stat-forapproval" );
    var ACTIVE = $constants.data( "stat-active" );
    var CANCELED = $constants.data( "stat-canceled" );
    var isStaff = $constants.data( "role-staff" );
    var isHead = $constants.data( "role-head" );
	
	  initDataTable();
	
	  function initDataTable() {
        $list.ajaxDataTable({
		        'autoload'    : true,
		        'ajaxSource'  : $list.data( "url" ),
		        'columnHeaders' : [ 
		            $list.data( "hdr-code" ),
                $list.data( "hdr-description" ),
                $list.data( "hdr-schedule" ),
                $list.data( "hdr-type" ),
                $list.data( "hdr-status" ),
				        { text: "", className : "span3" }
			      ],
			      'modelFields'  : [
				        { name  : 'code', sortable : true },
				        { name  : 'description', sortable : true },
                { name  : 'schedule', sortable : true, fieldCellRenderer : function (data, type, row) {
                    return ( (row.dispSchedule)? new Date( row.dispSchedule ).customize(1) : "" )
                        + " " + row.dispTime;
                }},
                { name  : 'type', sortable : true },
                { name  : 'status', sortable : true, fieldCellRenderer : function (data, type, row) {
                    return ( retrieveDispStatus(row.status)? retrieveDispStatus(row.status) : row.status );
                }},
		            { customCell : 
		                function ( innerData, sSpecific, json ) {
		                    var btns = "";
		                    if ( sSpecific == 'display' ) {
		                    	  if ( isStaff && json.status == DRAFT ) {
		                    		    btns += $btns.find( ".editMarketingChannelBtn" ).prop( "outerHTML" )
		                                .replace( new RegExp( "%ID%", "g" ), json.id ).replace( new RegExp( "%TYPE%", "g" ), json.type );
		                    	  }
		                    	  if ( isStaff && json.status == DRAFT ) {
		                    		    btns += $btns.find( ".updateMarketingChannelStatBtn" ).prop( "outerHTML" )
		                                .replace( new RegExp( "%ID%", "g" ), json.id ).replace( new RegExp( "%STATUS%", "g" ), "FORAPPROVAL" );
		                    	  }
	                          if ( ( isStaff && json.status == FORAPPROVAL ) || ( isHead && json.status == DRAFT ) || ( isHead && json.status == ACTIVE ) ) {
	                              btns += $btns.find( ".viewMarketingChannelBtn" ).prop( "outerHTML" )
	                                  .replace( new RegExp( "%ID%", "g" ), json.id );
	                          }
	                          if ( ( isHead && json.status == FORAPPROVAL ) ) {
                                btns += $btns.find( ".processMarketingChannelBtn" ).prop( "outerHTML" )
                                    .replace( new RegExp( "%ID%", "g" ), json.id );
	                          }
		                    }
		                    return btns;
							      }
		            }
            ]
        })
        .on( "click", ".editMarketingChannelBtn", edit )
        .on( "click", ".updateMarketingChannelStatBtn", updateStatus )
        .on( "click", ".viewMarketingChannelBtn", view )
        .on( "click", ".processMarketingChannelBtn", viewProcess );
	  }

    function view(e) {
        e.preventDefault();
        $.get( $(this).attr( "href" ), function( resp ) { 
        	  $( "#view_marketingChannel" ).html( resp );
        	  MarketingChannelView.show( resp ); 
        }, "html" );
    }

    function viewProcess(e) {
        e.preventDefault();
        $.get( $(this).attr( "href" ), function( resp ) {
            $( "#view_marketingChannel" ).html( resp );
            MarketingChannelProcess.show( resp ); 
        }, "html" );
    }

    function edit(e) {
        e.preventDefault();

        var isEmail = $(this).data( "type" ) == "email";
        var isSms = $(this).data( "type" ) == "sms";

        $.get( $(this).attr( "href" ), function( resp ) {
            if ( isEmail ) {
                MarketingChannel.loadEmailDraftForm( resp ); 
            }
            else if ( isSms ) {
                MarketingChannel.loadSmsDraftForm( resp ); 
            }
        }, "html" );
    }

    function updateStatus(e) {
        e.preventDefault();
        $.post( $(this).attr( "href" ), function( resp ) { if ( resp.success ) { MarketingChannelList.reloadTable(); } });
    }

    function retrieveDispStatus( stat ) {
    	  switch( stat ) {
    	      case FORAPPROVAL: return $constants.data( "statdisp-forapproval" ).toUpperCase();
    	      default: return "";
    	  }
    }

    MarketingChannelList = {
        reloadTable  : function() { $list.ajaxDataTable( "search" ); },
        filterTable  : function( filterForm, retFunction ) { $list.ajaxDataTable( "search", filterForm, retFunction ); }
    };

});
</script>


</div>