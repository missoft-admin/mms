drop table CRM_GC_RETURN_BY_CARDS cascade constraints;
    drop table CRM_GC_RETURN_BY_CARDS_ITEM cascade constraints;

create table CRM_GC_RETURN_BY_CARDS_ITEM (
        id number(19,0) not null,
        CREATED_BY varchar2(255 char),
        CREATED_DATETIME timestamp,
        LAST_UPDATED_BY varchar2(255 char),
        LAST_UPDATED_DATETIME timestamp,
        ENDING_SERIES varchar2(255 char),
        STARTING_SERIES varchar2(255 char),
        TYPE varchar2(255 char),
        REC number(19,0),
        primary key (id)
    );


alter table CRM_GC_RETURN add (REASON varchar2(15 char));


alter table CRM_GC_RETURN 
        add constraint FK_82ue4gofqbehbs6dnp90dxym8 
        foreign key (REASON) 
        references CRM_REF_LOOKUP_DTL;

    alter table CRM_GC_RETURN_BY_CARDS_ITEM 
        add constraint FK_svxax4cvdnrfryjxt26363ald 
        foreign key (REC) 
        references CRM_GC_RETURN;
