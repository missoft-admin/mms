package com.transretail.crm.web.controller;

import org.junit.Before;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

/**
 *
 */
public abstract class AbstractMockMvcTest extends AbstractMvcTest {
    @Autowired
    protected WebApplicationContext wac;
    protected MockMvc mockMvc;

    @Before
    public final void onSetup() throws Exception {
        mockMvc = MockMvcBuilders.webAppContextSetup(wac).build();
        doSetup();
    }

    protected void doSetup() throws Exception {

    }

}
