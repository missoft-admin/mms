package com.transretail.crm.web.member.validator;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import com.transretail.crm.core.service.CodePropertiesService;
import com.transretail.crm.core.service.MemberService;
import com.transretail.crm.web.member.bean.MemberDto;

/**
 * @author Allan G. Ramirez (agramirez@exist.com)
 */
@Component("memberUpdateValidator")
public class MemberUpdateValidator implements Validator {
    @Autowired
    protected MemberService memberService;
    @Autowired
    private CodePropertiesService codePropertiesService;

    @Override
    public boolean supports(Class<?> clazz) {
        return MemberDto.class.equals(clazz);
    }

    @Override
    public void validate(Object target, Errors errors) {
        MemberDto form = (MemberDto) target;
        for (String contact : form.getContact()) {
            if (memberService.contactExists(form.getUsername(), contact)) {
                errors.rejectValue("contact", "member.contact.exists");
            }

            String c = contact.replaceAll("(\\s)|(-)", "");
            if (!c.matches("\\d*")) {
                errors.rejectValue("contact", "member.contact.incorrect");
            }
        }
        if ( StringUtils.isNotBlank( form.getHomePhone() ) && !form.getHomePhone().replaceAll("(\\s)|(-)", "").matches("\\d*") ) {
            errors.rejectValue("homePhone", "member.homephone.incorrect");
        }
        if (memberService.emailExists(form.getUsername(), form.getEmail())) {
            errors.rejectValue("email", "member.email.exists");
        }

        if (StringUtils.isNotBlank(form.getKtpId())) {
            if (memberService.ktpIdExists(form.getUsername(), form.getKtpId())) { // Task #85818
                errors.rejectValue("ktpId", "member.ktpId.exists");
            }
        }
        
        if (codePropertiesService.getDetailMemberTypeProfessional().equals(form.getMemberTypeCode()) && StringUtils.isBlank(form.getIdNumber())) {
            errors.rejectValue("idNumber", "NotEmpty.idNumber");
        }

        if ( null != form.getNpwp() && StringUtils.isNotBlank( form.getNpwp().getNpwpId() ) 
				&& !StringUtils.containsOnly( form.getNpwp().getNpwpId(), "0" ) ) {
        	com.transretail.crm.core.dto.MemberDto dto = new com.transretail.crm.core.dto.MemberDto();
        	dto.setAccountId( form.getAccountId() );
        	dto.setNpwpId( form.getNpwp().getNpwpId() );
        	dto.setParent( form.getParentAccountId() );
        	if ( memberService.findDuplicateNpwpId( dto ) != null ) {
                errors.rejectValue("npwp.npwpId", "member.npwpId.exists");
        	}
        }
    }
}
