package com.transretail.crm.report.template.impl;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.google.common.collect.Sets;
import com.mysema.query.jpa.impl.JPAQuery;
import com.mysema.query.types.Projections;
import com.mysema.query.types.QBean;
import com.mysema.query.types.expr.BooleanExpression;
import com.mysema.query.types.expr.CaseBuilder;
import com.mysema.query.types.expr.NumberExpression;
import com.mysema.query.types.path.StringPath;
import com.transretail.crm.common.reporting.engine.data.JRQueryDSLDataSource;
import com.transretail.crm.common.reporting.jasper.ClasspathFileResolver;
import com.transretail.crm.common.reporting.jasper.DefaultJRProcessor;
import com.transretail.crm.common.reporting.jasper.JRProcessor;
import com.transretail.crm.common.util.DateUtil;
import com.transretail.crm.core.entity.QMemberModel;
import com.transretail.crm.core.entity.QPointsTxnModel;
import com.transretail.crm.core.entity.enums.ReportType;
import com.transretail.crm.core.entity.lookup.QLookupDetail;
import com.transretail.crm.core.entity.lookup.QStore;
import com.transretail.crm.report.template.CommonReportFilter;
import com.transretail.crm.report.template.FilterField;
import com.transretail.crm.report.template.NoDataFoundSupport;
import com.transretail.crm.report.template.ReportTemplate;
import org.apache.commons.lang.StringUtils;
import org.joda.time.LocalDate;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;

/**
 * @author Mike de Guzman
 */
@Service("MaritalStatusContributionReport")
public class MaritalStatusContributionReport implements ReportTemplate, NoDataFoundSupport {

    private static Logger LOG = LoggerFactory.getLogger(MaritalStatusContributionReport.class);

    protected static final String TEMPLATE_NAME = "Member Contribution By Marital Status Report";
    protected static final String REPORT_NAME = "reports/member_contribution_by_marital_status_report.jasper";

    protected static final String FILTER_TXN_DATE_FROM = CommonReportFilter.DATE_FROM;
    protected static final String FILTER_TXN_DATE_TO = CommonReportFilter.DATE_TO;
    protected static final String FILTER_CUSTOMER_TYPE = "customerType";
    protected static final String FILTER_STORE = "store";

    private static final DateTimeFormatter INPUT_DATE_PATTERN = DateTimeFormat.forPattern("dd-MM-yyyy");

    @PersistenceContext
    private EntityManager em;

    @Autowired
    private MessageSource messageSource;

    @Override
    public String getName() {
        return TEMPLATE_NAME;
    }

    @Override
    public String getDescription() {
        return messageSource.getMessage("marital.status.contribution.report.label", (Object[]) null, LocaleContextHolder.getLocale());
    }

    @Override
    public Set<FilterField> getFilters() {
        Set<FilterField> filters = Sets.newLinkedHashSet();
        filters.add(FilterField.createDateField(FILTER_TXN_DATE_FROM,
                messageSource.getMessage("label_from", (Object[]) null, LocaleContextHolder.getLocale()),
                DateUtil.SEARCH_DATE_FORMAT, true));
        filters.add(FilterField.createDateField(FILTER_TXN_DATE_TO,
                messageSource.getMessage("label_to", (Object[]) null, LocaleContextHolder.getLocale()),
                DateUtil.SEARCH_DATE_FORMAT, true));

        QStore store = QStore.store;
        Map<String, String> stores = Maps.newLinkedHashMap();
        stores.put("", "");
        stores.putAll(new JPAQuery(em).from(store).distinct().orderBy(
                store.code.asc()).map(store.code, store.code.concat(" - ").concat(store.name)));

        filters.add(FilterField.createDropdownField(
                FILTER_STORE, messageSource.getMessage("label_store", (Object[]) null, LocaleContextHolder.getLocale()), stores));

        QLookupDetail lookupDetail = QLookupDetail.lookupDetail;
        Map<String, String> customerTypes = Maps.newLinkedHashMap();
        customerTypes.put("", "");
        customerTypes.putAll(new JPAQuery(em).from(lookupDetail).where(lookupDetail.header.code.eq("MEM007")).map(lookupDetail.code, lookupDetail.description));

        filters.add(FilterField.createDropdownField(
                FILTER_CUSTOMER_TYPE, messageSource.getMessage("label_customertype", null, LocaleContextHolder.getLocale()), customerTypes));
        return filters;
    }

    @Override
    public boolean canView(Set<String> permissions) {
        return true;
    }

    @Override
    public JRProcessor createJrProcessor(Map<String, String> map) {
        String storeCode = map.get(FILTER_STORE);
        String customerType = map.get(FILTER_CUSTOMER_TYPE);

        LocalDate dateFrom = parseDate(map.get(FILTER_TXN_DATE_FROM));
        LocalDate dateTo = parseDate(map.get(FILTER_TXN_DATE_TO));
        DefaultJRProcessor jrProcessor = new DefaultJRProcessor(REPORT_NAME);
        Map<String, Object> reportParams = prepareReportParameters(dateFrom, dateTo);

        // logic to put dataset into reportParams
        QPointsTxnModel points = QPointsTxnModel.pointsTxnModel;
        QMemberModel member = QMemberModel.memberModel;
        QStore store = QStore.store;

        Long nullValue = null;
        StringPath maritalStatus = member.customerProfile.maritalStatus.code;
        NumberExpression<Long> singleCount = new CaseBuilder()
                .when(maritalStatus.eq("MARI001")).then(points.memberModel.id)
                .otherwise(nullValue);
        NumberExpression<Long> marriedCount = new CaseBuilder()
                .when(maritalStatus.eq("MARI002")).then(points.memberModel.id)
                .otherwise(nullValue);
        NumberExpression<Long> widowerCount = new CaseBuilder()
                .when(maritalStatus.eq("MARI003")).then(points.memberModel.id)
                .otherwise(nullValue);

        JPAQuery query = createQuery(map)
                .groupBy(points.storeCode, store.name)
                .orderBy(points.storeCode.asc());

        QBean<MaritalStatusContributionReportBean> projection = Projections.bean(MaritalStatusContributionReportBean.class,
                points.storeCode.as("storeCode"),
                store.name.as("storeName"),
                singleCount.countDistinct().as("singleNo"),
                marriedCount.countDistinct().as("marriedNo"),
                widowerCount.countDistinct().as("widowerNo")
        );

        reportParams.put("SUB_DATA_SOURCE", new JRQueryDSLDataSource(query, projection));
        jrProcessor.addParameters(reportParams);
        jrProcessor.setFileResolver(new ClasspathFileResolver("reports"));
        return jrProcessor;
    }

    @Override
    public ReportType[] getReportTypes() {
        return ReportType.values();
    }

    private LocalDate parseDate(String dateInstance) {
        return INPUT_DATE_PATTERN.parseLocalDate(dateInstance);
    }

    public Map<String, Object> prepareReportParameters(LocalDate dateFrom, LocalDate dateTo) {
        Map<String, Object> reportMap = Maps.newHashMap();
        Locale locale = LocaleContextHolder.getLocale();
        reportMap.put("REPORT_TYPE", messageSource.getMessage("marital.status.contribution.report.label", null, locale));
        reportMap.put("ROW_NUMBER", messageSource.getMessage("marital.status.contribution.report.rownum", null, locale));
        reportMap.put("STORE_CODE", messageSource.getMessage("marital.status.contribution.report.storecode", null, locale));
        reportMap.put("STORE_NAME", messageSource.getMessage("marital.status.contribution.report.storename", null, locale));
        reportMap.put("SINGLE_NO", messageSource.getMessage("marital.status.contribution.report.singlenum", null, locale));
        reportMap.put("MARRIED_NO", messageSource.getMessage("marital.status.contribution.report.marriednum", null, locale));
        reportMap.put("WIDOWER_NO", messageSource.getMessage("marital.status.contribution.report.widowernum", null, locale));
        reportMap.put("TOTAL_NO", messageSource.getMessage("marital.status.contribution.report.totalnum", null, locale));
        reportMap.put("SINGLE_RATE", messageSource.getMessage("marital.status.contribution.report.singerate", null, locale));
        reportMap.put("MARRIED_RATE", messageSource.getMessage("marital.status.contribution.report.marriedrate", null, locale));
        reportMap.put("WIDOWER_RATE", messageSource.getMessage("marital.status.contribution.report.widowerrate", null, locale));
        reportMap.put("DATE_FROM", dateFrom.toString(INPUT_DATE_PATTERN));
        reportMap.put("DATE_TO", dateTo.toString(INPUT_DATE_PATTERN));
        return reportMap;
    }

    @Override
    public boolean isEmpty(Map<String, String> map) {
        return !createQuery(map).exists();
    }

    private JPAQuery createQuery(Map<String, String> map) {
        LocalDate dateFrom = parseDate(map.get(FILTER_TXN_DATE_FROM));
        LocalDate dateTo = parseDate(map.get(FILTER_TXN_DATE_TO));
        String storeCode = map.get(FILTER_STORE);
        String customerType = map.get(FILTER_CUSTOMER_TYPE);

        QPointsTxnModel points = QPointsTxnModel.pointsTxnModel;
        QMemberModel member = QMemberModel.memberModel;
        QStore store = QStore.store;

        List<BooleanExpression> exprs = Lists.newArrayList();
        exprs.add(points.transactionDateTime.between(dateFrom.toDate(), dateTo.toDate()));

        if (StringUtils.isNotBlank(storeCode)) {
            exprs.add(points.storeCode.eq(storeCode));
        }

        if (StringUtils.isNotBlank(customerType)) {
            exprs.add(member.memberType.code.eq(customerType));
        }

        // single.code=MARI001
        // married.code=MARI002
        // divorce.code=MARI003 == WIDOWER?

        JPAQuery query = new JPAQuery(em).from(store, points)
                .leftJoin(points.memberModel, member)
                .where(store.code.isNotNull()
                        .and(store.code.eq(points.storeCode))
                        .and(BooleanExpression.allOf(exprs.toArray(new BooleanExpression[exprs.size()]))));
//                .groupBy(points.storeCode, store.name)
//                .orderBy(points.storeCode.asc());

        return query;
    }

    public static class MaritalStatusContributionReportBean {

        private String storeCode;
        private String storeName;
        private long singleNo;
        private long marriedNo;
        private long widowerNo;
        private long totalNo;
        private String singleRate;
        private String marriedRate;
        private String widowerRate;

        public String getStoreCode() {
            return storeCode;
        }

        public void setStoreCode(String storeCode) {
            this.storeCode = storeCode;
        }

        public String getStoreName() {
            return storeName;
        }

        public void setStoreName(String storeName) {
            this.storeName = storeName;
        }

        public long getSingleNo() {
            return singleNo;
        }

        public void setSingleNo(long singleNo) {
            this.singleNo = singleNo;
        }

        public long getMarriedNo() {
            return marriedNo;
        }

        public void setMarriedNo(long marriedNo) {
            this.marriedNo = marriedNo;
        }

        public long getWidowerNo() {
            return widowerNo;
        }

        public void setWidowerNo(long widowerNo) {
            this.widowerNo = widowerNo;
        }

        public long getTotalNo() {
            return this.getSingleNo() + this.getMarriedNo() + this.getWidowerNo();
        }

        public void setTotalNo(long totalNo) {
            this.totalNo = totalNo;
        }

        public String getSingleRate() {
            if (this.getTotalNo() != 0) {
                BigDecimal female = new BigDecimal(this.singleNo).divide(new BigDecimal(this.getTotalNo()), 2, RoundingMode.HALF_EVEN).multiply(new BigDecimal(100)).setScale(2, RoundingMode.HALF_EVEN);
                return female.floatValue() + "%";
            } else {
                return BigDecimal.ZERO.setScale(2).floatValue() + "%";
            }
        }

        public void setSingleRate(String singleRate) {
            this.singleRate = singleRate;
        }

        public String getMarriedRate() {
            if (this.getTotalNo() != 0) {
                BigDecimal female = new BigDecimal(this.marriedNo).divide(new BigDecimal(this.getTotalNo()), 2, RoundingMode.HALF_EVEN).multiply(new BigDecimal(100)).setScale(2, RoundingMode.HALF_EVEN);
                return female.floatValue() + "%";
            } else {
                return BigDecimal.ZERO.setScale(2).floatValue() + "%";
            }
        }

        public void setMarriedRate(String marriedRate) {
            this.marriedRate = marriedRate;
        }

        public String getWidowerRate() {
            if (this.getTotalNo() != 0) {
                BigDecimal female = new BigDecimal(this.widowerNo).divide(new BigDecimal(this.getTotalNo()), 2, RoundingMode.HALF_EVEN).multiply(new BigDecimal(100)).setScale(2, RoundingMode.HALF_EVEN);
                return female.floatValue() + "%";
            } else {
                return BigDecimal.ZERO.setScale(2).floatValue() + "%";
            }
        }

        public void setWidowerRate(String widowerRate) {
            this.widowerRate = widowerRate;
        }
    }
}