package com.transretail.crm.report.template.impl;

import com.google.common.collect.Maps;
import com.transretail.crm.common.reporting.jasper.JRProcessor;
import com.transretail.crm.common.reporting.service.ReportService;
import com.transretail.crm.core.entity.MemberModel;
import com.transretail.crm.core.entity.PosTransaction;
import com.transretail.crm.core.entity.embeddable.CustomerProfile;
import com.transretail.crm.core.entity.enums.MemberStatus;
import com.transretail.crm.core.entity.enums.Status;
import com.transretail.crm.core.entity.lookup.LookupDetail;
import com.transretail.crm.core.entity.lookup.LookupHeader;
import com.transretail.crm.core.entity.lookup.Store;
import com.transretail.crm.core.repo.*;
import com.transretail.crm.report.template.CommonReportFilter;
import net.sf.jasperreports.engine.JRParameter;
import org.apache.commons.io.FileUtils;
import org.joda.time.LocalDateTime;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.context.support.MessageSourceResourceBundle;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.io.File;
import java.util.Date;
import java.util.Locale;
import java.util.Map;

/**
 * @author Mike de Guzman
 */
@Transactional
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath*:/META-INF/spring/applicationContext*.xml",
        "/META-INF/spring/applicationContext-test.xml"})
public class BuyingCustomerReportTest {

    @Autowired
    private LookupHeaderRepo lookupHeaderRepo;

    @Autowired
    private LookupDetailRepo lookupDetailRepo;

    @Autowired
    private MemberRepo memberRepo;

    @Autowired
    private StoreRepo storeRepo;

    @Autowired
    private PosTransactionRepo posTransactionRepo;

    @Resource(name = "buyingCustomerReport")
    private BuyingCustomerReport buyingCustomerReport;

    @PersistenceContext
    private EntityManager em;

    @Autowired
    private ReportService reportService;

    @Autowired
    private MessageSource messageSource;

    private static final Logger logger = LoggerFactory.getLogger(BuyingCustomerReportTest.class);

    protected static final String FILTER_STORE = "store";
    protected static final String FILTER_STANDARD_REPORT_TYPE = "standardReportType";
    protected static final String FILTER_START_WEEK = "startWeek";
    protected static final String BUYING_CUSTOMER_REPORT_TYPE_WEEKLY = "weekly";
    protected static final String BUYING_CUSTOMER_REPORT_TYPE_MONTHLY = "monthly";

    @Before
    public void setup() {
        LookupHeader memberType = new LookupHeader("MEM007");
        memberType.setDescription("member type");
        memberType = lookupHeaderRepo.saveAndFlush(memberType);

        lookupDetailRepo.save(new LookupDetail("MTYP001", "Individual", memberType, Status.ACTIVE));
        lookupDetailRepo.save(new LookupDetail("MTYP002", "Employee", memberType, Status.ACTIVE));
        lookupDetailRepo.save(new LookupDetail("MTYP003", "Professional", memberType, Status.ACTIVE));
        lookupDetailRepo.save(new LookupDetail("CARDTYPE", "REGULAR", memberType, Status.ACTIVE));

        Store store = new Store();
        store.setId(022);
        store.setCode("22022");
        store.setName("Lebak Bulus Test Store");
        storeRepo.save(store);

        Store storeReg = new Store();
        storeReg.setId(0223);
        storeReg.setCode("22022r");
        storeReg.setName("Goserindo Test Store");
        storeRepo.save(storeReg);

        MemberModel member = new MemberModel();
        final String ACCOUNT_ID = "23452345345";
        member.setAccountId(ACCOUNT_ID);
        member.setAccountStatus(MemberStatus.ACTIVE);
        member.setContact("contact");
        member.setPin("1234securepin");
        member.setUsername("test-member-emp");
        member.setRegisteredStore("22022r");
        member.setCardType(lookupDetailRepo.findByCode("CARDTYPE"));
        member.setMemberType(lookupDetailRepo.findByCode("MTYP001"));
        CustomerProfile customerProfile = new CustomerProfile();
        customerProfile.setBirthdate(new Date());
        member.setCustomerProfile(customerProfile);
        member = memberRepo.save(member);

        PosTransaction pt = new PosTransaction();
        pt.setId("txn220220001");
        pt.setCustomerId(ACCOUNT_ID);
        pt.setSalesDate(new LocalDateTime(2014, 05, 30, 13, 58));
        pt.setStartDate(new LocalDateTime(2014, 05, 30, 13, 58));
        pt.setTransactionDate(new LocalDateTime(2014, 05, 30, 13, 58));
        pt.setStatus("COMPLETED");
        pt.setStore(store);
        pt.setTaxAmount(127389.1231);
        pt.setTotalAmount(41234123.3243);
        pt.setTotalDiscount(0.00);
        pt.setTransactionDate(new LocalDateTime(2014, 05, 30, 13, 58));
        pt.setType("SALE");

        PosTransaction savePt = posTransactionRepo.save(pt);

        em.flush();
    }

    @After
    public void cleanup() {
        posTransactionRepo.deleteAll();
        memberRepo.deleteAll();
        lookupDetailRepo.deleteAll();
        lookupHeaderRepo.deleteAll();
    }

    @Test
    public void createJrProcessorWeeklyTest() throws Exception {
        Map<String, String> map = Maps.newHashMap();
        map.put(CommonReportFilter.DATE_FROM, "30-05-2014");
        map.put(CommonReportFilter.DATE_TO, "30-05-2014");
        map.put(FILTER_STORE, null);
        map.put(FILTER_STANDARD_REPORT_TYPE, BUYING_CUSTOMER_REPORT_TYPE_WEEKLY);
        map.put(CommonReportFilter.YEAR, "2014");
        map.put(CommonReportFilter.WEEK, "1");
        map.put(FILTER_START_WEEK, "1");

        JRProcessor jrProcessor = buyingCustomerReport.createJrProcessor(map);

        File testClassesDir = FileUtils.toFile(Thread.currentThread().getContextClassLoader().getResource(""));
        File output = new File(testClassesDir, "test-buying-customer-report-output.pdf");
        logger.debug("Output location {}", output.getAbsolutePath());
        output.delete();
        Locale locale = LocaleContextHolder.getLocale();
        jrProcessor.addParameter(JRParameter.REPORT_LOCALE, locale);
        jrProcessor.addParameter(JRParameter.REPORT_RESOURCE_BUNDLE, new MessageSourceResourceBundle(messageSource, locale));
        reportService.exportPdfReport(jrProcessor, output);

        double bytes = output.length();
        double kilobytes = (bytes / 1024);
        logger.debug("Kilobytes: {}", kilobytes);
    }

    @Test
    public void createJrProcessorMonthlyTest() throws Exception {
        Map<String, String> map = Maps.newHashMap();
        map.put(CommonReportFilter.DATE_FROM, "30-05-2014");
        map.put(CommonReportFilter.DATE_TO, "30-05-2014");
        map.put(FILTER_STORE, null);
        map.put(FILTER_STANDARD_REPORT_TYPE, BUYING_CUSTOMER_REPORT_TYPE_MONTHLY);
        map.put(CommonReportFilter.YEAR, "2014");
        map.put(CommonReportFilter.MONTH, "1");

        JRProcessor jrProcessor = buyingCustomerReport.createJrProcessor(map);

        File testClassesDir = FileUtils.toFile(Thread.currentThread().getContextClassLoader().getResource(""));
        File output = new File(testClassesDir, "test-buying-customer-report-output.pdf");
        logger.debug("Output location {}", output.getAbsolutePath());
        output.delete();
        Locale locale = LocaleContextHolder.getLocale();
        jrProcessor.addParameter(JRParameter.REPORT_LOCALE, locale);
        jrProcessor.addParameter(JRParameter.REPORT_RESOURCE_BUNDLE, new MessageSourceResourceBundle(messageSource, locale));
        reportService.exportPdfReport(jrProcessor, output);

        double bytes = output.length();
        double kilobytes = (bytes / 1024);
        logger.debug("Kilobytes: {}", kilobytes);
    }
}
