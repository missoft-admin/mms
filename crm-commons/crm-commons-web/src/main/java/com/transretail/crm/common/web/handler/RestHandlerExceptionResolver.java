package com.transretail.crm.common.web.handler;

import java.io.IOException;
import java.util.Collections;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.context.MessageSource;
import org.springframework.context.MessageSourceAware;
import org.springframework.http.HttpInputMessage;
import org.springframework.http.HttpOutputMessage;
import org.springframework.http.MediaType;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.server.ServletServerHttpRequest;
import org.springframework.http.server.ServletServerHttpResponse;
import org.springframework.util.Assert;
import org.springframework.util.StringUtils;
import org.springframework.web.client.HttpStatusCodeException;
import org.springframework.web.context.request.ServletWebRequest;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.handler.AbstractHandlerExceptionResolver;
import org.springframework.web.servlet.support.RequestContextUtils;

import com.fasterxml.jackson.databind.exc.InvalidFormatException;
import com.transretail.crm.common.enums.MessageType;
import com.transretail.crm.common.service.dto.rest.ReturnMessage;

/**
 *
 */
public class RestHandlerExceptionResolver extends AbstractHandlerExceptionResolver implements MessageSourceAware, InitializingBean {
    private static final Logger _LOG = LoggerFactory.getLogger(RestHandlerExceptionResolver.class);
    private String restUrlPrefix = "/api";
    private List<HttpMessageConverter<?>> messageConverters;
    private MessageSource messageSource;

    public void setRestUrlPrefix(String restUrlPrefix) {
        this.restUrlPrefix = restUrlPrefix;
    }

    public void setMessageConverters(List<HttpMessageConverter<?>> messageConverters) {
        this.messageConverters = messageConverters;
    }

    @Override
    public void setMessageSource(MessageSource messageSource) {
        this.messageSource = messageSource;
    }

    @Override
    public void afterPropertiesSet() throws Exception {
        Assert.notEmpty(messageConverters);
    }

    @Override
    protected ModelAndView doResolveException(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex) {
        ModelAndView mav = null;
        if (isRestRequest(request)) {
            _LOG.error("Handling rest request exception.", ex);
            String message = null;
            if (HttpStatusCodeException.class.isAssignableFrom(ex.getClass())) {
                message = ((HttpStatusCodeException) ex).getResponseBodyAsString();
            } else {
                Throwable root = ex;
                while (root.getCause() != null) {
                    root = root.getCause();
                }
                if (root instanceof InvalidFormatException) {
                    InvalidFormatException invalidFormatException = (InvalidFormatException) root;
                    Class type = invalidFormatException.getTargetType();
                    Object value = invalidFormatException.getValue();
                    message = messageSource.getMessage(InvalidFormatException.class.getName(), new Object[]{value, type},
                        invalidFormatException.getMessage(), RequestContextUtils.getLocale(request));
                } else {
                    message = root.getMessage();
                }
            }
            try {
                mav = handleResponseBody(new ReturnMessage().withType(MessageType.ERROR).withMessage(message),
                    new ServletWebRequest(request, response));
            } catch (Exception invocationEx) {
                _LOG.error("Acquiring ModelAndView for Exception [" + ex + "] resulted in an exception.", invocationEx);
            }

        }
        return mav;
    }

    private ModelAndView handleResponseBody(Object body, ServletWebRequest webRequest) throws ServletException, IOException {
        HttpInputMessage inputMessage = new ServletServerHttpRequest(webRequest.getRequest());
        webRequest.getResponse().setStatus(HttpServletResponse.SC_BAD_REQUEST);
        List<MediaType> acceptedMediaTypes = inputMessage.getHeaders().getAccept();
        if (acceptedMediaTypes.isEmpty()) {
            acceptedMediaTypes = Collections.singletonList(MediaType.ALL);
        }

        MediaType.sortByQualityValue(acceptedMediaTypes);

        HttpOutputMessage outputMessage = new ServletServerHttpResponse(webRequest.getResponse());

        Class<?> bodyType = body.getClass();

        for (MediaType acceptedMediaType : acceptedMediaTypes) {
            for (HttpMessageConverter messageConverter : messageConverters) {
                if (messageConverter.canWrite(bodyType, acceptedMediaType)) {
                    messageConverter.write(body, acceptedMediaType, outputMessage);
                    //return empty model and view to short circuit the iteration and to let
                    //Spring know that we've rendered the view ourselves:
                    return new ModelAndView();
                }
            }
        }

        if (_LOG.isWarnEnabled()) {
            _LOG.warn("Could not find HttpMessageConverter that supports return type ["
                + bodyType + "] and " + acceptedMediaTypes);
        }
        return null;
    }

    private boolean isRestRequest(HttpServletRequest request) {
        String contextPath = request.getContextPath();
        String requestURI = request.getRequestURI();
        if (StringUtils.hasText(contextPath) && !"/".equals(contextPath.trim())) {
            requestURI = requestURI.substring(contextPath.length());
        }
        return requestURI.startsWith(restUrlPrefix);
    }
}
