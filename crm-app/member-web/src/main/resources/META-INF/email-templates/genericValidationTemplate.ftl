<html>
<head>
    <title>${title}</title>
</head>
<body>
<p>Please click the <a href="${link}?code=${queryString?url}">validation link</a>! or manually enter ${link}?code=${queryString?url} in browser address bar.</p>
</body>
</html>