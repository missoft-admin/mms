package com.transretail.crm.core.dto;


import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.BooleanUtils;
import org.apache.commons.lang3.StringUtils;
import org.joda.time.LocalDate;
import org.joda.time.format.DateTimeFormat;
import org.springframework.web.multipart.MultipartFile;

import com.transretail.crm.core.entity.MarketingChannel;
import com.transretail.crm.core.entity.MarketingChannel.ChannelStatus;
import com.transretail.crm.core.entity.MarketingChannel.ChannelType;


public class MarketingChannelDto {

    private String description;
    private String message;
	private String memberGroup;
	private String memberGroupName;
	private String promotion;
	private String promotionName;

	private String id;
    private String code;

	private Date schedule;
    private MultipartFile file;
    private String filename;
    private String fileId;
    private ChannelType type;
    private ChannelStatus status;

	private String dispTime;
    private Long dispSchedule;

    private Integer targetCount;
    private Boolean deleteFile;



//	private static final String[] IGNORE = { "id", "code", "schedule", "type", "status", "filename" };
	public MarketingChannelDto() {}
	public MarketingChannelDto( MarketingChannel model ) {
//		BeanUtils.copyProperties( model, this, IGNORE );
        this.description = model.getDescription();
        this.message = model.getMessage();
        this.memberGroup = model.getMemberGroup();
        this.promotion = model.getPromotion();

		id = ( null != model.getId() )? model.getId().toString() : null;
		code = ( StringUtils.isNotBlank( model.getCode() ) )? model.getCode() : null;

		if ( null != model.getTime() ) {
			dispTime = model.getTime().toString( MarketingChannel.TIME_FORMAT_1 );
		}
		dispSchedule = ( null != model.getSchedule() )? model.getSchedule().toDate().getTime() : null;
		schedule = ( null != model.getSchedule() )? model.getSchedule().toDate() : null;
		filename = ( StringUtils.isNotBlank( model.getFilename() ) )? model.getFilename() : null;
		fileId = ( StringUtils.isNotBlank( model.getFileId() ) )? model.getFileId() : null;
		type = ( null != model.getType() )? model.getType() : null;
		status = ( null != model.getStatus() )? model.getStatus() : null;
	}

	public MarketingChannel toModel() { return toModel( null ); }
	public MarketingChannel toModel( MarketingChannel model ) {
		if ( null == model ) {
			model = new MarketingChannel();
		}
//		BeanUtils.copyProperties( this, model , IGNORE );
        model.setDescription( this.description );
        model.setMessage( this.message );
        model.setMemberGroup( this.memberGroup );
        model.setPromotion( this.promotion );

		if ( StringUtils.isNotBlank( code ) ) { model.setCode( code ); }

		if ( null != dispSchedule && null == schedule ) {
			model.setSchedule( new LocalDate( dispSchedule ) );
		}
		else if ( ( null != schedule && null == dispSchedule ) || ( null != schedule && null != dispSchedule ) ) {
			model.setSchedule( new LocalDate( schedule.getTime() ) );
		}
		if ( StringUtils.isNotBlank( dispTime ) ) { 
			if ( StringUtils.containsIgnoreCase( dispTime, "AM" ) || StringUtils.containsIgnoreCase( dispTime, "PM" ) ) {
				model.setTime( DateTimeFormat.forPattern(  MarketingChannel.TIME_FORMAT_1 ).parseLocalTime( dispTime ) ); 
			}
			else {
				model.setTime( DateTimeFormat.forPattern(  MarketingChannel.TIME_FORMAT ).parseLocalTime( dispTime ) ); 
			}
		}
		if ( StringUtils.isNotBlank( filename ) ) { model.setFilename( filename ); }
		if ( StringUtils.isNotBlank( fileId ) ) { model.setFileId( fileId ); }
		if ( BooleanUtils.isTrue( deleteFile ) ) {
			 model.setFilename( filename );
			 model.setFileId( fileId );
		}
		if ( null != type ) { model.setType( type ); }
		if ( null != status ) { model.setStatus( status ); }

		return model;
	}

	public static List<MarketingChannelDto> toDtoList( List<MarketingChannel> models ) {
		if ( CollectionUtils.isEmpty( models ) ) {
			return new ArrayList<MarketingChannelDto>();
		}

		List<MarketingChannelDto> dtos = new ArrayList<MarketingChannelDto>();
		for ( MarketingChannel model : models ) {
			dtos.add( new MarketingChannelDto( model ) );
		}
		return dtos;
	}



	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getMemberGroup() {
		return memberGroup;
	}
	public void setMemberGroup(String memberGroup) {
		this.memberGroup = memberGroup;
	}
	public String getMemberGroupName() {
		return memberGroupName;
	}
	public void setMemberGroupName(String memberGroupName) {
		this.memberGroupName = memberGroupName;
	}
	public String getPromotion() {
		return promotion;
	}
	public void setPromotion(String promotion) {
		this.promotion = promotion;
	}
	public String getPromotionName() {
		return promotionName;
	}
	public void setPromotionName(String promotionName) {
		this.promotionName = promotionName;
	}
	public Date getSchedule() {
		return schedule;
	}
	public void setSchedule(Date schedule) {
		this.schedule = schedule;
	}
	public MultipartFile getFile() {
		return file;
	}
	public void setFile(MultipartFile file) {
		this.file = file;
	}
	public String getFilename() {
		return filename;
	}
	public void setFilename(String filename) {
		this.filename = filename;
	}
	public String getFileId() {
		return fileId;
	}
	public void setFileId(String fileId) {
		this.fileId = fileId;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public ChannelType getType() {
		return type;
	}
	public void setType(ChannelType type) {
		this.type = type;
	}
	public ChannelStatus getStatus() {
		return status;
	}
	public void setStatus(ChannelStatus status) {
		this.status = status;
	}
	public String getDispTime() {
		return dispTime;
	}
	public void setDispTime(String dispTime) {
		this.dispTime = dispTime;
	}
	public Long getDispSchedule() {
		return dispSchedule;
	}
	public void setDispSchedule(Long dispSchedule) {
		this.dispSchedule = dispSchedule;
	}
	public Integer getTargetCount() {
		return targetCount;
	}
	public void setTargetCount(Integer targetCount) {
		this.targetCount = targetCount;
	}
	public Boolean getDeleteFile() {
		return deleteFile;
	}
	public void setDeleteFile(Boolean deleteFile) {
		this.deleteFile = deleteFile;
	}

}
