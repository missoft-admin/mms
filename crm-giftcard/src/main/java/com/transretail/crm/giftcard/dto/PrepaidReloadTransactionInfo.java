package com.transretail.crm.giftcard.dto;

/**
 * @author Allan G. Ramirez (agramirez@exist.com)
 */
public class PrepaidReloadTransactionInfo extends GiftCardTransactionInfo<PrepaidReloadTransactionInfo> {
    private String cardNo;
    private Long reloadAmount;

    public String getCardNo() {
        return cardNo;
    }

    public PrepaidReloadTransactionInfo cardNo(String cardNo) {
        this.cardNo = cardNo;
        return this;

    }

    public Long getReloadAmount() {
        return reloadAmount;
    }

    public PrepaidReloadTransactionInfo reloadAmount(Long reloadAmount) {
        this.reloadAmount = reloadAmount;
        return this;
    }
}
