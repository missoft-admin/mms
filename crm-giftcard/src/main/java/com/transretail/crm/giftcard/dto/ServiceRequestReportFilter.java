package com.transretail.crm.giftcard.dto;

import javax.validation.constraints.NotNull;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import org.joda.time.LocalDate;
import org.joda.time.LocalTime;

/**
 *
 * @author Monte Cillo Co (mco@exist.com)
 */
public class ServiceRequestReportFilter {

    @NotNull(message = "{NotNull.serviceRequestReportFilter.dateFiled}")
    private LocalDate dateFiled;
    @NotNull(message = "{NotNull.serviceRequestReportFilter.timeFrom}")
    private LocalTime timeFrom;
    @NotNull(message = "{NotNull.serviceRequestReportFilter.timeTo}")
    private LocalTime timeTo;
    private String filedBy;

    public LocalDate getDateFiled() {
	return dateFiled;
    }

    public LocalTime getTimeFrom() {
	return timeFrom;
    }

    public LocalTime getTimeTo() {
	return timeTo;
    }

    public String getFiledBy() {
	return filedBy;
    }

    public void setDateFiled(LocalDate dateFiled) {
	this.dateFiled = dateFiled;
    }

    public void setTimeFrom(LocalTime timeFrom) {
	this.timeFrom = timeFrom;
    }

    public void setTimeTo(LocalTime timeTo) {
	this.timeTo = timeTo;
    }

    public void setFiledBy(String filedBy) {
	this.filedBy = filedBy;
    }

    @Override
    public String toString() {
	return ToStringBuilder.reflectionToString(this, ToStringStyle.DEFAULT_STYLE);
    }
}
