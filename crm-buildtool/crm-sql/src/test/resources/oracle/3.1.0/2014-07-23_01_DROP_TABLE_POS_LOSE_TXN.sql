-- DROP CRM_POS_LOSE_TXN, it was replaced by CRM_GC_TRANSACTION
-- with additional column of LOSE_TXN as marking the transaction as lose.
drop table CRM_POS_LOSE_TXN cascade constraints;