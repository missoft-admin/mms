package com.transretail.crm.common.web.converter;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;
import java.io.IOException;
import java.text.DecimalFormat;

/**
 *
 * @author Monte Cillo Co (mco@exist.com)
 */
public class FormattedIntegerNumberSerializer extends JsonSerializer<Number> {

    @Override
    public void serialize(Number number, JsonGenerator jsonGenerator, SerializerProvider serializerProvider) throws IOException {
	DecimalFormat fmt = new DecimalFormat("#,##0");
	jsonGenerator.writeString(fmt.format(number));
    }
}
