package com.transretail.crm.giftcard.dto;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.joda.time.LocalDate;

import com.mysema.query.types.expr.BooleanExpression;
import com.transretail.crm.common.service.dto.request.AbstractSearchFormDto;
import com.transretail.crm.giftcard.entity.QGiftCardOrder;
import com.transretail.crm.giftcard.entity.support.GiftCardOrderStatus;

/**
 *
 */
public class GiftCardOrderSearchDto extends AbstractSearchFormDto {
	private String moNo;
	private String poNo;
	private String mpoNo;
	private Long cardVendorId;
	private String statusCode;
	private LocalDate orderDate;

    @Override
    public BooleanExpression createSearchExpression() {
        List<BooleanExpression> expressions = new ArrayList<BooleanExpression>();
        QGiftCardOrder qGiftCardOrder = QGiftCardOrder.giftCardOrder;

        if(StringUtils.isNotEmpty(moNo)) {
        	expressions.add(qGiftCardOrder.moNumber.containsIgnoreCase(moNo));
        }
        if(StringUtils.isNotEmpty(poNo)) {
        	expressions.add(qGiftCardOrder.poNumber.containsIgnoreCase(poNo));
        }
        if(StringUtils.isNotEmpty(mpoNo)) {
        	expressions.add(qGiftCardOrder.moNumber.containsIgnoreCase(mpoNo).or(qGiftCardOrder.poNumber.containsIgnoreCase(mpoNo)));
        }
        if(cardVendorId != null) {
        	expressions.add(qGiftCardOrder.vendor.id.eq(cardVendorId));
        }
        if(StringUtils.isNotBlank(statusCode)) {
        	expressions.add(qGiftCardOrder.status.eq(GiftCardOrderStatus.valueOf(statusCode)));
        }
        if(orderDate != null) {
        	expressions.add(qGiftCardOrder.poDate.eq(orderDate));
        }
        return BooleanExpression.allOf(expressions.toArray(new BooleanExpression[expressions.size()]));
    }

	public String getMoNo() {
		return moNo;
	}

	public void setMoNo(String moNo) {
		this.moNo = moNo;
	}

	public String getPoNo() {
		return poNo;
	}

	public void setPoNo(String poNo) {
		this.poNo = poNo;
	}

	public String getMpoNo() {
		return mpoNo;
	}

	public void setMpoNo(String mpoNo) {
		this.mpoNo = mpoNo;
	}

	public Long getCardVendorId() {
		return cardVendorId;
	}

	public void setCardVendorId(Long cardVendorId) {
		this.cardVendorId = cardVendorId;
	}

	public String getStatusCode() {
		return statusCode;
	}

	public void setStatusCode(String statusCode) {
		this.statusCode = statusCode;
	}

	public LocalDate getOrderDate() {
		return orderDate;
	}

	public void setOrderDate(LocalDate orderDate) {
		this.orderDate = orderDate;
	}

}
