package com.transretail.crm.template.service.streaming.excel.handler.cell;

import com.transretail.crm.core.entity.MemberModel;
import com.transretail.crm.core.entity.enums.MemberStatus;
import com.transretail.crm.template.service.MemberTemplateParseException;
import org.apache.commons.lang3.StringUtils;

/**
 * @author Allan G. Ramirez (agramirez@exist.com)
 */
public class TerminationDateCellHandler implements CellHandler {
    @Override
    public void handle(MemberModel dto, String cellText) throws MemberTemplateParseException {
        dto.setAccountStatus(StringUtils.isNotBlank(cellText) ? MemberStatus.TERMINATED : MemberStatus.ACTIVE);
    }
}
