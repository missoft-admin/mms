package com.transretail.crm.web.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Configurable;
import org.springframework.core.convert.converter.Converter;
import org.springframework.format.FormatterRegistry;
import org.springframework.format.support.FormattingConversionServiceFactoryBean;

import com.transretail.crm.core.entity.MemberModel;
import com.transretail.crm.core.entity.UserModel;
import com.transretail.crm.core.entity.UserRoleModel;
import com.transretail.crm.core.service.MemberService;
import com.transretail.crm.core.service.UserRoleModelService;
import com.transretail.crm.core.service.UserService;

/**
 * A central place to register application converters and formatters.
 *
 */
@Configurable
public class ApplicationConversionServiceFactoryBean extends
        FormattingConversionServiceFactoryBean {

    @Autowired
    MemberService memberService;
    @Autowired
    UserService userService;

    @Autowired
    UserRoleModelService userRoleModelService;

    @Override
    protected void installFormatters(FormatterRegistry registry) {
        super.installFormatters(registry);
        // Register application converters and formatters
    }

    public Converter<MemberModel, String> getMemberModelToStringConverter() {
        return new Converter<MemberModel, String>() {
            public String convert(MemberModel customerModel) {
                return new StringBuilder().append(customerModel.getFirstName())
                        .append(' ').append(customerModel.getMiddleName())
                        .append(' ').append(customerModel.getLastName())
                        .append(' ').append(customerModel.getCustomerProfile().getGender().getDescription())
                        .toString();
            }
        };
    }

    public Converter<Long, MemberModel> getIdToMemberModelConverter() {
        return new Converter<Long, MemberModel>() {
            public MemberModel convert(Long id) {
                return memberService.findCustomerModel(id);
            }
        };
    }

    public Converter<String, MemberModel> getStringToMemberModelConverter() {
        return new Converter<String, MemberModel>() {
            public MemberModel convert(String id) {
                return getObject().convert(getObject().convert(id, Long.class),
                        MemberModel.class);
            }
        };
    }

    public Converter<UserModel, String> getUserModelToStringConverter() {
        return new Converter<UserModel, String>() {
            public String convert(UserModel userModel) {
                return new StringBuilder().append(userModel.getUsername())
                        .append(' ').append(userModel.getPassword()).toString();
            }
        };
    }

    public Converter<Long, UserModel> getIdToUserModelConverter() {
        return new Converter<Long, UserModel>() {
            public UserModel convert(Long id) {
                return userService.findUserModel(id);
            }
        };
    }

    public Converter<String, UserModel> getStringToUserModelConverter() {
        return new Converter<String, UserModel>() {
            public UserModel convert(String id) {
                return getObject().convert(getObject().convert(id, Long.class),
                        UserModel.class);
            }
        };
    }

    public Converter<UserRoleModel, String> getUserRoleModelToStringConverter() {
        return new Converter<UserRoleModel, String>() {
            public String convert(UserRoleModel userRoleModel) {
                return new StringBuilder().append(userRoleModel.getCode())
                        .append(' ').append(userRoleModel.getDescription())
                        .toString();
            }
        };
    }

    public Converter<String, UserRoleModel> getIdToUserRoleModelConverter() {
        return new Converter<String, UserRoleModel>() {
            public UserRoleModel convert(String id) {
                return userRoleModelService.findUserRoleModel(id);
            }
        };
    }

    public Converter<String, UserRoleModel> getStringToUserRoleModelConverter() {
        return new Converter<String, UserRoleModel>() {
            public UserRoleModel convert(String id) {
                return getObject().convert(getObject().convert(id, Long.class),
                        UserRoleModel.class);
            }
        };
    }

    public void installLabelConverters(FormatterRegistry registry) {
        registry.addConverter(getMemberModelToStringConverter());
        registry.addConverter(getIdToMemberModelConverter());
        registry.addConverter(getStringToMemberModelConverter());
        registry.addConverter(getUserModelToStringConverter());
        registry.addConverter(getIdToUserModelConverter());
        registry.addConverter(getStringToUserModelConverter());
        registry.addConverter(getUserRoleModelToStringConverter());
        registry.addConverter(getIdToUserRoleModelConverter());
        registry.addConverter(getStringToUserRoleModelConverter());
    }

    public void afterPropertiesSet() {
        super.afterPropertiesSet();
        installLabelConverters(getObject());
    }
}
