package com.transretail.crm.giftcard.dto;


import java.util.Date;

import org.joda.time.LocalDateTime;

import com.mysema.query.annotations.QueryProjection;
import com.transretail.crm.giftcard.entity.GiftCardInventory;


public class GiftCardInventoryStockDto {

	private String productCode;
	private String productName;
	private String orderNumber;

	private String transaction;
	private String activationDate;
	private Long quantity;
	private Long beginningBalance;
	private Long endingBalance;
	private Date createdDate;
	private Long createdDateMilli;
	private Date compiledTimestamp;
	private Long compiledTimestampMilli;

    private GiftCardInventory inventory;
    private String status;
    private String transferFrom;
    private String transferTo;
    private String location;

	public GiftCardInventoryStockDto(){}

    @QueryProjection
    public GiftCardInventoryStockDto( 
    		LocalDateTime compiledTimestamp,
    		String productCode,
    		String productName,
    		String orderNumber,
    		String transaction,
    		Long quantity ) {

    	super();
    	/*this.createdDate = null != createdDate? createdDate.toDate() : null;
    	this.createdDateMilli = null != createdDate? createdDate.toDate().getTime() : null;*/
    	this.compiledTimestamp = null != compiledTimestamp? compiledTimestamp.toDate() : null;
    	this.compiledTimestampMilli = null != compiledTimestamp? compiledTimestamp.toDate().getTime() : null;
    	this.productCode = productCode;
    	this.productName = productName;
    	this.orderNumber = orderNumber;
    	this.transaction = transaction;
    	this.quantity = quantity;
    }

    @QueryProjection
    public GiftCardInventoryStockDto( 
    		LocalDateTime compiledTimestamp,
    		String productCode,
    		String productName,
    		String transaction,
    		Long quantity ) {

    	super();
    	/*this.createdDate = null != createdDate? createdDate.toDate() : null;
    	this.createdDateMilli = null != createdDate? createdDate.toDate().getTime() : null;*/
    	this.compiledTimestamp = null != compiledTimestamp? compiledTimestamp.toDate() : null;
    	this.compiledTimestampMilli = null != compiledTimestamp? compiledTimestamp.toDate().getTime() : null;
    	this.productCode = productCode;
    	this.productName = productName;
    	this.transaction = transaction;
    	this.quantity = quantity;
    }

    public String getProductCode() {
		return productCode;
	}

	public String getProductName() {
		return productName;
	}

	public String getOrderNumber() {
		return orderNumber;
	}

	public String getTransaction() {
		return transaction;
	}

	public String getActivationDate() {
		return activationDate;
	}

	public Long getQuantity() {
		return quantity;
	}

	public Long getBeginningBalance() {
		return beginningBalance;
	}

	public void setBeginningBalance(Long beginningBalance) {
		this.beginningBalance = beginningBalance;
	}

	public Long getEndingBalance() {
		return endingBalance;
	}

	public void setEndingBalance(Long endingBalance) {
		this.endingBalance = endingBalance;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public Long getCreatedDateMilli() {
		return createdDateMilli;
	}

    public Date getCompiledTimestamp() {
		return compiledTimestamp;
	}

	public Long getCompiledTimestampMilli() {
		return compiledTimestampMilli;
	}

	public GiftCardInventory getInventory() {
        return inventory;
    }

    public void setInventory(GiftCardInventory inventory) {
        this.inventory = inventory;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getTransferFrom() {
        return transferFrom;
    }

    public void setTransferFrom(String transferFrom) {
        this.transferFrom = transferFrom;
    }

    public String getTransferTo() {
        return transferTo;
    }

    public void setTransferTo(String transferTo) {
        this.transferTo = transferTo;
    }

    public void setQuantity(Long quantity) {
        this.quantity = quantity;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

}
