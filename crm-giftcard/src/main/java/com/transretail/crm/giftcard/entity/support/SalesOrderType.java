package com.transretail.crm.giftcard.entity.support;


public enum SalesOrderType {

    B2B_SALES, B2B_ADV_SALES, YEARLY_DISCOUNT, INTERNAL_ORDER, REPLACEMENT, VOUCHER;
    
    public boolean isRyns() {
    	switch(this) {
	    	case B2B_SALES: return false; 
	    	case B2B_ADV_SALES: return false;
	    	case REPLACEMENT: return false;
	    	case INTERNAL_ORDER: return true;
	    	case YEARLY_DISCOUNT: return true;
	    	case VOUCHER: return true;
		}
		return false;
    }
}
