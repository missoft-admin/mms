package com.transretail.crm.web.controller;

import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.sql.SQLException;
import java.util.Locale;

import javax.imageio.ImageIO;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.springframework.web.servlet.ModelAndView;

import com.transretail.crm.common.service.dto.response.ResultList;
import com.transretail.crm.common.web.notification.NotificationType;
import com.transretail.crm.common.web.notification.annotation.Notifiable;
import com.transretail.crm.common.web.notification.annotation.NotificationUpdate;
import com.transretail.crm.core.dto.PromoBannerDto;
import com.transretail.crm.core.dto.PromoBannerSearchDto;
import com.transretail.crm.core.entity.enums.PromoImageType;
import com.transretail.crm.core.entity.lookup.LookupDetail;
import com.transretail.crm.core.security.util.UserUtil;
import com.transretail.crm.core.service.CodePropertiesService;
import com.transretail.crm.core.service.LookupService;
import com.transretail.crm.core.service.PromoBannerService;
import com.transretail.crm.web.controller.util.ControllerResponse;

@Controller
@RequestMapping("/promobanner")
public class PromoBannerController {
	@Autowired
	private PromoBannerService promoBannerService;
	@Autowired
	private MessageSource messageSource;
	@Autowired
	private LookupService lookupService;
	@Autowired
	private CodePropertiesService codePropertiesService;
	
	private static final Logger LOGGER = LoggerFactory.getLogger(PromoBannerController.class);
	
	private static final String MAIN_PAGE = "/promobanner/list";
	private static final String FORM_PAGE = "/promobanner/form";
	private static final String ATTR_MODEL = "promoBanner";
	private static final String MODEL_ID_PARAM = "itemId";
	
	@RequestMapping(produces="text/html")
	public String list(Model uiModel) {
		return MAIN_PAGE;
	}
	
	@RequestMapping(value="/search")
	@ResponseBody
	public ResultList<PromoBannerDto> search(PromoBannerSearchDto searchDto) {
		return promoBannerService.searchPromoBanner(searchDto);
	}
	
	@RequestMapping(value="/save", method=RequestMethod.POST)
	public ModelAndView save(
			@ModelAttribute(ATTR_MODEL) PromoBannerDto promoBanner, 
			@RequestParam("file") MultipartFile file,
			BindingResult result, Model uiModel,
            MultipartHttpServletRequest httpServletRequest) {
		ControllerResponse response = new ControllerResponse();
		
		validate(promoBanner, file, result);
		
		if(result.hasErrors()) {
			populateForm(uiModel, promoBanner, false);
			return new ModelAndView(FORM_PAGE);
		}
		
		try {
			if(!file.isEmpty()) {
				promoBanner.setImage(file.getBytes());
			}
			else {
				promoBanner.setImage(null);
			}
		}
		catch(IOException e) {
			e.printStackTrace();
			response.setSuccess(false);
		}
		
		if(promoBanner.getStatus() == null) {
			promoBanner.setStatus(lookupService.getDetailByCode(promoBanner.getStatusCode()));
		}
		
		ModelAndView mv = new ModelAndView("redirect:/promobanner").addObject(MODEL_ID_PARAM, 
				promoBannerService.save(promoBanner).getId());
		
		return mv;
	}
	
	@NotificationUpdate(type=NotificationType.PROMO_BANNER)
	@RequestMapping(value="/approve", method=RequestMethod.POST)
	public ModelAndView approve(@ModelAttribute(ATTR_MODEL) PromoBannerDto promoBanner, 
			@RequestParam("file") MultipartFile file, 
			BindingResult result, Model uiModel,
            MultipartHttpServletRequest httpServletRequest) {
		LookupDetail prevStatus = promoBanner.getStatus();
		promoBanner.setStatus(lookupService.getDetailByCode(codePropertiesService.getDetailStatusActive()));
		ModelAndView mv = save(promoBanner, file, result, uiModel, httpServletRequest);
		
		if(result.hasErrors()) {
			promoBanner.setStatus(prevStatus);
			populateForm(uiModel, promoBanner, true);
			return mv;
		}
		return mv;
	}
	
	@Notifiable(type=NotificationType.PROMO_BANNER)
	@NotificationUpdate(type=NotificationType.PROMO_BANNER)
	@RequestMapping(value="/update/status/{statusCode}", method=RequestMethod.POST)
	public ModelAndView updateStatus(@ModelAttribute(ATTR_MODEL) PromoBannerDto promoBanner, 
			@RequestParam("file") MultipartFile file, @PathVariable("statusCode") String statusCode,
			BindingResult result, Model uiModel,
            MultipartHttpServletRequest httpServletRequest) {
		LookupDetail prevStatus = promoBanner.getStatus();
		promoBanner.setStatus(lookupService.getDetailByCode(statusCode));
		ModelAndView mv = save(promoBanner, file, result, uiModel, httpServletRequest);
		
		if(result.hasErrors()) {
			promoBanner.setStatus(prevStatus);
			populateForm(uiModel, promoBanner, prevStatus != null && 
					prevStatus.getCode().equals(codePropertiesService.getDetailStatusApproval()));
			return mv;
		}
		
		if(statusCode.equals(codePropertiesService.getDetailStatusApproval())) {
			return mv.addObject("new", "");
		}
		
		return mv.addObject("update", "");
	}
	
	@Notifiable(type=NotificationType.PROMO_BANNER)
	@RequestMapping(value="/new", method=RequestMethod.POST)
	public ModelAndView create(@ModelAttribute(ATTR_MODEL) PromoBannerDto promoBanner, 
			@RequestParam("file") MultipartFile file, 
			BindingResult result, Model uiModel,
            MultipartHttpServletRequest httpServletRequest) {
//		return save(promoBanner, file, result, uiModel, httpServletRequest).addObject("new", "");
		return updateStatus(promoBanner, file, codePropertiesService.getDetailStatusApproval(), 
				result, uiModel, httpServletRequest);
	}
	
	@RequestMapping(value="/new", method=RequestMethod.GET, produces="text/html")
	public String create(Model uiModel) {
		PromoBannerDto dto = new PromoBannerDto();
//		dto.setStatus(lookupService.getDetailByCode(codePropertiesService.getDetailStatusApproval()));
		populateForm(uiModel, dto, false);
		uiModel.addAttribute("update", false);
		return FORM_PAGE;
	}
	
	@RequestMapping(value="/update/{id}", method=RequestMethod.GET, produces="text/html") 
	public String update(@PathVariable("id") String id, Model uiModel) {
		PromoBannerDto dto = promoBannerService.findById(id);
		populateForm(uiModel, dto, dto.getStatus().getCode().equals(codePropertiesService.getDetailStatusApproval())
				&& UserUtil.getCurrentUser().hasAuthority(codePropertiesService.getMktHeadCode()));
		uiModel.addAttribute("id", id);
		uiModel.addAttribute("update", true);
		return FORM_PAGE;
	}
	
	@NotificationUpdate(type=NotificationType.PROMO_BANNER)
	@RequestMapping(value="/{id}", method=RequestMethod.DELETE)
	public ModelAndView delete(@PathVariable("id") String id) {
		promoBannerService.delete(id);
		return new ModelAndView("redirect:/promobanner").addObject(MODEL_ID_PARAM, id);
	}
	
	@RequestMapping(value="/image/{id}")
	public void getPromoBannerImage(@PathVariable("id") String id, HttpServletResponse response) {
		PromoBannerDto promoBanner = promoBannerService.findById(id);
		
		if(promoBanner != null) {
			try {
				writeFileContentToHttpResponse(promoBanner.getImage(), response);
			}
			catch(Exception e) {
				e.printStackTrace();
			}
		}
	}
	
	private void validate(PromoBannerDto promoBanner, MultipartFile file, BindingResult result) {
		Locale locale = LocaleContextHolder.getLocale();
		
		if(promoBanner.getStartDate() == null) {
			result.rejectValue("startDate", "propkey_msg_notempty", new Object[] {
					messageSource.getMessage("label_promo_banner_startdate", null, 
							locale)}, "Invalid info");
		}
		
		if(promoBanner.getEndDate() == null) {
			result.rejectValue("endDate", "propkey_msg_notempty", new Object[] {
					messageSource.getMessage("label_promo_banner_enddate", null, 
							locale)}, "Invalid info");
		}
		
		if(StringUtils.isEmpty(promoBanner.getId())) {
			int height = promoBanner.getType().getHeight();
			int width = promoBanner.getType().getWidth();
			
			if(file.isEmpty()) {
				result.rejectValue("image", "propkey_msg_notempty", new Object[] {
						messageSource.getMessage("label_promo_banner_image", null, 
								locale)}, "Invalid info");
			}
			else if(!(file.getContentType().equalsIgnoreCase("image/png") || file.getContentType().equalsIgnoreCase("image/jpeg"))) {
				result.rejectValue("image", "propkey_msg_invalid_file", new Object[] {
						messageSource.getMessage("label_promo_banner_image", null, 
								locale)}, "Invalid info");
			}
			else {
				try {
					BufferedImage img = ImageIO.read(file.getInputStream());
					
					if(Math.abs(img.getHeight() - height) > 5 ||
							Math.abs(img.getWidth() - width) > 9) {
						result.rejectValue("image", "label_promo_banner_size_error", new Object[] {
								promoBanner.getType().toString(), width, height}, "Invalid info");
					}
				}
				catch(IOException e) {
					LOGGER.error(e.getMessage(), e);
				}
			}
		}
		
		switch(promoBanner.getType()) {
			case NEWS:
				if(StringUtils.isBlank(promoBanner.getInfo())) {
					result.rejectValue("info", "propkey_msg_notempty", new Object[] {
							messageSource.getMessage("label_promo_banner_summary", null, 
									locale)}, "Invalid info");
				}
				if(StringUtils.isBlank(promoBanner.getName())) {
					result.rejectValue("name", "propkey_msg_notempty", new Object[] {
							messageSource.getMessage("label_promo_banner_headline", null, 
									locale)}, "Invalid info");
				}
			case ADVERTISMENT:
			case BANNER:
				if(StringUtils.isBlank(promoBanner.getLink())) {
					result.rejectValue("link", "propkey_msg_notempty", new Object[] {
							messageSource.getMessage("label_promo_banner_link", null, 
									locale)}, "Invalid info");
				}
				break;
			case PRODUCT:
				if(StringUtils.isBlank(promoBanner.getName())) {
					result.rejectValue("name", "propkey_msg_notempty", new Object[] {
							messageSource.getMessage("label_promo_banner_short_desc", null, 
									locale)}, "Invalid info");
				}
				if(StringUtils.isBlank(promoBanner.getInfo())) {
					result.rejectValue("info", "propkey_msg_notempty", new Object[] {
							messageSource.getMessage("label_promo_banner_description", null, 
									locale)}, "Invalid info");
				}
				if(StringUtils.isBlank(promoBanner.getProductQuantity())) {
					result.rejectValue("productQuantity", "propkey_msg_notempty", new Object[] {
							messageSource.getMessage("label_promo_banner_quantity", null, 
									locale)}, "Invalid info");
				}
				if(null == (promoBanner.getOldPrice())) {
					result.rejectValue("oldPrice", "propkey_msg_notempty", new Object[] {
							messageSource.getMessage("label_promo_banner_old_price", null, 
									locale)}, "Invalid info");
				}
				if(null == (promoBanner.getNewPrice())) {
					result.rejectValue("newPrice", "propkey_msg_notempty", new Object[] {
							messageSource.getMessage("label_promo_banner_new_price", null, 
									locale)}, "Invalid info");
				}
		}
	}
	
	private void populateForm(Model uiModel, PromoBannerDto promoBanner, boolean forApproval) {
		uiModel.addAttribute(ATTR_MODEL, promoBanner);
		uiModel.addAttribute("forApproval", forApproval);
		uiModel.addAttribute("types", PromoImageType.values());
	}
	
	private void writeFileContentToHttpResponse(byte[] image, HttpServletResponse response) throws IOException, SQLException {
    	if (image != null) {
            try {
                response.setContentType("image/jpeg");
                ServletOutputStream out = response.getOutputStream();
                IOUtils.copy(new ByteArrayInputStream(image), out);
                out.flush();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
}
