package com.transretail.crm.giftcard.service.impl;

import java.math.BigDecimal;
import java.util.List;
import java.util.Locale;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.support.TransactionSynchronizationAdapter;
import org.springframework.transaction.support.TransactionSynchronizationManager;
import org.springframework.validation.BindingResult;

import com.google.common.collect.Lists;
import com.mysema.query.BooleanBuilder;
import com.mysema.query.Tuple;
import com.mysema.query.jpa.JPASubQuery;
import com.mysema.query.jpa.impl.JPAQuery;
import com.mysema.query.jpa.impl.JPAUpdateClause;
import com.mysema.query.types.Predicate;
import com.mysema.query.types.expr.BooleanExpression;
import com.transretail.crm.common.service.dto.response.ResultList;
import com.transretail.crm.common.service.util.SpringDataPagingUtil;
import com.transretail.crm.core.service.CodePropertiesService;
import com.transretail.crm.giftcard.dto.ReturnByCardsItemDto;
import com.transretail.crm.giftcard.dto.ReturnRecordDto;
import com.transretail.crm.giftcard.dto.ReturnSearchDto;
import com.transretail.crm.giftcard.dto.SalesOrderDto;
import com.transretail.crm.giftcard.entity.GiftCardInventory;
import com.transretail.crm.giftcard.entity.GiftCardInventoryStock.GCIStockStatus;
import com.transretail.crm.giftcard.entity.QGiftCardInventory;
import com.transretail.crm.giftcard.entity.QReturnRecord;
import com.transretail.crm.giftcard.entity.QSalesOrder;
import com.transretail.crm.giftcard.entity.QSalesOrderAlloc;
import com.transretail.crm.giftcard.entity.ReturnRecord;
import com.transretail.crm.giftcard.entity.SalesOrder;
import com.transretail.crm.giftcard.entity.support.GiftCardInventoryStatus;
import com.transretail.crm.giftcard.entity.support.ReturnStatus;
import com.transretail.crm.giftcard.entity.support.SalesOrderStatus;
import com.transretail.crm.giftcard.repo.GiftCardInventoryRepo;
import com.transretail.crm.giftcard.repo.ReturnRecordRepo;
import com.transretail.crm.giftcard.repo.SalesOrderRepo;
import com.transretail.crm.giftcard.service.GiftCardAccountingService;
import com.transretail.crm.giftcard.service.GiftCardInventoryStockService;
import com.transretail.crm.giftcard.service.GiftCardManager;
import com.transretail.crm.giftcard.service.ReturnByCardsService;
import com.transretail.crm.giftcard.service.SalesOrderService;



@Service("returnByCardsService")
public class ReturnByCardsServiceImpl implements ReturnByCardsService {
	
	private static final Logger logger = LoggerFactory.getLogger(ReturnByCardsServiceImpl.class);
	
	@Autowired
	private ReturnRecordRepo recordRepo;
	@PersistenceContext
	private EntityManager em;
	@Autowired
	private CodePropertiesService codePropertiesService;
	@Autowired
	private GiftCardManager giftCardManager;
	@Autowired
	private SalesOrderService salesOrderService;
	@Autowired
	private GiftCardInventoryRepo inventoryRepo;
	@Autowired
	private GiftCardInventoryStockService stockService;
	@Autowired
	private MessageSource messageSource;
	@Autowired
	private GiftCardAccountingService accountingService;
	@Autowired
	private SalesOrderRepo salesOrderRepo;
	
	
	@Override
    @Transactional(readOnly = true)
    public ResultList<ReturnRecordDto> getReturns(ReturnSearchDto searchForm) {
		searchForm.setByCards(true);
		Pageable pageable = SpringDataPagingUtil.INSTANCE.createPageable(searchForm.getPagination());
        BooleanExpression filter = searchForm.createSearchExpression();
        Page<ReturnRecord> page = filter != null ? recordRepo.findAll(filter, pageable) : recordRepo.findAll(pageable);
        return new ResultList<ReturnRecordDto>(ReturnRecordDto.toDto(page.getContent()), page.getTotalElements(), page.hasPreviousPage(),
            page.hasNextPage());
    }
	
	private String generateReturnNo() {
		DateTimeFormatter formatter = DateTimeFormat.forPattern("yyyyMMddHHmmss");
		return "RR" + formatter.print(new DateTime());
		//RRYYYYMMDDHHMMSS
	}
	
	@Override
    public boolean isValidReturn(ReturnRecordDto ret, BindingResult res) {
    	String starting = ret.getStartingSeries();
    	String ending = ret.getEndingSeries();
    	Locale loc = LocaleContextHolder.getLocale();
    	
    	QGiftCardInventory qIn = QGiftCardInventory.giftCardInventory;
    	Predicate seriesFilter = qIn.barcode.between(starting, ending)
    			.and(qIn.status.eq(GiftCardInventoryStatus.ACTIVATED));
    	
    	boolean isExist = new JPAQuery(em).from(qIn).where(seriesFilter).exists();
    	boolean isNotActivatedExist = new JPAQuery(em).from(qIn)
    			.where(qIn.barcode.between(starting, ending)
    					.and(qIn.status.ne(GiftCardInventoryStatus.ACTIVATED)))
    			.exists();
    	if(!isExist || isNotActivatedExist) {
    		res.reject(messageSource.getMessage("gc_return_invalid_series", null, loc));
    		return false;
    	}

		Long seriesStart = NumberUtils.toLong( starting.substring( 0, starting.length() - 4 ) );
		Long seriesEnd = NumberUtils.toLong( ending.substring( 0, ending.length() - 4 ) );
    	
    	QSalesOrderAlloc qAll = QSalesOrderAlloc.salesOrderAlloc;
    	QSalesOrderAlloc qAllA = new QSalesOrderAlloc("a");
    	QReturnRecord qRet = QReturnRecord.returnRecord;
    	QGiftCardInventory qGc = QGiftCardInventory.giftCardInventory;
    	Predicate allocFilter = 
    			qGc.barcode.between(starting, ending)
    			.and(qGc.series.goe( qAll.seriesStart ).and( qGc.series.loe( qAll.seriesEnd ) ))
    			//qAll.gcInventory.barcode.between(starting, ending)
    			.and(qAll.soItem.order.status.eq(SalesOrderStatus.SOLD))
    			/*.and(qAll.soItem.order.id.notIn(new JPASubQuery().from(qRet).distinct().list(qRet.orderNo.id)))*/
    			.and(qAll.soItem.order.lastUpdated.eq(new JPASubQuery().from(qAllA)
    					//.where( qAllA.gcInventory.id.eq(qAll.gcInventory.id) )
    					.where( qAllA.id.eq(qAll.id) )
    					.unique(qAllA.soItem.order.lastUpdated.max())));
    			
    	List<Tuple> orderIds = new JPAQuery(em).from(qAll, qGc ).where(allocFilter)
    			.groupBy(qAll.soItem.order.id, qAll.soItem.order.orderNo, 
    					qAll.soItem.order.customer.name, qAll.soItem.order.customer.id)
    			.list(qAll.soItem.order.id, qAll.soItem.order.orderNo, 
    					qAll.soItem.order.customer.name, qAll.soItem.order.customer.id);
    	if(CollectionUtils.isEmpty(orderIds) || orderIds.size() > 1) {
    		res.reject(messageSource.getMessage("gc_return_invalid_sales_order_multiple", null, loc));
    		return false;
    	} else {
    		Tuple order = orderIds.get(0);
    		Long orderId = order.get(qAll.soItem.order.id);
    		String orderNo = order.get(qAll.soItem.order.orderNo);
    		String custName = order.get(qAll.soItem.order.customer.name);
    		Long custId = order.get(qAll.soItem.order.customer.id);
    		
    		Tuple details = new JPAQuery(em).from(qIn).where(seriesFilter)
    		.groupBy(qIn.productName, qIn.productCode, qIn.status, qIn.profile.id)
    		.singleResult(qIn.productName, qIn.productCode, qIn.profile.id, qIn.status, qIn.balance.sum(), qIn.faceValue.sum());
    		
    		ret.setProductName(details.get(qIn.productName));
    		ret.setProductCode(details.get(qIn.productCode));
    		ret.setCardStatus(details.get(qIn.status));
    		ret.setBalance(details.get(qIn.balance.sum()));
    		ret.setFaceAmount(details.get(qIn.faceValue.sum()));
    		ret.setProdId(details.get(qIn.profile.id));
    		
    		if(ret.getOrderId() == null){
	    		ret.setOrderId(orderId);
	    		ret.setOrderNo(orderNo);
	    		ret.setCustomerName(custName);
	    		ret.setCustomerId(custId);
	    		
	    		return true;
	    	} else if(ret.getOrderId() != null && ret.getOrderId().longValue() != orderId.longValue()) {
	    		res.reject(messageSource.getMessage("gc_return_invalid_sales_order_multiple", null, loc));
	    		return false;
	    	}
    	}
    	
    	//TODO double entry, multiple products per entry validation
    		
    	return false;
    }
	
	@Override
	@Transactional(readOnly = true)
	public ReturnRecordDto getReturn(Long id) {
		return new ReturnRecordDto(recordRepo.findOne(id), true);
	}
	
	@Override
	@Transactional(readOnly = true)
	public ReturnRecordDto getReturnByRecordNo(String returnNo) {
		ReturnRecord record = recordRepo.findByRecordNo(returnNo);
		if(record == null)
			return null;
		return new ReturnRecordDto(record, false);
	}
	
	
	@Override
	@Transactional
	public void saveAndReturn(ReturnRecordDto recordDto) {
		save(recordDto);
		returnCards(recordDto);
	}
	
	@Override
	public void save(ReturnRecordDto recordDto) {
		if(StringUtils.isBlank(recordDto.getRecordNo())) {
			recordDto.setRecordNo(generateReturnNo());
		}
		populateDetails(recordDto);
		ReturnRecord record = new ReturnRecord();
		if(recordDto.getId() != null) {
			record = recordRepo.findOne(recordDto.getId());
		}
		
		recordRepo.save(recordDto.toModel(record));
		accountingService.forB2BReturn(new SalesOrderDto(salesOrderRepo.findOne(recordDto.getOrderId()), true), recordDto);
	}
	
	private void populateDetails(ReturnRecordDto dto) {
		QSalesOrder qOr = QSalesOrder.salesOrder;
		Tuple tuple = new JPAQuery(em)
			.from(qOr)
			.where(qOr.id.eq(dto.getOrderId()))
			.singleResult(qOr.discountVal, qOr.customer.id);
		dto.setFaceAmount(getTotalFaceAmount(dto));
		dto.setCustomerId(tuple.get(qOr.customer.id));
		BigDecimal disc = tuple.get(qOr.discountVal);
		if(disc == null)
			disc = BigDecimal.ZERO;
		dto.setReturnAmount(dto.getFaceAmount().subtract(dto.getFaceAmount().multiply(disc.divide(BigDecimal.TEN))));
	}
	
	private BigDecimal getTotalFaceAmount(ReturnRecordDto recordDto) {
		QGiftCardInventory qIn = QGiftCardInventory.giftCardInventory;
		return new JPAQuery(em).from(qIn)
				.where(createInventoryFilter(recordDto, qIn))
				.singleResult(qIn.faceValue.sum());
			
	}
	
	@Override
	public void returnCards(ReturnRecordDto recordDto) {
		QGiftCardInventory qIn = QGiftCardInventory.giftCardInventory;
		final Predicate inventoryFilter = createInventoryFilter(recordDto, qIn);
		final Long orderId = recordDto.getOrderId();
		long rows = new JPAUpdateClause(em, qIn)
	        .set(qIn.status, GiftCardInventoryStatus.IN_STOCK)
	        .set(qIn.ryns, Boolean.FALSE)
	        .set(qIn.previousBalance, 0.0)
	        .setNull(qIn.balance)
	        .setNull(qIn.salesType)
	        .setNull(qIn.activationDate)
	        .setNull(qIn.expiryDate)
	        .set(qIn.location, codePropertiesService.getDetailInvLocationHeadOffice())
	        .where(inventoryFilter).execute();
		
		if (TransactionSynchronizationManager.isSynchronizationActive()) {
            TransactionSynchronizationManager.registerSynchronization(new TransactionSynchronizationAdapter() {

                @Override
                public void afterCommit() {
                    logger.debug("Transaction::afterCommit started.");
                    logger.debug("Invoking returnB2b...running in separated thread.");
                    SalesOrder so = salesOrderService.get(orderId);
                    giftCardManager.returnB2BByCard(so, inventoryRepo.findAll(inventoryFilter));
                    logger.debug("Updating stocks...running in separated thread.");
                    saveStock(inventoryFilter);
                    logger.debug("Transaction::afterCommit executed.");
                }
            });
        }
	}
	
	@Override
	@Transactional
	public long updateByCardsRecord(String recordNo, ReturnStatus status) {
		QReturnRecord ret = QReturnRecord.returnRecord;
		return new JPAUpdateClause(em, ret)
        .set(ret.status, status)
        .where(ret.recordNo.eq(recordNo).and(ret.items.isNotEmpty())).execute();
	}
	
	private void saveStock(Predicate filter) {
		List<GiftCardInventory> inventories = Lists.newArrayList(inventoryRepo.findAll(filter));
		stockService.saveStocks(inventories, GCIStockStatus.RETURNED);
	}
	
	private Predicate createInventoryFilter(ReturnRecordDto recordDto, QGiftCardInventory qIn) {
		BooleanBuilder inventoryFilter = new BooleanBuilder();
		for(ReturnByCardsItemDto itemDto: recordDto.getReturns()) {
			if(StringUtils.isBlank(itemDto.getEndingSeries()))
				inventoryFilter.or(qIn.barcode.eq(itemDto.getStartingSeries()));
			else
				inventoryFilter.or(qIn.barcode.between(itemDto.getStartingSeries(), itemDto.getEndingSeries()));
			
		}
		return inventoryFilter.getValue();
	}
}
