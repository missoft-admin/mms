package com.transretail.crm.report.template.impl;

import java.io.File;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;
import java.util.Map;

import net.sf.jasperreports.engine.JRParameter;

import org.apache.commons.io.FileUtils;
import org.joda.time.LocalDate;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.context.support.MessageSourceResourceBundle;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import com.google.common.collect.Maps;
import com.transretail.crm.common.reporting.jasper.JRProcessor;
import com.transretail.crm.common.reporting.service.ReportService;
import com.transretail.crm.core.entity.MemberModel;
import com.transretail.crm.core.entity.PointsTxnModel;
import com.transretail.crm.core.entity.embeddable.CustomerProfile;
import com.transretail.crm.core.entity.enums.Status;
import com.transretail.crm.core.entity.lookup.LookupDetail;
import com.transretail.crm.core.entity.lookup.LookupHeader;
import com.transretail.crm.core.entity.lookup.Store;
import com.transretail.crm.core.repo.LookupDetailRepo;
import com.transretail.crm.core.repo.LookupHeaderRepo;
import com.transretail.crm.core.repo.MemberRepo;
import com.transretail.crm.core.repo.PointsTxnModelRepo;
import com.transretail.crm.core.repo.StoreRepo;
import com.transretail.crm.core.service.CodePropertiesService;
import com.transretail.crm.giftcard.entity.CardVendor;
import com.transretail.crm.giftcard.entity.GiftCardInventory;
import com.transretail.crm.giftcard.entity.GiftCardOrder;
import com.transretail.crm.giftcard.entity.GiftCardOrderItem;
import com.transretail.crm.giftcard.entity.ProductProfile;
import com.transretail.crm.giftcard.entity.support.GiftCardOrderStatus;
import com.transretail.crm.giftcard.entity.support.ProductProfileStatus;
import com.transretail.crm.giftcard.repo.CardVendorRepo;
import com.transretail.crm.giftcard.repo.GiftCardInventoryRepo;
import com.transretail.crm.giftcard.repo.GiftCardOrderRepo;
import com.transretail.crm.giftcard.repo.ProductProfileRepo;
import com.transretail.crm.giftcard.repo.SalesOrderAllocRepo;
import com.transretail.crm.giftcard.repo.SalesOrderItemRepo;
import com.transretail.crm.giftcard.repo.SalesOrderRepo;
import com.transretail.crm.report.template.CommonReportFilter;

/**
 * @author ftopico
 */
@Transactional
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath*:/META-INF/spring/applicationContext*.xml",
    "/META-INF/spring/applicationContext-test.xml"})
public class GiftCardExpireAndBalanceReportTest {

    @Autowired
    private CardVendorRepo cardVendorRepo;
    @Autowired
    private GiftCardOrderRepo giftCardOrderRepo;
    @Autowired
    private GiftCardInventoryRepo giftCardInventoryRepo;
    @Autowired
    private LookupDetailRepo lookupDetailRepo;
    @Autowired
    private LookupHeaderRepo lookupHeaderRepo;
    @Autowired
    private SalesOrderRepo salesOrderRepo;
    @Autowired
    private SalesOrderAllocRepo salesOrderAllocRepo;
    @Autowired
    private SalesOrderItemRepo salesOrderItemRepo;
    @Autowired
    private ProductProfileRepo productProfileRepo;
    @Autowired
    private CodePropertiesService codePropertiesService;
    @Autowired
    private GiftCardExpireAndBalanceReport giftCardExpireAndBalanceReport;
    @Autowired
    private MessageSource messageSource;
    @Autowired
    private ReportService reportService;
    private static final Logger logger = LoggerFactory.getLogger(AffiliateTransactionSalesReport.class);
    
    protected static final String FILTER_CUSTOMER_TYPE = "customerType";
    protected static final String FILTER_STORE = "store";
    protected static final String FILTER_STANDARD_REPORT_TYPE = "standardReportType";
    protected static final String VISIT_FREQ_REPORT_TYPE_COUNT = "customerCount";
    protected static final String VISIT_FREQ_REPORT_TYPE_PURCHASE = "purchaseCount";
    
    
    @Before
    public void setup() {
        LookupHeader hdr = new LookupHeader(codePropertiesService.getProductProfileFaceValue());
        hdr = lookupHeaderRepo.saveAndFlush(hdr);

        LookupDetail dtl = new LookupDetail("PPFV1");
        dtl.setDescription("100000");
        dtl.setHeader(hdr);
        dtl = lookupDetailRepo.saveAndFlush(dtl);
        
        ProductProfile product = new ProductProfile();
        product.setProductCode("000000000001");
        product.setProductDesc("profile 1");
        product.setFaceValue(dtl);
        product.setCardFee(0.0);
        product.setMaxAmount(100L);
        product.setEffectiveMonths(10L);
        product.setUnitCost(100D);
        product.setStatus(ProductProfileStatus.APPROVED);
        product = productProfileRepo.saveAndFlush(product);
        
        CardVendor vendor = new CardVendor();
        vendor.setFormalName("vendor");
        vendor.setMailAddress("ail@a.com");
        vendor.setEmailAddress("email");
        vendor.setEnabled(true);
        vendor.setRegion("region");
        vendor.setDefaultVendor(true);
        vendor = cardVendorRepo.saveAndFlush(vendor);
        
        GiftCardOrder order = new GiftCardOrder();
        order.setPoNumber("1");
        order.setPoDate(LocalDate.now());
        order.setMoNumber("1");
        order.setMoDate(LocalDate.now());
        order.setVendor(vendor);
        order.setItems(new ArrayList<GiftCardOrderItem>());
        order.setStatus(GiftCardOrderStatus.APPROVED);
        order = giftCardOrderRepo.saveAndFlush(order);
        
        GiftCardInventory gcInventory = new GiftCardInventory();
        gcInventory.setProfile(product);
        gcInventory.setOrder(order);
        gcInventory.setProductCode(product.getProductCode());
        gcInventory.setProductName(product.getProductDesc());
        gcInventory.setSeqNo(1);
        gcInventory.setBatchNo(1);
        gcInventory.setOrderDate(LocalDate.now());
        gcInventory.setSeries(10000L);
        gcInventory.setBarcode(String.format("%05d", 1));
        gcInventory.setDeliveryReceipt("1");
        gcInventory.setReceiveDate(LocalDate.now());
        gcInventory = giftCardInventoryRepo.saveAndFlush(gcInventory);
        
        
        
    }
    
    @After
    public void cleanup() {
        productProfileRepo.deleteAll();
        giftCardInventoryRepo.deleteAll();
        giftCardOrderRepo.deleteAll();
        cardVendorRepo.deleteAll();
        lookupDetailRepo.deleteAll();
        lookupHeaderRepo.deleteAll();
    }
    
    @Test
    public void createJrProcessorTest() throws Exception {
        Map<String, String> map = Maps.newHashMap();
        map.put(CommonReportFilter.DATE_FROM, "01-01-2013");
        map.put(CommonReportFilter.DATE_TO, "01-01-2100");
        JRProcessor jrProcessor = giftCardExpireAndBalanceReport.createJrProcessor(map);
        
        File testClassesDir = FileUtils.toFile(Thread.currentThread().getContextClassLoader().getResource(""));
        File output = new File(testClassesDir, "test-visit-expire-and-balance-report-output.pdf");
        logger.debug("Output location {}", output.getAbsolutePath());
        output.delete();
        Locale locale = LocaleContextHolder.getLocale();
        jrProcessor.addParameter(JRParameter.REPORT_LOCALE, locale);
        jrProcessor.addParameter(JRParameter.REPORT_RESOURCE_BUNDLE, new MessageSourceResourceBundle(messageSource, locale));
        reportService.exportPdfReport(jrProcessor, output);

        double bytes = output.length();
        double kilobytes = (bytes / 1024);
        logger.debug("Kilobytes: {}", kilobytes);
    }
    
}
