package com.transretail.crm.giftcard.repo;

import org.springframework.stereotype.Repository;

import com.transretail.crm.common.repo.CrmQueryDslPredicateExecutor;
import com.transretail.crm.giftcard.entity.SalesOrderDeptAlloc;

/**
 *
 */
@Repository
public interface SalesOrderDeptAllocRepo extends CrmQueryDslPredicateExecutor<SalesOrderDeptAlloc, Long> {
	
	
}
