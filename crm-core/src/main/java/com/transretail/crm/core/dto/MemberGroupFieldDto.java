package com.transretail.crm.core.dto;

import java.util.List;

import com.transretail.crm.core.entity.MemberGroup;
import com.transretail.crm.core.entity.MemberGroupField;
import com.transretail.crm.core.entity.MemberGroupFieldValue;
import com.transretail.crm.core.entity.lookup.LookupDetail;
import com.transretail.crm.core.entity.lookup.LookupHeader;

public class MemberGroupFieldDto {
	Long id;
	MemberGroup memberGroup;
	LookupHeader name;
	LookupDetail type;
	Boolean isLookup;
	List<MemberGroupFieldValue> fieldValues;
	
	public MemberGroupFieldDto() {
	}
	
	public MemberGroupFieldDto(MemberGroupField groupField) {
//		BeanUtils.copyProperties(groupField, this);
        this.id = groupField.getId();
        this.memberGroup = groupField.getMemberGroup();
        this.name = groupField.getName();
        this.type = groupField.getType();
	}

	public MemberGroup getMemberGroup() {
		return memberGroup;
	}

	public void setMemberGroup(MemberGroup memberGroup) {
		this.memberGroup = memberGroup;
	}

	public LookupHeader getName() {
		return name;
	}

	public void setName(LookupHeader name) {
		this.name = name;
	}

	public LookupDetail getType() {
		return type;
	}

	public void setType(LookupDetail type) {
		this.type = type;
	}

	public List<MemberGroupFieldValue> getFieldValues() {
		return fieldValues;
	}

	public void setFieldValues(List<MemberGroupFieldValue> fieldValues) {
		this.fieldValues = fieldValues;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Boolean getIsLookup() {
		return isLookup;
	}

	public void setIsLookup(Boolean isLookup) {
		this.isLookup = isLookup;
	}
}
