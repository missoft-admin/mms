<%@ include file="../common/taglibs.jsp" %>

<sec:authorize var="isHelpDesk" ifAnyGranted="ROLE_HELPDESK"/>

<div id="content_promotionList"
data-no-participants-alert="<spring:message code = "report.no.data" />">


  <c:if test="${not empty promotionPrograms}">
  <div class="tableBody">
    <div class="contentTableTitle"></div>

    <div>
			<c:forEach var="program" items="${promotionPrograms}">
			<div class="data-content">

        <div class="page-header page-header2">
          <div><h3><c:out value="${program.description}" /></h3></div>
          <div><h5><c:out value="${program.name}" />, <c:out value="${program.dispStartDate}" /> - <c:out value="${program.dispEndDate}" /></h5></div>
        </div>
        
            <c:forEach var="campaign" items="${program.campaigns}">
            <div class="data-sub-content">
            <fieldset>
              <div class="accordion" id="accordion2">
                  <div class="accordion-group">
              <div class="accordion-heading">
                <a class="accordion-toggle" data-toggle="collapse" data-target="#accor-${campaign.id}">
                  <div class="page-header3">
                    <div><h4><c:out value="${campaign.description}" /></h4></div>
                    <div><h6><c:out value="${campaign.name}" />, <c:out value="${campaign.dispStartDate}" /> - <c:out value="${campaign.dispEndDate}" /></h6></div>
                  </div>
                </a>
              </div>
              <c:if test="${not empty campaign.promotions}">
        
              <div id="accor-${campaign.id}" class="collapse">
                <div class="accordion-inner">
                    
                  <div class="data-list-content">
                    <table class="table table-bordered table-striped">
                      <thead>
                        <tr>
                          <th><spring:message code="promotion_prop_name" /></th>
                          <th><spring:message code="promotion_prop_description" /></th>
                          <th><spring:message code="promotion_prop_startdate" /></th>
                          <th><spring:message code="promotion_prop_enddate" /></th>
                          <th><spring:message code="promotion_prop_affecteddays" /></th>
                          <th><spring:message code="promotion_prop_rewardtype" /></th>
                          <th><spring:message code="promotion_prop_status" /></th>
                          <th></th>
                        </tr>
                      </thead>
                      <tbody>            
                        <c:forEach var="promotion" items="${campaign.promotions}">
                        <tr>
                          <td class="table-data-01"><c:out value="${promotion.name}" /></td>
                          <td><c:out value="${promotion.description}" /></td>
                          <td class="promotion_endDate"><c:out value="${promotion.startDate.time}" /></td>
                          <td class="promotion_endDate"><c:out value="${promotion.endDate.time}" /></td>
                          <td>
                            <c:forTokens var="affectedDay" items="${promotion.affectedDays}" delims=",">
                              <spring:message code="lookup_${affectedDay}" /><br/>
                            </c:forTokens>
                          </td>
                          <td><spring:message code="lookup_${promotion.rewardType}" text="" /></td>
                          <td id="status${promotion.id}" class="uppercase"><spring:message code="lookup_${promotion.status}" /></td>
                          <td class="">

                            <div class="" style="width: 180px">
                              <div><a href="<c:url value="/promotion/process/${promotion.id}" />" class="link_promotionRemarks btn btn-primary tiptip icn-view pull-left" title="<spring:message code="label_history"/>"><spring:message code="label_history"/></a></div>
                              <c:if test="${!isHelpDesk}">
                                <div>
                                  <c:choose>
                                    <c:when test="${ promotion.rewardType eq 'RWRD005' }"><c:url var="editUrl" value="/promotion/redemption/edit/${promotion.id}" /></c:when>
                                    <c:when test="${ promotion.rewardType eq 'RWRD006' }"><c:url var="editUrl" value="/promotion/stamp/edit/${promotion.id}" /></c:when>
                                    <c:otherwise><c:url var="editUrl" value="/promotion/edit/${promotion.id}" /></c:otherwise>
                                  </c:choose>
                                  <a href="${editUrl}" class="btn tiptip tiptip icn-edit pull-left" title="<spring:message code="label_process"/>"><spring:message code="label_edit"/></a>
                                </div>
                              </c:if>
                              <div><a class="btn btn-primary link_overlap tiptip icn-view pull-left" id="${promotion.id}" title="<spring:message code="promotion_label_checkoverlap"/>"><spring:message code="promotion_label_checkoverlap"/></a></div>
                              <%-- <div class=""><a href="<c:url value="/promotion/target/member/count/${promotion.id}" />" class="countMembersLink pull-left tiptip btn icn-view pull-left" title="<spring:message code="promotion_label_membercount"/>" ><spring:message code="promotion_label_membercount"/></a></div> --%>
                              <div class=""><a href="<c:url value="/promotion/target/member/target/${promotion.id}" />" class="countMembersLink pull-left tiptip btn icn-view pull-left" title="<spring:message code="promotion_label_members"/>" ><spring:message code="promotion_label_membercount"/></a></div>
                              
                              <c:if test="${promotion.status != 'STAT003' }">
                              <div class=""><a href="<c:url value="/promotion/promoparticipants/${promotion.id}" />" data-validation-url="<c:url value="/promotion/promoparticipants/validate/${promotion.id}" />" class="promoParticipantsLink pull-left tiptip btn icn-print pull-left" title="<spring:message code="promotion_label_promoparticipants"/>" ><spring:message code="promotion_label_promoparticipants"/></a></div>
                              </c:if>
                            </div>

                          </td>
                        </tr>
                        </c:forEach>
                      </tbody>
                    </table>
                  </div> 
          
                </div> <!--accordion-inner-->
              </div> <!--collapseTwo collapse in-->
                             
              </c:if>


              <!-- <div id="list_promotion" class="list_promotion" 
    				    data-url="<c:url value="/promotion/list/${campaign.id}" />"
    				    data-hdr-name="<spring:message code="promotion_prop_name"/>"
    				    data-hdr-description="<spring:message code="promotion_prop_description"/>" 
    				    data-hdr-lastname="<spring:message code="user_prop_lastname"/>" 
    				    data-hdr-enabled="<spring:message code="user_prop_enabled"/>" ></div> -->
        			  </div> <!--accordion-group-->
              </div> <!--accordion accordion2-->
            </fieldset>
            </div>
            </c:forEach>

    			</div>
    			</c:forEach>
			
			

			
			

		</div>

  </div>
  
  <div id="form_promotionRemarks"></div>
  <div id="content_targetMemberCount" class="modal hide  nofly modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4><spring:message code="promotion_label_membercount" /></h4>
      </div>
    </div>
    <div class="modal-body">
      <div class="control-group">
        <label for="" class="control-label"><spring:message code="promotion_label_membercount"/></label>
        <spring:message var="label_days" code="promotion_label_days" />
        <div class="controls data-content-04"><span id="noOfTargetMembers"></span>&nbsp;<spring:message code="promotion_label_members"/></div>
      </div>
    </div>
  </div>
  <div id="content_targetMemberList" class="modal hide nofly modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <span><spring:message code="promotion_label_members" /></span>
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
      </div>
    </div>
    <div class="modal-body">
      <div id="list_targetMember" 
	      data-url="" 
	      data-hdr-accountno="<spring:message code="promo_target_member_acctnumber"/>" 
	      data-hdr-username="<spring:message code="promo_target_member_username"/>" 
	      data-hdr-firstname="<spring:message code="promo_target_member_firstname"/>" 
	      data-hdr-lastname="<spring:message code="promo_target_member_lastname"/>" ></div>
    </div>
  </div>
  </c:if>
 
  <%@ include file="overlap.jsp" %>
  <jsp:include page="../common/confirm.jsp" />


  <link href="<spring:url value="/css/dataTables.css" />" rel="stylesheet">
  <script src="<spring:url value="/js/jquery/jquery.dataTables.js" />" ></script>
  <script src="<spring:url value="/js/jquery/jquery.dataTables.linkLengthMenu.js" />"></script>
  <script src="<spring:url value="/js/jquery/jquery.dataTables.customPaging.js" />"></script>
  <script src="<spring:url value="/js/jquery/ajax-datatable.js" />" ></script>
  <script src="<spring:url value="/js/common/form2js.js" />" ></script>
  <script src="<spring:url value="/js/common/json2.js" />" ></script>
  <script src="<spring:url value="/js/jquery/jquery.toObject.js" />" ></script>

  <script src="<spring:url value="/js/viewspecific/promotion/promotionList.js" />" type="text/javascript"><![CDATA[&nbsp; ]]></script>


</div>

<style> 
    .accordion-inner {
      border-top: none;
      padding: 0;
      margin-top: -10px;
    }
    .accordion-heading .page-header3 {
        border-top: none;
        margin: 10px 0 0;
        padding-bottom: 0px;
    }
    .accordion-inner .data-list-content {
        margin-bottom: -16px;
        padding-bottom: 0;
    }
    .data-sub-content {
        padding-bottom: 0 !important;
    }
    
</style>
