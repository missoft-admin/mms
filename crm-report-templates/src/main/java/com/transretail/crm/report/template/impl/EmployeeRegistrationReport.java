package com.transretail.crm.report.template.impl;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.google.common.collect.Sets;
import com.mysema.query.jpa.impl.JPAQuery;
import com.mysema.query.types.expr.BooleanExpression;
import com.transretail.crm.common.reporting.jasper.ClasspathFileResolver;
import com.transretail.crm.common.reporting.jasper.DefaultJRProcessor;
import com.transretail.crm.common.reporting.jasper.JRProcessor;
import com.transretail.crm.common.util.DateUtil;
import com.transretail.crm.core.entity.MemberModel;
import com.transretail.crm.core.entity.QMemberModel;
import com.transretail.crm.core.entity.embeddable.Address;
import com.transretail.crm.core.entity.embeddable.CustomerProfile;
import com.transretail.crm.core.entity.enums.MemberCreatedFromType;
import com.transretail.crm.core.entity.enums.MemberStatus;
import com.transretail.crm.core.entity.enums.ReportType;
import com.transretail.crm.core.entity.enums.Status;
import com.transretail.crm.core.repo.MemberRepo;
import com.transretail.crm.core.service.CodePropertiesService;
import com.transretail.crm.core.service.StoreService;
import com.transretail.crm.report.template.*;
import java.util.*;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.apache.commons.lang3.StringUtils;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.MessageSourceAware;
import org.springframework.context.support.MessageSourceAccessor;
import org.springframework.stereotype.Service;

/**
 * @author Ervy V. Bigornia (ebigornia@exist.com)
 */
@Service("employeeRegistrationReport")
public class EmployeeRegistrationReport implements ReportTemplate, MessageSourceAware, ReportsCustomizer, ReportDateFormatProvider, NoDataFoundSupport {

    protected static final String TEMPLATE_NAME = "Employee Registration Report";
    protected static final String REPORT_NAME = "reports/employee_registration.jasper";
    protected static final String FILTER_DATE_FROM = CommonReportFilter.DATE_FROM;
    protected static final String FILTER_DATE_TO = CommonReportFilter.DATE_TO;
    protected static final String FILTER_STATUS = "status";
    private static final String SEARCH_DATE_FORMAT = DateUtil.SEARCH_DATE_FORMAT;
    private static final DateTimeFormatter INPUT_DATE_PATTERN = DateTimeFormat.forPattern(SEARCH_DATE_FORMAT);
    private static final DateTimeFormatter FULL_DATE_PATTERN = DateTimeFormat.forPattern("MMMM d, yyyy");
    private static final String PARAM_DATE_RANGE = "dateRange";
    private static final String PARAM_IS_EMPTY = "isEmpty";
    private static final String PARAM_NO_DATA_FOUND = "noDataFound";
    private static final String PARAM_TOTAL_ENCODED = "totalEncoded";
    private static final String PARAM_TOTAL_UPLOADED = "totalUploaded";
    private static final String REG_TYPE_ENCODE = "Encoded";
    private static final String REG_TYPE_UPLOAD = "Uploaded";
    private static final ReportType[] REPORT_TYPES = {ReportType.EXCEL};
    private MessageSourceAccessor messageSource;
    @PersistenceContext
    private EntityManager em;
    @Autowired
    private StoreService storeService;
    @Autowired
    private CodePropertiesService codePropertiesService;
    @Autowired
    private MemberRepo memberRepo;
    @Autowired
    private LOVFactory lOVFactory;

    @Override
    public ReportType[] getReportTypes() {
        return REPORT_TYPES;
    }

    @Override
    public void setMessageSource(MessageSource messageSource) {
        this.messageSource = new MessageSourceAccessor(messageSource);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getName() {
        return TEMPLATE_NAME;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getDescription() {
        return messageSource.getMessage("employee.registration.report.title");
    }

    /**
     * {@inheritDoc}
     *
     * @return
     */
    @Override
    public Set<FilterField> getFilters() {
        Set<FilterField> filters = new LinkedHashSet<FilterField>(4);
        filters.add(FilterField.createDropdownField(
                FILTER_STATUS, messageSource.getMessage("employee.registration.filter.status"),
                statusOptions()));
        filters.add(FilterField.createDateField(CommonReportFilter.DATE_FROM,
                messageSource.getMessage("employee.registration.filter.datefrom"),
                DateUtil.SEARCH_DATE_FORMAT, true));
        filters.add(FilterField.createDateField(CommonReportFilter.DATE_TO,
                messageSource.getMessage("employee.registration.filter.dateto"),
                DateUtil.SEARCH_DATE_FORMAT, true));
        filters.add(FilterField.createMultipleSelectField(CommonReportFilter.STORES,
                messageSource.getMessage("employee.registration.filter.stores"), lOVFactory.getStoreOptions()));
        return filters;
    }

    private Map<String, String> statusOptions() {
        Map<String, String> selectValues = Maps.newHashMap();
        selectValues.put("", "");

        for (Status status : Status.values()) {
            selectValues.put(status.name(), status.name());
        }

        return selectValues;
    }

    private DateTime toDateTime(String dateString) {
        if (StringUtils.isNotBlank(dateString)) {
            return INPUT_DATE_PATTERN.parseDateTime(dateString);
        }
        return null;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean canView(Set<String> permissions) {
        return permissions.contains("REPORT_EMPLOYEE_REGISTRATION_SUMMARY_VIEW");
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public JRProcessor createJrProcessor(Map<String, String> map) {
        QMemberModel qMemberModel = QMemberModel.memberModel;
        StringBuilder dateRange = new StringBuilder(50);

        long totalEncoded = 0;
        long totalUploaded = 0;
        JPAQuery statement = createQuery(map)
                .groupBy(qMemberModel.memberCreatedFromType, qMemberModel.registeredStore, qMemberModel.accountId)
                .orderBy(qMemberModel.memberCreatedFromType.asc(), qMemberModel.registeredStore.asc(), qMemberModel.accountId.asc());
        DateTime earliestDate = statement.singleResult(qMemberModel.created.min());
        List<String> members = statement.list(qMemberModel.accountId);

        Map<String, Object> parameters = Maps.newHashMap();
        parameters.put(PARAM_DATE_RANGE, dateRange.toString());

        List<ReportBean> beansDataSource = Lists.newArrayList();

        if (!members.isEmpty()) {
            Map<String, ReportBean> reportBeanMap = Maps.newHashMap();
            for (String accountId : members) {
                MemberModel memberModel = memberRepo.findByAccountId(accountId);
                String registerType;
                MemberCreatedFromType memberCreatedFromTypeCode = memberModel.getMemberCreatedFromType();
                if (memberCreatedFromTypeCode != null && memberCreatedFromTypeCode.equals(MemberCreatedFromType.FROM_UPLOAD)) {
                    registerType = REG_TYPE_UPLOAD;
                    totalUploaded += 1;
                } else {
                    registerType = REG_TYPE_ENCODE;
                    totalEncoded += 1;
                }

                String store = memberModel.getRegisteredStore();
                String storeName = memberModel.getStoreName();
                try {
                    storeName = storeService.getStoreByCode(store).getName();
                } catch (NullPointerException npe) {

                }
                String employeeNo = memberModel.getAccountId();
                String firstName = StringUtils.defaultString(memberModel.getFirstName());
                String lastName = StringUtils.defaultString(memberModel.getLastName());
                String terminated = memberModel.getAccountStatus().toString();

                if (terminated.equalsIgnoreCase(MemberStatus.ACTIVE.toString())) {
                    terminated = "N";
                } else {
                    terminated = "Y";
                }

                String employeeType = "";
                if (null != memberModel.getEmpType()) {
                    employeeType = memberModel.getEmpType().getDescription();
                }

                String department = "";
                if (null != memberModel.getProfessionalProfile()) {
                    if (null != memberModel.getProfessionalProfile().getDepartment()) {
                        department = memberModel.getProfessionalProfile().getDepartment().getDescription();
                    }
                }

                Date dateOfBirth = null;
                String placeOfBirth = "", gender = "", religion = "", nationality = "";

                if (null != memberModel.getCustomerProfile()) {
                    CustomerProfile customerProfile = memberModel.getCustomerProfile();
                    dateOfBirth = customerProfile.getBirthdate();
                    placeOfBirth = customerProfile.getPlaceOfBirth();
                    if (null != customerProfile.getGender()) {
                        gender = customerProfile.getGender().getDescription();
                    }
                    if (null != customerProfile.getReligion()) {
                        religion = customerProfile.getReligion().getDescription();
                    }
                    if (null != customerProfile.getNationality()) {
                        nationality = customerProfile.getNationality().getDescription();
                    }
                }

                String contact = memberModel.getContact();

                String street = "", city = "", postCode = "";

                if (null != memberModel.getAddress()) {
                    Address address = memberModel.getAddress();
                    street = address.getStreet();
                    city = address.getCity();
                    postCode = address.getPostCode();
                }

                StringBuilder keyBuilder = new StringBuilder(100);
                keyBuilder.append(registerType);
                keyBuilder.append("-");
                keyBuilder.append(store);
                keyBuilder.append("-");
                keyBuilder.append(employeeNo);
                String key = keyBuilder.toString();

                ReportBean bean = reportBeanMap.get(key);
                if (bean == null) {
                    bean = new ReportBean(registerType, store, storeName, employeeNo, firstName, lastName, terminated,
                            employeeType, department, dateOfBirth, placeOfBirth, gender, religion, nationality,
                            contact, street, city, postCode);
                    reportBeanMap.put(key, bean);
                }
            }
            beansDataSource.addAll(reportBeanMap.values());

            List<ReportBean> uploadList = new ArrayList<ReportBean>();
            List<ReportBean> encodeList = new ArrayList<ReportBean>();
            for (ReportBean bean : beansDataSource) {
                if (bean.getRegisterType().equalsIgnoreCase(REG_TYPE_UPLOAD)) {
                    uploadList.add(bean);
                } else {
                    encodeList.add(bean);
                }
            }
            beansDataSource.removeAll(uploadList);
            beansDataSource.removeAll(encodeList);

            Collections.sort(uploadList, new Comparator<ReportBean>() {
                @Override
                public int compare(ReportBean o1, ReportBean o2) {
                    String store1 = StringUtils.defaultString(o1.getStore());
                    String store2 = StringUtils.defaultString(o2.getStore());
                    return store1.compareTo(store2);
                }
            });
            Collections.sort(encodeList, new Comparator<ReportBean>() {
                @Override
                public int compare(ReportBean o1, ReportBean o2) {
                    String store1 = StringUtils.defaultString(o1.getStore());
                    String store2 = StringUtils.defaultString(o2.getStore());
                    return store1.compareTo(store2);
                }
            });

            beansDataSource.addAll(encodeList);
            beansDataSource.addAll(uploadList);

            parameters.put(PARAM_TOTAL_ENCODED, totalEncoded);
            parameters.put(PARAM_TOTAL_UPLOADED, totalUploaded);

            List<ReportBean> toRemove = new ArrayList<ReportBean>();
            for (ReportBean bean : beansDataSource) {
                if (null == bean.getEmployeeNo() || bean.getEmployeeNo().equals("")) {
                    toRemove.add(bean);
                }
            }
            beansDataSource.removeAll(toRemove);
        }

        if (beansDataSource.isEmpty()) {
            parameters.put(PARAM_IS_EMPTY, true);
            parameters.put(PARAM_NO_DATA_FOUND, "No data found");
            beansDataSource.add(new ReportBean());
        }

        DefaultJRProcessor jrProcessor = new DefaultJRProcessor(REPORT_NAME, beansDataSource);
        jrProcessor.addParameters(parameters);
        jrProcessor.setFileResolver(new ClasspathFileResolver("reports"));
        return jrProcessor;
    }

    private JPAQuery createQuery(Map<String, String> map) {
        DateTime dateFrom = toDateTime(map.get(FILTER_DATE_FROM));
        DateTime dateTo = toDateTime(map.get(FILTER_DATE_TO));
        String statusCode = map.get(FILTER_STATUS);
        String stores = map.get(CommonReportFilter.STORES);

        QMemberModel qMemberModel = QMemberModel.memberModel;
        StringBuilder dateRange = new StringBuilder(50);

        BooleanExpression predicate = qMemberModel.memberType.code.eq(codePropertiesService.getDetailMemberTypeEmployee());

        if (StringUtils.isNotBlank(statusCode)) {
            predicate = predicate.and(qMemberModel.accountStatus.stringValue().eq(statusCode));
        }

        if (dateFrom != null && dateTo != null) {
            dateFrom = dateFrom.toLocalDate().toDateTimeAtStartOfDay();
            dateTo = dateTo.toLocalDate().plusDays(1).toDateTimeAtStartOfDay().minusMillis(1);
            predicate = predicate.and(qMemberModel.created.goe(dateFrom));
            predicate = predicate.and(qMemberModel.created.loe(dateTo));
            dateRange.append(FULL_DATE_PATTERN.print(dateFrom));
            dateRange.append(" - ");
            dateRange.append(FULL_DATE_PATTERN.print(dateTo));
        } else if (dateFrom != null) {
            dateFrom = dateFrom.toLocalDate().toDateTimeAtStartOfDay();
            predicate = predicate.and(qMemberModel.created.goe(dateFrom));
            dateRange.append(messageSource.getMessage("employee.registration.report.label.datefrom"));
            dateRange.append(" ");
            dateRange.append(FULL_DATE_PATTERN.print(dateFrom));
        } else if (dateTo != null) {
            dateTo = dateTo.toLocalDate().plusDays(1).toDateTimeAtStartOfDay().minusMillis(1);
            predicate = predicate.and(qMemberModel.created.loe(dateTo));
        }

        if (StringUtils.isNotBlank(stores)) {
            predicate = predicate.and(qMemberModel.registeredStore.in(stores.split("!")));
        }

        final JPAQuery statement = new JPAQuery(em).from(qMemberModel).where(predicate);

        return statement;
    }

    @Override
    public Set<Option> customizerOption() {
        return Sets.newHashSet(Option.DATE_FROM_OPTIONAL, Option.DATE_RANGE_FULL_VALIDATION);
    }

    @Override
    public DateTimeFormatter getDefaultDateFormat() {
        return INPUT_DATE_PATTERN;
    }

    @Override
    public boolean isEmpty(Map<String, String> filter) {
        return createQuery(filter).notExists();
    }

    /**
     * Bean used by member card activation jasper report. Access modifier is
     * intentionallly set to public because jasper fill manager cannot read
     * protected or private classes
     */
    public static class ReportBean {

        private String registerType;
        private String store;
        private String storeName;
        private String employeeNo;
        private String firstName;
        private String lastName;
        private String terminated;
        private String employeeType;
        private String department;
        private Date dateOfBirth;
        private String placeOfBirth;
        private String gender;
        private String religion;
        private String nationality;
        private String contact;
        private String street;
        private String city;
        private String postCode;

        public ReportBean() {

        }

        public ReportBean(String registerType, String store, String storeName, String employeeNo,
                String firstName, String lastName, String terminated, String employeeType,
                String department, Date dateOfBirth, String placeOfBirth, String gender,
                String religion, String nationality, String contact, String street,
                String city, String postCode) {
            this.registerType = registerType;
            this.store = store;
            this.storeName = storeName;
            this.employeeNo = employeeNo;
            this.firstName = firstName;
            this.lastName = lastName;
            this.terminated = terminated;
            this.employeeType = employeeType;
            this.department = department;
            this.dateOfBirth = dateOfBirth;
            this.placeOfBirth = placeOfBirth;
            this.gender = gender;
            this.religion = religion;
            this.nationality = nationality;
            this.contact = contact;
            this.street = street;
            this.city = city;
            this.postCode = postCode;
        }

        public String getStore() {
            return store;
        }

        public void setStore(String store) {
            this.store = store;
        }

        public String getStoreName() {
            return storeName;
        }

        public void setStoreName(String storeName) {
            this.storeName = storeName;
        }

        public String getEmployeeNo() {
            return employeeNo;
        }

        public void setEmployeeNo(String employeeNo) {
            this.employeeNo = employeeNo;
        }

        public String getFirstName() {
            return firstName;
        }

        public void setFirstName(String firstName) {
            this.firstName = firstName;
        }

        public String getLastName() {
            return lastName;
        }

        public void setLastName(String lastName) {
            this.lastName = lastName;
        }

        public String getTerminated() {
            return terminated;
        }

        public void setTerminated(String terminated) {
            this.terminated = terminated;
        }

        public String getEmployeeType() {
            return employeeType;
        }

        public void setEmployeeType(String employeeType) {
            this.employeeType = employeeType;
        }

        public String getDepartment() {
            return department;
        }

        public void setDepartment(String department) {
            this.department = department;
        }

        public Date getDateOfBirth() {
            return dateOfBirth;
        }

        public void setDateOfBirth(Date dateOfBirth) {
            this.dateOfBirth = dateOfBirth;
        }

        public String getPlaceOfBirth() {
            return placeOfBirth;
        }

        public void setPlaceOfBirth(String placeOfBirth) {
            this.placeOfBirth = placeOfBirth;
        }

        public String getGender() {
            return gender;
        }

        public void setGender(String gender) {
            this.gender = gender;
        }

        public String getReligion() {
            return religion;
        }

        public void setReligion(String religion) {
            this.religion = religion;
        }

        public String getNationality() {
            return nationality;
        }

        public void setNationality(String nationality) {
            this.nationality = nationality;
        }

        public String getContact() {
            return contact;
        }

        public void setContact(String contact) {
            this.contact = contact;
        }

        public String getStreet() {
            return street;
        }

        public void setStreet(String street) {
            this.street = street;
        }

        public String getCity() {
            return city;
        }

        public void setCity(String city) {
            this.city = city;
        }

        public String getPostCode() {
            return postCode;
        }

        public void setPostCode(String postCode) {
            this.postCode = postCode;
        }

        public String getRegisterType() {
            return registerType;
        }

        public void setRegisterType(String registerType) {
            this.registerType = registerType;
        }

    }
}
