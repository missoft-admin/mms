package com.transretail.crm.web.member.bean;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Past;
import javax.validation.constraints.Pattern;

import org.hibernate.validator.constraints.NotEmpty;
import org.joda.time.DateTime;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.util.StringUtils;

import com.transretail.crm.core.entity.MemberModel;
import com.transretail.crm.core.entity.embeddable.CustomerProfile;
import com.transretail.crm.core.entity.lookup.LookupDetail;
import com.transretail.crm.core.util.FormatterUtil;
import com.transretail.crm.core.validator.Confirm;
import com.transretail.crm.core.validator.Gender;
import com.transretail.crm.core.validator.Password;

/**
 * @author Allan G. Ramirez (agramirez@exist.com)
 */
@Confirm(field = "password")
public class RegisterDto {
    private String lastName;
    @NotEmpty
    private String firstName;
    private String middleName;
    @NotEmpty
    @Pattern(regexp = "^$|^([0-9a-zA-Z]([-.\\w]*[0-9a-zA-Z])*@([0-9a-zA-Z][-\\w]*[0-9a-zA-Z]\\.)+[a-zA-Z]{2,9})")
    private String email;
    @NotEmpty
    private String username;
    @Password
    private String password;
    @NotEmpty
    private String confirmPassword;
    @Gender
    private String genderTmp;
    @NotNull
    private LookupDetail gender;
    @NotNull
    @Past
    @DateTimeFormat(pattern = "dd-MM-yyyy")
    private DateTime birthDate;
    @NotEmpty
    private String contact;
    private String preferredStoreCode;

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getMiddleName() {
        return middleName;
    }

    public void setMiddleName(String middleName) {
        this.middleName = middleName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getConfirmPassword() {
        return confirmPassword;
    }

    public void setConfirmPassword(String confirmPassword) {
        this.confirmPassword = confirmPassword;
    }

    public String getGenderTmp() {
        return genderTmp;
    }

    public void setGenderTmp(String gender) {
        this.genderTmp = gender;
    }

    public DateTime getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(DateTime birthDate) {
        this.birthDate = birthDate;
    }

    public String getContact() {
        return contact;
    }

    public void setContact(String contact) {
        this.contact = contact;
    }

    public String getPreferredStoreCode() {
        return preferredStoreCode;
    }

    public void setPreferredStoreCode(String preferredStoreCode) {
        this.preferredStoreCode = preferredStoreCode;
    }

    public LookupDetail getGender() {
        return gender;
    }

    public void setGender(LookupDetail gender) {
        this.gender = gender;
    }

    public MemberModel toModel() {
        MemberModel model = new MemberModel();
        model.setUsername(username);
        model.setLastName(lastName);
        model.setFirstName(firstName);
        model.setMiddleName(middleName);
        model.setEmail(email);
        model.setPassword(password);
        CustomerProfile profile = new CustomerProfile();
        profile.setGender(gender);

        if (birthDate != null) {
            profile.setBirthdate(birthDate.toDate());
        }
        model.setCustomerProfile(profile);
        model.setEnabled(false);
        model.setContact(contact);
        model.setRegisteredStore(preferredStoreCode);

        return model;
    }

    /**
     * Returns the firstname + lastname if either of the fields are present. Else, returns the username.
     *
     * @return fullname which could either be firstname + lastname or username
     */
    public String getFullName() {
        String fullName = FormatterUtil.INSTANCE.formatName(lastName, firstName);
        if (!StringUtils.hasText(fullName)) {
            // If no firstname and lastname, use the username
            fullName = username;
        }
        return fullName;
    }
}
