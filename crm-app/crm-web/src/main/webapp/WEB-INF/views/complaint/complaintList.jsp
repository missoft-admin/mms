<%@include file="../common/taglibs.jsp" %>

<style type="text/css"> 
<!-- 
  .table tbody tr td:last-child, .table thead tr th:last-child {
      min-width: 144px;
  }
  #content_complaintList .dataTable tr td:nth-child(1), #list_employee .dataTable tr th:nth-child(1) {
    min-width: 78px;
  }
  #content_complaintList .dataTable tr td:nth-child(2), #list_employee .dataTable tr th:nth-child(2) {
    min-width: 96px;
  }
  #content_complaintList .dataTable tr td:nth-child(3), #list_employee .dataTable tr th:nth-child(3) {
    min-width: 192px;
  }
  #content_complaintList .dataTable tr td:nth-child(4), #list_employee .dataTable tr th:nth-child(4) {
    min-width: 160px;
  }
  #content_complaintList .dataTable tr td:nth-child(5), #list_employee .dataTable tr th:nth-child(5) {
    min-width: 152px;
  }  
  #content_complaintList .dataTable tr td:nth-child(6), #list_employee .dataTable tr th:nth-child(6) {
    min-width: 290px;
  }
  #content_complaintList .dataTable tr td:nth-child(7), #list_employee .dataTable tr th:nth-child(7) {
    min-width: 64px;
  }
  #content_complaintList .dataTable tr td:nth-child(8), #list_employee .dataTable tr th:nth-child(8) {
    min-width: 94px;
  }  

--> 
</style>


<div id="content_complaintList">

  <div id="list_complaint" 
      data-url="<c:url value="${listUrl}" />" 
      data-hdr-ticketno="<spring:message code="complaint_ticketno"/>" 
      data-hdr-date="<spring:message code="complaint_lbl_datefiled"/>" 
      data-hdr-store="<spring:message code="complaint_store"/>" 
      data-hdr-firstname="<spring:message code="complaint_lbl_name"/>" 
      data-hdr-gendercode="<spring:message code="complaint_gender"/>" 
      data-hdr-contact="<spring:message code="complaint_lbl_retcontact"/>" 
      data-hdr-filedby="<spring:message code="complaint_lbl_filedby"/>" 
      data-hdr-assignedto="<spring:message code="complaint_assignedto"/>" 
      data-hdr-complaints="<spring:message code="complaint_complaints"/>" 
      data-hdr-priority="<spring:message code="complaint_priority"/>" 
      data-hdr-status="<spring:message code="complaint_status"/>" ></div>

  <div class="btns hide">
    <a href="<c:url value="/customer/complaint/form/view/%ID%" />" class="btn pull-left tiptip icn-view viewBtn" 
      title="<spring:message code="label_view" />" ><spring:message code="label_view" /></a>
    <a href="<c:url value="/customer/complaint/form/edit/%ID%" />" class="btn pull-left tiptip icn-edit editBtn" 
      title="<spring:message code="label_edit" />" ><spring:message code="label_edit" /></a>
    <a href="<c:url value="/customer/complaint/form/assign/%ID%" />" class="btn pull-left tiptip icn-edit assignBtn" 
      title="<spring:message code="label_assign" />" ><spring:message code="label_assign" /></a>
    <a href="<c:url value="/customer/complaint/form/process/%ID%" />" class="btn pull-left tiptip icn-edit processBtn" 
      title="<spring:message code="label_process" />" ><spring:message code="label_process" /></a>
    <a href="<c:url value="/customer/complaint/form/reply/%ID%" />" class="btn pull-left tiptip icn-edit replyBtn" 
      title="<spring:message code="label_reply" />" ><spring:message code="label_reply" /></a>
  </div>

  <div id="constants" class="hide" 
      data-stat-new="${statNew}" data-statdisp-new="<spring:message code="label_new_" />"
      data-is-css="${isCss}" ></div>
  <div id="form_complaint"></div>


<link href="<spring:url value="/css/dataTables.css" />" rel="stylesheet">
<script src="<spring:url value="/js/jquery/jquery.dataTables.js" />" ></script>
<script src="<spring:url value="/js/jquery/jquery.dataTables.linkLengthMenu.js" />"></script>
<script src="<spring:url value="/js/jquery/jquery.dataTables.customPaging.js" />"></script>
<script src="<spring:url value="/js/jquery/ajax-datatable.js" />" ></script>
<script src="<spring:url value="/js/common/form2js.js" />" ></script>
<script src="<spring:url value="/js/common/json2.js" />" ></script>
<script src="<spring:url value="/js/jquery/jquery.toObject.js" />" ></script>

<script type="text/javascript">
var ComplaintList = null;

$(document).ready( function() {

    var $container = $( "#content_complaintList" );
    var $btns = $container.find( ".btns" );
    var $list = $container.find( "#list_complaint" );

    var $constants = $container.find( "#constants" );
    var NEW = { val: $constants.data( "stat-new" ), disp: $constants.data( "statdisp-new" ) };
  
    initDataTable();
  
    function initDataTable() {
        $list.ajaxDataTable({
            'autoload'    : true,
            'ajaxSource'  : $list.data( "url" ),
            'columnHeaders' : [ 
                $list.data( "hdr-ticketno" ),
                $list.data( "hdr-date" ),
                $list.data( "hdr-store" ),
                $list.data( "hdr-firstname" ),
                $list.data( "hdr-gendercode" ),
                $list.data( "hdr-contact" ),
                $list.data( "hdr-filedby" ),
                $list.data( "hdr-assignedto" ),
                $list.data( "hdr-priority" ),
                $list.data( "hdr-status" ),
                { text: "", className : "span5" }
            ],
          aaSorting:[[ 1, 'desc' ]],
            'modelFields'  : [
                { name  : 'ticketNo', sortable : true },
                { name  : 'dateFiled', sortable : true, fieldCellRenderer : function (data, type, row) {
                    return (row.dateTime)? new Date( row.dateTime - 0 ).customize( 1 ) : "";
                }},
                { name  : 'storeCode', sortable : true, fieldCellRenderer : function (data, type, row) {
                    return row.storeCode + " - " + row.storeDesc;
                }},
                { name  : 'firstName', sortable : true, fieldCellRenderer : function (data, type, row) {
                    return row.firstName + " " + (row.lastName ? row.lastName : "");
                }},
                { name  : 'genderCode', sortable : true, fieldCellRenderer : function (data, type, row) {
                    return (row.genderCode)? row.genderDesc : "";
                }},
                { name  : 'mobileNo', sortable : true, fieldCellRenderer : function (data, type, row) {
                    return ( (row.email)? row.email : "" ) + ( (row.email && row.mobileNo)? " / " : "" ) + ( (row.mobileNo)? row.mobileNo : "" );
                }},
                { name  : 'createUser', sortable : true },
                { name  : 'assignedTo', sortable : true },
                { name  : 'priority', sortable : true },
                { name  : 'status', sortable : true, fieldCellRenderer : function (data, type, row) {
                    return "<span class='text-uppercase'>" + ( row.status? row.dispStatus : "" ) + "</span>";
                }},
                { customCell : 
                    function ( innerData, sSpecific, json ) {
                        var btns = "";
                        if ( sSpecific == 'display' ) {
                            btns += $btns.find( ".viewBtn" ).prop( "outerHTML" ).replace( new RegExp( "%ID%", "g" ), json.id );
                            if ( !json.info.closed && ( json.info.creator || json.info.owner || $constants.data( "is-css" ) ) ) {
                                btns += $btns.find( ".editBtn" ).prop( "outerHTML" ).replace( new RegExp( "%ID%", "g" ), json.id );
                                //btns += $btns.find( ".assignBtn" ).prop( "outerHTML" ).replace( new RegExp( "%ID%", "g" ), json.id );
                                if ( json.info.owner || $constants.data( "is-css" )  ) {
                                    btns += $btns.find( ".processBtn" ).prop( "outerHTML" ).replace( new RegExp( "%ID%", "g" ), json.id );
                                    if ( json.mobileNo || json.email ) {
                                        btns += $btns.find( ".replyBtn" ).prop( "outerHTML" ).replace( new RegExp( "%ID%", "g" ), json.id );
                                    }
                                }
                            }
                        }
                        return btns;
                    }
                }
            ]
        })
        .on( "click", ".viewBtn", view )
        .on( "click", ".editBtn", edit )
        .on( "click", ".assignBtn", assign )
        .on( "click", ".processBtn", process )
        .on( "click", ".replyBtn", reply );
    }

    function edit(e) {
        e.preventDefault();
        $.get( $(this).attr( "href" ), function( resp ) { Complaint.loadForm( resp ); }, "html" );
    }

    function view(e) {
        e.preventDefault();
        $.get( $(this).attr( "href" ), function( resp ) { 
            $container.find( "#form_complaint" ).html( resp );
            ComplaintViewForm.show(); 
        }, "html" );
    }

    function assign(e) {
        e.preventDefault();
        $.get( $(this).attr( "href" ), function( resp ) { 
            $container.find( "#form_complaint" ).html( resp );
            ComplaintAssignForm.show(); 
        }, "html" );
    }

    function process(e) {
        e.preventDefault();
        $.get( $(this).attr( "href" ), function( resp ) {
            $container.find( "#form_complaint" ).html( resp );
            ComplaintProcessForm.show(); 
        }, "html" );
    }

    function reply(e) {
        e.preventDefault();
        $.get( $(this).attr( "href" ), function( resp ) {
            $container.find( "#form_complaint" ).html( resp );
            ComplaintReplyForm.show(); 
        }, "html" );
    }

    function retrieveDispStatus( stat ) {
        switch( stat ) {
            case NEW.val: return NEW.disp.toUpperCase();
            default: return "";
        }
    }

    ComplaintList = {
        reloadTable  : function() { $list.ajaxDataTable( "search" ); },
        filterTable  : function( filterForm, retFunction ) { $list.ajaxDataTable( "search", filterForm, retFunction ); }
    };
});
</script>


</div>