package com.transretail.crm.giftcard.dto;

import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.joda.time.DateTime;
import org.joda.time.LocalDate;
import org.springframework.format.annotation.DateTimeFormat;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.google.common.collect.Lists;
import com.transretail.crm.common.web.converter.LocalDateSerializer;
import com.transretail.crm.giftcard.entity.CardVendor;
import com.transretail.crm.giftcard.entity.GiftCardOrder;
import com.transretail.crm.giftcard.entity.GiftCardOrderItem;
import com.transretail.crm.giftcard.entity.support.GiftCardOrderStatus;

/**
 *
 */
public class GiftCardOrderDto {
	
	@DateTimeFormat(pattern = "dd-MM-yyyy")
	@JsonSerialize(using = LocalDateSerializer.class)
	private LocalDate moDate;
	
	@DateTimeFormat(pattern = "dd-MM-yyyy")
	@JsonSerialize(using = LocalDateSerializer.class)
	private LocalDate poDate;

	private Long id;
	private String poNumber;
	private String moNumber;
    private Long vendorId;
    private String vendorName;
    private List<GiftCardOrderItemDto> items = Lists.newArrayList(new GiftCardOrderItemDto());
    private GiftCardOrderStatus status;
    private Boolean isEgc;
    
    private DateTime lastUpdated;
    
    public GiftCardOrderDto() {}
    
    public GiftCardOrderDto(GiftCardOrder order) {
    	this.id = order.getId();
    	this.poNumber = order.getPoNumber();
    	this.poDate = order.getPoDate();
    	this.moNumber = order.getMoNumber();
    	this.moDate = order.getMoDate();
    	if(order.getVendor() != null) {
    		this.vendorId = order.getVendor().getId();
    		this.vendorName = order.getVendor().getFormalName();
    	}
    	if(CollectionUtils.isNotEmpty(order.getItems())) {
    		this.items = toDto(order.getItems());	
    	}
    	this.status = order.getStatus();
    	this.isEgc = order.getIsEgc();
    }
    
    public GiftCardOrder toModel(GiftCardOrder order) {
    	if(order == null)
    		order = new GiftCardOrder();
    	
    	order.setPoNumber(this.poNumber);
    	order.setPoDate(this.poDate);
    	order.setMoNumber(this.moNumber);
    	order.setMoDate(this.moDate);
    	if ( null != this.vendorId ) {
        	order.setVendor(new CardVendor(this.vendorId));
    	}
    
    	if(CollectionUtils.isNotEmpty(order.getItems())) {
    		order.getItems().clear();
			if(CollectionUtils.isNotEmpty(this.items)) {
				for(GiftCardOrderItemDto dto : this.items) {
	                if (dto != null && dto.getProfileId() != null) {
	                	GiftCardOrderItem item = dto.toModel(new GiftCardOrderItem());
	            		item.setOrder(order);
	                    order.getItems().add(item);
	                }
	            }
			}
		} else {
			order.setItems(toModel(this.items, order));	
		}
    	
    	order.setStatus(this.status);
    	order.setIsEgc( this.isEgc );
    	
    	return order;
    }
    
    private List<GiftCardOrderItem> toModel(List<GiftCardOrderItemDto> dtos, GiftCardOrder order) {
    	List<GiftCardOrderItem> items = Lists.newArrayList();
    	for(GiftCardOrderItemDto dto: dtos) {
    		GiftCardOrderItem item = dto.toModel(new GiftCardOrderItem());
    		item.setOrder(order);
    		items.add(item);
    	}
    	return items;
    }
    
    private List<GiftCardOrderItemDto> toDto(List<GiftCardOrderItem> items) {
    	List<GiftCardOrderItemDto> dtos = Lists.newArrayList();
    	for(GiftCardOrderItem item: items) {
    		dtos.add(new GiftCardOrderItemDto(item));
    	}
    	return dtos;
    }
    
	public String getPoNumber() {
		return poNumber;
	}
	public void setPoNumber(String poNumber) {
		this.poNumber = poNumber;
	}
	public LocalDate getPoDate() {
		return poDate;
	}
	public void setPoDate(LocalDate poDate) {
		this.poDate = poDate;
	}
	public String getMoNumber() {
		return moNumber;
	}
	public void setMoNumber(String moNumber) {
		this.moNumber = moNumber;
	}
	public LocalDate getMoDate() {
		return moDate;
	}
	public void setMoDate(LocalDate moDate) {
		this.moDate = moDate;
	}
	
	public Long getVendorId() {
		return vendorId;
	}

	public void setVendorId(Long vendorId) {
		this.vendorId = vendorId;
	}

	public List<GiftCardOrderItemDto> getItems() {
		return items;
	}

	public void setItems(List<GiftCardOrderItemDto> items) {
		this.items = items;
	}

	public GiftCardOrderStatus getStatus() {
		return status;
	}
	public void setStatus(GiftCardOrderStatus status) {
		this.status = status;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getVendorName() {
		return vendorName;
	}

	public void setVendorName(String vendorName) {
		this.vendorName = vendorName;
	}

	public DateTime getLastUpdated() {
		return lastUpdated;
	}

	public void setLastUpdated(DateTime lastUpdated) {
		this.lastUpdated = lastUpdated;
	}

	public Boolean getIsEgc() {
		return isEgc;
	}

	public void setIsEgc(Boolean isEgc) {
		this.isEgc = isEgc;
	}
    
}
