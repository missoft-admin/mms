<%@ include file="../taglibs.jsp" %>
<!-- just so i can visualize 3 column layout - remove when real layout is applied -->
<style type="text/css">
    #memberPrepaidLeft {
        float: left; 
    }
    #memberPrepaidCenter {
        display: inline-block; 
    }
    #memberPrepaidRight {
        float: right;
    }
</style>
<div id="memberPrepaid">
    <div class="page-header page-header2">
        <h1>Prepaid</h1>
    </div>
    <div class="well tile-stats mb20">
        <div class="icon">
          <i class="fa fa-user"></i>
      
        </div>
        <h2><%--<spring:message code="gc.prepaid.profile" />:--%>Hi <c:out value="${memberInfo.firstName}"/> <c:out value="${memberInfo.lastName}"/>.</h2>
        <%--<h4>Your last visit was 29 Sep 2014.</h4>--%>
        <%--<label><spring:message code="gc.prepaid.email" />:</label>--%>
        <h3><c:out value="${memberInfo.email}"/></h3>
        <%--<h4><spring:message code="gc.prepaid.totalpoints" />:</h4>
        <div class="num" data-delay="0" data-duration="1500" data-postfix="" data-end="83" data-start="0" id="memberBalance"><c:out value="${prepaidInfo.totalBalance}"/></div>--%>
    </div>  
    <div class="row-fluid">
        <div id="cardList">
            <button type="button" class="btn btn-primary btn-small pull-right" id="linkButton" onClick="javascript:showLinkForm()"><spring:message code="gc.prepaid.link.button" /></button>
            <div id="list_cards" class="data-table-01"
                    data-url="<c:url value="/member/prepaid/cardlist" />"
                    data-hdr-cardno="<spring:message code="gc.prepaid.transaction.history.cardno"/>"
                    data-hdr-proddesc="Product Description"
                    data-hdr-prevbal="<spring:message code="gc.prepaid.transaction.history.prevbal"/>"
                    data-hdr-currbal="<spring:message code="gc.prepaid.transaction.history.curramt"/>"
                    data-url-txnlist="<c:url value="/member/prepaid/txnlist" />"
                    data-hdr-action="Action">
            </div>
        </div>
        <%--        <div id="memberPrepaidLeft" class="well span4 tile-stats">
                    <div class="icon">
                        <i class="fa fa-certificate"></i>
                    </div>--%>
            
            <%-- 83 placed as dummytext --%>
            <%--<div class="num" data-delay="0" data-duration="1500" data-postfix="" data-end="83" data-start="0" id="memberBalance">83<c:out value="${prepaidInfo.totalBalance}"/></div>--%>
            <%--<h3><spring:message code="gc.prepaid.totalpoints" /></h3>--%>
            <%--<p>lorem ispum, dolor socsargen.</p>--%>
<%--        </div>--%>
<%--        <div id="memberPrepaidCenter" class="well span4 tile-stats">
            <button type="button" class="btn btn-primary btn-small pull-right" id="linkButton" onClick="javascript:showLinkForm()"><spring:message code="gc.prepaid.link.button" /></button>
            <div class="icon">
                <i class="fa fa-credit-card"></i>
            </div>
            
            <div class="num" data-delay="0" data-duration="1500" data-postfix="" data-end="83" data-start="0" id="memberBalance">0</div>
            <c:forEach var="card" items="${prepaidInfo.giftCards}">
                <div class="num" data-delay="0" data-duration="1500" data-postfix="" data-end="83" data-start="0" id="memberBalance"><c:out value="${card.barcode}"/></div>
            </c:forEach>
            <h3><spring:message code="gc.prepaid.mycards" /></h3>
            <p>lorem ispum, dolor socsargen.</p>

            
        </div>--%>
<%--        <div id="memberPrepaidRight" class="well span4 tile-stats">
            <button type="button" class="btn btn-primary btn-small pull-right" id="transactionButton"><spring:message code="gc.prepaid.transaction.button" /></button>
            <div class="icon">
                <i class="fa fa-money"></i>
            </div>    
             94 placed as dummytext
            <div class="num" data-delay="0" data-duration="1500" data-postfix="" data-end="83" data-start="0" id="memberBalance">94</div>
            <h3>Transactions</h3>
            <p>lorem ispum, dolor socsargen.</p>
        </div>--%>
    </div>    
</div>

<div class="modal hide nofly" tabindex="-1" role="dialog" aria-hidden="true" id="linkFormDialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4>Link Card to Member</h4>
            </div>
            <div class="modal-body">
                <div id="contentSuccess" class="hide alert alert-success mt0">
                    <div>Gift Card has successfully linked to member.</div>
                </div>
                <div id="contentError" class="hide alert alert-error">
                    <button type="button" class="close" onclick="$(this).parent().hide();">&times;</button>
                    <div><form:errors path="*"/></div>
                </div>
                <form id="prepaidLinkForm" action="<c:url value="/member/prepaid/link" />" class="form-reset">
                    <div class="contentMain">
                        <div class="row-fluid">
                            <div class="span6">
                                <div class="control-group">
                                    <label for="cardNo" class="control-label"><b class="required">*</b>Card No:</label>
                                    <div class="controls">
                                        <input type="text" class="reqdField" name="cardNo"/>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" id="linkSubmit" class="btn btn-primary">Link Card</button>
                <button type="button" id="linkCancel" data-dismiss="modal" class="btn">Cancel</button>
            </div>
        </div>
    </div>
</div>
<link href="<spring:url value="/css/dataTables.css" />" rel="stylesheet">
<script src="<spring:url value="/js/jquery/jquery.dataTables.js" />" ></script>
<script src="<spring:url value="/js/jquery/jquery.dataTables.linkLengthMenu.js" />"></script>
<script src="<spring:url value="/js/jquery/jquery.dataTables.customPaging.js" />"></script>
<script src="<spring:url value="/js/jquery/ajax-datatable.js" />" ></script>
<script src="<spring:url value="/js/common/form2js.js" />" ></script>
<script src="<spring:url value="/js/common/json2.js" />" ></script>
<script src="<spring:url value="/js/jquery/jquery.toObject.js" />" ></script>
<link href="<c:url value="/styles/temp.css" />" rel="stylesheet" />
<script>
    $(document).ready(function() {
        var CONTAINER_ERROR = "#contentError > div";
        var CONTENT_ERROR = "#contentError";

        var balance = $("#memberBalance").text();
        if (balance !== null) {
            $("#memberBalance").text(commaSeparateNumber(balance));
        }
        $("#linkFormDialog").modal({
           show: false,
            keyboard: false,
            backdrop: "static"
        });
        $("#transactionButton").click(function() {
            window.location.href="<c:url value="/member/prepaid/history/" />";
        });

        $("#linkSubmit").click(function(e) {
            e.preventDefault();
            $("#linkSubmit").prop("disabled", true);
            var toSend = $("#prepaidLinkForm").serialize();
            $.post($("#prepaidLinkForm").attr("action"), toSend, function(data) {
                if (data.success) {
                    $("#contentSuccess").show();
                    $("#linkCancel").html("Close");
                    $("#linkCancel").click(function() {
                        location.reload();
                    });
                } else {
                    errorInfo = "";
                    for (i = 0; i < data.result.length; i++) {
                        errorInfo += "<br>" + (i + 1) + ". "
                                + ( data.result[i].code != undefined ?
                                        data.result[i].code : data.result[i]);
                    }
                    $("#linkFormDialog").find(CONTAINER_ERROR).html("Please correct following errors: " + errorInfo);
                    $("#linkFormDialog").find(CONTENT_ERROR).show('slow');

                    $("#linkSubmit").prop("disabled", false);
                }
            });
        });

        //for cards list
        var cardList = $("#list_cards");

        var headers = {
            cardNo: cardList.data("hdrCardno"),
            prodDesc: cardList.data("hdrProddesc"),
            prevBal: cardList.data("hdrPrevbal"),
            currBal: cardList.data("hdrCurrbal"),
            txnList: cardList.data("hdrAction")
        };

        var columnHeaders=[
            headers['cardNo'],
            headers['prodDesc'],
            headers['prevBal'],
            headers['currBal'],
            headers['txnList']
        ];

        var modelFields= [
            {name : 'cardNo', sortable: true},
            {name : 'description'},
            {name : 'previousBalance', fieldCellRenderer : function(data, type, row) {
                return commaSeparateNumber(row.previousBalance);
            }},
            {name : 'currentBalance', fieldCellRenderer : function(data, type, row) {
                return commaSeparateNumber(row.currentBalance);
            }},
            {customCell : function(innerData, sSpecific, json) {
                return "<a class='btn pull-left icn-view tiptip' title='View Transaction History' onclick=\"javascript: viewTxnHistory('" + json.cardNo + "');\" href='#'>" + "</a>";
            }}
        ];

        cardList.ajaxDataTable({
           'autoload' : false,
           'ajaxSource' : cardList.data("url"),
           'columnHeaders' : columnHeaders,
           'modelFields' : modelFields
        });

        var searchDto = new Object();

        cardList.ajaxDataTable('search', searchDto);
    });
    function showLinkForm() {
        $modal = $("#linkFormDialog");
        $modal.find("#contentSuccess").hide();
        $modal.modal("show");
    }
    function commaSeparateNumber(val) {
        while (/(\d+)(\d{3})/.test(val.toString())){
            val = val.toString().replace(/(\d+)(\d{3})/, '$1'+','+'$2');
        }
        return val;
    }
    function viewTxnHistory(cardNo) {
        window.location.href="<c:url value="/member/prepaid/history/" />" + cardNo;
    }
</script>
<style>
    table td:nth-child(3).dataTables_align_left {
        border-right: 1px solid #f1f1f1;
        text-align: right;
    }
    table td:nth-child(4).dataTables_align_left {
        text-align: right;
    }
</style>