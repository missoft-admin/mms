<%@ include file="../../common/taglibs.jsp" %>


<style type="text/css">
<!--
  #addProductBtn.btn-small {
    margin-bottom: 5px;
  }
  
  #productTable tr td:last-child, #detailsTable tr td:last-child, #receivedTable tr td:last-child  {
    border-left: 0px;
  }
-->
</style>

<c:set var="ENCTYPE" value="application/x-www-form-urlencoded"/>

<div class="modal-dialog">
  <div class="modal-content">
  
    <div class="modal-header">
      <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
      <h4><spring:message code="receive_order"/></h4>
    </div>
 
    <div class="modal-body">
    
      <form:form id="receiveForm" name="receiveForm" modelAttribute="receiveForm" data-print-url="${pageContext.request.contextPath}/giftcard/order/printreceive/" action="${pageContext.request.contextPath}/giftcard/order/receiveform" method="POST" enctype="${ENCTYPE}" >
      
      <input type="hidden" name="id" value="${orderForm.id}" />
      
      <div id="contentError" class="hide alert alert-error">
        <button type="button" class="close" onclick="$(this).parent().hide();">&times;</button>
        <div><form:errors path="*"/></div>
      </div>
      
      <div class="contentMain">
      <div class="well">
        <div class="row-fluid">
          <div class="span4">
            <label for=""><spring:message code="order_po" /></label>
            <input type="text" value="${orderForm.poNumber} / ${orderForm.poDate}" disabled="disabled" />
          </div>
          
          <div class="span4">
            <label for=""><spring:message code="order_mo" /></label>
            <input type="text" value="${orderForm.moNumber} / ${orderForm.moDate}" disabled="disabled" />
          </div>
        
          <div class="span4">
            <spring:message code="order_supplier" var="supplier" />
            <label for="">${supplier}</label>
            <input type="text" value="${orderForm.vendorName}" disabled="disabled" />
          </div>
          
        </div>
        
        <div style="margin-top: 10px; margin-bottom: 10px;">
          <table id="detailsTable" class="table table-condensed table-compressed table-striped">
            <thead>
              <tr>
                <th><spring:message code="profile_product_desc" /></th>
                <th><spring:message code="order_item_starting_series" /></th>
                <th><spring:message code="order_item_ending_series" /></th>
                <th><spring:message code="order_ordered" /></th>
                <th><spring:message code="order_served" /></th>
              </tr>
            </thead>
            <tbody>
              <c:forEach var="item" items="${receiveForm.series}" varStatus="status" >
              <tr>
                <td>${item.profile}</td>
                <td>${item.startingSeries}</td>
                <td>${item.endingSeries}</td>
                <td>${item.ordered}</td>
                <td>${item.served}</td>
              </tr>
              </c:forEach>
            </tbody>
          </table>
        </div>
        <c:if test="${not empty receiveForm.receivedSeries}">
        <div style="margin-top: 10px; margin-bottom: 10px;">
          <table id="receivedTable" class="table table-condensed table-compressed table-striped">
            <thead>
              <tr>
                <th><spring:message code="profile_product_desc" /></th>
                <th><spring:message code="order_item_starting_series" /></th>
                <th><spring:message code="order_item_ending_series" /></th>
                <th><spring:message code="order_received" /></th>
                <th><spring:message code="order_delivery_receipt" /></th>
              </tr>
            </thead>
            <tbody>
              <c:forEach var="item" items="${receiveForm.receivedSeries}" varStatus="status" >
              <tr>
                <td>${item.profile}</td>
                <td>${item.startingSeries}</td>
                <td>${item.endingSeries}</td>
                <td>${item.received}</td>
                <td>${item.deliveryReceipt} (${item.receivedDate})</td>
              </tr>
              </c:forEach>
            </tbody>
          </table>
        </div>
        </c:if>
        </div>
        
        <sec:authentication var="userLocationCode" property="principal.inventoryLocation" />
        
        <div class="row-fluid">
          <div class="span4">
            <spring:message code="order_received_at" var="receivedAt" />
            <label for=""><b class="required">*</b> ${receivedAt}</label>
            
            <select name="receivedAt">
            <c:if test="${not empty userLocationCode }">
            <c:if test="${userLocationCode == 'INVT001' }">
            <c:forEach items="${stores}" var="item" >
              <option value="${item.key}">${item.value}</option>
            </c:forEach>
            </c:if>
            <c:if test="${userLocationCode != 'INVT001' }">
              <option value="${userLocationCode}">${stores[userLocationCode]}</option>
            </c:if>
            </c:if>
            </select>
            
          </div>
          
          <div class="span4">
            <spring:message code="order_received_date" var="receiveDate" />
            <label for=""><b class="required">*</b> ${receiveDate}</label>
            <form:input path="receivedDate" data-mo-date="${orderForm.moDate}" placeholder="${receiveDate}" />
          </div>
          
          <div class="span4">
            <spring:message code="order_delivery_receipt" var="receipt" />
            <label for=""><b class="required">*</b> ${receipt}</label>
            <form:input path="deliveryReceipt" placeholder="${receipt}" />
          </div>
        </div>
        
        
        
        <div class="pull-left" style="margin-right: 5px;">        
        <button type="button" id="addProductBtn" class="btn btn-small">+</button><br/>
        <button type="button" id="removeProductBtn" class="btn btn-small">&ndash;</button>
        </div>
        
    
        <div class="pull-left" style="width:90%;">
          <table id="productTable" class="table table-condensed table-compressed table-striped">
            <thead>
              <tr>
                <th></th>
                <th><spring:message code="order_item_quantity" /></th>
                <th><spring:message code="order_item_starting_series" /></th>
                <th><spring:message code="order_item_ending_series" /></th>
                <th><spring:message code="order_item_box_no" /></th>
              </tr>
            </thead>
            <tbody>
              <c:forEach var="item" items="${receiveForm.items}" varStatus="status" >
              <tr data-index="${status.index}">
                <td><input type="checkbox" class="productRemoveFlag" /></td>
                <td><input type="text" class="input-mini modalQuantity intInput" value="${item.quantity}" name="items[${status.index}].quantity" /></td>
                <td><input type="text" class="input-medium modalStartingSeries intInput" value="${item.startingSeries}" name="items[${status.index}].startingSeries" maxlength="16" /></td>
                <td><input type="text" class="input-medium modalEndingSeries intInput" value="${item.endingSeries}" name="items[${status.index}].endingSeries" maxlength="16" /></td>
                <td><input type="text" class="input-mini" value="${item.boxNo}" name="items[${status.index}].boxNo" /></td>
              </tr>
              </c:forEach>
            </tbody>
          </table>
        </div>
      </div>
      </form:form>
    
    </div>
    <div class="modal-footer">
      <button type="button" id="" data-loading-text="<spring:message code="loading_text" />" class="btn btn-primary receiveOrderSave"><spring:message code="label_save" /></button>
      <button type="button" id="" class="btn btn-default" data-dismiss="modal"><spring:message code="label_cancel"/></button>
    </div>
  </div>
</div>


<script>
$(document).ready(function() {
	var CONTAINER_ERROR = "#contentError > div";
	var CONTENT_ERROR = "#contentError";
	$form = $("#receiveForm");
	$orderDialog = $("#orderDialog").css({"width": "60%"});
	
	$("input[name='receivedDate']")
	.datepicker({
		format: 'dd-mm-yyyy',
		autoclose: true
	})
	.datepicker("setEndDate", new Date())
	.datepicker("setStartDate", new Date($("input[name='receivedDate']").data("moDate")))
	.datepicker( "setDate", new Date() );
	
	var $productTable = $('#productTable');
	
	var  $productTbody = $("tbody", $productTable);
	
	$('#removeProductBtn', $orderDialog ).click(function() {
        $("input.productRemoveFlag:checked").each(function() {
            var $tr = $(this).closest("tr");
            var length = $productTbody.find("tr").length;
            if($tr.is(":first-child") && length == 1) {
                $tr.find("input.productRemoveFlag").prop('checked', false);
                $tr.find("input").removeAttr('value').prop('disabled', true);
                $tr.find("select").removeAttr('value').prop('disabled', true);
                $tr.invisible();
            } else
                $tr.remove();
        });
    });

    $('#addProductBtn', $orderDialog ).click(function() {
        var $tr = $productTbody.find("tr:first-child");
        if($tr.css('visibility') == 'hidden' || $tr.css('display') == 'none') {
            $tr.find("input").removeAttr('value').prop('disabled', false);
            $tr.find("select").removeAttr('value').prop('disabled', false);
            $tr.visible();
        } else {
            var $trLast = $productTbody.find("tr:last-child");
            var  $row = $tr.clone();
            $row.find("input").removeAttr('value').prop('disabled', false);
            $row.find("select").removeAttr('value').prop('disabled', false);
            var index = +$trLast.data("index") + 1;
            var lastIndex = $tr.data("index");
            $row.data("index", index);
            var rg = new RegExp("\\["+lastIndex+"\\]", 'g');
            var html = $row.html();
            html = html.replace(rg, '['+index+']');
            $row.html(html);

            $productTbody.append($row);
            initFields($row);
        }
    });
    
    
    function initFields(e) {
    	$('.quantity', $(e)).numberInput( "type", "int" );
        $('.intInput', $(e)).numberInput( "type", "int" );
    	$('.floatInput', $(e)).numberInput( "type", "float" );
    	$('.modalQuantity, .modalStartingSeries', $(e)).on("input", populateEndingSeries);
    }
    
    initFields($orderDialog);
	
	function populateEndingSeries() {
		var startSeries = $(this).closest("tr").find(".modalStartingSeries").val();
		var quantity = $(this).closest("tr").find(".modalQuantity").val();
		if(startSeries != null && quantity != null && startSeries != "" && quantity != "") {
			var temp = +startSeries + +quantity - 1;
			var endingSeries = temp.toString();
			var padZero = 12 - endingSeries.length;
			if(padZero > 0) {
				for(var i = 0; i < padZero; i++)
					endingSeries = "0" + endingSeries;
			}
			$(this).closest("tr").find(".modalEndingSeries").val(endingSeries);
		}
	}
	
	jQuery.fn.visible = function() {
	    return this.css('visibility', 'visible');
	};

	jQuery.fn.invisible = function() {
	    return this.css('visibility', 'hidden');
	};
	
	$(".receiveOrderSave").click(validateForm);
	  
	function validateForm() {
	    $(".receiveOrderSave").prop( "disabled", true );
	    $.post($form.attr("action"), $form.serialize(), function(data) {
	      $(".receiveOrderSave").prop( "disabled", false );
	      if (data.success) {
	    	/* saveStocks(); */
	    	var url = $form.data("printUrl") + "?" + $form.serialize();
			window.open(url, '_blank', 'toolbar=0,location=0,menubar=0');
	        location.reload();
	      } 
	      else {
	        errorInfo = "";
	        for (i = 0; i < data.result.length; i++) {
	          errorInfo += "<br>" + (i + 1) + ". "
	              + ( data.result[i].code != undefined ? 
	                  data.result[i].code : data.result[i]);
	        }
	        $orderDialog.find(CONTAINER_ERROR).html("Please correct following errors: " + errorInfo);
	        $orderDialog.find(CONTENT_ERROR).show('slow');
	      }
	    }); 
	  }
	
	function saveStocks() {
		$.post($form.attr("action") + "/stocks", $form.serialize());
	}
});
</script>