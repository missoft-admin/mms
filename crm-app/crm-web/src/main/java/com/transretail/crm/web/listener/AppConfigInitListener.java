package com.transretail.crm.web.listener;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;

import org.springframework.context.ApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

import com.transretail.crm.schedule.service.AppConfigService;

@WebListener
public class AppConfigInitListener implements ServletContextListener {

	@Override
	public void contextInitialized(ServletContextEvent sce) {
		ApplicationContext ctx = WebApplicationContextUtils.getWebApplicationContext(sce.getServletContext());
		AppConfigService configService = (AppConfigService) ctx.getBean("appConfigService");
		configService.initAppConfigs();
	}

	@Override
	public void contextDestroyed(ServletContextEvent sce) {
	}

}
