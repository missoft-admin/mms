<%@include file="/WEB-INF/views/common/taglibs.jsp" %>
<div class="form-horizontal">
  <div class="page-header page-header2"><h1><spring:message code="report.card.usage"/></h1></div>
  
  <div class="errorMessages error">&nbsp;</div>

  <div class="control-group">
    <label class="control-label"></label>
    <div class="controls">
      <select name="reportType" id="reportType">
        <c:forEach items="${reportTypes}" var="reportType">
          <option value="${reportType.code}"><spring:message code="report_type_${reportType.code}" /></option>
        </c:forEach>
      </select>
    </div>
  </div>
  

  <fieldset>
    <legend><spring:message code="report.filters.label"/></legend>
    <form id="filters">
      <div class="control-group">
        <label class="control-label"><spring:message code="report.period" /></label>
        <div class="controls">
          <select name="standardReportType" id="standardReportType">
            <option value="weekly"><spring:message code="report.card.usage.weekly"/></option>
            <option value="monthly"><spring:message code="report.card.usage.monthly"/></option>
          </select>
        </div>
      </div>
      
      <div class="control-group store">
        <label class="control-label"><spring:message code="report.store" /></label>
        <div class="controls">
          <select name="store" id="store" class="searchDropdown">
            <option value=""></option>
            <c:forEach items="${stores}" var="store">
              <option value="${store.code}">${store.code} - ${store.name}</option>
            </c:forEach>
          </select>
        </div>
      </div>
      
      <div class="control-group">
        <label class="control-label"><span class="required">*</span> <spring:message code="label_year" /></label>
        <div class="controls">
          <input name="year" id="year" type="text" placeHolder="<spring:message code="label_year" />" />
        </div>
      </div>
      
      <div class="control-group week">
        <label class="control-label"><spring:message code="label_week" /></label>
        <div class="controls">
          <select name="week" id="week" class="input-small">
            <c:forEach var="weekNo" begin="1" end="44">
              <option value="${weekNo}">${weekNo} - ${weekNo + 8}</option>
            </c:forEach>
          </select>
        </div>
      </div>
      
      <div class="control-group week">
        <label class="control-label"><spring:message code="label_start_week" /></label>
        <div class="controls">
          <select name="startWeek" id="startWeek">
            <option value="1">Sunday</option>
            <option value="2">Monday</option>
          </select>
        </div>
      </div>
      
      <div class="control-group month hide">
        <label class="control-label"><spring:message code="label_month" /></label>
        <div class="controls">
          <select name="month" id="month">
            <option value="1"><spring:message code="report.january"/> - <spring:message code="report.september"/></option>
            <option value="2"><spring:message code="report.february"/> - <spring:message code="report.october"/></option>
            <option value="3"><spring:message code="report.march"/> - <spring:message code="report.november"/></option>
            <option value="4"><spring:message code="report.april"/> - <spring:message code="report.december"/></option>
          </select>
        </div>
      </div>
      
    </form>
    <div class="control-group form-actions">
        <button id="view" type="button" data-loading-text="Loading..." class="btn btn-primary"><spring:message code="view"/></button>
    </div>
  </fieldset>

<jsp:include page="../../common/noDataFound.jsp"/>

</div>
<style>
  #templateName {
    width: 30%;
  }
</style>
<script src="<c:url value="/js/bootstrap/bootstrap-datepicker.js"/>"></script>
<link href="<c:url value="/css/bootstrap/datepicker.css" />" rel="stylesheet"/>

<script>
$(document).ready(function() {
  var standardReportType = $("#standardReportType");
  var errorMessagesContainer = $('div.errorMessages' );
  
  initFields();
  initButtons();
  
  function initFields() {
    
    standardReportType.change(function(e) {
  	  if (standardReportType.val()==="monthly") {
  	    $(".week").hide();
  	    $(".month").show();
  	    resetFields();
  	  } else {
  	    $(".week").show();
  	    $(".month").hide();
  	    resetFields();
  	  }
    });
    
    $("#year").datepicker({
      autoclose : true,
      format : 'yyyy',
      startView : 2,
      minViewMode : 2
    });
    
  }
  
  function initButtons() {
    $("#view").click(function(e) {
      $("#view").button("loading");
  	  var selectedReportType = "reportType=" + $("select[name='reportType']").val();
      var standardReportType = "&standardReportType=" + $("#standardReportType").val();
  	  var year = "&year=" + $("#year").val();
  	  var week = "&week=" + $("#week").val();
  	  var startWeek = "&startWeek=" + $("#startWeek").val();
  	  var month = "&month=" + $("#month").val();
  	  var store = "&store=" + $("#store").val();
  	  
  	  var url = ctx + "/member/cardusage/print?"
  	      		+ selectedReportType
          		+ standardReportType
          		+ year;
  	  
  	  if ($("#standardReportType").val() == "weekly") {
    	  if ($("#week").val() != "")
    	    url = url + week;
    	  if ($("#startWeek").val() != "")
    	    url = url + startWeek;
  	  } else {
    	  if ($("#month").val() != "")
    	    url = url + month;
  	  }
  	  if ($("#store").val() != "")
  	    url = url + store;
  	  
  	  var validateUrl = ctx + "/member/cardusage/print/validate";
  	  var filters = $("#filters").serialize();
  	  
  	  $.post(validateUrl, filters, function(data) {
  	    if (data.success) {
      	  if ($("select[name='reportType']").val() == "pdf")
            window.open(url, '_blank', 'toolbar=0,location=0,menubar=0');
          else if ($("select[name='reportType']").val() == "excel")
            window.location.href = url;
      	} else {
      	  if (data.result[0] === "No Data Found") {
      	    $("#noData").modal('show');
      	  } else {
        	  var errorInfo = "1. " + data.result[0];
            for ( var i = 1; i < data.result.length; i++ ) {
                errorInfo += "<br>" + (i + 1) + ". " + data.result[i];
            }
            showErrorMessage(errorInfo);
      	  }
      	}
  	    $("#view").button("reset");
  	  });
    });
  }
    
  var showErrorMessage = function(message) {
    errorMessagesContainer.html('<div class="alert alert-error">' +
        '<button type="button" class="close" data-dismiss="alert">&times;</button>' +
        message + '</div>');
  };

  function resetFields() {
    $("#month").val($("#month option:first").val());
    $("#week").val($("#week option:first").val());
  }
  
});
</script>