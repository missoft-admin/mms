package com.transretail.crm.core.service;


import java.util.List;

import com.transretail.crm.common.service.dto.response.ResultList;
import com.transretail.crm.core.dto.PsoftStoreDto;
import com.transretail.crm.core.dto.PsoftStoreSearchDto;
import com.transretail.crm.core.dto.StoreDto;
import com.transretail.crm.core.entity.PeopleSoftStore;
import java.util.Map;


public interface PsoftStoreService {

	ResultList<PsoftStoreDto> search(PsoftStoreSearchDto dto);

	PsoftStoreDto save(PsoftStoreDto dto);

	PeopleSoftStore get(Long id);

	PsoftStoreDto getDto(Long id);

	void delete(Long id);

	PeopleSoftStore getStoreMapping(String storeCode);

	List<StoreDto> getGcAllowedStores();

	List<StoreDto> getGcAllowedStores(PsoftStoreSearchDto dto);

	List<StoreDto> getUnmappedStores();

	boolean isStoreAllowedForGcTransaction(String storeCode);
        
	Map<String, String> getAllRegion();

}
