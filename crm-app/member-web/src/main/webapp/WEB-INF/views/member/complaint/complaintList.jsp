<%@include file="../../taglibs.jsp" %>


<div id="content_complaintList">

  <div id="list_complaint" 
      data-url="<c:url value="/member/complaint/list" />" 
      data-hdr-ticketno="<spring:message code="complaint_ticketno"/>" 
      data-hdr-date="<spring:message code="complaint_lbl_datefiled"/>" 
      data-hdr-store="<spring:message code="complaint_store"/>" 
      data-hdr-complaints="<spring:message code="complaint_complaints"/>" 
      data-hdr-status="<spring:message code="complaint_status"/>" ></div>

  <div class="btns hide">
    <a href="<c:url value="/customer/complaint/form/view/%ID%" />" class="btn pull-left tiptip icn-view viewBtn" 
      title="<spring:message code="label_view" />" ><spring:message code="label_view" /></a>
  </div>

  <div id="constants" class="hide" 
      data-stat-new="${statNew}" data-statdisp-new="<spring:message code="label_new_" />" ></div>
  <div id="form_complaint"></div>


<link href="<spring:url value="/css/dataTables.css" />" rel="stylesheet">
<script src="<spring:url value="/js/jquery/jquery.dataTables.js" />" ></script>
<script src="<spring:url value="/js/jquery/jquery.dataTables.linkLengthMenu.js" />"></script>
<script src="<spring:url value="/js/jquery/jquery.dataTables.customPaging.js" />"></script>
<script src="<spring:url value="/js/jquery/ajax-datatable.js" />" ></script>
<script src="<spring:url value="/js/common/form2js.js" />" ></script>
<script src="<spring:url value="/js/common/json2.js" />" ></script>
<script src="<spring:url value="/js/jquery/jquery.toObject.js" />" ></script>

<script type="text/javascript">
var ComplaintList = null;

$(document).ready( function() {

    var $container = $( "#content_complaintList" );
    var $btns = $container.find( ".btns" );
    var $list = $container.find( "#list_complaint" );

    var $constants = $container.find( "#constants" );
    var NEW = { val: $constants.data( "stat-new" ), disp: $constants.data( "statdisp-new" ) };
  
    initDataTable();
  
    function initDataTable() {
        $list.ajaxDataTable({
            'autoload'    : true,
            'ajaxSource'  : $list.data( "url" ),
            'columnHeaders' : [
                $list.data( "hdr-status" ),
                $list.data( "hdr-date" ),
                $list.data( "hdr-ticketno" ) + " - " + $list.data( "hdr-store" )/* ,
                { text: "", className : "span5" } */
            ],
            'modelFields'  : [
                { name  : 'status', sortable : true, fieldCellRenderer : function (data, type, row) {
                    return ( retrieveDispStatus(row.status)? retrieveDispStatus(row.status) : row.status );
                }},
                { name  : 'dateFiled', sortable : true, fieldCellRenderer : function (data, type, row) {
                    return (row.dateTime)? new Date( row.dateTime - 0 ).customize( 1 ) : "";
                }},
                { name  : 'ticketNo', sortable : true, fieldCellRenderer : function (data, type, row) {
                    return row.ticketNo + ( row.storeCode? " - " + row.storeCode + " (" + row.storeDesc + ")" : "" );
                }}/* ,
                { customCell : 
                    function ( innerData, sSpecific, json ) {
                        var btns = "";
                        if ( sSpecific == 'display' ) {
                            btns += $btns.find( ".viewBtn" ).prop( "outerHTML" ).replace( new RegExp( "%ID%", "g" ), json.id );
                        }
                        return btns;
                    }
                } */
            ]
        })
        .on( "click", ".viewBtn", view );
    }

    function view(e) {
        e.preventDefault();
        $.get( $(this).attr( "href" ), function( resp ) { 
            $container.find( "#form_complaint" ).html( resp );
            ComplaintViewForm.show(); 
        }, "html" );
    }

    function retrieveDispStatus( stat ) {
        switch( stat ) {
            case NEW.val: return NEW.disp.toUpperCase();
            default: return "";
        }
    }

    ComplaintList = {
        reloadTable  : function() { $list.ajaxDataTable( "search" ); }
    };
});
</script>


</div>