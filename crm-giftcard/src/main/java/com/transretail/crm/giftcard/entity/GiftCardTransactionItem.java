package com.transretail.crm.giftcard.entity;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

import java.io.Serializable;
import javax.persistence.*;

/**
 *
 * @author Monte Cillo Co (mco@exist.com)
 */
@Entity
@Table(name = "CRM_GC_TRANSACTION_ITEM")
public class GiftCardTransactionItem implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    @ManyToOne(cascade = CascadeType.ALL)
    private GiftCardTransaction transaction;
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "CARD_NO", referencedColumnName = "BARCODE", nullable = false, updatable = false)
    private GiftCardInventory giftCard;
    @Column(name = "CURR_AMOUNT")
    private double currentAmount;
    @Column(name = "TXN_AMOUNT")
    private double transactionAmount;

    public GiftCardTransactionItem transaction(GiftCardTransaction transaction) {
	this.transaction = transaction;
	return this;
    }

    public GiftCardTransactionItem giftCard(GiftCardInventory giftCard) {
	this.giftCard = giftCard;
	return this;
    }

    public GiftCardTransactionItem currentAmount(double currentAmount) {
	this.currentAmount = currentAmount;
	return this;
    }

    public GiftCardTransactionItem transactionAmount(double transactionAmount) {
	this.transactionAmount = transactionAmount;
	return this;
    }

    public Long getId() {
	return id;
    }

    public void setId(Long id) {
	this.id = id;
    }

    public GiftCardTransaction getTransaction() {
	return transaction;
    }

    public void setTransaction(GiftCardTransaction transaction) {
	this.transaction = transaction;
    }

    public GiftCardInventory getGiftCard() {
	return giftCard;
    }

    public void setGiftCard(GiftCardInventory giftCard) {
	this.giftCard = giftCard;
    }

    public double getCurrentAmount() {
	return currentAmount;
    }

    public void setCurrentAmount(double currentAmount) {
	this.currentAmount = currentAmount;
    }

    public double getTransactionAmount() {
	return transactionAmount;
    }

    public void setTransactionAmount(double txnAmount) {
	this.transactionAmount = txnAmount;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;

        if (o == null || getClass() != o.getClass()) return false;

        GiftCardTransactionItem that = (GiftCardTransactionItem) o;

        return new EqualsBuilder()
                .append(id, that.id)
                .isEquals();
    }

    @Override
    public int hashCode() {

        int hashcode = 17;

        hashcode += (null == id) ? 0 : id.hashCode() * 31;

        return hashcode;
    }
}
