package com.transretail.crm.giftcard.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.Parameter;
import org.hibernate.annotations.Type;
import org.joda.time.DateTime;
import org.springframework.data.annotation.CreatedBy;
import org.springframework.data.annotation.CreatedDate;

import com.transretail.crm.giftcard.entity.support.SalesOrderStatus;

/**
 * @author Khairullah
 * f7urryy@gmail.com
 */

@Entity
@Table(name = "CRM_SALES_ORD_HIST")
public class SalesOrderHistory implements Serializable {

	private static final long serialVersionUID = 6639678784735620573L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "ID")
	private Long id;

	@Column(name = "CREATED_DATETIME")
	@CreatedDate
	@Type(type = "org.jadira.usertype.dateandtime.joda.PersistentDateTime", parameters = { @Parameter(name = "databaseZone", value = "jvm") })
	private DateTime created;

	@Column(name = "CREATED_BY")
	@CreatedBy
	private String createUser;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "ORDER_NO", nullable = false)
	private SalesOrder order;

	@Column(name = "STATUS", nullable = false, length = 15)
	@Enumerated(EnumType.STRING)
	private SalesOrderStatus status;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public DateTime getCreated() {
		return created;
	}
	public Date getCreatedDate(){
		return created.toDate();
	}
	public void setCreated(DateTime created) {
		this.created = created;
	}

	public String getCreateUser() {
		return createUser;
	}

	public void setCreateUser(String createUser) {
		this.createUser = createUser;
	}

	public SalesOrder getOrder() {
		return order;
	}

	public void setOrder(SalesOrder order) {
		this.order = order;
	}

	public SalesOrderStatus getStatus() {
		return status;
	}

	public void setStatus(SalesOrderStatus status) {
		this.status = status;
	}

}
