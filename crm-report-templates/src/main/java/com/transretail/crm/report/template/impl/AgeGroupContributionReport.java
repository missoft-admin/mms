package com.transretail.crm.report.template.impl;

import java.util.Date;
import java.util.Map;
import java.util.Set;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import net.sf.jasperreports.engine.JRDataSource;

import org.apache.commons.lang3.StringUtils;
import org.joda.time.LocalDate;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Service;

import com.google.common.collect.Maps;
import com.google.common.collect.Sets;
import com.mysema.query.BooleanBuilder;
import com.mysema.query.jpa.JPASubQuery;
import com.mysema.query.jpa.impl.JPAQuery;
import com.mysema.query.support.Expressions;
import com.mysema.query.types.Projections;
import com.mysema.query.types.QBean;
import com.mysema.query.types.expr.CaseBuilder;
import com.mysema.query.types.expr.NumberExpression;
import com.mysema.query.types.path.DateTimePath;
import com.mysema.query.types.template.NumberTemplate;
import com.transretail.crm.common.reporting.engine.data.JRQueryDSLDataSource;
import com.transretail.crm.common.reporting.jasper.ClasspathFileResolver;
import com.transretail.crm.common.reporting.jasper.DefaultJRProcessor;
import com.transretail.crm.common.reporting.jasper.JRProcessor;
import com.transretail.crm.common.util.DateUtil;
import com.transretail.crm.core.entity.QMemberModel;
import com.transretail.crm.core.entity.QPointsTxnModel;
import com.transretail.crm.core.entity.enums.ReportType;
import com.transretail.crm.core.entity.lookup.QLookupDetail;
import com.transretail.crm.core.entity.lookup.QStore;
import com.transretail.crm.core.jpa.DbMetadataUtil;
import com.transretail.crm.core.jpa.DbMetadataUtil.DbType;
import com.transretail.crm.core.security.util.UserUtil;
import com.transretail.crm.report.template.CommonReportFilter;
import com.transretail.crm.report.template.FilterField;
import com.transretail.crm.report.template.NoDataFoundSupport;
import com.transretail.crm.report.template.ReportTemplate;

@Service("ageGroupContributionReport")
public class AgeGroupContributionReport  implements ReportTemplate, NoDataFoundSupport {
	protected static final String TEMPLATE_NAME = "Contribution by Age Group Report";
    protected static final String REPORT_NAME = "reports/age_contribution_report.jasper";
    
    protected static final String FILTER_DATE_TO = CommonReportFilter.DATE_TO;
    protected static final String FILTER_DATE_FROM = CommonReportFilter.DATE_FROM;
    protected static final String FILTER_CUSTOMER_TYPE = "customerType";
    protected static final String FILTER_STORE = "store";
    
    private static final DateTimeFormatter INPUT_DATE_PATTERN = DateTimeFormat.forPattern(DateUtil.SEARCH_DATE_FORMAT);
    
    private static final Logger LOGGER = LoggerFactory.getLogger(AgeGroupContributionReport.class);
    
    @PersistenceContext
    private EntityManager em;
    @Autowired
    private MessageSource messageSource;
    @Autowired
    private DbMetadataUtil dbMetadataUtil;
    
    @Override
    public ReportType[] getReportTypes() {
    	return ReportType.values();
    }
    
    @Override
    public String getName() {
    	return TEMPLATE_NAME;
    }

    @Override
    public String getDescription() {
    	return messageSource.getMessage("age.contribution.report.label", (Object[]) null, LocaleContextHolder.getLocale());
    }
    
    @Override
    public boolean canView(Set<String> permissions) {
    	return true;
    }
    
    @Override
    public Set<FilterField> getFilters() {
    	Set<FilterField> filters = Sets.newLinkedHashSet();
        filters.add(FilterField.createDateField(FILTER_DATE_FROM,
                messageSource.getMessage("label_from", (Object[]) null, LocaleContextHolder.getLocale()),
                DateUtil.SEARCH_DATE_FORMAT, true));
        filters.add(FilterField.createDateField(FILTER_DATE_TO,
                messageSource.getMessage("label_to", (Object[]) null, LocaleContextHolder.getLocale()),
                DateUtil.SEARCH_DATE_FORMAT, true));

        QStore store = QStore.store;
        Map<String, String> stores = Maps.newLinkedHashMap();
        stores.put("", "");
        stores.putAll(new JPAQuery(em).from(store).distinct().orderBy(
                store.code.asc()).map(store.code, store.code.concat(" - ").concat(store.name)));

        filters.add(FilterField.createDropdownField(
                FILTER_STORE, messageSource.getMessage("label_store", (Object[]) null, LocaleContextHolder.getLocale()), stores));

        QLookupDetail lookupDetail = QLookupDetail.lookupDetail;
        Map<String, String> customerTypes = Maps.newLinkedHashMap();
        customerTypes.put("", "");
        customerTypes.putAll(new JPAQuery(em).from(lookupDetail).where(lookupDetail.header.code.eq("MEM007")).map(lookupDetail.code, lookupDetail.description));

        filters.add(FilterField.createDropdownField(
                FILTER_CUSTOMER_TYPE, messageSource.getMessage("label_customertype", (Object[]) null, LocaleContextHolder.getLocale()), customerTypes));
        return filters;
    }
    
    @Override
    public JRProcessor createJrProcessor(Map<String, String> map) {
        QPointsTxnModel qpoints = QPointsTxnModel.pointsTxnModel;
        QMemberModel qmember = QMemberModel.memberModel;
        QStore qstore = QStore.store;

        NumberExpression<Long> one = Expressions.numberTemplate(Long.class, "1");
        NumberExpression<Long> zero = Expressions.numberTemplate(Long.class, "0");

        /*floor(months_between(sysdate, BIRTHDATE)/12)*/
        boolean isH2 = (dbMetadataUtil.getDbType() == DbType.H2);

        DateTimePath<Date> bdate = qmember.customerProfile.birthdate;
        NumberExpression age = null;
        if (isH2) {
            age = NumberTemplate.create(Double.class, "floor(DATEDIFF('MONTH', sysdate, {0})/12)", bdate);
        } else {
            age = NumberTemplate.create(Float.class, "floor(months_between(sysdate, {0})/12)", bdate);
        }

        NumberExpression<Long> cnt0to18 = new CaseBuilder()
                .when(bdate.isNull().or(age.loe(18))).then(one)
                .otherwise(zero);
        NumberExpression<Long> cnt19to22 = new CaseBuilder()
                .when(age.goe(19).and(age.loe(22))).then(one)
                .otherwise(zero);
        NumberExpression<Long> cnt23to28 = new CaseBuilder()
                .when(age.goe(23).and(age.loe(28))).then(one)
                .otherwise(zero);
        NumberExpression<Long> cnt29to35 = new CaseBuilder()
                .when(age.goe(29).and(age.loe(35))).then(one)
                .otherwise(zero);
        NumberExpression<Long> cnt36to40 = new CaseBuilder()
                .when(age.goe(36).and(age.loe(40))).then(one)
                .otherwise(zero);
        NumberExpression<Long> cnt41to45 = new CaseBuilder()
                .when(age.goe(41).and(age.loe(45))).then(one)
                .otherwise(zero);
        NumberExpression<Long> cntGt45 = new CaseBuilder()
                .when(age.gt(45)).then(one)
                .otherwise(zero);
        JPAQuery query = createQuery(map).groupBy(qpoints.storeCode, qstore.name)
                .orderBy(qpoints.storeCode.asc());

        /*private Long cnt0to18;
         private Long cnt19to22;
         private Long cnt23to28;
         private Long cnt29to35;
         private Long cnt36to40;
         private Long cnt41to45;
         private Long cntGt45;*/
        QBean<ReportBean> projection = Projections.bean(ReportBean.class,
                qpoints.storeCode.as("storeCode"),
                qstore.name.as("storeName"),
                cnt0to18.sum().as("cnt0to18"),
                cnt19to22.sum().as("cnt19to22"),
                cnt23to28.sum().as("cnt23to28"),
                cnt29to35.sum().as("cnt29to35"),
                cnt36to40.sum().as("cnt36to40"),
                cnt41to45.sum().as("cnt41to45"),
                cntGt45.sum().as("cntGt45")
        );

        JRDataSource dataSource = new JRQueryDSLDataSource(query, projection, null);

        DefaultJRProcessor jrProcessor = new DefaultJRProcessor(REPORT_NAME);
        jrProcessor.setFileResolver(new ClasspathFileResolver("reports"));
        jrProcessor.addParameter("SUB_DATA_SOURCE", dataSource);
        jrProcessor.addParameter("START_DATE", map.get(FILTER_DATE_FROM));
        jrProcessor.addParameter("END_DATE", map.get(FILTER_DATE_TO));

        String storeCode = map.get(FILTER_STORE);
        String memberType = map.get(FILTER_CUSTOMER_TYPE);
        if (StringUtils.isNotBlank(storeCode)) {
            jrProcessor.addParameter("STORE", new JPAQuery(em).from(qstore).where(qstore.code.eq(storeCode)).singleResult(qstore.code.append(" - ").append(qstore.name)));
        }
        if (StringUtils.isNotBlank(memberType)) {
            QLookupDetail qlookup = QLookupDetail.lookupDetail;
            jrProcessor.addParameter("MEMBER_TYPE", new JPAQuery(em).from(qlookup).where(qlookup.code.eq(memberType)).singleResult(qlookup.code.append(" - ").append(qlookup.description)));
        }

        jrProcessor.addParameter("printedBy", UserUtil.getCurrentUser().getUsername());
        jrProcessor.addParameter("printedDate", new LocalDate().toString("dd-MM-yyyy"));

        return jrProcessor;
    }
    
    
    private JPAQuery createQuery(Map<String, String> filter) {
         QPointsTxnModel qpoints = QPointsTxnModel.pointsTxnModel;
        QMemberModel qmember = QMemberModel.memberModel;
        QStore qstore = QStore.store;

        Date startDate = INPUT_DATE_PATTERN.parseLocalDate(filter.get(FILTER_DATE_FROM)).toDateTimeAtStartOfDay().toDate();
        Date endDate = INPUT_DATE_PATTERN.parseLocalDate(filter.get(FILTER_DATE_TO)).plusDays(1).toDateTimeAtStartOfDay().minusMillis(1).toDate();
        String memberType = filter.get(FILTER_CUSTOMER_TYPE);
        String storeCode = filter.get(FILTER_STORE);

        BooleanBuilder f = new BooleanBuilder();
        f.and(qpoints.storeCode.eq(qstore.code));
        f.and(qpoints.transactionDateTime.goe(startDate));
        f.and(qpoints.transactionDateTime.loe(endDate));
        if (StringUtils.isNotBlank(memberType)) {
            f.and(qmember.memberType.code.eq(memberType));
        }
        if (StringUtils.isNotBlank(storeCode)) {
            f.and(qpoints.storeCode.eq(storeCode));
        }

        QPointsTxnModel qpointsA = new QPointsTxnModel("pointsA");
        f.and(qpoints.id.in(new JPASubQuery().from(qpointsA)
                .where(qpointsA.transactionDateTime.goe(startDate).and(qpointsA.transactionDateTime.loe(endDate)))
                .groupBy(qpointsA.memberModel.id, qpointsA.storeCode).list(qpointsA.id.min())));
        JPAQuery query = new JPAQuery(em).from(qpoints, qstore)
                .leftJoin(qpoints.memberModel, qmember)
                .where(f);

        return query;
    }

    @Override
    public boolean isEmpty(Map<String, String> filter) {
        return !createQuery(filter).exists();
    }
    public static class ReportBean {
    	
    	/*Count transaction of customers of the age group 0-18, 19-22, 22-28, 29-35, 36-40, 41-45, >45*/
    	
    	private String storeCode;
    	private String storeName;
    	private Long cnt0to18;
    	private Long cnt19to22;
    	private Long cnt23to28;
    	private Long cnt29to35;
    	private Long cnt36to40;
    	private Long cnt41to45;
    	private Long cntGt45;
		public String getStoreCode() {
			return storeCode;
		}
		public void setStoreCode(String storeCode) {
			this.storeCode = storeCode;
		}
		public String getStoreName() {
			return storeName;
		}
		public void setStoreName(String storeName) {
			this.storeName = storeName;
		}
		public Long getCnt0to18() {
			return cnt0to18;
		}
		public void setCnt0to18(Long cnt0to18) {
			this.cnt0to18 = cnt0to18;
		}
		public Long getCnt19to22() {
			return cnt19to22;
		}
		public void setCnt19to22(Long cnt19to22) {
			this.cnt19to22 = cnt19to22;
		}
		public Long getCnt23to28() {
			return cnt23to28;
		}
		public void setCnt23to28(Long cnt23to28) {
			this.cnt23to28 = cnt23to28;
		}
		public Long getCnt29to35() {
			return cnt29to35;
		}
		public void setCnt29to35(Long cnt29to35) {
			this.cnt29to35 = cnt29to35;
		}
		
		public Long getCnt36to40() {
			return cnt36to40;
		}
		public void setCnt36to40(Long cnt36to40) {
			this.cnt36to40 = cnt36to40;
		}
		public Long getCnt41to45() {
			return cnt41to45;
		}
		public void setCnt41to45(Long cnt41to45) {
			this.cnt41to45 = cnt41to45;
		}
		public Long getCntGt45() {
			return cntGt45;
		}
		public void setCntGt45(Long cntGt45) {
			this.cntGt45 = cntGt45;
		}
    	
    	
    }


}
