<%@ include file="../../common/taglibs.jsp" %>

<c:set var="ENCTYPE" value="application/x-www-form-urlencoded"/>
<spring:message code="loyalty_annual_scheme" var="itemType" />

<div class="modal-dialog">
  <div class="modal-content">
    
    <div class="modal-header">
      <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
      <h4><spring:message code="global_menu_new" arguments="${itemType}" /></h4>
    </div>
    <div class="modal-body">
    
      <div id="contentError" class="hide alert alert-error">
        <button type="button" class="close" onclick="$(this).parent().hide();">&times;</button>
        <div><form:errors path="*"/></div>
      </div>
    
      <form:form id="scheme" name="scheme" modelAttribute="scheme" action="${pageContext.request.contextPath}/loyaltyannualfee/scheme/form" method="POST" enctype="${ENCTYPE}" cssClass="form-reset form-horizontal" >
      
        <form:hidden path="id"/>
      
        <div class="control-group">
          <spring:message code="loyalty_annual_company" var="company" />
          <label for="company" class="control-label"><b class="required">*</b>${company}</label>
          <div class="controls">
            <form:select path="company" items="${companies}" itemLabel="description" cssClass="reqdField" itemValue="code" />
          </div>
        </div>
        
        <div class="control-group">
          <spring:message code="loyalty_annual_cardtype" var="cardType" />
          <label for="cardType" class="control-label">${cardType}</label> 
          <div class="controls">
            <form:select path="cardType" items="${cardTypes}" itemLabel="description" itemValue="code" />
          </div>
        </div>
        
        <div class="control-group">
          <spring:message code="loyalty_annual_annualfee" var="annualFee" />
          <label for="annualFee" class="control-label"><b class="required">*</b>${annualFee}</label>
          <div class="controls">
            <form:input path="annualFee" cssClass="floatInput reqdField" placeholder="${annualFee}" />
          </div>
        </div>
        
        <div class="control-group">
          <spring:message code="loyalty_annual_replacementfee" var="replacementFee" />
          <label for="replacementFee" class="control-label"><b class="required">*</b>${replacementFee}</label>
          <div class="controls">
            <form:input path="replacementFee" cssClass="floatInput reqdField" placeholder="${replacementFee}" />
          </div>
        </div>
      
      </form:form>
    
    </div>
    <div class="modal-footer">
      <button type="button" id="submitBtn" class="btn btn-primary"><spring:message code="label_save" /></button>
      <button type="button" id="" class="btn btn-default" data-dismiss="modal"><spring:message code="label_cancel"/></button>
    </div>
  </div>
</div>


<script type="text/javascript">

$(document).ready(function() {
	
	$schemeForm = $("#scheme");
	$schemeModal = $("#schemeModal")
	$submitBtn = $("#submitBtn");
	
	var CONTAINER_ERROR = "#contentError > div";
	var CONTENT_ERROR = "#contentError";
	
	$submitBtn.click(submitForm);
	
	function submitForm(e) {
		e.preventDefault();
		$submitBtn.prop( "disabled", true );
		$.post($schemeForm.attr("action"), $schemeForm.serialize(), function(data) {
			$submitBtn.prop( "disabled", false );
			if (data.success) {
				refreshTable();
				$schemeModal.modal("hide");
			} 
			else {
				errorInfo = "";
				for (i = 0; i < data.result.length; i++) {
					errorInfo += "<br>" + (i + 1) + ". "
							+ ( data.result[i].code != undefined ? 
									data.result[i].code : data.result[i]);
				}
				$schemeModal.find(CONTAINER_ERROR).html("Please correct following errors: " + errorInfo);
				$schemeModal.find(CONTENT_ERROR).show('slow');
			}
		});	
	}
	
});
</script>

