
alter table crm_gc_transaction 
  add SALES_TYPE varchar2(255 char);

update crm_gc_transaction 
  set sales_type = (
    select case when so.order_type = 'B2B_SALES' then  'B2B'
      when so.order_type = 'REPLACEMENT' then 'B2B'
      when so.order_type = 'INTERNAL_ORDER' then 'INTERNAL'
      when so.order_type = 'YEARLY_DISCOUNT' then 'REBATE'
      when so.order_type = 'VOUCHER' then 'REBATE'
      else 'UNDEFINED' end as sales_type
    from crm_sales_ord so where so.id = SALES_ORDER_ID )
  where sales_order_id is not null and txn_type in ('ACTIVATION', 'VOID_ACTIVATED');

update crm_gc_transaction  gt set gt.sales_type = (
  select distinct gc.sales_type from crm_gc_transaction_item ti, crm_gc_inventory gc
    where ti.card_no = gc.barcode and ti.transaction_id  = gt.id)
where sales_order_id is null and txn_type in ('ACTIVATION', 'VOID_ACTIVATED');

