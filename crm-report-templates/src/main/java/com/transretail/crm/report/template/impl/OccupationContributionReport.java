package com.transretail.crm.report.template.impl;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.google.common.collect.Sets;
import com.mysema.query.jpa.impl.JPAQuery;
import com.mysema.query.types.Projections;
import com.mysema.query.types.QBean;
import com.mysema.query.types.expr.BooleanExpression;
import com.mysema.query.types.expr.CaseBuilder;
import com.mysema.query.types.expr.NumberExpression;
import com.mysema.query.types.path.StringPath;
import com.transretail.crm.common.reporting.engine.data.JRQueryDSLDataSource;
import com.transretail.crm.common.reporting.jasper.ClasspathFileResolver;
import com.transretail.crm.common.reporting.jasper.DefaultJRProcessor;
import com.transretail.crm.common.reporting.jasper.JRProcessor;
import com.transretail.crm.common.util.DateUtil;
import com.transretail.crm.core.entity.QMemberModel;
import com.transretail.crm.core.entity.QPointsTxnModel;
import com.transretail.crm.core.entity.enums.ReportType;
import com.transretail.crm.core.entity.lookup.QLookupDetail;
import com.transretail.crm.core.entity.lookup.QStore;
import com.transretail.crm.report.template.CommonReportFilter;
import com.transretail.crm.report.template.FilterField;
import com.transretail.crm.report.template.NoDataFoundSupport;
import com.transretail.crm.report.template.ReportTemplate;
import org.apache.commons.lang.StringUtils;
import org.joda.time.LocalDate;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;

/**
 * @author Mike de Guzman
 */
@Service("OccupationContributionReport")
public class OccupationContributionReport implements ReportTemplate, NoDataFoundSupport {

    private static Logger LOG = LoggerFactory.getLogger(OccupationContributionReport.class);

    protected static final String TEMPLATE_NAME = "Member Contribution By Occupation Report";
    protected static final String REPORT_NAME = "reports/member_contribution_by_occupation_report.jasper";

    protected static final String FILTER_TXN_DATE_FROM = CommonReportFilter.DATE_FROM;
    protected static final String FILTER_TXN_DATE_TO = CommonReportFilter.DATE_TO;
    protected static final String FILTER_CUSTOMER_TYPE = "customerType";
    protected static final String FILTER_STORE = "store";

    private static final DateTimeFormatter INPUT_DATE_PATTERN = DateTimeFormat.forPattern("dd-MM-yyyy");

    @PersistenceContext
    private EntityManager em;

    @Autowired
    private MessageSource messageSource;

    @Override
    public String getName() {
        return TEMPLATE_NAME;
    }

    @Override
    public String getDescription() {
        return messageSource.getMessage("occupation.contribution.report.label", (Object[]) null, LocaleContextHolder.getLocale());
    }

    @Override
    public Set<FilterField> getFilters() {
        Set<FilterField> filters = Sets.newLinkedHashSet();
        filters.add(FilterField.createDateField(FILTER_TXN_DATE_FROM,
                messageSource.getMessage("label_from", (Object[]) null, LocaleContextHolder.getLocale()),
                DateUtil.SEARCH_DATE_FORMAT, true));
        filters.add(FilterField.createDateField(FILTER_TXN_DATE_TO,
                messageSource.getMessage("label_to", (Object[]) null, LocaleContextHolder.getLocale()),
                DateUtil.SEARCH_DATE_FORMAT, true));

        QStore store = QStore.store;
        Map<String, String> stores = Maps.newLinkedHashMap();
        stores.put("", "");
        stores.putAll(new JPAQuery(em).from(store).distinct().orderBy(
                store.code.asc()).map(store.code, store.code.concat(" - ").concat(store.name)));

        filters.add(FilterField.createDropdownField(
                FILTER_STORE, messageSource.getMessage("label_store", (Object[]) null, LocaleContextHolder.getLocale()), stores));

        QLookupDetail lookupDetail = QLookupDetail.lookupDetail;
        Map<String, String> customerTypes = Maps.newLinkedHashMap();
        customerTypes.put("", "");
        customerTypes.putAll(new JPAQuery(em).from(lookupDetail).where(lookupDetail.header.code.eq("MEM007")).map(lookupDetail.code, lookupDetail.description));

        filters.add(FilterField.createDropdownField(
                FILTER_CUSTOMER_TYPE, messageSource.getMessage("label_customertype", null, LocaleContextHolder.getLocale()), customerTypes));
        return filters;

    }

    @Override
    public boolean canView(Set<String> permissions) {
        return true;
    }

    @Override
    public JRProcessor createJrProcessor(Map<String, String> map) {
        LocalDate dateFrom = parseDate(map.get(FILTER_TXN_DATE_FROM));
        LocalDate dateTo = parseDate(map.get(FILTER_TXN_DATE_TO));
        DefaultJRProcessor jrProcessor = new DefaultJRProcessor(REPORT_NAME);
        Map<String, Object> reportParams = prepareReportParameters(dateFrom, dateTo);

        // logic to put dataset into reportParams
        QPointsTxnModel points = QPointsTxnModel.pointsTxnModel;
        QMemberModel member = QMemberModel.memberModel;
        QStore store = QStore.store;

        /*
            OCCU001 - PEGAWAI NEGERI = Public employees
            OCCU002 - TENTARA / POLISI = Military/Police
            OCCU003 - PENGUSAHA / WIRASWASTA = Self employed
            OCCU004 - PEGAWAI SWASTA = Private Employees
            OCCU005 - PROFESIONAL
            OCCU006 - PELAJAR = Student
            OCCU007 - IBU RUMAH TANGGA = Housewife
            OCCU008 - PENSIUNAN = Pension
            OCCU009 - LAINNYA = Other
            OCCU010 - Doctor
            OCCU011 - Lawyer
            OCCU012 - Teacher
        */

        Long nullValue = null;
        StringPath occupation = member.customerProfile.occupation.code;
        NumberExpression<Long> studentCount = new CaseBuilder()
                .when(occupation.eq("OCCU006")).then(points.memberModel.id)
                .otherwise(nullValue);
        NumberExpression<Long> publicEmpCount = new CaseBuilder()
                .when(occupation.eq("OCCU001")).then(points.memberModel.id)
                .otherwise(nullValue);
        NumberExpression<Long> privateEmpCount = new CaseBuilder()
                .when(occupation.eq("OCCU004")).then(points.memberModel.id)
                .otherwise(nullValue);
        NumberExpression<Long> selfEmpCount = new CaseBuilder()
                .when(occupation.eq("OCCU003")).then(points.memberModel.id)
                .otherwise(nullValue);
        NumberExpression<Long> houseWifeCount = new CaseBuilder()
                .when(occupation.eq("OCCU007")).then(points.memberModel.id)
                .otherwise(nullValue);
        NumberExpression<Long> militaryCount = new CaseBuilder()
                .when(occupation.eq("OCCU002")).then(points.memberModel.id)
                .otherwise(nullValue);
        NumberExpression<Long> doctorCount = new CaseBuilder()
                .when(occupation.eq("OCCU010")).then(points.memberModel.id)
                .otherwise(nullValue);
        NumberExpression<Long> lawyerCount = new CaseBuilder()
                .when(occupation.eq("OCCU011")).then(points.memberModel.id)
                .otherwise(nullValue);
        NumberExpression<Long> teacherCount = new CaseBuilder()
                .when(occupation.eq("OCCU012")).then(points.memberModel.id)
                .otherwise(nullValue);
        NumberExpression<Long> otherCount = new CaseBuilder()
                .when(occupation.eq("OCCU009")).then(points.memberModel.id)
                .otherwise(nullValue);

        JPAQuery query = createQuery(map)
                .groupBy(points.storeCode, store.name)
                .orderBy(points.storeCode.asc());

        QBean<OccupationContributionReportBean> projection = Projections.bean(OccupationContributionReportBean.class,
                points.storeCode.as("storeCode"),
                store.name.as("storeName"),
                studentCount.countDistinct().as("studentNo"),
                publicEmpCount.countDistinct().as("civilianPublicEmployeeNo"),
                privateEmpCount.countDistinct().as("privateEmployeeNo"),
                selfEmpCount.countDistinct().as("selfEmployedNo"),
                houseWifeCount.countDistinct().as("houseWifeNo"),
                militaryCount.countDistinct().as("militaryNo"),
                doctorCount.countDistinct().as("doctorNo"),
                lawyerCount.countDistinct().as("lawyerNo"),
                teacherCount.countDistinct().as("teacherNo"),
                otherCount.countDistinct().as("otherProfessionNo")
        );

        reportParams.put("SUB_DATA_SOURCE", new JRQueryDSLDataSource(query, projection));
        jrProcessor.addParameters(reportParams);
        jrProcessor.setFileResolver(new ClasspathFileResolver("reports"));
        return jrProcessor;
    }

    @Override
    public ReportType[] getReportTypes() {
        return ReportType.values();
    }

    private LocalDate parseDate(String dateInstance) {
        return INPUT_DATE_PATTERN.parseLocalDate(dateInstance);
    }

    public Map<String, Object> prepareReportParameters(LocalDate dateFrom, LocalDate dateTo) {
        Map<String, Object> reportMap = Maps.newHashMap();
        Locale locale = LocaleContextHolder.getLocale();
        reportMap.put("REPORT_TYPE", messageSource.getMessage("occupation.contribution.report.label", null, locale));
        reportMap.put("ROW_NUMBER", messageSource.getMessage("occupation.contribution.report.rownum", null, locale));
        reportMap.put("STORE_CODE", messageSource.getMessage("occupation.contribution.report.storecode", null, locale));
        reportMap.put("STORE_NAME", messageSource.getMessage("occupation.contribution.report.storename", null, locale));
        reportMap.put("STUDENT_NO", messageSource.getMessage("occupation.contribution.report.studentnum", null, locale));
        reportMap.put("CIVILIAN_PUBLIC_NO", messageSource.getMessage("occupation.contribution.report.civpubempnum", null, locale));
        reportMap.put("PRIVATE_NO", messageSource.getMessage("occupation.contribution.report.privempnum", null, locale));
        reportMap.put("SELF_EMP_NO", messageSource.getMessage("occupation.contribution.report.selfempnum", null, locale));
        reportMap.put("HOUSEWIFE_NO", messageSource.getMessage("occupation.contribution.report.housewifenum", null, locale));
        reportMap.put("MILITARY_NO", messageSource.getMessage("occupation.contribution.report.militarynum", null, locale));
        reportMap.put("DOCTOR_NO", messageSource.getMessage("occupation.contribution.report.meddocnum", null, locale));
        reportMap.put("LAWYER_NO", messageSource.getMessage("occupation.contribution.report.lawyernum", null, locale));
        reportMap.put("TEACHER_NO", messageSource.getMessage("occupation.contribution.report.teachernum", null, locale));
        reportMap.put("OTHER_PROF_NO", messageSource.getMessage("occupation.contribution.report.othernum", null, locale));
        reportMap.put("TOTAL_NO", messageSource.getMessage("occupation.contribution.report.totalnum", null, locale));
        reportMap.put("DATE_FROM", dateFrom.toString(INPUT_DATE_PATTERN));
        reportMap.put("DATE_TO", dateTo.toString(INPUT_DATE_PATTERN));
        return reportMap;
    }

    @Override
    public boolean isEmpty(Map<String, String> map) {
        return !createQuery(map).exists();
    }

    private JPAQuery createQuery(Map<String, String> map) {
        LocalDate dateFrom = parseDate(map.get(FILTER_TXN_DATE_FROM));
        LocalDate dateTo = parseDate(map.get(FILTER_TXN_DATE_TO));
        String storeCode = map.get(FILTER_STORE);
        String customerType = map.get(FILTER_CUSTOMER_TYPE);

        QPointsTxnModel points = QPointsTxnModel.pointsTxnModel;
        QMemberModel member = QMemberModel.memberModel;
        QStore store = QStore.store;

        List<BooleanExpression> exprs = Lists.newArrayList();
        exprs.add(points.transactionDateTime.between(dateFrom.toDate(), dateTo.toDate()));

        if (StringUtils.isNotBlank(storeCode)) {
            exprs.add(points.storeCode.eq(storeCode));
        }

        if (StringUtils.isNotBlank(customerType)) {
            exprs.add(member.memberType.code.eq(customerType));
        }

        JPAQuery query = new JPAQuery(em).from(store, points)
                .leftJoin(points.memberModel, member)
                .where(store.code.isNotNull()
                        .and(store.code.eq(points.storeCode))
                        .and(BooleanExpression.allOf(exprs.toArray(new BooleanExpression[exprs.size()]))));
//                .groupBy(points.storeCode, store.name)
//                .orderBy(points.storeCode.asc());

        return query;

    }

    public static class OccupationContributionReportBean {

        private String storeCode;
        private String storeName;
        private long studentNo;
        private long civilianPublicEmployeeNo;
        private long privateEmployeeNo;
        private long selfEmployedNo;
        private long houseWifeNo;
        private long militaryNo;
        private long doctorNo;
        private long lawyerNo;
        private long teacherNo;
        private long otherProfessionNo;
        private long totalNo;

        public String getStoreCode() {
            return storeCode;
        }

        public void setStoreCode(String storeCode) {
            this.storeCode = storeCode;
        }

        public String getStoreName() {
            return storeName;
        }

        public void setStoreName(String storeName) {
            this.storeName = storeName;
        }

        public long getStudentNo() {
            return studentNo;
        }

        public void setStudentNo(long studentNo) {
            this.studentNo = studentNo;
        }

        public long getCivilianPublicEmployeeNo() {
            return civilianPublicEmployeeNo;
        }

        public void setCivilianPublicEmployeeNo(long civilianPublicEmployeeNo) {
            this.civilianPublicEmployeeNo = civilianPublicEmployeeNo;
        }

        public long getPrivateEmployeeNo() {
            return privateEmployeeNo;
        }

        public void setPrivateEmployeeNo(long privateEmployeeNo) {
            this.privateEmployeeNo = privateEmployeeNo;
        }

        public long getSelfEmployedNo() {
            return selfEmployedNo;
        }

        public void setSelfEmployedNo(long selfEmployedNo) {
            this.selfEmployedNo = selfEmployedNo;
        }

        public long getMilitaryNo() {
            return militaryNo;
        }

        public void setMilitaryNo(long militaryNo) {
            this.militaryNo = militaryNo;
        }

        public long getHouseWifeNo() {
            return houseWifeNo;
        }

        public void setHouseWifeNo(long houseWifeNo) {
            this.houseWifeNo = houseWifeNo;
        }

        public long getDoctorNo() {
            return doctorNo;
        }

        public void setDoctorNo(long doctorNo) {
            this.doctorNo = doctorNo;
        }

        public long getLawyerNo() {
            return lawyerNo;
        }

        public void setLawyerNo(long lawyerNo) {
            this.lawyerNo = lawyerNo;
        }

        public long getOtherProfessionNo() {
            return otherProfessionNo;
        }

        public void setOtherProfessionNo(long otherProfessionNo) {
            this.otherProfessionNo = otherProfessionNo;
        }

        public long getTeacherNo() {
            return teacherNo;
        }

        public void setTeacherNo(long teacherNo) {
            this.teacherNo = teacherNo;
        }

        public long getTotalNo() {
            return this.studentNo + this.civilianPublicEmployeeNo + this.privateEmployeeNo + this.selfEmployedNo + this.houseWifeNo
                    + this.militaryNo + this.doctorNo + this.lawyerNo + this.teacherNo + this.otherProfessionNo;
        }

        public void setTotalNo(long totalNo) {
            this.totalNo = totalNo;
        }

    }
}
