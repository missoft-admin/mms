package com.transretail.crm.web.controller;

import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.argThat;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.reset;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.springframework.http.MediaType.APPLICATION_JSON;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.model;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.view;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import org.hamcrest.core.IsInstanceOf;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentMatcher;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.AnnotationConfigWebContextLoader;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.transretail.crm.common.service.dto.response.NameIdPairDto;
import com.transretail.crm.common.service.dto.response.NameIdPairResultList;
import com.transretail.crm.core.entity.lookup.QStore;
import com.transretail.crm.giftcard.dto.CardVendorDto;
import com.transretail.crm.giftcard.dto.CardVendorResultList;
import com.transretail.crm.giftcard.dto.CardVendorSearchDto;
import com.transretail.crm.giftcard.dto.GiftCardDto;
import com.transretail.crm.giftcard.dto.GiftCardResultList;
import com.transretail.crm.giftcard.dto.GiftCardSearchDTO;
import com.transretail.crm.giftcard.entity.QCardVendor;
import com.transretail.crm.giftcard.entity.QGiftCard;
import com.transretail.crm.giftcard.service.CardVendorService;
import com.transretail.crm.giftcard.service.GiftCardOrderService;
import com.transretail.crm.giftcard.service.GiftCardService;
import com.transretail.crm.giftcard.service.StockRequestService;
import com.transretail.crm.core.service.NameIdPairService;

/**
 *
 */
@RunWith(SpringJUnit4ClassRunner.class)
@WebAppConfiguration
@ContextConfiguration(loader = AnnotationConfigWebContextLoader.class)
public class GiftCardControllerTest extends AbstractMockMvcTest {
    @Configuration
    @EnableWebMvc
    static class Config {
        @Bean
        public GiftCardController giftCardController() {
            return new GiftCardController();
        }

        @Bean
        public MessageSource messageSource() {
            return mock(MessageSource.class);
        }

        @Bean
        public CardVendorService vendorService() {
            return mock(CardVendorService.class);
        }

        @Bean
        public GiftCardService giftCardService() {
            return mock(GiftCardService.class);
        }

        @Bean
        public NameIdPairService nameIdPairService() {
            return mock(NameIdPairService.class);
        }

        @Bean
        public GiftCardOrderService giftCardOrderService() {
            return mock(GiftCardOrderService.class);
        }

        @Bean
        public StockRequestService stockRequestService() {
            return mock(StockRequestService.class);
        }
    }

    @Autowired
    private MessageSource messageSource;
    @Autowired
    private CardVendorService vendorService;
    @Autowired
    private GiftCardService giftCardService;
    @Autowired
    private NameIdPairService nameIdPairService;
    @Autowired
    private GiftCardOrderService giftCardOrderService;

    @Override
    protected void doSetup() throws Exception {
        reset(messageSource);
        reset(vendorService);
        reset(giftCardService);
        reset(nameIdPairService);
        reset(giftCardOrderService);
    }

    @Test
    public void showGiftCardListPageTest() throws Exception {
        QCardVendor qCardVendor = QCardVendor.cardVendor;
        NameIdPairResultList vendors = new NameIdPairResultList(new ArrayList<NameIdPairDto>(), 0, false, false);
        when(nameIdPairService.getNameIdPairs(null, qCardVendor, qCardVendor.id, qCardVendor.formalName, null))
            .thenReturn(vendors);
        mockMvc.perform(get("/giftcard/list"))
            .andExpect(model().attribute("vendors", vendors))
            .andExpect(view().name("giftcard/list"))
            .andExpect(status().isOk());
    }

    @Test
    public void getGiftCardByIdTest() throws Exception {
        Long gcId = 1L;
        GiftCardDto expectedResult = new GiftCardDto();
        expectedResult.setName("name");
        when(giftCardService.getGiftCard(gcId)).thenReturn(expectedResult);

        mockMvc.perform(get("/giftcard/" + gcId).contentType(APPLICATION_JSON))
            .andExpect(status().isOk())
            .andExpect(content().bytes(new ObjectMapper().writeValueAsBytes(expectedResult)));
        verify(giftCardService).getGiftCard(gcId);
    }

    @Test
    public void listGiftCardsTest() throws Exception {
        GiftCardDto dto = new GiftCardDto();
        dto.setName("name");
        GiftCardResultList expectedResult = new GiftCardResultList(Arrays.asList(dto), 1, false, false);
        when(giftCardService.getGiftCards(any(GiftCardSearchDTO.class))).thenReturn(expectedResult);

        ObjectMapper mapper = new ObjectMapper();
        mockMvc
            .perform(post("/giftcard/list").content(mapper.writeValueAsBytes(new GiftCardSearchDTO())).contentType(APPLICATION_JSON))
            .andExpect(content().bytes(mapper.writeValueAsBytes(expectedResult)))
            .andExpect(status().isOk());
        verify(giftCardService).getGiftCards(any(GiftCardSearchDTO.class));
    }

    @Test
    public void doAddGcTest() throws Exception {
        final String name = "name";
        GiftCardDto dto = new GiftCardDto();
        dto.setName(name);
        dto.setGenCode("gencode");
        dto.setVendor(1L);
        dto.setFaceValue(new BigDecimal("10000"));
        dto.setRandomNoDigits(123);
        String successMessages = "success";
        when(messageSource.getMessage(eq("generic.save.successful"), argThat(new ArgumentMatcher<String[]>() {
            @Override
            public boolean matches(Object argument) {
                return ((String[]) argument)[0].equals(name);
            }
        }), any(Locale.class))).thenReturn(
            successMessages);

        Map<String, Object> expectedResult = new HashMap<String, Object>();
        expectedResult.put(GiftCardController.KEY_SUCCESS, successMessages);

        ObjectMapper mapper = new ObjectMapper();
        String content = mockMvc.perform(post("/giftcard/add").content(mapper.writeValueAsBytes(dto)).contentType(APPLICATION_JSON))
            .andExpect(status().isOk()).andReturn().getResponse().getContentAsString();

        Map<String, String> actualResult = mapper.readValue(content,
            new TypeReference<HashMap<String, String>>() {
            });
        assertEquals(expectedResult.get(GiftCardController.KEY_SUCCESS), actualResult.get(GiftCardController.KEY_SUCCESS));

        verify(giftCardService).saveGiftCard(argThat(new ArgumentMatcher<GiftCardDto>() {
            @Override
            public boolean matches(Object argument) {
                return ((GiftCardDto) argument).getName().equals(name);
            }
        }));
    }

    @Test
    public void showVendorListTest() throws Exception {
        mockMvc.perform(get("/giftcard/vendor/list")).andExpect(view().name("giftcard/vendor/list"));
    }

    @Test
    public void listVendorsTest() throws Exception {
        CardVendorDto dto = new CardVendorDto();
        dto.setFormalName("name");
        CardVendorResultList expectedResult = new CardVendorResultList(Arrays.asList(dto), 1, false, false);
        when(vendorService.getCardVendors(any(CardVendorSearchDto.class))).thenReturn(expectedResult);

        ObjectMapper mapper = new ObjectMapper();
        mockMvc
            .perform(post("/giftcard/vendor/list").content(mapper.writeValueAsBytes(new CardVendorSearchDto()))
                .contentType(APPLICATION_JSON))
            .andExpect(content().bytes(mapper.writeValueAsBytes(expectedResult)))
            .andExpect(status().isOk());
        verify(vendorService).getCardVendors(any(CardVendorSearchDto.class));
    }

    @Test
    public void showAddVendorPageTest() throws Exception {
        mockMvc.perform(get("/giftcard/vendor/add")).andExpect(view().name("giftcard/vendor/add"))
            .andExpect(model().attribute(GiftCardController.FORM_ATTR_NAME, new IsInstanceOf(CardVendorDto.class)));
    }

    @Test
    public void doAddVendorTest() throws Exception {
        final String name = "name";
        CardVendorDto dto = new CardVendorDto();
        dto.setFormalName(name);
        dto.setMailAddress("mail");
        dto.setEmailAddress("email");
        dto.setRegion("region");

        String successMessages = "success";
        when(messageSource.getMessage(eq("generic.save.successful"), argThat(new ArgumentMatcher<String[]>() {
            @Override
            public boolean matches(Object argument) {
                return ((String[]) argument)[0].equals(name);
            }
        }), any(Locale.class))).thenReturn(
            successMessages);

        Map<String, Object> expectedResult = new HashMap<String, Object>();
        expectedResult.put(GiftCardController.KEY_SUCCESS, successMessages);

        ObjectMapper mapper = new ObjectMapper();
        String content =
            mockMvc.perform(post("/giftcard/vendor/add").content(mapper.writeValueAsBytes(dto)).contentType(APPLICATION_JSON))
                .andExpect(
                    status().isOk()).andReturn().getResponse().getContentAsString();

        Map<String, String> actualResult = mapper.readValue(content,
            new TypeReference<HashMap<String, String>>() {
            });
        assertEquals(expectedResult.get(GiftCardController.KEY_SUCCESS), actualResult.get(GiftCardController.KEY_SUCCESS));

        verify(vendorService).saveCardVendor(argThat(new ArgumentMatcher<CardVendorDto>() {
            @Override
            public boolean matches(Object argument) {
                return ((CardVendorDto) argument).getFormalName().equals(name);
            }
        }));
    }

    @Test
    public void showStockListTest() throws Exception {
        QGiftCard qGiftCard = QGiftCard.giftCard;
//        QStore qStore = QStore.store;
        NameIdPairResultList gcs = mock(NameIdPairResultList.class);
//        NameIdPairResultList stores = mock(NameIdPairResultList.class);

        when(nameIdPairService.getNameIdPairs(null, qGiftCard, qGiftCard.id, qGiftCard.name, null)).thenReturn(gcs);
//        when(nameIdPairService.getNameIdPairs(null, qStore, qStore.code, qStore.name)).thenReturn(stores);

        mockMvc.perform(get("/giftcard/stock/list")).andExpect(view().name("giftcard/stock/list"))
            .andExpect(model().attribute("gcs", gcs));
//            .andExpect(model().attribute("stores", stores));
    }
}
