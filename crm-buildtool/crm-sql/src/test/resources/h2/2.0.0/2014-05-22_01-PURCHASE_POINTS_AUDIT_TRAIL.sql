create table CRM_EMPLOYEE_PURCHASE_TXN_AUD (
    id varchar(255) not null,
    REV bigint not null,
    REVTYPE tinyint,
    APPROVAL_DATE timestamp,
    APPROVED_BY varchar(255),
    REASON varchar(10000),
    REMARKS varchar(10000),
    STATUS varchar(20),
    TXN_AMOUNT double,
    TXN_DATE timestamp,
    TXN_NO varchar(255),
    TXN_TYPE varchar(255),
    primary key (id, REV)
);

alter table CRM_EMPLOYEE_PURCHASE_TXN_AUD 
    add constraint FK_sh1vebd930hxlqy3r8valatra 
    foreign key (REV) 
    references CRM_REVINFO;