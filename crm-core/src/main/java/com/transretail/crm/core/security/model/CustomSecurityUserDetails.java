/*
 * Copyright (c) 2012.
 * All rights reserved.
 */

package com.transretail.crm.core.security.model;

import java.util.Collection;

import com.transretail.crm.common.security.model.SecurityUserDetails;
import com.transretail.crm.core.entity.UserRoleModel;

/**
 * @author mhua
 */
public interface CustomSecurityUserDetails extends SecurityUserDetails<CustomSecurityUserInfo> {

    Collection<UserRoleModel> getRoles();

    void setRoles(Collection<UserRoleModel> roles);

}
