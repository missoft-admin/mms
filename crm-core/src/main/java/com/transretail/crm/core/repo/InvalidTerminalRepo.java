package com.transretail.crm.core.repo;

import com.transretail.crm.common.repo.CrmQueryDslPredicateExecutor;
import com.transretail.crm.core.entity.InvalidTerminalModel;
import org.springframework.stereotype.Repository;

/**
 *
 * @author Monte Cillo Co (mco@exist.com)
 */
@Repository
public interface InvalidTerminalRepo extends CrmQueryDslPredicateExecutor<InvalidTerminalModel, String>{
}
