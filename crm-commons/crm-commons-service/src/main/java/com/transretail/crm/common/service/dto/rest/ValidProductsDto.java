package com.transretail.crm.common.service.dto.rest;


import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class ValidProductsDto  implements Serializable {


    private Double memberPoints = 0.0;

    private List<ProductRestDto> productRestDtos = new ArrayList<ProductRestDto>();


    public Double getMemberPoints() {
        return memberPoints;
    }

    public void setMemberPoints(Double memberPoints) {
        this.memberPoints = memberPoints;
    }


    public List<ProductRestDto> getProductRestDtos() {
        return productRestDtos;
    }

    public void setProductRestDtos(List<ProductRestDto> productRestDtos) {
        this.productRestDtos = productRestDtos;
    }


}
