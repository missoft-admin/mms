package com.transretail.crm.web.interceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import com.transretail.crm.common.web.controller.support.RequestUtils;

/**
 *
 */
public class PageRequestUrlInterceptor extends HandlerInterceptorAdapter {
    public static final String ALREADY_INTERCEPTED_SUFFIX = ".INTERCEPTED";
    public static final String ATTR_CURRENT_URL = "currentUrl";
    public static final String ATTR_CURRENT_PATH = "currentPath";

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        boolean hasAlreadyInterceptedAttribute = request.getAttribute(getAlreadyInterceptedAttributeName()) != null;
        if (!hasAlreadyInterceptedAttribute) {
            String currentUrl = request.getRequestURL().toString();
            String rootUrl = RequestUtils.INSTANCE.getRootUrl(request);
            request.setAttribute(ATTR_CURRENT_URL, currentUrl);
            request.setAttribute(ATTR_CURRENT_PATH, currentUrl.substring(rootUrl.length()));
        }
        return super.preHandle(request, response, handler);
    }

    protected String getAlreadyInterceptedAttributeName() {
        return getClass().getName() + ALREADY_INTERCEPTED_SUFFIX;
    }
}
