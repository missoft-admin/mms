ALTER TABLE CRM_STORE_MEMBER_AUD ADD STORE_BACKUP VARCHAR2(10 CHAR);
UPDATE CRM_STORE_MEMBER_AUD SET STORE_BACKUP = STORE;
ALTER TABLE CRM_STORE_MEMBER_AUD MODIFY (STORE varchar2(10 char) NULL);
UPDATE CRM_STORE_MEMBER_AUD SET STORE = STORE_BACKUP;
ALTER TABLE CRM_STORE_MEMBER_AUD DROP COLUMN STORE_BACKUP;