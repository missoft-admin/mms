<%@ include file="../../common/taglibs.jsp" %>

  <style type="text/css">
  <!--
    #content_scheme .dataTable tr td:last-child {
      min-width: 150px;
      padding-top: 10px;
    }
    
    .well .input {
      background-color: #fff;
      border: 1px solid #E8E8E8;
      box-shadow: none;    
      padding: 6px;
      height: 18px;
      margin-left: 5px;
    }
    .well .btn {
      margin-left: 5px;
    }
    .well select {
      margin: 0;
    }
    .well .input-group {
      margin: 5px;
    }
    .checkbox {
      margin: 0 0 0 20px;
    }
  -->
  </style>

<spring:message code="loyalty_annual_scheme" var="itemType" />

<div class="page-header page-header2">
    <h1><spring:message code="menutab_loyalty_annual_fee_scheme" /></h1>
</div>

<div id="schemeSearchForm" class="well clearfix">
        <div class="pull-left input-group">
          <spring:message code="loyalty_annual_company" var="company" />
          <label for="">${company}</label>
          <select name="company" class="span2">
            <option value=""></option>
            <c:forEach items="${companies}" var="item" >
              <option value="${item.code}">${item.description}</option>
            </c:forEach>
          </select>
        </div>
        
        <div class="pull-left input-group">
          <spring:message code="loyalty_annual_cardtype" var="cardType" />
          <label for="">${cardType}</label>
          <select name="cardType" class="span2">
            <option value=""></option>
            <c:forEach items="${cardTypes}" var="item" >
              <option value="${item.code}">${item.description}</option>
            </c:forEach>
          </select>
        </div>
        
        <div class="input-group">
          <div class="pull-left">
          <label for="">&nbsp;</label>
          <input type="button" id="searchButton" value="<spring:message code="label_search" />" class="btn btn-primary"/>
          </div>
          
          <div class="pull-left">
          <label for="">&nbsp;</label>
          <input type="button" id="clearButton" value="<spring:message code="label_clear" />" class="btn"/>
          </div>
        </div>
</div>

<div class="pull-right mb20">
  <button type="button" id="createBtn" class="btn btn-primary"><spring:message code="global_menu_new" arguments="${itemType}" /></button>
</div>

<div id="content_scheme">
  <div id="list_scheme"></div>
</div>

<div class="modal hide nofly" tabindex="-1" role="dialog" aria-hidden="true" id="schemeModal"></div>
<jsp:include page="../../common/confirm.jsp" />

  <link href="<spring:url value="/css/dataTables.css" />" rel="stylesheet">
  <script src="<spring:url value="/js/jquery/jquery.dataTables.js" />" ></script>
  <script src="<spring:url value="/js/jquery/jquery.dataTables.linkLengthMenu.js" />"></script>
  <script src="<spring:url value="/js/jquery/jquery.dataTables.customPaging.js" />"></script>
  <script src="<spring:url value="/js/jquery/ajax-datatable.js" />" ></script>
  <script src="<spring:url value="/js/common/form2js.js" />" ></script>
  <script src="<spring:url value="/js/common/json2.js" />" ></script>
  <script src="<spring:url value="/js/jquery/jquery.toObject.js" />" ></script>
  <link href="<c:url value="/styles/temp.css" />" rel="stylesheet" />
  <script src="<spring:url value="/js/jquery/jquery.numberInput.js" />" ></script>
  
<script type="text/javascript">

$(document).ready(function() {
	
	$schemeModal = $("#schemeModal").modal({
		show: false
	}).on('hidden.bs.modal', function() {
		$(this).empty();
	})
	.css({
		"width": "500px",
	})
	;
	
	$("#list_scheme").ajaxDataTable({
		'autoload'  : true,
		'ajaxSource' : "<c:url value="/loyaltyannualfee/scheme/list" />",
		'columnHeaders' : [
			"<spring:message code="loyalty_annual_company"/>",
			"<spring:message code="loyalty_annual_cardtype"/>",
			"<spring:message code="loyalty_annual_annualfee"/>",
			"<spring:message code="loyalty_annual_replacementfee"/>",
			""
		],
		'modelFields' : [
			{name : 'company.description', sortable : true},
			{name : 'cardType.description', sortable : true},
			{name : 'annualFee', sortable : true, fieldCellRenderer : function (data, type, row) {
              	 if(row.annualFee != null && row.annualFee != "")
              		 return row.annualFee.toLocaleString();
              	 else
              		 return "";
            }},
			{name : 'replacementFee', sortable : true, fieldCellRenderer : function (data, type, row) {
             	 if(row.replacementFee != null && row.replacementFee != "")
            		 return row.replacementFee.toLocaleString();
            	 else
            		 return "";
			}},
			{customCell : function ( innerData, sSpecific, json ) {
				if (sSpecific == 'display') {
					return '<button type="button" data-scheme-id="'+json.id+'" class="tiptip btn pull-left icn-edit schemeBtn" title="<spring:message code="label_update_arg" arguments="${itemType}" />"><spring:message code="label_update_arg" arguments="${itemType}" /></button>'
					+ '<button type="button" data-scheme-id="'+json.id+'" class="tiptip btn pull-left icn-delete schemeDltBtn" title="<spring:message code="entity_delete" arguments="${itemType}" />"><spring:message code="entity_delete" arguments="${itemType}" /></button>';
					
					
				} else {
					return "";
				}
			}
			}
		]
	}).on("click", ".schemeBtn", showModal)
	.on("click", ".schemeDltBtn", deleteScheme);;
	
	$("#createBtn").click(showModal);
	
	initSearchForm();
	
	function deleteScheme() {
		var schemeId = $(this).data("schemeId");
		getConfirm('<spring:message code="entity_delete_confirm" />', function(result) { 
			if(result) {
				$.post("<c:url value="/loyaltyannualfee/scheme/" />" + schemeId, {_method: "DELETE"}, function(data) {
					refreshTable();
				});	
			}
		});
	}
	
	function showModal(e) {
		e.preventDefault();
		var url = "<spring:url value="/loyaltyannualfee/scheme/form/" />";
		$.get(url + $(this).data("schemeId"), function(data) {
			$schemeModal.modal("show").html(data);
			$schemeModal.find(".floatInput").numberInput({ "type" : "float" });
		}, "html");
	}
	
	function initSearchForm() {
		$("#searchButton").click(function() {
			var formDataObj = $("#schemeSearchForm").toObject( { mode : 'first', skipEmpty : true } );
			$btnSearch = $(this);
			$btnSearch.prop('disabled', true);

			$("#list_scheme").ajaxDataTable( 
				'search', 
				formDataObj, 
				function() {
					$btnSearch.prop( 'disabled', false );
			});
		});
		
		$("#clearButton").click(function() {
			$("#schemeSearchForm select").val("");
			refreshTable();
		});
	}
	
});

function refreshTable() {
	$("#searchButton").click();
}

</script>