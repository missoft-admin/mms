package com.transretail.crm.schedule.job.service;

/**
 *
 * @author Monte Cillo Co (mco@exist.com)
 */
public interface ClaimToAffiliatesJobService {

    void execute();
}
