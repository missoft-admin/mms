<%@include file="../../../common/taglibs.jsp"%>


<div id="content_soInfoSegment" class="content_soForm">
        <div class="soorder-block span6">
          <div class="control-group">
            <label class="control-label"><spring:message code="gc.so.date" /></label>
            <div class="controls">${salesOrder.orderDate}</div>
          </div>
          <div class="control-group">
            <label class="control-label"><spring:message code="gc.so.customer" /></label>
            <div class="controls">${salesOrder.customerId} - ${salesOrder.customerDesc}</div>
          </div>
          <c:if test="${ salesOrder.orderType == 'B2B_SALES' || salesOrder.orderType == 'B2B_ADV_SALES' || salesOrder.orderType == 'REPLACEMENT' }">
          <div class="control-group">
            <label class="control-label"><spring:message code="gc.so.discount" /></label>
            <div class="controls">${salesOrder.discountVal}%</div>
          </div>
          </c:if>
          
          <c:if test="${ salesOrder.orderType == 'B2B_SALES' || salesOrder.orderType == 'B2B_ADV_SALES'}">
          <div class="control-group">
            <label class="control-label"><spring:message code="sales_order_payment_store" /></label>
            <div class="controls">${salesOrder.paymentStore} - ${salesOrder.paymentStoreName}</div>
          </div>
          </c:if>
          <c:if test="${ salesOrder.orderType == 'REPLACEMENT'}">
          <div class="control-group">
            <label class="control-label"><spring:message code="sales_original_salesorder" /></label>
            <div class="controls">${salesOrder.origOrderNo} (${salesOrder.origOrderDate})</div>
          </div>
          </c:if>
          
        </div>

        <div class="soorder-block span6">
          <div class="control-group">
            <label class="control-label"><spring:message code="gc.so.contact" /></label>
            <div class="controls">${salesOrder.contactPerson} - ${salesOrder.contactNumber}/${salesOrder.contactEmail}</div>
          </div>
          
          <div class="control-group">
            <label class="control-label"><spring:message code="gc.so.notes" /></label>
            <div class="controls">${salesOrder.notes}</div>
          </div>
          
          <div class="control-group">
            <label class="control-label"><spring:message code="order_total_face_amount" /></label>
            <div class="controls"><fmt:formatNumber type="number" pattern="#,##0" value="${salesOrder.totalFaceAmount}"/></div>
          </div>
          
          
        </div>


<style type="text/css"> 
<!-- 
  .content_soForm .alert { padding-left: 0px !important; }
  .content_soForm .soorderalloc-block { margin-top: 20px; }
  .content_soForm .checkElement { float: left; margin-right: 5px; }
  .content_soForm .checkElements .controls { margin-left: 150px !important; }
  .content_soForm .checkElements .control-label { width: 150px !important; }
  .content_soForm .pull-center { padding-left: 10px; padding-right: 10px; }
  .content_soForm .modal-footer { text-align: center; } 
  .content_soForm .control-group .controls { margin-top: 5px; margin-left: 110px; }
  .content_soForm .control-group .control-label { width: 100px; text-align: left; }
  .content_soForm .table-col-xlarge { width: 320px; }
--> 
</style>
</div>