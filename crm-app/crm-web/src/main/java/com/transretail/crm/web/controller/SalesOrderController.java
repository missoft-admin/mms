package com.transretail.crm.web.controller;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.BooleanUtils;
import org.apache.commons.lang3.StringUtils;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.CollectionUtils;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.transretail.crm.common.reporting.jasper.JRProcessor;
import com.transretail.crm.common.service.dto.response.ResultList;
import com.transretail.crm.core.entity.enums.ReportType;
import com.transretail.crm.core.entity.lookup.Store;
import com.transretail.crm.core.service.CodePropertiesService;
import com.transretail.crm.core.service.LookupService;
import com.transretail.crm.core.service.PsoftStoreService;
import com.transretail.crm.core.service.StoreService;
import com.transretail.crm.giftcard.dto.GiftCardCxProfileSearchDto;
import com.transretail.crm.giftcard.dto.GiftCardDiscountSchemeDto;
import com.transretail.crm.giftcard.dto.ReturnRecordDto;
import com.transretail.crm.giftcard.dto.SalesOrderDto;
import com.transretail.crm.giftcard.dto.SalesOrderItemDto;
import com.transretail.crm.giftcard.dto.SalesOrderSearchDto;
import com.transretail.crm.giftcard.entity.SalesOrder;
import com.transretail.crm.giftcard.entity.support.CustomerProfileType;
import com.transretail.crm.giftcard.entity.support.DiscountType;
import com.transretail.crm.giftcard.entity.support.PaymentInfoStatus;
import com.transretail.crm.giftcard.entity.support.SalesOrderDiscountType;
import com.transretail.crm.giftcard.entity.support.SalesOrderStatus;
import com.transretail.crm.giftcard.entity.support.SalesOrderType;
import com.transretail.crm.giftcard.service.DiscountSchemeService;
import com.transretail.crm.giftcard.service.DiscountSchemeService.DiscountDto;
import com.transretail.crm.giftcard.service.GiftCardAccountingService;
import com.transretail.crm.giftcard.service.GiftCardCxProfileService;
import com.transretail.crm.giftcard.service.ProductProfileService;
import com.transretail.crm.giftcard.service.ReturnGiftCardService;
import com.transretail.crm.giftcard.service.SalesOrderDeptService;
import com.transretail.crm.giftcard.service.SalesOrderPaymentService;
import com.transretail.crm.giftcard.service.SalesOrderService;
import com.transretail.crm.web.controller.util.ControllerResponse;

@Controller
@RequestMapping("/giftcard/salesorder")
public class SalesOrderController extends AbstractReportsController {

	private static final Logger LOG = LoggerFactory.getLogger(SalesOrderController.class);

	private SalesOrderService orderService;
	private MessageSource messageSource;
	private ProductProfileService productProfileService;
	private GiftCardCxProfileService customerService;
	private CodePropertiesService codePropertiesService;
	private ReturnGiftCardService returnService;
	private LookupService lookupService;
	private StoreService storeService;
	private SalesOrderPaymentService paymentService;
	private DiscountSchemeService discountService;
	private SalesOrderDeptService deptService;
	private GiftCardAccountingService accountingService;
	private PsoftStoreService psoftStoreService;

	private static final DateTimeFormatter FORMATTER = DateTimeFormat.forPattern("dd-MM-yyyy");

	/* ----------------- IoC -------------------*/
	@Autowired
	public void setOrderService(SalesOrderService orderService) {
		this.orderService = orderService;
	}

	@Autowired
	public void setMessageSource(MessageSource messageSource) {
		this.messageSource = messageSource;
	}

	@Autowired
	public void setProductProfileService(ProductProfileService productProfileService) {
		this.productProfileService = productProfileService;
	}

	@Autowired
	public void setCustomerService(GiftCardCxProfileService customerService) {
		this.customerService = customerService;
	}

	@Autowired
	public void setCodePropertiesService(CodePropertiesService codePropertiesService) {
		this.codePropertiesService = codePropertiesService;
	}

	@Autowired
	public void setReturnService(ReturnGiftCardService returnService) {
		this.returnService = returnService;
	}

	@Autowired
	public void setLookupService(LookupService lookupService) {
		this.lookupService = lookupService;
	}

	@Autowired
	public void setStoreService(StoreService storeService) {
		this.storeService = storeService;
	}

	@Autowired
	public void setPaymentService(SalesOrderPaymentService paymentService) {
		this.paymentService = paymentService;
	}

	@Autowired
	public void setDiscountService(DiscountSchemeService discountService) {
		this.discountService = discountService;
	}

	@Autowired
	public void setDeptService(SalesOrderDeptService deptService) {
		this.deptService = deptService;
	}

	@Autowired
	public void setAccountingService(GiftCardAccountingService accountingService) {
		this.accountingService = accountingService;
	}

	@Autowired
	public void setPsoftStoreService(PsoftStoreService psoftStoreService) {
		this.psoftStoreService = psoftStoreService;
	}

	/* ----------------- Controller ------------------- */
	@RequestMapping(value = "/history/{id}", method = RequestMethod.GET, produces = "text/html")
	public String showHistories(@PathVariable("id") String id, Model uiModel) {
		uiModel.addAttribute("histories", orderService.getHistories(Long.valueOf(id)));
		return "giftcard/salesorder/historylist";
	}

	@RequestMapping(method = RequestMethod.POST, produces = "text/html")
	public String search(@RequestParam Map<String, String> allRequestParams, Model uiModel) {
		uiModel.addAttribute("showDialog", allRequestParams.get("showDialog"));
		uiModel.addAttribute("returnNo", allRequestParams.get("returnNo"));
		uiModel.addAttribute("byCards", allRequestParams.get("byCards"));
		return "giftcard/salesorder/list";
	}

	@RequestMapping(value = "/form/fromreturn/{returnNo}/{byCards}", method = RequestMethod.GET, produces = "text/html")
	public String showFormFromReturn(
			@PathVariable("returnNo") String returnNo,
			@PathVariable Boolean byCards,
			Model uiModel) {
		SalesOrderDto orderForm = new SalesOrderDto();
		orderForm.setReturnNo(returnNo);
		orderForm.setOrderType(SalesOrderType.REPLACEMENT);
		populateForm(orderForm, uiModel);
		return "giftcard/salesorder/replacementform";
	}

	@RequestMapping(value = "/search", method = RequestMethod.POST)
	public @ResponseBody ResultList<SalesOrderDto> search(@RequestBody SalesOrderSearchDto filterDto) {
		return orderService.getSalesOrders(filterDto);
	}

	@RequestMapping(value = "/form", method = RequestMethod.GET, produces = "text/html")
	public String showForm(Model uiModel) {
		SalesOrderDto orderForm = new SalesOrderDto();
		orderForm.setOrderType(SalesOrderType.B2B_SALES);

		populateFormWithoutCustomersAttribute(orderForm, uiModel);
		return "giftcard/salesorder/form";
	}

	@RequestMapping(value = "/form/promoorder", method = RequestMethod.GET, produces = "text/html")
	public String showPromoOrderForm(Model uiModel) {
		SalesOrderDto orderForm = new SalesOrderDto();
		orderForm.setOrderType(SalesOrderType.INTERNAL_ORDER);

		populateForm(orderForm, uiModel);
		return "giftcard/salesorder/form";
	}

	@RequestMapping(value = "/form/replacement", method = RequestMethod.GET, produces = "text/html")
	public String showReplacementForm(Model uiModel) {
		SalesOrderDto orderForm = new SalesOrderDto();
		orderForm.setOrderType(SalesOrderType.REPLACEMENT);

		populateForm(orderForm, uiModel);
		return "giftcard/salesorder/replacementform";
	}

	@RequestMapping(value = "/form/{id}", method = RequestMethod.GET, produces = "text/html")
	public String showForm(
			@PathVariable("id") String id,
			Model uiModel) {
		SalesOrder order = orderService.get(Long.valueOf(id));
		SalesOrderDto orderDto = new SalesOrderDto(order, true);
		Store store = storeService.getStoreByCode(orderDto.getPaymentStore());
		orderDto.setPaymentStoreName(null != store ? store.getCodeAndName() : null);
		populateForm(orderDto, uiModel);
		uiModel.addAttribute("cxProfile", order.getCustomer());

		if (orderDto.getOrderType().compareTo(SalesOrderType.REPLACEMENT) != 0) {
			if (orderDto.getStatus().compareTo(SalesOrderStatus.FOR_APPROVAL) == 0) {
				return "giftcard/salesorder/approvalform";
			}
			return "giftcard/salesorder/form";
		} else {
			if (orderDto.getStatus().compareTo(SalesOrderStatus.FOR_APPROVAL) == 0) {
				return "giftcard/salesorder/replacementapprovalform";
			}
			return "giftcard/salesorder/replacementform";
		}

	}

	@RequestMapping(value = "/save/{status}", method = RequestMethod.POST, produces = "application/json; charset=utf-8")
	@ResponseBody
	public ControllerResponse ajaxSave(
			@PathVariable("status") String status,
			@ModelAttribute(value = "profileForm") SalesOrderDto orderDto,
			BindingResult result, HttpServletRequest request) {

		ControllerResponse theResponse = new ControllerResponse();
		theResponse.setSuccess(true);
		SalesOrderStatus stat = SalesOrderStatus.valueOf(status);
		orderDto.setStatus(stat);

		validateProfile(result, orderDto);

		if (result.hasErrors()) {
			theResponse.setSuccess(false);
			theResponse.setResult(result.getAllErrors());
			return theResponse;
		}

		if (theResponse.isSuccess()) {

			Long id = orderService.saveOrder(orderDto);
			orderService.processHistory(id, stat);
			LOG.info("id :: " + id);
			if (stat.compareTo(SalesOrderStatus.FOR_APPROVAL) == 0) {

				if ((orderDto.getOrderType().compareTo(SalesOrderType.B2B_SALES) == 0
						|| orderDto.getOrderType().compareTo(SalesOrderType.B2B_ADV_SALES) == 0)
						&& paymentService.updateStatus(id, PaymentInfoStatus.FIRST_APPROVAL) == 0) {
					paymentService.createEmptyPayment(id, orderDto.getOrderDate());
				}
				if (orderDto.getOrderType().compareTo(SalesOrderType.INTERNAL_ORDER) == 0) {
					deptService.createEmptyAllocForRemaingAmt(id, orderDto.getTotalFaceAmount());
				}
			}
		}

		return theResponse;
	}

	@RequestMapping(produces = "text/html")
	public String show(Model uiModel) {
		populateSearch(uiModel);
		return "giftcard/salesorder/list";
	}

	private void populateSearch(Model uiModel) {
		uiModel.addAttribute("orderSearchCriteria", OrderSearchCriteria.values());
		uiModel.addAttribute("orderStatusCriteria", SalesOrderStatus.values());
		uiModel.addAttribute("orderTypeCriteria", SalesOrderType.values());
		uiModel.addAttribute("reportTypes", ReportType.values());
	}

	@RequestMapping(value = "/{id}", method = RequestMethod.DELETE, produces = "application/json; charset=utf-8")
	@ResponseBody
	public ControllerResponse delete(@PathVariable("id") Long id,
			Model uiModel) {
		ControllerResponse theResponse = new ControllerResponse();
		theResponse.setSuccess(true);
		orderService.deleteOrder(id);
		return theResponse;
	}

	@RequestMapping(value = "/approve/{status}", method = RequestMethod.POST, produces = "application/json; charset=utf-8")
	@ResponseBody
	public ControllerResponse ajaxApprove(
			@PathVariable("status") String status,
			@ModelAttribute(value = "profileForm") SalesOrderDto orderDto,
			BindingResult result, HttpServletRequest request) {

		ControllerResponse theResponse = new ControllerResponse();
		theResponse.setSuccess(true);

		if (result.hasErrors()) {
			theResponse.setSuccess(false);
			theResponse.setResult(result.getAllErrors());
			return theResponse;
		}

		if (theResponse.isSuccess()) {
			SalesOrderStatus stat = SalesOrderStatus.valueOf(status);
			orderService.approveOrder(orderDto.getId(), stat);
			orderService.processDiscount(orderDto.getId(), stat);
			orderService.processReplacement(orderDto.getId());
			orderService.processHistory(orderDto.getId(), stat);
			if (stat.compareTo(SalesOrderStatus.DRAFT) == 0) {
				paymentService.updateStatus(orderDto.getId(), PaymentInfoStatus.DRAFT);

				orderDto = orderService.getOrder(orderDto.getId());
				if (orderDto.getOrderType().compareTo(SalesOrderType.B2B_ADV_SALES) == 0) {
					accountingService.forAdvSoVerifCancelled(new DateTime(), orderDto.getTotalFaceAmount(), orderDto.getCustomerDesc());
				}
			}
		}

		return theResponse;
	}

	@RequestMapping(value = "/printall", method = RequestMethod.GET)
	public void printMo(
			@RequestParam(value = "reportType") String reportType,
			@RequestParam(value = "searchType", required = false) String searchType,
			@RequestParam(value = "searchValue", required = false) String searchValue,
			@RequestParam(value = "dateFrom", required = false) String dateFrom,
			@RequestParam(value = "dateTo", required = false) String dateTo,
			@RequestParam(value = "status", required = false) String status,
			@RequestParam(value = "orderType", required = false) String orderType,
			HttpServletResponse response) throws Exception {

		SalesOrderSearchDto searchDto = new SalesOrderSearchDto();

		if (searchType != null && searchValue != null) {
			if (searchType.equalsIgnoreCase("company"))
				searchDto.setCompany(searchValue);
			else if (searchType.equalsIgnoreCase("contactPerson"))
				searchDto.setContactPerson(searchValue);
		}
		if (dateFrom != null) {
			searchDto.setOrderDateFrom(FORMATTER.parseLocalDate(dateFrom));
		}

		if (dateTo != null) {
			searchDto.setOrderDateTo(FORMATTER.parseLocalDate(dateTo));
		}

		if (status != null) {
			searchDto.setStatus(status);
		}

		if (orderType != null) {
			searchDto.setOrderType(SalesOrderType.valueOf(orderType));
		}

		JRProcessor jrProcessor = orderService.createJrProcessor(searchDto);
		if (ReportType.PDF.getCode().equals(reportType))
			renderReport(response, jrProcessor);
		else if (ReportType.EXCEL.getCode().equals(reportType))
			renderExcelReport(response, jrProcessor, getMessage("sales_orders", null));
	}

	public static enum OrderSearchCriteria {
		COMPANY("company"), CONTACT("contactPerson"),CODE("orderNo");

		private final String field;

		private OrderSearchCriteria(String field) {
			this.field = field;
		}

		public String getField() {
			return field;
		}

	}

	/* ===================== Helper Method ========================== */

	private void validateProfile(BindingResult result, SalesOrderDto orderDto) {
		if (orderDto.getOrderDate() == null) {
			result.reject(getMessage("propkey_msg_notempty", new Object[] { "sales_order_date" }));
		}

		if (orderDto.getOrderType().compareTo(SalesOrderType.REPLACEMENT) != 0) {

			if (orderDto.getCustomerId() == null) {
				result.reject(getMessage("propkey_msg_notempty", new Object[] { "sales_order_customer" }));
			} else if (orderDto.getOrderType().compareTo(SalesOrderType.INTERNAL_ORDER) != 0 && orderDto.getOrderType().compareTo(SalesOrderType.VOUCHER) != 0) {
				GiftCardDiscountSchemeDto schemeDto = discountService.getDiscountScheme(orderDto.getCustomerId(), orderDto.getOrderDate());
				if ((schemeDto == null || schemeDto.getDiscountType().compareTo(DiscountType.SO_YEARLY_DISCOUNT) == 0) && orderDto.getDiscType() == null) {
					result.reject(getMessage("sales_order_err_discounttype", null));
				}
			}
		}

		BigDecimal returnAmount = null;

		if (orderDto.getOrderType().compareTo(SalesOrderType.REPLACEMENT) == 0) {
			if (StringUtils.isBlank(orderDto.getReturnNo())) {
				result.reject(getMessage("propkey_msg_notempty", new Object[] { "gc_return_no" }));
			} else {
				ReturnRecordDto retRec = returnService.getReturnByRecordNo(orderDto.getReturnNo());
				if (retRec == null) {
					result.reject(getMessage("gc_return_invalid_number", null));
				} else {
					returnAmount = retRec.getRemainingReturnAmount();
				}
			}
		}

		if (!isValidItems(orderDto.getItems())) {
			result.reject(getMessage("propkey_msg_notempty", new Object[] { "sales_order_item_product_name" }));
		}
		if (orderDto.getOrderType().compareTo(SalesOrderType.REPLACEMENT) == 0 && returnAmount != null) {

			if (orderDto.getHandlingFee() == null) {
				result.reject(getMessage("propkey_msg_notempty", new Object[] { "sales_order_replacementfee" }));
			} else {
				BigDecimal totalSoAmt = orderDto.getTotalFaceAmount();
				if (BooleanUtils.isNotTrue(orderDto.getPaidReplacement())) {
					totalSoAmt = totalSoAmt.add(orderDto.getHandlingFee());
				}

				if (totalSoAmt.compareTo(returnAmount) > 0) {
					result.reject(getMessage("gc_return_err_replace_gt_return", null));
				}

			}

		}

		if (orderDto.getOrderType().compareTo(SalesOrderType.B2B_SALES) == 0
				|| orderDto.getOrderType().compareTo(SalesOrderType.B2B_ADV_SALES) == 0) {
			if (StringUtils.isBlank(orderDto.getPaymentStore()))
				result.reject(getMessage("propkey_msg_notempty", new Object[] { "sales_order_payment_store" }));
		}

		if (orderDto.getOrderType().compareTo(SalesOrderType.INTERNAL_ORDER) == 0) {
			if (orderDto.getCategory() == null) {
				result.reject(getMessage("propkey_msg_notempty", new Object[] { "sales_order_category" }));
			}
		}

		if (orderDto.getStatus().compareTo(SalesOrderStatus.FOR_APPROVAL) == 0 && (orderDto.getOrderType().compareTo(SalesOrderType.B2B_SALES) == 0
				|| orderDto.getOrderType().compareTo(SalesOrderType.B2B_ADV_SALES) == 0)
				&& orderDto.getCustomerId() != null && orderDto.getOrderDate() != null && orderDto.getDiscType() != null) {
			BigDecimal orderAmt = orderDto.getTotalFaceAmount();
			BigDecimal totalCardFee = BigDecimal.ZERO;
			BigDecimal shippingFee = orderDto.getShippingFee() == null ? BigDecimal.ZERO : orderDto.getShippingFee();
			DiscountDto discount = discountService.getDiscountBeforeApproval(orderDto.getCustomerId(), orderAmt, orderDto.getOrderDate(), orderDto.getDiscType());
			for (SalesOrderItemDto itemDto : orderDto.getItems()) {
				if (itemDto.getPrintFee() != null)
					totalCardFee = totalCardFee.add(itemDto.getPrintFee());
			}
			if (discount != null && discount.getDiscount() != null) {
				orderAmt = orderAmt.subtract(orderAmt.multiply(discount.getDiscount().divide(new BigDecimal(100))));
			}
			orderAmt = orderAmt.add(totalCardFee);
			orderAmt = orderAmt.add(shippingFee);
			BigDecimal totalPayment = BigDecimal.ZERO;
			if (orderDto.getId() != null) {
				totalPayment = paymentService.getTotalPayments(orderDto.getId());
			}
			if (totalPayment.compareTo(orderAmt) < 0) {
				result.reject(getMessage("sales_order_err_insufficientpayment", null));
			}

		}

		if (SalesOrderType.VOUCHER.compareTo(orderDto.getOrderType()) == 0 || SalesOrderType.YEARLY_DISCOUNT.compareTo(orderDto.getOrderType()) == 0) {
			BigDecimal parentSoAmt = orderDto.getParentFaceAmt();
			BigDecimal voucherVal = orderDto.getParentVoucherVal();
			BigDecimal soAmt = orderDto.getNetAmt();

			BigDecimal compare = parentSoAmt.multiply(voucherVal.divide(new BigDecimal(100)));

			if (compare.compareTo(soAmt) < 0) {
				result.reject(getMessage("sales_order_err_vouchersoamt", null));
			}
		}

	}

	private void populateFormWithoutCustomersAttribute(SalesOrderDto orderDto, Model uiModel) {

		uiModel.addAttribute("orderForm", orderDto);
		uiModel.addAttribute("discTypes", SalesOrderDiscountType.values());
		uiModel.addAttribute("paymentTypes", lookupService.getActiveDetailsByHeaderCode(codePropertiesService.getHeaderGcSoPaymentMethod()));
		uiModel.addAttribute("stores", psoftStoreService.getGcAllowedStores());
		uiModel.addAttribute("categories", lookupService.getActiveDetailsByHeaderCode(codePropertiesService.getHeaderGcSoCategory()));
		uiModel.addAttribute("profiles", productProfileService.findAllDtos());
	}

	private void populateForm(SalesOrderDto orderDto, Model uiModel) {
		uiModel.addAttribute("orderForm", orderDto);
		if (null != orderDto.getOrderType()) {
			GiftCardCxProfileSearchDto searchDto = new GiftCardCxProfileSearchDto();
			if (orderDto.getOrderType() == SalesOrderType.B2B_SALES
					|| orderDto.getOrderType() == SalesOrderType.B2B_ADV_SALES) {
				searchDto.setCustomerTypes(
						new CustomerProfileType[] { CustomerProfileType.B2B, CustomerProfileType.GENERAL });
			} else if (orderDto.getOrderType() == SalesOrderType.INTERNAL_ORDER) {
				searchDto.setCustomerTypes(
						new CustomerProfileType[] { CustomerProfileType.INTERNAL, CustomerProfileType.GENERAL });
			}
			uiModel.addAttribute("customers", customerService.getProfiles(searchDto));
		}

		uiModel.addAttribute("discTypes", SalesOrderDiscountType.values());
		uiModel.addAttribute("paymentTypes", lookupService.getActiveDetailsByHeaderCode(codePropertiesService.getHeaderGcSoPaymentMethod()));
		uiModel.addAttribute("stores", psoftStoreService.getGcAllowedStores());
		uiModel.addAttribute("categories", lookupService.getActiveDetailsByHeaderCode(codePropertiesService.getHeaderGcSoCategory()));
		uiModel.addAttribute("profiles", productProfileService.findAllDtos());
	}

	private boolean isValidItems(List<SalesOrderItemDto> items) {
		if (CollectionUtils.isEmpty(items))
			return false;
		for (SalesOrderItemDto itemDto : items) {
			if (itemDto.getProductId() == null || itemDto.getQuantity() == null)
				return false;
		}
		return true;

	}

	private String getMessage(String inMsgCode, Object[] inArgMsgCodes) {
		if (inArgMsgCodes != null) {
			for (int i = 0; i < inArgMsgCodes.length; i++) {
				inArgMsgCodes[i] = messageSource.getMessage(inArgMsgCodes[i].toString().toLowerCase(), null, LocaleContextHolder.getLocale());
			}
		}
		return messageSource.getMessage(inMsgCode, inArgMsgCodes, "Info given is invalid.", LocaleContextHolder.getLocale());
	}
}
