package com.transretail.crm.core.entity.embeddable;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.validation.constraints.Pattern;

import com.transretail.crm.core.util.AppConstants;

/**
 * @author Allan G. Ramirez (agramirez@exist.com)
 */
@Embeddable
public class Npwp implements Serializable {
    private static final long serialVersionUID = 8763534042314493576L;
    
    public static final String DEFAULT_NPWP_ID = "000000000000000";

    @Pattern(regexp = AppConstants.NPWP_ID_PATTERN)
    @Column(name = "NPWP_ID")
    private String npwpId;

    @Column(name = "NPWP_NAME")
    private String npwpName;

    @Column(name = "NPWP_ADDRESS")
    private String npwpAddress;

    public Npwp() {

    }

    public Npwp(String npwpId, String npwpName, String npwpAddress) {
        this.npwpId = npwpId;
        this.npwpName = npwpName;
        this.npwpAddress = npwpAddress;
    }

    public String getNpwpId() {
        return npwpId;
    }

    public void setNpwpId(String npwpId) {
        this.npwpId = npwpId;
    }

    public String getNpwpName() {
        return npwpName;
    }

    public void setNpwpName(String npwpName) {
        this.npwpName = npwpName;
    }

    public String getNpwpAddress() {
        return npwpAddress;
    }

    public void setNpwpAddress(String npwpAddress) {
        this.npwpAddress = npwpAddress;
    }
}
