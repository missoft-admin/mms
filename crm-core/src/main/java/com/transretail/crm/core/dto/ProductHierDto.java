package com.transretail.crm.core.dto;


import java.util.ArrayList;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;

import com.transretail.crm.product.HierarchyLevel;
import com.transretail.crm.product.entity.CrmProductHier;


public class ProductHierDto {

	private String code;
	private String description;
	private String localDesc;
	//private LookupDetail type;
	private String level;


	public ProductHierDto(){}
	public ProductHierDto( CrmProductHier model ) {
		this.code = model.getCode();
		this.description = model.getEnglishDesc();
		this.localDesc = model.getLocalDesc();
		this.level = null != model.getLevel()? model.getLevel().toString() : null;
	}

	public static List<ProductHierDto> toDtoList( List<CrmProductHier> models ) {
		if ( CollectionUtils.isEmpty( models ) ) {
			return null;
		}

		List<ProductHierDto> dtos = new ArrayList<ProductHierDto>();
		for ( CrmProductHier model : models ) {
			dtos.add( new ProductHierDto( model ) );
		}
		return dtos;
	}

    public static HierarchyLevel getHierarchyLevel( String strLevel ) {
    	/*if ( StringUtils.isBlank( strLevel ) && !EnumUtils.isValidEnum( HierarchyLevel.class, strLevel ) ) {
    		return null;
    	}
		return EnumUtils.getEnum( HierarchyLevel.class, strLevel );*/
    	if ( StringUtils.isBlank( strLevel ) && !NumberUtils.isNumber( strLevel )
    			&& NumberUtils.toInt( strLevel ) >= HierarchyLevel.values().length ) {
    		return null;
    	}
		return HierarchyLevel.values()[ NumberUtils.toInt( strLevel ) ];
    	
    }

    public static int[] getHierarchyLevelValues() {
    	int[] ordinalValues = new int[ HierarchyLevel.values().length ];
    	for ( int i=0; i< HierarchyLevel.values().length; i ++ ) {
    		ordinalValues[i] = i;
    	}
    	return ordinalValues;
    }



	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getLocalDesc() {
		return localDesc;
	}
	public void setLocalDesc(String localDesc) {
		this.localDesc = localDesc;
	}
	/*public LookupDetail getType() {
		return type;
	}
	public void setType(LookupDetail type) {
		this.type = type;
	}*/
	public String getLevel() {
		return level;
	}
	public void setLevel(String level) {
		this.level = level;
	}

}