package com.transretail.crm.loyalty.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.transretail.crm.core.entity.lookup.LookupDetail;
import com.transretail.crm.core.entity.support.CustomAuditableEntity;

@Table(name = "CRM_LC_ANNUAL_FEE")
@Entity
public class LoyaltyAnnualFeeScheme extends CustomAuditableEntity<Long> implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 5532596525628495030L;
	
	@ManyToOne
	@JoinColumn(name = "COMPANY")
	private LookupDetail company;
	@ManyToOne
	@JoinColumn(name = "CARD_TYPE")
	private LookupDetail cardType;
	@Column(name = "ANNUAL_FEE")
	private Double annualFee;
	@Column(name = "REPLACEMENT_FEE")
	private Double replacementFee;
	
	public LookupDetail getCompany() {
		return company;
	}
	public void setCompany(LookupDetail company) {
		this.company = company;
	}
	public LookupDetail getCardType() {
		return cardType;
	}
	public void setCardType(LookupDetail cardType) {
		this.cardType = cardType;
	}
	public Double getAnnualFee() {
		return annualFee;
	}
	public void setAnnualFee(Double annualFee) {
		this.annualFee = annualFee;
	}
	public Double getReplacementFee() {
		return replacementFee;
	}
	public void setReplacementFee(Double replacementFee) {
		this.replacementFee = replacementFee;
	}
	
}
