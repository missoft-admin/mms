package com.transretail.crm.loyalty.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.transretail.crm.core.entity.lookup.LookupDetail;
import com.transretail.crm.core.entity.support.CustomAuditableEntity;

@Table(name = "CRM_MO_TIER")
@Entity
public class ManufactureOrderTier extends CustomAuditableEntity<Long> implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "MO_NO")
	private ManufactureOrder manufactureOrder;
	
	@Column(name = "BATCH_NO")
	private String batchNo;

    @ManyToOne
    @JoinColumn(name = "NAME")
    private LookupDetail name;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "COMPANY")
    private LookupDetail company;

    @Column(name = "STARTING_SERIES")
	private String startingSeries;
	
	@Column(name = "QUANTITY")
	private Long quantity;

	public ManufactureOrder getManufactureOrder() {
		return manufactureOrder;
	}

	public void setManufactureOrder(ManufactureOrder manufactureOrder) {
		this.manufactureOrder = manufactureOrder;
	}

	public LookupDetail getName() {
		return name;
	}

	public void setName(LookupDetail name) {
		this.name = name;
	}

    public LookupDetail getCompany() {
        return company;
    }

    public void setCompany(LookupDetail company) {
        this.company = company;
    }

    public String getStartingSeries() {
		return startingSeries;
	}

	public void setStartingSeries(String startingSeries) {
		this.startingSeries = startingSeries;
	}

	public Long getQuantity() {
		return quantity;
	}

	public void setQuantity(Long quantity) {
		this.quantity = quantity;
	}

	public String getBatchNo() {
		return batchNo;
	}

	public void setBatchNo(String batchNo) {
		this.batchNo = batchNo;
	}
	
	
}
