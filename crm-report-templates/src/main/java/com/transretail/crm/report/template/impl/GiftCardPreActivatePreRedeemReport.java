package com.transretail.crm.report.template.impl;

import com.google.common.collect.ImmutableMap;
import com.google.common.collect.ImmutableMap.Builder;
import com.google.common.collect.Sets;
import com.mysema.query.BooleanBuilder;
import com.mysema.query.jpa.impl.JPAQuery;
import com.mysema.query.types.ConstantImpl;
import com.mysema.query.types.Expression;
import com.mysema.query.types.Projections;
import com.mysema.query.types.expr.DateExpression;
import com.mysema.query.types.template.DateTemplate;
import com.transretail.crm.common.reporting.jasper.ClasspathFileResolver;
import com.transretail.crm.common.reporting.jasper.DefaultJRProcessor;
import com.transretail.crm.common.reporting.jasper.JRProcessor;
import com.transretail.crm.common.util.DateUtil;
import com.transretail.crm.core.entity.enums.ReportType;
import com.transretail.crm.core.entity.lookup.Store;
import com.transretail.crm.core.service.StoreService;
import com.transretail.crm.giftcard.entity.QGiftCardTransaction;
import com.transretail.crm.giftcard.entity.QGiftCardTransactionItem;
import com.transretail.crm.giftcard.entity.support.GiftCardSaleTransaction;
import com.transretail.crm.report.template.FilterField;
import com.transretail.crm.report.template.ReportTemplate;
import java.util.List;
import java.util.Map;
import java.util.Set;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import net.sf.jasperreports.engine.data.JRMapArrayDataSource;
import org.apache.commons.lang.StringUtils;
import org.joda.time.LocalDate;
import org.joda.time.LocalDateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.MessageSourceAware;
import org.springframework.context.NoSuchMessageException;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.context.support.MessageSourceAccessor;
import org.springframework.stereotype.Service;

/**
 *
 * @author Monte Cillo Co (mco@exist.com)
 */
@Service("giftCardPreActivatePreRedeemReport")
public class GiftCardPreActivatePreRedeemReport implements ReportTemplate, MessageSourceAware {

    protected static GiftCardSaleTransaction[] TARGET_TRANSACTIONS = {GiftCardSaleTransaction.PRE_ACTIVATION, GiftCardSaleTransaction.PRE_REDEMPTION};
    protected static final String REPORT_PERMISSION = "REPORT_GC_PRE_REACTIVATE_PREREDEEM_VIEW";
    protected static final String TEMPLATE_NAME = "preActivatePreRedeemReport";
    protected static final String REPORT_NAME = "reports/gift_card_preactivate_preredeem-report.jasper";
    protected static final String PARAM_DATE_FROM = "dateFrom";
    protected static final String PARAM_DATE_TO = "dateTo";
    protected static final String PARAM_TRANSACTION_TYPE = "transactionType";
    protected static final String PARAM_STORE = "store";
    protected static final String DATE_FORMAT = DateUtil.SEARCH_DATE_FORMAT;
    private MessageSourceAccessor messageSource;
    private static final Logger logger = LoggerFactory.getLogger(GiftCardPreActivatePreRedeemReport.class);

    @PersistenceContext
    private EntityManager em;
    @Autowired
    private StoreService storeService;

    @Override
    public String getName() {
        return TEMPLATE_NAME;
    }

    @Override
    public String getDescription() {
        return getMessage("gc.pre.report.title");
    }

    @Override
    public Set<FilterField> getFilters() {
        Set<FilterField> filters = Sets.newLinkedHashSetWithExpectedSize(2);
        final String dateFrom = getMessage("gc.pre.report.param.date.from");
        filters.add(FilterField.createDateField(PARAM_DATE_FROM, dateFrom, dateFrom, true));
        final String dateTo = getMessage("gc.pre.report.param.date.to");
        filters.add(FilterField.createDateField(PARAM_DATE_TO, dateTo, dateTo, true));
        filters.add(FilterField.createDropdownField(PARAM_TRANSACTION_TYPE,
                getMessage("gc.pre.report.param.transaction.type"), createTransactionTypeOptions()));
        filters.add(FilterField.createDropdownField(PARAM_STORE,
                getMessage("gc.pre.report.param.store"), createStoreOptions()));
        return filters;
    }

    String getMessage(String messageCode) throws NoSuchMessageException {
        return messageSource.getMessage(messageCode, LocaleContextHolder.getLocale());
    }

    @Override
    public boolean canView(Set<String> permissions) {
        return permissions.contains(REPORT_PERMISSION);
    }

    @Override
    public JRProcessor createJrProcessor(Map<String, String> parameters) {
        DateTimeFormatter formatter = DateTimeFormat.forPattern(DateUtil.SEARCH_DATE_FORMAT);
        String tmp = parameters.get(PARAM_DATE_FROM);
        LocalDate dateFrom = LocalDate.parse(tmp, formatter);
        tmp = parameters.get(PARAM_DATE_TO);
        LocalDate dateTo = LocalDate.parse(tmp, formatter);
        String storeCode = parameters.get(PARAM_STORE);
        tmp = parameters.get(PARAM_TRANSACTION_TYPE);
        GiftCardSaleTransaction transactionType = StringUtils.isNotBlank(tmp) ? GiftCardSaleTransaction.valueOf(tmp) : null;
        QGiftCardTransaction qtx = QGiftCardTransaction.giftCardTransaction;
        QGiftCardTransactionItem qtxi = QGiftCardTransactionItem.giftCardTransactionItem;

        BooleanBuilder expBuilder = new BooleanBuilder(qtx.transactionDate.goe(dateFrom.toDateTimeAtStartOfDay().toLocalDateTime())
                .and(qtx.transactionDate.loe(dateTo.plusDays(1).toDateTimeAtStartOfDay().toLocalDateTime().minusMillis(1))));
        if (null == transactionType) {
            expBuilder.and(qtx.transactionType.in(TARGET_TRANSACTIONS));
        } else {
            expBuilder.and(qtx.transactionType.eq(transactionType));
        }

        if (StringUtils.isNotBlank(storeCode)) {
            expBuilder.and(qtx.merchantId.eq(storeCode));
        }

        List<ReportBean> result = new JPAQuery(em).from(qtx)
                .join(qtx.transactionItems, qtxi)
                .where(expBuilder.getValue())
                .list(Projections.bean(ReportBean.class,
                                qtx.merchantId.as("store"), qtx.terminalId.as("terminal"),
                                qtx.cashierId.as("cashier"), qtx.transactionDate,
                                qtx.transactionType.stringValue().as("transactionType"),
                                qtxi.giftCard.barcode.as("cardNo")));

        for (ReportBean bean : result) {
            String code = bean.getStore();
            Store store = storeService.getStoreByCode(code);
            if (store != null) {
                bean.setStore(store.getCodeAndName());
            }
        }

        logger.debug("datasource lenght {}", result.size());
        DefaultJRProcessor processor = new DefaultJRProcessor(
                REPORT_NAME, new JRMapArrayDataSource(new Map[]{ImmutableMap.of("details", result)}));

        processor.setFileResolver(new ClasspathFileResolver("reports"));
        processor.addParameter("DATE_FROM", dateFrom.toString(DateUtil.SEARCH_DATE_FORMAT));
        processor.addParameter("DATE_TO", dateTo.toString(DateUtil.SEARCH_DATE_FORMAT));

        return processor;
    }

    protected DateExpression<LocalDateTime> truncate(Expression<?> expression, String pattern) {
        return DateTemplate.create(LocalDateTime.class, "trunc({0}, '{1s}')", expression, ConstantImpl.create(pattern));
    }

    @Override
    public ReportType[] getReportTypes() {
        return ReportType.values();
    }

    @Override
    public void setMessageSource(MessageSource messageSource) {
        this.messageSource = new MessageSourceAccessor(messageSource);
    }

    private Map<String, String> createStoreOptions() {
        return ImmutableMap.<String, String>builder()
                .put("", "").putAll(storeService.getAllStoresHashMap()).build();
    }

    private Map<String, String> createTransactionTypeOptions() {
        Builder<String, String> builder = ImmutableMap.<String, String>builder().put("", "")
                .put(GiftCardSaleTransaction.PRE_ACTIVATION.name(), GiftCardSaleTransaction.PRE_ACTIVATION.name())
                .put(GiftCardSaleTransaction.PRE_REDEMPTION.name(), GiftCardSaleTransaction.PRE_REDEMPTION.name());

        return builder.build();
    }

    public static class ReportBean {

        private String store;
        private String terminal;
        private String cashier;
        private LocalDateTime transactionDate;
        private String transactionType;
        private String cardNo;
        private boolean success;

        public String getStore() {
            return store;
        }

        public void setStore(String store) {
            this.store = store;
        }

        public String getTerminal() {
            return terminal;
        }

        public void setTerminal(String terminal) {
            this.terminal = terminal;
        }

        public String getCashier() {
            return cashier;
        }

        public void setCashier(String cashier) {
            this.cashier = cashier;
        }

        public LocalDateTime getTransactionDate() {
            return transactionDate;
        }

        public void setTransactionDate(LocalDateTime transactionDate) {
            this.transactionDate = transactionDate;
        }

        public String getTransactionType() {
            return transactionType;
        }

        public void setTransactionType(String transactionType) {
            this.transactionType = transactionType;
        }

        public String getCardNo() {
            return cardNo;
        }

        public void setCardNo(String cardNo) {
            this.cardNo = cardNo;
        }

        public boolean isSuccess() {
            return success;
        }

        public void setSuccess(boolean success) {
            this.success = success;
        }

    }
}
