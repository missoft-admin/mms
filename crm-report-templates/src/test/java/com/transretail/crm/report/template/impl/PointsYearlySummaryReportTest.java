package com.transretail.crm.report.template.impl;

import com.google.common.collect.ImmutableMap;
import com.transretail.crm.common.reporting.exception.ReportException;
import com.transretail.crm.common.reporting.jasper.JRProcessor;
import com.transretail.crm.report.template.ReportTemplate;
import java.io.File;
import java.util.*;
import javax.annotation.Resource;
import net.sf.jasperreports.engine.JRParameter;
import org.apache.commons.io.FileUtils;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.context.support.MessageSourceResourceBundle;

import static org.hamcrest.Matchers.hasSize;
import static org.junit.Assert.assertThat;

/**
 *
 * @author Monte Cillo Co (mco@exist.com)
 */
public class PointsYearlySummaryReportTest extends AbstractPointSummaryReportTest {

    private static final Logger logger = LoggerFactory.getLogger(PointsYearlySummaryReportTest.class);
    private static final String TARGER_YEAR = "2014";
    @Resource(name = "yearlyPointsSummaryReport")
    private ReportTemplate yearlyPointsSummaryReport;

    @Test
    @Override
    public void testCreateJrProcessor() throws ReportException {
	final Map<String, String> param = new ImmutableMap.Builder<String, String>()
		.put(PointsSummaryReport.Yearly.REPORT_FILTER_YEAR, TARGER_YEAR)
		.build();

	JRProcessor jrProcessor = yearlyPointsSummaryReport.createJrProcessor(param);
	File testClassesDir = FileUtils.toFile(Thread.currentThread().getContextClassLoader().getResource(""));
	File output = new File(testClassesDir, "test-points-summary-yearly-report-output.pdf");
	logger.debug("Output location {}", output.getAbsolutePath());
	output.delete();
	Locale locale = LocaleContextHolder.getLocale();
	jrProcessor.addParameter(JRParameter.REPORT_LOCALE, locale);
	jrProcessor.addParameter(JRParameter.REPORT_RESOURCE_BUNDLE, new MessageSourceResourceBundle(messageSourceRef, locale));
	reportService.exportPdfReport(jrProcessor, output);

	double bytes = output.length();
	double kilobytes = (bytes / 1024);
	logger.debug("Kilobytes: {}", kilobytes);
    }

    @Test
    public void getCollectionDatasourceTest() {
	final Map<String, Object> param = new ImmutableMap.Builder<String, Object>()
		.put(PointsSummaryReport.Yearly.REPORT_FILTER_YEAR, TARGER_YEAR)
		.build();
	PointsSummaryReport psr = (PointsSummaryReport) yearlyPointsSummaryReport;
	Collection<?> ds = psr.getCollectionDatasource(param);
//	assertThat(ds, hasSize(11)); // real records is 10, since jasper read the first record we simply add an empty record at index 0
	// TODO: verbose checking of collection
    }
}