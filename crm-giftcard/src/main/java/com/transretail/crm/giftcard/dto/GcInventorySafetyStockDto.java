package com.transretail.crm.giftcard.dto;

/**
 * @author Allan G. Ramirez (agramirez@exist.com)
 */
public class GcInventorySafetyStockDto {
    private String productCode;
    private String productDesc;
    private long inventoryCount;
    private long safetyStockQty;
    private String currMonth;
    private String nextMonth;

    public String getProductCode() {
        return productCode;
    }

    public void setProductCode(String productCode) {
        this.productCode = productCode;
    }

    public String getProductDesc() {
        return productDesc;
    }

    public void setProductDesc(String productDesc) {
        this.productDesc = productDesc;
    }

    public long getInventoryCount() {
        return inventoryCount;
    }

    public void setInventoryCount(long inventoryCount) {
        this.inventoryCount = inventoryCount;
    }

    public long getSafetyStockQty() {
        return safetyStockQty;
    }

    public void setSafetyStockQty(long safetyStockQty) {
        this.safetyStockQty = safetyStockQty;
    }

    public String getCurrMonth() {
        return currMonth;
    }

    public void setCurrMonth(String currMonth) {
        this.currMonth = currMonth;
    }

    public String getNextMonth() {
        return nextMonth;
    }

    public void setNextMonth(String nextMonth) {
        this.nextMonth = nextMonth;
    }
}
