package com.transretail.crm.media.service;


import com.transretail.crm.core.dto.MessageDto;


public interface SmsService {

	boolean send(MessageDto dto);

}
