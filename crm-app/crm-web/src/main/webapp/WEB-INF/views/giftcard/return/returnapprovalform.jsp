<%@ include file="../../common/taglibs.jsp" %>

<style type="text/css">
<!--

  #productTable tr td:last-child {
    border-left: 0px;
  }

  #addProductBtn.btn-small {
    margin-bottom: 5px;
  }
  table select, textarea, table input[type="text"], input[type="password"], input[type="datetime"], input[type="datetime-local"], input[type="date"], input[type="month"], input[type="time"], input[type="week"], input[type="number"], input[type="email"], input[type="url"], input[type="search"], input[type="tel"], input[type="color"], .uneditable-input {
      margin-bottom: 0px;
      margin-top: 0px;
  }
  
-->
</style>



<c:set var="ENCTYPE" value="application/x-www-form-urlencoded"/>

<div class="modal-dialog">
  <div class="modal-content">
    
    <div class="modal-header">
      <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
      <h4><spring:message code="gc_return"/></h4>
    </div>
 
    <div class="modal-body">
    
      <div id="contentError" class="hide alert alert-error">
        <button type="button" class="close" onclick="$(this).parent().hide();">&times;</button>
        <div><form:errors path="*"/></div>
      </div>
    
      
      <form:form id="returnForm" name="returnForm" modelAttribute="returnForm" action="${pageContext.request.contextPath}/giftcard/return/approve/" method="POST" enctype="${ENCTYPE}" cssClass="form-reset form-horizontal" >
      <form:hidden path="id"/>
      
      <div class="contentMain">
      
      <div class="row-fluid">
      
        <div class="span6">
        <div class="control-group">
          <spring:message code="gc_return_no" var="returnNo" />
          <label for="recordNo" class="control-label">${returnNo}</label> 
          <div class="controls">
            <form:input path="recordNo" class="form-control" placeholder="${returnNo}" disabled="true" />
          </div>
        </div>
        
        <div class="control-group">
          <spring:message code="gc_return_date" var="dateReturned" />
          <label for="returnDate" class="control-label">${dateReturned}</label> 
          <div class="controls">
            <form:input path="returnDate" class="form-control" placeholder="${dateReturned}" disabled="true" />
          </div>
        </div>
        
        
        
        
        </div>
        
        <div class="span6">
        
        <div class="control-group">
          <spring:message code="gc_return_customer" var="customer" />
          <label for="customerId" class="control-label">${customer}</label> 
          <div class="controls">
            <form:select path="customerId" class="form-control" disabled="true">
            <form:option value="" label="" />
            <form:options items="${customers}" itemLabel="name" itemValue="id" />
            </form:select>
          </div>
        </div>
        
        <div class="control-group">
          <spring:message code="gc_return_order_no" var="salesOrder" />
          <label for="orderNo" class="control-label">${salesOrder}</label> 
          <div class="controls">
            <form:input path="orderNo" class="form-control" placeholder="${salesOrder}" disabled="true" />
          </div>
        </div>
        
        
        
        </div>
        
        
      </div>
      
      
      
      
          
      </div>
      
      </form:form>
    
    </div>
    <div class="modal-footer">
      <button type="button" data-status="APPROVED" class="orderSubmit btn btn-primary"><spring:message code="label_approve" /></button>
      <button type="button" data-status="REJECTED" class="orderSubmit btn btn-primary"><spring:message code="label_reject" /></button>
      <button type="button" id="orderCancel" class="btn btn-default" data-dismiss="modal"><spring:message code="label_cancel"/></button>
    </div>
  </div>
</div>

<script>
$(document).ready(function() {
  
  var CONTAINER_ERROR = "#contentError > div";
  var CONTENT_ERROR = "#contentError";
  $form = $("#returnForm");
  $formDialog = $("#formDialog").css({"width": "900px"});
  
  $(".orderSubmit").click(submitForm);
  
  function submitForm() {
    $(".orderSubmit").prop( "disabled", true );
    $.post($form.attr("action") + $(this).data("status"), $form.serialize(), function(data) {
      $(".orderSubmit").prop( "disabled", false );
      if (data.success) {
        location.reload();
      } 
      else {
        errorInfo = "";
        for (i = 0; i < data.result.length; i++) {
          errorInfo += "<br>" + (i + 1) + ". "
              + ( data.result[i].code != undefined ? 
                  data.result[i].code : data.result[i]);
        }
        $formDialog.find(CONTAINER_ERROR).html("Please correct following errors: " + errorInfo);
        $formDialog.find(CONTENT_ERROR).show('slow');
      }
    }); 
  }
  
  	$("input[name='returnDate']")
	.datepicker({
		format: 'dd-mm-yyyy',
		autoclose: true
	})
	.datepicker("setEndDate", new Date())
	.datepicker("setDate", new Date());
  
    var $productTable = $('#productTable');
	
	var  $productTbody = $("tbody", $productTable);
	
	$('#removeProductBtn', $formDialog ).click(function() {
        $("input.productRemoveFlag:checked").each(function() {
            var $tr = $(this).closest("tr");
            var length = $productTbody.find("tr").length;
            if($tr.is(":first-child") && length == 1) {
                $tr.find("input.productRemoveFlag").prop('checked', false);
                $tr.find("input").removeAttr('value').prop('disabled', true);
                $tr.find("select").removeAttr('value').prop('disabled', true);
                $tr.invisible();
            } else
                $tr.remove();
        });
    });

    $('#addProductBtn', $formDialog ).click(function() {
        var $tr = $productTbody.find("tr:first-child");
        if($tr.css('visibility') == 'hidden' || $tr.css('display') == 'none') {
            $tr.find("input").removeAttr('value').prop('disabled', false);
            $tr.find("select").removeAttr('value').prop('disabled', false);
            $tr.visible();
        } else {
            var $trLast = $productTbody.find("tr:last-child");
            var  $row = $tr.clone();
            $row.find("input").removeAttr('value').prop('disabled', false);
            $row.find("select").removeAttr('value').prop('disabled', false);
            var index = +$trLast.data("index") + 1;
            var lastIndex = $tr.data("index");
            $row.data("index", index);
            var rg = new RegExp("\\["+lastIndex+"\\]", 'g');
            var html = $row.html();
            html = html.replace(rg, '['+index+']');
            $row.html(html);

            $productTbody.append($row);
            initFields($row);
        }
    });
    
    initFields($formDialog);
	
	jQuery.fn.visible = function() {
	    return this.css('visibility', 'visible');
	};

	jQuery.fn.invisible = function() {
	    return this.css('visibility', 'hidden');
	};
	
	function initFields(e) {
    	$('.quantity', $(e)).numberInput({"type" : "int"});
        $('.intInput', $(e)).numberInput({"type" : "int"});
    	$('.floatInput', $(e)).numberInput({"type" : "float"});
    	$(".productId", $(e)).change(function() {
    		$(".faceValue", $(this).closest("tr")).val($("option:selected", $(this)).data("faceValue"));
    	});
    	$('.modalQuantity, .modalStartingSeries', $(e)).keyup(populateEndingSeries);
    }
	
	function populateEndingSeries() {
		var startSeries = $(this).closest("tr").find(".modalStartingSeries").val();
		var quantity = $(this).closest("tr").find(".modalQuantity").val();
		if(startSeries != null && quantity != null && startSeries != "" && quantity != "") {
			var temp = +startSeries + +quantity - 1;
			var endingSeries = temp.toString();
			var padZero = 12 - endingSeries.length;
			if(padZero > 0) {
				for(var i = 0; i < padZero; i++)
					endingSeries = "0" + endingSeries;
			}
			$(this).closest("tr").find(".modalEndingSeries").val(endingSeries);
		}
	}
  
});
</script>