package com.transretail.crm.giftcard.dto;

import com.transretail.crm.core.entity.CrmForPeoplesoftConfig.TransactionType;

/**
 *
 * @author Monte Cillo Co (mco@exist.com)
 */
public class AffiliateDto {

    private final String bu;
    private final TransactionType transactionType;
    private final double amount;

    public AffiliateDto(String bu, TransactionType transactionType, double amount) {
        this.bu = bu;
        this.transactionType = transactionType;
        this.amount = amount;
    }
    public AffiliateDto(String bu, String transactionType, double amount) {
        this.bu = bu;
        this.transactionType = TransactionType.valueOf(transactionType);
        this.amount = amount;
    }

    public String getBu() {
        return bu;
    }

    public TransactionType getTransactionType() {
        return transactionType;
    }

    public double getAmount() {
        return amount;
    }

}
