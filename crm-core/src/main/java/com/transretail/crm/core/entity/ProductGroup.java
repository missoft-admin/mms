package com.transretail.crm.core.entity;


import java.io.Serializable;
import java.util.Arrays;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.apache.commons.lang3.StringUtils;
import org.hibernate.annotations.Type;
import org.hibernate.envers.Audited;
import org.hibernate.envers.NotAudited;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.transretail.crm.core.dto.ProductHierDto;
import com.transretail.crm.core.entity.lookup.LookupDetail;
import com.transretail.crm.core.entity.lookup.Product;
import com.transretail.crm.core.entity.support.CustomAuditableEntity;

@Audited
@Table(name = "CRM_PRODUCT_GRP")
@Entity
public class ProductGroup extends CustomAuditableEntity<Long> implements Serializable {
	private static final long serialVersionUID = 1L;

	public static final String PRODUCT_LIST_SEPARATOR = ",";

    @Column(name = "NAME")
    private String name; 

    @Column(name="BRAND")
    private String brand;

    @JoinColumn(name="CLASSIFICATION")
    @ManyToOne(fetch = FetchType.LAZY)
    // Added fetch = FetchType.LAZY because classification is not needed in CrmProductHierUpdaterServiceImpl
    // and will cause performance issue
    private LookupDetail classification;

    @JoinColumn(name="SUBCLASSIFICATION")
    // Added fetch = FetchType.LAZY because subclassification is not needed in CrmProductHierUpdaterServiceImpl
    // and will cause performance issue
    @ManyToOne(fetch = FetchType.LAZY)
    private LookupDetail subclassification;

    @NotAudited
    @JsonIgnore
    @OneToMany(mappedBy = "productGroup", orphanRemoval = true, cascade = CascadeType.ALL)
    private List<PromotionProductGroup> promotionProductGroups;

    @Column(name = "PRODUCTS", length = 4000)
    private String products;

    @Column(name = "PRODUCTS_CFNS", length = 4000)
    private String productsCfns;

    @Column(name="EXC_PRODUCTS", length = 4000)
    private String excludedProducts;

    @Column(name = "VALID", length = 1)
    @Type(type = "yes_no")
    private Boolean valid = Boolean.TRUE;

    @Transient
    private List<Product> productModels;

    @Transient
    private List<ProductHierDto> productClassifications;

    
    public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

	public String getProducts() {
		return products;
	}

	public void setProducts(String products) {
		this.products = products;
	}

	public List<String> getProductList() {
		if(StringUtils.isNotBlank(products))
            return Arrays.asList(StringUtils.split(products, PRODUCT_LIST_SEPARATOR));
        else
			return null;
	}

	public List<String> getProductCfnList() {
		if(StringUtils.isNotBlank(productsCfns))
            return Arrays.asList(StringUtils.split(productsCfns, PRODUCT_LIST_SEPARATOR));
        else
			return null;
	}

	public List<String> getExcProductList() {
		if(StringUtils.isNotBlank(excludedProducts))
            return Arrays.asList(StringUtils.split(excludedProducts, PRODUCT_LIST_SEPARATOR));
        else
			return null;
	}

	public LookupDetail getClassification() {
		return classification;
	}

	public void setClassification(LookupDetail classification) {
		this.classification = classification;
	}

	public LookupDetail getSubclassification() {
		return subclassification;
	}

	public void setSubclassification(LookupDetail subclassification) {
		this.subclassification = subclassification;
	}

	public List<Product> getProductModels() {
		return productModels;
	}

	public void setProductModels(List<Product> productModels) {
		this.productModels = productModels;
	}

	public List<PromotionProductGroup> getPromotionProductGroups() {
		return promotionProductGroups;
	}

	public void setPromotionProductGroups(
			List<PromotionProductGroup> promotionProductGroups) {
		this.promotionProductGroups = promotionProductGroups;
	}

	public String getProductsCfns() {
		return productsCfns;
	}

	public void setProductsCfns(String productsCfns) {
		this.productsCfns = productsCfns;
	}

	public String getExcludedProducts() {
		return excludedProducts;
	}

	public void setExcludedProducts(String excludedProducts) {
		this.excludedProducts = excludedProducts;
	}

	public Boolean isValid() {
		return valid;
	}

	public void setValid(Boolean valid) {
		this.valid = valid;
	}

}
