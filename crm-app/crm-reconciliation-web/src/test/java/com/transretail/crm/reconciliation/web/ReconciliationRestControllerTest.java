package com.transretail.crm.reconciliation.web;

import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.argThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.reset;
import static org.mockito.Mockito.when;
import static org.springframework.http.MediaType.APPLICATION_JSON;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.sql.Timestamp;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentMatcher;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.PropertyPlaceholderConfigurer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.AnnotationConfigWebContextLoader;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.transretail.crm.common.service.exception.MessageSourceResolvableException;
import com.transretail.crm.common.web.RestConstants;
import com.transretail.crm.devtool.dto.ColumnMetadataBean;
import com.transretail.crm.devtool.dto.TableMetadataBean;
import com.transretail.crm.reconciliation.service.DataSyncService;
import com.transretail.crm.reconciliation.service.DiscrepancyToolService;
import com.transretail.crm.web.controller.AbstractMockMvcTest;

/**
 * @author Allan G. Ramirez (agramirez@exist.com)
 */
@RunWith(SpringJUnit4ClassRunner.class)
@WebAppConfiguration
@ContextConfiguration(loader = AnnotationConfigWebContextLoader.class)
public class ReconciliationRestControllerTest extends AbstractMockMvcTest {
    @Configuration
    @EnableWebMvc
    static class Config {
        @Bean
        public ReconciliationRestController reconciliationRestController() {
            return new ReconciliationRestController();
        }

        @Bean
        public DiscrepancyToolService discrepancyToolService() {
            return mock(DiscrepancyToolService.class);
        }

        @Bean
        public DataSyncService dataSyncService() {
            return mock(DataSyncService.class);
        }

        @Bean
        public PropertyPlaceholderConfigurer propertyPlaceholderConfigurer() {
            PropertyPlaceholderConfigurer propertyPlaceholderConfigurer = new PropertyPlaceholderConfigurer();
            Properties properties = new Properties();
            properties.setProperty("crm.proxy.host", "localhost");
            properties.setProperty("crm.proxy.port", "8080");
            propertyPlaceholderConfigurer.setProperties(properties);
            return propertyPlaceholderConfigurer;
        }
    }

    @Autowired
    private DiscrepancyToolService discrepancyToolService;
    @Autowired
    private DataSyncService dataSyncService;

    @Override
    public void doSetup() {
        reset(discrepancyToolService);
        reset(dataSyncService);
    }

    @Test
    public void getLocalDbMissingRecords() throws Exception {
        final String tableName = "TEST_TABLE";
        ObjectMapper mapper = new ObjectMapper();
        List<TableMetadataBean> beans = Lists.newArrayList();
        TableMetadataBean bean = new TableMetadataBean();
        bean.setTable(tableName);
        bean.setId("ID");
        beans.add(bean);

        Map<String, List<ColumnMetadataBean>> expectedResult = Maps.newHashMap();
        Timestamp now = new Timestamp(System.currentTimeMillis());
        expectedResult.put(tableName, Arrays.asList(new ColumnMetadataBean("1", now.toString())));
        when(discrepancyToolService.getLocalDbMissingRecords(argThat(new ArgumentMatcher<List<TableMetadataBean>>() {
            @Override
            public boolean matches(Object argument) {
                List<TableMetadataBean> bean = (List<TableMetadataBean>) argument;
                return bean != null && bean.size() == 1 && bean.get(0).getTable().equals(tableName);
            }
        }))).thenReturn(expectedResult);

        String content = mockMvc.perform(post("/localdb/missing/records").content(mapper.writeValueAsBytes(beans))
            .contentType(APPLICATION_JSON)).andExpect(status().isOk())
            .andReturn().getResponse().getContentAsString();
        Map<String, List<ColumnMetadataBean>> actualResult =
            mapper.readValue(content, new TypeReference<HashMap<String, List<ColumnMetadataBean>>>() {
            });
        List<ColumnMetadataBean> result = actualResult.get(tableName);
        assertEquals(1, result.size());
        assertEquals("1", result.get(0).getId());
        assertEquals(now.toString(), result.get(0).getCreatedDateTime());
    }

    @Test
    public void getLocalDbMissingRecordsWithError() throws Exception {
        final String tableName = "TEST_TABLE";
        ObjectMapper mapper = new ObjectMapper();
        List<TableMetadataBean> beans = Lists.newArrayList();
        TableMetadataBean bean = new TableMetadataBean();
        bean.setTable(tableName);
        bean.setId("ID");
        beans.add(bean);
        when(discrepancyToolService.getLocalDbMissingRecords(argThat(new ArgumentMatcher<List<TableMetadataBean>>() {
            @Override
            public boolean matches(Object argument) {
                List<TableMetadataBean> bean = (List<TableMetadataBean>) argument;
                return bean != null && bean.size() == 1 && bean.get(0).getTable().equals(tableName);
            }
        }))).thenThrow(new MessageSourceResolvableException("test.code", null, "TEST ERROR"));

        String content = mockMvc.perform(post("/localdb/missing/records").content(mapper.writeValueAsBytes(beans))
            .contentType(APPLICATION_JSON)).andExpect(status().isOk())
            .andReturn().getResponse().getContentAsString();
        Map<String, List<ColumnMetadataBean>> actualResult =
            mapper.readValue(content, new TypeReference<HashMap<String, List<ColumnMetadataBean>>>() {
            });
        List<ColumnMetadataBean> result = actualResult.get(RestConstants.KEY_REST_ERROR.toString());
        assertEquals(1, result.size());
        assertEquals("TEST ERROR", result.get(0).getException().getDefaultMessage());
    }

    @Test
    public void getMainDbMissingRecords() throws Exception {
        final String tableName = "TEST_TABLE";
        ObjectMapper mapper = new ObjectMapper();
        List<TableMetadataBean> beans = Lists.newArrayList();
        TableMetadataBean bean = new TableMetadataBean();
        bean.setTable(tableName);
        bean.setId("ID");
        beans.add(bean);

        Map<String, List<ColumnMetadataBean>> expectedResult = Maps.newHashMap();
        Timestamp now = new Timestamp(System.currentTimeMillis());
        expectedResult.put(tableName, Arrays.asList(new ColumnMetadataBean("1", now.toString())));
        when(discrepancyToolService.getMainDbMissingRecords(argThat(new ArgumentMatcher<List<TableMetadataBean>>() {
            @Override
            public boolean matches(Object argument) {
                List<TableMetadataBean> bean = (List<TableMetadataBean>) argument;
                return bean != null && bean.size() == 1 && bean.get(0).getTable().equals(tableName);
            }
        }))).thenReturn(expectedResult);

        String content = mockMvc.perform(post("/maindb/missing/records").content(mapper.writeValueAsBytes(beans))
            .contentType(APPLICATION_JSON)).andExpect(status().isOk())
            .andReturn().getResponse().getContentAsString();
        Map<String, List<ColumnMetadataBean>> actualResult =
            mapper.readValue(content, new TypeReference<HashMap<String, List<ColumnMetadataBean>>>() {
            });
        List<ColumnMetadataBean> result = actualResult.get(tableName);
        assertEquals(1, result.size());
        assertEquals("1", result.get(0).getId());
        assertEquals(now.toString(), result.get(0).getCreatedDateTime());
    }

    @Test
    public void getMainDbMissingRecordsWithError() throws Exception {
        final String tableName = "TEST_TABLE";
        ObjectMapper mapper = new ObjectMapper();
        List<TableMetadataBean> beans = Lists.newArrayList();
        TableMetadataBean bean = new TableMetadataBean();
        bean.setTable(tableName);
        bean.setId("ID");
        beans.add(bean);
        when(discrepancyToolService.getMainDbMissingRecords(argThat(new ArgumentMatcher<List<TableMetadataBean>>() {
            @Override
            public boolean matches(Object argument) {
                List<TableMetadataBean> bean = (List<TableMetadataBean>) argument;
                return bean != null && bean.size() == 1 && bean.get(0).getTable().equals(tableName);
            }
        }))).thenThrow(new MessageSourceResolvableException("test.code", null, "TEST ERROR"));

        String content = mockMvc.perform(post("/maindb/missing/records").content(mapper.writeValueAsBytes(beans))
            .contentType(APPLICATION_JSON)).andExpect(status().isOk())
            .andReturn().getResponse().getContentAsString();
        Map<String, List<ColumnMetadataBean>> actualResult =
            mapper.readValue(content, new TypeReference<HashMap<String, List<ColumnMetadataBean>>>() {
            });
        List<ColumnMetadataBean> result = actualResult.get(RestConstants.KEY_REST_ERROR.toString());
        assertEquals(1, result.size());
        assertEquals("test.code", result.get(0).getException().getLastCode());
        assertEquals("TEST ERROR", result.get(0).getException().getDefaultMessage());
    }

}