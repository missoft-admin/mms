package com.transretail.crm.giftcard.service.impl;

import com.googlecode.catchexception.CatchException;
import com.googlecode.catchexception.apis.CatchExceptionBdd;
import com.transretail.crm.core.security.model.impl.CustomSecurityUserDetailsImpl;
import com.transretail.crm.core.security.util.UserUtil;
import com.transretail.crm.giftcard.entity.GiftCardInventory;
import com.transretail.crm.giftcard.entity.GiftCardServiceRequest;
import com.transretail.crm.giftcard.repo.GiftCardServiceRequestRepo;
import com.transretail.crm.giftcard.service.GiftCardServiceRequestService;
import java.util.ArrayList;
import java.util.List;
import org.fest.assertions.core.Condition;
import org.joda.time.DateTime;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.*;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import static org.fest.assertions.api.Assertions.assertThat;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.when;

/**
 *
 * @author Monte Cillo Co (mco@exist.com)
 */
@RunWith(PowerMockRunner.class)
@PrepareForTest(UserUtil.class)
public class GiftCardServiceRequestServiceImplTest {

    @Mock
    private GiftCardServiceRequestRepo giftCardServiceRequestRepo;
    @InjectMocks
    private final static GiftCardServiceRequestService sut = new GiftCardServiceRequestServiceImpl();
    private static final DateTime FIXED_DATE_TIME = new DateTime(2014, 05, 30, 17, 45, 34);

    @Before
    public void setup() {
	CustomSecurityUserDetailsImpl user = new CustomSecurityUserDetailsImpl();
	user.setUsername("JUNIT");
	PowerMockito.mockStatic(UserUtil.class);
	when(UserUtil.getCurrentUser()).thenReturn(user);

	MockitoAnnotations.initMocks(this);
    }

    @Test
    public void shouldSuccessfullyFileANewServiceRequest() {
	GiftCardInventory giftcard = new GiftCardInventory();
	giftcard.setBarcode("123123123123123");
	String requestDetails = "Sample Request";
	GiftCardServiceRequest giftCardServiceRequest = new GiftCardServiceRequest();
	giftCardServiceRequest.setFiledBy("JUNIT");

	when(giftCardServiceRequestRepo.save(any(GiftCardServiceRequest.class)))
		.thenReturn(giftCardServiceRequest);

	sut.fileNewServiceRequest(giftcard, null, requestDetails);

	ArgumentCaptor<GiftCardServiceRequest> captor = ArgumentCaptor.forClass(GiftCardServiceRequest.class);
	Mockito.verify(giftCardServiceRequestRepo).save(captor.capture());
	GiftCardServiceRequest capturedValue = captor.getValue();
	assertThat(capturedValue).isNotNull().has(new Condition<GiftCardServiceRequest>() {

	    @Override
	    public boolean matches(GiftCardServiceRequest value) {
		return value.getFiledBy().equals("JUNIT");
	    }
	});

	PowerMockito.verifyStatic();
    }

    @Test
    public void fileNewServiceRequest_shouldThrownAnIAEWhenGiftCardArgumentIsNull() {
	CatchExceptionBdd.when(sut).fileNewServiceRequest(null, null, "details");
	CatchExceptionBdd.then(CatchException.caughtException())
		.isInstanceOf(IllegalArgumentException.class)
		.hasMessage("GiftCardInventory argument is required; it must not be null");
	Mockito.verifyZeroInteractions(giftCardServiceRequestRepo);
    }

    @Test
    public void fileNewServiceRequest_shouldThrownAnIEAWhenDetailsIsNullOrEmpty() {
	CatchExceptionBdd.when(sut).fileNewServiceRequest(new GiftCardInventory(),null,  "");
	CatchExceptionBdd.then(CatchException.caughtException())
		.isInstanceOf(IllegalArgumentException.class)
		.hasMessage("Details argument is required; it must not be null or empty");

	Mockito.verifyZeroInteractions(giftCardServiceRequestRepo);
    }

    @Test
    public void shouldRetrieveAllServiceRequestOfSuppliedGiftCard() {
	List<GiftCardServiceRequest> expectedResults = new ArrayList<GiftCardServiceRequest>();
	when(giftCardServiceRequestRepo.findAllServiceRequestOf(any(GiftCardInventory.class)))
		.thenReturn(expectedResults);
	GiftCardInventory giftcard = new GiftCardInventory();
	giftcard.setBarcode("123123123123123");

	List<GiftCardServiceRequest> results = sut.findAllServiceRequestOf(giftcard);
	assertThat(results).isEqualTo(expectedResults);
	Mockito.verify(giftCardServiceRequestRepo).findAllServiceRequestOf(any(GiftCardInventory.class));
	Mockito.verifyNoMoreInteractions(giftCardServiceRequestRepo);
    }

    @Test
    public void findAllServiceRequestOf_shouldThrownAnIAEWhenGiftCardArgumentIsNull() {
	CatchExceptionBdd.when(sut).findAllServiceRequestOf(null);
	CatchExceptionBdd.then(CatchException.caughtException())
		.isInstanceOf(IllegalArgumentException.class)
		.hasMessage("GiftCardInventory argument is required; it must not be null");
	Mockito.verifyZeroInteractions(giftCardServiceRequestRepo);
    }

}
