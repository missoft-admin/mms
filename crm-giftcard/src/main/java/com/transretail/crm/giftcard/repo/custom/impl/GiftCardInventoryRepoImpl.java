package com.transretail.crm.giftcard.repo.custom.impl;


import com.mysema.query.jpa.impl.JPAQuery;

import com.transretail.crm.giftcard.entity.*;

import com.transretail.crm.giftcard.repo.custom.GiftCardInventoryRepoCustom;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

public class GiftCardInventoryRepoImpl implements GiftCardInventoryRepoCustom {

    @PersistenceContext
    private EntityManager em;

    @Override
    public boolean isGiftCardForPromo(String barcode) {

        QGiftCardInventory qtx = QGiftCardInventory.giftCardInventory;
        return new JPAQuery(em).from(qtx).where(qtx.barcode.eq(barcode).and(qtx.isForPromo.isTrue())).exists();
    }




}
