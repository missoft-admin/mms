package com.transretail.crm.report.template.impl;

import com.google.common.collect.ImmutableMap;
import com.transretail.crm.common.reporting.exception.ReportException;
import com.transretail.crm.common.reporting.jasper.JRProcessor;
import com.transretail.crm.report.template.ReportTemplate;
import java.io.File;
import java.util.Locale;
import java.util.Map;
import javax.annotation.Resource;
import net.sf.jasperreports.engine.JRParameter;
import org.apache.commons.io.FileUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.context.support.MessageSourceResourceBundle;

/**
 *
 * @author Monte Cillo Co (mco@exist.com)
 */
public class PointsMonthlySummaryTest extends AbstractPointSummaryReportTest {

    private static final Logger logger = LoggerFactory.getLogger(PointsMonthlySummaryTest.class);
    @Resource(name = "monthlyPointSummaryReport")
    private ReportTemplate monthlyPointSummaryReport;

    @Override
    public void testCreateJrProcessor() throws ReportException {
	final Map<String, String> param = new ImmutableMap.Builder<String, String>()
		.put(PointsSummaryReport.Monthly.FILTER_DATE_FROM, "December-2013")
		.put(PointsSummaryReport.Monthly.FILTER_DATE_TO, "December-2014")
		.build();

	JRProcessor jrProcessor = monthlyPointSummaryReport.createJrProcessor(param);
	File testClassesDir = FileUtils.toFile(Thread.currentThread().getContextClassLoader().getResource(""));
	File output = new File(testClassesDir, "test-points-summary-monthly-report-output.pdf");
	logger.debug("Output location {}", output.getAbsolutePath());
	output.delete();
	Locale locale = LocaleContextHolder.getLocale();
	jrProcessor.addParameter(JRParameter.REPORT_LOCALE, locale);
	jrProcessor.addParameter(JRParameter.REPORT_RESOURCE_BUNDLE, new MessageSourceResourceBundle(messageSourceRef, locale));
	reportService.exportPdfReport(jrProcessor, output);

	double bytes = output.length();
	double kilobytes = (bytes / 1024);
	logger.debug("Kilobytes: {}", kilobytes);
    }

}
