    create table CRM_GC_PHYSICAL_COUNT (
        id number(19,0) not null,
        CREATED_BY varchar2(255 char),
        CREATED_DATETIME timestamp,
        LAST_UPDATED_BY varchar2(255 char),
        LAST_UPDATED_DATETIME timestamp,
        ENDING_SERIES varchar2(16 char) not null,
        LOCATION varchar2(255 char) not null,
        PRODUCT_PROFILE varchar2(255 char),
        QUANTITY number(19,0) not null,
        STARTING_SERIES varchar2(16 char) not null,
        primary key (id)
    );

    create table CRM_GC_PHYSICAL_COUNT_SUMMARY (
        ID number(19,0) not null,
        CREATED_BY varchar2(255 char) not null,
        CREATED_DATETIME timestamp not null,
        CURRENT_INVENTORY number(19,0),
        LOCATION varchar2(255 char) not null,
        PHYSICAL_COUNT number(19,0),
        PRODUCT_PROFILE varchar2(255 char) not null,
        primary key (ID)
    );