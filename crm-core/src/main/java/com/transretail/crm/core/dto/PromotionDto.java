package com.transretail.crm.core.dto;


import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
//import org.springframework.beans.BeanUtils;

import com.google.common.collect.Maps;
import com.transretail.crm.common.util.DateUtil;
import com.transretail.crm.core.entity.Promotion;
import com.transretail.crm.core.entity.PromotionMemberGroup;
import com.transretail.crm.core.entity.PromotionPaymentType;
import com.transretail.crm.core.entity.PromotionProductGroup;
import com.transretail.crm.core.entity.PromotionRedemption;
import com.transretail.crm.core.entity.PromotionReward;
import com.transretail.crm.core.entity.PromotionStoreGroup;
import com.transretail.crm.core.entity.enums.ModelType;
import com.transretail.crm.core.entity.lookup.LookupDetail;
import com.transretail.crm.core.util.CalendarUtil;


public class PromotionDto {

//	private static final String[] IGNORE = { "rewardType", "campaign", "status", "promotionRewards",
//		"targetMemberGroups", "targetProductGroups", "targetStoreGroups", "targetPaymentTypes",
//		"startDate", "endDate", "startTime", "endTime", "createUser" };

	private Long id;
	private String status;
	private String name;
    private String description;
    private Date startDate;
    private Date endDate;
    private String dispStartDate;
    private String dispEndDate;
    private String startTime;
    private String endTime;
    private String createUser;
    private String createUserFirstName;
    private String createUserLastName;

	private Long program;
	private Long campaign;

	private String affectedDays;
	private List<String> promotionDays;

	private String rewardType;
    private List<PromotionRewardDto> promotionRewards;
    private List<PromotionRedemptionDto> promotionRedemptions;
	
	private List<Long> targetMemberGroups;
	private List<Long> targetProductGroups;
	private List<Long> targetStoreGroups;
	private List<String> targetPaymentTypes;
	private Map<String, String> targetWildcards;

	private ApprovalRemarkDto remarks;

	private Long dispCreated;
	
	public PromotionDto() {}
	public PromotionDto( Promotion inPromotion ) {
//		BeanUtils.copyProperties( inPromotion, this, IGNORE );
        id = inPromotion.getId();
        if ( null != inPromotion.getStatus() ) {
            status = inPromotion.getStatus().getCode();
        }
		name = inPromotion.getName();
		description = inPromotion.getDescription();

		if ( null != inPromotion.getStartDate() ) {
			startDate = new Date(inPromotion.getStartDate().getTime());
			dispStartDate = CalendarUtil.INSTANCE.getDateFormatter().print( startDate.getTime() );
		}
		if ( null != inPromotion.getEndDate() ) {
			endDate = new Date(inPromotion.getEndDate().getTime());
			dispEndDate = CalendarUtil.INSTANCE.getDateFormatter().print( endDate.getTime() );
		}
        if ( null != inPromotion.getStartTime() ) {
            startTime = inPromotion.getStartTime().toString("HH:mm:ss");
        }
        if ( null != inPromotion.getEndTime() ) {
            endTime = inPromotion.getEndTime().toString("HH:mm:ss");
        }
        if ( StringUtils.isNotBlank( inPromotion.getCreateUser() ) ) {
            createUser = inPromotion.getCreateUser();
        }

        if ( null != inPromotion.getCampaign() ) {
            campaign = inPromotion.getCampaign().getId();
            program = inPromotion.getCampaign().getProgram().getId();
        }

        affectedDays = inPromotion.getAffectedDays();
        if ( StringUtils.isNotEmpty( inPromotion.getAffectedDays() ) ) {
            promotionDays = new ArrayList<String>();
            for ( String theDay : inPromotion.getAffectedDays().split( "," ) ) {
                promotionDays.add( theDay );
            }
        }

        if ( null != inPromotion.getRewardType() ) {
			rewardType = inPromotion.getRewardType().getCode();
		}
		if ( null != inPromotion.getPromotionRewards() ) {
			promotionRewards = new ArrayList<PromotionRewardDto>();
			for ( PromotionReward theReward : inPromotion.getPromotionRewards() ) {
				promotionRewards.add( new PromotionRewardDto( theReward ) );
			}
		}
		
		if ( null != inPromotion.getPromotionRedemptions() ) {
			promotionRedemptions = new ArrayList<PromotionRedemptionDto>();
			for ( PromotionRedemption theReward : inPromotion.getPromotionRedemptions() ) {
				promotionRedemptions.add( new PromotionRedemptionDto( theReward ) );
			}
		}

		if ( null != inPromotion.getTargetMemberGroups() && !inPromotion.getTargetMemberGroups().isEmpty() ) {
			targetMemberGroups = new ArrayList<Long>();
			for (PromotionMemberGroup memberGroup : inPromotion.getTargetMemberGroups()) {
				targetMemberGroups.add(memberGroup.getMemberGroup().getId());
			}
		}
		if ( null != inPromotion.getTargetProductGroups() && !inPromotion.getTargetProductGroups().isEmpty() ) {
			targetProductGroups = new ArrayList<Long>();
			for (PromotionProductGroup productGroup : inPromotion.getTargetProductGroups()) {
				targetProductGroups.add(productGroup.getProductGroup().getId());
			}
		}
        if ( null != inPromotion.getTargetStoreGroups() && !inPromotion.getTargetStoreGroups().isEmpty() ) {
            targetStoreGroups = new ArrayList<Long>();
            for (PromotionStoreGroup storeGroup : inPromotion.getTargetStoreGroups()) {
                targetStoreGroups.add(storeGroup.getStoreGroup().getId());
            }
        }
		if ( null != inPromotion.getTargetPaymentTypes() && !inPromotion.getTargetPaymentTypes().isEmpty() ) {
			targetPaymentTypes = new ArrayList<String>();
			targetWildcards = Maps.newHashMap();
			for (PromotionPaymentType paymentTypes : inPromotion.getTargetPaymentTypes()) {
				targetPaymentTypes.add(paymentTypes.getLookupDetail().getCode()); 
				if(StringUtils.isNotBlank(paymentTypes.getWildcards()))
					targetWildcards.put(paymentTypes.getLookupDetail().getCode(), paymentTypes.getWildcards());
			}
		}

		remarks = new ApprovalRemarkDto();
		remarks.setModelId( inPromotion.getId() != null? inPromotion.getId().toString() : "" );
		remarks.setModelType( ModelType.PROMOTION.toString() );
		dispCreated = ( null != inPromotion.getCreated() )? inPromotion.getCreated().toDate().getTime() : null;
	}

	public Promotion toModel() {
		return toModel( new Promotion() );
	}

	public Promotion toModel( Promotion inModel ) {
		if ( null == inModel ) {
			inModel = new Promotion();
		}
//		BeanUtils.copyProperties( this, inModel, IGNORE );
        inModel.setName(this.name);
        inModel.setDescription(this.description);
		inModel.setStartDate( startDate );
		inModel.setEndDate( endDate );
		inModel.setStartTime(DateUtil.convertLocalTimeString(DateUtil.TIME_FORMAT, startTime));
		inModel.setEndTime(DateUtil.convertLocalTimeString(DateUtil.TIME_FORMAT, endTime));
		if ( CollectionUtils.isNotEmpty( promotionDays ) ) {
			inModel.setAffectedDays( StringUtils.join( promotionDays, "," ) );
		}
        if (StringUtils.isNotBlank(this.rewardType)) {
            inModel.setRewardType(new LookupDetail(this.rewardType));
        } else {
            inModel.setRewardType(null);
        }
        return inModel;
	}




	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Date getStartDate() {
		return startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	public Date getEndDate() {
		return endDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	public String getStartTime() {
		return startTime;
	}

	public void setStartTime(String startTime) {
		this.startTime = startTime;
	}

	public String getEndTime() {
		return endTime;
	}

	public void setEndTime(String endTime) {
		this.endTime = endTime;
	}

	public String getCreateUser() {
		return createUser;
	}

	public void setCreateUser(String createUser) {
		this.createUser = createUser;
	}

	public String getCreateUserFirstName() {
		return createUserFirstName;
	}
	public void setCreateUserFirstName(String createUserFirstName) {
		this.createUserFirstName = createUserFirstName;
	}
	public String getCreateUserLastName() {
		return createUserLastName;
	}
	public void setCreateUserLastName(String createUserLastName) {
		this.createUserLastName = createUserLastName;
	}
	public Long getProgram() {
		return program;
	}
	public void setProgram(Long program) {
		this.program = program;
	}
	public Long getCampaign() {
		return campaign;
	}
	public void setCampaign(Long campaign) {
		this.campaign = campaign;
	}
	public String getAffectedDays() {
		return affectedDays;
	}
	public void setAffectedDays(String affectedDays) {
		this.affectedDays = affectedDays;
	}
	public List<String> getPromotionDays() {
		return promotionDays;
	}
	public void setPromotionDays(List<String> promotionDays) {
		this.promotionDays = promotionDays;
	}
	public String getRewardType() {
		return rewardType;
	}
	public void setRewardType(String rewardType) {
		this.rewardType = rewardType;
	}
	public List<PromotionRewardDto> getPromotionRewards() {
		return promotionRewards;
	}
	public void setPromotionRewards(List<PromotionRewardDto> promotionRewards) {
		this.promotionRewards = promotionRewards;
	}
	public List<Long> getTargetMemberGroups() {
		return targetMemberGroups;
	}
	public void setTargetMemberGroups(List<Long> targetMemberGroups) {
		this.targetMemberGroups = targetMemberGroups;
	}
	public List<Long> getTargetProductGroups() {
		return targetProductGroups;
	}
	public void setTargetProductGroups(List<Long> targetProductGroups) {
		this.targetProductGroups = targetProductGroups;
	}
	public List<Long> getTargetStoreGroups() {
		return targetStoreGroups;
	}
	public void setTargetStoreGroups(List<Long> targetStoreGroups) {
		this.targetStoreGroups = targetStoreGroups;
	}
	
	public List<String> getTargetPaymentTypes() {
		return targetPaymentTypes;
	}

	public void setTargetPaymentTypes(List<String> targetPaymentTypes) {
		this.targetPaymentTypes = targetPaymentTypes;
	}

	public String getDispStartDate() {
		return dispStartDate;
	}

	public void setDispStartDate(String dispStartDate) {
		this.dispStartDate = dispStartDate;
	}

	public String getDispEndDate() {
		return dispEndDate;
	}

	public void setDispEndDate(String dispEndDate) {
		this.dispEndDate = dispEndDate;
	}

	public ApprovalRemarkDto getRemarks() {
		return remarks;
	}

	public void setRemarks(ApprovalRemarkDto remarks) {
		this.remarks = remarks;
	}

	public Long getDispCreated() {
		return dispCreated;
	}

	public void setDispCreated(Long dispCreated) {
		this.dispCreated = dispCreated;
	}

	public Map<String, String> getTargetWildcards() {
		return targetWildcards;
	}

	public void setTargetWildcards(Map<String, String> targetWildcards) {
		this.targetWildcards = targetWildcards;
	}

	public String getCreateUserFullName() {
		return ( StringUtils.isNotBlank( this.createUserFirstName )? this.createUserFirstName : "" )
			+ ( StringUtils.isNotBlank( this.createUserLastName )? " " + this.createUserLastName : "" );
	}
	public List<PromotionRedemptionDto> getPromotionRedemptions() {
		return promotionRedemptions;
	}
	public void setPromotionRedemptions(
			List<PromotionRedemptionDto> promotionRedemptions) {
		this.promotionRedemptions = promotionRedemptions;
	}
	

}
