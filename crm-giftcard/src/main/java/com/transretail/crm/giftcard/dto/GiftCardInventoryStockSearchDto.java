package com.transretail.crm.giftcard.dto;


import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.time.DateUtils;
import org.joda.time.DateTime;
import org.joda.time.LocalDate;
import org.joda.time.format.DateTimeFormat;

import com.mysema.query.types.expr.BooleanExpression;
import com.transretail.crm.common.service.dto.request.AbstractSearchFormDto;
import com.transretail.crm.common.util.DateUtil;
import com.transretail.crm.giftcard.entity.QGiftCardInventoryStock;


public class GiftCardInventoryStockSearchDto extends AbstractSearchFormDto {

	private static final String CREATED_DATE_FORMAT = "dd MMM yyyy";

    private String cardType;
    private String location;
    private Long createdFromMilli;
    private Long createdToMilli;
    private String createdFromStr;
    private String createdToStr;
    private DateTime dateCreated;
    private String vendorFormalName;

    public String getCardType() {
		return cardType;
	}

	public void setCardType(String cardType) {
		this.cardType = cardType;
	}

	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		this.location = location;
	}

	public Long getCreatedFromMilli() {
		return createdFromMilli;
	}

	public void setCreatedFromMilli(Long createdFromMilli) {
		this.createdFromMilli = createdFromMilli;
	}

	public Long getCreatedToMilli() {
		return createdToMilli;
	}

	public void setCreatedToMilli(Long createdToMilli) {
		this.createdToMilli = createdToMilli;
	}

	public String getCreatedFromStr() {
		return createdFromStr;
	}

	public void setCreatedFromStr(String createdFromStr) {
		this.createdFromStr = createdFromStr;
	}

	public String getCreatedToStr() {
		return createdToStr;
	}

	public void setCreatedToStr(String createdToStr) {
		this.createdToStr = createdToStr;
	}

	public DateTime getDateCreated() {
        return dateCreated;
    }

    public void setDateCreated(DateTime dateCreated) {
        this.dateCreated = dateCreated;
    }

    public String getVendorFormalName() {
        return vendorFormalName;
    }

    public void setVendorFormalName(String vendorFormalName) {
        this.vendorFormalName = vendorFormalName;
    }

    @Override
    public BooleanExpression createSearchExpression() {
        List<BooleanExpression> exp = new ArrayList<BooleanExpression>();
        QGiftCardInventoryStock qModel = QGiftCardInventoryStock.giftCardInventoryStock;

        /*if ( StringUtils.isNotBlank( cardType ) ) {
        	exp.add( qModel.inventory.profile.productCode.equalsIgnoreCase( cardType ) );
        }*/
        if ( StringUtils.isNotBlank( location ) ) {
        	exp.add( qModel.location.equalsIgnoreCase( location ) );
        }
        if ( null != createdFromMilli ) {
        	exp.add( qModel.createdDate.goe( new LocalDate( DateUtils.truncate( new Date( createdFromMilli ), Calendar.DATE ).getTime() ) ) );
        }
        if ( null != createdToMilli ) {
        	exp.add( qModel.createdDate.loe( new LocalDate( DateUtils.addMilliseconds( DateUtils.ceiling( new Date( createdToMilli ), Calendar.DATE ), -1 ).getTime() ) ) );
        }
        if ( StringUtils.isNotBlank( createdFromStr ) ) {
        	exp.add( qModel.createdDate.goe( LocalDate.parse( createdFromStr, DateTimeFormat.forPattern( CREATED_DATE_FORMAT ) ) ) );
        }
        if ( StringUtils.isNotBlank( createdToStr ) ) {
        	exp.add( qModel.createdDate.loe( LocalDate.parse( createdToStr, DateTimeFormat.forPattern( CREATED_DATE_FORMAT ) ) ) );
        }

        if(dateCreated != null) {
            exp.add(qModel.created.between(dateCreated, DateUtil.getEndOfDay(dateCreated)));
        }
        return BooleanExpression.allOf( exp.toArray(new BooleanExpression[ exp.size() ] ) );
    }

    public BooleanExpression createCountFilter() {
        List<BooleanExpression> exp = new ArrayList<BooleanExpression>();
        QGiftCardInventoryStock qModel = QGiftCardInventoryStock.giftCardInventoryStock;

        /*if ( StringUtils.isNotBlank( cardType ) ) {
        	exp.add( qModel.inventory.profile.productCode.equalsIgnoreCase( cardType ) );
        }*/
        if ( StringUtils.isNotBlank( location ) ) {
        	exp.add( qModel.location.equalsIgnoreCase( location ) );
        }

        return BooleanExpression.allOf( exp.toArray(new BooleanExpression[ exp.size() ] ) );
    }

}
