create table CRM_ADDRESS_AUD (
        id bigint not null,
        REV bigint not null,
        REVTYPE tinyint,
        BLOCK varchar(255),
        BUILDING varchar(255),
        CITY varchar(255),
        DISTRICT varchar(255),
        FLOOR integer,
        KM integer,
        POST_CODE varchar(255),
        PROVINCE varchar(50),
        ROOM varchar(255),
        RT varchar(255),
        RW varchar(255),
        STREET varchar(4000),
        STR_NO integer,
        SUBDISTRICT varchar(255),
        VILLAGE varchar(255),
        primary key (id, REV)
    );

create table CRM_AGE_OF_CHILDREN_AUD (
        REV bigint not null,
        ACCOUNT_ID bigint not null,
        AGE integer not null,
        REVTYPE tinyint,
        primary key (REV, ACCOUNT_ID, AGE)
    );

 create table CRM_APPLICATION_CONFIG_AUD (
        id bigint not null,
        REV bigint not null,
        REVTYPE tinyint,
        CONFIG_KEY varchar(255),
        CONFIG_VALUE varchar(255),
        primary key (id, REV)
    );

create table CRM_MEMBER_AUD (
        id bigint not null,
        REV bigint not null,
        REVTYPE tinyint,
        ACCOUNT_ID varchar(255),
        STATUS varchar(20),
        TIME_TO_CALL varchar(255),
        CARD_NO varchar(50),
        ACCEPT_EMAIL char(1),
        ACCEPT_MAIL char(1),
        ACCEPT_PHONE char(1),
        ACCEPT_SMS char(1),
        SMS char(1),
        COMPANY_NAME varchar(255),
        CONTACT varchar(255),
        BIRTHDATE timestamp,
        CARS_OWNED integer,
        CHILDREN_COUNT integer,
        CLOSEST_STORE varchar(255),
        CREDIT_CARD_OWNERSHIP char(1),
        DOMESTIC_HELPERS integer,
        FAX_NO varchar(255),
        INSURANCE_OWNERSHIP char(1),
        NAME_NATIVE varchar(120),
        PLACE_OF_BIRTH varchar(255),
        POSTAL_CODE varchar(255),
        EMAIL varchar(255),
        ACCOUNT_ENABLED char(1),
        FIRST_NAME varchar(255),
        KTP varchar(255),
        LAST_NAME varchar(255),
        LOYALTY_CARD_NO varchar(255),
        CREATED_FROM varchar(255),
        MIDDLE_NAME varchar(255),
        NPWP_ADDRESS varchar(255),
        NPWP_ID varchar(255),
        NPWP_NAME varchar(255),
        PARENT_ACCOUNT_ID varchar(255),
        PASSWORD varchar(255),
        PIN varchar(255),
        BUSINESS_EMAIL varchar(255),
        BUSINESS_LICENSE varchar(255),
        BUSINESS_NAME varchar(255),
        POSITION varchar(255),
        RADIUS varchar(255),
        TRANS_CODE_1 smallint,
        TRANS_CODE_2 smallint,
        TRANS_CODE_3 smallint,
        TRANS_CODE_4 smallint,
        TRANS_CODE_LONG1 smallint,
        TRANS_CODE_LONG2 smallint,
        TRANS_CODE_SHORT smallint,
        ZONE varchar(255),
        REGISTERED_STORE varchar(255),
        STORE_NAME varchar(255),
        TOTAL_POINTS double,
        username varchar(255),
        VALIDATION_CODE varchar(120),
        CARD_TYPE varchar(15),
        EDUCATION varchar(15),
        FAMILY_SIZE varchar(15),
        GENDER varchar(15),
        HOUSEHOLD_INCOME varchar(15),
        MARITAL_STATUS varchar(15),
        NATIONALITY varchar(15),
        OCCUPATION varchar(15),
        PERSONAL_INCOME varchar(15),
        PREFERED_LANGUAGE varchar(15),
        RELIGION varchar(15),
        TITLE varchar(15),
        EMP_TYPE varchar(15),
        MEMBER_TYPE varchar(15),
        BUSINESS_FIELD varchar(15),
        CUSTOMER_GROUP varchar(15),
        CUSTOMER_SEGREGATION varchar(15),
        DEPARTMENT varchar(15),
        primary key (id, REV)
    );

create table CRM_MEMBER_BANKS_AUD (
        REV bigint not null,
        ACCOUNT_ID bigint not null,
        CODE varchar(15) not null,
        REVTYPE tinyint,
        primary key (REV, ACCOUNT_ID, CODE)
    );

create table CRM_MEMBER_FOOD_AUD (
        REV bigint not null,
        ACCOUNT_ID bigint not null,
        CODE varchar(15) not null,
        REVTYPE tinyint,
        primary key (REV, ACCOUNT_ID, CODE)
    );

create table CRM_MEMBER_INTERESTS_AUD (
        REV bigint not null,
        ACCOUNT_ID bigint not null,
        CODE varchar(15) not null,
        REVTYPE tinyint,
        primary key (REV, ACCOUNT_ID, CODE)
    );

create table CRM_MEMBER_PETS_AUD (
        REV bigint not null,
        ACCOUNT_ID bigint not null,
        CODE varchar(15) not null,
        REVTYPE tinyint,
        primary key (REV, ACCOUNT_ID, CODE)
    );

alter table CRM_ADDRESS_AUD 
        add constraint FK_bf46udgrnqhbkmxvm5td9pwjk 
        foreign key (REV) 
        references CRM_REVINFO;

alter table CRM_AGE_OF_CHILDREN_AUD 
        add constraint FK_nqpfei3rr0qwjw2fhrtj8v6gr 
        foreign key (REV) 
        references CRM_REVINFO;

    alter table CRM_APPLICATION_CONFIG_AUD 
        add constraint FK_6h3dy60fsvw5wlel3qqr53osb 
        foreign key (REV) 
        references CRM_REVINFO;

    alter table CRM_MEMBER_AUD 
        add constraint FK_e1ah0ncgl6v9q3l55108dbv3n 
        foreign key (REV) 
        references CRM_REVINFO;

    alter table CRM_MEMBER_BANKS_AUD 
        add constraint FK_jwk9lwsqq2yehwa7lo3narayx 
        foreign key (REV) 
        references CRM_REVINFO;

    alter table CRM_MEMBER_FOOD_AUD 
        add constraint FK_j9253deupidw0cryd97oqa3fm 
        foreign key (REV) 
        references CRM_REVINFO;

    alter table CRM_MEMBER_INTERESTS_AUD 
        add constraint FK_1eqjs2chf44l8m35nkdmas2wa 
        foreign key (REV) 
        references CRM_REVINFO;

    alter table CRM_MEMBER_PETS_AUD 
        add constraint FK_ayt12hd0xdjyt1ujsiv6cwi3c 
        foreign key (REV) 
        references CRM_REVINFO;