package com.transretail.crm.reconciliation.service;

import com.transretail.crm.common.service.dto.response.ResultList;
import com.transretail.crm.common.service.exception.MessageSourceResolvableException;
import com.transretail.crm.rest.pos.response.dto.MissingEmpTxnBean;
import com.transretail.crm.rest.pos.response.dto.MissingPointsBean;
import com.transretail.crm.rest.request.MissingEmpTxnSearchDto;
import com.transretail.crm.rest.request.MissingPointsSearchDto;

/**
 * @author Allan G. Ramirez (agramirez@exist.com)
 */
public interface ReconciliationService {
    /**
     * Return missing professional and individual transactions
     *
     * @param dto search filters
     * @return missing professional and individual transactions
     * @throws MessageSourceResolvableException
     */
    ResultList<MissingPointsBean> getMissingIndProfPoints(MissingPointsSearchDto dto) throws MessageSourceResolvableException;

    /**
     * Return all eligible employee transactions that has no CRM_EMPLOYEE_PURCHASE_TXN nor CRM_POINTS record
     *
     * @param dto search filters
     * @return Return all eligible employee transactions that has no CRM_EMPLOYEE_PURCHASE_TXN nor CRM_POINTS record
     * @throws MessageSourceResolvableException
     */
    ResultList<MissingEmpTxnBean> getMissingEmpTxns(MissingEmpTxnSearchDto dto, String a, String b) throws MessageSourceResolvableException;
}
