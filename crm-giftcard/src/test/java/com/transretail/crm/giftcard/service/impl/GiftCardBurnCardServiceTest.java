package com.transretail.crm.giftcard.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.joda.time.LocalDate;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.transretail.crm.core.entity.lookup.LookupDetail;
import com.transretail.crm.core.entity.lookup.LookupHeader;
import com.transretail.crm.core.repo.LookupDetailRepo;
import com.transretail.crm.core.repo.LookupHeaderRepo;
import com.transretail.crm.core.service.CodePropertiesService;
import com.transretail.crm.giftcard.dto.GiftCardBurnCardDto;
import com.transretail.crm.giftcard.dto.GiftCardOrderDto;
import com.transretail.crm.giftcard.entity.CardVendor;
import com.transretail.crm.giftcard.entity.GiftCardBurnCard;
import com.transretail.crm.giftcard.entity.GiftCardInventory;
import com.transretail.crm.giftcard.entity.GiftCardOrder;
import com.transretail.crm.giftcard.entity.GiftCardOrderItem;
import com.transretail.crm.giftcard.entity.ProductProfile;
import com.transretail.crm.giftcard.entity.ProductProfile.Denomination;
import com.transretail.crm.giftcard.entity.support.GiftCardBurnCardStatus;
import com.transretail.crm.giftcard.entity.support.GiftCardInventoryStatus;
import com.transretail.crm.giftcard.entity.support.GiftCardOrderStatus;
import com.transretail.crm.giftcard.repo.CardVendorRepo;
import com.transretail.crm.giftcard.repo.GiftCardBurnCardRepo;
import com.transretail.crm.giftcard.repo.GiftCardInventoryRepo;
import com.transretail.crm.giftcard.repo.GiftCardOrderRepo;
import com.transretail.crm.giftcard.repo.ProductProfileRepo;
import com.transretail.crm.giftcard.service.GiftCardBurnCardService;
import com.transretail.crm.giftcard.service.GiftCardInventoryService;
import com.transretail.crm.giftcard.service.GiftCardOrderService;

/**
 * @author ftopico
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath*:/META-INF/spring/applicationContext*.xml", "classpath:META-INF/spring/applicationContext-test.xml"})
public class GiftCardBurnCardServiceTest {

    @Autowired
    private GiftCardBurnCardService giftCardBurnCardService;
    @Autowired
    private GiftCardBurnCardRepo giftCardBurnCardRepo;
    @Autowired
    private CardVendorRepo vendorRepo;
    @Autowired
    private ProductProfileRepo profileRepo;
    @Autowired
    private GiftCardOrderRepo giftCardOrderRepo;
    @Autowired
    private GiftCardOrderService orderService;
    @Autowired
    private GiftCardInventoryRepo inventoryRepo;
    @Autowired
    private GiftCardInventoryService giftCardInventoryService;
    @Autowired
    private CodePropertiesService codePropertiesService;
    @Autowired
    private LookupHeaderRepo lookupHeaderRepo;
    @Autowired
    private LookupDetailRepo lookupDetailRepo;

    private CardVendor savedVendor;
    private GiftCardOrder savedGiftCardOrder;
    private ProductProfile savedProfile;

    @Before
    public void setup() {

        savedVendor = new CardVendor();
        savedVendor.setFormalName("name");
        savedVendor.setMailAddress("add");
        savedVendor.setEmailAddress("email");
        savedVendor.setEnabled(true);
        savedVendor.setRegion("region");
        savedVendor.setDefaultVendor(true);
        savedVendor = vendorRepo.save(savedVendor);


        LookupHeader hdr = new LookupHeader(codePropertiesService.getProductProfileFaceValue());
        hdr = lookupHeaderRepo.saveAndFlush(hdr);

        LookupDetail dtl = new LookupDetail("PPFV1");
        dtl.setDescription("100000");
        dtl.setHeader(hdr);
        dtl = lookupDetailRepo.saveAndFlush(dtl);


        savedProfile = new ProductProfile();
        savedProfile.setFaceValue(dtl);
        savedProfile.setProductCode("000000001");
        savedProfile.setProductDesc("PRODUCT PROFILE DESC");
        savedProfile = profileRepo.save(savedProfile);

        savedGiftCardOrder = new GiftCardOrder();
        savedGiftCardOrder.setMoNumber("140403000001");
        savedGiftCardOrder.setPoNumber("0001");
        savedGiftCardOrder.setMoDate(new LocalDate());
        savedGiftCardOrder.setPoDate(new LocalDate());
        savedGiftCardOrder.setVendor(savedVendor);
        savedGiftCardOrder.setStatus(GiftCardOrderStatus.APPROVED);
        savedGiftCardOrder.setItems(new ArrayList<GiftCardOrderItem>());

        GiftCardOrderItem item = new GiftCardOrderItem();
        item.setProfile(savedProfile);
        item.setProductCode(savedProfile.getProductCode());
        item.setQuantity(10L);
        item.setOrder(savedGiftCardOrder);
        savedGiftCardOrder.getItems().add(item);

        savedGiftCardOrder = giftCardOrderRepo.save(savedGiftCardOrder);

        GiftCardOrderDto orderDto = orderService.getGiftCardOrder(savedGiftCardOrder.getId());
        giftCardInventoryService.generateGiftCardInventory(orderDto.getItems(), orderDto.getMoDate(), orderDto.getId());

        List<GiftCardInventory> list = inventoryRepo.findAll();
        GiftCardInventory giftCardInventory = list.get(0);
        giftCardInventory.setStatus(GiftCardInventoryStatus.IN_STOCK);
        inventoryRepo.save(giftCardInventory);

        GiftCardBurnCard model = new GiftCardBurnCard();
        model.setBurnNo("1234");
        model.setInventory(inventoryRepo.findAll().iterator().next());
        model.setStatus(GiftCardBurnCardStatus.FORAPPROVAL);
        giftCardBurnCardRepo.save(model);
    }

    @After
    public void cleanUp() {
        giftCardBurnCardRepo.deleteAll();
        inventoryRepo.deleteAll();
        giftCardOrderRepo.deleteAll();
        vendorRepo.deleteAll();
        profileRepo.deleteAll();
    }

    @Test
    public void getBurnCardRequestTest() {
        Assert.assertNotNull(giftCardBurnCardService.getBurnCardRequest("1234"));
    }

    @Test
    public void rejectRequestTest() {
        GiftCardBurnCardDto dto = new GiftCardBurnCardDto();
        dto.setBurnNo("1234");
        giftCardBurnCardService.rejectRequest(dto);

        GiftCardBurnCardDto rejected = giftCardBurnCardService.getBurnCardRequest("1234");

        Assert.assertTrue(rejected.getStatus().equalsIgnoreCase(GiftCardBurnCardStatus.REJECTED.toString()));
    }

    @Test
    public void burnCardsTest() {
        GiftCardBurnCardDto dto = new GiftCardBurnCardDto();
        dto.setBurnNo("1234");
        giftCardBurnCardService.burnCards(dto);

        GiftCardBurnCardDto burned = giftCardBurnCardService.getBurnCardRequest("1234");

        Assert.assertTrue(burned.getStatus().equalsIgnoreCase(GiftCardBurnCardStatus.BURNED.toString()));
    }
}
