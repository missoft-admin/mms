package com.transretail.crm.loyalty.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.Type;
import org.joda.time.LocalDateTime;
import org.springframework.data.annotation.CreatedBy;
import org.springframework.data.annotation.CreatedDate;

import com.transretail.crm.core.entity.lookup.LookupDetail;

@Table(name = "CRM_LC_PHYSICAL_COUNT_SUMMARY")
@Entity
public class LoyaltyPhysicalCountSummary implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "ID")
    private Long id;
	
	@Column(name = "CREATED_DATETIME")
    @Type(type = "org.jadira.usertype.dateandtime.joda.PersistentLocalDateTime")
    private LocalDateTime created;
    
	@Column(name = "CREATED_BY")
    @CreatedBy
    private String createUser;
    
    @Column(name = "LOCATION", nullable = false)
	private String location;
    
	@ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "CARD_TYPE", nullable = false)
	private LookupDetail cardType;
    
	@Column(name = "CURRENT_INVENTORY")
	private Long currentInventory;
    
	@Column(name = "PHYSICAL_COUNT")
	private Long physicalCount;
	
	public LoyaltyPhysicalCountSummary() {}
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public LocalDateTime getCreated() {
		return created;
	}

	public void setCreated(LocalDateTime created) {
		this.created = created;
	}

	public String getCreateUser() {
		return createUser;
	}

	public void setCreateUser(String createUser) {
		this.createUser = createUser;
	}

	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		this.location = location;
	}

	public LookupDetail getCardType() {
		return cardType;
	}
	public void setCardType(LookupDetail cardType) {
		this.cardType = cardType;
	}
	public Long getCurrentInventory() {
		return currentInventory;
	}
	public void setCurrentInventory(Long currentInventory) {
		this.currentInventory = currentInventory;
	}
	public Long getPhysicalCount() {
		return physicalCount;
	}
	public void setPhysicalCount(Long physicalCount) {
		this.physicalCount = physicalCount;
	}
}
