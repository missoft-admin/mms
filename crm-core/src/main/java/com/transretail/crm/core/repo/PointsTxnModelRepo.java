package com.transretail.crm.core.repo;


import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Repository;

import com.transretail.crm.common.repo.CrmQueryDslPredicateExecutor;
import com.transretail.crm.core.entity.MemberModel;
import com.transretail.crm.core.entity.PointsTxnModel;
import com.transretail.crm.core.entity.enums.PointTxnType;
import com.transretail.crm.core.entity.enums.TxnStatus;
import com.transretail.crm.core.repo.custom.PointsTxnRepoCustom;


@Repository
public interface PointsTxnModelRepo extends CrmQueryDslPredicateExecutor<PointsTxnModel, String>, PointsTxnRepoCustom {

    List<PointsTxnModel> findByTransactionNo(String transactionNo);

    List<PointsTxnModel> findByTransactionNoAndTransactionType(String transactionNo, PointTxnType transactionType);

    List<PointsTxnModel> findByMemberModel(MemberModel memberModel);
    
    List<PointsTxnModel> findByTransactionType( PointTxnType transactionType );
    
    List<PointsTxnModel> findByStatus( TxnStatus approvalStatus );
    
    Page<PointsTxnModel> findByTransactionType( PointTxnType transactionType, Pageable pageable );

}
