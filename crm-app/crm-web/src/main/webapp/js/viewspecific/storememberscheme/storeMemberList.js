$(document).ready( function() {
  
  var $storeMemberList = $("#list_store_member");
  var $theSearchForm = $( "#form_SearchForm" );
  var $theSearchBtn = $( "#searchButton" );
  
  initStoreMemberDataTable();
  
  function initStoreMemberDataTable() {
  
    var headers = {
        store:    $storeMemberList.data("store"),
        company:  $storeMemberList.data("company"),
        cardType: $storeMemberList.data("card-type")
    };
    
    $storeMemberList.ajaxDataTable({
      'autoload'    : true,
      'ajaxSource'  : $storeMemberList.data("url"),
      'columnHeaders' : [
         headers['store'],
         headers['company'],
         headers['cardType'],
         { text: "", className : "el-width-11" }
      ],
      'modelFields'  : [
         { name  : 'store', sortable : true, fieldCellRenderer : function( data, type, row ) {
             if ( row.storeDto != null && row.storeDto != "" ) {
               return row.storeDto.code + " - " + row.storeDto.name;
             }
             else {
               return  + row.store;
             }
           }
         },
         { name  : 'company', sortable : true, fieldCellRenderer : function( data, type, row ) {
           if ( row.companyDto != null && row.companyDto != "" ) {
             return row.companyDto.code + " - " + row.companyDto.description;
           }
           else {
             return  '-';
           }
         }
       },
         { name  : 'cardType', sortable : true, fieldCellRenderer : function( data, type, row ) {
           if ( row.cardTypeDto != null && row.cardTypeDto != "" ) {
             return row.cardTypeDto.code + " - " + row.cardTypeDto.description;
           }
           else {
             return  '-';
           }
         }
       },
         { customCell : function ( innerData, sSpecific, json ) {
             if ( sSpecific == 'display' ) {
               return $( "#storeMember-list-actions" ).html()
                 .replace( new RegExp("%ID%", 'g'), json.id);
             } else {
               return '';
             }
           }
         }  
        ]
    })
    .on( "click", ".btn_editStoreMember", editStoreMember )
    .on( "click", ".btn_deleteStoreMember", deleteStoreMember );
    initSearchForm($storeMemberList);
  }
  
  function editStoreMember(e) {
    e.preventDefault();
        var url = $(this ).data('url');
    $.ajax({
      cache     : false, 
      dataType  : "html",
      type    : "GET", 
      url     : url + $( this ).data( "id" ),
      error     : function(jqXHR, textStatus, errorThrown) {}, 
      success   : function( inResponse ) {
        $( "#form_store_member" ).html( inResponse );
      }
    });
  }
  
  function deleteStoreMember(e) {
    e.preventDefault();
    var deleteUrl = $( ".btn_deleteStoreMember" ).data( "url" ) + $( this ).data( "id" );
    getConfirm( $( ".btn_deleteStoreMember" ).data( "msg-confirm" ), function( result ) {
      if( result ) {
        $.post( 
            deleteUrl,
            function( inResponse, textStatus, jqXHR ) { $( "#list_store_member" ).ajaxDataTable( "search" ); },
            "json" );
      }
    });
  }
  
  function initSearchForm( inDataTable ) {
    $theSearchBtn.click( function() {
      var $theFormDataObj = $theSearchForm.toObject( { mode : 'first', skipEmpty : true } );
      var $btnSearch = $( this );
      $btnSearch.prop( 'disabled', true );
      inDataTable.ajaxDataTable( 
        'search', 
        $theFormDataObj, 
        function () {
          $btnSearch.prop( 'disabled', false );
      });
    });
    
    $("#clearButton").click(function(e) {
      e.preventDefault();
      $(".pointsSearchDropdown").val("");
      $(".pointsSearchDropdown").val("");
      $("#searchButton").click();
    });
  }
});