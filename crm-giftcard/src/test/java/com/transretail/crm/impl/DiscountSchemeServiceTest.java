package com.transretail.crm.impl;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashSet;

import org.joda.time.DateTime;
import org.joda.time.LocalDate;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.google.common.collect.Lists;
import com.transretail.crm.common.service.dto.request.PageSortDto;
import com.transretail.crm.common.service.dto.response.ResultList;
import com.transretail.crm.giftcard.dto.GiftCardDiscountSchemeDto;
import com.transretail.crm.giftcard.entity.GiftCardCustomerProfile;
import com.transretail.crm.giftcard.entity.GiftCardDiscountScheme;
import com.transretail.crm.giftcard.entity.SalesOrder;
import com.transretail.crm.giftcard.entity.SalesOrderItem;
import com.transretail.crm.giftcard.entity.support.SalesOrderStatus;
import com.transretail.crm.giftcard.entity.support.SalesOrderType;
import com.transretail.crm.giftcard.repo.GiftCardCustomerProfileRepo;
import com.transretail.crm.giftcard.repo.GiftCardDiscountSchemeRepo;
import com.transretail.crm.giftcard.repo.SalesOrderRepo;
import com.transretail.crm.giftcard.service.DiscountSchemeService;
import com.transretail.crm.giftcard.service.DiscountSchemeService.DiscountDto;

/**
 *
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath*:/META-INF/spring/applicationContext*.xml",
    "classpath:META-INF/spring/applicationContext-test.xml"})
public class DiscountSchemeServiceTest {
	
	@Autowired
	private DiscountSchemeService schemeService;
	@Autowired
	private GiftCardDiscountSchemeRepo schemeRepo;
	@Autowired
	private GiftCardCustomerProfileRepo customerRepo;
	@Autowired
	private SalesOrderRepo salesOrderRepo;
	
	private GiftCardDiscountScheme savedScheme;
	private GiftCardCustomerProfile savedCustomer;
	
	@Before
	public void setup() {
		savedScheme = new GiftCardDiscountScheme();
		savedScheme.setStartingDiscount(new BigDecimal(10));
		savedScheme.setEndingDiscount(new BigDecimal(25));
		savedScheme.setPurchaseForMaxDiscount(new BigDecimal(2));
		savedScheme.setMinPurchase(new BigDecimal(1));
		savedScheme.setStartDate(new LocalDate().minusDays(1));
		savedScheme.setEndDate(new LocalDate().plusDays(1));
		savedScheme = schemeRepo.save(savedScheme);
		
		savedCustomer = new GiftCardCustomerProfile();
		savedCustomer.setCustomerId("CUSTOMER ID");
		savedCustomer.setName("CUSTOMER NAME");
		savedCustomer.setInvoiceTitle("INVOICE TITLE");
		savedCustomer = customerRepo.save(savedCustomer);
		
		SalesOrder order = new SalesOrder();
		order.setCustomer(savedCustomer);
		order.setStatus(SalesOrderStatus.APPROVED);
		order.setOrderType(SalesOrderType.B2B_SALES);
		order.setLastUpdated(new DateTime());
		order.setOrderDate(new LocalDate());
		order.setItems(new HashSet<SalesOrderItem>());
		
		SalesOrderItem item = new SalesOrderItem();
		item.setOrder(order);
		item.setFaceAmount(new BigDecimal(1500000000));
		order.getItems().add(item);
		
		salesOrderRepo.save(order);
		
	}
	
	@After
	public void teardown() {
		schemeRepo.deleteAll();
	}
	
	@Test
	public void saveSchemeTest() {
		GiftCardDiscountSchemeDto schemeDto = new GiftCardDiscountSchemeDto();
		schemeDto.setStartingDiscount(new BigDecimal(20));
		schemeDto.setEndingDiscount(new BigDecimal(30));
		schemeDto.setPurchaseForMaxDiscount(new BigDecimal(300000));
		schemeDto.setMinPurchase(new BigDecimal(200000));
		schemeDto.setStartDate(new LocalDate().minusDays(1));
		schemeDto.setEndDate(new LocalDate().plusDays(1));
		schemeService.saveScheme(schemeDto);
		
		Assert.assertTrue(schemeRepo.count() == 2);
	}
	
	@Test
	public void listTest() {
		ResultList<GiftCardDiscountSchemeDto> result = schemeService.list(new PageSortDto());
		Assert.assertTrue(result.getTotalElements() == 1);
	}
	
	@Test
	public void getSchemeDtoTest() {
		GiftCardDiscountSchemeDto schemeDto = schemeService.getSchemeDto(savedScheme.getId());
		Assert.assertTrue(schemeDto != null);
		Assert.assertTrue(schemeDto.getStartingDiscount().compareTo(new BigDecimal(10)) == 0);
		Assert.assertTrue(schemeDto.getEndingDiscount().compareTo(new BigDecimal(25)) == 0);
	}
	
	@Test
	public void deleteSchemeTest() {
		schemeService.deleteScheme(savedScheme.getId());
		Assert.assertTrue(schemeRepo.count() == 0);
	}
	
	@Test
	public void getDiscountTest() {
		GiftCardDiscountSchemeDto schemeDto = schemeService.getSchemeDto(savedScheme.getId());
		DiscountDto discount = schemeService.getDiscount(savedCustomer.getId(), schemeDto, new BigDecimal(1500000000));
		Assert.assertTrue(discount.getDiscount().compareTo(new BigDecimal(10)) == 0);
		Assert.assertTrue(discount.getVoucher() == null);
		
		SalesOrder order = new SalesOrder();
		order.setCustomer(savedCustomer);
		order.setStatus(SalesOrderStatus.APPROVED);
		order.setOrderType(SalesOrderType.B2B_SALES);
		order.setLastUpdated(new DateTime());
		order.setOrderDate(new LocalDate());
		order.setItems(new HashSet<SalesOrderItem>());
		
		SalesOrderItem item = new SalesOrderItem();
		item.setOrder(order);
		item.setFaceAmount(new BigDecimal(1000000000));
		order.getItems().add(item);
		
		salesOrderRepo.save(order);
		
		discount = schemeService.getDiscount(savedCustomer.getId(), schemeDto, new BigDecimal(1000000000));
		Assert.assertTrue(discount.getDiscount().compareTo(new BigDecimal(10)) == 0);
		Assert.assertTrue(discount.getVoucher().compareTo(new BigDecimal(15)) == 0);
		
		order = new SalesOrder();
		order.setCustomer(savedCustomer);
		order.setStatus(SalesOrderStatus.APPROVED);
		order.setOrderType(SalesOrderType.B2B_SALES);
		order.setLastUpdated(new DateTime());
		order.setOrderDate(new LocalDate());
		order.setItems(new HashSet<SalesOrderItem>());
		
		item = new SalesOrderItem();
		item.setOrder(order);
		item.setFaceAmount(new BigDecimal(1000000000));
		order.getItems().add(item);
		
		salesOrderRepo.save(order);
		
		discount = schemeService.getDiscount(savedCustomer.getId(), schemeDto, new BigDecimal(1000000000));
		Assert.assertTrue(discount.getDiscount().compareTo(new BigDecimal(25)) == 0);
	}
}
