    create table CRM_PREPAID_RELOAD_INFO (
        id number(19,0) not null,
        CREATED_BY varchar2(255 char),
        CREATED_DATETIME timestamp,
        LAST_UPDATED_BY varchar2(255 char),
        LAST_UPDATED_DATETIME timestamp,
        monthEffective number(10,0) not null,
        reloadAmount number(19,0),
        PRODUCT number(19,0),
        primary key (id)
    );

    alter table CRM_PREPAID_RELOAD_INFO
        add constraint FK_4anityxy29y76he8smun6fpjx
        foreign key (PRODUCT)
        references CRM_GC_PROD_PROFILE;