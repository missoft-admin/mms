<%@ include file="../../common/taglibs.jsp" %>

<c:set var="ENCTYPE" value="application/x-www-form-urlencoded"/>

<div class="modal-dialog">
  <div class="modal-content">
 
    <div class="modal-body">
      <form:form id="multipleAllocateDto" name="multipleAllocateDto" modelAttribute="multipleAllocateDto" data-print-action="${pageContext.request.contextPath}/loyaltycardinventory/printtransfer?" action="${pageContext.request.contextPath}/loyaltycardinventory/transfer" method="POST" enctype="${ENCTYPE}" cssClass="form-reset" >

      <div id="contentError" class="hide alert alert-error">
        <button type="button" class="close" onclick="$(this).parent().hide();">&times;</button>
        <div><form:errors path="*"/></div>
      </div>
      
      <div class="contentMain">
        <div class="row-fluid">
          <div class="span4">
            <spring:message code="mo_tier_prop_name" var="cardType" />
            <label for="">${cardType}</label>
            <input type="text" name="cardTypeDesc" value="${multipleAllocateDto.cardType.description}" disabled="disabled" /><input type="hidden" name="cardType" value="${multipleAllocateDto.cardType.code}" />
          </div>
          
          <div class="span4">
            <spring:message code="inventory_prop_location" var="locationLabel" />
            <label for="">${locationLabel}</label>
            <input type="text" name="locationDesc" value="${multipleAllocateDto.locationName}" disabled="disabled" /><input type="hidden" name="location" value="${multipleAllocateDto.location}" />
          </div>
          
          <div class="span4">
            <spring:message code="inventory_prop_allocateto" var="allocateToLabel" />
            <label for="">${allocateToLabel}</label>
            <input type="text" name="allocateToDesc" value="${multipleAllocateDto.transferToName}" disabled="disabled" /><input type="hidden" name="transferTo" value="${multipleAllocateDto.transferTo}" />
          </div>
          
        </div>
        <div>
          <table id="productTable" class="table table-condensed table-compressed table-striped">
            <thead>
              <tr>
                <th><spring:message code="mo_tier_prop_startingseries" /></th>
                <th><spring:message code="mo_tier_prop_endingseries" /></th>
                <th><spring:message code="inventory_prop_encodedby" /></th>
              </tr>
            </thead>
            <tbody>
              <c:forEach var="item" items="${multipleAllocateDto.allocations}" varStatus="status" >
              <tr>
                <td><input type="hidden" name="allocations[${status.index}].referenceNo" value="${item.referenceNo}" />
                <input type="hidden" name="allocations[${status.index}].location" value="${item.location}" />
                <input type="hidden" name="allocations[${status.index}].transferTo" value="${item.transferTo}" />${item.startingSeries}</td>
                <td>${item.endingSeries}</td>
                <td>${item.encodedBy} (${item.allocateDate})</td>
              </tr>
              </c:forEach>
            </tbody>
          </table>
        </div>
      </div>
      </form:form>
    
    </div>
    <div class="modal-footer">
      <button type="button" id="" class="btn btn-default" data-dismiss="modal"><spring:message code="label_cancel"/></button>
      <button type="button" id="" class="btn btn-primary printTransfer"><spring:message code="label_lc_transfer_print" /></button>
    </div>
  </div>
</div>

<script src="<spring:url value="/js/viewspecific/loyalty/transfer.js"/>"></script>
