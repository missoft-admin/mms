package com.transretail.crm.rest.pos;

import static org.junit.Assert.assertEquals;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.support.PropertySourcesPlaceholderConfigurer;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.AnnotationConfigWebContextLoader;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.transretail.crm.rest.store.PosApiController;
import com.transretail.crm.web.controller.AbstractMockMvcTest;

/**
 * @author Allan G. Ramirez (agramirez@exist.com)
 */
@RunWith(SpringJUnit4ClassRunner.class)
@WebAppConfiguration
@ContextConfiguration(loader = AnnotationConfigWebContextLoader.class)
public class PosApiControllerTest extends AbstractMockMvcTest {
    @Configuration
    @EnableWebMvc
    static class Config {
        @Bean
        public PosApiController posApiController() {
            return new PosApiController();
        }

        @Bean
        public PropertySourcesPlaceholderConfigurer propertySourcesPlaceholderConfigurer() throws Exception {
            PropertySourcesPlaceholderConfigurer configurer = new PropertySourcesPlaceholderConfigurer();
            Properties properties = new Properties();
            InputStream is = Thread.currentThread().getContextClassLoader().getResourceAsStream("config/build.properties");
            try {
                properties.load(is);
                configurer.setProperties(properties);
            } finally {
                org.apache.commons.io.IOUtils.closeQuietly(is);
            }
            return configurer;
        }
    }

    @Test
    public void getGitInfoTest() throws Exception {
        ObjectMapper mapper = new ObjectMapper();
        String content = mockMvc.perform(get("/posapi/info")).andExpect(status().isOk())
            .andReturn().getResponse().getContentAsString();
        Map<String, String> actualResult = mapper.readValue(content, new TypeReference<HashMap<String, String>>() {
        });

        assertEquals("1.2.0", actualResult.get("project.version"));
        assertEquals("chicken-adobo", actualResult.get("git.branch"));
        assertEquals("test-commit-031312", actualResult.get("git.commit.id"));
        assertEquals("test-commit-031312", actualResult.get("git.commit.id.abbrev"));
        assertEquals("CloudX", actualResult.get("git.commit.user.name"));
        //assertEquals("Sample Commit", actualResult.get("git.commit.message.full"));
        assertEquals("03:13:12", actualResult.get("git.commit.time"));
    }

}
