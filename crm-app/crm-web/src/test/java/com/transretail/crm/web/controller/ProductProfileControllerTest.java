package com.transretail.crm.web.controller;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.mockito.Matchers.argThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.reset;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.Locale;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentMatcher;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.http.MediaType;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.AnnotationConfigWebContextLoader;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.transretail.crm.giftcard.dto.ProductProfileDto;
import com.transretail.crm.giftcard.entity.support.ProductProfileStatus;
import com.transretail.crm.giftcard.service.ProductProfileService;
import com.transretail.crm.web.controller.util.ControllerResponse;

/**
 * @author Allan G. Ramirez (agramirez@exist.com)
 */
@RunWith(SpringJUnit4ClassRunner.class)
@WebAppConfiguration
@ContextConfiguration(loader = AnnotationConfigWebContextLoader.class)
public class ProductProfileControllerTest extends AbstractMockMvcTest {
    @Configuration
    @EnableWebMvc
    static class Config {
        @Bean
        public ProductProfileController productProfileController() {
            return new ProductProfileController();
        }

        @Bean
        public ProductProfileService profileService() {
            return mock(ProductProfileService.class);
        }

        @Bean
        public MessageSource messageSource() {
            return mock(MessageSource.class);
        }
    }

    @Autowired
    private ProductProfileService profileService;
    @Autowired
    private MessageSource messageSource;

    @Override
    protected void doSetup() throws Exception {
        reset(profileService);
        reset(messageSource);
    }

    @Test
    public void showFormWithProfileId() throws Exception {
        Long id = 1l;
        ProductProfileDto expected = new ProductProfileDto();
        expected.setId(id);
        when(profileService.findDto(id)).thenReturn(expected);

        MvcResult mvcResult =
            mockMvc.perform(get("/gc/productprofile/form/" + id).accept(MediaType.TEXT_HTML)).andExpect(status().isOk()).andReturn();
        ProductProfileDto actual = (ProductProfileDto) mvcResult.getModelAndView().getModel().get("profileForm");
        assertEquals(expected.getId(), actual.getId());
    }

    @Test
    public void ajaxSaveWithValidationErrorTest() throws Exception {
        Locale locale = Locale.ENGLISH;
        LocaleContextHolder.setLocale(locale);

        String productCodeErr = "Code Empty";
        String productDescErr = "Desc Empty";
        String unitCostNull = "Unit cost null";

        when(messageSource.getMessage("profile_product_code", null, locale)).thenReturn(productCodeErr);
        when(messageSource.getMessage("profile_product_desc", null, locale)).thenReturn(productDescErr);
        when(messageSource.getMessage("profile_unit_cost", null, locale)).thenReturn(unitCostNull);

        ProductProfileDto dto = new ProductProfileDto();
        String response = mockMvc.perform(setParams(
                post("/gc/productprofile/save/" + ProductProfileStatus.APPROVED).accept(MediaType.APPLICATION_JSON), dto)
        ).andExpect(status().isOk()).andReturn().getResponse().getContentAsString();
        ObjectMapper objectMapper = new ObjectMapper();
        ControllerResponse controllerResponse = objectMapper.readValue(response, ControllerResponse.class);

        assertFalse(controllerResponse.isSuccess());
        verifyInvocation(1, "propkey_msg_notempty", productCodeErr, locale);
        verifyInvocation(1, "propkey_msg_notempty", productDescErr, locale);
        verifyInvocation(1, "propkey_msg_notempty", unitCostNull, locale);

        dto.setProductCode("someProductCode");
        response = mockMvc.perform(setParams(
                post("/gc/productprofile/save/" + ProductProfileStatus.APPROVED).accept(MediaType.APPLICATION_JSON), dto)
        ).andExpect(status().isOk()).andReturn().getResponse().getContentAsString();
        controllerResponse = objectMapper.readValue(response, ControllerResponse.class);

        assertFalse(controllerResponse.isSuccess());
        verifyInvocation(1, "propkey_msg_notempty", productCodeErr, locale);
        verifyInvocation(2, "propkey_msg_notempty", productDescErr, locale);
        verifyInvocation(2, "propkey_msg_notempty", unitCostNull, locale);

        dto.setProductDesc("someProductDesc");
        response = mockMvc.perform(setParams(
                post("/gc/productprofile/save/" + ProductProfileStatus.APPROVED).accept(MediaType.APPLICATION_JSON), dto)
        ).andExpect(status().isOk()).andReturn().getResponse().getContentAsString();
        controllerResponse = objectMapper.readValue(response, ControllerResponse.class);

        assertFalse(controllerResponse.isSuccess());
        verifyInvocation(1, "propkey_msg_notempty", productCodeErr, locale);
        verifyInvocation(2, "propkey_msg_notempty", productDescErr, locale);
        verifyInvocation(3, "propkey_msg_notempty", unitCostNull, locale);

        dto.setUnitCost(200000d);
        response = mockMvc.perform(setParams(
                post("/gc/productprofile/save/" + ProductProfileStatus.APPROVED).accept(MediaType.APPLICATION_JSON), dto)
        ).andExpect(status().isOk()).andReturn().getResponse().getContentAsString();
        controllerResponse = objectMapper.readValue(response, ControllerResponse.class);

        assertFalse(controllerResponse.isSuccess());
        verifyInvocation(1, "propkey_msg_notempty", productCodeErr, locale);
        verifyInvocation(2, "propkey_msg_notempty", productDescErr, locale);
        verifyInvocation(3, "propkey_msg_notempty", unitCostNull, locale);

        // Allow reload and max amount null validation
        dto.setAllowReload(true);
        response = mockMvc.perform(setParams(
                post("/gc/productprofile/save/" + ProductProfileStatus.APPROVED).accept(MediaType.APPLICATION_JSON), dto)
        ).andExpect(status().isOk()).andReturn().getResponse().getContentAsString();
        controllerResponse = objectMapper.readValue(response, ControllerResponse.class);

        assertFalse(controllerResponse.isSuccess());
        verifyInvocation(1, "required.maxamount", null, locale);
    }

    private void verifyInvocation(int times, final String code, final String arg, final Locale locale) {
        verify(messageSource, times(times)).getMessage(argThat(new ArgumentMatcher<String>() {
            @Override
            public boolean matches(Object argument) {
                return code.equals(argument);
            }
        }), argThat(new ArgumentMatcher<Object[]>() {
            @Override
            public boolean matches(Object argument) {
                return (argument == null && arg == null) || (argument != null && ((Object[]) argument)[0] != null
                    && ((Object[]) argument)[0].equals(arg));
            }
        }), argThat(new ArgumentMatcher<String>() {
            @Override
            public boolean matches(Object argument) {
                return "Info given is invalid.".equals(argument);
            }
        }), argThat(new ArgumentMatcher<Locale>() {
            @Override
            public boolean matches(Object argument) {
                return locale.equals(argument);
            }
        }));
    }
}
