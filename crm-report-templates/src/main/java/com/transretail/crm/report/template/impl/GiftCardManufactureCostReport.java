package com.transretail.crm.report.template.impl;

import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.joda.time.DateTime;
import org.joda.time.LocalDate;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.MessageSourceAware;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.context.support.MessageSourceAccessor;
import org.springframework.stereotype.Service;

import com.google.common.base.Function;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.google.common.collect.Sets;
import com.mysema.query.Tuple;
import com.mysema.query.group.GroupBy;
import com.mysema.query.jpa.impl.JPAQuery;
import com.mysema.query.types.ConstructorExpression;
import com.mysema.query.types.Projections;
import com.mysema.query.types.expr.BooleanExpression;
import com.transretail.crm.common.reporting.jasper.ClasspathFileResolver;
import com.transretail.crm.common.reporting.jasper.DefaultJRProcessor;
import com.transretail.crm.common.reporting.jasper.JRProcessor;
import com.transretail.crm.common.util.DateUtil;
import com.transretail.crm.core.entity.enums.ReportType;
import com.transretail.crm.giftcard.dto.ProductProfileDto;
import com.transretail.crm.giftcard.entity.GiftCardOrderItem;
import com.transretail.crm.giftcard.entity.QGiftCardInventory;
import com.transretail.crm.giftcard.entity.QGiftCardOrder;
import com.transretail.crm.giftcard.entity.QGiftCardOrderItem;
import com.transretail.crm.giftcard.entity.QProductProfile;
import com.transretail.crm.giftcard.service.ProductProfileService;
import com.transretail.crm.report.template.CommonReportFilter;
import com.transretail.crm.report.template.FilterField;
import com.transretail.crm.report.template.ReportTemplate;
import com.transretail.crm.report.template.ReportsCustomizer;

@Service("giftCardManufactureCostReport")
public class GiftCardManufactureCostReport implements ReportTemplate, MessageSourceAware, ReportsCustomizer {
	
	protected static final String TEMPLATE_NAME = "Card Manufacture Cost Report";
    protected static final String REPORT_NAME = "reports/card_manufacture_cost_report.jasper";
    protected static final String FILTER_DATE_FROM = CommonReportFilter.DATE_FROM;
    protected static final String FILTER_DATE_TO = CommonReportFilter.DATE_TO;
    
    private static final DateTimeFormatter INPUT_DATE_PATTERN = DateTimeFormat.forPattern(DateUtil.SEARCH_DATE_FORMAT);
    private static final DateTimeFormatter FULL_DATE_PATTERN = DateTimeFormat.forPattern("dd-MMM-yyyy");
    
    private static final Logger LOGGER = LoggerFactory.getLogger(GiftCardManufactureCostReport.class);
	
	private MessageSourceAccessor messageSource;
    @PersistenceContext
    private EntityManager em;
    @Autowired
    private ProductProfileService productProfileService;
    
    private LocalDate parseDate(String dateInstance) {
    	return INPUT_DATE_PATTERN.parseLocalDate(dateInstance);
    }
    
    @Override
    public ReportType[] getReportTypes() {
    	return ReportType.values();
    }
    
    @Override
    public String getName() {
    	return TEMPLATE_NAME;
    }

    @Override
    public String getDescription() {
    	return messageSource.getMessage("gc.mo.cost.report.label", (Object[]) null, LocaleContextHolder.getLocale());
    }
    
    @Override
    public boolean canView(Set<String> permissions) {
    	return true;
    }
    
    @Override
	public Set<Option> customizerOption() {
		return Sets.newHashSet(ReportsCustomizer.Option.DATE_RANGE_FULL_VALIDATION);
	}
    
    @Override
    public Set<FilterField> getFilters() {
		Set<FilterField> filters = Sets.newLinkedHashSet();
		filters.add(FilterField.createDateField(FILTER_DATE_FROM,
			messageSource.getMessage("gc.mo.cost.report.label.date.from"),
			DateUtil.SEARCH_DATE_FORMAT, true));
		filters.add(FilterField.createDateField(FILTER_DATE_TO,
			messageSource.getMessage("gc.mo.cost.report.label.date.to"),
			DateUtil.SEARCH_DATE_FORMAT, true));
	
		return filters;
    }
    
    @Override
    public JRProcessor createJrProcessor(Map<String, String> map) {
		LocalDate dateFrom = parseDate(map.get(FILTER_DATE_FROM));
		LocalDate dateTo = parseDate(map.get(FILTER_DATE_TO));
		DefaultJRProcessor jrProcessor = new DefaultJRProcessor(REPORT_NAME);
		Map<String, Object> paramMap = prepareReportParameters(dateFrom, dateTo);
		paramMap.put("SUB_DATA_SOURCE", createDatasource(dateFrom, dateTo, paramMap));
		jrProcessor.addParameters(paramMap);
		jrProcessor.setFileResolver(new ClasspathFileResolver("reports"));
		return jrProcessor;
    }

    @Override
    public void setMessageSource(MessageSource messageSource) {
    	this.messageSource = new MessageSourceAccessor(messageSource);
    }
    
    public List<ReportBean> createDatasource(LocalDate dateFrom, LocalDate dateTo, Map<String, Object> paramMap) {
    	List<ProductProfileDto> productProfiles = productProfileService.findAllDtos();
    	List<ReportBean> reportBeans = Lists.newArrayList();
    	QGiftCardInventory gcInventory = QGiftCardInventory.giftCardInventory;
    	QGiftCardOrderItem gcOrderItem = QGiftCardOrderItem.giftCardOrderItem;
    	Double totalCost = 0D;
    	
    	for(ProductProfileDto dto : productProfiles) {
    		List<ManufactureOrderBean> beans = Lists.newArrayList();
    		Double subtotal = 0D;
    		
    		Map<String, List<ManufactureOrderBean>> recievedMap = new JPAQuery(em)
    			.from(gcInventory)
    			.where(BooleanExpression.allOf(
    				gcInventory.receiveDate.isNotNull(),
    				gcInventory.receiveDate.between(dateFrom, dateTo),
    				gcInventory.productCode.eq(dto.getProductCode()))
    			)
    			.groupBy(gcInventory.order.moNumber, gcInventory.order.moDate, 
    					gcInventory.order.approvalDate)
    			.transform(GroupBy.groupBy(gcInventory.order.moNumber)
    				.as(GroupBy.list(ConstructorExpression.create(ManufactureOrderBean.class,
    						gcInventory.order.moDate, gcInventory.order.approvalDate, 
    						gcInventory.series.min(), gcInventory.series.max(), 
    						gcInventory.id.count()))
    				)
    			);
    		
    		for(String moNumber : recievedMap.keySet()) {
    			Long quantityOrdered = new JPAQuery(em).from(gcInventory).where(BooleanExpression.allOf(
    					gcInventory.order.moNumber.eq(moNumber),
    					gcInventory.productCode.eq(dto.getProductCode()))
    				)
    				.singleResult(gcInventory.series.max().subtract(gcInventory.series.min()).add(1));
    			Long quantityReceived = new JPAQuery(em).from(gcInventory)
					.where(BooleanExpression.allOf(
							gcInventory.order.moNumber.eq(moNumber),
							gcInventory.productCode.eq(dto.getProductCode()),
							gcInventory.receiveDate.isNotNull()))
					.count();
    			Double unitCost = new JPAQuery(em).from(gcOrderItem)
    				.where(BooleanExpression.allOf(
    						gcOrderItem.order.moNumber.eq(moNumber),
    						gcOrderItem.profile.productCode.eq(dto.getProductCode()))
    				)
    				.singleResult(gcOrderItem.unitCost.max());
    			
    			LOGGER.error("" + unitCost);
    			
    			for(ManufactureOrderBean bean : recievedMap.get(moNumber)) {
    				bean.setMoNumber(moNumber);
    				bean.setQuantityOrdered(quantityOrdered);
    				bean.setQuantityReceived(quantityReceived);
    				bean.setCardCost(bean.getQuantityCosting() * (unitCost != null ? unitCost : 0D));
    				subtotal += bean.getCardCost();
    				beans.add(bean);
    			}
    		}
    		
    		ReportBean reportBean = new ReportBean();
    		reportBean.setCode(dto.getProductCode());
    		reportBean.setDescription(dto.getProductDesc());
    		reportBean.setUnitCost(dto.getUnitCost());
    		reportBean.setSubTotal(subtotal);
    		reportBean.setManufactureOrders(beans);
    		totalCost += subtotal;
    		reportBeans.add(reportBean);
    	}
    	
    	paramMap.put("TOTAL_COST_VALUE", totalCost);
    	
    	return reportBeans;
    }
    
    private Map<String, Object> prepareReportParameters(LocalDate dateFrom, LocalDate dateTo) {
    	HashMap<String, Object> reportMap = Maps.newHashMap();
    	reportMap.put("DATE_FROM", dateFrom.toString(FULL_DATE_PATTERN));
    	reportMap.put("DATE_TO", dateTo.toString(FULL_DATE_PATTERN));
    	Locale locale = LocaleContextHolder.getLocale();
    	Object[] args = null;
    	reportMap.put("CODE", messageSource.getMessage("gc.mo.cost.report.label.code", args, locale));
    	reportMap.put("DESCRIPTION", messageSource.getMessage("gc.mo.cost.report.label.description", args, locale));
    	reportMap.put("UNIT_COST", messageSource.getMessage("gc.mo.cost.report.label.unit.cost", args, locale));
    	reportMap.put("MO_DATE", messageSource.getMessage("gc.mo.cost.report.label.mo.date", args, locale));
    	reportMap.put("MO_NUMBER", messageSource.getMessage("gc.mo.cost.report.label.mo.number", args, locale));
    	reportMap.put("APPROVE_DATE", messageSource.getMessage("gc.mo.cost.report.label.approve.date", args, locale));
    	reportMap.put("QUANTITY_ORDERED", messageSource.getMessage("gc.mo.cost.report.label.quantity.ordered", args, locale));
    	reportMap.put("QUANTITY_RECEIVED", messageSource.getMessage("gc.mo.cost.report.label.quantity.received", args, locale));
    	reportMap.put("QUANTITY_COSTING", messageSource.getMessage("gc.mo.cost.report.label.quantity.costing", args, locale));
    	reportMap.put("CARD_COST", messageSource.getMessage("gc.mo.cost.report.label.card.cost", args, locale));
    	reportMap.put("SUBTOTAL", messageSource.getMessage("gc.mo.cost.report.label.subtotal", args, locale));
    	reportMap.put("TOTAL_COST", messageSource.getMessage("gc.mo.cost.report.label.total.cost", args, locale));
    	reportMap.put("REPORT_TYPE", messageSource.getMessage("gc.mo.cost.report.label", args, locale));
    	reportMap.put("NO_DATA", messageSource.getMessage("report.no.data", (Object[]) null, 
				LocaleContextHolder.getLocale()));
    	
    	return reportMap;
    }
    
    public static class ReportBean {
    	private String code;
    	private String description;
    	private Double unitCost;
    	private Double subTotal;
    	private List<ManufactureOrderBean> manufactureOrders;
		public String getCode() {
			return code;
		}
		public void setCode(String code) {
			this.code = code;
		}
		public String getDescription() {
			return description;
		}
		public void setDescription(String description) {
			this.description = description;
		}
		public Double getUnitCost() {
			return unitCost;
		}
		public void setUnitCost(Double unitCost) {
			this.unitCost = unitCost;
		}
		public Double getSubTotal() {
			return subTotal;
		}
		public void setSubTotal(Double subTotal) {
			this.subTotal = subTotal;
		}
		public List<ManufactureOrderBean> getManufactureOrders() {
			return manufactureOrders;
		}
		public void setManufactureOrders(List<ManufactureOrderBean> manufactureOrders) {
			this.manufactureOrders = manufactureOrders;
		}
    }
    
    public static class ManufactureOrderBean {
		private String moDate;
    	private String moNumber;
    	private String approveDate;
    	private Long quantityOrdered;
    	private Long quantityReceived;
    	private Long quantityCosting;
    	private Double cardCost;
    	private Long seriesMin;
    	private Long seriesMax;
    	
    	public ManufactureOrderBean() {
    	}
    	
    	public ManufactureOrderBean(LocalDate moDate,
				DateTime approveDate, Long seriesMin, Long seriesMax, Long quantityCosting) {
			super();
			this.moDate = moDate.toString("dd-MM-yyyy");
			this.approveDate = approveDate != null ? approveDate.toString("dd-MM-yyyy") : "n/a";
			this.seriesMin = seriesMin;
			this.seriesMax = seriesMax;
			this.quantityCosting = quantityCosting;
		}
    	
		public String getMoDate() {
			return moDate;
		}
		public void setMoDate(String moDate) {
			this.moDate = moDate;
		}
		public String getMoNumber() {
			return moNumber;
		}
		public void setMoNumber(String moNumber) {
			this.moNumber = moNumber;
		}
		public String getApproveDate() {
			return approveDate;
		}
		public void setApproveDate(String approveDate) {
			this.approveDate = approveDate;
		}
		public Long getQuantityOrdered() {
			return quantityOrdered;
		}
		public void setQuantityOrdered(Long quantityOrdered) {
			this.quantityOrdered = quantityOrdered;
		}
		public Long getQuantityReceived() {
			return quantityReceived;
		}
		public void setQuantityReceived(Long quantityReceived) {
			this.quantityReceived = quantityReceived;
		}
		public Long getQuantityCosting() {
			return quantityCosting;
		}
		public void setQuantityCosting(Long quantityCosting) {
			this.quantityCosting = quantityCosting;
		}
		public Double getCardCost() {
			return cardCost;
		}
		public void setCardCost(Double cardCost) {
			this.cardCost = cardCost;
		}

		public Long getSeriesMin() {
			return seriesMin;
		}

		public void setSeriesMin(Long seriesMin) {
			this.seriesMin = seriesMin;
		}

		public Long getSeriesMax() {
			return seriesMax;
		}

		public void setSeriesMax(Long seriesMax) {
			this.seriesMax = seriesMax;
		}
    }
}
