package com.transretail.crm.core.dto;


import com.fasterxml.jackson.annotation.JsonIgnore;
import com.mysema.query.types.Order;
import com.mysema.query.types.OrderSpecifier;
import com.mysema.query.types.expr.BooleanExpression;
import com.transretail.crm.common.service.dto.request.AbstractSearchFormDto;
import com.transretail.crm.core.entity.QEmployeePurchaseTxnModel;
import com.transretail.crm.core.entity.enums.MemberStatus;
import com.transretail.crm.core.entity.enums.TxnStatus;
import com.transretail.crm.core.entity.enums.VoucherTransactionType;
import com.transretail.crm.core.util.BooleanExprUtil;
import org.apache.commons.collections.CollectionUtils;
import org.joda.time.DateTime;
import org.springframework.util.StringUtils;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;


public class PurchaseTxnSearchDto extends AbstractSearchFormDto {

	public static enum PurchaseTxnField { member, txnDate };

	private String accountId;
	private String name;
	private String email;
	private String contact;
	private String ktpId;
	private String location;
	private Date postedAfter;
	private Date txnDateFrom;
	private Date txnDateTo;
	private Date txnDate;
	private String txnNo;
	private TxnStatus status;
	private VoucherTransactionType transactionType;
	private boolean isValidRecord;
	private String memberType;
	private MemberStatus memberStatus;
	private List<Long> memberIds;



	@Override
	@JsonIgnore
	public BooleanExpression createSearchExpression() {
		List<BooleanExpression> expr = new ArrayList<BooleanExpression>();
		QEmployeePurchaseTxnModel qPurchase = QEmployeePurchaseTxnModel.employeePurchaseTxnModel;

		if ( StringUtils.hasText( accountId ) ) {
            expr.add( BooleanExprUtil.INSTANCE.isStringPropertyLike( qPurchase.memberModel.accountId, accountId ) );
        }
		
		if ( StringUtils.hasText( contact ) ) {
            expr.add( BooleanExprUtil.INSTANCE.isStringPropertyLike( qPurchase.memberModel.contact, contact ) );
        }
		
		if ( StringUtils.hasText( ktpId ) ) {
            expr.add( BooleanExprUtil.INSTANCE.isStringPropertyLike( qPurchase.memberModel.idNumber, ktpId ) );
        }
		
        if ( StringUtils.hasText( email ) ) {
        	expr.add( BooleanExprUtil.INSTANCE.isStringPropertyLike( qPurchase.memberModel.email, email ) );
        }
        
        if (StringUtils.hasText( name ) ) {
        	expr.add( 
            		BooleanExprUtil.INSTANCE.isStringPropertyLike( qPurchase.memberModel.firstName, name).or( 
            		BooleanExprUtil.INSTANCE.isStringPropertyLike( qPurchase.memberModel.lastName, name) ) );
        }

        if ( StringUtils.hasText( location ) ) {
            expr.add( BooleanExprUtil.INSTANCE.isStringPropertyLike( qPurchase.store.name, location ) );
        }
        
        if ( StringUtils.hasText( txnNo ) ) {
            expr.add( BooleanExprUtil.INSTANCE.isStringPropertyLike( qPurchase.transactionNo, txnNo ) );
        }
        
		if ( null != postedAfter ) {
			expr.add( qPurchase.created.after( new DateTime( postedAfter.getTime() ) ) );
		}
		if ( null != txnDateFrom ) {
			expr.add( qPurchase.transactionDateTime.goe( txnDateFrom ) );
		}
		if ( null != txnDateTo ) {
			expr.add( qPurchase.transactionDateTime.loe( txnDateTo ) );
		}
		if ( null != txnDate ) {
            expr.add( qPurchase.transactionDateTime.eq( txnDate ) );
        }
		if ( null != status ) {
			expr.add( qPurchase.status.eq( status ) );
		}
		if ( isValidRecord ) {
			expr.add( qPurchase.transactionType.in( new VoucherTransactionType[]{ 
					VoucherTransactionType.PURCHASE, VoucherTransactionType.VOID, VoucherTransactionType.ADJUSTED } ) );
			expr.add( qPurchase.status.eq( TxnStatus.ACTIVE ) );
		}
		if (!StringUtils.isEmpty( memberType ) ) {
			expr.add( qPurchase.memberModel.memberType.code.equalsIgnoreCase( memberType ) );
		}
		if ( null != memberStatus ) {
			expr.add( qPurchase.memberModel.accountStatus.eq( memberStatus ) );
		}
		if ( CollectionUtils.isNotEmpty( memberIds ) ) {
			expr.add( qPurchase.memberModel.id.in( memberIds ) );
		}

		return BooleanExpression.allOf( expr.toArray( new BooleanExpression[expr.size()] ) );
	}

	public static OrderSpecifier<?> createOrderSpecifier( Order order, PurchaseTxnField field ) {
		if ( null == field ) {
			return null;
		}

		QEmployeePurchaseTxnModel qPurchase = QEmployeePurchaseTxnModel.employeePurchaseTxnModel;
		if ( order == Order.ASC ) {
			switch( field ) {
				case member		: return qPurchase.memberModel.accountId.asc();
				case txnDate	: return qPurchase.transactionDateTime.asc();
				default			: return null; 
			}
		}
		else {
			switch( field ) {
				case member		: return qPurchase.memberModel.accountId.desc();
				case txnDate	: return qPurchase.transactionDateTime.desc();
				default			: return null; 
			}
		}
	}



	public Date getPostedAfter() {
		return postedAfter;
	}
	public void setPostedAfter(Date postedAfter) {
		this.postedAfter = postedAfter;
	}
	public Date getTxnDateFrom() {
		return txnDateFrom;
	}
	public void setTxnDateFrom(Date txnDateFrom) {
		this.txnDateFrom = txnDateFrom;
	}
	public Date getTxnDateTo() {
		return txnDateTo;
	}
	public void setTxnDateTo(Date txnDateTo) {
		this.txnDateTo = txnDateTo;
	}
	public TxnStatus getStatus() {
		return status;
	}
	public void setStatus(TxnStatus status) {
		this.status = status;
	}
	public boolean isValidRecord() {
		return isValidRecord;
	}
	public void setValidRecord(boolean isValidRecord) {
		this.isValidRecord = isValidRecord;
	}
	public String getMemberType() {
		return memberType;
	}
	public void setMemberType(String memberType) {
		this.memberType = memberType;
	}
	public MemberStatus getMemberStatus() {
		return memberStatus;
	}
	public void setMemberStatus(MemberStatus memberStatus) {
		this.memberStatus = memberStatus;
	}
	public List<Long> getMemberIds() {
		return memberIds;
	}
	public void setMemberIds(List<Long> memberIds) {
		this.memberIds = memberIds;
	}

    public String getAccountId() {
        return accountId;
    }

    public void setAccountId(String accountId) {
        this.accountId = accountId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getContact() {
        return contact;
    }

    public void setContact(String contact) {
        this.contact = contact;
    }

    public String getKtpId() {
        return ktpId;
    }

    public void setKtpId(String ktpId) {
        this.ktpId = ktpId;
    }

    public VoucherTransactionType getTransactionType() {
        return transactionType;
    }

    public void setTransactionType(VoucherTransactionType transactionType) {
        this.transactionType = transactionType;
    }

    public String getTxnNo() {
        return txnNo;
    }

    public void setTxnNo(String txnNo) {
        this.txnNo = txnNo;
    }

    public Date getTxnDate() {
        return txnDate;
    }

    public void setTxnDate(Date txnDate) {
        this.txnDate = txnDate;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

}