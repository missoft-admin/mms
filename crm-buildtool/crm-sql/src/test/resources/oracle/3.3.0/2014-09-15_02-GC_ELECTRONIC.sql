alter table CRM_GC_ORD add IS_EGC char(1 char);
alter table CRM_GC_PROD_PROFILE add IS_EGC char(1 char);
alter table CRM_GC_ORD modify VENDOR number(19,0) null;

alter table CRM_GC_INVENTORY add (
        IS_EGC char(1 char),
        IS_EMAIL_SENT char(1 char),
        MOBILE_NO varchar2(50 char),
        EMAIL varchar2(50 char),
        PIN varchar2(20 char)
);