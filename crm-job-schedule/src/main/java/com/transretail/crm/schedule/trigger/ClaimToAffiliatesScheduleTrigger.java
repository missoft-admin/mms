package com.transretail.crm.schedule.trigger;

import com.transretail.crm.common.service.exception.GenericServiceException;
import com.transretail.crm.core.entity.ApplicationConfig;
import com.transretail.crm.core.entity.enums.AppConfigDefaults;
import com.transretail.crm.core.entity.enums.AppKey;
import com.transretail.crm.core.repo.ApplicationConfigRepo;
import java.text.ParseException;
import javax.annotation.Resource;
import org.apache.commons.lang3.StringUtils;
import org.quartz.CronExpression;
import org.quartz.JobDetail;
import org.quartz.Scheduler;
import org.quartz.impl.triggers.CronTriggerImpl;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.quartz.JobDetailAwareTrigger;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


/**
 *
 * @author Monte Cillo Co (mco@exist.com)
 */
@Service
public class ClaimToAffiliatesScheduleTrigger extends CronTriggerImpl implements InitializingBean {

    public static final String TRIGGER_NAME = "GC_CLAIMS_TO_AFFILIATES_JOB_TRIGGER";
    public static final String TRIGGER_GROUP = Scheduler.DEFAULT_GROUP;
    /**
     * CRON Expression used if no ApplicationConfig with key equal to
     * AppKey.GC_CLAIMS_TO_AFFILIATES_JOB in db
     */
    @Resource(name = "claimToAffiliatesJob")
    private JobDetail jobDetail;
    @Autowired
    private ApplicationConfigRepo applicationConfigRepo;

    @Override
    public void afterPropertiesSet() throws Exception {
        getJobDataMap().put(JobDetailAwareTrigger.JOB_DETAIL_KEY, jobDetail);
        setName(TRIGGER_NAME);
        setGroup(TRIGGER_GROUP);
        super.setCronExpression(getCronExpression());
        setJobKey(jobDetail.getKey());
    }

    @Override
    public String getCronExpression() {
        ApplicationConfig config = applicationConfigRepo.findByKey(AppKey.GC_CLAIMS_TO_AFFILIATES_JOB);
        return config != null && StringUtils.isNotBlank(config.getValue()) ? config.getValue()
                : AppConfigDefaults.DEFAULT_GC_CLAIMS_TO_AFFILIATES_CRON;
    }

    @Override
    @Transactional
    public void setCronExpression(String cronExpression) throws ParseException {
        if (!CronExpression.isValidExpression(cronExpression)) {
            throw new GenericServiceException("Invalid cron express [" + cronExpression + "].");
        }
        
        super.setCronExpression(cronExpression);
        ApplicationConfig config = applicationConfigRepo.findByKey(AppKey.GC_CLAIMS_TO_AFFILIATES_JOB);
        if (null == config) {
            config = new ApplicationConfig();
            config.setKey(AppKey.GC_CLAIMS_TO_AFFILIATES_JOB);
        }
        
        config.setValue(cronExpression);
        applicationConfigRepo.save(config);
    }
}