<%@include file="../common/taglibs.jsp" %>

  <spring:message code="application_config" var="typeName" />
  
  <div class="page-header page-header2">
      <h1><spring:message code="menutab_programsetup_permissions_appconfig" /></h1>
  </div>

	<div class="well well-blue">
	  <h3><a id="link_fetchdate" href="<c:url value="/appconfig/date/fetch" />" ><spring:message code="points_reward_fetch_date" />:</a> <span id="serverDate"></span>
	  <script type="text/javascript">
	  $(document).ready(function() {
	      $( "#link_fetchdate" ).click( function(e) {
	          e.preventDefault();
	          $.get( $(this).attr( "href" ), function(resp) { $( "#serverDate" ).html( " " + resp.result ); }, "json" );
	      }).click();
	  });
	  </script>
	  </h3>
	</div>

<div id="content_appconfig">
  <div id="list_appconfig"></div>
</div>


<div class="modal hide  nofly" tabindex="-1" role="dialog" aria-hidden="true" id="formDialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title"><spring:message code="label_update_arg" arguments="${typeName}" /></span></h4>
      </div>
      <div class="modal-body">
        <div id="contentError" class="hide alert alert-error">
          <button type="button" class="close" onclick="$(this).parent().hide();">&times;</button>
          <div><form:errors path="*"/></div>
        </div>
        <form id="appConfigForm" action="<c:url value="/appconfig/update" />" class="form-reset">
          <div style="margin-bottom: 5px;">
          <label for="name"><spring:message code="application_config_key"/></label>
          <span id="keyStr"></span>
          <input type="hidden" name="key" />
          </div>
          <div>
          <label for="name"><b class="required">*</b> <spring:message code="application_config_value"/></label><input type="text" name="value" placeholder="<spring:message code="application_config_value"/>" />
          </div>
        </form>
      </div>
      <div class="modal-footer">
        <button type="button" id="formSubmit" class="btn btn-primary"><spring:message code="label_submit" /></button>
        <button type="button" id="formCancel" data-dismiss="modal" class="btn btn-default"><spring:message code="label_cancel"/></button>
      </div>
    </div>
  </div>
</div>

  <link href="<spring:url value="/css/dataTables.css" />" rel="stylesheet">
  <script src="<spring:url value="/js/jquery/jquery.dataTables.js" />" ></script>
  <script src="<spring:url value="/js/jquery/jquery.dataTables.linkLengthMenu.js" />"></script>
  <script src="<spring:url value="/js/jquery/jquery.dataTables.customPaging.js" />"></script>
  <script src="<spring:url value="/js/jquery/ajax-datatable.js" />" ></script>
  <script src="<spring:url value="/js/common/form2js.js" />" ></script>
  <script src="<spring:url value="/js/common/json2.js" />" ></script>
  <script src="<spring:url value="/js/jquery/jquery.toObject.js" />" ></script>
  <link href="<c:url value="/styles/temp.css" />" rel="stylesheet" />

<script type="text/javascript">

$(document).ready(function() {
	var CONTAINER_ERROR = "#contentError > div";
	var CONTENT_ERROR = "#contentError";
	
	$("#formDialog").modal({
		show: false,
		keyboard:	false, 
		backdrop: "static",
	})
	.on('hidden.bs.modal', function() {
		$("#formDialog").find("input").val("");
		$("#formDialog").find(CONTENT_ERROR).hide();
		$("#formDialog").find(CONTAINER_ERROR).empty();
	})
	.css({
		"width": "300px"
	})
	;
	
	$("#list_appconfig").ajaxDataTable({
		'autoload'  : true,
		'ajaxSource' : "<c:url value="/appconfig/list" />",
		'columnHeaders' : [
			"<spring:message code="application_config_key"/>",
			"<spring:message code="application_config_value"/>",
			""
		],
		'modelFields' : [
			{name : 'key', sortable : true},
			{name : 'value', sortable : true},
			{customCell : function ( innerData, sSpecific, json ) {
				if (sSpecific == 'display') {
					return "<button class=\"btn btn-primary pull-left tiptip icn-edit\" title=\"<spring:message code="label_update"/>\" onclick=\"javascript:updateConfig('"+ json.key +"', '"+ json.value +"');\"></button>";
					/* var btnHtml = $("#cityBtn").html();
					var rg = new RegExp("%ITEMID%", 'g');
					btnHtml = btnHtml.replace(rg, json.id);
					rg = new RegExp("%ITEMNAME%", 'g');
					btnHtml = btnHtml.replace(rg, json.name);
					rg = new RegExp("%PROVINCEID%", 'g');
					btnHtml = btnHtml.replace(rg, $("#createCity").data("provinceId"));
					return btnHtml; */
				} else {
					return "";
				}
			}
			}
		]
	}); 
	
	$("#formSubmit").click(submitForm);
	
	function submitForm(e) {
		e.preventDefault();
		$( "#formSubmit" ).prop( "disabled", true );
		$.post($("#appConfigForm").attr("action"), $("#appConfigForm").serialize(), function(data) {
			$( "#formSubmit" ).prop( "disabled", false );
			if (data.success) {
				location.reload();
			} 
			else {
				errorInfo = "";
				for (i = 0; i < data.result.length; i++) {
					errorInfo += "<br>" + (i + 1) + ". "
							+ ( data.result[i].code != undefined ? 
									data.result[i].code : data.result[i]);
				}
				$("#formDialog").find(CONTAINER_ERROR).html("Please correct following errors: " + errorInfo);
				$("#formDialog").find(CONTENT_ERROR).show('slow');
			}
		});	
	}
	
});

function updateConfig(key, value) {
	$modal = $("#formDialog");
	$modal.find("[name='key']").val(key);
	$modal.find("#keyStr").text(key);
	$modal.find("[name='value']").val(value);
	$modal.modal("show");
}

</script>