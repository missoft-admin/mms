$.fn.dataTableExt.oPagination.custom_paging = {
    // ctx is defined in default.jsp
    "contextPath" : ctx !== 'undefined' ? ctx : '',
    "o" : {
        ends : 2,
        aroundCurrent : 1,
        currentClass : 'current',
        distanceSpacer : ' &#133; ',
        adjacentSpacer : ' | ',
        link : '<a href="#" onclick="return false;">{page}</a>'
    },
    "updatePreviousButton" : function ( oSettings, nPaging, fnCallbackDraw ) {
        var $nPrevious = $( 'span.previous', nPaging );
        if ( $nPrevious.length == 0 ) {
            $nPrevious = $( document.createElement( 'span' ) ).addClass( 'previous' );
            $nPrevious.html( '<img src="' + this.contextPath + '/images/icn-prev.png"> ' + oSettings.oLanguage.oPaginate.sPrevious + '&nbsp;' );
            $nPrevious.appendTo( nPaging );

            $nPrevious.click( function () {
                if ( oSettings._iDisplayLength >= 0
                        && oSettings.fnRecordsDisplay() > 0
                        && Math.ceil( oSettings._iDisplayStart / oSettings._iDisplayLength ) > 0 ) {
                    oSettings.oApi._fnPageChange( oSettings, "previous" );
                    fnCallbackDraw( oSettings );
                }
            } );

            $nPrevious.bind( 'selectstart', function () {
                return false;
            } );
        }

        if ( oSettings._iDisplayLength < 0
                || oSettings.fnRecordsDisplay() < 1
                || Math.ceil( oSettings._iDisplayStart / oSettings._iDisplayLength ) == 0 ) {
            $nPrevious.addClass( 'disabled' );
        } else {
            $nPrevious.removeClass( 'disabled' );
        }
    },
    "updateNextButton" : function ( oSettings, nPaging, fnCallbackDraw ) {
        var $nNext = $( 'span.next', nPaging );
        if ( $nNext.length == 0 ) {
            $nNext = $( document.createElement( 'span' ) ).addClass( 'next' );
            $nNext.html( ' ' + oSettings.oLanguage.oPaginate.sNext + ' <img src="' + this.contextPath + '/images/icn-next.png">' );
            $nNext.appendTo( nPaging );

            $nNext.click( function () {
                if ( oSettings._iDisplayLength >= 0
                        && oSettings.fnRecordsDisplay() > 0
                        && Math.ceil( oSettings._iDisplayStart / oSettings._iDisplayLength )
                        != (Math.ceil( (oSettings.fnRecordsDisplay()) / oSettings._iDisplayLength ) ) - 1 ) {
                    oSettings.oApi._fnPageChange( oSettings, "next" );
                    fnCallbackDraw( oSettings );
                }
            } );

            $nNext.bind( 'selectstart', function () {
                return false;
            } );
        }

        if ( oSettings._iDisplayLength < 0
                || oSettings.fnRecordsDisplay() < 1
                || Math.ceil( oSettings._iDisplayStart / oSettings._iDisplayLength )
                == (Math.ceil( (oSettings.fnRecordsDisplay()) / oSettings._iDisplayLength ) ) - 1 ) {
            $nNext.addClass( 'disabled' );
        } else {
            $nNext.removeClass( 'disabled' );
        }
    },
    "updatePageNumberButtons" : function ( oSettings, nPaging, fnCallbackDraw ) {
        var $pages = $( 'span.page-numbers', nPaging );
        if ( $pages.length == 0 ) {
            $pages = $( document.createElement( 'span' ) ).addClass( 'page-numbers' );
            $pages.appendTo( nPaging );

            $pages.on( 'click', 'a', function () {
                oSettings.oApi._fnPageChange( oSettings, parseInt( $( this ).attr( 'data-page' ) ) - 1 );
                fnCallbackDraw( oSettings );
            } );
        }

        $pages.html( '' );
        if ( oSettings._iDisplayLength != -1 && oSettings.fnRecordsDisplay() > 0 ) {
            var totalPages = Math.ceil( (oSettings.fnRecordsDisplay()) / oSettings._iDisplayLength );
            if ( totalPages > 1 ) {
                var pageNo = Math.ceil( oSettings._iDisplayStart / oSettings._iDisplayLength );
                var o = this.o;
                var i, t = [],
                        cur = pageNo + 1,
                        start = cur > 1 ? (totalPages - cur < o.aroundCurrent ? -(o.aroundCurrent + 1) + (totalPages - cur) : -o.aroundCurrent) : 0,
                        end = cur < o.aroundCurrent + 1 ? o.aroundCurrent + 3 - cur : o.aroundCurrent + 1;
                for ( i = start; i < end; i++ ) {
                    if ( cur + i >= 1 && cur + i < totalPages ) {
                        t.push( cur + i );
                    }
                }
                // include first and last pages (ends) in the pagination
                for ( i = 0; i < o.ends; i++ ) {
                    if ( $.inArray( i + 1, t ) === -1 ) {
                        t.push( i + 1 );
                    }
                    if ( $.inArray( totalPages - i, t ) === -1 ) {
                        t.push( totalPages - i );
                    }
                }
                // sort the list
                t = t.sort( function ( a, b ) {
                    return a - b;
                } );

                // make links and spacers
                $.each( t, function ( j, v ) {
                    $pages
                            .append( $( o.link.replace( /\{page\}/g, v ) ).toggleClass( o.currentClass, v === cur ).attr( 'data-page', v ) )
                            .append( '<span>' + (j < t.length - 1 && ( t[j + 1] - 1 !== v ) ? o.distanceSpacer : ( j >= t.length - 1 ? '' : o.adjacentSpacer )) + '</span>' );
                } );
            }
        }
    },
    /*
     * Function: oPagination.custom_paging.fnInit
     * Purpose:  Initalise dom elements required for pagination with a list of the pages
     * Returns:  -
     * Inputs:   object:oSettings - dataTables settings object
     *           node:nPaging - the DIV which contains this pagination control
     *           function:fnCallbackDraw - draw function which must be called on update
     */
    "fnInit" : function ( oSettings, nPaging, fnCallbackDraw ) {
        this.updatePreviousButton( oSettings, nPaging, fnCallbackDraw );
        this.updatePageNumberButtons( oSettings, nPaging, fnCallbackDraw );
        this.updateNextButton( oSettings, nPaging, fnCallbackDraw );
    },

    /*
     * Function: oPagination.custom_paging.fnUpdate
     * Purpose:  Update the list of page buttons shows
     * Returns:  -
     * Inputs:   object:oSettings - dataTables settings object
     *           function:fnCallbackDraw - draw function which must be called on update
     */
    "fnUpdate" : function ( oSettings, fnCallbackDraw ) {
        if ( !oSettings.aanFeatures.p ) {
            return;
        }

        /* Loop over each instance of the pager */
        var nNode = null;
        var an = oSettings.aanFeatures.p;
        for ( var i = 0, iLen = an.length; i < iLen; i++ ) {
            nNode = an[i];
            if ( !nNode.hasChildNodes() ) {
                continue;
            }

            this.updatePreviousButton( oSettings, nNode, fnCallbackDraw );
            this.updatePageNumberButtons( oSettings, nNode, fnCallbackDraw );
            this.updateNextButton( oSettings, nNode, fnCallbackDraw );
        }
    }
};