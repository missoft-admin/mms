package com.transretail.crm.report.template.impl;


import com.google.common.base.Function;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.mysema.query.Tuple;
import com.mysema.query.group.GroupBy;
import com.mysema.query.jpa.JPASubQuery;
import com.mysema.query.jpa.impl.JPAQuery;
import com.mysema.query.types.OrderSpecifier;
import com.mysema.query.types.expr.BooleanExpression;
import com.mysema.query.types.expr.CaseBuilder;
import com.mysema.query.types.expr.NumberExpression;
import com.mysema.query.types.expr.StringExpression;
import com.mysema.query.types.path.DatePath;
import com.transretail.crm.common.reporting.jasper.ClasspathFileResolver;
import com.transretail.crm.common.reporting.jasper.DefaultJRProcessor;
import com.transretail.crm.common.reporting.jasper.JRProcessor;
import com.transretail.crm.core.entity.QPeopleSoftStore;
import com.transretail.crm.core.entity.enums.ReportType;
import com.transretail.crm.core.entity.lookup.LookupDetail;
import com.transretail.crm.core.entity.lookup.QLookupDetail;
import com.transretail.crm.core.entity.lookup.QStore;
import com.transretail.crm.core.service.CodePropertiesService;
import com.transretail.crm.core.service.LookupService;
import com.transretail.crm.core.service.StoreService;
import com.transretail.crm.giftcard.entity.*;
import com.transretail.crm.giftcard.entity.support.GiftCardInventoryStatus;
import com.transretail.crm.giftcard.entity.support.GiftCardSalesType;
import com.transretail.crm.giftcard.entity.support.SalesOrderStatus;
import com.transretail.crm.giftcard.service.ProductProfileService;
import com.transretail.crm.report.template.CommonReportFilter;
import com.transretail.crm.report.template.FilterField;
import com.transretail.crm.report.template.ReportTemplate;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang3.EnumUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.time.DateUtils;
import org.joda.time.LocalDate;
import org.joda.time.LocalDateTime;
import org.joda.time.Months;
import org.joda.time.YearMonth;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.MessageSourceAware;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.context.support.MessageSourceAccessor;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.Map.Entry;


@Service( "giftCardSoSummaryReport" )
public final class GiftCardSoSummaryReport implements ReportTemplate, MessageSourceAware {

    private MessageSourceAccessor messageSource;
    @Autowired
    StoreService storeService;
    @Autowired
    ProductProfileService productProfileService;
    @Autowired
    CodePropertiesService codePropertiesService;
    @Autowired
    LookupService lookupService;
    @PersistenceContext
    protected EntityManager em;

    private static final String DATE_FORMAT_FILTER = "MMM-yyyy";
    private static final String DATE_FORMAT_2 = "MMM yyyy";
    private static final String DATE_FORMAT_3 = "MMMM yyyy";
    private static final String DATE_FORMAT_4 = "yyyyMM";
    private static final String REPORTFILTER_ALL = "";
	private static enum ReportFilter { PRODUCT, SALES_STORE, SALES_TYPE, REGION, ACTIVATION_STAT, ALL };
	private static enum SoActivationStat { ACTIVATED, PARTIALLY_ACTIVATED, NON_ACTIVATED };



	@Override
	public void setMessageSource( MessageSource messageSource ) {
        this.messageSource = new MessageSourceAccessor( messageSource );
	}

    @Override
    public ReportType[] getReportTypes() {
    	return ReportType.values();
    }

    @Override
    public String getName() {
    	return "Gift Card Order Summary";
    }

    @Override
    public String getDescription() {
    	return getName();
    }

    @Override
    public Set<FilterField> getFilters() {
        Set<FilterField> filters = new LinkedHashSet<FilterField>(2);
        filters.add( FilterField.createDateField( CommonReportFilter.DATE_FROM,
            messageSource.getMessage( "report.misc.datefrom" ), DATE_FORMAT_FILTER, FilterField.DateTypeOption.YEAR_MONTH , true) );
        filters.add( FilterField.createDateField( CommonReportFilter.DATE_TO,
            messageSource.getMessage( "report.misc.dateto" ), DATE_FORMAT_FILTER, FilterField.DateTypeOption.YEAR_MONTH , true) );

        Map<String, String> filter = Maps.newLinkedHashMap();

        QGiftCardInventory qGcInv = QGiftCardInventory.giftCardInventory;
        QStore qStore = QStore.store;
        QProductProfile qProd= QProductProfile.productProfile;
    	final String hoCode = codePropertiesService.getDetailInvLocationHeadOffice();
    	//final String hoName = lookupService.getActiveDetailByCode( hoCode ).getDescription();
    	final StringExpression qLocationPath = qGcInv.location;
    	final StringExpression qStoreCodePath = qStore.code;
    	//final StringExpression qStoreNamePath = qStore.name;
    	//final StringExpression qStorePath = new CaseBuilder().when( qLocationPath.equalsIgnoreCase( hoCode ) ).then( hoName ).otherwise( qStoreNamePath );
        JPAQuery query = new JPAQuery( em )
        	.from( qGcInv, qStore )
        	.where( BooleanExpression.allOf( 
				qLocationPath.equalsIgnoreCase( qStoreCodePath ).or( qLocationPath.equalsIgnoreCase( hoCode ) ),
				qGcInv.status.eq( GiftCardInventoryStatus.ACTIVATED ) ) )
			.orderBy( qGcInv.salesType.stringValue().asc() ).distinct();
        filter.put( ReportFilter.ALL.name(), REPORTFILTER_ALL/*messageSource.getMessage( "report.misc.all" )*/ );
        filter.putAll( query.map( qGcInv.salesType.stringValue(), qGcInv.salesType.stringValue() ) );
        filters.add( FilterField.createDropdownField( ReportFilter.SALES_TYPE.name(), messageSource.getMessage( "report.gc.ordersummary.salestype" ), filter ) );

        filter = Maps.newLinkedHashMap();
        filter.put( ReportFilter.ALL.name(), REPORTFILTER_ALL );
        filter.putAll(  new JPAQuery( em ).from( qProd ).distinct().orderBy( qProd.productDesc.asc() ).map( qProd.productCode, qProd.productDesc )  );
        filters.add( FilterField.createDropdownField( ReportFilter.PRODUCT.name(), messageSource.getMessage( "report.gc.ordersummary.product" ), filter ) );

        Set<Entry<String, String>> filterSet = new LinkedHashSet<Map.Entry<String,String>>();
        filterSet.add( new AbstractMap.SimpleEntry<String, String>( ReportFilter.ALL.name(), REPORTFILTER_ALL ) );
        filterSet.addAll( new JPAQuery( em ).from( qStore ).distinct().orderBy( qStore.code.asc() )
    		.map( qStore.code, qStore.code.concat( " - " ).concat( qStore.name ) ).entrySet() );
        filters.add( FilterField.createEntrySetDropdownField( ReportFilter.SALES_STORE.name(), messageSource.getMessage( "report.gc.ordersummary.salesstore" ), filterSet ) );

        filter = Maps.newLinkedHashMap();
        filter.put( ReportFilter.ALL.name(), REPORTFILTER_ALL );
        List<LookupDetail> brs = lookupService.getActiveDetailsByHeaderCode( 
        		codePropertiesService.getHeaderBusinessRegion(), new OrderSpecifier<?>[]{ QLookupDetail.lookupDetail.description.asc() } );
        for ( LookupDetail br : brs ) {
            filter.put( br.getCode(), br.getDescription() );
        }
        filters.add( FilterField.createDropdownField( ReportFilter.REGION.name(), messageSource.getMessage( "report.gc.ordersummary.region" ), filter ) );

        filter = Maps.newLinkedHashMap();
        filter.put( ReportFilter.ALL.name(), REPORTFILTER_ALL );
        for ( SoActivationStat stat : SoActivationStat.values() ) {
            filter.put( stat.name(),  messageSource.getMessage( "report.gc.actvnstat." + stat.name().toLowerCase() ) );
        }
        filters.add( FilterField.createDropdownField( ReportFilter.ACTIVATION_STAT.name(), messageSource.getMessage( "report.gc.activationstat" ), filter ) );

		return filters;
    }

    @Override
    public boolean canView( Set<String> perms ) {
		/*if ( CollectionUtils.isNotEmpty( perms ) ) { for ( String perm : perms ) { if ( perm.equalsIgnoreCase( PERMISSION ) ) { return true; } } }*/
		return true;
    }

    @Override
    public JRProcessor createJrProcessor( Map<String, String> map ) {
    	Map<String, Date> dateRange = getDateRange( map );
    	LocalDate dateFrom = new LocalDate( dateRange.get( CommonReportFilter.DATE_FROM ).getTime() );
    	LocalDate dateTo = new LocalDate( dateRange.get( CommonReportFilter.DATE_TO ).getTime() );
		String salesType = map.get( ReportFilter.SALES_TYPE.name() );
		String productCode = map.get( ReportFilter.PRODUCT.name() );
		String salesStore = map.get( ReportFilter.SALES_STORE.name() );
		String region = map.get( ReportFilter.REGION.name() );
		List<String> regionStoreCodes = null;
		if ( StringUtils.isNotBlank( region ) ) {
			QPeopleSoftStore psoftStore = QPeopleSoftStore.peopleSoftStore;
			regionStoreCodes = new JPAQuery( em ).from( psoftStore ).where( psoftStore.businessRegion.code.equalsIgnoreCase( region ) ).list( psoftStore.store.code.toLowerCase() );
		}
		String activationStatus = map.get( ReportFilter.ACTIVATION_STAT.name() );

    	final String hoCode = codePropertiesService.getDetailInvLocationHeadOffice();
    	final String hoName = lookupService.getActiveDetailByCode( hoCode ).getDescription();
    	final QStore qStore = QStore.store;
    	final QGiftCardInventory qGcInv = QGiftCardInventory.giftCardInventory;
    	final String LBL_FACEAMT = messageSource.getMessage( "report.gc.ordersummary.faceamt", LocaleContextHolder.getLocale() );
    	final String LBL_ACTUALSALES = messageSource.getMessage( "report.gc.ordersummary.actualsales", LocaleContextHolder.getLocale() );
    	final String LBL_SALESRATE = messageSource.getMessage( "report.gc.ordersummary.salesrate", LocaleContextHolder.getLocale() );

    	NumberExpression<BigDecimal> sumEx = new CaseBuilder().when( qGcInv.ryns.isNull().or( qGcInv.ryns.eq( Boolean.FALSE ) ) ).then( qGcInv.faceValue ).otherwise( new BigDecimal( 0 ) ).sum();
    	final DatePath<LocalDate> qDate = qGcInv.activationDate;
    	final StringExpression qLocationPath = qGcInv.location;
    	final StringExpression qStoreCodePath = qStore.code;
    	final StringExpression qStoreNamePath = qStore.name;
    	final StringExpression qStorePath = new CaseBuilder().when( qLocationPath.equalsIgnoreCase( hoCode ) ).then( qLocationPath ).otherwise( qStoreNamePath );

    	BooleanExpression filterBoolExpr = BooleanExpression.allOf(
			qGcInv.status.eq( GiftCardInventoryStatus.ACTIVATED ),
			qDate.between( dateFrom, dateTo ), 
			StringUtils.isBlank( salesType ) || salesType.equalsIgnoreCase( ReportFilter.ALL.name() )? 
				null : qGcInv.salesType.eq( GiftCardSalesType.valueOf( salesType ) ), 
			StringUtils.isBlank( productCode ) || productCode.equalsIgnoreCase( ReportFilter.ALL.name() )? 
				null : qGcInv.productCode.equalsIgnoreCase( productCode ), 
			StringUtils.isBlank( salesStore ) || salesStore.equalsIgnoreCase( ReportFilter.ALL.name() )? 
				null : qGcInv.location.equalsIgnoreCase( salesStore ), 
			StringUtils.isBlank( region ) || region.equalsIgnoreCase( ReportFilter.ALL.name() )? 
				null : qGcInv.location.toLowerCase().in( regionStoreCodes ), 
			StringUtils.isBlank( activationStatus ) || activationStatus.equalsIgnoreCase( ReportFilter.ALL.name() )? 
				null : createActvnStatExpr( EnumUtils.getEnum( SoActivationStat.class, activationStatus ) ) );
    	BooleanExpression boolExpr = BooleanExpression.allOf( /*qSo.status.eq( SalesOrderStatus.SOLD ),*/
    		qGcInv.isEgc.isFalse().or( qGcInv.isEgc.isNull() ),
			qLocationPath.equalsIgnoreCase( qStoreCodePath ).or( qLocationPath.equalsIgnoreCase( hoCode ) ),
			filterBoolExpr );

    	List<CrosstabBean> summary = new ArrayList<CrosstabBean>();
    	JPAQuery query = new JPAQuery( em )
    		/*.from( qSo ).leftJoin( qSo.items, qSoItem ).leftJoin( qSoItem.soAlloc, qSoAlloc ).leftJoin( qSoAlloc.gcInventory, )*/
			.from( qGcInv, qStore )
    		.where( boolExpr )
			.groupBy( qLocationPath, qStoreNamePath );
    	List<Tuple> tuples = query.list( qLocationPath, qGcInv.faceValue.sum(), sumEx, sumEx.divide( qGcInv.faceValue.sum() ), qStorePath );
		List<CrosstabBean> faceAmtSummary = Lists.transform( tuples,
				new Function<Tuple, CrosstabBean>() {
				@Override
				public CrosstabBean apply( Tuple f ) {
					BigDecimal val = f.get( 1, BigDecimal.class );
					if ( null == f.get( qLocationPath ) || null == val || 0 == val.compareTo( new BigDecimal( 0 ) ) ) {
						return null;
					}
					boolean isHo = f.get( 0, String.class ).equalsIgnoreCase( hoCode );
					String storeName = " - " + ( isHo? hoName : f.get( 4, String.class ) );
					return new CrosstabBean( null, f.get( 0, String.class ).concat( storeName ), LBL_FACEAMT, val + "" );
				}
			});
		summary.addAll( faceAmtSummary );

		List<CrosstabBean> actualSalesSummary = Lists.transform( tuples,
				new Function<Tuple, CrosstabBean>() {
				@Override
				public CrosstabBean apply( Tuple f ) {
					BigDecimal val = f.get( 2, BigDecimal.class );
					if ( null == f.get( qLocationPath ) || null == val || 0 == val.compareTo( new BigDecimal( 0 ) ) ) {
						return null;
					}
					boolean isHo = f.get( 0, String.class ).equalsIgnoreCase( hoCode );
					String storeName = " - " + ( isHo? hoName : f.get( 4, String.class ) );
					return new CrosstabBean( null, f.get( 0, String.class ).concat( storeName ), LBL_ACTUALSALES, val + "");
				}
			});
		summary.addAll( actualSalesSummary );

		List<CrosstabBean> salesRateSummary = Lists.transform( tuples,
				new Function<Tuple, CrosstabBean>() {
				@Override
				public CrosstabBean apply( Tuple f ) {
					BigDecimal val = f.get( 3, BigDecimal.class );
                    val = val.setScale(2, RoundingMode.CEILING);
					if ( null == f.get( qLocationPath ) || null == val || 0 == val.compareTo( new BigDecimal( 0 ) ) ) {
						return null;
					}
					boolean isHo = f.get( 0, String.class ).equalsIgnoreCase( hoCode );
					String storeName = " - " + ( isHo? hoName : f.get( 4, String.class ) );
					return new CrosstabBean( null, f.get( 0, String.class ).concat( storeName ), LBL_SALESRATE, val +"%");
				}
			});
		summary.addAll( salesRateSummary );

		List<CrosstabBean> monthlySummaryFix = Lists.newArrayList();
		if ( CollectionUtils.isNotEmpty( summary ) ) {
			query.groupBy( qDate, qStorePath ).orderBy( qLocationPath.asc() );
			Map<String, String> tr = query.transform( GroupBy.groupBy( qLocationPath ).as( qStorePath/*, set( qDate.yearMonth() )*/ ) );
			int months = Months.monthsBetween( dateFrom, dateTo ).getMonths();
			if ( MapUtils.isNotEmpty( tr ) ) {
				for ( Entry<String, String> entry: tr.entrySet() ) {
					boolean isHo = entry.getKey().equalsIgnoreCase( hoCode );
					String storeName = entry.getKey() + " - " + ( isHo? hoName : entry.getValue() );
			        for ( int i = 0; i <= months; i++ ) {
			            LocalDate month = dateFrom.plusMonths( i );
			        	//if ( !entry.getValue().contains( Integer.parseInt( month.toString( DATE_FORMAT_4 ) ) ) ) {
				            CrosstabBean cb = new CrosstabBean( month.toDate(), storeName, month.toString( DATE_FORMAT_2 ), new BigDecimal( 0 ) + "" );
				            monthlySummaryFix.add( cb );
			        	//}
			        }
					break;
				}
			}
		}

		query = new JPAQuery( em )
			/*.from( qSo ).leftJoin( qSo.items, qSoItem ).leftJoin( qSoItem.soAlloc, qSoAlloc ).leftJoin( qSoAlloc.gcInventory, )*/
			.from( qGcInv, qStore )
			.where( boolExpr )
    		.groupBy( qLocationPath, qStoreNamePath, qDate.yearMonth()/*, qSo.orderType*/ );
		tuples = query.list( qLocationPath, qDate.yearMonth(), sumEx, qGcInv.faceValue.sum(), qStorePath );
		List<CrosstabBean> monthlySummary = Lists.transform( tuples,
			new Function<Tuple, CrosstabBean>() {
				@Override
				public CrosstabBean apply( Tuple f ) {
					BigDecimal val = f.get( 2, BigDecimal.class );
					if ( null == f.get( qLocationPath ) || null == f.get( qDate.yearMonth() ) 
							|| null == val || 0 == val.compareTo( new BigDecimal( 0 ) ) ) {
						return null;
					}
					Date date = YearMonth.parse( String.valueOf( f.get( 1, Integer.class ) ), DateTimeFormat.forPattern( DATE_FORMAT_4 ) ).toLocalDate( 1 ).toDate();
					boolean isHo = f.get( 0, String.class ).equalsIgnoreCase( hoCode );
					String storeName = " - " + ( isHo? hoName : f.get( 4, String.class ) );
					return new CrosstabBean( date, f.get( 0, String.class ).concat( storeName ), new SimpleDateFormat( DATE_FORMAT_2 ).format( date ), val + "");
				}
			});
	    List<CrosstabBean> monthlySummarySort = new ArrayList<GiftCardSoSummaryReport.CrosstabBean>();
	    monthlySummarySort.addAll( monthlySummaryFix );
	    monthlySummarySort.addAll( monthlySummary );
	    /*Collections.sort( monthlySummarySort, new Comparator<CrosstabBean>() { @Override public int compare( CrosstabBean a, CrosstabBean b ) { return DateTimeComparator.getDateOnlyInstance().compare( a.getDate(), b.getDate() ); } });*/
		summary.addAll( monthlySummarySort );
		summary.removeAll( Collections.singleton( null ) );
	    Collections.sort( summary, new Comparator<CrosstabBean>() {
            @Override
            public int compare( CrosstabBean a, CrosstabBean b ) {
                return a.getColumn().compareTo( b.getColumn() );
            }
        });


		Map<String, Object> params = new HashMap<String, Object>();
		DateTimeFormatter df = DateTimeFormat.forPattern(  DATE_FORMAT_3 );
		params.put( "dateFrom", df.print( dateFrom ) );
		params.put( "dateTo", df.print( dateTo ) );
		params.put( "salesType", salesType );
		if ( StringUtils.isNotBlank( productCode ) ) {
			params.put( "product", productCode.equalsIgnoreCase( ReportFilter.ALL.name() )? productCode
					: productCode.concat( " - " ).concat( productProfileService.findProductProfileByCode( productCode ).getProductDesc() ) );
		}
		if ( StringUtils.isNotBlank( salesStore ) ) {
			params.put( "salesStore", salesStore.equalsIgnoreCase( ReportFilter.ALL.name() )? salesStore
					: salesStore.concat( " - " ).concat( StringUtils.equalsIgnoreCase( salesStore, hoCode )? hoName : storeService.getStoreByCode( salesStore ).getName() ) );
		}
		if ( StringUtils.isNotBlank( region ) ) {
			params.put( "region", region.equalsIgnoreCase( ReportFilter.ALL.name() )? region 
					: lookupService.getActiveDetailByCode( region ).getDescription() );
		}
		if ( StringUtils.isNotBlank( activationStatus ) ) {
			params.put( "activationStatus", activationStatus.equalsIgnoreCase( ReportFilter.ALL.name() )? activationStatus 
					: messageSource.getMessage( "report.gc.actvnstat." + activationStatus.toLowerCase() ) );
		}
		params.put( "printDate", df.print( new LocalDateTime() ) );
		params.put( "isEmpty", CollectionUtils.isEmpty( summary ) );
		//params.put( "reportList", summary );

		DefaultJRProcessor jrProcessor = new DefaultJRProcessor( "reports/gift_card_order_summary.jasper", summary ); //default-param=REPORT_DATA_SOURCE 
		jrProcessor.setParameters( params );
		jrProcessor.setFileResolver( new ClasspathFileResolver( "reports" ) );
		return jrProcessor;
    }



    private Map<String, Date> getDateRange( Map<String, String> map ) {
    	Map<String, Date> dateRange = Maps.newHashMap();

    	LocalDate dateFrom =  LocalDate.parse( map.get( CommonReportFilter.DATE_FROM ), DateTimeFormat.forPattern( DATE_FORMAT_FILTER ) );
    	LocalDate dateTo =  LocalDate.parse( map.get( CommonReportFilter.DATE_TO ), DateTimeFormat.forPattern( DATE_FORMAT_FILTER ) );
		Date startDate = null != dateFrom? dateFrom.toDateTimeAtStartOfDay().toDate() : null;
		Date endDate = null != dateTo? dateTo.toDateMidnight().toDate() : null;;
		if ( null == startDate && null != endDate ) {
			startDate = DateUtils.addDays( endDate, -30 );
    	}
		else if ( null != startDate && null == endDate ) {
			endDate = DateUtils.addDays( startDate, 30 );
    	}
		else if ( null == startDate && null == endDate ) {
			startDate = DateUtils.truncate( new Date(), Calendar.MONTH );
			endDate = DateUtils.ceiling( new Date(), Calendar.MONTH );
		}

		dateRange.put( CommonReportFilter.DATE_FROM, DateUtils.truncate( startDate, Calendar.DATE ) );
		dateRange.put( CommonReportFilter.DATE_TO, DateUtils.addMilliseconds( DateUtils.ceiling( endDate, Calendar.MONTH ), -1 ) );
    	return dateRange;
    }

    private BooleanExpression createActvnStatExpr( SoActivationStat actvnStat ) {
    	QGiftCardInventory qGcInv = QGiftCardInventory.giftCardInventory;
    	QSalesOrder qSo = QSalesOrder.salesOrder;
    	QSalesOrderItem qSoItem = QSalesOrderItem.salesOrderItem;
    	QSalesOrderAlloc qSoAlloc = QSalesOrderAlloc.salesOrderAlloc;
    	QReturnRecord qRet = QReturnRecord.returnRecord;
    	if ( SoActivationStat.ACTIVATED == actvnStat ) {
    		return BooleanExpression.allOf(
    			qGcInv.id.in( new JPASubQuery()
    				.from( qSo )
    				.leftJoin( qSo.items, qSoItem ).leftJoin( qSoItem.soAlloc, qSoAlloc )
            		//.leftJoin( qSoAlloc.gcInventory, qGcInv )
            		.innerJoin( qGcInv ).on( qGcInv.series.goe( qSoAlloc.seriesStart ).and( qGcInv.series.loe( qSoAlloc.seriesEnd ) ) )
    				.where( BooleanExpression.allOf( qSo.status.eq( SalesOrderStatus.SOLD ) ) )
    				.list( qGcInv.id ) ) );
    	}
    	else if ( SoActivationStat.PARTIALLY_ACTIVATED == actvnStat ) {
    		return BooleanExpression.allOf(
    			qGcInv.id.in( new JPASubQuery()
    				.from( qSo )
    				.leftJoin( qSo.items, qSoItem ).leftJoin( qSoItem.soAlloc, qSoAlloc )
            		//.leftJoin( qSoAlloc.gcInventory, qGcInv )
            		.innerJoin( qGcInv ).on( qGcInv.series.goe( qSoAlloc.seriesStart ).and( qGcInv.series.loe( qSoAlloc.seriesEnd ) ) )
    				.rightJoin( qRet.orderNo, qSo )
    				.where( BooleanExpression.allOf( qSo.status.eq( SalesOrderStatus.SOLD ) ) )
    				.list( qGcInv.id ) ) );
    	}
    	else if ( SoActivationStat.NON_ACTIVATED == actvnStat ) {
    		return BooleanExpression.allOf(
    			qGcInv.id.in( new JPASubQuery()
    				.from( qSo )
    				.leftJoin( qSo.items, qSoItem ).leftJoin( qSoItem.soAlloc, qSoAlloc )
            		//.leftJoin( qSoAlloc.gcInventory, qGcInv )
            		.innerJoin( qGcInv ).on( qGcInv.series.goe( qSoAlloc.seriesStart ).and( qGcInv.series.loe( qSoAlloc.seriesEnd ) ) )
    				.rightJoin( qRet.orderNo, qSo )
    				.where( BooleanExpression.allOf( qSo.status.ne( SalesOrderStatus.SOLD ) ) )
    				.list( qGcInv.id ) ) );
    	}
		return null;
    }



    public static class CrosstabBean {
		private Date date;
		private String column;
		private String row;
		private String value;

		public CrosstabBean() {}
		public CrosstabBean( Date date, String column, String row, String value ) {
			this.date = date;
			this.column = column;
			this.row = row;
			this.value = value;
		}

		public Date getDate() {
			return date;
		}
		public String getColumn() {
			return column;
		}
		public String getRow() {
			return row;
		}
		public String getValue() {
			return value;
		}
	}

}
