package com.transretail.crm.report.template.impl;

import com.google.common.collect.Maps;
import com.mysema.query.jpa.impl.JPAQuery;
import com.transretail.crm.common.reporting.jasper.JRProcessor;
import com.transretail.crm.common.reporting.service.ReportService;
import com.transretail.crm.core.entity.MemberModel;
import com.transretail.crm.core.entity.embeddable.*;
import com.transretail.crm.core.entity.enums.MemberCreatedFromType;
import com.transretail.crm.core.entity.enums.MemberStatus;
import com.transretail.crm.core.entity.enums.Status;
import com.transretail.crm.core.entity.lookup.*;
import com.transretail.crm.core.repo.*;
import com.transretail.crm.core.service.CodePropertiesService;
import com.transretail.crm.report.template.CommonReportFilter;
import com.transretail.crm.report.template.ReportTemplate;
import java.io.File;
import java.io.InputStream;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;
import javax.annotation.Resource;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import net.sf.jasperreports.engine.JRParameter;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.joda.time.DateTime;
import org.joda.time.DateTimeUtils;
import org.joda.time.LocalDateTime;
import org.junit.*;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.context.support.MessageSourceResourceBundle;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.support.TransactionCallback;
import org.springframework.transaction.support.TransactionTemplate;

// TODO: need to refactor
/**
 * @author Ervy V. Bigornia (ebigornia@exist.com)
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath*:/META-INF/spring/applicationContext*.xml",
    "/META-INF/spring/applicationContext-test.xml"})
@Transactional
public class EmployeeRegistrationReportTest {

    private static final Logger _LOG = LoggerFactory.getLogger(EmployeeRegistrationReportTest.class);
    @PersistenceContext
    private EntityManager em;
    @Autowired
    private PlatformTransactionManager transactionManager;
    @Resource(name = "employeeRegistrationReport")
    private ReportTemplate employeeRegistrationReport;
    @Autowired
    private ReportService reportService;
    @Autowired
    private MessageSource messageSource;
    @Autowired
    private LookupHeaderRepo lookupHeaderRepo;
    @Autowired
    private LookupDetailRepo lookupDetailRepo;
    @Autowired
    private MemberRepo memberRepo;
    @Autowired
    private CodePropertiesService codePropertiesService;
    @Autowired
    private PasswordEncoder passwordEncoder;
    @Autowired
    private StoreRepo storeRepo;

    private static final DateFormat BIRTHDATE_PASSWORD_FORMAT = new SimpleDateFormat("ddMMyyyy");
    private static final DateFormat BIRTHDATE_PIN_FORMAT = new SimpleDateFormat("ddMMyy");

    @Before
    public void onSetup() {
	DateTimeUtils.setCurrentMillisFixed(new LocalDateTime(2014, 05, 05, 12, 55).toDateTime().getMillis());
	setUpLookupDetails();
	Store lebak = new Store("22022");
	lebak.setId(022);
	lebak.setName("Lebak Bulus");
	Store ambassador = new Store("10021");
	ambassador.setName("Ambassador");
	ambassador.setId(021);
	storeRepo.save(lebak);
	storeRepo.save(ambassador);
	setupData();
	em.flush();
    }

    @After
    public void tearDown() {
	memberRepo.deleteAll();
	lookupDetailRepo.deleteAll();
	lookupHeaderRepo.deleteAll();
	storeRepo.deleteAllInBatch();
    }

    @Test
    public void getNameAndGetFiltersTest() {
	assertEquals(EmployeeRegistrationReport.TEMPLATE_NAME, employeeRegistrationReport.getName());
	assertNotNull(employeeRegistrationReport.getFilters());
    }

    @Test
    public void createJrProcessorTest() throws Exception {
	Map<String, String> map = Maps.newHashMap();
	map.put(EmployeeRegistrationReport.FILTER_DATE_FROM, "13-03-2013");
	map.put(EmployeeRegistrationReport.FILTER_DATE_TO, "30-05-2014");
	map.put(EmployeeRegistrationReport.FILTER_STATUS, Status.ACTIVE.toString());
	map.put(CommonReportFilter.STORES, "22022!10021");
	JRProcessor jrProcessor = employeeRegistrationReport.createJrProcessor(map);

	File testClassesDir = FileUtils.toFile(Thread.currentThread().getContextClassLoader().getResource(""));
	File output = new File(testClassesDir, "test-employee-registration-report-output.pdf");
	_LOG.debug("Output location {}", output.getAbsolutePath());
	output.delete();
	Locale locale = LocaleContextHolder.getLocale();
	jrProcessor.addParameter(JRParameter.REPORT_LOCALE, locale);
	jrProcessor.addParameter(JRParameter.REPORT_RESOURCE_BUNDLE, new MessageSourceResourceBundle(messageSource, locale));
	reportService.exportPdfReport(jrProcessor, output);

	double bytes = output.length();
	double kilobytes = (bytes / 1024);
	_LOG.debug("Kilobytes: {}", kilobytes);
    }

    @Test // TODO: make this parametized test
    public void createJrProcessorTestWithOneStore() throws Exception {
	Map<String, String> map = Maps.newHashMap();
	map.put(EmployeeRegistrationReport.FILTER_DATE_FROM, "13-03-2013");
	map.put(EmployeeRegistrationReport.FILTER_DATE_TO, "30-05-2014");
	map.put(EmployeeRegistrationReport.FILTER_STATUS, Status.ACTIVE.toString());
	map.put(CommonReportFilter.STORES, "22022");
	JRProcessor jrProcessor = employeeRegistrationReport.createJrProcessor(map);

	File testClassesDir = FileUtils.toFile(Thread.currentThread().getContextClassLoader().getResource(""));
	File output = new File(testClassesDir, "test-employee-registration-report-output-with-one-store.pdf");
	_LOG.debug("Output location {}", output.getAbsolutePath());
	output.delete();
	Locale locale = LocaleContextHolder.getLocale();
	jrProcessor.addParameter(JRParameter.REPORT_LOCALE, locale);
	jrProcessor.addParameter(JRParameter.REPORT_RESOURCE_BUNDLE, new MessageSourceResourceBundle(messageSource, locale));
	reportService.exportPdfReport(jrProcessor, output);

	double bytes = output.length();
	double kilobytes = (bytes / 1024);
	_LOG.debug("Kilobytes: {}", kilobytes);
    }

    private void setupData() {
	String preferredStore1 = "22022";
	String preferredStore2 = "10021";

	for (int i = 0; i < 10; i++) {
	    MemberModel model = new MemberModel();
	    model.setUsername("username" + i);
	    String accountId = "accountId" + i;
	    model.setAccountId(accountId);
	    model.setAccountStatus(MemberStatus.ACTIVE);
	    model.setFirstName("FNUser" + i);
	    model.setLastName("LNUser" + i);
	    model.setContact("contact" + i);
	    model.setPin("pin" + i);
	    model.setEnabled(true);
	    model.setRegisteredStore(preferredStore1);
	    model.setStoreName("TITA GWAPA Store");
	    CustomerProfile customerProfile = new CustomerProfile();
	    ProfessionalProfile professionalProfile = new ProfessionalProfile();
	    Address address = new Address();
	    address.setCity("City" + i);
	    address.setStreet("Street" + i);
	    address.setPostCode("1234" + i);

	    if (i % 2 == 0) {
		model.setMemberType(new LookupDetail(codePropertiesService.getDetailMemberTypeEmployee()));
		model.setMemberCreatedFromType(MemberCreatedFromType.FROM_UPLOAD);
		customerProfile.setGender(new LookupDetail(codePropertiesService.getDetailGenderFemale()));
	    } else {
		model.setMemberType(new LookupDetail(codePropertiesService.getDetailMemberTypeEmployee()));
		model.setMemberCreatedFromType(MemberCreatedFromType.FROM_CRM);
		customerProfile.setGender(new LookupDetail(codePropertiesService.getDetailGenderMale()));
	    }
	    String departmentCode = getCode(codePropertiesService.getHeaderBusinessFields(), "Other");
	    professionalProfile.setDepartment(new LookupDetail(departmentCode));
	    customerProfile.setBirthdate(new Date());

	    String empTypeCode = getCode(codePropertiesService.getHeaderEmpType(), "PROBATIONARY ASSOCIATE");
	    model.setEmpType(new LookupDetail(empTypeCode));

	    String genderCode = getCode(codePropertiesService.getHeaderGender(), "Female");
	    customerProfile.setGender(new LookupDetail(genderCode));

	    String religionCode = getCode(codePropertiesService.getHeaderReligion(), "Islam");
	    customerProfile.setReligion(new LookupDetail(religionCode));

	    String nationalityCode = getCode(codePropertiesService.getHeaderNationality(), "INDONESIAN");
	    customerProfile.setNationality(new LookupDetail(nationalityCode));

	    customerProfile.setPlaceOfBirth("birthplace" + i);
	    model.setContact("0856309991" + i);

	    model.setProfessionalProfile(professionalProfile);
	    model.setCustomerProfile(customerProfile);
	    model.setAddress(address);
	    model.setCreated(DateTime.now());
	    memberRepo.save(model);
	}

	for (int i = 10; i < 20; i++) {
	    MemberModel model = new MemberModel();
	    model.setUsername("username" + i);
	    String accountId = "accountId" + i;
	    model.setAccountId(accountId);
	    model.setAccountStatus(MemberStatus.ACTIVE);
	    model.setFirstName("FNUser" + i);
	    model.setLastName("LNUser" + i);
	    model.setContact("contact" + i);
	    model.setPin("pin" + i);
	    model.setEnabled(true);
	    model.setRegisteredStore(preferredStore2);
	    model.setStoreName("TITO POGI Store");
	    CustomerProfile customerProfile = new CustomerProfile();
	    ProfessionalProfile professionalProfile = new ProfessionalProfile();
	    Address address = new Address();
	    address.setCity("City" + i);
	    address.setStreet("Street" + i);
	    address.setPostCode("1234" + i);

	    if (i % 2 == 0) {
		model.setMemberType(new LookupDetail(codePropertiesService.getDetailMemberTypeEmployee()));
		model.setMemberCreatedFromType(MemberCreatedFromType.FROM_CRM);
		customerProfile.setGender(new LookupDetail(codePropertiesService.getDetailGenderMale()));
	    } else {
		model.setMemberType(new LookupDetail(codePropertiesService.getDetailMemberTypeEmployee()));
		model.setMemberCreatedFromType(MemberCreatedFromType.FROM_UPLOAD);
		customerProfile.setGender(new LookupDetail(codePropertiesService.getDetailGenderFemale()));
	    }
	    String departmentCode = getCode(codePropertiesService.getHeaderBusinessFields(), "Other");
	    professionalProfile.setDepartment(new LookupDetail(departmentCode));
	    Date birthDate = new Date();
	    customerProfile.setBirthdate(birthDate);
	    String formattedBirthDate = BIRTHDATE_PASSWORD_FORMAT.format(birthDate);
	    model.setPassword(passwordEncoder.encode(formattedBirthDate));
	    model.setPin(BIRTHDATE_PIN_FORMAT.format(birthDate));

	    String empTypeCode = getCode(codePropertiesService.getHeaderEmpType(), "CONTRACTED ASSOCIATED FULL-TIME");
	    model.setEmpType(new LookupDetail(empTypeCode));

	    String genderCode = getCode(codePropertiesService.getHeaderGender(), "Male");
	    customerProfile.setGender(new LookupDetail(genderCode));

	    String religionCode = getCode(codePropertiesService.getHeaderReligion(), "Islam");
	    customerProfile.setReligion(new LookupDetail(religionCode));

	    String nationalityCode = getCode(codePropertiesService.getHeaderNationality(), "INDONESIAN");
	    customerProfile.setNationality(new LookupDetail(nationalityCode));

	    customerProfile.setPlaceOfBirth("birthplace" + i);
	    model.setContact("0856309991" + i);

	    model.setProfessionalProfile(professionalProfile);
	    model.setCustomerProfile(customerProfile);
	    model.setAddress(address);

	    model.setCreated(DateTime.now().minusMonths(15));
	    memberRepo.save(model);
	}
    }

    @Transactional
    private void setUpLookupDetails() {
	InputStream is1 = Thread.currentThread().getContextClassLoader().getResourceAsStream("A_LOOKUPHEADERS.sql");
	InputStream is2 = Thread.currentThread().getContextClassLoader().getResourceAsStream("B_LOOKUPDETAILS.sql");
	try {
	    final Scanner in1 = new Scanner(is1);
	    final Scanner in2 = new Scanner(is2);
	    TransactionTemplate transactionTemplate = new TransactionTemplate(transactionManager);
	    transactionTemplate.execute(new TransactionCallback() {
		public Object doInTransaction(TransactionStatus status) {
		    while (in1.hasNext()) {
			String script = in1.nextLine();
			if (StringUtils.isNotBlank(script)) {
			    em.createNativeQuery(script).executeUpdate();
			}
		    }
		    while (in2.hasNext()) {
			String script = in2.nextLine();
			if (StringUtils.isNotBlank(script)) {
			    em.createNativeQuery(script).executeUpdate();
			}
		    }
		    em.clear();
		    em.flush();
		    return null;
		}
	    });
	} finally {
	    IOUtils.closeQuietly(is1);
	    IOUtils.closeQuietly(is2);
	}
    }

    protected String getCode(String headerCode, String description) {
	QLookupDetail qLookupDetail = QLookupDetail.lookupDetail;
	String code = new JPAQuery(em).from(qLookupDetail).where(
		qLookupDetail.header.code.eq(headerCode)
		.and(qLookupDetail.description.toUpperCase().eq(description.toUpperCase()))
	)
		.uniqueResult(
			qLookupDetail.code);
	return code;
    }
}
