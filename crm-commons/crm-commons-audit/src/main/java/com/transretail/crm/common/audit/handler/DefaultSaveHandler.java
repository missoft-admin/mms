package com.transretail.crm.common.audit.handler;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import com.transretail.crm.common.audit.AuditMessage;
import com.transretail.crm.common.audit.AuditStatus;

/**
 * @author Allan G. Ramirez (agramirez@exist.com)
 */
@Component
public class DefaultSaveHandler implements SaveHandler {
    private static final Logger LOGGER = LoggerFactory.getLogger(DefaultSaveHandler.class);

    @Override
    public void saveMessage(AuditStatus status, String moduleName, AuditMessage message) {
        StringBuilder builder = new StringBuilder();
        builder.append("Status=");
        builder.append(status);
        builder.append("\t");
        builder.append("Module=");
        builder.append(moduleName);
        builder.append("\t");
        builder.append("Audit Name=");
        builder.append(message.getName());
        builder.append("\t");
        builder.append("Audit Description=");
        builder.append(message.getDescription());
        LOGGER.debug(builder.toString());
    }
}
