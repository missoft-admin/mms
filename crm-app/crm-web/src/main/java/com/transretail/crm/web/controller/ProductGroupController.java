package com.transretail.crm.web.controller;


import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.BooleanUtils;
import org.apache.commons.lang3.EnumUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.transretail.crm.common.service.dto.response.ResultList;
import com.transretail.crm.core.dto.ProductGroupDto;
import com.transretail.crm.core.dto.ProductGroupDto.CodeDescBean;
import com.transretail.crm.core.dto.ProductGroupSearchDto;
import com.transretail.crm.core.service.ProductHierService;
import com.transretail.crm.core.service.ProductGroupService;
import com.transretail.crm.core.service.ProductService;
import com.transretail.crm.product.HierarchyLevel;
import com.transretail.crm.web.controller.util.ControllerResponse;


@RequestMapping("/groups/product")
@Controller
public class ProductGroupController {

	private static final String PATH_MAIN		  	= "/groups/product";
	private static final String PATH_CREATE		  	= "/groups/product/create";
	private static final String PATH_UPDATE		  	= "/groups/product/update/";
	private static final String PATH_SAVE		  	= "/groups/product/save";

//	private static final String PATH_REQ_PRDCFN		= "/groups/product/list/productcfn/";
//	private static final String PATH_REQ_PRODUCT	= "/groups/product/list/product/";
//	private static final String PATH_REQ_CFN		= "/groups/product/list/classification/";
	

	private static final String UIATTR_MODEL		= "productGroup";
	private static final String UIATTR_SEARCHMODEL	= "searchProductGroup";
	private static final String UIATTR_FORMACTION	= "action";
	private static final String UIATTR_SAVEACTION	= "saveAction";
	private static final String UIATTR_LBL_HEADER	= "headerLabel";

	private static final String UIATTR_PRODUCTCFN	= "productCfnOptions";



	@Autowired
	ProductGroupService productGroupService;
	@Autowired
	ProductService productService;
	@Autowired
	ProductHierService productHierService;
	@Autowired
	MessageSource messageSource;



	@RequestMapping( produces = "text/html" )
    public String show( @ModelAttribute(UIATTR_MODEL) ProductGroupDto dto, Model uiModel ) {
        populateFields( uiModel );
        return PATH_MAIN;
    }

	@RequestMapping( value = "/delete/{id}", produces = "application/json; charset=utf-8", method = RequestMethod.GET )
    public @ResponseBody ControllerResponse delete( @PathVariable("id") Long id ) {
    	ControllerResponse theResp = new ControllerResponse();
    	productGroupService.invalidate( id );
		theResp.setSuccess( true );
		return theResp;
	}

	@RequestMapping( value = "/view/{id}", produces = "text/html" )
	public String view( @PathVariable("id") Long id, Model inUiModel) {

		if ( null != id ) {
			inUiModel.addAttribute( UIATTR_MODEL, productGroupService.findDto( id ) );
		}
		return "/groups/product/view";
    }

	@RequestMapping( value = "/create", produces = "text/html" )
    public String create( @ModelAttribute(UIATTR_MODEL) ProductGroupDto dto, Model uiModel ) {
        populateFields( uiModel );
        uiModel.addAttribute( UIATTR_FORMACTION, PATH_SAVE );
        uiModel.addAttribute( UIATTR_SAVEACTION, PATH_SAVE );
        uiModel.addAttribute( UIATTR_MODEL, dto );

        return PATH_CREATE;
    }

	@RequestMapping( value = "/edit/{id}", produces = "text/html" )
    public String edit( @PathVariable("id") Long id, Model uiModel ) {
        populateFields( uiModel );
        uiModel.addAttribute( UIATTR_FORMACTION, PATH_UPDATE + id );
        uiModel.addAttribute( UIATTR_SAVEACTION, PATH_SAVE );
        uiModel.addAttribute( UIATTR_MODEL, productGroupService.findDto( id ) );

        return PATH_CREATE;
    }

    @RequestMapping( value = "/save", produces="application/json; charset=utf-8", method=RequestMethod.POST )
    public @ResponseBody ControllerResponse save( @ModelAttribute(UIATTR_MODEL) ProductGroupDto dto, BindingResult binding ) {
    	ControllerResponse resp = new ControllerResponse();
		resp.setSuccess( false );

    	if ( isValid( dto, binding ) ) {
    		productGroupService.saveDto( dto );
    		resp.setSuccess( true );
    	}
    	else {
    		resp.setResult( binding.getAllErrors() );
    	}

		return resp;
    }

    @RequestMapping( value = "/update/{id}", produces="application/json; charset=utf-8", method=RequestMethod.POST )
    public @ResponseBody ControllerResponse update( @PathVariable("id") Long id, 
    		@ModelAttribute(UIATTR_MODEL) ProductGroupDto dto, BindingResult binding ) {
    	ControllerResponse resp = new ControllerResponse();
		resp.setSuccess( false );

    	if ( isValid( dto, binding ) ) {
    		dto.setId( id );
    		productGroupService.updateDto( dto );
    		resp.setSuccess( true );
    	}
    	else {
    		resp.setResult( binding.getAllErrors() );
    	}

		return resp;
    }

	@RequestMapping( value = "/datalist", produces = "application/json; charset=utf-8", method = RequestMethod.POST )
    public @ResponseBody ResultList<ProductGroupDto> list( @RequestBody ProductGroupSearchDto dto ) {
		dto.setValid( true );
		ResultList<ProductGroupDto> dtos = productGroupService.search( dto );
		return dtos;
	}

    @RequestMapping(value = "/search/list", method = RequestMethod.POST)
    public @ResponseBody ResultList<ProductGroupDto> searchList( @RequestBody ProductGroupSearchDto dto ) {
    	dto.setValid( true );
    	return productGroupService.search( dto );
    }

	@RequestMapping( value = "/productcfns", produces = "application/json; charset=utf-8", method = RequestMethod.POST )
    public @ResponseBody ResultList<CodeDescBean> listProductCfns( @RequestBody ProductGroupSearchDto dto ) {
		dto.setValid( true );
		if ( null == dto.getId() ) {
			return new ResultList<CodeDescBean>( new ArrayList<CodeDescBean>() );
		}
		List<CodeDescBean> beans = productGroupService.findDto( dto.getId() ).getItemCodeDesc();
		return new ResultList<CodeDescBean>(  ( null == beans )? new ArrayList<CodeDescBean>() : beans );
	}

	@RequestMapping( value = "/excproducts", produces = "application/json; charset=utf-8", method = RequestMethod.POST )
    public @ResponseBody ResultList<CodeDescBean> listExcProducts( @RequestBody ProductGroupSearchDto dto ) {
		dto.setValid( true );
		if ( null == dto.getId() ) {
			return new ResultList<CodeDescBean>( new ArrayList<CodeDescBean>() );
		}
		List<CodeDescBean> beans = productGroupService.findDto( dto.getId() ).getExcProductCodeDesc();
		return new ResultList<CodeDescBean>(  ( null == beans )? new ArrayList<CodeDescBean>() : beans );
	}

	@RequestMapping( value = "/list", produces = "application/json; charset=utf-8", method = RequestMethod.GET )
    public @ResponseBody ControllerResponse list() {
    	ControllerResponse theResp = new ControllerResponse();
    	ProductGroupSearchDto dto = new ProductGroupSearchDto();
    	dto.setValid( true );
    	theResp.setResult( productGroupService.search( dto ).getResults() );
		theResp.setSuccess( true );
		return theResp;
	}

	@RequestMapping( value = "/search", produces = "application/json; charset=utf-8", method = RequestMethod.POST )
    public @ResponseBody ControllerResponse search( @ModelAttribute(UIATTR_SEARCHMODEL) ProductGroupSearchDto dto ) {
    	ControllerResponse theResp = new ControllerResponse();
    	theResp.setResult( productGroupService.search( dto ).getResults() );
		theResp.setSuccess( true );
		return theResp;
	}



	@RequestMapping( value="/list/productcfn", produces="application/json; charset=utf-8", method=RequestMethod.GET )
	public @ResponseBody ControllerResponse getProductCfnList( 
			@RequestParam(value = "code", required = true) String code,
			@RequestParam(value = "index", required = false) Integer index,
			@RequestParam(value = "size", required = false) Integer size ) {

    	ControllerResponse resp = new ControllerResponse();
		resp.setSuccess( false );

    	if ( StringUtils.isNotBlank( code ) ) {
    		if ( EnumUtils.isValidEnum( ProductGroupDto.PRODUCT_OPTION.class, code ) ) {
    			if ( null != index && null != size ) {
    				resp.setResult( productService.getProducts( index, size ) );
    			}
    			else {
    				resp.setResult( productService.getAllProducts() );
    			}
    		}
    		//else if ( null != lookupService.getDetail( code ) ) {
    		else if ( StringUtils.isNotBlank( code ) ) {
    			if ( null != index && null != size ) {
    				resp.setResult( productHierService.get( code, index, size ) );
    			}
    			else {
    				resp.setResult( productHierService.get( code ) );
    			}
    		}
    		resp.setSuccess( true );
    	}

		return resp;
	}

    @RequestMapping( value="/list/productcfn/filter", produces="application/json; charset=utf-8", method=RequestMethod.GET )
	public @ResponseBody ControllerResponse filterProductCfnList( 
			@RequestParam(value = "code", required = true) String code,
			@RequestParam(value = "prdcode", required = true) String prdCode,
			@RequestParam(value = "cfncode", required = true) String cfnCode ) {

    	ControllerResponse theResp = new ControllerResponse();
		theResp.setResult( productGroupService.filterProductAndCfns( code, prdCode, cfnCode ) );
		theResp.setSuccess( true );

		return theResp;
	}

	@RequestMapping( value="/list/products", produces="application/json; charset=utf-8", method=RequestMethod.GET )
	public @ResponseBody ControllerResponse getProductList( 
			@RequestParam(value = "prdcode", required = true) String prdCode,
			@RequestParam(value = "cfncode", required = true) String cfnCode,
			@RequestParam(value = "index", required = false) Integer index,
			@RequestParam(value = "size", required = false) Integer size ) {

    	ControllerResponse resp = new ControllerResponse();
    	if ( StringUtils.isNotBlank( prdCode ) || StringUtils.isNotBlank( cfnCode ) ) {
    		if ( null != index && null != size ) {
    			resp.setResult( productGroupService.getProducts( prdCode, cfnCode, index, size ) );
    		}
    		else {
    			resp.setResult( productGroupService.getProducts( prdCode, cfnCode ) );
    		}
    	}
    	else {
    		resp.setResult( new ArrayList<ProductGroupDto.CodeDescBean>() );
    	}
		resp.setSuccess( true );

		return resp;
	}

    @RequestMapping( value="/list/productcfn/search", produces="application/json; charset=utf-8", method=RequestMethod.GET )
	public @ResponseBody ControllerResponse searchProductCfnList( 
			@RequestParam(value = "code", required = true) String code,
			@RequestParam(value = "prdcode", required = true) String prdCode,
			@RequestParam(value = "cfncode", required = true) String cfnCode ) {

    	ControllerResponse theResp = new ControllerResponse();
		theResp.setResult( productGroupService.searchProductAndCfns( code, BooleanUtils.toBoolean( prdCode ), cfnCode ) );
		theResp.setSuccess( true );

		return theResp;
	}

    @RequestMapping( value="/list/product/search", produces="application/json; charset=utf-8", method=RequestMethod.GET )
	public @ResponseBody ControllerResponse searchProductList( 
			@RequestParam(value = "code", required = true) String code,
			@RequestParam(value = "prdcode", required = true) String prdCode,
			@RequestParam(value = "cfncode", required = true) String cfnCode ) {

    	ControllerResponse theResp = new ControllerResponse();
		theResp.setResult( productGroupService.searchProducts( code, prdCode, cfnCode ) );
		theResp.setSuccess( true );

		return theResp;
	}





	private void populateFields( Model uiModel ) {
        uiModel.addAttribute( UIATTR_LBL_HEADER, messageSource.getMessage( "group_product_create", null, null ) );
        List<CodeDescBean> prdcfns = new ArrayList<CodeDescBean>();
        /*for ( LookupDetail lookup : lookupService.getActiveDetailsByHeaderCode( codePropertiesService.getHeaderProductClassification() ) ) {
        	prdcfns.add( new CodeDescBean( lookup.getCode(), lookup.getDescription() ) );
        }*/
        for ( HierarchyLevel level : HierarchyLevel.values() ) {
        	prdcfns.add( new CodeDescBean( String.valueOf( level.ordinal() ), level.toString() ) );
        }
        prdcfns.add( new CodeDescBean( ProductGroupDto.PRODUCT_OPTION.product.name(), ProductGroupDto.PRODUCT_OPTION.product.name(), null, true ) );
        uiModel.addAttribute( UIATTR_PRODUCTCFN, prdcfns );
        uiModel.addAttribute( UIATTR_FORMACTION, PATH_SAVE );
        uiModel.addAttribute( UIATTR_SAVEACTION, PATH_SAVE );
	}

	private boolean isValid( ProductGroupDto dto, BindingResult inBinding ) {
		if ( StringUtils.isBlank( dto.getName() ) ) {
			inBinding.reject( messageSource.getMessage( "group_err_emptyname", null, null ) );
		}
		return !inBinding.hasErrors();
	}

}


