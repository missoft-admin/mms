package com.transretail.crm.web.controller.editor;

import java.beans.PropertyEditorSupport;

public class NumberEditor extends PropertyEditorSupport {

	private Class<? extends Number> numberClass;

	public NumberEditor(Class<? extends Number> numberClass) {
		super();
		this.numberClass = numberClass;
	}

	@Override
	public void setAsText(String text) throws IllegalArgumentException {
		if (text.equals("null") || text == null || text.isEmpty()) {
			setValue(null);
		}else {
			if (numberClass.getClass().equals(Long.class))
				setValue(Long.valueOf(text));
			else if (numberClass.getClass().equals(Float.class))
				setValue(Float.valueOf(text));
			else if (numberClass.getClass().equals(Double.class))
				setValue(Double.valueOf(text));
			else if (numberClass.getClass().equals(Integer.class))
				setValue(Integer.valueOf(text));
			else
				setValue(text);

		}
	}
}
