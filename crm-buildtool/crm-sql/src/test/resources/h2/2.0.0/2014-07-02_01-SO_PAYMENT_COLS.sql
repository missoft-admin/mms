ALTER TABLE CRM_SALES_ORD ADD PAYMENT_AMT decimal(19,2);
ALTER TABLE CRM_SALES_ORD ADD PAYMENT_DATE date;
ALTER TABLE CRM_SALES_ORD ADD PAYMENT_STORE varchar(255);
ALTER TABLE CRM_SALES_ORD ADD PAYMENT_TYPE varchar(15);

    alter table CRM_SALES_ORD 
        add constraint FK_mx3qgy79a83811rq24lpoqx4v 
        foreign key (PAYMENT_TYPE) 
        references CRM_REF_LOOKUP_DTL;