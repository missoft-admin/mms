package com.transretail.crm.web.giftcard.controller;

import com.google.common.base.Function;
import com.google.common.collect.FluentIterable;
import com.transretail.crm.common.service.exception.GenericServiceException;
import com.transretail.crm.common.service.exception.MessageSourceResolvableException;
import com.transretail.crm.core.security.service.MD5Service;
import com.transretail.crm.core.service.CodePropertiesService;
import com.transretail.crm.giftcard.GiftCardNotFoundException;
import com.transretail.crm.giftcard.GiftCardServiceResolvableException;
import com.transretail.crm.giftcard.dto.GiftCardInfo;
import com.transretail.crm.giftcard.dto.PrepaidReloadTransactionInfo;
import com.transretail.crm.giftcard.entity.support.GiftCardSalesType;
import com.transretail.crm.giftcard.service.GiftCardManager;
import com.transretail.crm.web.giftcard.controller.GiftCardRequest.CardItem;
import com.transretail.crm.web.giftcard.controller.dto.PrepaidReloadRequest;
import com.transretail.crm.web.giftcard.controller.dto.PrepaidReloadVoidRequest;
import com.transretail.crm.web.giftcard.controller.validation.RedemptionCheck;
import java.util.Set;
import javax.validation.Valid;
import org.joda.time.DateTimeZone;
import org.joda.time.LocalDateTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.NoSuchMessageException;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.*;

import static com.transretail.crm.giftcard.entity.support.GiftCardSalesType.B2C;
import static com.transretail.crm.giftcard.entity.support.GiftCardSalesType.REBATE;

/**
 * @author Monte Cillo Co (mco@exist.com)
 */
@Controller
@RequestMapping(value = "/giftcard",
        consumes = MediaType.APPLICATION_JSON_VALUE,
        produces = MediaType.APPLICATION_JSON_VALUE)
public class GiftCardRestController {

    private static final Logger LOGGER = LoggerFactory.getLogger(GiftCardRestController.class);

    @Autowired
    private MD5Service mD5Service;
    @Autowired
    private CodePropertiesService codePropertiesService;
    @Autowired
    private GiftCardManager giftCardManager;
    @Autowired
    private MessageSource messageSource;

    @RequestMapping(value = "/inquire", method = RequestMethod.POST)
    @ResponseBody
    public GiftCardResponse inquire(@Valid @RequestBody GiftCardCommonRequest request) {
        this.validateSignature(request);
        GiftCardInfo cardInfo = giftCardManager.inquire(request.getTransactionNo(), request.getCardNo(), request.getMerchantId());
        return new GiftCardResponse().withSuccessful(true).withProcessedDateTime(LocalDateTime.now()).addCardInfo(cardInfo);
    }

    @RequestMapping(value = "/pre-activate", method = RequestMethod.POST)
    @ResponseBody
    public GiftCardResponse preActivate(@RequestParam(value = "free", defaultValue = "false") boolean isFree, @Valid @RequestBody GiftCardCommonRequest request) {
        this.validateSignature(request);
        GiftCardInfo cardInfo = giftCardManager.preActivate(isFree ? REBATE : B2C, request.toGiftCardTransactionInfo(),
                request.getCardNo(), request.getAccountId());
        return new GiftCardResponse().withSuccessful(true).withProcessedDateTime(LocalDateTime.now()).addCardInfo(cardInfo);
    }

    @RequestMapping(value = "/activate", method = RequestMethod.POST)
    @ResponseBody
    public GiftCardResponse activate(@Valid @RequestBody GiftCardRequest request) {
        LOGGER.info("[GIFTCARD][ACTIVTE] account id = {}; transaction date = {}; transaction no = {}; "
                + "Merchant id {} validation key {}", request.getAccountId(), request.getTransactionDate(),
                request.getTransactionNo(), request.getMerchantId(), request.getValidationKey());

        this.validateSignature(request);
 
        Set<String> cards = FluentIterable.from(request.getItems()).transform(new Function<GiftCardRequest.CardItem, String>() {

            @Override
            public String apply(CardItem input) {
                return input.getCardNo();
            }
        }).toSet();

        Set<GiftCardInfo> cardInfos = giftCardManager.activate(request.toGiftCardTransactionInfo(), cards,
                request.getAccountId(), GiftCardSalesType.B2C, false);
        //gciStockService.saveStocksFromB2Activation( cardInfos, GiftCardInventoryStatus.ACTIVATED.name() );
        return new GiftCardResponse().withSuccessful(true).withProcessedDateTime(LocalDateTime.now()).withCardInfos(cardInfos);
    }

    @RequestMapping(value = "/activate/freevoucher", method = RequestMethod.POST)
    @ResponseBody
    public GiftCardResponse activateFreeVoucher(@Valid @RequestBody GiftCardRequest request) {
        LOGGER.info("[GIFTCARD][ACTIVTE FREEVOUCHER] account id = {}; transaction date = {}; transaction no = {}; "
                + "Merchant id {} validation key {}", request.getAccountId(), request.getTransactionDate(),
                request.getTransactionNo(), request.getMerchantId(), request.getValidationKey());
        this.validateSignature(request);

        Set<String> cards = FluentIterable.from(request.getItems())
                .transform(new Function<GiftCardRequest.CardItem, String>() {
                    @Override
                    public String apply(CardItem input) {
                        return input.getCardNo();
                    }
                }).toSet();

        Set<GiftCardInfo> cardInfos = giftCardManager.activateFreeVoucher(request.toGiftCardTransactionInfo(), cards);

        return new GiftCardResponse().withSuccessful(true)
                .withProcessedDateTime(LocalDateTime.now())
                .withCardInfos(cardInfos);
    }

    @RequestMapping(value = "/activate/void", method = RequestMethod.POST)
    @ResponseBody
    public GiftCardResponse voidActivated(@Valid @RequestBody GiftCardVoidRequest request) {
        this.validateSignature(request);
        Set<GiftCardInfo> results = giftCardManager.voidActivation(request.toGiftCardTransactionInfo());
        GiftCardResponse response = new GiftCardResponse();

        return response.withSuccessful(true).withProcessedDateTime(LocalDateTime.now()).withCardInfos(results);
    }

    @RequestMapping(value = "/redeem", method = RequestMethod.POST)
    @ResponseBody
    public GiftCardResponse redeem(@Validated(RedemptionCheck.class) @RequestBody GiftCardCommonRequest request) {
        this.validateSignature(request);
        Set<GiftCardInfo> cardInfos = giftCardManager.redeem(request.toGiftCardTransactionInfo(), request.getCardNo(), request.getRedemptionAmount());
        return new GiftCardResponse().withSuccessful(true)
                .withProcessedDateTime(LocalDateTime.now())
                .withCardInfos(cardInfos);
    }

    @RequestMapping(value = "/redeem/void", method = RequestMethod.POST)
    @ResponseBody
    public GiftCardResponse voidRedeem(@Valid @RequestBody GiftCardVoidRequest request) {
        this.validateSignature(request);
        Set<GiftCardInfo> cardInfos = giftCardManager.voidRedemption(request.toGiftCardTransactionInfo());
        return new GiftCardResponse().withSuccessful(true).withProcessedDateTime(LocalDateTime.now()).withCardInfos(cardInfos);
    }

    @RequestMapping(value = "/reload", method = RequestMethod.POST)
    @ResponseBody
    public GiftCardResponse reload(@Valid @RequestBody PrepaidReloadRequest request) throws
            MessageSourceResolvableException {
        this.validateSignature(request);
        giftCardManager.preReload(request.getCardNo(), request.getMerchantId(), request.getReloadAmount().doubleValue());

        Set<GiftCardInfo> cardInfos = giftCardManager.reload(request.toGiftCardTransactionInfo(),
                request.getCardNo(), request.getReloadAmount());
        return new GiftCardResponse().withSuccessful(true).withProcessedDateTime(
                LocalDateTime.now(DateTimeZone.getDefault())).withCardInfos(cardInfos);
    }

    @RequestMapping(value = "/reload/void", method = RequestMethod.POST)
    @ResponseBody
    public GiftCardResponse reloadVoid(@Valid @RequestBody GiftCardVoidRequest request) throws
            MessageSourceResolvableException {
        this.validateSignature(request);

        Set<GiftCardInfo> cardInfos = giftCardManager.reloadVoid(request.toGiftCardTransactionInfo());
        return new GiftCardResponse().withSuccessful(true).withProcessedDateTime(
                LocalDateTime.now(DateTimeZone.getDefault())).withCardInfos(cardInfos);
    }

    private void validateSignature(AbstractGiftCardRequest request) throws UnauthorizedException {
        if (!mD5Service.validateMd5(request.getSignature(), request.getValidationKey())) {
            throw new UnauthorizedException("Failed to perform request. Request validation key does not match!"); //
            // TODO
        }
    }

    private PrepaidReloadTransactionInfo prepaidReloadTransactionInfo(PrepaidReloadRequest request) {
        PrepaidReloadTransactionInfo transactionInfo = new PrepaidReloadTransactionInfo().merchantId(request
                .getMerchantId())
                .terminalId(request.getTerminalId()).cashierId(request.getCashierId()).transactionNo(request
                        .getTransactionNo())
                .transactionTime(request.getTransactionDate())
                .cardNo(request.getCardNo())
                .reloadAmount(request.getReloadAmount());
        return transactionInfo;
    }

    private PrepaidReloadTransactionInfo prepaidReloadTransactionInfo(PrepaidReloadVoidRequest request) {
        PrepaidReloadTransactionInfo transactionInfo = prepaidReloadTransactionInfo((PrepaidReloadRequest) request)
                .refTransactionNo(request.getRefTransactionNo());
        return transactionInfo;
    }

    @ResponseBody
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler(MessageSourceResolvableException.class)
    public ErrorResponse handleMessage(MessageSourceResolvableException ex) {
        ErrorResponse errorResponse = new ErrorResponse();
        String errorMessage = null;
        try {
            errorMessage = messageSource.getMessage(ex.getLastCode(), ex.getArguments(),
                    LocaleContextHolder.getLocale());
        } catch (NoSuchMessageException e) {
            errorMessage = ex.getDefaultMessage();
        }

        errorResponse.setErrorMessage(errorMessage);
        errorResponse.setStatus(HttpStatus.BAD_REQUEST.value());

        return errorResponse;
    }

    @ResponseBody
    @ResponseStatus(HttpStatus.UNPROCESSABLE_ENTITY)
    @ExceptionHandler({GiftCardNotFoundException.class, GenericServiceException.class,
        MethodArgumentNotValidException.class, GiftCardServiceResolvableException.class})
    public ErrorResponse handleException(Exception ex) {
        ErrorResponse errorResponse = new ErrorResponse();
        errorResponse.setErrorCode("2000");
        errorResponse.setErrorMessage("Could not process the request. " + ex.getMessage());
        errorResponse.setStatus(HttpStatus.UNPROCESSABLE_ENTITY.value());

        if (ex instanceof MethodArgumentNotValidException) {
            BindingResult result = ((MethodArgumentNotValidException) ex).getBindingResult();
            errorResponse.setErrorMessage("Validation Error"); //TODO: 18ln
            for (FieldError fieldError : result.getFieldErrors()) {
                errorResponse.addFieldError(fieldError.getField(), fieldError.getDefaultMessage()); // TODO: Localize
                // Messages
            }
        } else if (ex instanceof GiftCardServiceResolvableException) {
            GiftCardServiceResolvableException gcse = (GiftCardServiceResolvableException) ex;
            errorResponse.setErrorCode(gcse.getErrorCode().getErrorCode());
            String errorMessage;
            try {
                errorMessage = messageSource.getMessage(gcse.getCodes()[0], gcse.getArguments(),
                        LocaleContextHolder.getLocale());
            } catch (NoSuchMessageException e) {
                errorMessage = gcse.getDefaultMessage();
            }

            errorResponse.setErrorMessage(errorMessage);
        }
        return errorResponse;
    }

    @ExceptionHandler({UnauthorizedException.class})
    @ResponseBody
    @ResponseStatus(HttpStatus.UNAUTHORIZED)
    public ErrorResponse handleUnauthorizedException(Exception ex) {
        ErrorResponse errorResponse = new ErrorResponse();
        errorResponse.setErrorCode(codePropertiesService.getDetailMessageCodeInvalidClientSignature()); // retain
        // error code and used this invalidClientSignature
        errorResponse.setStatus(HttpStatus.UNAUTHORIZED.value());
        errorResponse.setErrorMessage(ex.getMessage());

        return errorResponse;
    }

    @ExceptionHandler({InvalidCardException.class})
    @ResponseBody
    @ResponseStatus(HttpStatus.UNAUTHORIZED)
    public ErrorResponse handleInvalidCardException(Exception ex) {
        ErrorResponse errorResponse = new ErrorResponse();
        errorResponse.setErrorCode(codePropertiesService.getDetailMessageCodeInvalidCard()); // retain error code and
        // used this invalidClientSignature
        errorResponse.setStatus(HttpStatus.UNAUTHORIZED.value());
        errorResponse.setErrorMessage(ex.getMessage());

        return errorResponse;
    }
}
