create table CRM_PROMOTION_REDEMPTION (
        id number(19,0) not null,
        CREATED_BY varchar2(255 char),
        CREATED_DATETIME timestamp,
        LAST_UPDATED_BY varchar2(255 char),
        LAST_UPDATED_DATETIME timestamp,
        QUANTITY number(19,0),
        REDEEM_FROM date,
        REDEEM_TO date,
        SKU varchar2(255 char),
        PROMO_ID number(19,0),
        primary key (id)
    );

    create table CRM_PROMOTION_REDEMPTION_AUD (
        id number(19,0) not null,
        REV number(19,0) not null,
        REVTYPE number(3,0),
        QUANTITY number(19,0),
        REDEEM_FROM date,
        REDEEM_TO date,
        SKU varchar2(255 char),
        PROMO_ID number(19,0),
        primary key (id, REV)
    );



alter table CRM_PROMOTION_REDEMPTION 
        add constraint FK_ata3b2xu3bs37yafkyq0os2aw 
        foreign key (PROMO_ID) 
        references CRM_PROMOTION;

    alter table CRM_PROMOTION_REDEMPTION_AUD 
        add constraint FK_ji9yengtmvhfxn0ujdjmmpr5x 
        foreign key (REV) 
        references CRM_REVINFO;