package com.transretail.crm.giftcard.repo;

import org.springframework.stereotype.Repository;

import com.transretail.crm.common.repo.CrmQueryDslPredicateExecutor;
import com.transretail.crm.giftcard.entity.B2BExhibition;

@Repository
public interface B2BExhibitionRepo extends CrmQueryDslPredicateExecutor<B2BExhibition, Long> {

}
