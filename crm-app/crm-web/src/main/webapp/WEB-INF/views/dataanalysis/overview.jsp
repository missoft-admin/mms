<%@include file="../common/taglibs.jsp" %>
<style type="text/css">
<!--
  #overviewTable tr td:last-child {
    border-left: 0px;    
  }
  
-->
</style>

<c:forEach items="${analysisDto}" var="item">
<c:set var="monthHTML">${monthHTML}<th>${item.month}</th></c:set>
<c:set var="currentHTML">${currentHTML}<td><fmt:formatNumber type="number" pattern="${decimalFormat}" value="${item.current}" /></td></c:set>
<c:set var="previousHTML">${previousHTML}<td><fmt:formatNumber type="number" pattern="${decimalFormat}" value="${item.previous}" /></td></c:set>
<c:set var="diffHTML">${diffHTML}<td><fmt:formatNumber type="number" pattern="${decimalFormat};(${decimalFormat})" value="${item.diff}" /></td></c:set>
<c:set var="diffPercentHTML">${diffPercentHTML}<td><fmt:formatNumber type="number" pattern="0.00;(0.00)" value="${item.diffPercent}" /></td></c:set>
<c:set var="line1">${line1}<c:if test="${not empty line1}">,</c:if>['${item.month}', ${item.current}]</c:set>
<c:set var="line2">${line2}<c:if test="${not empty line2}">,</c:if>['${item.month}', ${item.previous}]</c:set>
</c:forEach>

<div style="overflow: auto; width: 100%">
<div id="overviewChart"></div>
</div>

<br/>

<c:if test="${searchDto.overview == 'salesValue'}">
<label><spring:message code="legend_one_is_to_thousand" /></label>
</c:if>

<div style="overflow: auto; width: 100%">
<table id="overviewTable" class="table table-condensed table-compressed table-striped">
  <tr><th></th>${monthHTML}</tr>
  <tr><td>${currentYear}</td>${currentHTML}</tr>
  <tr><td>${previousYear}</td>${previousHTML}</tr>
  <tr><td><spring:message code="label_overview_table_diff" /></td>${diffHTML}</tr>
  <tr><td><spring:message code="label_overview_table_diffpercent" /></td>${diffPercentHTML}</tr>
</table>
</div>

<script>
$(document).ready(function() {
	var line1 = [${line1}];
	var line2 = [${line2}];
	$.jqplot('overviewChart',  [line1, line2], {
		title: '<spring:message code="title_analysisby_${searchDto.overview}"/>',
		seriesDefaults: {
            breakOnNull: true
        },
		legend: {
			show:true,
			location: 'se',
			labels: ['${currentYear}', '${previousYear}'],
			showSwatches: true,
			placement: "insideGrid"
		}, 
		axes: {
			xaxis:{
	        	renderer:$.jqplot.CategoryAxisRenderer
			}
		}
	});
});

</script>
