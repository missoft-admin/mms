
alter table CRM_GC_TRANSACTION
  add column SALES_ORDER_ID bigint;

alter table CRM_GC_TRANSACTION 
     add constraint FK_nv7kgolgx7y90lwj6wmaw2evx 
     foreign key (SALES_ORDER_ID) 
     references CRM_SALES_ORD;
