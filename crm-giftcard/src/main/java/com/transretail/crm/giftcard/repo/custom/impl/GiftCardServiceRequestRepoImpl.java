package com.transretail.crm.giftcard.repo.custom.impl;

import com.mysema.query.jpa.impl.JPAQuery;
import com.transretail.crm.giftcard.entity.GiftCardInventory;
import com.transretail.crm.giftcard.entity.GiftCardServiceRequest;
import com.transretail.crm.giftcard.entity.QGiftCardServiceRequest;
import com.transretail.crm.giftcard.repo.custom.GiftCardServiceRequestRepoCustom;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.springframework.data.jpa.repository.support.Querydsl;
import org.springframework.data.querydsl.QueryDslUtils;

/**
 *
 * @author Monte Cillo Co (mco@exist.com)
 * @since 2.0
 */
public class GiftCardServiceRequestRepoImpl implements GiftCardServiceRequestRepoCustom {

    @PersistenceContext
    private EntityManager em;

    @Override
    public List<GiftCardServiceRequest> findAllServiceRequestOf(GiftCardInventory giftcard) {
	QGiftCardServiceRequest qreq = QGiftCardServiceRequest.giftCardServiceRequest;
	
	return new JPAQuery(em).from(qreq)
		.where(qreq.giftcard.eq(giftcard)).list(qreq);
    }
}

