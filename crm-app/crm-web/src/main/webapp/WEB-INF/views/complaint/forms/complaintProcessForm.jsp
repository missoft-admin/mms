<%@include file="../../common/taglibs.jsp" %>


<div id="content_complaintProcessForm" class="modal hide  nofly modal-dialog">

  <div class="modal-content">

  <div class="modal-form form-horizontal">
    <div class="modal-header">
      <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
      <h4><spring:message code="complaint_lbl_process" /></h4>
    </div>


    <div class="modal-body">
      <div class="hide alert alert-error" id="content_error"><button type="button" class="close" onclick="$(this).parent().hide();">&times;</button><div><form:errors   path="*"/></div></div>

      <div class="main-block">
        <div id="content_dateTime" class="control-group">
          <label for="dateTime" class="control-label"><spring:message code="complaint_date"/></label>
          <div  class="controls"><div>${complaint.dateTime}</div></div>
        </div>
        <div class="control-group">
          <label for="ticketNo" class="control-label"><spring:message code="complaint_ticketno"/></label>
          <div  class="controls"><div>${complaint.ticketNo}</div></div>
        </div>
        <div id="content_status" class="control-group">
          <label for="status" class="control-label"><spring:message code="complaint_status"/></label>
          <div  class="controls"><div>${complaint.status}</div></div>
        </div>
        <div class="control-group">
          <label for="memberBarcode" class="control-label"><spring:message code="complaint_member_barcode"/></label>
          <div  class="controls"><div>${complaint.memberBarcode}</div></div>
        </div>
        <div id="content_store" class="control-group">
          <label for="store" class="control-label"><spring:message code="complaint_store"/></label>
          <div  class="controls"><div>${store.code} - ${store.name}</div></div>
        </div>
        <div class="control-group">
          <label for="mobileNo" class="control-label"><spring:message code="complaint_mobile"/></label>
          <div  class="controls"><div>${complaint.mobileNo}</div></div>
        </div>
        <div class="control-group">
          <label for="email" class="control-label"><spring:message code="complaint_email"/></label>
          <div  class="controls"><div>${complaint.email}</div></div>
        </div>
        <div class="control-group">
          <label for="firstName" class="control-label"><spring:message code="complaint_fname"/></label>
          <div  class="controls"><div>${complaint.firstName}</div></div>
        </div>
        <div class="control-group">
          <label for="lastName" class="control-label"><spring:message code="complaint_lname"/></label>
          <div  class="controls"><div>${complaint.lastName}</div></div>
        </div>
        <div class="control-group">
          <label for="category" class="control-label"><spring:message code="complaint_category"/></label>
          <div  class="controls"><div>${complaint.category}</div></div>
        </div>
        <div class="control-group">
          <label for="complaints" class="control-label"><spring:message code="complaint_complaints"/></label>
          <div  class="controls"><div>${complaint.complaints}</div></div>
        </div>
        <div class="control-group">
          <label for="genderDesc" class="control-label"><spring:message code="complaint_gender"/></label>
          <div  class="controls"><div>${complaint.genderDesc}</div></div>
        </div>
        <div class="control-group">
          <label for="priority" class="control-label"><spring:message code="complaint_priority"/></label>
          <div  class="controls"><div>${complaint.priority}</div></div>
        </div>

        <jsp:include page="complaintRemarks.jsp" />

        <div class="clearfix"></div>
        <c:url var="url_remarksAction" value="/customer/complaint/process/remarks/save" />
        <form:form id="complaintProcessForm" name="complaint" modelAttribute="complaint" method="POST" 
          action="${url_remarksAction}" class="modal-form form-horizontal">

          <div class="control-group">
	          <label for="priority" class="control-label"><spring:message code="complaint_assignedto"/></label>
	          <div  class="controls">
	            <form:hidden path="id"/>
	            <form:select id="assignedTo" path="assignedTo" >
	              <option/>
	              <%-- <c:forEach var="user" items="${users}">
	                <option value="${user.username}">${user.storeDesc} - ${user.username}</option>
	              </c:forEach> --%>
	              <form:options items="${users}" itemLabel="storeDescAndUser" itemValue="username" />
	            </form:select>
	          </div>
	        </div>
          <div class="control-group">
            <label for="priority" class="control-label"><spring:message code="complaint_status"/></label>
            <div  class="controls">
              <form:hidden path="id"/>
              <form:select id="status" path="status" >
                <option/>
                <form:options items="${stats}" itemValue="code" itemLabel="desc" />
              </form:select>
            </div>
          </div>

	        <div class="control-group">
	          <label for="remarks" class="control-label"><spring:message code="complaint_lbl_remarks"/></label>
	          <div  class="controls"><form:textarea path="remarks"/></div>
	        </div>

        </form:form>
        <%--  --%>
      </div>
    </div>


    <div class="modal-footer">
      <!-- <input id="processBtn" class="btn btn-primary" type="button" value="<spring:message code="label_process" />" />
      <input id="cancelBtn" class="btn" type="button" value="<spring:message code="label_cancel" />" data-dismiss="modal" /> -->
      <button type="button" id="processBtn" class="btn btn-primary"><spring:message code="label_process" /></button>
      <button type="button" id="cancelBtn" data-dismiss="modal" class="btn btn-default"><spring:message code="label_cancel"/></button>
    </div>
  </div>

  </div>


<style type="text/css"> 
<!-- 
  .clearfix { margin-top: 20px !important; }
  .modal-footer { text-align: center; } 
  .control-label { width: 120px !important; } 
  .controls { margin-left: 140px !important; } 
  .controls div { padding-top: 4px !important; }
--> 
</style>
<script type="text/javascript">
var ComplaintProcessForm = null;

$(document).ready( function() {

	  var errorContainer = $( "#content_complaintProcessForm" ).find( "#content_error" );

	  var $dialog = $( "#content_complaintProcessForm" );
	  var $form = $dialog.find( "#complaintProcessForm" );
	  var $save = $dialog.find( "#processBtn" );
	  var $dispDate = $dialog.find( "#content_dateTime > .controls > div" );

	  initDialog();
	  initFormFields();

    function initDialog() {
		    $dialog.modal({ show : false });
		    $dialog.on( "hide", function(e) { 
			      if ( e.target === this ) { 
				        errorContainer.hide();
				        ComplaintCommon.reset( $form );
			      }
		    });
	  }

    function initFormFields() {
    	  $save.click( function(e) {
            e.preventDefault();
    		    $form.submit();
    	  });

    	  $form.submit( function(e) {
    		    $save.prop( "disabled", true );
    		    e.preventDefault();
    		    $.post( $form.attr( "action" ), $form.serialize(), function( resp ) { 
    		        ComplaintList.reloadTable();
    		        $dialog.modal( "hide" );
    		    }, "json" );
    	  });

    	  $dispDate.html( ( $dispDate.html() )? new Date( $dispDate.html() - 0 ).customize( 0 ) : "" );
    }

	  ComplaintProcessForm = {
        show : function() { $dialog.modal( "show" ); }
	  };
});
</script>


</div>