alter table CRM_MEMBER_GRP add PREPAID_USER varchar2(1 char);
alter table CRM_MEMBER_GRP modify (PREPAID_USER DEFAULT 'N');

alter table CRM_MEMBER_GRP_AUD add PREPAID_USER varchar2(1 char);

update CRM_MEMBER_GRP set PREPAID_USER = 'N' where PREPAID_USER IS NULL;