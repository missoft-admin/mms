package com.transretail.crm.template.service.streaming.excel.handler.cell;

import com.transretail.crm.core.entity.MemberModel;
import com.transretail.crm.core.entity.embeddable.Npwp;
import com.transretail.crm.template.service.MemberTemplateParseException;

/**
 * @author Allan G. Ramirez (agramirez@exist.com)
 */
public class NpwpNameHandler implements CellHandler {
    @Override
    public void handle(MemberModel dto, String cellText) throws MemberTemplateParseException {
        if (dto.getNpwp() == null) {
            dto.setNpwp(new Npwp());
        }
        dto.getNpwp().setNpwpName(cellText);
    }
}
