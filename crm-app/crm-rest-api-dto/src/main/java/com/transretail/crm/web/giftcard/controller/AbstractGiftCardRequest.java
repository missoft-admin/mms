package com.transretail.crm.web.giftcard.controller;

import com.transretail.crm.giftcard.dto.GiftCardTransactionInfo;
import javax.validation.constraints.Size;
import org.hibernate.validator.constraints.NotEmpty;

/**
 * @author Monte Cillo Co (mco@exist.com)
 */
public abstract class AbstractGiftCardRequest implements GiftCardRequestSignatureProvider {

    @NotEmpty
    @Size(max = 5)
    private String merchantId;
    @NotEmpty
    @Size(max = 12)
    private String terminalId;
    @NotEmpty
    @Size(max = 12)
    private String cashierId;
    @NotEmpty
    @Size(max = 32)
    private String validationKey;

    public String getMerchantId() {
        return merchantId;
    }

    public void setMerchantId(String merchantId) {
        this.merchantId = merchantId;
    }

    public String getTerminalId() {
        return terminalId;
    }

    public void setTerminalId(String terminalId) {
        this.terminalId = terminalId;
    }

    public String getCashierId() {
        return cashierId;
    }

    public void setCashierId(String cashierId) {
        this.cashierId = cashierId;
    }

    public String getValidationKey() {
        return validationKey;
    }

    public void setValidationKey(String validationKey) {
        this.validationKey = validationKey;
    }

    public abstract GiftCardTransactionInfo toGiftCardTransactionInfo();

    protected GiftCardTransactionInfo templateBaseTransactionInfo() {
        return new GiftCardTransactionInfo().merchantId(this.getMerchantId())
                .terminalId(this.getTerminalId()).cashierId(this.getCashierId());
    }
}
