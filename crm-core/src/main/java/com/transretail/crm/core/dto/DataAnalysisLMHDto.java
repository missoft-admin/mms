package com.transretail.crm.core.dto;

import java.math.BigDecimal;







public class DataAnalysisLMHDto {
	
	private String monthYear;
	private String lightMembers;
	private String mediumMembers;
	private String heavyMembers;
	
	private BigDecimal light;
	private BigDecimal medium;
	private BigDecimal heavy;
	public String getMonthYear() {
		return monthYear;
	}
	public void setMonthYear(String monthYear) {
		this.monthYear = monthYear;
	}
	
	public BigDecimal getLight() {
		return light;
	}
	public void setLight(BigDecimal light) {
		this.light = light;
	}
	public BigDecimal getMedium() {
		return medium;
	}
	public void setMedium(BigDecimal medium) {
		this.medium = medium;
	}
	public BigDecimal getHeavy() {
		return heavy;
	}
	public void setHeavy(BigDecimal heavy) {
		this.heavy = heavy;
	}
	public String getLightMembers() {
		return lightMembers;
	}
	public void setLightMembers(String lightMembers) {
		this.lightMembers = lightMembers;
	}
	public String getMediumMembers() {
		return mediumMembers;
	}
	public void setMediumMembers(String mediumMembers) {
		this.mediumMembers = mediumMembers;
	}
	public String getHeavyMembers() {
		return heavyMembers;
	}
	public void setHeavyMembers(String heavyMembers) {
		this.heavyMembers = heavyMembers;
	}
	
}
