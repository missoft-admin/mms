<%@include file="../../../common/taglibs.jsp"%>


          <table class="table table-condensed table-compressed table-striped">
            <thead>
              <tr>
                <th><spring:message code="gc.so.item.productname" /></th>
                <th><spring:message code="gc.so.item.facevalue" /></th>
                <th><spring:message code="gc.so.item.qty.abbr" /></th>
                <th><spring:message code="gc.so.item.faceamt" /></th>
                <th><spring:message code="gc.so.item.printfee" /></th>

                <th id="notEmptyFlagView"><spring:message code="gc.so.lbl.alloc" /></th>
              </tr>
            </thead>
            <tbody>            
              <c:forEach var="item" items="${salesOrder.items}">
              <tr>
                <td class="table-data-01">${item.productId} - ${item.productDesc}</td>
                <td><fmt:formatNumber type="number" pattern="#,##0" value="${item.faceValue}" /></td>
                <td>${item.quantity}</td>
                <td><fmt:formatNumber type="number" pattern="#,##0" value="${item.faceAmount}" /></td>
                <td><fmt:formatNumber type="number" pattern="#,##0" value="${item.printFee}" /></td>

                <c:if test="${not empty item.gcAllocs}">
                <td class="notEmptyFlag">
                  <div id="${item.id}" data-product-id="${item.productId}" class="allocations table-col-xlarge">
                    <%-- <c:if test="${not empty item.gcAlloc}">
                      ${item.gcAlloc.barcodeStart} - ${item.gcAlloc.barcodeEnd} (${item.gcAlloc.quantity})
                    </c:if> --%>
                    <c:if test="${not empty item.gcAllocs}">
                    <c:forEach var="gcAlloc" items="${item.gcAllocs}">
                      <div>${gcAlloc.barcodeStart} - ${gcAlloc.barcodeEnd} (${gcAlloc.quantity})</div>
                    </c:forEach>
                    </c:if>
                  </div>
                </td>
                </c:if>
              </tr>
              </c:forEach>
            </tbody>
          </table>

<style type="text/css"> 
<!-- 
  .content_soForm .table-col-xlarge { width: 360px; }
--> 
</style>
<script type='text/javascript'>
$(document).ready( function() {
	  if ( !$( ".notEmptyFlag" ).length ) {
		    $( "#notEmptyFlagView" ).remove();
	  }
});
</script>