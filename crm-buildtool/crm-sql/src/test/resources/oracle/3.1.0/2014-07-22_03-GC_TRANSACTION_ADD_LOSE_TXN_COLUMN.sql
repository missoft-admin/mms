
alter table CRM_GC_TRANSACTION 
  add LOSE_TXN number(1,0);

update CRM_GC_TRANSACTION set LOSE_TXN=0;

alter table CRM_GC_TRANSACTION 
  modify LOSE_TXN number(1,0) not null;
