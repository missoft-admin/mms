$(document).ready( function() {

	var $theSearchField = $( "#searchField" );
	var $theSearchValue = $( "#searchValue" );
	var $thePromotionSearchForm = $( "#promotionSearchForm" );



	initFormFields();



	function initFormFields() {
		
		$("#startDate").datepicker({
		    autoclose : true,
		    format : "dd M yyyy",
		}).on('changeDate', function(selected){
			startDate = new Date(selected.date.valueOf());
			startDate.setDate(startDate.getDate(new Date(selected.date.valueOf())));
		    $('#endDate').datepicker('setStartDate', startDate);
		});
		$("#endDate").datepicker({
			autoclose : true,
			format : "dd M yyyy",
		}).on('changeDate', function(selected){
			FromEndDate = new Date(selected.date.valueOf());
	        FromEndDate.setDate(FromEndDate.getDate(new Date(selected.date.valueOf())));
	        $('#startDate').datepicker('setEndDate', FromEndDate);
		});

		$thePromotionSearchForm.submit( function(e) {
			e.preventDefault();
			$theSearchValue.attr( "name", $theSearchField.val() );
			searchPromotions( $thePromotionSearchForm.attr( "action" ) );
		});
	}



	function searchPromotions( inUrl ) {
		$.ajax({
			cache		: false,
			dataType	: "html",
			type 		: "GET", 
			url 		: inUrl,		
			data		: $thePromotionSearchForm.serialize(),
			error 		: function(jqXHR, textStatus, errorThrown) {},
			success 	: function( inResponse ) {
				$( "#list_promotions" ).html( inResponse );
			}
		});
	}

});