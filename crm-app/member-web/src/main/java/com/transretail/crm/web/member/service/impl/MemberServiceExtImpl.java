package com.transretail.crm.web.member.service.impl;

import java.io.InputStream;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang3.BooleanUtils;
import org.jasypt.util.text.BasicTextEncryptor;
import org.joda.time.DateTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.Cache;
import org.springframework.cache.CacheManager;
import org.springframework.context.MessageSource;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;
import org.springframework.util.StringUtils;
import org.springframework.web.servlet.support.RequestContextUtils;

import com.google.common.collect.Lists;
import com.mysema.query.Tuple;
import com.mysema.query.jpa.impl.JPAQuery;
import com.mysema.query.types.expr.BooleanExpression;
import com.transretail.crm.common.reporting.jasper.JRProcessor;
import com.transretail.crm.common.service.dto.request.PageSortDto;
import com.transretail.crm.common.service.dto.response.ResultList;
import com.transretail.crm.common.service.exception.MessageSourceResolvableException;
import com.transretail.crm.common.service.util.SpringDataPagingUtil;
import com.transretail.crm.common.util.SimpleEncrytor;
import com.transretail.crm.common.web.controller.support.RequestUtils;
import com.transretail.crm.core.dto.EmployeePurchaseTxnDto;
import com.transretail.crm.core.dto.ShoppingListDto;
import com.transretail.crm.core.entity.EmployeePurchaseTxnModel;
import com.transretail.crm.core.entity.MemberModel;
import com.transretail.crm.core.entity.PointsTxnModel;
import com.transretail.crm.core.entity.PosPayment;
import com.transretail.crm.core.entity.QEmployeePurchaseTxnModel;
import com.transretail.crm.core.entity.QMemberModel;
import com.transretail.crm.core.entity.QPosPayment;
import com.transretail.crm.core.entity.enums.MemberCreatedFromType;
import com.transretail.crm.core.entity.enums.TxnStatus;
import com.transretail.crm.core.entity.enums.VoucherTransactionType;
import com.transretail.crm.core.entity.lookup.LookupDetail;
import com.transretail.crm.core.repo.EmployeePurchaseTxnRepo;
import com.transretail.crm.core.repo.MemberRepo;
import com.transretail.crm.core.repo.PointsTxnModelRepo;
import com.transretail.crm.core.repo.PosPaymentRepo;
import com.transretail.crm.core.service.CodePropertiesService;
import com.transretail.crm.core.service.LookupService;
import com.transretail.crm.core.service.MemberService;
import com.transretail.crm.core.service.PosTransactionService;
import com.transretail.crm.core.service.StoreService;
import com.transretail.crm.core.util.FormatterUtil;
import com.transretail.crm.loyalty.entity.LoyaltyCardInventory;
import com.transretail.crm.loyalty.service.LoyaltyCardService;
import com.transretail.crm.web.dto.PointDto;
import com.transretail.crm.web.dto.PointResultList;
import com.transretail.crm.web.dto.PointSearchDto;
import com.transretail.crm.web.member.bean.ChangePasswordDto2;
import com.transretail.crm.web.member.bean.MemberDto;
import com.transretail.crm.web.member.bean.RegisterDto;
import com.transretail.crm.web.member.service.EmailService;
import com.transretail.crm.web.member.service.MemberServiceExt;

/**
 * @author Allan G. Ramirez (agramirez@exist.com)
 */
@Service("memberServiceExt")
public class MemberServiceExtImpl implements MemberServiceExt, InitializingBean {
    protected static final String CACHE_NAME = "validationCodes";
    private static final Logger _LOG = LoggerFactory.getLogger(MemberServiceExtImpl.class);
    private static final BasicTextEncryptor ENCRYPTOR = new BasicTextEncryptor();
    private static final DateFormat DATE_FORMAT = new SimpleDateFormat("MM/dd/yyyy");
    static {
        ENCRYPTOR.setPassword("tR@N$R3t@1l");
    }

    @PersistenceContext
    private EntityManager itsEntityMgr;
    @Autowired
    private MemberService memberService;
    @Autowired
    private EmailService emailService;
    @Autowired
    private MemberRepo memberRepo;
    @Autowired
    private PasswordEncoder passwordEncoder;
    @Autowired
    private PointsTxnModelRepo pointsModelRepo;
    @Autowired
    private MessageSource messageSource;
    @Autowired
    private CodePropertiesService codePropertiesService;
    @Autowired
    private LookupService lookupService;
    @Autowired
    private StoreService storeService;
    @Autowired
    private EmployeePurchaseTxnRepo purchaseRepo;
    @Autowired
    PosPaymentRepo posPaymentRepo;
    @Autowired
    private PosTransactionService posTransactionService;
    @Autowired
    private LoyaltyCardService loyaltyCardService;
    

    private Cache cache;

    @Autowired
    public void setCacheManager(CacheManager cacheManager) {
        cache = cacheManager.getCache(CACHE_NAME);
    }

    @Override
    public void afterPropertiesSet() throws Exception {
        Assert.notNull(cache, "Cache [] should not be null. Please check ehcache configuration.");
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void save(RegisterDto registerDto, HttpServletRequest request) throws MessageSourceResolvableException {
        String username = registerDto.getUsername();
        String code = encrypt(username);
        _LOG.debug("Registration Validation code of {} : {}", username, code);
        MemberModel model = registerDto.toModel();
        model.setMemberCreatedFromType(MemberCreatedFromType.FROM_PORTAL);
        model.setMemberType(new LookupDetail(codePropertiesService.getDetailMemberTypeIndividual()));
        model.setValidationCode(code);
        memberService.saveCustomerModel(model);
        Locale locale = RequestContextUtils.getLocale(request);
        StringBuilder link = new StringBuilder(RequestUtils.INSTANCE.getRootUrl(request));
        // For now, make sure this url is equal to MemberController#validateRegistration
        link.append("/member/validate");

        Map<String, Object> data = new HashMap<String, Object>();
        data.put("membername", registerDto.getFullName());
        data.put("link", link.toString());
        data.put("queryString", code);
        emailService.sendValidationLink(messageSource.getMessage("member.registration.email.subject", null, locale),
            registerDto.getEmail(), "registrationValidation.ftl", data, locale);
    }

    @Override
    @Transactional
    public boolean validate(String validationCode) {
        boolean validated = false;
        _LOG.debug("Attempting to validate registration code {}", validationCode);
        MemberModel model = memberService.getModelByValidationCode(validationCode);
        if (model != null && !BooleanUtils.toBoolean(model.getEnabled())) {
            model.setEnabled(true);
            memberService.updateCustomerModel(model);
            validated = true;
            _LOG.debug("Validate registration code {} successful.", validationCode);
        }
        return validated;
    }

    @Transactional
    public void sendChangePasswordValidation(String username, ChangePasswordDto2 form, HttpServletRequest request) throws
        MessageSourceResolvableException {
        JPAQuery theQuery = new JPAQuery(itsEntityMgr);
        QMemberModel qMemberModel = QMemberModel.memberModel;
        List<Tuple> result =
            theQuery.from(qMemberModel).where(qMemberModel.username.eq(username))
                .list(qMemberModel.password, qMemberModel.firstName, qMemberModel.lastName,
                    qMemberModel.email);

        if (result.isEmpty()) {
            throw new MessageSourceResolvableException("member.username.notexist", new String[]{username},
                "User with username [" + username + "] does not exist.");
        }
        Tuple columns = result.get(0);
        String password = columns.get(qMemberModel.password);
        String firstName = columns.get(qMemberModel.firstName);
        String lastName = columns.get(qMemberModel.lastName);
        String email = columns.get(qMemberModel.email);

        if (!passwordEncoder.matches(form.getOldPassword(), password)) {
            throw new MessageSourceResolvableException("member.password.invalid", null, "Invalid password.");
        }
        
        if(form.getOldPassword().equals(form.getNewPassword())) {
        	throw new MessageSourceResolvableException("member.password.same", null, "Same current and new password");
        }
        
        if (!StringUtils.hasText(email)) {
            throw new MessageSourceResolvableException("member.username.noemail", new String[]{username},
                "User [" + username + "] does not have an email.");
        }

        String encryptedUsername = encrypt(username);
        try {
            _LOG.debug("Change Password validation code of {} : {}", username, encryptedUsername);
            cache.put(encryptedUsername, form.getPassword());
            Locale locale = RequestContextUtils.getLocale(request);
            StringBuilder link = new StringBuilder(RequestUtils.INSTANCE.getRootUrl(request));
            // For now, make sure this url is equal to MemberController#validatePasswordChange
            link.append("/member/");
            link.append(username);
            link.append("/password/change");

            Map<String, Object> data = new HashMap<String, Object>();
            String memberName = FormatterUtil.INSTANCE.formatName(lastName, firstName);
            String subject = messageSource.getMessage("member.password.change.email.subject", null, locale);
            data.put("membername", StringUtils.hasText(memberName) ? memberName : username);
            data.put("title", subject);
            data.put("link", link.toString());
            data.put("queryString", encryptedUsername);
            emailService.sendValidationLink(subject, email, "genericValidationTemplate.ftl", data, locale);
        } catch (MessageSourceResolvableException e) {
            // Remove from cache if email is unsuccessful
            cache.evict(encryptedUsername);
            throw e;
        }
    }

    /**
     * Validate the validation code and proceed the changing of password of the user associated with username
     *
     * @param validationCode validation code sent when changing password
     * @param username       username of member
     * @return true if validated. Otherwise, false
     * @see MemberServiceExtImpl#sendChangePasswordValidation(String, com.transretail.crm.web.member.bean.ChangePasswordDto2, javax.servlet.http.HttpServletRequest
     */
    @Transactional
    public boolean validateChangePassword(String username, String validationCode) {
        boolean result = false;
        // See saveChangePasswordValidation below
        Cache.ValueWrapper cachedPass = cache.get(validationCode);
        if (cachedPass != null && StringUtils.hasText(cachedPass.get().toString())) {
            MemberModel model = memberRepo.findByUsername(username);
            model.setPassword(passwordEncoder.encode(cachedPass.get().toString()));
            model.setValidationCode(validationCode);
            memberRepo.save(model);
            result = true;
            _LOG.debug("Validate change password code {} successful.", validationCode);
        }
        return result;
    }

    @Override
    public MemberDto getMemberDto(String username) {
    	MemberModel member = memberRepo.findByUsername(username);
    	MemberDto memberDto = new MemberDto(member);
    	if(StringUtils.hasText(member.getLoyaltyCardNo())) {
    		LoyaltyCardInventory card = loyaltyCardService.findLoyaltyCard(member.getLoyaltyCardNo());
		memberDto.setHasLoyaltyCard(card != null);
        	memberDto.setLoyaltyCardIssued(card.getActivationDate().toString("MM-dd-yyyy"));
        	memberDto.setLoyaltyCardExpiry(card.getExpiryDate().toString("MM-dd-yyyy"));
    	}
    	if(StringUtils.hasText(member.getParentAccountId())) {
    		memberDto.setParentName(memberRepo.findByAccountId(member.getParentAccountId())
    				.getFormattedMemberName());
    	}
        return memberDto;
    }

    @Override
    @Transactional
    public void updateMember(MemberDto dto) {
        if (!dto.getMemberTypeCode().equals(codePropertiesService.getDetailMemberTypeCompany())) {
            dto.setCompanyName(null);
            dto.getNpwp().setNpwpId(null);
            dto.getNpwp().setNpwpName(null);
            dto.getNpwp().setNpwpAddress(null);
        }
        MemberModel model = memberRepo.findByUsername(dto.getUsername());
        dto.update(model);
        /*model.setAddress(memberService.saveMemberAddress(model.getAddress()));*/
        memberRepo.save(model);
    }

    @Transactional(rollbackFor = Exception.class)
    public void updatePin(String username, String newPin) throws MessageSourceResolvableException,
        SimpleEncrytor.EncryptDecryptException {
        MemberModel model = memberRepo.findByUsername(username);

        String oldPin = model.getPin();
        if (BooleanUtils.toBoolean(model.getPinEncryped())) {
            oldPin = SimpleEncrytor.getInstance().decrypt(oldPin);
        }

        if (newPin.equals(oldPin)) {
            throw new MessageSourceResolvableException("member.pin.same.old", null,
                "Entered PIN is the same as your current PIN.");
        }
        model.setPin(SimpleEncrytor.getInstance().encrypt(newPin));
        model.setPinEncryped(true);
        memberRepo.save(model);
    }

    @Transactional(readOnly = true)
    public String getPin(String username) {
        QMemberModel qMemberModel = QMemberModel.memberModel;
        JPAQuery theQuery = new JPAQuery(itsEntityMgr);
        List<String> result =
            theQuery.from(qMemberModel).where(qMemberModel.username.eq(username))
                .list(qMemberModel.pin);
        if (!result.isEmpty()) {
            return result.get(0);
        }
        return null;
    }

    @Override
    @Transactional(readOnly = true)
    public PointResultList getUserPoints(PointSearchDto dto) {
        Pageable pageable = SpringDataPagingUtil.INSTANCE.createPageable(dto.getPagination());
        Page<PointsTxnModel> page = pointsModelRepo.findAll(dto.createSearchExpression(), pageable);
        List<PointDto> results = Lists.newArrayList();
        for (PointsTxnModel model : page.getContent()) {
            /*results.add(new PointDto(model.getStoreCode(), model.getTransactionType(), model.getTransactionDateTime(),
                model.getTransactionNo(), model.getTransactionPoints().longValue(), model.getTransactionAmount()));*/
        	PointDto pointDto = new PointDto(model);
        	String storeCode = model.getStoreCode();
        	if(!org.apache.commons.lang3.StringUtils.isBlank(storeCode)) {
        		pointDto.setStoreCode(storeCode + " - " + storeService.getStoreByCode(storeCode));
        	}
        	else {
        		pointDto.setStoreCode("");
        	}
            if(!StringUtils.isEmpty(pointDto.getTransactionMedia())) {
            	LookupDetail payment = lookupService.getDetail(pointDto.getTransactionMedia());
            	if(payment != null)
            		pointDto.setTransactionMediaDesc(payment.getDescription());
            }
        	results.add(pointDto);
        }
        return new PointResultList(results, page.getTotalElements(), page.hasPreviousPage(), page.hasNextPage());
    }
    
    @Override
    @Transactional(readOnly = true)
    public ResultList<EmployeePurchaseTxnDto> getPurchasesForCurrentMonth(Long userId, PageSortDto dto) {
    	return getPurchaseTxns(userId, beginningOfMonth(new DateTime()).toDate(), endOfMonth(new DateTime()).toDate(), dto);
    }

    @Transactional(readOnly = true)
    public Long getTotalPoints(Long userId) {
        QMemberModel qMemberModel = QMemberModel.memberModel;
        JPAQuery theQuery = new JPAQuery(itsEntityMgr);
        List<Double> result =
            theQuery.from(qMemberModel).where(qMemberModel.id.eq(userId))
                .list(qMemberModel.totalPoints);
        if (!result.isEmpty() && result.get(0) != null) {
            return result.get(0).longValue();
        }
        return null;
    }
    
    @Transactional(readOnly = true)
    public Double getTotalPurchaseAmountForCurrentMonth(Long userId) {
        return getTotalPurchaseAmount(userId, beginningOfMonth(new DateTime()).toDate(), endOfMonth(new DateTime()).toDate());
    }
    
    private ResultList<EmployeePurchaseTxnDto> getPurchaseTxns(Long userId, Date dateFrom, Date dateTo, PageSortDto dto) {
    	QEmployeePurchaseTxnModel qPurchase = QEmployeePurchaseTxnModel.employeePurchaseTxnModel;
    	Pageable pageable = SpringDataPagingUtil.INSTANCE.createPageable(dto.getPagination());
    	BooleanExpression filters = qPurchase.memberModel.id.eq(userId)
				.and(qPurchase.transactionDateTime.between(dateFrom, dateTo))
				.and(qPurchase.status.eq(TxnStatus.ACTIVE));
    	Page<EmployeePurchaseTxnModel> page = purchaseRepo.findAll(filters, pageable);
    	return new ResultList<EmployeePurchaseTxnDto>(convertToDto(page.getContent()), page.getTotalElements(), page.hasPreviousPage(),
                page.hasNextPage());
    }
    
    private List<EmployeePurchaseTxnDto> convertToDto(Iterable<EmployeePurchaseTxnModel> models) {
    	List<EmployeePurchaseTxnDto> dtos = new ArrayList<EmployeePurchaseTxnDto>();
    	for(EmployeePurchaseTxnModel model : models) {
    		if(model.getTransactionType() != null && model.getTransactionType().toString().equals(VoucherTransactionType.PURCHASE.toString())) {
	    		List<PosPayment> paymentTypes = getPaymentsTypes( model.getTransactionNo(), model.getTransactionAmount() );
	    		if(paymentTypes.size() < 1) {
	    			EmployeePurchaseTxnDto dto = new EmployeePurchaseTxnDto(model);
	    			dtos.add(dto);
	    		} else {
	    			for(PosPayment payment : paymentTypes) {
	    				EmployeePurchaseTxnDto dto = new EmployeePurchaseTxnDto(model);
	    				/*dto.setPaymentType(lookupService.getDetailByCode(payment.getMediaType()).getDescription());*/ //Bug #91364: description is stored in media_type column of pos_payment
	    				dto.setPaymentType(payment.getMediaType());
	    				dto.setTransactionAmount(payment.getAmount());
	    				dtos.add(dto);
	    			}
	    		}
    		} else {
    			EmployeePurchaseTxnDto dto = new EmployeePurchaseTxnDto(model);
    			dtos.add(dto);
    		}
    	}
    	return dtos;
    }
    
    private List<PosPayment> getPaymentsTypes(String transactionNo, double amount) {
		QPosPayment qPayment = QPosPayment.posPayment;
		return Lists.newArrayList(posPaymentRepo.findAll(qPayment.posTransaction.id.eq(transactionNo).and( qPayment.amount.eq(amount))));
	}
    
    private Double getTotalPurchaseAmount(Long userId, Date dateFrom, Date dateTo) {
    	QEmployeePurchaseTxnModel qPurchase = QEmployeePurchaseTxnModel.employeePurchaseTxnModel;
    	return new JPAQuery(itsEntityMgr)
    		.from(qPurchase)
    		.where(qPurchase.memberModel.id.eq(userId)
    				.and(qPurchase.transactionDateTime.between(dateFrom, dateTo))
    				.and(qPurchase.status.eq(TxnStatus.ACTIVE)))
    		.singleResult(qPurchase.transactionAmount.sum());
    }
    
    private DateTime endOfMonth(DateTime dateTime) {
	    return beginningOfMonth(dateTime.plusMonths(1)).minusMillis(1);
	}
	
	private DateTime beginningOfMonth(DateTime dateTime) {
	    return dateTime.withMillisOfSecond(0).withSecondOfMinute(0).withMinuteOfHour(0).withHourOfDay(0).withDayOfMonth(1);
	}

    @Transactional(readOnly = true)
    public void sendResetPasswordValidation(Date birthDate, String email, HttpServletRequest request) throws
        MessageSourceResolvableException {
        QMemberModel qMemberModel = QMemberModel.memberModel;
        // Only 1 result should be returned if birthDate and email exists in db
        MemberModel member = memberRepo.findOne(qMemberModel.customerProfile.birthdate.eq(birthDate).and(qMemberModel.email.eq(email)));
        if (member == null) {
            String formattedDate = DATE_FORMAT.format(birthDate);
            throw new MessageSourceResolvableException("member.password.reset.error2", new String[]{formattedDate, email},
                "User with birthdate [" + formattedDate + "] and email [" + email + "] does not exist");
        }
        String encryptedUsername = encrypt(member.getUsername());
        try {
            _LOG.debug("Reset Password validation code of {} : {}", member.getUsername(), encryptedUsername);
            cache.put(encryptedUsername, email);
            Locale locale = RequestContextUtils.getLocale(request);
            StringBuilder link = new StringBuilder(RequestUtils.INSTANCE.getRootUrl(request));
            // For now, make sure this url is equal to MemberController#resetPassword
            link.append("/member/");
            link.append(member.getUsername());
            link.append("/password/reset");

            List<Tuple> result =
                new JPAQuery(itsEntityMgr).from(qMemberModel).where(qMemberModel.username.eq(member.getUsername()))
                    .list(qMemberModel.firstName, qMemberModel.lastName);

            Tuple columns = result.get(0);
            String firstName = columns.get(qMemberModel.firstName);
            String lastName = columns.get(qMemberModel.lastName);

            Map<String, Object> data = new HashMap<String, Object>();
            String memberName = FormatterUtil.INSTANCE.formatName(lastName, firstName);
            String subject = messageSource.getMessage("member.password.reset.email.subject", null, locale);
            data.put("membername", StringUtils.hasText(memberName) ? memberName : member.getUsername());
            data.put("title", subject);
            data.put("link", link.toString());
            data.put("queryString", encryptedUsername);
            emailService.sendValidationLink(subject, email, "genericValidationTemplate.ftl", data, locale);
        } catch (MessageSourceResolvableException e) {
            // Remove from cache if email is unsuccessful
            cache.evict(encryptedUsername);
            throw e;
        }
    }

    @Transactional(readOnly = true)
    public boolean validateResetPassword(String username, String validationCode) {
        boolean result = false;
        // See saveChangePasswordValidation below
        Cache.ValueWrapper cachedEmail = cache.get(validationCode);
        if (cachedEmail != null && StringUtils.hasText(cachedEmail.get().toString())) {
            QMemberModel qMemberModel = QMemberModel.memberModel;
            List<String> emails = new JPAQuery(itsEntityMgr).from(qMemberModel).where(qMemberModel.username.eq(username))
                .list(qMemberModel.email);
            String email = emails.get(0);
            if (cachedEmail.get().equals(email)) {
                result = true;
                _LOG.debug("Reset password code {} valid.", validationCode);
                cache.evict(validationCode);
            }
        }
        return result;
    }

    @Transactional
    public void updatePassword(String username, String newPassword) {
        MemberModel model = memberRepo.findByUsername(username);
        model.setPassword(passwordEncoder.encode(newPassword));
        memberRepo.save(model);
    }

    /**
     * Central method for creation of encryption used for validation
     *
     * @param username username of user to validate
     * @return encrypted code
     */
    protected String encrypt(String username) {
        return ENCRYPTOR.encrypt(username + DateTime.now().getMillis());
    }
    
    @Override
    public Boolean isSupplementary(Long id) {
    	JPAQuery query = new JPAQuery(itsEntityMgr);
    	QMemberModel members = QMemberModel.memberModel;
    	
    	return StringUtils.hasText(query.from(members).where(members.id.eq(id))
            .singleResult(members.parentAccountId));
    }
    
    @Override
    public List<MemberModel> getSupplements(Long userId) {
    	JPAQuery query = new JPAQuery(itsEntityMgr);
    	QMemberModel members = QMemberModel.memberModel;
    	
    	String parentAccountId = query.from(members).where(members.id.eq(userId)).singleResult(members.accountId);
    	query = new JPAQuery(itsEntityMgr);
    	return query.from(members).where(members.parentAccountId.eq(parentAccountId)).list(members);
    }

    @Override
    public boolean hasPin(String username) {
        QMemberModel qMemberModel = QMemberModel.memberModel;
        String pin =
            new JPAQuery(itsEntityMgr).from(qMemberModel).where(qMemberModel.username.eq(username)).singleResult(qMemberModel.pin);
        return StringUtils.hasText(pin);
    }
    
    @Override
    public ResultList<ShoppingListDto> getShoppingList(Long userId, PageSortDto pageSort) {
    	MemberModel member = memberRepo.findOne(userId);
    	return posTransactionService.getProductRankingsByTransaction(member.getAccountId(), pageSort);
    }
    
    @Override
    public JRProcessor getShoppingListJRProcessor(Long userId, InputStream logo) {
    	return posTransactionService.createJRProcessor(memberRepo.findOne(userId), logo);
    }
}
