$(document).ready( function() {

	var theErrorContainer = $( "#content_programForm" ).find( "#content_error");

	var $theProgramDialog = $( "#content_programForm" );
	var $theProgramForm = $( "#programForm" );
	var $theProgramDuration = $( "#programDuration" );
	var $theProgramStartDate = $( "#programStartDate" );
	var $theProgramEndDate = $( "#programEndDate" );

	var $thePromotionProgram = $( "#program" );



	initFormFields();
	initDialog();



	function initFormFields() {

		$theProgramForm.submit( function(e) {
			e.preventDefault();
			$( "#content_programForm" ).find( "#saveButton" ).prop( "disabled", true );
			if ( $( "#programDurationStart" ).val() != "" && $( "#programDurationStart" ).val() != null ) {
				$theProgramStartDate.val( $( "#programDurationStart" ).datepicker( "getDate").getTime() );
			}
			if ( $( "#programDurationEnd" ).val() != "" && $( "#programDurationEnd" ).val() != null ) {
				$theProgramEndDate.val( $( "#programDurationEnd" ).datepicker( "getDate").getTime() );
			}
			saveProgramPost( $(this).attr("action") );
		});

		$theProgramDuration.datepicker({
			autoclose	: true,
			format		: "dd M yyyy",
			startDate 	: '-0d'
		});

		setProgramDateDuration();
		setDateDifference();
	}

	function initDialog() {

		$theProgramDialog.modal({ show : false });
		$theProgramDialog.on( "hide", function() { theErrorContainer.hide(); });
	}



	function saveProgramPost( inUrl ) {
		$.ajax({
			cache 		: false, 
			dataType 	: "json",
			type 		: "POST", 
			url 		: inUrl,
			data		: $theProgramForm.serialize(),
			error 		: function(jqXHR, textStatus, errorThrown) { $( "#content_programForm" ).find( "#saveButton" ).prop( "disabled", false ); },
			success : function( inResponse ) {			
				if ( inResponse.success ) {
					$theProgramDialog.modal( "hide" );
					//$theProgramDialog.remove();
					$( $theProgramForm )[0].reset();
					updateProgramList( inResponse.result );
				}
				else {
					var theErrorInfo = "";
					$.each( inResponse.result, function( key, value ) {
						theErrorInfo += ( ( key + 1 ) + ". " + value.code + "<br>" );
					});
					theErrorContainer.find( "div" ).html( theErrorInfo );
					theErrorContainer.show( "slow" );
				}
				$( "#content_programForm" ).find( "#saveButton" ).prop( "disabled", false );
			}
		});
	}

	function updateProgramList( inData ) {
		$thePromotionProgram.append( "<option value='" + inData.id + "'>" + inData.name + "</option>" );
		$thePromotionProgram.val( inData.id );
		$thePromotionProgram.change();
	}

	function setProgramDateDuration() {
		$( "#programDurationStart" ).on( "changeDate", function(e) {
			$( "#programDurationEnd" ).datepicker( "setStartDate", $( this ).datepicker( "getDate") );
		});
		$( "#programDurationEnd" ).on( "changeDate", function(e) {
			$( this ).datepicker( "setDate", new Date( $( this ).datepicker( "getDate" ).getTime() + 24*60*60*999.999 ) );
		});
	}

	function setDateDifference() {
		var $theProgramEnd = $( "#programDurationEnd" ), $theProgramStart = $( "#programDurationStart" );
		var conversionFactor = 24*60*60*1000;
		$theProgramStart.change( function(e) {
			if ( $theProgramStart.val() && $theProgramEnd.val()
					&& $theProgramEnd.datepicker( "getDate").getTime() > $theProgramStart.datepicker( "getDate").getTime() ) {
				$( "#noOfProgramDays" ).html( 
					Math.round( Math.abs( 
						( $theProgramEnd.datepicker( "getDate").getTime() - $theProgramStart.datepicker( "getDate").getTime() ) )
							/( conversionFactor ) ) + $( "#noOfProgramDays" ).data( "label" ) );
			}
			else {
				$( "#noOfProgramDays" ).html( "" );
			}
		});
		$theProgramEnd.change( function(e) {
			$theProgramStart.change();
		}).change();
	}

});