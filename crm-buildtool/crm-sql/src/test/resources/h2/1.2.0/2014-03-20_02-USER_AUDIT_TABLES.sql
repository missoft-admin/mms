create table CRM_USER_ROLE_AUD (
        CODE varchar(255) not null,
        REV bigint not null,
        REVTYPE tinyint,
        DESCRIPTION varchar(255),
        ENABLED char(1),
        primary key (CODE, REV)
    );

create table CRM_USER_PERMISSION_AUD (
        CODE varchar(255) not null,
        REV bigint not null,
        REVTYPE tinyint,
        DESCRIPTION varchar(255),
        ROLE_CATEGORY varchar(1000),
        STATUS varchar(8),
        primary key (CODE, REV)
    );

create table CRM_USER_ROLE_PERMISSION_AUD (
        id bigint not null,
        REV bigint not null,
        REVTYPE tinyint,
        PERM_ID varchar(255),
        ROLE_ID varchar(255),
        USER_ID bigint,
        primary key (id, REV)
    );

create table CRM_USER_AUD (
        id bigint not null,
        REV bigint not null,
        REVTYPE tinyint,
        EMAIL varchar(255),
        ACCOUNT_ENABLED char(1),
        FIRST_NAME varchar(255),
        INVENTORY_LOCATION varchar(255),
        LAST_NAME varchar(255),
        MIDDLE_NAME varchar(255),
        password varchar(255),
        STORE_CODE varchar(255),
        username varchar(255),
        primary key (id, REV)
    );



alter table CRM_USER_ROLE_AUD 
        add constraint FK_7f1lhw2xcqyey4elh13m12fth 
        foreign key (REV) 
        references CRM_REVINFO;

alter table CRM_USER_PERMISSION_AUD 
        add constraint FK_e1dlsylkcdjyyr270x34erv45 
        foreign key (REV) 
        references CRM_REVINFO;

alter table CRM_USER_ROLE_PERMISSION_AUD 
        add constraint FK_5dlqrrlp218ai1k73sgbnw1af 
        foreign key (REV) 
        references CRM_REVINFO;

alter table CRM_USER_AUD 
        add constraint FK_byfy6hi11asf3h8qvd1j4u45p 
        foreign key (REV) 
        references CRM_REVINFO;