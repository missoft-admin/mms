package com.transretail.crm.common.audit.helper;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.aop.framework.Advised;
import org.springframework.aop.support.AopUtils;

/**
 * @author Allan G. Ramirez (agramirez@exist.com)
 */
public class ProxyHelper {
    private final Logger logger = LoggerFactory.getLogger(getClass());

    public Object unwrapIfProxy(Object instance) {
        if (AopUtils.isAopProxy(instance) && instance instanceof Advised) {
            try {
                instance = ((Advised) instance).getTargetSource().getTarget();
            } catch (Exception e) {
                logger.warn("Failed to get proxy target instance.", e);
            }
        }
        return instance;
    }

}
