package com.transretail.crm.core.security.service.impl;


import com.transretail.crm.core.entity.ApplicationConfig;
import com.transretail.crm.core.entity.enums.AppKey;
import com.transretail.crm.core.repo.ApplicationConfigRepo;
import com.transretail.crm.core.security.service.MD5Service;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

@Service("md5Service")
public class MD5ServiceImpl implements MD5Service {

    @Autowired
    ApplicationConfigRepo applicationConfigRepo;

    @Override
    public boolean validateMd5(String keySignature, String validationKey) {
        //append secret Key

        ApplicationConfig appConfig = applicationConfigRepo.findByKey(AppKey.MD5_SECRET_KEY);
        String secretKey =  appConfig == null?"":appConfig.getValue();
        if(secretKey == null) {

            secretKey = "";
        }


        String signatureWithKey = keySignature + secretKey;

        //create MD5 hash
        if (createMD5Hash(signatureWithKey).equals(validationKey)) {
            return true;
        }



        return false;
    }

    @Override
    public String createMD5Hash(String signature)  {


        MessageDigest md = null;
        try {
            md = MessageDigest.getInstance("MD5");
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
            return "";
        }
        md.update(signature.getBytes());

        byte byteData[] = md.digest();

        //convert the byte to hex format method 1
        StringBuffer sb = new StringBuffer();
        for (int i = 0; i < byteData.length; i++) {
            sb.append(Integer.toString((byteData[i] & 0xff) + 0x100, 16).substring(1));
        }


        return sb.toString();
    }

}
