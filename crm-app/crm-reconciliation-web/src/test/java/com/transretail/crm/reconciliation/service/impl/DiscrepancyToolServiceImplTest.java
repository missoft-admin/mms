package com.transretail.crm.reconciliation.service.impl;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.reset;
import static org.mockito.Mockito.when;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import javax.annotation.Resource;
import javax.sql.DataSource;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.support.PropertySourcesPlaceholderConfigurer;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.AnnotationConfigContextLoader;

import com.google.common.collect.Lists;
import com.transretail.crm.common.util.IOUtils;
import com.transretail.crm.devtool.dto.ColumnMetadataBean;
import com.transretail.crm.devtool.dto.TableMetadataBean;
import com.transretail.crm.reconciliation.service.DbConnectionProvider;
import com.transretail.crm.reconciliation.service.DiscrepancyToolService;
import com.transretail.crm.reconciliation.service.StoreInfoService;

/**
 * @author Allan G. Ramirez (agramirez@exist.com)
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(loader = AnnotationConfigContextLoader.class)
public class DiscrepancyToolServiceImplTest {
    @Configuration
    static class ContextConfiguration {
        @Bean
        public DiscrepancyToolService discrepancyToolService() {
            return new DiscrepancyToolServiceImpl();
        }

        @Bean
        public StoreInfoService storeInfoService() {
            return mock(StoreInfoService.class);
        }

        @Bean
        public PropertySourcesPlaceholderConfigurer propertySourcesPlaceholderConfigurer() throws Exception {
            PropertySourcesPlaceholderConfigurer configurer = new PropertySourcesPlaceholderConfigurer();
            Properties properties = new Properties();
            properties.put("dbee.jdbc.driverClassName", "org.h2.Driver");
            properties.put("dbee.jdbc.url", "jdbc:h2:mem:dbee");
            properties.put("dbee.jdbc.username", "sa");
            properties.put("dbee.jdbc.password", "");
            properties.put("jdbc.driverClassName", "org.h2.Driver");
            properties.put("ttserver.jdbc.url", "jdbc:h2:mem:ttserver");
            properties.put("ttserver.jdbc.username", "sa");
            properties.put("ttserver.jdbc.password", "");
            configurer.setProperties(properties);
            return configurer;
        }

        @Bean(name = "dataSource")
        public DataSource dataSource() {
            DriverManagerDataSource ds = new DriverManagerDataSource();
            ds.setDriverClassName("org.h2.Driver");
            ds.setUrl("jdbc:h2:mem:db2");
            ds.setUsername("sa");
            return ds;
        }

        @Bean
        public DbConnectionProvider connectionProvider() {
            return mock(DbConnectionProvider.class);
        }
    }

    @Autowired
    private DiscrepancyToolService discrepancyToolService;
    @Autowired
    private StoreInfoService storeInfoService;
    @Autowired
    private DbConnectionProvider connectionProvider;
    @Resource(name = "dataSource")
    private DataSource localDataSource;
    private Connection ttCon = null;
    private Connection localCon = null;

    private String tableName = "TEST_TABLE";

    @Before
    public void onSetup() throws Exception {
        reset(storeInfoService);
        reset(connectionProvider);

        ttCon = createTtConnection();

        // Prepare
        String createTableScript =
            "CREATE TABLE " + tableName + " ( ID VARCHAR(15), CREATED_DATETIME timestamp, PRIMARY KEY (ID));";

        Statement ttSt = null;
        try {
            ttCon.setAutoCommit(false);
            ttSt = ttCon.createStatement();
            ttSt.execute(createTableScript);
            ttCon.commit();
        } finally {
            IOUtils.INSTANCE.close(ttSt);
        }

        localCon = localDataSource.getConnection();
        Statement localSt = null;
        try {
            localCon.setAutoCommit(false);
            localSt = localCon.createStatement();
            localSt.execute(createTableScript);
            localCon.commit();
        } finally {
            IOUtils.INSTANCE.close(localSt);
        }
    }

    @After
    public void tearDown() throws Exception {
        String truncateScript = "DROP TABLE " + tableName + ";";
        Statement ttSt = null;
        try {
            ttSt = ttCon.createStatement();
            ttSt.execute(truncateScript);
            ttCon.commit();
            ;
        } finally {
            IOUtils.INSTANCE.close(ttSt);
            IOUtils.INSTANCE.close(ttCon, true);
        }

        localCon = localDataSource.getConnection();
        Statement localSt = null;
        try {
            localSt = localCon.createStatement();
            localSt.execute(truncateScript);
            localCon.commit();
        } finally {
            IOUtils.INSTANCE.close(localSt);
            IOUtils.INSTANCE.close(localCon, true);
        }
    }

    @Test
    public void getLocalDbMissingRecords() throws Exception {
        // Prepare
        Timestamp now = new Timestamp(System.currentTimeMillis());
        String insertScript = "INSERT INTO " + tableName + "(ID, CREATED_DATETIME) VALUES (?, ?);";
        // COnnections are intentionally not closed because we are using in memory-db
        PreparedStatement ps = null;
        try {
            ttCon.setAutoCommit(false);
            ps = ttCon.prepareStatement(insertScript);
            ps.setObject(1, "1");
            ps.setTimestamp(2, now);
            Assert.assertEquals(1, ps.executeUpdate());
            ttCon.commit();
        } finally {
            IOUtils.INSTANCE.close(ps);
        }

        List<TableMetadataBean> beans = Lists.newArrayList();
        TableMetadataBean bean = new TableMetadataBean();
        bean.setTable(tableName);
        bean.setId("ID");
        beans.add(bean);

        when(connectionProvider.createMainDbDbConnection()).thenReturn(createTtConnection());
        Map<String, List<ColumnMetadataBean>> actualResult = discrepancyToolService.getLocalDbMissingRecords(beans);
        List<ColumnMetadataBean> missingRecords = actualResult.get(tableName);
        assertEquals(1, missingRecords.size());
        assertEquals("1", missingRecords.get(0).getId());
        assertEquals(now.toString(), missingRecords.get(0).getCreatedDateTime());
    }

    @Test
    public void getMainDbMissingIds() throws Exception {
        // Prepare
        Timestamp now = new Timestamp(System.currentTimeMillis());
        String insertScript = "INSERT INTO " + tableName + "(ID, CREATED_DATETIME) VALUES (?, ?);";
        // COnnections are intentionally not closed because we are using in memory-db
        Connection con = localDataSource.getConnection();

        PreparedStatement ps = null;
        try {
            con.setAutoCommit(false);
            ps = con.prepareStatement(insertScript);
            ps.setObject(1, "1");
            ps.setTimestamp(2, now);
            Assert.assertEquals(1, ps.executeUpdate());
            con.commit();
        } finally {
            IOUtils.INSTANCE.close(ps);
        }

        List<TableMetadataBean> beans = Lists.newArrayList();
        TableMetadataBean bean = new TableMetadataBean();
        bean.setTable(tableName);
        bean.setId("ID");
        beans.add(bean);

        when(connectionProvider.createMainDbDbConnection()).thenReturn(createTtConnection());
        Map<String, List<ColumnMetadataBean>> actualResult = discrepancyToolService.getMainDbMissingRecords(beans);
        List<ColumnMetadataBean> missingRecords = actualResult.get(tableName);
        assertEquals(1, missingRecords.size());
        assertEquals("1", missingRecords.get(0).getId());
        assertEquals(now.toString(), missingRecords.get(0).getCreatedDateTime());
    }

    private Connection createTtConnection() throws SQLException {
        return DriverManager.getConnection("jdbc:h2:mem:db1", "sa", "");
    }
}
