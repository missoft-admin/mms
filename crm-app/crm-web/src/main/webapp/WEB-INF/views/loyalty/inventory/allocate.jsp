<%@ include file="../../common/taglibs.jsp" %>

<c:set var="ENCTYPE" value="application/x-www-form-urlencoded"/>

<div class="modal-dialog">
  <div class="modal-content">
    
    <div class="modal-header">
		  <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
			<h4 class="isPrimary"><spring:message code="label_multiple_allocation" /></h4>
		</div>
 
    <div class="modal-body">
      <form:form id="multipleAllocateDto" name="multipleAllocateDto" modelAttribute="multipleAllocateDto" data-print-url="${pageContext.request.contextPath}/loyaltycardinventory/printallocation?" action="${pageContext.request.contextPath}/loyaltycardinventory/saveallocation/" method="POST" enctype="${ENCTYPE}" cssClass="form-reset" >

      <div id="contentError" class="hide alert alert-error">
        <button type="button" class="close" onclick="$(this).parent().hide();">&times;</button>
        <div><form:errors path="*"/></div>
      </div>
      
      <div class="contentMain">
        <div class="row-fluid">
          <div class="span4">
            <spring:message code="inventory_prop_allocatedate" var="allocateDate" />
            <label for="">${allocateDate}</label>
            <form:input path="allocateDate" readonly="true" />
          </div>
          <div class="span4">
            <spring:message code="inventory_prop_encodedby" var="encodedBy" />
            <label for="">${encodedBy}</label>
            <form:input path="encodedBy" readonly="true" />
          </div>
          
        </div>
        <div class="row-fluid">
          <div class="span4">
            <spring:message code="inventory_prop_location" var="locationLabel" />
            <label for="">${locationLabel}</label>
            <input type="text" name="locationDesc" value="${multipleAllocateDto.locationName}" disabled="disabled" /><input type="hidden" name="location" value="${multipleAllocateDto.location}" />
          </div>
          
          <div class="span4">
            <spring:message code="inventory_prop_allocateto" var="allocateTo" />
            <label for="">${allocateTo}</label>
            <select name="transferTo">
            <c:forEach items="${inventoryLocations}" var="item" >
              <c:if test="${item.key != multipleAllocateDto.location}">
              <option value="${item.key}">${item.value}</option>
              </c:if>
            </c:forEach>
            </select>
          </div>
          
        </div>
    
        <div id="productContainer">
          <table id="productTable" class="table table-condensed table-compressed table-striped">
            <thead>
              <tr>
                <th><spring:message code="mo_tier_prop_name" /></th>
                <th><spring:message code="mo_tier_prop_quantity" /></th>
                <th><spring:message code="mo_tier_prop_startingseries" /></th>
                <th><spring:message code="mo_tier_prop_endingseries" /></th>
                <th></th>
              </tr>
            </thead>
            <tbody>
              <c:forEach var="item" items="${multipleAllocateDto.allocations}" varStatus="status" >
              <tr>
                <td>
                  <select name="allocations[${status.index}].cardType">
                  <c:forEach items="${products}" var="tier" >
                    <option value="${tier.code}">${tier.description}</option>
                  </c:forEach>
                  </select>
                </td>
                <td><input type="text" class="input-mini modalQuantity numeric" /></td>
                <td><input type="text" class="input-medium numeric modalStartingSeries" value="${item.startingSeries}" name="allocations[${status.index}].startingSeries" maxlength="12" /></td>
                <td><input type="text" class="input-medium numeric modalEndingSeries" value="${item.endingSeries}" name="allocations[${status.index}].endingSeries" maxlength="12" /></td>
                <td><button type="button" id="" class="tiptip btn rowDelete icn-delete hidden" title="<spring:message code="label_remove"/>"><spring:message code="label_remove"/></button></td>
              </tr>
              </c:forEach>
            </tbody>
          </table>
          <button type="button" id="addProductBtn" class="tiptip btn pull-right" style="margin-top: 5px;" title="<spring:message code="label_add"/>"><spring:message code="label_add"/></button>
        </div>
      </div>
      </form:form>
    
    </div>
    <div class="modal-footer">
      <button type="button" id="" class="btn btn-default" data-dismiss="modal"><spring:message code="label_cancel"/></button>
      <button type="button" id="" data-status="${forAllocation}" class="btn btn-primary allocateSave"><spring:message code="label_save" /></button>
    </div>
  </div>
</div>

<script src="<spring:url value="/js/viewspecific/loyalty/allocate.js"/>"></script>
<style type="text/css">
  .table tbody tr td:last-child {
      min-width: auto;
  }
</style>