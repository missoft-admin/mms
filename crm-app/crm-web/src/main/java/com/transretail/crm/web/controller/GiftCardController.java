package com.transretail.crm.web.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.support.RequestContextUtils;

import com.google.common.collect.Maps;
import com.transretail.crm.core.entity.lookup.QStore;
import com.transretail.crm.core.service.NameIdPairService;
import com.transretail.crm.giftcard.dto.CardVendorDto;
import com.transretail.crm.giftcard.dto.CardVendorResultList;
import com.transretail.crm.giftcard.dto.CardVendorSearchDto;
import com.transretail.crm.giftcard.dto.GiftCardDto;
import com.transretail.crm.giftcard.dto.GiftCardResultList;
import com.transretail.crm.giftcard.dto.GiftCardSearchDTO;
import com.transretail.crm.giftcard.dto.StockRequestDto;
import com.transretail.crm.giftcard.dto.StockRequestResultList;
import com.transretail.crm.giftcard.dto.StockRequestSearchDto;
import com.transretail.crm.giftcard.entity.QCardVendor;
import com.transretail.crm.giftcard.entity.QGiftCard;
import com.transretail.crm.giftcard.service.CardVendorService;
import com.transretail.crm.giftcard.service.GiftCardService;
import com.transretail.crm.giftcard.service.StockRequestService;

/**
 *
 */
@Controller
@RequestMapping("/giftcard")
public class GiftCardController {
    protected static final String FORM_ATTR_NAME = "form";
    protected static final String KEY_SUCCESS = "successMessages";
    @Autowired
    private MessageSource messageSource;
    @Autowired
    private CardVendorService vendorService;
    @Autowired
    private GiftCardService giftCardService;
    @Autowired
    private NameIdPairService nameIdPairService;
    @Autowired
    private StockRequestService stockRequestService;

    @RequestMapping(value = "/list", method = RequestMethod.GET)
    public ModelAndView showGiftCardListPage() {
        QCardVendor qCardVendor = QCardVendor.cardVendor;
        return new ModelAndView("giftcard/list")
                // TODO: Create and use vendor search dialog instead of displaying everything in dropdown
                .addObject("vendors",
                        nameIdPairService.getNameIdPairs(null, qCardVendor, qCardVendor.id, qCardVendor.formalName, null));
    }

    @RequestMapping(value = "/vendor/list", method = RequestMethod.GET)
    public ModelAndView showVendorList() {
        return new ModelAndView("giftcard/vendor/list");
    }

    @RequestMapping(value = "/vendor/add", method = RequestMethod.GET)
    public ModelAndView showAddVendorPage() {
        return new ModelAndView("giftcard/vendor/add").addObject(FORM_ATTR_NAME, new CardVendorDto());
    }

    @RequestMapping(value = "/stock/list", method = RequestMethod.GET)
    public ModelAndView showStockList() {
        QGiftCard qGiftCard = QGiftCard.giftCard;
        final QStore qStore = QStore.store;
        return new ModelAndView("giftcard/stock/list")
            // TODO: Create and use gift card search dialog instead of displaying everything in dropdown
            .addObject("gcs",
                nameIdPairService.getNameIdPairs(null, qGiftCard, qGiftCard.id, qGiftCard.name, null))
                // TODO: Create and use gift card search dialog instead of displaying everything in dropdown if there are 100+ or 1000+ stores
            .addObject("stores", nameIdPairService.getNameIdPairs(null, qStore, qStore.id, qStore.name, null));
    }

    @ExceptionHandler(MethodArgumentNotValidException.class)
    @ResponseBody
    public Map<String, List<String>> handleMethodArgumentNotValidException(MethodArgumentNotValidException error) {
        BindingResult result = error.getBindingResult();
        List<String> errors = new ArrayList<String>();
        for (ObjectError oe : result.getAllErrors()) {
            errors.add(messageSource.getMessage(oe, LocaleContextHolder.getLocale()));
        }
        Map<String, List<String>> map = new HashMap<String, List<String>>();
        map.put("errorMessages", errors);
        return map;
    }

    /* Start of gift card ajax */
    @RequestMapping(value = "/{gcId}", method = RequestMethod.GET)
    @ResponseBody
    public GiftCardDto getGiftCardById(@PathVariable("gcId") Long gcId) {
        return giftCardService.getGiftCard(gcId);
    }

    @RequestMapping(value = "/list", method = RequestMethod.POST)
    @ResponseBody
    public GiftCardResultList listGiftCards(@RequestBody GiftCardSearchDTO searchForm) {
        return giftCardService.getGiftCards(searchForm);
    }

    @RequestMapping(value = "/add", method = RequestMethod.POST)
    @ResponseBody
    public Map<String, Object> doAddGc(@Valid @RequestBody GiftCardDto dto, HttpServletRequest request) {
        Map<String, Object> map = Maps.newHashMap();
        Locale locale = RequestContextUtils.getLocale(request);
        giftCardService.saveGiftCard(dto);
        map.put(KEY_SUCCESS, messageSource.getMessage("generic.save.successful", new String[]{dto.getName()}, locale));
        return map;
    }

    @RequestMapping(value = "/update/{gcId}", method = RequestMethod.POST)
    @ResponseBody
    public Map<String, Object> doUpdateGc(@PathVariable("gcId") Long gcId, @Valid @RequestBody GiftCardDto dto,
            HttpServletRequest request) {
        Map<String, Object> map = Maps.newHashMap();
        Locale locale = RequestContextUtils.getLocale(request);
        giftCardService.updateGiftCard(gcId, dto);
        map.put(KEY_SUCCESS, messageSource.getMessage("generic.save.successful", new String[]{dto.getName()}, locale));
        return map;
    }
    /* End of gift card ajax */

    /* Start of Vendor ajax */
    @RequestMapping(value = "/vendor/{vendorId}", method = RequestMethod.GET)
    @ResponseBody
    public CardVendorDto getCardVendorById(@PathVariable("vendorId") Long vendorId) {
        return vendorService.getCardVendorById(vendorId);
    }

    @RequestMapping(value = "/vendor/list", method = RequestMethod.POST)
    @ResponseBody
    public CardVendorResultList listVendors(@RequestBody CardVendorSearchDto searchForm) {
        return vendorService.getCardVendors(searchForm);
    }

    @RequestMapping(value = "/vendor/add", method = RequestMethod.POST)
    @ResponseBody
    public Map<String, Object> doAddVendor(@Valid @RequestBody CardVendorDto dto, HttpServletRequest request) {
        Map<String, Object> map = Maps.newHashMap();
        Locale locale = RequestContextUtils.getLocale(request);
        vendorService.saveCardVendor(dto);
        map.put(KEY_SUCCESS, messageSource.getMessage("generic.save.successful", new String[]{dto.getFormalName()}, locale));
        return map;
    }

    @RequestMapping(value = "/vendor/update/{vendorId}", method = RequestMethod.POST)
    @ResponseBody
    public Map<String, Object> doUpdateVendor(@PathVariable("vendorId") Long vendorId, @Valid @RequestBody CardVendorDto dto,
            HttpServletRequest request) {
        Map<String, Object> map = Maps.newHashMap();
        Locale locale = RequestContextUtils.getLocale(request);
        vendorService.updateCardVendor(vendorId, dto);
        map.put(KEY_SUCCESS, messageSource.getMessage("generic.save.successful", new String[]{dto.getFormalName()}, locale));
        return map;
    }
    /* End of vendor ajax */

    /* Start of stock request ajax */
    @RequestMapping(value = "/stock/{stockRequestId}", method = RequestMethod.GET)
    @ResponseBody
    public StockRequestDto getStockRequestById(@PathVariable("stockRequestId") Long stockRequestId) {
        return stockRequestService.getStockRequest(stockRequestId);
    }

    @RequestMapping(value = "/stock/list", method = RequestMethod.POST)
    @ResponseBody
    public StockRequestResultList listStockRequests(@RequestBody StockRequestSearchDto searchForm) {
        return stockRequestService.getStockRequests(searchForm);
    }

    @RequestMapping(value = "/stock/add", method = RequestMethod.POST)
    @ResponseBody
    public Map<String, Object> doAddStockRequest(@Valid @RequestBody StockRequestDto dto, HttpServletRequest request) {
        Map<String, Object> map = Maps.newHashMap();
        stockRequestService.saveStockRequest(dto);
//        map.put(KEY_SUCCESS, messageSource
//                .getMessage("gc.stock.save.successful", new String[]{dto.getGiftCardName()}, RequestContextUtils.getLocale(request)));
        return map;
    }

    @RequestMapping(value = "/stock/update/{stockRequestId}", method = RequestMethod.POST)
    @ResponseBody
    public Map<String, Object> doUpdateStockRequest(@PathVariable("stockRequestId") Long stockRequestId,
            @Valid @RequestBody StockRequestDto dto,
            HttpServletRequest request) {
        Map<String, Object> map = Maps.newHashMap();
        Locale locale = RequestContextUtils.getLocale(request);
        stockRequestService.updateStockRequest(stockRequestId, dto);
//        map.put(KEY_SUCCESS, messageSource.getMessage("gc.stock.save.successful", new String[]{dto.getGiftCardName()}, locale));
        return map;
    }

    @RequestMapping(value = "/stock/submit/{stockRequestId}/{quantity}", method = RequestMethod.POST)
    @ResponseBody
    public Map<String, Object> doSubmitStockRequest(@PathVariable("stockRequestId") Long stockRequestId,
            @PathVariable("quantity") Integer quantity, HttpServletRequest request) {
        Locale locale = RequestContextUtils.getLocale(request);
        stockRequestService.submitStockRequest(stockRequestId, quantity);
        Map<String, Object> map = Maps.newHashMap();
        map.put(KEY_SUCCESS, messageSource.getMessage("gc.stock.submitted", null, locale));
        return map;
    }

    @RequestMapping(value = "/stock/validate/{stockRequestId}", method = RequestMethod.POST)
    @ResponseBody
    public Map<String, Object> doValidateStockRequest(@PathVariable("stockRequestId") Long stockRequestId,
            HttpServletRequest request) {
        Locale locale = RequestContextUtils.getLocale(request);
        stockRequestService.validateStockRequest(stockRequestId);
        Map<String, Object> map = Maps.newHashMap();
        map.put(KEY_SUCCESS, messageSource.getMessage("gc.stock.validated", null, locale));
        return map;
    }

    @RequestMapping(value = "/stock/reject/{stockRequestId}", method = RequestMethod.POST)
    @ResponseBody
    public Map<String, Object> doRejectStockRequest(@PathVariable("stockRequestId") Long stockRequestId,
            @RequestParam("reason") String reason, HttpServletRequest request) {
        Locale locale = RequestContextUtils.getLocale(request);
        stockRequestService.rejectStockRequest(stockRequestId, reason);
        Map<String, Object> map = Maps.newHashMap();
        map.put(KEY_SUCCESS, messageSource.getMessage("gc.stock.rejected", null, locale));
        return map;
    }

    @RequestMapping(value = "/stock/approve/{stockRequestId}", method = RequestMethod.POST)
    @ResponseBody
    public Map<String, Object> doApproveStockRequest(@PathVariable("stockRequestId") Long stockRequestId,
            HttpServletRequest request) {
        Locale locale = RequestContextUtils.getLocale(request);
        stockRequestService.approveStockRequest(stockRequestId);
        Map<String, Object> map = Maps.newHashMap();
        map.put(KEY_SUCCESS, messageSource.getMessage("gc.stock.approved", null, locale));
        return map;
    }

    @RequestMapping(value = "/stock/disapprove/{stockRequestId}", method = RequestMethod.POST)
    @ResponseBody
    public Map<String, Object> doDisapproveStockRequest(@PathVariable("stockRequestId") Long stockRequestId,
            @RequestParam("reason") String reason, HttpServletRequest request) {
        Locale locale = RequestContextUtils.getLocale(request);
        stockRequestService.disapproveStockRequest(stockRequestId, reason);
        Map<String, Object> map = Maps.newHashMap();
        map.put(KEY_SUCCESS, messageSource.getMessage("gc.stock.disapproved", null, locale));
        return map;
    }
    /* End of stock request ajax */
}
