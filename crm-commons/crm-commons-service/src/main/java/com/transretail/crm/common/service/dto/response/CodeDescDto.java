package com.transretail.crm.common.service.dto.response;

/**
 * @author Allan G. Ramirez (agramirez@exist.com)
 */
public class CodeDescDto {
    private String code;
    private String desc;

    public CodeDescDto() {

    }

    public CodeDescDto(String code, String desc) {
        this.code = code;
        this.desc = desc;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof CodeDescDto)) return false;

        CodeDescDto that = (CodeDescDto) o;

        if (!code.equals(that.code)) return false;

        return true;
    }

    @Override
    public int hashCode() {
        return code.hashCode();
    }
}
