package com.transretail.crm.loyalty.dto;

import java.util.ArrayList;
import java.util.List;

import org.joda.time.LocalDate;
import org.springframework.format.annotation.DateTimeFormat;

public class ReceiveOrderDto {
	
	private Long manufactureOrder;
	
	private String receivedAt;
	
	@DateTimeFormat(pattern = "dd-MM-yyyy")
	private LocalDate receiveDate;
	
	private String receiptNo;
	
	private List<ProductSeriesDto> products;
	
	public ReceiveOrderDto() {
		List<ProductSeriesDto> products = new ArrayList<ProductSeriesDto>();
		products.add(new ProductSeriesDto());
		this.products = products;
	}

	public Long getManufactureOrder() {
		return manufactureOrder;
	}

	public void setManufactureOrder(Long manufactureOrder) {
		this.manufactureOrder = manufactureOrder;
	}

	public String getReceivedAt() {
		return receivedAt;
	}

	public void setReceivedAt(String receivedAt) {
		this.receivedAt = receivedAt;
	}

	public LocalDate getReceiveDate() {
		return receiveDate;
	}

	public void setReceiveDate(LocalDate receiveDate) {
		this.receiveDate = receiveDate;
	}

	public String getReceiptNo() {
		return receiptNo;
	}

	public void setReceiptNo(String receiptNo) {
		this.receiptNo = receiptNo;
	}

	public List<ProductSeriesDto> getProducts() {
		return products;
	}

	public void setProducts(List<ProductSeriesDto> products) {
		this.products = products;
	}
	
}
