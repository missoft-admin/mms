package com.transretail.crm.giftcard.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.Type;
import org.joda.time.LocalDate;

import com.transretail.crm.core.entity.support.CustomAuditableEntity;

@Entity
@Table(name = "CRM_GC_ORD_ITEM")
public class GiftCardOrderItem extends CustomAuditableEntity<Long> implements Serializable {

	private static final long serialVersionUID = 1L;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "GC_ORDER", nullable = false)
	private GiftCardOrder order;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "PROD_PROFILE", nullable = false)
	private ProductProfile profile;

	@Column(name = "PRODUCT_CODE")
	private String productCode;

	@Column(name = "QUANTITY")
	private Long quantity;

	@Column(name = "UNIT_COST")
	private Double unitCost;

	@Column(name = "EXPIRED_DATE")
	@Type(type = "org.jadira.usertype.dateandtime.joda.PersistentLocalDate")
	private LocalDate expiredDate;

	public ProductProfile getProfile() {
		return profile;
	}

	public void setProfile(ProductProfile profile) {
		this.profile = profile;
	}

	public String getProductCode() {
		return productCode;
	}

	public void setProductCode(String productCode) {
		this.productCode = productCode;
	}

	public GiftCardOrder getOrder() {
		return order;
	}

	public void setOrder(GiftCardOrder order) {
		this.order = order;
	}

	public Long getQuantity() {
		return quantity;
	}

	public void setQuantity(Long quantity) {
		this.quantity = quantity;
	}

	public Double getUnitCost() {
		return unitCost;
	}

	public void setUnitCost(Double unitCost) {
		this.unitCost = unitCost;
	}

	public LocalDate getExpiredDate() {
		return expiredDate;
	}

	public void setExpiredDate(LocalDate expiredDate) {
		this.expiredDate = expiredDate;
	}

}
