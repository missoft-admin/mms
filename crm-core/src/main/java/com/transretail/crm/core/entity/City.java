package com.transretail.crm.core.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.envers.Audited;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.transretail.crm.core.entity.support.CustomAuditableEntity;

@Audited
@Entity
@Table(name = "CRM_CITY")
public class City extends CustomAuditableEntity<Long> implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 7226043337223749092L;

	@ManyToOne
	@JoinColumn(name = "PROVINCE")
	private Province province;
	
	@Column(name = "NAME")
	public String name;
	
	@JsonIgnore
	public Province getProvince() {
		return province;
	}

	public void setProvince(Province province) {
		this.province = province;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
}
