ALTER TABLE CRM_MEMBER
ADD (
REG_ID varchar2(255 char),
BUSINESS_PHONE varchar2(255 char),
NO_OF_EMP varchar2(255 char),
POTENTIAL_BUYING_PER_MONTH varchar2(255 char),
SIM varchar2(255 char),
PASSPORT varchar2(255 char),
HOME_PHONE varchar2(255 char),
CORRESPONDENCE_ADDRESS varchar2(255 char),
INFORMED_ABOUT varchar2(255 char),
INTERESTED_PRODUCTS varchar2(255 char)
);

ALTER TABLE CRM_MEMBER_AUD
ADD (
REG_ID varchar2(255 char),
BUSINESS_PHONE varchar2(255 char),
NO_OF_EMP varchar2(255 char),
POTENTIAL_BUYING_PER_MONTH varchar2(255 char),
SIM varchar2(255 char),
PASSPORT varchar2(255 char),
HOME_PHONE varchar2(255 char),
CORRESPONDENCE_ADDRESS varchar2(255 char),
INFORMED_ABOUT varchar2(255 char),
INTERESTED_PRODUCTS varchar2(255 char)
);
