package com.transretail.crm.giftcard.dto;

import java.util.List;
import java.util.Map;

import org.joda.time.DateTime;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.transretail.crm.core.entity.lookup.LookupDetail;
import com.transretail.crm.giftcard.entity.PrepaidReloadInfo;
import com.transretail.crm.giftcard.entity.ProductProfile;
import com.transretail.crm.giftcard.entity.ProductProfileUnit;
import com.transretail.crm.giftcard.entity.ProductProfile.Denomination;
import com.transretail.crm.giftcard.entity.support.ProductProfileStatus;

public class ProductProfileDto {

	private Long id;
	private String productCode;
	private String productDesc;
	private LookupDetail faceValue;
	private int faceValueDesc;
	private double cardFee = 0;
	private Boolean allowReload = Boolean.FALSE;
	private Boolean allowPartialRedeem = Boolean.FALSE;
	private Long maxAmount = 0L;
	private Long effectiveMonths = 0L;
	private ProductProfileStatus status;
	private Double unitCost;
	private List<PrepaidReloadInfoDto> reloadInfoDtos = Lists.newArrayList();
	private Boolean isEgc;
	private List<ProductProfileUnitDTO> productUnit = Lists.newArrayList();
	private DateTime created;

	public ProductProfileDto() {
		reloadInfoDtos.add(new PrepaidReloadInfoDto());
	}

	public ProductProfileDto(ProductProfile profile) {
		this.id = profile.getId();
		this.productCode = profile.getProductCode();
		this.productDesc = profile.getProductDesc();
		this.faceValue = profile.getFaceValue();
		this.faceValueDesc = Integer.parseInt(profile.getFaceValue().getDescription());
		this.cardFee = profile.getCardFee();
		this.allowReload = profile.isAllowReload();
		this.allowPartialRedeem = profile.isAllowPartialRedeem();
		this.maxAmount = profile.getMaxAmount();
		this.effectiveMonths = profile.getEffectiveMonths();
		this.status = profile.getStatus();
		this.unitCost = profile.getUnitCost();
		this.isEgc = profile.getIsEgc();

		if (profile.getPrepaidReloadInfos() != null && profile.getPrepaidReloadInfos().size() > 0) {
			for (PrepaidReloadInfo reload : profile.getPrepaidReloadInfos()) {
				reloadInfoDtos.add(new PrepaidReloadInfoDto(reload.getId(), reload.getReloadAmount(), reload.getMonthEffective()));
			}
		} else {
			reloadInfoDtos.add(new PrepaidReloadInfoDto());
		}

		if(profile.getProductProfileUnits()!=null  && profile.getProductProfileUnits().size()>0) {
			for(ProductProfileUnit unit:profile.getProductProfileUnits()) {
				ProductProfileUnitDTO dtoUnit=new ProductProfileUnitDTO();
				dtoUnit.setId(unit.getId());
				dtoUnit.setBusinessUnit(unit.getBusinessUnit());
				productUnit.add(dtoUnit);
			}
		}
	}

	public ProductProfile toModel(ProductProfile profile) {
		if (profile == null) {
			profile = new ProductProfile();
		}
		profile.setProductCode(this.productCode);
		profile.setProductDesc(this.productDesc);
		if (this.faceValue != null) {
			profile.setFaceValue(this.faceValue);
		}
		profile.setCardFee(this.cardFee);
		profile.setAllowReload(this.allowReload);
		profile.setAllowPartialRedeem(this.allowPartialRedeem);
		profile.setMaxAmount(this.maxAmount);
		profile.setEffectiveMonths(this.effectiveMonths);
		profile.setStatus(this.status);
		profile.setUnitCost(this.unitCost);

		Map<Long, PrepaidReloadInfo> existingReloadInfos = Maps.newHashMap();
		List<PrepaidReloadInfo> reloadInfoList = profile.getPrepaidReloadInfos();
		if (reloadInfoList != null) {
			for (PrepaidReloadInfo reload : profile.getPrepaidReloadInfos()) {
				existingReloadInfos.put(reload.getId(), reload);
			}
		} else {
			reloadInfoList = Lists.newArrayList();
			profile.setPrepaidReloadInfos(reloadInfoList);
		}
		if (this.allowReload) {
			for (PrepaidReloadInfoDto dto : reloadInfoDtos) {
				if (dto.getReloadAmount() != null && dto.getMonthEffective() != null) {
					PrepaidReloadInfo reloadInfo = existingReloadInfos.get(dto.getId());
					if (reloadInfo == null) {
						reloadInfo = new PrepaidReloadInfo();
						reloadInfo.setProductProfile(profile);
						reloadInfoList.add(reloadInfo);
					} else {
						existingReloadInfos.remove(dto.getId());
					}
					reloadInfo.setMonthEffective(dto.getMonthEffective());
					reloadInfo.setReloadAmount(dto.getReloadAmount());
				}
			}
		}
		for (PrepaidReloadInfo reloadInfo : existingReloadInfos.values()) {
			reloadInfoList.remove(reloadInfo);
		}
		profile.setIsEgc(this.isEgc);

		if (productUnit != null) {
			Map<Long,ProductProfileUnit> existingUnit=Maps.newHashMap();
			for(ProductProfileUnit unit:profile.getProductProfileUnits()){
				existingUnit.put(unit.getId(), unit);
			}
			for (ProductProfileUnitDTO unitDto : getProductUnit()) {
				if(unitDto.getId()==null) {
					ProductProfileUnit punit=new ProductProfileUnit();
					punit.setProductProfile(profile);
					punit.setBusinessUnit(unitDto.getBusinessUnit());
					profile.getProductProfileUnits().add(punit);
				}else{
					ProductProfileUnit punit=existingUnit.get(unitDto.getId());
					punit.setBusinessUnit(unitDto.getBusinessUnit());
					existingUnit.remove(unitDto.getId());
				}
			}
			for(ProductProfileUnit removedUnit:existingUnit.values()){
				profile.getProductProfileUnits().remove(removedUnit);
			}
		}
		return profile;
	}

	public String getProductCode() {
		return productCode;
	}

	public void setProductCode(String productCode) {
		this.productCode = productCode;
	}

	public String getProductDesc() {
		return productDesc;
	}

	public void setProductDesc(String productDesc) {
		this.productDesc = productDesc;
	}

	public LookupDetail getFaceValue() {
		return faceValue;
	}

	public void setFaceValue(LookupDetail faceValue) {
		this.faceValue = faceValue;
	}

	public double getCardFee() {
		return cardFee;
	}

	public void setCardFee(double cardFee) {
		this.cardFee = cardFee;
	}

	public Boolean getAllowReload() {
		return allowReload;
	}

	public void setAllowReload(Boolean allowReload) {
		this.allowReload = allowReload;
	}

	public Boolean getAllowPartialRedeem() {
		return allowPartialRedeem;
	}

	public void setAllowPartialRedeem(Boolean allowPartialRedeem) {
		this.allowPartialRedeem = allowPartialRedeem;
	}

	public Long getMaxAmount() {
		return maxAmount;
	}

	public void setMaxAmount(Long maxAmount) {
		this.maxAmount = maxAmount;
	}

	public Long getEffectiveMonths() {
		return effectiveMonths;
	}

	public void setEffectiveMonths(Long effectiveMonths) {
		this.effectiveMonths = effectiveMonths;
	}

	public ProductProfileStatus getStatus() {
		return status;
	}

	public void setStatus(ProductProfileStatus status) {
		this.status = status;
	}

	public Double getUnitCost() {
		return unitCost;
	}

	public void setUnitCost(Double unitCost) {
		this.unitCost = unitCost;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public List<PrepaidReloadInfoDto> getReloadInfoDtos() {
		return reloadInfoDtos;
	}

	public void setReloadInfoDtos(List<PrepaidReloadInfoDto> reloadInfoDtos) {
		this.reloadInfoDtos = reloadInfoDtos;
	}

	public Boolean getIsEgc() {
		return isEgc;
	}

	public void setIsEgc(Boolean isEgc) {
		this.isEgc = isEgc;
	}

	public int getFaceValueDesc() {
		return faceValueDesc;
	}

	public void setFaceValueDesc(int faceValueDesc) {
		this.faceValueDesc = faceValueDesc;
	}

	public DateTime getCreated() {
		return created;
	}

	public void setCreated(DateTime created) {
		this.created = created;
	}

	public List<ProductProfileUnitDTO> getProductUnit() {
		return productUnit;
	}

	public void setProductUnit(List<ProductProfileUnitDTO> productUnit) {
		this.productUnit = productUnit;
	}

}
