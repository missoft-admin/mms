package com.transretail.crm.giftcard.service.impl;

import com.google.common.collect.Lists;
import com.google.common.collect.Sets;
import com.transretail.crm.core.security.model.impl.CustomSecurityUserDetailsImpl;
import com.transretail.crm.core.security.util.UserUtil;
import com.transretail.crm.core.service.CodePropertiesService;
import com.transretail.crm.giftcard.dto.GiftCardPhysicalCountDto;
import com.transretail.crm.giftcard.entity.GiftCardInventory;
import com.transretail.crm.giftcard.entity.GiftCardPhysicalCount;
import com.transretail.crm.giftcard.entity.QGiftCardInventory;
import com.transretail.crm.giftcard.repo.GiftCardAdjustInventoryRepo;
import com.transretail.crm.giftcard.repo.GiftCardInventoryRepo;
import com.transretail.crm.giftcard.repo.GiftCardPhysicalCountSummaryRepo;
import com.transretail.crm.giftcard.service.GiftCardAdjustmentInventoryService;
import com.transretail.crm.giftcard.service.GiftCardInventoryStockService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import static org.fest.assertions.api.Assertions.assertThat;
import static org.mockito.Mockito.*;

/**
 *
 * @author Monte Cillo Co (mco@exist.com)
 */
@RunWith(PowerMockRunner.class)
@PrepareForTest(UserUtil.class)
public class GiftCardAdjustmentInventoryServiceImplTest {

    @Mock
    private GiftCardAdjustInventoryRepo countRepo;
    @Mock
    private GiftCardPhysicalCountSummaryRepo summaryRepo;
    @Mock
    private GiftCardInventoryRepo inventoryRepo;
    @Mock
    private GiftCardInventoryStockService stockService;
    @InjectMocks
    private final GiftCardAdjustmentInventoryService sut = new GiftCardAdjustmentInventoryServiceImpl();
    @Mock
    private CodePropertiesService codePropertiesService;

    @Before
    public void setup() {
	MockitoAnnotations.initMocks(this);
	when(codePropertiesService.getDetailInvLocationHeadOffice())
		.thenReturn("INVT001");
	reset(countRepo, summaryRepo, inventoryRepo, stockService);
	
    }

    @Test
    public void validateSeries_usingSameSeriesAsParamShouldReturn1Records() {
	QGiftCardInventory qinv = QGiftCardInventory.giftCardInventory;
	String seriesInput = "1404241010000003";
	long series = 1404241010000003l;

	when(inventoryRepo.findAll(qinv.series.in(series, series)))
		.thenReturn(Lists.newArrayList(new GiftCardInventory()));

	boolean validationResult = sut.validateSeries(seriesInput, seriesInput);

	assertThat(validationResult).isTrue();
	verify(inventoryRepo).findAll(qinv.series.in(series, series));
    }

    @Test
    public void validateSeries_usingStartingAndEndingSeriesAsParamShouldReturn2Records() {
	QGiftCardInventory qinv = QGiftCardInventory.giftCardInventory;
	String startingSeriesInput = "1404241010000003";
	String endingSeriesInput = "1404241010000005";
	long startingSeries = 1404241010000003l;
	long endingSeries = 1404241010000005l;

	when(inventoryRepo.findAll(qinv.series.in(startingSeries, endingSeries)))
		.thenReturn(Lists.newArrayList(new GiftCardInventory(), new GiftCardInventory()));

	boolean validationResult = sut.validateSeries(startingSeriesInput, endingSeriesInput);

	assertThat(validationResult).isTrue();
	verify(inventoryRepo).findAll(qinv.series.in(startingSeries, endingSeries));
    }

    @Test
    public void validateSeries_startingGTEndingSeriesShouldReturnFalse() {
	String startingSeriesInput = "1404241010000005";
	String endingSeriesInput = "1404241010000001";

	boolean validationResult = sut.validateSeries(startingSeriesInput, endingSeriesInput);

	assertThat(validationResult).isFalse();
	verifyZeroInteractions(inventoryRepo);
    }

    @Test
    public void validateSeries_notExistingEndingSeriesShouldReturnFalse() {
	QGiftCardInventory qinv = QGiftCardInventory.giftCardInventory;
	String startingSeriesInput = "1404241010000003";
	String endingSeriesInput = "1404241010000005";
	long startingSeries = 1404241010000003l;
	long endingSeries = 1404241010000005l;

	when(inventoryRepo.findAll(qinv.series.in(startingSeries, endingSeries)))
		.thenReturn(Lists.newArrayList(new GiftCardInventory())); // return just one record, assume the one of input series is not existing

	boolean validationResult = sut.validateSeries(startingSeriesInput, endingSeriesInput);

	assertThat(validationResult).isFalse();
	verify(inventoryRepo).findAll(qinv.series.in(startingSeries, endingSeries));
    }

    @Test
    public void isBarcodeExists_shouldReturnTrue() {
	String barcode = "14042410100000029959";
	QGiftCardInventory qinv = QGiftCardInventory.giftCardInventory;
	when(inventoryRepo.findOne(qinv.barcode.eq(barcode)))
		.thenReturn(new GiftCardInventory());

	boolean exists = sut.isBarcodeExist(barcode);
	assertThat(exists).isTrue();

	verify(inventoryRepo).findOne(qinv.barcode.eq(barcode));
    }

    @Test
    public void isBarcodeExists_shouldReturnFalse() {
	String nonExistingBarcode = "14042410100000029959";
	QGiftCardInventory qinv = QGiftCardInventory.giftCardInventory;
	when(inventoryRepo.findOne(qinv.barcode.eq(nonExistingBarcode)))
		.thenReturn(isNull(GiftCardInventory.class));

	boolean exists = sut.isBarcodeExist(nonExistingBarcode);
	assertThat(exists).isFalse();

	verify(inventoryRepo).findOne(qinv.barcode.eq(nonExistingBarcode));
    }

    @Test
    public void savePhysicalCountTest() {
	GiftCardPhysicalCountDto giftCardPhysicalCountDto = new GiftCardPhysicalCountDto();

	sut.savePhysicalCount(giftCardPhysicalCountDto);

	verify(countRepo).save(any(GiftCardPhysicalCount.class));
    }

    @Test
    public void shouldReturnFalseWhenGCLocationIsNotEqualToUserLocationExceptHO() {
	final CustomSecurityUserDetailsImpl CURRENT_USER = new CustomSecurityUserDetailsImpl();
	CURRENT_USER.setUsername("JUNIT");
	CURRENT_USER.setInventoryLocation("20001");

	PowerMockito.mockStatic(UserUtil.class);
	PowerMockito.when(UserUtil.getCurrentUser()).thenReturn(CURRENT_USER);

	String startingSeries = "1404241010000003";
	String endingSeries = "1404241010000020";
	when(countRepo.getStoreCodeIn(startingSeries, endingSeries))
		.thenReturn(Sets.newHashSet("20001", "INVT001"));
	boolean result = sut.validateCardByUserInventoryLocation(startingSeries, endingSeries);

	assertThat(result).isFalse();

	verify(countRepo).getStoreCodeIn(startingSeries, endingSeries);
	PowerMockito.verifyStatic();
    }

    @Test
    public void shouldReturnTrueWhenUserLocationIsHOAndGCLocationHasAnyLocation() {
	final CustomSecurityUserDetailsImpl CURRENT_USER = new CustomSecurityUserDetailsImpl();
	CURRENT_USER.setUsername("JUNIT");
	CURRENT_USER.setInventoryLocation("INVT001");

	PowerMockito.mockStatic(UserUtil.class);
	PowerMockito.when(UserUtil.getCurrentUser()).thenReturn(CURRENT_USER);

	String startingSeries = "1404241010000003";
	String endingSeries = "1404241010000020";
	when(countRepo.getStoreCodeIn(startingSeries, endingSeries))
		.thenReturn(Sets.newHashSet("20001", "INVT001", "LEBAK"));
	boolean result = sut.validateCardByUserInventoryLocation(startingSeries, endingSeries);

	assertThat(result).isTrue();

	verifyZeroInteractions(countRepo);
	PowerMockito.verifyStatic();
    }

}
