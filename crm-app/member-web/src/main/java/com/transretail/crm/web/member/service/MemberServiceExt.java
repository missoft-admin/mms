package com.transretail.crm.web.member.service;

import java.io.InputStream;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import com.transretail.crm.common.reporting.jasper.JRProcessor;
import com.transretail.crm.common.service.dto.request.PageSortDto;
import com.transretail.crm.common.service.dto.response.ResultList;
import com.transretail.crm.common.service.exception.MessageSourceResolvableException;
import com.transretail.crm.common.util.SimpleEncrytor;
import com.transretail.crm.core.dto.EmployeePurchaseTxnDto;
import com.transretail.crm.core.dto.ShoppingListDto;
import com.transretail.crm.core.entity.MemberModel;
import com.transretail.crm.web.dto.PointResultList;
import com.transretail.crm.web.dto.PointSearchDto;
import com.transretail.crm.web.member.bean.ChangePasswordDto2;
import com.transretail.crm.web.member.bean.MemberDto;
import com.transretail.crm.web.member.bean.RegisterDto;

/**
 * @author Allan G. Ramirez (agramirez@exist.com)
 */
public interface MemberServiceExt {
    void save(RegisterDto registerDto, HttpServletRequest request) throws MessageSourceResolvableException;

    boolean validate(String validationCode);

    void sendChangePasswordValidation(String username, ChangePasswordDto2 form, HttpServletRequest request) throws
        MessageSourceResolvableException;

    boolean validateChangePassword(String username, String validationCode);

    MemberDto getMemberDto(String username);

    void updateMember(MemberDto dto);

    void updatePin(String username, String newPin) throws MessageSourceResolvableException, SimpleEncrytor.EncryptDecryptException;

    String getPin(String username);

    PointResultList getUserPoints(PointSearchDto dto);

    Long getTotalPoints(Long userId);

    void sendResetPasswordValidation(Date birthdate, String email, HttpServletRequest request) throws MessageSourceResolvableException;

    boolean validateResetPassword(String username, String validationCode);

    void updatePassword(String username, String newPassword);
    
    Boolean isSupplementary(Long userId);
    
    List<MemberModel> getSupplements(Long userId);

    boolean hasPin(String username);
    
    Double getTotalPurchaseAmountForCurrentMonth(Long userId);

	ResultList<EmployeePurchaseTxnDto> getPurchasesForCurrentMonth(Long userId,
			PageSortDto dto);
	
	ResultList<ShoppingListDto> getShoppingList(Long userId, PageSortDto pageSort);
	
	JRProcessor getShoppingListJRProcessor(Long userId, InputStream logo);
}
