package com.transretail.crm.core.service.impl;


import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyString;

import java.util.Date;
import java.util.List;
import java.util.Locale;

import junit.framework.Assert;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Configurable;
import org.springframework.context.MessageSource;
import org.springframework.security.authentication.TestingAuthenticationToken;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.context.SecurityContextImpl;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import com.google.common.collect.Lists;
import com.transretail.crm.common.service.dto.response.ResultList;
import com.transretail.crm.core.dto.ComplaintDto;
import com.transretail.crm.core.dto.ComplaintSearchDto;
import com.transretail.crm.core.dto.MessageDto;
import com.transretail.crm.core.dto.MessageDto.MsgType;
import com.transretail.crm.core.entity.Complaint.ComplaintPriority;
import com.transretail.crm.core.entity.UserModel;
import com.transretail.crm.core.entity.enums.Status;
import com.transretail.crm.core.entity.lookup.LookupDetail;
import com.transretail.crm.core.entity.lookup.LookupHeader;
import com.transretail.crm.core.entity.lookup.Store;
import com.transretail.crm.core.repo.ComplaintRepo;
import com.transretail.crm.core.repo.LookupDetailRepo;
import com.transretail.crm.core.repo.LookupHeaderRepo;
import com.transretail.crm.core.repo.StoreRepo;
import com.transretail.crm.core.repo.UserRepo;
import com.transretail.crm.core.security.model.impl.CustomSecurityUserDetailsImpl;
import com.transretail.crm.core.service.CodePropertiesService;
import com.transretail.crm.core.service.ComplaintService;


@Configurable
@Transactional
@ContextConfiguration(locations = "classpath*:/META-INF/spring/applicationContext*.xml")
@RunWith(SpringJUnit4ClassRunner.class)
public class ComplaintServiceTest {

	@Autowired
	ComplaintService service;
	@Autowired
	CodePropertiesService codePropertiesService;

	@Autowired
	ComplaintRepo repo;
	@Autowired
	UserRepo userRepo;
	@Autowired
	StoreRepo storeRepo;
	@Autowired
	LookupHeaderRepo lookupHeaderRepo;
	@Autowired
	LookupDetailRepo lookupDetailRepo;

	@Mock
	MessageSource messageSource;



	UserModel loggedInCsUser = null;
	UserModel otherCsUser = null;



	@Before
    public void setup() {
		MockitoAnnotations.initMocks(this);
		Object[] nullObject = null;
		Locale locale = any( Locale.class );
		try {
			Mockito.when( messageSource.getMessage( anyString(), Mockito.eq( nullObject ), locale ) ).thenReturn( "test" );
		}
		catch ( Exception ex ) {
			ex.printStackTrace();
		}
	}

    @After
    public void tearDown() {
        repo.deleteAll();
        userRepo.deleteAll();
        lookupDetailRepo.deleteAll();
        lookupHeaderRepo.deleteAll();
        storeRepo.deleteAll();
        SecurityContextHolder.clearContext();
    }

    @Test
    public void testSaveGetListDto() {
    	setupComplaintTest();

    	ComplaintDto dto = new ComplaintDto();
    	dto.setComplaints( "Test Complaints" );
    	dto.setDateFiled( new Date() );
    	dto.setEmail( "optimusprime031312@gmail.com" );
    	dto.setFirstName( "Test" );
    	dto.setGenderCode( codePropertiesService.getDetailGenderMale() );
    	dto.setPriority( ComplaintPriority.HIGH );
    	Assert.assertEquals( null, dto.getId() );

    	dto = service.saveDto( dto, true );
    	Assert.assertEquals( true, codePropertiesService.getComplaintStatNew().equalsIgnoreCase( dto.getStatus() ) );
    	Assert.assertEquals( true, null != service.getDto( dto.getId() ).getId() );

    	dto.setStatus( codePropertiesService.getComplaintStatAssigned() );
    	dto.setAssignedTo( loggedInCsUser.getUsername() );
    	dto = service.updateDto( dto );
    	Assert.assertEquals( true, dto.getAssignedTo().equalsIgnoreCase( loggedInCsUser.getUsername() ) );
    	Assert.assertEquals( true, dto.getStatus().equalsIgnoreCase( codePropertiesService.getComplaintStatAssigned() ) );

    	dto.setAssignedTo( otherCsUser.getUsername() );
    	dto = service.updateDto( dto );
    	Assert.assertEquals( true, dto.getStatus().equalsIgnoreCase( codePropertiesService.getComplaintStatReassigned() ) );

    	ComplaintDto otherDto = new ComplaintDto();
    	otherDto.setComplaints( "Test Complaints 2" );
    	otherDto.setDateFiled( new Date() );
    	otherDto.setEmail( "optimusprime031312@gmail.com" );
    	otherDto.setFirstName( "Test" );
    	otherDto.setGenderCode( codePropertiesService.getDetailGenderMale() );
    	otherDto.setPriority( ComplaintPriority.NORMAL );
    	service.saveDto( otherDto, true );

    	ResultList<ComplaintDto> resultDtos = service.search( new ComplaintSearchDto() );
    	Assert.assertEquals( 2, resultDtos.getNumberOfElements() );
    }

    @Test
    public void testProcessReply() {
    	setupComplaintTest();

    	ComplaintDto dto = new ComplaintDto();
    	dto.setComplaints( "Test Complaints" );
    	dto.setDateFiled( new Date() );
    	dto.setEmail( "optimusprime031312@gmail.com" );
    	dto.setFirstName( "Test" );
    	dto.setGenderCode( codePropertiesService.getDetailGenderMale() );
    	dto.setPriority( ComplaintPriority.HIGH );
    	dto.setStatus( codePropertiesService.getComplaintStatAssigned() );
    	dto.setAssignedTo( loggedInCsUser.getUsername() );
    	dto.setRemarks( "Replied" );

    	dto = service.saveDto( dto, true );
    	Assert.assertEquals( true, codePropertiesService.getComplaintStatAssigned().equalsIgnoreCase( dto.getStatus() ) );
    	MessageDto msgDto = new MessageDto();
    	msgDto.setMsgType( MsgType.email );
    	msgDto.setEmailAd( "test@mail.com" );
    	service.processReply( dto.getId(), msgDto);
    	dto = service.getDto( dto.getId() );
    	Assert.assertEquals( true, codePropertiesService.getComplaintStatInprogress().equalsIgnoreCase( dto.getStatus() ) );
    }



    private void setupComplaintTest() {
    	setupLookups();
    	setupCurrentUser();
	}

	private void setupLookups() {
		LookupHeader hd = new LookupHeader( "TEST", "TEST" );
		lookupHeaderRepo.saveAndFlush( hd );

		LookupDetail ld = new LookupDetail( codePropertiesService.getDetailGenderMale(), "Male", hd, Status.ACTIVE );
		lookupDetailRepo.saveAndFlush( ld );

		ld = new LookupDetail( codePropertiesService.getComplaintStatNew(), "New", hd, Status.ACTIVE );
		lookupDetailRepo.saveAndFlush( ld );
		ld = new LookupDetail( codePropertiesService.getComplaintStatAssigned(), "Assigned", hd, Status.ACTIVE );
		lookupDetailRepo.saveAndFlush( ld );
		ld = new LookupDetail( codePropertiesService.getComplaintStatReassigned(), "Reassigned", hd, Status.ACTIVE );
		lookupDetailRepo.saveAndFlush( ld );
		ld = new LookupDetail( codePropertiesService.getComplaintStatInprogress(), "In Progress", hd, Status.ACTIVE );
		lookupDetailRepo.saveAndFlush( ld );
		ld = new LookupDetail( codePropertiesService.getComplaintStatResolved(), "Resolved", hd, Status.ACTIVE );
		lookupDetailRepo.saveAndFlush( ld );
	}

	private void setupCurrentUser() {
		Store store = new Store( 1 );
		store.setCode( "store1" );
		storeRepo.saveAndFlush( store );

		loggedInCsUser = new UserModel();
    	loggedInCsUser.setUsername( "loggedInCssUser" );
    	loggedInCsUser.setPassword( "user" );
    	loggedInCsUser.setEnabled( true );
    	loggedInCsUser.setStoreCode( "store1" );
    	userRepo.saveAndFlush( loggedInCsUser );

		otherCsUser = new UserModel();
		otherCsUser.setUsername( "otherCsUser" );
		otherCsUser.setPassword( "user" );
		otherCsUser.setEnabled( true );
		otherCsUser.setStoreCode( "store1" );
    	userRepo.saveAndFlush( otherCsUser );
   
        CustomSecurityUserDetailsImpl user = new CustomSecurityUserDetailsImpl();
        user.setUserId( loggedInCsUser.getId() );
        user.setUsername( loggedInCsUser.getUsername() );
        List<GrantedAuthority> authorities = Lists.newArrayList();
        authorities.add( new SimpleGrantedAuthority( "ROLE_CS" ) );
        authorities.add( new SimpleGrantedAuthority( "ROLE_CSS" ) );
        user.setAuthorities( authorities );

        SecurityContextImpl securityContext = new SecurityContextImpl();
        securityContext.setAuthentication( new TestingAuthenticationToken( user, "" ) );
        SecurityContextHolder.setContext( securityContext );
    }

}
