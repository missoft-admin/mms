package com.transretail.crm.web.giftcard.controller;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.google.common.collect.Lists;
import java.util.List;

/**
 *
 * @author Monte Cillo Co (mco@exist.com)
 */
public class ErrorResponse {

    private int status;
    private String errorCode;
    private String errorMessage;
    @JsonInclude(Include.NON_NULL)
    private Object details;
    @JsonInclude(Include.NON_EMPTY)
    private List<FieldError> fieldErrors;

    public ErrorResponse() {
        fieldErrors = Lists.newArrayList();
    }

    public String getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(String errorCode) {
        this.errorCode = errorCode;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public Object getDetails() {
        return details;
    }

    public void setDetails(Object details) {
        this.details = details;
    }

    public List<FieldError> getFieldErrors() {
        return fieldErrors;
    }

    public void setFieldErrors(List<FieldError> fieldErrors) {
        this.fieldErrors = fieldErrors;
    }

    public ErrorResponse addFieldError(String fieldName, String errorMessage) {
        fieldErrors.add(new FieldError(fieldName, errorMessage));
        return this;
    }

    public static class FieldError {

        private String field;
        private String message;

        public FieldError(String field, String message) {
            this.field = field;
            this.message = message;
        }

        public String getField() {
            return field;
        }

        public String getMessage() {
            return message;
        }

    }
}
