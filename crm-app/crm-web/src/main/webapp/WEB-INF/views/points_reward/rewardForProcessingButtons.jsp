<%@ include file="../common/taglibs.jsp" %>

  <div id="rewardForProcessingButtons">

    <spring:message code="role_hr" var="role_hr"/>
    <spring:message code="role_hrs" var="role_hrs"/>

    <div class="contentButton">
      <%-- <sec:authorize ifAnyGranted="${role_hr}">
      <spring:message var="label_reward_generate" code="label_reward_generate"/>
      <button type="button" id="generateRewardButton" class="btn btn-primary pull-right" onclick="javascript: generateReward( '${pageContext.request.contextPath}' )">${label_reward_generate}</button>  
      </sec:authorize> --%>

      

    <script type="text/javascript">
    $(document).ready( function() {
    	  PrintUtil.init( '<spring:message code="points_lbl_emprewards" />', '<c:url value="/report/employee/rewards/report" />' );
    });
    </script>
<%--       <spring:message var="label_reward_print" code="label_reward_print"/> --%>
<!--       <div class="form-horizontal"> -->
<!--         <select name="reportType"class="input-small" id="reportType"> -->
<%--           <c:forEach items="${reportTypes}" var="reportType"> --%>
<%--             <option value="${reportType.code}"><spring:message code="report_type_${reportType.code}" /></option> --%>
<%--           </c:forEach> --%>
<!--         </select> -->
<!--         <input class="btn btn-default" id="printRewardsList" formtarget="_blank" type="button"  -->
<%--           onclick="javascript: showReport( '${dateFrom}' , '${dateTo}', '${pageContext.request.contextPath}' );" value="${label_reward_print}"/> --%>
<!--       </div> -->
    </div>
  </div>
  
  <br/>

