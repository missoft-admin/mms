package com.transretail.crm.web.controller;


import java.text.DateFormatSymbols;
import java.util.List;
import java.util.Locale;
import java.util.Set;
import java.util.concurrent.Callable;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.StringUtils;
import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.google.common.collect.Lists;
import com.transretail.crm.common.reporting.jasper.JRProcessor;
import com.transretail.crm.core.dto.DataAnalysisSearchDto;
import com.transretail.crm.core.dto.MemberGroupDto;
import com.transretail.crm.core.entity.MemberGroup;
import com.transretail.crm.core.entity.enums.DataAnalysis;
import com.transretail.crm.core.entity.enums.DataAnalysisLMHAnalysisBy;
import com.transretail.crm.core.entity.enums.DataAnalysisLMHBy;
import com.transretail.crm.core.entity.enums.DataAnalysisOverview;
import com.transretail.crm.core.entity.enums.ReportType;
import com.transretail.crm.core.service.DataAnalysisService;
import com.transretail.crm.core.service.MemberGroupService;
import com.transretail.crm.core.service.ProductService;
import com.transretail.crm.core.service.StoreService;
import com.transretail.crm.core.util.AppConstants;
import com.transretail.crm.marketing.dto.DecileReportDto;
import com.transretail.crm.marketing.dto.YearMonthDto;
import com.transretail.crm.marketing.service.DecileAnalysisService;
import com.transretail.crm.web.controller.util.ControllerResponse;


@RequestMapping("/dataanalysis")
@Controller
public class DataAnalysisController extends AbstractReportsController {

    @Autowired
    DataAnalysisService dataAnalysisService;
    @Autowired
    MemberGroupService memberGroupService;
    @Autowired
    StoreService storeService;
    @Autowired
    ProductService productService;
    @Autowired
    MessageSource messageSource;
    @Autowired
    private DecileAnalysisService decileAnalysisService;


    @RequestMapping(produces = "text/html")
    public String show(Model uiModel) {
        uiModel.addAttribute("searchDto", new DataAnalysisSearchDto());
        populateForm(uiModel);
        return "dataanalysis/show";
    }

    @RequestMapping(value = "/generategroup", method = RequestMethod.POST, produces = "application/json; charset=utf-8")
    @ResponseBody
    public ControllerResponse generateGroup(@ModelAttribute(value = "groupForm") MemberGroupDto dto,
        BindingResult result, HttpServletRequest request) {
        ControllerResponse response = new ControllerResponse();
        response.setSuccess(true);

        if (result.hasErrors()) {
            response.setSuccess(false);
            response.setResult(result.getAllErrors());
            return response;
        }

        if (response.isSuccess()) {
            MemberGroup memberGroup = new MemberGroup();
            memberGroup.setName(dto.getName());
            memberGroup.setMembers(toString(dto.getMembers()));
            memberGroupService.saveMemberGroup(memberGroup);
        }
        return response;
    }

    private String toString(Set<String> strs) {
    	strs.remove("");
		return strs.toString().replaceAll(" ", "").replaceAll("\\[", "").replaceAll("\\]", "");
    }

    @RequestMapping(value = "/show", produces = "text/html", method = RequestMethod.POST)
    public String show(@ModelAttribute DataAnalysisSearchDto searchDto, Model uiModel,
        BindingResult result, HttpServletRequest request) {
    	uiModel.addAttribute("searchDto", searchDto);

        validateSearch(result, searchDto);
        if (result.hasErrors()) {
            uiModel.addAttribute("errorMessages", result.getAllErrors());
            return "dataanalysis/messages";
        }

        if (DataAnalysis.OVERVIEW.getCode().equalsIgnoreCase(searchDto.getType())) {
            uiModel.addAttribute("analysisDto", dataAnalysisService.getOverviewAnalysis(searchDto));
            uiModel.addAttribute("currentYear", new DateTime().year().getAsText());
            uiModel.addAttribute("previousYear", new DateTime().minusYears(1).year().getAsText());

            if (DataAnalysisOverview.SALES_VALUE.getCode().equalsIgnoreCase(searchDto.getOverview())
                || DataAnalysisOverview.AVG_VISIT_FREQ.getCode().equalsIgnoreCase(searchDto.getOverview())) {
                uiModel.addAttribute("decimalFormat", "#,##0.00");
            } else {
                uiModel.addAttribute("decimalFormat", "#,##0");
            }

            return "dataanalysis/overview";
        } else if (DataAnalysis.LMH.getCode().equalsIgnoreCase(searchDto.getType())) {
            uiModel.addAttribute("analysisDto", dataAnalysisService.getLMHAnalysis(searchDto));
            if (DataAnalysisLMHAnalysisBy.SALES_VALUE.getCode().equalsIgnoreCase(searchDto.getAnalysisBy())
                || DataAnalysisLMHAnalysisBy.AVG_VISIT_FREQ.getCode().equalsIgnoreCase(searchDto.getAnalysisBy())) {
                uiModel.addAttribute("decimalFormat", "#,##0.00");
            } else {
                uiModel.addAttribute("decimalFormat", "#,##0");
            }

            return "dataanalysis/lmh";
        } else if (DataAnalysis.DECILE.getCode().equalsIgnoreCase(searchDto.getType())) {
            YearMonthDto yearMonthDto =
                new YearMonthDto(searchDto.getDecileYearFrom(), searchDto.getDecileMonthFrom(), searchDto.getDecileYearTo(),
                    searchDto.getDecileMonthTo());
            List<DecileReportDto> results = decileAnalysisService.getDecileReport(yearMonthDto);
            uiModel.addAttribute("decileReportDto", results);
            uiModel.addAttribute("decimalFormat", "#,#00.00#######");
            uiModel.addAttribute("months", new DateFormatSymbols(LocaleContextHolder.getLocale()).getMonths());
            return "dataanalysis/decile";
        }

        return "dataanalysis/show";
    }


    private void validateSearch(BindingResult result, DataAnalysisSearchDto searchDto) {
        if (DataAnalysis.LMH.getCode().equalsIgnoreCase(searchDto.getType())) {
            if ((searchDto.getYearFrom().equals(searchDto.getYearTo().intValue()) && searchDto.getMonthFrom() > searchDto.getMonthTo())
                || searchDto.getYearFrom() > searchDto.getYearTo()) {
                result.reject("data_analysis_err_invaliddate");
            }
        }
    }

    @ResponseBody
    @RequestMapping(value = "/decile/validate", method = RequestMethod.POST)
    public List<String> validateDecileParams(@RequestBody DataAnalysisSearchDto dto) {
        Locale locale = LocaleContextHolder.getLocale();
        List<String> result = Lists.newArrayList();
        if (StringUtils.isBlank(dto.getDecileYearFrom())) {
            result.add(messageSource.getMessage("required.from.year", null, locale));
        } else if (!AppConstants.REGEX_YEAR.matcher(dto.getDecileYearFrom()).matches()) {
            result.add(messageSource.getMessage("invalid.from.year", null, locale));
        }

        if (StringUtils.isBlank(dto.getDecileYearTo())) {
            result.add(messageSource.getMessage("required.to.year", null, locale));
        } else if (!AppConstants.REGEX_YEAR.matcher(dto.getDecileYearTo()).matches()) {
            result.add(messageSource.getMessage("invalid.to.year", null, locale));
        }

        if (StringUtils.isBlank(dto.getDecileMonthFrom())) {
            result.add(messageSource.getMessage("required.from.month", null, locale));
        }

        if (StringUtils.isBlank(dto.getDecileMonthTo())) {
            result.add(messageSource.getMessage("required.to.month", null, locale));
        }

        return result;
    }

    @RequestMapping(value = "/print/{reportType}")
    public void print(@ModelAttribute DataAnalysisSearchDto searchDto,
        @PathVariable String reportType,
        HttpServletResponse response) throws Exception {

        JRProcessor jrProcessor = null;
        if (DataAnalysis.OVERVIEW.getCode().equalsIgnoreCase(searchDto.getType())) {
            jrProcessor = dataAnalysisService.createOverviewJrProcessor(searchDto);
        } else if (DataAnalysis.LMH.getCode().equalsIgnoreCase(searchDto.getType())) {
            jrProcessor = dataAnalysisService.createLMHJrProcessor(searchDto);
        }
        if (ReportType.PDF.getCode().equals(reportType))
            renderReport(response, jrProcessor);
        else if (ReportType.EXCEL.getCode().equals(reportType))
            renderExcelReport(response, jrProcessor, "test");
    }

    private void populateForm(Model uiModel) {
        uiModel.addAttribute("types", DataAnalysis.values());
        uiModel.addAttribute("overviewTypes", DataAnalysisOverview.values());
        uiModel.addAttribute("lmhTypes", DataAnalysisLMHBy.values());
        uiModel.addAttribute("lmhAnalysisTypes", DataAnalysisLMHAnalysisBy.values());
        uiModel.addAttribute("years", dataAnalysisService.getYears());
        uiModel.addAttribute("months", new DateFormatSymbols(LocaleContextHolder.getLocale()).getMonths());
        uiModel.addAttribute("stores", storeService.getAllStores());
        /*uiModel.addAttribute("products", productService.getAllProducts());*/
        uiModel.addAttribute("reportTypes", ReportType.values());
    }


}
