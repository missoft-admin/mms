<%@ include file="taglibs.jsp" %>
<c:if test="${not empty successMessages}">
  <div class="alert alert-info">
    <%--<button type="button" class="close" data-dismiss="alert">&times;</button>--%>
    <button type="button" class="close" onclick="$(this).parent().hide();">&times;</button>
    <c:out value="${successMessages}" escapeXml="false"/>
  </div>
</c:if>
<c:if test="${not empty errorMessages}">
  <div class="alert alert-error">
    <%--<button type="button" class="close" data-dismiss="alert">&times;</button>--%>  
    <button type="button" class="close" onclick="$(this).parent().hide();">&times;</button>
    <c:out value="${errorMessages}" escapeXml="false"/>
  </div>
</c:if>