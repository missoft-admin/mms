package com.transretail.crm.common.web.converter;

import java.io.IOException;

import org.apache.commons.lang3.StringUtils;
import org.joda.time.LocalDate;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;

/**
 *
 */
public class LocalDateDeserializer extends JsonDeserializer<LocalDate> {
	public static final String DATETIME_FORMAT = "dd-MM-yyyy";
    private static final DateTimeFormatter FORMATTER = DateTimeFormat.forPattern(DATETIME_FORMAT);

    @Override
    public LocalDate deserialize(JsonParser jp, DeserializationContext ctxt) throws IOException {
        if (StringUtils.isNotBlank(jp.getText())) {
            return FORMATTER.parseLocalDate(jp.getText());
        } else {
            return null;
        }
    }
}
