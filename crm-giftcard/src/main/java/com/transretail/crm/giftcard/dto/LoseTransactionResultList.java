package com.transretail.crm.giftcard.dto;

import com.transretail.crm.common.service.dto.response.AbstractResultListDTO;
import java.util.Collection;

/**
 *
 * @author Monte Cillo Co (mco@exist.com)
 */
public class LoseTransactionResultList extends AbstractResultListDTO<PosLoseTxForm> {

    public LoseTransactionResultList(Collection<PosLoseTxForm> results, long totalElements, int pageNo, int pageSize) {
	super(results, totalElements, pageNo, pageSize);
    }
}
