package com.transretail.crm.giftcard.entity.support;




public enum GiftCardSalesType {

    B2B, B2C, INTERNAL, REBATE;
    
    public static GiftCardSalesType getSalesType(SalesOrderType orderType) {
    	switch(orderType) {
	    	case B2B_SALES: return B2B;
	    	case B2B_ADV_SALES: return B2B;
	    	case REPLACEMENT: return B2B;
	
	    	case INTERNAL_ORDER: return INTERNAL;
	    		
	    	case YEARLY_DISCOUNT: return REBATE;
	    	case VOUCHER: return REBATE;
    	}
    	return null;
    }
    
    public SalesOrderType[] getOrderTypes() {
    	switch(this) {
    	case B2B: return new SalesOrderType[]{SalesOrderType.B2B_SALES, SalesOrderType.REPLACEMENT, SalesOrderType.B2B_ADV_SALES};
    	case INTERNAL: return new SalesOrderType[]{SalesOrderType.INTERNAL_ORDER};
    	case REBATE: return new SalesOrderType[]{SalesOrderType.YEARLY_DISCOUNT, SalesOrderType.VOUCHER};
    	}
    	return null;
    }
}
