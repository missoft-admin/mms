package com.transretail.crm.giftcard.entity;


import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.Type;
import org.joda.time.LocalDate;
import org.joda.time.LocalDateTime;

import com.mysema.query.annotations.QueryInit;
import com.transretail.crm.core.entity.support.CustomAuditableEntity;


@Entity
@Table(name = "CRM_GC_INVENTORY_STOCK")
public class GiftCardInventoryStock extends CustomAuditableEntity<Long> implements Serializable {

    private static final long serialVersionUID = 1L;
	public static enum GCIStockStatus { RECEIVED, ASSIGNED, TRANSFERRED_OUT, TRANSFERRED_IN, ACTIVATED, VOID_ACTIVATION, RETURNED, BURNED, 
		PHYSCOUNT, PHYSCOUNT_MISSING, PHYSCOUNT_FOUND };
	
	@Column(name = "CREATED_DATE")
    @Type(type = "org.jadira.usertype.dateandtime.joda.PersistentLocalDate")
    private LocalDate createdDate;

	@Column(name = "COMPILED_TIMESTAMP")
    @Type(type = "org.jadira.usertype.dateandtime.joda.PersistentLocalDateTime")
    private LocalDateTime compiledTimestamp;

	@ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "INVENTORY_NO", nullable = true)
	@QueryInit({"profile.*", "status.*"})
	private GiftCardInventory inventory;

    @Column(name = "TXN_STATUS")
    private String status;

    @Column(name = "TRANSFER_FROM")
	private String transferFrom;

    @Column(name = "TRANSFER_TO")
   	private String transferTo;

    @Column(name = "LOCATION")
    private String location;

    @Column(name = "SERIES_START")
    private Long seriesStart;

    @Column(name = "SERIES_END")
    private Long seriesEnd;

    @Column(name = "QUANTITY")
    private Long quantity;

    @Column(name = "CARD_TYPE")
    private String cardType;



	public LocalDate getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(LocalDate createdDate) {
		this.createdDate = createdDate;
	}

	public LocalDateTime getCompiledTimestamp() {
		return compiledTimestamp;
	}

	public void setCompiledTimestamp(LocalDateTime compiledTimestamp) {
		this.compiledTimestamp = compiledTimestamp;
	}

	public GiftCardInventory getInventory() {
		return inventory;
	}

	public void setInventory(GiftCardInventory inventory) {
		this.inventory = inventory;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getTransferFrom() {
		return transferFrom;
	}

	public void setTransferFrom(String transferFrom) {
		this.transferFrom = transferFrom;
	}

	public String getTransferTo() {
		return transferTo;
	}

	public void setTransferTo(String transferTo) {
		this.transferTo = transferTo;
	}

	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		this.location = location;
	}

	public Long getSeriesStart() {
		return seriesStart;
	}

	public void setSeriesStart(Long seriesStart) {
		this.seriesStart = seriesStart;
	}

	public Long getSeriesEnd() {
		return seriesEnd;
	}

	public void setSeriesEnd(Long seriesEnd) {
		this.seriesEnd = seriesEnd;
	}

	public Long getQuantity() {
		return quantity;
	}

	public void setQuantity(Long quantity) {
		this.quantity = quantity;
	}

	public String getCardType() {
		return cardType;
	}

	public void setCardType(String cardType) {
		this.cardType = cardType;
	}

}
