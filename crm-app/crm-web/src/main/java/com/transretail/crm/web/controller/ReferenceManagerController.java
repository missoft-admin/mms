package com.transretail.crm.web.controller;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.transretail.crm.common.enums.MessageType;
import com.transretail.crm.common.service.dto.response.CodeDescDto;
import com.transretail.crm.common.service.exception.MessageSourceResolvableException;
import com.transretail.crm.core.dto.LookupDetailResultList;
import com.transretail.crm.core.dto.LookupDetailSearchDto;
import com.transretail.crm.core.entity.enums.Status;
import com.transretail.crm.core.entity.lookup.LookupDetail;
import com.transretail.crm.core.entity.lookup.LookupHeader;
import com.transretail.crm.core.service.CodePropertiesService;
import com.transretail.crm.core.service.LookupService;
import com.transretail.crm.core.util.AppConstants;
import com.transretail.crm.web.controller.util.ControllerResponse;

@RequestMapping("/lookup")
@Controller
public class ReferenceManagerController {

	private static final String UI_ATTR_HEADERS = "headers";
	private static final String UI_ATTR_DETAIL = "detail";
	private static final String UI_ATTR_STATUS = "status";
	@Autowired
	private LookupService lookupService;
	@Autowired
	private MessageSource messageSource;
	@Autowired
	private CodePropertiesService codePropertiesService;

	@RequestMapping(value = "/list/{header}")
	public @ResponseBody LookupDetailResultList list(
			@PathVariable("header") String header,
			@RequestBody(required = false) LookupDetailSearchDto searchDto) {
		if (searchDto == null)
			searchDto = new LookupDetailSearchDto();
		searchDto.setHeaderCode(header);
		return lookupService.getLookupDetails(searchDto);
	}

	@RequestMapping(params = { "page", "size" }, produces = "text/html")
	public String listHeaders(
			@RequestParam(value = "page", required = false) Integer page,
			@RequestParam(value = "size", required = false) Integer size,
			Model uiModel) {

		String[] ignore = new String[] { codePropertiesService.getHeaderMemberType() };

		uiModel.addAttribute(UI_ATTR_DETAIL, new LookupDetail());
		final List<LookupHeader> headers = lookupService.getAllHeaders(ignore);
		Collections.sort(headers, new Comparator<LookupHeader>() {

			@Override
			public int compare(LookupHeader o1, LookupHeader o2) {
				if (o1.getCode().startsWith(AppConstants.HEADER_CUSTMEMBERFLD_PFX)
						&& o2.getCode().startsWith(AppConstants.HEADER_CUSTMEMBERFLD_PFX)) {
					return o1.getCode().compareTo(o2.getCode());
				} else if (o1.getCode().startsWith(AppConstants.HEADER_CUSTMEMBERFLD_PFX)) {
					return -1;
				} else if (o2.getCode().startsWith(AppConstants.HEADER_CUSTMEMBERFLD_PFX)) {
					return 1;
				} else {
					return o1.getDescription().compareTo(o2.getDescription());
				}
			}

		});
		uiModel.addAttribute(UI_ATTR_HEADERS, headers);
		uiModel.addAttribute(UI_ATTR_STATUS, Status.values());

		return "lookup/header";
	}

	@RequestMapping(value = "/{header}", method = RequestMethod.POST, produces = "application/json; charset=utf-8")
	@ResponseBody
	public ControllerResponse createDetail(@ModelAttribute LookupDetail detail,
			@PathVariable("header") String header,
			BindingResult bindingResult) {

		ControllerResponse theResponse = new ControllerResponse();
		theResponse.setSuccess(true);

		validateDetail(detail, bindingResult, true);

		if (bindingResult.hasErrors()) {
			theResponse.setSuccess(false);
			theResponse.setResult(bindingResult.getAllErrors());
			return theResponse;
		}

		if (theResponse.isSuccess()) {
			lookupService.saveDetail(detail);
		}

		return theResponse;
	}

	private void validateDetail(LookupDetail detail, BindingResult bindingResult, boolean isCreate) {
		if (StringUtils.isBlank(detail.getCode())) {
			bindingResult.reject(getMessage("propkey_msg_notempty", new Object[] { "propkey_lookup_detail_code" }));
		} else {
			detail.setCode(detail.getCode().toUpperCase());
			if (isCreate) {
				if (lookupService.isDuplicateDetail(detail.getCode())) {
					bindingResult.reject(getMessage("propkey_msg_duplicate", new Object[] { "propkey_lookup_detail_code" }));
				}

				if (codePropertiesService.getHeaderCompany().equals(detail.getHeader().getCode()) && !lookupService.isValidCompanyDetailCode(detail.getCode())) {
					bindingResult.reject(getMessage("propkey_companycode_invalid", null));
				}

				if (codePropertiesService.getHeaderCardTypes().equals(detail.getHeader().getCode()) && !lookupService.isValidCardTypeDetailCode(detail.getCode())) {
					bindingResult.reject(getMessage("propkey_cardtypecode_invalid", null));
				}
			}
		}

		if (StringUtils.isBlank(detail.getDescription())) {
			bindingResult.reject(getMessage("propkey_msg_notempty", new Object[] { "propkey_lookup_detail_description" }));
		} else if (lookupService.hasDuplicateDetailDescription(detail.getCode(), detail.getDescription(), detail.getHeader())) {
			bindingResult.reject(getMessage("propkey_msg_duplicate", new Object[] { "propkey_lookup_detail_description" }));
		}
	}

	@RequestMapping(value = "/update/{header}", method = RequestMethod.POST, produces = "application/json; charset=utf-8")
	@ResponseBody
	public ControllerResponse updateDetail(@ModelAttribute LookupDetail detail,
			@PathVariable("header") String header,
			BindingResult bindingResult) {

		ControllerResponse theResponse = new ControllerResponse();
		theResponse.setSuccess(true);

		validateDetail(detail, bindingResult, false);

		if (bindingResult.hasErrors()) {
			theResponse.setSuccess(false);
			theResponse.setResult(bindingResult.getAllErrors());
			return theResponse;
		}

		if (theResponse.isSuccess()) {
			lookupService.saveDetail(detail);
		}

		return theResponse;
	}

	@RequestMapping(value = "/header/update", method = RequestMethod.POST)
	@ResponseBody
	public ControllerResponse updateHeaderDesc(@RequestBody CodeDescDto codeDescDto) {
		ControllerResponse theResponse = new ControllerResponse();
		if (StringUtils.isBlank(codeDescDto.getDesc())) {
			theResponse.setSuccess(false);
			theResponse.setMessage(MessageType.ERROR);
			theResponse.setResult(messageSource.getMessage("description.required", null, LocaleContextHolder.getLocale()));
		} else {
			try {
				lookupService.updateHeader(codeDescDto);
				theResponse.setSuccess(true);
			} catch (MessageSourceResolvableException e) {
				theResponse.setSuccess(false);
				theResponse.setMessage(MessageType.ERROR);
				theResponse.setResult(messageSource.getMessage(e, LocaleContextHolder.getLocale()));
			}
		}
		return theResponse;
	}

	/**
	 * Get lookup details by codes.
	 *
	 * @param codes comma separated {@link com.transretail.crm.core.entity.lookup.LookupDetail#code}
	 * @return lookup details by codes.
	 */
	@RequestMapping(value = "/detail/{codes}", method = RequestMethod.GET)
	@ResponseBody
	public List<LookupDetail> getLookupDetails(@PathVariable("codes") String codes) {
		return lookupService.getLookupDetails(codes);
	}

	private String getMessage(String inMsgCode, Object[] inArgMsgCodes) {
		if (inArgMsgCodes != null) {
			for (int i = 0; i < inArgMsgCodes.length; i++) {
				inArgMsgCodes[i] = messageSource.getMessage(inArgMsgCodes[i].toString().toLowerCase(), null, LocaleContextHolder.getLocale());
			}
		}
		return messageSource.getMessage(inMsgCode, inArgMsgCodes, "Info given is invalid.", LocaleContextHolder.getLocale());
	}

}
