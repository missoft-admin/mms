/*
 * Copyright (c) 2014. Medcurial, Inc.
 * All rights reserved.
 */
package com.transretail.crm.giftcard.dto;

import com.transretail.crm.common.service.dto.response.AbstractResultListDTO;

import java.util.Collection;

/**
 * description here.
 *
 * @author Vincent Gerard Tan
 */
public class PrepaidCardResultList extends AbstractResultListDTO<PrepaidCardDto> {
    public PrepaidCardResultList(Collection<PrepaidCardDto> results) {
        super(results);
    }

    public PrepaidCardResultList(Collection<PrepaidCardDto> results, long totalElements, boolean hasPreviousPage, boolean hasNextPage) {
        super(results, totalElements, hasPreviousPage, hasNextPage);
    }

    public PrepaidCardResultList(Collection<PrepaidCardDto> results, long totalElements, int pageNo, int pageSize) {
        super(results, totalElements, pageNo, pageSize);
    }
}
