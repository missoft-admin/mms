package com.transretail.crm.core.entity;

import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;

import org.junit.Assert;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Configurable;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import com.transretail.crm.core.repo.UserRoleRepo;
import com.transretail.crm.core.service.UserRoleModelService;

@Transactional
@ContextConfiguration(locations = "classpath*:/META-INF/spring/applicationContext*.xml")
@RunWith(SpringJUnit4ClassRunner.class)
@Configurable
public class UserRoleModelIntegrationTest {

    @Autowired
    UserRoleModelDataOnDemand dod;
    @Autowired
    UserRoleModelService userRoleModelService;
    @Autowired
    UserRoleRepo userRoleRepo;

    @Test
    public void testMarkerMethod() {
    }

    @Test
    @Ignore
    public void testDeleteUserRoleModel() {
        UserRoleModel obj = dod.getRandomUserRoleModel();
        Assert.assertNotNull("Data on demand for 'UserRoleModel' failed to initialize correctly", obj);
        String id = obj.getId();
        Assert.assertNotNull("Data on demand for 'UserRoleModel' failed to provide an identifier", id);
        obj = userRoleModelService.findUserRoleModel(id);
        userRoleModelService.deleteUserRoleModel(obj);
        userRoleRepo.flush();
        Assert.assertNull("Failed to remove 'UserRoleModel' with identifier '" + id + "'", userRoleModelService.findUserRoleModel(id));
    }

    @Test
    @Ignore("Fix this since user id is now a string not long")
    public void testSaveUserRoleModel() {
        Assert.assertNotNull("Data on demand for 'UserRoleModel' failed to initialize correctly", dod.getRandomUserRoleModel());
        UserRoleModel obj = dod.getNewTransientUserRoleModel(Integer.MAX_VALUE);
        Assert.assertNotNull("Data on demand for 'UserRoleModel' failed to provide a new transient entity", obj);
        Assert.assertNull("Expected 'UserRoleModel' identifier to be null", obj.getId());
        try {
            userRoleModelService.saveUserRoleModel(obj);
        } catch (final ConstraintViolationException e) {
            final StringBuilder msg = new StringBuilder();
            for (Iterator<ConstraintViolation<?>> iter = e.getConstraintViolations().iterator(); iter.hasNext(); ) {
                final ConstraintViolation<?> cv = iter.next();
                msg.append("[").append(cv.getRootBean().getClass().getName()).append(".").append(cv.getPropertyPath()).append(": ").append(cv.getMessage()).append(" (invalid value = ").append(cv.getInvalidValue()).append(")").append("]");
            }
            throw new IllegalStateException(msg.toString(), e);
        }
        userRoleRepo.flush();
        Assert.assertNotNull("Expected 'UserRoleModel' identifier to no longer be null", obj.getId());
    }

    @Test
    public void testUpdateUserRoleModelUpdate() {
        UserRoleModel obj = dod.getRandomUserRoleModel();
        Assert.assertNotNull("Data on demand for 'UserRoleModel' failed to initialize correctly", obj);
        String id = obj.getId();
        Assert.assertNotNull("Data on demand for 'UserRoleModel' failed to provide an identifier", id);
        obj = userRoleModelService.findUserRoleModel(id);
        boolean modified = dod.modifyUserRoleModel(obj);
        Integer currentVersion = obj.getVersion();
        UserRoleModel merged = userRoleModelService.updateUserRoleModel(obj);
        userRoleRepo.flush();
        Assert.assertEquals("Identifier of merged object not the same as identifier of original object", merged.getId(), id);
        Assert.assertTrue("Version for 'UserRoleModel' failed to increment on merge and flush directive", (currentVersion != null && obj.getVersion() > currentVersion) || !modified);
    }

    @Test
    public void testFlush() {
        UserRoleModel obj = dod.getRandomUserRoleModel();
        Assert.assertNotNull("Data on demand for 'UserRoleModel' failed to initialize correctly", obj);
        String id = obj.getId();
        Assert.assertNotNull("Data on demand for 'UserRoleModel' failed to provide an identifier", id);
        obj = userRoleModelService.findUserRoleModel(id);
        Assert.assertNotNull("Find method for 'UserRoleModel' illegally returned null for id '" + id + "'", obj);
        boolean modified = dod.modifyUserRoleModel(obj);
        Integer currentVersion = obj.getVersion();
        userRoleRepo.flush();
        Assert.assertTrue("Version for 'UserRoleModel' failed to increment on flush directive", (currentVersion != null && obj.getVersion() > currentVersion) || !modified);
    }

    @Test
    public void testFindUserRoleModelEntries() {
        Assert.assertNotNull("Data on demand for 'UserRoleModel' failed to initialize correctly", dod.getRandomUserRoleModel());
        long count = userRoleModelService.countAllUserRoleModels();
        if (count > 20) count = 20;
        int firstResult = 0;
        int maxResults = (int) count;
        List<UserRoleModel> result = userRoleModelService.findUserRoleModelEntries(firstResult, maxResults);
        Assert.assertNotNull("Find entries method for 'UserRoleModel' illegally returned null", result);
        Assert.assertEquals("Find entries method for 'UserRoleModel' returned an incorrect number of entries", count, result.size());
    }

    @Test
    public void testFindAllUserRoleModels() {
        Assert.assertNotNull("Data on demand for 'UserRoleModel' failed to initialize correctly", dod.getRandomUserRoleModel());
        long count = userRoleModelService.countAllUserRoleModels();
        Assert.assertTrue("Too expensive to perform a find all test for 'UserRoleModel', as there are " + count + " entries; set the findAllMaximum to exceed this value or set findAll=false on the integration test annotation to disable the test", count < 250);
        List<UserRoleModel> result = userRoleModelService.findAllUserRoleModels();
        Assert.assertNotNull("Find all method for 'UserRoleModel' illegally returned null", result);
        Assert.assertTrue("Find all method for 'UserRoleModel' failed to return any data", result.size() > 0);
    }

    @Test
    public void testFindUserRoleModel() {
        UserRoleModel obj = dod.getRandomUserRoleModel();
        Assert.assertNotNull("Data on demand for 'UserRoleModel' failed to initialize correctly", obj);
        String id = obj.getId();
        Assert.assertNotNull("Data on demand for 'UserRoleModel' failed to provide an identifier", id);
        obj = userRoleModelService.findUserRoleModel(id);
        Assert.assertNotNull("Find method for 'UserRoleModel' illegally returned null for id '" + id + "'", obj);
        Assert.assertEquals("Find method for 'UserRoleModel' returned the incorrect identifier", id, obj.getId());
    }

    @Test
    public void testCountAllUserRoleModels() {
        Assert.assertNotNull("Data on demand for 'UserRoleModel' failed to initialize correctly", dod.getRandomUserRoleModel());
        long count = userRoleModelService.countAllUserRoleModels();
        Assert.assertTrue("Counter for 'UserRoleModel' incorrectly reported there were no entries", count > 0);
    }
    
    @Test
    public void testFindByIdSet() {
    	int expectedSize = 5;
    	String[] list = new String[expectedSize];
    	
    	for(int i = 0; i < expectedSize; i++) {
    		UserRoleModel userRole = dod.getSpecificUserRoleModel(i);
    		Assert.assertNotNull("Data on demand for 'UserRoleModel' failed to initialize correctly", userRole);
            String id = userRole.getId();
    		Assert.assertNotNull("Data on demand for 'UserRoleModel' failed to provide an identifier", id);
    		list[i] = id.toString();
    	}
    	
    	Arrays.sort(list);
    	Set<UserRoleModel> userRoles = userRoleModelService.findByIdSet(list);
    	Assert.assertEquals("Incorrect number of results returned", expectedSize, userRoles.size());
    	
    	for(UserRoleModel userRole : userRoles) {
    		int result = Arrays.binarySearch(list, userRole.getId().toString());
    		Assert.assertTrue("Incorrect result found",result > -1);
    	}
    }
}
