package com.transretail.crm.core.dto;

import com.mysema.query.types.expr.BooleanExpression;
import com.transretail.crm.common.service.dto.request.AbstractSearchFormDto;

/**
 *
 */
public class CityProvinceSearchDto extends AbstractSearchFormDto {
	private Long provinceId;
    private String province;
    private String city;
    
    @Override
    public BooleanExpression createSearchExpression() {
        return null;
    }

	public String getProvince() {
		return province;
	}

	public void setProvince(String province) {
		this.province = province;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public Long getProvinceId() {
		return provinceId;
	}

	public void setProvinceId(Long provinceId) {
		this.provinceId = provinceId;
	}
	
}

