package com.transretail.crm.core.entity.enums;

public enum PointsSearchCriteria {

	POSTINGDATE ( "Posting Date", "postingDate" ),
	CONTACT ( "Transaction Date", "transactionDate" );

    private PointsSearchCriteria(String inDesc, String inValue){
        this.desc = inDesc;
        this.value = inValue;
    }

    private final String desc;
    private final String value;
    public String getDescription() {
    	return desc;
    }
    public String getValue() {
    	return value;
    }
}
