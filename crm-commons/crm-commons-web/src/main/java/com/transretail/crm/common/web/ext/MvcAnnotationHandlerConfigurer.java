package com.transretail.crm.common.web.ext;

import org.apache.commons.lang3.Validate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.support.DefaultSessionAttributeStore;
import org.springframework.web.bind.support.SessionAttributeStore;
import org.springframework.web.servlet.mvc.method.annotation.RequestMappingHandlerAdapter;

/**
 * Based on the solution provided here: http://scottfrederick.blogspot.com/2011/03/customizing-spring-3-mvcannotation.html
 *
 * @author aco
 */
public class MvcAnnotationHandlerConfigurer implements InitializingBean {

    private static final Logger _LOG = LoggerFactory.getLogger(MvcAnnotationHandlerConfigurer.class);

    /**
     * Annotation handler that needs to be post configured. Depends on spring version:
     * -> 3.0 - AnnotationMethodHandlerAdapter
     * -> 3.1 - RequestMappingHandlerAdapter
     */
    @Autowired
    private RequestMappingHandlerAdapter mvcAnnotationHandler;

    private SessionAttributeStore sessionAttributeStore = new DefaultSessionAttributeStore();

    @Override
    public void afterPropertiesSet() throws Exception {
        Validate.notNull(mvcAnnotationHandler, "mvcAnnotationHandler cannot be null.");

        _LOG.info("Post configuration of mvc annotation handler: {}", mvcAnnotationHandler);
        _LOG.info("  * sessionAttributeStore={}", sessionAttributeStore);
        this.mvcAnnotationHandler.setSessionAttributeStore(sessionAttributeStore);
    }

    public void setSessionAttributeStore(SessionAttributeStore sessionAttributeStore) {
        this.sessionAttributeStore = sessionAttributeStore;
    }
}
