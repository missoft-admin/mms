<%@ include file="../../common/taglibs.jsp" %>

<style type="text/css">
  <!--

  #productTable tr td:last-child {
    border-left: 0px;
  }

  table select, textarea, table input[type="text"], input[type="password"], input[type="datetime"], input[type="datetime-local"], input[type="date"], input[type="month"], input[type="time"], input[type="week"], input[type="number"], input[type="email"], input[type="url"], input[type="search"], input[type="tel"], input[type="color"], .uneditable-input {
    margin-bottom: 0px;
    margin-top: 0px;
  }
  -->
</style>
<c:set var="ENCTYPE" value="application/x-www-form-urlencoded"/>
<div class="modal-dialog">
  <div class="modal-content">
    <div class="modal-header">
      <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
      <h4 class="modal-title"><spring:message code="gc.poslose.view"/></h4>
    </div>
    <div class="modal-body">
      <div id="contentError" class="hide alert alert-error">
        <button type="button" class="close" onclick="$(this).parent().hide();">&times;</button>
        <div><form:errors path="*"/></div>
      </div>
      <form:form id="posLoseTxForm" name="posLoseTxForm" modelAttribute="posLoseTxForm" 
		 action="${pageContext.request.contextPath}/giftcard/pos-lose-tx/save" 
		 method="POST" enctype="${ENCTYPE}" cssClass="form-reset form-horizontal" >
	<div class="contentMain">
	  <div class="row-fluid">
	    <div class="span6 pull-left">
	      <div class="control-group">
		<spring:message code="gc.poslose.label.store" var="merchantLabel" />
		<label for="merchantId" class="control-label">${merchantLabel}</label> 
		<div class="controls">
		  <input type="text" value="${posLoseTxForm.merchant.codeAndName}" />
		</div>
	      </div>    
	      <div class="control-group">
		<spring:message code="gc.poslose.label.pos.no" var="terminalIdLabel" />
		<label for="terminalId" class="control-label">${terminalIdLabel}</label> 
		<div class="controls">
		  <form:input path="terminalId" class="form-control" placeholder="${terminalIdLabel}" maxlength="12"/>
		</div>
	      </div>
	      <div class="control-group">
		<spring:message code="gc.poslose.label.cashier.no" var="cashierIdLabel" />
		<label for="cashierId" class="control-label">${cashierIdLabel}</label> 
		<div class="controls">
		  <form:input path="cashierId" class="form-control"  placeholder="${cashierIdLabel}" maxlength="12"/>
		</div>
	      </div>
	    </div>

	    <div class="span6 pull-left">
	      <div class="control-group">
		<spring:message code="gc.poslose.label.type" var="transactionTypeLabel" />
		<label for="transactionType" class="control-label">${transactionTypeLabel}</label> 
		<div class="controls">
		  <form:input path="transactionType" class="form-control" maxlength="12"/>
		</div>
	      </div>
	      <div class="control-group">
		<spring:message code="gc.poslose.label.date.time" var="transactionTimeLabel" />
		<label for="transactionDate" class="control-label">${transactionTimeLabel}</label> 
		<div class="controls">
		  <form:input path="transactionDate" class="form-control"/>
		</div>
	      </div>
	      <div class="control-group">
		<spring:message code="gc.poslose.label.transaction.no" var="transactionNoLabel" />
		<label for="transactionNo" class="control-label">${transactionNoLabel}</label> 
		<div class="controls">
		  <form:input path="transactionNo" id="transactionNo" class="form-control" placeholder="${transactionNoLabel}" maxlength="20"/>
		</div>
	      </div>
	      <div id="div-reftx" class="control-group hide">
		<spring:message code="gc.poslose.label.ref.transaction.no" var="refTransactionNo" />
		<label for="refTransactionNo" class="control-label">${refTransactionNo}</label> 
		<div class="controls">
		  <form:input path="refTransactionNo" id="refTransactionNo" class="form-control" placeholder="${refTransactionNo}" maxlength="15" />
		</div>
	      </div>
	    </div>

	    <div class="clearfix"></div>
	    <div id="tx-items-div" class="pull-center">
	      <label class="block-stack-title" style="border-bottom: 1px solid #E8E8E8; margin-bottom: 10px; padding: 5px 0 4px;">
		<spring:message code="gc.poslose.section.transaction.items" />
	      </label>
	      <div class="pull-left span3" style="width:95%;">
		<table id="productTable" class="table table-condensed table-compressed table-striped table-scrollable">
		  <thead>
		    <tr>
		      <th><spring:message code="gc.poslose.label.gift.card.no" /></th>
		      <th><spring:message code="gc.poslose.label.old.balance" /></th>
		      <th><spring:message code="gc.poslose.label.transaction.amount" /></th>
		      <th><spring:message code="gc.poslose.label.new.balance" /></th>
		    </tr>
		  </thead>
		  <tbody>
		    <c:forEach var="item" items="${posLoseTxForm.items}" varStatus="status" >
		      <tr data-index="${status.index}">
			<td>
			  <input type="text" class="input-large text-right cardNo" value="${item.cardNo}" name="items[${status.index}].cardNo" maxlength="20" />
			</td>
			<fmt:formatNumber type="number" pattern="#,##0" value="${item.previousBalance}" var="formattedPrevBal"/>
			<fmt:formatNumber type="number" pattern="#,##0" value="${item.amount}" var="formattedAmount"/>
			<fmt:formatNumber type="number" pattern="#,##0" value="${item.balance}" var="formattedBalance"/>
			<td><input type="text" class="input-medium text-right previousBalance" value="${formattedPrevBal}" name="items[${status.index}].previousBalance" readonly="readonly" /></td>
			<td><input type="text" class="input-medium text-right amount" value="${formattedAmount}" name="items[${status.index}].amount" /></td>
			<td><input type="text" class="input-medium text-right balance" value="${formattedBalance}" name="items[${status.index}].balance" readonly="readonly"/></td>
		      </tr>
		    </c:forEach>
		  </tbody>
		</table>
	      </div>
	    </div>
	  </div>
	</div>
      </form:form>
    </div>
  </div>
</div>
<script>
  $(document).ready(function() {
    $("input:text").prop('disabled', true);
  });
</script>