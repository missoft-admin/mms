<%@ include file="../common/taglibs.jsp" %>

<style type="text/css">
<!--
  .well .input {
    background-color: #fff;
    border: 1px solid #E8E8E8;
    box-shadow: none;    
    padding: 6px;
    height: 18px;
    margin-left: 5px;
  }
  .well .btn-primary {
    margin-left: 5px;
  }
  .well select, .well .input {
    margin: 5px;
  }
  .label-single {
      width: 150px;
  }
  
-->
</style>

<div id="form_SearchForm" class="form-horizontal block-search well">
  <div id="control-group" class="form-inline">
  
    <div class="form-inline">
      <label for="" class="label-single"><h5><spring:message code="search_by"/></h5></label>
      <select id="searchField">
        <c:forEach var="field" items="${rewardsSearchFields}">
          <option value="${field.value}"><spring:message code="search_employee_label_${field.value}" /></option>
        </c:forEach>
      </select>
      <input name="" id="searchValue" class="input" placeholder="Search"/>
      <input type="button" id="searchButton" value="<spring:message code="label_search"/>" class="btn btn-primary"/>
      <input id="clearButton" type="button" value="<spring:message code="label_clear" />" class="btn"/>
    </div>
			  	
    <div class="form-inline">
      <label for="" class="label-single"><h5><spring:message code="search_filter"/>:</h5></label>
      <select name="registeredStore" class="pointsSearchDropdown">
        <option value=""><spring:message code="search_registered_store"/></option>
          <c:forEach var="item" items="${rewardsFilterRegisteredStores}">
            <option value="${item.code}">${item.codeAndName}</option>
          </c:forEach>
      </select>
      
<!--       <select name="transactionType" class="pointsSearchDropdown"> -->
<!--         <option value=""> -->
<%--           <spring:message code="search_txn_types"/> --%>
<!--         </option> -->
<%--         <c:forEach var="item" items="${rewardsFilterTxnTypes}"> --%>
<%--           <option value="${item}">${item}</option> --%>
<%--         </c:forEach> --%>
<!--       </select> -->
      
      <select name="status" class="pointsSearchDropdown">
        <option value="">
          <spring:message code="search_status"/>
        </option>
        <c:forEach var="item" items="${rewardsFilterStatus}">
          <option value="${item}"><spring:message code="points_adjust_status_${item}" /></option>
        </c:forEach>
      </select>
    </div>
  </div>
	  <script src='<c:url value="/js/bootstrap/bootstrap-datepicker.js"/>' type='text/javascript'><![CDATA[&nbsp; ]]></script>
	  <link href="<c:url value="/css/bootstrap/datepicker.css" />" rel="stylesheet" />
</div>

<jsp:include page="print.jsp" />