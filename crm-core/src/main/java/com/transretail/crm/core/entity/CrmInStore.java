package com.transretail.crm.core.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotEmpty;

/**
 * @author Allan G. Ramirez (agramirez@exist.com)
 */
@Entity
@Table(name = "CRM_IN_STORE",
    uniqueConstraints = @UniqueConstraint(name = "CRM_IN_STORE_UNQ_IP_PORT", columnNames = {"IP", "TOMCAT_PORT"}))
public class CrmInStore implements Serializable {
    private static final long serialVersionUID = 8074914193648468383L;
    /**
     * Should be equal to STORE.CODE
     */
    @Id
    @NotEmpty
    @Column(name = "CODE", length = 15, nullable = false)
    private String id;
    @Transient
    private String prevId;
    @NotEmpty
    @Column(name = "STORE_NAME", length = 255, unique = true, nullable = false)
    private String name;
    @NotEmpty
    @Column(name = "IP", length = 50, nullable = false)
    private String ip;
    @NotNull
    @Column(name = "TOMCAT_PORT", nullable = false)
    private Integer tomcatPort = 9090;
    @NotEmpty
    @Column(name = "CRM_PROXY_REST_PRFX", length = 100, nullable = false)
    private String crmProxyRestPrefix = "/crm-proxy/api";
    @NotEmpty
    @Column(name = "CRM_RECONCLTN_REST_PRFX", length = 100, nullable = false)
    private String crmReconciliationRestPrefix = "/crm-reconciliation";

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getPrevId() {
        return prevId;
    }

    public void setPrevId(String prevId) {
        this.prevId = prevId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    public Integer getTomcatPort() {
        return tomcatPort;
    }

    public void setTomcatPort(Integer tomcatPort) {
        this.tomcatPort = tomcatPort;
    }

    public String getCrmProxyRestPrefix() {
        return crmProxyRestPrefix;
    }

    public void setCrmProxyRestPrefix(String crmProxyRestPrefix) {
        this.crmProxyRestPrefix = crmProxyRestPrefix;
    }

    public String getCrmReconciliationRestPrefix() {
        return crmReconciliationRestPrefix;
    }

    public void setCrmReconciliationRestPrefix(String crmReconciliationRestPrefix) {
        this.crmReconciliationRestPrefix = crmReconciliationRestPrefix;
    }

    public String getStoreCode() {
        return id;
    }

    @Transient
    public String getCrmProxyWsUrl() {
        StringBuilder stringBuilder = new StringBuilder("http://");
        stringBuilder.append(ip.trim());
        stringBuilder.append(":");
        stringBuilder.append(tomcatPort);
        stringBuilder.append(crmProxyRestPrefix.trim());
        return stringBuilder.toString();
    }

}
