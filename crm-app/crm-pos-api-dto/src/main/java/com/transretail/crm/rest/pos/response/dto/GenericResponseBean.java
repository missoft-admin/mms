package com.transretail.crm.rest.pos.response.dto;

import com.transretail.crm.AbstractRestResultBean;

/**
 * @author Allan G. Ramirez (agramirez@exist.com)
 */
public class GenericResponseBean extends AbstractRestResultBean {
    private String successMessage;
    private String errorMessage;

    public GenericResponseBean() {

    }

    public String getSuccessMessage() {
        return successMessage;
    }

    public void setSuccessMessage(String successMessage) {
        this.successMessage = successMessage;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }
}
