package com.transretail.crm.reconciliation.service.impl;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

import javax.annotation.Resource;
import javax.sql.DataSource;

import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.transretail.crm.Constants;
import com.transretail.crm.reconciliation.service.DbConnectionProvider;
import com.transretail.crm.reconciliation.service.StoreInfoService;

/**
 * @author Allan G. Ramirez (agramirez@exist.com)
 */
@Service
public class DbConnectionProviderImpl implements DbConnectionProvider, InitializingBean {
    @Value("#{'${jdbc.driverClassName}'}")
    private String driverClassName;
    @Value("#{'${ttserver.jdbc.url}'}")
    private String ttServerJdbcUrl;
    @Value("#{'${ttserver.jdbc.username}'}")
    private String ttServerJdbcUsername;
    @Value("#{'${ttserver.jdbc.password}'}")
    private String ttServerJdbcPassword;

    @Value("#{'${dbee.jdbc.driverClassName}'}")
    private String dbeeDriverClassName;
    @Value("#{'${dbee.jdbc.url}'}")
    private String dbeeJdbcUrl;
    @Value("#{'${dbee.jdbc.username}'}")
    private String dbeeJdbcUsername;
    @Value("#{'${dbee.jdbc.password}'}")
    private String dbeeJdbcPassword;

    @Resource(name = "dataSource")
    private DataSource localDataSource;
    @Autowired
    private StoreInfoService storeInfoService;

    @Override
    public void afterPropertiesSet() throws Exception {
        Class.forName(driverClassName);
        Class.forName(dbeeDriverClassName);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Connection createMainDbDbConnection() throws SQLException {
        String mainDbUsed = storeInfoService.getMainDbUsed();
        if (Constants.TTSERVER.toString().equals(mainDbUsed)) {
            return DriverManager.getConnection(ttServerJdbcUrl, ttServerJdbcUsername, ttServerJdbcPassword);
        } else if (Constants.LOCALDB.toString().equals(mainDbUsed)) {
            return localDataSource.getConnection();
        } else {
            return DriverManager.getConnection(dbeeJdbcUrl, dbeeJdbcUsername, dbeeJdbcPassword);
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Connection createPosIntegDbConnection() throws SQLException {
        String posIntegDbUsed = storeInfoService.getPosIntegDbUsed();
        if (Constants.TTSERVER.toString().equals(posIntegDbUsed)) {
            return DriverManager.getConnection(ttServerJdbcUrl, ttServerJdbcUsername, ttServerJdbcPassword);
        } else if (Constants.LOCALDB.toString().equals(posIntegDbUsed)) {
            return localDataSource.getConnection();
        } else {
            return DriverManager.getConnection(dbeeJdbcUrl, dbeeJdbcUsername, dbeeJdbcPassword);
        }
    }
}
