package com.transretail.crm.schedule.trigger;

import java.text.ParseException;

import javax.annotation.Resource;

import org.apache.commons.lang3.StringUtils;
import org.quartz.CronExpression;
import org.quartz.JobDetail;
import org.quartz.Scheduler;
import org.quartz.impl.triggers.CronTriggerImpl;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.quartz.JobDetailAwareTrigger;
import org.springframework.stereotype.Service;

import com.transretail.crm.common.service.exception.GenericServiceException;
import com.transretail.crm.core.entity.ApplicationConfig;
import com.transretail.crm.core.entity.enums.AppConfigDefaults;
import com.transretail.crm.core.entity.enums.AppKey;
import com.transretail.crm.core.repo.ApplicationConfigRepo;

@Service("memberMonitorTrigger")
public class MemberMonitorNotifSchedulerTrigger extends CronTriggerImpl implements InitializingBean {
	private static final long serialVersionUID = 1L;
	
	public static final String TRIGGER_NAME = "MEMBER_MONITOR_NOTIF_TRIGGER";
    public static final String TRIGGER_GROUP = Scheduler.DEFAULT_GROUP;
    
    @Resource(name = "memberMonitorNotifJob")
    private JobDetail jobDetail;
    
    @Autowired
    private ApplicationConfigRepo applicationConfigRepo;
	
    @Override
	public void setCronExpression(String inCronExpression) throws ParseException {
    	if ( !CronExpression.isValidExpression( inCronExpression ) ) {
            throw new GenericServiceException( "Invalid cron expression [" + inCronExpression + "]." );
        }
        super.setCronExpression( inCronExpression );
        ApplicationConfig theConfig = applicationConfigRepo.findByKey(AppKey.MEMBER_MONITOR_NOTIF_JOB);
        if (theConfig == null) {
            theConfig = new ApplicationConfig();
            theConfig.setKey(AppKey.MEMBER_MONITOR_NOTIF_JOB);
        }

        theConfig.setValue(inCronExpression);
        applicationConfigRepo.save(theConfig);
    }

	@Override
    public String getCronExpression() {
        ApplicationConfig theConfig = applicationConfigRepo.findByKey(AppKey.MEMBER_MONITOR_NOTIF_JOB);
        if(theConfig == null || StringUtils.isBlank(theConfig.getValue())) {
        	theConfig = new ApplicationConfig();
        	theConfig.setKey(AppKey.MEMBER_MONITOR_NOTIF_JOB);
        	theConfig.setValue(AppConfigDefaults.DEFAULT_MEMBER_MONITOR_NOTIF_CRON);
        	applicationConfigRepo.save(theConfig);
        }
        return theConfig.getValue();
    }

	@Override
	public void afterPropertiesSet() throws Exception {
		getJobDataMap().put( JobDetailAwareTrigger.JOB_DETAIL_KEY, jobDetail );
        setName( "MEMBER_MONITOR_NOTIF_TRIGGER" );
        setGroup( TRIGGER_GROUP );
        super.setCronExpression( getCronExpression() );
        setJobKey( jobDetail.getKey() );
	}
}
