package com.transretail.crm.report.template.impl;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.google.common.collect.Sets;
import com.mysema.query.jpa.JPASubQuery;
import com.mysema.query.jpa.impl.JPAQuery;
import com.mysema.query.types.OrderSpecifier;
import com.mysema.query.types.Projections;
import com.mysema.query.types.expr.BooleanExpression;
import com.transretail.crm.common.reporting.jasper.ClasspathFileResolver;
import com.transretail.crm.common.reporting.jasper.DefaultJRProcessor;
import com.transretail.crm.common.reporting.jasper.JRProcessor;
import com.transretail.crm.common.util.DateUtil;
import com.transretail.crm.core.entity.enums.ReportType;
import com.transretail.crm.core.entity.lookup.QStore;
import com.transretail.crm.core.entity.lookup.Store;
import com.transretail.crm.core.service.StoreService;
import com.transretail.crm.giftcard.dto.ProductProfileDto;
import com.transretail.crm.giftcard.entity.QGiftCardTransactionItem;
import com.transretail.crm.giftcard.entity.QProductProfile;
import com.transretail.crm.giftcard.entity.support.GiftCardSaleTransaction;
import com.transretail.crm.giftcard.service.ProductProfileService;
import com.transretail.crm.report.template.*;
import java.util.*;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import net.sf.jasperreports.engine.JRParameter;
import org.apache.commons.lang3.StringUtils;
import org.joda.time.LocalDate;
import org.joda.time.LocalDateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.context.support.MessageSourceResourceBundle;
import org.springframework.stereotype.Service;

@Service("giftCardSettlementReport")
public class GiftCardSettlementReport implements ReportTemplate, ReportsCustomizer, ReportDateFormatProvider, NoDataFoundSupport {

    protected static final String TEMPLATE_NAME = "Gift Card Settlement Report";
    protected static final String REPORT_NAME = "reports/gift_card_settlement_report.jasper";
    protected static final String FILTER_DATE_FROM = CommonReportFilter.DATE_FROM;
    protected static final String FILTER_DATE_TO = CommonReportFilter.DATE_TO;
    protected static final String FILTER_STORE = "store";
    protected static final String FILTER_PRODUCT = "product";
    protected static final String FILTER_SALES_TYPE = "salesType";

    private static final DateTimeFormatter INPUT_DATE_PATTERN = DateTimeFormat.forPattern("dd-MM-yyyy");
    private static final DateTimeFormatter FULL_DATE_PATTERN = DateTimeFormat.forPattern("dd-MM-yyyy");

    private static final Logger LOGGER = LoggerFactory.getLogger(GiftCardSettlementReport.class);

    @PersistenceContext
    private EntityManager em;
    @Autowired
    private MessageSource messageSource;
    @Autowired
    private StoreService storeService;
    @Autowired
    private ProductProfileService productService;

    private LocalDate parseDate(String dateInstance) {
        return INPUT_DATE_PATTERN.parseLocalDate(dateInstance);
    }

    @Override
    public DateTimeFormatter getDefaultDateFormat() {
        return FULL_DATE_PATTERN;
    }

    @Override
    public ReportType[] getReportTypes() {
        return ReportType.values();
    }

    @Override
    public String getName() {
        return TEMPLATE_NAME;
    }

    @Override
    public String getDescription() {
        return messageSource.getMessage("gc.settlement.report.label", (Object[]) null, LocaleContextHolder.getLocale());
    }

    @Override
    public boolean canView(Set<String> permissions) {
        return true;
    }

    @Override
    public Set<Option> customizerOption() {
        return Sets.newHashSet(ReportsCustomizer.Option.DATE_RANGE_FULL_VALIDATION);
    }

    @Override
    public Set<FilterField> getFilters() {
        Set<FilterField> filters = Sets.newLinkedHashSet();
        filters.add(FilterField.createDateField(FILTER_DATE_FROM,
                messageSource.getMessage("label_from", (Object[]) null, LocaleContextHolder.getLocale()),
                DateUtil.SEARCH_DATE_FORMAT, true));
        filters.add(FilterField.createDateField(FILTER_DATE_TO,
                messageSource.getMessage("label_to", (Object[]) null, LocaleContextHolder.getLocale()),
                DateUtil.SEARCH_DATE_FORMAT, true));

        QStore store = QStore.store;
        Map<String, String> stores = new LinkedHashMap<String, String>();
        stores.put("", "");
        stores.putAll(new JPAQuery(em).from(store).distinct().orderBy(store.code.asc())
                .map(store.code, store.code.concat(" - ").concat(store.name)));
        filters.add(FilterField.createDropdownField(FILTER_STORE,
                messageSource.getMessage("gc.settlement.report.label.store", (Object[]) null, LocaleContextHolder.getLocale()), stores));

        QProductProfile product = QProductProfile.productProfile;
        Map<String, String> productMap = Maps.newHashMap();
        productMap.put("", "");
        productMap.putAll(new JPAQuery(em).from(product).distinct().orderBy(product.productDesc.asc()).map(product.productCode, product.productDesc));
        filters.add(FilterField.createDropdownField(FILTER_PRODUCT,
                messageSource.getMessage("gc.settlement.report.label.product", (Object[]) null,
                        LocaleContextHolder.getLocale()), productMap));

        Map<String, String> salesType = new LinkedHashMap<String, String>();
        salesType.put("", "");
        salesType.put(GiftCardSaleTransaction.ACTIVATION.toString(), GiftCardSaleTransaction.ACTIVATION.toString());
        salesType.put(GiftCardSaleTransaction.REDEMPTION.toString(), GiftCardSaleTransaction.REDEMPTION.toString());
        salesType.put(GiftCardSaleTransaction.VOID_ACTIVATED.toString(), GiftCardSaleTransaction.VOID_ACTIVATED.toString());
        salesType.put(GiftCardSaleTransaction.VOID_REDEMPTION.toString(), GiftCardSaleTransaction.VOID_REDEMPTION.toString());

        filters.add(FilterField.createDropdownField(FILTER_SALES_TYPE,
                messageSource.getMessage("gc.settlement.report.label.transaction.type", (Object[]) null,
                        LocaleContextHolder.getLocale()), salesType));

        return filters;
    }

    @Override
    public JRProcessor createJrProcessor(Map<String, String> map) {
        LocalDate dateFrom = parseDate(map.get(FILTER_DATE_FROM));
        LocalDate dateTo = parseDate(map.get(FILTER_DATE_TO));
        String storeCode = map.get(FILTER_STORE);
        String productCode = map.get(FILTER_PRODUCT);
        String salesType = map.get(FILTER_SALES_TYPE);
        DefaultJRProcessor jrProcessor = new DefaultJRProcessor(REPORT_NAME);
        Map<String, Object> paramMap = prepareReportParameters(dateFrom, dateTo, productCode);
        paramMap.put("SUB_DATA_SOURCE", createDatasource(dateFrom, dateTo, storeCode, productCode,
                salesType, paramMap));
        jrProcessor.addParameters(paramMap);
        jrProcessor.setFileResolver(new ClasspathFileResolver("reports"));
        return jrProcessor;
    }

    public Map<String, Object> prepareReportParameters(LocalDate dateFrom, LocalDate dateTo, String productCode) {
        Map<String, Object> reportMap = Maps.newHashMap();
        Locale locale = LocaleContextHolder.getLocale();
        ProductProfileDto product = StringUtils.isBlank(productCode) ? null : productService.findProductProfileByCode(productCode);
        reportMap.put("PRODUCT", product == null ? "ALL" : product.getProductCode() + " - " + product.getProductDesc());
        reportMap.put("DATE_FROM", dateFrom.toString("MMM dd, yyyy"));
        reportMap.put("DATE_TO", dateTo.toString("MMM dd, yyyy"));
        reportMap.put("REPORT_TYPE", messageSource.getMessage("gc.settlement.report.label", (Object[]) null, locale));
        reportMap.put("NO_DATA", messageSource.getMessage("report.no.data", (Object[]) null,
                LocaleContextHolder.getLocale()));
        reportMap.put(JRParameter.REPORT_RESOURCE_BUNDLE, new MessageSourceResourceBundle(messageSource, locale));
        return reportMap;
    }

    public List<ReportBean> createDatasource(LocalDate dateFrom, LocalDate dateTo, String storeCode,
            String productCode, String salesType, Map<String, Object> paramMap) {

        QGiftCardTransactionItem gcTxnItem = QGiftCardTransactionItem.giftCardTransactionItem;
        QStore store = QStore.store;

        JPASubQuery storeQuery = new JPASubQuery().from(store)
                .where(store.code.eq(gcTxnItem.transaction.merchantId));
        OrderSpecifier<LocalDateTime> sortOrder = gcTxnItem.transaction.transactionDate.desc();

        return createQuery(dateFrom, dateTo, storeCode, productCode, salesType, paramMap).orderBy(sortOrder)
                .list(Projections.bean(ReportBean.class, storeQuery.unique(store).as("store"),
                                gcTxnItem.transaction.transactionNo.as("transactionNo"),
                                gcTxnItem.giftCard.barcode.as("giftCardNo"),
                                gcTxnItem.transaction.transactionType.stringValue().as("transactionType"),
                                gcTxnItem.transactionAmount.as("posAmount"),
                                gcTxnItem.transaction.transactionDate.as("transactionDateHandler")));
    }

    private JPAQuery createQuery(LocalDate dateFrom, LocalDate dateTo, String storeCode,
            String productCode, String salesType, Map<String, Object> paramMap) {

        QGiftCardTransactionItem gcTxnItem = QGiftCardTransactionItem.giftCardTransactionItem;
        QStore store = QStore.store;

        LocalDateTime start = dateFrom.toDateTimeAtStartOfDay().toLocalDateTime();
        LocalDateTime end = dateTo.plusDays(1).toDateTimeAtStartOfDay().toLocalDateTime();

        JPAQuery query = new JPAQuery(em);

        List<BooleanExpression> exprs = Lists.newArrayList();
        exprs.add(gcTxnItem.transaction.transactionDate.between(start, end));
        exprs.add(gcTxnItem.transaction.settlement.isTrue());
        if (StringUtils.isNotBlank(productCode)) {
            exprs.add(gcTxnItem.giftCard.profile.productCode.eq(productCode));
        }
        if (StringUtils.isNotBlank(storeCode)) {
            exprs.add(gcTxnItem.transaction.merchantId.eq(storeCode));
        }
        if (StringUtils.isNotBlank(salesType)) {
            exprs.add(gcTxnItem.transaction.transactionType.eq(GiftCardSaleTransaction.valueOf(salesType)));
        }

        return query.from(gcTxnItem).where(BooleanExpression.allOf(
                exprs.toArray(new BooleanExpression[exprs.size()])));
    }

    @Override
    public boolean isEmpty(Map<String, String> filter) {
        LocalDate dateFrom = parseDate(filter.get(FILTER_DATE_FROM));
        LocalDate dateTo = parseDate(filter.get(FILTER_DATE_TO));
        String storeCode = filter.get(FILTER_STORE);
        String productCode = filter.get(FILTER_PRODUCT);
        String salesType = filter.get(FILTER_SALES_TYPE);
        return createQuery(dateFrom, dateTo, storeCode, productCode, salesType, Maps.<String, Object>newHashMap())
                .notExists();
    }

    public static class ReportBean {

        private String transactionNo;
        private String giftCardNo;
        private String transactionType;
        private String storeCode;
        private String storeName;
        private Double posAmount;
        private String transactionDate;
        private LocalDateTime transactionDateHandler;

        public ReportBean() {

        }

        public ReportBean(Store store, String transactionNo, String giftCardNo,
                String transactionType, Double posAmount, LocalDateTime transactionDate) {
            super();
            this.storeCode = store.getCode();
            this.storeName = store.getName();
//			this.storeName = storeService.getStoreByCode(store).getName();
            this.transactionNo = transactionNo;
            this.giftCardNo = giftCardNo;
            this.transactionType = transactionType;
            this.posAmount = posAmount;
            this.transactionDate = transactionDate.toString(FULL_DATE_PATTERN);
        }

        public String getTransactionNo() {
            return transactionNo;
        }

        public void setTransactionNo(String transactionNo) {
            this.transactionNo = transactionNo;
        }

        public String getGiftCardNo() {
            return giftCardNo;
        }

        public void setGiftCardNo(String giftCardNo) {
            this.giftCardNo = giftCardNo;
        }

        public String getTransactionType() {
            return transactionType;
        }

        public void setTransactionType(String transactionType) {
            this.transactionType = transactionType;
        }

        public String getStoreCode() {
            return storeCode;
        }

        public void setStoreCode(String storeCode) {
            this.storeCode = storeCode;
        }

        public String getStoreName() {
            return storeName;
        }

        public void setStoreName(String storeName) {
            this.storeName = storeName;
        }

        public Double getPosAmount() {
            return posAmount;
        }

        public void setPosAmount(Double posAmount) {
            this.posAmount = posAmount;
        }

        public String getTransactionDate() {
            return transactionDate;
        }

        public void setTransactionDateHandler(LocalDateTime transactionDate) {
            this.transactionDate = transactionDate.toString("dd-MMM-yyyy hh:mm:ss aa");
        }

        public LocalDateTime getTransactionDateHandler() {
            return transactionDateHandler;
        }

        public void setTransactionDate(String transactionDate) {
            this.transactionDate = transactionDate;
        }

        public void setStore(Store store) {
            this.storeCode = store.getCode();
            this.storeName = store.getName();
        }
    }
}
