package com.transretail.crm.common.audit.handler;

import com.transretail.crm.common.audit.AuditMessage;
import com.transretail.crm.common.audit.AuditStatus;

/**
 * @author Allan G. Ramirez (agramirez@exist.com)
 */
public interface SaveHandler {
    void saveMessage(AuditStatus status, String moduleName, AuditMessage message);
}
