package com.transretail.crm.common.util;


import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.text.DateFormat;
import java.util.Calendar;
import java.util.Date;

import org.apache.commons.lang3.time.DateUtils;
import org.joda.time.DateTime;
import org.joda.time.LocalDate;
import org.joda.time.LocalDateTime;
import org.joda.time.LocalTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;


public final class DateUtil {


    public final static String REST_DATE_FORMAT   = "ddMMyyyyHHmmss";
    public final static String REST_BIRTH_DATE_FORMAT   = "ddMMyyyy";
    public final static String SEARCH_DATE_FORMAT = "dd-MM-yyyy";
    public final static String TIME_FORMAT = "HH:mm:ss";


    public static boolean betweenDates(Date targetDate, Date startDate, Date endDate) {
        if (DateUtils.isSameDay(targetDate, startDate) || DateUtils.isSameDay(targetDate, endDate)) {
            return true;
        } else if (targetDate.before(endDate) && targetDate.after(startDate)) {

            return true;
        } else {
            return false;
        }

    }


    public static Date convertDateString(String format, String dateString) throws ParseException {


        if (null == dateString)
            return null;

        DateFormat formatter;
        formatter = new SimpleDateFormat(format);
        return (Date) formatter.parse(dateString);

    }
    
    public static String convertDateToString(String format, Date date) {
    	DateFormat df = new SimpleDateFormat(format);
    	return df.format(date);
    }
    
    public static LocalTime convertLocalTimeString(String format, String timeString) {
    	if(timeString == null)
    		return null;
    	
    	DateTimeFormatter formatter = DateTimeFormat.forPattern(format);
    	return formatter.parseLocalTime(timeString);
    	
    }

    /**
     * Returns a Date set to the last possible millisecond of the day, just
     * before midnight. If a null day is passed in, a new Date is created.
     * midnight (00m 00h 00s)
     */
    public static Date getEndOfDay(Date day) {
        return getEndOfDay(day,Calendar.getInstance());
    }
    
    /**
     * Returns a DateTime set to the last possible millisecond of the day, just
     * before midnight. If a null day is passed in, a new DateTime is created.
     * midnight (00m 00h 00s)
     */
    public static DateTime getEndOfDay(DateTime day) {
        return getEndOfDay(day,Calendar.getInstance());
    }
    
    /**
     * Returns a LocalDate set to the last possible millisecond of the day, just
     * before midnight. If a null day is passed in, a new LocalDate is created.
     * midnight (00m 00h 00s)
     */
    public static LocalDate getEndOfDay(LocalDate day) {
        return getEndOfDay(day,Calendar.getInstance());
    }
    
    /**
     * Returns a LocalDate set to the last possible millisecond of the day, just
     * before midnight. If a null day is passed in, a new LocalDate is created.
     * midnight (00m 00h 00s)
     */
    public static LocalDateTime getEndOfDay(LocalDateTime day) {
        return getEndOfDay(day,Calendar.getInstance());
    }
    
    public static Date getEndOfDay(Date day,Calendar cal) {
        if (day == null) day = new Date();
        cal.setTime(day);
        cal.set(Calendar.HOUR_OF_DAY, cal.getMaximum(Calendar.HOUR_OF_DAY));
        cal.set(Calendar.MINUTE,      cal.getMaximum(Calendar.MINUTE));
        cal.set(Calendar.SECOND,      cal.getMaximum(Calendar.SECOND));
        cal.set(Calendar.MILLISECOND, cal.getMaximum(Calendar.MILLISECOND));
        return cal.getTime();
    }
    
    public static DateTime getEndOfDay(DateTime day,Calendar cal) {
        if (day == null) day = new DateTime();
        cal.setTime(day.toDate());
        cal.set(Calendar.HOUR_OF_DAY, cal.getMaximum(Calendar.HOUR_OF_DAY));
        cal.set(Calendar.MINUTE,      cal.getMaximum(Calendar.MINUTE));
        cal.set(Calendar.SECOND,      cal.getMaximum(Calendar.SECOND));
        cal.set(Calendar.MILLISECOND, cal.getMaximum(Calendar.MILLISECOND));
        return new DateTime(cal.getTime());
    }
    
    public static LocalDate getEndOfDay(LocalDate day,Calendar cal) {
        if (day == null) day = new LocalDate();
        cal.setTime(day.toDate());
        cal.set(Calendar.HOUR_OF_DAY, cal.getMaximum(Calendar.HOUR_OF_DAY));
        cal.set(Calendar.MINUTE,      cal.getMaximum(Calendar.MINUTE));
        cal.set(Calendar.SECOND,      cal.getMaximum(Calendar.SECOND));
        cal.set(Calendar.MILLISECOND, cal.getMaximum(Calendar.MILLISECOND));
        return new LocalDate(cal.getTime());
    }
    
    public static LocalDateTime getEndOfDay(LocalDateTime day,Calendar cal) {
        if (day == null) day = new LocalDateTime();
        cal.setTime(day.toDate());
        cal.set(Calendar.HOUR_OF_DAY, cal.getMaximum(Calendar.HOUR_OF_DAY));
        cal.set(Calendar.MINUTE,      cal.getMaximum(Calendar.MINUTE));
        cal.set(Calendar.SECOND,      cal.getMaximum(Calendar.SECOND));
        cal.set(Calendar.MILLISECOND, cal.getMaximum(Calendar.MILLISECOND));
        return new LocalDateTime(cal.getTime());
    }
    
    public static Date getWeekDate(int week, int startWeek, int year) {
        Calendar cal = Calendar.getInstance();
        cal.set(year, Calendar.JANUARY, Calendar.DAY_OF_MONTH, 0, 0, 0);
        cal.set(Calendar.WEEK_OF_YEAR, week);
        cal.set(Calendar.DAY_OF_WEEK, startWeek);
        
        return cal.getTime();
    }
    
    public static Date getEndOfWeek(int week, int startWeek, int year) {
        Calendar cal = Calendar.getInstance();
        cal.set(Calendar.YEAR, year);
        cal.set(Calendar.HOUR_OF_DAY, cal.getMaximum(Calendar.HOUR_OF_DAY));
        cal.set(Calendar.MINUTE,      cal.getMaximum(Calendar.MINUTE));
        cal.set(Calendar.SECOND,      cal.getMaximum(Calendar.SECOND));
        cal.set(Calendar.MILLISECOND, cal.getMaximum(Calendar.MILLISECOND));
        cal.set(Calendar.WEEK_OF_YEAR, week);
        if (startWeek == Calendar.SUNDAY)
            cal.set(Calendar.DAY_OF_WEEK, Calendar.SATURDAY);
        else if (startWeek == Calendar.MONDAY)
            cal.set(Calendar.DAY_OF_WEEK, Calendar.SUNDAY);
        return cal.getTime();
    }
    
    public static Date addDays(Date date, int days) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        cal.add(Calendar.DATE, days);
        
        return cal.getTime();
    }
    
    public static Date getFirstDateOfYear(int year) {
        Calendar cal = Calendar.getInstance();
        cal.set(Calendar.YEAR, year);
        cal.set(Calendar.MONTH, Calendar.JANUARY);
        cal.set(Calendar.DAY_OF_MONTH, 1);
        cal.set(Calendar.HOUR_OF_DAY, cal.getMinimum(Calendar.HOUR_OF_DAY));
        cal.set(Calendar.MINUTE,      cal.getMinimum(Calendar.MINUTE));
        cal.set(Calendar.SECOND,      cal.getMinimum(Calendar.SECOND));
        cal.set(Calendar.MILLISECOND, cal.getMinimum(Calendar.MILLISECOND));
        
        return cal.getTime();
    }
    
    public static Date getLastDateOfYear(int year) {
        Calendar cal = Calendar.getInstance();
        cal.set(Calendar.YEAR, year);
        cal.set(Calendar.MONTH, Calendar.DECEMBER);
        cal.set(Calendar.DAY_OF_MONTH, 31);
        cal.set(Calendar.HOUR_OF_DAY, cal.getMaximum(Calendar.HOUR_OF_DAY));
        cal.set(Calendar.MINUTE,      cal.getMaximum(Calendar.MINUTE));
        cal.set(Calendar.SECOND,      cal.getMaximum(Calendar.SECOND));
        cal.set(Calendar.MILLISECOND, cal.getMaximum(Calendar.MILLISECOND));
        
        return cal.getTime();
    }
    
    public static Date getBeginningOfMonth(int year, int month) {
        Calendar cal = Calendar.getInstance();
        cal.set(Calendar.YEAR, year);
        cal.set(Calendar.MONTH, month-1);
        cal.set(Calendar.DAY_OF_MONTH, 1);
        cal.set(Calendar.HOUR_OF_DAY, cal.getMinimum(Calendar.HOUR_OF_DAY));
        cal.set(Calendar.MINUTE,      cal.getMinimum(Calendar.MINUTE));
        cal.set(Calendar.SECOND,      cal.getMinimum(Calendar.SECOND));
        cal.set(Calendar.MILLISECOND, cal.getMinimum(Calendar.MILLISECOND));
        
        return cal.getTime();
    }
    
    public static Date getEndOfMonth(int year, int month) {
        Calendar cal = Calendar.getInstance();
        cal.set(Calendar.YEAR, year);
        cal.set(Calendar.MONTH, month-1);
        cal.set(Calendar.DAY_OF_MONTH, cal.getActualMaximum(Calendar.DAY_OF_MONTH));
        cal.set(Calendar.HOUR_OF_DAY, cal.getMaximum(Calendar.HOUR_OF_DAY));
        cal.set(Calendar.MINUTE,      cal.getMaximum(Calendar.MINUTE));
        cal.set(Calendar.SECOND,      cal.getMaximum(Calendar.SECOND));
        cal.set(Calendar.MILLISECOND, cal.getMaximum(Calendar.MILLISECOND));
        
        return cal.getTime();
    }
}
