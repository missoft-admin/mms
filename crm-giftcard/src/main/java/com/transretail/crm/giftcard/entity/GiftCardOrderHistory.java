package com.transretail.crm.giftcard.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.Parameter;
import org.hibernate.annotations.Type;
import org.joda.time.DateTime;
import org.springframework.data.annotation.CreatedBy;
import org.springframework.data.annotation.CreatedDate;

import com.transretail.crm.giftcard.entity.support.GiftCardOrderStatus;

/**
 *
 */
@Entity
@Table(name = "CRM_GC_ORD_HIST")
public class GiftCardOrderHistory implements Serializable {
    private static final long serialVersionUID = 8586787820388288151L;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "ID")
    private Long id;
    @Column(name = "CREATED_DATETIME")
    @CreatedDate
    @Type(type = "org.jadira.usertype.dateandtime.joda.PersistentDateTime",
        parameters = {@Parameter(name = "databaseZone", value = "jvm")})
    private DateTime created;
    @Column(name = "CREATED_BY")
    @CreatedBy
    private String createUser;
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "ORDER_NO", nullable = false)
    private GiftCardOrder order;
    @Column(name = "STATUS", nullable = false, length = 15)
    @Enumerated(EnumType.STRING)
    private GiftCardOrderStatus status;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public DateTime getCreated() {
        return created;
    }

    public void setCreated(DateTime created) {
        this.created = created;
    }

    public String getCreateUser() {
        return createUser;
    }

    public void setCreateUser(String createUser) {
        this.createUser = createUser;
    }

    public GiftCardOrder getOrder() {
        return order;
    }

    public void setOrder(GiftCardOrder order) {
        this.order = order;
    }

    public GiftCardOrderStatus getStatus() {
        return status;
    }

    public void setStatus(GiftCardOrderStatus status) {
        this.status = status;
    }
}
