package com.transretail.crm.web.controller;

import java.io.FileInputStream;
import java.io.InputStream;
import java.text.ParseException;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.Callable;
import java.util.concurrent.ConcurrentHashMap;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.Predicate;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.joda.time.LocalDate;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.NoSuchMessageException;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.context.ServletContextAware;
import org.springframework.web.servlet.ModelAndView;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.transretail.crm.common.reporting.engine.data.ReportDataSource;
import com.transretail.crm.common.reporting.jasper.JRProcessor;
import com.transretail.crm.common.service.dto.response.NameIdPairDto;
import com.transretail.crm.common.service.exception.GenericServiceException;
import com.transretail.crm.common.util.DateUtil;
import com.transretail.crm.core.dto.MemberSearchDto;
import com.transretail.crm.core.entity.MemberModel;
import com.transretail.crm.core.entity.enums.ReportType;
import com.transretail.crm.core.security.model.impl.CustomSecurityUserDetailsImpl;
import com.transretail.crm.core.security.util.UserUtil;
import com.transretail.crm.core.service.EmployeePurchaseService;
import com.transretail.crm.core.service.MemberMonitorService;
import com.transretail.crm.core.service.MemberService;
import com.transretail.crm.core.service.PointsRewardService;
import com.transretail.crm.core.service.PointsTxnManagerService;
import com.transretail.crm.core.service.StoreService;
import com.transretail.crm.report.template.*;
import com.transretail.crm.report.template.impl.MemberListDocument;
import com.transretail.crm.web.controller.util.ControllerResponse;
import net.sf.jasperreports.engine.JRRewindableDataSource;

@RequestMapping("/report")
@Controller
public class ReportsController extends AbstractReportsController implements ServletContextAware {
    protected static final String ATTR_REPORT_TEMPLATE = "reportTemplates";
    protected static final String ATTR_REPORT_TYPE = "reportTypes";
	private static final Logger LOGGER = LoggerFactory.getLogger(ReportsController.class);
	
	private static final Date START_DATE = new Date(0);
	private static final Date END_DATE = new Date(Long.MAX_VALUE);
	
    private static final Map<String, ReportTemplate> REPORT_TEMPLATE_MAP = new ConcurrentHashMap<String, ReportTemplate>(16, 0.9f, 1);
    private static final String logoLocation = "/carrefour/carrefouridlg.png";

    @Autowired
    private PointsTxnManagerService pointsManagerService;
    @Autowired
    private MemberService memberService;    
    @Autowired
    private EmployeePurchaseService employeePurchaseService;    
    @Autowired
    private PointsRewardService pointsRewardService;
    @Autowired
    private MessageSource messageSource;
    @Autowired
    private MemberMonitorService memberMonitorService;
    @Autowired
    private StoreService storeService;
    @Autowired
    private MemberListDocument memberListDocument;

    
    private ServletContext servletContext;
    private String imagePath;

    @Override
    public void setServletContext(ServletContext servletContext) {
        this.servletContext = servletContext;
        imagePath = servletContext.getRealPath("images");
    }

    @RequestMapping(value = "/points/export/{id}/{reportType}/{dateFrom}/{dateTo}")
    public void exportPointsToPdf(@PathVariable String id,
    	@PathVariable String reportType,
        @PathVariable String dateFrom,
        @PathVariable String dateTo, final HttpServletResponse response, HttpServletRequest request) throws Exception {
        InputStream logo = null;
        Date from;
		try {
			from = DateUtil.convertDateString(DateUtil.SEARCH_DATE_FORMAT, dateFrom);
		} catch (ParseException e) {
			from = START_DATE;
		}
        
        Date to;
		try {
			to = DateUtil.convertDateString(DateUtil.SEARCH_DATE_FORMAT, dateTo);
		} catch (ParseException e) {
			to = END_DATE;
		}
        
        try {
            MemberModel member = memberService.findByAccountId(id);
            logo = new FileInputStream(imagePath + "/carrefour/carrefouridlg.png");
            JRProcessor jrProcessor = pointsManagerService
                .createPointsJrProcessor(member, from, to, logo);
            if(ReportType.PDF.getCode().equals(reportType))
            	renderReport(response, jrProcessor);
            else if(ReportType.EXCEL.getCode().equals(reportType))
            	renderExcelReport(response, jrProcessor, "Points Audit Report");
        } finally {
            IOUtils.closeQuietly(logo);
        }
    }

    @RequestMapping(value = "/member/points/export/pdf/{id}/{dateFrom}/{dateTo}", produces="application/json; charset=utf-8", method=RequestMethod.POST)
    public @ResponseBody ControllerResponse ajaxExportPointsToPdf(
    		@PathVariable String id,
    		@PathVariable String dateFrom,
    		@PathVariable String dateTo, final HttpServletResponse response, HttpServletRequest request) throws Exception {

        InputStream logo = null;
        Date from;
		try {
			from = DateUtil.convertDateString(DateUtil.SEARCH_DATE_FORMAT, dateFrom);
		} catch (ParseException e) {
			from = START_DATE;
		}
        
        Date to;
		try {
			to = DateUtil.convertDateString(DateUtil.SEARCH_DATE_FORMAT, dateTo);
		} catch (ParseException e) {
			to = END_DATE;
		}


		ControllerResponse theResp = new ControllerResponse();
    	if ( StringUtils.isNotBlank( dateFrom )  && StringUtils.isNotBlank( dateTo )  
    			&& from.getTime() != START_DATE.getTime() && to.getTime() != END_DATE.getTime() ) {
    		if( from.after( to ) ) {
        		theResp.setSuccess( false );
    			theResp.setResult( messageSource.getMessage( "points_msg_invaliddaterange", null, LocaleContextHolder.getLocale() ) );
    			return theResp;
    		}
    	}

        try {
            MemberModel member = memberService.findByAccountId(id);
            logo = new FileInputStream(imagePath + "/carrefour/carrefouridlg.png");
            JRProcessor jrProcessor = pointsManagerService
                .createPointsJrProcessor(member, from, to, logo);
            renderReport(response, jrProcessor);
        } finally {
            IOUtils.closeQuietly(logo);
        }
		theResp.setSuccess( true );
        return theResp;
    }
    
    @RequestMapping(value = "/points/export/{id}/{reportType}")
    public void exportPointsToPdf(@PathVariable String id, 
    		@PathVariable String reportType,
    		final HttpServletResponse response, HttpServletRequest request) throws Exception {
        exportPointsToPdf(id, reportType, null, null, response, request);
    }
    
    @RequestMapping(value = "/employee/transaction/{type}/export/pdf/{id}/{reportType}/{dateFrom}/{dateTo}")
    public void exportEmplPurchaseTxnToPdf(@PathVariable String id,
    	@PathVariable String type,	
    	@PathVariable String reportType,	
        @PathVariable String dateFrom,
        @PathVariable String dateTo, final HttpServletResponse response, HttpServletRequest request) throws Exception {
        InputStream logo = null;
        
        Date dateTimeFrom = null;
        Date dateTimeTo = null;
        
    	try {
    		dateTimeFrom = DateUtil.convertDateString(DateUtil.SEARCH_DATE_FORMAT, dateFrom);
        } catch (ParseException e) {
            LOGGER.error("Error Parsing date parameter {} ", dateFrom, e.getMessage());
        }
    	
    	try {
    		dateTimeTo = DateUtil.convertDateString(DateUtil.SEARCH_DATE_FORMAT, dateTo);
        } catch (ParseException e) {
            LOGGER.error("Error Parsing date parameter {} ", dateTo, e.getMessage());
        }
    	
        try {
            logo = new FileInputStream(imagePath + "/carrefour/carrefouridlg.png");
            JRProcessor jrProcessor = null;
            if(type.equals("purchase")) {
            	jrProcessor = employeePurchaseService.createPurchaseTxnJrProcessor(id, dateTimeFrom, dateTimeTo, logo);
            }
            if(ReportType.PDF.getCode().equals(reportType))
            	renderReport(response, jrProcessor);
            else if(ReportType.EXCEL.getCode().equals(reportType))
            	renderExcelReport(response, jrProcessor, messageSource.getMessage("txn_report_label_purchase_report_type", null, LocaleContextHolder.getLocale()));
        } finally {
            IOUtils.closeQuietly(logo);
        }
    }

    @Autowired
    public void setReportTemplates(List<ReportTemplate> reportTemplates) {
        Map<String, ReportTemplate> map = Maps.newHashMap();
        for (ReportTemplate template : reportTemplates) {
            String name = template.getName();
            if (map.containsKey(name)) {
                throw new GenericServiceException("Report template name [" + name + "] already exists.");
            }
            map.put(name, template);
        }
        REPORT_TEMPLATE_MAP.clear();
        REPORT_TEMPLATE_MAP.putAll(map);
    }
    
    @RequestMapping(value = "/employee/transaction/{type}/export/pdf/{id}/{reportType}")
    public void exportEmplPurchaseTxnToPdf(@PathVariable String id,
    	@PathVariable String type,
    	@PathVariable String reportType,
    	final HttpServletResponse response, HttpServletRequest request) throws Exception {
        exportEmplPurchaseTxnToPdf(id, type, reportType, null, null, response, request);
    }

	@RequestMapping(value = "/employee/rewards/export/{reportType}")
    public void exportRewardsToPdf(@PathVariable String reportType, final HttpServletResponse response, HttpServletRequest request) throws Exception {
        InputStream logo = null;
        
        try {
            logo = new FileInputStream(imagePath + "/carrefour/carrefouridlg.png");
            JRProcessor jrProcessor = null;
            jrProcessor = pointsRewardService.createRewardsJrProcessor(logo, null, null);
            if(ReportType.PDF.getCode().equals(reportType))
            	renderReport(response, jrProcessor);
            else if(ReportType.EXCEL.getCode().equals(reportType))
            	renderExcelReport(response, jrProcessor, messageSource.getMessage(
        				"rewards_report_label_report_type", null,
        				LocaleContextHolder.getLocale()));
        } finally {
            IOUtils.closeQuietly(logo);
        }
    }

	/*@RequestMapping(value = "/employee/rewards/report/{reportType}/{dateFrom}/{dateTo}", produces="application/json; charset=utf-8", method=RequestMethod.GET)
    public @ResponseBody ControllerResponse exportEmployeeRewards( 
    		@PathVariable("reportType") ReportType reportType, 
    		@PathVariable("dateFrom") Long dateFrom, 
    		@PathVariable("dateTo") Long dateTo, 
    		final HttpServletResponse response, 
    		HttpServletRequest request ) {

		ControllerResponse resp = new ControllerResponse();
		resp.setSuccess( false );
		if ( null == dateFrom || null == dateTo || dateFrom > dateTo ) {
			resp.setResult( messageSource.getMessage( "err_invaliddates", null, LocaleContextHolder.getLocale() ) );
			return resp;
		}

        InputStream logo = null;        
        try {
            logo = new FileInputStream( imagePath + "/carrefour/carrefouridlg.png" );
            JRProcessor jrProcessor = null;
            jrProcessor = pointsRewardService.createRewardsJrProcessor(logo);
            if( reportType == ReportType.PDF ) {
            	renderReport(response, jrProcessor);
            }
            else if( reportType == ReportType.EXCEL ) {
            	renderExcelReport( response, jrProcessor, messageSource.getMessage( "rewards_report_label_report_type", 
            			null, LocaleContextHolder.getLocale() ) );
            }
            resp.setSuccess( true );
        } 
        catch( Exception ex ) {
        	resp.setResult( ex.getMessage() );
            return resp;
        }
        finally {
            IOUtils.closeQuietly(logo);
        }
        return null;
    }*/

	@RequestMapping(value = "/employee/rewards/report/{reportType}/{dateFrom}/{dateTo}")
    public void exportEmployeeRewards( 
    		@PathVariable("reportType") ReportType reportType, 
    		@PathVariable("dateFrom") String dateFrom, 
    		@PathVariable("dateTo") String dateTo, 
    		final HttpServletResponse response, 
    		HttpServletRequest request ) throws Exception {

        InputStream logo = null;        
        try {
            logo = new FileInputStream( imagePath + "/carrefour/carrefouridlg.png" );
            JRProcessor jrProcessor = null;
            jrProcessor = pointsRewardService.createRewardsJrProcessor( logo, 
            		DateUtil.convertDateString( "dd MMM yyyy", dateFrom ),
            		DateUtil.convertDateString( "dd MMM yyyy", dateTo ) );
            if( reportType == ReportType.PDF ) {
            	renderReport(response, jrProcessor);
            }
            else if( reportType == ReportType.EXCEL ) {
            	renderExcelReport( response, jrProcessor, messageSource.getMessage( "rewards_report_label_report_type", 
            			null, LocaleContextHolder.getLocale() ) );
            }
        }
        finally {
            IOUtils.closeQuietly(logo);
        }
    }

    @RequestMapping(value = "/template/list")
    public ModelAndView templateList() {
        Set<String> currentPermissions = UserUtil.getCurrentUser().getPermissions();
        List<NameIdPairDto> reportTemplates = Lists.newArrayList();
        for (ReportTemplate template : REPORT_TEMPLATE_MAP.values()) {
            if (template.canView(currentPermissions)) {
                reportTemplates.add(new NameIdPairDto(template.getName(), template.getDescription()));
            }
        }

        return new ModelAndView("report/template/list")
        	.addObject(ATTR_REPORT_TEMPLATE, reportTemplates)
        	.addObject(ATTR_REPORT_TYPE, ReportType.values());
    }

    @RequestMapping(value = "/template/{reportTemplateName}")
    public ModelAndView viewReport(@PathVariable("reportTemplateName") String reportTemplateName) {
        Set<String> currentPermissions = UserUtil.getCurrentUser().getPermissions();
        List<ReportType> reportTypes = Lists.newArrayList();
        List<NameIdPairDto> reportTemplates = Lists.newArrayList();
        for (ReportTemplate template : REPORT_TEMPLATE_MAP.values()) {
            if (template.canView(currentPermissions) && template.getName().equals(reportTemplateName)) {
                reportTemplates.add(new NameIdPairDto(template.getName(), template.getDescription()));
                for(ReportType type : template.getReportTypes()) {
                	if(!reportTypes.contains(type)) {
                		reportTypes.add(type);
                	}
                }
            }
        }
        // Sort by description
        Collections.sort(reportTemplates, new Comparator<NameIdPairDto>() {
            @Override
            public int compare(NameIdPairDto o1, NameIdPairDto o2) {
                return o1.getName().compareTo(o2.getName());
            }
        });
        return new ModelAndView("report/template/list")
            .addObject(ATTR_REPORT_TEMPLATE, reportTemplates)
            .addObject(ATTR_REPORT_TYPE, reportTypes.toArray(new ReportType[reportTypes.size()]));
    }

    @RequestMapping(value = "/template/{reportTemplateName}/filters")
    @ResponseBody
    public Set<FilterField> getFilters(@PathVariable String reportTemplateName) {
        return REPORT_TEMPLATE_MAP.get(reportTemplateName).getFilters();
    }

    @RequestMapping(value = "/template/{reportType}/{reportTemplateName}/view")
    public void viewReport(@PathVariable String reportTemplateName,
    	@PathVariable String reportType,
        @RequestParam Map<String, String> allRequestParams, HttpServletResponse response) throws Exception {
    	ReportTemplate reportTemplate = REPORT_TEMPLATE_MAP.get(reportTemplateName);
        allRequestParams.put(ReportTemplate.PARAM_REPORT_TYPE, reportType);
        JRProcessor jrProcessor = reportTemplate.createJrProcessor(allRequestParams);
        InputStream logo = null;        
        try {
            logo = new FileInputStream( imagePath + "/carrefour/carrefouridlg.png" );
            jrProcessor.addParameter("logo", logo);
            final CustomSecurityUserDetailsImpl currentUser = UserUtil.getCurrentUser();
            jrProcessor.addParameter("PRINTED_BY", currentUser == null ? "" : currentUser.getUsername());
            if(ReportType.PDF.getCode().equals(reportType))
            	renderReport(response, jrProcessor);
            else if(ReportType.EXCEL.getCode().equals(reportType))
            	renderExcelReport(response, jrProcessor, reportTemplateName);
        }
        finally {
        	IOUtils.closeQuietly(logo);
        }
    }
    
    @ResponseBody
    @RequestMapping(value = "/template/{templateName}/data/check", method = RequestMethod.POST)
    public Boolean reportEmpty(final @RequestParam Map<String, String> requestParams, @PathVariable String templateName) {
        ReportTemplate reportTemplate = REPORT_TEMPLATE_MAP.get(templateName);
        if (reportTemplate instanceof NoDataFoundSupport) {
            NoDataFoundSupport ndfs = (NoDataFoundSupport) reportTemplate;
            return ndfs.isEmpty(requestParams);
        } 
        
        return false; //  support for old reports while it wa not yet migrated.
    }

    @ResponseBody
    @RequestMapping(value = "/template/validate/{reportTemplateName}", method = RequestMethod.POST)
    public List<String> validate(final @RequestParam Map<String, String> allRequestParams, @PathVariable String reportTemplateName) {

	    Locale locale = LocaleContextHolder.getLocale();
	    ReportTemplate reportTemplate = REPORT_TEMPLATE_MAP.get(reportTemplateName);
	    List<String> result = Lists.newArrayList();
	    Set<FilterField> filters = reportTemplate.getFilters();
        for (FilterField filter : filters) {

            if(filter.isRequired()){

                String value = allRequestParams.get(filter.getName());
                if(StringUtils.isBlank(value)){

                    result.add(messageSource.getMessage("report.required.filter", new Object[]{filter.getLabel()}, locale));
                }
            }
        }
        
        if(reportTemplate instanceof ValidatableReportTemplate){

            ValidatableReportTemplate vrt = (ValidatableReportTemplate) reportTemplate;
            return vrt.validate(allRequestParams);
        }
        
        if (reportTemplate instanceof ReportsCustomizer && result.isEmpty()) {

            ReportsCustomizer rc = (ReportsCustomizer) reportTemplate;
            Set<ReportsCustomizer.Option> customizerOption = rc.customizerOption();
	    
            DateTimeFormatter dtf = DateTimeFormat.forPattern(DateUtil.SEARCH_DATE_FORMAT); // TODO: extension for custom format
            if(reportTemplate instanceof ReportDateFormatProvider){

                dtf = ((ReportDateFormatProvider)reportTemplate).getDefaultDateFormat();
            }

            for (ReportsCustomizer.Option option : customizerOption) {

                String dateFrom = allRequestParams.get(CommonReportFilter.DATE_FROM);
                String dateTo = allRequestParams.get(CommonReportFilter.DATE_TO);
		
                switch (option) {
                    case DATE_RANGE_FULL_VALIDATION:
                        boolean withDateFromOptional = customizerOption.contains(ReportsCustomizer.Option.DATE_FROM_OPTIONAL);
			            if (withDateFromOptional) {

			                continue;
			            }
		
			            LocalDate df = LocalDate.parse(dateFrom, dtf);
			            LocalDate dt = LocalDate.parse(dateTo, dtf);

			            if(df.isAfter(dt)){

			                result.add(messageSource.getMessage("report.date.from.invalid.message", null, locale));
			            }
			
			            if(dt.isBefore(df)){

			                result.add(messageSource.getMessage("report.date.to.invalid.message", null, locale));
			            }

			            break;
                }


            }

        }
        //        else {
        //	    validateDateRange(allRequestParams, result, locale);
        //	}
	    return result;
    }

    private void validateDateRange(final Map<String, String> allRequestParams, List<String> result, Locale locale) throws NoSuchMessageException {
	// retain for backward compatibility
	boolean bool = CollectionUtils.countMatches(allRequestParams.keySet(), new Predicate() {
	    @Override
	    public boolean evaluate(Object object) {
		String key = (String) object;
		return (StringUtils.containsIgnoreCase(key, "date") && StringUtils.isBlank(allRequestParams.get(key)));
	    }
	}) > 0;
	
	if (bool) {
	    result.add(messageSource.getMessage("required_date_fields", null, locale));
	}
    }
    
    @RequestMapping(value = "/membermonitor/{reportType}/{type}")
    public void exportMemberMonitorToPdf(@PathVariable("type") String type, 
    		@PathVariable("reportType") String reportType, final HttpServletResponse response, 
    		HttpServletRequest request) throws Exception {
    	exportMemberMonitorToPdf(LocalDate.now(), type, reportType, response, request);
    }

    @RequestMapping(value = "/membermonitor/{reportType}/{type}/{date}")
    public void exportMemberMonitorToPdf(@PathVariable("date") LocalDate date, @PathVariable("type") String type, 
    		@PathVariable("reportType") String reportType, final HttpServletResponse response, 
    		HttpServletRequest request) throws Exception {
    	InputStream logo = null;
    	
    	try {
    		logo = new FileInputStream(imagePath + "/carrefour/carrefouridlg.png");
    		JRProcessor jrProcessor = memberMonitorService.createJRProcessor(date, logo, type.equals("hr"));
    		if(ReportType.PDF.getCode().equals(reportType))
            	renderReport(response, jrProcessor);
            else if(ReportType.EXCEL.getCode().equals(reportType))
            	renderExcelReport(response, jrProcessor, "member_monitor_report_" + date.toString());
    	}
    	finally {
    		IOUtils.closeQuietly(logo);
    	}
    }

    @RequestMapping( value="/types/list", produces="application/json; charset=utf-8", method=RequestMethod.GET )
	public @ResponseBody ReportType[] getReportTypes() {
		return ReportType.values();
	}
    
    @RequestMapping( value="/types/{reportTemplateName}/list", produces="application/json; charset=utf-8", method=RequestMethod.GET )
	public @ResponseBody ReportType[] getReportTypes(@PathVariable String reportTemplateName) {
    	return REPORT_TEMPLATE_MAP.get(reportTemplateName).getReportTypes();
	}
    
    
    @RequestMapping(value="/points/expire/{type}/{dateFrom}/{dateTo}")
    public void exportExpirePoints(@PathVariable("type") String type, 
    		@PathVariable("dateFrom") LocalDate dateFrom, @PathVariable("dateTo") LocalDate dateTo, 
    		final HttpServletResponse response, HttpServletRequest request) throws Exception {
    	InputStream logo = null;
    	LocalDate startMonth = dateFrom != null ? dateFrom : LocalDate.now().minusMonths(1);
    	LocalDate endMonth = dateTo != null ? dateTo : LocalDate.now().minusMonths(1);
    	
    	try {
    		logo = new FileInputStream(imagePath + "/carrefour/carrefouridlg.png");
    		JRProcessor jrProcessor = pointsManagerService.createExpirePointsJrProcessor(startMonth, endMonth, logo);
    		if(ReportType.PDF.getCode().equals(type))
            	renderReport(response, jrProcessor);
            else if(ReportType.EXCEL.getCode().equals(type))
            	renderExcelReport(response, jrProcessor, "points_expire_report_" + 
            			startMonth.toString("MMM-yyyy") + "_to_" + endMonth.toString("MMM-yyyy"));
    	}
    	finally {
    		IOUtils.closeQuietly(logo);
    	}
    }
    
    @RequestMapping(value="/points/expire/future/{type}/{dateFrom}/{dateTo}")
    public void exportExpirePointsFuture(@PathVariable("type") String type, 
    		@PathVariable("dateFrom") LocalDate dateFrom, @PathVariable("dateTo") LocalDate dateTo, 
    		final HttpServletResponse response, HttpServletRequest request) throws Exception {
    	InputStream logo = null;
    	LocalDate startMonth = dateFrom != null ? dateFrom : LocalDate.now().minusMonths(1);
    	LocalDate endMonth = dateTo != null ? dateTo : LocalDate.now().minusMonths(1);
    	
    	try {
    		logo = new FileInputStream(imagePath + "/carrefour/carrefouridlg.png");
    		JRProcessor jrProcessor = pointsManagerService.createFutureExpirePointsJrProcessor(startMonth, endMonth, logo);
    		if(ReportType.PDF.getCode().equals(type))
            	renderReport(response, jrProcessor);
            else if(ReportType.EXCEL.getCode().equals(type))
            	renderExcelReport(response, jrProcessor, "points_expire_report_" + 
            			startMonth.toString("MMM-yyyy") + "_to_" + endMonth.toString("MMM-yyyy"));
    	}
    	finally {
    		IOUtils.closeQuietly(logo);
    	}
    }

    @RequestMapping( value="/store/popular/{type}/{dateFrom}/{dateTo}" )
    public void exportPopularStoresToVisit(
    		@PathVariable("type") String type, 
    		@PathVariable("dateFrom") LocalDate dateFrom, 
    		@PathVariable("dateTo") LocalDate dateTo, 
    		final HttpServletResponse res, HttpServletRequest req ) throws Exception {

    	InputStream logo = null;
    	try {
    		JRProcessor jrProcessor = storeService.createPopularStoresJrProcessor( dateFrom, dateTo );
    		if( null != type && ReportType.PDF.getCode().equalsIgnoreCase( type ) ) {
            	renderReport( res, jrProcessor );
    		}
            else if( ReportType.EXCEL.getCode().equalsIgnoreCase( type ) ) {
            	renderExcelReport( res, jrProcessor, "popular_stores_to_visit_report_" 
            			+ dateFrom.toString( "ddMMMyyyy" ) + "_to_" + dateTo.toString( "ddMMMyyyy" ) );
            }
    	}
    	finally {
    		IOUtils.closeQuietly(logo);
    	}
    }

    @RequestMapping(value="/points/expire")
    public ModelAndView viewPointsExpireReportpage() {
    	return new ModelAndView("report/points/expire")
    		.addObject(ATTR_REPORT_TYPE, ReportType.values());
    }
    
    @RequestMapping(value = "/member/print", method=RequestMethod.GET)
    public void exportMembersList(
            @RequestParam(value="segment") Integer segment,
            @RequestParam(value="memberType") String memberType,
            @RequestParam(value="searchType", required=false) String searchType,
            @RequestParam(value="searchValue", required=false) String searchValue,
            @RequestParam(value="status", required=false) String status,
            @RequestParam(value="store", required=false) String store,
            HttpServletResponse response) throws Exception {
        
        MemberSearchDto searchDto = new MemberSearchDto();
        if (searchType.equalsIgnoreCase("accountId"))
            searchDto.setAccountId(searchValue);
        else if (searchType.equalsIgnoreCase("contact"))
            searchDto.setContact(searchValue);
        else if (searchType.equalsIgnoreCase("email"))
            searchDto.setEmail(searchValue);
        else if (searchType.equalsIgnoreCase("ktpId"))
            searchDto.setKtpId(searchValue);
        else if (searchType.equalsIgnoreCase("name"))
            searchDto.setName(searchValue);
        else if (searchType.equalsIgnoreCase("businessName"))
            searchDto.setBusinessName(searchValue);
        
        if (store != null) {
            searchDto.setRegisteredStore(store);
        }
        
        JRProcessor jrProcessor = memberListDocument.createJrProcessor(searchDto, segment, memberType, status);
        
        if (memberType.equalsIgnoreCase("MTYP001")) {
            renderExcelReport(response, jrProcessor, "Member Individual List Document (" + LocalDate.now() +")");
        } else if (memberType.equalsIgnoreCase("MTYP002")) {
            renderExcelReport(response, jrProcessor,
                    "Employee List Document (" + LocalDate.now() + ")");
        } else if (memberType.equalsIgnoreCase("MTYP003")) {
            renderExcelReport(response, jrProcessor, "Member Professional List Document (" + LocalDate.now() +")");
        }
        
    }
}
