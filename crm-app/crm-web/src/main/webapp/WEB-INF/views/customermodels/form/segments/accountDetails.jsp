<%@ include file="../../../common/taglibs.jsp" %>


  <!-- Account Details -->
  <fieldset class="container-fluid pull-left">
    <legend><spring:message code="label_field_accountdetails" /></legend>

    <div class="pull-left">
      <div>
        <div class="control-group">
          <label for="accountId" class="control-label col-sm-2">
              <c:if test="${isEmployee}">
                <b class="required">*</b>
              </c:if>
              <spring:message code="label_com_transretail_crm_entity_customermodel_accountnumber" />
          </label>
          <div class="controls">
            <c:choose>
                <c:when test="${isEmployee}">
                    <form:input path="accountId" id="accountId" maxlength="12"/>
                </c:when>
                <c:otherwise>
                    <span class="accountId label col-sm-10"><spring:message code="msg_value_to_be_generated" /> <![CDATA[&nbsp;]]></span>
                </c:otherwise>
            </c:choose>
          </div>
        </div>
        <c:if test="${!isEmployee}">
        <div class="control-group">
          <label for="accountId" class="control-label col-sm-2"><spring:message code="label_com_transretail_crm_entity_customermodel_memberbarcode" /></label>
          <div class="controls">
            <span class="label col-sm-10" id="memberBarcode"><![CDATA[&nbsp;]]></span>
          </div>
        </div>
        </c:if>

        <div id="parentIdGroup" class="control-group hidden">
          <label for="parent" class="control-label"> <spring:message code="label_com_transretail_crm_entity_customermodel_parent" /></label>
          <div class="controls"><form:input path="parent" cssClass="hidden" /></div>
        </div>

        <div id="memberTypeStatic" class="control-group">
          <label class="control-label"> <spring:message code="label_com_transretail_crm_entity_customermodel_membertype" /></label>
          <div class="controls"><span id="contentMemberType" class="label"></span></div>
        </div>

        <div id="memberTypeSelect" class="control-group memberTypeSelect">
          <div class="contentInfo">
            <label for="memberType" class="control-label col-sm-2"><spring:message code="label_com_transretail_crm_entity_customermodel_membertype" /></label>
            <div class="controls" id="mainPaneMemberType">
              <form:select items="${memberType}" itemValue="code" itemLabel="description" path="memberType" multiple="false" cssClass="form-control" />
            </div>
          </div>
        </div>

        <div class="">
          <c:if test="${isEmployee}">
            <div id="employeeTypeSelect" class="control-group">
              <div class="contentInfo">
                <label for="employeeType" class="control-label col-sm-2"><spring:message
                    code="emp_prop_emptype" /></label>
                <div class="controls">
                  <form:select items="${employeeType}" itemValue="code"
                    itemLabel="description" path="empType" multiple="false"
                    cssClass="form-control" id="employeeType" />
                </div>
              </div>
            </div>
          </c:if>
          <c:if test="${isEmployee}">
            <div class="employeeDetails">
              <div class="form-group">
                <label for="status" class="case-cap"></label>
                <div class="checkbox controls" style="margin-left: 170px;">
                  <input type="checkbox" name="accountStatus" value="TERMINATED"
                    id="accountStatus"> <label class="case-cap"><spring:message
                      code="label_com_transretail_crm_entity_customermodel_terminated" /></label>
                </div>
              </div>
            </div>
          </c:if>

            <div class="employeeDetails" id="accountEnabled">
              <div class="form-group">
                <label for="status" class="case-cap"></label>
                <div class="checkbox controls" style="margin-left: 170px;">
                  <input type="checkbox" name="enabled"> <label
                    class="case-cap"><spring:message
                      code="label_com_transretail_crm_entity_customermodel_portal_enabled" /></label>
                </div>
              </div>
            </div>
        </div>
        <div class="control-group">
          <div class="contentInfo">
            <c:choose>
              <c:when test="${isEmployee}">
                <label class="control-label"><spring:message
                    code="label_com_transretail_crm_entity_customermodel_assignedstore" /></label>
              </c:when>
              <c:otherwise>
                <label for="registeredStore" class="control-label"> <spring:message
                    code="member_prop_prefstore" /></label>
              </c:otherwise>
            </c:choose>
            <div class="controls">
              <%--<span class="label col-sm-10" id="content_registeredStoreStatic"></span>--%>
                        <c:choose>
                          <c:when test="${isEmployee}">
                            <select name="registeredStore" id="content_registeredStore"
                            data-url-store="<c:url value="/employeemodels/member/store/" />"></select>
                          </c:when>
                          <c:otherwise>
                            <select name="registeredStore" id="content_registeredStore"
                            data-url-store="<c:url value="/customermodels/member/store/" />"></select>
                          </c:otherwise>
                        </c:choose>

              <%-- <c:if test="${preferredStore.size() > 1}"><form:select path="registeredStore" items="${preferredStore}" itemLabel="name" itemValue="code" cssClass="form-control" /></c:if>
              <c:if test="${preferredStore.size() == 1}">
                <span class="label col-sm-10"><c:out value="${preferredStore[0].name}" /></span>
                <select name="registeredStore" id="registeredStore" data-storelength="${preferredStore.size()}">
                  <option value="${preferredStore[0].code}" selected="selected"><c:out value="${preferredStore[0].name}" /></option>
                </select>
              </c:if> --%>
            </div>
          </div>
        </div>
      </div>

      <div class="space-top-01">
        <div class="professionalDetails">
          <hr />
          <div class="control-group">
            <label for="businessType" class="control-label"> <spring:message code="label_com_transretail_crm_entity_customermodel_businesstype" /></label>
            <div class="controls">
              <form:select id="customerGroup" cssClass="customerGroup" path="professionalProfile.customerGroup" items="${customerGroups}" itemLabel="description" itemValue="code" />
            </div>
            <label for="businessType" class="control-label"></label>
            <div class="controls">
              <select name="professionalProfile.customerSegmentation" id="customerSegmentation"></select>
            </div>
          </div>
          <div class="control-group">
            <spring:message code="label_com_transretail_crm_entity_customermodel_businessname" var="businessName" />
            <label for="professionalProfile.businessName" class="control-label"><b class="required">*</b>
            <c:out value="${businessName}" />:</label>
            <div class="controls">
              <input type="text" name="professionalProfile.businessName" placeholder="${businessName}" />
            </div>
          </div>
          <div class="control-group professionalDetails">
            <spring:message code="label_com_transretail_crm_entity_customermodel_businesslicense" var="businessLicense" />
            <label for="businessLicense" class="control-label"><c:out value="${businessLicense}" /></label>
            <div class="controls">
              <form:input path="professionalProfile.businessLicense" placeholder="${businessLicense}" />
            </div>
          </div>
          <jsp:include page="../npwppane.jsp" />
        </div>
      </div>
      
      <div class="control-group">
      	<label for="accountId" class="control-label col-sm-2"> <spring:message
            code="label_member_created_by" />
        </label>
        <div class="controls">
          <span id="createdBy" class="label"></span>
        </div>
      </div>
      
      <c:if test="${not isEmployee}">
      <div class="control-group">
        <label for="accountId" class="control-label col-sm-2"> <spring:message
            code="label_member_registered_date" />
        </label>
        <div class="controls">
          <span id="registeredDate" class="label"></span>
        </div>
      </div>
      <div class="control-group">
        <label for="accountId" class="control-label col-sm-2"> <spring:message
            code="label_member_loyalty_card_issue_date" />
        </label>
        <div class="controls">
          <span id="loyaltyCardIssued" class="label"></span>
        </div>
      </div>
      <div class="control-group">
        <label for="accountId" class="control-label col-sm-2"> <spring:message
            code="label_member_loyalty_card_expiry_date" />
        </label>
        <div class="controls">
          <span id="loyaltyCardExpire" class="label"></span>
        </div>
      </div>
      </c:if>
    </div>

    <div class="clearfix"></div>

  </fieldset>