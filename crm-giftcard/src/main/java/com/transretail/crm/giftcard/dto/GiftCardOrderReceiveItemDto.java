package com.transretail.crm.giftcard.dto;


import org.joda.time.LocalDate;

/**
 *
 */
public class GiftCardOrderReceiveItemDto {
	public String productCode;
	public String productDesc;
	public String expiryDate;
	public Long quantity;
	public Long startingSeries;
	public Long endingSeries;
	public String boxNo;
	
	public GiftCardOrderReceiveItemDto() {
		
	}
	
	public GiftCardOrderReceiveItemDto(String productCode, String productDesc,
                                       Long quantity, Long startingSeries, Long endingSeries, String boxNo, LocalDate expiryDate) {
		this.productCode = productCode;
		this.productDesc = productDesc;
		this.quantity = quantity;
		this.startingSeries = startingSeries;
		this.endingSeries = endingSeries;
		this.boxNo = boxNo;
		this.expiryDate = expiryDate == null ? "" : expiryDate.toString("dd-MMM-yyyy");
	}

    public String getProductCode() {
	return productCode;
    }

    public String getProductDesc() {
	return productDesc;
    }
	
	
	public Long getQuantity() {
		return quantity;
	}
	public void setQuantity(Long quantity) {
		this.quantity = quantity;
	}
	
	public Long getStartingSeries() {
		return startingSeries;
	}
	public void setStartingSeries(Long startingSeries) {
		this.startingSeries = startingSeries;
	}
	public Long getEndingSeries() {
		return endingSeries;
	}
	public void setEndingSeries(Long endingSeries) {
		this.endingSeries = endingSeries;
	}
	public String getBoxNo() {
		return boxNo;
	}
	public void setBoxNo(String boxNo) {
		this.boxNo = boxNo;
	}

    public String getExpiryDate() {
        return expiryDate;
    }

    public void setExpiryDate(String expiryDate) {
        this.expiryDate = expiryDate;
    }
}
