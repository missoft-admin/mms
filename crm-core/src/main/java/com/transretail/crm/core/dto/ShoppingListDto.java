package com.transretail.crm.core.dto;

public class ShoppingListDto {
	private String productId;
	private String productName;
	private Integer totalItemsBought;
	private Integer totalTransactions;
	private Double average;
	
	public ShoppingListDto() {}
	
	public ShoppingListDto(String productId, String productName, 
			Long totalTransactions, Double totalItemsBought) {
		this.productId = productId;
		this.productName = productName;
		this.totalItemsBought = totalItemsBought.intValue();
		this.totalTransactions = totalTransactions.intValue();
		this.average = totalItemsBought / totalTransactions.doubleValue();
	}
	
	public ShoppingListDto(String productName, Double average) {
		this.productName = productName;
		this.average = average;
	}

	public String getProductId() {
		return productId;
	}

	public void setProductId(String productId) {
		this.productId = productId;
	}

	public String getProductName() {
		return productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}

	public Integer getTotalItemsBought() {
		return totalItemsBought;
	}

	public void setTotalItemsBought(Integer totalItemsBought) {
		this.totalItemsBought = totalItemsBought;
	}

	public Integer getTotalTransactions() {
		return totalTransactions;
	}

	public void setTotalTransactions(Integer totalTransactions) {
		this.totalTransactions = totalTransactions;
	}

	public Double getAverage() {
		return average;
	}

	public void setAverage(Double average) {
		this.average = average;
	}
}
