<%@ include file="../../common/taglibs.jsp"%>

<style type="text/css">
<!--
#productTable tr td:last-child {
	border-left: 0px;
}

#addProductBtn.btn-small {
	margin-bottom: 5px;
}

table select, textarea, table input[type="text"], input[type="password"],
	input[type="datetime"], input[type="datetime-local"], input[type="date"],
	input[type="month"], input[type="time"], input[type="week"], input[type="number"],
	input[type="email"], input[type="url"], input[type="search"], input[type="tel"],
	input[type="color"], .uneditable-input {
	margin-bottom: 0px;
	margin-top: 0px;
}
-->
</style>
<script src="<spring:url value="/js/common/typeaheadutil.js" />"></script>



<c:set var="ENCTYPE" value="application/x-www-form-urlencoded" />
<c:set var="freeSo" value="${orderForm.orderType == 'YEARLY_DISCOUNT' || orderForm.orderType == 'VOUCHER' }" />

<div class="modal-dialog">
	<div class="modal-content">

		<div class="modal-header">
			<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
			<h4>
				<spring:message code="sales_order" />
				History
			</h4>
		</div>

		<div class="modal-body">

			<div id="contentError" class="hide alert alert-error">
				<button type="button" class="close" onclick="$(this).parent().hide();">&times;</button>
			</div>
			<div class="pull-left mb20" style="width: 94%; overflow: auto;">
				<table id="productTable" class="table table-condensed table-compressed table-striped">
					<thead>
						<tr>
							<th>No.</th>
							<th>Status Time</th>
							<th>Status Updater</th>
							<th>Status</th>
						</tr>
					</thead>
					<tbody>
						<c:forEach var="item" items="${histories}" varStatus="status">
							<tr data-index="${status.index}">
								<td>${status.index+1}</td>
								<td><fmt:formatDate value="${item.createdDate}" pattern="dd-MM-yyyy HH:mm"/></td>
								<td>${item.createUser}</td>
								<td>${fn:replace(item.status,"_"," ")}</td>
							</tr>
						</c:forEach>
					</tbody>
				</table>
			</div>

		</div>
		<div class="modal-footer">
			<button type="button" id="orderCancel" class="btn btn-default" data-dismiss="modal">
				<spring:message code="label_cancel" />
			</button>
		</div>
	</div>
</div>
