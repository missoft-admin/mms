package com.transretail.crm.core.entity.lookup;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import org.apache.commons.lang3.BooleanUtils;
import org.hibernate.annotations.Type;

//TODO Change name
@Entity
@Table(name = "CRM_REF_MEMBERTYPE")
public class MemberType implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
    @Id
    @Column(name = "CODE")
    private String code;
    
    @Column(name = "TYPE_NAME")
    private String name;

    @Column(name = "COMPANY", length = 1)
    @Type(type = "yes_no")
    private Boolean company = Boolean.FALSE;
    
    public MemberType() {}

    public MemberType(String code) {
        this.code = code;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

	public Boolean getCompany() {
		return company;
	}

	public void setCompany(Boolean company) {
		this.company = BooleanUtils.toBoolean(company);
	}
}
