package com.transretail.crm.web.controller;

import java.beans.PropertyEditorSupport;
import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.StringUtils;
import org.joda.time.LocalDate;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.transretail.crm.common.reporting.jasper.JRProcessor;
import com.transretail.crm.common.service.dto.response.ResultList;
import com.transretail.crm.core.entity.enums.ReportType;
import com.transretail.crm.core.service.CodePropertiesService;
import com.transretail.crm.core.service.LookupService;
import com.transretail.crm.giftcard.dto.SalesOrderDto;
import com.transretail.crm.giftcard.dto.SalesOrderPaymentInfoDto;
import com.transretail.crm.giftcard.dto.SalesOrderPaymentSearchDto;
import com.transretail.crm.giftcard.dto.SalesOrderPaymentsDto;
import com.transretail.crm.giftcard.entity.support.PaymentInfoStatus;
import com.transretail.crm.giftcard.entity.support.SalesOrderStatus;
import com.transretail.crm.giftcard.service.DiscountSchemeService;
import com.transretail.crm.giftcard.service.DiscountSchemeService.DiscountDto;
import com.transretail.crm.giftcard.service.SalesOrderPaymentService;
import com.transretail.crm.giftcard.service.SalesOrderService;
import com.transretail.crm.web.controller.util.ControllerResponse;

@Controller
@RequestMapping("/giftcard/salesorder/payments")
public class SalesOrderPaymentController extends AbstractReportsController {
	@Autowired
	private SalesOrderPaymentService paymentService;
	@Autowired
	private LookupService lookupService;
	@Autowired
	private SalesOrderService orderService;
	@Autowired
	private CodePropertiesService codePropertiesService;
	@Autowired
	private MessageSource messageSource;
	@Autowired
	private DiscountSchemeService discountSchemeService;
	
	private static final DateTimeFormatter FORMATTER = DateTimeFormat.forPattern("dd-MM-yyyy");
	
	@InitBinder
	void initBinder(WebDataBinder binder) {
		PropertyEditorSupport editor = new PropertyEditorSupport() {
			@Override
			public void setAsText(String text) throws IllegalArgumentException {
				if(StringUtils.isNotBlank(text))
					setValue(FORMATTER.parseLocalDate(text));
				else
					setValue(null);
			}

			@Override
			public String getAsText() throws IllegalArgumentException {
				if (getValue() != null)
					return FORMATTER.print((LocalDate) getValue());
				else
					return "";
			}
		};
		binder.registerCustomEditor(LocalDate.class, editor);
	}
	
	@RequestMapping
    public String list(Model uiModel) {
		uiModel.addAttribute("searchCriteria", SearchCriteria.values());
		uiModel.addAttribute("status", SalesOrderPaymentService.UI_STATUS);
		uiModel.addAttribute("reportTypes", ReportType.values());
		
    	return "giftcard/so/payments/list";
    }
	@RequestMapping(value = "/search", method = RequestMethod.POST)
    public @ResponseBody ResultList<SalesOrderPaymentInfoDto> search(@RequestBody SalesOrderPaymentSearchDto filterDto) {
    	return paymentService.getPayments(filterDto);
    }
	@RequestMapping(value = "/form/{id}", method = RequestMethod.GET, produces = "text/html")
    public String showPaymentForm(
    		@PathVariable("id") String id,
    		Model uiModel) {
		SalesOrderPaymentsDto payments = paymentService.getPaymentsDto(Long.valueOf(id));
		
		payments.setStatus(PaymentInfoStatus.DRAFT);
		populateForm(payments, uiModel);
		
		SalesOrderDto orderDto = orderService.getOrder(Long.valueOf(id));
		DiscountDto disc = discountSchemeService.getDiscountBeforeApproval(orderDto.getCustomerId(), orderDto.getTotalSoAmt(), orderDto.getOrderDate(), orderDto.getDiscType());
		orderDto.setDiscountVal(disc == null? BigDecimal.ZERO: disc.getDiscount());
		uiModel.addAttribute("orderForm", orderDto);
    	return "giftcard/so/payments/form";
    }
	
	@RequestMapping(value = "/approvalform/{id}", method = RequestMethod.GET, produces = "text/html")
    public String showPaymentApprovalForm(
    		@PathVariable("id") String id,
    		Model uiModel) {
		uiModel.addAttribute("paymentForm", paymentService.getPaymentsInfoDto(Long.valueOf(id)));
    	return "giftcard/so/payments/approvalform";
    }
	
	private void populateForm(SalesOrderPaymentsDto paymentsDto, Model uiModel) {
		uiModel.addAttribute("paymentForm", paymentsDto);
		uiModel.addAttribute("paymentTypes", lookupService.getActiveDetailsByHeaderCode(codePropertiesService.getHeaderGcSoPaymentMethod()));
	}
	
	@RequestMapping(value = "/save", method = RequestMethod.POST, produces = "application/json; charset=utf-8")
	@ResponseBody
	public ControllerResponse ajaxSavePayments(
			@ModelAttribute(value = "paymentForm") SalesOrderPaymentsDto paymentForm,
			BindingResult result, HttpServletRequest request) {

		ControllerResponse theResponse = new ControllerResponse();
		theResponse.setSuccess(true);
		
		validate(paymentForm, result);
		
		if (result.hasErrors()) {
			theResponse.setSuccess(false);
			theResponse.setResult(result.getAllErrors());
			return theResponse;
		}

		if (theResponse.isSuccess()) {
			paymentService.savePayments(paymentForm);
		}

		return theResponse;
	}
	
	private String getMessage(String inMsgCode, Object[] inArgMsgCodes) {
        if (inArgMsgCodes != null) {
            for (int i = 0; i < inArgMsgCodes.length; i++) {
                inArgMsgCodes[i] =
                    messageSource.getMessage(inArgMsgCodes[i].toString().toLowerCase(), null, LocaleContextHolder.getLocale());
            }
        }
        return messageSource.getMessage(inMsgCode, inArgMsgCodes, "Info given is invalid.", LocaleContextHolder.getLocale());
    }
	
	public void validate(SalesOrderPaymentsDto paymentForm, BindingResult result) {
		for(SalesOrderPaymentInfoDto payment: paymentForm.getPayments()) {
			boolean isValid = true;
			if(payment.getPaymentType() == null) {
				result.reject(getMessage("propkey_msg_notempty", new Object[]{"sales_order_payment_type"}));
				isValid = false;
			}
			if(payment.getPaymentDate() == null) {
				result.reject(getMessage("propkey_msg_notempty", new Object[]{"sales_order_payment_date"}));
				isValid = false;
			}
			if(payment.getPaymentAmount() == null) {
				result.reject(getMessage("propkey_msg_notempty", new Object[]{"sales_order_payment_amount"}));
				isValid = false;
			}
			if(!isValid) break;
		}
	}
	
	@RequestMapping(value = "/approve/{ids}", method = RequestMethod.POST, produces = "application/json; charset=utf-8")
	@ResponseBody
	public ControllerResponse ajaxProcessPayments(
			@PathVariable("ids") Long[] ids, HttpServletRequest request) {
		ControllerResponse theResponse = new ControllerResponse();
		theResponse.setSuccess(true);
		paymentService.verifyPayments(ids);
		paymentService.generateAcctForPeoplesoft(ids);
		paymentService.approvePayments(ids);
		return theResponse;
	}
	
	@RequestMapping(value = "/reject/{orderId}", method = RequestMethod.POST, produces = "application/json; charset=utf-8")
	@ResponseBody
	public ControllerResponse ajaxProcessPayments(
			@PathVariable("orderId") Long orderId, HttpServletRequest request) {
		ControllerResponse theResponse = new ControllerResponse();
		theResponse.setSuccess(true);
		paymentService.updateStatus(orderId, PaymentInfoStatus.DRAFT);
		orderService.approveOrder(orderId, SalesOrderStatus.DRAFT);
		return theResponse;
	}
	
	@RequestMapping(value = "/save/{status}", method = RequestMethod.POST, produces = "application/json; charset=utf-8")
	@ResponseBody
	public ControllerResponse ajaxProcessPayments(
			@ModelAttribute(value = "paymentForm") SalesOrderPaymentInfoDto paymentForm,
			@PathVariable("status") String status,
			BindingResult result, HttpServletRequest request) {

		ControllerResponse theResponse = new ControllerResponse();
		theResponse.setSuccess(true);
		
		if (result.hasErrors()) {
			theResponse.setSuccess(false);
			theResponse.setResult(result.getAllErrors());
			return theResponse;
		}

		if (theResponse.isSuccess()) {
			PaymentInfoStatus stat = PaymentInfoStatus.valueOf(status);
			if(stat.compareTo(PaymentInfoStatus.APPROVED) == 0)
				paymentService.processPayment(paymentForm.getId(), stat);
			else if(stat.compareTo(PaymentInfoStatus.REJECTED) == 0) {
				paymentService.updateStatus(paymentForm.getOrderId(), PaymentInfoStatus.DRAFT);
				orderService.approveOrder(paymentForm.getOrderId(), SalesOrderStatus.DRAFT);
			}
		}

		return theResponse;
	}
	
	public static enum SearchCriteria {
		ORDERNO("salesOrderNo"),
		CUSTOMER("customer");
		
		private final String field;
		
		private SearchCriteria(String field) {
			this.field = field;
		}

		public String getField() {
			return field;
		}

	}
	
	@RequestMapping(value = "/printall", method=RequestMethod.GET)
    public void print(
            @RequestParam(value="reportType") String reportType,
            @RequestParam(value="searchType", required=false) String searchType,
            @RequestParam(value="searchValue", required=false) String searchValue,
            @RequestParam(value="dateFrom", required=false) String dateFrom,
            @RequestParam(value="dateTo", required=false) String dateTo,
            @RequestParam(value="status", required=false) String status,
            HttpServletResponse response) throws Exception {
	    
	    SalesOrderPaymentSearchDto searchDto = new SalesOrderPaymentSearchDto();
	    if (searchType != null && searchValue != null) {
    	    if (searchType.equalsIgnoreCase("customer"))
                searchDto.setCustomer(searchValue);
            else if (searchType.equalsIgnoreCase("salesOrderNo"))
                searchDto.setSalesOrderNo(searchValue);
	    }
	    if (dateFrom != null) {
	        searchDto.setPaymentDateFrom(FORMATTER.parseLocalDate(dateFrom));
	    }
	    
        if (dateTo != null) {
            searchDto.setPaymentDateTo(FORMATTER.parseLocalDate(dateTo));
        }

        if (status != null) {
            searchDto.setStatus(status);
        }
        
        JRProcessor jrProcessor = paymentService.createJrProcessor(searchDto);
        if(ReportType.PDF.getCode().equals(reportType))
            renderReport(response, jrProcessor);
        else if(ReportType.EXCEL.getCode().equals(reportType))
            renderExcelReport(response, jrProcessor, getMessage("sales_order_so_payment_info", null));
    }
}
