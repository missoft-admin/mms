package com.transretail.crm.core.entity.embeddable;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import org.hibernate.envers.Audited;

import com.transretail.crm.core.entity.support.CustomAuditableEntity;

@Entity
@Table(name="CRM_ADDRESS")
@Audited
public class Address extends CustomAuditableEntity<Long> implements Serializable {
	@Column(name="STREET", length = 4000)
	private String street;
	@Column(name="STR_NO")
	private String streetNumber;
	@Column(name="BLOCK")
	private String block;
	@Column(name="BUILDING")
	private String building;
	@Column(name="FLOOR")
	private Integer floor;
	@Column(name="ROOM")
	private String room;
	@Column(name="VILLAGE")
	private String village;
	@Column(name="SUBDISTRICT")
	private String subdistrict;
	@Column(name="DISTRICT")
	private String district;
	@Column(name="CITY")
	private String city;
	@Column(name="PROVINCE", length = 50)
	private String province;
	@Column(name="POST_CODE")
	private String postCode;
	@Column(name="KM")
	private Integer km;
	@Column(name="RT")
	private String rt;
	@Column(name="RW")
	private String rw;

    public void setId(Long id) {
        super.setId(id);
    }
	
	public String getStreet() {
		return street;
	}
	public void setStreet(String street) {
		this.street = street;
	}
	public String getBlock() {
		return block;
	}
	public void setBlock(String block) {
		this.block = block;
	}
	public String getBuilding() {
		return building;
	}
	public void setBuilding(String building) {
		this.building = building;
	}
	public Integer getFloor() {
		return floor;
	}
	public void setFloor(Integer floor) {
		this.floor = floor;
	}
	public String getRoom() {
		return room;
	}
	public void setRoom(String room) {
		this.room = room;
	}
	public String getVillage() {
		return village;
	}
	public void setVillage(String village) {
		this.village = village;
	}
	public String getSubdistrict() {
		return subdistrict;
	}
	public void setSubdistrict(String subdistrict) {
		this.subdistrict = subdistrict;
	}
	public String getDistrict() {
		return district;
	}
	public void setDistrict(String district) {
		this.district = district;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public String getProvince() {
		return province;
	}
	public void setProvince(String province) {
		this.province = province;
	}
	public String getPostCode() {
		return postCode;
	}
	public void setPostCode(String postCode) {
		this.postCode = postCode;
	}
	public String getStreetNumber() {
		return streetNumber;
	}
	public void setStreetNumber(String streetNumber) {
		this.streetNumber = streetNumber;
	}
	public Integer getKm() {
		return km;
	}
	public void setKm(Integer km) {
		this.km = km;
	}
	public String getRt() {
		return rt;
	}
	public void setRt(String rt) {
		this.rt = rt;
	}
	public String getRw() {
		return rw;
	}
	public void setRw(String rw) {
		this.rw = rw;
	}
	
}
