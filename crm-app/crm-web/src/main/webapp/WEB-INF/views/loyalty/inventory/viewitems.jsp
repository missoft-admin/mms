<%@ include file="../../common/taglibs.jsp" %>

<div class="modal-dialog">
  <div class="modal-content">
 
    <div class="modal-body">
    
    <div id="inventoryParams" class="hide">
      <input type="hidden" name="manufactureOrder" value="${searchDto.manufactureOrder}" />
      <input type="hidden" name="manufactureOrderId" value="${searchDto.manufactureOrderId}" />
      <input type="hidden" name="manufactureOrderTierCode" value="${searchDto.manufactureOrderTierCode}" />
      <input type="hidden" name="locationCode" value="${searchDto.locationCode}" />
      <input type="hidden" name="transferToCode" value="${searchDto.transferToCode}" />
      <input type="hidden" name="newTransferToCode" value="${searchDto.newTransferToCode}" />
      <input type="hidden" name="status" value="${searchDto.status}" />
      <input type="hidden" name="referenceNo" value="${searchDto.referenceNo}" />
      <input type="hidden" name="forBurning" value="${searchDto.forBurning}" />
    </div>
    
    <c:if test="${searchDto.newTransferToCode != 'null'}">
    <spring:message code="changealloc_forapproval_msg" arguments="${searchDto.transferToCode};${searchDto.transferTo};${searchDto.newTransferToCode};${searchDto.newTransferTo}" argumentSeparator=";" /><br/><br/>
    </c:if>
    
    <div id="content_inventoryItemsList">
    
      <div id="list_inventoryItems" 
        data-url="<c:url value="/loyaltycardinventory/searchitems" />"
        data-hdr-product="<spring:message code="mo_tier_prop_name" />"
        data-hdr-barcode="<spring:message code="inventory_prop_barcode" />"
        data-hdr-series="<spring:message code="inventory_prop_series" />"></div>
    
    </div>
    
    
    </div>
    <div class="modal-footer">
      <button type="button" id="calendarCancel" class="btn" data-dismiss="modal"><spring:message code="label_cancel"/></button>
    </div>
  </div>
</div>


<script src="<spring:url value="/js/viewspecific/loyalty/viewitems.js"/>"></script>