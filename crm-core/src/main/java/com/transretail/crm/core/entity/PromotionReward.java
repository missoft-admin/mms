package com.transretail.crm.core.entity;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.Type;
import org.hibernate.envers.Audited;

import com.transretail.crm.core.entity.lookup.LookupDetail;
import com.transretail.crm.core.entity.support.CustomAuditableEntity;

@Audited
@Table(name = "CRM_PROMOTION_REWARD")
@Entity
public class PromotionReward extends CustomAuditableEntity<Long> implements Serializable {

    private static final long serialVersionUID = 1L;


    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "PROMO_ID")
    private Promotion promotion;

    @Column(name="REMARK", length = 1000)
    private String remark;

    @Column(name="MIN_AMOUNT")
    private BigDecimal minAmount;

    @Column(name="MAX_AMOUNT")
    private BigDecimal maxAmount;

    @Column(name="MIN_QTY")
    private Long minQty;

    @Column(name="MAX_QTY")
    private Long maxQty;

    @Column(name="BASE_AMOUNT")
    private BigDecimal baseAmount;

    @Column(name="BASE_FACTOR", precision = 15)
    private Double baseFactor;

    @Column(name="MAX_BONUS")
    private BigDecimal maxBonus;

    @Column(name = "DISCOUNT", precision = 15)
    private Double discount;

    //header.reward.type=MKT005, 
    //reward.type.points.code=RWRD001, reward.type.discount.code=RWRD002 ; reward.type.exchangeitem.code=RWRD005 ;
    @ManyToOne
    @JoinColumn(name="REWARD_TYPE")
    private LookupDetail rewardType;
    
    @Column(name = "FIXED", length = 1)
    @Type(type = "yes_no")
    private Boolean fixed;


    //PLU_ID
    @Column(name="PRODUCT_CODE", length = 1000)
    private String productCode;



	public Promotion getPromotion() {
		return promotion;
	}

	public void setPromotion(Promotion promotion) {
		this.promotion = promotion;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	public BigDecimal getMinAmount() {
		return minAmount;
	}

	public void setMinAmount(BigDecimal minAmount) {
		this.minAmount = minAmount;
	}

	public BigDecimal getMaxAmount() {
		return maxAmount;
	}

	public void setMaxAmount(BigDecimal maxAmount) {
		this.maxAmount = maxAmount;
	}

	public Long getMinQty() {
		return minQty;
	}

	public void setMinQty(Long minQty) {
		this.minQty = minQty;
	}

	public Long getMaxQty() {
		return maxQty;
	}

	public void setMaxQty(Long maxQty) {
		this.maxQty = maxQty;
	}

	public BigDecimal getBaseAmount() {
		return baseAmount;
	}

	public void setBaseAmount(BigDecimal baseAmount) {
		this.baseAmount = baseAmount;
	}

	public Double getBaseFactor() {
		return baseFactor;
	}

	public void setBaseFactor(Double baseFactor) {
		this.baseFactor = baseFactor;
	}

	public BigDecimal getMaxBonus() {
		return maxBonus;
	}

	public void setMaxBonus(BigDecimal maxBonus) {
		this.maxBonus = maxBonus;
	}

	public Double getDiscount() {
		return discount;
	}

	public void setDiscount(Double discount) {
		this.discount = discount;
	}

	public LookupDetail getRewardType() {
		return rewardType;
	}

	public void setRewardType(LookupDetail rewardType) {
		this.rewardType = rewardType;
	}

	public Boolean getFixed() {
		return fixed;
	}

	public void setFixed(Boolean fixed) {
		this.fixed = fixed;
	}

    public String getProductCode() {
        return productCode;
    }

    public void setProductCode(String productCode) {
        this.productCode = productCode;
    }
}
