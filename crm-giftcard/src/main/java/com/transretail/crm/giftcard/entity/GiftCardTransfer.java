package com.transretail.crm.giftcard.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import java.io.Serializable;

import com.transretail.crm.core.entity.support.CustomAuditableEntity;

/**
 *
 */
@Entity
@Table(name = "CRM_GC_TXNTRANSFER")
public class GiftCardTransfer extends CustomAuditableEntity<Long> implements Serializable {
    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "GC_INVENTORY")
    private GiftCardInventory item;
    @Column(name = "SERIES", nullable = false, length = 16)
    private String series;
    /**
     * Must be equal to GiftCardInventory#productName
     */
    @Column(name = "PRODUCT_NAME", length = 100, nullable = false)
    private String productName;

    public GiftCardInventory getItem() {
        return item;
    }

    public void setItem(GiftCardInventory item) {
        this.item = item;
    }

    public String getSeries() {
        return series;
    }

    public void setSeries(String series) {
        this.series = series;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }
}
