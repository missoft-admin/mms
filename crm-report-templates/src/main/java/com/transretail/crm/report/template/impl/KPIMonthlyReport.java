package com.transretail.crm.report.template.impl;

import com.google.common.collect.Iterables;
import com.google.common.collect.Lists;
import com.google.common.collect.Sets;
import com.mysema.query.jpa.impl.JPAQuery;
import com.mysema.query.support.Expressions;
import com.mysema.query.types.Projections;
import com.mysema.query.types.expr.StringExpression;
import com.transretail.crm.common.reporting.engine.data.ReportDataSource;
import com.transretail.crm.common.reporting.jasper.ClasspathFileResolver;
import com.transretail.crm.common.reporting.jasper.DefaultJRProcessor;
import com.transretail.crm.common.reporting.jasper.JRProcessor;
import com.transretail.crm.core.entity.QPosTransaction;
import com.transretail.crm.core.entity.QPosTxItem;
import com.transretail.crm.core.entity.enums.ReportType;
import com.transretail.crm.report.template.FilterField;
import com.transretail.crm.report.template.FilterField.DateTypeOption;
import com.transretail.crm.report.template.ReportDateFormat;
import com.transretail.crm.report.template.ValidatableReportTemplate;
import java.math.BigDecimal;
import java.math.MathContext;
import java.math.RoundingMode;
import java.util.*;
import java.util.Map.Entry;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRField;
import org.apache.commons.lang.StringUtils;
import org.joda.time.YearMonth;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.core.NamedThreadLocal;
import org.springframework.stereotype.Service;

/**
 *
 * @author Monte Cillo Co (mco@exist.com)
 */
@Service("kpiMonthlyReport")
public class KPIMonthlyReport implements ValidatableReportTemplate {

    public static final String PERMISSION = "KPI_MONTHLY_REPORT_PERMISSION";
    public static final String JASPER = "reports/kpi-monthly-report.jrxml";
    public static final String TEMPLATE_NAME = "kpi-monthly-report";
    public static final String YEAR_PARAM = "year";

    static final String VALUE = "value";
    static final String CATEGORY = "category";
    static final String YEAR_MONTH_HANDLER = "yearMonthHandler";

    private static final ThreadLocal<ReportDataSource> localContext = new NamedThreadLocal("kpiMonthlyReport-localContext");

    enum Category {

        CARD_HOLDER_TX_PCT,
        CARD_HOLDER_SALES_PCT,
        CARD_HOLDER_BASKER_PER_VISIT,
        NON_CARD_HOLDER_BASKER_PER_VISIT,
        CARD_HOLDER_UNIQUE_ITEM_PER_VISIT,
        NON_CARD_HOLDER_UNIQUE_ITEM_PER_VISIT
    }

    @Autowired
    private MessageSource messageSouce;
    @PersistenceContext
    private EntityManager em;

    @Override
    public List<String> validate(Map<String, String> parameters) {
        String from = parameters.get(YEAR_PARAM);

        List<String> errors = Lists.newArrayListWithExpectedSize(1);

        if (StringUtils.isBlank(from)) {
            errors.add(messageSouce.getMessage("kpi.params.year.required", null, LocaleContextHolder.getLocale()));
        }

        return errors;
    }

    @Override
    public String getName() {
        return TEMPLATE_NAME;
    }

    @Override
    public String getDescription() {
        return messageSouce.getMessage("kpi.report.title", null, LocaleContextHolder.getLocale());
    }

    @Override
    public Set<FilterField> getFilters() {
        Set<FilterField> filters = Sets.newLinkedHashSetWithExpectedSize(2);
        filters.add(FilterField.createDateField(YEAR_PARAM,
                messageSouce.getMessage("kpi.report.param.year", null, LocaleContextHolder.getLocale()),
                "YYYY", DateTypeOption.YEAR, true));
        return filters;
    }

    @Override
    public boolean canView(Set<String> permissions) {
        return permissions.contains(PERMISSION);
    }

    @Override
    public JRProcessor createJrProcessor(Map<String, String> parameters) {
        final int year = Integer.parseInt(parameters.get(YEAR_PARAM));
        Iterable<ReportBean> ds = Iterables.concat(retrieveCardHolderTransaction(year),
                retrieveCardHolderSales(year),
                retrieveCardHolderBasketSizePerVisit(year),
                retrieveNonCardHolderBasketSizePerVisit(year));

        List<ReportBean> rds = Lists.newArrayList(ds);
        DefaultJRProcessor jrProcessor = new DefaultJRProcessor(JASPER, rds);
        doResetLocalContext();
        jrProcessor.setFileResolver(new ClasspathFileResolver("reports"));
        jrProcessor.addParameter("YEAR", year);
        jrProcessor.addParameter("CATEGORY_COMPARATOR", createRowComparator());
        jrProcessor.addParameter("YEAR_MONTH_COMPARATOR", new Comparator<YearMonth>() {

            @Override
            public int compare(YearMonth o1, YearMonth o2) {
                return compareIndex(ReportDateFormat.getQueryDSLYearMonth(o1),
                        ReportDateFormat.getQueryDSLYearMonth(o2));
            }
        });
        return jrProcessor;
    }

    private void doResetLocalContext() {
        localContext.remove();
        localContext.set(null);
    }

    @Override
    public ReportType[] getReportTypes() {
        return ReportType.values();
    }

    private StringExpression constant(Category constantValue) {
        return Expressions.stringTemplate(String.format("'%s'", constantValue.name()));
    }

    private List<ReportBean> retrieveNonCardHolderBasketSizePerVisit(int year) {
        QPosTransaction qtx = QPosTransaction.posTransaction;
        QPosTxItem qti = QPosTxItem.posTxItem;
        return new JPAQuery(em).from(qtx)
                .join(qtx.orderItems, qti)
                .where(qtx.transactionDate.year().eq(year))
                .groupBy(qtx.transactionDate.yearMonth())
                .list(Projections.bean(ReportBean.class,
                                constant(Category.NON_CARD_HOLDER_BASKER_PER_VISIT).as(CATEGORY),
                                qtx.transactionDate.yearMonth().as(YEAR_MONTH_HANDLER),
                                qti.id.count().as("valueLong")));
    }

    private List<ReportBean> retrieveCardHolderBasketSizePerVisit(int year) {
        QPosTransaction qtx = QPosTransaction.posTransaction;
        QPosTxItem qti = QPosTxItem.posTxItem;

        return new JPAQuery(em).from(qtx)
                .join(qtx.orderItems, qti)
                .where(qtx.customerId.isNotNull().or(qtx.customerId.isNotEmpty()).and(qtx.transactionDate.year().eq(year)))
                .groupBy(qtx.transactionDate.yearMonth())
                .list(Projections.bean(ReportBean.class,
                                constant(Category.CARD_HOLDER_BASKER_PER_VISIT).as(CATEGORY),
                                qtx.transactionDate.yearMonth().as(YEAR_MONTH_HANDLER),
                                qti.id.count().as("valueLong")));
    }

    private List<ReportBean> retrieveCardHolderSales(int year) {
        QPosTransaction qtx = QPosTransaction.posTransaction;
        String[] POS_SALE_TX = new String[]{"SALE", "REFUND", "RETURN", "VOID"};

        Map<Integer, Double> memberWithTxMap = new JPAQuery(em).from(qtx)
                .where(qtx.customerId.isNotNull().or(qtx.customerId.isNotEmpty())
                        .and(qtx.type.in(POS_SALE_TX))
                        .and(qtx.transactionDate.year().eq(year)))
                .groupBy(qtx.transactionDate.yearMonth())
                .map(qtx.transactionDate.yearMonth(), qtx.totalAmount.sum()
                        .subtract(qtx.totalDiscount.sum())
                        .subtract(qtx.voidedDiscount.sum()));

        Map<Integer, Double> totalTxMap = new JPAQuery(em).from(qtx)
                .where(qtx.type.in(POS_SALE_TX))
                .groupBy(qtx.transactionDate.yearMonth())
                .map(qtx.transactionDate.yearMonth(), qtx.totalAmount.sum()
                        .subtract(qtx.totalDiscount.sum())
                        .subtract(qtx.voidedDiscount.sum()));
        List<ReportBean> ds = Lists.newArrayList();

        for (Entry<Integer, Double> entry : memberWithTxMap.entrySet()) {
            final Integer yearMonth = entry.getKey();
            Double count = totalTxMap.get(yearMonth);

            ReportBean reportBean = new ReportBean(Category.CARD_HOLDER_SALES_PCT.name(),
                    ReportDateFormat.getYearMonth(yearMonth),
                    (BigDecimal.valueOf(entry.getValue())
                    .divide(BigDecimal.valueOf(count), 7, RoundingMode.HALF_EVEN)
                    .multiply(BigDecimal.valueOf(100)).doubleValue()));
            ds.add(reportBean);
        }

        return ds;
    }

    private List<ReportBean> retrieveCardHolderTransaction(int year) {
        QPosTransaction qtx = QPosTransaction.posTransaction;
        QPosTxItem qti = QPosTxItem.posTxItem;

        Map<Integer, Long> memberWithTxMap = new JPAQuery(em).from(qtx)
                .where(qtx.customerId.isNotNull().or(qtx.customerId.isNotEmpty())
                        .and(qtx.transactionDate.year().eq(year)))
                .groupBy(qtx.transactionDate.yearMonth())
                .map(qtx.transactionDate.yearMonth(), qtx.count());

        qtx = new QPosTransaction("qtx2");
        Map<Integer, Long> totalTxMap = new JPAQuery(em).from(qtx)
                .where(qtx.transactionDate.year().eq(year))
                .groupBy(qtx.transactionDate.yearMonth())
                .map(qtx.transactionDate.yearMonth(), qtx.count());
        List<ReportBean> ds = Lists.newArrayList();

        for (Entry<Integer, Long> entry : memberWithTxMap.entrySet()) {
            final Integer yearMonth = entry.getKey();
            Long count = totalTxMap.get(yearMonth);

            ReportBean reportBean = new ReportBean(Category.CARD_HOLDER_TX_PCT.name(),
                    ReportDateFormat.getYearMonth(yearMonth),
                    (BigDecimal.valueOf(entry.getValue())
                    .divide(BigDecimal.valueOf(count), 7, RoundingMode.HALF_EVEN)
                    .multiply(BigDecimal.valueOf(100)).doubleValue()));
            ds.add(reportBean);
        }

        return ds;
    }

    public static int compareIndex(int x, int y) {
        return (x < y) ? -1 : ((x == y) ? 0 : 1);
    }

    private Comparator<String> createRowComparator() {
        return new Comparator<String>() {

            @Override
            public int compare(String o1, String o2) {
                return compareIndex(Category.valueOf(o1).ordinal(), Category.valueOf(o2).ordinal());
            }
        };
    }

    public static class ReportBean {

        private String category;
        private YearMonth yearMonth;
        private Double value;

        public ReportBean() {
        }

        public ReportBean(String category, YearMonth yearMonth, Double value) {
            this.category = category;
            this.yearMonth = yearMonth;
            this.value = value;
        }

        public void setCategory(String category) {
            this.category = category;
        }

        public void setYearMonth(YearMonth yearMonth) {
            this.yearMonth = yearMonth;
        }

        public void setValueLong(Long value) {
            this.value = value.doubleValue();
        }

        public void setValue(Double value) {
            this.value = value;
        }

        public void setYearMonthHandler(Integer yearMonth) {
            this.yearMonth = ReportDateFormat.getYearMonth(yearMonth);
        }

        public String getCategory() {
            return category;
        }

        public YearMonth getYearMonth() {
            return yearMonth;
        }

        public Double getValue() {
            return value;
        }
    }
}
