package com.transretail.crm.core.repo;

import org.springframework.stereotype.Repository;

import com.transretail.crm.common.repo.CrmQueryDslPredicateExecutor;
import com.transretail.crm.core.entity.MemberGroup;
import com.transretail.crm.core.repo.custom.MemberGroupRepoCustom;

@Repository
public interface MemberGroupRepo extends CrmQueryDslPredicateExecutor<MemberGroup, Long>, MemberGroupRepoCustom {
	MemberGroup findByName(String name);
}
