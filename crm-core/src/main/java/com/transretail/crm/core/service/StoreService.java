package com.transretail.crm.core.service;


import java.util.Date;
import java.util.List;
import java.util.Map;

import org.joda.time.LocalDate;

import com.transretail.crm.common.reporting.jasper.JRProcessor;
import com.transretail.crm.core.dto.PopularStoreDto;
import com.transretail.crm.core.dto.StoreDto;
import com.transretail.crm.core.entity.lookup.Store;


public interface StoreService {

    Iterable<Store> getAllStores();
    
    Map<String, String> getAllStoresHashMap();

    Map<String, String> getStoreByUserStoreLoc();
    
    Map<String, Store> getAllStoresMap();

	Store getStoreByCode(String code);

	Store findById(Integer id);

	List<StoreDto> find(String storeLike);

	List<PopularStoreDto> getPopularStores(Date startDate, Date endDate);

	JRProcessor createPopularStoresJrProcessor(LocalDate dateFrom, LocalDate dateTo);

}
