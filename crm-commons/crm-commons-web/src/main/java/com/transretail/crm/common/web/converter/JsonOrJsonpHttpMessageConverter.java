package com.transretail.crm.common.web.converter;

import java.io.IOException;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang3.StringUtils;
import org.springframework.http.HttpOutputMessage;
import org.springframework.http.converter.HttpMessageNotWritableException;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.util.Assert;
import org.springframework.web.context.request.RequestAttributes;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import com.fasterxml.jackson.databind.util.JSONPObject;

/**
 * @author Allan G. Ramirez (agramirez@exist.com)
 */
public class JsonOrJsonpHttpMessageConverter extends MappingJackson2HttpMessageConverter {
    private static final String CALLBACK_PARAMETER = "callback";

    @Override
    protected void writeInternal(Object object, HttpOutputMessage outputMessage) throws IOException, HttpMessageNotWritableException {
        RequestAttributes attrs = RequestContextHolder.getRequestAttributes();
        Assert.isInstanceOf(ServletRequestAttributes.class, attrs);
        HttpServletRequest request = ((ServletRequestAttributes) attrs).getRequest();
        String callback = request.getParameter(CALLBACK_PARAMETER);
        if (StringUtils.isNotBlank(callback)) {
            super.writeInternal(new JSONPObject(callback, object), outputMessage);
        } else {
            super.writeInternal(object, outputMessage);
        }
    }
}
