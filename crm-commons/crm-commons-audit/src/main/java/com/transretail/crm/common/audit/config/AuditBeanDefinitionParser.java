package com.transretail.crm.common.audit.config;

import org.springframework.beans.factory.support.BeanDefinitionBuilder;
import org.springframework.beans.factory.xml.AbstractSingleBeanDefinitionParser;
import org.springframework.util.StringUtils;
import org.w3c.dom.Element;

import com.transretail.crm.common.audit.AuditPostProcessor;

/**
 * @author Allan G. Ramirez (agramirez@exist.com)
 */
public class AuditBeanDefinitionParser extends AbstractSingleBeanDefinitionParser {
    private static final String ATT_SAVE_HANDLER_REF = "save-handler-ref";
    private static final String ATT_SAVE_HANDLER_CLASS = "save-handler-class";

    @Override
    protected Class<?> getBeanClass(Element element) {
        return AuditPostProcessor.class;
    }

    @Override
    protected void doParse(Element element, BeanDefinitionBuilder bean) {
        String ref = element.getAttribute(ATT_SAVE_HANDLER_REF);
        String className = element.getAttribute(ATT_SAVE_HANDLER_CLASS);
        if (StringUtils.hasText(ref) && StringUtils.hasText(className)) {
            throw new IllegalArgumentException(
                "Attributes save-handler-ref and save-handler-class is set. Only either of them should be set.");
        }

        if (StringUtils.hasText(ref)) {
            bean.addPropertyReference("saveHandler", ref);
        }

        if (StringUtils.hasText(className)) {
            try {
                Class<?> clazz = Class.forName(className);
                bean.addPropertyValue("saveHandler", clazz.newInstance());
            } catch (Exception e) {
                throw new IllegalArgumentException(e);
            }
        }
    }

    @Override
    protected boolean shouldGenerateId() {
        return true;
    }
}
