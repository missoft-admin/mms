<%@ include file="../common/taglibs.jsp" %>

  <c:set var="ENCTYPE" value="application/x-www-form-urlencoded"/>
  <c:if test="${multipart}"><c:set var="ENCTYPE" value="multipart/form-data"/></c:if>

  <spring:message var="typeName" code="menu_item_employeepurchasetxnad_new_label"/>
  <spring:message var="dialogLabel" code="global_menu_new" arguments="${typeName}"/>

<div class="modal hide  groupModal nofly" id="approverFormDialog" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
    
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title"><spring:message code="label_add_override_permission" /></h4>
      </div>
   
      <div class="modal-body">
      <form:form  cssClass="form-horizontal" id="approver" name="approver" modelAttribute="approver" action="${pageContext.request.contextPath}/override/permissions" method="POST" enctype="${ENCTYPE}" >
            <div class="contentMain">
            
            <div class="control-group">  
                <label class="control-label" for="user"><spring:message code="adjustments_approver_prop_username"/></label>  
                <div class="controls">  
                    <select id="user" name="user">
                        <option value=""></option>
                      <c:forEach var="item" items="${users}">
                          <option value="${item.id}">${item.username}</option>
                      </c:forEach>
                  </select>
                </div>  
            </div>
            
                
            </div>
        </form:form>
       
      </div>
      <div class="modal-footer container-btn">
        <button type="button" id="button_save" onclick="javascript: $('#approver').submit();" class="btn btn-primary"><spring:message code="label_save" /></button>
        <button type="button" id="button_cancel" class="btn btn-default"><spring:message code="label_cancel"/></button>
      </div>
    </div>
  </div>
</div>

