package com.transretail.crm.template.service.streaming.excel.handler.sheet;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author Allan G. Ramirez (agramirez@exist.com)
 */
public interface SheetHandler {
    static final Logger _LOG = LoggerFactory.getLogger(AbstractSheetHandler.class);

    /**
     * A row with the (zero based) row number has started
     */
    public void startRow(int rowNum);

    /**
     * A row with the (zero based) row number has ended
     */
    public void endRow();

    /**
     * A cell, with the given formatted value, was encountered
     */
    public void cell(int index, String cellReference, String formattedValue, short formatIndex);

    /**
     * Returns total row data parsed.
     *
     * @return all non-empty rows excluding the column header names which is normally the first row
     */
    int getTotalRowData();

    /**
     * Returns all rows
     *
     * @return all rows - empty or not empty
     */
    int getTotalRows();

    String setSheetName(String sheetName);

    int getExpectedNumberOfColumns();

    void cleanup();
}
