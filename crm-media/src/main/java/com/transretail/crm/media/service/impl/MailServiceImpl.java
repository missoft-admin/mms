package com.transretail.crm.media.service.impl;


import java.io.File;
import java.io.IOException;
import java.io.UnsupportedEncodingException;

import javax.mail.MessagingException;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;

import com.transretail.crm.core.dto.MessageDto;
import com.transretail.crm.media.service.MailService;


@Service("mailService")
public class MailServiceImpl implements MailService, ApplicationContextAware {

    @Autowired
    private JavaMailSender mailSender;

    @Value("#{'${mail.from.name}'}")
    private String fromName;

    @Value("#{'${mail.from.address}'}")
    private String fromAddress;

	private static final Logger LOGGER = LoggerFactory.getLogger( MailServiceImpl.class );



    @Override
    public void setApplicationContext( ApplicationContext applicationContext ) throws BeansException {}



    @Override
    public boolean send( MessageDto dto ) {
		if ( ArrayUtils.isEmpty( dto.getRecipients() ) && StringUtils.isBlank( dto.getEmailAd() ) ) {
			return false;
		}

    	try {
    		LOGGER.info( ":send:: Email " );

    		MimeMessageHelper helper = this.setMimeMessageHelper( dto );
			helper.setText( dto.getMessage() );
            mailSender.send( helper.getMimeMessage() );

    		LOGGER.info( ":send:: Email successfully sent email to recipients:" + 
    				( ArrayUtils.isNotEmpty( dto.getRecipients() )? StringUtils.join( dto.getRecipients(), "," ) : 
    					StringUtils.isNotBlank( dto.getEmailAd() )? dto.getEmailAd() : "" ) );
            return true;
		} 
    	catch ( MessagingException e ) {
    		LOGGER.error( ":send:: Email MessagingException message: " + e.getMessage() );
		} 
    	catch ( UnsupportedEncodingException e ) {
    		LOGGER.error( ":send:: Email UnsupportedEncodingException message: " + e.getMessage() );
		}
    	catch ( Exception e ) {
    		LOGGER.error( ":send:: Email Exception message: " + e.getMessage() );
		}
    	return false;
    }

    @Override
    public boolean sendWithHtml( MessageDto dto ) {
		if ( ArrayUtils.isEmpty( dto.getRecipients() ) && StringUtils.isBlank( dto.getEmailAd() ) ) {
			return false;
		}

    	try {
    		LOGGER.info( ":sendWithHtml:: Email " );

    		MimeMessageHelper helper = this.setMimeMessageHelper( dto );
			helper.setText( ( StringUtils.isNotBlank( dto.getMessage() )? dto.getMessage() : "" ) 
					+ ( null != dto.getFile()? new String( dto.getFile().getBytes() ) : "" ), true );
			/*if ( null != dto.getFile() && !dto.getFile().isEmpty() ) {
				helper.addInline( dto.getFile().getOriginalFilename(), 
					new ByteArrayResource( IOUtils.toByteArray( dto.getFile().getInputStream() ) ), "text/html; charset=utf-8" );
			}*/
            mailSender.send( helper.getMimeMessage() );

    		LOGGER.info( ":sendWithHtml:: Email successfully sent email to recipients:" + 
    				( ArrayUtils.isNotEmpty( dto.getRecipients() )? StringUtils.join( dto.getRecipients(), "," ) : 
    					StringUtils.isNotBlank( dto.getEmailAd() )? dto.getEmailAd() : "" ) );
            return true;
		}
    	catch ( MessagingException e ) {
    		LOGGER.error( ":sendWithHtml:: Email MessagingException message: " + e.getMessage() );
		} 
    	catch ( UnsupportedEncodingException e ) {
    		LOGGER.error( ":sendWithHtml:: Email UnsupportedEncodingException message: " + e.getMessage() );
		}
		catch (IOException e) {
    		LOGGER.error( ":sendWithHtml:: Email IOException message: " + e.getMessage() );
		}
    	return false;
    }

    @Override
    public boolean sendWithInlineFile( MessageDto dto ) {
		if ( ArrayUtils.isEmpty( dto.getRecipients() ) && StringUtils.isBlank( dto.getEmailAd() ) ) {
			return false;
		}
		if ( null == dto.getFile() ) {
			return send( dto );
		}

    	try {
    		LOGGER.info( ":sendWithInlineFile:: Email " );

    		MimeMessageHelper helper = this.setMimeMessageHelper( dto );
			helper.setText( dto.getMessage(), true );
			File file = new File( dto.getFile().getOriginalFilename() );
			FileUtils.writeByteArrayToFile( file, dto.getFile().getBytes() );
			helper.addAttachment( dto.getFile().getOriginalFilename(), file );
            mailSender.send( helper.getMimeMessage() );

    		LOGGER.info( ":sendWithInlineFile:: Email successfully sent email to recipients:" + 
    				( ArrayUtils.isNotEmpty( dto.getRecipients() )? StringUtils.join( dto.getRecipients(), "," ) : 
    					StringUtils.isNotBlank( dto.getEmailAd() )? dto.getEmailAd() : "" ) );
            return true;
		} 
    	catch ( MessagingException e ) {
    		LOGGER.error( ":sendWithInlineFile:: Email MessagingException message: " + e.getMessage() );
		} 
    	catch ( UnsupportedEncodingException e ) {
    		LOGGER.error( ":sendWithInlineFile:: Email UnsupportedEncodingException message: " + e.getMessage() );
		}
    	catch ( Exception e ) {
    		LOGGER.error( ":sendWithInlineFile:: Email Exception message: " + e.getMessage() );
		}
    	return false;
    }



    private MimeMessageHelper setMimeMessageHelper( MessageDto dto ) throws UnsupportedEncodingException, MessagingException {
    	MimeMessageHelper helper = new MimeMessageHelper( mailSender.createMimeMessage(), true );
		helper.setFrom( fromAddress, fromName );
		if ( ArrayUtils.isNotEmpty( dto.getRecipients() ) ) {
			helper.setBcc( dto.getRecipients() );
		}
		else if ( StringUtils.isNotBlank( dto.getEmailAd() ) ) {
			helper.setTo( dto.getEmailAd() );
		}
		helper.setSubject( StringUtils.isNotBlank( dto.getSubject() )? dto.getSubject() : "" );
		return helper;
    }

}