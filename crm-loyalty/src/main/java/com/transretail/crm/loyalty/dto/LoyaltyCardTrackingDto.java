package com.transretail.crm.loyalty.dto;

import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.mysema.query.annotations.QueryProjection;
import com.transretail.crm.common.web.converter.DateTimeSerializer;

public class LoyaltyCardTrackingDto {
	
	@JsonSerialize(using = DateTimeSerializer.class)
	private DateTime createdDate;
	private Long beginningBal;
	private String transaction;
	private Long quantity;
	private Long endingBal;
	
	private static final DateTimeFormatter FULL_DATETIME_PATTERN = DateTimeFormat.forPattern("MMMM d, yyyy HH:mm");
	
	public LoyaltyCardTrackingDto() {}
	
	public LoyaltyCardTrackingDto(DateTime createdDate, Long beginningBal,
			String transaction, Long quantity, Long endingBal) {
		super();
		this.createdDate = createdDate;
		this.beginningBal = beginningBal;
		this.transaction = transaction;
		this.quantity = quantity;
		this.endingBal = endingBal;
	}
	
	
	@QueryProjection
	public LoyaltyCardTrackingDto(DateTime createdDate, String transaction,
			Long quantity) {
		super();
		this.createdDate = createdDate;
		this.transaction = transaction;
		this.quantity = quantity;
	}

	public DateTime getCreatedDate() {
		return createdDate;
	}
	public void setCreatedDate(DateTime createdDate) {
		this.createdDate = createdDate;
	}
	public Long getBeginningBal() {
		return beginningBal;
	}
	public void setBeginningBal(Long beginningBal) {
		this.beginningBal = beginningBal;
	}
	public String getTransaction() {
		return transaction;
	}
	public void setTransaction(String transaction) {
		this.transaction = transaction;
	}
	public Long getQuantity() {
		return quantity;
	}
	public void setQuantity(Long quantity) {
		this.quantity = quantity;
	}
	public Long getEndingBal() {
		return endingBal;
	}
	public void setEndingBal(Long endingBal) {
		this.endingBal = endingBal;
	}
	public String getCreatedDateStr() {
		return FULL_DATETIME_PATTERN.print(this.createdDate);
	}
	
}
