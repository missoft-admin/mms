package com.transretail.crm.giftcard.dto;

import java.math.BigDecimal;
import java.util.List;

import com.google.common.collect.Lists;
import com.transretail.crm.giftcard.entity.ProductProfile;
import com.transretail.crm.giftcard.entity.ReturnByCardsItem;
import com.transretail.crm.giftcard.entity.ReturnByCardsItem.ItemType;
import com.transretail.crm.giftcard.entity.ReturnRecord;
import com.transretail.crm.giftcard.entity.support.GiftCardInventoryStatus;

/**
 *
 */
public class ReturnByCardsItemDto {

	private Long recordId;
	private String recordNo;
	private String startingSeries;
	private String endingSeries;
	private ItemType type;
	
	private Long prodId;
	private String productName;
	private String productCode;
	
	private BigDecimal balance;
	private BigDecimal faceAmount;
	private GiftCardInventoryStatus status;

	public ReturnByCardsItemDto() {
	}

	public ReturnByCardsItemDto(ReturnByCardsItem item) {
		ReturnRecord record = item.getRecord();
		this.recordId = record.getId();
		this.recordNo = record.getRecordNo();
		this.startingSeries = item.getStartingSeries();
		this.endingSeries = item.getEndingSeries();
		ProductProfile product = item.getProduct();
		if(product != null) {
			this.prodId = product.getId();
			this.productCode = product.getProductCode();
			this.productName = product.getProductDesc();
		}
		this.balance = item.getBalance();
		this.faceAmount = item.getFaceAmount();
		this.status = item.getStatus();
		this.type = item.getType();
	}

	public ReturnByCardsItem toModel(ReturnByCardsItem item) {
		if (item == null)
			item = new ReturnByCardsItem();
		item.setRecord(new ReturnRecord(this.recordId));
		item.setStartingSeries(this.startingSeries);
		item.setEndingSeries(this.endingSeries);
		item.setType(this.type);
		item.setProduct(new ProductProfile(this.prodId));
		item.setBalance(this.balance);
		item.setFaceAmount(this.faceAmount);
		item.setStatus(this.status);
		return item;
	}

	public static List<ReturnByCardsItem> toModel(
			List<ReturnByCardsItemDto> itemsDto, ReturnRecord record) {
		List<ReturnByCardsItem> items = Lists.newArrayList();
		for (ReturnByCardsItemDto itemDto : itemsDto) {
			ReturnByCardsItem item = itemDto.toModel(null);
			item.setRecord(record);
			items.add(item);
		}
		return items;
	}

	public Long getRecordId() {
		return recordId;
	}

	public void setRecordId(Long recordId) {
		this.recordId = recordId;
	}

	public String getRecordNo() {
		return recordNo;
	}

	public void setRecordNo(String recordNo) {
		this.recordNo = recordNo;
	}

	public String getStartingSeries() {
		return startingSeries;
	}

	public void setStartingSeries(String startingSeries) {
		this.startingSeries = startingSeries;
	}

	public String getEndingSeries() {
		return endingSeries;
	}

	public void setEndingSeries(String endingSeries) {
		this.endingSeries = endingSeries;
	}

	public ItemType getType() {
		return type;
	}

	public void setType(ItemType type) {
		this.type = type;
	}

	public Long getProdId() {
		return prodId;
	}

	public void setProdId(Long prodId) {
		this.prodId = prodId;
	}

	public String getProductName() {
		return productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}

	public String getProductCode() {
		return productCode;
	}

	public void setProductCode(String productCode) {
		this.productCode = productCode;
	}

	public BigDecimal getBalance() {
		return balance;
	}

	public void setBalance(BigDecimal balance) {
		this.balance = balance;
	}

	public BigDecimal getFaceAmount() {
		return faceAmount;
	}

	public void setFaceAmount(BigDecimal faceAmount) {
		this.faceAmount = faceAmount;
	}

	public GiftCardInventoryStatus getStatus() {
		return status;
	}

	public void setStatus(GiftCardInventoryStatus status) {
		this.status = status;
	}

}
