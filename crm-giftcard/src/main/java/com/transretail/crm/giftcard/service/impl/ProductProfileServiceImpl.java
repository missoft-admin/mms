package com.transretail.crm.giftcard.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.google.common.collect.Lists;
import com.mysema.query.types.Order;
import com.mysema.query.types.OrderSpecifier;
import com.mysema.query.types.Predicate;
import com.mysema.query.types.expr.BooleanExpression;
import com.transretail.crm.common.service.dto.response.ResultList;
import com.transretail.crm.common.service.util.SpringDataPagingUtil;
import com.transretail.crm.giftcard.dto.ProductProfileDto;
import com.transretail.crm.giftcard.dto.ProductProfileSearchDto;
import com.transretail.crm.giftcard.entity.ProductProfile;
import com.transretail.crm.giftcard.entity.QProductProfile;
import com.transretail.crm.giftcard.entity.support.ProductProfileStatus;
import com.transretail.crm.giftcard.repo.ProductProfileRepo;
import com.transretail.crm.giftcard.service.ProductProfileService;

@Service("productProfileService")
public class ProductProfileServiceImpl implements ProductProfileService {

	@Autowired
	private ProductProfileRepo profileRepo;

	@Override
	@Transactional(readOnly = true)
	public boolean findDuplicateProductCode(String productCode, Long profileId) {
		QProductProfile qProfile = QProductProfile.productProfile;
		Predicate idPred = null;
		if (profileId != null)
			idPred = qProfile.id.ne(profileId);
		if (profileRepo.count(qProfile.productCode.eq(productCode)
				.and(idPred)) > 0)
			return false;
		return true;
	}

	@Override
	@Transactional(readOnly = true)
	public ResultList<ProductProfileDto> list(ProductProfileSearchDto searchDto) {
		Pageable pageable = SpringDataPagingUtil.INSTANCE.createPageable(searchDto.getPagination());
		BooleanExpression filter = searchDto.createSearchExpression();
		Page<ProductProfile> page = filter != null ? profileRepo.findAll(filter, pageable) : profileRepo.findAll(pageable);
		return new ResultList<ProductProfileDto>(toDto(page.getContent()), page.getTotalElements(), page.hasPreviousPage(),
				page.hasNextPage());
	}

	@Override
	@Transactional(readOnly = true)
	public List<ProductProfileDto> findAllDtos() {
		return toDto(Lists.newArrayList(profileRepo.findAll(QProductProfile.productProfile.status.eq(ProductProfileStatus.APPROVED),
				new OrderSpecifier(Order.ASC, QProductProfile.productProfile.productDesc))));
	}

	@Override
	@Transactional
	public void saveDto(ProductProfileDto profileDto) {
		ProductProfile profile = new ProductProfile();
		if (profileDto.getId() != null)
			profile = profileRepo.findOne(profileDto.getId());
		profile = profileDto.toModel(profile);
		profileRepo.save(profile);

	}

	@Override
	@Transactional
	public void deleteProfile(Long id) {
		profileRepo.delete(id);
	}

	@Override
	@Transactional(readOnly = true)
	public ProductProfileDto findDto(Long id) {
		return new ProductProfileDto(profileRepo.findOne(id));
	}

	@Override
	@Transactional
	public void approveProfile(Long id, ProductProfileStatus status) {
		ProductProfile profile = profileRepo.findOne(id);
		profile.setStatus(status);
		profileRepo.save(profile);
	}

	private List<ProductProfileDto> toDto(List<ProductProfile> profiles) {
		List<ProductProfileDto> dtos = Lists.newArrayList();
		for (ProductProfile profile : profiles) {
			dtos.add(new ProductProfileDto(profile));
		}
		return dtos;
	}

	@Override
	@Transactional(readOnly = true)
	public ProductProfileDto findProductProfileByCode(String productCode) {
		QProductProfile qProduct = QProductProfile.productProfile;
		ProductProfileDto dto = new ProductProfileDto(profileRepo.findOne(qProduct.productCode.eq(productCode)));
		return dto;
	}

	@SuppressWarnings("unchecked")
	@Override
	@Transactional(readOnly = true)
	public List<ProductProfileDto> findDtos(ProductProfileSearchDto dto) {
		return toDto(Lists.newArrayList(profileRepo.findAll(dto.createSearchExpression(),
				new OrderSpecifier(Order.ASC, QProductProfile.productProfile.productDesc))));
	}

	@Override
	public ProductProfileDto activate(Long id, boolean activate) {
		if (null == id) {
			return null;
		}

		ProductProfile profile = profileRepo.findOne(id);
		profile.setStatus(activate ? ProductProfileStatus.DRAFT : ProductProfileStatus.DEACTIVATED);
		return new ProductProfileDto(profileRepo.saveAndFlush(profile));
	}

}
