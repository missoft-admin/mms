/*
 * Copyright (c) 2012.
 * All rights reserved.
 */
package com.transretail.crm.common.reporting.service.impl;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.reset;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Arrays;
import java.util.List;

import org.apache.commons.io.IOUtils;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TemporaryFolder;
import org.junit.runner.RunWith;
import org.mockito.internal.matchers.StartsWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.transretail.crm.common.reporting.exception.ReportException;
import com.transretail.crm.common.reporting.jasper.ClasspathFileResolver;
import com.transretail.crm.common.reporting.jasper.DefaultJRProcessor;
import com.transretail.crm.common.reporting.jasper.JRProcessor;
import com.transretail.crm.common.reporting.service.ReportService;

import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRExporter;
import net.sf.jasperreports.engine.export.JRExportProgressMonitor;

/**
 * @author Allan G. Ramirez (agramirez@exist.com)
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("classpath:META-INF/spring/applicationContext-reporting.xml")
public class ReportServiceImplTest {
    private static final Logger _LOG = LoggerFactory.getLogger(ReportServiceImplTest.class);
    @Rule
    public TemporaryFolder tmpFolder = new TemporaryFolder();
    @Autowired
    private ReportService reportService;

    @Test(expected = ReportException.class)
    public void exportPdfReportWithReportExceptionTest() throws Exception {
        File output = new File(tmpFolder.getRoot(), "NonExistingFile");
        try {
            reportService.exportPdfReport(mock(JRProcessor.class), output);
        } finally {
            // Created files should be removed if report generation failed
            assertFalse(output.exists());
        }
    }

    @Test
    public void exportReportWithExceptionTest() throws Exception {
        JRExporter exporter = mock(JRExporter.class);
        Exception exception = new JRException("test");
        doThrow(exception).when(exporter).exportReport();

        JRProcessor jrProcessor = mock(JRProcessor.class);
        when(jrProcessor.getJasperPrints()).thenReturn(mock(List.class));

        OutputStream os = mock(OutputStream.class);
        try {
            ((ReportServiceImpl) reportService).exportReport(exporter, jrProcessor, os, false);
        } catch (ReportException e) {
            assertEquals(exception, e.getCause());
            verify(os, never()).flush();
        }

        reset(exporter);
        exception = new IOException("test");
        doThrow(exception).when(os).flush();
        try {
            ((ReportServiceImpl) reportService).exportReport(exporter, jrProcessor, os, false);
        } catch (ReportException e) {
            verify(os).flush();
            assertEquals(exception, e.getCause());
            assertEquals("Failed to flush to outputstream", e.getMessage());
        }
    }

    @Test
    public void exportPdfReportUsingOutputStream() throws Exception {
        JRExportProgressMonitor afterPageExportListener = mock(JRExportProgressMonitor.class);
        OutputStream os = null;
        try {
            os = new FileOutputStream(tmpFolder.newFile("output5.pdf"));

            DefaultJRProcessor jrProcessor = new DefaultJRProcessor("reports/test-report.jrxml");
            jrProcessor.setFileResolver(new ClasspathFileResolver("reports"));

            reportService.exportPdfReport(jrProcessor, os);

            verify(afterPageExportListener, never()).afterPageExport();
        } catch (IllegalArgumentException e) {
            assertThat(e.getMessage(), new StartsWith("Instance of subtype of FileOutputStream is not allowed"));
        } finally {
            IOUtils.closeQuietly(os);
        }

        ByteArrayOutputStream baos = new ByteArrayOutputStream(1024);
        DefaultJRProcessor jrProcessor = new DefaultJRProcessor("reports/test-report.jrxml");
        jrProcessor.setFileResolver(new ClasspathFileResolver("reports"));
        jrProcessor.setAfterPageExportListener(afterPageExportListener);
        reportService.exportPdfReport(jrProcessor, baos);

        assertFalse("".equals(baos.toString()));
        verify(afterPageExportListener).afterPageExport();
    }

    @Test
    public void exportPdfReportTest() throws Exception {
        // Case 1: Compiled iReport 4.5.1 jrxml
        File output = exportPdfReport("output1.pdf", "reports/test-report.jasper");
        _LOG.debug("Output file: " + output.getAbsolutePath());
        checkEmpty(output, false);
        // Case 2: Raw iReport 4.5.1 jrxml
        output = exportPdfReport("output2.pdf", "reports/test-report.jrxml");
        _LOG.debug("Output file: " + output.getAbsolutePath());
        checkEmpty(output, false);
        // Case 3: Compiled iReport 5.0.0 jrxml
        output = exportPdfReport("output3.pdf", "reports/test-report-5.jasper");
        _LOG.debug("Output file: " + output.getAbsolutePath());
        checkEmpty(output, false);
        // Case 4: Raw iReport 5.0.0 jrxml
        output = exportPdfReport("output4.pdf", "reports/test-report-5.jrxml");
        _LOG.debug("Output file: " + output.getAbsolutePath());
        checkEmpty(output, false);
    }

    private File exportPdfReport(String fileName, final String reportFileName) throws Exception {
//        File file = new File(fileName);
//        if(!file.exists()) {
//            file.createNewFile();
//            checkEmpty(file, true);
//        }
        File file = tmpFolder.newFile(fileName);

        SampleBean bean = new SampleBean();
        bean.setName("CloudX");
        DefaultJRProcessor jrProcessor = new DefaultJRProcessor(reportFileName, Arrays.asList(bean));
        jrProcessor.setFileResolver(new ClasspathFileResolver("reports"));
        reportService.exportPdfReport(jrProcessor, file);
        return file;
    }

    private void checkEmpty(File file, boolean empty) {
        InputStream is = null;
        try {
            is = new FileInputStream(file);
            byte[] bytes = IOUtils.toByteArray(is);
            if (empty) {
                assertFalse(bytes.length > 0);
            } else {
                assertTrue(bytes.length > 0);
            }
        } catch (IOException e) {
            fail(e.getMessage());
        } finally {
            IOUtils.closeQuietly(is);
        }
    }

    public static class SampleBean {
        private String name;

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }
    }
}
