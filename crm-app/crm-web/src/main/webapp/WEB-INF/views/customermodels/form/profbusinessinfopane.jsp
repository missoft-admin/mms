<%@ include file="../../common/taglibs.jsp" %>


<fieldset class="form-horizontal container-fluid">
    <div class="control-group">
      <div class="contentInfo">
        <label for="registeredStore" class="control-label"> <spring:message
            code="member_prop_prefstore" /></label>
        <div class="controls">
          <span class="label col-sm-10" id="content_registeredStoreStatic"></span>
          <select name="registeredStore" id="content_registeredStore"
          data-url-store="<c:url value="/customermodels/member/store/" />"></select>
        </div>
      </div>
    </div>

    <div class="control-group">
      <spring:message
        code="label_com_transretail_crm_entity_customermodel_businessname"
        var="businessName" />
      <label for="professionalProfile.businessName"
        class="control-label"><b class="required">*</b>
      <c:out value="${businessName}" />:</label>
      <div class="controls">
        <input type="text" name="companyName"
          placeholder="${businessName}" />
      </div>
    </div>
    
    
</fieldset>



<!-- Address -->
<fieldset class="container-fluid">
    <legend><spring:message code="label_com_transretail_crm_entity_customermodel_businessaddress" /></legend>
    <div class="row-fluid">
      <spring:message code="label_address_street" var="street"/>
      <fieldset class="pull-left span6">
        <label for="street"><b class="required">*</b>${street }</label>
        <form:input path="professionalProfile.businessAddress.street" placeholder="${street}" cssClass="span12" />
      </fieldset>
    	<spring:message code="label_address_number" var="streetNumber"/>
    	<fieldset class="pull-left ml10">
	    	<label for="streetNumber">${streetNumber }</label>
	        <form:input path="professionalProfile.businessAddress.streetNumber" cssClass="span10" placeholder="${streetNumber}" />
      </fieldset>
    </div>
    <div class="row-fluid">
        <spring:message code="label_address_block" var="block"/>
        <fieldset class="pull-left">
	        <label for="block">${block }</label>
        	<form:input path="professionalProfile.businessAddress.block" placeholder="${block}" cssClass="span11" />
        </fieldset>
        <spring:message code="label_address_km" var="km"/>
        <fieldset class="pull-left ">
	        <label for="km">${km }</label>
        	<form:input path="professionalProfile.businessAddress.km" placeholder="${km}" cssClass="span11 intInput" />
        </fieldset>
        <%-- <spring:message code="label_address_rtrw" var="rtrw"/>
        <fieldset class="span3">
          <label for="rt">${rtrw }</label>
          <form:input path="professionalProfile.businessAddress.rt" placeholder="${rtrw}" cssClass="span11" />
        </fieldset> --%>
        <spring:message code="label_address_rt" var="rt"/>
        <fieldset class="pull-left">
	        <label for="rt">${rt }</label>
        	<form:input path="professionalProfile.businessAddress.rt" placeholder="${rt}" cssClass="span11 intInput" />
        </fieldset>
        <spring:message code="label_address_rw" var="rw"/>
        <fieldset class="pull-left">
	        <label for="rw">${rw }</label>
        	<form:input path="professionalProfile.businessAddress.rw" placeholder="${rw}" cssClass="span11 intInput" />
        </fieldset>
    </div>
    <div class="row-fluid">
        <spring:message code="label_address_building" var="building"/>
        <fieldset class="pull-left span6">
	        <label for="building">${building }</label>
        	<form:input path="professionalProfile.businessAddress.building" placeholder="${building}" cssClass="span12" />
        </fieldset>
        <spring:message code="label_address_floor" var="floor"/>
        <fieldset class="pull-left ml10">
	        <label for="floor">${floor }</label>
        	<form:input path="professionalProfile.businessAddress.floor" placeholder="${floor}" cssClass="span10 intInput" />
        </fieldset>
        <spring:message code="label_address_room" var="room"/>
        <fieldset class="pull-left">
	        <label for="room">${room }</label>
        	<form:input path="professionalProfile.businessAddress.room" placeholder="${room}" cssClass="span10" />
        </fieldset>
    </div>
    <div class="row-fluid">
        <spring:message code="label_address_postalcode" var="postCode"/>
        <fieldset class="pull-left">
          <label for="postCode"><span class="required">*</span>  ${postCode }</label>
          <form:input path="professionalProfile.businessAddress.postCode" placeholder="${postCode}" cssClass="span11" />
        </fieldset>
        <spring:message code="label_address_subdistrict" var="subdistrict"/>
        <fieldset class="pull-left">
	        <label for="subdistrict">${subdistrict }</label>
        	<form:input path="professionalProfile.businessAddress.subdistrict" placeholder="${subdistrict}" cssClass="span11" />
        </fieldset>
        <spring:message code="label_address_district" var="district"/>
        <fieldset class="pull-left">
	        <label for="district">${district }</label>
        	<form:input path="professionalProfile.businessAddress.district" placeholder="${district}" cssClass="span11" />
        </fieldset>
    </div>
    <div class="row-fluid provinceCityBlock">
        <spring:message code="label_address_province" var="province"/>
        <fieldset class="pull-left">
          <label for="province">${province}</label>
          <select name="professionalProfile.businessAddress.province" class="provinceSelect">
            <option value=""></option>
            <c:forEach items="${provinces}" var="province">
            <option value="${province.name}">${province.name}</option>
            </c:forEach>
          </select>
          <%--<form:input path="professionalProfile.businessAddress.province" placeholder="${province}" cssClass="span10" /> --%>
        </fieldset>
        <spring:message code="label_address_city" var="city"/>
        <fieldset class="pull-left ml10">
          <label for="city">${city }</label>
          <select name="professionalProfile.businessAddress.city" class="citySelect">
          <option value=""></option>
          </select>
          <%--<form:input path="professionalProfile.businessAddress.city" placeholder="${city}" cssClass="span11" /> --%>
        </fieldset>
    </div>
</fieldset>

<fieldset class="form-horizontal container-fluid">
  <legend></legend>
  
  <div class="control-group">
    <label for="businessType" class="control-label"><span class="required">*</span> <spring:message
        code="label_com_transretail_crm_entity_customermodel_businesstype" />
    </label>
    <div class="controls">
      <form:select id="customerGroup" cssClass="customerGroup"
        path="professionalProfile.customerGroup"
        items="${customerGroups}" itemLabel="description"
        itemValue="code" />
    </div>
    <label for="businessType" class="control-label"></label>
    <div class="controls mt10">
      <select name="professionalProfile.customerSegmentation"
        id="customerSegmentation"></select>
    </div>
  </div>
  
  <div class="control-group professionalDetails">
    <spring:message
      code="label_com_transretail_crm_entity_customermodel_businesslicense"
      var="businessLicense" />
    <label for="businessLicense" class="control-label"><c:out
        value="${businessLicense}" /></label>
    <div class="controls">
      <form:input path="professionalProfile.businessLicense"
        placeholder="${businessLicense}" />
    </div>
  </div>
  
  <div class="control-group professionalDetails">
    <spring:message
      code="label_com_transretail_crm_entity_customermodel_registrationid"
      var="registrationId" />
    <label for="registrationId" class="control-label"><c:out
        value="${registrationId}" /></label>
    <div class="controls">
      <form:input path="professionalProfile.registrationId"
        placeholder="${registrationId}" />
    </div>
  </div>
 
  
</fieldset>

<fieldset class="form-horizontal container-fluid">
    <legend><spring:message code="label_com_transretail_crm_entity_customermodel_npwp" /></legend>
    
    <jsp:include page="npwppane.jsp" />
    
    <div class="control-group">
    	<spring:message
    		code="label_com_transretail_crm_entity_customermodel_businessphone"
    		var="businessPhone" />
    	<label for="businessPhone" class="control-label"><c:out
    			value="${businessPhone}" />
    	</label> 
    	<div class="controls">
    		<input type="text" name="professionalProfile.businessPhone" class="form-control" placeholder="${businessPhone}" />
    	</div>
    </div>

    
    <div class="control-group">
    	<spring:message
    		code="label_com_transretail_crm_entity_customermodel_businessemail"
    		var="businessEmail" />
    	<label for="businessEmail" class="control-label"><c:out
    			value="${businessEmail}" />
    	</label> 
    	<div class="controls">
    		<input type="text" name="professionalProfile.businessEmail" class="form-control" placeholder="${businessEmail}" />
    	</div>
    </div>


    
    
    <div class="control-group">
        <spring:message
            code="label_com_transretail_crm_entity_customermodel_radius" var="radius" />
        <label for="radius" class="control-label"><b class="required">*</b> <c:out value="${radius}" />
        </label> 
        <div class="controls">
          <form:select path="professionalProfile.radius"
            items="${radii}" itemLabel="description" itemValue="code" />
          <%-- <form:input path="professionalProfile.radius" id="radius" cssClass="form-control" placeholder="${radius}" /> --%>
        </div>
    </div>
    
    <div class="control-group">
        <spring:message code="label_com_transretail_crm_entity_customermodel_zone" var="zone" />
        <label for="zone" class="control-label"><b class="required">*</b> <c:out value="${zone}" /></label> 
        <div class="controls">
          <form:input path="professionalProfile.zone" cssClass="form-control" placeholder="${zone}" />
        </div>
    </div>
    
    <div class="control-group">
        <spring:message code="label_com_transretail_crm_entity_customermodel_noofemp" var="noOfEmp" />
        <label for="zone" class="control-label"><c:out value="${noOfEmp}" /></label> 
        <div class="controls">
          <form:select path="professionalProfile.noOfEmp" items="${noOfEmployees}"
            itemLabel="description" itemValue="code" />
          <%-- <form:input path="professionalProfile.noOfEmp" cssClass="form-control" placeholder="${noOfEmp}" /> --%>
        </div>
    </div>
    
    <div class="control-group">
        <spring:message code="label_com_transretail_crm_entity_customermodel_potentialbuyingpermonth" var="potentialBuying" />
        <label for="zone" class="control-label"><c:out value="${potentialBuying}" /></label> 
        <div class="controls">
          <div class="radio"><input type="radio" name="professionalProfile.potentialBuyingPerMonth" value="<5 m"><label><spring:message code="member_prof_potential_buying_lt5" /></label></div>
          <div class="radio"><input type="radio" name="professionalProfile.potentialBuyingPerMonth" value="5-9.9 m"><label><spring:message code="member_prof_potential_buying_5to9.9" /></label></div>
          <div class="radio"><input type="radio" name="professionalProfile.potentialBuyingPerMonth" value="10-50 m"><label><spring:message code="member_prof_potential_buying_10to50" /></label></div>
          <div class="radio"><input type="radio" name="professionalProfile.potentialBuyingPerMonth" value=">50 m"><label><spring:message code="member_prof_potential_buying_gt50" /></label></div>
          <%-- <form:input path="professionalProfile.potentialBuyingPerMonth" cssClass="form-control" placeholder="${potentialBuying}" /> --%>
        </div>
    </div>
    
    
</fieldset>

<div class="clearfix"></div>

<style>
  #childrenAge input {
    display: list-item;
    width: 90px;
    margin-top: 10px !important;
  }
  
</style>
