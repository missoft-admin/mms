package com.transretail.crm.template.service.streaming.excel.handler.cell;

import org.apache.commons.lang3.StringUtils;

import com.transretail.crm.core.entity.MemberModel;
import com.transretail.crm.template.service.MemberTemplateParseException;

/**
 * @author Allan G. Ramirez (agramirez@exist.com)
 */
public class KtpIdCellHandler implements CellHandler {
    @Override
    public void handle(MemberModel dto, String cellText) throws MemberTemplateParseException {
        if (StringUtils.isNotBlank(cellText)) {
            dto.setIdNumber("KTP-" + cellText);
        }
    }
}
