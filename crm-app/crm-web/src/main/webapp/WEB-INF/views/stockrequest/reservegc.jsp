<%@include file="/WEB-INF/views/common/taglibs.jsp" %>

<style>
    .table {
        float: left;
    }
    .table tbody tr td:last-child {
        min-width: 40px;
    }
    .table tbody tr td:last-child button {
        margin-left: 10px;
    }
    .table-compressed td {
      padding-left: 0px !important;
    }
    input, textarea, .uneditable-input {
        width: 132px;
    }
</style>

<div class="modal-dialog">
	<div class="modal-content">
		<div class="modal-header">
		  <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
			<h4 class="modal-title"><spring:message code="gc.reserve.label" /></h4>
		</div>
		<div class="modal-body">
			<div id="contentError" class="hide alert alert-error">
		        <button type="button" class="close" onclick="$(this).parent().hide();">&times;</button>
		        <div><form:errors path="*"/></div>
	      	</div>
	      	
	      	
			<form:form id="reservegc" modelAttribute="reservelist" action="${pageContext.request.contextPath}/stockrequest/">
			<div id="reserve-container">

				<table id="reserve-table" class="table table-condensed table-compressed table-striped" style="width: 94%;">
					<thead><tr>
						<th><spring:message code="gc.reserve.label.quantity" /></th>
						<th><spring:message code="gc.reserve.label.starting.series" /></th>
						<th><spring:message code="gc.reserve.label.ending.series" /></th>
						<th><spring:message code="gc.reserve.label.product" /></th>
						<th></th>
					</tr></thead>
					<tbody>
					<c:forEach items="${reservelist.gcList}" var="gcList" varStatus="status">
						<tr>
							<td><form:input path="gcList[${status.index}].quantity" class="quantity input-mini numeric main-input" readonly="true"/></td>
							<td><form:input path="gcList[${status.index}].startingSeries" maxlength="16" class="starting-series series main-input" readonly="true" /></td>
							<td><form:input path="gcList[${status.index}].endingSeries" maxlength="16" class="ending-series series main-input" readonly="true"/></td>
							<td>
								<form:input path="gcList[${status.index}].productCode" type="hidden" class="product-code" />							
								<form:input path="gcList[${status.index}].productDesc" class="product-description" readonly="true" type="text" />
							</td>
							<td><button type="button" id="" class="tiptip btn rowDelete icn-delete hidden" title="<spring:message code="label_remove"/>">-</button></td>
						</tr>
					</c:forEach>
					</tbody>
				</table>
				<button type="button" id="addRowButton" class="tiptip btn btn-small pull-right" style="" title="<spring:message code="label_add"/>">+</button>
		
                <form:hidden path="stockRequestId" />
                <form:hidden path="startIndex"/>
			</div>
			</form:form>
		</div>
		<div class="modal-footer">			
    		<button type="button" id="reserve-gc-save" class="btn btn-primary"><spring:message code="label_save" /></button>
    		<button type="button" id="" class="btn btn-default" data-dismiss="modal"><spring:message code="label_cancel"/></button>
		</div>
	</div>
</div>

<link href="<spring:url value="/styles/temp.css" />" rel="stylesheet">

<script type="text/javascript">
$(document).ready(function() {
	var CONTAINER_ERROR = "#contentError > div";
	var CONTENT_ERROR = "#contentError";

    if(!$(".series").val()) {
        $(".main-input").attr('readonly', false);
    }

	$("#reserve-container").on('click', "#addRowButton", function() {
		var reserveTable = $("#reserve-table > tbody");
		$tr = reserveTable.find("tr:first-child");
		if($tr.css('visibility') == 'hidden' || $tr.css('display') == 'none') {
			$tr.find("input").val("").prop('disabled', false);
			$tr.find("select").val("").prop('disabled', false);
			$tr.visible();
		} else {
			var index = reserveTable.find("tr").length;
			var $row = $tr.clone();
			$row.find(".rowDelete").css('visibility', 'visible');
			
			var rg = new RegExp("\\[0\\]", 'g');
			var html = $row.html();
			html = html.replace(rg, '['+index+']');
			$row.html(html);
			reserveTable.append($row);
			$row.find("input").val("").prop('disabled', false);
			$row.find("select").val("").prop('disabled', false);
            $row.find(".main-input").attr('readonly', false);
		}
	}).on("click", ".rowDelete", function(e) {
		e.preventDefault();
		$tr = $(this).closest("tr"); 
		if($tr.is(":first-child")) {
			$tr.find("input").val("");
			$tr.find("select").val("");
		} else {
			$tr.remove();
		}
			
	})
//	.on('change', 'input.series', getGiftCard)
	.on('change', 'input.starting-series', function() {
		getGiftCard($(this), function() {
			$tr = $(this).closest("tr");
			var quantity = $tr.find('.quantity').val();
			var startingSeries = $(this).val();

			if(quantity && $.isNumeric(quantity) && quantity > 0) {
				$tr.find('.ending-series').val(startingSeries + quantity - 1);
			}
		});
	})
	.on('change', 'input.ending-series', function() {
		getGiftCard($(this), function() {});
	})
	.on('change', 'input.quantity', function() {
		$tr = $(this).closest("tr");
		var startingSeries = $tr.find(".starting-series").val();
		var quantity = $(this).val();

		if(quantity && quantity > 0 && startingSeries) {
			var endingSeries = $tr.find('.ending-series');
			endingSeries.val(parseInt(startingSeries) + parseInt(quantity) - 1);
			endingSeries.change();
	    }
	})
	.on('change', 'input.starting-series', function() {
		$tr = $(this).closest("tr");
		var startingSeries = $tr.find(".starting-series").val();
		var quantity = $tr.find(".quantity").val();

		if(quantity && quantity > 0 && startingSeries) {
			var endingSeries = $tr.find('.ending-series');
			endingSeries.val(parseInt(startingSeries) + parseInt(quantity) - 1);
			endingSeries.change();
		}
	});

	$("#reserve-gc-save").unbind("click").click(function(e) {
		e.preventDefault();
		$form = $("form#reservegc");
	  $(this).prop( "disabled", true );
		ajaxSubmit($form.attr('action') + "reserve/gc/${id}", $form.serialize(), function() {
            //console.log("hello");
			location.reload();
		}, $(this));
	});

	function ajaxSubmit(action, data, fn, $this) {
		$.ajax({
			type 	: "POST",
			url 	: action,
			dataType: 'json',
			data 	: data,
			        
			success : function(inResponse) {
				if (inResponse.success) {
					fn(inResponse);
				} 
				else {
					errorInfo = "";
					console.log(inResponse.result);
					for (i = 0; i < inResponse.result.length; i++) {
						errorInfo += "<br>" + (i + 1) + ". "
								+ ( inResponse.result[i].code != undefined ? 
										inResponse.result[i].code : inResponse.result[i]);
					}
					$( CONTAINER_ERROR ).html("Please correct following errors: " + errorInfo);
					$( CONTENT_ERROR ).show('slow');
				}
				$this.prop( "disabled", false );
			},
			error : function(jqXHR, textStatus, errorThrown) {
				$( CONTENT_ERROR ).html("Error: " + errorThrown);
		    $this.prop( "disabled", false );
			}
		});
	}

	function getGiftCard($source, fn) {
		var series = $source.val();

		if(series) {
			ajaxSubmit($("form#reservegc").attr("action") + "${id}/gc/" + series, null, function(response) {
				//fn();
				
				var result = response.result;
				$source.closest("tr").find(".product-description").val(result.productName + " - " + result.productCode);
				$source.closest("tr").find(".product-code").val(result.productCode);
				
				var start = $source.closest("tr").find(".starting-series").val().trim();
				var end = $source.closest("tr").find(".ending-series").val().trim();
	
				if(start && end) {
					$source.closest("tr").find(".quantity").val(end - start + 1);
				}
			});
		}
		else {
			$source.closest("tr").find(".quantity").val("");
		}
	}
});
</script>