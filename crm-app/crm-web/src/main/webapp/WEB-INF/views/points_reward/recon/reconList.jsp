<%@include file="../../common/taglibs.jsp" %>


<div id="content_reconList">

  <div id="list_recon" 
      data-url="<c:url value="/points/recon/list" />" 
      data-hdr-employee="<spring:message code="recon_lbl_employee"/>" 
      data-hdr-postingdate="<spring:message code="recon_lbl_postingdate"/>" 
      data-hdr-txndate="<spring:message code="recon_lbl_txndate"/>" 
      data-hdr-totaltxnamt="<spring:message code="recon_lbl_txnamt_newtotal"/>" 
      data-hdr-totalreward="<spring:message code="recon_lbl_txnpt_totalreward"/>" 
      data-hdr-totalrecon="<spring:message code="recon_lbl_txnpt_totalrecon"/>" 
      data-hdr-total="<spring:message code="recon_lbl_txnpt_totalrewardrecon"/>" 
      data-hdr-latepostedtxnamt="<spring:message code="recon_lbl_txnamt_lateposted"/>" 
      data-hdr-recon="<spring:message code="recon_lbl_txnpt_recon"/>" ></div>

  <div class="btns hide">
    <a href="<c:url value="/points/recon/process/%ID%?status=%STATUS%" />" class="btn pull-left tiptip icn-approve approveBtn" 
      title="<spring:message code="label_approve" />" ><spring:message code="label_approve" /></a>
  </div>

  <div id="constants" class="hide" data-stat-active="${statActive}" data-is-css="${isCss}" ></div>


<link href="<spring:url value="/css/dataTables.css" />" rel="stylesheet">
<script src="<spring:url value="/js/jquery/jquery.dataTables.js" />" ></script>
<script src="<spring:url value="/js/jquery/jquery.dataTables.linkLengthMenu.js" />"></script>
<script src="<spring:url value="/js/jquery/jquery.dataTables.customPaging.js" />"></script>
<script src="<spring:url value="/js/jquery/ajax-datatable.js" />" ></script>
<script src="<spring:url value="/js/common/form2js.js" />" ></script>
<script src="<spring:url value="/js/common/json2.js" />" ></script>
<script src="<spring:url value="/js/jquery/jquery.toObject.js" />" ></script>

<script type="text/javascript">
var ReconList = null;

$(document).ready( function() {

    var $container = $( "#content_reconList" );
    var $btns = $container.find( ".btns" );
    var $list = $container.find( "#list_recon" );

    var $constants = $container.find( "#constants" );
  
    initDataTable();

    function initDataTable() {
        $list.ajaxDataTable({
            'autoload'    : true,
            'ajaxSource'  : $list.data( "url" ),
            'columnHeaders' : [ 
                $list.data( "hdr-employee" ),
                $list.data( "hdr-postingdate" ),
                /* $list.data( "hdr-txndate" ), */
                /* $list.data( "hdr-totaltxnamt" ), */
                /* $list.data( "hdr-totalreward" ),
                $list.data( "hdr-totalrecon" ),
                $list.data( "hdr-total" ), */
                $list.data( "hdr-latepostedtxnamt" ),
                $list.data( "hdr-recon" ),
                { text: "", className : "span1" }
            ],
            'modelFields'  : [
                { name  : 'memberModel.accountId', sortable : true, fieldCellRenderer : function (data, type, row) {
                    return (row.memberModel)? row.memberModel.accountId + " - " + row.memberModel.firstName + " " + row.memberModel.lastName : "";
                }},
                { name  : 'created', sortable : true, fieldCellRenderer : function (data, type, row) {
                    return (row.dispCreatedDate)? new Date( row.dispCreatedDate - 0 ).customize( 0 ) : "";
                }},
                /* { name  : 'dispTxnDate', sortable : false, fieldCellRenderer : function (data, type, row) {
                    return (row.dispTxnDate)? new Date( row.dispTxnDate - 0 ).customize( 1 ) : "";
                }}, */
                /* { name  : 'txnAmount', sortable : false }, */
                /* { name  : 'dispTxnDate', sortable : false, fieldCellRenderer : function (data, type, row) {
                    return (row.dispTxnDate)? new Date( row.dispTxnDate - 0 ).customize( 1 ) : "";
                }},
                { name  : 'dispTxnDate', sortable : false, fieldCellRenderer : function (data, type, row) {
                    return (row.dispTxnDate)? new Date( row.dispTxnDate - 0 ).customize( 1 ) : "";
                }},
                { name  : 'dispTxnDate', sortable : false, fieldCellRenderer : function (data, type, row) {
                    return (row.dispTxnDate)? new Date( row.dispTxnDate - 0 ).customize( 1 ) : "";
                }}, */
                { name  : 'txnAmount', sortable : false },
                { name  : 'rewardPoints', sortable : false },
                { customCell : 
                    function ( innerData, sSpecific, json ) {
                        var btns = "";
                        if ( sSpecific == 'display' ) {
                        	  if ( json.forProcessing && $constants.data( "is-css" ) ) {
                                  btns += $btns.find( ".approveBtn" ).prop( "outerHTML" ).replace( new RegExp( "%ID%", "g" ), json.id );
                        	  }
                        }
                        return btns;
                    }
                }
            ]
        })
        .on( "click", ".approveBtn", approve );
    }

    function approve(e){
    	  e.preventDefault();

    	  $.get( $(this).attr( "href" ).replace( new RegExp( "%STATUS%", "g" ), $constants.data( "stat-active" ) ), function( resp ) {
    		    ReconList.reloadTable();
    	  }, "json" );
    }

    ReconList = {
        reloadTable  : function() { $list.ajaxDataTable( "search" ); },
        filterTable  : function( filterForm, retFunction ) { $list.ajaxDataTable( "search", filterForm, retFunction ); }
    };
});
</script>


</div>