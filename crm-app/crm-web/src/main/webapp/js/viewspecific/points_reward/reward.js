var NAME_PREFIX = "hidden_";

$(document).ready(function() {

});



function generateReward( inContextPath ) {
	window.location.href = inContextPath + "/pointsreward/forprocessing/generate";
}


function approveRewards( inContextPath, msgConfirm ) {

	if ( getBoundCheckboxes( 'id' ) != "" ) {
		window.location.href = inContextPath + "/pointsreward/approve/" + getBoundCheckboxes( 'id' );
	}
	else {
		$( "#confirmTrue" ).hide();
		getConfirm( msgConfirm, function( result ) {
    		$( "#confirmTrue" ).show();
	    });
	}
}

function approveAllRewards(inContextPath) {
  window.location.href = inContextPath + "/pointsreward/approveAll";
}

function rejectRewards( inContextPath, msgConfirm ) {
	if ( getBoundCheckboxes( 'id' ) != "" ) {
		$( "#rewardRejectDialog" ).modal("show");
	}
	else {
		$( "#confirmTrue" ).hide();
		getConfirm( msgConfirm, function( result ) {
    		$( "#confirmTrue" ).show();
	    });
	}
}

function resetRewards( inContextPath, msgConfirm ) {
	if ( getBoundCheckboxes( 'id' ) != "" ) {
		window.location.href = inContextPath + "/pointsreward/reset/" + getBoundCheckboxes( 'id' );
	}
	else {
		$( "#confirmTrue" ).hide();
		getConfirm( msgConfirm, function( result ) {
    		$( "#confirmTrue" ).show();
	    });
	}
}

function showReport( dateFrom, dateTo, contextPath ) {
	var selectedReportType = $("select[name='reportType']").val();
	theLocation = contextPath+ "/report/employee/rewards/export/" + selectedReportType;
	
	if(selectedReportType == "pdf")
    	window.open( theLocation, '_blank', 'toolbar=0,location=0,menubar=0' );
    else if(selectedReportType == "excel")
  	  	window.location.href = theLocation;
}

function getBoundCheckboxes(inName) {
	var theValues = [];
	$("input:checkbox[name='"+ inName + "']").each(function() {
		if (!$(this).is(':checked')) {
			return;
		}
		theValues.push($(this).val());
	});
	return theValues;
}

function doCheck( inId, inCheckboxName ) {

	$( "#" + inId ).change( function() {
    	if ( $(this).is(':checked') ) {
        	doCheckAll( inCheckboxName, true );
    	}
    	else {
    		doCheckAll( inCheckboxName, false );
    	}
    });
}

function doCheckAll( inName, doCheck ) {
	var theCheckBoxes = $("input:checkbox[name='"+ inName + "']");
	$(theCheckBoxes).each(function() {
		this.checked = doCheck;
	});
}
