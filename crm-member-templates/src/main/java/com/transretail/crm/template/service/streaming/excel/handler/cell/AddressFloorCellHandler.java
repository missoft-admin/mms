package com.transretail.crm.template.service.streaming.excel.handler.cell;

import org.apache.commons.lang3.StringUtils;

import com.transretail.crm.core.entity.MemberModel;
import com.transretail.crm.core.entity.embeddable.Address;
import com.transretail.crm.template.service.MemberTemplateParseException;

/**
 * @author Allan G. Ramirez (agramirez@exist.com)
 */
public class AddressFloorCellHandler implements CellHandler {
    @Override
    public void handle(MemberModel dto, String cellText) throws MemberTemplateParseException {
        if (dto.getAddress() == null) {
            dto.setAddress(new Address());
        }
        dto.getAddress().setFloor(StringUtils.isNotBlank(cellText) ? Integer.valueOf(cellText) : null);
    }
}
