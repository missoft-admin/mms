package com.transretail.crm.core.dto;

import com.transretail.crm.core.entity.Promotion;

import java.io.Serializable;

/**
 * @author Mike de Guzman
 */
public class StampPromoPortaEarnDetailsDto implements Serializable {

    private Long id;
    private String name;
    private String description;
    private Double earnedStamps;

    public StampPromoPortaEarnDetailsDto() {};

    public StampPromoPortaEarnDetailsDto(Long id, String name, String description, Double earnedStamps) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.earnedStamps = earnedStamps;
    }

    public StampPromoPortaEarnDetailsDto(Promotion promotion) {
        id = promotion.getId();
        name = promotion.getName();
        description = promotion.getDescription();
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Double getEarnedStamps() {
        return earnedStamps;
    }

    public void setEarnedStamps(Double earnedStamps) {
        this.earnedStamps = earnedStamps;
    }
}
