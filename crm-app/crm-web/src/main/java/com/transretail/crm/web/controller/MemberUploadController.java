package com.transretail.crm.web.controller;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.concurrent.Callable;

import javax.annotation.Resource;

import org.apache.commons.io.IOUtils;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.transretail.crm.common.service.dto.response.NameIdPairDto;
import com.transretail.crm.common.util.Timer;
import com.transretail.crm.core.repo.LookupDetailRepo;
import com.transretail.crm.core.service.CodePropertiesService;
import com.transretail.crm.template.service.MemberTemplateParseException;
import com.transretail.crm.template.service.MemberTemplateParser;
import com.transretail.crm.web.dto.FileUpload;

/**
 * @author Allan G. Ramirez (agramirez@exist.com)
 */
@Controller
@RequestMapping("/member")
public class MemberUploadController {
    private static final Logger _LOG = LoggerFactory.getLogger(MemberUploadController.class);
    private static final String KEY_FORM = "fileUploadForm";
    private static final String KEY_SUCCESS = "successMessages";
    private static final String KEY_ERROR = "errorMessages";

    private static final String ATTR_NAME_MEMBERTYPES = "memberTypes";
    private static final String ATTR_NAME_LBLHEADER = "label_header";

    private static final String PARAM_EMP = "employee";

    @Resource(name = "streamingExcelMemberTemplateImpl")
    private MemberTemplateParser excelMemberTemplate;
    @Autowired
    private MessageSource messageSource;

    @Autowired
    private CodePropertiesService codePropertiesService;
    @Autowired
    private LookupDetailRepo lookupDetailRepo;

    private Map<String, Object> getModelAttributes(String memberType) {
        Map<String, Object> map = Maps.newHashMap();
        List<NameIdPairDto> memberTypes = Lists.newArrayList();
        map.put(ATTR_NAME_MEMBERTYPES, memberTypes);

        if (PARAM_EMP.equals(memberType)) {
            String employeeCode = codePropertiesService.getDetailMemberTypeEmployee();
            memberTypes.add(new NameIdPairDto(employeeCode, lookupDetailRepo.getDescriptionByCode(employeeCode)));

            map.put(ATTR_NAME_LBLHEADER, messageSource.getMessage("member.upload.employee", null, LocaleContextHolder.getLocale()));
        } else {
            String individualCode = codePropertiesService.getDetailMemberTypeIndividual();
            String companyCode = codePropertiesService.getDetailMemberTypeCompany();

            memberTypes.add(new NameIdPairDto(individualCode, lookupDetailRepo.getDescriptionByCode(individualCode)));
            memberTypes.add(new NameIdPairDto(companyCode, lookupDetailRepo.getDescriptionByCode(companyCode)));

            map.put(ATTR_NAME_LBLHEADER, messageSource.getMessage("member.upload", null, LocaleContextHolder.getLocale()));
        }
        return map;
    }

    @RequestMapping(value = "/upload", method = RequestMethod.GET)
    public ModelAndView showMemberUploadPage() {
        return showMemberUploadPage(null);
    }

    @RequestMapping(value = "/upload/{memberType}", method = RequestMethod.GET)
    public ModelAndView showMemberUploadPage(@PathVariable("memberType") String memberType) {
        return new ModelAndView("customermodels/upload").addAllObjects(getModelAttributes(memberType))
            .addObject(KEY_FORM, new FileUpload());
    }


    @RequestMapping(value = "/upload", method = RequestMethod.POST)
    public Callable<ModelAndView> saveMemberUpload(@ModelAttribute(KEY_FORM) FileUpload form) {
        return saveMemberUpload(form, null);
    }

    @RequestMapping(value = "/upload/{memberType}", method = RequestMethod.POST)
    public Callable<ModelAndView> saveMemberUpload(final @ModelAttribute(KEY_FORM) FileUpload form,
        final @PathVariable("memberType") String memberType) {
        final Locale locale = LocaleContextHolder.getLocale();
        final String fileName = form.getFile().getOriginalFilename();
        return new Callable<ModelAndView>() {
            @Override
            public ModelAndView call() throws Exception {
                ModelAndView mav = new ModelAndView("customermodels/upload").addAllObjects(getModelAttributes(memberType));
                InputStream is = null;
                try {
                    is = form.getFile().getInputStream();
                    if (fileName.endsWith("xlsx")) {
                        Timer timer = new Timer().start();
                        _LOG.info("[Member Upload] Started Processing file {}, Operation {}, Member Type Code {}", fileName,
                            form.getOperation(), form.getMemberTypeCode());
                        Integer totalMembers =
                            excelMemberTemplate.parseMembers(is, form.getMemberTypeCode(), locale, form.getOperation());
                        String localeMemberString = messageSource.getMessage("members", null, locale);
                        mav.addObject(KEY_SUCCESS, messageSource.getMessage("generic.save.successful",
                            new String[]{totalMembers + " " + localeMemberString}, locale));
                        timer.stop();
                        _LOG.info("[Member Upload] Done Processing file {}, Operation {}, Member Type Code {}. Total time: {}",
                            fileName,
                            form.getOperation(), form.getMemberTypeCode(), timer.elapseTime());
                    } else {
                        mav.addObject(KEY_ERROR,
                            messageSource.getMessage("member.template.messages.excel.extension.invalid", null, locale));
                    }
                } catch (InvalidFormatException e) {
                    _LOG.error("Failed to read file.", fileName, e);
                    mav.addObject(KEY_ERROR,
                        messageSource.getMessage("member.template.messages.excel.read.fail", new String[]{fileName}, locale));
                } catch (IOException e) {
                    _LOG.error("Failed to read file.", fileName, e);
                    mav.addObject(KEY_ERROR,
                        messageSource.getMessage("member.template.messages.excel.read.fail", new String[]{fileName}, locale));
                } catch (MemberTemplateParseException e) {
                    _LOG.error("Failed to read parse {}.", fileName, e);
                    String message = e.getMessage().replaceAll("\n", "<br />");
                    mav.addObject(KEY_ERROR, message);
                } finally {
                    IOUtils.closeQuietly(is);
                }
                return mav;
            }
        };
    }
}
