<%@ include file="../../common/taglibs.jsp" %>

<style type="text/css">
<!--
  .table tbody tr td:last-child {
   width: 16%;
    }
  
-->
</style>
	
	<spring:message var="typeName" code='label_group_membergroup' />
	<spring:url value="/resources/images/show.png" var="show_image_url" />
	<spring:url value="/resources/images/update.png" var="update_image_url" />
	<spring:url value="/resources/images/delete.png" var="delete_image_url" />

	<div class="page-header page-header2">
		<h1><spring:message code="menutab_marketingmgmt_campaign_customergroup" /></h1>
	</div>

	<div class="contentLink">
		<button class="btn btn-primary pull-right mb20"
			onclick="javascript: createGroup( '${pageContext.request.queryString}' );">
			<spring:message code="global_menu_new" arguments="${ typeName }" />
		</button>
	</div>
  
  <div class="modal hide groupModal nofly" tabindex="-1" role="dialog" aria-hidden="true" id="form_export"></div>
  
  <div class="clearfix"></div>
  
  <div id="content_groupList">

  <div id="list_group"></div>
  </div>
  
  <div class="hide" id="btnContainer">
    <div id="btnView">
    <spring:message code="label_group_view_members" var="view_label" htmlEscape="false" />
      <a id="" href="#"
      onclick="javascript: viewMembers( '%ITEMID%', '%ITEMNAME%' );"
      alt="${fn:escapeXml(view_label)}"
      title="${fn:escapeXml(view_label)}"
      class="tiptip btn icn-view pull-left">
    </a>
    </div>
    
    <div id="btnUpdate">
    <spring:message arguments="${typeName}"
        code="entity_update" var="update_label" htmlEscape="false" /><a
      title="${fn:escapeXml(update_label)}"
      alt="${fn:escapeXml(update_label)}"
      onclick="javascript: editGroup( '%ITEMID%', '${pageContext.request.contextPath}' );"
      href="#" id=""
      class="tiptip btn icn-edit pull-left"></a>
      </div>
      
    <div id="btnPrint">
      <input type="button" id="print" data-url="<c:url value="groups/member/print" />/%ITEMID%" value="<spring:message code="label_export" />" class="btn printBtn icn-print tiptip pull-left" title="<spring:message code="label_export" />" />
    </div>
    
      <div id="btnDelete">
      <spring:url
        value="/groups/member/%ITEMID%" var="delete_form_url" /> <form:form
        action="${delete_form_url}" id="deleteForm%ITEMID%" method="DELETE" cssClass="form-reset">
        <spring:message arguments="${typeName}" code="entity_delete"
          var="delete_label" htmlEscape="false" />
        <c:set var="delete_confirm_msg">
          <spring:escapeBody javaScriptEscape="true">
            <spring:message code="entity_delete_confirm" />
          </spring:escapeBody>
        </c:set>
                            
                            <input class="btn btn-primary icn-delete tiptip" type="button" alt="${fn:escapeXml(delete_label)}" title="${fn:escapeXml(delete_label)}" value="${delete_label}" onclick="getConfirm('${delete_confirm_msg}',function(result) {if(result) $('form#deleteForm%ITEMID%').submit();});" />
                            
        <c:if test="${not empty param.page}">
          <input name="page" type="hidden" value="1" />
        </c:if>
        <c:if test="${not empty param.size}">
          <input name="size" type="hidden"
            value="${fn:escapeXml(param.size)}" />
        </c:if>
      </form:form>
      </div>
  </div>
	
  <jsp:include page="membergroupform.jsp" />
  <jsp:include page="memberlist.jsp" />
  <jsp:include page="../../common/confirm.jsp" />
  
  <link href="<spring:url value="/css/dataTables.css" />" rel="stylesheet">
  <script src="<spring:url value="/js/jquery/jquery.dataTables.js" />" ></script>
  <script src="<spring:url value="/js/jquery/jquery.dataTables.linkLengthMenu.js" />"></script>
  <script src="<spring:url value="/js/jquery/jquery.dataTables.customPaging.js" />"></script>
  <script src="<spring:url value="/js/jquery/ajax-datatable.js" />" ></script>
  <script src="<spring:url value="/js/common/form2js.js" />" ></script>
  <script src="<spring:url value="/js/common/json2.js" />" ></script>
  <script src="<spring:url value="/js/jquery/jquery.toObject.js" />" ></script>
  <script src="<spring:url value="/js/jquery/jquery.numberInput.js" />" ></script>
	
<spring:url value="/styles/temp.css" var="temp_css"/>
<link href="${temp_css}" rel="stylesheet" />    

  <script>
    var columnHeaders = [
      '<spring:message code="label_group_groupname" />',
      ''
    ];
    
    var modelFields = [
      {name : 'name', sortable : true, fieldCellRenderer: function(data, type, row) {
            return row.cardTypeGroupCode ? row.name + " (" + row.cardTypeGroupCode + ")" : row.name;
      }},
      {customCell : function ( innerData, sSpecific, json ) {
          if ( sSpecific == 'display' ) {
        	  var btnHtml = $("#btnView").html();
        	  btnHtml += $("#btnPrint").html();
        	  if ( !json.isUsed ) {
                  if( json.members == null || json.members == "" ) {
                      btnHtml += $("#btnUpdate").html();                
                  }
                  btnHtml += $("#btnDelete").html();
        	  }
              var rg = new RegExp("%ITEMID%", 'g');
              btnHtml = btnHtml.replace(rg, json.id);
              rg = new RegExp("%ITEMNAME%", 'g');
              btnHtml = btnHtml.replace(rg, json.name);
              return btnHtml;
            } else {
              return '';
            }
            }
       }
    ];
    
    $list = $("#list_group");
    
    $list.ajaxDataTable({
      'autoload'  : true,
      'ajaxSource' : '<c:url value="/groups/member/search/list" />',
      'columnHeaders' : columnHeaders,
      'modelFields' : modelFields,
    })
    .on("click", "#print", function(){
  	  $.get(ctx+"/"+$(this).data("url") , function(response) {
        $("#form_export").modal('show').html(response);
        $("#form_export").on('hidden.bs.modal', function() {
          $("#form_export").empty();
        });
      }, "html");
    });

  </script>
