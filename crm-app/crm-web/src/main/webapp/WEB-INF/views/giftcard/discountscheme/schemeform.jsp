<%@ include file="../../common/taglibs.jsp" %>

<style type="text/css">
<!--

  #productTable tr td:last-child {
    border-left: 0px;
  }

  #addProductBtn.btn-small {
    margin-bottom: 5px;
  }
  table select, textarea, table input[type="text"], input[type="password"], input[type="datetime"], input[type="datetime-local"], input[type="date"], input[type="month"], input[type="time"], input[type="week"], input[type="number"], input[type="email"], input[type="url"], input[type="search"], input[type="tel"], input[type="color"], .uneditable-input {
      margin-bottom: 0px;
      margin-top: 0px;
  }
  
-->
</style>



<c:set var="ENCTYPE" value="application/x-www-form-urlencoded"/>

<div class="modal-dialog">
  <div class="modal-content">
    <spring:message code="label_manufacture_order" var="typeName" />
    
    <div class="modal-header">
      <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
      <h4><spring:message code="gc_dcscheme"/></h4>
    </div>
 
    <div class="modal-body">
    
      <div id="contentError" class="hide alert alert-error">
        <button type="button" class="close" onclick="$(this).parent().hide();">&times;</button>
        <div><form:errors path="*"/></div>
      </div>
    
      
      <form:form id="scheme" name="scheme" modelAttribute="scheme" action="${pageContext.request.contextPath}/giftcard/discountscheme/form" method="POST" enctype="${ENCTYPE}" cssClass="form-reset form-horizontal" >
      <form:hidden path="id"/>
      
      <div class="contentMain">
      
      <div class="row-fluid">
      
        <div class="span6">
        <div class="control-group">
          <spring:message code="gc_dcscheme_customer" var="customer" />
          <label for="customerId" class="control-label"><b class="required">*</b> ${customer}</label> 
          <div class="controls">
            <form:select path="customerId" items="${customers}" itemLabel="name" itemValue="id" />
          </div>
        </div>
        
        <div class="control-group">
          <spring:message code="gc_dcscheme_minpurchase" var="minPurchase" />
          <label for="minPurchase" class="control-label"><b class="required">*</b> ${minPurchase}</label> 
          <div class="controls">
            <form:input path="minPurchase" class="form-control floatInput" placeholder="${minPurchase}" />
          </div>
        </div>
        
        <div class="control-group">
          <spring:message code="gc_dcscheme_requiredaccumulation" var="purchaseMaxDisc" />
          <label for="purchaseForMaxDiscount" class="control-label"><b class="required">*</b> ${purchaseMaxDisc}</label> 
          <div class="controls">
            <form:input path="purchaseForMaxDiscount" class="form-control floatInput" placeholder="${purchaseMaxDisc}" />
          </div>
        </div>
        
        
        
        <div class="control-group">
          <spring:message code="gc_dcscheme_discounttype" var="discountType" />
          <label for="discountType" class="control-label"><b class="required">*</b> ${discountType}</label> 
          <div class="controls">
            <form:select path="discountType" items="${discountTypes}" id="discountType" class="form-control" />
          </div>
        </div>
        </div>
        
        <div class="span6">
        
        <div class="control-group field hideOnRegYrlyDisc">
          <spring:message code="gc_dcscheme_startingdiscount" var="startingDisc" />
          <label for="startingDiscount" class="control-label"><b class="required">*</b> ${startingDisc}</label> 
          <div class="controls">
            <form:input path="startingDiscount" class="form-control floatInput" placeholder="${startingDisc}" />
          </div>
        </div>
        
        <div class="control-group">
          <spring:message code="gc_dcscheme_endingdiscount" var="endingDisc" />
          <label for="endingDiscount" class="control-label"><b class="required">*</b> ${endingDisc}</label> 
          <div class="controls">
            <form:input path="endingDiscount" class="form-control floatInput" placeholder="${endingDisc}" />
          </div>
        </div>
        
        <div class="control-group">
          <spring:message code="gc_dcscheme_startdate" var="startYr" />
          <label for="startDate" class="control-label"><b class="required">*</b> ${startYr}</label> 
          <div class="controls">
            <form:input path="startDate" id="startDate" class="form-control yearInput" placeholder="${startYr}" />
          </div>
        </div>
        
        <div class="control-group">
          <spring:message code="gc_dcscheme_enddate" var="endYr" />
          <label for="endDate" class="control-label"><b class="required">*</b> ${endYr}</label> 
          <div class="controls">
            <form:input path="endDate" id="endDate" class="form-control yearInput" placeholder="${endYr}" />
          </div>
        </div>
        <div class="control-group">
          <label class="control-label">&nbsp;</label> 
          <div class="controls">
            <div class="span checkbox">
              <form:checkbox path="includeBelowMinPurchase" /><label for="includeBelowMinPurchase"><spring:message code="gc_dcscheme_include_below_min" /></label>
            </div>
          </div>
        </div>
        </div>
      
        
        
        
       
        
        
        
      </div>
      
      
          
      </div>
      
      </form:form>
    
    </div>
    <div class="modal-footer">
      <button type="button" id="submitBtn" class="btn btn-primary"><spring:message code="label_save" /></button>
      <button type="button" id="" class="btn btn-default" data-dismiss="modal"><spring:message code="label_cancel"/></button>
    </div>
  </div>
</div>

<script>
$(document).ready(function() {
  
	$schemeForm = $("#scheme");
	$schemeDialog = $("#schemeDialog")
	$submitBtn = $("#submitBtn");
	
	var CONTAINER_ERROR = "#contentError > div";
	var CONTENT_ERROR = "#contentError";
	
	$submitBtn.click(submitForm);
	
	$("#discountType")
		.each(toggleFields)
		.change(toggleFields);
	
	function toggleFields() {
		var discType = $(this).val();
		$(".field").show();
		if(discType == 'REGULAR_YEARLY_DISCOUNT') {
			$(".hideOnRegYrlyDisc").hide();
			$(".hideOnRegYrlyDisc").find("input").val("");
		}
	}
	
	function submitForm(e) {
		e.preventDefault();
		$submitBtn.prop( "disabled", true );
		$.post($schemeForm.attr("action"), $schemeForm.serialize(), function(data) {
			$submitBtn.prop( "disabled", false );
			if (data.success) {
				location.reload();
				$schemeDialog.modal("hide");
			} 
			else {
				errorInfo = "";
				for (i = 0; i < data.result.length; i++) {
					errorInfo += "<br>" + (i + 1) + ". "
							+ ( data.result[i].code != undefined ? 
									data.result[i].code : data.result[i]);
				}
				$schemeDialog.find(CONTAINER_ERROR).html("Please correct following errors: " + errorInfo);
				$schemeDialog.find(CONTENT_ERROR).show('slow');
			}
		});	
	}
	
    
    initFields($schemeDialog);
	

	
	function initFields(e) {
    	$('.quantity', $(e)).numberInput({"type" : "int"});
        $('.intInput', $(e)).numberInput({"type" : "int"});
    	$('.floatInput', $(e)).numberInput({"type" : "float"});
    	
    	$("input.yearInput").datepicker({
    		format: 'dd-mm-yyyy',
    		autoclose: true
    	})
    	
    	$("#startDate").datepicker({
  	    	autoclose : true,
  			format: 'dd-mm-yyyy'
  		}).on('changeDate', function(selected){
  			startDate = new Date(selected.date.valueOf());
  	        startDate.setDate(startDate.getDate(new Date(selected.date.valueOf())));
  	        $('#endDate').datepicker('setStartDate', startDate);
  		});
  		$("#endDate").datepicker({
  		  	autoclose : true,
  			format: 'dd-mm-yyyy'
  		}).on('changeDate', function(selected){
  			FromEndDate = new Date(selected.date.valueOf());
  	        FromEndDate.setDate(FromEndDate.getDate(new Date(selected.date.valueOf())));
  	        $('#startDate').datepicker('setEndDate', FromEndDate);
  		});
    }
  
});
</script>