<%@ include file="../../common/taglibs.jsp" %>

<spring:message var="groupName" code='label_group_groupname' />
<spring:message var="field" code='label_group_field' />
<spring:message var="value" code='label_group_value' />

<div id="content_productGroupForm" class="modal hide  nofly modal-dialog">

  <style type="text/css">
  <!-- 
    .form-search { margin-top: 30px; }
    .checkbox {width: 100px; padding-left: 0px;}
  -->
  </style>
  <div class="modal-content">
  <c:url var="url_action" value="${action}" />
  <c:url var="url_saveAction" value="${saveAction}" />
  <form:form id="productGroupForm" name="productGroup" data-save-action="${url_saveAction}" modelAttribute="productGroup" action="${url_action}" class="modal-form form-horizontal">

    <div class="modal-header">
      <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
      <h4>Edit/<c:out value="${headerLabel}" /></h4>
    </div>

    <div class="modal-body">
      <div class="hide alert alert-error" id="content_error"><button type="button" class="close" onclick="$(this).parent().hide();">&times;</button><div><form:errors path="*"/></div></div>

      <spring:message var="groupName" code='label_group_groupname'/>
      <div class="control-group">
        <label for="name" class="control-label"><b class="required">*</b> ${groupName}</label>
        <div class="controls"><form:input path="name" id="name" placeholder="${groupName}" /></div>
      </div>

      <div>
	      <div style="margin-left: 159px; width: 520px">
          <c:url var="url_getProductCfnList" value="/groups/product/list/productcfn?code=%CODE%&index=%IDX%&size=%SIZE%" />
	        <c:forEach var="productCfn" items="${productCfnOptions}" varStatus="stat">
          <div class="span checkbox" >
            <c:if test="${productCfn.product}"><c:set var="id_product" value="${productCfn.code}Option" /></c:if>
	          <input type="checkbox" value="${productCfn.code}" class="productCfnOption ui" id="${id_product}" 
              data-list-idx="0" data-list-size="7" data-init-idx="0" data-fn-scroll=false
              data-is-productcode="${productCfn.product}" 
              data-url="${url_getProductCfnList}"
              data-queue-num="${stat.index}" />
	          <label class="case-cap">${productCfn.description}</label>
	        </div>
	        </c:forEach>
	      </div>
      </div>

      <c:set var="classProduct" value="productCode" />
      <c:set var="classCfn" value="classificationCode" />

      <div id="filterPrdCfnForm" class="inline form-search">
        <c:url var="url_filterProductCfnList" value="/groups/product/list/productcfn/filter?code=%CODE%&prdcode=%PRDCODE%&cfncode=%CFNCODE%" />
        <c:url var="url_searchProductCfnList" value="/groups/product/list/productcfn/search?code=%CODE%&prdcode=%PRDCODE%&cfncode=%CFNCODE%" />        
        <span class="input-append">
          <input id="filterValue" class="search-query search_field" />
          <input id="filterButton" type="button" data-loading-text="Searching..." data-url="${url_filterProductCfnList}" data-url-search="${url_searchProductCfnList}" value="<spring:message code="label_search" />" class="btn btn-primary search_submit btnLoading"/>
        </span>
      </div>
      <div class="row-fluid" style="min-width: 690px">
        <fieldset class="span6 pull-left text-right text-center">
          <label for="classificationSelections" class="control-label" id="label_accountId"><spring:message code="group_product_andclassifications"/></label>
          <select id="classificationSelections" multiple="multiple" class="span12" data-class-product="${classProduct}" data-class-cfn="${classCfn}"></select>
        </fieldset>

        <fieldset class="cont-02" style="margin-top: 30px">
          <div><input type="button" id="addProductCfnBtn" class="btn" value=">>"/></div>
          <div><input type="button" id="removeProductCfnBtn" class="btn" value="<<"/></div>          
        </fieldset>

        <fieldset class="span5 pull-left">
          <c:url var="url_getProductList" value="/groups/product/list/products?prdcode=%PRDCODE%&cfncode=%CFNCODE%&index=%IDX%&size=%SIZE%" />
          <label for="itemCodeDesc" class="control-label" id="label_accountId" style="width: auto;"><spring:message code="label_group_selected"/></label>
          <select name="itemCodeDesc" id="itemCodeDesc" class="span12" data-url="${url_getProductList}" multiple="multiple">
            <c:forEach var="item" items="${productGroup.itemCodeDesc}">
              <c:if test="${item.product}"><c:set var="classname" value="${classProduct}" /></c:if>
              <c:if test="${!item.product}"><c:set var="classname" value="${classCfn}" /></c:if>
              <option value="${item.code}" class="${classname}">${item.sku} - ${item.description}</option>
            </c:forEach>
          </select>
          <form:hidden path="products" id="products" />
          <form:hidden path="productsCfns" id="productsCfns" />
          <form:hidden path="excludedProducts" id="excludedProducts" />
        </fieldset>
        <div class="clearfix"></div>
      </div>

      <div id="filterPrdForm" class="inline form-search">
        <c:url var="url_filterProductList" value="/groups/product/list/productcfn/filter?code=%CODE%&prdcode=%PRDCODE%&cfncode=%CFNCODE%" />
        <c:url var="url_searchProductList" value="/groups/product/list/product/search?code=%CODE%&prdcode=%PRDCODE%&cfncode=%CFNCODE%" />
        <span class="input-append">
          <input id="filterValue" class="search-query search_field" />
          <input id="filterButton" type="button" data-loading-text="Searching..." data-url="${url_filterProductList}" data-url-search="${url_searchProductList}" value="<spring:message code="label_search" />" class="btn btn-primary search_submit btnLoading"/>
        </span>
      </div>
      <div class="row-fluid  pb20" style="min-width: 690px">
        <fieldset class="span6 pull-left text-right text-center">
          <label for="productSelections" class="control-label" id="label_accountId" style="text-align: left; width: auto;"><spring:message code="group_product_selections"/></label>
          <select id="productSelections" multiple="multiple" class="span12"></select>
        </fieldset>
        <fieldset class="cont-02" style="margin-top: 30px">
          <div><input type="button" id="addExcProductBtn" class="btn" value=">>"/></div>
          <div><input type="button" id="removeExcProductBtn" class="btn" value="<<"/></div>          
        </fieldset>
        <fieldset class="span5 pull-left">
          <label for="excProductCodeDesc" class="control-label" id="label_accountId" style="text-align: left; width: auto;"><spring:message code="label_group_excluded"/></label>
          <select name="excProductCodeDesc" id="excProductCodeDesc" class="span12" multiple="multiple">
            <c:forEach var="item" items="${productGroup.excProductCodeDesc}">
              <option value="${item.code}">${item.code} - ${item.description}</option>
            </c:forEach>
          </select>
        </fieldset>
        <div class="clearfix"></div>
      </div>
    </div>

    <div class="modal-footer">
      <button type="saveButton" id="button_save" data-loading-text="Searching..." class="btn btn-primary btnLoading"><spring:message code="label_save" /></button>
      <button type="button" class="btn" data-dismiss="modal" id="cancelButton"><spring:message code="label_cancel" /></button>
    </div>

  </form:form>
  </div>


  <script src='<c:url value="/js/viewspecific/groups/product/productGroupForm.js"/>' type='text/javascript'><![CDATA[&nbsp; ]]></script>
  <script src='<c:url value="/js/jquery/jquery.ajaxQueue.js"/>' type='text/javascript'><![CDATA[&nbsp; ]]></script>
  <link href="<c:url value="/styles/temp.css" />" rel="stylesheet" />


</div>