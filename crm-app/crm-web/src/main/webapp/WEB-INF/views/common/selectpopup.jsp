<%@ include file="taglibs.jsp" %>

<style type="text/css">
<!--

  .well .input {
    background-color: #fff;
    border: 1px solid #E8E8E8;
    box-shadow: none;    
    padding: 6px;
    height: 18px;
    margin-left: 5px;
  }
  .well .btn-primary {
    margin-left: 10px;
  }
  .well select {
    margin: 5px;
  }

  .search_list_container .table tbody tr td:last-child {
      border-left: 0px;
  }
  
  .search_list_container input[type="radio"] {
    margin-bottom: 3px !important;
  }
-->
</style>

<div id="selectPopupTemplate">
<div class="modal hide  nofly" id="selectPopupModal" role="dialog" aria-hidden="true" data-backdrop="static" >
<div class="modal-dialog">
  <div class="modal-content">
    <div class="modal-header">
      <button type="button" name="selectPopupClose" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
      <h4><span id="selectPopupTitle"></span></h4>
    </div>
    <div class="modal-body">
      <div class="well">
        <div class="form-inline form-search" id="searchForm">
          <label class="label-single"><spring:message code="label_search" />:</label>
          <select id="criteria" class="searchField input-medium">
          </select>
          <input type="text" id="criteriaValue" class="input input-medium searchField" placeholder="<spring:message code="label_search" />"/>
          <input id="search" type="button" class="btn btn-primary" value="<spring:message code="button_search"/>"/>
          <input type="button" id="clear" value="<spring:message code="label_clear" />" class="btn custom-reset" data-default-id="search" />
        </div>
      </div>
      <div id="search_list_container" class="search_list_container">
      </div>
    </div>
    <div class="modal-footer">
      <button type="button" id="submit" class="btn btn-primary"><spring:message code="label_select_noargs" /></button>
      <button type="button" name="selectPopupClose" class="btn btn-default" data-dismiss="modal"><spring:message code="label_cancel"/></button>
    </div>
</div>
</div>
</div>
</div>


<script>
	var SelectPopup = {
			
			init: function(elem, param) {
				var self = this;
				
				var modalHtml = $("#selectPopupTemplate").html();
				self.field = param.field;
				
				var modalId = "selectPopupModal" + self.field; 
				var rg = new RegExp("selectPopupModal", 'g');
				modalHtml = modalHtml.replace(rg, modalId);

				$(modalHtml).appendTo("body");
				
				self.objProps = param.objProps;
				self.columnHeaders = param.columnHeaders;
				self.modelFields = param.modelFields;
				self.url = param.url;
				self.fieldName = param.field;
				self.$searchModal = $("#" + modalId);
				self.$searchForm = $("#searchForm", self.$searchModal);
				self.$elem = $(elem);
				self.elem = elem;
				self.callback = param.callback;
				
				$("#selectPopupTitle", self.$searchModal).text(param.title);
				$("[name=selectPopupClose]", self.$searchModal).click(function () {
				  var event = $.Event('selectpopupmodalhide');
  				$('#selectPopupModal').trigger(event);
				});
				
				self._initDataTables();
				self._initSearch();
				self._initSubmit();
				
				self.$elem.click(function(e) {
					e.preventDefault();
					self.show();
				});
			},
			
			_initDataTables : function() {
				var self = this;
				var criteriaHtml = "";
				for (var i = 0; i < self.columnHeaders.length; i++) {
					criteriaHtml += "<option value=\"\" data-field-name=\""+self.modelFields[i].name+"\" >"+self.columnHeaders[i]+"</option>";					
				}
				$("select#criteria", self.$searchForm).html(criteriaHtml);
				
				var radio = function ( innerData, sSpecific, json ) {
					if (sSpecific == 'display') {
						var html = "<input type=\"radio\" name=\"searchResult\" value=\"\" ";
						for (var i = 0; i < self.objProps.length; i++) {
							var prop = self.objProps[i];
							var jsonProp = json[prop] == null ? '' : json[prop];
						    html += "data-" + prop + "=\"" + jsonProp + "\" ";
						}
						html += " />";
						return html;
					} else {
						return "";
					}
				};
				
				self.columnHeaders.unshift("");
				self.modelFields.unshift({customCell : radio});
				
				$("#search_list_container", self.$searchModal).empty().ajaxDataTable({
					'autoload'  	: false,
					'ajaxSource' 	: self.url,
					'columnHeaders' : self.columnHeaders,
					'modelFields' 	: self.modelFields,
				});
			},
			
			_initSearch : function() {
				var self = this;
				$("#search", self.$searchForm).click(function() {
					$("#criteriaValue", self.$searchForm).attr("name", $("#criteria", self.$searchForm).find("option:selected").data("fieldName"));
					var formDataObj = self.$searchForm.toObject( { mode : 'first', skipEmpty : true } );
					console.log(formDataObj);
					$("#search_list_container", self.$searchModal).ajaxDataTable('search', formDataObj);
				});	
				
				$("#clear", self.$searchForm).click(function() {
					$(".searchField", self.$searchForm).val("");
				});
			},
			
			_initSubmit: function() {
				var self = this;
				$("#submit", self.$searchModal).click(function() {
					var options = new Object();
					$selected = $("input[name='searchResult']:checked", self.$searchModal);
					for (var i = 0; i < self.objProps.length; i++) {
						var prop = self.objProps[i];
						options[prop] = $selected.data(prop.toLowerCase());
					}
					self.callback(options);
					self.$searchModal.modal("hide");
					
					var event = $.Event('selectpopupmodalhide');
  				$('#selectPopupModal').trigger(event);	
				});
			},
			
			show: function() {
				var self = this;
				$(".searchField", self.$searchForm).val("");
				$("#search", self.$searchForm).click();
				self.$searchModal.modal("show");

				var event = $.Event('selectpopupmodalshow');
				$('#selectPopupModal').trigger(event);
			}
	
	
	
			
			
	};
	
	$.fn.selectPopup = function (options) {
		var args = arguments;
		if(SelectPopup[options]) {
			return SelectPopup[options].apply($(this).data("selectPopupInstance"), Array.prototype.slice.call(args, 1));
		} else if ( typeof options === 'object' || !options) {
			options.field = $(this).attr("name") != "" || $(this).attr("name") != null ? $(this).attr("name") : $(this).attr("id");
			var selectPopupIns = Object.create( SelectPopup );
			SelectPopup.init.call( selectPopupIns,$(this), options );
	        $(this).data( 'selectPopupInstance', selectPopupIns );
		}
    };
	
	/* var options = {
			columnHeaders : [
		           	      "<spring:message code="gc.cxprofile.cx.id"/>",
		        	      "<spring:message code="gc.cxprofile.cx.name"/>"
			        	    ],
			        	    
      	    modelFields : [
		         	      {name : 'customerId', sortable : true},
		        	      {name : 'contactFullName', sortable: true}
		        	      ],
    	    objProps : ["customerId", "contactFullName", "contactNo", "email"],
    	    url : "<c:url value="/giftcard/customer/profile/list" />",
    	    callback : function(options) {
    	    	var $customerId = $( "#customerIdField" );
    	    	$("#typeahead").val(options[ "customerId" ]);
    	    	$customerId.val(options[ "customerId" ]);
    	    	$customerId.data( "contact-fullname", options[ "contactFullName" ] );
    		    $customerId.data( "contact-no", options[ "contactNo" ] );
    		    $customerId.data( "email", options[ "email" ] );
    		    $customerId.change();
    	    }
    	    
	};
	
	$("#typeahead").selectPopup(options); */
</script>

<script type="text/javascript">

$(document).ready(function() {

    $('#selectPopupModal').on('selectpopupmodalshow', function() {
      $('#orderDialog').css('opacity', .9);
      $('#orderDialog').find(':input').prop('disabled', true);
      $('#orderDialog').find(':button').prop('disabled', true);
    });
    $('#selectPopupModal').on('selectpopupmodalhide', function() {
      $('#orderDialog').css('opacity', 1);
      $('#orderDialog').find(':input').prop('disabled', false);
      $('#orderDialog').find(':button').prop('disabled', false);
    });

});

</script>