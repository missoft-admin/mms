package com.transretail.crm.core.service.impl;

import com.mysema.query.BooleanBuilder;
import com.mysema.query.types.expr.BooleanExpression;
import com.transretail.crm.common.service.util.SpringDataPagingUtil;
import com.transretail.crm.core.dto.MemberDto;
import com.transretail.crm.core.dto.MemberResultList;
import com.transretail.crm.core.dto.MemberSearchDto;
import com.transretail.crm.core.entity.MemberModel;
import com.transretail.crm.core.entity.QMemberModel;
import com.transretail.crm.core.entity.enums.MemberStatus;
import com.transretail.crm.core.entity.lookup.Store;
import com.transretail.crm.core.repo.EmployeePurchaseTxnRepo;
import com.transretail.crm.core.repo.MemberMonitorConfigRepo;
import com.transretail.crm.core.repo.MemberRepo;
import com.transretail.crm.core.repo.StoreRepo;
import com.transretail.crm.core.security.util.UserUtil;
import com.transretail.crm.core.service.CodePropertiesService;
import com.transretail.crm.core.service.EmployeeService;
import com.transretail.crm.core.service.LookupService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;


@Service("employeeService")
@Transactional
public class EmployeeServiceImpl implements EmployeeService {

    @Autowired
    MemberRepo memberRepo;
    
    @Autowired
    StoreRepo storeRepo;

    @Autowired
    EmployeePurchaseTxnRepo employeePurchaseTxnRepo;

    @Autowired
    private CodePropertiesService codePropertiesService;
    
    @Autowired
    private MemberMonitorConfigRepo memberMonitorRepo;
    
    @Autowired
    private LookupService lookupService;
    
    private List<MemberDto> convertModelToDtoList(Iterable<MemberModel> employeeModels) {
    	List<MemberDto> employeeDtos = new ArrayList<MemberDto>();
    	for(MemberModel employeeModel : employeeModels) {
    		employeeDtos.add(new MemberDto(employeeModel));
    	}
    	return employeeDtos;
    }
    
    @Override
    public MemberResultList listEmployeeDto(MemberSearchDto searchDto) {
    	QMemberModel qModel = QMemberModel.memberModel;
    	Pageable pageable = SpringDataPagingUtil.INSTANCE.createPageable(searchDto.getPagination());
    	BooleanExpression filter = qModel.memberType.code.eq(codePropertiesService.getDetailMemberTypeEmployee()).and(searchDto.createSearchExpression());
        Page<MemberModel> page = filter != null ? memberRepo.findAll(filter, pageable) : memberRepo.findAll(pageable);
        List<MemberDto> list = convertModelToDtoList(page.getContent());
        for(MemberDto member : list) {
        	member.setTotalTransactionAmount(employeePurchaseTxnRepo.findTotalTxnAmountByAccountId(member.getAccountId(), null, null));
        	member.setCurrentTotalTransactionAmount(employeePurchaseTxnRepo.findTotalTxnAmountByAccountId(member.getAccountId(), Calendar.getInstance(), Calendar.getInstance()));
        	if(StringUtils.isNotBlank(member.getRegisteredStore())) {
    			Store store = storeRepo.findByCode(member.getRegisteredStore());
    			if(store != null)
    				member.setRegisteredStoreName(store.getName());	
    		}
    	}
        return new MemberResultList(list, page.getTotalElements(), page.hasPreviousPage(),
            page.hasNextPage());
    }


    @Override
    public MemberResultList listDto( MemberSearchDto searchDto, Calendar txnDateFrom, Calendar txnDateTo ) {
    	QMemberModel qModel = QMemberModel.memberModel;
    	Pageable pageable = SpringDataPagingUtil.INSTANCE.createPageable(searchDto.getPagination());
    	BooleanExpression filter = qModel.memberType.code.eq(codePropertiesService.getDetailMemberTypeEmployee()).and(searchDto.createSearchExpression());
        Page<MemberModel> page = filter != null ? memberRepo.findAll(filter, pageable) : memberRepo.findAll(pageable);
        List<MemberDto> list = convertModelToDtoList(page.getContent());
        for(MemberDto member : list) {
        	member.setTotalTransactionAmount(employeePurchaseTxnRepo.findTotalTxnAmountByAccountId(member.getAccountId(), txnDateFrom, txnDateTo ));
        	member.setCurrentTotalTransactionAmount(employeePurchaseTxnRepo.findTotalTxnAmountByAccountId(member.getAccountId(), null, null));
        	if(StringUtils.isNotBlank(member.getRegisteredStore())) {
    			Store store = storeRepo.findByCode(member.getRegisteredStore());
    			if(store != null)
    				member.setRegisteredStoreName(store.getName());	
    		}
    	}
        return new MemberResultList(list, page.getTotalElements(), page.hasPreviousPage(),
            page.hasNextPage());
    }

    @Override
    public List<MemberDto> findAllEmployees() {
    	QMemberModel memberModel = QMemberModel.memberModel;
    	List<MemberDto> theMembers = convertModelToDtoList(memberRepo.findAll(memberModel.memberType.code.eq(codePropertiesService.getDetailMemberTypeEmployee())));
    	for(MemberDto theMember : theMembers) {
    		theMember.setTotalTransactionAmount(employeePurchaseTxnRepo.findTotalTxnAmountByAccountId(theMember.getAccountId(), null, null));
    	}
    	return theMembers;
    }

    @Override
    public List<MemberDto> findEmployeeEntries(int firstResult, int maxResults) {
    	QMemberModel memberModel = QMemberModel.memberModel;
    	List<MemberDto> theMembers = convertModelToDtoList(memberRepo.findAll(memberModel.memberType.code.eq(codePropertiesService.getDetailMemberTypeEmployee()), new PageRequest(firstResult / maxResults, maxResults)));
    	for(MemberDto theMember : theMembers) {
    		theMember.setTotalTransactionAmount(employeePurchaseTxnRepo.findTotalTxnAmountByAccountId(theMember.getAccountId(), null, null));
    	}
    	return theMembers;
    }
    
    @Override
    public long countAllEmployees() {
    	QMemberModel memberModel = QMemberModel.memberModel;
        return memberRepo.count(memberModel.memberType.code.eq(codePropertiesService.getDetailMemberTypeEmployee()));
    }
    
    @Override
    public long countAllEmployeesAsIndividuals() {
        QMemberModel memberModel = QMemberModel.memberModel;
        return memberRepo.count(memberModel.memberType.code.eq(codePropertiesService.getDetailMemberTypeIndividual()));
    }
    
    @Override
    public long countAllEmployeesAsProfessionals() {
        QMemberModel memberModel = QMemberModel.memberModel;
        return memberRepo.count(memberModel.memberType.code.eq(codePropertiesService.getDetailMemberTypeProfessional()));
    }
    
    @Override
    public long countCurrentUserStoreMembers() {
        QMemberModel memberModel = QMemberModel.memberModel;
        String currentUserStore = UserUtil.getCurrentUser().getStoreCode();
        return memberRepo.count(memberModel.memberType.code.ne(codePropertiesService.getDetailMemberTypeEmployee())
                .and(memberModel.registeredStore.eq(currentUserStore)));
    }
    
    @Override
    public List<MemberDto> findByCriteria(MemberDto inModel, String employeeType) {
    	List<MemberDto> theModels = convertModelToDtoList(memberRepo.findEmployeeByCriteria(new MemberModel(inModel), employeeType));
    	for(MemberDto theMember : theModels) {
    		theMember.setTotalTransactionAmount(employeePurchaseTxnRepo.findTotalTxnAmountByAccountId(theMember.getAccountId(), null, null));
    	}
    	return theModels;
    	
    }

    @Override
    public MemberDto findEmployeeByAccountId(String accountId) {
    	QMemberModel memberModel = QMemberModel.memberModel;
    	MemberModel theMember = memberRepo.findOne(new BooleanBuilder().orAllOf(memberModel.accountId.eq(accountId),
    			memberModel.memberType.code.eq(codePropertiesService.getDetailMemberTypeEmployee())));
    	if(theMember != null)
    		return new MemberDto(theMember);
    	else
    		return null;
    }



    @Override
    public MemberDto findActiveEmployeeByAccountId(String accountId) {
    	return findEmployeeByAccountIdAndStatus(accountId, MemberStatus.ACTIVE);
    }
    
    @Override
    public MemberDto findEmployeeByAccountIdAndStatus(String accountId, MemberStatus status) {
    	QMemberModel memberModel = QMemberModel.memberModel;
    	MemberModel theMember = memberRepo.findOne(new BooleanBuilder().orAllOf(memberModel.accountId.eq(accountId),
    			memberModel.memberType.code.eq(codePropertiesService.getDetailMemberTypeEmployee()),
    			memberModel.accountStatus.eq(status)));
    	if(theMember != null)
    		return new MemberDto(theMember);
    	else
    		return null;
    }
}
