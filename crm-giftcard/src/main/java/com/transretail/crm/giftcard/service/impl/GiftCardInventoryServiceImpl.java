package com.transretail.crm.giftcard.service.impl;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Random;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import com.transretail.crm.giftcard.entity.*;
import org.apache.commons.lang3.BooleanUtils;
import org.hibernate.StatelessSession;
import org.joda.time.DateTime;
import org.joda.time.LocalDate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.MessageSource;
import org.springframework.context.annotation.Primary;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.core.task.AsyncTaskExecutor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.mysema.query.jpa.JPQLQuery;
import com.mysema.query.jpa.hibernate.HibernateUpdateClause;
import com.mysema.query.jpa.impl.JPAQuery;
import com.mysema.query.types.ConstructorExpression;
import com.mysema.query.types.Predicate;
import com.mysema.query.types.expr.BooleanExpression;
import com.transretail.crm.common.service.dto.request.PageSortDto;
import com.transretail.crm.common.service.dto.request.PagingParam;
import com.transretail.crm.common.service.dto.response.ResultList;
import com.transretail.crm.common.service.exception.GenericServiceException;
import com.transretail.crm.common.service.exception.MessageSourceResolvableException;
import com.transretail.crm.common.service.util.SpringDataPagingUtil;
import com.transretail.crm.core.dto.MemberDto;
import com.transretail.crm.core.dto.MemberGroupFieldDto;
import com.transretail.crm.core.entity.CrmForPeoplesoftConfig;
import com.transretail.crm.core.entity.CrmForPeoplesoftConfig.TransactionType;
import com.transretail.crm.core.entity.MemberGroup;
import com.transretail.crm.core.repo.CrmForPeoplesoftConfigRepo;
import com.transretail.crm.core.service.CodePropertiesService;
import com.transretail.crm.core.service.MemberGroupService;
import com.transretail.crm.core.service.StoreService;
import com.transretail.crm.giftcard.dto.GiftCardInventoryDto;
import com.transretail.crm.giftcard.dto.GiftCardInventoryResultList;
import com.transretail.crm.giftcard.dto.GiftCardInventorySaveDto;
import com.transretail.crm.giftcard.dto.GiftCardInventorySeriesSearchDto;
import com.transretail.crm.giftcard.dto.GiftCardInventorySummarizedDto;
import com.transretail.crm.giftcard.dto.GiftCardInventorySummarizedResultList;
import com.transretail.crm.giftcard.dto.GiftCardOrderAllocateDto;
import com.transretail.crm.giftcard.dto.GiftCardOrderItemDto;
import com.transretail.crm.giftcard.dto.GiftCardSeriesDto;
import com.transretail.crm.giftcard.dto.ProductProfileDto;
import com.transretail.crm.giftcard.dto.StockRequestDto;
import com.transretail.crm.giftcard.dto.StockRequestReceiveDto;
import com.transretail.crm.giftcard.entity.GiftCardInventoryStock.GCIStockStatus;
import com.transretail.crm.giftcard.entity.QGiftCardInventory;
import com.transretail.crm.giftcard.entity.support.GiftCardInventoryStatus;
import com.transretail.crm.giftcard.entity.support.GiftCardOrderStatus;
import com.transretail.crm.giftcard.repo.CrmForPeoplesoftRepo;
import com.transretail.crm.giftcard.repo.GiftCardInventoryRepo;
import com.transretail.crm.giftcard.repo.GiftCardOrderRepo;
import com.transretail.crm.giftcard.service.GiftCardInventoryService;
import com.transretail.crm.giftcard.service.GiftCardInventoryStockService;

/**
 * @author Allan G. Ramirez (agramirez@exist.com)
 */
@Service
@Primary
public class GiftCardInventoryServiceImpl implements GiftCardInventoryService {
	@PersistenceContext
	private EntityManager entityManager;
	@Autowired
	private GiftCardInventoryRepo inventoryRepo;
	@Autowired
	private StatelessSession statelessSession;
	@Autowired
	private CodePropertiesService propertiesService;
	@Autowired
	private GiftCardInventoryStockService inventoryStockService;
	@Autowired
	private StoreService storeService;
	@Autowired
	private GiftCardOrderRepo orderRepo;
	@Autowired
	private MessageSource messageSource;
	@Autowired
	private AsyncTaskExecutor asyncTaskExecutor;
	@Autowired
	private CrmForPeoplesoftRepo peoplesoftRepo;
	@Autowired
	private CrmForPeoplesoftConfigRepo peoplesoftConfigRepo;
	@Autowired
	private MemberGroupService memberGroupService;

	private static final Logger LOGGER = LoggerFactory.getLogger(GiftCardInventoryServiceImpl.class);

	@Value("#{'${store.headoffice}'}")
	private String headOfficeCode;
	@Value("#{'${hibernate.jdbc.batch_size}'}")
	private int batchSize;

	@Override
	@Transactional
	public GiftCardInventory save(GiftCardInventory gcInv) {
		if (gcInv == null) {
			return null;
		}
		return inventoryRepo.save(gcInv);
	}

	@Override
	@Transactional
	public GiftCardInventoryResultList getGiftCardInventories(GiftCardInventorySeriesSearchDto searchForm) {
		Pageable pageable = SpringDataPagingUtil.INSTANCE.createPageable(searchForm.getPagination());
		BooleanExpression filter = searchForm.createSearchExpression();
		Page<GiftCardInventory> page = filter != null ? inventoryRepo.findAll(filter, pageable) : inventoryRepo.findAll(pageable);
		return new GiftCardInventoryResultList(toDtos(page.getContent()), page.getTotalElements(), page.hasPreviousPage(),
				page.hasNextPage());
	}

	private List<GiftCardInventoryDto> toDtos(Collection<GiftCardInventory> list) {
		List<GiftCardInventoryDto> results = Lists.newArrayList();
		for (GiftCardInventory model : list) {
			results.add(new GiftCardInventoryDto(model));
		}
		return results;
	}

	@Override
	@Transactional(readOnly = true)
	public GiftCardInventorySummarizedResultList getSummarizedGiftCardInventories(GiftCardInventorySeriesSearchDto searchDto) {
		QGiftCardInventory qInventory = QGiftCardInventory.giftCardInventory;

		PagingParam pagination = searchDto.getPagination();

		JPQLQuery selectQuery = createSummarizedGcInventoryBaseQuery(qInventory);
		selectQuery = SpringDataPagingUtil.INSTANCE.applyPagination(selectQuery, pagination, GiftCardInventory.class);

		JPQLQuery countQuery = createSummarizedGcInventoryBaseQuery(qInventory);

		BooleanExpression expression = searchDto.createSearchExpression();
		if (expression != null) {
			selectQuery.where(expression);
			countQuery.where(expression);
		}

		List<GiftCardInventorySummarizedDto> list = selectQuery.list(ConstructorExpression.create(GiftCardInventorySummarizedDto.class,
				qInventory.profile.id,
				qInventory.productName,
				qInventory.faceValue,
				qInventory.status,
				qInventory.productName.count(),
				qInventory.batchNo,
				qInventory.series.min(),
				qInventory.series.max(),
				qInventory.location,
				qInventory.allocateTo));

		Long totalElements = countQuery.uniqueResult(qInventory.productName.countDistinct());

		return new GiftCardInventorySummarizedResultList(list, totalElements != null ? totalElements : 0, pagination.getPageNo(),
				pagination.getPageSize());
	}

	private JPQLQuery createSummarizedGcInventoryBaseQuery(QGiftCardInventory qInventory) {
		return new JPAQuery(entityManager).from(qInventory)
				.groupBy(qInventory.productName, qInventory.status,
						qInventory.location,
						qInventory.allocateTo,
						qInventory.batchNo, qInventory.profile.id, qInventory.faceValue);
	}

	@Override
	public void generateGiftCardInventory(final List<GiftCardOrderItemDto> items, final LocalDate orderDate, final Long orderId) {
		Runnable task = new Runnable() {
			public void run() {
				generateInventory(items, orderDate, orderId);
			}
		};
		asyncTaskExecutor.execute(task);
	}

	private void generateInventory(List<GiftCardOrderItemDto> items, LocalDate orderDate, Long orderId) {
		LOGGER.info("[GENERATE INVENTORY][STARTED on " + System.currentTimeMillis() + "] orderId : " + orderId + " orderDate: " + orderDate);

		GiftCardOrder order = orderRepo.findOne(orderId);
		order.setStatus(GiftCardOrderStatus.BARCODING);
		orderRepo.saveAndFlush(order);

		Map<Integer, BatchNoSeqNoTuple> batchNoSeqNoTupleMap = Maps.newHashMap();

		// Sort the items/giftcards first to make sure that duplicate items would be executed in order
		Collections.sort(items, new Comparator<GiftCardOrderItemDto>() {
			@Override
			public int compare(GiftCardOrderItemDto o1, GiftCardOrderItemDto o2) {
				return o1.getFaceValue().getCode().compareTo(o2.getFaceValue().getCode());
			}
		});

		int insertCounter = 0;
		for (GiftCardOrderItemDto itemDto : items) {
			String faceValueCodeStr = itemDto.getFaceValue().getCode();
			Integer faceValueCode = null;
			try {
				faceValueCode = Integer.parseInt(faceValueCodeStr.substring(Math.max(faceValueCodeStr.length() - 2, 0)));
			} catch (NumberFormatException e) {
				throw new GenericServiceException("Invalid face value code extracted from " + faceValueCodeStr + ".");
			}

			BigDecimal faceValue = new BigDecimal(itemDto.getFaceValue().getDescription());
			BatchNoSeqNoTuple batchNoSeqNoTuple = batchNoSeqNoTupleMap.get(faceValueCode);
			if (batchNoSeqNoTuple == null) {
				batchNoSeqNoTuple = new BatchNoSeqNoTuple();
				batchNoSeqNoTuple.currBatchNo = getLastBatchByOrderDate(orderDate, faceValueCodeStr);
				batchNoSeqNoTuple.currSeqNo = getLastSequenceByOrderDate(orderDate, faceValueCodeStr, batchNoSeqNoTuple.currBatchNo) + 1;
				if (batchNoSeqNoTuple.currSeqNo == 10000000) {
					batchNoSeqNoTuple.currSeqNo = 1;
					batchNoSeqNoTuple.currBatchNo++;
				}
				batchNoSeqNoTupleMap.put(faceValueCode, batchNoSeqNoTuple);
			}

			for (int i = 0; i < itemDto.getQuantity(); i++) {
				LocalDate orderDt = orderDate;
				int batchNo = batchNoSeqNoTuple.currBatchNo;

				if (batchNoSeqNoTuple.currBatchNo > 9) {
					orderDt = orderDate.plusDays(batchNoSeqNoTuple.currBatchNo / 10);
					batchNo = batchNoSeqNoTuple.currBatchNo % 10;
				}

				GiftCardInventory inventory = new GiftCardInventory();
				inventory.setProfile(new ProductProfile(itemDto.getProfileId()));
				inventory.setProductName(itemDto.getProductName());
				inventory.setProductCode(itemDto.getProductCode());
				inventory.setSeqNo(batchNoSeqNoTuple.currSeqNo);
				inventory.setBatchNo(batchNo);
				inventory.setOrderDate(orderDt);
				inventory.setFaceValue(faceValue);
				inventory.setOrder(new GiftCardOrder(orderId));
				inventory.setIsEgc(order.getIsEgc());
				inventory.setExpiryDate(itemDto.getExpiredDate());

				String seriesNo = createSeriesNo(inventory.getOrderDate(), inventory.getBatchNo(), faceValueCode, inventory.getSeqNo());
				inventory.setSeries(Long.valueOf(seriesNo));

				String barCode = generateBarcode(seriesNo + Integer.toString(new Random().nextInt(899) + 100));
				inventory.setBarcode(barCode);

				if (insertCounter < batchSize) {
					inventoryRepo.save(inventory);
					insertCounter++;
				} else {
					inventoryRepo.saveAndFlush(inventory);
					// Prevent OOM
					entityManager.clear();
					insertCounter = 0;
				}
				if (batchNoSeqNoTuple.currSeqNo == 9999999) {
					batchNoSeqNoTuple.currSeqNo = 1;
					batchNoSeqNoTuple.currBatchNo++;
				} else {
					batchNoSeqNoTuple.currSeqNo++;
				}
			}
		}

		order.setStatus(GiftCardOrderStatus.GENERATED);
		orderRepo.saveAndFlush(order);
		LOGGER.info("[GENERATE INVENTORY][FINISHED on " + System.currentTimeMillis() + "] orderId : " + orderId + " orderDate: " + orderDate);
	}

	private int getLastBatchByOrderDate(LocalDate orderDate, String faceValueCode) {
		QGiftCardInventory qInventory = QGiftCardInventory.giftCardInventory;
		Integer batchNo = new JPAQuery(entityManager).from(qInventory)
				.where(qInventory.orderDate.eq(orderDate).and(qInventory.profile.faceValue.code.eq(faceValueCode)))
				.singleResult(qInventory.batchNo.max());
		if (batchNo == null)
			batchNo = 0;
		return batchNo;
	}

	private int getLastSequenceByOrderDate(LocalDate orderDate, String faceValueCode, int batchNo) {
		QGiftCardInventory qInventory = QGiftCardInventory.giftCardInventory;
		Integer sequenceNo = new JPAQuery(entityManager).from(qInventory)
				.where(qInventory.orderDate.eq(orderDate).and(qInventory.profile.faceValue.code.eq(faceValueCode)).and(qInventory.batchNo.eq(batchNo)))
				.singleResult(qInventory.seqNo.max());
		if (sequenceNo == null)
			sequenceNo = 0;
		return sequenceNo;
	}

	@Override
	public String generateBarcode(String series) {
		int sumOfDigits = 0;
		int seriesLength = series.length();
		for (int i = 1; i <= seriesLength; i++) {
			if (i % 2 == 1) {
				int x2 = Character.getNumericValue(series.charAt(seriesLength - i)) * 2;
				if (x2 >= 10)
					sumOfDigits += x2 % 10 + x2 / 10;
				else
					sumOfDigits += x2;
			} else
				sumOfDigits += Character.getNumericValue(series.charAt(seriesLength - i));
		}
		int unitsDigit = sumOfDigits % 10;
		int checkSum = 0;
		if (unitsDigit != 0) {
			checkSum = 10 - unitsDigit;
		}
		return series + checkSum;
	}

	@Override
	@Transactional
	public void saveGiftCardOrderItems(List<GiftCardOrderItemDto> itemDtos, DateTime orderDate) {
		/*
		Map<Long, Tuple> gcTupleMap = Maps.newHashMap();
		Map<BigDecimal, BatchNoSeqNoTuple> batchNoSeqNoTupleMap = Maps.newHashMap();
		QGiftCard qGiftCard = QGiftCard.giftCard;
		
		// Sort the items/giftcards first to make sure that duplicate items would be executed in order
		Collections.sort(itemDtos, new Comparator<GiftCardOrderItemDto>() {
		@Override
		public int compare(GiftCardOrderItemDto o1, GiftCardOrderItemDto o2) {
		return o1.getGiftCardId().compareTo(o2.getGiftCardId());
		}
		});
		
		int insertCounter = 0;
		for (GiftCardOrderItemDto itemDto : itemDtos) {
		Tuple tuple = gcTupleMap.get(itemDto.getGiftCardId());
		if (tuple == null) {
		tuple = getGiftCardTuple(itemDto.getGiftCardId());
		gcTupleMap.put(itemDto.getGiftCardId(), tuple);
		}
		
		BigDecimal faceValue = tuple.get(qGiftCard.faceValue);
		if (faceValue == null) {
		throw new GenericServiceException(
		"Gift card [" + itemDto.getGiftCardId() + "] has no face value.");
		}
		BigDecimal faceValuePer100k =
		faceValue.divide(new BigDecimal(GiftCardInventory.FACE_VALUE_MULTIPLIER), 0, RoundingMode.DOWN);
		if (faceValuePer100k.intValue() > 9) {
		throw new GenericServiceException(
		"Gift card [" + itemDto.getGiftCardId() + "] facevalue is more than or equal to 1000k");
		}
		Integer randomNoDigits = tuple.get(qGiftCard.randomNoDigits);
		if (randomNoDigits == null) {
		throw new GenericServiceException(
		"Gift card [" + itemDto.getGiftCardId() + "] has no randomNoDigits value.");
		}
		String randomDigits = randomNoDigits.toString();
		for (int j = randomDigits.length(); j < 3; j++) {
		randomDigits = "0" + randomDigits;
		}
		
		BatchNoSeqNoTuple batchNoSeqNoTuple = batchNoSeqNoTupleMap.get(faceValuePer100k);
		if (batchNoSeqNoTuple == null) {
		batchNoSeqNoTuple = new BatchNoSeqNoTuple();
		batchNoSeqNoTuple.currBatchNo = 1;
		batchNoSeqNoTuple.currSeqNo = 1;
		batchNoSeqNoTupleMap.put(faceValuePer100k, batchNoSeqNoTuple);
		}
		
		for (int i = 0; i < itemDto.getQuantity(); i++) {
		GiftCardInventory inventory = new GiftCardInventory();
		inventory.setProduct(new GiftCard(itemDto.getGiftCardId()));
		inventory.setProductName(itemDto.getGiftCardName());
		inventory.setSeqNo(batchNoSeqNoTuple.currSeqNo);
		inventory.setBatchNo(batchNoSeqNoTuple.currBatchNo);
		inventory.setOrderDate(orderDate);
		inventory.setFaceValue(faceValue);
		
		String seriesNo =
		createSeriesNo(inventory.getOrderDate(), inventory.getBatchNo(), faceValuePer100k, inventory.getSeqNo());
		inventory.setSeries(Long.valueOf(seriesNo));
		
		String luhnCheckDigit = BooleanUtils.toBoolean(tuple.get(qGiftCard.luhnCheck)) ? "1" : "0";
		String barCode = seriesNo + randomDigits + luhnCheckDigit;
		inventory.setBarcode(barCode);
		
		if (insertCounter < batchSize) {
		inventoryRepo.save(inventory);
		} else {
		inventoryRepo.saveAndFlush(inventory);
		// Prevent OOM
		entityManager.clear();
		insertCounter = 0;
		}
		if (batchNoSeqNoTuple.currSeqNo == 9999999) {
		batchNoSeqNoTuple.currSeqNo = 1;
		batchNoSeqNoTuple.currBatchNo++;
		} else {
		batchNoSeqNoTuple.currSeqNo++;
		}
		}
		}
		*/}

	@Override
	@Transactional
	public long receiveNewGiftCards(GiftCardInventorySaveDto dto) {
		GiftCardSeriesDto seriesFrom = dto.getSeriesFrom();
		GiftCardSeriesDto seriesTo = dto.getSeriesTo();
		Assert.isTrue(seriesFrom.getFaceValue() == seriesTo.getFaceValue());
		Assert.isTrue(seriesFrom.getOrderDate().equals(seriesTo.getOrderDate()));

		QGiftCardInventory qInventory = QGiftCardInventory.giftCardInventory;
		HibernateUpdateClause clause = new HibernateUpdateClause(statelessSession, qInventory)
				.set(qInventory.location, propertiesService.getDetailInvLocationHeadOffice());
		if (dto.getExpiryDate() != null) {
			clause.set(qInventory.expiryDate, dto.getExpiryDate());
		}
		return clause.where(qInventory.series.between(seriesFrom.getSeriesNo(), seriesTo.getSeriesNo())).execute();
	}

	@Override
	@Transactional
	public long allocateGiftCards(GiftCardOrderAllocateDto dto) {
		GiftCardSeriesDto seriesFrom = dto.getSeriesFromDto();
		GiftCardSeriesDto seriesTo = dto.getSeriesToDto();
		Assert.isTrue(seriesFrom.getFaceValue() == seriesTo.getFaceValue());
		Assert.isTrue(seriesFrom.getOrderDate().equals(seriesTo.getOrderDate()));
		Assert.notNull(dto.getAllocateToCode());

		QGiftCardInventory qInventory = QGiftCardInventory.giftCardInventory;
		HibernateUpdateClause clause = new HibernateUpdateClause(statelessSession, qInventory)
				.set(qInventory.allocateTo, dto.getAllocateToCode());
		if (dto.getExpiryDate() != null) {
			clause.set(qInventory.expiryDate, dto.getExpiryDate());
		}

		return clause.where(qInventory.series.between(seriesFrom.getSeriesNo(), seriesTo.getSeriesNo())).execute();
	}

	private String createSeriesNo(LocalDate orderDateTime, Integer batchNo, Integer faceValueCode, Integer seqNo) {
		String yyMMdd = orderDateTime.toString(GiftCardInventory.SERIES_DATETIME_FORMAT);

		String seriesDigits = seqNo.toString();
		for (int i = seriesDigits.length(); i < 7; i++) {
			seriesDigits = "0" + seriesDigits;
		}

		String faceValue = faceValueCode.toString();
		for (int i = faceValue.length(); i < 2; i++) {
			faceValue = "0" + faceValue;
		}
		return yyMMdd + batchNo + faceValue + seriesDigits;
	}

	@Override
	public GiftCardInventory getGiftCardByBarcode(String barcode) {
		return inventoryRepo.findByBarcode(barcode);
	}

	@Override
	public GiftCardInventoryDto getGiftCardDtoByBarcode(String barcode) {
		return new GiftCardInventoryDto(inventoryRepo.findByBarcode(barcode));
	}

	private class BatchNoSeqNoTuple {
		int currBatchNo;
		int currSeqNo;
	}

	@Override
	public List<String> getGiftCardSeries(
			GiftCardInventorySeriesSearchDto searchDto) {
		Predicate predicate = null;
		QGiftCardInventory qg = QGiftCardInventory.giftCardInventory;
		if (searchDto.getSeriesFrom() != null && searchDto.getSeriesTo() != null) {
			predicate = qg.series.between(searchDto.getSeriesFrom(), searchDto.getSeriesTo());
		} else if (searchDto.getSeriesFrom() != null) {
			predicate = qg.series.goe(searchDto.getSeriesFrom());
		} else if (searchDto.getSeriesTo() != null) {
			predicate = qg.series.loe(searchDto.getSeriesTo());
		}

		Iterable<GiftCardInventory> result = inventoryRepo.findAll(predicate);

		List<String> list = new ArrayList<String>();

		if (searchDto.getCards() != null) {
			list.addAll(searchDto.getCards());
		}

		if (result == null)
			return null;

		for (GiftCardInventory dto : result) {
			if (!list.contains(String.valueOf(dto.getSeries())))
				list.add(String.valueOf(dto.getSeries()));
		}

		return list;
	}

	@Override
	public SalesOrderAlloc getGiftCardSo(String noSo) {

        QSalesOrder qSalesOrder = QSalesOrder.salesOrder;
        QSalesOrderItem qSalesOrderItem = QSalesOrderItem.salesOrderItem;
        QSalesOrderAlloc qSalesOrderAlloc = QSalesOrderAlloc.salesOrderAlloc;

		return new JPAQuery(entityManager).from(qSalesOrderAlloc)
                .innerJoin(qSalesOrderAlloc.soItem, qSalesOrderItem)
                .innerJoin(qSalesOrderItem.order, qSalesOrder)
                .where(qSalesOrder.orderNo.eq(noSo))
                .uniqueResult(qSalesOrderAlloc);
	}

	@Override
	public List<String> removeGiftCardSeriesFromList(
			GiftCardInventorySeriesSearchDto searchDto) {

		for (Long series = searchDto.getSeriesFrom(); series <= searchDto.getSeriesTo(); series++) {
			if (searchDto.getCards() != null || !searchDto.getCards().isEmpty()) {
				if (searchDto.getCards().contains(String.valueOf(series))) {
					searchDto.getCards().remove(String.valueOf(series));
				}
			}
		}
		return searchDto.getCards();
	}

	@Override
	public boolean isSeriesNotExistsInStockStatus(String seriesNo) {
		boolean exists = true;
		QGiftCardInventory qg = QGiftCardInventory.giftCardInventory;
		GiftCardInventory gc = inventoryRepo.findOne(qg.series.eq(Long.valueOf(seriesNo))
				.and(qg.status.eq(GiftCardInventoryStatus.IN_STOCK)));
		if (gc == null)
			exists = false;
		return exists;
	}

	@Override
	public boolean isSeriesNotExists(String seriesNo) {
		boolean exists = true;
		QGiftCardInventory qg = QGiftCardInventory.giftCardInventory;
		GiftCardInventory gc = inventoryRepo.findOne(qg.series.eq(Long.valueOf(seriesNo)));
		if (gc == null)
			exists = false;
		return exists;
	}

	@Override
	public boolean isCardInTheList(String seriesNo,
			GiftCardInventorySeriesSearchDto searchDto) {
		return searchDto.getCards().contains(seriesNo);
	}

	@Override
	public GiftCardInventoryDto getGiftCardSeriesProductForTransferIn(GiftCardInventorySeriesSearchDto searchDto, StockRequestDto stockRequest) throws MessageSourceResolvableException {
		Predicate predicate = null;
		QGiftCardInventory qg = QGiftCardInventory.giftCardInventory;
		if (searchDto.getSeriesFrom() != null && searchDto.getSeriesTo() != null) {
			predicate = qg.series.between(searchDto.getSeriesFrom(), searchDto.getSeriesTo())
					.and(qg.status.eq(GiftCardInventoryStatus.IN_TRANSIT));
		}

		Iterable<GiftCardInventory> result = inventoryRepo.findAll(predicate);

		String allocateToDescription = storeService.getStoreByCode(stockRequest.getAllocateTo()).getName();
		for (GiftCardInventory giftCardInventory : result) {
			if (giftCardInventory.getLocation().equalsIgnoreCase(stockRequest.getAllocateTo())) {
				String[] args = { allocateToDescription };
				throw new MessageSourceResolvableException("gc.stock.error.product.already.in.store", args,
						"Invalid product/s, gift card/s already part of " + allocateToDescription + " inventory.");
			}
		}

		if (!result.iterator().hasNext()) {
			String[] args = { String.valueOf(searchDto.getSeriesFrom()), String.valueOf(searchDto.getSeriesTo()) };
			throw new MessageSourceResolvableException("gc.stock.error.series.not.exist", args,
					"Gift card with series " + String.valueOf(searchDto.getSeriesFrom())
							+ " to " + String.valueOf(searchDto.getSeriesTo()) + " does not exist in our inventory.");
		}

		long quantity = searchDto.getSeriesTo() - searchDto.getSeriesFrom();
		if (inventoryRepo.count(predicate) == quantity) {
			String[] args = { String.valueOf(searchDto.getSeriesFrom()), String.valueOf(searchDto.getSeriesTo()) };
			throw new MessageSourceResolvableException("gc.stock.error.inventory.discrepancy", args,
					"Gift card/s not present in our inventory between " + searchDto.getSeriesFrom() + " and " + searchDto.getSeriesTo() + ".");
		}

		ProductProfileDto productProfileDto = new ProductProfileDto(result.iterator().next().getProfile());
		GiftCardInventoryDto giftCardInventoryDto = new GiftCardInventoryDto(result.iterator().next());
		if (!stockRequest.getProductCode().equalsIgnoreCase(productProfileDto.getProductCode())) {
			String[] args = { productProfileDto.getProductDesc() };
			throw new MessageSourceResolvableException("gc.stock.error.expected.product", args,
					"Product " + productProfileDto.getProductDesc() + " is not part of this stock request.");
		}

		return giftCardInventoryDto;
	}

	@Override
	@Transactional
	public void updateGiftCardLocations(StockRequestReceiveDto receiveDto, StockRequest stockRequest) {
		QGiftCardInventory qg = QGiftCardInventory.giftCardInventory;
		Predicate predicate = qg.series.between(receiveDto.getStartingSeries(), receiveDto.getEndingSeries())
				.and(qg.status.eq(GiftCardInventoryStatus.IN_TRANSIT));
		Iterable<GiftCardInventory> gcs = inventoryRepo.findAll(predicate);

		for (GiftCardInventory giftCardInventory : gcs) {
			giftCardInventory.setLocation(stockRequest.getAllocateTo());
			giftCardInventory.setAllocateTo(null);
			giftCardInventory.setStatus(GiftCardInventoryStatus.IN_STOCK);
		}
		inventoryRepo.save(gcs);
		inventoryStockService.saveStocksFromStockRequest(stockRequest, receiveDto, GCIStockStatus.TRANSFERRED_IN);
	}

	@Override
	public GiftCardInventory getGiftCardBySeries(Long series) {
		return inventoryRepo.findOne(QGiftCardInventory.giftCardInventory.series.eq(series));
	}

	@Override
	@Transactional
	public List<String> reserveGiftCards(StockRequestReceiveDto receiveDto, StockRequestDto stockRequest) {
		QGiftCardInventory qg = QGiftCardInventory.giftCardInventory;
		Predicate predicate = qg.series.between(receiveDto.getStartingSeries(), receiveDto.getEndingSeries());
		Iterable<GiftCardInventory> gcs = inventoryRepo.findAll(predicate);

		List<String> errors = Lists.newArrayList();
		Locale locale = LocaleContextHolder.getLocale();

		for (GiftCardInventory giftCardInventory : gcs) {
			if (!giftCardInventory.getLocation().equals(stockRequest.getSourceLocation())) {
				errors.add(messageSource.getMessage("gc.reserve.error.source.mismatch",
						new Object[] { giftCardInventory.getSeries() }, locale));
			}

			if (giftCardInventory.getAllocateTo() != null) {
				errors.add(messageSource.getMessage("gc.reserve.error.allocated",
						new Object[] { giftCardInventory.getSeries() }, locale));
			}

			giftCardInventory.setAllocateTo(stockRequest.getAllocateTo());
			if (BooleanUtils.isTrue(stockRequest.getIsForPromo())) {
				giftCardInventory.setIsForPromo(true);
			}
		}

		if (!errors.isEmpty()) {
			return errors;
		}

		inventoryRepo.save(gcs);
		return null;
	}

	@Override
	public boolean isSeriesValidForExtension(String seriesNo) {
		boolean isValid = true;
		QGiftCardInventory qg = QGiftCardInventory.giftCardInventory;
		GiftCardInventory gc = inventoryRepo.findOne(qg.series.eq(Long.valueOf(seriesNo)));
		if (gc.getExpiryDate() == null) {
			isValid = false;
		}
		return isValid;
	}

	@Override
	public void updateStatus(String series, GiftCardInventoryStatus status) {
		QGiftCardInventory qg = QGiftCardInventory.giftCardInventory;
		GiftCardInventory gc = inventoryRepo.findOne(qg.series.eq(Long.valueOf(series)));
		gc.setStatus(status);
		inventoryRepo.save(gc);
	}

	@Override
	@Transactional
	public void expireGiftCards() {
		LocalDate date = LocalDate.now().minusDays(1);
		QGiftCardInventory gcInv = QGiftCardInventory.giftCardInventory;
		HibernateUpdateClause clause = new HibernateUpdateClause(statelessSession, gcInv);
		clause.where(gcInv.expiryDate.eq(date)).set(gcInv.status, GiftCardInventoryStatus.DISABLED);
		clause.execute();

		JPAQuery query = new JPAQuery(entityManager);
		BigDecimal amount = query.from(gcInv).where(gcInv.expiryDate.eq(date))
				.singleResult(gcInv.faceValue.sum());
		if (amount != null) {
			amount = amount.negate();
			CrmForPeoplesoftConfig config = peoplesoftConfigRepo.findByTransactionType(TransactionType.EXPIRE_GC);
			CrmForPeoplesoft expire = new CrmForPeoplesoft();
			expire.setTransactionType(config.getTransactionType());
			expire.setType(config.getType());
			expire.setMmsAcct(config.getMmsAcct());
			expire.setPsAcct(config.getPsAcct());
			expire.setTransactionAmt(amount);
			expire.setDeptId(config.getDeptId());
			expire.setBusinessUnit(config.getBusinessUnit());
			peoplesoftRepo.save(expire);
		}
	}

	@Override
	public ResultList<MemberDto> removeNonPrepaidUserMembers(
			ResultList<MemberDto> members) {
		ResultList<MemberDto> updatedMembers = new ResultList<MemberDto>();
		QGiftCardInventory qInventory = QGiftCardInventory.giftCardInventory;
		List<Long> prepaidCardUsers = new JPAQuery(entityManager)
				.from(qInventory)
				.where(qInventory.owningMember.isNotNull())
				.list(qInventory.owningMember.id);

		List<MemberDto> memberList = Lists.newArrayList(members.getResults());
		if (!prepaidCardUsers.isEmpty() && members.getResults() != null) {
			for (MemberDto member : memberList) {
				if (prepaidCardUsers.contains(member.getId())) {
					updatedMembers.getResults().add(member);
				}
			}

		}
		return updatedMembers;
	}

	@Override
	public Long getQualifiedMembersCountPrepaidUsers(
			List<MemberGroupFieldDto> memberGroupFieldsDto, MemberGroup group,
			PageSortDto pageSortDto) {
		PagingParam pagination = new PagingParam();
		pagination.setPageSize(Integer.MAX_VALUE);
		pageSortDto.setPagination(pagination);
		List<MemberDto> members = memberGroupService.getQualifiedMembersForPrint(memberGroupFieldsDto, group, pageSortDto);

		List<MemberDto> updatedMembers = new ArrayList<MemberDto>();
		QGiftCardInventory qInventory = QGiftCardInventory.giftCardInventory;
		List<Long> prepaidCardUsers = new JPAQuery(entityManager)
				.from(qInventory)
				.where(qInventory.owningMember.isNotNull())
				.list(qInventory.owningMember.id);
		List<MemberDto> memberList = Lists.newArrayList(members);
		if (!prepaidCardUsers.isEmpty() && members != null) {
			for (MemberDto member : memberList) {
				if (prepaidCardUsers.contains(member.getId())) {
					updatedMembers.add(member);
				}
			}

		}

		return Long.valueOf(updatedMembers.size());
	}

	@Override
	public boolean isGiftCardForPromo(String barcode) {
		return inventoryRepo.isGiftCardForPromo(barcode);
	}

}
