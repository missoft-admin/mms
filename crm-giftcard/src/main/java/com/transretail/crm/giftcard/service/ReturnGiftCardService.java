package com.transretail.crm.giftcard.service;

import org.springframework.validation.BindingResult;

import com.transretail.crm.common.service.dto.response.ResultList;
import com.transretail.crm.giftcard.dto.ReturnRecordDto;
import com.transretail.crm.giftcard.dto.ReturnSearchDto;
import com.transretail.crm.giftcard.dto.SalesOrderDto;
import com.transretail.crm.giftcard.entity.support.ReturnStatus;

public interface ReturnGiftCardService {

    void saveReturn(ReturnRecordDto dto);

    ResultList<ReturnRecordDto> getReturns(ReturnSearchDto searchForm);

    ReturnRecordDto getReturn(Long id);

    void deleteReturn(Long id);

    void approveReturn(Long id, ReturnStatus status);

    ReturnRecordDto getReturnByRecordNo(String returnNo);

    void updateReturn(String returnNo, ReturnStatus status);

    boolean isValidSalesOrder(String orderNo);

    void saveRefund(ReturnRecordDto refund);

    SalesOrderDto getSalesOrderByReturnNo(String returnNo);


}
