package com.transretail.crm.giftcard.entity.support;

public enum ReturnType {
    FOR_REFUND, FOR_REPLACE
}
