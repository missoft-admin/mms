package com.transretail.crm.giftcard.service.impl;

import java.util.Collection;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.tuple.Pair;
import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.google.common.collect.Lists;
import com.mysema.query.jpa.impl.JPAQuery;
import com.mysema.query.jpa.impl.JPAUpdateClause;
import com.mysema.query.types.expr.BooleanExpression;
import com.transretail.crm.common.service.dto.request.PagingParam;
import com.transretail.crm.common.service.dto.request.PagingParam.DataType;
import com.transretail.crm.common.service.dto.response.ResultList;
import com.transretail.crm.common.service.exception.MessageSourceResolvableException;
import com.transretail.crm.common.service.util.SpringDataPagingUtil;
import com.transretail.crm.core.entity.lookup.Store;
import com.transretail.crm.core.security.util.UserUtil;
import com.transretail.crm.core.service.CodePropertiesService;
import com.transretail.crm.core.service.LookupService;
import com.transretail.crm.core.service.StoreService;
import com.transretail.crm.giftcard.dto.GiftCardInventorySeriesSearchDto;
import com.transretail.crm.giftcard.dto.ProductProfileDto;
import com.transretail.crm.giftcard.dto.ProductProfileSearchDto;
import com.transretail.crm.giftcard.dto.StockRequestDto;
import com.transretail.crm.giftcard.dto.StockRequestReceiveDto;
import com.transretail.crm.giftcard.dto.StockRequestResultList;
import com.transretail.crm.giftcard.dto.StockRequestSearchDto;
import com.transretail.crm.giftcard.entity.GiftCardInventory;
import com.transretail.crm.giftcard.entity.GiftCardInventoryStock.GCIStockStatus;
import com.transretail.crm.giftcard.entity.QStockRequest;
import com.transretail.crm.giftcard.entity.StockRequest;
import com.transretail.crm.giftcard.entity.support.GiftCardInventoryStatus;
import com.transretail.crm.giftcard.entity.support.StockRequestStatus;
import com.transretail.crm.giftcard.repo.GiftCardInventoryRepo;
import com.transretail.crm.giftcard.repo.StockRequestRepo;
import com.transretail.crm.giftcard.service.GiftCardInventoryService;
import com.transretail.crm.giftcard.service.GiftCardInventoryStockService;
import com.transretail.crm.giftcard.service.ProductProfileService;
import com.transretail.crm.giftcard.service.StockRequestService;

/**
 *
 */
@Service("stockRequestService")
public class StockRequestServiceImpl implements StockRequestService {
    @Autowired
    private StockRequestRepo repo;
    @Autowired
    private GiftCardInventoryRepo giftCardInventoryRepo;
    @Autowired
    private StoreService storeService;
    @Autowired
    private ProductProfileService productProfileService;
    @Autowired
    private CodePropertiesService codePropertiesService;
    @Autowired
    private GiftCardInventoryService giftCardInventoryService;
    @Autowired
    private GiftCardInventoryStockService giftCardInventoryStockService;
    @Autowired
    private LookupService lookupService;
    @Autowired
    private MessageSource messageSource;

    @PersistenceContext
    private EntityManager em;
    
    // fields/properties of class StockRequestModel
    private static final String[] DATE_FIELDS = new String[]{"created", "requestDate"};

    @Override
    @Transactional
    public Long saveStockRequest(StockRequestDto dto) {
        return repo.save(dto.toModel()).getId();
    }

    @Override
    @Transactional
    public Long updateStockRequest(Long id, StockRequestDto dto) {
        StockRequest request = repo.findOne(id);
        dto.update(request);
        request.setLastUpdated(DateTime.now());
        return repo.save(request).getId();
    }

    @Override
    @Transactional(readOnly = true)
    public StockRequestDto getStockRequest(Long id) {
        StockRequestDto dto = new StockRequestDto(repo.findOne(id));
        if (dto.getSourceLocation() != null) {
        	if(dto.getSourceLocation().equals(codePropertiesService.getDetailInvLocationHeadOffice())) {
        		dto.setSourceLocationDescription(lookupService.getDetailByCode(dto.getSourceLocation()).getDescription());
        	}
        	else {
        		dto.setSourceLocationDescription(storeService.getStoreByCode(dto.getSourceLocation()).getName());
        	}
        }
        if (dto.getAllocateTo() != null) {
            dto.setAllocateToDescription(storeService.getStoreByCode(dto.getAllocateTo()).getName());
        }
        if(dto.getProductCode() != null) {
        	ProductProfileSearchDto searchDto = new ProductProfileSearchDto();
        	searchDto.setProdCode(dto.getProductCode());
        	for(ProductProfileDto ppDto : productProfileService.list(searchDto).getResults()) {
        		dto.setProductDescription(ppDto.getProductDesc());
        	}
        }
        return dto;
    }

    @Override
    @Transactional(readOnly = true)
    public StockRequestResultList getStockRequests(StockRequestSearchDto searchForm) {
        if (UserUtil.getCurrentUser().getInventoryLocation() == null) {
            searchForm.setAllocateTo(UserUtil.getCurrentUser().getStore().getCode());
            searchForm.setSourceLocation(UserUtil.getCurrentUser().getStore().getCode());
        } else if (!UserUtil.getCurrentUser().getInventoryLocation().equals(codePropertiesService.getDetailInvLocationHeadOffice())) {
            searchForm.setAllocateTo(UserUtil.getCurrentUser().getStore().getCode());
            searchForm.setSourceLocation(UserUtil.getCurrentUser().getStore().getCode());
        }
        
        final PagingParam pagination = searchForm.getPagination();
        // set manually the date field to used PagingParam.DataType
        // doing this, it will not treat as String so the querydsl executor 
        // will not use the lower(created|requestDate) in order by clause
        // that cause the db to sort the date fields as String.
        // related ticket: https://projects.exist.com/issues/show/94458
        // TODO: proper setup in pointList.jps ajaxDatatable config 
        //      - need to pass from page the data type of per field that required for sorting
        //        otherwise it will sort as string.
        if (pagination.getOrder() != null && pagination.getOrder().keySet() != null) {
            for (String key : pagination.getOrder().keySet()) {
                if (ArrayUtils.contains(DATE_FIELDS, key)) {
                pagination.getDataTypes().put(key, DataType.DATE);
                }
            }
        }
        
        Pageable pageable = SpringDataPagingUtil.INSTANCE.createPageable(searchForm.getPagination());
        BooleanExpression filter = searchForm.createSearchExpression();
        Page<StockRequest> page = filter != null ? repo.findAll(filter, pageable) : repo.findAll(pageable);
        return new StockRequestResultList(toDto(page.getContent()), page.getTotalElements(), page.hasPreviousPage(),
            page.hasNextPage());
    }
    
    @Override
    @Transactional(readOnly = true)
    public List<StockRequestDto> getStockRequestsForPrint(StockRequestSearchDto searchForm) {
        if (!UserUtil.getCurrentUser().getInventoryLocation().equals(codePropertiesService.getDetailInvLocationHeadOffice())) {
            searchForm.setAllocateTo(UserUtil.getCurrentUser().getStore().getCode());
            searchForm.setSourceLocation(UserUtil.getCurrentUser().getStore().getCode());
        }
        
        BooleanExpression filter = searchForm.createSearchExpression();
        return toDto(Lists.newArrayList(repo.findAll(filter)));
    }

    @Override
    @Transactional
    public void submitStockRequest(Long id, Integer quantity) {
    	// TODO 
    }

    @Override
    public void validateStockRequest(Long id) {
    	// TODO 
    }

    @Override
    public void rejectStockRequest(Long id, String reason) {
    	// TODO 
    }

    @Override
    public void approveStockRequest(Long id) {
    	// TODO 
    }

    @Override
    public void disapproveStockRequest(Long id, String reason) {
    	// TODO 
    }

    @Override
    public List<StockRequestDto> findStockRequestByRequestNo(String requestNo) {
    	return toDto(repo.findByRequestNo(requestNo));
    }
    
    @Override
    public String createStockRequestNumber() {
    	return repo.createStockRequestNumber();
    }

    @Override
    @Transactional
    public void transferStockRequest(Long id) {
        if (id == null)
            return;
        
        StockRequest stockRequest = repo.findOne(id);
        if (stockRequest != null) {
            stockRequest.setStatus(StockRequestStatus.IN_TRANSIT);
            stockRequest.setQuantityTransferred(0);
            repo.save(stockRequest);
        }
        StockRequestDto dto = new StockRequestDto(stockRequest);
        for (Pair<String, String> pairs : dto.getReservedSeriesList()) {
            GiftCardInventorySeriesSearchDto searchDto = new GiftCardInventorySeriesSearchDto();
            searchDto.setSeriesFrom(Long.valueOf(pairs.getLeft()));
            searchDto.setSeriesTo(Long.valueOf(pairs.getRight()));
            
            BooleanExpression filter = searchDto.createSearchExpression();
            Iterable<GiftCardInventory> list = giftCardInventoryRepo.findAll(filter);
            for (GiftCardInventory gcInventoryDto : list) {
                gcInventoryDto.setStatus(GiftCardInventoryStatus.IN_TRANSIT);
            }
            giftCardInventoryRepo.save(list);
        }
        saveHistory(stockRequest);
    }

    private void saveHistory(StockRequest stockRequest) {
        giftCardInventoryStockService.saveStocksFromStockRequest(stockRequest, null,GCIStockStatus.TRANSFERRED_OUT);
        
    }
    
    private List<StockRequestDto> toDto(Collection<StockRequest> items) {
        List<StockRequestDto> results = Lists.newArrayList();
        for (StockRequest item : items) {
        	StockRequestDto dto = new StockRequestDto(item);
        	if(StringUtils.isNotBlank(dto.getSourceLocation())) {
        		if(dto.getSourceLocation().equals(codePropertiesService.getDetailInvLocationHeadOffice())) {
        			dto.setSourceLocationDescription("HEAD OFFICE");
        		}
        		else {
        			Store store = storeService.getStoreByCode(dto.getSourceLocation());
            		dto.setSourceLocationDescription(store.getName());
        		}
        	}
        	
        	if(StringUtils.isNotBlank(dto.getAllocateTo())) {
        		Store store = storeService.getStoreByCode(dto.getAllocateTo());
        		dto.setAllocateToDescription(store.getName());
        	}
        	
        	if(StringUtils.isNotBlank(dto.getProductCode())) {
        		ProductProfileSearchDto searchDto = new ProductProfileSearchDto();
        		searchDto.setProdCode(dto.getProductCode());
        		ResultList<ProductProfileDto> productProfiles = productProfileService.list(searchDto);
        		if(productProfiles.getTotalElements() > 0) {
        			for(ProductProfileDto i : productProfiles.getResults()) {
        				dto.setProductDescription(i.getProductDesc());
        			}
        		}
        	}
            results.add(dto);
        }
        return results;
    }

    @Override
    public boolean hasUnapprovedRequests(String requestNo) {
    	QStockRequest stockRequest = QStockRequest.stockRequest;
    	return repo.count(stockRequest.requestNo.eq(requestNo)
			.and(stockRequest.status.eq(StockRequestStatus.FORAPPROVAL))) > 0;
    }

    @Override
    @Transactional
    public void transferInStocks(StockRequestDto stockrequest) throws MessageSourceResolvableException {
        StockRequest stockModel = repo.findOne(stockrequest.getId());
        if (!stockrequest.getReceiveDtos().isEmpty() && stockModel != null) {
            for (StockRequestReceiveDto receiveDto : stockrequest.getReceiveDtos()) {
                if (receiveDto.getProductCode().equals(stockModel.getProductCode())) {
                    if (stockModel.getQuantityTransferred() == null) {
                        stockModel.setQuantityTransferred(0);
                    }
                    if (receiveDto.getQuantity() > stockModel.getQuantity() || receiveDto.getQuantity() > stockModel.getQuantity()-stockModel.getQuantityTransferred()) {
                        ProductProfileDto product = productProfileService.findProductProfileByCode(stockModel.getProductCode());
                        String[] args = {stockrequest.getRequestNo(), stockrequest.getTransferRefNo(), 
                                            String.valueOf(stockModel.getQuantity()), product.getProductDesc()};
                        throw new MessageSourceResolvableException("gc.stock.error.expected.quantity", args,
                                "Stock request no " + stockrequest.getRequestNo() 
                                    + " with transfer reference no " + stockrequest.getTransferRefNo()
                                    + " is only expecting " + String.valueOf(stockModel.getQuantity()) 
                                    + " pieces of " + product.getProductDesc() + ".");
                    } else {
                        stockModel.setQuantityTransferred(stockModel.getQuantityTransferred() + receiveDto.getQuantity());
                        if (stockModel.getQuantityTransferred().compareTo(stockModel.getQuantity()) == 0) {
                            stockModel.setStatus(StockRequestStatus.TRANSFERRED_IN);
                            stockModel.setReceiveBy(UserUtil.getCurrentUser().getUsername());
                        }
                        StockRequest stockUpdated = repo.save(stockModel);
                        giftCardInventoryService.updateGiftCardLocations(receiveDto, stockUpdated);
                    }
                }
            }
        } else {
            throw new MessageSourceResolvableException("gc.stock.error.no.products", null, "No products encoded.");
        }
        
    }

    @Override
    @Transactional
    public List<String> reserveGiftCardsForTranserOut(List<StockRequestReceiveDto> gcList, StockRequestDto stockRequestDto) {
    	int total = stockRequestDto.getQuantityReserved() != null ? stockRequestDto.getQuantityReserved() : 0;
    	
    	for(StockRequestReceiveDto reserveGc : gcList) {
    		total += reserveGc.getQuantity();
    	}
    	
    	if(total > stockRequestDto.getQuantity()) {
    		return Lists.newArrayList(messageSource.getMessage("gc.reserve.error.quantity", null, LocaleContextHolder.getLocale()));
    	}
    	
    	if(total == stockRequestDto.getQuantity()) {
    		stockRequestDto.setStatus(StockRequestStatus.FOR_TRANSFER_OUT);
    	}
    	else {
    		stockRequestDto.setStatus(StockRequestStatus.INCOMPLETE);
    		stockRequestDto.setQuantityReserved(total);
    	}
    	
    	for(StockRequestReceiveDto reserveGc : gcList) {
			List<String> result = giftCardInventoryService.reserveGiftCards(reserveGc, stockRequestDto);
			if(result != null) {
				return result;
			}
			stockRequestDto.addSeriesToList(reserveGc.getStartingSeries(), reserveGc.getEndingSeries());
		}
    	
    	updateStockRequest(stockRequestDto.getId(), stockRequestDto);
    	return null;
    }

    @Transactional
    @Override
    public void updateStatus( List<Long> ids, StockRequestStatus stat ) {
        QStockRequest qModel = QStockRequest.stockRequest;
        JPAUpdateClause update = new JPAUpdateClause( em, qModel );
        update.set( qModel.status, stat );
        update.where( qModel.id.in( ids ) );
        update.execute();
        em.clear();
        em.flush();
    }

    @Override
    public List<String> getApprovedStockNos( List<Long> ids ) {
        QStockRequest qStockReq = QStockRequest.stockRequest;
        return new JPAQuery( em ).from( qStockReq )
        		.where( qStockReq.id.in( ids ).and( qStockReq.status.eq( StockRequestStatus.APPROVED ) ) ).list( qStockReq.requestNo );
    }

}
