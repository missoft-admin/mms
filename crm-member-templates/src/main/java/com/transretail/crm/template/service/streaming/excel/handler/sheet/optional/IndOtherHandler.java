package com.transretail.crm.template.service.streaming.excel.handler.sheet.optional;

import static org.apache.commons.lang3.StringUtils.isBlank;

import java.util.Map;

import org.springframework.context.i18n.LocaleContextHolder;

import com.google.common.collect.Maps;
import com.mysema.query.jpa.impl.JPAUpdateClause;
import com.transretail.crm.core.entity.MemberModel;
import com.transretail.crm.core.entity.QMemberModel;
import com.transretail.crm.template.service.streaming.excel.handler.HandlerException;
import com.transretail.crm.template.service.streaming.excel.handler.cell.AccountIdCellHandler;
import com.transretail.crm.template.service.streaming.excel.handler.cell.CellHandler;
import com.transretail.crm.template.service.streaming.excel.handler.cell.ClosestStoreCellHandler;
import com.transretail.crm.template.service.streaming.excel.handler.sheet.AbstractSheetHandler;

/**
 * @author Allan G. Ramirez (agramirez@exist.com)
 */
public class IndOtherHandler extends AbstractSheetHandler {
    private static final String HEADER_ACCOUNTID = "ACCOUNTNUMBER";
    private static final String HEADER_CLOSEST_STORE = "STORECLOSESTTOHOME";

    private Map<String, CellHandler> cellHandlerClassMap = Maps.newHashMap();
    private CellHandler[] cellHandlers = null;

    public IndOtherHandler() {
        cellHandlerClassMap.put(HEADER_ACCOUNTID, new AccountIdCellHandler());
        cellHandlerClassMap.put(HEADER_CLOSEST_STORE, new ClosestStoreCellHandler());
        cellHandlers = new CellHandler[cellHandlerClassMap.size()];
    }

    @Override
    protected void processHeaderCell(int i, String headerName) {
        CellHandler cellHandler = cellHandlerClassMap.get(headerName);
        if (cellHandler == null) {
            throw new HandlerException(getMessageSource()
                .getMessage("member.template.messages.excel.nohandler", new String[]{sheetName, headerName},
                    LocaleContextHolder.getLocale()));
        }
        cellHandlers[i] = cellHandler;
    }

    @Override
    protected CellHandler getCellHandler(int index) {
        return cellHandlers[index];
    }

    @Override
    protected void saveOrUpdateDto(MemberModel dto) throws HandlerException {
        String accountId = dto.getAccountId();
        if (isBlank(accountId)) {
            throw new HandlerException(getMessageSource().getMessage("member.template.messages.excel.columns.required",
                new String[]{sheetName, String.valueOf(currentRowNum), HEADER_ACCOUNTID},
                LocaleContextHolder.getLocale()));
        } else if (accountId.length() != 13 || accountId.charAt(2) != '0' || accountId.charAt(3) != '1') {
            throw new HandlerException(getMessageSource().getMessage("member.template.messages.excel.accountid.invalid",
                new String[]{sheetName, accountId, "01"},
                LocaleContextHolder.getLocale()));
        }
        if (!accountIdExists(dto.getAccountId())) {
            throw new HandlerException(getMessageSource().getMessage("member.template.messages.excel.accountid.notfound",
                new String[]{sheetName, String.valueOf(currentRowNum), dto.getAccountId()},
                LocaleContextHolder.getLocale()));
        }

        QMemberModel qMemberModel = QMemberModel.memberModel;
        new JPAUpdateClause(em, qMemberModel).where(qMemberModel.accountId.eq(dto.getAccountId()))
            .set(qMemberModel.customerProfile.closestStore, dto.getCustomerProfile().getClosestStore())
            .execute();

        if (currentRowNum % batchSize == 0) {
            cleanup();
        }
    }

    @Override
    public int getExpectedNumberOfColumns() {
        return cellHandlers.length;
    }
}
