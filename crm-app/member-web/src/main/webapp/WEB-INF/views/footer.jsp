<%@ include file="taglibs.jsp" %>
<div id="footer">
  <%
    boolean isLoginPage = ((String) request.getAttribute("javax.servlet.forward.request_uri")).endsWith("login");
    pageContext.setAttribute("isLoginPage", isLoginPage);
  %>
  <c:choose>
    <c:when test="${pageContext['request'].userPrincipal != null}">
      <a href="<spring:url value="/"/>"><spring:message code="button_home"/></a>
      |
      <a href="<spring:url value="/logout" />"><spring:message code="security_logout"/></a>
    </c:when>
    <c:when test="${!isLoginPage}">
      <a href="<spring:url value="/login" />"><spring:message code="security_login"/></a>
    </c:when>
    <c:otherwise>

    </c:otherwise>
  </c:choose>

  <%--<span id="language">--%>
  <%--<c:out value=" | "/>--%>
  <%--<spring:message code="global_language"/>--%>
  <%--<c:out value=": "/>--%>
  <%--<util:language label="English" locale="en"/>--%>
  <%--<util:language label="Bahasa" locale="id"/>--%>
  <%--</span>--%>
</div>
