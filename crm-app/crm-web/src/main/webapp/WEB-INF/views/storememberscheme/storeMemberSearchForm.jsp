<%@ include file="../common/taglibs.jsp" %>

<style type="text/css">
<!--
  .well .btn-primary {
    margin-left: 5px;
  }
  .well select, .well .input {
    margin: 5px;
  }
  .label-single {
      width: 150px;
  }
  
-->
</style>

<div id="form_SearchForm" class="form-horizontal block-search well">
  <div id="control-group" class="form-inline">
    <div class="form-inline">
      <label for="" class="label-single"><h5><spring:message code="search_filter"/>:</h5></label>
      <select name="store" class="pointsSearchDropdown">
        <option value=""><spring:message code="search_store"/></option>
          <c:forEach var="item" items="${stores}">
            <option value="${item.code}">${item.codeAndName}</option>
          </c:forEach>
      </select>
      <select name="company" class="pointsSearchDropdown">
        <option value=""><spring:message code="search_company"/></option>
          <c:forEach var="item" items="${companies}">
            <c:set var="codeAndDescription" value="${item.code} - ${item.description }"/>
            <option value="${item.code}">${codeAndDescription}</option>
          </c:forEach>
      </select>
      <select name="cardType" class="pointsSearchDropdown">
        <option value=""><spring:message code="search_card_type"/></option>
          <c:forEach var="item" items="${cardTypes}">
            <c:set var="codeAndDescription" value="${item.code} - ${item.description }"/>
            <option value="${item.code}">${codeAndDescription}</option>
          </c:forEach>
      </select>
      <input type="button" id="searchButton" value="<spring:message code="search_filter"/>" class="btn btn-primary"/>
      <input id="clearButton" type="button" value="<spring:message code="label_clear" />" class="btn"/>
    </div>
  </div>
</div>