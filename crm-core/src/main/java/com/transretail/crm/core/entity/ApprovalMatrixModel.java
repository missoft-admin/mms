package com.transretail.crm.core.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.envers.Audited;

import com.transretail.crm.core.entity.enums.ModelType;
import com.transretail.crm.core.entity.support.CustomAuditableEntity;


@Audited
@Table(name = "CRM_APPROVAL_MATRIX")
@Entity
public class ApprovalMatrixModel extends CustomAuditableEntity<Long> implements Serializable {

	private static final long serialVersionUID = 1L;


	@Column(name = "MODEL_TYPE")
    @Enumerated(EnumType.STRING)
    private ModelType modelType;

    @ManyToOne
    @JoinColumn(name = "USERNAME")
    private UserModel user;

    @Column(name="AMOUNT")
    private Double amount;


    public ModelType getModelType() {
		return modelType;
	}

	public void setModelType(ModelType modelType) {
		this.modelType = modelType;
	}

	public UserModel getUser() {
		return user;
	}

	public void setUser(UserModel user) {
		this.user = user;
	}

	public Double getAmount() {
		return amount;
	}

	public void setAmount(Double amount) {
		this.amount = amount;
	}
}
