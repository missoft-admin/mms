(function($){
	var cometd = $.cometd;
	var channels = [];
	
	$.modelType = {
			'purchase'					:	"/purchasetxn",
			'promotion'					:	"/promotion",
			'points'					:	"/points",
			'reward'					:	"/reward",
			'manufacture_order'			:	"/mo",
			'loyalty_allocation'		:	"/loyalty",
			"member"					:	"/monitor",
			'loyalty_burn'				:	"/loyalty/burn",
			'loyalty_change_allocation'	:	"/loyalty/change"
		};
	
	$.notification = {
		init: function() {
			$.notification._retrieveNotifications();
			
			var rootUrl = encodeURI(location.protocol + "//" + location.host + ctx + "/cometd");
			cometd.configure({
				url		:	rootUrl,
//				logLevel:	'debug',
			});
			cometd.addListener('/meta/handshake', $.notification._metaHandshake);
//			cometd.addListener('/meta/connect', $.notification._metaConnect);
			
			$(window).unload(function() {
	        	$.cometd.reload();
	        });
			
			cometd.handshake();
		},
		
		_retrieveNotifications:	function() {
			$.ajax({
				type	: 	"POST",
				url		:	ctx + "/list",
				success	:	function(response) {
					if(!response.success) {
						return;
					}
					
					if(response.result.length > 4) {
//						$("#notifications").css("height", "300px");
					} 
					
					$.each(response.result, function(key, data) {
						$.notification._addToNotifications(data, false);
					});
					
					var notificationCount = response.result.length;
					
					if(notificationCount > 0) {
						$("#notificationCount").val(notificationCount);
						$("#notificationCountBadge").append(notificationCount);
						$("#noNotifications").hide();
					}
					else {
						$("#notificationCountBadge").empty();
						$("#noNotifications").show();
					}
					
//					console.debug($("#notifications").html());
//					console.debug($("#notifications").parent().html());
				},
				error	:	function(response) {
					console.debug("error on notifications");
				}
			});
		},
		
		_addToNotifications:	function(data, isNew) {
			var id = 'notification-' + data.type + "-" + data.id;
			var link = $.parseHTML("<div><li><a href='" + ctx + data.url + 
						"' id='" + id + "'><dl><dt>" + data.time + 
						"</dt><dd>" + data.message + "</dd></dl></a></li></div>");
			if(isNew) {
				$(link).first().addClass('alert alert-info');
			}
			$("#notifications").prepend($(link).html());
			if(data.type == "monitor_member" || data.type == "monitor_employee") {
				$("#" + id).click(function() {
					$.notification.send(data.type, data.id);
				});
			}
		},
		
		_notificationAlert:		function(url, message, time) {
			$("#notificationAlertsContent").append($("<li>" + message + "</li>"));
			$("#notificationAlerts").show("slow");
			var oldVal = parseInt($("#notificationCount").val());
			var newVal = (oldVal) ? oldVal + 1 : 1;
			$("#notificationCount").val(newVal);
			$("#notificationCountBadge").empty();
			$("#notificationCountBadge").append(newVal);
			$("#noNotifications").hide();
		},
		
		recieve: function(message) {
			var data = message.data;
			console.debug(data);
			$.notification._addToNotifications(data, true);
			$.notification._notificationAlert(data.url, data.message, data.time);
		},
		
		send: function(notificationType, itemId) {
			var data = "id=" + itemId + "&type=" + notificationType;
			$.ajax({
				type	: 'DELETE',
				url		: ctx + "/delete/" + notificationType + "/" + itemId,
				success	: function(response) {
					console.log("deleted monitor for member " + itemId);
				},
				error	: function(response) {}
			});
		},
		
		_metaHandshake: function(message) {
			if(message.successful) {
				$.ajax({
					type	: 	"POST",
					url		:	ctx + "/channels",
					success	:	function(response) {
						if(!response.success) {
							return;
						}
						
						$.each(response.result, function(key, data) {
							console.debug('/notification/notify' + data);
							cometd.subscribe('/notification/notify' + data, $.notification.recieve);
//							cometd.subscribe('/notification/update' + data, $.notification.recieve);
						});
						
					},
					error	:	function(response) {
						console.debug("error on notifications during channel configuration");
					}
				});
			}
		},
		
		_metaConnect:	function(message) {
			console.debug(message);
			console.debug("multiple: " + message.advice.multiple-clients);
			if(message.advice.multiple-clients) {
				console.debug("multiple clients here");
			}
		}
	};
})(jQuery);