package com.transretail.crm.encryptpin;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.transretail.crm.encryptpin.service.EncryptPinService;

/**
 * @author Allan G. Ramirez (agramirez@exist.com)
 */
public class Main {
    private static final Logger _LOG = LoggerFactory.getLogger(Main.class);

    public static void main(String[] args) throws Exception {
        ApplicationContext context = new ClassPathXmlApplicationContext("applicationContext.xml");
        EncryptPinService service = context.getBean(EncryptPinService.class);
        _LOG.info("[PIN ENCRYPTOR] Started encrypting all PINS.");
        try {
            service.encryptAllPins();
            _LOG.info("[PIN ENCRYPTOR] Successfully encrypted PINS.");
        } catch (Exception e) {
            _LOG.info("[PIN ENCRYPTOR] Failed to encrypt PINS.");
        }
    }
}
