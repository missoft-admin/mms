package com.transretail.crm.core.entity;


import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.Transient;

import org.hibernate.annotations.Type;
import org.joda.time.LocalDate;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.transretail.crm.common.web.converter.LocalDateSerializer;


@Embeddable
public class MemberGroupRFS {
	
	@Column(name = "ENABLED")
    @Type(type = "yes_no")
	private Boolean enabled;
	
	@Column(name = "STORE_GROUP")
	private Long storeGroup;
	
	@Transient
	private String storeGroupName;
	
	@Column(name = "PRODUCT_GROUP")
	private Long productGroup;
	
	@Transient
	private String productGroupName;

	@Column(name = "RECENCY")
	private String recency;

	@Column(name = "RECENT_VALUE")
	private String recentValue;

	@JsonSerialize(using = LocalDateSerializer.class)
	@Column(name = "DATE_RANGE_FROM")
    @Type(type = "org.jadira.usertype.dateandtime.joda.PersistentLocalDate")
	private LocalDate dateRangeFrom;
	
	@JsonSerialize(using = LocalDateSerializer.class)
	@Column(name = "DATE_RANGE_TO")
    @Type(type = "org.jadira.usertype.dateandtime.joda.PersistentLocalDate")
	private LocalDate dateRangeTo;
	
    @Column(name = "FREQUENCY")
    @Type(type = "yes_no")
	private Boolean frequency;
    
    @Transient
	private Boolean zeroFrequency;
	
    @Column(name = "FREQUENCY_FROM")
    private Long frequencyFrom;
	
    @Column(name = "FREQUENCY_TO")
	private Long frequencyTo;
	
	@Column(name = "SPEND")
    @Type(type = "yes_no")
	private Boolean spend;
	
	@Column(name = "SPEND_PER_TRANSACTION")
    @Type(type = "yes_no")
	private Boolean spendPerTransaction;
	
	@Column(name = "SPEND_FROM")
	private Long spendFrom;
	
	@Column(name = "SPEND_TO")
	private Long spendTo;

	public Long getStoreGroup() {
		return storeGroup;
	}

	public void setStoreGroup(Long storeGroup) {
		this.storeGroup = storeGroup;
	}

	public Long getProductGroup() {
		return productGroup;
	}

	public void setProductGroup(Long productGroup) {
		this.productGroup = productGroup;
	}

	public String getRecency() {
		return recency;
	}

	public void setRecency(String recency) {
		this.recency = recency;
	}

	public String getRecentValue() {
		return recentValue;
	}

	public void setRecentValue(String recentValue) {
		this.recentValue = recentValue;
	}

	public LocalDate getDateRangeFrom() {
		return dateRangeFrom;
	}

	public void setDateRangeFrom(LocalDate dateRangeFrom) {
		this.dateRangeFrom = dateRangeFrom;
	}

	public LocalDate getDateRangeTo() {
		return dateRangeTo;
	}

	public void setDateRangeTo(LocalDate dateRangeTo) {
		this.dateRangeTo = dateRangeTo;
	}

	public Boolean getFrequency() {
		return frequency;
	}

	public void setFrequency(Boolean frequency) {
		this.frequency = frequency;
	}

	public Long getFrequencyFrom() {
		return frequencyFrom;
	}

	public void setFrequencyFrom(Long frequencyFrom) {
		this.frequencyFrom = frequencyFrom;
	}

	public Long getFrequencyTo() {
		return frequencyTo;
	}

	public void setFrequencyTo(Long frequencyTo) {
		this.frequencyTo = frequencyTo;
	}

	public Boolean getSpend() {
		return spend;
	}

	public void setSpend(Boolean spend) {
		this.spend = spend;
	}

	public Boolean getSpendPerTransaction() {
		return spendPerTransaction;
	}

	public void setSpendPerTransaction(Boolean spendPerTransaction) {
		this.spendPerTransaction = spendPerTransaction;
	}

	public Long getSpendFrom() {
		return spendFrom;
	}

	public void setSpendFrom(Long spendFrom) {
		this.spendFrom = spendFrom;
	}

	public Long getSpendTo() {
		return spendTo;
	}

	public void setSpendTo(Long spendTo) {
		this.spendTo = spendTo;
	}

	public Boolean getEnabled() {
		return enabled;
	}

	public void setEnabled(Boolean enabled) {
		this.enabled = enabled;
	}

	public String getStoreGroupName() {
		return storeGroupName;
	}

	public void setStoreGroupName(String storeGroupName) {
		this.storeGroupName = storeGroupName;
	}

	public String getProductGroupName() {
		return productGroupName;
	}

	public void setProductGroupName(String productGroupName) {
		this.productGroupName = productGroupName;
	}

	public Boolean getZeroFrequency() {
		return zeroFrequency;
	}

	public void setZeroFrequency(Boolean zeroFrequency) {
		this.zeroFrequency = zeroFrequency;
	}
	
	


    
}



