<%@ include file="../common/taglibs.jsp" %>


<style type="text/css">
<!--
    .control-group-02 {
        width: 277px !important;
    }
    #block_promotionDays.well {
        margin-left: 20px;
    }
    #content_target select {
        width: 202px;
    }
    .span5 {
        margin-left: 0px;
    }
    .block-stack-01 {
        margin-bottom: 0 !important;
    }
    #block_promotionDuration {
      margin-left: 20px;
    }
-->
</style>


<div id="content_promotionForm" class="">

  <c:if test="${!isStampPromo}">
  <div class="page-header page-header2"><h1><spring:message code="promotion_label_create" /></h1></div>
  </c:if>
  <c:if test="${isStampPromo}">
  <div class="page-header page-header2"><h1><spring:message code="stamp_promo_create" /></h1></div>
  </c:if>
  
  
  <c:url var="url_listPromotions" value="/promotion"/>
  <c:url var="url_promotionForm" value="${promotionFormAction}"/>
  <form:form id="promotionForm" name="promotion" modelAttribute="promotion" action="${url_promotionForm}" data-url="${url_listPromotions}" class="form-horizontal">
  <fieldset>    

    <div class="content_promo">

      <div class="hide alert alert-error" id="content_error">
          <button type="button" class="close" onclick="$(this).parent().hide();">&times;</button>
          <div><form:errors path="*"/></div>    
      </div>

      <div class="">

        <div class="block-stack-01">
          <jsp:include page="promotionFormCommon/promotionCommonForm.jsp" />
        </div>

        <div class="block-stack-01">          
          <div id="content_target" class="control-group"><jsp:include page="targets/targets.jsp" /></div>
        </div>
        
        <c:if test="${!isStampPromo}">
        <div class="block-stack-01">
          <jsp:include page="promotionFormCommon/promotionRewardsForm.jsp" />
        </div>
        </c:if>
        
        
        <c:if test="${isStampPromo}">
        <form:hidden path="rewardType"  />
        <div class="block-stack-01">
          
          <jsp:include page="stamp/stampRewardsForm.jsp" />
        </div>
        <br/>
        <div class="block-stack-01">
          
          <jsp:include page="stamp/stampRedeemForm.jsp" />
        </div>
        </c:if>

        <c:if test="${isMarketingHead}">
        <div class="block-stack-01">
          <jsp:include page="promotionFormCommon/promotionRemarksForm.jsp" />
        </div>
        </c:if>

      </div>

    </div>
  
  </fieldset>

  <div class="form-actions container-btn">
    <jsp:include page="promotionFormCommon/promotionButtons.jsp" />
  </div>

  </form:form>

	<div id="form_promotionProgram"><jsp:include page="program/programForm.jsp" /></div>
	<jsp:include page="targets/productgroup.jsp" />
	<jsp:include page="targets/storegroup.jsp" />
	<jsp:include page="targets/membergroup.jsp" />
	<jsp:include page="targets/paymenttype.jsp" />
  <div id="storeGroupContainer"></div>
  <div id="productGroupContainer"></div>
  <div id="memberGroupContainer"></div>

  <script src='<c:url value="/js/viewspecific/promotion/promotionForm.js"/>' type='text/javascript'><![CDATA[&nbsp; ]]></script>
  <script src='<c:url value="/js/bootstrap/bootstrap-datepicker.js"/>' type='text/javascript'><![CDATA[&nbsp; ]]></script>
  <script src='<c:url value="/js/bootstrap/bootstrap-timepicker.min.js"/>' type='text/javascript'><![CDATA[&nbsp; ]]></script>
  <link href="<c:url value="/css/bootstrap/datepicker.css" />" rel="stylesheet" />
  <link href="<c:url value="/css/bootstrap/bootstrap-timepicker.min.css" />" rel="stylesheet" />
  <link href="<c:url value="/styles/temp.css" />" rel="stylesheet" />
  <script src="<spring:url value="/js/jquery/jquery.numberInput.js" />" ></script>
  <script src="<spring:url value="/js/common/typeaheadutil.js" />"></script>



</div>