package com.transretail.crm.web.controller;


import java.io.ByteArrayInputStream;
import java.io.IOException;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.io.IOUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.transretail.crm.core.entity.CrmFile;
import com.transretail.crm.core.service.CrmFileService;
import com.transretail.crm.web.controller.util.ControllerResponse;
import com.transretail.crm.web.controller.util.ControllerResponseExceptionHandler;


@RequestMapping("/file")
@Controller
public class CrmFileController extends ControllerResponseExceptionHandler {

	@Autowired
	CrmFileService service;



	@RequestMapping( value = "/render/{id}", method = RequestMethod.GET )
    public void render( 
    		@PathVariable(value="id") Long id, 
    		BindingResult result,
    		HttpServletResponse response )  throws IOException {
		
		CrmFile file = service.get( id );
		ServletOutputStream out = null;
		
        try {
        	response.setContentType( file.getContentType() );
        	out = response.getOutputStream();
            IOUtils.copy( new ByteArrayInputStream( file.getFile() ), out );
		} 
        catch ( Exception e ) {
			e.printStackTrace();
		}
        finally {
        	try { out.flush(); } catch ( Exception e ) {}
        }
    }

	@RequestMapping( value="/redirect/{id}", produces="application/json; charset=utf-8", method = RequestMethod.GET )
	public @ResponseBody ControllerResponse redirect( @PathVariable(value="id") Long id ) {
		ControllerResponse resp = new ControllerResponse();
		resp.setResult( service.temporarilySaveFile( id ) );
		resp.setSuccess( true );
		return resp;
	}

}
