package com.transretail.crm.impl;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashSet;

import junit.framework.Assert;

import org.joda.time.LocalDate;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.transretail.crm.core.entity.lookup.LookupDetail;
import com.transretail.crm.core.entity.lookup.LookupHeader;
import com.transretail.crm.core.repo.LookupDetailRepo;
import com.transretail.crm.core.repo.LookupHeaderRepo;
import com.transretail.crm.core.service.CodePropertiesService;
import com.transretail.crm.giftcard.dto.ReturnRecordDto;
import com.transretail.crm.giftcard.dto.ReturnSearchDto;
import com.transretail.crm.giftcard.entity.GiftCardCustomerProfile;
import com.transretail.crm.giftcard.entity.ProductProfile;
import com.transretail.crm.giftcard.entity.ProductProfile.Denomination;
import com.transretail.crm.giftcard.entity.QReturnRecord;
import com.transretail.crm.giftcard.entity.ReturnRecord;
import com.transretail.crm.giftcard.entity.SalesOrder;
import com.transretail.crm.giftcard.entity.SalesOrderItem;
import com.transretail.crm.giftcard.entity.support.ProductProfileStatus;
import com.transretail.crm.giftcard.entity.support.ReturnStatus;
import com.transretail.crm.giftcard.entity.support.SalesOrderStatus;
import com.transretail.crm.giftcard.entity.support.SalesOrderType;
import com.transretail.crm.giftcard.repo.GiftCardCustomerProfileRepo;
import com.transretail.crm.giftcard.repo.ProductProfileRepo;
import com.transretail.crm.giftcard.repo.ReturnRecordRepo;
import com.transretail.crm.giftcard.repo.SalesOrderRepo;
import com.transretail.crm.giftcard.service.ReturnGiftCardService;

/**
 *
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath*:/META-INF/spring/applicationContext*.xml",
    "classpath:META-INF/spring/applicationContext-test.xml"})
public class ReturnGiftCardServiceTest {
	@Autowired
	private ReturnGiftCardService service;
	@Autowired
	private ReturnRecordRepo repo;
	@Autowired
	private SalesOrderRepo orderRepo;
	@Autowired
	private ProductProfileRepo productRepo;
	@Autowired
	private GiftCardCustomerProfileRepo customerRepo;
	
	private SalesOrder order;
	private ReturnRecord record;
	
	@Autowired
    private CodePropertiesService codePropertiesService;
    @Autowired
    private LookupHeaderRepo lookupHeaderRepo;
    @Autowired
    private LookupDetailRepo lookupDetailRepo;

	
	@Before
	public void init() {
		LookupHeader hdr = new LookupHeader(codePropertiesService.getProductProfileFaceValue());
        hdr = lookupHeaderRepo.saveAndFlush(hdr);

        LookupDetail dtl = new LookupDetail("PPFV5");
        dtl.setDescription("500000");
        dtl.setHeader(hdr);
        dtl = lookupDetailRepo.saveAndFlush(dtl);
		
		ProductProfile product = new ProductProfile();
		product.setCardFee(1.5);
		product.setEffectiveMonths(12l);
		product.setFaceValue(dtl);
		product.setProductCode("0000000000001");
		product.setStatus(ProductProfileStatus.APPROVED);
		product.setUnitCost(.5);
		product = productRepo.save(product);
		
		GiftCardCustomerProfile customer = new GiftCardCustomerProfile();
		customer.setContactFirstName("fname");
		customer.setContactLastName("lname");
		customer.setContactNo("contactno");
		customer.setName("name");
		customer.setInvoiceTitle("invoicetitle");
		customer.setCustomerId("customerid");
		customer = customerRepo.save(customer);
		
		order = new SalesOrder();
		order.setOrderNo("BBMMS0501001");
		order.setOrderDate(new LocalDate());
		order.setStatus(SalesOrderStatus.SOLD);
		order.setOrderType(SalesOrderType.B2B_SALES);
		order.setItems(new HashSet<SalesOrderItem>());
		order.setCustomer(customer);
		
		SalesOrderItem item = new SalesOrderItem();
		item.setProduct(product);
		item.setQuantity(10l);
		item.setFaceAmount(new BigDecimal(Denomination.D500K.getAmount() * 10));
		item.setOrder(order);
		order.getItems().add(item);
		order = orderRepo.save(order);
		
		record = new ReturnRecord();
		record.setOrderNo(order);
		record.setRecordNo("111111111");
		record.setStatus(ReturnStatus.NEW);
		record = repo.save(record);
	}
	@After
	public void teardown() {
		repo.deleteAll();
		orderRepo.deleteAll();
	}
	
	@Test
	public void saveRefundTest() {
		Assert.assertNull(repo.findOne(record.getId()).getRefundAmount());
		ReturnRecordDto rec = new ReturnRecordDto();
		rec.setId(record.getId());
		rec.setRefundAmount(BigDecimal.ZERO);
		service.saveRefund(rec);
		Assert.assertTrue(repo.findOne(record.getId()).getRefundAmount().compareTo(BigDecimal.ZERO) == 0);
	}
	@Test
	public void getReturnTest() {
		ReturnRecordDto recDto = service.getReturn(record.getId());
		Assert.assertTrue(recDto.getRecordNo().equalsIgnoreCase("111111111"));
	}
	@Test
	public void getReturnByRecordNoTest() {
		ReturnRecordDto recDto = service.getReturnByRecordNo("111111111");
		Assert.assertNotNull(recDto);
		recDto = service.getReturnByRecordNo("111111112");
		Assert.assertNull(recDto);
	}
	@Test
	public void saveReturnTest() {
		long initCnt = repo.count();
		ReturnRecordDto recDto = service.getReturnByRecordNo("111111111");
		recDto.setStatus(ReturnStatus.APPROVED);
		recDto.setOrderNo(order.getOrderNo());
		service.saveReturn(recDto);
		Assert.assertEquals(initCnt, repo.count());
		
		recDto = new ReturnRecordDto();
		recDto.setStatus(ReturnStatus.NEW);
		recDto.setOrderNo(order.getOrderNo());
		service.saveReturn(recDto);
		Assert.assertEquals(initCnt + 1, repo.count());
	}
	@Test
	public void getReturnsTest() {
		Assert.assertTrue(service.getReturns(new ReturnSearchDto()).getNumberOfElements() == 1);
	}
	@Test
	public void updateReturnTest() {
		QReturnRecord qReturn = QReturnRecord.returnRecord;
		long initCnt = repo.count(qReturn.status.eq(ReturnStatus.APPROVED));
		service.updateReturn("111111111", ReturnStatus.APPROVED);
		Assert.assertTrue(repo.count(qReturn.status.eq(ReturnStatus.APPROVED)) == initCnt + 1);
	}
	@Test
	public void approveReturnTest() {
		QReturnRecord qReturn = QReturnRecord.returnRecord;
		long initCnt = repo.count(qReturn.status.eq(ReturnStatus.APPROVED));
		service.approveReturn(record.getId(), ReturnStatus.APPROVED);
		Assert.assertTrue(repo.count(qReturn.status.eq(ReturnStatus.APPROVED)) == initCnt + 1);
	}
}



