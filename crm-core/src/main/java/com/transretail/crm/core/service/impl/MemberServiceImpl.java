package com.transretail.crm.core.service.impl;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.IteratorUtils;
import org.apache.commons.lang3.BooleanUtils;
import org.apache.commons.lang3.StringUtils;
import org.joda.time.LocalDate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.mysema.query.BooleanBuilder;
import com.mysema.query.Tuple;
import com.mysema.query.jpa.JPQLQuery;
import com.mysema.query.jpa.impl.JPAQuery;
import com.mysema.query.jpa.impl.JPAUpdateClause;
import com.mysema.query.types.OrderSpecifier;
import com.mysema.query.types.expr.BooleanExpression;
import com.mysema.query.types.path.StringPath;
import com.transretail.crm.common.service.dto.request.PagingParam;
import com.transretail.crm.common.service.exception.GenericServiceException;
import com.transretail.crm.common.service.util.SpringDataPagingUtil;
import com.transretail.crm.common.util.SimpleEncrytor;
import com.transretail.crm.core.dto.MemberDto;
import com.transretail.crm.core.dto.MemberResultList;
import com.transretail.crm.core.dto.MemberSearchDto;
import com.transretail.crm.core.dto.PointsConfigDto;
import com.transretail.crm.core.entity.ApplicationConfig;
import com.transretail.crm.core.entity.MemberModel;
import com.transretail.crm.core.entity.QMemberModel;
import com.transretail.crm.core.entity.embeddable.Address;
import com.transretail.crm.core.entity.embeddable.Channel;
import com.transretail.crm.core.entity.embeddable.CustomerProfile;
import com.transretail.crm.core.entity.embeddable.Npwp;
import com.transretail.crm.core.entity.embeddable.ProfessionalProfile;
import com.transretail.crm.core.entity.enums.AppKey;
import com.transretail.crm.core.entity.enums.MemberCreatedFromType;
import com.transretail.crm.core.entity.enums.MemberStatus;
import com.transretail.crm.core.entity.lookup.LookupDetail;
import com.transretail.crm.core.entity.lookup.Store;
import com.transretail.crm.core.repo.AddressRepo;
import com.transretail.crm.core.repo.ApplicationConfigRepo;
import com.transretail.crm.core.repo.MemberRepo;
import com.transretail.crm.core.repo.StoreRepo;
import com.transretail.crm.core.security.model.impl.CustomSecurityUserDetailsImpl;
import com.transretail.crm.core.security.util.UserUtil;
import com.transretail.crm.core.service.CodePropertiesService;
import com.transretail.crm.core.service.MemberService;
import com.transretail.crm.core.service.PointsTxnManagerService;
import com.transretail.crm.core.util.generator.IdGeneratorService;
import com.transretail.crm.logging.annotation.Loggable;

@Service("memberService")
@Transactional
public class MemberServiceImpl implements MemberService {
    @PersistenceContext
    private EntityManager entityManager;
    @Autowired
    MemberRepo memberRepo;
    @Autowired
    StoreRepo storeRepo;
    @Autowired
    IdGeneratorService pinGeneratorService;
    @Autowired
    IdGeneratorService accountIdGeneratorService;
    @Autowired
    private PasswordEncoder passwordEncoder;
    @Autowired
    private CodePropertiesService codePropertiesService;
    @Autowired
    private AddressRepo addressRepo;
    @Autowired
    private ApplicationConfigRepo applicationConfigRepo;
    
    @Autowired
	PointsTxnManagerService pointsManagerService;
    
    private static final String COMMA = ","; 
    protected static final String DEFAULT_PWD_FROM_BIRTH_DATE_FORMAT = "ddMMyyyy";
    
    @Override
    public MemberResultList listMemberDto(MemberSearchDto searchDto) {
    	QMemberModel qModel = QMemberModel.memberModel;
    	BooleanExpression storeFilter = null;
    	CustomSecurityUserDetailsImpl userDetails = UserUtil.getCurrentUser();
    	if(!StringUtils.defaultString(userDetails.getInventoryLocation()).equals(codePropertiesService.getDetailInvLocationHeadOffice())) {
        	storeFilter = qModel.registeredStore.eq(userDetails.getStoreCode());
        }
    	BooleanExpression filter = qModel.memberType.code.notEqualsIgnoreCase(codePropertiesService.getDetailMemberTypeEmployee()).and(searchDto.createSearchExpression());
    	PagingParam paging = searchDto.getPagination();
    	if(paging.getPageSize() >= 0) {
    		Pageable pageable = SpringDataPagingUtil.INSTANCE.createPageable(searchDto.getPagination());
    		Page<MemberModel> page = memberRepo.findAll(filter, pageable);
    		List<MemberDto> list = convertMemberListToDtoList(page.getContent());
            populateStoreName(list);
            return new MemberResultList(list, page.getTotalElements(), page.hasPreviousPage(),
                page.hasNextPage());
    	} else if(paging.getOrder() != null && !paging.getOrder().isEmpty()) {
    		Iterable<MemberModel> memberList = memberRepo.findAll(filter, SpringDataPagingUtil.INSTANCE.createOrderSpecifier(searchDto.getPagination(), MemberModel.class));
    		List<MemberDto> list = convertMemberListToDtoList(memberList);
            populateStoreName(list);
            return new MemberResultList(list, list.size(), false, false);
    	} else {
    		Iterable<MemberModel> memberList = memberRepo.findAll(filter);
    		List<MemberDto> list = convertMemberListToDtoList(memberList);
            populateStoreName(list);
            return new MemberResultList(list, list.size(), false, false);
    	}
    }
    
    @Override
    public MemberResultList listMemberDtoForPrint(MemberSearchDto searchDto, Integer segment) {
        QMemberModel qModel = QMemberModel.memberModel;
        Pageable pageable = SpringDataPagingUtil.INSTANCE.createPageable(searchDto.getPagination());
        
        BooleanExpression filter = searchDto.createSearchExpression();
        JPQLQuery query = new JPAQuery( entityManager );
        
        if (filter != null) {
            query = query.from(qModel)
            .where(filter);
            
            segment--;
            query = query.offset(segment*5000L).limit(5000L);
        } else {
            query = query.from(qModel);
            
            segment--;
            query = query.offset(segment*5000L).limit(5000L);
        }
        query = query.orderBy( new OrderSpecifier( com.mysema.query.types.Order.ASC, qModel.created ) );
        
        List<MemberModel> memberList = query.list(qModel);
        List<MemberDto> list = new ArrayList<MemberDto>();
        for (MemberModel model : memberList) {
            list.add(new MemberDto(model));
        }
        return new MemberResultList(list, list.size(), false, false);
    }
    
    private void populateStoreName(List<MemberDto> members) {
    	for(MemberDto memberDto : members) {
    		if(StringUtils.isNotBlank(memberDto.getRegisteredStore())) {
    			Store store = storeRepo.findByCode(memberDto.getRegisteredStore());
    			if(store != null)
    				memberDto.setRegisteredStoreName(store.getName());	
    		}
    		
    	}
    }

    @Override
    public boolean emailExists(String email) {
        return memberRepo.count(QMemberModel.memberModel.email.eq(email)) > 0;
    }

    @Override
    public boolean emailExists(String username, String email) {
        QMemberModel memberModel = QMemberModel.memberModel;
        return memberRepo.count(memberModel.username.ne(username).and(memberModel.email.eq(email))) > 0;
    }

    @Override
    public boolean usernameExists(String username) {
        return memberRepo.count(QMemberModel.memberModel.username.eq(username)) > 0;
    }

    @Override
    public boolean accountIdExists(String accountId) {
        return memberRepo.count(QMemberModel.memberModel.accountId.eq(accountId)) > 0;
    }

    @Override
    public boolean contactExists(String contact) {
        return memberRepo.count(QMemberModel.memberModel.contact.eq(contact)) > 0;
    }

    @Override
    public boolean contactExists(String username, String contact) {
        QMemberModel memberModel = QMemberModel.memberModel;
        return memberRepo.count(memberModel.username.ne(username).and(memberModel.contact.eq(contact))) > 0;
    }

    @Override
    public boolean ktpIdExists(String username, String ktpId) {
        QMemberModel memberModel = QMemberModel.memberModel;
        return memberRepo.count(memberModel.username.ne(username).and(memberModel.ktpId.eq(ktpId))) > 0;
    }
    
    public long countAllCustomerModels() {
        return memberRepo.count();
    }

    @Transactional
    public void deleteCustomerModel(MemberModel customerModel) {
        QMemberModel qMemberModel = QMemberModel.memberModel;
        JPAUpdateClause updateClause = new JPAUpdateClause(entityManager, qMemberModel);
        if (customerModel.getMemberType().getCode().equalsIgnoreCase("MTYP002"))
            updateClause.set(qMemberModel.accountStatus, MemberStatus.TERMINATED);
        else
            updateClause.set(qMemberModel.accountStatus, MemberStatus.INACTIVE);
        updateClause.where(qMemberModel.id.eq(customerModel.getId()));
        updateClause.execute();
        entityManager.clear();
        entityManager.flush();
    }


    public MemberModel findByAccountId(String accountId) {
        return memberRepo.findByAccountId(accountId);
    }


    @Override
    public MemberModel findActiveWithAccountId(String accountId) {
    	if ( StringUtils.isBlank( accountId ) ) {
    		return null;
    	}
        return memberRepo.findOne( 
        		QMemberModel.memberModel.accountId.equalsIgnoreCase( accountId )
        		.and( QMemberModel.memberModel.accountStatus.in( MemberStatus.ACTIVE ) ) );
    }
    
    @Override
	public MemberDto findDtoByAccountId(String accountId) {
    	MemberModel theMember = memberRepo.findByAccountId(accountId);
    	if(theMember == null)
    		return null;
    	else
    		return new MemberDto(theMember);
	}

    @Override
    public MemberModel findByContact(String contact) {
    	StringPath memContact = QMemberModel.memberModel.contact;
    	Iterable<MemberModel> members = memberRepo.findAll( 
    			memContact.equalsIgnoreCase( contact ).or( memContact.like( contact + ",%" ) ).or( memContact.like( "%," + contact ) ) );
        return members.iterator().hasNext()? members.iterator().next() : null;
    }

    @Override
    public List<MemberModel> findByEmail(String email) {
        return memberRepo.findByEmail(email);
    }


    public MemberModel findCustomerModel(Long id) {
        return memberRepo.findOne(id);
    }

    public List<MemberModel> findAllCustomerModels() {
        return memberRepo.findAll();
    }

    public List<MemberModel> findAllByMemberType(LookupDetail type) {
        return memberRepo.findByMemberType(type);
    }



    private List<MemberDto> convertMemberListToDtoList(Iterable<MemberModel> iterable) {
    	List<MemberDto> dtoList = new ArrayList<MemberDto>();
    	for(MemberModel member : iterable) {
    		MemberDto dto = new MemberDto(member);
    		if ( StringUtils.isNotBlank( member.getParentAccountId() ) ) {
    			MemberModel parent = memberRepo.findOne( QMemberModel.memberModel.accountId.equalsIgnoreCase( member.getParentAccountId() ) );
    			if ( null != parent && null != parent.getAccountStatus() ) {
            		dto.setParentStatus( parent.getAccountStatus().toString() );
    			}
    		}
    		dtoList.add(dto);
    	}
    	return dtoList;
    }
    
    public List<MemberDto> findAllCustomerModelsAsDto() {
    	QMemberModel qMemberModel = QMemberModel.memberModel;
    	return convertMemberListToDtoList(memberRepo.findAll(qMemberModel.memberType.code.notEqualsIgnoreCase(codePropertiesService.getDetailMemberTypeEmployee())));
    }

    public List<MemberModel> findCustomerModelEntries(int firstResult, int maxResults) {
    	QMemberModel qMemberModel = QMemberModel.memberModel;
        return memberRepo.findAll(qMemberModel.memberType.code.notEqualsIgnoreCase(codePropertiesService.getDetailMemberTypeEmployee()),
        		new org.springframework.data.domain.PageRequest(firstResult / maxResults, maxResults)).getContent();
    }
    
    public List<MemberDto> findCustomerModelEntriesAsDto(int firstResult, int maxResults) {
    	return convertMemberListToDtoList(findCustomerModelEntries(firstResult, maxResults));
    }

    @Loggable
    public void saveCustomerModel(MemberModel customerModel) {
        customerModel.setAccountId(accountIdGeneratorService.generateId());
        try {
            customerModel.setPin(SimpleEncrytor.getInstance().encrypt(pinGeneratorService.generateId()));
            customerModel.setPinEncryped(true);
        } catch (SimpleEncrytor.EncryptDecryptException e) {
            throw new GenericServiceException(e);
        }
        customerModel.setPassword(passwordEncoder.encode(customerModel.getPassword()));
        customerModel.setUsername(StringUtils.isBlank(customerModel.getUsername()) ?
            customerModel.getAccountId() : customerModel.getUsername());
        if(customerModel.getAddress() != null) {
        	Address address = addressRepo.save(customerModel.getAddress());
        	customerModel.setAddress(address);
        }
        memberRepo.saveAndFlush(customerModel);
    }

    public MemberModel updateCustomerModel(MemberModel customerModel) {
        // To be safe, passwords cannot be updated at this method
        MemberModel oldModel = memberRepo.findByUsername(customerModel.getUsername());
        customerModel.setPassword(oldModel.getPassword());
        if(customerModel.getAddress() != null) {
        	customerModel.setAddress(addressRepo.save(customerModel.getAddress()));
        }
        return memberRepo.save(customerModel);
    }
    
    public Address saveMemberAddress(Address address) {
    	return addressRepo.save(address);
    }
    
    public MemberDto updateCustomerModel(MemberDto customerModel) {
        MemberModel model = memberRepo.findOne(customerModel.getId());
        model.setMemberType(customerModel.getMemberType());
    	model.setCompanyName(customerModel.getCompanyName());
    	model.setUsername(customerModel.getUsername());
    	model.setEmpType( customerModel.getEmpType() );
    	model.setStoreName( customerModel.getStoreName() );
    	if(!StringUtils.isBlank(customerModel.getPassword()) && 
    			!customerModel.getPassword().equals(customerModel.getPasswordCheck())) {
    		model.setPassword(passwordEncoder.encode(customerModel.getPassword()));
    	}
    	model.setKtpId(customerModel.getKtpId());
    	model.setPin(customerModel.getEncryptedPin());
        model.setPinEncryped(true);
    	model.setFirstName(customerModel.getFirstName());
    	model.setLastName(customerModel.getLastName());
    	model.setEnabled(customerModel.getEnabled());
    	model.setParentAccountId(customerModel.getParent());
//    	model.setContact(customerModel.getContact());
    	List<String> contacts = customerModel.getContact();
    	if(contacts != null && contacts.size() > 0) {
    		StringBuilder builder = new StringBuilder(contacts.get(0));
    		for(int i = 1; i < contacts.size(); i++) {
    			builder.append(COMMA).append(contacts.get(i));
    		}
    		model.setContact(builder.toString());
    	}

    	List<String> times = customerModel.getBestTimeToCall();
    	if(times != null && times.size() > 0) {
    		StringBuilder builder = new StringBuilder(times.get(0));
    		for(int i = 1; i < times.size(); i++) {
    			builder.append(COMMA).append(times.get(i));
    		}
    		model.setBestTimeToCall(builder.toString());
    	}

    	List<String> socialMediaAccts = customerModel.getSocialMediaAccts();
    	if( CollectionUtils.isNotEmpty( socialMediaAccts ) ) {
    		StringBuilder builder = new StringBuilder( socialMediaAccts.get(0) );
    		for( int i = 1; i < socialMediaAccts.size(); i++ ) {
    			builder.append( COMMA ).append( socialMediaAccts.get(i) );
    		}
    		model.setSocialMediaAccts( builder.toString() );
    	}
    	
    	model.setEmail(customerModel.getEmail());
    	model.setRegisteredStore(customerModel.getRegisteredStore());
//    	model.setBestTimeToCall(customerModel.getBestTimeToCall());
    	if(customerModel.getAccountStatus() == null) {
    		model.setAccountStatus(MemberStatus.ACTIVE);
    	}
    	else{
    		model.setAccountStatus(customerModel.getAccountStatus());
    	}
    	
    	if (customerModel.getAccountStatus().equals(MemberStatus.TERMINATED) && customerModel.getTerminationDate() == null) {
            model.setTerminationDate(LocalDate.now());
        } else if (customerModel.getAccountStatus().equals(MemberStatus.ACTIVE) && customerModel.getTerminationDate() != null) {
            model.setTerminationDate(null);
        }
    	
    	if ( null == model.getMemberCreatedFromType() ) {
        	model.setMemberCreatedFromType(MemberCreatedFromType.FROM_CRM);
    	}
    	model.setCustomerProfile(customerModel.getCustomerProfile());
    	model.setChannel(customerModel.getChannel());
        
    	Npwp npwp = model.getNpwp();
        if(npwp == null) {
            npwp = new Npwp();
            model.setNpwp(npwp);
        }
        npwp.setNpwpAddress(customerModel.getNpwpAddress());
        npwp.setNpwpName(customerModel.getNpwpName());
        if(StringUtils.isNotBlank(customerModel.getNpwpId()))
        	npwp.setNpwpId(customerModel.getNpwpId());
        else
        	npwp.setNpwpId(Npwp.DEFAULT_NPWP_ID);
        
        Address address = customerModel.getAddress();
        if(address != null) {
        	model.setAddress(updateAddress(model.getAddress(), address));
        }
        
        Address oldAddress = null;
        if(model.getProfessionalProfile() != null) {
        	oldAddress = model.getProfessionalProfile().getBusinessAddress();
        }
        Address newAddress = customerModel.getProfessionalProfile().getBusinessAddress();

        model.setProfessionalProfile(customerModel.getProfessionalProfile());
        if(newAddress != null) {
        	model.getProfessionalProfile().setBusinessAddress(updateAddress(oldAddress, newAddress));
        }
        model.setMarketingDetails(customerModel.getMarketingDetails());
        model.setIdNumber(customerModel.getIdNumber());
        model.setHomePhone(customerModel.getHomePhone());
        model.setConfigurableFields(customerModel.getConfigurableFields());
        if ( model.getMemberType().getCode().equalsIgnoreCase( codePropertiesService.getDetailMemberTypeEmployee() )
        		&& StringUtils.isNotBlank( customerModel.getAccountId() ) ) {
        	model.setAccountId( customerModel.getAccountId() );
        }
        return new MemberDto(memberRepo.save(model));
    }
    
    private Address updateAddress(Address oldAddress, Address newAddress) {
    	if(oldAddress != null) {
    		oldAddress.setBlock(newAddress.getBlock());
    		oldAddress.setBuilding(newAddress.getBuilding());
    		oldAddress.setCity(newAddress.getCity());
    		oldAddress.setDistrict(newAddress.getDistrict());
    		oldAddress.setFloor(newAddress.getFloor());
    		oldAddress.setKm(newAddress.getKm());
    		oldAddress.setPostCode(newAddress.getPostCode());
    		oldAddress.setProvince(newAddress.getProvince());
    		oldAddress.setRoom(newAddress.getRoom());
    		oldAddress.setRt(newAddress.getRt());
    		oldAddress.setRw(newAddress.getRw());
    		oldAddress.setStreet(newAddress.getStreet());
    		oldAddress.setStreetNumber(newAddress.getStreetNumber());
    		oldAddress.setSubdistrict(newAddress.getSubdistrict());
    		oldAddress.setVillage(newAddress.getVillage());
    		return addressRepo.save(oldAddress);
    	}
    	return addressRepo.save(newAddress);
    }

    public void updatePin(Long id, String newPin) {
        MemberModel model = memberRepo.findOne(id);
        try {
            model.setPin(SimpleEncrytor.getInstance().encrypt(newPin));
            model.setPinEncryped(true);
        } catch (SimpleEncrytor.EncryptDecryptException e) {
            throw new GenericServiceException(e);
        }
        memberRepo.save(model);
    }

    public MemberModel findDuplicate(MemberModel inModel) {
        return memberRepo.findByContact(inModel.getContact());
    }

    public MemberModel findDuplicateContact(MemberModel inModel) {
        MemberModel theModel = memberRepo.findByContact(inModel.getContact());
        return (theModel != null && !theModel.getId().equals(inModel.getId())) ? theModel : null;
    }
    
    public MemberDto findDuplicateContact(MemberDto memberDto) {
    	List<String> contacts = memberDto.getContact();
    	String contact = null;
    	if(contacts != null && contacts.size() > 0) {
    		StringBuilder builder = new StringBuilder(contacts.get(0));
    		for(int i = 1; i < contacts.size(); i++) {
    			builder.append(COMMA).append(contacts.get(i));
    		}
    		contact = (builder.toString());
    	}
    	MemberModel model = memberRepo.findByContact(contact);
    	MemberDto theModel = null;
    	if(model != null)
    		theModel = new MemberDto(model);
        return (theModel != null && !theModel.getId().equals(memberDto.getId())) ? theModel : null;
    }

    public MemberModel findDuplicateEmail(MemberModel inModel) {
        List<MemberModel> theModels = memberRepo.findByEmail(inModel.getEmail());
        for (MemberModel theModel : theModels) {
            if (!theModel.getId().equals(inModel.getId())) {
                return theModel;
            }
        }
        return null;
    }
    
    public MemberDto findDuplicateEmail(MemberDto memberDto) {
        List<MemberModel> theModels = memberRepo.findByEmail(memberDto.getEmail());
        for (MemberModel theModel : theModels) {
            if (!theModel.getId().equals(memberDto.getId())) {
                return new MemberDto(theModel);
            }
        }
        return null;
    }

    public MemberModel findDuplicateUsername(MemberModel inModel) {
        MemberModel theModel = memberRepo.findByUsername(inModel.getUsername());
        return (theModel != null && !theModel.getId().equals(inModel.getId())) ? theModel : null;
    }
    
    @Override
    public MemberDto findDuplicateKtpId(MemberDto memberDto) {
    	QMemberModel qMember = QMemberModel.memberModel;
    	MemberModel model = memberRepo.findOne(qMember.ktpId.eq(memberDto.getKtpId()));
    	MemberDto theModel = null;
    	if(model != null)
    		theModel = new MemberDto(model);
        return (theModel != null && !theModel.getId().equals(memberDto.getId())) ? theModel : null;
    }
    
    public MemberDto findDuplicateUsername(MemberDto memberDto) {
    	MemberModel model = memberRepo.findByUsername(memberDto.getUsername());
    	MemberDto theModel = null;
    	if(model != null)
    		theModel = new MemberDto(model);
        return (theModel != null && !theModel.getId().equals(memberDto.getId())) ? theModel : null;
    }

    public MemberModel findDuplicateCompany(MemberModel inModel) {
    	MemberDto model = findDuplicateCompany(new MemberDto(inModel));
    	if(model == null) return null;
    	return new MemberModel(model);
//    	List<MemberModel>  theModel = memberRepo.findByCompanyName(inModel.getCompanyName());
//    	if(theModel == null || theModel.size() == 0) {
//    		return null;
//    	}
//    	
//    	for(MemberModel model : theModel) {
//    		MemberModel duplicate = model;
//    		
//    		if(model.getId().equals(inModel.getId())) {
//    			duplicate = null;
//    		}
//    		
//    		if(inModel.getParent() != null) {
//        		// if model found is the parent
//    			String parentId = inModel.getParent().getAccountId();
//        		if(parentId.equals(model.getAccountId())) {
//        			duplicate = null;
//        		}
//        		// if model found has the same parent as the given model
//        		else if(model.getParent() != null && parentId.equals(model.getParent().getAccountId())) {
//        			duplicate = null;
//        		}
//        	}
//    		
//    		if(duplicate != null) {
//    			return duplicate;
//    		}
//    	}
//    	
//    	return null;
    }
    
    public MemberDto findDuplicateCompany(MemberDto memberDto) {
    	List<MemberModel>  theModel = memberRepo.findByCompanyName(memberDto.getCompanyName());
    	if(theModel == null || theModel.size() == 0) {
    		return null;
    	}
    	for(MemberModel model : theModel) {
            if(model.getId().equals(memberDto.getId())) continue;
            if(StringUtils.isNotBlank(memberDto.getParent()) && model.getAccountId().equals(memberDto.getParent()))continue;
            if(model.getParentAccountId() == null || (StringUtils.isNotBlank(memberDto.getParent()) && model.getParentAccountId().equals(memberDto.getParent()))) continue;
            return new MemberDto(model);
    	}
    	return null;
    }

    public MemberModel findDuplicateName(MemberModel inModel) {
        MemberModel theModel = memberRepo.findByLastNameAndFirstName(inModel.getLastName(), inModel.getFirstName());
        return (theModel != null && !theModel.getId().equals(inModel.getId())) ? theModel : null;
    }
    
    public MemberDto findDuplicateName(MemberDto memberDto) {
    	MemberModel model = memberRepo.findByLastNameAndFirstName(memberDto.getLastName(), memberDto.getFirstName());
    	MemberDto theModel = null;
    	if(model != null)
    		theModel = new MemberDto(model);
        return (theModel != null && !theModel.getId().equals(memberDto.getId())) ? theModel : null;
    }
    
    public MemberDto findDuplicateNpwpId(MemberDto member) {
    	QMemberModel qmodel = QMemberModel.memberModel;
    	Iterable<MemberModel> it = memberRepo.findAll( 
    		qmodel.npwp.npwpId.equalsIgnoreCase( member.getNpwpId() )
    			.and( new BooleanBuilder().orAllOf( 
    					null != member.getId()? qmodel.id.notIn( member.getId() ) : null,
    					null != member.getAccountId()? qmodel.accountId.notEqualsIgnoreCase( member.getAccountId() ) : null,
        				StringUtils.isNotBlank( member.getParent() )? 
        						qmodel.accountId.notEqualsIgnoreCase( member.getParent() ).and( qmodel.parentAccountId.notEqualsIgnoreCase( member.getParent() ) )
        						: null ) ), new PageRequest( 0,  1 ) );
    	if ( null != it && it.iterator().hasNext() ) {
    		return new MemberDto( it.iterator().next() );
    	}
    	return null;
    }

    public MemberModel generateInitModel() {
        MemberModel theModel = new MemberModel();
        //theModel.setAccountId(accountIdGeneratorService.generateId());
        //theModel.setPin(pinGeneratorService.generateId());
        //theModel.setUsername(theModel.getAccountId());
        theModel.setEnabled(true);
        theModel.setCustomerProfile(new CustomerProfile());
        theModel.setChannel(new Channel());
        theModel.setProfessionalProfile(new ProfessionalProfile());
        theModel.getProfessionalProfile().setBusinessAddress(new Address());
        theModel.setAddress(new Address());

        return theModel;
    }

    public List<MemberModel> findByCriteria(MemberModel inModel,  String employeeTypeCode) {
        return memberRepo.findByCriteria(inModel, employeeTypeCode);
    }
    
    @Override
    public List<MemberDto> findByCriteria(MemberDto inModel,  String employeeTypeCode) {
    	List<MemberModel> theModels = memberRepo.findByCriteria(new MemberModel(inModel), employeeTypeCode);
    	if(theModels == null || theModels.size() == 0)
    		return null;
    	else 
    		return convertMemberListToDtoList(theModels);
    }
    
    @Override
    public MemberResultList filterMemberTypes(MemberSearchDto dto) {
    	PointsConfigDto config = pointsManagerService.getPointsConfig();
    	Pageable pageable = SpringDataPagingUtil.INSTANCE.createPageable(dto.getPagination());
        List<LookupDetail> suspendedTypes = config.getSuspendedTypes();
        BooleanExpression filter = dto.createSearchExpression();
        if (suspendedTypes != null && suspendedTypes.size() > 0) {
        	if(filter != null) {
        		filter = filter.and(QMemberModel.memberModel.memberType.notIn(suspendedTypes));
        	}
        	else filter = QMemberModel.memberModel.memberType.notIn(suspendedTypes);
        }
        Page<MemberModel> page = filter != null ? memberRepo.findAll(filter, pageable) : memberRepo.findAll(pageable);
        return new MemberResultList(convertMemberListToDtoList(page.getContent()), page.getTotalElements(), page.hasPreviousPage(),
            page.hasNextPage());
//    	return convertMemberListToDtoList(memberRepo.findAll(
//    			QMemberModel.memberModel.memberType.notIn(memberTypes)));
        
    }

    public MemberModel getModelByValidationCode(String validationCode) {
        return memberRepo.findOne(QMemberModel.memberModel.validationCode.eq(validationCode));
    }

    @Override
    public Boolean validatePin(String accountId, String pin) {
        QMemberModel qMemberModel = QMemberModel.memberModel;
        Tuple tuple = new JPAQuery(entityManager).from(qMemberModel)
            .where(qMemberModel.accountId.eq(accountId))
            .singleResult(qMemberModel.pin, qMemberModel.pinEncryped);
        if (tuple != null && tuple.size() > 0) {
            String dbPin = tuple.get(qMemberModel.pin);
            if (BooleanUtils.toBoolean(tuple.get(qMemberModel.pinEncryped))) {
                try {
                    dbPin = SimpleEncrytor.getInstance().decrypt(dbPin);
                } catch (SimpleEncrytor.EncryptDecryptException e) {
                    throw new GenericServiceException(e);
                }
            }
            return pin.equals(dbPin);
        }
        return false;
    }
    
    public MemberDto findCustomer(Long id) {
    	return new MemberDto(memberRepo.findOne(id));
    }

    @Override
    public List<MemberDto> findSupplementaryMembersByParentId(String accountId) {
    	Iterable<MemberModel> supplements = memberRepo.findAll(
    			QMemberModel.memberModel.parentAccountId.eq(accountId));
    	List<MemberDto> supplementList = new ArrayList<MemberDto>();
    	for(MemberModel member : supplements) {
    		supplementList.add(new MemberDto(member));
    	}
    	return supplementList;
    }
    
    @Override
    public void addSupplementaryMember(String parentId, MemberModel memberSupplement) {
    	memberSupplement.setParentAccountId(parentId);
    	saveCustomerModel(memberSupplement);
    }
    
    @Override
    public void saveCustomerModel(MemberDto customerModel) {
	this.doAccountDefaulting(customerModel);
     
        if(customerModel.getAddress() != null) {
        	Address address = addressRepo.save(customerModel.getAddress());
        	customerModel.setAddress(address);
        }
        Address businessAddress = customerModel.getProfessionalProfile().getBusinessAddress();
        if(businessAddress != null) {
        	customerModel.getProfessionalProfile().setBusinessAddress(addressRepo.save(businessAddress));
        } 
        MemberModel member = new MemberModel(customerModel);
        if(customerModel.getParent() != null) {
        	member.setParentAccountId(customerModel.getParent());
        }
        memberRepo.saveAndFlush(member);
    }

    @Override
    public void saveMember( MemberModel theModel ) { 
    	memberRepo.save( theModel );
    }

    @Override
    public void activateMember( long id, boolean activate ) {
    	MemberModel member = memberRepo.findOne( id );
        String parentAcctId;
    	if ( null != member ) {
            parentAcctId = member.getAccountId();
    		member.setAccountStatus( activate? MemberStatus.ACTIVE : MemberStatus.INACTIVE);
    		memberRepo.save( member );

            List<MemberDto> supplementaryList = findSupplementaryMembersByParentId(parentAcctId);
            if (null != supplementaryList) {
                for (MemberDto supplementary : supplementaryList) {
                    MemberModel sMember = memberRepo.findOne(supplementary.getId());
                    if ( null != sMember ) {
                        sMember.setAccountStatus( activate? MemberStatus.ACTIVE : MemberStatus.INACTIVE);
                        memberRepo.save( sMember );
                    }
                }
            }
        }
    }

    @Override
	public long countAllActive() {
		return memberRepo.count( QMemberModel.memberModel.accountStatus.eq( MemberStatus.ACTIVE ) );
	}

    @SuppressWarnings("unchecked")
    @Override
	public List<MemberModel> findAll( List<MemberModel> members ) {
    	Iterable<MemberModel> models = memberRepo.findAll( QMemberModel.memberModel.in( members )
    			.and( QMemberModel.memberModel.accountStatus.eq( MemberStatus.ACTIVE ) ) );
    	return null == models? new ArrayList<MemberModel>() : IteratorUtils.toList( models.iterator() );
    }

    private void doAccountDefaulting(MemberDto customerModel) {
	// Default username by mobile number 
	// ref: https://projects.exist.com/issues/show/88497
	// no need to check if the contact list is not empty. Mobile Number is mandatory by default!
	String defaultUserName = customerModel.getContact().iterator().next();
	customerModel.setUsername(StringUtils.defaultIfBlank(customerModel.getUsername(), defaultUserName));
	// Default password by birthdate with format of ddMMyyyy 
	// ref: https://projects.exist.com/issues/show/88497
	// XXXX MemberModel.customerProfile.birthdate is not mapped to type Joda
	//      Replace the code below if it was changed to Joda
	LocalDate birthdate = LocalDate.fromDateFields(
		customerModel.getCustomerProfile().getBirthdate());
	String defaultPwd = StringUtils.defaultIfBlank(customerModel.getPassword(),
		birthdate.toString(DEFAULT_PWD_FROM_BIRTH_DATE_FORMAT));
	customerModel.setPassword(passwordEncoder.encode(defaultPwd));
	
		if(!customerModel.getMemberType().getCode().equals(codePropertiesService.getDetailMemberTypeEmployee())) {
			customerModel.setAccountId(accountIdGeneratorService.generateId());
		}
		customerModel.setPin(pinGeneratorService.generateId());
    }

    @Override
    public Long countMembers(MemberSearchDto searchDto) {
        QMemberModel qModel = QMemberModel.memberModel;
        return memberRepo.count(searchDto.createSearchExpression());
    }

    @Override
    public Integer getMemberReportLimit() {
        ApplicationConfig rowLimit = applicationConfigRepo.findByKey(AppKey.MEMBER_EXPORT_LIST_ROW_LIMIT);
        if (rowLimit == null) {
            return 5000;
        }
        return Integer.parseInt(applicationConfigRepo.findByKey(AppKey.MEMBER_EXPORT_LIST_ROW_LIMIT).getValue());
    }
}
