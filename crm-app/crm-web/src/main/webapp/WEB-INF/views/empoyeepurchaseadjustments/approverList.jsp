<%@ include file="../common/taglibs.jsp" %>
  
  <spring:message var="typeName" code='menu_item_overridepermission_new_label'/>
  <spring:url value="/resources/images/delete.png" var="delete_image_url" />
  <spring:url value="/override/permissions/${item.id}" var="delete_form_url" />
    <spring:message arguments="${typeName}" code="entity_delete"
        var="delete_label" htmlEscape="false" />
      <c:set var="delete_confirm_msg"><spring:escapeBody 
      javaScriptEscape="true"><spring:message 
      code="entity_delete_confirm" /></spring:escapeBody></c:set> 
  
  <div class="page-header page-header2">
      <h1><spring:message code="menutab_programsetup_permissions_hroverridescheme" /></h1>
  </div>
  
  <div>
	<button class="btn btn-primary pull-right mb20" id="addApproverLnk">
		<spring:message code="global_menu_new" arguments="${ typeName }"/>
	</button>
</div>
  
  <div id="content_approverList">

    <div id="list_approver"></div>
  
  </div>
    <jsp:include page="approverDialog.jsp" />
    <jsp:include page="../common/confirm.jsp" />
  <script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/viewspecific/empoyeepurchaseadjustments/adjustments.js"><![CDATA[&nbsp;]]></script>
  <link href="<c:url value="/styles/temp.css" />" rel="stylesheet" />
  
  <link href="<spring:url value="/css/dataTables.css" />" rel="stylesheet">
  <script src="<spring:url value="/js/jquery/jquery.dataTables.js" />" ></script>
  <script src="<spring:url value="/js/jquery/jquery.dataTables.linkLengthMenu.js" />"></script>
  <script src="<spring:url value="/js/jquery/jquery.dataTables.customPaging.js" />"></script>
  <script src="<spring:url value="/js/jquery/ajax-datatable.js" />" ></script>
  <script src="<spring:url value="/js/common/form2js.js" />" ></script>
  <script src="<spring:url value="/js/common/json2.js" />" ></script>
  <script src="<spring:url value="/js/jquery/jquery.toObject.js" />" ></script>
  <script src="<spring:url value="/js/jquery/jquery.numberInput.js" />" ></script>
  
        <script type="text/javascript">
        
        $("#list_approver").ajaxDataTable({
          'autoload'  : true,
          'ajaxSource' : "<c:url value="/override/permissions/list" />",
          'columnHeaders' : [
          "<spring:message code="adjustments_approver_prop_username"/>",
          "<spring:message code="adjustments_approver_prop_firstname"/>",
          "<spring:message code="adjustments_approver_prop_lastname"/>",
          ""
          ],
          'modelFields' : [
             {name : 'user.username', sortable : true},
             {name : 'user.firstName', sortable : true},
             {name : 'user.lastName', sortable : true},
             {customCell : function ( innerData, sSpecific, json ) {
                if ( sSpecific == 'display') {
                return "<form action=\"<spring:url value="/override/permissions/"/>"+json.id+"\" id=\"deleteForm"+json.id+"\" method=\"post\" class=\"form-reset\">"+
                "<input type=\"hidden\" name=\"_method\" value=\"DELETE\">"+
                  "<input class=\"btn btn-primary icn-delete tiptip\" title=\"<spring:message code="emp_label_delete_permission" />\" alt=\"<spring:message code="emp_label_delete_permission" />\" type=\"button\" value=\"${delete_label}\" onclick=\"getConfirm('${delete_confirm_msg}',function(result) {if(result) $('form#deleteForm"+json.id+"').submit();});\" />"+
              "</form>";
                } else {
                  return '';
                }
                }
           }
           ]
        });  
        </script>