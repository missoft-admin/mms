package com.transretail.crm.web.controller;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.google.common.collect.Lists;
import com.transretail.crm.common.service.dto.response.ResultList;
import com.transretail.crm.core.service.CodePropertiesService;
import com.transretail.crm.core.service.LookupService;
import com.transretail.crm.giftcard.dto.PrepaidReloadInfoDto;
import com.transretail.crm.giftcard.dto.ProductProfileDto;
import com.transretail.crm.giftcard.dto.ProductProfileSearchDto;
import com.transretail.crm.giftcard.dto.ProductProfileUnitDTO;
import com.transretail.crm.giftcard.entity.support.ProductProfileStatus;
import com.transretail.crm.giftcard.service.ProductProfileService;
import com.transretail.crm.web.controller.GiftCardOrderController.ProfileSearchCriteria;
import com.transretail.crm.web.controller.editor.NumberEditor;
import com.transretail.crm.web.controller.util.ControllerResponse;

@Controller
@RequestMapping("/gc/productprofile")
public class ProductProfileController {

	@Autowired
	private ProductProfileService profileService;

	@Autowired
	private MessageSource messageSource;

	@Autowired
	private LookupService lookupService;

	@Autowired
	private CodePropertiesService codePropertiesService;

	@InitBinder
	public void initBinder(WebDataBinder binder) {
		binder.registerCustomEditor(Long.class, new NumberEditor(Long.class));
	}

	@RequestMapping(value = "/search", method = RequestMethod.POST)
	public @ResponseBody ResultList<ProductProfileDto> search(@RequestBody ProductProfileSearchDto filterDto) {
		return profileService.list(filterDto);
	}

	@RequestMapping(value = "/form", method = RequestMethod.GET, produces = "text/html")
	public String showForm(Model uiModel) {
		//93620: for create new product profile
		ProductProfileDto profileDto = new ProductProfileDto();
		profileDto.setAllowPartialRedeem(true);
		populateForm(profileDto, uiModel);
		return "giftcard/profileform";
	}

	@RequestMapping(value = "/form/{id}", method = RequestMethod.GET, produces = "text/html")
	public String showForm(
			@PathVariable("id") String id,
			Model uiModel) {
		ProductProfileDto profileDto = profileService.findDto(Long.valueOf(id));
		populateForm(profileDto, uiModel);

		/*if(profileDto.getStatus().compareTo(ProductProfileStatus.SUBMITTED) == 0)
			return "giftcard/profileapprovalform";*/

		return "giftcard/profileform";
	}

	@RequestMapping(value = "/view/{id}", method = RequestMethod.GET, produces = "text/html")
	public String showView(@PathVariable("id") String id, Model uiModel) {
		ProductProfileDto profileDto = profileService.findDto(Long.valueOf(id));
		populateForm(profileDto, uiModel);
		uiModel.addAttribute("edit", true);
		return "giftcard/profileform";
	}

	private void populateForm(ProductProfileDto profileDto, Model uiModel) {
		uiModel.addAttribute("profileForm", profileDto);
		uiModel.addAttribute("faceValues", lookupService.getActiveDetailsByHeaderCode(codePropertiesService.getProductProfileFaceValue()));
		uiModel.addAttribute("businessUnits", lookupService.getActiveDetailsByHeaderCode("STO000"));
	}

	@RequestMapping(value = "/save/{status}", method = RequestMethod.POST, produces = "application/json; charset=utf-8")
	@ResponseBody
	public ControllerResponse ajaxSave(
			@PathVariable("status") String status,
			@ModelAttribute(value = "profileForm") ProductProfileDto profileDto,
			@RequestParam(value = "reloadIds", required = false) Long[] reloadIds,
			@RequestParam(value = "reloadAmount", required = false) Long[] reloadAmount,
			@RequestParam(value = "monthEffective", required = false) Integer[] monthEffective,
			@RequestParam(value = "buIds", required = false) Long[] buIds,
			@RequestParam(value = "productUnitIds", required = false) String[] productUnit,
			BindingResult result, HttpServletRequest request) {

		ControllerResponse theResponse = new ControllerResponse();
		theResponse.setSuccess(true);

		validateProfile(result, profileDto);
		if (reloadAmount != null) {
			List<PrepaidReloadInfoDto> reloadList=Lists.newArrayList();
			for (int i = 0; i < reloadAmount.length; i++) {
				Long id = reloadIds[i];
				Long amt = reloadAmount[i];
				Integer monthE = monthEffective[i];
				PrepaidReloadInfoDto reloadInfo = new PrepaidReloadInfoDto();
				reloadInfo.setId(id);
				reloadInfo.setMonthEffective(monthE);
				reloadInfo.setReloadAmount(amt);
				reloadList.add(reloadInfo);
			}
			profileDto.setReloadInfoDtos(reloadList);
		}
		if (productUnit != null) {
			List<ProductProfileUnitDTO> unitList=Lists.newArrayList();
			for (int i = 0; i < productUnit.length; i++) {
				Long id = buIds[i];
				String bu = productUnit[i];
				ProductProfileUnitDTO punit = new ProductProfileUnitDTO();
				punit.setId(id);
				punit.setBusinessUnit(lookupService.getDetail(bu));
				unitList.add(punit);
			}
			profileDto.setProductUnit(unitList);
		}

		if (result.hasErrors()) {
			theResponse.setSuccess(false);
			theResponse.setResult(result.getAllErrors());
			return theResponse;
		}

		if (theResponse.isSuccess()) {
			profileDto.setStatus(ProductProfileStatus.valueOf(status));
			if (profileDto.getEffectiveMonths() == null)
				profileDto.setEffectiveMonths(12l);
			profileService.saveDto(profileDto);
		}

		return theResponse;
	}

	private void validateProfile(BindingResult result, ProductProfileDto profileDto) {
		if (StringUtils.isBlank(profileDto.getProductCode())) {
			result.reject(getMessage("propkey_msg_notempty", new Object[] { "profile_product_code" }));
		} else if (profileDto.getProductCode().length() != 12 && profileDto.getProductCode().length() != 13) {
			result.reject(getMessage("profile_invalid_profile_code_length", null));
		} else if (!profileService.findDuplicateProductCode(profileDto.getProductCode(), profileDto.getId())) {
			result.reject(getMessage("profile_invalid_profile_code_unique", null));
		}
		if (StringUtils.isBlank(profileDto.getProductDesc())) {
			result.reject(getMessage("propkey_msg_notempty", new Object[] { "profile_product_desc" }));
		}
		if (profileDto.getUnitCost() == null) {
			result.reject(getMessage("propkey_msg_notempty", new Object[] { "profile_unit_cost" }));
		}
		if (profileDto.getAllowReload()) {
			if (profileDto.getMaxAmount() == null || profileDto.getMaxAmount() < 1) {
				result.reject(getMessage("required.maxamount", null));
			} else {
				List<PrepaidReloadInfoDto> reloadInfoDtos = profileDto.getReloadInfoDtos();
				long totalAmount = 0;
				boolean hasError = false;
				for (PrepaidReloadInfoDto reloadDto : reloadInfoDtos) {
					if (reloadDto.getReloadAmount() == null) {
						result.reject(getMessage("propkey_msg_notempty", new Object[] { "reload.amount" }));
						hasError = true;

					} else if (profileDto.getMaxAmount() < reloadDto.getReloadAmount()) {
						result.reject(getMessage("exceeds.maxamount", null));
						hasError = true;
					} else {
						totalAmount += reloadDto.getReloadAmount();
					}

					if (reloadDto.getMonthEffective() == null || reloadDto.getMonthEffective() < 1) {
						result.reject(getMessage("effective.month.required", null));
						hasError = true;

					}
					if (hasError)
						break;
				}
			}
		}
	}

	private String getMessage(String inMsgCode, Object[] inArgMsgCodes) {
		if (inArgMsgCodes != null) {
			for (int i = 0; i < inArgMsgCodes.length; i++) {
				inArgMsgCodes[i] =
						messageSource.getMessage(inArgMsgCodes[i].toString().toLowerCase(), null, LocaleContextHolder.getLocale());
			}
		}
		return messageSource.getMessage(inMsgCode, inArgMsgCodes, "Info given is invalid.", LocaleContextHolder.getLocale());
	}

	@RequestMapping(produces = "text/html")
	public String show(Model uiModel) {
		populateSearch(uiModel);
		return "giftcard/productprofile/list";
	}

	private void populateSearch(Model uiModel) {
		uiModel.addAttribute("profileSearchCriteria", ProfileSearchCriteria.values());
		uiModel.addAttribute("profileStatus", ProductProfileStatus.values());
	}

	@RequestMapping(value = "/{id}", method = RequestMethod.DELETE, produces = "application/json; charset=utf-8")
	@ResponseBody
	public ControllerResponse delete(@PathVariable("id") Long id,
			Model uiModel) {
		ControllerResponse theResponse = new ControllerResponse();
		theResponse.setSuccess(true);
		profileService.deleteProfile(id);
		return theResponse;
	}

	@RequestMapping(value = "/approve/{status}", method = RequestMethod.POST, produces = "application/json; charset=utf-8")
	@ResponseBody
	public ControllerResponse ajaxApprove(
			@PathVariable("status") String status,
			@ModelAttribute(value = "profileForm") ProductProfileDto profileDto,
			BindingResult result, HttpServletRequest request) {

		ControllerResponse theResponse = new ControllerResponse();
		theResponse.setSuccess(true);

		if (result.hasErrors()) {
			theResponse.setSuccess(false);
			theResponse.setResult(result.getAllErrors());
			return theResponse;
		}

		if (theResponse.isSuccess()) {
			profileService.approveProfile(profileDto.getId(), ProductProfileStatus.valueOf(status));
		}

		return theResponse;
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	@RequestMapping(value = "/list/{filter}", produces = "application/json; charset=utf-8", method = RequestMethod.GET)
	public @ResponseBody List<ProductProfileDto> listCustomers(@PathVariable(value = "filter") String filter) {
		ProductProfileSearchDto searchDto = new ProductProfileSearchDto();
		searchDto.setProdDescLike(filter);
		searchDto.setStatusCode(ProductProfileStatus.APPROVED.toString());
		ResultList<ProductProfileDto> dtos = profileService.list(searchDto);
		return Lists.newArrayList(CollectionUtils.isNotEmpty(dtos.getResults()) ? dtos.getResults() : new ArrayList());
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	@RequestMapping(value = "/list/{isEgc}/{filter}", produces = "application/json; charset=utf-8", method = RequestMethod.GET)
	public @ResponseBody List<ProductProfileDto> listCustomers(
			@PathVariable(value = "filter") String filter,
			@PathVariable(value = "isEgc") boolean isEgc) {
		ProductProfileSearchDto searchDto = new ProductProfileSearchDto();
		searchDto.setProdDescLike(filter);
		searchDto.setIsEgc(isEgc);
		searchDto.setStatusCode(ProductProfileStatus.APPROVED.toString());
		ResultList<ProductProfileDto> dtos = profileService.list(searchDto);
		return Lists.newArrayList(CollectionUtils.isNotEmpty(dtos.getResults()) ? dtos.getResults() : new ArrayList());
	}

	@RequestMapping(value = "/activate/{id}/{activate}", produces = "application/json; charset=utf-8")
	public @ResponseBody ControllerResponse activate(
			@PathVariable("id") long id,
			@PathVariable("activate") boolean activate) {

		ControllerResponse resp = new ControllerResponse();
		profileService.activate(id, activate);
		resp.setSuccess(true);
		return resp;
	}

}
