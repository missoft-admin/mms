<%@ include file="/WEB-INF/views/common/taglibs.jsp" %>
<style>
  #crmProxyInfoDialog .list tr.selected > td {
    background-color: #ffffd6;
  }

  #crmProxyInfoDialog .list a.clickable {
    color: #000000 !important;
  }

  #logList {
    overflow-x: scroll;
    width: 100%;
    white-space: nowrap;
  }
  
  .well .form-cta {
    padding-left: 180px;
    text-align: left;
  }
  
</style>
<div class="page-header page-header2"><h1><spring:message code="admin.log.title"/></h1></div>
<div id="logSearchForm">
  <div class="messagesDiv">

  </div>
  <div class="form-horizontal row-fluid well">
    <div class="span6">
      <div class="control-group">
        <label class="control-label"><span class="required">*</span><spring:message code="admin.log.search.string"/></label>

        <div class="controls">
          <input type="text" name="filter" class="">
        </div>
      </div>
      <div class="control-group">
        <label class="control-label"><span class="required">*</span><spring:message code="admin.log.search.date"/></label>

        <div class="controls">
          <input type="text" name="date" class="searchDate" readonly>
        </div>
      </div>
    </div>
    <div class="span6">
      <div class="control-group">
        <label class="control-label"><span class="required">*</span><spring:message code="admin.log.store"/></label>

        <div class="controls">
          <input type="text" name="crmProxyInfoName" class="" disabled style="width: 55%">
          <input type="button" id="showSearchCrmProxyInfo" class="btn btn-primary ml10" value="<spring:message code="label_search"/>" />
          <input type="hidden" name="crmInStoreId">
        </div>

      </div>
    </div>
    <div class="form-cta">
      <button id="searchLogs" type="button" class="btn btn-primary" style="width: 120px;"><spring:message code="search"/></button>
    </div>
  </div>
  <div id="logList"></div>
</div>

<!-- Search Store Dialog -->
<div id="crmProxyInfoDialog" class="modal hide nofly modal-dialog" style="display: none">
  <div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
    <h4>Search <spring:message code="crm.instore.details"/></h4>
    <div class="clearfix"></div>
  </div>
  <div class="modal-body">
    <div class="messagesDiv">

    </div>
    <div class="searchElements">
      <div class="searchForm form-horizontal row-fluid well">
        <div class="control-group mb0">
          <label class="control-label"><spring:message code="label_search"/></label>

          <div class="controls">
            <input type="text" name="name" class="">
            <input type="button"  class="btn btn-primary ml10" id="searchCrmInfos" value="<spring:message code="label_search"/>" />
          </div>
        </div>
      </div>
      <div class="list" style="margin-top: 20px;">

      </div>
    </div>
    <br style="clear: both;"/>

    <div id="crmProxyAddForm" class="form-horizontal row-fluid addElements" style="display: none;">
      <input type="hidden" name="id">

      <div class="span6">
        <div class="control-group">
          <label class="control-label"><span class="required">*</span><spring:message code="crm.instore.code"/></label>

          <div class="controls">
            <input type="text" name="id" class="">
            <input type="hidden" name="prevId">
          </div>
        </div>
        <div class="control-group">
          <label class="control-label"><span class="required">*</span><spring:message code="crm.instore.storename"/></label>

          <div class="controls">
            <input type="text" name="name" class="">
          </div>
        </div>
        <div class="control-group">
          <label class="control-label"><span class="required">*</span><spring:message code="crm.instore.ip"/></label>

          <div class="controls">
            <input type="text" name="ip" class="">
          </div>
        </div>
      </div>
      <div class="span6">
        <div class="control-group">
          <label class="control-label"><span class="required">*</span><spring:message code="crm.instore.port"/></label>

          <div class="controls">
            <input type="text" name="tomcatPort" class="" value="9090">
          </div>
        </div>
        <div class="control-group">
          <label class="control-label"><span class="required">*</span><spring:message code="crm.instore.crmproxy.prefix"/></label>

          <div class="controls">
            <input type="text" name="crmProxyRestPrefix" class="" value="/crm-proxy/api">
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="modal-footer container-btn">
    <div class="searchElements">
      <button type="button" class="btn btn-primary" id="selectCrmInfo" disabled><spring:message code="hdr_select"/></button>
      <button type="button" class="btn btn-primary" id="editCrmInfo" disabled><spring:message code="edit"/></button>
      <button type="button" class="btn btn-primary" id="addCrmInfo"><spring:message code="add"/></button>
      <button type="button" class="btn" data-dismiss="modal"><spring:message code="cancel"/></button>
    </div>
    <div class="addElements" style="display: none;">
      <button type="button" class="btn btn-primary" id="saveAndSelectCrmInfo" style="width: 150px;"><spring:message
              code="label_save_select"/></button>
      <button type="button" class="btn btn-primary" id="saveCrmInfo"><spring:message code="label_save"/></button>
      <button type="button" class="btn" id="cancelAddCrmInfo"><spring:message code="label_cancel"/></button>
    </div>
    <div class="clearfix"></div>
  </div>
</div>
<script src="<spring:url value="/js/bootstrap/bootstrap-datepicker.js"/>"></script>
<script src="<spring:url value="/js/jquery/jquery.dataTables.js" />"></script>
<script src="<spring:url value="/js/jquery/jquery.dataTables.linkLengthMenu.js" />"></script>
<script src="<spring:url value="/js/jquery/jquery.dataTables.customPaging.js" />"></script>
<script src="<spring:url value="/js/jquery/ajax-datatable.js" />"></script>
<script src="<spring:url value="/js/common/form2js.js" />"></script>
<script src="<spring:url value="/js/common/json2.js" />"></script>
<script src="<spring:url value="/js/jquery/jquery.toObject.js" />"></script>
<link href="<spring:url value="/css/bootstrap/datepicker.css" />" rel="stylesheet"/>
<link href="<spring:url value="/css/dataTables.css" />" rel="stylesheet">
<script>
$( document ).ready( function () {
  $( '.searchDate' ).datepicker( {
    format : 'dd-mm-yyyy',
    endDate : '+1d',
    autoclose : true
  } );

  var $crmProxyInfoDialog = $( '#crmProxyInfoDialog' ).modal( { show : false } );
  var $crmProxyInfos = $( '.list', $crmProxyInfoDialog );
  var rowIdPrefix = 'crmproxyinfo-';
  var clickableCell = function ( data, type, row ) {
    return '<a class="clickable" href="#" onclick="return false;"><div>' + data + '</div></a>';
  };
  $crmProxyInfos.ajaxDataTable( {
    'autoload' : false,
    'ajaxSource' : '<spring:url value="/crmproxyinfo/search" />',
    'fnRowCallback' : function ( nRow, aData, iDisplayIndex ) {
      nRow.id = rowIdPrefix + aData.id;
      return nRow;
    },
    'columnHeaders' : [
      '<spring:message code="crm.instore.storename" />',
      '<spring:message code="crm.instore.ip" />',
      '<spring:message code="crm.instore.port" />',
      '<spring:message code="crm.instore.crmproxy.prefix" />'
    ],
    'modelFields' : [
      {name : 'name', sortable : true, fieldCellRenderer : clickableCell},
      {name : 'ip', sortable : false, fieldCellRenderer : clickableCell},
      {name : 'tomcatPort', sortable : false, fieldCellRenderer : clickableCell},
      {name : 'crmProxyRestPrefix', sortable : false, fieldCellRenderer : clickableCell}
    ]
  } );

  var selectCrmInfo = function ( crmInfo ) {
    $( 'input[name="crmProxyInfoName"]', $logSearchForm ).val( crmInfo.name );
    $( 'input[name="crmInStoreId"]', $logSearchForm ).val( crmInfo.id );
    $crmProxyInfoDialog.modal( 'toggle' );
  }

  var $selectCrmInfo = $( '#selectCrmInfo' ).click( function () {
    var $tr = $( 'tr.selected', $crmProxyInfos );
    var tds = $( 'td a > div', $tr );
    var id = $tr.attr( 'id' ).substring( rowIdPrefix.length );
    var name = tds[0].innerHTML;
    var crmInfo = new Object();
    crmInfo.id = id;
    crmInfo.name = name;
    selectCrmInfo( crmInfo );
  } );

  var $editCrmInfo = $( '#editCrmInfo' ).click( function () {
    var $tr = $( 'tr.selected', $crmProxyInfos );
    var id = $tr.attr( 'id' ).substring( rowIdPrefix.length );
    var tds = $( 'td a > div', $tr );
    var name = tds[0].innerHTML;
    var ip = tds[1].innerHTML;
    var port = tds[2].innerHTML;
    var wsPfx = tds[3].innerHTML;

    $( 'input[name="id"]', $crmProxyInfoDialog ).val( id );
    $( 'input[name="name"]', $crmProxyInfoDialog ).val( name );
    $( 'input[name="ip"]', $crmProxyInfoDialog ).val( ip );
    $( 'input[name="tomcatPort"]', $crmProxyInfoDialog ).val( port );
    $( 'input[name="crmProxyRestPrefix"]', $crmProxyInfoDialog ).val( wsPfx );

    $( '.searchElements', $crmProxyInfoDialog ).hide();
    $( '.addElements', $crmProxyInfoDialog ).show();
  } );

  $( $crmProxyInfos ).on( 'click', 'a.clickable', function () {
    $selectCrmInfo.prop( 'disabled', false );
    $editCrmInfo.prop( 'disabled', false );
    $( 'tr', $crmProxyInfos ).removeClass( 'selected' );
    $( this ).parents( 'tr' ).addClass( 'selected' );
  } );

  var $searchCrmProxyInfoBtn = $( '#searchCrmInfos' ).click( function () {
    var formDataObject = $( '.searchForm', $crmProxyInfoDialog ).toObject( {mode : 'first', skipEmpty : false} );
    var $btnSearch = $( this );
    $btnSearch.prop( 'disabled', true );
    $crmProxyInfos.ajaxDataTable( 'search', formDataObject, function () {
      $btnSearch.prop( 'disabled', false );
    } );
  } );

  var $addCrmInfo = $( '#addCrmInfo' );

  $( '#showSearchCrmProxyInfo' ).click( function () {
    $( '.searchElements', $crmProxyInfoDialog ).show();
    $( '.addElements', $crmProxyInfoDialog ).hide();
    $selectCrmInfo.prop( 'disabled', true );
    $editCrmInfo.prop( 'disabled', true );
    $crmProxyInfoDialog.modal( 'toggle' );
    $searchCrmProxyInfoBtn.click();
  } );

  $addCrmInfo.click( function () {
    $( 'input[name="id"]', $crmProxyInfoDialog ).val( '' );
    $( 'input[name="name"]', $crmProxyInfoDialog ).val( '' );
    $( 'input[name="ip"]', $crmProxyInfoDialog ).val( '' );
    $( 'input[name="tomcatPort"]', $crmProxyInfoDialog ).val( '9090' );
    $( 'input[name="crmProxyRestPrefix"]', $crmProxyInfoDialog ).val( '/crm-proxy/api' );

    $( '.searchElements', $crmProxyInfoDialog ).hide();
    $( '.addElements', $crmProxyInfoDialog ).show();
  } );

  var $crmProxyAddForm = $( '#crmProxyAddForm' );
  var $messagesDiv = $( '.messagesDiv', $crmProxyInfoDialog );

  var saveCrmInfo = function ( successCallback ) {
    $.ajax( {
      "contentType" : 'application/json',
      "type" : "POST",
      "url" : "<spring:url value="/crmproxyinfo/save" />",
      "data" : JSON.stringify( $crmProxyAddForm.toObject( {mode : 'first', skipEmpty : false} ) ),
      "success" : function ( data ) {
        var alertClass = 'alert-success';
        if ( !data.success ) {
          alertClass = 'alert-error';
          var htmlMessageString = '1. ' + data.result[0];
          for ( var i = 1; i < data.result.length; i++ ) {
            htmlMessageString += '<br />';
            htmlMessageString += (i + 1) + '. ' + data.result[i];
          }
          $messagesDiv.html( '<div class="alert ' + alertClass + '">' +
                  '<button type="button" class="close" data-dismiss="alert">&times;</button>' + htmlMessageString +
                  '</div>' );
        } else {
          $messagesDiv.html( '<div class="alert ' + alertClass + '">' +
                  '<button type="button" class="close" data-dismiss="alert">&times;</button>' + data.message +
                  '</div>' );
          $( '.searchElements', $crmProxyInfoDialog ).show();
          $( '.addElements', $crmProxyInfoDialog ).hide();
          $selectCrmInfo.prop( 'disabled', true );
          $editCrmInfo.prop( 'disabled', true );
          if ( successCallback ) {
            successCallback( data.result );
          } else {
            $searchCrmProxyInfoBtn.click();
          }
        }
      },
      "error" : function ( xhr, textStatus, error ) {
        var message = '';
        if ( textStatus === 'timeout' ) {
          message = 'The server took too long to send the data.';
        } else if ( error ) {
          message = error;
        } else {
          message = 'An error occurred on the server. Please try again.';
        }
        $messagesDiv.html( '<div class="alert alert-error">' +
                '<button type="button" class="close" data-dismiss="alert">&times;</button>' +
                message +
                '</div>' )
      }
    } );
  };

  $( '#saveCrmInfo' ).click( function () {
    saveCrmInfo();
  } );

  var $logSearchForm = $( '#logSearchForm' );
  $( '#saveAndSelectCrmInfo' ).click( function () {
    saveCrmInfo( selectCrmInfo );
  } );
  $( '#cancelAddCrmInfo' ).click( function () {
    $( '.searchElements', $crmProxyInfoDialog ).show();
    $( '.addElements', $crmProxyInfoDialog ).hide();

    $( '.alert', $messagesDiv ).alert( 'close' );
  } );

  $( '#searchLogs' ).click( function () {
    var $btnSearch = $( this );
    $btnSearch.prop( 'disabled', true );
    $btnSearch.html( "<spring:message code="searching" />" );
    $.ajax( {
      "contentType" : 'application/json',
      "type" : "POST",
      "url" : "<spring:url value="/log/search" />",
      "data" : JSON.stringify( $logSearchForm.toObject( {mode : 'first', skipEmpty : false} ) ),
      "success" : function ( data ) {
        if ( !data.success ) {
          var htmlMessageString = '1. ' + data.result[0];
          for ( var i = 1; i < data.result.length; i++ ) {
            htmlMessageString += '<br/>';
            htmlMessageString += (i + 1) + '. ' + data.result[i];
          }
          $( '.messagesDiv', $logSearchForm ).html( '<div class="alert alert-error">' +
                  '<button type="button" class="close" data-dismiss="alert">&times;</button>' + htmlMessageString +
                  '</div>' );
        } else {
          $( '.messagesDiv', $logSearchForm ).html( '' );
          var $logList = $( '#logList' ).html( '' );
          for ( var i = 0; i < data.result.length; i++ ) {
            $logList.append( '<div>' + data.result[i] + '</div>' );
          }
        }
        $btnSearch.prop( 'disabled', false );
        $btnSearch.html( "<spring:message code="search" />" );
      },
      "error" : function ( xhr, textStatus, error ) {
        var message = '';
        if ( textStatus === 'timeout' ) {
          message = 'The server took too long to send the data.';
        } else if ( error ) {
          message = error;
        } else {
          message = 'An error occurred on the server. Please try again.';
        }
        $( '.messagesDiv', $logSearchForm ).html( '<div class="alert alert-error">' +
                '<button type="button" class="close" data-dismiss="alert">&times;</button>' +
                message +
                '</div>' );
        $btnSearch.html( "<spring:message code="search" />" );
      }
    } );
  } );
} );
</script>