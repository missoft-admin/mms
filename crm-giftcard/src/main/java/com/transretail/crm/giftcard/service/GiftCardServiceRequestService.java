package com.transretail.crm.giftcard.service;

import com.transretail.crm.common.reporting.jasper.JRProcessor;
import com.transretail.crm.common.service.dto.request.PageSortDto;
import com.transretail.crm.common.service.dto.response.ResultList;
import com.transretail.crm.core.entity.lookup.Store;
import com.transretail.crm.giftcard.dto.GiftCardInfoDto;
import com.transretail.crm.giftcard.dto.GiftCardServiceRequestDto;
import com.transretail.crm.giftcard.dto.GiftCardTransactionDto;
import com.transretail.crm.giftcard.dto.ServiceRequestReportFilter;
import com.transretail.crm.giftcard.entity.GiftCardInventory;
import com.transretail.crm.giftcard.entity.GiftCardServiceRequest;
import com.transretail.crm.giftcard.entity.support.GiftCardInventoryStatus;
import java.util.List;

/**
 *
 * @author Monte Cillo Co (mco@exist.com)
 * @since 2.0
 */
public interface GiftCardServiceRequestService {

    GiftCardServiceRequest fileNewServiceRequest(GiftCardInventory giftCard, Store storeFiledIn, String details);

    List<GiftCardServiceRequest> findAllServiceRequestOf(GiftCardInventory giftcard);
    
    GiftCardInfoDto retrieveGiftCardInfoByCardNo(String cardNo);

    ResultList<GiftCardServiceRequestDto> findAllServiceRequestOf(String cardNo, PageSortDto pageSort);

    ResultList<GiftCardTransactionDto> findAllTransactionOf(String cardNo, PageSortDto pageSort);

    JRProcessor printServiceRequest(ServiceRequestReportFilter filter);
    
    GiftCardInventoryStatus enabledDisableGiftCard(String cardNo);
}
