package com.transretail.crm.generated.sources;

import javax.annotation.Generated;

/**
 * BCrmMember is a Querydsl bean type
 */
@Generated("com.mysema.query.codegen.BeanSerializer")
public class BCrmMember {

    private String acceptEmail;

    private String acceptMail;

    private String acceptPhone;

    private String acceptSms;

    private String accountEnabled;

    private String accountId;

    private Long address;

    private java.sql.Timestamp birthdate;

    private Long businessAddress;

    private String businessEmail;

    private String businessField;

    private String businessLicense;

    private String businessName;

    private String cardNo;

    private String cardType;

    private Integer carsOwned;

    private Integer childrenCount;

    private String closestStore;

    private String companyName;

    private String contact;

    private String createdBy;

    private java.sql.Timestamp createdDatetime;

    private String createdFrom;

    private String creditCardOwnership;

    private String customerGroup;

    private String customerSegregation;

    private String department;

    private Integer domesticHelpers;

    private String education;

    private String email;

    private String empType;

    private String familySize;

    private String faxNo;

    private String firstName;

    private String gender;

    private String householdIncome;

    private Long id;

    private String insuranceOwnership;

    private String ktp;

    private String lastName;

    private String lastUpdatedBy;

    private java.sql.Timestamp lastUpdatedDatetime;

    private String loyaltyCardNo;

    private String maritalStatus;

    private String memberType;

    private String middleName;

    private String nameNative;

    private String nationality;

    private String npwpAddress;

    private String npwpId;

    private String npwpName;

    private String occupation;

    private String parentAccountId;

    private String password;

    private String personalIncome;

    private String pin;

    private String placeOfBirth;

    private String position;

    private String postalCode;

    private String preferedLanguage;

    private String radius;

    private String registeredStore;

    private String religion;

    private String sms;

    private String status;

    private String storeName;

    private String timeToCall;

    private String title;

    private Double totalPoints;

    private Short transCode1;

    private Short transCode2;

    private Short transCode3;

    private Short transCode4;

    private Short transCodeLong1;

    private Short transCodeLong2;

    private Short transCodeShort;

    private String username;

    private String validationCode;

    private Integer version;

    private String zone;

    public String getAcceptEmail() {
        return acceptEmail;
    }

    public void setAcceptEmail(String acceptEmail) {
        this.acceptEmail = acceptEmail;
    }

    public String getAcceptMail() {
        return acceptMail;
    }

    public void setAcceptMail(String acceptMail) {
        this.acceptMail = acceptMail;
    }

    public String getAcceptPhone() {
        return acceptPhone;
    }

    public void setAcceptPhone(String acceptPhone) {
        this.acceptPhone = acceptPhone;
    }

    public String getAcceptSms() {
        return acceptSms;
    }

    public void setAcceptSms(String acceptSms) {
        this.acceptSms = acceptSms;
    }

    public String getAccountEnabled() {
        return accountEnabled;
    }

    public void setAccountEnabled(String accountEnabled) {
        this.accountEnabled = accountEnabled;
    }

    public String getAccountId() {
        return accountId;
    }

    public void setAccountId(String accountId) {
        this.accountId = accountId;
    }

    public Long getAddress() {
        return address;
    }

    public void setAddress(Long address) {
        this.address = address;
    }

    public java.sql.Timestamp getBirthdate() {
        return birthdate;
    }

    public void setBirthdate(java.sql.Timestamp birthdate) {
        this.birthdate = birthdate;
    }

    public Long getBusinessAddress() {
        return businessAddress;
    }

    public void setBusinessAddress(Long businessAddress) {
        this.businessAddress = businessAddress;
    }

    public String getBusinessEmail() {
        return businessEmail;
    }

    public void setBusinessEmail(String businessEmail) {
        this.businessEmail = businessEmail;
    }

    public String getBusinessField() {
        return businessField;
    }

    public void setBusinessField(String businessField) {
        this.businessField = businessField;
    }

    public String getBusinessLicense() {
        return businessLicense;
    }

    public void setBusinessLicense(String businessLicense) {
        this.businessLicense = businessLicense;
    }

    public String getBusinessName() {
        return businessName;
    }

    public void setBusinessName(String businessName) {
        this.businessName = businessName;
    }

    public String getCardNo() {
        return cardNo;
    }

    public void setCardNo(String cardNo) {
        this.cardNo = cardNo;
    }

    public String getCardType() {
        return cardType;
    }

    public void setCardType(String cardType) {
        this.cardType = cardType;
    }

    public Integer getCarsOwned() {
        return carsOwned;
    }

    public void setCarsOwned(Integer carsOwned) {
        this.carsOwned = carsOwned;
    }

    public Integer getChildrenCount() {
        return childrenCount;
    }

    public void setChildrenCount(Integer childrenCount) {
        this.childrenCount = childrenCount;
    }

    public String getClosestStore() {
        return closestStore;
    }

    public void setClosestStore(String closestStore) {
        this.closestStore = closestStore;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public String getContact() {
        return contact;
    }

    public void setContact(String contact) {
        this.contact = contact;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public java.sql.Timestamp getCreatedDatetime() {
        return createdDatetime;
    }

    public void setCreatedDatetime(java.sql.Timestamp createdDatetime) {
        this.createdDatetime = createdDatetime;
    }

    public String getCreatedFrom() {
        return createdFrom;
    }

    public void setCreatedFrom(String createdFrom) {
        this.createdFrom = createdFrom;
    }

    public String getCreditCardOwnership() {
        return creditCardOwnership;
    }

    public void setCreditCardOwnership(String creditCardOwnership) {
        this.creditCardOwnership = creditCardOwnership;
    }

    public String getCustomerGroup() {
        return customerGroup;
    }

    public void setCustomerGroup(String customerGroup) {
        this.customerGroup = customerGroup;
    }

    public String getCustomerSegregation() {
        return customerSegregation;
    }

    public void setCustomerSegregation(String customerSegregation) {
        this.customerSegregation = customerSegregation;
    }

    public String getDepartment() {
        return department;
    }

    public void setDepartment(String department) {
        this.department = department;
    }

    public Integer getDomesticHelpers() {
        return domesticHelpers;
    }

    public void setDomesticHelpers(Integer domesticHelpers) {
        this.domesticHelpers = domesticHelpers;
    }

    public String getEducation() {
        return education;
    }

    public void setEducation(String education) {
        this.education = education;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getEmpType() {
        return empType;
    }

    public void setEmpType(String empType) {
        this.empType = empType;
    }

    public String getFamilySize() {
        return familySize;
    }

    public void setFamilySize(String familySize) {
        this.familySize = familySize;
    }

    public String getFaxNo() {
        return faxNo;
    }

    public void setFaxNo(String faxNo) {
        this.faxNo = faxNo;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getHouseholdIncome() {
        return householdIncome;
    }

    public void setHouseholdIncome(String householdIncome) {
        this.householdIncome = householdIncome;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getInsuranceOwnership() {
        return insuranceOwnership;
    }

    public void setInsuranceOwnership(String insuranceOwnership) {
        this.insuranceOwnership = insuranceOwnership;
    }

    public String getKtp() {
        return ktp;
    }

    public void setKtp(String ktp) {
        this.ktp = ktp;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getLastUpdatedBy() {
        return lastUpdatedBy;
    }

    public void setLastUpdatedBy(String lastUpdatedBy) {
        this.lastUpdatedBy = lastUpdatedBy;
    }

    public java.sql.Timestamp getLastUpdatedDatetime() {
        return lastUpdatedDatetime;
    }

    public void setLastUpdatedDatetime(java.sql.Timestamp lastUpdatedDatetime) {
        this.lastUpdatedDatetime = lastUpdatedDatetime;
    }

    public String getLoyaltyCardNo() {
        return loyaltyCardNo;
    }

    public void setLoyaltyCardNo(String loyaltyCardNo) {
        this.loyaltyCardNo = loyaltyCardNo;
    }

    public String getMaritalStatus() {
        return maritalStatus;
    }

    public void setMaritalStatus(String maritalStatus) {
        this.maritalStatus = maritalStatus;
    }

    public String getMemberType() {
        return memberType;
    }

    public void setMemberType(String memberType) {
        this.memberType = memberType;
    }

    public String getMiddleName() {
        return middleName;
    }

    public void setMiddleName(String middleName) {
        this.middleName = middleName;
    }

    public String getNameNative() {
        return nameNative;
    }

    public void setNameNative(String nameNative) {
        this.nameNative = nameNative;
    }

    public String getNationality() {
        return nationality;
    }

    public void setNationality(String nationality) {
        this.nationality = nationality;
    }

    public String getNpwpAddress() {
        return npwpAddress;
    }

    public void setNpwpAddress(String npwpAddress) {
        this.npwpAddress = npwpAddress;
    }

    public String getNpwpId() {
        return npwpId;
    }

    public void setNpwpId(String npwpId) {
        this.npwpId = npwpId;
    }

    public String getNpwpName() {
        return npwpName;
    }

    public void setNpwpName(String npwpName) {
        this.npwpName = npwpName;
    }

    public String getOccupation() {
        return occupation;
    }

    public void setOccupation(String occupation) {
        this.occupation = occupation;
    }

    public String getParentAccountId() {
        return parentAccountId;
    }

    public void setParentAccountId(String parentAccountId) {
        this.parentAccountId = parentAccountId;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getPersonalIncome() {
        return personalIncome;
    }

    public void setPersonalIncome(String personalIncome) {
        this.personalIncome = personalIncome;
    }

    public String getPin() {
        return pin;
    }

    public void setPin(String pin) {
        this.pin = pin;
    }

    public String getPlaceOfBirth() {
        return placeOfBirth;
    }

    public void setPlaceOfBirth(String placeOfBirth) {
        this.placeOfBirth = placeOfBirth;
    }

    public String getPosition() {
        return position;
    }

    public void setPosition(String position) {
        this.position = position;
    }

    public String getPostalCode() {
        return postalCode;
    }

    public void setPostalCode(String postalCode) {
        this.postalCode = postalCode;
    }

    public String getPreferedLanguage() {
        return preferedLanguage;
    }

    public void setPreferedLanguage(String preferedLanguage) {
        this.preferedLanguage = preferedLanguage;
    }

    public String getRadius() {
        return radius;
    }

    public void setRadius(String radius) {
        this.radius = radius;
    }

    public String getRegisteredStore() {
        return registeredStore;
    }

    public void setRegisteredStore(String registeredStore) {
        this.registeredStore = registeredStore;
    }

    public String getReligion() {
        return religion;
    }

    public void setReligion(String religion) {
        this.religion = religion;
    }

    public String getSms() {
        return sms;
    }

    public void setSms(String sms) {
        this.sms = sms;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getStoreName() {
        return storeName;
    }

    public void setStoreName(String storeName) {
        this.storeName = storeName;
    }

    public String getTimeToCall() {
        return timeToCall;
    }

    public void setTimeToCall(String timeToCall) {
        this.timeToCall = timeToCall;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Double getTotalPoints() {
        return totalPoints;
    }

    public void setTotalPoints(Double totalPoints) {
        this.totalPoints = totalPoints;
    }

    public Short getTransCode1() {
        return transCode1;
    }

    public void setTransCode1(Short transCode1) {
        this.transCode1 = transCode1;
    }

    public Short getTransCode2() {
        return transCode2;
    }

    public void setTransCode2(Short transCode2) {
        this.transCode2 = transCode2;
    }

    public Short getTransCode3() {
        return transCode3;
    }

    public void setTransCode3(Short transCode3) {
        this.transCode3 = transCode3;
    }

    public Short getTransCode4() {
        return transCode4;
    }

    public void setTransCode4(Short transCode4) {
        this.transCode4 = transCode4;
    }

    public Short getTransCodeLong1() {
        return transCodeLong1;
    }

    public void setTransCodeLong1(Short transCodeLong1) {
        this.transCodeLong1 = transCodeLong1;
    }

    public Short getTransCodeLong2() {
        return transCodeLong2;
    }

    public void setTransCodeLong2(Short transCodeLong2) {
        this.transCodeLong2 = transCodeLong2;
    }

    public Short getTransCodeShort() {
        return transCodeShort;
    }

    public void setTransCodeShort(Short transCodeShort) {
        this.transCodeShort = transCodeShort;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getValidationCode() {
        return validationCode;
    }

    public void setValidationCode(String validationCode) {
        this.validationCode = validationCode;
    }

    public Integer getVersion() {
        return version;
    }

    public void setVersion(Integer version) {
        this.version = version;
    }

    public String getZone() {
        return zone;
    }

    public void setZone(String zone) {
        this.zone = zone;
    }

}

