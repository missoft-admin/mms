package com.transretail.crm.giftcard.dto;

import java.math.BigDecimal;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.joda.time.LocalDate;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.format.annotation.NumberFormat;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.google.common.collect.Lists;
import com.transretail.crm.common.web.converter.CurrencySerializer;
import com.transretail.crm.common.web.converter.LocalDateSerializer;
import com.transretail.crm.giftcard.entity.SalesOrderDeptAlloc.DeptAllocStatus;
import com.transretail.crm.giftcard.entity.support.SalesOrderStatus;

public class SalesOrderMultDeptAllocsDto {
	
	private Long orderId;
	private String orderNo;
	private String customerName;
	private SalesOrderStatus orderStatus;
	@DateTimeFormat(pattern = "dd-MM-yyyy")
	@JsonSerialize(using = LocalDateSerializer.class)
	private LocalDate orderDate;
	private BigDecimal salesOrderAmt;
	private DeptAllocStatus status;
	private List<SalesOrderDeptAllocDto> deptAllocs = Lists.newArrayList(new SalesOrderDeptAllocDto());
	public Long getOrderId() {
		return orderId;
	}
	public void setOrderId(Long orderId) {
		this.orderId = orderId;
	}

	public String getOrderNo() {
		return orderNo;
	}
	public void setOrderNo(String orderNo) {
		this.orderNo = orderNo;
	}
	public String getCustomerName() {
		return customerName;
	}
	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}
	public LocalDate getOrderDate() {
		return orderDate;
	}
	public void setOrderDate(LocalDate orderDate) {
		this.orderDate = orderDate;
	}
	public BigDecimal getSalesOrderAmt() {
		return salesOrderAmt;
	}

	@JsonInclude(Include.ALWAYS)
    @JsonSerialize(using = CurrencySerializer.class)
	@NumberFormat(style = NumberFormat.Style.NUMBER, pattern = "#,####,###.##")
	public BigDecimal getSalesOrderAmtFmt() {
		return salesOrderAmt;
	}
	public void setSalesOrderAmt(BigDecimal salesOrderAmt) {
		this.salesOrderAmt = salesOrderAmt;
	}
	public List<SalesOrderDeptAllocDto> getDeptAllocs() {
		return deptAllocs;
	}
	public void setDeptAllocs(List<SalesOrderDeptAllocDto> deptAllocs) {
		if(CollectionUtils.isNotEmpty(deptAllocs))
			this.deptAllocs = deptAllocs;
	}
	public SalesOrderStatus getOrderStatus() {
		return orderStatus;
	}
	public void setOrderStatus(SalesOrderStatus orderStatus) {
		this.orderStatus = orderStatus;
	}
	public DeptAllocStatus getStatus() {
		return status;
	}
	public void setStatus(DeptAllocStatus status) {
		this.status = status;
	}
	
	

}
