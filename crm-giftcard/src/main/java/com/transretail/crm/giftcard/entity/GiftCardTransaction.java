package com.transretail.crm.giftcard.entity;

import com.google.common.collect.Lists;
import com.google.common.collect.Sets;
import com.transretail.crm.core.entity.PeopleSoftStore;
import com.transretail.crm.core.entity.support.CustomAuditableEntity;
import com.transretail.crm.giftcard.entity.support.GiftCardSaleTransaction;
import com.transretail.crm.giftcard.entity.support.GiftCardSalesType;
import java.io.Serializable;
import java.util.List;
import java.util.Map;
import java.util.Set;
import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.groups.Default;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.hibernate.annotations.Type;
import org.hibernate.validator.constraints.NotEmpty;
import org.joda.time.LocalDateTime;

/**
 *
 * @author Monte Cillo Co (mco@exist.com)
 */
@Entity
@Table(name = "CRM_GC_TRANSACTION", uniqueConstraints = @UniqueConstraint(columnNames = {"TXN_NO", "TXN_TYPE"}))
public class GiftCardTransaction extends CustomAuditableEntity<Long> implements Serializable {

    interface Webservice extends Default {

    }

    @NotEmpty
    @Column(name = "TXN_NO", nullable = false, length = 20)
    private String transactionNo;

    @Column(name = "TXN_DATE", nullable = false)
    @Type(type = "org.jadira.usertype.dateandtime.joda.PersistentLocalDateTime")
    private LocalDateTime transactionDate;

    @NotNull
    @Enumerated(EnumType.STRING)
    @Column(name = "TXN_TYPE", nullable = false)
    private GiftCardSaleTransaction transactionType;

    @Enumerated(EnumType.STRING)
    @Column(name = "SALES_TYPE")
    private GiftCardSalesType salesType;

    @OneToOne
    @JoinColumn(name = "REF_TXN_ID")
    private GiftCardTransaction refTransaction;

    @NotEmpty(groups = Webservice.class)
    @Column(name = "MERCHANT_ID")
    private String merchantId;

    @NotEmpty(groups = Webservice.class)
    @Column(name = "TERMINAL_ID", length = 12)
    private String terminalId;

    @NotEmpty(groups = Webservice.class)
    @Column(name = "CASHIER_ID", length = 12)
    private String cashierId;

    @OneToMany(mappedBy = "transaction", cascade = CascadeType.ALL, orphanRemoval = true)
    @OrderBy("id ASC")
    private Set<GiftCardTransactionItem> transactionItems;

    @OneToOne
    @JoinColumn(name = "SALES_ORDER_ID", updatable = false)
    private SalesOrder salesOrder;

    @Column(name = "SETTLEMENT")
    private Boolean settlement = Boolean.FALSE;

    @Column(name = "LOSE_TXN", nullable = false)
    private boolean loseTransaction;

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "PSOFT_STORE_MAPPING_ID")
    private PeopleSoftStore peoplesoftStoreMapping;

    @Column(name = "CLAIMED_BY_AF", nullable = false)
    private boolean claimedByAffiliates;

    @Column(name = "DISCOUNT")
    private double discountPercentage;

    public GiftCardTransaction merchantId(String merchantId) {
        this.merchantId = merchantId;
        return this;
    }

    public GiftCardTransaction terminalId(String terminalId) {
        this.terminalId = terminalId;
        return this;
    }

    public GiftCardTransaction cashierId(String cashierId) {
        this.cashierId = cashierId;
        return this;
    }

    public GiftCardTransaction transactionNo(String transactionNo) {
        this.transactionNo = transactionNo;
        return this;
    }

    public GiftCardTransaction transactionType(GiftCardSaleTransaction transactionType) {
        this.transactionType = transactionType;
        return this;
    }

    public GiftCardTransaction salesType(GiftCardSalesType salesType) {
        this.salesType = salesType;
        return this;
    }

    public GiftCardTransaction transactionDate(LocalDateTime transactionDate) {
        this.transactionDate = transactionDate;
        return this;
    }

    public GiftCardTransaction refTransactionNo(GiftCardTransaction refTransaction) {
        this.refTransaction = refTransaction;
        return this;
    }

    public GiftCardTransaction businessUnit(PeopleSoftStore businessUnit) {
        this.peoplesoftStoreMapping = businessUnit;
        return this;
    }

    public GiftCardTransaction addItem(GiftCardTransactionItem item) {
        addTransactionItem(item);
        return this;
    }

    public GiftCardTransaction salesOrder(SalesOrder salesOrder) {
        this.salesOrder = salesOrder;
        return this;
    }

    public GiftCardTransaction discountPercentage(double discount) {
        this.discountPercentage = discount;
        return this;
    }

    public GiftCardTransaction isLoseTransaction(boolean loseTransaction) {
        this.loseTransaction = loseTransaction;
        return this;
    }

    public String getTransactionNo() {
        return transactionNo;
    }

    public void setTransactionNo(String transactionNo) {
        this.transactionNo = transactionNo;
    }

    public LocalDateTime getTransactionDate() {
        return transactionDate;
    }

    public void setTransactionDate(LocalDateTime transactionDate) {
        this.transactionDate = transactionDate;
    }

    public GiftCardSaleTransaction getTransactionType() {
        return transactionType;
    }

    public void setTransactionType(GiftCardSaleTransaction transactionType) {
        this.transactionType = transactionType;
    }

    public GiftCardTransaction getRefTransaction() {
        return refTransaction;
    }

    public void setRefTransaction(GiftCardTransaction refTransaction) {
        this.refTransaction = refTransaction;
    }

    public String getMerchantId() {
        return merchantId;
    }

    public void setMerchantId(String merchantId) {
        this.merchantId = merchantId;
    }

    public String getTerminalId() {
        return terminalId;
    }

    public void setTerminalId(String terminalId) {
        this.terminalId = terminalId;
    }

    public String getCashierId() {
        return cashierId;
    }

    public void setCashierId(String cashierId) {
        this.cashierId = cashierId;
    }

    public boolean isLoseTransaction() {
        return loseTransaction;
    }

    public void setLoseTransaction(boolean loseTransaction) {
        this.loseTransaction = loseTransaction;
    }

    public Set<GiftCardTransactionItem> getTransactionItems() {
        if (transactionItems == null) {
            transactionItems = Sets.newHashSet();
        }
        return transactionItems;
    }

    public void setTransactionItems(Set<GiftCardTransactionItem> transactionItems) {
        this.transactionItems = transactionItems;
    }

    public void addTransactionItem(GiftCardTransactionItem item) {
        if (transactionItems == null) {
            transactionItems = Sets.newHashSet();
        }
        item.setTransaction(this);
        transactionItems.add(item);
    }

    public SalesOrder getSalesOrder() {
        return salesOrder;
    }

    public Boolean getSettlement() {
        return settlement;
    }

    public void setSettlement(Boolean settlement) {
        this.settlement = settlement;
    }

    public PeopleSoftStore getPeoplesoftStoreMapping() {
        return peoplesoftStoreMapping;
    }

    public void setPeoplesoftStoreMapping(PeopleSoftStore peoplesoftStoreMapping) {
        this.peoplesoftStoreMapping = peoplesoftStoreMapping;
    }

    public boolean isClaimedByAffiliates() {
        return claimedByAffiliates;
    }

    public void setClaimedByAffiliates(boolean claimedByAffiliates) {
        this.claimedByAffiliates = claimedByAffiliates;
    }

    public double getDiscountPercentage() {
        return discountPercentage;
    }

    public void setDiscountPercentage(double discountPercentage) {
        this.discountPercentage = discountPercentage;
    }

    public GiftCardSalesType getSalesType() {
        return salesType;
    }

    public void setSalesType(GiftCardSalesType salesType) {
        this.salesType = salesType;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;

        if (o == null || getClass() != o.getClass()) return false;

        GiftCardTransaction that = (GiftCardTransaction) o;

        return new EqualsBuilder()
                .append(transactionNo, that.transactionNo)
                .append(transactionType, that.transactionType)
                .isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 37)
                .append(transactionNo)
                .append(transactionType)
                .toHashCode();
    }
}
