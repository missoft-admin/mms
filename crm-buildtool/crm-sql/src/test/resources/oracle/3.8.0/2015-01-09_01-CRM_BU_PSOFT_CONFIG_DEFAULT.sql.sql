-- ARI
insert into CRM_BU_PSOFT_CONFIG (ID,CREATED_BY,CREATED_DATETIME,BU,PSOFT_CONFIG_ID) 
  values (hibernate_sequence.nextval,'SYSTEM',SYSDATE,'ID050', 
      (select id from CRM_FOR_PEOPLESOFT_CONFIG cnf where cnf.txn_type='AFFILIATE_CLAIM'));
insert into CRM_BU_PSOFT_CONFIG (ID,CREATED_BY,CREATED_DATETIME,BU,PSOFT_CONFIG_ID) 
  values (hibernate_sequence.nextval,'SYSTEM',SYSDATE,'ID050', 
      (select id from CRM_FOR_PEOPLESOFT_CONFIG cnf where cnf.txn_type='AFFILIATE_REDEMPTION'));
-- TGI
insert into CRM_BU_PSOFT_CONFIG (ID,CREATED_BY,CREATED_DATETIME,BU,PSOFT_CONFIG_ID) 
  values (hibernate_sequence.nextval,'SYSTEM',SYSDATE,'ID110', 
      (select id from CRM_FOR_PEOPLESOFT_CONFIG cnf where cnf.txn_type='AFFILIATE_CLAIM'));
insert into CRM_BU_PSOFT_CONFIG (ID,CREATED_BY,CREATED_DATETIME,BU,PSOFT_CONFIG_ID) 
  values (hibernate_sequence.nextval,'SYSTEM',SYSDATE,'ID110', 
      (select id from CRM_FOR_PEOPLESOFT_CONFIG cnf where cnf.txn_type='AFFILIATE_REDEMPTION'));
