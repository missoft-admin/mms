--alter table CRM_GC_INVENTORY drop constraint FK_kxdawmmiocjfxhtam9cih1c4s;

alter table CRM_GC_INVENTORY drop constraint FK_ade12xow89h6uc5f1bvxm6o35;

drop table CRM_GC_INVENTORY if exists;

create table CRM_GC_INVENTORY (
        id bigint generated by default as identity,
        CREATED_BY varchar(255),
        CREATED_DATETIME timestamp,
        LAST_UPDATED_BY varchar(255),
        LAST_UPDATED_DATETIME timestamp,
        ACTIVATION_DATE date,
        ALLOCATE_TO varchar(255),
        BALANCE double,
        BARCODE varchar(20) not null,
        BATCH_NO integer not null,
        BATCH_REF_NO varchar(255),
        CURR_VALUE decimal(12,4),
        RECEIPT_NO varchar(255),
        EXPIRY_DATE date,
        FACE_VALUE decimal(12,4),
        LOCATION varchar(255),
        ORDER_DATE date not null,
        PREV_BALANCE double,
        PRODUCT_CODE varchar(100) not null,
        PRODUCT_NAME varchar(100) not null,
        RECEIVE_DATE date,
        REDEMPTION_DATE date,
        REFUNDABLE boolean,
        SEQ_NO integer not null,
        SERIES bigint not null,
        STATUS varchar(8) not null,
        ORDER_ID bigint not null,
        PROFILE_ID bigint not null,
        primary key (id)
    );