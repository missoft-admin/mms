package com.transretail.crm.encryptpin.service.impl;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.hibernate.FlushMode;
import org.hibernate.ScrollMode;
import org.hibernate.ScrollableResults;
import org.hibernate.Session;
import org.hibernate.engine.transaction.spi.TransactionContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.support.TransactionCallbackWithoutResult;
import org.springframework.transaction.support.TransactionTemplate;

import com.transretail.crm.common.util.SimpleEncrytor;
import com.transretail.crm.encryptpin.entity.MemberPinModel;
import com.transretail.crm.encryptpin.service.EncryptPinService;

/**
 * @author Allan G. Ramirez (agramirez@exist.com)
 */
@Service
public class EncryptPinServiceImpl implements EncryptPinService {
    private static final Logger _LOG = LoggerFactory.getLogger(EncryptPinServiceImpl.class);
    @Value("#{'${hibernate.jdbc.batch_size}'}")
    private int batchSize;

    @PersistenceContext
    private EntityManager entityManager;

    @Autowired
    private PlatformTransactionManager transactionManager;

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void encryptAllPins() throws Exception {
        final Session session = entityManager.unwrap(Session.class);
        new TransactionTemplate(transactionManager).execute(new TransactionCallbackWithoutResult() {
            @Override
            public void doInTransactionWithoutResult(TransactionStatus status) {
                String query =
                    "from MemberPinModel m WHERE m.pin IS NOT NULL AND (m.pinEncryped IS NULL OR m.pinEncryped = :pinEncrypted)";
                ScrollableResults scrollableResults = session.createQuery(query)
                    .setBoolean("pinEncrypted", false)
                    .setFlushMode(FlushMode.COMMIT)
                    .scroll(ScrollMode.FORWARD_ONLY);
                try {
                    int count = 0;
                    while (scrollableResults.next()) {
                        MemberPinModel member = (MemberPinModel) scrollableResults.get(0);
                        String rawPin = member.getPin();
                        String encryptedPin = SimpleEncrytor.getInstance().encrypt(rawPin);
                        member.setPin(encryptedPin);
                        member.setPinEncryped(true);
                        session.update(member);
                        _LOG.info("[PIN ENCRYPTOR] Raw PIN: {} - Encrypted PIN - {}", rawPin, encryptedPin);
                        count++;
                        if (count % batchSize == 0) {
                            ((TransactionContext) session).managedFlush();
                        }
                    }
                    ((TransactionContext) session).managedFlush();
                    _LOG.info("[PIN ENCRYPTOR] Total Encrypted PINS: {}.", count);
                } catch (SimpleEncrytor.EncryptDecryptException e) {
                    throw new RuntimeException(e);
                } finally {
                    scrollableResults.close();
                }
            }
        });
    }
}
