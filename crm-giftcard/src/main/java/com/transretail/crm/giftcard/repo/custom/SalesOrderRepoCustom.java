package com.transretail.crm.giftcard.repo.custom;

import com.transretail.crm.giftcard.entity.GiftCardInventory;
import com.transretail.crm.giftcard.entity.SalesOrder;

/**
 *
 * @author Monte Cillo Co (mco@exist.com)
 */
public interface SalesOrderRepoCustom {

    /**
     * Find the latest enclosing sales order of cardInventory
     *
     * @param cardInventory
     * @return the latest sales order where the cardInventory is.
     */
    SalesOrder findEnclosingSalesOrderOfGiftCardInventory(GiftCardInventory cardInventory);
}
