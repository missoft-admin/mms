package com.transretail.crm.core.entity;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.Immutable;

@Table(name = "POS_TX_ITEM")
@Entity
@Immutable
public class PosTxItem {
	
	@Id
	@Column(name = "ID")
    private String id;

	@Column(name = "PRODUCT_ID")
    private String productId;

    @ManyToOne(targetEntity = PosTransaction.class, cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    @JoinColumn(name = "POS_TXN_ID")
    private PosTransaction posTransaction;

    @Column(name = "QUANTITY")
    private Double quantity;

    @Column(name = "SALES_TYPE")
    private String salesType;

    @Column(name="DISCOUNT_AMOUNT")
    private Double discountAmt;

    @Column(name="PRICE_UNIT")
    private Double unitPrice;

    @Column(name="PRICE_SUBTOTAL")
    private Double totalPrice;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getProductId() {
		return productId;
	}

	public void setProductId(String productId) {
		this.productId = productId;
	}

	public PosTransaction getPosTransaction() {
		return posTransaction;
	}

	public void setPosTransaction(PosTransaction posTransaction) {
		this.posTransaction = posTransaction;
	}


	public Double getQuantity() {
		return quantity;
	}

	public void setQuantity(Double quantity) {
		this.quantity = quantity;
	}


	public String getSalesType() {
		return salesType;
	}

	public void setSalesType(String salesType) {
		this.salesType = salesType;
	}

	public Double getDiscountAmt() {
		return discountAmt;
	}

	public void setDiscountAmt(Double discountAmt) {
		this.discountAmt = discountAmt;
	}

	public Double getUnitPrice() {
		return unitPrice;
	}

	public void setUnitPrice(Double unitPrice) {
		this.unitPrice = unitPrice;
	}

	public Double getTotalPrice() {
		return totalPrice;
	}

	public void setTotalPrice(Double totalPrice) {
		this.totalPrice = totalPrice;
	}

    
    /*
     
   @Column
    private double priceUnit;
    
    @Column
    private String shortDesc;

    @Column
    private double priceSubtotal;

    private int orderIndex;

    @Type(type = "yes_no")
    private Boolean isTaxInclusive;
    
    private double memberDiscountAmount;
     
     @Column
    private double discountAmount;

    @Column
    private double priceSubtotalInclusive;
     
     
     * 
     */
    
    

}
