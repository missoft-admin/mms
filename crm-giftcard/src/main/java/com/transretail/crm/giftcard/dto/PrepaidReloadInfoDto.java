package com.transretail.crm.giftcard.dto;

/**
 * @author Allan G. Ramirez (agramirez@exist.com)
 */
public class PrepaidReloadInfoDto {
    private Long id;
    private Long reloadAmount;
    /**
     * Effectivity month, 1 as January and 12 as December
     */
    private Integer monthEffective;

    public PrepaidReloadInfoDto() {

    }

    public PrepaidReloadInfoDto(Long id, Long reloadAmount, Integer monthEffective) {
        this.id = id;
        this.reloadAmount = reloadAmount;
        this.monthEffective = monthEffective;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getReloadAmount() {
        return reloadAmount;
    }

    public void setReloadAmount(Long reloadAmount) {
        this.reloadAmount = reloadAmount;
    }

    public Integer getMonthEffective() {
        return monthEffective;
    }

    public void setMonthEffective(Integer monthEffective) {
        this.monthEffective = monthEffective;
    }
}
