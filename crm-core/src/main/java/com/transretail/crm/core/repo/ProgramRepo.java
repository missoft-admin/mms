package com.transretail.crm.core.repo;


import org.springframework.stereotype.Repository;

import com.transretail.crm.common.repo.CrmQueryDslPredicateExecutor;
import com.transretail.crm.core.entity.Program;
import com.transretail.crm.core.repo.custom.ProgramRepoCustom;


@Repository
public interface ProgramRepo extends CrmQueryDslPredicateExecutor<Program, Long>, ProgramRepoCustom {
}
