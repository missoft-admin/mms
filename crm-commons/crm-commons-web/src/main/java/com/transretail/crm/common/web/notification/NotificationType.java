package com.transretail.crm.common.web.notification;

public enum NotificationType {
	POINTS, PURCHASETXN, PROMOTION, REWARDS, MANUFACTURE_ORDER, LOYALTY_ALLOCATION, MONITOR_MEMBER, MONITOR_EMPLOYEE, PROMO_BANNER,
	LOYALTY_BURN, LOYALTY_CHANGE_ALLOCATION, REDEMPTION_PROMO, STOCK_REQUEST
}
