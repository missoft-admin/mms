package com.transretail.crm.giftcard.repo.custom.impl;

import com.google.common.collect.Lists;
import com.mysema.query.jpa.impl.JPAQuery;
import com.mysema.query.types.OrderSpecifier;
import com.transretail.crm.giftcard.entity.GiftCardTransactionItem;
import com.transretail.crm.giftcard.entity.QGiftCardTransactionItem;
import com.transretail.crm.giftcard.entity.support.GiftCardSaleTransaction;
import com.transretail.crm.giftcard.repo.custom.GiftCardTransactionItemRepoCustom;
import org.joda.time.LocalDateTime;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

/**
 * description here.
 *
 * @author Vincent Gerard Tan
 */
public class GiftCardTransactionItemRepoImpl implements GiftCardTransactionItemRepoCustom {

    @PersistenceContext
    private EntityManager em;

    @Override
    public List<GiftCardTransactionItem> findPrepaidTransactions(String cardNo, LocalDateTime dateFrom, LocalDateTime dateTo) {
        QGiftCardTransactionItem qItem = QGiftCardTransactionItem.giftCardTransactionItem;
        OrderSpecifier<LocalDateTime> orderSpecifier = qItem.transaction.transactionDate.desc();
        List<GiftCardSaleTransaction> saleTransactionList = Lists.newArrayList(
                GiftCardSaleTransaction.RELOAD,
                GiftCardSaleTransaction.ACTIVATION,
                GiftCardSaleTransaction.VOID_ACTIVATED,
                GiftCardSaleTransaction.REDEMPTION,
                GiftCardSaleTransaction.VOID_REDEMPTION,
                GiftCardSaleTransaction.VOID_RELOAD);

        JPAQuery query = new JPAQuery(em).from(qItem).where(qItem.giftCard.barcode.eq(cardNo).
                and(qItem.transaction.transactionDate.between(dateFrom, dateTo))
                .and(qItem.transaction.transactionType.in(saleTransactionList))).orderBy(orderSpecifier);
        return query.list(qItem);
    }
}
