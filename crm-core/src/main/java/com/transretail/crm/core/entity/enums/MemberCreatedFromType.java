package com.transretail.crm.core.entity.enums;


public enum MemberCreatedFromType {


    FROM_CRM, FROM_PORTAL, FROM_UPLOAD, FROM_STAMP
}
