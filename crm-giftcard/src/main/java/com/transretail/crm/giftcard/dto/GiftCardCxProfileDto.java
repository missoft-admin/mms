package com.transretail.crm.giftcard.dto;


import java.util.Collection;
import java.util.Date;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.joda.time.DateTime;

import com.google.common.collect.Lists;
import com.transretail.crm.core.entity.lookup.LookupDetail;
import com.transretail.crm.core.util.FormatterUtil;
import com.transretail.crm.giftcard.entity.GiftCardCustomerProfile;
import com.transretail.crm.giftcard.entity.support.CustomerProfileType;


public class GiftCardCxProfileDto {

	private Long id;
	private DateTime created;
	private String createUser;

	private String customerId;
    private String customerType;
	private String name;
	private String invoiceTitle;
	private String contactFirstName;
	private String contactLastName;
	private String gender;
	private String contactNo;
	private String companyPhoneNo;
	private String faxNo;
	private String email;
	private String mailAddress;
	private String shippingAddress;
    private String discountType;
    private String lastOrderNo;
    private Date lastOrderDate;

    private Long lastOrderDateTime;
    private String customerTypeDesc;
    private String discountTypeDesc;
    private String paymentStore;
	private String paymentStoreName;



    public GiftCardCxProfileDto() {}
    public GiftCardCxProfileDto( GiftCardCustomerProfile model ) {
    	this.id = model.getId();
    	this.created = model.getCreated();
    	this.createUser = model.getCreateUser();

    	this.customerId = model.getCustomerId();
    	this.customerType = null != model.getCustomerType()? model.getCustomerType().name() : null;
    	this.customerTypeDesc = null != model.getCustomerType()? model.getCustomerType().name() : null;
    	this.name = model.getName();
    	this.invoiceTitle = model.getInvoiceTitle();
    	this.contactFirstName = model.getContactFirstName();
    	this.contactLastName = model.getContactLastName();
    	this.gender = null != model.getGender()? model.getGender().getCode() : null;
    	this.contactNo = model.getContactNo();
    	this.companyPhoneNo = model.getCompanyPhoneNo();
    	this.faxNo = model.getFaxNo();
    	this.email = model.getEmail();
    	this.mailAddress = model.getMailAddress();
    	this.shippingAddress = model.getShippingAddress();
    	this.discountType = null != model.getDiscountType()? model.getDiscountType().getCode() : null;
    	this.discountTypeDesc = null != model.getDiscountType()? model.getDiscountType().getDescription() : null;
    	this.lastOrderNo = model.getLastOrderNo();
    	this.lastOrderDate = null != model.getLastOrderDate()? model.getLastOrderDate().toDate() : null;
    	this.lastOrderDateTime = null != model.getLastOrderDate()? model.getLastOrderDate().getMillis() : null;
    	this.paymentStore=model.getPaymentStore();
    }

    public GiftCardCustomerProfile toModel() {
    	return toUpdatedModel( null );
    }
    public GiftCardCustomerProfile toUpdatedModel( GiftCardCustomerProfile model ) {
		if ( null == model ) {
			model = new GiftCardCustomerProfile();
		}

		model.setCustomerType( StringUtils.isNotBlank( customerType )? CustomerProfileType.valueOf(customerType) : null );
		model.setName( name );
		model.setInvoiceTitle( invoiceTitle );
		model.setContactFirstName( contactFirstName );
		model.setContactLastName( contactLastName );
		model.setGender( StringUtils.isNotBlank( gender )? new LookupDetail( gender ) : null );
		model.setContactNo( contactNo );
		model.setCompanyPhoneNo( companyPhoneNo );
		model.setFaxNo( faxNo );
		model.setEmail( email );
		model.setMailAddress( mailAddress );
		model.setShippingAddress( shippingAddress );
		model.setDiscountType( StringUtils.isNotBlank( discountType )? new LookupDetail( discountType ) : null );
		model.setLastOrderNo( lastOrderNo );
		model.setLastOrderDate( null != lastOrderDateTime? new DateTime( lastOrderDateTime ) :
			null != lastOrderDate? new DateTime( lastOrderDate.getTime() ) : null );
		model.setPaymentStore(paymentStore);
		return model;
    }

    public static List<GiftCardCxProfileDto> toDtoList( Collection<GiftCardCustomerProfile> models ) {
    	List<GiftCardCxProfileDto> dtos = Lists.newArrayList();
    	if ( CollectionUtils.isEmpty( models ) ) {
    		return dtos;
    	}

    	for ( GiftCardCustomerProfile model : models ) {
    		dtos.add( new GiftCardCxProfileDto( model ) );
    	}
    	return dtos;
    }



	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public DateTime getCreated() {
		return created;
	}
	public void setCreated(DateTime created) {
		this.created = created;
	}
	public String getCreateUser() {
		return createUser;
	}
	public void setCreateUser(String createUser) {
		this.createUser = createUser;
	}
	public String getCustomerId() {
		return customerId;
	}
	public void setCustomerId(String customerId) {
		this.customerId = customerId;
	}
	public String getCustomerType() {
		return customerType;
	}
	public void setCustomerType(String customerType) {
		this.customerType = customerType;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getInvoiceTitle() {
		return invoiceTitle;
	}
	public void setInvoiceTitle(String invoiceTitle) {
		this.invoiceTitle = invoiceTitle;
	}
	public String getContactFirstName() {
		return contactFirstName;
	}
	public void setContactFirstName(String contactFirstName) {
		this.contactFirstName = contactFirstName;
	}
	public String getContactLastName() {
		return contactLastName;
	}
	public void setContactLastName(String contactLastName) {
		this.contactLastName = contactLastName;
	}
	public String getGender() {
		return gender;
	}
	public void setGender(String gender) {
		this.gender = gender;
	}
	public String getContactNo() {
		return contactNo;
	}
	public void setContactNo(String contactNo) {
		this.contactNo = contactNo;
	}
	public String getCompanyPhoneNo() {
		return companyPhoneNo;
	}
	public void setCompanyPhoneNo(String companyPhoneNo) {
		this.companyPhoneNo = companyPhoneNo;
	}
	public String getFaxNo() {
		return faxNo;
	}
	public void setFaxNo(String faxNo) {
		this.faxNo = faxNo;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getMailAddress() {
		return mailAddress;
	}
	public void setMailAddress(String mailAddress) {
		this.mailAddress = mailAddress;
	}
	public String getShippingAddress() {
		return shippingAddress;
	}
	public void setShippingAddress(String shippingAddress) {
		this.shippingAddress = shippingAddress;
	}
	public String getDiscountType() {
		return discountType;
	}
	public void setDiscountType(String discountType) {
		this.discountType = discountType;
	}
	public String getLastOrderNo() {
		return lastOrderNo;
	}
	public void setLastOrderNo(String lastOrderNo) {
		this.lastOrderNo = lastOrderNo;
	}
	public Date getLastOrderDate() {
		return lastOrderDate;
	}
	public void setLastOrderDate(Date lastOrderDate) {
		this.lastOrderDate = lastOrderDate;
	}
	public Long getLastOrderDateTime() {
		return lastOrderDateTime;
	}
	public void setLastOrderDateTime(Long lastOrderDateTime) {
		this.lastOrderDateTime = lastOrderDateTime;
	}
	public String getCustomerTypeDesc() {
		return customerTypeDesc;
	}
	public void setCustomerTypeDesc(String customerTypeDesc) {
		this.customerTypeDesc = customerTypeDesc;
	}
	public String getDiscountTypeDesc() {
		return discountTypeDesc;
	}
	public void setDiscountTypeDesc(String discountTypeDesc) {
		this.discountTypeDesc = discountTypeDesc;
	}
    public String getContactFullName() {
        return FormatterUtil.INSTANCE.formatName(contactLastName, contactFirstName);

    }
	public String getPaymentStore() {
		return paymentStore;
	}
	public void setPaymentStore(String paymentStore) {
		this.paymentStore = paymentStore;
	}
	public String getPaymentStoreName() {
		return paymentStoreName;
	}
	public void setPaymentStoreName(String paymentStoreName) {
		this.paymentStoreName = paymentStoreName;
	}

}