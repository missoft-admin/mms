package com.transretail.crm.web.controller;

import static org.junit.Assert.*;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Random;

import javax.servlet.http.HttpServletResponse;

import org.apache.commons.io.IOUtils;
import org.junit.Ignore;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.web.WebAppConfiguration;

import com.transretail.crm.common.reporting.jasper.JRProcessor;
import com.transretail.crm.common.reporting.service.ReportService;
import com.transretail.crm.web.rest.BaseMockController;
import com.transretail.crm.core.entity.MemberModel;
import com.transretail.crm.core.entity.PointsTxnModel;
import com.transretail.crm.core.entity.enums.PointTxnType;
import com.transretail.crm.core.service.MemberService;
import com.transretail.crm.core.service.PointsTxnManagerService;

//@Ignore
//@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = "classpath*:/META-INF/spring/applicationContext*.xml")
@WebAppConfiguration
public class ReportsControllerTest extends BaseMockController {
	@Mock
	private MemberService memberService;
	
	@Mock
	private PointsTxnManagerService pointsManagerService;
	
	@Mock
	private ReportService reportService;
	
	@Mock
	private HttpServletResponse response;
	
	@InjectMocks
	private ReportsController reportsController = new ReportsController();
	
	private MemberModel member;
	private List<PointsTxnModel> pointsList;
	private String accountId = "00001";
	
	public ReportsControllerTest() {
		member = new MemberModel();
		member.setAccountId(accountId);
		member.setFirstName("John");
		member.setLastName("Doe");
		
		pointsList = new ArrayList<PointsTxnModel>();
		double totalPoints = 0;
		Random rnd = new Random();
		for(int i = 1; i < 100; i++) {
			PointsTxnModel points = new PointsTxnModel();
			points.setStoreCode("Test Store");
			points.setTransactionDateTime(new Date());
			points.setTransactionType(PointTxnType.EARN);
			points.setTransactionNo(i + "");
			double amt = 100.0 + rnd.nextInt(9900);
			double pts =  (amt / 100);
			points.setTransactionPoints(pts);
			points.setTransactionAmount(amt);
			points.setMemberModel(member);
			
			totalPoints += pts;
			pointsList.add(points);
		}
		
		member.setTotalPoints(totalPoints);
		member.setPointsDetails(pointsList);
	}
	
	@Ignore
	@Test
	public void testExportToPdf() throws Exception {
		File file = new File("points_audit_report_00001.pdf");
		try {
//			ReportService reportService = new ReportServiceImpl();
//			Whitebox.setInternalState(reportsController, "reportService", reportService);
			Mockito.doNothing().when(reportService).exportPdfReport(Mockito.any(JRProcessor.class), Mockito.any(File.class));
			Mockito.when(memberService.findByAccountId(Mockito.anyString())).thenReturn(member);
			Mockito.when(pointsManagerService.retrievePoints(Mockito.any(MemberModel.class))).thenReturn(pointsList);
//			reportsController.exportToPdf(accountId);
			
			Mockito.verify(reportService).exportPdfReport(Mockito.any(JRProcessor.class), Mockito.any(File.class));
			Mockito.verify(memberService).findByAccountId(Mockito.anyString());
			Mockito.verify(pointsManagerService).retrievePoints(Mockito.any(MemberModel.class));
//			checkEmpty(file, false);
		}
		finally {
			if(file.exists()) {
				file.delete();
			}
		}
	}
	
	private void checkEmpty(File file, boolean empty) {
        InputStream is = null;
        try {
            is = new FileInputStream(file);
            byte[] bytes = IOUtils.toByteArray(is);
            if (empty) {
                assertFalse(bytes.length > 0);
            } else {
                assertTrue(bytes.length > 0);
            }
        } catch (IOException e) {
            fail(e.getMessage());
        } finally {
            IOUtils.closeQuietly(is);
        }
    }
}
