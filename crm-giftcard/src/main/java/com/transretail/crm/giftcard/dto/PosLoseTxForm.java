package com.transretail.crm.giftcard.dto;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.google.common.collect.Lists;
import com.transretail.crm.common.web.converter.DateTimeSerializer;
import com.transretail.crm.common.web.converter.LocalDateTimeSerializer;
import com.transretail.crm.core.entity.lookup.Store;
import com.transretail.crm.giftcard.entity.support.GiftCardSaleTransaction;
import java.util.List;
import javax.validation.constraints.NotNull;
import org.hibernate.validator.constraints.NotEmpty;
import org.joda.time.DateTime;
import org.joda.time.LocalDateTime;
import org.springframework.format.annotation.DateTimeFormat;

/**
 *
 * @author Monte Cillo Co (mco@exist.com)
 */
public class PosLoseTxForm {

    private Store merchant;
    private String createdBy;

    @JsonSerialize(using = DateTimeSerializer.class)
    private DateTime bookedDate;

    @NotEmpty(message = "{gc.poslose.field.required.tx.no}")
    private String transactionNo;

    @NotNull(message = "{gc.poslose.field.required.tx.type}")
    private GiftCardSaleTransaction transactionType;

    @NotNull(message = "{gc.poslose.field.required.tx.time}")
    @DateTimeFormat(pattern = "dd/MM/yyyy hh:mm:ss a")
    @JsonSerialize(using = LocalDateTimeSerializer.class)
    private LocalDateTime transactionDate;

    private String refTransactionNo;

    @NotEmpty(message = "{gc.poslose.field.required.merchant.id}")
    private String merchantId;

    @NotEmpty(message = "{gc.poslose.field.required.terminal.id}")
    private String terminalId;

    @NotEmpty(message = "{gc.poslose.field.required.cashier.id}")
    private String cashierId;

    private List<PosLoseTxItem> items = Lists.newArrayList(new PosLoseTxItem());

    public Store getMerchant() {
	return merchant;
    }

    public void setMerchant(Store merchant) {
	this.merchant = merchant;
    }
    
    public String getTransactionNo() {
	return transactionNo;
    }

    public void setTransactionNo(String transactionNo) {
	this.transactionNo = transactionNo;
    }

    public GiftCardSaleTransaction getTransactionType() {
	return transactionType;
    }

    public void setTransactionType(GiftCardSaleTransaction transactionType) {
	this.transactionType = transactionType;
    }

    public String getRefTransactionNo() {
	return refTransactionNo;
    }

    public void setRefTransactionNo(String refTransactionNo) {
	this.refTransactionNo = refTransactionNo;
    }

    public String getMerchantId() {
	return merchantId;
    }

    public void setMerchantId(String merchantId) {
	this.merchantId = merchantId;
    }

    public String getTerminalId() {
	return terminalId;
    }

    public void setTerminalId(String terminalId) {
	this.terminalId = terminalId;
    }

    public String getCashierId() {
	return cashierId;
    }

    public void setCashierId(String cashierId) {
	this.cashierId = cashierId;
    }

//    public List<PosLoseTxItem> getTransactionItems() {
//	return transactionItems;
//    }
//
//    public void setTransactionItems(List<PosLoseTxItem> transactionItems) {
//	this.transactionItems = transactionItems;
//    }

    public LocalDateTime getTransactionDate() {
	return transactionDate;
    }

    public void setTransactionDate(LocalDateTime transactionDate) {
	this.transactionDate = transactionDate;
    }

    public String getCreatedBy() {
	return createdBy;
    }

    public void setCreatedBy(String createdBy) {
	this.createdBy = createdBy;
    }

    public DateTime getBookedDate() {
	return bookedDate;
    }

    public void setBookedDate(DateTime bookedDate) {
	this.bookedDate = bookedDate;
    }

	public List<PosLoseTxItem> getItems() {
		return items;
	}

	public void setItems(List<PosLoseTxItem> items) {
		this.items = items;
	}

}
