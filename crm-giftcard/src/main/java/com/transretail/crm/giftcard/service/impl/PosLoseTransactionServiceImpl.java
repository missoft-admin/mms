package com.transretail.crm.giftcard.service.impl;

import com.google.common.base.Function;
import com.google.common.collect.*;
import com.google.common.collect.ImmutableList.Builder;
import com.transretail.crm.common.service.exception.GenericServiceException;
import com.transretail.crm.core.service.StoreService;
import com.transretail.crm.giftcard.dto.*;
import com.transretail.crm.giftcard.entity.GiftCardTransaction;
import com.transretail.crm.giftcard.entity.GiftCardTransactionItem;
import com.transretail.crm.giftcard.entity.support.GiftCardSaleTransaction;
import com.transretail.crm.giftcard.entity.support.GiftCardSalesType;
import com.transretail.crm.giftcard.repo.GiftCardTransactionRepo;
import com.transretail.crm.giftcard.repo.custom.PosLoseTransactionService;
import com.transretail.crm.giftcard.service.GiftCardManager;
import java.util.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service("posLoseTransactionService")
public class PosLoseTransactionServiceImpl implements PosLoseTransactionService {

    @Autowired
    private StoreService storeService;
    @Autowired
    private GiftCardManager giftCardManager;
    @Autowired
    private GiftCardTransactionRepo transactionRepo;
    @Autowired
    private MessageSource messageSource;

    @Override
    @Transactional(readOnly = true)
    public LoseTransactionResultList search(PosLoseTransactionSearchDto searchDto) {
        return transactionRepo.findAllLoseTransactions(searchDto);
    }

    @Override
    @Transactional
    public void createNewLoseTransaction(PosLoseTxForm form) {

        final String transactionNo = form.getTransactionNo();
        final GiftCardSaleTransaction TRANSACTION_TYPE = form.getTransactionType();

        final GiftCardTransactionInfo txInfo = new GiftCardTransactionInfo()
                .loseTransaction(true)
                .cashierId(form.getCashierId())
                .merchantId(form.getMerchantId())
                .terminalId(form.getTerminalId())
                .transactionNo(transactionNo)
                .transactionTime(form.getTransactionDate())
                .refTransactionNo(form.getRefTransactionNo());

        List<PosLoseTxItem> transactionItems = form.getItems();
        Set<String> cards = FluentIterable.from(transactionItems)
                .transform(new ItemToStringFunction()).toSet();

        switch (TRANSACTION_TYPE) {
            case ACTIVATION:
                validateCardsUponActivation(GiftCardSalesType.B2C, cards, txInfo);
                giftCardManager.activate(txInfo, cards, GiftCardSalesType.B2C, false);
                break;
            case GIFT_TO_CUSTOMER:
                validateCardsUponActivation(GiftCardSalesType.REBATE, cards, txInfo);
                giftCardManager.activateFreeVoucher(txInfo, cards);
                break;
            case VOID_ACTIVATED:
                giftCardManager.voidActivation(txInfo.refTransactionNo(form.getRefTransactionNo()));
                break;
            case VOID_REDEMPTION:
                giftCardManager.voidRedemption(txInfo.refTransactionNo(form.getRefTransactionNo()));
                break;
            case REDEMPTION:
                for (PosLoseTxItem transactionItem : transactionItems) {
                    giftCardManager.redeem(txInfo, transactionItem.getCardNo(), transactionItem.getAmount());
                }
                break;
            case RELOAD:
                PosLoseTxItem item = form.getItems().iterator().next();
                giftCardManager.preReload(item.getCardNo(), txInfo.getMerchantId(), item.getAmount());
                giftCardManager.reload(txInfo, item.getCardNo(), item.getAmount().longValue());
                break;
            case VOID_RELOAD:
                final PrepaidReloadTransactionInfo reloadTx = new PrepaidReloadTransactionInfo()
                        .cashierId(txInfo.getCashierId())
                        .merchantId(txInfo.getMerchantId())
                        .transactionNo(txInfo.getTransactionNo())
                        .terminalId(txInfo.getTerminalId())
                        .transactionTime(txInfo.getTransactionTime())
                        .loseTransaction(true)
                        .refTransactionNo(txInfo.getRefTransactionNo());
                giftCardManager.reloadVoid(reloadTx);
                break;

                default:
                    throw new IllegalArgumentException("unknown GiftCardSaleTransaction :: " + TRANSACTION_TYPE);
        }
    }

    void validateCardsUponActivation(GiftCardSalesType salesType, Set<String> cards, final GiftCardTransactionInfo txInfo) throws GenericServiceException {
        for (String card : cards) {
            try {
                giftCardManager.validateActivation(txInfo, salesType, card, null);
            } catch (GenericServiceException e) {
                throw e;
            }
        }
    }

    @Override
    @Transactional(readOnly = true)
    public PosLoseTxForm findByTransactionNo(String transactioNo) {
        GiftCardTransaction result = transactionRepo.findLoseTransactionByTransactionNo(transactioNo);
        PosLoseTxForm form = new PosLoseTxForm();
        form.setBookedDate(result.getCreated());
        form.setCashierId(result.getCashierId());
        form.setCreatedBy(result.getCreateUser());
        form.setMerchant(storeService.getStoreByCode(result.getMerchantId()));
        GiftCardTransaction refTransaction = result.getRefTransaction();
        form.setRefTransactionNo(refTransaction == null ? null : refTransaction.getTransactionNo());
        form.setTerminalId(result.getTerminalId());
        form.setTransactionDate(result.getTransactionDate());
        form.setTransactionNo(result.getTransactionNo());
        form.setTransactionType(result.getTransactionType());
        Builder<PosLoseTxItem> builder = ImmutableList.builder();

        for (GiftCardTransactionItem item : result.getTransactionItems()) {
            final PosLoseTxItem posLoseTxItem = new PosLoseTxItem();
            double currentAmount = item.getCurrentAmount();
            double transactionAmount = item.getTransactionAmount();
            double prevBalance = currentAmount;
            posLoseTxItem.setAmount(Math.abs(transactionAmount));
            posLoseTxItem.setCardNo(item.getGiftCard().getBarcode());
            posLoseTxItem.setBalance(currentAmount + transactionAmount);
            posLoseTxItem.setPreviousBalance(prevBalance);
            builder.add(posLoseTxItem);
        }

        form.setItems(builder.build());
        return form;
    }

    private static class ItemToStringFunction implements Function<PosLoseTxItem, String> {

        public ItemToStringFunction() {
        }

        @Override
        public String apply(PosLoseTxItem input) {
            return input.getCardNo();
        }
    }

}
