package com.transretail.crm.giftcard.service;

import org.springframework.validation.BindingResult;

import com.transretail.crm.common.service.dto.response.ResultList;
import com.transretail.crm.giftcard.dto.ReturnRecordDto;
import com.transretail.crm.giftcard.dto.ReturnSearchDto;
import com.transretail.crm.giftcard.entity.support.ReturnStatus;



public interface ReturnByCardsService {

	ResultList<ReturnRecordDto> getReturns(ReturnSearchDto searchForm);

	boolean isValidReturn(ReturnRecordDto ret, BindingResult res);

	ReturnRecordDto getReturn(Long id);

	ReturnRecordDto getReturnByRecordNo(String returnNo);

	void saveAndReturn(ReturnRecordDto recordDto);

	void save(ReturnRecordDto recordDto);

	void returnCards(ReturnRecordDto recordDto);

	long updateByCardsRecord(String recordNo, ReturnStatus status);
	
	
}
