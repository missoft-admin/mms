package com.transretail.crm.web.member.controller;

import java.io.FileInputStream;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.servlet.ServletContext;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import net.sf.jasperreports.engine.JRParameter;

import org.apache.commons.io.IOUtils;
import org.apache.commons.io.output.ByteArrayOutputStream;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.context.support.MessageSourceResourceBundle;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;
import org.springframework.validation.Validator;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.context.ServletContextAware;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import org.springframework.web.servlet.support.RequestContextUtils;

import com.google.common.base.Predicate;
import com.google.common.collect.Collections2;
import com.google.common.collect.Maps;
import com.transretail.crm.common.reporting.exception.ReportException;
import com.transretail.crm.common.reporting.jasper.JRProcessor;
import com.transretail.crm.common.reporting.service.ReportService;
import com.transretail.crm.common.service.dto.request.PageSortDto;
import com.transretail.crm.common.service.dto.response.ResultList;
import com.transretail.crm.common.service.exception.MessageSourceResolvableException;
import com.transretail.crm.core.dto.EmployeePurchaseTxnDto;
import com.transretail.crm.core.dto.ShoppingListDto;
import com.transretail.crm.core.entity.MemberModel;
import com.transretail.crm.core.entity.enums.MarketingCorrespondenceAddress;
import com.transretail.crm.core.entity.enums.MarketingInformedAbout;
import com.transretail.crm.core.entity.enums.MarketingInterestedProds;
import com.transretail.crm.core.entity.lookup.LookupDetail;
import com.transretail.crm.core.entity.lookup.LookupHeader;
import com.transretail.crm.core.security.util.UserUtil;
import com.transretail.crm.core.service.CodePropertiesService;
import com.transretail.crm.core.service.LookupService;
import com.transretail.crm.core.service.StoreService;
import com.transretail.crm.web.dto.PointResultList;
import com.transretail.crm.web.dto.PointSearchDto;
import com.transretail.crm.web.member.bean.ChangePasswordDto;
import com.transretail.crm.web.member.bean.ChangePasswordDto2;
import com.transretail.crm.web.member.bean.MemberDto;
import com.transretail.crm.web.member.bean.RegisterDto;
import com.transretail.crm.web.member.bean.ResetPasswordDto;
import com.transretail.crm.web.member.bean.UpdatePinDto;
import com.transretail.crm.web.member.service.MemberServiceExt;

@Controller
@RequestMapping("/member")
public class MemberController implements ServletContextAware {
    protected static final String KEY_SUCCESS = "successMessages";
    protected static final String KEY_ERROR = "errorMessages";
    protected static final String FORM_ATTR_NAME = "form";
    protected static final String STORES_ATTR_NAME = "stores";
    protected static final String GENDER_ATTR_NAME = "genders";
    protected static final String MEMBERTYPES_ATTR_NAME = "memberTypes";
    protected static final String PREFERED_LANGUAGES_ATTR_NAME = "preferedLanguages";
    protected static final String MARITALSTATUSES_ATTR_NAME = "maritalStatuses";
    protected static final String EDUCATIONS_ATTR_NAME = "educations";
    protected static final String OCCUPATIONS_ATTR_NAME = "occupations";
    protected static final String HOUSEHOLDINCOMES_ATTR_NAME = "householdIncomes";
    protected static final String PERSONALINCOMES_ATTR_NAME = "personalIncomes";
    protected static final String FAMILYSIZES_ATTR_NAME = "familySizes";
    protected static final String CHILDRENCOUNTS_ATTR_NAME = "childrenCounts";
    protected static final String PETS_ATTR_NAME = "pets";
    protected static final String CUISINES_ATTR_NAME = "cuisines";
    protected static final String INTERESTS_ATTR_NAME = "interests";
    protected static final String RELIGIONS_ATTR_NAME = "religions";
    protected static final String NATIONALITIES_ATTR_NAME = "nationalities";
    protected static final String DEPARTMENT_FIELDS_ATTR_NAME = "departmentFields";
    protected static final String BUSINESS_FIELDS_ATTR_NAME = "businessFields";
    protected static final String EMPTYPES_ATTR_NAME = "employeeTypes";
    protected static final String CUSTOMER_GROUP_ATTR_NAME = "customerGroups";
    protected static final String CUSTOMER_SEG_ATTR_NAME = "customerSegmentations";
    protected static final String BANKS_ATTR_NAME = "banks";
    protected static final String BEST_TIME_TO_CALL = "bestTimeToCall";
    protected static final String SOCIAL_MEDIA = "socialMedia";
    protected static final String VISITED_SUPERMARKETS = "visitedSupermarkets";
    protected static final String INTERESTED_ACTIVITIES = "interestedActivities";
    protected static final String VEHICLES_OWNED = "vehiclesOwned";
    private static final Logger _LOG = LoggerFactory.getLogger(MemberController.class);
    
    private String imagePath;

    @Autowired
    private MemberServiceExt memberServiceExt;
    @Autowired
    @Qualifier("memberValidator")
    private Validator memberValidator;
    @Autowired
    @Qualifier("memberUpdateValidator")
    private Validator memberUpdateValidator;
    @Autowired
    private MessageSource messageSource;
    @Autowired
    private StoreService storeService;
    @Autowired
    private LookupService lookupService;
    @Autowired
    private ReportService reportService;

    @Autowired
    private CodePropertiesService codePropertiesService;

    @RequestMapping(value = "/points", method = RequestMethod.GET)
    public ModelAndView showPointsPage() {
        Long userId = UserUtil.getCurrentUser().getUserId();
        return new ModelAndView("member/pointsList").addObject("totalPoints", memberServiceExt.getTotalPoints(userId))
        		.addObject("isSupplementary", memberServiceExt.isSupplementary(userId))
        		.addObject("supplements", memberServiceExt.getSupplements(userId));
    }
    
    @RequestMapping(value = "/purchases", method = RequestMethod.GET)
    public ModelAndView showPurchasePage() {
        Long userId = UserUtil.getCurrentUser().getUserId();
        return new ModelAndView("member/purchaseList").addObject("totalAmount", memberServiceExt.getTotalPurchaseAmountForCurrentMonth(userId));
    }

    @RequestMapping(value = "/pin/update", method = RequestMethod.GET)
    public ModelAndView showUpdatePinPage() {
        return new ModelAndView("member/changePin").addObject(FORM_ATTR_NAME, new UpdatePinDto());
    }

    @RequestMapping(value = "/pin/update", method = RequestMethod.POST)
    public ModelAndView updatePin(@ModelAttribute(FORM_ATTR_NAME) @Valid UpdatePinDto dto, BindingResult result,
        HttpServletRequest request) throws Exception {
        ModelAndView mav = null;
        if (result.hasErrors()) {
            mav = new ModelAndView("member/changePin");
        } else {
            String username = UserUtil.getCurrentUser().getUsername();
            try {
                memberServiceExt.updatePin(username, dto.getPin());
                /*redirectAttributes.addFlashAttribute(KEY_SUCCESS,
                messageSource.getMessage("member.pin.change.successful", new String[]{username},
                    RequestContextUtils.getLocale(request)));*/
                mav = new ModelAndView("member/changePin");
                mav.addObject(KEY_SUCCESS, messageSource
                    .getMessage("member.pin.change.successful", new String[]{username}, RequestContextUtils.getLocale(request)));
            } catch (MessageSourceResolvableException e) {
                _LOG.warn("Failed to change pin", e);
                mav = new ModelAndView("member/changePin")
                        .addObject(KEY_ERROR, messageSource.getMessage(e, RequestContextUtils.getLocale(request)));
            }
        }
        return mav;
    }

    @RequestMapping( value="/pin/check", produces="application/json; charset=utf-8", method=RequestMethod.POST )
	public @ResponseBody Boolean isPinNull() {
        return !memberServiceExt.hasPin(UserUtil.getCurrentUser().getUsername());
	}

    private Map<String, Object> getMemberUpdateLookupDetails() {
        Map<String, Object> map = Maps.newHashMap();
        map.put(GENDER_ATTR_NAME, lookupService.getActiveDetailsByHeaderCode(codePropertiesService.getHeaderGender()));
        map.put(MEMBERTYPES_ATTR_NAME, lookupService.getActiveDetailsByHeaderCode(codePropertiesService.getHeaderMemberType()));
        map.put(PREFERED_LANGUAGES_ATTR_NAME, lookupService.getActiveDetailsByHeaderCode(codePropertiesService.getHeaderPreferredLanguage()));
        map.put(MARITALSTATUSES_ATTR_NAME, lookupService.getActiveDetailsByHeaderCode(codePropertiesService.getHeaderMaritalStatus()));
        map.put(EDUCATIONS_ATTR_NAME, lookupService.getActiveDetailsByHeaderCode(codePropertiesService.getHeaderEducation()));
        map.put(OCCUPATIONS_ATTR_NAME, lookupService.getActiveDetailsByHeaderCode(codePropertiesService.getHeaderOccupation()));
        map.put(HOUSEHOLDINCOMES_ATTR_NAME, lookupService.getActiveDetailsByHeaderCode(codePropertiesService.getHeaderHouseHoldIncome()));
        map.put(PERSONALINCOMES_ATTR_NAME, lookupService.getActiveDetailsByHeaderCode(codePropertiesService.getHeaderPersonalIncome()));
        map.put(FAMILYSIZES_ATTR_NAME, lookupService.getActiveDetailsByHeaderCode(codePropertiesService.getHeaderFamilySize()));
        map.put(CHILDRENCOUNTS_ATTR_NAME, lookupService.getActiveDetailsByHeaderCode(codePropertiesService.getHeaderChildren()));
        map.put(PETS_ATTR_NAME, lookupService.getActiveDetailsByHeaderCode(codePropertiesService.getHeaderPetOwnership()));
        map.put(CUISINES_ATTR_NAME, lookupService.getActiveDetailsByHeaderCode(codePropertiesService.getHeaderFoodPreference()));
        map.put(INTERESTS_ATTR_NAME, lookupService.getActiveDetailsByHeaderCode(codePropertiesService.getHeaderInterest()));
        map.put(NATIONALITIES_ATTR_NAME, lookupService.getActiveDetailsByHeaderCode(codePropertiesService.getHeaderNationality()));
        map.put(RELIGIONS_ATTR_NAME, lookupService.getActiveDetailsByHeaderCode(codePropertiesService.getHeaderReligion()));
        map.put(DEPARTMENT_FIELDS_ATTR_NAME, lookupService.getActiveDetailsByHeaderCode(codePropertiesService.getHeaderDepartmentFields()));
        map.put(BUSINESS_FIELDS_ATTR_NAME, lookupService.getActiveDetailsByHeaderCode(codePropertiesService.getHeaderBusinessFields()));
        map.put(EMPTYPES_ATTR_NAME, lookupService.getActiveDetailsByHeaderCode(codePropertiesService.getHeaderEmpType()));
        map.put(BANKS_ATTR_NAME, lookupService.getActiveDetailsByHeaderCode(codePropertiesService.getHeaderBanks()));
        map.put(BEST_TIME_TO_CALL, lookupService.getActiveDetailsByHeaderCode(codePropertiesService.getBestTimeToCall()));
        map.put(SOCIAL_MEDIA, lookupService.getActiveDetailsByHeaderCode(codePropertiesService.getSocialMedia()));
        map.put(VISITED_SUPERMARKETS, lookupService.getActiveDetailsByHeaderCode(codePropertiesService.getVisitedSupermarkets()));
        map.put(INTERESTED_ACTIVITIES, lookupService.getActiveDetailsByHeaderCode(codePropertiesService.getInterestedActivities()));
        map.put(VEHICLES_OWNED, lookupService.getActiveDetailsByHeaderCode(codePropertiesService.getVehiclesOwned()));
        
        map.put("noOfEmps", lookupService.getActiveDetailsByHeaderCode(codePropertiesService.getHeaderNoOfEmps()));
        
        map.put("correspondenceAddressCodes", lookupService.getActiveDetailsByHeaderCode(codePropertiesService.getHeaderCorrespondenceAddress()));
        map.put("informedAboutCodes", lookupService.getActiveDetailsByHeaderCode(codePropertiesService.getHeaderInformedAbout()));
        map.put("interestedProductsCodes", lookupService.getActiveDetailsByHeaderCode(codePropertiesService.getHeaderInterestedProds()));
        
        List<LookupDetail> customerGroups = lookupService.getActiveDetailsByHeaderCode(codePropertiesService.getHeaderCustomerGroup());
        List<LookupDetail> customerSegmentation = lookupService.getActiveDetailsByHeaderCode(codePropertiesService.getHeaderCustomerSegmentation());
        map.put(CUSTOMER_GROUP_ATTR_NAME, customerGroups);
        Map<String, Collection<LookupDetail>> segmentMap = new HashMap<String, Collection<LookupDetail>>();
        for(LookupDetail detail : customerGroups) {
        	final String detailCode = detail.getCode();
        	Collection<LookupDetail> segmentation = Collections2.filter(customerSegmentation, new Predicate<LookupDetail>() {
				@Override
				public boolean apply(LookupDetail input) {

					char code = detailCode.charAt(detailCode.length() - 1);
					return (input.getCode().charAt(4) == code);
				}
        	});
        	segmentMap.put(detailCode, segmentation);
        }
        map.put(CUSTOMER_SEG_ATTR_NAME, segmentMap);
        return map;
    }

    @RequestMapping(value = "/update", method = RequestMethod.GET)
    public ModelAndView showMemberUpdatePage() {
        MemberDto member = memberServiceExt.getMemberDto(UserUtil.getCurrentUser().getUsername());
        return new ModelAndView("member/update", FORM_ATTR_NAME, member)
            .addObject(STORES_ATTR_NAME, storeService.getAllStores())
            .addObject("isProffesional", codePropertiesService.getDetailMemberTypeCompany().equals(member.getMemberTypeCode()))
            .addObject("isPrimary", !memberServiceExt.isSupplementary(UserUtil.getCurrentUser().getUserId()))
            .addAllObjects(getMemberUpdateLookupDetails());
    }


    @RequestMapping(value = "/update", method = RequestMethod.POST)
    public ModelAndView saveMemberUpdate(@ModelAttribute(FORM_ATTR_NAME) @Valid MemberDto dto,
        BindingResult result, RedirectAttributes redirectAttributes, HttpServletRequest request) {
        ModelAndView mav = null;
        String username = UserUtil.getCurrentUser().getUsername();
        boolean isProffesional = codePropertiesService.getDetailMemberTypeCompany().equals(dto.getMemberTypeCode());
        _LOG.debug(result.getAllErrors().toString());
        if (result.hasErrors()) {
        	_LOG.debug("UPDATE: has errors");
            return new ModelAndView("member/update").addObject("isProffesional", isProffesional).addObject(STORES_ATTR_NAME, storeService.getAllStores())
                .addAllObjects(getMemberUpdateLookupDetails());
        } else {
            dto.withUsername(username);
            memberUpdateValidator.validate(dto, result);
            if (result.hasErrors()) {
            	_LOG.debug("UPDATE: has errors here");
                return new ModelAndView("member/update").addObject("isProffesional", isProffesional).addObject(STORES_ATTR_NAME, storeService.getAllStores())
                    .addAllObjects(getMemberUpdateLookupDetails());
            } else {
            	_LOG.debug("UPDATE: saving member");
                memberServiceExt.updateMember(dto);
                redirectAttributes.addFlashAttribute(KEY_SUCCESS,
                    messageSource.getMessage("member.save.successful", new String[]{username},
                        RequestContextUtils.getLocale(request)));
                mav = new ModelAndView("redirect:/member/update");
            }
        }
        return mav;
    }

    @RequestMapping(value = "/registration", method = RequestMethod.GET)
    public ModelAndView showRegistrationPage() {
        return new ModelAndView("member/registration", FORM_ATTR_NAME, new RegisterDto())
        	.addObject(GENDER_ATTR_NAME, lookupService.getActiveDetails(new LookupHeader(codePropertiesService.getHeaderGender())))
            .addObject(STORES_ATTR_NAME, storeService.getAllStores());
    }

    @RequestMapping(value = "/registration", method = RequestMethod.POST)
    public ModelAndView saveRegistration(@ModelAttribute(FORM_ATTR_NAME) @Valid RegisterDto registerDto,
        BindingResult result, RedirectAttributes redirectAttributes, HttpServletRequest request) {
        ModelAndView mav = null;
        if (result.hasErrors()) {
            mav = new ModelAndView("member/registration")
            .addObject(GENDER_ATTR_NAME, lookupService.getActiveDetails(new LookupHeader(codePropertiesService.getHeaderGender())))
            .addObject(STORES_ATTR_NAME, storeService.getAllStores());
        } else {
            memberValidator.validate(registerDto, result);
            if (result.hasErrors()) {
                mav = new ModelAndView("member/registration")
                .addObject(GENDER_ATTR_NAME, lookupService.getActiveDetails(new LookupHeader(codePropertiesService.getHeaderGender())))
                .addObject(STORES_ATTR_NAME, storeService.getAllStores());
            } else {
                try {
                    memberServiceExt.save(registerDto, request);
                    redirectAttributes.addFlashAttribute(KEY_SUCCESS,
                        messageSource.getMessage("member.registration.confirmation", new String[]{registerDto.getUsername()},
                            RequestContextUtils.getLocale(request)));
                    mav = new ModelAndView("redirect:/login");
                } catch (MessageSourceResolvableException e) {
                    mav = new ModelAndView("member/registration")
                        .addObject(GENDER_ATTR_NAME, lookupService.getActiveDetails(new LookupHeader(codePropertiesService.getHeaderGender())))
                        .addObject(STORES_ATTR_NAME, storeService.getAllStores())
                        .addObject(messageSource.getMessage(e, RequestContextUtils.getLocale(request)));
                }
            }
        }
        return mav;
    }

    @RequestMapping(value = "/validate", method = RequestMethod.GET)
    public ModelAndView validateRegistration(@RequestParam("code") String validationCode,
        RedirectAttributes redirectAttributes,
        HttpServletRequest request) {
        _LOG.debug("Validating code {}", validationCode);
        if (memberServiceExt.validate(validationCode)) {
            redirectAttributes.addFlashAttribute(KEY_SUCCESS,
                messageSource.getMessage("member.validation.confirmation", null,
                    RequestContextUtils.getLocale(request)));
        } else {
            redirectAttributes.addFlashAttribute(KEY_ERROR,
                messageSource.getMessage("member.validation.error", null,
                    RequestContextUtils.getLocale(request)));
        }
        return new ModelAndView("redirect:/login");
    }

    @RequestMapping(value = "/password/change", method = RequestMethod.GET)
    public ModelAndView showPasswordChangePage() {
        return new ModelAndView("member/changePassword", FORM_ATTR_NAME, new ChangePasswordDto2())
            .addObject("username", UserUtil.getCurrentUser().getUsername());
    }

    @RequestMapping(value = "/password/change", method = RequestMethod.POST)
    public ModelAndView savePasswordChange(@ModelAttribute(FORM_ATTR_NAME) @Valid ChangePasswordDto2 ChangePasswordDto2,
        BindingResult result, HttpServletRequest request) {
        ModelAndView mav = null;
        if (result.hasErrors()) {
            mav = new ModelAndView("member/changePassword");
        } else {
            try {
                memberServiceExt.sendChangePasswordValidation(UserUtil.getCurrentUser().getUsername(), ChangePasswordDto2, request);
                /*redirectAttributes.addFlashAttribute(KEY_SUCCESS,
	                    messageSource.getMessage("member.password.change.confirmation", null, RequestContextUtils.getLocale(request)));
	                mav = new ModelAndView("redirect:/");*/
                mav = new ModelAndView("member/changePassword");
                mav.addObject(KEY_SUCCESS,
                    messageSource.getMessage("member.password.change.confirmation", null, RequestContextUtils.getLocale(request)));
            } catch (MessageSourceResolvableException e) {
                _LOG.warn("Failed to change password", e);
                mav = new ModelAndView("member/changePassword")
                    .addObject(KEY_ERROR, messageSource.getMessage(e, RequestContextUtils.getLocale(request)));
            }
        }
        return mav;
    }

    @RequestMapping(value = "/{username}/password/change", method = RequestMethod.GET)
    public ModelAndView validatePasswordChange(@PathVariable("username") String username, @RequestParam("code") String validationCode,
        RedirectAttributes redirectAttributes, HttpServletRequest request) {
        _LOG.debug("Change password validation code {}", validationCode);
        ModelAndView mav = null;
        if(UserUtil.getCurrentUser() != null) {
            mav = new ModelAndView("redirect:/");
        } else {
            mav = new ModelAndView("redirect:/login");
        }
        if (memberServiceExt.validateChangePassword(username, validationCode)) {
            redirectAttributes.addFlashAttribute(KEY_SUCCESS, messageSource
                .getMessage("member.password.change.validation.successful", null, RequestContextUtils.getLocale(request)));
        } else {
            redirectAttributes.addFlashAttribute(KEY_ERROR,
                messageSource.getMessage("member.password.change.validation.error", null, RequestContextUtils.getLocale(request)));
        }
        return mav;
    }

    @RequestMapping(value = "/password/reset", method = RequestMethod.GET)
    public ModelAndView resetPassword() {
        return new ModelAndView("member/resetPassword").addObject(FORM_ATTR_NAME, new ResetPasswordDto());
    }

    @RequestMapping(value = "/password/reset", method = RequestMethod.POST)
    public ModelAndView resetPassword(@ModelAttribute(FORM_ATTR_NAME) @Valid ResetPasswordDto dto,
        BindingResult result, RedirectAttributes redirectAttributes, HttpServletRequest request) {
        ModelAndView mav = null;
        if (result.hasErrors()) {
            mav = new ModelAndView("member/resetPassword");
        } else {
            try {
            	memberServiceExt.sendResetPasswordValidation(dto.getBirthDate(), dto.getEmail(), request);//memberServiceExt.sendResetPasswordValidation(dto.getUsername(), dto.getEmail(), request);
                redirectAttributes.addFlashAttribute(KEY_SUCCESS,
                    messageSource.getMessage("member.password.reset.confirmation", null, RequestContextUtils.getLocale(request)));
                mav = new ModelAndView("redirect:/login");
            } catch (MessageSourceResolvableException e) {
                _LOG.warn("Failed to reset password", e);
                mav = new ModelAndView("member/resetPassword")
                    .addObject(KEY_ERROR, messageSource.getMessage(e, RequestContextUtils.getLocale(request)));
            }
        }
        return mav;
    }

    @RequestMapping(value = "/{username}/password/reset", method = RequestMethod.GET)
    public ModelAndView resetPassword(@PathVariable("username") String username, @RequestParam("code") String validationCode,
        HttpServletRequest request) {
        _LOG.debug("Reset password validation code {}", validationCode);
        ModelAndView mav = null;
        if (memberServiceExt.validateResetPassword(username, validationCode)) {
            mav = new ModelAndView("member/resetPassword2").addObject("username", username)
                .addObject(FORM_ATTR_NAME, new ChangePasswordDto())
                .addObject(KEY_SUCCESS, messageSource.getMessage("member.password.reset.validated", null,
                    RequestContextUtils.getLocale(request)));
        } else {
            mav = new ModelAndView("login")
                .addObject(KEY_ERROR, messageSource.getMessage("member.password.change.validation.error", null,
                    RequestContextUtils.getLocale(request)));
        }
        return mav;
    }

    @RequestMapping(value = "/{username}/password/reset", method = RequestMethod.POST)
    public ModelAndView resetPassword(@PathVariable("username") String username,
        @ModelAttribute(FORM_ATTR_NAME) @Valid ChangePasswordDto dto, BindingResult result, RedirectAttributes redirectAttributes,
        HttpServletRequest request) {
        ModelAndView mav = null;
        if (result.hasErrors()) {
            mav = new ModelAndView("member/resetPassword2").addObject("username", username);
        } else {
            memberServiceExt.updatePassword(username, dto.getPassword());
            redirectAttributes.addFlashAttribute(KEY_SUCCESS,
                messageSource.getMessage("member.save.successful", new String[]{username},
                    RequestContextUtils.getLocale(request)));
            mav = new ModelAndView("redirect:/login");
        }
        return mav;
    }

    /* Ajax Requests */
    @ExceptionHandler(MethodArgumentNotValidException.class)
    @ResponseBody
    public Map<String, List<String>> handleMethodArgumentNotValidException(MethodArgumentNotValidException error) {
        BindingResult result = error.getBindingResult();
        List<String> errors = new ArrayList<String>();
        for (ObjectError oe : result.getAllErrors()) {
            errors.add(messageSource.getMessage(oe, LocaleContextHolder.getLocale()));
        }
        Map<String, List<String>> map = new HashMap<String, List<String>>();
        map.put(KEY_ERROR, errors);
        return map;
    }

    @RequestMapping(value = "/points", method = RequestMethod.POST)
    @ResponseBody
    public PointResultList getPoints(@RequestBody PointSearchDto dto) {
        Long userId = UserUtil.getCurrentUser().getUserId();
        return memberServiceExt.getUserPoints(dto.withUserId(userId));
    }
    
    @RequestMapping(value = "/purchases", method = RequestMethod.POST)
    @ResponseBody
    public ResultList<EmployeePurchaseTxnDto> getPurchases(@RequestBody PageSortDto dto) {
        Long userId = UserUtil.getCurrentUser().getUserId();
        return memberServiceExt.getPurchasesForCurrentMonth(userId, dto);
    }

    @RequestMapping(value = "/supplements", method=RequestMethod.POST)
    @ResponseBody
    public ResultList<MemberModel> getSupplements() {
    	Long userId = UserUtil.getCurrentUser().getUserId();
    	List<MemberModel> results = memberServiceExt.getSupplements(userId);
    	return new ResultList<MemberModel>(results, results.size(), false, false);
    }
    
    @RequestMapping(value="/shoppinglist", method=RequestMethod.POST)
    @ResponseBody
    public ResultList<ShoppingListDto> getShoppingList(@RequestBody PageSortDto dto) {
    	Long userId = UserUtil.getCurrentUser().getUserId();
    	return memberServiceExt.getShoppingList(userId, dto);
    }
    
    @RequestMapping(value="/shoppinglist", method=RequestMethod.GET)
    public ModelAndView showShoppingListPage() {
    	return new ModelAndView("member/shoppingList");
    }
    
    @RequestMapping(value="/report/shoppinglist")
    public void printShoppingList(final HttpServletResponse response, HttpServletRequest request) throws Exception {
    	Long userId = UserUtil.getCurrentUser().getUserId();
    	InputStream logo = null;
    	try {
    		logo = new FileInputStream(imagePath + "/carrefour/carrefouridlg.png");
    		JRProcessor jrProcessor = memberServiceExt.getShoppingListJRProcessor(userId, logo);
    		renderReport(response, jrProcessor);
    	}
    	finally {
    		IOUtils.closeQuietly(logo);
    	}
    }
    
    protected void renderReport(HttpServletResponse response, JRProcessor jrProcessor) throws ReportException {
        Locale locale = LocaleContextHolder.getLocale();
        if (!jrProcessor.hasParameter(JRParameter.REPORT_LOCALE)) {
            jrProcessor.addParameter(JRParameter.REPORT_LOCALE, locale);
        }
        if (!jrProcessor.hasParameter(JRParameter.REPORT_RESOURCE_BUNDLE)) {
            jrProcessor.addParameter(JRParameter.REPORT_RESOURCE_BUNDLE, new MessageSourceResourceBundle(messageSource, locale));
        }

        try {
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            reportService.exportPdfReport(jrProcessor, baos);

            response.setHeader("Cache-Control", "no-cache");
            // Write content type and also length (determined via byte array).
            response.setContentType("application/pdf");
            response.setContentLength(baos.size());
            // Flush byte array to servlet output stream.
            ServletOutputStream out = response.getOutputStream();
            baos.writeTo(out);
            out.flush();
        } catch (Exception e) {
            throw new ReportException(e);
        }
    }

	@Override
	public void setServletContext(ServletContext servletContext) {
		imagePath = servletContext.getRealPath("images");
	}
}
