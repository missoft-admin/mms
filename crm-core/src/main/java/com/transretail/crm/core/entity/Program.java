package com.transretail.crm.core.entity;


import java.io.Serializable;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.envers.Audited;

import com.transretail.crm.core.entity.embeddable.Duration;
import com.transretail.crm.core.entity.enums.ProgramType;
import com.transretail.crm.core.entity.lookup.LookupDetail;
import com.transretail.crm.core.entity.support.CustomAuditableEntity;


@Audited
@Table(name = "CRM_PROGRAM")
@Entity
public class Program extends CustomAuditableEntity<Long> implements Serializable {


	@Column(name="NAME")
    private String name;

	@Column(name="DESCRIPTION")
    private String description;

    @ManyToOne
    @JoinColumn(name="STATUS")
    private LookupDetail status;

    @Column(name = "TYPE", length = 20, nullable = false)
    @Enumerated(EnumType.STRING)
    private ProgramType type = ProgramType.BASIC;

    @OneToMany(mappedBy = "program", orphanRemoval = true, fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private List<Campaign> campaigns;

    @Embedded
    private Duration duration;


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public LookupDetail getStatus() {
        return status;
    }

    public void setStatus(LookupDetail status) {
        this.status = status;
    }

    public ProgramType getType() {
        return type;
    }

    public void setType(ProgramType type) {
        this.type = type;
    }

    public List<Campaign> getCampaigns() {
        return campaigns;
    }

    public void setCampaigns(List<Campaign> campaigns) {
        this.campaigns = campaigns;
    }

    public Duration getDuration() {
        return duration;
    }

    public void setDuration(Duration duration) {
        this.duration = duration;
    }
}
