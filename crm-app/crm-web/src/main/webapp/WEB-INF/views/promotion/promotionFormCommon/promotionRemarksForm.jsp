<%@ include file="../../common/taglibs.jsp" %>

 
          <div class="control-group-01">
          <fieldset>
            <legend class="block-stack-title"><spring:message code="remark_prop_remark" /></legend>
            <div class="controls"><form:textarea path="remarks.remarks" id="remarksRemarks"/></div>
            <form:hidden path="remarks.status" id="remarksStatus"/>
            <form:hidden path="remarks.modelId"/>
            <form:hidden path="remarks.modelType"/>
          </fieldset>
          </div>
        