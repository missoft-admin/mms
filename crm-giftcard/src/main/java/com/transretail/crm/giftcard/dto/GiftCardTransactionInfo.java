package com.transretail.crm.giftcard.dto;

import org.joda.time.LocalDateTime;

/**
 * @author Monte Cillo Co (mco@exist.com)
 */
@SuppressWarnings({"unchecked"})
public class GiftCardTransactionInfo<T extends GiftCardTransactionInfo> {
    private String merchantId;
    private String terminalId;
    private String cashierId;
    private String transactionNo;
    private LocalDateTime transactionTime;
    private String refTransactionNo;
    private boolean loseTransaction;

    public T merchantId(String merchantId) {
        this.merchantId = merchantId;
        return (T) this;
    }

    public T terminalId(String terminalId) {
        this.terminalId = terminalId;
        return (T) this;
    }

    public T cashierId(String cashierId) {
        this.cashierId = cashierId;
        return (T) this;
    }

    public T transactionNo(String transactionNo) {
        this.transactionNo = transactionNo;
        return (T) this;
    }

    public T transactionTime(LocalDateTime transactionTime) {
        this.transactionTime = transactionTime;
        return (T) this;
    }

    public T refTransactionNo(String refTransactionNo) {
        this.refTransactionNo = refTransactionNo;
        return (T) this;
    }

    public T loseTransaction(boolean loseTransaction) {
        this.loseTransaction = loseTransaction;
        return (T) this;
    }

    public String getMerchantId() {
        return merchantId;
    }

    public String getTerminalId() {
        return terminalId;
    }

    public String getCashierId() {
        return cashierId;
    }

    public String getTransactionNo() {
        return transactionNo;
    }

    public LocalDateTime getTransactionTime() {
        return transactionTime;
    }

    public String getRefTransactionNo() {
        return refTransactionNo;
    }

    public boolean isLoseTransaction() {
        return loseTransaction;
    }

}
