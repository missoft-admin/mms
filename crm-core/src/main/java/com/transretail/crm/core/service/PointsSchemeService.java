package com.transretail.crm.core.service;

import com.transretail.crm.common.service.dto.request.PageSortDto;
import com.transretail.crm.common.service.dto.response.ResultList;
import com.transretail.crm.core.entity.RewardSchemeModel;

import java.util.List;

public interface PointsSchemeService {


    RewardSchemeModel saveAndUpdate(RewardSchemeModel pointsSchemeModel);


    List<RewardSchemeModel> findAllPointsSchemes();


    List<RewardSchemeModel> findAllActivePointsSchemes();


    void deleteAllPointsSchemes();

    List<RewardSchemeModel> findPointsSchemeEntries(int firstResult, int maxResult);
    
    long countAllPointsSchemes();
    
    void savePointsScheme(RewardSchemeModel pointsScheme);

    Double findLowestActivePointsSchemeValue();
    
    void deletePointsScheme(Long id);
    
    RewardSchemeModel findPointsScheme(Long id);
    
    boolean hasOverlappingRange(Long id, Double from, Double to);
    
    boolean hasOverlappingValue(Long id, Double value);


	ResultList<RewardSchemeModel> listSchemeDto(PageSortDto dto);

}
