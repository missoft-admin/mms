package com.transretail.crm.core.service;


import java.util.List;

import com.transretail.crm.common.service.dto.response.NameIdPairDto;
import com.transretail.crm.common.service.dto.response.ResultList;
import com.transretail.crm.core.dto.ServiceDto;
import com.transretail.crm.core.dto.UserDto;
import com.transretail.crm.core.dto.UserSearchDto;
import com.transretail.crm.core.entity.UserModel;
import com.transretail.crm.core.entity.UserRoleModel;


public interface UserService {

    long countAllUserModels();

    void deleteUserModel(UserModel userModel);

    UserModel findUserModel(Long id);

    List<UserModel> findAllUserModels();

    List<UserModel> findUserModelEntries(int firstResult, int maxResults);

    void saveUserModel(UserModel userModel);

    UserModel updateUserModel(UserModel userModel);

    UserModel findUserModel(String username);

	ServiceDto retrieveCssUsers();

	ServiceDto searchUser(UserModel inUser, Integer inPage, Integer inSize);

	ServiceDto searchUser(UserModel inUser);

	List<UserModel> retrieveHrsUsers();

	ResultList<UserDto> searchUser(UserSearchDto theSearchDto);

	UserDto findUserDto(Long id);

	void saveUserDto(UserDto inUser);

	void updateUserDto(UserDto inUser);
	
	List<UserModel> findUsersByRoleCode(String roleCode);
	
	List<UserRoleModel> findUserRoleModel(String code);

    void changePassword(String username, String newPassword);
    
    boolean currentPasswordMatch(String currentPassword) ;

	List<UserDto> findCsByStoreCode(String storeCode, String currentlyAssigned);

	List<String> listSupervisorEmails(String storeCode, String supervisorCode);

	UserModel findUserModelIgnoreCase(String inUsername);

	List<String> listSupervisorEmails(String supervisorCode);

	NameIdPairDto getUserExhibition(Long id);

	void saveExhibition(NameIdPairDto form);
}
