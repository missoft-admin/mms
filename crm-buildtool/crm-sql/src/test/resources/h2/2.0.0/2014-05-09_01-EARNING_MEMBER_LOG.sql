create table CRM_ID_REF (
        id varchar(255) not null,
        CREATED_DATETIME timestamp,
        UPDATED_DATETIME timestamp,
        MODEL_ID varchar(50),
        ID_TYPE varchar(50),
        primary key (id)
    );