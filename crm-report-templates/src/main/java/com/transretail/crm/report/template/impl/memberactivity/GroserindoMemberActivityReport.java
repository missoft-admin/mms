package com.transretail.crm.report.template.impl.memberactivity;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Locale;
import java.util.Map;
import java.util.Set;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.sql.DataSource;

import org.apache.commons.lang3.StringUtils;
import org.joda.time.LocalDate;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.google.common.collect.Maps;
import com.google.common.collect.Sets;
import com.mysema.query.jpa.impl.JPAQuery;
import com.mysema.query.types.ConstructorExpression;
import com.transretail.crm.common.reporting.jasper.JRProcessor;
import com.transretail.crm.common.service.dto.response.CodeDescDto;
import com.transretail.crm.common.util.DateUtil;
import com.transretail.crm.core.entity.enums.ReportType;
import com.transretail.crm.core.entity.enums.Status;
import com.transretail.crm.core.entity.lookup.QLookupDetail;
import com.transretail.crm.core.service.CodePropertiesService;
import com.transretail.crm.report.template.CommonReportFilter;
import com.transretail.crm.report.template.FilterField;
import com.transretail.crm.report.template.LOVFactory;
import com.transretail.crm.report.template.ReportTemplate;

/**
 * @author Allan G. Ramirez (agramirez@exist.com)
 */
@Service
public class GroserindoMemberActivityReport implements ReportTemplate {

    @PersistenceContext
    private EntityManager em;
    @Autowired
    private MessageSource messageSource;
    @Autowired
    private DataSource dataSource;
    @Autowired
    private CodePropertiesService codePropertiesService;
    @Autowired
    private LOVFactory lOVFactory;

    @Override
    public String getName() {
        return "Member Activity";
    }

    @Override
    public String getDescription() {
        return messageSource.getMessage("member.activity.report.print.label", null, LocaleContextHolder.getLocale());
    }

    @Override
    public Set<FilterField> getFilters() {
        Locale locale = LocaleContextHolder.getLocale();
        Set<FilterField> filters = Sets.newLinkedHashSet();
        Map<String, String> reportTypes = Maps.newHashMap();
        reportTypes.put("daily", messageSource.getMessage("member.activity.report.filter.reporttype.daily", null, locale));
        // Task #96520
        // reportTypes.put("weekly", messageSource.getMessage("member.activity.report.filter.reporttype.weekly", null, locale));
        reportTypes.put("monthly", messageSource.getMessage("member.activity.report.filter.reporttype.monthly", null, locale));

        filters.add(FilterField.createDropdownField("activityReportType",
                messageSource.getMessage("member.activity.report.filter.reporttype", null, locale), reportTypes));
        filters.add(FilterField.createDateField(CommonReportFilter.DATE_TO,
                messageSource.getMessage("member.activity.report.filter.date.to", null, locale),
                DateUtil.SEARCH_DATE_FORMAT, true));
        filters.add(FilterField.createDateField(CommonReportFilter.DATE_FROM,
                messageSource.getMessage("member.activity.report.filter.date.from", null, locale),
                DateUtil.SEARCH_DATE_FORMAT, true));
        filters.add(FilterField.createDropdownField(CommonReportFilter.STORE, 
                messageSource.getMessage("member.activity.report.filter.store", null, locale), 
                lOVFactory.getStoreOptions()));
        return filters;
    }

    @Override
    public boolean canView(Set<String> permissions) {
        return permissions.contains("REPORT_MEMBER_ACTIVITY_VIEW");
    }

    @Transactional(readOnly = true)
    @Override
    public JRProcessor createJrProcessor(Map<String, String> parameters) {
        final LocalDate dateFrom = parseDate(parameters.get(CommonReportFilter.DATE_FROM));
        final LocalDate dateTo = parseDate(parameters.get(CommonReportFilter.DATE_TO));
        final String storeCode = parameters.get(CommonReportFilter.STORE);
        final String activityReportType = parameters.get("activityReportType");
        final ReportType reportType = ReportType.valueOf(parameters.get(ReportTemplate.PARAM_REPORT_TYPE).toUpperCase());

        Locale locale = LocaleContextHolder.getLocale();

        AbstractMemberActivityReport jrProcessor = null;
        if ("daily".equalsIgnoreCase(activityReportType)) {
            jrProcessor = new DailyJrProcessor("daily-member-activity-report",
                    messageSource.getMessage("member.activity.report.daily.title", null, locale),
                    messageSource.getMessage("member.activity.report.list.daily.title", null, locale),
                    dataSource, reportType);
        } else if ("weekly".equalsIgnoreCase(activityReportType)) {
            jrProcessor = new WeeklyJrProcessor("weekly-member-activity-report",
                    messageSource.getMessage("member.activity.report.weekly.title", null, locale),
                    messageSource.getMessage("member.activity.report.list.weekly.title", null, locale),
                    dataSource, reportType);
        } else {
            MonthlyJrProcessor monthlyJrProcessor = new MonthlyJrProcessor("mothly-member-activity-report",
                    messageSource.getMessage("member.activity.report.monthly.title", null, locale),
                    messageSource.getMessage("member.activity.report.list.monthly.title", null, locale),
                    dataSource, reportType);
            monthlyJrProcessor.setMontlySegmentationSummaryReportTitle(
                    messageSource.getMessage("member.activity.report.monthly.custgroup.title", null, locale));
            QLookupDetail qLookupDetail = QLookupDetail.lookupDetail;
            monthlyJrProcessor.setCustomerGroups(new JPAQuery(em).from(qLookupDetail).where(
                    qLookupDetail.header.code.eq(codePropertiesService.getHeaderCustomerGroup())
                    .and(qLookupDetail.code.startsWithIgnoreCase("TEST").not().and(qLookupDetail.status.eq(Status.ACTIVE))))
                    .list(ConstructorExpression.create(CodeDescDto.class, qLookupDetail.code, qLookupDetail.description)));
            jrProcessor = monthlyJrProcessor;
        }
        jrProcessor.setDateFrom(dateFrom);
        jrProcessor.setDateTo(dateTo);
        jrProcessor.setStoreCode(storeCode);
        return jrProcessor;
    }

    @Override
    public ReportType[] getReportTypes() {
        return new ReportType[]{ReportType.EXCEL, ReportType.PDF};
    }

    private LocalDate parseDate(String dateInstance) {
        return StringUtils.isNotBlank(dateInstance) ? INPUT_DATE_PATTERN.parseLocalDate(dateInstance) : null;
    }

}
