<%@include file="../../common/taglibs.jsp" %>


<div id="content_inventoryStock">

  <div class="page-header page-header2"><h1><spring:message code="gc.invstock.page" /></h1></div>

  <div id="searchForm" class="form-horizontal well">
    <form:form id="form_inventorySearch" modelAttribute="inventorySearch">
      <div class="form-inline search-block">
        <label class="label-single">
          <h5><spring:message code="entity_search" />:</h5>
        </label>
        <select name="vendorFormalName" class="vendor" id="vendor">
          <option value=""/><spring:message code="gc.product.label.vendor" /></option>
          <c:forEach var="item" items="${vendors}"><option value="${item}">${item}</option></c:forEach>
        </select>

        <select name="cardType" class="cardType" id="cardType">
          <c:forEach var="item" items="${cardTypes}"><option value="${item.code}">${item.desc}</option></c:forEach>
        </select>

        <c:url var="url_cardTypesList" value="/giftcard/inventory/stock/cards/list/" />
        <form:select path="location" class="location" id="location" data-url="${url_cardTypesList}">
          <c:forEach var="item" items="${locations}"><option value="${item.code}">${item.code} - ${item.desc}</option></c:forEach>           
        </form:select>

        <input id="searchBtn" class="btn btn-primary" type="submit" value="<spring:message code="label_search" />" />
        <input id="resetBtn" type="button" class="btn" value="<spring:message code="label_reset" />" />
      </div>
      
      <div class="form-inline search-block">
        <label class="label-single">
        </label>
        <div class="input-daterange" id="duration">
          <%-- <form:hidden path="createdFromMilli" id="createdFromMilli" />
          <form:hidden path="createdToMilli" id="createdToMilli" /> --%>
          <spring:message var="labelFrom" code="label_from" />
          <spring:message var="labelTo" code="label_to" />
          <form:input path="createdFromStr" id="dateFrom"  class="input" placeholder="${labelFrom}"/>
          <form:input path="createdToStr" id="dateTo"  class="input" placeholder="${labelTo}"/>
        </div>
      </div>
      
    </form:form>
  </div>


  <div class="form-horizontal hide">
    <select name="reportType" class="input-small" id="reportType">
      <c:forEach var="reportType" items="${reportTypes}">
        <option data-url="<c:url value="/giftcard/inventory/stock/print/${reportType.code}" />" value="${reportType}">${reportType}</option>
      </c:forEach>
    </select>
    <input type="button" id="printBtn" value="<spring:message code="label_print" />" class="btn btn-print btn-print-small tiptip" title="Print"/>
    
  </div>
  <div class="checkbox pull">
    <input id="allCards" type="checkbox"/>All Cards
  </div>
  <div class="checkbox pull">
    <input id="allLocations" type="checkbox"/>All Locations
  </div>
    
  <div id="content_inventoryStockTxnStat" 
    <c:forEach var="item" items="${txnStatus}">data-${item}='<spring:message code="gc.invstock.txnstat.${item}"/>'</c:forEach> >
  </div>
  
  <div class="btn-group">
    <button type="button" class="btn btn-primary"><spring:message code="gc.stock.label.print.current.stock" /></button>
    <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown">
      <span class="caret"></span>
      <span class="sr-only"></span>
    </button>
    <ul class="dropdown-menu" role="menu">
      <li><a id="printListSummary" href="#"><spring:message code="gc.stock.label.print.current.stock.summary" /></a></li>
      <li><a id="printListDetail" href="#"><spring:message code="gc.stock.label.print.current.stock.detail"/></a></li>
    </ul>
  </div>
  
  <button type="button" data-url="<spring:url value="/report/template/pdf/Gift Card Daily Inventory Report/view?" />" id="dailyInventoryBtn" class="btn btn-primary"><spring:message code="daily.inventory.title" /></button>
  
  
  <div id="content_inventoryStockList">
    <div id="list_inventoryStock" 
        data-url="<c:url value="${listUrl}" />" 
        data-hdr-cardtype="<spring:message code="gc.invstock.cardtype"/>" 
        data-hdr-txntype="<spring:message code="gc.invstock.txn.type"/>" 
        data-hdr-txndateandtime="<spring:message code="gc.invstock.txn.date"/>" 
        data-hdr-beginningbal="<spring:message code="gc.invstock.beginningbal"/>" 
        data-hdr-txntype="<spring:message code="gc.invstock.txn.type"/>" 
        data-hdr-quantity="<spring:message code="gc.invstock.qty"/>" 
        data-hdr-endingbal="<spring:message code="gc.invstock.endingbal"/>" ></div>
  </div>


<link href="<c:url value="/css/bootstrap/datepicker.css" />" rel="stylesheet" />
<link href="<spring:url value="/css/dataTables.css" />" rel="stylesheet">
<script src="<spring:url value="/js/jquery/jquery.dataTables.js" />" ></script>
<script src="<spring:url value="/js/jquery/jquery.dataTables.linkLengthMenu.js" />"></script>
<script src="<spring:url value="/js/jquery/jquery.dataTables.customPaging.js" />"></script>
<script src="<spring:url value="/js/jquery/ajax-datatable.js" />" ></script>
<script src="<spring:url value="/js/common/form2js.js" />" ></script>
<script src="<spring:url value="/js/common/json2.js" />" ></script>
<script src="<spring:url value="/js/jquery/jquery.toObject.js" />" ></script>
<script src='<c:url value="/js/bootstrap/bootstrap-datepicker.js"/>' type='text/javascript'><![CDATA[&nbsp; ]]></script>
<script type="text/javascript">
var InventoryStockList = null, InventoryStock = null;
$(document).ready( function() {

    var $container = $( "#content_inventoryStock" );
    var $list = $container.find( "#list_inventoryStock" );
    var $txnStat = $container.find( "#content_inventoryStockTxnStat" );
    var $location = $container.find( "#location" );
    var $cardType = $container.find( "#cardType" );

    initDataTable();
    initSearch();
    //initPrint();
    $("#printListSummary").click(printListSummary);
    $("#printListDetail").click(printListDetail);
    
    $("#dailyInventoryBtn").click(function() {
    	var url = $(this).data("url");
    	var formVal = $("#form_inventorySearch").serialize();
    	window.open(url + formVal, '_blank', 'toolbar=0,location=0,menubar=0');
    });

    function initDataTable() {
        $list.ajaxDataTable({
            'autoload'      : false, /* disabled autoload as per Jules and Bug #93656 */
            'ajaxSource'    : $list.data( "url" ),
            'aaSorting'     : [[ 0, 'desc' ]],
            'columnHeaders' : [ 
                $list.data( "hdr-cardtype" ),
                $list.data( "hdr-txndateandtime" ),
                $list.data( "hdr-beginningbal" ),
                $list.data( "hdr-txntype" ),
                $list.data( "hdr-quantity" ),
                $list.data( "hdr-endingbal" )
            ],
            'modelFields'  : [
                { name  : 'productCode', sortable : false, fieldCellRenderer : function (data, type, row) {
                    return ( row.productCode? ( row.productCode  + ( row.productName? " - " + row.productName : "" ) ) : "" );
                }},
                { name  : 'compiledTimestamp', sortable : true, fieldCellRenderer : function (data, type, row) {
                      return ( row.compiledTimestamp? new Date( row.compiledTimestampMilli - 0 ).customize( 1 ) : null );
                  }},
                { name  : 'beginningBalance', sortable : false, fieldCellRenderer : function (data, type, row) {
                    return null != row.beginningBalance? ( "" + row.beginningBalance ).replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1") : "";
                }},
                { name  : 'transaction', sortable : false, fieldCellRenderer : function (data, type, row) {
                    return row.transaction != null ? row.transaction.toLowerCase() : '';
                }},
                { name  : 'quantity', sortable : false, fieldCellRenderer : function (data, type, row) {
                    return null != row.quantity? ( "" + row.quantity ).replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1") : "";
                }},
                { name  : 'endingBalance', sortable : false, fieldCellRenderer : function (data, type, row) {
                    return null != row.endingBalance? ( "" + row.endingBalance ).replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1") : "";
                }}
            ]
        });
    }

    function initSearch() {
        var $searchFormCon = $container.find( "#searchForm" );
        var $duration = $searchFormCon.find( "#duration" );
        var $durationFrom = $duration.find( "#dateFrom" );
        var $durationTo = $duration.find( "#dateTo" );
        var $searchForm = $searchFormCon.find( "#form_inventorySearch" );
        var $submit = $searchFormCon.find( "#searchBtn" );
        var $reset = $searchFormCon.find( "#resetBtn" );

        $duration.datepicker({ autoclose : true, format : "dd M yyyy" });

        $submit.click( function(e) {
            e.preventDefault();
            var $formDataObj = $searchForm.toObject( { mode : 'first', skipEmpty : true } );
            /* $formDataObj.createdFromMilli = $durationFrom.val()? $durationFrom.datepicker( "getDate" ).getTime() : null;
            $formDataObj.createdToMilli = $durationTo.val()? $durationTo.datepicker( "getDate" ).getTime() : null; */
            $submit.prop( 'disabled', true );
            InventoryStockList.filterTable( $formDataObj, function () { $submit.prop( 'disabled', false ); } );
        });

        $reset.click( function(e) {
            InventoryStock.reset( $searchForm );
            $submit.click();
        });

        $location.change( function(e) {
            e.preventDefault();
            $.post( $(this).data( "url" ) + $(this).val(), function( resp ) {
            	  if ( resp.success ) {
            		    if ( resp.result.length ) {
            		    	  $.each( resp.result, function( key, val ) { $cardType.append( new Option( val.desc, val.code ) ); } ); 
            		    }
            		    else { 
            		    	  $cardType.find( "option" ).remove(); 
            		    }
            	  }
            }, "json" );
        });
    }

    function initPrint() {
        var $print = $container.find( "#printBtn" );
        $print.click( function(e) {
            e.preventDefault();
            var url = $("#reportType option:selected").data("url") + "?" + $("#inventoryTrackerForm").serialize();
            
            var selectedReportType = $( "select[name='reportType']" ).val();
            if ( selectedReportType == "pdf" || selectedReportType == "PDF" ) {
                window.open( url, '_blank', 'toolbar=0,location=0,menubar=0' );
            }
            else if ( selectedReportType == "excel" || selectedReportType == "EXCEL" ) {
                window.location.href = url;
            }
        });
    }

    InventoryStockList = {
        reloadTable  : function() { $list.ajaxDataTable( "search" ); },
        filterTable  : function( filterForm, retFunction ) { $list.ajaxDataTable( "search", filterForm, retFunction ); }
    };

    InventoryStock = {
        reset     : function( ele, isEmpty ) {
            $( ele ).find(':input').each( function() {
                switch(this.type) {
                    case 'password':
                    case 'select-one':
                    case 'text':
                    case 'textarea':
                    case 'file':
                    case 'hidden': $(this).val(''); break;
                    case 'select-multiple': isEmpty? $(this).empty() : $(this).val(""); break;
                    case 'checkbox':
                    case 'radio': this.checked = false;
                }
            });
        }
    };

    function printListSummary() {
      var cardType = $("#cardType").val();
      var location = $("#location").val();
      var vendor = $("#vendor").val();
      var allCards = $("#allCards").attr("checked");
      var allLocations = $("#allLocations").attr("checked");
      var url = ctx + "/giftcard/inventory/printlist/SUMMARY";
      
      if (cardType != "" && allCards != "checked") {
        url = url + "/" + cardType;
      } else {
        url = url + "/null";
      }
      
      if (location != "" && allLocations != "checked") {
        url = url + "/" + location;
      } else {
        url = url + "/null";
      }
      
      if (vendor != "") {
        url = url + "/" + vendor;
      } else {
        url = url + "/null";
      }
  	  window.location.href = url;
    }
    
    function printListDetail() {
      var cardType = $("#cardType").val();
      var location = $("#location").val();
      var vendor = $("#vendor").val();
      var allCards = $("#allCards").attr("checked");
      var allLocations = $("#allLocations").attr("checked");
      var url = ctx + "/giftcard/inventory/printlist/DETAILED";
      
      if (cardType != "" && allCards != "checked") {
        url = url + "/" + cardType;
      } else {
        url = url + "/null";
      }
      
      if (location != "" && allLocations != "checked") {
        url = url + "/" + location;
      } else {
        url = url + "/null";
      }
      
      if (vendor != "") {
        url = url + "/" + vendor;
      } else {
        url = url + "/null";
      }
  	  window.location.href = url;
    }
});
</script>
<style type="text/css">
  <!-- 
  .row-fluid { width: 100px !important; display: inline-block; display: inline; margin-right: 5px; }
  .input-daterange { display: inline-block; display: inline; }
  .well select, .well .input {
      margin: 5px;
  }
  -->
</style>


</div>