package com.transretail.crm.report.template.impl;

import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Lists;
import com.mysema.query.Tuple;
import com.mysema.query.jpa.impl.JPAQuery;
import com.transretail.crm.common.reporting.exception.ReportException;
import com.transretail.crm.common.reporting.jasper.JRProcessor;
import com.transretail.crm.common.reporting.service.ReportService;
import com.transretail.crm.core.dto.PointsIssuanceDto;
import com.transretail.crm.core.dto.QPointsIssuanceDto;
import com.transretail.crm.core.entity.Campaign;
import com.transretail.crm.core.entity.MemberModel;
import com.transretail.crm.core.entity.PointsTxnModel;
import com.transretail.crm.core.entity.Program;
import com.transretail.crm.core.entity.Promotion;
import com.transretail.crm.core.entity.QMemberModel;
import com.transretail.crm.core.entity.QPointsTxnModel;
import com.transretail.crm.core.entity.QPromotion;
import com.transretail.crm.core.entity.QTransactionAudit;
import com.transretail.crm.core.entity.TransactionAudit;
import com.transretail.crm.core.entity.embeddable.Address;
import com.transretail.crm.core.entity.embeddable.CustomerProfile;
import com.transretail.crm.core.entity.embeddable.Duration;
import com.transretail.crm.core.entity.embeddable.ProfessionalProfile;
import com.transretail.crm.core.entity.enums.MemberCreatedFromType;
import com.transretail.crm.core.entity.enums.PointTxnType;
import com.transretail.crm.core.entity.enums.ProgramType;
import com.transretail.crm.core.entity.lookup.LookupDetail;
import com.transretail.crm.core.entity.lookup.QLookupDetail;
import com.transretail.crm.core.repo.CampaignRepo;
import com.transretail.crm.core.repo.LookupDetailRepo;
import com.transretail.crm.core.repo.LookupHeaderRepo;
import com.transretail.crm.core.repo.MemberRepo;
import com.transretail.crm.core.repo.PointsTxnModelRepo;
import com.transretail.crm.core.repo.ProgramRepo;
import com.transretail.crm.core.repo.PromotionRepo;
import com.transretail.crm.core.repo.TransactionAuditRepo;
import com.transretail.crm.core.service.CodePropertiesService;
import com.transretail.crm.report.template.ReportTemplate;
import java.io.File;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Scanner;
import javax.annotation.Resource;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import net.sf.jasperreports.engine.JRParameter;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.nullValue;
import static org.hamcrest.core.IsNot.not;
import org.joda.time.LocalDate;
import org.joda.time.LocalDateTime;
import org.junit.After;
import static org.junit.Assert.assertThat;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.context.support.MessageSourceResourceBundle;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.TransactionCallback;
import org.springframework.transaction.support.TransactionTemplate;

/**
 *
 * @author Monte Cillo Co (mco@exist.com)
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath*:/META-INF/spring/applicationContext*.xml",
    "/META-INF/spring/applicationContext-test.xml"})
public class PointsIssuanceReportTest {

    @PersistenceContext
    private EntityManager em;
    @Autowired
    private PlatformTransactionManager transactionManager;
    @Resource(name = "pointsIssuanceReport")
    private ReportTemplate pointsIssuanceReport;
    @Autowired
    private LookupHeaderRepo lookupHeaderRepo;
    @Autowired
    private LookupDetailRepo lookupDetailRepo;
    @Autowired
    private PromotionRepo promotionRepo;
    @Autowired
    private MemberRepo memberRepo;
    @Autowired
    private TransactionAuditRepo taRepo;
    @Autowired
    private PointsTxnModelRepo pointsTxnModelRepo;
    @Autowired
    private ProgramRepo programRepo;
    @Autowired
    private CampaignRepo campaignRepo;
    @Autowired
    private CodePropertiesService codePropertiesService;
    @Autowired
    private ReportService reportService;
    @Autowired
    private MessageSource messageSource;

    private static final Logger logger = LoggerFactory.getLogger(PointsIssuanceReportTest.class);

    @Before
    public void onSetup() {
	setUpLookupDetails();
	setupPromotion();
    }

    @After
    public void tearDown() {
	pointsTxnModelRepo.deleteAll();
	taRepo.deleteAll();
	promotionRepo.deleteAll();
	campaignRepo.deleteAll();
	programRepo.deleteAll();

	memberRepo.deleteAll();
	lookupDetailRepo.deleteAll();
	lookupHeaderRepo.deleteAll();
    }

    @Test
    public void createJrProcessorTest() throws ReportException {
	setupMember();
	// TODO: Refactoring here, code is ugly
	PointsTxnModel pEarn = new PointsTxnModel();
	pEarn.setId("pa-id-01");
	pEarn.setTransactionAmount(6000.0);
	pEarn.setTransactionDateTime(new LocalDateTime(2014, 03, 06, 14, 45).toDate());
	pEarn.setTransactionNo("TXN_TEST_1001");
	pEarn.setTransactionPoints(3.0);
	pEarn.setTransactionType(PointTxnType.EARN);
	pointsTxnModelRepo.save(pEarn);

	PointsTxnModel pRewards = new PointsTxnModel();
	pRewards.setId("pa-id-02");
	pRewards.setTransactionAmount(6000.0);
	pRewards.setTransactionDateTime(new LocalDateTime(2014, 03, 06, 14, 45).toDate());
	pRewards.setTransactionNo("TXN_TEST_1002");
	pRewards.setTransactionPoints(3.0);
	pRewards.setTransactionType(PointTxnType.REWARD);
	pointsTxnModelRepo.save(pRewards);

	QMemberModel qm = QMemberModel.memberModel;
	MemberModel member = memberRepo.findOne(qm.accountId.eq("accountId1"));

	TransactionAudit ta = new TransactionAudit();
	ta.setId("ta-id");
	ta.setMemberModel(member);
	ta.setTransactionNo("TXN_TEST_1001");
	ta.setTransactionPoints(3l);
	QPromotion qpm = QPromotion.promotion;
	Promotion promo = promotionRepo.findOne(qpm.name.eq("test-promo"));
	assertThat(promo, is(not(nullValue())));
	assertThat(promo.getId(), is(not(nullValue())));
	ta.setPromotion(promo);
	taRepo.save(ta);

	QPointsTxnModel qpoints = QPointsTxnModel.pointsTxnModel;
	QPromotion qpromo = QPromotion.promotion;
	QTransactionAudit qta = QTransactionAudit.transactionAudit;
	QLookupDetail qld = QLookupDetail.lookupDetail;

	String month = "3";
	int monthIndex = Integer.valueOf(month);
	String year = "2014";
	int yearInt = Integer.valueOf(year);

	//<editor-fold defaultstate="collapsed" desc="Query Test must be remove">
	final LocalDate baseDate = new LocalDate(yearInt, monthIndex, 1);
	Date startDate = baseDate.toDate();
	final int lastDayOfMonth = baseDate.dayOfMonth().withMaximumValue().getDayOfMonth();
	Date endDate = new LocalDate(yearInt, monthIndex, lastDayOfMonth).toDate();

	List<PointsIssuanceDto> rows = new JPAQuery(em).from(qpoints, qta, qpromo, qld)
		.where(qpoints.transactionType.eq(PointTxnType.EARN)
			.and(qpoints.transactionNo.eq(qta.transactionNo))
			.and(qta.promotion.id.eq(qpromo.id))
			.and(qpromo.rewardType.code.eq(qld.code))
			.and(qpoints.transactionDateTime.between(startDate, endDate)))
		.groupBy(qpromo.rewardType.code, qpoints.transactionDateTime.dayOfMonth())
		.orderBy(qpoints.transactionDateTime.asc())
		.list(new QPointsIssuanceDto(qld.description,
				qpoints.transactionPoints.sum(), qpoints.transactionDateTime.dayOfMonth()));
	// TODO: remove? change? this is for debugging purpose only
	for (PointsIssuanceDto pi : rows) {
	    logger.debug("{} {} {}", pi.getRewardType(), pi.getTotalPointsPerDay(), pi.getDay());
	}

	List<Tuple> results = new JPAQuery(em).from(qpoints, qta, qpromo, qld)
		.where(qpoints.transactionType.eq(PointTxnType.EARN)
			.and(qpoints.transactionNo.eq(qta.transactionNo))
			.and(qta.promotion.id.eq(qpromo.id))
			.and(qpoints.transactionDateTime.between(startDate, endDate)))
		.groupBy(qpoints.transactionDateTime.dayOfMonth())
		.orderBy(qpoints.transactionDateTime.dayOfMonth().asc())
		.list(qpoints.transactionPoints.sum(), qpoints.transactionDateTime.dayOfMonth());
	ArrayList<PointsIssuanceDto> chartData = Lists.newArrayList();
	for (Tuple tuple : results) {
	    logger.debug("{} {}", tuple.get(0, Double.class), tuple.get(1, Integer.class));
	    chartData.add(new PointsIssuanceDto("Total Points", tuple.get(0, Double.class), tuple.get(1, Integer.class)));
	}
	//</editor-fold>

	Map<String, String> param = new ImmutableMap.Builder<String, String>()
		.put(PointsIssuanceReport.FILTER_TARGET_YEAR, year)
		.put(PointsIssuanceReport.FILTER_TARGET_MONTH, month)
		.build();
	JRProcessor jrProcessor = pointsIssuanceReport.createJrProcessor(param);
	File testClassesDir = FileUtils.toFile(Thread.currentThread().getContextClassLoader().getResource(""));
	File output = new File(testClassesDir, "test-points-issuance-report-output.pdf");
	logger.debug("Output location {}", output.getAbsolutePath());
	output.delete();
	Locale locale = LocaleContextHolder.getLocale();
	jrProcessor.addParameter(JRParameter.REPORT_LOCALE, locale);
	jrProcessor.addParameter(JRParameter.REPORT_RESOURCE_BUNDLE, new MessageSourceResourceBundle(messageSource, locale));
	reportService.exportPdfReport(jrProcessor, output);

	double bytes = output.length();
	double kilobytes = (bytes / 1024);
	logger.debug("Kilobytes: {}", kilobytes);
    }

    @Test
    public void getFilterTest() {
	assertThat(pointsIssuanceReport.getFilters().size(), is(2));
    }

    private void setupMember() {
	String preferredStore1 = "TITA GWAPA";
	int index = 1;
	MemberModel model = new MemberModel();
	model.setUsername("username" + index);
	String accountId = "accountId" + index;
	model.setAccountId(accountId);
	model.setFirstName("FNUser" + index);
	model.setLastName("LNUser" + index);
	model.setContact("contact" + index);
	model.setPin("pin" + index);
	model.setEnabled(true);
	model.setRegisteredStore(preferredStore1);
	model.setStoreName("TITA GWAPA Store");
	CustomerProfile customerProfile = new CustomerProfile();
	ProfessionalProfile professionalProfile = new ProfessionalProfile();
	Address address = new Address();
	address.setCity("City" + index);
	address.setStreet("Street" + index);
	address.setPostCode("1234" + index);

	model.setMemberType(new LookupDetail(codePropertiesService.getDetailMemberTypeEmployee()));
	model.setMemberCreatedFromType(MemberCreatedFromType.FROM_UPLOAD);
	customerProfile.setGender(new LookupDetail(codePropertiesService.getDetailGenderFemale()));

	String departmentCode = getCode(codePropertiesService.getHeaderBusinessFields(), "Other");
	professionalProfile.setDepartment(new LookupDetail(departmentCode));
	customerProfile.setBirthdate(new Date());

	String empTypeCode = getCode(codePropertiesService.getHeaderEmpType(), "PROBATIONARY ASSOCIATE");
	model.setEmpType(new LookupDetail(empTypeCode));

	String genderCode = getCode(codePropertiesService.getHeaderGender(), "Female");
	customerProfile.setGender(new LookupDetail(genderCode));

	String religionCode = getCode(codePropertiesService.getHeaderReligion(), "Islam");
	customerProfile.setReligion(new LookupDetail(religionCode));

	String nationalityCode = getCode(codePropertiesService.getHeaderNationality(), "INDONESIAN");
	customerProfile.setNationality(new LookupDetail(nationalityCode));

	customerProfile.setPlaceOfBirth("birthplace" + index);
	model.setContact("0856309991" + index);

	model.setProfessionalProfile(professionalProfile);
	model.setCustomerProfile(customerProfile);
	model.setAddress(address);
	memberRepo.save(model);
    }

    protected String getCode(String headerCode, String description) {
	QLookupDetail qLookupDetail = QLookupDetail.lookupDetail;
	String code = new JPAQuery(em).from(qLookupDetail).where(
		qLookupDetail.header.code.eq(headerCode)
		.and(qLookupDetail.description.toUpperCase().eq(description.toUpperCase()))
	)
		.uniqueResult(
			qLookupDetail.code);
	return code;
    }

    private void setUpLookupDetails() {
	InputStream is1 = Thread.currentThread().getContextClassLoader().getResourceAsStream("A_LOOKUPHEADERS.sql");
	InputStream is2 = Thread.currentThread().getContextClassLoader().getResourceAsStream("B_LOOKUPDETAILS.sql");
	try {
	    final Scanner in1 = new Scanner(is1);
	    final Scanner in2 = new Scanner(is2);
	    TransactionTemplate transactionTemplate = new TransactionTemplate(transactionManager);
	    transactionTemplate.execute(new TransactionCallback() {
		@Override
		public Object doInTransaction(TransactionStatus status) {
		    while (in1.hasNext()) {
			String script = in1.nextLine();
			if (StringUtils.isNotBlank(script)) {
			    em.createNativeQuery(script).executeUpdate();
			}
		    }
		    while (in2.hasNext()) {
			String script = in2.nextLine();
			if (StringUtils.isNotBlank(script)) {
			    em.createNativeQuery(script).executeUpdate();
			}
		    }
		    em.clear();
		    em.flush();
		    return null;
		}
	    });
	} finally {
	    IOUtils.closeQuietly(is1);
	    IOUtils.closeQuietly(is2);
	}
    }

    private void setupPromotion() {
	Duration d = new Duration();
	d.setStartDate(new LocalDate(2014, 01, 01).toDate());
	d.setEndDate(new LocalDate(2014, 12, 31).toDate());

	Program pr = new Program();
	pr.setName("test-program");
	pr.setDuration(d);
	pr.setStatus(lookupDetailRepo.findByCode("STAT001"));
	pr.setType(ProgramType.BASIC);
	pr.setDescription("test-program description");
	programRepo.save(pr);

	Campaign c = new Campaign();
	c.setName("test-campaign");
	c.setDescription("des");
	c.setDuration(d);
	c.setProgram(pr);
	campaignRepo.save(c);

	final Promotion p = new Promotion(123l);
	p.setAffectedDays("DAYS001,DAYS002,DAYS003,DAYS004,DAYS005,DAYS006,DAYS007");
	p.setName("test-promo");
	p.setStartDate(d.getStartDate());
	p.setEndDate(d.getEndDate());
	p.setRewardType(lookupDetailRepo.findByCode("RWRD001"));
	p.setStatus(lookupDetailRepo.findByCode("STAT001"));
	p.setCampaign(c);

	promotionRepo.save(p);
    }
}
