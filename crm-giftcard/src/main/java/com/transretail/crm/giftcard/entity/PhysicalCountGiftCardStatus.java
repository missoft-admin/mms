package com.transretail.crm.giftcard.entity;

import java.io.Serializable;
import javax.persistence.*;

/**
 *
 * @author Monte Cillo Co (mco@exist.com)
 */
@Entity
@Table(name = "CRM_GC_PCS_CARD_STATUS")
@IdClass(PhysicalCountGiftCardStatusId.class)
public class PhysicalCountGiftCardStatus implements Serializable {

    public static enum Status {

	MISSING, FOUND
    }

    @Id
    @ManyToOne
    @JoinColumn(name = "PCS_ID")
    private GiftCardPhysicalCountSummary summary;
    @Id
    @ManyToOne
    @JoinColumn(name = "GC_INV_ID")
    private GiftCardInventory inventory;
    @Enumerated(EnumType.STRING)
    @Column(name = "STATUS", nullable = false, length = 7)
    private Status status;

    public PhysicalCountGiftCardStatus inventory(GiftCardInventory item) {
	this.inventory = item;
	return this;
    }

    public PhysicalCountGiftCardStatus summary(GiftCardPhysicalCountSummary summary) {
	this.summary = summary;
	return this;
    }

    public PhysicalCountGiftCardStatus status(PhysicalCountGiftCardStatus.Status status) {
	this.status = status;
	return this;
    }

    public GiftCardPhysicalCountSummary getSummary() {
	return summary;
    }

    public void setSummary(GiftCardPhysicalCountSummary summary) {
	this.summary = summary;
    }

    public GiftCardInventory getInventory() {
	return inventory;
    }

    public void setInventory(GiftCardInventory inventory) {
	this.inventory = inventory;
    }

    public Status getStatus() {
	return status;
    }

    public void setStatus(Status status) {
	this.status = status;
    }

}
